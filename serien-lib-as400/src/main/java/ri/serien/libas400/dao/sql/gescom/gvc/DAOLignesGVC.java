/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.gvc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import ri.serien.libas400.dao.gvx.programs.parametres.GM_Personnalisation;
import ri.serien.libas400.dao.gvx.programs.tiers.pricejet.GestionPriceJet;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.gescom.commun.article.GroupeArticles;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeMarques;
import ri.serien.libcommun.gescom.commun.article.StatutArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.vente.gvc.ConditionAchatGVC;
import ri.serien.libcommun.gescom.vente.gvc.EnumEtatWorkFlow;
import ri.serien.libcommun.gescom.vente.gvc.EtatWorkFlow;
import ri.serien.libcommun.gescom.vente.gvc.FiltresGVC;
import ri.serien.libcommun.gescom.vente.gvc.LigneGvc;
import ri.serien.libcommun.gescom.vente.gvc.WorkFlowGVC;
import ri.serien.libcommun.gescom.vente.gvc.pricejet.ObjetPricejet;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class DAOLignesGVC {
  protected QueryManager queryManager = null;
  private static final int NB_MAX_ENREGISTREMENTS_SQL = 1000;
  
  public DAOLignesGVC(QueryManager aqueryManager) {
    queryManager = aqueryManager;
  }
  
  /**
   * Retourner le nombre d'enregistrements d'une recherche de lignes GVC
   */
  public int chargerNombreLignesGVC(FiltresGVC pFiltres) {
    int nombrelignesGVC = -1;
    String sqlFiltres = convertirFiltresSQL(pFiltres);
    if (sqlFiltres == null) {
      return -1;
    }
    
    String requete = " SELECT COUNT(art.A1ART) AS NBLIGNES" + " FROM " + queryManager.getLibrary() + ".pgvmartm AS art " + " LEFT JOIN "
        + queryManager.getLibrary() + ".pgvmeaam e  ON e.a1etb = art.a1etb AND e.a1art = art.a1art " + " LEFT JOIN "
        + queryManager.getLibrary() + ".PGVMFRSM f ON art.a1etb = f.fretb AND CONCAT(art.a1cof, art.a1frs) = CONCAT(f.frcol, f.frfrs) "
        + " LEFT JOIN " + queryManager.getLibrary() + ".PSEMGVCWM w ON art.a1etb = w.wrk_etb AND art.a1art = w.wrk_art " + sqlFiltres;
    
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    
    if (listrcd != null && listrcd.size() > -1) {
      if (listrcd.size() == 1 && listrcd.get(0).isPresentField("NBLIGNES")) {
        nombrelignesGVC = Integer.parseInt(listrcd.get(0).getField("NBLIGNES").toString().trim());
      }
    }
    else {
      Trace.erreur("[GVC] lireNombreLignesGVC() SQL:" + queryManager.getMsgError());
    }
    return nombrelignesGVC;
  }
  
  /**
   * Retourner les enregistrements d'une recherche de lignes GVC
   */
  public ArrayList<LigneGvc> chargerListeLigneGvc(FiltresGVC pFiltres, boolean onlitTout) {
    
    String sqlFiltres = convertirFiltresSQL(pFiltres);
    if (sqlFiltres == null) {
      return null;
    }
    
    ArrayList<LigneGvc> listeLignesGVC = null;
    
    int indexDebut = pFiltres.getIndexDebut();
    int indexFin = pFiltres.getIndexFin();
    
    if (onlitTout) {
      indexDebut = 0;
      indexFin = NB_MAX_ENREGISTREMENTS_SQL;
    }
    
    String requete = "" + " SELECT num, tmpa1etb, tmpa1art, a1npu, a1in15, substr(a1fam, 1, 1) as a1famgrp, "
        + " a1fam, a1sfa, a1lib, a1lb1, a1lb2, a1lb3, a1ref1, a1top2, a1zp21, a1cof,a1frs, concat(a1cof, a1frs) AS prcolfrs, nomFour, "
        + " WRK_EXP,WRK_DES,WRK_ETAT,VG_PRIX,VG_NOM,VG_DATE " + " FROM " + " ("
        + " SELECT row_number() over() AS num, art.a1etb AS tmpa1etb, art.a1art AS tmpa1art, art.a1lib AS tmpa1lib, art.*, e.*, f.frnom as"
        + " nomFour, w.*,v.* " + " FROM " + queryManager.getLibrary() + ".PGVMARTM AS art " + " LEFT JOIN " + queryManager.getLibrary()
        + ".PGVMEAAM e  ON e.a1etb = art.a1etb AND e.a1art = art.a1art " + " LEFT JOIN " + queryManager.getLibrary()
        + ".PGVMFRSM f ON art.a1etb = f.fretb AND CONCAT(art.a1cof, art.a1frs) = CONCAT(f.frcol, f.frfrs) " + " LEFT JOIN "
        + queryManager.getLibrary() + ".PSEMGVCWM w ON art.a1etb = w.wrk_etb AND art.a1art = w.wrk_art " + " LEFT JOIN "
        + queryManager.getLibrary() + ".PSEMVGSBM v ON art.a1etb = v.VG_A1ETB AND art.a1art = v.VG_A1ART " + sqlFiltres + ") "
        + " AS tmp WHERE num BETWEEN " + indexDebut + " AND " + indexFin;
    
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    
    if (listrcd == null || listrcd.size() < 0) {
      Trace.erreur("[GVC] lireLignesGVC() SQL:" + queryManager.getMsgError());
      return null;
    }
    
    listeLignesGVC = new ArrayList<LigneGvc>();
    GM_Personnalisation personnalisation = new GM_Personnalisation(queryManager);
    
    for (GenericRecord record : listrcd) {
      LigneGvc ligneGvc = new LigneGvc();
      
      if (record.isPresentField("TMPA1ART")) {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getField("TMPA1ETB").toString());
        IdArticle idArticle = IdArticle.getInstance(idEtablissement, record.getField("TMPA1ART").toString());
        ligneGvc.setIdArticle(idArticle);
      }
      
      if (record.isPresentField("A1LIB") && record.isPresentField("A1LB1") && record.isPresentField("A1LB2")
          && record.isPresentField("A1LB3")) {
        ligneGvc.setLibelleArticle(record.getField("A1LIB").toString().trim() + record.getField("A1LB1").toString().trim()
            + record.getField("A1LB2").toString().trim() + record.getField("A1LB3").toString().trim());
      }
      
      String libelle = "";
      if (record.isPresentField("A1LIB")) {
        libelle += record.getField("A1LIB").toString().trim();
      }
      
      if (record.isPresentField("A1LB1")) {
        libelle += record.getField("A1LB1").toString().trim();
      }
      
      if (record.isPresentField("A1LB2")) {
        libelle += record.getField("A1LB2").toString().trim();
      }
      
      if (record.isPresentField("A1LB3")) {
        libelle += record.getField("A1LB3").toString().trim();
      }
      ligneGvc.setLibelleArticle(libelle);
      
      // Fournisseur principal
      Fournisseur fournisseurPrincipal = null;
      if (record.isPresentField("A1COF") && record.isPresentField("A1FRS")) {
        if (record.getIntValue("A1FRS", 0) > 0 && record.getIntValue("A1COF", 0) > 0) {
          IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("TMPA1ETB"));
          IdFournisseur idFournisseur =
              IdFournisseur.getInstance(idEtablissement, record.getIntValue("A1COF", 0), record.getIntValue("A1FRS", 0));
          
          fournisseurPrincipal = new Fournisseur(idFournisseur);
          if (record.isPresentField("NOMFOUR")) {
            fournisseurPrincipal.setNomFournisseur(record.getField("NOMFOUR").toString().trim());
          }
        }
      }
      ligneGvc.setFournisseurPrincipal(fournisseurPrincipal);
      
      // Groupe de la ligneGvc
      if (record.isPresentField("A1FAMGRP")) {
        GroupeArticles groupeArticle =
            personnalisation.lireUnGroupearticles(pFiltres.getEtablissementEnCours(), record.getField("A1FAMGRP").toString().trim());
        ligneGvc.setGroupeArticles(groupeArticle);
      }
      // Famille de la ligneGvc
      if (record.isPresentField("A1FAM")) {
        Famille famille =
            personnalisation.lireUneFamilleArticles(pFiltres.getEtablissementEnCours(), record.getField("A1FAM").toString().trim());
        
        ligneGvc.setFamilleArticles(famille);
      }
      // Sous famille de la ligneGvc
      if (record.isPresentField("A1SFA")) {
        ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille sousFamille =
            personnalisation.lireUneSousFamilleArticles(pFiltres.getEtablissementEnCours(), record.getField("A1SFA").toString().trim());
        ligneGvc.setSousFamille(sousFamille);
      }
      
      // Etat Série N de l'article
      if (record.isPresentField("A1NPU")) {
        char statut = record.getField("A1NPU").toString().charAt(0);
        ligneGvc.setStatutArticle(new StatutArticle(statut));
      }
      // Marque de l'article
      if (record.isPresentField("A1REF1")) {
        ligneGvc.setMarque(record.getField("A1REF1").toString().trim());
      }
      
      // Points Watt Magento de l'article
      if (record.isPresentField("A1ZP21")) {
        // Attention la saisie de cette zone dans Série N peut être ALPHA !!
        try {
          ligneGvc.setNbPointsWatt(Integer.valueOf(record.getField("A1ZP21").toString().trim()));
        }
        catch (Exception e) {
          // on init à Zero pour les calculs impactant les
          ligneGvc.setNbPointsWatt(0);
        }
      }
      
      // Workflow
      if (record.isPresentField("WRK_ETAT")) {
        WorkFlowGVC workFlow = new WorkFlowGVC();
        int code = EnumEtatWorkFlow.NOUVEAU.getCode();
        try {
          
          code = Integer.parseInt(record.getField("WRK_ETAT").toString().trim());
        }
        catch (NumberFormatException e) {
          e.printStackTrace();
        }
        workFlow.setEtatWF(new EtatWorkFlow(EnumEtatWorkFlow.valueOfByCode(code)));
        
        if (record.isPresentField("WRK_EXP")) {
          workFlow.setExpediteur((record.getField("WRK_EXP").toString().trim()));
        }
        if (record.isPresentField("WRK_DES")) {
          workFlow.setDestinataire((record.getField("WRK_DES").toString().trim()));
        }
        
        ligneGvc.setWorkFlowGVC(workFlow);
      }
      
      // Veille GSB
      if (record.isPresentField("VG_PRIX")) {
        BigDecimal prix = ((BigDecimal) record.getField("VG_PRIX")).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
        if (prix.compareTo(BigDecimal.ZERO) == 0) {
          ligneGvc.setVeillePrixGsb(null);
        }
        else {
          ligneGvc.setVeillePrixGsb(prix);
        }
      }
      if (record.isPresentField("VG_NOM")) {
        ligneGvc.setVeilleNomGsb(record.getField("VG_NOM").toString().trim());
      }
      if (record.isPresentField("VG_DATE")) {
        ligneGvc.setVeilleDateGsb(record.getField("VG_DATE").toString().trim());
      }
      
      // Mise en ligne (gestion web)
      if (record.isPresentField("A1IN15")) {
        ligneGvc.setGestionWeb(record.getField("A1IN15").toString());
      }
      
      // Récup des tarifs
      if (ligneGvc.getIdArticle() != null) {
        record = lireLesTarifsUnArticle(ligneGvc.getIdArticle());
      }
      if (record != null) {
        if (record.isPresentField("ATDAP")) {
          ligneGvc.setDateTarifs(record.getField("ATDAP").toString().trim());
        }
        if (record.isPresentField("ATP01")) {
          ligneGvc.setPrixPublicHT((BigDecimal) record.getField("ATP01"));
        }
        if (record.isPresentField("ATP02")) {
          ligneGvc.setPrixParticulierHT((BigDecimal) record.getField("ATP02"));
        }
        if (record.isPresentField("ATP03")) {
          ligneGvc.setPrixPromoHT((BigDecimal) record.getField("ATP03"));
        }
        if (record.isPresentField("ATP04")) {
          ligneGvc.setPrixArtisNelecHT((BigDecimal) record.getField("ATP04"));
        }
        if (record.isPresentField("ATP05")) {
          ligneGvc.setPrixArtisanHT((BigDecimal) record.getField("ATP05"));
        }
        if (record.isPresentField("ATP06")) {
          ligneGvc.setPrixRevendeurHT((BigDecimal) record.getField("ATP06"));
        }
        if (record.isPresentField("ATP07")) {
          ligneGvc.setPrixGrossisteHT((BigDecimal) record.getField("ATP07"));
        }
        if (record.isPresentField("ATP08")) {
          ligneGvc.setPrixGsbHT((BigDecimal) record.getField("ATP08"));
        }
        if (record.isPresentField("ATP09")) {
          ligneGvc.setPrixAdminiHT((BigDecimal) record.getField("ATP09"));
        }
        if (record.isPresentField("ATP10")) {
          ligneGvc.setPrixInterneHT((BigDecimal) record.getField("ATP10"));
        }
      }
      
      listeLignesGVC.add(ligneGvc);
    }
    
    return listeLignesGVC;
  }
  
  /**
   * Lire les 10 colonnes de tarifs d'un article Série N ainsi que leur date d'application
   */
  private GenericRecord lireLesTarifsUnArticle(IdArticle pIdArticle) {
    IdArticle.controlerId(pIdArticle, true);
    
    String request = "select atdap, atart, atp01, atp02, atp03, atp04, atp05, atp06, atp07, atp08, atp09, atp10 " + " from "
        + queryManager.getLibrary() + ".pgvmtarm a " + " where a.atetb='" + pIdArticle.getCodeEtablissement() + "' and a.atart = '"
        + pIdArticle.getCodeArticle() + "' and a.atdap " + " in ( select max(b.atdap) from " + queryManager.getLibrary() + ".pgvmtarm b "
        + " where atdap <= (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) "
        + " CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) FROM SYSIBM.SYSDUMMY1 ) and a.atetb=b.atetb and a.atart=b.atart )";
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if (listrcd == null || listrcd.size() < 0) {
      Trace.erreur("[GVC] lireLesTarifsUnArticle() SQL:" + queryManager.getMsgError());
      return null;
    }
    else if (listrcd.size() == 0) {
      return null;
    }
    
    return listrcd.get(0);
  }
  
  /**
   * Reteourner une chaine de conditions SQL à partir des filtres faits sur la recherche de lignes GVC
   */
  private String convertirFiltresSQL(FiltresGVC pFiltres) {
    String retour = null;
    // On met des conditions par défaut OBLIGATOIRES pour remonter des articles cohérents
    // Pas d'articles spéciaux, pas d'articles non gérés en stock, pas d'articles sans famille
    retour = " WHERE art.A1SPE = 0 AND art.A1STK = 0 AND art.A1FAM <> ' ' ";
    
    if (pFiltres.getEtablissementEnCours() != null) {
      retour += " AND art.A1ETB = '" + pFiltres.getEtablissementEnCours() + "' ";
    }
    
    if (pFiltres.getStatutSNArticle() != null && pFiltres.getStatutSNArticle().isUnstatut()) {
      retour += " AND art.A1NPU = '" + pFiltres.getStatutSNArticle().getCode() + "' ";
    }
    
    if (pFiltres.getEtatWeb() != null) {
      retour += " AND art.A1IN15 = '" + pFiltres.getEtatWeb() + "' ";
    }
    
    if (pFiltres.getGroupeArticle() != null && pFiltres.getGroupeArticle().isUnGroupe()) {
      retour += " AND substr(art.A1FAM, 1, 1) = '" + pFiltres.getGroupeArticle().getCode() + "' ";
    }
    
    if (pFiltres.getFamilleArticle() != null && pFiltres.getFamilleArticle().isUneFamille()) {
      retour += " AND art.A1FAM = '" + pFiltres.getFamilleArticle().getId().getCode() + "' ";
    }
    
    if (pFiltres.getSousFamilleArticle() != null && pFiltres.getSousFamilleArticle().isUneSousFamille()) {
      retour += " AND art.A1SFA = '" + pFiltres.getSousFamilleArticle().getId().getCode() + "' ";
    }
    
    if (pFiltres.getMarqueArticle() != null && !pFiltres.getMarqueArticle().equals(ListeMarques.TOUTES_MARQUES)) {
      retour += " AND art.A1REF1 = '" + pFiltres.getMarqueArticle() + "' ";
    }
    
    if (pFiltres.getFournisseurArticle() != null && pFiltres.getFournisseurArticle().isUnFournisseurValide()) {
      retour += " AND art.A1COF = '" + pFiltres.getFournisseurArticle().getId().getCollectif() + "' AND art.A1FRS = '"
          + pFiltres.getFournisseurArticle().getId().getNumero() + "' ";
    }
    
    if (pFiltres.getEtatWorkFlow() != null && pFiltres.getEtatWorkFlow().getCodeEtat() != EnumEtatWorkFlow.TOUS) {
      retour += " AND w.WRK_ETAT = '" + pFiltres.getEtatWorkFlow().getCodeEtat() + "' ";
    }
    
    if (pFiltres.getExpediteurArt() != null) {
      retour += " AND w.WRK_EXP = '" + pFiltres.getExpediteurArt() + "' ";
    }
    
    if (pFiltres.getDestinataireArt() != null) {
      retour += " AND w.WRK_DES = '" + pFiltres.getDestinataireArt() + "' ";
    }
    
    if (pFiltres.isMoinsCherAilleurs()) {
      String articlesMoinsCher = retournerArticlesMoinsCherPJ();
      if (articlesMoinsCher != null) {
        retour += " AND art.A1ART IN(" + articlesMoinsCher + ") ";
      }
    }
    
    if (pFiltres.getSaisieUtilisateur() != null) {
      retour += " AND (art.A1ART LIKE '" + pFiltres.getSaisieUtilisateur() + "%' OR art.A1LIB LIKE '%" + pFiltres.getSaisieUtilisateur()
          + "%') ";
    }
    
    return retour;
  }
  
  /**
   * Permet de retourner la liste des codes articles déclarés dans PriceJet
   * dont le tarif de la concurrence est inférieur à mes tarifs
   * Cette liste est formatée pour les arguments SQL de type IN ()
   */
  private String retournerArticlesMoinsCherPJ() {
    String retour = null;
    GestionPriceJet gestionPriceJet = new GestionPriceJet();
    ObjetPricejet objetPricejet = gestionPriceJet.retournerUneListeDinfos("PRIXPASMOINSCHER", null);
    
    if (objetPricejet != null && objetPricejet.getListeElements() != null) {
      for (int i = 0; i < objetPricejet.getListeElements().size(); i++) {
        if (i != 0) {
          retour += ",";
        }
        else {
          retour = "";
        }
        
        if (objetPricejet.getListeElements().get(i).getCode() != null) {
          retour += "'" + objetPricejet.getListeElements().get(i).getCode() + "'";
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Lire la condition d'achat active du fournisseur principal d'un article Série N
   * TODO conscient que l'objet ConditionAchat existe déjà avec appel RPG. Celui-ci est trop éloigné de l'existant sql pour en modifier
   * la structure (risque d'impact sur les démos HOLLOCO)
   */
  public ConditionAchatGVC chargerConditionAchatGVC(LigneGvc pLigne, Fournisseur pFournisseur) {
    if (pLigne == null) {
      return null;
    }
    IdArticle idArticle = IdArticle.controlerId(pLigne.getIdArticle(), true);
    IdFournisseur idFournisseur = Fournisseur.controlerId(pFournisseur, true);
    
    String requete = "SELECT CADAP,CAETB,CAART,CAPRA,CACOL,CAFRS,CAIN3,CAIN4,CAUNA, "
        + " CAREM1,CAREM2,CAREM3,CAREM4,CAREM5,CAREM6,CATRL,CAKAC,CAKSC,CAPRS " + " FROM " + queryManager.getLibrary() + ".PGVMCNAM "
        + " WHERE CAETB = '" + idArticle.getCodeEtablissement() + "' AND CAART = '" + idArticle.getCodeArticle() + "' " + " AND CACOL = '"
        + idFournisseur.getCollectif() + "' AND CAFRS = '" + idFournisseur.getNumero() + "' "
        + " AND CADAP <= (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) " + " CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) "
        + " CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) " + " FROM SYSIBM.SYSDUMMY1) ORDER BY CADAP DESC "
        + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ";
    
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    if (listrcd == null || listrcd.size() < 0) {
      Trace.erreur("[GVC] getInfosFournisseurPrincipal() SQL:" + queryManager.getMsgError());
      return null;
    }
    else if (listrcd.size() == 0) {
      return null;
    }
    
    ConditionAchatGVC conditionAchat = new ConditionAchatGVC(pLigne, pFournisseur);
    attribuerChampsAlaCondition(listrcd.get(0), conditionAchat);
    
    return conditionAchat;
  }
  
  /**
   * Lire la condition d'achat active du distributeur de référence d'un article Série N
   */
  public ConditionAchatGVC chargerConditionAchatGVCdistributeurReference(LigneGvc pLigne) {
    if (pLigne == null) {
      return null;
    }
    IdArticle idArticle = IdArticle.controlerId(pLigne.getIdArticle(), true);
    
    String requete = " SELECT CADAP,CAETB,CAART,CAPRA,CACOL,CAFRS,CAIN3,CAIN4,CAUNA, "
        + " CAREM1,CAREM2,CAREM3,CAREM4,CAREM5,CAREM6,CATRL,CAKAC,CAKSC,CAPRS,FRNOM " + " FROM " + queryManager.getLibrary()
        + ".PGVMCNAM " + " LEFT JOIN " + queryManager.getLibrary() + ".PGVMFRSM " + " ON CACOL = FRCOL AND CAFRS = FRFRS "
        + " WHERE CAETB = '" + idArticle.getCodeEtablissement() + "' AND CAART = '" + idArticle.getCodeArticle() + "' "
        + " AND CAIN3 = 'R' " + " AND CADAP <= (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) "
        + " CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) " + " CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) "
        + " FROM SYSIBM.SYSDUMMY1) ORDER BY CADAP DESC " + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ";
    
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    
    if (listrcd == null || listrcd.size() < 0) {
      Trace.erreur("[GVC] chargerInfosDistributeurReference() SQL:" + queryManager.getMsgError());
      return null;
    }
    else if (listrcd.size() == 0) {
      return null;
    }
    
    ConditionAchatGVC conditionAchat = new ConditionAchatGVC(pLigne, null);
    GenericRecord record = listrcd.get(0);
    if (conditionAchat.getFournisseur() == null) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("CAETB"));
      IdFournisseur idFournisseur =
          IdFournisseur.getInstance(idEtablissement, record.getIntValue("CACOL", 0), record.getIntValue("CAFRS", 0));
      
      Fournisseur fournisseur = new Fournisseur(idFournisseur);
      if (record.isPresentField("FRNOM")) {
        fournisseur.setNomFournisseur(record.getStringValue("FRNOM", "", true));
      }
      
      conditionAchat.setFournisseur(fournisseur);
    }
    attribuerChampsAlaCondition(record, conditionAchat);
    
    return conditionAchat;
  }
  
  /**
   * On attribue les champs de l'enregistrement SQL à la condition d'achat
   */
  private void attribuerChampsAlaCondition(GenericRecord record, ConditionAchatGVC conditionAchat) {
    if (record == null || conditionAchat == null) {
      return;
    }
    
    if (record.isPresentField("CAUNA")) {
      conditionAchat.setUniteAchat(record.getStringValue("CAUNA", "", true));
    }
    if (record.isPresentField("CAPRA")) {
      conditionAchat.setPrixAchatBrut(record.getDecimal("CAPRA"));
    }
    if (record.isPresentField("CAREM1")) {
      conditionAchat.setRemise1(record.getDecimal("CAREM1"));
    }
    if (record.isPresentField("CAREM2")) {
      conditionAchat.setRemise2(record.getDecimal("CAREM2"));
    }
    if (record.isPresentField("CAREM3")) {
      conditionAchat.setRemise3(record.getDecimal("CAREM3"));
    }
    if (record.isPresentField("CAREM4")) {
      conditionAchat.setRemise4(record.getDecimal("CAREM4"));
    }
    if (record.isPresentField("CAREM5")) {
      conditionAchat.setRemise5(record.getDecimal("CAREM5"));
    }
    if (record.isPresentField("CAREM6")) {
      conditionAchat.setRemise6(record.getDecimal("CAREM6"));
    }
    if (record.isPresentField("CAIN4")) {
      conditionAchat.setCAIN4(record.getStringValue("CAIN4", "", true));
    }
    if (record.isPresentField("CATRL")) {
      try {
        if (record.getField("CATRL") != null && !record.getField("CATRL").toString().trim().equals("")) {
          conditionAchat.setCATRL(Integer.parseInt(record.getField("CATRL").toString().trim()));
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    if (record.isPresentField("CAPRS")) {
      conditionAchat.setCAPRS(record.getDecimal("CAPRS"));
    }
    if (record.isPresentField("CAKAC")) {
      conditionAchat.setCAKAC(record.getDecimal("CAKAC"));
    }
    if (record.isPresentField("CAKSC")) {
      conditionAchat.setCAKSC(record.getDecimal("CAKSC"));
    }
    
    conditionAchat.majPrixAchatRemise();
  }
  
  /**
   * Récupérer les historiques de Prix d'achat d'un article
   * sous la forme d'un tableau de Bigdecimal de longueur 2
   * 0 -> PUMP prix unitaire moyen pondéré, 1 -> H1PRV prix de revient standard
   */
  public BigDecimal[] chargerPrixAchatArticle(IdArticle pIdArticle) {
    IdArticle.controlerId(pIdArticle, true);
    
    BigDecimal[] tarifsAchat = null;
    
    String requete = "" + " SELECT H1DAT, H1ART, H1PMP, H1PRV FROM " + queryManager.getLibrary() + ".PGVMHISM " + " WHERE H1ETB ='"
        + pIdArticle.getCodeEtablissement() + "' and H1ART ='" + pIdArticle.getCodeArticle() + "' AND H1ORD < 1000 "
        + " AND H1DAT <= (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) " + " CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) "
        + " CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) " + " FROM SYSIBM.SYSDUMMY1) ORDER BY H1DAT DESC "
        + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ";
    
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    if (listrcd == null || listrcd.size() < 0) {
      Trace.erreur("[GVC] lireLesPrixAchatArticle() SQL:" + queryManager.getMsgError());
      return null;
    }
    else if (listrcd.size() == 0) {
      return null;
    }
    
    tarifsAchat = new BigDecimal[2];
    if (listrcd.get(0).isPresentField("H1PMP")) {
      tarifsAchat[0] = listrcd.get(0).getDecimal("H1PMP");
    }
    if (listrcd.get(0).isPresentField("H1PRV")) {
      tarifsAchat[1] = listrcd.get(0).getDecimal("H1PRV");
    }
    
    return tarifsAchat;
  }
  
  /**
   * Insérer ou mettre à jour les 10 colonnes de tarifs de vente d'un article Série N à partir d'une ligne GVC
   * Fichier des tarifs PGVMTARM
   * Si un enregistrment existe à la date du jour on met à jour sinon on insert
   */
  public boolean sauverTarifsArticle(LigneGvc pLigne) {
    if (pLigne == null) {
      return false;
    }
    IdArticle idArticle = IdArticle.controlerId(pLigne.getIdArticle(), true);
    
    int retour = -1;
    String dateJour = "1" + ConvertDate.formateObjetDateEnString(null, "yyMMdd");
    
    BigDecimal tarif1 = pLigne.getPrixPublicHT();
    BigDecimal tarif2 = pLigne.getPrixParticulierHT();
    BigDecimal tarif3 = pLigne.getPrixPromoHT();
    BigDecimal tarif4 = pLigne.getPrixArtisNelecHT();
    BigDecimal tarif5 = pLigne.getPrixArtisanHT();
    BigDecimal tarif6 = pLigne.getPrixRevendeurHT();
    BigDecimal tarif7 = pLigne.getPrixGrossisteHT();
    BigDecimal tarif8 = pLigne.getPrixGsbHT();
    BigDecimal tarif9 = pLigne.getPrixAdminiHT();
    BigDecimal tarif10 = pLigne.getPrixInterneHT();
    
    if (tarif1 == null || tarif2 == null || tarif3 == null || tarif4 == null || tarif5 == null || tarif6 == null || tarif7 == null
        || tarif8 == null || tarif9 == null || tarif10 == null) {
      return false;
    }
    // 1 On checke l'existence de tarif sur la date d'application du jour
    if (recuperationUnTarif(pLigne.getIdArticle(), null) != null) {
      // UPDATE
      retour =
          queryManager.requete("" + " UPDATE " + queryManager.getLibrary() + ".PGVMTARM SET " + " ATP01 = '" + formaterInsertPrix(tarif1)
              + "'," + " ATP02 = '" + formaterInsertPrix(tarif2) + "'," + " ATP03 = '" + formaterInsertPrix(tarif3) + "'," + " ATP04 = '"
              + formaterInsertPrix(tarif4) + "'," + " ATP05 = '" + formaterInsertPrix(tarif5) + "'," + " ATP06 = '"
              + formaterInsertPrix(tarif6) + "'," + " ATP07 = '" + formaterInsertPrix(tarif7) + "'," + " ATP08 = '"
              + formaterInsertPrix(tarif8) + "'," + " ATP09 = '" + formaterInsertPrix(tarif9) + "'," + " ATP10 = '"
              + formaterInsertPrix(tarif10) + "' " + " WHERE ATETB ='" + idArticle.getCodeEtablissement() + "' " + " AND ATART = '"
              + idArticle.getCodeArticle() + "' AND ATDAP = '" + dateJour + "' ");
      return (retour == 1);
    }
    else {
      // TODO CONTROLES METIER
      // INSERT
      // TODO contrôle de valeurs et de dates
      retour = queryManager.requete("" + "INSERT INTO " + queryManager.getLibrary() + ".PGVMTARM " + " (" + " ATCRE," + " ATETB,"
          + " ATART," + " ATDAP," + " ATP01,ATP02,ATP03,ATP04,ATP05,ATP06,ATP07,ATP08,ATP09,ATP10) " + " VALUES " + " ( " + " '"
          + dateJour + "'," + " '" + idArticle.getCodeEtablissement() + "'," + " '" + idArticle.getCodeArticle() + "'," + " '" + dateJour
          + "'," + " '" + formaterInsertPrix(tarif1) + "'," + " '" + formaterInsertPrix(tarif2) + "'," + " '" + formaterInsertPrix(tarif3)
          + "'," + " '" + formaterInsertPrix(tarif4) + "'," + " '" + formaterInsertPrix(tarif5) + "'," + " '" + formaterInsertPrix(tarif6)
          + "'," + " '" + formaterInsertPrix(tarif7) + "'," + " '" + formaterInsertPrix(tarif8) + "'," + " '" + formaterInsertPrix(tarif9)
          + "'," + " '" + formaterInsertPrix(tarif10) + "'" + " )");
      
      if (retour < 0) {
        Trace.erreur("[GVC] majTarifs() INSERT SQL:" + queryManager.getMsgError());
      }
      
      return (retour == 1);
    }
  }
  
  /**
   * Récupère les 10 tarifs actifs pour un article à une date d'application donnée
   */
  private GenericRecord recuperationUnTarif(IdArticle pIdArticle, String pDate) {
    IdArticle.controlerId(pIdArticle, true);
    
    // On applique la date du jour si la dte transmise est Nulle
    if (pDate == null) {
      pDate = "1" + ConvertDate.formateObjetDateEnString(null, "yyMMdd");
    }
    
    String request = "SELECT atdap, atart, atp01, atp02, atp03, atp04, atp05, atp06, atp07, atp08, atp09, atp10 " + " FROM "
        + queryManager.getLibrary() + ".pgvmtarm a " + " WHERE a.atetb='" + pIdArticle.getCodeEtablissement() + "' and a.atart = '"
        + pIdArticle.getCodeArticle() + "' and a.atdap = '" + pDate + "' ";
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if (listrcd == null || listrcd.size() < 0) {
      Trace.erreur("[GVC] recuperationUnTarif() SQL:" + queryManager.getMsgError());
      return null;
    }
    else if (listrcd.size() == 0) {
      return null;
    }
    
    return listrcd.get(0);
  }
  
  /**
   * Formater l'insertion d'un tarif dans la base
   * En clair jamais de valeur null et un typage en String
   */
  private String formaterInsertPrix(BigDecimal prix) {
    if (prix == null) {
      return BigDecimal.ZERO.toString();
    }
    
    return prix.toString();
  }
  
  /**
   * Insérer ou mettre à jour le workflow d'une ligne Gvc :
   * état du workflow, expéditeur et destinataire
   */
  public boolean sauverWorkFlow(LigneGvc pLigne) {
    if (pLigne == null) {
      return false;
    }
    IdArticle idArticle = IdArticle.controlerId(pLigne.getIdArticle(), true);
    
    int retour = -1;
    
    String expe = "";
    String desti = "";
    String etatWF = EnumEtatWorkFlow.NOUVEAU.toString();
    // 1 on check si cet article est existant dans le workflow
    if (existeEnWorkFlowGVC(idArticle)) {
      if (pLigne.getWorkFlowGVC().getExpediteur() != null) {
        expe = pLigne.getWorkFlowGVC().getExpediteur();
      }
      if (pLigne.getWorkFlowGVC().getDestinataire() != null) {
        desti = pLigne.getWorkFlowGVC().getDestinataire();
      }
      if (pLigne.getWorkFlowGVC().getEtatWF() != null) {
        etatWF = "" + pLigne.getWorkFlowGVC().getEtatWF().getCodeEtat();
      }
      // UPDATE
      retour = queryManager.requete("" + " UPDATE " + queryManager.getLibrary() + ".PSEMGVCWM SET " + " WRK_EXP = '" + expe + "',"
          + " WRK_DES = '" + desti + "'," + " WRK_ETAT = '" + etatWF + "'," + " WRK_DATE = CURDATE() " + " WHERE WRK_ETB = '"
          + idArticle.getCodeEtablissement() + "' " + " AND WRK_ART = '" + idArticle.getCodeArticle() + "' ");
    }
    // INSERT
    else {
      retour = queryManager.requete("" + "INSERT INTO " + queryManager.getLibrary() + ".PSEMGVCWM " + " (" + " WRK_ETB," + " WRK_ART,"
          + " WRK_EXP," + " WRK_DES," + " WRK_ETAT," + " WRK_DATE )" + " VALUES " + " ( '" + idArticle.getCodeEtablissement() + "',"
          + " '" + idArticle.getCodeArticle() + "'," + " '" + expe + "'," + " '" + desti + "'," + " '" + etatWF + "'," + " CURDATE()) ");
    }
    
    if (retour < 0) {
      Trace.erreur("[GVC] majWorkFlow() SQL:" + queryManager.getMsgError());
    }
    
    return (retour == 1);
  }
  
  /**
   * On checke si un workflow existe déjà pour l'article passé en paramètre
   */
  private boolean existeEnWorkFlowGVC(IdArticle pIdArticle) {
    ArrayList<GenericRecord> liste = queryManager.select("SELECT WRK_ID FROM " + queryManager.getLibrary()
        + ".PSEMGVCWM WHERE WRK_ETB = '" + pIdArticle.getCodeEtablissement() + "' AND WRK_ART = '" + pIdArticle.getCodeArticle() + "' ");
    
    if (liste == null || liste.size() < 0) {
      Trace.erreur("[GVC] existeEnWorkFlowGVC() SQL:" + queryManager.getMsgError());
    }
    
    return (liste != null && liste.size() == 1);
  }
  
  /**
   * Met à jour la valeur de mise en ligne Web A1IN15 dans la table Article PGVMARTM de Série N
   */
  public boolean sauverMiseEnLigneArticle(LigneGvc pLigne) {
    if (pLigne == null) {
      return false;
    }
    IdArticle idArticle = IdArticle.controlerId(pLigne.getIdArticle(), true);
    
    int retour = queryManager.requete("UPDATE " + queryManager.getLibrary() + ".PGVMARTM SET A1IN15 = '" + pLigne.getGestionWeb()
        + "' WHERE A1ETB = '" + idArticle.getCodeEtablissement() + "' AND A1ART = '" + idArticle.getCodeArticle() + "' ");
    
    if (retour < 0) {
      Trace.erreur("[GVC] majMiseEnLigne() SQL:" + queryManager.getMsgError());
    }
    
    return (retour == 1);
  }
  
  /**
   * Met à jour la valeur des points Watt de l'article
   */
  public boolean sauverPointsWattArticle(LigneGvc pLigne) {
    if (pLigne == null) {
      return false;
    }
    IdArticle idArticle = IdArticle.controlerId(pLigne.getIdArticle(), true);
    int retour =
        queryManager.requete("UPDATE " + queryManager.getLibrary() + ".PGVMARTM SET A1ZP21 = '" + pLigne.getNbPointsWatt().toString()
            + "' " + " WHERE A1ETB = '" + idArticle.getCodeEtablissement() + "' AND A1ART = '" + idArticle.getCodeArticle() + "' ");
    
    if (retour < 0) {
      Trace.erreur("[GVC] ecrirePointsWattArticle() SQL:" + queryManager.getMsgError());
    }
    
    return (retour == 1);
  }
  
  /**
   * Met à jour les informations GSB propres à la veille des GSB (grandes surfaces de bricolage) d'une ligne GVC
   * Table PSEMVGSBM
   **/
  public boolean sauverVeilleGSB(LigneGvc pLigne) {
    if (pLigne == null) {
      return false;
    }
    IdArticle idArticle = IdArticle.controlerId(pLigne.getIdArticle(), true);
    
    int retour = -1;
    
    BigDecimal montant = pLigne.getVeillePrixGsb();
    String nomGSB = pLigne.getVeilleNomGsb();
    
    // On supprime si les données de la liste sont vides
    if ((montant == null || montant.compareTo(BigDecimal.ZERO) == 0) && (nomGSB == null || nomGSB.trim().equals(""))) {
      // dans ce cas il faudra faire un DELETE de l'article dans cette table
      retour = queryManager.requete("" + "DELETE FROM " + queryManager.getLibrary() + ".PSEMVGSBM " + " WHERE VG_A1ETB = '"
          + idArticle.getCodeEtablissement() + "' AND VG_A1ART = '" + idArticle.getCodeArticle() + "' ");
      
      if (retour < 0) {
        Trace.erreur("[GVC] majVeilleGSB() DELETE SQL:" + queryManager.getMsgError());
      }
    }
    
    // Update de veille
    if (existeUneVeilleGSB(pLigne.getIdArticle())) {
      retour = queryManager.requete("" + " UPDATE " + queryManager.getLibrary() + ".PSEMVGSBM SET " + " VG_PRIX = '"
          + formaterInsertPrix(montant) + "'," + " VG_NOM = '" + nomGSB + "'," + " VG_DATE = CURRENT TIMESTAMP " + " WHERE VG_A1ETB ='"
          + idArticle.getCodeEtablissement() + "' AND VG_A1ART = '" + idArticle.getCodeArticle() + "' ");
      
      if (retour < 0) {
        Trace.erreur("[GVC] majVeilleGSB() UPDATE SQL:" + queryManager.getMsgError());
      }
    }
    // Insert de veille
    else {
      retour = queryManager.requete("" + "INSERT INTO " + queryManager.getLibrary() + ".PSEMVGSBM " + "(" + " VG_A1ETB," + " VG_A1ART,"
          + " VG_PRIX," + " VG_NOM" + ")" + " VALUES " + "( " + " '" + idArticle.getCodeEtablissement() + "'," + " '"
          + idArticle.getCodeArticle() + "'," + " '" + formaterInsertPrix(montant) + "'," + " '" + nomGSB + "'" + ")");
      
      if (retour < 0) {
        Trace.erreur("[GVC] majVeilleGSB() UPDATE SQL:" + queryManager.getMsgError());
      }
    }
    
    return (retour == 1);
  }
  
  /**
   * On vérifie s'il n'existe pas déjà une ligne de veille GSB pour cet article
   */
  private boolean existeUneVeilleGSB(IdArticle pIdArticle) {
    ArrayList<GenericRecord> liste =
        queryManager.select("SELECT VG_A1ART FROM " + queryManager.getLibrary() + ".PSEMVGSBM WHERE VG_A1ETB = '"
            + pIdArticle.getCodeEtablissement() + "' AND VG_A1ART = '" + pIdArticle.getCodeArticle() + "' ");
    
    if (liste == null || liste.size() < 0) {
      Trace.erreur("[GVC] existeUneVeilleGSB() SQL:" + queryManager.getMsgError());
    }
    
    return (liste != null && liste.size() == 1);
  }
  
  /**
   * Crée un flux Magento FLX001 sur la base d'une ligne GVC
   */
  public void sauverFluxMagento(LigneGvc pLigne) {
    if (pLigne == null) {
      return;
      // TODO on va d'abord vérifier si les flux correspondants sont paramétrés
    }
    
    ManagerFluxMagento gestionDeFlux = new ManagerFluxMagento(queryManager.getLibrary());
    IdEtablissement idEtablissement = pLigne.getIdArticle().getIdEtablissement();
    String version = gestionDeFlux.retournerVersionFlux(idEtablissement);
    FluxMagento flux = new FluxMagento(queryManager, version);
    
    flux.setFLETB(pLigne.getIdArticle().getCodeEtablissement());
    flux.setFLIDF(FluxMagento.IDF_FLX001_ARTICLE);
    flux.setFLCOD(pLigne.getIdArticle().getCodeArticle());
    flux.setFLSNS(FluxMagento.SNS_VERS_MAGENTO);
    flux.setFLSTT(EnumStatutFlux.A_TRAITER);
    
    if (!gestionDeFlux.addRecordInterne(flux)) {
      Trace.erreur("[GVC] gestionDeFlux.addRecordInterne(): false pour le flux " + flux.getFLID());
    }
  }
  
  /**
   * On retoure le mail associé à un profil AS400
   */
  public String chargerAdresseMailProfilAs400(String pProfil) {
    String retour = null;
    if (pProfil == null) {
      return retour;
    }
    
    ArrayList<GenericRecord> liste =
        queryManager.select("SELECT RENET FROM " + queryManager.getLibrary() + ".PSEMRTEM WHERE REPRF = '" + pProfil + "' ");
    
    if (liste == null || liste.size() < 0) {
      Trace.erreur("[GVC] lireMailProfilAs400() SQL:" + queryManager.getMsgError());
    }
    
    if (liste != null && liste.size() > 0 && liste.get(0).isPresentField("RENET")) {
      retour = liste.get(0).getField("RENET").toString().trim();
    }
    
    return retour;
  }
  
}
