/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx012.v2;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;

import ri.serien.libas400.dao.sql.gescom.flux.FluxAutomatique;
import ri.serien.libas400.dao.sql.gescom.flux.ParametreFTP;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.ftp.FTPManager;

public class GestionFLX012 {
  private ParametresFluxBibli parametres = null;
  private QueryManager queryManager = null;
  private SystemeManager systeme = null;
  private ParametreFTP parametreFTP = null;
  private FTPManager serveurFTP = null;
  private boolean parametrageIsOk = true;
  private final String EXTENSION_FICHIER_RECUP = ".csv";
  private final String EXTENSION_FICHIER_TRAIT = ".trt";
  private final String EXTENSION_FICHIER_ERREUR = ".err";
  private String msgErreur = "";
  InjecteurFacYooz injecteurFacYooz = null;
  
  public GestionFLX012(ParametresFluxBibli pParms, QueryManager pQuery, SystemeManager pSystem) {
    parametres = pParms;
    queryManager = pQuery;
    systeme = pSystem;
  }
  
  /**
   * Traitement global d'une facture Yooz disponible sur le serveur FTP
   */
  public boolean traiterUneFactureYooz(FluxMagento pFlux) {
    boolean retour = false;
    if (injecterFichierCompta(pFlux)) {
      // Si réponse OK supprimer le fichier sur le serveur FTP
      retour = supprimerFichierFTP(pFlux);
    }
    else {
      majMsgErreur("TraiterUneFactureYooz() PB injecteur : " + injecteurFacYooz.retournerLibelleCodeErreur());
      // Si probleme renommer en .err
      renommerFichierFTPerreur(pFlux);
    }
    return retour;
  }
  
  /**
   * On appelle le programme qui injecte la facture Yooz en comptabilité Série
   * N
   */
  private boolean injecterFichierCompta(FluxMagento pFlux) {
    
    if (pFlux == null || pFlux.getFLJSN() == null || parametres.getLettreEnvironnement() == null || systeme == null) {
      majMsgErreur("InjecterFichierCompta() paramètres initiaux corrompus ");
      return false;
    }
    
    if (parametreFTP == null) {
      parametreFTP = ManagerFluxMagento.recupererParametreFTP(queryManager.getLibrary(), pFlux.getFLJSN().trim());
      if (parametreFTP == null) {
        parametrageIsOk = false;
        return false;
      }
    }
    
    String cheminFichier = null;
    cheminFichier = parametreFTP.getRepertoireLocal();
    if (!cheminFichier.endsWith(File.separator)) {
      cheminFichier += File.separator;
    }
    cheminFichier += pFlux.getFLCOD().trim();
    
    if (injecteurFacYooz == null) {
      injecteurFacYooz = new InjecteurFacYooz(systeme, parametres.getLettreEnvironnement(), parametres.getLibrairie());
      injecteurFacYooz.init();
    }
    
    return injecteurFacYooz.execute(cheminFichier, pFlux.getFLETB());
  }
  
  /**
   * On renomme le fichier sur le serveur FTP dans le cas où l'injection s'est
   * bien déroulée
   */
  private boolean renommerFichierFTPerreur(FluxMagento pFlux) {
    if (pFlux == null || pFlux.getFLCOD() == null || parametreFTP == null) {
      majMsgErreur("renommerFichierFTPerreur() les paramètres initiaux sont corrompus");
      return false;
    }
    
    // Etape 1 récupérer les commandes sur le serveur FTP @GP
    if (serveurFTP == null) {
      serveurFTP = new FTPManager();
    }
    
    String nomFichierArenommer = renommerExtensionFichier(pFlux.getFLCOD(), EXTENSION_FICHIER_RECUP, EXTENSION_FICHIER_TRAIT);
    String nomFichierRenomme = renommerExtensionFichier(pFlux.getFLCOD(), EXTENSION_FICHIER_RECUP, EXTENSION_FICHIER_ERREUR);
    
    if (serveurFTP.connexion(parametreFTP.getAdresse(), parametreFTP.getUser(), parametreFTP.getMotPasse())) {
      try {
        if (serveurFTP.getConnexion().changeWorkingDirectory(parametreFTP.getRepertoireDistant())) {
          serveurFTP.getConnexion().enterLocalPassiveMode();
          serveurFTP.getConnexion().setFileType(FTP.BINARY_FILE_TYPE);
          
          FTPFile[] tabFichiersBruts = serveurFTP.getConnexion().listFiles();
          
          String nomFichier = null;
          for (FTPFile fichierFTP : tabFichiersBruts) {
            nomFichier = fichierFTP.getName();
            if (fichierFTP.isFile()) {
              if (nomFichier.equals(nomFichierArenommer)) {
                return serveurFTP.getConnexion().rename(nomFichierArenommer, nomFichierRenomme);
              }
            }
          }
          serveurFTP.disconnect();
        }
      }
      catch (IOException e) {
        majMsgErreur("[GestionFLX012] supprimerFichierFTP(): PD changeWorkingDirectory " + parametreFTP.getRepertoireDistant() + " -> "
            + e.getMessage());
        return false;
      }
      
    }
    else {
      majMsgErreur("[GestionFLX012] supprimerFichierFTP() connexion FTP en échec pour " + parametreFTP.getAdresse() + " / "
          + parametreFTP.getUser());
      return false;
    }
    return false;
  }
  
  /**
   * On supprime le fichier sur le serveur FTP dans le cas où l'injection
   * s'est bien déroulée
   */
  private boolean supprimerFichierFTP(FluxMagento pFlux) {
    if (pFlux == null || pFlux.getFLCOD() == null || parametreFTP == null) {
      majMsgErreur("supprimerFichierFTP() les paramètres initiaux sont corrompus ");
      return false;
    }
    
    // Etape 1 récupérer les commandes sur le serveur FTP @GP
    if (serveurFTP == null) {
      serveurFTP = new FTPManager();
    }
    
    String nomFichierASuppr = renommerExtensionFichier(pFlux.getFLCOD(), EXTENSION_FICHIER_RECUP, EXTENSION_FICHIER_TRAIT);
    
    if (serveurFTP.connexion(parametreFTP.getAdresse(), parametreFTP.getUser(), parametreFTP.getMotPasse())) {
      try {
        if (serveurFTP.getConnexion().changeWorkingDirectory(parametreFTP.getRepertoireDistant())) {
          serveurFTP.getConnexion().enterLocalPassiveMode();
          serveurFTP.getConnexion().setFileType(FTP.BINARY_FILE_TYPE);
          
          FTPFile[] tabFichiersBruts = serveurFTP.getConnexion().listFiles();
          
          String nomFichier = null;
          for (FTPFile fichierFTP : tabFichiersBruts) {
            nomFichier = fichierFTP.getName();
            if (fichierFTP.isFile()) {
              if (nomFichier.equals(nomFichierASuppr)) {
                return serveurFTP.getConnexion().deleteFile(nomFichier);
              }
            }
          }
          serveurFTP.disconnect();
        }
      }
      catch (IOException e) {
        majMsgErreur("[GestionFLX012] supprimerFichierFTP(): PD changeWorkingDirectory " + parametreFTP.getRepertoireDistant() + " -> "
            + e.getMessage());
        return false;
      }
      
    }
    else {
      majMsgErreur("[GestionFLX012] supprimerFichierFTP() connexion FTP en échec pour " + parametreFTP.getAdresse() + " / "
          + parametreFTP.getUser());
      return false;
    }
    return false;
  }
  
  /**
   * Permet de retourner les factures Yooz disponibles sur un serveur FTP sous
   * forme de flux à traiter
   */
  public ArrayList<FluxMagento> recupererFacturesYooz(FluxAutomatique pFlux) {
    if (parametrageIsOk == false) {
      majMsgErreur("[GestionFLX012] recupererFacturesYooz() le Le paramétrage FTP est erroné");
      return null;
    }
    
    if (pFlux == null) {
      majMsgErreur("[GestionFLX012] recupererFacturesYooz() le flux transmis est NULL");
      return null;
    }
    if (pFlux.getCodeFTP() == null || queryManager.getLibrary() == null) {
      majMsgErreur("[GestionFLX012] recupererFacturesYooz() code FTP " + pFlux.getCodeFTP() + " ou librairie " + queryManager.getLibrary()
          + " corrompus");
      return null;
    }
    
    // Récupérer les paramètres FTP pour savoir quoi et où récupérer les
    // infos
    if (parametreFTP == null) {
      parametreFTP = ManagerFluxMagento.recupererParametreFTP(queryManager.getLibrary(), pFlux.getCodeFTP());
      if (parametreFTP == null) {
        parametrageIsOk = false;
        return null;
      }
    }
    
    // Créer un flux par fichier à récupérer
    ArrayList<FluxMagento> listeFlux = null;
    // Aller scanner les fichiers à récupérer sur le serveur FTP
    ArrayList<FTPFile> listeFichiers = recupererListeFichiersDistants(null);
    if (listeFichiers == null) {
      return null;
    }
    else {
      listeFlux = new ArrayList<FluxMagento>();
    }
    
    for (FTPFile fichiersFTP : listeFichiers) {
      FluxMagento flux = pFlux.getFlux().clone();
      // on met le nom du fichier à récupérer
      flux.setFLJSN(pFlux.getCodeFTP());
      flux.setFLCOD(fichiersFTP.getName().trim());
      listeFlux.add(flux);
    }
    
    if (listeFichiers.size() == 0) {
      Trace.info("FLX012, aucun fichier à récupérer pour " + parametres.getLibrairie());
    }
    
    return listeFlux;
  }
  
  /**
   * Récupérer la liste des fichiers présents sur le dossier distants et
   * correspondant aux attentes du paramétrage FTP
   */
  private ArrayList<FTPFile> recupererListeFichiersDistants(String pFichierArecuperer) {
    if (parametreFTP == null) {
      parametrageIsOk = false;
      majMsgErreur("[GestionFLX012] recupererListeFichiersDistants() FTP parametrage NULL");
      return null;
    }
    if (parametreFTP.getAdresse() == null || parametreFTP.getAdresse().trim().equals("")) {
      parametrageIsOk = false;
      majMsgErreur("[GestionFLX012] recupererListeFichiersDistants() FTP adresse corrompue " + parametreFTP.getAdresse());
      return null;
    }
    if (parametreFTP.getUser() == null || parametreFTP.getUser().trim().equals("")) {
      parametrageIsOk = false;
      majMsgErreur("[GestionFLX012] recupererListeFichiersDistants() FTP utilisateur corrompu " + parametreFTP.getUser());
      return null;
    }
    if (parametreFTP.getMotPasse() == null || parametreFTP.getMotPasse().trim().equals("")) {
      parametrageIsOk = false;
      majMsgErreur("[GestionFLX012] recupererListeFichiersDistants() FTP mot de passe corrompu " + parametreFTP.getMotPasse());
      return null;
    }
    
    ArrayList<FTPFile> listeFichiers = null;
    
    // Etape 1 récupérer les commandes sur le serveur FTP @GP
    if (serveurFTP == null) {
      serveurFTP = new FTPManager();
    }
    
    if (serveurFTP.connexion(parametreFTP.getAdresse(), parametreFTP.getUser(), parametreFTP.getMotPasse())) {
      try {
        if (serveurFTP.getConnexion().changeWorkingDirectory(parametreFTP.getRepertoireDistant())) {
          serveurFTP.getConnexion().enterLocalPassiveMode();
          serveurFTP.getConnexion().setFileType(FTP.BINARY_FILE_TYPE);
          
          FTPFile[] tabFichiersBruts = serveurFTP.getConnexion().listFiles();
          listeFichiers = new ArrayList<FTPFile>();
          
          if (tabFichiersBruts != null) {
            String nomFichier = null;
            for (FTPFile fichierFTP : tabFichiersBruts) {
              nomFichier = fichierFTP.getName();
              if (fichierFTP.isFile()) {
                if (nomFichier.startsWith(parametreFTP.getNomFichier()) && nomFichier.endsWith(EXTENSION_FICHIER_RECUP)) {
                  File downloadFile1 = new File(parametreFTP.getRepertoireLocal() + File.separator + nomFichier);
                  
                  OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
                  if (serveurFTP.getConnexion().retrieveFile(nomFichier, outputStream1)) {
                    listeFichiers.add(fichierFTP);
                    serveurFTP.getConnexion().rename(nomFichier,
                        renommerExtensionFichier(nomFichier, EXTENSION_FICHIER_RECUP, EXTENSION_FICHIER_TRAIT));
                  }
                  else {
                    majMsgErreur("[GestionFLX012] Tentative de récupération du fichier FTP " + nomFichier + " en échec");
                  }
                  outputStream1.close();
                }
              }
            }
            
          }
          else {
            majMsgErreur("[GestionFLX012] recupererListeFichiersDistants(): tabFichiersBruts");
          }
          serveurFTP.disconnect();
        }
      }
      catch (IOException e) {
        majMsgErreur("[GestionFLX012] recupererListeFichiersDistants(): PD changeWorkingDirectory " + parametreFTP.getRepertoireDistant()
            + " -> " + e.getMessage());
        return null;
      }
      
    }
    else {
      parametrageIsOk = false;
      majMsgErreur("[GestionFLX012] recupererListeFichiersDistants() connexion FTP en échec pour " + parametreFTP.getAdresse() + " / "
          + parametreFTP.getUser());
      return listeFichiers;
    }
    
    return listeFichiers;
  }
  
  /**
   * Renommer l'extension d'un fichier en une autre
   */
  private String renommerExtensionFichier(String pFichier, String pOld, String pNew) {
    if (pFichier == null || pOld == null || pNew == null) {
      majMsgErreur("renommerExtensionFichier() Un des paramètres est Null");
      return pFichier;
    }
    if (!pFichier.endsWith(pOld)) {
      majMsgErreur("renommerExtensionFichier() l'extension du fichier d'origine est incorrecte " + pOld);
      return pFichier;
    }
    
    String fichierModifie = pFichier;
    fichierModifie = fichierModifie.replaceFirst(pOld, pNew);
    return fichierModifie;
  }
  
  private void majMsgErreur(String pMess) {
    Trace.erreur(pMess);
    msgErreur += pMess;
  }
  
  public String getMsgErreur() {
    return msgErreur;
  }
}
