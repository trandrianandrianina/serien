/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.recherche;

import java.math.BigDecimal;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeExtractionDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatCommentaire;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.prix.ParametrePrixAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.document.ListeZonePersonnalisee;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

public class RpgChargerLignesDocument extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGAM0007";
  
  /**
   * Chargement des lignes d'un document ou d'une ligne spécifique.
   * Cette méthode rend transparent le mode de chargement par bloc (ou par page) du programme RPG.
   */
  public ListeLigneAchat chargerListeLigneAchat(SystemeManager pSysteme, CritereLigneAchat pCriteres) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères sont invalides.");
    }
    
    // Dans ce cas, une seule ligne est récupérée donc un seul appel suffira
    if (pCriteres.getIdLigneAchat() != null) {
      return chargerListeLigneAchatParPage(pSysteme, pCriteres, -1);
    }
    
    // Dans les autres cas, il peut y avoir plusieurs appel du programme RPG
    ListeLigneAchat listeLigneAchatDocument = new ListeLigneAchat();
    int numeroPage = 1;
    boolean rechercher = true;
    do {
      ListeLigneAchat listeLigneAchatPage = chargerListeLigneAchatParPage(pSysteme, pCriteres, numeroPage);
      if (listeLigneAchatPage == null || listeLigneAchatPage.isEmpty()) {
        rechercher = false;
        break;
      }
      
      // Ajoute les lignes lues dans une liste plus globale
      for (LigneAchat ligneAchat : listeLigneAchatPage) {
        // Contrôle que toutes les lignes du document aient été lu
        if (ligneAchat == null) {
          rechercher = false;
          break;
        }
        listeLigneAchatDocument.add(ligneAchat);
      }
      if (listeLigneAchatPage.size() < DocumentAchat.NOMBRE_LIGNES_ARTICLES_PAR_BLOC) {
        rechercher = false;
      }
      else {
        numeroPage++;
      }
    }
    while (rechercher);
    return listeLigneAchatDocument;
  }
  
  /**
   * Appel du programme RPG qui va rechercher les lignes d'un document par bloc de 15 lignes maximum.
   */
  private ListeLigneAchat chargerListeLigneAchatParPage(SystemeManager pSysteme, CritereLigneAchat pCriteres, int pNumeroPage) {
    // Préparation des paramètres du programme RPG
    Svgam0007i entree = new Svgam0007i();
    Svgam0007o sortie = new Svgam0007o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    ListeLigneAchat listeLigne = new ListeLigneAchat();
    
    // Initialisation des paramètres d'entrée
    if (pCriteres.isLignesOrigine()) {
      entree.setPiind("1000000000");
    }
    else {
      entree.setPiind("0000000000");
    }
    
    if (pCriteres.getIdLigneAchat() != null) {
      entree.setPietb(pCriteres.getIdLigneAchat().getCodeEtablissement());
      entree.setPicod(pCriteres.getIdLigneAchat().getCodeEntete().getCode());
      entree.setPinum(pCriteres.getIdLigneAchat().getNumero());
      entree.setPisuf(pCriteres.getIdLigneAchat().getSuffixe());
      entree.setPinli(pCriteres.getIdLigneAchat().getNumeroLigne());
    }
    else if (pCriteres.getIdDocumentAchat() != null) {
      entree.setPietb(pCriteres.getIdDocumentAchat().getCodeEtablissement());
      entree.setPicod(pCriteres.getIdDocumentAchat().getCodeEntete().getCode());
      entree.setPinum(pCriteres.getIdDocumentAchat().getNumero());
      entree.setPisuf(pCriteres.getIdDocumentAchat().getSuffixe());
      entree.setPilig(DocumentAchat.NOMBRE_LIGNES_ARTICLES_PAR_BLOC);
      if (pNumeroPage <= 0) {
        throw new MessageErreurException("Le numéro de page est invalide.");
      }
      entree.setPipag(pNumeroPage);
    }
    else if (pCriteres.getIdEtablissement() != null && pCriteres.getNumeroDocumentAchat() != null) {
      entree.setPietb(pCriteres.getIdEtablissement().getCodeEtablissement());
      entree.setPinum(pCriteres.getNumeroDocumentAchat());
      entree.setPilig(DocumentAchat.NOMBRE_LIGNES_ARTICLES_PAR_BLOC);
      if (pNumeroPage <= 0) {
        throw new MessageErreurException("Le numéro de page est invalide.");
      }
      entree.setPipag(pNumeroPage);
    }
    Trace.debug("Chargement des lignes par bloc de " + entree.getPilig() + ", numéro de la page en cours : " + entree.getPipag());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      // Entrée
      // Sortie
      Object[] lignes = sortie.getValeursBrutesLignes();
      if (lignes != null) {
        for (int i = 0; i < lignes.length; i++) {
          listeLigne.add(traiterUneLigne((Object[]) lignes[i]));
        }
      }
    }
    
    return listeLigne;
  }
  
  /**
   * Alimentation des variables d'une ligne à partir des données lues dans la base.
   */
  private LigneAchat traiterUneLigne(Object[] ligneBrute) {
    if (ligneBrute == null) {
      return null;
    }
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) ligneBrute[Svgam0007d.VAR_LAETB]);
    EnumCodeEnteteDocumentAchat codeEntete =
        EnumCodeEnteteDocumentAchat.valueOfByCode(((String) ligneBrute[Svgam0007d.VAR_LACOD]).charAt(0));
    IdDocumentAchat idDocumentAchat = IdDocumentAchat.getInstance(idEtablissement, codeEntete,
        ((BigDecimal) ligneBrute[Svgam0007d.VAR_LANUM]).intValue(), ((BigDecimal) ligneBrute[Svgam0007d.VAR_LASUF]).intValue());
    IdLigneAchat idLigneAchat = IdLigneAchat.getInstance(idDocumentAchat, ((BigDecimal) ligneBrute[Svgam0007d.VAR_LANLI]).intValue());
    
    // On détermine le type de la ligne
    EnumTypeLigneAchat typeLigne = EnumTypeLigneAchat.valueOfByCode(((String) ligneBrute[Svgam0007d.VAR_LAERL]).charAt(0));
    EnumTypeArticle typeArticle = EnumTypeArticle.valueOfByCodeAlpha(((String) ligneBrute[Svgam0007d.VAR_WTYPART]));
    
    // C'est un article commentaire
    if ((typeArticle != null && typeArticle.equals(EnumTypeArticle.COMMENTAIRE))
        || (typeLigne != null && typeLigne.equals(EnumTypeLigneAchat.COMMENTAIRE))) {
      return traiterUneLigneCommentaire(ligneBrute, idLigneAchat);
    }
    // Sinon c'est un article normal
    else {
      return traiterUneLigneArticle(ligneBrute, idLigneAchat);
    }
  }
  
  /**
   * Alimentation des variables d'une ligne commentaire à partir des données lues dans la base.
   */
  private LigneAchatCommentaire traiterUneLigneCommentaire(Object[] ligneBrute, IdLigneAchat pIdLigneAchat) {
    LigneAchatCommentaire ligneAchat = new LigneAchatCommentaire(pIdLigneAchat);
    // Toutes les lignes d'un document ne comporte pas un code article
    String codeArticle = (String) ligneBrute[Svgam0007d.VAR_LAART];
    if (!codeArticle.trim().isEmpty()) {
      IdArticle idArticle = IdArticle.getInstance(pIdLigneAchat.getIdEtablissement(), (String) ligneBrute[Svgam0007d.VAR_LAART]);
      ligneAchat.setIdArticle(idArticle);
    }
    
    ligneAchat.setCodeEtat(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATOP]).intValue());
    EnumTypeLigneAchat typeLigne = EnumTypeLigneAchat.valueOfByCode(((String) ligneBrute[Svgam0007d.VAR_LAERL]).charAt(0));
    ligneAchat.setTypeLigne(typeLigne);
    ligneAchat.setCodeExtraction(EnumCodeExtractionDocumentAchat.valueOfByCode(((String) ligneBrute[Svgam0007d.VAR_LACEX]).charAt(0)));
    
    ligneAchat.setTexte((String) ligneBrute[Svgam0007d.VAR_WLIGLB]);
    
    return ligneAchat;
  }
  
  /**
   * Alimentation des variables d'une ligne article à partir des données lues dans la base.
   */
  private LigneAchatArticle traiterUneLigneArticle(Object[] ligneBrute, IdLigneAchat pIdLigneAchat) {
    LigneAchatArticle ligneAchat = new LigneAchatArticle(pIdLigneAchat);
    
    // Toutes les lignes d'un document ne comporte pas un code article
    String codeArticle = (String) ligneBrute[Svgam0007d.VAR_LAART];
    if (!codeArticle.trim().isEmpty()) {
      IdArticle idArticle = IdArticle.getInstance(pIdLigneAchat.getIdEtablissement(), (String) ligneBrute[Svgam0007d.VAR_LAART]);
      ligneAchat.setIdArticle(idArticle);
    }
    
    ligneAchat.setCodeEtat(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATOP]).intValue());
    EnumTypeLigneAchat typeLigne = EnumTypeLigneAchat.valueOfByCode(((String) ligneBrute[Svgam0007d.VAR_LAERL]).charAt(0));
    ligneAchat.setTypeLigne(typeLigne);
    ligneAchat.setCodeExtraction(EnumCodeExtractionDocumentAchat.valueOfByCode(((String) ligneBrute[Svgam0007d.VAR_LACEX]).charAt(0)));
    ligneAchat.setQuantiteExtraiteUV((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQCX]);
    ligneAchat.setCodeTVA(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATVA]).intValue());
    
    ligneAchat.setTopLigneEnValeur(((BigDecimal) ligneBrute[Svgam0007d.VAR_LAVAL]).intValue());
    ligneAchat.setNumeroColonneTVA(((BigDecimal) ligneBrute[Svgam0007d.VAR_LACOL]).intValue());
    ligneAchat.setSigneLigne(((BigDecimal) ligneBrute[Svgam0007d.VAR_LASGN]).intValue());
    ligneAchat.setCodeAvoir(((String) ligneBrute[Svgam0007d.VAR_LAAVR]).charAt(0));
    if (!((String) ligneBrute[Svgam0007d.VAR_LAMAG]).trim().isEmpty()) {
      ligneAchat
          .setIdMagasin(IdMagasin.getInstance(pIdLigneAchat.getIdEtablissement(), ((String) ligneBrute[Svgam0007d.VAR_LAMAG]).trim()));
    }
    if (!((String) ligneBrute[Svgam0007d.VAR_LAMAGA]).trim().isEmpty()) {
      ligneAchat
          .setIdMagasinAvantModif(IdMagasin.getInstance(pIdLigneAchat.getIdEtablissement(), (String) ligneBrute[Svgam0007d.VAR_LAMAGA]));
    }
    
    // Le rapprochement est toujours lié à son code Entete opposé : Facture avec un bon et un bon avec une facture
    int numeroLigneRapprochee = ((BigDecimal) ligneBrute[Svgam0007d.VAR_LANUMR]).intValue();
    if (numeroLigneRapprochee > 0) {
      IdLigneAchat idLigneRapprochee =
          IdLigneAchat.getInstance(ligneAchat.getId().getIdEtablissement(), pIdLigneAchat.getCodeEntete(), numeroLigneRapprochee,
              ((BigDecimal) ligneBrute[Svgam0007d.VAR_LASUFR]).intValue(), ((BigDecimal) ligneBrute[Svgam0007d.VAR_LANLIR]).intValue());
      ligneAchat.setIdLigneRapprochee(idLigneRapprochee);
    }
    ligneAchat.setDateHistoriqueStock(ConvertDate.db2ToDate(((BigDecimal) ligneBrute[Svgam0007d.VAR_LASUFR]).intValue(), null));
    ligneAchat.setOrdreHistoriqueStock(((BigDecimal) ligneBrute[Svgam0007d.VAR_LAORDH]).intValue());
    ligneAchat.setCodeSectionAnalytique((String) ligneBrute[Svgam0007d.VAR_LASAN]);
    ligneAchat.setCodeAffaire((String) ligneBrute[Svgam0007d.VAR_LAACT]);
    ligneAchat.setMontantAffecte((BigDecimal) ligneBrute[Svgam0007d.VAR_LAMTA]);
    ligneAchat.setDateLivraisonPrevue(ConvertDate.db2ToDate(((BigDecimal) ligneBrute[Svgam0007d.VAR_LADLP]).intValue(), null));
    ligneAchat.setEtatSaisieNumeroSerieOuLot(((BigDecimal) ligneBrute[Svgam0007d.VAR_LASER]).intValue());
    
    // Les tops personnalisables
    ListeZonePersonnalisee listeTopPersonnalisable = new ListeZonePersonnalisee();
    ligneAchat.setListeTopPersonnalisable(listeTopPersonnalisable);
    listeTopPersonnalisable.setTopPersonnalisation1((String) ligneBrute[Svgam0007d.VAR_LATP1]);
    listeTopPersonnalisable.setTopPersonnalisation2((String) ligneBrute[Svgam0007d.VAR_LATP2]);
    listeTopPersonnalisable.setTopPersonnalisation3((String) ligneBrute[Svgam0007d.VAR_LATP3]);
    listeTopPersonnalisable.setTopPersonnalisation4((String) ligneBrute[Svgam0007d.VAR_LATP4]);
    listeTopPersonnalisable.setTopPersonnalisation5((String) ligneBrute[Svgam0007d.VAR_LATP5]);
    
    ligneAchat.setImputationFrais(((String) ligneBrute[Svgam0007d.VAR_LAIN1]).charAt(0));
    ligneAchat.setTypeFrais(((String) ligneBrute[Svgam0007d.VAR_LAIN2]).charAt(0));
    ligneAchat.setRegroupementArticle(((String) ligneBrute[Svgam0007d.VAR_LAIN3]).charAt(0));
    ligneAchat.setComptabilisationStockFlottant(((String) ligneBrute[Svgam0007d.VAR_LAIN4]).charAt(0));
    ligneAchat.setConditionEnNombreGratuit(((String) ligneBrute[Svgam0007d.VAR_LAIN5]).charAt(0));
    ligneAchat.setDateLivraisonCalculee(ConvertDate.db2ToDate(((BigDecimal) ligneBrute[Svgam0007d.VAR_LADLC]).intValue(), null));
    
    ligneAchat.setLibelleArticle((String) ligneBrute[Svgam0007d.VAR_WLIGLB]);
    ligneAchat.setQuantiteDocumentOrigine((BigDecimal) ligneBrute[Svgam0007d.VAR_WQTEOR]);
    ligneAchat.setQuantiteDejaExtraite((BigDecimal) ligneBrute[Svgam0007d.VAR_WQTEEX]);
    ligneAchat.setTypeArticle(EnumTypeArticle.valueOfByCodeAlpha(((String) ligneBrute[Svgam0007d.VAR_WTYPART])));
    
    // Alimentation de la classe ParamètrePrixAchat avec les données retournées
    if (!ligneAchat.isLigneCommentaire()) {
      ParametrePrixAchat parametre = new ParametrePrixAchat();
      
      // Renseigner les unités
      if (!((String) ligneBrute[Svgam0007d.VAR_LAUNA]).trim().isEmpty()) {
        IdUnite idUA = IdUnite.getInstance((String) ligneBrute[Svgam0007d.VAR_LAUNA]);
        parametre.setIdUA(idUA);
      }
      if (!((String) ligneBrute[Svgam0007d.VAR_LAUNC]).trim().isEmpty()) {
        IdUnite idUCA = IdUnite.getInstance((String) ligneBrute[Svgam0007d.VAR_LAUNC]);
        parametre.setIdUCA(idUCA);
      }
      parametre.setNombreDecimaleUA(((BigDecimal) ligneBrute[Svgam0007d.VAR_LADCA]).intValue());
      parametre.setNombreDecimaleUCA(((BigDecimal) ligneBrute[Svgam0007d.VAR_LADCC]).intValue());
      parametre.setNombreDecimaleUS(((BigDecimal) ligneBrute[Svgam0007d.VAR_LADCS]).intValue());
      
      // Renseigner les ratios
      parametre.setNombreUAParUCA((BigDecimal) ligneBrute[Svgam0007d.VAR_LAKAC]);
      parametre.setNombreUSParUCA((BigDecimal) ligneBrute[Svgam0007d.VAR_LAKSC]);
      
      // Renseigner les quantités en reliquat
      parametre.setQuantiteReliquatUA((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQTA]);
      parametre.setQuantiteReliquatUCA((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQTC]);
      parametre.setQuantiteReliquatUS((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQTS]);
      // Renseigner les quantités initiales
      parametre.setQuantiteInitialeUA((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQTAI]);
      parametre.setQuantiteInitialeUCA((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQTCI]);
      parametre.setQuantiteInitialeUS((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQTSI]);
      // Renseigner les quantités traitées
      parametre.setQuantiteTraiteeUA((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQTAT]);
      parametre.setQuantiteTraiteeUCA((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQTCT]);
      parametre.setQuantiteTraiteeUS((BigDecimal) ligneBrute[Svgam0007d.VAR_LAQTST]);
      
      // Renseigner les montants
      parametre.setPrixAchatBrutHT((BigDecimal) ligneBrute[Svgam0007d.VAR_LAPAB]);
      ListeRemise listeRemise = parametre.getListeRemise();
      listeRemise.setPourcentageRemise1((BigDecimal) ligneBrute[Svgam0007d.VAR_LAREM1]);
      listeRemise.setPourcentageRemise2((BigDecimal) ligneBrute[Svgam0007d.VAR_LAREM2]);
      listeRemise.setPourcentageRemise3((BigDecimal) ligneBrute[Svgam0007d.VAR_LAREM3]);
      listeRemise.setPourcentageRemise4((BigDecimal) ligneBrute[Svgam0007d.VAR_LAREM4]);
      listeRemise.setPourcentageRemise5((BigDecimal) ligneBrute[Svgam0007d.VAR_LAREM5]);
      listeRemise.setPourcentageRemise6((BigDecimal) ligneBrute[Svgam0007d.VAR_LAREM6]);
      listeRemise.setTypeRemise(((String) ligneBrute[Svgam0007d.VAR_LATRL]).charAt(0));
      listeRemise.setBaseRemise(((String) ligneBrute[Svgam0007d.VAR_LABRL]).charAt(0));
      parametre.setPourcentageTaxeOuMajoration(((BigDecimal) ligneBrute[Svgam0007d.VAR_W7LFK3]));
      parametre.setMontantConditionnement(((BigDecimal) ligneBrute[Svgam0007d.VAR_W7LFV3]));
      parametre.setPrixNetHT((BigDecimal) ligneBrute[Svgam0007d.VAR_LAPAN]);
      parametre.setPrixCalculeHT((BigDecimal) ligneBrute[Svgam0007d.VAR_LAPAC]);
      parametre.setMontantAuPoidsPortHT(((BigDecimal) ligneBrute[Svgam0007d.VAR_W7LFV1]));
      parametre.setPoidsPort(((BigDecimal) ligneBrute[Svgam0007d.VAR_W7LFK1]));
      parametre
          .setPourcentagePort(new BigPercentage(((BigDecimal) ligneBrute[Svgam0007d.VAR_W7LFP1]), EnumFormatPourcentage.POURCENTAGE));
      parametre.setMontantPortHT((BigDecimal) ligneBrute[Svgam0007d.VAR_W7PORTF]);
      parametre.setPrixRevientFournisseurHT((BigDecimal) ligneBrute[Svgam0007d.VAR_W7PRVFR]);
      parametre.setMontantReliquatHT((BigDecimal) ligneBrute[Svgam0007d.VAR_LAMHT]);
      parametre.setMontantInitialHT((BigDecimal) ligneBrute[Svgam0007d.VAR_LAMHTI]);
      parametre.setMontantTraiteHT((BigDecimal) ligneBrute[Svgam0007d.VAR_LAMHTT]);
      
      // Renseigner les exclusions de remises de pied
      Character[] exlusionremise = parametre.getExclusionRemisePied();
      exlusionremise[0] = ((String) ligneBrute[Svgam0007d.VAR_LARP1]).charAt(0);
      exlusionremise[1] = ((String) ligneBrute[Svgam0007d.VAR_LARP2]).charAt(0);
      exlusionremise[2] = ((String) ligneBrute[Svgam0007d.VAR_LARP3]).charAt(0);
      exlusionremise[3] = ((String) ligneBrute[Svgam0007d.VAR_LARP4]).charAt(0);
      exlusionremise[4] = ((String) ligneBrute[Svgam0007d.VAR_LARP5]).charAt(0);
      exlusionremise[5] = ((String) ligneBrute[Svgam0007d.VAR_LARP6]).charAt(0);
      
      // Renseigner les indicateurs
      parametre.setTopPrixBaseSaisie(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATB]).intValue());
      parametre.setTopRemiseSaisie(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATR]).intValue());
      parametre.setTopPrixNetSaisi(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATN]).intValue());
      parametre.setTopUniteSaisie(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATU]).intValue());
      parametre.setTopQuantiteSaisie(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATQ]).intValue());
      parametre.setTopMontantHTSaisi(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATH]).intValue());
      
      // Renseigner les éléments de TVA
      parametre.setCodeTVA(((BigDecimal) ligneBrute[Svgam0007d.VAR_LATVA]).intValue());
      parametre.setColonneTVA(((BigDecimal) ligneBrute[Svgam0007d.VAR_LACOL]).intValue());
      
      // Mettre à jour l'article
      if (ligneAchat.getPrixAchat() == null) {
        ligneAchat.setPrixAchat(new PrixAchat());
      }
      ligneAchat.getPrixAchat().initialiser(parametre);
    }
    
    return ligneAchat;
  }
}
