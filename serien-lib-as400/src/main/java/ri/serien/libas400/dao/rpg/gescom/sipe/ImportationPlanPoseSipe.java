/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.sipe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.dao.sql.exploitation.parametres.SqlExploitationParametres;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.personnalisation.parametrent.ParametreNT;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.prodevis.InformationsConfigurationProDevis;
import ri.serien.libcommun.gescom.vente.sipe.CritereSipe;
import ri.serien.libcommun.gescom.vente.sipe.EnumStatutSipe;
import ri.serien.libcommun.gescom.vente.sipe.EnumTypePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.IdPlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.InformationsConfigurationSipe;
import ri.serien.libcommun.gescom.vente.sipe.ListePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.PlanPoseSipe;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Cette classe regroupe l'ensemble des traitements nécessaires à l'importation des plans de pose Sipe.
 */
public class ImportationPlanPoseSipe extends RpgBase {
  // Constantes
  private static final String PROGRAMME_COLLECTE = "SGVMSIPC1";
  private static final String PROGRAMME_IMPORT = "SGVMSIPC2";
  
  private static final String DOSSIER_SIPE = "SIPE";
  private static final String DOSSIER_SAUVEGARDE = "SAV";
  
  // Variables
  private SystemeManager systeme = null;
  private QueryManager querymg = null;
  private InformationsConfigurationSipe informationsConfiguration = null;
  
  /**
   * Constructeur.
   */
  public ImportationPlanPoseSipe(SystemeManager pSysteme, QueryManager pQuerymg) {
    if (pSysteme == null) {
      throw new MessageErreurException("La connexion avec le serveur n'est pas initialisée.");
    }
    if (pQuerymg == null) {
      throw new MessageErreurException("La connexion à la base de données n'est pas initialisée.");
    }
    
    systeme = pSysteme;
    querymg = pQuerymg;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne une liste d'identifiants correspondants aux critères de recherche.
   * 
   * @param pCritere
   * @param pMiseAJourDepuisIfs
   * @return
   */
  public List<IdPlanPoseSipe> chargerListeIdPlanPoseSipe(CritereSipe pCritere, boolean pMiseAJourDepuisIfs) {
    // Chargement de la table PGVMSIPM avec les fichiers textes trouvés dans l'IFS
    if (pMiseAJourDepuisIfs) {
      miseAJourListeFichierSipe();
    }
    
    // Contrôle de l'existence de la table
    if (!querymg.isTableExiste(EnumTableBDD.PLAN_POSE_SIPE)) {
      throw new MessageErreurException(
          "La table " + EnumTableBDD.PLAN_POSE_SIPE + " n'est pas présente dans la base de données " + querymg.getLibrary());
    }
    
    RequeteSql requeteSql = new RequeteSql();
    
    // Construction de la requête
    requeteSql.ajouter("select SIPKEY from " + querymg.getLibrary() + '.' + EnumTableBDD.PLAN_POSE_SIPE);
    
    // Ajout des conditions si les critères sont présents
    if (pCritere != null) {
      RequeteSql requeteSqlCondition = new RequeteSql();
      
      // Filtrage sur le statut
      if (pCritere.getListeStatut() != null && !pCritere.getListeStatut().isEmpty()) {
        requeteSqlCondition.ajouter("(");
        for (EnumStatutSipe statut : pCritere.getListeStatut()) {
          requeteSqlCondition.ajouterConditionOr("SIPSTA", "=", statut.getCode());
        }
        requeteSqlCondition.ajouter(")");
      }
      // Filtrage sur la date
      if (pCritere.getDateDebutPlan() != null || pCritere.getDateFinPlan() != null) {
        Integer dateDebut = null;
        if (pCritere.getDateDebutPlan() != null) {
          dateDebut = Integer.valueOf(ConvertDate.dateToDb2(pCritere.getDateDebutPlan()));
        }
        Integer dateFin = null;
        if (pCritere.getDateFinPlan() != null) {
          dateFin = Integer.valueOf(ConvertDate.dateToDb2(pCritere.getDateFinPlan()));
        }
        requeteSqlCondition.ajouterPlageDateConditionAnd("SIPDAT", dateDebut, dateFin);
      }
      
      // Ajout des conditions à la requête principale
      if (!requeteSqlCondition.isEmpty()) {
        requeteSql.ajouter(" where " + requeteSqlCondition.getRequete());
      }
    }
    
    // Exécution de la requête
    List<GenericRecord> listeRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Traitement des données lues en table
    List<IdPlanPoseSipe> listeIdLicence = new ArrayList<IdPlanPoseSipe>();
    for (GenericRecord record : listeRecord) {
      PlanPoseSipe planPose = initialiserDescription(record);
      if (record == null) {
        continue;
      }
      listeIdLicence.add(planPose.getId());
    }
    return listeIdLicence;
  }
  
  /**
   * Recherche les plans de pose Sipe correspondants aux critères voulus.
   */
  public ListePlanPoseSipe chargerListePlanPoseSipe(List<IdPlanPoseSipe> pListeIdPlanPose) {
    if (pListeIdPlanPose == null || pListeIdPlanPose.isEmpty()) {
      return null;
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter("select"
        + "   SIPKEY,"
        + "   SIPSTA,"
        + "   SIPUSR,"
        + "   SIPTYP,"
        + "   SIPDAT,"
        + "   SIPENT,"
        + "   SIPBAT,"
        + "   SIPAFF,"
        + "   SIPNUM,"
        + "   SIPCHA,"
        + "   SIPFIC"
        + " from " + querymg.getLibrary() + '.' + EnumTableBDD.PLAN_POSE_SIPE);
    // @formatter:on
    // Construction du groupe
    for (IdPlanPoseSipe idPlanSipe : pListeIdPlanPose) {
      requeteSql.ajouterValeurAuGroupe("idPlanPose", idPlanSipe.getNumero());
    }
    requeteSql.ajouter(" where SIPKEY");
    requeteSql.ajouterGroupeDansRequete("idPlanPose", "in");
    
    // Exécution de la requête
    List<GenericRecord> listeRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    ListePlanPoseSipe listePlanPose = new ListePlanPoseSipe();
    for (GenericRecord record : listeRecord) {
      PlanPoseSipe planPose = initialiserDescription(record);
      if (record == null) {
        continue;
      }
      listePlanPose.add(planPose);
    }
    
    return listePlanPose;
  }
  
  /**
   * Supprime fichier correspondant à une plan de pose.
   * 
   * @param pPlanPoseSipe
   */
  public void supprimerFichier(PlanPoseSipe pPlanPoseSipe) {
    if (pPlanPoseSipe == null) {
      throw new MessageErreurException("Le plan de pose Sipe est invalide.");
    }
    
    String nomFichier = Constantes.normerTexte(pPlanPoseSipe.getNomFichier());
    if (nomFichier.isEmpty()) {
      throw new MessageErreurException("Le nom du fichier Sipe est invalide.");
    }
    
    // Récupération du dossier Sipe
    String dossier = retournerDossierSipe(pPlanPoseSipe.getTypePlanPoseSipe(), false);
    dossier = Constantes.normerTexte(dossier);
    if (dossier.isEmpty()) {
      throw new MessageErreurException("Le dossier de stockage pour les fichiers Sipe est invalide.");
    }
    // Suppression fichier de l'IFS
    String fichierASupprimer = dossier + nomFichier;
    boolean retour = true;
    File fichier = new File(fichierASupprimer);
    if (fichier.exists()) {
      retour = fichier.delete();
    }
    else {
      retour = false;
    }
    if (!retour) {
      throw new MessageErreurException("Il y a eu un problème lors de la suppression du fichier " + fichierASupprimer);
    }
    
    // Modification du statut du plan de la liste
    changerStatutPlanPose(pPlanPoseSipe.getId(), EnumStatutSipe.SUPPRIME);
  }
  
  /**
   * Mise à jour de la liste des plans de pose Sipe présents sur l'IFS
   */
  public void miseAJourListeFichierSipe() {
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(systeme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    rpg.executerProgramme(PROGRAMME_COLLECTE, EnvironnementExecution.getGvmas(), null);
  }
  
  /**
   * Appel du programme RPG qui va injecter un plan de pose.
   */
  public void injecterPlanPose(PlanPoseSipe pPlanPoseSipe, IdDocumentVente pIdDocument) {
    if (pPlanPoseSipe == null) {
      throw new MessageErreurException("Le plane de pose Sipe est invalide.");
    }
    if (pPlanPoseSipe.getTypePlanPoseSipe() == null) {
      throw new MessageErreurException("Le type du fichier du plane de pose Sipe n'est pas renseigné.");
    }
    if (pPlanPoseSipe.getNomFichier().isEmpty()) {
      throw new MessageErreurException("Le nom du fichier du plane de pose Sipe n'est pas renseigné.");
    }
    if (pIdDocument == null) {
      throw new MessageErreurException("L'identifiant du document de ventes est invalide.");
    }
    
    // Préparation des paramètres
    ProgramParameter[] listeParametres = new ProgramParameter[4];
    AS400Text fichier = new AS400Text(PlanPoseSipe.TAILLE_NOM_FICHIER, systeme.getSystem());
    listeParametres[0] = new ProgramParameter(fichier.toBytes(pPlanPoseSipe.getNomFichier()), PlanPoseSipe.TAILLE_NOM_FICHIER);
    AS400Text type = new AS400Text(1, systeme.getSystem());
    listeParametres[1] = new ProgramParameter(type.toBytes("" + pPlanPoseSipe.getTypePlanPoseSipe().getCode()), 1);
    AS400Text idDocument = new AS400Text(IdDocumentVente.LONGUEUR_INDICATIF, systeme.getSystem());
    listeParametres[2] = new ProgramParameter(idDocument.toBytes(pIdDocument.getIndicatif(IdDocumentVente.INDICATIF_ENTETE_ETB_NUM_SUF)),
        IdDocumentVente.LONGUEUR_INDICATIF);
    listeParametres[3] = erreur;
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(systeme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG d'importation pour chaque fichier trouvé
    if (rpg.executerProgramme(PROGRAMME_IMPORT, EnvironnementExecution.getGvmas(), listeParametres)) {
      // Récupération et conversion des paramètres du RPG
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Extraction de la description des fichiers textes Pro-Devis dans un fichier base de données de Série N,
   * afin d'extraire les informations principales.
   */
  public boolean extraireDescriptionsFichiersTextes(String[] pListeFichiers, IdEtablissement pIdEtablissement) {
    String pCodeEtablissement = "";
    if (pIdEtablissement != null) {
      pCodeEtablissement = Constantes.normerTexte(pIdEtablissement.getCodeEtablissement());
    }
    if (pCodeEtablissement.isEmpty()) {
      throw new MessageErreurException("L'établissement passé en paramètre est vide.");
    }
    if (pListeFichiers == null) {
      throw new MessageErreurException("La liste des fichiers à importer est à null.");
    }
    if (systeme == null) {
      throw new MessageErreurException("La variable 'systeme' est à null.");
    }
    
    boolean retour = true;
    ContexteProgrammeRPG rpg = new ContexteProgrammeRPG(systeme.getSystem(), systeme.getCurlib());
    rpg.executerCommande("CHGDTAARA DTAARA(*LDA (29 10)) VALUE(" + String.format("'%-10s'", systeme.getProfilUtilisateur()) + ")");
    rpg.initialiserLettreEnvironnement(EnvironnementExecution.getLettreEnvironnement());
    rpg.ajouterBibliotheque(systeme.getCurlib(), true);
    rpg.ajouterBibliotheque(systeme.getLibenv(), false);
    rpg.ajouterBibliotheque(EnvironnementExecution.getExpas(), false);
    rpg.ajouterBibliotheque(EnvironnementExecution.getGvmas(), false);
    rpg.ajouterBibliotheque(EnvironnementExecution.getGvmx(), false);
    
    // Exécution du programme RPG d'importation pour chaque fichier trouvé
    for (String fichier : pListeFichiers) {
      fichier = Constantes.normerTexte(fichier);
      if (fichier.isEmpty()) {
        continue;
      }
      String programme = String.format("CALL PGM(SGVM9GC3) PARM('%-" + InformationsConfigurationProDevis.TAILLE_NOM_DOSSIER + "s' '%-"
          + IdEtablissement.LONGUEUR_CODE_ETABLISSEMENT + "s')", fichier, pCodeEtablissement);
      if (!rpg.executerCommande(programme)) {
        retour = false;
      }
    }
    
    return retour;
  }
  
  /**
   * Retourne le dossier dans lequel sont stockés les fichiers de Sipe.
   * (avec le slash à la fin)
   * 
   * @param pTypePlanPoseSipe
   * @param pDossierSauvegarde
   * @return
   */
  public String retournerDossierSipe(EnumTypePlanPoseSipe pTypePlanPoseSipe, boolean pDossierSauvegarde) {
    if (pTypePlanPoseSipe == null) {
      throw new MessageErreurException("Le type du plan de pose est invalide.");
    }
    
    // Lire le paramètre NT
    String dossierRacine = null;
    try {
      SqlExploitationParametres sqlExploitationParametres = new SqlExploitationParametres(querymg);
      ParametreNT parametreNT = sqlExploitationParametres.lireParametreNT();
      if (parametreNT == null) {
        throw new MessageErreurException("Le paramètre NT de la base de données " + querymg.getLibrary() + " est invalide.");
      }
      dossierRacine = Constantes.normerTexte(parametreNT.getRacine());
      if (dossierRacine.isEmpty()) {
        return null;
      }
    }
    catch (Exception e) {
      throw new MessageErreurException("Erreur lors de la lecture du paramètre NT.");
    }
    
    // Construire le chemin complet du dossier IFS
    if (dossierRacine.charAt(0) != '/') {
      dossierRacine = '/' + dossierRacine;
    }
    if (!dossierRacine.endsWith("/")) {
      dossierRacine = dossierRacine + '/';
    }
    // Ajout du nom de la base de données
    dossierRacine = dossierRacine + querymg.getLibrary() + '/';
    // Ajout du nom du logiciel
    dossierRacine = dossierRacine + DOSSIER_SIPE + '/';
    // Ajout du type de plan sous forme de libellé
    dossierRacine = dossierRacine + pTypePlanPoseSipe.getLibelle().toUpperCase() + '/';
    // Est-ce un dossier sauvegarde ?
    if (pDossierSauvegarde) {
      dossierRacine = dossierRacine + DOSSIER_SAUVEGARDE + '/';
    }
    
    return dossierRacine;
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise une description de plan de pose Sipe à partir de l'enregistrement lu en table.
   */
  private PlanPoseSipe initialiserDescription(GenericRecord pRecord) {
    if (pRecord == null) {
      return null;
    }
    if (!pRecord.isPresentField("SIPKEY")) {
      return null;
    }
    
    try {
      // Création de l'identifiant
      IdPlanPoseSipe idPlanPoseSipe = IdPlanPoseSipe.getInstance(pRecord.getIntegerValue("SIPKEY"));
      // Création de l'objet
      PlanPoseSipe descriptionSipe = new PlanPoseSipe(idPlanPoseSipe);
      
      // Initialisation des attributs
      if (pRecord.isPresentField("SIPSTA")) {
        descriptionSipe.setStatut(EnumStatutSipe.valueOfByCode(pRecord.getIntegerValue("SIPSTA")));
      }
      if (pRecord.isPresentField("SIPUSR")) {
        descriptionSipe.setUtilisateurChangementStatut(pRecord.getStringValue("SIPUSR", "", true));
      }
      if (pRecord.isPresentField("SIPDAT")) {
        descriptionSipe.setDatePlan(pRecord.getDateValue("SIPDAT"));
      }
      if (pRecord.isPresentField("SIPTYP")) {
        descriptionSipe.setTypePlanPoseSipe(EnumTypePlanPoseSipe.valueOfByCode(pRecord.getCharacterValue("SIPTYP")));
      }
      if (pRecord.isPresentField("SIPENT")) {
        descriptionSipe.setNomEntreprise(pRecord.getStringValue("SIPENT", "", true));
      }
      if (pRecord.isPresentField("SIPBAT")) {
        descriptionSipe.setNomBatiment(pRecord.getStringValue("SIPBAT", "", true));
      }
      if (pRecord.isPresentField("SIPAFF")) {
        descriptionSipe.setNumeroAffaire(pRecord.getStringValue("SIPAFF", "", true));
      }
      if (pRecord.isPresentField("SIPNUM")) {
        descriptionSipe.setNumeroPlan(pRecord.getStringValue("SIPNUM", "", true));
      }
      if (pRecord.isPresentField("SIPCHA")) {
        descriptionSipe.setNomChantier(pRecord.getStringValue("SIPCHA", "", true));
      }
      if (pRecord.isPresentField("SIPFIC")) {
        descriptionSipe.setNomFichier(pRecord.getStringValue("SIPFIC", "", true));
      }
      return descriptionSipe;
    }
    catch (Exception e) {
      return null;
    }
  }
  
  /**
   * Changer le statut d'un plan de pose.
   */
  private void changerStatutPlanPose(IdPlanPoseSipe pIdPlanPose, EnumStatutSipe pStatut) {
    if (pIdPlanPose == null) {
      throw new MessageErreurException("L'identifiant du plan de pose est invalide.");
    }
    if (pStatut == null) {
      throw new MessageErreurException("Le statut du plan de pose est invalide.");
    }
    // Construction de la requête
    RequeteSql requete = new RequeteSql();
    // @formatter:off
    requete.ajouter("update " + querymg.getLibrary() + "." + EnumTableBDD.PLAN_POSE_SIPE
        + " set SIPSTA = " + pStatut.getCode()
        + " where SIPKEY = " + pIdPlanPose.getNumero());
    // @formatter:on
    
    // Execution de la requête
    querymg.requete(requete.getRequete());
    Trace.info("Le statut du plan de pose " + pIdPlanPose.getNumero() + " a été modifié dans la base " + querymg.getLibrary());
  }
  
}
