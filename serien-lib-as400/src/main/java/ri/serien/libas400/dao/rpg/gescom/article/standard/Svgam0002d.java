/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.standard;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0002d {
  // Constantes
  public static final int SIZE_WETB = 3;
  public static final int SIZE_WART = 20;
  public static final int SIZE_WLIB = 30;
  public static final int SIZE_WLB1 = 30;
  public static final int SIZE_WLB2 = 30;
  public static final int SIZE_WLB3 = 30;
  public static final int SIZE_WOBS = 10;
  public static final int SIZE_WDAP = 7;
  public static final int DECIMAL_WDAP = 0;
  public static final int SIZE_WPRA = 11;
  public static final int DECIMAL_WPRA = 2;
  public static final int SIZE_WREM1 = 4;
  public static final int DECIMAL_WREM1 = 2;
  public static final int SIZE_WREM2 = 4;
  public static final int DECIMAL_WREM2 = 2;
  public static final int SIZE_WREM3 = 4;
  public static final int DECIMAL_WREM3 = 2;
  public static final int SIZE_WREM4 = 4;
  public static final int DECIMAL_WREM4 = 2;
  public static final int SIZE_WREM5 = 4;
  public static final int DECIMAL_WREM5 = 2;
  public static final int SIZE_WREM6 = 4;
  public static final int DECIMAL_WREM6 = 2;
  public static final int SIZE_WUNA = 2;
  public static final int SIZE_WNUA = 8;
  public static final int DECIMAL_WNUA = 3;
  public static final int SIZE_WKUAUCA = 8;
  public static final int DECIMAL_WKUAUCA = 3;
  public static final int SIZE_WDEL = 2;
  public static final int DECIMAL_WDEL = 0;
  public static final int SIZE_WREF = 20;
  public static final int SIZE_WUCA = 2;
  public static final int SIZE_WKUSUCA = 8;
  public static final int DECIMAL_WKUSUCA = 3;
  public static final int SIZE_WUS = 2;
  public static final int SIZE_WUCS = 2;
  public static final int SIZE_WKUSUCS = 8;
  public static final int DECIMAL_WKUSUCS = 3;
  public static final int SIZE_WKUCAUCS = 8;
  public static final int DECIMAL_WKUCAUCS = 3;
  public static final int SIZE_WDEV = 3;
  public static final int SIZE_WQMI = 10;
  public static final int DECIMAL_WQMI = 3;
  public static final int SIZE_WQEC = 10;
  public static final int DECIMAL_WQEC = 3;
  public static final int SIZE_WDELS = 2;
  public static final int DECIMAL_WDELS = 0;
  public static final int SIZE_WKPR = 5;
  public static final int DECIMAL_WKPR = 4;
  public static final int SIZE_WTRL = 1;
  public static final int SIZE_WRFC = 20;
  public static final int SIZE_WGCD = 13;
  public static final int DECIMAL_WGCD = 0;
  public static final int SIZE_WDAF = 7;
  public static final int DECIMAL_WDAF = 0;
  public static final int SIZE_WLIBA = 30;
  public static final int SIZE_WOPA = 3;
  public static final int SIZE_WIN7 = 1;
  public static final int SIZE_WNET = 11;
  public static final int DECIMAL_WNET = 2;
  public static final int SIZE_WNETP = 11;
  public static final int DECIMAL_WNETP = 2;
  public static final int SIZE_WKAP = 5;
  public static final int DECIMAL_WKAP = 4;
  public static final int SIZE_WPRS = 9;
  public static final int DECIMAL_WPRS = 2;
  public static final int SIZE_WPRSA = 9;
  public static final int DECIMAL_WPRSA = 2;
  public static final int SIZE_WFV1 = 9;
  public static final int DECIMAL_WFV1 = 2;
  public static final int SIZE_WFK1 = 7;
  public static final int DECIMAL_WFK1 = 4;
  public static final int SIZE_WFP1 = 4;
  public static final int DECIMAL_WFP1 = 2;
  public static final int SIZE_WFU1 = 1;
  public static final int SIZE_WFV2 = 9;
  public static final int DECIMAL_WFV2 = 2;
  public static final int SIZE_WFK2 = 7;
  public static final int DECIMAL_WFK2 = 4;
  public static final int SIZE_WFP2 = 4;
  public static final int DECIMAL_WFP2 = 2;
  public static final int SIZE_WFU2 = 1;
  public static final int SIZE_WFV3 = 9;
  public static final int DECIMAL_WFV3 = 2;
  public static final int SIZE_WFK3 = 7;
  public static final int DECIMAL_WFK3 = 4;
  public static final int SIZE_WFU3 = 1;
  public static final int SIZE_WIN5 = 1;
  public static final int SIZE_WSTK = 11;
  public static final int DECIMAL_WSTK = 3;
  public static final int SIZE_WRES = 11;
  public static final int DECIMAL_WRES = 3;
  public static final int SIZE_WATT = 11;
  public static final int DECIMAL_WATT = 3;
  public static final int SIZE_WDIV = 11;
  public static final int DECIMAL_WDIV = 3;
  public static final int SIZE_WDIA = 11;
  public static final int DECIMAL_WDIA = 3;
  public static final int SIZE_WMIN = 11;
  public static final int DECIMAL_WMIN = 3;
  public static final int SIZE_WMAX = 11;
  public static final int DECIMAL_WMAX = 3;
  public static final int SIZE_WIDEAL = 11;
  public static final int DECIMAL_WIDEAL = 3;
  public static final int SIZE_WCONSM = 11;
  public static final int DECIMAL_WCONSM = 3;
  public static final int SIZE_WDCC = 1;
  public static final int DECIMAL_WDCC = 0;
  public static final int SIZE_WDCA = 1;
  public static final int DECIMAL_WDCA = 0;
  public static final int SIZE_WDCS = 1;
  public static final int DECIMAL_WDCS = 0;
  public static final int SIZE_WARR = 1;
  public static final int SIZE_TOTALE_DS = 578;
  public static final int SIZE_FILLER = 550 - 578;
  
  // Constantes indices Nom DS
  public static final int VAR_WETB = 0;
  public static final int VAR_WART = 1;
  public static final int VAR_WLIB = 2;
  public static final int VAR_WLB1 = 3;
  public static final int VAR_WLB2 = 4;
  public static final int VAR_WLB3 = 5;
  public static final int VAR_WOBS = 6;
  public static final int VAR_WDAP = 7;
  public static final int VAR_WPRA = 8;
  public static final int VAR_WREM1 = 9;
  public static final int VAR_WREM2 = 10;
  public static final int VAR_WREM3 = 11;
  public static final int VAR_WREM4 = 12;
  public static final int VAR_WREM5 = 13;
  public static final int VAR_WREM6 = 14;
  public static final int VAR_WUNA = 15;
  public static final int VAR_WNUA = 16;
  public static final int VAR_WKUAUCA = 17;
  public static final int VAR_WDEL = 18;
  public static final int VAR_WREF = 19;
  public static final int VAR_WUCA = 20;
  public static final int VAR_WKUSUCA = 21;
  public static final int VAR_WUS = 22;
  public static final int VAR_WUCS = 23;
  public static final int VAR_WKUSUCS = 24;
  public static final int VAR_WKUCAUCS = 25;
  public static final int VAR_WDEV = 26;
  public static final int VAR_WQMI = 27;
  public static final int VAR_WQEC = 28;
  public static final int VAR_WDELS = 29;
  public static final int VAR_WKPR = 30;
  public static final int VAR_WTRL = 31;
  public static final int VAR_WRFC = 32;
  public static final int VAR_WGCD = 33;
  public static final int VAR_WDAF = 34;
  public static final int VAR_WLIBA = 35;
  public static final int VAR_WOPA = 36;
  public static final int VAR_WIN7 = 37;
  public static final int VAR_WNET = 38;
  public static final int VAR_WNETP = 39;
  public static final int VAR_WKAP = 40;
  public static final int VAR_WPRS = 41;
  public static final int VAR_WPRSA = 42;
  public static final int VAR_WFV1 = 43;
  public static final int VAR_WFK1 = 44;
  public static final int VAR_WFP1 = 45;
  public static final int VAR_WFU1 = 46;
  public static final int VAR_WFV2 = 47;
  public static final int VAR_WFK2 = 48;
  public static final int VAR_WFP2 = 49;
  public static final int VAR_WFU2 = 50;
  public static final int VAR_WFV3 = 51;
  public static final int VAR_WFK3 = 52;
  public static final int VAR_WFU3 = 53;
  public static final int VAR_WIN5 = 54;
  public static final int VAR_WSTK = 55;
  public static final int VAR_WRES = 56;
  public static final int VAR_WATT = 57;
  public static final int VAR_WDIV = 58;
  public static final int VAR_WDIA = 59;
  public static final int VAR_WMIN = 60;
  public static final int VAR_WMAX = 61;
  public static final int VAR_WIDEAL = 62;
  public static final int VAR_WCONSM = 63;
  public static final int VAR_WDCC = 64;
  public static final int VAR_WDCA = 65;
  public static final int VAR_WDCS = 66;
  public static final int VAR_WARR = 67;
  
  // Variables AS400
  private String wetb = ""; //
  private String wart = ""; // Code ARTICLE
  private String wlib = ""; // Libellé 1
  private String wlb1 = ""; // Libellé 2
  private String wlb2 = ""; // Libellé 3
  private String wlb3 = ""; // Libellé 4
  private String wobs = ""; // Observation
  private BigDecimal wdap = BigDecimal.ZERO; // Date application
  private BigDecimal wpra = BigDecimal.ZERO; // Prix catalogue
  private BigDecimal wrem1 = BigDecimal.ZERO; // Remise 1
  private BigDecimal wrem2 = BigDecimal.ZERO; // Remise 2
  private BigDecimal wrem3 = BigDecimal.ZERO; // Remise 3
  private BigDecimal wrem4 = BigDecimal.ZERO; // Remise 4
  private BigDecimal wrem5 = BigDecimal.ZERO; // Remise 5
  private BigDecimal wrem6 = BigDecimal.ZERO; // Remise 6
  private String wuna = ""; // Unité achat
  private BigDecimal wnua = BigDecimal.ZERO; // Conditionnement
  private BigDecimal wkuauca = BigDecimal.ZERO; // Nombre unité achat/unité Cde
  private BigDecimal wdel = BigDecimal.ZERO; // Délai de livraison
  private String wref = ""; // Référence fournisseur
  private String wuca = ""; // Unité de commande
  private BigDecimal wkusuca = BigDecimal.ZERO; // Nombre unité Stk/unité Cde
  private String wus = ""; // Unité de stocks
  private String wucs = ""; // Unité conditionnement stocks
  private BigDecimal wkusucs = BigDecimal.ZERO; // Nombre unité Stk/unité UCS
  private BigDecimal wkucaucs = BigDecimal.ZERO; // Nombre unité UCA/unité UCS
  private String wdev = ""; // Devise
  private BigDecimal wqmi = BigDecimal.ZERO; // Quantité minimum de commande
  private BigDecimal wqec = BigDecimal.ZERO; // Quantité économique
  private BigDecimal wdels = BigDecimal.ZERO; // Délai supplémentaire
  private BigDecimal wkpr = BigDecimal.ZERO; // Coeff. calcul prix
  private String wtrl = ""; // Type remise ligne, A=Ajout
  private String wrfc = ""; // Référence constructeur
  private BigDecimal wgcd = BigDecimal.ZERO; // Code gencod frs
  private BigDecimal wdaf = BigDecimal.ZERO; // Date limite validité
  private String wliba = ""; // Libellé
  private String wopa = ""; // origine
  private String win7 = ""; // Delai en Jours et Non Semain
  private BigDecimal wnet = BigDecimal.ZERO; // Prix Achat Net Hors Port
  private BigDecimal wnetp = BigDecimal.ZERO; // Prix Achat Net Port Inclus
  private BigDecimal wkap = BigDecimal.ZERO; // Coeff. approche fixe
  private BigDecimal wprs = BigDecimal.ZERO; // Prix revient standard en US
  private BigDecimal wprsa = BigDecimal.ZERO; // Prix revient standard en UA
  private BigDecimal wfv1 = BigDecimal.ZERO; // Frais 1 valeur
  private BigDecimal wfk1 = BigDecimal.ZERO; // Frais 1 coefficient
  private BigDecimal wfp1 = BigDecimal.ZERO; // Frais 1 % Port
  private String wfu1 = ""; // Unité frais 1
  private BigDecimal wfv2 = BigDecimal.ZERO; // Frais 2 valeur
  private BigDecimal wfk2 = BigDecimal.ZERO; // Frais 2 coefficient N.U
  private BigDecimal wfp2 = BigDecimal.ZERO; // Frais 2 % MAJORATION
  private String wfu2 = ""; // Unité frais 2
  private BigDecimal wfv3 = BigDecimal.ZERO; // Frais 3 valeur
  private BigDecimal wfk3 = BigDecimal.ZERO; // Frais 3 coefficient
  private String wfu3 = ""; // Unité frais 3
  private String win5 = ""; // 1=qté achat non multiple CND
  private BigDecimal wstk = BigDecimal.ZERO; // Quantité stock
  private BigDecimal wres = BigDecimal.ZERO; // Quantité réservée
  private BigDecimal watt = BigDecimal.ZERO; // Quantité attendue
  private BigDecimal wdiv = BigDecimal.ZERO; // Quantité disponible Vente
  private BigDecimal wdia = BigDecimal.ZERO; // Quantité disponible Achat
  private BigDecimal wmin = BigDecimal.ZERO; // stock mini
  private BigDecimal wmax = BigDecimal.ZERO; // stock max
  private BigDecimal wideal = BigDecimal.ZERO; // stock ideal S1QTE1
  private BigDecimal wconsm = BigDecimal.ZERO; // conso moyenne S1QTE2
  private BigDecimal wdcc = BigDecimal.ZERO; // Décimalisation QTC
  private BigDecimal wdca = BigDecimal.ZERO; // " " QTA
  private BigDecimal wdcs = BigDecimal.ZERO; // " " QTS
  private String warr = ""; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WETB), //
      new AS400Text(SIZE_WART), // Code ARTICLE
      new AS400Text(SIZE_WLIB), // Libellé 1
      new AS400Text(SIZE_WLB1), // Libellé 2
      new AS400Text(SIZE_WLB2), // Libellé 3
      new AS400Text(SIZE_WLB3), // Libellé 4
      new AS400Text(SIZE_WOBS), // Observation
      new AS400ZonedDecimal(SIZE_WDAP, DECIMAL_WDAP), // Date application
      new AS400PackedDecimal(SIZE_WPRA, DECIMAL_WPRA), // Prix catalogue
      new AS400ZonedDecimal(SIZE_WREM1, DECIMAL_WREM1), // Remise 1
      new AS400ZonedDecimal(SIZE_WREM2, DECIMAL_WREM2), // Remise 2
      new AS400ZonedDecimal(SIZE_WREM3, DECIMAL_WREM3), // Remise 3
      new AS400ZonedDecimal(SIZE_WREM4, DECIMAL_WREM4), // Remise 4
      new AS400ZonedDecimal(SIZE_WREM5, DECIMAL_WREM5), // Remise 5
      new AS400ZonedDecimal(SIZE_WREM6, DECIMAL_WREM6), // Remise 6
      new AS400Text(SIZE_WUNA), // Unité achat
      new AS400PackedDecimal(SIZE_WNUA, DECIMAL_WNUA), // Conditionnement
      new AS400PackedDecimal(SIZE_WKUAUCA, DECIMAL_WKUAUCA), // Nombre unité achat/unité Cde
      new AS400ZonedDecimal(SIZE_WDEL, DECIMAL_WDEL), // Délai de livraison
      new AS400Text(SIZE_WREF), // Référence fournisseur
      new AS400Text(SIZE_WUCA), // Unité de commande
      new AS400PackedDecimal(SIZE_WKUSUCA, DECIMAL_WKUSUCA), // Nombre unité Stk/unité Cde
      new AS400Text(SIZE_WUS), // Unité de stocks
      new AS400Text(SIZE_WUCS), // Unité conditionnement stocks
      new AS400PackedDecimal(SIZE_WKUSUCS, DECIMAL_WKUSUCS), // Nombre unité Stk/unité UCS
      new AS400PackedDecimal(SIZE_WKUCAUCS, DECIMAL_WKUCAUCS), // Nombre unité UCA/unité UCS
      new AS400Text(SIZE_WDEV), // Devise
      new AS400PackedDecimal(SIZE_WQMI, DECIMAL_WQMI), // Quantité minimum de commande
      new AS400PackedDecimal(SIZE_WQEC, DECIMAL_WQEC), // Quantité économique
      new AS400ZonedDecimal(SIZE_WDELS, DECIMAL_WDELS), // Délai supplémentaire
      new AS400PackedDecimal(SIZE_WKPR, DECIMAL_WKPR), // Coeff. calcul prix
      new AS400Text(SIZE_WTRL), // Type remise ligne, A=Ajout
      new AS400Text(SIZE_WRFC), // Référence constructeur
      new AS400ZonedDecimal(SIZE_WGCD, DECIMAL_WGCD), // Code gencod frs
      new AS400ZonedDecimal(SIZE_WDAF, DECIMAL_WDAF), // Date limite validité
      new AS400Text(SIZE_WLIBA), // Libellé
      new AS400Text(SIZE_WOPA), // origine
      new AS400Text(SIZE_WIN7), // Delai en Jours et Non Semain
      new AS400PackedDecimal(SIZE_WNET, DECIMAL_WNET), // Prix Achat Net Hors Port
      new AS400PackedDecimal(SIZE_WNETP, DECIMAL_WNETP), // Prix Achat Net Port Inclus
      new AS400ZonedDecimal(SIZE_WKAP, DECIMAL_WKAP), // Coeff. approche fixe
      new AS400PackedDecimal(SIZE_WPRS, DECIMAL_WPRS), // Prix revient standard en US
      new AS400PackedDecimal(SIZE_WPRSA, DECIMAL_WPRSA), // Prix revient standard en UA
      new AS400PackedDecimal(SIZE_WFV1, DECIMAL_WFV1), // Frais 1 valeur
      new AS400PackedDecimal(SIZE_WFK1, DECIMAL_WFK1), // Frais 1 coefficient
      new AS400ZonedDecimal(SIZE_WFP1, DECIMAL_WFP1), // Frais 1 % Port
      new AS400Text(SIZE_WFU1), // Unité frais 1
      new AS400PackedDecimal(SIZE_WFV2, DECIMAL_WFV2), // Frais 2 valeur
      new AS400PackedDecimal(SIZE_WFK2, DECIMAL_WFK2), // Frais 2 coefficient N.U
      new AS400PackedDecimal(SIZE_WFP2, DECIMAL_WFP2), // Frais 2 % MAJORATION
      new AS400Text(SIZE_WFU2), // Unité frais 2
      new AS400PackedDecimal(SIZE_WFV3, DECIMAL_WFV3), // Frais 3 valeur
      new AS400PackedDecimal(SIZE_WFK3, DECIMAL_WFK3), // Frais 3 coefficient
      new AS400Text(SIZE_WFU3), // Unité frais 3
      new AS400Text(SIZE_WIN5), // 1=qté achat non multiple CND
      new AS400ZonedDecimal(SIZE_WSTK, DECIMAL_WSTK), // Quantité stock
      new AS400ZonedDecimal(SIZE_WRES, DECIMAL_WRES), // Quantité réservée
      new AS400ZonedDecimal(SIZE_WATT, DECIMAL_WATT), // Quantité attendue
      new AS400ZonedDecimal(SIZE_WDIV, DECIMAL_WDIV), // Quantité disponible Vente
      new AS400ZonedDecimal(SIZE_WDIA, DECIMAL_WDIA), // Quantité disponible Achat
      new AS400ZonedDecimal(SIZE_WMIN, DECIMAL_WMIN), // stock mini
      new AS400ZonedDecimal(SIZE_WMAX, DECIMAL_WMAX), // stock max
      new AS400ZonedDecimal(SIZE_WIDEAL, DECIMAL_WIDEAL), // stock ideal S1QTE1
      new AS400ZonedDecimal(SIZE_WCONSM, DECIMAL_WCONSM), // conso moyenne S1QTE2
      new AS400ZonedDecimal(SIZE_WDCC, DECIMAL_WDCC), // Décimalisation QTC
      new AS400ZonedDecimal(SIZE_WDCA, DECIMAL_WDCA), // " " QTA
      new AS400ZonedDecimal(SIZE_WDCS, DECIMAL_WDCS), // " " QTS
      new AS400Text(SIZE_WARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  public Object[] o = { wetb, wart, wlib, wlb1, wlb2, wlb3, wobs, wdap, wpra, wrem1, wrem2, wrem3, wrem4, wrem5, wrem6, wuna, wnua,
      wkuauca, wdel, wref, wuca, wkusuca, wus, wucs, wkusucs, wkucaucs, wdev, wqmi, wqec, wdels, wkpr, wtrl, wrfc, wgcd, wdaf, wliba,
      wopa, win7, wnet, wnetp, wkap, wprs, wprsa, wfv1, wfk1, wfp1, wfu1, wfv2, wfk2, wfp2, wfu2, wfv3, wfk3, wfu3, win5, wstk, wres,
      watt, wdiv, wdia, wmin, wmax, wideal, wconsm, wdcc, wdca, wdcs, warr, };
  
  // -- Accesseurs
  
  public void setWetb(String pWetb) {
    if (pWetb == null) {
      return;
    }
    wetb = pWetb;
  }
  
  public String getWetb() {
    return wetb;
  }
  
  public void setWart(String pWart) {
    if (pWart == null) {
      return;
    }
    wart = pWart;
  }
  
  public String getWart() {
    return wart;
  }
  
  public void setWlib(String pWlib) {
    if (pWlib == null) {
      return;
    }
    wlib = pWlib;
  }
  
  public String getWlib() {
    return wlib;
  }
  
  public void setWlb1(String pWlb1) {
    if (pWlb1 == null) {
      return;
    }
    wlb1 = pWlb1;
  }
  
  public String getWlb1() {
    return wlb1;
  }
  
  public void setWlb2(String pWlb2) {
    if (pWlb2 == null) {
      return;
    }
    wlb2 = pWlb2;
  }
  
  public String getWlb2() {
    return wlb2;
  }
  
  public void setWlb3(String pWlb3) {
    if (pWlb3 == null) {
      return;
    }
    wlb3 = pWlb3;
  }
  
  public String getWlb3() {
    return wlb3;
  }
  
  public void setWobs(String pWobs) {
    if (pWobs == null) {
      return;
    }
    wobs = pWobs;
  }
  
  public String getWobs() {
    return wobs;
  }
  
  public void setWdap(BigDecimal pWdap) {
    if (pWdap == null) {
      return;
    }
    wdap = pWdap.setScale(DECIMAL_WDAP, RoundingMode.HALF_UP);
  }
  
  public void setWdap(Integer pWdap) {
    if (pWdap == null) {
      return;
    }
    wdap = BigDecimal.valueOf(pWdap);
  }
  
  public void setWdap(Date pWdap) {
    if (pWdap == null) {
      return;
    }
    wdap = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdap));
  }
  
  public Integer getWdap() {
    return wdap.intValue();
  }
  
  public Date getWdapConvertiEnDate() {
    return ConvertDate.db2ToDate(wdap.intValue(), null);
  }
  
  public void setWpra(BigDecimal pWpra) {
    if (pWpra == null) {
      return;
    }
    wpra = pWpra.setScale(DECIMAL_WPRA, RoundingMode.HALF_UP);
  }
  
  public void setWpra(Double pWpra) {
    if (pWpra == null) {
      return;
    }
    wpra = BigDecimal.valueOf(pWpra).setScale(DECIMAL_WPRA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpra() {
    return wpra.setScale(DECIMAL_WPRA, RoundingMode.HALF_UP);
  }
  
  public void setWrem1(BigDecimal pWrem1) {
    if (pWrem1 == null) {
      return;
    }
    wrem1 = pWrem1.setScale(DECIMAL_WREM1, RoundingMode.HALF_UP);
  }
  
  public void setWrem1(Double pWrem1) {
    if (pWrem1 == null) {
      return;
    }
    wrem1 = BigDecimal.valueOf(pWrem1).setScale(DECIMAL_WREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWrem1() {
    return wrem1.setScale(DECIMAL_WREM1, RoundingMode.HALF_UP);
  }
  
  public void setWrem2(BigDecimal pWrem2) {
    if (pWrem2 == null) {
      return;
    }
    wrem2 = pWrem2.setScale(DECIMAL_WREM2, RoundingMode.HALF_UP);
  }
  
  public void setWrem2(Double pWrem2) {
    if (pWrem2 == null) {
      return;
    }
    wrem2 = BigDecimal.valueOf(pWrem2).setScale(DECIMAL_WREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWrem2() {
    return wrem2.setScale(DECIMAL_WREM2, RoundingMode.HALF_UP);
  }
  
  public void setWrem3(BigDecimal pWrem3) {
    if (pWrem3 == null) {
      return;
    }
    wrem3 = pWrem3.setScale(DECIMAL_WREM3, RoundingMode.HALF_UP);
  }
  
  public void setWrem3(Double pWrem3) {
    if (pWrem3 == null) {
      return;
    }
    wrem3 = BigDecimal.valueOf(pWrem3).setScale(DECIMAL_WREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWrem3() {
    return wrem3.setScale(DECIMAL_WREM3, RoundingMode.HALF_UP);
  }
  
  public void setWrem4(BigDecimal pWrem4) {
    if (pWrem4 == null) {
      return;
    }
    wrem4 = pWrem4.setScale(DECIMAL_WREM4, RoundingMode.HALF_UP);
  }
  
  public void setWrem4(Double pWrem4) {
    if (pWrem4 == null) {
      return;
    }
    wrem4 = BigDecimal.valueOf(pWrem4).setScale(DECIMAL_WREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWrem4() {
    return wrem4.setScale(DECIMAL_WREM4, RoundingMode.HALF_UP);
  }
  
  public void setWrem5(BigDecimal pWrem5) {
    if (pWrem5 == null) {
      return;
    }
    wrem5 = pWrem5.setScale(DECIMAL_WREM5, RoundingMode.HALF_UP);
  }
  
  public void setWrem5(Double pWrem5) {
    if (pWrem5 == null) {
      return;
    }
    wrem5 = BigDecimal.valueOf(pWrem5).setScale(DECIMAL_WREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWrem5() {
    return wrem5.setScale(DECIMAL_WREM5, RoundingMode.HALF_UP);
  }
  
  public void setWrem6(BigDecimal pWrem6) {
    if (pWrem6 == null) {
      return;
    }
    wrem6 = pWrem6.setScale(DECIMAL_WREM6, RoundingMode.HALF_UP);
  }
  
  public void setWrem6(Double pWrem6) {
    if (pWrem6 == null) {
      return;
    }
    wrem6 = BigDecimal.valueOf(pWrem6).setScale(DECIMAL_WREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWrem6() {
    return wrem6.setScale(DECIMAL_WREM6, RoundingMode.HALF_UP);
  }
  
  public void setWuna(String pWuna) {
    if (pWuna == null) {
      return;
    }
    wuna = pWuna;
  }
  
  public String getWuna() {
    return wuna;
  }
  
  public void setWnua(BigDecimal pWnua) {
    if (pWnua == null) {
      return;
    }
    wnua = pWnua.setScale(DECIMAL_WNUA, RoundingMode.HALF_UP);
  }
  
  public void setWnua(Double pWnua) {
    if (pWnua == null) {
      return;
    }
    wnua = BigDecimal.valueOf(pWnua).setScale(DECIMAL_WNUA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWnua() {
    return wnua.setScale(DECIMAL_WNUA, RoundingMode.HALF_UP);
  }
  
  public void setWkuauca(BigDecimal pWkuauca) {
    if (pWkuauca == null) {
      return;
    }
    wkuauca = pWkuauca.setScale(DECIMAL_WKUAUCA, RoundingMode.HALF_UP);
  }
  
  public void setWkuauca(Double pWkuauca) {
    if (pWkuauca == null) {
      return;
    }
    wkuauca = BigDecimal.valueOf(pWkuauca).setScale(DECIMAL_WKUAUCA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkuauca() {
    return wkuauca.setScale(DECIMAL_WKUAUCA, RoundingMode.HALF_UP);
  }
  
  public void setWdel(BigDecimal pWdel) {
    if (pWdel == null) {
      return;
    }
    wdel = pWdel.setScale(DECIMAL_WDEL, RoundingMode.HALF_UP);
  }
  
  public void setWdel(Integer pWdel) {
    if (pWdel == null) {
      return;
    }
    wdel = BigDecimal.valueOf(pWdel);
  }
  
  public Integer getWdel() {
    return wdel.intValue();
  }
  
  public void setWref(String pWref) {
    if (pWref == null) {
      return;
    }
    wref = pWref;
  }
  
  public String getWref() {
    return wref;
  }
  
  public void setWuca(String pWuca) {
    if (pWuca == null) {
      return;
    }
    wuca = pWuca;
  }
  
  public String getWuca() {
    return wuca;
  }
  
  public void setWkusuca(BigDecimal pWkusuca) {
    if (pWkusuca == null) {
      return;
    }
    wkusuca = pWkusuca.setScale(DECIMAL_WKUSUCA, RoundingMode.HALF_UP);
  }
  
  public void setWkusuca(Double pWkusuca) {
    if (pWkusuca == null) {
      return;
    }
    wkusuca = BigDecimal.valueOf(pWkusuca).setScale(DECIMAL_WKUSUCA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkusuca() {
    return wkusuca.setScale(DECIMAL_WKUSUCA, RoundingMode.HALF_UP);
  }
  
  public void setWus(String pWus) {
    if (pWus == null) {
      return;
    }
    wus = pWus;
  }
  
  public String getWus() {
    return wus;
  }
  
  public void setWucs(String pWucs) {
    if (pWucs == null) {
      return;
    }
    wucs = pWucs;
  }
  
  public String getWucs() {
    return wucs;
  }
  
  public void setWkusucs(BigDecimal pWkusucs) {
    if (pWkusucs == null) {
      return;
    }
    wkusucs = pWkusucs.setScale(DECIMAL_WKUSUCS, RoundingMode.HALF_UP);
  }
  
  public void setWkusucs(Double pWkusucs) {
    if (pWkusucs == null) {
      return;
    }
    wkusucs = BigDecimal.valueOf(pWkusucs).setScale(DECIMAL_WKUSUCS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkusucs() {
    return wkusucs.setScale(DECIMAL_WKUSUCS, RoundingMode.HALF_UP);
  }
  
  public void setWkucaucs(BigDecimal pWkucaucs) {
    if (pWkucaucs == null) {
      return;
    }
    wkucaucs = pWkucaucs.setScale(DECIMAL_WKUCAUCS, RoundingMode.HALF_UP);
  }
  
  public void setWkucaucs(Double pWkucaucs) {
    if (pWkucaucs == null) {
      return;
    }
    wkucaucs = BigDecimal.valueOf(pWkucaucs).setScale(DECIMAL_WKUCAUCS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkucaucs() {
    return wkucaucs.setScale(DECIMAL_WKUCAUCS, RoundingMode.HALF_UP);
  }
  
  public void setWdev(String pWdev) {
    if (pWdev == null) {
      return;
    }
    wdev = pWdev;
  }
  
  public String getWdev() {
    return wdev;
  }
  
  public void setWqmi(BigDecimal pWqmi) {
    if (pWqmi == null) {
      return;
    }
    wqmi = pWqmi.setScale(DECIMAL_WQMI, RoundingMode.HALF_UP);
  }
  
  public void setWqmi(Double pWqmi) {
    if (pWqmi == null) {
      return;
    }
    wqmi = BigDecimal.valueOf(pWqmi).setScale(DECIMAL_WQMI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqmi() {
    return wqmi.setScale(DECIMAL_WQMI, RoundingMode.HALF_UP);
  }
  
  public void setWqec(BigDecimal pWqec) {
    if (pWqec == null) {
      return;
    }
    wqec = pWqec.setScale(DECIMAL_WQEC, RoundingMode.HALF_UP);
  }
  
  public void setWqec(Double pWqec) {
    if (pWqec == null) {
      return;
    }
    wqec = BigDecimal.valueOf(pWqec).setScale(DECIMAL_WQEC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqec() {
    return wqec.setScale(DECIMAL_WQEC, RoundingMode.HALF_UP);
  }
  
  public void setWdels(BigDecimal pWdels) {
    if (pWdels == null) {
      return;
    }
    wdels = pWdels.setScale(DECIMAL_WDELS, RoundingMode.HALF_UP);
  }
  
  public void setWdels(Integer pWdels) {
    if (pWdels == null) {
      return;
    }
    wdels = BigDecimal.valueOf(pWdels);
  }
  
  public Integer getWdels() {
    return wdels.intValue();
  }
  
  public void setWkpr(BigDecimal pWkpr) {
    if (pWkpr == null) {
      return;
    }
    wkpr = pWkpr.setScale(DECIMAL_WKPR, RoundingMode.HALF_UP);
  }
  
  public void setWkpr(Double pWkpr) {
    if (pWkpr == null) {
      return;
    }
    wkpr = BigDecimal.valueOf(pWkpr).setScale(DECIMAL_WKPR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkpr() {
    return wkpr.setScale(DECIMAL_WKPR, RoundingMode.HALF_UP);
  }
  
  public void setWtrl(Character pWtrl) {
    if (pWtrl == null) {
      return;
    }
    wtrl = String.valueOf(pWtrl);
  }
  
  public Character getWtrl() {
    return wtrl.charAt(0);
  }
  
  public void setWrfc(String pWrfc) {
    if (pWrfc == null) {
      return;
    }
    wrfc = pWrfc;
  }
  
  public String getWrfc() {
    return wrfc;
  }
  
  public void setWgcd(BigDecimal pWgcd) {
    if (pWgcd == null) {
      return;
    }
    wgcd = pWgcd.setScale(DECIMAL_WGCD, RoundingMode.HALF_UP);
  }
  
  public void setWgcd(Integer pWgcd) {
    if (pWgcd == null) {
      return;
    }
    wgcd = BigDecimal.valueOf(pWgcd);
  }
  
  public Integer getWgcd() {
    return wgcd.intValue();
  }
  
  public void setWdaf(BigDecimal pWdaf) {
    if (pWdaf == null) {
      return;
    }
    wdaf = pWdaf.setScale(DECIMAL_WDAF, RoundingMode.HALF_UP);
  }
  
  public void setWdaf(Integer pWdaf) {
    if (pWdaf == null) {
      return;
    }
    wdaf = BigDecimal.valueOf(pWdaf);
  }
  
  public void setWdaf(Date pWdaf) {
    if (pWdaf == null) {
      return;
    }
    wdaf = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdaf));
  }
  
  public Integer getWdaf() {
    return wdaf.intValue();
  }
  
  public Date getWdafConvertiEnDate() {
    return ConvertDate.db2ToDate(wdaf.intValue(), null);
  }
  
  public void setWliba(String pWliba) {
    if (pWliba == null) {
      return;
    }
    wliba = pWliba;
  }
  
  public String getWliba() {
    return wliba;
  }
  
  public void setWopa(String pWopa) {
    if (pWopa == null) {
      return;
    }
    wopa = pWopa;
  }
  
  public String getWopa() {
    return wopa;
  }
  
  public void setWin7(Character pWin7) {
    if (pWin7 == null) {
      return;
    }
    win7 = String.valueOf(pWin7);
  }
  
  public Character getWin7() {
    return win7.charAt(0);
  }
  
  public void setWnet(BigDecimal pWnet) {
    if (pWnet == null) {
      return;
    }
    wnet = pWnet.setScale(DECIMAL_WNET, RoundingMode.HALF_UP);
  }
  
  public void setWnet(Double pWnet) {
    if (pWnet == null) {
      return;
    }
    wnet = BigDecimal.valueOf(pWnet).setScale(DECIMAL_WNET, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWnet() {
    return wnet.setScale(DECIMAL_WNET, RoundingMode.HALF_UP);
  }
  
  public void setWnetp(BigDecimal pWnetp) {
    if (pWnetp == null) {
      return;
    }
    wnetp = pWnetp.setScale(DECIMAL_WNETP, RoundingMode.HALF_UP);
  }
  
  public void setWnetp(Double pWnetp) {
    if (pWnetp == null) {
      return;
    }
    wnetp = BigDecimal.valueOf(pWnetp).setScale(DECIMAL_WNETP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWnetp() {
    return wnetp.setScale(DECIMAL_WNETP, RoundingMode.HALF_UP);
  }
  
  public void setWkap(BigDecimal pWkap) {
    if (pWkap == null) {
      return;
    }
    wkap = pWkap.setScale(DECIMAL_WKAP, RoundingMode.HALF_UP);
  }
  
  public void setWkap(Double pWkap) {
    if (pWkap == null) {
      return;
    }
    wkap = BigDecimal.valueOf(pWkap).setScale(DECIMAL_WKAP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkap() {
    return wkap.setScale(DECIMAL_WKAP, RoundingMode.HALF_UP);
  }
  
  public void setWprs(BigDecimal pWprs) {
    if (pWprs == null) {
      return;
    }
    wprs = pWprs.setScale(DECIMAL_WPRS, RoundingMode.HALF_UP);
  }
  
  public void setWprs(Double pWprs) {
    if (pWprs == null) {
      return;
    }
    wprs = BigDecimal.valueOf(pWprs).setScale(DECIMAL_WPRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprs() {
    return wprs.setScale(DECIMAL_WPRS, RoundingMode.HALF_UP);
  }
  
  public void setWprsa(BigDecimal pWprsa) {
    if (pWprsa == null) {
      return;
    }
    wprsa = pWprsa.setScale(DECIMAL_WPRSA, RoundingMode.HALF_UP);
  }
  
  public void setWprsa(Double pWprsa) {
    if (pWprsa == null) {
      return;
    }
    wprsa = BigDecimal.valueOf(pWprsa).setScale(DECIMAL_WPRSA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprsa() {
    return wprsa.setScale(DECIMAL_WPRSA, RoundingMode.HALF_UP);
  }
  
  public void setWfv1(BigDecimal pWfv1) {
    if (pWfv1 == null) {
      return;
    }
    wfv1 = pWfv1.setScale(DECIMAL_WFV1, RoundingMode.HALF_UP);
  }
  
  public void setWfv1(Double pWfv1) {
    if (pWfv1 == null) {
      return;
    }
    wfv1 = BigDecimal.valueOf(pWfv1).setScale(DECIMAL_WFV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfv1() {
    return wfv1.setScale(DECIMAL_WFV1, RoundingMode.HALF_UP);
  }
  
  public void setWfk1(BigDecimal pWfk1) {
    if (pWfk1 == null) {
      return;
    }
    wfk1 = pWfk1.setScale(DECIMAL_WFK1, RoundingMode.HALF_UP);
  }
  
  public void setWfk1(Double pWfk1) {
    if (pWfk1 == null) {
      return;
    }
    wfk1 = BigDecimal.valueOf(pWfk1).setScale(DECIMAL_WFK1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfk1() {
    return wfk1.setScale(DECIMAL_WFK1, RoundingMode.HALF_UP);
  }
  
  public void setWfp1(BigDecimal pWfp1) {
    if (pWfp1 == null) {
      return;
    }
    wfp1 = pWfp1.setScale(DECIMAL_WFP1, RoundingMode.HALF_UP);
  }
  
  public void setWfp1(Double pWfp1) {
    if (pWfp1 == null) {
      return;
    }
    wfp1 = BigDecimal.valueOf(pWfp1).setScale(DECIMAL_WFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfp1() {
    return wfp1.setScale(DECIMAL_WFP1, RoundingMode.HALF_UP);
  }
  
  public void setWfu1(Character pWfu1) {
    if (pWfu1 == null) {
      return;
    }
    wfu1 = String.valueOf(pWfu1);
  }
  
  public Character getWfu1() {
    return wfu1.charAt(0);
  }
  
  public void setWfv2(BigDecimal pWfv2) {
    if (pWfv2 == null) {
      return;
    }
    wfv2 = pWfv2.setScale(DECIMAL_WFV2, RoundingMode.HALF_UP);
  }
  
  public void setWfv2(Double pWfv2) {
    if (pWfv2 == null) {
      return;
    }
    wfv2 = BigDecimal.valueOf(pWfv2).setScale(DECIMAL_WFV2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfv2() {
    return wfv2.setScale(DECIMAL_WFV2, RoundingMode.HALF_UP);
  }
  
  public void setWfk2(BigDecimal pWfk2) {
    if (pWfk2 == null) {
      return;
    }
    wfk2 = pWfk2.setScale(DECIMAL_WFK2, RoundingMode.HALF_UP);
  }
  
  public void setWfk2(Double pWfk2) {
    if (pWfk2 == null) {
      return;
    }
    wfk2 = BigDecimal.valueOf(pWfk2).setScale(DECIMAL_WFK2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfk2() {
    return wfk2.setScale(DECIMAL_WFK2, RoundingMode.HALF_UP);
  }
  
  public void setWfp2(BigDecimal pWfp2) {
    if (pWfp2 == null) {
      return;
    }
    wfp2 = pWfp2.setScale(DECIMAL_WFP2, RoundingMode.HALF_UP);
  }
  
  public void setWfp2(Double pWfp2) {
    if (pWfp2 == null) {
      return;
    }
    wfp2 = BigDecimal.valueOf(pWfp2).setScale(DECIMAL_WFP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfp2() {
    return wfp2.setScale(DECIMAL_WFP2, RoundingMode.HALF_UP);
  }
  
  public void setWfu2(Character pWfu2) {
    if (pWfu2 == null) {
      return;
    }
    wfu2 = String.valueOf(pWfu2);
  }
  
  public Character getWfu2() {
    return wfu2.charAt(0);
  }
  
  public void setWfv3(BigDecimal pWfv3) {
    if (pWfv3 == null) {
      return;
    }
    wfv3 = pWfv3.setScale(DECIMAL_WFV3, RoundingMode.HALF_UP);
  }
  
  public void setWfv3(Double pWfv3) {
    if (pWfv3 == null) {
      return;
    }
    wfv3 = BigDecimal.valueOf(pWfv3).setScale(DECIMAL_WFV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfv3() {
    return wfv3.setScale(DECIMAL_WFV3, RoundingMode.HALF_UP);
  }
  
  public void setWfk3(BigDecimal pWfk3) {
    if (pWfk3 == null) {
      return;
    }
    wfk3 = pWfk3.setScale(DECIMAL_WFK3, RoundingMode.HALF_UP);
  }
  
  public void setWfk3(Double pWfk3) {
    if (pWfk3 == null) {
      return;
    }
    wfk3 = BigDecimal.valueOf(pWfk3).setScale(DECIMAL_WFK3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfk3() {
    return wfk3.setScale(DECIMAL_WFK3, RoundingMode.HALF_UP);
  }
  
  public void setWfu3(Character pWfu3) {
    if (pWfu3 == null) {
      return;
    }
    wfu3 = String.valueOf(pWfu3);
  }
  
  public Character getWfu3() {
    return wfu3.charAt(0);
  }
  
  public void setWin5(Character pWin5) {
    if (pWin5 == null) {
      return;
    }
    win5 = String.valueOf(pWin5);
  }
  
  public Character getWin5() {
    return win5.charAt(0);
  }
  
  public void setWstk(BigDecimal pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = pWstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWstk(Double pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = BigDecimal.valueOf(pWstk).setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWstk() {
    return wstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWres(BigDecimal pWres) {
    if (pWres == null) {
      return;
    }
    wres = pWres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWres(Double pWres) {
    if (pWres == null) {
      return;
    }
    wres = BigDecimal.valueOf(pWres).setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWres() {
    return wres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWatt(BigDecimal pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = pWatt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWatt(Double pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = BigDecimal.valueOf(pWatt).setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWatt() {
    return watt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWdiv(BigDecimal pWdiv) {
    if (pWdiv == null) {
      return;
    }
    wdiv = pWdiv.setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public void setWdiv(Double pWdiv) {
    if (pWdiv == null) {
      return;
    }
    wdiv = BigDecimal.valueOf(pWdiv).setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWdiv() {
    return wdiv.setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public void setWdia(BigDecimal pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = pWdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWdia(Double pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = BigDecimal.valueOf(pWdia).setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWdia() {
    return wdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWmin(BigDecimal pWmin) {
    if (pWmin == null) {
      return;
    }
    wmin = pWmin.setScale(DECIMAL_WMIN, RoundingMode.HALF_UP);
  }
  
  public void setWmin(Double pWmin) {
    if (pWmin == null) {
      return;
    }
    wmin = BigDecimal.valueOf(pWmin).setScale(DECIMAL_WMIN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmin() {
    return wmin.setScale(DECIMAL_WMIN, RoundingMode.HALF_UP);
  }
  
  public void setWmax(BigDecimal pWmax) {
    if (pWmax == null) {
      return;
    }
    wmax = pWmax.setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public void setWmax(Double pWmax) {
    if (pWmax == null) {
      return;
    }
    wmax = BigDecimal.valueOf(pWmax).setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmax() {
    return wmax.setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public void setWideal(BigDecimal pWideal) {
    if (pWideal == null) {
      return;
    }
    wideal = pWideal.setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public void setWideal(Double pWideal) {
    if (pWideal == null) {
      return;
    }
    wideal = BigDecimal.valueOf(pWideal).setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWideal() {
    return wideal.setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public void setWconsm(BigDecimal pWconsm) {
    if (pWconsm == null) {
      return;
    }
    wconsm = pWconsm.setScale(DECIMAL_WCONSM, RoundingMode.HALF_UP);
  }
  
  public void setWconsm(Double pWconsm) {
    if (pWconsm == null) {
      return;
    }
    wconsm = BigDecimal.valueOf(pWconsm).setScale(DECIMAL_WCONSM, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWconsm() {
    return wconsm.setScale(DECIMAL_WCONSM, RoundingMode.HALF_UP);
  }
  
  public void setWdcc(BigDecimal pWdcc) {
    if (pWdcc == null) {
      return;
    }
    wdcc = pWdcc.setScale(DECIMAL_WDCC, RoundingMode.HALF_UP);
  }
  
  public void setWdcc(Integer pWdcc) {
    if (pWdcc == null) {
      return;
    }
    wdcc = BigDecimal.valueOf(pWdcc);
  }
  
  public Integer getWdcc() {
    return wdcc.intValue();
  }
  
  public void setWdca(BigDecimal pWdca) {
    if (pWdca == null) {
      return;
    }
    wdca = pWdca.setScale(DECIMAL_WDCA, RoundingMode.HALF_UP);
  }
  
  public void setWdca(Integer pWdca) {
    if (pWdca == null) {
      return;
    }
    wdca = BigDecimal.valueOf(pWdca);
  }
  
  public Integer getWdca() {
    return wdca.intValue();
  }
  
  public void setWdcs(BigDecimal pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = pWdcs.setScale(DECIMAL_WDCS, RoundingMode.HALF_UP);
  }
  
  public void setWdcs(Integer pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = BigDecimal.valueOf(pWdcs);
  }
  
  public Integer getWdcs() {
    return wdcs.intValue();
  }
  
  public void setWarr(Character pWarr) {
    if (pWarr == null) {
      return;
    }
    warr = String.valueOf(pWarr);
  }
  
  public Character getWarr() {
    return warr.charAt(0);
  }
}
