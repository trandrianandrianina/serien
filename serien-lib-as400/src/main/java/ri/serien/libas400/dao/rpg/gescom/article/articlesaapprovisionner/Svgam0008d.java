/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlesaapprovisionner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0008d {
  // Constantes
  public static final int SIZE_WCOD = 1;
  public static final int SIZE_WETB = 3;
  public static final int SIZE_WNUM = 6;
  public static final int DECIMAL_WNUM = 0;
  public static final int SIZE_WSUF = 1;
  public static final int DECIMAL_WSUF = 0;
  public static final int SIZE_WNLI = 4;
  public static final int DECIMAL_WNLI = 0;
  public static final int SIZE_WNFA = 7;
  public static final int DECIMAL_WNFA = 0;
  public static final int SIZE_WCLLP = 6;
  public static final int DECIMAL_WCLLP = 0;
  public static final int SIZE_WCLLS = 3;
  public static final int DECIMAL_WCLLS = 0;
  public static final int SIZE_WNOM = 30;
  public static final int SIZE_WDLP = 7;
  public static final int DECIMAL_WDLP = 0;
  public static final int SIZE_WART = 20;
  public static final int SIZE_WLIB = 30;
  public static final int SIZE_WUCA = 2;
  public static final int SIZE_WUA = 2;
  public static final int SIZE_WKUAUCA = 8;
  public static final int DECIMAL_WKUAUCA = 3;
  public static final int SIZE_WKUSUCA = 8;
  public static final int DECIMAL_WKUSUCA = 3;
  public static final int SIZE_WNPU = 1;
  public static final int SIZE_WTSP = 1;
  public static final int SIZE_WABC = 1;
  public static final int SIZE_WSPE = 1;
  public static final int DECIMAL_WSPE = 0;
  public static final int SIZE_WUS = 2;
  public static final int SIZE_WUCS = 2;
  public static final int SIZE_WKUSUCS = 8;
  public static final int DECIMAL_WKUSUCS = 3;
  public static final int SIZE_WKUCAUCS = 8;
  public static final int DECIMAL_WKUCAUCS = 3;
  public static final int SIZE_WQTC = 11;
  public static final int DECIMAL_WQTC = 3;
  public static final int SIZE_WSTK = 11;
  public static final int DECIMAL_WSTK = 3;
  public static final int SIZE_WRES = 11;
  public static final int DECIMAL_WRES = 3;
  public static final int SIZE_WAFF = 11;
  public static final int DECIMAL_WAFF = 3;
  public static final int SIZE_WATT = 11;
  public static final int DECIMAL_WATT = 3;
  public static final int SIZE_WDIV = 11;
  public static final int DECIMAL_WDIV = 3;
  public static final int SIZE_WDIA = 11;
  public static final int DECIMAL_WDIA = 3;
  public static final int SIZE_WMAX = 11;
  public static final int DECIMAL_WMAX = 3;
  public static final int SIZE_WMINI = 11;
  public static final int DECIMAL_WMINI = 3;
  public static final int SIZE_WIDEAL = 11;
  public static final int DECIMAL_WIDEAL = 3;
  public static final int SIZE_WCONSM = 11;
  public static final int DECIMAL_WCONSM = 3;
  public static final int SIZE_WDCC = 1;
  public static final int DECIMAL_WDCC = 0;
  public static final int SIZE_WDCA = 1;
  public static final int DECIMAL_WDCA = 0;
  public static final int SIZE_WDCS = 1;
  public static final int DECIMAL_WDCS = 0;
  public static final int SIZE_WA1STK = 1;
  public static final int DECIMAL_WA1STK = 0;
  public static final int SIZE_TOTALE_DS = 287;
  public static final int SIZE_FILLER = 400 - 287;
  
  // Constantes indices Nom DS
  public static final int VAR_WCOD = 0;
  public static final int VAR_WETB = 1;
  public static final int VAR_WNUM = 2;
  public static final int VAR_WSUF = 3;
  public static final int VAR_WNLI = 4;
  public static final int VAR_WNFA = 5;
  public static final int VAR_WCLLP = 6;
  public static final int VAR_WCLLS = 7;
  public static final int VAR_WNOM = 8;
  public static final int VAR_WDLP = 9;
  public static final int VAR_WART = 10;
  public static final int VAR_WLIB = 11;
  public static final int VAR_WUCA = 12;
  public static final int VAR_WUA = 13;
  public static final int VAR_WKUAUCA = 14;
  public static final int VAR_WKUSUCA = 15;
  public static final int VAR_WNPU = 16;
  public static final int VAR_WTSP = 17;
  public static final int VAR_WABC = 18;
  public static final int VAR_WSPE = 19;
  public static final int VAR_WUS = 20;
  public static final int VAR_WUCS = 21;
  public static final int VAR_WKUSUCS = 22;
  public static final int VAR_WKUCAUCS = 23;
  public static final int VAR_WQTC = 24;
  public static final int VAR_WSTK = 25;
  public static final int VAR_WRES = 26;
  public static final int VAR_WAFF = 27;
  public static final int VAR_WATT = 28;
  public static final int VAR_WDIV = 29;
  public static final int VAR_WDIA = 30;
  public static final int VAR_WMAX = 31;
  public static final int VAR_WMINI = 32;
  public static final int VAR_WIDEAL = 33;
  public static final int VAR_WCONSM = 34;
  public static final int VAR_WDCC = 35;
  public static final int VAR_WDCA = 36;
  public static final int VAR_WDCS = 37;
  public static final int VAR_WA1STK = 38;
  
  // Variables AS400
  private String wcod = ""; // Code ERL "E" vente
  private String wetb = ""; // Code Etablissement
  private BigDecimal wnum = BigDecimal.ZERO; // Numéro de Bon vente
  private BigDecimal wsuf = BigDecimal.ZERO; // Suffixe du Bon vente
  private BigDecimal wnli = BigDecimal.ZERO; // Numéro de Ligne vente
  private BigDecimal wnfa = BigDecimal.ZERO; // Numéro de facture vente
  private BigDecimal wcllp = BigDecimal.ZERO; // Code Client Livré
  private BigDecimal wclls = BigDecimal.ZERO; // Code Client Livré suffixe
  private String wnom = ""; // Raison sociale
  private BigDecimal wdlp = BigDecimal.ZERO; // Date de livraison prévue
  private String wart = ""; // Code article
  private String wlib = ""; // Libellé article
  private String wuca = ""; // Code unité de commande achat
  private String wua = ""; // Code unité d"achat
  private BigDecimal wkuauca = BigDecimal.ZERO; // Nombre unité achat/unité Cde
  private BigDecimal wkusuca = BigDecimal.ZERO; // Nombre unité Stk/unité Cde
  private String wnpu = ""; // Top ne plus utiliser
  private String wtsp = ""; // Top (2,3,V,S ou *)
  private String wabc = ""; // Code ABC,Hors gamme = R
  private BigDecimal wspe = BigDecimal.ZERO; // Code spécial, = 1 SPÉCIAL
  private String wus = ""; // Unité de stocks
  private String wucs = ""; // Unité conditionnement stocks
  private BigDecimal wkusucs = BigDecimal.ZERO; // Nombre unité Stk/unité UCS
  private BigDecimal wkucaucs = BigDecimal.ZERO; // Nombre unité UCA/unité UCS
  private BigDecimal wqtc = BigDecimal.ZERO; // Qté commandée ligne de vente
  private BigDecimal wstk = BigDecimal.ZERO; // Qté stock
  private BigDecimal wres = BigDecimal.ZERO; // Qté commandes clients
  private BigDecimal waff = BigDecimal.ZERO; // Qté réservées clients
  private BigDecimal watt = BigDecimal.ZERO; // Qté attendue
  private BigDecimal wdiv = BigDecimal.ZERO; // Qté disponible VENTE
  private BigDecimal wdia = BigDecimal.ZERO; // Qté disponible ACHAT
  private BigDecimal wmax = BigDecimal.ZERO; // Qté stock maximum
  private BigDecimal wmini = BigDecimal.ZERO; // Stock mini
  private BigDecimal wideal = BigDecimal.ZERO; // Stock idéal (lot de cmnde)
  private BigDecimal wconsm = BigDecimal.ZERO; // Conso moyenne mensuelle
  private BigDecimal wdcc = BigDecimal.ZERO; // Décimalisation QTC
  private BigDecimal wdca = BigDecimal.ZERO; // " " QTA
  private BigDecimal wdcs = BigDecimal.ZERO; // " " QTS
  private BigDecimal wa1stk = BigDecimal.ZERO; // = 1 -> Non génére en stock
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WCOD), // Code ERL "E" vente
      new AS400Text(SIZE_WETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_WNUM, DECIMAL_WNUM), // Numéro de Bon vente
      new AS400ZonedDecimal(SIZE_WSUF, DECIMAL_WSUF), // Suffixe du Bon vente
      new AS400ZonedDecimal(SIZE_WNLI, DECIMAL_WNLI), // Numéro de Ligne vente
      new AS400ZonedDecimal(SIZE_WNFA, DECIMAL_WNFA), // Numéro de facture vente
      new AS400ZonedDecimal(SIZE_WCLLP, DECIMAL_WCLLP), // Code Client Livré
      new AS400ZonedDecimal(SIZE_WCLLS, DECIMAL_WCLLS), // Code Client Livré suffixe
      new AS400Text(SIZE_WNOM), // Raison sociale
      new AS400ZonedDecimal(SIZE_WDLP, DECIMAL_WDLP), // Date de livraison prévue
      new AS400Text(SIZE_WART), // Code article
      new AS400Text(SIZE_WLIB), // Libellé article
      new AS400Text(SIZE_WUCA), // Code unité de commande achat
      new AS400Text(SIZE_WUA), // Code unité d"achat
      new AS400PackedDecimal(SIZE_WKUAUCA, DECIMAL_WKUAUCA), // Nombre unité achat/unité Cde
      new AS400PackedDecimal(SIZE_WKUSUCA, DECIMAL_WKUSUCA), // Nombre unité Stk/unité Cde
      new AS400Text(SIZE_WNPU), // Top ne plus utiliser
      new AS400Text(SIZE_WTSP), // Top (2,3,V,S ou *)
      new AS400Text(SIZE_WABC), // Code ABC,Hors gamme = R
      new AS400ZonedDecimal(SIZE_WSPE, DECIMAL_WSPE), // Code spécial, = 1 SPÉCIAL
      new AS400Text(SIZE_WUS), // Unité de stocks
      new AS400Text(SIZE_WUCS), // Unité conditionnement stocks
      new AS400PackedDecimal(SIZE_WKUSUCS, DECIMAL_WKUSUCS), // Nombre unité Stk/unité UCS
      new AS400PackedDecimal(SIZE_WKUCAUCS, DECIMAL_WKUCAUCS), // Nombre unité UCA/unité UCS
      new AS400ZonedDecimal(SIZE_WQTC, DECIMAL_WQTC), // Qté commandée ligne de vente
      new AS400ZonedDecimal(SIZE_WSTK, DECIMAL_WSTK), // Qté stock
      new AS400ZonedDecimal(SIZE_WRES, DECIMAL_WRES), // Qté commandes clients
      new AS400ZonedDecimal(SIZE_WAFF, DECIMAL_WAFF), // Qté réservées clients
      new AS400ZonedDecimal(SIZE_WATT, DECIMAL_WATT), // Qté attendue
      new AS400ZonedDecimal(SIZE_WDIV, DECIMAL_WDIV), // Qté disponible VENTE
      new AS400ZonedDecimal(SIZE_WDIA, DECIMAL_WDIA), // Qté disponible ACHAT
      new AS400ZonedDecimal(SIZE_WMAX, DECIMAL_WMAX), // Qté stock maximum
      new AS400ZonedDecimal(SIZE_WMINI, DECIMAL_WMINI), // Stock mini
      new AS400ZonedDecimal(SIZE_WIDEAL, DECIMAL_WIDEAL), // Stock idéal (lot de cmnde)
      new AS400ZonedDecimal(SIZE_WCONSM, DECIMAL_WCONSM), // Conso moyenne mensuelle
      new AS400ZonedDecimal(SIZE_WDCC, DECIMAL_WDCC), // Décimalisation QTC
      new AS400ZonedDecimal(SIZE_WDCA, DECIMAL_WDCA), // " " QTA
      new AS400ZonedDecimal(SIZE_WDCS, DECIMAL_WDCS), // " " QTS
      new AS400ZonedDecimal(SIZE_WA1STK, DECIMAL_WA1STK), // = 1 -> Non génére en stock
      new AS400Text(SIZE_FILLER), // Filler
  };
  public Object[] o = { wcod, wetb, wnum, wsuf, wnli, wnfa, wcllp, wclls, wnom, wdlp, wart, wlib, wuca, wua, wkuauca, wkusuca, wnpu, wtsp,
      wabc, wspe, wus, wucs, wkusucs, wkucaucs, wqtc, wstk, wres, waff, watt, wdiv, wdia, wmax, wmini, wideal, wconsm, wdcc, wdca, wdcs,
      wa1stk, filler, };
  
  // -- Accesseurs
  
  public void setWcod(Character pWcod) {
    if (pWcod == null) {
      return;
    }
    wcod = String.valueOf(pWcod);
  }
  
  public Character getWcod() {
    return wcod.charAt(0);
  }
  
  public void setWetb(String pWetb) {
    if (pWetb == null) {
      return;
    }
    wetb = pWetb;
  }
  
  public String getWetb() {
    return wetb;
  }
  
  public void setWnum(BigDecimal pWnum) {
    if (pWnum == null) {
      return;
    }
    wnum = pWnum.setScale(DECIMAL_WNUM, RoundingMode.HALF_UP);
  }
  
  public void setWnum(Integer pWnum) {
    if (pWnum == null) {
      return;
    }
    wnum = BigDecimal.valueOf(pWnum);
  }
  
  public Integer getWnum() {
    return wnum.intValue();
  }
  
  public void setWsuf(BigDecimal pWsuf) {
    if (pWsuf == null) {
      return;
    }
    wsuf = pWsuf.setScale(DECIMAL_WSUF, RoundingMode.HALF_UP);
  }
  
  public void setWsuf(Integer pWsuf) {
    if (pWsuf == null) {
      return;
    }
    wsuf = BigDecimal.valueOf(pWsuf);
  }
  
  public Integer getWsuf() {
    return wsuf.intValue();
  }
  
  public void setWnli(BigDecimal pWnli) {
    if (pWnli == null) {
      return;
    }
    wnli = pWnli.setScale(DECIMAL_WNLI, RoundingMode.HALF_UP);
  }
  
  public void setWnli(Integer pWnli) {
    if (pWnli == null) {
      return;
    }
    wnli = BigDecimal.valueOf(pWnli);
  }
  
  public Integer getWnli() {
    return wnli.intValue();
  }
  
  public void setWnfa(BigDecimal pWnfa) {
    if (pWnfa == null) {
      return;
    }
    wnfa = pWnfa.setScale(DECIMAL_WNFA, RoundingMode.HALF_UP);
  }
  
  public void setWnfa(Integer pWnfa) {
    if (pWnfa == null) {
      return;
    }
    wnfa = BigDecimal.valueOf(pWnfa);
  }
  
  public void setWnfa(Date pWnfa) {
    if (pWnfa == null) {
      return;
    }
    wnfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pWnfa));
  }
  
  public Integer getWnfa() {
    return wnfa.intValue();
  }
  
  public Date getWnfaConvertiEnDate() {
    return ConvertDate.db2ToDate(wnfa.intValue(), null);
  }
  
  public void setWcllp(BigDecimal pWcllp) {
    if (pWcllp == null) {
      return;
    }
    wcllp = pWcllp.setScale(DECIMAL_WCLLP, RoundingMode.HALF_UP);
  }
  
  public void setWcllp(Integer pWcllp) {
    if (pWcllp == null) {
      return;
    }
    wcllp = BigDecimal.valueOf(pWcllp);
  }
  
  public Integer getWcllp() {
    return wcllp.intValue();
  }
  
  public void setWclls(BigDecimal pWclls) {
    if (pWclls == null) {
      return;
    }
    wclls = pWclls.setScale(DECIMAL_WCLLS, RoundingMode.HALF_UP);
  }
  
  public void setWclls(Integer pWclls) {
    if (pWclls == null) {
      return;
    }
    wclls = BigDecimal.valueOf(pWclls);
  }
  
  public Integer getWclls() {
    return wclls.intValue();
  }
  
  public void setWnom(String pWnom) {
    if (pWnom == null) {
      return;
    }
    wnom = pWnom;
  }
  
  public String getWnom() {
    return wnom;
  }
  
  public void setWdlp(BigDecimal pWdlp) {
    if (pWdlp == null) {
      return;
    }
    wdlp = pWdlp.setScale(DECIMAL_WDLP, RoundingMode.HALF_UP);
  }
  
  public void setWdlp(Integer pWdlp) {
    if (pWdlp == null) {
      return;
    }
    wdlp = BigDecimal.valueOf(pWdlp);
  }
  
  public void setWdlp(Date pWdlp) {
    if (pWdlp == null) {
      return;
    }
    wdlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdlp));
  }
  
  public Integer getWdlp() {
    return wdlp.intValue();
  }
  
  public Date getWdlpConvertiEnDate() {
    return ConvertDate.db2ToDate(wdlp.intValue(), null);
  }
  
  public void setWart(String pWart) {
    if (pWart == null) {
      return;
    }
    wart = pWart;
  }
  
  public String getWart() {
    return wart;
  }
  
  public void setWlib(String pWlib) {
    if (pWlib == null) {
      return;
    }
    wlib = pWlib;
  }
  
  public String getWlib() {
    return wlib;
  }
  
  public void setWuca(String pWuca) {
    if (pWuca == null) {
      return;
    }
    wuca = pWuca;
  }
  
  public String getWuca() {
    return wuca;
  }
  
  public void setWua(String pWua) {
    if (pWua == null) {
      return;
    }
    wua = pWua;
  }
  
  public String getWua() {
    return wua;
  }
  
  public void setWkuauca(BigDecimal pWkuauca) {
    if (pWkuauca == null) {
      return;
    }
    wkuauca = pWkuauca.setScale(DECIMAL_WKUAUCA, RoundingMode.HALF_UP);
  }
  
  public void setWkuauca(Double pWkuauca) {
    if (pWkuauca == null) {
      return;
    }
    wkuauca = BigDecimal.valueOf(pWkuauca).setScale(DECIMAL_WKUAUCA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkuauca() {
    return wkuauca.setScale(DECIMAL_WKUAUCA, RoundingMode.HALF_UP);
  }
  
  public void setWkusuca(BigDecimal pWkusuca) {
    if (pWkusuca == null) {
      return;
    }
    wkusuca = pWkusuca.setScale(DECIMAL_WKUSUCA, RoundingMode.HALF_UP);
  }
  
  public void setWkusuca(Double pWkusuca) {
    if (pWkusuca == null) {
      return;
    }
    wkusuca = BigDecimal.valueOf(pWkusuca).setScale(DECIMAL_WKUSUCA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkusuca() {
    return wkusuca.setScale(DECIMAL_WKUSUCA, RoundingMode.HALF_UP);
  }
  
  public void setWnpu(Character pWnpu) {
    if (pWnpu == null) {
      return;
    }
    wnpu = String.valueOf(pWnpu);
  }
  
  public Character getWnpu() {
    return wnpu.charAt(0);
  }
  
  public void setWtsp(Character pWtsp) {
    if (pWtsp == null) {
      return;
    }
    wtsp = String.valueOf(pWtsp);
  }
  
  public Character getWtsp() {
    return wtsp.charAt(0);
  }
  
  public void setWabc(Character pWabc) {
    if (pWabc == null) {
      return;
    }
    wabc = String.valueOf(pWabc);
  }
  
  public Character getWabc() {
    return wabc.charAt(0);
  }
  
  public void setWspe(BigDecimal pWspe) {
    if (pWspe == null) {
      return;
    }
    wspe = pWspe.setScale(DECIMAL_WSPE, RoundingMode.HALF_UP);
  }
  
  public void setWspe(Integer pWspe) {
    if (pWspe == null) {
      return;
    }
    wspe = BigDecimal.valueOf(pWspe);
  }
  
  public Integer getWspe() {
    return wspe.intValue();
  }
  
  public void setWus(String pWus) {
    if (pWus == null) {
      return;
    }
    wus = pWus;
  }
  
  public String getWus() {
    return wus;
  }
  
  public void setWucs(String pWucs) {
    if (pWucs == null) {
      return;
    }
    wucs = pWucs;
  }
  
  public String getWucs() {
    return wucs;
  }
  
  public void setWkusucs(BigDecimal pWkusucs) {
    if (pWkusucs == null) {
      return;
    }
    wkusucs = pWkusucs.setScale(DECIMAL_WKUSUCS, RoundingMode.HALF_UP);
  }
  
  public void setWkusucs(Double pWkusucs) {
    if (pWkusucs == null) {
      return;
    }
    wkusucs = BigDecimal.valueOf(pWkusucs).setScale(DECIMAL_WKUSUCS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkusucs() {
    return wkusucs.setScale(DECIMAL_WKUSUCS, RoundingMode.HALF_UP);
  }
  
  public void setWkucaucs(BigDecimal pWkucaucs) {
    if (pWkucaucs == null) {
      return;
    }
    wkucaucs = pWkucaucs.setScale(DECIMAL_WKUCAUCS, RoundingMode.HALF_UP);
  }
  
  public void setWkucaucs(Double pWkucaucs) {
    if (pWkucaucs == null) {
      return;
    }
    wkucaucs = BigDecimal.valueOf(pWkucaucs).setScale(DECIMAL_WKUCAUCS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkucaucs() {
    return wkucaucs.setScale(DECIMAL_WKUCAUCS, RoundingMode.HALF_UP);
  }
  
  public void setWqtc(BigDecimal pWqtc) {
    if (pWqtc == null) {
      return;
    }
    wqtc = pWqtc.setScale(DECIMAL_WQTC, RoundingMode.HALF_UP);
  }
  
  public void setWqtc(Double pWqtc) {
    if (pWqtc == null) {
      return;
    }
    wqtc = BigDecimal.valueOf(pWqtc).setScale(DECIMAL_WQTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqtc() {
    return wqtc.setScale(DECIMAL_WQTC, RoundingMode.HALF_UP);
  }
  
  public void setWstk(BigDecimal pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = pWstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWstk(Double pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = BigDecimal.valueOf(pWstk).setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWstk() {
    return wstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWres(BigDecimal pWres) {
    if (pWres == null) {
      return;
    }
    wres = pWres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWres(Double pWres) {
    if (pWres == null) {
      return;
    }
    wres = BigDecimal.valueOf(pWres).setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWres() {
    return wres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWaff(BigDecimal pWaff) {
    if (pWaff == null) {
      return;
    }
    waff = pWaff.setScale(DECIMAL_WAFF, RoundingMode.HALF_UP);
  }
  
  public void setWaff(Double pWaff) {
    if (pWaff == null) {
      return;
    }
    waff = BigDecimal.valueOf(pWaff).setScale(DECIMAL_WAFF, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWaff() {
    return waff.setScale(DECIMAL_WAFF, RoundingMode.HALF_UP);
  }
  
  public void setWatt(BigDecimal pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = pWatt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWatt(Double pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = BigDecimal.valueOf(pWatt).setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWatt() {
    return watt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWdiv(BigDecimal pWdiv) {
    if (pWdiv == null) {
      return;
    }
    wdiv = pWdiv.setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public void setWdiv(Double pWdiv) {
    if (pWdiv == null) {
      return;
    }
    wdiv = BigDecimal.valueOf(pWdiv).setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWdiv() {
    return wdiv.setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public void setWdia(BigDecimal pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = pWdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWdia(Double pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = BigDecimal.valueOf(pWdia).setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWdia() {
    return wdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWmax(BigDecimal pWmax) {
    if (pWmax == null) {
      return;
    }
    wmax = pWmax.setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public void setWmax(Double pWmax) {
    if (pWmax == null) {
      return;
    }
    wmax = BigDecimal.valueOf(pWmax).setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmax() {
    return wmax.setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public void setWmini(BigDecimal pWmini) {
    if (pWmini == null) {
      return;
    }
    wmini = pWmini.setScale(DECIMAL_WMINI, RoundingMode.HALF_UP);
  }
  
  public void setWmini(Double pWmini) {
    if (pWmini == null) {
      return;
    }
    wmini = BigDecimal.valueOf(pWmini).setScale(DECIMAL_WMINI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmini() {
    return wmini.setScale(DECIMAL_WMINI, RoundingMode.HALF_UP);
  }
  
  public void setWideal(BigDecimal pWideal) {
    if (pWideal == null) {
      return;
    }
    wideal = pWideal.setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public void setWideal(Double pWideal) {
    if (pWideal == null) {
      return;
    }
    wideal = BigDecimal.valueOf(pWideal).setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWideal() {
    return wideal.setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public void setWconsm(BigDecimal pWconsm) {
    if (pWconsm == null) {
      return;
    }
    wconsm = pWconsm.setScale(DECIMAL_WCONSM, RoundingMode.HALF_UP);
  }
  
  public void setWconsm(Double pWconsm) {
    if (pWconsm == null) {
      return;
    }
    wconsm = BigDecimal.valueOf(pWconsm).setScale(DECIMAL_WCONSM, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWconsm() {
    return wconsm.setScale(DECIMAL_WCONSM, RoundingMode.HALF_UP);
  }
  
  public void setWdcc(BigDecimal pWdcc) {
    if (pWdcc == null) {
      return;
    }
    wdcc = pWdcc.setScale(DECIMAL_WDCC, RoundingMode.HALF_UP);
  }
  
  public void setWdcc(Integer pWdcc) {
    if (pWdcc == null) {
      return;
    }
    wdcc = BigDecimal.valueOf(pWdcc);
  }
  
  public Integer getWdcc() {
    return wdcc.intValue();
  }
  
  public void setWdca(BigDecimal pWdca) {
    if (pWdca == null) {
      return;
    }
    wdca = pWdca.setScale(DECIMAL_WDCA, RoundingMode.HALF_UP);
  }
  
  public void setWdca(Integer pWdca) {
    if (pWdca == null) {
      return;
    }
    wdca = BigDecimal.valueOf(pWdca);
  }
  
  public Integer getWdca() {
    return wdca.intValue();
  }
  
  public void setWdcs(BigDecimal pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = pWdcs.setScale(DECIMAL_WDCS, RoundingMode.HALF_UP);
  }
  
  public void setWdcs(Integer pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = BigDecimal.valueOf(pWdcs);
  }
  
  public Integer getWdcs() {
    return wdcs.intValue();
  }
  
  public void setWa1stk(BigDecimal pWa1stk) {
    if (pWa1stk == null) {
      return;
    }
    wa1stk = pWa1stk.setScale(DECIMAL_WA1STK, RoundingMode.HALF_UP);
  }
  
  public void setWa1stk(Integer pWa1stk) {
    if (pWa1stk == null) {
      return;
    }
    wa1stk = BigDecimal.valueOf(pWa1stk);
  }
  
  public Integer getWa1stk() {
    return wa1stk.intValue();
  }
}
