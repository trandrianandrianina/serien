/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.standard;

/**
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */
import java.beans.PropertyVetoException;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgam0002o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POA01 = 550;
  public static final int SIZE_POA02 = 550;
  public static final int SIZE_POA03 = 550;
  public static final int SIZE_POA04 = 550;
  public static final int SIZE_POA05 = 550;
  public static final int SIZE_POA06 = 550;
  public static final int SIZE_POA07 = 550;
  public static final int SIZE_POA08 = 550;
  public static final int SIZE_POA09 = 550;
  public static final int SIZE_POA10 = 550;
  public static final int SIZE_POA11 = 550;
  public static final int SIZE_POA12 = 550;
  public static final int SIZE_POA13 = 550;
  public static final int SIZE_POA14 = 550;
  public static final int SIZE_POA15 = 550;
  public static final int SIZE_POA16 = 550;
  public static final int SIZE_POA17 = 550;
  public static final int SIZE_POA18 = 550;
  public static final int SIZE_POA19 = 550;
  public static final int SIZE_POA20 = 550;
  public static final int SIZE_POA21 = 550;
  public static final int SIZE_POA22 = 550;
  public static final int SIZE_POA23 = 550;
  public static final int SIZE_POA24 = 550;
  public static final int SIZE_POA25 = 550;
  public static final int SIZE_POA26 = 550;
  public static final int SIZE_POA27 = 550;
  public static final int SIZE_POA28 = 550;
  public static final int SIZE_POA29 = 550;
  public static final int SIZE_POA30 = 550;
  public static final int SIZE_POA31 = 550;
  public static final int SIZE_POA32 = 550;
  public static final int SIZE_POA33 = 550;
  public static final int SIZE_POA34 = 550;
  public static final int SIZE_POA35 = 550;
  public static final int SIZE_POA36 = 550;
  public static final int SIZE_POA37 = 550;
  public static final int SIZE_POA38 = 550;
  public static final int SIZE_POA39 = 550;
  public static final int SIZE_POA40 = 550;
  public static final int SIZE_POA41 = 550;
  public static final int SIZE_POA42 = 550;
  public static final int SIZE_POA43 = 550;
  public static final int SIZE_POA44 = 550;
  public static final int SIZE_POA45 = 550;
  public static final int SIZE_POA46 = 550;
  public static final int SIZE_POA47 = 550;
  public static final int SIZE_POA48 = 550;
  public static final int SIZE_POA49 = 550;
  public static final int SIZE_POA50 = 550;
  public static final int SIZE_POA51 = 550;
  public static final int SIZE_POA52 = 550;
  public static final int SIZE_POA53 = 550;
  public static final int SIZE_POA54 = 550;
  public static final int SIZE_POA55 = 550;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 30261;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POA01 = 1;
  public static final int VAR_POA02 = 2;
  public static final int VAR_POA03 = 3;
  public static final int VAR_POA04 = 4;
  public static final int VAR_POA05 = 5;
  public static final int VAR_POA06 = 6;
  public static final int VAR_POA07 = 7;
  public static final int VAR_POA08 = 8;
  public static final int VAR_POA09 = 9;
  public static final int VAR_POA10 = 10;
  public static final int VAR_POA11 = 11;
  public static final int VAR_POA12 = 12;
  public static final int VAR_POA13 = 13;
  public static final int VAR_POA14 = 14;
  public static final int VAR_POA15 = 15;
  public static final int VAR_POA16 = 16;
  public static final int VAR_POA17 = 17;
  public static final int VAR_POA18 = 18;
  public static final int VAR_POA19 = 19;
  public static final int VAR_POA20 = 20;
  public static final int VAR_POA21 = 21;
  public static final int VAR_POA22 = 22;
  public static final int VAR_POA23 = 23;
  public static final int VAR_POA24 = 24;
  public static final int VAR_POA25 = 25;
  public static final int VAR_POA26 = 26;
  public static final int VAR_POA27 = 27;
  public static final int VAR_POA28 = 28;
  public static final int VAR_POA29 = 29;
  public static final int VAR_POA30 = 30;
  public static final int VAR_POA31 = 31;
  public static final int VAR_POA32 = 32;
  public static final int VAR_POA33 = 33;
  public static final int VAR_POA34 = 34;
  public static final int VAR_POA35 = 35;
  public static final int VAR_POA36 = 36;
  public static final int VAR_POA37 = 37;
  public static final int VAR_POA38 = 38;
  public static final int VAR_POA39 = 39;
  public static final int VAR_POA40 = 40;
  public static final int VAR_POA41 = 41;
  public static final int VAR_POA42 = 42;
  public static final int VAR_POA43 = 43;
  public static final int VAR_POA44 = 44;
  public static final int VAR_POA45 = 45;
  public static final int VAR_POA46 = 46;
  public static final int VAR_POA47 = 47;
  public static final int VAR_POA48 = 48;
  public static final int VAR_POA49 = 49;
  public static final int VAR_POA50 = 50;
  public static final int VAR_POA51 = 51;
  public static final int VAR_POA52 = 52;
  public static final int VAR_POA53 = 53;
  public static final int VAR_POA54 = 54;
  public static final int VAR_POA55 = 55;
  public static final int VAR_POARR = 56;
  
  private Svgam0002d[] dsd = { new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(),
      new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(),
      new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(),
      new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(),
      new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(),
      new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(),
      new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(),
      new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), new Svgam0002d(), };
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private Object[] poa01 = dsd[0].o; //
  private Object[] poa02 = dsd[1].o; //
  private Object[] poa03 = dsd[2].o; //
  private Object[] poa04 = dsd[3].o; //
  private Object[] poa05 = dsd[4].o; //
  private Object[] poa06 = dsd[5].o; //
  private Object[] poa07 = dsd[6].o; //
  private Object[] poa08 = dsd[7].o; //
  private Object[] poa09 = dsd[8].o; //
  private Object[] poa10 = dsd[9].o; //
  private Object[] poa11 = dsd[10].o; //
  private Object[] poa12 = dsd[11].o; //
  private Object[] poa13 = dsd[12].o; //
  private Object[] poa14 = dsd[13].o; //
  private Object[] poa15 = dsd[14].o; //
  private Object[] poa16 = dsd[15].o; //
  private Object[] poa17 = dsd[16].o; //
  private Object[] poa18 = dsd[17].o; //
  private Object[] poa19 = dsd[18].o; //
  private Object[] poa20 = dsd[19].o; //
  private Object[] poa21 = dsd[20].o; //
  private Object[] poa22 = dsd[21].o; //
  private Object[] poa23 = dsd[22].o; //
  private Object[] poa24 = dsd[23].o; //
  private Object[] poa25 = dsd[24].o; //
  private Object[] poa26 = dsd[25].o; //
  private Object[] poa27 = dsd[26].o; //
  private Object[] poa28 = dsd[27].o; //
  private Object[] poa29 = dsd[28].o; //
  private Object[] poa30 = dsd[29].o; //
  private Object[] poa31 = dsd[30].o; //
  private Object[] poa32 = dsd[31].o; //
  private Object[] poa33 = dsd[32].o; //
  private Object[] poa34 = dsd[33].o; //
  private Object[] poa35 = dsd[34].o; //
  private Object[] poa36 = dsd[35].o; //
  private Object[] poa37 = dsd[36].o; //
  private Object[] poa38 = dsd[37].o; //
  private Object[] poa39 = dsd[38].o; //
  private Object[] poa40 = dsd[39].o; //
  private Object[] poa41 = dsd[40].o; //
  private Object[] poa42 = dsd[41].o; //
  private Object[] poa43 = dsd[42].o; //
  private Object[] poa44 = dsd[43].o; //
  private Object[] poa45 = dsd[44].o; //
  private Object[] poa46 = dsd[45].o; //
  private Object[] poa47 = dsd[46].o; //
  private Object[] poa48 = dsd[47].o; //
  private Object[] poa49 = dsd[48].o; //
  private Object[] poa50 = dsd[49].o; //
  private Object[] poa51 = dsd[50].o; //
  private Object[] poa52 = dsd[51].o; //
  private Object[] poa53 = dsd[52].o; //
  private Object[] poa54 = dsd[53].o; //
  private Object[] poa55 = dsd[54].o; //
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400Structure(dsd[0].structure), //
      new AS400Structure(dsd[1].structure), //
      new AS400Structure(dsd[2].structure), //
      new AS400Structure(dsd[3].structure), //
      new AS400Structure(dsd[4].structure), //
      new AS400Structure(dsd[5].structure), //
      new AS400Structure(dsd[6].structure), //
      new AS400Structure(dsd[7].structure), //
      new AS400Structure(dsd[8].structure), //
      new AS400Structure(dsd[9].structure), //
      new AS400Structure(dsd[10].structure), //
      new AS400Structure(dsd[11].structure), //
      new AS400Structure(dsd[12].structure), //
      new AS400Structure(dsd[13].structure), //
      new AS400Structure(dsd[14].structure), //
      new AS400Structure(dsd[15].structure), //
      new AS400Structure(dsd[16].structure), //
      new AS400Structure(dsd[17].structure), //
      new AS400Structure(dsd[18].structure), //
      new AS400Structure(dsd[19].structure), //
      new AS400Structure(dsd[20].structure), //
      new AS400Structure(dsd[21].structure), //
      new AS400Structure(dsd[22].structure), //
      new AS400Structure(dsd[23].structure), //
      new AS400Structure(dsd[24].structure), //
      new AS400Structure(dsd[25].structure), //
      new AS400Structure(dsd[26].structure), //
      new AS400Structure(dsd[27].structure), //
      new AS400Structure(dsd[28].structure), //
      new AS400Structure(dsd[29].structure), //
      new AS400Structure(dsd[30].structure), //
      new AS400Structure(dsd[31].structure), //
      new AS400Structure(dsd[32].structure), //
      new AS400Structure(dsd[33].structure), //
      new AS400Structure(dsd[34].structure), //
      new AS400Structure(dsd[35].structure), //
      new AS400Structure(dsd[36].structure), //
      new AS400Structure(dsd[37].structure), //
      new AS400Structure(dsd[38].structure), //
      new AS400Structure(dsd[39].structure), //
      new AS400Structure(dsd[40].structure), //
      new AS400Structure(dsd[41].structure), //
      new AS400Structure(dsd[42].structure), //
      new AS400Structure(dsd[43].structure), //
      new AS400Structure(dsd[44].structure), //
      new AS400Structure(dsd[45].structure), //
      new AS400Structure(dsd[46].structure), //
      new AS400Structure(dsd[47].structure), //
      new AS400Structure(dsd[48].structure), //
      new AS400Structure(dsd[49].structure), //
      new AS400Structure(dsd[50].structure), //
      new AS400Structure(dsd[51].structure), //
      new AS400Structure(dsd[52].structure), //
      new AS400Structure(dsd[53].structure), //
      new AS400Structure(dsd[54].structure), //
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, poa01, poa02, poa03, poa04, poa05, poa06, poa07, poa08, poa09, poa10, poa11, poa12, poa13, poa14, poa15,
          poa16, poa17, poa18, poa19, poa20, poa21, poa22, poa23, poa24, poa25, poa26, poa27, poa28, poa29, poa30, poa31, poa32, poa33,
          poa34, poa35, poa36, poa37, poa38, poa39, poa40, poa41, poa42, poa43, poa44, poa45, poa46, poa47, poa48, poa49, poa50, poa51,
          poa52, poa53, poa54, poa55, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400 et leur redécoupage.
   */
  public void setDSOutput() {
    byte[] as400out = getOutputData();
    int offset = 0;
    poind = (String) ds.getMembers(0).toObject(as400out, offset);
    offset += SIZE_POIND;
    poa01 = convertTObject(as400out, offset, 1);
    offset += SIZE_POA01;
    poa02 = convertTObject(as400out, offset, 2);
    offset += SIZE_POA02;
    poa03 = convertTObject(as400out, offset, 3);
    offset += SIZE_POA03;
    poa04 = convertTObject(as400out, offset, 4);
    offset += SIZE_POA04;
    poa05 = convertTObject(as400out, offset, 5);
    offset += SIZE_POA05;
    poa06 = convertTObject(as400out, offset, 6);
    offset += SIZE_POA06;
    poa07 = convertTObject(as400out, offset, 7);
    offset += SIZE_POA07;
    poa08 = convertTObject(as400out, offset, 8);
    offset += SIZE_POA08;
    poa09 = convertTObject(as400out, offset, 9);
    offset += SIZE_POA09;
    poa10 = convertTObject(as400out, offset, 10);
    offset += SIZE_POA10;
    poa11 = convertTObject(as400out, offset, 11);
    offset += SIZE_POA11;
    poa12 = convertTObject(as400out, offset, 12);
    offset += SIZE_POA12;
    poa13 = convertTObject(as400out, offset, 13);
    offset += SIZE_POA13;
    poa14 = convertTObject(as400out, offset, 14);
    offset += SIZE_POA14;
    poa15 = convertTObject(as400out, offset, 15);
    offset += SIZE_POA15;
    poa16 = convertTObject(as400out, offset, 16);
    offset += SIZE_POA16;
    poa17 = convertTObject(as400out, offset, 17);
    offset += SIZE_POA17;
    poa18 = convertTObject(as400out, offset, 18);
    offset += SIZE_POA18;
    poa19 = convertTObject(as400out, offset, 19);
    offset += SIZE_POA19;
    poa20 = convertTObject(as400out, offset, 20);
    offset += SIZE_POA20;
    poa21 = convertTObject(as400out, offset, 21);
    offset += SIZE_POA21;
    poa22 = convertTObject(as400out, offset, 22);
    offset += SIZE_POA22;
    poa23 = convertTObject(as400out, offset, 23);
    offset += SIZE_POA23;
    poa24 = convertTObject(as400out, offset, 24);
    offset += SIZE_POA24;
    poa25 = convertTObject(as400out, offset, 25);
    offset += SIZE_POA25;
    poa26 = convertTObject(as400out, offset, 26);
    offset += SIZE_POA26;
    poa27 = convertTObject(as400out, offset, 27);
    offset += SIZE_POA27;
    poa28 = convertTObject(as400out, offset, 28);
    offset += SIZE_POA28;
    poa29 = convertTObject(as400out, offset, 29);
    offset += SIZE_POA29;
    poa30 = convertTObject(as400out, offset, 30);
    offset += SIZE_POA30;
    poa31 = convertTObject(as400out, offset, 31);
    offset += SIZE_POA31;
    poa32 = convertTObject(as400out, offset, 32);
    offset += SIZE_POA32;
    poa33 = convertTObject(as400out, offset, 33);
    offset += SIZE_POA33;
    poa34 = convertTObject(as400out, offset, 34);
    offset += SIZE_POA34;
    poa35 = convertTObject(as400out, offset, 35);
    offset += SIZE_POA35;
    poa36 = convertTObject(as400out, offset, 36);
    offset += SIZE_POA36;
    poa37 = convertTObject(as400out, offset, 37);
    offset += SIZE_POA37;
    poa38 = convertTObject(as400out, offset, 38);
    offset += SIZE_POA38;
    poa39 = convertTObject(as400out, offset, 39);
    offset += SIZE_POA39;
    poa40 = convertTObject(as400out, offset, 40);
    offset += SIZE_POA40;
    poa41 = convertTObject(as400out, offset, 41);
    offset += SIZE_POA41;
    poa42 = convertTObject(as400out, offset, 42);
    offset += SIZE_POA42;
    poa43 = convertTObject(as400out, offset, 43);
    offset += SIZE_POA43;
    poa44 = convertTObject(as400out, offset, 44);
    offset += SIZE_POA44;
    poa45 = convertTObject(as400out, offset, 45);
    offset += SIZE_POA45;
    poa46 = convertTObject(as400out, offset, 46);
    offset += SIZE_POA46;
    poa47 = convertTObject(as400out, offset, 47);
    offset += SIZE_POA47;
    poa48 = convertTObject(as400out, offset, 48);
    offset += SIZE_POA48;
    poa49 = convertTObject(as400out, offset, 49);
    offset += SIZE_POA49;
    poa50 = convertTObject(as400out, offset, 50);
    offset += SIZE_POA50;
    poa51 = convertTObject(as400out, offset, 51);
    offset += SIZE_POA51;
    poa52 = convertTObject(as400out, offset, 52);
    offset += SIZE_POA52;
    poa53 = convertTObject(as400out, offset, 53);
    offset += SIZE_POA53;
    poa54 = convertTObject(as400out, offset, 54);
    offset += SIZE_POA54;
    poa55 = convertTObject(as400out, offset, 55);
    offset += SIZE_POA55;
    poarr = (String) ds.getMembers(56).toObject(as400out, offset);
    offset += SIZE_POARR;
  }
  
  /**
   * Retourne un tableau des valeurs correspondants aux lignes.
   */
  public Object[] getValeursBrutesLignes() {
    Object[] o = { poa01, poa02, poa03, poa04, poa05, poa06, poa07, poa08, poa09, poa10, poa11, poa12, poa13, poa14, poa15, poa16, poa17,
        poa18, poa19, poa20, poa21, poa22, poa23, poa24, poa25, poa26, poa27, poa28, poa29, poa30, poa31, poa32, poa33, poa34, poa35,
        poa36, poa37, poa38, poa39, poa40, poa41, poa42, poa43, poa44, poa45, poa46, poa47, poa48, poa49, poa50, poa51, poa52, poa53,
        poa54, poa55, };
    controlerProblemeConversionElement(o);
    return o;
  }
  
  /**
   * Convertit les valeurs de la datastructure.
   */
  private Object[] convertTObject(byte[] out, int offset, int indice) {
    try {
      return (Object[]) ds.getMembers(indice).toObject(out, offset);
    }
    catch (NumberFormatException e) {
      return null;
    }
  }
  
  /**
   * Permet de contrôler qu'un élément du tableau ne contient pas une erreur de données décimale lors de la conversion RPG -> Java.
   */
  private void controlerProblemeConversionElement(Object[] pTableau) {
    if (pTableau == null) {
      return;
    }
    for (int i = 0; i < pTableau.length; i++) {
      int j = i + 1;
      if (j < pTableau.length && pTableau[i] == null && pTableau[j] != null) {
        throw new MessageErreurException(
            "Il y a eu un problème lors de la conversion (RPG -> Java) des données à l'indice " + i + " du tableau. ");
      }
    }
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
