/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_FP pour les Formules de prix.
 */
public class DsFormulePrix extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "FPETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "FPTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "FPCOD"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "FPL1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "FPL2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "FPL3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "FPL4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "FPL5"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(9), "FPC1"));
    // rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(9, 0), "FPC1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(9), "FPC2"));
    // rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(9, 0), "FPC2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(9), "FPC3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(9), "FPC4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(9), "FPC5"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(15), "FPLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "FPS1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "FPS2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "FPS3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "FPS4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "FPS5"));
    
    length = 300;
  }
}
