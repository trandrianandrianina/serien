/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx002.v1;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.google.gson.JsonSyntaxException;

import ri.serien.libas400.dao.gvm.programs.InjecteurBdVetF;
import ri.serien.libas400.dao.sql.gescom.flux.OutilsMagento;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v1.JsonCommandeMagentoV1;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.AlmostQtemp;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.job.SurveillanceJob;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;

/**
 * Version des flux en V1.
 * Cette classe regroupe l'ensemble des traitements à effectuer sur l'importation des commandes Magento dans Série N.
 * 
 * Note :
 * Attention cette version à le taux de TVA de 20% en dur dans le code. Dans les versions V2 et V3 ela a été corrigé pour Tecnifibre
 * (SNC-10032).
 */
public class ImportationCommandeMagentoV1 extends BaseGroupDB {
  private SystemeManager systemeManager = null;
  private String codeVendeurMagento = null;
  private String magasinMagento = null;
  private String articleFraisPort = null;
  private String articleRemise = null;
  private char lettreEnv = ' ';
  private ParametresFluxBibli parametresBibli = null;
  private SurveillanceJob surveillanceJob = null;
  
  /**
   * Constructeur.
   */
  public ImportationCommandeMagentoV1(SystemeManager pSystemeManager, QueryManager pQueryManager, ParametresFluxBibli pParamFluxBibli,
      SurveillanceJob pSurveillanceJob) {
    super(pQueryManager);
    parametresBibli = pParamFluxBibli;
    systemeManager = pSystemeManager;
    if (pParamFluxBibli.getLettreEnvironnement() != null && pParamFluxBibli.getLettreEnvironnement().length() == 1) {
      lettreEnv = pParamFluxBibli.getLettreEnvironnement().charAt(0);
    }
    surveillanceJob = pSurveillanceJob;
  }
  
  /**
   * Permet de vérifier le contenu du JSON pour le matcher avec une commande Série N.
   */
  public boolean recupererUneCommandeMagento(FluxMagento flux) {
    if (flux == null) {
      majError("[GM_Import_Commandes] recupererUneCommandeMagento() Flux NULL ");
      return false;
    }
    
    if (flux.getFLJSN() == null || flux.getFLJSN().trim().length() == 0) {
      majError("[GM_Import_Commandes] recupererUneCommandeMagento() etb ou json corrompus ");
      return false;
    }
    
    boolean retour = false;
    
    // debut de la construction du JSON
    if (initJSON()) {
      // on récupère la commande Magento
      
      JsonCommandeMagentoV1 commande = null;// new CommandeMagento(json);//null;
      try {
        commande = gson.fromJson(flux.getFLJSN(), JsonCommandeMagentoV1.class);
      }
      catch (JsonSyntaxException e) {
        // TODO Bloc catch généré automatiquement
        majError("[GM_Import_Commandes] recupererUneCommandeMagento() Objet JSON: " + e.getMessage());
        return false;
      }
      
      // Si la commande JSON est correcte
      if (commande != null) {
        commande.setEtb(flux.getFLETB());
        
        if (commande.getIdCommandeClient() != null) {
          flux.setFLCOD(commande.getIdCommandeClient());
        }
        
        // Si les articles de la commandes sont cohérents
        if (commande.getListeArticlesCommandes() != null) {
          // On initialise les paramètres propres à toutes les commandes
          initParametresMagento(commande.getEtb());
          // On formate certaines zones de la commande
          commande.formaterDonnees();
          
          // ON va faire les traitements périphériques sur la commandes
          majSectionAnalytique(commande);
          
          // On teste la commande, le client tout ça tout ça
          if (testerValiditeCommande(commande)) {
            // On génre la commande
            retour = genererUneCommandeSerieN(commande);
          }
        }
        else {
          majError("[recupererUneCommandeMagento] getListeArticlesCommande NULL ");
          // retour = mettreAJourUnClient(commande);
        }
      }
      else {
        majError("[GM_Import_Commandes] recupererUneCommandeMagento() Objet JSON NULL ");
      }
    }
    
    return retour;
  }
  
  /**
   * Pemet de mettre à jour la section analytique de la commande sur la base du site Web de de cette derniere
   * PGVMPSIM SISAN = SICOD / SIETB
   */
  private void majSectionAnalytique(JsonCommandeMagentoV1 commande) {
    if (commande == null || commande.getIdSite() == 0 || commande.getEtb() == null) {
      if (commande != null) {
        majError("[GM_Import_Commandes] majSectionAnalytique() commande corrompue - commande.getIdSite():" + commande.getIdSite()
            + " -commande.getEtb(): " + commande.getEtb());
      }
      else {
        majError("[GM_Import_Commandes] majSectionAnalytique() commande null");
      }
      return;
    }
    
    ArrayList<GenericRecord> liste = queryManager.select(" " + " SELECT SISAN FROM " + queryManager.getLibrary()
        + ".PSEMPSIM WHERE SIETB = '" + commande.getEtb() + "' AND SICOD = '" + commande.getIdSite() + "' ");
    
    if (liste == null) {
      majError("[GM_Import_Commandes] majSectionAnalytique() la table des sites WEB est null ou corrompue pour " + commande.getEtb() + "/"
          + commande.getIdSite());
    }
    else {
      // On attribue la section analytique à la commande
      if (liste.size() > 0) {
        if (liste.get(0).isPresentField("SISAN")) {
          commande.setBESAN(liste.get(0).getField("SISAN").toString().trim());
        }
      }
    }
  }
  
  /**
   * Permet de tester si cette commande est valide: client existe, commande n'existe pas déjà....etc
   */
  private boolean testerValiditeCommande(JsonCommandeMagentoV1 commande) {
    if (commande == null || commande.getIdClientMagento() == null || commande.getIdCommandeClient() == null
        || commande.getEtb() == null) {
      if (commande != null) {
        majError("[GM_Import_Commandes] testerValiditeCommande() commande corrompue - commande.getIdClientMagento():"
            + commande.getIdClientMagento() + " - commande.getIdCommandeClient(): " + commande.getIdCommandeClient()
            + " - commande.getEtb(): " + commande.getEtb());
      }
      return false;
    }
    
    // On teste le client
    if (!testerValiditeDuClient(commande)) {
      return false;
    }
    
    String idCommandeMagento = OutilsMagento.formaterTexteLongueurMax(commande.getIdCommandeClient(), 10);
    commande.setIdCommandeClient(idCommandeMagento);
    
    // On va tester si la commande n'existe pas déjà dans la base
    ArrayList<GenericRecord> liste2 =
        queryManager.select("SELECT E1NUM FROM " + queryManager.getLibrary() + ".PGVMEBCM WHERE  E1COD = 'E' AND E1ETB ='"
            + commande.getEtb() + "' AND E1IN18 = 'W' AND E1CCT = '" + commande.getIdCommandeClient() + "' ");
    // Si on a un problème de requête
    if (liste2 == null) {
      majError("[GM_Import_Commandes] testerValiditeCommande() la requête est null pour " + commande.getEtb() + "/"
          + commande.getIdCommandeClient() + ": " + queryManager.getMsgError());
      return false;
    }
    // Si on a trouvé des enregistrements
    else if (liste2.size() > 0) {
      majError("[GM_Import_Commandes] testerValiditeCommande() Il existe déjà des commandes Magento pour " + commande.getEtb() + "/"
          + commande.getIdCommandeClient() + ": " + queryManager.getMsgError());
      return false;
    }
    
    return true;
  }
  
  private boolean testerValiditeDuClient(JsonCommandeMagentoV1 commande) {
    // On commence par le client Série N
    ArrayList<GenericRecord> liste = queryManager.select("SELECT CLCLI,CLLIV,CLETB,CLTTC,CLCAT FROM " + queryManager.getLibrary()
        + ".PGVMCLIM WHERE CLETB ='" + commande.getEtb() + "' AND CLADH = '" + commande.getIdClientMagento() + "' ");
    
    // Null ou pas de client
    if (liste == null || liste.size() == 0) {
      majError("[GM_Import_Commandes] testerValiditeDuClient() le client est null ou inexistant " + commande.getEtb() + "/"
          + commande.getIdClientMagento() + " " + queryManager.getMsgError());
      return false;
    }
    // Un client trouvé
    else if (liste.size() == 1) {
      // Notre client est valide on met à jour les données propres à l'injection
      if (liste.get(0).isPresentField("CLCLI")) {
        commande.setBENCLP(liste.get(0).getField("CLCLI").toString());
      }
      if (liste.get(0).isPresentField("CLLIV")) {
        commande.setBENCLS(liste.get(0).getField("CLLIV").toString());
      }
      if (liste.get(0).isPresentField("CLTTC")) {
        commande.setTTC(liste.get(0).getField("CLTTC").toString().equals("1") || liste.get(0).getField("CLTTC").toString().equals("2"));
      }
      // Si on est TTC (particulier) on n'a pas associé de contact à notre client inutile de contrôler son existence donc
      if (commande.isTTC()) {
        if (liste.get(0).isPresentField("CLETB") && liste.get(0).isPresentField("CLCAT") && liste.get(0).isPresentField("CLCLI")
            && liste.get(0).isPresentField("CLLIV")) {
          ArrayList<GenericRecord> listeC = queryManager.select(" SELECT RLNUMT FROM " + queryManager.getLibrary()
              + ".PSEMRTLM WHERE RLETB = '" + liste.get(0).getField("CLETB").toString().trim() + "' " + " AND RLCOD='C' AND  RLIND = '"
              + formaterLeClientPourRLIND(liste.get(0).getField("CLCLI").toString()) + "000" + "' ");
          if (listeC != null && listeC.size() == 1) {
            if (listeC.get(0).isPresentField("RLNUMT")) {
              commande.setRENUM(listeC.get(0).getField("RLNUMT").toString().trim());
            }
          }
          else {
            // Aucun contact trouvé dans la table des liens contact/client
            if (listeC == null || listeC.isEmpty()) {
              majError(
                  "[testerValiditeDuClient] Le contact du client n'a pas été trouvé dans la table des liens contact/client (PSEMRTLM)");
            }
            // Plusieurs contacts trouvés dans la table des liens contact/client
            else {
              String listeRLNUMT = "";
              for (GenericRecord record : listeC) {
                listeRLNUMT += record.getStringValue("RLNUMT", "", true) + " ";
              }
              majError("[testerValiditeDuClient] Plusieurs contacts ont été trouvé dans la table des liens contact/client (PSEMRTLM) : "
                  + listeRLNUMT);
            }
            return false;
          }
        }
      }
    }
    else {
      majError("[GM_Import_Commandes] testerValiditeDuClient() il existe plusieurs clients pour " + commande.getEtb() + "/"
          + commande.getIdClientMagento() + " -> " + liste.size());
      return false;
    }
    
    // On teste le fucking code postal qui est intégré
    if (!JsonEntiteMagento.controlerUnCodePostal(true, commande.getCdpClientFac())) {
      majError("[GM_Import_Commandes] testerValiditeDuClient() le code postal client facturé est erroné ");
      return false;
    }
    
    if (!JsonEntiteMagento.controlerUnCodePostal(true, commande.getCdpClientLiv())) {
      majError("[GM_Import_Commandes] testerValiditeDuClient() le code postal client livré est erroné ");
      return false;
    }
    
    return true;
  }
  
  /**
   * Permet de générer une commande Série N.
   */
  private boolean genererUneCommandeSerieN(JsonCommandeMagentoV1 pCommande) {
    SystemeManager system = new SystemeManager(true);
    
    boolean retour = false;
    LinkedHashMap<String, String> rapport = new LinkedHashMap<String, String>();
    // TODO Remplacer par le DIGITS de la commande Magento
    int numeroAleatoire = 145;
    // Création de la "QTEMP" à partir d'un numéro aléatoire
    AlmostQtemp qtemp = null;
    try {
      qtemp = new AlmostQtemp(system.getSystem(), numeroAleatoire);
    }
    catch (Exception e) {
      majError("[ImportCmdeMagento]  Création AlmostQtemp: " + e.getMessage());
      system.deconnecter();
      return false;
    }
    if (qtemp.create()) {
      // On initialise le gestionnaire d'injection de bons JAVA
      InjecteurBdVetF sgvmiv = new InjecteurBdVetF(system, qtemp.getName(), queryManager.getLibrary(), lettreEnv,
          ParametresFluxBibli.getBibliothequeEnvironnement());
      retour = sgvmiv.initialiser(surveillanceJob);
      if (!retour) {
        majError("[GM_Import_Commandes]  sgvmiv.init(): " + sgvmiv.getMsgError());
        qtemp.delete();
        system.deconnecter();
        return retour;
      }
      else {
        sgvmiv.setGestionComplementEntete(true);
        // Nettoyage des fichiers physiques PINJBDV*
        retour = sgvmiv.clearAndCreateAllFilesInWrkLib();
        if (!retour) {
          majError("[GM_Import_Commandes]  sgvmiv.clearAndCreateAllFilesInWrkLib(): " + sgvmiv.getMsgError());
          qtemp.delete();
          system.deconnecter();
          return retour;
        }
        else {
          // On construit le contenu de la commande Série N sur la base de la commande Magento
          ArrayList<GenericRecord> listeRcds = new ArrayList<GenericRecord>();
          
          // TODO VARIABILISER TOUTES LES Mother fucker constantes métier de type magasin défaut, expédition, etc...
          
          // SI la commande doit être injectée en TTC on le fait .... ben oui c'est bizarre mais bon
          if (pCommande.isTTC()) {
            // TODO Faire gaffe avec la bonne récupération de la TVA
            pCommande.passerEnTTC();
          }
          
          // Création de la ligne d'entête du bon
          GenericRecord enteteBon = new GenericRecord();
          enteteBon.setField("BEETB", pCommande.getEtb());
          enteteBon.setField("BEMAG", magasinMagento);
          enteteBon.setField("BEDAT", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
          enteteBon.setField("BEDLS", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
          enteteBon.setField("BERBV", "");
          enteteBon.setField("BEERL", "E");
          enteteBon.setField("BENLI", 0);
          enteteBon.setField("BENCLP", pCommande.getBENCLP());
          enteteBon.setField("BENCLS", pCommande.getBENCLS());
          // TODO Faudra paramétrer un Code vendeur Magento
          enteteBon.setField("BEVDE", codeVendeurMagento);
          // enteteBon.setField("BEMEX", commande.getModeLivraison());
          // TODO BOn là on a une confuse à l'analyse: la donnée dans la livraison est en fait ... le code transporteur Du coup on se sert
          // pas de BEMEX
          enteteBon.setField("BECTR", pCommande.getModeLivraison().trim());
          enteteBon.setField("BERG2", pCommande.getModeReglement());
          
          if (pCommande.getBESAN() != null) {
            enteteBon.setField("BESAN", pCommande.getBESAN());
          }
          enteteBon.setField("BEOPT", formaterStatut_Magento_SN(pCommande.getStatut()));
          
          // Bloc adresse facturée
          enteteBon.setField("BENOMF", tronquerBlocAdresse(pCommande.getBENOMF()));
          enteteBon.setField("BECPLF", tronquerBlocAdresse(pCommande.getBECPLF()));
          enteteBon.setField("BERUEF", tronquerBlocAdresse(pCommande.getBERUEF()));
          enteteBon.setField("BELOCF", tronquerBlocAdresse(pCommande.getBELOCF()));
          
          enteteBon.setField("BECDPF", pCommande.getCdpClientFac());
          enteteBon.setField("BEVILF", pCommande.getVilleClientFac());
          if (pCommande.getPaysClientFac() != null) {
            enteteBon.setField("BEPAYF", recupererLibellePaysSerieN(pCommande.getPaysClientFac()));
          }
          
          // Bloc adresse livraison
          // on a retiré le nettoyage SQL car on traite en GAP
          enteteBon.setField("BENOML", tronquerBlocAdresse(pCommande.getBENOML()));
          enteteBon.setField("BECPLL", tronquerBlocAdresse(pCommande.getBECPLL()));
          enteteBon.setField("BERUEL", tronquerBlocAdresse(pCommande.getBERUEL()));
          enteteBon.setField("BELOCL", tronquerBlocAdresse(pCommande.getBELOCL()));
          
          enteteBon.setField("BECDPL", pCommande.getCdpClientLiv());
          
          enteteBon.setField("BEVILL", pCommande.getVilleClientLiv());
          
          if (pCommande.getPaysClientLiv() != null) {
            enteteBon.setField("BEPAYL", recupererLibellePaysSerieN(pCommande.getPaysClientLiv()));
          }
          
          listeRcds.add(enteteBon);
          
          if (sgvmiv.isGestionComplementEntete()) {
            // Création de la ligne d'entête du bon complémentaire
            GenericRecord enteteComp = new GenericRecord();
            
            enteteComp.setField("BFETB", pCommande.getEtb());
            enteteComp.setField("BFMAG", magasinMagento);
            enteteComp.setField("BFDAT", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
            enteteComp.setField("BFRBV", "");
            enteteComp.setField("BFERL", "F");
            enteteComp.setField("BFNLI", 0);
            enteteComp.setField("BFCCT", pCommande.getIdCommandeClient());
            enteteComp.setField("BFTTC", pCommande.getTotalTTC());
            enteteComp.setField("BFTHTL", pCommande.getTotalHT());
            // frais de port
            if (pCommande.getModeLivraison() != null && pCommande.getFraisPortCommande() != null) {
              articleFraisPort = "PORT-" + pCommande.getModeLivraison();
            }
            else {
              articleFraisPort = null;
            }
            if (articleFraisPort != null) {
              enteteComp.setField("BFARTF", articleFraisPort);
              enteteComp.setField("BFFRP", pCommande.getFraisPortCommande());
            }
            
            // REMISE
            if (articleRemise != null && pCommande.getRemise() != null) {
              enteteComp.setField("BFARTR", articleRemise);
              enteteComp.setField("BFREM", pCommande.getRemise());
            }
            
            enteteComp.setField("BFPDS", pCommande.retournerPoidsFormate());
            
            enteteComp.setField("BFTELF", pCommande.getTelClientFac());
            enteteComp.setField("BFTELL", pCommande.getTelClientLiv());
            
            if (pCommande.getServiceLivraison() != null) {
              enteteComp.setField("BFCRT", pCommande.getServiceLivraison());
            }
            if (pCommande.getPointRelais() != null) {
              enteteComp.setField("BFCREL", pCommande.getPointRelais());
            }
            if (pCommande.getPaysPointRelais() != null) {
              enteteComp.setField("BFPREL", pCommande.getPaysPointRelais());
            }
            // Numéro de contact
            if (pCommande.getRENUM() != null) {
              enteteComp.setField("BFCNUM", pCommande.getRENUM());
            }
            
            listeRcds.add(enteteComp);
          }
          
          int numLignes = 0;
          // Articles commentaires
          if (pCommande.getListeBlocNotes() != null) {
            GenericRecord commentaire = new GenericRecord();
            numLignes = numLignes + 10;
            // lignes commentaires
            commentaire.setField("BLETB", pCommande.getEtb());
            commentaire.setField("BLMAGE", magasinMagento);
            commentaire.setField("BLDAT", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
            commentaire.setField("BLRBV", "");
            commentaire.setField("BLNLI", numLignes);
            commentaire.setField("BLERL", "L");
            for (int j = 0; j < pCommande.getListeBlocNotes().size(); j++) {
              switch (j) {
                case 0:
                  commentaire.setField("BLLIB1", pCommande.getListeBlocNotes().get(j));
                  break;
                case 1:
                  commentaire.setField("BLLIB2", pCommande.getListeBlocNotes().get(j));
                  break;
                case 2:
                  commentaire.setField("BLLIB3", pCommande.getListeBlocNotes().get(j));
                  break;
                case 3:
                  commentaire.setField("BLLIB4", pCommande.getListeBlocNotes().get(j));
                  break;
                
                default:
                  break;
              }
            }
            listeRcds.add(commentaire);
          }
          
          // Préparation de la clef pour les lignes du futur bon
          for (int i = 0; i < pCommande.getListeArticlesCommandes().size(); i++) {
            numLignes = numLignes + 10;
            GenericRecord article = new GenericRecord();
            article.setField("BLETB", pCommande.getEtb());
            article.setField("BLMAGE", magasinMagento);
            article.setField("BLDAT", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
            article.setField("BLRBV", "");
            article.setField("BLNLI", "" + (numLignes));
            article.setField("BLERL", "L");
            if (pCommande.getListeArticlesCommandes().get(i).getCodeArticle() != null) {
              article.setField("BLART", pCommande.getListeArticlesCommandes().get(i).getCodeArticle());
            }
            // libellés article
            if (pCommande.getListeArticlesCommandes().get(i).getBLLIB1() != null) {
              article.setField("BLLIB1", pCommande.getListeArticlesCommandes().get(i).getBLLIB1());
            }
            if (pCommande.getListeArticlesCommandes().get(i).getBLLIB2() != null) {
              article.setField("BLLIB2", pCommande.getListeArticlesCommandes().get(i).getBLLIB2());
            }
            if (pCommande.getListeArticlesCommandes().get(i).getBLLIB3() != null) {
              article.setField("BLLIB3", pCommande.getListeArticlesCommandes().get(i).getBLLIB3());
            }
            if (pCommande.getListeArticlesCommandes().get(i).getBLLIB4() != null) {
              article.setField("BLLIB4", pCommande.getListeArticlesCommandes().get(i).getBLLIB4());
            }
            
            article.setField("BLQTE", pCommande.getListeArticlesCommandes().get(i).getQteCommandee());
            article.setField("BLPRX", pCommande.getListeArticlesCommandes().get(i).getPrixPourInjection());
            // TODO faire un truc avec les vraies colones de TVA de Série N 1 à 6 (DG)
            // commande.getListeArticlesCommandes().get(i).getColonneTVA()
            article.setField("BLTVA", "1");
            article.setField("BLMHTF", pCommande.getListeArticlesCommandes().get(i).getTotalHT());
            
            article.setField("BLD3EU", pCommande.getListeArticlesCommandes().get(i).getDEEE());
            article.setField("BLD3ET", pCommande.getListeArticlesCommandes().get(i).getTotalDEEE());
            
            listeRcds.add(article);
          }
          
          // On insert dans le
          retour = sgvmiv.insertRecord(listeRcds);
          if (!retour) {
            majError("[GM_Import_Commandes]  sgvmiv.insertRecord: " + sgvmiv.getMsgError());
            qtemp.delete();
            system.deconnecter();
            return retour;
          }
          else {
            // On injecte le PINJBDV avec le rapport mis à jour: soit avec des erreurs soit avec les infos du bon créé
            retour = sgvmiv.injectFile(rapport);
            // On a un souci
            if (!retour) {
              majError("[GM_Import_Commandes]  PB sgvmiv.injectFile: " + sgvmiv.getMsgError() + rapport);
              qtemp.delete();
              system.deconnecter();
              return retour;
            }
            else {
              if (rapport.containsKey("TYPE") && rapport.containsKey("ETB") && rapport.containsKey("NUMERO")
                  && rapport.containsKey("SUFFIXE")) {
                retour = true;
              }
            }
          }
        }
      }
      // Si on est en débug on ne supprime pas la qtemp
      if (parametresBibli.isModeDebug()) {
        retour = true;
      }
      else {
        retour = qtemp.delete();
      }
      
      if (!retour) {
        majError("[GM_Import_Commandes]  qtemp.delete(): " + qtemp.getMsgError());
      }
    }
    else {
      majError("[GM_Import_Commandes]  qtemp.create(): " + qtemp.getMsgError());
    }
    
    try {
      system.deconnecter();
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la déconnexion.");
    }
    return retour;
  }
  
  /**
   * Initialiser les paramètres propres à l'établissement de vente
   **/
  private void initParametresMagento(String etb) {
    // TODO contrôler les paramètres Série N de PGVMPARM ??!! trouver une soluc de paramétrage
    
    // Attention pour tests paramètres de OPNK
    codeVendeurMagento = parametresBibli.getVendeurMagento();
    magasinMagento = parametresBibli.getMagasinMagento();
    // TODO en attente de la création dans Série N de ces fucking articles
    articleFraisPort = null;
    articleRemise = "REMISE";
  }
  
  /**
   * On récupère le libellé d'un pays sur la base de son code Série N
   */
  protected String recupererLibellePaysSerieN(String pCodePays) {
    String retour = "";
    
    if (pCodePays == null || pCodePays.trim().equals("")) {
      return "FRANCE                     " + "FR ";
    }
    
    String libelle = null;
    ArrayList<GenericRecord> liste = queryManager.select(" SELECT PARFIL FROM " + queryManager.getLibrary() + ".PSEMPARM "
        + " WHERE PARCLE LIKE '   CP%' AND SUBSTR(PARCLE, 6, 3) = '" + pCodePays + "' ");
    
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("PARFIL")) {
        libelle = liste.get(0).getField("PARFIL").toString().trim();
      }
    }
    
    if (libelle != null) {
      int nbCarLib = 26;
      int nbCar = libelle.length();
      int carManquants = nbCarLib - nbCar;
      retour = libelle;
      
      if (carManquants > 0) {
        for (int i = 0; i < carManquants; i++) {
          retour += " ";
        }
      }
      
      retour += " " + pCodePays;
    }
    
    return retour;
  }
  
  /**
   * formater le statut Magento en statut Série N
   */
  protected String formaterStatut_Magento_SN(String magento) {
    // TODO avec une vraie table de paramètres propre plut^to que ces tests en DUUUUR
    // Faire une classe TraitementMagento
    
    if (magento == null) {
      return "ATT";
    }
    
    String retour = "ATT";
    
    if (magento.equals("0")) {
      retour = "ATT";
    }
    else if (magento.equals("1") || magento.equals("3")) {
      retour = "HOM";
    }
    else if (magento.equals("4")) {
      retour = "EXP";
    }
    else if (magento.equals("6") || magento.equals("7")) {
      retour = "FAC";
    }
    
    return retour;
    
  }
  
  /**
   * formater une date jj/mm/aaaa
   */
  public String formaterUneDateJJMMAAAA_SN(String date) {
    String retour = "";
    if (date == null || date.trim().equals("")) {
      retour = DateHeure.getFormateDateHeure(DateHeure.AAMMJJ);
    }
    else {
      retour = date;
      String[] tab = retour.split("/");
      if (tab.length == 3) {
        if (tab[0].length() == 2 && tab[1].length() == 2 && tab[2].length() == 4) {
          retour = tab[2].substring(2) + tab[1] + tab[0];
        }
      }
    }
    
    return retour;
  }
  
  @Override
  public void dispose() {
    queryManager = null;
    if (systemeManager != null) {
      systemeManager.deconnecter();
    }
  }
  
  /**
   * Alors il faut formater l'affichage du fucking code client pour un insert dans le fucking RLIND de PSEMRTLM avant sa fucking
   * concaténation.....POUAAAAAH
   */
  protected String formaterLeClientPourRLIND(String client) {
    String retour = client;
    if (retour != null && retour.length() > 0) {
      if (retour.length() == 1) {
        retour = "00000" + retour;
      }
      else if (retour.length() == 2) {
        retour = "0000" + retour;
      }
      else if (retour.length() == 3) {
        retour = "000" + retour;
      }
      else if (retour.length() == 4) {
        retour = "00" + retour;
      }
      else if (retour.length() == 5) {
        retour = "0" + retour;
      }
    }
    else {
      retour = "000000";
    }
    
    return retour;
  }
  
  /**
   * Permet de tronquer les zones du bloc adresses
   */
  protected String tronquerBlocAdresse(String adresse) {
    String retour = null;
    if (adresse != null) {
      retour = adresse;
      if (retour.trim().length() > JsonEntiteMagento.NB_MAX_CARACTERES_BLOC_ADRESSE) {
        retour = retour.trim().substring(0, JsonEntiteMagento.NB_MAX_CARACTERES_BLOC_ADRESSE);
      }
    }
    
    return retour;
  }
}
