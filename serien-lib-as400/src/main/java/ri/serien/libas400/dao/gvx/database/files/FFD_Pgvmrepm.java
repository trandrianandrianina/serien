/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

public abstract class FFD_Pgvmrepm extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_RPTOP = 1;
  public static final int DECIMAL_RPTOP = 0;
  public static final int SIZE_RPCRE = 7;
  public static final int DECIMAL_RPCRE = 0;
  public static final int SIZE_RPMOD = 7;
  public static final int DECIMAL_RPMOD = 0;
  public static final int SIZE_RPTRT = 7;
  public static final int DECIMAL_RPTRT = 0;
  public static final int SIZE_RPETB = 3;
  public static final int SIZE_RPREP = 2;
  public static final int SIZE_RPCIV = 3;
  public static final int SIZE_RPNOM = 30;
  public static final int SIZE_RPEDT = 30;
  public static final int SIZE_RPRUE = 30;
  public static final int SIZE_RPLOC = 30;
  public static final int SIZE_RPVIL = 30;
  public static final int SIZE_RPPAY = 26;
  public static final int SIZE_RPCOP = 3;
  public static final int SIZE_RPCDP = 5;
  public static final int DECIMAL_RPCDP = 0;
  public static final int SIZE_RPTYP = 5;
  public static final int SIZE_RPCLK = 15;
  public static final int SIZE_RPTEL = 20;
  public static final int SIZE_RPFAX = 20;
  public static final int SIZE_RPNET = 120;
  public static final int SIZE_RPOBS = 18;
  public static final int SIZE_RPNOF = 7;
  public static final int DECIMAL_RPNOF = 0;
  public static final int SIZE_RPGEO = 5;
  public static final int SIZE_RPMAG = 2;
  public static final int SIZE_RPCNC = 4;
  public static final int SIZE_RPMOC = 1;
  public static final int DECIMAL_RPMOC = 0;
  public static final int SIZE_RPSEU = 5;
  public static final int DECIMAL_RPSEU = 0;
  public static final int SIZE_RPCOM = 4;
  public static final int DECIMAL_RPCOM = 2;
  public static final int SIZE_RPO01 = 7;
  public static final int DECIMAL_RPO01 = 0;
  public static final int SIZE_RPO02 = 7;
  public static final int DECIMAL_RPO02 = 0;
  public static final int SIZE_RPO03 = 7;
  public static final int DECIMAL_RPO03 = 0;
  public static final int SIZE_RPO04 = 7;
  public static final int DECIMAL_RPO04 = 0;
  public static final int SIZE_RPO05 = 7;
  public static final int DECIMAL_RPO05 = 0;
  public static final int SIZE_RPO06 = 7;
  public static final int DECIMAL_RPO06 = 0;
  public static final int SIZE_RPO07 = 7;
  public static final int DECIMAL_RPO07 = 0;
  public static final int SIZE_RPO08 = 7;
  public static final int DECIMAL_RPO08 = 0;
  public static final int SIZE_RPO09 = 7;
  public static final int DECIMAL_RPO09 = 0;
  public static final int SIZE_RPO10 = 7;
  public static final int DECIMAL_RPO10 = 0;
  public static final int SIZE_RPO11 = 7;
  public static final int DECIMAL_RPO11 = 0;
  public static final int SIZE_RPO12 = 7;
  public static final int DECIMAL_RPO12 = 0;
  public static final int SIZE_RPCO2 = 4;
  public static final int DECIMAL_RPCO2 = 2;
  public static final int SIZE_RPTYA = 3;
  public static final int SIZE_RPNTA = 2;
  public static final int DECIMAL_RPNTA = 0;
  public static final int SIZE_RPLEM = 2;
  public static final int DECIMAL_RPLEM = 0;
  public static final int SIZE_RPSAN = 4;
  public static final int SIZE_RPNSA = 1;
  public static final int DECIMAL_RPNSA = 0;
  public static final int SIZE_RPIN1 = 1;
  public static final int SIZE_RPIN2 = 1;
  public static final int SIZE_RPIN3 = 1;
  public static final int SIZE_RPIN4 = 1;
  public static final int SIZE_RPIN5 = 1;
  public static final int SIZE_RPVDE = 3;
  
  // Variables fichiers
  protected int RPTOP = 0; // Top système
  protected int RPCRE = 0; // Date de création
  protected int RPMOD = 0; // Date de modification
  protected int RPTRT = 0; // Date de traitement
  protected String RPETB = null; // Code établissement
  protected String RPREP = null; // Code Représentant
  protected String RPCIV = null; // Civilité
  protected String RPNOM = null; // Nom
  protected String RPEDT = null; // Libellé édition
  protected String RPRUE = null; // Adresse - rue
  protected String RPLOC = null; // Adresse - localité
  protected String RPVIL = null; // Adresse - ville
  protected String RPPAY = null; // Adresse - pays
  protected String RPCOP = null; // Code pays
  protected int RPCDP = 0; // Code Postal
  protected String RPTYP = null; // Type représentant
  protected String RPCLK = null; // Clé de classement alpha
  protected String RPTEL = null; // Téléphone
  protected String RPFAX = null; // Télécopie
  protected String RPNET = null; // Adresse eMail
  protected String RPOBS = null; // Observations
  protected int RPNOF = 0; // Notes de frais
  protected String RPGEO = null; // Zone géographique
  protected String RPMAG = null; // Magasin
  protected String RPCNC = null; // Code regroupement
  protected int RPMOC = 0; // Base
  protected int RPSEU = 0; // Seuil commisionnement
  protected int RPCOM = 0; // Taux de base
  protected int RPO01 = 0; // Plafond chiffre d affaire
  protected int RPO02 = 0; // Plafond chiffre d affaire
  protected int RPO03 = 0; // Plafond chiffre d affaire
  protected int RPO04 = 0; // Plafond chiffre d affaire
  protected int RPO05 = 0; // Plafond chiffre d affaire
  protected int RPO06 = 0; // Plafond chiffre d affaire
  protected int RPO07 = 0; // Plafond chiffre d affaire
  protected int RPO08 = 0; // Plafond chiffre d affaire
  protected int RPO09 = 0; // Plafond chiffre d affaire
  protected int RPO10 = 0; // Plafond chiffre d affaire
  protected int RPO11 = 0; // Plafond chiffre d affaire
  protected int RPO12 = 0; // Plafond chiffre d affaire
  protected int RPCO2 = 0; // Taux plafond
  protected String RPTYA = null; // Type action prospection
  protected int RPNTA = 0; // Nbr.actions / 12 mois
  protected int RPLEM = 0; // Limite dernière act° en mois
  protected String RPSAN = null; // Section analytique
  protected int RPNSA = 0; // Pointeur analytique
  protected char RPIN1 = ' '; // Non utilisé
  protected char RPIN2 = ' '; // Non utilisé
  protected char RPIN3 = ' '; // Non utilisé
  protected char RPIN4 = ' '; // Non utilisé
  protected char RPIN5 = ' '; // Non utilisé
  protected String RPVDE = null; // Code vendeur
  
  /**
   * Constructeur.
   */
  public FFD_Pgvmrepm(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Initialise les variables avec les valeurs par défaut.
   */
  @Override
  public void initialization() {
    RPTOP = 0;
    RPCRE = 0;
    RPMOD = 0;
    RPTRT = 0;
    RPETB = null;
    RPREP = null;
    RPCIV = null;
    RPNOM = null;
    RPEDT = null;
    RPRUE = null;
    RPLOC = null;
    RPVIL = null;
    RPPAY = null;
    RPCOP = null;
    RPCDP = 0;
    RPTYP = null;
    RPCLK = null;
    RPTEL = null;
    RPFAX = null;
    RPNET = null;
    RPOBS = null;
    RPNOF = 0;
    RPGEO = null;
    RPMAG = null;
    RPCNC = null;
    RPMOC = 0;
    RPSEU = 0;
    RPCOM = 0;
    RPO01 = 0;
    RPO02 = 0;
    RPO03 = 0;
    RPO04 = 0;
    RPO05 = 0;
    RPO06 = 0;
    RPO07 = 0;
    RPO08 = 0;
    RPO09 = 0;
    RPO10 = 0;
    RPO11 = 0;
    RPO12 = 0;
    RPCO2 = 0;
    RPTYA = null;
    RPNTA = 0;
    RPLEM = 0;
    RPSAN = null;
    RPNSA = 0;
    RPIN1 = ' ';
    RPIN2 = ' ';
    RPIN3 = ' ';
    RPIN4 = ' ';
    RPIN5 = ' ';
    RPVDE = null;
  }
  
  /**
   * @return le rPTOP
   */
  public int getRPTOP() {
    return RPTOP;
  }
  
  /**
   * @param rPTOP le rPTOP à définir
   */
  public void setRPTOP(int rPTOP) {
    RPTOP = rPTOP;
  }
  
  /**
   * @return le rPCRE
   */
  public int getRPCRE() {
    return RPCRE;
  }
  
  /**
   * @param rPCRE le rPCRE à définir
   */
  public void setRPCRE(int rPCRE) {
    RPCRE = rPCRE;
  }
  
  /**
   * @return le rPMOD
   */
  public int getRPMOD() {
    return RPMOD;
  }
  
  /**
   * @param rPMOD le rPMOD à définir
   */
  public void setRPMOD(int rPMOD) {
    RPMOD = rPMOD;
  }
  
  /**
   * @return le rPTRT
   */
  public int getRPTRT() {
    return RPTRT;
  }
  
  /**
   * @param rPTRT le rPTRT à définir
   */
  public void setRPTRT(int rPTRT) {
    RPTRT = rPTRT;
  }
  
  /**
   * @return le rPETB
   */
  public String getRPETB() {
    return RPETB;
  }
  
  /**
   * @param rPETB le rPETB à définir
   */
  public void setRPETB(String rPETB) {
    RPETB = rPETB;
  }
  
  /**
   * @return le rPREP
   */
  public String getRPREP() {
    return RPREP;
  }
  
  /**
   * @param rPREP le rPREP à définir
   */
  public void setRPREP(String rPREP) {
    RPREP = rPREP;
  }
  
  /**
   * @return le rPCIV
   */
  public String getRPCIV() {
    return RPCIV;
  }
  
  /**
   * @param rPCIV le rPCIV à définir
   */
  public void setRPCIV(String rPCIV) {
    RPCIV = rPCIV;
  }
  
  /**
   * @return le rPNOM
   */
  public String getRPNOM() {
    return RPNOM;
  }
  
  /**
   * @param rPNOM le rPNOM à définir
   */
  public void setRPNOM(String rPNOM) {
    RPNOM = rPNOM;
  }
  
  /**
   * @return le rPEDT
   */
  public String getRPEDT() {
    return RPEDT;
  }
  
  /**
   * @param rPEDT le rPEDT à définir
   */
  public void setRPEDT(String rPEDT) {
    RPEDT = rPEDT;
  }
  
  /**
   * @return le rPRUE
   */
  public String getRPRUE() {
    return RPRUE;
  }
  
  /**
   * @param rPRUE le rPRUE à définir
   */
  public void setRPRUE(String rPRUE) {
    RPRUE = rPRUE;
  }
  
  /**
   * @return le rPLOC
   */
  public String getRPLOC() {
    return RPLOC;
  }
  
  /**
   * @param rPLOC le rPLOC à définir
   */
  public void setRPLOC(String rPLOC) {
    RPLOC = rPLOC;
  }
  
  /**
   * @return le rPVIL
   */
  public String getRPVIL() {
    return RPVIL;
  }
  
  /**
   * @param rPVIL le rPVIL à définir
   */
  public void setRPVIL(String rPVIL) {
    RPVIL = rPVIL;
  }
  
  /**
   * @return le rPPAY
   */
  public String getRPPAY() {
    return RPPAY;
  }
  
  /**
   * @param rPPAY le rPPAY à définir
   */
  public void setRPPAY(String rPPAY) {
    RPPAY = rPPAY;
  }
  
  /**
   * @return le rPCOP
   */
  public String getRPCOP() {
    return RPCOP;
  }
  
  /**
   * @param rPCOP le rPCOP à définir
   */
  public void setRPCOP(String rPCOP) {
    RPCOP = rPCOP;
  }
  
  /**
   * @return le rPCDP
   */
  public int getRPCDP() {
    return RPCDP;
  }
  
  /**
   * @param rPCDP le rPCDP à définir
   */
  public void setRPCDP(int rPCDP) {
    RPCDP = rPCDP;
  }
  
  /**
   * @return le rPTYP
   */
  public String getRPTYP() {
    return RPTYP;
  }
  
  /**
   * @param rPTYP le rPTYP à définir
   */
  public void setRPTYP(String rPTYP) {
    RPTYP = rPTYP;
  }
  
  /**
   * @return le rPCLK
   */
  public String getRPCLK() {
    return RPCLK;
  }
  
  /**
   * @param rPCLK le rPCLK à définir
   */
  public void setRPCLK(String rPCLK) {
    RPCLK = rPCLK;
  }
  
  /**
   * @return le rPTEL
   */
  public String getRPTEL() {
    return RPTEL;
  }
  
  /**
   * @param rPTEL le rPTEL à définir
   */
  public void setRPTEL(String rPTEL) {
    RPTEL = rPTEL;
  }
  
  /**
   * @return le rPFAX
   */
  public String getRPFAX() {
    return RPFAX;
  }
  
  /**
   * @param rPFAX le rPFAX à définir
   */
  public void setRPFAX(String rPFAX) {
    RPFAX = rPFAX;
  }
  
  /**
   * @return le rPNET
   */
  public String getRPNET() {
    return RPNET;
  }
  
  /**
   * @param rPNET le rPNET à définir
   */
  public void setRPNET(String rPNET) {
    RPNET = rPNET;
  }
  
  /**
   * @return le rPOBS
   */
  public String getRPOBS() {
    return RPOBS;
  }
  
  /**
   * @param rPOBS le rPOBS à définir
   */
  public void setRPOBS(String rPOBS) {
    RPOBS = rPOBS;
  }
  
  /**
   * @return le rPNOF
   */
  public int getRPNOF() {
    return RPNOF;
  }
  
  /**
   * @param rPNOF le rPNOF à définir
   */
  public void setRPNOF(int rPNOF) {
    RPNOF = rPNOF;
  }
  
  /**
   * @return le rPGEO
   */
  public String getRPGEO() {
    return RPGEO;
  }
  
  /**
   * @param rPGEO le rPGEO à définir
   */
  public void setRPGEO(String rPGEO) {
    RPGEO = rPGEO;
  }
  
  /**
   * @return le rPMAG
   */
  public String getRPMAG() {
    return RPMAG;
  }
  
  /**
   * @param rPMAG le rPMAG à définir
   */
  public void setRPMAG(String rPMAG) {
    RPMAG = rPMAG;
  }
  
  /**
   * @return le rPCNC
   */
  public String getRPCNC() {
    return RPCNC;
  }
  
  /**
   * @param rPCNC le rPCNC à définir
   */
  public void setRPCNC(String rPCNC) {
    RPCNC = rPCNC;
  }
  
  /**
   * @return le rPMOC
   */
  public int getRPMOC() {
    return RPMOC;
  }
  
  /**
   * @param rPMOC le rPMOC à définir
   */
  public void setRPMOC(int rPMOC) {
    RPMOC = rPMOC;
  }
  
  /**
   * @return le rPSEU
   */
  public int getRPSEU() {
    return RPSEU;
  }
  
  /**
   * @param rPSEU le rPSEU à définir
   */
  public void setRPSEU(int rPSEU) {
    RPSEU = rPSEU;
  }
  
  /**
   * @return le rPCOM
   */
  public int getRPCOM() {
    return RPCOM;
  }
  
  /**
   * @param rPCOM le rPCOM à définir
   */
  public void setRPCOM(int rPCOM) {
    RPCOM = rPCOM;
  }
  
  /**
   * @return le rPO01
   */
  public int getRPO01() {
    return RPO01;
  }
  
  /**
   * @param rPO01 le rPO01 à définir
   */
  public void setRPO01(int rPO01) {
    RPO01 = rPO01;
  }
  
  /**
   * @return le rPO02
   */
  public int getRPO02() {
    return RPO02;
  }
  
  /**
   * @param rPO02 le rPO02 à définir
   */
  public void setRPO02(int rPO02) {
    RPO02 = rPO02;
  }
  
  /**
   * @return le rPO03
   */
  public int getRPO03() {
    return RPO03;
  }
  
  /**
   * @param rPO03 le rPO03 à définir
   */
  public void setRPO03(int rPO03) {
    RPO03 = rPO03;
  }
  
  /**
   * @return le rPO04
   */
  public int getRPO04() {
    return RPO04;
  }
  
  /**
   * @param rPO04 le rPO04 à définir
   */
  public void setRPO04(int rPO04) {
    RPO04 = rPO04;
  }
  
  /**
   * @return le rPO05
   */
  public int getRPO05() {
    return RPO05;
  }
  
  /**
   * @param rPO05 le rPO05 à définir
   */
  public void setRPO05(int rPO05) {
    RPO05 = rPO05;
  }
  
  /**
   * @return le rPO06
   */
  public int getRPO06() {
    return RPO06;
  }
  
  /**
   * @param rPO06 le rPO06 à définir
   */
  public void setRPO06(int rPO06) {
    RPO06 = rPO06;
  }
  
  /**
   * @return le rPO07
   */
  public int getRPO07() {
    return RPO07;
  }
  
  /**
   * @param rPO07 le rPO07 à définir
   */
  public void setRPO07(int rPO07) {
    RPO07 = rPO07;
  }
  
  /**
   * @return le rPO08
   */
  public int getRPO08() {
    return RPO08;
  }
  
  /**
   * @param rPO08 le rPO08 à définir
   */
  public void setRPO08(int rPO08) {
    RPO08 = rPO08;
  }
  
  /**
   * @return le rPO09
   */
  public int getRPO09() {
    return RPO09;
  }
  
  /**
   * @param rPO09 le rPO09 à définir
   */
  public void setRPO09(int rPO09) {
    RPO09 = rPO09;
  }
  
  /**
   * @return le rPO10
   */
  public int getRPO10() {
    return RPO10;
  }
  
  /**
   * @param rPO10 le rPO10 à définir
   */
  public void setRPO10(int rPO10) {
    RPO10 = rPO10;
  }
  
  /**
   * @return le rPO11
   */
  public int getRPO11() {
    return RPO11;
  }
  
  /**
   * @param rPO11 le rPO11 à définir
   */
  public void setRPO11(int rPO11) {
    RPO11 = rPO11;
  }
  
  /**
   * @return le rPO12
   */
  public int getRPO12() {
    return RPO12;
  }
  
  /**
   * @param rPO12 le rPO12 à définir
   */
  public void setRPO12(int rPO12) {
    RPO12 = rPO12;
  }
  
  /**
   * @return le rPCO2
   */
  public int getRPCO2() {
    return RPCO2;
  }
  
  /**
   * @param rPCO2 le rPCO2 à définir
   */
  public void setRPCO2(int rPCO2) {
    RPCO2 = rPCO2;
  }
  
  /**
   * @return le rPTYA
   */
  public String getRPTYA() {
    return RPTYA;
  }
  
  /**
   * @param rPTYA le rPTYA à définir
   */
  public void setRPTYA(String rPTYA) {
    RPTYA = rPTYA;
  }
  
  /**
   * @return le rPNTA
   */
  public int getRPNTA() {
    return RPNTA;
  }
  
  /**
   * @param rPNTA le rPNTA à définir
   */
  public void setRPNTA(int rPNTA) {
    RPNTA = rPNTA;
  }
  
  /**
   * @return le rPLEM
   */
  public int getRPLEM() {
    return RPLEM;
  }
  
  /**
   * @param rPLEM le rPLEM à définir
   */
  public void setRPLEM(int rPLEM) {
    RPLEM = rPLEM;
  }
  
  /**
   * @return le rPSAN
   */
  public String getRPSAN() {
    return RPSAN;
  }
  
  /**
   * @param rPSAN le rPSAN à définir
   */
  public void setRPSAN(String rPSAN) {
    RPSAN = rPSAN;
  }
  
  /**
   * @return le rPNSA
   */
  public int getRPNSA() {
    return RPNSA;
  }
  
  /**
   * @param rPNSA le rPNSA à définir
   */
  public void setRPNSA(int rPNSA) {
    RPNSA = rPNSA;
  }
  
  /**
   * @return le rPIN1
   */
  public char getRPIN1() {
    return RPIN1;
  }
  
  /**
   * @param rPIN1 le rPIN1 à définir
   */
  public void setRPIN1(char rPIN1) {
    RPIN1 = rPIN1;
  }
  
  /**
   * @return le rPIN2
   */
  public char getRPIN2() {
    return RPIN2;
  }
  
  /**
   * @param rPIN2 le rPIN2 à définir
   */
  public void setRPIN2(char rPIN2) {
    RPIN2 = rPIN2;
  }
  
  /**
   * @return le rPIN3
   */
  public char getRPIN3() {
    return RPIN3;
  }
  
  /**
   * @param rPIN3 le rPIN3 à définir
   */
  public void setRPIN3(char rPIN3) {
    RPIN3 = rPIN3;
  }
  
  /**
   * @return le rPIN4
   */
  public char getRPIN4() {
    return RPIN4;
  }
  
  /**
   * @param rPIN4 le rPIN4 à définir
   */
  public void setRPIN4(char rPIN4) {
    RPIN4 = rPIN4;
  }
  
  /**
   * @return le rPIN5
   */
  public char getRPIN5() {
    return RPIN5;
  }
  
  /**
   * @param rPIN5 le rPIN5 à définir
   */
  public void setRPIN5(char rPIN5) {
    RPIN5 = rPIN5;
  }
  
  /**
   * @return le rPVDE
   */
  public String getRPVDE() {
    return RPVDE;
  }
  
  /**
   * @param rPVDE le rPVDE à définir
   */
  public void setRPVDE(String rPVDE) {
    RPVDE = rPVDE;
  }
  
}
