/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0050o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_PONLI = 4;
  public static final int DECIMAL_PONLI = 0;
  public static final int SIZE_POIN21 = 1;
  public static final int SIZE_POTP1 = 2;
  public static final int SIZE_POTP2 = 2;
  public static final int SIZE_POTP3 = 2;
  public static final int SIZE_POTP4 = 2;
  public static final int SIZE_POTP5 = 2;
  public static final int SIZE_POLB = 124;
  public static final int SIZE_POCPL = 8;
  public static final int SIZE_POCPY = 1;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 159;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_PONLI = 1;
  public static final int VAR_POIN21 = 2;
  public static final int VAR_POTP1 = 3;
  public static final int VAR_POTP2 = 4;
  public static final int VAR_POTP3 = 5;
  public static final int VAR_POTP4 = 6;
  public static final int VAR_POTP5 = 7;
  public static final int VAR_POLB = 8;
  public static final int VAR_POCPL = 9;
  public static final int VAR_POCPY = 10;
  public static final int VAR_POARR = 11;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal ponli = BigDecimal.ZERO; // N° de ligne
  private String poin21 = ""; // Ind. regroupement ligne
  private String potp1 = ""; // Top personnal.N°1
  private String potp2 = ""; // Top personnal.N°2
  private String potp3 = ""; // Top personnal.N°3
  private String potp4 = ""; // Top personnal.N°4
  private String potp5 = ""; // Top personnal.N°5
  private String polb = ""; // Libellés
  private String pocpl = ""; // Complément de libellé
  private String pocpy = ""; // Copie commentaire dans Achat
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_PONLI, DECIMAL_PONLI), // N° de ligne
      new AS400Text(SIZE_POIN21), // Ind. regroupement ligne
      new AS400Text(SIZE_POTP1), // Top personnal.N°1
      new AS400Text(SIZE_POTP2), // Top personnal.N°2
      new AS400Text(SIZE_POTP3), // Top personnal.N°3
      new AS400Text(SIZE_POTP4), // Top personnal.N°4
      new AS400Text(SIZE_POTP5), // Top personnal.N°5
      new AS400Text(SIZE_POLB), // Libellés
      new AS400Text(SIZE_POCPL), // Complément de libellé
      new AS400Text(SIZE_POCPY), // Copie commentaire dans Achat
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { poind, ponli, poin21, potp1, potp2, potp3, potp4, potp5, polb, pocpl, pocpy, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    ponli = (BigDecimal) output[1];
    poin21 = (String) output[2];
    potp1 = (String) output[3];
    potp2 = (String) output[4];
    potp3 = (String) output[5];
    potp4 = (String) output[6];
    potp5 = (String) output[7];
    polb = (String) output[8];
    pocpl = (String) output[9];
    pocpy = (String) output[10];
    poarr = (String) output[11];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPonli(BigDecimal pPonli) {
    if (pPonli == null) {
      return;
    }
    ponli = pPonli.setScale(DECIMAL_PONLI, RoundingMode.HALF_UP);
  }
  
  public void setPonli(Integer pPonli) {
    if (pPonli == null) {
      return;
    }
    ponli = BigDecimal.valueOf(pPonli);
  }
  
  public Integer getPonli() {
    return ponli.intValue();
  }
  
  public void setPoin21(Character pPoin21) {
    if (pPoin21 == null) {
      return;
    }
    poin21 = String.valueOf(pPoin21);
  }
  
  public Character getPoin21() {
    return poin21.charAt(0);
  }
  
  public void setPotp1(String pPotp1) {
    if (pPotp1 == null) {
      return;
    }
    potp1 = pPotp1;
  }
  
  public String getPotp1() {
    return potp1;
  }
  
  public void setPotp2(String pPotp2) {
    if (pPotp2 == null) {
      return;
    }
    potp2 = pPotp2;
  }
  
  public String getPotp2() {
    return potp2;
  }
  
  public void setPotp3(String pPotp3) {
    if (pPotp3 == null) {
      return;
    }
    potp3 = pPotp3;
  }
  
  public String getPotp3() {
    return potp3;
  }
  
  public void setPotp4(String pPotp4) {
    if (pPotp4 == null) {
      return;
    }
    potp4 = pPotp4;
  }
  
  public String getPotp4() {
    return potp4;
  }
  
  public void setPotp5(String pPotp5) {
    if (pPotp5 == null) {
      return;
    }
    potp5 = pPotp5;
  }
  
  public String getPotp5() {
    return potp5;
  }
  
  public void setPolb(String pPolb) {
    if (pPolb == null) {
      return;
    }
    polb = pPolb;
  }
  
  public String getPolb() {
    return polb;
  }
  
  public void setPocpl(String pPocpl) {
    if (pPocpl == null) {
      return;
    }
    pocpl = pPocpl;
  }
  
  public String getPocpl() {
    return pocpl;
  }
  
  public void setPocpy(Character pPocpy) {
    if (pPocpy == null) {
      return;
    }
    pocpy = String.valueOf(pPocpy);
  }
  
  public Character getPocpy() {
    return pocpy.charAt(0);
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
