/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi.RecupFluxEDI;
import ri.serien.libas400.dao.sql.gescom.flux.flx012.v2.GestionFLX012;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.flux.DeclenchementFluxAutomatique;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.exploitation.personnalisation.flux.PersonnalisationFlux;
import ri.serien.libcommun.exploitation.personnalisation.flux.TempsAttenteDeclenchement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe traite les flux automatiques dont le déclenchement a été planifié dans le paramètre "FX" :
 * - soit à une fréquence données,
 * - soit à une heure fixe,
 * - soit à une date et heure fixe,
 * - soit à une fréquence donnée.
 */
public class FluxAutomatique extends Thread {
  // Variables
  private PersonnalisationFlux personnalisationFlux = null;
  private int sensFlux = -1;
  private DeclenchementFluxAutomatique declenchementFluxAutomatique = null;
  private ManagerFluxMagento managerFluxMagento = null;
  private FluxMagento flux = null;
  private RecupFluxEDI recuperationCdesEDI = null;
  
  /**
   * Constructeur.
   */
  public FluxAutomatique(PersonnalisationFlux pPersonnalisationFlux, boolean pDeclenchementImmediat) {
    if (pPersonnalisationFlux == null) {
      throw new MessageErreurException("La personnalisation du flux est invalide.");
    }
    personnalisationFlux = pPersonnalisationFlux;
    List<TempsAttenteDeclenchement> listeTempsAttente = null;
    
    // Initialisation du sens en fonction du type du flux
    switch (personnalisationFlux.getTypeFlux()) {
      case STOCK:
        sensFlux = FluxMagento.SNS_VERS_MAGENTO;
        // Le stock peut avoir 2 types de déclenchement :
        // - avec la fréquence pour les mouvements de stock
        // - avec une heure fixe pour les stocks complets (stock
        listeTempsAttente = personnalisationFlux.getTempsAttenteAvecFrequenceEtDate();
        if (listeTempsAttente != null && !listeTempsAttente.isEmpty()) {
          declenchementFluxAutomatique = new DeclenchementFluxAutomatique(listeTempsAttente);
        }
        break;
      case COMMANDE_EDI:
        sensFlux = FluxMagento.SNS_EDI_RECEP;
        listeTempsAttente = personnalisationFlux.getTempsAttenteAvecFrequence();
        if (listeTempsAttente != null && !listeTempsAttente.isEmpty()) {
          declenchementFluxAutomatique = new DeclenchementFluxAutomatique(listeTempsAttente);
        }
        break;
      case FACTURE_YOOZ:
        sensFlux = FluxMagento.SNS_EDI_RECEP;
        listeTempsAttente = personnalisationFlux.getTempsAttenteAvecFrequence();
        if (listeTempsAttente != null && !listeTempsAttente.isEmpty()) {
          declenchementFluxAutomatique = new DeclenchementFluxAutomatique(listeTempsAttente);
        }
        break;
      case PRIX:
        sensFlux = FluxMagento.SNS_VERS_MAGENTO;
        listeTempsAttente = personnalisationFlux.getTempsAttenteAvecDate();
        if (listeTempsAttente != null && !listeTempsAttente.isEmpty()) {
          declenchementFluxAutomatique = new DeclenchementFluxAutomatique(listeTempsAttente);
        }
        break;
      default:
        sensFlux = -1;
    }
    
    // Si le flux doit être déclenché immédiatement (dans 1 seconde)
    if (pDeclenchementImmediat) {
      if (listeTempsAttente == null) {
        listeTempsAttente = new ArrayList<TempsAttenteDeclenchement>();
      }
      listeTempsAttente.clear();
      listeTempsAttente.add(new TempsAttenteDeclenchement(1 * 1000));
      declenchementFluxAutomatique = new DeclenchementFluxAutomatique(listeTempsAttente);
    }
  }
  
  // -- Méthodes publiques
  
  /**
   * Indique si un type de flux peut être un flux automatique.
   */
  public static boolean isFluxAutomatique(EnumTypeFlux pEnumFlux) {
    if (pEnumFlux == null) {
      return false;
    }
    
    switch (pEnumFlux) {
      case STOCK:
        return true;
      case COMMANDE_EDI:
        return true;
      case FACTURE_YOOZ:
        return true;
      case PRIX:
        return true;
      default:
        return false;
    }
  }
  
  /**
   * Initialiser les données essentielles de ce flux qui ne peuvent être récupérés à partir du paramétrage.
   */
  public void initialiser(ManagerFluxMagento pDaoFluxMagento) {
    // Ajout d'un enregistrement
    managerFluxMagento = pDaoFluxMagento;
    if (managerFluxMagento == null) {
      return;
    }
    
    // Initialisation des champs
    flux = managerFluxMagento.getRecord(true);
    if (flux == null) {
      Trace.erreur("Le flux automatique " + personnalisationFlux.getTypeFlux() + " est invalide.");
      return;
    }
    flux.setFluxAutomatique(true);
    flux.setFLETB(personnalisationFlux.getId().getCodeEtablissement());
    flux.setFLVER(String.valueOf(managerFluxMagento.getVersionFlux()));
    flux.setFLIDF(personnalisationFlux.getTypeFlux().getCode());
    flux.setFLSNS(sensFlux);
    flux.setFLSTT(EnumStatutFlux.A_TRAITER);
    
    setName("Flux automatique " + personnalisationFlux.getTypeFlux() + " : " + getId());
  }
  
  /**
   * Action du thread de flux récurrent.
   */
  @Override
  public void run() {
    EnumTypeFlux enumTypeFlux = personnalisationFlux.getTypeFlux();
    boolean actif = true;
    
    while (isAlive() && actif) {
      try {
        // Délai d'attente en seconde
        int tempsAttente = declenchementFluxAutomatique.getTempsAttente();
        String libelle = declenchementFluxAutomatique.getLibelle();
        Trace.info("Mise en pause du flux " + enumTypeFlux + " pendant " + tempsAttente + " (" + libelle + ").");
        sleep(tempsAttente);
        
        // Contrôle que le traitement est possible
        if (flux == null || managerFluxMagento == null || !managerFluxMagento.controlerStatutBaseDeDonnees()) {
          Trace.erreur("Erreur lors du traitement du flux automatique " + enumTypeFlux + ".");
          continue;
        }
        
        switch (enumTypeFlux) {
          // Gestion de l'EDI
          case COMMANDE_EDI:
            if (recuperationCdesEDI == null) {
              recuperationCdesEDI = new RecupFluxEDI(managerFluxMagento, personnalisationFlux.getId().getCodeEtablissement());
            }
            // On va scanner le serveur EDI distant afin de créer un flux par commande dispo
            ArrayList<FluxMagento> listeEDI = recuperationCdesEDI.recupererCommandesEDI(managerFluxMagento.getParametresFlux());
            if (listeEDI != null) {
              // On crée un enregistrement par commande afin de dissocier leur traitement
              for (int i = 0; i < listeEDI.size(); i++) {
                managerFluxMagento.insererFluxDansTableNonTraite(listeEDI.get(i));
              }
            }
            break;
          
          // Gestion des factures Yooz
          case FACTURE_YOOZ:
            GestionFLX012 gestionFLX012 = new GestionFLX012(managerFluxMagento.getParametresFlux(), managerFluxMagento.getQueryManager(),
                managerFluxMagento.getSystemeManager());
            
            // Méthode globale récupérer des factures Yooz
            ArrayList<FluxMagento> listeFactures = gestionFLX012.recupererFacturesYooz(this);
            if (listeFactures != null) {
              // On crée un enregistrement par commande afin de dissocier leur traitement
              for (int i = 0; i < listeFactures.size(); i++) {
                managerFluxMagento.insererFluxDansTableNonTraite(listeFactures.get(i));
              }
            }
            break;
          
          // Les autres types de flux
          default:
            // Insertion des flux automatiques dans la mesure où il n'en existe pas déjà des non traités
            if (managerFluxMagento.existeDesFluxIdentiquesNonTraites(flux.getFLIDF())) {
              actif = true;
              continue;
            }
            // Insertion du flux dans la table PSEMHFLXM
            flux.setFLJSN(libelle);
            actif = managerFluxMagento.insererFluxDansTableNonTraite(flux);
            if (!actif) {
              Trace.erreur("Le flux automatique " + enumTypeFlux + " est invalide : " + managerFluxMagento.getMsgError());
              continue;
            }
            Trace.info("Le flux automatique " + enumTypeFlux + " a été inséré dans la table " + EnumTableBDD.FLUX + '.');
        }
      }
      catch (InterruptedException e) {
        actif = false;
        Thread.currentThread().interrupt();
        Trace.erreur(e, "Erreur lors du traitement d'un flux automatique " + enumTypeFlux + ".");
        break;
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      
      // Fin des traitements si actif est à false
      if (!actif) {
        continue;
      }
      
      // Calcul du temps d'attente du prochain déclenchement
      actif = declenchementFluxAutomatique.calculerProchainDeclenchement();
    }
    
    Trace.info("Sortie de la boucle d'attente de l'exécution du flux automatique " + enumTypeFlux.getCode());
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le code du flux.
   */
  public String getIdFlux() {
    return personnalisationFlux.getTypeFlux().getCode();
  }
  
  /**
   * Retourne le code étabmlissement du flux.
   */
  public String getCodeEtablissement() {
    return personnalisationFlux.getId().getCodeEtablissement();
  }
  
  /**
   * Retourne si le flux est à démarrer.
   */
  public boolean isADemarrer() {
    boolean actif = false;
    if (personnalisationFlux.getActif() == null || !personnalisationFlux.getActif().booleanValue()) {
      actif = false;
    }
    else if (declenchementFluxAutomatique == null) {
      actif = false;
    }
    else if (sensFlux != -1 && managerFluxMagento != null && flux != null) {
      actif = true;
    }
    else {
      actif = false;
    }
    return actif;
  }
  
  /**
   * Retourne le code FTP de la personnalisation du flux.
   */
  public String getCodeFTP() {
    return personnalisationFlux.getCodeFTP();
  }
  
  /**
   * Retourne le flux.
   */
  public FluxMagento getFlux() {
    return flux;
  }
  
}
