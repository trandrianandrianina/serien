/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx005.v2;

import java.io.File;
import java.util.ArrayList;

import com.google.gson.JsonSyntaxException;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.JsonFactureMagentoV2;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.fichier.ConvertFileToBytes;

public class ExportFactureMagentoV2 extends GM_Export_FacturesV2 {
  private ConvertFileToBytes convert = null;
  
  public ExportFactureMagentoV2(QueryManager aquerymg, ParametresFluxBibli pParam) {
    super(aquerymg, pParam);
  }
  
  /**
   * On retourne sous forme binaire une facture dans le JSON
   */
  @Override
  public String retournerUneFactureJSON(FluxMagento record) {
    if (record == null) {
      majError("[ExportFactures] retournerUneFactureJSON() record null ou corrompu");
      return null;
    }
    
    if (record.getFLETB() == null || record.getFLCOD() == null) {
      record.construireMessageErreur("[ExportFactures] retournerUneFactureJSON() Etb ou code à NULL");
      return null;
    }
    
    String retour = null;
    
    String numFacture = isolerNumeroFacture(record, record.getFLCOD().trim());
    if (numFacture == null) {
      return null;
    }
    
    // Contrôler s'il s'agit bien d'une facture existante SERIE N
    String requete = "SELECT E1ETB, E1NFA, E1CCT, E1AVR FROM " + queryManager.getLibrary() + ".PGVMEBCM WHERE  E1COD = 'E' AND E1ETB = '"
        + record.getFLETB() + "' AND E1NFA = '" + numFacture + "' AND (E1ETA = 6 OR E1ETA = 7) ";
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    // Contrôler si cette facture existe bien dans le bon dossier
    if (liste != null && liste.size() == 1) {
      // On contrôle d'abord s'il s'agit bien d'une facture
      if (!controlerTypeDocument(liste.get(0))) {
        record.construireMessageErreur("Ce document est un avoir");
        return "";
      }
      
      String chemin = recupererLeBonCheminFacture(record);
      if (chemin != null) {
        String facture = recupererLeBonNomFacturePDF(record, record.getFLETB(), numFacture);
        if (facture != null) {
          chemin = chemin + facture;
          
          // On checke qu'il s'agisse bien d'un Fichier valide
          File fichierPhysique = new File(chemin);
          if (!fichierPhysique.isFile()) {
            record.construireMessageErreur("Le fichier " + chemin + " n'existe pas ");
            return null;
          }
          
          if (convert == null) {
            convert = new ConvertFileToBytes();
          }
          try {
            
            retour = convert.encodeFileToBase64Binary(chemin);
            // TODO Il faudra un peu mieux contrôler tout ce bouzin
            if (retour != null && retour.trim().length() > 10) {
              // Si la conversion s'est bien déroulée, créer un nouvel objet FactureMagento pour le JSOniser
              JsonFactureMagentoV2 objetFacture = new JsonFactureMagentoV2(record);
              Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), objetFacture.getEtb());
              if (numeroInstance == null) {
                majError("[GM_Export_Clients] retournerUneFactureJSON() : probleme d'interpretation de l'ETb vers l'instance Magento");
                return null;
              }
              objetFacture.setIdInstanceMagento(numeroInstance);
              
              objetFacture.setNomFacture(facture);
              // TODO Il va falloir contrôler l'existence de cette référence Magento E1CCT avant même d'envoyer la facture
              if (liste.get(0).isPresentField("E1CCT")) {
                objetFacture.setCommandeMagento(liste.get(0).getField("E1CCT").toString().trim());
              }
              
              objetFacture.setFactureClient(retour);
              
              // debut de la construction du JSON
              if (initJSON()) {
                // retour = "";
                if (objetFacture.getNomFacture() != null && objetFacture.getIdInstanceMagento() > 0
                    && objetFacture.getCommandeMagento() != null) {
                  retour = gson.toJson(objetFacture);
                }
                else {
                  record.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() valeurs ESSENTIELLES à NULL");
                }
              }
            }
            else {
              record.construireMessageErreur("[GM_Export_Factures] PB convert: " + retour);
            }
          }
          catch (JsonSyntaxException e) {
            record.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() PB PARSE Objet JSON: " + e.getMessage());
            return null;
          }
        }
      }
      else {
        record.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() PB de recup du chemin de facture SERIE N : " + " / "
            + record.getFLETB() + " / " + record.getFLCOD());
      }
    }
    else {
      record.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() PB de recup de facture SERIE N : " + " / "
          + queryManager.getMsgError() + " / " + requete);
    }
    
    return retour;
  }
}
