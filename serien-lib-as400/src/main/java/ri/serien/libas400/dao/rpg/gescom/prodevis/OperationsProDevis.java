/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.prodevis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.dataarea.GestionDataareaAS400;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.prodevis.CriteresRechercheProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.DescriptionProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.EnumStatutProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.InformationsConfigurationProDevis;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class OperationsProDevis {
  
  // Constantes
  // Nom de la dataarea qui se trouve dans la curlib
  protected static final String NOM_DATAAREA = "PGVMPRODEV";
  protected static final int LONGUEUR_DATAAREA = 500;
  
  // Variables
  private SystemeManager systeme = null;
  private QueryManager querymg = null;
  private InformationsConfigurationProDevis informationsConfiguration = null;
  
  /**
   * Constructeur.
   */
  public OperationsProDevis(SystemeManager pSysteme, QueryManager pQuerymg) {
    systeme = pSysteme;
    querymg = pQuerymg;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne les descriptions des documents prodevis.
   */
  public List<DescriptionProDevis> chargerListeDescriptionProDevis(CriteresRechercheProDevis pCriteres) {
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères de recherche des Pro-Devis sont invalides.");
    }
    String codeEtablissement = Constantes.normerTexte(pCriteres.getCodeEtablissement());
    if (codeEtablissement.isEmpty()) {
      throw new MessageErreurException("Le code établissement est vide.");
    }
    if (systeme == null) {
      throw new MessageErreurException("La connexion avec le serveur n'est pas initialisée.");
    }
    
    // Récupération des informations dans la dataarea
    lireInformationsTraitement(systeme.getCurlib());
    if (informationsConfiguration == null) {
      throw new MessageErreurException(
          "Vous ne pouvez pas importer de devis Pro-Devis car les éléments nécessaires à ce traitement n’existent pas."
              + "\nMerci de consulter la documentation.");
    }
    if (informationsConfiguration.getNomDossierIFS().isEmpty()) {
      throw new MessageErreurException("Le nom du dossier où sont stockés les fichiers Pro-Devis n'est pas renseigné.");
    }
    
    miseAJourListeProDevis(pCriteres.getIdEtablissement());
    
    return rechercherDescriptionProDevis(pCriteres);
  }
  
  /**
   * Importation d'un devis prodevis dans Série N (injection).
   */
  public IdDocumentVente importerDevis(DescriptionProDevis pDescription, IdClient pIdClient) {
    RpgInjecterProDevis injection = new RpgInjecterProDevis();
    return injection.injecterDocumentProDevis(systeme, pDescription, pIdClient);
  }
  
  /**
   * Met à jour la liste des devis prodevis présents sur l'IFS
   */
  public void miseAJourListeProDevis(IdEtablissement pIdEtablissement) {
    RpgMiseAJourListeProDevis maj = new RpgMiseAJourListeProDevis();
    maj.mettreAJourListeDocumentProDevis(systeme, pIdEtablissement);
  }
  
  /**
   * Lecture des informations nécessaires au traitement des pro-devis.
   */
  public void lireInformationsTraitement(String pCurlib) {
    try {
      Dataarea dataarea = lireDataArea(pCurlib);
      if (dataarea != null && dataarea.getValeur() != null && dataarea.getValeur().length() > 0) {
        analyseInformationsDataArea(dataarea);
      }
    }
    catch (Exception e) {
      throw new MessageErreurException(e,
          "Il y a eu une erreur lors de la lecture de la dataarea contenant des informations" + " pour Pro-Devis .");
    }
  }
  
  /**
   * Liste le dossier contenant les fichiers de pro-devis à importer dans Série N.
   * L'extension peut être avec ou sans point.
   */
  /*
  public String[] listerFichiersAImporter(String pDossier, String pExtension) {
    pDossier = Constantes.normerTexte(pDossier);
    if (pDossier.isEmpty()) {
      throw new MessageErreurException("Le chemin du dossier passé en paramètre est vide.");
    }
    pExtension = Constantes.normerTexte(pExtension);
    if (pExtension.isEmpty()) {
      throw new MessageErreurException("L'extension pour filtrer les fichiers à importer est vide.");
    }
    
    // Construction du chemin pour accéder au dossier
    File dossier = new File(pDossier);
    if (!dossier.exists()) {
      throw new MessageErreurException("Le dossier " + dossier.getAbsolutePath() + ", où sont stockés les Pro-Devis, n'existe pas.");
    }
    
    // On controle l'extension
    if (pExtension.charAt(0) != '.') {
      pExtension = '.' + pExtension;
    }
    final String extension = pExtension;
    
    // On liste le dossier
    return dossier.list(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.endsWith(extension);
      }
    });
  }*/
  
  /**
   * Extraction de la description des fichiers textes Pro-Devis dans un fichier base de données de Série N,
   * afin d'extraire les informations principales.
   */
  public boolean extraireDescriptionsFichiersTextes(String[] pListeFichiers, IdEtablissement pIdEtablissement) {
    String pCodeEtablissement = "";
    if (pIdEtablissement != null) {
      pCodeEtablissement = Constantes.normerTexte(pIdEtablissement.getCodeEtablissement());
    }
    if (pCodeEtablissement.isEmpty()) {
      throw new MessageErreurException("L'établissement passé en paramètre est vide.");
    }
    if (pListeFichiers == null) {
      throw new MessageErreurException("La liste des fichiers à importer est à null.");
    }
    if (systeme == null) {
      throw new MessageErreurException("La variable 'systeme' est à null.");
    }
    
    boolean retour = true;
    ContexteProgrammeRPG rpg = new ContexteProgrammeRPG(systeme.getSystem(), systeme.getCurlib());
    rpg.executerCommande("CHGDTAARA DTAARA(*LDA (29 10)) VALUE(" + String.format("'%-10s'", systeme.getProfilUtilisateur()) + ")");
    rpg.initialiserLettreEnvironnement(EnvironnementExecution.getLettreEnvironnement());
    rpg.ajouterBibliotheque(systeme.getCurlib(), true);
    rpg.ajouterBibliotheque(systeme.getLibenv(), false);
    rpg.ajouterBibliotheque(EnvironnementExecution.getExpas(), false);
    rpg.ajouterBibliotheque(EnvironnementExecution.getGvmas(), false);
    rpg.ajouterBibliotheque(EnvironnementExecution.getGvmx(), false);
    
    // Exécution du programme RPG d'importation pour chaque fichier trouvé
    for (String fichier : pListeFichiers) {
      fichier = Constantes.normerTexte(fichier);
      if (fichier.isEmpty()) {
        continue;
      }
      String programme = String.format("CALL PGM(SGVM9GC3) PARM('%-" + InformationsConfigurationProDevis.TAILLE_NOM_DOSSIER + "s' '%-"
          + IdEtablissement.LONGUEUR_CODE_ETABLISSEMENT + "s')", fichier, pCodeEtablissement);
      if (!rpg.executerCommande(programme)) {
        retour = false;
      }
    }
    
    return retour;
  }
  
  /**
   * Recherche les prodevis correspondants aux critères voulus.
   */
  public List<DescriptionProDevis> rechercherDescriptionProDevis(CriteresRechercheProDevis pCriteres) {
    if (querymg == null) {
      throw new MessageErreurException("La variable 'querymg' n'est pas initialisée.");
    }
    
    String requete = "select * from " + querymg.getLibrary() + '.' + EnumTableBDD.DESCRIPTION_PRODEVIS + " where PRDETB = "
        + RequeteSql.formaterStringSQL(pCriteres.getCodeEtablissement()) + " AND PRDSTA <> 2 ORDER BY PRDNUM";
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    List<DescriptionProDevis> listeDescriptions = new ArrayList<DescriptionProDevis>();
    for (GenericRecord record : listeRecord) {
      DescriptionProDevis description = new DescriptionProDevis();
      if (record != null) {
        initialiserDescription(record, description);
        if (description.getStatut().equals(EnumStatutProDevis.ATTENTE)) {
          listeDescriptions.add(description);
        }
      }
    }
    for (GenericRecord record : listeRecord) {
      DescriptionProDevis description = new DescriptionProDevis();
      if (record != null) {
        initialiserDescription(record, description);
        // pour chaque ligne, il faut lire s'il existe avec le même PRDETB+PRDNUM un autre erl avec PRDSTA=1 (importé)
        if (description.getStatut().equals(EnumStatutProDevis.IMPORTE)) {
          for (int i = 0; i < listeDescriptions.size(); i++) {
            DescriptionProDevis descriptionProDevis = listeDescriptions.get(i);
            // si c'est le cas récupérer l'identifiant du bon de la dernière importation pour récupérer la date de dernière import
            if (descriptionProDevis.getEtablissement().equals(description.getEtablissement())
                && descriptionProDevis.getNumero().equals(description.getNumero())) {
              descriptionProDevis.setDateImport(description.getDateImport());
              listeDescriptions.set(i, descriptionProDevis);
              break;
            }
          }
        }
      }
    }
    
    return listeDescriptions;
  }
  
  // -- Méthodes privées
  
  /**
   * Lecture du contenu de la dataarea.
   */
  private Dataarea lireDataArea(String pCurlib) {
    if (systeme == null) {
      throw new MessageErreurException("La variable 'systeme' est à null.");
    }
    Dataarea dataarea = new Dataarea();
    dataarea.setNom(NOM_DATAAREA);
    Bibliotheque bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(pCurlib), EnumTypeBibliotheque.BASE_DE_DONNEES);
    dataarea.setBibliotheque(bibliotheque);
    dataarea.setLongueur(LONGUEUR_DATAAREA);
    GestionDataareaAS400 operation = new GestionDataareaAS400(systeme.getSystem());
    dataarea.setValeur(operation.chargerDataarea(dataarea));
    return dataarea;
  }
  
  /**
   * Alimentation de la classe DonneesTraitementProDevis avec la chaine de la dataarea.
   */
  private void analyseInformationsDataArea(Dataarea dataarea) {
    if (dataarea == null) {
      throw new MessageErreurException("La classe Dataarea pour " + NOM_DATAAREA + " n'est pas instanciée.");
    }
    if (dataarea.getValeur() == null) {
      throw new MessageErreurException("Le contenu de la dataarea " + NOM_DATAAREA + " est nul.");
    }
    informationsConfiguration = new InformationsConfigurationProDevis();
    if (dataarea.getValeur().length() < LONGUEUR_DATAAREA) {
      throw new MessageErreurException("La dataarea " + NOM_DATAAREA + " a une longueur inférieure à " + LONGUEUR_DATAAREA);
    }
    informationsConfiguration.initialiserVariables(dataarea.getValeur(), dataarea.getLongueur());
  }
  
  /**
   * Initialise une description prodevis à partir de l'enregistrement lu en table.
   */
  private void initialiserDescription(GenericRecord record, DescriptionProDevis pDescription) {
    pDescription.setEtablissement(IdEtablissement.getInstance(record.getStringValue("PRDETB", "", true)));
    pDescription.setCleUnique(record.getIntValue("PRDKEY", 0));
    pDescription.setStatut(EnumStatutProDevis.valueOfByCode(record.getIntValue("PRDSTA", 0)));
    pDescription.setUtilisateurChangementStatut(record.getStringValue("PRDUSR", "", true));
    pDescription.setNumero(record.getStringValue("PRDNUM", "", true));
    pDescription.setNomFichier(record.getStringValue("PRDFIC", "", true));
    pDescription.setDateCreation(record.getDateValue("PRDDAT", 0));
    pDescription.setNomClient(record.getStringValue("PRDNOM", "", true));
    pDescription.setPrenomClient(record.getStringValue("PRDPRE", "", true));
    pDescription.setNombreTotalDArticles(record.getIntValue("PRDNBA", 0));
    pDescription.setMontantTotalHT(record.getBigDecimalValue("PRDMHT", BigDecimal.ZERO));
    pDescription.setDateImport(record.getDateValue("PRDDTI", 0));
    if (pDescription.getStatut().equals(EnumStatutProDevis.IMPORTE)) {
      pDescription.setIdDocumentImporte(
          IdDocumentVente.getInstanceGenerique(IdEtablissement.getInstance(record.getStringValue("PRDCDE", "", true)),
              EnumCodeEnteteDocumentVente.valueOfByCode(record.getCharacterValue("PRDCDC")), record.getIntValue("PRDCDN", 0),
              record.getIntValue("PRDCDS", 0), null));
    }
  }
}
