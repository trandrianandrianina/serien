/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0032o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_PONLID = 4;
  public static final int DECIMAL_PONLID = 0;
  public static final int SIZE_PONLIF = 4;
  public static final int DECIMAL_PONLIF = 0;
  public static final int SIZE_POTIT1 = 30;
  public static final int SIZE_POTIT2 = 30;
  public static final int SIZE_POTIT3 = 30;
  public static final int SIZE_POTIT4 = 30;
  public static final int SIZE_POTOT1 = 30;
  public static final int SIZE_POTOT2 = 30;
  public static final int SIZE_POTOT3 = 30;
  public static final int SIZE_POTOT4 = 30;
  public static final int SIZE_POEDTA = 1;
  public static final int SIZE_POEDTP = 1;
  public static final int SIZE_POTMHT = 13;
  public static final int DECIMAL_POTMHT = 2;
  public static final int SIZE_POTPRV = 13;
  public static final int DECIMAL_POTPRV = 2;
  public static final int SIZE_POTMAR = 13;
  public static final int DECIMAL_POTMAR = 2;
  public static final int SIZE_POPMAR = 5;
  public static final int DECIMAL_POPMAR = 2;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 305;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_PONLID = 1;
  public static final int VAR_PONLIF = 2;
  public static final int VAR_POTIT1 = 3;
  public static final int VAR_POTIT2 = 4;
  public static final int VAR_POTIT3 = 5;
  public static final int VAR_POTIT4 = 6;
  public static final int VAR_POTOT1 = 7;
  public static final int VAR_POTOT2 = 8;
  public static final int VAR_POTOT3 = 9;
  public static final int VAR_POTOT4 = 10;
  public static final int VAR_POEDTA = 11;
  public static final int VAR_POEDTP = 12;
  public static final int VAR_POTMHT = 13;
  public static final int VAR_POTPRV = 14;
  public static final int VAR_POTMAR = 15;
  public static final int VAR_POPMAR = 16;
  public static final int VAR_POARR = 17;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal ponlid = BigDecimal.ZERO; // Numéro de ligne début
  private BigDecimal ponlif = BigDecimal.ZERO; // Numéro de ligne fin
  private String potit1 = ""; // Titre Texte 1
  private String potit2 = ""; // Titre Texte 2
  private String potit3 = ""; // Titre Texte 3
  private String potit4 = ""; // Titre Texte 4
  private String potot1 = ""; // Total Texte 1
  private String potot2 = ""; // Total Texte 2
  private String potot3 = ""; // Total Texte 3
  private String potot4 = ""; // Total Texte 4
  private String poedta = ""; // Top édition détail articles
  private String poedtp = ""; // Top édition prix d"articles
  private BigDecimal potmht = BigDecimal.ZERO; // Montant total
  private BigDecimal potprv = BigDecimal.ZERO; // Total prix de revient
  private BigDecimal potmar = BigDecimal.ZERO; // Montant marge totale
  private BigDecimal popmar = BigDecimal.ZERO; // Pourcentage marge
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_PONLID, DECIMAL_PONLID), // Numéro de ligne début
      new AS400ZonedDecimal(SIZE_PONLIF, DECIMAL_PONLIF), // Numéro de ligne fin
      new AS400Text(SIZE_POTIT1), // Titre Texte 1
      new AS400Text(SIZE_POTIT2), // Titre Texte 2
      new AS400Text(SIZE_POTIT3), // Titre Texte 3
      new AS400Text(SIZE_POTIT4), // Titre Texte 4
      new AS400Text(SIZE_POTOT1), // Total Texte 1
      new AS400Text(SIZE_POTOT2), // Total Texte 2
      new AS400Text(SIZE_POTOT3), // Total Texte 3
      new AS400Text(SIZE_POTOT4), // Total Texte 4
      new AS400Text(SIZE_POEDTA), // Top édition détail articles
      new AS400Text(SIZE_POEDTP), // Top édition prix d"articles
      new AS400ZonedDecimal(SIZE_POTMHT, DECIMAL_POTMHT), // Montant total
      new AS400ZonedDecimal(SIZE_POTPRV, DECIMAL_POTPRV), // Total prix de revient
      new AS400ZonedDecimal(SIZE_POTMAR, DECIMAL_POTMAR), // Montant marge totale
      new AS400ZonedDecimal(SIZE_POPMAR, DECIMAL_POPMAR), // Pourcentage marge
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, ponlid, ponlif, potit1, potit2, potit3, potit4, potot1, potot2, potot3, potot4, poedta, poedtp, potmht,
          potprv, potmar, popmar, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    ponlid = (BigDecimal) output[1];
    ponlif = (BigDecimal) output[2];
    potit1 = (String) output[3];
    potit2 = (String) output[4];
    potit3 = (String) output[5];
    potit4 = (String) output[6];
    potot1 = (String) output[7];
    potot2 = (String) output[8];
    potot3 = (String) output[9];
    potot4 = (String) output[10];
    poedta = (String) output[11];
    poedtp = (String) output[12];
    potmht = (BigDecimal) output[13];
    potprv = (BigDecimal) output[14];
    potmar = (BigDecimal) output[15];
    popmar = (BigDecimal) output[16];
    poarr = (String) output[17];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPonlid(BigDecimal pPonlid) {
    if (pPonlid == null) {
      return;
    }
    ponlid = pPonlid.setScale(DECIMAL_PONLID, RoundingMode.HALF_UP);
  }
  
  public void setPonlid(Integer pPonlid) {
    if (pPonlid == null) {
      return;
    }
    ponlid = BigDecimal.valueOf(pPonlid);
  }
  
  public Integer getPonlid() {
    return ponlid.intValue();
  }
  
  public void setPonlif(BigDecimal pPonlif) {
    if (pPonlif == null) {
      return;
    }
    ponlif = pPonlif.setScale(DECIMAL_PONLIF, RoundingMode.HALF_UP);
  }
  
  public void setPonlif(Integer pPonlif) {
    if (pPonlif == null) {
      return;
    }
    ponlif = BigDecimal.valueOf(pPonlif);
  }
  
  public Integer getPonlif() {
    return ponlif.intValue();
  }
  
  public void setPotit1(String pPotit1) {
    if (pPotit1 == null) {
      return;
    }
    potit1 = pPotit1;
  }
  
  public String getPotit1() {
    return potit1;
  }
  
  public void setPotit2(String pPotit2) {
    if (pPotit2 == null) {
      return;
    }
    potit2 = pPotit2;
  }
  
  public String getPotit2() {
    return potit2;
  }
  
  public void setPotit3(String pPotit3) {
    if (pPotit3 == null) {
      return;
    }
    potit3 = pPotit3;
  }
  
  public String getPotit3() {
    return potit3;
  }
  
  public void setPotit4(String pPotit4) {
    if (pPotit4 == null) {
      return;
    }
    potit4 = pPotit4;
  }
  
  public String getPotit4() {
    return potit4;
  }
  
  public void setPotot1(String pPotot1) {
    if (pPotot1 == null) {
      return;
    }
    potot1 = pPotot1;
  }
  
  public String getPotot1() {
    return potot1;
  }
  
  public void setPotot2(String pPotot2) {
    if (pPotot2 == null) {
      return;
    }
    potot2 = pPotot2;
  }
  
  public String getPotot2() {
    return potot2;
  }
  
  public void setPotot3(String pPotot3) {
    if (pPotot3 == null) {
      return;
    }
    potot3 = pPotot3;
  }
  
  public String getPotot3() {
    return potot3;
  }
  
  public void setPotot4(String pPotot4) {
    if (pPotot4 == null) {
      return;
    }
    potot4 = pPotot4;
  }
  
  public String getPotot4() {
    return potot4;
  }
  
  public void setPoedta(Character pPoedta) {
    if (pPoedta == null) {
      return;
    }
    poedta = String.valueOf(pPoedta);
  }
  
  public Character getPoedta() {
    return poedta.charAt(0);
  }
  
  public void setPoedtp(Character pPoedtp) {
    if (pPoedtp == null) {
      return;
    }
    poedtp = String.valueOf(pPoedtp);
  }
  
  public Character getPoedtp() {
    return poedtp.charAt(0);
  }
  
  public void setPotmht(BigDecimal pPotmht) {
    if (pPotmht == null) {
      return;
    }
    potmht = pPotmht.setScale(DECIMAL_POTMHT, RoundingMode.HALF_UP);
  }
  
  public void setPotmht(Double pPotmht) {
    if (pPotmht == null) {
      return;
    }
    potmht = BigDecimal.valueOf(pPotmht).setScale(DECIMAL_POTMHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPotmht() {
    return potmht;
  }
  
  public void setPotprv(BigDecimal pPotprv) {
    if (pPotprv == null) {
      return;
    }
    potprv = pPotprv.setScale(DECIMAL_POTPRV, RoundingMode.HALF_UP);
  }
  
  public void setPotprv(Double pPotprv) {
    if (pPotprv == null) {
      return;
    }
    potprv = BigDecimal.valueOf(pPotprv).setScale(DECIMAL_POTPRV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPotprv() {
    return potprv;
  }
  
  public void setPotmar(BigDecimal pPotmar) {
    if (pPotmar == null) {
      return;
    }
    potmar = pPotmar.setScale(DECIMAL_POTMAR, RoundingMode.HALF_UP);
  }
  
  public void setPotmar(Double pPotmar) {
    if (pPotmar == null) {
      return;
    }
    potmar = BigDecimal.valueOf(pPotmar).setScale(DECIMAL_POTMAR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPotmar() {
    return potmar;
  }
  
  public void setPopmar(BigDecimal pPopmar) {
    if (pPopmar == null) {
      return;
    }
    popmar = pPopmar.setScale(DECIMAL_POPMAR, RoundingMode.HALF_UP);
  }
  
  public void setPopmar(Double pPopmar) {
    if (pPopmar == null) {
      return;
    }
    popmar = BigDecimal.valueOf(pPopmar).setScale(DECIMAL_POPMAR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPopmar() {
    return popmar;
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
