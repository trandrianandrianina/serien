/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres;

import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.CritereSousFamilles;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class RequetesRechercheSousFamilles {
  
  /**
   * Retourne la requête en fonction des critères de recherche.
   */
  public static String getRequete(String curlib, CritereSousFamilles pCriteres) {
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select * from " + curlib + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where");
    if (pCriteres.getIdEtablissement() != null) {
      requeteSql.ajouterConditionAnd("PARETB", "=", pCriteres.getIdEtablissement().getCodeEtablissement());
    }
    requeteSql.ajouterConditionAnd("PARTYP", "=", "SF");
    if (!pCriteres.isAvecAnnule()) {
      requeteSql.ajouterConditionAnd("PARTOP", "<>", "9");
    }
    requeteSql.ajouter("ORDER BY PARZ2");
    return requeteSql.getRequete();
  }
  
}
