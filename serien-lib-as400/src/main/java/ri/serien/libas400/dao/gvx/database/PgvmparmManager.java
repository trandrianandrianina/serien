/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import java.sql.Connection;
import java.util.ArrayList;

import com.ibm.as400.access.Record;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.DataStructureRecord;
import ri.serien.libas400.database.record.ExtendRecord;
import ri.serien.libas400.parameters.EtablissementMobilite;
import ri.serien.libas400.parameters.MagasinMobilite;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 * ATTENTION de ne pas oublier d'initialiser la FM via le setLibrary
 */
public class PgvmparmManager extends QueryManager {
  /**
   * Constructeur
   * @param database
   */
  public PgvmparmManager(Connection database) {
    super(database);
  }
  
  /**
   * Lecture de la DG blanche.
   */
  public boolean chargerInfosEtablissementPilote(EtablissementMobilite pEtablissementPilote) {
    if (pEtablissementPilote == null) {
      return false;
    }
    
    String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_DG";
    final ArrayList<Record> listeRecord =
        selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + getLibrary()
            + ".PGVMPARM where PARETB = '' and PARTYP = 'DG' and PARIND = ''", (DataStructureRecord) newObject(ds));
    
    // Alimentation des informations avec l'enregistrement trouvé
    if (listeRecord.size() == 1) {
      ExtendRecord record = new ExtendRecord();
      record.setRecord(listeRecord.get(0));
      pEtablissementPilote.setNom((String) record.getField("DGNOM"));
      String codeEtablissement = Constantes.normerTexte((String) record.getField("DGETP"));
      if (codeEtablissement.isEmpty()) {
        Trace.alerte("Attention il n'y a pas d'établisement pilote.");
        return false;
      }
      IdEtablissement idEtablissement = IdEtablissement.getInstance(codeEtablissement);
      pEtablissementPilote.setId(idEtablissement);
      return true;
    }
    
    return false;
  }
  
  /**
   * Lecture de la DG pour un établissement.
   */
  public boolean getInfosEtablissement(EtablissementMobilite pEtablissement) {
    // Tester s'il y a un établissement définit par défaut pour l'utilisateur en cours
    if (pEtablissement == null || pEtablissement.getId() == null) {
      return false;
    }
    
    String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_DG";
    final ArrayList<Record> listeRecord = selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + getLibrary() + ".PGVMPARM where PARETB = '"
            + pEtablissement.getId().getCodeEtablissement() + "' and PARTYP = 'DG'",
        (DataStructureRecord) newObject(ds));
    
    // Alimentation des infos avec l'enregistrement trouvé
    if (listeRecord.size() == 1) {
      ExtendRecord record = new ExtendRecord();
      record.setRecord(listeRecord.get(0));
      pEtablissement.setNom((String) record.getField("DGNOM"));
      pEtablissement.setComplementNom((String) record.getField("DGCPL"));
      
      String codeMagasin = Constantes.normerTexte((String) record.getField("DGETP"));
      if (!codeMagasin.isEmpty() && codeMagasin.trim().length() == IdMagasin.LONGUEUR_CODE) {
        pEtablissement.setMagasinGeneral(IdMagasin.getInstance(pEtablissement.getId(), codeMagasin));
      }
      lectureDesPS(pEtablissement);
      
      return true;
    }
    
    return false;
  }
  
  /**
   * Lecture des PS pour un etablissement
   * @param aetablissement
   * @return
   *
   */
  public boolean lectureDesPS(EtablissementMobilite aetablissement) {
    if (aetablissement == null) {
      return false;
    }
    
    String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_PS";
    final ArrayList<Record> listeRecord =
        selectAvecDataStructure(
            "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + getLibrary() + ".PGVMPARM where PARETB = '"
                + aetablissement.getId().getCodeEtablissement() + "' and PARTYP = 'PS' and PARIND = ''",
            (DataStructureRecord) newObject(ds));
    
    // On alimente les infos si on trouve l'enregistrement
    if (listeRecord.size() == 1) {
      ExtendRecord record = new ExtendRecord();
      record.setRecord(listeRecord.get(0));
      aetablissement.getPs().setTableau((String) record.getField("PS"));
      
      return true;
    }
    
    return false;
  }
  
  /**
   * Lecture des infos pour un magasin
   * @param etb
   * @param codemag
   * @return
   *
   */
  public boolean getInfosMagasin(IdEtablissement pIdEtablissement, MagasinMobilite amagasin) {
    if (amagasin == null) {
      return false;
    }
    
    String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_MA";
    final ArrayList<Record> listeRecord = selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + getLibrary() + ".PGVMPARM where PARETB = '"
            + pIdEtablissement.getCodeEtablissement() + "' and PARTYP = 'MA' and PARIND = '" + amagasin.getCodeMag() + "'",
        (DataStructureRecord) newObject(ds));
    
    // On alimente les infos si on trouve l'enregistrement
    if (listeRecord.size() == 1) {
      ExtendRecord record = new ExtendRecord();
      record.setRecord(listeRecord.get(0));
      amagasin.setMALIB((String) record.getField("MALIB"));
      
      return true;
    }
    
    return false;
  }
  
  /**
   * Retourne la liste de tous les champs pour un établissement et un paramètre donné
   * @param etb
   * @param parameter
   * @return
   *
   */
  public ArrayList<Record> getRecordsByETB_PARAM(String etb, String parameter) {
    if (parameter == null) {
      return null;
    }
    
    etb = etb.toUpperCase();
    parameter = parameter.trim().toUpperCase();
    String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_" + parameter;
    final ArrayList<Record> listeRecord =
        selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + getLibrary()
            + ".PGVMPARM where PARETB = '" + etb + "' and PARTYP = '" + parameter + "'", (DataStructureRecord) newObject(ds));
    
    return listeRecord;
  }
  
  /**
   * Retourne la liste de tous les champs pour un établissement, un type donné et un code. Normalement cela ne devrait
   * retourner qu'un enregistrement
   * @param etb
   * @param parameter
   * @return
   *
   */
  public ArrayList<Record> getRecordsByType_Code(IdEtablissement pIdEtablissement, String type, String code) {
    if (pIdEtablissement == null || type == null || code == null) {
      return null;
    }
    
    String etb = pIdEtablissement.getCodeEtablissement().trim();
    type = type.trim().toUpperCase();
    code = code.trim().toUpperCase();
    String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_" + type;
    final ArrayList<Record> listeRecord = selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + getLibrary() + ".PGVMPARM where PARETB = '" + etb
            + "' and PARTYP = '" + type + "' and PARIND = '" + code + "'",
        (DataStructureRecord) newObject(ds));
    
    return listeRecord;
  }
}
