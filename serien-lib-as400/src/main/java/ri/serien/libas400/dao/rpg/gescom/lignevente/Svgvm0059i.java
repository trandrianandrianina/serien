/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0059i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PIID01 = 15;
  public static final int SIZE_PIID02 = 15;
  public static final int SIZE_PIID03 = 15;
  public static final int SIZE_PIID04 = 15;
  public static final int SIZE_PIID05 = 15;
  public static final int SIZE_PIID06 = 15;
  public static final int SIZE_PIID07 = 15;
  public static final int SIZE_PIID08 = 15;
  public static final int SIZE_PIID09 = 15;
  public static final int SIZE_PIID10 = 15;
  public static final int SIZE_PIID11 = 15;
  public static final int SIZE_PIID12 = 15;
  public static final int SIZE_PIID13 = 15;
  public static final int SIZE_PIID14 = 15;
  public static final int SIZE_PIID15 = 15;
  public static final int SIZE_PIID16 = 15;
  public static final int SIZE_PIID17 = 15;
  public static final int SIZE_PIID18 = 15;
  public static final int SIZE_PIID19 = 15;
  public static final int SIZE_PIID20 = 15;
  public static final int SIZE_PIID21 = 15;
  public static final int SIZE_PIID22 = 15;
  public static final int SIZE_PIID23 = 15;
  public static final int SIZE_PIID24 = 15;
  public static final int SIZE_PIID25 = 15;
  public static final int SIZE_PIID26 = 15;
  public static final int SIZE_PIID27 = 15;
  public static final int SIZE_PIID28 = 15;
  public static final int SIZE_PIID29 = 15;
  public static final int SIZE_PIID30 = 15;
  public static final int SIZE_PIID31 = 15;
  public static final int SIZE_PIID32 = 15;
  public static final int SIZE_PIID33 = 15;
  public static final int SIZE_PIID34 = 15;
  public static final int SIZE_PIID35 = 15;
  public static final int SIZE_PIID36 = 15;
  public static final int SIZE_PIID37 = 15;
  public static final int SIZE_PIID38 = 15;
  public static final int SIZE_PIID39 = 15;
  public static final int SIZE_PIID40 = 15;
  public static final int SIZE_PIID41 = 15;
  public static final int SIZE_PIID42 = 15;
  public static final int SIZE_PIID43 = 15;
  public static final int SIZE_PIID44 = 15;
  public static final int SIZE_PIID45 = 15;
  public static final int SIZE_PIID46 = 15;
  public static final int SIZE_PIID47 = 15;
  public static final int SIZE_PIID48 = 15;
  public static final int SIZE_PIID49 = 15;
  public static final int SIZE_PIID50 = 15;
  public static final int SIZE_PIID51 = 15;
  public static final int SIZE_PIID52 = 15;
  public static final int SIZE_PIID53 = 15;
  public static final int SIZE_PIID54 = 15;
  public static final int SIZE_PIID55 = 15;
  public static final int SIZE_PIID56 = 15;
  public static final int SIZE_PIID57 = 15;
  public static final int SIZE_PIID58 = 15;
  public static final int SIZE_PIID59 = 15;
  public static final int SIZE_PIID60 = 15;
  public static final int SIZE_PIID61 = 15;
  public static final int SIZE_PIID62 = 15;
  public static final int SIZE_PIID63 = 15;
  public static final int SIZE_PIID64 = 15;
  public static final int SIZE_PIID65 = 15;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 989;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PIID01 = 2;
  public static final int VAR_PIID02 = 3;
  public static final int VAR_PIID03 = 4;
  public static final int VAR_PIID04 = 5;
  public static final int VAR_PIID05 = 6;
  public static final int VAR_PIID06 = 7;
  public static final int VAR_PIID07 = 8;
  public static final int VAR_PIID08 = 9;
  public static final int VAR_PIID09 = 10;
  public static final int VAR_PIID10 = 11;
  public static final int VAR_PIID11 = 12;
  public static final int VAR_PIID12 = 13;
  public static final int VAR_PIID13 = 14;
  public static final int VAR_PIID14 = 15;
  public static final int VAR_PIID15 = 16;
  public static final int VAR_PIID16 = 17;
  public static final int VAR_PIID17 = 18;
  public static final int VAR_PIID18 = 19;
  public static final int VAR_PIID19 = 20;
  public static final int VAR_PIID20 = 21;
  public static final int VAR_PIID21 = 22;
  public static final int VAR_PIID22 = 23;
  public static final int VAR_PIID23 = 24;
  public static final int VAR_PIID24 = 25;
  public static final int VAR_PIID25 = 26;
  public static final int VAR_PIID26 = 27;
  public static final int VAR_PIID27 = 28;
  public static final int VAR_PIID28 = 29;
  public static final int VAR_PIID29 = 30;
  public static final int VAR_PIID30 = 31;
  public static final int VAR_PIID31 = 32;
  public static final int VAR_PIID32 = 33;
  public static final int VAR_PIID33 = 34;
  public static final int VAR_PIID34 = 35;
  public static final int VAR_PIID35 = 36;
  public static final int VAR_PIID36 = 37;
  public static final int VAR_PIID37 = 38;
  public static final int VAR_PIID38 = 39;
  public static final int VAR_PIID39 = 40;
  public static final int VAR_PIID40 = 41;
  public static final int VAR_PIID41 = 42;
  public static final int VAR_PIID42 = 43;
  public static final int VAR_PIID43 = 44;
  public static final int VAR_PIID44 = 45;
  public static final int VAR_PIID45 = 46;
  public static final int VAR_PIID46 = 47;
  public static final int VAR_PIID47 = 48;
  public static final int VAR_PIID48 = 49;
  public static final int VAR_PIID49 = 50;
  public static final int VAR_PIID50 = 51;
  public static final int VAR_PIID51 = 52;
  public static final int VAR_PIID52 = 53;
  public static final int VAR_PIID53 = 54;
  public static final int VAR_PIID54 = 55;
  public static final int VAR_PIID55 = 56;
  public static final int VAR_PIID56 = 57;
  public static final int VAR_PIID57 = 58;
  public static final int VAR_PIID58 = 59;
  public static final int VAR_PIID59 = 60;
  public static final int VAR_PIID60 = 61;
  public static final int VAR_PIID61 = 62;
  public static final int VAR_PIID62 = 63;
  public static final int VAR_PIID63 = 64;
  public static final int VAR_PIID64 = 65;
  public static final int VAR_PIID65 = 66;
  public static final int VAR_PIARR = 67;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private String piid01 = ""; //
  private String piid02 = ""; //
  private String piid03 = ""; //
  private String piid04 = ""; //
  private String piid05 = ""; //
  private String piid06 = ""; //
  private String piid07 = ""; //
  private String piid08 = ""; //
  private String piid09 = ""; //
  private String piid10 = ""; //
  private String piid11 = ""; //
  private String piid12 = ""; //
  private String piid13 = ""; //
  private String piid14 = ""; //
  private String piid15 = ""; //
  private String piid16 = ""; //
  private String piid17 = ""; //
  private String piid18 = ""; //
  private String piid19 = ""; //
  private String piid20 = ""; //
  private String piid21 = ""; //
  private String piid22 = ""; //
  private String piid23 = ""; //
  private String piid24 = ""; //
  private String piid25 = ""; //
  private String piid26 = ""; //
  private String piid27 = ""; //
  private String piid28 = ""; //
  private String piid29 = ""; //
  private String piid30 = ""; //
  private String piid31 = ""; //
  private String piid32 = ""; //
  private String piid33 = ""; //
  private String piid34 = ""; //
  private String piid35 = ""; //
  private String piid36 = ""; //
  private String piid37 = ""; //
  private String piid38 = ""; //
  private String piid39 = ""; //
  private String piid40 = ""; //
  private String piid41 = ""; //
  private String piid42 = ""; //
  private String piid43 = ""; //
  private String piid44 = ""; //
  private String piid45 = ""; //
  private String piid46 = ""; //
  private String piid47 = ""; //
  private String piid48 = ""; //
  private String piid49 = ""; //
  private String piid50 = ""; //
  private String piid51 = ""; //
  private String piid52 = ""; //
  private String piid53 = ""; //
  private String piid54 = ""; //
  private String piid55 = ""; //
  private String piid56 = ""; //
  private String piid57 = ""; //
  private String piid58 = ""; //
  private String piid59 = ""; //
  private String piid60 = ""; //
  private String piid61 = ""; //
  private String piid62 = ""; //
  private String piid63 = ""; //
  private String piid64 = ""; //
  private String piid65 = ""; //
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400Text(SIZE_PIID01), //
      new AS400Text(SIZE_PIID02), //
      new AS400Text(SIZE_PIID03), //
      new AS400Text(SIZE_PIID04), //
      new AS400Text(SIZE_PIID05), //
      new AS400Text(SIZE_PIID06), //
      new AS400Text(SIZE_PIID07), //
      new AS400Text(SIZE_PIID08), //
      new AS400Text(SIZE_PIID09), //
      new AS400Text(SIZE_PIID10), //
      new AS400Text(SIZE_PIID11), //
      new AS400Text(SIZE_PIID12), //
      new AS400Text(SIZE_PIID13), //
      new AS400Text(SIZE_PIID14), //
      new AS400Text(SIZE_PIID15), //
      new AS400Text(SIZE_PIID16), //
      new AS400Text(SIZE_PIID17), //
      new AS400Text(SIZE_PIID18), //
      new AS400Text(SIZE_PIID19), //
      new AS400Text(SIZE_PIID20), //
      new AS400Text(SIZE_PIID21), //
      new AS400Text(SIZE_PIID22), //
      new AS400Text(SIZE_PIID23), //
      new AS400Text(SIZE_PIID24), //
      new AS400Text(SIZE_PIID25), //
      new AS400Text(SIZE_PIID26), //
      new AS400Text(SIZE_PIID27), //
      new AS400Text(SIZE_PIID28), //
      new AS400Text(SIZE_PIID29), //
      new AS400Text(SIZE_PIID30), //
      new AS400Text(SIZE_PIID31), //
      new AS400Text(SIZE_PIID32), //
      new AS400Text(SIZE_PIID33), //
      new AS400Text(SIZE_PIID34), //
      new AS400Text(SIZE_PIID35), //
      new AS400Text(SIZE_PIID36), //
      new AS400Text(SIZE_PIID37), //
      new AS400Text(SIZE_PIID38), //
      new AS400Text(SIZE_PIID39), //
      new AS400Text(SIZE_PIID40), //
      new AS400Text(SIZE_PIID41), //
      new AS400Text(SIZE_PIID42), //
      new AS400Text(SIZE_PIID43), //
      new AS400Text(SIZE_PIID44), //
      new AS400Text(SIZE_PIID45), //
      new AS400Text(SIZE_PIID46), //
      new AS400Text(SIZE_PIID47), //
      new AS400Text(SIZE_PIID48), //
      new AS400Text(SIZE_PIID49), //
      new AS400Text(SIZE_PIID50), //
      new AS400Text(SIZE_PIID51), //
      new AS400Text(SIZE_PIID52), //
      new AS400Text(SIZE_PIID53), //
      new AS400Text(SIZE_PIID54), //
      new AS400Text(SIZE_PIID55), //
      new AS400Text(SIZE_PIID56), //
      new AS400Text(SIZE_PIID57), //
      new AS400Text(SIZE_PIID58), //
      new AS400Text(SIZE_PIID59), //
      new AS400Text(SIZE_PIID60), //
      new AS400Text(SIZE_PIID61), //
      new AS400Text(SIZE_PIID62), //
      new AS400Text(SIZE_PIID63), //
      new AS400Text(SIZE_PIID64), //
      new AS400Text(SIZE_PIID65), //
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pietb, piid01, piid02, piid03, piid04, piid05, piid06, piid07, piid08, piid09, piid10, piid11, piid12, piid13,
          piid14, piid15, piid16, piid17, piid18, piid19, piid20, piid21, piid22, piid23, piid24, piid25, piid26, piid27, piid28, piid29,
          piid30, piid31, piid32, piid33, piid34, piid35, piid36, piid37, piid38, piid39, piid40, piid41, piid42, piid43, piid44, piid45,
          piid46, piid47, piid48, piid49, piid50, piid51, piid52, piid53, piid54, piid55, piid56, piid57, piid58, piid59, piid60, piid61,
          piid62, piid63, piid64, piid65, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    piid01 = (String) output[2];
    piid02 = (String) output[3];
    piid03 = (String) output[4];
    piid04 = (String) output[5];
    piid05 = (String) output[6];
    piid06 = (String) output[7];
    piid07 = (String) output[8];
    piid08 = (String) output[9];
    piid09 = (String) output[10];
    piid10 = (String) output[11];
    piid11 = (String) output[12];
    piid12 = (String) output[13];
    piid13 = (String) output[14];
    piid14 = (String) output[15];
    piid15 = (String) output[16];
    piid16 = (String) output[17];
    piid17 = (String) output[18];
    piid18 = (String) output[19];
    piid19 = (String) output[20];
    piid20 = (String) output[21];
    piid21 = (String) output[22];
    piid22 = (String) output[23];
    piid23 = (String) output[24];
    piid24 = (String) output[25];
    piid25 = (String) output[26];
    piid26 = (String) output[27];
    piid27 = (String) output[28];
    piid28 = (String) output[29];
    piid29 = (String) output[30];
    piid30 = (String) output[31];
    piid31 = (String) output[32];
    piid32 = (String) output[33];
    piid33 = (String) output[34];
    piid34 = (String) output[35];
    piid35 = (String) output[36];
    piid36 = (String) output[37];
    piid37 = (String) output[38];
    piid38 = (String) output[39];
    piid39 = (String) output[40];
    piid40 = (String) output[41];
    piid41 = (String) output[42];
    piid42 = (String) output[43];
    piid43 = (String) output[44];
    piid44 = (String) output[45];
    piid45 = (String) output[46];
    piid46 = (String) output[47];
    piid47 = (String) output[48];
    piid48 = (String) output[49];
    piid49 = (String) output[50];
    piid50 = (String) output[51];
    piid51 = (String) output[52];
    piid52 = (String) output[53];
    piid53 = (String) output[54];
    piid54 = (String) output[55];
    piid55 = (String) output[56];
    piid56 = (String) output[57];
    piid57 = (String) output[58];
    piid58 = (String) output[59];
    piid59 = (String) output[60];
    piid60 = (String) output[61];
    piid61 = (String) output[62];
    piid62 = (String) output[63];
    piid63 = (String) output[64];
    piid64 = (String) output[65];
    piid65 = (String) output[66];
    piarr = (String) output[67];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPiid01(String pPiid01) {
    if (pPiid01 == null) {
      return;
    }
    piid01 = pPiid01;
  }
  
  public String getPiid01() {
    return piid01;
  }
  
  public void setPiid02(String pPiid02) {
    if (pPiid02 == null) {
      return;
    }
    piid02 = pPiid02;
  }
  
  public String getPiid02() {
    return piid02;
  }
  
  public void setPiid03(String pPiid03) {
    if (pPiid03 == null) {
      return;
    }
    piid03 = pPiid03;
  }
  
  public String getPiid03() {
    return piid03;
  }
  
  public void setPiid04(String pPiid04) {
    if (pPiid04 == null) {
      return;
    }
    piid04 = pPiid04;
  }
  
  public String getPiid04() {
    return piid04;
  }
  
  public void setPiid05(String pPiid05) {
    if (pPiid05 == null) {
      return;
    }
    piid05 = pPiid05;
  }
  
  public String getPiid05() {
    return piid05;
  }
  
  public void setPiid06(String pPiid06) {
    if (pPiid06 == null) {
      return;
    }
    piid06 = pPiid06;
  }
  
  public String getPiid06() {
    return piid06;
  }
  
  public void setPiid07(String pPiid07) {
    if (pPiid07 == null) {
      return;
    }
    piid07 = pPiid07;
  }
  
  public String getPiid07() {
    return piid07;
  }
  
  public void setPiid08(String pPiid08) {
    if (pPiid08 == null) {
      return;
    }
    piid08 = pPiid08;
  }
  
  public String getPiid08() {
    return piid08;
  }
  
  public void setPiid09(String pPiid09) {
    if (pPiid09 == null) {
      return;
    }
    piid09 = pPiid09;
  }
  
  public String getPiid09() {
    return piid09;
  }
  
  public void setPiid10(String pPiid10) {
    if (pPiid10 == null) {
      return;
    }
    piid10 = pPiid10;
  }
  
  public String getPiid10() {
    return piid10;
  }
  
  public void setPiid11(String pPiid11) {
    if (pPiid11 == null) {
      return;
    }
    piid11 = pPiid11;
  }
  
  public String getPiid11() {
    return piid11;
  }
  
  public void setPiid12(String pPiid12) {
    if (pPiid12 == null) {
      return;
    }
    piid12 = pPiid12;
  }
  
  public String getPiid12() {
    return piid12;
  }
  
  public void setPiid13(String pPiid13) {
    if (pPiid13 == null) {
      return;
    }
    piid13 = pPiid13;
  }
  
  public String getPiid13() {
    return piid13;
  }
  
  public void setPiid14(String pPiid14) {
    if (pPiid14 == null) {
      return;
    }
    piid14 = pPiid14;
  }
  
  public String getPiid14() {
    return piid14;
  }
  
  public void setPiid15(String pPiid15) {
    if (pPiid15 == null) {
      return;
    }
    piid15 = pPiid15;
  }
  
  public String getPiid15() {
    return piid15;
  }
  
  public void setPiid16(String pPiid16) {
    if (pPiid16 == null) {
      return;
    }
    piid16 = pPiid16;
  }
  
  public String getPiid16() {
    return piid16;
  }
  
  public void setPiid17(String pPiid17) {
    if (pPiid17 == null) {
      return;
    }
    piid17 = pPiid17;
  }
  
  public String getPiid17() {
    return piid17;
  }
  
  public void setPiid18(String pPiid18) {
    if (pPiid18 == null) {
      return;
    }
    piid18 = pPiid18;
  }
  
  public String getPiid18() {
    return piid18;
  }
  
  public void setPiid19(String pPiid19) {
    if (pPiid19 == null) {
      return;
    }
    piid19 = pPiid19;
  }
  
  public String getPiid19() {
    return piid19;
  }
  
  public void setPiid20(String pPiid20) {
    if (pPiid20 == null) {
      return;
    }
    piid20 = pPiid20;
  }
  
  public String getPiid20() {
    return piid20;
  }
  
  public void setPiid21(String pPiid21) {
    if (pPiid21 == null) {
      return;
    }
    piid21 = pPiid21;
  }
  
  public String getPiid21() {
    return piid21;
  }
  
  public void setPiid22(String pPiid22) {
    if (pPiid22 == null) {
      return;
    }
    piid22 = pPiid22;
  }
  
  public String getPiid22() {
    return piid22;
  }
  
  public void setPiid23(String pPiid23) {
    if (pPiid23 == null) {
      return;
    }
    piid23 = pPiid23;
  }
  
  public String getPiid23() {
    return piid23;
  }
  
  public void setPiid24(String pPiid24) {
    if (pPiid24 == null) {
      return;
    }
    piid24 = pPiid24;
  }
  
  public String getPiid24() {
    return piid24;
  }
  
  public void setPiid25(String pPiid25) {
    if (pPiid25 == null) {
      return;
    }
    piid25 = pPiid25;
  }
  
  public String getPiid25() {
    return piid25;
  }
  
  public void setPiid26(String pPiid26) {
    if (pPiid26 == null) {
      return;
    }
    piid26 = pPiid26;
  }
  
  public String getPiid26() {
    return piid26;
  }
  
  public void setPiid27(String pPiid27) {
    if (pPiid27 == null) {
      return;
    }
    piid27 = pPiid27;
  }
  
  public String getPiid27() {
    return piid27;
  }
  
  public void setPiid28(String pPiid28) {
    if (pPiid28 == null) {
      return;
    }
    piid28 = pPiid28;
  }
  
  public String getPiid28() {
    return piid28;
  }
  
  public void setPiid29(String pPiid29) {
    if (pPiid29 == null) {
      return;
    }
    piid29 = pPiid29;
  }
  
  public String getPiid29() {
    return piid29;
  }
  
  public void setPiid30(String pPiid30) {
    if (pPiid30 == null) {
      return;
    }
    piid30 = pPiid30;
  }
  
  public String getPiid30() {
    return piid30;
  }
  
  public void setPiid31(String pPiid31) {
    if (pPiid31 == null) {
      return;
    }
    piid31 = pPiid31;
  }
  
  public String getPiid31() {
    return piid31;
  }
  
  public void setPiid32(String pPiid32) {
    if (pPiid32 == null) {
      return;
    }
    piid32 = pPiid32;
  }
  
  public String getPiid32() {
    return piid32;
  }
  
  public void setPiid33(String pPiid33) {
    if (pPiid33 == null) {
      return;
    }
    piid33 = pPiid33;
  }
  
  public String getPiid33() {
    return piid33;
  }
  
  public void setPiid34(String pPiid34) {
    if (pPiid34 == null) {
      return;
    }
    piid34 = pPiid34;
  }
  
  public String getPiid34() {
    return piid34;
  }
  
  public void setPiid35(String pPiid35) {
    if (pPiid35 == null) {
      return;
    }
    piid35 = pPiid35;
  }
  
  public String getPiid35() {
    return piid35;
  }
  
  public void setPiid36(String pPiid36) {
    if (pPiid36 == null) {
      return;
    }
    piid36 = pPiid36;
  }
  
  public String getPiid36() {
    return piid36;
  }
  
  public void setPiid37(String pPiid37) {
    if (pPiid37 == null) {
      return;
    }
    piid37 = pPiid37;
  }
  
  public String getPiid37() {
    return piid37;
  }
  
  public void setPiid38(String pPiid38) {
    if (pPiid38 == null) {
      return;
    }
    piid38 = pPiid38;
  }
  
  public String getPiid38() {
    return piid38;
  }
  
  public void setPiid39(String pPiid39) {
    if (pPiid39 == null) {
      return;
    }
    piid39 = pPiid39;
  }
  
  public String getPiid39() {
    return piid39;
  }
  
  public void setPiid40(String pPiid40) {
    if (pPiid40 == null) {
      return;
    }
    piid40 = pPiid40;
  }
  
  public String getPiid40() {
    return piid40;
  }
  
  public void setPiid41(String pPiid41) {
    if (pPiid41 == null) {
      return;
    }
    piid41 = pPiid41;
  }
  
  public String getPiid41() {
    return piid41;
  }
  
  public void setPiid42(String pPiid42) {
    if (pPiid42 == null) {
      return;
    }
    piid42 = pPiid42;
  }
  
  public String getPiid42() {
    return piid42;
  }
  
  public void setPiid43(String pPiid43) {
    if (pPiid43 == null) {
      return;
    }
    piid43 = pPiid43;
  }
  
  public String getPiid43() {
    return piid43;
  }
  
  public void setPiid44(String pPiid44) {
    if (pPiid44 == null) {
      return;
    }
    piid44 = pPiid44;
  }
  
  public String getPiid44() {
    return piid44;
  }
  
  public void setPiid45(String pPiid45) {
    if (pPiid45 == null) {
      return;
    }
    piid45 = pPiid45;
  }
  
  public String getPiid45() {
    return piid45;
  }
  
  public void setPiid46(String pPiid46) {
    if (pPiid46 == null) {
      return;
    }
    piid46 = pPiid46;
  }
  
  public String getPiid46() {
    return piid46;
  }
  
  public void setPiid47(String pPiid47) {
    if (pPiid47 == null) {
      return;
    }
    piid47 = pPiid47;
  }
  
  public String getPiid47() {
    return piid47;
  }
  
  public void setPiid48(String pPiid48) {
    if (pPiid48 == null) {
      return;
    }
    piid48 = pPiid48;
  }
  
  public String getPiid48() {
    return piid48;
  }
  
  public void setPiid49(String pPiid49) {
    if (pPiid49 == null) {
      return;
    }
    piid49 = pPiid49;
  }
  
  public String getPiid49() {
    return piid49;
  }
  
  public void setPiid50(String pPiid50) {
    if (pPiid50 == null) {
      return;
    }
    piid50 = pPiid50;
  }
  
  public String getPiid50() {
    return piid50;
  }
  
  public void setPiid51(String pPiid51) {
    if (pPiid51 == null) {
      return;
    }
    piid51 = pPiid51;
  }
  
  public String getPiid51() {
    return piid51;
  }
  
  public void setPiid52(String pPiid52) {
    if (pPiid52 == null) {
      return;
    }
    piid52 = pPiid52;
  }
  
  public String getPiid52() {
    return piid52;
  }
  
  public void setPiid53(String pPiid53) {
    if (pPiid53 == null) {
      return;
    }
    piid53 = pPiid53;
  }
  
  public String getPiid53() {
    return piid53;
  }
  
  public void setPiid54(String pPiid54) {
    if (pPiid54 == null) {
      return;
    }
    piid54 = pPiid54;
  }
  
  public String getPiid54() {
    return piid54;
  }
  
  public void setPiid55(String pPiid55) {
    if (pPiid55 == null) {
      return;
    }
    piid55 = pPiid55;
  }
  
  public String getPiid55() {
    return piid55;
  }
  
  public void setPiid56(String pPiid56) {
    if (pPiid56 == null) {
      return;
    }
    piid56 = pPiid56;
  }
  
  public String getPiid56() {
    return piid56;
  }
  
  public void setPiid57(String pPiid57) {
    if (pPiid57 == null) {
      return;
    }
    piid57 = pPiid57;
  }
  
  public String getPiid57() {
    return piid57;
  }
  
  public void setPiid58(String pPiid58) {
    if (pPiid58 == null) {
      return;
    }
    piid58 = pPiid58;
  }
  
  public String getPiid58() {
    return piid58;
  }
  
  public void setPiid59(String pPiid59) {
    if (pPiid59 == null) {
      return;
    }
    piid59 = pPiid59;
  }
  
  public String getPiid59() {
    return piid59;
  }
  
  public void setPiid60(String pPiid60) {
    if (pPiid60 == null) {
      return;
    }
    piid60 = pPiid60;
  }
  
  public String getPiid60() {
    return piid60;
  }
  
  public void setPiid61(String pPiid61) {
    if (pPiid61 == null) {
      return;
    }
    piid61 = pPiid61;
  }
  
  public String getPiid61() {
    return piid61;
  }
  
  public void setPiid62(String pPiid62) {
    if (pPiid62 == null) {
      return;
    }
    piid62 = pPiid62;
  }
  
  public String getPiid62() {
    return piid62;
  }
  
  public void setPiid63(String pPiid63) {
    if (pPiid63 == null) {
      return;
    }
    piid63 = pPiid63;
  }
  
  public String getPiid63() {
    return piid63;
  }
  
  public void setPiid64(String pPiid64) {
    if (pPiid64 == null) {
      return;
    }
    piid64 = pPiid64;
  }
  
  public String getPiid64() {
    return piid64;
  }
  
  public void setPiid65(String pPiid65) {
    if (pPiid65 == null) {
      return;
    }
    piid65 = pPiid65;
  }
  
  public String getPiid65() {
    return piid65;
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
