/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx002.v3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.JsonSyntaxException;

import ri.serien.libas400.dao.gvm.programs.InjecteurBdVetF;
import ri.serien.libas400.dao.sql.gescom.flux.OutilsMagento;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonCommandeWebV3;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.AlmostQtemp;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.job.SurveillanceJob;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.IdTypeFacturation;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Version des flux en V3.
 * Cette classe regroupe l'ensemble des traitements à effectuer sur l'importation des commandes SFCC dans Série N.
 */
public class ImportationCommandeWebV3 extends BaseGroupDB {
  // Variables
  private SystemeManager systemeManager = null;
  private String codeVendeurWeb = null;
  private String magasinWeb = null;
  private String articleFraisPort = null;
  private String articleRemise = null;
  private char lettreEnv = ' ';
  private ParametresFluxBibli parametresBibli = null;
  private SurveillanceJob surveillanceJob = null;
  private BigDecimal tauxTVAClient = null;
  
  // Forcer les prix transmis par SFCC dans Série N
  private boolean prixForces = true;
  private final String VALEUR_PRIX_FORCES = "1";
  
  /**
   * Constructeur.
   */
  public ImportationCommandeWebV3(QueryManager pQueryManager) {
    super(pQueryManager);
  }
  
  /**
   * Constructeur.
   */
  public ImportationCommandeWebV3(SystemeManager pSystemeManager, QueryManager pQueryManager, ParametresFluxBibli pParametres,
      SurveillanceJob pSurveillanceJob) {
    this(pQueryManager);
    parametresBibli = pParametres;
    systemeManager = pSystemeManager;
    if (pParametres.getLettreEnvironnement() != null && pParametres.getLettreEnvironnement().length() == 1) {
      lettreEnv = pParametres.getLettreEnvironnement().charAt(0);
    }
    surveillanceJob = pSurveillanceJob;
  }
  
  // -- Méthodes publiques
  
  /**
   * Permet de vérifier le contenu du JSON pour le matcher avec une commande Série N.
   */
  public boolean recupererUneCommandeWeb(FluxMagento pFlux) {
    // Contrôle du paramètre
    if (pFlux == null) {
      majError("[ImportationCommandeWebV3] recupererUneCommandeWeb() - Le flux est invalide.");
      return false;
    }
    
    if (pFlux.getFLJSN() == null || pFlux.getFLJSN().trim().length() == 0) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] recupererUneCommandeWeb() - Le champ FLJSN contenant le texte json est invalide.");
      return false;
    }
    
    // Initialisation des objets pour le traitement de la chaine json
    if (!initJSON()) {
      pFlux.construireMessageErreur(
          "[ImportationCommandeWebV3] recupererUneCommandeWeb() - Erreur lors de l'initialisation pour la conversion en objet json");
      return false;
    }
    
    // Conversion de la chaine json du flux en objet Json
    JsonCommandeWebV3 commande = null;
    try {
      commande = gson.fromJson(pFlux.getFLJSN(), JsonCommandeWebV3.class);
    }
    catch (JsonSyntaxException e) {
      pFlux.construireMessageErreur(
          "[ImportationCommandeWebV3] recupererUneCommandeWeb() - Erreur lors de la conversion en objet json:" + e.getMessage());
      return false;
    }
    
    // La chaine json correspondant à la commande n'a pas pu être transformée en objet
    if (commande == null) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] recupererUneCommandeWeb() - L'Objet json de la commande est invalide.");
      return false;
    }
    
    // Contrôle la présence d'articles dans la commande
    if (commande.getListeArticlesCommandes() == null) {
      pFlux.construireMessageErreur(
          "[ImportationCommandeWebV3] recupererUneCommandeWeb() - La liste des articles de commande est invalide pour le flux "
              + pFlux.getFLID() + '.');
      return false;
    }
    
    try {
      // Initialisation de base de l'objet commande
      commande.setEtb(pFlux.getFLETB());
      if (commande.getIdCommandeClient() != null) {
        pFlux.setFLCOD(commande.getIdCommandeClient());
      }
      
      // Initialisation des paramètres propres à toutes les commandes
      initParametresWeb(commande.getEtb());
      
      // Formatage de certaines zones de la commande
      commande.formaterDonnees();
      
      // Traitements périphériques sur la commandes
      majSectionAnalytique(commande, pFlux);
      
      // Test de la validité de la commande et importation dans Série N si tout est correct
      boolean retour = false;
      if (testerValiditeCommande(commande, pFlux)) {
        retour = genererUneCommandeSerieN(commande, pFlux);
      }
      return retour;
    }
    catch (Exception e) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] recupererUneCommandeWeb() - Erreur lors du traitement des données du flux "
          + pFlux.getFLID() + ": " + e.getMessage());
      return false;
    }
  }
  
  @Override
  public void dispose() {
    queryManager = null;
    if (systemeManager != null) {
      systemeManager.deconnecter();
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Pemet de mettre à jour la section analytique de la commande sur la base du site Web de cette derniere.
   * PGVMPSIM SISAN = SICOD / SIETB
   */
  private void majSectionAnalytique(JsonCommandeWebV3 commande, FluxMagento pFlux) {
    if (commande == null || commande.getIdSite() == 0 || commande.getEtb() == null) {
      if (commande != null) {
        pFlux.construireMessageErreur("[ImportationCommandeWebV3] majSectionAnalytique() - Commande corrompue - commande.getIdSite():"
            + commande.getIdSite() + " -commande.getEtb(): " + commande.getEtb());
      }
      else {
        pFlux.construireMessageErreur("[ImportationCommandeWebV3] majSectionAnalytique() - Commande null");
      }
      return;
    }
    
    ArrayList<GenericRecord> liste = queryManager.select("SELECT SISAN FROM " + queryManager.getLibrary() + ".PSEMPSIM WHERE SIETB = '"
        + commande.getEtb() + "' AND SICOD = '" + commande.getIdSite() + "' ");
    
    if (liste == null) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] majSectionAnalytique() - La table des sites WEB est null ou corrompue pour "
          + commande.getEtb() + "/" + commande.getIdSite());
    }
    else {
      // On attribue la section analytique à la commande
      if (liste.size() > 0) {
        if (liste.get(0).isPresentField("SISAN")) {
          commande.setBESAN(liste.get(0).getField("SISAN").toString().trim());
        }
      }
    }
  }
  
  /**
   * Permet de tester si cette commande est valide: client existe, commande n'existe pas déjà....etc
   */
  private boolean testerValiditeCommande(JsonCommandeWebV3 commande, FluxMagento pFlux) {
    if (commande == null || commande.getIdClientWeb() == null || commande.getIdCommandeClient() == null || commande.getEtb() == null) {
      if (commande != null) {
        pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerValiditeCommande() - Commande corrompue - commande.getIdClientWeb():"
            + commande.getIdClientWeb() + " - commande.getIdCommandeClient(): " + commande.getIdCommandeClient()
            + " - commande.getEtb(): " + commande.getEtb());
      }
      return false;
    }
    
    // On teste le client
    if (!testerValiditeDesClients(commande, pFlux)) {
      return false;
    }
    
    String idCommandeMagento = OutilsMagento.formaterTexteLongueurMax(commande.getIdCommandeClient(), 10);
    commande.setIdCommandeClient(idCommandeMagento);
    
    // Vérification que la commande n'existe pas déjà dans la base
    ArrayList<GenericRecord> liste = queryManager.select("SELECT E1NUM FROM " + queryManager.getLibrary() + "."
        + EnumTableBDD.DOCUMENT_VENTE + " WHERE E1COD = 'E' AND E1ETB ='" + commande.getEtb()
        + "' AND E1IN18 = 'W' AND E1CCT = '" + commande.getIdCommandeClient() + "' ");
    // Si on a un problème de requête
    if (liste == null) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerValiditeCommande() - La requête est null pour " + commande.getEtb() + "/"
          + commande.getIdCommandeClient() + ": " + queryManager.getMsgError());
      return false;
    }
    // Si on a trouvé des enregistrements
    else if (liste.size() > 0) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerValiditeCommande() - Il existe déjà des commandes Magento pour "
          + commande.getEtb() + "/" + commande.getIdCommandeClient() + ": " + queryManager.getMsgError());
      return false;
    }
    
    return true;
  }
  
  /**
   * Teste la validité des clients transmis dans la commande et attribution des identifiants aux zones d'injection correspondantes.
   */
  private boolean testerValiditeDesClients(JsonCommandeWebV3 commande, FluxMagento pFlux) {
    boolean retour = true;
    
    if (!testerClientWeb(commande, pFlux)) {
      return false;
    }
    
    if (!testerClientCreateur(commande, pFlux)) {
      return false;
    }
    
    if (!testerClientFacture(commande, pFlux)) {
      return false;
    }
    
    if (!testerClientLivre(commande, pFlux)) {
      return false;
    }
    
    return retour;
  }
  
  /**
   * Teste les clients réceptionnés et les attribue dans les champs correspondants.
   * Récupère le taux de TVA si le client est TTC.
   */
  private boolean testerClientWeb(JsonCommandeWebV3 pCommande, FluxMagento pFlux) {
    // Vérification du client Série N
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter(
        "select CLCLI,CLLIV,CLETB,CLTTC,CLCAT,CLTFA from " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT + " where ");
    requeteSql.ajouterConditionAnd("CLETB", "=", pCommande.getEtb());
    requeteSql.ajouterConditionAnd("CLADH", "=", pCommande.getIdClientWeb());
    ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
    // Null ou pas de client
    if (liste == null || liste.isEmpty()) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientWeb() - Le client est null ou inexistant " + pCommande.getEtb() + "/"
          + pCommande.getIdClientWeb() + " " + queryManager.getMsgError());
      return false;
    }
    
    // Vérification si plusieurs clients trouvés
    if (liste.size() > 1) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientWeb() - Il existe plusieurs clients pour " + pCommande.getEtb() + "/"
          + pCommande.getIdClientWeb() + " -> " + liste.size());
      return false;
    }
    
    // Un client trouvé
    // Notre client est valide on met à jour les données propres à l'injection
    if (liste.get(0).isPresentField("CLCLI")) {
      pCommande.setBENCLP(liste.get(0).getField("CLCLI").toString());
    }
    if (liste.get(0).isPresentField("CLLIV")) {
      pCommande.setBENCLS(liste.get(0).getField("CLLIV").toString());
    }
    
    // Initialisation volontairement par le client facturé pour laisser faire Série N
    if (liste.get(0).isPresentField("CLTTC")) {
      if (liste.get(0).getField("CLTTC").toString().equals("1") || liste.get(0).getField("CLTTC").toString().equals("2")) {
        pCommande.setTTC(true);
      }
      else {
        pCommande.setTTC(false);
      }
    }
    
    // Si on est TTC (particulier)
    if (pCommande.isTTC()) {
      // Récupération du taux de TVA si client TTC
      if (liste.get(0).isPresentField("CLTFA")) {
        try {
          // Récupération du type de facturation du client pour en extraire la colonne TVA à utiliser
          IdEtablissement idEtablissement = IdEtablissement.getInstance(pCommande.getEtb());
          Character codeTypeFacturation = liste.get(0).getCharacterValue("CLTFA");
          IdTypeFacturation idTypeFacturation = IdTypeFacturation.getInstance(idEtablissement, codeTypeFacturation.toString());
          tauxTVAClient = ManagerFluxMagento.retournerTauxTVA(queryManager, idTypeFacturation);
        }
        catch (Exception e) {
          Trace.erreur(e, "Erreur lors de la récupération du taux de TVA.");
        }
      }
      
      // On n'a pas associé de contact à notre client inutile de contrôler son existence donc
      if (liste.get(0).isPresentField("CLETB") && liste.get(0).isPresentField("CLCAT") && liste.get(0).isPresentField("CLCLI")
          && liste.get(0).isPresentField("CLLIV")) {
        requeteSql.effacerRequete();
        requeteSql.ajouter("select RLNUMT from " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT_LIEN + " where ");
        requeteSql.ajouterConditionAnd("RLETB", "=", liste.get(0).getField("CLETB").toString().trim());
        requeteSql.ajouterConditionAnd("RLCOD", "=", 'C');
        requeteSql.ajouterConditionAnd("RLIND", "=", formaterLeClientPourRLIND(liste.get(0).getField("CLCLI").toString()) + "000");
        ArrayList<GenericRecord> listeC = queryManager.select(requeteSql.getRequeteLectureSeule());
        if (listeC != null && listeC.size() == 1) {
          if (listeC.get(0).isPresentField("RLNUMT")) {
            pCommande.setRENUM(listeC.get(0).getField("RLNUMT").toString().trim());
          }
        }
        else {
          // Aucun contact trouvé dans la table des liens contact/client
          if (listeC == null || listeC.isEmpty()) {
            majError(
                "[ImportationCommandeWebV3] testerClientWeb() - Le contact du client n'a pas été trouvé dans la table des liens contact/client (PSEMRTLM)");
          }
          // Plusieurs contacts trouvés dans la table des liens contact/client
          else {
            String listeRLNUMT = "";
            for (GenericRecord record : listeC) {
              listeRLNUMT += record.getIntegerValue("RLNUMT").toString() + ", ";
            }
            majError(
                "[ImportationCommandeWebV3] testerClientWeb() - Plusieurs contacts ont été trouvé dans la table des liens contact/client (PSEMRTLM) : "
                    + listeRLNUMT);
          }
          return false;
        }
      }
    }
    
    // Vérification du code postal intégré du client facturé
    if (!JsonEntiteMagento.controlerUnCodePostal(true, pCommande.getCdpFac())) {
      majError("[ImportationCommandeWebV3] testerClientWeb() - Le code postal client facturé est erroné ");
      return false;
    }
    
    // Vérification du code postal intégré du client livré
    if (!JsonEntiteMagento.controlerUnCodePostal(true, pCommande.getCdpLiv())) {
      majError("[ImportationCommandeWebV3] testerClientWeb() - Le code postal client livré est erroné ");
      return false;
    }
    
    return true;
  }
  
  /**
   * Teste l'existence du client créateur de la commande dans la base de données.
   */
  private boolean testerClientCreateur(JsonCommandeWebV3 pCommande, FluxMagento pFlux) {
    // Si on pas de code transmis on passe notre chemin car SFCC n'est pas forcément informé de ces informations
    if (pCommande.getIdClient() == null || pCommande.getIdClient().trim().length() == 0) {
      return true;
    }
    
    String[] tabCode = OutilsMagento.decouperIdentifiantsClient(pCommande.getIdClient());
    if (tabCode == null) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientCreateur() - Le découpage du client est en échec " + pCommande.getEtb()
          + "/" + pCommande.getIdClientLiv() + " " + queryManager.getMsgError());
      return false;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    requeteSql
        .ajouter("select CLCLI,CLLIV,CLETB,CLADH from " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT + " where ");
    requeteSql.ajouterConditionAnd("CLETB", "=", pCommande.getEtb());
    requeteSql.ajouterConditionAnd("CLCLI", "=", tabCode[0], true, false);
    requeteSql.ajouterConditionAnd("CLLIV", "=", tabCode[1], true, false);
    ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
    
    // Null ou pas de client
    if (liste == null || liste.isEmpty()) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientCreateur() - Le client est null ou inexistant " + pCommande.getEtb()
          + "/" + pCommande.getIdClient() + " " + queryManager.getMsgError());
      return false;
    }
    
    // Vérification si plusieurs clients ont été trouvés
    if (liste.size() > 1) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientCreateur() - Il existe plusieurs clients pour " + pCommande.getEtb()
          + "/" + pCommande.getIdClient() + " " + queryManager.getMsgError());
      return false;
    }
    
    // Un client trouvé
    // TODO pour le moment cette zone ne sert qu'à contrôler le client créateur de la commande et son identifiant SFCC
    if (liste.get(0).isPresentField("CLADH") && liste.get(0).getField("CLADH").toString().trim().equals(pCommande.getIdClientWeb())) {
      return true;
    }
    
    pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientCreateur() - Le client ERP ne correspond pas au client SFCC "
        + pCommande.getEtb() + "/" + pCommande.getIdClient() + " " + pCommande.getIdClientWeb() + " " + queryManager.getMsgError());
    return false;
  }
  
  /**
   * Teste l'existence du client facturé de la commande dans la base de données de l'ERP.
   */
  private boolean testerClientFacture(JsonCommandeWebV3 pCommande, FluxMagento pFlux) {
    // Si on pas de code transmis on passe notre chemin car SFCC n'est pas forcément informé de ces informations
    if (pCommande.getIdClientFac() == null || pCommande.getIdClientFac().trim().length() == 0) {
      return true;
    }
    
    String[] tabCode = OutilsMagento.decouperIdentifiantsClient(pCommande.getIdClientFac());
    if (tabCode == null) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientFacture() - Le découpage du client est en échec " + pCommande.getEtb()
          + "/" + pCommande.getIdClientLiv() + " " + queryManager.getMsgError());
      return false;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select CLCLI,CLLIV,CLETB from " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT + " where ");
    requeteSql.ajouterConditionAnd("CLETB", "=", pCommande.getEtb());
    requeteSql.ajouterConditionAnd("CLCLI", "=", tabCode[0], true, false);
    requeteSql.ajouterConditionAnd("CLLIV", "=", tabCode[1], true, false);
    ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
    
    // Null ou pas de client
    if (liste == null || liste.isEmpty()) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientFacture() - Le client est null ou inexistant " + pCommande.getEtb() + "/"
          + pCommande.getIdClientFac() + " " + queryManager.getMsgError());
      return false;
    }
    
    // Vérification si plusieurs clients ont été trouvés
    if (liste.size() > 1) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientFacture() - Il existe plusieurs clients pour " + pCommande.getEtb() + "/"
          + pCommande.getIdClientFac() + " " + queryManager.getMsgError());
      return false;
    }
    
    // Un client trouvé
    if (liste.get(0).isPresentField("CLCLI") && liste.get(0).isPresentField("CLLIV")) {
      pCommande
          .setBECLF(recomposerBECLF(liste.get(0).getField("CLCLI").toString().trim(), liste.get(0).getField("CLLIV").toString().trim()));
    }
    return true;
  }
  
  /**
   * Teste l'existence du client livré de la commande dans la base de données de l'ERP.
   */
  private boolean testerClientLivre(JsonCommandeWebV3 pCommande, FluxMagento pFlux) {
    // Si on pas de code transmis on passe notre chemin car Magento n'est pas forcément informé de ces informations
    if (pCommande.getIdClientLiv() == null || pCommande.getIdClientLiv().trim().length() == 0) {
      return true;
    }
    
    String[] tabCode = OutilsMagento.decouperIdentifiantsClient(pCommande.getIdClientLiv());
    if (tabCode == null) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientLivre() - Le découpage du client est en échec " + pCommande.getEtb()
          + "/" + pCommande.getIdClientLiv() + " " + queryManager.getMsgError());
      return false;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select CLCLI,CLLIV,CLETB from " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT + " where ");
    requeteSql.ajouterConditionAnd("CLETB", "=", pCommande.getEtb());
    requeteSql.ajouterConditionAnd("CLCLI", "=", tabCode[0], true, false);
    requeteSql.ajouterConditionAnd("CLLIV", "=", tabCode[1], true, false);
    ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
    
    // Null ou pas de client
    if (liste == null || liste.isEmpty()) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientLivre() - Le client est null ou inexistant " + pCommande.getEtb() + "/"
          + pCommande.getIdClientLiv() + " " + queryManager.getMsgError());
      return false;
    }
    
    // Vérification si plusieurs clients ont été trouvés
    if (liste.size() > 1) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] testerClientLivre() - Il existe plusieurs clients pour " + pCommande.getEtb() + "/"
          + pCommande.getIdClientLiv() + " " + queryManager.getMsgError());
      return false;
    }
    
    // Un client trouvé
    if (liste.get(0).isPresentField("CLCLI")) {
      pCommande.setBENCLP(liste.get(0).getField("CLCLI").toString());
    }
    if (liste.get(0).isPresentField("CLLIV")) {
      pCommande.setBENCLS(liste.get(0).getField("CLLIV").toString());
    }
    return true;
  }
  
  /**
   * Recompose le code et le suffixe d'un client en une seule zone numérique sur 9 avec les 0.
   */
  private String recomposerBECLF(String pCode, String pSuffixe) {
    if (pCode == null || pSuffixe == null) {
      return null;
    }
    
    String code = null;
    switch (pCode.trim().length()) {
      case 1:
        code = "00000" + pCode;
        break;
      case 2:
        code = "0000" + pCode;
        break;
      case 3:
        code = "000" + pCode;
        break;
      case 4:
        code = "00" + pCode;
        break;
      case 5:
        code = "0" + pCode;
        break;
      case 6:
        code = pCode;
        break;
      
      default:
        code = "000000";
        break;
    }
    
    String suffixe = null;
    switch (pSuffixe.trim().length()) {
      case 1:
        suffixe = "00" + pSuffixe;
        break;
      case 2:
        suffixe = "0" + pSuffixe;
        break;
      case 3:
        suffixe = pSuffixe;
        break;
      
      default:
        suffixe = "000";
        break;
    }
    
    return code + suffixe;
  }
  
  /**
   * Permet de générer une commande Série N.
   */
  private boolean genererUneCommandeSerieN(JsonCommandeWebV3 pCommande, FluxMagento pFlux) {
    SystemeManager system = new SystemeManager(true);
    
    boolean retour = false;
    LinkedHashMap<String, String> rapport = new LinkedHashMap<String, String>();
    // TODO Remplacer par le DIGITS de la commande Magento
    int numeroAleatoire = 145;
    
    // Création de la "QTEMP" à partir d'un numéro aléatoire
    AlmostQtemp qtemp = null;
    try {
      qtemp = new AlmostQtemp(system.getSystem(), numeroAleatoire);
    }
    catch (Exception e) {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] genererUneCommandeSerieN() - Création AlmostQtemp: " + e.getMessage());
      system.deconnecter();
      return false;
    }
    
    if (qtemp.create()) {
      // On initialise le gestionnaire d'injection de bons JAVA
      InjecteurBdVetF sgvmiv = new InjecteurBdVetF(system, qtemp.getName(), queryManager.getLibrary(), lettreEnv,
          ParametresFluxBibli.getBibliothequeEnvironnement());
      retour = sgvmiv.initialiser(surveillanceJob);
      if (!retour) {
        pFlux.construireMessageErreur("[ImportationCommandeWebV3] genererUneCommandeSerieN() - sgvmiv.init(): " + sgvmiv.getMsgError());
        qtemp.delete();
        system.deconnecter();
        return retour;
      }
      else {
        sgvmiv.setGestionComplementEntete(true);
        // Nettoyage des fichiers physiques PINJBDV*
        retour = sgvmiv.clearAndCreateAllFilesInWrkLib();
        if (!retour) {
          pFlux.construireMessageErreur(
              "[ImportationCommandeWebV3] genererUneCommandeSerieN() - sgvmiv.clearAndCreateAllFilesInWrkLib(): " + sgvmiv.getMsgError());
          qtemp.delete();
          system.deconnecter();
          return retour;
        }
        else {
          // On construit le contenu de la commande Série N sur la base de la commande SFCC
          ArrayList<GenericRecord> listeRcds = new ArrayList<GenericRecord>();
          
          // Si la commande doit être injectée en TTC on le fait .... ben oui c'est bizarre mais bon
          if (pCommande.isTTC()) {
            pCommande.passerEnTTC(tauxTVAClient);
          }
          
          // Création de la ligne d'entête de la commande
          GenericRecord enteteBon = new GenericRecord();
          enteteBon.setField("BEETB", pCommande.getEtb());
          enteteBon.setField("BEMAG", magasinWeb);
          enteteBon.setField("BEDAT", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
          enteteBon.setField("BEDLS", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
          enteteBon.setField("BERBV", "");
          enteteBon.setField("BEERL", "E");
          enteteBon.setField("BENLI", 0);
          enteteBon.setField("BENCLP", pCommande.getBENCLP());
          enteteBon.setField("BENCLS", pCommande.getBENCLS());
          enteteBon.setField("BECLF", pCommande.getBECLF());
          enteteBon.setField("BEVDE", codeVendeurWeb);
          enteteBon.setField("BEMEX", pCommande.getModeLivraison());
          enteteBon.setField("BECTR", pCommande.getCodeTransporteur());
          enteteBon.setField("BERG2", pCommande.getModeReglement());
          // enteteBon.setField("BESAN", commande.getIdSite());
          if (pCommande.getBESAN() != null) {
            enteteBon.setField("BESAN", pCommande.getBESAN());
          }
          enteteBon.setField("BEOPT", formaterStatutWebSN(pCommande.getStatut()));
          
          // Bloc adresse facturée
          enteteBon.setField("BENOMF", OutilsMagento.formaterTexteLongueurMax(pCommande.getBENOMF(), 30));
          enteteBon.setField("BECPLF", OutilsMagento.formaterTexteLongueurMax(pCommande.getBECPLF(), 30));
          enteteBon.setField("BERUEF", OutilsMagento.formaterTexteLongueurMax(pCommande.getBERUEF(), 30));
          enteteBon.setField("BELOCF", OutilsMagento.formaterTexteLongueurMax(pCommande.getBELOCF(), 30));
          
          enteteBon.setField("BECDPF", pCommande.getCdpFac());
          enteteBon.setField("BEVILF", OutilsMagento.formaterTexteLongueurMax(pCommande.getVilleFac(), 24));
          if (pCommande.getPaysFac() != null) {
            enteteBon.setField("BEPAYF", OutilsMagento.formaterTexteLongueurMax(recupererLibellePaysSerieN(pCommande.getPaysFac()), 30));
          }
          
          // Bloc adresse livraison
          // on a retiré le nettoyage SQL car on traite en GAP
          enteteBon.setField("BENOML", OutilsMagento.formaterTexteLongueurMax(pCommande.getBENOML(), 30));
          enteteBon.setField("BECPLL", OutilsMagento.formaterTexteLongueurMax(pCommande.getBECPLL(), 30));
          enteteBon.setField("BERUEL", OutilsMagento.formaterTexteLongueurMax(pCommande.getBERUEL(), 30));
          enteteBon.setField("BELOCL", OutilsMagento.formaterTexteLongueurMax(pCommande.getBELOCL(), 30));
          
          enteteBon.setField("BECDPL", pCommande.getCdpLiv());
          
          enteteBon.setField("BEVILL", OutilsMagento.formaterTexteLongueurMax(pCommande.getVilleLiv(), 24));
          
          if (pCommande.getPaysLiv() != null) {
            enteteBon.setField("BEPAYL", OutilsMagento.formaterTexteLongueurMax(recupererLibellePaysSerieN(pCommande.getPaysLiv()), 30));
          }
          
          listeRcds.add(enteteBon);
          
          if (sgvmiv.isGestionComplementEntete()) {
            // Création de la ligne d'entête du bon complémentaire
            GenericRecord enteteComp = new GenericRecord();
            
            enteteComp.setField("BFETB", pCommande.getEtb());
            enteteComp.setField("BFMAG", magasinWeb);
            enteteComp.setField("BFDAT", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
            enteteComp.setField("BFRBV", "");
            enteteComp.setField("BFERL", "F");
            enteteComp.setField("BFNLI", 0);
            enteteComp.setField("BFCCT", OutilsMagento.formaterTexteLongueurMax(pCommande.getIdCommandeClient(), 10));
            enteteComp.setField("BFTTC", pCommande.getTotalTTC());
            enteteComp.setField("BFTHTL", pCommande.getTotalHT());
            // Frais de port
            if (pCommande.getFraisPortCommande() != null) {
              String codeTransporteur = "";
              if (pCommande.getCodeTransporteur() != null) {
                articleFraisPort = "PORT-" + codeTransporteur;
              }
              else {
                articleFraisPort = "PORT-";
              }
            }
            else {
              articleFraisPort = null;
            }
            if (articleFraisPort != null) {
              enteteComp.setField("BFARTF", articleFraisPort);
              enteteComp.setField("BFFRP", pCommande.getFraisPortCommande());
            }
            
            // REMISE
            if (articleRemise != null && pCommande.getRemise() != null) {
              enteteComp.setField("BFARTR", articleRemise);
              enteteComp.setField("BFREM", pCommande.getRemise());
            }
            
            enteteComp.setField("BFPDS", pCommande.retournerPoidsFormate());
            
            enteteComp.setField("BFTELF", pCommande.getTelFac());
            enteteComp.setField("BFTELL", pCommande.getTelLiv());
            
            if (pCommande.getServiceLivraison() != null) {
              enteteComp.setField("BFCRT", pCommande.getServiceLivraison());
            }
            if (pCommande.getPointRelais() != null) {
              enteteComp.setField("BFCREL", pCommande.getPointRelais());
            }
            if (pCommande.getPaysPointRelais() != null) {
              enteteComp.setField("BFPREL", pCommande.getPaysPointRelais());
            }
            // Numéro de contact
            if (pCommande.getRENUM() != null) {
              enteteComp.setField("BFCNUM", pCommande.getRENUM());
            }
            
            listeRcds.add(enteteComp);
          }
          
          int numLignes = 0;
          // Articles commentaires
          if (pCommande.getListeBlocNotes() != null) {
            GenericRecord commentaire = new GenericRecord();
            numLignes = numLignes + 10;
            // lignes commentaires
            commentaire.setField("BLETB", pCommande.getEtb());
            commentaire.setField("BLMAGE", magasinWeb);
            commentaire.setField("BLDAT", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
            commentaire.setField("BLRBV", "");
            commentaire.setField("BLNLI", numLignes);
            commentaire.setField("BLERL", "L");
            for (int j = 0; j < pCommande.getListeBlocNotes().size(); j++) {
              switch (j) {
                case 0:
                  commentaire.setField("BLLIB1", pCommande.getListeBlocNotes().get(j));
                  break;
                case 1:
                  commentaire.setField("BLLIB2", pCommande.getListeBlocNotes().get(j));
                  break;
                case 2:
                  commentaire.setField("BLLIB3", pCommande.getListeBlocNotes().get(j));
                  break;
                case 3:
                  commentaire.setField("BLLIB4", pCommande.getListeBlocNotes().get(j));
                  break;
                
                default:
                  break;
              }
            }
            listeRcds.add(commentaire);
          }
          
          // Préparation de la clef pour les lignes du futur bon
          for (int i = 0; i < pCommande.getListeArticlesCommandes().size(); i++) {
            numLignes = numLignes + 10;
            GenericRecord article = new GenericRecord();
            if (prixForces) {
              article.setField("BLNREM", VALEUR_PRIX_FORCES);
            }
            article.setField("BLETB", pCommande.getEtb());
            article.setField("BLMAGE", magasinWeb);
            article.setField("BLDAT", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
            article.setField("BLRBV", "");
            article.setField("BLNLI", "" + numLignes);
            article.setField("BLERL", "L");
            if (pCommande.getListeArticlesCommandes().get(i).getCodeArticle() != null) {
              article.setField("BLART", pCommande.getListeArticlesCommandes().get(i).getCodeArticle());
            }
            // libellés article
            if (pCommande.getListeArticlesCommandes().get(i).getBLLIB1() != null) {
              article.setField("BLLIB1", pCommande.getListeArticlesCommandes().get(i).getBLLIB1());
            }
            if (pCommande.getListeArticlesCommandes().get(i).getBLLIB2() != null) {
              article.setField("BLLIB2", pCommande.getListeArticlesCommandes().get(i).getBLLIB2());
            }
            if (pCommande.getListeArticlesCommandes().get(i).getBLLIB3() != null) {
              article.setField("BLLIB3", pCommande.getListeArticlesCommandes().get(i).getBLLIB3());
            }
            if (pCommande.getListeArticlesCommandes().get(i).getBLLIB4() != null) {
              article.setField("BLLIB4", pCommande.getListeArticlesCommandes().get(i).getBLLIB4());
            }
            
            article.setField("BLQTE", pCommande.getListeArticlesCommandes().get(i).getQteCommandee());
            article.setField("BLPRX", pCommande.getListeArticlesCommandes().get(i).getPrixPourInjection());
            article.setField("BLTVA", "1");
            article.setField("BLMHTF", pCommande.getListeArticlesCommandes().get(i).getTotalHT());
            
            if (pCommande.getListeArticlesCommandes().get(i).getDEEE() != null
                && !pCommande.getListeArticlesCommandes().get(i).getDEEE().trim().equals("")) {
              article.setField("BLD3EU", pCommande.getListeArticlesCommandes().get(i).getDEEE());
            }
            if (pCommande.getListeArticlesCommandes().get(i).getTotalDEEE() != null
                && !pCommande.getListeArticlesCommandes().get(i).getTotalDEEE().trim().equals("")) {
              article.setField("BLD3ET", pCommande.getListeArticlesCommandes().get(i).getTotalDEEE());
            }
            
            article.setField("BLRE1X", formaterRemiseSN(pCommande.getListeArticlesCommandes().get(i).getRemisePourCent()));
            article.setField("BLRE2X", formaterRemiseSN(null));
            article.setField("BLRE3X", formaterRemiseSN(null));
            article.setField("BLRE4X", formaterRemiseSN(null));
            article.setField("BLRE5X", formaterRemiseSN(null));
            article.setField("BLRE6X", formaterRemiseSN(null));
            article.setField("BLPRB", pCommande.getListeArticlesCommandes().get(i).getPrixBrutHT());
            
            listeRcds.add(article);
          }
          
          // Préparation de la ligne commentaire de fin envoyée par SFCC si elle est présente
          // Cette ligne peut être très longue donc la chaine sera découpée en autant de ligne commantaire que nécessaire
          if (pCommande.getListeLibelleCommentaireFin() != null) {
            List<String> listeLibelleCommentaireFin = pCommande.getListeLibelleCommentaireFin();
            int indiceLibelle = 0;
            // Calcul du nombre de lignes nécessaires au commentaire de fin (le nombre de libelle est un multiple de 4)
            int nombreLigneCommentaireFin = listeLibelleCommentaireFin.size() / 4;
            for (int ligne = 0; ligne < nombreLigneCommentaireFin; ligne++) {
              GenericRecord commentaireFin = new GenericRecord();
              numLignes = numLignes + 10;
              commentaireFin.setField("BLETB", pCommande.getEtb());
              commentaireFin.setField("BLMAGE", magasinWeb);
              commentaireFin.setField("BLDAT", formaterUneDateJJMMAAAA_SN(pCommande.getDateCreation()));
              commentaireFin.setField("BLRBV", "");
              commentaireFin.setField("BLNLI", numLignes);
              commentaireFin.setField("BLERL", "L");
              for (int numeroChamp = 1; numeroChamp <= 4; numeroChamp++) {
                // Initialisation du libellé en cours
                commentaireFin.setField("BLLIB" + numeroChamp, listeLibelleCommentaireFin.get(indiceLibelle));
                indiceLibelle++;
              }
              listeRcds.add(commentaireFin);
            }
          }
          
          // Insertion de la commande
          retour = sgvmiv.insertRecord(listeRcds);
          if (!retour) {
            pFlux.construireMessageErreur("[ImportationCommandeWebV3] genererUneCommandeSerieN() - sgvmiv.insertRecord: " + sgvmiv.getMsgError());
            qtemp.delete();
            system.deconnecter();
            return retour;
          }
          else {
            // Injection du PINJBDV avec le rapport mis à jour: soit avec des erreurs soit avec les infos du bon créé
            retour = sgvmiv.injectFile(rapport);
            if (!retour) {
              pFlux.construireMessageErreur(
                  "[ImportationCommandeWebV3] genererUneCommandeSerieN() - PB sgvmiv.injectFile: " + sgvmiv.getMsgError() + rapport);
              qtemp.delete();
              system.deconnecter();
              return retour;
            }
            else {
              if (rapport.containsKey("TYPE") && rapport.containsKey("ETB") && rapport.containsKey("NUMERO")
                  && rapport.containsKey("SUFFIXE")) {
                retour = true;
              }
            }
          }
        }
      }
      // Si on est en débug on ne supprime pas la qtemp
      if (parametresBibli.isModeDebug()) {
        retour = true;
      }
      else {
        retour = qtemp.delete();
      }
      
      if (!retour) {
        pFlux.construireMessageErreur("[ImportationCommandeWebV3] genererUneCommandeSerieN() - qtemp.delete(): " + qtemp.getMsgError());
      }
    }
    else {
      pFlux.construireMessageErreur("[ImportationCommandeWebV3] genererUneCommandeSerieN() - qtemp.create(): " + qtemp.getMsgError());
    }
    
    try {
      system.deconnecter();
    }
    catch (Exception e) {
      Trace.erreur(e, "[ImportationCommandeWebV3] genererUneCommandeSerieN() - Erreur lors de la déconnexion.");
    }
    return retour;
  }
  
  /**
   * Initialiser les paramètres propres à l'établissement de vente.
   */
  private void initParametresWeb(String pCodeEtablissement) {
    codeVendeurWeb = parametresBibli.getVendeurMagento();
    magasinWeb = parametresBibli.getMagasinMagento();
    // TODO en attente de la création dans Série N de ces fucking articles
    articleFraisPort = null;
    articleRemise = "REMISE";
  }
  
  /**
   * Permet de recevoir un pourcentage décimal de SFCCet de le formater à la mode moisie de Série N.
   * C'est à dire [D][u][d][c] D = dizaine u = unité d = dixième c = centième.
   */
  private String formaterRemiseSN(String pRemiseBrute) {
    if (pRemiseBrute == null || pRemiseBrute.equals("")) {
      return "0000";
    }
    
    String remise = pRemiseBrute;
    String decimale = ".";
    String partieUn = "00";
    String partieDeux = "00";
    
    String[] tabValeurs = pRemiseBrute.split(Pattern.quote(decimale));
    if (tabValeurs.length == 2) {
      if (pRemiseBrute.length() > 5) {
        return remise;
      }
      
      if (tabValeurs[1].length() == 1) {
        partieDeux = tabValeurs[1] + "0";
      }
      else if (tabValeurs[1].length() == 2) {
        partieDeux = tabValeurs[1];
      }
      // Si 00 ou plus alors on retourne 0
    }
    if (tabValeurs.length == 1 || tabValeurs.length == 2) {
      if (tabValeurs[0].length() == 1) {
        partieUn = "0" + tabValeurs[0];
      }
      else if (tabValeurs[0].length() == 2) {
        partieUn = tabValeurs[0];
      }
      // Si 00 ou plus alors on retourne 0
    }
    
    remise = partieUn + partieDeux;
    
    return remise;
  }
  
  /**
   * On récupère le libellé d'un pays sur la base de son code Série N.
   */
  private String recupererLibellePaysSerieN(String pCodePays) {
    String retour = "";
    
    if (pCodePays == null || pCodePays.trim().equals("")) {
      return "FRANCE                     " + "FR ";
    }
    
    String libelle = null;
    ArrayList<GenericRecord> liste = queryManager.select(" SELECT PARFIL FROM " + queryManager.getLibrary() + ".PSEMPARM "
        + " WHERE PARCLE LIKE '   CP%' AND SUBSTR(PARCLE, 6, 3) = '" + pCodePays + "' ");
    
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("PARFIL")) {
        libelle = liste.get(0).getField("PARFIL").toString().trim();
      }
    }
    
    if (libelle != null) {
      int nbCarLib = 26;
      int nbCar = libelle.length();
      int carManquants = nbCarLib - nbCar;
      retour = libelle;
      
      if (carManquants > 0) {
        for (int i = 0; i < carManquants; i++) {
          retour += " ";
        }
      }
      
      retour += " " + pCodePays;
    }
    
    return retour;
  }
  
  /**
   * Formatage du code client pour un insert dans le RLIND de PSEMRTLM avant concaténation.
   */
  private String formaterLeClientPourRLIND(String pCodeClient) {
    String retour = pCodeClient;
    if (retour != null && retour.length() > 0) {
      if (retour.length() == 1) {
        retour = "00000" + retour;
      }
      else if (retour.length() == 2) {
        retour = "0000" + retour;
      }
      else if (retour.length() == 3) {
        retour = "000" + retour;
      }
      else if (retour.length() == 4) {
        retour = "00" + retour;
      }
      else if (retour.length() == 5) {
        retour = "0" + retour;
      }
    }
    else {
      retour = "000000";
    }
    
    return retour;
  }
  
  /**
   * Formater une date jj/mm/aaaa.
   */
  private String formaterUneDateJJMMAAAA_SN(String pDate) {
    String retour = "";
    if (pDate == null || pDate.trim().equals("")) {
      retour = DateHeure.getFormateDateHeure(DateHeure.AAMMJJ);
    }
    else {
      retour = pDate;
      String[] tab = retour.split("/");
      if (tab.length == 3) {
        if (tab[0].length() == 2 && tab[1].length() == 2 && tab[2].length() == 4) {
          retour = tab[2].substring(2) + tab[1] + tab[0];
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Formater le statut SFCC en statut Série N.
   */
  private String formaterStatutWebSN(String pMagento) {
    // TODO avec une vraie table de paramètres propre plutôt que ces tests en DUUUUR
    // Faire une classe TraitementMagento
    
    if (pMagento == null) {
      return "ATT";
    }
    
    String retour = "ATT";
    
    if (pMagento.equals("0")) {
      retour = "ATT";
    }
    else if (pMagento.equals("1") || pMagento.equals("3")) {
      retour = "HOM";
    }
    else if (pMagento.equals("4")) {
      retour = "EXP";
    }
    else if (pMagento.equals("6") || pMagento.equals("7")) {
      retour = "FAC";
    }
    
    return retour;
  }
}
