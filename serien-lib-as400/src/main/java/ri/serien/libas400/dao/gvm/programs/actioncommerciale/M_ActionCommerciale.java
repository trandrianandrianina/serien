/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs.actioncommerciale;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.serien.libas400.dao.exp.programs.contact.M_Contact;
import ri.serien.libas400.dao.exp.programs.contact.M_EvenementContact;
import ri.serien.libas400.dao.exp.programs.contact.M_LienEvenementContact;
import ri.serien.libas400.dao.gvm.database.files.FFD_Pgvmaccm;
import ri.serien.libas400.dao.gvm.programs.client.M_Client;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

/**
 * Traitement pour UNE action commerciale
 * Classe contenant les zones du PGVMACCM
 * @author Administrateur
 * 
 */
public class M_ActionCommerciale extends FFD_Pgvmaccm {
  // Variables
  private M_EvenementContact evenementContact = new M_EvenementContact(querymg);
  private M_Client client = new M_Client(querymg);
  private ArrayList<M_LienEvenementContact> lienEvenementContact = new ArrayList<M_LienEvenementContact>();
  private LinkedHashMap<String, String> listObjectAction = null;
  
  private ArrayList<M_Contact> listContact = new ArrayList<M_Contact>();
  private String[] listLabelObjectAction = null;
  private String[] listCodeObjectAction = null;
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public M_ActionCommerciale(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    evenementContact.initialization();
    lienEvenementContact.clear();
    client.initialization();
    
    super.initialization();
  }
  
  /**
   * Initialise les données du record avec cet objet
   * @param rcd
   * @return
   */
  @Override
  public boolean initGenericRecord(GenericRecord rcd, boolean clearBefore) {
    evenementContact.initGenericRecord(rcd, false);
    return super.initGenericRecord(rcd, clearBefore);
  }
  
  /**
   * Initialise l'objet avec les données du record
   * @param rcd
   * @return
   */
  @Override
  public boolean initObject(GenericRecord rcd, boolean doinit) {
    evenementContact.initObject(rcd, true);
    // lienEvenementContact.initObject(rcd);
    return super.initObject(rcd, false);
  }
  
  /**
   * Initialise les données du record avec cet objet
   * @param rcd
   * @return
   * 
   *         public boolean initGenericRecord(GenericRecord rcd, boolean clearBefore)
   *         {
   *         if( rcd == null ) {
   *         return false;
   *         }
   * 
   *         evenementContact.initGenericRecord(rcd, false);
   *         //lienEvenementContact.initGenericRecord(rcd, false);
   * 
   *         rcd.setOmittedField(omittedField);
   *         return rcd.fromObject(this, clearBefore);
   *         }
   */
  
  /**
   * Initialise l'objet avec les données du record
   * @param rcd
   * @return
   * 
   *         public boolean initObject(GenericRecord rcd)
   *         {
   *         if( rcd == null ) {
   *         return false;
   *         }
   *         initialization();
   *         evenementContact.initObject(rcd);
   *         //lienEvenementContact.initObject(rcd);
   * 
   *         return rcd.toObject(this);
   *         }
   */
  
  /**
   * Insère une action commerciale dans le table
   * @return
   * 
   */
  @Override
  public boolean insertInDatabase() {
    int id = -1;
    
    // Insertion dans le PSEMEVTM
    if (evenementContact.insertInDatabase()) {
      id = evenementContact.getETID();
    }
    else {
      msgErreur += '\n' + querymg.getMsgError();
      return false;
    }
    
    // Insertion dans le PGVMACCM
    setACID(id);
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert("PGVMACCM", querymg.getLibrary());
    int ret = querymg.requete(requete);
    if (ret == -1) {
      msgErreur += '\n' + querymg.getMsgError();
      return false;
    }
    
    // Insertion dans le PSEMLECM
    if ((listContact != null) && (listContact.size() == 0)) {
      msgErreur += "\nLa liste des contacts est vide.";
      return false;
    }
    lienEvenementContact.clear();
    for (M_Contact contact : listContact) {
      M_LienEvenementContact lec = new M_LienEvenementContact(querymg);
      lienEvenementContact.add(lec);
      lec.setECIDC(contact.getRENUM());
      lec.setECETBC(contact.getREETB());
      lec.setECIDE(id);
      lec.setECTYP(contact.getCodeTypeContact());
      lec.setECHIE(contact.getCodeHierarchy());
      if (!lec.insertInDatabase()) {
        msgErreur += '\n' + querymg.getMsgError();
        return false; // A voir car pas vraiment judicieux
      }
    }
    
    return true;
  }
  
  /**
   * Modifie une action commerciale dans le table
   * @return
   * 
   */
  @Override
  public boolean updateInDatabase() {
    // Modification dans le PSEMEVTM
    if (!evenementContact.updateInDatabase()) {
      msgErreur += '\n' + evenementContact.getMsgError();
      return false;
    }
    
    // Modification dans le PGVMACCM
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestUpdate("PGVMACCM", querymg.getLibrary(), "ACID = " + getACID());
    int ret = querymg.requete(requete);
    if (ret == -1) {
      msgErreur += '\n' + querymg.getMsgError();
      return false;
    }
    
    // Modification dans le PSEMLECM
    analyzeListContacts();
    for (M_LienEvenementContact lec : lienEvenementContact) {
      switch (lec.getActionInDatabase()) {
        case M_LienEvenementContact.ACTION_INSERT:
          lec.insertInDatabase();
          break;
        case M_LienEvenementContact.ACTION_UPDATE:
          lec.updateInDatabase();
          break;
        case M_LienEvenementContact.ACTION_DELETE:
          lec.deleteInDatabase(false);
          break;
      }
    }
    
    return true;
  }
  
  /**
   * Suppression de l'enregistrement courant
   * @return
   * 
   */
  @Override
  public boolean deleteInDatabase() {
    // Nettoyage dans la table des liens contacts
    if (lienEvenementContact.size() >= 1) {
      lienEvenementContact.get(0).deleteInDatabase(true);
    }
    lienEvenementContact.clear();
    
    // Suppression dans le PSEMEVTM
    if (!evenementContact.deleteInDatabase()) {
      return false;
    }
    
    // Suppression dans le PGVMACCM
    String requete = "delete from " + querymg.getLibrary() + ".PGVMACCM where ACID=" + getACID();
    boolean ret = request(requete);
    
    // listObjectAction.clear(); // Variable commune donc non
    listContact.clear();
    
    return ret;
    
  }
  
  /**
   * Récupération des contacts pour une action commerciale
   * @param id
   * @return
   * 
   */
  public boolean loadContacts(QueryManager querymg) {
    // Lecture de la base afin de récupérer les contacts liées
    String requete = "select * from " + querymg.getLibrary() + ".psemrtem rte, " + querymg.getLibrary()
        + ".psemlecm lec where lec.ecide = " + getACID() + " and rte.renum = lec.ecidc and rte.reetb = lec.ecetbc";
    ArrayList<GenericRecord> listrcd = querymg.select(requete);
    if (listrcd == null) {
      return false;
    }
    // Chargement des classes avec les données de la base
    if (listContact != null) {
      listContact.clear();
    }
    else {
      listContact = new ArrayList<M_Contact>(listrcd.size());
    }
    lienEvenementContact.clear();
    for (GenericRecord rcd : listrcd) {
      M_Contact contact = new M_Contact(querymg);
      contact.initObject(rcd, true);
      M_LienEvenementContact lec = new M_LienEvenementContact(querymg);
      lec.initObject(rcd, true);
      contact.setCodeHierarchy(lec.getECHIE());
      contact.setCodeTypeContact(lec.getECTYP());
      listContact.add(contact);
      lienEvenementContact.add(lec);
    }
    
    return true;
  }
  
  /**
   * Retourne la liste des Contacts d'un type donné
   * @param filtre
   * @return
   */
  public M_Contact[] getFiltreContacts(String filtre) {
    ArrayList<M_Contact> list = new ArrayList<M_Contact>();
    if ((listContact == null) || (filtre == null)) {
      return new M_Contact[0];
    }
    
    for (M_Contact contact : listContact) {
      if (contact.getCodeTypeContact().equals(filtre)) {
        list.add(contact);
      }
    }
    M_Contact[] tab = new M_Contact[list.size()];
    tab = list.toArray(tab);
    list.clear();
    return tab;
    // return (Contact[]) list.toArray(tab);
  }
  
  /**
   * Récupération des informations du client (Penser à voir comment gérer la zone pour le suffixe ETSUFT)
   * @param querymg
   * @return
   * 
   */
  public boolean loadClient(QueryManager querymg) {
    // Lecture de la base afin de récupérer le client
    String requete = "select * from " + querymg.getLibrary() + ".pgvmclim where clcli = " + evenementContact.getETIDT() + " and clliv = "
        + evenementContact.getETSUFT() + " and cletb = '" + evenementContact.getETETBT() + "'";
    ArrayList<GenericRecord> listrcd = querymg.select(requete);
    if ((listrcd == null) || (listrcd.size() < 1)) {
      return false;
    }
    // Chargement des classes avec les données de la base
    boolean ret = client.initObject(listrcd.get(0), true);
    if (ret) {
      ret = client.loadContacts();
    }
    
    return ret;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  private boolean analyzeListContacts() {
    if ((listContact == null) || (listContact.size() == 0)) {
      msgErreur += "\nLa liste des contacts est vide.";
      return false;
    }
    
    // Initialisation du tableau contenant les actions à faire sur la table des liens
    for (M_Contact contact : listContact) {
      boolean found = false;
      for (M_LienEvenementContact lec : lienEvenementContact) {
        // contact.getREETB() + "| == |" + lec.getECETBC() + "| && |" + contact.getCodeTypeContact() + "| == |" +
        // lec.getECTYP() + '|');
        if ((contact.getRENUM() == lec.getECIDC()) && contact.getREETB().equals(lec.getECETBC())
            && contact.getCodeTypeContact().equals(lec.getECTYP())) {
          found = true;
          lec.setActionInDatabase(M_LienEvenementContact.ACTION_UPDATE);
        }
      }
      // Il n'est pas dans la liste alors on l'insère
      if (!found) {
        M_LienEvenementContact nlec = new M_LienEvenementContact(querymg);
        nlec.setECIDC(contact.getRENUM());
        nlec.setECETBC(contact.getREETB());
        nlec.setECIDE(getACID());
        nlec.setECHIE(contact.getCodeHierarchy());
        nlec.setECTYP(contact.getCodeTypeContact());
        nlec.setActionInDatabase(M_LienEvenementContact.ACTION_INSERT);
        lienEvenementContact.add(nlec);
      }
    }
    // On marque les liens que l'on doit supprimer (ceux qui sont en NOTHING pour l'instant)
    for (M_LienEvenementContact lec : lienEvenementContact) {
      if (lec.getActionInDatabase() == M_LienEvenementContact.ACTION_NOTHING) {
        lec.setActionInDatabase(M_LienEvenementContact.ACTION_DELETE);
      }
    }
    
    return true;
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
    
    if (evenementContact != null) {
      evenementContact.dispose();
    }
    if (client != null) {
      client.dispose();
    }
    if (lienEvenementContact != null) {
      for (M_LienEvenementContact lec : lienEvenementContact) {
        lec.dispose();
      }
      lienEvenementContact.clear();
    }
    // if( listObjectAction != null){
    // listObjectAction.clear();
    // }
    listObjectAction = null;
    if (listContact != null) {
      for (M_Contact c : listContact) {
        c.dispose();
      }
      listContact.clear();
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le evenementContact
   */
  public M_EvenementContact getEvenementContact() {
    return evenementContact;
  }
  
  /**
   * @param evenementContact le evenementContact à définir
   */
  public void setEvenementContact(M_EvenementContact evenementContact) {
    this.evenementContact = evenementContact;
  }
  
  /**
   * @return le lienEvenementContact
   */
  public ArrayList<M_LienEvenementContact> getLienEvenementContact() {
    return lienEvenementContact;
  }
  
  /**
   * @param lienEvenementContact le lienEvenementContact à définir
   */
  public void setLienEvenementContact(ArrayList<M_LienEvenementContact> lienEvenementContact) {
    this.lienEvenementContact = lienEvenementContact;
  }
  
  /**
   * @return le listObjectAction
   */
  public HashMap<String, String> getListObjectAction() {
    return listObjectAction;
  }
  
  /**
   * @param listObjectAction le listObjectAction à définir
   */
  public void setListObjectAction(LinkedHashMap<String, String> listObjectAction) {
    this.listObjectAction = listObjectAction;
    
    // Chargement de la table des libellés
    listLabelObjectAction = new String[listObjectAction.size()];
    listCodeObjectAction = new String[listObjectAction.size()];
    int i = 0;
    for (Entry<String, String> entry : listObjectAction.entrySet()) {
      listCodeObjectAction[i] = entry.getKey().trim();
      listLabelObjectAction[i++] = entry.getValue().trim();
    }
  }
  
  /**
   * Retourne le libellé de l'objet
   * @return
   */
  public String getLabelACOBJ() {
    String code = getACOBJ();
    if (code == null) {
      return "";
    }
    for (int i = 0; i < listLabelObjectAction.length; i++) {
      if (listCodeObjectAction[i].equals(code)) {
        return listLabelObjectAction[i];
      }
    }
    return code;
  }
  
  /**
   * @return le listContact
   */
  public ArrayList<M_Contact> getListContact() {
    return listContact;
  }
  
  /**
   * @param listContact le listContact à définir
   */
  public void setListContact(ArrayList<M_Contact> listContact) {
    this.listContact = listContact;
  }
  
  /**
   * @return le client
   */
  public M_Client getClient() {
    return client;
  }
  
  /**
   * @param client le client à définir
   */
  public void setClient(M_Client client) {
    this.client = client;
  }
  
  /**
   * @return le listLabelObjectAction
   */
  public String[] getListLabelObjectAction() {
    return listLabelObjectAction;
  }
  
  /**
   * @return le listCodeObjectAction
   */
  public String[] getListCodeObjectAction() {
    return listCodeObjectAction;
  }
  
}
