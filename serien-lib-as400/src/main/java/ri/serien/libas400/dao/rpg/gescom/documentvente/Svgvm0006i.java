/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0006i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PICLI = 6;
  public static final int DECIMAL_PICLI = 0;
  public static final int SIZE_PILIV = 3;
  public static final int DECIMAL_PILIV = 0;
  public static final int SIZE_PIMAG = 2;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIENV = 1;
  public static final int SIZE_PITYP = 3;
  public static final int SIZE_PICTR = 2;
  public static final int SIZE_PIDLS = 7;
  public static final int DECIMAL_PIDLS = 0;
  public static final int SIZE_PIDLP = 7;
  public static final int DECIMAL_PIDLP = 0;
  public static final int SIZE_PIDAT2 = 7;
  public static final int DECIMAL_PIDAT2 = 0;
  public static final int SIZE_PITFA = 1;
  public static final int SIZE_PINOM3 = 30;
  public static final int SIZE_PICPL3 = 30;
  public static final int SIZE_PIRUE3 = 30;
  public static final int SIZE_PILOC3 = 30;
  public static final int SIZE_PICDP3 = 5;
  public static final int SIZE_PIVIL3 = 24;
  public static final int SIZE_PINOM2 = 30;
  public static final int SIZE_PICPL2 = 30;
  public static final int SIZE_PIRUE2 = 30;
  public static final int SIZE_PILOC2 = 30;
  public static final int SIZE_PICDP2 = 5;
  public static final int SIZE_PIVIL2 = 24;
  public static final int SIZE_PICNT = 9;
  public static final int SIZE_PINCC = 8;
  public static final int SIZE_PIRCC = 25;
  public static final int SIZE_PIACT = 4;
  public static final int SIZE_PIVEH = 10;
  public static final int SIZE_PIMEX = 2;
  public static final int SIZE_PINFA = 7;
  public static final int DECIMAL_PINFA = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 431;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PICOD = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PICLI = 5;
  public static final int VAR_PILIV = 6;
  public static final int VAR_PIMAG = 7;
  public static final int VAR_PIDAT = 8;
  public static final int VAR_PIENV = 9;
  public static final int VAR_PITYP = 10;
  public static final int VAR_PICTR = 11;
  public static final int VAR_PIDLS = 12;
  public static final int VAR_PIDLP = 13;
  public static final int VAR_PIDAT2 = 14;
  public static final int VAR_PITFA = 15;
  public static final int VAR_PINOM3 = 16;
  public static final int VAR_PICPL3 = 17;
  public static final int VAR_PIRUE3 = 18;
  public static final int VAR_PILOC3 = 19;
  public static final int VAR_PICDP3 = 20;
  public static final int VAR_PIVIL3 = 21;
  public static final int VAR_PINOM2 = 22;
  public static final int VAR_PICPL2 = 23;
  public static final int VAR_PIRUE2 = 24;
  public static final int VAR_PILOC2 = 25;
  public static final int VAR_PICDP2 = 26;
  public static final int VAR_PIVIL2 = 27;
  public static final int VAR_PICNT = 28;
  public static final int VAR_PINCC = 29;
  public static final int VAR_PIRCC = 30;
  public static final int VAR_PIACT = 31;
  public static final int VAR_PIVEH = 32;
  public static final int VAR_PIMEX = 33;
  public static final int VAR_PINFA = 34;
  public static final int VAR_PIARR = 35;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private String picod = ""; // Code ERL "D","E",X","9"
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe
  private BigDecimal picli = BigDecimal.ZERO; // Numéro client
  private BigDecimal piliv = BigDecimal.ZERO; // Suffixe client
  private String pimag = ""; // Magasin
  private BigDecimal pidat = BigDecimal.ZERO; // Date du document
  private String pienv = ""; // N.U.
  private String pityp = ""; // Type de documents
  private String pictr = ""; // Code transporteur
  private BigDecimal pidls = BigDecimal.ZERO; // Date livraison souhaitée
  private BigDecimal pidlp = BigDecimal.ZERO; // Date livraison prévue
  private BigDecimal pidat2 = BigDecimal.ZERO; // Date validité devis
  private String pitfa = ""; // Code type de facturation
  private String pinom3 = ""; // Nom client
  private String picpl3 = ""; // Complément du nom
  private String pirue3 = ""; // Rue
  private String piloc3 = ""; // Localité
  private String picdp3 = ""; // Code Postal
  private String pivil3 = ""; // Ville
  private String pinom2 = ""; // Nom client
  private String picpl2 = ""; // Complément du nom
  private String pirue2 = ""; // Rue
  private String piloc2 = ""; // Localité
  private String picdp2 = ""; // Code Postal
  private String pivil2 = ""; // Ville
  private String picnt = ""; // Clé du N° de contact
  private String pincc = ""; // Référence courte commande
  private String pircc = ""; // Référence longue commande
  private String piact = ""; // Affaire ou chantier
  private String piveh = ""; // Immatriculation Véhicule
  private String pimex = ""; // Mode d"expédition
  private BigDecimal pinfa = BigDecimal.ZERO; // Numéro de facture
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9"
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe
      new AS400ZonedDecimal(SIZE_PICLI, DECIMAL_PICLI), // Numéro client
      new AS400ZonedDecimal(SIZE_PILIV, DECIMAL_PILIV), // Suffixe client
      new AS400Text(SIZE_PIMAG), // Magasin
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date du document
      new AS400Text(SIZE_PIENV), // N.U.
      new AS400Text(SIZE_PITYP), // Type de documents
      new AS400Text(SIZE_PICTR), // Code transporteur
      new AS400ZonedDecimal(SIZE_PIDLS, DECIMAL_PIDLS), // Date livraison souhaitée
      new AS400ZonedDecimal(SIZE_PIDLP, DECIMAL_PIDLP), // Date livraison prévue
      new AS400ZonedDecimal(SIZE_PIDAT2, DECIMAL_PIDAT2), // Date validité devis
      new AS400Text(SIZE_PITFA), // Code type de facturation
      new AS400Text(SIZE_PINOM3), // Nom client
      new AS400Text(SIZE_PICPL3), // Complément du nom
      new AS400Text(SIZE_PIRUE3), // Rue
      new AS400Text(SIZE_PILOC3), // Localité
      new AS400Text(SIZE_PICDP3), // Code Postal
      new AS400Text(SIZE_PIVIL3), // Ville
      new AS400Text(SIZE_PINOM2), // Nom client
      new AS400Text(SIZE_PICPL2), // Complément du nom
      new AS400Text(SIZE_PIRUE2), // Rue
      new AS400Text(SIZE_PILOC2), // Localité
      new AS400Text(SIZE_PICDP2), // Code Postal
      new AS400Text(SIZE_PIVIL2), // Ville
      new AS400Text(SIZE_PICNT), // Clé du N° de contact
      new AS400Text(SIZE_PINCC), // Référence courte commande
      new AS400Text(SIZE_PIRCC), // Référence longue commande
      new AS400Text(SIZE_PIACT), // Affaire ou chantier
      new AS400Text(SIZE_PIVEH), // Immatriculation Véhicule
      new AS400Text(SIZE_PIMEX), // Mode d"expédition
      new AS400ZonedDecimal(SIZE_PINFA, DECIMAL_PINFA), // Numéro de facture
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, pietb, picod, pinum, pisuf, picli, piliv, pimag, pidat, pienv, pityp, pictr, pidls, pidlp, pidat2, pitfa,
          pinom3, picpl3, pirue3, piloc3, picdp3, pivil3, pinom2, picpl2, pirue2, piloc2, picdp2, pivil2, picnt, pincc, pircc, piact,
          piveh, pimex, pinfa, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    picod = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    picli = (BigDecimal) output[5];
    piliv = (BigDecimal) output[6];
    pimag = (String) output[7];
    pidat = (BigDecimal) output[8];
    pienv = (String) output[9];
    pityp = (String) output[10];
    pictr = (String) output[11];
    pidls = (BigDecimal) output[12];
    pidlp = (BigDecimal) output[13];
    pidat2 = (BigDecimal) output[14];
    pitfa = (String) output[15];
    pinom3 = (String) output[16];
    picpl3 = (String) output[17];
    pirue3 = (String) output[18];
    piloc3 = (String) output[19];
    picdp3 = (String) output[20];
    pivil3 = (String) output[21];
    pinom2 = (String) output[22];
    picpl2 = (String) output[23];
    pirue2 = (String) output[24];
    piloc2 = (String) output[25];
    picdp2 = (String) output[26];
    pivil2 = (String) output[27];
    picnt = (String) output[28];
    pincc = (String) output[29];
    pircc = (String) output[30];
    piact = (String) output[31];
    piveh = (String) output[32];
    pimex = (String) output[33];
    pinfa = (BigDecimal) output[34];
    piarr = (String) output[35];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPicli(BigDecimal pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = pPicli.setScale(DECIMAL_PICLI, RoundingMode.HALF_UP);
  }
  
  public void setPicli(Integer pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = BigDecimal.valueOf(pPicli);
  }
  
  public Integer getPicli() {
    return picli.intValue();
  }
  
  public void setPiliv(BigDecimal pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = pPiliv.setScale(DECIMAL_PILIV, RoundingMode.HALF_UP);
  }
  
  public void setPiliv(Integer pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = BigDecimal.valueOf(pPiliv);
  }
  
  public Integer getPiliv() {
    return piliv.intValue();
  }
  
  public void setPimag(String pPimag) {
    if (pPimag == null) {
      return;
    }
    pimag = pPimag;
  }
  
  public String getPimag() {
    return pimag;
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPienv(Character pPienv) {
    if (pPienv == null) {
      return;
    }
    pienv = String.valueOf(pPienv);
  }
  
  public Character getPienv() {
    return pienv.charAt(0);
  }
  
  public void setPityp(String pPityp) {
    if (pPityp == null) {
      return;
    }
    pityp = pPityp;
  }
  
  public String getPityp() {
    return pityp;
  }
  
  public void setPictr(String pPictr) {
    if (pPictr == null) {
      return;
    }
    pictr = pPictr;
  }
  
  public String getPictr() {
    return pictr;
  }
  
  public void setPidls(BigDecimal pPidls) {
    if (pPidls == null) {
      return;
    }
    pidls = pPidls.setScale(DECIMAL_PIDLS, RoundingMode.HALF_UP);
  }
  
  public void setPidls(Integer pPidls) {
    if (pPidls == null) {
      return;
    }
    pidls = BigDecimal.valueOf(pPidls);
  }
  
  public void setPidls(Date pPidls) {
    if (pPidls == null) {
      return;
    }
    pidls = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidls));
  }
  
  public Integer getPidls() {
    return pidls.intValue();
  }
  
  public Date getPidlsConvertiEnDate() {
    return ConvertDate.db2ToDate(pidls.intValue(), null);
  }
  
  public void setPidlp(BigDecimal pPidlp) {
    if (pPidlp == null) {
      return;
    }
    pidlp = pPidlp.setScale(DECIMAL_PIDLP, RoundingMode.HALF_UP);
  }
  
  public void setPidlp(Integer pPidlp) {
    if (pPidlp == null) {
      return;
    }
    pidlp = BigDecimal.valueOf(pPidlp);
  }
  
  public void setPidlp(Date pPidlp) {
    if (pPidlp == null) {
      return;
    }
    pidlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidlp));
  }
  
  public Integer getPidlp() {
    return pidlp.intValue();
  }
  
  public Date getPidlpConvertiEnDate() {
    return ConvertDate.db2ToDate(pidlp.intValue(), null);
  }
  
  public void setPidat2(BigDecimal pPidat2) {
    if (pPidat2 == null) {
      return;
    }
    pidat2 = pPidat2.setScale(DECIMAL_PIDAT2, RoundingMode.HALF_UP);
  }
  
  public void setPidat2(Integer pPidat2) {
    if (pPidat2 == null) {
      return;
    }
    pidat2 = BigDecimal.valueOf(pPidat2);
  }
  
  public void setPidat2(Date pPidat2) {
    if (pPidat2 == null) {
      return;
    }
    pidat2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat2));
  }
  
  public Integer getPidat2() {
    return pidat2.intValue();
  }
  
  public Date getPidat2ConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat2.intValue(), null);
  }
  
  public void setPitfa(Character pPitfa) {
    if (pPitfa == null) {
      return;
    }
    pitfa = String.valueOf(pPitfa);
  }
  
  public Character getPitfa() {
    return pitfa.charAt(0);
  }
  
  public void setPinom3(String pPinom3) {
    if (pPinom3 == null) {
      return;
    }
    pinom3 = pPinom3;
  }
  
  public String getPinom3() {
    return pinom3;
  }
  
  public void setPicpl3(String pPicpl3) {
    if (pPicpl3 == null) {
      return;
    }
    picpl3 = pPicpl3;
  }
  
  public String getPicpl3() {
    return picpl3;
  }
  
  public void setPirue3(String pPirue3) {
    if (pPirue3 == null) {
      return;
    }
    pirue3 = pPirue3;
  }
  
  public String getPirue3() {
    return pirue3;
  }
  
  public void setPiloc3(String pPiloc3) {
    if (pPiloc3 == null) {
      return;
    }
    piloc3 = pPiloc3;
  }
  
  public String getPiloc3() {
    return piloc3;
  }
  
  public void setPicdp3(String pPicdp3) {
    if (pPicdp3 == null) {
      return;
    }
    picdp3 = pPicdp3;
  }
  
  public String getPicdp3() {
    return picdp3;
  }
  
  public void setPivil3(String pPivil3) {
    if (pPivil3 == null) {
      return;
    }
    pivil3 = pPivil3;
  }
  
  public String getPivil3() {
    return pivil3;
  }
  
  public void setPinom2(String pPinom2) {
    if (pPinom2 == null) {
      return;
    }
    pinom2 = pPinom2;
  }
  
  public String getPinom2() {
    return pinom2;
  }
  
  public void setPicpl2(String pPicpl2) {
    if (pPicpl2 == null) {
      return;
    }
    picpl2 = pPicpl2;
  }
  
  public String getPicpl2() {
    return picpl2;
  }
  
  public void setPirue2(String pPirue2) {
    if (pPirue2 == null) {
      return;
    }
    pirue2 = pPirue2;
  }
  
  public String getPirue2() {
    return pirue2;
  }
  
  public void setPiloc2(String pPiloc2) {
    if (pPiloc2 == null) {
      return;
    }
    piloc2 = pPiloc2;
  }
  
  public String getPiloc2() {
    return piloc2;
  }
  
  public void setPicdp2(String pPicdp2) {
    if (pPicdp2 == null) {
      return;
    }
    picdp2 = pPicdp2;
  }
  
  public String getPicdp2() {
    return picdp2;
  }
  
  public void setPivil2(String pPivil2) {
    if (pPivil2 == null) {
      return;
    }
    pivil2 = pPivil2;
  }
  
  public String getPivil2() {
    return pivil2;
  }
  
  public void setPicnt(String pPicnt) {
    if (pPicnt == null) {
      return;
    }
    picnt = pPicnt;
  }
  
  public String getPicnt() {
    return picnt;
  }
  
  public void setPincc(String pPincc) {
    if (pPincc == null) {
      return;
    }
    pincc = pPincc;
  }
  
  public String getPincc() {
    return pincc;
  }
  
  public void setPircc(String pPircc) {
    if (pPircc == null) {
      return;
    }
    pircc = pPircc;
  }
  
  public String getPircc() {
    return pircc;
  }
  
  public void setPiact(String pPiact) {
    if (pPiact == null) {
      return;
    }
    piact = pPiact;
  }
  
  public String getPiact() {
    return piact;
  }
  
  public void setPiveh(String pPiveh) {
    if (pPiveh == null) {
      return;
    }
    piveh = pPiveh;
  }
  
  public String getPiveh() {
    return piveh;
  }
  
  public void setPimex(String pPimex) {
    if (pPimex == null) {
      return;
    }
    pimex = pPimex;
  }
  
  public String getPimex() {
    return pimex;
  }
  
  public void setPinfa(BigDecimal pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = pPinfa.setScale(DECIMAL_PINFA, RoundingMode.HALF_UP);
  }
  
  public void setPinfa(Integer pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(pPinfa);
  }
  
  public void setPinfa(Date pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pPinfa));
  }
  
  public Integer getPinfa() {
    return pinfa.intValue();
  }
  
  public Date getPinfaConvertiEnDate() {
    return ConvertDate.db2ToDate(pinfa.intValue(), null);
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
