/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs.tiers.pricejet;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.vente.gvc.pricejet.ConcurrentPriceJet;
import ri.serien.libcommun.gescom.vente.gvc.pricejet.ObjetPricejet;
import ri.serien.libcommun.gescom.vente.gvc.pricejet.TarifPriceJet;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.encodage.Chiffrage;

public class GestionPriceJet {
  // Constantes
  private final static String ERROR_CODE_WAIT = "?WAIT";
  private final static String ERROR_CODE_ACCOUNT = "?ACCOUNT";
  private final static String ERROR_CODE_AUTH = "?AUTH";
  private final static String ERROR_MSG_ACCOUNT = "Le compte (login) n'existe pas.";
  private final static String ERROR_MSG_AUTH = "Votre authentification n'est pas correcte. Il y a un problème dans la signature.";
  private final static String URL = "https://api.pricejet.fr/api/?";
  private final static String SIGNATURE = "signature=";
  private final static String LOGIN = "login=";
  private final static String LISTE = "liste=";
  private final static String ARTICLE = "article=";
  private final static String TYPE = "type=";
  private final static String CONCURRENT = "concurrent=";
  // public final static String CONCURRENT_A_ELIMINER = "1";
  
  // TODO à mettre dans le paramètrage lorsqu'il existera
  private final static String ACCOUNT = "PhaseNeutre";
  private final static String KEY = "a79bd5f48594499a4d89786ed4c0960a";
  
  // Variables
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  protected GsonBuilder builderJSON = null;
  protected Gson gson = null;
  private ArrayList<ConcurrentPriceJet> listeConcurrents = null;
  
  /**
   * Initialisation JSON
   * @return
   */
  public boolean initJSON() {
    // debut de la construction du JSON
    if (builderJSON == null) {
      builderJSON = new GsonBuilder();
    }
    if (gson == null) {
      gson = builderJSON.create();
    }
    
    return (builderJSON != null && gson != null);
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Retourne la signature chiffrée en MD5
   * @param account
   * @param date, au format AAAAMMJJ ou null pour la date du jour
   * @return
   */
  private String getSignature(String key, String date) {
    // Vérification des paramètres
    if ((key == null) || (key.trim().length() == 0)) {
      Trace.erreur("getSignature() La clé associée au compte utilisateur n'est pas renseignée.");
      return null;
    }
    if (date == null) {
      date = DateHeure.getJourHeure(4);
    }
    
    return Chiffrage.md5(date + key); // AAAAMMJJ + compte pricejet
  }
  
  /**
   * Retourne l'url qui permet de récupérer une liste d'article chez Pricejet
   * @param signature
   * @param account
   * @param namelist
   * @return
   */
  private URL getURL4List(String signature, String account, String namelist) {
    if ((signature == null) || (signature.trim().length() == 0)) {
      
      Trace.erreur("getURL4List() La signature est incorrecte.");
      return null;
    }
    if ((account == null) || (account.trim().length() == 0)) {
      Trace.erreur("getURL4List() Le compte utilisateur est incorrect.");
      return null;
    }
    if ((namelist == null) || (namelist.trim().length() == 0)) {
      Trace.erreur("getURL4List() Le nom de la liste est incorrecte.");
      return null;
    }
    
    StringBuilder url = new StringBuilder(URL);
    // Ajout de la signature
    url.append(SIGNATURE).append(signature).append('&');
    // Ajout du login
    url.append(LOGIN).append(account).append('&');
    // Ajout de nom de la liste
    url.append(LISTE).append(namelist);
    
    try {
      return new URL(url.toString());
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  /**
   * Retourne l'url qui permet de récupérer un tarif article chez Pricejet
   * @param signature
   * @param account
   * @param namelist
   * @return
   */
  private URL getURLDunPrix(String signature, String account, String article, String concurrent, String type) {
    if ((signature == null) || (signature.trim().length() == 0)) {
      Trace.erreur("getURL4List() La signature est incorrecte.");
      return null;
    }
    if ((account == null) || (account.trim().length() == 0)) {
      Trace.erreur("getURL4List() Le compte utilisateur est incorrect.");
      return null;
    }
    if ((article == null) || (article.trim().length() == 0)) {
      Trace.erreur("getURL4List() Le code article est incorrect.");
      return null;
    }
    
    StringBuilder url = new StringBuilder(URL);
    // Ajout de la signature
    url.append(SIGNATURE).append(signature).append('&');
    // Ajout du login
    url.append(LOGIN).append(account).append('&');
    // Ajout du code article si nécessaire
    url.append(ARTICLE).append(article);
    // Paramètres facultatifs et non cumulables : id du concurrent et type de tarif
    // VOIR API PriceJet http://www.pricejet.fr/api.php
    if (concurrent != null) {
      url.append('&');
      url.append(CONCURRENT).append(concurrent);
    }
    else if (type != null) {
      url.append('&');
      url.append(TYPE).append(type);
    }
    
    try {
      return new URL(url.toString());
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  /**
   * Connexion au site de Pricejet
   * @param url
   * @param namefile
   * @return
   */
  /* private String connexion2Pricejet(URL url) {
    String text = null;
    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
  
      for (String line; (line = reader.readLine()) != null;) {
        text = line;
  
      }
      reader.close();
  
    }
    catch (Exception e) {
      e.printStackTrace();
      Log.erreur("connexion2Pricejet() " + e.getMessage());
    }
  
    return text;
  }*/
  
  /**
   * Connexion au site de Pricejet
   * @param url
   * @param namefile
   * @return
   */
  private String connexion2PriceJet(URL url) {
    String text = "";
    try {
      // Remplace le vérifieur de nom d'hôte par un autre moins restrictif
      HostnameVerifier hostnameVerifier = new HostnameVerifier() {
        public boolean verify(String urlHostName, SSLSession session) {
          return true;
        }
      };
      HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
      
      // Remplace le vérifieur de certificat par un autre moins restrictif
      TrustManager[] trustAllCerts = new TrustManager[] {
          new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
              return null;
            }
            
            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }
            
            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }
          }
      };
      SSLContext sslContext = SSLContext.getInstance("SSL");
      sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
      
      // Chargement de la page...
      URLConnection conn = url.openConnection();
      BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      do {
        String ligne = br.readLine();
        if (ligne == null) {
          break;
        }
        text += ligne;
      }
      while (true);
      
    }
    catch (Exception ex) {
      System.err.println(ex);
    }
    
    return text;
  }
  
  /**
   * Permet de mettre à jour la liste complète des concurrents Pricejet
   */
  private ArrayList<ConcurrentPriceJet> majConcurrents() {
    ArrayList<ConcurrentPriceJet> liste = null;
    ObjetPricejet concurrents = retournerUneListeDinfos("CONCURRENT", null);
    if (concurrents != null && concurrents.getListeElements() != null) {
      liste = new ArrayList<ConcurrentPriceJet>();
      for (int i = 0; i < concurrents.getListeElements().size(); i++) {
        liste.add(new ConcurrentPriceJet(concurrents.getListeElements().get(i).getId(), concurrents.getListeElements().get(i).getNom(),
            concurrents.getListeElements().get(i).getPhoto()));
      }
    }
    
    return liste;
  }
  
  /**
   * Traitement permettant de récupérer le code erreur
   * @param line
   * @return
   */
  private boolean JSONcontientUneErreur(String line) {
    if (line == null) {
      return true;
    }
    if (line.trim().length() == 1) {
      return true;
    }
    
    if (line.startsWith(ERROR_CODE_WAIT)) {
      return true;
    }
    else if (line.startsWith(ERROR_CODE_ACCOUNT)) {
      Trace.erreur("[GVC]" + getClass().getSimpleName() + " JSONcontientUneErreur() " + ERROR_MSG_ACCOUNT);
      return true;
    }
    else if (line.startsWith(ERROR_CODE_AUTH)) {
      Trace.erreur("[GVC]" + getClass().getSimpleName() + " JSONcontientUneErreur() " + ERROR_MSG_AUTH);
      return true;
    }
    
    return false;
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Récupère la liste souhaitée d'articles ou de concurrents
   * @param namelist
   * @param date
   * @return
   */
  public ObjetPricejet retournerUneListeDinfos(String namelist, String date) {
    String signature = getSignature(KEY, date);
    URL url = getURL4List(signature, ACCOUNT, namelist);
    String retour = connexion2PriceJet(url);
    ObjetPricejet objet = null;
    
    if (!JSONcontientUneErreur(retour)) {
      retour = "{typeObjet:\"" + namelist + "\",listeElements:" + retour + "}";
      
      if (initJSON()) {
        // on récupère l'objet PriceJet après l'avoir typé
        try {
          objet = gson.fromJson(retour, ObjetPricejet.class);
        }
        catch (JsonSyntaxException e) {
          Trace.erreur(e, "[GVC] retournerUneListeDinfos() ");
          return null;
        }
      }
    }
    else {
      return null;
    }
    
    return objet;
  }
  
  /**
   * Récupère un tarif ou le nombre de concurrents
   */
  private Float retournerUneInfo(IdArticle pIdArticle, String concurrent, String type, String date) {
    String signature = getSignature(KEY, date);
    URL url = getURLDunPrix(signature, ACCOUNT, pIdArticle.getCodeArticle(), concurrent, type);
    String retour = connexion2PriceJet(url);
    
    if (!JSONcontientUneErreur(retour)) {
      try {
        return Float.valueOf(retour);
      }
      catch (Exception e) {
        return null;
      }
    }
    else {
      return null;
    }
  }
  
  /**
   * Pour un article retourner un tarif concurrent s'il est moins cher que le notre sinon NULL
   */
  public TarifPriceJet retournerTarifConcurrentMoinsCher(IdArticle pIdArticle) {
    TarifPriceJet tarifConcurrent = null;
    
    if (listeConcurrents == null) {
      listeConcurrents = majConcurrents();
    }
    
    // D'abord on vérifie le tarif Mini
    Float tarifMini = retournerUneInfo(pIdArticle, null, null, null);
    // On vérifie quel concurrent le fait moins cher
    if (tarifMini != null && tarifMini != 0) {
      try {
        tarifConcurrent = new TarifPriceJet(new BigDecimal(tarifMini.toString()));
        if (listeConcurrents != null) {
          Float tarifConc = null;
          boolean trouve = false;
          int i = 0;
          while (i < listeConcurrents.size() && !trouve) {
            tarifConc = retournerUneInfo(pIdArticle, listeConcurrents.get(i).getId(), null, null);
            trouve = (tarifConc != null && (tarifConc.compareTo(tarifMini) == 0));
            if (trouve) {
              tarifConcurrent.setConcurrent(listeConcurrents.get(i));
            }
            
            i++;
          }
          
          if (!trouve) {
            tarifConcurrent = null;
          }
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "[GVC] retournerTarifConcurrentMoinsCher");
      }
    }
    // POur signifier que cet article a déjà été scanné sur PriceJet on l'initialise malgré tout
    if (tarifConcurrent == null) {
      tarifConcurrent = new TarifPriceJet(null);
    }
    
    return tarifConcurrent;
  }
}
