/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0008d {
  // Constantes
  public static final int SIZE_POETB = 3;
  public static final int SIZE_POIDU = 9;
  public static final int DECIMAL_POIDU = 0;
  public static final int SIZE_POCRE = 7;
  public static final int DECIMAL_POCRE = 0;
  public static final int SIZE_POTRG = 1;
  public static final int SIZE_PONMOD = 1;
  public static final int SIZE_POMTT = 11;
  public static final int DECIMAL_POMTT = 2;
  public static final int SIZE_POACPT1 = 1;
  public static final int SIZE_POACPT2 = 1;
  public static final int SIZE_POIDE = 10;
  public static final int DECIMAL_POIDE = 0;
  public static final int SIZE_PORTI = 10;
  public static final int SIZE_PODOM = 24;
  public static final int SIZE_TOTALE_DS = 78;
  public static final int SIZE_FILLER = 100 - 78;
  
  // Constantes indices Nom DS
  public static final int VAR_POETB = 0;
  public static final int VAR_POIDU = 1;
  public static final int VAR_POCRE = 2;
  public static final int VAR_POTRG = 3;
  public static final int VAR_PONMOD = 4;
  public static final int VAR_POMTT = 5;
  public static final int VAR_POACPT1 = 6;
  public static final int VAR_POACPT2 = 7;
  public static final int VAR_POIDE = 8;
  public static final int VAR_PORTI = 9;
  public static final int VAR_PODOM = 10;
  
  // Variables AS400
  private String poetb = ""; // Code établissement
  private BigDecimal poidu = BigDecimal.ZERO; // Identifiant unique Règlement
  private BigDecimal pocre = BigDecimal.ZERO; // Date de création
  private String potrg = ""; // Type règlement
  private String ponmod = ""; // Si <> blanc , Non modifiable
  private BigDecimal pomtt = BigDecimal.ZERO; // Montant
  private String poacpt1 = ""; // Si = A , Acompte Pris
  private String poacpt2 = ""; // Si = C , Acompte Consommé
  private BigDecimal poide = BigDecimal.ZERO; // Ident. unique si acompte
  private String porti = ""; // Référence tirée
  private String podom = ""; // Domiciliation
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POETB), // Code établissement
      new AS400ZonedDecimal(SIZE_POIDU, DECIMAL_POIDU), // Identifiant unique Règlement
      new AS400ZonedDecimal(SIZE_POCRE, DECIMAL_POCRE), // Date de création
      new AS400Text(SIZE_POTRG), // Type règlement
      new AS400Text(SIZE_PONMOD), // Si <> blanc , Non modifiable
      new AS400ZonedDecimal(SIZE_POMTT, DECIMAL_POMTT), // Montant
      new AS400Text(SIZE_POACPT1), // Si = A , Acompte Pris
      new AS400Text(SIZE_POACPT2), // Si = C , Acompte Consommé
      new AS400ZonedDecimal(SIZE_POIDE, DECIMAL_POIDE), // Ident. unique si acompte
      new AS400Text(SIZE_PORTI), // Référence tirée
      new AS400Text(SIZE_PODOM), // Domiciliation
      new AS400Text(SIZE_FILLER), // Filler
  };
  public Object[] o = { poetb, poidu, pocre, potrg, ponmod, pomtt, poacpt1, poacpt2, poide, porti, podom, filler, };
  
  // -- Accesseurs
  
  public void setPoetb(String pPoetb) {
    if (pPoetb == null) {
      return;
    }
    poetb = pPoetb;
  }
  
  public String getPoetb() {
    return poetb;
  }
  
  public void setPoidu(BigDecimal pPoidu) {
    if (pPoidu == null) {
      return;
    }
    poidu = pPoidu.setScale(DECIMAL_POIDU, RoundingMode.HALF_UP);
  }
  
  public void setPoidu(Integer pPoidu) {
    if (pPoidu == null) {
      return;
    }
    poidu = BigDecimal.valueOf(pPoidu);
  }
  
  public Integer getPoidu() {
    return poidu.intValue();
  }
  
  public void setPocre(BigDecimal pPocre) {
    if (pPocre == null) {
      return;
    }
    pocre = pPocre.setScale(DECIMAL_POCRE, RoundingMode.HALF_UP);
  }
  
  public void setPocre(Integer pPocre) {
    if (pPocre == null) {
      return;
    }
    pocre = BigDecimal.valueOf(pPocre);
  }
  
  public void setPocre(Date pPocre) {
    if (pPocre == null) {
      return;
    }
    pocre = BigDecimal.valueOf(ConvertDate.dateToDb2(pPocre));
  }
  
  public Integer getPocre() {
    return pocre.intValue();
  }
  
  public Date getPocreConvertiEnDate() {
    return ConvertDate.db2ToDate(pocre.intValue(), null);
  }
  
  public void setPotrg(Character pPotrg) {
    if (pPotrg == null) {
      return;
    }
    potrg = String.valueOf(pPotrg);
  }
  
  public Character getPotrg() {
    return potrg.charAt(0);
  }
  
  public void setPonmod(Character pPonmod) {
    if (pPonmod == null) {
      return;
    }
    ponmod = String.valueOf(pPonmod);
  }
  
  public Character getPonmod() {
    return ponmod.charAt(0);
  }
  
  public void setPomtt(BigDecimal pPomtt) {
    if (pPomtt == null) {
      return;
    }
    pomtt = pPomtt.setScale(DECIMAL_POMTT, RoundingMode.HALF_UP);
  }
  
  public void setPomtt(Double pPomtt) {
    if (pPomtt == null) {
      return;
    }
    pomtt = BigDecimal.valueOf(pPomtt).setScale(DECIMAL_POMTT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPomtt() {
    return pomtt.setScale(DECIMAL_POMTT, RoundingMode.HALF_UP);
  }
  
  public void setPoacpt1(Character pPoacpt1) {
    if (pPoacpt1 == null) {
      return;
    }
    poacpt1 = String.valueOf(pPoacpt1);
  }
  
  public Character getPoacpt1() {
    return poacpt1.charAt(0);
  }
  
  public void setPoacpt2(Character pPoacpt2) {
    if (pPoacpt2 == null) {
      return;
    }
    poacpt2 = String.valueOf(pPoacpt2);
  }
  
  public Character getPoacpt2() {
    return poacpt2.charAt(0);
  }
  
  public void setPoide(BigDecimal pPoide) {
    if (pPoide == null) {
      return;
    }
    poide = pPoide.setScale(DECIMAL_POIDE, RoundingMode.HALF_UP);
  }
  
  public void setPoide(Integer pPoide) {
    if (pPoide == null) {
      return;
    }
    poide = BigDecimal.valueOf(pPoide);
  }
  
  public Integer getPoide() {
    return poide.intValue();
  }
  
  public void setPorti(String pPorti) {
    if (pPorti == null) {
      return;
    }
    porti = pPorti;
  }
  
  public String getPorti() {
    return porti;
  }
  
  public void setPodom(String pPodom) {
    if (pPodom == null) {
      return;
    }
    podom = pPodom;
  }
  
  public String getPodom() {
    return podom;
  }
}
