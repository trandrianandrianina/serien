/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.standard;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

public class Svgvx0017d {
  // Constantes
  public static final int SIZE_WETB = 3;
  public static final int SIZE_WART = 20;
  public static final int SIZE_WLIB = 30;
  public static final int SIZE_WOBS = 10;
  public static final int SIZE_WLB1 = 30;
  public static final int SIZE_WLB2 = 30;
  public static final int SIZE_WLB3 = 30;
  public static final int SIZE_WCL1 = 20;
  public static final int SIZE_WCL2 = 20;
  public static final int SIZE_WUNV = 2;
  public static final int SIZE_WUNS = 2;
  public static final int SIZE_WUCS = 2;
  public static final int SIZE_WUNL = 2;
  public static final int SIZE_WCND = 9;
  public static final int DECIMAL_WCND = 3;
  public static final int SIZE_WSTK = 11;
  public static final int DECIMAL_WSTK = 3;
  public static final int SIZE_WRES = 11;
  public static final int DECIMAL_WRES = 3;
  public static final int SIZE_WATT = 11;
  public static final int DECIMAL_WATT = 3;
  public static final int SIZE_WAFF = 11;
  public static final int DECIMAL_WAFF = 3;
  public static final int SIZE_WDIV = 11;
  public static final int DECIMAL_WDIV = 3;
  public static final int SIZE_WDIA = 11;
  public static final int DECIMAL_WDIA = 3;
  public static final int SIZE_WATD = 11;
  public static final int DECIMAL_WATD = 3;
  public static final int SIZE_WPRIXC = 9;
  public static final int DECIMAL_WPRIXC = 2;
  public static final int SIZE_WPRIX01 = 9;
  public static final int DECIMAL_WPRIX01 = 2;
  public static final int SIZE_WPRIXN = 9;
  public static final int DECIMAL_WPRIXN = 2;
  public static final int SIZE_WORIPR = 10;
  public static final int SIZE_WFPVN = 9;
  public static final int DECIMAL_WFPVN = 2;
  public static final int SIZE_WFUSR = 10;
  public static final int SIZE_WFDAT = 8;
  public static final int SIZE_WFHEU = 4;
  public static final int DECIMAL_WFHEU = 0;
  public static final int SIZE_WIN33 = 1;
  public static final int SIZE_WTAUTVA = 5;
  public static final int DECIMAL_WTAUTVA = 3;
  public static final int SIZE_WTTC = 1;
  public static final int SIZE_WPRIX01R = 9;
  public static final int DECIMAL_WPRIX01R = 2;
  public static final int SIZE_WDCS = 1;
  public static final int DECIMAL_WDCS = 0;
  public static final int SIZE_WPVTTC = 9;
  public static final int DECIMAL_WPVTTC = 2;
  public static final int SIZE_WTYPG = 1;
  public static final int SIZE_TOTALE_DS = 382;
  public static final int SIZE_FILLER = 500 - 382;
  
  // Constantes indices Nom DS
  public static final int VAR_WETB = 0;
  public static final int VAR_WART = 1;
  public static final int VAR_WLIB = 2;
  public static final int VAR_WOBS = 3;
  public static final int VAR_WLB1 = 4;
  public static final int VAR_WLB2 = 5;
  public static final int VAR_WLB3 = 6;
  public static final int VAR_WCL1 = 7;
  public static final int VAR_WCL2 = 8;
  public static final int VAR_WUNV = 9;
  public static final int VAR_WUNS = 10;
  public static final int VAR_WUCS = 11;
  public static final int VAR_WUNL = 12;
  public static final int VAR_WCND = 13;
  public static final int VAR_WSTK = 14;
  public static final int VAR_WRES = 15;
  public static final int VAR_WATT = 16;
  public static final int VAR_WAFF = 17;
  public static final int VAR_WDIV = 18;
  public static final int VAR_WDIA = 19;
  public static final int VAR_WATD = 20;
  public static final int VAR_WPRIXC = 21;
  public static final int VAR_WPRIX01 = 22;
  public static final int VAR_WPRIXN = 23;
  public static final int VAR_WORIPR = 24;
  public static final int VAR_WFPVN = 25;
  public static final int VAR_WFUSR = 26;
  public static final int VAR_WFDAT = 27;
  public static final int VAR_WFHEU = 28;
  public static final int VAR_WIN33 = 29;
  public static final int VAR_WTAUTVA = 30;
  public static final int VAR_WTTC = 31;
  public static final int VAR_WPRIX01R = 32;
  public static final int VAR_WDCS = 33;
  public static final int VAR_WPVTTC = 34;
  public static final int VAR_WTYPG = 35;
  
  // Variables AS400
  private String wetb = ""; //
  private String wart = ""; // Code ARTICLE
  private String wlib = ""; // Libellé 1
  private String wobs = ""; // Observation
  private String wlb1 = ""; // Libellé 2
  private String wlb2 = ""; // Libellé 3
  private String wlb3 = ""; // Libellé 4
  private String wcl1 = ""; // Clé de classement 1
  private String wcl2 = ""; // Clé de classement 2
  private String wunv = ""; // Code unité de vente
  private String wuns = ""; // Code unité de stockage
  private String wucs = ""; // Unité de conditionnement Stk
  private String wunl = ""; // Unité de conditionnement Vte
  private BigDecimal wcnd = BigDecimal.ZERO; // Conditionnement
  private BigDecimal wstk = BigDecimal.ZERO; // Quantité stock
  private BigDecimal wres = BigDecimal.ZERO; // Quantité réservée
  private BigDecimal watt = BigDecimal.ZERO; // Quantité attendue
  private BigDecimal waff = BigDecimal.ZERO; // Quantité affectée
  private BigDecimal wdiv = BigDecimal.ZERO; // Quantité disponible Vente
  private BigDecimal wdia = BigDecimal.ZERO; // Quantité disponible Achat
  private BigDecimal watd = BigDecimal.ZERO; // ??
  private BigDecimal wprixc = BigDecimal.ZERO; // Prix client hors forçage
  private BigDecimal wprix01 = BigDecimal.ZERO; // Prix public colonne 1
  private BigDecimal wprixn = BigDecimal.ZERO; // Prix colonne n forcée
  private String woripr = ""; // Origine prix
  private BigDecimal wfpvn = BigDecimal.ZERO; // Prix de vente flash
  private String wfusr = ""; // Code utilisateur
  private String wfdat = ""; // Date prix flash
  private BigDecimal wfheu = BigDecimal.ZERO; // Heure
  private String win33 = ""; // Affichage Obligat. comptoir
  private BigDecimal wtautva = BigDecimal.ZERO; // Taux de TVA
  private String wttc = ""; // Top TTC
  private BigDecimal wprix01r = BigDecimal.ZERO; // Prix public remise forcée
  private BigDecimal wdcs = BigDecimal.ZERO; // Décimalisation un. de stock
  private BigDecimal wpvttc = BigDecimal.ZERO; // Prix de vente TTC provenant d"une CNV
  private String wtypg = ""; // Tupe de gratuit
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WETB), //
      new AS400Text(SIZE_WART), // Code ARTICLE
      new AS400Text(SIZE_WLIB), // Libellé 1
      new AS400Text(SIZE_WOBS), // Observation
      new AS400Text(SIZE_WLB1), // Libellé 2
      new AS400Text(SIZE_WLB2), // Libellé 3
      new AS400Text(SIZE_WLB3), // Libellé 4
      new AS400Text(SIZE_WCL1), // Clé de classement 1
      new AS400Text(SIZE_WCL2), // Clé de classement 2
      new AS400Text(SIZE_WUNV), // Code unité de vente
      new AS400Text(SIZE_WUNS), // Code unité de stockage
      new AS400Text(SIZE_WUCS), // Unité de conditionnement Stk
      new AS400Text(SIZE_WUNL), // Unité de conditionnement Vte
      new AS400ZonedDecimal(SIZE_WCND, DECIMAL_WCND), // Conditionnement
      new AS400ZonedDecimal(SIZE_WSTK, DECIMAL_WSTK), // Quantité stock
      new AS400ZonedDecimal(SIZE_WRES, DECIMAL_WRES), // Quantité réservée
      new AS400ZonedDecimal(SIZE_WATT, DECIMAL_WATT), // Quantité attendue
      new AS400ZonedDecimal(SIZE_WAFF, DECIMAL_WAFF), // Quantité affectée
      new AS400ZonedDecimal(SIZE_WDIV, DECIMAL_WDIV), // Quantité disponible Vente
      new AS400ZonedDecimal(SIZE_WDIA, DECIMAL_WDIA), // Quantité disponible Achat
      new AS400ZonedDecimal(SIZE_WATD, DECIMAL_WATD), // ??
      new AS400ZonedDecimal(SIZE_WPRIXC, DECIMAL_WPRIXC), // Prix client hors forçage
      new AS400ZonedDecimal(SIZE_WPRIX01, DECIMAL_WPRIX01), // Prix public colonne 1
      new AS400ZonedDecimal(SIZE_WPRIXN, DECIMAL_WPRIXN), // Prix colonne n forcée
      new AS400Text(SIZE_WORIPR), // Origine prix
      new AS400ZonedDecimal(SIZE_WFPVN, DECIMAL_WFPVN), // Prix de vente flash
      new AS400Text(SIZE_WFUSR), // Code utilisateur
      new AS400Text(SIZE_WFDAT), // Date prix flash
      new AS400ZonedDecimal(SIZE_WFHEU, DECIMAL_WFHEU), // Heure
      new AS400Text(SIZE_WIN33), // Affichage Obligat. comptoir
      new AS400ZonedDecimal(SIZE_WTAUTVA, DECIMAL_WTAUTVA), // Taux de TVA
      new AS400Text(SIZE_WTTC), // Top TTC
      new AS400ZonedDecimal(SIZE_WPRIX01R, DECIMAL_WPRIX01R), // Prix public remise forcée
      new AS400ZonedDecimal(SIZE_WDCS, DECIMAL_WDCS), // Décimalisation un. de stock
      new AS400ZonedDecimal(SIZE_WPVTTC, DECIMAL_WPVTTC), // Prix de vente TTC provenant d"une CNV
      new AS400Text(SIZE_WTYPG), // Tupe de gratuit
      new AS400Text(SIZE_FILLER), // Filler
  };
  public Object[] o =
      { wetb, wart, wlib, wobs, wlb1, wlb2, wlb3, wcl1, wcl2, wunv, wuns, wucs, wunl, wcnd, wstk, wres, watt, waff, wdiv, wdia, watd,
          wprixc, wprix01, wprixn, woripr, wfpvn, wfusr, wfdat, wfheu, win33, wtautva, wttc, wprix01r, wdcs, wpvttc, wtypg, filler, };
  
  // -- Accesseurs
  
  public void setWetb(String pWetb) {
    if (pWetb == null) {
      return;
    }
    wetb = pWetb;
  }
  
  public String getWetb() {
    return wetb;
  }
  
  public void setWart(String pWart) {
    if (pWart == null) {
      return;
    }
    wart = pWart;
  }
  
  public String getWart() {
    return wart;
  }
  
  public void setWlib(String pWlib) {
    if (pWlib == null) {
      return;
    }
    wlib = pWlib;
  }
  
  public String getWlib() {
    return wlib;
  }
  
  public void setWobs(String pWobs) {
    if (pWobs == null) {
      return;
    }
    wobs = pWobs;
  }
  
  public String getWobs() {
    return wobs;
  }
  
  public void setWlb1(String pWlb1) {
    if (pWlb1 == null) {
      return;
    }
    wlb1 = pWlb1;
  }
  
  public String getWlb1() {
    return wlb1;
  }
  
  public void setWlb2(String pWlb2) {
    if (pWlb2 == null) {
      return;
    }
    wlb2 = pWlb2;
  }
  
  public String getWlb2() {
    return wlb2;
  }
  
  public void setWlb3(String pWlb3) {
    if (pWlb3 == null) {
      return;
    }
    wlb3 = pWlb3;
  }
  
  public String getWlb3() {
    return wlb3;
  }
  
  public void setWcl1(String pWcl1) {
    if (pWcl1 == null) {
      return;
    }
    wcl1 = pWcl1;
  }
  
  public String getWcl1() {
    return wcl1;
  }
  
  public void setWcl2(String pWcl2) {
    if (pWcl2 == null) {
      return;
    }
    wcl2 = pWcl2;
  }
  
  public String getWcl2() {
    return wcl2;
  }
  
  public void setWunv(String pWunv) {
    if (pWunv == null) {
      return;
    }
    wunv = pWunv;
  }
  
  public String getWunv() {
    return wunv;
  }
  
  public void setWuns(String pWuns) {
    if (pWuns == null) {
      return;
    }
    wuns = pWuns;
  }
  
  public String getWuns() {
    return wuns;
  }
  
  public void setWucs(String pWucs) {
    if (pWucs == null) {
      return;
    }
    wucs = pWucs;
  }
  
  public String getWucs() {
    return wucs;
  }
  
  public void setWunl(String pWunl) {
    if (pWunl == null) {
      return;
    }
    wunl = pWunl;
  }
  
  public String getWunl() {
    return wunl;
  }
  
  public void setWcnd(BigDecimal pWcnd) {
    if (pWcnd == null) {
      return;
    }
    wcnd = pWcnd.setScale(DECIMAL_WCND, RoundingMode.HALF_UP);
  }
  
  public void setWcnd(Double pWcnd) {
    if (pWcnd == null) {
      return;
    }
    wcnd = BigDecimal.valueOf(pWcnd).setScale(DECIMAL_WCND, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWcnd() {
    return wcnd.setScale(DECIMAL_WCND, RoundingMode.HALF_UP);
  }
  
  public void setWstk(BigDecimal pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = pWstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWstk(Double pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = BigDecimal.valueOf(pWstk).setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWstk() {
    return wstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWres(BigDecimal pWres) {
    if (pWres == null) {
      return;
    }
    wres = pWres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWres(Double pWres) {
    if (pWres == null) {
      return;
    }
    wres = BigDecimal.valueOf(pWres).setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWres() {
    return wres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWatt(BigDecimal pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = pWatt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWatt(Double pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = BigDecimal.valueOf(pWatt).setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWatt() {
    return watt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWaff(BigDecimal pWaff) {
    if (pWaff == null) {
      return;
    }
    waff = pWaff.setScale(DECIMAL_WAFF, RoundingMode.HALF_UP);
  }
  
  public void setWaff(Double pWaff) {
    if (pWaff == null) {
      return;
    }
    waff = BigDecimal.valueOf(pWaff).setScale(DECIMAL_WAFF, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWaff() {
    return waff.setScale(DECIMAL_WAFF, RoundingMode.HALF_UP);
  }
  
  public void setWdiv(BigDecimal pWdiv) {
    if (pWdiv == null) {
      return;
    }
    wdiv = pWdiv.setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public void setWdiv(Double pWdiv) {
    if (pWdiv == null) {
      return;
    }
    wdiv = BigDecimal.valueOf(pWdiv).setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWdiv() {
    return wdiv.setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public void setWdia(BigDecimal pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = pWdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWdia(Double pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = BigDecimal.valueOf(pWdia).setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWdia() {
    return wdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWatd(BigDecimal pWatd) {
    if (pWatd == null) {
      return;
    }
    watd = pWatd.setScale(DECIMAL_WATD, RoundingMode.HALF_UP);
  }
  
  public void setWatd(Double pWatd) {
    if (pWatd == null) {
      return;
    }
    watd = BigDecimal.valueOf(pWatd).setScale(DECIMAL_WATD, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWatd() {
    return watd.setScale(DECIMAL_WATD, RoundingMode.HALF_UP);
  }
  
  public void setWprixc(BigDecimal pWprixc) {
    if (pWprixc == null) {
      return;
    }
    wprixc = pWprixc.setScale(DECIMAL_WPRIXC, RoundingMode.HALF_UP);
  }
  
  public void setWprixc(Double pWprixc) {
    if (pWprixc == null) {
      return;
    }
    wprixc = BigDecimal.valueOf(pWprixc).setScale(DECIMAL_WPRIXC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprixc() {
    return wprixc.setScale(DECIMAL_WPRIXC, RoundingMode.HALF_UP);
  }
  
  public void setWprix01(BigDecimal pWprix01) {
    if (pWprix01 == null) {
      return;
    }
    wprix01 = pWprix01.setScale(DECIMAL_WPRIX01, RoundingMode.HALF_UP);
  }
  
  public void setWprix01(Double pWprix01) {
    if (pWprix01 == null) {
      return;
    }
    wprix01 = BigDecimal.valueOf(pWprix01).setScale(DECIMAL_WPRIX01, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprix01() {
    return wprix01.setScale(DECIMAL_WPRIX01, RoundingMode.HALF_UP);
  }
  
  public void setWprixn(BigDecimal pWprixn) {
    if (pWprixn == null) {
      return;
    }
    wprixn = pWprixn.setScale(DECIMAL_WPRIXN, RoundingMode.HALF_UP);
  }
  
  public void setWprixn(Double pWprixn) {
    if (pWprixn == null) {
      return;
    }
    wprixn = BigDecimal.valueOf(pWprixn).setScale(DECIMAL_WPRIXN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprixn() {
    return wprixn.setScale(DECIMAL_WPRIXN, RoundingMode.HALF_UP);
  }
  
  public void setWoripr(String pWoripr) {
    if (pWoripr == null) {
      return;
    }
    woripr = pWoripr;
  }
  
  public String getWoripr() {
    return woripr;
  }
  
  public void setWfpvn(BigDecimal pWfpvn) {
    if (pWfpvn == null) {
      return;
    }
    wfpvn = pWfpvn.setScale(DECIMAL_WFPVN, RoundingMode.HALF_UP);
  }
  
  public void setWfpvn(Double pWfpvn) {
    if (pWfpvn == null) {
      return;
    }
    wfpvn = BigDecimal.valueOf(pWfpvn).setScale(DECIMAL_WFPVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfpvn() {
    return wfpvn.setScale(DECIMAL_WFPVN, RoundingMode.HALF_UP);
  }
  
  public void setWfusr(String pWfusr) {
    if (pWfusr == null) {
      return;
    }
    wfusr = pWfusr;
  }
  
  public String getWfusr() {
    return wfusr;
  }
  
  public void setWfdat(String pWfdat) {
    if (pWfdat == null) {
      return;
    }
    wfdat = pWfdat;
  }
  
  public String getWfdat() {
    return wfdat;
  }
  
  public void setWfheu(BigDecimal pWfheu) {
    if (pWfheu == null) {
      return;
    }
    wfheu = pWfheu.setScale(DECIMAL_WFHEU, RoundingMode.HALF_UP);
  }
  
  public void setWfheu(Integer pWfheu) {
    if (pWfheu == null) {
      return;
    }
    wfheu = BigDecimal.valueOf(pWfheu);
  }
  
  public Integer getWfheu() {
    return wfheu.intValue();
  }
  
  public void setWin33(Character pWin33) {
    if (pWin33 == null) {
      return;
    }
    win33 = String.valueOf(pWin33);
  }
  
  public Character getWin33() {
    return win33.charAt(0);
  }
  
  public void setWtautva(BigDecimal pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = pWtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setWtautva(Double pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = BigDecimal.valueOf(pWtautva).setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWtautva() {
    return wtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setWttc(Character pWttc) {
    if (pWttc == null) {
      return;
    }
    wttc = String.valueOf(pWttc);
  }
  
  public Character getWttc() {
    return wttc.charAt(0);
  }
  
  public void setWprix01r(BigDecimal pWprix01r) {
    if (pWprix01r == null) {
      return;
    }
    wprix01r = pWprix01r.setScale(DECIMAL_WPRIX01R, RoundingMode.HALF_UP);
  }
  
  public void setWprix01r(Double pWprix01r) {
    if (pWprix01r == null) {
      return;
    }
    wprix01r = BigDecimal.valueOf(pWprix01r).setScale(DECIMAL_WPRIX01R, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprix01r() {
    return wprix01r.setScale(DECIMAL_WPRIX01R, RoundingMode.HALF_UP);
  }
  
  public void setWdcs(BigDecimal pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = pWdcs.setScale(DECIMAL_WDCS, RoundingMode.HALF_UP);
  }
  
  public void setWdcs(Integer pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = BigDecimal.valueOf(pWdcs);
  }
  
  public Integer getWdcs() {
    return wdcs.intValue();
  }
  
  public void setWpvttc(BigDecimal pWpvttc) {
    if (pWpvttc == null) {
      return;
    }
    wpvttc = pWpvttc.setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpvttc(Double pWpvttc) {
    if (pWpvttc == null) {
      return;
    }
    wpvttc = BigDecimal.valueOf(pWpvttc).setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpvttc() {
    return wpvttc.setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public void setWtypg(Character pWtypg) {
    if (pWtypg == null) {
      return;
    }
    wtypg = String.valueOf(pWtypg);
  }
  
  public Character getWtypg() {
    return wtypg.charAt(0);
  }
}
