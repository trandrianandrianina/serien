/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.documentvente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.IdModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatBonDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatDevisDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatExtractionDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Regrouper les accès SQL en relation avec les documents de ventes.
 */
public class SqlDocumentVente {
  private static final int NOMBRE_MAX_IDENTIFIANT = 40;
  private QueryManager querymg = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pQueryManager Objet gestion de requêtes.
   */
  public SqlDocumentVente(QueryManager pQueryManager) {
    querymg = pQueryManager;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Charger les identifiants des documents de ventes correspondants aux critères de recherche.<br>
   * La requête SQL est construite en fusionnant les quatres requêtes permettant de rechercher les quatres types de documents.<br>
   * Cela permet de faire un tri impliquant les quatres types de document mélangés. La date est le seul critère de tri commun aux quatres
   * types de documents. Il a été choisit :<br>
   * - devis : date de création<br>
   * - commande : date de création<br>
   * - bon : date de création<br>
   * - facture : date de facturation (que l'on peut considérer comme étant la date de création)
   * @param pCritereDocumentVente Critère de recherche de documents de ventes.
   * @return Liste d'identifiant de documents de ventes.
   */
  public List<IdDocumentVente> chargerListeIdDocumentVente(CritereDocumentVente pCritereDocumentVente) {
    String requete = "";
    
    // Analyse critères
    boolean ajouterTableExtraction = false;
    // Ajout de la table d'extractions en jointure pour connaître l'état d'extraction
    if (pCritereDocumentVente.isExtractionPartielle()) {
      ajouterTableExtraction = true;
    }
    
    // Lire le type de documents de ventes souhaités
    EnumTypeDocumentVente typeDocumentVente = pCritereDocumentVente.getTypeDocumentVente();
    
    if (typeDocumentVente == null || typeDocumentVente.equals(EnumTypeDocumentVente.NON_DEFINI)) {
      requete += " select distinct doc.E1ETB, doc.E1COD, doc.E1NUM, doc.E1SUF, doc.E1NFA, doc.E1CRE AS DATE";
      requete += " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc "
          + pCritereDocumentVente.getRequeteFrom(querymg.getLibrary());
      requete += " where " + pCritereDocumentVente.getRequeteWhere(querymg.getLibrary());
    }
    
    // Ajouter la requête pour les devis
    else if (typeDocumentVente.equals(EnumTypeDocumentVente.DEVIS)) {
      requete += " select distinct doc.E1ETB, doc.E1COD, doc.E1NUM, doc.E1SUF, 0 AS E1NFA, doc.E1CRE AS DATE";
      requete += " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc "
          + pCritereDocumentVente.getRequeteFrom(querymg.getLibrary());
      if (ajouterTableExtraction) {
        requete += ajouterJointureTableExtraction();
      }
      requete += " where " + pCritereDocumentVente.getRequeteWherePourDevis(querymg.getLibrary());
    }
    
    // Ajouter la requête pour les commandes
    else if (typeDocumentVente.equals(EnumTypeDocumentVente.COMMANDE)) {
      if (!requete.isEmpty()) {
        requete += " union all";
      }
      requete += " select distinct doc.E1ETB, doc.E1COD, doc.E1NUM, doc.E1SUF, 0 AS E1NFA, doc.E1CRE AS DATE";
      requete += " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc "
          + pCritereDocumentVente.getRequeteFrom(querymg.getLibrary());
      if (ajouterTableExtraction) {
        requete += ajouterJointureTableExtraction();
      }
      requete += " where " + pCritereDocumentVente.getRequeteWherePourCommande(querymg.getLibrary());
    }
    
    // Ajouter la requête pour les bons
    else if (typeDocumentVente.equals(EnumTypeDocumentVente.BON)) {
      if (!requete.isEmpty()) {
        requete += " union all";
      }
      requete += " select distinct doc.E1ETB, doc.E1COD, doc.E1NUM, doc.E1SUF, 0 AS E1NFA, doc.E1CRE AS DATE";
      requete += " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc "
          + pCritereDocumentVente.getRequeteFrom(querymg.getLibrary());
      requete += " where " + pCritereDocumentVente.getRequeteWherePourBon(querymg.getLibrary());
    }
    
    // Ajouter la requête pour les factures
    else if (typeDocumentVente.equals(EnumTypeDocumentVente.FACTURE)) {
      if (!requete.isEmpty()) {
        requete += " union all";
      }
      requete += " select distinct doc.E1ETB, ' '  as E1COD, 0 AS E1NUM, 0 AS E1SUF, doc.E1NFA, doc.E1FAC AS DATE";
      requete += " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc "
          + pCritereDocumentVente.getRequeteFrom(querymg.getLibrary());
      requete += " where " + pCritereDocumentVente.getRequeteWherePourFacture(querymg.getLibrary());
    }
    
    // Ordonner le résultat par ordre décroissante si c'est un composant de plage.
    if (pCritereDocumentVente.isComposantDePlage()) {
      requete += pCritereDocumentVente.getRequeteOrderByPourComposantPlage();
    }
    else {
      // Ajouter l'ordre ORDER BY
      requete += " order by E1COD, E1ETB, DATE DESC, E1NUM DESC, E1SUF DESC, E1NFA DESC";
    }
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requete);
    
    // Préparer le résultat
    ArrayList<IdDocumentVente> listeIdDocumentVente = new ArrayList<IdDocumentVente>();
    for (GenericRecord genericRecord : listeGenericRecord) {
      IdDocumentVente idDocumentVente = initialiserIdDocumentVente(genericRecord);
      // Suppression des identifiants en doublon (factures regroupées)
      if (!listeIdDocumentVente.contains(idDocumentVente)) {
        listeIdDocumentVente.add(idDocumentVente);
      }
    }
    
    return listeIdDocumentVente;
  }
  
  /**
   * Charger la liste de documents de ventes de base correspondant aux identifiants.
   * @param pListeIdDocumentVente Liste d'identifiants de documents de ventes.
   * @return La liste de documents de ventes de base.
   */
  public ListeDocumentVenteBase chargerListeDocumentVenteBase(List<IdDocumentVente> pListeIdDocumentVente) {
    ListeDocumentVenteBase listeDocumentVenteBase = new ListeDocumentVenteBase();
    int index = 0;
    while (index < pListeIdDocumentVente.size()) {
      index = chargerListeDocumentVenteBase(listeDocumentVenteBase, pListeIdDocumentVente, index);
    }
    chargerListeFactureBase(listeDocumentVenteBase, pListeIdDocumentVente);
    return listeDocumentVenteBase;
  }
  
  /**
   * Retourner la liste des identifiants des documents (avoir) pour un document donné.
   * @param pIdDocument Identifiant d'un document.
   * @return La liste d'identifiant de document de ventes (avoir).
   */
  public List<IdDocumentVente> chargerListeIdDocumentAvoir(IdDocumentVente pIdDocument) {
    if (pIdDocument == null) {
      throw new MessageErreurException("L'id du document est invalide.");
    }
    
    // Préparation de la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter("select distinct e1etb, substr(IXREF2, 1, 1) as e1cod, int(substr(IXREF2, 2, 6)) as e1num, "
        + "int(substr(IXREF2, 8, 1)) as e1suf, e1nfa from "
        + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + ", "
        + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_INDEX + " where ");
    // @formatter:on
    requeteSql.ajouterConditionAnd("ixetb", "=", pIdDocument.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("ixtyp", "=", "la");
    requeteSql.ajouterConditionAnd("e1etb", "=", "ixetb", true, false);
    // Dans le cas où l'ID est celui d'une facture
    if (pIdDocument.isIdFacture()) {
      requeteSql.ajouterConditionAnd("e1nfa", "=", pIdDocument.getNumeroFacture());
      requeteSql.ajouterConditionAnd("e1cod", "=", "substr(IXREF1, 1, 1)", true, false);
      requeteSql.ajouterConditionAnd("e1num", "=", "substr(IXREF1, 2, 6)", true, false);
      requeteSql.ajouterConditionAnd("e1suf", "=", "substr(IXREF1, 8, 1)", true, false);
    }
    // Dans le cas où l'ID n'est pas celui d'une facture
    else {
      requeteSql.ajouterConditionAnd("ixref1", "like",
          pIdDocument.getIndicatif(IdDocumentVente.INDICATIF_ENTETE_NUM_SUF).toUpperCase() + '%');
      requeteSql.ajouterConditionAnd("e1cod", "=", "substr(IXREF1, 1, 1)", true, false);
      requeteSql.ajouterConditionAnd("e1num", "=", "substr(IXREF1, 2, 6)", true, false);
      requeteSql.ajouterConditionAnd("e1suf", "=", "substr(IXREF1, 8, 1)", true, false);
    }
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequete());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Création de la liste des id avec les données trouvées
    List<IdDocumentVente> listeIdDocumentVente = new ArrayList<IdDocumentVente>();
    for (GenericRecord record : listeGenericRecord) {
      listeIdDocumentVente.add(initialiserIdDocumentVenteAvoir(record));
    }
    return listeIdDocumentVente;
  }
  
  /**
   * Charger la liste des identfiants des factures non réglées.<br>
   * La requête est la même que celle qui est utilisée dans le programme RPG afin d'avoir les mêmes résultats.
   * C'est pour cette raison que la méthode chargerListeIdDocumentVente n'est pas utilisée.
   * @param pCritereDocumentVente Critère de recherche de document de ventes.
   * @return La liste d'identifiants de factures non réglées.
   */
  public List<IdDocumentVente> chargerListeIdFactureNonReglee(CritereDocumentVente pCritereDocumentVente) {
    if (pCritereDocumentVente == null) {
      throw new MessageErreurException("Les critères pour la recherche des documents de ventes est invalide.");
    }
    if (pCritereDocumentVente.getIdEtablissement() == null) {
      throw new MessageErreurException("L'identifiant de l'établissement est obligatoire.");
    }
    if (pCritereDocumentVente.getIdClient() == null) {
      throw new MessageErreurException("L'identifiant du client est obligatoire.");
    }
    
    // Création de la sous requête
    RequeteSql sousRequeteSql = new RequeteSql();
    sousRequeteSql.ajouter("Select 1 from ");
    sousRequeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_INDEX + " where ");
    sousRequeteSql.ajouterConditionAnd("IXTYP", "=", "cf");
    sousRequeteSql.ajouterConditionAnd("DECIMAL(substr(IXREF1,1,6),7,0)", "=", "E1CLFP", true, false);
    sousRequeteSql.ajouterConditionAnd("DECIMAL(substr(IXREF2,1,7),7,0)", "=", "E1NFA", true, false);
    
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("Select ' ' as E1COD, 0 AS E1NUM, 0 AS E1SUF, E1NFA, E1ETB from ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " where");
    requeteSql.ajouterConditionAnd("E1ETB", "=", pCritereDocumentVente.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("E1COD", "=", EnumCodeEnteteDocumentVente.COMMANDE_OU_BON.getCode());
    requeteSql.ajouterConditionAnd("E1ETA", "=", EnumEtatBonDocumentVente.COMPTABILISE.getCode().intValue());
    requeteSql.ajouter(" and (E1CLLP = " + pCritereDocumentVente.getIdClient().getNumero() + " Or E1CLFP = "
        + pCritereDocumentVente.getIdClient().getNumero() + ")");
    requeteSql.ajouterConditionAnd("E1IN18", "<>", 'I');
    requeteSql.ajouterConditionAnd("E1TOP", "=", 0);
    requeteSql.ajouterConditionAnd("E1TTC", "<>", 0);
    requeteSql.ajouter(" and Exists (" + sousRequeteSql.getRequete());
    requeteSql.ajouter(") order By E1NUM");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Création de la liste des id avec les données trouvées
    List<IdDocumentVente> listeIdDocumentVente = new ArrayList<IdDocumentVente>();
    for (GenericRecord record : listeGenericRecord) {
      listeIdDocumentVente.add(initialiserIdDocumentVente(record));
    }
    return listeIdDocumentVente;
  }
  
  /**
   * Charger les données de base d'un document de ventes de type avoir (facture négative) sur la base des identifiants de documents
   * transmis.
   * @param pListeIdDocumentVente Liste d'identifiant de documents de ventes.
   * @return La liste de documents de ventes de base.
   */
  public ListeDocumentVenteBase chargerListeAvoir(List<IdDocumentVente> pListeIdDocumentVente) {
    ListeDocumentVenteBase listeAvoir = new ListeDocumentVenteBase();
    // Construire la requête
    // @formatter:off
    String requete = ""
        + " select"
        + "   doc.E1ETB,"
        + "   doc.E1NFA,"
        + "   doc.E1COD,"
        + "   doc.E1NUM,"
        + "   doc.E1SUF,"
        + "   doc.E1MAG,"
        + "   doc.E1CRE,"
        + "   doc.E1DAT2,"
        + "   doc.E1HOM,"
        + "   doc.E1EXP,"
        + "   doc.E1FAC,"
        + "   doc.E1SL1,"
        + "   doc.E1SL2,"
        + "   doc.E1SL3,"
        + "   doc.E1SL1,"
        + "   doc.E1SL2,"
        + "   doc.E1SL3,"
        + "   doc.E1RCC,"
        + "   doc.E1VDE,"
        + "   doc.E1ETA,"
        + "   doc.E1MEX,"
        + "   doc.E1CLFP,"
        + "   doc.E1CLFS,"
        + "   doc.E1DCO,"
        + "   doc.E1TTC,"
        + "   doc.E1GBA,"
        + "   doc.E1IN18,"
        + "   '0' as EXTRACT"
        + " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc"
        + " where"
        + "   doc.E1ETB = " + RequeteSql.formaterStringSQL(pListeIdDocumentVente.get(0).getCodeEtablissement())
        + "   and ("
        // Soit on recherche les entêtes de factures regroupées (X)
        + "     doc.E1COD = 'X'"
        // Soit on recherche les entêtes de factures standards (E), une facture standard a IXCODG = 0 dans PGVMIDXM
        + "     or (doc.E1COD = 'E'"
        + "         and exists(select 1 from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_INDEX
        + "                    where"
        + "                    IXTYP = 'FB'"
        + "                    and IXETB = doc.E1ETB"
        + "                    and IXCODG = 0)))";
    // @formatter:on
    
    // Ajouter les identifiants des documents de ventes à charger à la requête
    int index = 0;
    for (IdDocumentVente idDocumentVente : pListeIdDocumentVente) {
      if (index == 0) {
        requete += " and (";
      }
      else {
        requete += " or ";
      }
      if (idDocumentVente.isIdFacture()) {
        requete += " (doc.E1NUM = " + idDocumentVente.getNumeroFacture() + ") ";
      }
      else {
        requete += " (doc.E1NUM = " + idDocumentVente.getNumero() + ") ";
      }
      index++;
    }
    requete += " )";
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requete);
    
    // Construire le résultat
    for (GenericRecord record : listeGenericRecord) {
      DocumentVenteBase documentVenteBase = initialiserDocumentVenteBase(record);
      listeAvoir.add(documentVenteBase);
    }
    
    return listeAvoir;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialiser un identifiant de document de ventes à partir d'un GenericRecord.
   * @param pGenericRecord Objet enregistrement générique.
   * @return L'identifiant du document de ventes.
   */
  private IdDocumentVente initialiserIdDocumentVente(GenericRecord pGenericRecord) {
    // Lire l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("E1ETB"));
    
    // Lire le code entête
    EnumCodeEnteteDocumentVente codeEntete = null;
    Character e1cod = pGenericRecord.getCharacterValue("E1COD");
    if (e1cod != null && !e1cod.equals(' ')) {
      codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(e1cod);
    }
    
    // Lire le numéro (celui-ci est égal à 0 pour les factures)
    Integer numero = pGenericRecord.getIntegerValue("E1NUM");
    
    // Lire le suffixe (celui-ci est égal à 0 pour les factures)
    Integer suffixe = pGenericRecord.getIntegerValue("E1SUF");
    
    // Lire le numéro de factures (celui-ci est égal à 0 pour les devis, commandes et bons)
    Integer numeroFacture = pGenericRecord.getIntegerValue("E1NFA");
    
    // Créer l'identifiant du document de ventes
    return IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, numero, suffixe, numeroFacture);
  }
  
  /**
   * Charger toutes les données de base d'un document de ventes (commande, devis , bons) sur la base des identifiants transmis.
   * @param pListeDocumentVenteBase Liste de documents de ventes à compléter.
   * @param pListeIdDocumentVente Liste d'identifiant de document de ventes à charger.
   * @param pIndex Index de début.
   * @return Le nombre de chargements faits.
   */
  private int chargerListeDocumentVenteBase(ListeDocumentVenteBase pListeDocumentVenteBase, List<IdDocumentVente> pListeIdDocumentVente,
      int pIndex) {
    // Construire la requête
    // @formatter:off
    String requete = ""
        + " SELECT"
        + "   doc.E1ETB,"
        + "   doc.E1COD,"
        + "   doc.E1NUM,"
        + "   doc.E1SUF,"
        + "   doc.E1NFA,"
        + "   doc.E1MAG,"
        + "   doc.E1CRE,"
        + "   doc.E1DAT2,"
        + "   doc.E1HOM,"
        + "   doc.E1EXP,"
        + "   doc.E1TOP,"
        // Récupération de l'état d'extraction du document en cours
        + "   case doc.E1COD when 'D' then doc.E1TDV"
        + "   else ifnull((substr(digits(ext.IXCODG), 1, 1)), '0')"
        + "   end as EXTRACT_CRT,"
        // Récupération de l'état d'extraction du document archivé
        + "   case doc2.E1COD when 'd' then doc2.E1TDV"
        + "   else ifnull((substr(digits(ext2.IXCODG), 1, 1)), '0')"
        + "   end as EXTRACT_ARCHIVE,"
        // Le montant HT du document courant
        + "   doc.E1SL1 as E1SL1_A,"
        + "   doc.E1SL2 as E1SL2_A,"
        + "   doc.E1SL3 as E1SL3_A,"
        // Le montant HT est lu en priorité dans l'archive (si l'état est différent entre le document courant et le document archivé)
        + "   coalesce(doc2.E1SL1, doc.E1SL1) AS E1SL1_B,"
        + "   coalesce(doc2.E1SL2, doc.E1SL2) AS E1SL2_B,"
        + "   coalesce(doc2.E1SL3, doc.E1SL3) AS E1SL3_B,"
        + "   doc.E1RCC,"
        + "   doc.E1VDE,"
        + "   doc.E1ETA,"
        + "   doc.E1MEX,"
        + "   doc.E1CLFP,"
        + "   doc.E1CLFS,"
        + "   doc.E1DCO,"
        // Le montant TTC lu du document courant
        + "   doc.E1TTC AS E1TTC_A,"
        // Le montant TTC est lu en priorité dans l'archive (si l'état est différent entre le document courant et le document archivé)
        + "   coalesce(doc2.E1TTC, doc.E1TTC) AS E1TTC_B,"
        + "   doc.E1GBA,"
        + "   doc.E1IN18,"
        + "   doc.E1TDV,"
        + "   ifnull((substr(digits(ext.IXCODG), 1, 1)), '0') as EXTRACT,"
        + "   xli.XILIB"
        + " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc"
        // Jointure avec la table d'extractions pour connaître l'état d'extraction
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_INDEX + " ext on"
        + "   doc.E1ETB = ext.IXETB"
        + "   and doc.E1COD = substring(ext.IXREF1, 1, 1)"
        + "   and doc.E1NUM = substring(ext.IXREF1, 2, 6)"
        + "   and doc.E1SUF = substring(ext.IXREF1, 8, 1)"
        + "   and ext.IXTYP = 'lx'"
        // Jointure avec le fichier des extentions pour obtenir l'identifiant du chantier
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_EXTENSION + " xli on"
        + "   xli.XIETB = doc.E1ETB"
        + "   and xli.XICOD = doc.E1COD"
        + "   and xli.XINUM = doc.E1NUM"
        + "   and xli.XISUF = doc.E1SUF"
        + "   and xli.XITYP = '40'"
        // Jointure avec l'éventuel document de ventes archivé
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc2 on"
        + "   doc2.E1ETB = doc.E1ETB"
        + "   and doc2.E1COD = lower(doc.E1COD)"
        + "   and doc2.E1NUM = doc.E1NUM"
        + "   and doc2.E1SUF = doc.E1SUF"
        // Jointure avec la table d'extractions pour connaître l'état d'extraction de l'archive
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_INDEX + " ext2 on"
        + "   doc2.E1ETB = ext2.IXETB"
        + "   and doc2.E1COD = lower(substring(ext2.IXREF1, 1, 1))"
        + "   and doc2.E1NUM = substring(ext2.IXREF1, 2, 6)"
        + "   and doc2.E1SUF = substring(ext2.IXREF1, 8, 1)"
        + "   and ext2.IXTYP = 'lx'"
        // Conditions générales
        + " where"
        + "    doc.E1ETB = " + RequeteSql.formaterStringSQL(pListeIdDocumentVente.get(0).getCodeEtablissement()) + " ";
    // @formatter:on
    
    // Ajouter les identifiants des documents de ventes à charger à la requête
    // Lors des tests, le temps d'exécution de la requête est passé de 150ms à 2000ms lorsque le nombre d'identifiant à chercher
    // a dépassé 47. Cela ressemble à un bug du moteur d'analyse de requête DB2. Pour éviter cette explosion du temps de traitement,
    // on limite le nombre d'identifiants à 40. Il faut enchaîner plusieurs requêtes si le nombre d'identifiants dépasse ce chiffre.
    int index = 0;
    int nombreId = 0;
    for (index = pIndex; index < pListeIdDocumentVente.size() && nombreId < NOMBRE_MAX_IDENTIFIANT; index++) {
      IdDocumentVente idDocumentVente = pListeIdDocumentVente.get(index);
      
      // Contrôler l'identifiant
      IdDocumentVente.controlerId(idDocumentVente, true);
      
      // Exclure les factures
      if (!idDocumentVente.isIdFacture()) {
        if (nombreId == 0) {
          requete += " and (";
        }
        else {
          requete += " or ";
        }
        if (idDocumentVente.isIdFacture()) {
          requete += " (doc.E1NFA = " + idDocumentVente.getNumeroFacture() + ") ";
        }
        else {
          requete += " (doc.E1COD = '" + idDocumentVente.getEntete() + "' and doc.E1NUM = " + idDocumentVente.getNumero()
              + " and doc.E1SUF = " + idDocumentVente.getSuffixe() + ") ";
        }
        nombreId++;
      }
    }
    
    // Quitter si aucun identifiant n'a été ajouté
    if (nombreId > 0) {
      requete += ")";
    }
    else {
      return index;
    }
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requete);
    if (listeGenericRecord == null) {
      return index;
    }
    
    // Construire le résultat
    for (GenericRecord record : listeGenericRecord) {
      DocumentVenteBase documentVenteBase = initialiserDocumentVenteBase(record);
      pListeDocumentVenteBase.add(documentVenteBase);
      // Pour les factures, on charge aussi le bon d'origine
      if (record.getIntegerValue("E1NFA", 0) > 0 && record.getIntegerValue("E1NUM", 0) > 0) {
        record.setField("E1NFA", 0);
        pListeDocumentVenteBase.add(initialiserDocumentVenteBase(record));
      }
    }
    
    return index;
  }
  
  /**
   * Charger les données de base de documents de ventes de type facture sur la base des identifiants de documents transmis.
   * @param pListeDocumentVenteBase Liste de document de ventes à compléter.
   * @param pListeIdDocumentVente Liste d'identifiants de document de ventes à charger.
   */
  private void chargerListeFactureBase(ListeDocumentVenteBase pListeDocumentVenteBase, List<IdDocumentVente> pListeIdDocumentVente) {
    // Construire la requête
    // @formatter:off
    String requete = ""
        + " select"
        + "   doc.E1ETB,"
        + "   doc.E1NFA,"
        + "   doc.E1NUM,"
        + "   doc.E1MAG,"
        + "   doc.E1CRE,"
        + "   doc.E1DAT2,"
        + "   doc.E1HOM,"
        + "   doc.E1EXP,"
        + "   doc.E1FAC,"
        + "   doc.E1SL1,"
        + "   doc.E1SL2,"
        + "   doc.E1SL3,"
        + "   doc.E1SL1,"
        + "   doc.E1SL2,"
        + "   doc.E1SL3,"
        + "   doc.E1RCC,"
        + "   doc.E1VDE,"
        + "   doc.E1ETA,"
        + "   doc.E1MEX,"
        + "   doc.E1CLFP,"
        + "   doc.E1CLFS,"
        + "   doc.E1DCO,"
        + "   doc.E1TTC,"
        + "   doc.E1GBA,"
        + "   doc.E1IN18,"
        + "   '0' as EXTRACT,"
        + "   xli.XILIB"
        + " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc"
        // Jointure avec le fichier des extentions pour obtenir l'identifiant du chantier
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_EXTENSION + " xli on"
        + "   xli.XIETB = doc.E1ETB"
        + "   and xli.XICOD = doc.E1COD"
        + "   and xli.XINUM = doc.E1NUM"
        + "   and xli.XITYP = '40'"
        + " where"
        + "   doc.E1ETB = " + RequeteSql.formaterStringSQL(pListeIdDocumentVente.get(0).getCodeEtablissement())
        + "   and ("
        // Soit on recherche les entêtes de factures regroupées (X)
        + "     doc.E1COD = 'X'"
        // Soit on recherche les entêtes de factures standards (E), une facture standard a IXCODG = 0 dans PGVMIDXM
        + "     or (doc.E1COD = 'E'"
        + "         and exists(select 1 from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_INDEX
        + "                    where"
        + "                    IXTYP = 'FB'"
        + "                    and IXETB = doc.E1ETB"
        + "                    and IXREF1 = doc.E1NFA"
        + "                    and IXCODG = 0)))";
    // @formatter:on
    
    // Ajouter les identifiants des documents de ventes à charger à la requête
    int index = 0;
    for (IdDocumentVente idDocumentVente : pListeIdDocumentVente) {
      // Ne conserver que les factures
      if (idDocumentVente.isIdFacture()) {
        if (index == 0) {
          requete += " and (";
        }
        else {
          requete += " or ";
        }
        requete += " (doc.E1NFA = '" + idDocumentVente.getIndicatif(IdDocumentVente.INDICATIF_NUM) + "') ";
        index++;
      }
    }
    
    // Quitter si aucun identifiant n'a été ajouté
    if (index > 0) {
      requete += ")";
    }
    else {
      return;
    }
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requete);
    if (listeGenericRecord == null) {
      return;
    }
    
    // Construire le résultat
    for (GenericRecord record : listeGenericRecord) {
      DocumentVenteBase documentVenteBase = initialiserDocumentVenteBase(record);
      pListeDocumentVenteBase.add(documentVenteBase);
    }
    
    return;
  }
  
  /**
   * Initialiser les données d'un document de ventes de base à partir d'un GenericRecord.
   * @param pGenericRecord Objet enregistrement générique.
   * @return Le document de ventes de base.
   */
  private DocumentVenteBase initialiserDocumentVenteBase(GenericRecord pGenericRecord) {
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("E1ETB"));
    
    // Construire l'identifiant
    IdDocumentVente idDocumentVente;
    int numeroFacture = pGenericRecord.getIntValue("E1NFA", 0);
    if (numeroFacture == 0) {
      EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(pGenericRecord.getCharacterValue("E1COD"));
      idDocumentVente = IdDocumentVente.getInstanceHorsFacture(idEtablissement, codeEntete, pGenericRecord.getIntegerValue("E1NUM", 0),
          pGenericRecord.getIntegerValue("E1SUF", 0));
    }
    else {
      idDocumentVente = IdDocumentVente.getInstancePourFacture(idEtablissement, pGenericRecord.getIntegerValue("E1NFA", 0));
    }
    
    // Créer le document de ventes
    DocumentVenteBase documentVenteBase = new DocumentVenteBase(idDocumentVente);
    
    // Verouillage (le document de ventes est verouillé si E1TOP != 0)
    documentVenteBase.setVerrouille(pGenericRecord.getIntegerValue("E1TOP", 0) != 0);
    
    // Magasin
    documentVenteBase.setIdMagasin(IdMagasin.getInstance(idEtablissement, pGenericRecord.getStringValue("E1MAG")));
    
    // Vendeur
    if (!pGenericRecord.getStringValue("E1VDE").isEmpty()) {
      IdVendeur idVendeur = IdVendeur.getInstance(idEtablissement, pGenericRecord.getStringValue("E1VDE"));
      documentVenteBase.setIdVendeur(idVendeur);
    }
    
    // Client facturé
    IdClient idClientFacture =
        IdClient.getInstance(idEtablissement, pGenericRecord.getIntegerValue("E1CLFP", 0), pGenericRecord.getIntegerValue("E1CLFS", 0));
    documentVenteBase.setIdClientFacture(idClientFacture);
    
    // Etat
    documentVenteBase.setEtat(EnumEtatBonDocumentVente.valueOfByCode(pGenericRecord.getIntegerValue("E1ETA", 0)));
    
    // Etat extraction
    boolean extractionTotale = pGenericRecord.getIntegerValue("E1TDV") == EnumEtatDevisDocumentVente.CLOTURE.getCode();
    if (extractionTotale) {
      documentVenteBase.setEtatExtraction(EnumEtatExtractionDocumentVente.COMPLETE);
    }
    else {
      String codeExtraction = pGenericRecord.getStringValue("EXTRACT");
      documentVenteBase.setEtatExtraction(EnumEtatExtractionDocumentVente.valueOfByCode(Integer.parseInt(codeExtraction)));
    }
    
    // Date de création
    documentVenteBase.setDateCreation(pGenericRecord.getDateValue("E1CRE"));
    
    // Date de validité devis
    documentVenteBase.setDateValiditeDevis(pGenericRecord.getDateValue("E1DAT2"));
    
    // Date de validation commande
    documentVenteBase.setDateValidationCommande(pGenericRecord.getDateValue("E1HOM"));
    
    // Date d'expédition du bon
    documentVenteBase.setDateExpeditionBon(pGenericRecord.getDateValue("E1EXP"));
    
    // Date de facturation
    documentVenteBase.setDateFacturation(pGenericRecord.getDateValue("E1FAC"));
    
    // Référence longue
    documentVenteBase.setReferenceLongue(pGenericRecord.getStringValue("E1RCC"));
    
    // Mode d'expédition
    documentVenteBase.setIdModeExpedition(IdModeExpedition.getInstance(pGenericRecord.getStringValue("E1MEX")));
    
    // Direct usine
    documentVenteBase.setDirectUsine(pGenericRecord.getStringValue("E1IN18").equals("D"));
    
    // Génération commande d'achats
    switch (pGenericRecord.getIntegerValue("E1GBA", 0)) {
      case 1:
        documentVenteBase.setGenerationImmediateCommandeAchat(false);
        documentVenteBase.setCommandeAchatGeneree(false);
        break;
      case 2:
        documentVenteBase.setGenerationImmediateCommandeAchat(false);
        documentVenteBase.setCommandeAchatGeneree(false);
        break;
      case 5:
        documentVenteBase.setGenerationImmediateCommandeAchat(true);
        documentVenteBase.setCommandeAchatGeneree(false);
        break;
      case 6:
        documentVenteBase.setGenerationImmediateCommandeAchat(true);
        documentVenteBase.setCommandeAchatGeneree(false);
        break;
      case 3:
        documentVenteBase.setGenerationImmediateCommandeAchat(false);
        documentVenteBase.setCommandeAchatGeneree(true);
        break;
      case 4:
        documentVenteBase.setGenerationImmediateCommandeAchat(false);
        documentVenteBase.setCommandeAchatGeneree(true);
        break;
      case 7:
        documentVenteBase.setGenerationImmediateCommandeAchat(true);
        documentVenteBase.setCommandeAchatGeneree(true);
        break;
      case 8:
        documentVenteBase.setGenerationImmediateCommandeAchat(true);
        documentVenteBase.setCommandeAchatGeneree(true);
        break;
      default:
        documentVenteBase.setGenerationImmediateCommandeAchat(false);
        documentVenteBase.setCommandeAchatGeneree(false);
        break;
    }
    
    // Récupération des l'états d'extraction du document courant et du document archivé
    Integer extractCrt = null;
    Integer extractArchive = null;
    if (pGenericRecord.isPresentField("EXTRACT_CRT")) {
      extractCrt = pGenericRecord.getIntegerValue("EXTRACT_CRT");
    }
    if (pGenericRecord.isPresentField("EXTRACT_ARCHIVE")) {
      extractArchive = pGenericRecord.getIntegerValue("EXTRACT_ARCHIVE");
    }
    
    // Pour les devis, commandes avec extraction non complète et bons (car la requête n'est pas la même issue de la méthode
    // chargerListeDocumentVenteBase)
    if ((pGenericRecord.isPresentField("E1SL1_A") && pGenericRecord.isPresentField("E1TTC_A"))
        || (pGenericRecord.isPresentField("E1SL1_B") && pGenericRecord.isPresentField("E1TTC_B"))) {
      // Récupération des montants HT et TTC en fonction des états d'extraction
      // Si l'état n'a pas changé ou s'il s'agit d'une facture alors les données sont lu dans le document courant, ceci est fait pour
      // éviter le bug sur les documents modifiés où les montants sont lus dans les documents archivés et donc potentiellement faux en cas
      // de modification sans extraction
      if (extractCrt != null && extractArchive != null && extractCrt.compareTo(extractArchive) == 0) {
        // S'il n'y a pas eu d'extraction alors lecture dans le document courant
        if (documentVenteBase.getEtatExtraction() == EnumEtatExtractionDocumentVente.AUCUNE) {
          // Total HT
          documentVenteBase.setTotalHT(pGenericRecord.getBigDecimalValue("E1SL1_A", BigDecimal.ZERO)
              .add(pGenericRecord.getBigDecimalValue("E1SL2_A", BigDecimal.ZERO))
              .add(pGenericRecord.getBigDecimalValue("E1SL3_A", BigDecimal.ZERO)));
          
          // Total TTC
          documentVenteBase.setTotalTTC(pGenericRecord.getBigDecimalValue("E1TTC_A", BigDecimal.ZERO));
        }
        // Sinon si extraction partielle ou complète alors lecture dans le document archivé si non null
        else {
          // Total HT
          documentVenteBase.setTotalHT(pGenericRecord.getBigDecimalValue("E1SL1_B", BigDecimal.ZERO)
              .add(pGenericRecord.getBigDecimalValue("E1SL2_B", BigDecimal.ZERO))
              .add(pGenericRecord.getBigDecimalValue("E1SL3_B", BigDecimal.ZERO)));
          
          // Total TTC
          documentVenteBase.setTotalTTC(pGenericRecord.getBigDecimalValue("E1TTC_B", BigDecimal.ZERO));
        }
      }
      // Sinon lecture dans le document archivé si non null ou alors dans le document courant
      else {
        // Total HT
        documentVenteBase.setTotalHT(pGenericRecord.getBigDecimalValue("E1SL1_B", BigDecimal.ZERO)
            .add(pGenericRecord.getBigDecimalValue("E1SL2_B", BigDecimal.ZERO))
            .add(pGenericRecord.getBigDecimalValue("E1SL3_B", BigDecimal.ZERO)));
        
        // Total TTC
        documentVenteBase.setTotalTTC(pGenericRecord.getBigDecimalValue("E1TTC_B", BigDecimal.ZERO));
      }
    }
    // Pour les factures (car la requête n'est pas la même issue de la méthode chargerListeFactureBase)
    else if (pGenericRecord.isPresentField("E1SL1") && pGenericRecord.isPresentField("E1TTC")) {
      // Total HT
      documentVenteBase.setTotalHT(
          pGenericRecord.getBigDecimalValue("E1SL1", BigDecimal.ZERO).add(pGenericRecord.getBigDecimalValue("E1SL2", BigDecimal.ZERO))
              .add(pGenericRecord.getBigDecimalValue("E1SL3", BigDecimal.ZERO)));
      
      // Total TTC
      documentVenteBase.setTotalTTC(pGenericRecord.getBigDecimalValue("E1TTC", BigDecimal.ZERO));
    }
    
    // Maj le type du document
    documentVenteBase.deduireTypeDocumentVente();
    
    // Identifiant du chantier
    int numeroChantier = Integer.parseInt(pGenericRecord.getStringValue("XILIB", "0", true));
    if (numeroChantier > 0) {
      documentVenteBase.setIdChantier(IdChantier.getInstance(idEtablissement, numeroChantier));
    }
    
    return documentVenteBase;
  }
  
  /**
   * Initialiser un identifiant de document de ventes (AVOIR) à partir d'un GenericRecord.
   * @param pGenericRecord Objet enregistrement générique.
   * @return L'identifiant de document de ventes.
   */
  private IdDocumentVente initialiserIdDocumentVenteAvoir(GenericRecord pGenericRecord) {
    // Lire l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("E1ETB"));
    
    // Lire le code entête
    EnumCodeEnteteDocumentVente codeEntete = null;
    Character e1cod = pGenericRecord.getCharacterValue("E1COD");
    if (e1cod != null && !e1cod.equals(' ')) {
      codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(e1cod);
    }
    
    // Lire le numéro de document de l'avoir
    Integer numero = pGenericRecord.getIntegerValue("E1NUM");
    
    // Lire le numéro de suffixe de l'avoir
    Integer suffixe = pGenericRecord.getIntegerValue("E1SUF");
    
    // Retourner l'identifiant du document de ventes
    return IdDocumentVente.getInstanceHorsFacture(idEtablissement, codeEntete, numero, suffixe);
  }
  
  /**
   * Ajouter en jointure la table des extractions.
   * @return Le texte de jointure de la table des extractions.
   */
  private String ajouterJointureTableExtraction() {
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter(" left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_INDEX + " ext on");
    requeteSql.ajouterConditionAnd("doc.E1ETB", "=", "ext.IXETB", true, false);
    requeteSql.ajouterConditionAnd("doc.E1COD", "=", "substring(ext.IXREF1,1,1)", true, false);
    requeteSql.ajouterConditionAnd("doc.E1NUM", "=", "substring(ext.IXREF1,2,6)", true, false);
    requeteSql.ajouterConditionAnd("doc.E1SUF", "=", "substring(ext.IXREF1,8,1)", true, false);
    requeteSql.ajouterConditionAnd("ext.IXTYP", "=", "lx");
    
    return requeteSql.getRequete();
  }
  
}
