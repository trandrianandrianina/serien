/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

public abstract class FFD_Psemlecm extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_ECID = 9;
  public static final int DECIMAL_ECID = 0;
  public static final int SIZE_ECIDC = 9;
  public static final int DECIMAL_ECIDC = 0;
  public static final int SIZE_ECETBC = 3;
  public static final int SIZE_ECIDE = 9;
  public static final int DECIMAL_ECIDE = 0;
  public static final int SIZE_ECTYP = 2;
  public static final int SIZE_ECHIE = 9;
  public static final int DECIMAL_ECHIE = 0;
  
  // Variables fichiers
  private int ECID = 0; // L'ID
  private int ECIDC = 0; // L'ID Contact
  private String ECETBC = null; // Etablissement du contact
  private int ECIDE = 0; // L'ID Evènement
  private String ECTYP = null; // Type du contact (Créateur, Executant, Cible)
  // NO_HIERARCHY; // Code responsabilité (hiérarchie des intervenants qui doivent réaliser la tache ds le cas d'une tache à plusieurs
  // comme du phoning par ex ou d'un salon)
  private int ECHIE = 0;
  
  // La valeur la plus faible correspond au niveau de hiérarchie la plus haute
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public FFD_Psemlecm(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    ECID = 0;
    ECIDC = 0;
    ECETBC = null;
    ECIDE = 0;
    ECTYP = null;
    ECHIE = 0; // NO_HIERARCHY;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le eCID
   */
  public int getECID() {
    return ECID;
  }
  
  /**
   * @param eCID le eCID à définir
   */
  public void setECID(int eCID) {
    ECID = eCID;
  }
  
  /**
   * @return le eCIDC
   */
  public int getECIDC() {
    return ECIDC;
  }
  
  /**
   * @param eCIDC le eCIDC à définir
   */
  public void setECIDC(int eCIDC) {
    ECIDC = eCIDC;
  }
  
  /**
   * @return le eCETBC
   */
  public String getECETBC() {
    return ECETBC;
  }
  
  /**
   * @param eCETBC le eCETBC à définir
   */
  public void setECETBC(String eCETBC) {
    ECETBC = eCETBC;
  }
  
  /**
   * @return le eCIDE
   */
  public int getECIDE() {
    return ECIDE;
  }
  
  /**
   * @param eCIDE le eCIDE à définir
   */
  public void setECIDE(int eCIDE) {
    ECIDE = eCIDE;
  }
  
  /**
   * @return le eCTYP
   */
  public String getECTYP() {
    return ECTYP;
  }
  
  /**
   * @param eCTYP le eCTYP à définir
   */
  public void setECTYP(String eCTYP) {
    ECTYP = eCTYP;
  }
  
  /**
   * @return le eCHIE
   */
  public int getECHIE() {
    return ECHIE;
  }
  
  /**
   * @param eCHIE le eCHIE à définir
   */
  public void setECHIE(int eCHIE) {
    ECHIE = eCHIE;
  }
  
}
