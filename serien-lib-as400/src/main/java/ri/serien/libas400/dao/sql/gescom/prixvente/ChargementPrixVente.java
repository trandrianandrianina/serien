/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.prixvente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ri.serien.libas400.dao.sql.gescom.parametres.SqlGescomParametres;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.dataarea.GestionDataareaAS400;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.CritereFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.CritereGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.EnumTypeGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.GroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.IdGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.ListeGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.ListeParametreSysteme;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.MapQuantiteRattachement;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineClient;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineConditionVenteCentrale;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineConditionVentePromotionnelle;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineConditionVenteStandard;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineDevise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.CritereParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.ParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.ListeParametreChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.EnumRegleExclusionConditionVenteEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.ParametreClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.EnumCategorieConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.EnumTypeRattachementArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.IdRattachementArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.IdRattachementClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ListeParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.ParametreDocumentVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Le rôle de cette classe est de charger toutes les données nécessaires au calcul d'un prix de vente.
 */
public class ChargementPrixVente {
  // Variables
  // Paramètres en entrée
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private IdArticle idArticle = null;
  private IdClient idClientLivre = null;
  private IdClient idClientFacture = null;
  private IdLigneVente idLigneVente = null;
  private IdDocumentVente idDocumentVente = null;
  private IdChantier idChantier = null;
  private Date dateApplication = null;
  
  // Paramètres en sortie
  private ParametreEtablissement parametreEtablissement = null;
  private ParametreArticle parametreArticle = null;
  private ParametreTarif parametreTarif = null;
  private ParametreClient parametreClientLivre = null;
  private ParametreClient parametreClientFacture = null;
  private ParametreDocumentVente parametreDocumentVente = null;
  private ParametreLigneVente parametreLigneVente = null;
  private ParametreConditionVente parametreConditionVentePrioritaire = null;
  private ListeParametreConditionVente listeParametreConditionVenteNormale = null;
  private ListeParametreConditionVente listeParametreConditionVenteQuantitative = null;
  private ListeParametreConditionVente listeParametreConditionVenteCumulative = null;
  private ListeParametreChantier parametreChantier = null;
  private ParametreChargement parametreChargement = null;
  
  // Variables de travail
  private SystemeManager systemeManager = null;
  private QueryManager queryManager = null;
  private SqlGescomParametres sqlGescomParametres = null;
  private SqlPrixVente sqlPrixVente = null;
  private String codeDevise = null;
  private EnumOrigineDevise origineDevise = null;
  private EnumOrigineClient origineClient = null;
  private ListeParametreConditionVente listeTousParametreConditionVente = null;
  
  // Identifiants et origines pour la sélection des conditions de ventes à appliquer
  private IdRattachementClient idRattachementClientConditionVenteStandardEtablissement = null;
  // Pour les conditions de ventes standards c'est soit un groupe soit un numéro client (mais pas les deux en même temps)
  private IdRattachementClient idRattachementClientConditionVenteStandard = null;
  // private IdClient idClientPourConditionVenteStandard = null;
  private EnumOrigineConditionVenteStandard origineConditionVenteStandard = null;
  private IdRattachementClient idRattachementClientConditionVentePromotionnelle = null;
  private EnumOrigineConditionVentePromotionnelle origineGroupeConditionVentePromotionnelle = null;
  private Integer numeroClientPourCentrale1 = null;
  private EnumOrigineConditionVenteCentrale origineConditionVenteCentrale1 = null;
  private Integer numeroClientPourCentrale2 = null;
  private EnumOrigineConditionVenteCentrale origineConditionVenteCentrale2 = null;
  private Integer numeroClientPourCentrale3 = null;
  private EnumOrigineConditionVenteCentrale origineConditionVenteCentrale3 = null;
  
  /**
   * Constructeur.
   */
  public ChargementPrixVente(SystemeManager pSystemeManager, QueryManager pQueryManager, ParametreChargement pParametreChargement) {
    initialiser(pSystemeManager, pQueryManager, pParametreChargement);
  }
  
  // -- Méthodes publiques
  
  /**
   * Charge les données nécessaires au calcul du prix.
   */
  public ParametreChargement charger() {
    // Chargement des données nécessaires au calcul de la date et de la devise en premier
    chargerParametreEtablissementPartie1();
    chargerLigneVente();
    chargerParametreDocumentVente();
    chargerParametreClientLivre();
    chargerParametreClientFacture();
    chargerParametreArticle();
    
    // Si l'article est un acrticle commentaire pas la peine d'aller plus loin
    if (parametreArticle != null && parametreArticle.isArticleCommentaire()) {
      Trace.alerte(
          "L'article " + parametreArticle.getCodeArticle() + " est un article commentaire. Le chargement des paramètres a été avorté.");
      return null;
    }
    
    calculerDateApplication();
    calculerDevise();
    
    // Chargement des autres données stockées en base
    chargerParametreEtablissementPartie2();
    chargerParametreTarif();
    chargerListeParametreConditionVente();
    cumulerQuantiteListeArticleDocumentVente();
    chargerParametreChantier();
    
    // Sélectionner les conditions de ventes
    selectionnerConditionVente();
    
    // Initialise les paramètres chargés
    parametreChargement.setParametreEtablissement(parametreEtablissement);
    parametreChargement.setParametreArticle(parametreArticle);
    parametreChargement.setParametreLigneVente(parametreLigneVente);
    parametreChargement.setParametreDocumentVente(parametreDocumentVente);
    parametreChargement.setParametreClientLivre(parametreClientLivre);
    parametreChargement.setParametreClientFacture(parametreClientFacture);
    parametreChargement.setParametreTarif(parametreTarif);
    parametreChargement.setParametreChantier(parametreChantier);
    parametreChargement.setParametreConditionVentePrioritaire(parametreConditionVentePrioritaire);
    parametreChargement.setListeParametreConditionVenteNormale(listeParametreConditionVenteNormale);
    parametreChargement.setListeParametreConditionVenteQuantitative(listeParametreConditionVenteQuantitative);
    parametreChargement.setListeParametreConditionVenteCumulative(listeParametreConditionVenteCumulative);
    
    return parametreChargement;
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise les variables avec les données du paramètres de chargement.
   * 
   * @param pSystemeManager
   * @param pQueryManager
   * @param pParametreChargement
   */
  private void initialiser(SystemeManager pSystemeManager, QueryManager pQueryManager, ParametreChargement pParametreChargement) {
    // Contrôle des paramètres du chargement
    if (pSystemeManager == null) {
      throw new MessageErreurException("Le systemeManager est invalide.");
    }
    if (pQueryManager == null) {
      throw new MessageErreurException("Le queryManager est invalide.");
    }
    if (Constantes.normerTexte(pQueryManager.getLibrary()).isEmpty()) {
      throw new MessageErreurException("La base de données est invalide.");
    }
    systemeManager = pSystemeManager;
    queryManager = pQueryManager;
    
    if (pParametreChargement == null) {
      throw new MessageErreurException("Les paramètres pour le chargement des données sont invalides.");
    }
    parametreChargement = pParametreChargement;
    parametreChargement.controlerParametreObligatoirePourChargement();
    
    // Initialisation des variables à partir des paramètres de chargement
    idEtablissement = pParametreChargement.getIdEtablissement();
    idMagasin = pParametreChargement.getIdMagasin();
    idArticle = pParametreChargement.getIdArticle();
    idLigneVente = pParametreChargement.getIdLigneVente();
    idDocumentVente = pParametreChargement.getIdDocumentVente();
    parametreDocumentVente = pParametreChargement.getParametreDocumentVente();
    parametreLigneVente = pParametreChargement.getParametreLigneVente();
    idChantier = pParametreChargement.getIdChantier();
    dateApplication = parametreChargement.getDateApplication();
    
    sqlPrixVente = new SqlPrixVente(queryManager);
  }
  
  /**
   * Chargement des paramètres généraux (première partie).
   * Le chargement de l'établissement et des paramètres systèmes.
   */
  private void chargerParametreEtablissementPartie1() {
    parametreEtablissement = new ParametreEtablissement();
    sqlGescomParametres = new SqlGescomParametres(queryManager);
    
    // Personnalisation DG (établissement)
    Etablissement etablissement = sqlGescomParametres.chargerEtablissement(idEtablissement);
    parametreEtablissement.modifierInformationEtablissement(etablissement);
    
    // Les paramètres systèmes
    ListeParametreSysteme listeParametreSysteme = sqlGescomParametres.chargerListeParametreSysteme(idEtablissement);
    parametreEtablissement.setPs032(listeParametreSysteme.getValeurParametreSysteme(idEtablissement, EnumParametreSysteme.PS032));
    parametreEtablissement.setPs047(listeParametreSysteme.getValeurParametreSysteme(idEtablissement, EnumParametreSysteme.PS047));
    parametreEtablissement.setPs119(listeParametreSysteme.getValeurParametreSysteme(idEtablissement, EnumParametreSysteme.PS119));
    parametreEtablissement
        .setNumeroColonneTarifPS105(listeParametreSysteme.getValeurParametreSysteme(idEtablissement, EnumParametreSysteme.PS105));
    parametreEtablissement.setPs124(listeParametreSysteme.isParametreSystemeActif(idEtablissement, EnumParametreSysteme.PS124));
    parametreEtablissement.setPs216(listeParametreSysteme.getValeurParametreSysteme(idEtablissement, EnumParametreSysteme.PS216));
    parametreEtablissement.setPs253(listeParametreSysteme.isParametreSystemeActif(idEtablissement, EnumParametreSysteme.PS253));
    parametreEtablissement
        .setArticleNonRemisablePS281(listeParametreSysteme.getValeurParametreSysteme(idEtablissement, EnumParametreSysteme.PS281));
    parametreEtablissement.setModeNegocePS287(listeParametreSysteme.isParametreSystemeActif(idEtablissement, EnumParametreSysteme.PS287));
    parametreEtablissement.setRechercheColonneTarifNonNullePS305(
        listeParametreSysteme.isParametreSystemeActif(idEtablissement, EnumParametreSysteme.PS305));
    parametreEtablissement
        .setPrixPublicCommePrixBasePS316(listeParametreSysteme.isParametreSystemeActif(idEtablissement, EnumParametreSysteme.PS316));
    
    // Les paramètres de la dataarea PGVMSPEM
    chargerDataareaPGVMSPEM();
  }
  
  /**
   * Charge les données nécessaires à partir de la DTAAREA PGVMSPEM.
   * 
   * Voir le CL INISPEMDTA pour la description du contenu.
   */
  private void chargerDataareaPGVMSPEM() {
    // Lecture du contenu de la dataarea
    Dataarea pgvmspem = new Dataarea();
    pgvmspem.setNom("PGVMSPEM");
    Bibliotheque bibliotheque =
        new Bibliotheque(IdBibliotheque.getInstance(queryManager.getLibrary()), EnumTypeBibliotheque.BASE_DE_DONNEES);
    pgvmspem.setBibliotheque(bibliotheque);
    try {
      GestionDataareaAS400 operation = new GestionDataareaAS400(systemeManager.getSystem());
      pgvmspem.setValeur(operation.chargerDataarea(pgvmspem));
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    
    // Récupération des données nécessaires
    String chaine = pgvmspem.getValeur();
    if (chaine == null) {
      return;
    }
    // En position 112
    if (chaine.length() >= 112) {
      // La valeur de la position 112 sert à 2 choses :
      // - si la valeur vaut ' ' alors le couple condition de ventes client/article en prix net comme prioritaire par rapport aux autres
      // CNV
      // - si la valeur est différente de ' ' alors les CNV identifiées par le numéro client (dans le T1CNV) sont ignorés
      char valeur = chaine.charAt(112 - 1);
      if (valeur != ' ') {
        parametreEtablissement.setConditionVenteClientArticlePourPrixNetPrioritaire(false);
        parametreEtablissement.setIgnoreConditionVenteSurNumeroClient(true);
      }
      else {
        parametreEtablissement.setConditionVenteClientArticlePourPrixNetPrioritaire(true);
        parametreEtablissement.setIgnoreConditionVenteSurNumeroClient(false);
      }
    }
  }
  
  /**
   * Chargement des paramètres généraux (deuxième partie).
   * Le reste du chargement des données nécessaires.
   */
  private void chargerParametreEtablissementPartie2() {
    // Paramètres CN (Groupe de conditions de ventes)
    // Les groupes de conditions de ventes hors période sont éléminés lors du chargement de ces groupes.
    CritereGroupeConditionVente critereGroupeConditionVente = new CritereGroupeConditionVente();
    critereGroupeConditionVente.setIdEtablissement(idEtablissement);
    critereGroupeConditionVente.setDateApplication(dateApplication);
    List<EnumTypeGroupeConditionVente> listeTypeGroupeConditionVente = new ArrayList<EnumTypeGroupeConditionVente>();
    listeTypeGroupeConditionVente.add(EnumTypeGroupeConditionVente.CONDITION_STANDARD);
    listeTypeGroupeConditionVente.add(EnumTypeGroupeConditionVente.CONDITION_PROMOTIONNELLE);
    critereGroupeConditionVente.setListeTypeGroupe(listeTypeGroupeConditionVente);
    parametreEtablissement
        .setListeGroupeConditionVente(sqlGescomParametres.chargerListeGroupeConditionVente(critereGroupeConditionVente));
    
    // Paramètres TA
    // TODO si nécessaire
    
    // Paramètres FP (Formule prix)
    CritereFormulePrix critereFormulePrix = new CritereFormulePrix();
    critereFormulePrix.setIdEtablissement(idEtablissement);
    parametreEtablissement.setListeFormulePrix(sqlGescomParametres.chargerListeFormulePrix(critereFormulePrix));
  }
  
  /**
   * Charge les paramètres du document de vente.
   */
  private void chargerParametreDocumentVente() {
    // Cas où les paramètres du document de vente ne sont pas chargés
    if (idDocumentVente == null && idLigneVente == null) {
      return;
    }
    // Si l'identifiant du document de vente n'a pas été fourni, il est construit à partir de l'identifiant de la ligne
    if (idDocumentVente == null) {
      idDocumentVente = idLigneVente.getIdDocumentVente();
    }
    // Contrôle si l'identifiant du document de vente existe en base
    if (!idDocumentVente.isExistant()) {
      return;
    }
    // Chargement du document de vente
    try {
      // Chargement de l'entête
      parametreDocumentVente = sqlPrixVente.chargerEnteteDocumentVente(idDocumentVente);
      if (parametreDocumentVente == null) {
        Trace.alerte("Aucun document de vente n'a été trouvé avec l'identifiant " + idDocumentVente.getTexte());
      }
      
      // Initialisation des identifants client et de l'origine
      origineClient = EnumOrigineClient.DOCUMENT_VENTE;
      if (parametreDocumentVente != null) {
        idClientLivre = parametreDocumentVente.getIdClientLivre();
        idClientFacture = parametreDocumentVente.getIdClientFacture();
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Charge les paramètres de la ligne de vente.
   */
  private void chargerLigneVente() {
    // Cas où les paramètres de la ligne de ventes ne sont pas chargés
    if (idLigneVente == null) {
      return;
    }
    // Contrôle si l'identifiant de la ligne de ventes existe en base
    if (!idLigneVente.isExistant()) {
      return;
    }
    // Chargement de la ligne de vente
    try {
      parametreLigneVente = sqlPrixVente.chargerLigneVente(idLigneVente);
      if (parametreLigneVente == null) {
        Trace.alerte("Aucune ligne de vente n'a été trouvée avec l'identifiant " + idLigneVente.getTexte());
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Charge les paramètres de l'article.
   */
  private void chargerParametreArticle() {
    // Si l'identifiant de l'article n'a pas été fourni
    if (idArticle == null) {
      // Si les paramètres de la ligne de vente ne contiennent pas le code de l'article alors les paramètres de l'article ne sont
      // pas chargés
      if (parametreLigneVente == null || parametreLigneVente.getIdArticle() == null) {
        return;
      }
      // L'identifiant article de la ligne du document de vente est utilisé
      idArticle = parametreLigneVente.getIdArticle();
    }
    
    // Contrôle si l'identifiant de l'article> existe en base
    if (!idArticle.isExistant()) {
      return;
    }
    
    // Chargement de l'article
    try {
      CritereParametreArticle critereParametreArticle = new CritereParametreArticle();
      critereParametreArticle.setIdArticle(idArticle);
      critereParametreArticle.setIdMagasin(idMagasin);
      critereParametreArticle.setPs124(parametreEtablissement.getPs124());
      critereParametreArticle.setModeNegoce(parametreEtablissement.isModeNegocePS287());
      parametreArticle = sqlPrixVente.chargerArticle(critereParametreArticle);
      if (parametreArticle == null) {
        Trace.alerte("Aucun article n'a été trouvé avec l'identifiant " + idArticle.getTexte());
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Charge les paramètres du tarif article.
   */
  private void chargerParametreTarif() {
    if (dateApplication == null) {
      throw new MessageErreurException("La date d'application est invalide.");
    }
    if (codeDevise == null) {
      throw new MessageErreurException("Le code devise est invalide.");
    }
    // Chargement du tarif article
    parametreTarif = sqlPrixVente.chargerTarif(idArticle, dateApplication, codeDevise);
    if (parametreTarif == null) {
      Trace.alerte("Aucun tarif n'a été trouvé pour l'article " + idArticle.getCodeArticle());
      return;
    }
    // Les paramètres sont complétés
    parametreTarif.setCodeDevise(codeDevise);
    parametreTarif.setOrigineDevise(origineDevise);
  }
  
  /**
   * Charge les paramètres du client livré.
   */
  private void chargerParametreClientLivre() {
    // Si l'identifiant du client n'est pas renseigné (fournit ou du document de vente) alors les paramètres du client ne sont pas chargés
    if (idClientLivre == null && parametreChargement.getIdClientLivre() == null) {
      return;
    }
    // Si l'identifiant du client livré n'a pas été trouvé dans l'entête du document alors utilisation de l'identifiant fournit
    if (idClientLivre == null && parametreChargement.getIdClientLivre() != null) {
      origineClient = EnumOrigineClient.SAISI;
      idClientLivre = parametreChargement.getIdClientLivre();
    }
    
    // Contrôle si l'identifiant du client livré existe en base
    if (idClientLivre == null || !idClientLivre.isExistant()) {
      return;
    }
    // Contrôle que le client livré et le client facturé ne sont pas identiques
    if (Constantes.equals(idClientLivre, idClientFacture)) {
      return;
    }
    
    // Chargement du client livré
    try {
      parametreClientLivre = sqlPrixVente.chargerClient(idClientLivre);
      if (parametreClientLivre == null) {
        throw new MessageErreurException("Aucun client livré n'a été trouvé avec l'identifiant " + idClientLivre.getTexte());
      }
      parametreClientLivre.setOrigineClient(origineClient);
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Charge les paramètres du client facturé.
   */
  private void chargerParametreClientFacture() {
    // Si l'identifiant du client n'est pas renseigné (fournit ou du document de vente) alors les paramètres du client ne sont pas chargés
    if (idClientFacture == null && parametreChargement.getIdClientFacture() == null) {
      return;
    }
    // Si l'identifiant du client facturé n'a pas été trouvé dans l'entête du document alors utilisation de l'identifiant fournit
    if (idClientFacture == null && parametreChargement.getIdClientFacture() != null) {
      origineClient = EnumOrigineClient.SAISI;
      idClientFacture = parametreChargement.getIdClientFacture();
    }
    
    // Contrôle si l'identifiant du client facturé existe en base
    if (idClientFacture == null || !idClientFacture.isExistant()) {
      return;
    }
    
    // Chargement du client facturé
    try {
      parametreClientFacture = sqlPrixVente.chargerClient(idClientFacture);
      if (parametreClientFacture == null) {
        throw new MessageErreurException("Aucun client facturé n'a été trouvé avec l'identifiant " + idClientFacture.getTexte());
      }
      parametreClientFacture.setOrigineClient(origineClient);
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Charge la liste de tous les paramètres des conditions de ventes.
   * 
   * Dans le but d'optimiser les temps de chargement, il a été décidé de charger l'ensemble des conditions de ventes d'un coup avec un
   * filtrage le plus fin possible.
   */
  private void chargerListeParametreConditionVente() {
    // Chargement de la liste de l'ensemble des conditions de ventes
    try {
      // Recherche des conditions de ventes sur fournisseur
      boolean ps253 = false;
      if (parametreEtablissement.getPs253() != null) {
        ps253 = parametreEtablissement.getPs253().booleanValue();
      }
      // Recherche sur le numéro du client
      String filtrePourIgnorerNumeroClient = null;
      if (parametreEtablissement.isIgnoreConditionVenteSurNumeroClient() && idClientFacture != null) {
        filtrePourIgnorerNumeroClient = idClientFacture.getIndicatif(IdClient.INDICATIF_NUM);
      }
      listeTousParametreConditionVente = sqlPrixVente.chargerListeConditionVente(idEtablissement, codeDevise, dateApplication,
          parametreArticle, ps253, filtrePourIgnorerNumeroClient);
      if (listeTousParametreConditionVente == null) {
        throw new MessageErreurException("Aucune condition de ventes n'a été trouvé dans l'établissement " + idEtablissement.getTexte());
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
      return;
    }
    Trace.alerte("Nombre total de conditions de ventes trouvées : " + listeTousParametreConditionVente.size());
    // afficherListeCNV("Liste CNV (toutes)", listeTousParametreConditionVente);
  }
  
  /**
   * Cumuler les quantités articles en fonction des rattachements possibles.
   * 
   * Ce cumul n'est effectué que si des conditions quantitatives sont présentes.
   * La ligne de vente du calcul de prix en cours n'est pas chargée.
   */
  private void cumulerQuantiteListeArticleDocumentVente() {
    if (idDocumentVente == null || !idDocumentVente.isExistant()) {
      return;
    }
    if (listeParametreConditionVenteQuantitative == null || listeParametreConditionVenteQuantitative.isEmpty()) {
      return;
    }
    
    try {
      // Chargement des paramètres des lignes de ventes excepté la ligne courante
      List<ParametreLigneVente> listeParametreLigneVente = sqlPrixVente.chargerListeParametreLigneVente(idDocumentVente, idLigneVente);
      if (listeParametreLigneVente == null || listeParametreLigneVente.isEmpty()) {
        return;
      }
      
      // Construction de la la liste des identifiants des articles dont il faut récupérer les informations
      List<IdArticle> listeIdArticle = new ArrayList<IdArticle>();
      for (ParametreLigneVente parametreLigneVente : listeParametreLigneVente) {
        IdArticle idArticle = parametreLigneVente.getIdArticle();
        if (!listeIdArticle.contains(idArticle)) {
          listeIdArticle.add(idArticle);
        }
      }
      
      // Chargement des informations des articles contennus dans le document de ventes
      Map<String, ParametreArticle> mapParametreArticle =
          sqlPrixVente.chargerListeArticle(listeIdArticle, idMagasin, parametreEtablissement.isModeNegocePS287());
      if (mapParametreArticle == null || mapParametreArticle.isEmpty()) {
        return;
      }
      
      // Initialisation des rattachements avec l'article dont il faut calculer le prix de vente
      MapQuantiteRattachement mapQuantiteParRattachement = parametreArticle.initialiserQuantiteRattachement();
      
      // Calculer les quantités des articles par regroupement (pour l'instant on ne tient pas compte de l'unité)
      for (ParametreLigneVente parametreLigneVente : listeParametreLigneVente) {
        String codeArticle = parametreLigneVente.getIdArticle().getCodeArticle();
        // Contrôle que la quantité soit valide
        BigDecimal quantite = parametreLigneVente.getQuantiteUV();
        if (quantite == null) {
          Trace.alerte("La quantité de l'article " + codeArticle + " est invalide.");
          continue;
        }
        ParametreArticle parametreArticle = mapParametreArticle.get(codeArticle);
        if (parametreArticle == null) {
          Trace.alerte("Les paramètres de l'article " + codeArticle + " sont invalides.");
          continue;
        }
        parametreArticle.ajouterQuantitePourRattachement(quantite, mapQuantiteParRattachement);
      }
      parametreDocumentVente.setMapQuantiteParRattachement(mapQuantiteParRattachement);
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Charge les paramètres du chantier.
   */
  private void chargerParametreChantier() {
    // Si l'identifiant du chantier n'est pas renseigné alors celui du document de vente est utilisé si présent
    if (idChantier == null && parametreDocumentVente != null) {
      idChantier = parametreDocumentVente.retournerIdChantier(idEtablissement);
    }
    if (idChantier == null) {
      return;
    }
    
    // Chargement du chantier
    try {
      parametreChantier = sqlPrixVente.chargerChantier(idChantier);
      if (parametreChantier == null) {
        Trace.alerte("Aucun chantier n'a été trouvé avec l'identifiant " + idChantier.getTexte());
      }
    }
    catch (Exception e) {
      idChantier = null;
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Sélectionne les conditions de ventes à appliquer.
   */
  private void selectionnerConditionVente() {
    // Initialisation des listes qui vont contenir les conditions normales et quantitatives
    listeParametreConditionVenteNormale = new ListeParametreConditionVente();
    listeParametreConditionVenteQuantitative = new ListeParametreConditionVente();
    
    // Sélection des codes des conditions de ventes standard et promotionnelles
    determinerGroupeConditionStandardEtablissement();
    determinerCodeConditionStandard();
    determinerGroupeConditionPromotionnelle();
    determinerCodeConditionCentrale();
    
    // Sélection des conditions de ventes à appliquer parmi toutes celles qui ont été chargé
    selectionnerConditionVenteStandardEtablissement();
    selectionnerConditionVenteStandard();
    selectionnerConditionVentePromotionnelle();
    selectionnerConditionVenteClientFacture();
    selectionnerConditionVenteCentrale();
    
    // Les conditions cumulatives (cette information est défini dans le paramètre CN)
    extraireConditionCumulative();
    marquerConditionVenteNonCumulative();
    
    // Contrôle de l'existence d'une condition de ventes prioritaire (pour l'instant uniquement sur les normales - à confirmer avec Marc)
    controlerExistenceConditionVentePrioritaire();
    
    if (parametreConditionVentePrioritaire != null) {
      Trace.alerte("Une condition de ventes prioritaire a été trouvé.");
    }
    if (listeParametreConditionVenteNormale != null) {
      Trace.alerte("Nombre total de conditions de ventes normales trouvées : " + listeParametreConditionVenteNormale.size());
    }
    if (listeParametreConditionVenteQuantitative != null) {
      Trace.alerte("Nombre total de conditions de ventes quantitatives trouvées : " + listeParametreConditionVenteQuantitative.size());
    }
    if (listeParametreConditionVenteCumulative != null) {
      Trace.alerte("Nombre total de conditions de ventes cumulatives trouvées : " + listeParametreConditionVenteCumulative.size());
    }
  }
  
  /**
   * Déterminer la date d'application qui est obligatoire pour récupérer un tarif.
   * 
   * La plupart du temps la date d'application est la date du jour. Il peut arriver que le contexte de calcul nécessite d'utiliser
   * une date d'applciation spécifique. Dans ce cas, c'est à l'utilisateur du composant de calcul du prix de vente de déterminer
   * la date d'application à utiliser en fonction des règles de gestion qui sont propres à son contexte.
   * 
   * La date d'application peut être :
   * 1- La date d'application fournie par l'utilisateur du composant de calcul de prix de vente.
   * 2- La date du jour.
   */
  private void calculerDateApplication() {
    // Renseigner la date d'application avec la date du jour
    if (dateApplication == null) {
      dateApplication = new Date();
      parametreChargement.setDateApplication(dateApplication);
    }
  }
  
  /**
   * Calcule le code de la devise qui est obligatoire pour récupérer un tarif.
   */
  private void calculerDevise() {
    // Par ordre de priorité
    // Devise du document de ventes
    if (parametreDocumentVente != null && parametreDocumentVente.getCodeDevise() != null
        && !parametreDocumentVente.getCodeDevise().isEmpty()) {
      codeDevise = parametreDocumentVente.getCodeDevise();
      origineDevise = EnumOrigineDevise.DOCUMENT_VENTE;
    }
    // Devise du client
    else if (parametreClientFacture != null && parametreClientFacture.getCodeDevise() != null
        && !parametreClientFacture.getCodeDevise().isEmpty()) {
      codeDevise = parametreClientFacture.getCodeDevise();
      origineDevise = EnumOrigineDevise.CLIENT;
    }
    // Devise de l'établissement (description générale)
    else if (parametreEtablissement != null && parametreEtablissement.getCodeDevise() != null
        && !parametreEtablissement.getCodeDevise().isEmpty()) {
      codeDevise = parametreEtablissement.getCodeDevise();
      origineDevise = EnumOrigineDevise.ETABLISSEMENT;
    }
    // Sinon devise par défaut qui est l'Euro donc ""
    else {
      codeDevise = "";
      origineDevise = EnumOrigineDevise.DEFAUT;
    }
    
    // Traitement de la devise EUR qui va être effacée car c'est la devise par défaut
    if (codeDevise.equals("EUR")) {
      codeDevise = "";
    }
  }
  
  /**
   * Indique si les conditions de ventes normales peuvent être sélectionnées.
   * 
   * @return
   */
  private boolean isConditionVenteNormaleApplicable() {
    // Le paramètre de la ligne prédomine (L1IN19) s'il existe
    if (parametreLigneVente != null) {
      return parametreLigneVente.isConditionVenteNormaleApplicable();
    }
    
    // Sinon le paramètre du client (CLIN10)
    if (parametreClientFacture != null) {
      return parametreClientFacture.isConditionVenteNormaleApplicable();
    }
    
    // Dans tous les autres cas
    return true;
  }
  
  /**
   * Indique si les conditions de ventes quantitatives peuvent être sélectionnées.
   * 
   * @return
   */
  private boolean isConditionVenteQuantitativeApplicable() {
    // Le paramètre de la ligne prédomine (L1IN19) s'il existe
    if (parametreLigneVente != null) {
      return parametreLigneVente.isConditionVenteQuantitativeApplicable();
    }
    
    // Sinon le paramètre du client (CLIN10)
    if (parametreClientFacture != null) {
      return parametreClientFacture.isConditionVenteQuantitativeApplicable();
    }
    
    // Dans tous les autres cas
    return true;
  }
  
  /**
   * Déterminer si le groupe des conditions standards de l'établissement est à appliquer.
   * 
   * Cette information n'est pas stockée dansle document de ventes donc elle doit être déterminée systématiquement.
   * Le groupe de l'établissement est pris en compte mais peut être exclu sous certaines conditions.
   */
  private void determinerGroupeConditionStandardEtablissement() {
    idRattachementClientConditionVenteStandardEtablissement = null;
    
    // Si le client facturé exclu les conditions de ventes de l'établissement
    if (parametreClientFacture != null && parametreClientFacture
        .getRegleExclusionConditionVenteEtablissement() != EnumRegleExclusionConditionVenteEtablissement.SANS_EXCLUSION) {
      return;
    }
    
    // Sinon contrôle de la présence d'un groupe de conditions de ventes standards
    if (parametreEtablissement.getIdGroupeConditionVenteGenerale() != null) {
      idRattachementClientConditionVenteStandardEtablissement =
          IdRattachementClient.getInstanceAvecGroupe(idEtablissement, parametreEtablissement.getIdGroupeConditionVenteGenerale());
    }
  }
  
  /**
   * Déterminer les groupes des conditions standards ou le numéro client identifiant les conditions de ventes à appliquer.
   * 
   * Cette information est stockée dans le document de vente donc cette information est recherchée uniquement si le document n'existe pas.
   * Ce choix se fait entre le client livré et le client facturé. Attention l'ordre de sélection est important.
   */
  private void determinerCodeConditionStandard() {
    idRattachementClientConditionVenteStandard = null;
    origineConditionVenteStandard = null;
    
    // Le document de vente existe donc les conditions de ventes à utiliser vont être issues du document
    if (parametreDocumentVente != null && parametreDocumentVente.getIdDocumentVente() == null) {
      String codeConditionVente = Constantes.normerTexte(parametreDocumentVente.getCodeConditionVente());
      // Si aucun code n'est présent alors aucune condition n'a été renseigné
      if (codeConditionVente.isEmpty()) {
        return;
      }
      
      // Sinon
      idRattachementClientConditionVenteStandard = IdRattachementClient.getInstance(idEtablissement, codeConditionVente);
      
      // Est un numéro client ?
      if (idRattachementClientConditionVenteStandard.isClient()) {
        origineConditionVenteStandard = EnumOrigineConditionVenteStandard.CNV_CLIENT_DU_DOCUMENT_VENTE;
      }
      // Ou un groupe de conditions de ventes ?
      else {
        origineConditionVenteStandard = EnumOrigineConditionVenteStandard.GROUPE_CNV_DOCUMENT_VENTE;
      }
      return;
    }
    
    // Le document de vente n'existe pas, il faut récupérer cette information parmi les clients livré ou facturé
    // Utiliser le code condition de ventes standard du client livré ?
    if (idClientLivre != null && parametreClientLivre != null
        && !Constantes.normerTexte(parametreClientLivre.getCodeConditionVente()).isEmpty()) {
      String codeConditionVente = Constantes.normerTexte(parametreClientLivre.getCodeConditionVente());
      // S'il s'agit d'un numéro client, il est ignoré car seul le client facturé utilise le numéro client pour les CNV
      if (IdClient.isNumeroClient(codeConditionVente)) {
        return;
      }
      
      // Contrôle que le groupe condition de vente soit bien de type promotionnel
      idRattachementClientConditionVenteStandard = IdRattachementClient.getInstance(idEtablissement, codeConditionVente);
      origineConditionVenteStandard = EnumOrigineConditionVenteStandard.GROUPE_CNV_CLIENT_LIVRE;
      return;
    }
    
    // Sinon
    // Utiliser le code condition de ventes standard du client facturé ?
    if (idClientFacture != null && parametreClientFacture != null
        && !Constantes.normerTexte(parametreClientFacture.getCodeConditionVente()).isEmpty()) {
      String codeConditionVente = Constantes.normerTexte(parametreClientFacture.getCodeConditionVente());
      idRattachementClientConditionVenteStandard = IdRattachementClient.getInstance(idEtablissement, codeConditionVente);
      
      // S'il s'agit d'un numéro client
      if (idRattachementClientConditionVenteStandard.isClient()) {
        origineConditionVenteStandard = EnumOrigineConditionVenteStandard.CNV_CLIENT_DU_CLIENT_FACTURE;
      }
      // Ou un groupe de conditions de ventes ?
      else {
        origineConditionVenteStandard = EnumOrigineConditionVenteStandard.GROUPE_CNV_CLIENT_FACTURE;
      }
    }
  }
  
  /**
   * Déterminer le groupe de la condition promotionnelle à appliquer.
   * 
   * Cette information n'est pas stockée dans le document de ventes donc elle doit être déterminée systématiquement.
   * Ce choix se fait entre le client livré, le client facturé et l'établissement. Attention l'ordre de sélection est important.
   */
  private void determinerGroupeConditionPromotionnelle() {
    idRattachementClientConditionVentePromotionnelle = null;
    origineGroupeConditionVentePromotionnelle = null;
    
    // Contrôler la présence des groupes de conditions de ventes du paramètres 'CN'
    ListeGroupeConditionVente listeGroupeConditionVenteParametreCN = parametreEtablissement.getListeGroupeConditionVente();
    if (listeGroupeConditionVenteParametreCN == null || listeGroupeConditionVenteParametreCN.isEmpty()) {
      return;
    }
    
    // Utiliser le groupe des conditions de ventes promotionnelles du client livré ?
    if (idClientLivre != null && parametreClientLivre != null
        && !Constantes.normerTexte(parametreClientLivre.getCodeConditionVentePromo()).isEmpty()) {
      // Contrôle que le groupe condition de vente soit bien de type promotionnel
      IdGroupeConditionVente idGroupeConditionVente =
          IdGroupeConditionVente.getInstance(idEtablissement, parametreClientLivre.getCodeConditionVentePromo());
      GroupeConditionVente groupeConditionVente = listeGroupeConditionVenteParametreCN.get(idGroupeConditionVente);
      if (groupeConditionVente != null
          && groupeConditionVente.getTypeConditionVente() == EnumTypeGroupeConditionVente.CONDITION_PROMOTIONNELLE) {
        idRattachementClientConditionVentePromotionnelle =
            IdRattachementClient.getInstanceAvecGroupe(idEtablissement, idGroupeConditionVente);
        origineGroupeConditionVentePromotionnelle = EnumOrigineConditionVentePromotionnelle.GROUPE_CNV_CLIENT_LIVRE;
        return;
      }
    }
    
    // Utiliser le groupe des conditions de ventes promotionnelles du client facturé ?
    if (idClientFacture != null && parametreClientFacture != null
        && !Constantes.normerTexte(parametreClientFacture.getCodeConditionVentePromo()).isEmpty()) {
      // Contrôle que le groupe condition de vente soit bien de type promotionnel
      IdGroupeConditionVente idGroupeConditionVente =
          IdGroupeConditionVente.getInstance(idEtablissement, parametreClientFacture.getCodeConditionVentePromo());
      GroupeConditionVente groupeConditionVente = listeGroupeConditionVenteParametreCN.get(idGroupeConditionVente);
      if (groupeConditionVente != null
          && groupeConditionVente.getTypeConditionVente() == EnumTypeGroupeConditionVente.CONDITION_PROMOTIONNELLE) {
        idRattachementClientConditionVentePromotionnelle =
            IdRattachementClient.getInstanceAvecGroupe(idEtablissement, idGroupeConditionVente);
        origineGroupeConditionVentePromotionnelle = EnumOrigineConditionVentePromotionnelle.GROUPE_CNV_CLIENT_FACTURE;
        return;
      }
    }
    
    // Sinon utiliser le groupe des conditions de ventes promotionnelles de l'établissement (paramètre DG) si la client facturé n'exclut
    // pas son utilisation
    if (parametreEtablissement.getIdGroupeConditionVentePromotionnelle() != null && parametreClientFacture != null
        && parametreClientFacture
            .getRegleExclusionConditionVenteEtablissement() == EnumRegleExclusionConditionVenteEtablissement.SANS_EXCLUSION) {
      idRattachementClientConditionVentePromotionnelle =
          IdRattachementClient.getInstanceAvecGroupe(idEtablissement, parametreEtablissement.getIdGroupeConditionVentePromotionnelle());
      origineGroupeConditionVentePromotionnelle = EnumOrigineConditionVentePromotionnelle.GROUPE_CNV_ETABLISSEMENT;
      return;
    }
  }
  
  /**
   * Déterminer les groupes des conditions centrales ou le numéro client identifiant les conditions de ventes à appliquer.
   * 
   * Seule la centrale 1 est stockée dans le document de vente pour les 2 autres elles sont déterminées.
   * Ce choix se fait entre le client livré et le client facturé. Attention l'ordre de sélection est important.
   */
  private void determinerCodeConditionCentrale() {
    numeroClientPourCentrale1 = null;
    origineConditionVenteCentrale1 = null;
    numeroClientPourCentrale2 = null;
    origineConditionVenteCentrale2 = null;
    numeroClientPourCentrale3 = null;
    origineConditionVenteCentrale3 = null;
    
    // Contrôle que le PS216 soit activé
    if (parametreEtablissement.getPs216() != null && parametreEtablissement.getPs216().charValue() != '1') {
      return;
    }
    
    // Le document de vente existe donc les conditions de ventes pour la centrale 1 à utiliser va être issue du document
    if (parametreDocumentVente != null && parametreDocumentVente.getIdDocumentVente() == null) {
      // Si aucun code n'est présent alors aucune condition n'a été renseigné
      if (parametreDocumentVente.getNumeroClientCentrale1() == null) {
        return;
      }
      // Sinon
      // Est un numéro client ?
      if (IdClient.isNumeroClient(parametreDocumentVente.getNumeroClientCentrale1().toString())) {
        numeroClientPourCentrale1 = parametreDocumentVente.getNumeroClientCentrale1();
        origineConditionVenteCentrale1 = EnumOrigineConditionVenteCentrale.CNV_CLIENT_DU_DOCUMENT_VENTE;
      }
    }
    // Le document de vente n'existe pas, il faut récupérer cette information parmi les clients
    else {
      // Est ce issu du client livré ?
      if (idClientLivre != null && parametreClientLivre != null && parametreClientLivre.getNumeroCentraleAchat1() != null
          && parametreClientLivre.getNumeroCentraleAchat1() > 0) {
        numeroClientPourCentrale1 = parametreClientLivre.getNumeroCentraleAchat1();
        origineConditionVenteCentrale1 = EnumOrigineConditionVenteCentrale.CNV_CLIENT_DU_CLIENT_LIVRE;
      }
      // Est ce issu du client facturé ?
      else if (idClientFacture != null && parametreClientFacture != null && parametreClientFacture.getNumeroCentraleAchat1() != null
          && parametreClientFacture.getNumeroCentraleAchat1() > 0) {
        numeroClientPourCentrale1 = parametreClientFacture.getNumeroCentraleAchat1();
        origineConditionVenteCentrale1 = EnumOrigineConditionVenteCentrale.CNV_CLIENT_DU_CLIENT_FACTURE;
      }
    }
    
    // La centrale 2 est obligatoirement déduite
    // Est ce issu du client livré ?
    if (idClientLivre != null && parametreClientLivre != null && parametreClientLivre.getNumeroCentraleAchat2() != null
        && parametreClientLivre.getNumeroCentraleAchat2() > 0) {
      numeroClientPourCentrale2 = parametreClientLivre.getNumeroCentraleAchat2();
      origineConditionVenteCentrale2 = EnumOrigineConditionVenteCentrale.CNV_CLIENT_DU_CLIENT_LIVRE;
    }
    // Est ce issu du client facturé ?
    else if (idClientFacture != null && parametreClientFacture != null && parametreClientFacture.getNumeroCentraleAchat2() != null
        && parametreClientFacture.getNumeroCentraleAchat2() > 0) {
      numeroClientPourCentrale2 = parametreClientFacture.getNumeroCentraleAchat2();
      origineConditionVenteCentrale2 = EnumOrigineConditionVenteCentrale.CNV_CLIENT_DU_CLIENT_FACTURE;
    }
    
    // La centrale 3 est obligatoirement déduite et provient du client facturé
    else if (idClientFacture != null && parametreClientFacture != null && parametreClientFacture.getNumeroCentraleAchat3() != null
        && parametreClientFacture.getNumeroCentraleAchat3() > 0) {
      numeroClientPourCentrale3 = parametreClientFacture.getNumeroCentraleAchat3();
      origineConditionVenteCentrale3 = EnumOrigineConditionVenteCentrale.CNV_CLIENT_DU_CLIENT_FACTURE;
    }
  }
  
  /**
   * Sélectionner les conditions de ventes standards (normales et quantitatives) à partir de l'identifiant de groupe de l'établissement.
   * Son rôle est de sélectionner les conditions de ventes standards parmi la liste des conditions de ventes qui ont été chargé en base.
   */
  private void selectionnerConditionVenteStandardEtablissement() {
    // Contrôler l'existence de conditions de ventes
    if (listeTousParametreConditionVente == null) {
      return;
    }
    
    // Contrôler la présence de groupes de conditions de ventes dans le paramètre 'CN'
    ListeGroupeConditionVente listeGroupeConditionVenteParametreCN = parametreEtablissement.getListeGroupeConditionVente();
    if (listeGroupeConditionVenteParametreCN == null || listeGroupeConditionVenteParametreCN.isEmpty()) {
      return;
    }
    
    // Contrôler l'identifiant du groupe des conditions de ventes de l'établissement
    if (idRattachementClientConditionVenteStandardEtablissement == null) {
      return;
    }
    
    // Vérification de la validité du groupe des conditions de ventes par rapport à la liste des groupes chargés dans ce contexte depuis
    // les paramètres. Le contrôle du type n'est pas fait pour ce groupe
    if (!parametreEtablissement
        .isGroupeConditionValide(idRattachementClientConditionVenteStandardEtablissement.getIdGroupeConditionVente(), null)) {
      return;
    }
    
    // Les conditions normales (si le client n'exclut pas les conditions de ventes normales)
    if (isConditionVenteNormaleApplicable()) {
      ListeParametreConditionVente listeParametreConditionVente = listeTousParametreConditionVente.retournerListeConditionVente(
          idRattachementClientConditionVenteStandardEtablissement, EnumCategorieConditionVente.NORMALE, true, parametreEtablissement);
      listeParametreConditionVenteNormale.addAllSansDoublon(listeParametreConditionVente,
          EnumOrigineConditionVente.GROUPE_CNV_STANDARD_ETABLISSEMENT);
      afficherListeCNV("Liste CNV normales standard (Etablissement)", listeParametreConditionVente);
    }
    
    // Les conditions quantitatives (si le client n'exclut pas les conditions de ventes quantitatives)
    if (isConditionVenteQuantitativeApplicable()) {
      ListeParametreConditionVente listeParametreConditionVente =
          listeTousParametreConditionVente.retournerListeConditionVente(idRattachementClientConditionVenteStandardEtablissement,
              EnumCategorieConditionVente.QUANTITATIVE, true, parametreEtablissement);
      listeParametreConditionVenteQuantitative.addAllSansDoublon(listeParametreConditionVente,
          EnumOrigineConditionVente.GROUPE_CNV_STANDARD_ETABLISSEMENT);
      afficherListeCNV("Liste CNV quantitatives standard (Etablissement)", listeParametreConditionVente);
    }
  }
  
  /**
   * Sélectionner les conditions de ventes standards.
   */
  private void selectionnerConditionVenteStandard() {
    // Contrôler l'existence de conditions de ventes
    if (listeTousParametreConditionVente == null) {
      return;
    }
    
    // Contrôler l'existence d'un rattachement client
    if (idRattachementClientConditionVenteStandard == null) {
      return;
    }
    
    // Contrôler la présence de groupes de conditions de ventes dans le paramètre 'CN'
    ListeGroupeConditionVente listeGroupeConditionVenteParametreCN = parametreEtablissement.getListeGroupeConditionVente();
    if (listeGroupeConditionVenteParametreCN == null || listeGroupeConditionVenteParametreCN.isEmpty()) {
      return;
    }
    
    // Vérification de la validité du groupe des conditions de ventes par rapport à la liste des groupes chargés dans ce contexte depuis
    // les paramètres si l'identifiant du rattachement client désigne un groupe de condition de ventes
    if (idRattachementClientConditionVenteStandard.isGroupeClient()) {
      if (!parametreEtablissement.isGroupeConditionValide(idRattachementClientConditionVenteStandard.getIdGroupeConditionVente(), null)) {
        return;
      }
    }
    
    // Détermine l'origine de la condition de ventes en fonction de l'identifiant rattachment client
    EnumOrigineConditionVente origineConditionVente = EnumOrigineConditionVente.CNV_CLIENT_STANDARD;
    String libelleComplementairePourTrace = "(standard/numéro client)";
    if (idRattachementClientConditionVenteStandard.isGroupeClient()) {
      origineConditionVente = EnumOrigineConditionVente.GROUPE_CNV_STANDARD;
      libelleComplementairePourTrace = "(standard/groupe)";
    }
    
    // Les conditions normales (avec contrôle que le client n'exclut pas les conditions de ventes normales)
    if (isConditionVenteNormaleApplicable()) {
      ListeParametreConditionVente listeParametreConditionVente = listeTousParametreConditionVente.retournerListeConditionVente(
          idRattachementClientConditionVenteStandard, EnumCategorieConditionVente.NORMALE, true, parametreEtablissement);
      listeParametreConditionVenteNormale.addAllSansDoublon(listeParametreConditionVente, origineConditionVente);
      afficherListeCNV("Liste CNV normales " + libelleComplementairePourTrace, listeParametreConditionVente);
    }
    
    // Les conditions quantitatives (avec contrôle que le client n'exclut pas les conditions de ventes quantitatives)
    if (isConditionVenteQuantitativeApplicable()) {
      ListeParametreConditionVente listeParametreConditionVenteDocumentVenteQuantitative =
          listeTousParametreConditionVente.retournerListeConditionVente(idRattachementClientConditionVenteStandard,
              EnumCategorieConditionVente.QUANTITATIVE, true, parametreEtablissement);
      listeParametreConditionVenteQuantitative.addAllSansDoublon(listeParametreConditionVenteDocumentVenteQuantitative,
          origineConditionVente);
      afficherListeCNV("Liste CNV quantitatives " + libelleComplementairePourTrace,
          listeParametreConditionVenteDocumentVenteQuantitative);
    }
  }
  
  /**
   * Sélectionner les conditions de ventes promotionnelles (normale et quantitatives) à partir de l'identifiant de groupe déterminé.
   * Son rôle est de sélectionner les conditions de ventes promotionnelles parmi la liste des conditions de ventes qui ont été chargé en
   * base.
   */
  private void selectionnerConditionVentePromotionnelle() {
    // Contrôler l'existence de conditions de ventes
    if (listeTousParametreConditionVente == null) {
      return;
    }
    
    // Aucun identifiant de groupe de conditions de ventes promotionnelles
    if (idRattachementClientConditionVentePromotionnelle == null) {
      return;
    }
    
    // Contrôle la validité (date) du groupe de la condition de ventes promotionnelle qui a été déterminé auparavant
    if (!parametreEtablissement.isGroupeConditionValide(idRattachementClientConditionVentePromotionnelle.getIdGroupeConditionVente(),
        null)) {
      return;
    }
    
    // Les conditions normales (avec contrôle que le client n'exclut pas les conditions de ventes normales)
    if (isConditionVenteNormaleApplicable()) {
      ListeParametreConditionVente listeParametreConditionVentePromotionnelleNormale =
          listeTousParametreConditionVente.retournerListeConditionVente(idRattachementClientConditionVentePromotionnelle,
              EnumCategorieConditionVente.NORMALE, true, parametreEtablissement);
      listeParametreConditionVenteNormale.addAllSansDoublon(listeParametreConditionVentePromotionnelleNormale,
          EnumOrigineConditionVente.GROUPE_CNV_PROMOTIONNELLE);
      afficherListeCNV("Liste CNV normales (promotionnelles)", listeParametreConditionVentePromotionnelleNormale);
    }
    
    // Les conditions quantitatives (avec contrôle que le client n'exclut pas les conditions de ventes quantitatives)
    if (isConditionVenteQuantitativeApplicable()) {
      ListeParametreConditionVente listeParametreConditionVentePromotionnelleQuantitative =
          listeTousParametreConditionVente.retournerListeConditionVente(idRattachementClientConditionVentePromotionnelle,
              EnumCategorieConditionVente.QUANTITATIVE, true, parametreEtablissement);
      listeParametreConditionVenteQuantitative.addAllSansDoublon(listeParametreConditionVentePromotionnelleQuantitative,
          EnumOrigineConditionVente.GROUPE_CNV_PROMOTIONNELLE);
      afficherListeCNV("Liste CNV quantitatives (promotionnelles)", listeParametreConditionVentePromotionnelleQuantitative);
    }
  }
  
  /**
   * Sélectionner les conditions de ventes directement liées au client facturé (normale et quantitatives).
   */
  private void selectionnerConditionVenteClientFacture() {
    // Contrôle l'existence de conditions de ventes
    if (listeTousParametreConditionVente == null) {
      return;
    }
    
    // Contrôle l'identifiant du client facturé
    if (idClientFacture == null) {
      return;
    }
    
    // Création de l'identifiant rattachement pour le client facturé
    IdRattachementClient idRattachementClientFacture = IdRattachementClient.getInstanceAvecClient(idEtablissement, idClientFacture);
    
    // Contrôle que les conditons de vente liées au client facturé n'aient pas été déjà sélectionnées lors de la sélection des conditions
    // de ventes standards
    if (Constantes.equals(idRattachementClientConditionVenteStandard, idRattachementClientFacture)) {
      return;
    }
    
    // Les conditions normales (avec contrôle que le client n'exclut pas les conditions de ventes normales)
    if (isConditionVenteNormaleApplicable()) {
      ListeParametreConditionVente listeParametreConditionVenteClientNormale = listeTousParametreConditionVente
          .retournerListeConditionVente(idRattachementClientFacture, EnumCategorieConditionVente.NORMALE, true, parametreEtablissement);
      listeParametreConditionVenteNormale.addAllSansDoublon(listeParametreConditionVenteClientNormale,
          EnumOrigineConditionVente.CLIENT_FACTURE);
      afficherListeCNV("Liste CNV standard (client)", listeParametreConditionVenteClientNormale);
    }
    
    // Les conditions quantitatives (avec contrôle que le client n'exclut pas les conditions de ventes quantitatives)
    if (isConditionVenteQuantitativeApplicable()) {
      ListeParametreConditionVente listeParametreConditionVenteClientQuantitative =
          listeTousParametreConditionVente.retournerListeConditionVente(idRattachementClientFacture,
              EnumCategorieConditionVente.QUANTITATIVE, true, parametreEtablissement);
      listeParametreConditionVenteQuantitative.addAllSansDoublon(listeParametreConditionVenteClientQuantitative,
          EnumOrigineConditionVente.CLIENT_FACTURE);
      afficherListeCNV("Liste CNV quantitatives (client)", listeParametreConditionVenteClientQuantitative);
    }
  }
  
  /**
   * Sélectionne les conditions de ventes directement liées aux centrales du client (normale et quantitatives).
   */
  private void selectionnerConditionVenteCentrale() {
    // Contrôler l'existence de conditions de ventes
    if (listeTousParametreConditionVente == null) {
      return;
    }
    
    // Si aucune centrale d'achat est initialisée
    if (numeroClientPourCentrale1 == null && numeroClientPourCentrale2 == null && numeroClientPourCentrale3 == null) {
      return;
    }
    
    // Formate les numéros des centrales et les stocke dans une liste
    List<IdRattachementClient> listeCodeCentrale = new ArrayList<IdRattachementClient>();
    if (numeroClientPourCentrale1 != null) {
      listeCodeCentrale.add(IdRattachementClient.getInstance(parametreEtablissement.getIdEtablissement(),
          String.format("%06d", numeroClientPourCentrale1)));
    }
    if (numeroClientPourCentrale2 != null) {
      listeCodeCentrale.add(IdRattachementClient.getInstance(parametreEtablissement.getIdEtablissement(),
          String.format("%06d", numeroClientPourCentrale2)));
    }
    if (numeroClientPourCentrale3 != null) {
      listeCodeCentrale.add(IdRattachementClient.getInstance(parametreEtablissement.getIdEtablissement(),
          String.format("%06d", numeroClientPourCentrale3)));
    }
    
    // Les conditions normales (avec contrôle que le client n'exclut pas les conditions de ventes normales)
    if (isConditionVenteNormaleApplicable()) {
      ListeParametreConditionVente listeParametreConditionVente = listeTousParametreConditionVente
          .retournerListeConditionVente(listeCodeCentrale, EnumCategorieConditionVente.NORMALE, true, parametreEtablissement);
      listeParametreConditionVenteNormale.addAllSansDoublon(listeParametreConditionVente, EnumOrigineConditionVente.CENTRALE);
      afficherListeCNV("Liste CNV normales (centrale)", listeParametreConditionVente);
    }
    
    // Les conditions quantitatives (avec contrôle que le client n'exclut pas les conditions de ventes quantitatives)
    if (isConditionVenteQuantitativeApplicable()) {
      ListeParametreConditionVente listeParametreConditionVenteCentraleQuantitative = listeTousParametreConditionVente
          .retournerListeConditionVente(listeCodeCentrale, EnumCategorieConditionVente.QUANTITATIVE, true, parametreEtablissement);
      listeParametreConditionVenteQuantitative.addAllSansDoublon(listeParametreConditionVenteCentraleQuantitative,
          EnumOrigineConditionVente.CENTRALE);
      afficherListeCNV("Liste CNV quantitatives (centrale)", listeParametreConditionVenteCentraleQuantitative);
    }
  }
  
  /**
   * Extrait les conditions cumulatives des listes des conditions normales et quantitatives.
   * C'est grâce au paramètre CN qu'une condition de ventes peut être définit comme cumulative ou non.
   */
  private void extraireConditionCumulative() {
    ListeGroupeConditionVente listeGroupeConditionVente = parametreEtablissement.getListeGroupeConditionVente();
    if (listeGroupeConditionVente == null || listeGroupeConditionVente.isEmpty()) {
      return;
    }
    
    // Extraction depuis les conditions normales
    if (listeParametreConditionVenteNormale != null && !listeParametreConditionVenteNormale.isEmpty()) {
      listeParametreConditionVenteCumulative = listeParametreConditionVenteNormale.extraireConditionVenteCumulative(idEtablissement,
          listeGroupeConditionVente, listeParametreConditionVenteCumulative);
    }
    
    // Extraction depuis les quantitatives
    if (listeParametreConditionVenteQuantitative != null && !listeParametreConditionVenteQuantitative.isEmpty()) {
      listeParametreConditionVenteCumulative = listeParametreConditionVenteQuantitative.extraireConditionVenteCumulative(idEtablissement,
          listeGroupeConditionVente, listeParametreConditionVenteCumulative);
    }
  }
  
  /**
   * Marque les conditions de ventes normales et quantitatives comme non cumulatives après l'extraction des conditions de ventes
   * cumulatives.
   */
  private void marquerConditionVenteNonCumulative() {
    // Les conditions de ventes normales
    if (listeParametreConditionVenteNormale != null && !listeParametreConditionVenteNormale.isEmpty()) {
      for (ParametreConditionVente parametreConditionVente : listeParametreConditionVenteNormale) {
        if (parametreConditionVente.getCumulative() == null) {
          parametreConditionVente.setCumulative(false);
        }
      }
    }
    
    // Les conditions de ventes quantitatives
    if (listeParametreConditionVenteQuantitative != null && !listeParametreConditionVenteQuantitative.isEmpty()) {
      for (ParametreConditionVente parametreConditionVente : listeParametreConditionVenteQuantitative) {
        if (parametreConditionVente.getCumulative() == null) {
          parametreConditionVente.setCumulative(false);
        }
      }
    }
  }
  
  /**
   * Contrôle l'existence d'une condition de ventes prioritaire.
   */
  private void controlerExistenceConditionVentePrioritaire() {
    if (!parametreEtablissement.isConditionVenteClientArticlePourPrixNetPrioritaire() || listeParametreConditionVenteNormale == null
        || idClientFacture == null) {
      return;
    }
    String numeroClient = idClientFacture.getIndicatif(IdClient.INDICATIF_NUM);
    
    // Parcours de la liste des conditions normales
    for (ParametreConditionVente parametreConditionVente : listeParametreConditionVenteNormale) {
      // Si la condition de ventes n'est pas de type "Prix net"
      if (!parametreConditionVente.isTypePrixNet()) {
        continue;
      }
      // Si le rattachement client ne concerne une condition de ventes client, la condition de ventes est ignorée
      if (!parametreConditionVente.isConditionVenteClient()) {
        continue;
      }
      // Si le rattachement article ne concerne pas un article, la condition de ventes est ignorée
      IdRattachementArticle idRattachementArticle = parametreConditionVente.getIdRattachementArticle();
      if (idRattachementArticle == null || idRattachementArticle.getTypeRattachement() != EnumTypeRattachementArticle.ARTICLE) {
        continue;
      }
      // La condition de ventes est de type "Prix net" sur le couple client / article alors elle est prioritaire sur toutes les autres
      if (parametreConditionVente.getIdRattachementClient().getCode().equals(numeroClient)
          && idRattachementArticle.getCodeRattachement().equals(parametreArticle.getCodeArticle())) {
        parametreConditionVentePrioritaire = parametreConditionVente;
        break;
      }
    }
  }
  
  /**
   * Afficher dans les traces pour le débuggage.
   * @param pTitre
   * @param pListe
   */
  private void afficherListeCNV(String pTitre, ListeParametreConditionVente pListe) {
    if (pListe == null) {
      return;
    }
    
    Trace.alerte("--> " + pTitre + " " + pListe.size());
    for (ParametreConditionVente p : pListe) {
      Trace.alerte("---> cnv:" + p.getIdRattachementClient().getCode() + " / " + p.getTexte() + " cat:" + p.getCategorie() + "  rat:"
          + p.getIdRattachementArticle().getCodeRattachement() + " tra:" + p.getIdRattachementArticle().getTypeRattachement());
    }
  }
}
