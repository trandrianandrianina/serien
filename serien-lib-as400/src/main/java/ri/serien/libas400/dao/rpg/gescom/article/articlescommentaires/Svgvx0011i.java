/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlescommentaires;

import java.beans.PropertyVetoException;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvx0011i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PILB1 = 30;
  public static final int SIZE_PILB2 = 30;
  public static final int SIZE_PILB3 = 30;
  public static final int SIZE_PILB4 = 30;
  public static final int SIZE_PIEDT1 = 1;
  public static final int SIZE_PIEDT2 = 1;
  public static final int SIZE_PIEDT3 = 1;
  public static final int SIZE_PIEDT4 = 1;
  public static final int SIZE_PIEDT5 = 1;
  public static final int SIZE_PIEDT6 = 1;
  public static final int SIZE_TOTALE_DS = 159;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PIART = 2;
  public static final int VAR_PILB1 = 3;
  public static final int VAR_PILB2 = 4;
  public static final int VAR_PILB3 = 5;
  public static final int VAR_PILB4 = 6;
  public static final int VAR_PIEDT1 = 7;
  public static final int VAR_PIEDT2 = 8;
  public static final int VAR_PIEDT3 = 9;
  public static final int VAR_PIEDT4 = 10;
  public static final int VAR_PIEDT5 = 11;
  public static final int VAR_PIEDT6 = 12;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code Etablissement
  private String piart = ""; // Code article
  private String pilb1 = ""; // Libelle n° 1
  private String pilb2 = ""; // Libelle n° 2
  private String pilb3 = ""; // Libellé n° 3
  private String pilb4 = ""; // Libellé n° 4
  private String piedt1 = ""; // Edition sur Bon de prépa
  private String piedt2 = ""; // Accusé de Réception de cde
  private String piedt3 = ""; // Bon d"expédition
  private String piedt4 = ""; // Bon transporteur
  private String piedt5 = ""; // Facture
  private String piedt6 = ""; // Devis
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code Etablissement
      new AS400Text(SIZE_PIART), // Code article
      new AS400Text(SIZE_PILB1), // Libelle n° 1
      new AS400Text(SIZE_PILB2), // Libelle n° 2
      new AS400Text(SIZE_PILB3), // Libellé n° 3
      new AS400Text(SIZE_PILB4), // Libellé n° 4
      new AS400Text(SIZE_PIEDT1), // Edition sur Bon de prépa
      new AS400Text(SIZE_PIEDT2), // Accusé de Réception de cde
      new AS400Text(SIZE_PIEDT3), // Bon d"expédition
      new AS400Text(SIZE_PIEDT4), // Bon transporteur
      new AS400Text(SIZE_PIEDT5), // Facture
      new AS400Text(SIZE_PIEDT6), // Devis
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pietb, piart, pilb1, pilb2, pilb3, pilb4, piedt1, piedt2, piedt3, piedt4, piedt5, piedt6, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    piart = (String) output[2];
    pilb1 = (String) output[3];
    pilb2 = (String) output[4];
    pilb3 = (String) output[5];
    pilb4 = (String) output[6];
    piedt1 = (String) output[7];
    piedt2 = (String) output[8];
    piedt3 = (String) output[9];
    piedt4 = (String) output[10];
    piedt5 = (String) output[11];
    piedt6 = (String) output[12];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPiart(String pPiart) {
    if (pPiart == null) {
      return;
    }
    piart = pPiart;
  }
  
  public String getPiart() {
    return piart;
  }
  
  public void setPilb1(String pPilb1) {
    if (pPilb1 == null) {
      return;
    }
    pilb1 = pPilb1;
  }
  
  public String getPilb1() {
    return pilb1;
  }
  
  public void setPilb2(String pPilb2) {
    if (pPilb2 == null) {
      return;
    }
    pilb2 = pPilb2;
  }
  
  public String getPilb2() {
    return pilb2;
  }
  
  public void setPilb3(String pPilb3) {
    if (pPilb3 == null) {
      return;
    }
    pilb3 = pPilb3;
  }
  
  public String getPilb3() {
    return pilb3;
  }
  
  public void setPilb4(String pPilb4) {
    if (pPilb4 == null) {
      return;
    }
    pilb4 = pPilb4;
  }
  
  public String getPilb4() {
    return pilb4;
  }
  
  public void setPiedt1(Character pPiedt1) {
    if (pPiedt1 == null) {
      return;
    }
    piedt1 = String.valueOf(pPiedt1);
  }
  
  public Character getPiedt1() {
    return piedt1.charAt(0);
  }
  
  public void setPiedt2(Character pPiedt2) {
    if (pPiedt2 == null) {
      return;
    }
    piedt2 = String.valueOf(pPiedt2);
  }
  
  public Character getPiedt2() {
    return piedt2.charAt(0);
  }
  
  public void setPiedt3(Character pPiedt3) {
    if (pPiedt3 == null) {
      return;
    }
    piedt3 = String.valueOf(pPiedt3);
  }
  
  public Character getPiedt3() {
    return piedt3.charAt(0);
  }
  
  public void setPiedt4(Character pPiedt4) {
    if (pPiedt4 == null) {
      return;
    }
    piedt4 = String.valueOf(pPiedt4);
  }
  
  public Character getPiedt4() {
    return piedt4.charAt(0);
  }
  
  public void setPiedt5(Character pPiedt5) {
    if (pPiedt5 == null) {
      return;
    }
    piedt5 = String.valueOf(pPiedt5);
  }
  
  public Character getPiedt5() {
    return piedt5.charAt(0);
  }
  
  public void setPiedt6(Character pPiedt6) {
    if (pPiedt6 == null) {
      return;
    }
    piedt6 = String.valueOf(pPiedt6);
  }
  
  public Character getPiedt6() {
    return piedt6.charAt(0);
  }
}
