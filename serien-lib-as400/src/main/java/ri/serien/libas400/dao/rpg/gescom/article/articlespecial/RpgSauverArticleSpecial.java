/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlespecial;

import java.math.BigDecimal;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgSauverArticleSpecial extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0043";
  private static final int NEGOCATION_ACHAT = 1;
  
  /**
   * Appel du programme RPG qui va créer un article spécial.
   */
  public Article sauverArticleSpecial(SystemeManager pSysteme, Article pArticle, Client pClient, BigDecimal pPrixNetHT,
      ConditionAchat pCNA) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pArticle == null) {
      throw new MessageErreurException("Le paramètre pArticle du service est à null.");
    }
    if (pClient == null) {
      throw new MessageErreurException("Le paramètre pClient du service est à null.");
    }
    if (pPrixNetHT == null) {
      throw new MessageErreurException("Le prix net HT est invalide.");
    }
    if (pCNA == null) {
      throw new MessageErreurException("Le paramètre pCNA du service est à null.");
    }
    IdFournisseur idFournisseur = IdFournisseur.controlerId(pArticle.getIdFournisseur(), true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0043i entree = new Svgvm0043i();
    Svgvm0043o sortie = new Svgvm0043o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pArticle.getId().getCodeEtablissement()); // Code établissement
    entree.setPicli(pClient.getId().getNumero()); // Numéro client
    entree.setPiliv(pClient.getId().getSuffixe()); // Suffixe client
    if (pArticle.getIdFamille() != null) {
      entree.setPifam(pArticle.getIdFamille().getCode()); // Groupe/Famille
    }
    if (pArticle.getIdSousFamille() != null) {
      entree.setPisfa(pArticle.getIdSousFamille().getCode()); // sous-famille
    }
    entree.setPili1(pArticle.getLibelle1() + pArticle.getLibelle2()); // Libellé 1
    entree.setPili2(pArticle.getLibelle3() + pArticle.getLibelle4()); // Libellé 2
    // Unité de vente
    if (pArticle.getIdUV() != null) {
      entree.setPiunv(pArticle.getIdUV().getCode());
    }
    // Nombre d'UV par UCV
    entree.setPicnd(pArticle.getNombreUVParUCV());
    
    // Unité de conditionnement
    if (pArticle.getIdUCV() != null) {
      entree.setPiunl(pArticle.getIdUCV().getCode());
    }
    // Prix de Vente
    entree.setPipxv(pPrixNetHT);
    entree.setPicof(idFournisseur.getCollectif()); // Collectif fournisseur
    entree.setPifrs(idFournisseur.getNumero()); // Numéro fournisseur
    // Prix de revient standard
    entree.setPiprs(pArticle.getPrixDeRevientStandardHT());
    entree.setPinga(NEGOCATION_ACHAT); // Indicateur Négociation achat
    entree.setPipra(pCNA.getPrixAchatBrutHT());
    entree.setPirem1(pCNA.getPourcentageRemise1()); // Remise 1
    entree.setPirem2(pCNA.getPourcentageRemise2()); // Remise 2
    entree.setPirem3(pCNA.getPourcentageRemise3()); // Remise 3
    entree.setPirem4(pCNA.getPourcentageRemise4()); // Remise 4
    // Coefficient ou le poids pour le port
    entree.setPifk1(pCNA.getPoidsPort());
    entree.setPifp1(pCNA.getPourcentagePort());
    // Montant frais de port
    entree.setPifv1(pCNA.getMontantAuPoidsPortHT());
    // Taxe ou majoration en %
    entree.setPifk3(pCNA.getPourcentageTaxeOuMajoration());
    // Montant Conditionnement
    entree.setPifv3(pCNA.getMontantConditionnement());
    // Frais d'exploitation
    entree.setPifp2(pCNA.getFraisExploitation());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      return null;
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Retourner l'identifiant de l'article
    IdArticle idArticle = IdArticle.getInstance(pArticle.getId().getIdEtablissement(), sortie.getPoart());
    pArticle.setId(idArticle);
    return pArticle;
  }
}
