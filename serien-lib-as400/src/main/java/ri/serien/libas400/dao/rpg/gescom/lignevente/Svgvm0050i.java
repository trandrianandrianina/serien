/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0050i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_L1COD = 1;
  public static final int SIZE_L1ETB = 3;
  public static final int SIZE_L1NUM = 6;
  public static final int DECIMAL_L1NUM = 0;
  public static final int SIZE_L1SUF = 1;
  public static final int DECIMAL_L1SUF = 0;
  public static final int SIZE_L1NLI = 4;
  public static final int DECIMAL_L1NLI = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 26;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_L1COD = 1;
  public static final int VAR_L1ETB = 2;
  public static final int VAR_L1NUM = 3;
  public static final int VAR_L1SUF = 4;
  public static final int VAR_L1NLI = 5;
  public static final int VAR_PIARR = 6;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String l1cod = ""; // Code ERL "E"
  private String l1etb = ""; // Code Etablissement
  private BigDecimal l1num = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal l1suf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal l1nli = BigDecimal.ZERO; // Numéro de Ligne
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_L1COD), // Code ERL "E"
      new AS400Text(SIZE_L1ETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_L1NUM, DECIMAL_L1NUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_L1SUF, DECIMAL_L1SUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_L1NLI, DECIMAL_L1NLI), // Numéro de Ligne
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, l1cod, l1etb, l1num, l1suf, l1nli, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    l1cod = (String) output[1];
    l1etb = (String) output[2];
    l1num = (BigDecimal) output[3];
    l1suf = (BigDecimal) output[4];
    l1nli = (BigDecimal) output[5];
    piarr = (String) output[6];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setL1cod(Character pL1cod) {
    if (pL1cod == null) {
      return;
    }
    l1cod = String.valueOf(pL1cod);
  }
  
  public Character getL1cod() {
    return l1cod.charAt(0);
  }
  
  public void setL1etb(String pL1etb) {
    if (pL1etb == null) {
      return;
    }
    l1etb = pL1etb;
  }
  
  public String getL1etb() {
    return l1etb;
  }
  
  public void setL1num(BigDecimal pL1num) {
    if (pL1num == null) {
      return;
    }
    l1num = pL1num.setScale(DECIMAL_L1NUM, RoundingMode.HALF_UP);
  }
  
  public void setL1num(Integer pL1num) {
    if (pL1num == null) {
      return;
    }
    l1num = BigDecimal.valueOf(pL1num);
  }
  
  public Integer getL1num() {
    return l1num.intValue();
  }
  
  public void setL1suf(BigDecimal pL1suf) {
    if (pL1suf == null) {
      return;
    }
    l1suf = pL1suf.setScale(DECIMAL_L1SUF, RoundingMode.HALF_UP);
  }
  
  public void setL1suf(Integer pL1suf) {
    if (pL1suf == null) {
      return;
    }
    l1suf = BigDecimal.valueOf(pL1suf);
  }
  
  public Integer getL1suf() {
    return l1suf.intValue();
  }
  
  public void setL1nli(BigDecimal pL1nli) {
    if (pL1nli == null) {
      return;
    }
    l1nli = pL1nli.setScale(DECIMAL_L1NLI, RoundingMode.HALF_UP);
  }
  
  public void setL1nli(Integer pL1nli) {
    if (pL1nli == null) {
      return;
    }
    l1nli = BigDecimal.valueOf(pL1nli);
  }
  
  public Integer getL1nli() {
    return l1nli.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
