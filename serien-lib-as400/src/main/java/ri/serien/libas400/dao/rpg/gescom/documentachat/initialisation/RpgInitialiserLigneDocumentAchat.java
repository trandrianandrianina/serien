/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.initialisation;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeExtractionDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.prix.ParametrePrixAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.document.ListeZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

public class RpgInitialiserLigneDocumentAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGAM0003";
  
  /**
   * Appel du programme RPG qui va récupérer les infos d'une ligne dans un document.
   */
  public LigneAchatArticle initialiserLigneArticleDocument(SystemeManager pSysteme, LigneAchatArticle pLigneArticle,
      Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pLigneArticle == null) {
      throw new MessageErreurException("Le paramètre pLigneArticle du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0003i entree = new Svgam0003i();
    Svgam0003o sortie = new Svgam0003o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    // Trace.info("idLigne=" + pLigneArticle.getId());
    entree.setPietb(pLigneArticle.getId().getCodeEtablissement());
    entree.setPicod(pLigneArticle.getId().getCodeEntete().getCode());
    entree.setPinum(pLigneArticle.getId().getNumero());
    entree.setPisuf(pLigneArticle.getId().getSuffixe());
    entree.setPinli(pLigneArticle.getId().getNumeroLigne());
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPiopt(LigneVente.OPTION_VALIDATION);
    if (pLigneArticle.getIdArticle() != null) {
      entree.setPiart(pLigneArticle.getIdArticle().getCodeArticle());
    }
    // Informations de la ligne de vente si celle-ci est renseignée
    IdLigneVente idLigneVente = pLigneArticle.getIdLigneVente();
    if (idLigneVente != null && idLigneVente.isExistant()) {
      entree.setPitos('V');
      entree.setPicos('E');
      entree.setPinos(idLigneVente.getNumero());
      entree.setPisos(idLigneVente.getSuffixe());
      entree.setPilos(idLigneVente.getNumeroLigne());
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      controlerErreur();
      
      // Initialisation de la classe métier
      // Entrée
      // Sortie
      int numeroLigne = 0;
      if (pLigneArticle.getId() != null && pLigneArticle.getId().isExistant()) {
        numeroLigne = pLigneArticle.getId().getNumeroLigne();
      }
      else {
        numeroLigne = sortie.getLonli();
      }
      IdLigneAchat idLigneDocument = IdLigneAchat.getInstance(pLigneArticle.getId().getIdDocumentAchat(), numeroLigne);
      pLigneArticle.setId(idLigneDocument);
      if (sortie.getPowser() == 'L') {
        pLigneArticle.setOptionSaisieLot(true);
      }
      else {
        pLigneArticle.setOptionSaisieLot(false);
      }
      if (sortie.getPowser() == 'S') {
        pLigneArticle.setOptionSaisieNumeroSerie(true);
      }
      else {
        pLigneArticle.setOptionSaisieNumeroSerie(false);
      }
      
      pLigneArticle.setCodeEtat(sortie.getLotop());
      pLigneArticle.setTypeLigne(EnumTypeLigneAchat.valueOfByCode(sortie.getLoerl()));
      pLigneArticle.setCodeExtraction(EnumCodeExtractionDocumentAchat.valueOfByCode(sortie.getLocex()));
      pLigneArticle.setQuantiteExtraiteUV(sortie.getLoqcx());
      pLigneArticle.setCodeTVA(sortie.getLotva());
      pLigneArticle.setNumeroColonneTVA(sortie.getLocol());
      pLigneArticle.setSigneLigne(sortie.getLosgn());
      pLigneArticle.setCodeAvoir(sortie.getLoavr());
      IdArticle idArticle = IdArticle.getInstance(pLigneArticle.getId().getIdEtablissement(), sortie.getLoart());
      pLigneArticle.setIdArticle(idArticle);
      if (!sortie.getLomag().trim().isEmpty()) {
        pLigneArticle.setIdMagasin(IdMagasin.getInstance(pLigneArticle.getId().getIdEtablissement(), sortie.getLomag()));
      }
      if (!sortie.getLomaga().trim().isEmpty()) {
        pLigneArticle.setIdMagasinAvantModif(IdMagasin.getInstance(pLigneArticle.getId().getIdEtablissement(), sortie.getLomaga()));
      }
      // Le rapprochement est toujours lié à son code Entete opposé : Facture avec un bon et un bon avec une facture
      EnumCodeEnteteDocumentAchat codeEntete;
      if (pLigneArticle.getId().getCodeEntete() == EnumCodeEnteteDocumentAchat.FACTURE) {
        codeEntete = EnumCodeEnteteDocumentAchat.FACTURE;
      }
      else {
        codeEntete = EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION;
      }
      if (sortie.getLonumr() > 0) {
        IdLigneAchat idLigneRapprochee = IdLigneAchat.getInstance(pLigneArticle.getId().getIdEtablissement(), codeEntete,
            sortie.getLonumr(), sortie.getLosufr(), sortie.getLonlir());
        pLigneArticle.setIdLigneRapprochee(idLigneRapprochee);
      }
      pLigneArticle.setDateHistoriqueStock(ConvertDate.db2ToDate(sortie.getLodath(), null));
      pLigneArticle.setOrdreHistoriqueStock(sortie.getLoordh());
      pLigneArticle.setCodeSectionAnalytique(sortie.getLosan());
      pLigneArticle.setCodeAffaire(sortie.getLoact());
      pLigneArticle.setCodeNatureAchat(sortie.getLonat());
      pLigneArticle.setMontantAffecte(sortie.getLomta());
      pLigneArticle.setDateLivraisonPrevue(sortie.getLodlpConvertiEnDate());
      pLigneArticle.setEtatSaisieNumeroSerieOuLot(sortie.getLoser());
      if (pLigneArticle.getListeTopPersonnalisable() == null) {
        pLigneArticle.setListeTopPersonnalisable(new ListeZonePersonnalisee());
      }
      ListeZonePersonnalisee liste = pLigneArticle.getListeTopPersonnalisable();
      liste.setTopPersonnalisation1(sortie.getLotp1());
      liste.setTopPersonnalisation2(sortie.getLotp2());
      liste.setTopPersonnalisation3(sortie.getLotp3());
      liste.setTopPersonnalisation4(sortie.getLotp4());
      liste.setTopPersonnalisation5(sortie.getLotp5());
      pLigneArticle.setImputationFrais(sortie.getLoin1());
      pLigneArticle.setTypeFrais(sortie.getLoin2());
      pLigneArticle.setRegroupementArticle(sortie.getLoin3());
      pLigneArticle.setComptabilisationStockFlottant(sortie.getLoin4());
      pLigneArticle.setDateLivraisonCalculee(sortie.getLodlcConvertiEnDate());
      pLigneArticle.setTypeArticle(EnumTypeArticle.valueOfByCodeAlpha(sortie.getWtypart()));
      pLigneArticle.setCodeNatureAnalytique(sortie.getPonatan());
      
      // Alimentation de la classe ParamètrePrixAchat avec les données retournées
      if (!pLigneArticle.isLigneCommentaire()) {
        ParametrePrixAchat parametre = new ParametrePrixAchat();
        if (!sortie.getLounc().trim().isEmpty()) {
          IdUnite idUCA = IdUnite.getInstance(sortie.getLounc());
          parametre.setIdUCA(idUCA);
        }
        
        // Renseigner les unités
        if (!sortie.getLouna().trim().isEmpty()) {
          IdUnite idUA = IdUnite.getInstance(sortie.getLouna());
          parametre.setIdUA(idUA);
        }
        if (!sortie.getLounc().trim().isEmpty()) {
          IdUnite idUCA = IdUnite.getInstance(sortie.getLounc());
          parametre.setIdUCA(idUCA);
        }
        parametre.setNombreDecimaleUA(sortie.getLodca());
        parametre.setNombreDecimaleUCA(sortie.getLodcc());
        parametre.setNombreDecimaleUS(sortie.getLodcs());
        
        // Renseigner les ratios
        parametre.setNombreUAParUCA(sortie.getLokac());
        parametre.setNombreUSParUCA(sortie.getLoksc());
        
        // Renseigner les quantités
        parametre.setQuantiteReliquatUA(sortie.getLoqta());
        parametre.setQuantiteReliquatUCA(sortie.getLoqtc());
        parametre.setQuantiteReliquatUS(sortie.getLoqts());
        // Renseigner les quantités initiales
        parametre.setQuantiteInitialeUA(sortie.getLoqtai());
        parametre.setQuantiteInitialeUCA(sortie.getLoqtci());
        parametre.setQuantiteInitialeUS(sortie.getLoqtsi());
        // Renseigner les quantités traitées
        parametre.setQuantiteTraiteeUA(sortie.getLoqtat());
        parametre.setQuantiteTraiteeUCA(sortie.getLoqtct());
        parametre.setQuantiteTraiteeUS(sortie.getLoqtst());
        
        // Renseigner les montants
        parametre.setPrixAchatBrutHT(sortie.getLopab());
        ListeRemise listeRemise = parametre.getListeRemise();
        listeRemise.setPourcentageRemise1(sortie.getLorem1());
        listeRemise.setPourcentageRemise2(sortie.getLorem2());
        listeRemise.setPourcentageRemise3(sortie.getLorem3());
        listeRemise.setPourcentageRemise4(sortie.getLorem4());
        listeRemise.setPourcentageRemise5(sortie.getLorem5());
        listeRemise.setPourcentageRemise6(sortie.getLorem6());
        listeRemise.setTypeRemise(sortie.getLotrl());
        listeRemise.setBaseRemise(sortie.getLobrl());
        parametre.setPourcentageTaxeOuMajoration(sortie.getPolfk3());
        parametre.setMontantConditionnement(sortie.getPolfv3());
        parametre.setPrixNetHT(sortie.getLopan());
        parametre.setPrixCalculeHT(sortie.getLopac());
        parametre.setMontantAuPoidsPortHT(sortie.getPolfv1());
        parametre.setPoidsPort(sortie.getPolfk1());
        parametre.setPourcentagePort(new BigPercentage(sortie.getPolfp1(), EnumFormatPourcentage.POURCENTAGE));
        parametre.setMontantPortHT(sortie.getWportf());
        parametre.setPrixRevientFournisseurHT(sortie.getWprvfr());
        parametre.setMontantReliquatHT(sortie.getLomht());
        parametre.setMontantInitialHT(sortie.getLomhti());
        parametre.setMontantTraiteHT(sortie.getLomhtt());
        
        // Renseigner les exclusions de remises de pied
        Character[] exlusionremise = parametre.getExclusionRemisePied();
        exlusionremise[0] = sortie.getLorp1();
        exlusionremise[1] = sortie.getLorp2();
        exlusionremise[2] = sortie.getLorp3();
        exlusionremise[3] = sortie.getLorp4();
        exlusionremise[4] = sortie.getLorp5();
        exlusionremise[5] = sortie.getLorp6();
        
        // Renseigner les indicateurs
        parametre.setTopPrixBaseSaisie(sortie.getLotb());
        parametre.setTopRemiseSaisie(sortie.getLotr());
        parametre.setTopPrixNetSaisi(sortie.getLotn());
        parametre.setTopUniteSaisie(sortie.getLotu());
        parametre.setTopQuantiteSaisie(sortie.getLotq());
        parametre.setTopMontantHTSaisi(sortie.getLoth());
        
        // Renseigner les éléments de TVA
        parametre.setCodeTVA(sortie.getLotva());
        parametre.setColonneTVA(sortie.getLocol());
        
        // Remettre l'information concernant la ligne de vente rattachée s'il y en a une
        pLigneArticle.setIdLigneVente(idLigneVente);
        
        // Mettre à jour l'article
        if (pLigneArticle.getPrixAchat() == null) {
          pLigneArticle.setPrixAchat(new PrixAchat());
        }
        pLigneArticle.getPrixAchat().initialiser(parametre);
      }
    }
    return pLigneArticle;
  }
}
