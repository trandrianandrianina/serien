/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlescommentaires;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgChargerArticleCommentaire extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0012";
  private static final int NEGOCATION_ACHAT = 1;
  private Article article = null;
  
  /**
   * Appel du programme RPG qui va écrire une négociation.
   */
  public Article chargerArticleCommentaire(SystemeManager pSysteme, Article pArticle) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pArticle == null) {
      throw new MessageErreurException("Le paramètre pArticle du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvx0012i entree = new Svgvx0012i();
    Svgvx0012o sortie = new Svgvx0012o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pArticle.getId().getCodeEtablissement()); // Code établissement
    entree.setPiart(pArticle.getId().getCodeArticle()); // Code article
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
    
    // Initialisation de la classe métier
    article = new Article(pArticle.getId());
    article.setLibelle1(sortie.getPolb1());
    article.setLibelle2(sortie.getPolb2());
    article.setLibelle3(sortie.getPolb3());
    article.setLibelle4(sortie.getPolb4());
    article.setTopEditionBonPreparation(!Character.isSpaceChar(sortie.getPoedt1()));
    article.setTopAccuseReceptionCommande(!Character.isSpaceChar(sortie.getPoedt2()));
    article.setTopBonExpedition(!Character.isSpaceChar(sortie.getPoedt3()));
    article.setTopEditionBonTransporteur(!Character.isSpaceChar(sortie.getPoedt4()));
    article.setTopFacture(!Character.isSpaceChar(sortie.getPoedt5()));
    article.setTopDevis(!Character.isSpaceChar(sortie.getPoedt6()));
    
    return article;
  }
}
