/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.stocks;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvx0003i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PIMAG = 2;
  public static final int SIZE_PILIG = 3;
  public static final int DECIMAL_PILIG = 0;
  public static final int SIZE_PIPAG = 3;
  public static final int DECIMAL_PIPAG = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 42;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PIART = 2;
  public static final int VAR_PIMAG = 3;
  public static final int VAR_PILIG = 4;
  public static final int VAR_PIPAG = 5;
  public static final int VAR_PIARR = 6;
  
  // Variables AS400
  public String piind = ""; // Indicateurs
  public String pietb = ""; // Code établissement
  public String piart = ""; // Code Article
  public String pimag = ""; // Code Magasin
  public BigDecimal pilig = BigDecimal.ZERO; // Nombre de ligne par page
  public BigDecimal pipag = BigDecimal.ZERO; // Numéro de la page
  public String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = {
      new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400Text(SIZE_PIART), // Code Article
      new AS400Text(SIZE_PIMAG), // Code Magasin
      new AS400ZonedDecimal(SIZE_PILIG, DECIMAL_PILIG), // Nombre de ligne par page
      new AS400ZonedDecimal(SIZE_PIPAG, DECIMAL_PIPAG), // Numéro de la page
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pietb, piart, pimag, pilig, pipag, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    piart = (String) output[2];
    pimag = (String) output[3];
    pilig = (BigDecimal) output[4];
    pipag = (BigDecimal) output[5];
    piarr = (String) output[6];
    
  }
}
