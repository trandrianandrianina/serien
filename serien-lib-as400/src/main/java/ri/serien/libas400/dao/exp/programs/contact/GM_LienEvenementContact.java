/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.programs.contact;

import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;

public class GM_LienEvenementContact extends BaseGroupDB {
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public GM_LienEvenementContact(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Contrôle qu'un contact soit lié à des évènements
   * @param contact
   * @return
   * 
   */
  public boolean checkContactWithoutLink(M_Contact contact) {
    if (contact == null) {
      msgErreur += "\nLa classe M_Contact est nulle";
      return false;
    }
    
    String requete = "select count(*) from " + queryManager.getLibrary() + ".PSEMLECM where ECIDC = " + contact.getRENUM() + " and ECETBC = '"
        + contact.getREETB() + "'";
    String resultat = queryManager.firstEnrgSelect(requete, 1);
    if (resultat == null) {
      return false;
    }
    else if (resultat.equals("0")) {
      return true;
    }
    return false;
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
  
}
