/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.article;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.outils.MessageErreurException;

public class SqlURLArticle {
  // Variables
  protected QueryManager querymg = null;
  private String[] listeURL = null;
  
  /**
   * Constructeur.
   */
  public SqlURLArticle(QueryManager aquerymg) {
    
    querymg = aquerymg;
  }
  
  /**
   * Lecture d'un Fournisseur donnée.
   */
  public String[] chargerListeURLArticle(Article aArticle) {
    try {
      if (aArticle == null) {
        throw new MessageErreurException("La variable aArticle est à null.");
      }
      
      String codeEtablissement = aArticle.getId().getCodeEtablissement();
      String codeArticle = aArticle.getId().getCodeArticle();
      if ((codeEtablissement == null) || (codeArticle.trim().equals(""))) {
        throw new MessageErreurException("La clé pour lire l'article est incorrecte.");
      }
      String requete = "select * from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_URL + " where FTETB = '"
          + codeEtablissement + "' and FTART = '" + codeArticle + "'";
      
      listeURL = request4listeURL(requete);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Une requête SQL a provoqué en erreur, merci de contacter le support.");
    }
    return listeURL;
  }
  
  /**
   * Lecture des résultats
   */
  public String[] request4listeURL(String requete) {
    if ((requete == null) || (requete.trim().length() == 0)) {
      return null;
    }
    if (querymg.getLibrary() == null) {
      return null;
    }
    
    // Lecture de la base afin de récupérer les actions commerciales
    ArrayList<GenericRecord> listbnd = querymg.select(requete);
    if (listbnd == null) {
      return null;
    }
    String[] listbns = new String[listbnd.size()];
    for (int j = 0; j < listbnd.size(); j++) {
      listbns[j] = (String) listbnd.get(j).getField("FTURL");
    }
    return listbns;
  }
  
}
