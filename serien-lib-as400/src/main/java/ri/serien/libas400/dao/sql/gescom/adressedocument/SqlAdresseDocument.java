/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.adressedocument;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.adressedocument.AdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.CritereAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.EnumCodeEnteteAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.IdAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.ListeAdresseDocument;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Regrouper les accès SQL en relation avec les bons de cour.
 */
public class SqlAdresseDocument {
  
  // Variables
  private QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlAdresseDocument(QueryManager pQuerymg) {
    querymg = pQuerymg;
  }
  
  // -- Méthodes publiques
  
  /**
   * Charger la liste des identifiants d'adresses de documents correspondant aux critères.
   */
  public List<IdAdresseDocument> chargerListeIdAdresseDocument(CritereAdresseDocument pCriteres) {
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères de recherche des adresses de document sont invalides.");
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT AVCOD, AVETB, AVNUM, AVSUF, AVNLI FROM " + querymg.getLibrary() + '.'
        + EnumTableBDD.DOCUMENT_VENTE_ADRESSE + " WHERE ");
    requeteSql.ajouterConditionAnd("AVETB", "=", pCriteres.getIdEtablissement().getCodeEtablissement());
    
    // Filtre sur le code entête de l'adresse
    if (pCriteres.getTypeAdresse() != null) {
      requeteSql.ajouterConditionAnd("AVCOD", "=", pCriteres.getTypeAdresse().getCode());
    }
    
    // Filtre sur le numéro de document
    if (pCriteres.getIdDocument() != null) {
      requeteSql.ajouterConditionAnd("AVNUM", "=", pCriteres.getIdDocument().getNumero().intValue());
      requeteSql.ajouterConditionAnd("AVSUF", "=", pCriteres.getIdDocument().getSuffixe().intValue());
    }
    
    // Filtre sur le numéro de l'adresse
    if (pCriteres.getNumeroAdresse() != null) {
      requeteSql.ajouterConditionAnd("AVNLI", "=", pCriteres.getNumeroAdresse().intValue());
    }
    
    // Ordre de tri
    requeteSql.ajouter(" ORDER BY AVNUM, AVSUF, AVNLI");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    
    // Création de la liste des identifiants trouvés
    List<IdAdresseDocument> listeIdAdresseDocument = new ArrayList<IdAdresseDocument>();
    for (GenericRecord record : listeGenericRecord) {
      listeIdAdresseDocument.add(initialiserIdAdresseDocument(record));
    }
    return listeIdAdresseDocument;
  }
  
  /**
   * Constituer l'identifiant d'une adresse de document à partir de la lecture d'un record
   */
  private IdAdresseDocument initialiserIdAdresseDocument(GenericRecord pGenericRecord) {
    // Lire l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("AVETB"));
    
    EnumCodeEnteteDocumentVente codeEnteteDocument = null;
    switch (pGenericRecord.getCharacterValue("AVCOD", 'D')) {
      case 'L':
      case 'F':
        codeEnteteDocument = EnumCodeEnteteDocumentVente.COMMANDE_OU_BON;
        break;
      
      default:
        codeEnteteDocument = EnumCodeEnteteDocumentVente.DEVIS;
        break;
    }
    
    // Identifiant du document
    IdDocumentVente idDocumentVente = IdDocumentVente.getInstanceGenerique(idEtablissement, codeEnteteDocument,
        pGenericRecord.getIntegerValue("AVNUM"), pGenericRecord.getIntegerValue("AVSUF"), null);
    
    IdAdresseDocument idAdresseDocument = IdAdresseDocument.getInstance(idEtablissement,
        EnumCodeEnteteAdresseDocument.valueOfByCode(pGenericRecord.getCharacterValue("AVCOD")), idDocumentVente,
        pGenericRecord.getIntegerValue("AVNLI"));
    
    return idAdresseDocument;
  }
  
  /**
   * Charger la liste des adresses de documents correspondant à la liste de leurs identifiants.
   */
  public ListeAdresseDocument chargerListeAdresseDocument(List<IdAdresseDocument> pListeIdAdresseDocument) {
    if (pListeIdAdresseDocument == null || pListeIdAdresseDocument.isEmpty()) {
      throw new MessageErreurException("Les identifiants de carnets de bons de cour sont invalides.");
    }
    
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT *  FROM ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_ADRESSE);
    requeteSql.ajouter(" WHERE");
    requeteSql.ajouterConditionAnd("AVETB", "=", pListeIdAdresseDocument.get(0).getIdEtablissement().getCodeEtablissement());
    
    // Ajouter les identifiants des documents de ventes des adresses à charger à la requête
    int index = 0;
    for (IdAdresseDocument idAdresseDocument : pListeIdAdresseDocument) {
      if (index == 0) {
        requeteSql.ajouter(" and (");
      }
      else {
        requeteSql.ajouter(" or ");
      }
      requeteSql.ajouter(" (AVNUM = " + idAdresseDocument.getIdDocumentVente().getNumero());
      requeteSql.ajouterConditionAnd("AVSUF", "=", idAdresseDocument.getIdDocumentVente().getSuffixe());
      requeteSql.ajouterConditionAnd("AVCOD", "=", idAdresseDocument.getCodeAdresse().getCode());
      requeteSql.ajouterConditionAnd("AVNLI", "=", idAdresseDocument.getNumeroAdresse());
      requeteSql.ajouter(")");
      index++;
    }
    requeteSql.ajouter(" )");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Création de la liste objets avec les données trouvées
    ListeAdresseDocument listeAdresseDocument = new ListeAdresseDocument();
    for (GenericRecord record : listeGenericRecord) {
      listeAdresseDocument.add(initialiserAdresseDocument(record));
    }
    return listeAdresseDocument;
  }
  
  /**
   * Constituer un objet AdresseDocument à partir de la lecture d'un record.
   */
  private AdresseDocument initialiserAdresseDocument(GenericRecord pGenericRecord) {
    // Identifiant de l'adresse
    IdAdresseDocument idAdresseDocument = initialiserIdAdresseDocument(pGenericRecord);
    
    // Adresse
    Adresse adresse = new Adresse();
    adresse.setNom(pGenericRecord.getStringValue("AVNOM"));
    adresse.setComplementNom(pGenericRecord.getStringValue("AVCPL"));
    adresse.setRue(pGenericRecord.getStringValue("AVRUE"));
    adresse.setLocalisation(pGenericRecord.getStringValue("AVLOC"));
    adresse.setCodePostal(pGenericRecord.getIntegerValue("AVCDP", 0));
    adresse.setVille(pGenericRecord.getStringValue("AVVIL"));
    adresse.setPays(pGenericRecord.getStringValue("AVPAY"));
    
    AdresseDocument adresseDocument = new AdresseDocument(idAdresseDocument);
    adresseDocument.setAdresse(adresse);
    adresseDocument.setNumeroTelephone(pGenericRecord.getStringValue("AVTEL"));
    adresseDocument.setNumeroTelephone(pGenericRecord.getStringValue("AVFAX"));
    
    return adresseDocument;
  }
  
  /**
   * Renvoyer le premier numéro libre pour une nouvelle adresse du document en base de donnée
   */
  public int chercherPremierNumeroAdresseLibre(IdDocumentVente pIdDocument) {
    
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT MIN(AVNLI+1) as numero FROM " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_ADRESSE
        + " WHERE (AVNLI+1) NOT IN (SELECT AVNLI FROM " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_ADRESSE
        + " WHERE ");
    requeteSql.ajouterConditionAnd("AVNUM", "=", pIdDocument.getNumero());
    requeteSql.ajouterConditionAnd("AVSUF", "=", pIdDocument.getSuffixe());
    requeteSql.ajouter(")");
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      throw new MessageErreurException("Pas de numéro libre pour la création d'un contact en base de donnée !");
    }
    
    return listeRecord.get(0).getIntValue("numero", 1);
  }
  
  /**
   * Créer une adresse de document de vente
   */
  public void creerAdresseDocument(AdresseDocument pAdresseDocument) {
    if (pAdresseDocument == null || pAdresseDocument.getAdresse() == null) {
      throw new MessageErreurException("L'AdresseDocument que vous souhaitez créer est invalide.");
    }
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("INSERT INTO " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_ADRESSE
        + " (AVCOD, AVETB, AVNUM, AVSUF, AVNLI, AVNOM, AVCPL, AVRUE, AVLOC, AVCDP, AVVIL) VALUES (");
    requeteSql.ajouter(RequeteSql.formaterCharacterSQL(pAdresseDocument.getId().getCodeAdresse().getCode()));
    requeteSql.ajouter(", " + RequeteSql.formaterStringSQL(pAdresseDocument.getId().getCodeEtablissement()));
    requeteSql.ajouter(", " + pAdresseDocument.getId().getIdDocumentVente().getNumero());
    requeteSql.ajouter(", " + pAdresseDocument.getId().getIdDocumentVente().getSuffixe());
    requeteSql.ajouter(", " + pAdresseDocument.getId().getNumeroAdresse());
    
    // Mettre à jour le nom
    if (pAdresseDocument.getAdresse().getNom().trim().length() > Adresse.LONGUEUR_ADRESSE) {
      throw new MessageErreurException("La longueur maximale pour le champ nom est de " + Adresse.LONGUEUR_ADRESSE + " caractères.");
    }
    requeteSql.ajouter(", " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getNom()));
    
    // Mettre à jour le complément de nom
    if (pAdresseDocument.getAdresse().getComplementNom().trim().length() > Adresse.LONGUEUR_ADRESSE) {
      throw new MessageErreurException("La longueur maximale pour le champ prénom est de " + Adresse.LONGUEUR_ADRESSE + " caractères.");
    }
    requeteSql.ajouter(", " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getComplementNom()));
    
    // Mettre à jour la rue
    if (pAdresseDocument.getAdresse().getRue().trim().length() > Adresse.LONGUEUR_ADRESSE) {
      throw new MessageErreurException("La longueur maximale pour le champ rue est de " + Adresse.LONGUEUR_ADRESSE + " caractères.");
    }
    requeteSql.ajouter(", " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getRue()));
    
    // Mettre à jour la localisation
    if (pAdresseDocument.getAdresse().getLocalisation().trim().length() > Adresse.LONGUEUR_ADRESSE) {
      throw new MessageErreurException(
          "La longueur maximale pour le champ localisation est de " + Adresse.LONGUEUR_ADRESSE + " caractères.");
    }
    requeteSql.ajouter(", " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getLocalisation()));
    
    // Mettre à jour le code postal
    if (("" + pAdresseDocument.getAdresse().getCodePostal()).trim().length() > Adresse.LONGUEUR_CODEPOSTAL) {
      throw new MessageErreurException(
          "La longueur maximale pour le champ code postal est de " + Adresse.LONGUEUR_CODEPOSTAL + " caractères.");
    }
    requeteSql.ajouter(", " + pAdresseDocument.getAdresse().getCodePostal());
    
    // Mettre à jour la ville
    if (pAdresseDocument.getAdresse().getVille().trim().length() > Adresse.LONGUEUR_VILLE) {
      throw new MessageErreurException("La longueur maximale pour le champ ville est de " + Adresse.LONGUEUR_VILLE + " caractères.");
    }
    requeteSql.ajouter(", " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getVille()));
    
    requeteSql.ajouter(")");
    querymg.requete(requeteSql.getRequete());
  }
  
  /**
   * Supprimer une adresse de document de vente
   */
  public void supprimerAdresseDocument(AdresseDocument pAdresseDocument) {
    if (pAdresseDocument == null) {
      throw new MessageErreurException("L'AdresseDocument que vous souhaitez supprimer est invalide.");
    }
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("DELETE FROM " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_ADRESSE + " WHERE ");
    requeteSql.ajouter("AVETB = " + RequeteSql.formaterStringSQL(pAdresseDocument.getId().getIdEtablissement().getCodeEtablissement()));
    requeteSql.ajouterConditionAnd("AVCOD", "=", pAdresseDocument.getId().getCodeAdresse().getCode());
    requeteSql.ajouterConditionAnd("AVNUM", "=", pAdresseDocument.getId().getIdDocumentVente().getNumero().intValue());
    requeteSql.ajouterConditionAnd("AVSUF", "=", pAdresseDocument.getId().getIdDocumentVente().getSuffixe().intValue());
    requeteSql.ajouterConditionAnd("AVNLI", "=", pAdresseDocument.getId().getNumeroAdresse().intValue());
    querymg.requete(requeteSql.getRequete());
  }
  
  /**
   * Sauvegarder une adresse de document de vente en base de données
   */
  public void sauverAdresseDocument(AdresseDocument pAdresseDocument) {
    if (pAdresseDocument == null) {
      throw new MessageErreurException("L'AdresseDocument que vous souhaitez modifier est invalide.");
    }
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("UPDATE " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_ADRESSE + " SET");
    
    // Mettre à jour le nom
    if (pAdresseDocument.getAdresse().getNom().trim().length() > Adresse.LONGUEUR_ADRESSE) {
      throw new MessageErreurException("La longueur maximale pour le champ nom est de " + Adresse.LONGUEUR_ADRESSE + " caractères.");
    }
    requeteSql.ajouter("  AVNOM = " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getNom()));
    
    // Mettre à jour le complément de nom
    if (pAdresseDocument.getAdresse().getComplementNom().trim().length() > Adresse.LONGUEUR_ADRESSE) {
      throw new MessageErreurException("La longueur maximale pour le champ prénom est de " + Adresse.LONGUEUR_ADRESSE + " caractères.");
    }
    requeteSql.ajouter(", AVCPL = " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getComplementNom()));
    
    // Mettre à jour la rue
    if (pAdresseDocument.getAdresse().getRue().trim().length() > Adresse.LONGUEUR_ADRESSE) {
      throw new MessageErreurException("La longueur maximale pour le champ rue est de " + Adresse.LONGUEUR_ADRESSE + " caractères.");
    }
    requeteSql.ajouter(", AVRUE = " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getRue()));
    
    // Mettre à jour la localisation
    if (pAdresseDocument.getAdresse().getLocalisation().trim().length() > Adresse.LONGUEUR_ADRESSE) {
      throw new MessageErreurException(
          "La longueur maximale pour le champ localisation est de " + Adresse.LONGUEUR_ADRESSE + " caractères.");
    }
    requeteSql.ajouter(", AVLOC = " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getLocalisation()));
    
    // Mettre à jour le code postal
    if (("" + pAdresseDocument.getAdresse().getCodePostal()).trim().length() > Adresse.LONGUEUR_CODEPOSTAL) {
      throw new MessageErreurException(
          "La longueur maximale pour le champ code postal est de " + Adresse.LONGUEUR_CODEPOSTAL + " caractères.");
    }
    requeteSql.ajouter(", AVCDP = " + pAdresseDocument.getAdresse().getCodePostal());
    
    // Mettre à jour la ville
    if (pAdresseDocument.getAdresse().getVille().trim().length() > Adresse.LONGUEUR_VILLE) {
      throw new MessageErreurException("La longueur maximale pour le champ ville est de " + Adresse.LONGUEUR_VILLE + " caractères.");
    }
    
    requeteSql.ajouter(", AVVIL = " + RequeteSql.formaterStringSQL(pAdresseDocument.getAdresse().getVille()));
    requeteSql.ajouter(" WHERE ");
    requeteSql.ajouter("AVETB = " + RequeteSql.formaterStringSQL(pAdresseDocument.getId().getIdEtablissement().getCodeEtablissement()));
    requeteSql.ajouterConditionAnd("AVCOD", "=", pAdresseDocument.getId().getCodeAdresse().getCode());
    requeteSql.ajouterConditionAnd("AVNUM", "=", pAdresseDocument.getId().getIdDocumentVente().getNumero().intValue());
    requeteSql.ajouterConditionAnd("AVSUF", "=", pAdresseDocument.getId().getIdDocumentVente().getSuffixe().intValue());
    requeteSql.ajouterConditionAnd("AVNLI", "=", pAdresseDocument.getId().getNumeroAdresse().intValue());
    
    querymg.requete(requeteSql.getRequete());
  }
  
}
