/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.modification;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0024i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIUSR = 10;
  public static final int SIZE_PITOP = 1;
  public static final int DECIMAL_PITOP = 0;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PICRE = 7;
  public static final int DECIMAL_PICRE = 0;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIHOM = 7;
  public static final int DECIMAL_PIHOM = 0;
  public static final int SIZE_PIREC = 7;
  public static final int DECIMAL_PIREC = 0;
  public static final int SIZE_PIFAC = 7;
  public static final int DECIMAL_PIFAC = 0;
  public static final int SIZE_PINFA = 6;
  public static final int DECIMAL_PINFA = 0;
  public static final int SIZE_PINLS = 4;
  public static final int DECIMAL_PINLS = 0;
  public static final int SIZE_PIPLI = 4;
  public static final int DECIMAL_PIPLI = 0;
  public static final int SIZE_PIDLI = 4;
  public static final int DECIMAL_PIDLI = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PITLV = 1;
  public static final int DECIMAL_PITLV = 0;
  public static final int SIZE_PITLL = 1;
  public static final int DECIMAL_PITLL = 0;
  public static final int SIZE_PIDSU = 1;
  public static final int DECIMAL_PIDSU = 0;
  public static final int SIZE_PIETA = 1;
  public static final int DECIMAL_PIETA = 0;
  public static final int SIZE_PIEDT = 1;
  public static final int DECIMAL_PIEDT = 0;
  public static final int SIZE_PITFC = 1;
  public static final int DECIMAL_PITFC = 0;
  public static final int SIZE_PITT = 1;
  public static final int DECIMAL_PITT = 0;
  public static final int SIZE_PINRG = 1;
  public static final int DECIMAL_PINRG = 0;
  public static final int SIZE_PITVA1 = 1;
  public static final int DECIMAL_PITVA1 = 0;
  public static final int SIZE_PITVA2 = 1;
  public static final int DECIMAL_PITVA2 = 0;
  public static final int SIZE_PITVA3 = 1;
  public static final int DECIMAL_PITVA3 = 0;
  public static final int SIZE_PIRLV = 1;
  public static final int DECIMAL_PIRLV = 0;
  public static final int SIZE_PINEX = 1;
  public static final int DECIMAL_PINEX = 0;
  public static final int SIZE_PIEXC = 1;
  public static final int DECIMAL_PIEXC = 0;
  public static final int SIZE_PIEXE = 1;
  public static final int DECIMAL_PIEXE = 0;
  public static final int SIZE_PINRR = 1;
  public static final int DECIMAL_PINRR = 0;
  public static final int SIZE_PISGN = 1;
  public static final int DECIMAL_PISGN = 0;
  public static final int SIZE_PILIV = 1;
  public static final int DECIMAL_PILIV = 0;
  public static final int SIZE_PICPT = 1;
  public static final int DECIMAL_PICPT = 0;
  public static final int SIZE_PIPGC = 1;
  public static final int DECIMAL_PIPGC = 0;
  public static final int SIZE_PICHF = 1;
  public static final int DECIMAL_PICHF = 0;
  public static final int SIZE_PIINA = 1;
  public static final int DECIMAL_PIINA = 0;
  public static final int SIZE_PICOL = 1;
  public static final int DECIMAL_PICOL = 0;
  public static final int SIZE_PIFRS = 6;
  public static final int DECIMAL_PIFRS = 0;
  public static final int SIZE_PIDLP = 7;
  public static final int DECIMAL_PIDLP = 0;
  public static final int SIZE_PINUM0 = 6;
  public static final int DECIMAL_PINUM0 = 0;
  public static final int SIZE_PISUF0 = 1;
  public static final int DECIMAL_PISUF0 = 0;
  public static final int SIZE_PIAVR = 1;
  public static final int SIZE_PITFA = 1;
  public static final int SIZE_PINAT = 1;
  public static final int SIZE_PIESC = 3;
  public static final int DECIMAL_PIESC = 2;
  public static final int SIZE_PIE1G = 7;
  public static final int DECIMAL_PIE1G = 0;
  public static final int SIZE_PIE2G = 7;
  public static final int DECIMAL_PIE2G = 0;
  public static final int SIZE_PIE3G = 7;
  public static final int DECIMAL_PIE3G = 0;
  public static final int SIZE_PIE4G = 7;
  public static final int DECIMAL_PIE4G = 0;
  public static final int SIZE_PIR1G = 11;
  public static final int DECIMAL_PIR1G = 2;
  public static final int SIZE_PIR2G = 11;
  public static final int DECIMAL_PIR2G = 2;
  public static final int SIZE_PIR3G = 11;
  public static final int DECIMAL_PIR3G = 2;
  public static final int SIZE_PIR4G = 11;
  public static final int DECIMAL_PIR4G = 2;
  public static final int SIZE_PISL1 = 11;
  public static final int DECIMAL_PISL1 = 2;
  public static final int SIZE_PISL2 = 11;
  public static final int DECIMAL_PISL2 = 2;
  public static final int SIZE_PISL3 = 11;
  public static final int DECIMAL_PISL3 = 2;
  public static final int SIZE_PITHTL = 11;
  public static final int DECIMAL_PITHTL = 2;
  public static final int SIZE_PITL1 = 11;
  public static final int DECIMAL_PITL1 = 2;
  public static final int SIZE_PITL2 = 11;
  public static final int DECIMAL_PITL2 = 2;
  public static final int SIZE_PITL3 = 11;
  public static final int DECIMAL_PITL3 = 2;
  public static final int SIZE_PITV1 = 5;
  public static final int DECIMAL_PITV1 = 3;
  public static final int SIZE_PITV2 = 5;
  public static final int DECIMAL_PITV2 = 3;
  public static final int SIZE_PITV3 = 5;
  public static final int DECIMAL_PITV3 = 3;
  public static final int SIZE_PIMES = 9;
  public static final int DECIMAL_PIMES = 2;
  public static final int SIZE_PITTC = 11;
  public static final int DECIMAL_PITTC = 2;
  public static final int SIZE_PIRG1 = 2;
  public static final int SIZE_PIEC1 = 2;
  public static final int SIZE_PIPC1 = 2;
  public static final int SIZE_PIRG2 = 2;
  public static final int SIZE_PIEC2 = 2;
  public static final int SIZE_PIPC2 = 2;
  public static final int SIZE_PIRG3 = 2;
  public static final int SIZE_PIEC3 = 2;
  public static final int SIZE_PIPC3 = 2;
  public static final int SIZE_PIRG4 = 2;
  public static final int SIZE_PIEC4 = 2;
  public static final int SIZE_PIPC4 = 2;
  public static final int SIZE_PIMAG = 2;
  public static final int SIZE_PIRBL = 10;
  public static final int SIZE_PISAN = 4;
  public static final int SIZE_PIACT = 4;
  public static final int SIZE_PILLV = 5;
  public static final int SIZE_PIACH = 3;
  public static final int SIZE_PICJA = 2;
  public static final int SIZE_PIDEV = 3;
  public static final int SIZE_PICHG = 11;
  public static final int DECIMAL_PICHG = 6;
  public static final int SIZE_PICPR = 5;
  public static final int DECIMAL_PICPR = 4;
  public static final int SIZE_PIBAS = 5;
  public static final int DECIMAL_PIBAS = 0;
  public static final int SIZE_PIDOS = 7;
  public static final int SIZE_PICNT = 13;
  public static final int SIZE_PIPDS = 8;
  public static final int DECIMAL_PIPDS = 3;
  public static final int SIZE_PIVOL = 8;
  public static final int DECIMAL_PIVOL = 3;
  public static final int SIZE_PIMTA = 11;
  public static final int DECIMAL_PIMTA = 2;
  public static final int SIZE_PIREM1 = 4;
  public static final int DECIMAL_PIREM1 = 2;
  public static final int SIZE_PIREM2 = 4;
  public static final int DECIMAL_PIREM2 = 2;
  public static final int SIZE_PIREM3 = 4;
  public static final int DECIMAL_PIREM3 = 2;
  public static final int SIZE_PIREM4 = 4;
  public static final int DECIMAL_PIREM4 = 2;
  public static final int SIZE_PIREM5 = 4;
  public static final int DECIMAL_PIREM5 = 2;
  public static final int SIZE_PIREM6 = 4;
  public static final int DECIMAL_PIREM6 = 2;
  public static final int SIZE_PITRL = 1;
  public static final int SIZE_PIBRL = 1;
  public static final int SIZE_PIRP1 = 4;
  public static final int DECIMAL_PIRP1 = 2;
  public static final int SIZE_PIRP2 = 4;
  public static final int DECIMAL_PIRP2 = 2;
  public static final int SIZE_PIRP3 = 4;
  public static final int DECIMAL_PIRP3 = 2;
  public static final int SIZE_PIRP4 = 4;
  public static final int DECIMAL_PIRP4 = 2;
  public static final int SIZE_PIRP5 = 4;
  public static final int DECIMAL_PIRP5 = 2;
  public static final int SIZE_PIRP6 = 4;
  public static final int DECIMAL_PIRP6 = 2;
  public static final int SIZE_PITRP = 1;
  public static final int SIZE_PITP1 = 2;
  public static final int SIZE_PITP2 = 2;
  public static final int SIZE_PITP3 = 2;
  public static final int SIZE_PITP4 = 2;
  public static final int SIZE_PITP5 = 2;
  public static final int SIZE_PIIN1 = 1;
  public static final int SIZE_PIIN2 = 1;
  public static final int SIZE_PIIN3 = 1;
  public static final int SIZE_PIIN4 = 1;
  public static final int SIZE_PIIN5 = 1;
  public static final int SIZE_PICCT = 10;
  public static final int SIZE_PIDAT1 = 7;
  public static final int DECIMAL_PIDAT1 = 0;
  public static final int SIZE_PIRBC = 10;
  public static final int SIZE_PIINT1 = 7;
  public static final int DECIMAL_PIINT1 = 0;
  public static final int SIZE_PIINT2 = 7;
  public static final int DECIMAL_PIINT2 = 0;
  public static final int SIZE_PIETAI = 1;
  public static final int SIZE_PIMEX = 2;
  public static final int SIZE_PICTR = 2;
  public static final int SIZE_PIDILI = 1;
  public static final int SIZE_PIIN6 = 1;
  public static final int SIZE_PIIN7 = 1;
  public static final int SIZE_PIIN8 = 1;
  public static final int SIZE_PIIN9 = 1;
  public static final int SIZE_PIRFL = 50;
  public static final int SIZE_PIPOR0 = 11;
  public static final int DECIMAL_PIPOR0 = 2;
  public static final int SIZE_PIPOR1 = 11;
  public static final int DECIMAL_PIPOR1 = 2;
  public static final int SIZE_PIIN10 = 1;
  public static final int SIZE_PIIN11 = 1;
  public static final int SIZE_PIIN12 = 1;
  public static final int SIZE_PIIN13 = 1;
  public static final int SIZE_PIIN14 = 1;
  public static final int SIZE_PIIN15 = 1;
  public static final int SIZE_PINOM2 = 30;
  public static final int SIZE_PICPL2 = 30;
  public static final int SIZE_PIRUE2 = 30;
  public static final int SIZE_PILOC2 = 30;
  public static final int SIZE_PICDP2 = 5;
  public static final int SIZE_PIVIL2 = 24;
  public static final int SIZE_PINOM3 = 30;
  public static final int SIZE_PICPL3 = 30;
  public static final int SIZE_PIRUE3 = 30;
  public static final int SIZE_PILOC3 = 30;
  public static final int SIZE_PICDP3 = 5;
  public static final int SIZE_PIFIL3 = 1;
  public static final int SIZE_PIVIL3 = 24;
  public static final int SIZE_PIENV = 1;
  public static final int SIZE_PICNTCT = 9;
  public static final int SIZE_PICODX = 1;
  public static final int SIZE_PIETBX = 3;
  public static final int SIZE_PINUMX = 6;
  public static final int DECIMAL_PINUMX = 0;
  public static final int SIZE_PISUFX = 1;
  public static final int DECIMAL_PISUFX = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 963;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIUSR = 1;
  public static final int VAR_PITOP = 2;
  public static final int VAR_PICOD = 3;
  public static final int VAR_PIETB = 4;
  public static final int VAR_PINUM = 5;
  public static final int VAR_PISUF = 6;
  public static final int VAR_PICRE = 7;
  public static final int VAR_PIDAT = 8;
  public static final int VAR_PIHOM = 9;
  public static final int VAR_PIREC = 10;
  public static final int VAR_PIFAC = 11;
  public static final int VAR_PINFA = 12;
  public static final int VAR_PINLS = 13;
  public static final int VAR_PIPLI = 14;
  public static final int VAR_PIDLI = 15;
  public static final int VAR_PINLI = 16;
  public static final int VAR_PITLV = 17;
  public static final int VAR_PITLL = 18;
  public static final int VAR_PIDSU = 19;
  public static final int VAR_PIETA = 20;
  public static final int VAR_PIEDT = 21;
  public static final int VAR_PITFC = 22;
  public static final int VAR_PITT = 23;
  public static final int VAR_PINRG = 24;
  public static final int VAR_PITVA1 = 25;
  public static final int VAR_PITVA2 = 26;
  public static final int VAR_PITVA3 = 27;
  public static final int VAR_PIRLV = 28;
  public static final int VAR_PINEX = 29;
  public static final int VAR_PIEXC = 30;
  public static final int VAR_PIEXE = 31;
  public static final int VAR_PINRR = 32;
  public static final int VAR_PISGN = 33;
  public static final int VAR_PILIV = 34;
  public static final int VAR_PICPT = 35;
  public static final int VAR_PIPGC = 36;
  public static final int VAR_PICHF = 37;
  public static final int VAR_PIINA = 38;
  public static final int VAR_PICOL = 39;
  public static final int VAR_PIFRS = 40;
  public static final int VAR_PIDLP = 41;
  public static final int VAR_PINUM0 = 42;
  public static final int VAR_PISUF0 = 43;
  public static final int VAR_PIAVR = 44;
  public static final int VAR_PITFA = 45;
  public static final int VAR_PINAT = 46;
  public static final int VAR_PIESC = 47;
  public static final int VAR_PIE1G = 48;
  public static final int VAR_PIE2G = 49;
  public static final int VAR_PIE3G = 50;
  public static final int VAR_PIE4G = 51;
  public static final int VAR_PIR1G = 52;
  public static final int VAR_PIR2G = 53;
  public static final int VAR_PIR3G = 54;
  public static final int VAR_PIR4G = 55;
  public static final int VAR_PISL1 = 56;
  public static final int VAR_PISL2 = 57;
  public static final int VAR_PISL3 = 58;
  public static final int VAR_PITHTL = 59;
  public static final int VAR_PITL1 = 60;
  public static final int VAR_PITL2 = 61;
  public static final int VAR_PITL3 = 62;
  public static final int VAR_PITV1 = 63;
  public static final int VAR_PITV2 = 64;
  public static final int VAR_PITV3 = 65;
  public static final int VAR_PIMES = 66;
  public static final int VAR_PITTC = 67;
  public static final int VAR_PIRG1 = 68;
  public static final int VAR_PIEC1 = 69;
  public static final int VAR_PIPC1 = 70;
  public static final int VAR_PIRG2 = 71;
  public static final int VAR_PIEC2 = 72;
  public static final int VAR_PIPC2 = 73;
  public static final int VAR_PIRG3 = 74;
  public static final int VAR_PIEC3 = 75;
  public static final int VAR_PIPC3 = 76;
  public static final int VAR_PIRG4 = 77;
  public static final int VAR_PIEC4 = 78;
  public static final int VAR_PIPC4 = 79;
  public static final int VAR_PIMAG = 80;
  public static final int VAR_PIRBL = 81;
  public static final int VAR_PISAN = 82;
  public static final int VAR_PIACT = 83;
  public static final int VAR_PILLV = 84;
  public static final int VAR_PIACH = 85;
  public static final int VAR_PICJA = 86;
  public static final int VAR_PIDEV = 87;
  public static final int VAR_PICHG = 88;
  public static final int VAR_PICPR = 89;
  public static final int VAR_PIBAS = 90;
  public static final int VAR_PIDOS = 91;
  public static final int VAR_PICNT = 92;
  public static final int VAR_PIPDS = 93;
  public static final int VAR_PIVOL = 94;
  public static final int VAR_PIMTA = 95;
  public static final int VAR_PIREM1 = 96;
  public static final int VAR_PIREM2 = 97;
  public static final int VAR_PIREM3 = 98;
  public static final int VAR_PIREM4 = 99;
  public static final int VAR_PIREM5 = 100;
  public static final int VAR_PIREM6 = 101;
  public static final int VAR_PITRL = 102;
  public static final int VAR_PIBRL = 103;
  public static final int VAR_PIRP1 = 104;
  public static final int VAR_PIRP2 = 105;
  public static final int VAR_PIRP3 = 106;
  public static final int VAR_PIRP4 = 107;
  public static final int VAR_PIRP5 = 108;
  public static final int VAR_PIRP6 = 109;
  public static final int VAR_PITRP = 110;
  public static final int VAR_PITP1 = 111;
  public static final int VAR_PITP2 = 112;
  public static final int VAR_PITP3 = 113;
  public static final int VAR_PITP4 = 114;
  public static final int VAR_PITP5 = 115;
  public static final int VAR_PIIN1 = 116;
  public static final int VAR_PIIN2 = 117;
  public static final int VAR_PIIN3 = 118;
  public static final int VAR_PIIN4 = 119;
  public static final int VAR_PIIN5 = 120;
  public static final int VAR_PICCT = 121;
  public static final int VAR_PIDAT1 = 122;
  public static final int VAR_PIRBC = 123;
  public static final int VAR_PIINT1 = 124;
  public static final int VAR_PIINT2 = 125;
  public static final int VAR_PIETAI = 126;
  public static final int VAR_PIMEX = 127;
  public static final int VAR_PICTR = 128;
  public static final int VAR_PIDILI = 129;
  public static final int VAR_PIIN6 = 130;
  public static final int VAR_PIIN7 = 131;
  public static final int VAR_PIIN8 = 132;
  public static final int VAR_PIIN9 = 133;
  public static final int VAR_PIRFL = 134;
  public static final int VAR_PIPOR0 = 135;
  public static final int VAR_PIPOR1 = 136;
  public static final int VAR_PIIN10 = 137;
  public static final int VAR_PIIN11 = 138;
  public static final int VAR_PIIN12 = 139;
  public static final int VAR_PIIN13 = 140;
  public static final int VAR_PIIN14 = 141;
  public static final int VAR_PIIN15 = 142;
  public static final int VAR_PINOM2 = 143;
  public static final int VAR_PICPL2 = 144;
  public static final int VAR_PIRUE2 = 145;
  public static final int VAR_PILOC2 = 146;
  public static final int VAR_PICDP2 = 147;
  public static final int VAR_PIVIL2 = 148;
  public static final int VAR_PINOM3 = 149;
  public static final int VAR_PICPL3 = 150;
  public static final int VAR_PIRUE3 = 151;
  public static final int VAR_PILOC3 = 152;
  public static final int VAR_PICDP3 = 153;
  public static final int VAR_PIFIL3 = 154;
  public static final int VAR_PIVIL3 = 155;
  public static final int VAR_PIENV = 156;
  public static final int VAR_PICNTCT = 157;
  public static final int VAR_PICODX = 158;
  public static final int VAR_PIETBX = 159;
  public static final int VAR_PINUMX = 160;
  public static final int VAR_PISUFX = 161;
  public static final int VAR_PIARR = 162;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String piusr = ""; // User pour lect. sécurité
  private BigDecimal pitop = BigDecimal.ZERO; // Top système
  private String picod = ""; // Code ERL "E","F" *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal picre = BigDecimal.ZERO; // Date Création
  private BigDecimal pidat = BigDecimal.ZERO; // Date dernier traitemt
  private BigDecimal pihom = BigDecimal.ZERO; // Date Homologation
  private BigDecimal pirec = BigDecimal.ZERO; // Date Réception
  private BigDecimal pifac = BigDecimal.ZERO; // Date Facturation
  private BigDecimal pinfa = BigDecimal.ZERO; // Numéro de facture
  private BigDecimal pinls = BigDecimal.ZERO; // Ligne sélectable/Fac
  private BigDecimal pipli = BigDecimal.ZERO; // N°Première ligne
  private BigDecimal pidli = BigDecimal.ZERO; // N°Dernière ligne
  private BigDecimal pinli = BigDecimal.ZERO; // N°Ligne en cours
  private BigDecimal pitlv = BigDecimal.ZERO; // Top ligne véritable
  private BigDecimal pitll = BigDecimal.ZERO; // Top ligne liée
  private BigDecimal pidsu = BigDecimal.ZERO; // Dernier suffixe
  private BigDecimal pieta = BigDecimal.ZERO; // Etat du bon
  private BigDecimal piedt = BigDecimal.ZERO; // Top édition
  private BigDecimal pitfc = BigDecimal.ZERO; // Top Frs de commande
  private BigDecimal pitt = BigDecimal.ZERO; // Montants facture forcés
  private BigDecimal pinrg = BigDecimal.ZERO; // Rang réglement
  private BigDecimal pitva1 = BigDecimal.ZERO; // N° TVA 1
  private BigDecimal pitva2 = BigDecimal.ZERO; // N° TVA 2
  private BigDecimal pitva3 = BigDecimal.ZERO; // N° TVA 3
  private BigDecimal pirlv = BigDecimal.ZERO; // Top relevé
  private BigDecimal pinex = BigDecimal.ZERO; // Nbre d'exemplaires
  private BigDecimal piexc = BigDecimal.ZERO; // Rang extension commande
  private BigDecimal piexe = BigDecimal.ZERO; // Rang extension expédition
  private BigDecimal pinrr = BigDecimal.ZERO; // Nbre réglements réel
  private BigDecimal pisgn = BigDecimal.ZERO; // Signe géré
  private BigDecimal piliv = BigDecimal.ZERO; // Top livraison
  private BigDecimal picpt = BigDecimal.ZERO; // Top comptabilisation
  private BigDecimal pipgc = BigDecimal.ZERO; // Top prix garanti
  private BigDecimal pichf = BigDecimal.ZERO; // Top chiffrage
  private BigDecimal piina = BigDecimal.ZERO; // Top affectation
  private BigDecimal picol = BigDecimal.ZERO; // Collectif FRS
  private BigDecimal pifrs = BigDecimal.ZERO; // Code Fournisseur
  private BigDecimal pidlp = BigDecimal.ZERO; // Date liv.Prévue
  private BigDecimal pinum0 = BigDecimal.ZERO; // N°Bon origine
  private BigDecimal pisuf0 = BigDecimal.ZERO; // N°Suf.origine
  private String piavr = ""; // Code Avoir (A ou Blanc)
  private String pitfa = ""; // Code Type de Facturation
  private String pinat = ""; // Nature Bon
  private BigDecimal piesc = BigDecimal.ZERO; // % Escompte
  private BigDecimal pie1g = BigDecimal.ZERO; // Date échéance 1
  private BigDecimal pie2g = BigDecimal.ZERO; // Date échéance 2
  private BigDecimal pie3g = BigDecimal.ZERO; // Date échéance 3
  private BigDecimal pie4g = BigDecimal.ZERO; // Date échéance 4
  private BigDecimal pir1g = BigDecimal.ZERO; // Mtt à régler échéance 1
  private BigDecimal pir2g = BigDecimal.ZERO; // Mtt à régler échéance 2
  private BigDecimal pir3g = BigDecimal.ZERO; // Mtt à régler échéance 3
  private BigDecimal pir4g = BigDecimal.ZERO; // Mtt à régler échéance 4
  private BigDecimal pisl1 = BigDecimal.ZERO; // Soumis T.V.A 1
  private BigDecimal pisl2 = BigDecimal.ZERO; // Soumis T.V.A 2
  private BigDecimal pisl3 = BigDecimal.ZERO; // Soumis T.V.A 3
  private BigDecimal pithtl = BigDecimal.ZERO; // Total Hors Taxes Lignes
  private BigDecimal pitl1 = BigDecimal.ZERO; // Montant TVA 1
  private BigDecimal pitl2 = BigDecimal.ZERO; // Montant TVA 2
  private BigDecimal pitl3 = BigDecimal.ZERO; // Montant TVA 3
  private BigDecimal pitv1 = BigDecimal.ZERO; // Taux TVA 1
  private BigDecimal pitv2 = BigDecimal.ZERO; // Taux TVA 2
  private BigDecimal pitv3 = BigDecimal.ZERO; // Taux TVA 3
  private BigDecimal pimes = BigDecimal.ZERO; // Montant Escompte
  private BigDecimal pittc = BigDecimal.ZERO; // Montant TTC
  private String pirg1 = ""; // Code Règlement 1
  private String piec1 = ""; // Code Echéance 1
  private String pipc1 = ""; // % Règlement 1
  private String pirg2 = ""; // Code Règlement 2
  private String piec2 = ""; // Code Echéance 2
  private String pipc2 = ""; // % Règlement 2
  private String pirg3 = ""; // Code Règlement 3
  private String piec3 = ""; // Code Echéance 3
  private String pipc3 = ""; // % Règlement 3
  private String pirg4 = ""; // Code Règlement 4
  private String piec4 = ""; // Code Echéance 4
  private String pipc4 = ""; // % Règlement 4
  private String pimag = ""; // Code Magasin de Sortie
  private String pirbl = ""; // Référence Bon de livraison
  private String pisan = ""; // Section Analytique
  private String piact = ""; // Code activité ou Affaire
  private String pillv = ""; // Code Lieu de Livraison
  private String piach = ""; // Code Acheteur
  private String picja = ""; // Code Journal d"Achat
  private String pidev = ""; // Code Devise
  private BigDecimal pichg = BigDecimal.ZERO; // Taux de Change
  private BigDecimal picpr = BigDecimal.ZERO; // Coeff. Achat (frais d"appro)
  private BigDecimal pibas = BigDecimal.ZERO; // Base de la Devise
  private String pidos = ""; // Code Dossier appro
  private String picnt = ""; // Code Container
  private BigDecimal pipds = BigDecimal.ZERO; // Poids
  private BigDecimal pivol = BigDecimal.ZERO; // Volume
  private BigDecimal pimta = BigDecimal.ZERO; // Montant frais à répartir
  private BigDecimal pirem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal pirem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal pirem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal pirem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal pirem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal pirem6 = BigDecimal.ZERO; // % Remise 6
  private String pitrl = ""; // Type remise ligne, 1=cascade
  private String pibrl = ""; // Base remise ligne, 1=Montant
  private BigDecimal pirp1 = BigDecimal.ZERO; // % Remise 1 en Pied
  private BigDecimal pirp2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal pirp3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal pirp4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal pirp5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal pirp6 = BigDecimal.ZERO; // % Remise 6
  private String pitrp = ""; // Type remise/Pied, 1=cascade
  private String pitp1 = ""; // Top personnalisé n°1
  private String pitp2 = ""; // Top personnalisé n°2
  private String pitp3 = ""; // Top personnalisé n°3
  private String pitp4 = ""; // Top personnalisé n°4
  private String pitp5 = ""; // Top personnalisé n°5
  private String piin1 = ""; // Litige (1=Non Rap,2=Non Cpt.
  private String piin2 = ""; // utilisé par module S33
  private String piin3 = ""; // Réception définitive
  private String piin4 = ""; // Cpt stocks flottants
  private String piin5 = ""; // blocage comptabilisation
  private String picct = ""; // Code Contrat
  private BigDecimal pidat1 = BigDecimal.ZERO; // Dte Confirmation Fournisseur
  private String pirbc = ""; // Référence Commande
  private BigDecimal piint1 = BigDecimal.ZERO; // Dte Intermédiaire 1
  private BigDecimal piint2 = BigDecimal.ZERO; // Dte Intermédiaire 2
  private String pietai = ""; // Etat intermédiaire
  private String pimex = ""; // Mode d'expédition
  private String pictr = ""; // Code transporteur
  private String pidili = ""; // Traitée par commande DILICOM
  private String piin6 = ""; // Blocage commande,
  private String piin7 = ""; // Scénario de dates
  private String piin8 = ""; // Etat frais fixes
  private String piin9 = ""; // I = Commande Interne
  private String pirfl = ""; // Référence commande longue
  private BigDecimal pipor0 = BigDecimal.ZERO; // montant port théorique
  private BigDecimal pipor1 = BigDecimal.ZERO; // montant port facturé
  private String piin10 = ""; // 1=Port non facturé
  private String piin11 = ""; // N.U.
  private String piin12 = ""; // N.U.
  private String piin13 = ""; // N.U.
  private String piin14 = ""; // N.U.
  private String piin15 = ""; // N.U.
  private String pinom2 = ""; // Nom fournisseur
  private String picpl2 = ""; // Complément du nom
  private String pirue2 = ""; // Rue
  private String piloc2 = ""; // Localité
  private String picdp2 = ""; // Code Postal
  private String pivil2 = ""; // Ville
  private String pinom3 = ""; // Nom du client
  private String picpl3 = ""; // Complément du nom
  private String pirue3 = ""; // Rue
  private String piloc3 = ""; // Localité
  private String picdp3 = ""; // Code Postal
  private String pifil3 = ""; // Code Postal
  private String pivil3 = ""; // Ville
  private String pienv = ""; // Enlevement ou livraison
  private String picntct = ""; // Clé du N° de contact
  private String picodx = ""; // Code ERL pour extract° *
  private String pietbx = ""; // Code établis pour extract°
  private BigDecimal pinumx = BigDecimal.ZERO; // Numéro cmde pour extract°
  private BigDecimal pisufx = BigDecimal.ZERO; // Suffixe pour extract° *
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIUSR), // User pour lect. sécurité
      new AS400ZonedDecimal(SIZE_PITOP, DECIMAL_PITOP), // Top système
      new AS400Text(SIZE_PICOD), // Code ERL "E","F" *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400PackedDecimal(SIZE_PICRE, DECIMAL_PICRE), // Date Création
      new AS400PackedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date dernier traitemt
      new AS400PackedDecimal(SIZE_PIHOM, DECIMAL_PIHOM), // Date Homologation
      new AS400PackedDecimal(SIZE_PIREC, DECIMAL_PIREC), // Date Réception
      new AS400PackedDecimal(SIZE_PIFAC, DECIMAL_PIFAC), // Date Facturation
      new AS400ZonedDecimal(SIZE_PINFA, DECIMAL_PINFA), // Numéro de facture
      new AS400ZonedDecimal(SIZE_PINLS, DECIMAL_PINLS), // Ligne sélectable/Fac
      new AS400PackedDecimal(SIZE_PIPLI, DECIMAL_PIPLI), // N°Première ligne
      new AS400PackedDecimal(SIZE_PIDLI, DECIMAL_PIDLI), // N°Dernière ligne
      new AS400PackedDecimal(SIZE_PINLI, DECIMAL_PINLI), // N°Ligne en cours
      new AS400ZonedDecimal(SIZE_PITLV, DECIMAL_PITLV), // Top ligne véritable
      new AS400ZonedDecimal(SIZE_PITLL, DECIMAL_PITLL), // Top ligne liée
      new AS400ZonedDecimal(SIZE_PIDSU, DECIMAL_PIDSU), // Dernier suffixe
      new AS400ZonedDecimal(SIZE_PIETA, DECIMAL_PIETA), // Etat du bon
      new AS400ZonedDecimal(SIZE_PIEDT, DECIMAL_PIEDT), // Top édition
      new AS400ZonedDecimal(SIZE_PITFC, DECIMAL_PITFC), // Top Frs de commande
      new AS400ZonedDecimal(SIZE_PITT, DECIMAL_PITT), // Montants facture forcés
      new AS400ZonedDecimal(SIZE_PINRG, DECIMAL_PINRG), // Rang réglement
      new AS400ZonedDecimal(SIZE_PITVA1, DECIMAL_PITVA1), // N° TVA 1
      new AS400ZonedDecimal(SIZE_PITVA2, DECIMAL_PITVA2), // N° TVA 2
      new AS400ZonedDecimal(SIZE_PITVA3, DECIMAL_PITVA3), // N° TVA 3
      new AS400ZonedDecimal(SIZE_PIRLV, DECIMAL_PIRLV), // Top relevé
      new AS400ZonedDecimal(SIZE_PINEX, DECIMAL_PINEX), // Nbre d'exemplaires
      new AS400ZonedDecimal(SIZE_PIEXC, DECIMAL_PIEXC), // Rang extension commande
      new AS400ZonedDecimal(SIZE_PIEXE, DECIMAL_PIEXE), // Rang extension expédition
      new AS400ZonedDecimal(SIZE_PINRR, DECIMAL_PINRR), // Nbre réglements réel
      new AS400ZonedDecimal(SIZE_PISGN, DECIMAL_PISGN), // Signe géré
      new AS400ZonedDecimal(SIZE_PILIV, DECIMAL_PILIV), // Top livraison
      new AS400ZonedDecimal(SIZE_PICPT, DECIMAL_PICPT), // Top comptabilisation
      new AS400ZonedDecimal(SIZE_PIPGC, DECIMAL_PIPGC), // Top prix garanti
      new AS400ZonedDecimal(SIZE_PICHF, DECIMAL_PICHF), // Top chiffrage
      new AS400ZonedDecimal(SIZE_PIINA, DECIMAL_PIINA), // Top affectation
      new AS400ZonedDecimal(SIZE_PICOL, DECIMAL_PICOL), // Collectif FRS
      new AS400ZonedDecimal(SIZE_PIFRS, DECIMAL_PIFRS), // Code Fournisseur
      new AS400PackedDecimal(SIZE_PIDLP, DECIMAL_PIDLP), // Date liv.Prévue
      new AS400ZonedDecimal(SIZE_PINUM0, DECIMAL_PINUM0), // N°Bon origine
      new AS400ZonedDecimal(SIZE_PISUF0, DECIMAL_PISUF0), // N°Suf.origine
      new AS400Text(SIZE_PIAVR), // Code Avoir (A ou Blanc)
      new AS400Text(SIZE_PITFA), // Code Type de Facturation
      new AS400Text(SIZE_PINAT), // Nature Bon
      new AS400PackedDecimal(SIZE_PIESC, DECIMAL_PIESC), // % Escompte
      new AS400PackedDecimal(SIZE_PIE1G, DECIMAL_PIE1G), // Date échéance 1
      new AS400PackedDecimal(SIZE_PIE2G, DECIMAL_PIE2G), // Date échéance 2
      new AS400PackedDecimal(SIZE_PIE3G, DECIMAL_PIE3G), // Date échéance 3
      new AS400PackedDecimal(SIZE_PIE4G, DECIMAL_PIE4G), // Date échéance 4
      new AS400PackedDecimal(SIZE_PIR1G, DECIMAL_PIR1G), // Mtt à régler échéance 1
      new AS400PackedDecimal(SIZE_PIR2G, DECIMAL_PIR2G), // Mtt à régler échéance 2
      new AS400PackedDecimal(SIZE_PIR3G, DECIMAL_PIR3G), // Mtt à régler échéance 3
      new AS400PackedDecimal(SIZE_PIR4G, DECIMAL_PIR4G), // Mtt à régler échéance 4
      new AS400PackedDecimal(SIZE_PISL1, DECIMAL_PISL1), // Soumis T.V.A 1
      new AS400PackedDecimal(SIZE_PISL2, DECIMAL_PISL2), // Soumis T.V.A 2
      new AS400PackedDecimal(SIZE_PISL3, DECIMAL_PISL3), // Soumis T.V.A 3
      new AS400PackedDecimal(SIZE_PITHTL, DECIMAL_PITHTL), // Total Hors Taxes Lignes
      new AS400PackedDecimal(SIZE_PITL1, DECIMAL_PITL1), // Montant TVA 1
      new AS400PackedDecimal(SIZE_PITL2, DECIMAL_PITL2), // Montant TVA 2
      new AS400PackedDecimal(SIZE_PITL3, DECIMAL_PITL3), // Montant TVA 3
      new AS400PackedDecimal(SIZE_PITV1, DECIMAL_PITV1), // Taux TVA 1
      new AS400PackedDecimal(SIZE_PITV2, DECIMAL_PITV2), // Taux TVA 2
      new AS400PackedDecimal(SIZE_PITV3, DECIMAL_PITV3), // Taux TVA 3
      new AS400PackedDecimal(SIZE_PIMES, DECIMAL_PIMES), // Montant Escompte
      new AS400PackedDecimal(SIZE_PITTC, DECIMAL_PITTC), // Montant TTC
      new AS400Text(SIZE_PIRG1), // Code Règlement 1
      new AS400Text(SIZE_PIEC1), // Code Echéance 1
      new AS400Text(SIZE_PIPC1), // % Règlement 1
      new AS400Text(SIZE_PIRG2), // Code Règlement 2
      new AS400Text(SIZE_PIEC2), // Code Echéance 2
      new AS400Text(SIZE_PIPC2), // % Règlement 2
      new AS400Text(SIZE_PIRG3), // Code Règlement 3
      new AS400Text(SIZE_PIEC3), // Code Echéance 3
      new AS400Text(SIZE_PIPC3), // % Règlement 3
      new AS400Text(SIZE_PIRG4), // Code Règlement 4
      new AS400Text(SIZE_PIEC4), // Code Echéance 4
      new AS400Text(SIZE_PIPC4), // % Règlement 4
      new AS400Text(SIZE_PIMAG), // Code Magasin de Sortie
      new AS400Text(SIZE_PIRBL), // Référence Bon de livraison
      new AS400Text(SIZE_PISAN), // Section Analytique
      new AS400Text(SIZE_PIACT), // Code activité ou Affaire
      new AS400Text(SIZE_PILLV), // Code Lieu de Livraison
      new AS400Text(SIZE_PIACH), // Code Acheteur
      new AS400Text(SIZE_PICJA), // Code Journal d"Achat
      new AS400Text(SIZE_PIDEV), // Code Devise
      new AS400PackedDecimal(SIZE_PICHG, DECIMAL_PICHG), // Taux de Change
      new AS400PackedDecimal(SIZE_PICPR, DECIMAL_PICPR), // Coeff. Achat (frais d"appro)
      new AS400PackedDecimal(SIZE_PIBAS, DECIMAL_PIBAS), // Base de la Devise
      new AS400Text(SIZE_PIDOS), // Code Dossier appro
      new AS400Text(SIZE_PICNT), // Code Container
      new AS400PackedDecimal(SIZE_PIPDS, DECIMAL_PIPDS), // Poids
      new AS400PackedDecimal(SIZE_PIVOL, DECIMAL_PIVOL), // Volume
      new AS400PackedDecimal(SIZE_PIMTA, DECIMAL_PIMTA), // Montant frais à répartir
      new AS400ZonedDecimal(SIZE_PIREM1, DECIMAL_PIREM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_PIREM2, DECIMAL_PIREM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_PIREM3, DECIMAL_PIREM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_PIREM4, DECIMAL_PIREM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_PIREM5, DECIMAL_PIREM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_PIREM6, DECIMAL_PIREM6), // % Remise 6
      new AS400Text(SIZE_PITRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_PIBRL), // Base remise ligne, 1=Montant
      new AS400ZonedDecimal(SIZE_PIRP1, DECIMAL_PIRP1), // % Remise 1 en Pied
      new AS400ZonedDecimal(SIZE_PIRP2, DECIMAL_PIRP2), // % Remise 2
      new AS400ZonedDecimal(SIZE_PIRP3, DECIMAL_PIRP3), // % Remise 3
      new AS400ZonedDecimal(SIZE_PIRP4, DECIMAL_PIRP4), // % Remise 4
      new AS400ZonedDecimal(SIZE_PIRP5, DECIMAL_PIRP5), // % Remise 5
      new AS400ZonedDecimal(SIZE_PIRP6, DECIMAL_PIRP6), // % Remise 6
      new AS400Text(SIZE_PITRP), // Type remise/Pied, 1=cascade
      new AS400Text(SIZE_PITP1), // Top personnalisé n°1
      new AS400Text(SIZE_PITP2), // Top personnalisé n°2
      new AS400Text(SIZE_PITP3), // Top personnalisé n°3
      new AS400Text(SIZE_PITP4), // Top personnalisé n°4
      new AS400Text(SIZE_PITP5), // Top personnalisé n°5
      new AS400Text(SIZE_PIIN1), // Litige (1=Non Rap,2=Non Cpt.
      new AS400Text(SIZE_PIIN2), // utilisé par module S33
      new AS400Text(SIZE_PIIN3), // Réception définitive
      new AS400Text(SIZE_PIIN4), // Cpt stocks flottants
      new AS400Text(SIZE_PIIN5), // blocage comptabilisation
      new AS400Text(SIZE_PICCT), // Code Contrat
      new AS400PackedDecimal(SIZE_PIDAT1, DECIMAL_PIDAT1), // Dte Confirmation Fournisseur
      new AS400Text(SIZE_PIRBC), // Référence Commande
      new AS400PackedDecimal(SIZE_PIINT1, DECIMAL_PIINT1), // Dte Intermédiaire 1
      new AS400PackedDecimal(SIZE_PIINT2, DECIMAL_PIINT2), // Dte Intermédiaire 2
      new AS400Text(SIZE_PIETAI), // Etat intermédiaire
      new AS400Text(SIZE_PIMEX), // Mode d'expédition
      new AS400Text(SIZE_PICTR), // Code transporteur
      new AS400Text(SIZE_PIDILI), // Traitée par commande DILICOM
      new AS400Text(SIZE_PIIN6), // Blocage commande,
      new AS400Text(SIZE_PIIN7), // Scénario de dates
      new AS400Text(SIZE_PIIN8), // Etat frais fixes
      new AS400Text(SIZE_PIIN9), // I = Commande Interne
      new AS400Text(SIZE_PIRFL), // Référence commande longue
      new AS400ZonedDecimal(SIZE_PIPOR0, DECIMAL_PIPOR0), // montant port théorique
      new AS400ZonedDecimal(SIZE_PIPOR1, DECIMAL_PIPOR1), // montant port facturé
      new AS400Text(SIZE_PIIN10), // 1=Port non facturé
      new AS400Text(SIZE_PIIN11), // N.U.
      new AS400Text(SIZE_PIIN12), // N.U.
      new AS400Text(SIZE_PIIN13), // N.U.
      new AS400Text(SIZE_PIIN14), // N.U.
      new AS400Text(SIZE_PIIN15), // N.U.
      new AS400Text(SIZE_PINOM2), // Nom fournisseur
      new AS400Text(SIZE_PICPL2), // Complément du nom
      new AS400Text(SIZE_PIRUE2), // Rue
      new AS400Text(SIZE_PILOC2), // Localité
      new AS400Text(SIZE_PICDP2), // Code Postal
      new AS400Text(SIZE_PIVIL2), // Ville
      new AS400Text(SIZE_PINOM3), // Nom du client
      new AS400Text(SIZE_PICPL3), // Complément du nom
      new AS400Text(SIZE_PIRUE3), // Rue
      new AS400Text(SIZE_PILOC3), // Localité
      new AS400Text(SIZE_PICDP3), // Code Postal
      new AS400Text(SIZE_PIFIL3), // Code Postal
      new AS400Text(SIZE_PIVIL3), // Ville
      new AS400Text(SIZE_PIENV), // Enlevement ou livraison
      new AS400Text(SIZE_PICNTCT), // Clé du N° de contact
      new AS400Text(SIZE_PICODX), // Code ERL pour extract° *
      new AS400Text(SIZE_PIETBX), // Code établis pour extract°
      new AS400ZonedDecimal(SIZE_PINUMX, DECIMAL_PINUMX), // Numéro cmde pour extract°
      new AS400ZonedDecimal(SIZE_PISUFX, DECIMAL_PISUFX), // Suffixe pour extract° *
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, piusr, pitop, picod, pietb, pinum, pisuf, picre, pidat, pihom, pirec, pifac, pinfa, pinls, pipli, pidli,
          pinli, pitlv, pitll, pidsu, pieta, piedt, pitfc, pitt, pinrg, pitva1, pitva2, pitva3, pirlv, pinex, piexc, piexe, pinrr, pisgn,
          piliv, picpt, pipgc, pichf, piina, picol, pifrs, pidlp, pinum0, pisuf0, piavr, pitfa, pinat, piesc, pie1g, pie2g, pie3g, pie4g,
          pir1g, pir2g, pir3g, pir4g, pisl1, pisl2, pisl3, pithtl, pitl1, pitl2, pitl3, pitv1, pitv2, pitv3, pimes, pittc, pirg1, piec1,
          pipc1, pirg2, piec2, pipc2, pirg3, piec3, pipc3, pirg4, piec4, pipc4, pimag, pirbl, pisan, piact, pillv, piach, picja, pidev,
          pichg, picpr, pibas, pidos, picnt, pipds, pivol, pimta, pirem1, pirem2, pirem3, pirem4, pirem5, pirem6, pitrl, pibrl, pirp1,
          pirp2, pirp3, pirp4, pirp5, pirp6, pitrp, pitp1, pitp2, pitp3, pitp4, pitp5, piin1, piin2, piin3, piin4, piin5, picct, pidat1,
          pirbc, piint1, piint2, pietai, pimex, pictr, pidili, piin6, piin7, piin8, piin9, pirfl, pipor0, pipor1, piin10, piin11, piin12,
          piin13, piin14, piin15, pinom2, picpl2, pirue2, piloc2, picdp2, pivil2, pinom3, picpl3, pirue3, piloc3, picdp3, pifil3, pivil3,
          pienv, picntct, picodx, pietbx, pinumx, pisufx, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    piusr = (String) output[1];
    pitop = (BigDecimal) output[2];
    picod = (String) output[3];
    pietb = (String) output[4];
    pinum = (BigDecimal) output[5];
    pisuf = (BigDecimal) output[6];
    picre = (BigDecimal) output[7];
    pidat = (BigDecimal) output[8];
    pihom = (BigDecimal) output[9];
    pirec = (BigDecimal) output[10];
    pifac = (BigDecimal) output[11];
    pinfa = (BigDecimal) output[12];
    pinls = (BigDecimal) output[13];
    pipli = (BigDecimal) output[14];
    pidli = (BigDecimal) output[15];
    pinli = (BigDecimal) output[16];
    pitlv = (BigDecimal) output[17];
    pitll = (BigDecimal) output[18];
    pidsu = (BigDecimal) output[19];
    pieta = (BigDecimal) output[20];
    piedt = (BigDecimal) output[21];
    pitfc = (BigDecimal) output[22];
    pitt = (BigDecimal) output[23];
    pinrg = (BigDecimal) output[24];
    pitva1 = (BigDecimal) output[25];
    pitva2 = (BigDecimal) output[26];
    pitva3 = (BigDecimal) output[27];
    pirlv = (BigDecimal) output[28];
    pinex = (BigDecimal) output[29];
    piexc = (BigDecimal) output[30];
    piexe = (BigDecimal) output[31];
    pinrr = (BigDecimal) output[32];
    pisgn = (BigDecimal) output[33];
    piliv = (BigDecimal) output[34];
    picpt = (BigDecimal) output[35];
    pipgc = (BigDecimal) output[36];
    pichf = (BigDecimal) output[37];
    piina = (BigDecimal) output[38];
    picol = (BigDecimal) output[39];
    pifrs = (BigDecimal) output[40];
    pidlp = (BigDecimal) output[41];
    pinum0 = (BigDecimal) output[42];
    pisuf0 = (BigDecimal) output[43];
    piavr = (String) output[44];
    pitfa = (String) output[45];
    pinat = (String) output[46];
    piesc = (BigDecimal) output[47];
    pie1g = (BigDecimal) output[48];
    pie2g = (BigDecimal) output[49];
    pie3g = (BigDecimal) output[50];
    pie4g = (BigDecimal) output[51];
    pir1g = (BigDecimal) output[52];
    pir2g = (BigDecimal) output[53];
    pir3g = (BigDecimal) output[54];
    pir4g = (BigDecimal) output[55];
    pisl1 = (BigDecimal) output[56];
    pisl2 = (BigDecimal) output[57];
    pisl3 = (BigDecimal) output[58];
    pithtl = (BigDecimal) output[59];
    pitl1 = (BigDecimal) output[60];
    pitl2 = (BigDecimal) output[61];
    pitl3 = (BigDecimal) output[62];
    pitv1 = (BigDecimal) output[63];
    pitv2 = (BigDecimal) output[64];
    pitv3 = (BigDecimal) output[65];
    pimes = (BigDecimal) output[66];
    pittc = (BigDecimal) output[67];
    pirg1 = (String) output[68];
    piec1 = (String) output[69];
    pipc1 = (String) output[70];
    pirg2 = (String) output[71];
    piec2 = (String) output[72];
    pipc2 = (String) output[73];
    pirg3 = (String) output[74];
    piec3 = (String) output[75];
    pipc3 = (String) output[76];
    pirg4 = (String) output[77];
    piec4 = (String) output[78];
    pipc4 = (String) output[79];
    pimag = (String) output[80];
    pirbl = (String) output[81];
    pisan = (String) output[82];
    piact = (String) output[83];
    pillv = (String) output[84];
    piach = (String) output[85];
    picja = (String) output[86];
    pidev = (String) output[87];
    pichg = (BigDecimal) output[88];
    picpr = (BigDecimal) output[89];
    pibas = (BigDecimal) output[90];
    pidos = (String) output[91];
    picnt = (String) output[92];
    pipds = (BigDecimal) output[93];
    pivol = (BigDecimal) output[94];
    pimta = (BigDecimal) output[95];
    pirem1 = (BigDecimal) output[96];
    pirem2 = (BigDecimal) output[97];
    pirem3 = (BigDecimal) output[98];
    pirem4 = (BigDecimal) output[99];
    pirem5 = (BigDecimal) output[100];
    pirem6 = (BigDecimal) output[101];
    pitrl = (String) output[102];
    pibrl = (String) output[103];
    pirp1 = (BigDecimal) output[104];
    pirp2 = (BigDecimal) output[105];
    pirp3 = (BigDecimal) output[106];
    pirp4 = (BigDecimal) output[107];
    pirp5 = (BigDecimal) output[108];
    pirp6 = (BigDecimal) output[109];
    pitrp = (String) output[110];
    pitp1 = (String) output[111];
    pitp2 = (String) output[112];
    pitp3 = (String) output[113];
    pitp4 = (String) output[114];
    pitp5 = (String) output[115];
    piin1 = (String) output[116];
    piin2 = (String) output[117];
    piin3 = (String) output[118];
    piin4 = (String) output[119];
    piin5 = (String) output[120];
    picct = (String) output[121];
    pidat1 = (BigDecimal) output[122];
    pirbc = (String) output[123];
    piint1 = (BigDecimal) output[124];
    piint2 = (BigDecimal) output[125];
    pietai = (String) output[126];
    pimex = (String) output[127];
    pictr = (String) output[128];
    pidili = (String) output[129];
    piin6 = (String) output[130];
    piin7 = (String) output[131];
    piin8 = (String) output[132];
    piin9 = (String) output[133];
    pirfl = (String) output[134];
    pipor0 = (BigDecimal) output[135];
    pipor1 = (BigDecimal) output[136];
    piin10 = (String) output[137];
    piin11 = (String) output[138];
    piin12 = (String) output[139];
    piin13 = (String) output[140];
    piin14 = (String) output[141];
    piin15 = (String) output[142];
    pinom2 = (String) output[143];
    picpl2 = (String) output[144];
    pirue2 = (String) output[145];
    piloc2 = (String) output[146];
    picdp2 = (String) output[147];
    pivil2 = (String) output[148];
    pinom3 = (String) output[149];
    picpl3 = (String) output[150];
    pirue3 = (String) output[151];
    piloc3 = (String) output[152];
    picdp3 = (String) output[153];
    pifil3 = (String) output[154];
    pivil3 = (String) output[155];
    pienv = (String) output[156];
    picntct = (String) output[157];
    picodx = (String) output[158];
    pietbx = (String) output[159];
    pinumx = (BigDecimal) output[160];
    pisufx = (BigDecimal) output[161];
    piarr = (String) output[162];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPiusr(String pPiusr) {
    if (pPiusr == null) {
      return;
    }
    piusr = pPiusr;
  }
  
  public String getPiusr() {
    return piusr;
  }
  
  public void setPitop(BigDecimal pPitop) {
    if (pPitop == null) {
      return;
    }
    pitop = pPitop.setScale(DECIMAL_PITOP, RoundingMode.HALF_UP);
  }
  
  public void setPitop(Integer pPitop) {
    if (pPitop == null) {
      return;
    }
    pitop = BigDecimal.valueOf(pPitop);
  }
  
  public Integer getPitop() {
    return pitop.intValue();
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPicre(BigDecimal pPicre) {
    if (pPicre == null) {
      return;
    }
    picre = pPicre.setScale(DECIMAL_PICRE, RoundingMode.HALF_UP);
  }
  
  public void setPicre(Integer pPicre) {
    if (pPicre == null) {
      return;
    }
    picre = BigDecimal.valueOf(pPicre);
  }
  
  public void setPicre(Date pPicre) {
    if (pPicre == null) {
      return;
    }
    picre = BigDecimal.valueOf(ConvertDate.dateToDb2(pPicre));
  }
  
  public Integer getPicre() {
    return picre.intValue();
  }
  
  public Date getPicreConvertiEnDate() {
    return ConvertDate.db2ToDate(picre.intValue(), null);
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPihom(BigDecimal pPihom) {
    if (pPihom == null) {
      return;
    }
    pihom = pPihom.setScale(DECIMAL_PIHOM, RoundingMode.HALF_UP);
  }
  
  public void setPihom(Integer pPihom) {
    if (pPihom == null) {
      return;
    }
    pihom = BigDecimal.valueOf(pPihom);
  }
  
  public void setPihom(Date pPihom) {
    if (pPihom == null) {
      return;
    }
    pihom = BigDecimal.valueOf(ConvertDate.dateToDb2(pPihom));
  }
  
  public Integer getPihom() {
    return pihom.intValue();
  }
  
  public Date getPihomConvertiEnDate() {
    return ConvertDate.db2ToDate(pihom.intValue(), null);
  }
  
  public void setPirec(BigDecimal pPirec) {
    if (pPirec == null) {
      return;
    }
    pirec = pPirec.setScale(DECIMAL_PIREC, RoundingMode.HALF_UP);
  }
  
  public void setPirec(Integer pPirec) {
    if (pPirec == null) {
      return;
    }
    pirec = BigDecimal.valueOf(pPirec);
  }
  
  public void setPirec(Date pPirec) {
    if (pPirec == null) {
      return;
    }
    pirec = BigDecimal.valueOf(ConvertDate.dateToDb2(pPirec));
  }
  
  public Integer getPirec() {
    return pirec.intValue();
  }
  
  public Date getPirecConvertiEnDate() {
    return ConvertDate.db2ToDate(pirec.intValue(), null);
  }
  
  public void setPifac(BigDecimal pPifac) {
    if (pPifac == null) {
      return;
    }
    pifac = pPifac.setScale(DECIMAL_PIFAC, RoundingMode.HALF_UP);
  }
  
  public void setPifac(Integer pPifac) {
    if (pPifac == null) {
      return;
    }
    pifac = BigDecimal.valueOf(pPifac);
  }
  
  public void setPifac(Date pPifac) {
    if (pPifac == null) {
      return;
    }
    pifac = BigDecimal.valueOf(ConvertDate.dateToDb2(pPifac));
  }
  
  public Integer getPifac() {
    return pifac.intValue();
  }
  
  public Date getPifacConvertiEnDate() {
    return ConvertDate.db2ToDate(pifac.intValue(), null);
  }
  
  public void setPinfa(BigDecimal pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = pPinfa.setScale(DECIMAL_PINFA, RoundingMode.HALF_UP);
  }
  
  public void setPinfa(Integer pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(pPinfa);
  }
  
  public Integer getPinfa() {
    return pinfa.intValue();
  }
  
  public void setPinls(BigDecimal pPinls) {
    if (pPinls == null) {
      return;
    }
    pinls = pPinls.setScale(DECIMAL_PINLS, RoundingMode.HALF_UP);
  }
  
  public void setPinls(Integer pPinls) {
    if (pPinls == null) {
      return;
    }
    pinls = BigDecimal.valueOf(pPinls);
  }
  
  public Integer getPinls() {
    return pinls.intValue();
  }
  
  public void setPipli(BigDecimal pPipli) {
    if (pPipli == null) {
      return;
    }
    pipli = pPipli.setScale(DECIMAL_PIPLI, RoundingMode.HALF_UP);
  }
  
  public void setPipli(Integer pPipli) {
    if (pPipli == null) {
      return;
    }
    pipli = BigDecimal.valueOf(pPipli);
  }
  
  public Integer getPipli() {
    return pipli.intValue();
  }
  
  public void setPidli(BigDecimal pPidli) {
    if (pPidli == null) {
      return;
    }
    pidli = pPidli.setScale(DECIMAL_PIDLI, RoundingMode.HALF_UP);
  }
  
  public void setPidli(Integer pPidli) {
    if (pPidli == null) {
      return;
    }
    pidli = BigDecimal.valueOf(pPidli);
  }
  
  public Integer getPidli() {
    return pidli.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPitlv(BigDecimal pPitlv) {
    if (pPitlv == null) {
      return;
    }
    pitlv = pPitlv.setScale(DECIMAL_PITLV, RoundingMode.HALF_UP);
  }
  
  public void setPitlv(Integer pPitlv) {
    if (pPitlv == null) {
      return;
    }
    pitlv = BigDecimal.valueOf(pPitlv);
  }
  
  public Integer getPitlv() {
    return pitlv.intValue();
  }
  
  public void setPitll(BigDecimal pPitll) {
    if (pPitll == null) {
      return;
    }
    pitll = pPitll.setScale(DECIMAL_PITLL, RoundingMode.HALF_UP);
  }
  
  public void setPitll(Integer pPitll) {
    if (pPitll == null) {
      return;
    }
    pitll = BigDecimal.valueOf(pPitll);
  }
  
  public Integer getPitll() {
    return pitll.intValue();
  }
  
  public void setPidsu(BigDecimal pPidsu) {
    if (pPidsu == null) {
      return;
    }
    pidsu = pPidsu.setScale(DECIMAL_PIDSU, RoundingMode.HALF_UP);
  }
  
  public void setPidsu(Integer pPidsu) {
    if (pPidsu == null) {
      return;
    }
    pidsu = BigDecimal.valueOf(pPidsu);
  }
  
  public Integer getPidsu() {
    return pidsu.intValue();
  }
  
  public void setPieta(BigDecimal pPieta) {
    if (pPieta == null) {
      return;
    }
    pieta = pPieta.setScale(DECIMAL_PIETA, RoundingMode.HALF_UP);
  }
  
  public void setPieta(Integer pPieta) {
    if (pPieta == null) {
      return;
    }
    pieta = BigDecimal.valueOf(pPieta);
  }
  
  public Integer getPieta() {
    return pieta.intValue();
  }
  
  public void setPiedt(BigDecimal pPiedt) {
    if (pPiedt == null) {
      return;
    }
    piedt = pPiedt.setScale(DECIMAL_PIEDT, RoundingMode.HALF_UP);
  }
  
  public void setPiedt(Integer pPiedt) {
    if (pPiedt == null) {
      return;
    }
    piedt = BigDecimal.valueOf(pPiedt);
  }
  
  public Integer getPiedt() {
    return piedt.intValue();
  }
  
  public void setPitfc(BigDecimal pPitfc) {
    if (pPitfc == null) {
      return;
    }
    pitfc = pPitfc.setScale(DECIMAL_PITFC, RoundingMode.HALF_UP);
  }
  
  public void setPitfc(Integer pPitfc) {
    if (pPitfc == null) {
      return;
    }
    pitfc = BigDecimal.valueOf(pPitfc);
  }
  
  public Integer getPitfc() {
    return pitfc.intValue();
  }
  
  public void setPitt(BigDecimal pPitt) {
    if (pPitt == null) {
      return;
    }
    pitt = pPitt.setScale(DECIMAL_PITT, RoundingMode.HALF_UP);
  }
  
  public void setPitt(Integer pPitt) {
    if (pPitt == null) {
      return;
    }
    pitt = BigDecimal.valueOf(pPitt);
  }
  
  public Integer getPitt() {
    return pitt.intValue();
  }
  
  public void setPinrg(BigDecimal pPinrg) {
    if (pPinrg == null) {
      return;
    }
    pinrg = pPinrg.setScale(DECIMAL_PINRG, RoundingMode.HALF_UP);
  }
  
  public void setPinrg(Integer pPinrg) {
    if (pPinrg == null) {
      return;
    }
    pinrg = BigDecimal.valueOf(pPinrg);
  }
  
  public Integer getPinrg() {
    return pinrg.intValue();
  }
  
  public void setPitva1(BigDecimal pPitva1) {
    if (pPitva1 == null) {
      return;
    }
    pitva1 = pPitva1.setScale(DECIMAL_PITVA1, RoundingMode.HALF_UP);
  }
  
  public void setPitva1(Integer pPitva1) {
    if (pPitva1 == null) {
      return;
    }
    pitva1 = BigDecimal.valueOf(pPitva1);
  }
  
  public Integer getPitva1() {
    return pitva1.intValue();
  }
  
  public void setPitva2(BigDecimal pPitva2) {
    if (pPitva2 == null) {
      return;
    }
    pitva2 = pPitva2.setScale(DECIMAL_PITVA2, RoundingMode.HALF_UP);
  }
  
  public void setPitva2(Integer pPitva2) {
    if (pPitva2 == null) {
      return;
    }
    pitva2 = BigDecimal.valueOf(pPitva2);
  }
  
  public Integer getPitva2() {
    return pitva2.intValue();
  }
  
  public void setPitva3(BigDecimal pPitva3) {
    if (pPitva3 == null) {
      return;
    }
    pitva3 = pPitva3.setScale(DECIMAL_PITVA3, RoundingMode.HALF_UP);
  }
  
  public void setPitva3(Integer pPitva3) {
    if (pPitva3 == null) {
      return;
    }
    pitva3 = BigDecimal.valueOf(pPitva3);
  }
  
  public Integer getPitva3() {
    return pitva3.intValue();
  }
  
  public void setPirlv(BigDecimal pPirlv) {
    if (pPirlv == null) {
      return;
    }
    pirlv = pPirlv.setScale(DECIMAL_PIRLV, RoundingMode.HALF_UP);
  }
  
  public void setPirlv(Integer pPirlv) {
    if (pPirlv == null) {
      return;
    }
    pirlv = BigDecimal.valueOf(pPirlv);
  }
  
  public Integer getPirlv() {
    return pirlv.intValue();
  }
  
  public void setPinex(BigDecimal pPinex) {
    if (pPinex == null) {
      return;
    }
    pinex = pPinex.setScale(DECIMAL_PINEX, RoundingMode.HALF_UP);
  }
  
  public void setPinex(Integer pPinex) {
    if (pPinex == null) {
      return;
    }
    pinex = BigDecimal.valueOf(pPinex);
  }
  
  public Integer getPinex() {
    return pinex.intValue();
  }
  
  public void setPiexc(BigDecimal pPiexc) {
    if (pPiexc == null) {
      return;
    }
    piexc = pPiexc.setScale(DECIMAL_PIEXC, RoundingMode.HALF_UP);
  }
  
  public void setPiexc(Integer pPiexc) {
    if (pPiexc == null) {
      return;
    }
    piexc = BigDecimal.valueOf(pPiexc);
  }
  
  public Integer getPiexc() {
    return piexc.intValue();
  }
  
  public void setPiexe(BigDecimal pPiexe) {
    if (pPiexe == null) {
      return;
    }
    piexe = pPiexe.setScale(DECIMAL_PIEXE, RoundingMode.HALF_UP);
  }
  
  public void setPiexe(Integer pPiexe) {
    if (pPiexe == null) {
      return;
    }
    piexe = BigDecimal.valueOf(pPiexe);
  }
  
  public Integer getPiexe() {
    return piexe.intValue();
  }
  
  public void setPinrr(BigDecimal pPinrr) {
    if (pPinrr == null) {
      return;
    }
    pinrr = pPinrr.setScale(DECIMAL_PINRR, RoundingMode.HALF_UP);
  }
  
  public void setPinrr(Integer pPinrr) {
    if (pPinrr == null) {
      return;
    }
    pinrr = BigDecimal.valueOf(pPinrr);
  }
  
  public Integer getPinrr() {
    return pinrr.intValue();
  }
  
  public void setPisgn(BigDecimal pPisgn) {
    if (pPisgn == null) {
      return;
    }
    pisgn = pPisgn.setScale(DECIMAL_PISGN, RoundingMode.HALF_UP);
  }
  
  public void setPisgn(Integer pPisgn) {
    if (pPisgn == null) {
      return;
    }
    pisgn = BigDecimal.valueOf(pPisgn);
  }
  
  public Integer getPisgn() {
    return pisgn.intValue();
  }
  
  public void setPiliv(BigDecimal pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = pPiliv.setScale(DECIMAL_PILIV, RoundingMode.HALF_UP);
  }
  
  public void setPiliv(Integer pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = BigDecimal.valueOf(pPiliv);
  }
  
  public Integer getPiliv() {
    return piliv.intValue();
  }
  
  public void setPicpt(BigDecimal pPicpt) {
    if (pPicpt == null) {
      return;
    }
    picpt = pPicpt.setScale(DECIMAL_PICPT, RoundingMode.HALF_UP);
  }
  
  public void setPicpt(Integer pPicpt) {
    if (pPicpt == null) {
      return;
    }
    picpt = BigDecimal.valueOf(pPicpt);
  }
  
  public Integer getPicpt() {
    return picpt.intValue();
  }
  
  public void setPipgc(BigDecimal pPipgc) {
    if (pPipgc == null) {
      return;
    }
    pipgc = pPipgc.setScale(DECIMAL_PIPGC, RoundingMode.HALF_UP);
  }
  
  public void setPipgc(Integer pPipgc) {
    if (pPipgc == null) {
      return;
    }
    pipgc = BigDecimal.valueOf(pPipgc);
  }
  
  public Integer getPipgc() {
    return pipgc.intValue();
  }
  
  public void setPichf(BigDecimal pPichf) {
    if (pPichf == null) {
      return;
    }
    pichf = pPichf.setScale(DECIMAL_PICHF, RoundingMode.HALF_UP);
  }
  
  public void setPichf(Integer pPichf) {
    if (pPichf == null) {
      return;
    }
    pichf = BigDecimal.valueOf(pPichf);
  }
  
  public Integer getPichf() {
    return pichf.intValue();
  }
  
  public void setPiina(BigDecimal pPiina) {
    if (pPiina == null) {
      return;
    }
    piina = pPiina.setScale(DECIMAL_PIINA, RoundingMode.HALF_UP);
  }
  
  public void setPiina(Integer pPiina) {
    if (pPiina == null) {
      return;
    }
    piina = BigDecimal.valueOf(pPiina);
  }
  
  public Integer getPiina() {
    return piina.intValue();
  }
  
  public void setPicol(BigDecimal pPicol) {
    if (pPicol == null) {
      return;
    }
    picol = pPicol.setScale(DECIMAL_PICOL, RoundingMode.HALF_UP);
  }
  
  public void setPicol(Integer pPicol) {
    if (pPicol == null) {
      return;
    }
    picol = BigDecimal.valueOf(pPicol);
  }
  
  public Integer getPicol() {
    return picol.intValue();
  }
  
  public void setPifrs(BigDecimal pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = pPifrs.setScale(DECIMAL_PIFRS, RoundingMode.HALF_UP);
  }
  
  public void setPifrs(Integer pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = BigDecimal.valueOf(pPifrs);
  }
  
  public Integer getPifrs() {
    return pifrs.intValue();
  }
  
  public void setPidlp(BigDecimal pPidlp) {
    if (pPidlp == null) {
      return;
    }
    pidlp = pPidlp.setScale(DECIMAL_PIDLP, RoundingMode.HALF_UP);
  }
  
  public void setPidlp(Integer pPidlp) {
    if (pPidlp == null) {
      return;
    }
    pidlp = BigDecimal.valueOf(pPidlp);
  }
  
  public void setPidlp(Date pPidlp) {
    if (pPidlp == null) {
      return;
    }
    pidlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidlp));
  }
  
  public Integer getPidlp() {
    return pidlp.intValue();
  }
  
  public Date getPidlpConvertiEnDate() {
    return ConvertDate.db2ToDate(pidlp.intValue(), null);
  }
  
  public void setPinum0(BigDecimal pPinum0) {
    if (pPinum0 == null) {
      return;
    }
    pinum0 = pPinum0.setScale(DECIMAL_PINUM0, RoundingMode.HALF_UP);
  }
  
  public void setPinum0(Integer pPinum0) {
    if (pPinum0 == null) {
      return;
    }
    pinum0 = BigDecimal.valueOf(pPinum0);
  }
  
  public Integer getPinum0() {
    return pinum0.intValue();
  }
  
  public void setPisuf0(BigDecimal pPisuf0) {
    if (pPisuf0 == null) {
      return;
    }
    pisuf0 = pPisuf0.setScale(DECIMAL_PISUF0, RoundingMode.HALF_UP);
  }
  
  public void setPisuf0(Integer pPisuf0) {
    if (pPisuf0 == null) {
      return;
    }
    pisuf0 = BigDecimal.valueOf(pPisuf0);
  }
  
  public Integer getPisuf0() {
    return pisuf0.intValue();
  }
  
  public void setPiavr(Character pPiavr) {
    if (pPiavr == null) {
      return;
    }
    piavr = String.valueOf(pPiavr);
  }
  
  public Character getPiavr() {
    return piavr.charAt(0);
  }
  
  public void setPitfa(Character pPitfa) {
    if (pPitfa == null) {
      return;
    }
    pitfa = String.valueOf(pPitfa);
  }
  
  public Character getPitfa() {
    return pitfa.charAt(0);
  }
  
  public void setPinat(Character pPinat) {
    if (pPinat == null) {
      return;
    }
    pinat = String.valueOf(pPinat);
  }
  
  public Character getPinat() {
    return pinat.charAt(0);
  }
  
  public void setPiesc(BigDecimal pPiesc) {
    if (pPiesc == null) {
      return;
    }
    piesc = pPiesc.setScale(DECIMAL_PIESC, RoundingMode.HALF_UP);
  }
  
  public void setPiesc(Double pPiesc) {
    if (pPiesc == null) {
      return;
    }
    piesc = BigDecimal.valueOf(pPiesc).setScale(DECIMAL_PIESC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiesc() {
    return piesc.setScale(DECIMAL_PIESC, RoundingMode.HALF_UP);
  }
  
  public void setPie1g(BigDecimal pPie1g) {
    if (pPie1g == null) {
      return;
    }
    pie1g = pPie1g.setScale(DECIMAL_PIE1G, RoundingMode.HALF_UP);
  }
  
  public void setPie1g(Integer pPie1g) {
    if (pPie1g == null) {
      return;
    }
    pie1g = BigDecimal.valueOf(pPie1g);
  }
  
  public void setPie1g(Date pPie1g) {
    if (pPie1g == null) {
      return;
    }
    pie1g = BigDecimal.valueOf(ConvertDate.dateToDb2(pPie1g));
  }
  
  public Integer getPie1g() {
    return pie1g.intValue();
  }
  
  public Date getPie1gConvertiEnDate() {
    return ConvertDate.db2ToDate(pie1g.intValue(), null);
  }
  
  public void setPie2g(BigDecimal pPie2g) {
    if (pPie2g == null) {
      return;
    }
    pie2g = pPie2g.setScale(DECIMAL_PIE2G, RoundingMode.HALF_UP);
  }
  
  public void setPie2g(Integer pPie2g) {
    if (pPie2g == null) {
      return;
    }
    pie2g = BigDecimal.valueOf(pPie2g);
  }
  
  public void setPie2g(Date pPie2g) {
    if (pPie2g == null) {
      return;
    }
    pie2g = BigDecimal.valueOf(ConvertDate.dateToDb2(pPie2g));
  }
  
  public Integer getPie2g() {
    return pie2g.intValue();
  }
  
  public Date getPie2gConvertiEnDate() {
    return ConvertDate.db2ToDate(pie2g.intValue(), null);
  }
  
  public void setPie3g(BigDecimal pPie3g) {
    if (pPie3g == null) {
      return;
    }
    pie3g = pPie3g.setScale(DECIMAL_PIE3G, RoundingMode.HALF_UP);
  }
  
  public void setPie3g(Integer pPie3g) {
    if (pPie3g == null) {
      return;
    }
    pie3g = BigDecimal.valueOf(pPie3g);
  }
  
  public void setPie3g(Date pPie3g) {
    if (pPie3g == null) {
      return;
    }
    pie3g = BigDecimal.valueOf(ConvertDate.dateToDb2(pPie3g));
  }
  
  public Integer getPie3g() {
    return pie3g.intValue();
  }
  
  public Date getPie3gConvertiEnDate() {
    return ConvertDate.db2ToDate(pie3g.intValue(), null);
  }
  
  public void setPie4g(BigDecimal pPie4g) {
    if (pPie4g == null) {
      return;
    }
    pie4g = pPie4g.setScale(DECIMAL_PIE4G, RoundingMode.HALF_UP);
  }
  
  public void setPie4g(Integer pPie4g) {
    if (pPie4g == null) {
      return;
    }
    pie4g = BigDecimal.valueOf(pPie4g);
  }
  
  public void setPie4g(Date pPie4g) {
    if (pPie4g == null) {
      return;
    }
    pie4g = BigDecimal.valueOf(ConvertDate.dateToDb2(pPie4g));
  }
  
  public Integer getPie4g() {
    return pie4g.intValue();
  }
  
  public Date getPie4gConvertiEnDate() {
    return ConvertDate.db2ToDate(pie4g.intValue(), null);
  }
  
  public void setPir1g(BigDecimal pPir1g) {
    if (pPir1g == null) {
      return;
    }
    pir1g = pPir1g.setScale(DECIMAL_PIR1G, RoundingMode.HALF_UP);
  }
  
  public void setPir1g(Double pPir1g) {
    if (pPir1g == null) {
      return;
    }
    pir1g = BigDecimal.valueOf(pPir1g).setScale(DECIMAL_PIR1G, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPir1g() {
    return pir1g.setScale(DECIMAL_PIR1G, RoundingMode.HALF_UP);
  }
  
  public void setPir2g(BigDecimal pPir2g) {
    if (pPir2g == null) {
      return;
    }
    pir2g = pPir2g.setScale(DECIMAL_PIR2G, RoundingMode.HALF_UP);
  }
  
  public void setPir2g(Double pPir2g) {
    if (pPir2g == null) {
      return;
    }
    pir2g = BigDecimal.valueOf(pPir2g).setScale(DECIMAL_PIR2G, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPir2g() {
    return pir2g.setScale(DECIMAL_PIR2G, RoundingMode.HALF_UP);
  }
  
  public void setPir3g(BigDecimal pPir3g) {
    if (pPir3g == null) {
      return;
    }
    pir3g = pPir3g.setScale(DECIMAL_PIR3G, RoundingMode.HALF_UP);
  }
  
  public void setPir3g(Double pPir3g) {
    if (pPir3g == null) {
      return;
    }
    pir3g = BigDecimal.valueOf(pPir3g).setScale(DECIMAL_PIR3G, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPir3g() {
    return pir3g.setScale(DECIMAL_PIR3G, RoundingMode.HALF_UP);
  }
  
  public void setPir4g(BigDecimal pPir4g) {
    if (pPir4g == null) {
      return;
    }
    pir4g = pPir4g.setScale(DECIMAL_PIR4G, RoundingMode.HALF_UP);
  }
  
  public void setPir4g(Double pPir4g) {
    if (pPir4g == null) {
      return;
    }
    pir4g = BigDecimal.valueOf(pPir4g).setScale(DECIMAL_PIR4G, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPir4g() {
    return pir4g.setScale(DECIMAL_PIR4G, RoundingMode.HALF_UP);
  }
  
  public void setPisl1(BigDecimal pPisl1) {
    if (pPisl1 == null) {
      return;
    }
    pisl1 = pPisl1.setScale(DECIMAL_PISL1, RoundingMode.HALF_UP);
  }
  
  public void setPisl1(Double pPisl1) {
    if (pPisl1 == null) {
      return;
    }
    pisl1 = BigDecimal.valueOf(pPisl1).setScale(DECIMAL_PISL1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPisl1() {
    return pisl1.setScale(DECIMAL_PISL1, RoundingMode.HALF_UP);
  }
  
  public void setPisl2(BigDecimal pPisl2) {
    if (pPisl2 == null) {
      return;
    }
    pisl2 = pPisl2.setScale(DECIMAL_PISL2, RoundingMode.HALF_UP);
  }
  
  public void setPisl2(Double pPisl2) {
    if (pPisl2 == null) {
      return;
    }
    pisl2 = BigDecimal.valueOf(pPisl2).setScale(DECIMAL_PISL2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPisl2() {
    return pisl2.setScale(DECIMAL_PISL2, RoundingMode.HALF_UP);
  }
  
  public void setPisl3(BigDecimal pPisl3) {
    if (pPisl3 == null) {
      return;
    }
    pisl3 = pPisl3.setScale(DECIMAL_PISL3, RoundingMode.HALF_UP);
  }
  
  public void setPisl3(Double pPisl3) {
    if (pPisl3 == null) {
      return;
    }
    pisl3 = BigDecimal.valueOf(pPisl3).setScale(DECIMAL_PISL3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPisl3() {
    return pisl3.setScale(DECIMAL_PISL3, RoundingMode.HALF_UP);
  }
  
  public void setPithtl(BigDecimal pPithtl) {
    if (pPithtl == null) {
      return;
    }
    pithtl = pPithtl.setScale(DECIMAL_PITHTL, RoundingMode.HALF_UP);
  }
  
  public void setPithtl(Double pPithtl) {
    if (pPithtl == null) {
      return;
    }
    pithtl = BigDecimal.valueOf(pPithtl).setScale(DECIMAL_PITHTL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPithtl() {
    return pithtl.setScale(DECIMAL_PITHTL, RoundingMode.HALF_UP);
  }
  
  public void setPitl1(BigDecimal pPitl1) {
    if (pPitl1 == null) {
      return;
    }
    pitl1 = pPitl1.setScale(DECIMAL_PITL1, RoundingMode.HALF_UP);
  }
  
  public void setPitl1(Double pPitl1) {
    if (pPitl1 == null) {
      return;
    }
    pitl1 = BigDecimal.valueOf(pPitl1).setScale(DECIMAL_PITL1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPitl1() {
    return pitl1.setScale(DECIMAL_PITL1, RoundingMode.HALF_UP);
  }
  
  public void setPitl2(BigDecimal pPitl2) {
    if (pPitl2 == null) {
      return;
    }
    pitl2 = pPitl2.setScale(DECIMAL_PITL2, RoundingMode.HALF_UP);
  }
  
  public void setPitl2(Double pPitl2) {
    if (pPitl2 == null) {
      return;
    }
    pitl2 = BigDecimal.valueOf(pPitl2).setScale(DECIMAL_PITL2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPitl2() {
    return pitl2.setScale(DECIMAL_PITL2, RoundingMode.HALF_UP);
  }
  
  public void setPitl3(BigDecimal pPitl3) {
    if (pPitl3 == null) {
      return;
    }
    pitl3 = pPitl3.setScale(DECIMAL_PITL3, RoundingMode.HALF_UP);
  }
  
  public void setPitl3(Double pPitl3) {
    if (pPitl3 == null) {
      return;
    }
    pitl3 = BigDecimal.valueOf(pPitl3).setScale(DECIMAL_PITL3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPitl3() {
    return pitl3.setScale(DECIMAL_PITL3, RoundingMode.HALF_UP);
  }
  
  public void setPitv1(BigDecimal pPitv1) {
    if (pPitv1 == null) {
      return;
    }
    pitv1 = pPitv1.setScale(DECIMAL_PITV1, RoundingMode.HALF_UP);
  }
  
  public void setPitv1(Double pPitv1) {
    if (pPitv1 == null) {
      return;
    }
    pitv1 = BigDecimal.valueOf(pPitv1).setScale(DECIMAL_PITV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPitv1() {
    return pitv1.setScale(DECIMAL_PITV1, RoundingMode.HALF_UP);
  }
  
  public void setPitv2(BigDecimal pPitv2) {
    if (pPitv2 == null) {
      return;
    }
    pitv2 = pPitv2.setScale(DECIMAL_PITV2, RoundingMode.HALF_UP);
  }
  
  public void setPitv2(Double pPitv2) {
    if (pPitv2 == null) {
      return;
    }
    pitv2 = BigDecimal.valueOf(pPitv2).setScale(DECIMAL_PITV2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPitv2() {
    return pitv2.setScale(DECIMAL_PITV2, RoundingMode.HALF_UP);
  }
  
  public void setPitv3(BigDecimal pPitv3) {
    if (pPitv3 == null) {
      return;
    }
    pitv3 = pPitv3.setScale(DECIMAL_PITV3, RoundingMode.HALF_UP);
  }
  
  public void setPitv3(Double pPitv3) {
    if (pPitv3 == null) {
      return;
    }
    pitv3 = BigDecimal.valueOf(pPitv3).setScale(DECIMAL_PITV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPitv3() {
    return pitv3.setScale(DECIMAL_PITV3, RoundingMode.HALF_UP);
  }
  
  public void setPimes(BigDecimal pPimes) {
    if (pPimes == null) {
      return;
    }
    pimes = pPimes.setScale(DECIMAL_PIMES, RoundingMode.HALF_UP);
  }
  
  public void setPimes(Double pPimes) {
    if (pPimes == null) {
      return;
    }
    pimes = BigDecimal.valueOf(pPimes).setScale(DECIMAL_PIMES, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPimes() {
    return pimes.setScale(DECIMAL_PIMES, RoundingMode.HALF_UP);
  }
  
  public void setPittc(BigDecimal pPittc) {
    if (pPittc == null) {
      return;
    }
    pittc = pPittc.setScale(DECIMAL_PITTC, RoundingMode.HALF_UP);
  }
  
  public void setPittc(Double pPittc) {
    if (pPittc == null) {
      return;
    }
    pittc = BigDecimal.valueOf(pPittc).setScale(DECIMAL_PITTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPittc() {
    return pittc.setScale(DECIMAL_PITTC, RoundingMode.HALF_UP);
  }
  
  public void setPirg1(String pPirg1) {
    if (pPirg1 == null) {
      return;
    }
    pirg1 = pPirg1;
  }
  
  public String getPirg1() {
    return pirg1;
  }
  
  public void setPiec1(String pPiec1) {
    if (pPiec1 == null) {
      return;
    }
    piec1 = pPiec1;
  }
  
  public String getPiec1() {
    return piec1;
  }
  
  public void setPipc1(String pPipc1) {
    if (pPipc1 == null) {
      return;
    }
    pipc1 = pPipc1;
  }
  
  public String getPipc1() {
    return pipc1;
  }
  
  public void setPirg2(String pPirg2) {
    if (pPirg2 == null) {
      return;
    }
    pirg2 = pPirg2;
  }
  
  public String getPirg2() {
    return pirg2;
  }
  
  public void setPiec2(String pPiec2) {
    if (pPiec2 == null) {
      return;
    }
    piec2 = pPiec2;
  }
  
  public String getPiec2() {
    return piec2;
  }
  
  public void setPipc2(String pPipc2) {
    if (pPipc2 == null) {
      return;
    }
    pipc2 = pPipc2;
  }
  
  public String getPipc2() {
    return pipc2;
  }
  
  public void setPirg3(String pPirg3) {
    if (pPirg3 == null) {
      return;
    }
    pirg3 = pPirg3;
  }
  
  public String getPirg3() {
    return pirg3;
  }
  
  public void setPiec3(String pPiec3) {
    if (pPiec3 == null) {
      return;
    }
    piec3 = pPiec3;
  }
  
  public String getPiec3() {
    return piec3;
  }
  
  public void setPipc3(String pPipc3) {
    if (pPipc3 == null) {
      return;
    }
    pipc3 = pPipc3;
  }
  
  public String getPipc3() {
    return pipc3;
  }
  
  public void setPirg4(String pPirg4) {
    if (pPirg4 == null) {
      return;
    }
    pirg4 = pPirg4;
  }
  
  public String getPirg4() {
    return pirg4;
  }
  
  public void setPiec4(String pPiec4) {
    if (pPiec4 == null) {
      return;
    }
    piec4 = pPiec4;
  }
  
  public String getPiec4() {
    return piec4;
  }
  
  public void setPipc4(String pPipc4) {
    if (pPipc4 == null) {
      return;
    }
    pipc4 = pPipc4;
  }
  
  public String getPipc4() {
    return pipc4;
  }
  
  public void setPimag(String pPimag) {
    if (pPimag == null) {
      return;
    }
    pimag = pPimag;
  }
  
  public String getPimag() {
    return pimag;
  }
  
  public void setPirbl(String pPirbl) {
    if (pPirbl == null) {
      return;
    }
    pirbl = pPirbl;
  }
  
  public String getPirbl() {
    return pirbl;
  }
  
  public void setPisan(String pPisan) {
    if (pPisan == null) {
      return;
    }
    pisan = pPisan;
  }
  
  public String getPisan() {
    return pisan;
  }
  
  public void setPiact(String pPiact) {
    if (pPiact == null) {
      return;
    }
    piact = pPiact;
  }
  
  public String getPiact() {
    return piact;
  }
  
  public void setPillv(String pPillv) {
    if (pPillv == null) {
      return;
    }
    pillv = pPillv;
  }
  
  public String getPillv() {
    return pillv;
  }
  
  public void setPiach(String pPiach) {
    if (pPiach == null) {
      return;
    }
    piach = pPiach;
  }
  
  public String getPiach() {
    return piach;
  }
  
  public void setPicja(String pPicja) {
    if (pPicja == null) {
      return;
    }
    picja = pPicja;
  }
  
  public String getPicja() {
    return picja;
  }
  
  public void setPidev(String pPidev) {
    if (pPidev == null) {
      return;
    }
    pidev = pPidev;
  }
  
  public String getPidev() {
    return pidev;
  }
  
  public void setPichg(BigDecimal pPichg) {
    if (pPichg == null) {
      return;
    }
    pichg = pPichg.setScale(DECIMAL_PICHG, RoundingMode.HALF_UP);
  }
  
  public void setPichg(Double pPichg) {
    if (pPichg == null) {
      return;
    }
    pichg = BigDecimal.valueOf(pPichg).setScale(DECIMAL_PICHG, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPichg() {
    return pichg.setScale(DECIMAL_PICHG, RoundingMode.HALF_UP);
  }
  
  public void setPicpr(BigDecimal pPicpr) {
    if (pPicpr == null) {
      return;
    }
    picpr = pPicpr.setScale(DECIMAL_PICPR, RoundingMode.HALF_UP);
  }
  
  public void setPicpr(Double pPicpr) {
    if (pPicpr == null) {
      return;
    }
    picpr = BigDecimal.valueOf(pPicpr).setScale(DECIMAL_PICPR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPicpr() {
    return picpr.setScale(DECIMAL_PICPR, RoundingMode.HALF_UP);
  }
  
  public void setPibas(BigDecimal pPibas) {
    if (pPibas == null) {
      return;
    }
    pibas = pPibas.setScale(DECIMAL_PIBAS, RoundingMode.HALF_UP);
  }
  
  public void setPibas(Integer pPibas) {
    if (pPibas == null) {
      return;
    }
    pibas = BigDecimal.valueOf(pPibas);
  }
  
  public Integer getPibas() {
    return pibas.intValue();
  }
  
  public void setPidos(String pPidos) {
    if (pPidos == null) {
      return;
    }
    pidos = pPidos;
  }
  
  public String getPidos() {
    return pidos;
  }
  
  public void setPicnt(String pPicnt) {
    if (pPicnt == null) {
      return;
    }
    picnt = pPicnt;
  }
  
  public String getPicnt() {
    return picnt;
  }
  
  public void setPipds(BigDecimal pPipds) {
    if (pPipds == null) {
      return;
    }
    pipds = pPipds.setScale(DECIMAL_PIPDS, RoundingMode.HALF_UP);
  }
  
  public void setPipds(Double pPipds) {
    if (pPipds == null) {
      return;
    }
    pipds = BigDecimal.valueOf(pPipds).setScale(DECIMAL_PIPDS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipds() {
    return pipds.setScale(DECIMAL_PIPDS, RoundingMode.HALF_UP);
  }
  
  public void setPivol(BigDecimal pPivol) {
    if (pPivol == null) {
      return;
    }
    pivol = pPivol.setScale(DECIMAL_PIVOL, RoundingMode.HALF_UP);
  }
  
  public void setPivol(Double pPivol) {
    if (pPivol == null) {
      return;
    }
    pivol = BigDecimal.valueOf(pPivol).setScale(DECIMAL_PIVOL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPivol() {
    return pivol.setScale(DECIMAL_PIVOL, RoundingMode.HALF_UP);
  }
  
  public void setPimta(BigDecimal pPimta) {
    if (pPimta == null) {
      return;
    }
    pimta = pPimta.setScale(DECIMAL_PIMTA, RoundingMode.HALF_UP);
  }
  
  public void setPimta(Double pPimta) {
    if (pPimta == null) {
      return;
    }
    pimta = BigDecimal.valueOf(pPimta).setScale(DECIMAL_PIMTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPimta() {
    return pimta.setScale(DECIMAL_PIMTA, RoundingMode.HALF_UP);
  }
  
  public void setPirem1(BigDecimal pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = pPirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem1(Double pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = BigDecimal.valueOf(pPirem1).setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem1() {
    return pirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(BigDecimal pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = pPirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(Double pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = BigDecimal.valueOf(pPirem2).setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem2() {
    return pirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(BigDecimal pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = pPirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(Double pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = BigDecimal.valueOf(pPirem3).setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem3() {
    return pirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirem4(BigDecimal pPirem4) {
    if (pPirem4 == null) {
      return;
    }
    pirem4 = pPirem4.setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public void setPirem4(Double pPirem4) {
    if (pPirem4 == null) {
      return;
    }
    pirem4 = BigDecimal.valueOf(pPirem4).setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem4() {
    return pirem4.setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public void setPirem5(BigDecimal pPirem5) {
    if (pPirem5 == null) {
      return;
    }
    pirem5 = pPirem5.setScale(DECIMAL_PIREM5, RoundingMode.HALF_UP);
  }
  
  public void setPirem5(Double pPirem5) {
    if (pPirem5 == null) {
      return;
    }
    pirem5 = BigDecimal.valueOf(pPirem5).setScale(DECIMAL_PIREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem5() {
    return pirem5.setScale(DECIMAL_PIREM5, RoundingMode.HALF_UP);
  }
  
  public void setPirem6(BigDecimal pPirem6) {
    if (pPirem6 == null) {
      return;
    }
    pirem6 = pPirem6.setScale(DECIMAL_PIREM6, RoundingMode.HALF_UP);
  }
  
  public void setPirem6(Double pPirem6) {
    if (pPirem6 == null) {
      return;
    }
    pirem6 = BigDecimal.valueOf(pPirem6).setScale(DECIMAL_PIREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem6() {
    return pirem6.setScale(DECIMAL_PIREM6, RoundingMode.HALF_UP);
  }
  
  public void setPitrl(Character pPitrl) {
    if (pPitrl == null) {
      return;
    }
    pitrl = String.valueOf(pPitrl);
  }
  
  public Character getPitrl() {
    return pitrl.charAt(0);
  }
  
  public void setPibrl(Character pPibrl) {
    if (pPibrl == null) {
      return;
    }
    pibrl = String.valueOf(pPibrl);
  }
  
  public Character getPibrl() {
    return pibrl.charAt(0);
  }
  
  public void setPirp1(BigDecimal pPirp1) {
    if (pPirp1 == null) {
      return;
    }
    pirp1 = pPirp1.setScale(DECIMAL_PIRP1, RoundingMode.HALF_UP);
  }
  
  public void setPirp1(Double pPirp1) {
    if (pPirp1 == null) {
      return;
    }
    pirp1 = BigDecimal.valueOf(pPirp1).setScale(DECIMAL_PIRP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirp1() {
    return pirp1.setScale(DECIMAL_PIRP1, RoundingMode.HALF_UP);
  }
  
  public void setPirp2(BigDecimal pPirp2) {
    if (pPirp2 == null) {
      return;
    }
    pirp2 = pPirp2.setScale(DECIMAL_PIRP2, RoundingMode.HALF_UP);
  }
  
  public void setPirp2(Double pPirp2) {
    if (pPirp2 == null) {
      return;
    }
    pirp2 = BigDecimal.valueOf(pPirp2).setScale(DECIMAL_PIRP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirp2() {
    return pirp2.setScale(DECIMAL_PIRP2, RoundingMode.HALF_UP);
  }
  
  public void setPirp3(BigDecimal pPirp3) {
    if (pPirp3 == null) {
      return;
    }
    pirp3 = pPirp3.setScale(DECIMAL_PIRP3, RoundingMode.HALF_UP);
  }
  
  public void setPirp3(Double pPirp3) {
    if (pPirp3 == null) {
      return;
    }
    pirp3 = BigDecimal.valueOf(pPirp3).setScale(DECIMAL_PIRP3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirp3() {
    return pirp3.setScale(DECIMAL_PIRP3, RoundingMode.HALF_UP);
  }
  
  public void setPirp4(BigDecimal pPirp4) {
    if (pPirp4 == null) {
      return;
    }
    pirp4 = pPirp4.setScale(DECIMAL_PIRP4, RoundingMode.HALF_UP);
  }
  
  public void setPirp4(Double pPirp4) {
    if (pPirp4 == null) {
      return;
    }
    pirp4 = BigDecimal.valueOf(pPirp4).setScale(DECIMAL_PIRP4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirp4() {
    return pirp4.setScale(DECIMAL_PIRP4, RoundingMode.HALF_UP);
  }
  
  public void setPirp5(BigDecimal pPirp5) {
    if (pPirp5 == null) {
      return;
    }
    pirp5 = pPirp5.setScale(DECIMAL_PIRP5, RoundingMode.HALF_UP);
  }
  
  public void setPirp5(Double pPirp5) {
    if (pPirp5 == null) {
      return;
    }
    pirp5 = BigDecimal.valueOf(pPirp5).setScale(DECIMAL_PIRP5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirp5() {
    return pirp5.setScale(DECIMAL_PIRP5, RoundingMode.HALF_UP);
  }
  
  public void setPirp6(BigDecimal pPirp6) {
    if (pPirp6 == null) {
      return;
    }
    pirp6 = pPirp6.setScale(DECIMAL_PIRP6, RoundingMode.HALF_UP);
  }
  
  public void setPirp6(Double pPirp6) {
    if (pPirp6 == null) {
      return;
    }
    pirp6 = BigDecimal.valueOf(pPirp6).setScale(DECIMAL_PIRP6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirp6() {
    return pirp6.setScale(DECIMAL_PIRP6, RoundingMode.HALF_UP);
  }
  
  public void setPitrp(Character pPitrp) {
    if (pPitrp == null) {
      return;
    }
    pitrp = String.valueOf(pPitrp);
  }
  
  public Character getPitrp() {
    return pitrp.charAt(0);
  }
  
  public void setPitp1(String pPitp1) {
    if (pPitp1 == null) {
      return;
    }
    pitp1 = pPitp1;
  }
  
  public String getPitp1() {
    return pitp1;
  }
  
  public void setPitp2(String pPitp2) {
    if (pPitp2 == null) {
      return;
    }
    pitp2 = pPitp2;
  }
  
  public String getPitp2() {
    return pitp2;
  }
  
  public void setPitp3(String pPitp3) {
    if (pPitp3 == null) {
      return;
    }
    pitp3 = pPitp3;
  }
  
  public String getPitp3() {
    return pitp3;
  }
  
  public void setPitp4(String pPitp4) {
    if (pPitp4 == null) {
      return;
    }
    pitp4 = pPitp4;
  }
  
  public String getPitp4() {
    return pitp4;
  }
  
  public void setPitp5(String pPitp5) {
    if (pPitp5 == null) {
      return;
    }
    pitp5 = pPitp5;
  }
  
  public String getPitp5() {
    return pitp5;
  }
  
  public void setPiin1(Character pPiin1) {
    if (pPiin1 == null) {
      return;
    }
    piin1 = String.valueOf(pPiin1);
  }
  
  public Character getPiin1() {
    return piin1.charAt(0);
  }
  
  public void setPiin2(Character pPiin2) {
    if (pPiin2 == null) {
      return;
    }
    piin2 = String.valueOf(pPiin2);
  }
  
  public Character getPiin2() {
    return piin2.charAt(0);
  }
  
  public void setPiin3(Character pPiin3) {
    if (pPiin3 == null) {
      return;
    }
    piin3 = String.valueOf(pPiin3);
  }
  
  public Character getPiin3() {
    return piin3.charAt(0);
  }
  
  public void setPiin4(Character pPiin4) {
    if (pPiin4 == null) {
      return;
    }
    piin4 = String.valueOf(pPiin4);
  }
  
  public Character getPiin4() {
    return piin4.charAt(0);
  }
  
  public void setPiin5(Character pPiin5) {
    if (pPiin5 == null) {
      return;
    }
    piin5 = String.valueOf(pPiin5);
  }
  
  public Character getPiin5() {
    return piin5.charAt(0);
  }
  
  public void setPicct(String pPicct) {
    if (pPicct == null) {
      return;
    }
    picct = pPicct;
  }
  
  public String getPicct() {
    return picct;
  }
  
  public void setPidat1(BigDecimal pPidat1) {
    if (pPidat1 == null) {
      return;
    }
    pidat1 = pPidat1.setScale(DECIMAL_PIDAT1, RoundingMode.HALF_UP);
  }
  
  public void setPidat1(Integer pPidat1) {
    if (pPidat1 == null) {
      return;
    }
    pidat1 = BigDecimal.valueOf(pPidat1);
  }
  
  public void setPidat1(Date pPidat1) {
    if (pPidat1 == null) {
      return;
    }
    pidat1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat1));
  }
  
  public Integer getPidat1() {
    return pidat1.intValue();
  }
  
  public Date getPidat1ConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat1.intValue(), null);
  }
  
  public void setPirbc(String pPirbc) {
    if (pPirbc == null) {
      return;
    }
    pirbc = pPirbc;
  }
  
  public String getPirbc() {
    return pirbc;
  }
  
  public void setPiint1(BigDecimal pPiint1) {
    if (pPiint1 == null) {
      return;
    }
    piint1 = pPiint1.setScale(DECIMAL_PIINT1, RoundingMode.HALF_UP);
  }
  
  public void setPiint1(Integer pPiint1) {
    if (pPiint1 == null) {
      return;
    }
    piint1 = BigDecimal.valueOf(pPiint1);
  }
  
  public void setPiint1(Date pPiint1) {
    if (pPiint1 == null) {
      return;
    }
    piint1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPiint1));
  }
  
  public Integer getPiint1() {
    return piint1.intValue();
  }
  
  public Date getPiint1ConvertiEnDate() {
    return ConvertDate.db2ToDate(piint1.intValue(), null);
  }
  
  public void setPiint2(BigDecimal pPiint2) {
    if (pPiint2 == null) {
      return;
    }
    piint2 = pPiint2.setScale(DECIMAL_PIINT2, RoundingMode.HALF_UP);
  }
  
  public void setPiint2(Integer pPiint2) {
    if (pPiint2 == null) {
      return;
    }
    piint2 = BigDecimal.valueOf(pPiint2);
  }
  
  public void setPiint2(Date pPiint2) {
    if (pPiint2 == null) {
      return;
    }
    piint2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPiint2));
  }
  
  public Integer getPiint2() {
    return piint2.intValue();
  }
  
  public Date getPiint2ConvertiEnDate() {
    return ConvertDate.db2ToDate(piint2.intValue(), null);
  }
  
  public void setPietai(Character pPietai) {
    if (pPietai == null) {
      return;
    }
    pietai = String.valueOf(pPietai);
  }
  
  public Character getPietai() {
    return pietai.charAt(0);
  }
  
  public void setPimex(String pPimex) {
    if (pPimex == null) {
      return;
    }
    pimex = pPimex;
  }
  
  public String getPimex() {
    return pimex;
  }
  
  public void setPictr(String pPictr) {
    if (pPictr == null) {
      return;
    }
    pictr = pPictr;
  }
  
  public String getPictr() {
    return pictr;
  }
  
  public void setPidili(Character pPidili) {
    if (pPidili == null) {
      return;
    }
    pidili = String.valueOf(pPidili);
  }
  
  public Character getPidili() {
    return pidili.charAt(0);
  }
  
  public void setPiin6(Character pPiin6) {
    if (pPiin6 == null) {
      return;
    }
    piin6 = String.valueOf(pPiin6);
  }
  
  public Character getPiin6() {
    return piin6.charAt(0);
  }
  
  public void setPiin7(Character pPiin7) {
    if (pPiin7 == null) {
      return;
    }
    piin7 = String.valueOf(pPiin7);
  }
  
  public Character getPiin7() {
    return piin7.charAt(0);
  }
  
  public void setPiin8(Character pPiin8) {
    if (pPiin8 == null) {
      return;
    }
    piin8 = String.valueOf(pPiin8);
  }
  
  public Character getPiin8() {
    return piin8.charAt(0);
  }
  
  public void setPiin9(Character pPiin9) {
    if (pPiin9 == null) {
      return;
    }
    piin9 = String.valueOf(pPiin9);
  }
  
  public Character getPiin9() {
    return piin9.charAt(0);
  }
  
  public void setPirfl(String pPirfl) {
    if (pPirfl == null) {
      return;
    }
    pirfl = pPirfl;
  }
  
  public String getPirfl() {
    return pirfl;
  }
  
  public void setPipor0(BigDecimal pPipor0) {
    if (pPipor0 == null) {
      return;
    }
    pipor0 = pPipor0.setScale(DECIMAL_PIPOR0, RoundingMode.HALF_UP);
  }
  
  public void setPipor0(Double pPipor0) {
    if (pPipor0 == null) {
      return;
    }
    pipor0 = BigDecimal.valueOf(pPipor0).setScale(DECIMAL_PIPOR0, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipor0() {
    return pipor0.setScale(DECIMAL_PIPOR0, RoundingMode.HALF_UP);
  }
  
  public void setPipor1(BigDecimal pPipor1) {
    if (pPipor1 == null) {
      return;
    }
    pipor1 = pPipor1.setScale(DECIMAL_PIPOR1, RoundingMode.HALF_UP);
  }
  
  public void setPipor1(Double pPipor1) {
    if (pPipor1 == null) {
      return;
    }
    pipor1 = BigDecimal.valueOf(pPipor1).setScale(DECIMAL_PIPOR1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipor1() {
    return pipor1.setScale(DECIMAL_PIPOR1, RoundingMode.HALF_UP);
  }
  
  public void setPiin10(Character pPiin10) {
    if (pPiin10 == null) {
      return;
    }
    piin10 = String.valueOf(pPiin10);
  }
  
  public Character getPiin10() {
    return piin10.charAt(0);
  }
  
  public void setPiin11(Character pPiin11) {
    if (pPiin11 == null) {
      return;
    }
    piin11 = String.valueOf(pPiin11);
  }
  
  public Character getPiin11() {
    return piin11.charAt(0);
  }
  
  public void setPiin12(Character pPiin12) {
    if (pPiin12 == null) {
      return;
    }
    piin12 = String.valueOf(pPiin12);
  }
  
  public Character getPiin12() {
    return piin12.charAt(0);
  }
  
  public void setPiin13(Character pPiin13) {
    if (pPiin13 == null) {
      return;
    }
    piin13 = String.valueOf(pPiin13);
  }
  
  public Character getPiin13() {
    return piin13.charAt(0);
  }
  
  public void setPiin14(Character pPiin14) {
    if (pPiin14 == null) {
      return;
    }
    piin14 = String.valueOf(pPiin14);
  }
  
  public Character getPiin14() {
    return piin14.charAt(0);
  }
  
  public void setPiin15(Character pPiin15) {
    if (pPiin15 == null) {
      return;
    }
    piin15 = String.valueOf(pPiin15);
  }
  
  public Character getPiin15() {
    return piin15.charAt(0);
  }
  
  public void setPinom2(String pPinom2) {
    if (pPinom2 == null) {
      return;
    }
    pinom2 = pPinom2;
  }
  
  public String getPinom2() {
    return pinom2;
  }
  
  public void setPicpl2(String pPicpl2) {
    if (pPicpl2 == null) {
      return;
    }
    picpl2 = pPicpl2;
  }
  
  public String getPicpl2() {
    return picpl2;
  }
  
  public void setPirue2(String pPirue2) {
    if (pPirue2 == null) {
      return;
    }
    pirue2 = pPirue2;
  }
  
  public String getPirue2() {
    return pirue2;
  }
  
  public void setPiloc2(String pPiloc2) {
    if (pPiloc2 == null) {
      return;
    }
    piloc2 = pPiloc2;
  }
  
  public String getPiloc2() {
    return piloc2;
  }
  
  public void setPicdp2(String pPicdp2) {
    if (pPicdp2 == null) {
      return;
    }
    picdp2 = pPicdp2;
  }
  
  public String getPicdp2() {
    return picdp2;
  }
  
  public void setPivil2(String pPivil2) {
    if (pPivil2 == null) {
      return;
    }
    pivil2 = pPivil2;
  }
  
  public String getPivil2() {
    return pivil2;
  }
  
  public void setPinom3(String pPinom3) {
    if (pPinom3 == null) {
      return;
    }
    pinom3 = pPinom3;
  }
  
  public String getPinom3() {
    return pinom3;
  }
  
  public void setPicpl3(String pPicpl3) {
    if (pPicpl3 == null) {
      return;
    }
    picpl3 = pPicpl3;
  }
  
  public String getPicpl3() {
    return picpl3;
  }
  
  public void setPirue3(String pPirue3) {
    if (pPirue3 == null) {
      return;
    }
    pirue3 = pPirue3;
  }
  
  public String getPirue3() {
    return pirue3;
  }
  
  public void setPiloc3(String pPiloc3) {
    if (pPiloc3 == null) {
      return;
    }
    piloc3 = pPiloc3;
  }
  
  public String getPiloc3() {
    return piloc3;
  }
  
  public void setPicdp3(String pPicdp3) {
    if (pPicdp3 == null) {
      return;
    }
    picdp3 = pPicdp3;
  }
  
  public String getPicdp3() {
    return picdp3;
  }
  
  public void setPifil3(Character pPifil3) {
    if (pPifil3 == null) {
      return;
    }
    pifil3 = String.valueOf(pPifil3);
  }
  
  public Character getPifil3() {
    return pifil3.charAt(0);
  }
  
  public void setPivil3(String pPivil3) {
    if (pPivil3 == null) {
      return;
    }
    pivil3 = pPivil3;
  }
  
  public String getPivil3() {
    return pivil3;
  }
  
  public void setPienv(Character pPienv) {
    if (pPienv == null) {
      return;
    }
    pienv = String.valueOf(pPienv);
  }
  
  public Character getPienv() {
    return pienv.charAt(0);
  }
  
  public void setPicntct(String pPicntct) {
    if (pPicntct == null) {
      return;
    }
    picntct = pPicntct;
  }
  
  public String getPicntct() {
    return picntct;
  }
  
  public void setPicodx(Character pPicodx) {
    if (pPicodx == null) {
      return;
    }
    picodx = String.valueOf(pPicodx);
  }
  
  public Character getPicodx() {
    return picodx.charAt(0);
  }
  
  public void setPietbx(String pPietbx) {
    if (pPietbx == null) {
      return;
    }
    pietbx = pPietbx;
  }
  
  public String getPietbx() {
    return pietbx;
  }
  
  public void setPinumx(BigDecimal pPinumx) {
    if (pPinumx == null) {
      return;
    }
    pinumx = pPinumx.setScale(DECIMAL_PINUMX, RoundingMode.HALF_UP);
  }
  
  public void setPinumx(Integer pPinumx) {
    if (pPinumx == null) {
      return;
    }
    pinumx = BigDecimal.valueOf(pPinumx);
  }
  
  public Integer getPinumx() {
    return pinumx.intValue();
  }
  
  public void setPisufx(BigDecimal pPisufx) {
    if (pPisufx == null) {
      return;
    }
    pisufx = pPisufx.setScale(DECIMAL_PISUFX, RoundingMode.HALF_UP);
  }
  
  public void setPisufx(Integer pPisufx) {
    if (pPisufx == null) {
      return;
    }
    pisufx = BigDecimal.valueOf(pPisufx);
  }
  
  public Integer getPisufx() {
    return pisufx.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
