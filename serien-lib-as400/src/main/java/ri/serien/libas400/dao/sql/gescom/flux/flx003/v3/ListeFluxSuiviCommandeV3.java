/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx003.v3;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonSuiviCommandeWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.ListeFluxBaseV3;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe permet de stocker les flux suivi de commande V3 dans une liste afin de générer un fichier JSON.
 */
public class ListeFluxSuiviCommandeV3 extends ListeFluxBaseV3<JsonSuiviCommandeWebV3> {
  // Variables
  private List<JsonSuiviCommandeWebV3> listeFluxSuiviCommande = new ArrayList<JsonSuiviCommandeWebV3>();
  
  /**
   * Constructeur.
   */
  public ListeFluxSuiviCommandeV3(EnumTypeFlux enumTypeFlux, String pPrefixeNomFichier) {
    super(enumTypeFlux, pPrefixeNomFichier);
    setListeFlux(listeFluxSuiviCommande);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void transfererVersServeur() {
    transfererVersServeur(this);
  }
  
  // -- Méthodes protégées
  
  /**
   * Contrôle que le code de l'élément à insérer n'existe pas déjà dans la liste (doublon).
   * Retourne l'indice du doublon sinon -1.
   */
  @Override
  protected int controlerExistenceElement(JsonSuiviCommandeWebV3 pFluxJson) {
    if (Constantes.normerTexte(pFluxJson.getCode()).isEmpty()) {
      Trace.alerte(
          "Le contrôle du doublon avant insertion dans la liste des suivis de commandes web est impossible car le champ Idclient est vide.");
      return -1;
    }
    for (int indice = 0; indice < listeFluxSuiviCommande.size(); indice++) {
      if (listeFluxSuiviCommande.get(indice).getCode().equals(pFluxJson.getCode())) {
        return indice;
      }
    }
    return -1;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la liste de flux suivi de commande.
   */
  public List<JsonSuiviCommandeWebV3> getListeFluxSuiviCommande() {
    return listeFluxSuiviCommande;
  }
  
  /**
   * Initialise la liste de flux suivi de commande.
   */
  public void setListeFluxSuiviCommande(List<JsonSuiviCommandeWebV3> pListeFluxSuiviCommande) {
    listeFluxSuiviCommande = pListeFluxSuiviCommande;
  }
  
}
