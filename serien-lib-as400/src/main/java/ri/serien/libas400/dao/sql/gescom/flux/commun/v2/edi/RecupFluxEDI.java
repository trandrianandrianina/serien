/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.ftp.FTPManager;

public class RecupFluxEDI {
  private ManagerFluxMagento gestionDesFlux = null;
  private String etablissement = null;
  private FTPManager serveurFTP = null;
  
  public RecupFluxEDI(ManagerFluxMagento gestion, String etb) {
    gestionDesFlux = gestion;
    etablissement = etb;
  }
  
  public ArrayList<FluxMagento> recupererCommandesEDI(ParametresFluxBibli pParam) {
    ArrayList<FluxMagento> listeFlux = null;
    
    if (pParam == null) {
      return null;
    }
    
    // Etape 1 récupérer les commandes sur le serveur FTP @GP
    if (serveurFTP == null) {
      serveurFTP = new FTPManager();
    }
    
    // On se connecte au serveur FTP distant
    if (serveurFTP.connexion(pParam.getUrl_ftp_EDI(), pParam.getLogin_ftp_EDI(), pParam.getPassword_ftp_EDI())) {
      try { // On se positionne dans le bon dossier
        if (serveurFTP.getConnexion().changeWorkingDirectory(pParam.getDossierCdes_ftp_EDI())) {
          serveurFTP.getConnexion().enterLocalPassiveMode();
          serveurFTP.getConnexion().setFileType(FTP.BINARY_FILE_TYPE);
          
          FTPFile[] listeFichiers = serveurFTP.getConnexion().listFiles();
          
          // S'il existe de fichiers à récupérer
          if (listeFichiers != null) {
            listeFlux = new ArrayList<FluxMagento>();
            
            for (int i = 0; i < listeFichiers.length; i++) {
              if (listeFichiers[i].isFile() && listeFichiers[i].getName() != null) {
                FluxMagento flux = gestionDesFlux.getRecord(true);
                
                // Initialisation des champs
                flux.setFLETB(etablissement);
                // flux.setFLVER(M_FluxMagento.VERSION_FLX008);
                
                // On ajoute le code métier du flux
                flux.setFLIDF(FluxMagento.IDF_FLX008_EDI_ORDERS);
                flux.setFLSNS(FluxMagento.SNS_EDI_RECEP);
                flux.setFLSTT(EnumStatutFlux.A_TRAITER);
                flux.setFLCOD(listeFichiers[i].getName().trim());
                
                listeFlux.add(flux);
              }
            }
            
          }
          else {
            Trace.erreur("[RecupFluxEDI] recupererCommandesEDI() listeFichiers NULLE ");
          }
          
          serveurFTP.disconnect();
        }
        else {
          Trace.erreur("[RecupFluxEDI] recupererCommandesEDI(): " + serveurFTP.getMsgError());
        }
      }
      catch (IOException e) {
        Trace.erreur("[RecupFluxEDI] recupererCommandesEDI(): PD changeWorkingDirectory " + pParam.getDossierCdes_ftp_EDI() + " -> "
            + e.getMessage());
      }
      serveurFTP.disconnect();
    }
    else {
      Trace.erreur("[RecupFluxEDI] recupererCommandesEDI(): " + serveurFTP.getMsgError());
    }
    
    return listeFlux;
  }
}
