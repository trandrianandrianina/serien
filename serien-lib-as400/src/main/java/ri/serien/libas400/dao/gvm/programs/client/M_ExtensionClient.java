/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs.client;

import java.util.ArrayList;

import ri.serien.libas400.dao.exp.programs.contact.M_Contact;
import ri.serien.libas400.dao.gvm.database.files.FFD_Pgvmecbm;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;

/**
 * (DAO) Gestion bas niveau de la table PGVMECBM - Concerne un enregistrement
 */
public class M_ExtensionClient extends FFD_Pgvmecbm {
  // Variables
  private ArrayList<M_Contact> listContact = null;
  
  /**
   * Constructeur
   */
  public M_ExtensionClient(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Insère l'enregistrement dans le table
   */
  @Override
  public boolean insertInDatabase() {
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert("PGVMECBM", querymg.getLibrary());
    return request(requete);
  }
  
  /**
   * Modifie l'enregistrement dans le table
   */
  @Override
  public boolean updateInDatabase() {
    initGenericRecord(genericrecord, false);
    String requete = genericrecord.createSQLRequestUpdate("PGVMECBM", querymg.getLibrary(),
        "EBNUM=" + getEBCLI() + " and EBLIV=" + getEBLIV() + " and EBETB='" + getEBETB() + "'");
    return request(requete);
  }
  
  /**
   * Suppression de l'enregistrement courant
   * @return
   */
  @Override
  public boolean deleteInDatabase() {
    return deleteInDatabase(false);
  }
  
  /**
   * Suppression de l'enregistrement courant ou de tous
   * @return
   */
  public boolean deleteInDatabase(boolean deleteAll) {
    String requete = "delete from " + querymg.getLibrary() + ".PGVMECBM where EBETB='" + getEBETB() + "'";
    if (!deleteAll) {
      requete += " and EBNUM=" + getEBCLI() + " and EBLIV=" + getEBLIV();
    }
    return request(requete);
  }
  
  /**
   * Retourne le numéro client et livré concaténés
   * @return
   */
  public String getCLIandLIV() {
    return getCLIandLIV(getEBCLI(), getEBLIV());
  }
  
  /**
   * Retourne le numéro client et livré concaténés.
   */
  public static String getCLIandLIV(int aCodeClient, int aSuffixeClient) {
    return String.format("%0" + FFD_Pgvmecbm.SIZE_EBCLI + "d%0" + FFD_Pgvmecbm.SIZE_EBLIV + "d", aCodeClient, aSuffixeClient);
  }
  
  /**
   * Récupération les extensions d'un client bien définit.
   * A revoir complètement.
   */
  public Client load(Client pClient) {
    if (pClient == null) {
      return null;
    }
    
    // Lecture de la base afin de récupérer un client bien définit
    String requete = "select * from " + querymg.getLibrary() + ".PGVMECBM where ebcli = " + pClient.getId().getNumero() + " and ebliv = "
        + pClient.getId().getSuffixe() + " and ebetb = '" + pClient.getId().getCodeEtablissement() + "'";
    ArrayList<GenericRecord> listrcd = querymg.select(requete);
    if ((listrcd == null) || (listrcd.size() < 1)) {
      return null;
    }
    // Chargement des classes avec les données de la base
    boolean ret = initObject(listrcd.get(0), true);
    if (!ret) {
      return null;
    }
    
    initExtensionsClientGlobal(pClient);
    
    return pClient;
  }
  
  /**
   * Initialise tous les champs de la classe extensionClient (pour l'instant le fichier PGVMECBM)
   * A revoir complètement.
   */
  private void initExtensionsClientGlobal(Client pClient) {
    
    pClient.setTypeZonePersonnalisee("" + getEBTZP());
    pClient.setZonePersonnalisee01(getEBZP1());
    pClient.setZonePersonnalisee02(getEBZP2());
    pClient.setZonePersonnalisee03(getEBZP3());
    pClient.setZonePersonnalisee04(getEBZP4());
    pClient.setZonePersonnalisee05(getEBZP5());
    pClient.setZonePersonnalisee06(getEBZP6());
    pClient.setZonePersonnalisee07(getEBZP7());
    pClient.setZonePersonnalisee08(getEBZP8());
    pClient.setZonePersonnalisee09(getEBZP9());
    pClient.setZonePersonnalisee10(getEBZP10());
    pClient.setZonePersonnalisee11(getEBZP11());
    pClient.setZonePersonnalisee12(getEBZP12());
    pClient.setZonePersonnalisee13(getEBZP13());
    pClient.setZonePersonnalisee14(getEBZP14());
    pClient.setZonePersonnalisee15(getEBZP15());
    pClient.setZonePersonnalisee16(getEBZP16());
    pClient.setZonePersonnalisee17(getEBZP17());
    pClient.setZonePersonnalisee18(getEBZP18());
    pClient.setPrisParEstObligatoire(getEBPRP() != ' ');
    pClient.setRefCourteCommandeObligatoire(getEBREFC() != ' ');
    pClient.setVehiculeEstObligatoire(getEBVEH() != ' ');
    pClient.setChantierEstObligatoire(getEBCHAN() != ' ');
    pClient.setFluxCommandeEDI("" + getEBIN01());
    pClient.setFluxExpeditionEDI("" + getEBIN02());
    pClient.setFluxFactureEDI("" + getEBIN03());
    pClient.setAdresseFactureClientPayeur("" + getEBIN04());
    if (!pClient.isClientProspect()) {
      pClient.setTypeCompteClient(EnumTypeCompteClient.valueOfByCode(("" + getEBIN05()).trim()));
    }
    pClient.setReglementComptantEstObligatoire(getEBIN06() != ' ');
    pClient.setReglementChequeEstInterdit(getEBIN07() != ' ');
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
    
    if (listContact != null) {
      for (M_Contact c : listContact) {
        c.dispose();
      }
      listContact.clear();
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
}
