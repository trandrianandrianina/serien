/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.documentachat;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.achat.document.CritereDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEtatDocument;
import ri.serien.libcommun.gescom.achat.document.EnumCodeExtractionDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumEtapeExtraction;
import ri.serien.libcommun.gescom.achat.document.EnumTypeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.ListeReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.ReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.ValeurInitialeCommandeAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatCommentaire;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.prix.ParametrePrixAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.IdAcheteur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlDocumentAchat {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlDocumentAchat(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  /**
   * Retourne une liste de documents pour un fournisseur correspondants aux critères de recherche.
   */
  public List<IdDocumentAchat> chargerListeIdDocumentAchat(CritereDocumentAchat pCriteres) {
    // Contrôler les critères de recherche
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères de recherche des documents d'achat sont invalides.");
    }
    
    // Générer la requête
    RequeteSql requete = new RequeteSql();
    requete.ajouter("select EACOD, EAETB, EANUM, EASUF from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT);
    
    // Article
    if (pCriteres.getCodeArticle() != null && !pCriteres.getCodeArticle().trim().isEmpty()) {
      requete.ajouter(", " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT_LIGNE + " where ");
      requete.ajouterConditionAnd("LAERL", "=", 'C');
      requete.ajouterConditionAnd("EACOD", "=", "LACOD", true, false);
      requete.ajouterConditionAnd("EAETB", "=", "LAETB", true, false);
      requete.ajouterConditionAnd("EANUM", "=", "LANUM", true, false);
      requete.ajouterConditionAnd("EASUF", "=", "LASUF", true, false);
      
      requete.ajouterConditionAnd("LAART", "=", pCriteres.getCodeArticle());
    }
    else {
      requete.ajouter(" where ");
      requete.ajouterConditionAnd("EAETB", "=", pCriteres.getCodeEtablissement());
    }
    
    requete.ajouterConditionAnd("EATOP", "=", 0);
    requete.ajouterConditionAnd("EAIN9", "<>", 'I');
    
    // Les types de document
    RequeteSql sousCondition = new RequeteSql();
    int nombreSousCondition = 0;
    List<EnumTypeDocumentAchat> listeTypeDocument = pCriteres.getListeTypeDocument();
    if (listeTypeDocument != null && !listeTypeDocument.isEmpty()) {
      requete.ajouter(" and (");
      for (EnumTypeDocumentAchat typeDocument : listeTypeDocument) {
        // Cas particulier de la commande
        if (typeDocument.equals(EnumTypeDocumentAchat.COMMANDE)) {
          sousCondition.effacerRequete();
          if (nombreSousCondition == 0) {
            sousCondition.ajouter(" (");
          }
          else {
            sousCondition.ajouter(" or (");
          }
          sousCondition.ajouterConditionAnd("EACOD", "=", typeDocument.retournerEnumCodeEnteteDocumentAchat().getCode());
          sousCondition.ajouterConditionAnd("EASUF", "=", 0);
          sousCondition.ajouter(" ) ");
          requete.ajouter(sousCondition.getRequete());
        }
        // Cas particulier de la réception
        else if (typeDocument.equals(EnumTypeDocumentAchat.RECEPTION)) {
          sousCondition.effacerRequete();
          if (nombreSousCondition == 0) {
            sousCondition.ajouter(" (");
          }
          else {
            sousCondition.ajouter(" or (");
          }
          sousCondition.ajouterConditionAnd("EACOD", "=", typeDocument.retournerEnumCodeEnteteDocumentAchat().getCode());
          sousCondition.ajouterConditionAnd("EASUF", ">", 0);
          sousCondition.ajouter(" ) ");
          requete.ajouter(sousCondition.getRequete());
        }
        // Cas général
        else {
          requete.ajouterConditionOr("EACOD", "=", typeDocument.retournerEnumCodeEnteteDocumentAchat().getCode());
        }
        
        nombreSousCondition++;
      }
      requete.ajouter(" ) ");
    }
    
    // Les codes états
    List<EnumCodeEtatDocument> listeCodeEtat = pCriteres.getListeCodeEtat();
    if (listeCodeEtat != null && !listeCodeEtat.isEmpty()) {
      requete.ajouterConditionAnd("EAETA", "=", listeCodeEtat);
    }
    
    // Référence
    if (!pCriteres.getRechercheGenerique().getTexteFinal().isEmpty()) {
      requete.ajouter(
          " AND (TRANSLATE(EARFL,'AAAAAABCCDEEEEEEEFGHIIIIIJKLMNOOOOOPQRSTUUUUUUVWXYZ','aàâÂäÄbcçdeéèêÊëËfghiîÎïÏjklmnoôÔöÖpqrstuùûÛüÜvwxyz')"
              + " like " + "'%" + pCriteres.getRechercheGenerique().getTexteRechercheReference().toUpperCase() + "%'"
              + " or TRANSLATE(EARBC,'AAAAAABCCDEEEEEEEFGHIIIIIJKLMNOOOOOPQRSTUUUUUUVWXYZ','aàâÂäÄbcçdeéèêÊëËfghiîÎïÏjklmnoôÔöÖpqrstuùûÛüÜvwxyz') like "
              + "'%" + pCriteres.getRechercheGenerique().getTexteRechercheReference().toUpperCase() + "%')");
    }
    
    // Sans commandes d'achat réceptionnées
    if (!pCriteres.isAvecDocumentAchatReceptionne()) {
      requete.ajouterConditionAnd("EATLV", "<>", 0);
    }
    
    // Fournisseur
    if (pCriteres.getIdFournisseur() != null) {
      IdFournisseur.controlerId(pCriteres.getIdFournisseur(), true);
      requete.ajouterConditionAnd("EACOL", "=", pCriteres.getIdFournisseur().getCollectif());
      requete.ajouterConditionAnd("EAFRS", "=", pCriteres.getIdFournisseur().getNumero());
    }
    
    // Magasin
    if (pCriteres.getIdMagasin() != null) {
      requete.ajouterConditionAnd("EAMAG", "=", pCriteres.getIdMagasin().getCode());
    }
    
    // Plage de dates
    if (pCriteres.getDateCreationDebut() != null && pCriteres.getDateCreationFin() != null) {
      requete.ajouter(" and (");
      requete.ajouterConditionAnd("EACRE", ">=", ConvertDate.dateToDb2(pCriteres.getDateCreationDebut()));
      requete.ajouterConditionAnd("EACRE", "<=", ConvertDate.dateToDb2(pCriteres.getDateCreationFin()));
      requete.ajouter(")");
    }
    
    // Numéro de document
    if (pCriteres.getNumeroDocument() > 0) {
      requete.ajouterConditionAnd("EANUM", "=", pCriteres.getNumeroDocument());
    }
    
    requete.ajouter("order By EANUM desc");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    
    List<IdDocumentAchat> listeId = new ArrayList<IdDocumentAchat>();
    if (listeRecord == null || listeRecord.isEmpty()) {
      return listeId;
    }
    
    // génèration des identifiants des documents trouvés
    for (GenericRecord record : listeRecord) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("EAETB"));
      EnumCodeEnteteDocumentAchat codeEntete = EnumCodeEnteteDocumentAchat.valueOfByCode(record.getCharacterValue("EACOD"));
      IdDocumentAchat idDocumentAchat =
          IdDocumentAchat.getInstance(idEtablissement, codeEntete, record.getIntegerValue("EANUM"), record.getIntegerValue("EASUF"));
      listeId.add(idDocumentAchat);
    }
    return listeId;
  }
  
  /**
   * Retourne les documents d'achat à partir d'une liste d'Id.
   * Note importante:
   * Cette méthode charge de manière incomplète les documents car par exemple l'adresse fournisseur est manquante. Car elle n'a pas
   * suivit l'évolution de la version RPG.
   * A voir si on la conserve ou si l'on conserve la version RPG.
   */
  public ListeDocumentAchat chargerListeDocumentAchat(List<IdDocumentAchat> pListeIdDocumentAchat) {
    ListeDocumentAchat listeDocument = new ListeDocumentAchat();
    if (pListeIdDocumentAchat == null || pListeIdDocumentAchat.isEmpty()) {
      return listeDocument;
    }
    
    RequeteSql requete = new RequeteSql();
    
    // On contrôle que le code du document et l'id établissement soit le même pour l'ensemble de la liste
    boolean identique = true;
    IdEtablissement idEtablissement = pListeIdDocumentAchat.get(0).getIdEtablissement();
    EnumCodeEnteteDocumentAchat codeEntete = pListeIdDocumentAchat.get(0).getCodeEntete();
    for (int i = 1; i < pListeIdDocumentAchat.size(); i++) {
      IdDocumentAchat.controlerId(pListeIdDocumentAchat.get(i), true);
      if (!codeEntete.equals(pListeIdDocumentAchat.get(i).getCodeEntete())
          || !idEtablissement.equals(pListeIdDocumentAchat.get(i).getIdEtablissement())) {
        identique = false;
        break;
      }
    }
    
    // Création du groupe des indicatifs
    // Si le code entête et l'établissement sont identiques pour l'ensemble des id on peut optimiser la requête
    if (identique) {
      for (IdDocumentAchat idDocumentAchat : pListeIdDocumentAchat) {
        IdDocumentAchat.controlerId(idDocumentAchat, true);
        requete.ajouterValeurAuGroupe("indicatif", idDocumentAchat.getIndicatif(IdDocumentAchat.INDICATIF_NUM_SUF));
      }
      // @formatter:off
      requete.ajouter("Select doc1.EATOP,"
          + " doc1.EAETB,"
          + " doc1.EACOD,"
          + " doc1.EANUM,"
          + " doc1.EASUF,"
          + " doc1.EACRE,"
          + " doc1.EADAT,"
          + " doc1.EAHOM,"
          + " doc1.EAREC,"
          + " doc1.EAFAC,"
          + " doc1.EAETA,"
          + " doc1.EACOL,"
          + " doc1.EAFRS,"
          + " coalesce(doc2.EATHTL, (IFNULL(doc1.EATHTL,0))) AS EATHTL,"
          + " coalesce(doc2.EATTC, (IFNULL(doc1.EATTC,0))) AS EATTC,"
          + " doc1.EAMAG,"
          + " doc1.EAACH,"
          + " doc1.EARBL,"
          + " doc1.EARBC,"
          + " doc1.EADLP,"
          + " doc1.EARFL,"
          + " doc1.EANFA,"
          + " ADNOM,"
          + " ADCPL,"
          + " ADRUE,"
          + " ADLOC,"
          + " ADCDP,"
          + " ADVIL,"
          + " ADPAY,"
          + " FENOM,"
          + " FECPL,"
          + " FERUE,"
          + " FELOCG,"
          + " FECDP,"
          + " FEVIL,"
          + " FRNOM,"
          + " FRCPL,"
          + " FRRUE,"
          + " FRLOC,"
          + " FRCDP,"
          + " FRVIL,"
          + " FRPAY"
          + " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT + " doc1"
          // Jointure avec l'éventuel document de ventes archivé
          + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT + " doc2 on"
          + "   doc2.EAETB = doc1.EAETB"
          + "   and doc2.EACOD = lower(doc1.EACOD)"
          + "   and doc2.EANUM = doc1.EANUM"
          + "   and doc2.EASUF = doc1.EASUF"
          // Jointure avec la table des adresses des achats
          + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT_ADRESSE + " on"
          + "   ADETB = doc1.EAETB"
          + "   and ADCOD = 'C'"
          + "   and ADNUM = doc1.EANUM"
          + "   and ADSUF = doc1.EASUF"
          // Jointure avec la table des adresses fournisseur
          + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR_ADRESSE + " on"
          + "   FEETB = doc1.EAETB"
          + "   and FECOL = doc1.EACOL"
          + "   and FEFRS = doc1.EAFRS"
          // Jointure avec la table des fournisseur
          + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " on"
          + "   FRETB = doc1.EAETB"
          + "   and FRCOL = doc1.EACOL"
          + "   and FRFRS = doc1.EAFRS"
          + " where ");
      // @formatter:on
      requete.ajouterConditionAnd("doc1.EACOD", "=", codeEntete.getCode());
      requete.ajouterConditionAnd("doc1.EAETB", "=", idEtablissement.getCodeEtablissement());
      requete.ajouter(" and CONCAT(DIGITS(doc1.EANUM), DIGITS(doc1.EASUF))");
    }
    // Sinon on créé une version moins performante de la requête
    else {
      for (IdDocumentAchat idDocumentAchat : pListeIdDocumentAchat) {
        IdDocumentAchat.controlerId(idDocumentAchat, true);
        requete.ajouterValeurAuGroupe("indicatif", idDocumentAchat.getIndicatif(IdDocumentAchat.INDICATIF_ENTETE_ETB_NUM_SUF));
      }
      // @formatter:off
      requete.ajouter("Select doc1.EATOP,"
          + " doc1.EAETB,"
          + " doc1.EACOD,"
          + " doc1.EANUM,"
          + " doc1.EASUF,"
          + " doc1.EACRE,"
          + " doc1.EADAT,"
          + " doc1.EAHOM,"
          + " doc1.EAREC,"
          + " doc1.EAFAC,"
          + " doc1.EAETA,"
          + " doc1.EACOL,"
          + " doc1.EAFRS,"
          + " coalesce(doc2.EATHTL, doc1.EATHTL) AS EATHTL,"
          + " coalesce(doc2.EATTC, doc1.EATTC) AS EATTC,"
          + " doc1.EAMAG,"
          + " doc1.EAACH,"
          + " doc1.EARBL,"
          + " doc1.EARBC,"
          + " doc1.EADLP,"
          + " doc1.EARFL,"
          + " doc1.EANFA,"
          + " ADNOM,"
          + " ADCPL,"
          + " ADRUE,"
          + " ADLOC,"
          + " ADCDP,"
          + " ADVIL,"
          + " ADPAY,"
          + " FENOM,"
          + " FECPL,"
          + " FERUE,"
          + " FELOCG,"
          + " FECDP,"
          + " FEVIL,"
          + " FRNOM,"
          + " FRCPL,"
          + " FRRUE,"
          + " FRLOC,"
          + " FRCDP,"
          + " FRVIL,"
          + " FRPAY"
          + " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT + " doc1"
          // Jointure avec l'éventuel document de ventes archivé
          + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT + " doc2 on"
          + "   doc2.EAETB = doc1.EAETB"
          + "   and doc2.EACOD = lower(doc1.EACOD)"
          + "   and doc2.EANUM = doc1.EANUM"
          + "   and doc2.EASUF = doc1.EASUF"
          // Jointure avec la table des adresses des achats
          + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT_ADRESSE + " on"
          + "   ADETB = doc1.EAETB"
          + "   and ADCOD = 'C'"
          + "   and ADNUM = doc1.EANUM"
          + "   and ADSUF = doc1.EASUF"
          // Jointure avec la table des adresses fournisseur
          + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR_ADRESSE + " on"
          + "   FEETB = doc1.EAETB"
          + "   and FECOL = doc1.EACOL"
          + "   and FEFRS = doc1.EAFRS"
          // Jointure avec la table des fournisseur
          + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " on"
          + "   FRETB = doc1.EAETB"
          + "   and FRCOL = doc1.EACOL"
          + "   and FRFRS = doc1.EAFRS"
          + " where CONCAT(CONCAT(CONCAT(doc1.EACOD, doc1.EAETB), DIGITS(doc1.EANUM)), DIGITS(doc1.EASUF))");
      // @formatter:on
    }
    
    // Ajout de la liste des identifiants fournisseurs
    requete.ajouterGroupeDansRequete("indicatif", "in");
    
    requete.ajouter("order By doc1.EANUM desc");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return listeDocument;
    }
    
    // Lecture des champs
    for (GenericRecord record : listeRecord) {
      idEtablissement = IdEtablissement.getInstance(record.getStringValue("EAETB"));
      codeEntete = EnumCodeEnteteDocumentAchat.valueOfByCode(record.getCharacterValue("EACOD"));
      IdDocumentAchat idDocumentAchat =
          IdDocumentAchat.getInstance(idEtablissement, codeEntete, record.getIntegerValue("EANUM"), record.getIntegerValue("EASUF"));
      DocumentAchat documentAchat = new DocumentAchat(idDocumentAchat);
      completerDocument(record, documentAchat);
      // Avant de l'insérer dans la liste, vérification qu'un enregistrement n'existe pas déjà (il peut y avoir des doublons à cause des
      // adresses fournisseurs)
      if (listeDocument.get(idDocumentAchat) == null) {
        listeDocument.add(documentAchat);
      }
    }
    return listeDocument;
  }
  
  /**
   * Lire les Id des lignes d'achat suivant les critères sélectionnés.
   */
  public List<IdLigneAchat> chargerListeIdLignesAchat(CritereLigneAchat pCriteres) {
    List<IdLigneAchat> listeIdLignes = new ArrayList<IdLigneAchat>();
    
    // Générer la requête
    RequeteSql requete = new RequeteSql();
    requete.ajouter("select LAETB, LACOD, LANUM, LASUF, LANLI" + " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT_LIGNE
        + ", " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT + " where");
    requete.ajouterConditionAnd("EACOD", "=", "LACOD", true, false);
    requete.ajouterConditionAnd("EAETB", "=", "LAETB", true, false);
    requete.ajouterConditionAnd("EANUM", "=", "LANUM", true, false);
    requete.ajouterConditionAnd("EASUF", "=", "LASUF", true, false);
    requete.ajouterConditionAnd("LAETB", "=", pCriteres.getIdEtablissement().getCodeEtablissement());
    
    // Filtrage sur le type de document
    RequeteSql sousCondition = new RequeteSql();
    int nombreSousCondition = 0;
    List<EnumTypeDocumentAchat> listeTypeDocument = pCriteres.getListeTypeDocument();
    if (listeTypeDocument != null && !listeTypeDocument.isEmpty()) {
      requete.ajouter(" and (");
      for (EnumTypeDocumentAchat typeDocument : listeTypeDocument) {
        // Cas particulier de la commande
        if (typeDocument.equals(EnumTypeDocumentAchat.COMMANDE)) {
          sousCondition.effacerRequete();
          if (nombreSousCondition == 0) {
            sousCondition.ajouter(" (");
          }
          else {
            sousCondition.ajouter(" or (");
          }
          sousCondition.ajouterConditionAnd("EACOD", "=", typeDocument.retournerEnumCodeEnteteDocumentAchat().getCode());
          sousCondition.ajouterConditionAnd("EASUF", "=", 0);
          sousCondition.ajouter(" ) ");
          requete.ajouter(sousCondition.getRequete());
        }
        // Cas particulier de la réception
        else if (typeDocument.equals(EnumTypeDocumentAchat.RECEPTION)) {
          sousCondition.effacerRequete();
          if (nombreSousCondition == 0) {
            sousCondition.ajouter(" (");
          }
          else {
            sousCondition.ajouter(" or (");
          }
          sousCondition.ajouterConditionAnd("EACOD", "=", typeDocument.retournerEnumCodeEnteteDocumentAchat().getCode());
          sousCondition.ajouterConditionAnd("EASUF", ">", 0);
          sousCondition.ajouter(" ) ");
          requete.ajouter(sousCondition.getRequete());
        }
        // Cas général
        else {
          requete.ajouterConditionOr("EACOD", "=", typeDocument.retournerEnumCodeEnteteDocumentAchat().getCode());
        }
        
        nombreSousCondition++;
      }
      requete.ajouter(" ) ");
    }
    
    // Filtrage sur les codes états suivants
    List<EnumCodeEtatDocument> listeCodeEtat = pCriteres.getListeCodeEtat();
    if (listeCodeEtat != null && !listeCodeEtat.isEmpty()) {
      requete.ajouterConditionAnd("EAETA", "=", listeCodeEtat);
    }
    
    // Exclure les lignes supprimées
    if (pCriteres.isExclureLigneSupprimee()) {
      requete.ajouterConditionAnd("LACEX", "<>", 'A');
    }
    
    // Numéro document achat
    if (pCriteres.getIdDocumentAchat() != null && pCriteres.getIdDocumentAchat().getNumero() > 0) {
      requete.ajouterConditionAnd("LANUM", "=", pCriteres.getIdDocumentAchat().getNumero());
    }
    else if (pCriteres.getIdLigneAchat() != null && pCriteres.getIdLigneAchat().getNumero() > 0) {
      requete.ajouterConditionAnd("LANUM", "=", pCriteres.getIdLigneAchat().getNumero());
    }
    else if (pCriteres.getNumeroDocumentAchat() != null && pCriteres.getNumeroDocumentAchat() > 0) {
      requete.ajouterConditionAnd("LANUM", "=", pCriteres.getNumeroDocumentAchat());
    }
    
    // Numéro de ligne
    if (pCriteres.getIdLigneAchat() != null && pCriteres.getIdLigneAchat().getNumeroLigne() > 0) {
      requete.ajouterConditionAnd("LANLI", "=", pCriteres.getIdLigneAchat().getNumeroLigne());
    }
    
    // L'id article
    if (pCriteres.getIdArticle() != null) {
      requete.ajouterConditionAnd("LAART", "=", pCriteres.getIdArticle().getCodeArticle());
    }
    
    // L'id fournisseur
    if (pCriteres.getIdFournisseur() != null) {
      requete.ajouterConditionAnd("EACOL", "=", pCriteres.getIdFournisseur().getCollectif());
      requete.ajouterConditionAnd("EAFRS", "=", pCriteres.getIdFournisseur().getNumero());
    }
    
    // Référence
    if (!pCriteres.getRechercheGenerique().getTexteFinal().isEmpty()) {
      requete.ajouter(
          " AND (TRANSLATE(EARFL,'AAAAAABCCDEEEEEEEFGHIIIIIJKLMNOOOOOPQRSTUUUUUUVWXYZ','aàâÂäÄbcçdeéèêÊëËfghiîÎïÏjklmnoôÔöÖpqrstuùûÛüÜvwxyz')"
              + " like " + "'%" + pCriteres.getRechercheGenerique().getTexteRechercheReference().toUpperCase() + "%'"
              + " or TRANSLATE(EARBC,'AAAAAABCCDEEEEEEEFGHIIIIIJKLMNOOOOOPQRSTUUUUUUVWXYZ','aàâÂäÄbcçdeéèêÊëËfghiîÎïÏjklmnoôÔöÖpqrstuùûÛüÜvwxyz') like "
              + "'%" + pCriteres.getRechercheGenerique().getTexteRechercheReference().toUpperCase() + "%')");
    }
    
    // Le magasin
    if (pCriteres.getIdMagasin() != null) {
      requete.ajouterConditionAnd("EAMAG", "=", pCriteres.getIdMagasin().getCode());
    }
    
    // Exclusion des commentaires
    if (pCriteres.isExclureLigneCommentaire()) {
      requete.ajouterConditionAnd("LAERL", "<>", '*');
    }
    
    // Conserver uniquement les lignes des documents réceptionnés (filtre sur la date de réception du document) - A confirmer
    if (pCriteres.isRechercherReception()) {
      requete.ajouterConditionAnd("EAREC", ">", 0);
    }
    
    // Filtrer sur la date de création début
    if (pCriteres.getDateCreationDebut() != null) {
      requete.ajouterConditionAnd("(EACRE", ">=", ConvertDate.dateToDb2(pCriteres.getDateCreationDebut()));
    }
    
    // Filtrer sur la date de création fin
    if (pCriteres.getDateCreationFin() != null) {
      requete.ajouterConditionAnd("EACRE", "<=", ConvertDate.dateToDb2(pCriteres.getDateCreationFin()));
      requete.ajouter(")");
    }
    
    // Exclusion des lignes totalement réceptionnées
    if (pCriteres.isExclureLigneReceptionnee()) {
      requete.ajouterConditionAnd("LAQTS", "<>", 0);
    }
    
    requete.ajouter("ORDER BY LANUM desc, LASUF, LANLI");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return listeIdLignes;
    }
    
    // Lecture des champs
    for (GenericRecord record : listeRecord) {
      IdLigneAchat idligne = null;
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("LAETB"));
      EnumCodeEnteteDocumentAchat codeEntete = EnumCodeEnteteDocumentAchat.valueOfByCode(record.getCharacterValue("LACOD"));
      IdDocumentAchat idDocumentAchat =
          IdDocumentAchat.getInstance(idEtablissement, codeEntete, record.getIntegerValue("LANUM"), record.getIntegerValue("LASUF"));
      idligne = IdLigneAchat.getInstance(idDocumentAchat, record.getIntegerValue("LANLI"));
      listeIdLignes.add(idligne);
    }
    
    return listeIdLignes;
    
  }
  
  public ListeLigneAchat chargerListeLignesAchat(List<IdLigneAchat> pListeIdLigneAchat) {
    if (pListeIdLigneAchat == null) {
      throw new MessageErreurException("La liste des identifiants de lignes d'achat est invalide.");
    }
    // Générer la requête
    RequeteSql requete = new RequeteSql();
    requete.ajouter("SELECT lig.*, art.A1FAM, art.A1ART, ext.A1LB1, ext.A1LB2, ext.A1LB3 FROM " + querymg.getLibrary() + '.'
        + EnumTableBDD.DOCUMENT_ACHAT_LIGNE + " lig INNER JOIN " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE
        + " art ON (art.A1ETB = lig.LAETB AND art.A1ART = lig.LAART) INNER JOIN " + querymg.getLibrary() + '.'
        + EnumTableBDD.ARTICLE_EXTENSION + " ext ON (ext.A1ETB = lig.LAETB AND ext.A1ART = lig.LAART) WHERE ");
    
    for (IdLigneAchat idLigne : pListeIdLigneAchat) {
      requete.ajouter("(");
      requete.ajouterConditionAnd("lig.LAETB", "=", idLigne.getIdEtablissement().getCodeEtablissement());
      requete.ajouterConditionAnd("lig.LACOD", "=", idLigne.getCodeEntete().getCode());
      requete.ajouterConditionAnd("lig.LANUM", "=", idLigne.getIdDocumentAchat().getNumero());
      requete.ajouterConditionAnd("lig.LASUF", "=", idLigne.getIdDocumentAchat().getSuffixe());
      requete.ajouterConditionAnd("lig.LANLI", "=", idLigne.getNumeroLigne());
      requete.ajouter(") or ");
    }
    requete.supprimerDernierOperateur("or");
    
    ListeLigneAchat listeLigneAchat = new ListeLigneAchat();
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return listeLigneAchat;
    }
    
    // Lecture des champs
    for (GenericRecord record : listeRecord) {
      // On détermine le type de la ligne
      if (record.getStringValue("lig.LAERL") != null) {
        EnumTypeLigneAchat typeLigne = EnumTypeLigneAchat.valueOfByCode(record.getCharacterValue("lig.LAERL"));
        
        // C'est un article commentaire
        if ((typeLigne != null && typeLigne.equals(EnumTypeLigneAchat.COMMENTAIRE))) {
          listeLigneAchat.add(completerLigneCommentaireAchat(record));
        }
        // Sinon c'est un article normal
        else {
          listeLigneAchat.add(completerLigneArticleAchat(record));
        }
      }
    }
    
    return listeLigneAchat;
  }
  
  /**
   * Compléter une LigneAchat à partir d'un record
   */
  private LigneAchat completerLigneCommentaireAchat(GenericRecord pRecord) {
    // Identifiant établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("LAETB"));
    // Identifiant document d'achat
    EnumCodeEnteteDocumentAchat codeEntete = EnumCodeEnteteDocumentAchat.valueOfByCode(pRecord.getCharacterValue("LACOD"));
    IdDocumentAchat idDocumentAchat =
        IdDocumentAchat.getInstance(idEtablissement, codeEntete, pRecord.getIntegerValue("LANUM"), pRecord.getIntegerValue("LASUF"));
    // Identifiant ligne d'achat
    IdLigneAchat idLigneAchat = IdLigneAchat.getInstance(idDocumentAchat, pRecord.getIntegerValue("LANLI"));
    // Nouvelle ligneAchat
    LigneAchatCommentaire ligneAchat = new LigneAchatCommentaire(idLigneAchat);
    ligneAchat.setLibelleArticle(
        pRecord.getStringValue("A1LB1") + " " + pRecord.getStringValue("A1LB2") + " " + pRecord.getStringValue("A1LB3"));
    ligneAchat.setTypeArticle(EnumTypeArticle.valueOfByCodeAlpha(pRecord.getStringValue("A1IN3")));
    
    return ligneAchat;
  }
  
  /**
   * Compléter une LigneAchat article à partir d'un record
   */
  private LigneAchat completerLigneArticleAchat(GenericRecord pRecord) {
    // Identifiant établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("LAETB"));
    // Identifiant document d'achat
    EnumCodeEnteteDocumentAchat codeEntete = EnumCodeEnteteDocumentAchat.valueOfByCode(pRecord.getCharacterValue("LACOD"));
    IdDocumentAchat idDocumentAchat =
        IdDocumentAchat.getInstance(idEtablissement, codeEntete, pRecord.getIntegerValue("LANUM"), pRecord.getIntegerValue("LASUF"));
    // Identifiant ligne d'achat
    IdLigneAchat idLigneAchat = IdLigneAchat.getInstance(idDocumentAchat, pRecord.getIntegerValue("LANLI"));
    // Nouvelle ligneAchat
    LigneAchatArticle ligneAchat = new LigneAchatArticle(idLigneAchat);
    
    // Données
    ligneAchat.setTypeLigne(EnumTypeLigneAchat.valueOfByCode(pRecord.getCharacterValue("LAERL")));
    ligneAchat.setCodeExtraction(EnumCodeExtractionDocumentAchat.valueOfByCode(pRecord.getCharacterValue("LACEX")));
    ligneAchat.setDateLivraisonPrevue(pRecord.getDateValue("LADLP"));
    ligneAchat.setIdArticle(IdArticle.getInstance(idEtablissement, pRecord.getStringValue("LAART")));
    ligneAchat.setLibelleArticle(
        pRecord.getStringValue("A1LB1") + " " + pRecord.getStringValue("A1LB2") + " " + pRecord.getStringValue("A1LB3"));
    ligneAchat.setTypeArticle(EnumTypeArticle.valueOfByCodeAlpha(pRecord.getStringValue("A1IN3")));
    
    // Alimentation de la classe ParamètrePrixAchat avec les données retournées
    ParametrePrixAchat parametre = new ParametrePrixAchat();
    // Renseigner les unités
    if (!pRecord.getStringValue("LAUNA").trim().isEmpty()) {
      IdUnite idUA = IdUnite.getInstance(pRecord.getStringValue("LAUNA"));
      parametre.setIdUA(idUA);
    }
    if (!pRecord.getStringValue("LAUNC").trim().isEmpty()) {
      IdUnite idUCA = IdUnite.getInstance(pRecord.getStringValue("LAUNC"));
      parametre.setIdUCA(idUCA);
    }
    // Renseigner les ratios
    parametre.setNombreUAParUCA(pRecord.getBigDecimalValue("LAKAC"));
    parametre.setNombreUSParUCA(pRecord.getBigDecimalValue("LAKSC"));
    
    // Renseigner les quantités
    parametre.setQuantiteReliquatUA(pRecord.getBigDecimalValue("LAQTA"));
    parametre.setQuantiteReliquatUCA(pRecord.getBigDecimalValue("LAQTC"));
    parametre.setQuantiteReliquatUS(pRecord.getBigDecimalValue("LAQTS"));
    // Renseigner les quantités initiales
    parametre.setQuantiteInitialeUA(pRecord.getBigDecimalValue("LAQTAI"));
    parametre.setQuantiteInitialeUCA(pRecord.getBigDecimalValue("LAQTCI"));
    parametre.setQuantiteInitialeUS(pRecord.getBigDecimalValue("LAQTSI"));
    // Renseigner les quantités traitées
    parametre.setQuantiteTraiteeUA(pRecord.getBigDecimalValue("LAQTAT"));
    parametre.setQuantiteTraiteeUCA(pRecord.getBigDecimalValue("LAQTCT"));
    parametre.setQuantiteTraiteeUS(pRecord.getBigDecimalValue("LAQTST"));
    // Renseigner les montants
    parametre.setPrixAchatBrutHT(pRecord.getBigDecimalValue("LAPAB"));
    ListeRemise listeRemise = parametre.getListeRemise();
    listeRemise.setPourcentageRemise1(pRecord.getBigDecimalValue("LAREM1"));
    listeRemise.setPourcentageRemise2(pRecord.getBigDecimalValue("LAREM2"));
    listeRemise.setPourcentageRemise3(pRecord.getBigDecimalValue("LAREM3"));
    listeRemise.setPourcentageRemise4(pRecord.getBigDecimalValue("LAREM4"));
    listeRemise.setPourcentageRemise5(pRecord.getBigDecimalValue("LAREM5"));
    listeRemise.setPourcentageRemise6(pRecord.getBigDecimalValue("LAREM6"));
    listeRemise.setTypeRemise(pRecord.getCharacterValue("LATRL"));
    listeRemise.setBaseRemise(pRecord.getCharacterValue("LABRL"));
    // parametre.setPourcentageTaxeOuMajoration(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFK3]));
    // parametre.setMontantConditionnement(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFV3]));
    parametre.setPrixNetHT(pRecord.getBigDecimalValue("LAPAC"));
    // parametre.setMontantAuPoidsPortHT(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFV1]));
    // parametre.setPoidsPort(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFK1]));
    // parametre.setPourcentagePort(new BigPercentage(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFP1]), false));
    // parametre.setMontantPortHT((BigDecimal) ligneBrute[Svgam0059d.VAR_W7PORTF]);
    // parametre.setPrixRevientFournisseurHT((BigDecimal) ligneBrute[Svgam0059d.VAR_W7PRVFR]);
    parametre.setMontantReliquatHT(pRecord.getBigDecimalValue("LAMHT"));
    parametre.setMontantInitialHT(pRecord.getBigDecimalValue("LAMHTI"));
    parametre.setMontantTraiteHT(pRecord.getBigDecimalValue("LAMHTT"));
    
    // Renseigner les exclusions de remises de pied
    Character[] exlusionremise = parametre.getExclusionRemisePied();
    exlusionremise[0] = (pRecord.getCharacterValue("LARP1"));
    exlusionremise[1] = (pRecord.getCharacterValue("LARP2"));
    exlusionremise[2] = (pRecord.getCharacterValue("LARP3"));
    exlusionremise[3] = (pRecord.getCharacterValue("LARP4"));
    exlusionremise[4] = (pRecord.getCharacterValue("LARP5"));
    exlusionremise[5] = (pRecord.getCharacterValue("LARP6"));
    
    // Renseigner les indicateurs
    parametre.setTopPrixBaseSaisie(pRecord.getBigDecimalValue("LATB").intValue());
    parametre.setTopRemiseSaisie(pRecord.getBigDecimalValue("LATR").intValue());
    parametre.setTopPrixNetSaisi(pRecord.getBigDecimalValue("LATN").intValue());
    parametre.setTopUniteSaisie(pRecord.getBigDecimalValue("LATU").intValue());
    parametre.setTopQuantiteSaisie(pRecord.getBigDecimalValue("LATQ").intValue());
    parametre.setTopMontantHTSaisi(pRecord.getBigDecimalValue("LATH").intValue());
    
    ligneAchat.setPrixAchat(new PrixAchat());
    ligneAchat.getPrixAchat().initialiser(parametre);
    
    return ligneAchat;
  }
  
  /**
   * Lire les lignes de réception d'achat pour la liste des identifiants fournis.
   */
  public ListeReceptionAchat chargerListeReceptionAchat(List<IdLigneAchat> pListeIdLigneAchatReception) {
    ListeReceptionAchat listeReceptionAchat = new ListeReceptionAchat();
    
    // Générer la requête
    RequeteSql requete = new RequeteSql();
    // @formatter:off
    requete.ajouter("select lig.LAETB, lig.LACOD, lig.LANUM, lig.LASUF, lig.LANLI, lig.LAQTA, lig.LAPAC, lig.LAUNA,"
        + " ent.EAREC, ent.EACOL, ent.EAFRS, ent.EARBL,"
        + " cmd.EARBC, cmd.EAACH from "
        + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT_LIGNE + " lig"
        + " inner join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT + " ent"
        + " on ent.EACOD = lig.LACOD and ent.EAETB = lig.LAETB and ent.EANUM = lig.LANUM and ent.EASUF = lig.LASUF "
        + " inner join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT + " cmd"
        + " on cmd.EACOD = lig.LACOD and cmd.EAETB = lig.LAETB and cmd.EANUM = lig.LANUM and cmd.EASUF = 0 WHERE ");
    // @formatter:on
    
    for (IdLigneAchat idLigne : pListeIdLigneAchatReception) {
      requete.ajouter("(");
      requete.ajouterConditionAnd("lig.LAETB", "=", idLigne.getIdEtablissement().getCodeEtablissement());
      requete.ajouterConditionAnd("lig.LACOD", "=", 'E');
      requete.ajouterConditionAnd("lig.LANUM", "=", idLigne.getIdDocumentAchat().getNumero());
      requete.ajouterConditionAnd("lig.LASUF", "=", idLigne.getIdDocumentAchat().getSuffixe());
      requete.ajouterConditionAnd("lig.LANLI", "=", idLigne.getNumeroLigne());
      requete.ajouter(") or ");
    }
    requete.supprimerDernierOperateur("or");
    requete.ajouter("order by EAREC desc");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return listeReceptionAchat;
    }
    
    // Lecture des champs
    for (GenericRecord record : listeRecord) {
      ReceptionAchat reception = null;
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("LAETB"));
      EnumCodeEnteteDocumentAchat codeEntete = EnumCodeEnteteDocumentAchat.valueOfByCode(record.getCharacterValue("LACOD"));
      IdDocumentAchat idDocumentAchat =
          IdDocumentAchat.getInstance(idEtablissement, codeEntete, record.getIntegerValue("LANUM"), record.getIntegerValue("LASUF"));
      reception = new ReceptionAchat(IdReceptionAchat.getInstance(idDocumentAchat, record.getIntegerValue("LANLI")));
      reception.setNumeroReception(idDocumentAchat.getNumero());
      reception.setDateReception(record.getDateValue("EAREC"));
      reception
          .setIdFournisseur(IdFournisseur.getInstance(idEtablissement, record.getIntegerValue("EACOL"), record.getIntegerValue("EAFRS")));
      reception.setReferences(record.getStringValue("EARBL") + record.getStringValue("EARBC"));
      reception.setQuantiteUCA(record.getBigDecimalValue("LAQTA", new BigDecimal(4)));
      reception.setPrixAchat(record.getBigDecimalValue("LAPAC", new BigDecimal(2)));
      reception.setAcheteur(record.getStringValue("EAACH"));
      if (!record.getStringValue("LAUNA").trim().isEmpty()) {
        IdUnite idUnite = IdUnite.getInstance(record.getStringValue("LAUNA"));
        reception.setIdUniteAchat(idUnite);
      }
      
      listeReceptionAchat.add(reception);
    }
    
    return listeReceptionAchat;
  }
  
  /**
   * Charger la liste de commandes d'achat générées automatiquement à partir d'un document de vente.
   */
  public List<IdDocumentAchat> chargerCommandeAchatliee(IdDocumentVente pIdDocumentVente) {
    if (pIdDocumentVente == null) {
      return null;
    }
    IdEtablissement idEtablissement = pIdDocumentVente.getIdEtablissement();
    
    List<IdDocumentAchat> listeIdDocumentAchat = null;
    RequeteSql requete = new RequeteSql();
    requete.ajouter("SELECT AACOE, AANOE, AASOE FROM " + querymg.getLibrary() + "." + EnumTableBDD.LIEN_LIGNE_VENTE_LIGNE_ACHAT
        + " WHERE AATOS = 'V' AND (AATOE = 'G' OR AATOE = 'A') ");
    requete.ajouterConditionAnd("AAETB", "=", pIdDocumentVente.getCodeEtablissement());
    // Si le document est une facture
    if (pIdDocumentVente.isIdFacture()) {
      if (pIdDocumentVente.getCodeEntete() != null) {
        requete.ajouterConditionAnd("AACOS", "=", pIdDocumentVente.getCodeEntete().getCode());
      }
      requete.ajouterConditionAnd("AANOS", "=", pIdDocumentVente.getNumeroFacture());
    }
    // Si le document n'est pas une facture
    else {
      if (pIdDocumentVente.getCodeEntete() != null) {
        requete.ajouterConditionAnd("AACOS", "=", pIdDocumentVente.getCodeEntete().getCode());
      }
      requete.ajouterConditionAnd("AANOS", "=", pIdDocumentVente.getNumero());
      requete.ajouterConditionAnd("AASOS", "=", pIdDocumentVente.getSuffixe());
    }
    requete.ajouter(" GROUP BY AACOE, AANOE, AASOE ORDER BY AANOE DESC");
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    listeIdDocumentAchat = new ArrayList<IdDocumentAchat>();
    // Génèration des identifiants des documents trouvés
    for (GenericRecord record : listeRecord) {
      EnumCodeEnteteDocumentAchat codeEntete = EnumCodeEnteteDocumentAchat.valueOfByCode(record.getCharacterValue("AACOE"));
      IdDocumentAchat idDocumentAchat =
          IdDocumentAchat.getInstance(idEtablissement, codeEntete, record.getIntegerValue("AANOE"), record.getIntegerValue("AASOE"));
      listeIdDocumentAchat.add(idDocumentAchat);
    }
    
    return listeIdDocumentAchat;
  }
  
  /**
   * Retourne les montants totaux HT, TTC et TVA pour un document.
   * Note:
   * - Comment être sur que la ligne n'est pas annulée.
   * - Quelle TVA utiliser pour le port de l'entête.
   */
  public ValeurInitialeCommandeAchat chargerDonneesCommandeInitiale(IdDocumentAchat pIdDocumentAchat) {
    if (pIdDocumentAchat == null) {
      return null;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    // Construction de la requête
    requeteSql.ajouter("select LAMHTI, LATVA, EASUF, EATLV, EATV1, EATV2, EATV3, EAIN10, EAPOR1 from " + querymg.getLibrary() + "."
        + EnumTableBDD.DOCUMENT_ACHAT_LIGNE);
    requeteSql.ajouter("left join " + querymg.getLibrary() + "." + EnumTableBDD.DOCUMENT_ACHAT + " on ");
    requeteSql.ajouterConditionAnd("EACOD", "=", "LACOD", true, false);
    requeteSql.ajouterConditionAnd("EAETB", "=", "LAETB", true, false);
    requeteSql.ajouterConditionAnd("EANUM", "=", "LANUM", true, false);
    requeteSql.ajouterConditionAnd("EASUF", "=", "LASUF", true, false);
    requeteSql.ajouter(" where ");
    requeteSql.ajouterConditionAnd("LACOD", "=", pIdDocumentAchat.getCodeEntete().getCode());
    requeteSql.ajouterConditionAnd("LAETB", "=", pIdDocumentAchat.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("LANUM", "=", pIdDocumentAchat.getNumero());
    requeteSql.ajouterConditionAnd("LACEX", "<>", "A");
    requeteSql.ajouter(" order by LASUF");
    
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Analyse des données
    BigDecimal montantHTTva1 = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    BigDecimal montantHTTva2 = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    BigDecimal montantHTTva3 = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    BigDecimal montantTva1 = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    BigDecimal montantTva2 = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    BigDecimal montantTva3 = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    
    // Données de l'entête
    BigDecimal eatv1 = listeRecord.get(0).getBigDecimalValue("EATV1", BigDecimal.ZERO)
        .divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32).setScale(2, RoundingMode.HALF_UP);
    BigDecimal eatv2 = listeRecord.get(0).getBigDecimalValue("EATV2", BigDecimal.ZERO)
        .divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32).setScale(2, RoundingMode.HALF_UP);
    BigDecimal eatv3 = listeRecord.get(0).getBigDecimalValue("EATV3", BigDecimal.ZERO)
        .divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32).setScale(2, RoundingMode.HALF_UP);
    BigDecimal portHT = BigDecimal.ZERO;
    boolean commandeNonReceptionnee = true;
    for (GenericRecord record : listeRecord) {
      // Données entêtes
      Integer easuf = record.getIntegerValue("EASUF", -1);
      BigDecimal eatlv = record.getBigDecimalValue("EATLV", BigDecimal.ZERO);
      String eain10 = record.getStringValue("EAIN10", "", true);
      BigDecimal eapor1 = record.getBigDecimalValue("EAPOR1", BigDecimal.ZERO).setScale(2, RoundingMode.HALF_UP);
      // Données lignes
      BigDecimal lamhti = record.getBigDecimalValue("LAMHTI", BigDecimal.ZERO);
      Integer latva = record.getIntegerValue("LATVA", 0);
      
      // Contrôle du suffixe afin de déterminer s'il a eu des extractions ou non
      if (easuf.intValue() > 0) {
        commandeNonReceptionnee = false;
      }
      
      // Comptabilisation des lignes uniquement liées à l'identifiant en cours (car tous suffixes ont été récupéré)
      if (easuf.equals(pIdDocumentAchat.getSuffixe())) {
        switch (latva) {
          case 1:
            montantHTTva1 = montantHTTva1.add(lamhti);
            montantTva1 = montantHTTva1.multiply(eatv1);
            break;
          case 2:
            montantHTTva2 = montantHTTva2.add(lamhti);
            montantTva2 = montantHTTva2.multiply(eatv2);
            break;
          case 3:
            montantHTTva3 = montantHTTva3.add(lamhti);
            montantTva3 = montantHTTva3.multiply(eatv3);
            break;
        }
      }
      // Récupération du port si présent
      // Le champ eatlv doit être différent de 0 (sinon une réception a déjà eu lieu le port ne sera donc pas présent dans cet
      // enregistrement)
      // Le port ne doit pas déjà été initialisé et le port stocké doit contenir une valeur > 0
      if (eatlv.intValue() != 0 && portHT.compareTo(BigDecimal.ZERO) == 0 && eapor1.compareTo(BigDecimal.ZERO) > 0) {
        if (!eain10.equals("1")) {
          portHT = eapor1.setScale(2, RoundingMode.HALF_UP);
        }
      }
    }
    
    BigDecimal valeur = null;
    BigDecimal valeurHT = null;
    ValeurInitialeCommandeAchat valeurInitialeCommandeAchat = new ValeurInitialeCommandeAchat();
    // Montant total HT
    valeurHT = montantHTTva1.add(montantHTTva2).add(montantHTTva3).setScale(2, RoundingMode.HALF_UP);
    valeurInitialeCommandeAchat.setMontantHT(valeurHT);
    // Montant total TVA
    valeur = montantTva1.add(montantTva2).add(montantTva3).setScale(2, RoundingMode.HALF_UP);
    valeurInitialeCommandeAchat.setMontantTVA(valeur);
    // Ajout du port si besoin
    valeurInitialeCommandeAchat.setMontantPortHT(portHT);
    if (portHT.compareTo(BigDecimal.ZERO) > 0) {
      // Mise à jour du montant HT avec le port HT
      valeurHT = valeurHT.add(portHT).setScale(2, RoundingMode.HALF_UP);
      valeurInitialeCommandeAchat.setMontantHT(valeurHT);
      // Calcul du montant de la TVA pour le port
      valeur = portHT.multiply(eatv1).setScale(2, RoundingMode.HALF_UP);
      // Mise à jour du montant de la TVA avec celle du port
      valeur = valeurInitialeCommandeAchat.getMontantTVA().add(valeur);
      valeurInitialeCommandeAchat.setMontantTVA(valeur);
    }
    // Montant total TTC
    valeur = valeurHT.add(valeurInitialeCommandeAchat.getMontantTVA()).setScale(2, RoundingMode.HALF_UP);
    valeurInitialeCommandeAchat.setMontantTTC(valeur);
    
    // Indication si la commande a été réceptionnée ou non (basé sur le contrôle du suffixe)
    if (commandeNonReceptionnee) {
      valeurInitialeCommandeAchat.setEtapeExtraction(EnumEtapeExtraction.AUCUNE);
    }
    // Réception partielle ou complète
    else {
      valeurInitialeCommandeAchat.setEtapeExtraction(EnumEtapeExtraction.PARTIELLE);
    }
    
    return valeurInitialeCommandeAchat;
  }
  
  // -- Méthodes privées
  
  /**
   * Créer un document à partir des champs de la base de données.
   */
  private void completerDocument(GenericRecord pRecord, DocumentAchat pDocumentAchat) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("EAETB"));
    if (pRecord.isPresentField("EACRE")) {
      pDocumentAchat.setDateCreation(pRecord.getDateValue("EACRE"));
    }
    if (pRecord.isPresentField("EADAT")) {
      pDocumentAchat.setDateDernierTraitement(pRecord.getDateValue("EADAT"));
    }
    if (pRecord.isPresentField("EAHOM")) {
      pDocumentAchat.setDateHomologation(pRecord.getDateValue("EAHOM"));
    }
    if (pRecord.isPresentField("EAREC")) {
      pDocumentAchat.setDateReception(pRecord.getDateValue("EAREC"));
    }
    if (pRecord.isPresentField("EAFAC")) {
      pDocumentAchat.setDateFacturation(pRecord.getDateValue("EAFAC"));
    }
    if (pRecord.isPresentField("EAETA")) {
      EnumCodeEtatDocument codeEtat = EnumCodeEtatDocument.valueOfByCode(pRecord.getIntegerValue("EAETA"));
      pDocumentAchat.setCodeEtat(codeEtat);
    }
    // L'id du fournisseur
    if (pRecord.isPresentField("EACOL") && pRecord.isPresentField("EAFRS")) {
      IdFournisseur idFournisseur =
          IdFournisseur.getInstance(idEtablissement, pRecord.getIntegerValue("EACOL"), pRecord.getIntegerValue("EAFRS"));
      pDocumentAchat.setIdFournisseur(idFournisseur);
    }
    
    // Cas particulier de la commande où pour avoir le montant HT et TTC il faut récupérer ces informations "manuellement" si des
    // récéptions ont déjà eu lieu
    if (pDocumentAchat.isCommande()) {
      // A supprimer avant commit (c'est pour le débug)
      if (pRecord.isPresentField("EATHTL")) {
        Trace.info(pDocumentAchat.getId() + " Montant HT en base : " + pRecord.getBigDecimalValue("EATHTL"));
      }
      if (pRecord.isPresentField("EATTC")) {
        Trace.info(pDocumentAchat.getId() + " Montant TTC en base : " + pRecord.getBigDecimalValue("EATTC"));
      }
      // Fin suppression
      ValeurInitialeCommandeAchat valeurInitialeCommandeAchat = chargerDonneesCommandeInitiale(pDocumentAchat.getId());
      if (valeurInitialeCommandeAchat == null) {
        Trace.erreur("Problème lors de la récupération des montants HT et TTC du document d'achat " + pDocumentAchat.getId());
      }
      else {
        pDocumentAchat.setTotalHTLignes(valeurInitialeCommandeAchat.getMontantHT());
        pDocumentAchat.setTotalTTC(valeurInitialeCommandeAchat.getMontantTTC());
        pDocumentAchat.setEtapeExtraction(valeurInitialeCommandeAchat.getEtapeExtraction());
        Trace.info(pDocumentAchat.getId() + " Montant HT calculé  " + pDocumentAchat.getTotalHTLignes());
        Trace.info(pDocumentAchat.getId() + " Montant TTC calculé " + pDocumentAchat.getTotalTTC());
      }
    }
    else {
      if (pRecord.isPresentField("EATHTL")) {
        pDocumentAchat.setTotalHTLignes(pRecord.getBigDecimalValue("EATHTL"));
      }
      if (pRecord.isPresentField("EATTC")) {
        pDocumentAchat.setTotalTTC(pRecord.getBigDecimalValue("EATTC"));
      }
    }
    
    if (pRecord.isPresentField("EAMAG")) {
      pDocumentAchat.setIdMagasinSortie(IdMagasin.getInstance(idEtablissement, pRecord.getStringValue("EAMAG")));
    }
    if (pRecord.isPresentField("EAACH")) {
      IdAcheteur idAcheteur = IdAcheteur.getInstance(idEtablissement, pRecord.getStringValue("EAACH"));
      pDocumentAchat.setIdAcheteur(idAcheteur);
    }
    if (pRecord.isPresentField("EARBL")) {
      pDocumentAchat.setReferenceBonLivraison(pRecord.getStringValue("EARBL"));
    }
    if (pRecord.isPresentField("EARBC")) {
      pDocumentAchat.setReferenceCommande(pRecord.getStringValue("EARBC"));
    }
    if (pRecord.isPresentField("EADLP")) {
      pDocumentAchat.setDateLivraisonPrevue(pRecord.getDateValue("EADLP"));
    }
    if (pRecord.isPresentField("EARFL")) {
      pDocumentAchat.setReferenceInterne(pRecord.getStringValue("EARFL"));
    }
    if (pRecord.isPresentField("EANFA")) {
      pDocumentAchat.setNumeroFacture(pRecord.getIntegerValue("EANFA"));
    }
    
    // L'adresse du fournisseur depuis les commandes
    Adresse adresseFournisseur = new Adresse();
    pDocumentAchat.setAdresseFournisseur(adresseFournisseur);
    if (pRecord.isPresentField("ADNOM") && pRecord.getString("ADNOM") != null) {
      adresseFournisseur.setNom(pRecord.getStringValue("ADNOM", "", true));
      if (pRecord.isPresentField("ADCPL")) {
        adresseFournisseur.setComplementNom(pRecord.getStringValue("ADCPL", "", true));
      }
      if (pRecord.isPresentField("ADRUE")) {
        adresseFournisseur.setRue(pRecord.getStringValue("ADRUE", "", true));
      }
      if (pRecord.isPresentField("ADLOC")) {
        adresseFournisseur.setLocalisation(pRecord.getStringValue("ADLOC", "", true));
      }
      if (pRecord.isPresentField("ADCDP")) {
        adresseFournisseur.setCodePostal(pRecord.getIntegerValue("ADCDP", 0));
      }
      if (pRecord.isPresentField("ADVIL")) {
        adresseFournisseur.setVille(pRecord.getStringValue("ADVIL", "", true));
      }
      if (pRecord.isPresentField("ADPAY")) {
        adresseFournisseur.setPays(pRecord.getStringValue("ADPAY", "", true));
      }
    }
    // L'adresse du fournisseur depuis les adresses fournisseurs
    else if (pRecord.isPresentField("FENOM") && pRecord.getString("FENOM") != null) {
      adresseFournisseur.setNom(pRecord.getStringValue("FENOM", "", true));
      if (pRecord.isPresentField("FECPL")) {
        adresseFournisseur.setComplementNom(pRecord.getStringValue("FECPL", "", true));
      }
      if (pRecord.isPresentField("FERUE")) {
        adresseFournisseur.setRue(pRecord.getStringValue("FERUE", "", true));
      }
      if (pRecord.isPresentField("FELOCG")) {
        adresseFournisseur.setLocalisation(pRecord.getStringValue("FELOCG", "", true));
      }
      if (pRecord.isPresentField("FECDP")) {
        adresseFournisseur.setCodePostal(pRecord.getIntegerValue("FECDP", 0));
      }
      if (pRecord.isPresentField("FEVIL")) {
        adresseFournisseur.setVille(pRecord.getStringValue("FEVIL", "", true));
      }
      if (pRecord.isPresentField("FEPAY")) {
        adresseFournisseur.setPays(pRecord.getStringValue("FEPAY", "", true));
      }
    }
    // L'adresse du fournisseur depuis les fournisseurs
    else if (pRecord.isPresentField("FRNOM") && pRecord.getString("FRNOM") != null) {
      adresseFournisseur.setNom(pRecord.getStringValue("FRNOM", "", true));
      if (pRecord.isPresentField("FRCPL")) {
        adresseFournisseur.setComplementNom(pRecord.getStringValue("FECPL", "", true));
      }
      if (pRecord.isPresentField("FRRUE")) {
        adresseFournisseur.setRue(pRecord.getStringValue("FRRUE", "", true));
      }
      if (pRecord.isPresentField("FRLOC")) {
        adresseFournisseur.setLocalisation(pRecord.getStringValue("FRLOC", "", true));
      }
      if (pRecord.isPresentField("FRCDP")) {
        adresseFournisseur.setCodePostal(pRecord.getIntegerValue("FRCDP", 0));
      }
      if (pRecord.isPresentField("FRVIL")) {
        adresseFournisseur.setVille(pRecord.getStringValue("FRVIL", "", true));
      }
      if (pRecord.isPresentField("FRPAY")) {
        adresseFournisseur.setPays(pRecord.getStringValue("FRPAY", "", true));
      }
    }
  }
  
}
