/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v1;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;

/**
 * Facture Série N au format Magento
 * Cette classe sert à l'intégration JAVA vers JSON
 * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonFactureMagentoV1 extends JsonEntiteMagento {
  private String nomFacture = null;
  private String commandeMagento = null;
  private String factureClient = null;
  
  /**
   * Constructeur de la facture Magento
   */
  public JsonFactureMagentoV1(FluxMagento record) {
    idFlux = record.getFLIDF();
    versionFlux = record.getFLVER();
    etb = record.getFLETB();
  }
  
  public String getNomFacture() {
    return nomFacture;
  }
  
  public void setNomFacture(String nomFacture) {
    this.nomFacture = nomFacture;
  }
  
  public String getFactureClient() {
    return factureClient;
  }
  
  public void setFactureClient(String factureClient) {
    this.factureClient = factureClient;
  }
  
  public String getCommandeMagento() {
    return commandeMagento;
  }
  
  public void setCommandeMagento(String commandeMagento) {
    this.commandeMagento = commandeMagento;
  }
  
}
