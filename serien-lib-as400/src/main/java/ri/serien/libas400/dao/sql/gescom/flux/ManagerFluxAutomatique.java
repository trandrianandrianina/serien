/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.exploitation.personnalisation.flux.ListePersonnalisationFlux;
import ri.serien.libcommun.exploitation.personnalisation.flux.PersonnalisationFlux;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe gère les flux qui sont déclenchés automatiquement côté Série N.
 * Par exemple le flux stock se déclenche à intervalle régulier (définit dans le paramètre FX).
 */
public class ManagerFluxAutomatique {
  // Variables
  private ParametresFluxBibli parametresFlux = null;
  private QueryManager queryManager = null;
  private SystemeManager systemeManager = null;
  private ManagerFluxMagento managerFluxMagento = null;
  
  /**
   * Constructeur.
   */
  public ManagerFluxAutomatique(ParametresFluxBibli pParametresFluxBibli) {
    parametresFlux = pParametresFluxBibli;
    
    systemeManager = new SystemeManager(true);
    queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
    if (parametresFlux.getLibrairie() != null) {
      queryManager.setLibrary(parametresFlux.getLibrairie().getNom());
    }
    managerFluxMagento = new ManagerFluxMagento(pParametresFluxBibli);
    Trace.info("Le manager de flux automatiques est demarré");
    initialiser();
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne les flux automatiques déclarés dans la personnalisation de l'exploitation.
   */
  public ArrayList<FluxAutomatique> retournerListeFluxAutomatique() {
    if (queryManager == null) {
      return null;
    }
    if (managerFluxMagento == null) {
      return null;
    }
    
    ListePersonnalisationFlux listePersonnalisationFlux = ManagerFluxMagento.listePersonnalisationFlux;
    if (listePersonnalisationFlux == null || listePersonnalisationFlux.isEmpty()) {
      Trace.erreur("Il n'y a aucune personnalisation de flux dans la base de données " + queryManager.getLibrary() + '.');
      return null;
    }
    
    ArrayList<FluxAutomatique> listeAutomatique = new ArrayList<FluxAutomatique>();
    for (PersonnalisationFlux personnalisationFlux : listePersonnalisationFlux) {
      // Contrôle que la personnalisation concerne un flux automatique
      if (personnalisationFlux != null && !FluxAutomatique.isFluxAutomatique(personnalisationFlux.getTypeFlux())) {
        continue;
      }
      
      // Création du flux automatique à partir de sa personnalisation
      try {
        FluxAutomatique fluxAutomatique = new FluxAutomatique(personnalisationFlux, false);
        listeAutomatique.add(fluxAutomatique);
        Trace.debug(ManagerFluxAutomatique.class, "retournerFluxAutomatique", "idFlux", fluxAutomatique.getIdFlux(), "code établissement",
            fluxAutomatique.getCodeEtablissement());
      }
      catch (Exception e) {
        Trace.erreur("Erreur lors de l'instanciation d'un flux automatique.");
      }
    }
    
    return listeAutomatique;
  }
  
  /**
   * Permet de forcer le déclenchement d'un flux automatique.
   * 
   * @param pTypeFlux
   */
  public void forcerDeclenchementFluxAutomatique(EnumTypeFlux pEnumTypeFlux) {
    if (pEnumTypeFlux == null) {
      return;
    }
    if (!FluxAutomatique.isFluxAutomatique(pEnumTypeFlux)) {
      return;
    }
    
    // Récpération de la personnalisation
    ListePersonnalisationFlux listePersonnalisationFlux = ManagerFluxMagento.listePersonnalisationFlux;
    if (listePersonnalisationFlux == null || listePersonnalisationFlux.isEmpty()) {
      Trace.erreur("Il n'y a aucune personnalisation de flux dans la base de données " + queryManager.getLibrary() + '.');
      return;
    }
    
    try {
      FluxAutomatique fluxAutomatique = null;
      // Parcourt de la personnalisation du flux à forcer
      for (PersonnalisationFlux personnalisationFlux : listePersonnalisationFlux) {
        // Contrôle que la personnalisation concerne notre flux à déclencher
        if (personnalisationFlux == null || !pEnumTypeFlux.equals(personnalisationFlux.getTypeFlux())) {
          continue;
        }
        
        // Création du flux automatique à partir de sa personnalisation
        fluxAutomatique = new FluxAutomatique(personnalisationFlux, true);
        Trace.debug(ManagerFluxAutomatique.class, "retournerFluxAutomatique", "idFlux", fluxAutomatique.getIdFlux(), "code établissement",
            fluxAutomatique.getCodeEtablissement());
        fluxAutomatique.initialiser(managerFluxMagento);
      }
      
      // Démarrage du flux à forcer
      if (fluxAutomatique != null) {
        Trace.info("Le déclenchement du type de flux " + pEnumTypeFlux.getCode() + " a été forcé.");
        pEnumTypeFlux = null;
        fluxAutomatique.start();
      }
    }
    catch (Exception e) {
      Trace.erreur("Erreur lors de l'instanciation d'un flux automatique.");
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise les flux automatiques.
   */
  private void initialiser() {
    ArrayList<FluxAutomatique> listeFluxAutomatique = retournerListeFluxAutomatique();
    if (listeFluxAutomatique == null || listeFluxAutomatique.isEmpty()) {
      Trace.info("Aucun flux automatique n'a été configuré.");
      return;
    }
    
    // Démarrage des flux automatiques
    for (FluxAutomatique fluxAutomatique : listeFluxAutomatique) {
      fluxAutomatique.initialiser(managerFluxMagento);
      if (fluxAutomatique.isADemarrer()) {
        fluxAutomatique.start();
        continue;
      }
      Trace.alerte("Le flux " + fluxAutomatique.getIdFlux() + " n'a pas été démarré car il n'est pas actif.");
    }
  }
  
}
