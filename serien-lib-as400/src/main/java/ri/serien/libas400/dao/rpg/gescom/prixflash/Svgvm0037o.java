/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.prixflash;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0037o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POPVN = 9;
  public static final int DECIMAL_POPVN = 2;
  public static final int SIZE_POUSR = 10;
  public static final int SIZE_PODAT = 8;
  public static final int SIZE_POHEU = 4;
  public static final int DECIMAL_POHEU = 0;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 42;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POPVN = 1;
  public static final int VAR_POUSR = 2;
  public static final int VAR_PODAT = 3;
  public static final int VAR_POHEU = 4;
  public static final int VAR_POARR = 5;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal popvn = BigDecimal.ZERO; // Prix de vente flash
  private String pousr = ""; // Code utilisateur
  private String podat = ""; // Date prix flash
  private BigDecimal poheu = BigDecimal.ZERO; // Heure
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_POPVN, DECIMAL_POPVN), // Prix de vente flash
      new AS400Text(SIZE_POUSR), // Code utilisateur
      new AS400Text(SIZE_PODAT), // Date prix flash
      new AS400ZonedDecimal(SIZE_POHEU, DECIMAL_POHEU), // Heure
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, popvn, pousr, podat, poheu, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    popvn = (BigDecimal) output[1];
    pousr = (String) output[2];
    podat = (String) output[3];
    poheu = (BigDecimal) output[4];
    poarr = (String) output[5];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPopvn(BigDecimal pPopvn) {
    if (pPopvn == null) {
      return;
    }
    popvn = pPopvn.setScale(DECIMAL_POPVN, RoundingMode.HALF_UP);
  }
  
  public void setPopvn(Double pPopvn) {
    if (pPopvn == null) {
      return;
    }
    popvn = BigDecimal.valueOf(pPopvn).setScale(DECIMAL_POPVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPopvn() {
    return popvn.setScale(DECIMAL_POPVN, RoundingMode.HALF_UP);
  }
  
  public void setPousr(String pPousr) {
    if (pPousr == null) {
      return;
    }
    pousr = pPousr;
  }
  
  public String getPousr() {
    return pousr;
  }
  
  public void setPodat(String pPodat) {
    if (pPodat == null) {
      return;
    }
    podat = pPodat;
  }
  
  public String getPodat() {
    return podat;
  }
  
  public void setPoheu(BigDecimal pPoheu) {
    if (pPoheu == null) {
      return;
    }
    poheu = pPoheu.setScale(DECIMAL_POHEU, RoundingMode.HALF_UP);
  }
  
  public void setPoheu(Integer pPoheu) {
    if (pPoheu == null) {
      return;
    }
    poheu = BigDecimal.valueOf(pPoheu);
  }
  
  public Integer getPoheu() {
    return poheu.intValue();
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
