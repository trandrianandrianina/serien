/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.prixvente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.Constantes;

/**
 * Cette classe décrit un enregistrement pour le testeur de calcul de prix de ventes.
 */
public class EnregistrementTestPrixVente implements Serializable {
  private Date dateCreation = null;
  private IdLigneVente idLigneVente = null;
  private IdClient idClient = null;
  private IdArticle idArticle = null;
  private String typeDocument = null;
  private BigDecimal prixBaseHTRPG = null;
  private BigDecimal prixBaseHTJava = null;
  private BigDecimal prixNetSaisiHTRPG = null;
  private BigDecimal prixNetCalculeHTRPG = null;
  private BigDecimal prixNetHTJava = null;
  private BigDecimal quantiteRPG = null;
  private BigDecimal quantiteJava = null;
  private BigDecimal montantHTRPG = null;
  private BigDecimal montantHTJava = null;
  private BigDecimal prixNetTTCRPG = null;
  private BigDecimal prixNetTTCJava = null;
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Générer un enregistrement pour le titre au format CSV.
   * 
   * @return
   */
  public static String genererTitreCsv() {
    String enregistrement = "";
    
    // la date de créatino du document d eventge
    enregistrement += "Date création;";
    
    // L'identifiant de la ligne de vente
    enregistrement += "Ligne de vente;";
    
    // L'identifiant du client
    enregistrement += "Client;";
    
    // L'identifiant de l'article
    enregistrement += "Article;";
    
    // Le type du document
    enregistrement += "Type document;";
    
    // Le prix base HT du calcul RPG
    enregistrement += "Prix base HT RPG;";
    
    // Le prix base HT du calcul Java
    enregistrement += "Prix base HT Java;";
    
    // Le prix unitaire HT du calcul RPG
    enregistrement += "Prix net saisi HT RPG;";
    
    // Le prix unitaire calculé HT du calcul RPG
    enregistrement += "Prix net calculé HT RPG;";
    
    // Le prix unitaire HT du calcul Java
    enregistrement += "Prix net HT Java;";
    
    // Le résultat de la comparaison des prix unitaires
    enregistrement += "Résultat prix net;";
    
    // Le montant HT du calcul RPG
    enregistrement += "Montant HT RPG;";
    
    // Le montant HT du calcul Java
    enregistrement += "Montant HT Java;";
    
    // Le résultat de la comparaison des montants HT
    enregistrement += "Comparaison HT;";
    
    // Le prix net TTC du calcul RPG
    enregistrement += "Prix net TTC RPG;";
    
    // Le prix net TTC du calcul Java
    enregistrement += "Prix net TTC Java;";
    
    // Le résultat de la comparaison des montants TTC
    enregistrement += "Comparaison TTC;";
    
    return enregistrement;
  }
  
  /**
   * Générer un enregistrement au format CSV.
   * 
   * @return
   */
  public String genererCsv() {
    String enregistrement = "";
    
    // Date de création
    if (dateCreation != null) {
      enregistrement += new SimpleDateFormat("dd/MM/yyyy").format(dateCreation);
    }
    enregistrement += ';';
    
    // L'identifiant de la ligne de vente
    if (idLigneVente != null) {
      enregistrement += idLigneVente.getTexte();
    }
    enregistrement += ';';
    
    // L'identifiant du client
    if (idClient != null) {
      enregistrement += idClient.getTexte();
    }
    enregistrement += ';';
    
    // L'identifiant de l'article
    if (idArticle != null) {
      enregistrement += idArticle.getTexte();
    }
    enregistrement += ';';
    
    // Le type du document
    if (typeDocument != null) {
      enregistrement += typeDocument;
    }
    enregistrement += ';';
    
    // Le prix base HT du calcul RPG
    if (prixBaseHTRPG != null) {
      enregistrement += prixBaseHTRPG.toPlainString();
    }
    enregistrement += ';';
    
    // Le prix base HT du calcul Java
    if (prixBaseHTJava != null) {
      enregistrement += prixBaseHTJava.toPlainString();
    }
    enregistrement += ';';
    
    // Le prix net saisi HT du calcul RPG
    if (prixNetSaisiHTRPG != null) {
      enregistrement += prixNetSaisiHTRPG.toPlainString();
    }
    enregistrement += ';';
    
    // Le prix net calculé HT du calcul RPG
    if (prixNetCalculeHTRPG != null) {
      enregistrement += prixNetCalculeHTRPG.toPlainString();
    }
    enregistrement += ';';
    
    // Le prix net HT du calcul Java
    if (prixNetHTJava != null) {
      enregistrement += prixNetHTJava.toPlainString();
    }
    enregistrement += ';';
    
    // Le résultat de la comparaison des prix nets
    // Note : si Java trouve null car prix invalide et que RPG trouve 0.00, on considère que c'est ok
    // Java est plus regardant que RPG sur les cas de gratuité
    if (Constantes.equals(prixNetCalculeHTRPG, prixNetHTJava)
        || (prixNetCalculeHTRPG.compareTo(BigDecimal.ZERO) == 0 && prixNetHTJava == null)) {
      enregistrement += "Vrai";
    }
    else {
      enregistrement += "Faux";
    }
    enregistrement += ';';
    
    // Le montant HT du calcul RPG
    if (montantHTRPG != null) {
      enregistrement += montantHTRPG.toPlainString();
    }
    enregistrement += ';';
    
    // Le montant HT du calcul Java
    if (montantHTJava != null) {
      enregistrement += montantHTJava.toPlainString();
    }
    enregistrement += ';';
    
    // Le résultat de la comparaison des montants
    // Note : si Java trouve null car prix invalide et que RPG trouve 0.00, on considère que c'est ok
    // Java est plus regardant que RPG sur les cas de gratuité
    if (Constantes.equals(montantHTRPG, montantHTJava) || (montantHTRPG.compareTo(BigDecimal.ZERO) == 0 && montantHTJava == null)) {
      enregistrement += "Vrai";
    }
    else {
      enregistrement += "Faux";
    }
    enregistrement += ';';
    
    // Le prix net TTC du calcul RPG
    if (prixNetTTCRPG != null) {
      enregistrement += prixNetTTCRPG.toPlainString();
    }
    enregistrement += ';';
    
    // Le prix net TTC du calcul Java
    if (prixNetTTCJava != null) {
      enregistrement += prixNetTTCJava.toPlainString();
    }
    enregistrement += ';';
    
    // Le résultat de la comparaison des prix nets TTC
    // Note : si Java trouve null car prix invalide et que RPG trouve 0.00, on considère que c'est ok
    // Java est plus regardant que RPG sur les cas de gratuité
    if (Constantes.equals(prixNetTTCRPG, prixNetTTCJava) || (prixNetTTCRPG.compareTo(BigDecimal.ZERO) == 0 && prixNetTTCJava == null)) {
      enregistrement += "Vrai";
    }
    else {
      enregistrement += "Faux";
    }
    enregistrement += ';';
    
    return enregistrement;
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier la date de création
   * @param pDateCreation Date de création.
   */
  public void setDateCreation(Date pDateCreation) {
    dateCreation = pDateCreation;
  }
  
  /**
   * Modifier l'identifiant de la ligne de vente.
   * @param pIdLigneVente Identifiant de la ligne de vente.
   */
  public void setIdLigneVente(IdLigneVente pIdLigneVente) {
    idLigneVente = pIdLigneVente;
  }
  
  /**
   * Modifier l'identifiant du client.
   * @param pIdClient Identifiant du client.
   */
  public void setIdClient(IdClient pIdClient) {
    idClient = pIdClient;
  }
  
  /**
   * Modifier l'identifiant de l'article.
   * @param pIdArticle Identifiant article.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  /**
   * Modifier le type du document.
   * @param pTypeDocument
   */
  public void setTypeDocument(String pTypeDocument) {
    typeDocument = pTypeDocument;
  }
  
  /**
   * Modifier le prix de base HT du calcul RPG.
   * @param pPrixBaseHTRPG
   */
  public void setPrixBaseHTRPG(BigDecimal pPrixBaseHTRPG) {
    prixBaseHTRPG = pPrixBaseHTRPG;
  }
  
  /**
   * Modifier le prix de base HT du calcul Java.
   * @param pPrixBaseHTJava
   */
  public void setPrixBaseHTJava(BigDecimal pPrixBaseHTJava) {
    prixBaseHTJava = pPrixBaseHTJava;
  }
  
  /**
   * Modifier le prix net HT du calcul RPG.
   * @param pPrixNetSaisiHTRPG
   */
  public void setPrixNetSaisiHTRPG(BigDecimal pPrixNetSaisiHTRPG) {
    prixNetSaisiHTRPG = pPrixNetSaisiHTRPG;
  }
  
  /**
   * Modifier le prix net HT du calcul Java.
   * @param pPrixNetHTJava
   */
  public void setPrixNetHTJava(BigDecimal pPrixNetHTJava) {
    prixNetHTJava = pPrixNetHTJava;
  }
  
  /**
   * Modifier le prix calculé HT du calcul RPG.
   * @param pPrixNetCalculeHTRPG
   */
  public void setPrixNetCalculeHTRPG(BigDecimal pPrixNetCalculeHTRPG) {
    prixNetCalculeHTRPG = pPrixNetCalculeHTRPG;
  }
  
  /**
   * Modifier la quantité du calcul RPG.
   * @param pQuantiteRPG
   */
  public void setQuantiteRPG(BigDecimal pQuantiteRPG) {
    quantiteRPG = pQuantiteRPG;
  }
  
  /**
   * Modifier la quantité du calcul Java.
   * @param pQuantiteJava
   */
  public void setQuantiteJava(BigDecimal pQuantiteJava) {
    quantiteJava = pQuantiteJava;
  }
  
  /**
   * Modifier le montant HT du calcul RPG.
   * @param pMontantHTRPG
   */
  public void setMontantHTRPG(BigDecimal pMontantHTRPG) {
    montantHTRPG = pMontantHTRPG;
  }
  
  /**
   * Modifier le montant HT du calcul java.
   * @param pMontantHTJava
   */
  public void setMontantHTJava(BigDecimal pMontantHTJava) {
    montantHTJava = pMontantHTJava;
  }
  
  /**
   * Modifier le prix net TTC RPG.
   * @param pPrixNetTTCRPG
   */
  public void setPrixNetTTCRPG(BigDecimal pPrixNetTTCRPG) {
    prixNetTTCRPG = pPrixNetTTCRPG;
  }
  
  /**
   * Modifier le prix net TTC Java.
   * @param pPrixNetTTCJava
   */
  public void setPrixNetTTCJava(BigDecimal pPrixNetTTCJava) {
    prixNetTTCJava = pPrixNetTTCJava;
  }
}
