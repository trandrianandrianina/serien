/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.article;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.lot.CritereLot;
import ri.serien.libcommun.gescom.vente.lot.IdLot;
import ri.serien.libcommun.gescom.vente.lot.ListeLot;
import ri.serien.libcommun.gescom.vente.lot.Lot;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlLot {
  // Constantes
  
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlLot(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  /**
   * Retourner la liste des lots correspondant aux identifiants passés en paramètre
   */
  public ListeLot chargerListeLot(List<IdLot> pListeIdLot) {
    if (pListeIdLot == null) {
      throw new MessageErreurException("La liste des identifiants de lots est invalide");
    }
    
    // Préparation de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql
        .ajouter("SELECT * FROM " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LOT_STOCK + " LEFT JOIN " + querymg.getLibrary()
            + '.' + EnumTableBDD.ARTICLE_LOT_HISTORIQUE_STOCK + " ON SLETB = HLETB AND SLMAG = HLMAG AND SLART = HLART AND SLLOT = HLLOT "
            + " WHERE SLETB = " + RequeteSql.formaterStringSQL(pListeIdLot.get(0).getCodeEtablissement()));
    
    // Ajouter les identifiants des lots à charger à la requête
    int index = 0;
    for (IdLot idLot : pListeIdLot) {
      if (index == 0) {
        requeteSql.ajouter(" and ((");
      }
      else {
        requeteSql.ajouter(" or (");
      }
      requeteSql.ajouter("SLART = "
          + RequeteSql.formaterStringSQL(idLot.getIdArticle().getCodeArticle() + " and SLMAG = " + idLot.getIdMagasin().getCode())
          + " and SLLOT= " + RequeteSql.formaterStringSQL(idLot.getCode()));
      requeteSql.ajouter(")");
      index++;
    }
    requeteSql.ajouter(" )");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequete());
    
    ListeLot listeLot = new ListeLot();
    
    // on charge les données complètes
    for (GenericRecord genericRecord : listeGenericRecord) {
      if (genericRecord != null) {
        listeLot.add(completerLot(genericRecord));
      }
    }
    
    return listeLot;
  }
  
  /**
   * Retourner la liste des lots correspondant aux critères passés en paramètre
   */
  public ListeLot chargerListeLot(CritereLot pCritereLot) {
    if (pCritereLot == null) {
      throw new MessageErreurException("Les critères de recherche de lots sont invalides");
    }
    
    // Préparation de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter(
        "SELECT * FROM " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LOT_STOCK + " LEFT JOIN " + querymg.getLibrary() + '.'
            + EnumTableBDD.ARTICLE_LOT_HISTORIQUE_STOCK + " ON SLETB = HLETB AND SLMAG = HLMAG AND SLART = HLART AND SLLOT = HLLOT ");
    
    if (pCritereLot.getIdLigneVente() != null) {
      requeteSql.ajouterConditionAnd("HLCOD0", "=", pCritereLot.getIdLigneVente().getCodeEntete().getCode());
      requeteSql.ajouterConditionAnd("HLNUM0", "=", pCritereLot.getIdLigneVente().getNumero());
      requeteSql.ajouterConditionAnd("HLSUF0", "=", pCritereLot.getIdLigneVente().getSuffixe());
      requeteSql.ajouterConditionAnd("HLNLI0", "=", pCritereLot.getIdLigneVente().getNumeroLigne());
    }
    
    requeteSql.ajouter(" WHERE ");
    
    if (pCritereLot.getCodeEtablissement() != null) {
      requeteSql.ajouterConditionAnd("SLETB", "=", pCritereLot.getCodeEtablissement());
    }
    
    if (pCritereLot.getIdMagasin() != null) {
      requeteSql.ajouterConditionAnd("SLMAG", "=", pCritereLot.getIdMagasin().getCode());
    }
    
    if (pCritereLot.getIdArticle() != null) {
      requeteSql.ajouterConditionAnd("SLART", "=", pCritereLot.getIdArticle().getCodeArticle());
    }
    
    if (pCritereLot.getRechercheGenerique() != null && !pCritereLot.getRechercheGenerique().getTexteFinal().trim().isEmpty()) {
      requeteSql.ajouterConditionAnd("SLLOT", "like", pCritereLot.getRechercheGenerique().getTexteFinal() + "%");
    }
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequete());
    
    ListeLot listeLot = new ListeLot();
    
    // on charge les données complètes
    for (GenericRecord genericRecord : listeGenericRecord) {
      if (genericRecord != null) {
        listeLot.add(completerLot(genericRecord));
      }
    }
    
    return listeLot;
  }
  
  /**
   * 
   * Charger la liste des lots saisis sur un document (pour les retours).
   * 
   * @param pCritereLot
   * @return
   */
  public ListeLot chargerListeLotDocument(CritereLot pCritereLot) {
    if (pCritereLot == null) {
      throw new MessageErreurException("Les critères de recherche de lots sont invalides");
    }
    
    // Préparation de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter(
        "SELECT * FROM " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LOT_HISTORIQUE_STOCK + " LEFT JOIN " + querymg.getLibrary()
            + '.' + EnumTableBDD.ARTICLE_LOT_STOCK + " ON SLETB = HLETB AND SLMAG = HLMAG AND SLART = HLART AND SLLOT = HLLOT ");
    
    requeteSql.ajouter(" WHERE ");
    
    if (pCritereLot.getIdDocumentOrigine() != null) {
      requeteSql.ajouterConditionAnd("HLCOD0", "=", pCritereLot.getIdDocumentOrigine().getCodeEntete().getCode());
      requeteSql.ajouterConditionAnd("HLNUM0", "=", pCritereLot.getIdDocumentOrigine().getNumero());
      requeteSql.ajouterConditionAnd("HLSUF0", "=", pCritereLot.getIdDocumentOrigine().getSuffixe());
      if (pCritereLot.getIdLigneVente() != null) {
        requeteSql.ajouterConditionAnd("HLNLI0", "=", pCritereLot.getIdLigneVente().getNumeroLigne());
      }
    }
    else if (pCritereLot.getIdLigneVente() != null) {
      requeteSql.ajouterConditionAnd("HLCOD0", "=", pCritereLot.getIdLigneVente().getCodeEntete().getCode());
      requeteSql.ajouterConditionAnd("HLNUM0", "=", pCritereLot.getIdLigneVente().getNumero());
      requeteSql.ajouterConditionAnd("HLSUF0", "=", pCritereLot.getIdLigneVente().getSuffixe());
      requeteSql.ajouterConditionAnd("HLNLI0", "=", pCritereLot.getIdLigneVente().getNumeroLigne());
    }
    
    if (pCritereLot.getCodeEtablissement() != null) {
      requeteSql.ajouterConditionAnd("HLETB", "=", pCritereLot.getCodeEtablissement());
    }
    
    if (pCritereLot.getIdMagasin() != null) {
      requeteSql.ajouterConditionAnd("HLMAG", "=", pCritereLot.getIdMagasin().getCode());
    }
    
    if (pCritereLot.getIdArticle() != null) {
      requeteSql.ajouterConditionAnd("HLART", "=", pCritereLot.getIdArticle().getCodeArticle());
    }
    
    if (pCritereLot.getRechercheGenerique() != null && !pCritereLot.getRechercheGenerique().getTexteFinal().trim().isEmpty()) {
      requeteSql.ajouterConditionAnd("HLLOT", "like", pCritereLot.getRechercheGenerique().getTexteFinal() + "%");
    }
    
    requeteSql.ajouterConditionAnd("HLQTE", "<>", 0);
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequete());
    
    ListeLot listeLot = new ListeLot();
    
    // on charge les données complètes
    for (GenericRecord genericRecord : listeGenericRecord) {
      if (genericRecord != null) {
        listeLot.add(completerLot(genericRecord));
      }
    }
    
    return listeLot;
  }
  
  /**
   * Compléter un identifiant de lot à partir des données lues en base
   */
  private IdLot completerIdLot(GenericRecord record) {
    IdLot idLot = null;
    IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("SLETB", "", false));
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, record.getStringValue("SLART"));
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, record.getStringValue("SLMAG"));
    idLot = IdLot.getInstance(idEtablissement, idArticle, idMagasin, record.getStringValue("SLLOT"));
    
    return idLot;
  }
  
  /**
   * Compléter un lot à partir des champs de la base de données.
   */
  private Lot completerLot(GenericRecord record) {
    IdLot idLot = completerIdLot(record);
    Lot lot = new Lot(idLot);
    lot.setStockEntree(record.getBigDecimalValue("SLQTE"));
    lot.setStockSortie(record.getBigDecimalValue("SLQTS"));
    lot.setStockReserve(record.getBigDecimalValue("SLQTR"));
    lot.setDatePeremption(record.getDateValue("SLDPE"));
    lot.setMontantAchat(record.getBigDecimalValue("SLMTA"));
    lot.setMontantVente(record.getBigDecimalValue("SLMTV"));
    lot.setStockEntreeExercice2(record.getBigDecimalValue("SLQPE"));
    lot.setStockSortieExercice2(record.getBigDecimalValue("SLQPS"));
    lot.setStockReserveExercice2(record.getBigDecimalValue("SLQPR"));
    lot.setStockDisponible(record.getBigDecimalValue("SLSDI"));
    lot.setDateFabrication(record.getDateValue("SLDT1"));
    lot.setNumeroPalette(record.getIntegerValue("SLNPA", 0));
    if (record.getBigDecimalValue("HLQTE") != null) {
      lot.setQuantitePrecedentSaisi(record.getBigDecimalValue("HLQTE"));
    }
    else {
      lot.setQuantitePrecedentSaisi(BigDecimal.ZERO);
    }
    if (!record.getStringValue("SLUNL").trim().isEmpty()) {
      IdUnite idUniteConditionnement = IdUnite.getInstance(record.getStringValue("SLUNL"));
      lot.setIdUniteConditionnement(idUniteConditionnement);
    }
    
    return lot;
  }
  
}
