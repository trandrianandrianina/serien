/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2;

public class JsonStockArticleV2 {
  private String codeArticle = null;
  private String stockDispo = null;
  private String stockAttendu = null;
  private String dateReassort = null;
  
  /**
   * Stock Série N au format Magento
   * Cette classe sert à l'intégration JAVA vers JSON
   * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS
   */
  public JsonStockArticleV2() {
    
  }
  
  public String getCodeArticle() {
    return codeArticle;
  }
  
  public void setCodeArticle(String codeArticle) {
    this.codeArticle = codeArticle;
  }
  
  public String getStockDispo() {
    return stockDispo;
  }
  
  public void setStockDispo(String stockDispo) {
    this.stockDispo = stockDispo;
  }
  
  public String getDateReassort() {
    return dateReassort;
  }
  
  public void setDateReassort(String dateReassort) {
    this.dateReassort = dateReassort;
  }
  
  public String getStockAttendu() {
    return stockAttendu;
  }
  
  public void setStockAttendu(String stockAttendu) {
    this.stockAttendu = stockAttendu;
  }
}
