/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.spm.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class PsemdinmManager extends QueryManager {
  /**
   * Constructeur
   * @param database
   */
  public PsemdinmManager(Connection database) {
    super(database);
  }
  
  /**
   * Retourne la liste des modules (pour les menus)
   * @param bibenv
   * @param prf
   * @return
   * 
   */
  public ArrayList<GenericRecord> getTousEnregistrements() {
    // On sélectionne tous les groupes sauf LTM (01268)
    return select("Select * from fm500.psemdinm ");
  }
  
}
