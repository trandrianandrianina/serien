/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.negociation;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvx0009o extends ProgramParameter {
  // Constantes
  public static final int SIZE_NATOP = 1;
  public static final int DECIMAL_NATOP = 0;
  public static final int SIZE_NACRE = 7;
  public static final int DECIMAL_NACRE = 0;
  public static final int SIZE_NAMOD = 7;
  public static final int DECIMAL_NAMOD = 0;
  public static final int SIZE_NATRT = 7;
  public static final int DECIMAL_NATRT = 0;
  public static final int SIZE_NATYP = 1;
  public static final int SIZE_NACOD = 1;
  public static final int SIZE_NAETB = 3;
  public static final int SIZE_NANUM = 6;
  public static final int DECIMAL_NANUM = 0;
  public static final int SIZE_NASUF = 1;
  public static final int DECIMAL_NASUF = 0;
  public static final int SIZE_NANLI = 4;
  public static final int DECIMAL_NANLI = 0;
  public static final int SIZE_NAART = 20;
  public static final int SIZE_NACOL = 1;
  public static final int DECIMAL_NACOL = 0;
  public static final int SIZE_NAFRS = 6;
  public static final int DECIMAL_NAFRS = 0;
  public static final int SIZE_NADAP = 7;
  public static final int DECIMAL_NADAP = 0;
  public static final int SIZE_NACPV = 1;
  public static final int SIZE_NAREM1 = 4;
  public static final int DECIMAL_NAREM1 = 2;
  public static final int SIZE_NAREM2 = 4;
  public static final int DECIMAL_NAREM2 = 2;
  public static final int SIZE_NAREM3 = 4;
  public static final int DECIMAL_NAREM3 = 2;
  public static final int SIZE_NAREM4 = 4;
  public static final int DECIMAL_NAREM4 = 2;
  public static final int SIZE_NAREM5 = 4;
  public static final int DECIMAL_NAREM5 = 2;
  public static final int SIZE_NAREM6 = 4;
  public static final int DECIMAL_NAREM6 = 2;
  public static final int SIZE_NAPRA = 11;
  public static final int DECIMAL_NAPRA = 2;
  public static final int SIZE_NAUNA = 2;
  public static final int SIZE_NANUA = 8;
  public static final int DECIMAL_NANUA = 3;
  public static final int SIZE_NAKAC = 8;
  public static final int DECIMAL_NAKAC = 3;
  public static final int SIZE_NADEL = 2;
  public static final int DECIMAL_NADEL = 0;
  public static final int SIZE_NAREF = 20;
  public static final int SIZE_NAUNC = 2;
  public static final int SIZE_NAKSC = 8;
  public static final int DECIMAL_NAKSC = 3;
  public static final int SIZE_NADCC = 1;
  public static final int DECIMAL_NADCC = 0;
  public static final int SIZE_NADCA = 1;
  public static final int DECIMAL_NADCA = 0;
  public static final int SIZE_NADEV = 3;
  public static final int SIZE_NAQMI = 10;
  public static final int DECIMAL_NAQMI = 3;
  public static final int SIZE_NAQEC = 10;
  public static final int DECIMAL_NAQEC = 3;
  public static final int SIZE_NAKPV = 4;
  public static final int DECIMAL_NAKPV = 3;
  public static final int SIZE_NADDP = 7;
  public static final int DECIMAL_NADDP = 0;
  public static final int SIZE_NARGA = 10;
  public static final int SIZE_NADELS = 2;
  public static final int DECIMAL_NADELS = 0;
  public static final int SIZE_NAKPR = 5;
  public static final int DECIMAL_NAKPR = 4;
  public static final int SIZE_NAIN1 = 1;
  public static final int SIZE_NAIN2 = 1;
  public static final int SIZE_NAIN3 = 1;
  public static final int SIZE_NAIN4 = 1;
  public static final int SIZE_NAIN5 = 1;
  public static final int SIZE_NATRL = 1;
  public static final int SIZE_NARFC = 20;
  public static final int SIZE_NAGCD = 13;
  public static final int DECIMAL_NAGCD = 0;
  public static final int SIZE_NADAF = 7;
  public static final int DECIMAL_NADAF = 0;
  public static final int SIZE_NALIB = 30;
  public static final int SIZE_NAOPA = 3;
  public static final int SIZE_NAIN6 = 1;
  public static final int SIZE_NAIN7 = 1;
  public static final int SIZE_NAIN8 = 1;
  public static final int SIZE_NAIN9 = 1;
  public static final int SIZE_NAPDI = 11;
  public static final int DECIMAL_NAPDI = 2;
  public static final int SIZE_NAKAP = 5;
  public static final int DECIMAL_NAKAP = 4;
  public static final int SIZE_NAPRS = 9;
  public static final int DECIMAL_NAPRS = 2;
  public static final int SIZE_NAPGN = 1;
  public static final int SIZE_NAFV1 = 9;
  public static final int DECIMAL_NAFV1 = 2;
  public static final int SIZE_NAFK1 = 7;
  public static final int DECIMAL_NAFK1 = 4;
  public static final int SIZE_NAFP1 = 4;
  public static final int DECIMAL_NAFP1 = 2;
  public static final int SIZE_NAFU1 = 1;
  public static final int SIZE_NAFV2 = 9;
  public static final int DECIMAL_NAFV2 = 2;
  public static final int SIZE_NAFK2 = 7;
  public static final int DECIMAL_NAFK2 = 4;
  public static final int SIZE_NAFP2 = 4;
  public static final int DECIMAL_NAFP2 = 2;
  public static final int SIZE_NAFU2 = 1;
  public static final int SIZE_NAFV3 = 9;
  public static final int DECIMAL_NAFV3 = 2;
  public static final int SIZE_NAFK3 = 7;
  public static final int DECIMAL_NAFK3 = 4;
  public static final int SIZE_NAFU3 = 1;
  public static final int SIZE_NAPR1 = 11;
  public static final int DECIMAL_NAPR1 = 2;
  public static final int SIZE_NAPR2 = 11;
  public static final int DECIMAL_NAPR2 = 2;
  public static final int SIZE_NADAT1 = 7;
  public static final int DECIMAL_NADAT1 = 0;
  public static final int SIZE_NADAT2 = 7;
  public static final int DECIMAL_NADAT2 = 0;
  public static final int SIZE_NAARR = 1;
  public static final int SIZE_TOTALE_DS = 416;
  
  // Constantes indices Nom DS
  public static final int VAR_NATOP = 0;
  public static final int VAR_NACRE = 1;
  public static final int VAR_NAMOD = 2;
  public static final int VAR_NATRT = 3;
  public static final int VAR_NATYP = 4;
  public static final int VAR_NACOD = 5;
  public static final int VAR_NAETB = 6;
  public static final int VAR_NANUM = 7;
  public static final int VAR_NASUF = 8;
  public static final int VAR_NANLI = 9;
  public static final int VAR_NAART = 10;
  public static final int VAR_NACOL = 11;
  public static final int VAR_NAFRS = 12;
  public static final int VAR_NADAP = 13;
  public static final int VAR_NACPV = 14;
  public static final int VAR_NAREM1 = 15;
  public static final int VAR_NAREM2 = 16;
  public static final int VAR_NAREM3 = 17;
  public static final int VAR_NAREM4 = 18;
  public static final int VAR_NAREM5 = 19;
  public static final int VAR_NAREM6 = 20;
  public static final int VAR_NAPRA = 21;
  public static final int VAR_NAUNA = 22;
  public static final int VAR_NANUA = 23;
  public static final int VAR_NAKAC = 24;
  public static final int VAR_NADEL = 25;
  public static final int VAR_NAREF = 26;
  public static final int VAR_NAUNC = 27;
  public static final int VAR_NAKSC = 28;
  public static final int VAR_NADCC = 29;
  public static final int VAR_NADCA = 30;
  public static final int VAR_NADEV = 31;
  public static final int VAR_NAQMI = 32;
  public static final int VAR_NAQEC = 33;
  public static final int VAR_NAKPV = 34;
  public static final int VAR_NADDP = 35;
  public static final int VAR_NARGA = 36;
  public static final int VAR_NADELS = 37;
  public static final int VAR_NAKPR = 38;
  public static final int VAR_NAIN1 = 39;
  public static final int VAR_NAIN2 = 40;
  public static final int VAR_NAIN3 = 41;
  public static final int VAR_NAIN4 = 42;
  public static final int VAR_NAIN5 = 43;
  public static final int VAR_NATRL = 44;
  public static final int VAR_NARFC = 45;
  public static final int VAR_NAGCD = 46;
  public static final int VAR_NADAF = 47;
  public static final int VAR_NALIB = 48;
  public static final int VAR_NAOPA = 49;
  public static final int VAR_NAIN6 = 50;
  public static final int VAR_NAIN7 = 51;
  public static final int VAR_NAIN8 = 52;
  public static final int VAR_NAIN9 = 53;
  public static final int VAR_NAPDI = 54;
  public static final int VAR_NAKAP = 55;
  public static final int VAR_NAPRS = 56;
  public static final int VAR_NAPGN = 57;
  public static final int VAR_NAFV1 = 58;
  public static final int VAR_NAFK1 = 59;
  public static final int VAR_NAFP1 = 60;
  public static final int VAR_NAFU1 = 61;
  public static final int VAR_NAFV2 = 62;
  public static final int VAR_NAFK2 = 63;
  public static final int VAR_NAFP2 = 64;
  public static final int VAR_NAFU2 = 65;
  public static final int VAR_NAFV3 = 66;
  public static final int VAR_NAFK3 = 67;
  public static final int VAR_NAFU3 = 68;
  public static final int VAR_NAPR1 = 69;
  public static final int VAR_NAPR2 = 70;
  public static final int VAR_NADAT1 = 71;
  public static final int VAR_NADAT2 = 72;
  public static final int VAR_NAARR = 73;
  
  // Variables AS400
  private BigDecimal natop = BigDecimal.ZERO; // Top système
  private BigDecimal nacre = BigDecimal.ZERO; // Date de création
  private BigDecimal namod = BigDecimal.ZERO; // Date de modification
  private BigDecimal natrt = BigDecimal.ZERO; // Date de traitement
  private String natyp = ""; // "A" pour achat, "V" vente
  private String nacod = ""; // Code ERL "E"
  private String naetb = ""; // Code Etablissement
  private BigDecimal nanum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal nasuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal nanli = BigDecimal.ZERO; // Numéro de Ligne
  private String naart = ""; // Code article
  private BigDecimal nacol = BigDecimal.ZERO; // Collectif Fournisseur
  private BigDecimal nafrs = BigDecimal.ZERO; // Numéro Fournisseur
  private BigDecimal nadap = BigDecimal.ZERO; // Date application
  private String nacpv = ""; // Top calcul prix de vente
  private BigDecimal narem1 = BigDecimal.ZERO; // Remise 1
  private BigDecimal narem2 = BigDecimal.ZERO; // Remise 2
  private BigDecimal narem3 = BigDecimal.ZERO; // Remise 3
  private BigDecimal narem4 = BigDecimal.ZERO; // Remise 4
  private BigDecimal narem5 = BigDecimal.ZERO; // Remise 5
  private BigDecimal narem6 = BigDecimal.ZERO; // Remise 6
  private BigDecimal napra = BigDecimal.ZERO; // Prix catalogue
  private String nauna = ""; // Unité achat
  private BigDecimal nanua = BigDecimal.ZERO; // Conditionnement
  private BigDecimal nakac = BigDecimal.ZERO; // Nombre unité achat/unité Cde
  private BigDecimal nadel = BigDecimal.ZERO; // Délai de livraison
  private String naref = ""; // Référence fournisseur
  private String naunc = ""; // Unité de commande
  private BigDecimal naksc = BigDecimal.ZERO; // Nombre unité Stk/unité Cde
  private BigDecimal nadcc = BigDecimal.ZERO; // Zone système
  private BigDecimal nadca = BigDecimal.ZERO; // Zone système
  private String nadev = ""; // Devise
  private BigDecimal naqmi = BigDecimal.ZERO; // Quantité minimum de commande
  private BigDecimal naqec = BigDecimal.ZERO; // Quantité économique
  private BigDecimal nakpv = BigDecimal.ZERO; // Coeff. calcul prix de vente
  private BigDecimal naddp = BigDecimal.ZERO; // Date de dernier prix
  private String narga = ""; // Regroupement Achat
  private BigDecimal nadels = BigDecimal.ZERO; // Délai supplémentaire
  private BigDecimal nakpr = BigDecimal.ZERO; // Coeff. calcul prix
  private String nain1 = ""; // P=Promo, C=Colonnes
  private String nain2 = ""; // Note qualité de 0 à 9
  private String nain3 = ""; // G = Fournisseur pour G.B.A
  private String nain4 = ""; // 3 remises pour calcul PV min
  private String nain5 = ""; // Ind. non utilisé
  private String natrl = ""; // Type remise ligne, A=Ajout
  private String narfc = ""; // Référence constructeur
  private BigDecimal nagcd = BigDecimal.ZERO; // Code gencod frs
  private BigDecimal nadaf = BigDecimal.ZERO; // Date limite validité
  private String nalib = ""; // Libellé
  private String naopa = ""; // origine
  private String nain6 = ""; // Ind. non utilisé
  private String nain7 = ""; // Delai en Jours et Non Semain
  private String nain8 = ""; // Ind. non utilisé
  private String nain9 = ""; // Ind. non utilisé
  private BigDecimal napdi = BigDecimal.ZERO; // Prix distributeur
  private BigDecimal nakap = BigDecimal.ZERO; // Coeff. approche fixe
  private BigDecimal naprs = BigDecimal.ZERO; // Prix de revient standard
  private String napgn = ""; // Top prix négocié
  private BigDecimal nafv1 = BigDecimal.ZERO; // Frais 1 valeur
  private BigDecimal nafk1 = BigDecimal.ZERO; // Frais 1 coefficient
  private BigDecimal nafp1 = BigDecimal.ZERO; // Frais 1 % de port
  private String nafu1 = ""; // Unité frais 1
  private BigDecimal nafv2 = BigDecimal.ZERO; // Frais 2 valeur
  private BigDecimal nafk2 = BigDecimal.ZERO; // Frais 2 coefficient
  private BigDecimal nafp2 = BigDecimal.ZERO; // Frais 2 % MAJORATION
  private String nafu2 = ""; // Unité frais 2
  private BigDecimal nafv3 = BigDecimal.ZERO; // Frais 3 valeur
  private BigDecimal nafk3 = BigDecimal.ZERO; // Frais 3 coefficient
  private String nafu3 = ""; // Unité frais 3
  private BigDecimal napr1 = BigDecimal.ZERO; // Zone non utilisée
  private BigDecimal napr2 = BigDecimal.ZERO; // Zone non utilisée
  private BigDecimal nadat1 = BigDecimal.ZERO; // Zone date non utilisée
  private BigDecimal nadat2 = BigDecimal.ZERO; // Zone date non utilisée
  private String naarr = ""; // Type négo : N C ou c
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400ZonedDecimal(SIZE_NATOP, DECIMAL_NATOP), // Top système
      new AS400PackedDecimal(SIZE_NACRE, DECIMAL_NACRE), // Date de création
      new AS400PackedDecimal(SIZE_NAMOD, DECIMAL_NAMOD), // Date de modification
      new AS400PackedDecimal(SIZE_NATRT, DECIMAL_NATRT), // Date de traitement
      new AS400Text(SIZE_NATYP), // "A" pour achat, "V" vente
      new AS400Text(SIZE_NACOD), // Code ERL "E"
      new AS400Text(SIZE_NAETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_NANUM, DECIMAL_NANUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_NASUF, DECIMAL_NASUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_NANLI, DECIMAL_NANLI), // Numéro de Ligne
      new AS400Text(SIZE_NAART), // Code article
      new AS400ZonedDecimal(SIZE_NACOL, DECIMAL_NACOL), // Collectif Fournisseur
      new AS400ZonedDecimal(SIZE_NAFRS, DECIMAL_NAFRS), // Numéro Fournisseur
      new AS400ZonedDecimal(SIZE_NADAP, DECIMAL_NADAP), // Date application
      new AS400Text(SIZE_NACPV), // Top calcul prix de vente
      new AS400ZonedDecimal(SIZE_NAREM1, DECIMAL_NAREM1), // Remise 1
      new AS400ZonedDecimal(SIZE_NAREM2, DECIMAL_NAREM2), // Remise 2
      new AS400ZonedDecimal(SIZE_NAREM3, DECIMAL_NAREM3), // Remise 3
      new AS400ZonedDecimal(SIZE_NAREM4, DECIMAL_NAREM4), // Remise 4
      new AS400ZonedDecimal(SIZE_NAREM5, DECIMAL_NAREM5), // Remise 5
      new AS400ZonedDecimal(SIZE_NAREM6, DECIMAL_NAREM6), // Remise 6
      new AS400PackedDecimal(SIZE_NAPRA, DECIMAL_NAPRA), // Prix catalogue
      new AS400Text(SIZE_NAUNA), // Unité achat
      new AS400PackedDecimal(SIZE_NANUA, DECIMAL_NANUA), // Conditionnement
      new AS400PackedDecimal(SIZE_NAKAC, DECIMAL_NAKAC), // Nombre unité achat/unité Cde
      new AS400ZonedDecimal(SIZE_NADEL, DECIMAL_NADEL), // Délai de livraison
      new AS400Text(SIZE_NAREF), // Référence fournisseur
      new AS400Text(SIZE_NAUNC), // Unité de commande
      new AS400PackedDecimal(SIZE_NAKSC, DECIMAL_NAKSC), // Nombre unité Stk/unité Cde
      new AS400ZonedDecimal(SIZE_NADCC, DECIMAL_NADCC), // Zone système
      new AS400ZonedDecimal(SIZE_NADCA, DECIMAL_NADCA), // Zone système
      new AS400Text(SIZE_NADEV), // Devise
      new AS400PackedDecimal(SIZE_NAQMI, DECIMAL_NAQMI), // Quantité minimum de commande
      new AS400PackedDecimal(SIZE_NAQEC, DECIMAL_NAQEC), // Quantité économique
      new AS400ZonedDecimal(SIZE_NAKPV, DECIMAL_NAKPV), // Coeff. calcul prix de vente
      new AS400PackedDecimal(SIZE_NADDP, DECIMAL_NADDP), // Date de dernier prix
      new AS400Text(SIZE_NARGA), // Regroupement Achat
      new AS400ZonedDecimal(SIZE_NADELS, DECIMAL_NADELS), // Délai supplémentaire
      new AS400PackedDecimal(SIZE_NAKPR, DECIMAL_NAKPR), // Coeff. calcul prix
      new AS400Text(SIZE_NAIN1), // P=Promo, C=Colonnes
      new AS400Text(SIZE_NAIN2), // Note qualité de 0 à 9
      new AS400Text(SIZE_NAIN3), // G = Fournisseur pour G.B.A
      new AS400Text(SIZE_NAIN4), // 3 remises pour calcul PV min
      new AS400Text(SIZE_NAIN5), // Ind. non utilisé
      new AS400Text(SIZE_NATRL), // Type remise ligne, A=Ajout
      new AS400Text(SIZE_NARFC), // Référence constructeur
      new AS400ZonedDecimal(SIZE_NAGCD, DECIMAL_NAGCD), // Code gencod frs
      new AS400ZonedDecimal(SIZE_NADAF, DECIMAL_NADAF), // Date limite validité
      new AS400Text(SIZE_NALIB), // Libellé
      new AS400Text(SIZE_NAOPA), // origine
      new AS400Text(SIZE_NAIN6), // Ind. non utilisé
      new AS400Text(SIZE_NAIN7), // Delai en Jours et Non Semain
      new AS400Text(SIZE_NAIN8), // Ind. non utilisé
      new AS400Text(SIZE_NAIN9), // Ind. non utilisé
      new AS400PackedDecimal(SIZE_NAPDI, DECIMAL_NAPDI), // Prix distributeur
      new AS400ZonedDecimal(SIZE_NAKAP, DECIMAL_NAKAP), // Coeff. approche fixe
      new AS400PackedDecimal(SIZE_NAPRS, DECIMAL_NAPRS), // Prix de revient standard
      new AS400Text(SIZE_NAPGN), // Top prix négocié
      new AS400PackedDecimal(SIZE_NAFV1, DECIMAL_NAFV1), // Frais 1 valeur
      new AS400PackedDecimal(SIZE_NAFK1, DECIMAL_NAFK1), // Frais 1 coefficient
      new AS400ZonedDecimal(SIZE_NAFP1, DECIMAL_NAFP1), // Frais 1 % de port
      new AS400Text(SIZE_NAFU1), // Unité frais 1
      new AS400PackedDecimal(SIZE_NAFV2, DECIMAL_NAFV2), // Frais 2 valeur
      new AS400PackedDecimal(SIZE_NAFK2, DECIMAL_NAFK2), // Frais 2 coefficient
      new AS400PackedDecimal(SIZE_NAFP2, DECIMAL_NAFP2), // Frais 2 % MAJORATION
      new AS400Text(SIZE_NAFU2), // Unité frais 2
      new AS400PackedDecimal(SIZE_NAFV3, DECIMAL_NAFV3), // Frais 3 valeur
      new AS400PackedDecimal(SIZE_NAFK3, DECIMAL_NAFK3), // Frais 3 coefficient
      new AS400Text(SIZE_NAFU3), // Unité frais 3
      new AS400PackedDecimal(SIZE_NAPR1, DECIMAL_NAPR1), // Zone non utilisée
      new AS400PackedDecimal(SIZE_NAPR2, DECIMAL_NAPR2), // Zone non utilisée
      new AS400PackedDecimal(SIZE_NADAT1, DECIMAL_NADAT1), // Zone date non utilisée
      new AS400PackedDecimal(SIZE_NADAT2, DECIMAL_NADAT2), // Zone date non utilisée
      new AS400Text(SIZE_NAARR), // Type négo : N C ou c
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { natop, nacre, namod, natrt, natyp, nacod, naetb, nanum, nasuf, nanli, naart, nacol, nafrs, nadap, nacpv, narem1,
          narem2, narem3, narem4, narem5, narem6, napra, nauna, nanua, nakac, nadel, naref, naunc, naksc, nadcc, nadca, nadev, naqmi,
          naqec, nakpv, naddp, narga, nadels, nakpr, nain1, nain2, nain3, nain4, nain5, natrl, narfc, nagcd, nadaf, nalib, naopa, nain6,
          nain7, nain8, nain9, napdi, nakap, naprs, napgn, nafv1, nafk1, nafp1, nafu1, nafv2, nafk2, nafp2, nafu2, nafv3, nafk3, nafu3,
          napr1, napr2, nadat1, nadat2, naarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    natop = (BigDecimal) output[0];
    nacre = (BigDecimal) output[1];
    namod = (BigDecimal) output[2];
    natrt = (BigDecimal) output[3];
    natyp = (String) output[4];
    nacod = (String) output[5];
    naetb = (String) output[6];
    nanum = (BigDecimal) output[7];
    nasuf = (BigDecimal) output[8];
    nanli = (BigDecimal) output[9];
    naart = (String) output[10];
    nacol = (BigDecimal) output[11];
    nafrs = (BigDecimal) output[12];
    nadap = (BigDecimal) output[13];
    nacpv = (String) output[14];
    narem1 = (BigDecimal) output[15];
    narem2 = (BigDecimal) output[16];
    narem3 = (BigDecimal) output[17];
    narem4 = (BigDecimal) output[18];
    narem5 = (BigDecimal) output[19];
    narem6 = (BigDecimal) output[20];
    napra = (BigDecimal) output[21];
    nauna = (String) output[22];
    nanua = (BigDecimal) output[23];
    nakac = (BigDecimal) output[24];
    nadel = (BigDecimal) output[25];
    naref = (String) output[26];
    naunc = (String) output[27];
    naksc = (BigDecimal) output[28];
    nadcc = (BigDecimal) output[29];
    nadca = (BigDecimal) output[30];
    nadev = (String) output[31];
    naqmi = (BigDecimal) output[32];
    naqec = (BigDecimal) output[33];
    nakpv = (BigDecimal) output[34];
    naddp = (BigDecimal) output[35];
    narga = (String) output[36];
    nadels = (BigDecimal) output[37];
    nakpr = (BigDecimal) output[38];
    nain1 = (String) output[39];
    nain2 = (String) output[40];
    nain3 = (String) output[41];
    nain4 = (String) output[42];
    nain5 = (String) output[43];
    natrl = (String) output[44];
    narfc = (String) output[45];
    nagcd = (BigDecimal) output[46];
    nadaf = (BigDecimal) output[47];
    nalib = (String) output[48];
    naopa = (String) output[49];
    nain6 = (String) output[50];
    nain7 = (String) output[51];
    nain8 = (String) output[52];
    nain9 = (String) output[53];
    napdi = (BigDecimal) output[54];
    nakap = (BigDecimal) output[55];
    naprs = (BigDecimal) output[56];
    napgn = (String) output[57];
    nafv1 = (BigDecimal) output[58];
    nafk1 = (BigDecimal) output[59];
    nafp1 = (BigDecimal) output[60];
    nafu1 = (String) output[61];
    nafv2 = (BigDecimal) output[62];
    nafk2 = (BigDecimal) output[63];
    nafp2 = (BigDecimal) output[64];
    nafu2 = (String) output[65];
    nafv3 = (BigDecimal) output[66];
    nafk3 = (BigDecimal) output[67];
    nafu3 = (String) output[68];
    napr1 = (BigDecimal) output[69];
    napr2 = (BigDecimal) output[70];
    nadat1 = (BigDecimal) output[71];
    nadat2 = (BigDecimal) output[72];
    naarr = (String) output[73];
  }
  
  // -- Accesseurs
  
  public void setNatop(BigDecimal pNatop) {
    if (pNatop == null) {
      return;
    }
    natop = pNatop.setScale(DECIMAL_NATOP, RoundingMode.HALF_UP);
  }
  
  public void setNatop(Integer pNatop) {
    if (pNatop == null) {
      return;
    }
    natop = BigDecimal.valueOf(pNatop);
  }
  
  public Integer getNatop() {
    return natop.intValue();
  }
  
  public void setNacre(BigDecimal pNacre) {
    if (pNacre == null) {
      return;
    }
    nacre = pNacre.setScale(DECIMAL_NACRE, RoundingMode.HALF_UP);
  }
  
  public void setNacre(Integer pNacre) {
    if (pNacre == null) {
      return;
    }
    nacre = BigDecimal.valueOf(pNacre);
  }
  
  public void setNacre(Date pNacre) {
    if (pNacre == null) {
      return;
    }
    nacre = BigDecimal.valueOf(ConvertDate.dateToDb2(pNacre));
  }
  
  public Integer getNacre() {
    return nacre.intValue();
  }
  
  public Date getNacreConvertiEnDate() {
    return ConvertDate.db2ToDate(nacre.intValue(), null);
  }
  
  public void setNamod(BigDecimal pNamod) {
    if (pNamod == null) {
      return;
    }
    namod = pNamod.setScale(DECIMAL_NAMOD, RoundingMode.HALF_UP);
  }
  
  public void setNamod(Integer pNamod) {
    if (pNamod == null) {
      return;
    }
    namod = BigDecimal.valueOf(pNamod);
  }
  
  public void setNamod(Date pNamod) {
    if (pNamod == null) {
      return;
    }
    namod = BigDecimal.valueOf(ConvertDate.dateToDb2(pNamod));
  }
  
  public Integer getNamod() {
    return namod.intValue();
  }
  
  public Date getNamodConvertiEnDate() {
    return ConvertDate.db2ToDate(namod.intValue(), null);
  }
  
  public void setNatrt(BigDecimal pNatrt) {
    if (pNatrt == null) {
      return;
    }
    natrt = pNatrt.setScale(DECIMAL_NATRT, RoundingMode.HALF_UP);
  }
  
  public void setNatrt(Integer pNatrt) {
    if (pNatrt == null) {
      return;
    }
    natrt = BigDecimal.valueOf(pNatrt);
  }
  
  public void setNatrt(Date pNatrt) {
    if (pNatrt == null) {
      return;
    }
    natrt = BigDecimal.valueOf(ConvertDate.dateToDb2(pNatrt));
  }
  
  public Integer getNatrt() {
    return natrt.intValue();
  }
  
  public Date getNatrtConvertiEnDate() {
    return ConvertDate.db2ToDate(natrt.intValue(), null);
  }
  
  public void setNatyp(Character pNatyp) {
    if (pNatyp == null) {
      return;
    }
    natyp = String.valueOf(pNatyp);
  }
  
  public Character getNatyp() {
    return natyp.charAt(0);
  }
  
  public void setNacod(Character pNacod) {
    if (pNacod == null) {
      return;
    }
    nacod = String.valueOf(pNacod);
  }
  
  public Character getNacod() {
    return nacod.charAt(0);
  }
  
  public void setNaetb(String pNaetb) {
    if (pNaetb == null) {
      return;
    }
    naetb = pNaetb;
  }
  
  public String getNaetb() {
    return naetb;
  }
  
  public void setNanum(BigDecimal pNanum) {
    if (pNanum == null) {
      return;
    }
    nanum = pNanum.setScale(DECIMAL_NANUM, RoundingMode.HALF_UP);
  }
  
  public void setNanum(Integer pNanum) {
    if (pNanum == null) {
      return;
    }
    nanum = BigDecimal.valueOf(pNanum);
  }
  
  public Integer getNanum() {
    return nanum.intValue();
  }
  
  public void setNasuf(BigDecimal pNasuf) {
    if (pNasuf == null) {
      return;
    }
    nasuf = pNasuf.setScale(DECIMAL_NASUF, RoundingMode.HALF_UP);
  }
  
  public void setNasuf(Integer pNasuf) {
    if (pNasuf == null) {
      return;
    }
    nasuf = BigDecimal.valueOf(pNasuf);
  }
  
  public Integer getNasuf() {
    return nasuf.intValue();
  }
  
  public void setNanli(BigDecimal pNanli) {
    if (pNanli == null) {
      return;
    }
    nanli = pNanli.setScale(DECIMAL_NANLI, RoundingMode.HALF_UP);
  }
  
  public void setNanli(Integer pNanli) {
    if (pNanli == null) {
      return;
    }
    nanli = BigDecimal.valueOf(pNanli);
  }
  
  public Integer getNanli() {
    return nanli.intValue();
  }
  
  public void setNaart(String pNaart) {
    if (pNaart == null) {
      return;
    }
    naart = pNaart;
  }
  
  public String getNaart() {
    return naart;
  }
  
  public void setNacol(BigDecimal pNacol) {
    if (pNacol == null) {
      return;
    }
    nacol = pNacol.setScale(DECIMAL_NACOL, RoundingMode.HALF_UP);
  }
  
  public void setNacol(Integer pNacol) {
    if (pNacol == null) {
      return;
    }
    nacol = BigDecimal.valueOf(pNacol);
  }
  
  public Integer getNacol() {
    return nacol.intValue();
  }
  
  public void setNafrs(BigDecimal pNafrs) {
    if (pNafrs == null) {
      return;
    }
    nafrs = pNafrs.setScale(DECIMAL_NAFRS, RoundingMode.HALF_UP);
  }
  
  public void setNafrs(Integer pNafrs) {
    if (pNafrs == null) {
      return;
    }
    nafrs = BigDecimal.valueOf(pNafrs);
  }
  
  public Integer getNafrs() {
    return nafrs.intValue();
  }
  
  public void setNadap(BigDecimal pNadap) {
    if (pNadap == null) {
      return;
    }
    nadap = pNadap.setScale(DECIMAL_NADAP, RoundingMode.HALF_UP);
  }
  
  public void setNadap(Integer pNadap) {
    if (pNadap == null) {
      return;
    }
    nadap = BigDecimal.valueOf(pNadap);
  }
  
  public void setNadap(Date pNadap) {
    if (pNadap == null) {
      return;
    }
    nadap = BigDecimal.valueOf(ConvertDate.dateToDb2(pNadap));
  }
  
  public Integer getNadap() {
    return nadap.intValue();
  }
  
  public Date getNadapConvertiEnDate() {
    return ConvertDate.db2ToDate(nadap.intValue(), null);
  }
  
  public void setNacpv(Character pNacpv) {
    if (pNacpv == null) {
      return;
    }
    nacpv = String.valueOf(pNacpv);
  }
  
  public Character getNacpv() {
    return nacpv.charAt(0);
  }
  
  public void setNarem1(BigDecimal pNarem1) {
    if (pNarem1 == null) {
      return;
    }
    narem1 = pNarem1.setScale(DECIMAL_NAREM1, RoundingMode.HALF_UP);
  }
  
  public void setNarem1(Double pNarem1) {
    if (pNarem1 == null) {
      return;
    }
    narem1 = BigDecimal.valueOf(pNarem1).setScale(DECIMAL_NAREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNarem1() {
    return narem1.setScale(DECIMAL_NAREM1, RoundingMode.HALF_UP);
  }
  
  public void setNarem2(BigDecimal pNarem2) {
    if (pNarem2 == null) {
      return;
    }
    narem2 = pNarem2.setScale(DECIMAL_NAREM2, RoundingMode.HALF_UP);
  }
  
  public void setNarem2(Double pNarem2) {
    if (pNarem2 == null) {
      return;
    }
    narem2 = BigDecimal.valueOf(pNarem2).setScale(DECIMAL_NAREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNarem2() {
    return narem2.setScale(DECIMAL_NAREM2, RoundingMode.HALF_UP);
  }
  
  public void setNarem3(BigDecimal pNarem3) {
    if (pNarem3 == null) {
      return;
    }
    narem3 = pNarem3.setScale(DECIMAL_NAREM3, RoundingMode.HALF_UP);
  }
  
  public void setNarem3(Double pNarem3) {
    if (pNarem3 == null) {
      return;
    }
    narem3 = BigDecimal.valueOf(pNarem3).setScale(DECIMAL_NAREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNarem3() {
    return narem3.setScale(DECIMAL_NAREM3, RoundingMode.HALF_UP);
  }
  
  public void setNarem4(BigDecimal pNarem4) {
    if (pNarem4 == null) {
      return;
    }
    narem4 = pNarem4.setScale(DECIMAL_NAREM4, RoundingMode.HALF_UP);
  }
  
  public void setNarem4(Double pNarem4) {
    if (pNarem4 == null) {
      return;
    }
    narem4 = BigDecimal.valueOf(pNarem4).setScale(DECIMAL_NAREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNarem4() {
    return narem4.setScale(DECIMAL_NAREM4, RoundingMode.HALF_UP);
  }
  
  public void setNarem5(BigDecimal pNarem5) {
    if (pNarem5 == null) {
      return;
    }
    narem5 = pNarem5.setScale(DECIMAL_NAREM5, RoundingMode.HALF_UP);
  }
  
  public void setNarem5(Double pNarem5) {
    if (pNarem5 == null) {
      return;
    }
    narem5 = BigDecimal.valueOf(pNarem5).setScale(DECIMAL_NAREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNarem5() {
    return narem5.setScale(DECIMAL_NAREM5, RoundingMode.HALF_UP);
  }
  
  public void setNarem6(BigDecimal pNarem6) {
    if (pNarem6 == null) {
      return;
    }
    narem6 = pNarem6.setScale(DECIMAL_NAREM6, RoundingMode.HALF_UP);
  }
  
  public void setNarem6(Double pNarem6) {
    if (pNarem6 == null) {
      return;
    }
    narem6 = BigDecimal.valueOf(pNarem6).setScale(DECIMAL_NAREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNarem6() {
    return narem6.setScale(DECIMAL_NAREM6, RoundingMode.HALF_UP);
  }
  
  public void setNapra(BigDecimal pNapra) {
    if (pNapra == null) {
      return;
    }
    napra = pNapra.setScale(DECIMAL_NAPRA, RoundingMode.HALF_UP);
  }
  
  public void setNapra(Double pNapra) {
    if (pNapra == null) {
      return;
    }
    napra = BigDecimal.valueOf(pNapra).setScale(DECIMAL_NAPRA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNapra() {
    return napra.setScale(DECIMAL_NAPRA, RoundingMode.HALF_UP);
  }
  
  public void setNauna(String pNauna) {
    if (pNauna == null) {
      return;
    }
    nauna = pNauna;
  }
  
  public String getNauna() {
    return nauna;
  }
  
  public void setNanua(BigDecimal pNanua) {
    if (pNanua == null) {
      return;
    }
    nanua = pNanua.setScale(DECIMAL_NANUA, RoundingMode.HALF_UP);
  }
  
  public void setNanua(Double pNanua) {
    if (pNanua == null) {
      return;
    }
    nanua = BigDecimal.valueOf(pNanua).setScale(DECIMAL_NANUA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNanua() {
    return nanua.setScale(DECIMAL_NANUA, RoundingMode.HALF_UP);
  }
  
  public void setNakac(BigDecimal pNakac) {
    if (pNakac == null) {
      return;
    }
    nakac = pNakac.setScale(DECIMAL_NAKAC, RoundingMode.HALF_UP);
  }
  
  public void setNakac(Double pNakac) {
    if (pNakac == null) {
      return;
    }
    nakac = BigDecimal.valueOf(pNakac).setScale(DECIMAL_NAKAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNakac() {
    return nakac.setScale(DECIMAL_NAKAC, RoundingMode.HALF_UP);
  }
  
  public void setNadel(BigDecimal pNadel) {
    if (pNadel == null) {
      return;
    }
    nadel = pNadel.setScale(DECIMAL_NADEL, RoundingMode.HALF_UP);
  }
  
  public void setNadel(Integer pNadel) {
    if (pNadel == null) {
      return;
    }
    nadel = BigDecimal.valueOf(pNadel);
  }
  
  public Integer getNadel() {
    return nadel.intValue();
  }
  
  public void setNaref(String pNaref) {
    if (pNaref == null) {
      return;
    }
    naref = pNaref;
  }
  
  public String getNaref() {
    return naref;
  }
  
  public void setNaunc(String pNaunc) {
    if (pNaunc == null) {
      return;
    }
    naunc = pNaunc;
  }
  
  public String getNaunc() {
    return naunc;
  }
  
  public void setNaksc(BigDecimal pNaksc) {
    if (pNaksc == null) {
      return;
    }
    naksc = pNaksc.setScale(DECIMAL_NAKSC, RoundingMode.HALF_UP);
  }
  
  public void setNaksc(Double pNaksc) {
    if (pNaksc == null) {
      return;
    }
    naksc = BigDecimal.valueOf(pNaksc).setScale(DECIMAL_NAKSC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNaksc() {
    return naksc.setScale(DECIMAL_NAKSC, RoundingMode.HALF_UP);
  }
  
  public void setNadcc(BigDecimal pNadcc) {
    if (pNadcc == null) {
      return;
    }
    nadcc = pNadcc.setScale(DECIMAL_NADCC, RoundingMode.HALF_UP);
  }
  
  public void setNadcc(Integer pNadcc) {
    if (pNadcc == null) {
      return;
    }
    nadcc = BigDecimal.valueOf(pNadcc);
  }
  
  public Integer getNadcc() {
    return nadcc.intValue();
  }
  
  public void setNadca(BigDecimal pNadca) {
    if (pNadca == null) {
      return;
    }
    nadca = pNadca.setScale(DECIMAL_NADCA, RoundingMode.HALF_UP);
  }
  
  public void setNadca(Integer pNadca) {
    if (pNadca == null) {
      return;
    }
    nadca = BigDecimal.valueOf(pNadca);
  }
  
  public Integer getNadca() {
    return nadca.intValue();
  }
  
  public void setNadev(String pNadev) {
    if (pNadev == null) {
      return;
    }
    nadev = pNadev;
  }
  
  public String getNadev() {
    return nadev;
  }
  
  public void setNaqmi(BigDecimal pNaqmi) {
    if (pNaqmi == null) {
      return;
    }
    naqmi = pNaqmi.setScale(DECIMAL_NAQMI, RoundingMode.HALF_UP);
  }
  
  public void setNaqmi(Double pNaqmi) {
    if (pNaqmi == null) {
      return;
    }
    naqmi = BigDecimal.valueOf(pNaqmi).setScale(DECIMAL_NAQMI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNaqmi() {
    return naqmi.setScale(DECIMAL_NAQMI, RoundingMode.HALF_UP);
  }
  
  public void setNaqec(BigDecimal pNaqec) {
    if (pNaqec == null) {
      return;
    }
    naqec = pNaqec.setScale(DECIMAL_NAQEC, RoundingMode.HALF_UP);
  }
  
  public void setNaqec(Double pNaqec) {
    if (pNaqec == null) {
      return;
    }
    naqec = BigDecimal.valueOf(pNaqec).setScale(DECIMAL_NAQEC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNaqec() {
    return naqec.setScale(DECIMAL_NAQEC, RoundingMode.HALF_UP);
  }
  
  public void setNakpv(BigDecimal pNakpv) {
    if (pNakpv == null) {
      return;
    }
    nakpv = pNakpv.setScale(DECIMAL_NAKPV, RoundingMode.HALF_UP);
  }
  
  public void setNakpv(Double pNakpv) {
    if (pNakpv == null) {
      return;
    }
    nakpv = BigDecimal.valueOf(pNakpv).setScale(DECIMAL_NAKPV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNakpv() {
    return nakpv.setScale(DECIMAL_NAKPV, RoundingMode.HALF_UP);
  }
  
  public void setNaddp(BigDecimal pNaddp) {
    if (pNaddp == null) {
      return;
    }
    naddp = pNaddp.setScale(DECIMAL_NADDP, RoundingMode.HALF_UP);
  }
  
  public void setNaddp(Integer pNaddp) {
    if (pNaddp == null) {
      return;
    }
    naddp = BigDecimal.valueOf(pNaddp);
  }
  
  public void setNaddp(Date pNaddp) {
    if (pNaddp == null) {
      return;
    }
    naddp = BigDecimal.valueOf(ConvertDate.dateToDb2(pNaddp));
  }
  
  public Integer getNaddp() {
    return naddp.intValue();
  }
  
  public Date getNaddpConvertiEnDate() {
    return ConvertDate.db2ToDate(naddp.intValue(), null);
  }
  
  public void setNarga(String pNarga) {
    if (pNarga == null) {
      return;
    }
    narga = pNarga;
  }
  
  public String getNarga() {
    return narga;
  }
  
  public void setNadels(BigDecimal pNadels) {
    if (pNadels == null) {
      return;
    }
    nadels = pNadels.setScale(DECIMAL_NADELS, RoundingMode.HALF_UP);
  }
  
  public void setNadels(Integer pNadels) {
    if (pNadels == null) {
      return;
    }
    nadels = BigDecimal.valueOf(pNadels);
  }
  
  public Integer getNadels() {
    return nadels.intValue();
  }
  
  public void setNakpr(BigDecimal pNakpr) {
    if (pNakpr == null) {
      return;
    }
    nakpr = pNakpr.setScale(DECIMAL_NAKPR, RoundingMode.HALF_UP);
  }
  
  public void setNakpr(Double pNakpr) {
    if (pNakpr == null) {
      return;
    }
    nakpr = BigDecimal.valueOf(pNakpr).setScale(DECIMAL_NAKPR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNakpr() {
    return nakpr.setScale(DECIMAL_NAKPR, RoundingMode.HALF_UP);
  }
  
  public void setNain1(Character pNain1) {
    if (pNain1 == null) {
      return;
    }
    nain1 = String.valueOf(pNain1);
  }
  
  public Character getNain1() {
    return nain1.charAt(0);
  }
  
  public void setNain2(Character pNain2) {
    if (pNain2 == null) {
      return;
    }
    nain2 = String.valueOf(pNain2);
  }
  
  public Character getNain2() {
    return nain2.charAt(0);
  }
  
  public void setNain3(Character pNain3) {
    if (pNain3 == null) {
      return;
    }
    nain3 = String.valueOf(pNain3);
  }
  
  public Character getNain3() {
    return nain3.charAt(0);
  }
  
  public void setNain4(Character pNain4) {
    if (pNain4 == null) {
      return;
    }
    nain4 = String.valueOf(pNain4);
  }
  
  public Character getNain4() {
    return nain4.charAt(0);
  }
  
  public void setNain5(Character pNain5) {
    if (pNain5 == null) {
      return;
    }
    nain5 = String.valueOf(pNain5);
  }
  
  public Character getNain5() {
    return nain5.charAt(0);
  }
  
  public void setNatrl(Character pNatrl) {
    if (pNatrl == null) {
      return;
    }
    natrl = String.valueOf(pNatrl);
  }
  
  public Character getNatrl() {
    return natrl.charAt(0);
  }
  
  public void setNarfc(String pNarfc) {
    if (pNarfc == null) {
      return;
    }
    narfc = pNarfc;
  }
  
  public String getNarfc() {
    return narfc;
  }
  
  public void setNagcd(BigDecimal pNagcd) {
    if (pNagcd == null) {
      return;
    }
    nagcd = pNagcd.setScale(DECIMAL_NAGCD, RoundingMode.HALF_UP);
  }
  
  public void setNagcd(Integer pNagcd) {
    if (pNagcd == null) {
      return;
    }
    nagcd = BigDecimal.valueOf(pNagcd);
  }
  
  public Integer getNagcd() {
    return nagcd.intValue();
  }
  
  public void setNadaf(BigDecimal pNadaf) {
    if (pNadaf == null) {
      return;
    }
    nadaf = pNadaf.setScale(DECIMAL_NADAF, RoundingMode.HALF_UP);
  }
  
  public void setNadaf(Integer pNadaf) {
    if (pNadaf == null) {
      return;
    }
    nadaf = BigDecimal.valueOf(pNadaf);
  }
  
  public void setNadaf(Date pNadaf) {
    if (pNadaf == null) {
      return;
    }
    nadaf = BigDecimal.valueOf(ConvertDate.dateToDb2(pNadaf));
  }
  
  public Integer getNadaf() {
    return nadaf.intValue();
  }
  
  public Date getNadafConvertiEnDate() {
    return ConvertDate.db2ToDate(nadaf.intValue(), null);
  }
  
  public void setNalib(String pNalib) {
    if (pNalib == null) {
      return;
    }
    nalib = pNalib;
  }
  
  public String getNalib() {
    return nalib;
  }
  
  public void setNaopa(String pNaopa) {
    if (pNaopa == null) {
      return;
    }
    naopa = pNaopa;
  }
  
  public String getNaopa() {
    return naopa;
  }
  
  public void setNain6(Character pNain6) {
    if (pNain6 == null) {
      return;
    }
    nain6 = String.valueOf(pNain6);
  }
  
  public Character getNain6() {
    return nain6.charAt(0);
  }
  
  public void setNain7(Character pNain7) {
    if (pNain7 == null) {
      return;
    }
    nain7 = String.valueOf(pNain7);
  }
  
  public Character getNain7() {
    return nain7.charAt(0);
  }
  
  public void setNain8(Character pNain8) {
    if (pNain8 == null) {
      return;
    }
    nain8 = String.valueOf(pNain8);
  }
  
  public Character getNain8() {
    return nain8.charAt(0);
  }
  
  public void setNain9(Character pNain9) {
    if (pNain9 == null) {
      return;
    }
    nain9 = String.valueOf(pNain9);
  }
  
  public Character getNain9() {
    return nain9.charAt(0);
  }
  
  public void setNapdi(BigDecimal pNapdi) {
    if (pNapdi == null) {
      return;
    }
    napdi = pNapdi.setScale(DECIMAL_NAPDI, RoundingMode.HALF_UP);
  }
  
  public void setNapdi(Double pNapdi) {
    if (pNapdi == null) {
      return;
    }
    napdi = BigDecimal.valueOf(pNapdi).setScale(DECIMAL_NAPDI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNapdi() {
    return napdi.setScale(DECIMAL_NAPDI, RoundingMode.HALF_UP);
  }
  
  public void setNakap(BigDecimal pNakap) {
    if (pNakap == null) {
      return;
    }
    nakap = pNakap.setScale(DECIMAL_NAKAP, RoundingMode.HALF_UP);
  }
  
  public void setNakap(Double pNakap) {
    if (pNakap == null) {
      return;
    }
    nakap = BigDecimal.valueOf(pNakap).setScale(DECIMAL_NAKAP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNakap() {
    return nakap.setScale(DECIMAL_NAKAP, RoundingMode.HALF_UP);
  }
  
  public void setNaprs(BigDecimal pNaprs) {
    if (pNaprs == null) {
      return;
    }
    naprs = pNaprs.setScale(DECIMAL_NAPRS, RoundingMode.HALF_UP);
  }
  
  public void setNaprs(Double pNaprs) {
    if (pNaprs == null) {
      return;
    }
    naprs = BigDecimal.valueOf(pNaprs).setScale(DECIMAL_NAPRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNaprs() {
    return naprs.setScale(DECIMAL_NAPRS, RoundingMode.HALF_UP);
  }
  
  public void setNapgn(Character pNapgn) {
    if (pNapgn == null) {
      return;
    }
    napgn = String.valueOf(pNapgn);
  }
  
  public Character getNapgn() {
    return napgn.charAt(0);
  }
  
  public void setNafv1(BigDecimal pNafv1) {
    if (pNafv1 == null) {
      return;
    }
    nafv1 = pNafv1.setScale(DECIMAL_NAFV1, RoundingMode.HALF_UP);
  }
  
  public void setNafv1(Double pNafv1) {
    if (pNafv1 == null) {
      return;
    }
    nafv1 = BigDecimal.valueOf(pNafv1).setScale(DECIMAL_NAFV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNafv1() {
    return nafv1.setScale(DECIMAL_NAFV1, RoundingMode.HALF_UP);
  }
  
  public void setNafk1(BigDecimal pNafk1) {
    if (pNafk1 == null) {
      return;
    }
    nafk1 = pNafk1.setScale(DECIMAL_NAFK1, RoundingMode.HALF_UP);
  }
  
  public void setNafk1(Double pNafk1) {
    if (pNafk1 == null) {
      return;
    }
    nafk1 = BigDecimal.valueOf(pNafk1).setScale(DECIMAL_NAFK1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNafk1() {
    return nafk1.setScale(DECIMAL_NAFK1, RoundingMode.HALF_UP);
  }
  
  public void setNafp1(BigDecimal pNafp1) {
    if (pNafp1 == null) {
      return;
    }
    nafp1 = pNafp1.setScale(DECIMAL_NAFP1, RoundingMode.HALF_UP);
  }
  
  public void setNafp1(Double pNafp1) {
    if (pNafp1 == null) {
      return;
    }
    nafp1 = BigDecimal.valueOf(pNafp1).setScale(DECIMAL_NAFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNafp1() {
    return nafp1.setScale(DECIMAL_NAFP1, RoundingMode.HALF_UP);
  }
  
  public void setNafu1(Character pNafu1) {
    if (pNafu1 == null) {
      return;
    }
    nafu1 = String.valueOf(pNafu1);
  }
  
  public Character getNafu1() {
    return nafu1.charAt(0);
  }
  
  public void setNafv2(BigDecimal pNafv2) {
    if (pNafv2 == null) {
      return;
    }
    nafv2 = pNafv2.setScale(DECIMAL_NAFV2, RoundingMode.HALF_UP);
  }
  
  public void setNafv2(Double pNafv2) {
    if (pNafv2 == null) {
      return;
    }
    nafv2 = BigDecimal.valueOf(pNafv2).setScale(DECIMAL_NAFV2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNafv2() {
    return nafv2.setScale(DECIMAL_NAFV2, RoundingMode.HALF_UP);
  }
  
  public void setNafk2(BigDecimal pNafk2) {
    if (pNafk2 == null) {
      return;
    }
    nafk2 = pNafk2.setScale(DECIMAL_NAFK2, RoundingMode.HALF_UP);
  }
  
  public void setNafk2(Double pNafk2) {
    if (pNafk2 == null) {
      return;
    }
    nafk2 = BigDecimal.valueOf(pNafk2).setScale(DECIMAL_NAFK2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNafk2() {
    return nafk2.setScale(DECIMAL_NAFK2, RoundingMode.HALF_UP);
  }
  
  public void setNafp2(BigDecimal pNafp2) {
    if (pNafp2 == null) {
      return;
    }
    nafp2 = pNafp2.setScale(DECIMAL_NAFP2, RoundingMode.HALF_UP);
  }
  
  public void setNafp2(Double pNafp2) {
    if (pNafp2 == null) {
      return;
    }
    nafp2 = BigDecimal.valueOf(pNafp2).setScale(DECIMAL_NAFP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNafp2() {
    return nafp2.setScale(DECIMAL_NAFP2, RoundingMode.HALF_UP);
  }
  
  public void setNafu2(Character pNafu2) {
    if (pNafu2 == null) {
      return;
    }
    nafu2 = String.valueOf(pNafu2);
  }
  
  public Character getNafu2() {
    return nafu2.charAt(0);
  }
  
  public void setNafv3(BigDecimal pNafv3) {
    if (pNafv3 == null) {
      return;
    }
    nafv3 = pNafv3.setScale(DECIMAL_NAFV3, RoundingMode.HALF_UP);
  }
  
  public void setNafv3(Double pNafv3) {
    if (pNafv3 == null) {
      return;
    }
    nafv3 = BigDecimal.valueOf(pNafv3).setScale(DECIMAL_NAFV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNafv3() {
    return nafv3.setScale(DECIMAL_NAFV3, RoundingMode.HALF_UP);
  }
  
  public void setNafk3(BigDecimal pNafk3) {
    if (pNafk3 == null) {
      return;
    }
    nafk3 = pNafk3.setScale(DECIMAL_NAFK3, RoundingMode.HALF_UP);
  }
  
  public void setNafk3(Double pNafk3) {
    if (pNafk3 == null) {
      return;
    }
    nafk3 = BigDecimal.valueOf(pNafk3).setScale(DECIMAL_NAFK3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNafk3() {
    return nafk3.setScale(DECIMAL_NAFK3, RoundingMode.HALF_UP);
  }
  
  public void setNafu3(Character pNafu3) {
    if (pNafu3 == null) {
      return;
    }
    nafu3 = String.valueOf(pNafu3);
  }
  
  public Character getNafu3() {
    return nafu3.charAt(0);
  }
  
  public void setNapr1(BigDecimal pNapr1) {
    if (pNapr1 == null) {
      return;
    }
    napr1 = pNapr1.setScale(DECIMAL_NAPR1, RoundingMode.HALF_UP);
  }
  
  public void setNapr1(Double pNapr1) {
    if (pNapr1 == null) {
      return;
    }
    napr1 = BigDecimal.valueOf(pNapr1).setScale(DECIMAL_NAPR1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNapr1() {
    return napr1.setScale(DECIMAL_NAPR1, RoundingMode.HALF_UP);
  }
  
  public void setNapr2(BigDecimal pNapr2) {
    if (pNapr2 == null) {
      return;
    }
    napr2 = pNapr2.setScale(DECIMAL_NAPR2, RoundingMode.HALF_UP);
  }
  
  public void setNapr2(Double pNapr2) {
    if (pNapr2 == null) {
      return;
    }
    napr2 = BigDecimal.valueOf(pNapr2).setScale(DECIMAL_NAPR2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getNapr2() {
    return napr2.setScale(DECIMAL_NAPR2, RoundingMode.HALF_UP);
  }
  
  public void setNadat1(BigDecimal pNadat1) {
    if (pNadat1 == null) {
      return;
    }
    nadat1 = pNadat1.setScale(DECIMAL_NADAT1, RoundingMode.HALF_UP);
  }
  
  public void setNadat1(Integer pNadat1) {
    if (pNadat1 == null) {
      return;
    }
    nadat1 = BigDecimal.valueOf(pNadat1);
  }
  
  public void setNadat1(Date pNadat1) {
    if (pNadat1 == null) {
      return;
    }
    nadat1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pNadat1));
  }
  
  public Integer getNadat1() {
    return nadat1.intValue();
  }
  
  public Date getNadat1ConvertiEnDate() {
    return ConvertDate.db2ToDate(nadat1.intValue(), null);
  }
  
  public void setNadat2(BigDecimal pNadat2) {
    if (pNadat2 == null) {
      return;
    }
    nadat2 = pNadat2.setScale(DECIMAL_NADAT2, RoundingMode.HALF_UP);
  }
  
  public void setNadat2(Integer pNadat2) {
    if (pNadat2 == null) {
      return;
    }
    nadat2 = BigDecimal.valueOf(pNadat2);
  }
  
  public void setNadat2(Date pNadat2) {
    if (pNadat2 == null) {
      return;
    }
    nadat2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pNadat2));
  }
  
  public Integer getNadat2() {
    return nadat2.intValue();
  }
  
  public Date getNadat2ConvertiEnDate() {
    return ConvertDate.db2ToDate(nadat2.intValue(), null);
  }
  
  public void setNaarr(Character pNaarr) {
    if (pNaarr == null) {
      return;
    }
    naarr = String.valueOf(pNaarr);
  }
  
  public Character getNaarr() {
    return naarr.charAt(0);
  }
}
