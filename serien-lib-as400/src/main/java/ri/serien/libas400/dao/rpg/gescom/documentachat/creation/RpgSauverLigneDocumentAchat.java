/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.creation;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.EnumCodeExtractionDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatCommentaire;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.document.ListeZonePersonnalisee;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

public class RpgSauverLigneDocumentAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGAM0005";
  
  /**
   * Appel du programme RPG qui va créer une ligne dans un document.
   */
  public void sauverLigneAchat(SystemeManager pSysteme, LigneAchatArticle pLigneArticle, Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pLigneArticle == null) {
      throw new MessageErreurException("Le paramètre pLigneArticle du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0005i entree = new Svgam0005i();
    Svgam0005o sortie = new Svgam0005o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind(pLigneArticle.getIndicateurs());
    entree.setLaetb(pLigneArticle.getId().getCodeEtablissement());
    entree.setLacod(pLigneArticle.getId().getCodeEntete().getCode());
    entree.setLanum(pLigneArticle.getId().getNumero());
    entree.setLasuf(pLigneArticle.getId().getSuffixe());
    entree.setLanli(pLigneArticle.getId().getNumeroLigne());
    entree.setPidat(pDateTraitement);
    
    PrixAchat prixAchat = pLigneArticle.getPrixAchat();
    if (prixAchat != null) {
      // Renseigner les unités
      if (prixAchat.getIdUA() != null) {
        entree.setLauna(prixAchat.getIdUA().getCode());
      }
      if (prixAchat.getIdUCA() != null) {
        entree.setLaunc(prixAchat.getIdUCA().getCode());
      }
      entree.setLadca(prixAchat.getNombreDecimaleUA());
      entree.setLadcc(prixAchat.getNombreDecimaleUCA());
      entree.setLadcs(prixAchat.getNombreDecimaleUS());
      
      // Renseigner les ratios
      entree.setLakac(prixAchat.getNombreUAParUCA());
      entree.setLaksc(prixAchat.getNombreUSParUCA());
      
      // Renseigner les quantités
      entree.setLaqta(prixAchat.getQuantiteReliquatUA());
      entree.setLaqtc(prixAchat.getQuantiteReliquatUCA());
      entree.setLaqts(prixAchat.getQuantiteReliquatUS());
      
      // Renseigner les montants
      entree.setLapab(prixAchat.getPrixAchatBrutHT());
      ListeRemise listeRemise = prixAchat.getListeRemise();
      entree.setLarem1(listeRemise.getPourcentageRemise1());
      entree.setLarem2(listeRemise.getPourcentageRemise2());
      entree.setLarem3(listeRemise.getPourcentageRemise3());
      entree.setLarem4(listeRemise.getPourcentageRemise4());
      entree.setLarem5(listeRemise.getPourcentageRemise5());
      entree.setLarem6(listeRemise.getPourcentageRemise6());
      entree.setLatrl(listeRemise.getTypeRemise());
      entree.setLabrl(listeRemise.getBaseRemise());
      entree.setPilfk3(prixAchat.getPourcentageTaxeOuMajoration());
      entree.setPilfv3(prixAchat.getMontantConditionnement());
      entree.setLapac(prixAchat.getPrixAchatCalculeHT());
      entree.setLapan(prixAchat.getPrixAchatNetHT());
      entree.setPiprnet(prixAchat.getPrixAchatNetHT()); // Prix net HT à sauver également dans ce champ
      entree.setPilfv1(prixAchat.getMontantAuPoidsPortHT());
      entree.setPilfk1(prixAchat.getPoidsPort());
      entree.setPilfp1(prixAchat.getPourcentagePort().getTauxAuFormatOriginal());
      entree.setPiportf(prixAchat.getMontantPortHT());
      entree.setPiprvfr(prixAchat.getPrixRevientFournisseurHT());
      entree.setLamht(prixAchat.getMontantReliquatHT());
      
      // Renseigner les indicateurs
      entree.setLatb(prixAchat.getTopPrixBaseSaisie());
      entree.setLatr(prixAchat.getTopRemiseSaisie());
      entree.setLatn(prixAchat.getTopPrixNetSaisi());
      entree.setLatu(prixAchat.getTopUniteSaisie());
      entree.setLatq(prixAchat.getTopQuantiteSaisie());
      entree.setLath(prixAchat.getTopMontantHTSaisi());
      
      // Renseigner les exclusions de remises de pied
      entree.setLarp1(prixAchat.getExclusionRemisePied(PrixAchat.REMISE_1));
      entree.setLarp2(prixAchat.getExclusionRemisePied(PrixAchat.REMISE_2));
      entree.setLarp3(prixAchat.getExclusionRemisePied(PrixAchat.REMISE_3));
      entree.setLarp4(prixAchat.getExclusionRemisePied(PrixAchat.REMISE_4));
      entree.setLarp5(prixAchat.getExclusionRemisePied(PrixAchat.REMISE_5));
      entree.setLarp6(prixAchat.getExclusionRemisePied(PrixAchat.REMISE_6));
      
      // Renseigner les éléments de TVA
      entree.setLatva(prixAchat.getCodeTVA());
      entree.setLacol(prixAchat.getColonneTVA());
    }
    
    entree.setLatop(pLigneArticle.getCodeEtat());
    entree.setLacod(pLigneArticle.getId().getCodeEntete().getCode());
    entree.setLaetb(pLigneArticle.getId().getCodeEtablissement());
    entree.setLanum(pLigneArticle.getId().getNumero());
    entree.setLasuf(pLigneArticle.getId().getSuffixe());
    entree.setLanli(pLigneArticle.getId().getNumeroLigne());
    entree.setLaerl(pLigneArticle.getTypeLigne().getCode());
    entree.setLacex(pLigneArticle.getCodeExtraction().getCode());
    entree.setLaqcx(pLigneArticle.getQuantiteExtraiteUV());
    entree.setLatva(pLigneArticle.getCodeTVA());
    
    entree.setLaval(pLigneArticle.getTopLigneEnValeur());
    entree.setLacol(pLigneArticle.getNumeroColonneTVA());
    entree.setLasgn(pLigneArticle.getSigneLigne());
    entree.setLaavr(pLigneArticle.getCodeAvoir());
    
    entree.setLaart(pLigneArticle.getIdArticle().getCodeArticle());
    if (pLigneArticle.getIdMagasin() != null) {
      entree.setLamag(pLigneArticle.getIdMagasin().getCode());
    }
    if (pLigneArticle.getIdMagasinAvantModif() != null) {
      entree.setLamaga(pLigneArticle.getIdMagasinAvantModif().getCode());
    }
    IdLigneAchat idLigneRapprochee = pLigneArticle.getIdLigneRapprochee();
    if (idLigneRapprochee != null) {
      entree.setLanumr(idLigneRapprochee.getNumero());
      entree.setLasufr(idLigneRapprochee.getSuffixe());
      entree.setLanlir(idLigneRapprochee.getNumeroLigne());
    }
    entree.setLadath(pLigneArticle.getDateHistoriqueStock());
    entree.setLaordh(pLigneArticle.getOrdreHistoriqueStock());
    entree.setLasan(pLigneArticle.getCodeSectionAnalytique());
    entree.setLaact(pLigneArticle.getCodeAffaire());
    entree.setLanat(pLigneArticle.getCodeNatureAchat());
    entree.setLamta(pLigneArticle.getMontantAffecte());
    entree.setLadlp(pLigneArticle.getDateLivraisonPrevue());
    entree.setLaser(pLigneArticle.getEtatSaisieNumeroSerieOuLot());
    if (pLigneArticle.getListeTopPersonnalisable() != null) {
      ListeZonePersonnalisee liste = pLigneArticle.getListeTopPersonnalisable();
      entree.setLatp1(liste.getTopPersonnalisation1());
      entree.setLatp2(liste.getTopPersonnalisation2());
      entree.setLatp3(liste.getTopPersonnalisation3());
      entree.setLatp4(liste.getTopPersonnalisation4());
      entree.setLatp5(liste.getTopPersonnalisation5());
    }
    entree.setLain1(pLigneArticle.getImputationFrais());
    entree.setLain2(pLigneArticle.getTypeFrais());
    entree.setLain3(pLigneArticle.getRegroupementArticle());
    entree.setLain4(pLigneArticle.getComptabilisationStockFlottant());
    entree.setLain5(pLigneArticle.getConditionEnNombreGratuit());
    entree.setLadlc(pLigneArticle.getDateLivraisonCalculee());
    
    // Création du lien avec la ligne de vente
    if (pLigneArticle.getIdLigneVente() != null) {
      IdLigneVente idLigneVente = pLigneArticle.getIdLigneVente();
      entree.setPitos('V');
      entree.setPicos(idLigneVente.getCodeEntete().getCode());
      entree.setPinos(idLigneVente.getNumero());
      entree.setPisos(idLigneVente.getSuffixe());
      entree.setPilos(idLigneVente.getNumeroLigne());
      entree.setPitoe('A');
    }
    
    entree.setPilb("");
    entree.setPinat(pLigneArticle.getCodeNatureAnalytique());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      controlerErreur();
      
      // Initialisation de la classe métier
      IdLigneAchat idLigneDocument = IdLigneAchat.getInstance(pLigneArticle.getId().getIdDocumentAchat(), sortie.getPonli());
      pLigneArticle.setId(idLigneDocument);
    }
  }
  
  /**
   * Appel du programme RPG qui va créer une ligne dans un document.
   */
  public void sauverLigneAchatCommentaire(SystemeManager pSysteme, LigneAchat pLigneAchat, Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pLigneAchat == null) {
      throw new MessageErreurException("Le paramètre pLigneAchat du service est à null.");
    }
    
    if (!(pLigneAchat instanceof LigneAchatCommentaire)) {
      throw new MessageErreurException("Le paramètre pLigneAchat du service est à null.");
    }
    
    LigneAchatCommentaire ligneAchatCommentaire = (LigneAchatCommentaire) pLigneAchat;
    
    if (ligneAchatCommentaire.getTexte() == null || ligneAchatCommentaire.getTexte().trim().isEmpty()) {
      return;
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0005i entree = new Svgam0005i();
    Svgam0005o sortie = new Svgam0005o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind(ligneAchatCommentaire.getIndicateurs());
    entree.setLaetb(ligneAchatCommentaire.getId().getCodeEtablissement());
    entree.setLacod(ligneAchatCommentaire.getId().getCodeEntete().getCode());
    entree.setLanum(ligneAchatCommentaire.getId().getNumero());
    entree.setLasuf(ligneAchatCommentaire.getId().getSuffixe());
    entree.setLanli(ligneAchatCommentaire.getId().getNumeroLigne());
    entree.setPidat(pDateTraitement);
    entree.setLaerl(ligneAchatCommentaire.getTypeLigne().getCode());
    Trace.info("++++++ Commentaire a inserer : |" + ligneAchatCommentaire.getTexte().trim() + "|");
    entree.setPilb(ligneAchatCommentaire.getTexte().trim());
    EnumCodeExtractionDocumentAchat codeExtraction = ligneAchatCommentaire.getCodeExtraction();
    if (codeExtraction == null) {
      codeExtraction = EnumCodeExtractionDocumentAchat.AUCUNE;
    }
    entree.setLacex(codeExtraction.getCode());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      controlerErreur();
    }
  }
}
