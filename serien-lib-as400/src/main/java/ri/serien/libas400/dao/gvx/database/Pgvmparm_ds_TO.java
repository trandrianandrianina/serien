/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_TO pour les TO
 */
public class Pgvmparm_ds_TO extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "TOLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "TOMEX"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "TODEL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "TOGEO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "TOMAG"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TOTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "TORGR"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "TOREP"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "TODP1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "TODP2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "TODP3"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "TODP4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(35), "TOJP1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(35), "TOJP2"));
    
    length = 300;
  }
}
