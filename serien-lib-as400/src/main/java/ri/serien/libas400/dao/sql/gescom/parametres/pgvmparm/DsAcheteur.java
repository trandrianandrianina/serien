/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_AH pour les AH
 */
public class DsAcheteur extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier.
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "PARETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "PARTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PARIND"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "AHLIB"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(6, 2), "AHBUD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 2), "AHBUD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "AHMMX"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(40), "AHOP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "AHHOM"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "AHREC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "AHFAC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "AHTEL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(40), "AHMAI"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "AHMS1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "AHMS2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "AHMS3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "AHMS4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "AHMS5"));
    
    length = 300;
  }
}
