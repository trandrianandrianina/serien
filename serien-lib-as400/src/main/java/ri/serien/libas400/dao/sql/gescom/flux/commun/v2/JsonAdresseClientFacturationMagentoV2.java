/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2;

import ri.serien.libas400.dao.sql.gescom.flux.OutilsMagento;
import ri.serien.libcommun.outils.Constantes;

/**
 * Cette classe décrit l'objet bloc adresse du client de facturation.
 * Ce client de facturation correspond aux champs CLCLFP et CLCLFS de la table PGVMCLIM. Ce cas de figure est normalement rare.
 */
public class JsonAdresseClientFacturationMagentoV2 {
  // Constantes
  private static final int LONGUEUR_CIVILITE = 3;
  private static final int LONGUEUR_TELEPHONE = 20;
  
  // Variables
  private String idClientFacturation = null;
  private String idClientFacturationMagento = null;
  private int isUnParticulierClientFacturation = JsonClientMagentoV2.ISPROFESSIONNEL;
  private String societeClientFacturation = null;
  private String complementNomClientFacturation = null;
  private String civiliteClientFacturation = null;
  private String nomClientFacturation = null;
  private String prenomClientFacturation = null;
  private String rueClientFacturation = null;
  private String rue2ClientFacturation = null;
  private String cdpClientFacturation = null;
  private String villeClientFacturation = null;
  private String paysClientFacturation = null;
  private String libellePaysClientFacturation = null;
  private String telClientFacturation = null;
  
  private transient String CLCLI = null;
  private transient String CLLIV = null;
  private transient String CLNOM = null;
  private transient String CLRUE = null;
  private transient String CLLOC = null;
  private transient String CLCDP1 = null;
  private transient String CLVIL = null;
  private transient String CLADH = null;
  private transient String CLTEL = null;
  
  // -- Méthodes publiques
  
  /**
   * Permet de découper l'algorithme de Série N pour le cdp et la ville sur la zone CLVIL.
   */
  public void decouperVilleEtCDP(String pChampVilleAvecCdp) {
    if (pChampVilleAvecCdp == null || pChampVilleAvecCdp.trim().length() < 7) {
      cdpClientFacturation = "";
      villeClientFacturation = "";
      return;
    }
    
    cdpClientFacturation = pChampVilleAvecCdp.substring(0, 5);
    villeClientFacturation = pChampVilleAvecCdp.substring(6);
  }
  
  /**
   * Vérification que le code SérieNClientFacture transmis soit bien formaté.
   */
  public boolean decouperCodeERP() {
    if (idClientFacturation == null || idClientFacturation.trim().equals("")) {
      return false;
    }
    
    String[] tab = idClientFacturation.split("/");
    if (tab != null && tab.length == 2) {
      CLCLI = tab[0];
      CLLIV = tab[1];
      
      if (CLCLI.length() > 0 && CLLIV.length() > 0) {
        return true;
      }
    }
    
    return false;
  }
  
  /**
   * Mise en forme du bloc adresse en import de données depuis Magento.
   */
  public void miseEnFormeBlocAdresseImport() {
    CLNOM = "";
    CLADH = idClientFacturationMagento;
    
    // Si on a affaire à un particulier on va concaténer le nom et le prénom dans la même zone
    // Les véritables données sont dans le contact
    if (isUnParticulierClientFacturation == JsonClientMagentoV2.ISPARTICULIER) {
      if (nomClientFacturation != null) {
        CLNOM = nomClientFacturation.trim();
      }
      if (prenomClientFacturation != null) {
        CLNOM += " " + prenomClientFacturation.trim();
      }
      CLNOM = OutilsMagento.formaterTexteLongueurMax(CLNOM, 30);
      complementNomClientFacturation = "";
    }
    // Un pro (alors on checke la zone "nom")
    else {
      if (societeClientFacturation != null) {
        if (societeClientFacturation.trim().length() < 30) {
          CLNOM = societeClientFacturation.trim();
        }
        else {
          // On réduit à 60 Max (2 x 30)
          societeClientFacturation = OutilsMagento.formaterTexteLongueurMax(societeClientFacturation.trim(), 60);
          CLNOM = societeClientFacturation.substring(0, 30);
        }
        if (complementNomClientFacturation != null) {
          complementNomClientFacturation = OutilsMagento.formaterTexteLongueurMax(complementNomClientFacturation, 30);
        }
      }
    }
    
    CLRUE = "";
    CLLOC = "";
    
    if (rueClientFacturation != null) {
      CLRUE = rueClientFacturation;
    }
    if (rue2ClientFacturation != null) {
      CLLOC = rue2ClientFacturation;
    }
    
    if (cdpClientFacturation == null) {
      CLCDP1 = "00000";
    }
    else {
      CLCDP1 = OutilsMagento.formaterUnCodePostalpourSN(cdpClientFacturation);
    }
    
    if (villeClientFacturation == null) {
      CLVIL = CLCDP1 + "";
    }
    else {
      CLVIL = CLCDP1 + " " + OutilsMagento.formaterTexteLongueurMax(villeClientFacturation, 24);
    }
    
    // Le numéro de téléphone
    CLTEL = Constantes.normerTexte(telClientFacturation);
    if (CLTEL.length() > LONGUEUR_TELEPHONE) {
      CLTEL = CLTEL.substring(0, LONGUEUR_TELEPHONE);
    }
  }
  
  // -- Accesseurs
  
  public String getIdClientFacturation() {
    return idClientFacturation;
  }
  
  public void setIdClientFacturation(String pIdClientFacturation) {
    this.idClientFacturation = pIdClientFacturation;
  }
  
  public String getIdClientFacturationMagento() {
    return idClientFacturationMagento;
  }
  
  public void setIdClientFacturationMagento(String idClientFacturationMagento) {
    this.idClientFacturationMagento = idClientFacturationMagento;
  }
  
  public String getSocieteClientFacturation() {
    return societeClientFacturation;
  }
  
  public void setSocieteClientFacturation(String societeClientFacturation) {
    this.societeClientFacturation = societeClientFacturation;
  }
  
  public String getCiviliteClientFacturation() {
    return civiliteClientFacturation;
  }
  
  public void setCiviliteClientFacturation(String civiliteClientFacturation) {
    this.civiliteClientFacturation = civiliteClientFacturation;
  }
  
  public String getNomClientFacturation() {
    return nomClientFacturation;
  }
  
  public void setNomClientFacturation(String nomClientFacturation) {
    this.nomClientFacturation = nomClientFacturation;
  }
  
  public String getComplementNomClientFacturation() {
    return complementNomClientFacturation;
  }
  
  public void setComplementNomClientFacturation(String complementNomClientFacturation) {
    this.complementNomClientFacturation = complementNomClientFacturation;
  }
  
  public String getPrenomClientFacturation() {
    return prenomClientFacturation;
  }
  
  public void setPrenomClientFacturation(String prenomClientFacturation) {
    this.prenomClientFacturation = prenomClientFacturation;
  }
  
  public String getRueClientFacturation() {
    return rueClientFacturation;
  }
  
  public void setRueClientFacturation(String rueClientFacturation) {
    this.rueClientFacturation = rueClientFacturation;
  }
  
  public String getRue2ClientFacturation() {
    return rue2ClientFacturation;
  }
  
  public void setRue2ClientFacturation(String rue2ClientFacturation) {
    this.rue2ClientFacturation = rue2ClientFacturation;
  }
  
  public String getCdpClientFacturation() {
    return cdpClientFacturation;
  }
  
  public void setCdpClientFacturation(String cdpClientFacturation) {
    this.cdpClientFacturation = cdpClientFacturation;
  }
  
  public String getVilleClientFacturation() {
    return villeClientFacturation;
  }
  
  public void setVilleClientFacturation(String villeClientFacturation) {
    this.villeClientFacturation = villeClientFacturation;
  }
  
  public String getPaysClientFacturation() {
    return paysClientFacturation;
  }
  
  public void setPaysClientFacturation(String paysClientFacturation) {
    this.paysClientFacturation = paysClientFacturation;
  }
  
  public String getLibellePaysClientFacturation() {
    return libellePaysClientFacturation;
  }
  
  public void setLibellePaysClientFacturation(String libPaysClientFacturation) {
    this.libellePaysClientFacturation = libPaysClientFacturation;
  }
  
  public int getIsUnParticulierClientFacturation() {
    return isUnParticulierClientFacturation;
  }
  
  public void setIsUnParticulierClientFacturation(int isUnParticulierClientFacturation) {
    this.isUnParticulierClientFacturation = isUnParticulierClientFacturation;
  }
  
  public String getTelClientFacturation() {
    return telClientFacturation;
  }
  
  public void setTelClientFacturation(String telClientFacturation) {
    this.telClientFacturation = telClientFacturation;
  }
  
  public String getCLCLI() {
    return CLCLI;
  }
  
  public String getCLLIV() {
    return CLLIV;
  }
  
  public String getCLADH() {
    return CLADH;
  }
  
  public String getCLNOM() {
    return CLNOM;
  }
  
  public String getCLRUE() {
    return CLRUE;
  }
  
  public String getCLLOC() {
    return CLLOC;
  }
  
  public String getCLCDP1() {
    return CLCDP1;
  }
  
  public String getCLVIL() {
    return CLVIL;
  }
  
  public String getCLTEL() {
    return CLTEL;
  }
}
