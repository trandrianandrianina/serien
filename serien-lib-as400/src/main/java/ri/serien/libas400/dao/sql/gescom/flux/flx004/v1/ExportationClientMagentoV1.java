/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx004.v1;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v1.JsonClientMagentoV1;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Version 1 des flux.
 * Cette classe permet d'exporter un client Série N vers Magento.
 */
public class ExportationClientMagentoV1 extends BaseGroupDB {
  
  /**
   * Constructeur.
   */
  public ExportationClientMagentoV1(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner une string JSON de notre client Magento
   */
  public String retournerUnClientMagentoJSON(FluxMagento record, ParametresFluxBibli pParams) {
    if (record == null || record.getFLETB() == null || record.getFLCOD() == null) {
      majError("[GM_Export_Clients] retournerUnClientMagentoJSON() -> Record NULL ou corrompues ");
      return null;
    }
    
    String retour = null;
    
    GenericRecord clientDB2 = retournerClientDB2(record.getFLETB(), record.getFLCOD());
    
    if (clientDB2 != null) {
      JsonClientMagentoV1 clientMG = new JsonClientMagentoV1(record);
      Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), clientMG.getEtb());
      if (numeroInstance == null) {
        majError("[ExportClientMagento] retournerUnClientMagentoJSON() : probleme d'interpretation de l'ETb vers l'instance Magento");
        return null;
      }
      clientMG.setIdInstanceMagento(numeroInstance);
      
      // On met à jour le client Magento sur la base de celui de Série N
      if (!clientMG.majDesInfosPourExport(clientDB2, pParams)) {
        majError("[GM_Export_Clients] retournerUnClientMagentoJSON() :" + clientMG.getMsgError());
        return null;
      }
      
      // debut de la construction du JSON
      if (initJSON()) {
        // On contrôle au mooooins les données de base
        if (clientMG.getCLCLI() != null && clientMG.getCLLIV() != null && clientMG.getNom() != null && clientMG.getEmail() != null) {
          try {
            // On JSONE l'objet comme un sauvage
            retour = gson.toJson(clientMG);
          }
          catch (Exception e) {
            retour = null;
            majError("[GM_Export_Clients] retournerUnClientMagentoJSON() -> ERREUR JSON: " + e.getMessage());
          }
        }
        else {
          majError("[GM_Export_Clients] retournerUnArticleMagentoJSON() -> clientMG corrompu ");
        }
      }
      else {
        majError("[GM_Export_Clients] retournerUnArticleMagentoJSON() -> initJSON en erreur ");
      }
    }
    
    return retour;
  }
  
  @Override
  public void dispose() {
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne les données d'un client Série N sur la base de l'établissement et du code fourni.
   */
  private GenericRecord retournerClientDB2(String fletb, String flcod) {
    if (fletb == null || flcod == null) {
      majError("[GM_Export_Clients] retournerClientDB2() -> etb/code NULL ou corrompus ");
      return null;
    }
    
    // On découpe le code du record en identifiants clients CLCLI et CLLIV
    String CLCLI = null;
    String CLLIV = null;
    String[] tabDecoupe = flcod.split("/");
    if (tabDecoupe != null && tabDecoupe.length == 2) {
      CLCLI = tabDecoupe[0];
      // On prend le client principal Systématiquement
      // Les clients livrés apparaissent sous forme de clients livrés
      CLLIV = "0";
      // Si notre découpe est foireuse
      if (CLCLI == null || CLCLI.length() == 0) {
        majError("[GM_Export_Clients] retournerClientDB2() -> CLCLI ou CLLIV corrompus " + flcod);
        return null;
      }
    }
    else {
      majError("[GM_Export_Clients] retournerClientDB2() -> tabDecoupe NULL ou corrompue " + flcod);
      return null;
    }
    
    // On checke et on récupère le client
    // @formatter:off
    String requete = " SELECT CLETB,CLCLI,CLLIV,CLADH,CLAPEN,CLCAT,CLRGL,CLNOM,CLCPL,CLRUE,CLLOC,CLVIL,CLCDP1,"
        + " CLCOP,CLPAY,CLTEL,CLFAX,CLSRN,CLSRT,CLTOP1,CLTOP2,CLTAR,CLIN9,CLCLFS,CLCLFP,CLTFA,EBIN09,"
        + " RECIV, RENUM, REPAC, RENOM, REPRE, RENET FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT_LIEN
        + " ON RLETB = CLETB AND RLIND = DIGITS(CLCLI)||DIGITS(CLLIV) AND RLIN1 = 'P'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT
        + " ON RENUM = RLNUMT AND RLCOD = 'C'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT_EXTENSION
        + " ON CLETB = EBETB AND EBCLI = CLCLI AND EBLIV = CLLIV "
        + " WHERE CLETB = " + RequeteSql.formaterStringSQL(fletb) + " AND CLCLI = " + CLCLI + " AND CLLIV = " + CLLIV;
    // @formatter:on
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    // Notre client existe bien dans Série N -> on récupère les données
    if (liste != null && liste.size() == 1) {
      return liste.get(0);
    }
    else {
      if (liste == null || liste.isEmpty()) {
        majError("[GM_Export_Clients] retournerClientDB2() -> La liste est nulle ou vide : " + fletb + "/" + flcod + " -> "
            + queryManager.getMsgError());
      }
      else {
        majError("[GM_Export_Clients] retournerClientDB2() -> Plusieurs clients ont été trouvés (" + liste.size() + ") : " + fletb + "/"
            + flcod);
      }
      return null;
    }
  }
}
