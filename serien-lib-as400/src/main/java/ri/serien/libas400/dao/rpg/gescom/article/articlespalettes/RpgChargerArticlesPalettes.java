/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlespalettes;

import java.math.BigDecimal;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.CritereResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette.IdResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette.ListeResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette.ResultatRechercheArticlePalette;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

public class RpgChargerArticlesPalettes extends RpgBase {
  // Constantes
  private static final String PROGRAMME = "SVGVX0014";
  private static final int NOMBRE_LIGNE_PAR_PAGE = 15;
  
  /**
   * Charger le résultat de la recherche pour les articles palettes correspondants aux critères de recherche.
   * 
   * Cette méthode utilise un programme RPG pour lire les données. Le programme RPG peut traiter 15 articles à la fois,
   * il est appelé en boucle pour lire l'intégralité des données.
   * 
   * @param pSystemeManager Gestionnaire de connexion.
   * @param pCritere Critères de recherche.
   * @return Liste de résultat de recherche pour des articles palettes.
   */
  public ListeResultatRechercheArticlePalette chargerListeResultatRechercheArticlePalette(SystemeManager pSystemeManager,
      CritereResultatRechercheArticlePalette pCritere) {
    ListeResultatRechercheArticlePalette listeRechercheArticlePalette = new ListeResultatRechercheArticlePalette();
    
    // La boucle stoppe lorsque la taille de la liste résultat n'évolue plus ou au bout de 100 pages
    boolean continuer = true;
    int page = 1;
    do {
      continuer = chargerListe15ResultatRechercheArticlePalette(pSystemeManager, listeRechercheArticlePalette, pCritere, page);
    }
    while (continuer && page++ < 100);
    return listeRechercheArticlePalette;
  }
  
  /**
   * Charger le résultats de recherche pour les articles palettes correspondants aux critères de recherche.
   * 
   * Le programme RPG remonte les articles palettes 15 par 15.
   * 
   * @param pSystemeManager Gestionnaire de connexion.
   * @param pCritere Critères de recherche.
   * @param pNumeroPage Numéro de la page à charger.
   * @return true=pour continuer la recherche sur les pages suivantes, false=arrêter la recherche.
   */
  private boolean chargerListe15ResultatRechercheArticlePalette(SystemeManager pSystemeManager,
      ListeResultatRechercheArticlePalette pListeRechercheArticlePalette, CritereResultatRechercheArticlePalette pCritere,
      int pNumeroPage) {
    // Contrôler les paramètres
    if (pSystemeManager == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pCritere == null) {
      throw new MessageErreurException("Les critères de recherche sont invalides.");
    }
    IdClient.controlerId(pCritere.getIdClient(), true);
    
    // Préparation des paramètres du programme RPG
    Svgvx0014i entree = new Svgvx0014i();
    Svgvx0014o sortie = new Svgvx0014o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Renseigner les paramètres de recherche
    if (pCritere.getIdEtablissement() != null) {
      entree.setPietb(pCritere.getIdEtablissement().getCodeEtablissement());
    }
    if (pCritere.getIdMagasin() != null) {
      entree.setPimag(pCritere.getIdMagasin().getCode());
    }
    if (pCritere.getIdClient() != null) {
      entree.setPicli(pCritere.getIdClient().getNumero());
      entree.setPiliv(pCritere.getIdClient().getSuffixe());
    }
    if (pCritere.getIdDocumentVente() != null) {
      entree.setPicod(pCritere.getIdDocumentVente().getEntete());
      entree.setPinum(pCritere.getIdDocumentVente().getNumero());
      entree.setPisuf(pCritere.getIdDocumentVente().getSuffixe());
    }
    if (pCritere.getIdChantier() != null) {
      entree.setPichan(pCritere.getIdChantier().getNumero());
    }
    entree.setPiarg(pCritere.getTexteRecherche());
    entree.setPipag(pNumeroPage);
    entree.setPilig(NOMBRE_LIGNE_PAR_PAGE);
    
    // Initialiser les paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparer l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSystemeManager);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécuter le programme RPG
    if (!rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      return false;
    }
    
    // Tracer le nombre de résultats obtenus
    Trace.info("[RPG] " + sortie.getValeursBrutesLignes().length + " lignes");
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Renseigner le résultat
    Object[] listeResultatRPG = sortie.getValeursBrutesLignes();
    for (int i = 0; i < listeResultatRPG.length; i++) {
      // Récupérer la liste des champs de la ligne (nul signifie qu'on a atteint le dernier résultat)
      Object[] listeChampRPG = (Object[]) listeResultatRPG[i];
      if (listeChampRPG == null) {
        return false;
      }
      ResultatRechercheArticlePalette resultatRechercheArticlePalette = creerResultatRechercheArticlePalette(listeChampRPG);
      pListeRechercheArticlePalette.add(resultatRechercheArticlePalette);
    }
    
    return true;
  }
  
  /**
   * Créer un objet métier ResultatRechercheArticlePalette à partir d'une ligne de résultat RPG.
   * 
   * @param listeChampRPG Liste des chamsp RPG retournés par le service.
   * @return ResultatRechercheArticlePalette.
   */
  private ResultatRechercheArticlePalette creerResultatRechercheArticlePalette(Object[] listeChampRPG) {
    // Contrôler les paramètres
    if (listeChampRPG == null) {
      return null;
    }
    
    // Créer l'identifiant de l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) listeChampRPG[Svgvx0014d.VAR_WETB]);
    
    // Créer l'identifiant de l'article
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, (String) listeChampRPG[Svgvx0014d.VAR_WART]);
    
    // Créer l'identifiant de la recherche article palette
    IdResultatRechercheArticlePalette idResultatRechercheArticlePalette = IdResultatRechercheArticlePalette.getInstance(idArticle);
    
    // Créer le résultat
    ResultatRechercheArticlePalette resultatRechercheArticlePalette =
        new ResultatRechercheArticlePalette(idResultatRechercheArticlePalette);
    
    // Colonne 2 : libellé
    resultatRechercheArticlePalette.setLibelle1((String) listeChampRPG[Svgvx0014d.VAR_WLIB]);
    resultatRechercheArticlePalette.setLibelle2((String) listeChampRPG[Svgvx0014d.VAR_WLB1]);
    resultatRechercheArticlePalette.setLibelle3((String) listeChampRPG[Svgvx0014d.VAR_WLB2]);
    resultatRechercheArticlePalette.setLibelle4((String) listeChampRPG[Svgvx0014d.VAR_WLB3]);
    
    // Colonne 3 : stock disponible
    resultatRechercheArticlePalette.setQuantitePhysique(((BigDecimal) listeChampRPG[Svgvx0014d.VAR_WSTK]));
    
    // Colonne 4 : stock client
    // Le stock client est lu dans le stock client par chantier si le stock est géré par chantier.
    // Il est lu dans le stock client standard dans le cas contraire.
    Boolean stockPaletteParChantier = !((String) listeChampRPG[Svgvx0014d.VAR_WEBIN10]).trim().isEmpty();
    if (stockPaletteParChantier) {
      resultatRechercheArticlePalette.setQuantiteStockClient(((BigDecimal) listeChampRPG[Svgvx0014d.VAR_WSTKCLIC]));
    }
    else {
      resultatRechercheArticlePalette.setQuantiteStockClient(((BigDecimal) listeChampRPG[Svgvx0014d.VAR_WSTKCLI]));
    }
    
    // Colonne 5 : prix consignation
    resultatRechercheArticlePalette.setPrixConsignation(((BigDecimal) listeChampRPG[Svgvx0014d.VAR_WPRIXC]));
    
    // Colonne 6 : prix déconsignation
    resultatRechercheArticlePalette.setPrixDeconsignation(((BigDecimal) listeChampRPG[Svgvx0014d.VAR_WPRIXD]));
    
    return resultatRechercheArticlePalette;
  }
}
