/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.programs.doclies;

import java.util.ArrayList;

import ri.serien.libas400.dao.gvx.programs.article.GM_Article;
import ri.serien.libas400.dao.gvx.programs.article.GM_EaArticle;
import ri.serien.libas400.dao.gvx.programs.article.GM_EcArticle;
import ri.serien.libas400.dao.gvx.programs.article.M_Article;
import ri.serien.libas400.dao.gvx.programs.article.M_EaArticle;
import ri.serien.libas400.dao.gvx.programs.article.M_EcArticle;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class GM_DocumentLie extends BaseGroupDB {
  /**
   * Constructeur
   * @param pQuerymg
   */
  public GM_DocumentLie(QueryManager pQuerymg) {
    super(pQuerymg);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne un enregistrement des documents liés.
   * @param pCodeDoc
   * @param pCodeFic
   * @return
   * 
   */
  public M_DocumentLie readOneLink(String pCodeDoc, String pCodeFic) {
    if (pCodeDoc == null) {
      msgErreur += "\nLe champ PDDOC est à null.";
      return null;
    }
    if (pCodeFic == null) {
      msgErreur += "\nLe champ PDFIC est à null.";
      return null;
    }
    if (queryManager.getLibrary() == null) {
      msgErreur += "\nLa CURLIB n'est pas initialisée.";
      return null;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select * from " + queryManager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_LIE_PARAMETRE + " where ");
    requeteSql.ajouterConditionAnd("PDDOC", "=", pCodeDoc);
    requeteSql.ajouterConditionAnd("PDFIC", "=", pCodeFic);
    ArrayList<GenericRecord> listrcd = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (listrcd == null) {
      return null;
    }
    M_DocumentLie dl = new M_DocumentLie(queryManager);
    dl.initObject(listrcd.get(0), true);
    
    return dl;
  }
  
  /**
   * Retourne le chemin partie commune (partie spécifique VEXPD2).
   * @param pCodeEtablissement
   * @param pCodeDoc
   * @param pCodeFic
   * @param pPath
   * @return
   * 
   */
  public M_DocumentLie getPathCommon(String pCodeEtablissement, String pCodeDoc, String pCodeFic, StringBuffer pPath) {
    if (pCodeEtablissement == null || pCodeEtablissement.trim().isEmpty()) {
      msgErreur += "\nL'établissement est à null ou vide.";
      return null;
    }
    if (pCodeDoc == null) {
      msgErreur += "\nLe champ PDDOC est à null.";
      return null;
    }
    if (pCodeFic == null) {
      msgErreur += "\nLe champ PDFIC est à null.";
      return null;
    }
    
    M_DocumentLie dl = readOneLink(pCodeDoc, pCodeFic);
    if (dl == null) {
      return null;
    }
    
    // Traite la racine
    String chaine = getRoot(dl.getPDCHM0());
    if (chaine == null) {
      return null;
    }
    pPath.append(chaine);
    
    // Traite la curlib
    if (dl.getPDIN1() == '1') {
      if (pPath.charAt(pPath.length() - 1) != '\\' && pPath.charAt(pPath.length() - 1) != '/') {
        pPath.append('/');
      }
      pPath.append(queryManager.getLibrary());
    }
    
    // Traite l'établissement
    if (dl.getPDIN2() == '1') {
      if (pPath.charAt(pPath.length() - 1) != '\\' && pPath.charAt(pPath.length() - 1) != '/') {
        pPath.append('/');
      }
      pPath.append(cleanString(pCodeEtablissement));
    }
    
    // Traite le type de la fiche
    if (dl.getPDIN3() == '1') {
      if (pPath.charAt(pPath.length() - 1) != '\\' && pPath.charAt(pPath.length() - 1) != '/') {
        pPath.append('/');
      }
      pPath.append(cleanString(pCodeFic));
    }
    
    // Traite le type du document
    if ((dl.getPDIN4() == '1') || (pCodeDoc.equalsIgnoreCase("PHO"))) {
      if (pPath.charAt(pPath.length() - 1) != '\\' && pPath.charAt(pPath.length() - 1) != '/') {
        pPath.append('/');
      }
      pPath.append(cleanString(pCodeDoc));
    }
    
    return dl;
  }
  
  /**
   * Retourne le chemin pour les articles.
   * @param pCodeEtablissement
   * @param pA1art
   * @param pCodeDoc
   * @param pCodeFic
   * @return
   * 
   */
  public String getPathArticles(String pCodeEtablissement, String pA1art, String pCodeDoc, String pCodeFic) {
    if (pA1art == null || pA1art.trim().isEmpty()) {
      msgErreur += "\nLe code article est à null ou vide.";
      return null;
    }
    
    StringBuffer path = new StringBuffer();
    M_DocumentLie dl = getPathCommon(pCodeEtablissement, pCodeDoc, pCodeFic, path);
    if (dl == null) {
      return null;
    }
    
    // Traite les zones variables
    String chaine = getValueArticle(pCodeEtablissement, pA1art, dl.getPDCHM1());
    if (chaine.trim().length() > 0) {
      path.append('/').append(cleanString(chaine));
    }
    chaine = getValueArticle(pCodeEtablissement, pA1art, dl.getPDCHM2());
    if (chaine.trim().length() > 0) {
      path.append('/').append(cleanString(chaine));
    }
    chaine = getValueArticle(pCodeEtablissement, pA1art, dl.getPDCHM3());
    if (chaine.trim().length() > 0) {
      path.append('/').append(cleanString(chaine));
    }
    chaine = getValueArticle(pCodeEtablissement, pA1art, dl.getPDCHM4());
    if (chaine.trim().length() > 0) {
      path.append('/').append(cleanString(chaine));
    }
    chaine = getValueArticle(pCodeEtablissement, pA1art, dl.getPDCHM5());
    if (chaine.trim().length() > 0) {
      path.append('/').append(cleanString(chaine));
    }
    
    if (Constantes.isWindows()) {
      chaine = path.toString().replace('/', '\\');
    }
    else {
      chaine = path.toString();
    }
    
    return chaine;
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne la racine du chemin (en fonction du type d'OS).
   * @param pValeur
   * @return
   */
  private String getRoot(String pValeur) {
    pValeur = Constantes.normerTexte(pValeur);
    if (pValeur.isEmpty()) {
      return pValeur;
    }
    
    String tab[] = pValeur.split(";");
    // Un point virgule a été trouvé signifiant qu'il y a 2 racines : 1 windows et 1 IFS
    if (tab.length == 2) {
      // Retourne la racine IFS
      if (Constantes.isOs400()) {
        return Constantes.normerTexte(tab[1]);
      }
      // Retourne la racine Windows
      return Constantes.normerTexte(tab[0]);
    }
    
    // Sinon retourne la valeur telle quelle
    return pValeur;
  }
  
  /**
   * Retourne pour un article la valeur du paramètre (partie spécifique VEXPDART).
   * @param pCodeEtablissement
   * @param pCode
   * @param pKey
   * @return
   * 
   */
  private String getValueArticle(String pCodeEtablissement, String pCode, String pKey) {
    if (pKey == null || pKey.trim().isEmpty()) {
      return "";
    }
    
    int indice = Integer.parseInt(pKey.substring(1).trim());
    
    // Pour le fichier PGVMEEAM
    if (indice == 2 || indice == 3 || indice == 4) {
      GM_EaArticle geaart = new GM_EaArticle(queryManager);
      M_EaArticle eaart = geaart.readOneArticle(pCodeEtablissement, pCode);
      if (eaart == null) {
        return "";
      }
      switch (indice) {
        case 2:
          return eaart.getA1LB1();
        case 3:
          return eaart.getA1LB2();
        case 4:
          return eaart.getA1LB3();
      }
    }
    
    // Pour le fichier PGVMECAM
    if ((indice > 70) && (indice <= 88)) {
      GM_EcArticle gecart = new GM_EcArticle(queryManager);
      M_EcArticle ecart = gecart.readOneArticle(pCodeEtablissement, pCode);
      if (ecart == null) {
        return "";
      }
      int index = indice - 70;
      return ecart.getEBZPX(index);
    }
    
    // Pour le fichier PGVMARTM
    GM_Article gart = new GM_Article(queryManager);
    M_Article art = gart.readOneArticle(pCodeEtablissement, pCode);
    if (art == null) {
      return "";
    }
    switch (indice) {
      case 1:
        return art.getA1LIB();
      case 5:
        return art.getA1CL1();
      case 6:
        return art.getA1CL2();
      case 7:
        return art.getA1FAM();
      case 10:
        return art.getA1ART();
      case 11:
        return art.getA1UNS();
      case 48:
        if (art.getA1GCD() > 0) {
          return "" + art.getA1GCD();
        }
        break;
      case 70:
        return "" + art.getA1IN7();
    }
    return "";
  }
  
  /**
   * Remplace les caractères inapropriés par un underscore.
   * @param pChaine
   * @return
   */
  private String cleanString(String pChaine) {
    char[] listchar = { ' ', '/', '\\', ':', '*', '?', '"', '<', '>', '|' };
    boolean trouve = false;
    
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < pChaine.length(); i++) {
      trouve = false;
      for (int c = 0; c < listchar.length; c++) {
        if (pChaine.charAt(i) == listchar[c]) {
          sb.append('_');
          trouve = true;
          break;
        }
      }
      if (!trouve) {
        sb.append(pChaine.charAt(i));
      }
    }
    
    return sb.toString();
  }
  
}
