/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.serien.libas400.dao.exp.programs.contact.M_Contact;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class PsemrtemManager extends QueryManager {
  /**
   * Constructeur
   * @param database
   */
  public PsemrtemManager(Connection database) {
    super(database);
  }
  
  /**
   * Retourne la liste des contacts
   * @param curlib
   * @return
   * 
   */
  public ArrayList<GenericRecord> getTousEnregistrements(String curlib) {
    try {
      return select("Select * from " + curlib + ".psemrtem");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
  }
  
  /**
   * Retourne les informations primordiales des contacts
   * @param curlib
   * @return
   * 
   */
  public ArrayList<GenericRecord> getCivNomPrenom(String curlib) {
    try {
      return select("Select RENUM, REETB, RECIV, REPAC, RENOM, REPRE from " + curlib + ".psemrtem");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
  }
  
  /**
   * Retourne une liste des civilités uniques
   * @param curlib
   * @return
   * 
   */
  public ArrayList<GenericRecord> getToutesLescivilites(String curlib) {
    try {
      return select("Select distinct reciv from " + curlib + ".psemrtem order by reciv asc");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
  }
  
  /**
   * Retourne la liste des établissements des contacts disponibles
   * @param curlib
   * @return
   * 
   */
  public ArrayList<GenericRecord> getTousLesEtablissements(String curlib) {
    try {
      return select("Select distinct reetb from " + curlib + ".psemrtem");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
  }
  
  /**
   * Retourne le generic record du contact à partir de l'établissement et du profil
   * @param curlib
   * @param etb
   * @return
   * 
   */
  public GenericRecord getGRContact(String curlib, String etb, String profil) {
    if ((etb == null) || (profil == null)) {
      return null;
    }
    
    ArrayList<GenericRecord> liste;
    try {
      liste = select("Select * from " + curlib + ".psemrtem where REETB='" + etb + "' and REPRF='" + profil + "'");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
    if ((liste != null) && !liste.isEmpty()) {
      return liste.get(0);
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourne le contact à partir de l'établissement et du profil
   * @param curlib
   * @param etb
   * @param profil
   * @return
   * 
   */
  public M_Contact getContact(String curlib, String etb, String profil) {
    GenericRecord record = getGRContact(curlib, etb, profil);
    if (record == null) {
      return null;
    }
    
    M_Contact contact = new M_Contact(this);
    record.toObject(contact);
    
    return contact;
  }
  
  // --> Méthodes privées <---------------------------------------------------
  
}
