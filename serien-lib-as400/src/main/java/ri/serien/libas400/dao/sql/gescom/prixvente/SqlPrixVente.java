/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.prixvente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.referencetarif.IdReferenceTarif;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.IdTypeGratuit;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.EnumOriginePrixVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVenteBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.CritereParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.EnumOriginePrv;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.EnumOriginePump;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.EnumTypePrv;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.ParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.EnumTypeChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.ListeParametreChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.ParametreChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.EnumRegleExclusionConditionVenteEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.ParametreClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.EnumCategorieConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.EnumTypeConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.EnumTypeRattachementArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.IdRattachementArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.IdRattachementClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.InformationConditionVenteLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ListeParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumModeTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.ParametreDocumentVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceCoefficient;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumTypeApplicationConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.EnumGraduationDecimaleTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Cette classe permet de regrouper les accès en base nécessaires au calcul du prix d'un article.
 * 
 * Pour les chantiers, le code chantier est ici PGVMXLIM , XITYP = 40, XINLI = 0
 */
public class SqlPrixVente {
  // Variables
  private QueryManager querymanager = null;
  
  /**
   * Constructeur.
   */
  public SqlPrixVente(QueryManager pQueryManager) {
    querymanager = pQueryManager;
  }
  
  // -- Méthodes publiques
  
  /**
   * Charge l'entête d'un document de vente à partir de son identifiant.
   * 
   * @param pIdDocumentVente
   * @return
   */
  public ParametreDocumentVente chargerEnteteDocumentVente(IdDocumentVente pIdDocumentVente) {
    if (pIdDocumentVente == null) {
      throw new MessageErreurException("L'identifiant du document de vente est invalide.");
    }
    if (!pIdDocumentVente.isExistant()) {
      throw new MessageErreurException("L'identifiant du document de vente n'existe pas en base.");
    }
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter(""
        + " select"
        + "   E1ETB,"
        + "   E1NFA,"
        + "   E1COD,"
        + "   E1NUM,"
        + "   E1SUF,"
        + "   E1CNV,"
        + "   E1CC1,"
        + "   E1ETA,"
        + "   E1TAR,"
        + "   E1REM1,"
        + "   E1REM2,"
        + "   E1REM3,"
        + "   E1REM4,"
        + "   E1REM5,"
        + "   E1REM6,"
        + "   E1TRL,"
        + "   E1BRL,"
        + "   E1CRE,"
        + "   E1DEV,"
        + "   E1CLLP,"
        + "   E1CLLS,"
        + "   E1CLFP,"
        + "   E1CLFS,"
        + "   E1TVA1,"
        + "   E1TVA2,"
        + "   E1TVA3,"
        + "   E1TV1,"
        + "   E1TV2,"
        + "   E1TV3,"
        + "   E1TFA,"
        + "   E1NAT,"
        + "   E1TO1G,"
        + "   XILIB as NUMCHANTIER" + " from " + querymanager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE
        + " left outer join " + querymanager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_EXTENSION
        + " on  XICOD = E1COD" + " and XIETB = E1ETB"
        + " and XINUM = E1NUM"
        + " and XISUF = E1SUF"
        + " and XITYP = 40"
        + " where");
    requeteSql.ajouterConditionAnd("E1ETB", "=", pIdDocumentVente.getCodeEtablissement());
    if (pIdDocumentVente.isIdFacture()) {
      requeteSql.ajouterConditionAnd("E1NFA", "=", pIdDocumentVente.getNumeroFacture());
    }
    else {
      requeteSql.ajouterConditionAnd("E1COD", "=", pIdDocumentVente.getCodeEntete().getCode());
      requeteSql.ajouterConditionAnd("E1NUM", "=", pIdDocumentVente.getNumero());
      requeteSql.ajouterConditionAnd("E1SUF", "=", pIdDocumentVente.getSuffixe());
    }
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymanager.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Contrôle qu'il n'y ait pas plus d'un enregistrement
    if (listeGenericRecord.size() > 1) {
      throw new MessageErreurException("Plusieurs documents de ventes ont été trouvé avec l'identifiant " + pIdDocumentVente);
    }
    // Construire le résultat
    return completerParametreDocumentVente(listeGenericRecord.get(0));
  }
  
  /**
   * Charge une ligne de vente à partir de son identifiant.
   * 
   * @param pIdLigneVente
   * @return
   */
  public ParametreLigneVente chargerLigneVente(IdLigneVente pIdLigneVente) {
    if (pIdLigneVente == null) {
      throw new MessageErreurException("L'identifiant de la ligne de vente est invalide.");
    }
    if (!pIdLigneVente.isExistant()) {
      throw new MessageErreurException("L'identifiant de la ligne de vente n'existe pas en base.");
    }
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter(""
        + " select"
        + "   L1TOP,"
        + "   L1ETB,"
        + "   L1COD,"
        + "   L1NUM,"
        + "   L1SUF,"
        + "   L1NLI,"
        + "   L1ART,"
        + "   L1TT,"
        + "   L1TAR,"
        + "   L1TB,"
        + "   L1PVB,"
        + "   L1TC,"
        + "   L1COE,"
        + "   L1TR,"
        + "   L1REM1,"
        + "   L1REM2,"
        + "   L1REM3,"
        + "   L1REM4,"
        + "   L1REM5,"
        + "   L1REM6,"
        + "   L1TRL,"
        + "   L1BRL,"
        + "   L1TN,"
        + "   L1PVN,"
        + "   L1PVC,"
        + "   L1IN2,"
        + "   L1IN18,"
        + "   L1IN19,"
        + "   L1UNV,"
        + "   L1CPL,"
        + "   L1MHT,"
        + "   L1COL,"
        + "   L1TVA,"
        + "   L1QTE,"
        + "   XILIB");
    requeteSql.ajouter(" from " + querymanager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_LIGNE);
    requeteSql.ajouter(" left outer join " + querymanager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_EXTENSION
            + " on  XICOD = L1COD"
            + " and XIETB = L1ETB"
            + " and XINUM = L1NUM"
            + " and XISUF = L1SUF"
            + " and XINLI = L1NLI"
            + " and XITYP = 6");
    requeteSql.ajouter(" where");
    requeteSql.ajouterConditionAnd("L1ETB", "=", pIdLigneVente.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("L1COD", "=", pIdLigneVente.getCodeEntete().getCode());
    requeteSql.ajouterConditionAnd("L1NUM", "=", pIdLigneVente.getNumero());
    requeteSql.ajouterConditionAnd("L1SUF", "=", pIdLigneVente.getSuffixe());
    requeteSql.ajouterConditionAnd("L1NLI", "=", pIdLigneVente.getNumeroLigne());
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymanager.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Contrôle qu'il n'y ait pas plus d'un enregistrement
    if (listeGenericRecord.size() > 1) {
      throw new MessageErreurException("Plusieurs lignes de ventes ont été trouvé avec l'identifiant " + pIdLigneVente);
    }
    // Construire le résultat
    return completerParametreLigneVente(listeGenericRecord.get(0));
  }
  
  /**
   * Charge les lignes de ventes du document.
   * 
   * @param pIdDocumentVente
   * @param pIdLigneVenteExclu
   * @return
   */
  public List<ParametreLigneVente> chargerListeParametreLigneVente(IdDocumentVente pIdDocumentVente, IdLigneVente pIdLigneVenteExclu) {
    if (pIdDocumentVente == null) {
      throw new MessageErreurException("L'identifiant du document de vente est invalide.");
    }
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter(""
        + " select"
        + "   L1TOP,"
        + "   L1ETB,"
        + "   L1COD,"
        + "   L1NUM,"
        + "   L1SUF,"
        + "   L1NLI,"
        + "   L1ART,"
        + "   L1TT,"
        + "   L1TAR,"
        + "   L1TB,"
        + "   L1PVB,"
        + "   L1TC,"
        + "   L1COE,"
        + "   L1TR,"
        + "   L1REM1,"
        + "   L1REM2,"
        + "   L1REM3,"
        + "   L1REM4,"
        + "   L1REM5,"
        + "   L1REM6,"
        + "   L1TRL,"
        + "   L1BRL,"
        + "   L1TN,"
        + "   L1PVN,"
        + "   L1PVC,"
        + "   L1IN2,"
        + "   L1IN18,"
        + "   L1IN19,"
        + "   L1UNV,"
        + "   L1CPL,"
        + "   L1MHT,"
        + "   L1COL,"
        + "   L1TVA,"
        + "   L1QTE"
        + " from " + querymanager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_LIGNE + " where");
    requeteSql.ajouterConditionAnd("L1ETB", "=", pIdDocumentVente.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("L1COD", "=", pIdDocumentVente.getCodeEntete().getCode());
    requeteSql.ajouterConditionAnd("L1NUM", "=", pIdDocumentVente.getNumero());
    requeteSql.ajouterConditionAnd("L1SUF", "=", pIdDocumentVente.getSuffixe());
    if (pIdLigneVenteExclu != null && pIdLigneVenteExclu.isExistant()) {
      requeteSql.ajouterConditionAnd("L1NLI", "<>", pIdLigneVenteExclu.getNumeroLigne());
    }
    // Ne pas récupérer les remises spéciales
    requeteSql.ajouterConditionAnd("L1NLI", "<=", LigneVenteBase.NUMERO_LIGNE_POUR_REMISE_SPECIALE);
    requeteSql.ajouterConditionAnd("L1ERL", "=", 'C');
    requeteSql.ajouterConditionAnd("L1CEX", "<>", 'A');
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymanager.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    List<ParametreLigneVente> listeParametreLigneVente = new ArrayList<ParametreLigneVente>();
    for (GenericRecord record : listeGenericRecord) {
      listeParametreLigneVente.add(completerParametreLigneVente(record));
    }
    return listeParametreLigneVente;
  }
  
  /**
   * Charge un article à partir de son identifiant.
   * Il s'agit de la méthode utilisée pour charger un article dont il faut calculer le prix de vente.
   * 
   * @param pCritereParametreArticle
   * @return
   */
  public ParametreArticle chargerArticle(CritereParametreArticle pCritereParametreArticle) {
    if (pCritereParametreArticle == null) {
      throw new MessageErreurException("Les critères de recherche du paramètre article sont invalides.");
    }
    if (pCritereParametreArticle.getIdArticle() == null) {
      throw new MessageErreurException("L'identifiant de l'article est invalide.");
    }
    if (!pCritereParametreArticle.getIdArticle().isExistant()) {
      throw new MessageErreurException("L'identifiant de l'article n'existe pas en base.");
    }
    
    // Le PS124 indique si le PUMP et le PRV sont gérés par établissement ou par magasin
    boolean chargerParEtablissement = false;
    String codeMagasin = null;
    Boolean ps124 = pCritereParametreArticle.getPs124();
    // Si le PS124 est actif et si la table dédiée existe alors il faut récupérer le PUMP par établissement
    if (ps124 != null && ps124 && querymanager.isTableExiste(EnumTableBDD.HISTORIQUE_MONOPUMP)) {
      chargerParEtablissement = true;
    }
    // Sinon c'est le PUMP par magasin qui est chargé
    else {
      if (pCritereParametreArticle.getIdMagasin() != null) {
        chargerParEtablissement = false;
        codeMagasin = pCritereParametreArticle.getIdMagasin().getCode();
      }
    }
    boolean modeNegoce = false;
    if (pCritereParametreArticle.getModeNegoce() != null && pCritereParametreArticle.getModeNegoce()) {
      modeNegoce = true;
    }
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter(""
        + " select"
        + "   A1ETB,"
        + "   A1ART,"
        + "   A1PRV,"
        // + " A1PRS,"
        + "   A1COF,"
        + "   A1FRS,"
        + "   A1CNV,"
        + "   A1CNV1,"
        + "   A1CNV2,"
        + "   A1CNV3,"
        + "   A1UNV,"
        + "   A1FAM,"
        + "   A1SFA,"
        + "   A1TVA,"
        + "   A1TPR,"
        + "   A1RTA,"
        + "   A1PMR,"
        + "   A1TSP,"
        + "   CAPRS,"
        + "   CARGA,"
        + "   H1PMP,"
        + "   H1PRV");
    if (chargerParEtablissement) {
      requeteSql.ajouter(""
          + ","
          + " HMPMP,"
          + " HMPRV");
    }
    requeteSql.ajouter(""
        + " from " + querymanager.getLibrary() + '.' + EnumTableBDD.ARTICLE
        + " left outer join " + querymanager.getLibrary() + '.' + EnumTableBDD.CONDITION_ACHAT
        + " on  CATOP = 0"
        + " and CAETB = A1ETB"
        + " and CAART = A1ART"
        + " and CACOL = A1COF"
        + " and CAFRS = A1FRS"
        + " left outer join " + querymanager.getLibrary() + '.' + EnumTableBDD.STOCK_HISTORIQUE
        + " on  H1TOP = 0"
        + " and H1ETB = A1ETB"
        + " and H1ART = A1ART"
        + " and H1MDP = '1'"
        + " and H1MAG = " + RequeteSql.formaterStringSQL(codeMagasin));
    if (chargerParEtablissement) {
      requeteSql.ajouter(""
          + " left outer join " + querymanager.getLibrary() + '.' + EnumTableBDD.HISTORIQUE_MONOPUMP
          + " on  HMTOP = 0"
          + " and HMETB = A1ETB"
          + " and HMART = A1ART");
    }
    requeteSql.ajouter(""
        + " where");
    requeteSql.ajouterConditionAnd("A1ETB", "=", pCritereParametreArticle.getIdArticle().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("A1ART", "=", pCritereParametreArticle.getIdArticle().getCodeArticle());
    requeteSql.ajouter("order by"
        + " CADAP desc,"
        + " H1DAT desc");
    if (chargerParEtablissement) {
      requeteSql.ajouter(", HMDAT desc");
    }
    requeteSql.ajouter(" fetch first 1 rows only optimize for 1 rows");
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymanager.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Contrôle qu'il n'y ait pas plus d'un enregistrement
    if (listeGenericRecord.size() > 1) {
      throw new MessageErreurException("Plusieurs articles ont été trouvé avec l'identifiant " + pCritereParametreArticle.getIdArticle());
    }
    // Construire le résultat
    return completerParametreArticle(listeGenericRecord.get(0), chargerParEtablissement, modeNegoce);
  }
  
  /**
   * Charge une liste d'articles à partir de son identifiant pour un magasin donné.
   * Utilisé pour cumuler la quantité d'articles d'un document de vente.
   * 
   * @param pIdArticle
   * @param pIdMagasin
   * @return
   */
  public Map<String, ParametreArticle> chargerListeArticle(List<IdArticle> pListeIdArticle, IdMagasin pIdMagasin, boolean pModeNegoce) {
    if (pListeIdArticle == null || pListeIdArticle.isEmpty()) {
      throw new MessageErreurException("La liste des identifiants articles est invalide.");
    }
    // Cas où l'identifiant du magasin n'est pas renseigné
    boolean parEtablissement = true;
    String codeMagasin = "";
    if (pIdMagasin != null) {
      codeMagasin = pIdMagasin.getCode();
      parEtablissement = false;
    }
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter(""
        + " select"
        + "   A1ETB,"
        + "   A1ART,"
        + "   A1PRV,"
        // + " A1PRS,"
        + "   A1COF,"
        + "   A1FRS,"
        + "   A1CNV,"
        + "   A1CNV1,"
        + "   A1CNV2,"
        + "   A1CNV3,"
        + "   A1UNV,"
        + "   A1FAM,"
        + "   A1SFA,"
        + "   A1TVA,"
        + "   A1TPR,"
        + "   A1RTA,"
        + "   A1TSP,"
        + "   CAPRS,"
        + "   CARGA,"
        + "   H1PMP,"
        + "   H1PRV");
    requeteSql.ajouter(""
        + " from " + querymanager.getLibrary() + '.' + EnumTableBDD.ARTICLE
        + " left outer join " + querymanager.getLibrary() + '.' + EnumTableBDD.CONDITION_ACHAT
        + " on  CATOP = 0"
        + " and CAETB = A1ETB"
        + " and CAART = A1ART"
        + " and CACOL = A1COF"
        + " and CAFRS = A1FRS"
        + " left outer join " + querymanager.getLibrary() + '.' + EnumTableBDD.STOCK_HISTORIQUE
        + " on  H1TOP = 0"
        + " and H1ETB = A1ETB"
        + " and H1ART = A1ART"
        + " and H1MDP = '1'"
        + " and H1MAG = " + RequeteSql.formaterStringSQL(codeMagasin));
    requeteSql.ajouter(""
        + " where");
    requeteSql.ajouterConditionAnd("A1ETB", "=", pListeIdArticle.get(0).getCodeEtablissement());
    requeteSql.ajouter(" and (");
    for (IdArticle idArticle : pListeIdArticle) {
      requeteSql.ajouterConditionOr("A1ART", "=", idArticle.getCodeArticle());
    }
    requeteSql.ajouter(")");
    requeteSql.ajouter("order by"
        + " CADAP desc,"
        + " H1DAT desc");
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymanager.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Construire le résultat
    Map<String, ParametreArticle> mapParametreArticle = new HashMap<String, ParametreArticle>();
    for (GenericRecord record : listeGenericRecord) {
      ParametreArticle parametreArticle = completerParametreArticle(record, parEtablissement, pModeNegoce);
      if (parametreArticle == null) {
        continue;
      }
      mapParametreArticle.put(parametreArticle.getCodeArticle(), parametreArticle);
    }
    return mapParametreArticle;
  }
  
  /**
   * Charge un tarif article.
   * 
   * @param pIdArticle
   * @param pDateApplication
   * @param pCodeDevise
   * @return
   */
  public ParametreTarif chargerTarif(IdArticle pIdArticle, Date pDateApplication, String pCodeDevise) {
    if (pIdArticle == null) {
      throw new MessageErreurException("L'identifiant de l'article est invalide.");
    }
    if (pDateApplication == null) {
      throw new MessageErreurException("La date d'application est invalide.");
    }
    if (pCodeDevise == null) {
      throw new MessageErreurException("Le code devise est invalide.");
    }
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter(""
        + " select"
        + "   ATETB,"
        + "   ATART,"
        + "   ATRON,"
        + "   ATK01,"
        + "   ATK02,"
        + "   ATK03,"
        + "   ATK04,"
        + "   ATK05,"
        + "   ATK06,"
        + "   ATK07,"
        + "   ATK08,"
        + "   ATK09,"
        + "   ATK10,"
        + "   ATP01,"
        + "   ATP02,"
        + "   ATP03,"
        + "   ATP04,"
        + "   ATP05,"
        + "   ATP06,"
        + "   ATP07,"
        + "   ATP08,"
        + "   ATP09,"
        + "   ATP10,"
        + "   ATDAP"
        + " from " + querymanager.getLibrary() + '.' + EnumTableBDD.ARTICLE_TARIF
        + " where");
    requeteSql.ajouterConditionAnd("ATETB", "=", pIdArticle.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("ATART", "=", pIdArticle.getCodeArticle());
    requeteSql.ajouterConditionAnd("ATDAP", "<=", ConvertDate.dateToDb2(pDateApplication));
    requeteSql.ajouterConditionAnd("ATDEV", "=", pCodeDevise);
    requeteSql.ajouter(" order by ATDAP desc fetch first 1 rows only optimize for 1 rows");
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymanager.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Contrôle qu'il n'y ait pas plus d'un enregistrement
    if (listeGenericRecord.size() > 1) {
      throw new MessageErreurException("Plusieurs tarifs ont été trouvé pour l'article " + pIdArticle);
    }
    // Construire le résultat
    return completerParametreTarif(listeGenericRecord.get(0));
  }
  
  /**
   * Charge un client.
   * 
   * La requête permet de charger à la fois les informations du client mais aussi les codes des groupes des conditions de ventes du client
   * référence si le cas est présent (le champ CLCNV doit contenir un nombre à 6 chiffres).
   * 
   * @param pIdClient
   * @return
   */
  public ParametreClient chargerClient(IdClient pIdClient) {
    if (pIdClient == null) {
      throw new MessageErreurException("L'identifiant du client est invalide.");
    }
    if (!pIdClient.isExistant()) {
      throw new MessageErreurException("L'identifiant du client n'existe pas en base.");
    }
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter(""
        + " select"
        + "   cli.CLETB,"
        + "   cli.CLCLI,"
        + "   cli.CLLIV,"
        + "   cli.CLCLFP,"
        + "   cli.CLCLFS,"
        + "   cli.CLTTC,"
        + "   cli.CLDEV,"
        + "   cli.CLTAR,"
        + "   cli.CLTA1,"
        + "   cli.CLREM1,"
        + "   cli.CLREM2,"
        + "   cli.CLREM3,"
        + "   cli.CLCNV,"
        + "   cli.CLCNP,"
        + "   cli.CLCNR,"
        + "   cli.CLCC1,"
        + "   cli.CLCC2,"
        + "   cli.CLCC3,"
        + "   cli.CLTFA,"
        + "   cli.CLIN4,"
        + "   cli.CLIN10,"
        + "   ref.clcnv as REFCLCNV,"
        + "   ref.clcnp as REFCLCNP"
        + " from " + querymanager.getLibrary() + '.' + EnumTableBDD.CLIENT + " as cli"
        + " left outer join " + querymanager.getLibrary() + '.' + EnumTableBDD.CLIENT + " as ref"
        + " on");
    requeteSql.ajouterConditionAnd("ref.CLETB", "=", "cli.CLETB", true, false);
    requeteSql.ajouterConditionAnd("ref.CLCLI", "=", "cli.CLCNV", true, false);
    requeteSql.ajouterConditionAnd("ref.CLLIV", "=", 0);
    requeteSql.ajouterConditionAnd("Length(Trim(cli.clcnv))", "=", 6);
    requeteSql.ajouter(""
        + " where");
    requeteSql.ajouterConditionAnd("cli.CLETB", "=", pIdClient.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("cli.CLCLI", "=", pIdClient.getNumero());
    requeteSql.ajouterConditionAnd("cli.CLLIV", "=", pIdClient.getSuffixe());
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymanager.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Contrôle qu'il n'y ait pas plus d'un enregistrement
    if (listeGenericRecord.size() > 1) {
      throw new MessageErreurException("Plusieurs clients ont été trouvé avec l'identifiant " + pIdClient);
    }
    // Construire le résultat
    return completerParametreClient(listeGenericRecord.get(0));
  }
  
  /**
   * Charge une liste de paramètres de conditions de ventes.
   * Le filtrage s'effectue sur l'établissement, le code de la devise et la plage de date, le code article, le groupe, la famille et la
   * sous-famille afin d'optimmiser les temps de chargement et d'avoir une requête relativement simple.
   * Note: les conditions de ventes quantitatives sont stockées dans une autre table nommée PGVMCNQM mais le premier enregistrement est
   * également stocké dans la table PGVMCNVM afin de servir de lien.
   * 
   * 
   * TODO faire une classe CritereParametreConditionVente
   * 
   * @param pIdEtablissement
   * @param pCodeDevise
   * @param pDateApplication
   * @param pParametreArticle
   * @param pPs253
   * @param pFiltrePourIgnorerNumeroClient
   * @return
   */
  public ListeParametreConditionVente chargerListeConditionVente(IdEtablissement pIdEtablissement, String pCodeDevise,
      Date pDateApplication, ParametreArticle pParametreArticle, boolean pPs253, String pFiltrePourIgnorerNumeroClient) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'identifiant de l'établissement est invalide.");
    }
    if (!pIdEtablissement.isExistant()) {
      throw new MessageErreurException("L'identifiant de l'établissement n'existe pas en base.");
    }
    if (pDateApplication == null) {
      throw new MessageErreurException("La date d'application est invalide.");
    }
    if (pParametreArticle == null) {
      throw new MessageErreurException("Les paramètres de l'article sont invalides.");
    }
    pCodeDevise = Constantes.normerTexte(pCodeDevise);
    String codeArticle = Constantes.normerTexte(pParametreArticle.getCodeArticle());
    String codeGroupe = Constantes.normerTexte(pParametreArticle.getGroupe());
    String codeFamille = Constantes.normerTexte(pParametreArticle.getFamille());
    String codeSousFamille = Constantes.normerTexte(pParametreArticle.getSousFamille());
    String codeFournisseur = null;
    if (pParametreArticle.getIdFournisseur() != null) {
      codeFournisseur = pParametreArticle.getIdFournisseur().getIndicatifCourt();
    }
    String codeRegroupementAchat = pParametreArticle.getCodeRegroupementAchat();
    String codeRattachementCNV = Constantes.normerTexte(pParametreArticle.getCodeRattachementCNV());
    String codeTarif = "";
    if (pParametreArticle.getIdReferenceTarif() != null) {
      codeTarif = pParametreArticle.getIdReferenceTarif().getCode();
    }
    
    // Convertir la date d'application en entier sous la forme SAAMMJJ afin de simplifier la comparaison des dates car elles peuvent aussi
    // contenir la valeur 0
    int dateApplication = ConvertDate.dateToDb2(pDateApplication);
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter(""
        + " select"
        // Champs de la table PGVMCNVM
        + "   cnv.T1TOP,"
        + "   cnv.T1CAT,"
        + "   cnv.T1CNV,"
        + "   cnv.T1TRA,"
        + "   cnv.T1RAT,"
        + "   cnv.T1QTE,"
        + "   cnv.T1TCD,"
        + "   cnv.T1VAL,"
        + "   cnv.T1REM1,"
        + "   cnv.T1REM2,"
        + "   cnv.T1REM3,"
        + "   cnv.T1REM4,"
        + "   cnv.T1REM5,"
        + "   cnv.T1REM6,"
        + "   cnv.T1COE,"
        + "   cnv.T1DTD,"
        + "   cnv.T1DTF,"
        + "   case cnv.T1DTF when 0 then 9999999 else cnv.T1DTF end as T1DateFin,"
        + "   cnv.T1FPR,"
        + "   cnv.T1DEV,"
        + "   cnv.T2TCD,"
        + "   cnv.T2VAL,"
        + "   cnv.T2REM1,"
        + "   cnv.T2REM2,"
        + "   cnv.T2REM3,"
        + "   cnv.T2REM4,"
        + "   cnv.T2REM5,"
        + "   cnv.T2REM6,"
        + "   cnv.T2COE,"
        + "   cnv.T2DTD,"
        + "   cnv.T2DTF,"
        + "   case cnv.T2DTF when 0 then 9999999 else cnv.T2DTF end as T2DateFin,"
        + "   cnv.T2FPR,"
        // Champs de la table PGVMCNQM
        + "   cnq.TQQTE,"
        + "   cnq.T1TCD as QT1TCD,"
        + "   cnq.T1VAL as QT1VAL,"
        + "   cnq.T1REM1 as QT1REM1,"
        + "   cnq.T1REM2 as QT1REM2,"
        + "   cnq.T1REM3 as QT1REM3,"
        + "   cnq.T1REM4 as QT1REM4,"
        + "   cnq.T1REM5 as QT1REM5,"
        + "   cnq.T1REM6 as QT1REM6,"
        + "   cnq.T1COE as QT1COE,"
        + "   cnq.T1DTD as QT1DTD,"
        + "   cnq.T1DTF as QT1DTF,"
        + "   case cnq.T1DTF when 0 then 9999999 else cnq.T1DTF end as QT1DateFin,"
        + "   cnq.T1FPR as QT1FPR,"
        + "   cnq.T2TCD as QT2TCD,"
        + "   cnq.T2VAL as QT2VAL,"
        + "   cnq.T2REM1 as QT2REM1,"
        + "   cnq.T2REM2 as QT2REM2,"
        + "   cnq.T2REM3 as QT2REM3,"
        + "   cnq.T2REM4 as QT2REM4,"
        + "   cnq.T2REM5 as QT2REM5,"
        + "   cnq.T2REM6 as QT2REM6,"
        + "   cnq.T2COE as QT2COE,"
        + "   cnq.T2DTD as QT2DTD,"
        + "   cnq.T2DTF as QT2DTF,"
        + "   case cnq.T2DTF when 0 then 9999999 else cnq.T2DTF end as QT2DateFin,"
        + "   cnq.T2FPR as QT2FPR"
        + " from " + querymanager.getLibrary() + '.' + EnumTableBDD.CONDITION_VENTE + " cnv "
        + " left outer join " + querymanager.getLibrary() + '.' + EnumTableBDD.CONDITION_VENTE_QUANTITATIVE + " cnq "
        + "   on cnq.T1TOP <> 9"
        + "   and cnq.T1ETB = cnv.T1ETB"
        + "   and cnq.T1CAT = cnv.T1CAT"
        + "   and cnq.T1CNV = cnv.T1CNV"
        + "   and cnq.T1TRA = cnv.T1TRA"
        + "   and cnq.T1RAT = cnv.T1RAT"
        + "   and cnq.T1DEV = cnv.T1DEV");
    requeteSql.ajouter(" and ((");
    requeteSql.ajouterConditionAnd("cnq.T1DTD", "<=", dateApplication);
    requeteSql.ajouterConditionAnd("case cnq.T1DTF when 0 then 9999999 else cnq.T1DTF end", ">=", dateApplication);
    requeteSql.ajouter(") OR (");
    requeteSql.ajouterConditionAnd("cnq.T2TCD", "<>", "");
    requeteSql.ajouterConditionAnd("cnq.T2DTD", "<=", dateApplication);
    requeteSql.ajouterConditionAnd("case cnq.T2DTF when 0 then 9999999 else cnq.T2DTF end", ">=", dateApplication);
    requeteSql.ajouter("))");
    requeteSql.ajouter(" where");
    // La valeur du top doit être égale à 0 (la valeur 1 du top est désormais obsolète, elle correspondait aux cnv quantitatives gérées
    // dans une table à part aujourd'hui)
    requeteSql.ajouterConditionAnd("cnv.T1TOP", "=", 0);
    requeteSql.ajouterConditionAnd("cnv.T1ETB", "=", pIdEtablissement.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("cnv.T1DEV", "=", pCodeDevise);
    // Permet d'éliminer les conditions de ventes sur le numéro du client (valeur en position 112 <> ' ' dans la dataarea PGVMSPEM)
    if (pFiltrePourIgnorerNumeroClient != null) {
      requeteSql.ajouterConditionAnd("cnv.T1CNV", "<>", pFiltrePourIgnorerNumeroClient);
    }
    // Elimine certains types de conditions de ventes qui n'ont rien à voir avec le calcul de prix
    // Avoir séparé
    requeteSql.ajouterConditionAnd("cnv.T1TCD", "<>", EnumTypeConditionVente.AVOIR_SEPARE.getCode());
    requeteSql.ajouterConditionAnd("cnv.T2TCD", "<>", EnumTypeConditionVente.AVOIR_SEPARE.getCode());
    // Article interdit
    requeteSql.ajouterConditionAnd("cnv.T1TCD", "<>", EnumTypeConditionVente.ARTICLE_INTERDIT.getCode());
    requeteSql.ajouterConditionAnd("cnv.T2TCD", "<>", EnumTypeConditionVente.ARTICLE_INTERDIT.getCode());
    // Filtrage sur les catégories "Normales" et "Quantitatives"
    requeteSql.ajouter(" and (");
    requeteSql.ajouterConditionOr("cnv.T1CAT", "=", EnumCategorieConditionVente.NORMALE.getCode());
    requeteSql.ajouterConditionOr("cnv.T1CAT", "=", EnumCategorieConditionVente.QUANTITATIVE.getCode());
    requeteSql.ajouter(") ");
    // La gestion des dates dans la table est merdique donc si T1DTF ou T2DTF = 0 alors la valeur 9999999 est forcée pour que
    // la comparaison soit possible
    requeteSql.ajouter(" and ((");
    requeteSql.ajouterConditionAnd("cnv.T1DTD", "<=", dateApplication);
    requeteSql.ajouterConditionAnd("case cnv.T1DTF when 0 then 9999999 else cnv.T1DTF end", ">=", dateApplication);
    requeteSql.ajouter(") OR (");
    requeteSql.ajouterConditionAnd("cnv.T2TCD", "<>", "");
    requeteSql.ajouterConditionAnd("cnv.T2DTD", "<=", dateApplication);
    requeteSql.ajouterConditionAnd("case cnv.T2DTF when 0 then 9999999 else cnv.T2DTF end", ">=", dateApplication);
    requeteSql.ajouter("))");
    // Filtrage sur les types de rattachement et les rattachements associés
    requeteSql.ajouter(" and ((");
    // Rattachement article
    requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.ARTICLE.getCode());
    requeteSql.ajouterConditionAnd("cnv.T1RAT", "=", codeArticle);
    requeteSql.ajouter(") or (");
    if (pPs253) {
      // Rattachement regroupement d'achats
      if (codeRegroupementAchat != null) {
        requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.REGROUPEMENT_ACHAT.getCode());
        requeteSql.ajouterConditionAnd("cnv.T1RAT", "=", Constantes.normerTexte(codeRegroupementAchat));
        requeteSql.ajouter(") or (");
      }
      // Rattachement fournisseur
      if (codeFournisseur != null) {
        requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.REGROUPEMENT_FOURNISSEUR.getCode());
        requeteSql.ajouterConditionAnd("cnv.T1RAT", "=", Constantes.normerTexte(codeFournisseur));
        requeteSql.ajouter(") or (");
      }
    }
    // Rattachement code CNV
    if (!codeRattachementCNV.isEmpty()) {
      requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.REGROUPEMENT_CNV.getCode());
      requeteSql.ajouterConditionAnd("cnv.T1RAT", "=", codeRattachementCNV);
      requeteSql.ajouter(") or (");
    }
    // Rattachement tarif
    if (!codeTarif.isEmpty()) {
      requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.TARIF.getCode());
      requeteSql.ajouterConditionAnd("cnv.T1RAT", "=", codeTarif);
      requeteSql.ajouter(") or (");
    }
    // Rattachement sous-famille
    requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.SOUS_FAMILLE.getCode());
    requeteSql.ajouterConditionAnd("cnv.T1RAT", "=", codeSousFamille);
    requeteSql.ajouter(") or (");
    // Rattachement famille
    requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.FAMILLE.getCode());
    requeteSql.ajouterConditionAnd("cnv.T1RAT", "=", codeFamille);
    requeteSql.ajouter(") or (");
    // Rattachement groupe
    requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.GROUPE.getCode());
    requeteSql.ajouterConditionAnd("cnv.T1RAT", "=", codeGroupe);
    requeteSql.ajouter(") or (");
    // Rattachement global : les conditions emboitées
    requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.EMBOITEE_OU_GLOBALE.getCode());
    requeteSql.ajouterConditionAnd("cnv.T1RAT", "<>", "");
    requeteSql.ajouter(") or (");
    // Rattachement global : les conditions qui s'appliquent à tous les articles
    requeteSql.ajouterConditionAnd("cnv.T1TRA", "=", EnumTypeRattachementArticle.EMBOITEE_OU_GLOBALE.getCode());
    requeteSql.ajouterConditionAnd("cnv.T1RAT", "=", "");
    // Avec élimination des enregistrements foireux où le type de la condition n'est pas renseigné
    requeteSql.ajouter(" and (");
    requeteSql.ajouterConditionOr("cnv.T1TCD", "<>", "");
    requeteSql.ajouterConditionOr("cnv.T2TCD", "<>", "");
    requeteSql.ajouter(")");
    requeteSql.ajouter("))");
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymanager.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Construire le résultat
    ListeParametreConditionVente listeParametreConditionVente = new ListeParametreConditionVente();
    for (GenericRecord record : listeGenericRecord) {
      ParametreConditionVente parametreConditionVente = completerParametreConditionVente(record, pIdEtablissement, dateApplication);
      if (parametreConditionVente == null) {
        continue;
      }
      listeParametreConditionVente.add(parametreConditionVente);
    }
    
    if (listeParametreConditionVente.isEmpty()) {
      return null;
    }
    return listeParametreConditionVente;
  }
  
  /**
   * Charge une liste de paramètre chantier à partir de l'identifiant d'un chantier.
   * 
   * @param pIdChantier Identifiant chantier.
   * @return Liste de paramètres chantiers.
   */
  public ListeParametreChantier chargerChantier(IdChantier pIdChantier) {
    // Vérifier les pré-requis
    if (pIdChantier == null) {
      throw new MessageErreurException("L'identifiant du chantier est invalide.");
    }
    if (!pIdChantier.isExistant()) {
      throw new MessageErreurException("L'identifiant du chantier n'existe pas en base.");
    }
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter(""
        + " select"
        + "   L1ART,"
        + "   L1QTE,"
        + "   L1UNV,"
        + "   L1TAR,"
        + "   L1IN20,"
        + "   L1PVB,"
        + "   L1PVC"
        + " from " + querymanager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_LIGNE + " where");
    requeteSql.ajouterConditionAnd("L1ETB", "=", pIdChantier.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("L1COD", "=", pIdChantier.getCodeEntete().getCode());
    requeteSql.ajouterConditionAnd("L1NUM", "=", pIdChantier.getNumero());
    requeteSql.ajouterConditionAnd("L1SUF", "=", pIdChantier.getSuffixe());
    requeteSql.ajouter(" and (");
    requeteSql.ajouterConditionOr("L1ERL", "=", 'C');
    requeteSql.ajouterConditionOr("L1ERL", "=", 'S');
    requeteSql.ajouter(") ");
    requeteSql.ajouterConditionAnd("L1CEX", "<>", 'A');
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymanager.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    
    // Construire le résultat
    ListeParametreChantier listeParametreChantier = new ListeParametreChantier();
    for (GenericRecord record : listeGenericRecord) {
      ParametreChantier parametreChantier = completerParametreChantier(pIdChantier, record);
      listeParametreChantier.add(parametreChantier);
    }
    return listeParametreChantier;
  }
  
  /**
   * Sauver la ou les conditions de ventes utilisées dans le calcul du prix de ventes d'une ligne.
   */
  public void sauverConditionVenteLigneVente(IdLigneVente pIdLigneVente,
      List<InformationConditionVenteLigneVente> pListeInformationConditionVenteLigneVente) {
    // Contrôle l'identifiant de la ligne de ventes
    if (pIdLigneVente == null) {
      throw new MessageErreurException("L'identifiant de la ligne de ventes est invalide.");
    }
    // Contrôle que l'identifiant
    if (!pIdLigneVente.isExistant()) {
      throw new MessageErreurException("L'identifiant de la ligne de ventes n'existe pas en base.");
    }
    
    // Supprimer les lignes existantes pour la ligne de ventes (plus simple à gérer pour la mise à jour)
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("delete from " + querymanager.getLibrary() + '.' + EnumTableBDD.CONDITION_VENTE_LIGNE_VENTE + " where");
    requeteSql.ajouterConditionAnd("LCCOD", "=", pIdLigneVente.getCodeEntete().getCode());
    requeteSql.ajouterConditionAnd("LCETB", "=", pIdLigneVente.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("LCNUM", "=", pIdLigneVente.getNumero());
    requeteSql.ajouterConditionAnd("LCSUF", "=", pIdLigneVente.getSuffixe());
    requeteSql.ajouterConditionAnd("LCNLI", "=", pIdLigneVente.getNumeroLigne());
    querymanager.requete(requeteSql.getRequete());
    
    // Si la liste des conditions de ventes est vide, c'est terminé
    if (pListeInformationConditionVenteLigneVente == null || pListeInformationConditionVenteLigneVente.isEmpty()) {
      return;
    }
    
    // Insérer les lignes des conditions de ventes
    for (InformationConditionVenteLigneVente informationConditionVenteLigneVente : pListeInformationConditionVenteLigneVente) {
      // Initialisation des variables
      // L'identifiant de la ligne
      informationConditionVenteLigneVente.setIdLigneVente(pIdLigneVente);
      // L'ordre d'application
      String ordreApplication = "";
      if (informationConditionVenteLigneVente.getOrdreApplication() != null
          && informationConditionVenteLigneVente.getOrdreApplication() > 0) {
        ordreApplication = String.format("%02d", informationConditionVenteLigneVente.getOrdreApplication());
      }
      // Le code rattachement client
      String codeRattachementClient = "";
      if (informationConditionVenteLigneVente.getIdRattachementClient() != null) {
        codeRattachementClient = informationConditionVenteLigneVente.getIdRattachementClient().getCode();
      }
      // Le code du type du rattachement article
      Character codeTypeRattachementArticle = ' ';
      if (informationConditionVenteLigneVente.getIdRattachementArticle() != null
          && informationConditionVenteLigneVente.getIdRattachementArticle().getTypeRattachement() != null) {
        codeTypeRattachementArticle = informationConditionVenteLigneVente.getIdRattachementArticle().getTypeRattachement().getCode();
      }
      // Le code catégorie
      Character codeCategorie = ' ';
      if (informationConditionVenteLigneVente.getCategorie() != null) {
        codeCategorie = informationConditionVenteLigneVente.getCategorie().getCode();
      }
      // Le type de la condition
      Character typeCondition = ' ';
      if (informationConditionVenteLigneVente.getType() != null) {
        typeCondition = informationConditionVenteLigneVente.getType().getCode();
      }
      // La valeur
      BigDecimal valeur = BigDecimal.ZERO;
      if (informationConditionVenteLigneVente.getValeur() != null) {
        valeur = informationConditionVenteLigneVente.getValeur();
      }
      // Les remises
      BigDecimal[] tableauRemise = new BigDecimal[6];
      for (int i = 0; i < 6; i++) {
        BigPercentage remise = informationConditionVenteLigneVente.getTauxRemise(i + 1);
        if (remise == null) {
          tableauRemise[i] = BigDecimal.ZERO;
        }
        else {
          tableauRemise[i] = remise.getTauxEnPourcentage();
        }
      }
      // Le coefficient
      BigDecimal coefficient = BigDecimal.ZERO;
      if (informationConditionVenteLigneVente.getCoefficient() != null) {
        coefficient = informationConditionVenteLigneVente.getCoefficient();
      }
      // Date de validité
      Integer dateDebut = 0;
      if (informationConditionVenteLigneVente.getDateDebutValidite() != null) {
        dateDebut = ConvertDate.dateToDb2(informationConditionVenteLigneVente.getDateDebutValidite());
      }
      Integer dateFin = 9999999;
      if (informationConditionVenteLigneVente.getDateFinValidite() != null) {
        dateFin = ConvertDate.dateToDb2(informationConditionVenteLigneVente.getDateFinValidite());
      }
      // Le code de la formule de prix
      String codeFormulePrix = "";
      if (informationConditionVenteLigneVente.getIdFormulePrix() != null) {
        codeFormulePrix = informationConditionVenteLigneVente.getIdFormulePrix().getCode();
      }
      // Condition en ajout
      Character cnvEnAjout = ' ';
      if (informationConditionVenteLigneVente.isConditionVenteCumulative()) {
        cnvEnAjout = 'A';
      }
      // Condition Promo
      Character cnvPromo = ' ';
      if (informationConditionVenteLigneVente.isConditionVentePromotionnelle()) {
        cnvEnAjout = 'P';
      }
      
      // Construction de la requête
      // @formatter:off
      requeteSql.effacerRequete();
      requeteSql.ajouter("insert into " + querymanager.getLibrary() + '.' + EnumTableBDD.CONDITION_VENTE_LIGNE_VENTE
          + " (LCTOP, LCCOD, LCETB, LCNUM, LCSUF, LCNLI, LCORD, LCCNV, LCTRA, LCCAT, LCTCD, LCVAL, LCREM1, LCREM2, LCREM3, LCREM4,"
          + " LCREM5, LCREM6, LCCOE, LCDTD, LCDTF, LCFPR, LCDIF, LCIN1, LCIN2) values (");
      requeteSql.ajouter(""
          + informationConditionVenteLigneVente.getEtatEnregistrement()
          + "," + RequeteSql.formaterCharacterSQL(pIdLigneVente.getCodeEntete().getCode())
          + "," + RequeteSql.formaterStringSQL(pIdLigneVente.getCodeEtablissement())
          + "," + pIdLigneVente.getNumero()
          + "," + pIdLigneVente.getSuffixe()
          + "," + pIdLigneVente.getNumeroLigne()
          + "," + RequeteSql.formaterStringSQL(ordreApplication)
          + "," + RequeteSql.formaterStringSQL(codeRattachementClient)
          + "," + RequeteSql.formaterCharacterSQL(codeTypeRattachementArticle)
          + "," + RequeteSql.formaterCharacterSQL(codeCategorie)
          + "," + RequeteSql.formaterCharacterSQL(typeCondition)
          + "," + valeur
          + "," + tableauRemise[0]
          + "," + tableauRemise[1]
          + "," + tableauRemise[2]
          + "," + tableauRemise[3]
          + "," + tableauRemise[4]
          + "," + tableauRemise[5]
          + "," + coefficient
          + "," + dateDebut
          + "," + dateFin
          + "," + RequeteSql.formaterStringSQL(codeFormulePrix)
          + "," + informationConditionVenteLigneVente.getDifferencePrixUnitaire()
          + "," + RequeteSql.formaterCharacterSQL(cnvEnAjout)
          + "," + RequeteSql.formaterCharacterSQL(cnvPromo) + ')');
      // @formatter:on
      
      querymanager.requete(requeteSql.getRequete());
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Créer un identifiant de document de ventes à partir d'un GenericRecord.
   * 
   * @param pGenericRecord
   * @return
   */
  private IdDocumentVente completerIdDocumentVente(GenericRecord pGenericRecord) {
    // Lire l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("E1ETB"));
    
    // Lire le code entête
    EnumCodeEnteteDocumentVente codeEntete = null;
    Character e1cod = pGenericRecord.getCharacterValue("E1COD");
    if (e1cod != null && !e1cod.equals(' ')) {
      codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(e1cod);
    }
    
    // Lire le numéro (celui-ci est égal à 0 pour les factures)
    Integer numero = pGenericRecord.getIntegerValue("E1NUM");
    
    // Lire le suffixe (celui-ci est égal à 0 pour les factures)
    Integer suffixe = pGenericRecord.getIntegerValue("E1SUF");
    
    // Lire le numéro de factures (celui-ci est égal à 0 pour les devis, commandes et bons)
    Integer numeroFacture = pGenericRecord.getIntegerValue("E1NFA");
    
    // Créer l'identifiant du document de ventes
    return IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, numero, suffixe, numeroFacture);
  }
  
  /**
   * Crééer un identifiant de ligne de ventes à partir d'un GenericRecord.
   * 
   * @param pGenericRecord
   * @return
   */
  private IdLigneVente completerIdLigneVente(GenericRecord pGenericRecord) {
    // L'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("L1ETB"));
    // Le code de l'entête
    EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(pGenericRecord.getCharacterValue("L1COD"));
    // Le numéro du document
    Integer numero = pGenericRecord.getIntegerValue("L1NUM");
    // Le suffixe du document
    Integer suffixe = pGenericRecord.getIntegerValue("L1SUF");
    // Le numéro de ligne
    Integer numeroLigne = pGenericRecord.getIntegerValue("L1NLI");
    
    // Construire l'identifiant
    return IdLigneVente.getInstance(idEtablissement, codeEntete, numero, suffixe, numeroLigne);
  }
  
  /**
   * Initialiser les données d'un document de ventes de base à partir d'un GenericRecord.
   * @param pGenericRecord
   * @return
   */
  private ParametreDocumentVente completerParametreDocumentVente(GenericRecord pGenericRecord) {
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("E1ETB"));
    
    // Construire l'identifiant
    IdDocumentVente idDocumentVente;
    int numeroFacture = pGenericRecord.getIntValue("E1NFA", 0);
    if (numeroFacture == 0) {
      EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(pGenericRecord.getCharacterValue("E1COD"));
      idDocumentVente = IdDocumentVente.getInstanceHorsFacture(idEtablissement, codeEntete, pGenericRecord.getIntegerValue("E1NUM", 0),
          pGenericRecord.getIntegerValue("E1SUF", 0));
    }
    else {
      idDocumentVente = IdDocumentVente.getInstancePourFacture(idEtablissement, pGenericRecord.getIntegerValue("E1NFA", 0));
    }
    
    // Créer le parametre du document de ventes
    ParametreDocumentVente parametreDocumentVente = new ParametreDocumentVente();
    
    // L'identifiant du document de vente
    parametreDocumentVente.setIdDocumentVente(idDocumentVente);
    
    // L'état du document de vente
    if (pGenericRecord.isPresentField("E1ETA")) {
      parametreDocumentVente.setEtat(pGenericRecord.getIntegerValue("E1ETA"));
    }
    
    // Le code devise
    if (pGenericRecord.isPresentField("E1DEV")) {
      parametreDocumentVente.setCodeDevise(pGenericRecord.getStringValue("E1DEV"));
    }
    
    // Le numéro de la colonne tarif
    if (pGenericRecord.isPresentField("E1TAR")) {
      parametreDocumentVente.setNumeroColonneTarif(pGenericRecord.getIntegerValue("E1TAR"));
    }
    
    // Les remises
    for (int i = 1; i <= 6; i++) {
      String nomChamp = "E1REM" + i;
      if (pGenericRecord.isPresentField(nomChamp)) {
        BigPercentage pourcentage = new BigPercentage(pGenericRecord.getBigDecimalValue(nomChamp), EnumFormatPourcentage.POURCENTAGE);
        if (!pourcentage.isZero()) {
          parametreDocumentVente.setTauxRemise(pourcentage, i);
        }
      }
    }
    // Type remise ligne ('1' = cascade, ajout sinon)
    if (pGenericRecord.isPresentField("E1TRL")) {
      EnumModeTauxRemise modePourcentageRemise = EnumModeTauxRemise.valueOfByCode(pGenericRecord.getCharacterValue("E1TRL"));
      parametreDocumentVente.setModeTauxRemise(modePourcentageRemise);
    }
    
    // La base sur laquelle la remise s’applique (le prix unitaire ou le montant de la ligne)
    if (pGenericRecord.isPresentField("E1BRL")) {
      EnumAssietteTauxRemise assiettePourcentageRemise = EnumAssietteTauxRemise.valueOfByCode(pGenericRecord.getCharacterValue("E1BRL"));
      parametreDocumentVente.setAssietteTauxRemise(assiettePourcentageRemise);
    }
    
    // Code condition de Vente
    if (pGenericRecord.isPresentField("E1CNV")) {
      parametreDocumentVente.setCodeConditionVente(pGenericRecord.getStringValue("E1CNV"));
    }
    
    // Le numéro client de la centrale 1
    if (pGenericRecord.isPresentField("E1CC1")) {
      parametreDocumentVente.setNumeroClientCentrale1(pGenericRecord.getIntegerValue("E1CC1"));
    }
    
    // Date de création
    if (pGenericRecord.isPresentField("E1CRE")) {
      parametreDocumentVente.setDateCreation(pGenericRecord.getDateValue("E1CRE"));
    }
    
    // Numéro et suffixe du client livré
    if (pGenericRecord.isPresentField("E1CLLP") && pGenericRecord.isPresentField("E1CLLS")) {
      IdClient idClientLivre =
          IdClient.getInstance(idEtablissement, pGenericRecord.getIntegerValue("E1CLLP"), pGenericRecord.getIntegerValue("E1CLLS"));
      parametreDocumentVente.setIdClientLivre(idClientLivre);
    }
    
    // Numéro et suffixe du client facturé
    if (pGenericRecord.isPresentField("E1CLFP") && pGenericRecord.isPresentField("E1CLFS")) {
      IdClient idClientFacture =
          IdClient.getInstance(idEtablissement, pGenericRecord.getIntegerValue("E1CLFP"), pGenericRecord.getIntegerValue("E1CLFS"));
      parametreDocumentVente.setIdClientFacture(idClientFacture);
    }
    
    // Le taux de TVA 1
    // Il est multiplé par 10 dans la base de données (20,00 est stocké 200,00)
    if (pGenericRecord.isPresentField("E1TV1")) {
      BigDecimal tauxTVA = pGenericRecord.getBigDecimalValue("E1TV1");
      if (tauxTVA != null) {
        tauxTVA = tauxTVA.divide(new BigDecimal(10));
        parametreDocumentVente.setTauxTVA1(new BigPercentage(tauxTVA, EnumFormatPourcentage.POURCENTAGE));
      }
    }
    
    // Le taux de TVA 2
    // Il est multiplé par 10 dans la base de données (20,00 est stocké 200,00)
    if (pGenericRecord.isPresentField("E1TV2")) {
      BigDecimal tauxTVA = pGenericRecord.getBigDecimalValue("E1TV2");
      if (tauxTVA != null) {
        tauxTVA = tauxTVA.divide(new BigDecimal(10));
        parametreDocumentVente.setTauxTVA2(new BigPercentage(tauxTVA, EnumFormatPourcentage.POURCENTAGE));
      }
    }
    
    // Le taux de TVA 3
    // Il est multiplé par 10 dans la base de données (20,00 est stocké 200,00)
    if (pGenericRecord.isPresentField("E1TV3")) {
      BigDecimal tauxTVA = pGenericRecord.getBigDecimalValue("E1TV3");
      if (tauxTVA != null) {
        tauxTVA = tauxTVA.divide(new BigDecimal(10));
        parametreDocumentVente.setTauxTVA3(new BigPercentage(tauxTVA, EnumFormatPourcentage.POURCENTAGE));
      }
    }
    
    // Le type de facturation
    if (pGenericRecord.isPresentField("E1TFA")) {
      parametreDocumentVente.setTypeFacturation(pGenericRecord.getCharacterValue("E1TFA"));
    }
    
    // Le document est-il TTC ?
    if (pGenericRecord.isPresentField("E1NAT")) {
      if (pGenericRecord.getCharacterValue("E1NAT").charValue() == 'T') {
        parametreDocumentVente.setModeTTC(true);
      }
      else {
        parametreDocumentVente.setModeTTC(false);
      }
    }
    
    // Le champ E1TO1G est une zone numérique de 9 chiffres (par exemple "000000010") dont chaque chiffre correspondant à un indicateur
    // indépendant. Le 8ème chiffre à partir de la gauche permet de savoir si le document de vente a des prix garanti.
    if (pGenericRecord.isPresentField("E1TO1G")) {
      parametreDocumentVente.setPrixGarantiE1TO1G(pGenericRecord.getBigDecimalValue("E1TO1G"));
    }
    
    // Le numéro de chantier
    if (pGenericRecord.isPresentField("NUMCHANTIER")) {
      Integer numeroChantier = Constantes.convertirTexteEnInteger(pGenericRecord.getString("NUMCHANTIER"));
      if (numeroChantier.intValue() > 0) {
        parametreDocumentVente.setNumeroChantier(numeroChantier);
      }
    }
    
    return parametreDocumentVente;
  }
  
  /**
   * Initialiser les données d'une ligne de ventes de base à partir d'un GenericRecord.
   * 
   * @param pGenericRecord
   * @return
   */
  private ParametreLigneVente completerParametreLigneVente(GenericRecord pGenericRecord) {
    // Construire l'identifiant de la ligne de ventes
    IdLigneVente idLigneVente = completerIdLigneVente(pGenericRecord);
    
    // Créer le parametre du document de ventes
    ParametreLigneVente parametreLigneVente = new ParametreLigneVente();
    parametreLigneVente.setIdLigneVente(idLigneVente);
    
    // L'état de la ligne
    if (pGenericRecord.isPresentField("L1TOP")) {
      parametreLigneVente.setEtat(pGenericRecord.getIntegerValue("L1TOP"));
    }
    
    // Identifiant article
    if (pGenericRecord.isPresentField("L1ART")) {
      IdArticle idArticle = IdArticle.getInstance(idLigneVente.getIdEtablissement(), pGenericRecord.getStringValue("L1ART", null, true));
      parametreLigneVente.setIdArticle(idArticle);
    }
    
    // Provenance de la colonne tarif
    if (pGenericRecord.isPresentField("L1TT")) {
      EnumProvenanceColonneTarif provenanceColonneTarif =
          EnumProvenanceColonneTarif.valueOfByCode(pGenericRecord.getIntegerValue("L1TT"));
      parametreLigneVente.setProvenanceColonneTarif(provenanceColonneTarif);
    }
    
    // Numéro de la colonne tarif
    if (pGenericRecord.isPresentField("L1TAR")) {
      parametreLigneVente.setNumeroColonneTarif(pGenericRecord.getIntegerValue("L1TAR"));
    }
    
    // Provenance du prix de base
    if (pGenericRecord.isPresentField("L1TB")) {
      EnumProvenancePrixBase enumProvenancePrixBaseHT = EnumProvenancePrixBase.valueOfByCode(pGenericRecord.getIntegerValue("L1TB"));
      parametreLigneVente.setProvenancePrixBase(enumProvenancePrixBaseHT);
    }
    
    // Prix de base HT
    if (pGenericRecord.isPresentField("L1PVB")) {
      parametreLigneVente.setPrixBaseHT(pGenericRecord.getBigDecimalValue("L1PVB"));
    }
    
    // Prix base TTC (dans l'extension XITYP = 6)
    // Données dans le champ XILIB (30 caractères) ;
    // - 0 à 8/2 : PVB prix de base TTC
    // - 9 à 17/2 : PVN prix net saisi TTC
    // - 18 à 26/2 : PVC prix calculé TTC
    // - 27 à 29 : non utilisé
    if (pGenericRecord.isPresentField("XILIB")) {
      String extensionTTC = pGenericRecord.getStringValue("XILIB");
      if (extensionTTC.length() >= 9) {
        BigDecimal prixBaseTTC = new BigDecimal(extensionTTC.substring(0, 7) + "." + extensionTTC.substring(7, 9));
        parametreLigneVente.setPrixBaseTTC(prixBaseTTC);
      }
    }
    
    // Provenance du coefficient
    if (pGenericRecord.isPresentField("L1TC")) {
      EnumProvenanceCoefficient enumProvenanceCoeffcient =
          EnumProvenanceCoefficient.valueOfByCode(pGenericRecord.getIntegerValue("L1TC"));
      parametreLigneVente.setProvenanceCoefficient(enumProvenanceCoeffcient);
    }
    
    // Coefficient
    if (pGenericRecord.isPresentField("L1COE")) {
      parametreLigneVente.setCoefficient(pGenericRecord.getBigDecimalValue("L1COE"));
    }
    
    // Provenance des pourcentages remises
    if (pGenericRecord.isPresentField("L1TR")) {
      EnumProvenanceTauxRemise enumProvenancePourcentageRemise =
          EnumProvenanceTauxRemise.valueOfByCode(pGenericRecord.getIntegerValue("L1TR"));
      parametreLigneVente.setProvenanceTauxRemise(enumProvenancePourcentageRemise);
    }
    
    // Pourcentage de remise 1
    if (pGenericRecord.isPresentField("L1REM1")) {
      BigPercentage pourcentageRemise = new BigPercentage(pGenericRecord.getBigDecimalValue("L1REM1"), EnumFormatPourcentage.POURCENTAGE);
      if (!pourcentageRemise.isZero()) {
        parametreLigneVente.setTauxRemise1(pourcentageRemise);
      }
    }
    
    // Pourcentage de remise 2
    if (pGenericRecord.isPresentField("L1REM2")) {
      BigPercentage pourcentageRemise = new BigPercentage(pGenericRecord.getBigDecimalValue("L1REM2"), EnumFormatPourcentage.POURCENTAGE);
      if (!pourcentageRemise.isZero()) {
        parametreLigneVente.setTauxRemise2(pourcentageRemise);
      }
    }
    
    // Pourcentage de remise 3
    if (pGenericRecord.isPresentField("L1REM3")) {
      BigPercentage pourcentageRemise = new BigPercentage(pGenericRecord.getBigDecimalValue("L1REM3"), EnumFormatPourcentage.POURCENTAGE);
      if (!pourcentageRemise.isZero()) {
        parametreLigneVente.setTauxRemise3(pourcentageRemise);
      }
    }
    
    // Pourcentage de remise 4
    if (pGenericRecord.isPresentField("L1REM4")) {
      BigPercentage pourcentageRemise = new BigPercentage(pGenericRecord.getBigDecimalValue("L1REM4"), EnumFormatPourcentage.POURCENTAGE);
      if (!pourcentageRemise.isZero()) {
        parametreLigneVente.setTauxRemise4(pourcentageRemise);
      }
    }
    
    // Pourcentage de remise 5
    if (pGenericRecord.isPresentField("L1REM5")) {
      BigPercentage pourcentageRemise = new BigPercentage(pGenericRecord.getBigDecimalValue("L1REM5"), EnumFormatPourcentage.POURCENTAGE);
      if (!pourcentageRemise.isZero()) {
        parametreLigneVente.setTauxRemise5(pourcentageRemise);
      }
    }
    
    // Pourcentage de remise 6
    if (pGenericRecord.isPresentField("L1REM6")) {
      BigPercentage pourcentageRemise = new BigPercentage(pGenericRecord.getBigDecimalValue("L1REM6"), EnumFormatPourcentage.POURCENTAGE);
      if (!pourcentageRemise.isZero()) {
        parametreLigneVente.setTauxRemise6(pourcentageRemise);
      }
    }
    
    // Mode d’application des remises ('1' = cascade, ajout sinon)
    if (pGenericRecord.isPresentField("L1TRL")) {
      EnumModeTauxRemise modePourcentageRemise = EnumModeTauxRemise.valueOfByCode(pGenericRecord.getCharacterValue("L1TRL"));
      parametreLigneVente.setModeTauxRemise(modePourcentageRemise);
    }
    
    // Assiette sur laquelle la remise s’applique (le prix unitaire ou le montant de la ligne)
    if (pGenericRecord.isPresentField("L1BRL")) {
      EnumAssietteTauxRemise assiettePourcentageRemise = EnumAssietteTauxRemise.valueOfByCode(pGenericRecord.getCharacterValue("L1BRL"));
      parametreLigneVente.setAssietteTauxRemise(assiettePourcentageRemise);
    }
    
    // Provenance du prix net saisi
    if (pGenericRecord.isPresentField("L1TN")) {
      EnumProvenancePrixNet enumProvenancePrixnetHT = EnumProvenancePrixNet.valueOfByCode(pGenericRecord.getIntegerValue("L1TN"));
      parametreLigneVente.setProvenancePrixNet(enumProvenancePrixnetHT);
    }
    
    // Prix net saisi HT
    if (pGenericRecord.isPresentField("L1PVN")) {
      parametreLigneVente.setPrixNetSaisiHT(pGenericRecord.getBigDecimalValue("L1PVN"));
    }
    
    // Prix net saisi TTC (dans l'extension XITYP = 6)
    // Données dans le champ XILIB (30 caractères) ;
    // - 0 à 8/2 : PVB prix de base TTC
    // - 9 à 17/2 : PVN prix net saisi TTC
    // - 18 à 26/2 : PVC prix calculé TTC
    // - 27 à 29 : non utilisé
    if (pGenericRecord.isPresentField("XILIB")) {
      String extensionTTC = pGenericRecord.getStringValue("XILIB");
      if (extensionTTC.length() >= 18) {
        BigDecimal prixNetSaisiTTC = new BigDecimal(extensionTTC.substring(9, 16) + "." + extensionTTC.substring(16, 18));
        parametreLigneVente.setPrixNetSaisiTTC(prixNetSaisiTTC);
      }
    }
    
    // Prix net calculé HT
    if (pGenericRecord.isPresentField("L1PVC")) {
      parametreLigneVente.setPrixNetCalculeHT(pGenericRecord.getBigDecimalValue("L1PVC"));
    }
    
    // Prix net calculé TTC (dans l'extension XITYP = 6)
    // Données dans le champ XILIB (30 caractères) ;
    // - 0 à 8/2 : PVB prix de base TTC
    // - 9 à 17/2 : PVN prix net saisi TTC
    // - 18 à 26/2 : PVC prix calculé TTC
    // - 27 à 29 : non utilisé
    if (pGenericRecord.isPresentField("XILIB")) {
      String extensionTTC = pGenericRecord.getStringValue("XILIB");
      if (extensionTTC.length() >= 27) {
        BigDecimal prixNetTTC = new BigDecimal(extensionTTC.substring(18, 25) + "." + extensionTTC.substring(25, 27));
        parametreLigneVente.setPrixNetCalculeTTC(prixNetTTC);
      }
    }
    
    // Le type de gratuit
    Character codeTypeGratuit = pGenericRecord.getCharacterValue("L1IN2");
    if (codeTypeGratuit != null) {
      parametreLigneVente.setIdTypeGratuit(IdTypeGratuit.getInstance(codeTypeGratuit));
    }
    
    // Renseigner l'origine du prix de vente
    if (pGenericRecord.isPresentField("L1IN18")) {
      EnumOriginePrixVente originePrixVente = EnumOriginePrixVente.valueOfByCode(pGenericRecord.getCharacterValue("L1IN18").charValue());
      parametreLigneVente.setOriginePrixVente(originePrixVente);
    }
    
    // Indique si les conditions de vente normales et/ou quantitatives sont utilisées
    if (pGenericRecord.isPresentField("L1IN19")) {
      try {
        // Attention - A supprimer en Aout/Septembre car un REC va être fait
        // Contrôle du contenu du champ car nous nous sommes aperçus qu'il pouvait contenir 'X'. En effet, ce 'X' vient de programmes RPG
        // qui gère les extractions (à priori il y a eu un méli-melo et le champ a 2 utilisations différentes : extractions & autorisation
        // des CNVs quantitatives).
        // A priori, Marc devrait faire le ménage et on part du principe ici que s'il y a 'X' il est remplacé par ' ' car sinon j'ai des
        // problème dans le calcul de prix car les CNV sont ignorés puisque EnumTypeApplicationConditionVente vaudra null.
        Character code = pGenericRecord.getCharacterValue("L1IN19");
        if (code != null && code.charValue() == 'X') {
          code = Character.valueOf(' ');
        }
        // Fin à supprimer d'ici septembre 2022
        
        // Fonctionnement normal
        EnumTypeApplicationConditionVente type = EnumTypeApplicationConditionVente.valueOfByCode(code);
        parametreLigneVente.setTypeApplicationConditionVente(type);
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
    
    // L'unité de vente
    if (pGenericRecord.isPresentField("L1UNV")) {
      parametreLigneVente.setCodeUniteVente(pGenericRecord.getStringValue("L1UNV", "", true));
    }
    
    // Le montant HT
    if (pGenericRecord.isPresentField("L1MHT")) {
      parametreLigneVente.setMontantHT(pGenericRecord.getBigDecimalValue("L1MHT"));
    }
    
    // Le numéro de la colonne TVA stocké dans l'entête
    if (pGenericRecord.isPresentField("L1COL")) {
      parametreLigneVente.setNumeroTVAEntete(pGenericRecord.getIntegerValue("L1COL"));
    }
    
    // Le numéro de la colonne du taux de TVA de la DG
    if (pGenericRecord.isPresentField("L1TVA")) {
      parametreLigneVente.setNumeroTVAEtablissement(pGenericRecord.getIntegerValue("L1TVA", 0));
    }
    
    // La quantité commandée
    if (pGenericRecord.isPresentField("L1QTE")) {
      parametreLigneVente.setQuantiteUV(pGenericRecord.getBigDecimalValue("L1QTE"));
    }
    
    return parametreLigneVente;
  }
  
  /**
   * Initialiser les données d'un article à partir d'un GenericRecord.
   * 
   * @param pGenericRecord
   * @param pParEtablissement
   * @param pModeNegoce
   * @return
   */
  private ParametreArticle completerParametreArticle(GenericRecord pGenericRecord, boolean pParEtablissement, boolean pModeNegoce) {
    // Créer le paramètre de l'article
    ParametreArticle parametreArticle = new ParametreArticle();
    
    // Construire l'identifiant
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("A1ETB"));
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, pGenericRecord.getStringValue("A1ART"));
    parametreArticle.setIdArticle(idArticle);
    
    // L'identifiant fournisseur
    if (pGenericRecord.isPresentField("A1COF") && pGenericRecord.isPresentField("A1FRS")) {
      Integer collectif = pGenericRecord.getIntegerValue("A1COF");
      Integer numero = pGenericRecord.getIntegerValue("A1FRS");
      // Afin d'éviter une exception, le contrôle est effectué en amont
      if (collectif.intValue() > 0 && numero.intValue() > 0) {
        parametreArticle.setIdFournisseur(IdFournisseur.getInstance(idEtablissement, collectif, numero));
      }
    }
    
    // Les rattachements CNV
    if (pGenericRecord.isPresentField("A1CNV")) {
      String rattachement = pGenericRecord.getStringValue("A1CNV", null, true);
      if (!rattachement.isEmpty()) {
        parametreArticle.setCodeRattachementCNV(rattachement);
      }
    }
    if (pGenericRecord.isPresentField("A1CNV1")) {
      String rattachement = pGenericRecord.getStringValue("A1CNV1", null, true);
      if (!rattachement.isEmpty()) {
        parametreArticle.setCodeRattachementCNV1(rattachement);
      }
    }
    if (pGenericRecord.isPresentField("A1CNV2")) {
      String rattachement = pGenericRecord.getStringValue("A1CNV2", null, true);
      if (!rattachement.isEmpty()) {
        parametreArticle.setCodeRattachementCNV2(rattachement);
      }
    }
    if (pGenericRecord.isPresentField("A1CNV3")) {
      String rattachement = pGenericRecord.getStringValue("A1CNV3", null, true);
      if (!rattachement.isEmpty()) {
        parametreArticle.setCodeRattachementCNV3(rattachement);
      }
    }
    
    // Le groupe et la famille
    if (pGenericRecord.isPresentField("A1FAM")) {
      String valeur = pGenericRecord.getStringValue("A1FAM", null, true);
      if (!valeur.isEmpty()) {
        parametreArticle.setFamille(valeur);
      }
      // Sinon l'article est un article commentaire
      else {
        parametreArticle.setArticleCommentaire(true);
      }
    }
    
    // La sous-famille
    if (pGenericRecord.isPresentField("A1SFA")) {
      String valeur = pGenericRecord.getStringValue("A1SFA", null, true);
      if (!valeur.isEmpty()) {
        parametreArticle.setSousFamille(valeur);
      }
    }
    
    // L'unité de vente
    if (pGenericRecord.isPresentField("A1UNV")) {
      parametreArticle.setCodeUniteVente(pGenericRecord.getStringValue("A1UNV", "", true));
    }
    
    // Le numéro de la colonne de la TVA de la DG
    if (pGenericRecord.isPresentField("A1TVA")) {
      parametreArticle.setNumeroTVAEtablissement(pGenericRecord.getIntegerValue("A1TVA", 0));
    }
    
    // La source du prix de revient
    if (pGenericRecord.isPresentField("A1TPR")) {
      try {
        Integer numeroSource = pGenericRecord.getIntegerValue("A1TPR", 0);
        parametreArticle.setTypePrv(EnumTypePrv.valueOfByCode(numeroSource));
      }
      catch (Exception e) {
      }
    }
    
    // Le référence tarif
    String referenceTarif = pGenericRecord.getStringValue("A1RTA", null, true);
    if (referenceTarif != null && !referenceTarif.isEmpty()) {
      IdReferenceTarif idReferenceTarif = IdReferenceTarif.getInstance(idEtablissement, referenceTarif);
      parametreArticle.setIdReferenceTarif(idReferenceTarif);
    }
    
    // La remise maximum (par rapport au prix de revient)
    if (pGenericRecord.isPresentField("A1PMR")) {
      parametreArticle.setTauxRemiseMaximum(pGenericRecord.getIntegerValue("A1PMR"));
    }
    
    // La remise maximum (par rapport au prix de revient)
    if (pGenericRecord.isPresentField("A1PMR")) {
      parametreArticle.setTauxRemiseMaximum(pGenericRecord.getIntegerValue("A1PMR"));
    }
    
    // L'article est-il un article commentaire ?
    if (pGenericRecord.isPresentField("A1TSP")) {
      if (pGenericRecord.getCharacterValue("A1TSP") == '*') {
        parametreArticle.setArticleCommentaire(true);
      }
    }
    
    // Chargement du PUMP par établissement
    if (pParEtablissement) {
      // Le PUMP par établissement de l'historique des monopumps
      if (pGenericRecord.isPresentField("HMPMP")) {
        parametreArticle.setOriginePump(EnumOriginePump.ETABLISSEMENT);
        parametreArticle.setPump(pGenericRecord.getBigDecimalValue("HMPMP"));
      }
    }
    // Chargement du PUMP par magasin
    else {
      // Le PUMP par magasin (issu de l'historique des stocks)
      if (pGenericRecord.isPresentField("H1PMP")) {
        parametreArticle.setOriginePump(EnumOriginePump.MAGASIN);
        parametreArticle.setPump(pGenericRecord.getBigDecimalValue("H1PMP"));
      }
    }
    
    // Chargement du prix de revient en fonction de son type ou du mode négoce
    if (pModeNegoce) {
      // En mode négoce, le prix de revient standard est issu de la condition d'achats
      // Le prix de revient standard de la condition d'achats
      if (pGenericRecord.isPresentField("CAPRS")) {
        parametreArticle.setOriginePrv(EnumOriginePrv.PRV_STANDARD_CNA);
        parametreArticle.setPrv(pGenericRecord.getBigDecimalValue("CAPRS"));
      }
    }
    else if (parametreArticle.getTypePrv() != null) {
      switch (parametreArticle.getTypePrv()) {
        // Saisissable et modifié à chaque entrée en stock -> H1PRV de la table PGVMHISM
        case ENTREE_STOCK:
          // Le prix de revient de l'historique des stocks
          if (pGenericRecord.isPresentField("H1PRV") && !pParEtablissement) {
            parametreArticle.setOriginePrv(EnumOriginePrv.PRV_MAGASIN);
            parametreArticle.setPrv(pGenericRecord.getBigDecimalValue("H1PRV"));
          }
          break;
        
        // Saisie et jamais calculé -> A1PRV
        case ENTREE_CALCUL_PARAMETRE:
          if (pGenericRecord.isPresentField("A1PRV")) {
            parametreArticle.setOriginePrv(EnumOriginePrv.PRV_ARTICLE);
            parametreArticle.setPrv(pGenericRecord.getBigDecimalValue("A1PRV"));
          }
          break;
        
        // Calculé à chaque entrée en stock avec mode de calcul paramétré -> ???
        case JAMAIS_CALCULE:
          // A priori peu utilisé (Vue avec Marc)
          // Si on doit le coder, il faut faire une étude sur les prix de revient car sujet complexe
          Trace.alerte("La récupération du prix de revient (A1ATR = " + EnumTypePrv.JAMAIS_CALCULE.getNumero()
              + ") n'est pas codée pour le moment.");
          break;
        
        // Saisi, jamais calculé & PUMP non modifié par le système -> ???
        case NON_MODIFIE:
          // A priori peu utilisé (Vue avec Marc)
          // Si on doit le coder, il faut faire une étude sur les prix de revient car sujet complexe
          Trace.alerte(
              "La récupération du prix de revient (A1ATR = " + EnumTypePrv.NON_MODIFIE.getNumero() + ") n'est pas codée pour le moment.");
          break;
      }
    }
    
    return parametreArticle;
  }
  
  /**
   * Initialiser les données d'un tarif article à partir d'un GenericRecord.
   * 
   * @param pGenericRecord
   * @return
   */
  private ParametreTarif completerParametreTarif(GenericRecord pGenericRecord) {
    // Créer le parametre de l'article
    ParametreTarif parametreTarif = new ParametreTarif();
    
    // Récupérer l'identifiant de l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("ATETB"));
    
    // Le code article
    if (pGenericRecord.isPresentField("ATART")) {
      IdArticle idArticle = IdArticle.getInstance(idEtablissement, pGenericRecord.getStringValue("ATART"));
      parametreTarif.setIdArticle(idArticle);
    }
    
    // L'arrondi appliqué (à priori pas nécessaire car les bons tarifs sont stockés en table mais il est arrivé de trouver des articles
    // avec des tarifs erronés pour une raison inconnue)
    if (pGenericRecord.isPresentField("ATRON")) {
      try {
        EnumGraduationDecimaleTarif arrondiTarif = EnumGraduationDecimaleTarif.valueOfByNumero(pGenericRecord.getIntegerValue("ATRON"));
        parametreTarif.setGraduationDecimaleTarif(arrondiTarif);
      }
      catch (Exception e) {
      }
    }
    
    // Les coefficients pour calculer les prix de vente et les prix de ventes
    for (int i = 1; i <= ColonneTarif.NOMBRE_MAX_COLONNE_TARIF; i++) {
      // Les coefficients pour les prix de vente
      BigDecimal coefficient = null;
      String nomChamp = String.format("ATK%02d", i);
      if (pGenericRecord.isPresentField(nomChamp)) {
        coefficient = pGenericRecord.getBigDecimalValue(nomChamp);
      }
      
      // Les prix de vente
      BigDecimal prixBaseHT = null;
      nomChamp = String.format("ATP%02d", i);
      if (pGenericRecord.isPresentField(nomChamp)) {
        prixBaseHT = pGenericRecord.getBigDecimalValue(nomChamp);
      }
      
      parametreTarif.ajouterColonneTarif(i, coefficient, prixBaseHT);
    }
    
    // La date du tarif
    if (pGenericRecord.isPresentField("ATDAP")) {
      parametreTarif.setDateTarif(pGenericRecord.getDateValue("ATDAP"));
    }
    
    return parametreTarif;
  }
  
  /**
   * Initialiser les données d'un client à partir d'un GenericRecord.
   * 
   * @param pGenericRecord
   * @return
   */
  private ParametreClient completerParametreClient(GenericRecord pGenericRecord) {
    // Construire l'identifiant
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("CLETB"));
    Integer numero = pGenericRecord.getIntegerValue("CLCLI");
    Integer suffixe = pGenericRecord.getIntegerValue("CLLIV");
    IdClient idClient = IdClient.getInstance(idEtablissement, numero, suffixe);
    
    // Créer le parametre du client
    ParametreClient parametreClient = new ParametreClient();
    parametreClient.setIdClient(idClient);
    
    // Le numéro du client facturé
    if (pGenericRecord.isPresentField("CLCLFP") && pGenericRecord.isPresentField("CLCLFS")) {
      Integer numeroClientFacture = pGenericRecord.getIntegerValue("CLCLFP");
      Integer suffixeClientFacture = pGenericRecord.getIntegerValue("CLCLFS");
      if (numeroClientFacture.intValue() > 0) {
        IdClient idClientFacture = IdClient.getInstance(idEtablissement, numeroClientFacture, suffixeClientFacture);
        parametreClient.setIdClientFacture(idClientFacture);
      }
    }
    
    // Le client est-il TTC ?
    if (pGenericRecord.isPresentField("CLTTC")) {
      if (pGenericRecord.getCharacterValue("CLTTC").charValue() == ' ') {
        parametreClient.setModeTTC(false);
      }
      else {
        parametreClient.setModeTTC(true);
      }
    }
    
    // Le code devise
    if (pGenericRecord.isPresentField("CLDEV")) {
      parametreClient.setCodeDevise(pGenericRecord.getStringValue("CLDEV"));
    }
    
    // Le numéro de la colonne tarif
    if (pGenericRecord.isPresentField("CLTAR")) {
      Integer numeroColonne = pGenericRecord.getIntegerValue("CLTAR");
      // La colonne 0 est en réalité la colonne 10 (car le champ ne contient un seul chiffre)
      if (numeroColonne != null && numeroColonne.intValue() == 0) {
        numeroColonne = Integer.valueOf(10);
      }
      parametreClient.setNumeroColonneTarif(numeroColonne);
    }
    
    // Le numéro de la colonne tarif 2
    if (pGenericRecord.isPresentField("CLTA1")) {
      Integer numeroColonne = pGenericRecord.getIntegerValue("CLTA1");
      // La colonne 0 est en réalité la colonne 10 (car le champ ne contient un seul chiffre)
      if (numeroColonne != null && numeroColonne.intValue() == 0) {
        numeroColonne = Integer.valueOf(10);
      }
      parametreClient.setNumeroColonneTarif2(numeroColonne);
    }
    
    // Les remises
    for (int i = 1; i <= 3; i++) {
      String nomChamp = "CLREM" + i;
      if (pGenericRecord.isPresentField(nomChamp)) {
        BigPercentage pourcentage = new BigPercentage(pGenericRecord.getBigDecimalValue(nomChamp), EnumFormatPourcentage.POURCENTAGE);
        if (!pourcentage.isZero()) {
          parametreClient.setTauxRemise(pourcentage, i);
        }
      }
    }
    
    // Le code de la condition de vente générale
    if (pGenericRecord.isPresentField("CLCNV")) {
      String valeur = pGenericRecord.getStringValue("CLCNV", "", true);
      // S'il s'agit d'un nombre à 6 chiffres alors il s'agit du numéro du client référence pour les conditions de ventes
      int numeroClient = 0;
      if (IdClient.isNumeroClient(valeur)) {
        numeroClient = Constantes.convertirTexteEnInteger(valeur);
      }
      if (numeroClient > 0) {
        parametreClient.setNumeroClientReferenceConditionVente(Integer.valueOf(numeroClient));
        // Initialisation des codes des groupes de conditions de ventes de ce client référence
        if (pGenericRecord.isPresentField("REFCLCNV")) {
          parametreClient.setCodeConditionStandardClientReference(pGenericRecord.getString("REFCLCNV"));
        }
        if (pGenericRecord.isPresentField("REFCLCNP")) {
          parametreClient.setCodeConditionPromoClientReference(pGenericRecord.getString("REFCLCNP"));
        }
      }
      // Sinon il s'agit du code du groupe des condition de ventes pour la condition standard
      else {
        parametreClient.setCodeConditionVente(valeur);
      }
    }
    
    // Le code de la condition de vente promo
    if (pGenericRecord.isPresentField("CLCNP")) {
      parametreClient.setCodeConditionVentePromo(pGenericRecord.getStringValue("CLCNP"));
    }
    
    // Les centrales d'achats
    if (pGenericRecord.isPresentField("CLCC1")) {
      Integer numeroCentrale = pGenericRecord.getIntegerValue("CLCC1");
      if (numero.intValue() > 0) {
        parametreClient.setNumeroCentraleAchat1(numeroCentrale);
      }
    }
    if (pGenericRecord.isPresentField("CLCC2")) {
      Integer numeroCentrale = pGenericRecord.getIntegerValue("CLCC2");
      if (numero.intValue() > 0) {
        parametreClient.setNumeroCentraleAchat2(numeroCentrale);
      }
    }
    if (pGenericRecord.isPresentField("CLCC2")) {
      Integer numeroCentrale = pGenericRecord.getIntegerValue("CLCC2");
      if (numero.intValue() > 0) {
        parametreClient.setNumeroCentraleAchat2(numeroCentrale);
      }
    }
    
    // Le type de facturation
    if (pGenericRecord.isPresentField("CLTFA")) {
      parametreClient.setTypeFacturation(pGenericRecord.getCharacterValue("CLTFA"));
    }
    
    // Top utilisation des conditions de ventes de la DG
    if (pGenericRecord.isPresentField("CLIN4")) {
      EnumRegleExclusionConditionVenteEtablissement regleExclusionConditionVenteEtablissement =
          EnumRegleExclusionConditionVenteEtablissement.valueOfByCode(pGenericRecord.getCharacterValue("CLIN4"));
      parametreClient.setRegleExclusionConditionVenteEtablissement(regleExclusionConditionVenteEtablissement);
    }
    
    // Indique si les conditions de vente normales et/ou quantitatives sont utilisées
    if (pGenericRecord.isPresentField("CLIN10")) {
      try {
        EnumTypeApplicationConditionVente type =
            EnumTypeApplicationConditionVente.valueOfByCode(pGenericRecord.getCharacterValue("CLIN10"));
        parametreClient.setTypeApplicationConditionVente(type);
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
    
    return parametreClient;
  }
  
  /**
   * Initialiser les données d'une condition de vente à partir d'un GenericRecord.
   * 
   * Comme les données proviennent protentiellement de 2 tables avec les mêmes noms de champs, pour ne pas trop complexifier
   * l'initialisation des paramètres, un préfixe (la lettre Q) va être utilisée pour marquer les champs venant de la table PGVMCNQM.
   * Les champs T1CAT, T1CNV, T1TRA, T1RAT, T1DEV ne sont pas préfixés car leur valeur est identiques dans les 2 tables (ils forment la
   * clé) et le champ TQQTE n'est pas préfixé car il est propre à la table PGVMCNVM
   * 
   * @param pGenericRecord
   * @param pDateApplication
   * @return
   */
  private ParametreConditionVente completerParametreConditionVente(GenericRecord pGenericRecord, IdEtablissement pIdEtablissement,
      int pDateApplication) {
    // Est ce une condition de ventes quantitative ?
    String prefixe = "";
    if (pGenericRecord.isPresentField("TQQTE") && pGenericRecord.getField("TQQTE") != null) {
      prefixe = "Q";
    }
    
    // Détermine s'il faut prendre les données de T1 ou de T2 ?
    // L'enregistrement est merdique car il contient 2 informations possibles mais c'est l'une ou l'autre. Le choix va se faire sur la
    // plage de date.
    int valeurMax = (int) Constantes.valeurMaxZoneNumerique(7);
    int t1dtd = 0;
    int t1dtf = valeurMax;
    int t2dtd = 0;
    int t2dtf = valeurMax;
    char t2tcd = ' ';
    boolean avecT1 = true;
    if (pGenericRecord.isPresentField(prefixe + "T1DTD") && pGenericRecord.isPresentField(prefixe + "T1DateFin")) {
      t1dtd = pGenericRecord.getIntegerValue(prefixe + "T1DTD", 0).intValue();
      t1dtf = pGenericRecord.getIntegerValue(prefixe + "T1DateFin", valeurMax).intValue();
    }
    if (pGenericRecord.isPresentField(prefixe + "T2DTD") && pGenericRecord.isPresentField(prefixe + "T2DateFin")) {
      t2dtd = pGenericRecord.getIntegerValue(prefixe + "T2DTD", 0).intValue();
      t2dtf = pGenericRecord.getIntegerValue(prefixe + "T2DateFin", valeurMax).intValue();
    }
    if (pGenericRecord.isPresentField(prefixe + "T2TCD")) {
      t2tcd = pGenericRecord.getCharacterValue(prefixe + "T2TCD", ' ').charValue();
      // Cas particulier pour éliminer des valeurs merdiques dans ce champ
      // A supprimer lorsque la cause de l'insertion sera identifiée et qu'un rec pour corriger les données sera fait
      if (t2tcd == ',') {
        t2tcd = ' ';
      }
    }
    
    // Si la date d'application est comprise dans la plage de date de T1
    if (t1dtd <= pDateApplication && pDateApplication <= t1dtf) {
      avecT1 = true;
    }
    // Si le champ T2TCD est renseigné et si la date d'application est comprise dans la plage de date de T2
    else if (t2tcd != ' ' && t2dtd <= pDateApplication && pDateApplication <= t2dtf) {
      avecT1 = false;
    }
    // Sinon la condition est hors date et elle est ignorée (inutile car la requête filtre désormais les bonnes plages de dates)
    else {
      return null;
    }
    
    // Créer le paramètre de la condition de ventes
    ParametreConditionVente parametreConditionVente = new ParametreConditionVente();
    parametreConditionVente.setIdEtablissement(pIdEtablissement);
    
    // Champs communs
    // La catégorie
    if (pGenericRecord.isPresentField("T1CAT")) {
      Character codeCategorie = pGenericRecord.getCharacterValue("T1CAT");
      parametreConditionVente.setCategorie(EnumCategorieConditionVente.valueOfByCode(codeCategorie));
    }
    // Le code de la condition
    if (pGenericRecord.isPresentField("T1CNV")) {
      IdRattachementClient idRattachementClient =
          IdRattachementClient.getInstance(pIdEtablissement, pGenericRecord.getStringValue("T1CNV"));
      parametreConditionVente.setIdRattachementClient(idRattachementClient);
    }
    // Le type du rattachement
    if (pGenericRecord.isPresentField("T1TRA") && pGenericRecord.isPresentField("T1RAT")) {
      EnumTypeRattachementArticle typeRattachementArticle =
          (EnumTypeRattachementArticle.valueOfByCode(pGenericRecord.getCharacterValue("T1TRA")));
      IdRattachementArticle idRattachementArticle =
          IdRattachementArticle.getInstance(typeRattachementArticle, pGenericRecord.getStringValue("T1RAT"));
      parametreConditionVente.setIdRattachementArticle(idRattachementArticle);
    }
    // Le code devise
    if (pGenericRecord.isPresentField("T1DEV")) {
      parametreConditionVente.setCodeDevise(pGenericRecord.getStringValue("T1DEV"));
    }
    
    // Car particulier car la quantité n'est pas du même type entre la PGVMCNVM (String) et la PGVMCNQM (BigDecimal)
    // La quantité
    if (prefixe.isEmpty()) {
      // Champ de la table PGVMCNVM
      if (pGenericRecord.isPresentField("T1QTE")) {
        String quantite = pGenericRecord.getStringValue("T1QTE");
        // Contrôle de la présence d'une valeur avant de convertir
        if (!Constantes.normerTexte(quantite).isEmpty()) {
          parametreConditionVente.setQuantiteMinimale(Constantes.convertirTexteEnBigDecimal(quantite));
        }
      }
    }
    else {
      // Champ de la table PGVMCNQM
      if (pGenericRecord.isPresentField("TQQTE")) {
        parametreConditionVente.setQuantiteMinimale(pGenericRecord.getBigDecimalValue("TQQTE"));
      }
    }
    
    // Soit avec les champs T1
    if (avecT1) {
      // Le type de condition (ne peut pas valoir ' ' car ce type n'existe pas)
      EnumTypeConditionVente typeConditionVente = null;
      if (pGenericRecord.isPresentField(prefixe + "T1TCD")) {
        Character codeType = pGenericRecord.getCharacterValue(prefixe + "T1TCD");
        if (codeType.charValue() != ' ') {
          typeConditionVente = EnumTypeConditionVente.valueOfByCode(codeType);
          parametreConditionVente.setTypeCondition(typeConditionVente);
        }
      }
      
      // La valeur
      if (pGenericRecord.isPresentField(prefixe + "T1VAL")) {
        BigDecimal valeur = pGenericRecord.getBigDecimalValue(prefixe + "T1VAL");
        parametreConditionVente.modifierValeur(valeur, typeConditionVente);
      }
      // Les remises
      for (int i = 1; i <= 6; i++) {
        String nomChamp = prefixe + "T1REM" + i;
        if (pGenericRecord.isPresentField(nomChamp)) {
          BigPercentage pourcentage = new BigPercentage(pGenericRecord.getBigDecimalValue(nomChamp), EnumFormatPourcentage.POURCENTAGE);
          if (!pourcentage.isZero()) {
            parametreConditionVente.setTauxRemise(pourcentage, i);
          }
        }
      }
      
      // Le coefficient
      if (pGenericRecord.isPresentField(prefixe + "T1COE")) {
        BigDecimal coefficient = pGenericRecord.getBigDecimalValue(prefixe + "T1COE");
        if (coefficient != null) {
          parametreConditionVente.setCoefficient(coefficient.abs());
        }
      }
      // Le code du paramètre 'FP'
      if (pGenericRecord.isPresentField(prefixe + "T1FPR")) {
        parametreConditionVente.setCodeFormulePrix(pGenericRecord.getStringValue(prefixe + "T1FPR"));
      }
      // Les dates de validité
      if (t1dtd != 0) {
        parametreConditionVente.setDateDebutValidite(pGenericRecord.getDateValue(prefixe + "T1DTD"));
      }
      if (t1dtf != valeurMax) {
        parametreConditionVente.setDateFinValidite(pGenericRecord.getDateValue(prefixe + "T1DTF"));
      }
    }
    // Soit avec les champs T2
    else {
      // Le type de condition
      EnumTypeConditionVente typeConditionVente = null;
      if (pGenericRecord.isPresentField(prefixe + "T2TCD")) {
        Character codeType = pGenericRecord.getCharacterValue(prefixe + "T2TCD");
        // Cas particulier pour éliminer des valeurs merdiques dans ce champ
        // A supprimer lorsque la cause de l'insertion sera identifiée et qu'un rec pour corriger les données sera fait
        if (codeType == ',') {
          codeType = ' ';
        }
        // Cas normal
        if (codeType.charValue() != ' ') {
          typeConditionVente = EnumTypeConditionVente.valueOfByCode(codeType);
          parametreConditionVente.setTypeCondition(typeConditionVente);
        }
      }
      // La valeur
      if (pGenericRecord.isPresentField(prefixe + "T2VAL")) {
        BigDecimal valeur = pGenericRecord.getBigDecimalValue(prefixe + "T2VAL");
        parametreConditionVente.modifierValeur(valeur, typeConditionVente);
      }
      // Les remises
      for (int i = 1; i <= 6; i++) {
        String nomChamp = prefixe + "T2REM" + i;
        if (pGenericRecord.isPresentField(nomChamp)) {
          BigPercentage pourcentage = new BigPercentage(pGenericRecord.getBigDecimalValue(nomChamp), EnumFormatPourcentage.POURCENTAGE);
          if (!pourcentage.isZero()) {
            parametreConditionVente.setTauxRemise(pourcentage, i);
          }
        }
      }
      // Le coefficient
      if (pGenericRecord.isPresentField(prefixe + "T2COE")) {
        BigDecimal coefficient = pGenericRecord.getBigDecimalValue(prefixe + "T2COE");
        if (coefficient != null) {
          parametreConditionVente.setCoefficient(coefficient.abs());
        }
      }
      // Le code du paramètre 'FP'
      if (pGenericRecord.isPresentField(prefixe + "T2FPR")) {
        parametreConditionVente.setCodeFormulePrix(pGenericRecord.getStringValue(prefixe + "T2FPR"));
      }
      // Les dates de validité
      if (t2dtd != 0) {
        parametreConditionVente.setDateDebutValidite(pGenericRecord.getDateValue(prefixe + "T2DTD"));
      }
      if (t2dtf != valeurMax) {
        parametreConditionVente.setDateFinValidite(pGenericRecord.getDateValue(prefixe + "T2DTF"));
      }
    }
    
    return parametreConditionVente;
  }
  
  /**
   * Renseigner les données d'un paramètre chantier à partir d'un GenericRecord.
   * 
   * @param pIdChantier Identifiant chantier.
   * @param pGenericRecord Résultat SQL.
   * @return Paramètre chantier.
   */
  private ParametreChantier completerParametreChantier(IdChantier pIdChantier, GenericRecord pGenericRecord) {
    ParametreChantier articleChantier = new ParametreChantier();
    
    // Identifiant chantier
    articleChantier.setIdChantier(pIdChantier);
    
    // Le code article
    if (pGenericRecord.isPresentField("L1ART")) {
      IdArticle idArticle = IdArticle.getInstance(pIdChantier.getIdEtablissement(), pGenericRecord.getStringValue("L1ART", null, true));
      articleChantier.setIdArticle(idArticle);
    }
    
    // Le numéro de la colonne
    if (pGenericRecord.isPresentField("L1TAR")) {
      articleChantier.setNumeroColonneTarif(pGenericRecord.getIntegerValue("L1TAR"));
    }
    
    // Le prix de base HT
    if (pGenericRecord.isPresentField("L1PVB")) {
      articleChantier.setPrixBaseHT(pGenericRecord.getBigDecimalValue("L1PVB"));
    }
    
    // Le prix de vente HT
    if (pGenericRecord.isPresentField("L1PVC")) {
      articleChantier.setPrixNetHT(pGenericRecord.getBigDecimalValue("L1PVC"));
    }
    
    // L'unité de vente
    if (pGenericRecord.isPresentField("L1UNV")) {
      articleChantier.setCodeUniteVente(pGenericRecord.getStringValue("L1UNV", "", true));
    }
    
    // La quantité
    if (pGenericRecord.isPresentField("L1QTE")) {
      articleChantier.setQuantiteManinimum(pGenericRecord.getBigDecimalValue("L1QTE"));
    }
    
    // Le type de la condition
    if (pGenericRecord.isPresentField("L1IN20")) {
      Character code = pGenericRecord.getCharacterValue("L1IN20");
      articleChantier.setTypeChantier(EnumTypeChantier.valueOfByCode(code));
    }
    
    return articleChantier;
  }
  
}
