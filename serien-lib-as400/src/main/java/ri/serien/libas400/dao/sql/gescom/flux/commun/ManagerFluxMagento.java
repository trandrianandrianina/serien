/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import magento.EndSession;
import magento.Login;
import magento.LoginResponse;
import magento.MagentoServiceStub;
import magento.SerieNReceive;
import magento.SerieNReceiveResponse;
import org.apache.axis2.AxisFault;

import ri.serien.libas400.Avertissement;
import ri.serien.libas400.dao.sql.exploitation.environnement.SqlEnvironnement;
import ri.serien.libas400.dao.sql.exploitation.parametres.SqlExploitationParametres;
import ri.serien.libas400.dao.sql.gescom.flux.CodeRetourMagento;
import ri.serien.libas400.dao.sql.gescom.flux.ParametreFTP;
import ri.serien.libas400.dao.sql.gescom.flux.ParametreFlux;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxInit;
import ri.serien.libas400.dao.sql.gescom.flux.SqlFlux;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonClientWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonListePrixWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonStockWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonSuiviCommandeWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx001.v1.ExportationArticleV1;
import ri.serien.libas400.dao.sql.gescom.flux.flx001.v2.ExportationArticleMagentoV2;
import ri.serien.libas400.dao.sql.gescom.flux.flx002.v1.ImportationCommandeMagentoV1;
import ri.serien.libas400.dao.sql.gescom.flux.flx002.v2.ImportationCommandeMagentoV2;
import ri.serien.libas400.dao.sql.gescom.flux.flx002.v3.ImportationCommandeWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx003.v1.ExportationSuiviCommandeV1;
import ri.serien.libas400.dao.sql.gescom.flux.flx003.v2.ExportationSuiviCommandeV2;
import ri.serien.libas400.dao.sql.gescom.flux.flx003.v3.ExportationSuiviCommandeV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx003.v3.ListeFluxSuiviCommandeV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx004.v1.ExportationClientMagentoV1;
import ri.serien.libas400.dao.sql.gescom.flux.flx004.v1.ImportationClientMagentoV1;
import ri.serien.libas400.dao.sql.gescom.flux.flx004.v2.ExportationClientMagentoV2;
import ri.serien.libas400.dao.sql.gescom.flux.flx004.v2.ImportationClientMagentoV2;
import ri.serien.libas400.dao.sql.gescom.flux.flx004.v3.ExportationClientWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx004.v3.ImportationClientWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx004.v3.ListeFluxClientV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx005.v1.GM_Export_FacturesV1;
import ri.serien.libas400.dao.sql.gescom.flux.flx005.v2.ExportFactureMagentoV2;
import ri.serien.libas400.dao.sql.gescom.flux.flx006.v1.ExportationStockMagentoV1;
import ri.serien.libas400.dao.sql.gescom.flux.flx006.v2.ExportationStockMagentoV2;
import ri.serien.libas400.dao.sql.gescom.flux.flx006.v3.ExportationStockWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx006.v3.FluxStockV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx007.v1.GM_Export_SuiviClientV1;
import ri.serien.libas400.dao.sql.gescom.flux.flx007.v2.ExportSuiviClientV2;
import ri.serien.libas400.dao.sql.gescom.flux.flx008.v2.GM_Import_Cdes_EDI;
import ri.serien.libas400.dao.sql.gescom.flux.flx009.v2.GM_Export_Expe_EDI;
import ri.serien.libas400.dao.sql.gescom.flux.flx010.v2.GM_Export_Factu_EDI;
import ri.serien.libas400.dao.sql.gescom.flux.flx012.v2.GestionFLX012;
import ri.serien.libas400.dao.sql.gescom.flux.flx013.v3.ExportationPrixWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.flx013.v3.FluxPrixV3;
import ri.serien.libas400.dao.sql.gescom.parametres.SqlGescomParametres;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.bibliotheque.BibliothequeAS400;
import ri.serien.libas400.system.job.SurveillanceJob;
import ri.serien.libcommun.commun.bdd.EnumStatutBDD;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.exploitation.flux.LienFluxTiersWebSerieN;
import ri.serien.libcommun.exploitation.flux.ListeLienFluxTiersWebSerieN;
import ri.serien.libcommun.exploitation.mail.ParametreCreationMail;
import ri.serien.libcommun.exploitation.notification.EnumPrioriteNotification;
import ri.serien.libcommun.exploitation.notification.ParametreCreationNotification;
import ri.serien.libcommun.exploitation.personnalisation.flux.ListePersonnalisationFlux;
import ri.serien.libcommun.exploitation.personnalisation.flux.PersonnalisationFlux;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.IdTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.ListeTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.TypeFacturation;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Cette classe gère l'ensemble des traitements des flux. Elle contient la logique "métier".
 * TODO A revoir completement.
 */
public class ManagerFluxMagento {
  // Constantes
  private static final int TRAITEMENT_NOK = 0;
  private static final int TRAITEMENT_OK = 1;
  private static final int TRAITEMENT_A_IGNORER = 2;
  
  // Variables
  private QueryManager queryManager = null;
  private SystemeManager systemeManager = null;
  private ParametresFluxBibli parametresFlux = null;
  private String lettreEnvironnement = null;
  private Bibliotheque baseDeDonnees = null;
  private FluxMagento fluxMagento = null;
  private SqlFlux sqlFlux = null;
  // Conserve le dernier message d'erreur émit et non lu
  private String msgErreur = "";
  private GestionRetourWS gestionRetourWS = null;
  
  private MagentoServiceStub stub = null;
  private String versionFlux = "1.0";
  
  private ArrayList<Integer> sensEnAttente = new ArrayList<Integer>();
  private EnumStatutBDD statutBaseDeDonneesPrecedent = EnumStatutBDD.NORMAL;
  private List<FluxMagento> listeFluxMagentoEnCours = new ArrayList<FluxMagento>();
  
  private GsonBuilder builderJSON = null;
  private Gson gson = null;
  
  // Listes pour les flux V3
  private ListeFluxClientV3 listeFluxClientV3 = null;
  private ListeFluxSuiviCommandeV3 listeFluxSuiviCommandeV3 = null;
  
  public static ListeLienFluxTiersWebSerieN listeLienFluxTiersWebSerieN = null;
  public static ListePersonnalisationFlux listePersonnalisationFlux = null;
  private static SurveillanceJob surveillanceJob = null;
  private static int nombreManagerFluxMagento = 0;
  private static int compteurIdManagerFluxMagento = 0;
  
  /**
   * Constructeur uniquement pour les WEBservices.
   * A modifier dès que possible <-- Pourquoi ?
   */
  public ManagerFluxMagento(String pNomBaseDeDonnees) {
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(pNomBaseDeDonnees);
    baseDeDonnees = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES);
    parametresFlux = initParametres(baseDeDonnees);
    
    // Initialisation des diverses connexions à la base
    systemeManager = new SystemeManager(true);
    queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
    queryManager.setLibrary(pNomBaseDeDonnees);
    sqlFlux = new SqlFlux(queryManager);
    
    // Chargement variables de l'environnement
    lettreEnvironnement = parametresFlux.getLettreEnvironnement();
    versionFlux = parametresFlux.getVersionFlux();
    chargerBibliothequeEnvironnement(lettreEnvironnement);
    
    // Chargement des instances pour les liens
    chargerListeLienFluxTiersWebSerieN();
    
    // Chargement des personnalisations des flux
    Trace.info("Chargement parametre FX (ManagerFluxMagento-String)");
    chargerPersonnalisationFlux();
    
    nombreManagerFluxMagento++;
    compteurIdManagerFluxMagento = nombreManagerFluxMagento;
  }
  
  /**
   * Constructeur.
   */
  public ManagerFluxMagento(ParametresFluxBibli pParametreFlux) {
    // Initialisation des diverses connexions à la base
    parametresFlux = pParametreFlux;
    systemeManager = new SystemeManager(true);
    queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
    baseDeDonnees = parametresFlux.getLibrairie();
    if (baseDeDonnees != null) {
      queryManager.setLibrary(baseDeDonnees.getNom());
    }
    sqlFlux = new SqlFlux(queryManager);
    
    // Chargement variables de l'environnement
    lettreEnvironnement = parametresFlux.getLettreEnvironnement();
    versionFlux = parametresFlux.getVersionFlux();
    chargerBibliothequeEnvironnement(lettreEnvironnement);
    
    // Chargement des instances pour les liens
    chargerListeLienFluxTiersWebSerieN();
    
    // Chargement des personnalisations des flux
    Trace.info("Chargement parametre FX (ManagerFluxMagento-ParametresFluxBibli)");
    chargerPersonnalisationFlux();
    
    nombreManagerFluxMagento++;
    compteurIdManagerFluxMagento = nombreManagerFluxMagento;
  }
  
  /**
   * Retourne un enregistrement.
   */
  public FluxMagento getRecord(boolean pNouveau) {
    if (pNouveau || fluxMagento == null) {
      fluxMagento = new FluxMagento(queryManager, versionFlux);
    }
    return fluxMagento;
  }
  
  /**
   * Ajout d'un enregistrement ManagerFluxMagento (avant traitement).
   */
  public boolean insererFluxDansTableNonTraite(FluxMagento pFluxMagento) {
    if (pFluxMagento == null) {
      majError("[ManagerFluxMagento] insererFluxDansTableNonTraite() - L'objet pFluxMagento est invalide.");
      return false;
    }
    
    try {
      // Si c'est un flux automatique pas besoin de récupérer l'établissement
      if (!pFluxMagento.isFluxAutomatique() && !interpreterInstanceVersEtb(pFluxMagento)) {
        return false;
      }
      
      fluxMagento = pFluxMagento;
      fluxMagento.setFLCRE(new Timestamp(new Date().getTime()));
      fluxMagento.setFLTRT(new Timestamp(new Date().getTime()));
      
      // Initialisation du statut
      if (testerSiSensEnAttente(fluxMagento.getFLSNS())) {
        fluxMagento.setFLSTT(EnumStatutFlux.EN_ATTENTE);
      }
      else {
        fluxMagento.setFLSTT(EnumStatutFlux.A_TRAITER);
      }
      fluxMagento.setFLERR(FluxMagento.ERR_PAS_ERREUR);
      fluxMagento.setFLCPT(0);
      
      // Insertion de l'enregistrement
      if (!fluxMagento.insertInDatabase()) {
        majError("[ManagerFluxMagento] insererFluxDansTableNonTraite() - Erreur lors de l'insertion : " + fluxMagento.getMsgError());
        return false;
      }
      
      return true;
    }
    catch (Exception e) {
      majError("[ManagerFluxMagento] insererFluxDansTableNonTraite() - " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Ajout d'un enregistrement de type flux dans la table des flux pour les flux internes Programme Java -> gestionnaire de flux : pas de
   * json pas besoin de récupérer l'établissement.
   */
  public boolean addRecordInterne(FluxMagento pFluxMagento) {
    if (pFluxMagento == null) {
      majError("(addRecordInterne) Le flux transmis est null");
      return false;
    }
    
    if (pFluxMagento.getFLETB() == null) {
      majError("(addRecordInterne) L'établissement du flux transmis est null");
      return false;
    }
    
    if (pFluxMagento.getFLCOD() == null || pFluxMagento.getFLIDF() == null || pFluxMagento.getFLSNS() < 0
        || pFluxMagento.getFLSTT() == null) {
      majError("(addRecordInterne) Il manque des données pour enregistrer ce flux : pFlux.getFLCOD() : " + pFluxMagento.getFLCOD()
          + " - pFlux.getFLIDF() : " + pFluxMagento.getFLIDF() + " - pFlux.getFLSNS() : " + pFluxMagento.getFLSNS()
          + "  - pFlux.getFLSTT() : " + pFluxMagento.getFLSTT());
      return false;
    }
    try {
      fluxMagento = pFluxMagento;
      if (fluxMagento == null) {
        majError("(addRecordInterne) La classe ManagerFluxMagento n'est pas instanciée");
        return false;
      }
      
      fluxMagento.setFLCRE(new Timestamp(new Date().getTime()));
      fluxMagento.setFLTRT(new Timestamp(new Date().getTime()));
      
      if (testerSiSensEnAttente(fluxMagento.getFLSNS())) {
        fluxMagento.setFLSTT(EnumStatutFlux.EN_ATTENTE);
      }
      else {
        fluxMagento.setFLSTT(EnumStatutFlux.A_TRAITER);
      }
      fluxMagento.setFLERR(FluxMagento.ERR_PAS_ERREUR);
      fluxMagento.setFLCPT(0);
      fluxMagento.setFLJSN("");
      
      if (!fluxMagento.insertInDatabase()) {
        majError("(addRecordInterne) Erreur lors de l'insert : " + fluxMagento.getFLMSGERR());
        return false;
      }
      
      return true;
    }
    catch (Exception e) {
      majError("[LA_FluxMagento](addRecordInterne) " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Teste si le sens d'un flux est un flux qui doit se mettre en attente.
   */
  public boolean testerSiSensEnAttente(int pSens) {
    boolean trouve = false;
    if (sensEnAttente != null) {
      int i = 0;
      while (i < sensEnAttente.size() && !trouve) {
        trouve = (sensEnAttente.get(i) == pSens);
        i++;
      }
    }
    return trouve;
  }
  
  /**
   * Mise à jour d'un enregistrement FluxMagento.
   */
  public boolean updateRecord(FluxMagento pFluxMagento) {
    try {
      if (pFluxMagento == null) {
        majError("(updateRecord) La classe ManagerFluxMagento n'est pas instanciée");
        return false;
      }
      
      if (!pFluxMagento.updateInDatabase()) {
        majError(
            "[ManagerFluxMagento](updateRecord) Erreur lors de l'update " + pFluxMagento.getFLID() + ": " + fluxMagento.getMsgError());
        if (!fluxMagento.updateRestreintInDatabase()) {
          majError("[ManagerFluxMagento](updateRecord) Erreur lors de l'update RESTREINT " + pFluxMagento.getFLID() + ": "
              + fluxMagento.getMsgError());
        }
        
        return false;
      }
    }
    catch (Exception e) {
      majError("(updateRecord) " + e.getMessage());
      // TODO on tente une mise à jour restreinte
      if (pFluxMagento != null && !pFluxMagento.updateRestreintInDatabase()) {
        majError("[ManagerFluxMagento](updateRecord) Erreur lors de l'update RESTREINT " + pFluxMagento.getFLID() + ": "
            + fluxMagento.getMsgError());
      }
      
      // De toute manière on remonte le souci
      return false;
    }
    msgErreur = "";
    return true;
  }
  
  /**
   * Retourne la liste des identifiants des enregistrements (contenu partiel: FLID + FLIDF) à traiter.
   */
  public FluxMagento[] chargerListeFluxATraiter(int pSens) {
    try {
      // Construction de la requête
      RequeteSql requeteSql = new RequeteSql();
      requeteSql.ajouter("select FLID, FLIDF, FLETB, FLCOD, FLSNS, FLVER, FLJSN, FLSTT, FLCPT from " + queryManager.getLibrary() + "."
          + EnumTableBDD.FLUX + " where");
      requeteSql.ajouterConditionAnd("FLSTT", "=", EnumStatutFlux.A_TRAITER.getNumero());
      requeteSql.ajouterConditionAnd("FLSNS", "=", pSens);
      requeteSql.ajouterConditionAnd("FLDECL", "<", "CURRENT TIMESTAMP", true, false);
      requeteSql.ajouter(" order by FLCRE asc fetch first " + parametresFlux.getNbFluxMaxBoucle() + " rows only optimize for "
          + parametresFlux.getNbFluxMaxBoucle() + " rows");
      
      // Execution de la requête
      ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequete());
      if (listeRecord == null) {
        majError("(chargerListeFluxATraiter) " + queryManager.getMsgError());
        return null;
      }
      
      // Instanciation des classes
      FluxMagento[] listeFlux = new FluxMagento[listeRecord.size()];
      for (int i = 0; i < listeRecord.size(); i++) {
        FluxMagento fluxMagento = new FluxMagento(queryManager, versionFlux, pSens);
        fluxMagento.completerFluxMagento(listeRecord.get(i));
        listeFlux[i] = fluxMagento;
      }
      return listeFlux;
    }
    catch (Exception e) {
      majError("(chargerListeFluxATraiter) " + e.getMessage());
      return null;
    }
  }
  
  /**
   * Retourne s'il existe des enregistrements récurrents identiques qui ne sont pas traités.
   */
  public boolean existeDesFluxIdentiquesNonTraites(String pTypeFlux) {
    if (pTypeFlux == null) {
      majError("(existeDesFluxIdentiquesNonTraites) typeFlux NULL ");
      return false;
    }
    
    try {
      String requete = "select FLID, FLIDF, FLVER, FLETB, FLCOD, FLSNS, FLJSN, FLSTT, FLCPT from " + queryManager.getLibrary() + "."
          + EnumTableBDD.FLUX + " where FLSTT = " + EnumStatutFlux.A_TRAITER.getNumero() + " and FLIDF = '" + pTypeFlux + "' ";
      ArrayList<GenericRecord> listeRecord = queryManager.select(requete);
      if (listeRecord == null || listeRecord.isEmpty()) {
        return false;
      }
      return true;
    }
    catch (Exception e) {
      majError("(existeDesFluxIdentiquesNonTraites) " + e.getMessage());
    }
    return false;
  }
  
  /**
   * Retourne la liste des enregistrements (contenu partiel: FLID + FLIDF) à traiter avec plusieurs sens en paramètres.
   */
  public FluxMagento[] getListIdToTreat(int[] pSens) {
    FluxMagento[] listFinale = null;
    ArrayList<FluxMagento> listeDynamique = null;
    
    if (pSens != null && pSens.length > 0) {
      listeDynamique = new ArrayList<FluxMagento>();
      
      for (int i = 0; i < pSens.length; i++) {
        FluxMagento[] listPartielle = chargerListeFluxATraiter(pSens[i]);
        if (listPartielle != null && listPartielle.length > 0) {
          for (int j = 0; j < listPartielle.length; j++) {
            listeDynamique.add(listPartielle[j]);
          }
        }
      }
      
      listFinale = new FluxMagento[listeDynamique.size()];
      for (int i = 0; i < listeDynamique.size(); i++) {
        listFinale[i] = listeDynamique.get(i);
      }
    }
    
    return listFinale;
  }
  
  /**
   * Mise en attente tous les flux d'un sens donné.
   */
  public boolean mettreEnAttenteFluxPourUnSens(int pSens, int pIdFlux) {
    try {
      FluxMagento[] list = chargerListeFluxATraiter(pSens);
      if (list != null) {
        // On rajoute le sens dans la liste des sens pour bloquer les autres flux identiques
        rajouterUnSensEnAttente(pSens);
        
        for (int i = 0; i < list.length; i++) {
          // On ne met pas à jour le flux initial qui ne passe pas à l'envoi seulement les autres du même FLIDF
          if (pIdFlux != list[i].getFLID()) {
            list[i].setFLSTT(EnumStatutFlux.EN_ATTENTE);
            updateRecord(list[i]);
          }
        }
      }
      else {
        majError("(mettreEnAttenteFluxPourUnSens) " + queryManager.getMsgError());
        return false;
      }
      
      return true;
      
    }
    catch (Exception e) {
      majError("(mettreEnAttenteFluxPourUnSens) " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Débloquer les flux mis en attente pour un sens donné.
   */
  public boolean debloquerFluxPourUnSens(int pSens, int pIdFlux) {
    
    try {
      FluxMagento[] list = retournerListeEnAttente(pSens);
      if (list != null) {
        // On retire le sens de lliste des flux à mettre en attente
        retirerUnSensEnAttente(pSens);
        
        for (int i = 0; i < list.length; i++) {
          // On ne met pas à jour le flux initial qui ne passe pas à l'envoi seulement les autres flux du même FLIDF
          if (pIdFlux != list[i].getFLID()) {
            list[i].setFLSTT(EnumStatutFlux.A_TRAITER);
            updateRecord(list[i]);
          }
        }
      }
      else {
        majError("(debloquerFluxPourUnSens) " + queryManager.getMsgError());
        return false;
      }
      
      return true;
    }
    catch (Exception e) {
      majError("(debloquerFluxPourUnSens) " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Retourne la liste des flux en attente pour un sens.
   */
  public FluxMagento[] retournerListeEnAttente(int pSens) {
    if (queryManager == null) {
      return null;
    }
    
    try {
      String requete = "SELECT FLID, FLIDF, FLVER, FLETB, FLCOD, FLSNS, FLJSN, FLSTT, FLCPT FROM " + queryManager.getLibrary() + "."
          + EnumTableBDD.FLUX + " WHERE FLSTT = " + EnumStatutFlux.EN_ATTENTE.getNumero() + " and FLSNS = " + pSens
          + " ORDER BY FLCRE ASC ";
      ArrayList<GenericRecord> listrcd = queryManager.select(requete);
      if (listrcd == null) {
        majError("(retournerListeEnAttente) " + queryManager.getMsgError());
        return null;
      }
      FluxMagento[] list = new FluxMagento[listrcd.size()];
      // On instancie les classes
      for (int i = 0; i < listrcd.size(); i++) {
        FluxMagento c = new FluxMagento(queryManager, versionFlux, pSens);
        c.completerFluxMagento(listrcd.get(i));
        list[i] = c;
      }
      return list;
    }
    catch (Exception e) {
      majError("(retournerListeEnAttente) " + e.getMessage());
      return null;
    }
  }
  
  /**
   * Traitement des flux reçus depuis Magento.
   */
  public int traiterFluxRecuDepuisMagento() {
    // On lance une requête afin de récupérer les id + ifFlux des flux à traiter
    FluxMagento[] listPartialRcd = chargerListeFluxATraiter(FluxMagento.SNS_VERS_SERIEN);
    if (listPartialRcd == null) {
      return -1;
    }
    
    boolean retour = false;
    long debut = 0;
    long fin = 0;
    // Choix du traitement en fonction de l'Idf du flux
    for (FluxMagento partialRcd : listPartialRcd) {
      debut = System.currentTimeMillis();
      
      Trace.info(" id:" + compteurIdManagerFluxMagento + " traiterFluxRecuDepuisMagento() debut Flux " + partialRcd.getFLID());
      
      // On s'assure que l'établissement est bien saisi pour les flux rentrants
      // En effet problème avec la version du serveur de Web services
      if (!partialRcd.isFluxAutomatique()) {
        if (!interpreterInstanceVersEtb(partialRcd)) {
          partialRcd.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
          partialRcd.setFLERR(FluxMagento.ERR_TRAITEMENT);
          valider(partialRcd);
          updateRecord(partialRcd);
          return 0;
        }
      }
      
      // Controle du numéro de la version du flux en cours
      if (!controlerVersion(partialRcd)) {
        Trace.info(" id: " + compteurIdManagerFluxMagento + " traiterFluxRecuDepuisMagento() controlerVersion: " + partialRcd.getFLID());
        retour = false;
      }
      // Traitement du flux en fonction de son type (IDF)
      else {
        // Changement du statut du flux afin d'indiquer qu'il est en cours de traitement
        partialRcd.modifierStatutEnCours();
        // Lancement du traitement adéquat
        if (partialRcd.getFLIDF().trim().equalsIgnoreCase(FluxMagento.IDF_FLX002_COMMANDE)) {
          retour = traiterFlux002Commande(partialRcd);
        }
        else if (partialRcd.getFLIDF().trim().equalsIgnoreCase(FluxMagento.IDF_FLX004_CLIENT)) {
          retour = traiterFlux004ClientRentrant(partialRcd);
        }
      }
      
      // Le résultat du traitement va déterminer si on envoie ou pas
      if (retour) {
        valider(partialRcd);
        updateRecord(partialRcd);
      }
      else {
        bloquertraitement(partialRcd);
      }
      
      // Libère les ressources
      partialRcd.dispose();
      fin = System.currentTimeMillis();
      Trace.info(" id:" + compteurIdManagerFluxMagento + " traiterFluxRecuDepuisMagento() fin flux " + partialRcd.getFLID() + " -> "
          + (fin - debut) + "ms");
    }
    
    return listPartialRcd.length;
  }
  
  /**
   * Traitement pour envoyer les flux vers Magento.
   * 
   * @param pFluxMagento
   * @param pTypeFluxADeclencher, permet d'indiquer qu'il faut forcer le déclenchement de ce type de flux (en V3 uniquement et
   *          pour les flux non automatiques)
   * @return
   */
  public int traiterFluxAenvoyerVersMagento(FluxMagento pFluxMagento, EnumTypeFlux pTypeFluxADeclencher) {
    FluxMagento[] listeFluxMagento = null;
    // Il s'agit d'un flux avec reliquat (je ne sais pas ce que ça veut dire pour l'instant)
    if (pFluxMagento != null) {
      listeFluxMagento = new FluxMagento[1];
      listeFluxMagento[0] = pFluxMagento;
    }
    // Sinon récupération d'une liste de flux classiques à traiter
    else {
      listeFluxMagento = chargerListeFluxATraiter(FluxMagento.SNS_VERS_MAGENTO);
    }
    
    // Aucun flux à traiter
    if (listeFluxMagento == null) {
      return -1;
    }
    if (listeFluxMagento.length == 0) {
      return 0;
    }
    
    // Stockage de la date, valable pour l'ensemble de ce traitement, pour contrôler éventuellement le déclenchement d'un flux
    Date maintenant = new Date();
    // Effacement des listes des flux pour la version V3
    int versionMajeure = retournerVersionMajeure(listeFluxMagento[0].getFLVER());
    if (versionMajeure == 3) {
      // Pour les flux client
      if (listeFluxClientV3 == null) {
        listeFluxClientV3 = new ListeFluxClientV3(EnumTypeFlux.CLIENT, "FLX004_Customers_");
      }
      listeFluxClientV3.initialiser();
      // Pour les flux suivi de commande
      if (listeFluxSuiviCommandeV3 == null) {
        listeFluxSuiviCommandeV3 = new ListeFluxSuiviCommandeV3(EnumTypeFlux.SUIVI_COMMANDE, "FLX003_Orders_");
      }
      listeFluxSuiviCommandeV3.initialiser();
    }
    
    // Liste des flux trouvés à traiter
    for (FluxMagento fluxMagento : listeFluxMagento) {
      long debut = System.currentTimeMillis();
      String typeFlux = Constantes.normerTexte(fluxMagento.getFLIDF()).toUpperCase();
      Trace.info(
          " id:" + compteurIdManagerFluxMagento + " traiterFluxAenvoyer() début Flux " + fluxMagento.getFLID() + " (" + typeFlux + ')');
      
      int valeur = TRAITEMENT_NOK;
      if (typeFlux.equals(FluxMagento.IDF_FLX001_ARTICLE)) {
        valeur = traiterFlux001Article(fluxMagento);
      }
      else if (typeFlux.equals(FluxMagento.IDF_FLX003_SUIVIDMD)) {
        valeur = traiterFlux003SuiviCommande(fluxMagento, maintenant, pTypeFluxADeclencher);
      }
      else if (typeFlux.equals(FluxMagento.IDF_FLX004_CLIENT)) {
        valeur = traiterFlux004ClientSortant(fluxMagento, maintenant, pTypeFluxADeclencher);
      }
      else if (typeFlux.equals(FluxMagento.IDF_FLX005_FACTURE)) {
        valeur = traiterFlux005Facture(fluxMagento);
      }
      else if (typeFlux.equals(FluxMagento.IDF_FLX006_STOCK)) {
        valeur = traiterFlux006Stock(fluxMagento);
      }
      else if (typeFlux.equals(FluxMagento.IDF_FLX007_SUIVICLIENT)) {
        valeur = traiterFlux007SuiviClient(fluxMagento);
      }
      else if (typeFlux.equals(FluxMagento.IDF_FLX013_PRIX)) {
        valeur = traiterFlux013Prix(fluxMagento);
      }
      
      // Validation si le traitement c'est bien passé (avant l'envoi)
      boolean retour = false;
      switch (valeur) {
        case TRAITEMENT_OK:
          retour = true;
          break;
        case TRAITEMENT_NOK:
          retour = false;
          break;
        case TRAITEMENT_A_IGNORER:
          continue;
      }
      if (retour) {
        // Envoi du flux et envoi d'un mail si un problème a été détecté
        if (!valider(fluxMagento)) {
          fluxMagento.setFLMSGERR(getMsgError());
          
          // Envoi du mail et de la notification
          String sujet = "Erreur sur le flux " + fluxMagento.getFLID();
          String message = "Le flux numéro " + fluxMagento.getFLID() + " de type " + fluxMagento.getFLIDF() + " a declenché une erreur : "
              + fluxMagento.getFLMSGERR();
          IdEtablissement idEtablissement = IdEtablissement.getInstance(fluxMagento.getFLETB());
          signalerProbleme(sujet, message, idEtablissement);
        }
        // Mise à jour de l'enregistrement
        updateRecord(fluxMagento);
      }
      else {
        bloquertraitement(fluxMagento);
      }
      
      // Tracer la fin du traitement du flux
      long fin = System.currentTimeMillis();
      if (retour) {
        Trace.info(" id:" + compteurIdManagerFluxMagento + " traiterFluxAenvoyer() fin Flux " + fluxMagento.getFLID() + " -> "
            + (fin - debut) + "ms");
      }
      else {
        Trace.erreur(" id:" + compteurIdManagerFluxMagento + " traiterFluxAenvoyer() ERREUR flux " + fluxMagento.getFLID() + " -> "
            + (fin - debut) + "ms");
      }
      
      // Libère les ressources
      fluxMagento.dispose();
    }
    
    // Traitement des listes de flux V3 si besoin
    if (versionMajeure == 3) {
      // Pour les flux client
      if (listeFluxClientV3 != null && !listeFluxClientV3.isEmpty()) {
        listeFluxClientV3.transfererVersServeur();
      }
      // Pour les flux suivi de commande
      if (listeFluxSuiviCommandeV3 != null && !listeFluxSuiviCommandeV3.isEmpty()) {
        listeFluxSuiviCommandeV3.transfererVersServeur();
      }
    }
    
    return listeFluxMagento.length;
  }
  
  /**
   * Traitements propres à l'EDI.
   */
  public int traiterFluxEDI() {
    int[] tabSens = { FluxMagento.SNS_EDI_RECEP, FluxMagento.SNS_EDI_ENVOI };
    FluxMagento[] listPartialRcd = getListIdToTreat(tabSens);
    if (listPartialRcd == null) {
      return -1;
    }
    
    boolean retour = false;
    
    for (FluxMagento partialRcd : listPartialRcd) {
      if (partialRcd.getFLIDF().trim().equalsIgnoreCase(FluxMagento.IDF_FLX008_EDI_ORDERS)) {
        retour = traiterFlux008CommandeEDI(partialRcd);
      }
      else if (partialRcd.getFLIDF().trim().equalsIgnoreCase(FluxMagento.IDF_FLX009_EDI_DESADV)) {
        retour = traiterFlux009ExpeditionEDI(partialRcd);
      }
      else if (partialRcd.getFLIDF().trim().equalsIgnoreCase(FluxMagento.IDF_FLX010_EDI_FACTU)) {
        retour = traiterFlux010FacturationEDI(partialRcd);
      }
      else if (partialRcd.getFLIDF().trim().equalsIgnoreCase(FluxMagento.IDF_FLX012_FAC_YOOZ)) {
        retour = traiterFLX012FactureYooz(partialRcd);
      }
      
      // Le résultat du traitement va déterminer si on recoit ou pas
      if (retour) {
        valider(partialRcd);
      }
      else {
        bloquertraitement(partialRcd);
      }
      
      partialRcd.dispose();
    }
    
    return listPartialRcd.length;
  }
  
  /**
   * Extraction du reliquat d'un flux trop important et de créer un nouveau flux identique qui contient ce reliquat.
   */
  public void traiterReliquatFluxSortant(FluxMagento pFluxInitial, List<Object> pListeReliquat) {
    if (pFluxInitial == null || pListeReliquat == null) {
      return;
    }
    
    FluxMagento fluxReliquat = new FluxMagento(queryManager, versionFlux, pFluxInitial.getFLSNS());
    fluxReliquat.setFLIDF(pFluxInitial.getFLIDF());
    fluxReliquat.setFLETB(pFluxInitial.getFLETB());
    fluxReliquat.setFLVER(pFluxInitial.getFLVER());
    fluxReliquat.setFLSNS(pFluxInitial.getFLSNS());
    fluxReliquat.setFLSTT(pFluxInitial.getFLSTT());
    fluxReliquat.setListeReliquat(pListeReliquat);
    fluxReliquat.setFLCOD("" + pListeReliquat.size());
    // Enregistrement du flux en base
    if (addRecordInterne(fluxReliquat)) {
      traiterFluxAenvoyerVersMagento(fluxReliquat, null);
    }
  }
  
  /**
   * Retourne le numéro de l'instance à partir du nom de la base de données et du code établissement.
   */
  public static Integer retournerInstanceLien(String pNomBibliotheque, String pCodeEtablissement) {
    try {
      IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(pNomBibliotheque);
      Bibliotheque baseDeDonnees = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES);
      IdEtablissement idEtablissement = IdEtablissement.getInstance(pCodeEtablissement);
      
      LienFluxTiersWebSerieN lien = listeLienFluxTiersWebSerieN.retournerLien(baseDeDonnees, idEtablissement);
      if (lien != null) {
        return lien.getId().getNumeroInstance();
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la recherche du lien entre le tiers web et Série N.");
    }
    return null;
  }
  
  /**
   * Contrôle la présence des clés de paramètres dans la table PSEMFXPA Cette table est la table des Flux.
   */
  public static ParametresFluxBibli initParametres(Bibliotheque pBaseDeDonnees) {
    ParametresFluxBibli parametres = null;
    ArrayList<ParametreFlux> parametresInitiaux = ParametresFluxInit.recupererParametresInitiaux();
    if (parametresInitiaux != null) {
      try {
        SystemeManager systemeManager = new SystemeManager(true);
        QueryManager queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
        queryManager.setLibrary(pBaseDeDonnees.getNom());
        
        ArrayList<ParametreFlux> listeParametres = new ArrayList<ParametreFlux>();
        
        int i = 0;
        int traitement = 0;
        while (i < parametresInitiaux.size() && traitement >= 0) {
          traitement = 0;
          ParametreFlux parametre = null;
          ArrayList<GenericRecord> liste = queryManager.select("SELECT PM_ID,PM_CLE,PM_VAL,PM_TYPE FROM " + pBaseDeDonnees
              + ".PSEMFXPA WHERE PM_CLE  = '" + parametresInitiaux.get(i).getPM_CLE() + "' ");
          if (liste != null) {
            // Ce paramètre n'existe pas dans la table
            if (liste.size() == 0) {
              traitement = queryManager.requete(
                  "INSERT INTO " + pBaseDeDonnees + ".PSEMFXPA (PM_CLE,PM_VAL,PM_TYPE) VALUES ('" + parametresInitiaux.get(i).getPM_CLE()
                      + "','" + parametresInitiaux.get(i).getPM_VALEUR() + "','" + parametresInitiaux.get(i).getPM_TYPE() + "') ");
              if (traitement < 1) {
                Trace
                    .erreur(" initParametres() PB INSERT NOUVEAU PARAM : " + parametresInitiaux.get(i).getPM_CLE() + " -> " + traitement);
              }
              else {
                parametre = new ParametreFlux(parametresInitiaux.get(i).getPM_CLE(), parametresInitiaux.get(i).getPM_VALEUR(),
                    parametresInitiaux.get(i).getPM_TYPE(), true, null);
              }
            }
            // Ce paramètre existe dans la table PSEMFXPA
            else if (liste.size() >= 1) {
              String cle = liste.get(0).getField("PM_CLE").toString().trim();
              String valeur = null;
              if (liste.get(0).isPresentField("PM_VAL")) {
                valeur = liste.get(0).getField("PM_VAL").toString().trim();
              }
              parametre = new ParametreFlux(cle, valeur, liste.get(0).getField("PM_TYPE").toString().trim(), true, null);
              
              // Contrôle que le paramètre ne soit pas plusieurs fois dans la table
              if (liste.size() > 1) {
                Trace.alerte("Le paramètre " + cle + " se trouve " + liste.size() + " dans la table " + pBaseDeDonnees
                    + ".PSEMFXPA. Ceci n'est pas normal veuillez supprimer les enregistrements inutiles.");
              }
            }
          }
          else {
            Trace.erreur(" initParametres() PB REQUETE: " + queryManager.getMsgError());
            traitement = -1;
          }
          
          if (parametre != null) {
            Trace.debug(
                " initParametres() On rajoute un paramètre à la liste : " + parametre.getPM_CLE() + ": " + parametre.getPM_VALEUR());
            listeParametres.add(parametre);
          }
          
          i++;
        }
        
        parametres = new ParametresFluxBibli(listeParametres);
      }
      catch (Exception e) {
        Trace.erreur(" initParametres()" + e.getMessage());
      }
    }
    
    return parametres;
  }
  
  /**
   * Récupére les paramètres FTP liés à un flux.
   */
  public static ParametreFTP recupererParametreFTP(String pBaseDeDonnees, String pCodeFtp) {
    if (pBaseDeDonnees == null || pCodeFtp == null) {
      Trace.erreur(" recupererParametreFTP() code FTP " + pCodeFtp + " ou librairie " + pBaseDeDonnees + " corrompus");
      return null;
    }
    ParametreFTP parametre = null;
    SystemeManager system = new SystemeManager(true);
    QueryManager query = new QueryManager(system.getDatabaseManager().getConnection());
    query.setLibrary(pBaseDeDonnees);
    
    ArrayList<GenericRecord> listeRec = query.select("SELECT * FROM " + pBaseDeDonnees + ".PSEMFTPM WHERE FTPCOD = '" + pCodeFtp + "' ");
    if (listeRec == null) {
      Trace.erreur(" recupererParametreFTP() Erreur de récupération du paramètre FTP " + query.getMsgError());
      return null;
    }
    if (listeRec.size() == 0) {
      Trace.erreur(" recupererParametreFTP() Pas de paramètre FTP pour la bibli " + pBaseDeDonnees + " et le code " + pCodeFtp);
      return null;
    }
    if (listeRec.size() > 1) {
      Trace.erreur(" recupererParametreFTP() Plusieurs paramètres FTP pour la bibli " + pBaseDeDonnees + " et le code " + pCodeFtp);
      return null;
    }
    
    GenericRecord record = listeRec.get(0);
    parametre = new ParametreFTP(pBaseDeDonnees, pCodeFtp);
    if (record.isPresentField("FTPLIB")) {
      parametre.setLibelle(record.getField("FTPLIB").toString().trim());
    }
    if (record.isPresentField("FTPADR")) {
      parametre.setAdresse(record.getField("FTPADR").toString().trim());
    }
    if (record.isPresentField("FTPPRT")) {
      parametre.setPort(record.getField("FTPPRT").toString().trim());
    }
    if (record.isPresentField("FTPUSR")) {
      parametre.setUser(record.getField("FTPUSR").toString().trim());
    }
    if (record.isPresentField("FTPPSS")) {
      parametre.setMotPasse(record.getField("FTPPSS").toString().trim());
    }
    if (record.isPresentField("FTPMSG")) {
      parametre.setUserLocal(record.getField("FTPMSG").toString().trim());
    }
    if (record.isPresentField("FTPFLX")) {
      try {
        parametre.setType(Integer.parseInt(record.getField("FTPFLX").toString().trim()));
      }
      catch (NumberFormatException e) {
        Trace.erreur(" recupererParametreFTP() le type de échange FTP n'est pas numérique " + e.getMessage());
      }
    }
    if (record.isPresentField("FTPLOC")) {
      parametre.setRepertoireLocal(record.getField("FTPLOC").toString().trim());
    }
    if (record.isPresentField("FTPDIS")) {
      parametre.setRepertoireDistant(record.getField("FTPDIS").toString().trim());
    }
    if (record.isPresentField("FTPFIC")) {
      parametre.setNomFichier(record.getField("FTPFIC").toString().trim());
    }
    
    return parametre;
  }
  
  /**
   * Recupére la version des flux en fonction de la base de données et l'établissement.
   */
  public String retournerVersionFlux(IdEtablissement pIdEtablissement) {
    if (queryManager == null || pIdEtablissement == null) {
      majError("recupererVersionFlux() query ou etb null : " + pIdEtablissement);
      return null;
    }
    
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(queryManager.getLibrary());
    Bibliotheque baseDeDonnees = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES);
    LienFluxTiersWebSerieN lien = listeLienFluxTiersWebSerieN.retournerLien(baseDeDonnees, pIdEtablissement);
    if (lien == null || lien.getVersionFlux() == null) {
      majError("retournerVersionFlux() : la version du flux est invalide.");
      return null;
    }
    
    return lien.getVersionFlux();
  }
  
  /**
   * Contrôle le statut de la base de données.
   */
  public boolean controlerStatutBaseDeDonnees() {
    if (systemeManager == null) {
      Trace.erreur("Le systemeManager est invalide.");
      return false;
    }
    if (queryManager == null) {
      Trace.erreur("Le queryManager est invalide.");
      return false;
    }
    if (parametresFlux == null) {
      Trace.erreur("Les paramètres du flux n'ont pas été initialisés.");
      return false;
    }
    
    // Contrôle du statut de la base de données
    StringBuilder messageErreurStatut = new StringBuilder();
    EnumStatutBDD statutBaseDeDonnees = BibliothequeAS400.controlerStatut(systemeManager.getSystem(), queryManager, messageErreurStatut);
    if (statutBaseDeDonnees == EnumStatutBDD.NORMAL || statutBaseDeDonnees == EnumStatutBDD.NORMAL_ALTERNATIF) {
      statutBaseDeDonneesPrecedent = EnumStatutBDD.NORMAL;
      return true;
    }
    
    // Envoi d'une notification et d'une mail uniquement si le statut est en erreur et a changé depuis la dernère fois
    if (!statutBaseDeDonnees.equals(statutBaseDeDonneesPrecedent)) {
      String sujet = "Une anomalie de la base de données " + baseDeDonnees + " a été détectée";
      String message =
          "Le gestionnaire de flux va être mis en veille à cause d'une anomalie sur la base de données " + baseDeDonnees + '.';
      if (!Constantes.normerTexte(messageErreurStatut.toString()).isEmpty()) {
        message += "\nCause du problème : \n" + messageErreurStatut.toString();
      }
      
      // Signaler le problème aux destinataires des erreurs définit dans le PSEMFXPA
      signalerProbleme(sujet, message, null);
      
      // Ecriture d'une trace avertissant la mise en pause du traitement
      Trace.alerte("Le traitement du superviseur de flux pour la base de données " + baseDeDonnees + " a été mis en pause ("
          + statutBaseDeDonnees.getLibelle() + ')');
      
      // Une anomalie a été détectée : une surveillance de la base de données va être lancée et la thread va être mis en pause
      // Démarrage d'une thread de surveillance
      Thread surveillance = new Thread() {
        @Override
        public void run() {
          // Construction du nom du thread
          String nomThread = String.format("ControleBaseDonnees(%03d)-surveiller-" + baseDeDonnees, Thread.currentThread().getId());
          setName(nomThread);
          boolean boucle = true;
          
          // Boucle infinie
          while (boucle) {
            try {
              // Mise en veille
              Thread.sleep(60 * 1000);
              
              // Contrôle du statut de la base de données
              EnumStatutBDD statutBaseDeDonnees = BibliothequeAS400.controlerStatut(systemeManager.getSystem(), queryManager, null);
              if (statutBaseDeDonnees == EnumStatutBDD.NORMAL || statutBaseDeDonnees == EnumStatutBDD.NORMAL_ALTERNATIF) {
                // Il n'y a plus de soucis sur la base de données
                boucle = false;
                statutBaseDeDonneesPrecedent = EnumStatutBDD.NORMAL;
                Trace.alerte("Le traitement du superviseur de flux (" + getName() + ") pour la base de données " + baseDeDonnees
                    + " a été mis réactivé.");
              }
            }
            catch (Exception e) {
              // Très important de réinterrompre
              Thread.currentThread().interrupt();
            }
          }
          Trace.info("Arrêt de la surveillance de la base de données " + baseDeDonnees + '.');
        }
      };
      surveillance.start();
    }
    statutBaseDeDonneesPrecedent = statutBaseDeDonnees;
    
    return false;
  }
  
  /**
   * Contrôler le temps de traitement des flux.
   */
  public boolean controlerTempsTraitementFlux(int pTempsExecutionMaxEnSeconde) {
    if (queryManager == null) {
      Trace.erreur("Le queryManager est invalide.");
      return false;
    }
    listeFluxMagentoEnCours.clear();
    
    try {
      RequeteSql requeteSql = new RequeteSql();
      requeteSql.effacerRequete();
      requeteSql.ajouter("select FLID, FLIDF, FLTRT from " + queryManager.getLibrary() + '.' + EnumTableBDD.FLUX + " where ");
      requeteSql.ajouterConditionAnd("FLSTT", "=", EnumStatutFlux.EN_COURS.getNumero());
      requeteSql.ajouterConditionAnd("FLSNS", "=", FluxMagento.SNS_VERS_SERIEN);
      requeteSql.ajouterConditionAnd("(midnight_seconds(CURRENT TIMESTAMP) - midnight_seconds(FLTRT))", ">", pTempsExecutionMaxEnSeconde);
      ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequeteLectureSeule());
      // Aucune traitement de flux ne dépasse le temps max
      if (listeRecord == null || listeRecord.isEmpty()) {
        return true;
      }
      
      // Conversion des records en flux
      for (GenericRecord record : listeRecord) {
        FluxMagento flux = new FluxMagento(queryManager, versionFlux);
        flux.completerFluxMagento(record);
        listeFluxMagentoEnCours.add(flux);
      }
    }
    catch (Exception e) {
      // Si une exception inconnue se déclenche, retourne true pour ne pas bloquer inutilement le gestionnaire de flux
      return true;
    }
    
    return false;
  }
  
  /**
   * Signale aux personnes désignées que le gestionnaire de flux a rencontré un problème.
   */
  public void signalerProbleme(String pSujet, String pMessage, IdEtablissement pIdEtablissement) {
    if (queryManager == null) {
      Trace.erreur("Le queryManager est invalide.");
      return;
    }
    if (parametresFlux == null) {
      Trace.erreur("Les paramètres du flux n'ont pas été initialisés.");
      return;
    }
    
    // Préparation pour la notification
    try {
      // Initialisation des paramètres
      ParametreCreationNotification parametreCreationNotification = new ParametreCreationNotification();
      parametreCreationNotification.setPriorite(EnumPrioriteNotification.IMPORTANTE);
      parametreCreationNotification.setSujet(pSujet);
      parametreCreationNotification.setMessage(pMessage);
      parametreCreationNotification.setListeDestinataire(parametresFlux.getListeDestinataireNotification());
      
      // Envoi de la notification
      Avertissement.envoyerNotification(systemeManager, queryManager, parametreCreationNotification);
    }
    catch (Exception e) {
    }
    
    // Préparation pour le mail
    try {
      // Récupération du code établissement (c'est le premier que l'on trouve puisque recherche avec la base de données uniquement)
      if (pIdEtablissement == null && listeLienFluxTiersWebSerieN != null) {
        LienFluxTiersWebSerieN lien = listeLienFluxTiersWebSerieN.retournerLien(queryManager.getLibrary());
        if (lien != null) {
          pIdEtablissement = lien.getIdEtablissement();
        }
      }
      
      // Initialisation des paramètres
      ParametreCreationMail parametreCreationMail = new ParametreCreationMail();
      parametreCreationMail.setIdEtablissement(pIdEtablissement);
      parametreCreationMail.setSujet(pSujet);
      parametreCreationMail.setMessage(pMessage);
      // Contrôle de la présence de destinataires
      String listeDestinataire = parametresFlux.getMailDestinataireErreurs();
      if (Constantes.normerTexte(listeDestinataire).isEmpty()) {
        throw new MessageErreurException("Le mail pour notifier que \"" + pSujet
            + "\" ne peut pas être envoyé car aucun destinataire des mails n'a été paramétré dans la table " + EnumTableBDD.FLUX_PARAMETRE
            + '.');
      }
      parametreCreationMail.setListeDestinataireMail(listeDestinataire);
      
      // Envoi du mail
      Avertissement.envoyerMail(queryManager, parametreCreationMail);
    }
    catch (Exception e) {
    }
  }
  
  /**
   * Libération des ressources.
   */
  public void dispose() {
    try {
      if (queryManager != null) {
        queryManager.deconnecter();
      }
      if (systemeManager != null) {
        systemeManager.deconnecter();
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la déconnexion.");
    }
  }
  
  /**
   * Récupère le taux de TVA à partir de l'identifiant d'un type de facturation donné.
   */
  public static BigDecimal retournerTauxTVA(QueryManager pQueryManager, IdTypeFacturation pIdTypeFacturation) {
    // Contrôle des paramètres
    if (pQueryManager == null) {
      throw new MessageErreurException("Le queryManager n'est pas valide.");
    }
    if (pIdTypeFacturation == null) {
      throw new MessageErreurException("L'identifiant du type de facturation n'est pas valide.");
    }
    
    // Récuperation de la TVA
    SqlGescomParametres sqlGescomParametres = new SqlGescomParametres(pQueryManager);
    int colonneTVA = 1;
    IdEtablissement idEtablissement = pIdTypeFacturation.getIdEtablissement();
    
    // Récupération des types de facturation pour en extraire la colonne TVA à utiliser
    ListeTypeFacturation listeTypeFacturation = sqlGescomParametres.chargerListeTypeFacturation(idEtablissement);
    if (listeTypeFacturation == null || listeTypeFacturation.isEmpty()) {
      throw new MessageErreurException("Aucun type de facturation n'a été trouvé dans l'établissement " + idEtablissement + '.');
    }
    TypeFacturation typeFacturation = listeTypeFacturation.get(pIdTypeFacturation);
    if (typeFacturation == null) {
      throw new MessageErreurException(
          "Le type de facturation " + pIdTypeFacturation + " n'a pas été trouvé dans l'établissement " + idEtablissement + '.');
    }
    int colonne = typeFacturation.getChiffreCodeTVAEnPosition(1);
    if (colonne > 0 && colonne <= 6) {
      colonneTVA = colonne;
    }
    
    // Récupération du taux de TVA dans la DG pour l'établissement en cours
    Etablissement etablissement = sqlGescomParametres.chargerEtablissement(idEtablissement);
    return etablissement.getTauxTVA(colonneTVA);
  }
  
  // -- Méthodes privées
  
  /**
   * Chargement de la bilbiothèque d'environnement à partir de la lettre d'environnement.
   */
  private void chargerBibliothequeEnvironnement(String pLettre) {
    if (ParametresFluxBibli.getBibliothequeEnvironnement() != null) {
      queryManager.setLibrairieEnvironnement(ParametresFluxBibli.getBibliothequeEnvironnement().getNom());
      return;
    }
    
    pLettre = Constantes.normerTexte(pLettre);
    if (pLettre.isEmpty()) {
      throw new MessageErreurException("La lettre d'environnement est invalide.");
    }
    if (queryManager == null) {
      throw new MessageErreurException("Le queryManager est invalide.");
    }
    
    // Chargement de la bibliothèque d'environnement
    SqlEnvironnement sqlEnvironnement = new SqlEnvironnement(queryManager);
    Bibliotheque bibliothequeEnvironnementClient = sqlEnvironnement.retournerBibliothequeEnvironnementClient(pLettre.charAt(0));
    ParametresFluxBibli.setBibliothequeEnvironnement(bibliothequeEnvironnementClient);
    queryManager.setLibrairieEnvironnement(bibliothequeEnvironnementClient.getNom());
  }
  
  /**
   * Charge la liste de tous les liens entre les tiers web et Série N.
   */
  private void chargerListeLienFluxTiersWebSerieN() {
    // Contrôle que la liste ne soit pas déjà chargé
    if (listeLienFluxTiersWebSerieN != null) {
      return;
    }
    if (sqlFlux == null) {
      throw new MessageErreurException("L'objet SqlFlux n'est pas instancié.");
    }
    
    listeLienFluxTiersWebSerieN = sqlFlux.chargerLienTiersWebSerieN(null);
  }
  
  /**
   * Charge la liste de toutes les personnalisations des flux de l'exploitation.
   */
  private void chargerPersonnalisationFlux() {
    // Contrôle si les personnalisation ont déjà été chargé
    if (listePersonnalisationFlux != null) {
      return;
    }
    SqlExploitationParametres sqlExploitationParametres = new SqlExploitationParametres(queryManager);
    listePersonnalisationFlux = sqlExploitationParametres.chargerListePersonnalisationFlux(null);
  }
  
  /**
   * Ajoute un sens de flux à mettre en attente dans la liste correspondante.
   */
  private void rajouterUnSensEnAttente(int pSens) {
    if (!testerSiSensEnAttente(pSens) && sensEnAttente != null) {
      sensEnAttente.add(pSens);
    }
  }
  
  /**
   * Retire le sens du flux de la liste des sens à mettre en attente.
   */
  private void retirerUnSensEnAttente(int pSens) {
    if (sensEnAttente != null) {
      int indiceASupp = -1;
      for (int i = 0; i < sensEnAttente.size(); i++) {
        if (sensEnAttente.get(i) == pSens) {
          indiceASupp = i;
        }
      }
      
      if (indiceASupp > -1 && indiceASupp <= sensEnAttente.size()) {
        sensEnAttente.remove(indiceASupp);
      }
    }
  }
  
  /**
   * Initialisation JSON.
   */
  private boolean initJSON() {
    if (builderJSON == null) {
      builderJSON = new GsonBuilder();
    }
    if (gson == null) {
      gson = builderJSON.create();
    }
    
    return (builderJSON != null && gson != null);
  }
  
  /**
   * Contrôle la version d'un flux.
   */
  private boolean controlerVersion(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLVER() == null) {
      return false;
    }
    
    if (retournerVersionMajeure(pFluxMagento.getFLVER()) != retournerVersionMajeure(versionFlux)) {
      pFluxMagento.construireMessageErreur(
          "[controlerVersion]: La version du flux est erronée : " + pFluxMagento.getFLVER() + " - version attendue : " + versionFlux);
      return false;
    }
    
    return true;
  }
  
  /**
   * Retourne la version numérique majeure xx dans une version au format "xx.yy".
   */
  private int retournerVersionMajeure(String pVersion) {
    pVersion = Constantes.normerTexte(pVersion);
    int versionMajeure = 0;
    
    if (pVersion.isEmpty()) {
      return versionMajeure;
    }
    
    String[] tabVers = pVersion.split("\\.");
    
    if (tabVers.length == 2) {
      try {
        versionMajeure = Integer.parseInt(tabVers[0]);
      }
      catch (Exception e) {
      }
    }
    
    return versionMajeure;
  }
  
  /**
   * Traitement du flux FLX001 (article).
   */
  private int traiterFlux001Article(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null || pFluxMagento.getFLCOD() == null) {
      majError("[traitementFlux001_Article] record corrompu ou NULL");
      return TRAITEMENT_NOK;
    }
    
    try {
      String articleJSON = null;
      String messageErreur = "";
      int versionMajeureFlux = retournerVersionMajeure(versionFlux);
      switch (versionMajeureFlux) {
        
        // Flux version 1
        case 1:
          ExportationArticleV1 exportationArticleV1 = new ExportationArticleV1(queryManager);
          articleJSON = exportationArticleV1.retournerUnArticleMagentoJSON(pFluxMagento);
          messageErreur = exportationArticleV1.getMsgError();
          if (articleJSON == null) {
            majError(messageErreur);
            return TRAITEMENT_NOK;
          }
          break;
        
        // Flux version 2
        case 2:
          ExportationArticleMagentoV2 exportationArticleV2 = new ExportationArticleMagentoV2(queryManager);
          articleJSON = exportationArticleV2.retournerUnArticleMagentoJSON(pFluxMagento);
          messageErreur = exportationArticleV2.getMsgError();
          if (articleJSON == null) {
            majError(messageErreur);
            return TRAITEMENT_NOK;
          }
          break;
        
        // Les autres versions
        default:
          majError("Flux non géré dans la version " + versionMajeureFlux);
          return TRAITEMENT_NOK;
      }
      
      // Mise à jour le message JSON dans le flux
      // L'article est à transmettre
      if (Constantes.normerTexte(articleJSON).isEmpty()) {
        pFluxMagento.setFLJSN(articleJSON);
      }
      else {
        pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
        pFluxMagento.setFLJSN(messageErreur);
      }
      return TRAITEMENT_OK;
    }
    catch (Exception e) {
      majError("(traitementFlux001_Article) " + e.getMessage());
      return TRAITEMENT_NOK;
    }
  }
  
  /**
   * Traitement du flux Flux002 (Commande).
   */
  private boolean traiterFlux002Commande(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null || pFluxMagento.getFLJSN() == null) {
      majError("[traiterFlux002_Commande] record/JSON corrompu ou NULL");
      return false;
    }
    
    try {
      boolean traitementOK = false;
      String messageErreur = "";
      int versionMajeureFlux = retournerVersionMajeure(versionFlux);
      switch (versionMajeureFlux) {
        // Flux version 1
        case 1:
          ImportationCommandeMagentoV1 importationCommandeV1 =
              new ImportationCommandeMagentoV1(systemeManager, queryManager, parametresFlux, surveillanceJob);
          traitementOK = importationCommandeV1.recupererUneCommandeMagento(pFluxMagento);
          if (!traitementOK) {
            messageErreur = importationCommandeV1.getMsgError();
          }
          break;
        
        // Flux version 2
        case 2:
          ImportationCommandeMagentoV2 importationCommandeV2 =
              new ImportationCommandeMagentoV2(systemeManager, queryManager, parametresFlux, surveillanceJob);
          traitementOK = importationCommandeV2.recupererUneCommandeMagento(pFluxMagento);
          if (!traitementOK) {
            messageErreur = importationCommandeV2.getMsgError();
          }
          break;
        
        // Flux version 3
        case 3:
          ImportationCommandeWebV3 importationCommandeV3 =
              new ImportationCommandeWebV3(systemeManager, queryManager, parametresFlux, surveillanceJob);
          traitementOK = importationCommandeV3.recupererUneCommandeWeb(pFluxMagento);
          if (!traitementOK) {
            messageErreur = importationCommandeV3.getMsgError();
          }
          break;
        
        // Les autres versions
        default:
          majError("Flux non géré dans la version " + versionMajeureFlux);
          return false;
      }
      
      if (!traitementOK) {
        pFluxMagento
            .construireMessageErreur(" id:" + compteurIdManagerFluxMagento + " traiterFlux002_Commande() traitementOK false : " + messageErreur);
      }
      
      return traitementOK;
    }
    catch (Exception e) {
      Trace.erreur(e, " id:" + compteurIdManagerFluxMagento + " traiterFlux002_Commande() catch: " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Traitement du flux FLX003 (Suivi d'une commande).
   */
  private int traiterFlux003SuiviCommande(FluxMagento pFluxMagento, Date pMaintenant, EnumTypeFlux pTypeFluxADeclencher) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null || pFluxMagento.getFLCOD() == null) {
      majError("[traiterFlux003_SuiviCommande] record corrompu ou NULL");
      return TRAITEMENT_NOK;
    }
    
    try {
      String suiviCommandeJSON = null;
      int versionMajeureFlux = retournerVersionMajeure(versionFlux);
      switch (versionMajeureFlux) {
        
        // Flux version 1
        case 1:
          ExportationSuiviCommandeV1 exportationSuiviCommandeV1 = new ExportationSuiviCommandeV1(queryManager);
          suiviCommandeJSON = exportationSuiviCommandeV1.retournerUnSuiviCommandeMagentoJSON(pFluxMagento);
          if (suiviCommandeJSON == null) {
            majError(exportationSuiviCommandeV1.getMsgError());
            return TRAITEMENT_NOK;
          }
          break;
        
        // Flux version 2
        case 2:
          ExportationSuiviCommandeV2 exportationSuiviCommandeV2 = new ExportationSuiviCommandeV2(queryManager);
          suiviCommandeJSON = exportationSuiviCommandeV2.retournerUnSuiviCommandeMagentoJSON(pFluxMagento);
          if (suiviCommandeJSON == null) {
            majError(exportationSuiviCommandeV2.getMsgError());
            return TRAITEMENT_NOK;
          }
          break;
        
        // Flux version 3
        case 3:
          // Contrôle si le traitement du flux doit être déclenché
          if (!isDeclencherTraitementFLX003(pFluxMagento.getFLETB(), pMaintenant, pTypeFluxADeclencher)) {
            return TRAITEMENT_A_IGNORER;
          }
          
          // Traitement
          ExportationSuiviCommandeV3 exportationSuiviCommandeV3 = new ExportationSuiviCommandeV3(queryManager);
          JsonSuiviCommandeWebV3 suiviCommandeWeb = exportationSuiviCommandeV3.retournerUnSuiviCommandeWeb(pFluxMagento);
          // Contrôle du suivi de commande
          if (suiviCommandeWeb == null) {
            pFluxMagento.construireMessageErreur(exportationSuiviCommandeV3.getMsgError());
            return TRAITEMENT_NOK;
          }
          // Si le flux est marqué comme ne pas traiter, il n'est pas ajouté à la liste
          if (pFluxMagento.getFLSTT() == EnumStatutFlux.NE_PAS_TRAITER) {
            return TRAITEMENT_OK;
          }
          // Ajout à la liste de flux
          listeFluxSuiviCommandeV3.ajouterFlux(suiviCommandeWeb);
          return TRAITEMENT_OK;
        
        // Les autres versions
        default:
          majError("Flux non géré dans la version " + versionMajeureFlux);
          return TRAITEMENT_NOK;
      }
      
      // pFlux.setFLJSN(suiviCommandeJSON);
    }
    catch (Exception e) {
      pFluxMagento.construireMessageErreur("[traiterFlux003_SuiviCommande]: " + e.getMessage());
      return TRAITEMENT_NOK;
    }
    return TRAITEMENT_OK;
  }
  
  /**
   * Traitement du flux Flux004 (Client) dans le sens Magento vers Série N.
   */
  private boolean traiterFlux004ClientRentrant(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null || pFluxMagento.getFLJSN() == null) {
      majError("[traiterFlux004ClientRentrant] record corrompu ou NULL");
      return false;
    }
    
    try {
      boolean traitementOK = false;
      String messageErreur = "";
      int versionMajeureFlux = retournerVersionMajeure(versionFlux);
      switch (versionMajeureFlux) {
        // Flux version 1
        case 1:
          ImportationClientMagentoV1 importationClientV1 = new ImportationClientMagentoV1(queryManager, parametresFlux);
          traitementOK = importationClientV1.recupererUnClientMagento(pFluxMagento);
          if (!traitementOK) {
            messageErreur = importationClientV1.getMsgError().trim();
          }
          break;
        
        // Flux version 2
        case 2:
          ImportationClientMagentoV2 importationClientV2 = new ImportationClientMagentoV2(queryManager, parametresFlux);
          traitementOK = importationClientV2.recupererUnClientMagento(pFluxMagento);
          if (!traitementOK) {
            messageErreur = importationClientV2.getMsgError().trim();
          }
          break;
        
        // Flux version 3
        case 3:
          ImportationClientWebV3 importClientV3 = new ImportationClientWebV3(queryManager, parametresFlux, this);
          traitementOK = importClientV3.recupererUnClientMagento(pFluxMagento);
          if (!traitementOK) {
            messageErreur = importClientV3.getMsgError().trim();
          }
          break;
        
        // Les autres versions
        default:
          majError("Flux non géré dans la version " + versionMajeureFlux);
          return false;
      }
      
      if (!traitementOK) {
        majError(messageErreur);
      }
      
      return traitementOK;
    }
    catch (Exception e) {
      pFluxMagento.construireMessageErreur("[traiterFlux004ClientRentrant]: " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Traitement du flux FLX004 en mode sortant vers Magento.
   */
  private int traiterFlux004ClientSortant(FluxMagento pFluxMagento, Date pMaintenant, EnumTypeFlux pTypeFluxADeclencher) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null || pFluxMagento.getFLCOD() == null) {
      majError("[traiterFlux004ClientSortant] record corrompu ou NULL");
      return TRAITEMENT_NOK;
    }
    
    try {
      String clientJSON = null;
      int versionMajeureFlux = retournerVersionMajeure(versionFlux);
      switch (versionMajeureFlux) {
        
        // Flux version 1
        case 1:
          ExportationClientMagentoV1 exportationClientV1 = new ExportationClientMagentoV1(queryManager);
          clientJSON = exportationClientV1.retournerUnClientMagentoJSON(pFluxMagento, parametresFlux);
          // Contrôle de la chaine JSON
          if (clientJSON == null) {
            pFluxMagento.construireMessageErreur(exportationClientV1.getMsgError());
            return TRAITEMENT_NOK;
          }
          if (clientJSON.trim().isEmpty()) {
            pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
            pFluxMagento.construireMessageErreur(exportationClientV1.getMsgError());
            return TRAITEMENT_OK;
          }
          // Le client est à transmettre
          pFluxMagento.setFLJSN(clientJSON);
          return TRAITEMENT_OK;
        
        // Flux version 2
        case 2:
          ExportationClientMagentoV2 exportationClientV2 = new ExportationClientMagentoV2(queryManager);
          clientJSON = exportationClientV2.retournerUnClientMagentoJSON(pFluxMagento);
          // Contrôle de la chaine JSON
          if (clientJSON == null) {
            pFluxMagento.construireMessageErreur(exportationClientV2.getMsgError());
            return TRAITEMENT_NOK;
          }
          if (clientJSON.trim().isEmpty()) {
            pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
            pFluxMagento.construireMessageErreur(exportationClientV2.getMsgError());
            return TRAITEMENT_OK;
          }
          // Le client est à transmettre
          pFluxMagento.setFLJSN(clientJSON);
          return TRAITEMENT_OK;
        
        // Flux version 3
        case 3:
          // Contrôle si le traitement du flux doit être déclenché
          if (!isDeclencherTraitementFLX004(pFluxMagento.getFLETB(), pMaintenant, pTypeFluxADeclencher)) {
            return TRAITEMENT_A_IGNORER;
          }
          
          // Traitement
          ExportationClientWebV3 exportationClientV3 = new ExportationClientWebV3(queryManager);
          JsonClientWebV3 clientWeb = exportationClientV3.retournerUnClientWeb(pFluxMagento);
          // Contrôle du client
          if (clientWeb == null) {
            pFluxMagento.construireMessageErreur(exportationClientV3.getMsgError());
            return TRAITEMENT_NOK;
          }
          // Si le flux est marqué comme ne pas traiter, il n'est pas ajouté à la liste
          if (pFluxMagento.getFLSTT() == EnumStatutFlux.NE_PAS_TRAITER) {
            return TRAITEMENT_OK;
          }
          // Ajout à la liste de flux
          listeFluxClientV3.ajouterFlux(clientWeb);
          return TRAITEMENT_OK;
        
        // Les autres versions
        default:
          majError("Flux non géré dans la version " + versionMajeureFlux);
          return TRAITEMENT_NOK;
      }
    }
    catch (Exception e) {
      pFluxMagento.construireMessageErreur("[traiterFlux004ClientSortant]: " + e.getMessage());
    }
    return TRAITEMENT_NOK;
  }
  
  /**
   * Traitement du flux FLX005 (factures PDF).
   */
  private int traiterFlux005Facture(FluxMagento pFluxMagento) {
    if (pFluxMagento == null) {
      majError("[LA_FluxMagento][traitementFlux005_Facture] record NULL");
      return TRAITEMENT_NOK;
    }
    if (pFluxMagento.getFLETB() == null || pFluxMagento.getFLCOD() == null) {
      pFluxMagento.construireMessageErreur("[LA_FluxMagento][traitementFlux005_Facture] record corrompu ");
      return TRAITEMENT_NOK;
    }
    
    try {
      String factureJSON = null;
      String messageErreur = "";
      int versionMajeureFlux = retournerVersionMajeure(versionFlux);
      switch (versionMajeureFlux) {
        
        // Flux version 1
        case 1:
          GM_Export_FacturesV1 exportationFactureV1 = new GM_Export_FacturesV1(queryManager, parametresFlux);
          factureJSON = exportationFactureV1.retournerUneFactureJSON(pFluxMagento);
          messageErreur = exportationFactureV1.getMsgError();
          break;
        
        // Flux version 2
        case 2:
          ExportFactureMagentoV2 exportationFactureV2 = new ExportFactureMagentoV2(queryManager, parametresFlux);
          factureJSON = exportationFactureV2.retournerUneFactureJSON(pFluxMagento);
          messageErreur = exportationFactureV2.getMsgError();
          break;
        
        // Les autres versions
        default:
          majError("Flux non géré dans la version " + versionMajeureFlux);
          return TRAITEMENT_NOK;
      }
      
      // constyruction de l'article sous forme de message JSON
      if (factureJSON != null) {
        if (factureJSON.trim().length() > 0) {
          pFluxMagento.setFLJSN(factureJSON);
          pFluxMagento.setJsonAffichage("Facture " + pFluxMagento.getFLCOD());
        }
        else {
          pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
          pFluxMagento.setFLJSN(messageErreur);
        }
        return TRAITEMENT_OK;
      }
      else {
        return TRAITEMENT_NOK;
      }
    }
    catch (Exception e) {
      pFluxMagento.construireMessageErreur("[traitementFlux005_Facture]: " + e.getMessage());
      return TRAITEMENT_NOK;
    }
  }
  
  /**
   * Traitement du flux FLX006 (stocks).
   * C'est un flux qui rassemble dans un même envoi l'ensemble des articles cohérents (uniquement ceux pour lesquels on a eu des
   * mouvements) et leur stocks disponibles respectifs.
   */
  private int traiterFlux006Stock(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null) {
      majError("[traitementFlux006_Stocks] record corrompu ou NULL");
      return TRAITEMENT_NOK;
    }
    
    try {
      String stocksJson = null;
      int versionMajeureFlux = retournerVersionMajeure(versionFlux);
      switch (versionMajeureFlux) {
        
        // Flux version 1
        case 1:
          ExportationStockMagentoV1 exportationStockMagentoV1 = new ExportationStockMagentoV1(this);
          stocksJson = exportationStockMagentoV1.retournerStocksJSON(pFluxMagento);
          if (stocksJson == null) {
            majError(exportationStockMagentoV1.getMsgError().trim());
            return TRAITEMENT_NOK;
          }
          // Pas de stocks à mettre à jour
          if (stocksJson.trim().length() == 0) {
            pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
            pFluxMagento.setFLJSN("Pas de stock à transmettre");
            pFluxMagento.setFLCOD("0");
          }
          else {
            pFluxMagento.setFLJSN(stocksJson);
          }
          updateRecord(pFluxMagento);
          break;
        
        // Flux version 2
        case 2:
          ExportationStockMagentoV2 exportationStockMagentoV2 = new ExportationStockMagentoV2(this);
          stocksJson = exportationStockMagentoV2.retournerStocksJSON(pFluxMagento);
          if (stocksJson == null) {
            majError(exportationStockMagentoV2.getMsgError().trim());
            return TRAITEMENT_NOK;
          }
          // Pas de stocks à mettre à jour
          if (stocksJson.trim().length() == 0) {
            pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
            pFluxMagento.setFLJSN("Pas de stock à transmettre");
            pFluxMagento.setFLCOD("0");
          }
          else {
            pFluxMagento.setFLJSN(stocksJson);
          }
          updateRecord(pFluxMagento);
          break;
        
        // Flux version 3
        case 3:
          ExportationStockWebV3 exportationStockWebV3 = new ExportationStockWebV3(this);
          JsonStockWebV3 jsonStockWebV3 = exportationStockWebV3.retournerStocksWeb(pFluxMagento);
          if (jsonStockWebV3 == null) {
            majError(exportationStockWebV3.getMsgError().trim());
            return TRAITEMENT_NOK;
          }
          
          // Si le flux est marqué comme ne pas traiter, il n'est pas ajouté à la liste
          if (pFluxMagento.getFLSTT() == EnumStatutFlux.NE_PAS_TRAITER) {
            return TRAITEMENT_OK;
          }
          
          // Récupération de la personnalisation pour ce flux
          IdEtablissement idEtablissement = IdEtablissement.getInstance(pFluxMagento.getFLETB());
          PersonnalisationFlux personnalisationFlux =
              listePersonnalisationFlux.retournerPersonnalisation(EnumTypeFlux.STOCK, idEtablissement);
          if (personnalisationFlux == null) {
            majError("Pas de personnalisation trouvée pour le flux stock (FLX006) pour l'établissement " + idEtablissement
                + " de la base de données " + queryManager.getLibrary()
                + ". La version 3 des flux a besoin obligatoirement de cette personnalisation.");
            return TRAITEMENT_NOK;
          }
          FluxStockV3 fluxStockV3 = new FluxStockV3(jsonStockWebV3);
          fluxStockV3.setPersonnalisationFlux(personnalisationFlux);
          fluxStockV3.setSysteme(systemeManager.getSystem());
          fluxStockV3.setBaseDeDonnees(baseDeDonnees);
          fluxStockV3.transfererVersServeur();
          break;
        
        // Les autres versions
        default:
          majError("Flux non géré dans la version " + versionMajeureFlux);
          return TRAITEMENT_NOK;
      }
    }
    catch (Exception e) {
      pFluxMagento.construireMessageErreur("[traitementFlux006_Stocks] " + e.getMessage());
      return TRAITEMENT_NOK;
    }
    
    return TRAITEMENT_OK;
  }
  
  /**
   * Traitement du flux FLX007(suivi client).
   */
  private int traiterFlux007SuiviClient(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null) {
      return TRAITEMENT_NOK;
    }
    
    int versionMajeureFlux = retournerVersionMajeure(versionFlux);
    switch (versionMajeureFlux) {
      
      // Flux version 1
      case 1:
        try {
          GM_Export_SuiviClientV1 exportSuiviClient = new GM_Export_SuiviClientV1(queryManager);
          String suiviClient = exportSuiviClient.retournerEnCoursClientsJSON(pFluxMagento);
          if (suiviClient != null) {
            pFluxMagento.setFLJSN(suiviClient);
          }
          else {
            majError("traitementFlux007_Suivi_client() : " + exportSuiviClient.getMsgError());
            return TRAITEMENT_NOK;
          }
        }
        catch (Exception e) {
          majError("traitementFlux007_Suivi_client() : " + e.getMessage());
          return TRAITEMENT_NOK;
        }
        break;
      
      // Flux version 2
      case 2:
        try {
          ExportSuiviClientV2 exportSuiviClient = new ExportSuiviClientV2(queryManager);
          String suiviClient = exportSuiviClient.retournerEnCoursClientsJSON(pFluxMagento);
          if (suiviClient != null) {
            pFluxMagento.setFLJSN(suiviClient);
          }
          else {
            majError("traitementFlux007_Suivi_client() : " + exportSuiviClient.getMsgError());
            return TRAITEMENT_NOK;
          }
        }
        catch (Exception e) {
          majError("traitementFlux007_Suivi_client() : " + e.getMessage());
          return TRAITEMENT_NOK;
        }
        break;
      
      // Les autres versions
      default:
        majError("Flux non géré dans la version " + versionMajeureFlux);
        return TRAITEMENT_NOK;
    }
    
    return TRAITEMENT_OK;
  }
  
  /**
   * Traitement de flux FLX008 ORDERS EDI.
   */
  private boolean traiterFlux008CommandeEDI(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null) {
      majError("[traiterFlux008_CommandeEDI] record corrompu ou NULL");
      return false;
    }
    
    try {
      GM_Import_Cdes_EDI importsORDERS =
          new GM_Import_Cdes_EDI(parametresFlux, queryManager, systemeManager, lettreEnvironnement, surveillanceJob);
      
      if (importsORDERS.recupererCommandeEDI(pFluxMagento)) {
        return true;
      }
      else {
        majError(importsORDERS.getMsgError());
        return false;
      }
    }
    catch (Exception e) {
      majError("[traiterFlux008_CommandeEDI]: " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Traitement du flux FLX009 Il s'agit d'envoyer les DESADV(expeditions) sur un serveur EDI
   */
  private boolean traiterFlux009ExpeditionEDI(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null) {
      majError("[traiterFlux009_ExpeditionEDI] record corrompu ou NULL");
      return false;
    }
    
    try {
      GM_Export_Expe_EDI exportDESADV = new GM_Export_Expe_EDI(parametresFlux, queryManager, systemeManager, lettreEnvironnement);
      
      if (exportDESADV.envoyerDESADV(pFluxMagento)) {
        return true;
      }
      else {
        majError(exportDESADV.getMsgError().trim());
        return false;
      }
    }
    catch (Exception e) {
      majError("[traiterFlux009_ExpeditionEDI] " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Traitement du flux FLX010.
   */
  private boolean traiterFlux010FacturationEDI(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null) {
      majError("[traiterFlux010_FacturationEDI] record corrompu ou NULL");
      return false;
    }
    
    try {
      GM_Export_Factu_EDI exportFACTURE = new GM_Export_Factu_EDI(parametresFlux, queryManager, systemeManager, lettreEnvironnement);
      
      if (exportFACTURE.envoyerFACTURE(pFluxMagento)) {
        return true;
      }
      else {
        majError(exportFACTURE.getMsgError().trim());
        return false;
      }
    }
    catch (Exception e) {
      majError("[traiterFlux010_FacturationEDI] " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Traitement du flux FLX012.
   * Factures Yooz à importer et injecter en comptabilité.
   */
  private boolean traiterFLX012FactureYooz(FluxMagento pFluxMagento) {
    if (pFluxMagento == null) {
      majError("[traiterFLX012] pFlux NULL");
      return false;
    }
    if (pFluxMagento.getFLETB() == null || pFluxMagento.getFLETB().trim().isEmpty()) {
      majError("[traiterFLX012] etablissement corrompu ou NULL");
      return false;
    }
    if (pFluxMagento.getFLCOD() == null || pFluxMagento.getFLCOD().trim().isEmpty()) {
      majError("[traiterFLX012] nom de facture corrompu ou NULL");
      return false;
    }
    
    try {
      GestionFLX012 gestionFLX012 = new GestionFLX012(parametresFlux, queryManager, systemeManager);
      if (gestionFLX012.traiterUneFactureYooz(pFluxMagento)) {
        majError("Facture " + pFluxMagento.getFLCOD() + " traitée avec succés ");
        return true;
      }
      else {
        majError(gestionFLX012.getMsgErreur());
        return false;
      }
    }
    catch (Exception e) {
      majError("[traiterFLX012] " + e.getMessage());
      return false;
    }
  }
  
  /**
   * Traitement du flux FLX013 (prix).
   */
  private int traiterFlux013Prix(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null || pFluxMagento.getFLCOD() == null) {
      majError("[ManagerFluxMagento] traiterFlux013_Prix() - Record corrompu ou NULL");
      return TRAITEMENT_NOK;
    }
    
    try {
      int versionMajeureFlux = retournerVersionMajeure(versionFlux);
      switch (versionMajeureFlux) {
        
        // Flux version 3
        case 3:
          ExportationPrixWebV3 exportationPrixWebV3 = new ExportationPrixWebV3(this, queryManager);
          JsonListePrixWebV3 jsonListePrixWebV3 = exportationPrixWebV3.retournerListePrixWeb(pFluxMagento);
          // Contrôle du prix
          if (jsonListePrixWebV3 == null) {
            pFluxMagento.construireMessageErreur(exportationPrixWebV3.getMsgError());
            return TRAITEMENT_NOK;
          }
          // Si le flux est marqué comme ne pas traiter, il n'est pas ajouté à la liste
          if (pFluxMagento.getFLSTT() == EnumStatutFlux.NE_PAS_TRAITER) {
            return TRAITEMENT_OK;
          }
          // Récupération de la personnalisation pour ce flux
          IdEtablissement idEtablissement = IdEtablissement.getInstance(pFluxMagento.getFLETB());
          PersonnalisationFlux personnalisationFlux =
              listePersonnalisationFlux.retournerPersonnalisation(EnumTypeFlux.PRIX, idEtablissement);
          if (personnalisationFlux == null) {
            majError("Pas de personnalisation trouvée pour le flux prix (FLX013) pour l'établissement " + idEtablissement
                + " de la base de données " + queryManager.getLibrary()
                + ". La version 3 des flux a besoin obligatoirement de cette personnalisation.");
            return TRAITEMENT_NOK;
          }
          FluxPrixV3 fluxPrixV3 = new FluxPrixV3(jsonListePrixWebV3);
          fluxPrixV3.setPersonnalisationFlux(personnalisationFlux);
          fluxPrixV3.setSysteme(systemeManager.getSystem());
          fluxPrixV3.setBaseDeDonnees(baseDeDonnees);
          fluxPrixV3.transfererVersServeur();
          break;
        
        // Les autres versions
        default:
          majError("Flux non géré dans la version " + versionMajeureFlux);
          return TRAITEMENT_NOK;
      }
    }
    catch (Exception e) {
      majError("[ManagerFluxMagento] traiterFlux013_Prix() - " + e.getMessage());
      return TRAITEMENT_NOK;
    }
    return TRAITEMENT_OK;
  }
  
  /**
   * Valide un flux après traitement et envoi.
   */
  private boolean valider(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null) {
      majError("[valider] record corrompu ou NULL");
      return false;
    }
    
    int versionMajeureFlux = retournerVersionMajeure(pFluxMagento.getFLVER());
    boolean isOk = false;
    try {
      // Si le flux est judicieux à traiter (ex: des stocks vides ne doivent pas être envoyés)
      if (pFluxMagento.getFLSTT() != EnumStatutFlux.NE_PAS_TRAITER) {
        // Envoi vers Magento si flux V1 et V2
        if (versionMajeureFlux != 3 && pFluxMagento.getFLSNS() == FluxMagento.SNS_VERS_MAGENTO
            && pFluxMagento.getFLSTT() == EnumStatutFlux.A_TRAITER) {
          isOk = appelerWebServiceMagento(pFluxMagento);
        }
        else if (pFluxMagento.getFLSNS() == FluxMagento.SNS_EDI_ENVOI && pFluxMagento.getFLSTT() == EnumStatutFlux.A_TRAITER) {
          isOk = true;
        }
        else if (pFluxMagento.getFLSNS() == FluxMagento.SNS_EDI_RECEP
            && pFluxMagento.getFLIDF().equals(FluxMagento.IDF_FLX012_FAC_YOOZ)) {
          isOk = true;
        }
        // Réception de commandes EDI OK mais sans commande à récupérer
        else if (pFluxMagento.getFLSNS() == FluxMagento.SNS_EDI_RECEP && pFluxMagento.getFLJSN() == null) {
          pFluxMagento.setFLJSN("AUCUNE COMMANDE A RECUPERER SUR LE SERVEUR EDI");
          pFluxMagento.setFLMSGERR(getMsgError());
          isOk = true;
        }
        // Dans tous les autres cas et notamment en V3
        else {
          isOk = true;
        }
        
        pFluxMagento.setFLCPT(pFluxMagento.getFLCPT() + 1);
        pFluxMagento.setFLTRT(new Timestamp(new Date().getTime()));
        // Si le flux a bien été envoyé
        if (isOk) {
          pFluxMagento.setFLSTT(EnumStatutFlux.TRAITE);
          pFluxMagento.setFLERR(FluxMagento.ERR_PAS_ERREUR);
          
          // Débloque les autres flux s'ils étaient en attente
          debloquerFluxPourUnSens(pFluxMagento.getFLSNS(), pFluxMagento.getFLID());
        }
        else {
          // Dans le cas où le web service appelé a rejeté volontairement notre demande
          if (pFluxMagento.getFLERR() == FluxMagento.ERR_REFUS) {
            pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
            Trace.erreur("++++ valider() REFUS WEB SERVICE ");
            pFluxMagento.setFLMSGERR("REFUS DU GESTIONNAIRE " + getMsgError());
          }
          else {
            // Le nombre de tentative d'envoi est dépassé: le statut du flux est changé en "Ne pas traiter"
            if (pFluxMagento.getFLCPT() > parametresFlux.getNbTentativesMax()) {
              pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
              pFluxMagento.setFLERR(FluxMagento.ERR_ENVOI);
              String message = "TENTATIVES_MAX_ENVOI: le nombre de tentatives d'envoi est dépassé, le statut du flux "
                  + pFluxMagento.getFLID() + " est passé à \"Ne pas traiter\"";
              Trace.erreur(message);
              pFluxMagento.setFLMSGERR(message);
            }
            // Alerte si le seuil est atteint
            else if (pFluxMagento.getFLCPT() == parametresFlux.getSeuilEchecAlerte()) {
              Trace.erreur("++++ ON ALERTE POUR LE FLUX " + pFluxMagento.getFLID());
              pFluxMagento.setFLJSN("");
              pFluxMagento.setFLMSGERR("");
              // TODO On va mettre tous les autres flux du même sens en attente
              mettreEnAttenteFluxPourUnSens(pFluxMagento.getFLSNS(), pFluxMagento.getFLID());
            }
            // Sinon on passe son tour pour réssayer une prochaine fois
            else {
              isOk = true;
              
              Trace.info("Nouvelle tentative pour " + pFluxMagento.getFLID() + " " + pFluxMagento.getFLCPT());
              pFluxMagento.setFLJSN("");
              pFluxMagento.setFLMSGERR("");
              mettreEnAttenteFluxPourUnSens(pFluxMagento.getFLSNS(), pFluxMagento.getFLID());
            }
          }
        }
      }
      else {
        isOk = true;
        Trace.info("On ne traite pas le flux " + pFluxMagento.getFLID() + " c'est inutile " + pFluxMagento.getFLSTT());
      }
      
      pFluxMagento.setFLMSGERR(pFluxMagento.getFLMSGERR() + " " + getMsgError());
      // Mise à jour et contrôle
      if (!updateRecord(pFluxMagento)) {
        majError("[valider] record corrompu ou NULL");
        return false;
      }
    }
    catch (Exception e) {
      majError("[valider] " + e.getMessage());
      return false;
    }
    
    return isOk;
  }
  
  /**
   * Récupère la personnalisation du flux suivi de commande et contrôle s'il est temps de le déclencher son traitement.
   */
  private boolean isDeclencherTraitementFLX003(String pCodeEtablissement, Date pMaintenant, EnumTypeFlux pTypeFluxADeclencher) {
    try {
      boolean forcerDeclenchement = false;
      // Contrôle s'il y a une demande pour forcer le déclenchement de ce type de flux
      if (pTypeFluxADeclencher != null && pTypeFluxADeclencher == EnumTypeFlux.SUIVI_COMMANDE) {
        Trace.info("Le déclenchement du type de flux " + EnumTypeFlux.SUIVI_COMMANDE.getCode() + " a été forcé.");
        forcerDeclenchement = true;
      }
      
      // Sinon
      // Récupération de la personnalisation pour ce flux
      IdEtablissement idEtablissement = IdEtablissement.getInstance(pCodeEtablissement);
      PersonnalisationFlux personnalisationFlux =
          listePersonnalisationFlux.retournerPersonnalisation(EnumTypeFlux.SUIVI_COMMANDE, idEtablissement);
      if (personnalisationFlux == null) {
        majError("Pas de personnalisation trouvée pour le flux suivi de commande (FLX003) pour l'établissement " + idEtablissement
            + " de la base de données " + queryManager.getLibrary()
            + ". La version 3 des flux a besoin obligatoirement de cette personnalisation.");
        return false;
      }
      
      // Contrôle si le flux est actif
      if (personnalisationFlux.getActif() == null || !personnalisationFlux.getActif().booleanValue()) {
        majError("Le flux suivi de commande (FLX003) n'est pas actif.");
        return false;
      }
      
      // Contrôle les heures de déclenchement paramétrées
      if (forcerDeclenchement || personnalisationFlux.controlerHeureMinute(pMaintenant)) {
        Trace.info("Déclenchement du traitement FLX003 à " + pMaintenant);
        // Initialisation des paramètres pour le transfert
        if (!listeFluxSuiviCommandeV3.isInitialiser()) {
          listeFluxSuiviCommandeV3.setPersonnalisationFlux(personnalisationFlux);
          listeFluxSuiviCommandeV3.setSysteme(systemeManager.getSystem());
          listeFluxSuiviCommandeV3.setBaseDeDonnees(baseDeDonnees);
        }
        return true;
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la récupération de la personnalisation du flux suivi de commande (FLX003).");
      majError("Erreur lors de la récupération de la personnalisation du flux suivi de commande (FLX003).");
    }
    return false;
  }
  
  /**
   * Récupère la personnalisation du flux client et contrôle s'il est temps de le déclencher son traitement.
   */
  private boolean isDeclencherTraitementFLX004(String pCodeEtablissement, Date pMaintenant, EnumTypeFlux pTypeFluxADeclencher) {
    try {
      boolean forcerDeclenchement = false;
      // Contrôle s'il y a une demande pour forcer le déclenchement de ce type de flux
      if (pTypeFluxADeclencher != null && pTypeFluxADeclencher == EnumTypeFlux.CLIENT) {
        Trace.info("Le déclenchement du type de flux " + EnumTypeFlux.CLIENT.getCode() + " a été forcé.");
        forcerDeclenchement = true;
      }
      
      // Sinon
      // Récupération de la personnalisation pour ce flux
      IdEtablissement idEtablissement = IdEtablissement.getInstance(pCodeEtablissement);
      PersonnalisationFlux personnalisationFlux =
          listePersonnalisationFlux.retournerPersonnalisation(EnumTypeFlux.CLIENT, idEtablissement);
      if (personnalisationFlux == null) {
        majError("Pas de personnalisation trouvée pour le flux client (FLX004) pour l'établissement " + idEtablissement
            + " de la base de données " + queryManager.getLibrary()
            + ". La version 3 des flux a besoin obligatoirement de cette personnalisation.");
        return false;
      }
      
      // Contrôle si le flux est actif
      if (personnalisationFlux.getActif() == null || !personnalisationFlux.getActif().booleanValue()) {
        majError("Le flux client (FLX004) n'est pas actif.");
        return false;
      }
      
      // Contrôle les heures de déclenchement paramétrées
      if (forcerDeclenchement || personnalisationFlux.controlerHeureMinute(pMaintenant)) {
        Trace.info("Déclenchement du traitement FLX004 à " + pMaintenant);
        // Initialisation des paramètres pour le transfert
        if (!listeFluxClientV3.isInitialiser()) {
          listeFluxClientV3.setPersonnalisationFlux(personnalisationFlux);
          listeFluxClientV3.setSysteme(systemeManager.getSystem());
          listeFluxClientV3.setBaseDeDonnees(baseDeDonnees);
        }
        return true;
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la récupération de la personnalisation du flux client (FLX004).");
      majError("Erreur lors de la récupération de la personnalisation du flux client (FLX004).");
    }
    return false;
  }
  
  /**
   * Après traitement, envoi des flux sortants en appellant le WS Magento.
   */
  private boolean appelerWebServiceMagento(FluxMagento pFlux) {
    if (pFlux == null || pFlux.getFLETB() == null || pFlux.getFLJSN() == null) {
      majError("[appelWS] record corrompu ou NULL");
      return false;
    }
    boolean retour = false;
    
    if (parametresFlux.isModeTest()) {
      Trace.debug(ManagerFluxMagento.class, "appelWS", "Flux sortant, on simule l'envoi en WS " + pFlux.getFLID());
      return parametresFlux.isModeTest();
    }
    
    try {
      if (stub == null) {
        Trace.info("Appel au web service : " + parametresFlux.getUrlWebServiceMagento());
        stub = new MagentoServiceStub(parametresFlux.getUrlWebServiceMagento());
      }
      
      // Requete à envoyer à Magento
      SerieNReceive requete = new SerieNReceive();
      // D'abord on se logge
      Login monLogin = new Login();
      monLogin.setUsername(parametresFlux.getLoginWsMagento());
      monLogin.setApiKey(parametresFlux.getPasswordWsMagento());
      // On récupère l'ID de session
      LoginResponse maSession = stub.login(monLogin);
      
      requete.setSessionId(maSession.getLoginReturn());
      requete.setMessage(pFlux.getFLJSN());
      
      // envoi
      SerieNReceiveResponse reponse = stub.serieNReceive(requete);
      
      if (reponse != null) {
        if (gestionRetourWS == null) {
          gestionRetourWS = new GestionRetourWS(queryManager);
        }
        
        CodeRetourMagento code = null;
        if (gestionRetourWS.initJSON()) {
          // On cosntruit un objet JSON pour intérpréter les codes de retour avec libellés tout ça tout ça
          try {
            code = gestionRetourWS.getGson().fromJson(reponse.getResult().trim(), CodeRetourMagento.class);
          }
          catch (JsonSyntaxException e) {
            {
              majError("[appelWS] PARSE JSON: " + reponse.getResult());
              return false;
            }
          }
          // On interprète le message retour
          if (code != null) {
            // S'il est accepté par le gestionnaire de flux Magento
            if (code.getCode_retour() == ParametresFluxInit.WS_RETOUR_TRUE) {
              retour = true;
            }
            // S'il est refusé par le gestionnaire de flux
            else if (code.getCode_retour() == ParametresFluxInit.WS_RETOUR_REFUS) {
              pFlux.setFLERR(FluxMagento.ERR_REFUS);
            }
          }
        }
      }
      
      EndSession fin = new EndSession();
      stub.endSession(fin);
    }
    // Connexion
    catch (AxisFault e) {
      majError("[appelWS]  probleme de connexion : " + e.getMessage());
    }
    // Login
    catch (RemoteException e) {
      majError("[appelWS] probleme de login " + e.getMessage());
    }
    catch (Exception e) {
      majError("(appelWS) ERREUR: " + e.getMessage());
    }
    
    return retour;
  }
  
  /**
   * Bloque le flux suite à une erreur de traitement.
   */
  private void bloquertraitement(FluxMagento pFlux) {
    // On incrémente les tentatives en échec
    pFlux.setFLCPT(pFlux.getFLCPT() + 1);
    
    // pour les flux FTP en erreur, on déclenche une erreur Envoi_reception
    if (pFlux.getFLSNS() == FluxMagento.SNS_EDI_ENVOI) {
      pFlux.setFLERR(FluxMagento.ERR_ENVOI_RECEPTION);
    }
    else {
      pFlux.setFLERR(FluxMagento.ERR_TRAITEMENT);
    }
    
    pFlux.setFLTRT(new Timestamp(new Date().getTime()));
    pFlux.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
    
    msgErreur = null;
    
    try {
      updateRecord(pFlux);
      
      String sujet = "Erreur sur le flux " + pFlux.getFLID();
      String message =
          "Le flux numéro " + pFlux.getFLID() + " de type " + pFlux.getFLIDF() + " a declenché une erreur : " + pFlux.getFLMSGERR();
      IdEtablissement idEtablissement = IdEtablissement.getInstance(pFlux.getFLETB());
      signalerProbleme(sujet, message, idEtablissement);
    }
    catch (Exception e) {
      pFlux.construireMessageErreur("(blocagetraitement) ERREUR: " + e.getMessage());
      Trace.info("+++++++++++" + e.getMessage());
    }
  }
  
  /**
   * Pour les flux rentrants Magento on associe l'instance à une FM et un établissement.
   * Mise à jour du query de la gestion de flux ainsi que les paramètres de cette gestion.
   */
  private boolean interpreterInstanceVersEtb(FluxMagento pFlux) {
    if (pFlux == null) {
      Trace.erreur("(interpreterInstanceVersEtb) Le flux est invalide.");
      return false;
    }
    if (pFlux.getFLJSN() == null) {
      Trace.erreur("(interpreterInstanceVersEtb) Le champ FLJSN du flux est invalide.");
      return false;
    }
    
    // 1 On va récuperer l'instance transmise dans le flux JSon
    if (!initJSON()) {
      pFlux.construireMessageErreur("(interpreterInstanceVersEtb()) Erreur lors de l'initialisation pour la désérialisation du Json.");
      return false;
    }
    JsonEntiteMagento entite = null;
    try {
      int versionMajeure = retournerVersionMajeure(pFlux.getFLVER());
      // Traitement spécifique si flux en version 3
      if (versionMajeure == 3) {
        // Modifier le nom du champ idInstance en idInstanceMagento (Provisoire)
        pFlux.setFLJSN(pFlux.getFLJSN().replaceAll("idInstance", "idInstanceMagento"));
      }
      
      entite = gson.fromJson(pFlux.getFLJSN(), JsonEntiteMagento.class);
      if (entite != null) {
        if (entite.getIdInstanceMagento() <= -1) {
          pFlux.construireMessageErreur("(interpreterInstanceVersEtb()) L'idInstance est invalide.");
          return false;
        }
      }
      else {
        pFlux.construireMessageErreur("(interpreterInstanceVersEtb()) Objet JSON NULL ");
        return false;
      }
    }
    catch (JsonSyntaxException e) {
      pFlux.construireMessageErreur("(interpreterInstanceVersEtb()) Objet JSON: " + e.getMessage());
      return false;
    }
    
    // 2 On récupère la FM et l'établissement à partir de l'instance
    LienFluxTiersWebSerieN lien = listeLienFluxTiersWebSerieN.retournerLien(Integer.valueOf(entite.getIdInstanceMagento()));
    if (lien == null) {
      pFlux.construireMessageErreur("(interpreterInstanceVersEtb) Problème de récupération des données de l'instance "
          + entite.getIdInstanceMagento() + " " + queryManager.getMsgError());
      return false;
    }
    // Contrôle de l'enregistrement
    if (lien.getLettre() == null || lien.getBaseDeDonnees() == null || lien.getIdEtablissement() == null
        || lien.getVersionFlux() == null) {
      pFlux.construireMessageErreur("(majCorrespondancesInstance) variables d'environnement en erreur: lettre: " + lien.getLettre() + " - fm:"
          + lien.getBaseDeDonnees() + " - etb:" + lien.getIdEtablissement() + " - version: " + lien.getVersionFlux());
      return false;
    }
    
    // Contrôle de la base de donnée en lien
    // Permet la gestion de plusieurs bases de données dans un même environnement <== Voir pour supprimer.
    if (!Constantes.equals(queryManager.getLibrary(), lien.getBaseDeDonnees().getNom())) {
      // On rajoute l'établissement par défaut pour la base de données, pour faire remonter le flux dans le gestionnaire
      String baseDeDonneeActuelle = queryManager.getLibrary();
      LienFluxTiersWebSerieN lienBaseActuelle = listeLienFluxTiersWebSerieN.retournerLien(baseDeDonneeActuelle);
      String codeEtablissementParDefaut = lienBaseActuelle.getIdEtablissement().getCodeEtablissement();
      pFlux.setFLETB(codeEtablissementParDefaut);
      
      // Un message d'erreur explicite est renvoyé il indique :
      // La méthode qui a généré l'erreur : interpreterInstanceVersEtb()
      // La source de l'erreur et sa valeur : IdInstanceMagento + entite.getIdInstanceMagento()
      // La base de donnée de travail actuelle et la valeur de l'IdInstanceMagento attendu :
      // retournerLien(queryManager.getLibrary()).getId().getNumeroInstance())
      
      pFlux.construireMessageErreur("(interpreterInstanceVersEtb()) IdInstanceMagento " + entite.getIdInstanceMagento() + " non conforme pour la "
          + queryManager.getLibrary() + ". IdInstanceMagento attendu : "
          + listeLienFluxTiersWebSerieN.retournerLien(queryManager.getLibrary()).getId().getNumeroInstance());
      
      // On renvoie false pour faire passer le flux en erreur, et ne pas le traiter.
      return false;
    }
    
    parametresFlux = initParametres(lien.getBaseDeDonnees());
    if (parametresFlux == null) {
      pFlux.construireMessageErreur("(majCorrespondancesInstance) parametresFlux NUll");
      return false;
    }
    parametresFlux.setLettreEnvironnement(lien.getLettre().toString());
    lettreEnvironnement = parametresFlux.getLettreEnvironnement();
    parametresFlux.setVersionFlux(lien.getVersionFlux());
    versionFlux = parametresFlux.getVersionFlux();
    
    // 3 On va mettre à jour le systeme le query et les variables propres à l'instance: Lettre, FM et ETB
    // A voir mais ne me parait pas nécessaire (15/10/2021)
    // systemeManager = new SystemeManager(true);
    // queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
    if (!queryManager.getLibrary().equals(lien.getBaseDeDonnees().getNom())) {
      queryManager.setLibrary(lien.getBaseDeDonnees().getNom());
      
      // Chargement des personnalisations des flux
      Trace.info("Chargement parametre FX (interpreterInstanceVersEtb)");
      chargerPersonnalisationFlux();
    }
    pFlux.setFLETB(lien.getIdEtablissement().getCodeEtablissement());
    pFlux.setQuerymg(queryManager);
    
    return true;
  }
  
  // -- Méthodes protégées
  
  /**
   * Construit le message d'erreur proprement.
   */
  protected void majError(String pMessage) {
    if (pMessage == null) {
      msgErreur = "";
    }
    else {
      if (msgErreur == null) {
        msgErreur = "";
      }
      Trace.erreur(pMessage);
      msgErreur += pMessage;
    }
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    return msgErreur;
  }
  
  public QueryManager getQueryManager() {
    return queryManager;
  }
  
  public String getVersionFlux() {
    return versionFlux;
  }
  
  public ParametresFluxBibli getParametresFlux() {
    return parametresFlux;
  }
  
  public SystemeManager getSystemeManager() {
    return systemeManager;
  }
  
  public static void setSurveillanceJob(SurveillanceJob pSurveillanceJob) {
    ManagerFluxMagento.surveillanceJob = pSurveillanceJob;
  }
  
  public static SurveillanceJob getSurveillanceJob() {
    return surveillanceJob;
  }
  
  public List<FluxMagento> getListeFluxMagentoEnCours() {
    return listeFluxMagentoEnCours;
  }
  
}
