/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.initialisation;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0003i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND03 = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIOPT = 3;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PITOS = 1;
  public static final int SIZE_PICOS = 1;
  public static final int SIZE_PINOS = 6;
  public static final int DECIMAL_PINOS = 0;
  public static final int SIZE_PISOS = 1;
  public static final int DECIMAL_PISOS = 0;
  public static final int SIZE_PILOS = 4;
  public static final int DECIMAL_PILOS = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 69;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND03 = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINLI = 5;
  public static final int VAR_PIDAT = 6;
  public static final int VAR_PIOPT = 7;
  public static final int VAR_PIART = 8;
  public static final int VAR_PITOS = 9;
  public static final int VAR_PICOS = 10;
  public static final int VAR_PINOS = 11;
  public static final int VAR_PISOS = 12;
  public static final int VAR_PILOS = 13;
  public static final int VAR_PIARR = 14;
  
  // Variables AS400
  private String piind03 = ""; // Indicateurs
  private String picod = ""; // Code ERL *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de ligne à 0
  private BigDecimal pidat = BigDecimal.ZERO; // Date de traitement
  private String piopt = ""; // Option de traitement *
  private String piart = ""; // Code article
  private String pitos = ""; // Type origine sortie
  private String picos = ""; // Code origine sortie
  private BigDecimal pinos = BigDecimal.ZERO; // N°Bon origine sortie
  private BigDecimal pisos = BigDecimal.ZERO; // N°Suf.origine sortie
  private BigDecimal pilos = BigDecimal.ZERO; // N°Lig.origine sortie
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND03), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de ligne à 0
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date de traitement
      new AS400Text(SIZE_PIOPT), // Option de traitement *
      new AS400Text(SIZE_PIART), // Code article
      new AS400Text(SIZE_PITOS), // Type origine sortie
      new AS400Text(SIZE_PICOS), // Code origine sortie
      new AS400ZonedDecimal(SIZE_PINOS, DECIMAL_PINOS), // N°Bon origine sortie
      new AS400ZonedDecimal(SIZE_PISOS, DECIMAL_PISOS), // N°Suf.origine sortie
      new AS400ZonedDecimal(SIZE_PILOS, DECIMAL_PILOS), // N°Lig.origine sortie
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind03, picod, pietb, pinum, pisuf, pinli, pidat, piopt, piart, pitos, picos, pinos, pisos, pilos, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind03 = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinli = (BigDecimal) output[5];
    pidat = (BigDecimal) output[6];
    piopt = (String) output[7];
    piart = (String) output[8];
    pitos = (String) output[9];
    picos = (String) output[10];
    pinos = (BigDecimal) output[11];
    pisos = (BigDecimal) output[12];
    pilos = (BigDecimal) output[13];
    piarr = (String) output[14];
  }
  
  // -- Accesseurs
  
  public void setPiind03(String pPiind03) {
    if (pPiind03 == null) {
      return;
    }
    piind03 = pPiind03;
  }
  
  public String getPiind03() {
    return piind03;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPiopt(String pPiopt) {
    if (pPiopt == null) {
      return;
    }
    piopt = pPiopt;
  }
  
  public String getPiopt() {
    return piopt;
  }
  
  public void setPiart(String pPiart) {
    if (pPiart == null) {
      return;
    }
    piart = pPiart;
  }
  
  public String getPiart() {
    return piart;
  }
  
  public void setPitos(Character pPitos) {
    if (pPitos == null) {
      return;
    }
    pitos = String.valueOf(pPitos);
  }
  
  public Character getPitos() {
    return pitos.charAt(0);
  }
  
  public void setPicos(Character pPicos) {
    if (pPicos == null) {
      return;
    }
    picos = String.valueOf(pPicos);
  }
  
  public Character getPicos() {
    return picos.charAt(0);
  }
  
  public void setPinos(BigDecimal pPinos) {
    if (pPinos == null) {
      return;
    }
    pinos = pPinos.setScale(DECIMAL_PINOS, RoundingMode.HALF_UP);
  }
  
  public void setPinos(Integer pPinos) {
    if (pPinos == null) {
      return;
    }
    pinos = BigDecimal.valueOf(pPinos);
  }
  
  public Integer getPinos() {
    return pinos.intValue();
  }
  
  public void setPisos(BigDecimal pPisos) {
    if (pPisos == null) {
      return;
    }
    pisos = pPisos.setScale(DECIMAL_PISOS, RoundingMode.HALF_UP);
  }
  
  public void setPisos(Integer pPisos) {
    if (pPisos == null) {
      return;
    }
    pisos = BigDecimal.valueOf(pPisos);
  }
  
  public Integer getPisos() {
    return pisos.intValue();
  }
  
  public void setPilos(BigDecimal pPilos) {
    if (pPilos == null) {
      return;
    }
    pilos = pPilos.setScale(DECIMAL_PILOS, RoundingMode.HALF_UP);
  }
  
  public void setPilos(Integer pPilos) {
    if (pPilos == null) {
      return;
    }
    pilos = BigDecimal.valueOf(pPilos);
  }
  
  public Integer getPilos() {
    return pilos.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
