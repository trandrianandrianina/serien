/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.edition;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.dataarea.LdaSerien;
import ri.serien.libcommun.gescom.achat.document.EnumOptionValidation;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.OptionsEditionAchat;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

public class RpgEditerDocumentAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGAM0018";
  
  /**
   * Appel du programme RPG qui va éditer un document.
   */
  public String editerDocument(SystemeManager pSysteme, IdDocumentAchat pIdDocument, Date pDateTraitement,
      OptionsEditionAchat pOptionEdition, LdaSerien pLdaSerien) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pIdDocument == null) {
      throw new MessageErreurException("Le paramètre pIdDocument du service est à null.");
    }
    if (pOptionEdition == null) {
      throw new MessageErreurException("Les options d'édition sont invalides.");
    }
    if (pLdaSerien == null) {
      throw new MessageErreurException("Le paramètre LdaSerieN est invalide.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0018i entree = new Svgam0018i();
    Svgam0018o sortie = new Svgam0018o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    Trace.info("idDocument=" + pIdDocument);
    entree.setPiind("0000000000");
    entree.setPietb(pIdDocument.getCodeEtablissement());
    entree.setPicod(pIdDocument.getCodeEntete().getCode());
    entree.setPinum(pIdDocument.getNumero());
    entree.setPisuf(pIdDocument.getSuffixe());
    entree.setPidat(pDateTraitement);
    entree.setPiopt(EnumOptionValidation.VALIDATION.getCode());
    entree.setPiedt(pOptionEdition.getOptionEdition().getCode());
    if (pOptionEdition.isStockageBaseDocumentaire()) {
      entree.setPistk('1');
    }
    else {
      entree.setPistk(' ');
    }
    if (pOptionEdition.getIdMail() != null) {
      entree.setPinml(pOptionEdition.getIdMail().getNumero());
    }
    if (pOptionEdition.getIdFaxMail() != null) {
      entree.setPinfl(pOptionEdition.getIdFaxMail().getNumero());
    }
    if (pOptionEdition.getTypeDocumentStocke() != null) {
      entree.setPitdsk(pOptionEdition.getTypeDocumentStocke().getCode());
    }
    if (pOptionEdition.getIndicatifIdDocumentAStocker() != null) {
      entree.setPiiddc(pOptionEdition.getIndicatifIdDocumentAStocker());
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Mise à jour des données dans la LDA
    String curlib = pSysteme.getCurlib();
    if ((curlib != null) && (curlib.startsWith("FM"))) {
      // Met le numéro de la FM, nécessaire pour construire un chemin correct pour le stockage du PDF
      curlib = String.format("%-3s", curlib.substring(2));
      rpg.executerCommande("CHGDTAARA DTAARA(*LDA (13 3)) VALUE('" + curlib + "')");
    }
    // Création ou mise à jour de la DTAARA QTEMP/LDASERIEN qui contient le code écran pour les affectations d'impression
    rpg.executerCommande(pLdaSerien.getCommandeCLCreerDataArea());
    rpg.executerCommande(pLdaSerien.getCommandeCLMettreAJourDataArea());
    
    // Mise à jour de la locale pour les options d'édition
    if (pOptionEdition.getModeEdition() != null && pOptionEdition.getModeEdition().getEnumModeEdition() != null) {
      switch (pOptionEdition.getModeEdition().getEnumModeEdition()) {
        case VISUALISER:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE('@')");
          break;
        case IMPRIMER:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE(' ')");
          break;
        case ENVOYER_PAR_MAIL:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE('@')");
          break;
        case ENVOYER_PAR_FAX:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE('f')");
          break;
        default:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE(' ')");
      }
    }
    else {
      rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE(' ')");
    }
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      return null;
    }
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    controlerErreur();
    
    // Initialisation de la classe métier
    // Nettoayge du chemin qui n'est pas toujours parfait...
    String fichierPDF = sortie.getPochm().trim();
    fichierPDF = fichierPDF.replaceAll("//", "/");
    Trace.info("Editer le document PDF : " + fichierPDF);
    return fichierPDF;
  }
  
}
