/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx006.v1;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v1.JsonStockMagentoV1;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;

/**
 * Cette classe traite l'exportation des stocks pour les flux V1.
 */
public class ExportationStockMagentoV1 extends BaseGroupDB {
  protected ManagerFluxMagento gestionFlux = null;
  private static int NB_ART_STOCK_MAX = 500;
  
  /**
   * Constructeur.
   */
  public ExportationStockMagentoV1(ManagerFluxMagento gestFlux) {
    super(gestFlux.getQueryManager());
    gestionFlux = gestFlux;
  }
  
  /**
   * Retourne l'ensemble des stocks articles au format JSON.
   * 
   * @param pRecord
   * @return
   */
  public String retournerStocksJSON(FluxMagento pRecord) {
    if (pRecord == null || pRecord.getFLETB() == null) {
      majError("[GM_Export_Stocks] retournerStocksJSON() record null ou corrompu ");
      return null;
    }
    String retour = null;
    
    List<GenericRecord> liste = null;
    // On récupère le reliquat ou on traite de A à Z
    if (pRecord.getListeReliquat() != null) {
      liste = (List<GenericRecord>) (Object) pRecord.getListeReliquat();
    }
    else {
      liste = retournerStocksMouvementsDuJour(pRecord);
    }
    
    // On créee le flux de stock
    JsonStockMagentoV1 stocks = new JsonStockMagentoV1(pRecord);
    Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), stocks.getEtb());
    if (numeroInstance == null) {
      majError("[GM_Export_StocksV1] retournerStocksJSON() : probleme d'interpretation de l'ETb vers l'instance Magento");
      return null;
    }
    stocks.setIdInstanceMagento(numeroInstance);
    
    if (liste != null) {
      // Si des articles ont été modifiés dans cet interval
      if (liste.size() > 0) {
        if (liste.size() > NB_ART_STOCK_MAX) {
          liste = decoupageDuGrosFluxEnPlusieursPetitsFlux(pRecord, liste);
        }
        // on associe les articles
        stocks.mettreAJourLaListeArticles(liste);
        
        // log.ecritureMessage("liste des stocks : " +stocks.getListeStocks());
        // on transforme en JSON
        if (stocks.getListeStocks() != null && stocks.getListeStocks().size() > 0) {
          pRecord.setFLCOD("" + stocks.getListeStocks().size());
          if (initJSON()) {
            try {
              retour = gson.toJson(stocks);
            }
            catch (Exception e) {
              majError("[GM_Export_Stocks] retournerStocksJSON() PB  PARSE JSON ");
              return null;
            }
          }
          else {
            majError("[GM_Export_Stocks] retournerStocksJSON() PB initJSON ");
          }
        }
        else {
          majError("[GM_Export_Stocks] retournerStocksJSON() PB de getListeArticles() à NULL ou 0 ");
        }
      }
      // On a pas de modif dans ce laps de temps
      else {
        retour = "";
      }
    }
    
    return retour;
  }
  
  /**
   * Permet de retourner tous les articles ainsi que leur stock, qui ont subi des mouvements à la date du jour.
   * 
   * @param pRecord
   * @return
   */
  protected ArrayList<GenericRecord> retournerStocksMouvementsDuJour(FluxMagento pRecord) {
    if (pRecord == null || pRecord.getFLETB() == null) {
      majError("[GM_Export_Stocks] retournerStocksMouvementsDuJour() record null ou corrompu ");
      return null;
    }
    
    // CONTROLE DU DERNIER SUCCES ENVOI FLUX DES STOCKS
    ArrayList<GenericRecord> liste = null;
    String dateDernierEnvoi = "0";
    String heureDernierEnvoi = "0";
    String requete = "" + " SELECT  VARCHAR_FORMAT(MAX(FLTRT) ,'YYMMDD') AS MADATE, VARCHAR_FORMAT(MAX(FLTRT) ,'HH24MISS') AS MONHEURE "
        + " FROM " + queryManager.getLibrary() + "." + EnumTableBDD.FLUX + " " + " WHERE FLETB = '" + pRecord.getFLETB()
        + "' AND FLIDF = '" + FluxMagento.IDF_FLX006_STOCK + "' " + " AND FLSTT = '" + EnumStatutFlux.TRAITE.getNumero()
        + "' AND FLERR = '" + FluxMagento.ERR_PAS_ERREUR + "' ";
    
    liste = queryManager.select(requete);
    if (liste != null) {
      // Date
      if (liste.size() > 0 && liste.get(0).isPresentField("MADATE")) {
        dateDernierEnvoi = "1" + liste.get(0).getField("MADATE").toString().trim();
      }
      else {
        dateDernierEnvoi = "0";
      }
      // HEure
      if (liste.size() > 0 && liste.get(0).isPresentField("MONHEURE")) {
        heureDernierEnvoi = liste.get(0).getField("MONHEURE").toString().trim();
      }
      else {
        heureDernierEnvoi = "0";
      }
    }
    else {
      majError("[GM_Export_Stocks] retournerStocksMouvementsDuJour() LISTE 1 NULL " + queryManager.getMsgError() + " / " + requete);
      return null;
    }
    
    // RECUP DES STOCKS
    liste = null;
    requete = "" + " SELECT S1ETB,S1ART,(S1STD+ S1QEE + S1QSE + S1QDE + S1QEM + S1QSM + S1QDM+ S1QES "
        + " + S1QSS + S1QDS- S1RES) AS STK, S1ATT, S1DTMJ, S1HRMJ " + " FROM " + queryManager.getLibrary() + ".PGVMSTKM " + " LEFT JOIN "
        + queryManager.getLibrary() + ".PGVMARTM ON S1ETB = A1ETB AND S1ART = A1ART " + " WHERE S1ETB ='" + pRecord.getFLETB()
        + "' AND S1MAG = '" + gestionFlux.getParametresFlux().getMagasinMagento() + "' " + " AND ( S1DTMJ > '" + dateDernierEnvoi + "' "
        + " OR  (S1DTMJ = '" + dateDernierEnvoi + "' AND S1HRMJ >= (" + heureDernierEnvoi + "-300) ))" // On laisse 5mn de rab
        + " AND S1DTMJ <> '0' AND A1IN15 <> '1' " + " ORDER BY S1DTMJ,S1HRMJ ";
    
    liste = queryManager.select(requete);
    
    if (liste == null) {
      majError("[GM_Export_Stocks] retournerStocksMouvementsDuJour() LISTE 2 NULL " + queryManager.getMsgError() + " / " + requete);
    }
    
    return liste;
  }
  
  /**
   * Permet d'isoler des flux de stocks trop importants en plusieurs flux plus petits.
   * Le critère étant la quantité de lignes d'articles concernés.
   * 
   * @param pFluxInitial
   * @param pListeInitiale
   * @return
   */
  protected List<GenericRecord> decoupageDuGrosFluxEnPlusieursPetitsFlux(FluxMagento pFluxInitial, List<GenericRecord> pListeInitiale) {
    if (gestionFlux == null || pListeInitiale == null || pListeInitiale.size() == 0) {
      return pListeInitiale;
    }
    
    List<GenericRecord> listeAmaigrie = new ArrayList<GenericRecord>();
    List<Object> listeReliquat = new ArrayList<Object>();
    
    // On découpe le flux initial en 2 listes 1 à traiter et un reliquat
    for (int i = 0; i < pListeInitiale.size(); i++) {
      if (i < NB_ART_STOCK_MAX) {
        listeAmaigrie.add(pListeInitiale.get(i));
      }
      else {
        listeReliquat.add(pListeInitiale.get(i));
      }
    }
    
    if (listeReliquat.size() > 0) {
      gestionFlux.traiterReliquatFluxSortant(pFluxInitial, listeReliquat);
    }
    
    return listeAmaigrie;
  }
  
  /**
   * Met à null les variables.
   */
  @Override
  public void dispose() {
    queryManager = null;
  }
}
