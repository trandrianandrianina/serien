/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.programs.contact;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.serien.libas400.dao.exp.database.files.FFD_Psemrtem;
import ri.serien.libas400.dao.exp.programs.parametres.GM_PersonnalisationEXP;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.IdCategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;

/**
 * Classe contenant les zones du PSEMRTEM
 * @author Administrateur
 *
 */
public class M_Contact extends FFD_Psemrtem {
  // Variables de travail
  private int codeHierarchy = M_LienEvenementContact.NO_HIERARCHY;
  private String codeTypeContact = null; // TODO à voir si pas mieux de mettre createur par defaut
  private String etablissement = "";
  
  private LinkedHashMap<String, String> listCivility = null;
  private String[] listLabelCivility = null;
  private String[] listCodeCivility = null;
  
  private LinkedHashMap<String, String> listClass = null;
  private String[] listLabelClass = null;
  private String[] listCodeClass = null;
  
  private ArrayList<M_LienContactAvecTiers> listLienContactTiers = new ArrayList<M_LienContactAvecTiers>();
  private M_ContactExtension extensionContact = null;
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public M_Contact(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Insère l'enregistrement dans le table
   * @return
   *
   */
  @Override
  public boolean insertInDatabase() {
    setRENUM(computeRENUM4Insert());
    if ((REPAC == null) || (REPAC.trim().length() == 0)) {
      setREPAC(getRENOM() + " " + getREPRE());
    }
    
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert("PSEMRTEM", querymg.getLibrary());
    return request(requete);
  }
  
  /**
   * Modifie l'enregistrement dans le table
   * @return
   *
   */
  @Override
  public boolean updateInDatabase() {
    if ((REPAC == null) || (REPAC.trim().length() == 0)) {
      setREPAC(getRENOM() + " " + getREPRE());
    }
    
    initGenericRecord(genericrecord, false);
    String requete =
        genericrecord.createSQLRequestUpdate("PSEMRTEM", querymg.getLibrary(), "RENUM=" + getRENUM() + " and REETB='" + getREETB() + "'");
    return request(requete);
  }
  
  /**
   * Suppression de l'enregistrement courant
   * @return
   *
   */
  @Override
  public boolean deleteInDatabase() {
    String requete = "delete from " + querymg.getLibrary() + ".PSEMRTEM where RENUM=" + getRENUM() + " and REETB='" + getREETB() + "'";
    return request(requete);
  }
  
  /**
   * Retourne la zone Nom "amélioré"
   */
  @Override
  public String getRENOM() {
    if ((super.getRENOM() == null) || super.getRENOM().equals("")) {
      return getREPAC();
    }
    return super.getRENOM();
  }
  
  /**
   * Retourne la zone Prénom "amélioré"
   */
  @Override
  public String getREPRE() {
    if ((super.getREPRE() == null) || super.getREPRE().equals("")) {
      return "";
    }
    return super.getREPRE();
  }
  
  /**
   * Initialise tous les champs de la classe contact (pour l'instant le fichier PSEMRTEM)
   * @param contact
   */
  public Contact initContact() {
    Contact contact = new Contact(IdContact.getInstance(getRENUM())); // Numéro d'ordre (RENUM)
    if (!getRECIV().isEmpty()) {
      IdCivilite idCivilite = IdCivilite.getInstance(getRECIV());
      contact.setIdCivilite(idCivilite); // Civilité (RECIV)
    }
    contact.setNumeroTelephone1(getRETEL()); // Téléphone (RETEL)
    contact.setNumeroFax(getREFAX()); // Fax (REFAX)
    if (getRECAT() != null && !getRECAT().isEmpty()) {
      IdCategorieContact idCategorie = IdCategorieContact.getInstance(getRECAT());
      contact.setIdCategorie(idCategorie);// Catégorie (RECAT)
    }
    contact.setObservations(getREOBS()); // Observations (REOBS)
    contact.setCleClassement1(getRECL1()); // Clé de classement 1 (RECL1)
    contact.setNumeroPoste(getREPOS()); // N° poste (REPOS)
    contact.setCleIndexTelephone(getRECTL()); // Clé pour index tel (RECTL)
    contact.setEmail(getRENET()); // Adresse Email (RENET)
    contact.setDestinataire(getREIN1()); // 1=destinataire bon (REIN1)
    contact.setEmailCopieCachee(getREIN2()); // Copie Carbon cachée (REIN2)
    contact.setREIN3(getREIN3()); // Non utilisé (REIN3)
    contact.setCleClassement2(getRECL2()); // Clé de class.2 (RECL2)
    contact.setNumeroTelephone2(getRETEL2()); // Téléphone 2 (RETEL2)
    contact.setCriteresAnalyse1(getRETOP1()); // Critères d''analyse 1 (RETOP1)
    contact.setCriteresAnalyse2(getRETOP2()); // Critères d''analyse 2 (RETOP2)
    contact.setCriteresAnalyse3(getRETOP3()); // Critères d''analyse 3 (RETOP3)
    contact.setCriteresAnalyse4(getRETOP4()); // Critères d''analyse 4 (RETOP4)
    contact.setCriteresAnalyse5(getRETOP5()); // Critères d''analyse 5 (RETOP5)
    contact.setEmailAntiSpam(getREASPA()); // 1=ANTI-SPAM (REASPA)
    contact.setAntiAppel(getREAAPP()); // 1=ANTI-APPEL (REAAPP)
    contact.setTypeZonePersonnalisee(getRETYZP()); // 'TYPE ZP (RETYZP)
    contact.setNom(getRENOM()); // Nom contact (RENOM)
    contact.setPrenom(getREPRE()); // Prénom contact (REPRE)
    contact.setEmail2(getRENET2()); // Adresse Email2 (RENET2)
    contact.setProfilAlias(getREPRF()); // Profil / ALIAS (REPRF) lien avec le psemussm
    contact.setDestinataireFactures(getREIN4()); // Non utilisé (REIN4)
    contact.setREIN5(getREIN4()); // Non utilisé (REIN5)
    contact.setREIN6(getREIN4()); // Non utilisé (REIN6)
    contact.setREIN7(getREIN4()); // Non utilisé (REIN7)
    contact.setREIN8(getREIN4()); // Non utilisé (REIN8)
    
    return contact;
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
    
    listCivility.clear();
    listClass.clear();
    listLienContactTiers.clear();
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Charge les civilités possibles
   *
   */
  private void loadCivility() {
    GM_PersonnalisationEXP psemparm = new GM_PersonnalisationEXP(querymg);
    setListCivility(psemparm.getListCivility(etablissement));
  }
  
  /**
   * Charge les fonctions possibles
   *
   */
  private void loadClass() {
    GM_PersonnalisationEXP psemparm = new GM_PersonnalisationEXP(querymg);
    setListClass(psemparm.getListClass(etablissement));
  }
  
  /**
   * Calcule le numéro (id) avant l'insertion d'un enregistrement
   * @return
   *
   */
  private int computeRENUM4Insert() {
    String resultat = querymg.firstEnrgSelect("select max(RENM) from " + querymg.getLibrary() + ".psemrtem", 1);
    int number = 1;
    if (resultat != null) {
      number = Integer.parseInt(resultat) + 1;
    }
    return number;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le codeHierarchy
   */
  public int getCodeHierarchy() {
    return codeHierarchy;
  }
  
  /**
   * @param codeHierarchy le codeHierarchy à définir
   */
  public void setCodeHierarchy(int codeHierarchy) {
    this.codeHierarchy = codeHierarchy;
  }
  
  /**
   * @return le codeTypeContact
   */
  public String getCodeTypeContact() {
    return codeTypeContact;
  }
  
  /**
   * @param codeTypeContact le codeTypeContact à définir
   */
  public void setCodeTypeContact(String codeTypeContact) {
    this.codeTypeContact = codeTypeContact;
  }
  
  /**
   * @return le listLienContactTiers
   */
  public ArrayList<M_LienContactAvecTiers> getListLienContactTiers() {
    return listLienContactTiers;
  }
  
  /**
   * @param listLienContactTiers le listLienContactTiers à définir
   */
  public void setListLienContactTiers(ArrayList<M_LienContactAvecTiers> listLienContactTiers) {
    this.listLienContactTiers = listLienContactTiers;
  }
  
  /**
   * @return le listCivility
   *
   */
  public LinkedHashMap<String, String> getListCivility() {
    if (listCivility == null) {
      loadCivility();
    }
    return listCivility;
  }
  
  /**
   * @param listCivility le listCivility à définir
   */
  public void setListCivility(LinkedHashMap<String, String> listCivility) {
    this.listCivility = listCivility;
    
    // Chargement de la table des libellés
    listLabelCivility = new String[listCivility.size()];
    listCodeCivility = new String[listCivility.size()];
    int i = 0;
    for (Entry<String, String> entry : listCivility.entrySet()) {
      listCodeCivility[i] = entry.getKey().trim();
      listLabelCivility[i++] = entry.getValue().trim();
    }
  }
  
  /**
   * @return le listLabelCivility
   *
   */
  public String[] getListLabelCivility() {
    if (listCivility == null) {
      loadCivility();
    }
    return listLabelCivility;
  }
  
  /**
   * @return le listCodeCivility
   *
   */
  public String[] getListCodeCivility() {
    if (listCivility == null) {
      loadCivility();
    }
    return listCodeCivility;
  }
  
  /**
   * @return le listClass
   *
   */
  public LinkedHashMap<String, String> getListClass() {
    if (listClass == null) {
      loadClass();
    }
    return listClass;
  }
  
  /**
   * @param listClass le listClass à définir
   */
  public void setListClass(LinkedHashMap<String, String> listClass) {
    this.listClass = listClass;
    
    // Chargement de la table des libellés
    listLabelClass = new String[listClass.size()];
    listCodeClass = new String[listClass.size()];
    int i = 0;
    for (Entry<String, String> entry : listClass.entrySet()) {
      listCodeClass[i] = entry.getKey().trim();
      listLabelClass[i++] = entry.getValue().trim();
    }
  }
  
  /**
   * @return le listLabelClass
   *
   */
  public String[] getListLabelClass() {
    if (listClass == null) {
      loadClass();
    }
    return listLabelClass;
  }
  
  /**
   * @return le listCodeClass
   *
   */
  public String[] getListCodeClass() {
    if (listClass == null) {
      loadClass();
    }
    return listCodeClass;
  }
  
  /**
   * @return le extensionContact
   */
  public M_ContactExtension getExtensionContact() {
    return extensionContact;
  }
  
  /**
   * @param extensionContact le extensionContact à définir
   */
  public void setExtensionContact(M_ContactExtension extensionContact) {
    this.extensionContact = extensionContact;
  }
  
}
