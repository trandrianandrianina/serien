/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.client.creation;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0022o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POCLI = 6;
  public static final int DECIMAL_POCLI = 0;
  public static final int SIZE_POLIV = 3;
  public static final int DECIMAL_POLIV = 0;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 20;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POCLI = 1;
  public static final int VAR_POLIV = 2;
  public static final int VAR_POARR = 3;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal pocli = BigDecimal.ZERO; // Numéro client créé
  private BigDecimal poliv = BigDecimal.ZERO; // Suffixe client
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_POCLI, DECIMAL_POCLI), // Numéro client créé
      new AS400ZonedDecimal(SIZE_POLIV, DECIMAL_POLIV), // Suffixe client
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, pocli, poliv, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    pocli = (BigDecimal) output[1];
    poliv = (BigDecimal) output[2];
    poarr = (String) output[3];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPocli(BigDecimal pPocli) {
    if (pPocli == null) {
      return;
    }
    pocli = pPocli.setScale(DECIMAL_POCLI, RoundingMode.HALF_UP);
  }
  
  public void setPocli(Integer pPocli) {
    if (pPocli == null) {
      return;
    }
    pocli = BigDecimal.valueOf(pPocli);
  }
  
  public Integer getPocli() {
    return pocli.intValue();
  }
  
  public void setPoliv(BigDecimal pPoliv) {
    if (pPoliv == null) {
      return;
    }
    poliv = pPoliv.setScale(DECIMAL_POLIV, RoundingMode.HALF_UP);
  }
  
  public void setPoliv(Integer pPoliv) {
    if (pPoliv == null) {
      return;
    }
    poliv = BigDecimal.valueOf(pPoliv);
  }
  
  public Integer getPoliv() {
    return poliv.intValue();
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
