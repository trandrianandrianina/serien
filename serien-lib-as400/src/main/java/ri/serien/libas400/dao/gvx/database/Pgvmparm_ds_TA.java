/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_TA pour les TA
 */
public class Pgvmparm_ds_TA extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "TALIB"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "TADAP"));
    // A contrôler car à l'origine c'est une zone packed
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(30, 0), "TACO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(100), "TALB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TAIN1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TAIN2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TAIN3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TAART"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(13), "TATIR"));
    
    length = 300;
  }
}
