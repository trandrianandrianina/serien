/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_DG pour les DG
 */
public class DsDescriptionGenerale extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "DGTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "DGCOD"));
    // Nom ou raison sociale
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGNOM"));
    // Complément de nom
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGCPL"));
    // Taux de TVA
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT01"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT02"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT03"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT04"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT05"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGT06"));
    // Taxe parafiscale
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGTPF"));
    
    // DGDIVG (11)
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(11, 0), "DGDIVG"));
    // Arrondi
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(1, 0), "DGRON"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(4, 0), "DGDEB"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(4, 0), "DGFIN"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(2, 0), "DGMEX"));
    
    // DGEX1G (9)
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX1G"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(1, 0), "DGEX1G_"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(4, 0), "DGDE1"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(4, 0), "DGFE1"));
    
    // DGDATG (9)
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGDATG"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(4, 0), "DGDEC"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(4, 0), "DGDER"));
    // Champ qui ne sert à rien mais nécessaire car sinon cela décale la lecture du buffer
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(1, 0), "DGDATGNOP"));
    
    // DGEX0G (9)
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX0G"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(1, 0), "DGEX0G_"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(4, 0), "DGDE0"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(4, 0), "DGFE0"));
    
    // DGETP (établissement pilote sur 3 caractères) ou DGMAG + 1 caractère qui ne sert à rien dans ce cas
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGETP"));
    
    // Coefficient de vente
    // DGCMAG (9)
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGCMAG"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 4), "DGCMA"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(3, 2), "DGCMX"));
    // rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(1, 0), "DGCMAG_"));
    
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(3, 0), "DGNBS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 0), "DGNUM"));
    // Condition générale
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "DGCNG"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGTC1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGTC2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGTC3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGTC4"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "DGJRL"));
    // Pourcentage de base
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "DGRBP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGEUR"));
    // Code devise
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGDEV"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGDEP"));
    // Condition promo
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "DGCNP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGIN1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGIN2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGCGE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(13), "DGICS"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "DGPXFL"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "DGMTES"));
    
    length = 300;
  }
  
}
