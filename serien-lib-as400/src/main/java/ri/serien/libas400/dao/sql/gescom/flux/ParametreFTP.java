/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux;

public class ParametreFTP {
  private String bibli = null;
  private String code = null;
  private String libelle = null;
  private String adresse = null;
  private String port = null;
  private String user = null;
  private String motPasse = null;
  private String userLocal = null;
  private int type = 0;
  private String repertoireLocal = null;
  private String repertoireDistant = null;
  private String nomFichier = null;
  private boolean supprLocalApTranfert = false;
  private boolean supprDistantApTranfert = false;
  
  public ParametreFTP(String pBib, String pCode) {
    bibli = pBib;
    code = pCode;
  }
  
  public String getCode() {
    return code;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  public String getAdresse() {
    return adresse;
  }
  
  public void setAdresse(String adresse) {
    this.adresse = adresse;
  }
  
  public String getPort() {
    return port;
  }
  
  public void setPort(String port) {
    this.port = port;
  }
  
  public String getUser() {
    return user;
  }
  
  public void setUser(String user) {
    this.user = user;
  }
  
  public String getMotPasse() {
    return motPasse;
  }
  
  public void setMotPasse(String motPasse) {
    this.motPasse = motPasse;
  }
  
  public String getUserLocal() {
    return userLocal;
  }
  
  public void setUserLocal(String userLocal) {
    this.userLocal = userLocal;
  }
  
  public int getType() {
    return type;
  }
  
  public void setType(int type) {
    this.type = type;
  }
  
  public String getRepertoireLocal() {
    return repertoireLocal;
  }
  
  public void setRepertoireLocal(String repertoireLocal) {
    this.repertoireLocal = repertoireLocal;
  }
  
  public String getRepertoireDistant() {
    return repertoireDistant;
  }
  
  public void setRepertoireDistant(String repertoireDistant) {
    this.repertoireDistant = repertoireDistant;
  }
  
  public String getNomFichier() {
    return nomFichier;
  }
  
  public void setNomFichier(String nomFichier) {
    this.nomFichier = nomFichier;
  }
  
  public boolean isSupprLocalApTranfert() {
    return supprLocalApTranfert;
  }
  
  public void setSupprLocalApTranfert(boolean supprLocalApTranfert) {
    this.supprLocalApTranfert = supprLocalApTranfert;
  }
  
  public boolean isSupprDistantApTranfert() {
    return supprDistantApTranfert;
  }
  
  public void setSupprDistantApTranfert(boolean supprDistantApTranfert) {
    this.supprDistantApTranfert = supprDistantApTranfert;
  }
  
  public String getBibli() {
    return bibli;
  }
  
}
