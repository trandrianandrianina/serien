/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0021o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_E1CRE = 7;
  public static final int DECIMAL_E1CRE = 0;
  public static final int SIZE_E1HOM = 7;
  public static final int DECIMAL_E1HOM = 0;
  public static final int SIZE_E1EXP = 7;
  public static final int DECIMAL_E1EXP = 0;
  public static final int SIZE_E1FAC = 7;
  public static final int DECIMAL_E1FAC = 0;
  public static final int SIZE_E1NFA6 = 6;
  public static final int DECIMAL_E1NFA6 = 0;
  public static final int SIZE_E1NLIG = 15;
  public static final int DECIMAL_E1NLIG = 0;
  public static final int SIZE_E1ETA = 1;
  public static final int DECIMAL_E1ETA = 0;
  public static final int SIZE_E1EDT = 1;
  public static final int DECIMAL_E1EDT = 0;
  public static final int SIZE_E1TCL = 1;
  public static final int DECIMAL_E1TCL = 0;
  public static final int SIZE_E1TCF = 1;
  public static final int DECIMAL_E1TCF = 0;
  public static final int SIZE_E1TVA1 = 1;
  public static final int DECIMAL_E1TVA1 = 0;
  public static final int SIZE_E1TVA2 = 1;
  public static final int DECIMAL_E1TVA2 = 0;
  public static final int SIZE_E1TVA3 = 1;
  public static final int DECIMAL_E1TVA3 = 0;
  public static final int SIZE_E1TPF = 1;
  public static final int DECIMAL_E1TPF = 0;
  public static final int SIZE_E1RLV = 1;
  public static final int DECIMAL_E1RLV = 0;
  public static final int SIZE_E1NEX = 1;
  public static final int DECIMAL_E1NEX = 0;
  public static final int SIZE_E1TAR = 2;
  public static final int DECIMAL_E1TAR = 0;
  public static final int SIZE_E1RBF = 1;
  public static final int DECIMAL_E1RBF = 0;
  public static final int SIZE_E1SGN = 1;
  public static final int DECIMAL_E1SGN = 0;
  public static final int SIZE_E1SL1 = 9;
  public static final int DECIMAL_E1SL1 = 2;
  public static final int SIZE_E1SL2 = 9;
  public static final int DECIMAL_E1SL2 = 2;
  public static final int SIZE_E1SL3 = 9;
  public static final int DECIMAL_E1SL3 = 2;
  public static final int SIZE_E1THTL = 9;
  public static final int DECIMAL_E1THTL = 2;
  public static final int SIZE_E1TL1 = 9;
  public static final int DECIMAL_E1TL1 = 2;
  public static final int SIZE_E1TL2 = 9;
  public static final int DECIMAL_E1TL2 = 2;
  public static final int SIZE_E1TL3 = 9;
  public static final int DECIMAL_E1TL3 = 2;
  public static final int SIZE_E1STP = 9;
  public static final int DECIMAL_E1STP = 2;
  public static final int SIZE_E1MTP = 7;
  public static final int DECIMAL_E1MTP = 2;
  public static final int SIZE_E1MES = 7;
  public static final int DECIMAL_E1MES = 2;
  public static final int SIZE_E1TTC = 9;
  public static final int DECIMAL_E1TTC = 2;
  public static final int SIZE_E1TPVL = 9;
  public static final int DECIMAL_E1TPVL = 2;
  public static final int SIZE_E1TPRL = 9;
  public static final int DECIMAL_E1TPRL = 2;
  public static final int SIZE_E1TO1G = 9;
  public static final int DECIMAL_E1TO1G = 0;
  public static final int SIZE_E1SE1 = 9;
  public static final int DECIMAL_E1SE1 = 2;
  public static final int SIZE_E1SE2 = 9;
  public static final int DECIMAL_E1SE2 = 2;
  public static final int SIZE_E1SE3 = 9;
  public static final int DECIMAL_E1SE3 = 2;
  public static final int SIZE_E1AFF = 7;
  public static final int DECIMAL_E1AFF = 0;
  public static final int SIZE_E1ARR = 1;
  public static final int DECIMAL_E1ARR = 0;
  public static final int SIZE_E1TDV = 1;
  public static final int DECIMAL_E1TDV = 0;
  public static final int SIZE_E1MCI = 9;
  public static final int DECIMAL_E1MCI = 2;
  public static final int SIZE_E1NFA = 7;
  public static final int DECIMAL_E1NFA = 0;
  public static final int SIZE_E1CLCG = 7;
  public static final int DECIMAL_E1CLCG = 0;
  public static final int SIZE_E1GBA = 1;
  public static final int DECIMAL_E1GBA = 0;
  public static final int SIZE_E1DCO = 7;
  public static final int DECIMAL_E1DCO = 0;
  public static final int SIZE_E1DLP = 7;
  public static final int DECIMAL_E1DLP = 0;
  public static final int SIZE_E1DLS = 7;
  public static final int DECIMAL_E1DLS = 0;
  public static final int SIZE_E1IN10 = 1;
  public static final int SIZE_E1CC1 = 6;
  public static final int DECIMAL_E1CC1 = 0;
  public static final int SIZE_E1ESC = 3;
  public static final int DECIMAL_E1ESC = 2;
  public static final int SIZE_E1TV1 = 5;
  public static final int DECIMAL_E1TV1 = 2;
  public static final int SIZE_E1TV2 = 5;
  public static final int DECIMAL_E1TV2 = 2;
  public static final int SIZE_E1TV3 = 5;
  public static final int DECIMAL_E1TV3 = 2;
  public static final int SIZE_E1TTP = 5;
  public static final int DECIMAL_E1TTP = 3;
  public static final int SIZE_E1COMG = 5;
  public static final int DECIMAL_E1COMG = 2;
  public static final int SIZE_E1CO2G = 5;
  public static final int DECIMAL_E1CO2G = 2;
  public static final int SIZE_E1CLFP = 6;
  public static final int DECIMAL_E1CLFP = 0;
  public static final int SIZE_E1CLFS = 3;
  public static final int DECIMAL_E1CLFS = 0;
  public static final int SIZE_E1TFA = 1;
  public static final int SIZE_E1DPR = 2;
  public static final int DECIMAL_E1DPR = 0;
  public static final int SIZE_E1COL = 4;
  public static final int DECIMAL_E1COL = 0;
  public static final int SIZE_E1PDS = 7;
  public static final int DECIMAL_E1PDS = 2;
  public static final int SIZE_E1BAS = 5;
  public static final int DECIMAL_E1BAS = 0;
  public static final int SIZE_E1CHG = 11;
  public static final int DECIMAL_E1CHG = 6;
  public static final int SIZE_E1RBC = 7;
  public static final int DECIMAL_E1RBC = 2;
  public static final int SIZE_E1RBC2 = 7;
  public static final int DECIMAL_E1RBC2 = 2;
  public static final int SIZE_E1DAT1 = 7;
  public static final int DECIMAL_E1DAT1 = 0;
  public static final int SIZE_E1DAT2 = 7;
  public static final int DECIMAL_E1DAT2 = 0;
  public static final int SIZE_E1DAT3 = 7;
  public static final int DECIMAL_E1DAT3 = 0;
  public static final int SIZE_E1DAT4 = 7;
  public static final int DECIMAL_E1DAT4 = 0;
  public static final int SIZE_E1REM1 = 4;
  public static final int DECIMAL_E1REM1 = 2;
  public static final int SIZE_E1REM2 = 4;
  public static final int DECIMAL_E1REM2 = 2;
  public static final int SIZE_E1REM3 = 4;
  public static final int DECIMAL_E1REM3 = 2;
  public static final int SIZE_E1REM4 = 4;
  public static final int DECIMAL_E1REM4 = 2;
  public static final int SIZE_E1REM5 = 4;
  public static final int DECIMAL_E1REM5 = 2;
  public static final int SIZE_E1REM6 = 4;
  public static final int DECIMAL_E1REM6 = 2;
  public static final int SIZE_E1RP1 = 4;
  public static final int DECIMAL_E1RP1 = 2;
  public static final int SIZE_E1RP2 = 4;
  public static final int DECIMAL_E1RP2 = 2;
  public static final int SIZE_E1RP3 = 4;
  public static final int DECIMAL_E1RP3 = 2;
  public static final int SIZE_E1RP4 = 4;
  public static final int DECIMAL_E1RP4 = 2;
  public static final int SIZE_E1RP5 = 4;
  public static final int DECIMAL_E1RP5 = 2;
  public static final int SIZE_E1RP6 = 4;
  public static final int DECIMAL_E1RP6 = 2;
  public static final int SIZE_E1HRE = 4;
  public static final int DECIMAL_E1HRE = 0;
  public static final int SIZE_E1NTO = 5;
  public static final int DECIMAL_E1NTO = 0;
  public static final int SIZE_E1OTO = 5;
  public static final int DECIMAL_E1OTO = 0;
  public static final int SIZE_E1VOL = 7;
  public static final int DECIMAL_E1VOL = 2;
  public static final int SIZE_E1LGM = 4;
  public static final int DECIMAL_E1LGM = 2;
  public static final int SIZE_E1M2P = 4;
  public static final int DECIMAL_E1M2P = 2;
  public static final int SIZE_E1DAT5 = 7;
  public static final int DECIMAL_E1DAT5 = 0;
  public static final int SIZE_E1DAT6 = 7;
  public static final int DECIMAL_E1DAT6 = 0;
  public static final int SIZE_E1DAT7 = 7;
  public static final int DECIMAL_E1DAT7 = 0;
  public static final int SIZE_E1NCC = 8;
  public static final int SIZE_E1REP = 2;
  public static final int SIZE_E1REP2 = 2;
  public static final int SIZE_E1MAG = 2;
  public static final int SIZE_E1MAGA = 2;
  public static final int SIZE_E1MEX = 2;
  public static final int SIZE_E1CTR = 2;
  public static final int SIZE_E1SAN = 4;
  public static final int SIZE_E1ACT = 4;
  public static final int SIZE_E1DEV = 3;
  public static final int SIZE_E1CNV = 6;
  public static final int SIZE_E1RCC = 25;
  public static final int SIZE_E1VDE = 3;
  public static final int SIZE_E1TRC = 1;
  public static final int SIZE_E1ZTR = 5;
  public static final int SIZE_E1EBP = 1;
  public static final int SIZE_E1TRP = 1;
  public static final int SIZE_E1NHO = 1;
  public static final int SIZE_E1TP1 = 2;
  public static final int SIZE_E1TP2 = 2;
  public static final int SIZE_E1TP3 = 2;
  public static final int SIZE_E1TP4 = 2;
  public static final int SIZE_E1TP5 = 2;
  public static final int SIZE_E1VEH = 10;
  public static final int SIZE_E1IN1 = 1;
  public static final int SIZE_E1IN2 = 1;
  public static final int SIZE_E1IN3 = 1;
  public static final int SIZE_E1IN4 = 1;
  public static final int SIZE_E1IN5 = 1;
  public static final int SIZE_E1IN6 = 1;
  public static final int SIZE_E1IN7 = 1;
  public static final int SIZE_E1IN8 = 1;
  public static final int SIZE_E1IN9 = 1;
  public static final int SIZE_E1CCT = 10;
  public static final int SIZE_E1TRL = 1;
  public static final int SIZE_E1BRL = 1;
  public static final int SIZE_E1TRE = 1;
  public static final int SIZE_E1FIL2 = 1;
  public static final int SIZE_E1IN11 = 1;
  public static final int SIZE_E1IN12 = 1;
  public static final int SIZE_E1IN13 = 1;
  public static final int SIZE_E1IN14 = 1;
  public static final int SIZE_E1IN15 = 1;
  public static final int SIZE_E1IN16 = 1;
  public static final int SIZE_E1CAN = 3;
  public static final int SIZE_E1CRT = 5;
  public static final int SIZE_E1PRE = 3;
  public static final int SIZE_E1PFC = 3;
  public static final int SIZE_E1IN17 = 1;
  public static final int SIZE_E1IN18 = 1;
  public static final int SIZE_E1IN19 = 1;
  public static final int SIZE_E1IN20 = 1;
  public static final int SIZE_E1IN21 = 1;
  public static final int SIZE_E1IN22 = 1;
  public static final int SIZE_E1NAT = 1;
  public static final int SIZE_POENV = 1;
  public static final int SIZE_PODIR = 1;
  public static final int SIZE_POIMD = 1;
  public static final int SIZE_POCHAN = 6;
  public static final int DECIMAL_POCHAN = 0;
  public static final int SIZE_POCOL = 1;
  public static final int DECIMAL_POCOL = 0;
  public static final int SIZE_POFRS = 6;
  public static final int DECIMAL_POFRS = 0;
  public static final int SIZE_POFRE = 4;
  public static final int DECIMAL_POFRE = 0;
  public static final int SIZE_POINEX = 1;
  public static final int DECIMAL_POINEX = 0;
  public static final int SIZE_POENLV = 120;
  public static final int SIZE_POBAN = 6;
  public static final int DECIMAL_POBAN = 0;
  public static final int SIZE_POBAS = 1;
  public static final int DECIMAL_POBAS = 0;
  public static final int SIZE_POBAE = 1;
  public static final int DECIMAL_POBAE = 0;
  public static final int SIZE_POBON = 6;
  public static final int DECIMAL_POBON = 0;
  public static final int SIZE_POBOS = 1;
  public static final int DECIMAL_POBOS = 0;
  public static final int SIZE_E1CLLP = 6;
  public static final int DECIMAL_E1CLLP = 0;
  public static final int SIZE_E1CLLS = 3;
  public static final int DECIMAL_E1CLLS = 0;
  public static final int SIZE_PODAT3 = 7;
  public static final int DECIMAL_PODAT3 = 0;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 812;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_E1CRE = 1;
  public static final int VAR_E1HOM = 2;
  public static final int VAR_E1EXP = 3;
  public static final int VAR_E1FAC = 4;
  public static final int VAR_E1NFA6 = 5;
  public static final int VAR_E1NLIG = 6;
  public static final int VAR_E1ETA = 7;
  public static final int VAR_E1EDT = 8;
  public static final int VAR_E1TCL = 9;
  public static final int VAR_E1TCF = 10;
  public static final int VAR_E1TVA1 = 11;
  public static final int VAR_E1TVA2 = 12;
  public static final int VAR_E1TVA3 = 13;
  public static final int VAR_E1TPF = 14;
  public static final int VAR_E1RLV = 15;
  public static final int VAR_E1NEX = 16;
  public static final int VAR_E1TAR = 17;
  public static final int VAR_E1RBF = 18;
  public static final int VAR_E1SGN = 19;
  public static final int VAR_E1SL1 = 20;
  public static final int VAR_E1SL2 = 21;
  public static final int VAR_E1SL3 = 22;
  public static final int VAR_E1THTL = 23;
  public static final int VAR_E1TL1 = 24;
  public static final int VAR_E1TL2 = 25;
  public static final int VAR_E1TL3 = 26;
  public static final int VAR_E1STP = 27;
  public static final int VAR_E1MTP = 28;
  public static final int VAR_E1MES = 29;
  public static final int VAR_E1TTC = 30;
  public static final int VAR_E1TPVL = 31;
  public static final int VAR_E1TPRL = 32;
  public static final int VAR_E1TO1G = 33;
  public static final int VAR_E1SE1 = 34;
  public static final int VAR_E1SE2 = 35;
  public static final int VAR_E1SE3 = 36;
  public static final int VAR_E1AFF = 37;
  public static final int VAR_E1ARR = 38;
  public static final int VAR_E1TDV = 39;
  public static final int VAR_E1MCI = 40;
  public static final int VAR_E1NFA = 41;
  public static final int VAR_E1CLCG = 42;
  public static final int VAR_E1GBA = 43;
  public static final int VAR_E1DCO = 44;
  public static final int VAR_E1DLP = 45;
  public static final int VAR_E1DLS = 46;
  public static final int VAR_E1IN10 = 47;
  public static final int VAR_E1CC1 = 48;
  public static final int VAR_E1ESC = 49;
  public static final int VAR_E1TV1 = 50;
  public static final int VAR_E1TV2 = 51;
  public static final int VAR_E1TV3 = 52;
  public static final int VAR_E1TTP = 53;
  public static final int VAR_E1COMG = 54;
  public static final int VAR_E1CO2G = 55;
  public static final int VAR_E1CLFP = 56;
  public static final int VAR_E1CLFS = 57;
  public static final int VAR_E1TFA = 58;
  public static final int VAR_E1DPR = 59;
  public static final int VAR_E1COL = 60;
  public static final int VAR_E1PDS = 61;
  public static final int VAR_E1BAS = 62;
  public static final int VAR_E1CHG = 63;
  public static final int VAR_E1RBC = 64;
  public static final int VAR_E1RBC2 = 65;
  public static final int VAR_E1DAT1 = 66;
  public static final int VAR_E1DAT2 = 67;
  public static final int VAR_E1DAT3 = 68;
  public static final int VAR_E1DAT4 = 69;
  public static final int VAR_E1REM1 = 70;
  public static final int VAR_E1REM2 = 71;
  public static final int VAR_E1REM3 = 72;
  public static final int VAR_E1REM4 = 73;
  public static final int VAR_E1REM5 = 74;
  public static final int VAR_E1REM6 = 75;
  public static final int VAR_E1RP1 = 76;
  public static final int VAR_E1RP2 = 77;
  public static final int VAR_E1RP3 = 78;
  public static final int VAR_E1RP4 = 79;
  public static final int VAR_E1RP5 = 80;
  public static final int VAR_E1RP6 = 81;
  public static final int VAR_E1HRE = 82;
  public static final int VAR_E1NTO = 83;
  public static final int VAR_E1OTO = 84;
  public static final int VAR_E1VOL = 85;
  public static final int VAR_E1LGM = 86;
  public static final int VAR_E1M2P = 87;
  public static final int VAR_E1DAT5 = 88;
  public static final int VAR_E1DAT6 = 89;
  public static final int VAR_E1DAT7 = 90;
  public static final int VAR_E1NCC = 91;
  public static final int VAR_E1REP = 92;
  public static final int VAR_E1REP2 = 93;
  public static final int VAR_E1MAG = 94;
  public static final int VAR_E1MAGA = 95;
  public static final int VAR_E1MEX = 96;
  public static final int VAR_E1CTR = 97;
  public static final int VAR_E1SAN = 98;
  public static final int VAR_E1ACT = 99;
  public static final int VAR_E1DEV = 100;
  public static final int VAR_E1CNV = 101;
  public static final int VAR_E1RCC = 102;
  public static final int VAR_E1VDE = 103;
  public static final int VAR_E1TRC = 104;
  public static final int VAR_E1ZTR = 105;
  public static final int VAR_E1EBP = 106;
  public static final int VAR_E1TRP = 107;
  public static final int VAR_E1NHO = 108;
  public static final int VAR_E1TP1 = 109;
  public static final int VAR_E1TP2 = 110;
  public static final int VAR_E1TP3 = 111;
  public static final int VAR_E1TP4 = 112;
  public static final int VAR_E1TP5 = 113;
  public static final int VAR_E1VEH = 114;
  public static final int VAR_E1IN1 = 115;
  public static final int VAR_E1IN2 = 116;
  public static final int VAR_E1IN3 = 117;
  public static final int VAR_E1IN4 = 118;
  public static final int VAR_E1IN5 = 119;
  public static final int VAR_E1IN6 = 120;
  public static final int VAR_E1IN7 = 121;
  public static final int VAR_E1IN8 = 122;
  public static final int VAR_E1IN9 = 123;
  public static final int VAR_E1CCT = 124;
  public static final int VAR_E1TRL = 125;
  public static final int VAR_E1BRL = 126;
  public static final int VAR_E1TRE = 127;
  public static final int VAR_E1FIL2 = 128;
  public static final int VAR_E1IN11 = 129;
  public static final int VAR_E1IN12 = 130;
  public static final int VAR_E1IN13 = 131;
  public static final int VAR_E1IN14 = 132;
  public static final int VAR_E1IN15 = 133;
  public static final int VAR_E1IN16 = 134;
  public static final int VAR_E1CAN = 135;
  public static final int VAR_E1CRT = 136;
  public static final int VAR_E1PRE = 137;
  public static final int VAR_E1PFC = 138;
  public static final int VAR_E1IN17 = 139;
  public static final int VAR_E1IN18 = 140;
  public static final int VAR_E1IN19 = 141;
  public static final int VAR_E1IN20 = 142;
  public static final int VAR_E1IN21 = 143;
  public static final int VAR_E1IN22 = 144;
  public static final int VAR_E1NAT = 145;
  public static final int VAR_POENV = 146;
  public static final int VAR_PODIR = 147;
  public static final int VAR_POIMD = 148;
  public static final int VAR_POCHAN = 149;
  public static final int VAR_POCOL = 150;
  public static final int VAR_POFRS = 151;
  public static final int VAR_POFRE = 152;
  public static final int VAR_POINEX = 153;
  public static final int VAR_POENLV = 154;
  public static final int VAR_POBAN = 155;
  public static final int VAR_POBAS = 156;
  public static final int VAR_POBAE = 157;
  public static final int VAR_POBON = 158;
  public static final int VAR_POBOS = 159;
  public static final int VAR_E1CLLP = 160;
  public static final int VAR_E1CLLS = 161;
  public static final int VAR_PODAT3 = 162;
  public static final int VAR_POARR = 163;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal e1cre = BigDecimal.ZERO; // Date Création
  private BigDecimal e1hom = BigDecimal.ZERO; // Date Homologation
  private BigDecimal e1exp = BigDecimal.ZERO; // Date Expédition
  private BigDecimal e1fac = BigDecimal.ZERO; // Date Facturation
  private BigDecimal e1nfa6 = BigDecimal.ZERO; // Numéro de facture
  private BigDecimal e1nlig = BigDecimal.ZERO; // Groupe Nombre de Lignes
  private BigDecimal e1eta = BigDecimal.ZERO; // Etat du bon
  private BigDecimal e1edt = BigDecimal.ZERO; // Top édition
  private BigDecimal e1tcl = BigDecimal.ZERO; // Top client livré
  private BigDecimal e1tcf = BigDecimal.ZERO; // Top client facturé
  private BigDecimal e1tva1 = BigDecimal.ZERO; // N° TVA 1
  private BigDecimal e1tva2 = BigDecimal.ZERO; // N° TVA 2
  private BigDecimal e1tva3 = BigDecimal.ZERO; // N° TVA 3
  private BigDecimal e1tpf = BigDecimal.ZERO; // Top Taxe parafiscale
  private BigDecimal e1rlv = BigDecimal.ZERO; // Top Relevé (Rang du RGL/RLV)
  private BigDecimal e1nex = BigDecimal.ZERO; // Nbre d'exemplaires factures
  private BigDecimal e1tar = BigDecimal.ZERO; // N° de tarif
  private BigDecimal e1rbf = BigDecimal.ZERO; // Top regroup.bon/facture
  private BigDecimal e1sgn = BigDecimal.ZERO; // SIGNE DU BON
  private BigDecimal e1sl1 = BigDecimal.ZERO; // Soumis T.V.A 1
  private BigDecimal e1sl2 = BigDecimal.ZERO; // Soumis T.V.A 2
  private BigDecimal e1sl3 = BigDecimal.ZERO; // Soumis T.V.A 3
  private BigDecimal e1thtl = BigDecimal.ZERO; // Total Hors Taxes Lignes
  private BigDecimal e1tl1 = BigDecimal.ZERO; // Montant TVA 1
  private BigDecimal e1tl2 = BigDecimal.ZERO; // Montant TVA 2
  private BigDecimal e1tl3 = BigDecimal.ZERO; // Montant TVA 3
  private BigDecimal e1stp = BigDecimal.ZERO; // Soumis à Taxe Parafiscale
  private BigDecimal e1mtp = BigDecimal.ZERO; // Montant Taxe Parafiscale
  private BigDecimal e1mes = BigDecimal.ZERO; // Montant Escompte
  private BigDecimal e1ttc = BigDecimal.ZERO; // Total TTC
  private BigDecimal e1tpvl = BigDecimal.ZERO; // Total HT Lignes au Prix de
  private BigDecimal e1tprl = BigDecimal.ZERO; // Total HT Lignes au Prix de
  private BigDecimal e1to1g = BigDecimal.ZERO; // Tops de Traitement système
  private BigDecimal e1se1 = BigDecimal.ZERO; // Base Escompte 1
  private BigDecimal e1se2 = BigDecimal.ZERO; // Base Escompte 2
  private BigDecimal e1se3 = BigDecimal.ZERO; // Base Escompte 3
  private BigDecimal e1aff = BigDecimal.ZERO; // Date affectation
  private BigDecimal e1arr = BigDecimal.ZERO; // Arrondi TTC en centimes
  private BigDecimal e1tdv = BigDecimal.ZERO; // Top Devis 0,1 ou 2
  private BigDecimal e1mci = BigDecimal.ZERO; // Montant commande initial
  private BigDecimal e1nfa = BigDecimal.ZERO; // Numéro de facture
  private BigDecimal e1clcg = BigDecimal.ZERO; // Code Client Comptable
  private BigDecimal e1gba = BigDecimal.ZERO; // Génération bon achat auto.
  private BigDecimal e1dco = BigDecimal.ZERO; // Date de commande
  private BigDecimal e1dlp = BigDecimal.ZERO; // Date de livraison prévue
  private BigDecimal e1dls = BigDecimal.ZERO; // Date de livraison souhaitée
  private String e1in10 = ""; //
  private BigDecimal e1cc1 = BigDecimal.ZERO; // N° Client centrale
  private BigDecimal e1esc = BigDecimal.ZERO; // % Escompte
  private BigDecimal e1tv1 = BigDecimal.ZERO; // Taux TVA 1
  private BigDecimal e1tv2 = BigDecimal.ZERO; // Taux TVA 2
  private BigDecimal e1tv3 = BigDecimal.ZERO; // Taux TVA 3
  private BigDecimal e1ttp = BigDecimal.ZERO; // Taux Taxe Parafiscale
  private BigDecimal e1comg = BigDecimal.ZERO; // % Commissionnement Représen-
  private BigDecimal e1co2g = BigDecimal.ZERO; // % Commissionnement Représen-
  private BigDecimal e1clfp = BigDecimal.ZERO; // Code Client Facturé
  private BigDecimal e1clfs = BigDecimal.ZERO; // Code Client Facturé suffixe
  private String e1tfa = ""; // Code Type de Facturation
  private BigDecimal e1dpr = BigDecimal.ZERO; // Délai de préparation
  private BigDecimal e1col = BigDecimal.ZERO; // Nbre de colis
  private BigDecimal e1pds = BigDecimal.ZERO; // Poids en kg
  private BigDecimal e1bas = BigDecimal.ZERO; // Base devise
  private BigDecimal e1chg = BigDecimal.ZERO; // Taux de change
  private BigDecimal e1rbc = BigDecimal.ZERO; // Réduction base commission.RP
  private BigDecimal e1rbc2 = BigDecimal.ZERO; // Réduction base comm.REP2
  private BigDecimal e1dat1 = BigDecimal.ZERO; // Date mini de facturat°
  private BigDecimal e1dat2 = BigDecimal.ZERO; // Date non utilisée
  private BigDecimal e1dat3 = BigDecimal.ZERO; // Non utilisé
  private BigDecimal e1dat4 = BigDecimal.ZERO; // Date dernier traitement dev.
  private BigDecimal e1rem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal e1rem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal e1rem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal e1rem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal e1rem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal e1rem6 = BigDecimal.ZERO; // % Remise 6
  private BigDecimal e1rp1 = BigDecimal.ZERO; // % Remise 1 en Pied
  private BigDecimal e1rp2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal e1rp3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal e1rp4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal e1rp5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal e1rp6 = BigDecimal.ZERO; // % Remise 6
  private BigDecimal e1hre = BigDecimal.ZERO; // Heure de vente
  private BigDecimal e1nto = BigDecimal.ZERO; // N°de Tournée
  private BigDecimal e1oto = BigDecimal.ZERO; // Ordre dans la Tournée
  private BigDecimal e1vol = BigDecimal.ZERO; // Volume
  private BigDecimal e1lgm = BigDecimal.ZERO; // Longueur maxi
  private BigDecimal e1m2p = BigDecimal.ZERO; // M² plancher
  private BigDecimal e1dat5 = BigDecimal.ZERO; //
  private BigDecimal e1dat6 = BigDecimal.ZERO; //
  private BigDecimal e1dat7 = BigDecimal.ZERO; //
  private String e1ncc = ""; // Référence Commande Client
  private String e1rep = ""; // Code Représentant
  private String e1rep2 = ""; // Code Représentant sup.
  private String e1mag = ""; // Code Magasin de Sortie
  private String e1maga = ""; // Code Mag. de Sortie (Ancien)
  private String e1mex = ""; // Mode d'expédition
  private String e1ctr = ""; // Code transporteur
  private String e1san = ""; // Section analytique
  private String e1act = ""; // Activité ou affaire
  private String e1dev = ""; // Code devise
  private String e1cnv = ""; // Code condition de Vente
  private String e1rcc = ""; // Reference commande n°2
  private String e1vde = ""; // Code vendeur
  private String e1trc = ""; // Transport à calculer
  private String e1ztr = ""; // Zone transport
  private String e1ebp = ""; // Top edt. bon de préparation
  private String e1trp = ""; // Type de commissionnement Rep
  private String e1nho = ""; // Top Bon non confirmable
  private String e1tp1 = ""; // Top personnalisé n°1
  private String e1tp2 = ""; // Top personnalisé n°2
  private String e1tp3 = ""; // Top personnalisé n°3
  private String e1tp4 = ""; // Top personnalisé n°4
  private String e1tp5 = ""; // Top personnalisé n°5
  private String e1veh = ""; // Immatriculation Véhicule
  private String e1in1 = ""; // Ind. Ne pas facturer
  private String e1in2 = ""; // Indicateur tournée IN2=0 bon non sur tour
  private String e1in3 = ""; // Ind. Traite édité
  private String e1in4 = ""; // Ind. remises spéciales
  private String e1in5 = ""; // Ind. remise de pied existe
  private String e1in6 = ""; // Ind. Facture Comptoir
  private String e1in7 = ""; // Code Sélec./Préparation Cde
  private String e1in8 = ""; // top edt brd transporteur
  private String e1in9 = ""; // Blocage expédition
  private String e1cct = ""; // Code Contrat
  private String e1trl = ""; // Type remise ligne, 1=cascade
  private String e1brl = ""; // Base remise ligne, 1=Montant
  private String e1tre = ""; // Type remise/Pied, 1=cascade
  private String e1fil2 = ""; // E1IN10 / PGVMEBCDS2
  private String e1in11 = ""; // Poids/Colis/Volume saisis
  private String e1in12 = ""; // Extraction forcée à expédit°
  private String e1in13 = ""; // Non utilisé
  private String e1in14 = ""; // Non utilisé
  private String e1in15 = ""; // Non utilisé
  private String e1in16 = ""; // Non utilisé
  private String e1can = ""; // Canal de Vente
  private String e1crt = ""; // Code Regroupemt Transport
  private String e1pre = ""; // Code préparateur
  private String e1pfc = ""; // Plate-forme de chargement
  private String e1in17 = ""; // Type de vente
  private String e1in18 = ""; // N.U
  private String e1in19 = ""; // N.U
  private String e1in20 = ""; // N.U
  private String e1in21 = ""; // N.U
  private String e1in22 = ""; // N.U
  private String e1nat = ""; // Nature Bon ("T" = TTC)
  private String poenv = ""; // N.U.
  private String podir = ""; // Direct usine = 1
  private String poimd = ""; // Génération immédiate = 1
  private BigDecimal pochan = BigDecimal.ZERO; // N°Chantier
  private BigDecimal pocol = BigDecimal.ZERO; // Collectif fournisseur
  private BigDecimal pofrs = BigDecimal.ZERO; // Numéro fournisseur
  private BigDecimal pofre = BigDecimal.ZERO; // Extension adresse
  private BigDecimal poinex = BigDecimal.ZERO; // Indicateur Extraction
  private String poenlv = ""; // Pris par
  private BigDecimal poban = BigDecimal.ZERO; // Numéro bon d"achat
  private BigDecimal pobas = BigDecimal.ZERO; // Suffixe bon d"achat
  private BigDecimal pobae = BigDecimal.ZERO; // Etat bon d"achat 4=Récept.
  private BigDecimal pobon = BigDecimal.ZERO; // Numéro bon d"origine
  private BigDecimal pobos = BigDecimal.ZERO; // Suffixe bon d"origine
  private BigDecimal e1cllp = BigDecimal.ZERO; // Code Client Livré
  private BigDecimal e1clls = BigDecimal.ZERO; // Code Client Livré suffixe
  private BigDecimal podat3 = BigDecimal.ZERO; // Date relance devis
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_E1CRE, DECIMAL_E1CRE), // Date Création
      new AS400ZonedDecimal(SIZE_E1HOM, DECIMAL_E1HOM), // Date Homologation
      new AS400ZonedDecimal(SIZE_E1EXP, DECIMAL_E1EXP), // Date Expédition
      new AS400ZonedDecimal(SIZE_E1FAC, DECIMAL_E1FAC), // Date Facturation
      new AS400ZonedDecimal(SIZE_E1NFA6, DECIMAL_E1NFA6), // Numéro de facture
      new AS400ZonedDecimal(SIZE_E1NLIG, DECIMAL_E1NLIG), // Groupe Nombre de Lignes
      new AS400ZonedDecimal(SIZE_E1ETA, DECIMAL_E1ETA), // Etat du bon
      new AS400ZonedDecimal(SIZE_E1EDT, DECIMAL_E1EDT), // Top édition
      new AS400ZonedDecimal(SIZE_E1TCL, DECIMAL_E1TCL), // Top client livré
      new AS400ZonedDecimal(SIZE_E1TCF, DECIMAL_E1TCF), // Top client facturé
      new AS400ZonedDecimal(SIZE_E1TVA1, DECIMAL_E1TVA1), // N° TVA 1
      new AS400ZonedDecimal(SIZE_E1TVA2, DECIMAL_E1TVA2), // N° TVA 2
      new AS400ZonedDecimal(SIZE_E1TVA3, DECIMAL_E1TVA3), // N° TVA 3
      new AS400ZonedDecimal(SIZE_E1TPF, DECIMAL_E1TPF), // Top Taxe parafiscale
      new AS400ZonedDecimal(SIZE_E1RLV, DECIMAL_E1RLV), // Top Relevé (Rang du RGL/RLV)
      new AS400ZonedDecimal(SIZE_E1NEX, DECIMAL_E1NEX), // Nbre d'exemplaires factures
      new AS400ZonedDecimal(SIZE_E1TAR, DECIMAL_E1TAR), // N° de tarif
      new AS400ZonedDecimal(SIZE_E1RBF, DECIMAL_E1RBF), // Top regroup.bon/facture
      new AS400ZonedDecimal(SIZE_E1SGN, DECIMAL_E1SGN), // SIGNE DU BON
      new AS400ZonedDecimal(SIZE_E1SL1, DECIMAL_E1SL1), // Soumis T.V.A 1
      new AS400ZonedDecimal(SIZE_E1SL2, DECIMAL_E1SL2), // Soumis T.V.A 2
      new AS400ZonedDecimal(SIZE_E1SL3, DECIMAL_E1SL3), // Soumis T.V.A 3
      new AS400ZonedDecimal(SIZE_E1THTL, DECIMAL_E1THTL), // Total Hors Taxes Lignes
      new AS400ZonedDecimal(SIZE_E1TL1, DECIMAL_E1TL1), // Montant TVA 1
      new AS400ZonedDecimal(SIZE_E1TL2, DECIMAL_E1TL2), // Montant TVA 2
      new AS400ZonedDecimal(SIZE_E1TL3, DECIMAL_E1TL3), // Montant TVA 3
      new AS400ZonedDecimal(SIZE_E1STP, DECIMAL_E1STP), // Soumis à Taxe Parafiscale
      new AS400ZonedDecimal(SIZE_E1MTP, DECIMAL_E1MTP), // Montant Taxe Parafiscale
      new AS400ZonedDecimal(SIZE_E1MES, DECIMAL_E1MES), // Montant Escompte
      new AS400ZonedDecimal(SIZE_E1TTC, DECIMAL_E1TTC), // Total TTC
      new AS400ZonedDecimal(SIZE_E1TPVL, DECIMAL_E1TPVL), // Total HT Lignes au Prix de
      new AS400ZonedDecimal(SIZE_E1TPRL, DECIMAL_E1TPRL), // Total HT Lignes au Prix de
      new AS400ZonedDecimal(SIZE_E1TO1G, DECIMAL_E1TO1G), // Tops de Traitement système
      new AS400ZonedDecimal(SIZE_E1SE1, DECIMAL_E1SE1), // Base Escompte 1
      new AS400ZonedDecimal(SIZE_E1SE2, DECIMAL_E1SE2), // Base Escompte 2
      new AS400ZonedDecimal(SIZE_E1SE3, DECIMAL_E1SE3), // Base Escompte 3
      new AS400ZonedDecimal(SIZE_E1AFF, DECIMAL_E1AFF), // Date affectation
      new AS400ZonedDecimal(SIZE_E1ARR, DECIMAL_E1ARR), // Arrondi TTC en centimes
      new AS400ZonedDecimal(SIZE_E1TDV, DECIMAL_E1TDV), // Top Devis 0,1 ou 2
      new AS400ZonedDecimal(SIZE_E1MCI, DECIMAL_E1MCI), // Montant commande initial
      new AS400ZonedDecimal(SIZE_E1NFA, DECIMAL_E1NFA), // Numéro de facture
      new AS400ZonedDecimal(SIZE_E1CLCG, DECIMAL_E1CLCG), // Code Client Comptable
      new AS400ZonedDecimal(SIZE_E1GBA, DECIMAL_E1GBA), // Génération bon achat auto.
      new AS400ZonedDecimal(SIZE_E1DCO, DECIMAL_E1DCO), // Date de commande
      new AS400ZonedDecimal(SIZE_E1DLP, DECIMAL_E1DLP), // Date de livraison prévue
      new AS400ZonedDecimal(SIZE_E1DLS, DECIMAL_E1DLS), // Date de livraison souhaitée
      new AS400Text(SIZE_E1IN10), //
      new AS400ZonedDecimal(SIZE_E1CC1, DECIMAL_E1CC1), // N° Client centrale
      new AS400ZonedDecimal(SIZE_E1ESC, DECIMAL_E1ESC), // % Escompte
      new AS400ZonedDecimal(SIZE_E1TV1, DECIMAL_E1TV1), // Taux TVA 1
      new AS400ZonedDecimal(SIZE_E1TV2, DECIMAL_E1TV2), // Taux TVA 2
      new AS400ZonedDecimal(SIZE_E1TV3, DECIMAL_E1TV3), // Taux TVA 3
      new AS400ZonedDecimal(SIZE_E1TTP, DECIMAL_E1TTP), // Taux Taxe Parafiscale
      new AS400ZonedDecimal(SIZE_E1COMG, DECIMAL_E1COMG), // % Commissionnement Représen-
      new AS400ZonedDecimal(SIZE_E1CO2G, DECIMAL_E1CO2G), // % Commissionnement Représen-
      new AS400ZonedDecimal(SIZE_E1CLFP, DECIMAL_E1CLFP), // Code Client Facturé
      new AS400ZonedDecimal(SIZE_E1CLFS, DECIMAL_E1CLFS), // Code Client Facturé suffixe
      new AS400Text(SIZE_E1TFA), // Code Type de Facturation
      new AS400ZonedDecimal(SIZE_E1DPR, DECIMAL_E1DPR), // Délai de préparation
      new AS400ZonedDecimal(SIZE_E1COL, DECIMAL_E1COL), // Nbre de colis
      new AS400ZonedDecimal(SIZE_E1PDS, DECIMAL_E1PDS), // Poids en kg
      new AS400ZonedDecimal(SIZE_E1BAS, DECIMAL_E1BAS), // Base devise
      new AS400ZonedDecimal(SIZE_E1CHG, DECIMAL_E1CHG), // Taux de change
      new AS400ZonedDecimal(SIZE_E1RBC, DECIMAL_E1RBC), // Réduction base commission.RP
      new AS400ZonedDecimal(SIZE_E1RBC2, DECIMAL_E1RBC2), // Réduction base comm.REP2
      new AS400ZonedDecimal(SIZE_E1DAT1, DECIMAL_E1DAT1), // Date mini de facturat°
      new AS400ZonedDecimal(SIZE_E1DAT2, DECIMAL_E1DAT2), // Date non utilisée
      new AS400ZonedDecimal(SIZE_E1DAT3, DECIMAL_E1DAT3), // Non utilisé
      new AS400ZonedDecimal(SIZE_E1DAT4, DECIMAL_E1DAT4), // Date dernier traitement dev.
      new AS400ZonedDecimal(SIZE_E1REM1, DECIMAL_E1REM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_E1REM2, DECIMAL_E1REM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_E1REM3, DECIMAL_E1REM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_E1REM4, DECIMAL_E1REM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_E1REM5, DECIMAL_E1REM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_E1REM6, DECIMAL_E1REM6), // % Remise 6
      new AS400ZonedDecimal(SIZE_E1RP1, DECIMAL_E1RP1), // % Remise 1 en Pied
      new AS400ZonedDecimal(SIZE_E1RP2, DECIMAL_E1RP2), // % Remise 2
      new AS400ZonedDecimal(SIZE_E1RP3, DECIMAL_E1RP3), // % Remise 3
      new AS400ZonedDecimal(SIZE_E1RP4, DECIMAL_E1RP4), // % Remise 4
      new AS400ZonedDecimal(SIZE_E1RP5, DECIMAL_E1RP5), // % Remise 5
      new AS400ZonedDecimal(SIZE_E1RP6, DECIMAL_E1RP6), // % Remise 6
      new AS400ZonedDecimal(SIZE_E1HRE, DECIMAL_E1HRE), // Heure de vente
      new AS400ZonedDecimal(SIZE_E1NTO, DECIMAL_E1NTO), // N°de Tournée
      new AS400ZonedDecimal(SIZE_E1OTO, DECIMAL_E1OTO), // Ordre dans la Tournée
      new AS400ZonedDecimal(SIZE_E1VOL, DECIMAL_E1VOL), // Volume
      new AS400ZonedDecimal(SIZE_E1LGM, DECIMAL_E1LGM), // Longueur maxi
      new AS400ZonedDecimal(SIZE_E1M2P, DECIMAL_E1M2P), // M² plancher
      new AS400ZonedDecimal(SIZE_E1DAT5, DECIMAL_E1DAT5), //
      new AS400ZonedDecimal(SIZE_E1DAT6, DECIMAL_E1DAT6), //
      new AS400ZonedDecimal(SIZE_E1DAT7, DECIMAL_E1DAT7), //
      new AS400Text(SIZE_E1NCC), // Référence Commande Client
      new AS400Text(SIZE_E1REP), // Code Représentant
      new AS400Text(SIZE_E1REP2), // Code Représentant sup.
      new AS400Text(SIZE_E1MAG), // Code Magasin de Sortie
      new AS400Text(SIZE_E1MAGA), // Code Mag. de Sortie (Ancien)
      new AS400Text(SIZE_E1MEX), // Mode d'expédition
      new AS400Text(SIZE_E1CTR), // Code transporteur
      new AS400Text(SIZE_E1SAN), // Section analytique
      new AS400Text(SIZE_E1ACT), // Activité ou affaire
      new AS400Text(SIZE_E1DEV), // Code devise
      new AS400Text(SIZE_E1CNV), // Code condition de Vente
      new AS400Text(SIZE_E1RCC), // Reference commande n°2
      new AS400Text(SIZE_E1VDE), // Code vendeur
      new AS400Text(SIZE_E1TRC), // Transport à calculer
      new AS400Text(SIZE_E1ZTR), // Zone transport
      new AS400Text(SIZE_E1EBP), // Top edt. bon de préparation
      new AS400Text(SIZE_E1TRP), // Type de commissionnement Rep
      new AS400Text(SIZE_E1NHO), // Top Bon non confirmable
      new AS400Text(SIZE_E1TP1), // Top personnalisé n°1
      new AS400Text(SIZE_E1TP2), // Top personnalisé n°2
      new AS400Text(SIZE_E1TP3), // Top personnalisé n°3
      new AS400Text(SIZE_E1TP4), // Top personnalisé n°4
      new AS400Text(SIZE_E1TP5), // Top personnalisé n°5
      new AS400Text(SIZE_E1VEH), // Immatriculation Véhicule
      new AS400Text(SIZE_E1IN1), // Ind. Ne pas facturer
      new AS400Text(SIZE_E1IN2), // Indicateur tournée IN2=0 bon non sur tour
      new AS400Text(SIZE_E1IN3), // Ind. Traite édité
      new AS400Text(SIZE_E1IN4), // Ind. remises spéciales
      new AS400Text(SIZE_E1IN5), // Ind. remise de pied existe
      new AS400Text(SIZE_E1IN6), // Ind. Facture Comptoir
      new AS400Text(SIZE_E1IN7), // Code Sélec./Préparation Cde
      new AS400Text(SIZE_E1IN8), // top edt brd transporteur
      new AS400Text(SIZE_E1IN9), // Blocage expédition
      new AS400Text(SIZE_E1CCT), // Code Contrat
      new AS400Text(SIZE_E1TRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_E1BRL), // Base remise ligne, 1=Montant
      new AS400Text(SIZE_E1TRE), // Type remise/Pied, 1=cascade
      new AS400Text(SIZE_E1FIL2), // E1IN10 / PGVMEBCDS2
      new AS400Text(SIZE_E1IN11), // Poids/Colis/Volume saisis
      new AS400Text(SIZE_E1IN12), // Extraction forcée à expédit°
      new AS400Text(SIZE_E1IN13), // Non utilisé
      new AS400Text(SIZE_E1IN14), // Non utilisé
      new AS400Text(SIZE_E1IN15), // Non utilisé
      new AS400Text(SIZE_E1IN16), // Non utilisé
      new AS400Text(SIZE_E1CAN), // Canal de Vente
      new AS400Text(SIZE_E1CRT), // Code Regroupemt Transport
      new AS400Text(SIZE_E1PRE), // Code préparateur
      new AS400Text(SIZE_E1PFC), // Plate-forme de chargement
      new AS400Text(SIZE_E1IN17), // Type de vente
      new AS400Text(SIZE_E1IN18), // N.U
      new AS400Text(SIZE_E1IN19), // N.U
      new AS400Text(SIZE_E1IN20), // N.U
      new AS400Text(SIZE_E1IN21), // N.U
      new AS400Text(SIZE_E1IN22), // N.U
      new AS400Text(SIZE_E1NAT), // Nature Bon ("T" = TTC)
      new AS400Text(SIZE_POENV), // N.U.
      new AS400Text(SIZE_PODIR), // Direct usine = 1
      new AS400Text(SIZE_POIMD), // Génération immédiate = 1
      new AS400ZonedDecimal(SIZE_POCHAN, DECIMAL_POCHAN), // N°Chantier
      new AS400ZonedDecimal(SIZE_POCOL, DECIMAL_POCOL), // Collectif fournisseur
      new AS400ZonedDecimal(SIZE_POFRS, DECIMAL_POFRS), // Numéro fournisseur
      new AS400ZonedDecimal(SIZE_POFRE, DECIMAL_POFRE), // Extension adresse
      new AS400ZonedDecimal(SIZE_POINEX, DECIMAL_POINEX), // Indicateur Extraction
      new AS400Text(SIZE_POENLV), // Pris par
      new AS400ZonedDecimal(SIZE_POBAN, DECIMAL_POBAN), // Numéro bon d"achat
      new AS400ZonedDecimal(SIZE_POBAS, DECIMAL_POBAS), // Suffixe bon d"achat
      new AS400ZonedDecimal(SIZE_POBAE, DECIMAL_POBAE), // Etat bon d"achat 4=Récept.
      new AS400ZonedDecimal(SIZE_POBON, DECIMAL_POBON), // Numéro bon d"origine
      new AS400ZonedDecimal(SIZE_POBOS, DECIMAL_POBOS), // Suffixe bon d"origine
      new AS400ZonedDecimal(SIZE_E1CLLP, DECIMAL_E1CLLP), // Code Client Livré
      new AS400ZonedDecimal(SIZE_E1CLLS, DECIMAL_E1CLLS), // Code Client Livré suffixe
      new AS400ZonedDecimal(SIZE_PODAT3, DECIMAL_PODAT3), // Date relance devis
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { poind, e1cre, e1hom, e1exp, e1fac, e1nfa6, e1nlig, e1eta, e1edt, e1tcl, e1tcf, e1tva1, e1tva2, e1tva3, e1tpf, e1rlv,
          e1nex, e1tar, e1rbf, e1sgn, e1sl1, e1sl2, e1sl3, e1thtl, e1tl1, e1tl2, e1tl3, e1stp, e1mtp, e1mes, e1ttc, e1tpvl, e1tprl,
          e1to1g, e1se1, e1se2, e1se3, e1aff, e1arr, e1tdv, e1mci, e1nfa, e1clcg, e1gba, e1dco, e1dlp, e1dls, e1in10, e1cc1, e1esc, e1tv1,
          e1tv2, e1tv3, e1ttp, e1comg, e1co2g, e1clfp, e1clfs, e1tfa, e1dpr, e1col, e1pds, e1bas, e1chg, e1rbc, e1rbc2, e1dat1, e1dat2,
          e1dat3, e1dat4, e1rem1, e1rem2, e1rem3, e1rem4, e1rem5, e1rem6, e1rp1, e1rp2, e1rp3, e1rp4, e1rp5, e1rp6, e1hre, e1nto, e1oto,
          e1vol, e1lgm, e1m2p, e1dat5, e1dat6, e1dat7, e1ncc, e1rep, e1rep2, e1mag, e1maga, e1mex, e1ctr, e1san, e1act, e1dev, e1cnv,
          e1rcc, e1vde, e1trc, e1ztr, e1ebp, e1trp, e1nho, e1tp1, e1tp2, e1tp3, e1tp4, e1tp5, e1veh, e1in1, e1in2, e1in3, e1in4, e1in5,
          e1in6, e1in7, e1in8, e1in9, e1cct, e1trl, e1brl, e1tre, e1fil2, e1in11, e1in12, e1in13, e1in14, e1in15, e1in16, e1can, e1crt,
          e1pre, e1pfc, e1in17, e1in18, e1in19, e1in20, e1in21, e1in22, e1nat, poenv, podir, poimd, pochan, pocol, pofrs, pofre, poinex,
          poenlv, poban, pobas, pobae, pobon, pobos, e1cllp, e1clls, podat3, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    e1cre = (BigDecimal) output[1];
    e1hom = (BigDecimal) output[2];
    e1exp = (BigDecimal) output[3];
    e1fac = (BigDecimal) output[4];
    e1nfa6 = (BigDecimal) output[5];
    e1nlig = (BigDecimal) output[6];
    e1eta = (BigDecimal) output[7];
    e1edt = (BigDecimal) output[8];
    e1tcl = (BigDecimal) output[9];
    e1tcf = (BigDecimal) output[10];
    e1tva1 = (BigDecimal) output[11];
    e1tva2 = (BigDecimal) output[12];
    e1tva3 = (BigDecimal) output[13];
    e1tpf = (BigDecimal) output[14];
    e1rlv = (BigDecimal) output[15];
    e1nex = (BigDecimal) output[16];
    e1tar = (BigDecimal) output[17];
    e1rbf = (BigDecimal) output[18];
    e1sgn = (BigDecimal) output[19];
    e1sl1 = (BigDecimal) output[20];
    e1sl2 = (BigDecimal) output[21];
    e1sl3 = (BigDecimal) output[22];
    e1thtl = (BigDecimal) output[23];
    e1tl1 = (BigDecimal) output[24];
    e1tl2 = (BigDecimal) output[25];
    e1tl3 = (BigDecimal) output[26];
    e1stp = (BigDecimal) output[27];
    e1mtp = (BigDecimal) output[28];
    e1mes = (BigDecimal) output[29];
    e1ttc = (BigDecimal) output[30];
    e1tpvl = (BigDecimal) output[31];
    e1tprl = (BigDecimal) output[32];
    e1to1g = (BigDecimal) output[33];
    e1se1 = (BigDecimal) output[34];
    e1se2 = (BigDecimal) output[35];
    e1se3 = (BigDecimal) output[36];
    e1aff = (BigDecimal) output[37];
    e1arr = (BigDecimal) output[38];
    e1tdv = (BigDecimal) output[39];
    e1mci = (BigDecimal) output[40];
    e1nfa = (BigDecimal) output[41];
    e1clcg = (BigDecimal) output[42];
    e1gba = (BigDecimal) output[43];
    e1dco = (BigDecimal) output[44];
    e1dlp = (BigDecimal) output[45];
    e1dls = (BigDecimal) output[46];
    e1in10 = (String) output[47];
    e1cc1 = (BigDecimal) output[48];
    e1esc = (BigDecimal) output[49];
    e1tv1 = (BigDecimal) output[50];
    e1tv2 = (BigDecimal) output[51];
    e1tv3 = (BigDecimal) output[52];
    e1ttp = (BigDecimal) output[53];
    e1comg = (BigDecimal) output[54];
    e1co2g = (BigDecimal) output[55];
    e1clfp = (BigDecimal) output[56];
    e1clfs = (BigDecimal) output[57];
    e1tfa = (String) output[58];
    e1dpr = (BigDecimal) output[59];
    e1col = (BigDecimal) output[60];
    e1pds = (BigDecimal) output[61];
    e1bas = (BigDecimal) output[62];
    e1chg = (BigDecimal) output[63];
    e1rbc = (BigDecimal) output[64];
    e1rbc2 = (BigDecimal) output[65];
    e1dat1 = (BigDecimal) output[66];
    e1dat2 = (BigDecimal) output[67];
    e1dat3 = (BigDecimal) output[68];
    e1dat4 = (BigDecimal) output[69];
    e1rem1 = (BigDecimal) output[70];
    e1rem2 = (BigDecimal) output[71];
    e1rem3 = (BigDecimal) output[72];
    e1rem4 = (BigDecimal) output[73];
    e1rem5 = (BigDecimal) output[74];
    e1rem6 = (BigDecimal) output[75];
    e1rp1 = (BigDecimal) output[76];
    e1rp2 = (BigDecimal) output[77];
    e1rp3 = (BigDecimal) output[78];
    e1rp4 = (BigDecimal) output[79];
    e1rp5 = (BigDecimal) output[80];
    e1rp6 = (BigDecimal) output[81];
    e1hre = (BigDecimal) output[82];
    e1nto = (BigDecimal) output[83];
    e1oto = (BigDecimal) output[84];
    e1vol = (BigDecimal) output[85];
    e1lgm = (BigDecimal) output[86];
    e1m2p = (BigDecimal) output[87];
    e1dat5 = (BigDecimal) output[88];
    e1dat6 = (BigDecimal) output[89];
    e1dat7 = (BigDecimal) output[90];
    e1ncc = (String) output[91];
    e1rep = (String) output[92];
    e1rep2 = (String) output[93];
    e1mag = (String) output[94];
    e1maga = (String) output[95];
    e1mex = (String) output[96];
    e1ctr = (String) output[97];
    e1san = (String) output[98];
    e1act = (String) output[99];
    e1dev = (String) output[100];
    e1cnv = (String) output[101];
    e1rcc = (String) output[102];
    e1vde = (String) output[103];
    e1trc = (String) output[104];
    e1ztr = (String) output[105];
    e1ebp = (String) output[106];
    e1trp = (String) output[107];
    e1nho = (String) output[108];
    e1tp1 = (String) output[109];
    e1tp2 = (String) output[110];
    e1tp3 = (String) output[111];
    e1tp4 = (String) output[112];
    e1tp5 = (String) output[113];
    e1veh = (String) output[114];
    e1in1 = (String) output[115];
    e1in2 = (String) output[116];
    e1in3 = (String) output[117];
    e1in4 = (String) output[118];
    e1in5 = (String) output[119];
    e1in6 = (String) output[120];
    e1in7 = (String) output[121];
    e1in8 = (String) output[122];
    e1in9 = (String) output[123];
    e1cct = (String) output[124];
    e1trl = (String) output[125];
    e1brl = (String) output[126];
    e1tre = (String) output[127];
    e1fil2 = (String) output[128];
    e1in11 = (String) output[129];
    e1in12 = (String) output[130];
    e1in13 = (String) output[131];
    e1in14 = (String) output[132];
    e1in15 = (String) output[133];
    e1in16 = (String) output[134];
    e1can = (String) output[135];
    e1crt = (String) output[136];
    e1pre = (String) output[137];
    e1pfc = (String) output[138];
    e1in17 = (String) output[139];
    e1in18 = (String) output[140];
    e1in19 = (String) output[141];
    e1in20 = (String) output[142];
    e1in21 = (String) output[143];
    e1in22 = (String) output[144];
    e1nat = (String) output[145];
    poenv = (String) output[146];
    podir = (String) output[147];
    poimd = (String) output[148];
    pochan = (BigDecimal) output[149];
    pocol = (BigDecimal) output[150];
    pofrs = (BigDecimal) output[151];
    pofre = (BigDecimal) output[152];
    poinex = (BigDecimal) output[153];
    poenlv = (String) output[154];
    poban = (BigDecimal) output[155];
    pobas = (BigDecimal) output[156];
    pobae = (BigDecimal) output[157];
    pobon = (BigDecimal) output[158];
    pobos = (BigDecimal) output[159];
    e1cllp = (BigDecimal) output[160];
    e1clls = (BigDecimal) output[161];
    podat3 = (BigDecimal) output[162];
    poarr = (String) output[163];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setE1cre(BigDecimal pE1cre) {
    if (pE1cre == null) {
      return;
    }
    e1cre = pE1cre.setScale(DECIMAL_E1CRE, RoundingMode.HALF_UP);
  }
  
  public void setE1cre(Integer pE1cre) {
    if (pE1cre == null) {
      return;
    }
    e1cre = BigDecimal.valueOf(pE1cre);
  }
  
  public void setE1cre(Date pE1cre) {
    if (pE1cre == null) {
      return;
    }
    e1cre = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1cre));
  }
  
  public Integer getE1cre() {
    return e1cre.intValue();
  }
  
  public Date getE1creConvertiEnDate() {
    return ConvertDate.db2ToDate(e1cre.intValue(), null);
  }
  
  public void setE1hom(BigDecimal pE1hom) {
    if (pE1hom == null) {
      return;
    }
    e1hom = pE1hom.setScale(DECIMAL_E1HOM, RoundingMode.HALF_UP);
  }
  
  public void setE1hom(Integer pE1hom) {
    if (pE1hom == null) {
      return;
    }
    e1hom = BigDecimal.valueOf(pE1hom);
  }
  
  public void setE1hom(Date pE1hom) {
    if (pE1hom == null) {
      return;
    }
    e1hom = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1hom));
  }
  
  public Integer getE1hom() {
    return e1hom.intValue();
  }
  
  public Date getE1homConvertiEnDate() {
    return ConvertDate.db2ToDate(e1hom.intValue(), null);
  }
  
  public void setE1exp(BigDecimal pE1exp) {
    if (pE1exp == null) {
      return;
    }
    e1exp = pE1exp.setScale(DECIMAL_E1EXP, RoundingMode.HALF_UP);
  }
  
  public void setE1exp(Integer pE1exp) {
    if (pE1exp == null) {
      return;
    }
    e1exp = BigDecimal.valueOf(pE1exp);
  }
  
  public void setE1exp(Date pE1exp) {
    if (pE1exp == null) {
      return;
    }
    e1exp = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1exp));
  }
  
  public Integer getE1exp() {
    return e1exp.intValue();
  }
  
  public Date getE1expConvertiEnDate() {
    return ConvertDate.db2ToDate(e1exp.intValue(), null);
  }
  
  public void setE1fac(BigDecimal pE1fac) {
    if (pE1fac == null) {
      return;
    }
    e1fac = pE1fac.setScale(DECIMAL_E1FAC, RoundingMode.HALF_UP);
  }
  
  public void setE1fac(Integer pE1fac) {
    if (pE1fac == null) {
      return;
    }
    e1fac = BigDecimal.valueOf(pE1fac);
  }
  
  public void setE1fac(Date pE1fac) {
    if (pE1fac == null) {
      return;
    }
    e1fac = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1fac));
  }
  
  public Integer getE1fac() {
    return e1fac.intValue();
  }
  
  public Date getE1facConvertiEnDate() {
    return ConvertDate.db2ToDate(e1fac.intValue(), null);
  }
  
  public void setE1nfa6(BigDecimal pE1nfa6) {
    if (pE1nfa6 == null) {
      return;
    }
    e1nfa6 = pE1nfa6.setScale(DECIMAL_E1NFA6, RoundingMode.HALF_UP);
  }
  
  public void setE1nfa6(Integer pE1nfa6) {
    if (pE1nfa6 == null) {
      return;
    }
    e1nfa6 = BigDecimal.valueOf(pE1nfa6);
  }
  
  public Integer getE1nfa6() {
    return e1nfa6.intValue();
  }
  
  public void setE1nlig(BigDecimal pE1nlig) {
    if (pE1nlig == null) {
      return;
    }
    e1nlig = pE1nlig.setScale(DECIMAL_E1NLIG, RoundingMode.HALF_UP);
  }
  
  public void setE1nlig(Integer pE1nlig) {
    if (pE1nlig == null) {
      return;
    }
    e1nlig = BigDecimal.valueOf(pE1nlig);
  }
  
  public Integer getE1nlig() {
    return e1nlig.intValue();
  }
  
  public void setE1eta(BigDecimal pE1eta) {
    if (pE1eta == null) {
      return;
    }
    e1eta = pE1eta.setScale(DECIMAL_E1ETA, RoundingMode.HALF_UP);
  }
  
  public void setE1eta(Integer pE1eta) {
    if (pE1eta == null) {
      return;
    }
    e1eta = BigDecimal.valueOf(pE1eta);
  }
  
  public Integer getE1eta() {
    return e1eta.intValue();
  }
  
  public void setE1edt(BigDecimal pE1edt) {
    if (pE1edt == null) {
      return;
    }
    e1edt = pE1edt.setScale(DECIMAL_E1EDT, RoundingMode.HALF_UP);
  }
  
  public void setE1edt(Integer pE1edt) {
    if (pE1edt == null) {
      return;
    }
    e1edt = BigDecimal.valueOf(pE1edt);
  }
  
  public Integer getE1edt() {
    return e1edt.intValue();
  }
  
  public void setE1tcl(BigDecimal pE1tcl) {
    if (pE1tcl == null) {
      return;
    }
    e1tcl = pE1tcl.setScale(DECIMAL_E1TCL, RoundingMode.HALF_UP);
  }
  
  public void setE1tcl(Integer pE1tcl) {
    if (pE1tcl == null) {
      return;
    }
    e1tcl = BigDecimal.valueOf(pE1tcl);
  }
  
  public Integer getE1tcl() {
    return e1tcl.intValue();
  }
  
  public void setE1tcf(BigDecimal pE1tcf) {
    if (pE1tcf == null) {
      return;
    }
    e1tcf = pE1tcf.setScale(DECIMAL_E1TCF, RoundingMode.HALF_UP);
  }
  
  public void setE1tcf(Integer pE1tcf) {
    if (pE1tcf == null) {
      return;
    }
    e1tcf = BigDecimal.valueOf(pE1tcf);
  }
  
  public Integer getE1tcf() {
    return e1tcf.intValue();
  }
  
  public void setE1tva1(BigDecimal pE1tva1) {
    if (pE1tva1 == null) {
      return;
    }
    e1tva1 = pE1tva1.setScale(DECIMAL_E1TVA1, RoundingMode.HALF_UP);
  }
  
  public void setE1tva1(Integer pE1tva1) {
    if (pE1tva1 == null) {
      return;
    }
    e1tva1 = BigDecimal.valueOf(pE1tva1);
  }
  
  public Integer getE1tva1() {
    return e1tva1.intValue();
  }
  
  public void setE1tva2(BigDecimal pE1tva2) {
    if (pE1tva2 == null) {
      return;
    }
    e1tva2 = pE1tva2.setScale(DECIMAL_E1TVA2, RoundingMode.HALF_UP);
  }
  
  public void setE1tva2(Integer pE1tva2) {
    if (pE1tva2 == null) {
      return;
    }
    e1tva2 = BigDecimal.valueOf(pE1tva2);
  }
  
  public Integer getE1tva2() {
    return e1tva2.intValue();
  }
  
  public void setE1tva3(BigDecimal pE1tva3) {
    if (pE1tva3 == null) {
      return;
    }
    e1tva3 = pE1tva3.setScale(DECIMAL_E1TVA3, RoundingMode.HALF_UP);
  }
  
  public void setE1tva3(Integer pE1tva3) {
    if (pE1tva3 == null) {
      return;
    }
    e1tva3 = BigDecimal.valueOf(pE1tva3);
  }
  
  public Integer getE1tva3() {
    return e1tva3.intValue();
  }
  
  public void setE1tpf(BigDecimal pE1tpf) {
    if (pE1tpf == null) {
      return;
    }
    e1tpf = pE1tpf.setScale(DECIMAL_E1TPF, RoundingMode.HALF_UP);
  }
  
  public void setE1tpf(Integer pE1tpf) {
    if (pE1tpf == null) {
      return;
    }
    e1tpf = BigDecimal.valueOf(pE1tpf);
  }
  
  public Integer getE1tpf() {
    return e1tpf.intValue();
  }
  
  public void setE1rlv(BigDecimal pE1rlv) {
    if (pE1rlv == null) {
      return;
    }
    e1rlv = pE1rlv.setScale(DECIMAL_E1RLV, RoundingMode.HALF_UP);
  }
  
  public void setE1rlv(Integer pE1rlv) {
    if (pE1rlv == null) {
      return;
    }
    e1rlv = BigDecimal.valueOf(pE1rlv);
  }
  
  public Integer getE1rlv() {
    return e1rlv.intValue();
  }
  
  public void setE1nex(BigDecimal pE1nex) {
    if (pE1nex == null) {
      return;
    }
    e1nex = pE1nex.setScale(DECIMAL_E1NEX, RoundingMode.HALF_UP);
  }
  
  public void setE1nex(Integer pE1nex) {
    if (pE1nex == null) {
      return;
    }
    e1nex = BigDecimal.valueOf(pE1nex);
  }
  
  public Integer getE1nex() {
    return e1nex.intValue();
  }
  
  public void setE1tar(BigDecimal pE1tar) {
    if (pE1tar == null) {
      return;
    }
    e1tar = pE1tar.setScale(DECIMAL_E1TAR, RoundingMode.HALF_UP);
  }
  
  public void setE1tar(Integer pE1tar) {
    if (pE1tar == null) {
      return;
    }
    e1tar = BigDecimal.valueOf(pE1tar);
  }
  
  public Integer getE1tar() {
    return e1tar.intValue();
  }
  
  public void setE1rbf(BigDecimal pE1rbf) {
    if (pE1rbf == null) {
      return;
    }
    e1rbf = pE1rbf.setScale(DECIMAL_E1RBF, RoundingMode.HALF_UP);
  }
  
  public void setE1rbf(Integer pE1rbf) {
    if (pE1rbf == null) {
      return;
    }
    e1rbf = BigDecimal.valueOf(pE1rbf);
  }
  
  public Integer getE1rbf() {
    return e1rbf.intValue();
  }
  
  public void setE1sgn(BigDecimal pE1sgn) {
    if (pE1sgn == null) {
      return;
    }
    e1sgn = pE1sgn.setScale(DECIMAL_E1SGN, RoundingMode.HALF_UP);
  }
  
  public void setE1sgn(Integer pE1sgn) {
    if (pE1sgn == null) {
      return;
    }
    e1sgn = BigDecimal.valueOf(pE1sgn);
  }
  
  public Integer getE1sgn() {
    return e1sgn.intValue();
  }
  
  public void setE1sl1(BigDecimal pE1sl1) {
    if (pE1sl1 == null) {
      return;
    }
    e1sl1 = pE1sl1.setScale(DECIMAL_E1SL1, RoundingMode.HALF_UP);
  }
  
  public void setE1sl1(Double pE1sl1) {
    if (pE1sl1 == null) {
      return;
    }
    e1sl1 = BigDecimal.valueOf(pE1sl1).setScale(DECIMAL_E1SL1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1sl1() {
    return e1sl1.setScale(DECIMAL_E1SL1, RoundingMode.HALF_UP);
  }
  
  public void setE1sl2(BigDecimal pE1sl2) {
    if (pE1sl2 == null) {
      return;
    }
    e1sl2 = pE1sl2.setScale(DECIMAL_E1SL2, RoundingMode.HALF_UP);
  }
  
  public void setE1sl2(Double pE1sl2) {
    if (pE1sl2 == null) {
      return;
    }
    e1sl2 = BigDecimal.valueOf(pE1sl2).setScale(DECIMAL_E1SL2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1sl2() {
    return e1sl2.setScale(DECIMAL_E1SL2, RoundingMode.HALF_UP);
  }
  
  public void setE1sl3(BigDecimal pE1sl3) {
    if (pE1sl3 == null) {
      return;
    }
    e1sl3 = pE1sl3.setScale(DECIMAL_E1SL3, RoundingMode.HALF_UP);
  }
  
  public void setE1sl3(Double pE1sl3) {
    if (pE1sl3 == null) {
      return;
    }
    e1sl3 = BigDecimal.valueOf(pE1sl3).setScale(DECIMAL_E1SL3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1sl3() {
    return e1sl3.setScale(DECIMAL_E1SL3, RoundingMode.HALF_UP);
  }
  
  public void setE1thtl(BigDecimal pE1thtl) {
    if (pE1thtl == null) {
      return;
    }
    e1thtl = pE1thtl.setScale(DECIMAL_E1THTL, RoundingMode.HALF_UP);
  }
  
  public void setE1thtl(Double pE1thtl) {
    if (pE1thtl == null) {
      return;
    }
    e1thtl = BigDecimal.valueOf(pE1thtl).setScale(DECIMAL_E1THTL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1thtl() {
    return e1thtl.setScale(DECIMAL_E1THTL, RoundingMode.HALF_UP);
  }
  
  public void setE1tl1(BigDecimal pE1tl1) {
    if (pE1tl1 == null) {
      return;
    }
    e1tl1 = pE1tl1.setScale(DECIMAL_E1TL1, RoundingMode.HALF_UP);
  }
  
  public void setE1tl1(Double pE1tl1) {
    if (pE1tl1 == null) {
      return;
    }
    e1tl1 = BigDecimal.valueOf(pE1tl1).setScale(DECIMAL_E1TL1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1tl1() {
    return e1tl1.setScale(DECIMAL_E1TL1, RoundingMode.HALF_UP);
  }
  
  public void setE1tl2(BigDecimal pE1tl2) {
    if (pE1tl2 == null) {
      return;
    }
    e1tl2 = pE1tl2.setScale(DECIMAL_E1TL2, RoundingMode.HALF_UP);
  }
  
  public void setE1tl2(Double pE1tl2) {
    if (pE1tl2 == null) {
      return;
    }
    e1tl2 = BigDecimal.valueOf(pE1tl2).setScale(DECIMAL_E1TL2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1tl2() {
    return e1tl2.setScale(DECIMAL_E1TL2, RoundingMode.HALF_UP);
  }
  
  public void setE1tl3(BigDecimal pE1tl3) {
    if (pE1tl3 == null) {
      return;
    }
    e1tl3 = pE1tl3.setScale(DECIMAL_E1TL3, RoundingMode.HALF_UP);
  }
  
  public void setE1tl3(Double pE1tl3) {
    if (pE1tl3 == null) {
      return;
    }
    e1tl3 = BigDecimal.valueOf(pE1tl3).setScale(DECIMAL_E1TL3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1tl3() {
    return e1tl3.setScale(DECIMAL_E1TL3, RoundingMode.HALF_UP);
  }
  
  public void setE1stp(BigDecimal pE1stp) {
    if (pE1stp == null) {
      return;
    }
    e1stp = pE1stp.setScale(DECIMAL_E1STP, RoundingMode.HALF_UP);
  }
  
  public void setE1stp(Double pE1stp) {
    if (pE1stp == null) {
      return;
    }
    e1stp = BigDecimal.valueOf(pE1stp).setScale(DECIMAL_E1STP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1stp() {
    return e1stp.setScale(DECIMAL_E1STP, RoundingMode.HALF_UP);
  }
  
  public void setE1mtp(BigDecimal pE1mtp) {
    if (pE1mtp == null) {
      return;
    }
    e1mtp = pE1mtp.setScale(DECIMAL_E1MTP, RoundingMode.HALF_UP);
  }
  
  public void setE1mtp(Double pE1mtp) {
    if (pE1mtp == null) {
      return;
    }
    e1mtp = BigDecimal.valueOf(pE1mtp).setScale(DECIMAL_E1MTP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1mtp() {
    return e1mtp.setScale(DECIMAL_E1MTP, RoundingMode.HALF_UP);
  }
  
  public void setE1mes(BigDecimal pE1mes) {
    if (pE1mes == null) {
      return;
    }
    e1mes = pE1mes.setScale(DECIMAL_E1MES, RoundingMode.HALF_UP);
  }
  
  public void setE1mes(Double pE1mes) {
    if (pE1mes == null) {
      return;
    }
    e1mes = BigDecimal.valueOf(pE1mes).setScale(DECIMAL_E1MES, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1mes() {
    return e1mes.setScale(DECIMAL_E1MES, RoundingMode.HALF_UP);
  }
  
  public void setE1ttc(BigDecimal pE1ttc) {
    if (pE1ttc == null) {
      return;
    }
    e1ttc = pE1ttc.setScale(DECIMAL_E1TTC, RoundingMode.HALF_UP);
  }
  
  public void setE1ttc(Double pE1ttc) {
    if (pE1ttc == null) {
      return;
    }
    e1ttc = BigDecimal.valueOf(pE1ttc).setScale(DECIMAL_E1TTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1ttc() {
    return e1ttc.setScale(DECIMAL_E1TTC, RoundingMode.HALF_UP);
  }
  
  public void setE1tpvl(BigDecimal pE1tpvl) {
    if (pE1tpvl == null) {
      return;
    }
    e1tpvl = pE1tpvl.setScale(DECIMAL_E1TPVL, RoundingMode.HALF_UP);
  }
  
  public void setE1tpvl(Double pE1tpvl) {
    if (pE1tpvl == null) {
      return;
    }
    e1tpvl = BigDecimal.valueOf(pE1tpvl).setScale(DECIMAL_E1TPVL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1tpvl() {
    return e1tpvl.setScale(DECIMAL_E1TPVL, RoundingMode.HALF_UP);
  }
  
  public void setE1tprl(BigDecimal pE1tprl) {
    if (pE1tprl == null) {
      return;
    }
    e1tprl = pE1tprl.setScale(DECIMAL_E1TPRL, RoundingMode.HALF_UP);
  }
  
  public void setE1tprl(Double pE1tprl) {
    if (pE1tprl == null) {
      return;
    }
    e1tprl = BigDecimal.valueOf(pE1tprl).setScale(DECIMAL_E1TPRL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1tprl() {
    return e1tprl.setScale(DECIMAL_E1TPRL, RoundingMode.HALF_UP);
  }
  
  public void setE1to1g(BigDecimal pE1to1g) {
    if (pE1to1g == null) {
      return;
    }
    e1to1g = pE1to1g.setScale(DECIMAL_E1TO1G, RoundingMode.HALF_UP);
  }
  
  public void setE1to1g(Integer pE1to1g) {
    if (pE1to1g == null) {
      return;
    }
    e1to1g = BigDecimal.valueOf(pE1to1g);
  }
  
  public Integer getE1to1g() {
    return e1to1g.intValue();
  }
  
  public void setE1se1(BigDecimal pE1se1) {
    if (pE1se1 == null) {
      return;
    }
    e1se1 = pE1se1.setScale(DECIMAL_E1SE1, RoundingMode.HALF_UP);
  }
  
  public void setE1se1(Double pE1se1) {
    if (pE1se1 == null) {
      return;
    }
    e1se1 = BigDecimal.valueOf(pE1se1).setScale(DECIMAL_E1SE1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1se1() {
    return e1se1.setScale(DECIMAL_E1SE1, RoundingMode.HALF_UP);
  }
  
  public void setE1se2(BigDecimal pE1se2) {
    if (pE1se2 == null) {
      return;
    }
    e1se2 = pE1se2.setScale(DECIMAL_E1SE2, RoundingMode.HALF_UP);
  }
  
  public void setE1se2(Double pE1se2) {
    if (pE1se2 == null) {
      return;
    }
    e1se2 = BigDecimal.valueOf(pE1se2).setScale(DECIMAL_E1SE2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1se2() {
    return e1se2.setScale(DECIMAL_E1SE2, RoundingMode.HALF_UP);
  }
  
  public void setE1se3(BigDecimal pE1se3) {
    if (pE1se3 == null) {
      return;
    }
    e1se3 = pE1se3.setScale(DECIMAL_E1SE3, RoundingMode.HALF_UP);
  }
  
  public void setE1se3(Double pE1se3) {
    if (pE1se3 == null) {
      return;
    }
    e1se3 = BigDecimal.valueOf(pE1se3).setScale(DECIMAL_E1SE3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1se3() {
    return e1se3.setScale(DECIMAL_E1SE3, RoundingMode.HALF_UP);
  }
  
  public void setE1aff(BigDecimal pE1aff) {
    if (pE1aff == null) {
      return;
    }
    e1aff = pE1aff.setScale(DECIMAL_E1AFF, RoundingMode.HALF_UP);
  }
  
  public void setE1aff(Integer pE1aff) {
    if (pE1aff == null) {
      return;
    }
    e1aff = BigDecimal.valueOf(pE1aff);
  }
  
  public void setE1aff(Date pE1aff) {
    if (pE1aff == null) {
      return;
    }
    e1aff = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1aff));
  }
  
  public Integer getE1aff() {
    return e1aff.intValue();
  }
  
  public Date getE1affConvertiEnDate() {
    return ConvertDate.db2ToDate(e1aff.intValue(), null);
  }
  
  public void setE1arr(BigDecimal pE1arr) {
    if (pE1arr == null) {
      return;
    }
    e1arr = pE1arr.setScale(DECIMAL_E1ARR, RoundingMode.HALF_UP);
  }
  
  public void setE1arr(Integer pE1arr) {
    if (pE1arr == null) {
      return;
    }
    e1arr = BigDecimal.valueOf(pE1arr);
  }
  
  public Integer getE1arr() {
    return e1arr.intValue();
  }
  
  public void setE1tdv(BigDecimal pE1tdv) {
    if (pE1tdv == null) {
      return;
    }
    e1tdv = pE1tdv.setScale(DECIMAL_E1TDV, RoundingMode.HALF_UP);
  }
  
  public void setE1tdv(Integer pE1tdv) {
    if (pE1tdv == null) {
      return;
    }
    e1tdv = BigDecimal.valueOf(pE1tdv);
  }
  
  public Integer getE1tdv() {
    return e1tdv.intValue();
  }
  
  public void setE1mci(BigDecimal pE1mci) {
    if (pE1mci == null) {
      return;
    }
    e1mci = pE1mci.setScale(DECIMAL_E1MCI, RoundingMode.HALF_UP);
  }
  
  public void setE1mci(Double pE1mci) {
    if (pE1mci == null) {
      return;
    }
    e1mci = BigDecimal.valueOf(pE1mci).setScale(DECIMAL_E1MCI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1mci() {
    return e1mci.setScale(DECIMAL_E1MCI, RoundingMode.HALF_UP);
  }
  
  public void setE1nfa(BigDecimal pE1nfa) {
    if (pE1nfa == null) {
      return;
    }
    e1nfa = pE1nfa.setScale(DECIMAL_E1NFA, RoundingMode.HALF_UP);
  }
  
  public void setE1nfa(Integer pE1nfa) {
    if (pE1nfa == null) {
      return;
    }
    e1nfa = BigDecimal.valueOf(pE1nfa);
  }
  
  public void setE1nfa(Date pE1nfa) {
    if (pE1nfa == null) {
      return;
    }
    e1nfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1nfa));
  }
  
  public Integer getE1nfa() {
    return e1nfa.intValue();
  }
  
  public Date getE1nfaConvertiEnDate() {
    return ConvertDate.db2ToDate(e1nfa.intValue(), null);
  }
  
  public void setE1clcg(BigDecimal pE1clcg) {
    if (pE1clcg == null) {
      return;
    }
    e1clcg = pE1clcg.setScale(DECIMAL_E1CLCG, RoundingMode.HALF_UP);
  }
  
  public void setE1clcg(Integer pE1clcg) {
    if (pE1clcg == null) {
      return;
    }
    e1clcg = BigDecimal.valueOf(pE1clcg);
  }
  
  public void setE1clcg(Date pE1clcg) {
    if (pE1clcg == null) {
      return;
    }
    e1clcg = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1clcg));
  }
  
  public Integer getE1clcg() {
    return e1clcg.intValue();
  }
  
  public Date getE1clcgConvertiEnDate() {
    return ConvertDate.db2ToDate(e1clcg.intValue(), null);
  }
  
  public void setE1gba(BigDecimal pE1gba) {
    if (pE1gba == null) {
      return;
    }
    e1gba = pE1gba.setScale(DECIMAL_E1GBA, RoundingMode.HALF_UP);
  }
  
  public void setE1gba(Integer pE1gba) {
    if (pE1gba == null) {
      return;
    }
    e1gba = BigDecimal.valueOf(pE1gba);
  }
  
  public Integer getE1gba() {
    return e1gba.intValue();
  }
  
  public void setE1dco(BigDecimal pE1dco) {
    if (pE1dco == null) {
      return;
    }
    e1dco = pE1dco.setScale(DECIMAL_E1DCO, RoundingMode.HALF_UP);
  }
  
  public void setE1dco(Integer pE1dco) {
    if (pE1dco == null) {
      return;
    }
    e1dco = BigDecimal.valueOf(pE1dco);
  }
  
  public void setE1dco(Date pE1dco) {
    if (pE1dco == null) {
      return;
    }
    e1dco = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dco));
  }
  
  public Integer getE1dco() {
    return e1dco.intValue();
  }
  
  public Date getE1dcoConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dco.intValue(), null);
  }
  
  public void setE1dlp(BigDecimal pE1dlp) {
    if (pE1dlp == null) {
      return;
    }
    e1dlp = pE1dlp.setScale(DECIMAL_E1DLP, RoundingMode.HALF_UP);
  }
  
  public void setE1dlp(Integer pE1dlp) {
    if (pE1dlp == null) {
      return;
    }
    e1dlp = BigDecimal.valueOf(pE1dlp);
  }
  
  public void setE1dlp(Date pE1dlp) {
    if (pE1dlp == null) {
      return;
    }
    e1dlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dlp));
  }
  
  public Integer getE1dlp() {
    return e1dlp.intValue();
  }
  
  public Date getE1dlpConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dlp.intValue(), null);
  }
  
  public void setE1dls(BigDecimal pE1dls) {
    if (pE1dls == null) {
      return;
    }
    e1dls = pE1dls.setScale(DECIMAL_E1DLS, RoundingMode.HALF_UP);
  }
  
  public void setE1dls(Integer pE1dls) {
    if (pE1dls == null) {
      return;
    }
    e1dls = BigDecimal.valueOf(pE1dls);
  }
  
  public void setE1dls(Date pE1dls) {
    if (pE1dls == null) {
      return;
    }
    e1dls = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dls));
  }
  
  public Integer getE1dls() {
    return e1dls.intValue();
  }
  
  public Date getE1dlsConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dls.intValue(), null);
  }
  
  public void setE1in10(Character pE1in10) {
    if (pE1in10 == null) {
      return;
    }
    e1in10 = String.valueOf(pE1in10);
  }
  
  public Character getE1in10() {
    return e1in10.charAt(0);
  }
  
  public void setE1cc1(BigDecimal pE1cc1) {
    if (pE1cc1 == null) {
      return;
    }
    e1cc1 = pE1cc1.setScale(DECIMAL_E1CC1, RoundingMode.HALF_UP);
  }
  
  public void setE1cc1(Integer pE1cc1) {
    if (pE1cc1 == null) {
      return;
    }
    e1cc1 = BigDecimal.valueOf(pE1cc1);
  }
  
  public Integer getE1cc1() {
    return e1cc1.intValue();
  }
  
  public void setE1esc(BigDecimal pE1esc) {
    if (pE1esc == null) {
      return;
    }
    e1esc = pE1esc.setScale(DECIMAL_E1ESC, RoundingMode.HALF_UP);
  }
  
  public void setE1esc(Double pE1esc) {
    if (pE1esc == null) {
      return;
    }
    e1esc = BigDecimal.valueOf(pE1esc).setScale(DECIMAL_E1ESC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1esc() {
    return e1esc.setScale(DECIMAL_E1ESC, RoundingMode.HALF_UP);
  }
  
  public void setE1tv1(BigDecimal pE1tv1) {
    if (pE1tv1 == null) {
      return;
    }
    e1tv1 = pE1tv1.setScale(DECIMAL_E1TV1, RoundingMode.HALF_UP);
  }
  
  public void setE1tv1(Double pE1tv1) {
    if (pE1tv1 == null) {
      return;
    }
    e1tv1 = BigDecimal.valueOf(pE1tv1).setScale(DECIMAL_E1TV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1tv1() {
    return e1tv1.setScale(DECIMAL_E1TV1, RoundingMode.HALF_UP);
  }
  
  public void setE1tv2(BigDecimal pE1tv2) {
    if (pE1tv2 == null) {
      return;
    }
    e1tv2 = pE1tv2.setScale(DECIMAL_E1TV2, RoundingMode.HALF_UP);
  }
  
  public void setE1tv2(Double pE1tv2) {
    if (pE1tv2 == null) {
      return;
    }
    e1tv2 = BigDecimal.valueOf(pE1tv2).setScale(DECIMAL_E1TV2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1tv2() {
    return e1tv2.setScale(DECIMAL_E1TV2, RoundingMode.HALF_UP);
  }
  
  public void setE1tv3(BigDecimal pE1tv3) {
    if (pE1tv3 == null) {
      return;
    }
    e1tv3 = pE1tv3.setScale(DECIMAL_E1TV3, RoundingMode.HALF_UP);
  }
  
  public void setE1tv3(Double pE1tv3) {
    if (pE1tv3 == null) {
      return;
    }
    e1tv3 = BigDecimal.valueOf(pE1tv3).setScale(DECIMAL_E1TV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1tv3() {
    return e1tv3.setScale(DECIMAL_E1TV3, RoundingMode.HALF_UP);
  }
  
  public void setE1ttp(BigDecimal pE1ttp) {
    if (pE1ttp == null) {
      return;
    }
    e1ttp = pE1ttp.setScale(DECIMAL_E1TTP, RoundingMode.HALF_UP);
  }
  
  public void setE1ttp(Double pE1ttp) {
    if (pE1ttp == null) {
      return;
    }
    e1ttp = BigDecimal.valueOf(pE1ttp).setScale(DECIMAL_E1TTP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1ttp() {
    return e1ttp.setScale(DECIMAL_E1TTP, RoundingMode.HALF_UP);
  }
  
  public void setE1comg(BigDecimal pE1comg) {
    if (pE1comg == null) {
      return;
    }
    e1comg = pE1comg.setScale(DECIMAL_E1COMG, RoundingMode.HALF_UP);
  }
  
  public void setE1comg(Double pE1comg) {
    if (pE1comg == null) {
      return;
    }
    e1comg = BigDecimal.valueOf(pE1comg).setScale(DECIMAL_E1COMG, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1comg() {
    return e1comg.setScale(DECIMAL_E1COMG, RoundingMode.HALF_UP);
  }
  
  public void setE1co2g(BigDecimal pE1co2g) {
    if (pE1co2g == null) {
      return;
    }
    e1co2g = pE1co2g.setScale(DECIMAL_E1CO2G, RoundingMode.HALF_UP);
  }
  
  public void setE1co2g(Double pE1co2g) {
    if (pE1co2g == null) {
      return;
    }
    e1co2g = BigDecimal.valueOf(pE1co2g).setScale(DECIMAL_E1CO2G, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1co2g() {
    return e1co2g.setScale(DECIMAL_E1CO2G, RoundingMode.HALF_UP);
  }
  
  public void setE1clfp(BigDecimal pE1clfp) {
    if (pE1clfp == null) {
      return;
    }
    e1clfp = pE1clfp.setScale(DECIMAL_E1CLFP, RoundingMode.HALF_UP);
  }
  
  public void setE1clfp(Integer pE1clfp) {
    if (pE1clfp == null) {
      return;
    }
    e1clfp = BigDecimal.valueOf(pE1clfp);
  }
  
  public Integer getE1clfp() {
    return e1clfp.intValue();
  }
  
  public void setE1clfs(BigDecimal pE1clfs) {
    if (pE1clfs == null) {
      return;
    }
    e1clfs = pE1clfs.setScale(DECIMAL_E1CLFS, RoundingMode.HALF_UP);
  }
  
  public void setE1clfs(Integer pE1clfs) {
    if (pE1clfs == null) {
      return;
    }
    e1clfs = BigDecimal.valueOf(pE1clfs);
  }
  
  public Integer getE1clfs() {
    return e1clfs.intValue();
  }
  
  public void setE1tfa(Character pE1tfa) {
    if (pE1tfa == null) {
      return;
    }
    e1tfa = String.valueOf(pE1tfa);
  }
  
  public Character getE1tfa() {
    return e1tfa.charAt(0);
  }
  
  public void setE1dpr(BigDecimal pE1dpr) {
    if (pE1dpr == null) {
      return;
    }
    e1dpr = pE1dpr.setScale(DECIMAL_E1DPR, RoundingMode.HALF_UP);
  }
  
  public void setE1dpr(Integer pE1dpr) {
    if (pE1dpr == null) {
      return;
    }
    e1dpr = BigDecimal.valueOf(pE1dpr);
  }
  
  public Integer getE1dpr() {
    return e1dpr.intValue();
  }
  
  public void setE1col(BigDecimal pE1col) {
    if (pE1col == null) {
      return;
    }
    e1col = pE1col.setScale(DECIMAL_E1COL, RoundingMode.HALF_UP);
  }
  
  public void setE1col(Integer pE1col) {
    if (pE1col == null) {
      return;
    }
    e1col = BigDecimal.valueOf(pE1col);
  }
  
  public Integer getE1col() {
    return e1col.intValue();
  }
  
  public void setE1pds(BigDecimal pE1pds) {
    if (pE1pds == null) {
      return;
    }
    e1pds = pE1pds.setScale(DECIMAL_E1PDS, RoundingMode.HALF_UP);
  }
  
  public void setE1pds(Double pE1pds) {
    if (pE1pds == null) {
      return;
    }
    e1pds = BigDecimal.valueOf(pE1pds).setScale(DECIMAL_E1PDS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1pds() {
    return e1pds.setScale(DECIMAL_E1PDS, RoundingMode.HALF_UP);
  }
  
  public void setE1bas(BigDecimal pE1bas) {
    if (pE1bas == null) {
      return;
    }
    e1bas = pE1bas.setScale(DECIMAL_E1BAS, RoundingMode.HALF_UP);
  }
  
  public void setE1bas(Integer pE1bas) {
    if (pE1bas == null) {
      return;
    }
    e1bas = BigDecimal.valueOf(pE1bas);
  }
  
  public Integer getE1bas() {
    return e1bas.intValue();
  }
  
  public void setE1chg(BigDecimal pE1chg) {
    if (pE1chg == null) {
      return;
    }
    e1chg = pE1chg.setScale(DECIMAL_E1CHG, RoundingMode.HALF_UP);
  }
  
  public void setE1chg(Double pE1chg) {
    if (pE1chg == null) {
      return;
    }
    e1chg = BigDecimal.valueOf(pE1chg).setScale(DECIMAL_E1CHG, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1chg() {
    return e1chg.setScale(DECIMAL_E1CHG, RoundingMode.HALF_UP);
  }
  
  public void setE1rbc(BigDecimal pE1rbc) {
    if (pE1rbc == null) {
      return;
    }
    e1rbc = pE1rbc.setScale(DECIMAL_E1RBC, RoundingMode.HALF_UP);
  }
  
  public void setE1rbc(Double pE1rbc) {
    if (pE1rbc == null) {
      return;
    }
    e1rbc = BigDecimal.valueOf(pE1rbc).setScale(DECIMAL_E1RBC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rbc() {
    return e1rbc.setScale(DECIMAL_E1RBC, RoundingMode.HALF_UP);
  }
  
  public void setE1rbc2(BigDecimal pE1rbc2) {
    if (pE1rbc2 == null) {
      return;
    }
    e1rbc2 = pE1rbc2.setScale(DECIMAL_E1RBC2, RoundingMode.HALF_UP);
  }
  
  public void setE1rbc2(Double pE1rbc2) {
    if (pE1rbc2 == null) {
      return;
    }
    e1rbc2 = BigDecimal.valueOf(pE1rbc2).setScale(DECIMAL_E1RBC2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rbc2() {
    return e1rbc2.setScale(DECIMAL_E1RBC2, RoundingMode.HALF_UP);
  }
  
  public void setE1dat1(BigDecimal pE1dat1) {
    if (pE1dat1 == null) {
      return;
    }
    e1dat1 = pE1dat1.setScale(DECIMAL_E1DAT1, RoundingMode.HALF_UP);
  }
  
  public void setE1dat1(Integer pE1dat1) {
    if (pE1dat1 == null) {
      return;
    }
    e1dat1 = BigDecimal.valueOf(pE1dat1);
  }
  
  public void setE1dat1(Date pE1dat1) {
    if (pE1dat1 == null) {
      return;
    }
    e1dat1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dat1));
  }
  
  public Integer getE1dat1() {
    return e1dat1.intValue();
  }
  
  public Date getE1dat1ConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dat1.intValue(), null);
  }
  
  public void setE1dat2(BigDecimal pE1dat2) {
    if (pE1dat2 == null) {
      return;
    }
    e1dat2 = pE1dat2.setScale(DECIMAL_E1DAT2, RoundingMode.HALF_UP);
  }
  
  public void setE1dat2(Integer pE1dat2) {
    if (pE1dat2 == null) {
      return;
    }
    e1dat2 = BigDecimal.valueOf(pE1dat2);
  }
  
  public void setE1dat2(Date pE1dat2) {
    if (pE1dat2 == null) {
      return;
    }
    e1dat2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dat2));
  }
  
  public Integer getE1dat2() {
    return e1dat2.intValue();
  }
  
  public Date getE1dat2ConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dat2.intValue(), null);
  }
  
  public void setE1dat3(BigDecimal pE1dat3) {
    if (pE1dat3 == null) {
      return;
    }
    e1dat3 = pE1dat3.setScale(DECIMAL_E1DAT3, RoundingMode.HALF_UP);
  }
  
  public void setE1dat3(Integer pE1dat3) {
    if (pE1dat3 == null) {
      return;
    }
    e1dat3 = BigDecimal.valueOf(pE1dat3);
  }
  
  public void setE1dat3(Date pE1dat3) {
    if (pE1dat3 == null) {
      return;
    }
    e1dat3 = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dat3));
  }
  
  public Integer getE1dat3() {
    return e1dat3.intValue();
  }
  
  public Date getE1dat3ConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dat3.intValue(), null);
  }
  
  public void setE1dat4(BigDecimal pE1dat4) {
    if (pE1dat4 == null) {
      return;
    }
    e1dat4 = pE1dat4.setScale(DECIMAL_E1DAT4, RoundingMode.HALF_UP);
  }
  
  public void setE1dat4(Integer pE1dat4) {
    if (pE1dat4 == null) {
      return;
    }
    e1dat4 = BigDecimal.valueOf(pE1dat4);
  }
  
  public void setE1dat4(Date pE1dat4) {
    if (pE1dat4 == null) {
      return;
    }
    e1dat4 = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dat4));
  }
  
  public Integer getE1dat4() {
    return e1dat4.intValue();
  }
  
  public Date getE1dat4ConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dat4.intValue(), null);
  }
  
  public void setE1rem1(BigDecimal pE1rem1) {
    if (pE1rem1 == null) {
      return;
    }
    e1rem1 = pE1rem1.setScale(DECIMAL_E1REM1, RoundingMode.HALF_UP);
  }
  
  public void setE1rem1(Double pE1rem1) {
    if (pE1rem1 == null) {
      return;
    }
    e1rem1 = BigDecimal.valueOf(pE1rem1).setScale(DECIMAL_E1REM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rem1() {
    return e1rem1.setScale(DECIMAL_E1REM1, RoundingMode.HALF_UP);
  }
  
  public void setE1rem2(BigDecimal pE1rem2) {
    if (pE1rem2 == null) {
      return;
    }
    e1rem2 = pE1rem2.setScale(DECIMAL_E1REM2, RoundingMode.HALF_UP);
  }
  
  public void setE1rem2(Double pE1rem2) {
    if (pE1rem2 == null) {
      return;
    }
    e1rem2 = BigDecimal.valueOf(pE1rem2).setScale(DECIMAL_E1REM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rem2() {
    return e1rem2.setScale(DECIMAL_E1REM2, RoundingMode.HALF_UP);
  }
  
  public void setE1rem3(BigDecimal pE1rem3) {
    if (pE1rem3 == null) {
      return;
    }
    e1rem3 = pE1rem3.setScale(DECIMAL_E1REM3, RoundingMode.HALF_UP);
  }
  
  public void setE1rem3(Double pE1rem3) {
    if (pE1rem3 == null) {
      return;
    }
    e1rem3 = BigDecimal.valueOf(pE1rem3).setScale(DECIMAL_E1REM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rem3() {
    return e1rem3.setScale(DECIMAL_E1REM3, RoundingMode.HALF_UP);
  }
  
  public void setE1rem4(BigDecimal pE1rem4) {
    if (pE1rem4 == null) {
      return;
    }
    e1rem4 = pE1rem4.setScale(DECIMAL_E1REM4, RoundingMode.HALF_UP);
  }
  
  public void setE1rem4(Double pE1rem4) {
    if (pE1rem4 == null) {
      return;
    }
    e1rem4 = BigDecimal.valueOf(pE1rem4).setScale(DECIMAL_E1REM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rem4() {
    return e1rem4.setScale(DECIMAL_E1REM4, RoundingMode.HALF_UP);
  }
  
  public void setE1rem5(BigDecimal pE1rem5) {
    if (pE1rem5 == null) {
      return;
    }
    e1rem5 = pE1rem5.setScale(DECIMAL_E1REM5, RoundingMode.HALF_UP);
  }
  
  public void setE1rem5(Double pE1rem5) {
    if (pE1rem5 == null) {
      return;
    }
    e1rem5 = BigDecimal.valueOf(pE1rem5).setScale(DECIMAL_E1REM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rem5() {
    return e1rem5.setScale(DECIMAL_E1REM5, RoundingMode.HALF_UP);
  }
  
  public void setE1rem6(BigDecimal pE1rem6) {
    if (pE1rem6 == null) {
      return;
    }
    e1rem6 = pE1rem6.setScale(DECIMAL_E1REM6, RoundingMode.HALF_UP);
  }
  
  public void setE1rem6(Double pE1rem6) {
    if (pE1rem6 == null) {
      return;
    }
    e1rem6 = BigDecimal.valueOf(pE1rem6).setScale(DECIMAL_E1REM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rem6() {
    return e1rem6.setScale(DECIMAL_E1REM6, RoundingMode.HALF_UP);
  }
  
  public void setE1rp1(BigDecimal pE1rp1) {
    if (pE1rp1 == null) {
      return;
    }
    e1rp1 = pE1rp1.setScale(DECIMAL_E1RP1, RoundingMode.HALF_UP);
  }
  
  public void setE1rp1(Double pE1rp1) {
    if (pE1rp1 == null) {
      return;
    }
    e1rp1 = BigDecimal.valueOf(pE1rp1).setScale(DECIMAL_E1RP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rp1() {
    return e1rp1.setScale(DECIMAL_E1RP1, RoundingMode.HALF_UP);
  }
  
  public void setE1rp2(BigDecimal pE1rp2) {
    if (pE1rp2 == null) {
      return;
    }
    e1rp2 = pE1rp2.setScale(DECIMAL_E1RP2, RoundingMode.HALF_UP);
  }
  
  public void setE1rp2(Double pE1rp2) {
    if (pE1rp2 == null) {
      return;
    }
    e1rp2 = BigDecimal.valueOf(pE1rp2).setScale(DECIMAL_E1RP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rp2() {
    return e1rp2.setScale(DECIMAL_E1RP2, RoundingMode.HALF_UP);
  }
  
  public void setE1rp3(BigDecimal pE1rp3) {
    if (pE1rp3 == null) {
      return;
    }
    e1rp3 = pE1rp3.setScale(DECIMAL_E1RP3, RoundingMode.HALF_UP);
  }
  
  public void setE1rp3(Double pE1rp3) {
    if (pE1rp3 == null) {
      return;
    }
    e1rp3 = BigDecimal.valueOf(pE1rp3).setScale(DECIMAL_E1RP3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rp3() {
    return e1rp3.setScale(DECIMAL_E1RP3, RoundingMode.HALF_UP);
  }
  
  public void setE1rp4(BigDecimal pE1rp4) {
    if (pE1rp4 == null) {
      return;
    }
    e1rp4 = pE1rp4.setScale(DECIMAL_E1RP4, RoundingMode.HALF_UP);
  }
  
  public void setE1rp4(Double pE1rp4) {
    if (pE1rp4 == null) {
      return;
    }
    e1rp4 = BigDecimal.valueOf(pE1rp4).setScale(DECIMAL_E1RP4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rp4() {
    return e1rp4.setScale(DECIMAL_E1RP4, RoundingMode.HALF_UP);
  }
  
  public void setE1rp5(BigDecimal pE1rp5) {
    if (pE1rp5 == null) {
      return;
    }
    e1rp5 = pE1rp5.setScale(DECIMAL_E1RP5, RoundingMode.HALF_UP);
  }
  
  public void setE1rp5(Double pE1rp5) {
    if (pE1rp5 == null) {
      return;
    }
    e1rp5 = BigDecimal.valueOf(pE1rp5).setScale(DECIMAL_E1RP5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rp5() {
    return e1rp5.setScale(DECIMAL_E1RP5, RoundingMode.HALF_UP);
  }
  
  public void setE1rp6(BigDecimal pE1rp6) {
    if (pE1rp6 == null) {
      return;
    }
    e1rp6 = pE1rp6.setScale(DECIMAL_E1RP6, RoundingMode.HALF_UP);
  }
  
  public void setE1rp6(Double pE1rp6) {
    if (pE1rp6 == null) {
      return;
    }
    e1rp6 = BigDecimal.valueOf(pE1rp6).setScale(DECIMAL_E1RP6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1rp6() {
    return e1rp6.setScale(DECIMAL_E1RP6, RoundingMode.HALF_UP);
  }
  
  public void setE1hre(BigDecimal pE1hre) {
    if (pE1hre == null) {
      return;
    }
    e1hre = pE1hre.setScale(DECIMAL_E1HRE, RoundingMode.HALF_UP);
  }
  
  public void setE1hre(Integer pE1hre) {
    if (pE1hre == null) {
      return;
    }
    e1hre = BigDecimal.valueOf(pE1hre);
  }
  
  public Integer getE1hre() {
    return e1hre.intValue();
  }
  
  public void setE1nto(BigDecimal pE1nto) {
    if (pE1nto == null) {
      return;
    }
    e1nto = pE1nto.setScale(DECIMAL_E1NTO, RoundingMode.HALF_UP);
  }
  
  public void setE1nto(Integer pE1nto) {
    if (pE1nto == null) {
      return;
    }
    e1nto = BigDecimal.valueOf(pE1nto);
  }
  
  public Integer getE1nto() {
    return e1nto.intValue();
  }
  
  public void setE1oto(BigDecimal pE1oto) {
    if (pE1oto == null) {
      return;
    }
    e1oto = pE1oto.setScale(DECIMAL_E1OTO, RoundingMode.HALF_UP);
  }
  
  public void setE1oto(Integer pE1oto) {
    if (pE1oto == null) {
      return;
    }
    e1oto = BigDecimal.valueOf(pE1oto);
  }
  
  public Integer getE1oto() {
    return e1oto.intValue();
  }
  
  public void setE1vol(BigDecimal pE1vol) {
    if (pE1vol == null) {
      return;
    }
    e1vol = pE1vol.setScale(DECIMAL_E1VOL, RoundingMode.HALF_UP);
  }
  
  public void setE1vol(Double pE1vol) {
    if (pE1vol == null) {
      return;
    }
    e1vol = BigDecimal.valueOf(pE1vol).setScale(DECIMAL_E1VOL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1vol() {
    return e1vol.setScale(DECIMAL_E1VOL, RoundingMode.HALF_UP);
  }
  
  public void setE1lgm(BigDecimal pE1lgm) {
    if (pE1lgm == null) {
      return;
    }
    e1lgm = pE1lgm.setScale(DECIMAL_E1LGM, RoundingMode.HALF_UP);
  }
  
  public void setE1lgm(Double pE1lgm) {
    if (pE1lgm == null) {
      return;
    }
    e1lgm = BigDecimal.valueOf(pE1lgm).setScale(DECIMAL_E1LGM, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1lgm() {
    return e1lgm.setScale(DECIMAL_E1LGM, RoundingMode.HALF_UP);
  }
  
  public void setE1m2p(BigDecimal pE1m2p) {
    if (pE1m2p == null) {
      return;
    }
    e1m2p = pE1m2p.setScale(DECIMAL_E1M2P, RoundingMode.HALF_UP);
  }
  
  public void setE1m2p(Double pE1m2p) {
    if (pE1m2p == null) {
      return;
    }
    e1m2p = BigDecimal.valueOf(pE1m2p).setScale(DECIMAL_E1M2P, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getE1m2p() {
    return e1m2p.setScale(DECIMAL_E1M2P, RoundingMode.HALF_UP);
  }
  
  public void setE1dat5(BigDecimal pE1dat5) {
    if (pE1dat5 == null) {
      return;
    }
    e1dat5 = pE1dat5.setScale(DECIMAL_E1DAT5, RoundingMode.HALF_UP);
  }
  
  public void setE1dat5(Integer pE1dat5) {
    if (pE1dat5 == null) {
      return;
    }
    e1dat5 = BigDecimal.valueOf(pE1dat5);
  }
  
  public void setE1dat5(Date pE1dat5) {
    if (pE1dat5 == null) {
      return;
    }
    e1dat5 = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dat5));
  }
  
  public Integer getE1dat5() {
    return e1dat5.intValue();
  }
  
  public Date getE1dat5ConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dat5.intValue(), null);
  }
  
  public void setE1dat6(BigDecimal pE1dat6) {
    if (pE1dat6 == null) {
      return;
    }
    e1dat6 = pE1dat6.setScale(DECIMAL_E1DAT6, RoundingMode.HALF_UP);
  }
  
  public void setE1dat6(Integer pE1dat6) {
    if (pE1dat6 == null) {
      return;
    }
    e1dat6 = BigDecimal.valueOf(pE1dat6);
  }
  
  public void setE1dat6(Date pE1dat6) {
    if (pE1dat6 == null) {
      return;
    }
    e1dat6 = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dat6));
  }
  
  public Integer getE1dat6() {
    return e1dat6.intValue();
  }
  
  public Date getE1dat6ConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dat6.intValue(), null);
  }
  
  public void setE1dat7(BigDecimal pE1dat7) {
    if (pE1dat7 == null) {
      return;
    }
    e1dat7 = pE1dat7.setScale(DECIMAL_E1DAT7, RoundingMode.HALF_UP);
  }
  
  public void setE1dat7(Integer pE1dat7) {
    if (pE1dat7 == null) {
      return;
    }
    e1dat7 = BigDecimal.valueOf(pE1dat7);
  }
  
  public void setE1dat7(Date pE1dat7) {
    if (pE1dat7 == null) {
      return;
    }
    e1dat7 = BigDecimal.valueOf(ConvertDate.dateToDb2(pE1dat7));
  }
  
  public Integer getE1dat7() {
    return e1dat7.intValue();
  }
  
  public Date getE1dat7ConvertiEnDate() {
    return ConvertDate.db2ToDate(e1dat7.intValue(), null);
  }
  
  public void setE1ncc(String pE1ncc) {
    if (pE1ncc == null) {
      return;
    }
    e1ncc = pE1ncc;
  }
  
  public String getE1ncc() {
    return e1ncc;
  }
  
  public void setE1rep(String pE1rep) {
    if (pE1rep == null) {
      return;
    }
    e1rep = pE1rep;
  }
  
  public String getE1rep() {
    return e1rep;
  }
  
  public void setE1rep2(String pE1rep2) {
    if (pE1rep2 == null) {
      return;
    }
    e1rep2 = pE1rep2;
  }
  
  public String getE1rep2() {
    return e1rep2;
  }
  
  public void setE1mag(String pE1mag) {
    if (pE1mag == null) {
      return;
    }
    e1mag = pE1mag;
  }
  
  public String getE1mag() {
    return e1mag;
  }
  
  public void setE1maga(String pE1maga) {
    if (pE1maga == null) {
      return;
    }
    e1maga = pE1maga;
  }
  
  public String getE1maga() {
    return e1maga;
  }
  
  public void setE1mex(String pE1mex) {
    if (pE1mex == null) {
      return;
    }
    e1mex = pE1mex;
  }
  
  public String getE1mex() {
    return e1mex;
  }
  
  public void setE1ctr(String pE1ctr) {
    if (pE1ctr == null) {
      return;
    }
    e1ctr = pE1ctr;
  }
  
  public String getE1ctr() {
    return e1ctr;
  }
  
  public void setE1san(String pE1san) {
    if (pE1san == null) {
      return;
    }
    e1san = pE1san;
  }
  
  public String getE1san() {
    return e1san;
  }
  
  public void setE1act(String pE1act) {
    if (pE1act == null) {
      return;
    }
    e1act = pE1act;
  }
  
  public String getE1act() {
    return e1act;
  }
  
  public void setE1dev(String pE1dev) {
    if (pE1dev == null) {
      return;
    }
    e1dev = pE1dev;
  }
  
  public String getE1dev() {
    return e1dev;
  }
  
  public void setE1cnv(String pE1cnv) {
    if (pE1cnv == null) {
      return;
    }
    e1cnv = pE1cnv;
  }
  
  public String getE1cnv() {
    return e1cnv;
  }
  
  public void setE1rcc(String pE1rcc) {
    if (pE1rcc == null) {
      return;
    }
    e1rcc = pE1rcc;
  }
  
  public String getE1rcc() {
    return e1rcc;
  }
  
  public void setE1vde(String pE1vde) {
    if (pE1vde == null) {
      return;
    }
    e1vde = pE1vde;
  }
  
  public String getE1vde() {
    return e1vde;
  }
  
  public void setE1trc(Character pE1trc) {
    if (pE1trc == null) {
      return;
    }
    e1trc = String.valueOf(pE1trc);
  }
  
  public Character getE1trc() {
    return e1trc.charAt(0);
  }
  
  public void setE1ztr(String pE1ztr) {
    if (pE1ztr == null) {
      return;
    }
    e1ztr = pE1ztr;
  }
  
  public String getE1ztr() {
    return e1ztr;
  }
  
  public void setE1ebp(Character pE1ebp) {
    if (pE1ebp == null) {
      return;
    }
    e1ebp = String.valueOf(pE1ebp);
  }
  
  public Character getE1ebp() {
    return e1ebp.charAt(0);
  }
  
  public void setE1trp(Character pE1trp) {
    if (pE1trp == null) {
      return;
    }
    e1trp = String.valueOf(pE1trp);
  }
  
  public Character getE1trp() {
    return e1trp.charAt(0);
  }
  
  public void setE1nho(Character pE1nho) {
    if (pE1nho == null) {
      return;
    }
    e1nho = String.valueOf(pE1nho);
  }
  
  public Character getE1nho() {
    return e1nho.charAt(0);
  }
  
  public void setE1tp1(String pE1tp1) {
    if (pE1tp1 == null) {
      return;
    }
    e1tp1 = pE1tp1;
  }
  
  public String getE1tp1() {
    return e1tp1;
  }
  
  public void setE1tp2(String pE1tp2) {
    if (pE1tp2 == null) {
      return;
    }
    e1tp2 = pE1tp2;
  }
  
  public String getE1tp2() {
    return e1tp2;
  }
  
  public void setE1tp3(String pE1tp3) {
    if (pE1tp3 == null) {
      return;
    }
    e1tp3 = pE1tp3;
  }
  
  public String getE1tp3() {
    return e1tp3;
  }
  
  public void setE1tp4(String pE1tp4) {
    if (pE1tp4 == null) {
      return;
    }
    e1tp4 = pE1tp4;
  }
  
  public String getE1tp4() {
    return e1tp4;
  }
  
  public void setE1tp5(String pE1tp5) {
    if (pE1tp5 == null) {
      return;
    }
    e1tp5 = pE1tp5;
  }
  
  public String getE1tp5() {
    return e1tp5;
  }
  
  public void setE1veh(String pE1veh) {
    if (pE1veh == null) {
      return;
    }
    e1veh = pE1veh;
  }
  
  public String getE1veh() {
    return e1veh;
  }
  
  public void setE1in1(Character pE1in1) {
    if (pE1in1 == null) {
      return;
    }
    e1in1 = String.valueOf(pE1in1);
  }
  
  public Character getE1in1() {
    return e1in1.charAt(0);
  }
  
  public void setE1in2(Character pE1in2) {
    if (pE1in2 == null) {
      return;
    }
    e1in2 = String.valueOf(pE1in2);
  }
  
  public Character getE1in2() {
    return e1in2.charAt(0);
  }
  
  public void setE1in3(Character pE1in3) {
    if (pE1in3 == null) {
      return;
    }
    e1in3 = String.valueOf(pE1in3);
  }
  
  public Character getE1in3() {
    return e1in3.charAt(0);
  }
  
  public void setE1in4(Character pE1in4) {
    if (pE1in4 == null) {
      return;
    }
    e1in4 = String.valueOf(pE1in4);
  }
  
  public Character getE1in4() {
    return e1in4.charAt(0);
  }
  
  public void setE1in5(Character pE1in5) {
    if (pE1in5 == null) {
      return;
    }
    e1in5 = String.valueOf(pE1in5);
  }
  
  public Character getE1in5() {
    return e1in5.charAt(0);
  }
  
  public void setE1in6(Character pE1in6) {
    if (pE1in6 == null) {
      return;
    }
    e1in6 = String.valueOf(pE1in6);
  }
  
  public Character getE1in6() {
    return e1in6.charAt(0);
  }
  
  public void setE1in7(Character pE1in7) {
    if (pE1in7 == null) {
      return;
    }
    e1in7 = String.valueOf(pE1in7);
  }
  
  public Character getE1in7() {
    return e1in7.charAt(0);
  }
  
  public void setE1in8(Character pE1in8) {
    if (pE1in8 == null) {
      return;
    }
    e1in8 = String.valueOf(pE1in8);
  }
  
  public Character getE1in8() {
    return e1in8.charAt(0);
  }
  
  public void setE1in9(Character pE1in9) {
    if (pE1in9 == null) {
      return;
    }
    e1in9 = String.valueOf(pE1in9);
  }
  
  public Character getE1in9() {
    return e1in9.charAt(0);
  }
  
  public void setE1cct(String pE1cct) {
    if (pE1cct == null) {
      return;
    }
    e1cct = pE1cct;
  }
  
  public String getE1cct() {
    return e1cct;
  }
  
  public void setE1trl(Character pE1trl) {
    if (pE1trl == null) {
      return;
    }
    e1trl = String.valueOf(pE1trl);
  }
  
  public Character getE1trl() {
    return e1trl.charAt(0);
  }
  
  public void setE1brl(Character pE1brl) {
    if (pE1brl == null) {
      return;
    }
    e1brl = String.valueOf(pE1brl);
  }
  
  public Character getE1brl() {
    return e1brl.charAt(0);
  }
  
  public void setE1tre(Character pE1tre) {
    if (pE1tre == null) {
      return;
    }
    e1tre = String.valueOf(pE1tre);
  }
  
  public Character getE1tre() {
    return e1tre.charAt(0);
  }
  
  public void setE1fil2(Character pE1fil2) {
    if (pE1fil2 == null) {
      return;
    }
    e1fil2 = String.valueOf(pE1fil2);
  }
  
  public Character getE1fil2() {
    return e1fil2.charAt(0);
  }
  
  public void setE1in11(Character pE1in11) {
    if (pE1in11 == null) {
      return;
    }
    e1in11 = String.valueOf(pE1in11);
  }
  
  public Character getE1in11() {
    return e1in11.charAt(0);
  }
  
  public void setE1in12(Character pE1in12) {
    if (pE1in12 == null) {
      return;
    }
    e1in12 = String.valueOf(pE1in12);
  }
  
  public Character getE1in12() {
    return e1in12.charAt(0);
  }
  
  public void setE1in13(Character pE1in13) {
    if (pE1in13 == null) {
      return;
    }
    e1in13 = String.valueOf(pE1in13);
  }
  
  public Character getE1in13() {
    return e1in13.charAt(0);
  }
  
  public void setE1in14(Character pE1in14) {
    if (pE1in14 == null) {
      return;
    }
    e1in14 = String.valueOf(pE1in14);
  }
  
  public Character getE1in14() {
    return e1in14.charAt(0);
  }
  
  public void setE1in15(Character pE1in15) {
    if (pE1in15 == null) {
      return;
    }
    e1in15 = String.valueOf(pE1in15);
  }
  
  public Character getE1in15() {
    return e1in15.charAt(0);
  }
  
  public void setE1in16(Character pE1in16) {
    if (pE1in16 == null) {
      return;
    }
    e1in16 = String.valueOf(pE1in16);
  }
  
  public Character getE1in16() {
    return e1in16.charAt(0);
  }
  
  public void setE1can(String pE1can) {
    if (pE1can == null) {
      return;
    }
    e1can = pE1can;
  }
  
  public String getE1can() {
    return e1can;
  }
  
  public void setE1crt(String pE1crt) {
    if (pE1crt == null) {
      return;
    }
    e1crt = pE1crt;
  }
  
  public String getE1crt() {
    return e1crt;
  }
  
  public void setE1pre(String pE1pre) {
    if (pE1pre == null) {
      return;
    }
    e1pre = pE1pre;
  }
  
  public String getE1pre() {
    return e1pre;
  }
  
  public void setE1pfc(String pE1pfc) {
    if (pE1pfc == null) {
      return;
    }
    e1pfc = pE1pfc;
  }
  
  public String getE1pfc() {
    return e1pfc;
  }
  
  public void setE1in17(Character pE1in17) {
    if (pE1in17 == null) {
      return;
    }
    e1in17 = String.valueOf(pE1in17);
  }
  
  public Character getE1in17() {
    return e1in17.charAt(0);
  }
  
  public void setE1in18(Character pE1in18) {
    if (pE1in18 == null) {
      return;
    }
    e1in18 = String.valueOf(pE1in18);
  }
  
  public Character getE1in18() {
    return e1in18.charAt(0);
  }
  
  public void setE1in19(Character pE1in19) {
    if (pE1in19 == null) {
      return;
    }
    e1in19 = String.valueOf(pE1in19);
  }
  
  public Character getE1in19() {
    return e1in19.charAt(0);
  }
  
  public void setE1in20(Character pE1in20) {
    if (pE1in20 == null) {
      return;
    }
    e1in20 = String.valueOf(pE1in20);
  }
  
  public Character getE1in20() {
    return e1in20.charAt(0);
  }
  
  public void setE1in21(Character pE1in21) {
    if (pE1in21 == null) {
      return;
    }
    e1in21 = String.valueOf(pE1in21);
  }
  
  public Character getE1in21() {
    return e1in21.charAt(0);
  }
  
  public void setE1in22(Character pE1in22) {
    if (pE1in22 == null) {
      return;
    }
    e1in22 = String.valueOf(pE1in22);
  }
  
  public Character getE1in22() {
    return e1in22.charAt(0);
  }
  
  public void setE1nat(Character pE1nat) {
    if (pE1nat == null) {
      return;
    }
    e1nat = String.valueOf(pE1nat);
  }
  
  public Character getE1nat() {
    return e1nat.charAt(0);
  }
  
  public void setPoenv(Character pPoenv) {
    if (pPoenv == null) {
      return;
    }
    poenv = String.valueOf(pPoenv);
  }
  
  public Character getPoenv() {
    return poenv.charAt(0);
  }
  
  public void setPodir(Character pPodir) {
    if (pPodir == null) {
      return;
    }
    podir = String.valueOf(pPodir);
  }
  
  public Character getPodir() {
    return podir.charAt(0);
  }
  
  public void setPoimd(Character pPoimd) {
    if (pPoimd == null) {
      return;
    }
    poimd = String.valueOf(pPoimd);
  }
  
  public Character getPoimd() {
    return poimd.charAt(0);
  }
  
  public void setPochan(BigDecimal pPochan) {
    if (pPochan == null) {
      return;
    }
    pochan = pPochan.setScale(DECIMAL_POCHAN, RoundingMode.HALF_UP);
  }
  
  public void setPochan(Integer pPochan) {
    if (pPochan == null) {
      return;
    }
    pochan = BigDecimal.valueOf(pPochan);
  }
  
  public Integer getPochan() {
    return pochan.intValue();
  }
  
  public void setPocol(BigDecimal pPocol) {
    if (pPocol == null) {
      return;
    }
    pocol = pPocol.setScale(DECIMAL_POCOL, RoundingMode.HALF_UP);
  }
  
  public void setPocol(Integer pPocol) {
    if (pPocol == null) {
      return;
    }
    pocol = BigDecimal.valueOf(pPocol);
  }
  
  public Integer getPocol() {
    return pocol.intValue();
  }
  
  public void setPofrs(BigDecimal pPofrs) {
    if (pPofrs == null) {
      return;
    }
    pofrs = pPofrs.setScale(DECIMAL_POFRS, RoundingMode.HALF_UP);
  }
  
  public void setPofrs(Integer pPofrs) {
    if (pPofrs == null) {
      return;
    }
    pofrs = BigDecimal.valueOf(pPofrs);
  }
  
  public Integer getPofrs() {
    return pofrs.intValue();
  }
  
  public void setPofre(BigDecimal pPofre) {
    if (pPofre == null) {
      return;
    }
    pofre = pPofre.setScale(DECIMAL_POFRE, RoundingMode.HALF_UP);
  }
  
  public void setPofre(Integer pPofre) {
    if (pPofre == null) {
      return;
    }
    pofre = BigDecimal.valueOf(pPofre);
  }
  
  public Integer getPofre() {
    return pofre.intValue();
  }
  
  public void setPoinex(BigDecimal pPoinex) {
    if (pPoinex == null) {
      return;
    }
    poinex = pPoinex.setScale(DECIMAL_POINEX, RoundingMode.HALF_UP);
  }
  
  public void setPoinex(Integer pPoinex) {
    if (pPoinex == null) {
      return;
    }
    poinex = BigDecimal.valueOf(pPoinex);
  }
  
  public Integer getPoinex() {
    return poinex.intValue();
  }
  
  public void setPoenlv(String pPoenlv) {
    if (pPoenlv == null) {
      return;
    }
    poenlv = pPoenlv;
  }
  
  public String getPoenlv() {
    return poenlv;
  }
  
  public void setPoban(BigDecimal pPoban) {
    if (pPoban == null) {
      return;
    }
    poban = pPoban.setScale(DECIMAL_POBAN, RoundingMode.HALF_UP);
  }
  
  public void setPoban(Integer pPoban) {
    if (pPoban == null) {
      return;
    }
    poban = BigDecimal.valueOf(pPoban);
  }
  
  public Integer getPoban() {
    return poban.intValue();
  }
  
  public void setPobas(BigDecimal pPobas) {
    if (pPobas == null) {
      return;
    }
    pobas = pPobas.setScale(DECIMAL_POBAS, RoundingMode.HALF_UP);
  }
  
  public void setPobas(Integer pPobas) {
    if (pPobas == null) {
      return;
    }
    pobas = BigDecimal.valueOf(pPobas);
  }
  
  public Integer getPobas() {
    return pobas.intValue();
  }
  
  public void setPobae(BigDecimal pPobae) {
    if (pPobae == null) {
      return;
    }
    pobae = pPobae.setScale(DECIMAL_POBAE, RoundingMode.HALF_UP);
  }
  
  public void setPobae(Integer pPobae) {
    if (pPobae == null) {
      return;
    }
    pobae = BigDecimal.valueOf(pPobae);
  }
  
  public Integer getPobae() {
    return pobae.intValue();
  }
  
  public void setPobon(BigDecimal pPobon) {
    if (pPobon == null) {
      return;
    }
    pobon = pPobon.setScale(DECIMAL_POBON, RoundingMode.HALF_UP);
  }
  
  public void setPobon(Integer pPobon) {
    if (pPobon == null) {
      return;
    }
    pobon = BigDecimal.valueOf(pPobon);
  }
  
  public Integer getPobon() {
    return pobon.intValue();
  }
  
  public void setPobos(BigDecimal pPobos) {
    if (pPobos == null) {
      return;
    }
    pobos = pPobos.setScale(DECIMAL_POBOS, RoundingMode.HALF_UP);
  }
  
  public void setPobos(Integer pPobos) {
    if (pPobos == null) {
      return;
    }
    pobos = BigDecimal.valueOf(pPobos);
  }
  
  public Integer getPobos() {
    return pobos.intValue();
  }
  
  public void setE1cllp(BigDecimal pE1cllp) {
    if (pE1cllp == null) {
      return;
    }
    e1cllp = pE1cllp.setScale(DECIMAL_E1CLLP, RoundingMode.HALF_UP);
  }
  
  public void setE1cllp(Integer pE1cllp) {
    if (pE1cllp == null) {
      return;
    }
    e1cllp = BigDecimal.valueOf(pE1cllp);
  }
  
  public Integer getE1cllp() {
    return e1cllp.intValue();
  }
  
  public void setE1clls(BigDecimal pE1clls) {
    if (pE1clls == null) {
      return;
    }
    e1clls = pE1clls.setScale(DECIMAL_E1CLLS, RoundingMode.HALF_UP);
  }
  
  public void setE1clls(Integer pE1clls) {
    if (pE1clls == null) {
      return;
    }
    e1clls = BigDecimal.valueOf(pE1clls);
  }
  
  public Integer getE1clls() {
    return e1clls.intValue();
  }
  
  public void setPodat3(BigDecimal pPodat3) {
    if (pPodat3 == null) {
      return;
    }
    podat3 = pPodat3.setScale(DECIMAL_PODAT3, RoundingMode.HALF_UP);
  }
  
  public void setPodat3(Integer pPodat3) {
    if (pPodat3 == null) {
      return;
    }
    podat3 = BigDecimal.valueOf(pPodat3);
  }
  
  public void setPodat3(Date pPodat3) {
    if (pPodat3 == null) {
      return;
    }
    podat3 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodat3));
  }
  
  public Integer getPodat3() {
    return podat3.intValue();
  }
  
  public Date getPodat3ConvertiEnDate() {
    return ConvertDate.db2ToDate(podat3.intValue(), null);
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
