/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Psemparm_ds_T3 pour les T3
 * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
 */
public class Psemparm_ds_T3 extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD7"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF7"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN7"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD8"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF8"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN8"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD9"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF9"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN9"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD10"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF10"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN10"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD11"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF11"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN11"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD12"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF12"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN12"));
    
    length = 200;
  }
}
