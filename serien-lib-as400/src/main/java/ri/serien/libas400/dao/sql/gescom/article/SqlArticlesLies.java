/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.article;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.CritereArticleLie;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlArticlesLies {
  // Variables
  protected QueryManager querymg = null;
  public static final String ARTICLES_LIES_AUTOMATIQUEMENT = "*ALA";
  public static final String ARTICLES_LIES_PROPOSE = "*ALP";
  
  /**
   * Constructeur.
   */
  public SqlArticlesLies(QueryManager pQuerymg) {
    querymg = pQuerymg;
  }
  
  /**
   * Chargement d'une liste d'articles liés proposés et automatiques pour un article donné.
   */
  public ListeArticle chargerListeArticleLie(CritereArticleLie pCritere) {
    if (pCritere == null) {
      throw new MessageErreurException("Les critères de recherche des articles liés sont invalides.");
    }
    IdArticle idArticle = pCritere.getIdArticle();
    IdArticle.controlerId(idArticle, true);
    
    String requete = "select * from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LIE + " where ALETB = '"
        + idArticle.getCodeEtablissement() + "' and ALART = '" + idArticle.getCodeArticle() + "'";
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeArticle listeArticlesLies = new ListeArticle();
    for (GenericRecord record : listeRecord) {
      listeArticlesLies.add(initialiserArticleLie(record));
    }
    
    return listeArticlesLies;
  }
  
  /**
   * Vérifie l'existence en base d'au moins un article lié pour l'article donné.
   */
  public boolean verifierExistenceArticleLiePropose(CritereArticleLie pCritere) {
    if (pCritere == null) {
      throw new MessageErreurException("Les critères de recherche des articles liés sont invalides.");
    }
    IdArticle idArticle = pCritere.getIdArticle();
    IdArticle.controlerId(idArticle, true);
    IdFournisseur idFournisseur = pCritere.getIdFournisseur();
    String requete;
    
    // Requête simple sans contrôle du fourisseur
    if (idFournisseur == null || pCritere.isFiltreTousFournisseur()) {
      requete = "select ALARL from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LIE + " where ALETB = '"
          + idArticle.getCodeEtablissement() + "' and ALART = " + RequeteSql.formaterStringSQL(idArticle.getCodeArticle())
          + " and ALCNV <> " + RequeteSql.formaterStringSQL(ARTICLES_LIES_AUTOMATIQUEMENT);
    }
    // Requête avec contrôle que le fournisseur soit identique et les articles ne soient pas des palettes
    else {
      IdFournisseur.controlerId(idFournisseur, true);
      requete = "select ALARL from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LIE + ", " + querymg.getLibrary()
          + '.' + EnumTableBDD.ARTICLE + " art where ALETB = '" + idArticle.getCodeEtablissement() + "' and ALART = "
          + RequeteSql.formaterStringSQL(idArticle.getCodeArticle()) + " and ALETB = art.A1ETB and ALART = art.A1ART and ALCNV <> "
          + RequeteSql.formaterStringSQL(ARTICLES_LIES_AUTOMATIQUEMENT) + " and art.A1COF = " + idFournisseur.getCollectif()
          + " and art.A1FRS = " + idFournisseur.getNumero() + " and exists (select A1ART from " + querymg.getLibrary() + '.'
          + EnumTableBDD.ARTICLE + " artl where artl.A1ETB = ALETB"
          + " and artl.A1ART = ALARL and artl.A1COF = art.A1COF and artl.A1FRS = art.A1FRS and artl.A1TSP <> '0')";
    }
    
    int nombreResultats = querymg.nbrEnrgSelect(requete);
    if (nombreResultats < 1) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Chargement d'une liste d'articles liés proposés pour un article donné.
   */
  public ListeArticle chargerListeArticleLieProposes(CritereArticleLie pCritere) {
    if (pCritere == null) {
      throw new MessageErreurException("Les critères de recherche des articles liés sont invalides.");
    }
    IdArticle idArticle = pCritere.getIdArticle();
    IdArticle.controlerId(idArticle, true);
    IdFournisseur idFournisseur = pCritere.getIdFournisseur();
    String requete;
    
    // Requête simple sans contrôle du fournisseur
    if (idFournisseur == null || pCritere.isFiltreTousFournisseur()) {
      requete = "select * from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LIE + " where ALETB = '"
          + idArticle.getCodeEtablissement() + "' and ALART = " + RequeteSql.formaterStringSQL(idArticle.getCodeArticle())
          + " and ALCNV <> " + RequeteSql.formaterStringSQL(ARTICLES_LIES_AUTOMATIQUEMENT);
    }
    // Requête avec contrôle que le fournisseur soit identique et les articles ne soient pas des palettes
    else {
      IdFournisseur.controlerId(idFournisseur, true);
      requete = "select arl.* from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LIE + " arl, " + querymg.getLibrary()
          + '.' + EnumTableBDD.ARTICLE + " art where ALETB = '" + idArticle.getCodeEtablissement() + "' and ALART = "
          + RequeteSql.formaterStringSQL(idArticle.getCodeArticle()) + " and ALETB = art.A1ETB and ALART = art.A1ART and ALCNV <> "
          + RequeteSql.formaterStringSQL(ARTICLES_LIES_AUTOMATIQUEMENT) + " and art.A1COF = " + idFournisseur.getCollectif()
          + " and art.A1FRS = " + idFournisseur.getNumero() + " and exists (select A1ART from " + querymg.getLibrary() + '.'
          + EnumTableBDD.ARTICLE + " artl where artl.A1ETB = ALETB "
          + " and artl.A1ART = ALARL and artl.A1COF = art.A1COF and artl.A1FRS = art.A1FRS and artl.A1TSP <> '0')";
    }
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeArticle listeArticlesLies = new ListeArticle();
    for (GenericRecord record : listeRecord) {
      listeArticlesLies.add(initialiserArticleLie(record));
    }
    
    return listeArticlesLies;
  }
  
  /**
   * Lecture d'un article donnée.
   * 
   */
  public ListeArticle chargerListeArticleLieAutomatiquement(CritereArticleLie pCritere) {
    if (pCritere == null) {
      throw new MessageErreurException("Les critères de recherche des articles liés sont invalides.");
    }
    IdArticle idArticle = pCritere.getIdArticle();
    IdArticle.controlerId(idArticle, true);
    
    String requete = "select * from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LIE + " where ALETB = '"
        + idArticle.getCodeEtablissement() + "' and ALART = '" + idArticle.getCodeArticle() + "' and ALCNV = '"
        + ARTICLES_LIES_AUTOMATIQUEMENT + "'";
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeArticle listeArticlesLies = new ListeArticle();
    for (GenericRecord record : listeRecord) {
      listeArticlesLies.add(initialiserArticleLie(record));
    }
    
    return listeArticlesLies;
  }
  
  /**
   * Initilialise la classe article à partir du generic record.
   */
  private Article initialiserArticleLie(GenericRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("ALETB", "", false));
    Article article = new Article(IdArticle.getInstance(idEtablissement, pRecord.getStringValue("ALARL", "", true)));
    article.setTopSysteme(pRecord.getIntValue("ALTOP", 0));
    article.setDateCreation(pRecord.getDateValue("ALCRE", 0));
    article.setDateModification(pRecord.getDateValue("ALMOD", 0));
    article.setDateTraitement(pRecord.getDateValue("ALTRT", 0));
    article.setRattachementCNV(pRecord.getStringValue("ALCNV", "", true));
    
    return article;
  }
  
  /**
   * vérifie l'existance en base d'au moins un article lié pour l'article donné
   * 
   */
  public boolean verifierExistenceArticleLieAutomatiquement(CritereArticleLie pCritere) {
    if (pCritere == null) {
      throw new MessageErreurException("Les critères de recherche des articles liés sont invalides.");
    }
    IdArticle idArticle = pCritere.getIdArticle();
    IdArticle.controlerId(idArticle, true);
    
    String requete = "select ALARL from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_LIE + " where ALETB = '"
        + idArticle.getCodeEtablissement() + "' and ALART = '" + idArticle.getCodeArticle() + "' and TRIM(ALCNV) = '"
        + ARTICLES_LIES_AUTOMATIQUEMENT + "'";
    
    int nombreResultats = querymg.nbrEnrgSelect(requete);
    if (nombreResultats < 1) {
      return false;
    }
    return true;
  }
  
}
