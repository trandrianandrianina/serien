/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx004.v2;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.JsonClientMagentoV2;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Version 2 des flux.
 * Cette classe permet d'exporter un client Série N vers Magento.
 */
public class ExportationClientMagentoV2 extends BaseGroupDB {
  
  /**
   * Constructeur.
   */
  public ExportationClientMagentoV2(QueryManager pQueryManager) {
    super(pQueryManager);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne une string JSON contenant les informations d'un client pour Magento.
   */
  public String retournerUnClientMagentoJSON(FluxMagento pFlux) {
    // Contrôles
    if (pFlux == null || pFlux.getFLETB() == null || pFlux.getFLCOD() == null) {
      majError("[GM_Export_Clients] retournerUnClientMagentoJSON() -> Record NULL ou corrompues ");
      return null;
    }
    
    // Chargement du client à partir de la base Série N
    GenericRecord recordClientPrincipal = retournerClientSerieN(pFlux.getFLETB(), pFlux.getFLCOD());
    if (recordClientPrincipal == null) {
      return null;
    }
    
    // Préparation des données du client qui va être exporté vers Magento
    JsonClientMagentoV2 clientMagento = new JsonClientMagentoV2(pFlux);
    Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), clientMagento.getEtb());
    if (numeroInstance == null) {
      majError(
          "[ExportClientMagento] retournerUnClientMagentoJSON() : problème d'interprétation de l'établissement vers l'instance Magento");
      return null;
    }
    clientMagento.setIdInstanceMagento(numeroInstance);
    
    // Mise à jour du client Magento à partir des données de Série N
    if (!clientMagento.majDesInfosPourExport(recordClientPrincipal)) {
      majError("[GM_Export_Clients] retournerUnClientMagentoJSON() :" + clientMagento.getMsgError());
      return null;
    }
    
    // Chargement de l'adresse du client de facturation
    GenericRecord RcClientFacturation = retournerAdresseClientFacturation(clientMagento);
    if (RcClientFacturation != null) {
      clientMagento.majAdresseClientFacturation(RcClientFacturation);
    }
    
    // Chargement des adresses de livraison
    ArrayList<GenericRecord> RcClientsLivres = retournerAdressesLivraison(clientMagento);
    if (RcClientsLivres != null) {
      clientMagento.majAdressesClientsLivres(RcClientsLivres);
    }
    
    // Le flux est bloqué si la variable non géré sur le Web est positionné à 1
    if (clientMagento.getExclusWeb() == null || clientMagento.getExclusWeb().equals(JsonClientMagentoV2.EXCLUS_WEB)) {
      majError("Ce client n'est pas géré sur le Web");
      return "";
    }
    
    // Début de la construction du JSON
    String retour = null;
    if (initJSON()) {
      // Contrôle les données de base
      if (clientMagento.getCLCLI() != null && clientMagento.getCLLIV() != null && clientMagento.getNomSociete() != null
          && clientMagento.getEmail() != null) {
        try {
          // Conversion du record en JSON
          retour = gson.toJson(clientMagento);
        }
        catch (Exception e) {
          retour = null;
          majError("[GM_Export_Clients] retournerUnClientMagentoJSON() -> ERREUR JSON: " + e.getMessage());
        }
      }
      else {
        majError("[GM_Export_Clients] retournerUnArticleMagentoJSON() -> clientMG corrompu ");
      }
    }
    else {
      majError("[GM_Export_Clients] retournerUnArticleMagentoJSON() -> initJSON en erreur ");
    }
    
    return retour;
  }
  
  @Override
  public void dispose() {
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne les données d'un client Série N sur la base de l'établissement et du code fourni.
   */
  private GenericRecord retournerClientSerieN(String pCodeEtablissement, String pIndicatifClient) {
    if (pCodeEtablissement == null || pIndicatifClient == null) {
      majError("[GM_Export_Clients] retournerClientDB2() -> etb/code NULL ou corrompus ");
      return null;
    }
    
    // Découpe l'indicatif du client en identifiant clients CLCLI et CLLIV
    String CLCLI = null;
    String CLLIV = null;
    String[] tabDecoupe = pIndicatifClient.split("/");
    if (tabDecoupe != null && tabDecoupe.length == 2) {
      CLCLI = tabDecoupe[0];
      // On prend le client principal systématiquement, les clients livrés apparaissent sous forme de clients livrés
      CLLIV = "0";
      // Problème lors de la transformation de l'indicatif en identifiant
      if (CLCLI == null || CLCLI.length() == 0) {
        majError("[GM_Export_Clients] retournerClientDB2() -> CLCLI ou CLLIV corrompus " + pIndicatifClient);
        return null;
      }
    }
    else {
      majError("[GM_Export_Clients] retournerClientDB2() -> tabDecoupe NULL ou corrompue " + pIndicatifClient);
      return null;
    }
    
    // Récupération des informations en table du client
    // @formatter:off
    String requete = " SELECT CLETB,CLCLI,CLLIV,CLADH,CLAPEN,CLCAT,CLRGL,CLNOM,CLCPL,CLRUE,CLLOC,CLVIL,CLCDP1,"
        + " CLCOP,CLPAY,CLTEL,CLFAX,CLSRN,CLSRT,CLTOP1,CLTOP2,CLTAR,CLIN9,CLCLFS,CLCLFP,CLTFA,EBIN09,"
        + " RECIV, RENUM, REPAC, RENOM, REPRE, RENET FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT_LIEN
        + " ON RLETB = CLETB AND RLIND = DIGITS(CLCLI)||DIGITS(CLLIV) AND RLIN1 = 'P'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT
        + " ON RENUM = RLNUMT AND RLCOD = 'C'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT_EXTENSION
        + " ON CLETB = EBETB AND EBCLI = CLCLI AND EBLIV = CLLIV "
        + " WHERE CLETB = " + RequeteSql.formaterStringSQL(pCodeEtablissement) + " AND CLCLI = " + CLCLI + " AND CLLIV = " + CLLIV;
    // @formatter:on
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    if (liste != null && liste.size() == 1) {
      return liste.get(0);
    }
    else {
      if (liste == null || liste.isEmpty()) {
        majError("[GM_Export_Clients] retournerClientDB2() -> La liste est nulle ou vide : " + pCodeEtablissement + "/" + pIndicatifClient
            + " -> " + queryManager.getMsgError());
      }
      else {
        majError("[GM_Export_Clients] retournerClientDB2() -> Plusieurs clients ont été trouvés (" + liste.size() + ") : "
            + pCodeEtablissement + "/" + pIndicatifClient);
      }
      return null;
    }
  }
  
  /**
   * Retourner le client de facturation.
   * Si le client est désactivé il sera ignoré.
   */
  private GenericRecord retournerAdresseClientFacturation(JsonClientMagentoV2 pJsonClientMagento) {
    if (pJsonClientMagento.getCLCLFP() == null || pJsonClientMagento.getCLCLFS() == null) {
      return null;
    }
    
    // Récupération des informations en table du client
    // @formatter:off
    String requete = " SELECT CLETB,CLCLI,CLLIV,CLADH,CLAPEN,CLCAT,CLRGL,CLNOM,CLCPL,CLRUE,CLLOC,CLVIL,CLCDP1,"
        + " CLCOP,CLPAY,CLTEL,CLFAX,CLSRN,CLSRT,CLTOP1,CLTOP2,CLTAR,CLIN9,CLCLFS,CLCLFP,EBIN09,"
        + " RECIV, RENUM, REPAC, RENOM, REPRE, RENET FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT_LIEN
        + " ON RLETB = CLETB AND RLIND = DIGITS(CLCLI)||DIGITS(CLLIV) AND RLIN1 = 'P'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT
        + " ON RENUM = RLNUMT AND RLCOD = 'C'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT_EXTENSION
        + " ON CLETB = EBETB AND EBCLI = CLCLI AND EBLIV = CLLIV"
        + " WHERE CLETB = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getEtb())
        + " AND CLCLI = " + pJsonClientMagento.getCLCLFP() + " AND CLLIV = " + pJsonClientMagento.getCLCLFS() + " AND CLTNS <> 9";
    // @formatter:on
    
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    if (liste == null || liste.isEmpty()) {
      majError("retournerAdresseClientFacturation(): Aucun enregistrement trouvé " + queryManager.getMsgError());
      return null;
    }
    
    return liste.get(0);
  }
  
  /**
   * Retourner des clients livrés à partir d'un client facturé (xxxxxx/0).
   * Les clients livrés désactivés sont ignorés.
   */
  private ArrayList<GenericRecord> retournerAdressesLivraison(JsonClientMagentoV2 pClient) {
    // Récupération des informations en table du client
    // @formatter:off
    String requete = " SELECT CLETB,CLCLI,CLLIV,CLADH,CLAPEN,CLCAT,CLRGL,CLNOM,CLCPL,CLRUE,CLLOC,CLVIL,CLCDP1,"
        + " CLCOP,CLPAY,CLTEL,CLFAX,CLSRN,CLSRT,CLTOP1,CLTOP2,CLTAR,CLIN9,CLCLFS,CLCLFP,EBIN09,"
        + " RECIV, RENUM, REPAC, RENOM, REPRE, RENET FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT_LIEN
        + " ON RLETB = CLETB AND RLIND = DIGITS(CLCLI)||DIGITS(CLLIV) AND RLIN1 = 'P'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT
        + " ON RENUM = RLNUMT AND RLCOD = 'C'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT_EXTENSION
        + " ON CLETB = EBETB AND EBCLI = CLCLI AND EBLIV = CLLIV"
        + " WHERE CLETB = " + RequeteSql.formaterStringSQL(pClient.getEtb())
        + " AND CLCLI = " + pClient.getCLCLI() + " AND CLLIV <> 0 AND EBIN09 <> '' AND CLTNS <> 9";
    // @formatter:on
    
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    if (liste == null) {
      majError("retournerClientsLivres(): " + queryManager.getMsgError());
    }
    
    return liste;
  }
  
}
