/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_TA pour les TA
 */
public class DsTarif extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "TAETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "TATYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "TACOD"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "TALIB"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "TADAP"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO3"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO4"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO5"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO6"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO7"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO8"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO9"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TACO10"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB5"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB6"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB7"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB8"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB9"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TALB10"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TAIN1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TAIN2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TAIN3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TAART"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(13), "TATIR"));
    
    length = 300;
  }
}
