/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0003o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POOPP = 60;
  public static final int SIZE_POOPPL = 720;
  public static final int SIZE_POOPP0 = 3;
  public static final int SIZE_POOPPL0 = 20;
  public static final int SIZE_POER01 = 1;
  public static final int DECIMAL_POER01 = 0;
  public static final int SIZE_POER01A = 1;
  public static final int SIZE_POER02 = 1;
  public static final int DECIMAL_POER02 = 0;
  public static final int SIZE_POER03 = 1;
  public static final int DECIMAL_POER03 = 0;
  public static final int SIZE_POER03A = 7;
  public static final int DECIMAL_POER03A = 0;
  public static final int SIZE_POER04 = 1;
  public static final int DECIMAL_POER04 = 0;
  public static final int SIZE_POER05 = 1;
  public static final int DECIMAL_POER05 = 0;
  public static final int SIZE_POER05A = 7;
  public static final int DECIMAL_POER05A = 0;
  public static final int SIZE_POER06 = 1;
  public static final int DECIMAL_POER06 = 0;
  public static final int SIZE_POER06A = 5;
  public static final int DECIMAL_POER06A = 0;
  public static final int SIZE_POER07 = 1;
  public static final int DECIMAL_POER07 = 0;
  public static final int SIZE_POER07A = 5;
  public static final int DECIMAL_POER07A = 0;
  public static final int SIZE_POER08 = 1;
  public static final int DECIMAL_POER08 = 0;
  public static final int SIZE_POER08A = 5;
  public static final int DECIMAL_POER08A = 0;
  public static final int SIZE_POER09 = 1;
  public static final int DECIMAL_POER09 = 0;
  public static final int SIZE_POER10 = 1;
  public static final int DECIMAL_POER10 = 0;
  public static final int SIZE_POER11 = 1;
  public static final int DECIMAL_POER11 = 0;
  public static final int SIZE_POER12 = 1;
  public static final int DECIMAL_POER12 = 0;
  public static final int SIZE_POER13 = 1;
  public static final int DECIMAL_POER13 = 0;
  public static final int SIZE_POER14 = 1;
  public static final int DECIMAL_POER14 = 0;
  public static final int SIZE_POER14A = 5;
  public static final int DECIMAL_POER14A = 0;
  public static final int SIZE_POER15 = 1;
  public static final int DECIMAL_POER15 = 0;
  public static final int SIZE_POER16 = 1;
  public static final int DECIMAL_POER16 = 0;
  public static final int SIZE_POER16A = 30;
  public static final int SIZE_POER17 = 1;
  public static final int DECIMAL_POER17 = 0;
  public static final int SIZE_POER17A = 30;
  public static final int SIZE_POER18 = 1;
  public static final int DECIMAL_POER18 = 0;
  public static final int SIZE_POER19 = 1;
  public static final int DECIMAL_POER19 = 0;
  public static final int SIZE_POER19A = 5;
  public static final int DECIMAL_POER19A = 2;
  public static final int SIZE_POER20 = 1;
  public static final int DECIMAL_POER20 = 0;
  public static final int SIZE_POER20A = 9;
  public static final int DECIMAL_POER20A = 0;
  public static final int SIZE_POER21 = 1;
  public static final int DECIMAL_POER21 = 0;
  public static final int SIZE_POER21A = 4;
  public static final int SIZE_POER22 = 1;
  public static final int DECIMAL_POER22 = 0;
  public static final int SIZE_POER22A = 4;
  public static final int SIZE_POER23 = 1;
  public static final int DECIMAL_POER23 = 0;
  public static final int SIZE_POER23A = 4;
  public static final int SIZE_POER24 = 1;
  public static final int DECIMAL_POER24 = 0;
  public static final int SIZE_POER24A = 4;
  public static final int SIZE_POER25 = 1;
  public static final int DECIMAL_POER25 = 0;
  public static final int SIZE_POER25A = 4;
  public static final int SIZE_POER26 = 1;
  public static final int DECIMAL_POER26 = 0;
  public static final int SIZE_POER26A = 30;
  public static final int SIZE_POER27 = 1;
  public static final int DECIMAL_POER27 = 0;
  public static final int SIZE_POER27A = 5;
  public static final int DECIMAL_POER27A = 2;
  public static final int SIZE_POER28 = 1;
  public static final int DECIMAL_POER28 = 0;
  public static final int SIZE_POER28A = 20;
  public static final int SIZE_POER29 = 1;
  public static final int DECIMAL_POER29 = 0;
  public static final int SIZE_POER29A = 20;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 1047;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POOPP = 1;
  public static final int VAR_POOPPL = 2;
  public static final int VAR_POOPP0 = 3;
  public static final int VAR_POOPPL0 = 4;
  public static final int VAR_POER01 = 5;
  public static final int VAR_POER01A = 6;
  public static final int VAR_POER02 = 7;
  public static final int VAR_POER03 = 8;
  public static final int VAR_POER03A = 9;
  public static final int VAR_POER04 = 10;
  public static final int VAR_POER05 = 11;
  public static final int VAR_POER05A = 12;
  public static final int VAR_POER06 = 13;
  public static final int VAR_POER06A = 14;
  public static final int VAR_POER07 = 15;
  public static final int VAR_POER07A = 16;
  public static final int VAR_POER08 = 17;
  public static final int VAR_POER08A = 18;
  public static final int VAR_POER09 = 19;
  public static final int VAR_POER10 = 20;
  public static final int VAR_POER11 = 21;
  public static final int VAR_POER12 = 22;
  public static final int VAR_POER13 = 23;
  public static final int VAR_POER14 = 24;
  public static final int VAR_POER14A = 25;
  public static final int VAR_POER15 = 26;
  public static final int VAR_POER16 = 27;
  public static final int VAR_POER16A = 28;
  public static final int VAR_POER17 = 29;
  public static final int VAR_POER17A = 30;
  public static final int VAR_POER18 = 31;
  public static final int VAR_POER19 = 32;
  public static final int VAR_POER19A = 33;
  public static final int VAR_POER20 = 34;
  public static final int VAR_POER20A = 35;
  public static final int VAR_POER21 = 36;
  public static final int VAR_POER21A = 37;
  public static final int VAR_POER22 = 38;
  public static final int VAR_POER22A = 39;
  public static final int VAR_POER23 = 40;
  public static final int VAR_POER23A = 41;
  public static final int VAR_POER24 = 42;
  public static final int VAR_POER24A = 43;
  public static final int VAR_POER25 = 44;
  public static final int VAR_POER25A = 45;
  public static final int VAR_POER26 = 46;
  public static final int VAR_POER26A = 47;
  public static final int VAR_POER27 = 48;
  public static final int VAR_POER27A = 49;
  public static final int VAR_POER28 = 50;
  public static final int VAR_POER28A = 51;
  public static final int VAR_POER29 = 52;
  public static final int VAR_POER29A = 53;
  public static final int VAR_POARR = 54;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private String poopp = ""; // Table 20 options possibles
  private String pooppl = ""; // Table 20 libellés options
  private String poopp0 = ""; // Option proposée
  private String pooppl0 = ""; // Libellé option proposée
  private BigDecimal poer01 = BigDecimal.ZERO; // ALERTE POUR TYPE D'AVOIR
  private String poer01a = ""; // Type d'avoir
  private BigDecimal poer02 = BigDecimal.ZERO; // Franco garanti à la Cmde
  private BigDecimal poer03 = BigDecimal.ZERO; // Franco non atteint
  private BigDecimal poer03a = BigDecimal.ZERO; // Montant Franco
  private BigDecimal poer04 = BigDecimal.ZERO; // Poids Franco garanti/Cmde
  private BigDecimal poer05 = BigDecimal.ZERO; // Franco non atteint
  private BigDecimal poer05a = BigDecimal.ZERO; // Montant Franco
  private BigDecimal poer06 = BigDecimal.ZERO; // Minimum.Cmd non atteint
  private BigDecimal poer06a = BigDecimal.ZERO; // Minimum commande
  private BigDecimal poer07 = BigDecimal.ZERO; // Marge mini non atteinte
  private BigDecimal poer07a = BigDecimal.ZERO; // % Marge mini
  private BigDecimal poer08 = BigDecimal.ZERO; // Marge anormalement haute
  private BigDecimal poer08a = BigDecimal.ZERO; // % Marge Haute
  private BigDecimal poer09 = BigDecimal.ZERO; // Attn Facture différée
  private BigDecimal poer10 = BigDecimal.ZERO; // Existe situation juridique
  private BigDecimal poer11 = BigDecimal.ZERO; // Echéances dépassées
  private BigDecimal poer12 = BigDecimal.ZERO; // Stock non disponible
  private BigDecimal poer13 = BigDecimal.ZERO; // Aucune ligne livrable
  private BigDecimal poer14 = BigDecimal.ZERO; // Nombre de lignes en erreur
  private BigDecimal poer14a = BigDecimal.ZERO; // Nombre de lignes
  private BigDecimal poer15 = BigDecimal.ZERO; // Au - un lot non en stock
  private BigDecimal poer16 = BigDecimal.ZERO; // Plafond 1 dépassé pas le 2
  private String poer16a = ""; // message auto ou perso
  private BigDecimal poer17 = BigDecimal.ZERO; // Plafond dépassé
  private String poer17a = ""; // message auto ou perso
  private BigDecimal poer18 = BigDecimal.ZERO; // Acompte oblig. non reçu
  private BigDecimal poer19 = BigDecimal.ZERO; // Acompte obligatoire
  private BigDecimal poer19a = BigDecimal.ZERO; // % acompte / param."CC"
  private BigDecimal poer20 = BigDecimal.ZERO; // Mt.autorisé escompte dépassé
  private BigDecimal poer20a = BigDecimal.ZERO; // Mtt autorisé escompte CLVAE
  private BigDecimal poer21 = BigDecimal.ZERO; // Erreur de prix sur ligne
  private String poer21a = ""; // 1ère ligne en erreur
  private BigDecimal poer22 = BigDecimal.ZERO; // Double qté non saisie
  private String poer22a = ""; // 1ère ligne en erreur
  private BigDecimal poer23 = BigDecimal.ZERO; // Numéro de série incorrect
  private String poer23a = ""; // 1ère ligne en erreur
  private BigDecimal poer24 = BigDecimal.ZERO; // Numéro de lot incorrect
  private String poer24a = ""; // 1ère ligne en erreur
  private BigDecimal poer25 = BigDecimal.ZERO; // Adresse stock incorrecte
  private String poer25a = ""; // 1ère ligne en erreur
  private BigDecimal poer26 = BigDecimal.ZERO; // Poids supérieur à 100 tonnes
  private String poer26a = ""; // message auto ou perso
  private BigDecimal poer27 = BigDecimal.ZERO; // Ac. oblig. / art. spéciaux
  private BigDecimal poer27a = BigDecimal.ZERO; // % ac. sur cde /art. spéciaux
  private BigDecimal poer28 = BigDecimal.ZERO; // Prix de vente < PUMP
  private String poer28a = ""; // Code article
  private BigDecimal poer29 = BigDecimal.ZERO; // Prix de vente<Px de revient
  private String poer29a = ""; // Code article
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400Text(SIZE_POOPP), // Table 20 options possibles
      new AS400Text(SIZE_POOPPL), // Table 20 libellés options
      new AS400Text(SIZE_POOPP0), // Option proposée
      new AS400Text(SIZE_POOPPL0), // Libellé option proposée
      new AS400ZonedDecimal(SIZE_POER01, DECIMAL_POER01), // ALERTE POUR TYPE D'AVOIR
      new AS400Text(SIZE_POER01A), // Type d'avoir
      new AS400ZonedDecimal(SIZE_POER02, DECIMAL_POER02), // Franco garanti à la Cmde
      new AS400ZonedDecimal(SIZE_POER03, DECIMAL_POER03), // Franco non atteint
      new AS400ZonedDecimal(SIZE_POER03A, DECIMAL_POER03A), // Montant Franco
      new AS400ZonedDecimal(SIZE_POER04, DECIMAL_POER04), // Poids Franco garanti/Cmde
      new AS400ZonedDecimal(SIZE_POER05, DECIMAL_POER05), // Franco non atteint
      new AS400ZonedDecimal(SIZE_POER05A, DECIMAL_POER05A), // Montant Franco
      new AS400ZonedDecimal(SIZE_POER06, DECIMAL_POER06), // Minimum.Cmd non atteint
      new AS400ZonedDecimal(SIZE_POER06A, DECIMAL_POER06A), // Minimum commande
      new AS400ZonedDecimal(SIZE_POER07, DECIMAL_POER07), // Marge mini non atteinte
      new AS400ZonedDecimal(SIZE_POER07A, DECIMAL_POER07A), // % Marge mini
      new AS400ZonedDecimal(SIZE_POER08, DECIMAL_POER08), // Marge anormalement haute
      new AS400ZonedDecimal(SIZE_POER08A, DECIMAL_POER08A), // % Marge Haute
      new AS400ZonedDecimal(SIZE_POER09, DECIMAL_POER09), // Attn Facture différée
      new AS400ZonedDecimal(SIZE_POER10, DECIMAL_POER10), // Existe situation juridique
      new AS400ZonedDecimal(SIZE_POER11, DECIMAL_POER11), // Echéances dépassées
      new AS400ZonedDecimal(SIZE_POER12, DECIMAL_POER12), // Stock non disponible
      new AS400ZonedDecimal(SIZE_POER13, DECIMAL_POER13), // Aucune ligne livrable
      new AS400ZonedDecimal(SIZE_POER14, DECIMAL_POER14), // Nombre de lignes en erreur
      new AS400ZonedDecimal(SIZE_POER14A, DECIMAL_POER14A), // Nombre de lignes
      new AS400ZonedDecimal(SIZE_POER15, DECIMAL_POER15), // Au - un lot non en stock
      new AS400ZonedDecimal(SIZE_POER16, DECIMAL_POER16), // Plafond 1 dépassé pas le 2
      new AS400Text(SIZE_POER16A), // message auto ou perso
      new AS400ZonedDecimal(SIZE_POER17, DECIMAL_POER17), // Plafond dépassé
      new AS400Text(SIZE_POER17A), // message auto ou perso
      new AS400ZonedDecimal(SIZE_POER18, DECIMAL_POER18), // Acompte oblig. non reçu
      new AS400ZonedDecimal(SIZE_POER19, DECIMAL_POER19), // Acompte obligatoire
      new AS400ZonedDecimal(SIZE_POER19A, DECIMAL_POER19A), // % acompte / param."CC"
      new AS400ZonedDecimal(SIZE_POER20, DECIMAL_POER20), // Mt.autorisé escompte dépassé
      new AS400ZonedDecimal(SIZE_POER20A, DECIMAL_POER20A), // Mtt autorisé escompte CLVAE
      new AS400ZonedDecimal(SIZE_POER21, DECIMAL_POER21), // Erreur de prix sur ligne
      new AS400Text(SIZE_POER21A), // 1ère ligne en erreur
      new AS400ZonedDecimal(SIZE_POER22, DECIMAL_POER22), // Double qté non saisie
      new AS400Text(SIZE_POER22A), // 1ère ligne en erreur
      new AS400ZonedDecimal(SIZE_POER23, DECIMAL_POER23), // Numéro de série incorrect
      new AS400Text(SIZE_POER23A), // 1ère ligne en erreur
      new AS400ZonedDecimal(SIZE_POER24, DECIMAL_POER24), // Numéro de lot incorrect
      new AS400Text(SIZE_POER24A), // 1ère ligne en erreur
      new AS400ZonedDecimal(SIZE_POER25, DECIMAL_POER25), // Adresse stock incorrecte
      new AS400Text(SIZE_POER25A), // 1ère ligne en erreur
      new AS400ZonedDecimal(SIZE_POER26, DECIMAL_POER26), // Poids supérieur à 100 tonnes
      new AS400Text(SIZE_POER26A), // message auto ou perso
      new AS400ZonedDecimal(SIZE_POER27, DECIMAL_POER27), // Ac. oblig. / art. spéciaux
      new AS400ZonedDecimal(SIZE_POER27A, DECIMAL_POER27A), // % ac. sur cde /art. spéciaux
      new AS400ZonedDecimal(SIZE_POER28, DECIMAL_POER28), // Prix de vente < PUMP
      new AS400Text(SIZE_POER28A), // Code article
      new AS400ZonedDecimal(SIZE_POER29, DECIMAL_POER29), // Prix de vente<Px de revient
      new AS400Text(SIZE_POER29A), // Code article
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { poind, poopp, pooppl, poopp0, pooppl0, poer01, poer01a, poer02, poer03, poer03a, poer04, poer05, poer05a, poer06,
          poer06a, poer07, poer07a, poer08, poer08a, poer09, poer10, poer11, poer12, poer13, poer14, poer14a, poer15, poer16, poer16a,
          poer17, poer17a, poer18, poer19, poer19a, poer20, poer20a, poer21, poer21a, poer22, poer22a, poer23, poer23a, poer24, poer24a,
          poer25, poer25a, poer26, poer26a, poer27, poer27a, poer28, poer28a, poer29, poer29a, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    poopp = (String) output[1];
    pooppl = (String) output[2];
    poopp0 = (String) output[3];
    pooppl0 = (String) output[4];
    poer01 = (BigDecimal) output[5];
    poer01a = (String) output[6];
    poer02 = (BigDecimal) output[7];
    poer03 = (BigDecimal) output[8];
    poer03a = (BigDecimal) output[9];
    poer04 = (BigDecimal) output[10];
    poer05 = (BigDecimal) output[11];
    poer05a = (BigDecimal) output[12];
    poer06 = (BigDecimal) output[13];
    poer06a = (BigDecimal) output[14];
    poer07 = (BigDecimal) output[15];
    poer07a = (BigDecimal) output[16];
    poer08 = (BigDecimal) output[17];
    poer08a = (BigDecimal) output[18];
    poer09 = (BigDecimal) output[19];
    poer10 = (BigDecimal) output[20];
    poer11 = (BigDecimal) output[21];
    poer12 = (BigDecimal) output[22];
    poer13 = (BigDecimal) output[23];
    poer14 = (BigDecimal) output[24];
    poer14a = (BigDecimal) output[25];
    poer15 = (BigDecimal) output[26];
    poer16 = (BigDecimal) output[27];
    poer16a = (String) output[28];
    poer17 = (BigDecimal) output[29];
    poer17a = (String) output[30];
    poer18 = (BigDecimal) output[31];
    poer19 = (BigDecimal) output[32];
    poer19a = (BigDecimal) output[33];
    poer20 = (BigDecimal) output[34];
    poer20a = (BigDecimal) output[35];
    poer21 = (BigDecimal) output[36];
    poer21a = (String) output[37];
    poer22 = (BigDecimal) output[38];
    poer22a = (String) output[39];
    poer23 = (BigDecimal) output[40];
    poer23a = (String) output[41];
    poer24 = (BigDecimal) output[42];
    poer24a = (String) output[43];
    poer25 = (BigDecimal) output[44];
    poer25a = (String) output[45];
    poer26 = (BigDecimal) output[46];
    poer26a = (String) output[47];
    poer27 = (BigDecimal) output[48];
    poer27a = (BigDecimal) output[49];
    poer28 = (BigDecimal) output[50];
    poer28a = (String) output[51];
    poer29 = (BigDecimal) output[52];
    poer29a = (String) output[53];
    poarr = (String) output[54];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPoopp(String pPoopp) {
    if (pPoopp == null) {
      return;
    }
    poopp = pPoopp;
  }
  
  public String getPoopp() {
    return poopp;
  }
  
  public void setPooppl(String pPooppl) {
    if (pPooppl == null) {
      return;
    }
    pooppl = pPooppl;
  }
  
  public String getPooppl() {
    return pooppl;
  }
  
  public void setPoopp0(String pPoopp0) {
    if (pPoopp0 == null) {
      return;
    }
    poopp0 = pPoopp0;
  }
  
  public String getPoopp0() {
    return poopp0;
  }
  
  public void setPooppl0(String pPooppl0) {
    if (pPooppl0 == null) {
      return;
    }
    pooppl0 = pPooppl0;
  }
  
  public String getPooppl0() {
    return pooppl0;
  }
  
  public void setPoer01(BigDecimal pPoer01) {
    if (pPoer01 == null) {
      return;
    }
    poer01 = pPoer01.setScale(DECIMAL_POER01, RoundingMode.HALF_UP);
  }
  
  public void setPoer01(Integer pPoer01) {
    if (pPoer01 == null) {
      return;
    }
    poer01 = BigDecimal.valueOf(pPoer01);
  }
  
  public Integer getPoer01() {
    return poer01.intValue();
  }
  
  public void setPoer01a(Character pPoer01a) {
    if (pPoer01a == null) {
      return;
    }
    poer01a = String.valueOf(pPoer01a);
  }
  
  public Character getPoer01a() {
    return poer01a.charAt(0);
  }
  
  public void setPoer02(BigDecimal pPoer02) {
    if (pPoer02 == null) {
      return;
    }
    poer02 = pPoer02.setScale(DECIMAL_POER02, RoundingMode.HALF_UP);
  }
  
  public void setPoer02(Integer pPoer02) {
    if (pPoer02 == null) {
      return;
    }
    poer02 = BigDecimal.valueOf(pPoer02);
  }
  
  public Integer getPoer02() {
    return poer02.intValue();
  }
  
  public void setPoer03(BigDecimal pPoer03) {
    if (pPoer03 == null) {
      return;
    }
    poer03 = pPoer03.setScale(DECIMAL_POER03, RoundingMode.HALF_UP);
  }
  
  public void setPoer03(Integer pPoer03) {
    if (pPoer03 == null) {
      return;
    }
    poer03 = BigDecimal.valueOf(pPoer03);
  }
  
  public Integer getPoer03() {
    return poer03.intValue();
  }
  
  public void setPoer03a(BigDecimal pPoer03a) {
    if (pPoer03a == null) {
      return;
    }
    poer03a = pPoer03a.setScale(DECIMAL_POER03A, RoundingMode.HALF_UP);
  }
  
  public void setPoer03a(Integer pPoer03a) {
    if (pPoer03a == null) {
      return;
    }
    poer03a = BigDecimal.valueOf(pPoer03a);
  }
  
  public void setPoer03a(Date pPoer03a) {
    if (pPoer03a == null) {
      return;
    }
    poer03a = BigDecimal.valueOf(ConvertDate.dateToDb2(pPoer03a));
  }
  
  public Integer getPoer03a() {
    return poer03a.intValue();
  }
  
  public Date getPoer03aConvertiEnDate() {
    return ConvertDate.db2ToDate(poer03a.intValue(), null);
  }
  
  public void setPoer04(BigDecimal pPoer04) {
    if (pPoer04 == null) {
      return;
    }
    poer04 = pPoer04.setScale(DECIMAL_POER04, RoundingMode.HALF_UP);
  }
  
  public void setPoer04(Integer pPoer04) {
    if (pPoer04 == null) {
      return;
    }
    poer04 = BigDecimal.valueOf(pPoer04);
  }
  
  public Integer getPoer04() {
    return poer04.intValue();
  }
  
  public void setPoer05(BigDecimal pPoer05) {
    if (pPoer05 == null) {
      return;
    }
    poer05 = pPoer05.setScale(DECIMAL_POER05, RoundingMode.HALF_UP);
  }
  
  public void setPoer05(Integer pPoer05) {
    if (pPoer05 == null) {
      return;
    }
    poer05 = BigDecimal.valueOf(pPoer05);
  }
  
  public Integer getPoer05() {
    return poer05.intValue();
  }
  
  public void setPoer05a(BigDecimal pPoer05a) {
    if (pPoer05a == null) {
      return;
    }
    poer05a = pPoer05a.setScale(DECIMAL_POER05A, RoundingMode.HALF_UP);
  }
  
  public void setPoer05a(Integer pPoer05a) {
    if (pPoer05a == null) {
      return;
    }
    poer05a = BigDecimal.valueOf(pPoer05a);
  }
  
  public void setPoer05a(Date pPoer05a) {
    if (pPoer05a == null) {
      return;
    }
    poer05a = BigDecimal.valueOf(ConvertDate.dateToDb2(pPoer05a));
  }
  
  public Integer getPoer05a() {
    return poer05a.intValue();
  }
  
  public Date getPoer05aConvertiEnDate() {
    return ConvertDate.db2ToDate(poer05a.intValue(), null);
  }
  
  public void setPoer06(BigDecimal pPoer06) {
    if (pPoer06 == null) {
      return;
    }
    poer06 = pPoer06.setScale(DECIMAL_POER06, RoundingMode.HALF_UP);
  }
  
  public void setPoer06(Integer pPoer06) {
    if (pPoer06 == null) {
      return;
    }
    poer06 = BigDecimal.valueOf(pPoer06);
  }
  
  public Integer getPoer06() {
    return poer06.intValue();
  }
  
  public void setPoer06a(BigDecimal pPoer06a) {
    if (pPoer06a == null) {
      return;
    }
    poer06a = pPoer06a.setScale(DECIMAL_POER06A, RoundingMode.HALF_UP);
  }
  
  public void setPoer06a(Integer pPoer06a) {
    if (pPoer06a == null) {
      return;
    }
    poer06a = BigDecimal.valueOf(pPoer06a);
  }
  
  public Integer getPoer06a() {
    return poer06a.intValue();
  }
  
  public void setPoer07(BigDecimal pPoer07) {
    if (pPoer07 == null) {
      return;
    }
    poer07 = pPoer07.setScale(DECIMAL_POER07, RoundingMode.HALF_UP);
  }
  
  public void setPoer07(Integer pPoer07) {
    if (pPoer07 == null) {
      return;
    }
    poer07 = BigDecimal.valueOf(pPoer07);
  }
  
  public Integer getPoer07() {
    return poer07.intValue();
  }
  
  public void setPoer07a(BigDecimal pPoer07a) {
    if (pPoer07a == null) {
      return;
    }
    poer07a = pPoer07a.setScale(DECIMAL_POER07A, RoundingMode.HALF_UP);
  }
  
  public void setPoer07a(Integer pPoer07a) {
    if (pPoer07a == null) {
      return;
    }
    poer07a = BigDecimal.valueOf(pPoer07a);
  }
  
  public Integer getPoer07a() {
    return poer07a.intValue();
  }
  
  public void setPoer08(BigDecimal pPoer08) {
    if (pPoer08 == null) {
      return;
    }
    poer08 = pPoer08.setScale(DECIMAL_POER08, RoundingMode.HALF_UP);
  }
  
  public void setPoer08(Integer pPoer08) {
    if (pPoer08 == null) {
      return;
    }
    poer08 = BigDecimal.valueOf(pPoer08);
  }
  
  public Integer getPoer08() {
    return poer08.intValue();
  }
  
  public void setPoer08a(BigDecimal pPoer08a) {
    if (pPoer08a == null) {
      return;
    }
    poer08a = pPoer08a.setScale(DECIMAL_POER08A, RoundingMode.HALF_UP);
  }
  
  public void setPoer08a(Integer pPoer08a) {
    if (pPoer08a == null) {
      return;
    }
    poer08a = BigDecimal.valueOf(pPoer08a);
  }
  
  public Integer getPoer08a() {
    return poer08a.intValue();
  }
  
  public void setPoer09(BigDecimal pPoer09) {
    if (pPoer09 == null) {
      return;
    }
    poer09 = pPoer09.setScale(DECIMAL_POER09, RoundingMode.HALF_UP);
  }
  
  public void setPoer09(Integer pPoer09) {
    if (pPoer09 == null) {
      return;
    }
    poer09 = BigDecimal.valueOf(pPoer09);
  }
  
  public Integer getPoer09() {
    return poer09.intValue();
  }
  
  public void setPoer10(BigDecimal pPoer10) {
    if (pPoer10 == null) {
      return;
    }
    poer10 = pPoer10.setScale(DECIMAL_POER10, RoundingMode.HALF_UP);
  }
  
  public void setPoer10(Integer pPoer10) {
    if (pPoer10 == null) {
      return;
    }
    poer10 = BigDecimal.valueOf(pPoer10);
  }
  
  public Integer getPoer10() {
    return poer10.intValue();
  }
  
  public void setPoer11(BigDecimal pPoer11) {
    if (pPoer11 == null) {
      return;
    }
    poer11 = pPoer11.setScale(DECIMAL_POER11, RoundingMode.HALF_UP);
  }
  
  public void setPoer11(Integer pPoer11) {
    if (pPoer11 == null) {
      return;
    }
    poer11 = BigDecimal.valueOf(pPoer11);
  }
  
  public Integer getPoer11() {
    return poer11.intValue();
  }
  
  public void setPoer12(BigDecimal pPoer12) {
    if (pPoer12 == null) {
      return;
    }
    poer12 = pPoer12.setScale(DECIMAL_POER12, RoundingMode.HALF_UP);
  }
  
  public void setPoer12(Integer pPoer12) {
    if (pPoer12 == null) {
      return;
    }
    poer12 = BigDecimal.valueOf(pPoer12);
  }
  
  public Integer getPoer12() {
    return poer12.intValue();
  }
  
  public void setPoer13(BigDecimal pPoer13) {
    if (pPoer13 == null) {
      return;
    }
    poer13 = pPoer13.setScale(DECIMAL_POER13, RoundingMode.HALF_UP);
  }
  
  public void setPoer13(Integer pPoer13) {
    if (pPoer13 == null) {
      return;
    }
    poer13 = BigDecimal.valueOf(pPoer13);
  }
  
  public Integer getPoer13() {
    return poer13.intValue();
  }
  
  public void setPoer14(BigDecimal pPoer14) {
    if (pPoer14 == null) {
      return;
    }
    poer14 = pPoer14.setScale(DECIMAL_POER14, RoundingMode.HALF_UP);
  }
  
  public void setPoer14(Integer pPoer14) {
    if (pPoer14 == null) {
      return;
    }
    poer14 = BigDecimal.valueOf(pPoer14);
  }
  
  public Integer getPoer14() {
    return poer14.intValue();
  }
  
  public void setPoer14a(BigDecimal pPoer14a) {
    if (pPoer14a == null) {
      return;
    }
    poer14a = pPoer14a.setScale(DECIMAL_POER14A, RoundingMode.HALF_UP);
  }
  
  public void setPoer14a(Integer pPoer14a) {
    if (pPoer14a == null) {
      return;
    }
    poer14a = BigDecimal.valueOf(pPoer14a);
  }
  
  public Integer getPoer14a() {
    return poer14a.intValue();
  }
  
  public void setPoer15(BigDecimal pPoer15) {
    if (pPoer15 == null) {
      return;
    }
    poer15 = pPoer15.setScale(DECIMAL_POER15, RoundingMode.HALF_UP);
  }
  
  public void setPoer15(Integer pPoer15) {
    if (pPoer15 == null) {
      return;
    }
    poer15 = BigDecimal.valueOf(pPoer15);
  }
  
  public Integer getPoer15() {
    return poer15.intValue();
  }
  
  public void setPoer16(BigDecimal pPoer16) {
    if (pPoer16 == null) {
      return;
    }
    poer16 = pPoer16.setScale(DECIMAL_POER16, RoundingMode.HALF_UP);
  }
  
  public void setPoer16(Integer pPoer16) {
    if (pPoer16 == null) {
      return;
    }
    poer16 = BigDecimal.valueOf(pPoer16);
  }
  
  public Integer getPoer16() {
    return poer16.intValue();
  }
  
  public void setPoer16a(String pPoer16a) {
    if (pPoer16a == null) {
      return;
    }
    poer16a = pPoer16a;
  }
  
  public String getPoer16a() {
    return poer16a;
  }
  
  public void setPoer17(BigDecimal pPoer17) {
    if (pPoer17 == null) {
      return;
    }
    poer17 = pPoer17.setScale(DECIMAL_POER17, RoundingMode.HALF_UP);
  }
  
  public void setPoer17(Integer pPoer17) {
    if (pPoer17 == null) {
      return;
    }
    poer17 = BigDecimal.valueOf(pPoer17);
  }
  
  public Integer getPoer17() {
    return poer17.intValue();
  }
  
  public void setPoer17a(String pPoer17a) {
    if (pPoer17a == null) {
      return;
    }
    poer17a = pPoer17a;
  }
  
  public String getPoer17a() {
    return poer17a;
  }
  
  public void setPoer18(BigDecimal pPoer18) {
    if (pPoer18 == null) {
      return;
    }
    poer18 = pPoer18.setScale(DECIMAL_POER18, RoundingMode.HALF_UP);
  }
  
  public void setPoer18(Integer pPoer18) {
    if (pPoer18 == null) {
      return;
    }
    poer18 = BigDecimal.valueOf(pPoer18);
  }
  
  public Integer getPoer18() {
    return poer18.intValue();
  }
  
  public void setPoer19(BigDecimal pPoer19) {
    if (pPoer19 == null) {
      return;
    }
    poer19 = pPoer19.setScale(DECIMAL_POER19, RoundingMode.HALF_UP);
  }
  
  public void setPoer19(Integer pPoer19) {
    if (pPoer19 == null) {
      return;
    }
    poer19 = BigDecimal.valueOf(pPoer19);
  }
  
  public Integer getPoer19() {
    return poer19.intValue();
  }
  
  public void setPoer19a(BigDecimal pPoer19a) {
    if (pPoer19a == null) {
      return;
    }
    poer19a = pPoer19a.setScale(DECIMAL_POER19A, RoundingMode.HALF_UP);
  }
  
  public void setPoer19a(Double pPoer19a) {
    if (pPoer19a == null) {
      return;
    }
    poer19a = BigDecimal.valueOf(pPoer19a).setScale(DECIMAL_POER19A, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoer19a() {
    return poer19a.setScale(DECIMAL_POER19A, RoundingMode.HALF_UP);
  }
  
  public void setPoer20(BigDecimal pPoer20) {
    if (pPoer20 == null) {
      return;
    }
    poer20 = pPoer20.setScale(DECIMAL_POER20, RoundingMode.HALF_UP);
  }
  
  public void setPoer20(Integer pPoer20) {
    if (pPoer20 == null) {
      return;
    }
    poer20 = BigDecimal.valueOf(pPoer20);
  }
  
  public Integer getPoer20() {
    return poer20.intValue();
  }
  
  public void setPoer20a(BigDecimal pPoer20a) {
    if (pPoer20a == null) {
      return;
    }
    poer20a = pPoer20a.setScale(DECIMAL_POER20A, RoundingMode.HALF_UP);
  }
  
  public void setPoer20a(Integer pPoer20a) {
    if (pPoer20a == null) {
      return;
    }
    poer20a = BigDecimal.valueOf(pPoer20a);
  }
  
  public Integer getPoer20a() {
    return poer20a.intValue();
  }
  
  public void setPoer21(BigDecimal pPoer21) {
    if (pPoer21 == null) {
      return;
    }
    poer21 = pPoer21.setScale(DECIMAL_POER21, RoundingMode.HALF_UP);
  }
  
  public void setPoer21(Integer pPoer21) {
    if (pPoer21 == null) {
      return;
    }
    poer21 = BigDecimal.valueOf(pPoer21);
  }
  
  public Integer getPoer21() {
    return poer21.intValue();
  }
  
  public void setPoer21a(String pPoer21a) {
    if (pPoer21a == null) {
      return;
    }
    poer21a = pPoer21a;
  }
  
  public String getPoer21a() {
    return poer21a;
  }
  
  public void setPoer22(BigDecimal pPoer22) {
    if (pPoer22 == null) {
      return;
    }
    poer22 = pPoer22.setScale(DECIMAL_POER22, RoundingMode.HALF_UP);
  }
  
  public void setPoer22(Integer pPoer22) {
    if (pPoer22 == null) {
      return;
    }
    poer22 = BigDecimal.valueOf(pPoer22);
  }
  
  public Integer getPoer22() {
    return poer22.intValue();
  }
  
  public void setPoer22a(String pPoer22a) {
    if (pPoer22a == null) {
      return;
    }
    poer22a = pPoer22a;
  }
  
  public String getPoer22a() {
    return poer22a;
  }
  
  public void setPoer23(BigDecimal pPoer23) {
    if (pPoer23 == null) {
      return;
    }
    poer23 = pPoer23.setScale(DECIMAL_POER23, RoundingMode.HALF_UP);
  }
  
  public void setPoer23(Integer pPoer23) {
    if (pPoer23 == null) {
      return;
    }
    poer23 = BigDecimal.valueOf(pPoer23);
  }
  
  public Integer getPoer23() {
    return poer23.intValue();
  }
  
  public void setPoer23a(String pPoer23a) {
    if (pPoer23a == null) {
      return;
    }
    poer23a = pPoer23a;
  }
  
  public String getPoer23a() {
    return poer23a;
  }
  
  public void setPoer24(BigDecimal pPoer24) {
    if (pPoer24 == null) {
      return;
    }
    poer24 = pPoer24.setScale(DECIMAL_POER24, RoundingMode.HALF_UP);
  }
  
  public void setPoer24(Integer pPoer24) {
    if (pPoer24 == null) {
      return;
    }
    poer24 = BigDecimal.valueOf(pPoer24);
  }
  
  public Integer getPoer24() {
    return poer24.intValue();
  }
  
  public void setPoer24a(String pPoer24a) {
    if (pPoer24a == null) {
      return;
    }
    poer24a = pPoer24a;
  }
  
  public String getPoer24a() {
    return poer24a;
  }
  
  public void setPoer25(BigDecimal pPoer25) {
    if (pPoer25 == null) {
      return;
    }
    poer25 = pPoer25.setScale(DECIMAL_POER25, RoundingMode.HALF_UP);
  }
  
  public void setPoer25(Integer pPoer25) {
    if (pPoer25 == null) {
      return;
    }
    poer25 = BigDecimal.valueOf(pPoer25);
  }
  
  public Integer getPoer25() {
    return poer25.intValue();
  }
  
  public void setPoer25a(String pPoer25a) {
    if (pPoer25a == null) {
      return;
    }
    poer25a = pPoer25a;
  }
  
  public String getPoer25a() {
    return poer25a;
  }
  
  public void setPoer26(BigDecimal pPoer26) {
    if (pPoer26 == null) {
      return;
    }
    poer26 = pPoer26.setScale(DECIMAL_POER26, RoundingMode.HALF_UP);
  }
  
  public void setPoer26(Integer pPoer26) {
    if (pPoer26 == null) {
      return;
    }
    poer26 = BigDecimal.valueOf(pPoer26);
  }
  
  public Integer getPoer26() {
    return poer26.intValue();
  }
  
  public void setPoer26a(String pPoer26a) {
    if (pPoer26a == null) {
      return;
    }
    poer26a = pPoer26a;
  }
  
  public String getPoer26a() {
    return poer26a;
  }
  
  public void setPoer27(BigDecimal pPoer27) {
    if (pPoer27 == null) {
      return;
    }
    poer27 = pPoer27.setScale(DECIMAL_POER27, RoundingMode.HALF_UP);
  }
  
  public void setPoer27(Integer pPoer27) {
    if (pPoer27 == null) {
      return;
    }
    poer27 = BigDecimal.valueOf(pPoer27);
  }
  
  public Integer getPoer27() {
    return poer27.intValue();
  }
  
  public void setPoer27a(BigDecimal pPoer27a) {
    if (pPoer27a == null) {
      return;
    }
    poer27a = pPoer27a.setScale(DECIMAL_POER27A, RoundingMode.HALF_UP);
  }
  
  public void setPoer27a(Double pPoer27a) {
    if (pPoer27a == null) {
      return;
    }
    poer27a = BigDecimal.valueOf(pPoer27a).setScale(DECIMAL_POER27A, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoer27a() {
    return poer27a.setScale(DECIMAL_POER27A, RoundingMode.HALF_UP);
  }
  
  public void setPoer28(BigDecimal pPoer28) {
    if (pPoer28 == null) {
      return;
    }
    poer28 = pPoer28.setScale(DECIMAL_POER28, RoundingMode.HALF_UP);
  }
  
  public void setPoer28(Integer pPoer28) {
    if (pPoer28 == null) {
      return;
    }
    poer28 = BigDecimal.valueOf(pPoer28);
  }
  
  public Integer getPoer28() {
    return poer28.intValue();
  }
  
  public void setPoer28a(String pPoer28a) {
    if (pPoer28a == null) {
      return;
    }
    poer28a = pPoer28a;
  }
  
  public String getPoer28a() {
    return poer28a;
  }
  
  public void setPoer29(BigDecimal pPoer29) {
    if (pPoer29 == null) {
      return;
    }
    poer29 = pPoer29.setScale(DECIMAL_POER29, RoundingMode.HALF_UP);
  }
  
  public void setPoer29(Integer pPoer29) {
    if (pPoer29 == null) {
      return;
    }
    poer29 = BigDecimal.valueOf(pPoer29);
  }
  
  public Integer getPoer29() {
    return poer29.intValue();
  }
  
  public void setPoer29a(String pPoer29a) {
    if (pPoer29a == null) {
      return;
    }
    poer29a = pPoer29a;
  }
  
  public String getPoer29a() {
    return poer29a;
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
