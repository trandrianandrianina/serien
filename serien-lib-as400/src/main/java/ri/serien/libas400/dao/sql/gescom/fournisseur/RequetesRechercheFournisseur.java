/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.fournisseur;

import ri.serien.libcommun.commun.recherche.ArgumentRecherche;
import ri.serien.libcommun.commun.recherche.ListeArgumentRecherche;
import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;

public class RequetesRechercheFournisseur {
  
  /**
   * Retourne la requête en fonction du type de recherche.
   */
  public static String getRequete(String pCurlib, CritereFournisseur pCriteres) {
    String requete = null;
    if (pCriteres == null) {
      return requete;
    }
    
    switch (pCriteres.getTypeRecherche()) {
      case CritereFournisseur.RECHERCHER_TOUS_FOURNISSEURS_POUR_UN_ETABLISSEMENT:
        requete = "select frs.* from " + pCurlib + '.' + EnumTableBDD.FOURNISSEUR + " frs where FRETB = '"
            + pCriteres.getCodeEtablissement() + "' ORDER BY FRNOM ASC";
        break;
      case CritereFournisseur.RECHERCHER_ADRESSE_FOURNISSEURS_DOCUMENTACHAT:
        requete = getRequeteRechercheAdresseFournisseur(pCurlib, pCriteres);
        break;
      case CritereFournisseur.RECHERCHER_FOURNISSEURS_DOCUMENTACHAT:
        requete = getRequeteRechercheFournisseurAchat(pCurlib, pCriteres);
        break;
      case CritereFournisseur.RECHERCHER_TOUTES_ADRESSE_FOURNISSEURS:
        requete = getRequeteRechercheAdresseFournisseur(pCurlib, pCriteres);
        break;
    }
    
    return requete;
  }
  
  /**
   * Construit la requête spécifique recherche les adresses des fournisseurs.
   * Critères: raison sociale, clé de classement, le code, le code postal, la ville.
   */
  private static String getRequeteRechercheAdresseFournisseur(String curlib, CritereFournisseur criteres) {
    StringBuilder conditionFrs = new StringBuilder();
    StringBuilder conditionFre = new StringBuilder();
    RechercheGenerique rechercheGenerique = criteres.getRechercheGenerique();
    ListeArgumentRecherche listeArgumentRecherche = rechercheGenerique.getListeArgumentRecherche();
    if (listeArgumentRecherche != null) {
      for (ArgumentRecherche argumentRecherche : listeArgumentRecherche) {
        // On analyse le type des arguments
        switch (argumentRecherche.getType()) {
          // Conditions pour les arguments de type String
          case TEXTE:
          case EMAIL:
          case IDENTIFIANT:
          case DECIMAL:
          case TELEPHONE:
            String expression = argumentRecherche.getValeur();
            // Conditions pour la table fournissseur
            conditionFrs.append(" and (")
                .append("TRANSLATE(UPPER(CONCAT(TRIM(FRNOM), TRIM(FRCPL))), 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU',"
                    + " 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ') like TRANSLATE('%")
                .append(expression)
                .append("%', 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')")
                .append(" or UPPER(FRCLA) like '").append(expression).append("%'").append(" or FRVIL like '").append(expression)
                .append("%'").append(")");
            // Conditions pour la table des extensions fournisseur
            conditionFre.append(" and (")
                .append("TRANSLATE(UPPER(CONCAT(TRIM(FENOM), TRIM(FECPL))), 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU',"
                    + " 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ') like TRANSLATE('%")
                .append(expression)
                .append("%', 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')")
                .append(" or FEVIL like '").append(expression).append("%'").append(")");
            break;
          // Conditions pour les arguments de type Numérique
          
          case ENTIER:
          case CODE_POSTAL:
            // Conditions pour la table fournissseur
            conditionFrs.append(" and (").append(" cast(FRFRS as char(6)) like '").append(argumentRecherche.getValeur()).append("%'")
                .append(" or cast(FRCDP as char(5)) like '").append(argumentRecherche.getValeur()).append("%'").append(")");
            // Conditions pour la table des extensions fournisseur
            conditionFre.append(" and (").append(" cast(FEFRS as char(6)) like '").append(argumentRecherche.getValeur()).append("%'")
                .append(" or cast(FECDP as char(5)) like '").append(argumentRecherche.getValeur()).append("%'").append(")");
            break;
          
          case IGNORE:
            break;
        }
      }
    }
    
    // Construction de la requête finale
    String requete = "select FRETB, FRCOL, FRFRS, 0 as indice from " + curlib + '.' + EnumTableBDD.FOURNISSEUR
        + " where FRETB = '" + criteres.getCodeEtablissement() + "' and FRTOP <> 9 " + conditionFrs.toString() + " union"
        + " select FEETB, FECOL, FEFRS, FEEXT as indice from " + curlib + '.' + EnumTableBDD.FOURNISSEUR_ADRESSE
        + " where FEETB = '" + criteres.getCodeEtablissement() + "' and FETOP <> 9  and FEFAC = ' ' and FEEXP = ' '"
        + conditionFre.toString();
    
    if (criteres.getTypeAdresseFournisseur() != null) {
      switch (criteres.getTypeAdresseFournisseur()) {
        case SIEGE:
          requete = "select FRETB, FRCOL, FRFRS, 0 as indice from " + curlib + '.' + EnumTableBDD.FOURNISSEUR
              + " where FRETB = '" + criteres.getCodeEtablissement() + "' and FRTOP <> 9 " + conditionFrs.toString();
          break;
        
        case COMMANDE:
          requete = " select FEETB as FRETB, FECOL as FRCOL, FEFRS as FRFRS, FEEXT as indice from " + curlib + '.'
              + EnumTableBDD.FOURNISSEUR_ADRESSE + " where FEETB = '" + criteres.getCodeEtablissement()
              + "' and FETOP <> 9 " + conditionFre.toString() + " and FECMD = '1'";
          break;
        
        case FACTURATION:
          requete = " select FEETB as FRETB, FECOL as FRCOL, FEFRS as FRFRS, FEEXT as indice from " + curlib + '.'
              + EnumTableBDD.FOURNISSEUR_ADRESSE + " where FEETB = '" + criteres.getCodeEtablissement()
              + "' and FETOP <> 9 " + conditionFre.toString() + " and FEFAC = '1'";
          break;
        
        case EXPEDITION:
          requete = " select FEETB as FRETB, FECOL as FRCOL, FEFRS as FRFRS, FEEXT as indice from " + curlib + '.'
              + EnumTableBDD.FOURNISSEUR_ADRESSE + " where FEETB = '" + criteres.getCodeEtablissement()
              + "' and FETOP <> 9 " + conditionFre.toString() + " and FEEXP = '1'";
          break;
        
        default:
          break;
      }
    }
    
    return requete;
  }
  
  /**
   * Construit la requête spécifique pour les achats.
   * Critères: raison sociale, clé de classement, le code, le code postal, la ville.
   * Pour l'instant on ne traite que le fichier fournisseur donc les sièges, les agences seront traitées plus tard qd ce sera plus
   * clair...
   */
  private static String getRequeteRechercheFournisseurAchat(String curlib, CritereFournisseur criteres) {
    StringBuilder conditions = new StringBuilder();
    RechercheGenerique rechercheGenerique = criteres.getRechercheGenerique();
    ListeArgumentRecherche listeArgumentRecherches = rechercheGenerique.getListeArgumentRecherche();
    if (listeArgumentRecherches != null) {
      for (ArgumentRecherche argument : listeArgumentRecherches) {
        // On analyse le type des arguments
        switch (argument.getType()) {
          case TEXTE:
          case EMAIL:
          case IDENTIFIANT:
          case DECIMAL:
          case TELEPHONE:
            String expression = argument.getValeur();
            conditions.append(" and (")
                .append("TRANSLATE(UPPER(CONCAT(TRIM(frs.FRNOM), TRIM(frs.FRCPL))), 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU',"
                    + " 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ') like TRANSLATE('%")
                .append(expression)
                .append("%', 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')")
                .append(" or UPPER(frs.FRCLA) like '").append(expression).append("%'").append(" or frs.FRVIL like '").append(expression)
                .append("%'").append(" or ")
                .append("TRANSLATE(UPPER(CONCAT(TRIM(fre.FENOM), TRIM(fre.FECPL))), 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU',"
                    + " 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ') like TRANSLATE('%")
                .append(expression)
                .append("%', 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')")
                .append(" or fre.FEVIL like '").append(expression).append("%'").append(")");
            break;
          
          case ENTIER:
          case CODE_POSTAL:
            conditions.append(" and (").append(" cast(frs.FRFRS as char(6)) like '").append(argument.getValeur()).append("%'")
                .append(" or cast(frs.FRCDP as char(5)) like '").append(argument.getValeur()).append("%'")
                .append(" or cast(fre.FECDP as char(5)) like '").append(argument.getValeur()).append("%'").append(")");
            break;
          
          case IGNORE:
            break;
        }
      }
    }
    
    // Construction de la requête finale
    // On ne prend pas les fournisseurs et les bloc adresses annulés
    String requete = "select row_number() over() as numrow, frs.FRETB, frs.FRCOL, frs.FRFRS, frs.FRNOM, frs.FRCPL, frs.FRCDP, frs.FRVIL,"
        + " fre.FEEXT, fre.FENOM, fre.FECPL, fre.FECDP, fre.FEVIL" + " from " + curlib + '.' + EnumTableBDD.FOURNISSEUR + " frs"
        + " left join " + curlib + '.' + EnumTableBDD.FOURNISSEUR_ADRESSE + " fre"
        + " on frs.fretb = fre.feetb and  frs.frcol = fre.fecol and frs.frfrs = fre.fefrs " + " where frs.FRETB = '"
        + criteres.getCodeEtablissement() + "'" + " and frs.FRTOP <> 9 and (fre.FETOP is null or fre.FETOP <> 9)";
    if (conditions.length() > 0) {
      requete += conditions.toString();
    }
    return requete += " order by frs.FRNOM, frs.FRVIL, frs.FRFRS";
  }
  
}
