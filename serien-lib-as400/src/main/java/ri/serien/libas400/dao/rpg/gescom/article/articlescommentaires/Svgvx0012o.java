/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlescommentaires;

import java.beans.PropertyVetoException;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvx0012o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POLB1 = 30;
  public static final int SIZE_POLB2 = 30;
  public static final int SIZE_POLB3 = 30;
  public static final int SIZE_POLB4 = 30;
  public static final int SIZE_POEDT1 = 1;
  public static final int SIZE_POEDT2 = 1;
  public static final int SIZE_POEDT3 = 1;
  public static final int SIZE_POEDT4 = 1;
  public static final int SIZE_POEDT5 = 1;
  public static final int SIZE_POEDT6 = 1;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 137;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POLB1 = 1;
  public static final int VAR_POLB2 = 2;
  public static final int VAR_POLB3 = 3;
  public static final int VAR_POLB4 = 4;
  public static final int VAR_POEDT1 = 5;
  public static final int VAR_POEDT2 = 6;
  public static final int VAR_POEDT3 = 7;
  public static final int VAR_POEDT4 = 8;
  public static final int VAR_POEDT5 = 9;
  public static final int VAR_POEDT6 = 10;
  public static final int VAR_POARR = 11;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private String polb1 = ""; // Libelle n° 1
  private String polb2 = ""; // Libelle n° 2
  private String polb3 = ""; // Libellé n° 3
  private String polb4 = ""; // Libellé n° 4
  private String poedt1 = ""; // Edition sur Bon de prépa
  private String poedt2 = ""; // Accusé de Réception de cde
  private String poedt3 = ""; // Bon d"expédition
  private String poedt4 = ""; // Bon transporteur
  private String poedt5 = ""; // Facture
  private String poedt6 = ""; // Devis
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400Text(SIZE_POLB1), // Libelle n° 1
      new AS400Text(SIZE_POLB2), // Libelle n° 2
      new AS400Text(SIZE_POLB3), // Libellé n° 3
      new AS400Text(SIZE_POLB4), // Libellé n° 4
      new AS400Text(SIZE_POEDT1), // Edition sur Bon de prépa
      new AS400Text(SIZE_POEDT2), // Accusé de Réception de cde
      new AS400Text(SIZE_POEDT3), // Bon d"expédition
      new AS400Text(SIZE_POEDT4), // Bon transporteur
      new AS400Text(SIZE_POEDT5), // Facture
      new AS400Text(SIZE_POEDT6), // Devis
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, polb1, polb2, polb3, polb4, poedt1, poedt2, poedt3, poedt4, poedt5, poedt6, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    polb1 = (String) output[1];
    polb2 = (String) output[2];
    polb3 = (String) output[3];
    polb4 = (String) output[4];
    poedt1 = (String) output[5];
    poedt2 = (String) output[6];
    poedt3 = (String) output[7];
    poedt4 = (String) output[8];
    poedt5 = (String) output[9];
    poedt6 = (String) output[10];
    poarr = (String) output[11];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPolb1(String pPolb1) {
    if (pPolb1 == null) {
      return;
    }
    polb1 = pPolb1;
  }
  
  public String getPolb1() {
    return polb1;
  }
  
  public void setPolb2(String pPolb2) {
    if (pPolb2 == null) {
      return;
    }
    polb2 = pPolb2;
  }
  
  public String getPolb2() {
    return polb2;
  }
  
  public void setPolb3(String pPolb3) {
    if (pPolb3 == null) {
      return;
    }
    polb3 = pPolb3;
  }
  
  public String getPolb3() {
    return polb3;
  }
  
  public void setPolb4(String pPolb4) {
    if (pPolb4 == null) {
      return;
    }
    polb4 = pPolb4;
  }
  
  public String getPolb4() {
    return polb4;
  }
  
  public void setPoedt1(Character pPoedt1) {
    if (pPoedt1 == null) {
      return;
    }
    poedt1 = String.valueOf(pPoedt1);
  }
  
  public Character getPoedt1() {
    return poedt1.charAt(0);
  }
  
  public void setPoedt2(Character pPoedt2) {
    if (pPoedt2 == null) {
      return;
    }
    poedt2 = String.valueOf(pPoedt2);
  }
  
  public Character getPoedt2() {
    return poedt2.charAt(0);
  }
  
  public void setPoedt3(Character pPoedt3) {
    if (pPoedt3 == null) {
      return;
    }
    poedt3 = String.valueOf(pPoedt3);
  }
  
  public Character getPoedt3() {
    return poedt3.charAt(0);
  }
  
  public void setPoedt4(Character pPoedt4) {
    if (pPoedt4 == null) {
      return;
    }
    poedt4 = String.valueOf(pPoedt4);
  }
  
  public Character getPoedt4() {
    return poedt4.charAt(0);
  }
  
  public void setPoedt5(Character pPoedt5) {
    if (pPoedt5 == null) {
      return;
    }
    poedt5 = String.valueOf(pPoedt5);
  }
  
  public Character getPoedt5() {
    return poedt5.charAt(0);
  }
  
  public void setPoedt6(Character pPoedt6) {
    if (pPoedt6 == null) {
      return;
    }
    poedt6 = String.valueOf(pPoedt6);
  }
  
  public Character getPoedt6() {
    return poedt6.charAt(0);
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
