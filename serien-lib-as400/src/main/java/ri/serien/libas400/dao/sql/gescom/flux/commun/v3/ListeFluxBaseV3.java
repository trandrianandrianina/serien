/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.exploitation.personnalisation.flux.PersonnalisationFlux;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

/**
 * Cette classe permet de gérer une liste de flux V3 afin de générer un fichier JSON et son transfert.
 */
public abstract class ListeFluxBaseV3<J> {
  // Constantes
  private static final String DOSSIER_TEMPORAIRE = "/tmp/flux";
  private static final int LONGUEUR_VERSION = 1;
  private static final int LONGUEUR_CURLIB = 10;
  private static final int LONGUEUR_CODFTP = 5;
  private static final int NOMBRE_MAX_ELEMENT = 500;
  
  // Variables
  private transient List<J> listeFlux = null;
  private transient EnumTypeFlux enumTypeFlux = null;
  private transient String dossierTemporaire = null;
  private transient String prefixeNomFichier = null;
  private transient AS400 systeme = null;
  private transient PersonnalisationFlux personnalisationFlux = null;
  private transient Bibliotheque baseDeDonnees = null;
  
  /**
   * Constructeur.
   */
  public ListeFluxBaseV3(EnumTypeFlux pEnumFlux, String pPrefixeNomFichier) {
    if (EnvironnementExecution.getLettreEnvironnement() == null || EnvironnementExecution.getLettreEnvironnement().charValue() == ' ') {
      throw new MessageErreurException("La lettre d'environnement est invalide.");
    }
    
    enumTypeFlux = pEnumFlux;
    dossierTemporaire = DOSSIER_TEMPORAIRE + File.separator + enumTypeFlux.getCode() + File.separator
        + EnvironnementExecution.getLettreEnvironnement().charValue();
    prefixeNomFichier = pPrefixeNomFichier;
  }
  
  // -- Méthodes publiques
  
  /**
   * Ajoute un flux sous forme de chaine à la liste.
   */
  public void ajouterFlux(J pFluxJson) {
    if (pFluxJson == null) {
      Trace.alerte("Le " + enumTypeFlux.getLibelle().toLowerCase() + " sous forme de chaine est invalide.");
      return;
    }
    
    // Contrôle que le code ne soit pas en double (FLX003 code document, FLX004 numéro client et livré)
    // Si c'est le cas l'enregistrement existant est écrasé par le nouveau
    int indice = controlerExistenceElement(pFluxJson);
    if (indice != -1) {
      listeFlux.set(indice, pFluxJson);
    }
    else {
      listeFlux.add(pFluxJson);
    }
  }
  
  /**
   * Retourne si la liste est vide.
   */
  public boolean isEmpty() {
    return listeFlux.isEmpty();
  }
  
  /**
   * Initialise toutes les variables.
   */
  public void initialiser() {
    listeFlux.clear();
    systeme = null;
    personnalisationFlux = null;
    baseDeDonnees = null;
  }
  
  /**
   * Transfère le fichier JSON vers le serveur adéquat.
   */
  public abstract void transfererVersServeur();
  
  // -- Méthodes protégées
  
  /**
   * Contrôle que le code de l'élément à insérer n'existe pas déjà dans la liste (doublon).
   * Retourne l'indice du doublon sinon -1.
   */
  protected abstract int controlerExistenceElement(J pFluxJson);
  
  /**
   * Transfère le fichier JSON vers le serveur adéquat.
   * Le fichier contient au maximum 500 éléments. Il y aura donc autant de fichier que nécéssaire.
   */
  protected void transfererVersServeur(Object pObjet) {
    // Contrôle s'il y a plus de 500 enregistrements dans la liste
    List<J> listeFluxStockage = null;
    if (listeFlux.size() > NOMBRE_MAX_ELEMENT) {
      listeFluxStockage = new ArrayList<J>();
      // Stockage du surplus dans une autre liste
      for (int i = NOMBRE_MAX_ELEMENT; i < listeFlux.size(); i++) {
        listeFluxStockage.add(listeFlux.get(i));
      }
      // Suppression du surplus dans la liste originale
      int nombreElementASupprimer = listeFlux.size() - NOMBRE_MAX_ELEMENT;
      for (int i = 0; i < nombreElementASupprimer; i++) {
        listeFlux.remove(listeFlux.size() - 1);
      }
    }
    List<File> listeFichierATransferer = new ArrayList<File>();
    do {
      // Sérialisation
      String chaineJson = convertirEnJson(pObjet);
      if (chaineJson == null) {
        Trace.erreur("La chaine Json de la liste " + enumTypeFlux.getLibelle().toLowerCase() + " est invalide.");
        return;
      }
      
      // Modifier le nom du champ idInstanceMagento en idInstance (Provisoire)
      chaineJson = chaineJson.replaceAll("idInstanceMagento", "idInstance");
      
      // Construire le nom du fichier
      String nomFichier = construireNomFichier(null);
      File cheminComplet = new File(dossierTemporaire + File.separator + nomFichier);
      cheminComplet = controlerExistenceNomFichier(cheminComplet);
      listeFichierATransferer.add(cheminComplet);
      
      // Générer le fichier JSON dans un dossier tmp
      GestionFichierTexte gft = new GestionFichierTexte(cheminComplet);
      gft.setForceEncodage(GestionFichierTexte.ENCODAGE_UTF8);
      gft.setContenuFichier(chaineJson);
      gft.ecritureFichier();
      
      // Contrôle du surplus
      if (listeFluxStockage != null) {
        // Il reste encore plus de 500 enregistrements
        if (listeFluxStockage.size() > NOMBRE_MAX_ELEMENT) {
          listeFlux.clear();
          for (int i = 0; i < NOMBRE_MAX_ELEMENT; i++) {
            listeFlux.add(listeFluxStockage.get(i));
          }
          // Suppression des éléments copiés dans la liste de stockage
          for (int i = 0; i < NOMBRE_MAX_ELEMENT; i++) {
            listeFluxStockage.remove(0);
          }
        }
        // La liste de stockage est vide
        else if (listeFluxStockage.isEmpty()) {
          listeFluxStockage = null;
        }
        // Il reste moins de 500 enregistrements
        else {
          listeFlux.clear();
          listeFlux.addAll(listeFluxStockage);
          listeFluxStockage.clear();
        }
      }
    }
    // Contrôler s'il reste des élements à traiter dans la liste de stockage
    while (listeFluxStockage != null);
    
    // Transfert des fichiers
    try {
      
      // Contrôle des variables
      if (EnvironnementExecution.getLettreEnvironnement() == null) {
        throw new MessageErreurException("La lettre d'environnement est invalide.");
      }
      if (baseDeDonnees == null) {
        throw new MessageErreurException("La base de données est invalide.");
      }
      if (systeme == null) {
        throw new MessageErreurException("Le système est invalide.");
      }
      if (personnalisationFlux == null) {
        throw new MessageErreurException("La personnalisation du " + enumTypeFlux.getLibelle().toLowerCase() + " est invalide.");
      }
      if (Constantes.normerTexte(personnalisationFlux.getCodeFTP()).isEmpty()) {
        throw new MessageErreurException(
            "Le code FTP de la personnalisation du " + enumTypeFlux.getLibelle().toLowerCase() + " n'est pas renseigné.");
      }
      
      // Trace de débug
      Trace.debug("--> lettre : " + EnvironnementExecution.getLettreEnvironnement() + " curlib : " + baseDeDonnees.getNom() + " codftp : "
          + personnalisationFlux.getCodeFTP() + " expas  :" + EnvironnementExecution.getExpas());
      
      // Transférer le fichier vers le serveur sftp
      ProgramParameter[] listeParametre = new ProgramParameter[3];
      AS400Text lettre = new AS400Text(LONGUEUR_VERSION);
      listeParametre[0] =
          new ProgramParameter(lettre.toBytes(EnvironnementExecution.getLettreEnvironnement().toString()), LONGUEUR_VERSION);
      AS400Text curlib = new AS400Text(LONGUEUR_CURLIB);
      listeParametre[1] = new ProgramParameter(curlib.toBytes(baseDeDonnees.getNom()), LONGUEUR_CURLIB);
      AS400Text codftp = new AS400Text(LONGUEUR_CODFTP);
      listeParametre[2] = new ProgramParameter(codftp.toBytes(personnalisationFlux.getCodeFTP()), LONGUEUR_CODFTP);
      
      ContexteProgrammeRPG programme = new ContexteProgrammeRPG(systeme, baseDeDonnees);
      boolean retour = programme.executerProgramme("SEXP74CL", EnvironnementExecution.getExpas(), listeParametre);
      
      // Contrôle l'existence des fichiers en local
      boolean fichierLocalTrouve = false;
      String nomFichier = "";
      for (File fichier : listeFichierATransferer) {
        nomFichier += fichier.getAbsolutePath() + " ";
        if (fichier.exists()) {
          fichierLocalTrouve = true;
        }
      }
      // Si le transfert c'est bien passé
      if (retour && !fichierLocalTrouve) {
        Trace.info("Transfert des fichiers " + nomFichier + " réussit.");
      }
      else {
        Trace.info("Transfert des fichiers " + nomFichier + " en échec.");
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du transfert des " + enumTypeFlux.getLibelle().toLowerCase() + " vers le serveur.");
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Converti l'ojet liste en chaine JSON.
   */
  private String convertirEnJson(Object pObjet) {
    try {
      GsonBuilder builderJSON = new GsonBuilder();
      Gson gson = builderJSON.create();
      return gson.toJson(pObjet);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la conversion en JSON.");
    }
    return null;
  }
  
  /**
   * Construit le nom du fichier au format FLX???_XXXX_JJMMAAAAHHMMSS.json.
   */
  /**
   * Construit le nom du fichier au format FLX006_Inventory_JJMMAAAAHHMMSS.json.
   */
  private String construireNomFichier(Date pDate) {
    if (pDate == null) {
      pDate = new Date();
    }
    String jour = DateHeure.getJourHeure(6, pDate);
    String heure = DateHeure.getJourHeure(8, pDate);
    return prefixeNomFichier + jour + heure + ".json";
  }
  
  /**
   * Contrôle l'existence du nom du fichier.
   * Si c'est le cas le nom est modifié puis retrourné.
   */
  private File controlerExistenceNomFichier(File pCheminComplet) {
    if (!pCheminComplet.exists()) {
      return pCheminComplet;
    }
    
    String nomDossier = pCheminComplet.getParent();
    Calendar calendar = Calendar.getInstance();
    // Incrément d'une seconde à chaque contrôle afin de trouver un nom libre
    do {
      calendar.add(Calendar.SECOND, 1);
      String nomFichier = construireNomFichier(calendar.getTime());
      pCheminComplet = new File(nomDossier + File.separator + nomFichier);
    }
    while (pCheminComplet.exists());
    return pCheminComplet;
  }
  
  // -- Accesseurs
  
  /**
   * Initialise la liste des flux.
   */
  public void setListeFlux(List<J> listeFlux) {
    this.listeFlux = listeFlux;
  }
  
  /**
   * Initialise le système.
   */
  public void setSysteme(AS400 pSysteme) {
    this.systeme = pSysteme;
  }
  
  /**
   * Retourne la personnalisation du flux.
   */
  public PersonnalisationFlux getPersonnalisationFlux() {
    return personnalisationFlux;
  }
  
  /**
   * Initialisation de la personnalisation du flux.
   */
  public void setPersonnalisationFlux(PersonnalisationFlux personnalisationFlux) {
    this.personnalisationFlux = personnalisationFlux;
  }
  
  /**
   * Retourne la base de données.
   */
  public Bibliotheque getBaseDeDonnees() {
    return baseDeDonnees;
  }
  
  /**
   * Initialise la base de données.
   */
  public void setBaseDeDonnees(Bibliotheque baseDeDonnees) {
    this.baseDeDonnees = baseDeDonnees;
  }
  
  /**
   * Retourne si les variables sont initialisées.
   */
  public boolean isInitialiser() {
    if (baseDeDonnees == null || personnalisationFlux == null || systeme == null) {
      return false;
    }
    return true;
  }
  
}
