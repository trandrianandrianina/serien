/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlescommentaires;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgSauverArticleCommentaire extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0011";
  private static final int NEGOCATION_ACHAT = 1;
  
  /**
   * Appel du programme RPG qui va créer un article commentaire.
   */
  public void sauverArticleCommentaire(SystemeManager pSysteme, Article pArticle) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pArticle == null) {
      throw new MessageErreurException("Le paramètre pArticle du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvx0011i entree = new Svgvx0011i();
    Svgvx0011o sortie = new Svgvx0011o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pArticle.getId().getCodeEtablissement()); // Code établissement
    entree.setPiart(pArticle.getId().getCodeArticle()); // Code article
    entree.setPilb1(pArticle.getLibelle1()); // Libellé 1
    entree.setPilb2(pArticle.getLibelle2()); // Libellé 2
    entree.setPilb3(pArticle.getLibelle3()); // Libellé 3
    entree.setPilb4(pArticle.getLibelle4()); // Libellé 4
    if (pArticle.isTopEditionBonPreparation()) {
      entree.setPiedt1('1');
    }
    else {
      entree.setPiedt1(' ');
    }
    if (pArticle.isTopAccuseReceptionCommande()) {
      entree.setPiedt2('1');
    }
    else {
      entree.setPiedt2(' ');
    }
    
    if (pArticle.isTopBonExpedition()) {
      entree.setPiedt3('1');
    }
    else {
      entree.setPiedt3(' ');
    }
    if (pArticle.isTopEditionBonTransporteur()) {
      entree.setPiedt4('1');
    }
    else {
      entree.setPiedt4(' ');
    }
    if (pArticle.isTopFacture()) {
      entree.setPiedt5('1');
    }
    else {
      entree.setPiedt5(' ');
    }
    
    if (pArticle.isTopDevis()) {
      entree.setPiedt6('1');
    }
    else {
      entree.setPiedt6(' ');
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
}
