/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.negociation;

import java.math.MathContext;
import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.conditionachat.EnumTypeConditionAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgChargerNegociationAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0020";
  
  /**
   * Appel du programme RPG qui va lire une négociation pour une ligne d'achat.
   */
  public ConditionAchat chargerNegociationAchat(SystemeManager pSysteme, LigneAchat pLigneAchat) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdLigneAchat idLigneAchat = pLigneAchat.getId();
    IdEtablissement idEtablissement = pLigneAchat.getId().getIdEtablissement();
    
    ConditionAchat cna = null;
    
    // Préparation des paramètres du programme RPG
    Svgvx0020i entree = new Svgvx0020i();
    Svgvx0020o sortie = new Svgvx0020o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(idEtablissement.getCodeEtablissement());
    entree.setPicod(idLigneAchat.getCodeEntete().getCode());
    entree.setPinum(idLigneAchat.getNumero());
    entree.setPisuf(idLigneAchat.getSuffixe());
    entree.setPinli(idLigneAchat.getNumeroLigne());
    entree.setPidat(ConvertDate.dateToDb2(new Date()));
    entree.setPityp(EnumTypeConditionAchat.CONDITION_ACHAT.getCode());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      cna = new ConditionAchat();
      
      // Entrée
      cna.setIdEtablissement(idEtablissement); // Etablissement de GVM
      cna.setCodeERL(idLigneAchat.getCodeEntete().getCode()); // Code ERL
      cna.setNumeroBon(idLigneAchat.getNumero()); // Numéro du bon
      cna.setSuffixeBon(idLigneAchat.getSuffixe()); // Suffixe du bon
      cna.setNumeroLigne(idLigneAchat.getNumeroLigne()); // Numéro de ligne
      cna.setIdArticle(pLigneAchat.getIdArticle()); // Code article
      
      // Sortie
      cna.setDateCreation(ConvertDate.db2ToDate(sortie.getNacre().intValue(), null)); // date de création
      cna.setDateTraitement(ConvertDate.db2ToDate(sortie.getNatrt().intValue(), null)); // date de traitement
      cna.setDateModification(ConvertDate.db2ToDate(sortie.getNamod().intValue(), null)); // date de modification
      cna.setDateApplication(ConvertDate.db2ToDate(sortie.getNadap().intValue(), null)); // date d'application
      cna.setTypeCondition(EnumTypeConditionAchat.valueOfByCode(sortie.getNatyp())); // Type de condition
      IdFournisseur idFournisseur =
          IdFournisseur.getInstance(idEtablissement, sortie.getNacol().intValue(), sortie.getNafrs().intValue());
      cna.setIdFournisseur(idFournisseur);
      cna.setTopCalculPrixVente(sortie.getNacpv()); // Top calcul prix de vente
      cna.initialiser(sortie.getNapra(), sortie.getNaprnet(), sortie.getNaprprf(), sortie.getNaprprs(), sortie.getNarem1(),
          sortie.getNarem2(), sortie.getNarem3(), sortie.getNarem4(), sortie.getNakpr(), sortie.getNafk3(), sortie.getNafv3(),
          sortie.getNafv1(), sortie.getNafk1(), sortie.getNafp1(), sortie.getNafp2());
      
      cna.setPourcentageRemise5(sortie.getNarem5().round(MathContext.DECIMAL32)); // Remise 5
      cna.setPourcentageRemise6(sortie.getNarem6().round(MathContext.DECIMAL32)); // Remise 6
      if (!sortie.getNauna().trim().isEmpty()) {
        IdUnite idUA = IdUnite.getInstance(sortie.getNauna());
        cna.setIdUniteAchat(idUA); // Unité d'achat
      }
      cna.setConditionnement(sortie.getNanua()); // Conditionnement
      cna.setCoefficientUAparUCA(sortie.getNakac()); // Nombre unité achat/unité Cde
      cna.setDelaiLivraison((sortie.getNadel())); // Délai de livraison
      cna.setReferenceFournisseur((sortie.getNaref())); // Référence fournisseur
      cna.setUniteCommande(sortie.getNaunc()); // Unité de commande
      cna.setCoefficientUSparUCA(sortie.getNaksc()); // Nombre unité Stk/unité Cde
      cna.setCodeDevise((sortie.getNadev())); // Code devise
      cna.setQuantiteMinimumCommande(sortie.getNaqmi()); // Quantité minimum de commande
      cna.setQuantiteEconomique(sortie.getNaqec()); // Quantité économique
      cna.setCoefficientCalculPrixVente(sortie.getNakpv()); // Coeff. calcul prix de vente
      cna.setDateDernierPrix(ConvertDate.db2ToDate(sortie.getNaddp().intValue(), null)); // date d'application
      cna.setRegroupementAchat(sortie.getNarga()); // Regroupement Achat
      cna.setDelaiSupplementaire(sortie.getNadels()); // Délai de supplémentaire
      cna.setOriginePrix(sortie.getNain1()); // P=Promo, C=Colonnes
      cna.setNoteQualite(sortie.getNain2()); // Note qualité de 0 à 9
      cna.setTypeFournisseur(sortie.getNain3()); // G = Fournisseur pour G.B.A
      cna.setRemiseCalculPVminimum(sortie.getNain4()); // 3 remises pour calcul PV min
      cna.setTypeRemiseLigne(sortie.getNatrl()); // Type remise ligne, A=Ajout
      cna.setReferenceConstructeur((sortie.getNarfc())); // Référence constructeur
      cna.setGencodFournisseur(sortie.getNagcd()); // Code gencod frs
      cna.setDateLimiteValidite(ConvertDate.db2ToDate(sortie.getNadaf().intValue(), null)); // date limite de validité
      cna.setLibelle(sortie.getNalib()); // Libellé
      cna.setOrigine(sortie.getNaopa()); // origine
      cna.setDelaiJours(sortie.getNain7()); // Delai en Jours et Non Semaine
      cna.setPrixDistributeur(sortie.getNapdi()); // Prix distributeur
      cna.setCoefficientApprocheFixe(sortie.getNakap()); // Coeff. approche fixe
      cna.setTopPrixNegocie(sortie.getNapgn()); // Top prix négocié
      cna.setUniteFrais1((sortie.getNafu1())); // Unité frais 1
      cna.setFraisEnValeur2(sortie.getNafv2()); // Frais 2 valeur
      cna.setFraisEnCoeff2(sortie.getNafk2()); // Frais 2 coefficient
      cna.setUniteFrais2(sortie.getNafu2()); // Unité frais 2
      cna.setUniteFrais3(sortie.getNafu3()); // Unité frais 3
      
    }
    
    return cna;
  }
}
