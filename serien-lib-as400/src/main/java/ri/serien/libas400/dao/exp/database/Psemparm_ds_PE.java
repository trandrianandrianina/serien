/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Psemparm_ds_PE pour les PE
 * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
 */
public class Psemparm_ds_PE extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "PELIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "PEETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "PETYT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "PETAP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "PEETP"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "PEMAT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "PEREP1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "PEREP2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "PEREP3"));
    
    length = 200;
  }
}
