/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlespalettes;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

public class Svgvx0014d {
  // Constantes
  public static final int SIZE_WETB = 3;
  public static final int SIZE_WART = 20;
  public static final int SIZE_WLIB = 30;
  public static final int SIZE_WOBS = 10;
  public static final int SIZE_WLB1 = 30;
  public static final int SIZE_WLB2 = 30;
  public static final int SIZE_WLB3 = 30;
  public static final int SIZE_WSTK = 9;
  public static final int DECIMAL_WSTK = 0;
  public static final int SIZE_WSTKCLI = 9;
  public static final int DECIMAL_WSTKCLI = 0;
  public static final int SIZE_WSTKCLIM = 9;
  public static final int DECIMAL_WSTKCLIM = 0;
  public static final int SIZE_WSTKCLIC = 9;
  public static final int DECIMAL_WSTKCLIC = 0;
  public static final int SIZE_WPRIXC = 9;
  public static final int DECIMAL_WPRIXC = 2;
  public static final int SIZE_WPRIXD = 9;
  public static final int DECIMAL_WPRIXD = 2;
  public static final int SIZE_WEBIN10 = 1;
  public static final int SIZE_TOTALE_DS = 208;
  public static final int SIZE_FILLER = 500 - 208;
  
  // Constantes indices Nom DS
  public static final int VAR_WETB = 0;
  public static final int VAR_WART = 1;
  public static final int VAR_WLIB = 2;
  public static final int VAR_WOBS = 3;
  public static final int VAR_WLB1 = 4;
  public static final int VAR_WLB2 = 5;
  public static final int VAR_WLB3 = 6;
  public static final int VAR_WSTK = 7;
  public static final int VAR_WSTKCLI = 8;
  public static final int VAR_WSTKCLIM = 9;
  public static final int VAR_WSTKCLIC = 10;
  public static final int VAR_WPRIXC = 11;
  public static final int VAR_WPRIXD = 12;
  public static final int VAR_WEBIN10 = 13;
  
  // Variables AS400
  private String wetb = ""; //
  private String wart = ""; // Code ARTICLE
  private String wlib = ""; // Libellé 1
  private String wobs = ""; // Observation
  private String wlb1 = ""; // Libellé 2
  private String wlb2 = ""; // Libellé 3
  private String wlb3 = ""; // Libellé 4
  private BigDecimal wstk = BigDecimal.ZERO; // Quantité stock
  private BigDecimal wstkcli = BigDecimal.ZERO; // Quantité stock client
  private BigDecimal wstkclim = BigDecimal.ZERO; // Quantité stock client / mag
  private BigDecimal wstkclic = BigDecimal.ZERO; // Qté stock client/ma/chantier
  private BigDecimal wprixc = BigDecimal.ZERO; // Prix de vente ou consignat°
  private BigDecimal wprixd = BigDecimal.ZERO; // Prix de dé-consignation
  private String webin10 = ""; // Gestion palette chantier
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WETB), //
      new AS400Text(SIZE_WART), // Code ARTICLE
      new AS400Text(SIZE_WLIB), // Libellé 1
      new AS400Text(SIZE_WOBS), // Observation
      new AS400Text(SIZE_WLB1), // Libellé 2
      new AS400Text(SIZE_WLB2), // Libellé 3
      new AS400Text(SIZE_WLB3), // Libellé 4
      new AS400ZonedDecimal(SIZE_WSTK, DECIMAL_WSTK), // Quantité stock
      new AS400ZonedDecimal(SIZE_WSTKCLI, DECIMAL_WSTKCLI), // Quantité stock client
      new AS400ZonedDecimal(SIZE_WSTKCLIM, DECIMAL_WSTKCLIM), // Quantité stock client / mag
      new AS400ZonedDecimal(SIZE_WSTKCLIC, DECIMAL_WSTKCLIC), // Qté stock client/ma/chantier
      new AS400ZonedDecimal(SIZE_WPRIXC, DECIMAL_WPRIXC), // Prix de vente ou consignat°
      new AS400ZonedDecimal(SIZE_WPRIXD, DECIMAL_WPRIXD), // Prix de dé-consignation
      new AS400Text(SIZE_WEBIN10), // Gestion palette chantier
      new AS400Text(SIZE_FILLER), // Filler
  };
  private AS400Structure ds = new AS400Structure(structure);
  public Object[] o = { wetb, wart, wlib, wobs, wlb1, wlb2, wlb3, wstk, wstkcli, wstkclim, wstkclic, wprixc, wprixd, webin10, filler, };
  
  // -- Accesseurs
  
  public void setWetb(String pWetb) {
    if (pWetb == null) {
      return;
    }
    wetb = pWetb;
  }
  
  public String getWetb() {
    return wetb;
  }
  
  public void setWart(String pWart) {
    if (pWart == null) {
      return;
    }
    wart = pWart;
  }
  
  public String getWart() {
    return wart;
  }
  
  public void setWlib(String pWlib) {
    if (pWlib == null) {
      return;
    }
    wlib = pWlib;
  }
  
  public String getWlib() {
    return wlib;
  }
  
  public void setWobs(String pWobs) {
    if (pWobs == null) {
      return;
    }
    wobs = pWobs;
  }
  
  public String getWobs() {
    return wobs;
  }
  
  public void setWlb1(String pWlb1) {
    if (pWlb1 == null) {
      return;
    }
    wlb1 = pWlb1;
  }
  
  public String getWlb1() {
    return wlb1;
  }
  
  public void setWlb2(String pWlb2) {
    if (pWlb2 == null) {
      return;
    }
    wlb2 = pWlb2;
  }
  
  public String getWlb2() {
    return wlb2;
  }
  
  public void setWlb3(String pWlb3) {
    if (pWlb3 == null) {
      return;
    }
    wlb3 = pWlb3;
  }
  
  public String getWlb3() {
    return wlb3;
  }
  
  public void setWstk(BigDecimal pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = pWstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWstk(Integer pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = BigDecimal.valueOf(pWstk);
  }
  
  public Integer getWstk() {
    return wstk.intValue();
  }
  
  public void setWstkcli(BigDecimal pWstkcli) {
    if (pWstkcli == null) {
      return;
    }
    wstkcli = pWstkcli.setScale(DECIMAL_WSTKCLI, RoundingMode.HALF_UP);
  }
  
  public void setWstkcli(Integer pWstkcli) {
    if (pWstkcli == null) {
      return;
    }
    wstkcli = BigDecimal.valueOf(pWstkcli);
  }
  
  public Integer getWstkcli() {
    return wstkcli.intValue();
  }
  
  public void setWstkclim(BigDecimal pWstkclim) {
    if (pWstkclim == null) {
      return;
    }
    wstkclim = pWstkclim.setScale(DECIMAL_WSTKCLIM, RoundingMode.HALF_UP);
  }
  
  public void setWstkclim(Integer pWstkclim) {
    if (pWstkclim == null) {
      return;
    }
    wstkclim = BigDecimal.valueOf(pWstkclim);
  }
  
  public Integer getWstkclim() {
    return wstkclim.intValue();
  }
  
  public void setWstkclic(BigDecimal pWstkclic) {
    if (pWstkclic == null) {
      return;
    }
    wstkclic = pWstkclic.setScale(DECIMAL_WSTKCLIC, RoundingMode.HALF_UP);
  }
  
  public void setWstkclic(Integer pWstkclic) {
    if (pWstkclic == null) {
      return;
    }
    wstkclic = BigDecimal.valueOf(pWstkclic);
  }
  
  public Integer getWstkclic() {
    return wstkclic.intValue();
  }
  
  public void setWprixc(BigDecimal pWprixc) {
    if (pWprixc == null) {
      return;
    }
    wprixc = pWprixc.setScale(DECIMAL_WPRIXC, RoundingMode.HALF_UP);
  }
  
  public void setWprixc(Double pWprixc) {
    if (pWprixc == null) {
      return;
    }
    wprixc = BigDecimal.valueOf(pWprixc).setScale(DECIMAL_WPRIXC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprixc() {
    return wprixc.setScale(DECIMAL_WPRIXC, RoundingMode.HALF_UP);
  }
  
  public void setWprixd(BigDecimal pWprixd) {
    if (pWprixd == null) {
      return;
    }
    wprixd = pWprixd.setScale(DECIMAL_WPRIXD, RoundingMode.HALF_UP);
  }
  
  public void setWprixd(Double pWprixd) {
    if (pWprixd == null) {
      return;
    }
    wprixd = BigDecimal.valueOf(pWprixd).setScale(DECIMAL_WPRIXD, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprixd() {
    return wprixd.setScale(DECIMAL_WPRIXD, RoundingMode.HALF_UP);
  }
  
  public void setWebin10(Character pWebin10) {
    if (pWebin10 == null) {
      return;
    }
    webin10 = String.valueOf(pWebin10);
  }
  
  public Character getWebin10() {
    return webin10.charAt(0);
  }
}
