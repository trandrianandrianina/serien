/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0049i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_L1COD = 1;
  public static final int SIZE_L1ETB = 3;
  public static final int SIZE_L1NUM = 6;
  public static final int DECIMAL_L1NUM = 0;
  public static final int SIZE_L1SUF = 1;
  public static final int DECIMAL_L1SUF = 0;
  public static final int SIZE_L1NLI = 4;
  public static final int DECIMAL_L1NLI = 0;
  public static final int SIZE_PIIN21 = 1;
  public static final int SIZE_PITP1 = 2;
  public static final int SIZE_PITP2 = 2;
  public static final int SIZE_PITP3 = 2;
  public static final int SIZE_PITP4 = 2;
  public static final int SIZE_PITP5 = 2;
  public static final int SIZE_PILB = 124;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PICPL = 8;
  public static final int SIZE_PITYP = 3;
  public static final int SIZE_PICPY = 1;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 193;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_L1COD = 1;
  public static final int VAR_L1ETB = 2;
  public static final int VAR_L1NUM = 3;
  public static final int VAR_L1SUF = 4;
  public static final int VAR_L1NLI = 5;
  public static final int VAR_PIIN21 = 6;
  public static final int VAR_PITP1 = 7;
  public static final int VAR_PITP2 = 8;
  public static final int VAR_PITP3 = 9;
  public static final int VAR_PITP4 = 10;
  public static final int VAR_PITP5 = 11;
  public static final int VAR_PILB = 12;
  public static final int VAR_PIART = 13;
  public static final int VAR_PICPL = 14;
  public static final int VAR_PITYP = 15;
  public static final int VAR_PICPY = 16;
  public static final int VAR_PIARR = 17;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String l1cod = ""; // Code ERL "E"
  private String l1etb = ""; // Code Etablissement
  private BigDecimal l1num = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal l1suf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal l1nli = BigDecimal.ZERO; // Numéro de Ligne
  private String piin21 = ""; // Ind. regroupement ligne
  private String pitp1 = ""; // Top personnal.N°1
  private String pitp2 = ""; // Top personnal.N°2
  private String pitp3 = ""; // Top personnal.N°3
  private String pitp4 = ""; // Top personnal.N°4
  private String pitp5 = ""; // Top personnal.N°5
  private String pilb = ""; // Libellés
  private String piart = ""; // Code article commentaire
  private String picpl = ""; // Complément de libellé
  private String pityp = ""; // Type de document
  private String picpy = ""; // Copie commentaire dans Achat
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_L1COD), // Code ERL "E"
      new AS400Text(SIZE_L1ETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_L1NUM, DECIMAL_L1NUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_L1SUF, DECIMAL_L1SUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_L1NLI, DECIMAL_L1NLI), // Numéro de Ligne
      new AS400Text(SIZE_PIIN21), // Ind. regroupement ligne
      new AS400Text(SIZE_PITP1), // Top personnal.N°1
      new AS400Text(SIZE_PITP2), // Top personnal.N°2
      new AS400Text(SIZE_PITP3), // Top personnal.N°3
      new AS400Text(SIZE_PITP4), // Top personnal.N°4
      new AS400Text(SIZE_PITP5), // Top personnal.N°5
      new AS400Text(SIZE_PILB), // Libellés
      new AS400Text(SIZE_PIART), // Code article commentaire
      new AS400Text(SIZE_PICPL), // Complément de libellé
      new AS400Text(SIZE_PITYP), // Type de document
      new AS400Text(SIZE_PICPY), // Copie commentaire dans Achat
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, l1cod, l1etb, l1num, l1suf, l1nli, piin21, pitp1, pitp2, pitp3, pitp4, pitp5, pilb, piart, picpl, pityp,
          picpy, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    l1cod = (String) output[1];
    l1etb = (String) output[2];
    l1num = (BigDecimal) output[3];
    l1suf = (BigDecimal) output[4];
    l1nli = (BigDecimal) output[5];
    piin21 = (String) output[6];
    pitp1 = (String) output[7];
    pitp2 = (String) output[8];
    pitp3 = (String) output[9];
    pitp4 = (String) output[10];
    pitp5 = (String) output[11];
    pilb = (String) output[12];
    piart = (String) output[13];
    picpl = (String) output[14];
    pityp = (String) output[15];
    picpy = (String) output[16];
    piarr = (String) output[17];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setL1cod(Character pL1cod) {
    if (pL1cod == null) {
      return;
    }
    l1cod = String.valueOf(pL1cod);
  }
  
  public Character getL1cod() {
    return l1cod.charAt(0);
  }
  
  public void setL1etb(String pL1etb) {
    if (pL1etb == null) {
      return;
    }
    l1etb = pL1etb;
  }
  
  public String getL1etb() {
    return l1etb;
  }
  
  public void setL1num(BigDecimal pL1num) {
    if (pL1num == null) {
      return;
    }
    l1num = pL1num.setScale(DECIMAL_L1NUM, RoundingMode.HALF_UP);
  }
  
  public void setL1num(Integer pL1num) {
    if (pL1num == null) {
      return;
    }
    l1num = BigDecimal.valueOf(pL1num);
  }
  
  public Integer getL1num() {
    return l1num.intValue();
  }
  
  public void setL1suf(BigDecimal pL1suf) {
    if (pL1suf == null) {
      return;
    }
    l1suf = pL1suf.setScale(DECIMAL_L1SUF, RoundingMode.HALF_UP);
  }
  
  public void setL1suf(Integer pL1suf) {
    if (pL1suf == null) {
      return;
    }
    l1suf = BigDecimal.valueOf(pL1suf);
  }
  
  public Integer getL1suf() {
    return l1suf.intValue();
  }
  
  public void setL1nli(BigDecimal pL1nli) {
    if (pL1nli == null) {
      return;
    }
    l1nli = pL1nli.setScale(DECIMAL_L1NLI, RoundingMode.HALF_UP);
  }
  
  public void setL1nli(Integer pL1nli) {
    if (pL1nli == null) {
      return;
    }
    l1nli = BigDecimal.valueOf(pL1nli);
  }
  
  public Integer getL1nli() {
    return l1nli.intValue();
  }
  
  public void setPiin21(Character pPiin21) {
    if (pPiin21 == null) {
      return;
    }
    piin21 = String.valueOf(pPiin21);
  }
  
  public Character getPiin21() {
    return piin21.charAt(0);
  }
  
  public void setPitp1(String pPitp1) {
    if (pPitp1 == null) {
      return;
    }
    pitp1 = pPitp1;
  }
  
  public String getPitp1() {
    return pitp1;
  }
  
  public void setPitp2(String pPitp2) {
    if (pPitp2 == null) {
      return;
    }
    pitp2 = pPitp2;
  }
  
  public String getPitp2() {
    return pitp2;
  }
  
  public void setPitp3(String pPitp3) {
    if (pPitp3 == null) {
      return;
    }
    pitp3 = pPitp3;
  }
  
  public String getPitp3() {
    return pitp3;
  }
  
  public void setPitp4(String pPitp4) {
    if (pPitp4 == null) {
      return;
    }
    pitp4 = pPitp4;
  }
  
  public String getPitp4() {
    return pitp4;
  }
  
  public void setPitp5(String pPitp5) {
    if (pPitp5 == null) {
      return;
    }
    pitp5 = pPitp5;
  }
  
  public String getPitp5() {
    return pitp5;
  }
  
  public void setPilb(String pPilb) {
    if (pPilb == null) {
      return;
    }
    pilb = pPilb;
  }
  
  public String getPilb() {
    return pilb;
  }
  
  public void setPiart(String pPiart) {
    if (pPiart == null) {
      return;
    }
    piart = pPiart;
  }
  
  public String getPiart() {
    return piart;
  }
  
  public void setPicpl(String pPicpl) {
    if (pPicpl == null) {
      return;
    }
    picpl = pPicpl;
  }
  
  public String getPicpl() {
    return picpl;
  }
  
  public void setPityp(String pPityp) {
    if (pPityp == null) {
      return;
    }
    pityp = pPityp;
  }
  
  public String getPityp() {
    return pityp;
  }
  
  public void setPicpy(Character pPicpy) {
    if (pPicpy == null) {
      return;
    }
    picpy = String.valueOf(pPicpy);
  }
  
  public Character getPicpy() {
    return picpy.charAt(0);
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
