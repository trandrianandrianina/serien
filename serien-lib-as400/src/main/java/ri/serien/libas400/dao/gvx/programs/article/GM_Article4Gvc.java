/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs.article;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

public class GM_Article4Gvc extends GM_Article {
  
  public GM_Article4Gvc(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Récupération des champs du fichier article et des extensions
   * @param indexBegin
   * @param indexEnd
   * @param conditions
   * @return
   */
  public ArrayList<GenericRecord> getPrincipaleInfos(int indexBegin, int indexEnd, String conditions) {
    String request =
        "select num, tmpa1etb, tmpa1art, a1npu, a1in15, substr(a1fam, 1, 1) as a1famgrp, a1fam, a1sfa, a1lib, a1lb1, a1lb2, a1lb3, a1ref1, a1top2, concat(a1cof, a1frs) as prcolfrs "
            +
            // String request = "select num, tmpa1etb, tmpa1art, a1npu, case when (a1in15 = '1') then 1 else 0 end as
            // tmpa1in15, substr(a1fam, 1, 1) as a1famgrp, a1fam, a1sfa, a1lib, a1lb1, a1lb2, a1lb3, a1ref1, a1top2,
            // concat(a1cof, a1frs) as prcolfrs " +
            "from ( "
            + "select row_number() over() as num, art.a1etb as tmpa1etb, art.a1art as tmpa1art, art.a1lib as tmpa1lib, art.*, e.* "
            + "from " + queryManager.getLibrary() + ".pgvmartm as art " + "left join " + queryManager.getLibrary()
            + ".pgvmeaam e  on e.a1etb = art.a1etb and e.a1art = art.a1art " + " ) as tmp where num between " + indexBegin + " and "
            + indexEnd;
    if (conditions != null) {
      request += " and " + conditions;
    }
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if ((listrcd == null) || (listrcd.size() == 0)) {
      return null;
    }
    
    return listrcd;
  }
  
  /**
   * Récupère les 10 tarifs pour une liste d'articles
   * @param etb
   * @param listcodeart
   * @return
   *
   */
  public ArrayList<GenericRecord> getTarifs(String etb, Object[] listcodeart) {
    if ((listcodeart == null)) {
      return null;
    }
    
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < listcodeart.length; i++) {
      sb.append("'").append(listcodeart[i]).append("', ");
    }
    sb.delete(sb.length() - 2, sb.length());
    
    String request = "select atdap, atart, atp01, atp02, atp03, atp04, atp05, atp06, atp07, atp08, atp09, atp10 from "
        + queryManager.getLibrary() + ".pgvmtarm a where a.atetb='" + etb + "' and a.atart in (" + sb.toString()
        + ") and a.atdap in ( select max(b.atdap) from " + queryManager.getLibrary()
        + ".pgvmtarm b where atdap <= (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) FROM SYSIBM.SYSDUMMY1 ) and a.atetb=b.atetb and a.atart=b.atart )";
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if ((listrcd == null) || (listrcd.size() == 0)) {
      return null;
    }
    
    return listrcd;
  }
  
  /**
   * Récupère le PUMP pour une liste d'articles
   * @param etb
   * @param listcodeart
   * @return
   *
   */
  public ArrayList<GenericRecord> getPump(String etb, Object[] listcodeart) {
    if ((listcodeart == null)) {
      return null;
    }
    
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < listcodeart.length; i++) {
      sb.append("'").append(listcodeart[i]).append("', ");
    }
    sb.delete(sb.length() - 2, sb.length());
    
    String request = "select h1dat, h1art, h1pmp from " + queryManager.getLibrary() + ".pgvmhism a where a.h1etb='" + etb
        + "' and a.h1art in (" + sb.toString() + ") and a.h1dat in ( select max(b.h1dat) from " + queryManager.getLibrary()
        + ".pgvmhism b where h1dat <= (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) FROM SYSIBM.SYSDUMMY1 ) and a.h1etb=b.h1etb and a.h1art=b.h1art )";
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if ((listrcd == null) || (listrcd.size() == 0)) {
      return null;
    }
    
    return listrcd;
  }
  
  /**
   * Retourne la liste des nom des fournisseurs à partir d'une liste de code fournisseur
   * @param etb
   * @param listcollecfrs
   * @param listcodefrs
   * @return
   */
  public ArrayList<GenericRecord> getNomFournisseur(String etb, Object[] listcolleccodefrs) {
    if ((listcolleccodefrs == null)) {
      return null;
    }
    
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < listcolleccodefrs.length; i++) {
      sb.append("'").append(listcolleccodefrs[i]).append("', ");
    }
    sb.delete(sb.length() - 2, sb.length());
    
    String request = "select fretb, concat(frcol, frfrs) as colfrs, frnom from " + queryManager.getLibrary()
        + ".pgvmfrsm where concat(frcol, frfrs) in (" + sb.toString() + ")";
    if (etb != null) {
      request += " and fretb = '" + etb + "'";
    }
    
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if ((listrcd == null) || (listrcd.size() == 0)) {
      return null;
    }
    
    return listrcd;
  }
  
  /**
   * Retourne la liste des marques pour un établissement donnée
   * @return
   */
  public String[] getListeMarques(String etb) {
    String request = "select distinct a1ref1 from " + queryManager.getLibrary() + ".pgvmartm where a1etb = '" + etb + "'";
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if ((listrcd == null) || (listrcd.size() == 0)) {
      return null;
    }
    
    int i = 0;
    String[] liste = new String[listrcd.size()];
    for (GenericRecord rcd : listrcd) {
      liste[i++] = (String) rcd.getField(0);
    }
    
    return liste;
  }
  
  /**
   * Retourne la liste des nom des fournisseurs pour un établissement donnée
   * @param etb
   * @param liste
   * @return
   */
  public boolean getListeNomFournisseurs(String etb, LinkedHashMap<String, String> liste) {
    String request =
        "select concat(frcol, frfrs) as colfrs, frnom from " + queryManager.getLibrary() + ".pgvmfrsm where fretb = '" + etb + "'";
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if ((listrcd == null) || (listrcd.size() == 0)) {
      return false;
    }
    
    for (GenericRecord rcd : listrcd) {
      liste.put((String) rcd.getField(0), (String) rcd.getField(1));
    }
    
    return true;
  }
  
  /**
   * Récupère les infos tarifs et autres sur le fournisseur principal des articles donnés
   * @param etb
   * @param listcodeart
   * @return
   */
  public ArrayList<GenericRecord> getInfosFournisseurPrincipal(String etb, Object[] listcodeart) {
    if ((listcodeart == null)) {
      return null;
    }
    
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < listcodeart.length; i++) {
      sb.append("'").append(listcodeart[i]).append("', ");
    }
    sb.delete(sb.length() - 2, sb.length());
    
    String request = "select CADAP, CAART, CARFC, CAPDI, CAPRS, CAREM1 from " + queryManager.getLibrary() + ".pgvmcnam a where a.caetb = '"
        + etb + "' and a.caart in (" + sb.toString() + ") and a.cafrs = '1' and a.cacol = '1' and a.cadap in (select max(b.cadap) from "
        + queryManager.getLibrary()
        + ".pgvmcnam b where b.caDAP <= (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) CONCAT  SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) FROM SYSIBM.SYSDUMMY1) and a.caetb=b.caetb and a.caart = b.caart and a.cafrs = b.cafrs and a.cacol = b.cacol)";
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if ((listrcd == null) || (listrcd.size() == 0)) {
      return null;
    }
    
    return listrcd;
  }
  
}
