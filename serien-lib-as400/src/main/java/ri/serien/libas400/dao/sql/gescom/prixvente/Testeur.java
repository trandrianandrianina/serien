/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.prixvente;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.documentvente.SqlLigneVente;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatBonDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeIdLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

/**
 * Cette classe regroupe tous les traitements permettant de tester le calcul de prix de vente.
 */
public class Testeur {
  private SystemeManager systeme = null;
  private QueryManager querymanager = null;
  
  // Variables pour cible une ligne d'un document en particulier
  // private EnumCodeEnteteDocumentVente codeEnteteDocumentVente = EnumCodeEnteteDocumentVente.DEVIS;
  private EnumCodeEnteteDocumentVente codeEnteteDocumentVente = null;
  private Integer numero = 13053;
  private Integer suffixe = 0;
  private Integer numeroLigne = 120;
  private Integer numeroFacture = null;
  
  /**
   * Constructeur.
   */
  public Testeur(SystemeManager pSysteme, QueryManager pQuerymanager) {
    if (pSysteme == null) {
      throw new MessageErreurException("La connexion avec le serveur n'est pas initialisée.");
    }
    if (pQuerymanager == null) {
      throw new MessageErreurException("La connexion à la base de données n'est pas initialisée.");
    }
    
    systeme = pSysteme;
    querymanager = pQuerymanager;
  }
  
  /**
   * Lance la procédure de test.
   */
  public void executer(CritereLigneVente pCritereLigneVente, String pCheminRacine, int pNombreCalculMax) {
    // Générer la liste des dociuments de vente à tester
    ListeIdLigneVente listeIdLigneVente = null;
    if (codeEnteteDocumentVente != null) {
      // Bride sur une ligne de vente en particulier pour débugger
      IdEtablissement idEtablissement = pCritereLigneVente.getIdEtablissement();
      IdLigneVente idLigneVente = IdLigneVente.getInstance(idEtablissement, codeEnteteDocumentVente, numero, suffixe, numeroLigne);
      
      // Créer une liste avec une seule ligne de vente
      listeIdLigneVente = new ListeIdLigneVente();
      listeIdLigneVente.add(idLigneVente);
    }
    else {
      // Effacer les données inutiles
      numeroFacture = null;
      numero = null;
      suffixe = null;
      numeroLigne = null;
      
      // Charger tous les documents de ventes d'une base de données à partir des critères fournis
      SqlLigneVente sqlLigneVente = new SqlLigneVente(querymanager);
      listeIdLigneVente = sqlLigneVente.chargerListeIdLigneVente(pCritereLigneVente);
      if (listeIdLigneVente == null || listeIdLigneVente.isEmpty()) {
        Trace.alerte("Aucune ligne de vente ne correspond aux critères de recherche.");
        return;
      }
    }
    
    // Préparer les données CSV
    List<String> listeEnregistrementCsv = new ArrayList<String>();
    listeEnregistrementCsv.add(EnregistrementTestPrixVente.genererTitreCsv());
    
    IdDocumentVente idDocumentVentePrecedent = null;
    
    // Lancer le test pour chaque document de ventes
    for (int i = 0; i < listeIdLigneVente.size(); i++) {
      IdLigneVente idLigneVente = listeIdLigneVente.get(i);
      
      // Tracer
      Trace.titre("LIGNE DE VENTE " + idLigneVente + " (" + (i + 1) + "/" + listeIdLigneVente.size() + ")");
      
      // Lancer le calcul RPG (via le SGVM77) pour le document devente
      // On ne lance le calcul que si le document de vente a changé depuis la dernière fois (cela suppose que les lignes soit triée
      // par document de vente).
      IdDocumentVente idDocumentVente = idLigneVente.getIdDocumentVente();
      if (!Constantes.equals(idDocumentVente, idDocumentVentePrecedent)) {
        Trace.info("Lancer le calcul des prix de vente en RPG pour le document de vente : " + idDocumentVente);
        boolean retour = lancerCalculRPG(idDocumentVente);
        if (!retour) {
          Trace.alerte("Le recalcul de prix en RPG ne s'est pas exécuté pour le document de vente : " + idDocumentVente);
        }
        idDocumentVentePrecedent = idDocumentVente;
      }
      
      // Lancer le calcul Java pour la ligne de vente
      lancerCalculJava(idLigneVente, listeEnregistrementCsv);
      
      // Arrêter les calculs si on a atteint le nombre de calcul demandé
      if (pNombreCalculMax > 0 && i >= pNombreCalculMax) {
        break;
      }
    }
    
    // Création du fichier texte
    String nomFichier = pCheminRacine + File.separator + querymanager.getLibrary() + '-' + new DateHeure().getintJourHeure() + ".csv";
    Trace.info("Chemin du fichier CSV généré : " + nomFichier);
    GestionFichierTexte gft = new GestionFichierTexte(nomFichier);
    gft.setContenuFichier((ArrayList<String>) listeEnregistrementCsv);
    gft.ecritureFichier();
    gft.dispose();
  }
  
  // -- Méthodes privées
  
  /**
   * Lance le programme RPG pour rechiffrer les prix de ventes d'un document.
   * 
   * @param pIdDocumentVente
   */
  private boolean lancerCalculRPG(IdDocumentVente pIdDocumentVente) {
    ContexteProgrammeRPG rpg = new ContexteProgrammeRPG(systeme.getSystem(), systeme.getCurlib());
    rpg.executerCommande("CHGDTAARA DTAARA(*LDA (29 10)) VALUE(" + String.format("'%-10s'", systeme.getProfilUtilisateur()) + ")");
    rpg.initialiserLettreEnvironnement(EnvironnementExecution.getLettreEnvironnement());
    rpg.ajouterBibliotheque(systeme.getCurlib(), true);
    rpg.ajouterBibliotheque(systeme.getLibenv(), false);
    rpg.ajouterBibliotheque(EnvironnementExecution.getExpas(), false);
    rpg.ajouterBibliotheque(EnvironnementExecution.getGvmas(), false);
    rpg.ajouterBibliotheque(EnvironnementExecution.getGvmx(), false);
    
    // Exécution du programme RPG d'importation pour chaque fichier trouvé
    try {
      String parametre64 = "00X00000  " + pIdDocumentVente.getIndicatif(IdDocumentVente.INDICATIF_ENTETE_ETB_NUM_SUF);
      String programme = String.format("CALL PGM(SGVM77) PARM('%-64s')", parametre64);
      return rpg.executerCommande(programme);
    }
    catch (Exception e) {
      return false;
    }
  }
  
  /**
   * Charge les identifiants des lignes d'un document de ventes.
   * 
   * @param pIdDocumentVente
   * @return
   */
  private ListeIdLigneVente chargerListeIdLigneDocumentVente(IdDocumentVente pIdDocumentVente) {
    SqlLigneVente sqlLigneVente = new SqlLigneVente(querymanager);
    
    // Récupération de la liste des identifiants des lignes
    CritereLigneVente critere = new CritereLigneVente();
    critere.setIdEtablissement(pIdDocumentVente.getIdEtablissement());
    critere.setIdDocument(pIdDocumentVente);
    critere.setSansHistorique(true);
    critere.setExclureRemiseSpeciale(true);
    ListeIdLigneVente listeIdLigneVente = sqlLigneVente.chargerListeIdLigneVente(critere);
    if (listeIdLigneVente == null || listeIdLigneVente.isEmpty()) {
      return null;
    }
    return listeIdLigneVente;
  }
  
  /**
   * Calcule le prix de vente de chaque ligne avec le programme Java.
   * 
   * @param pListeLigneVente
   * @param pListeEnregistrementCsv
   */
  private void lancerCalculJava(IdLigneVente pIdLigneVente, List<String> pListeEnregistrementCsv) {
    if (pIdLigneVente == null) {
      return;
    }
    
    try {
      // Préparer les paramètres
      ParametreChargement parametreChargement = new ParametreChargement();
      parametreChargement.setIdEtablissement(pIdLigneVente.getIdEtablissement());
      parametreChargement.setIdLigneDocumentVente(pIdLigneVente);
      
      // Charger les données du calcul
      ChargementPrixVente chargementPrixVente = new ChargementPrixVente(systeme, querymanager, parametreChargement);
      parametreChargement = chargementPrixVente.charger();
      
      // Possiblement l'article est un article commentaire
      if (parametreChargement == null) {
        Trace.info("Le prix de la ligne de vente n'est pas recalculé car les données chargées sont vides");
        return;
      }
      
      // Le document de vente est un facture donc pas de recalcul
      if (parametreChargement.getParametreDocumentVente() != null && parametreChargement.getParametreDocumentVente().getEtat() != null
          && parametreChargement.getParametreDocumentVente().getEtat() >= EnumEtatBonDocumentVente.FACTURE.getCode()) {
        Trace.info("Le prix de la ligne de vente n'est pas recalculé car c'est une ligne de facture");
        return;
      }
      // Ligne non chargée si ligne commentaire ou ligne annulée
      if (parametreChargement.getParametreLigneVente() == null) {
        Trace.info("Le prix de la ligne de vente n'est pas recalculé car c'est un commentaire ou une ligne annulée");
        return;
      }
      
      // La ligne de vente est une ligne d'une commande qui a été expédié
      if (parametreChargement.getParametreLigneVente() != null && parametreChargement.getParametreLigneVente().getEtat() != null
          && parametreChargement.getParametreLigneVente().getEtat() >= EnumEtatBonDocumentVente.EXPEDIE.getCode()) {
        Trace.info("Le prix de la ligne de vente n'est pas recalculé car elle a été expédiée");
        return;
      }
      
      // Il est explicitement marquée dans la ligne de vente que le prix est garanti
      // Peut être contredit si la quantité a été modifié ou si des lignes ont été ajouté ou supprimé dans le document de vente mais non
      // géré ici
      if (parametreChargement.getParametreLigneVente() != null && parametreChargement.getParametreLigneVente().isPrixGaranti()) {
        Trace.info("Le prix de la ligne de vente n'est pas recalculé car le prix est garanti");
        return;
      }
      
      // Calcul Java
      CalculPrixVente calculPrixVente = new CalculPrixVente();
      calculPrixVente.calculer(parametreChargement);
      
      // Générer la ligne CSV
      EnregistrementTestPrixVente enregistrement = new EnregistrementTestPrixVente();
      
      // Renseigner la date de création
      if (calculPrixVente.getParametreDocumentVente() != null) {
        enregistrement.setDateCreation(calculPrixVente.getParametreDocumentVente().getDateCreation());
      }
      
      // Renseigner l'identifiant de la ligne de vente
      enregistrement.setIdLigneVente(pIdLigneVente);
      
      // Renseigner l'identifiant du client
      if (parametreChargement.getParametreDocumentVente() != null) {
        enregistrement.setIdClient(parametreChargement.getParametreDocumentVente().getIdClientFacture());
      }
      
      // Renseigner l'identifiant de l'article
      if (calculPrixVente.getParametreArticle() != null) {
        enregistrement.setIdArticle(calculPrixVente.getParametreArticle().getIdArticle());
      }
      
      // Renseigner le type du document de vente
      if (parametreChargement.getParametreDocumentVente() != null
          && parametreChargement.getParametreDocumentVente().getIdDocumentVente() != null) {
        IdDocumentVente idDocumentVente = parametreChargement.getParametreDocumentVente().getIdDocumentVente();
        if (idDocumentVente.isIdDevis()) {
          enregistrement.setTypeDocument("Devis");
        }
        else if (idDocumentVente.isIdFacture()) {
          enregistrement.setTypeDocument("Facture");
        }
        else if (idDocumentVente.isIdCommandeOuBon()) {
          enregistrement.setTypeDocument("Commande ou Bon");
        }
      }
      
      // Valeurs du RPG
      enregistrement.setPrixBaseHTRPG(parametreChargement.getParametreLigneVente().getPrixBaseHT());
      enregistrement.setPrixNetSaisiHTRPG(parametreChargement.getParametreLigneVente().getPrixNetSaisiHT());
      enregistrement.setPrixNetCalculeHTRPG(parametreChargement.getParametreLigneVente().getPrixNetCalculeHT());
      enregistrement.setQuantiteRPG(parametreChargement.getParametreLigneVente().getQuantiteUV());
      enregistrement.setMontantHTRPG(parametreChargement.getParametreLigneVente().getMontantHT());
      enregistrement.setPrixNetTTCRPG(parametreChargement.getParametreLigneVente().getPrixNetCalculeTTC());
      
      // Valeurs du Java
      if (calculPrixVente.getFormulePrixVenteFinale() != null) {
        enregistrement.setPrixBaseHTJava(calculPrixVente.getFormulePrixVenteFinale().getPrixBaseCalculHT());
        enregistrement.setPrixNetHTJava(calculPrixVente.getFormulePrixVenteFinale().getPrixNetHT());
        enregistrement.setQuantiteJava(calculPrixVente.getFormulePrixVenteFinale().getQuantiteUV());
        enregistrement.setMontantHTJava(calculPrixVente.getFormulePrixVenteFinale().getMontantHT());
        enregistrement.setPrixNetTTCJava(calculPrixVente.getFormulePrixVenteFinale().getPrixNetTTC());
      }
      
      // Tracer la ligne CSV généréé
      String ligneCSV = enregistrement.genererCsv();
      Trace.info("[CSV] " + ligneCSV);
      
      // Ajouter la ligne CSV à la liste
      pListeEnregistrementCsv.add(ligneCSV);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du calcul du prix de vente de la ligne de vente : " + pIdLigneVente + '.');
    }
  }
  
  // -- Programme principal
  
  /**
   * Lanceur.
   * 
   * @param args
   */
  public static void main(String[] args) {
    /*if (args.length < 2) {
      throw new MessageErreurException(
          "Il manque un paramètre :\n soit le nom de la base de donnnées,\n soit la lettre d'environnement.");
    }*/
    args = new String[2];
    // Nom de la base de données Outil Parfait
    args[0] = "FM410";
    // Nom de la base de données Tecnifibre
    // args[0] = "FM700";
    // Nom de la base de données SMN
    // args[0] = "FM946";
    // Nom de la base de données CPR
    // args[0] = "FM952";
    // Lettre d'environnement
    args[1] = "O";
    
    // Paramètres obligatoires
    // La nom de la base de données
    String nomBaseDeDonnes = Constantes.normerTexte(args[0]);
    if (nomBaseDeDonnes.isEmpty()) {
      throw new MessageErreurException("Le nom de la base de donnnées est invalide.");
    }
    // La lettre d'environnement
    String lettre = Constantes.normerTexte(args[1]);
    if (lettre.isEmpty() || lettre.length() > 1) {
      throw new MessageErreurException("La lettre d'environnement est invalide.");
    }
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(nomBaseDeDonnes);
    BDD baseDeDonnees = new BDD(idBibliotheque, lettre.charAt(0));
    
    // Connexion
    Bibliotheque bibEnv = new Bibliotheque(IdBibliotheque.getInstance("M_GPL"), EnumTypeBibliotheque.SYSTEME_SERIEN);
    SystemeManager systemeManager = new SystemeManager("172.31.0.200", "RIDEVSV", "GAR1972", true);
    systemeManager.setCurlib(nomBaseDeDonnes);
    systemeManager.setLibenv(bibEnv);
    if (!systemeManager.isConnecte()) {
      throw new MessageErreurException("Connexion au serveur en erreur.");
    }
    QueryManager queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
    queryManager.setLibrary(baseDeDonnees.getId());
    
    // Initialisation de l'environnment pour l'exécution directe
    if (EnvironnementExecution.getLettreEnvironnement() == null) {
      EnvironnementExecution.initialiserNomBibliotheques(baseDeDonnees.getLettreEnvironnement());
    }
    
    // Constuire le chemin racine
    String cheminRacine = "";
    if (Constantes.isWindows()) {
      cheminRacine = Constantes.getDossierBureau() + File.separatorChar + "testCPV";
    }
    else {
      cheminRacine = "/tmp/testCPV";
    }
    
    // Démarrer les traces
    Trace.demarrerLogiciel(cheminRacine, "Testeur", "Testeur");
    
    // Critères
    CritereLigneVente critereLigneVente = new CritereLigneVente();
    
    // FM410 (Outil Parfait)
    critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("OP"));
    critereLigneVente.setDateCreationDebut(ConvertDate.db2ToDate(1220615, null));
    critereLigneVente.setDateCreationFin(ConvertDate.db2ToDate(1220615, null));
    
    // FM700 (Tecnifibre)
    /*
    critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("MAJ"));
    critereLigneVente.setDateCreationDebut(ConvertDate.db2ToDate(1220101, null));
    critereLigneVente.setDateCreationFin(ConvertDate.db2ToDate(1220630, null));
    */
    
    // FM946 (SMN)
    /*
    critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("SMN"));
    critereLigneVente.setDateCreationDebut(ConvertDate.db2ToDate(1220101, null));
    critereLigneVente.setDateCreationFin(ConvertDate.db2ToDate(1220630, null));
    */
    
    // FM952 (CPR)
    /*
    // critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("CPR"));
    // critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("SUD"));
    critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("DIJ"));
    // critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("SUD"));
    // critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("DIJ"));
    // critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("SDP"));
    // critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("RPC"));
    // critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("COU"));
    critereLigneVente.setDateCreationDebut(ConvertDate.db2ToDate(1220101, null));
    critereLigneVente.setDateCreationFin(ConvertDate.db2ToDate(1220731, null));
    */
    
    // Critères communs
    // critereLigneVente.setPrixGaranti(false);
    critereLigneVente.setLigneSpeciale(false);
    critereLigneVente.ajouterTypeDocumentVente(EnumTypeDocumentVente.DEVIS);
    critereLigneVente.ajouterTypeDocumentVente(EnumTypeDocumentVente.COMMANDE);
    critereLigneVente.ajouterTypeDocumentVente(EnumTypeDocumentVente.BON);
    
    // Lancer le test
    Testeur testeur = new Testeur(systemeManager, queryManager);
    testeur.executer(critereLigneVente, cheminRacine, 0);
    
    // Arrêter les traces
    Trace.arreterLogiciel();
    
    // Déconnexion
    systemeManager.deconnecter();
  }
  
}
