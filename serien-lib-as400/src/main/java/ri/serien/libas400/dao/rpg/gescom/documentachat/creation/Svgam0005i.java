/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.creation;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0005i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_LATOP = 1;
  public static final int DECIMAL_LATOP = 0;
  public static final int SIZE_LACOD = 1;
  public static final int SIZE_LAETB = 3;
  public static final int SIZE_LANUM = 6;
  public static final int DECIMAL_LANUM = 0;
  public static final int SIZE_LASUF = 1;
  public static final int DECIMAL_LASUF = 0;
  public static final int SIZE_LANLI = 4;
  public static final int DECIMAL_LANLI = 0;
  public static final int SIZE_LAERL = 1;
  public static final int SIZE_LACEX = 1;
  public static final int SIZE_LAQCX = 11;
  public static final int DECIMAL_LAQCX = 3;
  public static final int SIZE_LATVA = 1;
  public static final int DECIMAL_LATVA = 0;
  public static final int SIZE_LATB = 1;
  public static final int DECIMAL_LATB = 0;
  public static final int SIZE_LATR = 1;
  public static final int DECIMAL_LATR = 0;
  public static final int SIZE_LATN = 1;
  public static final int DECIMAL_LATN = 0;
  public static final int SIZE_LATU = 1;
  public static final int DECIMAL_LATU = 0;
  public static final int SIZE_LATQ = 1;
  public static final int DECIMAL_LATQ = 0;
  public static final int SIZE_LATH = 1;
  public static final int DECIMAL_LATH = 0;
  public static final int SIZE_LADCC = 1;
  public static final int DECIMAL_LADCC = 0;
  public static final int SIZE_LADCA = 1;
  public static final int DECIMAL_LADCA = 0;
  public static final int SIZE_LADCS = 1;
  public static final int DECIMAL_LADCS = 0;
  public static final int SIZE_LAVAL = 1;
  public static final int DECIMAL_LAVAL = 0;
  public static final int SIZE_LACOL = 1;
  public static final int DECIMAL_LACOL = 0;
  public static final int SIZE_LASGN = 1;
  public static final int DECIMAL_LASGN = 0;
  public static final int SIZE_LAQTC = 11;
  public static final int DECIMAL_LAQTC = 3;
  public static final int SIZE_LAUNC = 2;
  public static final int SIZE_LAQTS = 11;
  public static final int DECIMAL_LAQTS = 3;
  public static final int SIZE_LAKSC = 8;
  public static final int DECIMAL_LAKSC = 3;
  public static final int SIZE_LAPAB = 9;
  public static final int DECIMAL_LAPAB = 2;
  public static final int SIZE_LAPAN = 9;
  public static final int DECIMAL_LAPAN = 2;
  public static final int SIZE_LAPAC = 9;
  public static final int DECIMAL_LAPAC = 2;
  public static final int SIZE_LAMHT = 11;
  public static final int DECIMAL_LAMHT = 2;
  public static final int SIZE_LAAVR = 1;
  public static final int SIZE_LAQTA = 11;
  public static final int DECIMAL_LAQTA = 3;
  public static final int SIZE_LAUNA = 2;
  public static final int SIZE_LAKAC = 8;
  public static final int DECIMAL_LAKAC = 3;
  public static final int SIZE_LAART = 20;
  public static final int SIZE_LAMAG = 2;
  public static final int SIZE_LAMAGA = 2;
  public static final int SIZE_LANUMR = 6;
  public static final int DECIMAL_LANUMR = 0;
  public static final int SIZE_LASUFR = 1;
  public static final int DECIMAL_LASUFR = 0;
  public static final int SIZE_LANLIR = 4;
  public static final int DECIMAL_LANLIR = 0;
  public static final int SIZE_LADATH = 7;
  public static final int DECIMAL_LADATH = 0;
  public static final int SIZE_LAORDH = 3;
  public static final int DECIMAL_LAORDH = 0;
  public static final int SIZE_LASAN = 4;
  public static final int SIZE_LAACT = 4;
  public static final int SIZE_LANAT = 1;
  public static final int SIZE_LAMTA = 9;
  public static final int DECIMAL_LAMTA = 2;
  public static final int SIZE_LADLP = 7;
  public static final int DECIMAL_LADLP = 0;
  public static final int SIZE_LASER = 2;
  public static final int DECIMAL_LASER = 0;
  public static final int SIZE_LAPAI = 9;
  public static final int DECIMAL_LAPAI = 2;
  public static final int SIZE_LAPAR = 9;
  public static final int DECIMAL_LAPAR = 2;
  public static final int SIZE_LAREM1 = 4;
  public static final int DECIMAL_LAREM1 = 2;
  public static final int SIZE_LAREM2 = 4;
  public static final int DECIMAL_LAREM2 = 2;
  public static final int SIZE_LAREM3 = 4;
  public static final int DECIMAL_LAREM3 = 2;
  public static final int SIZE_LAREM4 = 4;
  public static final int DECIMAL_LAREM4 = 2;
  public static final int SIZE_LAREM5 = 4;
  public static final int DECIMAL_LAREM5 = 2;
  public static final int SIZE_LAREM6 = 4;
  public static final int DECIMAL_LAREM6 = 2;
  public static final int SIZE_LATRL = 1;
  public static final int SIZE_LABRL = 1;
  public static final int SIZE_LARP1 = 1;
  public static final int SIZE_LARP2 = 1;
  public static final int SIZE_LARP3 = 1;
  public static final int SIZE_LARP4 = 1;
  public static final int SIZE_LARP5 = 1;
  public static final int SIZE_LARP6 = 1;
  public static final int SIZE_LATP1 = 2;
  public static final int SIZE_LATP2 = 2;
  public static final int SIZE_LATP3 = 2;
  public static final int SIZE_LATP4 = 2;
  public static final int SIZE_LATP5 = 2;
  public static final int SIZE_LAIN1 = 1;
  public static final int SIZE_LAIN2 = 1;
  public static final int SIZE_LAIN3 = 1;
  public static final int SIZE_LAIN4 = 1;
  public static final int SIZE_LAIN5 = 1;
  public static final int SIZE_LADLC = 7;
  public static final int DECIMAL_LADLC = 0;
  public static final int SIZE_PILB = 124;
  public static final int SIZE_PINAT = 5;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIPRBRU = 9;
  public static final int DECIMAL_PIPRBRU = 2;
  public static final int SIZE_PIPRNET = 9;
  public static final int DECIMAL_PIPRNET = 2;
  public static final int SIZE_PIPRVFR = 9;
  public static final int DECIMAL_PIPRVFR = 2;
  public static final int SIZE_PIPORTF = 9;
  public static final int DECIMAL_PIPORTF = 2;
  public static final int SIZE_PIXXX1 = 7;
  public static final int DECIMAL_PIXXX1 = 4;
  public static final int SIZE_PILFK1 = 7;
  public static final int DECIMAL_PILFK1 = 4;
  public static final int SIZE_PILFV1 = 9;
  public static final int DECIMAL_PILFV1 = 2;
  public static final int SIZE_PILFP1 = 4;
  public static final int DECIMAL_PILFP1 = 2;
  public static final int SIZE_PILFK3 = 7;
  public static final int DECIMAL_PILFK3 = 4;
  public static final int SIZE_PILFV3 = 9;
  public static final int DECIMAL_PILFV3 = 2;
  public static final int SIZE_PITOS = 1;
  public static final int SIZE_PICOS = 1;
  public static final int SIZE_PINOS = 6;
  public static final int DECIMAL_PINOS = 0;
  public static final int SIZE_PISOS = 1;
  public static final int DECIMAL_PISOS = 0;
  public static final int SIZE_PILOS = 4;
  public static final int DECIMAL_PILOS = 0;
  public static final int SIZE_PITOE = 1;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 518;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_LATOP = 1;
  public static final int VAR_LACOD = 2;
  public static final int VAR_LAETB = 3;
  public static final int VAR_LANUM = 4;
  public static final int VAR_LASUF = 5;
  public static final int VAR_LANLI = 6;
  public static final int VAR_LAERL = 7;
  public static final int VAR_LACEX = 8;
  public static final int VAR_LAQCX = 9;
  public static final int VAR_LATVA = 10;
  public static final int VAR_LATB = 11;
  public static final int VAR_LATR = 12;
  public static final int VAR_LATN = 13;
  public static final int VAR_LATU = 14;
  public static final int VAR_LATQ = 15;
  public static final int VAR_LATH = 16;
  public static final int VAR_LADCC = 17;
  public static final int VAR_LADCA = 18;
  public static final int VAR_LADCS = 19;
  public static final int VAR_LAVAL = 20;
  public static final int VAR_LACOL = 21;
  public static final int VAR_LASGN = 22;
  public static final int VAR_LAQTC = 23;
  public static final int VAR_LAUNC = 24;
  public static final int VAR_LAQTS = 25;
  public static final int VAR_LAKSC = 26;
  public static final int VAR_LAPAB = 27;
  public static final int VAR_LAPAN = 28;
  public static final int VAR_LAPAC = 29;
  public static final int VAR_LAMHT = 30;
  public static final int VAR_LAAVR = 31;
  public static final int VAR_LAQTA = 32;
  public static final int VAR_LAUNA = 33;
  public static final int VAR_LAKAC = 34;
  public static final int VAR_LAART = 35;
  public static final int VAR_LAMAG = 36;
  public static final int VAR_LAMAGA = 37;
  public static final int VAR_LANUMR = 38;
  public static final int VAR_LASUFR = 39;
  public static final int VAR_LANLIR = 40;
  public static final int VAR_LADATH = 41;
  public static final int VAR_LAORDH = 42;
  public static final int VAR_LASAN = 43;
  public static final int VAR_LAACT = 44;
  public static final int VAR_LANAT = 45;
  public static final int VAR_LAMTA = 46;
  public static final int VAR_LADLP = 47;
  public static final int VAR_LASER = 48;
  public static final int VAR_LAPAI = 49;
  public static final int VAR_LAPAR = 50;
  public static final int VAR_LAREM1 = 51;
  public static final int VAR_LAREM2 = 52;
  public static final int VAR_LAREM3 = 53;
  public static final int VAR_LAREM4 = 54;
  public static final int VAR_LAREM5 = 55;
  public static final int VAR_LAREM6 = 56;
  public static final int VAR_LATRL = 57;
  public static final int VAR_LABRL = 58;
  public static final int VAR_LARP1 = 59;
  public static final int VAR_LARP2 = 60;
  public static final int VAR_LARP3 = 61;
  public static final int VAR_LARP4 = 62;
  public static final int VAR_LARP5 = 63;
  public static final int VAR_LARP6 = 64;
  public static final int VAR_LATP1 = 65;
  public static final int VAR_LATP2 = 66;
  public static final int VAR_LATP3 = 67;
  public static final int VAR_LATP4 = 68;
  public static final int VAR_LATP5 = 69;
  public static final int VAR_LAIN1 = 70;
  public static final int VAR_LAIN2 = 71;
  public static final int VAR_LAIN3 = 72;
  public static final int VAR_LAIN4 = 73;
  public static final int VAR_LAIN5 = 74;
  public static final int VAR_LADLC = 75;
  public static final int VAR_PILB = 76;
  public static final int VAR_PINAT = 77;
  public static final int VAR_PIDAT = 78;
  public static final int VAR_PIPRBRU = 79;
  public static final int VAR_PIPRNET = 80;
  public static final int VAR_PIPRVFR = 81;
  public static final int VAR_PIPORTF = 82;
  public static final int VAR_PIXXX1 = 83;
  public static final int VAR_PILFK1 = 84;
  public static final int VAR_PILFV1 = 85;
  public static final int VAR_PILFP1 = 86;
  public static final int VAR_PILFK3 = 87;
  public static final int VAR_PILFV3 = 88;
  public static final int VAR_PITOS = 89;
  public static final int VAR_PICOS = 90;
  public static final int VAR_PINOS = 91;
  public static final int VAR_PISOS = 92;
  public static final int VAR_PILOS = 93;
  public static final int VAR_PITOE = 94;
  public static final int VAR_PIARR = 95;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private BigDecimal latop = BigDecimal.ZERO; // Code Etat de LA LAgne
  private String lacod = ""; // Code ERL "E" OU "F"
  private String laetb = ""; // Code Etablissement
  private BigDecimal lanum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal lasuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal lanli = BigDecimal.ZERO; // Numéro de LAgne
  private String laerl = ""; // Code ERL "C"
  private String lacex = ""; // Code Extraction
  private BigDecimal laqcx = BigDecimal.ZERO; // Quantité extraite en UV
  private BigDecimal latva = BigDecimal.ZERO; // Code TVA
  private BigDecimal latb = BigDecimal.ZERO; // Top prix de base saisi
  private BigDecimal latr = BigDecimal.ZERO; // Top remises saisies
  private BigDecimal latn = BigDecimal.ZERO; // Top prix net saisi
  private BigDecimal latu = BigDecimal.ZERO; // Top unité saisie
  private BigDecimal latq = BigDecimal.ZERO; // Top Qté saisie
  private BigDecimal lath = BigDecimal.ZERO; // Top montant H.T saisi
  private BigDecimal ladcc = BigDecimal.ZERO; // Décimalisation QTC
  private BigDecimal ladca = BigDecimal.ZERO; // " " QTA
  private BigDecimal ladcs = BigDecimal.ZERO; // " " QTS
  private BigDecimal laval = BigDecimal.ZERO; // Top LAgne en Valeur
  private BigDecimal lacol = BigDecimal.ZERO; // Colonne TVA
  private BigDecimal lasgn = BigDecimal.ZERO; // Signe
  private BigDecimal laqtc = BigDecimal.ZERO; // Quantité en unités de cde
  private String launc = ""; // Unité de commande
  private BigDecimal laqts = BigDecimal.ZERO; // Quantité en unités de stock
  private BigDecimal laksc = BigDecimal.ZERO; // Coeff. Stock/Cde
  private BigDecimal lapab = BigDecimal.ZERO; // Prix d"achat de base
  private BigDecimal lapan = BigDecimal.ZERO; // Prix d"achat net
  private BigDecimal lapac = BigDecimal.ZERO; // Prix d"achat calculé
  private BigDecimal lamht = BigDecimal.ZERO; // Montant hors taxes
  private String laavr = ""; // Code Avoir
  private BigDecimal laqta = BigDecimal.ZERO; // Quantité en unités d"achat
  private String launa = ""; // Unité d"achat
  private BigDecimal lakac = BigDecimal.ZERO; // Coeff. Achat/Cde
  private String laart = ""; // Code Article
  private String lamag = ""; // Magasin
  private String lamaga = ""; // Magasin avant modif
  private BigDecimal lanumr = BigDecimal.ZERO; // N°Bon ou Fac.
  private BigDecimal lasufr = BigDecimal.ZERO; // Suffixe
  private BigDecimal lanlir = BigDecimal.ZERO; // N°ligne
  private BigDecimal ladath = BigDecimal.ZERO; // Date sur Histo stocks
  private BigDecimal laordh = BigDecimal.ZERO; // Ordre sur Histo stocks
  private String lasan = ""; // Section Analytique
  private String laact = ""; // Activité ou Affaire
  private String lanat = ""; // Nature d"achat (M,F,I)
  private BigDecimal lamta = BigDecimal.ZERO; // Montant affecté
  private BigDecimal ladlp = BigDecimal.ZERO; // Date LAvraison prévue
  private BigDecimal laser = BigDecimal.ZERO; // Top n° de série ou lot
  private BigDecimal lapai = BigDecimal.ZERO; // Prix d'achat initial
  private BigDecimal lapar = BigDecimal.ZERO; // Prix d'achat rem. déduites
  private BigDecimal larem1 = BigDecimal.ZERO; // % Remise 1 / LAgne
  private BigDecimal larem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal larem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal larem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal larem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal larem6 = BigDecimal.ZERO; // % Remise 6
  private String latrl = ""; // Type remise LAgne, 1=cascade
  private String labrl = ""; // Base remise LAgne, 1=Montant
  private String larp1 = ""; // Exclusion remise de pied N°1
  private String larp2 = ""; // Exclusion remise de pied N°2
  private String larp3 = ""; // Exclusion remise de pied N°3
  private String larp4 = ""; // Exclusion remise de pied N°4
  private String larp5 = ""; // Exclusion remise de pied N°5
  private String larp6 = ""; // Exclusion remise de pied N°6
  private String latp1 = ""; // Top personnal.N°1
  private String latp2 = ""; // Top personnal.N°2
  private String latp3 = ""; // Top personnal.N°3
  private String latp4 = ""; // Top personnal.N°4
  private String latp5 = ""; // Top personnal.N°5
  private String lain1 = ""; // Imputation frais
  private String lain2 = ""; // type de frais
  private String lain3 = ""; // regroupement article
  private String lain4 = ""; // compt. stock flottant
  private String lain5 = ""; // Condit° en nb de gratuits
  private BigDecimal ladlc = BigDecimal.ZERO; // Date LAvraison calculée
  private String pilb = ""; // Libellés
  private String pinat = ""; // Nature ligne
  private BigDecimal pidat = BigDecimal.ZERO; // Date de traitement
  private BigDecimal piprbru = BigDecimal.ZERO; // Prix d achat brut
  private BigDecimal piprnet = BigDecimal.ZERO; // Prix d achat net sans port
  private BigDecimal piprvfr = BigDecimal.ZERO; // Prix revient fournisseur
  private BigDecimal piportf = BigDecimal.ZERO; // Port fournissseur eur
  private BigDecimal pixxx1 = BigDecimal.ZERO; // Plus utilisé (anc.PiLFK1)
  private BigDecimal pilfk1 = BigDecimal.ZERO; // Frais 1 coef.(anc.PiLFK1p)
  private BigDecimal pilfv1 = BigDecimal.ZERO; // Frais 1 valeur
  private BigDecimal pilfp1 = BigDecimal.ZERO; // Frais 1 % PORT
  private BigDecimal pilfk3 = BigDecimal.ZERO; // Frais 3 coefficient
  private BigDecimal pilfv3 = BigDecimal.ZERO; // Frais 3 valeur
  private String pitos = ""; // Type origine sortie
  private String picos = ""; // Code origine sortie
  private BigDecimal pinos = BigDecimal.ZERO; // N°Bon origine sortie
  private BigDecimal pisos = BigDecimal.ZERO; // N°Suf.origine sortie
  private BigDecimal pilos = BigDecimal.ZERO; // N°Lig.origine sortie
  private String pitoe = ""; // Type origine entrée
  private String piarr = "X"; // Fin
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_LATOP, DECIMAL_LATOP), // Code Etat de LA LAgne
      new AS400Text(SIZE_LACOD), // Code ERL "E" OU "F"
      new AS400Text(SIZE_LAETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_LANUM, DECIMAL_LANUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_LASUF, DECIMAL_LASUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_LANLI, DECIMAL_LANLI), // Numéro de LAgne
      new AS400Text(SIZE_LAERL), // Code ERL "C"
      new AS400Text(SIZE_LACEX), // Code Extraction
      new AS400PackedDecimal(SIZE_LAQCX, DECIMAL_LAQCX), // Quantité extraite en UV
      new AS400ZonedDecimal(SIZE_LATVA, DECIMAL_LATVA), // Code TVA
      new AS400ZonedDecimal(SIZE_LATB, DECIMAL_LATB), // Top prix de base saisi
      new AS400ZonedDecimal(SIZE_LATR, DECIMAL_LATR), // Top remises saisies
      new AS400ZonedDecimal(SIZE_LATN, DECIMAL_LATN), // Top prix net saisi
      new AS400ZonedDecimal(SIZE_LATU, DECIMAL_LATU), // Top unité saisie
      new AS400ZonedDecimal(SIZE_LATQ, DECIMAL_LATQ), // Top Qté saisie
      new AS400ZonedDecimal(SIZE_LATH, DECIMAL_LATH), // Top montant H.T saisi
      new AS400ZonedDecimal(SIZE_LADCC, DECIMAL_LADCC), // Décimalisation QTC
      new AS400ZonedDecimal(SIZE_LADCA, DECIMAL_LADCA), // " " QTA
      new AS400ZonedDecimal(SIZE_LADCS, DECIMAL_LADCS), // " " QTS
      new AS400ZonedDecimal(SIZE_LAVAL, DECIMAL_LAVAL), // Top LAgne en Valeur
      new AS400ZonedDecimal(SIZE_LACOL, DECIMAL_LACOL), // Colonne TVA
      new AS400ZonedDecimal(SIZE_LASGN, DECIMAL_LASGN), // Signe
      new AS400PackedDecimal(SIZE_LAQTC, DECIMAL_LAQTC), // Quantité en unités de cde
      new AS400Text(SIZE_LAUNC), // Unité de commande
      new AS400PackedDecimal(SIZE_LAQTS, DECIMAL_LAQTS), // Quantité en unités de stock
      new AS400PackedDecimal(SIZE_LAKSC, DECIMAL_LAKSC), // Coeff. Stock/Cde
      new AS400PackedDecimal(SIZE_LAPAB, DECIMAL_LAPAB), // Prix d"achat de base
      new AS400PackedDecimal(SIZE_LAPAN, DECIMAL_LAPAN), // Prix d"achat net
      new AS400PackedDecimal(SIZE_LAPAC, DECIMAL_LAPAC), // Prix d"achat calculé
      new AS400PackedDecimal(SIZE_LAMHT, DECIMAL_LAMHT), // Montant hors taxes
      new AS400Text(SIZE_LAAVR), // Code Avoir
      new AS400PackedDecimal(SIZE_LAQTA, DECIMAL_LAQTA), // Quantité en unités d"achat
      new AS400Text(SIZE_LAUNA), // Unité d"achat
      new AS400PackedDecimal(SIZE_LAKAC, DECIMAL_LAKAC), // Coeff. Achat/Cde
      new AS400Text(SIZE_LAART), // Code Article
      new AS400Text(SIZE_LAMAG), // Magasin
      new AS400Text(SIZE_LAMAGA), // Magasin avant modif
      new AS400ZonedDecimal(SIZE_LANUMR, DECIMAL_LANUMR), // N°Bon ou Fac.
      new AS400ZonedDecimal(SIZE_LASUFR, DECIMAL_LASUFR), // Suffixe
      new AS400ZonedDecimal(SIZE_LANLIR, DECIMAL_LANLIR), // N°ligne
      new AS400ZonedDecimal(SIZE_LADATH, DECIMAL_LADATH), // Date sur Histo stocks
      new AS400ZonedDecimal(SIZE_LAORDH, DECIMAL_LAORDH), // Ordre sur Histo stocks
      new AS400Text(SIZE_LASAN), // Section Analytique
      new AS400Text(SIZE_LAACT), // Activité ou Affaire
      new AS400Text(SIZE_LANAT), // Nature d"achat (M,F,I)
      new AS400PackedDecimal(SIZE_LAMTA, DECIMAL_LAMTA), // Montant affecté
      new AS400PackedDecimal(SIZE_LADLP, DECIMAL_LADLP), // Date LAvraison prévue
      new AS400ZonedDecimal(SIZE_LASER, DECIMAL_LASER), // Top n° de série ou lot
      new AS400PackedDecimal(SIZE_LAPAI, DECIMAL_LAPAI), // Prix d'achat initial
      new AS400PackedDecimal(SIZE_LAPAR, DECIMAL_LAPAR), // Prix d'achat rem. déduites
      new AS400ZonedDecimal(SIZE_LAREM1, DECIMAL_LAREM1), // % Remise 1 / LAgne
      new AS400ZonedDecimal(SIZE_LAREM2, DECIMAL_LAREM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_LAREM3, DECIMAL_LAREM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_LAREM4, DECIMAL_LAREM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_LAREM5, DECIMAL_LAREM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_LAREM6, DECIMAL_LAREM6), // % Remise 6
      new AS400Text(SIZE_LATRL), // Type remise LAgne, 1=cascade
      new AS400Text(SIZE_LABRL), // Base remise LAgne, 1=Montant
      new AS400Text(SIZE_LARP1), // Exclusion remise de pied N°1
      new AS400Text(SIZE_LARP2), // Exclusion remise de pied N°2
      new AS400Text(SIZE_LARP3), // Exclusion remise de pied N°3
      new AS400Text(SIZE_LARP4), // Exclusion remise de pied N°4
      new AS400Text(SIZE_LARP5), // Exclusion remise de pied N°5
      new AS400Text(SIZE_LARP6), // Exclusion remise de pied N°6
      new AS400Text(SIZE_LATP1), // Top personnal.N°1
      new AS400Text(SIZE_LATP2), // Top personnal.N°2
      new AS400Text(SIZE_LATP3), // Top personnal.N°3
      new AS400Text(SIZE_LATP4), // Top personnal.N°4
      new AS400Text(SIZE_LATP5), // Top personnal.N°5
      new AS400Text(SIZE_LAIN1), // Imputation frais
      new AS400Text(SIZE_LAIN2), // type de frais
      new AS400Text(SIZE_LAIN3), // regroupement article
      new AS400Text(SIZE_LAIN4), // compt. stock flottant
      new AS400Text(SIZE_LAIN5), // Condit° en nb de gratuits
      new AS400PackedDecimal(SIZE_LADLC, DECIMAL_LADLC), // Date LAvraison calculée
      new AS400Text(SIZE_PILB), // Libellés
      new AS400Text(SIZE_PINAT), // Nature ligne
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date de traitement
      new AS400ZonedDecimal(SIZE_PIPRBRU, DECIMAL_PIPRBRU), // Prix d achat brut
      new AS400ZonedDecimal(SIZE_PIPRNET, DECIMAL_PIPRNET), // Prix d achat net sans port
      new AS400ZonedDecimal(SIZE_PIPRVFR, DECIMAL_PIPRVFR), // Prix revient fournisseur
      new AS400ZonedDecimal(SIZE_PIPORTF, DECIMAL_PIPORTF), // Port fournissseur eur
      new AS400ZonedDecimal(SIZE_PIXXX1, DECIMAL_PIXXX1), // Plus utilisé (anc.PiLFK1)
      new AS400ZonedDecimal(SIZE_PILFK1, DECIMAL_PILFK1), // Frais 1 coef.(anc.PiLFK1p)
      new AS400ZonedDecimal(SIZE_PILFV1, DECIMAL_PILFV1), // Frais 1 valeur
      new AS400ZonedDecimal(SIZE_PILFP1, DECIMAL_PILFP1), // Frais 1 % PORT
      new AS400ZonedDecimal(SIZE_PILFK3, DECIMAL_PILFK3), // Frais 3 coefficient
      new AS400ZonedDecimal(SIZE_PILFV3, DECIMAL_PILFV3), // Frais 3 valeur
      new AS400Text(SIZE_PITOS), // Type origine sortie
      new AS400Text(SIZE_PICOS), // Code origine sortie
      new AS400ZonedDecimal(SIZE_PINOS, DECIMAL_PINOS), // N°Bon origine sortie
      new AS400ZonedDecimal(SIZE_PISOS, DECIMAL_PISOS), // N°Suf.origine sortie
      new AS400ZonedDecimal(SIZE_PILOS, DECIMAL_PILOS), // N°Lig.origine sortie
      new AS400Text(SIZE_PITOE), // Type origine entrée
      new AS400Text(SIZE_PIARR), // Fin
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, latop, lacod, laetb, lanum, lasuf, lanli, laerl, lacex, laqcx, latva, latb, latr, latn, latu, latq, lath,
          ladcc, ladca, ladcs, laval, lacol, lasgn, laqtc, launc, laqts, laksc, lapab, lapan, lapac, lamht, laavr, laqta, launa, lakac,
          laart, lamag, lamaga, lanumr, lasufr, lanlir, ladath, laordh, lasan, laact, lanat, lamta, ladlp, laser, lapai, lapar, larem1,
          larem2, larem3, larem4, larem5, larem6, latrl, labrl, larp1, larp2, larp3, larp4, larp5, larp6, latp1, latp2, latp3, latp4,
          latp5, lain1, lain2, lain3, lain4, lain5, ladlc, pilb, pinat, pidat, piprbru, piprnet, piprvfr, piportf, pixxx1, pilfk1, pilfv1,
          pilfp1, pilfk3, pilfv3, pitos, picos, pinos, pisos, pilos, pitoe, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    latop = (BigDecimal) output[1];
    lacod = (String) output[2];
    laetb = (String) output[3];
    lanum = (BigDecimal) output[4];
    lasuf = (BigDecimal) output[5];
    lanli = (BigDecimal) output[6];
    laerl = (String) output[7];
    lacex = (String) output[8];
    laqcx = (BigDecimal) output[9];
    latva = (BigDecimal) output[10];
    latb = (BigDecimal) output[11];
    latr = (BigDecimal) output[12];
    latn = (BigDecimal) output[13];
    latu = (BigDecimal) output[14];
    latq = (BigDecimal) output[15];
    lath = (BigDecimal) output[16];
    ladcc = (BigDecimal) output[17];
    ladca = (BigDecimal) output[18];
    ladcs = (BigDecimal) output[19];
    laval = (BigDecimal) output[20];
    lacol = (BigDecimal) output[21];
    lasgn = (BigDecimal) output[22];
    laqtc = (BigDecimal) output[23];
    launc = (String) output[24];
    laqts = (BigDecimal) output[25];
    laksc = (BigDecimal) output[26];
    lapab = (BigDecimal) output[27];
    lapan = (BigDecimal) output[28];
    lapac = (BigDecimal) output[29];
    lamht = (BigDecimal) output[30];
    laavr = (String) output[31];
    laqta = (BigDecimal) output[32];
    launa = (String) output[33];
    lakac = (BigDecimal) output[34];
    laart = (String) output[35];
    lamag = (String) output[36];
    lamaga = (String) output[37];
    lanumr = (BigDecimal) output[38];
    lasufr = (BigDecimal) output[39];
    lanlir = (BigDecimal) output[40];
    ladath = (BigDecimal) output[41];
    laordh = (BigDecimal) output[42];
    lasan = (String) output[43];
    laact = (String) output[44];
    lanat = (String) output[45];
    lamta = (BigDecimal) output[46];
    ladlp = (BigDecimal) output[47];
    laser = (BigDecimal) output[48];
    lapai = (BigDecimal) output[49];
    lapar = (BigDecimal) output[50];
    larem1 = (BigDecimal) output[51];
    larem2 = (BigDecimal) output[52];
    larem3 = (BigDecimal) output[53];
    larem4 = (BigDecimal) output[54];
    larem5 = (BigDecimal) output[55];
    larem6 = (BigDecimal) output[56];
    latrl = (String) output[57];
    labrl = (String) output[58];
    larp1 = (String) output[59];
    larp2 = (String) output[60];
    larp3 = (String) output[61];
    larp4 = (String) output[62];
    larp5 = (String) output[63];
    larp6 = (String) output[64];
    latp1 = (String) output[65];
    latp2 = (String) output[66];
    latp3 = (String) output[67];
    latp4 = (String) output[68];
    latp5 = (String) output[69];
    lain1 = (String) output[70];
    lain2 = (String) output[71];
    lain3 = (String) output[72];
    lain4 = (String) output[73];
    lain5 = (String) output[74];
    ladlc = (BigDecimal) output[75];
    pilb = (String) output[76];
    pinat = (String) output[77];
    pidat = (BigDecimal) output[78];
    piprbru = (BigDecimal) output[79];
    piprnet = (BigDecimal) output[80];
    piprvfr = (BigDecimal) output[81];
    piportf = (BigDecimal) output[82];
    pixxx1 = (BigDecimal) output[83];
    pilfk1 = (BigDecimal) output[84];
    pilfv1 = (BigDecimal) output[85];
    pilfp1 = (BigDecimal) output[86];
    pilfk3 = (BigDecimal) output[87];
    pilfv3 = (BigDecimal) output[88];
    pitos = (String) output[89];
    picos = (String) output[90];
    pinos = (BigDecimal) output[91];
    pisos = (BigDecimal) output[92];
    pilos = (BigDecimal) output[93];
    pitoe = (String) output[94];
    piarr = (String) output[95];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setLatop(BigDecimal pLatop) {
    if (pLatop == null) {
      return;
    }
    latop = pLatop.setScale(DECIMAL_LATOP, RoundingMode.HALF_UP);
  }
  
  public void setLatop(Integer pLatop) {
    if (pLatop == null) {
      return;
    }
    latop = BigDecimal.valueOf(pLatop);
  }
  
  public Integer getLatop() {
    return latop.intValue();
  }
  
  public void setLacod(Character pLacod) {
    if (pLacod == null) {
      return;
    }
    lacod = String.valueOf(pLacod);
  }
  
  public Character getLacod() {
    return lacod.charAt(0);
  }
  
  public void setLaetb(String pLaetb) {
    if (pLaetb == null) {
      return;
    }
    laetb = pLaetb;
  }
  
  public String getLaetb() {
    return laetb;
  }
  
  public void setLanum(BigDecimal pLanum) {
    if (pLanum == null) {
      return;
    }
    lanum = pLanum.setScale(DECIMAL_LANUM, RoundingMode.HALF_UP);
  }
  
  public void setLanum(Integer pLanum) {
    if (pLanum == null) {
      return;
    }
    lanum = BigDecimal.valueOf(pLanum);
  }
  
  public Integer getLanum() {
    return lanum.intValue();
  }
  
  public void setLasuf(BigDecimal pLasuf) {
    if (pLasuf == null) {
      return;
    }
    lasuf = pLasuf.setScale(DECIMAL_LASUF, RoundingMode.HALF_UP);
  }
  
  public void setLasuf(Integer pLasuf) {
    if (pLasuf == null) {
      return;
    }
    lasuf = BigDecimal.valueOf(pLasuf);
  }
  
  public Integer getLasuf() {
    return lasuf.intValue();
  }
  
  public void setLanli(BigDecimal pLanli) {
    if (pLanli == null) {
      return;
    }
    lanli = pLanli.setScale(DECIMAL_LANLI, RoundingMode.HALF_UP);
  }
  
  public void setLanli(Integer pLanli) {
    if (pLanli == null) {
      return;
    }
    lanli = BigDecimal.valueOf(pLanli);
  }
  
  public Integer getLanli() {
    return lanli.intValue();
  }
  
  public void setLaerl(Character pLaerl) {
    if (pLaerl == null) {
      return;
    }
    laerl = String.valueOf(pLaerl);
  }
  
  public Character getLaerl() {
    return laerl.charAt(0);
  }
  
  public void setLacex(Character pLacex) {
    if (pLacex == null) {
      return;
    }
    lacex = String.valueOf(pLacex);
  }
  
  public Character getLacex() {
    return lacex.charAt(0);
  }
  
  public void setLaqcx(BigDecimal pLaqcx) {
    if (pLaqcx == null) {
      return;
    }
    laqcx = pLaqcx.setScale(DECIMAL_LAQCX, RoundingMode.HALF_UP);
  }
  
  public void setLaqcx(Double pLaqcx) {
    if (pLaqcx == null) {
      return;
    }
    laqcx = BigDecimal.valueOf(pLaqcx).setScale(DECIMAL_LAQCX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLaqcx() {
    return laqcx.setScale(DECIMAL_LAQCX, RoundingMode.HALF_UP);
  }
  
  public void setLatva(BigDecimal pLatva) {
    if (pLatva == null) {
      return;
    }
    latva = pLatva.setScale(DECIMAL_LATVA, RoundingMode.HALF_UP);
  }
  
  public void setLatva(Integer pLatva) {
    if (pLatva == null) {
      return;
    }
    latva = BigDecimal.valueOf(pLatva);
  }
  
  public Integer getLatva() {
    return latva.intValue();
  }
  
  public void setLatb(BigDecimal pLatb) {
    if (pLatb == null) {
      return;
    }
    latb = pLatb.setScale(DECIMAL_LATB, RoundingMode.HALF_UP);
  }
  
  public void setLatb(Integer pLatb) {
    if (pLatb == null) {
      return;
    }
    latb = BigDecimal.valueOf(pLatb);
  }
  
  public Integer getLatb() {
    return latb.intValue();
  }
  
  public void setLatr(BigDecimal pLatr) {
    if (pLatr == null) {
      return;
    }
    latr = pLatr.setScale(DECIMAL_LATR, RoundingMode.HALF_UP);
  }
  
  public void setLatr(Integer pLatr) {
    if (pLatr == null) {
      return;
    }
    latr = BigDecimal.valueOf(pLatr);
  }
  
  public Integer getLatr() {
    return latr.intValue();
  }
  
  public void setLatn(BigDecimal pLatn) {
    if (pLatn == null) {
      return;
    }
    latn = pLatn.setScale(DECIMAL_LATN, RoundingMode.HALF_UP);
  }
  
  public void setLatn(Integer pLatn) {
    if (pLatn == null) {
      return;
    }
    latn = BigDecimal.valueOf(pLatn);
  }
  
  public Integer getLatn() {
    return latn.intValue();
  }
  
  public void setLatu(BigDecimal pLatu) {
    if (pLatu == null) {
      return;
    }
    latu = pLatu.setScale(DECIMAL_LATU, RoundingMode.HALF_UP);
  }
  
  public void setLatu(Integer pLatu) {
    if (pLatu == null) {
      return;
    }
    latu = BigDecimal.valueOf(pLatu);
  }
  
  public Integer getLatu() {
    return latu.intValue();
  }
  
  public void setLatq(BigDecimal pLatq) {
    if (pLatq == null) {
      return;
    }
    latq = pLatq.setScale(DECIMAL_LATQ, RoundingMode.HALF_UP);
  }
  
  public void setLatq(Integer pLatq) {
    if (pLatq == null) {
      return;
    }
    latq = BigDecimal.valueOf(pLatq);
  }
  
  public Integer getLatq() {
    return latq.intValue();
  }
  
  public void setLath(BigDecimal pLath) {
    if (pLath == null) {
      return;
    }
    lath = pLath.setScale(DECIMAL_LATH, RoundingMode.HALF_UP);
  }
  
  public void setLath(Integer pLath) {
    if (pLath == null) {
      return;
    }
    lath = BigDecimal.valueOf(pLath);
  }
  
  public Integer getLath() {
    return lath.intValue();
  }
  
  public void setLadcc(BigDecimal pLadcc) {
    if (pLadcc == null) {
      return;
    }
    ladcc = pLadcc.setScale(DECIMAL_LADCC, RoundingMode.HALF_UP);
  }
  
  public void setLadcc(Integer pLadcc) {
    if (pLadcc == null) {
      return;
    }
    ladcc = BigDecimal.valueOf(pLadcc);
  }
  
  public Integer getLadcc() {
    return ladcc.intValue();
  }
  
  public void setLadca(BigDecimal pLadca) {
    if (pLadca == null) {
      return;
    }
    ladca = pLadca.setScale(DECIMAL_LADCA, RoundingMode.HALF_UP);
  }
  
  public void setLadca(Integer pLadca) {
    if (pLadca == null) {
      return;
    }
    ladca = BigDecimal.valueOf(pLadca);
  }
  
  public Integer getLadca() {
    return ladca.intValue();
  }
  
  public void setLadcs(BigDecimal pLadcs) {
    if (pLadcs == null) {
      return;
    }
    ladcs = pLadcs.setScale(DECIMAL_LADCS, RoundingMode.HALF_UP);
  }
  
  public void setLadcs(Integer pLadcs) {
    if (pLadcs == null) {
      return;
    }
    ladcs = BigDecimal.valueOf(pLadcs);
  }
  
  public Integer getLadcs() {
    return ladcs.intValue();
  }
  
  public void setLaval(BigDecimal pLaval) {
    if (pLaval == null) {
      return;
    }
    laval = pLaval.setScale(DECIMAL_LAVAL, RoundingMode.HALF_UP);
  }
  
  public void setLaval(Integer pLaval) {
    if (pLaval == null) {
      return;
    }
    laval = BigDecimal.valueOf(pLaval);
  }
  
  public Integer getLaval() {
    return laval.intValue();
  }
  
  public void setLacol(BigDecimal pLacol) {
    if (pLacol == null) {
      return;
    }
    lacol = pLacol.setScale(DECIMAL_LACOL, RoundingMode.HALF_UP);
  }
  
  public void setLacol(Integer pLacol) {
    if (pLacol == null) {
      return;
    }
    lacol = BigDecimal.valueOf(pLacol);
  }
  
  public Integer getLacol() {
    return lacol.intValue();
  }
  
  public void setLasgn(BigDecimal pLasgn) {
    if (pLasgn == null) {
      return;
    }
    lasgn = pLasgn.setScale(DECIMAL_LASGN, RoundingMode.HALF_UP);
  }
  
  public void setLasgn(Integer pLasgn) {
    if (pLasgn == null) {
      return;
    }
    lasgn = BigDecimal.valueOf(pLasgn);
  }
  
  public Integer getLasgn() {
    return lasgn.intValue();
  }
  
  public void setLaqtc(BigDecimal pLaqtc) {
    if (pLaqtc == null) {
      return;
    }
    laqtc = pLaqtc.setScale(DECIMAL_LAQTC, RoundingMode.HALF_UP);
  }
  
  public void setLaqtc(Double pLaqtc) {
    if (pLaqtc == null) {
      return;
    }
    laqtc = BigDecimal.valueOf(pLaqtc).setScale(DECIMAL_LAQTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLaqtc() {
    return laqtc.setScale(DECIMAL_LAQTC, RoundingMode.HALF_UP);
  }
  
  public void setLaunc(String pLaunc) {
    if (pLaunc == null) {
      return;
    }
    launc = pLaunc;
  }
  
  public String getLaunc() {
    return launc;
  }
  
  public void setLaqts(BigDecimal pLaqts) {
    if (pLaqts == null) {
      return;
    }
    laqts = pLaqts.setScale(DECIMAL_LAQTS, RoundingMode.HALF_UP);
  }
  
  public void setLaqts(Double pLaqts) {
    if (pLaqts == null) {
      return;
    }
    laqts = BigDecimal.valueOf(pLaqts).setScale(DECIMAL_LAQTS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLaqts() {
    return laqts.setScale(DECIMAL_LAQTS, RoundingMode.HALF_UP);
  }
  
  public void setLaksc(BigDecimal pLaksc) {
    if (pLaksc == null) {
      return;
    }
    laksc = pLaksc.setScale(DECIMAL_LAKSC, RoundingMode.HALF_UP);
  }
  
  public void setLaksc(Double pLaksc) {
    if (pLaksc == null) {
      return;
    }
    laksc = BigDecimal.valueOf(pLaksc).setScale(DECIMAL_LAKSC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLaksc() {
    return laksc.setScale(DECIMAL_LAKSC, RoundingMode.HALF_UP);
  }
  
  public void setLapab(BigDecimal pLapab) {
    if (pLapab == null) {
      return;
    }
    lapab = pLapab.setScale(DECIMAL_LAPAB, RoundingMode.HALF_UP);
  }
  
  public void setLapab(Double pLapab) {
    if (pLapab == null) {
      return;
    }
    lapab = BigDecimal.valueOf(pLapab).setScale(DECIMAL_LAPAB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLapab() {
    return lapab.setScale(DECIMAL_LAPAB, RoundingMode.HALF_UP);
  }
  
  public void setLapan(BigDecimal pLapan) {
    if (pLapan == null) {
      return;
    }
    lapan = pLapan.setScale(DECIMAL_LAPAN, RoundingMode.HALF_UP);
  }
  
  public void setLapan(Double pLapan) {
    if (pLapan == null) {
      return;
    }
    lapan = BigDecimal.valueOf(pLapan).setScale(DECIMAL_LAPAN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLapan() {
    return lapan.setScale(DECIMAL_LAPAN, RoundingMode.HALF_UP);
  }
  
  public void setLapac(BigDecimal pLapac) {
    if (pLapac == null) {
      return;
    }
    lapac = pLapac.setScale(DECIMAL_LAPAC, RoundingMode.HALF_UP);
  }
  
  public void setLapac(Double pLapac) {
    if (pLapac == null) {
      return;
    }
    lapac = BigDecimal.valueOf(pLapac).setScale(DECIMAL_LAPAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLapac() {
    return lapac.setScale(DECIMAL_LAPAC, RoundingMode.HALF_UP);
  }
  
  public void setLamht(BigDecimal pLamht) {
    if (pLamht == null) {
      return;
    }
    lamht = pLamht.setScale(DECIMAL_LAMHT, RoundingMode.HALF_UP);
  }
  
  public void setLamht(Double pLamht) {
    if (pLamht == null) {
      return;
    }
    lamht = BigDecimal.valueOf(pLamht).setScale(DECIMAL_LAMHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLamht() {
    return lamht.setScale(DECIMAL_LAMHT, RoundingMode.HALF_UP);
  }
  
  public void setLaavr(Character pLaavr) {
    if (pLaavr == null) {
      return;
    }
    laavr = String.valueOf(pLaavr);
  }
  
  public Character getLaavr() {
    return laavr.charAt(0);
  }
  
  public void setLaqta(BigDecimal pLaqta) {
    if (pLaqta == null) {
      return;
    }
    laqta = pLaqta.setScale(DECIMAL_LAQTA, RoundingMode.HALF_UP);
  }
  
  public void setLaqta(Double pLaqta) {
    if (pLaqta == null) {
      return;
    }
    laqta = BigDecimal.valueOf(pLaqta).setScale(DECIMAL_LAQTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLaqta() {
    return laqta.setScale(DECIMAL_LAQTA, RoundingMode.HALF_UP);
  }
  
  public void setLauna(String pLauna) {
    if (pLauna == null) {
      return;
    }
    launa = pLauna;
  }
  
  public String getLauna() {
    return launa;
  }
  
  public void setLakac(BigDecimal pLakac) {
    if (pLakac == null) {
      return;
    }
    lakac = pLakac.setScale(DECIMAL_LAKAC, RoundingMode.HALF_UP);
  }
  
  public void setLakac(Double pLakac) {
    if (pLakac == null) {
      return;
    }
    lakac = BigDecimal.valueOf(pLakac).setScale(DECIMAL_LAKAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLakac() {
    return lakac.setScale(DECIMAL_LAKAC, RoundingMode.HALF_UP);
  }
  
  public void setLaart(String pLaart) {
    if (pLaart == null) {
      return;
    }
    laart = pLaart;
  }
  
  public String getLaart() {
    return laart;
  }
  
  public void setLamag(String pLamag) {
    if (pLamag == null) {
      return;
    }
    lamag = pLamag;
  }
  
  public String getLamag() {
    return lamag;
  }
  
  public void setLamaga(String pLamaga) {
    if (pLamaga == null) {
      return;
    }
    lamaga = pLamaga;
  }
  
  public String getLamaga() {
    return lamaga;
  }
  
  public void setLanumr(BigDecimal pLanumr) {
    if (pLanumr == null) {
      return;
    }
    lanumr = pLanumr.setScale(DECIMAL_LANUMR, RoundingMode.HALF_UP);
  }
  
  public void setLanumr(Integer pLanumr) {
    if (pLanumr == null) {
      return;
    }
    lanumr = BigDecimal.valueOf(pLanumr);
  }
  
  public Integer getLanumr() {
    return lanumr.intValue();
  }
  
  public void setLasufr(BigDecimal pLasufr) {
    if (pLasufr == null) {
      return;
    }
    lasufr = pLasufr.setScale(DECIMAL_LASUFR, RoundingMode.HALF_UP);
  }
  
  public void setLasufr(Integer pLasufr) {
    if (pLasufr == null) {
      return;
    }
    lasufr = BigDecimal.valueOf(pLasufr);
  }
  
  public Integer getLasufr() {
    return lasufr.intValue();
  }
  
  public void setLanlir(BigDecimal pLanlir) {
    if (pLanlir == null) {
      return;
    }
    lanlir = pLanlir.setScale(DECIMAL_LANLIR, RoundingMode.HALF_UP);
  }
  
  public void setLanlir(Integer pLanlir) {
    if (pLanlir == null) {
      return;
    }
    lanlir = BigDecimal.valueOf(pLanlir);
  }
  
  public Integer getLanlir() {
    return lanlir.intValue();
  }
  
  public void setLadath(BigDecimal pLadath) {
    if (pLadath == null) {
      return;
    }
    ladath = pLadath.setScale(DECIMAL_LADATH, RoundingMode.HALF_UP);
  }
  
  public void setLadath(Integer pLadath) {
    if (pLadath == null) {
      return;
    }
    ladath = BigDecimal.valueOf(pLadath);
  }
  
  public void setLadath(Date pLadath) {
    if (pLadath == null) {
      return;
    }
    ladath = BigDecimal.valueOf(ConvertDate.dateToDb2(pLadath));
  }
  
  public Integer getLadath() {
    return ladath.intValue();
  }
  
  public Date getLadathConvertiEnDate() {
    return ConvertDate.db2ToDate(ladath.intValue(), null);
  }
  
  public void setLaordh(BigDecimal pLaordh) {
    if (pLaordh == null) {
      return;
    }
    laordh = pLaordh.setScale(DECIMAL_LAORDH, RoundingMode.HALF_UP);
  }
  
  public void setLaordh(Integer pLaordh) {
    if (pLaordh == null) {
      return;
    }
    laordh = BigDecimal.valueOf(pLaordh);
  }
  
  public Integer getLaordh() {
    return laordh.intValue();
  }
  
  public void setLasan(String pLasan) {
    if (pLasan == null) {
      return;
    }
    lasan = pLasan;
  }
  
  public String getLasan() {
    return lasan;
  }
  
  public void setLaact(String pLaact) {
    if (pLaact == null) {
      return;
    }
    laact = pLaact;
  }
  
  public String getLaact() {
    return laact;
  }
  
  public void setLanat(Character pLanat) {
    if (pLanat == null) {
      return;
    }
    lanat = String.valueOf(pLanat);
  }
  
  public Character getLanat() {
    return lanat.charAt(0);
  }
  
  public void setLamta(BigDecimal pLamta) {
    if (pLamta == null) {
      return;
    }
    lamta = pLamta.setScale(DECIMAL_LAMTA, RoundingMode.HALF_UP);
  }
  
  public void setLamta(Double pLamta) {
    if (pLamta == null) {
      return;
    }
    lamta = BigDecimal.valueOf(pLamta).setScale(DECIMAL_LAMTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLamta() {
    return lamta.setScale(DECIMAL_LAMTA, RoundingMode.HALF_UP);
  }
  
  public void setLadlp(BigDecimal pLadlp) {
    if (pLadlp == null) {
      return;
    }
    ladlp = pLadlp.setScale(DECIMAL_LADLP, RoundingMode.HALF_UP);
  }
  
  public void setLadlp(Integer pLadlp) {
    if (pLadlp == null) {
      return;
    }
    ladlp = BigDecimal.valueOf(pLadlp);
  }
  
  public void setLadlp(Date pLadlp) {
    if (pLadlp == null) {
      return;
    }
    ladlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pLadlp));
  }
  
  public Integer getLadlp() {
    return ladlp.intValue();
  }
  
  public Date getLadlpConvertiEnDate() {
    return ConvertDate.db2ToDate(ladlp.intValue(), null);
  }
  
  public void setLaser(BigDecimal pLaser) {
    if (pLaser == null) {
      return;
    }
    laser = pLaser.setScale(DECIMAL_LASER, RoundingMode.HALF_UP);
  }
  
  public void setLaser(Integer pLaser) {
    if (pLaser == null) {
      return;
    }
    laser = BigDecimal.valueOf(pLaser);
  }
  
  public Integer getLaser() {
    return laser.intValue();
  }
  
  public void setLapai(BigDecimal pLapai) {
    if (pLapai == null) {
      return;
    }
    lapai = pLapai.setScale(DECIMAL_LAPAI, RoundingMode.HALF_UP);
  }
  
  public void setLapai(Double pLapai) {
    if (pLapai == null) {
      return;
    }
    lapai = BigDecimal.valueOf(pLapai).setScale(DECIMAL_LAPAI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLapai() {
    return lapai.setScale(DECIMAL_LAPAI, RoundingMode.HALF_UP);
  }
  
  public void setLapar(BigDecimal pLapar) {
    if (pLapar == null) {
      return;
    }
    lapar = pLapar.setScale(DECIMAL_LAPAR, RoundingMode.HALF_UP);
  }
  
  public void setLapar(Double pLapar) {
    if (pLapar == null) {
      return;
    }
    lapar = BigDecimal.valueOf(pLapar).setScale(DECIMAL_LAPAR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLapar() {
    return lapar.setScale(DECIMAL_LAPAR, RoundingMode.HALF_UP);
  }
  
  public void setLarem1(BigDecimal pLarem1) {
    if (pLarem1 == null) {
      return;
    }
    larem1 = pLarem1.setScale(DECIMAL_LAREM1, RoundingMode.HALF_UP);
  }
  
  public void setLarem1(Double pLarem1) {
    if (pLarem1 == null) {
      return;
    }
    larem1 = BigDecimal.valueOf(pLarem1).setScale(DECIMAL_LAREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLarem1() {
    return larem1.setScale(DECIMAL_LAREM1, RoundingMode.HALF_UP);
  }
  
  public void setLarem2(BigDecimal pLarem2) {
    if (pLarem2 == null) {
      return;
    }
    larem2 = pLarem2.setScale(DECIMAL_LAREM2, RoundingMode.HALF_UP);
  }
  
  public void setLarem2(Double pLarem2) {
    if (pLarem2 == null) {
      return;
    }
    larem2 = BigDecimal.valueOf(pLarem2).setScale(DECIMAL_LAREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLarem2() {
    return larem2.setScale(DECIMAL_LAREM2, RoundingMode.HALF_UP);
  }
  
  public void setLarem3(BigDecimal pLarem3) {
    if (pLarem3 == null) {
      return;
    }
    larem3 = pLarem3.setScale(DECIMAL_LAREM3, RoundingMode.HALF_UP);
  }
  
  public void setLarem3(Double pLarem3) {
    if (pLarem3 == null) {
      return;
    }
    larem3 = BigDecimal.valueOf(pLarem3).setScale(DECIMAL_LAREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLarem3() {
    return larem3.setScale(DECIMAL_LAREM3, RoundingMode.HALF_UP);
  }
  
  public void setLarem4(BigDecimal pLarem4) {
    if (pLarem4 == null) {
      return;
    }
    larem4 = pLarem4.setScale(DECIMAL_LAREM4, RoundingMode.HALF_UP);
  }
  
  public void setLarem4(Double pLarem4) {
    if (pLarem4 == null) {
      return;
    }
    larem4 = BigDecimal.valueOf(pLarem4).setScale(DECIMAL_LAREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLarem4() {
    return larem4.setScale(DECIMAL_LAREM4, RoundingMode.HALF_UP);
  }
  
  public void setLarem5(BigDecimal pLarem5) {
    if (pLarem5 == null) {
      return;
    }
    larem5 = pLarem5.setScale(DECIMAL_LAREM5, RoundingMode.HALF_UP);
  }
  
  public void setLarem5(Double pLarem5) {
    if (pLarem5 == null) {
      return;
    }
    larem5 = BigDecimal.valueOf(pLarem5).setScale(DECIMAL_LAREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLarem5() {
    return larem5.setScale(DECIMAL_LAREM5, RoundingMode.HALF_UP);
  }
  
  public void setLarem6(BigDecimal pLarem6) {
    if (pLarem6 == null) {
      return;
    }
    larem6 = pLarem6.setScale(DECIMAL_LAREM6, RoundingMode.HALF_UP);
  }
  
  public void setLarem6(Double pLarem6) {
    if (pLarem6 == null) {
      return;
    }
    larem6 = BigDecimal.valueOf(pLarem6).setScale(DECIMAL_LAREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLarem6() {
    return larem6.setScale(DECIMAL_LAREM6, RoundingMode.HALF_UP);
  }
  
  public void setLatrl(Character pLatrl) {
    if (pLatrl == null) {
      return;
    }
    latrl = String.valueOf(pLatrl);
  }
  
  public Character getLatrl() {
    return latrl.charAt(0);
  }
  
  public void setLabrl(Character pLabrl) {
    if (pLabrl == null) {
      return;
    }
    labrl = String.valueOf(pLabrl);
  }
  
  public Character getLabrl() {
    return labrl.charAt(0);
  }
  
  public void setLarp1(Character pLarp1) {
    if (pLarp1 == null) {
      return;
    }
    larp1 = String.valueOf(pLarp1);
  }
  
  public Character getLarp1() {
    return larp1.charAt(0);
  }
  
  public void setLarp2(Character pLarp2) {
    if (pLarp2 == null) {
      return;
    }
    larp2 = String.valueOf(pLarp2);
  }
  
  public Character getLarp2() {
    return larp2.charAt(0);
  }
  
  public void setLarp3(Character pLarp3) {
    if (pLarp3 == null) {
      return;
    }
    larp3 = String.valueOf(pLarp3);
  }
  
  public Character getLarp3() {
    return larp3.charAt(0);
  }
  
  public void setLarp4(Character pLarp4) {
    if (pLarp4 == null) {
      return;
    }
    larp4 = String.valueOf(pLarp4);
  }
  
  public Character getLarp4() {
    return larp4.charAt(0);
  }
  
  public void setLarp5(Character pLarp5) {
    if (pLarp5 == null) {
      return;
    }
    larp5 = String.valueOf(pLarp5);
  }
  
  public Character getLarp5() {
    return larp5.charAt(0);
  }
  
  public void setLarp6(Character pLarp6) {
    if (pLarp6 == null) {
      return;
    }
    larp6 = String.valueOf(pLarp6);
  }
  
  public Character getLarp6() {
    return larp6.charAt(0);
  }
  
  public void setLatp1(String pLatp1) {
    if (pLatp1 == null) {
      return;
    }
    latp1 = pLatp1;
  }
  
  public String getLatp1() {
    return latp1;
  }
  
  public void setLatp2(String pLatp2) {
    if (pLatp2 == null) {
      return;
    }
    latp2 = pLatp2;
  }
  
  public String getLatp2() {
    return latp2;
  }
  
  public void setLatp3(String pLatp3) {
    if (pLatp3 == null) {
      return;
    }
    latp3 = pLatp3;
  }
  
  public String getLatp3() {
    return latp3;
  }
  
  public void setLatp4(String pLatp4) {
    if (pLatp4 == null) {
      return;
    }
    latp4 = pLatp4;
  }
  
  public String getLatp4() {
    return latp4;
  }
  
  public void setLatp5(String pLatp5) {
    if (pLatp5 == null) {
      return;
    }
    latp5 = pLatp5;
  }
  
  public String getLatp5() {
    return latp5;
  }
  
  public void setLain1(Character pLain1) {
    if (pLain1 == null) {
      return;
    }
    lain1 = String.valueOf(pLain1);
  }
  
  public Character getLain1() {
    return lain1.charAt(0);
  }
  
  public void setLain2(Character pLain2) {
    if (pLain2 == null) {
      return;
    }
    lain2 = String.valueOf(pLain2);
  }
  
  public Character getLain2() {
    return lain2.charAt(0);
  }
  
  public void setLain3(Character pLain3) {
    if (pLain3 == null) {
      return;
    }
    lain3 = String.valueOf(pLain3);
  }
  
  public Character getLain3() {
    return lain3.charAt(0);
  }
  
  public void setLain4(Character pLain4) {
    if (pLain4 == null) {
      return;
    }
    lain4 = String.valueOf(pLain4);
  }
  
  public Character getLain4() {
    return lain4.charAt(0);
  }
  
  public void setLain5(Character pLain5) {
    if (pLain5 == null) {
      return;
    }
    lain5 = String.valueOf(pLain5);
  }
  
  public Character getLain5() {
    return lain5.charAt(0);
  }
  
  public void setLadlc(BigDecimal pLadlc) {
    if (pLadlc == null) {
      return;
    }
    ladlc = pLadlc.setScale(DECIMAL_LADLC, RoundingMode.HALF_UP);
  }
  
  public void setLadlc(Integer pLadlc) {
    if (pLadlc == null) {
      return;
    }
    ladlc = BigDecimal.valueOf(pLadlc);
  }
  
  public void setLadlc(Date pLadlc) {
    if (pLadlc == null) {
      return;
    }
    ladlc = BigDecimal.valueOf(ConvertDate.dateToDb2(pLadlc));
  }
  
  public Integer getLadlc() {
    return ladlc.intValue();
  }
  
  public Date getLadlcConvertiEnDate() {
    return ConvertDate.db2ToDate(ladlc.intValue(), null);
  }
  
  public void setPilb(String pPilb) {
    if (pPilb == null) {
      return;
    }
    pilb = pPilb;
  }
  
  public String getPilb() {
    return pilb;
  }
  
  public void setPinat(String pPinat) {
    if (pPinat == null) {
      return;
    }
    pinat = pPinat;
  }
  
  public String getPinat() {
    return pinat;
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPiprbru(BigDecimal pPiprbru) {
    if (pPiprbru == null) {
      return;
    }
    piprbru = pPiprbru.setScale(DECIMAL_PIPRBRU, RoundingMode.HALF_UP);
  }
  
  public void setPiprbru(Double pPiprbru) {
    if (pPiprbru == null) {
      return;
    }
    piprbru = BigDecimal.valueOf(pPiprbru).setScale(DECIMAL_PIPRBRU, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiprbru() {
    return piprbru.setScale(DECIMAL_PIPRBRU, RoundingMode.HALF_UP);
  }
  
  public void setPiprnet(BigDecimal pPiprnet) {
    if (pPiprnet == null) {
      return;
    }
    piprnet = pPiprnet.setScale(DECIMAL_PIPRNET, RoundingMode.HALF_UP);
  }
  
  public void setPiprnet(Double pPiprnet) {
    if (pPiprnet == null) {
      return;
    }
    piprnet = BigDecimal.valueOf(pPiprnet).setScale(DECIMAL_PIPRNET, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiprnet() {
    return piprnet.setScale(DECIMAL_PIPRNET, RoundingMode.HALF_UP);
  }
  
  public void setPiprvfr(BigDecimal pPiprvfr) {
    if (pPiprvfr == null) {
      return;
    }
    piprvfr = pPiprvfr.setScale(DECIMAL_PIPRVFR, RoundingMode.HALF_UP);
  }
  
  public void setPiprvfr(Double pPiprvfr) {
    if (pPiprvfr == null) {
      return;
    }
    piprvfr = BigDecimal.valueOf(pPiprvfr).setScale(DECIMAL_PIPRVFR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiprvfr() {
    return piprvfr.setScale(DECIMAL_PIPRVFR, RoundingMode.HALF_UP);
  }
  
  public void setPiportf(BigDecimal pPiportf) {
    if (pPiportf == null) {
      return;
    }
    piportf = pPiportf.setScale(DECIMAL_PIPORTF, RoundingMode.HALF_UP);
  }
  
  public void setPiportf(Double pPiportf) {
    if (pPiportf == null) {
      return;
    }
    piportf = BigDecimal.valueOf(pPiportf).setScale(DECIMAL_PIPORTF, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiportf() {
    return piportf.setScale(DECIMAL_PIPORTF, RoundingMode.HALF_UP);
  }
  
  public void setPixxx1(BigDecimal pPixxx1) {
    if (pPixxx1 == null) {
      return;
    }
    pixxx1 = pPixxx1.setScale(DECIMAL_PIXXX1, RoundingMode.HALF_UP);
  }
  
  public void setPixxx1(Double pPixxx1) {
    if (pPixxx1 == null) {
      return;
    }
    pixxx1 = BigDecimal.valueOf(pPixxx1).setScale(DECIMAL_PIXXX1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPixxx1() {
    return pixxx1.setScale(DECIMAL_PIXXX1, RoundingMode.HALF_UP);
  }
  
  public void setPilfk1(BigDecimal pPilfk1) {
    if (pPilfk1 == null) {
      return;
    }
    pilfk1 = pPilfk1.setScale(DECIMAL_PILFK1, RoundingMode.HALF_UP);
  }
  
  public void setPilfk1(Double pPilfk1) {
    if (pPilfk1 == null) {
      return;
    }
    pilfk1 = BigDecimal.valueOf(pPilfk1).setScale(DECIMAL_PILFK1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfk1() {
    return pilfk1.setScale(DECIMAL_PILFK1, RoundingMode.HALF_UP);
  }
  
  public void setPilfv1(BigDecimal pPilfv1) {
    if (pPilfv1 == null) {
      return;
    }
    pilfv1 = pPilfv1.setScale(DECIMAL_PILFV1, RoundingMode.HALF_UP);
  }
  
  public void setPilfv1(Double pPilfv1) {
    if (pPilfv1 == null) {
      return;
    }
    pilfv1 = BigDecimal.valueOf(pPilfv1).setScale(DECIMAL_PILFV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfv1() {
    return pilfv1.setScale(DECIMAL_PILFV1, RoundingMode.HALF_UP);
  }
  
  public void setPilfp1(BigDecimal pPilfp1) {
    if (pPilfp1 == null) {
      return;
    }
    pilfp1 = pPilfp1.setScale(DECIMAL_PILFP1, RoundingMode.HALF_UP);
  }
  
  public void setPilfp1(Double pPilfp1) {
    if (pPilfp1 == null) {
      return;
    }
    pilfp1 = BigDecimal.valueOf(pPilfp1).setScale(DECIMAL_PILFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfp1() {
    return pilfp1.setScale(DECIMAL_PILFP1, RoundingMode.HALF_UP);
  }
  
  public void setPilfk3(BigDecimal pPilfk3) {
    if (pPilfk3 == null) {
      return;
    }
    pilfk3 = pPilfk3.setScale(DECIMAL_PILFK3, RoundingMode.HALF_UP);
  }
  
  public void setPilfk3(Double pPilfk3) {
    if (pPilfk3 == null) {
      return;
    }
    pilfk3 = BigDecimal.valueOf(pPilfk3).setScale(DECIMAL_PILFK3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfk3() {
    return pilfk3.setScale(DECIMAL_PILFK3, RoundingMode.HALF_UP);
  }
  
  public void setPilfv3(BigDecimal pPilfv3) {
    if (pPilfv3 == null) {
      return;
    }
    pilfv3 = pPilfv3.setScale(DECIMAL_PILFV3, RoundingMode.HALF_UP);
  }
  
  public void setPilfv3(Double pPilfv3) {
    if (pPilfv3 == null) {
      return;
    }
    pilfv3 = BigDecimal.valueOf(pPilfv3).setScale(DECIMAL_PILFV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfv3() {
    return pilfv3.setScale(DECIMAL_PILFV3, RoundingMode.HALF_UP);
  }
  
  public void setPitos(Character pPitos) {
    if (pPitos == null) {
      return;
    }
    pitos = String.valueOf(pPitos);
  }
  
  public Character getPitos() {
    return pitos.charAt(0);
  }
  
  public void setPicos(Character pPicos) {
    if (pPicos == null) {
      return;
    }
    picos = String.valueOf(pPicos);
  }
  
  public Character getPicos() {
    return picos.charAt(0);
  }
  
  public void setPinos(BigDecimal pPinos) {
    if (pPinos == null) {
      return;
    }
    pinos = pPinos.setScale(DECIMAL_PINOS, RoundingMode.HALF_UP);
  }
  
  public void setPinos(Integer pPinos) {
    if (pPinos == null) {
      return;
    }
    pinos = BigDecimal.valueOf(pPinos);
  }
  
  public Integer getPinos() {
    return pinos.intValue();
  }
  
  public void setPisos(BigDecimal pPisos) {
    if (pPisos == null) {
      return;
    }
    pisos = pPisos.setScale(DECIMAL_PISOS, RoundingMode.HALF_UP);
  }
  
  public void setPisos(Integer pPisos) {
    if (pPisos == null) {
      return;
    }
    pisos = BigDecimal.valueOf(pPisos);
  }
  
  public Integer getPisos() {
    return pisos.intValue();
  }
  
  public void setPilos(BigDecimal pPilos) {
    if (pPilos == null) {
      return;
    }
    pilos = pPilos.setScale(DECIMAL_PILOS, RoundingMode.HALF_UP);
  }
  
  public void setPilos(Integer pPilos) {
    if (pPilos == null) {
      return;
    }
    pilos = BigDecimal.valueOf(pPilos);
  }
  
  public Integer getPilos() {
    return pilos.intValue();
  }
  
  public void setPitoe(Character pPitoe) {
    if (pPitoe == null) {
      return;
    }
    pitoe = String.valueOf(pPitoe);
  }
  
  public Character getPitoe() {
    return pitoe.charAt(0);
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
