/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.serien.libcommun.outils.Constantes;

/**
 * Ligne de commande Série N au format Magento
 * Cette classe sert à l'intégration JSON vers JAVA
 * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonLigneCommandeV3 {
  // Données internes qui ne transitent pas dans les flux
  private transient String BLLIB1 = null;
  private transient String BLLIB2 = null;
  private transient String BLLIB3 = null;
  private transient String BLLIB4 = null;
  private transient int longueurMaxLibs = 30;
  private transient String prixPourInjection = null;
  
  // Données qui transitent dans les flux
  private String codeArticle = null;
  private String nomArticle = null;
  private int qteCommandee = 0;
  private String prixHT = null;
  private String prixBrutHT = null;
  private String remisePourCent = null;
  private String prixTTC = null;
  private String colonneTVA = null;
  private String totalHT = null;
  // DEEE à l'article
  private String DEEE = null;
  // Total DEEE pour la ligne
  private String totalDEEE = null;
  
  /**
   * Formater les données de type libellés article
   */
  // TODO REMONTER CETTE METHODE
  protected void formaterDonnees() {
    if (nomArticle != null) {
      BLLIB1 = nomArticle;
      BLLIB2 = null;
      BLLIB3 = null;
      BLLIB4 = null;
      
      if (BLLIB1.length() > longueurMaxLibs) {
        BLLIB2 = BLLIB1.substring(longueurMaxLibs);
        BLLIB1 = BLLIB1.substring(0, longueurMaxLibs);
        
        if (BLLIB2.length() > longueurMaxLibs) {
          BLLIB3 = BLLIB2.substring(longueurMaxLibs);
          BLLIB2 = BLLIB2.substring(0, longueurMaxLibs);
          
          if (BLLIB3.length() > longueurMaxLibs) {
            BLLIB4 = BLLIB3.substring(longueurMaxLibs);
            BLLIB3 = BLLIB3.substring(0, longueurMaxLibs);
          }
        }
      }
    }
    // Le prix pour injection
    if (prixHT != null) {
      prixPourInjection = prixHT;
    }
  }
  
  public JsonLigneCommandeV3() {
    
  }
  
  /**
   * le client est TTC on passe les tarifs à injecter en TTC
   * V3 on récupère le TTC direct de Magento
   */
  public void passerEnTTC() {
    if (prixTTC != null) {
      prixPourInjection = prixTTC;
    }
    else {
      try {
        if (prixHT != null) {
          // TODO Faire gaffe avec la bonne récupération de la TVA
          // TODO méthodes génériques d'arrondi
          BigDecimal prix = (new BigDecimal(prixHT).multiply(new BigDecimal(1.2)));
          prix = prix.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
          prixPourInjection = prix.toString();
        }
        
      }
      catch (Exception e) {
        prixPourInjection = null;
      }
    }
  }
  
  public String getCodeArticle() {
    return codeArticle;
  }
  
  public void setCodeArticle(String pCodeArticle) {
    codeArticle = pCodeArticle;
  }
  
  public int getQteCommandee() {
    return qteCommandee;
  }
  
  public void setQteCommandee(int qteCommandee) {
    this.qteCommandee = qteCommandee;
  }
  
  public String getPrixHT() {
    return prixHT;
  }
  
  public void setPrixHT(String prixHT) {
    this.prixHT = prixHT;
  }
  
  public String getColonneTVA() {
    return colonneTVA;
  }
  
  public void setColonneTVA(String colonneTVA) {
    this.colonneTVA = colonneTVA;
  }
  
  public String getTotalHT() {
    return totalHT;
  }
  
  public void setTotalHT(String totalHT) {
    this.totalHT = totalHT;
  }
  
  public String getDEEE() {
    return DEEE;
  }
  
  public void setDEEE(String dEEE) {
    DEEE = dEEE;
  }
  
  public String getTotalDEEE() {
    return totalDEEE;
  }
  
  public void setTotalDEEE(String totalDEEE) {
    this.totalDEEE = totalDEEE;
  }
  
  public String getNomArticle() {
    return nomArticle;
  }
  
  public void setNomArticle(String nomArticle) {
    this.nomArticle = nomArticle;
  }
  
  public String getBLLIB1() {
    return BLLIB1;
  }
  
  public void setBLLIB1(String bLLIB1) {
    BLLIB1 = bLLIB1;
  }
  
  public String getBLLIB2() {
    return BLLIB2;
  }
  
  public void setBLLIB2(String bLLIB2) {
    BLLIB2 = bLLIB2;
  }
  
  public String getBLLIB3() {
    return BLLIB3;
  }
  
  public void setBLLIB3(String bLLIB3) {
    BLLIB3 = bLLIB3;
  }
  
  public String getBLLIB4() {
    return BLLIB4;
  }
  
  public void setBLLIB4(String bLLIB4) {
    BLLIB4 = bLLIB4;
  }
  
  public int getLongueurMaxLibs() {
    return longueurMaxLibs;
  }
  
  public void setLongueurMaxLibs(int longueurMaxLibs) {
    this.longueurMaxLibs = longueurMaxLibs;
  }
  
  public String getPrixTTC() {
    return prixTTC;
  }
  
  public void setPrixTTC(String prixTTC) {
    this.prixTTC = prixTTC;
  }
  
  public String getPrixPourInjection() {
    return prixPourInjection;
  }
  
  public void setPrixPourInjection(String prixPourInjection) {
    this.prixPourInjection = prixPourInjection;
  }
  
  public String getPrixBrutHT() {
    return prixBrutHT;
  }
  
  public void setPrixBrutHT(String prixBrutHT) {
    this.prixBrutHT = prixBrutHT;
  }
  
  public String getRemisePourCent() {
    return remisePourCent;
  }
  
  public void setRemisePourCent(String remisePourCent) {
    this.remisePourCent = remisePourCent;
  }
  
}
