/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.stocks;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

public class Svgvx0008d {
  // Constantes
  public static final int SIZE_WETB = 3;
  public static final int SIZE_WMAG = 2;
  public static final int SIZE_WART = 20;
  public static final int SIZE_WSTK = 11;
  public static final int DECIMAL_WSTK = 3;
  public static final int SIZE_WRES = 11;
  public static final int DECIMAL_WRES = 3;
  public static final int SIZE_WATT = 11;
  public static final int DECIMAL_WATT = 3;
  public static final int SIZE_WAFF = 11;
  public static final int DECIMAL_WAFF = 3;
  public static final int SIZE_WDIV = 11;
  public static final int DECIMAL_WDIV = 3;
  public static final int SIZE_WDIA = 11;
  public static final int DECIMAL_WDIA = 3;
  public static final int SIZE_WATD = 11;
  public static final int DECIMAL_WATD = 3;
  public static final int SIZE_WMINI = 11;
  public static final int DECIMAL_WMINI = 3;
  public static final int SIZE_WIDEAL = 11;
  public static final int DECIMAL_WIDEAL = 3;
  public static final int SIZE_WCONSM = 11;
  public static final int DECIMAL_WCONSM = 3;
  public static final int SIZE_WMAX = 11;
  public static final int DECIMAL_WMAX = 3;
  public static final int SIZE_WDCS = 1;
  public static final int DECIMAL_WDCS = 0;
  public static final int SIZE_TOTALE_DS = 147;
  public static final int SIZE_FILLER = 200 - 147;
  
  // Constantes indices Nom DS
  public static final int VAR_WETB = 0;
  public static final int VAR_WMAG = 1;
  public static final int VAR_WART = 2;
  public static final int VAR_WSTK = 3;
  public static final int VAR_WRES = 4;
  public static final int VAR_WATT = 5;
  public static final int VAR_WAFF = 6;
  public static final int VAR_WDIV = 7;
  public static final int VAR_WDIA = 8;
  public static final int VAR_WATD = 9;
  public static final int VAR_WMINI = 10;
  public static final int VAR_WIDEAL = 11;
  public static final int VAR_WCONSM = 12;
  public static final int VAR_WMAX = 13;
  public static final int VAR_WDCS = 14;
  
  // Variables AS400
  private String wetb = ""; //
  private String wmag = ""; // Code Magasin
  private String wart = ""; // Code Article
  private BigDecimal wstk = BigDecimal.ZERO; // Quantité stock
  private BigDecimal wres = BigDecimal.ZERO; // Quantité réservée
  private BigDecimal watt = BigDecimal.ZERO; // Quantité attendue
  private BigDecimal waff = BigDecimal.ZERO; // Quantité affectée
  private BigDecimal wdiv = BigDecimal.ZERO; // Quantité disponible Vente
  private BigDecimal wdia = BigDecimal.ZERO; // Quantité disponible Achat
  private BigDecimal watd = BigDecimal.ZERO; // ??
  private BigDecimal wmini = BigDecimal.ZERO; // Stock minimum
  private BigDecimal wideal = BigDecimal.ZERO; // Stock idéal (lot de cmnde)
  private BigDecimal wconsm = BigDecimal.ZERO; // Conso moyenne mensuelle
  private BigDecimal wmax = BigDecimal.ZERO; // Stock maximum
  private BigDecimal wdcs = BigDecimal.ZERO; // Décimalisation un. de stock
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WETB), //
      new AS400Text(SIZE_WMAG), // Code Magasin
      new AS400Text(SIZE_WART), // Code Article
      new AS400ZonedDecimal(SIZE_WSTK, DECIMAL_WSTK), // Quantité stock
      new AS400ZonedDecimal(SIZE_WRES, DECIMAL_WRES), // Quantité réservée
      new AS400ZonedDecimal(SIZE_WATT, DECIMAL_WATT), // Quantité attendue
      new AS400ZonedDecimal(SIZE_WAFF, DECIMAL_WAFF), // Quantité affectée
      new AS400ZonedDecimal(SIZE_WDIV, DECIMAL_WDIV), // Quantité disponible Vente
      new AS400ZonedDecimal(SIZE_WDIA, DECIMAL_WDIA), // Quantité disponible Achat
      new AS400ZonedDecimal(SIZE_WATD, DECIMAL_WATD), // ??
      new AS400ZonedDecimal(SIZE_WMINI, DECIMAL_WMINI), // Stock minimum
      new AS400ZonedDecimal(SIZE_WIDEAL, DECIMAL_WIDEAL), // Stock idéal (lot de cmnde)
      new AS400ZonedDecimal(SIZE_WCONSM, DECIMAL_WCONSM), // Conso moyenne mensuelle
      new AS400ZonedDecimal(SIZE_WMAX, DECIMAL_WMAX), // Stock maximum
      new AS400ZonedDecimal(SIZE_WDCS, DECIMAL_WDCS), // Décimalisation un. de stock
      new AS400Text(SIZE_FILLER), // Filler
  };
  private AS400Structure ds = new AS400Structure(structure);
  public Object[] o = { wetb, wmag, wart, wstk, wres, watt, waff, wdiv, wdia, watd, wmini, wideal, wconsm, wmax, wdcs, filler, };
  
  // -- Accesseurs
  
  public void setWetb(String pWetb) {
    if (pWetb == null) {
      return;
    }
    wetb = pWetb;
  }
  
  public String getWetb() {
    return wetb;
  }
  
  public void setWmag(String pWmag) {
    if (pWmag == null) {
      return;
    }
    wmag = pWmag;
  }
  
  public String getWmag() {
    return wmag;
  }
  
  public void setWart(String pWart) {
    if (pWart == null) {
      return;
    }
    wart = pWart;
  }
  
  public String getWart() {
    return wart;
  }
  
  public void setWstk(BigDecimal pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = pWstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWstk(Double pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = BigDecimal.valueOf(pWstk).setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWstk() {
    return wstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWres(BigDecimal pWres) {
    if (pWres == null) {
      return;
    }
    wres = pWres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWres(Double pWres) {
    if (pWres == null) {
      return;
    }
    wres = BigDecimal.valueOf(pWres).setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWres() {
    return wres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWatt(BigDecimal pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = pWatt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWatt(Double pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = BigDecimal.valueOf(pWatt).setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWatt() {
    return watt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWaff(BigDecimal pWaff) {
    if (pWaff == null) {
      return;
    }
    waff = pWaff.setScale(DECIMAL_WAFF, RoundingMode.HALF_UP);
  }
  
  public void setWaff(Double pWaff) {
    if (pWaff == null) {
      return;
    }
    waff = BigDecimal.valueOf(pWaff).setScale(DECIMAL_WAFF, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWaff() {
    return waff.setScale(DECIMAL_WAFF, RoundingMode.HALF_UP);
  }
  
  public void setWdiv(BigDecimal pWdiv) {
    if (pWdiv == null) {
      return;
    }
    wdiv = pWdiv.setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public void setWdiv(Double pWdiv) {
    if (pWdiv == null) {
      return;
    }
    wdiv = BigDecimal.valueOf(pWdiv).setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWdiv() {
    return wdiv.setScale(DECIMAL_WDIV, RoundingMode.HALF_UP);
  }
  
  public void setWdia(BigDecimal pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = pWdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWdia(Double pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = BigDecimal.valueOf(pWdia).setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWdia() {
    return wdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWatd(BigDecimal pWatd) {
    if (pWatd == null) {
      return;
    }
    watd = pWatd.setScale(DECIMAL_WATD, RoundingMode.HALF_UP);
  }
  
  public void setWatd(Double pWatd) {
    if (pWatd == null) {
      return;
    }
    watd = BigDecimal.valueOf(pWatd).setScale(DECIMAL_WATD, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWatd() {
    return watd.setScale(DECIMAL_WATD, RoundingMode.HALF_UP);
  }
  
  public void setWmini(BigDecimal pWmini) {
    if (pWmini == null) {
      return;
    }
    wmini = pWmini.setScale(DECIMAL_WMINI, RoundingMode.HALF_UP);
  }
  
  public void setWmini(Double pWmini) {
    if (pWmini == null) {
      return;
    }
    wmini = BigDecimal.valueOf(pWmini).setScale(DECIMAL_WMINI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmini() {
    return wmini.setScale(DECIMAL_WMINI, RoundingMode.HALF_UP);
  }
  
  public void setWideal(BigDecimal pWideal) {
    if (pWideal == null) {
      return;
    }
    wideal = pWideal.setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public void setWideal(Double pWideal) {
    if (pWideal == null) {
      return;
    }
    wideal = BigDecimal.valueOf(pWideal).setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWideal() {
    return wideal.setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public void setWconsm(BigDecimal pWconsm) {
    if (pWconsm == null) {
      return;
    }
    wconsm = pWconsm.setScale(DECIMAL_WCONSM, RoundingMode.HALF_UP);
  }
  
  public void setWconsm(Double pWconsm) {
    if (pWconsm == null) {
      return;
    }
    wconsm = BigDecimal.valueOf(pWconsm).setScale(DECIMAL_WCONSM, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWconsm() {
    return wconsm.setScale(DECIMAL_WCONSM, RoundingMode.HALF_UP);
  }
  
  public void setWmax(BigDecimal pWmax) {
    if (pWmax == null) {
      return;
    }
    wmax = pWmax.setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public void setWmax(Double pWmax) {
    if (pWmax == null) {
      return;
    }
    wmax = BigDecimal.valueOf(pWmax).setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmax() {
    return wmax.setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public void setWdcs(BigDecimal pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = pWdcs.setScale(DECIMAL_WDCS, RoundingMode.HALF_UP);
  }
  
  public void setWdcs(Integer pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = BigDecimal.valueOf(pWdcs);
  }
  
  public Integer getWdcs() {
    return wdcs.intValue();
  }
}
