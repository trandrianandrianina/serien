/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx003.v3;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonSuiviCommandeWebV3;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;

public class ExportationSuiviCommandeV3 extends BaseGroupDB {
  // Variables
  private SystemeManager systeme = null;
  
  /**
   * Constructeur.
   */
  public ExportationSuiviCommandeV3(QueryManager pQueryManager) {
    super(pQueryManager);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner un suivi de commande Série N sous forme de message JSON
   */
  public JsonSuiviCommandeWebV3 retournerUnSuiviCommandeWeb(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null || pFluxMagento.getFLCOD() == null) {
      majError("[ExportationSuiviCommandeV3] retournerUnSuiviCommandeWeb() - Record Null ou corrompu");
      return null;
    }
    
    // Chargement du client à partir de la base Série N
    ArrayList<GenericRecord> suivisCommandeDB2 = retournerUnSuiviCommandeDB2(pFluxMagento.getFLETB(), pFluxMagento.getFLCOD());
    if (suivisCommandeDB2 == null) {
      return null;
    }
    
    // Préparation des données du suivi de commande qui va être exporté vers SFCC
    JsonSuiviCommandeWebV3 suiviCommandeWeb = new JsonSuiviCommandeWebV3(pFluxMagento);
    Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), suiviCommandeWeb.getEtb());
    if (numeroInstance == null) {
      majError("[ExportationSuiviCommandeV3] retournerUnSuiviCommandeWeb() - Problème d'interprétation de l'ETb vers l'instance Magento");
      return null;
    }
    suiviCommandeWeb.setIdInstanceMagento(numeroInstance);
    if (!suiviCommandeWeb.mettreAJoursuiviCommande(suivisCommandeDB2)) {
      majError(suiviCommandeWeb.getMsgError());
      return null;
    }
    
    // Conversion de l'objet en Json pour le stocker dans l'enregistrement et contrôle les données de base
    if (!initJSON()) {
      majError("[ExportationSuiviCommandeV3] retournerUnSuiviCommandeWeb() - Problème d'initialisation pour la sérialisation Json.");
      return null;
    }
    try {
      // Conversion du record en JSON
      pFluxMagento.setFLJSN(gson.toJson(suiviCommandeWeb));
    }
    catch (Exception e) {
      majError("[ExportationSuiviCommandeV3] retournerUnSuiviCommandeWeb() - Erreur lors de la conversion de l'enregistrement en Json: "
          + e.getMessage());
    }
    
    return suiviCommandeWeb;
  }
  
  /**
   * Retourner le suivi d'une commande Série N sous forme GenericRecord : A AMELIORER AVEC LES CLASSES METIER COMMUNES DE STEPH.
   */
  protected ArrayList<GenericRecord> retournerUnSuiviCommandeDB2(String pCodeEtablissement, String pNumeroDocument) {
    if (pCodeEtablissement == null || pNumeroDocument == null) {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeDB2() etb ou numBon Null ou corrompus");
      return null;
    }
    
    String numero = null;
    String suffixe = null;
    
    // Découpage du paramètre code entre numéro de commande et suffixe (6 caractères + 1) on élimine les 4 premiers caractères (type de
    // bon sur 1 + société sur 3)
    if (pNumeroDocument.length() == 11) {
      numero = pNumeroDocument.trim().substring(4, pNumeroDocument.length() - 1);
      suffixe = pNumeroDocument.trim().substring(pNumeroDocument.length() - 1);
    }
    else {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeDB2() numBon longueur != 11: " + pNumeroDocument);
      return null;
    }
    
    // Le test du E1COD doit se faire sur la valeur 'E' pour la commande normale ou '9' pour une commande annulée (si la PS66 = '2')
    // Changement à cause de l'introduction du 'e' pour le comptoir
    ArrayList<GenericRecord> listeCommandes = queryManager
        .select("SELECT E1COD, E1CCT, E1ETA, E1AVR, COTRK FROM " + queryManager.getLibrary() + ".PGVMEBCM LEFT JOIN " + queryManager.getLibrary()
            + ".PGVMNCOM ON E1ETB = COETB AND E1NUM = CONUM AND E1SUF = COSUF WHERE (E1COD = 'E' OR E1COD = '9') AND E1ETB = '"
            + pCodeEtablissement.trim() + "' AND E1NUM = '" + numero + "' AND E1SUF = '" + suffixe + "'  ");
    
    if (listeCommandes != null && listeCommandes.size() > 0) {
      return listeCommandes;
    }
    else {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeDB2() listeCommandes à NULL ou 0 pour : " + pCodeEtablissement + "/"
          + pNumeroDocument);
      return null;
    }
  }
  
  @Override
  public void dispose() {
    queryManager = null;
    if (systeme != null) {
      systeme.deconnecter();
    }
  }
}
