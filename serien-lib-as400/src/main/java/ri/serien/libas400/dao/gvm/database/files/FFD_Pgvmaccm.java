/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

/**
 * Traitement pour UNE action commerciale
 * Classe contenant les zones du PGVMACCM
 * @author Administrateur
 * 
 */
public abstract class FFD_Pgvmaccm extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_ACID = 9;
  public static final int DECIMAL_ACID = 0;
  public static final int SIZE_ACOBJ = 5;
  public static final int SIZE_ACEBCN = 6;
  public static final int SIZE_ACEBCC = 1;
  public static final int DECIMAL_ACEBCN = 0;
  public static final int SIZE_ACEBCS = 1;
  public static final int DECIMAL_ACEBCS = 0;
  public static final int SIZE_ACAFN = 6;
  public static final int DECIMAL_ACAFN = 0;
  
  // Variables fichiers
  // ID, le même que ETID puisque c'est du complément d'information
  protected int ACID = 0;
  // Objet (Visite de courtoisie/devis/commande/récupération de règlement/...) - stocké dans le PGVMPARM paramètre AA
  protected String ACOBJ = null;
  // Code ERL (devis/commande)
  protected char ACEBCC = ' ';
  // Numéro de bon (devis/commande)
  protected int ACEBCN = 0;
  // Suffixe (devis/commande)
  protected byte ACEBCS = 0;
  // Numéro d'ordre affaire
  protected int ACAFN = 0;
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public FFD_Pgvmaccm(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    ACID = 0;
    // Objet (Visite de courtoisie/devis/commande/récupération de règlement/...) - stocké dans le PGVMPARM paramètre AA
    ACOBJ = null;
    // Code ERL (devis/commande)
    ACEBCC = ' ';
    ACEBCN = 0;
    ACEBCS = 0;
    ACAFN = 0;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le aCID
   */
  public int getACID() {
    return ACID;
  }
  
  /**
   * @param aCID le aCID à définir
   */
  public void setACID(int aCID) {
    ACID = aCID;
  }
  
  /**
   * @return le aCOBJ
   */
  public String getACOBJ() {
    return ACOBJ;
  }
  
  /**
   * @param aCOBJ le aCOBJ à définir
   */
  public void setACOBJ(String aCOBJ) {
    ACOBJ = aCOBJ;
  }
  
  /**
   * @return le aCEBCC
   */
  public char getACEBCC() {
    return ACEBCC;
  }
  
  /**
   * @param aCEBCC le aCEBCC à définir
   */
  public void setACEBCC(char aCEBCC) {
    ACEBCC = aCEBCC;
  }
  
  /**
   * @return le aCEBCS
   */
  public byte getACEBCS() {
    return ACEBCS;
  }
  
  /**
   * @param aCEBCS le aCEBCS à définir
   */
  public void setACEBCS(byte aCEBCS) {
    ACEBCS = aCEBCS;
  }
  
  /**
   * @return le aCEBCN
   */
  public int getACEBCN() {
    return ACEBCN;
  }
  
  /**
   * @param aCEBCN le aCEBCN à définir
   */
  public void setACEBCN(int aCEBCN) {
    ACEBCN = aCEBCN;
  }
  
  /**
   * @return le aCAFN
   */
  public int getACAFN() {
    return ACAFN;
  }
  
  /**
   * @param aCAFN le aCAFN à définir
   */
  public void setACAFN(int aCAFN) {
    ACAFN = aCAFN;
  }
  
}
