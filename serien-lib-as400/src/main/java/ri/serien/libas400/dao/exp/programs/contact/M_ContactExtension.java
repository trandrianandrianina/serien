/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.programs.contact;

import ri.serien.libas400.dao.exp.database.files.FFD_Psemrtym;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.mail.InfosMailServer;

/**
 * Classe contenant les zones du PSEMRTYM
 * @author Administrateur
 * 
 */
public class M_ContactExtension extends FFD_Psemrtym {
  // Variables de travail
  private InfosMailServer infosMailServer = null;
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public M_ContactExtension(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Insère l'enregistrement dans le table
   * @return
   * 
   */
  @Override
  public boolean insertInDatabase() {
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert("PSEMRTYM", querymg.getLibrary());
    return request(requete);
  }
  
  /**
   * Modifie l'enregistrement dans le table
   * @return
   * 
   */
  @Override
  public boolean updateInDatabase() {
    initGenericRecord(genericrecord, false);
    String requete =
        genericrecord.createSQLRequestUpdate("PSEMRTYM", querymg.getLibrary(), "RENUM=" + getRYNUM() + " and REETB='" + getRYETB() + "'");
    return request(requete);
  }
  
  /**
   * Suppression de l'enregistrement courant
   * @return
   * 
   */
  @Override
  public boolean deleteInDatabase() {
    String requete = "delete from " + querymg.getLibrary() + ".PSEMRTYM where RENUM=" + getRYNUM() + " and REETB='" + getRYETB() + "'";
    return request(requete);
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
  }
  
  /**
   * @return le infosMailServer
   */
  public InfosMailServer getInfosMailServer() {
    if (infosMailServer == null) {
      infosMailServer = new InfosMailServer();
      infosMailServer.setHosto(RYSRVS);
      infosMailServer.setPorto(RYPORS);
      infosMailServer.setHosti(RYSRVE);
      infosMailServer.setPorti(RYPORE);
      infosMailServer.setEmail(RYMAIL);
      infosMailServer.setLogin(RYCPTE);
      infosMailServer.setPassword(RYPASS);
    }
    return infosMailServer;
  }
  
  /**
   * @param infosMailServer le infosMailServer à définir
   */
  public void setInfosMailServer(InfosMailServer infosMailServer) {
    this.infosMailServer = infosMailServer;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  // -- Accesseurs ----------------------------------------------------------
  
}
