/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0026i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PICLI = 6;
  public static final int DECIMAL_PICLI = 0;
  public static final int SIZE_PINCG = 6;
  public static final int DECIMAL_PINCG = 0;
  public static final int SIZE_PI0TTC = 9;
  public static final int DECIMAL_PI0TTC = 2;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 43;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PICLI = 5;
  public static final int VAR_PINCG = 6;
  public static final int VAR_PI0TTC = 7;
  public static final int VAR_PIARR = 8;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "D","E",X","9"
  private String pietb = ""; // Code établissement
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe
  private BigDecimal picli = BigDecimal.ZERO; // Numéro client
  private BigDecimal pincg = BigDecimal.ZERO; // Numéro compte général
  private BigDecimal pi0ttc = BigDecimal.ZERO; // Montant avant modif. documnt
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9"
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe
      new AS400ZonedDecimal(SIZE_PICLI, DECIMAL_PICLI), // Numéro client
      new AS400ZonedDecimal(SIZE_PINCG, DECIMAL_PINCG), // Numéro compte général
      new AS400ZonedDecimal(SIZE_PI0TTC, DECIMAL_PI0TTC), // Montant avant modif. documnt
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, picod, pietb, pinum, pisuf, picli, pincg, pi0ttc, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    picli = (BigDecimal) output[5];
    pincg = (BigDecimal) output[6];
    pi0ttc = (BigDecimal) output[7];
    piarr = (String) output[8];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPicli(BigDecimal pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = pPicli.setScale(DECIMAL_PICLI, RoundingMode.HALF_UP);
  }
  
  public void setPicli(Integer pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = BigDecimal.valueOf(pPicli);
  }
  
  public Integer getPicli() {
    return picli.intValue();
  }
  
  public void setPincg(BigDecimal pPincg) {
    if (pPincg == null) {
      return;
    }
    pincg = pPincg.setScale(DECIMAL_PINCG, RoundingMode.HALF_UP);
  }
  
  public void setPincg(Integer pPincg) {
    if (pPincg == null) {
      return;
    }
    pincg = BigDecimal.valueOf(pPincg);
  }
  
  public Integer getPincg() {
    return pincg.intValue();
  }
  
  public void setPi0ttc(BigDecimal pPi0ttc) {
    if (pPi0ttc == null) {
      return;
    }
    pi0ttc = pPi0ttc.setScale(DECIMAL_PI0TTC, RoundingMode.HALF_UP);
  }
  
  public void setPi0ttc(Double pPi0ttc) {
    if (pPi0ttc == null) {
      return;
    }
    pi0ttc = BigDecimal.valueOf(pPi0ttc).setScale(DECIMAL_PI0TTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPi0ttc() {
    return pi0ttc;
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
