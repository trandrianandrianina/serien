/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.documentvente;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.ListeIdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlLigneVente {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlLigneVente(QueryManager pQquerymg) {
    querymg = pQquerymg;
  }
  
  // -- Méthodes publiques
  
  /**
   * Supprime une ligne de vente donnée.
   */
  public boolean supprimerLigne(LigneVente pLigneVente) {
    LigneVente.controlerId(pLigneVente, true);
    IdLigneVente idLigneVente = pLigneVente.getId();
    
    RequeteSql requete = new RequeteSql();
    requete.ajouter(
        "update " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_LIGNE + " set L1CEX = 'A', L1QEX = L1QTE where ");
    requete.ajouterConditionAnd("L1COD", "=", idLigneVente.getCodeEntete().getCode());
    requete.ajouterConditionAnd("L1ETB", "=", idLigneVente.getCodeEtablissement());
    requete.ajouterConditionAnd("L1NUM", "=", idLigneVente.getNumero());
    requete.ajouterConditionAnd("L1SUF", "=", idLigneVente.getSuffixe());
    requete.ajouterConditionAnd("L1NLI", "=", idLigneVente.getNumeroLigne());
    
    if (querymg.requete(requete.getRequete()) == Constantes.ERREUR) {
      throw new MessageErreurException("Erreur lors de la suppresion de la ligne de vente.");
    }
    return true;
  }
  
  /**
   * Recherche toutes les id des lignes contenant un code article donné d'un document donné.
   */
  public List<IdLigneVente> chargerIdLigneVenteParCodeArticle(CritereLigneVente pCriteres) {
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères de recherche des lignes de vente sont invalides.");
    }
    IdDocumentVente idDocumentVente = pCriteres.getIdDocument();
    IdDocumentVente.controlerId(idDocumentVente, true);
    IdArticle idArticle = pCriteres.getIdArticle();
    IdArticle.controlerId(idArticle, true);
    
    RequeteSql requete = new RequeteSql();
    if (idDocumentVente.isIdFacture()) {
      requete
          .ajouter("SELECT L1COD, L1NUM, L1SUF, L1NLI FROM " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_LIGNE
              + ", " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " WHERE");
      requete.ajouterConditionAnd("E1ETB", "=", idDocumentVente.getCodeEtablissement());
      requete.ajouterConditionAnd("E1NFA", "=", idDocumentVente.getNumeroFacture());
      requete.ajouterConditionAnd("L1ETB", "=", "E1ETB");
      requete.ajouterConditionAnd("L1COD", "=", "E1COD");
      requete.ajouterConditionAnd("L1NUM", "=", "E1NUM");
      requete.ajouterConditionAnd("L1SUF", "=", "E1SUF");
    }
    else {
      requete.ajouter("SELECT L1COD, L1NUM, L1SUF, L1NLI FROM " + querymg.getLibrary() + '.'
          + EnumTableBDD.DOCUMENT_VENTE_LIGNE + " WHERE");
      requete.ajouterConditionAnd("L1ETB", "=", idDocumentVente.getCodeEtablissement());
      requete.ajouterConditionAnd("L1COD", "=", idDocumentVente.getEntete());
      requete.ajouterConditionAnd("L1NUM", "=", idDocumentVente.getNumero());
      requete.ajouterConditionAnd("L1SUF", "=", idDocumentVente.getSuffixe());
      requete.ajouterConditionOrAuGroupe("CODE", "L1COD", "=", 'd');
      requete.ajouterConditionOrAuGroupe("CODE", "L1COD", "=", 'D');
      requete.ajouterConditionOrAuGroupe("CODE", "L1COD", "=", 'e');
      requete.ajouterConditionOrAuGroupe("CODE", "L1COD", "=", 'E');
      requete.ajouterGroupeDansRequete("CODE", "AND");
    }
    requete.ajouterConditionAnd("L1ERL", "<>", '*');
    requete.ajouterConditionAnd("L1CEX", "<>", 'A');
    requete.ajouterConditionAnd("L1ART", "=", idArticle.getCodeArticle());
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    List<IdLigneVente> listeIdLigneVente = new ArrayList<IdLigneVente>();
    for (GenericRecord record : listeRecord) {
      EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(record.getCharacterValue("L1COD"));
      Integer numero = record.getIntegerValue("L1NUM", 0);
      Integer suffixe = record.getIntegerValue("L1SUF", 0);
      Integer numeroLigne = record.getIntegerValue("L1NLI", 0);
      IdLigneVente idLigneVente =
          IdLigneVente.getInstance(idDocumentVente.getIdEtablissement(), codeEntete, numero, suffixe, numeroLigne);
      listeIdLigneVente.add(idLigneVente);
    }
    return listeIdLigneVente;
  }
  
  /**
   * Charger la liste des id des lignes de vente dont les articles sont à approvisionner.
   * Que les articles sont en stock ou hors stock cela dépendra des critères de recherche.
   */
  public List<IdLigneVente> chargerListeIdLigneVenteAApprovisionner(CritereLigneVente pCriteres) {
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères de recherche des lignes de vente sont invalides.");
    }
    if (pCriteres.getCodeEtablissement() == null) {
      throw new MessageErreurException("Le code de l'établissement est invalide.");
    }
    IdMagasin idMagasin = pCriteres.getIdMagasin();
    if (idMagasin == null) {
      throw new MessageErreurException("L'id du magasin est invalide.");
    }
    IdFournisseur.controlerId(pCriteres.getIdFournisseur(), true);
    
    // Puisque ce sont des articles à approvisionner
    pCriteres.setExlusionLignesEnCoursDeCommande(true);
    
    // Eliminer les remises spéciales
    pCriteres.setExclureRemiseSpeciale(true);
    
    // Construction de la requête
    RequeteSql requete = construireRequete(pCriteres);
    if (requete == null) {
      throw new MessageErreurException("La requête pour la recherche des lignes de vente est invalide.");
    }
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    List<IdLigneVente> listeIdLigneVente = new ArrayList<IdLigneVente>();
    for (GenericRecord record : listeRecord) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("L1ETB"));
      EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(record.getCharacterValue("L1COD"));
      Integer numero = record.getIntegerValue("L1NUM", 0);
      Integer suffixe = record.getIntegerValue("L1SUF", 0);
      Integer numeroLigne = record.getIntegerValue("L1NLI", 0);
      IdLigneVente idLigneVente = IdLigneVente.getInstance(idEtablissement, codeEntete, numero, suffixe, numeroLigne);
      listeIdLigneVente.add(idLigneVente);
    }
    return listeIdLigneVente;
  }
  
  /**
   * Récupère les id des documents d'une liste d'id des lignes de vente.
   * TODO SNC-5110 Provisoire jusqu'à ce que l'on ai chargerLigneVenteBase
   */
  public List<LigneVente> chargerListeLigneVente(List<IdLigneVente> pListeIdLigneVente) {
    if (pListeIdLigneVente == null || pListeIdLigneVente.isEmpty()) {
      return null;
    }
    // Construction de la requête
    RequeteSql requete = new RequeteSql();
    requete.ajouter("select E1ETB, E1COD, E1NUM, E1SUF, E1NFA from " + querymg.getLibrary() + '.'
        + EnumTableBDD.DOCUMENT_VENTE + " where");
    // Construction du groupe contenant les pseudo id des documents de vente (pas l'id définitif)
    for (IdLigneVente id : pListeIdLigneVente) {
      requete.ajouterValeurAuGroupe("iddocument",
          String.format(
              "%" + IdLigneVente.LONGUEUR_CODE_ETABLISSEMENT + "s%c%0" + IdLigneVente.LONGUEUR_NUMERO + "d%0"
                  + IdLigneVente.LONGUEUR_SUFFIXE + "d",
              id.getCodeEtablissement(), id.getCodeEntete().getCode(), id.getNumero(), id.getSuffixe()));
    }
    requete.ajouter("concat(E1ETB, concat(E1COD, concat(E1NUM, E1SUF)))");
    requete.ajouterGroupeDansRequete("iddocument", "in");
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    List<LigneVente> listeLigneVente = new ArrayList<LigneVente>();
    for (GenericRecord record : listeRecord) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("E1ETB"));
      EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(record.getCharacterValue("E1COD"));
      Integer numero = record.getIntegerValue("E1NUM", 0);
      Integer suffixe = record.getIntegerValue("E1SUF", 0);
      Integer numeroFacture = record.getIntegerValue("E1NFA", 0);
      // Création de l'id du document à partir des données récupérées en base
      IdDocumentVente idDocumentVente = IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, numero, suffixe, numeroFacture);
      // Recherche de l'id de la ligne de vente afin de créer un oblet ligne de vente et affecter l'id du document de vente
      for (IdLigneVente id : pListeIdLigneVente) {
        if (id.getIdEtablissement().equals(idEtablissement) && id.getCodeEntete().equals(codeEntete) && id.getNumero().equals(numero)
            && id.getSuffixe().equals(suffixe)) {
          LigneVente ligneVente = new LigneVente(id);
          ligneVente.setIdDocumentVente(idDocumentVente);
          listeLigneVente.add(ligneVente);
          break;
        }
      }
    }
    return listeLigneVente;
  }
  
  // -- Méthodes privées
  
  /**
   * Construit la requête pour une recherche de lignes de vente des directs usines.
   */
  private RequeteSql construireRequete(CritereLigneVente pCriteres) {
    IdFournisseur idFournisseur = pCriteres.getIdFournisseur();
    
    // Requête principale
    RequeteSql requete = new RequeteSql();
    requete.ajouter(
        "select L1COD, L1ETB, L1NUM, L1SUF, L1NLI from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_LIGNE);
    
    // Ajouter la jointure avec l'entête du document de vente
    requete.ajouter("inner join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " on");
    requete.ajouterConditionAnd("E1COD", "=", "L1COD", true, false);
    requete.ajouterConditionAnd("E1ETB", "=", "L1ETB", true, false);
    requete.ajouterConditionAnd("E1NUM", "=", "L1NUM", true, false);
    requete.ajouterConditionAnd("E1SUF", "=", "L1SUF", true, false);
    
    // Ajouter la jointure avec l'article
    requete.ajouter("inner join " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " on");
    requete.ajouterConditionAnd("A1ETB", "=", "L1ETB", true, false);
    requete.ajouterConditionAnd("A1ART", "=", "L1ART", true, false);
    
    // Ajouter la jointure facultative avec l'extension 74 du document de vente (si direct usine)
    if (pCriteres.isCommandeDirectUsine() != null && pCriteres.isCommandeDirectUsine()) {
      requete.ajouter("left outer join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_EXTENSION + " on");
      requete.ajouterConditionAnd("XITOP", "=", ' ');
      requete.ajouterConditionAnd("XICOD", "=", "L1COD", true, false);
      requete.ajouterConditionAnd("XIETB", "=", "L1ETB", true, false);
      requete.ajouterConditionAnd("XINUM", "=", "L1NUM", true, false);
      requete.ajouterConditionAnd("XISUF", "=", "L1SUF", true, false);
      requete.ajouterConditionAnd("XINLI", "=", 0);
      requete.ajouterConditionAnd("XITYP", "=", 74);
    }
    
    // Ajouter la clause WHERE
    requete.ajouter("where");
    
    // Ajouter les conditions qui concernent les lignes du document
    requete.ajouterConditionAnd("L1COD", "=", EnumCodeEnteteDocumentVente.COMMANDE_OU_BON.getCode());
    requete.ajouterConditionAnd("L1ETB", "=", pCriteres.getCodeEtablissement());
    requete.ajouter(" and ((E1MAG = '" + pCriteres.getIdMagasin().getCode() + "' and L1MAG = ' ') OR L1MAG = '"
        + pCriteres.getIdMagasin().getCode() + "')");
    
    // Ajouter les conditions qui concernent l'entête du document
    requete.ajouter("and E1ETA in(1, 3)");
    
    // Ajouter la sous requête qui contrôle le stock disponible pour les articles gérés en stock
    RequeteSql sousRequeteStockDisponible = new RequeteSql();
    sousRequeteStockDisponible.ajouter("select case when "
        + "(S1STD + S1QEE + S1QSE + S1QDE + S1QEM + S1QSM + S1QDM + S1QES + S1QSS + S1QDS + S1ATT - S1RES) <= 0 then -1 else 1 end"
        + " as qtedispo from " + querymg.getLibrary() + '.' + EnumTableBDD.STOCK
        + " where S1ETB = L1ETB and (S1MAG = L1MAG OR S1MAG = E1MAG) and S1ART = L1ART");
    requete.ajouter("and (L1ERL = 'S' or (L1ERL = 'C' and ((" + sousRequeteStockDisponible.getRequete() + ") < 0)))");
    
    // Ajouter la condition sur le client facturé
    if (pCriteres.getIdClientFacture() != null) {
      requete.ajouterConditionAnd("E1CLFP", "=", pCriteres.getIdClientFacture().getNumero());
      requete.ajouterConditionAnd("E1CLFS", "=", pCriteres.getIdClientFacture().getSuffixe());
    }
    
    // Ajouter la condition sur les documents direct usine ou non
    if (pCriteres.isCommandeDirectUsine() != null) {
      if (pCriteres.isCommandeDirectUsine()) {
        requete.ajouterConditionAnd("E1IN18", "=", 'D');
      }
      else {
        requete.ajouterConditionAnd("E1IN18", "<>", 'D');
      }
    }
    
    // Ajouter condition sur les remises spéciales
    if (pCriteres.getExclureRemiseSpeciale() != null && pCriteres.getExclureRemiseSpeciale()) {
      requete.ajouterConditionAnd("L1NLI", "<=", LigneVenteBase.NUMERO_LIGNE_POUR_REMISE_SPECIALE);
    }
    
    // Ajouter la condition sur le fournisseur des directs usines
    // L'extension 74 sert à stocker le fournisseur dans le cas de direct usine mono-fournisseur. Noter que dans le cas de direct
    // usine multi-fournisseur, l'extension 74 n'est pas renseignée et il faut récupérer le fournisseur principal de l'article
    if (pCriteres.isCommandeDirectUsine() != null && pCriteres.isCommandeDirectUsine() && idFournisseur != null) {
      requete.ajouter("and (");
      requete.ajouter("substring(XILIB, 1, " + IdFournisseur.LONGUEUR_INDICATIF_COURT + ") = "
          + RequeteSql.formaterStringSQL(idFournisseur.getIndicatifCourt()));
      requete.ajouter("or");
      requete.ajouter("(XILIB is null and a1cof=" + idFournisseur.getCollectif() + " and a1frs=" + idFournisseur.getNumero() + ")");
      requete.ajouter(")");
    }
    
    // Ajouter la condition sur le fournisseur dans le cas où la recherche ne concerne pas des directs usines
    if ((pCriteres.isCommandeDirectUsine() == null || !pCriteres.isCommandeDirectUsine()) && idFournisseur != null) {
      requete.ajouterConditionAnd("A1COF", "=", idFournisseur.getCollectif());
      requete.ajouterConditionAnd("A1FRS", "=", idFournisseur.getNumero());
    }
    
    // On construit la condition sur les types d'articles
    // Article spécial
    String conditionArticleSpecial = "";
    if (pCriteres.isContenantArticleSpecial() != null && pCriteres.isContenantArticleSpecial()) {
      conditionArticleSpecial = "A1SPE = 1";
    }
    
    // Article hors-gamme
    String conditionArticleHorsGamme = "";
    if (pCriteres.isContenantArticleHorsGamme() != null && pCriteres.isContenantArticleHorsGamme()) {
      conditionArticleHorsGamme = "A1ABC = 'R'";
    }
    
    // Article non géré en stock
    String conditionArticleNonStocke = "";
    if (pCriteres.isContenantArticleNonStockes() != null && pCriteres.isContenantArticleNonStockes()) {
      conditionArticleNonStocke = "A1STK = '1'";
    }
    
    // Ajouter les conditions sur l'article
    if (!conditionArticleSpecial.isEmpty() && !conditionArticleHorsGamme.isEmpty() && !conditionArticleNonStocke.isEmpty()) {
      requete.ajouter(" and (" + conditionArticleSpecial + " or " + conditionArticleHorsGamme + " or " + conditionArticleNonStocke + ")");
    }
    else if (!conditionArticleSpecial.isEmpty() && !conditionArticleHorsGamme.isEmpty()) {
      requete.ajouter(" and (" + conditionArticleSpecial + " or " + conditionArticleHorsGamme + ")");
    }
    else if (!conditionArticleNonStocke.isEmpty() && !conditionArticleHorsGamme.isEmpty()) {
      requete.ajouter(" and (" + conditionArticleNonStocke + " or " + conditionArticleHorsGamme + ")");
    }
    else if (!conditionArticleSpecial.isEmpty() && !conditionArticleNonStocke.isEmpty()) {
      requete.ajouter(" and (" + conditionArticleSpecial + " or " + conditionArticleNonStocke + ")");
    }
    else if (!conditionArticleNonStocke.isEmpty()) {
      requete.ajouter(" and " + conditionArticleNonStocke);
    }
    else if (!conditionArticleSpecial.isEmpty()) {
      requete.ajouter(" and " + conditionArticleSpecial);
    }
    else if (!conditionArticleHorsGamme.isEmpty()) {
      requete.ajouter(" and " + conditionArticleHorsGamme);
    }
    
    // Ajouter la sous requête sur l'article déjà commandé
    if (pCriteres.isExlusionLignesEnCoursDeCommande()) {
      // Création de la sous requête qui récupère les articles qui sont en cours de commandes
      RequeteSql sousRequeteArticleCommande = new RequeteSql();
      sousRequeteArticleCommande.ajouter("select AATOP3 from " + querymg.getLibrary() + '.'
          + EnumTableBDD.LIEN_LIGNE_VENTE_LIGNE_ACHAT
          + " where AAETB = L1ETB and AAART = L1ART and (AAMAG = L1MAG OR AAMAG = E1MAG) and AATOS = 'V' and AACOS = L1COD and AANOS = L1NUM "
          + " and AASOS = L1SUF and AALOS = L1NLI");
      // Insertion de la sous requête dans la requête principale
      requete.ajouter("and not exists (" + sousRequeteArticleCommande.getRequete() + ")");
    }
    
    // Ajouter le tri
    requete.ajouter("order by L1NUM desc, L1COD, L1ART");
    
    return requete;
  }
  
  public ListeIdLigneVente chargerToutesIdLigneVenteParCodeArticle(CritereLigneVente pCriteres) {
    // XXX Auto-generated method stub
    return null;
  }
  
  public ListeIdLigneVente chargerListeIdLigneVente(CritereLigneVente pCritereLigneVente) {
    if (pCritereLigneVente == null) {
      throw new MessageErreurException("Les critères de recherche des lignes de vente sont invalides.");
    }
    
    IdEtablissement.controlerId(pCritereLigneVente.getIdEtablissement(), true);
    
    RequeteSql requete = new RequeteSql();
    requete.ajouter("SELECT DISTINCT L1COD, L1NUM, L1SUF, L1NLI, E1CRE FROM " + querymg.getLibrary() + '.'
        + EnumTableBDD.DOCUMENT_VENTE_LIGNE + " INNER JOIN " + querymg.getLibrary() + '.'
        + EnumTableBDD.DOCUMENT_VENTE);
    requete.ajouter(" ON (L1ETB = E1ETB and L1COD = E1COD and L1NUM = E1NUM and L1SUF = E1SUF) ");
    if (pCritereLigneVente.getChantier() != null) {
      requete.ajouter(" left join " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_EXTENSION
          + " xli on xli.XIETB = E1ETB and xli.XICOD = " + "E1COD and xli.XINUM = E1NUM and xli.XISUF = E1SUF and xli.XITYP = '40'");
    }
    requete.ajouter(" WHERE");
    requete.ajouterConditionAnd("L1ETB", "=", pCritereLigneVente.getIdEtablissement().getCodeEtablissement());
    requete.ajouterConditionAnd("L1ERL", "<>", '*');
    requete.ajouterConditionAnd("L1CEX", "<>", 'A');
    requete.ajouterConditionAnd("L1COD", "<>", 'F');
    
    // Ajouter condition sur les remises spéciales
    if (pCritereLigneVente.getExclureRemiseSpeciale() != null && pCritereLigneVente.getExclureRemiseSpeciale()) {
      requete.ajouterConditionAnd("L1NLI", "<=", LigneVenteBase.NUMERO_LIGNE_POUR_REMISE_SPECIALE);
    }
    
    // Ajouter un test sur le prix garanti
    // Le prix garanti est géré sur la ligne dans le champ L1IN18 et sur l'entête dans le champ E1TO1G.
    // Le champ E1TO1G est une zone numérique de 9 chiffres (par exemple "001000013") dont chaque chiffre correspondant à un indicateur
    // indépendant. Le chiffre des dizaines permet de savoir si le document de vente a des prix garanti. Les valeurs possibles sont :
    // - 0 = prix non garanti
    // - 1 = prix garanti
    // - 2 = ancien prix
    // - 3 = prix garanti au devis
    // Alternative pour extraire le chiffre des dizaines : substr(LPAD(Digits(E1TO1G), 9, '0'), 8, 1)
    if (pCritereLigneVente.getPrixGaranti() != null) {
      if (pCritereLigneVente.getPrixGaranti()) {
        requete.ajouter(" and ((L1IN18 <> ' ' and L1IN18 <> '0') or MOD(TRUNCATE(E1TO1G / 10), 10) = 1)");
      }
      else {
        requete.ajouter(" and ((L1IN18 = ' ' or L1IN18 = '0') and MOD(TRUNCATE(E1TO1G / 10), 10) <> 1)");
      }
    }
    
    // Ajouter condition sur les lignes spéciales
    if (pCritereLigneVente.getLigneSpeciale() != null) {
      if (pCritereLigneVente.getLigneSpeciale()) {
        requete.ajouterConditionAnd("L1NLI", ">", 9500);
      }
      else {
        requete.ajouterConditionAnd("L1NLI", "<=", 9500);
      }
    }
    
    requete.ajouterConditionOrAuGroupe("CODE", "L1COD", "=", 'D');
    requete.ajouterConditionOrAuGroupe("CODE", "L1COD", "=", 'E');
    if (!pCritereLigneVente.isSansHistorique()) {
      requete.ajouterConditionOrAuGroupe("CODE", "L1COD", "=", 'd');
      requete.ajouterConditionOrAuGroupe("CODE", "L1COD", "=", 'e');
    }
    requete.ajouterGroupeDansRequete("CODE", "AND");
    
    // Filtrer sur l'article
    if (pCritereLigneVente.getIdArticle() != null) {
      IdArticle idArticle = pCritereLigneVente.getIdArticle();
      IdArticle.controlerId(idArticle, true);
      requete.ajouterConditionAnd("L1ART", "=", idArticle.getCodeArticle());
    }
    
    // Filtrer sur le numéro de document
    if (pCritereLigneVente.getNumeroDocument() > 0) {
      requete.ajouterConditionOrAuGroupe("NUMERO", "L1NUM", "=", pCritereLigneVente.getNumeroDocument());
      requete.ajouterConditionOrAuGroupe("NUMERO", "E1NFA", "=", pCritereLigneVente.getNumeroDocument());
      requete.ajouterGroupeDansRequete("NUMERO", "AND");
    }
    else if (pCritereLigneVente.getIdDocument() != null) {
      requete.ajouterConditionOrAuGroupe("NUMERO", "L1NUM", "=", pCritereLigneVente.getIdDocument().getNumero());
      requete.ajouterConditionOrAuGroupe("NUMERO", "E1NFA", "=", pCritereLigneVente.getIdDocument().getNumeroFacture());
      requete.ajouterGroupeDansRequete("NUMERO", "AND");
    }
    
    // Filtrer sur le magasin
    if (pCritereLigneVente.getIdMagasin() != null) {
      requete.ajouterConditionAnd("E1MAG", "=", pCritereLigneVente.getIdMagasin().getCode());
    }
    
    // Filtrer sur le client
    if (pCritereLigneVente.getIdClientFacture() != null) {
      requete.ajouterConditionAnd("((E1CLLP", "=", pCritereLigneVente.getIdClientFacture().getNumero());
      requete.ajouterConditionAnd("E1CLLS", "=", pCritereLigneVente.getIdClientFacture().getSuffixe());
      requete.ajouter(") or (");
      requete.ajouterConditionAnd("E1CLFP", "=", pCritereLigneVente.getIdClientFacture().getNumero());
      requete.ajouterConditionAnd("E1CLFS", "=", pCritereLigneVente.getIdClientFacture().getSuffixe());
      requete.ajouter("))");
    }
    
    // Filtrer sur le chantier
    if (pCritereLigneVente.getChantier() != null) {
      requete.ajouterConditionAnd("xli.XILIB", "=", pCritereLigneVente.getChantier().getId().getNumero());
    }
    
    // Filtrer sur la date de création début
    if (pCritereLigneVente.getDateCreationDebut() != null) {
      requete.ajouterConditionAnd("(E1CRE", ">=", ConvertDate.dateToDb2(pCritereLigneVente.getDateCreationDebut()));
    }
    
    // Filtrer sur la date de création fin
    if (pCritereLigneVente.getDateCreationFin() != null) {
      requete.ajouterConditionAnd("E1CRE", "<=", ConvertDate.dateToDb2(pCritereLigneVente.getDateCreationFin()));
      requete.ajouter(")");
    }
    
    // Filtrer sur la référence courte ou sur la référence longue
    if (!pCritereLigneVente.getRechercheGenerique().getTexteFinal().isEmpty()) {
      requete.ajouter(
          " AND (TRANSLATE(E1NCC,'AAAAAABCCDEEEEEEEFGHIIIIIJKLMNOOOOOPQRSTUUUUUUVWXYZ','aàâÂäÄbcçdeéèêÊëËfghiîÎïÏjklmnoôÔöÖpqrstuùûÛüÜvwxyz')"
              + " like " + "'%" + pCritereLigneVente.getRechercheGenerique().getTexteFinal().toUpperCase() + "%'"
              + " or TRANSLATE(E1RCC,'AAAAAABCCDEEEEEEEFGHIIIIIJKLMNOOOOOPQRSTUUUUUUVWXYZ','aàâÂäÄbcçdeéèêÊëËfghiîÎïÏjklmnoôÔöÖpqrstuùûÛüÜvwxyz') like "
              + "'%" + pCritereLigneVente.getRechercheGenerique().getTexteFinal().toUpperCase() + "%')");
    }
    
    // Filtrer sur le type de document
    if (pCritereLigneVente.getListeTypeDocumentVente() != null && !pCritereLigneVente.getListeTypeDocumentVente().isEmpty()) {
      boolean plusieursTypes = false;
      
      requete.ajouter("and ((E1COD, E1NUM, E1SUF, E1NFA, E1CLLP, E1CLFP) IN (");
      
      if (pCritereLigneVente.getListeTypeDocumentVente().contains(EnumTypeDocumentVente.DEVIS)) {
        requete.ajouter("select distinct doc.E1COD, doc.E1NUM, doc.E1SUF, 0 AS E1NFA, doc.E1CLLP, doc.E1CLFP");
        requete.ajouter(" from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc ");
        requete.ajouter(" where doc.E1ETB = '" + pCritereLigneVente.getIdEtablissement().getCodeEtablissement() + "'");
        getRequetePourDevis(requete);
        plusieursTypes = true;
      }
      
      if (pCritereLigneVente.getListeTypeDocumentVente().contains(EnumTypeDocumentVente.COMMANDE)) {
        if (plusieursTypes) {
          requete.ajouter(" union all ");
        }
        requete.ajouter(" select distinct doc.E1COD, doc.E1NUM, doc.E1SUF, 0 AS E1NFA, doc.E1CLLP, doc.E1CLFP");
        requete.ajouter(" from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc ");
        requete.ajouter(" where doc.E1ETB = '" + pCritereLigneVente.getIdEtablissement().getCodeEtablissement() + "'");
        getRequetePourCommande(requete);
        plusieursTypes = true;
      }
      
      if (pCritereLigneVente.getListeTypeDocumentVente().contains(EnumTypeDocumentVente.BON)) {
        if (plusieursTypes) {
          requete.ajouter(" union all ");
        }
        requete.ajouter(" select distinct doc.E1COD, doc.E1NUM, doc.E1SUF, 0 AS E1NFA, doc.E1CLLP, doc.E1CLFP");
        requete.ajouter(" from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc ");
        requete.ajouter(" where doc.E1ETB = '" + pCritereLigneVente.getIdEtablissement().getCodeEtablissement() + "'");
        getRequetePourBon(requete);
        plusieursTypes = true;
      }
      
      if (pCritereLigneVente.getListeTypeDocumentVente().contains(EnumTypeDocumentVente.FACTURE)) {
        if (plusieursTypes) {
          requete.ajouter(" union all ");
        }
        requete.ajouter(" select distinct doc.E1COD, doc.E1NUM, doc.E1SUF, doc.E1NFA, doc.E1CLLP, doc.E1CLFP");
        requete.ajouter(" from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc ");
        requete.ajouter(" where doc.E1ETB = '" + pCritereLigneVente.getIdEtablissement().getCodeEtablissement() + "'");
        getRequetePourFacture(requete);
        plusieursTypes = true;
      }
      
      requete.ajouter("))");
    }
    else {
      requete.ajouter("and ((E1COD, E1NUM, E1SUF, E1NFA, E1CLLP, E1CLFP) IN (");
      requete.ajouter("select distinct doc.E1COD, doc.E1NUM, doc.E1SUF, 0 AS E1NFA, doc.E1CLLP, doc.E1CLFP");
      requete.ajouter(" from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc ");
      requete.ajouter(" where doc.E1ETB = '" + pCritereLigneVente.getIdEtablissement().getCodeEtablissement() + "'");
      getRequetePourDevis(requete);
      requete.ajouter(" union all ");
      requete.ajouter(" select distinct doc.E1COD, doc.E1NUM, doc.E1SUF, 0 AS E1NFA, doc.E1CLLP, doc.E1CLFP");
      requete.ajouter(" from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc ");
      requete.ajouter(" where doc.E1ETB = '" + pCritereLigneVente.getIdEtablissement().getCodeEtablissement() + "'");
      getRequetePourCommande(requete);
      requete.ajouter(" union all ");
      requete.ajouter(" select distinct doc.E1COD, doc.E1NUM, doc.E1SUF, 0 AS E1NFA, doc.E1CLLP, doc.E1CLFP");
      requete.ajouter(" from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc ");
      requete.ajouter(" where doc.E1ETB = '" + pCritereLigneVente.getIdEtablissement().getCodeEtablissement() + "'");
      getRequetePourBon(requete);
      requete.ajouter(" union all ");
      requete.ajouter(" select distinct doc.E1COD, doc.E1NUM, doc.E1SUF, doc.E1NFA, doc.E1CLLP, doc.E1CLFP");
      requete.ajouter(" from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc ");
      requete.ajouter(" where doc.E1ETB = '" + pCritereLigneVente.getIdEtablissement().getCodeEtablissement() + "'");
      getRequetePourFacture(requete);
      requete.ajouter("))");
    }
    
    requete.ajouter(" order by E1CRE desc, L1NUM desc, L1NLI desc");
    
    // Lancement de la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeIdLigneVente listeIdLigneVente = new ListeIdLigneVente();
    for (GenericRecord record : listeRecord) {
      EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(record.getCharacterValue("L1COD"));
      Integer numero = record.getIntegerValue("L1NUM", 0);
      Integer suffixe = record.getIntegerValue("L1SUF", 0);
      Integer numeroLigne = record.getIntegerValue("L1NLI", 0);
      IdLigneVente idLigneVente =
          IdLigneVente.getInstance(pCritereLigneVente.getIdEtablissement(), codeEntete, numero, suffixe, numeroLigne);
      listeIdLigneVente.add(idLigneVente);
    }
    return listeIdLigneVente;
  }
  
  /**
   * Créer la partie WHERE d'une requête SQL correspond aux critères de recherche pour les devis.
   * Remarque : le code de cette méthode doit être cohérent avec celui de la méthode DocumentVenteBase.deduireTypeDocumentVente().
   * On considère comme devis tout document de ventes avec un entête de type 'D' et qui n'est pas un chantier.
   */
  public void getRequetePourDevis(RequeteSql pRequete) {
    // Filtrer sur le type de documents de vente
    pRequete.ajouterConditionAnd("doc.E1COD", "=", 'D');
    
    // Ne pas avoir les chantiers
    pRequete.ajouterConditionAnd("doc.E1PFC", "<>", "*CH");
  }
  
  /**
   * Créer la partie WHERE d'une requête SQL correspond aux critères de recherche pour les commandes.
   * Remarque : le code de cette méthode doit être cohérent avec celui de la méthode DocumentVenteBase.deduireTypeDocumentVente().
   * On considère comme commande tout document de ventes avec un entête de type 'E' qui n'est ni un bon ni une facture.
   * Un document de ventes avec le statut "En attente" n'a aucune date de renseigné et est donc considéré comme une commande.
   */
  public void getRequetePourCommande(RequeteSql pRequete) {
    // Filtrer sur le type de documents de vente
    pRequete.ajouterConditionAnd("doc.E1COD", "=", 'E');
    
    // Les commandes n'ont pas de date d'expédition (ce sont des bons dans ce cas)
    pRequete.ajouterConditionAnd("doc.E1EXP", "=", 0);
    
    // Les commandes n'ont pas de date de facture (ce sont des factures directes dans ce cas)
    pRequete.ajouterConditionAnd("doc.E1FAC", "=", 0);
  }
  
  /**
   * Créer la partie WHERE d'une requête SQL correspond aux critères de recherche pour les bons.
   * Remarque : le code de cette méthode doit être cohérent avec celui de la méthode DocumentVenteBase.deduireTypeDocumentVente().
   * On considère comme bon tout document de ventes avec un entête de type 'E' qui a une date d'expédition.
   */
  public void getRequetePourBon(RequeteSql pRequete) {
    // Filtrer sur le type de documents de vente
    pRequete.ajouterConditionAnd("doc.E1COD", "=", 'E');
    
    // Seuls les documents de ventes avec une date d'expédition renseignée sont des bons
    // Noter qu'un bon peut également être simultanément une facture si E1FAC > 0.
    pRequete.ajouterConditionAnd("doc.E1EXP", ">", 0);
  }
  
  /**
   * Créer la partie WHERE d'une requête SQL correspond aux critères de recherche pour les factures.
   * Remarque : le code de cette méthode doit être cohérent avec celui de la méthode DocumentVenteBase.deduireTypeDocumentVente().
   * On considère comme facture tout document de ventes possédant un numéro de facture et une date de facture.
   */
  public void getRequetePourFacture(RequeteSql pRequete) {
    
    // Filtrer sur les documents possédant un numéro de facture
    pRequete.ajouterConditionAnd("E1NFA", ">", 0);
    
    // Filtrer les documents avec le numéro de facture 9999999 qui signifie que le document de ventes n'est pas facturable
    pRequete.ajouterConditionAnd("E1NFA", "<>", IdDocumentVente.NUMERO_FACTURE_NON_FACTURABLE);
    
    // Seuls les documents de ventes avec une date de facturation renseignée sont des factures
    pRequete.ajouterConditionAnd("E1FAC", ">", 0);
  }
  
  /**
   * Cette méthode SQL permet de déterminer si tous les articles de la liste des lignes de vente
   * sont bien tous des articles direct usine
   */
  public Boolean existeArticleDirectUsine(ListeLigneVente pListeLigneVente) {
    if (pListeLigneVente == null) {
      throw new MessageErreurException("Les critères de recherche des lignes de vente sont invalides.");
    }
    
    Integer nombreArticles = 0;
    
    String codesArticles = null;
    String etbArticle = null;
    for (LigneVente ligneVente : pListeLigneVente) {
      if (ligneVente.getIdArticle() != null) {
        if (codesArticles != null) {
          codesArticles += ",";
        }
        else {
          codesArticles = "";
        }
        IdArticle idArticle = ligneVente.getIdArticle();
        codesArticles += "'" + idArticle.getCodeArticle() + "'";
        if (etbArticle == null) {
          etbArticle = idArticle.getCodeEtablissement();
        }
      }
    }
    if (codesArticles == null) {
      codesArticles = "";
    }
    
    RequeteSql requete = new RequeteSql();
    requete.ajouter("SELECT COUNT(A1ART) AS NB FROM " + querymg.getLibrary() + ".PGVMARTM WHERE A1IN32 <> '' AND A1ETB = '" + etbArticle
        + "' AND A1ART IN (" + codesArticles + ") ");
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return false;
    }
    
    GenericRecord record = listeRecord.get(0);
    if (record.isPresentField("NB")) {
      nombreArticles = record.getIntegerValue("NB", 0);
    }
    
    return nombreArticles > 0;
  }
}
