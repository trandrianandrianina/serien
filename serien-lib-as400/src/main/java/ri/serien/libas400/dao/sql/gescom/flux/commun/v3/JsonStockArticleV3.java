/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

public class JsonStockArticleV3 {
  // Variables
  private String codeArticle = null;
  private String stockAvantRupture = null;
  private String stockAttendu = null;
  private String dateFinRupture = null;
  
  /**
   * Stock Série N au format SFCC.
   * Cette classe sert à l'intégration JAVA vers JSON.
   * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS.
   */
  public JsonStockArticleV3() {
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le code de l'article.
   * 
   * @return
   */
  public String getCodeArticle() {
    return codeArticle;
  }
  
  /**
   * Initialise le code de l'article.
   * 
   * @param codeArticle
   */
  public void setCodeArticle(String codeArticle) {
    this.codeArticle = codeArticle;
  }
  
  /**
   * Retourne le stock avant rupture.
   * 
   * @return
   */
  public String getstockAvantRupture() {
    return stockAvantRupture;
  }
  
  /**
   * Initialise le stock avant rupture.
   * 
   * @param pStockAvantRupture
   */
  public void setstockAvantRupture(String pStockAvantRupture) {
    this.stockAvantRupture = pStockAvantRupture;
  }
  
  /**
   * Retourne la date de fin de rupture de stock.
   * 
   * @return
   */
  public String getDateFinRupture() {
    return dateFinRupture;
  }
  
  /**
   * Initialise la date de fin de rupture de stock.
   * 
   * @param pDateFinRupture
   */
  public void setDateFinRupture(String pDateFinRupture) {
    this.dateFinRupture = pDateFinRupture;
  }
  
  /**
   * Retourne le stock attendu.
   * 
   * @return
   */
  public String getStockAttendu() {
    return stockAttendu;
  }
  
  /**
   * Initialise le stock attendu.
   * 
   * @param pStockAttendu
   */
  public void setStockAttendu(String pStockAttendu) {
    this.stockAttendu = pStockAttendu;
  }
}
