/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.client.lecture;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.client.EncoursClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgChargerEncoursClient extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0015";
  
  /**
   * Appel du programme RPG qui va lire l'encours client.
   */
  public EncoursClient chargerEncoursClient(SystemeManager pSysteme, IdClient pIdClient) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdClient.controlerId(pIdClient, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0015i entree = new Svgvm0015i();
    Svgvm0015o sortie = new Svgvm0015o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdClient.getCodeEtablissement());
    entree.setPicli(pIdClient.getNumero());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Aucune information d'encours n'a été trouvée pour ce client");
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Initialisation de la classe métier
    EncoursClient encoursClient = new EncoursClient();
    
    // Entrée
    encoursClient.setIdClient(pIdClient);
    // Sortie
    // Indicateurs (POIND)
    encoursClient.setIndicateurs(sortie.getPoind());
    // Effets (PoEFF)
    encoursClient.setEffets(sortie.getPoeff());
    // Position comptable (PoPCO)
    encoursClient.setPositionComptable(sortie.getPopco());
    // Total accompte (PoACC)
    encoursClient.setTotalAcompte(sortie.getPoacc());
    // Total portefeuille (POPOR)
    encoursClient.setTotalPortefeuille(sortie.getPopor());
    // Remise Escompte (Effets) (PoESC )
    encoursClient.setRemiseEscompte(sortie.getPoesc());
    // Remise Encaissement(POENC)
    encoursClient.setRemiseEncaissement(sortie.getPoenc());
    // Total impayés (POTIM)
    encoursClient.setTotalImpayes(sortie.getPotim());
    // Impayes si date de création > date dernier impaye (PoLIM)
    encoursClient.setLimiteImpayes(sortie.getPolim());
    // Date impayé (PoDIM)
    encoursClient.setDateDernierImpaye(sortie.getPodim());
    // Nombre d"impayés (PONIM)
    encoursClient.setNombreImpayes(sortie.getPonim());
    // Total affacturage (POAFM)
    encoursClient.setTotalAffacturage(sortie.getPoafm());
    // Top affacturage (POTAF)
    encoursClient.setTopAffacturage(sortie.getPotaf());
    // Total échéances dépassées (POECD)
    encoursClient.setTotalEcheancesDepassees(sortie.getPoecd());
    // Encours / Commande (POCDE)
    encoursClient.setEncoursCommande(sortie.getPocde());
    // Encours / Expédié (POEXP)
    encoursClient.setEncoursExpedie(sortie.getPoexp());
    // Encours / Facturé (POFAC)
    encoursClient.setEncoursFacture(sortie.getPofac());
    // Vente assimilé export (POVAE)
    encoursClient.setVenteAssimileeExport(sortie.getPovae());
    // Plafond encours autorisé (POPLF)
    encoursClient.setPlafond(sortie.getPoplf());
    // Sur-Plafond encours autorisé (POPLF2)
    encoursClient.setPlafondExceptionnel(sortie.getPoplf2());
    // Date Limite pourSur-plafond (PODPL)
    encoursClient.setDateLimitePlafondExceptionnel(sortie.getPodplConvertiEnDate());
    // Plafond maxi en déblocage (POPLMX)
    encoursClient.setPlafondMaxDeblocage(sortie.getPoplmx());
    // Dépassement (PODEPA)
    encoursClient.setDepassement(sortie.getPodepa());
    // Position encours (PODPOS)
    encoursClient.setEncours(sortie.getPopos());
    
    return encoursClient;
  }
  
}
