/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.tarif;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0012o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_PODAP = 7;
  public static final int DECIMAL_PODAP = 0;
  public static final int SIZE_POK01 = 5;
  public static final int DECIMAL_POK01 = 4;
  public static final int SIZE_POK02 = 5;
  public static final int DECIMAL_POK02 = 4;
  public static final int SIZE_POK03 = 5;
  public static final int DECIMAL_POK03 = 4;
  public static final int SIZE_POK04 = 5;
  public static final int DECIMAL_POK04 = 4;
  public static final int SIZE_POK05 = 5;
  public static final int DECIMAL_POK05 = 4;
  public static final int SIZE_POK06 = 5;
  public static final int DECIMAL_POK06 = 4;
  public static final int SIZE_POK07 = 5;
  public static final int DECIMAL_POK07 = 4;
  public static final int SIZE_POK08 = 5;
  public static final int DECIMAL_POK08 = 4;
  public static final int SIZE_POK09 = 5;
  public static final int DECIMAL_POK09 = 4;
  public static final int SIZE_POK10 = 5;
  public static final int DECIMAL_POK10 = 4;
  public static final int SIZE_POP01 = 9;
  public static final int DECIMAL_POP01 = 2;
  public static final int SIZE_POP02 = 9;
  public static final int DECIMAL_POP02 = 2;
  public static final int SIZE_POP03 = 9;
  public static final int DECIMAL_POP03 = 2;
  public static final int SIZE_POP04 = 9;
  public static final int DECIMAL_POP04 = 2;
  public static final int SIZE_POP05 = 9;
  public static final int DECIMAL_POP05 = 2;
  public static final int SIZE_POP06 = 9;
  public static final int DECIMAL_POP06 = 2;
  public static final int SIZE_POP07 = 9;
  public static final int DECIMAL_POP07 = 2;
  public static final int SIZE_POP08 = 9;
  public static final int DECIMAL_POP08 = 2;
  public static final int SIZE_POP09 = 9;
  public static final int DECIMAL_POP09 = 2;
  public static final int SIZE_POP10 = 9;
  public static final int DECIMAL_POP10 = 2;
  public static final int SIZE_PORON = 1;
  public static final int DECIMAL_PORON = 0;
  public static final int SIZE_POCNV = 5;
  public static final int SIZE_PODAT1 = 7;
  public static final int DECIMAL_PODAT1 = 0;
  public static final int SIZE_POIN11 = 1;
  public static final int SIZE_POIN12 = 1;
  public static final int SIZE_POIN13 = 1;
  public static final int SIZE_PODAT2 = 7;
  public static final int DECIMAL_PODAT2 = 0;
  public static final int SIZE_POIVA = 3;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 184;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_PODAP = 1;
  public static final int VAR_POK01 = 2;
  public static final int VAR_POK02 = 3;
  public static final int VAR_POK03 = 4;
  public static final int VAR_POK04 = 5;
  public static final int VAR_POK05 = 6;
  public static final int VAR_POK06 = 7;
  public static final int VAR_POK07 = 8;
  public static final int VAR_POK08 = 9;
  public static final int VAR_POK09 = 10;
  public static final int VAR_POK10 = 11;
  public static final int VAR_POP01 = 12;
  public static final int VAR_POP02 = 13;
  public static final int VAR_POP03 = 14;
  public static final int VAR_POP04 = 15;
  public static final int VAR_POP05 = 16;
  public static final int VAR_POP06 = 17;
  public static final int VAR_POP07 = 18;
  public static final int VAR_POP08 = 19;
  public static final int VAR_POP09 = 20;
  public static final int VAR_POP10 = 21;
  public static final int VAR_PORON = 22;
  public static final int VAR_POCNV = 23;
  public static final int VAR_PODAT1 = 24;
  public static final int VAR_POIN11 = 25;
  public static final int VAR_POIN12 = 26;
  public static final int VAR_POIN13 = 27;
  public static final int VAR_PODAT2 = 28;
  public static final int VAR_POIVA = 29;
  public static final int VAR_POARR = 30;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal podap = BigDecimal.ZERO; // Date d"Application
  private BigDecimal pok01 = BigDecimal.ZERO; // Coeff. tarifs 1 N.U.
  private BigDecimal pok02 = BigDecimal.ZERO; // Coeff. tarifs 2
  private BigDecimal pok03 = BigDecimal.ZERO; // Coeff. tarifs 3
  private BigDecimal pok04 = BigDecimal.ZERO; // Coeff. tarifs 4
  private BigDecimal pok05 = BigDecimal.ZERO; // Coeff. tarifs 5
  private BigDecimal pok06 = BigDecimal.ZERO; // Coeff. tarifs 6
  private BigDecimal pok07 = BigDecimal.ZERO; // Coeff. tarifs 7
  private BigDecimal pok08 = BigDecimal.ZERO; // Coeff. tarifs 8
  private BigDecimal pok09 = BigDecimal.ZERO; // Coeff. tarifs 9
  private BigDecimal pok10 = BigDecimal.ZERO; // Coeff. tarifs 10
  private BigDecimal pop01 = BigDecimal.ZERO; // Prix de vente 1
  private BigDecimal pop02 = BigDecimal.ZERO; // Prix de vente 2
  private BigDecimal pop03 = BigDecimal.ZERO; // Prix de vente 3
  private BigDecimal pop04 = BigDecimal.ZERO; // Prix de vente 4
  private BigDecimal pop05 = BigDecimal.ZERO; // Prix de vente 5
  private BigDecimal pop06 = BigDecimal.ZERO; // Prix de vente 6
  private BigDecimal pop07 = BigDecimal.ZERO; // Prix de vente 7
  private BigDecimal pop08 = BigDecimal.ZERO; // Prix de vente 8
  private BigDecimal pop09 = BigDecimal.ZERO; // Prix de vente 9
  private BigDecimal pop10 = BigDecimal.ZERO; // Prix de vente 10
  private BigDecimal poron = BigDecimal.ZERO; // Code d'arrondi
  private String pocnv = ""; // Rattachement CNV
  private BigDecimal podat1 = BigDecimal.ZERO; // Date validation tarif
  private String poin11 = ""; // arrondi sur le TTC
  private String poin12 = ""; // coef/remi/HT/TTC,marge
  private String poin13 = ""; // Ind. non utilisé
  private BigDecimal podat2 = BigDecimal.ZERO; // Date non utilisée
  private String poiva = ""; // Initiales validation tarif
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_PODAP, DECIMAL_PODAP), // Date d"Application
      new AS400PackedDecimal(SIZE_POK01, DECIMAL_POK01), // Coeff. tarifs 1 N.U.
      new AS400PackedDecimal(SIZE_POK02, DECIMAL_POK02), // Coeff. tarifs 2
      new AS400PackedDecimal(SIZE_POK03, DECIMAL_POK03), // Coeff. tarifs 3
      new AS400PackedDecimal(SIZE_POK04, DECIMAL_POK04), // Coeff. tarifs 4
      new AS400PackedDecimal(SIZE_POK05, DECIMAL_POK05), // Coeff. tarifs 5
      new AS400PackedDecimal(SIZE_POK06, DECIMAL_POK06), // Coeff. tarifs 6
      new AS400PackedDecimal(SIZE_POK07, DECIMAL_POK07), // Coeff. tarifs 7
      new AS400PackedDecimal(SIZE_POK08, DECIMAL_POK08), // Coeff. tarifs 8
      new AS400PackedDecimal(SIZE_POK09, DECIMAL_POK09), // Coeff. tarifs 9
      new AS400PackedDecimal(SIZE_POK10, DECIMAL_POK10), // Coeff. tarifs 10
      new AS400PackedDecimal(SIZE_POP01, DECIMAL_POP01), // Prix de vente 1
      new AS400PackedDecimal(SIZE_POP02, DECIMAL_POP02), // Prix de vente 2
      new AS400PackedDecimal(SIZE_POP03, DECIMAL_POP03), // Prix de vente 3
      new AS400PackedDecimal(SIZE_POP04, DECIMAL_POP04), // Prix de vente 4
      new AS400PackedDecimal(SIZE_POP05, DECIMAL_POP05), // Prix de vente 5
      new AS400PackedDecimal(SIZE_POP06, DECIMAL_POP06), // Prix de vente 6
      new AS400PackedDecimal(SIZE_POP07, DECIMAL_POP07), // Prix de vente 7
      new AS400PackedDecimal(SIZE_POP08, DECIMAL_POP08), // Prix de vente 8
      new AS400PackedDecimal(SIZE_POP09, DECIMAL_POP09), // Prix de vente 9
      new AS400PackedDecimal(SIZE_POP10, DECIMAL_POP10), // Prix de vente 10
      new AS400ZonedDecimal(SIZE_PORON, DECIMAL_PORON), // Code d'arrondi
      new AS400Text(SIZE_POCNV), // Rattachement CNV
      new AS400PackedDecimal(SIZE_PODAT1, DECIMAL_PODAT1), // Date validation tarif
      new AS400Text(SIZE_POIN11), // arrondi sur le TTC
      new AS400Text(SIZE_POIN12), // coef/remi/HT/TTC,marge
      new AS400Text(SIZE_POIN13), // Ind. non utilisé
      new AS400PackedDecimal(SIZE_PODAT2, DECIMAL_PODAT2), // Date non utilisée
      new AS400Text(SIZE_POIVA), // Initiales validation tarif
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, podap, pok01, pok02, pok03, pok04, pok05, pok06, pok07, pok08, pok09, pok10, pop01, pop02, pop03, pop04,
          pop05, pop06, pop07, pop08, pop09, pop10, poron, pocnv, podat1, poin11, poin12, poin13, podat2, poiva, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    podap = (BigDecimal) output[1];
    pok01 = (BigDecimal) output[2];
    pok02 = (BigDecimal) output[3];
    pok03 = (BigDecimal) output[4];
    pok04 = (BigDecimal) output[5];
    pok05 = (BigDecimal) output[6];
    pok06 = (BigDecimal) output[7];
    pok07 = (BigDecimal) output[8];
    pok08 = (BigDecimal) output[9];
    pok09 = (BigDecimal) output[10];
    pok10 = (BigDecimal) output[11];
    pop01 = (BigDecimal) output[12];
    pop02 = (BigDecimal) output[13];
    pop03 = (BigDecimal) output[14];
    pop04 = (BigDecimal) output[15];
    pop05 = (BigDecimal) output[16];
    pop06 = (BigDecimal) output[17];
    pop07 = (BigDecimal) output[18];
    pop08 = (BigDecimal) output[19];
    pop09 = (BigDecimal) output[20];
    pop10 = (BigDecimal) output[21];
    poron = (BigDecimal) output[22];
    pocnv = (String) output[23];
    podat1 = (BigDecimal) output[24];
    poin11 = (String) output[25];
    poin12 = (String) output[26];
    poin13 = (String) output[27];
    podat2 = (BigDecimal) output[28];
    poiva = (String) output[29];
    poarr = (String) output[30];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPodap(BigDecimal pPodap) {
    if (pPodap == null) {
      return;
    }
    podap = pPodap.setScale(DECIMAL_PODAP, RoundingMode.HALF_UP);
  }
  
  public void setPodap(Integer pPodap) {
    if (pPodap == null) {
      return;
    }
    podap = BigDecimal.valueOf(pPodap);
  }
  
  public void setPodap(Date pPodap) {
    if (pPodap == null) {
      return;
    }
    podap = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodap));
  }
  
  public Integer getPodap() {
    return podap.intValue();
  }
  
  public Date getPodapConvertiEnDate() {
    return ConvertDate.db2ToDate(podap.intValue(), null);
  }
  
  public void setPok01(BigDecimal pPok01) {
    if (pPok01 == null) {
      return;
    }
    pok01 = pPok01.setScale(DECIMAL_POK01, RoundingMode.HALF_UP);
  }
  
  public void setPok01(Double pPok01) {
    if (pPok01 == null) {
      return;
    }
    pok01 = BigDecimal.valueOf(pPok01).setScale(DECIMAL_POK01, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok01() {
    return pok01;
  }
  
  public void setPok02(BigDecimal pPok02) {
    if (pPok02 == null) {
      return;
    }
    pok02 = pPok02.setScale(DECIMAL_POK02, RoundingMode.HALF_UP);
  }
  
  public void setPok02(Double pPok02) {
    if (pPok02 == null) {
      return;
    }
    pok02 = BigDecimal.valueOf(pPok02).setScale(DECIMAL_POK02, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok02() {
    return pok02;
  }
  
  public void setPok03(BigDecimal pPok03) {
    if (pPok03 == null) {
      return;
    }
    pok03 = pPok03.setScale(DECIMAL_POK03, RoundingMode.HALF_UP);
  }
  
  public void setPok03(Double pPok03) {
    if (pPok03 == null) {
      return;
    }
    pok03 = BigDecimal.valueOf(pPok03).setScale(DECIMAL_POK03, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok03() {
    return pok03;
  }
  
  public void setPok04(BigDecimal pPok04) {
    if (pPok04 == null) {
      return;
    }
    pok04 = pPok04.setScale(DECIMAL_POK04, RoundingMode.HALF_UP);
  }
  
  public void setPok04(Double pPok04) {
    if (pPok04 == null) {
      return;
    }
    pok04 = BigDecimal.valueOf(pPok04).setScale(DECIMAL_POK04, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok04() {
    return pok04;
  }
  
  public void setPok05(BigDecimal pPok05) {
    if (pPok05 == null) {
      return;
    }
    pok05 = pPok05.setScale(DECIMAL_POK05, RoundingMode.HALF_UP);
  }
  
  public void setPok05(Double pPok05) {
    if (pPok05 == null) {
      return;
    }
    pok05 = BigDecimal.valueOf(pPok05).setScale(DECIMAL_POK05, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok05() {
    return pok05;
  }
  
  public void setPok06(BigDecimal pPok06) {
    if (pPok06 == null) {
      return;
    }
    pok06 = pPok06.setScale(DECIMAL_POK06, RoundingMode.HALF_UP);
  }
  
  public void setPok06(Double pPok06) {
    if (pPok06 == null) {
      return;
    }
    pok06 = BigDecimal.valueOf(pPok06).setScale(DECIMAL_POK06, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok06() {
    return pok06;
  }
  
  public void setPok07(BigDecimal pPok07) {
    if (pPok07 == null) {
      return;
    }
    pok07 = pPok07.setScale(DECIMAL_POK07, RoundingMode.HALF_UP);
  }
  
  public void setPok07(Double pPok07) {
    if (pPok07 == null) {
      return;
    }
    pok07 = BigDecimal.valueOf(pPok07).setScale(DECIMAL_POK07, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok07() {
    return pok07;
  }
  
  public void setPok08(BigDecimal pPok08) {
    if (pPok08 == null) {
      return;
    }
    pok08 = pPok08.setScale(DECIMAL_POK08, RoundingMode.HALF_UP);
  }
  
  public void setPok08(Double pPok08) {
    if (pPok08 == null) {
      return;
    }
    pok08 = BigDecimal.valueOf(pPok08).setScale(DECIMAL_POK08, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok08() {
    return pok08;
  }
  
  public void setPok09(BigDecimal pPok09) {
    if (pPok09 == null) {
      return;
    }
    pok09 = pPok09.setScale(DECIMAL_POK09, RoundingMode.HALF_UP);
  }
  
  public void setPok09(Double pPok09) {
    if (pPok09 == null) {
      return;
    }
    pok09 = BigDecimal.valueOf(pPok09).setScale(DECIMAL_POK09, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok09() {
    return pok09;
  }
  
  public void setPok10(BigDecimal pPok10) {
    if (pPok10 == null) {
      return;
    }
    pok10 = pPok10.setScale(DECIMAL_POK10, RoundingMode.HALF_UP);
  }
  
  public void setPok10(Double pPok10) {
    if (pPok10 == null) {
      return;
    }
    pok10 = BigDecimal.valueOf(pPok10).setScale(DECIMAL_POK10, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPok10() {
    return pok10;
  }
  
  public void setPop01(BigDecimal pPop01) {
    if (pPop01 == null) {
      return;
    }
    pop01 = pPop01.setScale(DECIMAL_POP01, RoundingMode.HALF_UP);
  }
  
  public void setPop01(Double pPop01) {
    if (pPop01 == null) {
      return;
    }
    pop01 = BigDecimal.valueOf(pPop01).setScale(DECIMAL_POP01, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop01() {
    return pop01;
  }
  
  public void setPop02(BigDecimal pPop02) {
    if (pPop02 == null) {
      return;
    }
    pop02 = pPop02.setScale(DECIMAL_POP02, RoundingMode.HALF_UP);
  }
  
  public void setPop02(Double pPop02) {
    if (pPop02 == null) {
      return;
    }
    pop02 = BigDecimal.valueOf(pPop02).setScale(DECIMAL_POP02, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop02() {
    return pop02;
  }
  
  public void setPop03(BigDecimal pPop03) {
    if (pPop03 == null) {
      return;
    }
    pop03 = pPop03.setScale(DECIMAL_POP03, RoundingMode.HALF_UP);
  }
  
  public void setPop03(Double pPop03) {
    if (pPop03 == null) {
      return;
    }
    pop03 = BigDecimal.valueOf(pPop03).setScale(DECIMAL_POP03, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop03() {
    return pop03;
  }
  
  public void setPop04(BigDecimal pPop04) {
    if (pPop04 == null) {
      return;
    }
    pop04 = pPop04.setScale(DECIMAL_POP04, RoundingMode.HALF_UP);
  }
  
  public void setPop04(Double pPop04) {
    if (pPop04 == null) {
      return;
    }
    pop04 = BigDecimal.valueOf(pPop04).setScale(DECIMAL_POP04, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop04() {
    return pop04;
  }
  
  public void setPop05(BigDecimal pPop05) {
    if (pPop05 == null) {
      return;
    }
    pop05 = pPop05.setScale(DECIMAL_POP05, RoundingMode.HALF_UP);
  }
  
  public void setPop05(Double pPop05) {
    if (pPop05 == null) {
      return;
    }
    pop05 = BigDecimal.valueOf(pPop05).setScale(DECIMAL_POP05, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop05() {
    return pop05;
  }
  
  public void setPop06(BigDecimal pPop06) {
    if (pPop06 == null) {
      return;
    }
    pop06 = pPop06.setScale(DECIMAL_POP06, RoundingMode.HALF_UP);
  }
  
  public void setPop06(Double pPop06) {
    if (pPop06 == null) {
      return;
    }
    pop06 = BigDecimal.valueOf(pPop06).setScale(DECIMAL_POP06, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop06() {
    return pop06;
  }
  
  public void setPop07(BigDecimal pPop07) {
    if (pPop07 == null) {
      return;
    }
    pop07 = pPop07.setScale(DECIMAL_POP07, RoundingMode.HALF_UP);
  }
  
  public void setPop07(Double pPop07) {
    if (pPop07 == null) {
      return;
    }
    pop07 = BigDecimal.valueOf(pPop07).setScale(DECIMAL_POP07, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop07() {
    return pop07;
  }
  
  public void setPop08(BigDecimal pPop08) {
    if (pPop08 == null) {
      return;
    }
    pop08 = pPop08.setScale(DECIMAL_POP08, RoundingMode.HALF_UP);
  }
  
  public void setPop08(Double pPop08) {
    if (pPop08 == null) {
      return;
    }
    pop08 = BigDecimal.valueOf(pPop08).setScale(DECIMAL_POP08, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop08() {
    return pop08;
  }
  
  public void setPop09(BigDecimal pPop09) {
    if (pPop09 == null) {
      return;
    }
    pop09 = pPop09.setScale(DECIMAL_POP09, RoundingMode.HALF_UP);
  }
  
  public void setPop09(Double pPop09) {
    if (pPop09 == null) {
      return;
    }
    pop09 = BigDecimal.valueOf(pPop09).setScale(DECIMAL_POP09, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop09() {
    return pop09;
  }
  
  public void setPop10(BigDecimal pPop10) {
    if (pPop10 == null) {
      return;
    }
    pop10 = pPop10.setScale(DECIMAL_POP10, RoundingMode.HALF_UP);
  }
  
  public void setPop10(Double pPop10) {
    if (pPop10 == null) {
      return;
    }
    pop10 = BigDecimal.valueOf(pPop10).setScale(DECIMAL_POP10, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPop10() {
    return pop10;
  }
  
  public void setPoron(BigDecimal pPoron) {
    if (pPoron == null) {
      return;
    }
    poron = pPoron.setScale(DECIMAL_PORON, RoundingMode.HALF_UP);
  }
  
  public void setPoron(Integer pPoron) {
    if (pPoron == null) {
      return;
    }
    poron = BigDecimal.valueOf(pPoron);
  }
  
  public Integer getPoron() {
    return poron.intValue();
  }
  
  public void setPocnv(String pPocnv) {
    if (pPocnv == null) {
      return;
    }
    pocnv = pPocnv;
  }
  
  public String getPocnv() {
    return pocnv;
  }
  
  public void setPodat1(BigDecimal pPodat1) {
    if (pPodat1 == null) {
      return;
    }
    podat1 = pPodat1.setScale(DECIMAL_PODAT1, RoundingMode.HALF_UP);
  }
  
  public void setPodat1(Integer pPodat1) {
    if (pPodat1 == null) {
      return;
    }
    podat1 = BigDecimal.valueOf(pPodat1);
  }
  
  public void setPodat1(Date pPodat1) {
    if (pPodat1 == null) {
      return;
    }
    podat1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodat1));
  }
  
  public Integer getPodat1() {
    return podat1.intValue();
  }
  
  public Date getPodat1ConvertiEnDate() {
    return ConvertDate.db2ToDate(podat1.intValue(), null);
  }
  
  public void setPoin11(Character pPoin11) {
    if (pPoin11 == null) {
      return;
    }
    poin11 = String.valueOf(pPoin11);
  }
  
  public Character getPoin11() {
    return poin11.charAt(0);
  }
  
  public void setPoin12(Character pPoin12) {
    if (pPoin12 == null) {
      return;
    }
    poin12 = String.valueOf(pPoin12);
  }
  
  public Character getPoin12() {
    return poin12.charAt(0);
  }
  
  public void setPoin13(Character pPoin13) {
    if (pPoin13 == null) {
      return;
    }
    poin13 = String.valueOf(pPoin13);
  }
  
  public Character getPoin13() {
    return poin13.charAt(0);
  }
  
  public void setPodat2(BigDecimal pPodat2) {
    if (pPodat2 == null) {
      return;
    }
    podat2 = pPodat2.setScale(DECIMAL_PODAT2, RoundingMode.HALF_UP);
  }
  
  public void setPodat2(Integer pPodat2) {
    if (pPodat2 == null) {
      return;
    }
    podat2 = BigDecimal.valueOf(pPodat2);
  }
  
  public void setPodat2(Date pPodat2) {
    if (pPodat2 == null) {
      return;
    }
    podat2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodat2));
  }
  
  public Integer getPodat2() {
    return podat2.intValue();
  }
  
  public Date getPodat2ConvertiEnDate() {
    return ConvertDate.db2ToDate(podat2.intValue(), null);
  }
  
  public void setPoiva(String pPoiva) {
    if (pPoiva == null) {
      return;
    }
    poiva = pPoiva;
  }
  
  public String getPoiva() {
    return poiva;
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
