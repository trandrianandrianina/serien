/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0029o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_PONOM3 = 30;
  public static final int SIZE_POCPL3 = 30;
  public static final int SIZE_PORUE3 = 30;
  public static final int SIZE_POLOC3 = 30;
  public static final int SIZE_POCDP3 = 5;
  public static final int SIZE_POFIL3 = 1;
  public static final int SIZE_POVIL3 = 24;
  public static final int SIZE_POPAY3 = 30;
  public static final int SIZE_PONOM2 = 30;
  public static final int SIZE_POCPL2 = 30;
  public static final int SIZE_PORUE2 = 30;
  public static final int SIZE_POLOC2 = 30;
  public static final int SIZE_POCDP2 = 5;
  public static final int SIZE_POFIL2 = 1;
  public static final int SIZE_POVIL2 = 24;
  public static final int SIZE_POPAY2 = 30;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 371;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_PONOM3 = 1;
  public static final int VAR_POCPL3 = 2;
  public static final int VAR_PORUE3 = 3;
  public static final int VAR_POLOC3 = 4;
  public static final int VAR_POCDP3 = 5;
  public static final int VAR_POFIL3 = 6;
  public static final int VAR_POVIL3 = 7;
  public static final int VAR_POPAY3 = 8;
  public static final int VAR_PONOM2 = 9;
  public static final int VAR_POCPL2 = 10;
  public static final int VAR_PORUE2 = 11;
  public static final int VAR_POLOC2 = 12;
  public static final int VAR_POCDP2 = 13;
  public static final int VAR_POFIL2 = 14;
  public static final int VAR_POVIL2 = 15;
  public static final int VAR_POPAY2 = 16;
  public static final int VAR_POARR = 17;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private String ponom3 = ""; // Nom client
  private String pocpl3 = ""; // Complément du nom
  private String porue3 = ""; // Rue
  private String poloc3 = ""; // Localité
  private String pocdp3 = ""; // Code Postal
  private String pofil3 = ""; // Filler
  private String povil3 = ""; // Ville
  private String popay3 = ""; // Pays
  private String ponom2 = ""; // Nom client
  private String pocpl2 = ""; // Complément du nom
  private String porue2 = ""; // Rue
  private String poloc2 = ""; // Localité
  private String pocdp2 = ""; // Code Postal
  private String pofil2 = ""; // Filler
  private String povil2 = ""; // Ville
  private String popay2 = ""; // Pays
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400Text(SIZE_PONOM3), // Nom client
      new AS400Text(SIZE_POCPL3), // Complément du nom
      new AS400Text(SIZE_PORUE3), // Rue
      new AS400Text(SIZE_POLOC3), // Localité
      new AS400Text(SIZE_POCDP3), // Code Postal
      new AS400Text(SIZE_POFIL3), // Filler
      new AS400Text(SIZE_POVIL3), // Ville
      new AS400Text(SIZE_POPAY3), // Pays
      new AS400Text(SIZE_PONOM2), // Nom client
      new AS400Text(SIZE_POCPL2), // Complément du nom
      new AS400Text(SIZE_PORUE2), // Rue
      new AS400Text(SIZE_POLOC2), // Localité
      new AS400Text(SIZE_POCDP2), // Code Postal
      new AS400Text(SIZE_POFIL2), // Filler
      new AS400Text(SIZE_POVIL2), // Ville
      new AS400Text(SIZE_POPAY2), // Pays
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, ponom3, pocpl3, porue3, poloc3, pocdp3, pofil3, povil3, popay3, ponom2, pocpl2, porue2, poloc2, pocdp2,
          pofil2, povil2, popay2, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    ponom3 = (String) output[1];
    pocpl3 = (String) output[2];
    porue3 = (String) output[3];
    poloc3 = (String) output[4];
    pocdp3 = (String) output[5];
    pofil3 = (String) output[6];
    povil3 = (String) output[7];
    popay3 = (String) output[8];
    ponom2 = (String) output[9];
    pocpl2 = (String) output[10];
    porue2 = (String) output[11];
    poloc2 = (String) output[12];
    pocdp2 = (String) output[13];
    pofil2 = (String) output[14];
    povil2 = (String) output[15];
    popay2 = (String) output[16];
    poarr = (String) output[17];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPonom3(String pPonom3) {
    if (pPonom3 == null) {
      return;
    }
    ponom3 = pPonom3;
  }
  
  public String getPonom3() {
    return ponom3;
  }
  
  public void setPocpl3(String pPocpl3) {
    if (pPocpl3 == null) {
      return;
    }
    pocpl3 = pPocpl3;
  }
  
  public String getPocpl3() {
    return pocpl3;
  }
  
  public void setPorue3(String pPorue3) {
    if (pPorue3 == null) {
      return;
    }
    porue3 = pPorue3;
  }
  
  public String getPorue3() {
    return porue3;
  }
  
  public void setPoloc3(String pPoloc3) {
    if (pPoloc3 == null) {
      return;
    }
    poloc3 = pPoloc3;
  }
  
  public String getPoloc3() {
    return poloc3;
  }
  
  public void setPocdp3(String pPocdp3) {
    if (pPocdp3 == null) {
      return;
    }
    pocdp3 = pPocdp3;
  }
  
  public String getPocdp3() {
    return pocdp3;
  }
  
  public void setPofil3(Character pPofil3) {
    if (pPofil3 == null) {
      return;
    }
    pofil3 = String.valueOf(pPofil3);
  }
  
  public Character getPofil3() {
    return pofil3.charAt(0);
  }
  
  public void setPovil3(String pPovil3) {
    if (pPovil3 == null) {
      return;
    }
    povil3 = pPovil3;
  }
  
  public String getPovil3() {
    return povil3;
  }
  
  public void setPopay3(String pPopay3) {
    if (pPopay3 == null) {
      return;
    }
    popay3 = pPopay3;
  }
  
  public String getPopay3() {
    return popay3;
  }
  
  public void setPonom2(String pPonom2) {
    if (pPonom2 == null) {
      return;
    }
    ponom2 = pPonom2;
  }
  
  public String getPonom2() {
    return ponom2;
  }
  
  public void setPocpl2(String pPocpl2) {
    if (pPocpl2 == null) {
      return;
    }
    pocpl2 = pPocpl2;
  }
  
  public String getPocpl2() {
    return pocpl2;
  }
  
  public void setPorue2(String pPorue2) {
    if (pPorue2 == null) {
      return;
    }
    porue2 = pPorue2;
  }
  
  public String getPorue2() {
    return porue2;
  }
  
  public void setPoloc2(String pPoloc2) {
    if (pPoloc2 == null) {
      return;
    }
    poloc2 = pPoloc2;
  }
  
  public String getPoloc2() {
    return poloc2;
  }
  
  public void setPocdp2(String pPocdp2) {
    if (pPocdp2 == null) {
      return;
    }
    pocdp2 = pPocdp2;
  }
  
  public String getPocdp2() {
    return pocdp2;
  }
  
  public void setPofil2(Character pPofil2) {
    if (pPofil2 == null) {
      return;
    }
    pofil2 = String.valueOf(pPofil2);
  }
  
  public Character getPofil2() {
    return pofil2.charAt(0);
  }
  
  public void setPovil2(String pPovil2) {
    if (pPovil2 == null) {
      return;
    }
    povil2 = pPovil2;
  }
  
  public String getPovil2() {
    return povil2;
  }
  
  public void setPopay2(String pPopay2) {
    if (pPopay2 == null) {
      return;
    }
    popay2 = pPopay2;
  }
  
  public String getPopay2() {
    return popay2;
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
