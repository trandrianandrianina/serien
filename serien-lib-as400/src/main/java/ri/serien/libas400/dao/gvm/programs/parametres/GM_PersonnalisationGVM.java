/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs.parametres;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.ibm.as400.access.Record;

import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.ExtendRecord;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class GM_PersonnalisationGVM extends BaseGroupDB {
  /**
   * Constructeur
   * @param database
   */
  public GM_PersonnalisationGVM(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Retourne la liste des objets possibles pour les actions
   * @param curlib
   * @param etb
   * @return
   * 
   **/
  public LinkedHashMap<String, String> getListObjectAction(String etb) {
    LinkedHashMap<String, String> listObjectAction = new LinkedHashMap<String, String>();
    // String ds = getClass().getPackage().getName() + ".Pgvmparm_ds_AA";
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary()
            + ".PGVMPARM where PARTYP = 'AA' and PARETB = '" + etb + "'", "ri.serien.libas400.dao.gvm.database.Pgvmparm_ds_AA");
    
    // On alimente la hashmap avec les enregistrements trouvés
    if (listeRecord == null) {
      return listObjectAction;
    }
    
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      listObjectAction.put(((String) record.getField("INDIC")).substring(5).trim(), ((String) record.getField("AALIB")).trim());
    }
    
    return listObjectAction;
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    queryManager = null;
    
  }
  
}
