/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx012.v2;

import java.beans.PropertyVetoException;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.database.field.FieldAlpha;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

public class InjecteurFacYooz {
  // Constantes
  private static final int LG_PARAM_RETOUR = 3;
  private static final int LG_PARAM_CHEMIN = 255;
  private static final int LG_PARAM_ETB = 3;
  
  private FieldAlpha CHEMIN = new FieldAlpha(LG_PARAM_CHEMIN);
  private FieldAlpha ETB = new FieldAlpha(LG_PARAM_ETB);
  private FieldAlpha RETOUR = new FieldAlpha(LG_PARAM_RETOUR);
  
  private String msgErreur = "";
  
  // paramètres retour
  private String RETOUR_PGM = null;
  private int ERREURS = 3;
  
  // Constantes d'erreurs de retour du programme RPG
  public final static int PAS_ERREUR = 0;
  public final static int ERREUR_INJECTION = 1;
  public final static int ERREUR_FICHIER = 2;
  public final static int ERREUR_INCONNUE = 3;
  
  // Variables
  private boolean init = false;
  private SystemeManager systemeManager = null;
  private Bibliotheque bibliothequeEnvironnement = null;
  private ProgramParameter[] parameterList = new ProgramParameter[3];
  private AS400Text ppar0 = new AS400Text(255);
  private AS400Text ppar1 = new AS400Text(3);
  private AS400Text ppar2 = new AS400Text(3);
  private StringBuilder contenuParam0 = new StringBuilder();
  private StringBuilder contenuParam1 = new StringBuilder();
  private StringBuilder contenuParam2 = new StringBuilder();
  private char lettre = 'W';
  private Bibliotheque curlib = null;
  private ContexteProgrammeRPG rpg = null;
  
  /**
   * Constructeur.
   */
  public InjecteurFacYooz(SystemeManager pSystemeManager, String pLettre, Bibliotheque pCurlib) {
    systemeManager = pSystemeManager;
    pLettre = Constantes.normerTexte(pLettre);
    if (!pLettre.isEmpty()) {
      lettre = pLettre.charAt(0);
    }
    curlib = pCurlib;
  }
  
  /**
   * Initialise l'environnment d'execution du programme RPG
   * @return
   */
  public boolean init() {
    if (curlib == null) {
      majMsgErreur("[InjecteurFacYooz] La base de données est invalide.");
      return false;
    }
    if (init) {
      return true;
    }
    
    try {
      parameterList[0] = new ProgramParameter(LG_PARAM_CHEMIN);
      parameterList[1] = new ProgramParameter(LG_PARAM_ETB);
      parameterList[2] = new ProgramParameter(LG_PARAM_RETOUR);
      rpg = new ContexteProgrammeRPG(systemeManager.getSystem(), curlib);
      init = rpg.initialiserLettreEnvironnement(lettre);
      if (!init) {
        majMsgErreur("[InjecteurFacYooz] Erreur CHGDTAARA");
        return false;
      }
      
      rpg.ajouterBibliotheque(curlib, true);
      rpg.ajouterBibliotheque(ParametresFluxBibli.getBibliothequeEnvironnement().getNom(), false);
      rpg.ajouterBibliotheque(lettre + "EXPAS", false);
      rpg.ajouterBibliotheque(lettre + "CGMAS", false);
      init = rpg.ajouterBibliotheque(lettre + "GVMX", false);
    }
    catch (Exception e) {
      majMsgErreur("[InjecteurFacYooz] PB création Injecteur -> " + e.getMessage() + " " + e.getCause());
      init = false;
    }
    
    return init;
  }
  
  /**
   * Effectue la recherche du tarif
   * @return
   */
  public boolean execute(String pChemin, String pEtb) {
    if (pChemin == null || pChemin.trim().isEmpty()) {
      majMsgErreur("[InjecteurFacYooz] Chemin transmis corrompu " + pChemin);
      return false;
    }
    if (pEtb == null || pEtb.trim().isEmpty()) {
      majMsgErreur("[InjecteurFacYooz] pEtb transmis corrompu " + pEtb);
      return false;
    }
    
    CHEMIN.setValue(pChemin);
    ETB.setValue(formateVar(pEtb, LG_PARAM_ETB));
    RETOUR.setValue(formateVar(" ", LG_PARAM_RETOUR));
    
    return execute();
  }
  
  /**
   * Effectue la recherche du tarif
   * @return
   */
  private boolean execute() {
    if (rpg == null) {
      majMsgErreur("[InjecteurFacYooz] execute rpg est NULL");
      return false;
    }
    
    RETOUR_PGM = null;
    ERREURS = 0;
    
    // Construction du paramètre AS400 PPAR1
    contenuParam0.setLength(0);
    contenuParam0.append(CHEMIN.toFormattedString());
    contenuParam1.setLength(0);
    contenuParam1.append(ETB.toFormattedString());
    contenuParam2.setLength(0);
    contenuParam2.append(RETOUR.toFormattedString());
    
    try {
      parameterList[0].setInputData(ppar0.toBytes(contenuParam0.toString()));
      parameterList[1].setInputData(ppar1.toBytes(contenuParam1.toString()));
      parameterList[2].setInputData(ppar2.toBytes(contenuParam2.toString()));
      
      // Appel du programme RPG
      Bibliotheque bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(lettre + "CGMAS"), EnumTypeBibliotheque.PROGRAMMES_SERIEN);
      if (rpg.executerProgramme("VCGM74CPT3", bibliotheque, parameterList)) {
        RETOUR_PGM = (String) ppar2.toObject(parameterList[2].getOutputData());
        return traiterRetour() == InjecteurFacYooz.PAS_ERREUR;
      }
      else {
        majMsgErreur("[InjecteurFacYooz] probleme de rpg.execute().");
      }
    }
    catch (PropertyVetoException e) {
      majMsgErreur("[InjecteurFacYooz] execute() ERREUR " + e.getMessage());
    }
    return false;
  }
  
  /**
   * On découpe la chaine de retour en variables métier: Type, etb, commande, erreurs. On retourne le code erreur après
   * vérifications du découpage
   */
  private int traiterRetour() {
    if (RETOUR_PGM != null) {
      if (RETOUR_PGM.trim().isEmpty()) {
        ERREURS = PAS_ERREUR;
      }
      else if (RETOUR_PGM.trim().equals("001")) {
        ERREURS = ERREUR_INJECTION;
      }
      else if (RETOUR_PGM.trim().equals("002")) {
        ERREURS = ERREUR_FICHIER;
      }
    }
    return ERREURS;
  }
  
  /**
   * Retourner le libellé associé au code erreur retourné par le programme d'injection de la facture
   */
  public String retournerLibelleCodeErreur() {
    String retour = "";
    
    switch (ERREURS) {
      case 0:
        retour = "Erreur inconnue";
        break;
      case 1:
        retour = "Erreur d'injection du fichier";
        break;
      case 2:
        retour = "Erreur de données dans le fichier";
        break;
      default:
        retour = "Erreur inconnue";
        break;
    }
    
    return retour;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Formate une variable de type String (avec cadrage à droite, notamment pour l'établissement)
   * @param valeur
   * @param lg
   * @return
   */
  private String formateVar(String valeur, int lg) {
    if (valeur == null) {
      return String.format("%" + lg + "." + lg + "s", "");
    }
    else {
      return String.format("%" + lg + "." + lg + "s", valeur);
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  public int getERREURS() {
    return ERREURS;
  }
  
  private void majMsgErreur(String pMess) {
    Trace.erreur(pMess);
    if (pMess == null) {
      msgErreur = "";
    }
    else {
      msgErreur += pMess;
    }
  }
  
  public String getMsgErreur() {
    return msgErreur;
  }
}
