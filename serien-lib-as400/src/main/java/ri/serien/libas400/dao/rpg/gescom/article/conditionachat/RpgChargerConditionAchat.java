/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.conditionachat;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.conditionachat.EnumTypeConditionAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgChargerConditionAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0019";
  
  /**
   * Appel du programme RPG qui va lire une CNA pour un article.
   */
  public ConditionAchat chargerConditionAchat(SystemeManager pSysteme, IdArticle pIdArticle, Date pDate) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pIdArticle == null || !pIdArticle.isExistant()) {
      throw new MessageErreurException("L'identifiant de l'article est invalide.");
    }
    
    // Tracer
    Trace.info("Charger la condition d'achat de l'article " + pIdArticle);
    
    ConditionAchat cna = null;
    
    // Préparation des paramètres du programme RPG
    Svgvx0019i entree = new Svgvx0019i();
    Svgvx0019o sortie = new Svgvx0019o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind("0000000000");
    entree.setPietb(pIdArticle.getCodeEtablissement());
    entree.setPiart(pIdArticle.getCodeArticle());
    if (pDate != null) {
      entree.setPidat(ConvertDate.dateToDb2(pDate));
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      // Convertir les valeurs envoyées par le RPG en données Java
      try {
        entree.setDSOutput();
        sortie.setDSOutput();
        erreur.setDSOutput();
      }
      catch (NumberFormatException e) {
        Trace.erreur(e, "Impossible de charger la condition d'achat de l'article " + pIdArticle);
        // On traite une erreur de conversion de format comme si le service RPG n'avait pas de conditions d'achats à retourner
        return null;
      }
      
      // Tester la présence d'un code fournisseur
      // Si le programme RPG n'en renvoit pas, c'est qu'il n'y a pas de condition d'achat associée à l'article
      if (sortie.getPofrs() == 0) {
        return null;
      }
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      cna = new ConditionAchat();
      
      // Sortie
      IdEtablissement idEtablissement = IdEtablissement.getInstance(sortie.getPoetb());
      cna.setIdEtablissement(idEtablissement);
      IdArticle idArticle = IdArticle.getInstance(idEtablissement, sortie.getPoart());
      cna.setIdArticle(idArticle);
      cna.setDateCreation(ConvertDate.db2ToDate(sortie.getPocre().intValue(), null));
      cna.setDateModification(ConvertDate.db2ToDate(sortie.getPomod().intValue(), null));
      cna.setDateTraitement(ConvertDate.db2ToDate(sortie.getPotrt().intValue(), null));
      IdFournisseur idFournisseur =
          IdFournisseur.getInstance(idEtablissement, sortie.getPocol().intValue(), sortie.getPofrs().intValue());
      cna.setIdFournisseur(idFournisseur);
      cna.setDateApplication(ConvertDate.db2ToDate(sortie.getPodap().intValue(), null));
      cna.setTopCalculPrixVente(sortie.getPocpv());
      cna.setTypeCondition(EnumTypeConditionAchat.CONDITION_ACHAT);
      cna.initialiser(sortie.getPopra(), sortie.getPoprnet(), sortie.getPoprprf(), sortie.getPoprprs(), sortie.getPorem1(),
          sortie.getPorem2(), sortie.getPorem3(), sortie.getPorem4(), sortie.getPokpr(), sortie.getPofk3(), sortie.getPofv3(),
          sortie.getPofv1(), sortie.getPofk1(), sortie.getPofp1(), sortie.getPofp2());
      
      cna.setPourcentageRemise5(sortie.getPorem5());
      cna.setPourcentageRemise6(sortie.getPorem6());
      cna.setPourcentageRemise6(sortie.getPorem6());
      if (!sortie.getPouna().trim().isEmpty()) {
        IdUnite idUnite = IdUnite.getInstance(sortie.getPouna());
        cna.setIdUniteAchat(idUnite);
      }
      cna.setConditionnement(sortie.getPonua());
      cna.setCoefficientUAparUCA(sortie.getPokac());
      cna.setDelaiLivraison(sortie.getPodel());
      cna.setReferenceFournisseur(sortie.getPoref());
      cna.setUniteCommande(sortie.getPounc());
      cna.setCoefficientUSparUCA(sortie.getPoksc());
      cna.setCodeDevise(sortie.getPodev());
      cna.setQuantiteMinimumCommande(sortie.getPoqmi());
      cna.setQuantiteEconomique(sortie.getPoqec());
      cna.setCoefficientCalculPrixVente(sortie.getPokpv());
      cna.setDateDernierPrix(ConvertDate.db2ToDate(sortie.getPoddp().intValue(), null));
      cna.setRegroupementAchat(sortie.getPorga());
      cna.setDelaiSupplementaire(sortie.getPodels().intValue());
      cna.setOriginePrix(sortie.getPoin1());
      cna.setNoteQualite(sortie.getPoin2());
      cna.setTypeFournisseur(sortie.getPoin3());
      cna.setRemiseCalculPVminimum(sortie.getPoin4());
      cna.setTypeRemiseLigne(sortie.getPotrl());
      cna.setReferenceConstructeur(sortie.getPorfc());
      cna.setGencodFournisseur(sortie.getPogcd().intValue());
      cna.setDateLimiteValidite(ConvertDate.db2ToDate(sortie.getPodaf().intValue(), null));
      cna.setLibelle(sortie.getPolib());
      cna.setOrigine(sortie.getPoopa());
      cna.setDelaiJours(sortie.getPoin7());
      cna.setPrixDistributeur(sortie.getPopdi());
      cna.setCoefficientApprocheFixe(sortie.getPokap());
      cna.setTopPrixNegocie(sortie.getPopgn());
      cna.setUniteFrais1(sortie.getPofu1());
      cna.setFraisEnValeur2(sortie.getPofv2());
      cna.setFraisEnCoeff2(sortie.getPofk2());
      cna.setUniteFrais2(sortie.getPofu2());
      cna.setUniteFrais3(sortie.getPofu3());
      cna.setArticleInexistant(sortie.getPoerr1().equals('1'));
      cna.setConditionAchatNonTrouvee(sortie.getPoerr2().equals('1'));
      cna.setNombreDecimalesUCA(sortie.getPodcc().intValue());
      cna.setNombreDecimalesUA(sortie.getPodca().intValue());
    }
    
    return cna;
  }
}
