/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lienligne;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.lienligne.EnumTypeLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.ListeLienLigne;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVenteBase;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgChargerLienLigne extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0022";
  
  /**
   * Appel du programme RPG qui va lire une liste de liens entre lignes pour une liste de lignes de base.
   */
  public List<ListeLienLigne> chargerListeLienLigne(SystemeManager pSysteme, ListeLigneVenteBase pListeLigneVenteBase) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    
    List<ListeLienLigne> ListeLiens = new ArrayList<ListeLienLigne>();
    if (pListeLigneVenteBase != null) {
      
      for (int i = 0; i < pListeLigneVenteBase.size(); i++) {
        LigneVenteBase ligne = pListeLigneVenteBase.get(i);
        IdDocumentVente idDocumentVente = IdDocumentVente.getInstanceGenerique(ligne.getId().getIdEtablissement(),
            ligne.getId().getCodeEntete(), ligne.getId().getNumero(), ligne.getId().getSuffixe(), 0);
        IdLienLigne idLien = IdLienLigne.getInstanceVente(ligne.getId().getIdEtablissement(), EnumTypeLienLigne.LIEN_LIGNE_VENTE,
            ligne.getId().getCodeEntete().getCode(), ligne.getId().getNumero(), ligne.getId().getSuffixe(), 0,
            ligne.getId().getNumeroLigne(), idDocumentVente, ligne.getId().getNumeroLigne(), false);
        
        ListeLiens.add(chargerListeLienLigne(pSysteme, idLien));
      }
    }
    return ListeLiens;
  }
  
  /**
   * Appel du programme RPG qui va lire une liste de liens entre lignes à partir de l'identifiant d'un lien d'origine (ligne d'origine)
   */
  public ListeLienLigne chargerListeLienLigne(SystemeManager pSysteme, IdLienLigne pIdLigneOrigine) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvx0022i entree = new Svgvx0022i();
    Svgvx0022o sortie = new Svgvx0022o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPitypo(pIdLigneOrigine.getTypeOrigine().getCode());
    if (pIdLigneOrigine.getCodeEnteteOrigine().equals('X')) {
      entree.setPicodo('E');
    }
    else {
      entree.setPicodo(pIdLigneOrigine.getCodeEnteteOrigine());
    }
    entree.setPietbo(pIdLigneOrigine.getCodeEtablissement());
    entree.setPinumo(pIdLigneOrigine.getNumeroOrigine());
    entree.setPisufo(pIdLigneOrigine.getSuffixeOrigine());
    entree.setPinlio(pIdLigneOrigine.getNumeroLigneOrigine());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      return null;
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    ListeLienLigne listeLien = new ListeLienLigne();
    
    Object[] lignes = new Object[sortie.getValeursBrutesLignes().length];
    lignes = sortie.getValeursBrutesLignes();
    for (int i = 0; i < lignes.length; i++) {
      boolean isSoldee = !sortie.getPoind().trim().isEmpty();
      if (traiterUneLigne(pIdLigneOrigine, (Object[]) lignes[i], isSoldee) != null) {
        listeLien.add(traiterUneLigne(pIdLigneOrigine, (Object[]) lignes[i], isSoldee));
      }
    }
    
    return listeLien;
  }
  
  /**
   * Découpage d'une ligne.
   */
  private LienLigne traiterUneLigne(IdLienLigne pIdLigneOrigine, Object[] ligneBrute, boolean isSoldee) {
    if (ligneBrute == null) {
      return null;
    }
    
    // Ligne d'origine
    IdLienLigne idLigneOrigine = pIdLigneOrigine;
    IdEtablissement idEtablissementOrigine = idLigneOrigine.getIdEtablissement();
    Character codeEnteteOrigine = ((String) ligneBrute[Svgvx0022d.VAR_LICOD]).charAt(0);
    Integer numeroDocumentOrigine = ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LINUM]).intValue();
    Integer suffixeDocumentOrigine = ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LISUF]).intValue();
    Integer numeroFactureOrigine = ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LINFA]).intValue();
    Integer numeroLigneOrigine = ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LINLI]).intValue();
    EnumTypeLienLigne typeOrigine = idLigneOrigine.getTypeOrigine();
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) ligneBrute[Svgvx0022d.VAR_LOETB]);
    LienLigne lien = null;
    
    if (((String) ligneBrute[Svgvx0022d.VAR_LOTYP]).trim().equalsIgnoreCase("V")) {
      EnumCodeEnteteDocumentVente codeEntete =
          EnumCodeEnteteDocumentVente.valueOfByCode(((String) ligneBrute[Svgvx0022d.VAR_LOCOD]).charAt(0));
      IdDocumentVente idDocumentVente =
          IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LONUM]).intValue(),
              ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LOSUF]).intValue(), ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LONFA]).intValue());
      
      lien = new LienLigne(IdLienLigne.getInstanceVente(idEtablissementOrigine, typeOrigine, codeEnteteOrigine, numeroDocumentOrigine,
          suffixeDocumentOrigine, numeroFactureOrigine, numeroLigneOrigine, idDocumentVente,
          ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LONLI]).intValue(), isSoldee));
    }
    else {
      EnumCodeEnteteDocumentAchat codeEntete =
          EnumCodeEnteteDocumentAchat.valueOfByCode(((String) ligneBrute[Svgvx0022d.VAR_LOCOD]).charAt(0));
      IdDocumentAchat idDocumentAchat = IdDocumentAchat.getInstance(idEtablissement, codeEntete,
          ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LONUM]).intValue(), ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LOSUF]).intValue());
      lien = new LienLigne(IdLienLigne.getInstanceAchat(idEtablissementOrigine, typeOrigine, codeEnteteOrigine, numeroDocumentOrigine,
          suffixeDocumentOrigine, numeroFactureOrigine, numeroLigneOrigine, idDocumentAchat,
          ((BigDecimal) ligneBrute[Svgvx0022d.VAR_LONLI]).intValue()));
    }
    
    return lien;
  }
}
