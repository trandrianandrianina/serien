/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux;

import java.util.ArrayList;

/**
 * Parametres Magento //TODO VA FALLOIR ABSOLUMENT FAIRE UN MODULE DE PARAMETRES POUR PAS QUE TOUT CA SOIT EN DUR -> A FAIRE DANS SERIE N
 */
public class ParametresFluxInit {
  private static final String PARAM_TYPE_BOOLEAN = "BOO";
  private static final String PARAM_TYPE_STRING = "STR";
  private static final String PARAM_TYPE_INTEGER = "INT";
  private static final String PARAM_TYPE_PASSWORD = "PAS";
  
  // Code à passer au CLIN9 de PGVMCLIM pour que le client soit un particulier
  public static final String CODE_CLIN9 = "6";
  public static final int TAILLE_MAX_CHAMPS_ADRESSES = 30;
  public static final int TAILLE_MAX_CHAMP_VILLE = 24;
  public static final int TAILLE_MAX_BLOC_NOTES = 120;
  // Code retour du Web service Magento en cas de refus
  public static final int WS_RETOUR_REFUS = 0;
  // Code retour du Web service Magento en cas de succès
  public static final int WS_RETOUR_TRUE = 1;
  public static final String EDI_FIN_CHAMP = ";";
  public static final String EDI_FIN_LIGNE = "\n";
  
  public static final String PM_TEST_WEB_SERVICES = "MODE_TEST_WEB_SERVICES";
  public static final String PM_MODE_DEBUG = "MODE_DEBUG";
  public static final String PM_BIB_FORCEE_WS = "BIB_FORCEE_WS";
  public static final String PM_NB_FLUX_MAX_PAR_BOUCLE = "NB_FLUX_MAX_PAR_BOUCLE";
  public static final String PM_SEUIL_ALERTE_ENVOI = "SEUIL_ALERTE_ENVOI";
  public static final String PM_TENTATIVES_MAX_ENVOI = "TENTATIVES_MAX_ENVOI";
  public static final String PM_NBJOUR_FLUX_MAX = "NBJOUR_FLUX_MAX";
  public static final String PM_MAGASIN_GENERAL = "MAGASIN_GENERAL";
  public static final String PM_VENDEUR_MAGENTO = "VENDEUR_MAGENTO";
  public static final String PM_CATEGORIE_PARTICULIER = "CATEGORIE_PARTICULIER";
  public static final String PM_CATEGORIE_ARTISAN = "CATEGORIE_ARTISAN";
  public static final String PM_REGLEMENT_PARTICULIER = "REGLEMENT_PARTICULIER";
  public static final String PM_FACTU_NORMALE_CLIENT = "FACTU_NORMALE_CLIENT";
  public static final String PM_LIVRAISON_MAGENTO = "LIVRAISON_MAGENTO";
  public static final String PM_URL_WS_MAGENTO = "URL_WS_MAGENTO";
  public static final String PM_LOGIN_WS_MAGENTO = "LOGIN_WS_MAGENTO";
  public static final String PM_MP_WS_MAGENTO = "MP_WS_MAGENTO";
  public static final String PM_EDITIONS_RACINE = "EDITIONS_RACINE";
  public static final String PM_EDITIONS_CHEMIN_FAC = "EDITIONS_CHEMIN_FAC";
  public static final String PM_PREFIXE_NOM_FAC = "PREFIXE_NOM_FAC";
  public static final String PM_MAIL_DESTINATAIRE_ERREURS = "MAIL_DESTINATAIRE_ERREURS";
  public static final String PM_NOTIF_DESTINATAIRE_ERREURS = "NOTIF_DESTINATAIRE_ERREURS";
  
  public static final String PM_DESTINATAIRE_MAIL_EDI = "DESTINATAIRE_MAIL_EDI";
  public static final String PM_URL_EDI_FTP = "URL_EDI_FTP";
  public static final String PM_LOGIN_EDI_FTP = "LOGIN_EDI_FTP";
  public static final String PM_MP_EDI_FTP = "MP_EDI_FTP";
  public static final String PM_VENDEUR_EDI = "VENDEUR_EDI";
  public static final String PM_ENLEVEMENT_EDI = "ENLEVEMENT_EDI";
  public static final String PM_DOSSIER_CDES_EDI_FTP = "DOSSIER_CDES_EDI_FTP";
  public static final String PM_DOSSIER_EXPE_EDI_FTP = "DOSSIER_EXPE_EDI_FTP";
  public static final String PM_DOSSIER_FACT_EDI_FTP = "DOSSIER_FACT_EDI_FTP";
  public static final String PM_DOSSIER_LOCAL_TMP_EDI = "DOSSIER_LOCAL_TMP_EDI";
  public static final String PM_DOSSIER_LOCAL_CDES_EDI = "DOSSIER_LOCAL_CDES_EDI";
  public static final String PM_DOSSIER_LOCAL_EXPE_EDI = "DOSSIER_LOCAL_EXPE_EDI";
  public static final String PM_DOSSIER_LOCAL_FACT_EDI = "DOSSIER_LOCAL_FACT_EDI";
  public static final String PM_DOSSIER_LOCAL_ARCH_EDI = "DOSSIER_LOCAL_ARCH_EDI";
  public static final String PM_DOSSIER_LOCAL_QTAINE_EDI = "DOSSIER_LOCAL_QTAINE_EDI";
  public static final String PM_PREFIXE_EDI_ORDERS = "PREFIXE_EDI_ORDERS";
  public static final String PM_PREFIXE_EDI_DESADV = "PREFIXE_EDI_DESADV";
  public static final String PM_PREFIXE_EDI_INVOIC = "PREFIXE_EDI_INVOIC";
  public static final String PM_EAN_FOURNISSEUR_EDI = "EAN_FOURNISSEUR_EDI";
  public static final String PM_LIB_FOURNISSEUR_EDI = "LIB_FOURNISSEUR_EDI";
  public static final String PM_EDI_EAN_FACTOR = "EDI_EAN_FACTOR";
  public static final String PM_EDI_LIB_FACTOR = "EDI_LIB_FACTOR";
  public static final String PM_EDI_UNITE_VENTE = "EDI_UNITE_VENTE";
  public static final String PM_EDI_MODE_TEST = "EDI_MODE_TEST";
  
  private static ParametreFlux MODE_TEST_WEB_SERVICES =
      new ParametreFlux(PM_TEST_WEB_SERVICES, "1", PARAM_TYPE_BOOLEAN, true, "On simule l'envoi des données à un Web Service: Mode test");
  private static ParametreFlux MODE_DEBUG =
      new ParametreFlux(PM_MODE_DEBUG, "1", PARAM_TYPE_BOOLEAN, true, "On active ou desactive les logs informatifs coté applicatif");
  private static ParametreFlux BIB_FORCEE_WS =
      new ParametreFlux(PM_BIB_FORCEE_WS, null, PARAM_TYPE_STRING, true, "Bibliothèque de forcage de l'appel de nos Web Services");
  private static ParametreFlux NB_FLUX_MAX_PAR_BOUCLE = new ParametreFlux(PM_NB_FLUX_MAX_PAR_BOUCLE, "10", PARAM_TYPE_STRING, true,
      "Nombre de flux maximum traités par boucle de passage dans la table");
  private static ParametreFlux SEUIL_ALERTE_ENVOI = new ParametreFlux(PM_SEUIL_ALERTE_ENVOI, "10", PARAM_TYPE_INTEGER, true,
      "Nombre de tentatives avant alerte si l'envoi du flux est en échec");
  private static ParametreFlux TENTATIVES_MAX_ENVOI = new ParametreFlux(PM_TENTATIVES_MAX_ENVOI, "1000", PARAM_TYPE_INTEGER, true,
      "Nombre de tentatives avant fermeture du flux si l'envoi du flux est en échec");
  private static ParametreFlux MAGASIN_GENERAL =
      new ParametreFlux(PM_MAGASIN_GENERAL, "MG", PARAM_TYPE_STRING, true, "Magasin par défaut lors de l'import des commandes");
  private static ParametreFlux VENDEUR_MAGENTO =
      new ParametreFlux(PM_VENDEUR_MAGENTO, "*W", PARAM_TYPE_STRING, true, "Code vendeur Série N par défaut pour les commandes Magento");
  private static ParametreFlux CATEGORIE_PARTICULIER = new ParametreFlux(PM_CATEGORIE_PARTICULIER, "P01", PARAM_TYPE_STRING, true,
      "Code catégorie client Série N dédié aux particuliers...");
  private static ParametreFlux CATEGORIE_ARTISAN =
      new ParametreFlux(PM_CATEGORIE_ARTISAN, "I01", PARAM_TYPE_STRING, true, "Code catégorie client Série N dédié aux artisans ");
  private static ParametreFlux REGLEMENT_PARTICULIER = new ParametreFlux(PM_REGLEMENT_PARTICULIER, "CB", PARAM_TYPE_STRING, true,
      "Réglement client Série N par défaut pour insertion particuliers depuis Magento");
  private static ParametreFlux FACTU_NORMALE_CLIENT = new ParametreFlux(PM_FACTU_NORMALE_CLIENT, "N", PARAM_TYPE_STRING, true,
      "Type de facturation par défaut dans Série N pour import des clients Magento");
  private static ParametreFlux LIVRAISON_MAGENTO = new ParametreFlux(PM_LIVRAISON_MAGENTO, "C1", PARAM_TYPE_STRING, true,
      "Code de livraison par défaut dans Série N pour import commandes depuis Magento");
  private static ParametreFlux URL_WS_MAGENTO =
      new ParametreFlux(PM_URL_WS_MAGENTO, "", PARAM_TYPE_STRING, true, "URL du web service Magento");
  private static ParametreFlux LOGIN_WS_MAGENTO =
      new ParametreFlux(PM_LOGIN_WS_MAGENTO, "", PARAM_TYPE_STRING, true, "login de connexion au Web service Magento");
  private static ParametreFlux MP_WS_MAGENTO =
      new ParametreFlux(PM_MP_WS_MAGENTO, "", PARAM_TYPE_PASSWORD, true, "Mot de passe connexion au Web service Magento");
  private static ParametreFlux EDITIONS_RACINE =
      new ParametreFlux(PM_EDITIONS_RACINE, "documents", PARAM_TYPE_STRING, true, "Racine des editions");
  private static ParametreFlux EDITIONS_CHEMIN_FAC =
      new ParametreFlux(PM_EDITIONS_CHEMIN_FAC, "GVM/HTM", PARAM_TYPE_STRING, true, "chemin des factures");
  private static ParametreFlux PREFIXE_NOM_FAC =
      new ParametreFlux(PM_PREFIXE_NOM_FAC, "F", PARAM_TYPE_STRING, true, "Prefixe de nom de factures");
  private static ParametreFlux NBJOUR_FLUX_MAX = new ParametreFlux(PM_NBJOUR_FLUX_MAX, "30", PARAM_TYPE_INTEGER, true,
      "Nombre de jours maximums autorisés pour l'affichage des flux");
  private static ParametreFlux MAIL_DESTINATAIRE_ERREURS =
      new ParametreFlux(PM_MAIL_DESTINATAIRE_ERREURS, "", PARAM_TYPE_STRING, true, "destinataire des mails d'erreurs de flux");
  private static ParametreFlux NOTIF_DESTINATAIRE_ERREURS =
      new ParametreFlux(PM_NOTIF_DESTINATAIRE_ERREURS, "", PARAM_TYPE_STRING, true, "destinataire des notifications d'erreurs de flux");
  
  // EDI
  private static ParametreFlux DESTINATAIRE_MAIL_EDI =
      new ParametreFlux(PM_DESTINATAIRE_MAIL_EDI, "", PARAM_TYPE_STRING, true, "destinataire des mails EDI");
  private static ParametreFlux URL_EDI_FTP =
      new ParametreFlux(PM_URL_EDI_FTP, "", PARAM_TYPE_STRING, true, "URL du serveur FTP de notre partenaire EDI");
  private static ParametreFlux LOGIN_EDI_FTP =
      new ParametreFlux(PM_LOGIN_EDI_FTP, "", PARAM_TYPE_STRING, true, "Profil d'accès du serveur FTP de notre partenaire EDI");
  private static ParametreFlux MP_EDI_FTP =
      new ParametreFlux(PM_MP_EDI_FTP, "", PARAM_TYPE_PASSWORD, true, "Mot de passe d'accès du serveur FTP de notre partenaire EDI");
  private static ParametreFlux VENDEUR_EDI =
      new ParametreFlux(PM_VENDEUR_EDI, "*E", PARAM_TYPE_STRING, true, "Code vendeur Série N par défaut pour les commandes EDI");
  private static ParametreFlux ENLEVEMENT_EDI = new ParametreFlux(PM_ENLEVEMENT_EDI, "C3", PARAM_TYPE_STRING, true,
      "Code de livraison par défaut dans Série N pour import commandes depuis Magento");
  private static ParametreFlux DOSSIER_CDES_EDI_FTP = new ParametreFlux(PM_DOSSIER_CDES_EDI_FTP, "httpdocs/divers/EDI/commandes",
      PARAM_TYPE_STRING, true, "Dossier des commandes EDI sur le serveur FTP");
  private static ParametreFlux DOSSIER_EXPE_EDI_FTP = new ParametreFlux(PM_DOSSIER_EXPE_EDI_FTP, "httpdocs/divers/EDI/avisexp",
      PARAM_TYPE_STRING, true, "Dossier des expéditions EDI sur le serveur FTP");
  private static ParametreFlux DOSSIER_FACT_EDI_FTP = new ParametreFlux(PM_DOSSIER_FACT_EDI_FTP, "httpdocs/divers/EDI/factures",
      PARAM_TYPE_STRING, true, "Dossier des factures EDI sur le serveur FTP");
  private static ParametreFlux DOSSIER_LOCAL_TMP_EDI =
      new ParametreFlux(PM_DOSSIER_LOCAL_TMP_EDI, "/tmp/EDI/", PARAM_TYPE_STRING, true, "Dossier local des données EDI");
  private static ParametreFlux DOSSIER_LOCAL_CDES_EDI =
      new ParametreFlux(PM_DOSSIER_LOCAL_CDES_EDI, "commandes/", PARAM_TYPE_STRING, true, "Dossier local des commandes EDI");
  private static ParametreFlux DOSSIER_LOCAL_EXPE_EDI =
      new ParametreFlux(PM_DOSSIER_LOCAL_EXPE_EDI, "avisexp/", PARAM_TYPE_STRING, true, "Dossier local des expéditions EDI ");
  private static ParametreFlux DOSSIER_LOCAL_FACT_EDI =
      new ParametreFlux(PM_DOSSIER_LOCAL_FACT_EDI, "factures/", PARAM_TYPE_STRING, true, "Dossier local des factures EDI ");
  private static ParametreFlux DOSSIER_LOCAL_ARCH_EDI =
      new ParametreFlux(PM_DOSSIER_LOCAL_ARCH_EDI, "archives/", PARAM_TYPE_STRING, true, "Dossier local des archives EDI ");
  private static ParametreFlux DOSSIER_LOCAL_QTAINE_EDI =
      new ParametreFlux(PM_DOSSIER_LOCAL_QTAINE_EDI, "quarantaine/", PARAM_TYPE_STRING, true, "Dossier local des quarantaines EDI ");
  private static ParametreFlux PREFIXE_EDI_ORDERS =
      new ParametreFlux(PM_PREFIXE_EDI_ORDERS, "cde", PARAM_TYPE_STRING, true, "Préfixe de fichiers ORDERS EDI");
  private static ParametreFlux PREFIXE_EDI_DESADV =
      new ParametreFlux(PM_PREFIXE_EDI_DESADV, "BL-", PARAM_TYPE_STRING, true, "Préfixe de fichiers DESADV EDI");
  private static ParametreFlux PREFIXE_EDI_INVOIC =
      new ParametreFlux(PM_PREFIXE_EDI_INVOIC, "FC-", PARAM_TYPE_STRING, true, "Préfixe de fichiers INVOIC EDI");
  private static ParametreFlux EAN_FOURNISSEUR_EDI = new ParametreFlux(PM_EAN_FOURNISSEUR_EDI, "", PARAM_TYPE_STRING, true,
      "Code EAN du fournisseur (Notre client qui utilise Série N)");
  private static ParametreFlux LIB_FOURNISSEUR_EDI =
      new ParametreFlux(PM_LIB_FOURNISSEUR_EDI, "", PARAM_TYPE_STRING, true, "Libellé du fournisseur (Notre client qui utilise Série N)");
  private static ParametreFlux EDI_EAN_FACTOR =
      new ParametreFlux(PM_EDI_EAN_FACTOR, "", PARAM_TYPE_STRING, true, "Code EAN du prestataire GFACTOR ");
  private static ParametreFlux EDI_LIB_FACTOR =
      new ParametreFlux(PM_EDI_LIB_FACTOR, "", PARAM_TYPE_STRING, true, "libellé du prestataire GFACTOR ");
  private static ParametreFlux EDI_UNITE_VENTE =
      new ParametreFlux(PM_EDI_UNITE_VENTE, "PCE", PARAM_TYPE_STRING, true, "Unité de vente EDI");
  private static ParametreFlux EDI_MODE_TEST =
      new ParametreFlux(PM_EDI_MODE_TEST, "0", PARAM_TYPE_STRING, true, "Mode test du partenaire @GP: 0 PAS TEST / 1 TEST");
  
  // liste statique des parametres obligatoires pour la gestion des flux
  private static ArrayList<ParametreFlux> listeParametres = null;
  
  /**
   * Initialise et met à jour la liste des paramètres de flux.
   */
  public static ArrayList<ParametreFlux> recupererParametresInitiaux() {
    if (listeParametres == null || listeParametres.size() == 0) {
      listeParametres = new ArrayList<ParametreFlux>();
      listeParametres.add(MODE_TEST_WEB_SERVICES);
      listeParametres.add(MODE_DEBUG);
      listeParametres.add(SEUIL_ALERTE_ENVOI);
      listeParametres.add(TENTATIVES_MAX_ENVOI);
      listeParametres.add(MAGASIN_GENERAL);
      listeParametres.add(VENDEUR_MAGENTO);
      listeParametres.add(VENDEUR_EDI);
      listeParametres.add(CATEGORIE_PARTICULIER);
      listeParametres.add(CATEGORIE_ARTISAN);
      listeParametres.add(REGLEMENT_PARTICULIER);
      listeParametres.add(FACTU_NORMALE_CLIENT);
      listeParametres.add(LIVRAISON_MAGENTO);
      listeParametres.add(ENLEVEMENT_EDI);
      listeParametres.add(URL_WS_MAGENTO);
      listeParametres.add(LOGIN_WS_MAGENTO);
      listeParametres.add(MP_WS_MAGENTO);
      listeParametres.add(URL_EDI_FTP);
      listeParametres.add(LOGIN_EDI_FTP);
      listeParametres.add(MP_EDI_FTP);
      listeParametres.add(DOSSIER_CDES_EDI_FTP);
      listeParametres.add(DOSSIER_EXPE_EDI_FTP);
      listeParametres.add(DOSSIER_FACT_EDI_FTP);
      listeParametres.add(DOSSIER_LOCAL_TMP_EDI);
      listeParametres.add(DOSSIER_LOCAL_CDES_EDI);
      listeParametres.add(DOSSIER_LOCAL_EXPE_EDI);
      listeParametres.add(DOSSIER_LOCAL_FACT_EDI);
      listeParametres.add(DOSSIER_LOCAL_ARCH_EDI);
      listeParametres.add(DOSSIER_LOCAL_QTAINE_EDI);
      listeParametres.add(PREFIXE_EDI_ORDERS);
      listeParametres.add(PREFIXE_EDI_DESADV);
      listeParametres.add(PREFIXE_EDI_INVOIC);
      listeParametres.add(EAN_FOURNISSEUR_EDI);
      listeParametres.add(LIB_FOURNISSEUR_EDI);
      listeParametres.add(EDI_EAN_FACTOR);
      listeParametres.add(EDI_LIB_FACTOR);
      listeParametres.add(EDI_UNITE_VENTE);
      listeParametres.add(EDI_MODE_TEST);
      listeParametres.add(NBJOUR_FLUX_MAX);
      listeParametres.add(EDITIONS_RACINE);
      listeParametres.add(EDITIONS_CHEMIN_FAC);
      listeParametres.add(PREFIXE_NOM_FAC);
      listeParametres.add(BIB_FORCEE_WS);
      listeParametres.add(NB_FLUX_MAX_PAR_BOUCLE);
      listeParametres.add(DESTINATAIRE_MAIL_EDI);
      listeParametres.add(MAIL_DESTINATAIRE_ERREURS);
      listeParametres.add(NOTIF_DESTINATAIRE_ERREURS);
    }
    
    return listeParametres;
  }
}
