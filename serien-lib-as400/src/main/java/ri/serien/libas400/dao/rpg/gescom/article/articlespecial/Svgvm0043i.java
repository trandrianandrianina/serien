/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlespecial;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0043i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PICLI = 6;
  public static final int DECIMAL_PICLI = 0;
  public static final int SIZE_PILIV = 3;
  public static final int DECIMAL_PILIV = 0;
  public static final int SIZE_PIFAM = 3;
  public static final int SIZE_PILI1 = 60;
  public static final int SIZE_PILI2 = 60;
  public static final int SIZE_PIUNV = 2;
  public static final int SIZE_PIPXV = 9;
  public static final int DECIMAL_PIPXV = 2;
  public static final int SIZE_PICOF = 1;
  public static final int DECIMAL_PICOF = 0;
  public static final int SIZE_PIFRS = 6;
  public static final int DECIMAL_PIFRS = 0;
  public static final int SIZE_PIPRS = 9;
  public static final int DECIMAL_PIPRS = 2;
  public static final int SIZE_PINGA = 1;
  public static final int DECIMAL_PINGA = 0;
  public static final int SIZE_PIPRA = 11;
  public static final int DECIMAL_PIPRA = 2;
  public static final int SIZE_PIREM1 = 4;
  public static final int DECIMAL_PIREM1 = 2;
  public static final int SIZE_PIREM2 = 4;
  public static final int DECIMAL_PIREM2 = 2;
  public static final int SIZE_PIREM3 = 4;
  public static final int DECIMAL_PIREM3 = 2;
  public static final int SIZE_PIREM4 = 4;
  public static final int DECIMAL_PIREM4 = 2;
  public static final int SIZE_PIFK3 = 7;
  public static final int DECIMAL_PIFK3 = 4;
  public static final int SIZE_PIFV3 = 9;
  public static final int DECIMAL_PIFV3 = 2;
  public static final int SIZE_PIFK1 = 7;
  public static final int DECIMAL_PIFK1 = 4;
  public static final int SIZE_PIFV1 = 9;
  public static final int DECIMAL_PIFV1 = 2;
  public static final int SIZE_PIFP1 = 4;
  public static final int DECIMAL_PIFP1 = 2;
  public static final int SIZE_PISFA = 5;
  public static final int SIZE_PIFP2 = 4;
  public static final int DECIMAL_PIFP2 = 2;
  public static final int SIZE_PICND = 9;
  public static final int DECIMAL_PICND = 3;
  public static final int SIZE_PIUNL = 2;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 257;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PICLI = 2;
  public static final int VAR_PILIV = 3;
  public static final int VAR_PIFAM = 4;
  public static final int VAR_PILI1 = 5;
  public static final int VAR_PILI2 = 6;
  public static final int VAR_PIUNV = 7;
  public static final int VAR_PIPXV = 8;
  public static final int VAR_PICOF = 9;
  public static final int VAR_PIFRS = 10;
  public static final int VAR_PIPRS = 11;
  public static final int VAR_PINGA = 12;
  public static final int VAR_PIPRA = 13;
  public static final int VAR_PIREM1 = 14;
  public static final int VAR_PIREM2 = 15;
  public static final int VAR_PIREM3 = 16;
  public static final int VAR_PIREM4 = 17;
  public static final int VAR_PIFK3 = 18;
  public static final int VAR_PIFV3 = 19;
  public static final int VAR_PIFK1 = 20;
  public static final int VAR_PIFV1 = 21;
  public static final int VAR_PIFP1 = 22;
  public static final int VAR_PISFA = 23;
  public static final int VAR_PIFP2 = 24;
  public static final int VAR_PICND = 25;
  public static final int VAR_PIUNL = 26;
  public static final int VAR_PIARR = 27;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private BigDecimal picli = BigDecimal.ZERO; // Numéro client
  private BigDecimal piliv = BigDecimal.ZERO; // Suffixe client
  private String pifam = ""; // Groupe/Famille
  private String pili1 = ""; // Libellé 1
  private String pili2 = ""; // Libellé 2
  private String piunv = ""; // Unité de vente
  private BigDecimal pipxv = BigDecimal.ZERO; // Prix de Vente
  private BigDecimal picof = BigDecimal.ZERO; // Collectif fournisseur
  private BigDecimal pifrs = BigDecimal.ZERO; // Numéro fournisseur
  private BigDecimal piprs = BigDecimal.ZERO; // Prix de revient standard
  private BigDecimal pinga = BigDecimal.ZERO; // Indicateur Négociation achat
  private BigDecimal pipra = BigDecimal.ZERO; // Prix catalogue
  private BigDecimal pirem1 = BigDecimal.ZERO; // Remise 1
  private BigDecimal pirem2 = BigDecimal.ZERO; // Remise 2
  private BigDecimal pirem3 = BigDecimal.ZERO; // Remise 3
  private BigDecimal pirem4 = BigDecimal.ZERO; // Remise 4
  private BigDecimal pifk3 = BigDecimal.ZERO; // Taxe ou majoration en %
  private BigDecimal pifv3 = BigDecimal.ZERO; // Montant Conditionnement
  private BigDecimal pifk1 = BigDecimal.ZERO; // Coeff.port
  private BigDecimal pifv1 = BigDecimal.ZERO; // Montant Port
  private BigDecimal pifp1 = BigDecimal.ZERO; // % Port
  private String pisfa = ""; // Sous-Famille
  private BigDecimal pifp2 = BigDecimal.ZERO; // Frais 2 % exploitation
  private BigDecimal picnd = BigDecimal.ZERO; // Nombre d"UNV par UNL
  private String piunl = ""; // Unité conditionnement vente
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PICLI, DECIMAL_PICLI), // Numéro client
      new AS400ZonedDecimal(SIZE_PILIV, DECIMAL_PILIV), // Suffixe client
      new AS400Text(SIZE_PIFAM), // Groupe/Famille
      new AS400Text(SIZE_PILI1), // Libellé 1
      new AS400Text(SIZE_PILI2), // Libellé 2
      new AS400Text(SIZE_PIUNV), // Unité de vente
      new AS400ZonedDecimal(SIZE_PIPXV, DECIMAL_PIPXV), // Prix de Vente
      new AS400ZonedDecimal(SIZE_PICOF, DECIMAL_PICOF), // Collectif fournisseur
      new AS400ZonedDecimal(SIZE_PIFRS, DECIMAL_PIFRS), // Numéro fournisseur
      new AS400ZonedDecimal(SIZE_PIPRS, DECIMAL_PIPRS), // Prix de revient standard
      new AS400ZonedDecimal(SIZE_PINGA, DECIMAL_PINGA), // Indicateur Négociation achat
      new AS400ZonedDecimal(SIZE_PIPRA, DECIMAL_PIPRA), // Prix catalogue
      new AS400ZonedDecimal(SIZE_PIREM1, DECIMAL_PIREM1), // Remise 1
      new AS400ZonedDecimal(SIZE_PIREM2, DECIMAL_PIREM2), // Remise 2
      new AS400ZonedDecimal(SIZE_PIREM3, DECIMAL_PIREM3), // Remise 3
      new AS400ZonedDecimal(SIZE_PIREM4, DECIMAL_PIREM4), // Remise 4
      new AS400ZonedDecimal(SIZE_PIFK3, DECIMAL_PIFK3), // Taxe ou majoration en %
      new AS400ZonedDecimal(SIZE_PIFV3, DECIMAL_PIFV3), // Montant Conditionnement
      new AS400ZonedDecimal(SIZE_PIFK1, DECIMAL_PIFK1), // Coeff.port
      new AS400ZonedDecimal(SIZE_PIFV1, DECIMAL_PIFV1), // Montant Port
      new AS400ZonedDecimal(SIZE_PIFP1, DECIMAL_PIFP1), // % Port
      new AS400Text(SIZE_PISFA), // Sous-Famille
      new AS400PackedDecimal(SIZE_PIFP2, DECIMAL_PIFP2), // Frais 2 % exploitation
      new AS400ZonedDecimal(SIZE_PICND, DECIMAL_PICND), // Nombre d"UNV par UNL
      new AS400Text(SIZE_PIUNL), // Unité conditionnement vente
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, pietb, picli, piliv, pifam, pili1, pili2, piunv, pipxv, picof, pifrs, piprs, pinga, pipra, pirem1, pirem2,
          pirem3, pirem4, pifk3, pifv3, pifk1, pifv1, pifp1, pisfa, pifp2, picnd, piunl, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    picli = (BigDecimal) output[2];
    piliv = (BigDecimal) output[3];
    pifam = (String) output[4];
    pili1 = (String) output[5];
    pili2 = (String) output[6];
    piunv = (String) output[7];
    pipxv = (BigDecimal) output[8];
    picof = (BigDecimal) output[9];
    pifrs = (BigDecimal) output[10];
    piprs = (BigDecimal) output[11];
    pinga = (BigDecimal) output[12];
    pipra = (BigDecimal) output[13];
    pirem1 = (BigDecimal) output[14];
    pirem2 = (BigDecimal) output[15];
    pirem3 = (BigDecimal) output[16];
    pirem4 = (BigDecimal) output[17];
    pifk3 = (BigDecimal) output[18];
    pifv3 = (BigDecimal) output[19];
    pifk1 = (BigDecimal) output[20];
    pifv1 = (BigDecimal) output[21];
    pifp1 = (BigDecimal) output[22];
    pisfa = (String) output[23];
    pifp2 = (BigDecimal) output[24];
    picnd = (BigDecimal) output[25];
    piunl = (String) output[26];
    piarr = (String) output[27];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPicli(BigDecimal pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = pPicli.setScale(DECIMAL_PICLI, RoundingMode.HALF_UP);
  }
  
  public void setPicli(Integer pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = BigDecimal.valueOf(pPicli);
  }
  
  public Integer getPicli() {
    return picli.intValue();
  }
  
  public void setPiliv(BigDecimal pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = pPiliv.setScale(DECIMAL_PILIV, RoundingMode.HALF_UP);
  }
  
  public void setPiliv(Integer pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = BigDecimal.valueOf(pPiliv);
  }
  
  public Integer getPiliv() {
    return piliv.intValue();
  }
  
  public void setPifam(String pPifam) {
    if (pPifam == null) {
      return;
    }
    pifam = pPifam;
  }
  
  public String getPifam() {
    return pifam;
  }
  
  public void setPili1(String pPili1) {
    if (pPili1 == null) {
      return;
    }
    pili1 = pPili1;
  }
  
  public String getPili1() {
    return pili1;
  }
  
  public void setPili2(String pPili2) {
    if (pPili2 == null) {
      return;
    }
    pili2 = pPili2;
  }
  
  public String getPili2() {
    return pili2;
  }
  
  public void setPiunv(String pPiunv) {
    if (pPiunv == null) {
      return;
    }
    piunv = pPiunv;
  }
  
  public String getPiunv() {
    return piunv;
  }
  
  public void setPipxv(BigDecimal pPipxv) {
    if (pPipxv == null) {
      return;
    }
    pipxv = pPipxv.setScale(DECIMAL_PIPXV, RoundingMode.HALF_UP);
  }
  
  public void setPipxv(Double pPipxv) {
    if (pPipxv == null) {
      return;
    }
    pipxv = BigDecimal.valueOf(pPipxv).setScale(DECIMAL_PIPXV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipxv() {
    return pipxv.setScale(DECIMAL_PIPXV, RoundingMode.HALF_UP);
  }
  
  public void setPicof(BigDecimal pPicof) {
    if (pPicof == null) {
      return;
    }
    picof = pPicof.setScale(DECIMAL_PICOF, RoundingMode.HALF_UP);
  }
  
  public void setPicof(Integer pPicof) {
    if (pPicof == null) {
      return;
    }
    picof = BigDecimal.valueOf(pPicof);
  }
  
  public Integer getPicof() {
    return picof.intValue();
  }
  
  public void setPifrs(BigDecimal pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = pPifrs.setScale(DECIMAL_PIFRS, RoundingMode.HALF_UP);
  }
  
  public void setPifrs(Integer pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = BigDecimal.valueOf(pPifrs);
  }
  
  public Integer getPifrs() {
    return pifrs.intValue();
  }
  
  public void setPiprs(BigDecimal pPiprs) {
    if (pPiprs == null) {
      return;
    }
    piprs = pPiprs.setScale(DECIMAL_PIPRS, RoundingMode.HALF_UP);
  }
  
  public void setPiprs(Double pPiprs) {
    if (pPiprs == null) {
      return;
    }
    piprs = BigDecimal.valueOf(pPiprs).setScale(DECIMAL_PIPRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiprs() {
    return piprs.setScale(DECIMAL_PIPRS, RoundingMode.HALF_UP);
  }
  
  public void setPinga(BigDecimal pPinga) {
    if (pPinga == null) {
      return;
    }
    pinga = pPinga.setScale(DECIMAL_PINGA, RoundingMode.HALF_UP);
  }
  
  public void setPinga(Integer pPinga) {
    if (pPinga == null) {
      return;
    }
    pinga = BigDecimal.valueOf(pPinga);
  }
  
  public Integer getPinga() {
    return pinga.intValue();
  }
  
  public void setPipra(BigDecimal pPipra) {
    if (pPipra == null) {
      return;
    }
    pipra = pPipra.setScale(DECIMAL_PIPRA, RoundingMode.HALF_UP);
  }
  
  public void setPipra(Double pPipra) {
    if (pPipra == null) {
      return;
    }
    pipra = BigDecimal.valueOf(pPipra).setScale(DECIMAL_PIPRA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipra() {
    return pipra.setScale(DECIMAL_PIPRA, RoundingMode.HALF_UP);
  }
  
  public void setPirem1(BigDecimal pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = pPirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem1(Double pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = BigDecimal.valueOf(pPirem1).setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem1() {
    return pirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(BigDecimal pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = pPirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(Double pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = BigDecimal.valueOf(pPirem2).setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem2() {
    return pirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(BigDecimal pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = pPirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(Double pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = BigDecimal.valueOf(pPirem3).setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem3() {
    return pirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirem4(BigDecimal pPirem4) {
    if (pPirem4 == null) {
      return;
    }
    pirem4 = pPirem4.setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public void setPirem4(Double pPirem4) {
    if (pPirem4 == null) {
      return;
    }
    pirem4 = BigDecimal.valueOf(pPirem4).setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem4() {
    return pirem4.setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public void setPifk3(BigDecimal pPifk3) {
    if (pPifk3 == null) {
      return;
    }
    pifk3 = pPifk3.setScale(DECIMAL_PIFK3, RoundingMode.HALF_UP);
  }
  
  public void setPifk3(Double pPifk3) {
    if (pPifk3 == null) {
      return;
    }
    pifk3 = BigDecimal.valueOf(pPifk3).setScale(DECIMAL_PIFK3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifk3() {
    return pifk3.setScale(DECIMAL_PIFK3, RoundingMode.HALF_UP);
  }
  
  public void setPifv3(BigDecimal pPifv3) {
    if (pPifv3 == null) {
      return;
    }
    pifv3 = pPifv3.setScale(DECIMAL_PIFV3, RoundingMode.HALF_UP);
  }
  
  public void setPifv3(Double pPifv3) {
    if (pPifv3 == null) {
      return;
    }
    pifv3 = BigDecimal.valueOf(pPifv3).setScale(DECIMAL_PIFV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifv3() {
    return pifv3.setScale(DECIMAL_PIFV3, RoundingMode.HALF_UP);
  }
  
  public void setPifk1(BigDecimal pPifk1) {
    if (pPifk1 == null) {
      return;
    }
    pifk1 = pPifk1.setScale(DECIMAL_PIFK1, RoundingMode.HALF_UP);
  }
  
  public void setPifk1(Double pPifk1) {
    if (pPifk1 == null) {
      return;
    }
    pifk1 = BigDecimal.valueOf(pPifk1).setScale(DECIMAL_PIFK1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifk1() {
    return pifk1.setScale(DECIMAL_PIFK1, RoundingMode.HALF_UP);
  }
  
  public void setPifv1(BigDecimal pPifv1) {
    if (pPifv1 == null) {
      return;
    }
    pifv1 = pPifv1.setScale(DECIMAL_PIFV1, RoundingMode.HALF_UP);
  }
  
  public void setPifv1(Double pPifv1) {
    if (pPifv1 == null) {
      return;
    }
    pifv1 = BigDecimal.valueOf(pPifv1).setScale(DECIMAL_PIFV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifv1() {
    return pifv1.setScale(DECIMAL_PIFV1, RoundingMode.HALF_UP);
  }
  
  public void setPifp1(BigDecimal pPifp1) {
    if (pPifp1 == null) {
      return;
    }
    pifp1 = pPifp1.setScale(DECIMAL_PIFP1, RoundingMode.HALF_UP);
  }
  
  public void setPifp1(Double pPifp1) {
    if (pPifp1 == null) {
      return;
    }
    pifp1 = BigDecimal.valueOf(pPifp1).setScale(DECIMAL_PIFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifp1() {
    return pifp1.setScale(DECIMAL_PIFP1, RoundingMode.HALF_UP);
  }
  
  public void setPisfa(String pPisfa) {
    if (pPisfa == null) {
      return;
    }
    pisfa = pPisfa;
  }
  
  public String getPisfa() {
    return pisfa;
  }
  
  public void setPifp2(BigDecimal pPifp2) {
    if (pPifp2 == null) {
      return;
    }
    pifp2 = pPifp2.setScale(DECIMAL_PIFP2, RoundingMode.HALF_UP);
  }
  
  public void setPifp2(Double pPifp2) {
    if (pPifp2 == null) {
      return;
    }
    pifp2 = BigDecimal.valueOf(pPifp2).setScale(DECIMAL_PIFP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifp2() {
    return pifp2.setScale(DECIMAL_PIFP2, RoundingMode.HALF_UP);
  }
  
  public void setPicnd(BigDecimal pPicnd) {
    if (pPicnd == null) {
      return;
    }
    picnd = pPicnd.setScale(DECIMAL_PICND, RoundingMode.HALF_UP);
  }
  
  public void setPicnd(Double pPicnd) {
    if (pPicnd == null) {
      return;
    }
    picnd = BigDecimal.valueOf(pPicnd).setScale(DECIMAL_PICND, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPicnd() {
    return picnd.setScale(DECIMAL_PICND, RoundingMode.HALF_UP);
  }
  
  public void setPiunl(String pPiunl) {
    if (pPiunl == null) {
      return;
    }
    piunl = pPiunl;
  }
  
  public String getPiunl() {
    return piunl;
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
