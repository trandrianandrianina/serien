/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.documentvente;

import java.util.ArrayList;
import java.util.Date;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.vente.document.DemandeDeblocage;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlDemandeDeblocage {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlDemandeDeblocage(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  // -- Méthodes publiques
  
  /**
   * Lecture d'une demande de déblocage.
   */
  public DemandeDeblocage chargerDemandeDeblocage(IdDocumentVente pIdDocumentVente) {
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation et lancement de la requête
    String requete = "select * from " + querymg.getLibrary() + '.' + EnumTableBDD.DEMANDE_DEBLOCAGE_DOCUMENT
        + " where HDETB = '" + pIdDocumentVente.getCodeEtablissement() + "' " + "and HDBON = '"
        + pIdDocumentVente.getIndicatif(IdDocumentVente.INDICATIF_ENTETE_ETB_NUM_SUF) + "'";
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // Initialisation des champs
    DemandeDeblocage demandeDeblocage = new DemandeDeblocage();
    initialiserDonnees(listeRecord.get(0), demandeDeblocage);
    return demandeDeblocage;
  }
  
  /**
   * Ecriture d'une demande de déblocage.
   */
  public void sauverDemandeDeblocage(IdDocumentVente pIdDocumentVente, String pProfil) {
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    if (pProfil == null) {
      throw new MessageErreurException("Le profil du demandeur est à null.");
    }
    if (pProfil.trim().isEmpty()) {
      throw new MessageErreurException("Le profil du demandeur est vide.");
    }
    
    // On commence par vérifier que la demande n'existe pas déjà
    DemandeDeblocage dDeblocage = chargerDemandeDeblocage(pIdDocumentVente);
    
    String requete = "";
    // La demande n'existe pas: on passe en insertion
    if (dDeblocage == null) {
      dDeblocage = new DemandeDeblocage();
      dDeblocage.setDateCreation(new Date());
      dDeblocage.setProfilDemandeur(pProfil);
      dDeblocage.setIdDocumentVente(pIdDocumentVente);
      
      // Construction de la requête
      requete = "insert into " + querymg.getLibrary() + '.' + EnumTableBDD.DEMANDE_DEBLOCAGE_DOCUMENT
          + " (HDCRE, HDTIM, HDUSR, HDETB, HDBON) " + " values (" + dDeblocage.retournerDateCreationFormatee() + ", "
          + dDeblocage.retournerHeureCreationFormatee() + ", " + RequeteSql.formaterStringSQL(dDeblocage.getProfilDemandeur()) + ", "
          + RequeteSql.formaterStringSQL(dDeblocage.getIdDocumentVente().getIdEtablissement().getCodeEtablissement()) + ", "
          + RequeteSql.formaterStringSQL(dDeblocage.getIdDocumentVente().getIndicatif(IdDocumentVente.INDICATIF_ENTETE_ETB_NUM_SUF))
          + ")";
    }
    // La demande existe: on passe en modification
    else {
      dDeblocage.setDateCreation(new Date());
      dDeblocage.setProfilDemandeur(pProfil);
      dDeblocage.setProfilResponsable("");
      dDeblocage.setReponse("");
      dDeblocage.setMotif("");
      
      // Construction de la requête
      requete = "update " + querymg.getLibrary() + '.' + EnumTableBDD.DEMANDE_DEBLOCAGE_DOCUMENT + " set HDCRE="
          + dDeblocage.retournerDateCreationFormatee() + "," + " HDTIM=" + dDeblocage.retournerHeureCreationFormatee() + "," + " HDUSR="
          + RequeteSql.formaterStringSQL(dDeblocage.getProfilDemandeur()) + "," + " HDRES="
          + RequeteSql.formaterStringSQL(dDeblocage.getProfilResponsable()) + "," + " HDAUT="
          + RequeteSql.formaterStringSQL(dDeblocage.getReponse()) + "," + " HDMOT=" + RequeteSql.formaterStringSQL(dDeblocage.getMotif())
          + " where HDETB = " + RequeteSql.formaterStringSQL(dDeblocage.getIdDocumentVente().getIdEtablissement().getCodeEtablissement())
          + " and HDBON = "
          + RequeteSql.formaterStringSQL(dDeblocage.getIdDocumentVente().getIndicatif(IdDocumentVente.INDICATIF_ENTETE_ETB_NUM_SUF));
    }
    
    // Lancement de la requête
    int retour = querymg.requete(requete);
    if (retour == Constantes.ERREUR) {
      throw new MessageErreurException(querymg.getMsgError());
    }
  }
  
  /**
   * Supprime une demande de déblocage.
   */
  public void supprimerDemandeDeblocage(IdDocumentVente pIdDocumentVente) {
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // On contrôle que la demande existe
    DemandeDeblocage demande = chargerDemandeDeblocage(pIdDocumentVente);
    if (demande == null) {
      return;
    }
    
    // Préparation et lancement de la requête
    String requete = "delete from " + querymg.getLibrary() + '.' + EnumTableBDD.DEMANDE_DEBLOCAGE_DOCUMENT + " where HDETB = '"
        + pIdDocumentVente.getCodeEtablissement() + "' " + "and HDBON = '"
        + pIdDocumentVente.getIndicatif(IdDocumentVente.INDICATIF_ENTETE_ETB_NUM_SUF) + "'";
    
    // Lancement de la requête
    int retour = querymg.requete(requete);
    if (retour == Constantes.ERREUR) {
      throw new MessageErreurException(querymg.getMsgError());
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise les données de la classe à partir du generic record.
   */
  private void initialiserDonnees(GenericRecord pRecord, DemandeDeblocage pDemandeDeblocage) {
    String date = "" + pRecord.getIntValue("HDCRE", 0);
    int heure = pRecord.getIntValue("HDTIM", 0);
    pDemandeDeblocage.setDateCreation(ConvertDate.db2ToDate(date, heure, null));
    pDemandeDeblocage.setProfilDemandeur(pRecord.getStringValue("HDUSR", "", true));
    pDemandeDeblocage.setIdDocumentVente(IdDocumentVente.getInstanceParIndicatif(pRecord.getStringValue("HDBON", "", true)));
    pDemandeDeblocage.setProfilResponsable(pRecord.getStringValue("HDRES", "", true));
    pDemandeDeblocage.setReponse(pRecord.getStringValue("HDAUT", "", true));
    pDemandeDeblocage.setMotif(pRecord.getStringValue("HDMOT", "", false));
  }
}
