/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.math.BigDecimal;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.EnumOriginePrixVente;
import ri.serien.libcommun.gescom.vente.ligne.EnumTypeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVenteBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

/**
 * Centralise les accès aux programmes RPG permettant de manipuler les lignes des documents de ventes.
 */
public class RpgLigneVenteBase extends RpgBase {
  // Constantes
  private static final String SVGVM0059_CHARGER_LISTE_LIGNE_VENTE_BASE = "SVGVM0059";
  
  /**
   * Charger les lignes du document de ventes correspondant aux critères de recherche.
   */
  public ListeLigneVenteBase chargerListeLigneVenteBase(SystemeManager pSystemManager, List<IdLigneVente> pListeIdLigneVente) {
    
    // Préparation des paramètres du programme RPG
    Svgvm0059i entree = new Svgvm0059i();
    Svgvm0059o sortie = new Svgvm0059o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    ListeLigneVenteBase listeLigneVenteBase = new ListeLigneVenteBase();
    
    // Initialisation des paramètres d'entrée
    char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
    entree.setPiind(new String(indicateurs));
    entree.setPietb(pListeIdLigneVente.get(0).getCodeEtablissement());
    
    int index = 0;
    entree.setPiid01(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid02(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid03(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid04(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid05(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid06(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid07(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid08(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid09(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid10(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid11(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid12(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid13(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid14(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid15(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid16(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid17(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid18(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid19(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid20(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid21(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid22(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid23(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid24(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid25(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid26(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid27(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid28(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid29(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid30(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid31(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid32(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid33(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid34(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid35(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid36(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid37(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid38(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid39(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid40(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid41(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid42(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid43(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid44(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid45(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid46(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid47(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid48(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid49(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid50(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid51(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid52(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid53(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid54(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid55(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid56(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid57(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid58(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid59(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid60(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid61(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid62(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid63(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid64(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid65(alimenterIdLigneVente(pListeIdLigneVente, index++));
    
    // Initialiser des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparer de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSystemManager);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécuter le programme RPG
    if (!rpg.executerProgramme(SVGVM0059_CHARGER_LISTE_LIGNE_VENTE_BASE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors du chargement des lignes");
    }
    
    // Récupérer les paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Vérifier la présence d'erreurs
    controlerErreur();
    
    // Renseigner le résultat
    for (Object ligne : sortie.getValeursBrutesLignes()) {
      if (ligne == null) {
        break;
      }
      listeLigneVenteBase.add(traiterUneLigne((Object[]) ligne));
    }
    
    return listeLigneVenteBase;
    
  }
  
  /**
   * Découpage d'une ligne.
   */
  private LigneVenteBase traiterUneLigne(Object[] ligneBrute) {
    if (ligneBrute == null) {
      return null;
    }
    
    // Générer l'identifiant de l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) ligneBrute[Svgvm0059d.VAR_WL1ETB]);
    
    // Génèrer l'identifiant de la ligne de ventes
    EnumCodeEnteteDocumentVente codeEntete =
        EnumCodeEnteteDocumentVente.valueOfByCode(((String) ligneBrute[Svgvm0059d.VAR_WL1COD]).charAt(0));
    Integer numero = ((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1NUM]).intValue();
    Integer suffixe = ((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1SUF]).intValue();
    Integer numeroLigne = ((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1NLI]).intValue();
    IdLigneVente idLigneVente = IdLigneVente.getInstance(idEtablissement, codeEntete, numero, suffixe, numeroLigne);
    
    // Créer la ligne de ventes
    LigneVenteBase ligneVenteBase = new LigneVenteBase(idLigneVente);
    
    // Type de ligne
    ligneVenteBase.setTypeLigne(EnumTypeLigneVente.valueOfByCode(((String) ligneBrute[Svgvm0059d.VAR_WL1ERL])));
    
    // Numéro de facture
    ligneVenteBase.setNumeroFacture(((BigDecimal) ligneBrute[Svgvm0059d.VAR_WE1NFA]).intValue());
    
    // Identifiant article
    String codeArticle = (String) ligneBrute[Svgvm0059d.VAR_WL1ART];
    if (!codeArticle.trim().isEmpty()) {
      IdArticle idArticle = IdArticle.getInstance(idEtablissement, codeArticle);
      ligneVenteBase.setIdArticle(idArticle);
    }
    
    // Libellé de la ligne
    ligneVenteBase.setLibelle((String) ligneBrute[Svgvm0059d.VAR_WLIGLB]);
    
    // Information complémentaire
    // TODO : Modifier le service RPG pour avoir l'information complémentaire
    
    // Unité de ventes
    String uniteVente = ((String) ligneBrute[Svgvm0059d.VAR_WL1UNV]).trim();
    if (!uniteVente.isEmpty()) {
      IdUnite idUV = IdUnite.getInstance(uniteVente);
      ligneVenteBase.setIdUniteVente(idUV);
    }
    
    // Précision de l'unité de ventes
    int nombreDecimaleUV = ((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1DCV]).intValue();
    ligneVenteBase.setNombreDecimaleUV(nombreDecimaleUV);
    
    // Conditionnement de ventes
    ligneVenteBase.setNombreUVParUCV(((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1CND]));
    
    // Unité de conditionnement de ventes
    String uniteConditionnementVente = ((String) ligneBrute[Svgvm0059d.VAR_WA1UNL]).trim();
    if (!uniteConditionnementVente.isEmpty()) {
      IdUnite idUC = IdUnite.getInstance(uniteConditionnementVente);
      ligneVenteBase.setIdUniteConditionnementVente(idUC);
    }
    
    ligneVenteBase.setCodeExtraction(((String) ligneBrute[Svgvm0059d.VAR_WL1CEX]).charAt(0));
    
    ligneVenteBase.setNombreDecoupe(((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL2NBR]).intValue());
    ligneVenteBase.setCodeRegroupement(((String) ligneBrute[Svgvm0059d.VAR_WL1IN21]).charAt(0));
    
    // Le prix de vente
    if (!ligneVenteBase.isLigneCommentaire()) {
      // Quantité en UV, arrondie avec la précision stockée dans la ligne de ventes
      ligneVenteBase.setQuantiteUV(((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1QTE]).setScale(nombreDecimaleUV));
      
      ligneVenteBase.setPrixBaseHT((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1PVB]);
      int originePrixNet = ((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1TN]).intValue();
      ligneVenteBase.setProvenancePrixNet(EnumProvenancePrixNet.valueOfByCode(originePrixNet));
      ligneVenteBase.setPrixNetSaisiHT((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1PVN]);
      ligneVenteBase.setPrixNetCalculeHT((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1PVC]);
      ligneVenteBase.setMontantHT((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1MHT]);
      try {
        Character originePrixVente = ((String) ligneBrute[Svgvm0059d.VAR_WORIPR]).charAt(0);
        ligneVenteBase.setOriginePrixVente(EnumOriginePrixVente.valueOfByCode(originePrixVente));
      }
      catch (Exception e) {
      }
      
      ligneVenteBase
          .setTauxRemise1(new BigPercentage((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1REM1], EnumFormatPourcentage.POURCENTAGE));
      ligneVenteBase
          .setTauxRemise2(new BigPercentage((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1REM2], EnumFormatPourcentage.POURCENTAGE));
      ligneVenteBase
          .setTauxRemise3(new BigPercentage((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1REM3], EnumFormatPourcentage.POURCENTAGE));
      ligneVenteBase
          .setTauxRemise4(new BigPercentage((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1REM4], EnumFormatPourcentage.POURCENTAGE));
      ligneVenteBase
          .setTauxRemise5(new BigPercentage((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1REM5], EnumFormatPourcentage.POURCENTAGE));
      ligneVenteBase
          .setTauxRemise6(new BigPercentage((BigDecimal) ligneBrute[Svgvm0059d.VAR_WL1REM6], EnumFormatPourcentage.POURCENTAGE));
    }
    
    return ligneVenteBase;
  }
  
  /**
   * Méthode interne pour alimenter facilement les 65 champs comportant l'id des lignes de vente.
   */
  private String alimenterIdLigneVente(List<IdLigneVente> pListeIdLigneVente, int index) {
    if (index < pListeIdLigneVente.size()) {
      return pListeIdLigneVente.get(index).getIndicatifPourTraitement();
    }
    else {
      return "               ";
    }
  }
  
}
