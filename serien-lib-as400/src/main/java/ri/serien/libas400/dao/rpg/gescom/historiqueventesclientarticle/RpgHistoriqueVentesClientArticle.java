/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.historiqueventesclientarticle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.CriteresRechercheDocumentsHistoriqueVentesClientArticle;
import ri.serien.libcommun.gescom.commun.client.DocumentHistoriqueVenteClientArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumEtatBonDocumentVente;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgHistoriqueVentesClientArticle extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0020";
  
  /**
   * Appel du programme RPG qui va rechercher les documents pour un client
   */
  public List<DocumentHistoriqueVenteClientArticle> chargerDocumentHistoriqueVenteClientArticle(SystemeManager pSysteme,
      IdClient pIdClient, IdArticle pIdArticle, CriteresRechercheDocumentsHistoriqueVentesClientArticle pCriteres) {
    IdArticle.controlerId(pIdArticle, true);
    
    List<DocumentHistoriqueVenteClientArticle> documents = null;
    
    // Préparation des paramètres du programme RPG
    Svgvm0020i entree = new Svgvm0020i();
    Svgvm0020o sortie = new Svgvm0020o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdClient.getCodeEtablissement());
    entree.setPicli(pIdClient.getNumero());
    entree.setPiliv(pIdClient.getSuffixe());
    entree.setPiart(pIdArticle.getCodeArticle());
    entree.setPipag(pCriteres.getNumeroPage());
    entree.setPilig(pCriteres.getNbrLignesParPage());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      // Entrée
      // Sortie
      documents = new ArrayList<DocumentHistoriqueVenteClientArticle>();
      Object[] lignes = sortie.getValeursBrutesLignes();
      for (int i = 0; i < lignes.length; i++) {
        if (lignes[i] != null) {
          documents.add(traiterUneLigne((Object[]) lignes[i]));
        }
      }
    }
    
    return documents;
  }
  
  /**
   * Découpage d'une ligne
   * @param line
   * @return
   */
  private DocumentHistoriqueVenteClientArticle traiterUneLigne(Object[] line) {
    if (line == null) {
      return null;
    }
    
    DocumentHistoriqueVenteClientArticle document = new DocumentHistoriqueVenteClientArticle();
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) line[Svgvm0020d.VAR_WETB]);
    document.setIdEtablissement(idEtablissement); // code établissement
    document.setNumeroBon(((BigDecimal) line[Svgvm0020d.VAR_WNUM]).intValue()); // Numéro de bon
    document.setSuffixeBon(((BigDecimal) line[Svgvm0020d.VAR_WSUF]).intValue()); // suffixe du bon
    document.setTypeDocument(((String) line[Svgvm0020d.VAR_WTYP])); // type de document
    // date du bon
    document.setDateCreation(ConvertDate.db2ToDate(((BigDecimal) line[Svgvm0020d.VAR_WDAT]).intValue(), null));
    // état du bon
    document.setEtatBon(EnumEtatBonDocumentVente.valueOfByCode(((BigDecimal) line[Svgvm0020d.VAR_WETA]).intValue()));
    document.setCodeVendeur(((String) line[Svgvm0020d.VAR_WVDE])); // code vendeur
    // date de facture
    document.setDateFacture(ConvertDate.db2ToDate(((BigDecimal) line[Svgvm0020d.VAR_WFAC]).intValue(), null));
    document.setNumeroFacture(((BigDecimal) line[Svgvm0020d.VAR_WNFA]).intValue()); // numéro de facture
    document.setQuantite(((BigDecimal) line[Svgvm0020d.VAR_WQTE])); // quantité
    document.setPrixBase(((BigDecimal) line[Svgvm0020d.VAR_WPVB])); // prix de vente de base
    document.setPrixNet(((BigDecimal) line[Svgvm0020d.VAR_WPVN])); // prix de vente net
    document.setPrixCalcule(((BigDecimal) line[Svgvm0020d.VAR_WPVC])); // prix de vente calculé
    
    document.setModeRecuperation((String) line[Svgvm0020d.VAR_WENLV]); // Mode de récupération de la marchandise (enlèvement ou livraison)
    return document;
  }
}
