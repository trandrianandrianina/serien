/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.cgm.database.files;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pcgmpacm_ds_JO pour les JO
 * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
 */

public class Pcgmpacm_ds_JO extends DataStructureRecord {

  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "JOLIB"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "JOTYP"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "JOCAF"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "JONTT"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "JONTA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "JOTRE"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "JONCG"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), "JOCBQ"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), "JOGUI"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(11), "JONCB"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "JOCLE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "JOAGB"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "JONCA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "JOAUT"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "JOCEN"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "JORAP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "JODRA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "JOPCE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "JOCAL"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(32, 0), "JON"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "JOLIG"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "JOCOL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "JONDE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "JOSJO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "JOSSJO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "JOTIR"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(11), "JOSWIF"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "JOPIBA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "JOTPR"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(11, 0), "JOPLAF"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "JORVID"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "JOFMTB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "JOFMTC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "JOEME"));

    length = 300;
  }
}
