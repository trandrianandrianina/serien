/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx001.v1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v1.JsonArticleMagentoV1;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

public class ExportationArticleV1 extends BaseGroupDB {
  
  /**
   * Constructeur avec le queryManager en paramètre pour les requetes sur DB2
   */
  public ExportationArticleV1(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Retourner un article Série N sous forme GenericRecord : A AMELIORER AVEC LES CLASSES METIER COMMUNES DE STEPH
   */
  protected GenericRecord retournerUnArticleDB2(String etb, String codeArt) {
    if (etb == null || codeArt == null) {
      return null;
    }
    
    GenericRecord record = null;
    ArrayList<GenericRecord> listeArticles =
        queryManager.select("SELECT a.A1ART, a.A1ETB, DIGITS(A1GCD) AS GCD, A1UNV, A1NPU, A1LIB, A1LB1, A1LB2, A1LB3, A1PDS, A1LRG, A1LNG, "
            + "A1HTR, A1IN15, A1IN28, A1IN29, A1IN30, A1STK, A1REA, A1TOP1, A1ZP21, A1TOP2, A1TOP3, A1FRS, A1REF1, FRNOM " + " FROM "
            + queryManager.getLibrary() + ".PGVMARTM a " + " LEFT JOIN " + queryManager.getLibrary()
            + ".PGVMEAAM e ON e.A1ETB = a.A1ETB AND e.A1ART = a.A1ART " + " LEFT JOIN " + queryManager.getLibrary()
            + ".PGVMFRSM f ON f.FRETB = a.A1ETB AND f.FRFRS = a.A1FRS " + " WHERE a.A1ETB = '" + etb.trim() + "' AND a.A1ART = '"
            + codeArt.trim() + "' " + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
    
    if (listeArticles != null && listeArticles.size() == 1) {
      record = listeArticles.get(0);
    }
    else {
      majError("[ExportationArticleV1] retournerUnArticleDB2() - Problème de récupération de l'article " + etb + "/" + codeArt);
    }
    
    return record;
  }
  
  /**
   * Retourner Les 10 tarifs d'un article Série N ATTENTION SANS COEFFICIENT VU AVEC MARC le 27/04/16
   */
  public GenericRecord retournerLesTarifsUnArticle(String etb, String codeArt) {
    if (etb == null || codeArt == null) {
      return null;
    }
    // TODO Gestion des tarifs A FAIRE PLUS MIEUX DANS UNE CLASSE DE STEPH SERIEN METIER
    GenericRecord record = null;
    // On récupère le dernier tarif < à la date du jour
    ArrayList<GenericRecord> listeArticles =
        queryManager.select("" + " SELECT ATDAP, ATP01, ATP02 , ATP03, ATP04, ATP05, ATP06, ATP07, ATP08, ATP09, ATP10 " + " FROM "
            + queryManager.getLibrary() + ".PGVMTARM " + " WHERE ATETB = '" + etb.trim() + "' AND ATART = '" + codeArt.trim() + "' "
            + " AND ATDAP <=  (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) "
            + " CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) FROM SYSIBM.SYSDUMMY1) "
            + " ORDER BY ATDAP DESC " + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
    
    if (listeArticles != null) {
      if (listeArticles.size() > 0) {
        record = listeArticles.get(0);
      }
    }
    else {
      majError("[ExportationArticleV1] retournerLesTarifsUnArticle() - Problème de récupération des tarifs " + etb + "/" + codeArt);
    }
    
    return record;
  }
  
  /**
   * Retourner la DEEE d'un article
   */
  public String retournerDEEEarticle(String etb, String codeArt) {
    if ((etb == null) || (codeArt == null)) {
      return null;
    }
    
    String retour = null;
    // On récupère le dernier tarif < à la date du jour
    ArrayList<GenericRecord> listeArticles = queryManager.select("" + " SELECT ATP01,TENBR,A1UNV FROM " + queryManager.getLibrary() + ".PGVMTEEM "
        + " LEFT JOIN " + queryManager.getLibrary() + ".PGVMARTM ON TEETB = A1ETB AND TEARTT = A1ART " + " LEFT JOIN " + queryManager.getLibrary()
        + ".PGVMTARM ON ATETB = TEETB AND ATART = TEARTT " + " WHERE TEETB = '" + etb.trim() + "' AND TEARTD ='" + codeArt.trim() + "' "
        + " AND ATDAP <=  (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) FROM SYSIBM.SYSDUMMY1) "
        + " ORDER BY ATDAP DESC " + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
    
    if (listeArticles != null) {
      // Calcul de la DEEE
      if (listeArticles.size() > 0) {
        if (listeArticles.get(0).isPresentField("ATP01") && listeArticles.get(0).isPresentField("TENBR")
            && listeArticles.get(0).isPresentField("A1UNV")) {
          try {
            // gérer la 4 décimale
            if (listeArticles.get(0).getField("A1UNV") != null && listeArticles.get(0).getField("A1UNV").toString().equals("U*")) {
              retour =
                  (((BigDecimal) listeArticles.get(0).getField("TENBR")).multiply((BigDecimal) listeArticles.get(0).getField("ATP01"))
                      .divide(new BigDecimal(100))).setScale(4, RoundingMode.HALF_UP).toString();
            }
            else {
              retour =
                  (((BigDecimal) listeArticles.get(0).getField("TENBR")).multiply((BigDecimal) listeArticles.get(0).getField("ATP01")))
                      .setScale(4, RoundingMode.HALF_UP).toString();
            }
          }
          catch (Exception e) {
            majError("[ExportationArticleV1] retournerDEEEarticle() - Problème de récupération des tarifs " + etb + "/" + codeArt);
          }
        }
      }
    }
    else {
      majError("[ExportationArticleV1] retournerDEEEarticle() - Problème de récupération de la DEEE " + etb + "/" + codeArt);
    }
    
    return retour;
  }
  
  /**
   * Récupérer la référence fournisseur de cet article
   */
  /*private String recupererReferenceFournisseur(String etb, String codeArt) {
    if ((etb == null) || (codeArt == null)) {
      return null;
    }
    String retour = "";
    // TODO RAJOUTER L'exclusion des fabricant et des rférence dans la condition de la CNA sinon on va choper ces CNA
    ArrayList<GenericRecord> listeRef = querymg.select(""
        + " SELECT CAREF, CADAP "
        + " FROM " + querymg.getLibrary() + ".PGVMARTM a "
        + " LEFT JOIN " + querymg.getLibrary() + ".PGVMCNAM ON A1ETB = CAETB AND A1ART = CAART AND A1FRS = CAFRS AND A1COF = CACOL "
        + " WHERE A1ETB = '" + etb.trim() + "' AND A1ART = '" + codeArt.trim() + "' "
        + " AND CADAP <=  (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) "
        + "CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) FROM SYSIBM.SYSDUMMY1) "
        + " ORDER BY CADAP DESC "
        + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
    if (listeRef != null) {
      if (listeRef.size() > 0) {
        if (listeRef.get(0).isPresentField("CAREF") && !listeRef.get(0).getField("CAREF").toString().trim().equals("")) {
          retour = listeRef.get(0).getField("CAREF").toString().trim();
        }
      }
    }
    else {
  
      majError("[ExportationArticleV1] recupererReferenceFournisseur() - Problème de récupération fournisseur de " + etb + "/" + codeArt);
      return null;
    }
  
    return retour;
  }*/
  
  /**
   * Récupérer la référence fournisseur de cet article
   */
  protected String recupererReferenceFabricant(String etb, String codeArt) {
    if ((etb == null) || (codeArt == null)) {
      return null;
    }
    String retour = "";
    
    ArrayList<GenericRecord> listeRef = queryManager.select("" + " SELECT CARFC, CAREF, CADAP " + " FROM " + queryManager.getLibrary()
        + ".PGVMCNAM " + " WHERE CAETB = '" + etb.trim() + "' AND CAART = '" + codeArt.trim() + "' AND CAIN3 = 'F' "
        + " AND CADAP <=  (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) "
        + " CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) FROM SYSIBM.SYSDUMMY1) " + " ORDER BY CADAP DESC "
        + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
    if (listeRef != null) {
      if (listeRef.size() > 0) {
        if (listeRef.get(0).isPresentField("CARFC") && !listeRef.get(0).getField("CARFC").toString().trim().equals("")) {
          retour = listeRef.get(0).getField("CARFC").toString().trim();
        }
      }
    }
    else {
      
      majError("[ExportationArticleV1] recupererReferenceFabricant() - Problème de récupération fournisseur de " + etb + "/" + codeArt);
      return null;
    }
    
    return retour;
  }
  
  /**
   * Retourner un article Série N sous forme de message JSON
   */
  public String retournerUnArticleMagentoJSON(FluxMagento record) {
    if (record == null || record.getFLETB() == null || record.getFLCOD() == null) {
      majError("[ExportationArticleV1] retournerUnArticleMagentoJSON() - Valeurs du M_FluxMagento record corrompues ");
      return null;
    }
    
    String retour = null;
    
    GenericRecord unArticleDB2 = retournerUnArticleDB2(record.getFLETB(), record.getFLCOD());
    
    if (unArticleDB2 != null) {
      // On créé un article spécifique Magento
      JsonArticleMagentoV1 articleMag = new JsonArticleMagentoV1(record);
      Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), articleMag.getEtb());
      if (numeroInstance == null) {
        majError("[ExportArticles] retournerUnArticleMagentoJSON() : probleme d'interpretation de l'ETb vers l'instance Magento");
        return null;
      }
      articleMag.setIdInstanceMagento(numeroInstance);
      
      // Informations basiques de l'article
      if (!articleMag.mettreAJourArticle(unArticleDB2)) {
        majError(articleMag.getMsgError());
        return null;
      }
      
      // on bloque le flux si la variable non géré sur le web est positionné à 1
      if (articleMag.getExclutWeb() == null || articleMag.getExclutWeb().equals(JsonArticleMagentoV1.JAMAIS_VU_SUR_LE_WEB)) {
        majError("L'article n'est pas encoré géré sur le Web");
        return "";
      }
      
      // Informations annexes
      // Tarifs
      if (!articleMag.gererLesTarifs(retournerLesTarifsUnArticle(record.getFLETB(), record.getFLCOD()))) {
        // TODO pour le moment on ne le considère pas comme une erreur mais comme un avertissement d'initialisation
        // ON BLOQUE LE FLUX mais pas en erreur du coup pour alerter le gestionnaire on envoie du vide et non du NULL (erreur)
        majError(articleMag.getMsgError());
        return "";
      }
      // DEEE
      articleMag.calculerMontantDEEE(retournerDEEEarticle(record.getFLETB(), record.getFLCOD()));
      
      // Gestion de la mise à disposition et la visibilité de l'article
      // Si l'article n'est pas exclu du Web on envoie la liste de site Web sinon une liste vide
      // De cette manière on s'affranchit de l'envoi du flux ou non
      if (articleMag.getExclutWeb() != null && articleMag.getExclutWeb().equals(JsonArticleMagentoV1.SUR_LE_WEB)) {
        articleMag.gererVisibiliteDesSites(recupererSitesVisibles(record.getFLETB(), record.getFLCOD()));
      }
      else {
        articleMag.gererVisibiliteDesSites(null);
      }
      
      // référence fabricant:
      // String ref = recupererReferenceFournisseur(record.getFLETB(), record.getFLCOD());
      String ref = recupererReferenceFabricant(record.getFLETB(), record.getFLCOD());
      if (ref != null) {
        articleMag.setReferenceFabricant(ref);
      }
      
      // debut de la construction du JSON
      if (initJSON()) {
        if (articleMag.getCode() != null && articleMag.getIdInstanceMagento() > 0) {
          try {
            // On JSONE l'objet comme un sauvage
            retour = gson.toJson(articleMag);
          }
          catch (Exception e) {
            retour = null;
            majError("[ExportationArticleV1] retournerUnArticleMagentoJSON() - ERREUR JSON: " + e.getMessage());
          }
        }
        else {
          majError("[ExportationArticleV1] retournerUnArticleMagentoJSON() - ArticleMag corrompu ");
        }
      }
    }
    
    return retour;
  }
  
  /**
   * récupérer les sites Web sur lequels cet article est visible
   */
  protected ArrayList<GenericRecord> recupererSitesVisibles(String etb, String code) {
    if (etb == null || code == null) {
      return null;
    }
    
    ArrayList<GenericRecord> liste =
        queryManager.select("" + " SELECT SICOD FROM " + queryManager.getLibrary() + ".PSEMPSIM " + " WHERE SIETB = '" + etb + "' AND SICOD NOT IN "
            + " (SELECT SACOD FROM " + queryManager.getLibrary() + ".PSEMPSAM " + " WHERE SAETB = '" + etb + "' AND SAART = '" + code + "') ");
    
    if (liste == null) {
      majError("[ExportationArticleV1] recupererSitesVisibles() - Liste des sites NULL pour l'article " + etb + "/" + code);
    }
    
    return liste;
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
  
}
