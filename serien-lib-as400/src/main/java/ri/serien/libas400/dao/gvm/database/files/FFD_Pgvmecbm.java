/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

public abstract class FFD_Pgvmecbm extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_EBETB = 3;
  public static final int SIZE_EBCLI = 6;
  public static final int DECIMAL_EBCLI = 0;
  public static final int SIZE_EBLIV = 3;
  public static final int DECIMAL_EBLIV = 0;
  public static final int SIZE_EBTZP = 1;
  public static final int SIZE_EBZP1 = 30;
  public static final int SIZE_EBZP2 = 30;
  public static final int SIZE_EBZP3 = 30;
  public static final int SIZE_EBZP4 = 30;
  public static final int SIZE_EBZP5 = 30;
  public static final int SIZE_EBZP6 = 30;
  public static final int SIZE_EBZP7 = 30;
  public static final int SIZE_EBZP8 = 30;
  public static final int SIZE_EBZP9 = 30;
  public static final int SIZE_EBZP10 = 30;
  public static final int SIZE_EBZP11 = 30;
  public static final int SIZE_EBZP12 = 30;
  public static final int SIZE_EBZP13 = 30;
  public static final int SIZE_EBZP14 = 30;
  public static final int SIZE_EBZP15 = 30;
  public static final int SIZE_EBZP16 = 30;
  public static final int SIZE_EBZP17 = 30;
  public static final int SIZE_EBZP18 = 30;
  public static final int SIZE_EBPRP = 1;
  public static final int SIZE_EBREFC = 1;
  public static final int SIZE_EBVEH = 1;
  public static final int SIZE_EBCHAN = 1;
  public static final int SIZE_EBIN01 = 1;
  public static final int SIZE_EBIN02 = 1;
  public static final int SIZE_EBIN03 = 1;
  public static final int SIZE_EBIN04 = 1;
  public static final int SIZE_EBIN05 = 1;
  public static final int SIZE_EBIN06 = 1;
  public static final int SIZE_EBIN07 = 1;
  public static final int SIZE_EBIN08 = 1;
  public static final int SIZE_EBIN09 = 1;
  public static final int SIZE_EBIN10 = 1;
  public static final int SIZE_EBLI01 = 30;
  public static final int SIZE_EBLI02 = 30;
  public static final int SIZE_EBLI03 = 30;
  public static final int SIZE_EBLI04 = 30;
  public static final int SIZE_EBLI05 = 30;
  public static final int SIZE_EBMT01 = 9;
  public static final int DECIMAL_EBMT01 = 0;
  public static final int SIZE_EBMT02 = 9;
  public static final int DECIMAL_EBMT02 = 0;
  public static final int SIZE_EBMT03 = 9;
  public static final int DECIMAL_EBMT03 = 0;
  public static final int SIZE_EBMT04 = 9;
  public static final int DECIMAL_EBMT04 = 0;
  public static final int SIZE_EBMT05 = 9;
  public static final int DECIMAL_EBMT05 = 0;
  
  // Variables fichiers
  protected String EBETB = null; // Code Etablissement
  protected int EBCLI = 0; // Num. Client
  protected int EBLIV = 0; // Suf. Client
  protected char EBTZP = ' '; // TYPE DE ZP: "C"
  protected String EBZP1 = null; // Zone Perso. N01
  protected String EBZP2 = null; // Zone Perso. N02
  protected String EBZP3 = null; // Zone Perso. N03
  protected String EBZP4 = null; // Zone Perso. N04
  protected String EBZP5 = null; // Zone Perso. N05
  protected String EBZP6 = null; // Zone Perso. N06
  protected String EBZP7 = null; // Zone Perso. N07
  protected String EBZP8 = null; // Zone Perso. N08
  protected String EBZP9 = null; // Zone Perso. N09
  protected String EBZP10 = null; // Zone Perso. N10
  protected String EBZP11 = null; // Zone Perso. N11
  protected String EBZP12 = null; // Zone Perso. N12
  protected String EBZP13 = null; // Zone Perso. N13
  protected String EBZP14 = null; // Zone Perso. N14
  protected String EBZP15 = null; // Zone Perso. N15
  protected String EBZP16 = null; // Zone Perso. N16
  protected String EBZP17 = null; // Zone Perso. N17
  protected String EBZP18 = null; // Zone Perso. N18
  protected char EBPRP = ' '; // Pris par Obligatoire
  protected char EBREFC = ' '; // Ref.commande obligatoire
  protected char EBVEH = ' '; // Véhicule obligatoire
  protected char EBCHAN = ' '; // Chantier Obligatoire
  protected char EBIN01 = ' '; // Flux commande EDI
  protected char EBIN02 = ' '; // Flux expédition EDI
  protected char EBIN03 = ' '; // Flux facture EDI
  protected char EBIN04 = ' '; // Adr.facture / client payeur
  protected char EBIN05 = ' '; // Comptant,sinon en compte
  protected char EBIN06 = ' '; // Règlement comptant obligat.
  protected char EBIN07 = ' '; // Règlement chèque Interdit
  protected char EBIN08 = ' '; // Zone non utilisée
  protected char EBIN09 = ' '; // Zone non utilisée
  protected char EBIN10 = ' '; // Zone non utilisée
  protected String EBLI01 = null; // Zone non utilisée
  protected String EBLI02 = null; // Zone non utilisée
  protected String EBLI03 = null; // Zone non utilisée
  protected String EBLI04 = null; // Zone non utilisée
  protected String EBLI05 = null; // Zone non utilisée
  protected int EBMT01 = 0; // Zone non utilisée
  protected int EBMT02 = 0; // Zone non utilisée
  protected int EBMT03 = 0; // Zone non utilisée
  protected int EBMT04 = 0; // Zone non utilisée
  protected int EBMT05 = 0; // Zone non utilisée
  
  /**
   * Constructeur
   */
  public FFD_Pgvmecbm(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    EBETB = null;
    EBCLI = 0;
    EBLIV = 0;
    EBTZP = ' ';
    EBZP1 = null;
    EBZP2 = null;
    EBZP3 = null;
    EBZP4 = null;
    EBZP5 = null;
    EBZP6 = null;
    EBZP7 = null;
    EBZP8 = null;
    EBZP9 = null;
    EBZP10 = null;
    EBZP11 = null;
    EBZP12 = null;
    EBZP13 = null;
    EBZP14 = null;
    EBZP15 = null;
    EBZP16 = null;
    EBZP17 = null;
    EBZP18 = null;
    EBPRP = ' ';
    EBREFC = ' ';
    EBVEH = ' ';
    EBCHAN = ' ';
    EBIN01 = ' ';
    EBIN02 = ' ';
    EBIN03 = ' ';
    EBIN04 = ' ';
    EBIN05 = ' ';
    EBIN06 = ' ';
    EBIN07 = ' ';
    EBIN08 = ' ';
    EBIN09 = ' ';
    EBIN10 = ' ';
    EBLI01 = null;
    EBLI02 = null;
    EBLI03 = null;
    EBLI04 = null;
    EBLI05 = null;
    EBMT01 = 0;
    EBMT02 = 0;
    EBMT03 = 0;
    EBMT04 = 0;
    EBMT05 = 0;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  public String getEBETB() {
    return EBETB;
  }
  
  public void setEBETB(String eBETB) {
    EBETB = eBETB;
  }
  
  public int getEBCLI() {
    return EBCLI;
  }
  
  public void setEBCLI(int eBCLI) {
    EBCLI = eBCLI;
  }
  
  public int getEBLIV() {
    return EBLIV;
  }
  
  public void setEBLIV(int eBLIV) {
    EBLIV = eBLIV;
  }
  
  public char getEBTZP() {
    return EBTZP;
  }
  
  public void setEBTZP(char eBTZP) {
    EBTZP = eBTZP;
  }
  
  public String getEBZP1() {
    return EBZP1;
  }
  
  public void setEBZP1(String eBZP1) {
    EBZP1 = eBZP1;
  }
  
  public String getEBZP2() {
    return EBZP2;
  }
  
  public void setEBZP2(String eBZP2) {
    EBZP2 = eBZP2;
  }
  
  public String getEBZP3() {
    return EBZP3;
  }
  
  public void setEBZP3(String eBZP3) {
    EBZP3 = eBZP3;
  }
  
  public String getEBZP4() {
    return EBZP4;
  }
  
  public void setEBZP4(String eBZP4) {
    EBZP4 = eBZP4;
  }
  
  public String getEBZP5() {
    return EBZP5;
  }
  
  public void setEBZP5(String eBZP5) {
    EBZP5 = eBZP5;
  }
  
  public String getEBZP6() {
    return EBZP6;
  }
  
  public void setEBZP6(String eBZP6) {
    EBZP6 = eBZP6;
  }
  
  public String getEBZP7() {
    return EBZP7;
  }
  
  public void setEBZP7(String eBZP7) {
    EBZP7 = eBZP7;
  }
  
  public String getEBZP8() {
    return EBZP8;
  }
  
  public void setEBZP8(String eBZP8) {
    EBZP8 = eBZP8;
  }
  
  public String getEBZP9() {
    return EBZP9;
  }
  
  public void setEBZP9(String eBZP9) {
    EBZP9 = eBZP9;
  }
  
  public String getEBZP10() {
    return EBZP10;
  }
  
  public void setEBZP10(String eBZP10) {
    EBZP10 = eBZP10;
  }
  
  public String getEBZP11() {
    return EBZP11;
  }
  
  public void setEBZP11(String eBZP11) {
    EBZP11 = eBZP11;
  }
  
  public String getEBZP12() {
    return EBZP12;
  }
  
  public void setEBZP12(String eBZP12) {
    EBZP12 = eBZP12;
  }
  
  public String getEBZP13() {
    return EBZP13;
  }
  
  public void setEBZP13(String eBZP13) {
    EBZP13 = eBZP13;
  }
  
  public String getEBZP14() {
    return EBZP14;
  }
  
  public void setEBZP14(String eBZP14) {
    EBZP14 = eBZP14;
  }
  
  public String getEBZP15() {
    return EBZP15;
  }
  
  public void setEBZP15(String eBZP15) {
    EBZP15 = eBZP15;
  }
  
  public String getEBZP16() {
    return EBZP16;
  }
  
  public void setEBZP16(String eBZP16) {
    EBZP16 = eBZP16;
  }
  
  public String getEBZP17() {
    return EBZP17;
  }
  
  public void setEBZP17(String eBZP17) {
    EBZP17 = eBZP17;
  }
  
  public String getEBZP18() {
    return EBZP18;
  }
  
  public void setEBZP18(String eBZP18) {
    EBZP18 = eBZP18;
  }
  
  public char getEBPRP() {
    return EBPRP;
  }
  
  public void setEBPRP(char eBPRP) {
    EBPRP = eBPRP;
  }
  
  public char getEBREFC() {
    return EBREFC;
  }
  
  public void setEBREFC(char eBREFC) {
    EBREFC = eBREFC;
  }
  
  public char getEBVEH() {
    return EBVEH;
  }
  
  public void setEBVEH(char eBVEH) {
    EBVEH = eBVEH;
  }
  
  public char getEBCHAN() {
    return EBCHAN;
  }
  
  public void setEBCHAN(char eBCHAN) {
    EBCHAN = eBCHAN;
  }
  
  public char getEBIN01() {
    return EBIN01;
  }
  
  public void setEBIN01(char eBIN01) {
    EBIN01 = eBIN01;
  }
  
  public char getEBIN02() {
    return EBIN02;
  }
  
  public void setEBIN02(char eBIN02) {
    EBIN02 = eBIN02;
  }
  
  public char getEBIN03() {
    return EBIN03;
  }
  
  public void setEBIN03(char eBIN03) {
    EBIN03 = eBIN03;
  }
  
  public char getEBIN04() {
    return EBIN04;
  }
  
  public void setEBIN04(char eBIN04) {
    EBIN04 = eBIN04;
  }
  
  public char getEBIN05() {
    return EBIN05;
  }
  
  public void setEBIN05(char eBIN05) {
    EBIN05 = eBIN05;
  }
  
  public char getEBIN06() {
    return EBIN06;
  }
  
  public void setEBIN06(char eBIN06) {
    EBIN06 = eBIN06;
  }
  
  public char getEBIN07() {
    return EBIN07;
  }
  
  public void setEBIN07(char eBIN07) {
    EBIN07 = eBIN07;
  }
  
  public char getEBIN08() {
    return EBIN08;
  }
  
  public void setEBIN08(char eBIN08) {
    EBIN08 = eBIN08;
  }
  
  public char getEBIN09() {
    return EBIN09;
  }
  
  public void setEBIN09(char eBIN09) {
    EBIN09 = eBIN09;
  }
  
  public char getEBIN10() {
    return EBIN10;
  }
  
  public void setEBIN10(char eBIN10) {
    EBIN10 = eBIN10;
  }
  
  public String getEBLI01() {
    return EBLI01;
  }
  
  public void setEBLI01(String eBLI01) {
    EBLI01 = eBLI01;
  }
  
  public String getEBLI02() {
    return EBLI02;
  }
  
  public void setEBLI02(String eBLI02) {
    EBLI02 = eBLI02;
  }
  
  public String getEBLI03() {
    return EBLI03;
  }
  
  public void setEBLI03(String eBLI03) {
    EBLI03 = eBLI03;
  }
  
  public String getEBLI04() {
    return EBLI04;
  }
  
  public void setEBLI04(String eBLI04) {
    EBLI04 = eBLI04;
  }
  
  public String getEBLI05() {
    return EBLI05;
  }
  
  public void setEBLI05(String eBLI05) {
    EBLI05 = eBLI05;
  }
  
  public int getEBMT01() {
    return EBMT01;
  }
  
  public void setEBMT01(int eBMT01) {
    EBMT01 = eBMT01;
  }
  
  public int getEBMT02() {
    return EBMT02;
  }
  
  public void setEBMT02(int eBMT02) {
    EBMT02 = eBMT02;
  }
  
  public int getEBMT03() {
    return EBMT03;
  }
  
  public void setEBMT03(int eBMT03) {
    EBMT03 = eBMT03;
  }
  
  public int getEBMT04() {
    return EBMT04;
  }
  
  public void setEBMT04(int eBMT04) {
    EBMT04 = eBMT04;
  }
  
  public int getEBMT05() {
    return EBMT05;
  }
  
  public void setEBMT05(int eBMT05) {
    EBMT05 = eBMT05;
  }
}
