/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0014i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINFA = 7;
  public static final int DECIMAL_PINFA = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PILIG = 3;
  public static final int DECIMAL_PILIG = 0;
  public static final int SIZE_PIPAG = 3;
  public static final int DECIMAL_PIPAG = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 39;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PICOD = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINFA = 5;
  public static final int VAR_PINLI = 6;
  public static final int VAR_PILIG = 7;
  public static final int VAR_PIPAG = 8;
  public static final int VAR_PIARR = 9;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private String picod = ""; // Code ERL "D","E",X","9"
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe
  private BigDecimal pinfa = BigDecimal.ZERO; // Numéro de facture
  private BigDecimal pinli = BigDecimal.ZERO; // N°de ligne si unique
  private BigDecimal pilig = BigDecimal.ZERO; // Nombre de ligne par page
  private BigDecimal pipag = BigDecimal.ZERO; // Numéro de la page
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9"
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe
      new AS400ZonedDecimal(SIZE_PINFA, DECIMAL_PINFA), // Numéro de facture
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // N°de ligne si unique
      new AS400ZonedDecimal(SIZE_PILIG, DECIMAL_PILIG), // Nombre de ligne par page
      new AS400ZonedDecimal(SIZE_PIPAG, DECIMAL_PIPAG), // Numéro de la page
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pietb, picod, pinum, pisuf, pinfa, pinli, pilig, pipag, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    picod = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinfa = (BigDecimal) output[5];
    pinli = (BigDecimal) output[6];
    pilig = (BigDecimal) output[7];
    pipag = (BigDecimal) output[8];
    piarr = (String) output[9];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinfa(BigDecimal pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = pPinfa.setScale(DECIMAL_PINFA, RoundingMode.HALF_UP);
  }
  
  public void setPinfa(Integer pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(pPinfa);
  }
  
  public void setPinfa(Date pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pPinfa));
  }
  
  public Integer getPinfa() {
    return pinfa.intValue();
  }
  
  public Date getPinfaConvertiEnDate() {
    return ConvertDate.db2ToDate(pinfa.intValue(), null);
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPilig(BigDecimal pPilig) {
    if (pPilig == null) {
      return;
    }
    pilig = pPilig.setScale(DECIMAL_PILIG, RoundingMode.HALF_UP);
  }
  
  public void setPilig(Integer pPilig) {
    if (pPilig == null) {
      return;
    }
    pilig = BigDecimal.valueOf(pPilig);
  }
  
  public Integer getPilig() {
    return pilig.intValue();
  }
  
  public void setPipag(BigDecimal pPipag) {
    if (pPipag == null) {
      return;
    }
    pipag = pPipag.setScale(DECIMAL_PIPAG, RoundingMode.HALF_UP);
  }
  
  public void setPipag(Integer pPipag) {
    if (pPipag == null) {
      return;
    }
    pipag = BigDecimal.valueOf(pPipag);
  }
  
  public Integer getPipag() {
    return pipag.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
