/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.devise;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.devise.CriteresRechercheDevise;
import ri.serien.libcommun.gescom.commun.devise.Devise;
import ri.serien.libcommun.gescom.commun.devise.IdDevise;
import ri.serien.libcommun.gescom.commun.devise.ListeDevise;

public class SqlDevise {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlDevise(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  /**
   * Retourner la liste des devise pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   */
  public ListeDevise chargerListeDevise(CriteresRechercheDevise pDevise) {
    if (pDevise == null) {
      return null;
    }
    
    String requete = "Select DVDEV,DVLIB from " + querymg.getLibrary() + '.' + EnumTableBDD.DEVISE + " ORDER BY DVLIB";
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On alimente la liste avec les enregistrements trouvés
    ListeDevise listeDevise = new ListeDevise();
    
    for (GenericRecord record : listeRecord) {
      if (record != null) {
        listeDevise.add(initDevise(record));
      }
    }
    return listeDevise;
  }
  
  /**
   * Initialise une devise à partir de l'enregistrement donnée et du type de la recherche.
   */
  private Devise initDevise(GenericRecord pRecord) {
    Devise devise = null;
    IdDevise idDevise = IdDevise.getInstance(pRecord.getStringValue("DVDEV"));
    devise = new Devise(idDevise);
    devise.setLibelle((pRecord.getStringValue("DVLIB")));
    
    return devise;
  }
}
