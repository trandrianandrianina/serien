/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux;

import java.util.ArrayList;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.notification.ListeDestinataireNotification;
import ri.serien.libcommun.outils.Trace;

public class ParametresFluxBibli {
  // Constantes
  public static final String PM_EDI_MODE_TEST = "EDI_MODE_TEST";
  
  // Variables d'environnement
  private Bibliotheque baseDeDonnees = null;
  private String lettreEnvironnement = null;
  private String versionFlux = "1.0";
  // Variables de librairie
  private boolean isModeTest = true;
  private boolean isModeDebug = false;
  private String bibForcee = null;
  private int nbFluxMaxBoucle = 10;
  private int seuilEchecAlerte = 10;
  private int nbTentativesMax = 1000;
  private int nbJoursMaxFlux = 30;
  private String magasinMagento = null;
  private String vendeurMagento = null;
  private String categorieParticulierMg = null;
  private String categorieArtisanMg = null;
  private String reglementParticulierMg = null;
  private String modeFacturationClient = null;
  private String modeLivraisonMagento = null;
  private String urlWebServiceMagento = null;
  private String loginWsMagento = null;
  private String passwordWsMagento = null;
  private String racineIFSeditions = null;
  private String cheminIFSfactures = null;
  private String prefixeNomFactures = null;
  private String mailDestinataireErreurs = null;
  private ListeDestinataireNotification listeDestinataireNotification = null;
  
  private String destinataire_mail_EDI = null;
  private String url_ftp_EDI = null;
  private String login_ftp_EDI = null;
  private String password_ftp_EDI = null;
  private String vendeur_EDI = null;
  private String enlevement_EDI = null;
  private String dossierCdes_ftp_EDI = null;
  private String dossierExpe_ftp_EDI = null;
  private String dossierFact_ftp_EDI = null;
  private String dossier_local_tmp_EDI = null;
  private String dossier_local_cdes_EDI = null;
  private String dossier_local_expe_EDI = null;
  private String dossier_local_fact_EDI = null;
  private String dossier_local_arch_EDI = null;
  private String dossier_local_qtaine_EDI = null;
  private String prefixe_orders_EDI = null;
  private String prefixe_desadv_EDI = null;
  private String prefixe_invoic_EDI = null;
  private String EAN_founisseur_EDI = null;
  private String lib_founisseur_EDI = null;
  private String EAN_factor_EDI = null;
  private String lib_factor_EDI = null;
  private String unite_vente_EDI = null;
  private String mode_test_EDI = null;
  
  private static Bibliotheque bibliothequeEnvironnement = null;
  
  /**
   * Constructeur.
   */
  public ParametresFluxBibli(ArrayList<ParametreFlux> pListe) {
    if (pListe == null) {
      return;
    }
    
    for (ParametreFlux parametre : pListe) {
      if (parametre.getPM_CLE() != null) {
        if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_TEST_WEB_SERVICES)) {
          if (parametre.getPM_VALEUR() != null) {
            isModeTest = parametre.getPM_VALEUR().trim().equals("1");
          }
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_MODE_DEBUG)) {
          if (parametre.getPM_VALEUR() != null) {
            isModeDebug = parametre.getPM_VALEUR().trim().equals("1");
          }
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_BIB_FORCEE_WS)) {
          bibForcee = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_NB_FLUX_MAX_PAR_BOUCLE)) {
          if (parametre.getPM_VALEUR() != null) {
            try {
              nbFluxMaxBoucle = Integer.parseInt(parametre.getPM_VALEUR());
            }
            catch (NumberFormatException e) {
            }
          }
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_SEUIL_ALERTE_ENVOI)) {
          if (parametre.getPM_VALEUR() != null) {
            try {
              seuilEchecAlerte = Integer.parseInt(parametre.getPM_VALEUR());
            }
            catch (NumberFormatException e) {
            }
          }
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_TENTATIVES_MAX_ENVOI)) {
          if (parametre.getPM_VALEUR() != null) {
            try {
              nbTentativesMax = Integer.parseInt(parametre.getPM_VALEUR());
            }
            catch (NumberFormatException e) {
            }
          }
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_NBJOUR_FLUX_MAX)) {
          if (parametre.getPM_VALEUR() != null) {
            try {
              nbJoursMaxFlux = Integer.parseInt(parametre.getPM_VALEUR());
            }
            catch (NumberFormatException e) {
            }
          }
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_MAGASIN_GENERAL)) {
          magasinMagento = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_VENDEUR_MAGENTO)) {
          vendeurMagento = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_CATEGORIE_PARTICULIER)) {
          categorieParticulierMg = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_CATEGORIE_ARTISAN)) {
          categorieArtisanMg = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_REGLEMENT_PARTICULIER)) {
          reglementParticulierMg = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_FACTU_NORMALE_CLIENT)) {
          modeFacturationClient = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_LIVRAISON_MAGENTO)) {
          modeLivraisonMagento = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_URL_WS_MAGENTO)) {
          urlWebServiceMagento = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_LOGIN_WS_MAGENTO)) {
          loginWsMagento = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_MP_WS_MAGENTO)) {
          passwordWsMagento = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_EDITIONS_RACINE)) {
          racineIFSeditions = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_EDITIONS_CHEMIN_FAC)) {
          cheminIFSfactures = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_PREFIXE_NOM_FAC)) {
          prefixeNomFactures = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DESTINATAIRE_MAIL_EDI)) {
          destinataire_mail_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_MAIL_DESTINATAIRE_ERREURS)) {
          mailDestinataireErreurs = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_NOTIF_DESTINATAIRE_ERREURS)) {
          chargerListeDestinataireNotification(parametre.getPM_VALEUR());
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_URL_EDI_FTP)) {
          url_ftp_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_LOGIN_EDI_FTP)) {
          login_ftp_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_MP_EDI_FTP)) {
          password_ftp_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_VENDEUR_EDI)) {
          vendeur_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_ENLEVEMENT_EDI)) {
          enlevement_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DOSSIER_CDES_EDI_FTP)) {
          dossierCdes_ftp_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DOSSIER_EXPE_EDI_FTP)) {
          dossierExpe_ftp_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DOSSIER_FACT_EDI_FTP)) {
          dossierFact_ftp_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DOSSIER_LOCAL_TMP_EDI)) {
          dossier_local_tmp_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DOSSIER_LOCAL_CDES_EDI)) {
          dossier_local_cdes_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DOSSIER_LOCAL_EXPE_EDI)) {
          dossier_local_expe_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DOSSIER_LOCAL_FACT_EDI)) {
          dossier_local_fact_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DOSSIER_LOCAL_ARCH_EDI)) {
          dossier_local_arch_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_DOSSIER_LOCAL_QTAINE_EDI)) {
          dossier_local_qtaine_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_PREFIXE_EDI_ORDERS)) {
          prefixe_orders_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_PREFIXE_EDI_DESADV)) {
          prefixe_desadv_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_PREFIXE_EDI_INVOIC)) {
          prefixe_invoic_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_EAN_FOURNISSEUR_EDI)) {
          EAN_founisseur_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_LIB_FOURNISSEUR_EDI)) {
          lib_founisseur_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_EDI_EAN_FACTOR)) {
          EAN_factor_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_EDI_LIB_FACTOR)) {
          lib_factor_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_EDI_UNITE_VENTE)) {
          unite_vente_EDI = parametre.getPM_VALEUR();
        }
        else if (parametre.getPM_CLE().equals(ParametresFluxInit.PM_EDI_MODE_TEST)) {
          mode_test_EDI = parametre.getPM_VALEUR();
        }
      }
    }
  }
  
  /**
   * Charge la liste des destinataires des notifications erreurs.
   */
  private void chargerListeDestinataireNotification(String pValeur) {
    try {
      listeDestinataireNotification = ListeDestinataireNotification.convertirChaineEnListe(pValeur);
    }
    catch (Exception e) {
      Trace.erreur("Erreur lors du chargement des destinataires des notifications depuis la table des paramètres.");
      listeDestinataireNotification = null;
    }
  }
  
  // -- Accesseurs
  
  public boolean isModeTest() {
    return isModeTest;
  }
  
  public boolean isModeDebug() {
    return isModeDebug;
  }
  
  public String getBibForcee() {
    return bibForcee;
  }
  
  public int getNbFluxMaxBoucle() {
    return nbFluxMaxBoucle;
  }
  
  public int getSeuilEchecAlerte() {
    return seuilEchecAlerte;
  }
  
  public int getNbTentativesMax() {
    return nbTentativesMax;
  }
  
  public int getNbJoursMaxFlux() {
    return nbJoursMaxFlux;
  }
  
  public String getMagasinMagento() {
    return magasinMagento;
  }
  
  public String getVendeurMagento() {
    return vendeurMagento;
  }
  
  public String getCategorieParticulierMg() {
    return categorieParticulierMg;
  }
  
  public String getCategorieArtisanMg() {
    return categorieArtisanMg;
  }
  
  public String getReglementParticulierMg() {
    return reglementParticulierMg;
  }
  
  public String getModeFacturationClient() {
    return modeFacturationClient;
  }
  
  public String getModeLivraisonMagento() {
    return modeLivraisonMagento;
  }
  
  public String getUrlWebServiceMagento() {
    return urlWebServiceMagento;
  }
  
  public String getLoginWsMagento() {
    return loginWsMagento;
  }
  
  public String getPasswordWsMagento() {
    return passwordWsMagento;
  }
  
  public String getRacineIFSeditions() {
    return racineIFSeditions;
  }
  
  public String getCheminIFSfactures() {
    return cheminIFSfactures;
  }
  
  public String getPrefixeNomFactures() {
    return prefixeNomFactures;
  }
  
  public Bibliotheque getLibrairie() {
    return baseDeDonnees;
  }
  
  public void setLibrairie(Bibliotheque pBaseDeDonnees) {
    this.baseDeDonnees = pBaseDeDonnees;
  }
  
  public String getLettreEnvironnement() {
    return lettreEnvironnement;
  }
  
  public void setLettreEnvironnement(String lettreEnvironnement) {
    this.lettreEnvironnement = lettreEnvironnement;
  }
  
  public String getVersionFlux() {
    return versionFlux;
  }
  
  public void setVersionFlux(String versionFlux) {
    this.versionFlux = versionFlux;
  }
  
  public String getDestinataire_mail_EDI() {
    return destinataire_mail_EDI;
  }
  
  public String getMailDestinataireErreurs() {
    return mailDestinataireErreurs;
  }
  
  public ListeDestinataireNotification getListeDestinataireNotification() {
    return listeDestinataireNotification;
  }
  
  public String getUrl_ftp_EDI() {
    return url_ftp_EDI;
  }
  
  public String getLogin_ftp_EDI() {
    return login_ftp_EDI;
  }
  
  public String getPassword_ftp_EDI() {
    return password_ftp_EDI;
  }
  
  public String getVendeur_EDI() {
    return vendeur_EDI;
  }
  
  public String getEnlevement_EDI() {
    return enlevement_EDI;
  }
  
  public String getDossierCdes_ftp_EDI() {
    return dossierCdes_ftp_EDI;
  }
  
  public String getDossierExpe_ftp_EDI() {
    return dossierExpe_ftp_EDI;
  }
  
  public String getDossierFact_ftp_EDI() {
    return dossierFact_ftp_EDI;
  }
  
  public String getDossier_local_tmp_EDI() {
    return dossier_local_tmp_EDI;
  }
  
  public String getDossier_local_cdes_EDI() {
    return dossier_local_cdes_EDI;
  }
  
  public String getDossier_local_expe_EDI() {
    return dossier_local_expe_EDI;
  }
  
  public String getDossier_local_fact_EDI() {
    return dossier_local_fact_EDI;
  }
  
  public String getDossier_local_arch_EDI() {
    return dossier_local_arch_EDI;
  }
  
  public String getDossier_local_qtaine_EDI() {
    return dossier_local_qtaine_EDI;
  }
  
  public String getPrefixe_orders_EDI() {
    return prefixe_orders_EDI;
  }
  
  public String getPrefixe_desadv_EDI() {
    return prefixe_desadv_EDI;
  }
  
  public String getPrefixe_invoic_EDI() {
    return prefixe_invoic_EDI;
  }
  
  public String getEAN_founisseur_EDI() {
    return EAN_founisseur_EDI;
  }
  
  public String getLib_founisseur_EDI() {
    return lib_founisseur_EDI;
  }
  
  public String getEAN_factor_EDI() {
    return EAN_factor_EDI;
  }
  
  public String getLib_factor_EDI() {
    return lib_factor_EDI;
  }
  
  public String getUnite_vente_EDI() {
    return unite_vente_EDI;
  }
  
  public String getMode_test_EDI() {
    return mode_test_EDI;
  }
  
  public static Bibliotheque getBibliothequeEnvironnement() {
    return bibliothequeEnvironnement;
  }
  
  public static void setBibliothequeEnvironnement(Bibliotheque bibliothequeEnvironnement) {
    ParametresFluxBibli.bibliothequeEnvironnement = bibliothequeEnvironnement;
  }
  
}
