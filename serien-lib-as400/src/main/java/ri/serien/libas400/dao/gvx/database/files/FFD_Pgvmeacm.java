/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

public abstract class FFD_Pgvmeacm extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_EBETB = 3;
  public static final int SIZE_EBART = 20;
  public static final int SIZE_EBTZP = 1;
  public static final int SIZE_EBZP1 = 30;
  public static final int SIZE_EBZP2 = 30;
  public static final int SIZE_EBZP3 = 30;
  public static final int SIZE_EBZP4 = 30;
  public static final int SIZE_EBZP5 = 30;
  public static final int SIZE_EBZP6 = 30;
  public static final int SIZE_EBZP7 = 30;
  public static final int SIZE_EBZP8 = 30;
  public static final int SIZE_EBZP9 = 30;
  public static final int SIZE_EBZP10 = 30;
  public static final int SIZE_EBZP11 = 30;
  public static final int SIZE_EBZP12 = 30;
  public static final int SIZE_EBZP13 = 30;
  public static final int SIZE_EBZP14 = 30;
  public static final int SIZE_EBZP15 = 30;
  public static final int SIZE_EBZP16 = 30;
  public static final int SIZE_EBZP17 = 30;
  public static final int SIZE_EBZP18 = 30;
  
  // Variables fichiers
  protected String EBETB = null; // Code Etablissement
  protected String EBART = null; // Code Article
  protected char EBTZP = ' '; // TYPE DE ZP: N (1 À 9)
  protected String EBZP1 = null; // Zone Perso. N01
  protected String EBZP2 = null; // Zone Perso. N02
  protected String EBZP3 = null; // Zone Perso. N03
  protected String EBZP4 = null; // Zone Perso. N04
  protected String EBZP5 = null; // Zone Perso. N05
  protected String EBZP6 = null; // Zone Perso. N06
  protected String EBZP7 = null; // Zone Perso. N07
  protected String EBZP8 = null; // Zone Perso. N08
  protected String EBZP9 = null; // Zone Perso. N09
  protected String EBZP10 = null; // Zone Perso. N10
  protected String EBZP11 = null; // Zone Perso. N11
  protected String EBZP12 = null; // Zone Perso. N12
  protected String EBZP13 = null; // Zone Perso. N13
  protected String EBZP14 = null; // Zone Perso. N14
  protected String EBZP15 = null; // Zone Perso. N15
  protected String EBZP16 = null; // Zone Perso. N16
  protected String EBZP17 = null; // Zone Perso. N17
  protected String EBZP18 = null; // Zone Perso. N18
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public FFD_Pgvmeacm(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    EBETB = null;
    EBART = null;
    EBTZP = ' ';
    EBZP1 = null;
    EBZP2 = null;
    EBZP3 = null;
    EBZP4 = null;
    EBZP5 = null;
    EBZP6 = null;
    EBZP7 = null;
    EBZP8 = null;
    EBZP9 = null;
    EBZP10 = null;
    EBZP11 = null;
    EBZP12 = null;
    EBZP13 = null;
    EBZP14 = null;
    EBZP15 = null;
    EBZP16 = null;
    EBZP17 = null;
    EBZP18 = null;
  }
  
  /**
   * @return le eBETB
   */
  public String getEBETB() {
    return EBETB;
  }
  
  /**
   * @param eBETB le eBETB à définir
   */
  public void setEBETB(String eBETB) {
    EBETB = eBETB;
  }
  
  /**
   * @return le eBART
   */
  public String getEBART() {
    return EBART;
  }
  
  /**
   * @param eBART le eBART à définir
   */
  public void setEBART(String eBART) {
    EBART = eBART;
  }
  
  /**
   * @return le eBTZP
   */
  public char getEBTZP() {
    return EBTZP;
  }
  
  /**
   * @param eBTZP le eBTZP à définir
   */
  public void setEBTZP(char eBTZP) {
    EBTZP = eBTZP;
  }
  
  /**
   * @return le eBZP1
   */
  public String getEBZP1() {
    return EBZP1;
  }
  
  /**
   * @param eBZP1 le eBZP1 à définir
   */
  public void setEBZP1(String eBZP1) {
    EBZP1 = eBZP1;
  }
  
  /**
   * @return le eBZP2
   */
  public String getEBZP2() {
    return EBZP2;
  }
  
  /**
   * @param eBZP2 le eBZP2 à définir
   */
  public void setEBZP2(String eBZP2) {
    EBZP2 = eBZP2;
  }
  
  /**
   * @return le eBZP3
   */
  public String getEBZP3() {
    return EBZP3;
  }
  
  /**
   * @param eBZP3 le eBZP3 à définir
   */
  public void setEBZP3(String eBZP3) {
    EBZP3 = eBZP3;
  }
  
  /**
   * @return le eBZP4
   */
  public String getEBZP4() {
    return EBZP4;
  }
  
  /**
   * @param eBZP4 le eBZP4 à définir
   */
  public void setEBZP4(String eBZP4) {
    EBZP4 = eBZP4;
  }
  
  /**
   * @return le eBZP5
   */
  public String getEBZP5() {
    return EBZP5;
  }
  
  /**
   * @param eBZP5 le eBZP5 à définir
   */
  public void setEBZP5(String eBZP5) {
    EBZP5 = eBZP5;
  }
  
  /**
   * @return le eBZP6
   */
  public String getEBZP6() {
    return EBZP6;
  }
  
  /**
   * @param eBZP6 le eBZP6 à définir
   */
  public void setEBZP6(String eBZP6) {
    EBZP6 = eBZP6;
  }
  
  /**
   * @return le eBZP7
   */
  public String getEBZP7() {
    return EBZP7;
  }
  
  /**
   * @param eBZP7 le eBZP7 à définir
   */
  public void setEBZP7(String eBZP7) {
    EBZP7 = eBZP7;
  }
  
  /**
   * @return le eBZP8
   */
  public String getEBZP8() {
    return EBZP8;
  }
  
  /**
   * @param eBZP8 le eBZP8 à définir
   */
  public void setEBZP8(String eBZP8) {
    EBZP8 = eBZP8;
  }
  
  /**
   * @return le eBZP9
   */
  public String getEBZP9() {
    return EBZP9;
  }
  
  /**
   * @param eBZP9 le eBZP9 à définir
   */
  public void setEBZP9(String eBZP9) {
    EBZP9 = eBZP9;
  }
  
  /**
   * @return le eBZP10
   */
  public String getEBZP10() {
    return EBZP10;
  }
  
  /**
   * @param eBZP10 le eBZP10 à définir
   */
  public void setEBZP10(String eBZP10) {
    EBZP10 = eBZP10;
  }
  
  /**
   * @return le eBZP11
   */
  public String getEBZP11() {
    return EBZP11;
  }
  
  /**
   * @param eBZP11 le eBZP11 à définir
   */
  public void setEBZP11(String eBZP11) {
    EBZP11 = eBZP11;
  }
  
  /**
   * @return le eBZP12
   */
  public String getEBZP12() {
    return EBZP12;
  }
  
  /**
   * @param eBZP12 le eBZP12 à définir
   */
  public void setEBZP12(String eBZP12) {
    EBZP12 = eBZP12;
  }
  
  /**
   * @return le eBZP13
   */
  public String getEBZP13() {
    return EBZP13;
  }
  
  /**
   * @param eBZP13 le eBZP13 à définir
   */
  public void setEBZP13(String eBZP13) {
    EBZP13 = eBZP13;
  }
  
  /**
   * @return le eBZP14
   */
  public String getEBZP14() {
    return EBZP14;
  }
  
  /**
   * @param eBZP14 le eBZP14 à définir
   */
  public void setEBZP14(String eBZP14) {
    EBZP14 = eBZP14;
  }
  
  /**
   * @return le eBZP15
   */
  public String getEBZP15() {
    return EBZP15;
  }
  
  /**
   * @param eBZP15 le eBZP15 à définir
   */
  public void setEBZP15(String eBZP15) {
    EBZP15 = eBZP15;
  }
  
  /**
   * @return le eBZP16
   */
  public String getEBZP16() {
    return EBZP16;
  }
  
  /**
   * @param eBZP16 le eBZP16 à définir
   */
  public void setEBZP16(String eBZP16) {
    EBZP16 = eBZP16;
  }
  
  /**
   * @return le eBZP17
   */
  public String getEBZP17() {
    return EBZP17;
  }
  
  /**
   * @param eBZP17 le eBZP17 à définir
   */
  public void setEBZP17(String eBZP17) {
    EBZP17 = eBZP17;
  }
  
  /**
   * @return le eBZP18
   */
  public String getEBZP18() {
    return EBZP18;
  }
  
  /**
   * @param eBZP18 le eBZP18 à définir
   */
  public void setEBZP18(String eBZP18) {
    EBZP18 = eBZP18;
  }
}
