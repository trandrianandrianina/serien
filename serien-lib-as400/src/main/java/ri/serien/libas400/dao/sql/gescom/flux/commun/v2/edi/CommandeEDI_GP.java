/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.BlocCommentairesV2;

public class CommandeEDI_GP extends EntiteEDI {
  // CONSTANTES
  // lignes d'entête
  private final String DECLA_ENT = "ENT";
  // ligne de dates
  private final String DECLA_DTM = "DTM";
  // lignes partenaires
  private final String DECLA_PAR = "PAR";
  // partenaire fournisseur
  private final String DECLA_PAR_SU = "SU";
  // partenaire client acheteur
  private final String DECLA_PAR_BY = "BY";
  // partenaire client facturé
  private final String DECLA_PAR_IV = "IV";
  // partenaire client livré
  private final String DECLA_PAR_DP = "DP";
  // commentaires
  private final String DECLA_COM = "COM";
  // lignes articles
  private final String DECLA_LIG = "LIG";
  
  // ENTETE
  private String id_entete_1 = null;
  private String type_message_2 = null;
  private String numero_document_3 = null;
  private String date_document_4 = null;
  private String heure_document_5 = null;
  private String code_monnaie_6 = null;
  private String date_livraison_7 = null;
  private String heure_livraison_8 = null;
  
  // DATES
  private String id_dates_cdes_1 = null;
  private String type_date_cdes_2 = null;
  private String date_cdes_3 = null;
  // PARTENAIRES
  private Partenaire_EDI clientAcheteur = null;
  private Partenaire_EDI clientLivre = null;
  private Partenaire_EDI clientFacture = null;
  private Partenaire_EDI fournisseur = null;
  // Commentaires
  private String typeCommentaires = "";
  private String commentaires = "";
  private ArrayList<String> blocCommentaires = null;
  
  private ArrayList<Ligne_Cde_EDI_GP> lignesArticles = null;
  
  private transient String fichierOrigine = null;
  
  /**
   * Constructeur d'une commande EDI sur la base du contenu d'un fichier
   */
  public CommandeEDI_GP(ParametresFluxBibli pParams, String contenuFichier, String monEtb, String nomFichier) {
    super(pParams);
    fichierOrigine = nomFichier;
    contenuBrut = contenuFichier;
    ETB = monEtb;
    
    if (contenuBrut == null || ETB == null) {
      return;
    }
    
    isValide = traitementContenu();
  }
  
  /**
   * traitement du contenu de la commande
   */
  @Override
  protected boolean traitementContenu() {
    if (contenuBrut == null) {
      majError("(CommandeEDI_GP] " + fichierOrigine + " traitementContenu() contenuBrut NULL");
      return false;
    }
    
    boolean retour = true;
    
    // On decoupe en lignes d'enregistremeents avant tout
    if (contenuBrut.length() > 0) {
      tabDonnees = contenuBrut.split(SEPARATEUR_LIGNE);
    }
    // OK pour le découpage physique
    if (tabDonnees != null) {
      lignesArticles = new ArrayList<Ligne_Cde_EDI_GP>();
      int i = 0;
      while (i < tabDonnees.length && retour) {
        // Fichier @GP
        if (tabDonnees[i].startsWith(DECLA_GP)) {
          retour = majDesInfosDeFichier(tabDonnees[i]);
        }
        // Entête de commande
        if (tabDonnees[i].startsWith(DECLA_ENT)) {
          retour = majDesInfosEntete(tabDonnees[i]);
        }
        // Dates de la commande
        if (tabDonnees[i].startsWith(DECLA_DTM)) {
          retour = majDesInfosDates(tabDonnees[i]);
        }
        
        // intervenants de la commande
        if (tabDonnees[i].startsWith(DECLA_PAR)) {
          retour = majDesInfosPartenaires(tabDonnees[i]);
        }
        
        // intervenants de la commande
        if (tabDonnees[i].startsWith(DECLA_COM)) {
          retour = majDesCommentaires(tabDonnees[i]);
        }
        
        // Lignes d'articles
        if (tabDonnees[i].startsWith(DECLA_LIG)) {
          Ligne_Cde_EDI_GP ligne = new Ligne_Cde_EDI_GP(parametresFlux, tabDonnees[i]);
          if (ligne.isValide) {
            lignesArticles.add(ligne);
          }
          else {
            majError(ligne.getMsgError());
            retour = false;
          }
        }
        i++;
      }
    }
    else {
      majError("[CommandeEDI_GP] " + fichierOrigine + " traitementContenu() tabDonnees à NULL");
    }
    
    return retour;
  }
  
  /**
   * mettre à jour les infos de l'enête de commande sur la base de la ligne ENT
   */
  private boolean majDesInfosEntete(String ligne) {
    if (ligne == null) {
      majError("(CommandeEDI_GP] " + fichierOrigine + " majDesInfosEntete() ligne NULL");
      return false;
    }
    
    boolean retour = false;
    
    String[] tabTemp = decoupageDeLigne(ligne);
    
    if (tabTemp != null) {
      int compteur = 0;
      for (int i = 0; i < tabTemp.length; i++) {
        switch (i) {
          case 0:
            id_entete_1 = tabTemp[i];
            compteur++;
            break;
          case 1:
            type_message_2 = tabTemp[i];
            compteur++;
            break;
          case 2:
            numero_document_3 = tabTemp[i];
            compteur++;
            break;
          case 3:
            date_document_4 = tabTemp[i];
            compteur++;
            break;
          case 4:
            heure_document_5 = tabTemp[i];
            compteur++;
            break;
          case 5:
            code_monnaie_6 = tabTemp[i];
            compteur++;
            break;
          case 6:
            date_livraison_7 = tabTemp[i];
            compteur++;
            break;
          case 7:
            heure_livraison_8 = tabTemp[i];
            compteur++;
            break;
          
          default:
            break;
        }
      }
      if (compteur == tabTemp.length) {
        retour = true;
      }
      else {
        majError("[CommandeEDI_GP] tabTemp length: " + tabTemp.length + " -> compteur: " + compteur);
      }
      
      retour = controlerCoherenceEnTete();
    }
    else {
      majError("[CommandeEDI_GP] " + fichierOrigine + " majDesInfosEntete() tabTemp à NULL ");
    }
    
    return retour;
  }
  
  /**
   * Mettre à jour les dates de la commandes sur la base de la ligne DTM
   */
  private boolean majDesInfosDates(String ligne) {
    if (ligne == null) {
      majError("(CommandeEDI_GP] majDesInfosDates() ligne NULL");
      return false;
    }
    
    boolean retour = false;
    
    String[] tabTemp = decoupageDeLigne(ligne);
    
    if (tabTemp != null) {
      int compteur = 0;
      for (int i = 0; i < tabTemp.length; i++) {
        switch (i) {
          case 0:
            id_dates_cdes_1 = tabTemp[i];
            compteur++;
            break;
          case 1:
            type_date_cdes_2 = tabTemp[i];
            compteur++;
            break;
          case 2:
            date_cdes_3 = tabTemp[i];
            compteur++;
            break;
          
          default:
            break;
        }
      }
      if (compteur == tabTemp.length) {
        retour = true;
      }
      else {
        majError("[CommandeEDI_GP] majDesInfosDates() tabTemp length: " + tabTemp.length + " -> compteur: " + compteur);
      }
      
      retour = controlerCoherenceEnTete();
    }
    else {
      majError("[CommandeEDI_GP] majDesInfosDates() tabTemp à NULL ");
    }
    
    return retour;
  }
  
  /**
   * Permet de vérifier la cohérence des zones de l'entête de commande
   */
  private boolean controlerCoherenceEnTete() {
    if (id_entete_1 == null || !id_entete_1.equals(DECLA_ENT)) {
      majError("[CommandeEDI_GP] controlerCoherenceEnTete: id_entete_1: " + id_entete_1);
      return false;
    }
    
    if (code_destinataire_7 == null || !code_destinataire_7.equals(parametresFlux.getEAN_founisseur_EDI())) {
      majError("[CommandeEDI_GP] controlerCoherenceEnTete: code_destinataire_7: " + code_destinataire_7);
      return false;
    }
    // TODO IL FAUT CONTROLER LA COHERENCE DES AUTRES DONNEES
    
    return true;
  }
  
  /**
   * Met à jour les différents intervenants "partenaires" de la commande
   */
  private boolean majDesInfosPartenaires(String ligne) {
    if (ligne == null) {
      majError("(CommandeEDI_GP] majDesInfosPartenaires() ligne NULL");
      return false;
    }
    
    boolean retour = false;
    
    String[] tabTemp = decoupageDeLigne(ligne);
    if (tabTemp != null) {
      if (tabTemp.length > 1) {
        // On affecte la ligne au bon partenaire
        if (tabTemp[1].trim().equals(DECLA_PAR_SU)) {
          fournisseur = new Partenaire_EDI(parametresFlux, tabTemp);
          retour = fournisseur != null && fournisseur.isValide;
          if (!retour) {
            majError("[CommandeEDI_GP] majDesInfosPartenaires() PB fournisseurl " + fournisseur.getMsgError());
          }
        }
        else if (tabTemp[1].trim().equals(DECLA_PAR_BY)) {
          clientAcheteur = new Partenaire_EDI(parametresFlux, tabTemp);
          retour = clientAcheteur != null && clientAcheteur.isValide;
          if (!retour) {
            majError("[CommandeEDI_GP] majDesInfosPartenaires() PB clientAcheteur " + clientAcheteur.getMsgError());
          }
        }
        else if (tabTemp[1].trim().equals(DECLA_PAR_DP)) {
          clientLivre = new Partenaire_EDI(parametresFlux, tabTemp);
          retour = clientLivre != null && clientLivre.isValide;
          if (!retour) {
            majError("[CommandeEDI_GP] majDesInfosPartenaires() PB clientLivre " + clientLivre.getMsgError());
          }
        }
        else if (tabTemp[1].trim().equals(DECLA_PAR_IV)) {
          clientFacture = new Partenaire_EDI(parametresFlux, tabTemp);
          retour = clientFacture != null && clientFacture.isValide;
          if (!retour) {
            majError("[CommandeEDI_GP] majDesInfosPartenaires() PB clientFacture " + clientFacture.getMsgError());
          }
        }
        else {
          retour = false;
          majError("[CommandeEDI_GP] majDesInfosPartenaires() PB de typage partenaire ");
        }
      }
    }
    else {
      majError("[CommandeEDI_GP] majDesInfosPartenaires() tabTemp null ");
    }
    
    return retour;
  }
  
  /**
   * permet de récupérer les commentaires de la commande EDI
   */
  private boolean majDesCommentaires(String ligne) {
    if (ligne == null) {
      majError("(CommandeEDI_GP] majDesCommentaires() ligne NULL");
      return false;
    }
    
    boolean retour = false;
    String[] tabTemp = decoupageDeLigne(ligne);
    
    if (tabTemp != null) {
      for (int i = 0; i < tabTemp.length; i++) {
        switch (i) {
          // case 0: id_dates_cdes_1 = tabTemp[i]; compteur++; break;
          case 1:
            typeCommentaires = tabTemp[i];
            break;
          case 2:
            commentaires += tabTemp[i];
            break;
          
          default:
            break;
        }
      }
      
      // On gère les commentaires de la commande
      if (commentaires != null && !commentaires.trim().isEmpty()) {
        BlocCommentairesV2 gestionCommentaires = new BlocCommentairesV2(commentaires);
        blocCommentaires = gestionCommentaires.getFormateCommentaire();
      }
      
      retour = true;
    }
    else {
      majError("[CommandeEDI_GP] majDesCommentaires() tabTemp à NULL ");
    }
    
    return retour;
  }
  
  // ++++++++++++++++++++++++++++++++++ ACCESSEURS +++++++++++++++++++++++++++++++++
  
  public String getId_entete_1() {
    return id_entete_1;
  }
  
  public void setId_entete_1(String id_entete_1) {
    this.id_entete_1 = id_entete_1;
  }
  
  public String getType_message_2() {
    return type_message_2;
  }
  
  public void setType_message_2(String type_message_2) {
    this.type_message_2 = type_message_2;
  }
  
  public String getNumero_document_3() {
    return numero_document_3;
  }
  
  public void setNumero_document_3(String numero_document_3) {
    this.numero_document_3 = numero_document_3;
  }
  
  public String getDate_document_4() {
    return date_document_4;
  }
  
  public void setDate_document_4(String date_document_4) {
    this.date_document_4 = date_document_4;
  }
  
  public String getHeure_document_5() {
    return heure_document_5;
  }
  
  public void setHeure_document_5(String heure_document_5) {
    this.heure_document_5 = heure_document_5;
  }
  
  public String getCode_monnaie_6() {
    return code_monnaie_6;
  }
  
  public void setCode_monnaie_6(String code_monnaie_6) {
    this.code_monnaie_6 = code_monnaie_6;
  }
  
  public String getDate_livraison_7() {
    return date_livraison_7;
  }
  
  public void setDate_livraison_7(String date_livraison_7) {
    this.date_livraison_7 = date_livraison_7;
  }
  
  public String getHeure_livraison_8() {
    return heure_livraison_8;
  }
  
  public void setHeure_livraison_8(String heure_livraison_8) {
    this.heure_livraison_8 = heure_livraison_8;
  }
  
  public String getId_dates_cdes_1() {
    return id_dates_cdes_1;
  }
  
  public void setId_dates_cdes_1(String id_dates_cdes_1) {
    this.id_dates_cdes_1 = id_dates_cdes_1;
  }
  
  public String getType_date_cdes_2() {
    return type_date_cdes_2;
  }
  
  public void setType_date_cdes_2(String type_date_cdes_2) {
    this.type_date_cdes_2 = type_date_cdes_2;
  }
  
  public String getDate_cdes_3() {
    return date_cdes_3;
  }
  
  public void setDate_cdes_3(String date_cdes_3) {
    this.date_cdes_3 = date_cdes_3;
  }
  
  public Partenaire_EDI getClientAcheteur() {
    return clientAcheteur;
  }
  
  public void setClientAcheteur(Partenaire_EDI clientAcheteur) {
    this.clientAcheteur = clientAcheteur;
  }
  
  public Partenaire_EDI getClientLivre() {
    return clientLivre;
  }
  
  public void setClientLivre(Partenaire_EDI clientLivre) {
    this.clientLivre = clientLivre;
  }
  
  public Partenaire_EDI getClientFacture() {
    return clientFacture;
  }
  
  public void setClientFacture(Partenaire_EDI clientFacture) {
    this.clientFacture = clientFacture;
  }
  
  public Partenaire_EDI getFournisseur() {
    return fournisseur;
  }
  
  public void setFournisseur(Partenaire_EDI fournisseur) {
    this.fournisseur = fournisseur;
  }
  
  public String getTypeCommentaires() {
    return typeCommentaires;
  }
  
  public void setTypeCommentaires(String typeCommentaires) {
    this.typeCommentaires = typeCommentaires;
  }
  
  public String getCommentaires() {
    return commentaires;
  }
  
  public void setCommentaires(String commentaires) {
    this.commentaires = commentaires;
  }
  
  public ArrayList<Ligne_Cde_EDI_GP> getLignesArticles() {
    return lignesArticles;
  }
  
  public void setLignesArticles(ArrayList<Ligne_Cde_EDI_GP> lignesArticles) {
    this.lignesArticles = lignesArticles;
  }
  
  public ArrayList<String> getBlocCommentaires() {
    return blocCommentaires;
  }
  
  public void setBlocCommentaires(ArrayList<String> blocCommentaires) {
    this.blocCommentaires = blocCommentaires;
  }
  
  public String getFichierOrigine() {
    return fichierOrigine;
  }
}
