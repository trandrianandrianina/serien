/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.prodevis;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.prodevis.DescriptionProDevis;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgInjecterProDevis extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SGVM9GC2";
  
  /**
   * Appel du programme RPG qui va injecter un prodevis.
   */
  public IdDocumentVente injecterDocumentProDevis(SystemeManager pSysteme, DescriptionProDevis pDescription, IdClient pIdClient) {
    if (pSysteme == null) {
      throw new MessageErreurException("L'environnement du serveur n'est pas initialisé.");
    }
    if (pDescription == null || pDescription.getNomFichier().isEmpty()) {
      throw new MessageErreurException("Le nom du fichier pro-devis n'est pas renseigné.");
    }
    if (pIdClient == null || pIdClient.getCodeEtablissement().isEmpty()) {
      throw new MessageErreurException("Le client pour lequel le devis doit être importé n'est pas connu ou encore créé.");
    }
    
    // Préparation des paramètres
    ProgramParameter[] listeParametres = new ProgramParameter[5];
    AS400Text pfile = new AS400Text(DescriptionProDevis.TAILLE_NOM_FICHIER, pSysteme.getSystem());
    listeParametres[0] = new ProgramParameter(pfile.toBytes(pDescription.getNomFichier()), DescriptionProDevis.TAILLE_NOM_FICHIER);
    AS400Text petb = new AS400Text(IdClient.LONGUEUR_CODE_ETABLISSEMENT, pSysteme.getSystem());
    listeParametres[1] = new ProgramParameter(petb.toBytes(pIdClient.getCodeEtablissement()), IdClient.LONGUEUR_CODE_ETABLISSEMENT);
    AS400Text pcliliv = new AS400Text(IdClient.LONGUEUR_NUMERO + IdClient.LONGUEUR_SUFFIXE, pSysteme.getSystem());
    listeParametres[2] = new ProgramParameter(pcliliv.toBytes(pIdClient.getIndicatif(IdClient.INDICATIF_NUM_SUF)),
        IdClient.LONGUEUR_NUMERO + IdClient.LONGUEUR_SUFFIXE);
    AS400Text pxnbon = new AS400Text(IdDocumentVente.LONGUEUR_INDICATIF, pSysteme.getSystem());
    listeParametres[3] = new ProgramParameter(pxnbon.toBytes(""), IdDocumentVente.LONGUEUR_INDICATIF);
    listeParametres[4] = erreur;
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG d'importation pour chaque fichier trouvé
    IdDocumentVente idDocument = null;
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), listeParametres)) {
      // Récupération et conversion des paramètres du RPG
      byte[] as400Data = listeParametres[3].getOutputData();
      String indicatifDocument = (String) pxnbon.toObject(as400Data);
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de l'id du document généré
      idDocument = IdDocumentVente.getInstanceParIndicatif(indicatifDocument);
    }
    
    return idDocument;
  }
  
}
