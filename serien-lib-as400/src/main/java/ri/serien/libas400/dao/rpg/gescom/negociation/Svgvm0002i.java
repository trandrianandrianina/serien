/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.negociation;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0002i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIOPT = 3;
  public static final int SIZE_TITOP = 1;
  public static final int DECIMAL_TITOP = 0;
  public static final int SIZE_TICRE = 7;
  public static final int DECIMAL_TICRE = 0;
  public static final int SIZE_TIMOD = 7;
  public static final int DECIMAL_TIMOD = 0;
  public static final int SIZE_TITRT = 7;
  public static final int DECIMAL_TITRT = 0;
  public static final int SIZE_TIETB = 3;
  public static final int SIZE_TICAT = 1;
  public static final int SIZE_TICNV = 6;
  public static final int SIZE_TITRA = 1;
  public static final int SIZE_TIRAT = 20;
  public static final int SIZE_TIQTE = 7;
  public static final int SIZE_TITCD = 1;
  public static final int SIZE_TIVAL = 9;
  public static final int DECIMAL_TIVAL = 2;
  public static final int SIZE_TIREM1 = 4;
  public static final int DECIMAL_TIREM1 = 2;
  public static final int SIZE_TIREM2 = 4;
  public static final int DECIMAL_TIREM2 = 2;
  public static final int SIZE_TIREM3 = 4;
  public static final int DECIMAL_TIREM3 = 2;
  public static final int SIZE_TIREM4 = 4;
  public static final int DECIMAL_TIREM4 = 2;
  public static final int SIZE_TIREM5 = 4;
  public static final int DECIMAL_TIREM5 = 2;
  public static final int SIZE_TIREM6 = 4;
  public static final int DECIMAL_TIREM6 = 2;
  public static final int SIZE_TICOE = 5;
  public static final int DECIMAL_TICOE = 4;
  public static final int SIZE_TIDTD = 7;
  public static final int DECIMAL_TIDTD = 0;
  public static final int SIZE_TIDTF = 7;
  public static final int DECIMAL_TIDTF = 0;
  public static final int SIZE_TIFPR = 5;
  public static final int SIZE_TIDEV = 3;
  public static final int SIZE_TOTALE_DS = 156;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINLI = 5;
  public static final int VAR_PIDAT = 6;
  public static final int VAR_PIOPT = 7;
  public static final int VAR_TITOP = 8;
  public static final int VAR_TICRE = 9;
  public static final int VAR_TIMOD = 10;
  public static final int VAR_TITRT = 11;
  public static final int VAR_TIETB = 12;
  public static final int VAR_TICAT = 13;
  public static final int VAR_TICNV = 14;
  public static final int VAR_TITRA = 15;
  public static final int VAR_TIRAT = 16;
  public static final int VAR_TIQTE = 17;
  public static final int VAR_TITCD = 18;
  public static final int VAR_TIVAL = 19;
  public static final int VAR_TIREM1 = 20;
  public static final int VAR_TIREM2 = 21;
  public static final int VAR_TIREM3 = 22;
  public static final int VAR_TIREM4 = 23;
  public static final int VAR_TIREM5 = 24;
  public static final int VAR_TIREM6 = 25;
  public static final int VAR_TICOE = 26;
  public static final int VAR_TIDTD = 27;
  public static final int VAR_TIDTF = 28;
  public static final int VAR_TIFPR = 29;
  public static final int VAR_TIDEV = 30;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "D","E",X","9" *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de ligne *
  private BigDecimal pidat = BigDecimal.ZERO; // Date de traitement
  private String piopt = ""; // Option de traitement
  private BigDecimal titop = BigDecimal.ZERO; // Top système
  private BigDecimal ticre = BigDecimal.ZERO; // Date de création
  private BigDecimal timod = BigDecimal.ZERO; // Date de modification
  private BigDecimal titrt = BigDecimal.ZERO; // Date de traitement
  private String tietb = ""; // Code établissement
  private String ticat = ""; // Catégorie condition
  private String ticnv = ""; // Code condition
  private String titra = ""; // Type de rattachement
  private String tirat = ""; // Code Rattachement
  private String tiqte = ""; // Quantité minimale
  private String titcd = ""; // Type de condition
  private BigDecimal tival = BigDecimal.ZERO; // Valeur en EUROS
  private BigDecimal tirem1 = BigDecimal.ZERO; // Remise 1
  private BigDecimal tirem2 = BigDecimal.ZERO; // Remise 2
  private BigDecimal tirem3 = BigDecimal.ZERO; // Remise 3
  private BigDecimal tirem4 = BigDecimal.ZERO; // Remise 4
  private BigDecimal tirem5 = BigDecimal.ZERO; // Remise 5
  private BigDecimal tirem6 = BigDecimal.ZERO; // Remise 6
  private BigDecimal ticoe = BigDecimal.ZERO; // Coefficient < ou > à 1
  private BigDecimal tidtd = BigDecimal.ZERO; // Date de début de validité
  private BigDecimal tidtf = BigDecimal.ZERO; // Date de fin de validité
  private String tifpr = ""; // Formule de prix
  private String tidev = ""; // Code devise
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9" *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de ligne *
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date de traitement
      new AS400Text(SIZE_PIOPT), // Option de traitement
      new AS400ZonedDecimal(SIZE_TITOP, DECIMAL_TITOP), // Top système
      new AS400PackedDecimal(SIZE_TICRE, DECIMAL_TICRE), // Date de création
      new AS400PackedDecimal(SIZE_TIMOD, DECIMAL_TIMOD), // Date de modification
      new AS400PackedDecimal(SIZE_TITRT, DECIMAL_TITRT), // Date de traitement
      new AS400Text(SIZE_TIETB), // Code établissement
      new AS400Text(SIZE_TICAT), // Catégorie condition
      new AS400Text(SIZE_TICNV), // Code condition
      new AS400Text(SIZE_TITRA), // Type de rattachement
      new AS400Text(SIZE_TIRAT), // Code Rattachement
      new AS400Text(SIZE_TIQTE), // Quantité minimale
      new AS400Text(SIZE_TITCD), // Type de condition
      new AS400PackedDecimal(SIZE_TIVAL, DECIMAL_TIVAL), // Valeur en EUROS
      new AS400ZonedDecimal(SIZE_TIREM1, DECIMAL_TIREM1), // Remise 1
      new AS400ZonedDecimal(SIZE_TIREM2, DECIMAL_TIREM2), // Remise 2
      new AS400ZonedDecimal(SIZE_TIREM3, DECIMAL_TIREM3), // Remise 3
      new AS400ZonedDecimal(SIZE_TIREM4, DECIMAL_TIREM4), // Remise 4
      new AS400ZonedDecimal(SIZE_TIREM5, DECIMAL_TIREM5), // Remise 5
      new AS400ZonedDecimal(SIZE_TIREM6, DECIMAL_TIREM6), // Remise 6
      new AS400PackedDecimal(SIZE_TICOE, DECIMAL_TICOE), // Coefficient < ou > à 1
      new AS400PackedDecimal(SIZE_TIDTD, DECIMAL_TIDTD), // Date de début de validité
      new AS400PackedDecimal(SIZE_TIDTF, DECIMAL_TIDTF), // Date de fin de validité
      new AS400Text(SIZE_TIFPR), // Formule de prix
      new AS400Text(SIZE_TIDEV), // Code devise
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, picod, pietb, pinum, pisuf, pinli, pidat, piopt, titop, ticre, timod, titrt, tietb, ticat, ticnv, titra,
          tirat, tiqte, titcd, tival, tirem1, tirem2, tirem3, tirem4, tirem5, tirem6, ticoe, tidtd, tidtf, tifpr, tidev, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinli = (BigDecimal) output[5];
    pidat = (BigDecimal) output[6];
    piopt = (String) output[7];
    titop = (BigDecimal) output[8];
    ticre = (BigDecimal) output[9];
    timod = (BigDecimal) output[10];
    titrt = (BigDecimal) output[11];
    tietb = (String) output[12];
    ticat = (String) output[13];
    ticnv = (String) output[14];
    titra = (String) output[15];
    tirat = (String) output[16];
    tiqte = (String) output[17];
    titcd = (String) output[18];
    tival = (BigDecimal) output[19];
    tirem1 = (BigDecimal) output[20];
    tirem2 = (BigDecimal) output[21];
    tirem3 = (BigDecimal) output[22];
    tirem4 = (BigDecimal) output[23];
    tirem5 = (BigDecimal) output[24];
    tirem6 = (BigDecimal) output[25];
    ticoe = (BigDecimal) output[26];
    tidtd = (BigDecimal) output[27];
    tidtf = (BigDecimal) output[28];
    tifpr = (String) output[29];
    tidev = (String) output[30];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPiopt(String pPiopt) {
    if (pPiopt == null) {
      return;
    }
    piopt = pPiopt;
  }
  
  public String getPiopt() {
    return piopt;
  }
  
  public void setTitop(BigDecimal pTitop) {
    if (pTitop == null) {
      return;
    }
    titop = pTitop.setScale(DECIMAL_TITOP, RoundingMode.HALF_UP);
  }
  
  public void setTitop(Integer pTitop) {
    if (pTitop == null) {
      return;
    }
    titop = BigDecimal.valueOf(pTitop);
  }
  
  public Integer getTitop() {
    return titop.intValue();
  }
  
  public void setTicre(BigDecimal pTicre) {
    if (pTicre == null) {
      return;
    }
    ticre = pTicre.setScale(DECIMAL_TICRE, RoundingMode.HALF_UP);
  }
  
  public void setTicre(Integer pTicre) {
    if (pTicre == null) {
      return;
    }
    ticre = BigDecimal.valueOf(pTicre);
  }
  
  public void setTicre(Date pTicre) {
    if (pTicre == null) {
      return;
    }
    ticre = BigDecimal.valueOf(ConvertDate.dateToDb2(pTicre));
  }
  
  public Integer getTicre() {
    return ticre.intValue();
  }
  
  public Date getTicreConvertiEnDate() {
    return ConvertDate.db2ToDate(ticre.intValue(), null);
  }
  
  public void setTimod(BigDecimal pTimod) {
    if (pTimod == null) {
      return;
    }
    timod = pTimod.setScale(DECIMAL_TIMOD, RoundingMode.HALF_UP);
  }
  
  public void setTimod(Integer pTimod) {
    if (pTimod == null) {
      return;
    }
    timod = BigDecimal.valueOf(pTimod);
  }
  
  public void setTimod(Date pTimod) {
    if (pTimod == null) {
      return;
    }
    timod = BigDecimal.valueOf(ConvertDate.dateToDb2(pTimod));
  }
  
  public Integer getTimod() {
    return timod.intValue();
  }
  
  public Date getTimodConvertiEnDate() {
    return ConvertDate.db2ToDate(timod.intValue(), null);
  }
  
  public void setTitrt(BigDecimal pTitrt) {
    if (pTitrt == null) {
      return;
    }
    titrt = pTitrt.setScale(DECIMAL_TITRT, RoundingMode.HALF_UP);
  }
  
  public void setTitrt(Integer pTitrt) {
    if (pTitrt == null) {
      return;
    }
    titrt = BigDecimal.valueOf(pTitrt);
  }
  
  public void setTitrt(Date pTitrt) {
    if (pTitrt == null) {
      return;
    }
    titrt = BigDecimal.valueOf(ConvertDate.dateToDb2(pTitrt));
  }
  
  public Integer getTitrt() {
    return titrt.intValue();
  }
  
  public Date getTitrtConvertiEnDate() {
    return ConvertDate.db2ToDate(titrt.intValue(), null);
  }
  
  public void setTietb(String pTietb) {
    if (pTietb == null) {
      return;
    }
    tietb = pTietb;
  }
  
  public String getTietb() {
    return tietb;
  }
  
  public void setTicat(Character pTicat) {
    if (pTicat == null) {
      return;
    }
    ticat = String.valueOf(pTicat);
  }
  
  public Character getTicat() {
    return ticat.charAt(0);
  }
  
  public void setTicnv(String pTicnv) {
    if (pTicnv == null) {
      return;
    }
    ticnv = pTicnv;
  }
  
  public String getTicnv() {
    return ticnv;
  }
  
  public void setTitra(Character pTitra) {
    if (pTitra == null) {
      return;
    }
    titra = String.valueOf(pTitra);
  }
  
  public Character getTitra() {
    return titra.charAt(0);
  }
  
  public void setTirat(String pTirat) {
    if (pTirat == null) {
      return;
    }
    tirat = pTirat;
  }
  
  public String getTirat() {
    return tirat;
  }
  
  public void setTiqte(String pTiqte) {
    if (pTiqte == null) {
      return;
    }
    tiqte = pTiqte;
  }
  
  public String getTiqte() {
    return tiqte;
  }
  
  public void setTitcd(Character pTitcd) {
    if (pTitcd == null) {
      return;
    }
    titcd = String.valueOf(pTitcd);
  }
  
  public Character getTitcd() {
    return titcd.charAt(0);
  }
  
  public void setTival(BigDecimal pTival) {
    if (pTival == null) {
      return;
    }
    tival = pTival.setScale(DECIMAL_TIVAL, RoundingMode.HALF_UP);
  }
  
  public void setTival(Double pTival) {
    if (pTival == null) {
      return;
    }
    tival = BigDecimal.valueOf(pTival).setScale(DECIMAL_TIVAL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getTival() {
    return tival.setScale(DECIMAL_TIVAL, RoundingMode.HALF_UP);
  }
  
  public void setTirem1(BigDecimal pTirem1) {
    if (pTirem1 == null) {
      return;
    }
    tirem1 = pTirem1.setScale(DECIMAL_TIREM1, RoundingMode.HALF_UP);
  }
  
  public void setTirem1(Double pTirem1) {
    if (pTirem1 == null) {
      return;
    }
    tirem1 = BigDecimal.valueOf(pTirem1).setScale(DECIMAL_TIREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getTirem1() {
    return tirem1.setScale(DECIMAL_TIREM1, RoundingMode.HALF_UP);
  }
  
  public void setTirem2(BigDecimal pTirem2) {
    if (pTirem2 == null) {
      return;
    }
    tirem2 = pTirem2.setScale(DECIMAL_TIREM2, RoundingMode.HALF_UP);
  }
  
  public void setTirem2(Double pTirem2) {
    if (pTirem2 == null) {
      return;
    }
    tirem2 = BigDecimal.valueOf(pTirem2).setScale(DECIMAL_TIREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getTirem2() {
    return tirem2.setScale(DECIMAL_TIREM2, RoundingMode.HALF_UP);
  }
  
  public void setTirem3(BigDecimal pTirem3) {
    if (pTirem3 == null) {
      return;
    }
    tirem3 = pTirem3.setScale(DECIMAL_TIREM3, RoundingMode.HALF_UP);
  }
  
  public void setTirem3(Double pTirem3) {
    if (pTirem3 == null) {
      return;
    }
    tirem3 = BigDecimal.valueOf(pTirem3).setScale(DECIMAL_TIREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getTirem3() {
    return tirem3.setScale(DECIMAL_TIREM3, RoundingMode.HALF_UP);
  }
  
  public void setTirem4(BigDecimal pTirem4) {
    if (pTirem4 == null) {
      return;
    }
    tirem4 = pTirem4.setScale(DECIMAL_TIREM4, RoundingMode.HALF_UP);
  }
  
  public void setTirem4(Double pTirem4) {
    if (pTirem4 == null) {
      return;
    }
    tirem4 = BigDecimal.valueOf(pTirem4).setScale(DECIMAL_TIREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getTirem4() {
    return tirem4.setScale(DECIMAL_TIREM4, RoundingMode.HALF_UP);
  }
  
  public void setTirem5(BigDecimal pTirem5) {
    if (pTirem5 == null) {
      return;
    }
    tirem5 = pTirem5.setScale(DECIMAL_TIREM5, RoundingMode.HALF_UP);
  }
  
  public void setTirem5(Double pTirem5) {
    if (pTirem5 == null) {
      return;
    }
    tirem5 = BigDecimal.valueOf(pTirem5).setScale(DECIMAL_TIREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getTirem5() {
    return tirem5.setScale(DECIMAL_TIREM5, RoundingMode.HALF_UP);
  }
  
  public void setTirem6(BigDecimal pTirem6) {
    if (pTirem6 == null) {
      return;
    }
    tirem6 = pTirem6.setScale(DECIMAL_TIREM6, RoundingMode.HALF_UP);
  }
  
  public void setTirem6(Double pTirem6) {
    if (pTirem6 == null) {
      return;
    }
    tirem6 = BigDecimal.valueOf(pTirem6).setScale(DECIMAL_TIREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getTirem6() {
    return tirem6.setScale(DECIMAL_TIREM6, RoundingMode.HALF_UP);
  }
  
  public void setTicoe(BigDecimal pTicoe) {
    if (pTicoe == null) {
      return;
    }
    ticoe = pTicoe.setScale(DECIMAL_TICOE, RoundingMode.HALF_UP);
  }
  
  public void setTicoe(Double pTicoe) {
    if (pTicoe == null) {
      return;
    }
    ticoe = BigDecimal.valueOf(pTicoe).setScale(DECIMAL_TICOE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getTicoe() {
    return ticoe.setScale(DECIMAL_TICOE, RoundingMode.HALF_UP);
  }
  
  public void setTidtd(BigDecimal pTidtd) {
    if (pTidtd == null) {
      return;
    }
    tidtd = pTidtd.setScale(DECIMAL_TIDTD, RoundingMode.HALF_UP);
  }
  
  public void setTidtd(Integer pTidtd) {
    if (pTidtd == null) {
      return;
    }
    tidtd = BigDecimal.valueOf(pTidtd);
  }
  
  public void setTidtd(Date pTidtd) {
    if (pTidtd == null) {
      return;
    }
    tidtd = BigDecimal.valueOf(ConvertDate.dateToDb2(pTidtd));
  }
  
  public Integer getTidtd() {
    return tidtd.intValue();
  }
  
  public Date getTidtdConvertiEnDate() {
    return ConvertDate.db2ToDate(tidtd.intValue(), null);
  }
  
  public void setTidtf(BigDecimal pTidtf) {
    if (pTidtf == null) {
      return;
    }
    tidtf = pTidtf.setScale(DECIMAL_TIDTF, RoundingMode.HALF_UP);
  }
  
  public void setTidtf(Integer pTidtf) {
    if (pTidtf == null) {
      return;
    }
    tidtf = BigDecimal.valueOf(pTidtf);
  }
  
  public void setTidtf(Date pTidtf) {
    if (pTidtf == null) {
      return;
    }
    tidtf = BigDecimal.valueOf(ConvertDate.dateToDb2(pTidtf));
  }
  
  public Integer getTidtf() {
    return tidtf.intValue();
  }
  
  public Date getTidtfConvertiEnDate() {
    return ConvertDate.db2ToDate(tidtf.intValue(), null);
  }
  
  public void setTifpr(String pTifpr) {
    if (pTifpr == null) {
      return;
    }
    tifpr = pTifpr;
  }
  
  public String getTifpr() {
    return tifpr;
  }
  
  public void setTidev(String pTidev) {
    if (pTidev == null) {
      return;
    }
    tidev = pTidev;
  }
  
  public String getTidev() {
    return tidev;
  }
}
