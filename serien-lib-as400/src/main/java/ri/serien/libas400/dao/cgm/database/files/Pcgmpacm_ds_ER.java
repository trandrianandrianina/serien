/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.cgm.database.files;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pcgmpacm_ds_ER pour les ER
 * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
 */

public class Pcgmpacm_ds_ER extends DataStructureRecord {

  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "ERJ"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "ERL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "ERSNS"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ERRGL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "ERC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "ERG"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "ERCRI"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "ERREL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "ERVAL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "ERRFC"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "ERNUM"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "EREDT1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "EREDT2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), "ERDAT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "ERCRD"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ERRGL1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "ERSEUI"));

    length = 300;
  }
}
