/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0048i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIOPT = 3;
  public static final int SIZE_PIETA0 = 1;
  public static final int DECIMAL_PIETA0 = 0;
  public static final int SIZE_PITTC0 = 13;
  public static final int DECIMAL_PITTC0 = 2;
  public static final int SIZE_PINOM = 30;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 76;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PIDAT = 5;
  public static final int VAR_PIOPT = 6;
  public static final int VAR_PIETA0 = 7;
  public static final int VAR_PITTC0 = 8;
  public static final int VAR_PINOM = 9;
  public static final int VAR_PIARR = 10;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "D","E",X","9" *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal pidat = BigDecimal.ZERO; // Date de traitement *
  private String piopt = ""; // Option de traitement *
  private BigDecimal pieta0 = BigDecimal.ZERO; // Etat avant *
  private BigDecimal pittc0 = BigDecimal.ZERO; // Montant avant *
  private String pinom = ""; // Nom pour mvmt de stock *
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9" *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date de traitement *
      new AS400Text(SIZE_PIOPT), // Option de traitement *
      new AS400ZonedDecimal(SIZE_PIETA0, DECIMAL_PIETA0), // Etat avant *
      new AS400ZonedDecimal(SIZE_PITTC0, DECIMAL_PITTC0), // Montant avant *
      new AS400Text(SIZE_PINOM), // Nom pour mvmt de stock *
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, picod, pietb, pinum, pisuf, pidat, piopt, pieta0, pittc0, pinom, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pidat = (BigDecimal) output[5];
    piopt = (String) output[6];
    pieta0 = (BigDecimal) output[7];
    pittc0 = (BigDecimal) output[8];
    pinom = (String) output[9];
    piarr = (String) output[10];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPiopt(String pPiopt) {
    if (pPiopt == null) {
      return;
    }
    piopt = pPiopt;
  }
  
  public String getPiopt() {
    return piopt;
  }
  
  public void setPieta0(BigDecimal pPieta0) {
    if (pPieta0 == null) {
      return;
    }
    pieta0 = pPieta0.setScale(DECIMAL_PIETA0, RoundingMode.HALF_UP);
  }
  
  public void setPieta0(Integer pPieta0) {
    if (pPieta0 == null) {
      return;
    }
    pieta0 = BigDecimal.valueOf(pPieta0);
  }
  
  public Integer getPieta0() {
    return pieta0.intValue();
  }
  
  public void setPittc0(BigDecimal pPittc0) {
    if (pPittc0 == null) {
      return;
    }
    pittc0 = pPittc0.setScale(DECIMAL_PITTC0, RoundingMode.HALF_UP);
  }
  
  public void setPittc0(Double pPittc0) {
    if (pPittc0 == null) {
      return;
    }
    pittc0 = BigDecimal.valueOf(pPittc0).setScale(DECIMAL_PITTC0, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPittc0() {
    return pittc0;
  }
  
  public void setPinom(String pPinom) {
    if (pPinom == null) {
      return;
    }
    pinom = pPinom;
  }
  
  public String getPinom() {
    return pinom;
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
