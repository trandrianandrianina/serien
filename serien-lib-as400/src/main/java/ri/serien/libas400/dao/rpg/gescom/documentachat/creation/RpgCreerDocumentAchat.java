/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.creation;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.Reglements;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.document.ListeTva;
import ri.serien.libcommun.gescom.commun.document.ListeZonePersonnalisee;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgCreerDocumentAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGAM0010";
  
  /**
   * Appel du programme RPG qui va créer un document.
   */
  public DocumentAchat creerDocumentAchat(SystemeManager pSysteme, String pProfil, DocumentAchat pDocument) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    pProfil = Constantes.normerTexte(pProfil);
    if (pProfil.isEmpty()) {
      throw new MessageErreurException("Vous devez obligatoirement fournir le profil de l'utilisateur.");
    }
    DocumentAchat.controlerId(pDocument, false);
    IdFournisseur.controlerId(pDocument.getIdFournisseur(), true);
    
    // Préparation des paramètres du programme RPG
    Svgam0010i entree = new Svgam0010i();
    Svgam0010o sortie = new Svgam0010o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiusr(pProfil);
    entree.setPitop(pDocument.getTopSysteme());
    entree.setPicod(pDocument.getId().getCodeEntete().getCode());
    entree.setPietb(pDocument.getId().getCodeEtablissement());
    entree.setPinum(pDocument.getId().getNumero());
    entree.setPisuf(pDocument.getId().getSuffixe());
    
    entree.setPicre(pDocument.getDateCreation());
    entree.setPidat(pDocument.getDateDernierTraitement());
    entree.setPihom(pDocument.getDateHomologation());
    entree.setPirec(pDocument.getDateReception());
    entree.setPifac(pDocument.getDateFacturation());
    entree.setPinfa(pDocument.getNumeroFacture());
    /* non utilisées pour l'instant
    entree.pinls = ;
    entree.pipli = ;
    entree.pidli = ;
    entree.pinli = ;
    entree.pitlv = ;
    entree.pitll = ;
    entree.pidsu = ;
    */
    entree.setPirfl(pDocument.getReferenceInterne());
    if (pDocument.getCodeEtat() != null) {
      entree.setPieta(pDocument.getCodeEtat().getCode());
    }
    entree.setPitfc(pDocument.getTopFournisseurDeCommande());
    entree.setPitt(pDocument.getTopMontantsFactureForces());
    entree.setPinrg(pDocument.getRangReglement());
    
    // Alimentation des champs concernant les TVA
    ListeTva liste = pDocument.getListeTva();
    entree.setPitva1(liste.getNumeroTVA1());
    entree.setPitva2(liste.getNumeroTVA2());
    entree.setPitva3(liste.getNumeroTVA3());
    entree.setPisl1(liste.getMontantSoumisTVA1());
    entree.setPisl2(liste.getMontantSoumisTVA2());
    entree.setPisl3(liste.getMontantSoumisTVA3());
    entree.setPitl1(liste.getMontantTVA1());
    entree.setPitl2(liste.getMontantTVA2());
    entree.setPitl3(liste.getMontantTVA3());
    entree.setPitv1(liste.getTauxTVA1());
    entree.setPitv2(liste.getTauxTVA2());
    entree.setPitv3(liste.getTauxTVA3());
    
    entree.setPirlv(pDocument.getTopReleve());
    entree.setPinex(pDocument.getNombreExemplaire());
    // entree.piexc = ;
    // entree.piexe = ;
    entree.setPinrr(pDocument.getNombreReglementsReels());
    entree.setPisgn(pDocument.getSigneGere());
    entree.setPiliv(pDocument.getTopLivraison());
    entree.setPicpt(pDocument.getTopComptabilisation());
    entree.setPipgc(pDocument.getTopPrixGaranti());
    entree.setPichf(pDocument.getTopChiffrage());
    entree.setPiina(pDocument.getTopAffectation());
    entree.setPicol(pDocument.getIdFournisseur().getCollectif());
    entree.setPifrs(pDocument.getIdFournisseur().getNumero());
    entree.setPidlp(ConvertDate.dateToDb2(pDocument.getDateLivraisonPrevue()));
    if (pDocument.getListeIdDocumentOrigine() != null && !pDocument.getListeIdDocumentOrigine().isEmpty()) {
      entree.setPinum0(pDocument.getListeIdDocumentOrigine().get(0).getNumero());
      entree.setPisuf0(pDocument.getListeIdDocumentOrigine().get(0).getSuffixe());
    }
    entree.setPiavr(pDocument.getCodeAvoir());
    entree.setPitfa(pDocument.getCodeTypeFacturation());
    entree.setPinat(pDocument.getCodeNatureAchat());
    if (pDocument.getReglement() != null) {
      Reglements reglement = pDocument.getReglement();
      entree.setPie1g(ConvertDate.dateToDb2(reglement.getDateEcheance1()));
      entree.setPie2g(ConvertDate.dateToDb2(reglement.getDateEcheance2()));
      entree.setPie3g(ConvertDate.dateToDb2(reglement.getDateEcheance3()));
      entree.setPie4g(ConvertDate.dateToDb2(reglement.getDateEcheance4()));
      entree.setPir1g(reglement.getMontant1());
      entree.setPir2g(reglement.getMontant2());
      entree.setPir3g(reglement.getMontant3());
      entree.setPir4g(reglement.getMontant4());
      if (reglement.getModeReglement1() != null) {
        entree.setPirg1(reglement.getModeReglement1().getId().getCode());
      }
      entree.setPiec1(reglement.getCodeEcheance1());
      entree.setPipc1(reglement.getPourcentageReglement1());
      if (reglement.getModeReglement2() != null) {
        entree.setPirg2(reglement.getModeReglement2().getId().getCode());
      }
      entree.setPiec2(reglement.getCodeEcheance2());
      entree.setPipc2(reglement.getPourcentageReglement2());
      if (reglement.getModeReglement3() != null) {
        entree.setPirg3(reglement.getModeReglement3().getId().getCode());
      }
      entree.setPiec3(reglement.getCodeEcheance3());
      entree.setPipc3(reglement.getPourcentageReglement3());
      if (reglement.getModeReglement4() != null) {
        entree.setPirg4(reglement.getModeReglement4().getId().getCode());
      }
      entree.setPiec4(reglement.getCodeEcheance4());
      entree.setPipc4(reglement.getPourcentageReglement4());
    }
    entree.setPithtl(pDocument.getTotalHTLignes());
    entree.setPiesc(pDocument.getPourcentageEscompte());
    entree.setPimes(pDocument.getMontantEscompte());
    if (pDocument.getIdMagasinSortie() != null) {
      entree.setPimag(pDocument.getIdMagasinSortie().getCode());
    }
    entree.setPirbl(pDocument.getReferenceBonLivraison());
    entree.setPisan(pDocument.getCodeSectionAnalytique());
    entree.setPiact(pDocument.getCodeAffaire());
    entree.setPillv(pDocument.getCodeLieuLivraison());
    if (pDocument.getIdAcheteur() != null) {
      entree.setPiach(pDocument.getIdAcheteur().getCode());
    }
    entree.setPicja(pDocument.getCodeJournalAchat());
    entree.setPidev(pDocument.getCodeDevise());
    entree.setPichg(pDocument.getTauxDeChange());
    entree.setPicpr(pDocument.getCoefficientAchat());
    entree.setPibas(pDocument.getBaseDevise());
    entree.setPidos(pDocument.getCodeDossierApprovisionnement());
    entree.setPicnt(pDocument.getCodeContainer());
    entree.setPipds(pDocument.getPoids());
    entree.setPivol(pDocument.getVolume());
    entree.setPimta(pDocument.getMontantFraisRepartir());
    if (pDocument.getListeRemiseLigne() != null) {
      ListeRemise listeRemiseLigne = pDocument.getListeRemiseLigne();
      entree.setPirem1(listeRemiseLigne.getPourcentageRemise1());
      entree.setPirem2(listeRemiseLigne.getPourcentageRemise2());
      entree.setPirem3(listeRemiseLigne.getPourcentageRemise3());
      entree.setPirem4(listeRemiseLigne.getPourcentageRemise4());
      entree.setPirem5(listeRemiseLigne.getPourcentageRemise5());
      entree.setPirem6(listeRemiseLigne.getPourcentageRemise6());
      entree.setPitrl(listeRemiseLigne.getTypeRemise());
      entree.setPibrl(listeRemiseLigne.getBaseRemise());
    }
    if (pDocument.getListeRemisePied() != null) {
      ListeRemise listeRemisePied = pDocument.getListeRemisePied();
      entree.setPirp1(listeRemisePied.getPourcentageRemise1());
      entree.setPirp2(listeRemisePied.getPourcentageRemise2());
      entree.setPirp3(listeRemisePied.getPourcentageRemise3());
      entree.setPirp4(listeRemisePied.getPourcentageRemise4());
      entree.setPirp5(listeRemisePied.getPourcentageRemise5());
      entree.setPirp6(listeRemisePied.getPourcentageRemise6());
      entree.setPitrp(listeRemisePied.getTypeRemise());
    }
    if (pDocument.getListeTopPersonnalisable() != null) {
      ListeZonePersonnalisee listeTopPersonnalisable = pDocument.getListeTopPersonnalisable();
      entree.setPitp1(listeTopPersonnalisable.getTopPersonnalisation1());
      entree.setPitp2(listeTopPersonnalisable.getTopPersonnalisation2());
      entree.setPitp3(listeTopPersonnalisable.getTopPersonnalisation3());
      entree.setPitp4(listeTopPersonnalisable.getTopPersonnalisation4());
      entree.setPitp5(listeTopPersonnalisable.getTopPersonnalisation5());
    }
    entree.setPiin1(pDocument.getCodeLitige());
    // entree.setPiin2(pDocument.getCodeLitige());
    if (pDocument.isReceptionDefinitive()) {
      entree.setPiin3('1');
    }
    if (pDocument.getComptabilisationEnStocksFlottants()) {
      entree.setPiin4('1');
    }
    if (pDocument.getBlocageComptabilisation()) {
      entree.setPiin5('1');
    }
    entree.setPicct(pDocument.getCodeContrat());
    entree.setPidat1(pDocument.getDateConfirmationFournisseur());
    entree.setPirbc(pDocument.getReferenceCommande());
    entree.setPiint1(pDocument.getDateIntermediaire1());
    entree.setPiint2(pDocument.getDateIntermediaire2());
    entree.setPietai(pDocument.getCodeEtatIntermediaire());
    if (pDocument.getTraiteParCommandeDilicom()) {
      entree.setPidili('1');
    }
    if (pDocument.getBlocageCommande()) {
      entree.setPiin6('1');
    }
    entree.setPiin7(pDocument.getCodeScenarioDates());
    entree.setPiin8(pDocument.getCodeEtatFraisFixes());
    if (pDocument.getCodeTypeCommande() != null) {
      entree.setPiin9(pDocument.getCodeTypeCommande().getCode());
    }
    if (pDocument.getAdresseFournisseur() != null) {
      Adresse adresse = pDocument.getAdresseFournisseur();
      entree.setPinom2(adresse.getNom());
      entree.setPicpl2(adresse.getComplementNom());
      entree.setPirue2(adresse.getRue());
      entree.setPiloc2(adresse.getLocalisation());
      entree.setPicdp2(adresse.getCodePostalFormate());
      entree.setPivil2(adresse.getVille());
    }
    entree.setPipor0(pDocument.getMontantPortTheorique());
    entree.setPipor1(pDocument.getMontantPortFacture());
    if (pDocument.isFraisPortFournisseurFacture()) {
      entree.setPiin10(' ');
    }
    else {
      entree.setPiin10('1');
    }
    
    if (pDocument.getModeEnlevement()) {
      entree.setPienv('E');
    }
    else {
      entree.setPienv('L');
    }
    entree.setPicntct(pDocument.getIndicatifContact());
    if (pDocument.getIdDocumentPourExtraction() != null) {
      entree.setPicodx(pDocument.getIdDocumentPourExtraction().getCodeEntete().getCode());
      entree.setPietbx(pDocument.getIdDocumentPourExtraction().getCodeEtablissement());
      entree.setPinumx(pDocument.getIdDocumentPourExtraction().getNumero());
      entree.setPisufx(pDocument.getIdDocumentPourExtraction().getSuffixe());
    }
    
    // L'adresse de livraison ne concerne que les commandes usine
    if (pDocument.getAdresseLivraison() != null && pDocument.isDirectUsine()) {
      entree.setPillv("*GVM");
      Adresse adresse = pDocument.getAdresseLivraison();
      entree.setPinom3(adresse.getNom());
      entree.setPicpl3(adresse.getComplementNom());
      entree.setPirue3(adresse.getRue());
      entree.setPiloc3(adresse.getLocalisation());
      entree.setPicdp3(adresse.getCodePostalFormate());
      entree.setPivil3(adresse.getVille());
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Remettre l'identifiant du document avec le numéro et le suffixe renseigné
      EnumCodeEnteteDocumentAchat codeEntete = EnumCodeEnteteDocumentAchat.valueOfByCode(sortie.getPocod());
      IdDocumentAchat idDocument =
          IdDocumentAchat.getInstance(pDocument.getId().getIdEtablissement(), codeEntete, sortie.getPonum(), sortie.getPosuf());
      pDocument.setId(idDocument);
    }
    return pDocument;
  }
}
