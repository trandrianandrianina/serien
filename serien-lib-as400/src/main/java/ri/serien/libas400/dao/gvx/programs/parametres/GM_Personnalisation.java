/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs.parametres;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.ibm.as400.access.Record;

import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.ExtendRecord;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.article.GroupeArticles;
import ri.serien.libcommun.gescom.commun.article.ListeGroupesArticles;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.personnalisation.famille.IdFamille;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.IdSousFamille;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class GM_Personnalisation extends BaseGroupDB {
  /**
   * Constructeur
   * @param database
   */
  public GM_Personnalisation(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Retourne la liste des établissements
   * @return
   *
   */
  public String[] getListeEtablissements() {
    String request = "select paretb  from " + queryManager.getLibrary() + ".pgvmparm where partyp = 'DG' and paretb <> ''";
    ArrayList<GenericRecord> listrcd = queryManager.select(request);
    if ((listrcd == null) || (listrcd.size() == 0)) {
      return null;
    }
    
    int i = 0;
    String[] liste = new String[listrcd.size()];
    for (GenericRecord rcd : listrcd) {
      liste[i++] = (String) rcd.getField(0);
    }
    
    return liste;
  }
  
  /**
   * retourner la liste des groupes de famille article du catalogue d'un établissement
   */
  public ListeGroupesArticles chargerListeGroupesArticles(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return null;
    }
    
    ListeGroupesArticles listeGroupes = null;
    
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 FROM " + queryManager.getLibrary()
            + ".PGVMPARM WHERE PARTOP <> '9' and PARTYP = 'GR' and PARETB = '" + pIdEtablissement.getCodeEtablissement() + "'",
        "ri.serien.libas400.dao.gvx.database.Pgvmparm_ds_GR");
    if (listeRecord == null) {
      return listeGroupes;
    }
    
    listeGroupes = new ListeGroupesArticles();
    
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      GroupeArticles groupeArticles =
          new GroupeArticles(((String) record.getField("INDIC")).substring(5).trim(), ((String) record.getField("GRLIB")).trim());
      listeGroupes.rajouterUnGroupe(groupeArticles);
    }
    
    return listeGroupes;
  }
  
  /**
   * Retourner un groupe article sur la base d'un code et d'un établissement
   */
  public GroupeArticles lireUnGroupearticles(IdEtablissement pIdEtablissement, String pCodeGroupe) {
    if (pIdEtablissement == null || pCodeGroupe == null) {
      return null;
    }
    GroupeArticles groupeArticles = null;
    
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 FROM " + queryManager.getLibrary()
            + ".PGVMPARM WHERE PARTOP <> '9' and PARTYP = 'GR' and PARETB = '" + pIdEtablissement.getCodeEtablissement()
            + "' AND PARIND = '" + pCodeGroupe + "' ", "ri.serien.libas400.dao.gvx.database.Pgvmparm_ds_GR");
    if (listeRecord == null) {
      return groupeArticles;
    }
    
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      groupeArticles =
          new GroupeArticles(((String) record.getField("INDIC")).substring(5).trim(), ((String) record.getField("GRLIB")).trim());
    }
    
    return groupeArticles;
  }
  
  /**
   * Retourne la liste des familles.
   *
   **/
  public boolean getListFamilles(String etb, LinkedHashMap<String, String> liste) {
    if (liste == null) {
      return false;
    }
    
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary()
            + ".PGVMPARM where PARTOP <> '9' and PARTYP = 'FA' and PARETB = '" + etb + "'",
        "ri.serien.libas400.dao.gvx.database.Pgvmparm_ds_FA");
    if (listeRecord == null) {
      return false;
    }
    
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      liste.put(((String) record.getField("INDIC")).substring(5).trim(), ((String) record.getField("FALIB")).trim());
    }
    
    return true;
  }
  
  /**
   * Retourner une famille articles sur la base d'un établissement et d'un code famille
   */
  public Famille lireUneFamilleArticles(IdEtablissement pIdEtablissement, String pCodeFamille) {
    if (pIdEtablissement == null || pCodeFamille == null) {
      return null;
    }
    
    Famille famille = null;
    
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary()
            + ".PGVMPARM where PARTOP <> '9' and PARTYP = 'FA' and PARETB = '" + pIdEtablissement.getCodeEtablissement()
            + "' AND PARIND = '" + pCodeFamille + "' ", "ri.serien.libas400.dao.gvx.database.Pgvmparm_ds_FA");
    if (listeRecord == null) {
      return famille;
    }
    
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      IdFamille idFamille = IdFamille.getInstance(pIdEtablissement, ((String) record.getField("INDIC")).substring(5).trim());
      famille = new Famille(idFamille);
      famille.setLibelle(((String) record.getField("FALIB")).trim());
    }
    
    return famille;
  }
  
  /**
   * Retourne la liste des sous-familles du catalogue articles d'un établissement.
   **/
  public ListeSousFamille chargerListeSousFamille(IdEtablissement pIdEtablissement, String pFamille) {
    if (pIdEtablissement == null) {
      return null;
    }
    
    ListeSousFamille listeSousFamille = null;
    String conditionsFamille = "";
    if (pFamille != null && !pFamille.trim().equals("")) {
      conditionsFamille = " AND SUBSTR(PARIND, 1, 3) = '" + pFamille.trim() + "' ";
    }
    
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("SELECT PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 FROM " + queryManager.getLibrary()
            + ".PGVMPARM WHERE PARTOP <> '9' and PARTYP = 'SF' and PARETB = '" + pIdEtablissement.getCodeEtablissement() + "' "
            + conditionsFamille + " ", "ri.serien.libas400.dao.gvx.database.Pgvmparm_ds_SF");
    if (listeRecord == null) {
      return listeSousFamille;
    }
    
    listeSousFamille = new ListeSousFamille();
    
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      IdSousFamille idSousFamille = IdSousFamille.getInstance(pIdEtablissement, ((String) record.getField("INDIC")).substring(5).trim());
      SousFamille sousFamille = new SousFamille(idSousFamille);
      sousFamille.setLibelle(((String) record.getField("SFLIB")).trim());
      
      listeSousFamille.add(sousFamille);
    }
    
    return listeSousFamille;
  }
  
  /**
   * Retourner une sous-famille articles sur la base d'un établissement et d'un code sous famille
   */
  public SousFamille lireUneSousFamilleArticles(IdEtablissement pIdEtablissement, String pCodeSousFamille) {
    if (pIdEtablissement == null || pCodeSousFamille == null) {
      return null;
    }
    
    SousFamille sousFamille = null;
    
    String requete = "SELECT SUBSTR(PARZ2, 1, 30) AS LIBELLE FROM " + queryManager.getLibrary()
        + ".PGVMPARM WHERE PARTOP <> '9' AND PARTYP = 'SF' AND PARETB = '" + pIdEtablissement.getCodeEtablissement() + "' AND PARIND = '"
        + pCodeSousFamille + "' ";
    
    ArrayList<GenericRecord> listeRecords = queryManager.select(requete);
    
    if (listeRecords != null && listeRecords.size() == 1) {
      GenericRecord record = listeRecords.get(0);
      IdSousFamille idSousFamille = IdSousFamille.getInstance(pIdEtablissement, pCodeSousFamille);
      sousFamille = new SousFamille(idSousFamille);
      sousFamille.setLibelle(((String) record.getField("LIBELLE")).trim());
    }
    /*final ArrayList<Record> listeRecord =
        querymg.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + querymg.getLibrary()
            + ".PGVMPARM where PARTOP <> '9' and PARTYP = 'SF' and PARETB = '" + pIdEtablissement.getCodeEtablissement()
            + "' AND PARIND = '" + pCodeSousFamille + "' ", "ri.serien.libas400.dao.gvx.database.Pgvmparm_ds_SF");
    if (listeRecord == null) {
      return sousFamille;
    }
    
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      IdSousFamille idSousFamille = IdSousFamille.getInstance(pIdEtablissement, ((String) record.getField("INDIC")).substring(5).trim());
      sousFamille = new SousFamille(idSousFamille);
      if (record.getField("FALIB") != null) {
        sousFamille.setLibelle(((String) record.getField("FALIB")).trim());
      }
    }*/
    
    return sousFamille;
  }
  
  /**
   * Retourne la TVA de l'établissement pilote.
   *
   */
  public BigDecimal getTVA1EtablissementPilote() {
    BigDecimal tva = null;
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary()
            + ".PGVMPARM where PARTOP <> '9' and PARETB = '' and PARTYP = 'DG' and PARIND = ''",
        "ri.serien.libas400.dao.gvx.database.Pgvmparm_ds_DG");
    if (listeRecord == null) {
      return tva;
    }
    
    // On alimente la hashmap avec les enregistrements trouvés
    if (listeRecord.size() > 0) {
      ExtendRecord record = new ExtendRecord();
      record.setRecord(listeRecord.get(0));
      tva = (BigDecimal) record.getField("DGT01");
    }
    
    return tva;
  }
  
  /**
   * Libère les ressources.
   */
  @Override
  public void dispose() {
    queryManager = null;
    
  }
  
}
