/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.lot;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvx0025i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PIQTEA = 11;
  public static final int DECIMAL_PIQTEA = 3;
  public static final int SIZE_PIQTEL = 11;
  public static final int DECIMAL_PIQTEL = 3;
  public static final int SIZE_PIL001 = 25;
  public static final int SIZE_PIQ001 = 11;
  public static final int DECIMAL_PIQ001 = 3;
  public static final int SIZE_PIN001 = 11;
  public static final int DECIMAL_PIN001 = 3;
  public static final int SIZE_PIL002 = 25;
  public static final int SIZE_PIQ002 = 11;
  public static final int DECIMAL_PIQ002 = 3;
  public static final int SIZE_PIN002 = 11;
  public static final int DECIMAL_PIN002 = 3;
  public static final int SIZE_PIL003 = 25;
  public static final int SIZE_PIQ003 = 11;
  public static final int DECIMAL_PIQ003 = 3;
  public static final int SIZE_PIN003 = 11;
  public static final int DECIMAL_PIN003 = 3;
  public static final int SIZE_PIL004 = 25;
  public static final int SIZE_PIQ004 = 11;
  public static final int DECIMAL_PIQ004 = 3;
  public static final int SIZE_PIN004 = 11;
  public static final int DECIMAL_PIN004 = 3;
  public static final int SIZE_PIL005 = 25;
  public static final int SIZE_PIQ005 = 11;
  public static final int DECIMAL_PIQ005 = 3;
  public static final int SIZE_PIN005 = 11;
  public static final int DECIMAL_PIN005 = 3;
  public static final int SIZE_PIL006 = 25;
  public static final int SIZE_PIQ006 = 11;
  public static final int DECIMAL_PIQ006 = 3;
  public static final int SIZE_PIN006 = 11;
  public static final int DECIMAL_PIN006 = 3;
  public static final int SIZE_PIL007 = 25;
  public static final int SIZE_PIQ007 = 11;
  public static final int DECIMAL_PIQ007 = 3;
  public static final int SIZE_PIN007 = 11;
  public static final int DECIMAL_PIN007 = 3;
  public static final int SIZE_PIL008 = 25;
  public static final int SIZE_PIQ008 = 11;
  public static final int DECIMAL_PIQ008 = 3;
  public static final int SIZE_PIN008 = 11;
  public static final int DECIMAL_PIN008 = 3;
  public static final int SIZE_PIL009 = 25;
  public static final int SIZE_PIQ009 = 11;
  public static final int DECIMAL_PIQ009 = 3;
  public static final int SIZE_PIN009 = 11;
  public static final int DECIMAL_PIN009 = 3;
  public static final int SIZE_PIL010 = 25;
  public static final int SIZE_PIQ010 = 11;
  public static final int DECIMAL_PIQ010 = 3;
  public static final int SIZE_PIN010 = 11;
  public static final int DECIMAL_PIN010 = 3;
  public static final int SIZE_PIL011 = 25;
  public static final int SIZE_PIQ011 = 11;
  public static final int DECIMAL_PIQ011 = 3;
  public static final int SIZE_PIN011 = 11;
  public static final int DECIMAL_PIN011 = 3;
  public static final int SIZE_PIL012 = 25;
  public static final int SIZE_PIQ012 = 11;
  public static final int DECIMAL_PIQ012 = 3;
  public static final int SIZE_PIN012 = 11;
  public static final int DECIMAL_PIN012 = 3;
  public static final int SIZE_PIL013 = 25;
  public static final int SIZE_PIQ013 = 11;
  public static final int DECIMAL_PIQ013 = 3;
  public static final int SIZE_PIN013 = 11;
  public static final int DECIMAL_PIN013 = 3;
  public static final int SIZE_PIL014 = 25;
  public static final int SIZE_PIQ014 = 11;
  public static final int DECIMAL_PIQ014 = 3;
  public static final int SIZE_PIN014 = 11;
  public static final int DECIMAL_PIN014 = 3;
  public static final int SIZE_PIL015 = 25;
  public static final int SIZE_PIQ015 = 11;
  public static final int DECIMAL_PIQ015 = 3;
  public static final int SIZE_PIN015 = 11;
  public static final int DECIMAL_PIN015 = 3;
  public static final int SIZE_PIL016 = 25;
  public static final int SIZE_PIQ016 = 11;
  public static final int DECIMAL_PIQ016 = 3;
  public static final int SIZE_PIN016 = 11;
  public static final int DECIMAL_PIN016 = 3;
  public static final int SIZE_PIL017 = 25;
  public static final int SIZE_PIQ017 = 11;
  public static final int DECIMAL_PIQ017 = 3;
  public static final int SIZE_PIN017 = 11;
  public static final int DECIMAL_PIN017 = 3;
  public static final int SIZE_PIL018 = 25;
  public static final int SIZE_PIQ018 = 11;
  public static final int DECIMAL_PIQ018 = 3;
  public static final int SIZE_PIN018 = 11;
  public static final int DECIMAL_PIN018 = 3;
  public static final int SIZE_PIL019 = 25;
  public static final int SIZE_PIQ019 = 11;
  public static final int DECIMAL_PIQ019 = 3;
  public static final int SIZE_PIN019 = 11;
  public static final int DECIMAL_PIN019 = 3;
  public static final int SIZE_PIL020 = 25;
  public static final int SIZE_PIQ020 = 11;
  public static final int DECIMAL_PIQ020 = 3;
  public static final int SIZE_PIN020 = 11;
  public static final int DECIMAL_PIN020 = 3;
  public static final int SIZE_PIL021 = 25;
  public static final int SIZE_PIQ021 = 11;
  public static final int DECIMAL_PIQ021 = 3;
  public static final int SIZE_PIN021 = 11;
  public static final int DECIMAL_PIN021 = 3;
  public static final int SIZE_PIL022 = 25;
  public static final int SIZE_PIQ022 = 11;
  public static final int DECIMAL_PIQ022 = 3;
  public static final int SIZE_PIN022 = 11;
  public static final int DECIMAL_PIN022 = 3;
  public static final int SIZE_PIL023 = 25;
  public static final int SIZE_PIQ023 = 11;
  public static final int DECIMAL_PIQ023 = 3;
  public static final int SIZE_PIN023 = 11;
  public static final int DECIMAL_PIN023 = 3;
  public static final int SIZE_PIL024 = 25;
  public static final int SIZE_PIQ024 = 11;
  public static final int DECIMAL_PIQ024 = 3;
  public static final int SIZE_PIN024 = 11;
  public static final int DECIMAL_PIN024 = 3;
  public static final int SIZE_PIL025 = 25;
  public static final int SIZE_PIQ025 = 11;
  public static final int DECIMAL_PIQ025 = 3;
  public static final int SIZE_PIN025 = 11;
  public static final int DECIMAL_PIN025 = 3;
  public static final int SIZE_PIL026 = 25;
  public static final int SIZE_PIQ026 = 11;
  public static final int DECIMAL_PIQ026 = 3;
  public static final int SIZE_PIN026 = 11;
  public static final int DECIMAL_PIN026 = 3;
  public static final int SIZE_PIL027 = 25;
  public static final int SIZE_PIQ027 = 11;
  public static final int DECIMAL_PIQ027 = 3;
  public static final int SIZE_PIN027 = 11;
  public static final int DECIMAL_PIN027 = 3;
  public static final int SIZE_PIL028 = 25;
  public static final int SIZE_PIQ028 = 11;
  public static final int DECIMAL_PIQ028 = 3;
  public static final int SIZE_PIN028 = 11;
  public static final int DECIMAL_PIN028 = 3;
  public static final int SIZE_PIL029 = 25;
  public static final int SIZE_PIQ029 = 11;
  public static final int DECIMAL_PIQ029 = 3;
  public static final int SIZE_PIN029 = 11;
  public static final int DECIMAL_PIN029 = 3;
  public static final int SIZE_PIL030 = 25;
  public static final int SIZE_PIQ030 = 11;
  public static final int DECIMAL_PIQ030 = 3;
  public static final int SIZE_PIN030 = 11;
  public static final int DECIMAL_PIN030 = 3;
  public static final int SIZE_PIL031 = 25;
  public static final int SIZE_PIQ031 = 11;
  public static final int DECIMAL_PIQ031 = 3;
  public static final int SIZE_PIN031 = 11;
  public static final int DECIMAL_PIN031 = 3;
  public static final int SIZE_PIL032 = 25;
  public static final int SIZE_PIQ032 = 11;
  public static final int DECIMAL_PIQ032 = 3;
  public static final int SIZE_PIN032 = 11;
  public static final int DECIMAL_PIN032 = 3;
  public static final int SIZE_PIL033 = 25;
  public static final int SIZE_PIQ033 = 11;
  public static final int DECIMAL_PIQ033 = 3;
  public static final int SIZE_PIN033 = 11;
  public static final int DECIMAL_PIN033 = 3;
  public static final int SIZE_PIL034 = 25;
  public static final int SIZE_PIQ034 = 11;
  public static final int DECIMAL_PIQ034 = 3;
  public static final int SIZE_PIN034 = 11;
  public static final int DECIMAL_PIN034 = 3;
  public static final int SIZE_PIL035 = 25;
  public static final int SIZE_PIQ035 = 11;
  public static final int DECIMAL_PIQ035 = 3;
  public static final int SIZE_PIN035 = 11;
  public static final int DECIMAL_PIN035 = 3;
  public static final int SIZE_PIL036 = 25;
  public static final int SIZE_PIQ036 = 11;
  public static final int DECIMAL_PIQ036 = 3;
  public static final int SIZE_PIN036 = 11;
  public static final int DECIMAL_PIN036 = 3;
  public static final int SIZE_PIL037 = 25;
  public static final int SIZE_PIQ037 = 11;
  public static final int DECIMAL_PIQ037 = 3;
  public static final int SIZE_PIN037 = 11;
  public static final int DECIMAL_PIN037 = 3;
  public static final int SIZE_PIL038 = 25;
  public static final int SIZE_PIQ038 = 11;
  public static final int DECIMAL_PIQ038 = 3;
  public static final int SIZE_PIN038 = 11;
  public static final int DECIMAL_PIN038 = 3;
  public static final int SIZE_PIL039 = 25;
  public static final int SIZE_PIQ039 = 11;
  public static final int DECIMAL_PIQ039 = 3;
  public static final int SIZE_PIN039 = 11;
  public static final int DECIMAL_PIN039 = 3;
  public static final int SIZE_PIL040 = 25;
  public static final int SIZE_PIQ040 = 11;
  public static final int DECIMAL_PIQ040 = 3;
  public static final int SIZE_PIN040 = 11;
  public static final int DECIMAL_PIN040 = 3;
  public static final int SIZE_PIL041 = 25;
  public static final int SIZE_PIQ041 = 11;
  public static final int DECIMAL_PIQ041 = 3;
  public static final int SIZE_PIN041 = 11;
  public static final int DECIMAL_PIN041 = 3;
  public static final int SIZE_PIL042 = 25;
  public static final int SIZE_PIQ042 = 11;
  public static final int DECIMAL_PIQ042 = 3;
  public static final int SIZE_PIN042 = 11;
  public static final int DECIMAL_PIN042 = 3;
  public static final int SIZE_PIL043 = 25;
  public static final int SIZE_PIQ043 = 11;
  public static final int DECIMAL_PIQ043 = 3;
  public static final int SIZE_PIN043 = 11;
  public static final int DECIMAL_PIN043 = 3;
  public static final int SIZE_PIL044 = 25;
  public static final int SIZE_PIQ044 = 11;
  public static final int DECIMAL_PIQ044 = 3;
  public static final int SIZE_PIN044 = 11;
  public static final int DECIMAL_PIN044 = 3;
  public static final int SIZE_PIL045 = 25;
  public static final int SIZE_PIQ045 = 11;
  public static final int DECIMAL_PIQ045 = 3;
  public static final int SIZE_PIN045 = 11;
  public static final int DECIMAL_PIN045 = 3;
  public static final int SIZE_PIL046 = 25;
  public static final int SIZE_PIQ046 = 11;
  public static final int DECIMAL_PIQ046 = 3;
  public static final int SIZE_PIN046 = 11;
  public static final int DECIMAL_PIN046 = 3;
  public static final int SIZE_PIL047 = 25;
  public static final int SIZE_PIQ047 = 11;
  public static final int DECIMAL_PIQ047 = 3;
  public static final int SIZE_PIN047 = 11;
  public static final int DECIMAL_PIN047 = 3;
  public static final int SIZE_PIL048 = 25;
  public static final int SIZE_PIQ048 = 11;
  public static final int DECIMAL_PIQ048 = 3;
  public static final int SIZE_PIN048 = 11;
  public static final int DECIMAL_PIN048 = 3;
  public static final int SIZE_PIL049 = 25;
  public static final int SIZE_PIQ049 = 11;
  public static final int DECIMAL_PIQ049 = 3;
  public static final int SIZE_PIN049 = 11;
  public static final int DECIMAL_PIN049 = 3;
  public static final int SIZE_PIL050 = 25;
  public static final int SIZE_PIQ050 = 11;
  public static final int DECIMAL_PIQ050 = 3;
  public static final int SIZE_PIN050 = 11;
  public static final int DECIMAL_PIN050 = 3;
  public static final int SIZE_PIL051 = 25;
  public static final int SIZE_PIQ051 = 11;
  public static final int DECIMAL_PIQ051 = 3;
  public static final int SIZE_PIN051 = 11;
  public static final int DECIMAL_PIN051 = 3;
  public static final int SIZE_PIL052 = 25;
  public static final int SIZE_PIQ052 = 11;
  public static final int DECIMAL_PIQ052 = 3;
  public static final int SIZE_PIN052 = 11;
  public static final int DECIMAL_PIN052 = 3;
  public static final int SIZE_PIL053 = 25;
  public static final int SIZE_PIQ053 = 11;
  public static final int DECIMAL_PIQ053 = 3;
  public static final int SIZE_PIN053 = 11;
  public static final int DECIMAL_PIN053 = 3;
  public static final int SIZE_PIL054 = 25;
  public static final int SIZE_PIQ054 = 11;
  public static final int DECIMAL_PIQ054 = 3;
  public static final int SIZE_PIN054 = 11;
  public static final int DECIMAL_PIN054 = 3;
  public static final int SIZE_PIL055 = 25;
  public static final int SIZE_PIQ055 = 11;
  public static final int DECIMAL_PIQ055 = 3;
  public static final int SIZE_PIN055 = 11;
  public static final int DECIMAL_PIN055 = 3;
  public static final int SIZE_PIL056 = 25;
  public static final int SIZE_PIQ056 = 11;
  public static final int DECIMAL_PIQ056 = 3;
  public static final int SIZE_PIN056 = 11;
  public static final int DECIMAL_PIN056 = 3;
  public static final int SIZE_PIL057 = 25;
  public static final int SIZE_PIQ057 = 11;
  public static final int DECIMAL_PIQ057 = 3;
  public static final int SIZE_PIN057 = 11;
  public static final int DECIMAL_PIN057 = 3;
  public static final int SIZE_PIL058 = 25;
  public static final int SIZE_PIQ058 = 11;
  public static final int DECIMAL_PIQ058 = 3;
  public static final int SIZE_PIN058 = 11;
  public static final int DECIMAL_PIN058 = 3;
  public static final int SIZE_PIL059 = 25;
  public static final int SIZE_PIQ059 = 11;
  public static final int DECIMAL_PIQ059 = 3;
  public static final int SIZE_PIN059 = 11;
  public static final int DECIMAL_PIN059 = 3;
  public static final int SIZE_PIL060 = 25;
  public static final int SIZE_PIQ060 = 11;
  public static final int DECIMAL_PIQ060 = 3;
  public static final int SIZE_PIN060 = 11;
  public static final int DECIMAL_PIN060 = 3;
  public static final int SIZE_PIL061 = 25;
  public static final int SIZE_PIQ061 = 11;
  public static final int DECIMAL_PIQ061 = 3;
  public static final int SIZE_PIN061 = 11;
  public static final int DECIMAL_PIN061 = 3;
  public static final int SIZE_PIL062 = 25;
  public static final int SIZE_PIQ062 = 11;
  public static final int DECIMAL_PIQ062 = 3;
  public static final int SIZE_PIN062 = 11;
  public static final int DECIMAL_PIN062 = 3;
  public static final int SIZE_PIL063 = 25;
  public static final int SIZE_PIQ063 = 11;
  public static final int DECIMAL_PIQ063 = 3;
  public static final int SIZE_PIN063 = 11;
  public static final int DECIMAL_PIN063 = 3;
  public static final int SIZE_PIL064 = 25;
  public static final int SIZE_PIQ064 = 11;
  public static final int DECIMAL_PIQ064 = 3;
  public static final int SIZE_PIN064 = 11;
  public static final int DECIMAL_PIN064 = 3;
  public static final int SIZE_PIL065 = 25;
  public static final int SIZE_PIQ065 = 11;
  public static final int DECIMAL_PIQ065 = 3;
  public static final int SIZE_PIN065 = 11;
  public static final int DECIMAL_PIN065 = 3;
  public static final int SIZE_PIL066 = 25;
  public static final int SIZE_PIQ066 = 11;
  public static final int DECIMAL_PIQ066 = 3;
  public static final int SIZE_PIN066 = 11;
  public static final int DECIMAL_PIN066 = 3;
  public static final int SIZE_PIL067 = 25;
  public static final int SIZE_PIQ067 = 11;
  public static final int DECIMAL_PIQ067 = 3;
  public static final int SIZE_PIN067 = 11;
  public static final int DECIMAL_PIN067 = 3;
  public static final int SIZE_PIL068 = 25;
  public static final int SIZE_PIQ068 = 11;
  public static final int DECIMAL_PIQ068 = 3;
  public static final int SIZE_PIN068 = 11;
  public static final int DECIMAL_PIN068 = 3;
  public static final int SIZE_PIL069 = 25;
  public static final int SIZE_PIQ069 = 11;
  public static final int DECIMAL_PIQ069 = 3;
  public static final int SIZE_PIN069 = 11;
  public static final int DECIMAL_PIN069 = 3;
  public static final int SIZE_PIL070 = 25;
  public static final int SIZE_PIQ070 = 11;
  public static final int DECIMAL_PIQ070 = 3;
  public static final int SIZE_PIN070 = 11;
  public static final int DECIMAL_PIN070 = 3;
  public static final int SIZE_PIL071 = 25;
  public static final int SIZE_PIQ071 = 11;
  public static final int DECIMAL_PIQ071 = 3;
  public static final int SIZE_PIN071 = 11;
  public static final int DECIMAL_PIN071 = 3;
  public static final int SIZE_PIL072 = 25;
  public static final int SIZE_PIQ072 = 11;
  public static final int DECIMAL_PIQ072 = 3;
  public static final int SIZE_PIN072 = 11;
  public static final int DECIMAL_PIN072 = 3;
  public static final int SIZE_PIL073 = 25;
  public static final int SIZE_PIQ073 = 11;
  public static final int DECIMAL_PIQ073 = 3;
  public static final int SIZE_PIN073 = 11;
  public static final int DECIMAL_PIN073 = 3;
  public static final int SIZE_PIL074 = 25;
  public static final int SIZE_PIQ074 = 11;
  public static final int DECIMAL_PIQ074 = 3;
  public static final int SIZE_PIN074 = 11;
  public static final int DECIMAL_PIN074 = 3;
  public static final int SIZE_PIL075 = 25;
  public static final int SIZE_PIQ075 = 11;
  public static final int DECIMAL_PIQ075 = 3;
  public static final int SIZE_PIN075 = 11;
  public static final int DECIMAL_PIN075 = 3;
  public static final int SIZE_PIL076 = 25;
  public static final int SIZE_PIQ076 = 11;
  public static final int DECIMAL_PIQ076 = 3;
  public static final int SIZE_PIN076 = 11;
  public static final int DECIMAL_PIN076 = 3;
  public static final int SIZE_PIL077 = 25;
  public static final int SIZE_PIQ077 = 11;
  public static final int DECIMAL_PIQ077 = 3;
  public static final int SIZE_PIN077 = 11;
  public static final int DECIMAL_PIN077 = 3;
  public static final int SIZE_PIL078 = 25;
  public static final int SIZE_PIQ078 = 11;
  public static final int DECIMAL_PIQ078 = 3;
  public static final int SIZE_PIN078 = 11;
  public static final int DECIMAL_PIN078 = 3;
  public static final int SIZE_PIL079 = 25;
  public static final int SIZE_PIQ079 = 11;
  public static final int DECIMAL_PIQ079 = 3;
  public static final int SIZE_PIN079 = 11;
  public static final int DECIMAL_PIN079 = 3;
  public static final int SIZE_PIL080 = 25;
  public static final int SIZE_PIQ080 = 11;
  public static final int DECIMAL_PIQ080 = 3;
  public static final int SIZE_PIN080 = 11;
  public static final int DECIMAL_PIN080 = 3;
  public static final int SIZE_PIL081 = 25;
  public static final int SIZE_PIQ081 = 11;
  public static final int DECIMAL_PIQ081 = 3;
  public static final int SIZE_PIN081 = 11;
  public static final int DECIMAL_PIN081 = 3;
  public static final int SIZE_PIL082 = 25;
  public static final int SIZE_PIQ082 = 11;
  public static final int DECIMAL_PIQ082 = 3;
  public static final int SIZE_PIN082 = 11;
  public static final int DECIMAL_PIN082 = 3;
  public static final int SIZE_PIL083 = 25;
  public static final int SIZE_PIQ083 = 11;
  public static final int DECIMAL_PIQ083 = 3;
  public static final int SIZE_PIN083 = 11;
  public static final int DECIMAL_PIN083 = 3;
  public static final int SIZE_PIL084 = 25;
  public static final int SIZE_PIQ084 = 11;
  public static final int DECIMAL_PIQ084 = 3;
  public static final int SIZE_PIN084 = 11;
  public static final int DECIMAL_PIN084 = 3;
  public static final int SIZE_PIL085 = 25;
  public static final int SIZE_PIQ085 = 11;
  public static final int DECIMAL_PIQ085 = 3;
  public static final int SIZE_PIN085 = 11;
  public static final int DECIMAL_PIN085 = 3;
  public static final int SIZE_PIL086 = 25;
  public static final int SIZE_PIQ086 = 11;
  public static final int DECIMAL_PIQ086 = 3;
  public static final int SIZE_PIN086 = 11;
  public static final int DECIMAL_PIN086 = 3;
  public static final int SIZE_PIL087 = 25;
  public static final int SIZE_PIQ087 = 11;
  public static final int DECIMAL_PIQ087 = 3;
  public static final int SIZE_PIN087 = 11;
  public static final int DECIMAL_PIN087 = 3;
  public static final int SIZE_PIL088 = 25;
  public static final int SIZE_PIQ088 = 11;
  public static final int DECIMAL_PIQ088 = 3;
  public static final int SIZE_PIN088 = 11;
  public static final int DECIMAL_PIN088 = 3;
  public static final int SIZE_PIL089 = 25;
  public static final int SIZE_PIQ089 = 11;
  public static final int DECIMAL_PIQ089 = 3;
  public static final int SIZE_PIN089 = 11;
  public static final int DECIMAL_PIN089 = 3;
  public static final int SIZE_PIL090 = 25;
  public static final int SIZE_PIQ090 = 11;
  public static final int DECIMAL_PIQ090 = 3;
  public static final int SIZE_PIN090 = 11;
  public static final int DECIMAL_PIN090 = 3;
  public static final int SIZE_PIL091 = 25;
  public static final int SIZE_PIQ091 = 11;
  public static final int DECIMAL_PIQ091 = 3;
  public static final int SIZE_PIN091 = 11;
  public static final int DECIMAL_PIN091 = 3;
  public static final int SIZE_PIL092 = 25;
  public static final int SIZE_PIQ092 = 11;
  public static final int DECIMAL_PIQ092 = 3;
  public static final int SIZE_PIN092 = 11;
  public static final int DECIMAL_PIN092 = 3;
  public static final int SIZE_PIL093 = 25;
  public static final int SIZE_PIQ093 = 11;
  public static final int DECIMAL_PIQ093 = 3;
  public static final int SIZE_PIN093 = 11;
  public static final int DECIMAL_PIN093 = 3;
  public static final int SIZE_PIL094 = 25;
  public static final int SIZE_PIQ094 = 11;
  public static final int DECIMAL_PIQ094 = 3;
  public static final int SIZE_PIN094 = 11;
  public static final int DECIMAL_PIN094 = 3;
  public static final int SIZE_PIL095 = 25;
  public static final int SIZE_PIQ095 = 11;
  public static final int DECIMAL_PIQ095 = 3;
  public static final int SIZE_PIN095 = 11;
  public static final int DECIMAL_PIN095 = 3;
  public static final int SIZE_PIL096 = 25;
  public static final int SIZE_PIQ096 = 11;
  public static final int DECIMAL_PIQ096 = 3;
  public static final int SIZE_PIN096 = 11;
  public static final int DECIMAL_PIN096 = 3;
  public static final int SIZE_PIL097 = 25;
  public static final int SIZE_PIQ097 = 11;
  public static final int DECIMAL_PIQ097 = 3;
  public static final int SIZE_PIN097 = 11;
  public static final int DECIMAL_PIN097 = 3;
  public static final int SIZE_PIL098 = 25;
  public static final int SIZE_PIQ098 = 11;
  public static final int DECIMAL_PIQ098 = 3;
  public static final int SIZE_PIN098 = 11;
  public static final int DECIMAL_PIN098 = 3;
  public static final int SIZE_PIL099 = 25;
  public static final int SIZE_PIQ099 = 11;
  public static final int DECIMAL_PIQ099 = 3;
  public static final int SIZE_PIN099 = 11;
  public static final int DECIMAL_PIN099 = 3;
  public static final int SIZE_PIL100 = 25;
  public static final int SIZE_PIQ100 = 11;
  public static final int DECIMAL_PIQ100 = 3;
  public static final int SIZE_PIN100 = 11;
  public static final int DECIMAL_PIN100 = 3;
  public static final int SIZE_TOTALE_DS = 4767;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINLI = 5;
  public static final int VAR_PIART = 6;
  public static final int VAR_PIQTEA = 7;
  public static final int VAR_PIQTEL = 8;
  public static final int VAR_PIL001 = 9;
  public static final int VAR_PIQ001 = 10;
  public static final int VAR_PIN001 = 11;
  public static final int VAR_PIL002 = 12;
  public static final int VAR_PIQ002 = 13;
  public static final int VAR_PIN002 = 14;
  public static final int VAR_PIL003 = 15;
  public static final int VAR_PIQ003 = 16;
  public static final int VAR_PIN003 = 17;
  public static final int VAR_PIL004 = 18;
  public static final int VAR_PIQ004 = 19;
  public static final int VAR_PIN004 = 20;
  public static final int VAR_PIL005 = 21;
  public static final int VAR_PIQ005 = 22;
  public static final int VAR_PIN005 = 23;
  public static final int VAR_PIL006 = 24;
  public static final int VAR_PIQ006 = 25;
  public static final int VAR_PIN006 = 26;
  public static final int VAR_PIL007 = 27;
  public static final int VAR_PIQ007 = 28;
  public static final int VAR_PIN007 = 29;
  public static final int VAR_PIL008 = 30;
  public static final int VAR_PIQ008 = 31;
  public static final int VAR_PIN008 = 32;
  public static final int VAR_PIL009 = 33;
  public static final int VAR_PIQ009 = 34;
  public static final int VAR_PIN009 = 35;
  public static final int VAR_PIL010 = 36;
  public static final int VAR_PIQ010 = 37;
  public static final int VAR_PIN010 = 38;
  public static final int VAR_PIL011 = 39;
  public static final int VAR_PIQ011 = 40;
  public static final int VAR_PIN011 = 41;
  public static final int VAR_PIL012 = 42;
  public static final int VAR_PIQ012 = 43;
  public static final int VAR_PIN012 = 44;
  public static final int VAR_PIL013 = 45;
  public static final int VAR_PIQ013 = 46;
  public static final int VAR_PIN013 = 47;
  public static final int VAR_PIL014 = 48;
  public static final int VAR_PIQ014 = 49;
  public static final int VAR_PIN014 = 50;
  public static final int VAR_PIL015 = 51;
  public static final int VAR_PIQ015 = 52;
  public static final int VAR_PIN015 = 53;
  public static final int VAR_PIL016 = 54;
  public static final int VAR_PIQ016 = 55;
  public static final int VAR_PIN016 = 56;
  public static final int VAR_PIL017 = 57;
  public static final int VAR_PIQ017 = 58;
  public static final int VAR_PIN017 = 59;
  public static final int VAR_PIL018 = 60;
  public static final int VAR_PIQ018 = 61;
  public static final int VAR_PIN018 = 62;
  public static final int VAR_PIL019 = 63;
  public static final int VAR_PIQ019 = 64;
  public static final int VAR_PIN019 = 65;
  public static final int VAR_PIL020 = 66;
  public static final int VAR_PIQ020 = 67;
  public static final int VAR_PIN020 = 68;
  public static final int VAR_PIL021 = 69;
  public static final int VAR_PIQ021 = 70;
  public static final int VAR_PIN021 = 71;
  public static final int VAR_PIL022 = 72;
  public static final int VAR_PIQ022 = 73;
  public static final int VAR_PIN022 = 74;
  public static final int VAR_PIL023 = 75;
  public static final int VAR_PIQ023 = 76;
  public static final int VAR_PIN023 = 77;
  public static final int VAR_PIL024 = 78;
  public static final int VAR_PIQ024 = 79;
  public static final int VAR_PIN024 = 80;
  public static final int VAR_PIL025 = 81;
  public static final int VAR_PIQ025 = 82;
  public static final int VAR_PIN025 = 83;
  public static final int VAR_PIL026 = 84;
  public static final int VAR_PIQ026 = 85;
  public static final int VAR_PIN026 = 86;
  public static final int VAR_PIL027 = 87;
  public static final int VAR_PIQ027 = 88;
  public static final int VAR_PIN027 = 89;
  public static final int VAR_PIL028 = 90;
  public static final int VAR_PIQ028 = 91;
  public static final int VAR_PIN028 = 92;
  public static final int VAR_PIL029 = 93;
  public static final int VAR_PIQ029 = 94;
  public static final int VAR_PIN029 = 95;
  public static final int VAR_PIL030 = 96;
  public static final int VAR_PIQ030 = 97;
  public static final int VAR_PIN030 = 98;
  public static final int VAR_PIL031 = 99;
  public static final int VAR_PIQ031 = 100;
  public static final int VAR_PIN031 = 101;
  public static final int VAR_PIL032 = 102;
  public static final int VAR_PIQ032 = 103;
  public static final int VAR_PIN032 = 104;
  public static final int VAR_PIL033 = 105;
  public static final int VAR_PIQ033 = 106;
  public static final int VAR_PIN033 = 107;
  public static final int VAR_PIL034 = 108;
  public static final int VAR_PIQ034 = 109;
  public static final int VAR_PIN034 = 110;
  public static final int VAR_PIL035 = 111;
  public static final int VAR_PIQ035 = 112;
  public static final int VAR_PIN035 = 113;
  public static final int VAR_PIL036 = 114;
  public static final int VAR_PIQ036 = 115;
  public static final int VAR_PIN036 = 116;
  public static final int VAR_PIL037 = 117;
  public static final int VAR_PIQ037 = 118;
  public static final int VAR_PIN037 = 119;
  public static final int VAR_PIL038 = 120;
  public static final int VAR_PIQ038 = 121;
  public static final int VAR_PIN038 = 122;
  public static final int VAR_PIL039 = 123;
  public static final int VAR_PIQ039 = 124;
  public static final int VAR_PIN039 = 125;
  public static final int VAR_PIL040 = 126;
  public static final int VAR_PIQ040 = 127;
  public static final int VAR_PIN040 = 128;
  public static final int VAR_PIL041 = 129;
  public static final int VAR_PIQ041 = 130;
  public static final int VAR_PIN041 = 131;
  public static final int VAR_PIL042 = 132;
  public static final int VAR_PIQ042 = 133;
  public static final int VAR_PIN042 = 134;
  public static final int VAR_PIL043 = 135;
  public static final int VAR_PIQ043 = 136;
  public static final int VAR_PIN043 = 137;
  public static final int VAR_PIL044 = 138;
  public static final int VAR_PIQ044 = 139;
  public static final int VAR_PIN044 = 140;
  public static final int VAR_PIL045 = 141;
  public static final int VAR_PIQ045 = 142;
  public static final int VAR_PIN045 = 143;
  public static final int VAR_PIL046 = 144;
  public static final int VAR_PIQ046 = 145;
  public static final int VAR_PIN046 = 146;
  public static final int VAR_PIL047 = 147;
  public static final int VAR_PIQ047 = 148;
  public static final int VAR_PIN047 = 149;
  public static final int VAR_PIL048 = 150;
  public static final int VAR_PIQ048 = 151;
  public static final int VAR_PIN048 = 152;
  public static final int VAR_PIL049 = 153;
  public static final int VAR_PIQ049 = 154;
  public static final int VAR_PIN049 = 155;
  public static final int VAR_PIL050 = 156;
  public static final int VAR_PIQ050 = 157;
  public static final int VAR_PIN050 = 158;
  public static final int VAR_PIL051 = 159;
  public static final int VAR_PIQ051 = 160;
  public static final int VAR_PIN051 = 161;
  public static final int VAR_PIL052 = 162;
  public static final int VAR_PIQ052 = 163;
  public static final int VAR_PIN052 = 164;
  public static final int VAR_PIL053 = 165;
  public static final int VAR_PIQ053 = 166;
  public static final int VAR_PIN053 = 167;
  public static final int VAR_PIL054 = 168;
  public static final int VAR_PIQ054 = 169;
  public static final int VAR_PIN054 = 170;
  public static final int VAR_PIL055 = 171;
  public static final int VAR_PIQ055 = 172;
  public static final int VAR_PIN055 = 173;
  public static final int VAR_PIL056 = 174;
  public static final int VAR_PIQ056 = 175;
  public static final int VAR_PIN056 = 176;
  public static final int VAR_PIL057 = 177;
  public static final int VAR_PIQ057 = 178;
  public static final int VAR_PIN057 = 179;
  public static final int VAR_PIL058 = 180;
  public static final int VAR_PIQ058 = 181;
  public static final int VAR_PIN058 = 182;
  public static final int VAR_PIL059 = 183;
  public static final int VAR_PIQ059 = 184;
  public static final int VAR_PIN059 = 185;
  public static final int VAR_PIL060 = 186;
  public static final int VAR_PIQ060 = 187;
  public static final int VAR_PIN060 = 188;
  public static final int VAR_PIL061 = 189;
  public static final int VAR_PIQ061 = 190;
  public static final int VAR_PIN061 = 191;
  public static final int VAR_PIL062 = 192;
  public static final int VAR_PIQ062 = 193;
  public static final int VAR_PIN062 = 194;
  public static final int VAR_PIL063 = 195;
  public static final int VAR_PIQ063 = 196;
  public static final int VAR_PIN063 = 197;
  public static final int VAR_PIL064 = 198;
  public static final int VAR_PIQ064 = 199;
  public static final int VAR_PIN064 = 200;
  public static final int VAR_PIL065 = 201;
  public static final int VAR_PIQ065 = 202;
  public static final int VAR_PIN065 = 203;
  public static final int VAR_PIL066 = 204;
  public static final int VAR_PIQ066 = 205;
  public static final int VAR_PIN066 = 206;
  public static final int VAR_PIL067 = 207;
  public static final int VAR_PIQ067 = 208;
  public static final int VAR_PIN067 = 209;
  public static final int VAR_PIL068 = 210;
  public static final int VAR_PIQ068 = 211;
  public static final int VAR_PIN068 = 212;
  public static final int VAR_PIL069 = 213;
  public static final int VAR_PIQ069 = 214;
  public static final int VAR_PIN069 = 215;
  public static final int VAR_PIL070 = 216;
  public static final int VAR_PIQ070 = 217;
  public static final int VAR_PIN070 = 218;
  public static final int VAR_PIL071 = 219;
  public static final int VAR_PIQ071 = 220;
  public static final int VAR_PIN071 = 221;
  public static final int VAR_PIL072 = 222;
  public static final int VAR_PIQ072 = 223;
  public static final int VAR_PIN072 = 224;
  public static final int VAR_PIL073 = 225;
  public static final int VAR_PIQ073 = 226;
  public static final int VAR_PIN073 = 227;
  public static final int VAR_PIL074 = 228;
  public static final int VAR_PIQ074 = 229;
  public static final int VAR_PIN074 = 230;
  public static final int VAR_PIL075 = 231;
  public static final int VAR_PIQ075 = 232;
  public static final int VAR_PIN075 = 233;
  public static final int VAR_PIL076 = 234;
  public static final int VAR_PIQ076 = 235;
  public static final int VAR_PIN076 = 236;
  public static final int VAR_PIL077 = 237;
  public static final int VAR_PIQ077 = 238;
  public static final int VAR_PIN077 = 239;
  public static final int VAR_PIL078 = 240;
  public static final int VAR_PIQ078 = 241;
  public static final int VAR_PIN078 = 242;
  public static final int VAR_PIL079 = 243;
  public static final int VAR_PIQ079 = 244;
  public static final int VAR_PIN079 = 245;
  public static final int VAR_PIL080 = 246;
  public static final int VAR_PIQ080 = 247;
  public static final int VAR_PIN080 = 248;
  public static final int VAR_PIL081 = 249;
  public static final int VAR_PIQ081 = 250;
  public static final int VAR_PIN081 = 251;
  public static final int VAR_PIL082 = 252;
  public static final int VAR_PIQ082 = 253;
  public static final int VAR_PIN082 = 254;
  public static final int VAR_PIL083 = 255;
  public static final int VAR_PIQ083 = 256;
  public static final int VAR_PIN083 = 257;
  public static final int VAR_PIL084 = 258;
  public static final int VAR_PIQ084 = 259;
  public static final int VAR_PIN084 = 260;
  public static final int VAR_PIL085 = 261;
  public static final int VAR_PIQ085 = 262;
  public static final int VAR_PIN085 = 263;
  public static final int VAR_PIL086 = 264;
  public static final int VAR_PIQ086 = 265;
  public static final int VAR_PIN086 = 266;
  public static final int VAR_PIL087 = 267;
  public static final int VAR_PIQ087 = 268;
  public static final int VAR_PIN087 = 269;
  public static final int VAR_PIL088 = 270;
  public static final int VAR_PIQ088 = 271;
  public static final int VAR_PIN088 = 272;
  public static final int VAR_PIL089 = 273;
  public static final int VAR_PIQ089 = 274;
  public static final int VAR_PIN089 = 275;
  public static final int VAR_PIL090 = 276;
  public static final int VAR_PIQ090 = 277;
  public static final int VAR_PIN090 = 278;
  public static final int VAR_PIL091 = 279;
  public static final int VAR_PIQ091 = 280;
  public static final int VAR_PIN091 = 281;
  public static final int VAR_PIL092 = 282;
  public static final int VAR_PIQ092 = 283;
  public static final int VAR_PIN092 = 284;
  public static final int VAR_PIL093 = 285;
  public static final int VAR_PIQ093 = 286;
  public static final int VAR_PIN093 = 287;
  public static final int VAR_PIL094 = 288;
  public static final int VAR_PIQ094 = 289;
  public static final int VAR_PIN094 = 290;
  public static final int VAR_PIL095 = 291;
  public static final int VAR_PIQ095 = 292;
  public static final int VAR_PIN095 = 293;
  public static final int VAR_PIL096 = 294;
  public static final int VAR_PIQ096 = 295;
  public static final int VAR_PIN096 = 296;
  public static final int VAR_PIL097 = 297;
  public static final int VAR_PIQ097 = 298;
  public static final int VAR_PIN097 = 299;
  public static final int VAR_PIL098 = 300;
  public static final int VAR_PIQ098 = 301;
  public static final int VAR_PIN098 = 302;
  public static final int VAR_PIL099 = 303;
  public static final int VAR_PIQ099 = 304;
  public static final int VAR_PIN099 = 305;
  public static final int VAR_PIL100 = 306;
  public static final int VAR_PIQ100 = 307;
  public static final int VAR_PIN100 = 308;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "E"
  private String pietb = ""; // Code Etablissement
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de Ligne
  private String piart = ""; // Article
  private BigDecimal piqtea = BigDecimal.ZERO; // Quantité article
  private BigDecimal piqtel = BigDecimal.ZERO; // Quantité totale lot
  private String pil001 = ""; // N° de lot 001
  private BigDecimal piq001 = BigDecimal.ZERO; // Qte initiale lot 001
  private BigDecimal pin001 = BigDecimal.ZERO; // Qte nouvelle lot 001
  private String pil002 = ""; // N° de lot 002
  private BigDecimal piq002 = BigDecimal.ZERO; // Qte initiale lot 002
  private BigDecimal pin002 = BigDecimal.ZERO; // Qte nouvelle lot 002
  private String pil003 = ""; // N° de lot 003
  private BigDecimal piq003 = BigDecimal.ZERO; // Qte initiale lot 003
  private BigDecimal pin003 = BigDecimal.ZERO; // Qte nouvelle lot 003
  private String pil004 = ""; // N° de lot 004
  private BigDecimal piq004 = BigDecimal.ZERO; // Qte initiale lot 004
  private BigDecimal pin004 = BigDecimal.ZERO; // Qte nouvelle lot 004
  private String pil005 = ""; // N° de lot 005
  private BigDecimal piq005 = BigDecimal.ZERO; // Qte initiale lot 005
  private BigDecimal pin005 = BigDecimal.ZERO; // Qte nouvelle lot 005
  private String pil006 = ""; // N° de lot 006
  private BigDecimal piq006 = BigDecimal.ZERO; // Qte initiale lot 006
  private BigDecimal pin006 = BigDecimal.ZERO; // Qte nouvelle lot 006
  private String pil007 = ""; // N° de lot 007
  private BigDecimal piq007 = BigDecimal.ZERO; // Qte initiale lot 007
  private BigDecimal pin007 = BigDecimal.ZERO; // Qte nouvelle lot 007
  private String pil008 = ""; // N° de lot 008
  private BigDecimal piq008 = BigDecimal.ZERO; // Qte initiale lot 008
  private BigDecimal pin008 = BigDecimal.ZERO; // Qte nouvelle lot 008
  private String pil009 = ""; // N° de lot 009
  private BigDecimal piq009 = BigDecimal.ZERO; // Qte initiale lot 009
  private BigDecimal pin009 = BigDecimal.ZERO; // Qte nouvelle lot 009
  private String pil010 = ""; // N° de lot 010
  private BigDecimal piq010 = BigDecimal.ZERO; // Qte initiale lot 010
  private BigDecimal pin010 = BigDecimal.ZERO; // Qte nouvelle lot 010
  private String pil011 = ""; // N° de lot 011
  private BigDecimal piq011 = BigDecimal.ZERO; // Qte initiale lot 011
  private BigDecimal pin011 = BigDecimal.ZERO; // Qte nouvelle lot 011
  private String pil012 = ""; // N° de lot 012
  private BigDecimal piq012 = BigDecimal.ZERO; // Qte initiale lot 012
  private BigDecimal pin012 = BigDecimal.ZERO; // Qte nouvelle lot 012
  private String pil013 = ""; // N° de lot 013
  private BigDecimal piq013 = BigDecimal.ZERO; // Qte initiale lot 013
  private BigDecimal pin013 = BigDecimal.ZERO; // Qte nouvelle lot 013
  private String pil014 = ""; // N° de lot 014
  private BigDecimal piq014 = BigDecimal.ZERO; // Qte initiale lot 014
  private BigDecimal pin014 = BigDecimal.ZERO; // Qte nouvelle lot 014
  private String pil015 = ""; // N° de lot 015
  private BigDecimal piq015 = BigDecimal.ZERO; // Qte initiale lot 015
  private BigDecimal pin015 = BigDecimal.ZERO; // Qte nouvelle lot 015
  private String pil016 = ""; // N° de lot 016
  private BigDecimal piq016 = BigDecimal.ZERO; // Qte initiale lot 016
  private BigDecimal pin016 = BigDecimal.ZERO; // Qte nouvelle lot 016
  private String pil017 = ""; // N° de lot 017
  private BigDecimal piq017 = BigDecimal.ZERO; // Qte initiale lot 017
  private BigDecimal pin017 = BigDecimal.ZERO; // Qte nouvelle lot 017
  private String pil018 = ""; // N° de lot 018
  private BigDecimal piq018 = BigDecimal.ZERO; // Qte initiale lot 018
  private BigDecimal pin018 = BigDecimal.ZERO; // Qte nouvelle lot 018
  private String pil019 = ""; // N° de lot 019
  private BigDecimal piq019 = BigDecimal.ZERO; // Qte initiale lot 019
  private BigDecimal pin019 = BigDecimal.ZERO; // Qte nouvelle lot 019
  private String pil020 = ""; // N° de lot 020
  private BigDecimal piq020 = BigDecimal.ZERO; // Qte initiale lot 020
  private BigDecimal pin020 = BigDecimal.ZERO; // Qte nouvelle lot 020
  private String pil021 = ""; // N° de lot 021
  private BigDecimal piq021 = BigDecimal.ZERO; // Qte initiale lot 021
  private BigDecimal pin021 = BigDecimal.ZERO; // Qte nouvelle lot 021
  private String pil022 = ""; // N° de lot 022
  private BigDecimal piq022 = BigDecimal.ZERO; // Qte initiale lot 022
  private BigDecimal pin022 = BigDecimal.ZERO; // Qte nouvelle lot 022
  private String pil023 = ""; // N° de lot 023
  private BigDecimal piq023 = BigDecimal.ZERO; // Qte initiale lot 023
  private BigDecimal pin023 = BigDecimal.ZERO; // Qte nouvelle lot 023
  private String pil024 = ""; // N° de lot 024
  private BigDecimal piq024 = BigDecimal.ZERO; // Qte initiale lot 024
  private BigDecimal pin024 = BigDecimal.ZERO; // Qte nouvelle lot 024
  private String pil025 = ""; // N° de lot 025
  private BigDecimal piq025 = BigDecimal.ZERO; // Qte initiale lot 025
  private BigDecimal pin025 = BigDecimal.ZERO; // Qte nouvelle lot 025
  private String pil026 = ""; // N° de lot 026
  private BigDecimal piq026 = BigDecimal.ZERO; // Qte initiale lot 026
  private BigDecimal pin026 = BigDecimal.ZERO; // Qte nouvelle lot 026
  private String pil027 = ""; // N° de lot 027
  private BigDecimal piq027 = BigDecimal.ZERO; // Qte initiale lot 027
  private BigDecimal pin027 = BigDecimal.ZERO; // Qte nouvelle lot 027
  private String pil028 = ""; // N° de lot 028
  private BigDecimal piq028 = BigDecimal.ZERO; // Qte initiale lot 028
  private BigDecimal pin028 = BigDecimal.ZERO; // Qte nouvelle lot 028
  private String pil029 = ""; // N° de lot 029
  private BigDecimal piq029 = BigDecimal.ZERO; // Qte initiale lot 029
  private BigDecimal pin029 = BigDecimal.ZERO; // Qte nouvelle lot 029
  private String pil030 = ""; // N° de lot 030
  private BigDecimal piq030 = BigDecimal.ZERO; // Qte initiale lot 030
  private BigDecimal pin030 = BigDecimal.ZERO; // Qte nouvelle lot 030
  private String pil031 = ""; // N° de lot 031
  private BigDecimal piq031 = BigDecimal.ZERO; // Qte initiale lot 031
  private BigDecimal pin031 = BigDecimal.ZERO; // Qte nouvelle lot 031
  private String pil032 = ""; // N° de lot 032
  private BigDecimal piq032 = BigDecimal.ZERO; // Qte initiale lot 032
  private BigDecimal pin032 = BigDecimal.ZERO; // Qte nouvelle lot 032
  private String pil033 = ""; // N° de lot 033
  private BigDecimal piq033 = BigDecimal.ZERO; // Qte initiale lot 033
  private BigDecimal pin033 = BigDecimal.ZERO; // Qte nouvelle lot 033
  private String pil034 = ""; // N° de lot 034
  private BigDecimal piq034 = BigDecimal.ZERO; // Qte initiale lot 034
  private BigDecimal pin034 = BigDecimal.ZERO; // Qte nouvelle lot 034
  private String pil035 = ""; // N° de lot 035
  private BigDecimal piq035 = BigDecimal.ZERO; // Qte initiale lot 035
  private BigDecimal pin035 = BigDecimal.ZERO; // Qte nouvelle lot 035
  private String pil036 = ""; // N° de lot 036
  private BigDecimal piq036 = BigDecimal.ZERO; // Qte initiale lot 036
  private BigDecimal pin036 = BigDecimal.ZERO; // Qte nouvelle lot 036
  private String pil037 = ""; // N° de lot 037
  private BigDecimal piq037 = BigDecimal.ZERO; // Qte initiale lot 037
  private BigDecimal pin037 = BigDecimal.ZERO; // Qte nouvelle lot 037
  private String pil038 = ""; // N° de lot 038
  private BigDecimal piq038 = BigDecimal.ZERO; // Qte initiale lot 038
  private BigDecimal pin038 = BigDecimal.ZERO; // Qte nouvelle lot 038
  private String pil039 = ""; // N° de lot 039
  private BigDecimal piq039 = BigDecimal.ZERO; // Qte initiale lot 039
  private BigDecimal pin039 = BigDecimal.ZERO; // Qte nouvelle lot 039
  private String pil040 = ""; // N° de lot 040
  private BigDecimal piq040 = BigDecimal.ZERO; // Qte initiale lot 040
  private BigDecimal pin040 = BigDecimal.ZERO; // Qte nouvelle lot 040
  private String pil041 = ""; // N° de lot 041
  private BigDecimal piq041 = BigDecimal.ZERO; // Qte initiale lot 041
  private BigDecimal pin041 = BigDecimal.ZERO; // Qte nouvelle lot 041
  private String pil042 = ""; // N° de lot 042
  private BigDecimal piq042 = BigDecimal.ZERO; // Qte initiale lot 042
  private BigDecimal pin042 = BigDecimal.ZERO; // Qte nouvelle lot 042
  private String pil043 = ""; // N° de lot 043
  private BigDecimal piq043 = BigDecimal.ZERO; // Qte initiale lot 043
  private BigDecimal pin043 = BigDecimal.ZERO; // Qte nouvelle lot 043
  private String pil044 = ""; // N° de lot 044
  private BigDecimal piq044 = BigDecimal.ZERO; // Qte initiale lot 044
  private BigDecimal pin044 = BigDecimal.ZERO; // Qte nouvelle lot 044
  private String pil045 = ""; // N° de lot 045
  private BigDecimal piq045 = BigDecimal.ZERO; // Qte initiale lot 045
  private BigDecimal pin045 = BigDecimal.ZERO; // Qte nouvelle lot 045
  private String pil046 = ""; // N° de lot 046
  private BigDecimal piq046 = BigDecimal.ZERO; // Qte initiale lot 046
  private BigDecimal pin046 = BigDecimal.ZERO; // Qte nouvelle lot 046
  private String pil047 = ""; // N° de lot 047
  private BigDecimal piq047 = BigDecimal.ZERO; // Qte initiale lot 047
  private BigDecimal pin047 = BigDecimal.ZERO; // Qte nouvelle lot 047
  private String pil048 = ""; // N° de lot 048
  private BigDecimal piq048 = BigDecimal.ZERO; // Qte initiale lot 048
  private BigDecimal pin048 = BigDecimal.ZERO; // Qte nouvelle lot 048
  private String pil049 = ""; // N° de lot 049
  private BigDecimal piq049 = BigDecimal.ZERO; // Qte initiale lot 049
  private BigDecimal pin049 = BigDecimal.ZERO; // Qte nouvelle lot 049
  private String pil050 = ""; // N° de lot 050
  private BigDecimal piq050 = BigDecimal.ZERO; // Qte initiale lot 050
  private BigDecimal pin050 = BigDecimal.ZERO; // Qte nouvelle lot 050
  private String pil051 = ""; // N° de lot 051
  private BigDecimal piq051 = BigDecimal.ZERO; // Qte initiale lot 051
  private BigDecimal pin051 = BigDecimal.ZERO; // Qte nouvelle lot 051
  private String pil052 = ""; // N° de lot 052
  private BigDecimal piq052 = BigDecimal.ZERO; // Qte initiale lot 052
  private BigDecimal pin052 = BigDecimal.ZERO; // Qte nouvelle lot 052
  private String pil053 = ""; // N° de lot 053
  private BigDecimal piq053 = BigDecimal.ZERO; // Qte initiale lot 053
  private BigDecimal pin053 = BigDecimal.ZERO; // Qte nouvelle lot 053
  private String pil054 = ""; // N° de lot 054
  private BigDecimal piq054 = BigDecimal.ZERO; // Qte initiale lot 054
  private BigDecimal pin054 = BigDecimal.ZERO; // Qte nouvelle lot 054
  private String pil055 = ""; // N° de lot 055
  private BigDecimal piq055 = BigDecimal.ZERO; // Qte initiale lot 055
  private BigDecimal pin055 = BigDecimal.ZERO; // Qte nouvelle lot 055
  private String pil056 = ""; // N° de lot 056
  private BigDecimal piq056 = BigDecimal.ZERO; // Qte initiale lot 056
  private BigDecimal pin056 = BigDecimal.ZERO; // Qte nouvelle lot 056
  private String pil057 = ""; // N° de lot 057
  private BigDecimal piq057 = BigDecimal.ZERO; // Qte initiale lot 057
  private BigDecimal pin057 = BigDecimal.ZERO; // Qte nouvelle lot 057
  private String pil058 = ""; // N° de lot 058
  private BigDecimal piq058 = BigDecimal.ZERO; // Qte initiale lot 058
  private BigDecimal pin058 = BigDecimal.ZERO; // Qte nouvelle lot 058
  private String pil059 = ""; // N° de lot 059
  private BigDecimal piq059 = BigDecimal.ZERO; // Qte initiale lot 059
  private BigDecimal pin059 = BigDecimal.ZERO; // Qte nouvelle lot 059
  private String pil060 = ""; // N° de lot 060
  private BigDecimal piq060 = BigDecimal.ZERO; // Qte initiale lot 060
  private BigDecimal pin060 = BigDecimal.ZERO; // Qte nouvelle lot 060
  private String pil061 = ""; // N° de lot 061
  private BigDecimal piq061 = BigDecimal.ZERO; // Qte initiale lot 061
  private BigDecimal pin061 = BigDecimal.ZERO; // Qte nouvelle lot 061
  private String pil062 = ""; // N° de lot 062
  private BigDecimal piq062 = BigDecimal.ZERO; // Qte initiale lot 062
  private BigDecimal pin062 = BigDecimal.ZERO; // Qte nouvelle lot 062
  private String pil063 = ""; // N° de lot 063
  private BigDecimal piq063 = BigDecimal.ZERO; // Qte initiale lot 063
  private BigDecimal pin063 = BigDecimal.ZERO; // Qte nouvelle lot 063
  private String pil064 = ""; // N° de lot 064
  private BigDecimal piq064 = BigDecimal.ZERO; // Qte initiale lot 064
  private BigDecimal pin064 = BigDecimal.ZERO; // Qte nouvelle lot 064
  private String pil065 = ""; // N° de lot 065
  private BigDecimal piq065 = BigDecimal.ZERO; // Qte initiale lot 065
  private BigDecimal pin065 = BigDecimal.ZERO; // Qte nouvelle lot 065
  private String pil066 = ""; // N° de lot 066
  private BigDecimal piq066 = BigDecimal.ZERO; // Qte initiale lot 066
  private BigDecimal pin066 = BigDecimal.ZERO; // Qte nouvelle lot 066
  private String pil067 = ""; // N° de lot 067
  private BigDecimal piq067 = BigDecimal.ZERO; // Qte initiale lot 067
  private BigDecimal pin067 = BigDecimal.ZERO; // Qte nouvelle lot 067
  private String pil068 = ""; // N° de lot 068
  private BigDecimal piq068 = BigDecimal.ZERO; // Qte initiale lot 068
  private BigDecimal pin068 = BigDecimal.ZERO; // Qte nouvelle lot 068
  private String pil069 = ""; // N° de lot 069
  private BigDecimal piq069 = BigDecimal.ZERO; // Qte initiale lot 069
  private BigDecimal pin069 = BigDecimal.ZERO; // Qte nouvelle lot 069
  private String pil070 = ""; // N° de lot 070
  private BigDecimal piq070 = BigDecimal.ZERO; // Qte initiale lot 070
  private BigDecimal pin070 = BigDecimal.ZERO; // Qte nouvelle lot 070
  private String pil071 = ""; // N° de lot 071
  private BigDecimal piq071 = BigDecimal.ZERO; // Qte initiale lot 071
  private BigDecimal pin071 = BigDecimal.ZERO; // Qte nouvelle lot 071
  private String pil072 = ""; // N° de lot 072
  private BigDecimal piq072 = BigDecimal.ZERO; // Qte initiale lot 072
  private BigDecimal pin072 = BigDecimal.ZERO; // Qte nouvelle lot 072
  private String pil073 = ""; // N° de lot 073
  private BigDecimal piq073 = BigDecimal.ZERO; // Qte initiale lot 073
  private BigDecimal pin073 = BigDecimal.ZERO; // Qte nouvelle lot 073
  private String pil074 = ""; // N° de lot 074
  private BigDecimal piq074 = BigDecimal.ZERO; // Qte initiale lot 074
  private BigDecimal pin074 = BigDecimal.ZERO; // Qte nouvelle lot 074
  private String pil075 = ""; // N° de lot 075
  private BigDecimal piq075 = BigDecimal.ZERO; // Qte initiale lot 075
  private BigDecimal pin075 = BigDecimal.ZERO; // Qte nouvelle lot 075
  private String pil076 = ""; // N° de lot 076
  private BigDecimal piq076 = BigDecimal.ZERO; // Qte initiale lot 076
  private BigDecimal pin076 = BigDecimal.ZERO; // Qte nouvelle lot 076
  private String pil077 = ""; // N° de lot 077
  private BigDecimal piq077 = BigDecimal.ZERO; // Qte initiale lot 077
  private BigDecimal pin077 = BigDecimal.ZERO; // Qte nouvelle lot 077
  private String pil078 = ""; // N° de lot 078
  private BigDecimal piq078 = BigDecimal.ZERO; // Qte initiale lot 078
  private BigDecimal pin078 = BigDecimal.ZERO; // Qte nouvelle lot 078
  private String pil079 = ""; // N° de lot 079
  private BigDecimal piq079 = BigDecimal.ZERO; // Qte initiale lot 079
  private BigDecimal pin079 = BigDecimal.ZERO; // Qte nouvelle lot 079
  private String pil080 = ""; // N° de lot 080
  private BigDecimal piq080 = BigDecimal.ZERO; // Qte initiale lot 080
  private BigDecimal pin080 = BigDecimal.ZERO; // Qte nouvelle lot 080
  private String pil081 = ""; // N° de lot 081
  private BigDecimal piq081 = BigDecimal.ZERO; // Qte initiale lot 081
  private BigDecimal pin081 = BigDecimal.ZERO; // Qte nouvelle lot 081
  private String pil082 = ""; // N° de lot 082
  private BigDecimal piq082 = BigDecimal.ZERO; // Qte initiale lot 082
  private BigDecimal pin082 = BigDecimal.ZERO; // Qte nouvelle lot 082
  private String pil083 = ""; // N° de lot 083
  private BigDecimal piq083 = BigDecimal.ZERO; // Qte initiale lot 083
  private BigDecimal pin083 = BigDecimal.ZERO; // Qte nouvelle lot 083
  private String pil084 = ""; // N° de lot 084
  private BigDecimal piq084 = BigDecimal.ZERO; // Qte initiale lot 084
  private BigDecimal pin084 = BigDecimal.ZERO; // Qte nouvelle lot 084
  private String pil085 = ""; // N° de lot 085
  private BigDecimal piq085 = BigDecimal.ZERO; // Qte initiale lot 085
  private BigDecimal pin085 = BigDecimal.ZERO; // Qte nouvelle lot 085
  private String pil086 = ""; // N° de lot 086
  private BigDecimal piq086 = BigDecimal.ZERO; // Qte initiale lot 086
  private BigDecimal pin086 = BigDecimal.ZERO; // Qte nouvelle lot 086
  private String pil087 = ""; // N° de lot 087
  private BigDecimal piq087 = BigDecimal.ZERO; // Qte initiale lot 087
  private BigDecimal pin087 = BigDecimal.ZERO; // Qte nouvelle lot 087
  private String pil088 = ""; // N° de lot 088
  private BigDecimal piq088 = BigDecimal.ZERO; // Qte initiale lot 088
  private BigDecimal pin088 = BigDecimal.ZERO; // Qte nouvelle lot 088
  private String pil089 = ""; // N° de lot 089
  private BigDecimal piq089 = BigDecimal.ZERO; // Qte initiale lot 089
  private BigDecimal pin089 = BigDecimal.ZERO; // Qte nouvelle lot 089
  private String pil090 = ""; // N° de lot 090
  private BigDecimal piq090 = BigDecimal.ZERO; // Qte initiale lot 090
  private BigDecimal pin090 = BigDecimal.ZERO; // Qte nouvelle lot 090
  private String pil091 = ""; // N° de lot 091
  private BigDecimal piq091 = BigDecimal.ZERO; // Qte initiale lot 091
  private BigDecimal pin091 = BigDecimal.ZERO; // Qte nouvelle lot 091
  private String pil092 = ""; // N° de lot 092
  private BigDecimal piq092 = BigDecimal.ZERO; // Qte initiale lot 092
  private BigDecimal pin092 = BigDecimal.ZERO; // Qte nouvelle lot 092
  private String pil093 = ""; // N° de lot 093
  private BigDecimal piq093 = BigDecimal.ZERO; // Qte initiale lot 093
  private BigDecimal pin093 = BigDecimal.ZERO; // Qte nouvelle lot 093
  private String pil094 = ""; // N° de lot 094
  private BigDecimal piq094 = BigDecimal.ZERO; // Qte initiale lot 094
  private BigDecimal pin094 = BigDecimal.ZERO; // Qte nouvelle lot 094
  private String pil095 = ""; // N° de lot 095
  private BigDecimal piq095 = BigDecimal.ZERO; // Qte initiale lot 095
  private BigDecimal pin095 = BigDecimal.ZERO; // Qte nouvelle lot 095
  private String pil096 = ""; // N° de lot 096
  private BigDecimal piq096 = BigDecimal.ZERO; // Qte initiale lot 096
  private BigDecimal pin096 = BigDecimal.ZERO; // Qte nouvelle lot 096
  private String pil097 = ""; // N° de lot 097
  private BigDecimal piq097 = BigDecimal.ZERO; // Qte initiale lot 097
  private BigDecimal pin097 = BigDecimal.ZERO; // Qte nouvelle lot 097
  private String pil098 = ""; // N° de lot 098
  private BigDecimal piq098 = BigDecimal.ZERO; // Qte initiale lot 098
  private BigDecimal pin098 = BigDecimal.ZERO; // Qte nouvelle lot 098
  private String pil099 = ""; // N° de lot 099
  private BigDecimal piq099 = BigDecimal.ZERO; // Qte initiale lot 099
  private BigDecimal pin099 = BigDecimal.ZERO; // Qte nouvelle lot 099
  private String pil100 = ""; // N° de lot 100
  private BigDecimal piq100 = BigDecimal.ZERO; // Qte initiale lot 100
  private BigDecimal pin100 = BigDecimal.ZERO; // Qte nouvelle lot 100
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "E"
      new AS400Text(SIZE_PIETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de Ligne
      new AS400Text(SIZE_PIART), // Article
      new AS400ZonedDecimal(SIZE_PIQTEA, DECIMAL_PIQTEA), // Quantité article
      new AS400ZonedDecimal(SIZE_PIQTEL, DECIMAL_PIQTEL), // Quantité totale lot
      new AS400Text(SIZE_PIL001), // N° de lot 001
      new AS400ZonedDecimal(SIZE_PIQ001, DECIMAL_PIQ001), // Qte initiale lot 001
      new AS400ZonedDecimal(SIZE_PIN001, DECIMAL_PIN001), // Qte nouvelle lot 001
      new AS400Text(SIZE_PIL002), // N° de lot 002
      new AS400ZonedDecimal(SIZE_PIQ002, DECIMAL_PIQ002), // Qte initiale lot 002
      new AS400ZonedDecimal(SIZE_PIN002, DECIMAL_PIN002), // Qte nouvelle lot 002
      new AS400Text(SIZE_PIL003), // N° de lot 003
      new AS400ZonedDecimal(SIZE_PIQ003, DECIMAL_PIQ003), // Qte initiale lot 003
      new AS400ZonedDecimal(SIZE_PIN003, DECIMAL_PIN003), // Qte nouvelle lot 003
      new AS400Text(SIZE_PIL004), // N° de lot 004
      new AS400ZonedDecimal(SIZE_PIQ004, DECIMAL_PIQ004), // Qte initiale lot 004
      new AS400ZonedDecimal(SIZE_PIN004, DECIMAL_PIN004), // Qte nouvelle lot 004
      new AS400Text(SIZE_PIL005), // N° de lot 005
      new AS400ZonedDecimal(SIZE_PIQ005, DECIMAL_PIQ005), // Qte initiale lot 005
      new AS400ZonedDecimal(SIZE_PIN005, DECIMAL_PIN005), // Qte nouvelle lot 005
      new AS400Text(SIZE_PIL006), // N° de lot 006
      new AS400ZonedDecimal(SIZE_PIQ006, DECIMAL_PIQ006), // Qte initiale lot 006
      new AS400ZonedDecimal(SIZE_PIN006, DECIMAL_PIN006), // Qte nouvelle lot 006
      new AS400Text(SIZE_PIL007), // N° de lot 007
      new AS400ZonedDecimal(SIZE_PIQ007, DECIMAL_PIQ007), // Qte initiale lot 007
      new AS400ZonedDecimal(SIZE_PIN007, DECIMAL_PIN007), // Qte nouvelle lot 007
      new AS400Text(SIZE_PIL008), // N° de lot 008
      new AS400ZonedDecimal(SIZE_PIQ008, DECIMAL_PIQ008), // Qte initiale lot 008
      new AS400ZonedDecimal(SIZE_PIN008, DECIMAL_PIN008), // Qte nouvelle lot 008
      new AS400Text(SIZE_PIL009), // N° de lot 009
      new AS400ZonedDecimal(SIZE_PIQ009, DECIMAL_PIQ009), // Qte initiale lot 009
      new AS400ZonedDecimal(SIZE_PIN009, DECIMAL_PIN009), // Qte nouvelle lot 009
      new AS400Text(SIZE_PIL010), // N° de lot 010
      new AS400ZonedDecimal(SIZE_PIQ010, DECIMAL_PIQ010), // Qte initiale lot 010
      new AS400ZonedDecimal(SIZE_PIN010, DECIMAL_PIN010), // Qte nouvelle lot 010
      new AS400Text(SIZE_PIL011), // N° de lot 011
      new AS400ZonedDecimal(SIZE_PIQ011, DECIMAL_PIQ011), // Qte initiale lot 011
      new AS400ZonedDecimal(SIZE_PIN011, DECIMAL_PIN011), // Qte nouvelle lot 011
      new AS400Text(SIZE_PIL012), // N° de lot 012
      new AS400ZonedDecimal(SIZE_PIQ012, DECIMAL_PIQ012), // Qte initiale lot 012
      new AS400ZonedDecimal(SIZE_PIN012, DECIMAL_PIN012), // Qte nouvelle lot 012
      new AS400Text(SIZE_PIL013), // N° de lot 013
      new AS400ZonedDecimal(SIZE_PIQ013, DECIMAL_PIQ013), // Qte initiale lot 013
      new AS400ZonedDecimal(SIZE_PIN013, DECIMAL_PIN013), // Qte nouvelle lot 013
      new AS400Text(SIZE_PIL014), // N° de lot 014
      new AS400ZonedDecimal(SIZE_PIQ014, DECIMAL_PIQ014), // Qte initiale lot 014
      new AS400ZonedDecimal(SIZE_PIN014, DECIMAL_PIN014), // Qte nouvelle lot 014
      new AS400Text(SIZE_PIL015), // N° de lot 015
      new AS400ZonedDecimal(SIZE_PIQ015, DECIMAL_PIQ015), // Qte initiale lot 015
      new AS400ZonedDecimal(SIZE_PIN015, DECIMAL_PIN015), // Qte nouvelle lot 015
      new AS400Text(SIZE_PIL016), // N° de lot 016
      new AS400ZonedDecimal(SIZE_PIQ016, DECIMAL_PIQ016), // Qte initiale lot 016
      new AS400ZonedDecimal(SIZE_PIN016, DECIMAL_PIN016), // Qte nouvelle lot 016
      new AS400Text(SIZE_PIL017), // N° de lot 017
      new AS400ZonedDecimal(SIZE_PIQ017, DECIMAL_PIQ017), // Qte initiale lot 017
      new AS400ZonedDecimal(SIZE_PIN017, DECIMAL_PIN017), // Qte nouvelle lot 017
      new AS400Text(SIZE_PIL018), // N° de lot 018
      new AS400ZonedDecimal(SIZE_PIQ018, DECIMAL_PIQ018), // Qte initiale lot 018
      new AS400ZonedDecimal(SIZE_PIN018, DECIMAL_PIN018), // Qte nouvelle lot 018
      new AS400Text(SIZE_PIL019), // N° de lot 019
      new AS400ZonedDecimal(SIZE_PIQ019, DECIMAL_PIQ019), // Qte initiale lot 019
      new AS400ZonedDecimal(SIZE_PIN019, DECIMAL_PIN019), // Qte nouvelle lot 019
      new AS400Text(SIZE_PIL020), // N° de lot 020
      new AS400ZonedDecimal(SIZE_PIQ020, DECIMAL_PIQ020), // Qte initiale lot 020
      new AS400ZonedDecimal(SIZE_PIN020, DECIMAL_PIN020), // Qte nouvelle lot 020
      new AS400Text(SIZE_PIL021), // N° de lot 021
      new AS400ZonedDecimal(SIZE_PIQ021, DECIMAL_PIQ021), // Qte initiale lot 021
      new AS400ZonedDecimal(SIZE_PIN021, DECIMAL_PIN021), // Qte nouvelle lot 021
      new AS400Text(SIZE_PIL022), // N° de lot 022
      new AS400ZonedDecimal(SIZE_PIQ022, DECIMAL_PIQ022), // Qte initiale lot 022
      new AS400ZonedDecimal(SIZE_PIN022, DECIMAL_PIN022), // Qte nouvelle lot 022
      new AS400Text(SIZE_PIL023), // N° de lot 023
      new AS400ZonedDecimal(SIZE_PIQ023, DECIMAL_PIQ023), // Qte initiale lot 023
      new AS400ZonedDecimal(SIZE_PIN023, DECIMAL_PIN023), // Qte nouvelle lot 023
      new AS400Text(SIZE_PIL024), // N° de lot 024
      new AS400ZonedDecimal(SIZE_PIQ024, DECIMAL_PIQ024), // Qte initiale lot 024
      new AS400ZonedDecimal(SIZE_PIN024, DECIMAL_PIN024), // Qte nouvelle lot 024
      new AS400Text(SIZE_PIL025), // N° de lot 025
      new AS400ZonedDecimal(SIZE_PIQ025, DECIMAL_PIQ025), // Qte initiale lot 025
      new AS400ZonedDecimal(SIZE_PIN025, DECIMAL_PIN025), // Qte nouvelle lot 025
      new AS400Text(SIZE_PIL026), // N° de lot 026
      new AS400ZonedDecimal(SIZE_PIQ026, DECIMAL_PIQ026), // Qte initiale lot 026
      new AS400ZonedDecimal(SIZE_PIN026, DECIMAL_PIN026), // Qte nouvelle lot 026
      new AS400Text(SIZE_PIL027), // N° de lot 027
      new AS400ZonedDecimal(SIZE_PIQ027, DECIMAL_PIQ027), // Qte initiale lot 027
      new AS400ZonedDecimal(SIZE_PIN027, DECIMAL_PIN027), // Qte nouvelle lot 027
      new AS400Text(SIZE_PIL028), // N° de lot 028
      new AS400ZonedDecimal(SIZE_PIQ028, DECIMAL_PIQ028), // Qte initiale lot 028
      new AS400ZonedDecimal(SIZE_PIN028, DECIMAL_PIN028), // Qte nouvelle lot 028
      new AS400Text(SIZE_PIL029), // N° de lot 029
      new AS400ZonedDecimal(SIZE_PIQ029, DECIMAL_PIQ029), // Qte initiale lot 029
      new AS400ZonedDecimal(SIZE_PIN029, DECIMAL_PIN029), // Qte nouvelle lot 029
      new AS400Text(SIZE_PIL030), // N° de lot 030
      new AS400ZonedDecimal(SIZE_PIQ030, DECIMAL_PIQ030), // Qte initiale lot 030
      new AS400ZonedDecimal(SIZE_PIN030, DECIMAL_PIN030), // Qte nouvelle lot 030
      new AS400Text(SIZE_PIL031), // N° de lot 031
      new AS400ZonedDecimal(SIZE_PIQ031, DECIMAL_PIQ031), // Qte initiale lot 031
      new AS400ZonedDecimal(SIZE_PIN031, DECIMAL_PIN031), // Qte nouvelle lot 031
      new AS400Text(SIZE_PIL032), // N° de lot 032
      new AS400ZonedDecimal(SIZE_PIQ032, DECIMAL_PIQ032), // Qte initiale lot 032
      new AS400ZonedDecimal(SIZE_PIN032, DECIMAL_PIN032), // Qte nouvelle lot 032
      new AS400Text(SIZE_PIL033), // N° de lot 033
      new AS400ZonedDecimal(SIZE_PIQ033, DECIMAL_PIQ033), // Qte initiale lot 033
      new AS400ZonedDecimal(SIZE_PIN033, DECIMAL_PIN033), // Qte nouvelle lot 033
      new AS400Text(SIZE_PIL034), // N° de lot 034
      new AS400ZonedDecimal(SIZE_PIQ034, DECIMAL_PIQ034), // Qte initiale lot 034
      new AS400ZonedDecimal(SIZE_PIN034, DECIMAL_PIN034), // Qte nouvelle lot 034
      new AS400Text(SIZE_PIL035), // N° de lot 035
      new AS400ZonedDecimal(SIZE_PIQ035, DECIMAL_PIQ035), // Qte initiale lot 035
      new AS400ZonedDecimal(SIZE_PIN035, DECIMAL_PIN035), // Qte nouvelle lot 035
      new AS400Text(SIZE_PIL036), // N° de lot 036
      new AS400ZonedDecimal(SIZE_PIQ036, DECIMAL_PIQ036), // Qte initiale lot 036
      new AS400ZonedDecimal(SIZE_PIN036, DECIMAL_PIN036), // Qte nouvelle lot 036
      new AS400Text(SIZE_PIL037), // N° de lot 037
      new AS400ZonedDecimal(SIZE_PIQ037, DECIMAL_PIQ037), // Qte initiale lot 037
      new AS400ZonedDecimal(SIZE_PIN037, DECIMAL_PIN037), // Qte nouvelle lot 037
      new AS400Text(SIZE_PIL038), // N° de lot 038
      new AS400ZonedDecimal(SIZE_PIQ038, DECIMAL_PIQ038), // Qte initiale lot 038
      new AS400ZonedDecimal(SIZE_PIN038, DECIMAL_PIN038), // Qte nouvelle lot 038
      new AS400Text(SIZE_PIL039), // N° de lot 039
      new AS400ZonedDecimal(SIZE_PIQ039, DECIMAL_PIQ039), // Qte initiale lot 039
      new AS400ZonedDecimal(SIZE_PIN039, DECIMAL_PIN039), // Qte nouvelle lot 039
      new AS400Text(SIZE_PIL040), // N° de lot 040
      new AS400ZonedDecimal(SIZE_PIQ040, DECIMAL_PIQ040), // Qte initiale lot 040
      new AS400ZonedDecimal(SIZE_PIN040, DECIMAL_PIN040), // Qte nouvelle lot 040
      new AS400Text(SIZE_PIL041), // N° de lot 041
      new AS400ZonedDecimal(SIZE_PIQ041, DECIMAL_PIQ041), // Qte initiale lot 041
      new AS400ZonedDecimal(SIZE_PIN041, DECIMAL_PIN041), // Qte nouvelle lot 041
      new AS400Text(SIZE_PIL042), // N° de lot 042
      new AS400ZonedDecimal(SIZE_PIQ042, DECIMAL_PIQ042), // Qte initiale lot 042
      new AS400ZonedDecimal(SIZE_PIN042, DECIMAL_PIN042), // Qte nouvelle lot 042
      new AS400Text(SIZE_PIL043), // N° de lot 043
      new AS400ZonedDecimal(SIZE_PIQ043, DECIMAL_PIQ043), // Qte initiale lot 043
      new AS400ZonedDecimal(SIZE_PIN043, DECIMAL_PIN043), // Qte nouvelle lot 043
      new AS400Text(SIZE_PIL044), // N° de lot 044
      new AS400ZonedDecimal(SIZE_PIQ044, DECIMAL_PIQ044), // Qte initiale lot 044
      new AS400ZonedDecimal(SIZE_PIN044, DECIMAL_PIN044), // Qte nouvelle lot 044
      new AS400Text(SIZE_PIL045), // N° de lot 045
      new AS400ZonedDecimal(SIZE_PIQ045, DECIMAL_PIQ045), // Qte initiale lot 045
      new AS400ZonedDecimal(SIZE_PIN045, DECIMAL_PIN045), // Qte nouvelle lot 045
      new AS400Text(SIZE_PIL046), // N° de lot 046
      new AS400ZonedDecimal(SIZE_PIQ046, DECIMAL_PIQ046), // Qte initiale lot 046
      new AS400ZonedDecimal(SIZE_PIN046, DECIMAL_PIN046), // Qte nouvelle lot 046
      new AS400Text(SIZE_PIL047), // N° de lot 047
      new AS400ZonedDecimal(SIZE_PIQ047, DECIMAL_PIQ047), // Qte initiale lot 047
      new AS400ZonedDecimal(SIZE_PIN047, DECIMAL_PIN047), // Qte nouvelle lot 047
      new AS400Text(SIZE_PIL048), // N° de lot 048
      new AS400ZonedDecimal(SIZE_PIQ048, DECIMAL_PIQ048), // Qte initiale lot 048
      new AS400ZonedDecimal(SIZE_PIN048, DECIMAL_PIN048), // Qte nouvelle lot 048
      new AS400Text(SIZE_PIL049), // N° de lot 049
      new AS400ZonedDecimal(SIZE_PIQ049, DECIMAL_PIQ049), // Qte initiale lot 049
      new AS400ZonedDecimal(SIZE_PIN049, DECIMAL_PIN049), // Qte nouvelle lot 049
      new AS400Text(SIZE_PIL050), // N° de lot 050
      new AS400ZonedDecimal(SIZE_PIQ050, DECIMAL_PIQ050), // Qte initiale lot 050
      new AS400ZonedDecimal(SIZE_PIN050, DECIMAL_PIN050), // Qte nouvelle lot 050
      new AS400Text(SIZE_PIL051), // N° de lot 051
      new AS400ZonedDecimal(SIZE_PIQ051, DECIMAL_PIQ051), // Qte initiale lot 051
      new AS400ZonedDecimal(SIZE_PIN051, DECIMAL_PIN051), // Qte nouvelle lot 051
      new AS400Text(SIZE_PIL052), // N° de lot 052
      new AS400ZonedDecimal(SIZE_PIQ052, DECIMAL_PIQ052), // Qte initiale lot 052
      new AS400ZonedDecimal(SIZE_PIN052, DECIMAL_PIN052), // Qte nouvelle lot 052
      new AS400Text(SIZE_PIL053), // N° de lot 053
      new AS400ZonedDecimal(SIZE_PIQ053, DECIMAL_PIQ053), // Qte initiale lot 053
      new AS400ZonedDecimal(SIZE_PIN053, DECIMAL_PIN053), // Qte nouvelle lot 053
      new AS400Text(SIZE_PIL054), // N° de lot 054
      new AS400ZonedDecimal(SIZE_PIQ054, DECIMAL_PIQ054), // Qte initiale lot 054
      new AS400ZonedDecimal(SIZE_PIN054, DECIMAL_PIN054), // Qte nouvelle lot 054
      new AS400Text(SIZE_PIL055), // N° de lot 055
      new AS400ZonedDecimal(SIZE_PIQ055, DECIMAL_PIQ055), // Qte initiale lot 055
      new AS400ZonedDecimal(SIZE_PIN055, DECIMAL_PIN055), // Qte nouvelle lot 055
      new AS400Text(SIZE_PIL056), // N° de lot 056
      new AS400ZonedDecimal(SIZE_PIQ056, DECIMAL_PIQ056), // Qte initiale lot 056
      new AS400ZonedDecimal(SIZE_PIN056, DECIMAL_PIN056), // Qte nouvelle lot 056
      new AS400Text(SIZE_PIL057), // N° de lot 057
      new AS400ZonedDecimal(SIZE_PIQ057, DECIMAL_PIQ057), // Qte initiale lot 057
      new AS400ZonedDecimal(SIZE_PIN057, DECIMAL_PIN057), // Qte nouvelle lot 057
      new AS400Text(SIZE_PIL058), // N° de lot 058
      new AS400ZonedDecimal(SIZE_PIQ058, DECIMAL_PIQ058), // Qte initiale lot 058
      new AS400ZonedDecimal(SIZE_PIN058, DECIMAL_PIN058), // Qte nouvelle lot 058
      new AS400Text(SIZE_PIL059), // N° de lot 059
      new AS400ZonedDecimal(SIZE_PIQ059, DECIMAL_PIQ059), // Qte initiale lot 059
      new AS400ZonedDecimal(SIZE_PIN059, DECIMAL_PIN059), // Qte nouvelle lot 059
      new AS400Text(SIZE_PIL060), // N° de lot 060
      new AS400ZonedDecimal(SIZE_PIQ060, DECIMAL_PIQ060), // Qte initiale lot 060
      new AS400ZonedDecimal(SIZE_PIN060, DECIMAL_PIN060), // Qte nouvelle lot 060
      new AS400Text(SIZE_PIL061), // N° de lot 061
      new AS400ZonedDecimal(SIZE_PIQ061, DECIMAL_PIQ061), // Qte initiale lot 061
      new AS400ZonedDecimal(SIZE_PIN061, DECIMAL_PIN061), // Qte nouvelle lot 061
      new AS400Text(SIZE_PIL062), // N° de lot 062
      new AS400ZonedDecimal(SIZE_PIQ062, DECIMAL_PIQ062), // Qte initiale lot 062
      new AS400ZonedDecimal(SIZE_PIN062, DECIMAL_PIN062), // Qte nouvelle lot 062
      new AS400Text(SIZE_PIL063), // N° de lot 063
      new AS400ZonedDecimal(SIZE_PIQ063, DECIMAL_PIQ063), // Qte initiale lot 063
      new AS400ZonedDecimal(SIZE_PIN063, DECIMAL_PIN063), // Qte nouvelle lot 063
      new AS400Text(SIZE_PIL064), // N° de lot 064
      new AS400ZonedDecimal(SIZE_PIQ064, DECIMAL_PIQ064), // Qte initiale lot 064
      new AS400ZonedDecimal(SIZE_PIN064, DECIMAL_PIN064), // Qte nouvelle lot 064
      new AS400Text(SIZE_PIL065), // N° de lot 065
      new AS400ZonedDecimal(SIZE_PIQ065, DECIMAL_PIQ065), // Qte initiale lot 065
      new AS400ZonedDecimal(SIZE_PIN065, DECIMAL_PIN065), // Qte nouvelle lot 065
      new AS400Text(SIZE_PIL066), // N° de lot 066
      new AS400ZonedDecimal(SIZE_PIQ066, DECIMAL_PIQ066), // Qte initiale lot 066
      new AS400ZonedDecimal(SIZE_PIN066, DECIMAL_PIN066), // Qte nouvelle lot 066
      new AS400Text(SIZE_PIL067), // N° de lot 067
      new AS400ZonedDecimal(SIZE_PIQ067, DECIMAL_PIQ067), // Qte initiale lot 067
      new AS400ZonedDecimal(SIZE_PIN067, DECIMAL_PIN067), // Qte nouvelle lot 067
      new AS400Text(SIZE_PIL068), // N° de lot 068
      new AS400ZonedDecimal(SIZE_PIQ068, DECIMAL_PIQ068), // Qte initiale lot 068
      new AS400ZonedDecimal(SIZE_PIN068, DECIMAL_PIN068), // Qte nouvelle lot 068
      new AS400Text(SIZE_PIL069), // N° de lot 069
      new AS400ZonedDecimal(SIZE_PIQ069, DECIMAL_PIQ069), // Qte initiale lot 069
      new AS400ZonedDecimal(SIZE_PIN069, DECIMAL_PIN069), // Qte nouvelle lot 069
      new AS400Text(SIZE_PIL070), // N° de lot 070
      new AS400ZonedDecimal(SIZE_PIQ070, DECIMAL_PIQ070), // Qte initiale lot 070
      new AS400ZonedDecimal(SIZE_PIN070, DECIMAL_PIN070), // Qte nouvelle lot 070
      new AS400Text(SIZE_PIL071), // N° de lot 071
      new AS400ZonedDecimal(SIZE_PIQ071, DECIMAL_PIQ071), // Qte initiale lot 071
      new AS400ZonedDecimal(SIZE_PIN071, DECIMAL_PIN071), // Qte nouvelle lot 071
      new AS400Text(SIZE_PIL072), // N° de lot 072
      new AS400ZonedDecimal(SIZE_PIQ072, DECIMAL_PIQ072), // Qte initiale lot 072
      new AS400ZonedDecimal(SIZE_PIN072, DECIMAL_PIN072), // Qte nouvelle lot 072
      new AS400Text(SIZE_PIL073), // N° de lot 073
      new AS400ZonedDecimal(SIZE_PIQ073, DECIMAL_PIQ073), // Qte initiale lot 073
      new AS400ZonedDecimal(SIZE_PIN073, DECIMAL_PIN073), // Qte nouvelle lot 073
      new AS400Text(SIZE_PIL074), // N° de lot 074
      new AS400ZonedDecimal(SIZE_PIQ074, DECIMAL_PIQ074), // Qte initiale lot 074
      new AS400ZonedDecimal(SIZE_PIN074, DECIMAL_PIN074), // Qte nouvelle lot 074
      new AS400Text(SIZE_PIL075), // N° de lot 075
      new AS400ZonedDecimal(SIZE_PIQ075, DECIMAL_PIQ075), // Qte initiale lot 075
      new AS400ZonedDecimal(SIZE_PIN075, DECIMAL_PIN075), // Qte nouvelle lot 075
      new AS400Text(SIZE_PIL076), // N° de lot 076
      new AS400ZonedDecimal(SIZE_PIQ076, DECIMAL_PIQ076), // Qte initiale lot 076
      new AS400ZonedDecimal(SIZE_PIN076, DECIMAL_PIN076), // Qte nouvelle lot 076
      new AS400Text(SIZE_PIL077), // N° de lot 077
      new AS400ZonedDecimal(SIZE_PIQ077, DECIMAL_PIQ077), // Qte initiale lot 077
      new AS400ZonedDecimal(SIZE_PIN077, DECIMAL_PIN077), // Qte nouvelle lot 077
      new AS400Text(SIZE_PIL078), // N° de lot 078
      new AS400ZonedDecimal(SIZE_PIQ078, DECIMAL_PIQ078), // Qte initiale lot 078
      new AS400ZonedDecimal(SIZE_PIN078, DECIMAL_PIN078), // Qte nouvelle lot 078
      new AS400Text(SIZE_PIL079), // N° de lot 079
      new AS400ZonedDecimal(SIZE_PIQ079, DECIMAL_PIQ079), // Qte initiale lot 079
      new AS400ZonedDecimal(SIZE_PIN079, DECIMAL_PIN079), // Qte nouvelle lot 079
      new AS400Text(SIZE_PIL080), // N° de lot 080
      new AS400ZonedDecimal(SIZE_PIQ080, DECIMAL_PIQ080), // Qte initiale lot 080
      new AS400ZonedDecimal(SIZE_PIN080, DECIMAL_PIN080), // Qte nouvelle lot 080
      new AS400Text(SIZE_PIL081), // N° de lot 081
      new AS400ZonedDecimal(SIZE_PIQ081, DECIMAL_PIQ081), // Qte initiale lot 081
      new AS400ZonedDecimal(SIZE_PIN081, DECIMAL_PIN081), // Qte nouvelle lot 081
      new AS400Text(SIZE_PIL082), // N° de lot 082
      new AS400ZonedDecimal(SIZE_PIQ082, DECIMAL_PIQ082), // Qte initiale lot 082
      new AS400ZonedDecimal(SIZE_PIN082, DECIMAL_PIN082), // Qte nouvelle lot 082
      new AS400Text(SIZE_PIL083), // N° de lot 083
      new AS400ZonedDecimal(SIZE_PIQ083, DECIMAL_PIQ083), // Qte initiale lot 083
      new AS400ZonedDecimal(SIZE_PIN083, DECIMAL_PIN083), // Qte nouvelle lot 083
      new AS400Text(SIZE_PIL084), // N° de lot 084
      new AS400ZonedDecimal(SIZE_PIQ084, DECIMAL_PIQ084), // Qte initiale lot 084
      new AS400ZonedDecimal(SIZE_PIN084, DECIMAL_PIN084), // Qte nouvelle lot 084
      new AS400Text(SIZE_PIL085), // N° de lot 085
      new AS400ZonedDecimal(SIZE_PIQ085, DECIMAL_PIQ085), // Qte initiale lot 085
      new AS400ZonedDecimal(SIZE_PIN085, DECIMAL_PIN085), // Qte nouvelle lot 085
      new AS400Text(SIZE_PIL086), // N° de lot 086
      new AS400ZonedDecimal(SIZE_PIQ086, DECIMAL_PIQ086), // Qte initiale lot 086
      new AS400ZonedDecimal(SIZE_PIN086, DECIMAL_PIN086), // Qte nouvelle lot 086
      new AS400Text(SIZE_PIL087), // N° de lot 087
      new AS400ZonedDecimal(SIZE_PIQ087, DECIMAL_PIQ087), // Qte initiale lot 087
      new AS400ZonedDecimal(SIZE_PIN087, DECIMAL_PIN087), // Qte nouvelle lot 087
      new AS400Text(SIZE_PIL088), // N° de lot 088
      new AS400ZonedDecimal(SIZE_PIQ088, DECIMAL_PIQ088), // Qte initiale lot 088
      new AS400ZonedDecimal(SIZE_PIN088, DECIMAL_PIN088), // Qte nouvelle lot 088
      new AS400Text(SIZE_PIL089), // N° de lot 089
      new AS400ZonedDecimal(SIZE_PIQ089, DECIMAL_PIQ089), // Qte initiale lot 089
      new AS400ZonedDecimal(SIZE_PIN089, DECIMAL_PIN089), // Qte nouvelle lot 089
      new AS400Text(SIZE_PIL090), // N° de lot 090
      new AS400ZonedDecimal(SIZE_PIQ090, DECIMAL_PIQ090), // Qte initiale lot 090
      new AS400ZonedDecimal(SIZE_PIN090, DECIMAL_PIN090), // Qte nouvelle lot 090
      new AS400Text(SIZE_PIL091), // N° de lot 091
      new AS400ZonedDecimal(SIZE_PIQ091, DECIMAL_PIQ091), // Qte initiale lot 091
      new AS400ZonedDecimal(SIZE_PIN091, DECIMAL_PIN091), // Qte nouvelle lot 091
      new AS400Text(SIZE_PIL092), // N° de lot 092
      new AS400ZonedDecimal(SIZE_PIQ092, DECIMAL_PIQ092), // Qte initiale lot 092
      new AS400ZonedDecimal(SIZE_PIN092, DECIMAL_PIN092), // Qte nouvelle lot 092
      new AS400Text(SIZE_PIL093), // N° de lot 093
      new AS400ZonedDecimal(SIZE_PIQ093, DECIMAL_PIQ093), // Qte initiale lot 093
      new AS400ZonedDecimal(SIZE_PIN093, DECIMAL_PIN093), // Qte nouvelle lot 093
      new AS400Text(SIZE_PIL094), // N° de lot 094
      new AS400ZonedDecimal(SIZE_PIQ094, DECIMAL_PIQ094), // Qte initiale lot 094
      new AS400ZonedDecimal(SIZE_PIN094, DECIMAL_PIN094), // Qte nouvelle lot 094
      new AS400Text(SIZE_PIL095), // N° de lot 095
      new AS400ZonedDecimal(SIZE_PIQ095, DECIMAL_PIQ095), // Qte initiale lot 095
      new AS400ZonedDecimal(SIZE_PIN095, DECIMAL_PIN095), // Qte nouvelle lot 095
      new AS400Text(SIZE_PIL096), // N° de lot 096
      new AS400ZonedDecimal(SIZE_PIQ096, DECIMAL_PIQ096), // Qte initiale lot 096
      new AS400ZonedDecimal(SIZE_PIN096, DECIMAL_PIN096), // Qte nouvelle lot 096
      new AS400Text(SIZE_PIL097), // N° de lot 097
      new AS400ZonedDecimal(SIZE_PIQ097, DECIMAL_PIQ097), // Qte initiale lot 097
      new AS400ZonedDecimal(SIZE_PIN097, DECIMAL_PIN097), // Qte nouvelle lot 097
      new AS400Text(SIZE_PIL098), // N° de lot 098
      new AS400ZonedDecimal(SIZE_PIQ098, DECIMAL_PIQ098), // Qte initiale lot 098
      new AS400ZonedDecimal(SIZE_PIN098, DECIMAL_PIN098), // Qte nouvelle lot 098
      new AS400Text(SIZE_PIL099), // N° de lot 099
      new AS400ZonedDecimal(SIZE_PIQ099, DECIMAL_PIQ099), // Qte initiale lot 099
      new AS400ZonedDecimal(SIZE_PIN099, DECIMAL_PIN099), // Qte nouvelle lot 099
      new AS400Text(SIZE_PIL100), // N° de lot 100
      new AS400ZonedDecimal(SIZE_PIQ100, DECIMAL_PIQ100), // Qte initiale lot 100
      new AS400ZonedDecimal(SIZE_PIN100, DECIMAL_PIN100), // Qte nouvelle lot 100
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, picod, pietb, pinum, pisuf, pinli, piart, piqtea, piqtel, pil001, piq001, pin001, pil002, piq002, pin002,
          pil003, piq003, pin003, pil004, piq004, pin004, pil005, piq005, pin005, pil006, piq006, pin006, pil007, piq007, pin007, pil008,
          piq008, pin008, pil009, piq009, pin009, pil010, piq010, pin010, pil011, piq011, pin011, pil012, piq012, pin012, pil013, piq013,
          pin013, pil014, piq014, pin014, pil015, piq015, pin015, pil016, piq016, pin016, pil017, piq017, pin017, pil018, piq018, pin018,
          pil019, piq019, pin019, pil020, piq020, pin020, pil021, piq021, pin021, pil022, piq022, pin022, pil023, piq023, pin023, pil024,
          piq024, pin024, pil025, piq025, pin025, pil026, piq026, pin026, pil027, piq027, pin027, pil028, piq028, pin028, pil029, piq029,
          pin029, pil030, piq030, pin030, pil031, piq031, pin031, pil032, piq032, pin032, pil033, piq033, pin033, pil034, piq034, pin034,
          pil035, piq035, pin035, pil036, piq036, pin036, pil037, piq037, pin037, pil038, piq038, pin038, pil039, piq039, pin039, pil040,
          piq040, pin040, pil041, piq041, pin041, pil042, piq042, pin042, pil043, piq043, pin043, pil044, piq044, pin044, pil045, piq045,
          pin045, pil046, piq046, pin046, pil047, piq047, pin047, pil048, piq048, pin048, pil049, piq049, pin049, pil050, piq050, pin050,
          pil051, piq051, pin051, pil052, piq052, pin052, pil053, piq053, pin053, pil054, piq054, pin054, pil055, piq055, pin055, pil056,
          piq056, pin056, pil057, piq057, pin057, pil058, piq058, pin058, pil059, piq059, pin059, pil060, piq060, pin060, pil061, piq061,
          pin061, pil062, piq062, pin062, pil063, piq063, pin063, pil064, piq064, pin064, pil065, piq065, pin065, pil066, piq066, pin066,
          pil067, piq067, pin067, pil068, piq068, pin068, pil069, piq069, pin069, pil070, piq070, pin070, pil071, piq071, pin071, pil072,
          piq072, pin072, pil073, piq073, pin073, pil074, piq074, pin074, pil075, piq075, pin075, pil076, piq076, pin076, pil077, piq077,
          pin077, pil078, piq078, pin078, pil079, piq079, pin079, pil080, piq080, pin080, pil081, piq081, pin081, pil082, piq082, pin082,
          pil083, piq083, pin083, pil084, piq084, pin084, pil085, piq085, pin085, pil086, piq086, pin086, pil087, piq087, pin087, pil088,
          piq088, pin088, pil089, piq089, pin089, pil090, piq090, pin090, pil091, piq091, pin091, pil092, piq092, pin092, pil093, piq093,
          pin093, pil094, piq094, pin094, pil095, piq095, pin095, pil096, piq096, pin096, pil097, piq097, pin097, pil098, piq098, pin098,
          pil099, piq099, pin099, pil100, piq100, pin100, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinli = (BigDecimal) output[5];
    piart = (String) output[6];
    piqtea = (BigDecimal) output[7];
    piqtel = (BigDecimal) output[8];
    pil001 = (String) output[9];
    piq001 = (BigDecimal) output[10];
    pin001 = (BigDecimal) output[11];
    pil002 = (String) output[12];
    piq002 = (BigDecimal) output[13];
    pin002 = (BigDecimal) output[14];
    pil003 = (String) output[15];
    piq003 = (BigDecimal) output[16];
    pin003 = (BigDecimal) output[17];
    pil004 = (String) output[18];
    piq004 = (BigDecimal) output[19];
    pin004 = (BigDecimal) output[20];
    pil005 = (String) output[21];
    piq005 = (BigDecimal) output[22];
    pin005 = (BigDecimal) output[23];
    pil006 = (String) output[24];
    piq006 = (BigDecimal) output[25];
    pin006 = (BigDecimal) output[26];
    pil007 = (String) output[27];
    piq007 = (BigDecimal) output[28];
    pin007 = (BigDecimal) output[29];
    pil008 = (String) output[30];
    piq008 = (BigDecimal) output[31];
    pin008 = (BigDecimal) output[32];
    pil009 = (String) output[33];
    piq009 = (BigDecimal) output[34];
    pin009 = (BigDecimal) output[35];
    pil010 = (String) output[36];
    piq010 = (BigDecimal) output[37];
    pin010 = (BigDecimal) output[38];
    pil011 = (String) output[39];
    piq011 = (BigDecimal) output[40];
    pin011 = (BigDecimal) output[41];
    pil012 = (String) output[42];
    piq012 = (BigDecimal) output[43];
    pin012 = (BigDecimal) output[44];
    pil013 = (String) output[45];
    piq013 = (BigDecimal) output[46];
    pin013 = (BigDecimal) output[47];
    pil014 = (String) output[48];
    piq014 = (BigDecimal) output[49];
    pin014 = (BigDecimal) output[50];
    pil015 = (String) output[51];
    piq015 = (BigDecimal) output[52];
    pin015 = (BigDecimal) output[53];
    pil016 = (String) output[54];
    piq016 = (BigDecimal) output[55];
    pin016 = (BigDecimal) output[56];
    pil017 = (String) output[57];
    piq017 = (BigDecimal) output[58];
    pin017 = (BigDecimal) output[59];
    pil018 = (String) output[60];
    piq018 = (BigDecimal) output[61];
    pin018 = (BigDecimal) output[62];
    pil019 = (String) output[63];
    piq019 = (BigDecimal) output[64];
    pin019 = (BigDecimal) output[65];
    pil020 = (String) output[66];
    piq020 = (BigDecimal) output[67];
    pin020 = (BigDecimal) output[68];
    pil021 = (String) output[69];
    piq021 = (BigDecimal) output[70];
    pin021 = (BigDecimal) output[71];
    pil022 = (String) output[72];
    piq022 = (BigDecimal) output[73];
    pin022 = (BigDecimal) output[74];
    pil023 = (String) output[75];
    piq023 = (BigDecimal) output[76];
    pin023 = (BigDecimal) output[77];
    pil024 = (String) output[78];
    piq024 = (BigDecimal) output[79];
    pin024 = (BigDecimal) output[80];
    pil025 = (String) output[81];
    piq025 = (BigDecimal) output[82];
    pin025 = (BigDecimal) output[83];
    pil026 = (String) output[84];
    piq026 = (BigDecimal) output[85];
    pin026 = (BigDecimal) output[86];
    pil027 = (String) output[87];
    piq027 = (BigDecimal) output[88];
    pin027 = (BigDecimal) output[89];
    pil028 = (String) output[90];
    piq028 = (BigDecimal) output[91];
    pin028 = (BigDecimal) output[92];
    pil029 = (String) output[93];
    piq029 = (BigDecimal) output[94];
    pin029 = (BigDecimal) output[95];
    pil030 = (String) output[96];
    piq030 = (BigDecimal) output[97];
    pin030 = (BigDecimal) output[98];
    pil031 = (String) output[99];
    piq031 = (BigDecimal) output[100];
    pin031 = (BigDecimal) output[101];
    pil032 = (String) output[102];
    piq032 = (BigDecimal) output[103];
    pin032 = (BigDecimal) output[104];
    pil033 = (String) output[105];
    piq033 = (BigDecimal) output[106];
    pin033 = (BigDecimal) output[107];
    pil034 = (String) output[108];
    piq034 = (BigDecimal) output[109];
    pin034 = (BigDecimal) output[110];
    pil035 = (String) output[111];
    piq035 = (BigDecimal) output[112];
    pin035 = (BigDecimal) output[113];
    pil036 = (String) output[114];
    piq036 = (BigDecimal) output[115];
    pin036 = (BigDecimal) output[116];
    pil037 = (String) output[117];
    piq037 = (BigDecimal) output[118];
    pin037 = (BigDecimal) output[119];
    pil038 = (String) output[120];
    piq038 = (BigDecimal) output[121];
    pin038 = (BigDecimal) output[122];
    pil039 = (String) output[123];
    piq039 = (BigDecimal) output[124];
    pin039 = (BigDecimal) output[125];
    pil040 = (String) output[126];
    piq040 = (BigDecimal) output[127];
    pin040 = (BigDecimal) output[128];
    pil041 = (String) output[129];
    piq041 = (BigDecimal) output[130];
    pin041 = (BigDecimal) output[131];
    pil042 = (String) output[132];
    piq042 = (BigDecimal) output[133];
    pin042 = (BigDecimal) output[134];
    pil043 = (String) output[135];
    piq043 = (BigDecimal) output[136];
    pin043 = (BigDecimal) output[137];
    pil044 = (String) output[138];
    piq044 = (BigDecimal) output[139];
    pin044 = (BigDecimal) output[140];
    pil045 = (String) output[141];
    piq045 = (BigDecimal) output[142];
    pin045 = (BigDecimal) output[143];
    pil046 = (String) output[144];
    piq046 = (BigDecimal) output[145];
    pin046 = (BigDecimal) output[146];
    pil047 = (String) output[147];
    piq047 = (BigDecimal) output[148];
    pin047 = (BigDecimal) output[149];
    pil048 = (String) output[150];
    piq048 = (BigDecimal) output[151];
    pin048 = (BigDecimal) output[152];
    pil049 = (String) output[153];
    piq049 = (BigDecimal) output[154];
    pin049 = (BigDecimal) output[155];
    pil050 = (String) output[156];
    piq050 = (BigDecimal) output[157];
    pin050 = (BigDecimal) output[158];
    pil051 = (String) output[159];
    piq051 = (BigDecimal) output[160];
    pin051 = (BigDecimal) output[161];
    pil052 = (String) output[162];
    piq052 = (BigDecimal) output[163];
    pin052 = (BigDecimal) output[164];
    pil053 = (String) output[165];
    piq053 = (BigDecimal) output[166];
    pin053 = (BigDecimal) output[167];
    pil054 = (String) output[168];
    piq054 = (BigDecimal) output[169];
    pin054 = (BigDecimal) output[170];
    pil055 = (String) output[171];
    piq055 = (BigDecimal) output[172];
    pin055 = (BigDecimal) output[173];
    pil056 = (String) output[174];
    piq056 = (BigDecimal) output[175];
    pin056 = (BigDecimal) output[176];
    pil057 = (String) output[177];
    piq057 = (BigDecimal) output[178];
    pin057 = (BigDecimal) output[179];
    pil058 = (String) output[180];
    piq058 = (BigDecimal) output[181];
    pin058 = (BigDecimal) output[182];
    pil059 = (String) output[183];
    piq059 = (BigDecimal) output[184];
    pin059 = (BigDecimal) output[185];
    pil060 = (String) output[186];
    piq060 = (BigDecimal) output[187];
    pin060 = (BigDecimal) output[188];
    pil061 = (String) output[189];
    piq061 = (BigDecimal) output[190];
    pin061 = (BigDecimal) output[191];
    pil062 = (String) output[192];
    piq062 = (BigDecimal) output[193];
    pin062 = (BigDecimal) output[194];
    pil063 = (String) output[195];
    piq063 = (BigDecimal) output[196];
    pin063 = (BigDecimal) output[197];
    pil064 = (String) output[198];
    piq064 = (BigDecimal) output[199];
    pin064 = (BigDecimal) output[200];
    pil065 = (String) output[201];
    piq065 = (BigDecimal) output[202];
    pin065 = (BigDecimal) output[203];
    pil066 = (String) output[204];
    piq066 = (BigDecimal) output[205];
    pin066 = (BigDecimal) output[206];
    pil067 = (String) output[207];
    piq067 = (BigDecimal) output[208];
    pin067 = (BigDecimal) output[209];
    pil068 = (String) output[210];
    piq068 = (BigDecimal) output[211];
    pin068 = (BigDecimal) output[212];
    pil069 = (String) output[213];
    piq069 = (BigDecimal) output[214];
    pin069 = (BigDecimal) output[215];
    pil070 = (String) output[216];
    piq070 = (BigDecimal) output[217];
    pin070 = (BigDecimal) output[218];
    pil071 = (String) output[219];
    piq071 = (BigDecimal) output[220];
    pin071 = (BigDecimal) output[221];
    pil072 = (String) output[222];
    piq072 = (BigDecimal) output[223];
    pin072 = (BigDecimal) output[224];
    pil073 = (String) output[225];
    piq073 = (BigDecimal) output[226];
    pin073 = (BigDecimal) output[227];
    pil074 = (String) output[228];
    piq074 = (BigDecimal) output[229];
    pin074 = (BigDecimal) output[230];
    pil075 = (String) output[231];
    piq075 = (BigDecimal) output[232];
    pin075 = (BigDecimal) output[233];
    pil076 = (String) output[234];
    piq076 = (BigDecimal) output[235];
    pin076 = (BigDecimal) output[236];
    pil077 = (String) output[237];
    piq077 = (BigDecimal) output[238];
    pin077 = (BigDecimal) output[239];
    pil078 = (String) output[240];
    piq078 = (BigDecimal) output[241];
    pin078 = (BigDecimal) output[242];
    pil079 = (String) output[243];
    piq079 = (BigDecimal) output[244];
    pin079 = (BigDecimal) output[245];
    pil080 = (String) output[246];
    piq080 = (BigDecimal) output[247];
    pin080 = (BigDecimal) output[248];
    pil081 = (String) output[249];
    piq081 = (BigDecimal) output[250];
    pin081 = (BigDecimal) output[251];
    pil082 = (String) output[252];
    piq082 = (BigDecimal) output[253];
    pin082 = (BigDecimal) output[254];
    pil083 = (String) output[255];
    piq083 = (BigDecimal) output[256];
    pin083 = (BigDecimal) output[257];
    pil084 = (String) output[258];
    piq084 = (BigDecimal) output[259];
    pin084 = (BigDecimal) output[260];
    pil085 = (String) output[261];
    piq085 = (BigDecimal) output[262];
    pin085 = (BigDecimal) output[263];
    pil086 = (String) output[264];
    piq086 = (BigDecimal) output[265];
    pin086 = (BigDecimal) output[266];
    pil087 = (String) output[267];
    piq087 = (BigDecimal) output[268];
    pin087 = (BigDecimal) output[269];
    pil088 = (String) output[270];
    piq088 = (BigDecimal) output[271];
    pin088 = (BigDecimal) output[272];
    pil089 = (String) output[273];
    piq089 = (BigDecimal) output[274];
    pin089 = (BigDecimal) output[275];
    pil090 = (String) output[276];
    piq090 = (BigDecimal) output[277];
    pin090 = (BigDecimal) output[278];
    pil091 = (String) output[279];
    piq091 = (BigDecimal) output[280];
    pin091 = (BigDecimal) output[281];
    pil092 = (String) output[282];
    piq092 = (BigDecimal) output[283];
    pin092 = (BigDecimal) output[284];
    pil093 = (String) output[285];
    piq093 = (BigDecimal) output[286];
    pin093 = (BigDecimal) output[287];
    pil094 = (String) output[288];
    piq094 = (BigDecimal) output[289];
    pin094 = (BigDecimal) output[290];
    pil095 = (String) output[291];
    piq095 = (BigDecimal) output[292];
    pin095 = (BigDecimal) output[293];
    pil096 = (String) output[294];
    piq096 = (BigDecimal) output[295];
    pin096 = (BigDecimal) output[296];
    pil097 = (String) output[297];
    piq097 = (BigDecimal) output[298];
    pin097 = (BigDecimal) output[299];
    pil098 = (String) output[300];
    piq098 = (BigDecimal) output[301];
    pin098 = (BigDecimal) output[302];
    pil099 = (String) output[303];
    piq099 = (BigDecimal) output[304];
    pin099 = (BigDecimal) output[305];
    pil100 = (String) output[306];
    piq100 = (BigDecimal) output[307];
    pin100 = (BigDecimal) output[308];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPiart(String pPiart) {
    if (pPiart == null) {
      return;
    }
    piart = pPiart;
  }
  
  public String getPiart() {
    return piart;
  }
  
  public void setPiqtea(BigDecimal pPiqtea) {
    if (pPiqtea == null) {
      return;
    }
    piqtea = pPiqtea.setScale(DECIMAL_PIQTEA, RoundingMode.HALF_UP);
  }
  
  public void setPiqtea(Double pPiqtea) {
    if (pPiqtea == null) {
      return;
    }
    piqtea = BigDecimal.valueOf(pPiqtea).setScale(DECIMAL_PIQTEA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqtea() {
    return piqtea.setScale(DECIMAL_PIQTEA, RoundingMode.HALF_UP);
  }
  
  public void setPiqtel(BigDecimal pPiqtel) {
    if (pPiqtel == null) {
      return;
    }
    piqtel = pPiqtel.setScale(DECIMAL_PIQTEL, RoundingMode.HALF_UP);
  }
  
  public void setPiqtel(Double pPiqtel) {
    if (pPiqtel == null) {
      return;
    }
    piqtel = BigDecimal.valueOf(pPiqtel).setScale(DECIMAL_PIQTEL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqtel() {
    return piqtel.setScale(DECIMAL_PIQTEL, RoundingMode.HALF_UP);
  }
  
  public void setPil001(String pPil001) {
    if (pPil001 == null) {
      return;
    }
    pil001 = pPil001;
  }
  
  public String getPil001() {
    return pil001;
  }
  
  public void setPiq001(BigDecimal pPiq001) {
    if (pPiq001 == null) {
      return;
    }
    piq001 = pPiq001.setScale(DECIMAL_PIQ001, RoundingMode.HALF_UP);
  }
  
  public void setPiq001(Double pPiq001) {
    if (pPiq001 == null) {
      return;
    }
    piq001 = BigDecimal.valueOf(pPiq001).setScale(DECIMAL_PIQ001, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq001() {
    return piq001.setScale(DECIMAL_PIQ001, RoundingMode.HALF_UP);
  }
  
  public void setPin001(BigDecimal pPin001) {
    if (pPin001 == null) {
      return;
    }
    pin001 = pPin001.setScale(DECIMAL_PIN001, RoundingMode.HALF_UP);
  }
  
  public void setPin001(Double pPin001) {
    if (pPin001 == null) {
      return;
    }
    pin001 = BigDecimal.valueOf(pPin001).setScale(DECIMAL_PIN001, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin001() {
    return pin001.setScale(DECIMAL_PIN001, RoundingMode.HALF_UP);
  }
  
  public void setPil002(String pPil002) {
    if (pPil002 == null) {
      return;
    }
    pil002 = pPil002;
  }
  
  public String getPil002() {
    return pil002;
  }
  
  public void setPiq002(BigDecimal pPiq002) {
    if (pPiq002 == null) {
      return;
    }
    piq002 = pPiq002.setScale(DECIMAL_PIQ002, RoundingMode.HALF_UP);
  }
  
  public void setPiq002(Double pPiq002) {
    if (pPiq002 == null) {
      return;
    }
    piq002 = BigDecimal.valueOf(pPiq002).setScale(DECIMAL_PIQ002, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq002() {
    return piq002.setScale(DECIMAL_PIQ002, RoundingMode.HALF_UP);
  }
  
  public void setPin002(BigDecimal pPin002) {
    if (pPin002 == null) {
      return;
    }
    pin002 = pPin002.setScale(DECIMAL_PIN002, RoundingMode.HALF_UP);
  }
  
  public void setPin002(Double pPin002) {
    if (pPin002 == null) {
      return;
    }
    pin002 = BigDecimal.valueOf(pPin002).setScale(DECIMAL_PIN002, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin002() {
    return pin002.setScale(DECIMAL_PIN002, RoundingMode.HALF_UP);
  }
  
  public void setPil003(String pPil003) {
    if (pPil003 == null) {
      return;
    }
    pil003 = pPil003;
  }
  
  public String getPil003() {
    return pil003;
  }
  
  public void setPiq003(BigDecimal pPiq003) {
    if (pPiq003 == null) {
      return;
    }
    piq003 = pPiq003.setScale(DECIMAL_PIQ003, RoundingMode.HALF_UP);
  }
  
  public void setPiq003(Double pPiq003) {
    if (pPiq003 == null) {
      return;
    }
    piq003 = BigDecimal.valueOf(pPiq003).setScale(DECIMAL_PIQ003, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq003() {
    return piq003.setScale(DECIMAL_PIQ003, RoundingMode.HALF_UP);
  }
  
  public void setPin003(BigDecimal pPin003) {
    if (pPin003 == null) {
      return;
    }
    pin003 = pPin003.setScale(DECIMAL_PIN003, RoundingMode.HALF_UP);
  }
  
  public void setPin003(Double pPin003) {
    if (pPin003 == null) {
      return;
    }
    pin003 = BigDecimal.valueOf(pPin003).setScale(DECIMAL_PIN003, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin003() {
    return pin003.setScale(DECIMAL_PIN003, RoundingMode.HALF_UP);
  }
  
  public void setPil004(String pPil004) {
    if (pPil004 == null) {
      return;
    }
    pil004 = pPil004;
  }
  
  public String getPil004() {
    return pil004;
  }
  
  public void setPiq004(BigDecimal pPiq004) {
    if (pPiq004 == null) {
      return;
    }
    piq004 = pPiq004.setScale(DECIMAL_PIQ004, RoundingMode.HALF_UP);
  }
  
  public void setPiq004(Double pPiq004) {
    if (pPiq004 == null) {
      return;
    }
    piq004 = BigDecimal.valueOf(pPiq004).setScale(DECIMAL_PIQ004, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq004() {
    return piq004.setScale(DECIMAL_PIQ004, RoundingMode.HALF_UP);
  }
  
  public void setPin004(BigDecimal pPin004) {
    if (pPin004 == null) {
      return;
    }
    pin004 = pPin004.setScale(DECIMAL_PIN004, RoundingMode.HALF_UP);
  }
  
  public void setPin004(Double pPin004) {
    if (pPin004 == null) {
      return;
    }
    pin004 = BigDecimal.valueOf(pPin004).setScale(DECIMAL_PIN004, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin004() {
    return pin004.setScale(DECIMAL_PIN004, RoundingMode.HALF_UP);
  }
  
  public void setPil005(String pPil005) {
    if (pPil005 == null) {
      return;
    }
    pil005 = pPil005;
  }
  
  public String getPil005() {
    return pil005;
  }
  
  public void setPiq005(BigDecimal pPiq005) {
    if (pPiq005 == null) {
      return;
    }
    piq005 = pPiq005.setScale(DECIMAL_PIQ005, RoundingMode.HALF_UP);
  }
  
  public void setPiq005(Double pPiq005) {
    if (pPiq005 == null) {
      return;
    }
    piq005 = BigDecimal.valueOf(pPiq005).setScale(DECIMAL_PIQ005, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq005() {
    return piq005.setScale(DECIMAL_PIQ005, RoundingMode.HALF_UP);
  }
  
  public void setPin005(BigDecimal pPin005) {
    if (pPin005 == null) {
      return;
    }
    pin005 = pPin005.setScale(DECIMAL_PIN005, RoundingMode.HALF_UP);
  }
  
  public void setPin005(Double pPin005) {
    if (pPin005 == null) {
      return;
    }
    pin005 = BigDecimal.valueOf(pPin005).setScale(DECIMAL_PIN005, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin005() {
    return pin005.setScale(DECIMAL_PIN005, RoundingMode.HALF_UP);
  }
  
  public void setPil006(String pPil006) {
    if (pPil006 == null) {
      return;
    }
    pil006 = pPil006;
  }
  
  public String getPil006() {
    return pil006;
  }
  
  public void setPiq006(BigDecimal pPiq006) {
    if (pPiq006 == null) {
      return;
    }
    piq006 = pPiq006.setScale(DECIMAL_PIQ006, RoundingMode.HALF_UP);
  }
  
  public void setPiq006(Double pPiq006) {
    if (pPiq006 == null) {
      return;
    }
    piq006 = BigDecimal.valueOf(pPiq006).setScale(DECIMAL_PIQ006, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq006() {
    return piq006.setScale(DECIMAL_PIQ006, RoundingMode.HALF_UP);
  }
  
  public void setPin006(BigDecimal pPin006) {
    if (pPin006 == null) {
      return;
    }
    pin006 = pPin006.setScale(DECIMAL_PIN006, RoundingMode.HALF_UP);
  }
  
  public void setPin006(Double pPin006) {
    if (pPin006 == null) {
      return;
    }
    pin006 = BigDecimal.valueOf(pPin006).setScale(DECIMAL_PIN006, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin006() {
    return pin006.setScale(DECIMAL_PIN006, RoundingMode.HALF_UP);
  }
  
  public void setPil007(String pPil007) {
    if (pPil007 == null) {
      return;
    }
    pil007 = pPil007;
  }
  
  public String getPil007() {
    return pil007;
  }
  
  public void setPiq007(BigDecimal pPiq007) {
    if (pPiq007 == null) {
      return;
    }
    piq007 = pPiq007.setScale(DECIMAL_PIQ007, RoundingMode.HALF_UP);
  }
  
  public void setPiq007(Double pPiq007) {
    if (pPiq007 == null) {
      return;
    }
    piq007 = BigDecimal.valueOf(pPiq007).setScale(DECIMAL_PIQ007, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq007() {
    return piq007.setScale(DECIMAL_PIQ007, RoundingMode.HALF_UP);
  }
  
  public void setPin007(BigDecimal pPin007) {
    if (pPin007 == null) {
      return;
    }
    pin007 = pPin007.setScale(DECIMAL_PIN007, RoundingMode.HALF_UP);
  }
  
  public void setPin007(Double pPin007) {
    if (pPin007 == null) {
      return;
    }
    pin007 = BigDecimal.valueOf(pPin007).setScale(DECIMAL_PIN007, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin007() {
    return pin007.setScale(DECIMAL_PIN007, RoundingMode.HALF_UP);
  }
  
  public void setPil008(String pPil008) {
    if (pPil008 == null) {
      return;
    }
    pil008 = pPil008;
  }
  
  public String getPil008() {
    return pil008;
  }
  
  public void setPiq008(BigDecimal pPiq008) {
    if (pPiq008 == null) {
      return;
    }
    piq008 = pPiq008.setScale(DECIMAL_PIQ008, RoundingMode.HALF_UP);
  }
  
  public void setPiq008(Double pPiq008) {
    if (pPiq008 == null) {
      return;
    }
    piq008 = BigDecimal.valueOf(pPiq008).setScale(DECIMAL_PIQ008, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq008() {
    return piq008.setScale(DECIMAL_PIQ008, RoundingMode.HALF_UP);
  }
  
  public void setPin008(BigDecimal pPin008) {
    if (pPin008 == null) {
      return;
    }
    pin008 = pPin008.setScale(DECIMAL_PIN008, RoundingMode.HALF_UP);
  }
  
  public void setPin008(Double pPin008) {
    if (pPin008 == null) {
      return;
    }
    pin008 = BigDecimal.valueOf(pPin008).setScale(DECIMAL_PIN008, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin008() {
    return pin008.setScale(DECIMAL_PIN008, RoundingMode.HALF_UP);
  }
  
  public void setPil009(String pPil009) {
    if (pPil009 == null) {
      return;
    }
    pil009 = pPil009;
  }
  
  public String getPil009() {
    return pil009;
  }
  
  public void setPiq009(BigDecimal pPiq009) {
    if (pPiq009 == null) {
      return;
    }
    piq009 = pPiq009.setScale(DECIMAL_PIQ009, RoundingMode.HALF_UP);
  }
  
  public void setPiq009(Double pPiq009) {
    if (pPiq009 == null) {
      return;
    }
    piq009 = BigDecimal.valueOf(pPiq009).setScale(DECIMAL_PIQ009, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq009() {
    return piq009.setScale(DECIMAL_PIQ009, RoundingMode.HALF_UP);
  }
  
  public void setPin009(BigDecimal pPin009) {
    if (pPin009 == null) {
      return;
    }
    pin009 = pPin009.setScale(DECIMAL_PIN009, RoundingMode.HALF_UP);
  }
  
  public void setPin009(Double pPin009) {
    if (pPin009 == null) {
      return;
    }
    pin009 = BigDecimal.valueOf(pPin009).setScale(DECIMAL_PIN009, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin009() {
    return pin009.setScale(DECIMAL_PIN009, RoundingMode.HALF_UP);
  }
  
  public void setPil010(String pPil010) {
    if (pPil010 == null) {
      return;
    }
    pil010 = pPil010;
  }
  
  public String getPil010() {
    return pil010;
  }
  
  public void setPiq010(BigDecimal pPiq010) {
    if (pPiq010 == null) {
      return;
    }
    piq010 = pPiq010.setScale(DECIMAL_PIQ010, RoundingMode.HALF_UP);
  }
  
  public void setPiq010(Double pPiq010) {
    if (pPiq010 == null) {
      return;
    }
    piq010 = BigDecimal.valueOf(pPiq010).setScale(DECIMAL_PIQ010, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq010() {
    return piq010.setScale(DECIMAL_PIQ010, RoundingMode.HALF_UP);
  }
  
  public void setPin010(BigDecimal pPin010) {
    if (pPin010 == null) {
      return;
    }
    pin010 = pPin010.setScale(DECIMAL_PIN010, RoundingMode.HALF_UP);
  }
  
  public void setPin010(Double pPin010) {
    if (pPin010 == null) {
      return;
    }
    pin010 = BigDecimal.valueOf(pPin010).setScale(DECIMAL_PIN010, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin010() {
    return pin010.setScale(DECIMAL_PIN010, RoundingMode.HALF_UP);
  }
  
  public void setPil011(String pPil011) {
    if (pPil011 == null) {
      return;
    }
    pil011 = pPil011;
  }
  
  public String getPil011() {
    return pil011;
  }
  
  public void setPiq011(BigDecimal pPiq011) {
    if (pPiq011 == null) {
      return;
    }
    piq011 = pPiq011.setScale(DECIMAL_PIQ011, RoundingMode.HALF_UP);
  }
  
  public void setPiq011(Double pPiq011) {
    if (pPiq011 == null) {
      return;
    }
    piq011 = BigDecimal.valueOf(pPiq011).setScale(DECIMAL_PIQ011, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq011() {
    return piq011.setScale(DECIMAL_PIQ011, RoundingMode.HALF_UP);
  }
  
  public void setPin011(BigDecimal pPin011) {
    if (pPin011 == null) {
      return;
    }
    pin011 = pPin011.setScale(DECIMAL_PIN011, RoundingMode.HALF_UP);
  }
  
  public void setPin011(Double pPin011) {
    if (pPin011 == null) {
      return;
    }
    pin011 = BigDecimal.valueOf(pPin011).setScale(DECIMAL_PIN011, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin011() {
    return pin011.setScale(DECIMAL_PIN011, RoundingMode.HALF_UP);
  }
  
  public void setPil012(String pPil012) {
    if (pPil012 == null) {
      return;
    }
    pil012 = pPil012;
  }
  
  public String getPil012() {
    return pil012;
  }
  
  public void setPiq012(BigDecimal pPiq012) {
    if (pPiq012 == null) {
      return;
    }
    piq012 = pPiq012.setScale(DECIMAL_PIQ012, RoundingMode.HALF_UP);
  }
  
  public void setPiq012(Double pPiq012) {
    if (pPiq012 == null) {
      return;
    }
    piq012 = BigDecimal.valueOf(pPiq012).setScale(DECIMAL_PIQ012, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq012() {
    return piq012.setScale(DECIMAL_PIQ012, RoundingMode.HALF_UP);
  }
  
  public void setPin012(BigDecimal pPin012) {
    if (pPin012 == null) {
      return;
    }
    pin012 = pPin012.setScale(DECIMAL_PIN012, RoundingMode.HALF_UP);
  }
  
  public void setPin012(Double pPin012) {
    if (pPin012 == null) {
      return;
    }
    pin012 = BigDecimal.valueOf(pPin012).setScale(DECIMAL_PIN012, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin012() {
    return pin012.setScale(DECIMAL_PIN012, RoundingMode.HALF_UP);
  }
  
  public void setPil013(String pPil013) {
    if (pPil013 == null) {
      return;
    }
    pil013 = pPil013;
  }
  
  public String getPil013() {
    return pil013;
  }
  
  public void setPiq013(BigDecimal pPiq013) {
    if (pPiq013 == null) {
      return;
    }
    piq013 = pPiq013.setScale(DECIMAL_PIQ013, RoundingMode.HALF_UP);
  }
  
  public void setPiq013(Double pPiq013) {
    if (pPiq013 == null) {
      return;
    }
    piq013 = BigDecimal.valueOf(pPiq013).setScale(DECIMAL_PIQ013, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq013() {
    return piq013.setScale(DECIMAL_PIQ013, RoundingMode.HALF_UP);
  }
  
  public void setPin013(BigDecimal pPin013) {
    if (pPin013 == null) {
      return;
    }
    pin013 = pPin013.setScale(DECIMAL_PIN013, RoundingMode.HALF_UP);
  }
  
  public void setPin013(Double pPin013) {
    if (pPin013 == null) {
      return;
    }
    pin013 = BigDecimal.valueOf(pPin013).setScale(DECIMAL_PIN013, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin013() {
    return pin013.setScale(DECIMAL_PIN013, RoundingMode.HALF_UP);
  }
  
  public void setPil014(String pPil014) {
    if (pPil014 == null) {
      return;
    }
    pil014 = pPil014;
  }
  
  public String getPil014() {
    return pil014;
  }
  
  public void setPiq014(BigDecimal pPiq014) {
    if (pPiq014 == null) {
      return;
    }
    piq014 = pPiq014.setScale(DECIMAL_PIQ014, RoundingMode.HALF_UP);
  }
  
  public void setPiq014(Double pPiq014) {
    if (pPiq014 == null) {
      return;
    }
    piq014 = BigDecimal.valueOf(pPiq014).setScale(DECIMAL_PIQ014, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq014() {
    return piq014.setScale(DECIMAL_PIQ014, RoundingMode.HALF_UP);
  }
  
  public void setPin014(BigDecimal pPin014) {
    if (pPin014 == null) {
      return;
    }
    pin014 = pPin014.setScale(DECIMAL_PIN014, RoundingMode.HALF_UP);
  }
  
  public void setPin014(Double pPin014) {
    if (pPin014 == null) {
      return;
    }
    pin014 = BigDecimal.valueOf(pPin014).setScale(DECIMAL_PIN014, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin014() {
    return pin014.setScale(DECIMAL_PIN014, RoundingMode.HALF_UP);
  }
  
  public void setPil015(String pPil015) {
    if (pPil015 == null) {
      return;
    }
    pil015 = pPil015;
  }
  
  public String getPil015() {
    return pil015;
  }
  
  public void setPiq015(BigDecimal pPiq015) {
    if (pPiq015 == null) {
      return;
    }
    piq015 = pPiq015.setScale(DECIMAL_PIQ015, RoundingMode.HALF_UP);
  }
  
  public void setPiq015(Double pPiq015) {
    if (pPiq015 == null) {
      return;
    }
    piq015 = BigDecimal.valueOf(pPiq015).setScale(DECIMAL_PIQ015, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq015() {
    return piq015.setScale(DECIMAL_PIQ015, RoundingMode.HALF_UP);
  }
  
  public void setPin015(BigDecimal pPin015) {
    if (pPin015 == null) {
      return;
    }
    pin015 = pPin015.setScale(DECIMAL_PIN015, RoundingMode.HALF_UP);
  }
  
  public void setPin015(Double pPin015) {
    if (pPin015 == null) {
      return;
    }
    pin015 = BigDecimal.valueOf(pPin015).setScale(DECIMAL_PIN015, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin015() {
    return pin015.setScale(DECIMAL_PIN015, RoundingMode.HALF_UP);
  }
  
  public void setPil016(String pPil016) {
    if (pPil016 == null) {
      return;
    }
    pil016 = pPil016;
  }
  
  public String getPil016() {
    return pil016;
  }
  
  public void setPiq016(BigDecimal pPiq016) {
    if (pPiq016 == null) {
      return;
    }
    piq016 = pPiq016.setScale(DECIMAL_PIQ016, RoundingMode.HALF_UP);
  }
  
  public void setPiq016(Double pPiq016) {
    if (pPiq016 == null) {
      return;
    }
    piq016 = BigDecimal.valueOf(pPiq016).setScale(DECIMAL_PIQ016, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq016() {
    return piq016.setScale(DECIMAL_PIQ016, RoundingMode.HALF_UP);
  }
  
  public void setPin016(BigDecimal pPin016) {
    if (pPin016 == null) {
      return;
    }
    pin016 = pPin016.setScale(DECIMAL_PIN016, RoundingMode.HALF_UP);
  }
  
  public void setPin016(Double pPin016) {
    if (pPin016 == null) {
      return;
    }
    pin016 = BigDecimal.valueOf(pPin016).setScale(DECIMAL_PIN016, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin016() {
    return pin016.setScale(DECIMAL_PIN016, RoundingMode.HALF_UP);
  }
  
  public void setPil017(String pPil017) {
    if (pPil017 == null) {
      return;
    }
    pil017 = pPil017;
  }
  
  public String getPil017() {
    return pil017;
  }
  
  public void setPiq017(BigDecimal pPiq017) {
    if (pPiq017 == null) {
      return;
    }
    piq017 = pPiq017.setScale(DECIMAL_PIQ017, RoundingMode.HALF_UP);
  }
  
  public void setPiq017(Double pPiq017) {
    if (pPiq017 == null) {
      return;
    }
    piq017 = BigDecimal.valueOf(pPiq017).setScale(DECIMAL_PIQ017, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq017() {
    return piq017.setScale(DECIMAL_PIQ017, RoundingMode.HALF_UP);
  }
  
  public void setPin017(BigDecimal pPin017) {
    if (pPin017 == null) {
      return;
    }
    pin017 = pPin017.setScale(DECIMAL_PIN017, RoundingMode.HALF_UP);
  }
  
  public void setPin017(Double pPin017) {
    if (pPin017 == null) {
      return;
    }
    pin017 = BigDecimal.valueOf(pPin017).setScale(DECIMAL_PIN017, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin017() {
    return pin017.setScale(DECIMAL_PIN017, RoundingMode.HALF_UP);
  }
  
  public void setPil018(String pPil018) {
    if (pPil018 == null) {
      return;
    }
    pil018 = pPil018;
  }
  
  public String getPil018() {
    return pil018;
  }
  
  public void setPiq018(BigDecimal pPiq018) {
    if (pPiq018 == null) {
      return;
    }
    piq018 = pPiq018.setScale(DECIMAL_PIQ018, RoundingMode.HALF_UP);
  }
  
  public void setPiq018(Double pPiq018) {
    if (pPiq018 == null) {
      return;
    }
    piq018 = BigDecimal.valueOf(pPiq018).setScale(DECIMAL_PIQ018, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq018() {
    return piq018.setScale(DECIMAL_PIQ018, RoundingMode.HALF_UP);
  }
  
  public void setPin018(BigDecimal pPin018) {
    if (pPin018 == null) {
      return;
    }
    pin018 = pPin018.setScale(DECIMAL_PIN018, RoundingMode.HALF_UP);
  }
  
  public void setPin018(Double pPin018) {
    if (pPin018 == null) {
      return;
    }
    pin018 = BigDecimal.valueOf(pPin018).setScale(DECIMAL_PIN018, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin018() {
    return pin018.setScale(DECIMAL_PIN018, RoundingMode.HALF_UP);
  }
  
  public void setPil019(String pPil019) {
    if (pPil019 == null) {
      return;
    }
    pil019 = pPil019;
  }
  
  public String getPil019() {
    return pil019;
  }
  
  public void setPiq019(BigDecimal pPiq019) {
    if (pPiq019 == null) {
      return;
    }
    piq019 = pPiq019.setScale(DECIMAL_PIQ019, RoundingMode.HALF_UP);
  }
  
  public void setPiq019(Double pPiq019) {
    if (pPiq019 == null) {
      return;
    }
    piq019 = BigDecimal.valueOf(pPiq019).setScale(DECIMAL_PIQ019, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq019() {
    return piq019.setScale(DECIMAL_PIQ019, RoundingMode.HALF_UP);
  }
  
  public void setPin019(BigDecimal pPin019) {
    if (pPin019 == null) {
      return;
    }
    pin019 = pPin019.setScale(DECIMAL_PIN019, RoundingMode.HALF_UP);
  }
  
  public void setPin019(Double pPin019) {
    if (pPin019 == null) {
      return;
    }
    pin019 = BigDecimal.valueOf(pPin019).setScale(DECIMAL_PIN019, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin019() {
    return pin019.setScale(DECIMAL_PIN019, RoundingMode.HALF_UP);
  }
  
  public void setPil020(String pPil020) {
    if (pPil020 == null) {
      return;
    }
    pil020 = pPil020;
  }
  
  public String getPil020() {
    return pil020;
  }
  
  public void setPiq020(BigDecimal pPiq020) {
    if (pPiq020 == null) {
      return;
    }
    piq020 = pPiq020.setScale(DECIMAL_PIQ020, RoundingMode.HALF_UP);
  }
  
  public void setPiq020(Double pPiq020) {
    if (pPiq020 == null) {
      return;
    }
    piq020 = BigDecimal.valueOf(pPiq020).setScale(DECIMAL_PIQ020, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq020() {
    return piq020.setScale(DECIMAL_PIQ020, RoundingMode.HALF_UP);
  }
  
  public void setPin020(BigDecimal pPin020) {
    if (pPin020 == null) {
      return;
    }
    pin020 = pPin020.setScale(DECIMAL_PIN020, RoundingMode.HALF_UP);
  }
  
  public void setPin020(Double pPin020) {
    if (pPin020 == null) {
      return;
    }
    pin020 = BigDecimal.valueOf(pPin020).setScale(DECIMAL_PIN020, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin020() {
    return pin020.setScale(DECIMAL_PIN020, RoundingMode.HALF_UP);
  }
  
  public void setPil021(String pPil021) {
    if (pPil021 == null) {
      return;
    }
    pil021 = pPil021;
  }
  
  public String getPil021() {
    return pil021;
  }
  
  public void setPiq021(BigDecimal pPiq021) {
    if (pPiq021 == null) {
      return;
    }
    piq021 = pPiq021.setScale(DECIMAL_PIQ021, RoundingMode.HALF_UP);
  }
  
  public void setPiq021(Double pPiq021) {
    if (pPiq021 == null) {
      return;
    }
    piq021 = BigDecimal.valueOf(pPiq021).setScale(DECIMAL_PIQ021, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq021() {
    return piq021.setScale(DECIMAL_PIQ021, RoundingMode.HALF_UP);
  }
  
  public void setPin021(BigDecimal pPin021) {
    if (pPin021 == null) {
      return;
    }
    pin021 = pPin021.setScale(DECIMAL_PIN021, RoundingMode.HALF_UP);
  }
  
  public void setPin021(Double pPin021) {
    if (pPin021 == null) {
      return;
    }
    pin021 = BigDecimal.valueOf(pPin021).setScale(DECIMAL_PIN021, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin021() {
    return pin021.setScale(DECIMAL_PIN021, RoundingMode.HALF_UP);
  }
  
  public void setPil022(String pPil022) {
    if (pPil022 == null) {
      return;
    }
    pil022 = pPil022;
  }
  
  public String getPil022() {
    return pil022;
  }
  
  public void setPiq022(BigDecimal pPiq022) {
    if (pPiq022 == null) {
      return;
    }
    piq022 = pPiq022.setScale(DECIMAL_PIQ022, RoundingMode.HALF_UP);
  }
  
  public void setPiq022(Double pPiq022) {
    if (pPiq022 == null) {
      return;
    }
    piq022 = BigDecimal.valueOf(pPiq022).setScale(DECIMAL_PIQ022, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq022() {
    return piq022.setScale(DECIMAL_PIQ022, RoundingMode.HALF_UP);
  }
  
  public void setPin022(BigDecimal pPin022) {
    if (pPin022 == null) {
      return;
    }
    pin022 = pPin022.setScale(DECIMAL_PIN022, RoundingMode.HALF_UP);
  }
  
  public void setPin022(Double pPin022) {
    if (pPin022 == null) {
      return;
    }
    pin022 = BigDecimal.valueOf(pPin022).setScale(DECIMAL_PIN022, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin022() {
    return pin022.setScale(DECIMAL_PIN022, RoundingMode.HALF_UP);
  }
  
  public void setPil023(String pPil023) {
    if (pPil023 == null) {
      return;
    }
    pil023 = pPil023;
  }
  
  public String getPil023() {
    return pil023;
  }
  
  public void setPiq023(BigDecimal pPiq023) {
    if (pPiq023 == null) {
      return;
    }
    piq023 = pPiq023.setScale(DECIMAL_PIQ023, RoundingMode.HALF_UP);
  }
  
  public void setPiq023(Double pPiq023) {
    if (pPiq023 == null) {
      return;
    }
    piq023 = BigDecimal.valueOf(pPiq023).setScale(DECIMAL_PIQ023, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq023() {
    return piq023.setScale(DECIMAL_PIQ023, RoundingMode.HALF_UP);
  }
  
  public void setPin023(BigDecimal pPin023) {
    if (pPin023 == null) {
      return;
    }
    pin023 = pPin023.setScale(DECIMAL_PIN023, RoundingMode.HALF_UP);
  }
  
  public void setPin023(Double pPin023) {
    if (pPin023 == null) {
      return;
    }
    pin023 = BigDecimal.valueOf(pPin023).setScale(DECIMAL_PIN023, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin023() {
    return pin023.setScale(DECIMAL_PIN023, RoundingMode.HALF_UP);
  }
  
  public void setPil024(String pPil024) {
    if (pPil024 == null) {
      return;
    }
    pil024 = pPil024;
  }
  
  public String getPil024() {
    return pil024;
  }
  
  public void setPiq024(BigDecimal pPiq024) {
    if (pPiq024 == null) {
      return;
    }
    piq024 = pPiq024.setScale(DECIMAL_PIQ024, RoundingMode.HALF_UP);
  }
  
  public void setPiq024(Double pPiq024) {
    if (pPiq024 == null) {
      return;
    }
    piq024 = BigDecimal.valueOf(pPiq024).setScale(DECIMAL_PIQ024, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq024() {
    return piq024.setScale(DECIMAL_PIQ024, RoundingMode.HALF_UP);
  }
  
  public void setPin024(BigDecimal pPin024) {
    if (pPin024 == null) {
      return;
    }
    pin024 = pPin024.setScale(DECIMAL_PIN024, RoundingMode.HALF_UP);
  }
  
  public void setPin024(Double pPin024) {
    if (pPin024 == null) {
      return;
    }
    pin024 = BigDecimal.valueOf(pPin024).setScale(DECIMAL_PIN024, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin024() {
    return pin024.setScale(DECIMAL_PIN024, RoundingMode.HALF_UP);
  }
  
  public void setPil025(String pPil025) {
    if (pPil025 == null) {
      return;
    }
    pil025 = pPil025;
  }
  
  public String getPil025() {
    return pil025;
  }
  
  public void setPiq025(BigDecimal pPiq025) {
    if (pPiq025 == null) {
      return;
    }
    piq025 = pPiq025.setScale(DECIMAL_PIQ025, RoundingMode.HALF_UP);
  }
  
  public void setPiq025(Double pPiq025) {
    if (pPiq025 == null) {
      return;
    }
    piq025 = BigDecimal.valueOf(pPiq025).setScale(DECIMAL_PIQ025, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq025() {
    return piq025.setScale(DECIMAL_PIQ025, RoundingMode.HALF_UP);
  }
  
  public void setPin025(BigDecimal pPin025) {
    if (pPin025 == null) {
      return;
    }
    pin025 = pPin025.setScale(DECIMAL_PIN025, RoundingMode.HALF_UP);
  }
  
  public void setPin025(Double pPin025) {
    if (pPin025 == null) {
      return;
    }
    pin025 = BigDecimal.valueOf(pPin025).setScale(DECIMAL_PIN025, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin025() {
    return pin025.setScale(DECIMAL_PIN025, RoundingMode.HALF_UP);
  }
  
  public void setPil026(String pPil026) {
    if (pPil026 == null) {
      return;
    }
    pil026 = pPil026;
  }
  
  public String getPil026() {
    return pil026;
  }
  
  public void setPiq026(BigDecimal pPiq026) {
    if (pPiq026 == null) {
      return;
    }
    piq026 = pPiq026.setScale(DECIMAL_PIQ026, RoundingMode.HALF_UP);
  }
  
  public void setPiq026(Double pPiq026) {
    if (pPiq026 == null) {
      return;
    }
    piq026 = BigDecimal.valueOf(pPiq026).setScale(DECIMAL_PIQ026, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq026() {
    return piq026.setScale(DECIMAL_PIQ026, RoundingMode.HALF_UP);
  }
  
  public void setPin026(BigDecimal pPin026) {
    if (pPin026 == null) {
      return;
    }
    pin026 = pPin026.setScale(DECIMAL_PIN026, RoundingMode.HALF_UP);
  }
  
  public void setPin026(Double pPin026) {
    if (pPin026 == null) {
      return;
    }
    pin026 = BigDecimal.valueOf(pPin026).setScale(DECIMAL_PIN026, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin026() {
    return pin026.setScale(DECIMAL_PIN026, RoundingMode.HALF_UP);
  }
  
  public void setPil027(String pPil027) {
    if (pPil027 == null) {
      return;
    }
    pil027 = pPil027;
  }
  
  public String getPil027() {
    return pil027;
  }
  
  public void setPiq027(BigDecimal pPiq027) {
    if (pPiq027 == null) {
      return;
    }
    piq027 = pPiq027.setScale(DECIMAL_PIQ027, RoundingMode.HALF_UP);
  }
  
  public void setPiq027(Double pPiq027) {
    if (pPiq027 == null) {
      return;
    }
    piq027 = BigDecimal.valueOf(pPiq027).setScale(DECIMAL_PIQ027, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq027() {
    return piq027.setScale(DECIMAL_PIQ027, RoundingMode.HALF_UP);
  }
  
  public void setPin027(BigDecimal pPin027) {
    if (pPin027 == null) {
      return;
    }
    pin027 = pPin027.setScale(DECIMAL_PIN027, RoundingMode.HALF_UP);
  }
  
  public void setPin027(Double pPin027) {
    if (pPin027 == null) {
      return;
    }
    pin027 = BigDecimal.valueOf(pPin027).setScale(DECIMAL_PIN027, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin027() {
    return pin027.setScale(DECIMAL_PIN027, RoundingMode.HALF_UP);
  }
  
  public void setPil028(String pPil028) {
    if (pPil028 == null) {
      return;
    }
    pil028 = pPil028;
  }
  
  public String getPil028() {
    return pil028;
  }
  
  public void setPiq028(BigDecimal pPiq028) {
    if (pPiq028 == null) {
      return;
    }
    piq028 = pPiq028.setScale(DECIMAL_PIQ028, RoundingMode.HALF_UP);
  }
  
  public void setPiq028(Double pPiq028) {
    if (pPiq028 == null) {
      return;
    }
    piq028 = BigDecimal.valueOf(pPiq028).setScale(DECIMAL_PIQ028, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq028() {
    return piq028.setScale(DECIMAL_PIQ028, RoundingMode.HALF_UP);
  }
  
  public void setPin028(BigDecimal pPin028) {
    if (pPin028 == null) {
      return;
    }
    pin028 = pPin028.setScale(DECIMAL_PIN028, RoundingMode.HALF_UP);
  }
  
  public void setPin028(Double pPin028) {
    if (pPin028 == null) {
      return;
    }
    pin028 = BigDecimal.valueOf(pPin028).setScale(DECIMAL_PIN028, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin028() {
    return pin028.setScale(DECIMAL_PIN028, RoundingMode.HALF_UP);
  }
  
  public void setPil029(String pPil029) {
    if (pPil029 == null) {
      return;
    }
    pil029 = pPil029;
  }
  
  public String getPil029() {
    return pil029;
  }
  
  public void setPiq029(BigDecimal pPiq029) {
    if (pPiq029 == null) {
      return;
    }
    piq029 = pPiq029.setScale(DECIMAL_PIQ029, RoundingMode.HALF_UP);
  }
  
  public void setPiq029(Double pPiq029) {
    if (pPiq029 == null) {
      return;
    }
    piq029 = BigDecimal.valueOf(pPiq029).setScale(DECIMAL_PIQ029, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq029() {
    return piq029.setScale(DECIMAL_PIQ029, RoundingMode.HALF_UP);
  }
  
  public void setPin029(BigDecimal pPin029) {
    if (pPin029 == null) {
      return;
    }
    pin029 = pPin029.setScale(DECIMAL_PIN029, RoundingMode.HALF_UP);
  }
  
  public void setPin029(Double pPin029) {
    if (pPin029 == null) {
      return;
    }
    pin029 = BigDecimal.valueOf(pPin029).setScale(DECIMAL_PIN029, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin029() {
    return pin029.setScale(DECIMAL_PIN029, RoundingMode.HALF_UP);
  }
  
  public void setPil030(String pPil030) {
    if (pPil030 == null) {
      return;
    }
    pil030 = pPil030;
  }
  
  public String getPil030() {
    return pil030;
  }
  
  public void setPiq030(BigDecimal pPiq030) {
    if (pPiq030 == null) {
      return;
    }
    piq030 = pPiq030.setScale(DECIMAL_PIQ030, RoundingMode.HALF_UP);
  }
  
  public void setPiq030(Double pPiq030) {
    if (pPiq030 == null) {
      return;
    }
    piq030 = BigDecimal.valueOf(pPiq030).setScale(DECIMAL_PIQ030, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq030() {
    return piq030.setScale(DECIMAL_PIQ030, RoundingMode.HALF_UP);
  }
  
  public void setPin030(BigDecimal pPin030) {
    if (pPin030 == null) {
      return;
    }
    pin030 = pPin030.setScale(DECIMAL_PIN030, RoundingMode.HALF_UP);
  }
  
  public void setPin030(Double pPin030) {
    if (pPin030 == null) {
      return;
    }
    pin030 = BigDecimal.valueOf(pPin030).setScale(DECIMAL_PIN030, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin030() {
    return pin030.setScale(DECIMAL_PIN030, RoundingMode.HALF_UP);
  }
  
  public void setPil031(String pPil031) {
    if (pPil031 == null) {
      return;
    }
    pil031 = pPil031;
  }
  
  public String getPil031() {
    return pil031;
  }
  
  public void setPiq031(BigDecimal pPiq031) {
    if (pPiq031 == null) {
      return;
    }
    piq031 = pPiq031.setScale(DECIMAL_PIQ031, RoundingMode.HALF_UP);
  }
  
  public void setPiq031(Double pPiq031) {
    if (pPiq031 == null) {
      return;
    }
    piq031 = BigDecimal.valueOf(pPiq031).setScale(DECIMAL_PIQ031, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq031() {
    return piq031.setScale(DECIMAL_PIQ031, RoundingMode.HALF_UP);
  }
  
  public void setPin031(BigDecimal pPin031) {
    if (pPin031 == null) {
      return;
    }
    pin031 = pPin031.setScale(DECIMAL_PIN031, RoundingMode.HALF_UP);
  }
  
  public void setPin031(Double pPin031) {
    if (pPin031 == null) {
      return;
    }
    pin031 = BigDecimal.valueOf(pPin031).setScale(DECIMAL_PIN031, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin031() {
    return pin031.setScale(DECIMAL_PIN031, RoundingMode.HALF_UP);
  }
  
  public void setPil032(String pPil032) {
    if (pPil032 == null) {
      return;
    }
    pil032 = pPil032;
  }
  
  public String getPil032() {
    return pil032;
  }
  
  public void setPiq032(BigDecimal pPiq032) {
    if (pPiq032 == null) {
      return;
    }
    piq032 = pPiq032.setScale(DECIMAL_PIQ032, RoundingMode.HALF_UP);
  }
  
  public void setPiq032(Double pPiq032) {
    if (pPiq032 == null) {
      return;
    }
    piq032 = BigDecimal.valueOf(pPiq032).setScale(DECIMAL_PIQ032, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq032() {
    return piq032.setScale(DECIMAL_PIQ032, RoundingMode.HALF_UP);
  }
  
  public void setPin032(BigDecimal pPin032) {
    if (pPin032 == null) {
      return;
    }
    pin032 = pPin032.setScale(DECIMAL_PIN032, RoundingMode.HALF_UP);
  }
  
  public void setPin032(Double pPin032) {
    if (pPin032 == null) {
      return;
    }
    pin032 = BigDecimal.valueOf(pPin032).setScale(DECIMAL_PIN032, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin032() {
    return pin032.setScale(DECIMAL_PIN032, RoundingMode.HALF_UP);
  }
  
  public void setPil033(String pPil033) {
    if (pPil033 == null) {
      return;
    }
    pil033 = pPil033;
  }
  
  public String getPil033() {
    return pil033;
  }
  
  public void setPiq033(BigDecimal pPiq033) {
    if (pPiq033 == null) {
      return;
    }
    piq033 = pPiq033.setScale(DECIMAL_PIQ033, RoundingMode.HALF_UP);
  }
  
  public void setPiq033(Double pPiq033) {
    if (pPiq033 == null) {
      return;
    }
    piq033 = BigDecimal.valueOf(pPiq033).setScale(DECIMAL_PIQ033, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq033() {
    return piq033.setScale(DECIMAL_PIQ033, RoundingMode.HALF_UP);
  }
  
  public void setPin033(BigDecimal pPin033) {
    if (pPin033 == null) {
      return;
    }
    pin033 = pPin033.setScale(DECIMAL_PIN033, RoundingMode.HALF_UP);
  }
  
  public void setPin033(Double pPin033) {
    if (pPin033 == null) {
      return;
    }
    pin033 = BigDecimal.valueOf(pPin033).setScale(DECIMAL_PIN033, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin033() {
    return pin033.setScale(DECIMAL_PIN033, RoundingMode.HALF_UP);
  }
  
  public void setPil034(String pPil034) {
    if (pPil034 == null) {
      return;
    }
    pil034 = pPil034;
  }
  
  public String getPil034() {
    return pil034;
  }
  
  public void setPiq034(BigDecimal pPiq034) {
    if (pPiq034 == null) {
      return;
    }
    piq034 = pPiq034.setScale(DECIMAL_PIQ034, RoundingMode.HALF_UP);
  }
  
  public void setPiq034(Double pPiq034) {
    if (pPiq034 == null) {
      return;
    }
    piq034 = BigDecimal.valueOf(pPiq034).setScale(DECIMAL_PIQ034, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq034() {
    return piq034.setScale(DECIMAL_PIQ034, RoundingMode.HALF_UP);
  }
  
  public void setPin034(BigDecimal pPin034) {
    if (pPin034 == null) {
      return;
    }
    pin034 = pPin034.setScale(DECIMAL_PIN034, RoundingMode.HALF_UP);
  }
  
  public void setPin034(Double pPin034) {
    if (pPin034 == null) {
      return;
    }
    pin034 = BigDecimal.valueOf(pPin034).setScale(DECIMAL_PIN034, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin034() {
    return pin034.setScale(DECIMAL_PIN034, RoundingMode.HALF_UP);
  }
  
  public void setPil035(String pPil035) {
    if (pPil035 == null) {
      return;
    }
    pil035 = pPil035;
  }
  
  public String getPil035() {
    return pil035;
  }
  
  public void setPiq035(BigDecimal pPiq035) {
    if (pPiq035 == null) {
      return;
    }
    piq035 = pPiq035.setScale(DECIMAL_PIQ035, RoundingMode.HALF_UP);
  }
  
  public void setPiq035(Double pPiq035) {
    if (pPiq035 == null) {
      return;
    }
    piq035 = BigDecimal.valueOf(pPiq035).setScale(DECIMAL_PIQ035, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq035() {
    return piq035.setScale(DECIMAL_PIQ035, RoundingMode.HALF_UP);
  }
  
  public void setPin035(BigDecimal pPin035) {
    if (pPin035 == null) {
      return;
    }
    pin035 = pPin035.setScale(DECIMAL_PIN035, RoundingMode.HALF_UP);
  }
  
  public void setPin035(Double pPin035) {
    if (pPin035 == null) {
      return;
    }
    pin035 = BigDecimal.valueOf(pPin035).setScale(DECIMAL_PIN035, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin035() {
    return pin035.setScale(DECIMAL_PIN035, RoundingMode.HALF_UP);
  }
  
  public void setPil036(String pPil036) {
    if (pPil036 == null) {
      return;
    }
    pil036 = pPil036;
  }
  
  public String getPil036() {
    return pil036;
  }
  
  public void setPiq036(BigDecimal pPiq036) {
    if (pPiq036 == null) {
      return;
    }
    piq036 = pPiq036.setScale(DECIMAL_PIQ036, RoundingMode.HALF_UP);
  }
  
  public void setPiq036(Double pPiq036) {
    if (pPiq036 == null) {
      return;
    }
    piq036 = BigDecimal.valueOf(pPiq036).setScale(DECIMAL_PIQ036, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq036() {
    return piq036.setScale(DECIMAL_PIQ036, RoundingMode.HALF_UP);
  }
  
  public void setPin036(BigDecimal pPin036) {
    if (pPin036 == null) {
      return;
    }
    pin036 = pPin036.setScale(DECIMAL_PIN036, RoundingMode.HALF_UP);
  }
  
  public void setPin036(Double pPin036) {
    if (pPin036 == null) {
      return;
    }
    pin036 = BigDecimal.valueOf(pPin036).setScale(DECIMAL_PIN036, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin036() {
    return pin036.setScale(DECIMAL_PIN036, RoundingMode.HALF_UP);
  }
  
  public void setPil037(String pPil037) {
    if (pPil037 == null) {
      return;
    }
    pil037 = pPil037;
  }
  
  public String getPil037() {
    return pil037;
  }
  
  public void setPiq037(BigDecimal pPiq037) {
    if (pPiq037 == null) {
      return;
    }
    piq037 = pPiq037.setScale(DECIMAL_PIQ037, RoundingMode.HALF_UP);
  }
  
  public void setPiq037(Double pPiq037) {
    if (pPiq037 == null) {
      return;
    }
    piq037 = BigDecimal.valueOf(pPiq037).setScale(DECIMAL_PIQ037, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq037() {
    return piq037.setScale(DECIMAL_PIQ037, RoundingMode.HALF_UP);
  }
  
  public void setPin037(BigDecimal pPin037) {
    if (pPin037 == null) {
      return;
    }
    pin037 = pPin037.setScale(DECIMAL_PIN037, RoundingMode.HALF_UP);
  }
  
  public void setPin037(Double pPin037) {
    if (pPin037 == null) {
      return;
    }
    pin037 = BigDecimal.valueOf(pPin037).setScale(DECIMAL_PIN037, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin037() {
    return pin037.setScale(DECIMAL_PIN037, RoundingMode.HALF_UP);
  }
  
  public void setPil038(String pPil038) {
    if (pPil038 == null) {
      return;
    }
    pil038 = pPil038;
  }
  
  public String getPil038() {
    return pil038;
  }
  
  public void setPiq038(BigDecimal pPiq038) {
    if (pPiq038 == null) {
      return;
    }
    piq038 = pPiq038.setScale(DECIMAL_PIQ038, RoundingMode.HALF_UP);
  }
  
  public void setPiq038(Double pPiq038) {
    if (pPiq038 == null) {
      return;
    }
    piq038 = BigDecimal.valueOf(pPiq038).setScale(DECIMAL_PIQ038, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq038() {
    return piq038.setScale(DECIMAL_PIQ038, RoundingMode.HALF_UP);
  }
  
  public void setPin038(BigDecimal pPin038) {
    if (pPin038 == null) {
      return;
    }
    pin038 = pPin038.setScale(DECIMAL_PIN038, RoundingMode.HALF_UP);
  }
  
  public void setPin038(Double pPin038) {
    if (pPin038 == null) {
      return;
    }
    pin038 = BigDecimal.valueOf(pPin038).setScale(DECIMAL_PIN038, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin038() {
    return pin038.setScale(DECIMAL_PIN038, RoundingMode.HALF_UP);
  }
  
  public void setPil039(String pPil039) {
    if (pPil039 == null) {
      return;
    }
    pil039 = pPil039;
  }
  
  public String getPil039() {
    return pil039;
  }
  
  public void setPiq039(BigDecimal pPiq039) {
    if (pPiq039 == null) {
      return;
    }
    piq039 = pPiq039.setScale(DECIMAL_PIQ039, RoundingMode.HALF_UP);
  }
  
  public void setPiq039(Double pPiq039) {
    if (pPiq039 == null) {
      return;
    }
    piq039 = BigDecimal.valueOf(pPiq039).setScale(DECIMAL_PIQ039, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq039() {
    return piq039.setScale(DECIMAL_PIQ039, RoundingMode.HALF_UP);
  }
  
  public void setPin039(BigDecimal pPin039) {
    if (pPin039 == null) {
      return;
    }
    pin039 = pPin039.setScale(DECIMAL_PIN039, RoundingMode.HALF_UP);
  }
  
  public void setPin039(Double pPin039) {
    if (pPin039 == null) {
      return;
    }
    pin039 = BigDecimal.valueOf(pPin039).setScale(DECIMAL_PIN039, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin039() {
    return pin039.setScale(DECIMAL_PIN039, RoundingMode.HALF_UP);
  }
  
  public void setPil040(String pPil040) {
    if (pPil040 == null) {
      return;
    }
    pil040 = pPil040;
  }
  
  public String getPil040() {
    return pil040;
  }
  
  public void setPiq040(BigDecimal pPiq040) {
    if (pPiq040 == null) {
      return;
    }
    piq040 = pPiq040.setScale(DECIMAL_PIQ040, RoundingMode.HALF_UP);
  }
  
  public void setPiq040(Double pPiq040) {
    if (pPiq040 == null) {
      return;
    }
    piq040 = BigDecimal.valueOf(pPiq040).setScale(DECIMAL_PIQ040, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq040() {
    return piq040.setScale(DECIMAL_PIQ040, RoundingMode.HALF_UP);
  }
  
  public void setPin040(BigDecimal pPin040) {
    if (pPin040 == null) {
      return;
    }
    pin040 = pPin040.setScale(DECIMAL_PIN040, RoundingMode.HALF_UP);
  }
  
  public void setPin040(Double pPin040) {
    if (pPin040 == null) {
      return;
    }
    pin040 = BigDecimal.valueOf(pPin040).setScale(DECIMAL_PIN040, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin040() {
    return pin040.setScale(DECIMAL_PIN040, RoundingMode.HALF_UP);
  }
  
  public void setPil041(String pPil041) {
    if (pPil041 == null) {
      return;
    }
    pil041 = pPil041;
  }
  
  public String getPil041() {
    return pil041;
  }
  
  public void setPiq041(BigDecimal pPiq041) {
    if (pPiq041 == null) {
      return;
    }
    piq041 = pPiq041.setScale(DECIMAL_PIQ041, RoundingMode.HALF_UP);
  }
  
  public void setPiq041(Double pPiq041) {
    if (pPiq041 == null) {
      return;
    }
    piq041 = BigDecimal.valueOf(pPiq041).setScale(DECIMAL_PIQ041, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq041() {
    return piq041.setScale(DECIMAL_PIQ041, RoundingMode.HALF_UP);
  }
  
  public void setPin041(BigDecimal pPin041) {
    if (pPin041 == null) {
      return;
    }
    pin041 = pPin041.setScale(DECIMAL_PIN041, RoundingMode.HALF_UP);
  }
  
  public void setPin041(Double pPin041) {
    if (pPin041 == null) {
      return;
    }
    pin041 = BigDecimal.valueOf(pPin041).setScale(DECIMAL_PIN041, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin041() {
    return pin041.setScale(DECIMAL_PIN041, RoundingMode.HALF_UP);
  }
  
  public void setPil042(String pPil042) {
    if (pPil042 == null) {
      return;
    }
    pil042 = pPil042;
  }
  
  public String getPil042() {
    return pil042;
  }
  
  public void setPiq042(BigDecimal pPiq042) {
    if (pPiq042 == null) {
      return;
    }
    piq042 = pPiq042.setScale(DECIMAL_PIQ042, RoundingMode.HALF_UP);
  }
  
  public void setPiq042(Double pPiq042) {
    if (pPiq042 == null) {
      return;
    }
    piq042 = BigDecimal.valueOf(pPiq042).setScale(DECIMAL_PIQ042, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq042() {
    return piq042.setScale(DECIMAL_PIQ042, RoundingMode.HALF_UP);
  }
  
  public void setPin042(BigDecimal pPin042) {
    if (pPin042 == null) {
      return;
    }
    pin042 = pPin042.setScale(DECIMAL_PIN042, RoundingMode.HALF_UP);
  }
  
  public void setPin042(Double pPin042) {
    if (pPin042 == null) {
      return;
    }
    pin042 = BigDecimal.valueOf(pPin042).setScale(DECIMAL_PIN042, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin042() {
    return pin042.setScale(DECIMAL_PIN042, RoundingMode.HALF_UP);
  }
  
  public void setPil043(String pPil043) {
    if (pPil043 == null) {
      return;
    }
    pil043 = pPil043;
  }
  
  public String getPil043() {
    return pil043;
  }
  
  public void setPiq043(BigDecimal pPiq043) {
    if (pPiq043 == null) {
      return;
    }
    piq043 = pPiq043.setScale(DECIMAL_PIQ043, RoundingMode.HALF_UP);
  }
  
  public void setPiq043(Double pPiq043) {
    if (pPiq043 == null) {
      return;
    }
    piq043 = BigDecimal.valueOf(pPiq043).setScale(DECIMAL_PIQ043, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq043() {
    return piq043.setScale(DECIMAL_PIQ043, RoundingMode.HALF_UP);
  }
  
  public void setPin043(BigDecimal pPin043) {
    if (pPin043 == null) {
      return;
    }
    pin043 = pPin043.setScale(DECIMAL_PIN043, RoundingMode.HALF_UP);
  }
  
  public void setPin043(Double pPin043) {
    if (pPin043 == null) {
      return;
    }
    pin043 = BigDecimal.valueOf(pPin043).setScale(DECIMAL_PIN043, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin043() {
    return pin043.setScale(DECIMAL_PIN043, RoundingMode.HALF_UP);
  }
  
  public void setPil044(String pPil044) {
    if (pPil044 == null) {
      return;
    }
    pil044 = pPil044;
  }
  
  public String getPil044() {
    return pil044;
  }
  
  public void setPiq044(BigDecimal pPiq044) {
    if (pPiq044 == null) {
      return;
    }
    piq044 = pPiq044.setScale(DECIMAL_PIQ044, RoundingMode.HALF_UP);
  }
  
  public void setPiq044(Double pPiq044) {
    if (pPiq044 == null) {
      return;
    }
    piq044 = BigDecimal.valueOf(pPiq044).setScale(DECIMAL_PIQ044, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq044() {
    return piq044.setScale(DECIMAL_PIQ044, RoundingMode.HALF_UP);
  }
  
  public void setPin044(BigDecimal pPin044) {
    if (pPin044 == null) {
      return;
    }
    pin044 = pPin044.setScale(DECIMAL_PIN044, RoundingMode.HALF_UP);
  }
  
  public void setPin044(Double pPin044) {
    if (pPin044 == null) {
      return;
    }
    pin044 = BigDecimal.valueOf(pPin044).setScale(DECIMAL_PIN044, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin044() {
    return pin044.setScale(DECIMAL_PIN044, RoundingMode.HALF_UP);
  }
  
  public void setPil045(String pPil045) {
    if (pPil045 == null) {
      return;
    }
    pil045 = pPil045;
  }
  
  public String getPil045() {
    return pil045;
  }
  
  public void setPiq045(BigDecimal pPiq045) {
    if (pPiq045 == null) {
      return;
    }
    piq045 = pPiq045.setScale(DECIMAL_PIQ045, RoundingMode.HALF_UP);
  }
  
  public void setPiq045(Double pPiq045) {
    if (pPiq045 == null) {
      return;
    }
    piq045 = BigDecimal.valueOf(pPiq045).setScale(DECIMAL_PIQ045, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq045() {
    return piq045.setScale(DECIMAL_PIQ045, RoundingMode.HALF_UP);
  }
  
  public void setPin045(BigDecimal pPin045) {
    if (pPin045 == null) {
      return;
    }
    pin045 = pPin045.setScale(DECIMAL_PIN045, RoundingMode.HALF_UP);
  }
  
  public void setPin045(Double pPin045) {
    if (pPin045 == null) {
      return;
    }
    pin045 = BigDecimal.valueOf(pPin045).setScale(DECIMAL_PIN045, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin045() {
    return pin045.setScale(DECIMAL_PIN045, RoundingMode.HALF_UP);
  }
  
  public void setPil046(String pPil046) {
    if (pPil046 == null) {
      return;
    }
    pil046 = pPil046;
  }
  
  public String getPil046() {
    return pil046;
  }
  
  public void setPiq046(BigDecimal pPiq046) {
    if (pPiq046 == null) {
      return;
    }
    piq046 = pPiq046.setScale(DECIMAL_PIQ046, RoundingMode.HALF_UP);
  }
  
  public void setPiq046(Double pPiq046) {
    if (pPiq046 == null) {
      return;
    }
    piq046 = BigDecimal.valueOf(pPiq046).setScale(DECIMAL_PIQ046, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq046() {
    return piq046.setScale(DECIMAL_PIQ046, RoundingMode.HALF_UP);
  }
  
  public void setPin046(BigDecimal pPin046) {
    if (pPin046 == null) {
      return;
    }
    pin046 = pPin046.setScale(DECIMAL_PIN046, RoundingMode.HALF_UP);
  }
  
  public void setPin046(Double pPin046) {
    if (pPin046 == null) {
      return;
    }
    pin046 = BigDecimal.valueOf(pPin046).setScale(DECIMAL_PIN046, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin046() {
    return pin046.setScale(DECIMAL_PIN046, RoundingMode.HALF_UP);
  }
  
  public void setPil047(String pPil047) {
    if (pPil047 == null) {
      return;
    }
    pil047 = pPil047;
  }
  
  public String getPil047() {
    return pil047;
  }
  
  public void setPiq047(BigDecimal pPiq047) {
    if (pPiq047 == null) {
      return;
    }
    piq047 = pPiq047.setScale(DECIMAL_PIQ047, RoundingMode.HALF_UP);
  }
  
  public void setPiq047(Double pPiq047) {
    if (pPiq047 == null) {
      return;
    }
    piq047 = BigDecimal.valueOf(pPiq047).setScale(DECIMAL_PIQ047, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq047() {
    return piq047.setScale(DECIMAL_PIQ047, RoundingMode.HALF_UP);
  }
  
  public void setPin047(BigDecimal pPin047) {
    if (pPin047 == null) {
      return;
    }
    pin047 = pPin047.setScale(DECIMAL_PIN047, RoundingMode.HALF_UP);
  }
  
  public void setPin047(Double pPin047) {
    if (pPin047 == null) {
      return;
    }
    pin047 = BigDecimal.valueOf(pPin047).setScale(DECIMAL_PIN047, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin047() {
    return pin047.setScale(DECIMAL_PIN047, RoundingMode.HALF_UP);
  }
  
  public void setPil048(String pPil048) {
    if (pPil048 == null) {
      return;
    }
    pil048 = pPil048;
  }
  
  public String getPil048() {
    return pil048;
  }
  
  public void setPiq048(BigDecimal pPiq048) {
    if (pPiq048 == null) {
      return;
    }
    piq048 = pPiq048.setScale(DECIMAL_PIQ048, RoundingMode.HALF_UP);
  }
  
  public void setPiq048(Double pPiq048) {
    if (pPiq048 == null) {
      return;
    }
    piq048 = BigDecimal.valueOf(pPiq048).setScale(DECIMAL_PIQ048, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq048() {
    return piq048.setScale(DECIMAL_PIQ048, RoundingMode.HALF_UP);
  }
  
  public void setPin048(BigDecimal pPin048) {
    if (pPin048 == null) {
      return;
    }
    pin048 = pPin048.setScale(DECIMAL_PIN048, RoundingMode.HALF_UP);
  }
  
  public void setPin048(Double pPin048) {
    if (pPin048 == null) {
      return;
    }
    pin048 = BigDecimal.valueOf(pPin048).setScale(DECIMAL_PIN048, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin048() {
    return pin048.setScale(DECIMAL_PIN048, RoundingMode.HALF_UP);
  }
  
  public void setPil049(String pPil049) {
    if (pPil049 == null) {
      return;
    }
    pil049 = pPil049;
  }
  
  public String getPil049() {
    return pil049;
  }
  
  public void setPiq049(BigDecimal pPiq049) {
    if (pPiq049 == null) {
      return;
    }
    piq049 = pPiq049.setScale(DECIMAL_PIQ049, RoundingMode.HALF_UP);
  }
  
  public void setPiq049(Double pPiq049) {
    if (pPiq049 == null) {
      return;
    }
    piq049 = BigDecimal.valueOf(pPiq049).setScale(DECIMAL_PIQ049, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq049() {
    return piq049.setScale(DECIMAL_PIQ049, RoundingMode.HALF_UP);
  }
  
  public void setPin049(BigDecimal pPin049) {
    if (pPin049 == null) {
      return;
    }
    pin049 = pPin049.setScale(DECIMAL_PIN049, RoundingMode.HALF_UP);
  }
  
  public void setPin049(Double pPin049) {
    if (pPin049 == null) {
      return;
    }
    pin049 = BigDecimal.valueOf(pPin049).setScale(DECIMAL_PIN049, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin049() {
    return pin049.setScale(DECIMAL_PIN049, RoundingMode.HALF_UP);
  }
  
  public void setPil050(String pPil050) {
    if (pPil050 == null) {
      return;
    }
    pil050 = pPil050;
  }
  
  public String getPil050() {
    return pil050;
  }
  
  public void setPiq050(BigDecimal pPiq050) {
    if (pPiq050 == null) {
      return;
    }
    piq050 = pPiq050.setScale(DECIMAL_PIQ050, RoundingMode.HALF_UP);
  }
  
  public void setPiq050(Double pPiq050) {
    if (pPiq050 == null) {
      return;
    }
    piq050 = BigDecimal.valueOf(pPiq050).setScale(DECIMAL_PIQ050, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq050() {
    return piq050.setScale(DECIMAL_PIQ050, RoundingMode.HALF_UP);
  }
  
  public void setPin050(BigDecimal pPin050) {
    if (pPin050 == null) {
      return;
    }
    pin050 = pPin050.setScale(DECIMAL_PIN050, RoundingMode.HALF_UP);
  }
  
  public void setPin050(Double pPin050) {
    if (pPin050 == null) {
      return;
    }
    pin050 = BigDecimal.valueOf(pPin050).setScale(DECIMAL_PIN050, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin050() {
    return pin050.setScale(DECIMAL_PIN050, RoundingMode.HALF_UP);
  }
  
  public void setPil051(String pPil051) {
    if (pPil051 == null) {
      return;
    }
    pil051 = pPil051;
  }
  
  public String getPil051() {
    return pil051;
  }
  
  public void setPiq051(BigDecimal pPiq051) {
    if (pPiq051 == null) {
      return;
    }
    piq051 = pPiq051.setScale(DECIMAL_PIQ051, RoundingMode.HALF_UP);
  }
  
  public void setPiq051(Double pPiq051) {
    if (pPiq051 == null) {
      return;
    }
    piq051 = BigDecimal.valueOf(pPiq051).setScale(DECIMAL_PIQ051, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq051() {
    return piq051.setScale(DECIMAL_PIQ051, RoundingMode.HALF_UP);
  }
  
  public void setPin051(BigDecimal pPin051) {
    if (pPin051 == null) {
      return;
    }
    pin051 = pPin051.setScale(DECIMAL_PIN051, RoundingMode.HALF_UP);
  }
  
  public void setPin051(Double pPin051) {
    if (pPin051 == null) {
      return;
    }
    pin051 = BigDecimal.valueOf(pPin051).setScale(DECIMAL_PIN051, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin051() {
    return pin051.setScale(DECIMAL_PIN051, RoundingMode.HALF_UP);
  }
  
  public void setPil052(String pPil052) {
    if (pPil052 == null) {
      return;
    }
    pil052 = pPil052;
  }
  
  public String getPil052() {
    return pil052;
  }
  
  public void setPiq052(BigDecimal pPiq052) {
    if (pPiq052 == null) {
      return;
    }
    piq052 = pPiq052.setScale(DECIMAL_PIQ052, RoundingMode.HALF_UP);
  }
  
  public void setPiq052(Double pPiq052) {
    if (pPiq052 == null) {
      return;
    }
    piq052 = BigDecimal.valueOf(pPiq052).setScale(DECIMAL_PIQ052, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq052() {
    return piq052.setScale(DECIMAL_PIQ052, RoundingMode.HALF_UP);
  }
  
  public void setPin052(BigDecimal pPin052) {
    if (pPin052 == null) {
      return;
    }
    pin052 = pPin052.setScale(DECIMAL_PIN052, RoundingMode.HALF_UP);
  }
  
  public void setPin052(Double pPin052) {
    if (pPin052 == null) {
      return;
    }
    pin052 = BigDecimal.valueOf(pPin052).setScale(DECIMAL_PIN052, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin052() {
    return pin052.setScale(DECIMAL_PIN052, RoundingMode.HALF_UP);
  }
  
  public void setPil053(String pPil053) {
    if (pPil053 == null) {
      return;
    }
    pil053 = pPil053;
  }
  
  public String getPil053() {
    return pil053;
  }
  
  public void setPiq053(BigDecimal pPiq053) {
    if (pPiq053 == null) {
      return;
    }
    piq053 = pPiq053.setScale(DECIMAL_PIQ053, RoundingMode.HALF_UP);
  }
  
  public void setPiq053(Double pPiq053) {
    if (pPiq053 == null) {
      return;
    }
    piq053 = BigDecimal.valueOf(pPiq053).setScale(DECIMAL_PIQ053, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq053() {
    return piq053.setScale(DECIMAL_PIQ053, RoundingMode.HALF_UP);
  }
  
  public void setPin053(BigDecimal pPin053) {
    if (pPin053 == null) {
      return;
    }
    pin053 = pPin053.setScale(DECIMAL_PIN053, RoundingMode.HALF_UP);
  }
  
  public void setPin053(Double pPin053) {
    if (pPin053 == null) {
      return;
    }
    pin053 = BigDecimal.valueOf(pPin053).setScale(DECIMAL_PIN053, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin053() {
    return pin053.setScale(DECIMAL_PIN053, RoundingMode.HALF_UP);
  }
  
  public void setPil054(String pPil054) {
    if (pPil054 == null) {
      return;
    }
    pil054 = pPil054;
  }
  
  public String getPil054() {
    return pil054;
  }
  
  public void setPiq054(BigDecimal pPiq054) {
    if (pPiq054 == null) {
      return;
    }
    piq054 = pPiq054.setScale(DECIMAL_PIQ054, RoundingMode.HALF_UP);
  }
  
  public void setPiq054(Double pPiq054) {
    if (pPiq054 == null) {
      return;
    }
    piq054 = BigDecimal.valueOf(pPiq054).setScale(DECIMAL_PIQ054, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq054() {
    return piq054.setScale(DECIMAL_PIQ054, RoundingMode.HALF_UP);
  }
  
  public void setPin054(BigDecimal pPin054) {
    if (pPin054 == null) {
      return;
    }
    pin054 = pPin054.setScale(DECIMAL_PIN054, RoundingMode.HALF_UP);
  }
  
  public void setPin054(Double pPin054) {
    if (pPin054 == null) {
      return;
    }
    pin054 = BigDecimal.valueOf(pPin054).setScale(DECIMAL_PIN054, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin054() {
    return pin054.setScale(DECIMAL_PIN054, RoundingMode.HALF_UP);
  }
  
  public void setPil055(String pPil055) {
    if (pPil055 == null) {
      return;
    }
    pil055 = pPil055;
  }
  
  public String getPil055() {
    return pil055;
  }
  
  public void setPiq055(BigDecimal pPiq055) {
    if (pPiq055 == null) {
      return;
    }
    piq055 = pPiq055.setScale(DECIMAL_PIQ055, RoundingMode.HALF_UP);
  }
  
  public void setPiq055(Double pPiq055) {
    if (pPiq055 == null) {
      return;
    }
    piq055 = BigDecimal.valueOf(pPiq055).setScale(DECIMAL_PIQ055, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq055() {
    return piq055.setScale(DECIMAL_PIQ055, RoundingMode.HALF_UP);
  }
  
  public void setPin055(BigDecimal pPin055) {
    if (pPin055 == null) {
      return;
    }
    pin055 = pPin055.setScale(DECIMAL_PIN055, RoundingMode.HALF_UP);
  }
  
  public void setPin055(Double pPin055) {
    if (pPin055 == null) {
      return;
    }
    pin055 = BigDecimal.valueOf(pPin055).setScale(DECIMAL_PIN055, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin055() {
    return pin055.setScale(DECIMAL_PIN055, RoundingMode.HALF_UP);
  }
  
  public void setPil056(String pPil056) {
    if (pPil056 == null) {
      return;
    }
    pil056 = pPil056;
  }
  
  public String getPil056() {
    return pil056;
  }
  
  public void setPiq056(BigDecimal pPiq056) {
    if (pPiq056 == null) {
      return;
    }
    piq056 = pPiq056.setScale(DECIMAL_PIQ056, RoundingMode.HALF_UP);
  }
  
  public void setPiq056(Double pPiq056) {
    if (pPiq056 == null) {
      return;
    }
    piq056 = BigDecimal.valueOf(pPiq056).setScale(DECIMAL_PIQ056, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq056() {
    return piq056.setScale(DECIMAL_PIQ056, RoundingMode.HALF_UP);
  }
  
  public void setPin056(BigDecimal pPin056) {
    if (pPin056 == null) {
      return;
    }
    pin056 = pPin056.setScale(DECIMAL_PIN056, RoundingMode.HALF_UP);
  }
  
  public void setPin056(Double pPin056) {
    if (pPin056 == null) {
      return;
    }
    pin056 = BigDecimal.valueOf(pPin056).setScale(DECIMAL_PIN056, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin056() {
    return pin056.setScale(DECIMAL_PIN056, RoundingMode.HALF_UP);
  }
  
  public void setPil057(String pPil057) {
    if (pPil057 == null) {
      return;
    }
    pil057 = pPil057;
  }
  
  public String getPil057() {
    return pil057;
  }
  
  public void setPiq057(BigDecimal pPiq057) {
    if (pPiq057 == null) {
      return;
    }
    piq057 = pPiq057.setScale(DECIMAL_PIQ057, RoundingMode.HALF_UP);
  }
  
  public void setPiq057(Double pPiq057) {
    if (pPiq057 == null) {
      return;
    }
    piq057 = BigDecimal.valueOf(pPiq057).setScale(DECIMAL_PIQ057, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq057() {
    return piq057.setScale(DECIMAL_PIQ057, RoundingMode.HALF_UP);
  }
  
  public void setPin057(BigDecimal pPin057) {
    if (pPin057 == null) {
      return;
    }
    pin057 = pPin057.setScale(DECIMAL_PIN057, RoundingMode.HALF_UP);
  }
  
  public void setPin057(Double pPin057) {
    if (pPin057 == null) {
      return;
    }
    pin057 = BigDecimal.valueOf(pPin057).setScale(DECIMAL_PIN057, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin057() {
    return pin057.setScale(DECIMAL_PIN057, RoundingMode.HALF_UP);
  }
  
  public void setPil058(String pPil058) {
    if (pPil058 == null) {
      return;
    }
    pil058 = pPil058;
  }
  
  public String getPil058() {
    return pil058;
  }
  
  public void setPiq058(BigDecimal pPiq058) {
    if (pPiq058 == null) {
      return;
    }
    piq058 = pPiq058.setScale(DECIMAL_PIQ058, RoundingMode.HALF_UP);
  }
  
  public void setPiq058(Double pPiq058) {
    if (pPiq058 == null) {
      return;
    }
    piq058 = BigDecimal.valueOf(pPiq058).setScale(DECIMAL_PIQ058, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq058() {
    return piq058.setScale(DECIMAL_PIQ058, RoundingMode.HALF_UP);
  }
  
  public void setPin058(BigDecimal pPin058) {
    if (pPin058 == null) {
      return;
    }
    pin058 = pPin058.setScale(DECIMAL_PIN058, RoundingMode.HALF_UP);
  }
  
  public void setPin058(Double pPin058) {
    if (pPin058 == null) {
      return;
    }
    pin058 = BigDecimal.valueOf(pPin058).setScale(DECIMAL_PIN058, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin058() {
    return pin058.setScale(DECIMAL_PIN058, RoundingMode.HALF_UP);
  }
  
  public void setPil059(String pPil059) {
    if (pPil059 == null) {
      return;
    }
    pil059 = pPil059;
  }
  
  public String getPil059() {
    return pil059;
  }
  
  public void setPiq059(BigDecimal pPiq059) {
    if (pPiq059 == null) {
      return;
    }
    piq059 = pPiq059.setScale(DECIMAL_PIQ059, RoundingMode.HALF_UP);
  }
  
  public void setPiq059(Double pPiq059) {
    if (pPiq059 == null) {
      return;
    }
    piq059 = BigDecimal.valueOf(pPiq059).setScale(DECIMAL_PIQ059, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq059() {
    return piq059.setScale(DECIMAL_PIQ059, RoundingMode.HALF_UP);
  }
  
  public void setPin059(BigDecimal pPin059) {
    if (pPin059 == null) {
      return;
    }
    pin059 = pPin059.setScale(DECIMAL_PIN059, RoundingMode.HALF_UP);
  }
  
  public void setPin059(Double pPin059) {
    if (pPin059 == null) {
      return;
    }
    pin059 = BigDecimal.valueOf(pPin059).setScale(DECIMAL_PIN059, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin059() {
    return pin059.setScale(DECIMAL_PIN059, RoundingMode.HALF_UP);
  }
  
  public void setPil060(String pPil060) {
    if (pPil060 == null) {
      return;
    }
    pil060 = pPil060;
  }
  
  public String getPil060() {
    return pil060;
  }
  
  public void setPiq060(BigDecimal pPiq060) {
    if (pPiq060 == null) {
      return;
    }
    piq060 = pPiq060.setScale(DECIMAL_PIQ060, RoundingMode.HALF_UP);
  }
  
  public void setPiq060(Double pPiq060) {
    if (pPiq060 == null) {
      return;
    }
    piq060 = BigDecimal.valueOf(pPiq060).setScale(DECIMAL_PIQ060, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq060() {
    return piq060.setScale(DECIMAL_PIQ060, RoundingMode.HALF_UP);
  }
  
  public void setPin060(BigDecimal pPin060) {
    if (pPin060 == null) {
      return;
    }
    pin060 = pPin060.setScale(DECIMAL_PIN060, RoundingMode.HALF_UP);
  }
  
  public void setPin060(Double pPin060) {
    if (pPin060 == null) {
      return;
    }
    pin060 = BigDecimal.valueOf(pPin060).setScale(DECIMAL_PIN060, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin060() {
    return pin060.setScale(DECIMAL_PIN060, RoundingMode.HALF_UP);
  }
  
  public void setPil061(String pPil061) {
    if (pPil061 == null) {
      return;
    }
    pil061 = pPil061;
  }
  
  public String getPil061() {
    return pil061;
  }
  
  public void setPiq061(BigDecimal pPiq061) {
    if (pPiq061 == null) {
      return;
    }
    piq061 = pPiq061.setScale(DECIMAL_PIQ061, RoundingMode.HALF_UP);
  }
  
  public void setPiq061(Double pPiq061) {
    if (pPiq061 == null) {
      return;
    }
    piq061 = BigDecimal.valueOf(pPiq061).setScale(DECIMAL_PIQ061, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq061() {
    return piq061.setScale(DECIMAL_PIQ061, RoundingMode.HALF_UP);
  }
  
  public void setPin061(BigDecimal pPin061) {
    if (pPin061 == null) {
      return;
    }
    pin061 = pPin061.setScale(DECIMAL_PIN061, RoundingMode.HALF_UP);
  }
  
  public void setPin061(Double pPin061) {
    if (pPin061 == null) {
      return;
    }
    pin061 = BigDecimal.valueOf(pPin061).setScale(DECIMAL_PIN061, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin061() {
    return pin061.setScale(DECIMAL_PIN061, RoundingMode.HALF_UP);
  }
  
  public void setPil062(String pPil062) {
    if (pPil062 == null) {
      return;
    }
    pil062 = pPil062;
  }
  
  public String getPil062() {
    return pil062;
  }
  
  public void setPiq062(BigDecimal pPiq062) {
    if (pPiq062 == null) {
      return;
    }
    piq062 = pPiq062.setScale(DECIMAL_PIQ062, RoundingMode.HALF_UP);
  }
  
  public void setPiq062(Double pPiq062) {
    if (pPiq062 == null) {
      return;
    }
    piq062 = BigDecimal.valueOf(pPiq062).setScale(DECIMAL_PIQ062, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq062() {
    return piq062.setScale(DECIMAL_PIQ062, RoundingMode.HALF_UP);
  }
  
  public void setPin062(BigDecimal pPin062) {
    if (pPin062 == null) {
      return;
    }
    pin062 = pPin062.setScale(DECIMAL_PIN062, RoundingMode.HALF_UP);
  }
  
  public void setPin062(Double pPin062) {
    if (pPin062 == null) {
      return;
    }
    pin062 = BigDecimal.valueOf(pPin062).setScale(DECIMAL_PIN062, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin062() {
    return pin062.setScale(DECIMAL_PIN062, RoundingMode.HALF_UP);
  }
  
  public void setPil063(String pPil063) {
    if (pPil063 == null) {
      return;
    }
    pil063 = pPil063;
  }
  
  public String getPil063() {
    return pil063;
  }
  
  public void setPiq063(BigDecimal pPiq063) {
    if (pPiq063 == null) {
      return;
    }
    piq063 = pPiq063.setScale(DECIMAL_PIQ063, RoundingMode.HALF_UP);
  }
  
  public void setPiq063(Double pPiq063) {
    if (pPiq063 == null) {
      return;
    }
    piq063 = BigDecimal.valueOf(pPiq063).setScale(DECIMAL_PIQ063, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq063() {
    return piq063.setScale(DECIMAL_PIQ063, RoundingMode.HALF_UP);
  }
  
  public void setPin063(BigDecimal pPin063) {
    if (pPin063 == null) {
      return;
    }
    pin063 = pPin063.setScale(DECIMAL_PIN063, RoundingMode.HALF_UP);
  }
  
  public void setPin063(Double pPin063) {
    if (pPin063 == null) {
      return;
    }
    pin063 = BigDecimal.valueOf(pPin063).setScale(DECIMAL_PIN063, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin063() {
    return pin063.setScale(DECIMAL_PIN063, RoundingMode.HALF_UP);
  }
  
  public void setPil064(String pPil064) {
    if (pPil064 == null) {
      return;
    }
    pil064 = pPil064;
  }
  
  public String getPil064() {
    return pil064;
  }
  
  public void setPiq064(BigDecimal pPiq064) {
    if (pPiq064 == null) {
      return;
    }
    piq064 = pPiq064.setScale(DECIMAL_PIQ064, RoundingMode.HALF_UP);
  }
  
  public void setPiq064(Double pPiq064) {
    if (pPiq064 == null) {
      return;
    }
    piq064 = BigDecimal.valueOf(pPiq064).setScale(DECIMAL_PIQ064, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq064() {
    return piq064.setScale(DECIMAL_PIQ064, RoundingMode.HALF_UP);
  }
  
  public void setPin064(BigDecimal pPin064) {
    if (pPin064 == null) {
      return;
    }
    pin064 = pPin064.setScale(DECIMAL_PIN064, RoundingMode.HALF_UP);
  }
  
  public void setPin064(Double pPin064) {
    if (pPin064 == null) {
      return;
    }
    pin064 = BigDecimal.valueOf(pPin064).setScale(DECIMAL_PIN064, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin064() {
    return pin064.setScale(DECIMAL_PIN064, RoundingMode.HALF_UP);
  }
  
  public void setPil065(String pPil065) {
    if (pPil065 == null) {
      return;
    }
    pil065 = pPil065;
  }
  
  public String getPil065() {
    return pil065;
  }
  
  public void setPiq065(BigDecimal pPiq065) {
    if (pPiq065 == null) {
      return;
    }
    piq065 = pPiq065.setScale(DECIMAL_PIQ065, RoundingMode.HALF_UP);
  }
  
  public void setPiq065(Double pPiq065) {
    if (pPiq065 == null) {
      return;
    }
    piq065 = BigDecimal.valueOf(pPiq065).setScale(DECIMAL_PIQ065, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq065() {
    return piq065.setScale(DECIMAL_PIQ065, RoundingMode.HALF_UP);
  }
  
  public void setPin065(BigDecimal pPin065) {
    if (pPin065 == null) {
      return;
    }
    pin065 = pPin065.setScale(DECIMAL_PIN065, RoundingMode.HALF_UP);
  }
  
  public void setPin065(Double pPin065) {
    if (pPin065 == null) {
      return;
    }
    pin065 = BigDecimal.valueOf(pPin065).setScale(DECIMAL_PIN065, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin065() {
    return pin065.setScale(DECIMAL_PIN065, RoundingMode.HALF_UP);
  }
  
  public void setPil066(String pPil066) {
    if (pPil066 == null) {
      return;
    }
    pil066 = pPil066;
  }
  
  public String getPil066() {
    return pil066;
  }
  
  public void setPiq066(BigDecimal pPiq066) {
    if (pPiq066 == null) {
      return;
    }
    piq066 = pPiq066.setScale(DECIMAL_PIQ066, RoundingMode.HALF_UP);
  }
  
  public void setPiq066(Double pPiq066) {
    if (pPiq066 == null) {
      return;
    }
    piq066 = BigDecimal.valueOf(pPiq066).setScale(DECIMAL_PIQ066, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq066() {
    return piq066.setScale(DECIMAL_PIQ066, RoundingMode.HALF_UP);
  }
  
  public void setPin066(BigDecimal pPin066) {
    if (pPin066 == null) {
      return;
    }
    pin066 = pPin066.setScale(DECIMAL_PIN066, RoundingMode.HALF_UP);
  }
  
  public void setPin066(Double pPin066) {
    if (pPin066 == null) {
      return;
    }
    pin066 = BigDecimal.valueOf(pPin066).setScale(DECIMAL_PIN066, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin066() {
    return pin066.setScale(DECIMAL_PIN066, RoundingMode.HALF_UP);
  }
  
  public void setPil067(String pPil067) {
    if (pPil067 == null) {
      return;
    }
    pil067 = pPil067;
  }
  
  public String getPil067() {
    return pil067;
  }
  
  public void setPiq067(BigDecimal pPiq067) {
    if (pPiq067 == null) {
      return;
    }
    piq067 = pPiq067.setScale(DECIMAL_PIQ067, RoundingMode.HALF_UP);
  }
  
  public void setPiq067(Double pPiq067) {
    if (pPiq067 == null) {
      return;
    }
    piq067 = BigDecimal.valueOf(pPiq067).setScale(DECIMAL_PIQ067, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq067() {
    return piq067.setScale(DECIMAL_PIQ067, RoundingMode.HALF_UP);
  }
  
  public void setPin067(BigDecimal pPin067) {
    if (pPin067 == null) {
      return;
    }
    pin067 = pPin067.setScale(DECIMAL_PIN067, RoundingMode.HALF_UP);
  }
  
  public void setPin067(Double pPin067) {
    if (pPin067 == null) {
      return;
    }
    pin067 = BigDecimal.valueOf(pPin067).setScale(DECIMAL_PIN067, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin067() {
    return pin067.setScale(DECIMAL_PIN067, RoundingMode.HALF_UP);
  }
  
  public void setPil068(String pPil068) {
    if (pPil068 == null) {
      return;
    }
    pil068 = pPil068;
  }
  
  public String getPil068() {
    return pil068;
  }
  
  public void setPiq068(BigDecimal pPiq068) {
    if (pPiq068 == null) {
      return;
    }
    piq068 = pPiq068.setScale(DECIMAL_PIQ068, RoundingMode.HALF_UP);
  }
  
  public void setPiq068(Double pPiq068) {
    if (pPiq068 == null) {
      return;
    }
    piq068 = BigDecimal.valueOf(pPiq068).setScale(DECIMAL_PIQ068, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq068() {
    return piq068.setScale(DECIMAL_PIQ068, RoundingMode.HALF_UP);
  }
  
  public void setPin068(BigDecimal pPin068) {
    if (pPin068 == null) {
      return;
    }
    pin068 = pPin068.setScale(DECIMAL_PIN068, RoundingMode.HALF_UP);
  }
  
  public void setPin068(Double pPin068) {
    if (pPin068 == null) {
      return;
    }
    pin068 = BigDecimal.valueOf(pPin068).setScale(DECIMAL_PIN068, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin068() {
    return pin068.setScale(DECIMAL_PIN068, RoundingMode.HALF_UP);
  }
  
  public void setPil069(String pPil069) {
    if (pPil069 == null) {
      return;
    }
    pil069 = pPil069;
  }
  
  public String getPil069() {
    return pil069;
  }
  
  public void setPiq069(BigDecimal pPiq069) {
    if (pPiq069 == null) {
      return;
    }
    piq069 = pPiq069.setScale(DECIMAL_PIQ069, RoundingMode.HALF_UP);
  }
  
  public void setPiq069(Double pPiq069) {
    if (pPiq069 == null) {
      return;
    }
    piq069 = BigDecimal.valueOf(pPiq069).setScale(DECIMAL_PIQ069, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq069() {
    return piq069.setScale(DECIMAL_PIQ069, RoundingMode.HALF_UP);
  }
  
  public void setPin069(BigDecimal pPin069) {
    if (pPin069 == null) {
      return;
    }
    pin069 = pPin069.setScale(DECIMAL_PIN069, RoundingMode.HALF_UP);
  }
  
  public void setPin069(Double pPin069) {
    if (pPin069 == null) {
      return;
    }
    pin069 = BigDecimal.valueOf(pPin069).setScale(DECIMAL_PIN069, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin069() {
    return pin069.setScale(DECIMAL_PIN069, RoundingMode.HALF_UP);
  }
  
  public void setPil070(String pPil070) {
    if (pPil070 == null) {
      return;
    }
    pil070 = pPil070;
  }
  
  public String getPil070() {
    return pil070;
  }
  
  public void setPiq070(BigDecimal pPiq070) {
    if (pPiq070 == null) {
      return;
    }
    piq070 = pPiq070.setScale(DECIMAL_PIQ070, RoundingMode.HALF_UP);
  }
  
  public void setPiq070(Double pPiq070) {
    if (pPiq070 == null) {
      return;
    }
    piq070 = BigDecimal.valueOf(pPiq070).setScale(DECIMAL_PIQ070, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq070() {
    return piq070.setScale(DECIMAL_PIQ070, RoundingMode.HALF_UP);
  }
  
  public void setPin070(BigDecimal pPin070) {
    if (pPin070 == null) {
      return;
    }
    pin070 = pPin070.setScale(DECIMAL_PIN070, RoundingMode.HALF_UP);
  }
  
  public void setPin070(Double pPin070) {
    if (pPin070 == null) {
      return;
    }
    pin070 = BigDecimal.valueOf(pPin070).setScale(DECIMAL_PIN070, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin070() {
    return pin070.setScale(DECIMAL_PIN070, RoundingMode.HALF_UP);
  }
  
  public void setPil071(String pPil071) {
    if (pPil071 == null) {
      return;
    }
    pil071 = pPil071;
  }
  
  public String getPil071() {
    return pil071;
  }
  
  public void setPiq071(BigDecimal pPiq071) {
    if (pPiq071 == null) {
      return;
    }
    piq071 = pPiq071.setScale(DECIMAL_PIQ071, RoundingMode.HALF_UP);
  }
  
  public void setPiq071(Double pPiq071) {
    if (pPiq071 == null) {
      return;
    }
    piq071 = BigDecimal.valueOf(pPiq071).setScale(DECIMAL_PIQ071, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq071() {
    return piq071.setScale(DECIMAL_PIQ071, RoundingMode.HALF_UP);
  }
  
  public void setPin071(BigDecimal pPin071) {
    if (pPin071 == null) {
      return;
    }
    pin071 = pPin071.setScale(DECIMAL_PIN071, RoundingMode.HALF_UP);
  }
  
  public void setPin071(Double pPin071) {
    if (pPin071 == null) {
      return;
    }
    pin071 = BigDecimal.valueOf(pPin071).setScale(DECIMAL_PIN071, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin071() {
    return pin071.setScale(DECIMAL_PIN071, RoundingMode.HALF_UP);
  }
  
  public void setPil072(String pPil072) {
    if (pPil072 == null) {
      return;
    }
    pil072 = pPil072;
  }
  
  public String getPil072() {
    return pil072;
  }
  
  public void setPiq072(BigDecimal pPiq072) {
    if (pPiq072 == null) {
      return;
    }
    piq072 = pPiq072.setScale(DECIMAL_PIQ072, RoundingMode.HALF_UP);
  }
  
  public void setPiq072(Double pPiq072) {
    if (pPiq072 == null) {
      return;
    }
    piq072 = BigDecimal.valueOf(pPiq072).setScale(DECIMAL_PIQ072, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq072() {
    return piq072.setScale(DECIMAL_PIQ072, RoundingMode.HALF_UP);
  }
  
  public void setPin072(BigDecimal pPin072) {
    if (pPin072 == null) {
      return;
    }
    pin072 = pPin072.setScale(DECIMAL_PIN072, RoundingMode.HALF_UP);
  }
  
  public void setPin072(Double pPin072) {
    if (pPin072 == null) {
      return;
    }
    pin072 = BigDecimal.valueOf(pPin072).setScale(DECIMAL_PIN072, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin072() {
    return pin072.setScale(DECIMAL_PIN072, RoundingMode.HALF_UP);
  }
  
  public void setPil073(String pPil073) {
    if (pPil073 == null) {
      return;
    }
    pil073 = pPil073;
  }
  
  public String getPil073() {
    return pil073;
  }
  
  public void setPiq073(BigDecimal pPiq073) {
    if (pPiq073 == null) {
      return;
    }
    piq073 = pPiq073.setScale(DECIMAL_PIQ073, RoundingMode.HALF_UP);
  }
  
  public void setPiq073(Double pPiq073) {
    if (pPiq073 == null) {
      return;
    }
    piq073 = BigDecimal.valueOf(pPiq073).setScale(DECIMAL_PIQ073, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq073() {
    return piq073.setScale(DECIMAL_PIQ073, RoundingMode.HALF_UP);
  }
  
  public void setPin073(BigDecimal pPin073) {
    if (pPin073 == null) {
      return;
    }
    pin073 = pPin073.setScale(DECIMAL_PIN073, RoundingMode.HALF_UP);
  }
  
  public void setPin073(Double pPin073) {
    if (pPin073 == null) {
      return;
    }
    pin073 = BigDecimal.valueOf(pPin073).setScale(DECIMAL_PIN073, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin073() {
    return pin073.setScale(DECIMAL_PIN073, RoundingMode.HALF_UP);
  }
  
  public void setPil074(String pPil074) {
    if (pPil074 == null) {
      return;
    }
    pil074 = pPil074;
  }
  
  public String getPil074() {
    return pil074;
  }
  
  public void setPiq074(BigDecimal pPiq074) {
    if (pPiq074 == null) {
      return;
    }
    piq074 = pPiq074.setScale(DECIMAL_PIQ074, RoundingMode.HALF_UP);
  }
  
  public void setPiq074(Double pPiq074) {
    if (pPiq074 == null) {
      return;
    }
    piq074 = BigDecimal.valueOf(pPiq074).setScale(DECIMAL_PIQ074, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq074() {
    return piq074.setScale(DECIMAL_PIQ074, RoundingMode.HALF_UP);
  }
  
  public void setPin074(BigDecimal pPin074) {
    if (pPin074 == null) {
      return;
    }
    pin074 = pPin074.setScale(DECIMAL_PIN074, RoundingMode.HALF_UP);
  }
  
  public void setPin074(Double pPin074) {
    if (pPin074 == null) {
      return;
    }
    pin074 = BigDecimal.valueOf(pPin074).setScale(DECIMAL_PIN074, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin074() {
    return pin074.setScale(DECIMAL_PIN074, RoundingMode.HALF_UP);
  }
  
  public void setPil075(String pPil075) {
    if (pPil075 == null) {
      return;
    }
    pil075 = pPil075;
  }
  
  public String getPil075() {
    return pil075;
  }
  
  public void setPiq075(BigDecimal pPiq075) {
    if (pPiq075 == null) {
      return;
    }
    piq075 = pPiq075.setScale(DECIMAL_PIQ075, RoundingMode.HALF_UP);
  }
  
  public void setPiq075(Double pPiq075) {
    if (pPiq075 == null) {
      return;
    }
    piq075 = BigDecimal.valueOf(pPiq075).setScale(DECIMAL_PIQ075, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq075() {
    return piq075.setScale(DECIMAL_PIQ075, RoundingMode.HALF_UP);
  }
  
  public void setPin075(BigDecimal pPin075) {
    if (pPin075 == null) {
      return;
    }
    pin075 = pPin075.setScale(DECIMAL_PIN075, RoundingMode.HALF_UP);
  }
  
  public void setPin075(Double pPin075) {
    if (pPin075 == null) {
      return;
    }
    pin075 = BigDecimal.valueOf(pPin075).setScale(DECIMAL_PIN075, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin075() {
    return pin075.setScale(DECIMAL_PIN075, RoundingMode.HALF_UP);
  }
  
  public void setPil076(String pPil076) {
    if (pPil076 == null) {
      return;
    }
    pil076 = pPil076;
  }
  
  public String getPil076() {
    return pil076;
  }
  
  public void setPiq076(BigDecimal pPiq076) {
    if (pPiq076 == null) {
      return;
    }
    piq076 = pPiq076.setScale(DECIMAL_PIQ076, RoundingMode.HALF_UP);
  }
  
  public void setPiq076(Double pPiq076) {
    if (pPiq076 == null) {
      return;
    }
    piq076 = BigDecimal.valueOf(pPiq076).setScale(DECIMAL_PIQ076, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq076() {
    return piq076.setScale(DECIMAL_PIQ076, RoundingMode.HALF_UP);
  }
  
  public void setPin076(BigDecimal pPin076) {
    if (pPin076 == null) {
      return;
    }
    pin076 = pPin076.setScale(DECIMAL_PIN076, RoundingMode.HALF_UP);
  }
  
  public void setPin076(Double pPin076) {
    if (pPin076 == null) {
      return;
    }
    pin076 = BigDecimal.valueOf(pPin076).setScale(DECIMAL_PIN076, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin076() {
    return pin076.setScale(DECIMAL_PIN076, RoundingMode.HALF_UP);
  }
  
  public void setPil077(String pPil077) {
    if (pPil077 == null) {
      return;
    }
    pil077 = pPil077;
  }
  
  public String getPil077() {
    return pil077;
  }
  
  public void setPiq077(BigDecimal pPiq077) {
    if (pPiq077 == null) {
      return;
    }
    piq077 = pPiq077.setScale(DECIMAL_PIQ077, RoundingMode.HALF_UP);
  }
  
  public void setPiq077(Double pPiq077) {
    if (pPiq077 == null) {
      return;
    }
    piq077 = BigDecimal.valueOf(pPiq077).setScale(DECIMAL_PIQ077, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq077() {
    return piq077.setScale(DECIMAL_PIQ077, RoundingMode.HALF_UP);
  }
  
  public void setPin077(BigDecimal pPin077) {
    if (pPin077 == null) {
      return;
    }
    pin077 = pPin077.setScale(DECIMAL_PIN077, RoundingMode.HALF_UP);
  }
  
  public void setPin077(Double pPin077) {
    if (pPin077 == null) {
      return;
    }
    pin077 = BigDecimal.valueOf(pPin077).setScale(DECIMAL_PIN077, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin077() {
    return pin077.setScale(DECIMAL_PIN077, RoundingMode.HALF_UP);
  }
  
  public void setPil078(String pPil078) {
    if (pPil078 == null) {
      return;
    }
    pil078 = pPil078;
  }
  
  public String getPil078() {
    return pil078;
  }
  
  public void setPiq078(BigDecimal pPiq078) {
    if (pPiq078 == null) {
      return;
    }
    piq078 = pPiq078.setScale(DECIMAL_PIQ078, RoundingMode.HALF_UP);
  }
  
  public void setPiq078(Double pPiq078) {
    if (pPiq078 == null) {
      return;
    }
    piq078 = BigDecimal.valueOf(pPiq078).setScale(DECIMAL_PIQ078, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq078() {
    return piq078.setScale(DECIMAL_PIQ078, RoundingMode.HALF_UP);
  }
  
  public void setPin078(BigDecimal pPin078) {
    if (pPin078 == null) {
      return;
    }
    pin078 = pPin078.setScale(DECIMAL_PIN078, RoundingMode.HALF_UP);
  }
  
  public void setPin078(Double pPin078) {
    if (pPin078 == null) {
      return;
    }
    pin078 = BigDecimal.valueOf(pPin078).setScale(DECIMAL_PIN078, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin078() {
    return pin078.setScale(DECIMAL_PIN078, RoundingMode.HALF_UP);
  }
  
  public void setPil079(String pPil079) {
    if (pPil079 == null) {
      return;
    }
    pil079 = pPil079;
  }
  
  public String getPil079() {
    return pil079;
  }
  
  public void setPiq079(BigDecimal pPiq079) {
    if (pPiq079 == null) {
      return;
    }
    piq079 = pPiq079.setScale(DECIMAL_PIQ079, RoundingMode.HALF_UP);
  }
  
  public void setPiq079(Double pPiq079) {
    if (pPiq079 == null) {
      return;
    }
    piq079 = BigDecimal.valueOf(pPiq079).setScale(DECIMAL_PIQ079, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq079() {
    return piq079.setScale(DECIMAL_PIQ079, RoundingMode.HALF_UP);
  }
  
  public void setPin079(BigDecimal pPin079) {
    if (pPin079 == null) {
      return;
    }
    pin079 = pPin079.setScale(DECIMAL_PIN079, RoundingMode.HALF_UP);
  }
  
  public void setPin079(Double pPin079) {
    if (pPin079 == null) {
      return;
    }
    pin079 = BigDecimal.valueOf(pPin079).setScale(DECIMAL_PIN079, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin079() {
    return pin079.setScale(DECIMAL_PIN079, RoundingMode.HALF_UP);
  }
  
  public void setPil080(String pPil080) {
    if (pPil080 == null) {
      return;
    }
    pil080 = pPil080;
  }
  
  public String getPil080() {
    return pil080;
  }
  
  public void setPiq080(BigDecimal pPiq080) {
    if (pPiq080 == null) {
      return;
    }
    piq080 = pPiq080.setScale(DECIMAL_PIQ080, RoundingMode.HALF_UP);
  }
  
  public void setPiq080(Double pPiq080) {
    if (pPiq080 == null) {
      return;
    }
    piq080 = BigDecimal.valueOf(pPiq080).setScale(DECIMAL_PIQ080, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq080() {
    return piq080.setScale(DECIMAL_PIQ080, RoundingMode.HALF_UP);
  }
  
  public void setPin080(BigDecimal pPin080) {
    if (pPin080 == null) {
      return;
    }
    pin080 = pPin080.setScale(DECIMAL_PIN080, RoundingMode.HALF_UP);
  }
  
  public void setPin080(Double pPin080) {
    if (pPin080 == null) {
      return;
    }
    pin080 = BigDecimal.valueOf(pPin080).setScale(DECIMAL_PIN080, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin080() {
    return pin080.setScale(DECIMAL_PIN080, RoundingMode.HALF_UP);
  }
  
  public void setPil081(String pPil081) {
    if (pPil081 == null) {
      return;
    }
    pil081 = pPil081;
  }
  
  public String getPil081() {
    return pil081;
  }
  
  public void setPiq081(BigDecimal pPiq081) {
    if (pPiq081 == null) {
      return;
    }
    piq081 = pPiq081.setScale(DECIMAL_PIQ081, RoundingMode.HALF_UP);
  }
  
  public void setPiq081(Double pPiq081) {
    if (pPiq081 == null) {
      return;
    }
    piq081 = BigDecimal.valueOf(pPiq081).setScale(DECIMAL_PIQ081, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq081() {
    return piq081.setScale(DECIMAL_PIQ081, RoundingMode.HALF_UP);
  }
  
  public void setPin081(BigDecimal pPin081) {
    if (pPin081 == null) {
      return;
    }
    pin081 = pPin081.setScale(DECIMAL_PIN081, RoundingMode.HALF_UP);
  }
  
  public void setPin081(Double pPin081) {
    if (pPin081 == null) {
      return;
    }
    pin081 = BigDecimal.valueOf(pPin081).setScale(DECIMAL_PIN081, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin081() {
    return pin081.setScale(DECIMAL_PIN081, RoundingMode.HALF_UP);
  }
  
  public void setPil082(String pPil082) {
    if (pPil082 == null) {
      return;
    }
    pil082 = pPil082;
  }
  
  public String getPil082() {
    return pil082;
  }
  
  public void setPiq082(BigDecimal pPiq082) {
    if (pPiq082 == null) {
      return;
    }
    piq082 = pPiq082.setScale(DECIMAL_PIQ082, RoundingMode.HALF_UP);
  }
  
  public void setPiq082(Double pPiq082) {
    if (pPiq082 == null) {
      return;
    }
    piq082 = BigDecimal.valueOf(pPiq082).setScale(DECIMAL_PIQ082, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq082() {
    return piq082.setScale(DECIMAL_PIQ082, RoundingMode.HALF_UP);
  }
  
  public void setPin082(BigDecimal pPin082) {
    if (pPin082 == null) {
      return;
    }
    pin082 = pPin082.setScale(DECIMAL_PIN082, RoundingMode.HALF_UP);
  }
  
  public void setPin082(Double pPin082) {
    if (pPin082 == null) {
      return;
    }
    pin082 = BigDecimal.valueOf(pPin082).setScale(DECIMAL_PIN082, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin082() {
    return pin082.setScale(DECIMAL_PIN082, RoundingMode.HALF_UP);
  }
  
  public void setPil083(String pPil083) {
    if (pPil083 == null) {
      return;
    }
    pil083 = pPil083;
  }
  
  public String getPil083() {
    return pil083;
  }
  
  public void setPiq083(BigDecimal pPiq083) {
    if (pPiq083 == null) {
      return;
    }
    piq083 = pPiq083.setScale(DECIMAL_PIQ083, RoundingMode.HALF_UP);
  }
  
  public void setPiq083(Double pPiq083) {
    if (pPiq083 == null) {
      return;
    }
    piq083 = BigDecimal.valueOf(pPiq083).setScale(DECIMAL_PIQ083, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq083() {
    return piq083.setScale(DECIMAL_PIQ083, RoundingMode.HALF_UP);
  }
  
  public void setPin083(BigDecimal pPin083) {
    if (pPin083 == null) {
      return;
    }
    pin083 = pPin083.setScale(DECIMAL_PIN083, RoundingMode.HALF_UP);
  }
  
  public void setPin083(Double pPin083) {
    if (pPin083 == null) {
      return;
    }
    pin083 = BigDecimal.valueOf(pPin083).setScale(DECIMAL_PIN083, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin083() {
    return pin083.setScale(DECIMAL_PIN083, RoundingMode.HALF_UP);
  }
  
  public void setPil084(String pPil084) {
    if (pPil084 == null) {
      return;
    }
    pil084 = pPil084;
  }
  
  public String getPil084() {
    return pil084;
  }
  
  public void setPiq084(BigDecimal pPiq084) {
    if (pPiq084 == null) {
      return;
    }
    piq084 = pPiq084.setScale(DECIMAL_PIQ084, RoundingMode.HALF_UP);
  }
  
  public void setPiq084(Double pPiq084) {
    if (pPiq084 == null) {
      return;
    }
    piq084 = BigDecimal.valueOf(pPiq084).setScale(DECIMAL_PIQ084, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq084() {
    return piq084.setScale(DECIMAL_PIQ084, RoundingMode.HALF_UP);
  }
  
  public void setPin084(BigDecimal pPin084) {
    if (pPin084 == null) {
      return;
    }
    pin084 = pPin084.setScale(DECIMAL_PIN084, RoundingMode.HALF_UP);
  }
  
  public void setPin084(Double pPin084) {
    if (pPin084 == null) {
      return;
    }
    pin084 = BigDecimal.valueOf(pPin084).setScale(DECIMAL_PIN084, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin084() {
    return pin084.setScale(DECIMAL_PIN084, RoundingMode.HALF_UP);
  }
  
  public void setPil085(String pPil085) {
    if (pPil085 == null) {
      return;
    }
    pil085 = pPil085;
  }
  
  public String getPil085() {
    return pil085;
  }
  
  public void setPiq085(BigDecimal pPiq085) {
    if (pPiq085 == null) {
      return;
    }
    piq085 = pPiq085.setScale(DECIMAL_PIQ085, RoundingMode.HALF_UP);
  }
  
  public void setPiq085(Double pPiq085) {
    if (pPiq085 == null) {
      return;
    }
    piq085 = BigDecimal.valueOf(pPiq085).setScale(DECIMAL_PIQ085, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq085() {
    return piq085.setScale(DECIMAL_PIQ085, RoundingMode.HALF_UP);
  }
  
  public void setPin085(BigDecimal pPin085) {
    if (pPin085 == null) {
      return;
    }
    pin085 = pPin085.setScale(DECIMAL_PIN085, RoundingMode.HALF_UP);
  }
  
  public void setPin085(Double pPin085) {
    if (pPin085 == null) {
      return;
    }
    pin085 = BigDecimal.valueOf(pPin085).setScale(DECIMAL_PIN085, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin085() {
    return pin085.setScale(DECIMAL_PIN085, RoundingMode.HALF_UP);
  }
  
  public void setPil086(String pPil086) {
    if (pPil086 == null) {
      return;
    }
    pil086 = pPil086;
  }
  
  public String getPil086() {
    return pil086;
  }
  
  public void setPiq086(BigDecimal pPiq086) {
    if (pPiq086 == null) {
      return;
    }
    piq086 = pPiq086.setScale(DECIMAL_PIQ086, RoundingMode.HALF_UP);
  }
  
  public void setPiq086(Double pPiq086) {
    if (pPiq086 == null) {
      return;
    }
    piq086 = BigDecimal.valueOf(pPiq086).setScale(DECIMAL_PIQ086, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq086() {
    return piq086.setScale(DECIMAL_PIQ086, RoundingMode.HALF_UP);
  }
  
  public void setPin086(BigDecimal pPin086) {
    if (pPin086 == null) {
      return;
    }
    pin086 = pPin086.setScale(DECIMAL_PIN086, RoundingMode.HALF_UP);
  }
  
  public void setPin086(Double pPin086) {
    if (pPin086 == null) {
      return;
    }
    pin086 = BigDecimal.valueOf(pPin086).setScale(DECIMAL_PIN086, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin086() {
    return pin086.setScale(DECIMAL_PIN086, RoundingMode.HALF_UP);
  }
  
  public void setPil087(String pPil087) {
    if (pPil087 == null) {
      return;
    }
    pil087 = pPil087;
  }
  
  public String getPil087() {
    return pil087;
  }
  
  public void setPiq087(BigDecimal pPiq087) {
    if (pPiq087 == null) {
      return;
    }
    piq087 = pPiq087.setScale(DECIMAL_PIQ087, RoundingMode.HALF_UP);
  }
  
  public void setPiq087(Double pPiq087) {
    if (pPiq087 == null) {
      return;
    }
    piq087 = BigDecimal.valueOf(pPiq087).setScale(DECIMAL_PIQ087, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq087() {
    return piq087.setScale(DECIMAL_PIQ087, RoundingMode.HALF_UP);
  }
  
  public void setPin087(BigDecimal pPin087) {
    if (pPin087 == null) {
      return;
    }
    pin087 = pPin087.setScale(DECIMAL_PIN087, RoundingMode.HALF_UP);
  }
  
  public void setPin087(Double pPin087) {
    if (pPin087 == null) {
      return;
    }
    pin087 = BigDecimal.valueOf(pPin087).setScale(DECIMAL_PIN087, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin087() {
    return pin087.setScale(DECIMAL_PIN087, RoundingMode.HALF_UP);
  }
  
  public void setPil088(String pPil088) {
    if (pPil088 == null) {
      return;
    }
    pil088 = pPil088;
  }
  
  public String getPil088() {
    return pil088;
  }
  
  public void setPiq088(BigDecimal pPiq088) {
    if (pPiq088 == null) {
      return;
    }
    piq088 = pPiq088.setScale(DECIMAL_PIQ088, RoundingMode.HALF_UP);
  }
  
  public void setPiq088(Double pPiq088) {
    if (pPiq088 == null) {
      return;
    }
    piq088 = BigDecimal.valueOf(pPiq088).setScale(DECIMAL_PIQ088, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq088() {
    return piq088.setScale(DECIMAL_PIQ088, RoundingMode.HALF_UP);
  }
  
  public void setPin088(BigDecimal pPin088) {
    if (pPin088 == null) {
      return;
    }
    pin088 = pPin088.setScale(DECIMAL_PIN088, RoundingMode.HALF_UP);
  }
  
  public void setPin088(Double pPin088) {
    if (pPin088 == null) {
      return;
    }
    pin088 = BigDecimal.valueOf(pPin088).setScale(DECIMAL_PIN088, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin088() {
    return pin088.setScale(DECIMAL_PIN088, RoundingMode.HALF_UP);
  }
  
  public void setPil089(String pPil089) {
    if (pPil089 == null) {
      return;
    }
    pil089 = pPil089;
  }
  
  public String getPil089() {
    return pil089;
  }
  
  public void setPiq089(BigDecimal pPiq089) {
    if (pPiq089 == null) {
      return;
    }
    piq089 = pPiq089.setScale(DECIMAL_PIQ089, RoundingMode.HALF_UP);
  }
  
  public void setPiq089(Double pPiq089) {
    if (pPiq089 == null) {
      return;
    }
    piq089 = BigDecimal.valueOf(pPiq089).setScale(DECIMAL_PIQ089, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq089() {
    return piq089.setScale(DECIMAL_PIQ089, RoundingMode.HALF_UP);
  }
  
  public void setPin089(BigDecimal pPin089) {
    if (pPin089 == null) {
      return;
    }
    pin089 = pPin089.setScale(DECIMAL_PIN089, RoundingMode.HALF_UP);
  }
  
  public void setPin089(Double pPin089) {
    if (pPin089 == null) {
      return;
    }
    pin089 = BigDecimal.valueOf(pPin089).setScale(DECIMAL_PIN089, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin089() {
    return pin089.setScale(DECIMAL_PIN089, RoundingMode.HALF_UP);
  }
  
  public void setPil090(String pPil090) {
    if (pPil090 == null) {
      return;
    }
    pil090 = pPil090;
  }
  
  public String getPil090() {
    return pil090;
  }
  
  public void setPiq090(BigDecimal pPiq090) {
    if (pPiq090 == null) {
      return;
    }
    piq090 = pPiq090.setScale(DECIMAL_PIQ090, RoundingMode.HALF_UP);
  }
  
  public void setPiq090(Double pPiq090) {
    if (pPiq090 == null) {
      return;
    }
    piq090 = BigDecimal.valueOf(pPiq090).setScale(DECIMAL_PIQ090, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq090() {
    return piq090.setScale(DECIMAL_PIQ090, RoundingMode.HALF_UP);
  }
  
  public void setPin090(BigDecimal pPin090) {
    if (pPin090 == null) {
      return;
    }
    pin090 = pPin090.setScale(DECIMAL_PIN090, RoundingMode.HALF_UP);
  }
  
  public void setPin090(Double pPin090) {
    if (pPin090 == null) {
      return;
    }
    pin090 = BigDecimal.valueOf(pPin090).setScale(DECIMAL_PIN090, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin090() {
    return pin090.setScale(DECIMAL_PIN090, RoundingMode.HALF_UP);
  }
  
  public void setPil091(String pPil091) {
    if (pPil091 == null) {
      return;
    }
    pil091 = pPil091;
  }
  
  public String getPil091() {
    return pil091;
  }
  
  public void setPiq091(BigDecimal pPiq091) {
    if (pPiq091 == null) {
      return;
    }
    piq091 = pPiq091.setScale(DECIMAL_PIQ091, RoundingMode.HALF_UP);
  }
  
  public void setPiq091(Double pPiq091) {
    if (pPiq091 == null) {
      return;
    }
    piq091 = BigDecimal.valueOf(pPiq091).setScale(DECIMAL_PIQ091, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq091() {
    return piq091.setScale(DECIMAL_PIQ091, RoundingMode.HALF_UP);
  }
  
  public void setPin091(BigDecimal pPin091) {
    if (pPin091 == null) {
      return;
    }
    pin091 = pPin091.setScale(DECIMAL_PIN091, RoundingMode.HALF_UP);
  }
  
  public void setPin091(Double pPin091) {
    if (pPin091 == null) {
      return;
    }
    pin091 = BigDecimal.valueOf(pPin091).setScale(DECIMAL_PIN091, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin091() {
    return pin091.setScale(DECIMAL_PIN091, RoundingMode.HALF_UP);
  }
  
  public void setPil092(String pPil092) {
    if (pPil092 == null) {
      return;
    }
    pil092 = pPil092;
  }
  
  public String getPil092() {
    return pil092;
  }
  
  public void setPiq092(BigDecimal pPiq092) {
    if (pPiq092 == null) {
      return;
    }
    piq092 = pPiq092.setScale(DECIMAL_PIQ092, RoundingMode.HALF_UP);
  }
  
  public void setPiq092(Double pPiq092) {
    if (pPiq092 == null) {
      return;
    }
    piq092 = BigDecimal.valueOf(pPiq092).setScale(DECIMAL_PIQ092, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq092() {
    return piq092.setScale(DECIMAL_PIQ092, RoundingMode.HALF_UP);
  }
  
  public void setPin092(BigDecimal pPin092) {
    if (pPin092 == null) {
      return;
    }
    pin092 = pPin092.setScale(DECIMAL_PIN092, RoundingMode.HALF_UP);
  }
  
  public void setPin092(Double pPin092) {
    if (pPin092 == null) {
      return;
    }
    pin092 = BigDecimal.valueOf(pPin092).setScale(DECIMAL_PIN092, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin092() {
    return pin092.setScale(DECIMAL_PIN092, RoundingMode.HALF_UP);
  }
  
  public void setPil093(String pPil093) {
    if (pPil093 == null) {
      return;
    }
    pil093 = pPil093;
  }
  
  public String getPil093() {
    return pil093;
  }
  
  public void setPiq093(BigDecimal pPiq093) {
    if (pPiq093 == null) {
      return;
    }
    piq093 = pPiq093.setScale(DECIMAL_PIQ093, RoundingMode.HALF_UP);
  }
  
  public void setPiq093(Double pPiq093) {
    if (pPiq093 == null) {
      return;
    }
    piq093 = BigDecimal.valueOf(pPiq093).setScale(DECIMAL_PIQ093, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq093() {
    return piq093.setScale(DECIMAL_PIQ093, RoundingMode.HALF_UP);
  }
  
  public void setPin093(BigDecimal pPin093) {
    if (pPin093 == null) {
      return;
    }
    pin093 = pPin093.setScale(DECIMAL_PIN093, RoundingMode.HALF_UP);
  }
  
  public void setPin093(Double pPin093) {
    if (pPin093 == null) {
      return;
    }
    pin093 = BigDecimal.valueOf(pPin093).setScale(DECIMAL_PIN093, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin093() {
    return pin093.setScale(DECIMAL_PIN093, RoundingMode.HALF_UP);
  }
  
  public void setPil094(String pPil094) {
    if (pPil094 == null) {
      return;
    }
    pil094 = pPil094;
  }
  
  public String getPil094() {
    return pil094;
  }
  
  public void setPiq094(BigDecimal pPiq094) {
    if (pPiq094 == null) {
      return;
    }
    piq094 = pPiq094.setScale(DECIMAL_PIQ094, RoundingMode.HALF_UP);
  }
  
  public void setPiq094(Double pPiq094) {
    if (pPiq094 == null) {
      return;
    }
    piq094 = BigDecimal.valueOf(pPiq094).setScale(DECIMAL_PIQ094, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq094() {
    return piq094.setScale(DECIMAL_PIQ094, RoundingMode.HALF_UP);
  }
  
  public void setPin094(BigDecimal pPin094) {
    if (pPin094 == null) {
      return;
    }
    pin094 = pPin094.setScale(DECIMAL_PIN094, RoundingMode.HALF_UP);
  }
  
  public void setPin094(Double pPin094) {
    if (pPin094 == null) {
      return;
    }
    pin094 = BigDecimal.valueOf(pPin094).setScale(DECIMAL_PIN094, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin094() {
    return pin094.setScale(DECIMAL_PIN094, RoundingMode.HALF_UP);
  }
  
  public void setPil095(String pPil095) {
    if (pPil095 == null) {
      return;
    }
    pil095 = pPil095;
  }
  
  public String getPil095() {
    return pil095;
  }
  
  public void setPiq095(BigDecimal pPiq095) {
    if (pPiq095 == null) {
      return;
    }
    piq095 = pPiq095.setScale(DECIMAL_PIQ095, RoundingMode.HALF_UP);
  }
  
  public void setPiq095(Double pPiq095) {
    if (pPiq095 == null) {
      return;
    }
    piq095 = BigDecimal.valueOf(pPiq095).setScale(DECIMAL_PIQ095, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq095() {
    return piq095.setScale(DECIMAL_PIQ095, RoundingMode.HALF_UP);
  }
  
  public void setPin095(BigDecimal pPin095) {
    if (pPin095 == null) {
      return;
    }
    pin095 = pPin095.setScale(DECIMAL_PIN095, RoundingMode.HALF_UP);
  }
  
  public void setPin095(Double pPin095) {
    if (pPin095 == null) {
      return;
    }
    pin095 = BigDecimal.valueOf(pPin095).setScale(DECIMAL_PIN095, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin095() {
    return pin095.setScale(DECIMAL_PIN095, RoundingMode.HALF_UP);
  }
  
  public void setPil096(String pPil096) {
    if (pPil096 == null) {
      return;
    }
    pil096 = pPil096;
  }
  
  public String getPil096() {
    return pil096;
  }
  
  public void setPiq096(BigDecimal pPiq096) {
    if (pPiq096 == null) {
      return;
    }
    piq096 = pPiq096.setScale(DECIMAL_PIQ096, RoundingMode.HALF_UP);
  }
  
  public void setPiq096(Double pPiq096) {
    if (pPiq096 == null) {
      return;
    }
    piq096 = BigDecimal.valueOf(pPiq096).setScale(DECIMAL_PIQ096, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq096() {
    return piq096.setScale(DECIMAL_PIQ096, RoundingMode.HALF_UP);
  }
  
  public void setPin096(BigDecimal pPin096) {
    if (pPin096 == null) {
      return;
    }
    pin096 = pPin096.setScale(DECIMAL_PIN096, RoundingMode.HALF_UP);
  }
  
  public void setPin096(Double pPin096) {
    if (pPin096 == null) {
      return;
    }
    pin096 = BigDecimal.valueOf(pPin096).setScale(DECIMAL_PIN096, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin096() {
    return pin096.setScale(DECIMAL_PIN096, RoundingMode.HALF_UP);
  }
  
  public void setPil097(String pPil097) {
    if (pPil097 == null) {
      return;
    }
    pil097 = pPil097;
  }
  
  public String getPil097() {
    return pil097;
  }
  
  public void setPiq097(BigDecimal pPiq097) {
    if (pPiq097 == null) {
      return;
    }
    piq097 = pPiq097.setScale(DECIMAL_PIQ097, RoundingMode.HALF_UP);
  }
  
  public void setPiq097(Double pPiq097) {
    if (pPiq097 == null) {
      return;
    }
    piq097 = BigDecimal.valueOf(pPiq097).setScale(DECIMAL_PIQ097, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq097() {
    return piq097.setScale(DECIMAL_PIQ097, RoundingMode.HALF_UP);
  }
  
  public void setPin097(BigDecimal pPin097) {
    if (pPin097 == null) {
      return;
    }
    pin097 = pPin097.setScale(DECIMAL_PIN097, RoundingMode.HALF_UP);
  }
  
  public void setPin097(Double pPin097) {
    if (pPin097 == null) {
      return;
    }
    pin097 = BigDecimal.valueOf(pPin097).setScale(DECIMAL_PIN097, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin097() {
    return pin097.setScale(DECIMAL_PIN097, RoundingMode.HALF_UP);
  }
  
  public void setPil098(String pPil098) {
    if (pPil098 == null) {
      return;
    }
    pil098 = pPil098;
  }
  
  public String getPil098() {
    return pil098;
  }
  
  public void setPiq098(BigDecimal pPiq098) {
    if (pPiq098 == null) {
      return;
    }
    piq098 = pPiq098.setScale(DECIMAL_PIQ098, RoundingMode.HALF_UP);
  }
  
  public void setPiq098(Double pPiq098) {
    if (pPiq098 == null) {
      return;
    }
    piq098 = BigDecimal.valueOf(pPiq098).setScale(DECIMAL_PIQ098, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq098() {
    return piq098.setScale(DECIMAL_PIQ098, RoundingMode.HALF_UP);
  }
  
  public void setPin098(BigDecimal pPin098) {
    if (pPin098 == null) {
      return;
    }
    pin098 = pPin098.setScale(DECIMAL_PIN098, RoundingMode.HALF_UP);
  }
  
  public void setPin098(Double pPin098) {
    if (pPin098 == null) {
      return;
    }
    pin098 = BigDecimal.valueOf(pPin098).setScale(DECIMAL_PIN098, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin098() {
    return pin098.setScale(DECIMAL_PIN098, RoundingMode.HALF_UP);
  }
  
  public void setPil099(String pPil099) {
    if (pPil099 == null) {
      return;
    }
    pil099 = pPil099;
  }
  
  public String getPil099() {
    return pil099;
  }
  
  public void setPiq099(BigDecimal pPiq099) {
    if (pPiq099 == null) {
      return;
    }
    piq099 = pPiq099.setScale(DECIMAL_PIQ099, RoundingMode.HALF_UP);
  }
  
  public void setPiq099(Double pPiq099) {
    if (pPiq099 == null) {
      return;
    }
    piq099 = BigDecimal.valueOf(pPiq099).setScale(DECIMAL_PIQ099, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq099() {
    return piq099.setScale(DECIMAL_PIQ099, RoundingMode.HALF_UP);
  }
  
  public void setPin099(BigDecimal pPin099) {
    if (pPin099 == null) {
      return;
    }
    pin099 = pPin099.setScale(DECIMAL_PIN099, RoundingMode.HALF_UP);
  }
  
  public void setPin099(Double pPin099) {
    if (pPin099 == null) {
      return;
    }
    pin099 = BigDecimal.valueOf(pPin099).setScale(DECIMAL_PIN099, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin099() {
    return pin099.setScale(DECIMAL_PIN099, RoundingMode.HALF_UP);
  }
  
  public void setPil100(String pPil100) {
    if (pPil100 == null) {
      return;
    }
    pil100 = pPil100;
  }
  
  public String getPil100() {
    return pil100;
  }
  
  public void setPiq100(BigDecimal pPiq100) {
    if (pPiq100 == null) {
      return;
    }
    piq100 = pPiq100.setScale(DECIMAL_PIQ100, RoundingMode.HALF_UP);
  }
  
  public void setPiq100(Double pPiq100) {
    if (pPiq100 == null) {
      return;
    }
    piq100 = BigDecimal.valueOf(pPiq100).setScale(DECIMAL_PIQ100, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiq100() {
    return piq100.setScale(DECIMAL_PIQ100, RoundingMode.HALF_UP);
  }
  
  public void setPin100(BigDecimal pPin100) {
    if (pPin100 == null) {
      return;
    }
    pin100 = pPin100.setScale(DECIMAL_PIN100, RoundingMode.HALF_UP);
  }
  
  public void setPin100(Double pPin100) {
    if (pPin100 == null) {
      return;
    }
    pin100 = BigDecimal.valueOf(pPin100).setScale(DECIMAL_PIN100, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPin100() {
    return pin100.setScale(DECIMAL_PIN100, RoundingMode.HALF_UP);
  }
}
