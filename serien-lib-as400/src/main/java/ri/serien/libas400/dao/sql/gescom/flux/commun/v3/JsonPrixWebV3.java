/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

import java.math.BigDecimal;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Prix article Série N au format SFCC.
 * Cette classe est une classe qui sert à l'intégration JAVA vers JSON
 * IL NE FAUT PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonPrixWebV3 {
  // Variables
  private String code = null;
  private String codeEAN = null;
  private String libelle1 = null;
  private String libelle2 = null;
  private String libelle3 = null;
  private String libelle4 = null;
  private String montantDEEE = null;
  private String tarif1 = null;
  private String tarif2 = null;
  private String tarif3 = null;
  private String tarif4 = null;
  private String tarif5 = null;
  private String tarif6 = null;
  private String tarif7 = null;
  private String tarif8 = null;
  private String tarif9 = null;
  private String tarif10 = null;
  
  private transient IdArticle idArticle = null;
  private transient String exclutWeb = null;
  private transient String A1UNV = null;
  private transient String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public JsonPrixWebV3() {
  }
  
  // -- Méthodes publiques
  
  /**
   * Permet de construire l'article sur le modèle SFCC dans l'optique d'un export typé.
   */
  public IdArticle mettreAJourArticle(GenericRecord pRecord) {
    if (pRecord == null) {
      majError("[JsonPrixWebV3] mettreAJourArticle() - Le GenericRecord est incorrect.");
      return null;
    }
    
    try {
      if (pRecord.isPresentField("A1ETB") && pRecord.isPresentField("A1ART")) {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("A1ETB"));
        code = pRecord.getStringValue("A1ART", null, true);
        idArticle = IdArticle.getInstance(idEtablissement, code);
      }
      if (idArticle == null) {
        majError("[JsonPrixWebV3] mettreAJourArticle() - L'identifiant article n'a pas pu être créé.");
        return null;
      }
      
      if (pRecord.isPresentField("A1UNV")) {
        A1UNV = pRecord.getField("A1UNV").toString().trim();
      }
      
      // Code EAN de l'article
      if (pRecord.isPresentField("GCD")) {
        codeEAN = pRecord.getField("GCD").toString().trim();
      }
      else {
        codeEAN = "";
      }
      
      // Libellés de l'article
      if (pRecord.isPresentField("A1LIB")) {
        libelle1 = pRecord.getStringValue("A1LIB");
      }
      if (pRecord.isPresentField("A1LB1")) {
        libelle2 = pRecord.getStringValue("A1LB1");
      }
      if (pRecord.isPresentField("A1LB2")) {
        libelle3 = pRecord.getStringValue("A1LB2");
      }
      if (pRecord.isPresentField("A1LB3")) {
        libelle4 = pRecord.getStringValue("A1LB3");
      }
      
      // Statuts SFCC
      if (!pRecord.isPresentField("A1NPU")) {
        pRecord.setField("A1NPU", "1");
      }
      else if (pRecord.isPresentField("A1NPU") && pRecord.getField("A1NPU").toString().trim().length() == 0) {
        pRecord.setField("A1NPU", "0");
      }
      
      if (pRecord.isPresentField("A1IN15")) {
        exclutWeb = pRecord.getField("A1IN15").toString();
      }
    }
    catch (Exception e) {
      majError("[JsonPrixWebV3] mettreAJourArticle() - Erreur : " + e + '.');
    }
    
    return idArticle;
  }
  
  /**
   * Permet de mettre à jour les tarifs de l'article pour interprétation dans SFCC.
   */
  public boolean mettreAJourTarif(GenericRecord pRecord) {
    if (pRecord == null) {
      majError("[JsonPrixWebV3] gererLesTarifs() - Cet article ne possède pas encore de tarif valide.");
      return false;
    }
    
    if (pRecord.isPresentField("ATP01") && !pRecord.getField("ATP01").toString().trim().equals("")) {
      tarif1 = pRecord.getField("ATP01").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif1 corrompu ");
    }
    
    if (pRecord.isPresentField("ATP02") && !pRecord.getField("ATP02").toString().trim().equals("")) {
      tarif2 = pRecord.getField("ATP02").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif2 corrompu ");
    }
    
    if (pRecord.isPresentField("ATP03") && !pRecord.getField("ATP03").toString().trim().equals("")) {
      tarif3 = pRecord.getField("ATP03").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif3 corrompu ");
    }
    
    if (pRecord.isPresentField("ATP04") && !pRecord.getField("ATP04").toString().trim().equals("")) {
      tarif4 = pRecord.getField("ATP04").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif4 corrompu ");
    }
    
    if (pRecord.isPresentField("ATP05") && !pRecord.getField("ATP05").toString().trim().equals("")) {
      tarif5 = pRecord.getField("ATP05").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif5 corrompu ");
    }
    
    if (pRecord.isPresentField("ATP06") && !pRecord.getField("ATP06").toString().trim().equals("")) {
      tarif6 = pRecord.getField("ATP06").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif6 corrompu ");
    }
    
    if (pRecord.isPresentField("ATP07") && !pRecord.getField("ATP07").toString().trim().equals("")) {
      tarif7 = pRecord.getField("ATP07").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif7 corrompu ");
    }
    
    if (pRecord.isPresentField("ATP08") && !pRecord.getField("ATP08").toString().trim().equals("")) {
      tarif8 = pRecord.getField("ATP08").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif8 corrompu ");
    }
    
    if (pRecord.isPresentField("ATP09") && !pRecord.getField("ATP09").toString().trim().equals("")) {
      tarif9 = pRecord.getField("ATP09").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif9 corrompu ");
    }
    
    if (pRecord.isPresentField("ATP10") && !pRecord.getField("ATP10").toString().trim().equals("")) {
      tarif10 = pRecord.getField("ATP10").toString().trim();
    }
    else {
      majError("[JsonPrixWebV3] gererLesTarifs() -> tarif10 corrompu ");
    }
    
    return msgErreur != null;
  }
  
  /**
   * Permet de remonter le montant DEEE de cet article pour la notion SFCC.
   */
  public void calculerMontantDEEE(BigDecimal pMontantDeee) {
    if (pMontantDeee != null) {
      montantDEEE = pMontantDeee.toPlainString();
    }
    else {
      montantDEEE = "0";
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Construit le message d'erreur proprement
   */
  private void majError(String pMessage) {
    if (msgErreur == null) {
      msgErreur = pMessage;
    }
    else {
      msgErreur += "\n" + pMessage;
    }
  }
  
  // -- Accesseurs
  
  public String getCode() {
    return code;
  }
  
  public void setCode(String code) {
    this.code = code;
  }
  
  public String getCodeEAN() {
    return codeEAN;
  }
  
  public void setCodeEAN(String codeEAN) {
    this.codeEAN = codeEAN;
  }
  
  public String getLibelle1() {
    return libelle1;
  }
  
  public void setLibelle1(String libelle) {
    this.libelle1 = libelle;
  }
  
  public String getLibelle2() {
    return libelle2;
  }
  
  public void setLibelle2(String libelle) {
    this.libelle2 = libelle;
  }
  
  public String getLibelle3() {
    return libelle3;
  }
  
  public void setLibelle3(String libelle) {
    this.libelle3 = libelle;
  }
  
  public String getLibelle4() {
    return libelle4;
  }
  
  public void setLibelle4(String libelle) {
    this.libelle4 = libelle;
  }
  
  public String getMontantDEEE() {
    return montantDEEE;
  }
  
  public void setMontantDEEE(String montantDEEE) {
    this.montantDEEE = montantDEEE;
  }
  
  public String getTarif1() {
    return tarif1;
  }
  
  public void setTarif1(String tarif1) {
    this.tarif1 = tarif1;
  }
  
  public String getTarif2() {
    return tarif2;
  }
  
  public void setTarif2(String tarif2) {
    this.tarif2 = tarif2;
  }
  
  public String getTarif3() {
    return tarif3;
  }
  
  public void setTarif3(String tarif3) {
    this.tarif3 = tarif3;
  }
  
  public String getTarif4() {
    return tarif4;
  }
  
  public void setTarif4(String tarif4) {
    this.tarif4 = tarif4;
  }
  
  public String getTarif5() {
    return tarif5;
  }
  
  public void setTarif5(String tarif5) {
    this.tarif5 = tarif5;
  }
  
  public String getTarif6() {
    return tarif6;
  }
  
  public void setTarif6(String tarif6) {
    this.tarif6 = tarif6;
  }
  
  public String getTarif7() {
    return tarif7;
  }
  
  public void setTarif7(String tarif7) {
    this.tarif7 = tarif7;
  }
  
  public String getTarif8() {
    return tarif8;
  }
  
  public void setTarif8(String tarif8) {
    this.tarif8 = tarif8;
  }
  
  public String getTarif9() {
    return tarif9;
  }
  
  public void setTarif9(String tarif9) {
    this.tarif9 = tarif9;
  }
  
  public String getTarif10() {
    return tarif10;
  }
  
  public void setTarif10(String tarif10) {
    this.tarif10 = tarif10;
  }
  
  public String getExclutWeb() {
    return exclutWeb;
  }
  
  public void setExclutWeb(String exclutWeb) {
    this.exclutWeb = exclutWeb;
  }
  
  public String getA1UNV() {
    return A1UNV;
  }
  
  public void setA1UNV(String a1unv) {
    A1UNV = a1unv;
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
}
