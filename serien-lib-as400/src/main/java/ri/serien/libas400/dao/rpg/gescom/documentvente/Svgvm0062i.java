/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0062i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PIFOR = 1;
  public static final int SIZE_PIDEB = 7;
  public static final int DECIMAL_PIDEB = 0;
  public static final int SIZE_PIFIN = 7;
  public static final int DECIMAL_PIFIN = 0;
  public static final int SIZE_PIREF = 30;
  public static final int SIZE_PIFIL = 3;
  public static final int SIZE_PICHA = 1;
  public static final int SIZE_TOTALE_DS = 74;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINLI = 5;
  public static final int VAR_PIFOR = 6;
  public static final int VAR_PIDEB = 7;
  public static final int VAR_PIFIN = 8;
  public static final int VAR_PIREF = 9;
  public static final int VAR_PIFIL = 10;
  public static final int VAR_PICHA = 11;
  
  // Variables AS400
  private String piind = ""; // Indicateurs à "0" ou
  private String picod = ""; // Code ERL "D","E",X","9" *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de ligne
  private String pifor = ""; // caractère "f" obligatoire *
  private BigDecimal pideb = BigDecimal.ZERO; // Date début validité ou 0
  private BigDecimal pifin = BigDecimal.ZERO; // Date fin validité ou 0
  private String piref = ""; // Référence
  private String pifil = ""; // filler à blanc
  private String picha = ""; // "X" fin ou "C" chantier *
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs à "0" ou
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9" *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de ligne
      new AS400Text(SIZE_PIFOR), // caractère "f" obligatoire *
      new AS400ZonedDecimal(SIZE_PIDEB, DECIMAL_PIDEB), // Date début validité ou 0
      new AS400ZonedDecimal(SIZE_PIFIN, DECIMAL_PIFIN), // Date fin validité ou 0
      new AS400Text(SIZE_PIREF), // Référence
      new AS400Text(SIZE_PIFIL), // filler à blanc
      new AS400Text(SIZE_PICHA), // "X" fin ou "C" chantier *
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, picod, pietb, pinum, pisuf, pinli, pifor, pideb, pifin, piref, pifil, picha, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinli = (BigDecimal) output[5];
    pifor = (String) output[6];
    pideb = (BigDecimal) output[7];
    pifin = (BigDecimal) output[8];
    piref = (String) output[9];
    pifil = (String) output[10];
    picha = (String) output[11];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPifor(Character pPifor) {
    if (pPifor == null) {
      return;
    }
    pifor = String.valueOf(pPifor);
  }
  
  public Character getPifor() {
    return pifor.charAt(0);
  }
  
  public void setPideb(BigDecimal pPideb) {
    if (pPideb == null) {
      return;
    }
    pideb = pPideb.setScale(DECIMAL_PIDEB, RoundingMode.HALF_UP);
  }
  
  public void setPideb(Integer pPideb) {
    if (pPideb == null) {
      return;
    }
    pideb = BigDecimal.valueOf(pPideb);
  }
  
  public void setPideb(Date pPideb) {
    if (pPideb == null) {
      return;
    }
    pideb = BigDecimal.valueOf(ConvertDate.dateToDb2(pPideb));
  }
  
  public Integer getPideb() {
    return pideb.intValue();
  }
  
  public Date getPidebConvertiEnDate() {
    return ConvertDate.db2ToDate(pideb.intValue(), null);
  }
  
  public void setPifin(BigDecimal pPifin) {
    if (pPifin == null) {
      return;
    }
    pifin = pPifin.setScale(DECIMAL_PIFIN, RoundingMode.HALF_UP);
  }
  
  public void setPifin(Integer pPifin) {
    if (pPifin == null) {
      return;
    }
    pifin = BigDecimal.valueOf(pPifin);
  }
  
  public void setPifin(Date pPifin) {
    if (pPifin == null) {
      return;
    }
    pifin = BigDecimal.valueOf(ConvertDate.dateToDb2(pPifin));
  }
  
  public Integer getPifin() {
    return pifin.intValue();
  }
  
  public Date getPifinConvertiEnDate() {
    return ConvertDate.db2ToDate(pifin.intValue(), null);
  }
  
  public void setPiref(String pPiref) {
    if (pPiref == null) {
      return;
    }
    piref = pPiref;
  }
  
  public String getPiref() {
    return piref;
  }
  
  public void setPifil(String pPifil) {
    if (pPifil == null) {
      return;
    }
    pifil = pPifil;
  }
  
  public String getPifil() {
    return pifil;
  }
  
  public void setPicha(Character pPicha) {
    if (pPicha == null) {
      return;
    }
    picha = String.valueOf(pPicha);
  }
  
  public Character getPicha() {
    return picha.charAt(0);
  }
}
