/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.lien;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlLienDocument {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlLienDocument(QueryManager pQuerymg) {
    querymg = pQuerymg;
  }
  
  /**
   * Retourne la liste des documents d'achat d'origine à partir d'une réception.
   */
  public List<IdDocumentAchat> chargerListeIdCommandeOrigine(IdDocumentAchat pIdDocumentAchatReception) {
    if (pIdDocumentAchatReception == null) {
      return null;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    // Construction de la requête
    requeteSql.ajouter("select distinct LDCOD1, LDETB1, LDNUM1, LDSUF1 from " + querymg.getLibrary() + '.'
        + EnumTableBDD.LIEN_LIGNE_ACHAT + " where ");
    requeteSql.ajouterConditionAnd("LDSUF1", "=", 0);
    requeteSql.ajouterConditionAnd("LDETB2", "=", pIdDocumentAchatReception.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("LDCOD2", "=", pIdDocumentAchatReception.getCodeEntete().getCode());
    requeteSql.ajouterConditionAnd("LDNUM2", "=", pIdDocumentAchatReception.getNumero());
    requeteSql.ajouterConditionAnd("LDSUF2", "=", pIdDocumentAchatReception.getSuffixe());
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.size() == 0) {
      return null;
    }
    
    // Alimentation de la liste avec les enregistrements trouvés
    List<IdDocumentAchat> listeId = new ArrayList<IdDocumentAchat>();
    for (GenericRecord record : listeRecord) {
      if (record == null) {
        continue;
      }
      listeId.add(completerIdDocumentAchat(record));
    }
    return listeId;
  }
  
  /**
   * Initialise un identifiant de document d'achat.
   */
  private IdDocumentAchat completerIdDocumentAchat(GenericRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("LDETB1"));
    EnumCodeEnteteDocumentAchat codeEntete = EnumCodeEnteteDocumentAchat.valueOfByCode(pRecord.getCharacterValue("LDCOD1"));
    IdDocumentAchat idDocumentAchat =
        IdDocumentAchat.getInstance(idEtablissement, codeEntete, pRecord.getIntegerValue("LDNUM1"), pRecord.getIntegerValue("LDSUF1"));
    
    return idDocumentAchat;
  }
}
