/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_TR pour les TR
 */
public class Pgvmparm_ds_TR extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "TRETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "TRTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "TRCOD"));
    // rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "TRLIB"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TRPRO"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TRNBC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "TRTRF"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TRCLI"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 3), "TRPDS"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "TRTRP"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 2), "TRPDSD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 2), "TRPDSF"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 2), "TRVOLD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 2), "TRVOLF"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 2), "TRLNGD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 2), "TRLNGF"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 2), "TRSURD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 2), "TRSURF"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "TRZGEO"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "TRCOLD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "TRCOLF"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "TRMEX"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "TRPFC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TRIDE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(7), "TRFRS"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 4), "TRCCON"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TRMCP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TRCDE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TRNB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "TRFO"));
    
    length = 300;
  }
}
