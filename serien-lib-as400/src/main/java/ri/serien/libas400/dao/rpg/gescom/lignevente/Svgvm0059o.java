/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0059o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POLI01 = 400;
  public static final int SIZE_POLI02 = 400;
  public static final int SIZE_POLI03 = 400;
  public static final int SIZE_POLI04 = 400;
  public static final int SIZE_POLI05 = 400;
  public static final int SIZE_POLI06 = 400;
  public static final int SIZE_POLI07 = 400;
  public static final int SIZE_POLI08 = 400;
  public static final int SIZE_POLI09 = 400;
  public static final int SIZE_POLI10 = 400;
  public static final int SIZE_POLI11 = 400;
  public static final int SIZE_POLI12 = 400;
  public static final int SIZE_POLI13 = 400;
  public static final int SIZE_POLI14 = 400;
  public static final int SIZE_POLI15 = 400;
  public static final int SIZE_POLI16 = 400;
  public static final int SIZE_POLI17 = 400;
  public static final int SIZE_POLI18 = 400;
  public static final int SIZE_POLI19 = 400;
  public static final int SIZE_POLI20 = 400;
  public static final int SIZE_POLI21 = 400;
  public static final int SIZE_POLI22 = 400;
  public static final int SIZE_POLI23 = 400;
  public static final int SIZE_POLI24 = 400;
  public static final int SIZE_POLI25 = 400;
  public static final int SIZE_POLI26 = 400;
  public static final int SIZE_POLI27 = 400;
  public static final int SIZE_POLI28 = 400;
  public static final int SIZE_POLI29 = 400;
  public static final int SIZE_POLI30 = 400;
  public static final int SIZE_POLI31 = 400;
  public static final int SIZE_POLI32 = 400;
  public static final int SIZE_POLI33 = 400;
  public static final int SIZE_POLI34 = 400;
  public static final int SIZE_POLI35 = 400;
  public static final int SIZE_POLI36 = 400;
  public static final int SIZE_POLI37 = 400;
  public static final int SIZE_POLI38 = 400;
  public static final int SIZE_POLI39 = 400;
  public static final int SIZE_POLI40 = 400;
  public static final int SIZE_POLI41 = 400;
  public static final int SIZE_POLI42 = 400;
  public static final int SIZE_POLI43 = 400;
  public static final int SIZE_POLI44 = 400;
  public static final int SIZE_POLI45 = 400;
  public static final int SIZE_POLI46 = 400;
  public static final int SIZE_POLI47 = 400;
  public static final int SIZE_POLI48 = 400;
  public static final int SIZE_POLI49 = 400;
  public static final int SIZE_POLI50 = 400;
  public static final int SIZE_POLI51 = 400;
  public static final int SIZE_POLI52 = 400;
  public static final int SIZE_POLI53 = 400;
  public static final int SIZE_POLI54 = 400;
  public static final int SIZE_POLI55 = 400;
  public static final int SIZE_POLI56 = 400;
  public static final int SIZE_POLI57 = 400;
  public static final int SIZE_POLI58 = 400;
  public static final int SIZE_POLI59 = 400;
  public static final int SIZE_POLI60 = 400;
  public static final int SIZE_POLI61 = 400;
  public static final int SIZE_POLI62 = 400;
  public static final int SIZE_POLI63 = 400;
  public static final int SIZE_POLI64 = 400;
  public static final int SIZE_POLI65 = 400;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 26011;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POLI01 = 1;
  public static final int VAR_POLI02 = 2;
  public static final int VAR_POLI03 = 3;
  public static final int VAR_POLI04 = 4;
  public static final int VAR_POLI05 = 5;
  public static final int VAR_POLI06 = 6;
  public static final int VAR_POLI07 = 7;
  public static final int VAR_POLI08 = 8;
  public static final int VAR_POLI09 = 9;
  public static final int VAR_POLI10 = 10;
  public static final int VAR_POLI11 = 11;
  public static final int VAR_POLI12 = 12;
  public static final int VAR_POLI13 = 13;
  public static final int VAR_POLI14 = 14;
  public static final int VAR_POLI15 = 15;
  public static final int VAR_POLI16 = 16;
  public static final int VAR_POLI17 = 17;
  public static final int VAR_POLI18 = 18;
  public static final int VAR_POLI19 = 19;
  public static final int VAR_POLI20 = 20;
  public static final int VAR_POLI21 = 21;
  public static final int VAR_POLI22 = 22;
  public static final int VAR_POLI23 = 23;
  public static final int VAR_POLI24 = 24;
  public static final int VAR_POLI25 = 25;
  public static final int VAR_POLI26 = 26;
  public static final int VAR_POLI27 = 27;
  public static final int VAR_POLI28 = 28;
  public static final int VAR_POLI29 = 29;
  public static final int VAR_POLI30 = 30;
  public static final int VAR_POLI31 = 31;
  public static final int VAR_POLI32 = 32;
  public static final int VAR_POLI33 = 33;
  public static final int VAR_POLI34 = 34;
  public static final int VAR_POLI35 = 35;
  public static final int VAR_POLI36 = 36;
  public static final int VAR_POLI37 = 37;
  public static final int VAR_POLI38 = 38;
  public static final int VAR_POLI39 = 39;
  public static final int VAR_POLI40 = 40;
  public static final int VAR_POLI41 = 41;
  public static final int VAR_POLI42 = 42;
  public static final int VAR_POLI43 = 43;
  public static final int VAR_POLI44 = 44;
  public static final int VAR_POLI45 = 45;
  public static final int VAR_POLI46 = 46;
  public static final int VAR_POLI47 = 47;
  public static final int VAR_POLI48 = 48;
  public static final int VAR_POLI49 = 49;
  public static final int VAR_POLI50 = 50;
  public static final int VAR_POLI51 = 51;
  public static final int VAR_POLI52 = 52;
  public static final int VAR_POLI53 = 53;
  public static final int VAR_POLI54 = 54;
  public static final int VAR_POLI55 = 55;
  public static final int VAR_POLI56 = 56;
  public static final int VAR_POLI57 = 57;
  public static final int VAR_POLI58 = 58;
  public static final int VAR_POLI59 = 59;
  public static final int VAR_POLI60 = 60;
  public static final int VAR_POLI61 = 61;
  public static final int VAR_POLI62 = 62;
  public static final int VAR_POLI63 = 63;
  public static final int VAR_POLI64 = 64;
  public static final int VAR_POLI65 = 65;
  public static final int VAR_POARR = 66;
  
  private Svgvm0059d[] dsd = { new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(),
      new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(),
      new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(),
      new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(),
      new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(),
      new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(),
      new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(),
      new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(),
      new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(),
      new Svgvm0059d(), new Svgvm0059d(), new Svgvm0059d(), };
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private Object[] poli01 = dsd[0].o; // Zones Ligne 1
  private Object[] poli02 = dsd[1].o; // Zones Ligne 2
  private Object[] poli03 = dsd[2].o; // Zones Ligne 3
  private Object[] poli04 = dsd[3].o; // Zones Ligne 4
  private Object[] poli05 = dsd[4].o; // Zones Ligne 5
  private Object[] poli06 = dsd[5].o; // Zones Ligne 6
  private Object[] poli07 = dsd[6].o; // Zones Ligne 7
  private Object[] poli08 = dsd[7].o; // Zones Ligne 8
  private Object[] poli09 = dsd[8].o; // Zones Ligne 9
  private Object[] poli10 = dsd[9].o; // Zones Ligne 10
  private Object[] poli11 = dsd[10].o; // Zones Ligne 11
  private Object[] poli12 = dsd[11].o; // Zones Ligne 12
  private Object[] poli13 = dsd[12].o; // Zones Ligne 13
  private Object[] poli14 = dsd[13].o; // Zones Ligne 14
  private Object[] poli15 = dsd[14].o; // Zones Ligne 15
  private Object[] poli16 = dsd[15].o; // Zones Ligne 16
  private Object[] poli17 = dsd[16].o; // Zones Ligne 17
  private Object[] poli18 = dsd[17].o; // Zones Ligne 18
  private Object[] poli19 = dsd[18].o; // Zones Ligne 19
  private Object[] poli20 = dsd[19].o; // Zones Ligne 20
  private Object[] poli21 = dsd[20].o; // Zones Ligne 21
  private Object[] poli22 = dsd[21].o; // Zones Ligne 22
  private Object[] poli23 = dsd[22].o; // Zones Ligne 23
  private Object[] poli24 = dsd[23].o; // Zones Ligne 24
  private Object[] poli25 = dsd[24].o; // Zones Ligne 25
  private Object[] poli26 = dsd[25].o; // Zones Ligne 26
  private Object[] poli27 = dsd[26].o; // Zones Ligne 27
  private Object[] poli28 = dsd[27].o; // Zones Ligne 28
  private Object[] poli29 = dsd[28].o; // Zones Ligne 29
  private Object[] poli30 = dsd[29].o; // Zones Ligne 30
  private Object[] poli31 = dsd[30].o; // Zones Ligne 31
  private Object[] poli32 = dsd[31].o; // Zones Ligne 32
  private Object[] poli33 = dsd[32].o; // Zones Ligne 33
  private Object[] poli34 = dsd[33].o; // Zones Ligne 34
  private Object[] poli35 = dsd[34].o; // Zones Ligne 35
  private Object[] poli36 = dsd[35].o; // Zones Ligne 36
  private Object[] poli37 = dsd[36].o; // Zones Ligne 37
  private Object[] poli38 = dsd[37].o; // Zones Ligne 38
  private Object[] poli39 = dsd[38].o; // Zones Ligne 39
  private Object[] poli40 = dsd[39].o; // Zones Ligne 40
  private Object[] poli41 = dsd[40].o; // Zones Ligne 41
  private Object[] poli42 = dsd[41].o; // Zones Ligne 42
  private Object[] poli43 = dsd[42].o; // Zones Ligne 43
  private Object[] poli44 = dsd[43].o; // Zones Ligne 44
  private Object[] poli45 = dsd[44].o; // Zones Ligne 45
  private Object[] poli46 = dsd[45].o; // Zones Ligne 46
  private Object[] poli47 = dsd[46].o; // Zones Ligne 47
  private Object[] poli48 = dsd[47].o; // Zones Ligne 48
  private Object[] poli49 = dsd[48].o; // Zones Ligne 49
  private Object[] poli50 = dsd[49].o; // Zones Ligne 50
  private Object[] poli51 = dsd[50].o; // Zones Ligne 51
  private Object[] poli52 = dsd[51].o; // Zones Ligne 52
  private Object[] poli53 = dsd[52].o; // Zones Ligne 53
  private Object[] poli54 = dsd[53].o; // Zones Ligne 54
  private Object[] poli55 = dsd[54].o; // Zones Ligne 55
  private Object[] poli56 = dsd[55].o; // Zones Ligne 56
  private Object[] poli57 = dsd[56].o; // Zones Ligne 57
  private Object[] poli58 = dsd[57].o; // Zones Ligne 58
  private Object[] poli59 = dsd[58].o; // Zones Ligne 59
  private Object[] poli60 = dsd[59].o; // Zones Ligne 60
  private Object[] poli61 = dsd[60].o; // Zones Ligne 61
  private Object[] poli62 = dsd[61].o; // Zones Ligne 62
  private Object[] poli63 = dsd[62].o; // Zones Ligne 63
  private Object[] poli64 = dsd[63].o; // Zones Ligne 64
  private Object[] poli65 = dsd[64].o; // Zones Ligne 65
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400Structure(dsd[0].structure), // Zones Ligne 1
      new AS400Structure(dsd[1].structure), // Zones Ligne 2
      new AS400Structure(dsd[2].structure), // Zones Ligne 3
      new AS400Structure(dsd[3].structure), // Zones Ligne 4
      new AS400Structure(dsd[4].structure), // Zones Ligne 5
      new AS400Structure(dsd[5].structure), // Zones Ligne 6
      new AS400Structure(dsd[6].structure), // Zones Ligne 7
      new AS400Structure(dsd[7].structure), // Zones Ligne 8
      new AS400Structure(dsd[8].structure), // Zones Ligne 9
      new AS400Structure(dsd[9].structure), // Zones Ligne 10
      new AS400Structure(dsd[10].structure), // Zones Ligne 11
      new AS400Structure(dsd[11].structure), // Zones Ligne 12
      new AS400Structure(dsd[12].structure), // Zones Ligne 13
      new AS400Structure(dsd[13].structure), // Zones Ligne 14
      new AS400Structure(dsd[14].structure), // Zones Ligne 15
      new AS400Structure(dsd[15].structure), // Zones Ligne 16
      new AS400Structure(dsd[16].structure), // Zones Ligne 17
      new AS400Structure(dsd[17].structure), // Zones Ligne 18
      new AS400Structure(dsd[18].structure), // Zones Ligne 19
      new AS400Structure(dsd[19].structure), // Zones Ligne 20
      new AS400Structure(dsd[20].structure), // Zones Ligne 21
      new AS400Structure(dsd[21].structure), // Zones Ligne 22
      new AS400Structure(dsd[22].structure), // Zones Ligne 23
      new AS400Structure(dsd[23].structure), // Zones Ligne 24
      new AS400Structure(dsd[24].structure), // Zones Ligne 25
      new AS400Structure(dsd[25].structure), // Zones Ligne 26
      new AS400Structure(dsd[26].structure), // Zones Ligne 27
      new AS400Structure(dsd[27].structure), // Zones Ligne 28
      new AS400Structure(dsd[28].structure), // Zones Ligne 29
      new AS400Structure(dsd[29].structure), // Zones Ligne 30
      new AS400Structure(dsd[30].structure), // Zones Ligne 31
      new AS400Structure(dsd[31].structure), // Zones Ligne 32
      new AS400Structure(dsd[32].structure), // Zones Ligne 33
      new AS400Structure(dsd[33].structure), // Zones Ligne 34
      new AS400Structure(dsd[34].structure), // Zones Ligne 35
      new AS400Structure(dsd[35].structure), // Zones Ligne 36
      new AS400Structure(dsd[36].structure), // Zones Ligne 37
      new AS400Structure(dsd[37].structure), // Zones Ligne 38
      new AS400Structure(dsd[38].structure), // Zones Ligne 39
      new AS400Structure(dsd[39].structure), // Zones Ligne 40
      new AS400Structure(dsd[40].structure), // Zones Ligne 41
      new AS400Structure(dsd[41].structure), // Zones Ligne 42
      new AS400Structure(dsd[42].structure), // Zones Ligne 43
      new AS400Structure(dsd[43].structure), // Zones Ligne 44
      new AS400Structure(dsd[44].structure), // Zones Ligne 45
      new AS400Structure(dsd[45].structure), // Zones Ligne 46
      new AS400Structure(dsd[46].structure), // Zones Ligne 47
      new AS400Structure(dsd[47].structure), // Zones Ligne 48
      new AS400Structure(dsd[48].structure), // Zones Ligne 49
      new AS400Structure(dsd[49].structure), // Zones Ligne 50
      new AS400Structure(dsd[50].structure), // Zones Ligne 51
      new AS400Structure(dsd[51].structure), // Zones Ligne 52
      new AS400Structure(dsd[52].structure), // Zones Ligne 53
      new AS400Structure(dsd[53].structure), // Zones Ligne 54
      new AS400Structure(dsd[54].structure), // Zones Ligne 55
      new AS400Structure(dsd[55].structure), // Zones Ligne 56
      new AS400Structure(dsd[56].structure), // Zones Ligne 57
      new AS400Structure(dsd[57].structure), // Zones Ligne 58
      new AS400Structure(dsd[58].structure), // Zones Ligne 59
      new AS400Structure(dsd[59].structure), // Zones Ligne 60
      new AS400Structure(dsd[60].structure), // Zones Ligne 61
      new AS400Structure(dsd[61].structure), // Zones Ligne 62
      new AS400Structure(dsd[62].structure), // Zones Ligne 63
      new AS400Structure(dsd[63].structure), // Zones Ligne 64
      new AS400Structure(dsd[64].structure), // Zones Ligne 65
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, poli01, poli02, poli03, poli04, poli05, poli06, poli07, poli08, poli09, poli10, poli11, poli12, poli13,
          poli14, poli15, poli16, poli17, poli18, poli19, poli20, poli21, poli22, poli23, poli24, poli25, poli26, poli27, poli28, poli29,
          poli30, poli31, poli32, poli33, poli34, poli35, poli36, poli37, poli38, poli39, poli40, poli41, poli42, poli43, poli44, poli45,
          poli46, poli47, poli48, poli49, poli50, poli51, poli52, poli53, poli54, poli55, poli56, poli57, poli58, poli59, poli60, poli61,
          poli62, poli63, poli64, poli65, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400 et leur redécoupage.
   */
  public void setDSOutput() {
    byte[] as400out = getOutputData();
    int offset = 0;
    poind = (String) ds.getMembers(0).toObject(as400out, offset);
    offset += SIZE_POIND;
    poli01 = convertTObject(as400out, offset, 1);
    offset += SIZE_POLI01;
    poli02 = convertTObject(as400out, offset, 2);
    offset += SIZE_POLI02;
    poli03 = convertTObject(as400out, offset, 3);
    offset += SIZE_POLI03;
    poli04 = convertTObject(as400out, offset, 4);
    offset += SIZE_POLI04;
    poli05 = convertTObject(as400out, offset, 5);
    offset += SIZE_POLI05;
    poli06 = convertTObject(as400out, offset, 6);
    offset += SIZE_POLI06;
    poli07 = convertTObject(as400out, offset, 7);
    offset += SIZE_POLI07;
    poli08 = convertTObject(as400out, offset, 8);
    offset += SIZE_POLI08;
    poli09 = convertTObject(as400out, offset, 9);
    offset += SIZE_POLI09;
    poli10 = convertTObject(as400out, offset, 10);
    offset += SIZE_POLI10;
    poli11 = convertTObject(as400out, offset, 11);
    offset += SIZE_POLI11;
    poli12 = convertTObject(as400out, offset, 12);
    offset += SIZE_POLI12;
    poli13 = convertTObject(as400out, offset, 13);
    offset += SIZE_POLI13;
    poli14 = convertTObject(as400out, offset, 14);
    offset += SIZE_POLI14;
    poli15 = convertTObject(as400out, offset, 15);
    offset += SIZE_POLI15;
    poli16 = convertTObject(as400out, offset, 16);
    offset += SIZE_POLI16;
    poli17 = convertTObject(as400out, offset, 17);
    offset += SIZE_POLI17;
    poli18 = convertTObject(as400out, offset, 18);
    offset += SIZE_POLI18;
    poli19 = convertTObject(as400out, offset, 19);
    offset += SIZE_POLI19;
    poli20 = convertTObject(as400out, offset, 20);
    offset += SIZE_POLI20;
    poli21 = convertTObject(as400out, offset, 21);
    offset += SIZE_POLI21;
    poli22 = convertTObject(as400out, offset, 22);
    offset += SIZE_POLI22;
    poli23 = convertTObject(as400out, offset, 23);
    offset += SIZE_POLI23;
    poli24 = convertTObject(as400out, offset, 24);
    offset += SIZE_POLI24;
    poli25 = convertTObject(as400out, offset, 25);
    offset += SIZE_POLI25;
    poli26 = convertTObject(as400out, offset, 26);
    offset += SIZE_POLI26;
    poli27 = convertTObject(as400out, offset, 27);
    offset += SIZE_POLI27;
    poli28 = convertTObject(as400out, offset, 28);
    offset += SIZE_POLI28;
    poli29 = convertTObject(as400out, offset, 29);
    offset += SIZE_POLI29;
    poli30 = convertTObject(as400out, offset, 30);
    offset += SIZE_POLI30;
    poli31 = convertTObject(as400out, offset, 31);
    offset += SIZE_POLI31;
    poli32 = convertTObject(as400out, offset, 32);
    offset += SIZE_POLI32;
    poli33 = convertTObject(as400out, offset, 33);
    offset += SIZE_POLI33;
    poli34 = convertTObject(as400out, offset, 34);
    offset += SIZE_POLI34;
    poli35 = convertTObject(as400out, offset, 35);
    offset += SIZE_POLI35;
    poli36 = convertTObject(as400out, offset, 36);
    offset += SIZE_POLI36;
    poli37 = convertTObject(as400out, offset, 37);
    offset += SIZE_POLI37;
    poli38 = convertTObject(as400out, offset, 38);
    offset += SIZE_POLI38;
    poli39 = convertTObject(as400out, offset, 39);
    offset += SIZE_POLI39;
    poli40 = convertTObject(as400out, offset, 40);
    offset += SIZE_POLI40;
    poli41 = convertTObject(as400out, offset, 41);
    offset += SIZE_POLI41;
    poli42 = convertTObject(as400out, offset, 42);
    offset += SIZE_POLI42;
    poli43 = convertTObject(as400out, offset, 43);
    offset += SIZE_POLI43;
    poli44 = convertTObject(as400out, offset, 44);
    offset += SIZE_POLI44;
    poli45 = convertTObject(as400out, offset, 45);
    offset += SIZE_POLI45;
    poli46 = convertTObject(as400out, offset, 46);
    offset += SIZE_POLI46;
    poli47 = convertTObject(as400out, offset, 47);
    offset += SIZE_POLI47;
    poli48 = convertTObject(as400out, offset, 48);
    offset += SIZE_POLI48;
    poli49 = convertTObject(as400out, offset, 49);
    offset += SIZE_POLI49;
    poli50 = convertTObject(as400out, offset, 50);
    offset += SIZE_POLI50;
    poli51 = convertTObject(as400out, offset, 51);
    offset += SIZE_POLI51;
    poli52 = convertTObject(as400out, offset, 52);
    offset += SIZE_POLI52;
    poli53 = convertTObject(as400out, offset, 53);
    offset += SIZE_POLI53;
    poli54 = convertTObject(as400out, offset, 54);
    offset += SIZE_POLI54;
    poli55 = convertTObject(as400out, offset, 55);
    offset += SIZE_POLI55;
    poli56 = convertTObject(as400out, offset, 56);
    offset += SIZE_POLI56;
    poli57 = convertTObject(as400out, offset, 57);
    offset += SIZE_POLI57;
    poli58 = convertTObject(as400out, offset, 58);
    offset += SIZE_POLI58;
    poli59 = convertTObject(as400out, offset, 59);
    offset += SIZE_POLI59;
    poli60 = convertTObject(as400out, offset, 60);
    offset += SIZE_POLI60;
    poli61 = convertTObject(as400out, offset, 61);
    offset += SIZE_POLI61;
    poli62 = convertTObject(as400out, offset, 62);
    offset += SIZE_POLI62;
    poli63 = convertTObject(as400out, offset, 63);
    offset += SIZE_POLI63;
    poli64 = convertTObject(as400out, offset, 64);
    offset += SIZE_POLI64;
    poli65 = convertTObject(as400out, offset, 65);
    offset += SIZE_POLI65;
    poarr = (String) ds.getMembers(66).toObject(as400out, offset);
    offset += SIZE_POARR;
  }
  
  /**
   * Retourne un tableau des valeurs correspondants aux lignes.
   */
  public Object[] getValeursBrutesLignes() {
    Object[] o = { poli01, poli02, poli03, poli04, poli05, poli06, poli07, poli08, poli09, poli10, poli11, poli12, poli13, poli14, poli15,
        poli16, poli17, poli18, poli19, poli20, poli21, poli22, poli23, poli24, poli25, poli26, poli27, poli28, poli29, poli30, poli31,
        poli32, poli33, poli34, poli35, poli36, poli37, poli38, poli39, poli40, poli41, poli42, poli43, poli44, poli45, poli46, poli47,
        poli48, poli49, poli50, poli51, poli52, poli53, poli54, poli55, poli56, poli57, poli58, poli59, poli60, poli61, poli62, poli63,
        poli64, poli65, };
    controlerProblemeConversionElement(o);
    return o;
  }
  
  /**
   * Convertit les valeurs de la datastructure.
   */
  private Object[] convertTObject(byte[] out, int offset, int indice) {
    try {
      return (Object[]) ds.getMembers(indice).toObject(out, offset);
    }
    catch (NumberFormatException e) {
      return null;
    }
  }
  
  /**
   * Permet de contrôler qu'un élément du tableau ne contient pas une erreur de données décimale lors de la conversion RPG -> Java.
   */
  private void controlerProblemeConversionElement(Object[] pTableau) {
    if (pTableau == null) {
      return;
    }
    for (int i = 0; i < pTableau.length; i++) {
      int j = i + 1;
      if (j < pTableau.length && pTableau[i] == null && pTableau[j] != null) {
        throw new MessageErreurException(
            "Il y a eu un problème lors de la conversion (RPG -> Java) des données à l'indice " + i + " du tableau. ");
      }
    }
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
