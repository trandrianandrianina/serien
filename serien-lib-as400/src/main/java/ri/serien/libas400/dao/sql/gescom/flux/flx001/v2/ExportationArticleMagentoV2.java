/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx001.v2;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.JsonArticleMagentoV2;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

public class ExportationArticleMagentoV2 extends GM_Export_articlesV2 {
  
  /**
   * Constructeur avec le queryManager en paramètre pour les requetes sur DB2
   */
  public ExportationArticleMagentoV2(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Retourner un article Série N sous forme de message JSON
   */
  @Override
  public String retournerUnArticleMagentoJSON(FluxMagento record) {
    if (record == null || record.getFLETB() == null || record.getFLCOD() == null) {
      majError("[ExportationArticleMagentoV2] retournerUnArticleMagentoJSON() -> valeurs du M_FluxMagento record corrompues ");
      return null;
    }
    
    String retour = null;
    
    GenericRecord unArticleDB2 = retournerUnArticleDB2(record.getFLETB(), record.getFLCOD());
    
    if (unArticleDB2 != null) {
      // On créé un article spécifique Magento
      JsonArticleMagentoV2 articleMag = new JsonArticleMagentoV2(record);
      Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), articleMag.getEtb());
      if (numeroInstance == null) {
        majError("[ExportArticles] retournerUnArticleMagentoJSON() : probleme d'interpretation de l'ETb vers l'instance Magento");
        return null;
      }
      articleMag.setIdInstanceMagento(numeroInstance);
      
      // Informations basiques de l'article
      if (!articleMag.mettreAJourArticle(unArticleDB2)) {
        majError(articleMag.getMsgError());
        return null;
      }
      
      // on bloque le flux si la variable non géré sur le web est positionné à 1
      if (articleMag.getExclutWeb() == null || articleMag.getExclutWeb().equals(JsonArticleMagentoV2.JAMAIS_VU_SUR_LE_WEB)) {
        majError("L'article n'est pas encoré géré sur le Web");
        return "";
      }
      
      // Informations annexes
      // Tarifs
      if (!articleMag.gererLesTarifs(retournerLesTarifsUnArticle(record.getFLETB(), record.getFLCOD()))) {
        // TODO pour le moment on ne le considère pas comme une erreur mais comme un avertissement d'initialisation
        // ON BLOQUE LE FLUX mais pas en erreur du coup pour alerter le gestionnaire on envoie du vide et non du NULL (erreur)
        majError(articleMag.getMsgError());
        return "";
      }
      // DEEE
      articleMag.calculerMontantDEEE(retournerDEEEarticle(record.getFLETB(), record.getFLCOD()));
      
      // Gestion de la mise à disposition et la visibilité de l'article
      // Si l'article n'est pas exclu du Web on envoie la liste de site Web sinon une liste vide
      // De cette manière on s'affranchit de l'envoi du flux ou non
      if (articleMag.getExclutWeb() != null && articleMag.getExclutWeb().equals(JsonArticleMagentoV2.SUR_LE_WEB)) {
        articleMag.gererVisibiliteDesSites(recupererSitesVisibles(record.getFLETB(), record.getFLCOD()));
      }
      else {
        articleMag.gererVisibiliteDesSites(null);
      }
      
      // référence fabricant:
      // String ref = recupererReferenceFournisseur(record.getFLETB(), record.getFLCOD());
      String ref = recupererReferenceFabricant(record.getFLETB(), record.getFLCOD());
      if (ref != null) {
        articleMag.setReferenceFabricant(ref);
      }
      
      // debut de la construction du JSON
      if (initJSON()) {
        if (articleMag.getCode() != null && articleMag.getIdInstanceMagento() > 0) {
          try {
            // On JSONE l'objet comme un sauvage
            retour = gson.toJson(articleMag);
          }
          catch (Exception e) {
            retour = null;
            majError("[ExportationArticleMagentoV2] retournerUnArticleMagentoJSON() -> ERREUR JSON: " + e.getMessage());
          }
        }
        else {
          majError("[ExportationArticleMagentoV2] retournerUnArticleMagentoJSON() -> articleMag corrompu ");
        }
      }
    }
    
    return retour;
  }
}
