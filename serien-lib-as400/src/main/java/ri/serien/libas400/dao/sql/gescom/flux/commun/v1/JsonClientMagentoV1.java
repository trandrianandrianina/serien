/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v1;

import java.util.Date;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Constantes;

/**
 * Version 1 des flux.
 * Client Série N au format Magento
 * Cette classe est une classe qui sert à l'intégration JAVA vers JSON ou JSON vers JAVA
 * IL NE FAUT PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonClientMagentoV1 extends JsonEntiteMagento {
  // Variables
  private transient String CLCLI = null;
  private transient String CLLIV = null;
  // SIRET découpé en 2
  private transient String CLSRN = null;
  private transient String CLSRT = null;
  // BLOC ADRESSE FICHIER SERIE N
  private transient String CLNOM = null;
  private transient String CLCPL = null;
  private transient String CLRUE = null;
  private transient String CLLOC = null;
  private transient String CLTAR = "1";
  // Compte Général compta
  private transient String CLNCG = null;
  
  // Client particulier ou NON
  private transient String CLIN9 = " ";
  public static final transient String codePaysDefaut = "FR";
  public static final transient String libPaysDefaut = "FRANCE";
  public transient String libellePays = "FRANCE";
  
  private String timestampCrea = null;
  private String idClientMagento = null;
  private String email = null;
  private String siren = null;
  private String codeAPE = null;
  private String categorie = null;
  private String nom = null;
  private String tvaClient = null;
  private int tarif = 1;
  private transient String codeReglement = null;
  
  // Utilisés uniquement dans le cas d'un client particulier
  private transient String numeroTelephoneContact = null;
  private transient String numeroFaxContact = null;
  
  // Bloc adresse facturation
  private String civiliteFact = null;
  private String nomFact = null;
  private String prenomFact = null;
  private String rueFact = null;
  private String rue2Fact = null;
  private String cdpFact = null;
  private String villeFact = null;
  private String paysFact = null;
  private String telFact = null;
  private String faxFact = null;
  
  private String magClient = null;
  private String siteClient = null;
  
  // Infos pour insertion en Compta
  private transient String etbComptable = null;
  private transient String compteGeneral = null;
  
  /**
   * Constructeur par défaut pour les flux rentrants
   */
  public JsonClientMagentoV1() {
    
  }
  
  /**
   * Constructeur Client pour Les flux sortants
   */
  public JsonClientMagentoV1(FluxMagento record) {
    idFlux = record.getFLIDF();
    versionFlux = record.getFLVER();
    etb = record.getFLETB();
    
    // Gestion du timestamp de création du flux en millisecondes
    long tempsMs = new Date().getTime();
    timestampCrea = String.valueOf(tempsMs);
  }
  
  /**
   * On met à jour le client sur la base des informations DB2 / flux sortants
   * Pour le moment il a été choisi de stocker les données clients Série N - Magento de la manière suivante:
   * Particulier nomFact : CLNOM, prenomFact : CLCPL, rueFact : CLRUE, rue2Fact : CLLOC
   * la zone "nom" transmise à Magento doit être blanche ou nulle dans le cas des particuliers
   * Pros: nom : CLNOM + CLCPL, rueFact : CLRUE, rue2Fact : CLLOC
   */
  public boolean majDesInfosPourExport(GenericRecord clientDB2, ParametresFluxBibli pParam) {
    if (clientDB2 == null) {
      majError("[ClientMagento] majDesInfos() clientDB2 à NULL");
      return false;
    }
    
    boolean retour = false;
    int nbErreurs = 0;
    
    if (!clientDB2.isPresentField("CLCLI") || !clientDB2.isPresentField("CLLIV")) {
      majError("[ClientMagento] majDesInfos() clientDB2 corrompu CLCLI CLLIV");
      return false;
    }
    
    CLCLI = clientDB2.getField("CLCLI").toString().trim();
    CLLIV = clientDB2.getField("CLLIV").toString().trim();
    
    if (clientDB2.isPresentField("CLADH")) {
      idClientMagento = clientDB2.getField("CLADH").toString().trim();
    }
    else {
      idClientMagento = "";
    }
    
    // Important Car il détermine s'il s'agit d'un particulier ou NON
    if (clientDB2.isPresentField("CLCAT")) {
      categorie = clientDB2.getField("CLCAT").toString().trim();
    }
    else {
      majError("[ClientMagento] majDesInfos(): categorie NULL");
      nbErreurs++;
    }
    
    // On ne transmet le CLNOM (nom société que dans la mesure où le client n'est pas un client particulier
    if (categorie != null && categorie.equals(pParam.getCategorieParticulierMg())) {
      nom = "";
    }
    else if (clientDB2.isPresentField("CLNOM")) {
      nom = clientDB2.getField("CLNOM").toString().trim();
    }
    else {
      nom = "";
    }
    
    if (clientDB2.isPresentField("CLAPEN")) {
      codeAPE = clientDB2.getField("CLAPEN").toString().trim();
    }
    else {
      codeAPE = "";
    }
    
    // TODO tables de correspondances ??
    if (clientDB2.isPresentField("CLRGL")) {
      codeReglement = clientDB2.getField("CLRGL").toString().trim();
    }
    else {
      codeReglement = "";
    }
    
    if (clientDB2.isPresentField("CLRUE")) {
      rueFact = clientDB2.getField("CLRUE").toString().trim();
    }
    else {
      rueFact = "";
    }
    
    if (clientDB2.isPresentField("CLLOC")) {
      rue2Fact = clientDB2.getField("CLLOC").toString().trim();
    }
    else {
      rue2Fact = "";
    }
    
    if (clientDB2.isPresentField("CLVIL")) {
      // TODO pour le moment on bloque pas mais ca pue du cul si on a pas ces infos: A voir
      if (!decouperVilleEtCDP(clientDB2.getField("CLVIL").toString().trim())) {
        cdpFact = "";
        villeFact = "";
      }
    }
    
    // Si le code pays est null ou blanc on met la FrrrrAAAAAnnnce !!
    if (clientDB2.isPresentField("CLCOP") && !clientDB2.getField("CLCOP").toString().trim().equals("")) {
      paysFact = clientDB2.getField("CLCOP").toString().trim();
    }
    else {
      paysFact = codePaysDefaut;
    }
    
    if (clientDB2.isPresentField("CLPAY") && !clientDB2.getField("CLPAY").toString().trim().equals("")) {
      libellePays = clientDB2.getField("CLPAY").toString().trim();
    }
    else {
      libellePays = libPaysDefaut;
    }
    
    if (clientDB2.isPresentField("CLTEL")) {
      telFact = clientDB2.getField("CLTEL").toString().trim();
    }
    else {
      telFact = "";
    }
    
    if (clientDB2.isPresentField("CLFAX")) {
      faxFact = clientDB2.getField("CLFAX").toString().trim();
    }
    else {
      faxFact = "";
    }
    
    if (clientDB2.isPresentField("CLSRN") && clientDB2.isPresentField("CLSRT")) {
      siren = clientDB2.getField("CLSRN").toString().trim() + clientDB2.getField("CLSRT").toString().trim();
    }
    else {
      siren = "";
    }
    
    if (clientDB2.isPresentField("CLTOP1")) {
      magClient = clientDB2.getField("CLTOP1").toString().trim();
    }
    else {
      magClient = "";
    }
    
    if (clientDB2.isPresentField("CLTOP2")) {
      siteClient = clientDB2.getField("CLTOP2").toString().trim();
    }
    else {
      siteClient = "";
    }
    
    if (clientDB2.isPresentField("CLTAR")) {
      try {
        tarif = Integer.parseInt(clientDB2.getField("CLTAR").toString().trim());
      }
      catch (Exception e) {
        tarif = -1;
      }
    }
    
    // SI LE CLIENT EST UN PARTICULIER VA FALLOIR RECUPERER SES INFOS
    if (clientDB2.isPresentField("RENET")) {
      email = clientDB2.getField("RENET").toString().trim();
    }
    else {
      email = "";
    }
    
    if (clientDB2.isPresentField("RECIV")) {
      civiliteFact = clientDB2.getField("RECIV").toString().trim();
    }
    else {
      civiliteFact = "";
    }
    
    if (clientDB2.isPresentField("RENOM")) {
      nomFact = clientDB2.getField("RENOM").toString().trim();
    }
    else {
      nomFact = "";
    }
    if (clientDB2.isPresentField("REPRE")) {
      prenomFact = clientDB2.getField("REPRE").toString().trim();
    }
    else {
      prenomFact = "";
    }
    
    // TODO faut récupérer cette fucking info d'une manièere ou d'une autre pour l'instant c'est d'une autre
    tvaClient = pParam.getModeFacturationClient();
    
    // On fait le point à ce stade
    if (nbErreurs == 0) {
      retour = true;
    }
    
    return retour;
  }
  
  /**
   * Permet de découper l'algorithme de Série N pour le cdp et la ville sur la zone CLVIL
   */
  private boolean decouperVilleEtCDP(String zoneMoisie) {
    if (zoneMoisie == null || zoneMoisie.trim().length() < 7) {
      return false;
    }
    
    cdpFact = zoneMoisie.substring(0, 5);
    villeFact = zoneMoisie.substring(6);
    
    // TODO tester les longueurs
    return true;
  }
  
  /**
   * Mise en forme du bloc adresse en import de données depuis Magento
   */
  public void miseEnFormeBlocAdresseImport(ParametresFluxBibli pParam) {
    CLNOM = "";
    CLCPL = "";
    
    // Si on a affaire à un particulier on va concaténer le nom et le prénom dans la même zone
    // Les véritables données sont dans le contact
    if (categorie != null && categorie.equals(pParam.getCategorieParticulierMg())) {
      if (nomFact != null) {
        CLNOM = nomFact.trim();
      }
      if (prenomFact != null) {
        CLNOM += " " + prenomFact.trim();
      }
      CLNOM = formaterSaisieMax(CLNOM, 30);
      CLCPL = "";
      
      // Copie des numéros de téléphone et de fax uniquement si le client est un particulier
      numeroTelephoneContact = Constantes.normerTexte(telFact);
      numeroFaxContact = Constantes.normerTexte(faxFact);
    }
    // Un pro (alors on checke la zone "nom")
    else {
      if (nom != null) {
        if (nom.trim().length() < 30) {
          CLNOM = nom.trim();
        }
        else {
          // On réduit à 60 Max (2 x 30)
          nom = formaterSaisieMax(nom.trim(), 60);
          CLNOM = nom.substring(0, 30);
          CLCPL = nom.substring(30);
        }
      }
      // Dans le cas d'une société les numéros de téléphone et de fax sont initialisés à null
      numeroTelephoneContact = null;
      numeroFaxContact = null;
    }
    
    CLRUE = "";
    CLLOC = "";
    
    // TODO alog de découp propre VOIR EDI
    if (rueFact != null) {
      CLRUE = rueFact;
    }
    if (rue2Fact != null) {
      CLLOC = rue2Fact;
    }
  }
  
  /**
   * traitement divers avant import/export: Dans la mesure où on recoit la catégorie et non le type de client,
   * il faut faire une consversion sur certaines variables
   */
  public void traitementsDivers(ParametresFluxBibli pParam) {
    // Colonne tarifaire
    // TODO pas beau faire ça un peu plus paramétré qu'avec les valeurs en duuuuur
    // Et puis faut le mettre dans le client tout ça
    if (categorie != null) {
      if (categorie.equals(pParam.getCategorieParticulierMg())) {
        CLTAR = "2";
        CLIN9 = "1";
      }
      else if (categorie.equals(pParam.getCategorieArtisanMg())) {
        CLTAR = "5";
      }
    }
    
  }
  
  /**
   * Formater une saisie utilisateur à la longueur maximale de la zone
   */
  public String formaterSaisieMax(String saisie, int longueurMax) {
    if (saisie == null || saisie.trim().length() <= longueurMax) {
      return saisie;
    }
    
    return saisie.trim().substring(0, longueurMax);
  }
  
  /**
   * nettoyer des caractères spéciaux avant INSERT OU UPDATE
   * Ne plus utiliser, préférez : QueryManager.formaterStringSQL(Constantes.normerTexte(XXX))
   */
  public String nettoyerSaisies(String donnee) {
    if (donnee == null) {
      return donnee;
    }
    
    return (donnee.replace("'", "''")).trim();
  }
  
  // -- Accesseurs
  
  public String getIdClientMagento() {
    return idClientMagento;
  }
  
  public void setIdClientMagento(String idClientMagento) {
    this.idClientMagento = idClientMagento;
  }
  
  public String getEmail() {
    return email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getSiren() {
    return siren;
  }
  
  public void setSiren(String siren) {
    this.siren = siren;
  }
  
  public String getCodeAPE() {
    return codeAPE;
  }
  
  public void setCodeAPE(String codeAPE) {
    this.codeAPE = codeAPE;
  }
  
  public String getCategorie() {
    return categorie;
  }
  
  public void setCategorie(String categorie) {
    this.categorie = categorie;
  }
  
  public String getNom() {
    return nom;
  }
  
  public void setNom(String nom) {
    this.nom = nom;
  }
  
  public String getTvaClient() {
    return tvaClient;
  }
  
  public void setTvaClient(String tvaClient) {
    this.tvaClient = tvaClient;
  }
  
  public String getCiviliteFact() {
    return civiliteFact;
  }
  
  public void setCiviliteFact(String civiliteFact) {
    this.civiliteFact = civiliteFact;
  }
  
  public String getNomFact() {
    return nomFact;
  }
  
  public void setNomFact(String nomFact) {
    this.nomFact = nomFact;
  }
  
  public String getPrenomFact() {
    return prenomFact;
  }
  
  public void setPrenomFact(String prenomFact) {
    this.prenomFact = prenomFact;
  }
  
  public String getRueFact() {
    return rueFact;
  }
  
  public void setRueFact(String rueFact) {
    this.rueFact = rueFact;
  }
  
  public String getCdpFact() {
    return cdpFact;
  }
  
  public void setCdpFact(String cdpFact) {
    this.cdpFact = cdpFact;
  }
  
  public String getVilleFact() {
    return villeFact;
  }
  
  public void setVilleFact(String villeFact) {
    this.villeFact = villeFact;
  }
  
  public String getPaysFact() {
    return paysFact;
  }
  
  public void setPaysFact(String paysFact) {
    this.paysFact = paysFact;
  }
  
  public String getTelFact() {
    return telFact;
  }
  
  public void setTelFact(String telFact) {
    this.telFact = telFact;
  }
  
  public String getMagClient() {
    return magClient;
  }
  
  public void setMagClient(String magClient) {
    this.magClient = magClient;
  }
  
  public String getSiteClient() {
    return siteClient;
  }
  
  public void setSiteClient(String siteClient) {
    this.siteClient = siteClient;
  }
  
  public String getCLSRN() {
    return CLSRN;
  }
  
  public void setCLSRN(String cLSRN) {
    CLSRN = cLSRN;
  }
  
  public String getCLSRT() {
    return CLSRT;
  }
  
  public void setCLSRT(String cLSRT) {
    CLSRT = cLSRT;
  }
  
  public String getCodeReglement() {
    return codeReglement;
  }
  
  public void setCodeReglement(String codeReglement) {
    this.codeReglement = codeReglement;
  }
  
  public String getCLIN9() {
    return CLIN9;
  }
  
  public void setCLIN9(String cLIN9) {
    CLIN9 = cLIN9;
  }
  
  public String getFaxFact() {
    return faxFact;
  }
  
  public void setFaxFact(String faxFact) {
    this.faxFact = faxFact;
  }
  
  public String getCLCLI() {
    return CLCLI;
  }
  
  public void setCLCLI(String cLCLI) {
    CLCLI = cLCLI;
  }
  
  public String getCLLIV() {
    return CLLIV;
  }
  
  public void setCLLIV(String cLLIV) {
    CLLIV = cLLIV;
  }
  
  public String getLibellePays() {
    return libellePays;
  }
  
  public void setLibellePays(String libellePays) {
    this.libellePays = libellePays;
  }
  
  public String getRue2Fact() {
    return rue2Fact;
  }
  
  public void setRue2Fact(String rue2Fact) {
    this.rue2Fact = rue2Fact;
  }
  
  public String getCLNOM() {
    return CLNOM;
  }
  
  public void setCLNOM(String cLNOM) {
    CLNOM = cLNOM;
  }
  
  public String getCLCPL() {
    return CLCPL;
  }
  
  public void setCLCPL(String cLCPL) {
    CLCPL = cLCPL;
  }
  
  public String getCLRUE() {
    return CLRUE;
  }
  
  public void setCLRUE(String cLRUE) {
    CLRUE = cLRUE;
  }
  
  public String getCLLOC() {
    return CLLOC;
  }
  
  public void setCLLOC(String cLLOC) {
    CLLOC = cLLOC;
  }
  
  public String getTimestampCrea() {
    return timestampCrea;
  }
  
  public void setTimestampCrea(String timestampCrea) {
    this.timestampCrea = timestampCrea;
  }
  
  public String getCLTAR() {
    return CLTAR;
  }
  
  public void setCLTAR(String cLTAR) {
    CLTAR = cLTAR;
  }
  
  public String getCLNCG() {
    return CLNCG;
  }
  
  public void setCLNCG(String cLNCG) {
    CLNCG = cLNCG;
  }
  
  public String getEtbComptable() {
    return etbComptable;
  }
  
  public void setEtbComptable(String etbComptable) {
    this.etbComptable = etbComptable;
  }
  
  public String getCompteGeneral() {
    return compteGeneral;
  }
  
  public void setCompteGeneral(String compteGeneral) {
    this.compteGeneral = compteGeneral;
  }
  
  public String getNumeroTelephoneContact() {
    return numeroTelephoneContact;
  }
  
  public String getNumeroFaxContact() {
    return numeroFaxContact;
  }
  
}
