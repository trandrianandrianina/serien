/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_CV pour les CV
 */
public class Pgvmparm_ds_CV extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    // A contrôler car à l'origine c'est une zone packed
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(50, 0), "CVCT"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "CVCOL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(36), "CVS"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "CVETC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CVCJV"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CVCJR"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVCPC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVSEC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVSER"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVREF"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CVATT"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CVDIF"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CVCOA1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CVJOA1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CVCOA2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CVJOA2"));
    
    length = 300;
  }
}
