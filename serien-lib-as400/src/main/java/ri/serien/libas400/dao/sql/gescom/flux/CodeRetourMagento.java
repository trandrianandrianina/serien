/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux;

public class CodeRetourMagento {
  private int code_retour = -1;
  
  public CodeRetourMagento() {
  }
  
  public int getCode_retour() {
    return code_retour;
  }
  
  public void setCode_retour(int code_retour) {
    this.code_retour = code_retour;
  }
  
}
