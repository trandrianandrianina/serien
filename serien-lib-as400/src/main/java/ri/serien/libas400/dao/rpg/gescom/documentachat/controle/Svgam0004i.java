/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.controle;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0004i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND4 = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIOPT = 3;
  public static final int SIZE_PINATA = 5;
  public static final int SIZE_L0UNC = 2;
  public static final int SIZE_L0UNA = 2;
  public static final int SIZE_L0QTA = 11;
  public static final int DECIMAL_L0QTA = 3;
  public static final int SIZE_L0QTC = 11;
  public static final int DECIMAL_L0QTC = 3;
  public static final int SIZE_L0QTS = 11;
  public static final int DECIMAL_L0QTS = 3;
  public static final int SIZE_L0KSC = 8;
  public static final int DECIMAL_L0KSC = 3;
  public static final int SIZE_L0KAC = 8;
  public static final int DECIMAL_L0KAC = 3;
  public static final int SIZE_L0PAB = 9;
  public static final int DECIMAL_L0PAB = 2;
  public static final int SIZE_L0PAN = 9;
  public static final int DECIMAL_L0PAN = 2;
  public static final int SIZE_L0REM1 = 4;
  public static final int DECIMAL_L0REM1 = 2;
  public static final int SIZE_L0REM2 = 4;
  public static final int DECIMAL_L0REM2 = 2;
  public static final int SIZE_L0REM3 = 4;
  public static final int DECIMAL_L0REM3 = 2;
  public static final int SIZE_L0REM4 = 4;
  public static final int DECIMAL_L0REM4 = 2;
  public static final int SIZE_L0REM5 = 4;
  public static final int DECIMAL_L0REM5 = 2;
  public static final int SIZE_L0REM6 = 4;
  public static final int DECIMAL_L0REM6 = 2;
  public static final int SIZE_L0MHT = 11;
  public static final int DECIMAL_L0MHT = 2;
  public static final int SIZE_LITOP = 1;
  public static final int DECIMAL_LITOP = 0;
  public static final int SIZE_LICOD = 1;
  public static final int SIZE_LIETB = 3;
  public static final int SIZE_LINUM = 6;
  public static final int DECIMAL_LINUM = 0;
  public static final int SIZE_LISUF = 1;
  public static final int DECIMAL_LISUF = 0;
  public static final int SIZE_LINLI = 4;
  public static final int DECIMAL_LINLI = 0;
  public static final int SIZE_LIERL = 1;
  public static final int SIZE_LICEX = 1;
  public static final int SIZE_LIQCX = 11;
  public static final int DECIMAL_LIQCX = 3;
  public static final int SIZE_LITVA = 1;
  public static final int DECIMAL_LITVA = 0;
  public static final int SIZE_LITB = 1;
  public static final int DECIMAL_LITB = 0;
  public static final int SIZE_LITR = 1;
  public static final int DECIMAL_LITR = 0;
  public static final int SIZE_LITN = 1;
  public static final int DECIMAL_LITN = 0;
  public static final int SIZE_LITU = 1;
  public static final int DECIMAL_LITU = 0;
  public static final int SIZE_LITQ = 1;
  public static final int DECIMAL_LITQ = 0;
  public static final int SIZE_LITH = 1;
  public static final int DECIMAL_LITH = 0;
  public static final int SIZE_LIDCC = 1;
  public static final int DECIMAL_LIDCC = 0;
  public static final int SIZE_LIDCA = 1;
  public static final int DECIMAL_LIDCA = 0;
  public static final int SIZE_LIDCS = 1;
  public static final int DECIMAL_LIDCS = 0;
  public static final int SIZE_LIVAL = 1;
  public static final int DECIMAL_LIVAL = 0;
  public static final int SIZE_LICOL = 1;
  public static final int DECIMAL_LICOL = 0;
  public static final int SIZE_LISGN = 1;
  public static final int DECIMAL_LISGN = 0;
  public static final int SIZE_LIQTC = 11;
  public static final int DECIMAL_LIQTC = 3;
  public static final int SIZE_LIUNC = 2;
  public static final int SIZE_LIQTS = 11;
  public static final int DECIMAL_LIQTS = 3;
  public static final int SIZE_LIKSC = 8;
  public static final int DECIMAL_LIKSC = 3;
  public static final int SIZE_LIPAB = 9;
  public static final int DECIMAL_LIPAB = 2;
  public static final int SIZE_LIPAN = 9;
  public static final int DECIMAL_LIPAN = 2;
  public static final int SIZE_LIPAC = 9;
  public static final int DECIMAL_LIPAC = 2;
  public static final int SIZE_LIMHT = 11;
  public static final int DECIMAL_LIMHT = 2;
  public static final int SIZE_LIAVR = 1;
  public static final int SIZE_LIQTA = 11;
  public static final int DECIMAL_LIQTA = 3;
  public static final int SIZE_LIUNA = 2;
  public static final int SIZE_LIKAC = 8;
  public static final int DECIMAL_LIKAC = 3;
  public static final int SIZE_LIART = 20;
  public static final int SIZE_LIMAG = 2;
  public static final int SIZE_LIMAGA = 2;
  public static final int SIZE_LINUMR = 6;
  public static final int DECIMAL_LINUMR = 0;
  public static final int SIZE_LISUFR = 1;
  public static final int DECIMAL_LISUFR = 0;
  public static final int SIZE_LINLIR = 4;
  public static final int DECIMAL_LINLIR = 0;
  public static final int SIZE_LIDATH = 7;
  public static final int DECIMAL_LIDATH = 0;
  public static final int SIZE_LIORDH = 3;
  public static final int DECIMAL_LIORDH = 0;
  public static final int SIZE_LISAN = 4;
  public static final int SIZE_LIACT = 4;
  public static final int SIZE_LINAT = 1;
  public static final int SIZE_LIMTA = 9;
  public static final int DECIMAL_LIMTA = 2;
  public static final int SIZE_LIDLP = 7;
  public static final int DECIMAL_LIDLP = 0;
  public static final int SIZE_LISER = 2;
  public static final int DECIMAL_LISER = 0;
  public static final int SIZE_LIPAI = 9;
  public static final int DECIMAL_LIPAI = 2;
  public static final int SIZE_LIPAR = 9;
  public static final int DECIMAL_LIPAR = 2;
  public static final int SIZE_LIREM1 = 4;
  public static final int DECIMAL_LIREM1 = 2;
  public static final int SIZE_LIREM2 = 4;
  public static final int DECIMAL_LIREM2 = 2;
  public static final int SIZE_LIREM3 = 4;
  public static final int DECIMAL_LIREM3 = 2;
  public static final int SIZE_LIREM4 = 4;
  public static final int DECIMAL_LIREM4 = 2;
  public static final int SIZE_LIREM5 = 4;
  public static final int DECIMAL_LIREM5 = 2;
  public static final int SIZE_LIREM6 = 4;
  public static final int DECIMAL_LIREM6 = 2;
  public static final int SIZE_LITRL = 1;
  public static final int SIZE_LIBRL = 1;
  public static final int SIZE_LIRP1 = 1;
  public static final int SIZE_LIRP2 = 1;
  public static final int SIZE_LIRP3 = 1;
  public static final int SIZE_LIRP4 = 1;
  public static final int SIZE_LIRP5 = 1;
  public static final int SIZE_LIRP6 = 1;
  public static final int SIZE_LITP1 = 2;
  public static final int SIZE_LITP2 = 2;
  public static final int SIZE_LITP3 = 2;
  public static final int SIZE_LITP4 = 2;
  public static final int SIZE_LITP5 = 2;
  public static final int SIZE_LIIN1 = 1;
  public static final int SIZE_LIIN2 = 1;
  public static final int SIZE_LIIN3 = 1;
  public static final int SIZE_LIIN4 = 1;
  public static final int SIZE_LIIN5 = 1;
  public static final int SIZE_LIDLC = 7;
  public static final int DECIMAL_LIDLC = 0;
  public static final int SIZE_LIQTAI = 11;
  public static final int DECIMAL_LIQTAI = 3;
  public static final int SIZE_LIQTCI = 11;
  public static final int DECIMAL_LIQTCI = 3;
  public static final int SIZE_LIQTSI = 11;
  public static final int DECIMAL_LIQTSI = 3;
  public static final int SIZE_LIMHTI = 11;
  public static final int DECIMAL_LIMHTI = 2;
  public static final int SIZE_LIPRBRU = 9;
  public static final int DECIMAL_LIPRBRU = 2;
  public static final int SIZE_LIPRNET = 9;
  public static final int DECIMAL_LIPRNET = 2;
  public static final int SIZE_LIPRVFR = 9;
  public static final int DECIMAL_LIPRVFR = 2;
  public static final int SIZE_LIPORTF = 9;
  public static final int DECIMAL_LIPORTF = 2;
  public static final int SIZE_PIXXX1 = 7;
  public static final int DECIMAL_PIXXX1 = 4;
  public static final int SIZE_PILFK1 = 7;
  public static final int DECIMAL_PILFK1 = 4;
  public static final int SIZE_PILFV1 = 9;
  public static final int DECIMAL_PILFV1 = 2;
  public static final int SIZE_PILFP1 = 4;
  public static final int DECIMAL_PILFP1 = 2;
  public static final int SIZE_PILFK3 = 7;
  public static final int DECIMAL_PILFK3 = 4;
  public static final int SIZE_PILFV3 = 9;
  public static final int DECIMAL_PILFV3 = 2;
  public static final int SIZE_PITOS = 1;
  public static final int SIZE_PICOS = 1;
  public static final int SIZE_PINOS = 6;
  public static final int DECIMAL_PINOS = 0;
  public static final int SIZE_PISOS = 1;
  public static final int DECIMAL_PISOS = 0;
  public static final int SIZE_PILOS = 4;
  public static final int DECIMAL_PILOS = 0;
  public static final int SIZE_LIARR = 1;
  public static final int SIZE_TOTALE_DS = 561;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND4 = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINLI = 5;
  public static final int VAR_PIDAT = 6;
  public static final int VAR_PIOPT = 7;
  public static final int VAR_PINATA = 8;
  public static final int VAR_L0UNC = 9;
  public static final int VAR_L0UNA = 10;
  public static final int VAR_L0QTA = 11;
  public static final int VAR_L0QTC = 12;
  public static final int VAR_L0QTS = 13;
  public static final int VAR_L0KSC = 14;
  public static final int VAR_L0KAC = 15;
  public static final int VAR_L0PAB = 16;
  public static final int VAR_L0PAN = 17;
  public static final int VAR_L0REM1 = 18;
  public static final int VAR_L0REM2 = 19;
  public static final int VAR_L0REM3 = 20;
  public static final int VAR_L0REM4 = 21;
  public static final int VAR_L0REM5 = 22;
  public static final int VAR_L0REM6 = 23;
  public static final int VAR_L0MHT = 24;
  public static final int VAR_LITOP = 25;
  public static final int VAR_LICOD = 26;
  public static final int VAR_LIETB = 27;
  public static final int VAR_LINUM = 28;
  public static final int VAR_LISUF = 29;
  public static final int VAR_LINLI = 30;
  public static final int VAR_LIERL = 31;
  public static final int VAR_LICEX = 32;
  public static final int VAR_LIQCX = 33;
  public static final int VAR_LITVA = 34;
  public static final int VAR_LITB = 35;
  public static final int VAR_LITR = 36;
  public static final int VAR_LITN = 37;
  public static final int VAR_LITU = 38;
  public static final int VAR_LITQ = 39;
  public static final int VAR_LITH = 40;
  public static final int VAR_LIDCC = 41;
  public static final int VAR_LIDCA = 42;
  public static final int VAR_LIDCS = 43;
  public static final int VAR_LIVAL = 44;
  public static final int VAR_LICOL = 45;
  public static final int VAR_LISGN = 46;
  public static final int VAR_LIQTC = 47;
  public static final int VAR_LIUNC = 48;
  public static final int VAR_LIQTS = 49;
  public static final int VAR_LIKSC = 50;
  public static final int VAR_LIPAB = 51;
  public static final int VAR_LIPAN = 52;
  public static final int VAR_LIPAC = 53;
  public static final int VAR_LIMHT = 54;
  public static final int VAR_LIAVR = 55;
  public static final int VAR_LIQTA = 56;
  public static final int VAR_LIUNA = 57;
  public static final int VAR_LIKAC = 58;
  public static final int VAR_LIART = 59;
  public static final int VAR_LIMAG = 60;
  public static final int VAR_LIMAGA = 61;
  public static final int VAR_LINUMR = 62;
  public static final int VAR_LISUFR = 63;
  public static final int VAR_LINLIR = 64;
  public static final int VAR_LIDATH = 65;
  public static final int VAR_LIORDH = 66;
  public static final int VAR_LISAN = 67;
  public static final int VAR_LIACT = 68;
  public static final int VAR_LINAT = 69;
  public static final int VAR_LIMTA = 70;
  public static final int VAR_LIDLP = 71;
  public static final int VAR_LISER = 72;
  public static final int VAR_LIPAI = 73;
  public static final int VAR_LIPAR = 74;
  public static final int VAR_LIREM1 = 75;
  public static final int VAR_LIREM2 = 76;
  public static final int VAR_LIREM3 = 77;
  public static final int VAR_LIREM4 = 78;
  public static final int VAR_LIREM5 = 79;
  public static final int VAR_LIREM6 = 80;
  public static final int VAR_LITRL = 81;
  public static final int VAR_LIBRL = 82;
  public static final int VAR_LIRP1 = 83;
  public static final int VAR_LIRP2 = 84;
  public static final int VAR_LIRP3 = 85;
  public static final int VAR_LIRP4 = 86;
  public static final int VAR_LIRP5 = 87;
  public static final int VAR_LIRP6 = 88;
  public static final int VAR_LITP1 = 89;
  public static final int VAR_LITP2 = 90;
  public static final int VAR_LITP3 = 91;
  public static final int VAR_LITP4 = 92;
  public static final int VAR_LITP5 = 93;
  public static final int VAR_LIIN1 = 94;
  public static final int VAR_LIIN2 = 95;
  public static final int VAR_LIIN3 = 96;
  public static final int VAR_LIIN4 = 97;
  public static final int VAR_LIIN5 = 98;
  public static final int VAR_LIDLC = 99;
  public static final int VAR_LIQTAI = 100;
  public static final int VAR_LIQTCI = 101;
  public static final int VAR_LIQTSI = 102;
  public static final int VAR_LIMHTI = 103;
  public static final int VAR_LIPRBRU = 104;
  public static final int VAR_LIPRNET = 105;
  public static final int VAR_LIPRVFR = 106;
  public static final int VAR_LIPORTF = 107;
  public static final int VAR_PIXXX1 = 108;
  public static final int VAR_PILFK1 = 109;
  public static final int VAR_PILFV1 = 110;
  public static final int VAR_PILFP1 = 111;
  public static final int VAR_PILFK3 = 112;
  public static final int VAR_PILFV3 = 113;
  public static final int VAR_PITOS = 114;
  public static final int VAR_PICOS = 115;
  public static final int VAR_PINOS = 116;
  public static final int VAR_PISOS = 117;
  public static final int VAR_PILOS = 118;
  public static final int VAR_LIARR = 119;
  
  // Variables AS400
  private String piind4 = ""; // Indicateurs
  private String picod = ""; // Code ERL *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de ligne
  private BigDecimal pidat = BigDecimal.ZERO; // Date de traitement
  private String piopt = ""; // Option de traitement *
  private String pinata = ""; // Nature analytique
  private String l0unc = ""; // Unité de commande
  private String l0una = ""; // Unité d achat
  private BigDecimal l0qta = BigDecimal.ZERO; // Quantité en UA
  private BigDecimal l0qtc = BigDecimal.ZERO; // Quantité en UC
  private BigDecimal l0qts = BigDecimal.ZERO; // Quantité en US
  private BigDecimal l0ksc = BigDecimal.ZERO; // Coeff. Stock/Cde
  private BigDecimal l0kac = BigDecimal.ZERO; // Coeff. Achat/Cde
  private BigDecimal l0pab = BigDecimal.ZERO; // Prix d achat de base
  private BigDecimal l0pan = BigDecimal.ZERO; // Prix d achat net
  private BigDecimal l0rem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal l0rem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal l0rem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal l0rem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal l0rem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal l0rem6 = BigDecimal.ZERO; // % Remise 6
  private BigDecimal l0mht = BigDecimal.ZERO; // Montant HT
  private BigDecimal litop = BigDecimal.ZERO; // Code Etat de la Ligne
  private String licod = ""; // Code ERL "E" OU "F"
  private String lietb = ""; // Code Etablissement
  private BigDecimal linum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal lisuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal linli = BigDecimal.ZERO; // Numéro de Ligne
  private String lierl = ""; // Code ERL "C"
  private String licex = ""; // Code Extraction
  private BigDecimal liqcx = BigDecimal.ZERO; // Quantité extraite en UV
  private BigDecimal litva = BigDecimal.ZERO; // Code TVA
  private BigDecimal litb = BigDecimal.ZERO; // Top prix de base saisi
  private BigDecimal litr = BigDecimal.ZERO; // Top remises saisies
  private BigDecimal litn = BigDecimal.ZERO; // Top prix net saisi
  private BigDecimal litu = BigDecimal.ZERO; // Top unité saisie
  private BigDecimal litq = BigDecimal.ZERO; // Top Qté saisie
  private BigDecimal lith = BigDecimal.ZERO; // Top montant H.T saisi
  private BigDecimal lidcc = BigDecimal.ZERO; // Décimalisation QTC
  private BigDecimal lidca = BigDecimal.ZERO; // " " QTA
  private BigDecimal lidcs = BigDecimal.ZERO; // " " QTS
  private BigDecimal lival = BigDecimal.ZERO; // Top Ligne en Valeur
  private BigDecimal licol = BigDecimal.ZERO; // Colonne TVA
  private BigDecimal lisgn = BigDecimal.ZERO; // Signe
  private BigDecimal liqtc = BigDecimal.ZERO; // Quantité en unités de cde
  private String liunc = ""; // Unité de commande
  private BigDecimal liqts = BigDecimal.ZERO; // Quantité en unités de stock
  private BigDecimal liksc = BigDecimal.ZERO; // Coeff. Stock/Cde
  private BigDecimal lipab = BigDecimal.ZERO; // Prix d"achat de base
  private BigDecimal lipan = BigDecimal.ZERO; // Prix d"achat net
  private BigDecimal lipac = BigDecimal.ZERO; // Prix d"achat calculé
  private BigDecimal limht = BigDecimal.ZERO; // Montant hors taxes
  private String liavr = ""; // Code Avoir
  private BigDecimal liqta = BigDecimal.ZERO; // Quantité en unités d"achat
  private String liuna = ""; // Unité d"achat
  private BigDecimal likac = BigDecimal.ZERO; // Coeff. Achat/Cde
  private String liart = ""; // Code Article
  private String limag = ""; // Magasin
  private String limaga = ""; // Magasin avant modif
  private BigDecimal linumr = BigDecimal.ZERO; // N°Bon ou Fac.
  private BigDecimal lisufr = BigDecimal.ZERO; // Suffixe
  private BigDecimal linlir = BigDecimal.ZERO; // N°ligne
  private BigDecimal lidath = BigDecimal.ZERO; // Date sur Histo stocks
  private BigDecimal liordh = BigDecimal.ZERO; // Ordre sur Histo stocks
  private String lisan = ""; // Section Analytique
  private String liact = ""; // Activité ou Affaire
  private String linat = ""; // Nature d"achat (M,F,I)
  private BigDecimal limta = BigDecimal.ZERO; // Montant affecté
  private BigDecimal lidlp = BigDecimal.ZERO; // Date livraison prévue
  private BigDecimal liser = BigDecimal.ZERO; // Top n° de série ou lot
  private BigDecimal lipai = BigDecimal.ZERO; // Prix d'achat initial
  private BigDecimal lipar = BigDecimal.ZERO; // Prix d'achat rem. déduites
  private BigDecimal lirem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal lirem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal lirem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal lirem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal lirem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal lirem6 = BigDecimal.ZERO; // % Remise 6
  private String litrl = ""; // Type remise ligne, 1=cascade
  private String librl = ""; // Base remise ligne, 1=Montant
  private String lirp1 = ""; // Exclusion remise de pied N°1
  private String lirp2 = ""; // Exclusion remise de pied N°2
  private String lirp3 = ""; // Exclusion remise de pied N°3
  private String lirp4 = ""; // Exclusion remise de pied N°4
  private String lirp5 = ""; // Exclusion remise de pied N°5
  private String lirp6 = ""; // Exclusion remise de pied N°6
  private String litp1 = ""; // Top personnal.N°1
  private String litp2 = ""; // Top personnal.N°2
  private String litp3 = ""; // Top personnal.N°3
  private String litp4 = ""; // Top personnal.N°4
  private String litp5 = ""; // Top personnal.N°5
  private String liin1 = ""; // Imputation frais
  private String liin2 = ""; // type de frais
  private String liin3 = ""; // regroupement article
  private String liin4 = ""; // compt. stock flottant
  private String liin5 = ""; // Condit° en nb de gratuits
  private BigDecimal lidlc = BigDecimal.ZERO; // Date livraison calculée
  private BigDecimal liqtai = BigDecimal.ZERO; // Qte initiale unités d"achat
  private BigDecimal liqtci = BigDecimal.ZERO; // Qte initiale unités de cde
  private BigDecimal liqtsi = BigDecimal.ZERO; // Qte initiale unités de stock
  private BigDecimal limhti = BigDecimal.ZERO; // Montant initial hors taxe
  private BigDecimal liprbru = BigDecimal.ZERO; // Prix d achat brut
  private BigDecimal liprnet = BigDecimal.ZERO; // Prix d achat net sans port
  private BigDecimal liprvfr = BigDecimal.ZERO; // Prix revient fournisseur
  private BigDecimal liportf = BigDecimal.ZERO; // Port fournissseur eur
  private BigDecimal pixxx1 = BigDecimal.ZERO; // Plus utilisé (anc.PiLFK1)
  private BigDecimal pilfk1 = BigDecimal.ZERO; // Frais 1 coef.(anc.PiLFK1p)
  private BigDecimal pilfv1 = BigDecimal.ZERO; // Frais 1 valeur
  private BigDecimal pilfp1 = BigDecimal.ZERO; // Frais 1 % PORT
  private BigDecimal pilfk3 = BigDecimal.ZERO; // Frais 3 coefficient
  private BigDecimal pilfv3 = BigDecimal.ZERO; // Frais 3 valeur
  private String pitos = ""; // Type origine sortie
  private String picos = ""; // Code origine sortie
  private BigDecimal pinos = BigDecimal.ZERO; // N°Bon origine sortie
  private BigDecimal pisos = BigDecimal.ZERO; // N°Suf.origine sortie
  private BigDecimal pilos = BigDecimal.ZERO; // N°Lig.origine sortie
  private String liarr = ""; // FIN
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND4), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de ligne
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date de traitement
      new AS400Text(SIZE_PIOPT), // Option de traitement *
      new AS400Text(SIZE_PINATA), // Nature analytique
      new AS400Text(SIZE_L0UNC), // Unité de commande
      new AS400Text(SIZE_L0UNA), // Unité d achat
      new AS400ZonedDecimal(SIZE_L0QTA, DECIMAL_L0QTA), // Quantité en UA
      new AS400ZonedDecimal(SIZE_L0QTC, DECIMAL_L0QTC), // Quantité en UC
      new AS400ZonedDecimal(SIZE_L0QTS, DECIMAL_L0QTS), // Quantité en US
      new AS400ZonedDecimal(SIZE_L0KSC, DECIMAL_L0KSC), // Coeff. Stock/Cde
      new AS400ZonedDecimal(SIZE_L0KAC, DECIMAL_L0KAC), // Coeff. Achat/Cde
      new AS400ZonedDecimal(SIZE_L0PAB, DECIMAL_L0PAB), // Prix d achat de base
      new AS400ZonedDecimal(SIZE_L0PAN, DECIMAL_L0PAN), // Prix d achat net
      new AS400ZonedDecimal(SIZE_L0REM1, DECIMAL_L0REM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_L0REM2, DECIMAL_L0REM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_L0REM3, DECIMAL_L0REM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_L0REM4, DECIMAL_L0REM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_L0REM5, DECIMAL_L0REM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_L0REM6, DECIMAL_L0REM6), // % Remise 6
      new AS400ZonedDecimal(SIZE_L0MHT, DECIMAL_L0MHT), // Montant HT
      new AS400ZonedDecimal(SIZE_LITOP, DECIMAL_LITOP), // Code Etat de la Ligne
      new AS400Text(SIZE_LICOD), // Code ERL "E" OU "F"
      new AS400Text(SIZE_LIETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_LINUM, DECIMAL_LINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_LISUF, DECIMAL_LISUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_LINLI, DECIMAL_LINLI), // Numéro de Ligne
      new AS400Text(SIZE_LIERL), // Code ERL "C"
      new AS400Text(SIZE_LICEX), // Code Extraction
      new AS400PackedDecimal(SIZE_LIQCX, DECIMAL_LIQCX), // Quantité extraite en UV
      new AS400ZonedDecimal(SIZE_LITVA, DECIMAL_LITVA), // Code TVA
      new AS400ZonedDecimal(SIZE_LITB, DECIMAL_LITB), // Top prix de base saisi
      new AS400ZonedDecimal(SIZE_LITR, DECIMAL_LITR), // Top remises saisies
      new AS400ZonedDecimal(SIZE_LITN, DECIMAL_LITN), // Top prix net saisi
      new AS400ZonedDecimal(SIZE_LITU, DECIMAL_LITU), // Top unité saisie
      new AS400ZonedDecimal(SIZE_LITQ, DECIMAL_LITQ), // Top Qté saisie
      new AS400ZonedDecimal(SIZE_LITH, DECIMAL_LITH), // Top montant H.T saisi
      new AS400ZonedDecimal(SIZE_LIDCC, DECIMAL_LIDCC), // Décimalisation QTC
      new AS400ZonedDecimal(SIZE_LIDCA, DECIMAL_LIDCA), // " " QTA
      new AS400ZonedDecimal(SIZE_LIDCS, DECIMAL_LIDCS), // " " QTS
      new AS400ZonedDecimal(SIZE_LIVAL, DECIMAL_LIVAL), // Top Ligne en Valeur
      new AS400ZonedDecimal(SIZE_LICOL, DECIMAL_LICOL), // Colonne TVA
      new AS400ZonedDecimal(SIZE_LISGN, DECIMAL_LISGN), // Signe
      new AS400PackedDecimal(SIZE_LIQTC, DECIMAL_LIQTC), // Quantité en unités de cde
      new AS400Text(SIZE_LIUNC), // Unité de commande
      new AS400PackedDecimal(SIZE_LIQTS, DECIMAL_LIQTS), // Quantité en unités de stock
      new AS400PackedDecimal(SIZE_LIKSC, DECIMAL_LIKSC), // Coeff. Stock/Cde
      new AS400PackedDecimal(SIZE_LIPAB, DECIMAL_LIPAB), // Prix d"achat de base
      new AS400PackedDecimal(SIZE_LIPAN, DECIMAL_LIPAN), // Prix d"achat net
      new AS400PackedDecimal(SIZE_LIPAC, DECIMAL_LIPAC), // Prix d"achat calculé
      new AS400PackedDecimal(SIZE_LIMHT, DECIMAL_LIMHT), // Montant hors taxes
      new AS400Text(SIZE_LIAVR), // Code Avoir
      new AS400PackedDecimal(SIZE_LIQTA, DECIMAL_LIQTA), // Quantité en unités d"achat
      new AS400Text(SIZE_LIUNA), // Unité d"achat
      new AS400PackedDecimal(SIZE_LIKAC, DECIMAL_LIKAC), // Coeff. Achat/Cde
      new AS400Text(SIZE_LIART), // Code Article
      new AS400Text(SIZE_LIMAG), // Magasin
      new AS400Text(SIZE_LIMAGA), // Magasin avant modif
      new AS400ZonedDecimal(SIZE_LINUMR, DECIMAL_LINUMR), // N°Bon ou Fac.
      new AS400ZonedDecimal(SIZE_LISUFR, DECIMAL_LISUFR), // Suffixe
      new AS400ZonedDecimal(SIZE_LINLIR, DECIMAL_LINLIR), // N°ligne
      new AS400ZonedDecimal(SIZE_LIDATH, DECIMAL_LIDATH), // Date sur Histo stocks
      new AS400ZonedDecimal(SIZE_LIORDH, DECIMAL_LIORDH), // Ordre sur Histo stocks
      new AS400Text(SIZE_LISAN), // Section Analytique
      new AS400Text(SIZE_LIACT), // Activité ou Affaire
      new AS400Text(SIZE_LINAT), // Nature d"achat (M,F,I)
      new AS400PackedDecimal(SIZE_LIMTA, DECIMAL_LIMTA), // Montant affecté
      new AS400PackedDecimal(SIZE_LIDLP, DECIMAL_LIDLP), // Date livraison prévue
      new AS400ZonedDecimal(SIZE_LISER, DECIMAL_LISER), // Top n° de série ou lot
      new AS400PackedDecimal(SIZE_LIPAI, DECIMAL_LIPAI), // Prix d'achat initial
      new AS400PackedDecimal(SIZE_LIPAR, DECIMAL_LIPAR), // Prix d'achat rem. déduites
      new AS400ZonedDecimal(SIZE_LIREM1, DECIMAL_LIREM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_LIREM2, DECIMAL_LIREM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_LIREM3, DECIMAL_LIREM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_LIREM4, DECIMAL_LIREM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_LIREM5, DECIMAL_LIREM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_LIREM6, DECIMAL_LIREM6), // % Remise 6
      new AS400Text(SIZE_LITRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_LIBRL), // Base remise ligne, 1=Montant
      new AS400Text(SIZE_LIRP1), // Exclusion remise de pied N°1
      new AS400Text(SIZE_LIRP2), // Exclusion remise de pied N°2
      new AS400Text(SIZE_LIRP3), // Exclusion remise de pied N°3
      new AS400Text(SIZE_LIRP4), // Exclusion remise de pied N°4
      new AS400Text(SIZE_LIRP5), // Exclusion remise de pied N°5
      new AS400Text(SIZE_LIRP6), // Exclusion remise de pied N°6
      new AS400Text(SIZE_LITP1), // Top personnal.N°1
      new AS400Text(SIZE_LITP2), // Top personnal.N°2
      new AS400Text(SIZE_LITP3), // Top personnal.N°3
      new AS400Text(SIZE_LITP4), // Top personnal.N°4
      new AS400Text(SIZE_LITP5), // Top personnal.N°5
      new AS400Text(SIZE_LIIN1), // Imputation frais
      new AS400Text(SIZE_LIIN2), // type de frais
      new AS400Text(SIZE_LIIN3), // regroupement article
      new AS400Text(SIZE_LIIN4), // compt. stock flottant
      new AS400Text(SIZE_LIIN5), // Condit° en nb de gratuits
      new AS400PackedDecimal(SIZE_LIDLC, DECIMAL_LIDLC), // Date livraison calculée
      new AS400PackedDecimal(SIZE_LIQTAI, DECIMAL_LIQTAI), // Qte initiale unités d"achat
      new AS400PackedDecimal(SIZE_LIQTCI, DECIMAL_LIQTCI), // Qte initiale unités de cde
      new AS400PackedDecimal(SIZE_LIQTSI, DECIMAL_LIQTSI), // Qte initiale unités de stock
      new AS400PackedDecimal(SIZE_LIMHTI, DECIMAL_LIMHTI), // Montant initial hors taxe
      new AS400ZonedDecimal(SIZE_LIPRBRU, DECIMAL_LIPRBRU), // Prix d achat brut
      new AS400ZonedDecimal(SIZE_LIPRNET, DECIMAL_LIPRNET), // Prix d achat net sans port
      new AS400ZonedDecimal(SIZE_LIPRVFR, DECIMAL_LIPRVFR), // Prix revient fournisseur
      new AS400ZonedDecimal(SIZE_LIPORTF, DECIMAL_LIPORTF), // Port fournissseur eur
      new AS400ZonedDecimal(SIZE_PIXXX1, DECIMAL_PIXXX1), // Plus utilisé (anc.PiLFK1)
      new AS400ZonedDecimal(SIZE_PILFK1, DECIMAL_PILFK1), // Frais 1 coef.(anc.PiLFK1p)
      new AS400ZonedDecimal(SIZE_PILFV1, DECIMAL_PILFV1), // Frais 1 valeur
      new AS400ZonedDecimal(SIZE_PILFP1, DECIMAL_PILFP1), // Frais 1 % PORT
      new AS400ZonedDecimal(SIZE_PILFK3, DECIMAL_PILFK3), // Frais 3 coefficient
      new AS400ZonedDecimal(SIZE_PILFV3, DECIMAL_PILFV3), // Frais 3 valeur
      new AS400Text(SIZE_PITOS), // Type origine sortie
      new AS400Text(SIZE_PICOS), // Code origine sortie
      new AS400ZonedDecimal(SIZE_PINOS, DECIMAL_PINOS), // N°Bon origine sortie
      new AS400ZonedDecimal(SIZE_PISOS, DECIMAL_PISOS), // N°Suf.origine sortie
      new AS400ZonedDecimal(SIZE_PILOS, DECIMAL_PILOS), // N°Lig.origine sortie
      new AS400Text(SIZE_LIARR), // FIN
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind4, picod, pietb, pinum, pisuf, pinli, pidat, piopt, pinata, l0unc, l0una, l0qta, l0qtc, l0qts, l0ksc, l0kac,
          l0pab, l0pan, l0rem1, l0rem2, l0rem3, l0rem4, l0rem5, l0rem6, l0mht, litop, licod, lietb, linum, lisuf, linli, lierl, licex,
          liqcx, litva, litb, litr, litn, litu, litq, lith, lidcc, lidca, lidcs, lival, licol, lisgn, liqtc, liunc, liqts, liksc, lipab,
          lipan, lipac, limht, liavr, liqta, liuna, likac, liart, limag, limaga, linumr, lisufr, linlir, lidath, liordh, lisan, liact,
          linat, limta, lidlp, liser, lipai, lipar, lirem1, lirem2, lirem3, lirem4, lirem5, lirem6, litrl, librl, lirp1, lirp2, lirp3,
          lirp4, lirp5, lirp6, litp1, litp2, litp3, litp4, litp5, liin1, liin2, liin3, liin4, liin5, lidlc, liqtai, liqtci, liqtsi,
          limhti, liprbru, liprnet, liprvfr, liportf, pixxx1, pilfk1, pilfv1, pilfp1, pilfk3, pilfv3, pitos, picos, pinos, pisos, pilos,
          liarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind4 = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinli = (BigDecimal) output[5];
    pidat = (BigDecimal) output[6];
    piopt = (String) output[7];
    pinata = (String) output[8];
    l0unc = (String) output[9];
    l0una = (String) output[10];
    l0qta = (BigDecimal) output[11];
    l0qtc = (BigDecimal) output[12];
    l0qts = (BigDecimal) output[13];
    l0ksc = (BigDecimal) output[14];
    l0kac = (BigDecimal) output[15];
    l0pab = (BigDecimal) output[16];
    l0pan = (BigDecimal) output[17];
    l0rem1 = (BigDecimal) output[18];
    l0rem2 = (BigDecimal) output[19];
    l0rem3 = (BigDecimal) output[20];
    l0rem4 = (BigDecimal) output[21];
    l0rem5 = (BigDecimal) output[22];
    l0rem6 = (BigDecimal) output[23];
    l0mht = (BigDecimal) output[24];
    litop = (BigDecimal) output[25];
    licod = (String) output[26];
    lietb = (String) output[27];
    linum = (BigDecimal) output[28];
    lisuf = (BigDecimal) output[29];
    linli = (BigDecimal) output[30];
    lierl = (String) output[31];
    licex = (String) output[32];
    liqcx = (BigDecimal) output[33];
    litva = (BigDecimal) output[34];
    litb = (BigDecimal) output[35];
    litr = (BigDecimal) output[36];
    litn = (BigDecimal) output[37];
    litu = (BigDecimal) output[38];
    litq = (BigDecimal) output[39];
    lith = (BigDecimal) output[40];
    lidcc = (BigDecimal) output[41];
    lidca = (BigDecimal) output[42];
    lidcs = (BigDecimal) output[43];
    lival = (BigDecimal) output[44];
    licol = (BigDecimal) output[45];
    lisgn = (BigDecimal) output[46];
    liqtc = (BigDecimal) output[47];
    liunc = (String) output[48];
    liqts = (BigDecimal) output[49];
    liksc = (BigDecimal) output[50];
    lipab = (BigDecimal) output[51];
    lipan = (BigDecimal) output[52];
    lipac = (BigDecimal) output[53];
    limht = (BigDecimal) output[54];
    liavr = (String) output[55];
    liqta = (BigDecimal) output[56];
    liuna = (String) output[57];
    likac = (BigDecimal) output[58];
    liart = (String) output[59];
    limag = (String) output[60];
    limaga = (String) output[61];
    linumr = (BigDecimal) output[62];
    lisufr = (BigDecimal) output[63];
    linlir = (BigDecimal) output[64];
    lidath = (BigDecimal) output[65];
    liordh = (BigDecimal) output[66];
    lisan = (String) output[67];
    liact = (String) output[68];
    linat = (String) output[69];
    limta = (BigDecimal) output[70];
    lidlp = (BigDecimal) output[71];
    liser = (BigDecimal) output[72];
    lipai = (BigDecimal) output[73];
    lipar = (BigDecimal) output[74];
    lirem1 = (BigDecimal) output[75];
    lirem2 = (BigDecimal) output[76];
    lirem3 = (BigDecimal) output[77];
    lirem4 = (BigDecimal) output[78];
    lirem5 = (BigDecimal) output[79];
    lirem6 = (BigDecimal) output[80];
    litrl = (String) output[81];
    librl = (String) output[82];
    lirp1 = (String) output[83];
    lirp2 = (String) output[84];
    lirp3 = (String) output[85];
    lirp4 = (String) output[86];
    lirp5 = (String) output[87];
    lirp6 = (String) output[88];
    litp1 = (String) output[89];
    litp2 = (String) output[90];
    litp3 = (String) output[91];
    litp4 = (String) output[92];
    litp5 = (String) output[93];
    liin1 = (String) output[94];
    liin2 = (String) output[95];
    liin3 = (String) output[96];
    liin4 = (String) output[97];
    liin5 = (String) output[98];
    lidlc = (BigDecimal) output[99];
    liqtai = (BigDecimal) output[100];
    liqtci = (BigDecimal) output[101];
    liqtsi = (BigDecimal) output[102];
    limhti = (BigDecimal) output[103];
    liprbru = (BigDecimal) output[104];
    liprnet = (BigDecimal) output[105];
    liprvfr = (BigDecimal) output[106];
    liportf = (BigDecimal) output[107];
    pixxx1 = (BigDecimal) output[108];
    pilfk1 = (BigDecimal) output[109];
    pilfv1 = (BigDecimal) output[110];
    pilfp1 = (BigDecimal) output[111];
    pilfk3 = (BigDecimal) output[112];
    pilfv3 = (BigDecimal) output[113];
    pitos = (String) output[114];
    picos = (String) output[115];
    pinos = (BigDecimal) output[116];
    pisos = (BigDecimal) output[117];
    pilos = (BigDecimal) output[118];
    liarr = (String) output[119];
  }
  
  // -- Accesseurs
  
  public void setPiind4(String pPiind4) {
    if (pPiind4 == null) {
      return;
    }
    piind4 = pPiind4;
  }
  
  public String getPiind4() {
    return piind4;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPiopt(String pPiopt) {
    if (pPiopt == null) {
      return;
    }
    piopt = pPiopt;
  }
  
  public String getPiopt() {
    return piopt;
  }
  
  public void setPinata(String pPinata) {
    if (pPinata == null) {
      return;
    }
    pinata = pPinata;
  }
  
  public String getPinata() {
    return pinata;
  }
  
  public void setL0unc(String pL0unc) {
    if (pL0unc == null) {
      return;
    }
    l0unc = pL0unc;
  }
  
  public String getL0unc() {
    return l0unc;
  }
  
  public void setL0una(String pL0una) {
    if (pL0una == null) {
      return;
    }
    l0una = pL0una;
  }
  
  public String getL0una() {
    return l0una;
  }
  
  public void setL0qta(BigDecimal pL0qta) {
    if (pL0qta == null) {
      return;
    }
    l0qta = pL0qta.setScale(DECIMAL_L0QTA, RoundingMode.HALF_UP);
  }
  
  public void setL0qta(Double pL0qta) {
    if (pL0qta == null) {
      return;
    }
    l0qta = BigDecimal.valueOf(pL0qta).setScale(DECIMAL_L0QTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0qta() {
    return l0qta.setScale(DECIMAL_L0QTA, RoundingMode.HALF_UP);
  }
  
  public void setL0qtc(BigDecimal pL0qtc) {
    if (pL0qtc == null) {
      return;
    }
    l0qtc = pL0qtc.setScale(DECIMAL_L0QTC, RoundingMode.HALF_UP);
  }
  
  public void setL0qtc(Double pL0qtc) {
    if (pL0qtc == null) {
      return;
    }
    l0qtc = BigDecimal.valueOf(pL0qtc).setScale(DECIMAL_L0QTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0qtc() {
    return l0qtc.setScale(DECIMAL_L0QTC, RoundingMode.HALF_UP);
  }
  
  public void setL0qts(BigDecimal pL0qts) {
    if (pL0qts == null) {
      return;
    }
    l0qts = pL0qts.setScale(DECIMAL_L0QTS, RoundingMode.HALF_UP);
  }
  
  public void setL0qts(Double pL0qts) {
    if (pL0qts == null) {
      return;
    }
    l0qts = BigDecimal.valueOf(pL0qts).setScale(DECIMAL_L0QTS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0qts() {
    return l0qts.setScale(DECIMAL_L0QTS, RoundingMode.HALF_UP);
  }
  
  public void setL0ksc(BigDecimal pL0ksc) {
    if (pL0ksc == null) {
      return;
    }
    l0ksc = pL0ksc.setScale(DECIMAL_L0KSC, RoundingMode.HALF_UP);
  }
  
  public void setL0ksc(Double pL0ksc) {
    if (pL0ksc == null) {
      return;
    }
    l0ksc = BigDecimal.valueOf(pL0ksc).setScale(DECIMAL_L0KSC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0ksc() {
    return l0ksc.setScale(DECIMAL_L0KSC, RoundingMode.HALF_UP);
  }
  
  public void setL0kac(BigDecimal pL0kac) {
    if (pL0kac == null) {
      return;
    }
    l0kac = pL0kac.setScale(DECIMAL_L0KAC, RoundingMode.HALF_UP);
  }
  
  public void setL0kac(Double pL0kac) {
    if (pL0kac == null) {
      return;
    }
    l0kac = BigDecimal.valueOf(pL0kac).setScale(DECIMAL_L0KAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0kac() {
    return l0kac.setScale(DECIMAL_L0KAC, RoundingMode.HALF_UP);
  }
  
  public void setL0pab(BigDecimal pL0pab) {
    if (pL0pab == null) {
      return;
    }
    l0pab = pL0pab.setScale(DECIMAL_L0PAB, RoundingMode.HALF_UP);
  }
  
  public void setL0pab(Double pL0pab) {
    if (pL0pab == null) {
      return;
    }
    l0pab = BigDecimal.valueOf(pL0pab).setScale(DECIMAL_L0PAB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0pab() {
    return l0pab.setScale(DECIMAL_L0PAB, RoundingMode.HALF_UP);
  }
  
  public void setL0pan(BigDecimal pL0pan) {
    if (pL0pan == null) {
      return;
    }
    l0pan = pL0pan.setScale(DECIMAL_L0PAN, RoundingMode.HALF_UP);
  }
  
  public void setL0pan(Double pL0pan) {
    if (pL0pan == null) {
      return;
    }
    l0pan = BigDecimal.valueOf(pL0pan).setScale(DECIMAL_L0PAN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0pan() {
    return l0pan.setScale(DECIMAL_L0PAN, RoundingMode.HALF_UP);
  }
  
  public void setL0rem1(BigDecimal pL0rem1) {
    if (pL0rem1 == null) {
      return;
    }
    l0rem1 = pL0rem1.setScale(DECIMAL_L0REM1, RoundingMode.HALF_UP);
  }
  
  public void setL0rem1(Double pL0rem1) {
    if (pL0rem1 == null) {
      return;
    }
    l0rem1 = BigDecimal.valueOf(pL0rem1).setScale(DECIMAL_L0REM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem1() {
    return l0rem1.setScale(DECIMAL_L0REM1, RoundingMode.HALF_UP);
  }
  
  public void setL0rem2(BigDecimal pL0rem2) {
    if (pL0rem2 == null) {
      return;
    }
    l0rem2 = pL0rem2.setScale(DECIMAL_L0REM2, RoundingMode.HALF_UP);
  }
  
  public void setL0rem2(Double pL0rem2) {
    if (pL0rem2 == null) {
      return;
    }
    l0rem2 = BigDecimal.valueOf(pL0rem2).setScale(DECIMAL_L0REM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem2() {
    return l0rem2.setScale(DECIMAL_L0REM2, RoundingMode.HALF_UP);
  }
  
  public void setL0rem3(BigDecimal pL0rem3) {
    if (pL0rem3 == null) {
      return;
    }
    l0rem3 = pL0rem3.setScale(DECIMAL_L0REM3, RoundingMode.HALF_UP);
  }
  
  public void setL0rem3(Double pL0rem3) {
    if (pL0rem3 == null) {
      return;
    }
    l0rem3 = BigDecimal.valueOf(pL0rem3).setScale(DECIMAL_L0REM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem3() {
    return l0rem3.setScale(DECIMAL_L0REM3, RoundingMode.HALF_UP);
  }
  
  public void setL0rem4(BigDecimal pL0rem4) {
    if (pL0rem4 == null) {
      return;
    }
    l0rem4 = pL0rem4.setScale(DECIMAL_L0REM4, RoundingMode.HALF_UP);
  }
  
  public void setL0rem4(Double pL0rem4) {
    if (pL0rem4 == null) {
      return;
    }
    l0rem4 = BigDecimal.valueOf(pL0rem4).setScale(DECIMAL_L0REM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem4() {
    return l0rem4.setScale(DECIMAL_L0REM4, RoundingMode.HALF_UP);
  }
  
  public void setL0rem5(BigDecimal pL0rem5) {
    if (pL0rem5 == null) {
      return;
    }
    l0rem5 = pL0rem5.setScale(DECIMAL_L0REM5, RoundingMode.HALF_UP);
  }
  
  public void setL0rem5(Double pL0rem5) {
    if (pL0rem5 == null) {
      return;
    }
    l0rem5 = BigDecimal.valueOf(pL0rem5).setScale(DECIMAL_L0REM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem5() {
    return l0rem5.setScale(DECIMAL_L0REM5, RoundingMode.HALF_UP);
  }
  
  public void setL0rem6(BigDecimal pL0rem6) {
    if (pL0rem6 == null) {
      return;
    }
    l0rem6 = pL0rem6.setScale(DECIMAL_L0REM6, RoundingMode.HALF_UP);
  }
  
  public void setL0rem6(Double pL0rem6) {
    if (pL0rem6 == null) {
      return;
    }
    l0rem6 = BigDecimal.valueOf(pL0rem6).setScale(DECIMAL_L0REM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem6() {
    return l0rem6.setScale(DECIMAL_L0REM6, RoundingMode.HALF_UP);
  }
  
  public void setL0mht(BigDecimal pL0mht) {
    if (pL0mht == null) {
      return;
    }
    l0mht = pL0mht.setScale(DECIMAL_L0MHT, RoundingMode.HALF_UP);
  }
  
  public void setL0mht(Double pL0mht) {
    if (pL0mht == null) {
      return;
    }
    l0mht = BigDecimal.valueOf(pL0mht).setScale(DECIMAL_L0MHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0mht() {
    return l0mht.setScale(DECIMAL_L0MHT, RoundingMode.HALF_UP);
  }
  
  public void setLitop(BigDecimal pLitop) {
    if (pLitop == null) {
      return;
    }
    litop = pLitop.setScale(DECIMAL_LITOP, RoundingMode.HALF_UP);
  }
  
  public void setLitop(Integer pLitop) {
    if (pLitop == null) {
      return;
    }
    litop = BigDecimal.valueOf(pLitop);
  }
  
  public Integer getLitop() {
    return litop.intValue();
  }
  
  public void setLicod(Character pLicod) {
    if (pLicod == null) {
      return;
    }
    licod = String.valueOf(pLicod);
  }
  
  public Character getLicod() {
    return licod.charAt(0);
  }
  
  public void setLietb(String pLietb) {
    if (pLietb == null) {
      return;
    }
    lietb = pLietb;
  }
  
  public String getLietb() {
    return lietb;
  }
  
  public void setLinum(BigDecimal pLinum) {
    if (pLinum == null) {
      return;
    }
    linum = pLinum.setScale(DECIMAL_LINUM, RoundingMode.HALF_UP);
  }
  
  public void setLinum(Integer pLinum) {
    if (pLinum == null) {
      return;
    }
    linum = BigDecimal.valueOf(pLinum);
  }
  
  public Integer getLinum() {
    return linum.intValue();
  }
  
  public void setLisuf(BigDecimal pLisuf) {
    if (pLisuf == null) {
      return;
    }
    lisuf = pLisuf.setScale(DECIMAL_LISUF, RoundingMode.HALF_UP);
  }
  
  public void setLisuf(Integer pLisuf) {
    if (pLisuf == null) {
      return;
    }
    lisuf = BigDecimal.valueOf(pLisuf);
  }
  
  public Integer getLisuf() {
    return lisuf.intValue();
  }
  
  public void setLinli(BigDecimal pLinli) {
    if (pLinli == null) {
      return;
    }
    linli = pLinli.setScale(DECIMAL_LINLI, RoundingMode.HALF_UP);
  }
  
  public void setLinli(Integer pLinli) {
    if (pLinli == null) {
      return;
    }
    linli = BigDecimal.valueOf(pLinli);
  }
  
  public Integer getLinli() {
    return linli.intValue();
  }
  
  public void setLierl(Character pLierl) {
    if (pLierl == null) {
      return;
    }
    lierl = String.valueOf(pLierl);
  }
  
  public Character getLierl() {
    return lierl.charAt(0);
  }
  
  public void setLicex(Character pLicex) {
    if (pLicex == null) {
      return;
    }
    licex = String.valueOf(pLicex);
  }
  
  public Character getLicex() {
    return licex.charAt(0);
  }
  
  public void setLiqcx(BigDecimal pLiqcx) {
    if (pLiqcx == null) {
      return;
    }
    liqcx = pLiqcx.setScale(DECIMAL_LIQCX, RoundingMode.HALF_UP);
  }
  
  public void setLiqcx(Double pLiqcx) {
    if (pLiqcx == null) {
      return;
    }
    liqcx = BigDecimal.valueOf(pLiqcx).setScale(DECIMAL_LIQCX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqcx() {
    return liqcx.setScale(DECIMAL_LIQCX, RoundingMode.HALF_UP);
  }
  
  public void setLitva(BigDecimal pLitva) {
    if (pLitva == null) {
      return;
    }
    litva = pLitva.setScale(DECIMAL_LITVA, RoundingMode.HALF_UP);
  }
  
  public void setLitva(Integer pLitva) {
    if (pLitva == null) {
      return;
    }
    litva = BigDecimal.valueOf(pLitva);
  }
  
  public Integer getLitva() {
    return litva.intValue();
  }
  
  public void setLitb(BigDecimal pLitb) {
    if (pLitb == null) {
      return;
    }
    litb = pLitb.setScale(DECIMAL_LITB, RoundingMode.HALF_UP);
  }
  
  public void setLitb(Integer pLitb) {
    if (pLitb == null) {
      return;
    }
    litb = BigDecimal.valueOf(pLitb);
  }
  
  public Integer getLitb() {
    return litb.intValue();
  }
  
  public void setLitr(BigDecimal pLitr) {
    if (pLitr == null) {
      return;
    }
    litr = pLitr.setScale(DECIMAL_LITR, RoundingMode.HALF_UP);
  }
  
  public void setLitr(Integer pLitr) {
    if (pLitr == null) {
      return;
    }
    litr = BigDecimal.valueOf(pLitr);
  }
  
  public Integer getLitr() {
    return litr.intValue();
  }
  
  public void setLitn(BigDecimal pLitn) {
    if (pLitn == null) {
      return;
    }
    litn = pLitn.setScale(DECIMAL_LITN, RoundingMode.HALF_UP);
  }
  
  public void setLitn(Integer pLitn) {
    if (pLitn == null) {
      return;
    }
    litn = BigDecimal.valueOf(pLitn);
  }
  
  public Integer getLitn() {
    return litn.intValue();
  }
  
  public void setLitu(BigDecimal pLitu) {
    if (pLitu == null) {
      return;
    }
    litu = pLitu.setScale(DECIMAL_LITU, RoundingMode.HALF_UP);
  }
  
  public void setLitu(Integer pLitu) {
    if (pLitu == null) {
      return;
    }
    litu = BigDecimal.valueOf(pLitu);
  }
  
  public Integer getLitu() {
    return litu.intValue();
  }
  
  public void setLitq(BigDecimal pLitq) {
    if (pLitq == null) {
      return;
    }
    litq = pLitq.setScale(DECIMAL_LITQ, RoundingMode.HALF_UP);
  }
  
  public void setLitq(Integer pLitq) {
    if (pLitq == null) {
      return;
    }
    litq = BigDecimal.valueOf(pLitq);
  }
  
  public Integer getLitq() {
    return litq.intValue();
  }
  
  public void setLith(BigDecimal pLith) {
    if (pLith == null) {
      return;
    }
    lith = pLith.setScale(DECIMAL_LITH, RoundingMode.HALF_UP);
  }
  
  public void setLith(Integer pLith) {
    if (pLith == null) {
      return;
    }
    lith = BigDecimal.valueOf(pLith);
  }
  
  public Integer getLith() {
    return lith.intValue();
  }
  
  public void setLidcc(BigDecimal pLidcc) {
    if (pLidcc == null) {
      return;
    }
    lidcc = pLidcc.setScale(DECIMAL_LIDCC, RoundingMode.HALF_UP);
  }
  
  public void setLidcc(Integer pLidcc) {
    if (pLidcc == null) {
      return;
    }
    lidcc = BigDecimal.valueOf(pLidcc);
  }
  
  public Integer getLidcc() {
    return lidcc.intValue();
  }
  
  public void setLidca(BigDecimal pLidca) {
    if (pLidca == null) {
      return;
    }
    lidca = pLidca.setScale(DECIMAL_LIDCA, RoundingMode.HALF_UP);
  }
  
  public void setLidca(Integer pLidca) {
    if (pLidca == null) {
      return;
    }
    lidca = BigDecimal.valueOf(pLidca);
  }
  
  public Integer getLidca() {
    return lidca.intValue();
  }
  
  public void setLidcs(BigDecimal pLidcs) {
    if (pLidcs == null) {
      return;
    }
    lidcs = pLidcs.setScale(DECIMAL_LIDCS, RoundingMode.HALF_UP);
  }
  
  public void setLidcs(Integer pLidcs) {
    if (pLidcs == null) {
      return;
    }
    lidcs = BigDecimal.valueOf(pLidcs);
  }
  
  public Integer getLidcs() {
    return lidcs.intValue();
  }
  
  public void setLival(BigDecimal pLival) {
    if (pLival == null) {
      return;
    }
    lival = pLival.setScale(DECIMAL_LIVAL, RoundingMode.HALF_UP);
  }
  
  public void setLival(Integer pLival) {
    if (pLival == null) {
      return;
    }
    lival = BigDecimal.valueOf(pLival);
  }
  
  public Integer getLival() {
    return lival.intValue();
  }
  
  public void setLicol(BigDecimal pLicol) {
    if (pLicol == null) {
      return;
    }
    licol = pLicol.setScale(DECIMAL_LICOL, RoundingMode.HALF_UP);
  }
  
  public void setLicol(Integer pLicol) {
    if (pLicol == null) {
      return;
    }
    licol = BigDecimal.valueOf(pLicol);
  }
  
  public Integer getLicol() {
    return licol.intValue();
  }
  
  public void setLisgn(BigDecimal pLisgn) {
    if (pLisgn == null) {
      return;
    }
    lisgn = pLisgn.setScale(DECIMAL_LISGN, RoundingMode.HALF_UP);
  }
  
  public void setLisgn(Integer pLisgn) {
    if (pLisgn == null) {
      return;
    }
    lisgn = BigDecimal.valueOf(pLisgn);
  }
  
  public Integer getLisgn() {
    return lisgn.intValue();
  }
  
  public void setLiqtc(BigDecimal pLiqtc) {
    if (pLiqtc == null) {
      return;
    }
    liqtc = pLiqtc.setScale(DECIMAL_LIQTC, RoundingMode.HALF_UP);
  }
  
  public void setLiqtc(Double pLiqtc) {
    if (pLiqtc == null) {
      return;
    }
    liqtc = BigDecimal.valueOf(pLiqtc).setScale(DECIMAL_LIQTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqtc() {
    return liqtc.setScale(DECIMAL_LIQTC, RoundingMode.HALF_UP);
  }
  
  public void setLiunc(String pLiunc) {
    if (pLiunc == null) {
      return;
    }
    liunc = pLiunc;
  }
  
  public String getLiunc() {
    return liunc;
  }
  
  public void setLiqts(BigDecimal pLiqts) {
    if (pLiqts == null) {
      return;
    }
    liqts = pLiqts.setScale(DECIMAL_LIQTS, RoundingMode.HALF_UP);
  }
  
  public void setLiqts(Double pLiqts) {
    if (pLiqts == null) {
      return;
    }
    liqts = BigDecimal.valueOf(pLiqts).setScale(DECIMAL_LIQTS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqts() {
    return liqts.setScale(DECIMAL_LIQTS, RoundingMode.HALF_UP);
  }
  
  public void setLiksc(BigDecimal pLiksc) {
    if (pLiksc == null) {
      return;
    }
    liksc = pLiksc.setScale(DECIMAL_LIKSC, RoundingMode.HALF_UP);
  }
  
  public void setLiksc(Double pLiksc) {
    if (pLiksc == null) {
      return;
    }
    liksc = BigDecimal.valueOf(pLiksc).setScale(DECIMAL_LIKSC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiksc() {
    return liksc.setScale(DECIMAL_LIKSC, RoundingMode.HALF_UP);
  }
  
  public void setLipab(BigDecimal pLipab) {
    if (pLipab == null) {
      return;
    }
    lipab = pLipab.setScale(DECIMAL_LIPAB, RoundingMode.HALF_UP);
  }
  
  public void setLipab(Double pLipab) {
    if (pLipab == null) {
      return;
    }
    lipab = BigDecimal.valueOf(pLipab).setScale(DECIMAL_LIPAB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipab() {
    return lipab.setScale(DECIMAL_LIPAB, RoundingMode.HALF_UP);
  }
  
  public void setLipan(BigDecimal pLipan) {
    if (pLipan == null) {
      return;
    }
    lipan = pLipan.setScale(DECIMAL_LIPAN, RoundingMode.HALF_UP);
  }
  
  public void setLipan(Double pLipan) {
    if (pLipan == null) {
      return;
    }
    lipan = BigDecimal.valueOf(pLipan).setScale(DECIMAL_LIPAN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipan() {
    return lipan.setScale(DECIMAL_LIPAN, RoundingMode.HALF_UP);
  }
  
  public void setLipac(BigDecimal pLipac) {
    if (pLipac == null) {
      return;
    }
    lipac = pLipac.setScale(DECIMAL_LIPAC, RoundingMode.HALF_UP);
  }
  
  public void setLipac(Double pLipac) {
    if (pLipac == null) {
      return;
    }
    lipac = BigDecimal.valueOf(pLipac).setScale(DECIMAL_LIPAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipac() {
    return lipac.setScale(DECIMAL_LIPAC, RoundingMode.HALF_UP);
  }
  
  public void setLimht(BigDecimal pLimht) {
    if (pLimht == null) {
      return;
    }
    limht = pLimht.setScale(DECIMAL_LIMHT, RoundingMode.HALF_UP);
  }
  
  public void setLimht(Double pLimht) {
    if (pLimht == null) {
      return;
    }
    limht = BigDecimal.valueOf(pLimht).setScale(DECIMAL_LIMHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLimht() {
    return limht.setScale(DECIMAL_LIMHT, RoundingMode.HALF_UP);
  }
  
  public void setLiavr(Character pLiavr) {
    if (pLiavr == null) {
      return;
    }
    liavr = String.valueOf(pLiavr);
  }
  
  public Character getLiavr() {
    return liavr.charAt(0);
  }
  
  public void setLiqta(BigDecimal pLiqta) {
    if (pLiqta == null) {
      return;
    }
    liqta = pLiqta.setScale(DECIMAL_LIQTA, RoundingMode.HALF_UP);
  }
  
  public void setLiqta(Double pLiqta) {
    if (pLiqta == null) {
      return;
    }
    liqta = BigDecimal.valueOf(pLiqta).setScale(DECIMAL_LIQTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqta() {
    return liqta.setScale(DECIMAL_LIQTA, RoundingMode.HALF_UP);
  }
  
  public void setLiuna(String pLiuna) {
    if (pLiuna == null) {
      return;
    }
    liuna = pLiuna;
  }
  
  public String getLiuna() {
    return liuna;
  }
  
  public void setLikac(BigDecimal pLikac) {
    if (pLikac == null) {
      return;
    }
    likac = pLikac.setScale(DECIMAL_LIKAC, RoundingMode.HALF_UP);
  }
  
  public void setLikac(Double pLikac) {
    if (pLikac == null) {
      return;
    }
    likac = BigDecimal.valueOf(pLikac).setScale(DECIMAL_LIKAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLikac() {
    return likac.setScale(DECIMAL_LIKAC, RoundingMode.HALF_UP);
  }
  
  public void setLiart(String pLiart) {
    if (pLiart == null) {
      return;
    }
    liart = pLiart;
  }
  
  public String getLiart() {
    return liart;
  }
  
  public void setLimag(String pLimag) {
    if (pLimag == null) {
      return;
    }
    limag = pLimag;
  }
  
  public String getLimag() {
    return limag;
  }
  
  public void setLimaga(String pLimaga) {
    if (pLimaga == null) {
      return;
    }
    limaga = pLimaga;
  }
  
  public String getLimaga() {
    return limaga;
  }
  
  public void setLinumr(BigDecimal pLinumr) {
    if (pLinumr == null) {
      return;
    }
    linumr = pLinumr.setScale(DECIMAL_LINUMR, RoundingMode.HALF_UP);
  }
  
  public void setLinumr(Integer pLinumr) {
    if (pLinumr == null) {
      return;
    }
    linumr = BigDecimal.valueOf(pLinumr);
  }
  
  public Integer getLinumr() {
    return linumr.intValue();
  }
  
  public void setLisufr(BigDecimal pLisufr) {
    if (pLisufr == null) {
      return;
    }
    lisufr = pLisufr.setScale(DECIMAL_LISUFR, RoundingMode.HALF_UP);
  }
  
  public void setLisufr(Integer pLisufr) {
    if (pLisufr == null) {
      return;
    }
    lisufr = BigDecimal.valueOf(pLisufr);
  }
  
  public Integer getLisufr() {
    return lisufr.intValue();
  }
  
  public void setLinlir(BigDecimal pLinlir) {
    if (pLinlir == null) {
      return;
    }
    linlir = pLinlir.setScale(DECIMAL_LINLIR, RoundingMode.HALF_UP);
  }
  
  public void setLinlir(Integer pLinlir) {
    if (pLinlir == null) {
      return;
    }
    linlir = BigDecimal.valueOf(pLinlir);
  }
  
  public Integer getLinlir() {
    return linlir.intValue();
  }
  
  public void setLidath(BigDecimal pLidath) {
    if (pLidath == null) {
      return;
    }
    lidath = pLidath.setScale(DECIMAL_LIDATH, RoundingMode.HALF_UP);
  }
  
  public void setLidath(Integer pLidath) {
    if (pLidath == null) {
      return;
    }
    lidath = BigDecimal.valueOf(pLidath);
  }
  
  public void setLidath(Date pLidath) {
    if (pLidath == null) {
      return;
    }
    lidath = BigDecimal.valueOf(ConvertDate.dateToDb2(pLidath));
  }
  
  public Integer getLidath() {
    return lidath.intValue();
  }
  
  public Date getLidathConvertiEnDate() {
    return ConvertDate.db2ToDate(lidath.intValue(), null);
  }
  
  public void setLiordh(BigDecimal pLiordh) {
    if (pLiordh == null) {
      return;
    }
    liordh = pLiordh.setScale(DECIMAL_LIORDH, RoundingMode.HALF_UP);
  }
  
  public void setLiordh(Integer pLiordh) {
    if (pLiordh == null) {
      return;
    }
    liordh = BigDecimal.valueOf(pLiordh);
  }
  
  public Integer getLiordh() {
    return liordh.intValue();
  }
  
  public void setLisan(String pLisan) {
    if (pLisan == null) {
      return;
    }
    lisan = pLisan;
  }
  
  public String getLisan() {
    return lisan;
  }
  
  public void setLiact(String pLiact) {
    if (pLiact == null) {
      return;
    }
    liact = pLiact;
  }
  
  public String getLiact() {
    return liact;
  }
  
  public void setLinat(Character pLinat) {
    if (pLinat == null) {
      return;
    }
    linat = String.valueOf(pLinat);
  }
  
  public Character getLinat() {
    return linat.charAt(0);
  }
  
  public void setLimta(BigDecimal pLimta) {
    if (pLimta == null) {
      return;
    }
    limta = pLimta.setScale(DECIMAL_LIMTA, RoundingMode.HALF_UP);
  }
  
  public void setLimta(Double pLimta) {
    if (pLimta == null) {
      return;
    }
    limta = BigDecimal.valueOf(pLimta).setScale(DECIMAL_LIMTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLimta() {
    return limta.setScale(DECIMAL_LIMTA, RoundingMode.HALF_UP);
  }
  
  public void setLidlp(BigDecimal pLidlp) {
    if (pLidlp == null) {
      return;
    }
    lidlp = pLidlp.setScale(DECIMAL_LIDLP, RoundingMode.HALF_UP);
  }
  
  public void setLidlp(Integer pLidlp) {
    if (pLidlp == null) {
      return;
    }
    lidlp = BigDecimal.valueOf(pLidlp);
  }
  
  public void setLidlp(Date pLidlp) {
    if (pLidlp == null) {
      return;
    }
    lidlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pLidlp));
  }
  
  public Integer getLidlp() {
    return lidlp.intValue();
  }
  
  public Date getLidlpConvertiEnDate() {
    return ConvertDate.db2ToDate(lidlp.intValue(), null);
  }
  
  public void setLiser(BigDecimal pLiser) {
    if (pLiser == null) {
      return;
    }
    liser = pLiser.setScale(DECIMAL_LISER, RoundingMode.HALF_UP);
  }
  
  public void setLiser(Integer pLiser) {
    if (pLiser == null) {
      return;
    }
    liser = BigDecimal.valueOf(pLiser);
  }
  
  public Integer getLiser() {
    return liser.intValue();
  }
  
  public void setLipai(BigDecimal pLipai) {
    if (pLipai == null) {
      return;
    }
    lipai = pLipai.setScale(DECIMAL_LIPAI, RoundingMode.HALF_UP);
  }
  
  public void setLipai(Double pLipai) {
    if (pLipai == null) {
      return;
    }
    lipai = BigDecimal.valueOf(pLipai).setScale(DECIMAL_LIPAI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipai() {
    return lipai.setScale(DECIMAL_LIPAI, RoundingMode.HALF_UP);
  }
  
  public void setLipar(BigDecimal pLipar) {
    if (pLipar == null) {
      return;
    }
    lipar = pLipar.setScale(DECIMAL_LIPAR, RoundingMode.HALF_UP);
  }
  
  public void setLipar(Double pLipar) {
    if (pLipar == null) {
      return;
    }
    lipar = BigDecimal.valueOf(pLipar).setScale(DECIMAL_LIPAR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipar() {
    return lipar.setScale(DECIMAL_LIPAR, RoundingMode.HALF_UP);
  }
  
  public void setLirem1(BigDecimal pLirem1) {
    if (pLirem1 == null) {
      return;
    }
    lirem1 = pLirem1.setScale(DECIMAL_LIREM1, RoundingMode.HALF_UP);
  }
  
  public void setLirem1(Double pLirem1) {
    if (pLirem1 == null) {
      return;
    }
    lirem1 = BigDecimal.valueOf(pLirem1).setScale(DECIMAL_LIREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem1() {
    return lirem1.setScale(DECIMAL_LIREM1, RoundingMode.HALF_UP);
  }
  
  public void setLirem2(BigDecimal pLirem2) {
    if (pLirem2 == null) {
      return;
    }
    lirem2 = pLirem2.setScale(DECIMAL_LIREM2, RoundingMode.HALF_UP);
  }
  
  public void setLirem2(Double pLirem2) {
    if (pLirem2 == null) {
      return;
    }
    lirem2 = BigDecimal.valueOf(pLirem2).setScale(DECIMAL_LIREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem2() {
    return lirem2.setScale(DECIMAL_LIREM2, RoundingMode.HALF_UP);
  }
  
  public void setLirem3(BigDecimal pLirem3) {
    if (pLirem3 == null) {
      return;
    }
    lirem3 = pLirem3.setScale(DECIMAL_LIREM3, RoundingMode.HALF_UP);
  }
  
  public void setLirem3(Double pLirem3) {
    if (pLirem3 == null) {
      return;
    }
    lirem3 = BigDecimal.valueOf(pLirem3).setScale(DECIMAL_LIREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem3() {
    return lirem3.setScale(DECIMAL_LIREM3, RoundingMode.HALF_UP);
  }
  
  public void setLirem4(BigDecimal pLirem4) {
    if (pLirem4 == null) {
      return;
    }
    lirem4 = pLirem4.setScale(DECIMAL_LIREM4, RoundingMode.HALF_UP);
  }
  
  public void setLirem4(Double pLirem4) {
    if (pLirem4 == null) {
      return;
    }
    lirem4 = BigDecimal.valueOf(pLirem4).setScale(DECIMAL_LIREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem4() {
    return lirem4.setScale(DECIMAL_LIREM4, RoundingMode.HALF_UP);
  }
  
  public void setLirem5(BigDecimal pLirem5) {
    if (pLirem5 == null) {
      return;
    }
    lirem5 = pLirem5.setScale(DECIMAL_LIREM5, RoundingMode.HALF_UP);
  }
  
  public void setLirem5(Double pLirem5) {
    if (pLirem5 == null) {
      return;
    }
    lirem5 = BigDecimal.valueOf(pLirem5).setScale(DECIMAL_LIREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem5() {
    return lirem5.setScale(DECIMAL_LIREM5, RoundingMode.HALF_UP);
  }
  
  public void setLirem6(BigDecimal pLirem6) {
    if (pLirem6 == null) {
      return;
    }
    lirem6 = pLirem6.setScale(DECIMAL_LIREM6, RoundingMode.HALF_UP);
  }
  
  public void setLirem6(Double pLirem6) {
    if (pLirem6 == null) {
      return;
    }
    lirem6 = BigDecimal.valueOf(pLirem6).setScale(DECIMAL_LIREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem6() {
    return lirem6.setScale(DECIMAL_LIREM6, RoundingMode.HALF_UP);
  }
  
  public void setLitrl(Character pLitrl) {
    if (pLitrl == null) {
      return;
    }
    litrl = String.valueOf(pLitrl);
  }
  
  public Character getLitrl() {
    return litrl.charAt(0);
  }
  
  public void setLibrl(Character pLibrl) {
    if (pLibrl == null) {
      return;
    }
    librl = String.valueOf(pLibrl);
  }
  
  public Character getLibrl() {
    return librl.charAt(0);
  }
  
  public void setLirp1(Character pLirp1) {
    if (pLirp1 == null) {
      return;
    }
    lirp1 = String.valueOf(pLirp1);
  }
  
  public Character getLirp1() {
    return lirp1.charAt(0);
  }
  
  public void setLirp2(Character pLirp2) {
    if (pLirp2 == null) {
      return;
    }
    lirp2 = String.valueOf(pLirp2);
  }
  
  public Character getLirp2() {
    return lirp2.charAt(0);
  }
  
  public void setLirp3(Character pLirp3) {
    if (pLirp3 == null) {
      return;
    }
    lirp3 = String.valueOf(pLirp3);
  }
  
  public Character getLirp3() {
    return lirp3.charAt(0);
  }
  
  public void setLirp4(Character pLirp4) {
    if (pLirp4 == null) {
      return;
    }
    lirp4 = String.valueOf(pLirp4);
  }
  
  public Character getLirp4() {
    return lirp4.charAt(0);
  }
  
  public void setLirp5(Character pLirp5) {
    if (pLirp5 == null) {
      return;
    }
    lirp5 = String.valueOf(pLirp5);
  }
  
  public Character getLirp5() {
    return lirp5.charAt(0);
  }
  
  public void setLirp6(Character pLirp6) {
    if (pLirp6 == null) {
      return;
    }
    lirp6 = String.valueOf(pLirp6);
  }
  
  public Character getLirp6() {
    return lirp6.charAt(0);
  }
  
  public void setLitp1(String pLitp1) {
    if (pLitp1 == null) {
      return;
    }
    litp1 = pLitp1;
  }
  
  public String getLitp1() {
    return litp1;
  }
  
  public void setLitp2(String pLitp2) {
    if (pLitp2 == null) {
      return;
    }
    litp2 = pLitp2;
  }
  
  public String getLitp2() {
    return litp2;
  }
  
  public void setLitp3(String pLitp3) {
    if (pLitp3 == null) {
      return;
    }
    litp3 = pLitp3;
  }
  
  public String getLitp3() {
    return litp3;
  }
  
  public void setLitp4(String pLitp4) {
    if (pLitp4 == null) {
      return;
    }
    litp4 = pLitp4;
  }
  
  public String getLitp4() {
    return litp4;
  }
  
  public void setLitp5(String pLitp5) {
    if (pLitp5 == null) {
      return;
    }
    litp5 = pLitp5;
  }
  
  public String getLitp5() {
    return litp5;
  }
  
  public void setLiin1(Character pLiin1) {
    if (pLiin1 == null) {
      return;
    }
    liin1 = String.valueOf(pLiin1);
  }
  
  public Character getLiin1() {
    return liin1.charAt(0);
  }
  
  public void setLiin2(Character pLiin2) {
    if (pLiin2 == null) {
      return;
    }
    liin2 = String.valueOf(pLiin2);
  }
  
  public Character getLiin2() {
    return liin2.charAt(0);
  }
  
  public void setLiin3(Character pLiin3) {
    if (pLiin3 == null) {
      return;
    }
    liin3 = String.valueOf(pLiin3);
  }
  
  public Character getLiin3() {
    return liin3.charAt(0);
  }
  
  public void setLiin4(Character pLiin4) {
    if (pLiin4 == null) {
      return;
    }
    liin4 = String.valueOf(pLiin4);
  }
  
  public Character getLiin4() {
    return liin4.charAt(0);
  }
  
  public void setLiin5(Character pLiin5) {
    if (pLiin5 == null) {
      return;
    }
    liin5 = String.valueOf(pLiin5);
  }
  
  public Character getLiin5() {
    return liin5.charAt(0);
  }
  
  public void setLidlc(BigDecimal pLidlc) {
    if (pLidlc == null) {
      return;
    }
    lidlc = pLidlc.setScale(DECIMAL_LIDLC, RoundingMode.HALF_UP);
  }
  
  public void setLidlc(Integer pLidlc) {
    if (pLidlc == null) {
      return;
    }
    lidlc = BigDecimal.valueOf(pLidlc);
  }
  
  public void setLidlc(Date pLidlc) {
    if (pLidlc == null) {
      return;
    }
    lidlc = BigDecimal.valueOf(ConvertDate.dateToDb2(pLidlc));
  }
  
  public Integer getLidlc() {
    return lidlc.intValue();
  }
  
  public Date getLidlcConvertiEnDate() {
    return ConvertDate.db2ToDate(lidlc.intValue(), null);
  }
  
  public void setLiqtai(BigDecimal pLiqtai) {
    if (pLiqtai == null) {
      return;
    }
    liqtai = pLiqtai.setScale(DECIMAL_LIQTAI, RoundingMode.HALF_UP);
  }
  
  public void setLiqtai(Double pLiqtai) {
    if (pLiqtai == null) {
      return;
    }
    liqtai = BigDecimal.valueOf(pLiqtai).setScale(DECIMAL_LIQTAI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqtai() {
    return liqtai.setScale(DECIMAL_LIQTAI, RoundingMode.HALF_UP);
  }
  
  public void setLiqtci(BigDecimal pLiqtci) {
    if (pLiqtci == null) {
      return;
    }
    liqtci = pLiqtci.setScale(DECIMAL_LIQTCI, RoundingMode.HALF_UP);
  }
  
  public void setLiqtci(Double pLiqtci) {
    if (pLiqtci == null) {
      return;
    }
    liqtci = BigDecimal.valueOf(pLiqtci).setScale(DECIMAL_LIQTCI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqtci() {
    return liqtci.setScale(DECIMAL_LIQTCI, RoundingMode.HALF_UP);
  }
  
  public void setLiqtsi(BigDecimal pLiqtsi) {
    if (pLiqtsi == null) {
      return;
    }
    liqtsi = pLiqtsi.setScale(DECIMAL_LIQTSI, RoundingMode.HALF_UP);
  }
  
  public void setLiqtsi(Double pLiqtsi) {
    if (pLiqtsi == null) {
      return;
    }
    liqtsi = BigDecimal.valueOf(pLiqtsi).setScale(DECIMAL_LIQTSI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqtsi() {
    return liqtsi.setScale(DECIMAL_LIQTSI, RoundingMode.HALF_UP);
  }
  
  public void setLimhti(BigDecimal pLimhti) {
    if (pLimhti == null) {
      return;
    }
    limhti = pLimhti.setScale(DECIMAL_LIMHTI, RoundingMode.HALF_UP);
  }
  
  public void setLimhti(Double pLimhti) {
    if (pLimhti == null) {
      return;
    }
    limhti = BigDecimal.valueOf(pLimhti).setScale(DECIMAL_LIMHTI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLimhti() {
    return limhti.setScale(DECIMAL_LIMHTI, RoundingMode.HALF_UP);
  }
  
  public void setLiprbru(BigDecimal pLiprbru) {
    if (pLiprbru == null) {
      return;
    }
    liprbru = pLiprbru.setScale(DECIMAL_LIPRBRU, RoundingMode.HALF_UP);
  }
  
  public void setLiprbru(Double pLiprbru) {
    if (pLiprbru == null) {
      return;
    }
    liprbru = BigDecimal.valueOf(pLiprbru).setScale(DECIMAL_LIPRBRU, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiprbru() {
    return liprbru.setScale(DECIMAL_LIPRBRU, RoundingMode.HALF_UP);
  }
  
  public void setLiprnet(BigDecimal pLiprnet) {
    if (pLiprnet == null) {
      return;
    }
    liprnet = pLiprnet.setScale(DECIMAL_LIPRNET, RoundingMode.HALF_UP);
  }
  
  public void setLiprnet(Double pLiprnet) {
    if (pLiprnet == null) {
      return;
    }
    liprnet = BigDecimal.valueOf(pLiprnet).setScale(DECIMAL_LIPRNET, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiprnet() {
    return liprnet.setScale(DECIMAL_LIPRNET, RoundingMode.HALF_UP);
  }
  
  public void setLiprvfr(BigDecimal pLiprvfr) {
    if (pLiprvfr == null) {
      return;
    }
    liprvfr = pLiprvfr.setScale(DECIMAL_LIPRVFR, RoundingMode.HALF_UP);
  }
  
  public void setLiprvfr(Double pLiprvfr) {
    if (pLiprvfr == null) {
      return;
    }
    liprvfr = BigDecimal.valueOf(pLiprvfr).setScale(DECIMAL_LIPRVFR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiprvfr() {
    return liprvfr.setScale(DECIMAL_LIPRVFR, RoundingMode.HALF_UP);
  }
  
  public void setLiportf(BigDecimal pLiportf) {
    if (pLiportf == null) {
      return;
    }
    liportf = pLiportf.setScale(DECIMAL_LIPORTF, RoundingMode.HALF_UP);
  }
  
  public void setLiportf(Double pLiportf) {
    if (pLiportf == null) {
      return;
    }
    liportf = BigDecimal.valueOf(pLiportf).setScale(DECIMAL_LIPORTF, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiportf() {
    return liportf.setScale(DECIMAL_LIPORTF, RoundingMode.HALF_UP);
  }
  
  public void setPixxx1(BigDecimal pPixxx1) {
    if (pPixxx1 == null) {
      return;
    }
    pixxx1 = pPixxx1.setScale(DECIMAL_PIXXX1, RoundingMode.HALF_UP);
  }
  
  public void setPixxx1(Double pPixxx1) {
    if (pPixxx1 == null) {
      return;
    }
    pixxx1 = BigDecimal.valueOf(pPixxx1).setScale(DECIMAL_PIXXX1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPixxx1() {
    return pixxx1.setScale(DECIMAL_PIXXX1, RoundingMode.HALF_UP);
  }
  
  public void setPilfk1(BigDecimal pPilfk1) {
    if (pPilfk1 == null) {
      return;
    }
    pilfk1 = pPilfk1.setScale(DECIMAL_PILFK1, RoundingMode.HALF_UP);
  }
  
  public void setPilfk1(Double pPilfk1) {
    if (pPilfk1 == null) {
      return;
    }
    pilfk1 = BigDecimal.valueOf(pPilfk1).setScale(DECIMAL_PILFK1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfk1() {
    return pilfk1.setScale(DECIMAL_PILFK1, RoundingMode.HALF_UP);
  }
  
  public void setPilfv1(BigDecimal pPilfv1) {
    if (pPilfv1 == null) {
      return;
    }
    pilfv1 = pPilfv1.setScale(DECIMAL_PILFV1, RoundingMode.HALF_UP);
  }
  
  public void setPilfv1(Double pPilfv1) {
    if (pPilfv1 == null) {
      return;
    }
    pilfv1 = BigDecimal.valueOf(pPilfv1).setScale(DECIMAL_PILFV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfv1() {
    return pilfv1.setScale(DECIMAL_PILFV1, RoundingMode.HALF_UP);
  }
  
  public void setPilfp1(BigDecimal pPilfp1) {
    if (pPilfp1 == null) {
      return;
    }
    pilfp1 = pPilfp1.setScale(DECIMAL_PILFP1, RoundingMode.HALF_UP);
  }
  
  public void setPilfp1(Double pPilfp1) {
    if (pPilfp1 == null) {
      return;
    }
    pilfp1 = BigDecimal.valueOf(pPilfp1).setScale(DECIMAL_PILFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfp1() {
    return pilfp1.setScale(DECIMAL_PILFP1, RoundingMode.HALF_UP);
  }
  
  public void setPilfk3(BigDecimal pPilfk3) {
    if (pPilfk3 == null) {
      return;
    }
    pilfk3 = pPilfk3.setScale(DECIMAL_PILFK3, RoundingMode.HALF_UP);
  }
  
  public void setPilfk3(Double pPilfk3) {
    if (pPilfk3 == null) {
      return;
    }
    pilfk3 = BigDecimal.valueOf(pPilfk3).setScale(DECIMAL_PILFK3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfk3() {
    return pilfk3.setScale(DECIMAL_PILFK3, RoundingMode.HALF_UP);
  }
  
  public void setPilfv3(BigDecimal pPilfv3) {
    if (pPilfv3 == null) {
      return;
    }
    pilfv3 = pPilfv3.setScale(DECIMAL_PILFV3, RoundingMode.HALF_UP);
  }
  
  public void setPilfv3(Double pPilfv3) {
    if (pPilfv3 == null) {
      return;
    }
    pilfv3 = BigDecimal.valueOf(pPilfv3).setScale(DECIMAL_PILFV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPilfv3() {
    return pilfv3.setScale(DECIMAL_PILFV3, RoundingMode.HALF_UP);
  }
  
  public void setPitos(Character pPitos) {
    if (pPitos == null) {
      return;
    }
    pitos = String.valueOf(pPitos);
  }
  
  public Character getPitos() {
    return pitos.charAt(0);
  }
  
  public void setPicos(Character pPicos) {
    if (pPicos == null) {
      return;
    }
    picos = String.valueOf(pPicos);
  }
  
  public Character getPicos() {
    return picos.charAt(0);
  }
  
  public void setPinos(BigDecimal pPinos) {
    if (pPinos == null) {
      return;
    }
    pinos = pPinos.setScale(DECIMAL_PINOS, RoundingMode.HALF_UP);
  }
  
  public void setPinos(Integer pPinos) {
    if (pPinos == null) {
      return;
    }
    pinos = BigDecimal.valueOf(pPinos);
  }
  
  public Integer getPinos() {
    return pinos.intValue();
  }
  
  public void setPisos(BigDecimal pPisos) {
    if (pPisos == null) {
      return;
    }
    pisos = pPisos.setScale(DECIMAL_PISOS, RoundingMode.HALF_UP);
  }
  
  public void setPisos(Integer pPisos) {
    if (pPisos == null) {
      return;
    }
    pisos = BigDecimal.valueOf(pPisos);
  }
  
  public Integer getPisos() {
    return pisos.intValue();
  }
  
  public void setPilos(BigDecimal pPilos) {
    if (pPilos == null) {
      return;
    }
    pilos = pPilos.setScale(DECIMAL_PILOS, RoundingMode.HALF_UP);
  }
  
  public void setPilos(Integer pPilos) {
    if (pPilos == null) {
      return;
    }
    pilos = BigDecimal.valueOf(pPilos);
  }
  
  public Integer getPilos() {
    return pilos.intValue();
  }
  
  public void setLiarr(Character pLiarr) {
    if (pLiarr == null) {
      return;
    }
    liarr = String.valueOf(pLiarr);
  }
  
  public Character getLiarr() {
    return liarr.charAt(0);
  }
}
