/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.client.banque;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.personnalisation.reglement.EnumTypeReglement;
import ri.serien.libcommun.gescom.vente.reglement.BanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.CritereBanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.IdBanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.ListeBanqueClient;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Regrouper les accès SQL en relation avec les règlements.
 */
public class SqlBanqueClient {
  
  // Variables
  private QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlBanqueClient(QueryManager pQuerymg) {
    querymg = pQuerymg;
  }
  
  // -- Méthodes publiques
  
  /**
   * Charger la liste des banques des clients correspondants aux critères.
   */
  public ListeBanqueClient chargerListeIdBanqueClient(CritereBanqueClient pCriteres) {
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères de recherche des banques des clients sont invalides.");
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select distinct chetb, chdom from " + querymg.getLibrary() + '.' + EnumTableBDD.REGLEMENT + " where ");
    requeteSql.ajouterConditionAnd("chtrg", "=", EnumTypeReglement.CHEQUE.getCode());
    requeteSql.ajouterConditionAnd("chdom", "<>", "");
    
    // Filtre sur le code établissement
    if (pCriteres.getIdEtablissement() != null) {
      requeteSql.ajouterConditionAnd("chetb", "=", pCriteres.getIdEtablissement().getCodeEtablissement());
    }
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    
    // Création de la liste des banques trouvées
    ListeBanqueClient listeBanqueClient = new ListeBanqueClient();
    for (GenericRecord record : listeGenericRecord) {
      IdBanqueClient idBanqueClient = initialiserIdBanqueClient(record);
      BanqueClient banque = new BanqueClient(idBanqueClient);
      listeBanqueClient.add(banque);
    }
    return listeBanqueClient;
  }
  
  // -- Méthodes privées
  
  /**
   * Constituer l'identifiant d'une banque de client à partir de la lecture d'un record.
   */
  private IdBanqueClient initialiserIdBanqueClient(GenericRecord pGenericRecord) {
    // Lire la domiciliation
    String nomBanque = pGenericRecord.getStringValue("CHDOM");
    
    // création de l'identifiant de la banque
    IdBanqueClient idBanqueClient = IdBanqueClient.getInstance(nomBanque);
    
    return idBanqueClient;
  }
  
}
