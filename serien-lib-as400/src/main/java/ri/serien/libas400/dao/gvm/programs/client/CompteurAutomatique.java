/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs.client;

public class CompteurAutomatique {
  
  public final static String CPT_CLIENT = "CPT_CLI";
  public final static String CPT_FOURNISSEUR = "CPT_FRS";
  
  private String etb = null;
  private String type = null;
  private int numeroActif = 0;
  private int numeroMini = 0;
  private int numeroMaxi = 0;
  
  public CompteurAutomatique(String pEtb, String pType, int pNum, int pMini, int pMax) {
    etb = pEtb;
    type = pType;
    numeroActif = pNum;
    numeroMini = pMini;
    numeroMaxi = pMax;
  }
  
  public String getEtb() {
    return etb;
  }
  
  public String getType() {
    return type;
  }
  
  public int getNumeroActif() {
    return numeroActif;
  }
  
  public int getNumeroMini() {
    return numeroMini;
  }
  
  public int getNumeroMaxi() {
    return numeroMaxi;
  }
}
