/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx006.v3;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.flux.SqlFlux;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonStockWebV3;
import ri.serien.libas400.dao.sql.gescom.stock.SqlStock;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.exploitation.flux.historiqueenvoistock.HistoriqueEnvoiStock;
import ri.serien.libcommun.exploitation.flux.historiqueenvoistock.IdHistoriqueEnvoiStock;
import ri.serien.libcommun.exploitation.personnalisation.flux.TempsAttenteDeclenchement;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.stock.CritereStock;
import ri.serien.libcommun.gescom.commun.stock.ListeStock;
import ri.serien.libcommun.gescom.commun.stock.Stock;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Cette classe traite l'exportation des stocks pour les flux V3.
 */
public class ExportationStockWebV3 extends BaseGroupDB {
  // Constantes
  private static int NB_ART_STOCK_MAX = 500;
  private static int CONSERVATION_HISTORIQUE_EN_MOIS = 6;
  
  // Variables
  private ManagerFluxMagento managerFluxMagento = null;
  private static boolean purgeHistoriqueEnvoiStockEffectuee = false;
  
  /**
   * Constructeur.
   */
  public ExportationStockWebV3(ManagerFluxMagento pManagerFluxMagento) {
    super(pManagerFluxMagento.getQueryManager());
    managerFluxMagento = pManagerFluxMagento;
  }
  
  /**
   * Retourne l'ensemble des stocks articles.
   * 
   * @param pFluxMagento
   * @return
   */
  public JsonStockWebV3 retournerStocksWeb(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null) {
      majError("[ExportationStockWebV3] retournerStocksWeb() - Record null ou corrompu.");
      return null;
    }
    
    // Création d'un flux de stock
    JsonStockWebV3 jsonStockWebV3 = new JsonStockWebV3(pFluxMagento);
    Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), jsonStockWebV3.getEtb());
    if (numeroInstance == null) {
      majError("[ExportationStockWebV3] retournerStocksWeb() - Problème d'interpretation de l'Etb vers l'instance Web.");
      return null;
    }
    jsonStockWebV3.setIdInstanceMagento(numeroInstance);
    
    // Détermination du type de stock à transmettre en fonction du marqueur stocké dans le champ FLJSN
    String messageStock = "Pas de stock complet à transmettre.";
    boolean mouvementStock = false;
    if (Constantes.normerTexte(pFluxMagento.getFLJSN()).equals(TempsAttenteDeclenchement.FREQUENCE)) {
      mouvementStock = true;
      messageStock = "Pas de mouvement de stock à transmettre.";
    }
    
    // Création des identifiants établissement et magasin
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pFluxMagento.getFLETB());
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, managerFluxMagento.getParametresFlux().getMagasinMagento());
    List<IdArticle> listeIdArticle = null;
    
    // Récupération du reliquat sinon chargement des stocks depuis la base de données
    if (pFluxMagento.getListeReliquat() != null) {
      listeIdArticle = (List<IdArticle>) (Object) pFluxMagento.getListeReliquat();
    }
    else {
      // Retourne les identifiants des articles qui ont eu des mouvements de stock
      if (mouvementStock) {
        listeIdArticle = retournerListeIdArticleMouvementsDuJour(pFluxMagento.getFLIDF(), idMagasin);
      }
      // Retourne les identifiants des articles pour les stocks complets
      else {
        listeIdArticle = retournerListeIdArticleStockComplet(pFluxMagento.getFLIDF(), idMagasin);
      }
    }
    // Aucun stock trouvé
    if (listeIdArticle == null || listeIdArticle.isEmpty()) {
      pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
      pFluxMagento.setFLJSN(messageStock);
      pFluxMagento.setFLCOD("0");
      return jsonStockWebV3;
    }
    // Le nombre d'enregistrement est supérieur à celui accepté
    if (listeIdArticle.size() > NB_ART_STOCK_MAX) {
      listeIdArticle = extraireLigneATraiter(pFluxMagento, listeIdArticle);
    }
    
    // Chargement des stocks pour la liste des articles
    CritereStock critereStock = new CritereStock();
    critereStock.setListeIdArticle(listeIdArticle);
    critereStock.setIdMagasin(idMagasin);
    critereStock.setCalculerStockAvantRupture(true);
    
    SqlStock sqlStock = new SqlStock(queryManager);
    ListeStock listeStock = sqlStock.chargerListeStock(critereStock);
    jsonStockWebV3.mettreAJourListeStock(listeStock);
    
    // Enregistrement dans l'historique d'envoi des flux de stock
    sauverHistoriqueEnvoiStock(pFluxMagento.getFLID(), listeStock);
    
    // Aucun mouvement de stock
    if (jsonStockWebV3.getListeStocks() == null || jsonStockWebV3.getListeStocks().isEmpty()) {
      pFluxMagento.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
      pFluxMagento.setFLJSN(messageStock);
      pFluxMagento.setFLCOD("0");
      
      return jsonStockWebV3;
    }
    // Stockage du nombre d'enregistrements
    pFluxMagento.setFLCOD("" + jsonStockWebV3.getListeStocks().size());
    
    // Conversion de l'objet en Json pour le stocker dans l'enregistrement et contrôle les données de base
    if (!initJSON()) {
      majError("[ExportationStockWebV3] retournerStocksWeb() - Problème d'initialisation pour la sérialisation Json.");
      return null;
    }
    try {
      // Conversion du record en JSON
      pFluxMagento.setFLJSN(gson.toJson(jsonStockWebV3));
    }
    catch (Exception e) {
      majError(
          "[ExportationStockWebV3] retournerStocksWeb() - Erreur lors de la conversion de l'enregistrement en Json: " + e.getMessage());
    }
    
    return jsonStockWebV3;
  }
  
  /**
   * Met en forme la date de SERIEN à partir d'un String.
   * 
   * @param pDateSerieN
   * @return
   */
  public static String transformerDateSerieNEnHumaine(String pDateSerieN) {
    String separateurDate = "/";
    String retour = "";
    
    if (pDateSerieN == null) {
      return retour;
    }
    
    // Date pourrave du siècle dernier
    if (pDateSerieN.length() == 7) {
      // Récupération du jour
      retour = pDateSerieN.substring(5, 7) + separateurDate;
      // Récupération du mois
      retour += pDateSerieN.substring(3, 5) + separateurDate;
      // Récupération de l'année
      retour += 1900 + Integer.parseInt(pDateSerieN.substring(0, 3));
    }
    
    return retour;
  }
  
  /**
   * Met à null les variables.
   */
  @Override
  public void dispose() {
    queryManager = null;
  }
  
  // -- Méthodes privées
  
  /**
   * Permet de retourner tous les identifiants articles qui ont subi des mouvements à la date du jour.
   * 
   * @param pIdentifiantFlux
   * @param pIdMagasin
   * @return
   */
  private List<IdArticle> retournerListeIdArticleMouvementsDuJour(String pIdentifiantFlux, IdMagasin pIdMagasin) {
    if (Constantes.normerTexte(pIdentifiantFlux).isEmpty()) {
      majError("[ExportationStockWebV3] retournerListeIdArticleMouvementsDuJour() - L'identifiant Flux est invalide.");
      return null;
    }
    if (pIdMagasin == null) {
      majError("[ExportationStockWebV3] retournerListeIdArticleMouvementsDuJour() - L'identifiant magasin est invalide.");
      return null;
    }
    
    // Contrôle du dernier succès d'envoi flux des stocks
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    String dateDernierEnvoi = "0";
    String heureDernierEnvoi = "0";
    // @formatter:off
    requeteSql.ajouter("select VARCHAR_FORMAT(MAX(FLTRT), 'YYMMDD') as MADATE, VARCHAR_FORMAT(MAX(FLTRT), 'HH24MISS') as MONHEURE from "
        + queryManager.getLibrary() + '.' + EnumTableBDD.FLUX + " where");
    requeteSql.ajouterConditionAnd("FLETB", "=", pIdMagasin.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("FLIDF", "=", pIdentifiantFlux);
    requeteSql.ajouterConditionAnd("FLSTT", "=", EnumStatutFlux.TRAITE.getNumero());
    requeteSql.ajouterConditionAnd("FLERR", "=", FluxMagento.ERR_PAS_ERREUR);
    // @formatter:on
    // Exécution de la requête
    ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (liste == null) {
      majError("[ExportationStockWebV3] retournerStocksMouvementsDuJour() LISTE 1 NULL " + queryManager.getMsgError());
      return null;
    }
    
    // Date
    if (liste.size() > 0 && liste.get(0).isPresentField("MADATE")) {
      dateDernierEnvoi = "1" + liste.get(0).getField("MADATE").toString().trim();
    }
    else {
      dateDernierEnvoi = "0";
    }
    // Heure
    if (liste.size() > 0 && liste.get(0).isPresentField("MONHEURE")) {
      heureDernierEnvoi = liste.get(0).getField("MONHEURE").toString().trim();
    }
    else {
      heureDernierEnvoi = "0";
    }
    
    // Construction de la requête
    requeteSql.effacerRequete();
    // @formatter:off
    requeteSql.ajouter("select S1ETB, S1ART, S1DTMJ, S1HRMJ from " + queryManager.getLibrary() + '.' + EnumTableBDD.STOCK
        + " left join " + queryManager.getLibrary() + '.' + EnumTableBDD.ARTICLE + " on S1ETB = A1ETB and S1ART = A1ART where");
    requeteSql.ajouterConditionAnd("S1ETB", "=", pIdMagasin.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("S1MAG", "=", pIdMagasin.getCode());
    requeteSql.ajouterConditionAnd("A1IN15", "<>", Article.JAMAIS_VU_SUR_LE_WEB);
    requeteSql.ajouterConditionAnd("(S1DTMJ", ">", dateDernierEnvoi);
    requeteSql.ajouter(" or (");
    requeteSql.ajouterConditionAnd("S1DTMJ", "=", dateDernierEnvoi);
    // Avec marge de 5 mn
    requeteSql.ajouterConditionAnd("S1HRMJ", ">=", "(" + heureDernierEnvoi + "-300)", true, false);
    requeteSql.ajouter(")) ");
    requeteSql.ajouterConditionAnd("S1DTMJ", "<>", 0);
    requeteSql.ajouter(" order by S1DTMJ, S1HRMJ");
    // @formatter:on
    
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Création des identifiants articles
    List<IdArticle> listeIdArticle = new ArrayList<IdArticle>();
    for (GenericRecord record : listeRecord) {
      if (record == null) {
        continue;
      }
      if (!record.isPresentField("S1ETB") || !record.isPresentField("S1ART")) {
        continue;
      }
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getString("S1ETB"));
      String codeArticle = record.getString("S1ART");
      IdArticle idArticle = IdArticle.getInstance(idEtablissement, codeArticle);
      listeIdArticle.add(idArticle);
    }
    
    return listeIdArticle;
  }
  
  /**
   * Permet de retourner tous les identifiants articles.
   * 
   * @param pIdentifiantFlux
   * @param pIdMagasin
   * @return
   */
  private List<IdArticle> retournerListeIdArticleStockComplet(String pIdentifiantFlux, IdMagasin pIdMagasin) {
    if (Constantes.normerTexte(pIdentifiantFlux).isEmpty()) {
      majError("[ExportationStockWebV3] retournerListeIdArticleStockComplet() - L'identifiant Flux est invalide.");
      return null;
    }
    if (pIdMagasin == null) {
      majError("[ExportationStockWebV3] retournerListeIdArticleStockComplet() - L'identifiant magasin est invalide.");
      return null;
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter("select S1ETB, S1ART, S1DTMJ, S1HRMJ from " + queryManager.getLibrary() + '.' + EnumTableBDD.STOCK
        + " left join " + queryManager.getLibrary() + '.' + EnumTableBDD.ARTICLE + " on S1ETB = A1ETB and S1ART = A1ART where");
    requeteSql.ajouterConditionAnd("S1ETB", "=", pIdMagasin.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("S1MAG", "=", pIdMagasin.getCode());
    // Exclusion des articles désactivés
    requeteSql.ajouterConditionAnd("A1NPU", "<>", "1");
    requeteSql.ajouterConditionAnd("A1IN15", "<>", Article.JAMAIS_VU_SUR_LE_WEB);
    // @formatter:on
    
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Création des identifiants articles
    List<IdArticle> listeIdArticle = new ArrayList<IdArticle>();
    for (GenericRecord record : listeRecord) {
      if (record == null) {
        continue;
      }
      if (!record.isPresentField("S1ETB") || !record.isPresentField("S1ART")) {
        continue;
      }
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getString("S1ETB"));
      String codeArticle = record.getString("S1ART");
      IdArticle idArticle = IdArticle.getInstance(idEtablissement, codeArticle);
      listeIdArticle.add(idArticle);
    }
    
    return listeIdArticle;
  }
  
  /**
   * Permet d'isoler des flux de stocks trop importants en plusieurs flux plus petits.
   * Le critère étant la quantité de lignes d'articles concernés
   */
  private List<IdArticle> extraireLigneATraiter(FluxMagento pFluxInitial, List<IdArticle> pListeInitiale) {
    if (managerFluxMagento == null || pListeInitiale == null || pListeInitiale.isEmpty()) {
      return pListeInitiale;
    }
    
    List<IdArticle> listeAmaigrie = new ArrayList<IdArticle>();
    List<Object> listeReliquat = new ArrayList<Object>();
    
    // Découpage du flux initial en 2 listes : une à traiter et un reliquat
    for (int i = 0; i < pListeInitiale.size(); i++) {
      if (i < NB_ART_STOCK_MAX) {
        listeAmaigrie.add(pListeInitiale.get(i));
      }
      else {
        listeReliquat.add(pListeInitiale.get(i));
      }
    }
    
    if (listeReliquat.size() > 0) {
      managerFluxMagento.traiterReliquatFluxSortant(pFluxInitial, listeReliquat);
    }
    
    return listeAmaigrie;
  }
  
  /**
   * Sauvegarde des enregistrements de l'historique d'envoi du flux stock.
   * 
   * @param pNumeroFlux
   * @param pListeStock
   */
  private void sauverHistoriqueEnvoiStock(int pNumeroFlux, ListeStock pListeStock) {
    if (pListeStock == null || pListeStock.isEmpty()) {
      return;
    }
    
    SqlFlux sqlFlux = new SqlFlux(queryManager);
    Date dateEnvoi = new Date();
    
    // Purge de la table si cela n'a pas été encore fait aujourd'hui
    if (!purgeHistoriqueEnvoiStockEffectuee) {
      try {
        Date datePurge = Constantes.getDateDelaiMois(dateEnvoi, -CONSERVATION_HISTORIQUE_EN_MOIS);
        Trace.info("Suppression des enregistrements de l'historique d'envoi des flux stock antérieurs au "
            + DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, datePurge));
        sqlFlux.purgerEnregistrementEnvoiStockV3(datePurge);
        purgeHistoriqueEnvoiStockEffectuee = true;
      }
      catch (Exception e) {
        Trace.erreur(e, "La purge des enregistrements de l'historique d'envoi de flux stock n'a pas pu être effectuée.");
      }
    }
    
    // Création de la liste d'objets
    for (Stock stock : pListeStock) {
      if (stock == null) {
        continue;
      }
      try {
        IdHistoriqueEnvoiStock idHistoriqueEnvoiStock = IdHistoriqueEnvoiStock.getInstanceAvecCreationId(stock.getId().getIdMagasin());
        HistoriqueEnvoiStock historique = new HistoriqueEnvoiStock(idHistoriqueEnvoiStock);
        historique.setIdArticle(stock.getId().getIdArticle());
        historique.setNumeroFlux(pNumeroFlux);
        historique.setDateEnvoi(dateEnvoi);
        historique.setStockAvantRupture(stock.getStockAvantRupture());
        historique.setDateRupturePossible(stock.getDateRupturePossible());
        historique.setDateFinRupture(stock.getDateFinRupture());
        
        // Sauvegarde en table
        sqlFlux.sauverEnvoiStockV3(historique);
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur lors de l'enregistrement de l'historique d'envoi des flux stock " + stock);
      }
    }
  }
}
