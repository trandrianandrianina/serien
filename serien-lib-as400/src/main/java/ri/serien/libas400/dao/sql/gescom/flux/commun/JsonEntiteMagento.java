/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun;

import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Attention cette classe est commune entre les version V1 et V2 des flux.
 * Il faudra corriger cela dans un second temps car utlisée par LA_FluxMagento.
 */
public class JsonEntiteMagento {
  protected String idFlux = null;
  protected String versionFlux = "1.0";
  protected int idInstanceMagento = -1;
  
  protected transient String etb = null;
  protected transient String msgErreur = "";
  protected transient SimpleDateFormat formater = null;
  // TODO a mettre en paramètre pour activer un contrôle ou tronquer les blocs adresses
  public static final int NB_MAX_CARACTERES_BLOC_ADRESSE = 30;
  
  /**
   * Entité de base Série N au format Magento
   * Cette classe sert à l'intégration JSON vers JAVA ou JAVA vers JSON
   * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS
   */
  public JsonEntiteMagento() {
  }
  
  /**
   * Construit le message d'erreur proprement
   */
  protected void majError(String nvMessage) {
    if (msgErreur == null) {
      msgErreur = nvMessage;
    }
    else {
      msgErreur += "\n" + nvMessage;
    }
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    
    msgErreur = null;
    
    return chaine;
  }
  
  /**
   * retourner la date du jour au format Série N
   */
  public String retournerDateDuJourSN() {
    SimpleDateFormat formater = new SimpleDateFormat("yyMMdd");
    String retour = null;
    
    try {
      retour = "1" + formater.format(new Date());
    }
    catch (Exception e) {
      retour = "0";
      majError("[EntiteMagento] retournerDateDuJourSN() PB formatage DATE DU JOUR");
    }
    
    return retour;
  }
  
  /**
   * permet de retourner une clé de classement pour une expression passée
   */
  public String retournerUneCleDeClassement(String chaineBrute, int longueurMax) {
    if (chaineBrute == null) {
      return null;
    }
    
    if (chaineBrute.trim().length() > longueurMax) {
      chaineBrute = chaineBrute.trim().substring(0, longueurMax);
    }
    
    chaineBrute = Normalizer.normalize(chaineBrute.trim(), Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
    chaineBrute = chaineBrute.trim().toUpperCase();
    
    return chaineBrute.trim();
  }
  
  /**
   * On contrôle si la valeur passée est numérique (obligatoire pour Série N), option sur les codes francais
   */
  public static boolean controlerUnCodePostal(boolean isFR, String codePostal) {
    // On accepte un code postal Null ou vide
    if (codePostal == null || codePostal.trim().equals("")) {
      return true;
    }
    // Bonne longueur codes francais
    if (isFR) {
      // Pour l'instant on teste comme ça A voir pour durcir le contrôle
      if (codePostal.trim().length() > 5) {
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * On vérifie qu'un code postal soit un entier
   */
  public boolean codePostalEstUnEntier(String pCodePostal) {
    try {
      Integer.parseInt(pCodePostal);
    }
    catch (NumberFormatException e) {
      return false;
    }
    
    return true;
  }
  
  // -- Accesseurs
  
  public String getIdFlux() {
    return idFlux;
  }
  
  public void setIdFlux(String idFlux) {
    this.idFlux = idFlux;
  }
  
  public String getVersionFlux() {
    return versionFlux;
  }
  
  public void setVersionFlux(String versionFlux) {
    this.versionFlux = versionFlux;
  }
  
  public String getEtb() {
    return etb;
  }
  
  public void setEtb(String etb) {
    this.etb = etb;
  }
  
  public int getIdInstanceMagento() {
    return idInstanceMagento;
  }
  
  public void setIdInstanceMagento(int idInstanceMagento) {
    this.idInstanceMagento = idInstanceMagento;
  }
  
}
