/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx006.v3;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonStockWebV3;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.personnalisation.flux.PersonnalisationFlux;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

/**
 * Cette classe permet de stocker les flux stock V3 afin de générer un fichier JSON.
 */
public class FluxStockV3 {
  // Constantes
  public static final String DOSSIER_TEMPORAIRE = "/tmp/flux/FLX006";
  private static final int LONGUEUR_VERSION = 1;
  private static final int LONGUEUR_CURLIB = 10;
  private static final int LONGUEUR_CODFTP = 5;
  
  // Variables
  private JsonStockWebV3 jsonStockWebV3 = null;
  private transient AS400 systeme = null;
  private transient PersonnalisationFlux personnalisationFlux = null;
  private transient Bibliotheque baseDeDonnees = null;
  
  /**
   * Constructeur.
   */
  public FluxStockV3(JsonStockWebV3 pjsonStockWebV3) {
    jsonStockWebV3 = pjsonStockWebV3;
  }
  
  // -- Méthodes publiques
  
  /**
   * Transfère le fichier JSON vers le serveur adéquat.
   */
  public void transfererVersServeur() {
    String chaineJson = convertirEnJson();
    if (chaineJson == null) {
      Trace.erreur("La chaine Json des stocks est invalide.");
      return;
    }
    
    try {
      // Modifier le nom du champ idInstanceMagento en idInstance (Provisoire)
      chaineJson = chaineJson.replaceAll("idInstanceMagento", "idInstance");
      
      // Contrôle des variables
      if (EnvironnementExecution.getLettreEnvironnement() == null || EnvironnementExecution.getLettreEnvironnement().charValue() == ' ') {
        throw new MessageErreurException("La lettre d'environnement est invalide.");
      }
      if (baseDeDonnees == null) {
        throw new MessageErreurException("La base de données est invalide.");
      }
      if (systeme == null) {
        throw new MessageErreurException("Le système est invalide.");
      }
      if (personnalisationFlux == null) {
        throw new MessageErreurException("La personnalisation du flux stock est invalide.");
      }
      
      // Construire le nom du fichier
      String nomFichier = construireNomFichier(null);
      File cheminComplet = new File(DOSSIER_TEMPORAIRE + File.separator + EnvironnementExecution.getLettreEnvironnement().charValue()
          + File.separator + nomFichier);
      cheminComplet = controlerExistenceNomFichier(cheminComplet);
      
      // Générer le fichier JSON dans un dossier tmp
      GestionFichierTexte gft = new GestionFichierTexte(cheminComplet);
      gft.setForceEncodage(GestionFichierTexte.ENCODAGE_UTF8);
      gft.setContenuFichier(chaineJson);
      gft.ecritureFichier();
      
      // Trace de débug
      Trace.debug("--> lettre : " + EnvironnementExecution.getLettreEnvironnement() + " curlib : " + baseDeDonnees.getNom() + " codftp : "
          + personnalisationFlux.getCodeFTP() + " expas  :" + EnvironnementExecution.getExpas());
      
      // Transférer le fichier vers le serveur sftp
      ProgramParameter[] listeParametre = new ProgramParameter[3];
      AS400Text lettre = new AS400Text(LONGUEUR_VERSION);
      listeParametre[0] =
          new ProgramParameter(lettre.toBytes(EnvironnementExecution.getLettreEnvironnement().toString()), LONGUEUR_VERSION);
      AS400Text curlib = new AS400Text(LONGUEUR_CURLIB);
      listeParametre[1] = new ProgramParameter(curlib.toBytes(baseDeDonnees.getNom()), LONGUEUR_CURLIB);
      AS400Text codftp = new AS400Text(LONGUEUR_CODFTP);
      listeParametre[2] = new ProgramParameter(codftp.toBytes(personnalisationFlux.getCodeFTP()), LONGUEUR_CODFTP);
      
      ContexteProgrammeRPG programme = new ContexteProgrammeRPG(systeme, baseDeDonnees);
      boolean retour = programme.executerProgramme("SEXP74CL", EnvironnementExecution.getExpas(), listeParametre);
      // Si le transfert c'est bien passé
      if (retour) {
        Trace.info("Transfert du fichier " + cheminComplet + " réussit.");
      }
      else {
        Trace.info("Transfert du fichier " + cheminComplet + " en échec.");
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du transfert des flux stock vers le serveur.");
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Converti l'objet en chaine JSON.
   */
  private String convertirEnJson() {
    try {
      GsonBuilder builderJSON = new GsonBuilder();
      Gson gson = builderJSON.create();
      return gson.toJson(jsonStockWebV3);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la conversion en JSON.");
    }
    return null;
  }
  
  /**
   * Construit le nom du fichier au format FLX006_Inventory_JJMMAAAAHHMMSS.json.
   */
  private String construireNomFichier(Date pDate) {
    if (pDate == null) {
      pDate = new Date();
    }
    String jour = DateHeure.getJourHeure(6, pDate);
    String heure = DateHeure.getJourHeure(8, pDate);
    return "FLX006_Inventory_" + jour + heure + ".json";
  }
  
  /**
   * Contrôle l'existence du nom du fichier.
   * Si c'est le cas le nom est modifié puis retrourné.
   */
  private File controlerExistenceNomFichier(File pCheminComplet) {
    if (!pCheminComplet.exists()) {
      return pCheminComplet;
    }
    
    String nomDossier = pCheminComplet.getParent();
    Calendar calendar = Calendar.getInstance();
    // Incrément d'une seconde à chaque contrôle afin de trouver un nom libre
    do {
      calendar.add(Calendar.SECOND, 1);
      String nomFichier = construireNomFichier(calendar.getTime());
      pCheminComplet = new File(nomDossier + File.separator + nomFichier);
    }
    while (pCheminComplet.exists());
    return pCheminComplet;
  }
  
  // -- Accesseurs
  
  /**
   * Initialise le système.
   */
  public void setSysteme(AS400 pSysteme) {
    this.systeme = pSysteme;
  }
  
  /**
   * Retourne la personnalisation du flux.
   */
  public PersonnalisationFlux getPersonnalisationFlux() {
    return personnalisationFlux;
  }
  
  /**
   * Initialisation de la personnalisation du flux.
   */
  public void setPersonnalisationFlux(PersonnalisationFlux personnalisationFlux) {
    this.personnalisationFlux = personnalisationFlux;
  }
  
  /**
   * Retourne la base de données.
   */
  public Bibliotheque getBaseDeDonnees() {
    return baseDeDonnees;
  }
  
  /**
   * Initialise la base de données.
   */
  public void setBaseDeDonnees(Bibliotheque baseDeDonnees) {
    this.baseDeDonnees = baseDeDonnees;
  }
  
  /**
   * Retourne si les variables sont initialisées.
   */
  public boolean isInitialiser() {
    if (baseDeDonnees == null || personnalisationFlux == null || systeme == null) {
      return false;
    }
    return true;
  }
}
