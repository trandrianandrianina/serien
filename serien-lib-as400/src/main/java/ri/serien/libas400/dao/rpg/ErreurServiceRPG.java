/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg;

import java.io.Serializable;

public class ErreurServiceRPG implements Serializable {
  // Constantes
  public static final int ERREUR = 9;
  
  // Variables
  private int code = ' ';
  private String libelle = "";
  
  /**
   * Retourne si c'est un message d'avertissement.
   */
  public boolean isAvertissement() {
    return code < ERREUR;
  }
  
  /**
   * Retourne si c'est un message d'erreur.
   */
  public boolean isErreur() {
    return code == ERREUR;
  }
  
  // Accesseurs
  
  public int getCode() {
    return code;
  }
  
  public void setCode(int code) {
    this.code = code;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
}
