/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx010.v2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxInit;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi.FactureEDI_GP;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.outils.ftp.FTPManager;

public class GM_Export_Factu_EDI extends BaseGroupDB {
  
  FTPManager serveurFTP = null;
  // QueryManager querymg = null;
  SystemeManager system = null;
  private String lettreEnv = null;
  private ParametresFluxBibli parametresFlux = null;
  
  /**
   * Constructeur principal
   */
  public GM_Export_Factu_EDI(ParametresFluxBibli pParms, QueryManager aquerymg, SystemeManager sys, String lettre) {
    super(aquerymg);
    parametresFlux = pParms;
    system = sys;
    lettreEnv = lettre;
  }
  
  /**
   * Envoyer le détail d'une facture
   */
  public boolean envoyerFACTURE(FluxMagento record) {
    if (record == null) {
      majError("(GM_Export_Expe_EDI] envoyerFACTURE() record NULL");
      return false;
    }
    
    boolean retour = false;
    // 1 Récupérer la commande et vérifier ses données et son statut
    FactureEDI_GP facture = recupererLaCommandeSN(record);
    
    // 2 Construire un fichier de retour
    if (facture != null && facture.isValide()) {
      // On construit un fichier sur la base de ces données dans la zone d'envoi FTP
      File fichier = construireUneFACTURE(facture);
      // 3 Envoyer en FTP
      if (fichier != null && fichier.isFile()) {
        record.setFLCOD(fichier.getName());
        return envoyerLeFichierFTP(fichier);
      }
    }
    else {
      if (facture == null) {
        majError("(GM_Export_Factu_EDI] envoyerFACTURE facture NULL ");
      }
      else {
        majError("(GM_Export_Factu_EDI] envoyerFACTURE PB facture invalide: " + facture.getMsgError());
      }
    }
    
    return retour;
  }
  
  /**
   * Construire un fichier physique EDI de la facture Série N
   * @param facture
   * @return
   */
  private File construireUneFACTURE(FactureEDI_GP facture) {
    if (facture == null) {
      majError("(GM_Export_Factu_EDI] construireUneFACTURE() facture NULL ou corrompu");
      return null;
    }
    
    File fichier = null;
    String nomFichier = parametresFlux.getPrefixe_invoic_EDI() + facture.getNumero_Facture_13() + " " + facture.getLib_clientPayeur_3();
    if (nomFichier.length() > 46) {
      nomFichier = nomFichier.substring(0, 46) + ".txt";
    }
    
    fichier = new File(parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator
        + parametresFlux.getDossier_local_fact_EDI() + nomFichier);
    
    PrintWriter out;
    try {
      out = new PrintWriter(new BufferedWriter(new FileWriter(fichier)));
      
      String retour = retournerContenuFacture(facture);
      if (retour == null) {
        majError("(GM_Export_Factu_EDI] construireUneFACTURE() retour NUUUUL ");
        out.close();
        return null;
      }
      else {
        out.write(retour);
      }
      out.close(); // Ferme le flux du fichier, sauvegardant ainsi les données.
      
      return fichier;
    }
    catch (IOException e) {
      majError("(GM_Export_Factu_EDI] construireUneFACTURE() pb d'écriture de fichier " + e.getMessage());
      return null;
    }
    
  }
  
  /**
   * Retourne le contenu de la facture EDI ligne par ligne au format @GP
   */
  private String retournerContenuFacture(FactureEDI_GP facture) {
    if (facture == null) {
      majError("(GM_Export_Factu_EDI] retournerContenuFacture() facture NUUUULLE "); // ou invalide ");
      return null;
    }
    StringBuilder retour = new StringBuilder();
    
    // Entête de document
    retour.append(facture.getId_gp_1() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLogiciel_edi_2() + ParametresFluxInit.EDI_FIN_CHAMP
        + facture.getType_fichier_3() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getFormat_fichier_4()
        + ParametresFluxInit.EDI_FIN_LIGNE);
    
    // En tête de facture
    retour.append(facture.getId_entete_1() + ParametresFluxInit.EDI_FIN_CHAMP // 1
        + facture.getCommande_client_2() + ParametresFluxInit.EDI_FIN_CHAMP // 2
        + facture.getDate_commande_3() + ParametresFluxInit.EDI_FIN_CHAMP // 3
        + ParametresFluxInit.EDI_FIN_CHAMP // 4
        + ParametresFluxInit.EDI_FIN_CHAMP // 5
        + ParametresFluxInit.EDI_FIN_CHAMP // 6
        + facture.getDate_livraison_7() + ParametresFluxInit.EDI_FIN_CHAMP // 7
        + facture.getBon_livraison_8() + ParametresFluxInit.EDI_FIN_CHAMP // 8
        + ParametresFluxInit.EDI_FIN_CHAMP // 9
        + ParametresFluxInit.EDI_FIN_CHAMP // 10
        + ParametresFluxInit.EDI_FIN_CHAMP // 11
        + ParametresFluxInit.EDI_FIN_CHAMP // 12
        + facture.getNumero_Facture_13() + ParametresFluxInit.EDI_FIN_CHAMP // 13
        + facture.getDate_Facture_14() + ParametresFluxInit.EDI_FIN_CHAMP // 14
        + facture.getDate_echeance_15() + ParametresFluxInit.EDI_FIN_CHAMP // 15
        + facture.getTypeDocument_16() + ParametresFluxInit.EDI_FIN_CHAMP // 16
        + facture.getDevise_facture_17() + ParametresFluxInit.EDI_FIN_CHAMP // 17
        + ParametresFluxInit.EDI_FIN_CHAMP // 18
        + ParametresFluxInit.EDI_FIN_CHAMP // 19
        + ParametresFluxInit.EDI_FIN_CHAMP // 20
        + ParametresFluxInit.EDI_FIN_CHAMP // 21
        + ParametresFluxInit.EDI_FIN_CHAMP // 22
        + ParametresFluxInit.EDI_FIN_CHAMP // 23
        + ParametresFluxInit.EDI_FIN_CHAMP // 24
        + ParametresFluxInit.EDI_FIN_CHAMP // 25
        + parametresFlux.getMode_test_EDI() + ParametresFluxInit.EDI_FIN_CHAMP // 26
        + facture.getCodePaiement_27() + ParametresFluxInit.EDI_FIN_CHAMP // 27
        + facture.getNatureDocument_28() + ParametresFluxInit.EDI_FIN_LIGNE); // 28
    
    // Dates de factures: 35 est date de livraison mais bien sùr ils ont trouvé ça amusant de mettre la date de facture....
    // Du coup ben je fais pareil
    retour.append(facture.getDECLA_DTM() + ParametresFluxInit.EDI_FIN_CHAMP + "35" + ParametresFluxInit.EDI_FIN_CHAMP
        + facture.getDate_Facture_14() + ParametresFluxInit.EDI_FIN_LIGNE);
    
    // Partenaires
    retour.append(facture.getDECLA_PAR() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getEANclientPayeur_2()
        + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLib_clientPayeur_3() + ParametresFluxInit.EDI_FIN_CHAMP
        + facture.getEANfournisseur_4() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLib_fournisseur_5()
        + ParametresFluxInit.EDI_FIN_CHAMP + facture.getEANclientlivre_6() + ParametresFluxInit.EDI_FIN_CHAMP
        + facture.getLib_clientLivre_7() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getEANclientFacture_8()
        + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLib_clientFacture_9() + ParametresFluxInit.EDI_FIN_CHAMP
        + facture.getEAN_factor_10() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLib_factor_11() + ParametresFluxInit.EDI_FIN_LIGNE);
    
    // les lignes
    for (int i = 0; i < facture.getLignes().size(); i++) {
      retour.append(
          facture.getLignes().get(i).getId_ligne_1() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLignes().get(i).getNumero_ligne_2()
              + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLignes().get(i).getCode_EAN_produit_3() + ParametresFluxInit.EDI_FIN_CHAMP
              + facture.getLignes().get(i).getCode_fournisseur_produit_4() + ParametresFluxInit.EDI_FIN_CHAMP
              + facture.getLignes().get(i).getQuantite_ligne_7() + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP
              + facture.getLignes().get(i).getUniteQte_ligne_8() + ParametresFluxInit.EDI_FIN_CHAMP
              + facture.getLignes().get(i).getQuantite_ligne_7() + ParametresFluxInit.EDI_FIN_CHAMP
              + facture.getLignes().get(i).getQuantite_ligne_7() + ParametresFluxInit.EDI_FIN_CHAMP
              + facture.getLignes().get(i).getPrixU_net_ligne_9() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getDevise_facture_17()
              + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP
              + facture.getTauxTVA_2() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLignes().get(i).getPrixU_net_ligne_9()
              + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLignes().get(i).getLibelle_produit_10()
              + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLignes().get(i).getMontantHT() + ParametresFluxInit.EDI_FIN_CHAMP
              + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP
              + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP
              + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + facture.getLignes().get(i).getNumero_ligne_2()
              + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + facture.getCommande_client_2()
              + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_LIGNE);
    }
    
    // Pied de commande
    retour.append(facture.getDECLA_PIE() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getMontantTotalHT_2()
        + ParametresFluxInit.EDI_FIN_CHAMP + facture.getMontantTotalTVA_3() + ParametresFluxInit.EDI_FIN_CHAMP
        + facture.getMontantTotalTTC_4() + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_LIGNE);
    
    // Les TVA
    if (facture.getTvas() != null && facture.getTvas().size() > 0) {
      for (int i = 0; i < facture.getTvas().size(); i++) {
        retour.append(facture.getDECLA_TVA() + ParametresFluxInit.EDI_FIN_CHAMP + facture.getTvas().get(i).getTaux()
            + ParametresFluxInit.EDI_FIN_CHAMP + facture.getMontantTotalHT_2() + ParametresFluxInit.EDI_FIN_CHAMP
            + facture.getTvas().get(i).getMontant() + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_LIGNE);
      }
    }
    
    retour.append("END" + ParametresFluxInit.EDI_FIN_LIGNE);
    retour.append("@ND");
    
    return retour.toString();
  }
  
  /**
   * Récupérer la facture Série N et l'attribuer à l'objet EDI correspondant
   */
  private FactureEDI_GP recupererLaCommandeSN(FluxMagento record) {
    if (record == null || record.getFLETB() == null || record.getFLCOD() == null) {
      majError("(GM_Export_Factu_EDI] recupererLaCommandeSN() record NULL ou corrompu");
      return null;
    }
    FactureEDI_GP retour = null;
    
    String etb = null;
    String numero = null;
    // on découpe le code moisi ONE6000001
    if (record.getFLCOD().length() == 10) {
      etb = record.getFLCOD().substring(0, 3);
      numero = record.getFLCOD().substring(3, 10);
      
      if (etb == null || !etb.equals(record.getFLETB())) {
        majError("(GM_Export_Factu_EDI] recupererLaCommandeSN() les établissements sont différents " + record.getFLETB() + "/" + etb);
        return null;
      }
      if ((numero == null || numero.trim().length() != 7)) {
        majError("(GM_Export_Factu_EDI] recupererLaCommandeSN() le numero de commande est corrompu" + numero);
        return null;
      }
    }
    else {
      majError("(GM_Export_Factu_EDI] recupererLaCommandeSN() FLCOD (commande SN) pas à la bonne taille " + record.getFLCOD());
      return null;
    }
    
    ArrayList<GenericRecord> liste = queryManager.select("" + " SELECT E1ETB,E1NUM,E1SUF,E1NFA,E1CCT,E1ETA,E1CRE,E1EXP,E1FAC,E1DLP,E1CLLP"
        + ",E1CLLS,E1CLFP,E1CLFS,E1RG2,E1EC2,E1THTL,E1TTC,(E1TL1+E1TL2+E1TL3) AS TTTVA," + "E1TL1,E1TL2,E1TL3,E1TV1,E1TV2,E1TV3 "
        + " FROM " + queryManager.getLibrary() + ".PGVMEBCM " + " WHERE E1COD = 'E' AND E1ETB ='" + etb + "' AND E1NFA = '" + numero
        + "' ");
    
    if (liste != null && liste.size() == 1) {
      GenericRecord recordTemp = liste.get(0);
      if (!recordTemp.isPresentField("E1ETB") || !recordTemp.isPresentField("E1NUM") || !recordTemp.isPresentField("E1SUF")) {
        majError("[GM_Export_Factu_EDI] recupererLaCommandeSN() GenericRecord recordTemp E1ETB, E1NUM ou E1SUF NULL ");
        return null;
      }
      
      // On crée l'entête de l'expédition et on laisse l'objet gérer sa validité
      FactureEDI_GP facture = new FactureEDI_GP(parametresFlux, recordTemp, queryManager);
      if (facture.isValide()) {
        liste = queryManager.select(
            " " + " SELECT L1NLI,L1ART,DIGITS(A1GCD) AS A1GCD,A1LIB,L1QTE,L1UNV,L1PVN,L1TVA,L1MHT " + " FROM " + queryManager.getLibrary()
                + ".PGVMLBCM " + " LEFT JOIN " + queryManager.getLibrary() + ".PGVMARTM " + " ON A1ETB = L1ETB AND A1ART = L1ART "
                + " WHERE L1ERL IN ('C' , 'S') AND L1ETB = '" + liste.get(0).getField("E1ETB").toString().trim()
                + "' AND L1COD ='E' AND L1NUM='" + liste.get(0).getField("E1NUM").toString().trim() + "' AND L1SUF = '"
                + liste.get(0).getField("E1SUF").toString().trim() + "' ");
        if (liste != null && liste.size() > 0) {
          // On met à jour les lignes de cette commande
          facture.majDesLignes(liste);
          if (facture.isValide()) {
            retour = facture;
          }
          else {
            majError("[GM_Export_Factu_EDI] recupererLaCommandeSN() les lignes sont en erreur " + record.getFLCOD() + ": "
                + facture.getMsgError());
          }
        }
        else {
          majError("[GM_Export_Factu_EDI] recupererLaCommandeSN() les lignes sont nulles ou vides " + record.getFLCOD());
        }
      }
      else {
        majError("[GM_Export_Factu_EDI] recupererLaCommandeSN() facture invalide " + record.getFLCOD() + ": " + facture.getMsgError());
      }
    }
    else {
      majError("(GM_Export_Factu_EDI] Pb de récupération de la commande " + queryManager.getMsgError());
    }
    
    return retour;
  }
  
  /**
   * Envoyer le INVOIC sur le serveur FTP
   */
  private boolean envoyerLeFichierFTP(File fichier) {
    if (fichier == null || !fichier.isFile()) {
      majError("[GM_Export_Factu_EDI] envoyerLeFichierFTP() fichier NULL ou invalide ");
      return false;
    }
    
    // Etape 1 récupérer les commandes sur le serveur FTP @GP
    if (serveurFTP == null) {
      serveurFTP = new FTPManager();
    }
    
    boolean retour = false;
    
    if (serveurFTP.connexion(parametresFlux.getUrl_ftp_EDI(), parametresFlux.getLogin_ftp_EDI(), parametresFlux.getPassword_ftp_EDI())) {
      try {
        if (serveurFTP.getConnexion().changeWorkingDirectory(parametresFlux.getDossierFact_ftp_EDI())) {
          retour = serveurFTP.uploadFile(fichier.getAbsolutePath(), fichier.getName());
        }
        else {
          majError("[GM_Export_Factu_EDI] envoyerLeFichierFTP()  PB changeWorkingDirectory ");
        }
      }
      catch (Exception e) {
        majError("[GM_Export_Factu_EDI] envoyerLeFichierFTP() connexion invalide " + e.getMessage());
      }
      
      serveurFTP.disconnect();
    }
    else {
      majError("[GM_Export_Factu_EDI] envoyerLeFichierFTP()  " + serveurFTP.getMsgError());
    }
    
    return retour;
  }
  
  @Override
  public void dispose() {
    // TODO Stub de la méthode généré automatiquement
    
  }
  
}
