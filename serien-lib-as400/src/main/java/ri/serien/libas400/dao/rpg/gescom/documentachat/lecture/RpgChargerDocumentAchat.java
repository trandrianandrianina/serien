/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.lecture;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.dao.sql.gescom.documentachat.SqlDocumentAchat;
import ri.serien.libas400.dao.sql.gescom.lien.SqlLienDocument;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEtatDocument;
import ri.serien.libcommun.gescom.achat.document.EnumEtapeExtraction;
import ri.serien.libcommun.gescom.achat.document.EnumTypeCommande;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.Reglements;
import ri.serien.libcommun.gescom.achat.document.ValeurInitialeCommandeAchat;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.document.ListeTva;
import ri.serien.libcommun.gescom.commun.document.ListeZonePersonnalisee;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.IdAcheteur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.reglement.IdModeReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.ModeReglement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgChargerDocumentAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGAM0006";
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public RpgChargerDocumentAchat(QueryManager pQuerymg) {
    querymg = pQuerymg;
  }
  
  /**
   * Appel du programme RPG qui va lire l'entête du document.
   */
  public DocumentAchat chargerDocumentAchat(SystemeManager pSysteme, IdDocumentAchat pIdDocumentAchat, DocumentAchat pDocumentAchat,
      boolean avecHistorique) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentAchat.controlerId(pIdDocumentAchat, true);
    
    // Préparation des paramètres du programme RPG
    Svgam0006i entree = new Svgam0006i();
    Svgam0006o sortie = new Svgam0006o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    if (avecHistorique) {
      entree.setPiind("0000000000");
    }
    else {
      entree.setPiind("1000000000");
    }
    entree.setPietb(pIdDocumentAchat.getCodeEtablissement());
    entree.setPicod(pIdDocumentAchat.getCodeEntete().getCode());
    entree.setPinum(pIdDocumentAchat.getNumero());
    entree.setPisuf(pIdDocumentAchat.getSuffixe());
    // Envoi de l'option MAJ pour verrouillage du document d'achat
    if (pDocumentAchat != null && pDocumentAchat.isEnCoursModification()) {
      entree.setPiopt("MAJ");
    }
    else {
      entree.setPiopt("");
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      // Entrée
      // Sortie
      if (pDocumentAchat == null) {
        pDocumentAchat = new DocumentAchat(pIdDocumentAchat);
      }
      else {
        pDocumentAchat.setId(pIdDocumentAchat);
      }
      pDocumentAchat.setTopSysteme(sortie.getEatop());
      pDocumentAchat.setDateCreation(sortie.getEacreConvertiEnDate());
      pDocumentAchat.setDateDernierTraitement(sortie.getEadatConvertiEnDate());
      pDocumentAchat.setDateHomologation(sortie.getEahomConvertiEnDate());
      pDocumentAchat.setDateReception(sortie.getEarecConvertiEnDate());
      pDocumentAchat.setDateFacturation(sortie.getWeafacConvertiEnDate());
      pDocumentAchat.setNumeroFacture(sortie.getEanfa());
      // Non renseignée car non utilisés pour l'instant
      // EANLS
      // EAPLI
      // EADLI
      // EANLI
      // EATLV
      // EATLL
      // EADSU
      pDocumentAchat.setReferenceInterne(sortie.getEarfl());
      pDocumentAchat.setCodeEtat(EnumCodeEtatDocument.valueOfByCode(sortie.getEaeta()));
      pDocumentAchat.setTopEdition(sortie.getEaedt());
      pDocumentAchat.setTopFournisseurDeCommande(sortie.getEatfc());
      pDocumentAchat.setTopMontantsFactureForces(sortie.getEatt());
      pDocumentAchat.setRangReglement(sortie.getEanrg());
      // Les Tva
      ListeTva listeTva = pDocumentAchat.getListeTva();
      listeTva.setNumeroTVA1(sortie.getEatva1());
      listeTva.setNumeroTVA2(sortie.getEatva2());
      listeTva.setNumeroTVA3(sortie.getEatva3());
      listeTva.setMontantTVA1(sortie.getEatl1());
      listeTva.setMontantTVA2(sortie.getEatl2());
      listeTva.setMontantTVA3(sortie.getEatl3());
      listeTva.setMontantSoumisTVA1(sortie.getEasl1());
      listeTva.setMontantSoumisTVA2(sortie.getEasl2());
      listeTva.setMontantSoumisTVA3(sortie.getEasl3());
      listeTva.setTauxTVA1(sortie.getEatv1());
      listeTva.setTauxTVA2(sortie.getEatv2());
      listeTva.setTauxTVA3(sortie.getEatv3());
      
      pDocumentAchat.setTopReleve(sortie.getEarlv());
      pDocumentAchat.setNombreExemplaire(sortie.getEanex());
      pDocumentAchat.setNombreReglementsReels(sortie.getEanrr());
      pDocumentAchat.setSigneGere(sortie.getEasgn());
      pDocumentAchat.setTopLivraison(sortie.getEaliv());
      pDocumentAchat.setTopComptabilisation(sortie.getEacpt());
      pDocumentAchat.setTopPrixGaranti(sortie.getEapgc());
      pDocumentAchat.setTopChiffrage(sortie.getEachf());
      pDocumentAchat.setTopAffectation(sortie.getEaina());
      IdEtablissement idEtablissement = IdEtablissement.getInstance(sortie.getEaetb());
      IdFournisseur id = IdFournisseur.getInstance(idEtablissement, sortie.getEacol(), sortie.getEafrs());
      pDocumentAchat.setIdFournisseur(id);
      pDocumentAchat.setDateLivraisonPrevue(sortie.getEadlpConvertiEnDate());
      pDocumentAchat.setCodeAvoir(sortie.getEaavr());
      pDocumentAchat.setCodeTypeFacturation(sortie.getEatfa());
      pDocumentAchat.setCodeNatureAchat(sortie.getEanat());
      pDocumentAchat.setPourcentageEscompte(sortie.getEaesc());
      
      // Règlements
      Reglements reglements = pDocumentAchat.getReglement();
      reglements.setDateEcheance1(sortie.getEae1gConvertiEnDate());
      reglements.setDateEcheance2(sortie.getEae2gConvertiEnDate());
      reglements.setDateEcheance3(sortie.getEae3gConvertiEnDate());
      reglements.setDateEcheance4(sortie.getEae4gConvertiEnDate());
      reglements.setMontant1(sortie.getEar1g());
      reglements.setMontant2(sortie.getEar2g());
      reglements.setMontant3(sortie.getEar3g());
      reglements.setMontant4(sortie.getEar4g());
      if (sortie.getEarg1().trim().isEmpty()) {
        reglements.setModeReglement1(null);
      }
      else {
        reglements.setModeReglement1(new ModeReglement(IdModeReglement.getInstance(sortie.getEarg1())));
      }
      if (sortie.getEarg2().trim().isEmpty()) {
        reglements.setModeReglement2(null);
      }
      else {
        reglements.setModeReglement2(new ModeReglement(IdModeReglement.getInstance(sortie.getEarg2())));
      }
      if (sortie.getEarg3().trim().isEmpty()) {
        reglements.setModeReglement3(null);
      }
      else {
        reglements.setModeReglement3(new ModeReglement(IdModeReglement.getInstance(sortie.getEarg3())));
      }
      if (sortie.getEarg4().trim().isEmpty()) {
        reglements.setModeReglement4(null);
      }
      else {
        reglements.setModeReglement4(new ModeReglement(IdModeReglement.getInstance(sortie.getEarg4())));
      }
      reglements.setCodeEcheance1(sortie.getEaec1());
      reglements.setCodeEcheance2(sortie.getEaec2());
      reglements.setCodeEcheance3(sortie.getEaec3());
      reglements.setCodeEcheance4(sortie.getEaec4());
      
      // Attention ces valeurs peutvent être modifié en bas de cette méthode
      pDocumentAchat.setTotalHTLignes(sortie.getEathtl());
      pDocumentAchat.setTotalTTC(sortie.getEattc());
      pDocumentAchat.setMontantEscompte(sortie.getEames());
      if (!sortie.getEamag().trim().isEmpty()) {
        pDocumentAchat.setIdMagasinSortie(IdMagasin.getInstance(idEtablissement, sortie.getEamag()));
      }
      pDocumentAchat.setReferenceBonLivraison(sortie.getEarbl());
      pDocumentAchat.setCodeSectionAnalytique(sortie.getEasan());
      pDocumentAchat.setCodeAffaire(sortie.getEaact());
      pDocumentAchat.setCodeLieuLivraison(sortie.getEallv());
      if (!sortie.getEaach().trim().isEmpty()) {
        IdAcheteur idAcheteur = IdAcheteur.getInstance(pIdDocumentAchat.getIdEtablissement(), sortie.getEaach());
        pDocumentAchat.setIdAcheteur(idAcheteur);
      }
      pDocumentAchat.setCodeJournalAchat(sortie.getEacja());
      pDocumentAchat.setCodeDevise(sortie.getEadev());
      pDocumentAchat.setTauxDeChange(sortie.getEachg());
      pDocumentAchat.setCoefficientAchat(sortie.getEacpr());
      pDocumentAchat.setBaseDevise(sortie.getEabas());
      pDocumentAchat.setCodeDossierApprovisionnement(sortie.getEados());
      pDocumentAchat.setCodeContainer(sortie.getEacnt());
      pDocumentAchat.setPoids(sortie.getEapds());
      pDocumentAchat.setVolume(sortie.getEavol());
      pDocumentAchat.setMontantFraisRepartir(sortie.getEamta());
      
      // Remises ligne
      if (pDocumentAchat.getListeRemiseLigne() == null) {
        pDocumentAchat.setListeRemiseLigne(new ListeRemise());
      }
      ListeRemise listeRemiseLigne = pDocumentAchat.getListeRemiseLigne();
      listeRemiseLigne.setPourcentageRemise1(sortie.getEarem1());
      listeRemiseLigne.setPourcentageRemise2(sortie.getEarem2());
      listeRemiseLigne.setPourcentageRemise3(sortie.getEarem3());
      listeRemiseLigne.setPourcentageRemise4(sortie.getEarem4());
      listeRemiseLigne.setPourcentageRemise5(sortie.getEarem5());
      listeRemiseLigne.setPourcentageRemise6(sortie.getEarem6());
      listeRemiseLigne.setTypeRemise(sortie.getEatrl());
      listeRemiseLigne.setBaseRemise(sortie.getEabrl());
      
      // Remises pied
      if (pDocumentAchat.getListeRemisePied() == null) {
        pDocumentAchat.setListeRemisePied(new ListeRemise());
      }
      ListeRemise listeRemisePied = pDocumentAchat.getListeRemisePied();
      listeRemisePied.setPourcentageRemise1(sortie.getEarp1());
      listeRemisePied.setPourcentageRemise2(sortie.getEarp2());
      listeRemisePied.setPourcentageRemise3(sortie.getEarp3());
      listeRemisePied.setPourcentageRemise4(sortie.getEarp4());
      listeRemisePied.setPourcentageRemise5(sortie.getEarp5());
      listeRemisePied.setPourcentageRemise6(sortie.getEarp6());
      listeRemisePied.setTypeRemise(sortie.getEatrp());
      
      // Top personalisable
      if (pDocumentAchat.getListeTopPersonnalisable() == null) {
        pDocumentAchat.setListeTopPersonnalisable(new ListeZonePersonnalisee());
      }
      ListeZonePersonnalisee listeTopPersonnalisable = pDocumentAchat.getListeTopPersonnalisable();
      listeTopPersonnalisable.setTopPersonnalisation1(sortie.getEatp1());
      listeTopPersonnalisable.setTopPersonnalisation2(sortie.getEatp2());
      listeTopPersonnalisable.setTopPersonnalisation3(sortie.getEatp3());
      listeTopPersonnalisable.setTopPersonnalisation4(sortie.getEatp4());
      listeTopPersonnalisable.setTopPersonnalisation5(sortie.getEatp5());
      
      pDocumentAchat.setCodeLitige(sortie.getEain1());
      // EAIN2
      if (sortie.getEain3() == ' ') {
        pDocumentAchat.setReceptionDefinitive(false);
      }
      else {
        pDocumentAchat.setReceptionDefinitive(true);
      }
      if (sortie.getEain4() == ' ') {
        pDocumentAchat.setComptabilisationEnStocksFlottants(false);
      }
      else {
        pDocumentAchat.setComptabilisationEnStocksFlottants(true);
      }
      if (sortie.getEain5() == ' ') {
        pDocumentAchat.setBlocageComptabilisation(false);
      }
      else {
        pDocumentAchat.setBlocageComptabilisation(true);
      }
      pDocumentAchat.setCodeContrat(sortie.getEacct());
      pDocumentAchat.setDateConfirmationFournisseur(sortie.getEadat1ConvertiEnDate());
      pDocumentAchat.setReferenceCommande(sortie.getEarbc());
      pDocumentAchat.setDateIntermediaire1(sortie.getEaint1ConvertiEnDate());
      pDocumentAchat.setDateIntermediaire2(sortie.getEaint2ConvertiEnDate());
      pDocumentAchat.setCodeEtatIntermediaire(sortie.getEaetai());
      pDocumentAchat.setCodeModeExpedition(sortie.getEamex());
      pDocumentAchat.setCodeTransporteur(sortie.getEactr());
      if (sortie.getEadili() == ' ') {
        pDocumentAchat.setTraiteParCommandeDilicom(false);
      }
      else {
        pDocumentAchat.setTraiteParCommandeDilicom(true);
      }
      if (sortie.getEain6() == ' ') {
        pDocumentAchat.setBlocageCommande(false);
      }
      else {
        pDocumentAchat.setBlocageCommande(true);
      }
      pDocumentAchat.setCodeScenarioDates(sortie.getEain7());
      pDocumentAchat.setCodeEtatFraisFixes(sortie.getEain8());
      pDocumentAchat.setCodeTypeCommande(EnumTypeCommande.valueOfByCode(sortie.getEain9()));
      pDocumentAchat.setMontantPortTheorique(sortie.getEapor0());
      pDocumentAchat.setMontantPortFacture(sortie.getEapor1());
      // Si la valeur vaut "blanc" alors on facture les frais de port
      if (sortie.getEain10().equals(' ')) {
        pDocumentAchat.setFraisPortFournisseurFacture(true);
      }
      else {
        pDocumentAchat.setFraisPortFournisseurFacture(false);
      }
      
      // Le bloc adresse fournisseur
      if (pDocumentAchat.getAdresseFournisseur() == null) {
        pDocumentAchat.setAdresseFournisseur(new Adresse());
      }
      Adresse adresse = pDocumentAchat.getAdresseFournisseur();
      adresse.setNom(sortie.getPonom2());
      adresse.setComplementNom(sortie.getPocpl2());
      adresse.setRue(sortie.getPorue2());
      adresse.setLocalisation(sortie.getPoloc2());
      adresse.setCodePostal(Constantes.convertirTexteEnInteger(sortie.getPocdp2()));
      adresse.setVille(sortie.getPovil2());
      
      if (sortie.getPoenv() == 'E') {
        pDocumentAchat.setModeEnlevement(true);
      }
      else {
        pDocumentAchat.setModeEnlevement(false);
      }
      
      pDocumentAchat.setEtapeExtraction(EnumEtapeExtraction.valueOfByCode(sortie.getPoinex()));
      pDocumentAchat.setCodeTypePrixAchat(sortie.getPotyppx());
      
      // Le bloc adresse de livraison de la commande client éventuelle:
      // - si commande normale et mode livraison
      // - si commande direct usine mode livraison (adresse de livraison) ou mode enlèvement (adresse du client)
      if (pDocumentAchat.isModeLivraison() || (pDocumentAchat.isDirectUsine() && sortie.getEallv().trim().equals("*GVM"))) {
        if (pDocumentAchat.getAdresseLivraison() == null) {
          pDocumentAchat.setAdresseLivraison(new Adresse());
        }
        Adresse adresseLivraison = pDocumentAchat.getAdresseLivraison();
        adresseLivraison.setNom(sortie.getPonom3());
        adresseLivraison.setComplementNom(sortie.getPocpl3());
        adresseLivraison.setRue(sortie.getPorue3());
        adresseLivraison.setLocalisation(sortie.getPoloc3());
        adresseLivraison.setCodePostal(Constantes.convertirTexteEnInteger(sortie.getPocdp3()));
        adresseLivraison.setVille(sortie.getPovil3());
      }
      
      // Recherche l'ensemble des commandes d'origine si le RPG retourne quelque chose
      // (le RPG n'en retourne qu'un seul alors qu'il peut y en avoir plusieurs d'où la requête SQL)
      if (sortie.getEanum0() > 0) {
        SqlLienDocument sqlLienDocument = new SqlLienDocument(querymg);
        pDocumentAchat.setListeIdDocumentOrigine(sqlLienDocument.chargerListeIdCommandeOrigine(pIdDocumentAchat));
      }
      
      // Suite à la modification de philosophie des documents d'achat sur les quantités d'origine, certaines valeurs retournées par
      // le service sont fausses pour les commandes d'achats.
      // Cette méthode est appelée pour corriger ces valeurs, ne pas chercher à optimiser sur le contenu de TotalHTLignes.
      if (pDocumentAchat.isCommande()) {
        SqlDocumentAchat sqlDocumentAchat = new SqlDocumentAchat(querymg);
        ValeurInitialeCommandeAchat valeurInitialeCommandeAchat = sqlDocumentAchat.chargerDonneesCommandeInitiale(pDocumentAchat.getId());
        if (valeurInitialeCommandeAchat != null) {
          pDocumentAchat.setTotalHTLignes(valeurInitialeCommandeAchat.getMontantHT());
          pDocumentAchat.setTotalTTC(valeurInitialeCommandeAchat.getMontantTTC());
        }
      }
    }
    
    return pDocumentAchat;
  }
}
