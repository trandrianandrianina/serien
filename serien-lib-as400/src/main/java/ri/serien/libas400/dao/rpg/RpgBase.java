/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

public class RpgBase {
  // Constantes
  public static final int CODE_ERREUR_NON_REFERENCEE = -1;
  public static final char PAS_ERREUR = ' ';
  public static final char EN_ERREUR = '9';
  
  // Variables
  protected String texteMessage = "";
  protected ParametreGeneriqueErreur erreur = new ParametreGeneriqueErreur();
  public ErreurServiceRPG[] listeProbleme = null;
  
  /**
   * Controle des erreurs.
   */
  protected void controlerErreur() {
    // Contrôle si une erreur ou un warning est détecté
    if (erreur.peniv.trim().length() == 0) {
      return;
    }
    
    // Si un problème est détecté, on récupère les codes et messages
    int tailleTableauErreurs = erreur.peniv.trim().length();
    listeProbleme = new ErreurServiceRPG[tailleTableauErreurs];
    for (int i = 0; i < tailleTableauErreurs; i++) {
      listeProbleme[i] = new ErreurServiceRPG();
      if (erreur.peniv.charAt(i) == EN_ERREUR) {
        listeProbleme[i].setCode(ErreurServiceRPG.ERREUR);
      }
      
      switch (i) {
        case 0:
          listeProbleme[i].setLibelle(erreur.peerr1.trim());
          break;
        case 1:
          listeProbleme[i].setLibelle(erreur.peerr2.trim());
          break;
        case 2:
          listeProbleme[i].setLibelle(erreur.peerr3.trim());
          break;
        case 3:
          listeProbleme[i].setLibelle(erreur.peerr4.trim());
          break;
        case 4:
          listeProbleme[i].setLibelle(erreur.peerr5.trim());
          break;
        case 5:
          listeProbleme[i].setLibelle(erreur.peerr6.trim());
          break;
        case 6:
          listeProbleme[i].setLibelle(erreur.peerr7.trim());
          break;
        case 7:
          listeProbleme[i].setLibelle(erreur.peerr8.trim());
          break;
        case 8:
          listeProbleme[i].setLibelle(erreur.peerr9.trim());
          break;
        case 9:
          listeProbleme[i].setLibelle(erreur.peerr10.trim());
          break;
      }
    }
    // Controle de la gravité du problème afin de savoir si on throw ou pas une exception
    texteMessage = getLibellesProblemes();
    throw new MessageErreurException(texteMessage);
  }
  
  private String getLibellesProblemes() {
    String listeLibellesProbleme = "";
    for (int i = 0; i < listeProbleme.length; i++) {
      // On concatène les messages d'erreurs
      if (!listeProbleme[i].getLibelle().trim().equals("")) {
        listeLibellesProbleme += listeProbleme[i].getLibelle() + "\n";
      }
      Trace.debug(RpgBase.class, "getLibellesProblemes", "code", listeProbleme[i].getCode(), "texte", listeProbleme[i].getLibelle());
    }
    return listeLibellesProbleme;
  }
  
}
