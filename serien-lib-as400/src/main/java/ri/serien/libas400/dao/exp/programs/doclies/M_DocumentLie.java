/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.programs.doclies;

import ri.serien.libas400.dao.exp.database.files.FFD_Psempdmm;
import ri.serien.libas400.database.QueryManager;

/**
 * Classe contenant les zones du PSEMPDMM
 * @author Administrateur
 * 
 */
public class M_DocumentLie extends FFD_Psempdmm {
  // Variables de travail
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public M_DocumentLie(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Insère l'enregistrement dans le table
   * @return
   * 
   */
  @Override
  public boolean insertInDatabase() {
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert("PSEMPDMM", querymg.getLibrary());
    return request(requete);
  }
  
  /**
   * Modifie l'enregistrement dans le table
   * @return
   * 
   */
  @Override
  public boolean updateInDatabase() {
    initGenericRecord(genericrecord, false);
    String requete =
        genericrecord.createSQLRequestUpdate("PSEMPDMM", querymg.getLibrary(), "PDDOC=" + getPDDOC() + " and PDFIC='" + getPDFIC() + "'");
    return request(requete);
  }
  
  /**
   * Suppression de l'enregistrement courant
   * @return
   * 
   */
  @Override
  public boolean deleteInDatabase() {
    String requete = "delete from " + querymg.getLibrary() + ".PSEMPDMM where PDDOC=" + getPDDOC() + " and PDFIC='" + getPDFIC() + "'";
    return request(requete);
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  // -- Accesseurs ----------------------------------------------------------
  
}
