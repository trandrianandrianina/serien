/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0018i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINFA = 7;
  public static final int DECIMAL_PINFA = 0;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIOPT = 3;
  public static final int SIZE_PIEDT = 3;
  public static final int SIZE_PIETA0 = 1;
  public static final int DECIMAL_PIETA0 = 0;
  public static final int SIZE_PITTC0 = 13;
  public static final int DECIMAL_PITTC0 = 2;
  public static final int SIZE_PINOM = 30;
  public static final int SIZE_PISTK = 1;
  public static final int SIZE_PITDSK = 2;
  public static final int DECIMAL_PITDSK = 0;
  public static final int SIZE_PIIDDC = 11;
  public static final int SIZE_PINML = 9;
  public static final int DECIMAL_PINML = 0;
  public static final int SIZE_PINFL = 9;
  public static final int DECIMAL_PINFL = 0;
  public static final int SIZE_PIIN3 = 1;
  public static final int SIZE_PIIDCL = 12;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 131;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINFA = 5;
  public static final int VAR_PIDAT = 6;
  public static final int VAR_PIOPT = 7;
  public static final int VAR_PIEDT = 8;
  public static final int VAR_PIETA0 = 9;
  public static final int VAR_PITTC0 = 10;
  public static final int VAR_PINOM = 11;
  public static final int VAR_PISTK = 12;
  public static final int VAR_PITDSK = 13;
  public static final int VAR_PIIDDC = 14;
  public static final int VAR_PINML = 15;
  public static final int VAR_PINFL = 16;
  public static final int VAR_PIIN3 = 17;
  public static final int VAR_PIIDCL = 18;
  public static final int VAR_PIARR = 19;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "D","E",X","9" *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal pinfa = BigDecimal.ZERO; // Numéro de facture
  private BigDecimal pidat = BigDecimal.ZERO; // Date de traitement
  private String piopt = ""; // Option de traitement *
  private String piedt = ""; // Option d édition *
  private BigDecimal pieta0 = BigDecimal.ZERO; // Etat avant traitement
  private BigDecimal pittc0 = BigDecimal.ZERO; // Montant avant
  private String pinom = ""; // Nom pour mvmt de stock
  private String pistk = ""; // Stockage base documentaire
  private BigDecimal pitdsk = BigDecimal.ZERO; // Type du document stocké
  private String piiddc = ""; // Indicatif du DOCUMENT (LIEN)
  private BigDecimal pinml = BigDecimal.ZERO; // Numéro id mail
  private BigDecimal pinfl = BigDecimal.ZERO; // Numéro id mail pour le fax
  private String piin3 = ""; // BL chiffré oui/non
  private String piidcl = ""; // Indicatif du CLIENT (LIEN)
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9" *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400ZonedDecimal(SIZE_PINFA, DECIMAL_PINFA), // Numéro de facture
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date de traitement
      new AS400Text(SIZE_PIOPT), // Option de traitement *
      new AS400Text(SIZE_PIEDT), // Option d édition *
      new AS400ZonedDecimal(SIZE_PIETA0, DECIMAL_PIETA0), // Etat avant traitement
      new AS400ZonedDecimal(SIZE_PITTC0, DECIMAL_PITTC0), // Montant avant
      new AS400Text(SIZE_PINOM), // Nom pour mvmt de stock
      new AS400Text(SIZE_PISTK), // Stockage base documentaire
      new AS400ZonedDecimal(SIZE_PITDSK, DECIMAL_PITDSK), // Type du document stocké
      new AS400Text(SIZE_PIIDDC), // Indicatif du DOCUMENT (LIEN)
      new AS400ZonedDecimal(SIZE_PINML, DECIMAL_PINML), // Numéro id mail
      new AS400ZonedDecimal(SIZE_PINFL, DECIMAL_PINFL), // Numéro id mail pour le fax
      new AS400Text(SIZE_PIIN3), // BL chiffré oui/non
      new AS400Text(SIZE_PIIDCL), // Indicatif du CLIENT (LIEN)
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, picod, pietb, pinum, pisuf, pinfa, pidat, piopt, piedt, pieta0, pittc0, pinom, pistk, pitdsk, piiddc, pinml,
          pinfl, piin3, piidcl, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinfa = (BigDecimal) output[5];
    pidat = (BigDecimal) output[6];
    piopt = (String) output[7];
    piedt = (String) output[8];
    pieta0 = (BigDecimal) output[9];
    pittc0 = (BigDecimal) output[10];
    pinom = (String) output[11];
    pistk = (String) output[12];
    pitdsk = (BigDecimal) output[13];
    piiddc = (String) output[14];
    pinml = (BigDecimal) output[15];
    pinfl = (BigDecimal) output[16];
    piin3 = (String) output[17];
    piidcl = (String) output[18];
    piarr = (String) output[19];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinfa(BigDecimal pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = pPinfa.setScale(DECIMAL_PINFA, RoundingMode.HALF_UP);
  }
  
  public void setPinfa(Integer pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(pPinfa);
  }
  
  public void setPinfa(Date pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pPinfa));
  }
  
  public Integer getPinfa() {
    return pinfa.intValue();
  }
  
  public Date getPinfaConvertiEnDate() {
    return ConvertDate.db2ToDate(pinfa.intValue(), null);
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPiopt(String pPiopt) {
    if (pPiopt == null) {
      return;
    }
    piopt = pPiopt;
  }
  
  public String getPiopt() {
    return piopt;
  }
  
  public void setPiedt(String pPiedt) {
    if (pPiedt == null) {
      return;
    }
    piedt = pPiedt;
  }
  
  public String getPiedt() {
    return piedt;
  }
  
  public void setPieta0(BigDecimal pPieta0) {
    if (pPieta0 == null) {
      return;
    }
    pieta0 = pPieta0.setScale(DECIMAL_PIETA0, RoundingMode.HALF_UP);
  }
  
  public void setPieta0(Integer pPieta0) {
    if (pPieta0 == null) {
      return;
    }
    pieta0 = BigDecimal.valueOf(pPieta0);
  }
  
  public Integer getPieta0() {
    return pieta0.intValue();
  }
  
  public void setPittc0(BigDecimal pPittc0) {
    if (pPittc0 == null) {
      return;
    }
    pittc0 = pPittc0.setScale(DECIMAL_PITTC0, RoundingMode.HALF_UP);
  }
  
  public void setPittc0(Double pPittc0) {
    if (pPittc0 == null) {
      return;
    }
    pittc0 = BigDecimal.valueOf(pPittc0).setScale(DECIMAL_PITTC0, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPittc0() {
    return pittc0.setScale(DECIMAL_PITTC0, RoundingMode.HALF_UP);
  }
  
  public void setPinom(String pPinom) {
    if (pPinom == null) {
      return;
    }
    pinom = pPinom;
  }
  
  public String getPinom() {
    return pinom;
  }
  
  public void setPistk(Character pPistk) {
    if (pPistk == null) {
      return;
    }
    pistk = String.valueOf(pPistk);
  }
  
  public Character getPistk() {
    return pistk.charAt(0);
  }
  
  public void setPitdsk(BigDecimal pPitdsk) {
    if (pPitdsk == null) {
      return;
    }
    pitdsk = pPitdsk.setScale(DECIMAL_PITDSK, RoundingMode.HALF_UP);
  }
  
  public void setPitdsk(Integer pPitdsk) {
    if (pPitdsk == null) {
      return;
    }
    pitdsk = BigDecimal.valueOf(pPitdsk);
  }
  
  public Integer getPitdsk() {
    return pitdsk.intValue();
  }
  
  public void setPiiddc(String pPiiddc) {
    if (pPiiddc == null) {
      return;
    }
    piiddc = pPiiddc;
  }
  
  public String getPiiddc() {
    return piiddc;
  }
  
  public void setPinml(BigDecimal pPinml) {
    if (pPinml == null) {
      return;
    }
    pinml = pPinml.setScale(DECIMAL_PINML, RoundingMode.HALF_UP);
  }
  
  public void setPinml(Integer pPinml) {
    if (pPinml == null) {
      return;
    }
    pinml = BigDecimal.valueOf(pPinml);
  }
  
  public Integer getPinml() {
    return pinml.intValue();
  }
  
  public void setPinfl(BigDecimal pPinfl) {
    if (pPinfl == null) {
      return;
    }
    pinfl = pPinfl.setScale(DECIMAL_PINFL, RoundingMode.HALF_UP);
  }
  
  public void setPinfl(Integer pPinfl) {
    if (pPinfl == null) {
      return;
    }
    pinfl = BigDecimal.valueOf(pPinfl);
  }
  
  public Integer getPinfl() {
    return pinfl.intValue();
  }
  
  public void setPiin3(Character pPiin3) {
    if (pPiin3 == null) {
      return;
    }
    piin3 = String.valueOf(pPiin3);
  }
  
  public Character getPiin3() {
    return piin3.charAt(0);
  }
  
  public void setPiidcl(String pPiidcl) {
    if (pPiidcl == null) {
      return;
    }
    piidcl = pPiidcl;
  }
  
  public String getPiidcl() {
    return piidcl;
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
