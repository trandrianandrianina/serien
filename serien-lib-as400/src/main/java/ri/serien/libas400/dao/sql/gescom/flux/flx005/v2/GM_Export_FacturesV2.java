/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx005.v2;

import java.io.File;
import java.util.ArrayList;

import com.google.gson.JsonSyntaxException;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.JsonFactureMagentoV2;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.ConvertFileToBytes;

public class GM_Export_FacturesV2 extends BaseGroupDB {
  private ConvertFileToBytes convert = null;
  protected ParametresFluxBibli parametresBibli = null;
  
  public GM_Export_FacturesV2(QueryManager aquerymg, ParametresFluxBibli pParam) {
    super(aquerymg);
    parametresBibli = pParam;
  }
  
  /**
   * On retourne sous forme binaire une facture dans le JSON
   */
  public String retournerUneFactureJSON(FluxMagento pRecord) {
    if (pRecord == null || pRecord.getFLETB() == null || pRecord.getFLCOD() == null) {
      Trace.erreur("[GM_Export_Factures] retournerUneFactureJSON() record null ou corrompu");
      return null;
    }
    
    String retour = null;
    
    String numFacture = isolerNumeroFacture(pRecord, pRecord.getFLCOD().trim());
    if (numFacture == null) {
      return null;
    }
    
    // Contrôler s'il s'agit bien d'une facture existante SERIE N
    String requete = "SELECT E1ETB, E1NFA, E1CCT, E1AVR FROM " + queryManager.getLibrary() + ".PGVMEBCM WHERE  E1COD = 'E' AND E1ETB = '"
        + pRecord.getFLETB() + "' AND E1NFA = '" + numFacture + "' AND (E1ETA = 6 OR E1ETA = 7) ";
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    // Contrôler si cette facture existe bien dans le bon dossier
    if (liste != null && liste.size() == 1) {
      // On contrôle d'abord s'il s'agit bien d'une facture
      if (!controlerTypeDocument(liste.get(0))) {
        pRecord.construireMessageErreur("Ce document est un avoir");
        return "";
      }
      
      String chemin = recupererLeBonCheminFacture(pRecord);
      if (chemin != null) {
        String facture = recupererLeBonNomFacturePDF(pRecord, pRecord.getFLETB(), numFacture);
        if (facture != null) {
          chemin = chemin + facture;
          
          // On checke qu'il s'agisse bien d'un Fichier valide
          File fichierPhysique = new File(chemin);
          if (!fichierPhysique.isFile()) {
            pRecord.construireMessageErreur("[retournerUneFactureJSON] le fichier: " + chemin + " n'existe pas ");
            return null;
          }
          
          if (convert == null) {
            convert = new ConvertFileToBytes();
          }
          try {
            
            retour = convert.encodeFileToBase64Binary(chemin);
            // TODO Il faudra un peu mieux contrôler tout ce bouzin
            if (retour != null && retour.trim().length() > 10) {
              // Si la conversion s'est bien déroulée, créer un nouvel objet FactureMagento pour le JSOniser
              JsonFactureMagentoV2 objetFacture = new JsonFactureMagentoV2(pRecord);
              Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), objetFacture.getEtb());
              if (numeroInstance == null) {
                majError("[GM_Export_Factures] retournerUneFactureJSON() : probleme d'interpretation de l'ETb vers l'instance Magento");
                return null;
              }
              objetFacture.setIdInstanceMagento(numeroInstance);
              
              objetFacture.setNomFacture(facture);
              // TODO Il va falloir contrôler l'existence de cette référence Magento E1CCT avant même d'envoyer la facture
              if (liste.get(0).isPresentField("E1CCT")) {
                objetFacture.setCommandeMagento(liste.get(0).getField("E1CCT").toString().trim());
              }
              
              objetFacture.setFactureClient(retour);
              
              // debut de la construction du JSON
              if (initJSON()) {
                // retour = "";
                if (objetFacture.getNomFacture() != null && objetFacture.getIdInstanceMagento() > 0
                    && objetFacture.getCommandeMagento() != null) {
                  retour = gson.toJson(objetFacture);
                }
                else {
                  pRecord.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() valeurs ESSENTIELLES à NULL");
                }
              }
            }
            else {
              pRecord.construireMessageErreur("[GM_Export_Factures] PB convert: " + retour);
            }
          }
          catch (JsonSyntaxException e) {
            pRecord.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() PB PARSE Objet JSON: " + e.getMessage());
            return null;
          }
        }
      }
      else {
        pRecord.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() PB de recup du chemin de facture SERIE N : " + " / "
            + pRecord.getFLETB() + " / " + pRecord.getFLCOD());
      }
    }
    else {
      pRecord.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() PB de recup de facture SERIE N : " + " / "
          + queryManager.getMsgError() + " / " + requete);
    }
    
    return retour;
  }
  
  /**
   * contrôler le type de document qui est demandé
   * En effet pour Magento nous n'envoyons pas les avoirs
   * Nous allons donc effectuer un contrôle sur la zone E1AVR
   * si blanc FC sinon AV
   */
  protected boolean controlerTypeDocument(GenericRecord record) {
    if (record == null) {
      return false;
    }
    
    return (record.isPresentField("E1AVR") && record.getField("E1AVR").toString().trim().equals(""));
  }
  
  /**
   * On récupère le bon chemin pour accéder à la facture
   */
  protected String recupererLeBonCheminFacture(FluxMagento record) {
    String retour = File.separator + parametresBibli.getRacineIFSeditions() + File.separator + queryManager.getLibrary() + File.separator
        + parametresBibli.getCheminIFSfactures() + File.separator;
    
    return retour;
  }
  
  /**
   * Renvoyer un numero de facture sans l'établissement
   */
  protected String isolerNumeroFacture(FluxMagento pRecord, String factureEtbaliss) {
    if (factureEtbaliss == null) {
      pRecord.construireMessageErreur("[GM_Export_Factures] retaillerCodeFacture() factureEtbaliss à NULL ");
      return factureEtbaliss;
    }
    
    String retour = null;
    
    if (factureEtbaliss.length() > 3) {
      retour = factureEtbaliss.substring(3);
    }
    else {
      pRecord.construireMessageErreur("[GM_Export_Factures] retaillerCodeFacture() longueur erronée de factureEtbaliss:" + factureEtbaliss.length());
    }
    
    return retour;
  }
  
  /**
   * On récupère le bon nom de facture
   */
  protected String recupererLeBonNomFacturePDF(FluxMagento pRecord, String etb, String numero) {
    if (numero == null || etb == null) {
      pRecord.construireMessageErreur("[GM_Export_Factures] retaillerCodeFacture() numero " + numero + " ou etb " + etb + " à NULL ");
      return null;
    }
    
    if (parametresBibli.getPrefixeNomFactures() != null) {
      return etb + parametresBibli.getPrefixeNomFactures() + numero + ".pdf";
    }
    else {
      pRecord.construireMessageErreur("[GM_Export_Factures] isolerNumeroFacture() PREFIXE_NOM_FAC à NULL :");
      return null;
    }
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
}
