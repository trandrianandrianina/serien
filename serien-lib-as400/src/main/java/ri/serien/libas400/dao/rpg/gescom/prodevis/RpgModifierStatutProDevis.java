/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.prodevis;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.vente.prodevis.DescriptionProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.EnumStatutProDevis;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgModifierStatutProDevis extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0053";
  
  /**
   * Appel du programme RPG qui va modifier le statut d'un pro-devis.
   */
  public void modifierStatutProDevis(SystemeManager pSysteme, DescriptionProDevis pDescription, EnumStatutProDevis pNouveauStatut) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0053i entree = new Svgvm0053i();
    Svgvm0053o sortie = new Svgvm0053o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pDescription.getEtablissement().getCodeEtablissement());
    entree.setPikey(pDescription.getCleUnique());
    entree.setPista(pNouveauStatut.getCode());
    if (pDescription.getIdDocumentImporte() != null) {
      entree.setPicdc(pDescription.getIdDocumentImporte().getEntete());
      entree.setPicde(pDescription.getIdDocumentImporte().getIdEtablissement().getCodeEtablissement());
      entree.setPicdn(pDescription.getIdDocumentImporte().getNumero());
      entree.setPicds(pDescription.getIdDocumentImporte().getSuffixe());
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
}
