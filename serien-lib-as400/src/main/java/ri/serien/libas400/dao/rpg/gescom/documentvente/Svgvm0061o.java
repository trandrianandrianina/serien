/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0061o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POCOD = 1;
  public static final int SIZE_POETB = 3;
  public static final int SIZE_PONUM = 6;
  public static final int DECIMAL_PONUM = 0;
  public static final int SIZE_POSUF = 1;
  public static final int DECIMAL_POSUF = 0;
  public static final int SIZE_POCLI = 6;
  public static final int DECIMAL_POCLI = 0;
  public static final int SIZE_POLIV = 3;
  public static final int DECIMAL_POLIV = 0;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 31;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POCOD = 1;
  public static final int VAR_POETB = 2;
  public static final int VAR_PONUM = 3;
  public static final int VAR_POSUF = 4;
  public static final int VAR_POCLI = 5;
  public static final int VAR_POLIV = 6;
  public static final int VAR_POARR = 7;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private String pocod = ""; // Code ERL "D","E",X","9"
  private String poetb = ""; // Code établissement
  private BigDecimal ponum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal posuf = BigDecimal.ZERO; // Suffixe
  private BigDecimal pocli = BigDecimal.ZERO; // Numéro client
  private BigDecimal poliv = BigDecimal.ZERO; // Suffixe client
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400Text(SIZE_POCOD), // Code ERL "D","E",X","9"
      new AS400Text(SIZE_POETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PONUM, DECIMAL_PONUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_POSUF, DECIMAL_POSUF), // Suffixe
      new AS400ZonedDecimal(SIZE_POCLI, DECIMAL_POCLI), // Numéro client
      new AS400ZonedDecimal(SIZE_POLIV, DECIMAL_POLIV), // Suffixe client
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, pocod, poetb, ponum, posuf, pocli, poliv, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    pocod = (String) output[1];
    poetb = (String) output[2];
    ponum = (BigDecimal) output[3];
    posuf = (BigDecimal) output[4];
    pocli = (BigDecimal) output[5];
    poliv = (BigDecimal) output[6];
    poarr = (String) output[7];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPocod(Character pPocod) {
    if (pPocod == null) {
      return;
    }
    pocod = String.valueOf(pPocod);
  }
  
  public Character getPocod() {
    return pocod.charAt(0);
  }
  
  public void setPoetb(String pPoetb) {
    if (pPoetb == null) {
      return;
    }
    poetb = pPoetb;
  }
  
  public String getPoetb() {
    return poetb;
  }
  
  public void setPonum(BigDecimal pPonum) {
    if (pPonum == null) {
      return;
    }
    ponum = pPonum.setScale(DECIMAL_PONUM, RoundingMode.HALF_UP);
  }
  
  public void setPonum(Integer pPonum) {
    if (pPonum == null) {
      return;
    }
    ponum = BigDecimal.valueOf(pPonum);
  }
  
  public Integer getPonum() {
    return ponum.intValue();
  }
  
  public void setPosuf(BigDecimal pPosuf) {
    if (pPosuf == null) {
      return;
    }
    posuf = pPosuf.setScale(DECIMAL_POSUF, RoundingMode.HALF_UP);
  }
  
  public void setPosuf(Integer pPosuf) {
    if (pPosuf == null) {
      return;
    }
    posuf = BigDecimal.valueOf(pPosuf);
  }
  
  public Integer getPosuf() {
    return posuf.intValue();
  }
  
  public void setPocli(BigDecimal pPocli) {
    if (pPocli == null) {
      return;
    }
    pocli = pPocli.setScale(DECIMAL_POCLI, RoundingMode.HALF_UP);
  }
  
  public void setPocli(Integer pPocli) {
    if (pPocli == null) {
      return;
    }
    pocli = BigDecimal.valueOf(pPocli);
  }
  
  public Integer getPocli() {
    return pocli.intValue();
  }
  
  public void setPoliv(BigDecimal pPoliv) {
    if (pPoliv == null) {
      return;
    }
    poliv = pPoliv.setScale(DECIMAL_POLIV, RoundingMode.HALF_UP);
  }
  
  public void setPoliv(Integer pPoliv) {
    if (pPoliv == null) {
      return;
    }
    poliv = BigDecimal.valueOf(pPoliv);
  }
  
  public Integer getPoliv() {
    return poliv.intValue();
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
