/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.stocks;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.EnumCodeOperationLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.EnumOrigineOperationLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.IdLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.LigneMouvement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgStocksMouvements extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0003";
  
  /**
   * Appel du programme RPG qui va lire les lignes de stock.
   */
  public List<LigneMouvement> chargerListeMouvementStock(SystemeManager pSysteme, IdArticle pIdArticle, String pMagasin) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdArticle.controlerId(pIdArticle, true);
    
    List<LigneMouvement> stocks = null;
    
    // Préparation des paramètres du programme RPG
    Svgvx0003i entree = new Svgvx0003i();
    Svgvx0003o sortie = new Svgvx0003o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.pietb = pIdArticle.getCodeEtablissement();
    entree.piart = pIdArticle.getCodeArticle();
    entree.pimag = pMagasin;
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      // Entrée
      // Sortie
      stocks = new ArrayList<LigneMouvement>();
      Object[] lignes = sortie.getValeursBrutesLignes();
      for (int i = 0; i < lignes.length; i++) {
        stocks.add(traiterUneLigne((Object[]) lignes[i]));
      }
      if (stocks.get(0) == null) {
        return null;
      }
    }
    
    return stocks;
  }
  
  /**
   * Découpage d'une ligne
   * @param line
   * @return
   */
  private LigneMouvement traiterUneLigne(Object[] line) {
    if (line == null) {
      return null;
    }
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) line[Svgvx0003d.VAR_WETB]);
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, (String) line[Svgvx0003d.VAR_WART]);
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, (String) line[Svgvx0003d.VAR_WMAG]);
    Date dateMouvement = ConvertDate.db2ToDate(((BigDecimal) line[Svgvx0003d.VAR_WDAT]).intValue(), null);
    Integer numeroOrdre = ((BigDecimal) line[Svgvx0003d.VAR_WORD]).intValue();
    IdLigneMouvement idLigneMouvement = IdLigneMouvement.getInstance(idEtablissement, idArticle, idMagasin, dateMouvement, numeroOrdre);
    
    LigneMouvement ligneMouvement = new LigneMouvement(idLigneMouvement);
    ligneMouvement.setErlModificateurPump((String) line[Svgvx0003d.VAR_WMDP]);
    ligneMouvement.setCodeOperation(EnumCodeOperationLigneMouvement.valueOfByCode(((String) line[Svgvx0003d.VAR_WOPE]).charAt(0)));
    ligneMouvement.setQuantiteMouvement(((BigDecimal) line[Svgvx0003d.VAR_WQTM])); // quantité mouvement
    ligneMouvement.setQuantiteStock(((BigDecimal) line[Svgvx0003d.VAR_WQST])); // quantité en stock
    ligneMouvement.setPrixUnitaire(((BigDecimal) line[Svgvx0003d.VAR_WPRX]));
    ligneMouvement.setPump(((BigDecimal) line[Svgvx0003d.VAR_WPMP]));
    ligneMouvement.setPrixDeRevient(((BigDecimal) line[Svgvx0003d.VAR_WPRV]));
    ligneMouvement.setPrixUnitaireDevise(((BigDecimal) line[Svgvx0003d.VAR_WPRD]));
    ligneMouvement.setSigneOperation((String) line[Svgvx0003d.VAR_WSGN]);
    if (!((String) line[Svgvx0003d.VAR_WMAG2]).trim().isEmpty()) {
      ligneMouvement.setIdMagasinRecepteur(IdMagasin.getInstance(idEtablissement, (String) line[Svgvx0003d.VAR_WMAG2]));
    }
    ligneMouvement.setNumeroOrdreTransfert(((BigDecimal) line[Svgvx0003d.VAR_WORD2]).intValue());
    ligneMouvement.setOrigineOperation(EnumOrigineOperationLigneMouvement.valueOfByCode(((String) line[Svgvx0003d.VAR_WORI]).charAt(0)));
    ligneMouvement.setCodeBonOrigine((String) line[Svgvx0003d.VAR_WCOD0]);
    ligneMouvement.setNumeroBonOrigine(((BigDecimal) line[Svgvx0003d.VAR_WNUM0]).intValue());
    ligneMouvement.setSuffixeBonOrigine(((BigDecimal) line[Svgvx0003d.VAR_WSUF0]).intValue());
    ligneMouvement.setLigneBonOrigine(((BigDecimal) line[Svgvx0003d.VAR_WNLI0]).intValue());
    ligneMouvement.setCollectifFournisseur(((BigDecimal) line[Svgvx0003d.VAR_WTI1]).intValue());
    ligneMouvement.setNumeroTiers(((BigDecimal) line[Svgvx0003d.VAR_WTI2]).intValue());
    ligneMouvement.setSuffixeLivraison(((BigDecimal) line[Svgvx0003d.VAR_WTI3]).intValue());
    ligneMouvement.setDatePrecedenteOperation(ConvertDate.db2ToDate(((BigDecimal) line[Svgvx0003d.VAR_WDATP]).intValue(), null));
    ligneMouvement.setNumeroOrdrePrecedenteOperation(((BigDecimal) line[Svgvx0003d.VAR_WORDP]).intValue());
    ligneMouvement.setDatePrecedentInventaire(ConvertDate.db2ToDate(((BigDecimal) line[Svgvx0003d.VAR_WDATI]).intValue(), null));
    ligneMouvement.setNumeroOrdrePrecedentInventaire(((BigDecimal) line[Svgvx0003d.VAR_WORDI]).intValue());
    ligneMouvement.setQuantitePrecedentInventaire(((BigDecimal) line[Svgvx0003d.VAR_WQIV]));
    ligneMouvement.setCumulEntrees(((BigDecimal) line[Svgvx0003d.VAR_WQEI]));
    ligneMouvement.setCumulSorties(((BigDecimal) line[Svgvx0003d.VAR_WQSI]));
    ligneMouvement.setCumulDivers(((BigDecimal) line[Svgvx0003d.VAR_WQDI]));
    ligneMouvement.setTopInventairePurge(((BigDecimal) line[Svgvx0003d.VAR_WPUR]).intValue());
    ligneMouvement.setDateReelleOperation(ConvertDate.db2ToDate(((BigDecimal) line[Svgvx0003d.VAR_WDRM]).intValue(), null));
    ligneMouvement.setHeureReelleOperation(ConvertDate.db2ToDate(((BigDecimal) line[Svgvx0003d.VAR_WHRM]).intValue(), null));
    ligneMouvement.setQuantiteReelle(((BigDecimal) line[Svgvx0003d.VAR_WQTR]));
    
    return ligneMouvement;
  }
}
