/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.exp.database.files.FFD_Psemhflxm;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Classe contenant les zones du PSEMHFLXM et les requêtes qui traite un flux.
 */
public class FluxMagento extends FFD_Psemhflxm implements Cloneable {
  // Constantes
  private final static String FILE_NAME = "PSEMHFLXM";
  // Ids des différents flux (FLIDF)
  public final static String IDF_FLX001_ARTICLE = "FLX001";
  public final static String IDF_FLX002_COMMANDE = "FLX002";
  public final static String IDF_FLX003_SUIVIDMD = "FLX003";
  public final static String IDF_FLX004_CLIENT = "FLX004";
  public final static String IDF_FLX005_FACTURE = "FLX005";
  public final static String IDF_FLX006_STOCK = "FLX006";
  public final static String IDF_FLX007_SUIVICLIENT = "FLX007";
  public final static String IDF_FLX013_PRIX = "FLX013";
  
  // Flux d'EDIs
  public final static String IDF_FLX008_EDI_ORDERS = "FLX008";
  public final static String IDF_FLX009_EDI_DESADV = "FLX009";
  public final static String IDF_FLX010_EDI_FACTU = "FLX010";
  public final static String IDF_FLX012_FAC_YOOZ = "FLX012";
  
  // codes du flux
  public final static String COD_FLX001_ARTICLE = "ARTICLE";
  public final static String COD_FLX002_COMMANDE = "COMMANDE";
  public final static String COD_FLX003_SUIVIDMD = "SUIVIDMD";
  public final static String COD_FLX004_CLIENT = "CLIENT";
  public final static String COD_FLX005_FACTURE = "FACTURE";
  public final static String COD_FLX006_STOCK = "STOCK";
  public final static String COD_FLX007_SUIVICLIENT = "SUIVICLIENT";
  public final static String COD_FLX008_EDI_ORDERS = "EDI_ORDERS";
  public final static String COD_FLX009_EDI_DESADV = "EDI_DESADV";
  public final static String COD_FLX010_EDI_FACTU = "EDI_FACTU";
  public final static String COD_FLX012_FACT_YOOZ = "FACT_YOOZ";
  public final static String COD_FLX013_PRIX = "PRIX";
  
  public final static String[][] LIST_IDF = { { IDF_FLX001_ARTICLE, COD_FLX001_ARTICLE }, { IDF_FLX002_COMMANDE, COD_FLX002_COMMANDE },
      { IDF_FLX003_SUIVIDMD, COD_FLX003_SUIVIDMD }, { IDF_FLX004_CLIENT, COD_FLX004_CLIENT }, { IDF_FLX005_FACTURE, COD_FLX005_FACTURE },
      { IDF_FLX006_STOCK, COD_FLX006_STOCK }, { IDF_FLX007_SUIVICLIENT, COD_FLX007_SUIVICLIENT },
      { IDF_FLX008_EDI_ORDERS, COD_FLX008_EDI_ORDERS }, { IDF_FLX012_FAC_YOOZ, COD_FLX012_FACT_YOOZ },
      { IDF_FLX013_PRIX, COD_FLX013_PRIX } };
  
  // Constantes sens du flux
  public final static int SNS_VERS_MAGENTO = 0;
  public final static int SNS_VERS_SERIEN = 1;
  public final static int SNS_EDI_RECEP = 2;
  public final static int SNS_EDI_ENVOI = 3;
  
  // Constantes statuts d'erreur
  public final static int ERR_PAS_ERREUR = 0;
  public final static int ERR_TRAITEMENT = 1;
  public final static int ERR_ENVOI = 2;
  public final static int ERR_REFUS = 3;
  public final static int ERR_ENVOI_RECEPTION = 4;
  public static final int ERR_UPDATE = 5;
  
  private static final int LGR_MAX_CLOB = 5000;
  
  private String jsonAffichage = null;
  private boolean fluxAutomatique = false;
  
  private List<Object> listeReliquat = null;
  
  /**
   * Constructeur.
   */
  public FluxMagento(QueryManager pQueryManager, String pVersion, int pSens) {
    super(pQueryManager);
    omittedField = new String[] { "FLID" };
    if (pSens == SNS_VERS_MAGENTO) {
      setFLVER(pVersion);
    }
  }
  
  /**
   * Constructeur sortant vers Magento.
   * POURRI !!!!
   */
  public FluxMagento(QueryManager pQueryManager, String pVersion) {
    super(pQueryManager);
    omittedField = new String[] { "FLID" };
    setFLVER(pVersion);
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise la classe FluxMagento à partir du generic record.
   * 
   * @param pRecord
   */
  public void completerFluxMagento(GenericRecord pRecord) {
    if (pRecord == null) {
      return;
    }
    
    // FLID, FLIDF, FLETB, FLCOD, FLSNS, FLVER, FLJSN, FLSTT, FLCPT
    if (pRecord.isPresentField("FLID")) {
      try {
        setFLID(Integer.parseInt(pRecord.getField("FLID").toString().trim()));
      }
      catch (Exception e) {
      }
    }
    
    if (pRecord.isPresentField("FLIDF")) {
      setFLIDF(pRecord.getField("FLIDF").toString().trim());
    }
    
    if (pRecord.isPresentField("FLETB")) {
      setFLETB(pRecord.getField("FLETB").toString().trim());
    }
    
    if (pRecord.isPresentField("FLCOD")) {
      setFLCOD(pRecord.getField("FLCOD").toString().trim());
    }
    
    if (pRecord.isPresentField("FLSNS")) {
      try {
        setFLSNS(Integer.parseInt(pRecord.getField("FLSNS").toString().trim()));
      }
      catch (Exception e) {
      }
    }
    
    if (getFLSNS() != SNS_VERS_MAGENTO) {
      if (pRecord.isPresentField("FLVER")) {
        setFLVER(pRecord.getField("FLVER").toString().trim());
      }
    }
    
    if (pRecord.isPresentField("FLJSN")) {
      setFLJSN(pRecord.getField("FLJSN").toString().trim());
    }
    
    if (pRecord.isPresentField("FLSTT")) {
      try {
        Integer valeur = pRecord.getIntegerValue("FLSTT");
        setFLSTT(EnumStatutFlux.valueOfByNumero(valeur));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
    
    if (pRecord.isPresentField("FLCPT")) {
      try {
        setFLCPT(pRecord.getIntegerValue("FLCPT"));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
  }
  
  /**
   * Insère l'enregistrement dans le table.
   */
  @Override
  public boolean insertInDatabase() {
    
    try {
      Trace.info("++++++++++++++ insertInDatabase( : " + getFLETB());
      String requete = " INSERT INTO " + querymg.getLibrary() + ".PSEMHFLXM "
          + " (FLIDF,FLETB,FLVER,FLCRE,FLCOD,FLSNS,FLSTT,FLTRT,FLCPT,FLERR,FLJSN)" + " VALUES ('" + getFLIDF() + "','" + getFLETB()
          + "','" + getFLVER() + "', CURRENT TIMESTAMP,'" + getFLCOD() + "','" + getFLSNS() + "','" + getFLSTT().getNumero()
          + "', CURRENT TIMESTAMP,'" + getFLCPT() + "','" + getFLERR() + "'" + ",'" + traiterCaracteresSpeciauxSQL(getFLJSN()) + "') ";
      
      boolean retour = securerequest(requete);
      
      if (retour) {
        // On récupère l'ID du flux inséré (ca peut servir dans les flux reliquats traités en direct
        ArrayList<GenericRecord> liste = querymg.select("SELECT MAX(FLID) AS MONID FROM " + querymg.getLibrary()
            + ".PSEMHFLXM WHERE FLIDF = '" + getFLIDF() + "' AND FLSTT = " + getFLSTT().getNumero());
        if (liste != null && liste.size() == 1) {
          try {
            setFLID(Integer.parseInt(liste.get(0).getField("MONID").toString().trim()));
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
      return retour;
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * Passe le statut d'un flux à "En cours".
   */
  public void modifierStatutEnCours() {
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("update " + querymg.getLibrary() + "." + EnumTableBDD.FLUX + " set ");
    requeteSql.ajouterValeurUpdate("FLSTT", EnumStatutFlux.EN_COURS.getNumero());
    requeteSql.ajouterValeurUpdate("FLTRT", "CURRENT TIMESTAMP", true);
    requeteSql.ajouter(" where");
    requeteSql.ajouterConditionAnd("FLID", "=", getFLID());
    
    try {
      querymg.requete(requeteSql.getRequete());
    }
    catch (Exception e) {
      Trace.erreur(e, "Problème lors du changement du statut du flux " + getFLID() + " vers \"" + EnumStatutFlux.EN_COURS + "\".");
    }
  }
  
  /**
   * Modifie l'enregistrement du flux dans la table SQL correspondante PSEMHFLXM.
   *
   * @return
   */
  @Override
  public boolean updateInDatabase() {
    initGenericRecord(genericrecord, false);
    
    if (jsonAffichage != null) {
      jsonAffichage = traiterCaracteresSpeciauxSQL(jsonAffichage);
    }
    
    // Traitement du message d'erreur
    if (getFLMSGERR() != null && getFLMSGERR().length() > LGR_MAX_CLOB) {
      setFLMSGERR(getFLMSGERR().substring(0, LGR_MAX_CLOB));
    }
    else if (getFLMSGERR() == null) {
      setFLMSGERR("");
    }
    
    setFLMSGERR(traiterCaracteresSpeciauxSQL(getFLMSGERR()));
    
    String requete = " " + " UPDATE " + querymg.getLibrary() + "." + EnumTableBDD.FLUX + " " + " SET FLIDF='" + getFLIDF()
        + "', FLETB='" + getFLETB() + "', FLVER='" + getFLVER() + "', " + " FLCOD='" + getFLCOD() + "', FLSNS= '" + getFLSNS()
        + "', FLSTT='" + getFLSTT().getNumero() + "', FLTRT = CURRENT TIMESTAMP, " + " FLCPT='" + getFLCPT() + "', FLJSN='"
        + jsonAffichage + "', FLERR='" + getFLERR() + "', FLMSGERR = '" + getFLMSGERR() + "' " + " WHERE  FLID='" + getFLID() + "' ";
    
    try {
      int res = querymg.requete(requete);
      return (res == 1);
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * Suite à une erreur d'UPDATE classique on tente de faire un UPDATE restreint afin de stopper le traitement du flux et de le mettre à
   * jour de l'erreur d'UPDATE.
   *
   * @return
   */
  public boolean updateRestreintInDatabase() {
    setFLERR(FluxMagento.ERR_UPDATE);
    
    String requete = " UPDATE " + querymg.getLibrary() + "." + EnumTableBDD.FLUX + " " + " SET FLSTT='" + getFLSTT().getNumero()
        + "', FLTRT = CURRENT TIMESTAMP, " + " FLCPT='" + getFLCPT() + "', FLERR='" + getFLERR()
        + "', FLMSGERR = 'Erreur UPDATE dans la Table des flux'  " + " WHERE  FLID='" + getFLID() + "' ";
    
    try {
      int res = querymg.requete(requete);
      return (res == 1);
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }
  
  /**
   * Suppression de l'enregistrement courant.
   *
   * @return
   */
  @Override
  public boolean deleteInDatabase() {
    String requete = "delete from " + querymg.getLibrary() + "." + FILE_NAME + " where FLID=" + getFLID();
    return securerequest(requete);
  }
  
  @Override
  public FluxMagento clone() {
    FluxMagento copie = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      copie = (FluxMagento) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
      cnse.printStackTrace(System.err);
    }
    
    // on renvoie le clone
    return copie;
  }
  
  /**
   * Libère les ressources.
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
  }
  
  /**
   * Contrôle la validité d'un Id de Flux.
   * 
   * @param pIdFlux
   * @return
   */
  public static int controlValidityIdFlux(String pIdFlux) {
    boolean trouve = false;
    for (int i = 0; i < LIST_IDF.length; i++) {
      if (LIST_IDF[i][0].equalsIgnoreCase(pIdFlux)) {
        trouve = true;
        break;
      }
    }
    
    if (!trouve) {
      return Constantes.ERREUR;
    }
    else {
      return Constantes.OK;
    }
  }
  
  /**
   * Retourne le texte associé à l'id du flux.
   * 
   * @param pIdFlux
   * @return
   */
  public static String getTextToidf(String pIdFlux) {
    for (int i = 0; i < LIST_IDF.length; i++) {
      if (LIST_IDF[i][0].equalsIgnoreCase(pIdFlux)) {
        return LIST_IDF[i][1];
      }
    }
    return "UNKNOWN";
  }
  
  /**
   * Initialise la chaine JSON.
   * 
   * @param pJson
   */
  @Override
  public void setFLJSN(String pJson) {
    super.setFLJSN(pJson);
    
    if (getFLJSN() != null && getFLJSN().length() > LGR_MAX_CLOB) {
      jsonAffichage = getFLJSN().substring(0, LGR_MAX_CLOB);
    }
    else {
      jsonAffichage = getFLJSN();
    }
  }
  
  /**
   * Echapper les caractères spéciaux.
   * 
   * @param pChaine
   * @return
   */
  private String traiterCaracteresSpeciauxSQL(String pChaine) {
    if (pChaine == null) {
      return null;
    }
    
    return pChaine.replace("'", "''");
  }
  
  /**
   * Permet de mettre à jour le message d'erreur de ce flux.
   * 
   * @param pMessage
   */
  public void construireMessageErreur(String pMessage) {
    if (pMessage == null) {
      return;
    }
    
    Trace.erreur(pMessage);
    
    if (FLMSGERR == null) {
      FLMSGERR = pMessage;
    }
    else {
      FLMSGERR += "\n" + pMessage;
    }
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  /**
   * Retourne la chaine json.
   * 
   * @return
   */
  public String getJsonAffichage() {
    return jsonAffichage;
  }
  
  /**
   * Initialise la chaine json.
   * 
   * @param json
   */
  public void setJsonAffichage(String json) {
    this.jsonAffichage = json;
  }
  
  /**
   * Retourne la liste des reliquats.
   * 
   * @return
   */
  public List<Object> getListeReliquat() {
    return listeReliquat;
  }
  
  /**
   * Initialise la liste des reliquats.
   * 
   * @param pListeReliquat
   */
  public void setListeReliquat(List<Object> pListeReliquat) {
    this.listeReliquat = pListeReliquat;
  }
  
  /**
   * Retourne si le flux est un flux automatique.
   * 
   * @return
   */
  public boolean isFluxAutomatique() {
    return fluxAutomatique;
  }
  
  /**
   * Initialise le flux comme flux automatique.
   * 
   * @param pAutomatique
   */
  public void setFluxAutomatique(boolean pAutomatique) {
    this.fluxAutomatique = pAutomatique;
  }
}
