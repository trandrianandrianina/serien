/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx004.v3;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonClientWebV3;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Version 3 des flux.
 * Cette classe permet d'exporter un client Série N vers un tiers.
 */
public class ExportationClientWebV3 extends BaseGroupDB {
  
  /**
   * Constructeur.
   */
  public ExportationClientWebV3(QueryManager pQueryManager) {
    super(pQueryManager);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne une string JSON contenant les informations d'un client pour Magento.
   */
  public JsonClientWebV3 retournerUnClientWeb(FluxMagento pFlux) {
    // Contrôles
    if (pFlux == null || pFlux.getFLETB() == null || pFlux.getFLCOD() == null) {
      majError("[ExportationClientWebV3] retournerUnClientMagentoJSON() -> Record NULL ou corrompues ");
      return null;
    }
    
    // Chargement du client à partir de la base Série N
    GenericRecord recordClientPrincipal = retournerClientSerieN(pFlux.getFLETB(), pFlux.getFLCOD());
    if (recordClientPrincipal == null) {
      return null;
    }
    
    // Préparation des données du client qui va être exporté vers SFCC
    JsonClientWebV3 clientWeb = new JsonClientWebV3(pFlux);
    Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), clientWeb.getEtb());
    if (numeroInstance == null) {
      majError("[ExportationClientWebV3] retournerUnClientWeb() : problème d'interprétation de l'établissement vers l'instance Magento");
      return null;
    }
    clientWeb.setIdInstanceMagento(numeroInstance);
    
    // Mise à jour du client Magento à partir des données de Série N
    if (!clientWeb.majDesInfosPourExport(recordClientPrincipal)) {
      majError("[ExportationClientWebV3] retournerUnClientWeb() :" + clientWeb.getMsgError());
      return null;
    }
    
    // Chargement de l'adresse du client de facturation
    GenericRecord RcClientFacturation = retournerAdresseClientFacturation(clientWeb);
    if (RcClientFacturation != null) {
      clientWeb.majAdresseClientFacturation(RcClientFacturation);
    }
    
    // Chargement des adresses de livraison
    ArrayList<GenericRecord> RcClientsLivres = retournerAdressesLivraison(clientWeb);
    if (RcClientsLivres != null) {
      clientWeb.majAdressesClientsLivres(RcClientsLivres);
    }
    
    // Le flux n'est pas traité si la variable non géré sur le Web est positionné à 1
    if (clientWeb.getExclusWeb() == null || clientWeb.getExclusWeb().equals(JsonClientWebV3.EXCLUS_WEB)) {
      pFlux.setFLSTT(EnumStatutFlux.NE_PAS_TRAITER);
      pFlux.construireMessageErreur("Ce client n'est pas géré sur le Web");
    }
    
    // Conversion de l'objet en Json pour le stocker dans l'enregistrement et contrôle les données de base
    if (initJSON() && clientWeb.isValide()) {
      try {
        // Conversion du record en JSON
        pFlux.setFLJSN(gson.toJson(clientWeb));
      }
      catch (Exception e) {
        majError("[ExportationClientWebV3] retournerUnClientWeb() -> Erreur lors de la conversion de l'enregistrement en Json: "
            + e.getMessage());
      }
    }
    
    return clientWeb;
  }
  
  /**
   * Retourner le suivi d'une commande Série N sous forme GenericRecord : A AMELIORER AVEC LES CLASSES METIER COMMUNES DE STEPH.
   */
  protected ArrayList<GenericRecord> retournerUnSuiviCommandeDB2(String pCodeEtablissement, String pNumeroDocument) {
    if (pCodeEtablissement == null || pNumeroDocument == null) {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeDB2() etb ou numBon Null ou corrompus");
      return null;
    }
    
    String numero = null;
    String suffixe = null;
    
    // Découpage du paramètre code entre numéro de commande et suffixe (6 caractères + 1) on élimine les 4 premiers caractères (type de
    // bon sur 1 + société sur 3)
    if (pNumeroDocument.length() == 11) {
      numero = pNumeroDocument.trim().substring(4, pNumeroDocument.length() - 1);
      suffixe = pNumeroDocument.trim().substring(pNumeroDocument.length() - 1);
    }
    else {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeDB2() numBon longueur != 11: " + pNumeroDocument);
      return null;
    }
    
    // Le test du E1COD doit se faire sur la valeur 'E' pour la commande normale ou '9' pour une commande annulée (si la PS66 = '2')
    // Changement à cause de l'introduction du 'e' pour le comptoir
    ArrayList<GenericRecord> listeCommandes = queryManager.select(
        "SELECT E1COD, E1CCT, E1ETA, E1AVR, COTRK FROM " + queryManager.getLibrary() + ".PGVMEBCM LEFT JOIN " + queryManager.getLibrary()
            + ".PGVMNCOM ON E1ETB = COETB AND E1NUM = CONUM AND E1SUF = COSUF WHERE (E1COD = 'E' OR E1COD = '9') AND E1ETB = '"
            + pCodeEtablissement.trim() + "' AND E1NUM = '" + numero + "' AND E1SUF = '" + suffixe + "'  ");
    
    if (listeCommandes != null && listeCommandes.size() > 0) {
      return listeCommandes;
    }
    else {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeDB2() listeCommandes à NULL ou 0 pour : " + pCodeEtablissement + "/"
          + pNumeroDocument);
      return null;
    }
  }
  
  @Override
  public void dispose() {
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne les données d'un client Série N sur la base de l'établissement et du code fourni.
   */
  private GenericRecord retournerClientSerieN(String pCodeEtablissement, String pIndicatifClient) {
    if (pCodeEtablissement == null || pIndicatifClient == null) {
      majError("[ExportClientMagentoV3] retournerClientDB2() -> etb/code NULL ou corrompus ");
      return null;
    }
    
    // Découpe l'indicatif du client en identifiant clients CLCLI et CLLIV
    String CLCLI = null;
    String CLLIV = null;
    String[] tabDecoupe = pIndicatifClient.split("/");
    if (tabDecoupe != null && tabDecoupe.length == 2) {
      CLCLI = tabDecoupe[0];
      // On prend le client principal systématiquement, les clients livrés apparaissent sous forme de clients livrés
      CLLIV = "0";
      // Problème lors de la transformation de l'indicatif en identifiant
      if (CLCLI == null || CLCLI.length() == 0) {
        majError("[ExportClientMagentoV3] retournerClientDB2() -> CLCLI ou CLLIV corrompus " + pIndicatifClient);
        return null;
      }
    }
    else {
      majError("[ExportClientMagentoV3] retournerClientDB2() -> tabDecoupe NULL ou corrompue " + pIndicatifClient);
      return null;
    }
    
    // Récupération des informations en table du client
    // @formatter:off
    String requete = " SELECT CLETB,CLCLI,CLLIV,CLADH,CLAPEN,CLCAT,CLRGL,CLNOM,CLCPL,CLRUE,CLLOC,CLVIL,CLCDP1,"
        + " CLCOP,CLPAY,CLTEL,CLFAX,CLSRN,CLSRT,CLTOP1,CLTOP2,CLTAR,CLIN9,CLCLFS,CLCLFP,CLTFA,EBIN09,"
        + " RECIV, RENUM, REPAC, RENOM, REPRE, RENET FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT_LIEN
        + " ON RLETB = CLETB AND RLIND = DIGITS(CLCLI)||DIGITS(CLLIV) AND RLIN1 = 'P'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT
        + " ON RENUM = RLNUMT AND RLCOD = 'C'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT_EXTENSION
        + " ON CLETB = EBETB AND EBCLI = CLCLI AND EBLIV = CLLIV "
        + " WHERE CLETB = " + RequeteSql.formaterStringSQL(pCodeEtablissement) + " AND CLCLI = " + CLCLI + " AND CLLIV = " + CLLIV;
    // @formatter:on
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    if (liste != null && liste.size() == 1) {
      return liste.get(0);
    }
    else {
      if (liste == null || liste.isEmpty()) {
        majError("[ExportClientMagentoV3] retournerClientDB2() -> La liste est nulle ou vide : " + pCodeEtablissement + "/"
            + pIndicatifClient + " -> " + queryManager.getMsgError());
      }
      else {
        majError("[ExportClientMagentoV3] retournerClientDB2() -> Plusieurs clients ont été trouvés (" + liste.size() + ") : "
            + pCodeEtablissement + "/" + pIndicatifClient);
      }
      return null;
    }
  }
  
  /**
   * Retourner le client de facturation.
   * Si le client est désactivé il sera ignoré.
   */
  private GenericRecord retournerAdresseClientFacturation(JsonClientWebV3 pJsonClientMagento) {
    if (pJsonClientMagento.getCLCLFP() == null || pJsonClientMagento.getCLCLFS() == null) {
      return null;
    }
    
    // Récupération des informations en table du client
    // @formatter:off
    String requete = " SELECT CLETB,CLCLI,CLLIV,CLADH,CLAPEN,CLCAT,CLRGL,CLNOM,CLCPL,CLRUE,CLLOC,CLVIL,CLCDP1,"
        + " CLCOP,CLPAY,CLTEL,CLFAX,CLSRN,CLSRT,CLTOP1,CLTOP2,CLTAR,CLIN9,CLCLFS,CLCLFP,EBIN09,"
        + " RECIV, RENUM, REPAC, RENOM, REPRE, RENET FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT_LIEN
        + " ON RLETB = CLETB AND RLIND = DIGITS(CLCLI)||DIGITS(CLLIV) AND RLIN1 = 'P'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT
        + " ON RENUM = RLNUMT AND RLCOD = 'C'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT_EXTENSION
        + " ON CLETB = EBETB AND EBCLI = CLCLI AND EBLIV = CLLIV"
        + " WHERE CLETB = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getEtb())
        + " AND CLCLI = " + pJsonClientMagento.getCLCLFP() + " AND CLLIV = " + pJsonClientMagento.getCLCLFS() + " AND CLTNS <> 9";
    // @formatter:on
    
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    if (liste == null || liste.isEmpty()) {
      majError("retournerAdresseClientFacturation(): Aucun enregistrement trouvé " + queryManager.getMsgError());
      return null;
    }
    
    return liste.get(0);
  }
  
  /**
   * Retourner des clients livrés à partir d'un client facturé (xxxxxx/0).
   * Les clients livrés désactivés sont ignorés.
   */
  private ArrayList<GenericRecord> retournerAdressesLivraison(JsonClientWebV3 pClient) {
    // Récupération des informations en table du client
    // @formatter:off
    String requete = " SELECT CLETB,CLCLI,CLLIV,CLADH,CLAPEN,CLCAT,CLRGL,CLNOM,CLCPL,CLRUE,CLLOC,CLVIL,CLCDP1,"
        + " CLCOP,CLPAY,CLTEL,CLFAX,CLSRN,CLSRT,CLTOP1,CLTOP2,CLTAR,CLIN9,CLCLFS,CLCLFP,EBIN09,"
        + " RECIV, RENUM, REPAC, RENOM, REPRE, RENET FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT_LIEN
        + " ON RLETB = CLETB AND RLIND = DIGITS(CLCLI)||DIGITS(CLLIV) AND RLIN1 = 'P'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT
        + " ON RENUM = RLNUMT AND RLCOD = 'C'"
        + " LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT_EXTENSION
        + " ON CLETB = EBETB AND EBCLI = CLCLI AND EBLIV = CLLIV"
        + " WHERE CLETB = " + RequeteSql.formaterStringSQL(pClient.getEtb())
        + " AND CLCLI = " + pClient.getCLCLI() + " AND CLLIV <> 0 AND EBIN09 <> '' AND CLTNS <> 9";
    // @formatter:on
    
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    if (liste == null) {
      majError("retournerClientsLivres(): " + queryManager.getMsgError());
    }
    
    return liste;
  }
  
}
