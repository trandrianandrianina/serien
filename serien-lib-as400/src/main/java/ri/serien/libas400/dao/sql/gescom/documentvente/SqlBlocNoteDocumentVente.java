/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.documentvente;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.blocnotes.IdBlocNote;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlBlocNoteDocumentVente {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlBlocNoteDocumentVente(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  /**
   * Charger le bloc-notes du document de ventes.
   */
  public BlocNote chargerPetitBlocNotesDocumentVente(IdDocumentVente pIdDocumentVente) {
    // Vérifier les pré-requis
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Tester si c'est l'identifiant d'une facture
    // Dans ce cas, aucun bloc-notes n'est lié à la facture car ils sont intrinsèquement liés aux bons associés à la facture.
    if (pIdDocumentVente.isIdFacture()) {
      return null;
    }
    
    // Construire la requête
    RequeteSql requete = new RequeteSql();
    requete.ajouter("select * from " + querymg.getLibrary() + '.' + EnumTableBDD.PETIT_BLOCNOTES_DOCUMENT + " where");
    requete.ajouterConditionAnd("XIETB", "=", pIdDocumentVente.getCodeEtablissement());
    requete.ajouterConditionAnd("XICOD", "=", pIdDocumentVente.getEntete());
    requete.ajouterConditionAnd("XINUM", "=", pIdDocumentVente.getNumero());
    requete.ajouterConditionAnd("XISUF", "=", pIdDocumentVente.getSuffixe());
    requete.ajouterConditionAnd("XINLI", "=", 0);
    requete
        .ajouter(" and (XITYP = 41 or XITYP = 42 or XITYP = 43 or XITYP = 44 or XITYP = 45 or XITYP = 46 or XITYP = 47 or XITYP = 48)");
    String texte = request4OneBlocNotes(requete.getRequete());
    IdBlocNote idBlocNote = IdBlocNote.getInstancePourDocumentVente(pIdDocumentVente);
    BlocNote blocNote = new BlocNote(idBlocNote);
    blocNote.setTexte(texte);
    
    return blocNote;
  }
  
  /**
   * Mise à jour du bloc-notes du document.
   * @return int : nombre de lignes mises à jour
   */
  public int sauverPetitBlocNoteDocumentVente(IdDocumentVente pIdDocumentVente, BlocNote pBlocNote) {
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    if (pBlocNote == null || pBlocNote.getTexte() == null) {
      // nb de lignes enregistrées
      return -1;
    }
    String texteBlocNote = pBlocNote.getTexte();
    // nbr caractères par lignes
    int maxChar = 30;
    // retour si erreur
    int nombreDeLignesModifiees = 0;
    
    // mise à blanc lignes existantes (types de XLI 41 à 49)
    for (int b = 41; b < 49; b++) {
      updateInDatabase(pIdDocumentVente, "", b);
    }
    String[] nombreLigne = texteBlocNote.split("\r\n|\r|\n");
    ArrayList<String> ligneTexte = new ArrayList<String>();
    // On remplit un array list afin de pouvoir manipuler le tableau
    for (int i = 0; i < nombreLigne.length; i++) {
      ligneTexte.add(nombreLigne[i].toString().trim());
    }
    
    for (int i = 0; i < ligneTexte.size(); i++) {
      if (ligneTexte.get(i).length() > maxChar) {
        ligneTexte.add(ligneTexte.get(i).substring(maxChar, ligneTexte.get(i).length()));
      }
      
      // écriture
      // Update pour lignes existantes
      nombreDeLignesModifiees = updateInDatabase(pIdDocumentVente, ligneTexte.get(i), (i + 41));
      if (nombreDeLignesModifiees == 0) {
        // Si nouvelle ligne
        // Si observations vides insertion ligne 1
        if (i == 0) {
          nombreDeLignesModifiees = insertInDatabase(pIdDocumentVente, ligneTexte.get(i), 41);
          // sinon on rajoute à la fin
        }
        else {
          nombreDeLignesModifiees = insertInDatabase(pIdDocumentVente, ligneTexte.get(i), i + 41);
        }
      }
    }
    return nombreDeLignesModifiees;
  }
  
  /**
   * Insertion d'une ligne de texte d'un bloc-notes document en base
   * @param Document auquel le bloc-notes est rattaché
   * @param texte à écrire
   * @param type de XLI (41 à 48)
   * 
   * @return nombre d'enregistrements écrits
   */
  public int insertInDatabase(IdDocumentVente pIdDocumentVente, String texteBlocNotes, int type) {
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    String requete = "insert into " + querymg.getLibrary() + '.' + EnumTableBDD.PETIT_BLOCNOTES_DOCUMENT
        + " (XICOD, XIETB, XINUM, XISUF, XINLI, XITYP, XILIB) values ('" + pIdDocumentVente.getEntete() + "', '"
        + pIdDocumentVente.getCodeEtablissement() + "', '" + pIdDocumentVente.getNumero() + "', '" + pIdDocumentVente.getSuffixe()
        + "', '0', '" + type + "', '" + RequeteSql.traiterCaracteresSpeciauxSQL(texteBlocNotes.trim()) + "')";
    return querymg.requete(requete);
  }
  
  /**
   * Mise à jour d'un ligne de texte d'un bloc-notes document en base
   * 
   * @param Document auquel le bloc-notes est rattaché
   * @param texte à écrire
   * @param type de XLI (41 à 48)
   * 
   * @return nombre d'enregistrements écrits
   */
  public int updateInDatabase(IdDocumentVente pIdDocumentVente, String texteBlocNotes, int type) {
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    RequeteSql requete = new RequeteSql();
    requete.ajouter("update " + querymg.getLibrary() + '.' + EnumTableBDD.PETIT_BLOCNOTES_DOCUMENT + " set XILIB='"
        + RequeteSql.traiterCaracteresSpeciauxSQL(texteBlocNotes.trim()) + "' where");
    requete.ajouterConditionAnd("XIETB", "=", pIdDocumentVente.getCodeEtablissement());
    requete.ajouterConditionAnd("XICOD", "=", pIdDocumentVente.getEntete());
    requete.ajouterConditionAnd("XINUM", "=", pIdDocumentVente.getNumero());
    requete.ajouterConditionAnd("XISUF", "=", pIdDocumentVente.getSuffixe());
    requete.ajouterConditionAnd("XINLI", "=", 0);
    requete.ajouterConditionAnd("XITYP", "=", type);
    return querymg.requete(requete.getRequete());
  }
  
  /**
   * Lecture du bloc notes d'un document
   * 
   * @param requete
   * 
   * @return texte du bloc notes
   */
  public String request4OneBlocNotes(String requete) {
    if ((requete == null) || (requete.trim().length() == 0)) {
      return null;
    }
    if (querymg.getLibrary() == null) {
      return null;
    }
    
    // Lecture de la base afin de récupérer les actions commerciales
    ArrayList<GenericRecord> listbnd = querymg.select(requete);
    if (listbnd == null) {
      return null;
    }
    // Chargement des classes avec les données de la base
    String listbn = null;
    for (int j = 0; j < listbnd.size(); j++) {
      if (listbn == null) {
        listbn = (String) listbnd.get(j).getField("XILIB");
      }
      else {
        listbn += "\n" + ((String) listbnd.get(j).getField("XILIB"));
      }
    }
    return listbn;
  }
}
