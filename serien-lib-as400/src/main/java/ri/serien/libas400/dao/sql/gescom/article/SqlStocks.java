/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.article;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.EnumCodeOperationLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.EnumOrigineOperationLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.IdLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.LigneMouvement;
import ri.serien.libcommun.gescom.commun.client.ListeLigneMouvement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlStocks {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlStocks(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  /**
   * Lire les données de l'article dont on fourni l'identifiant.
   */
  public List<IdLigneMouvement> chargerListeIdLigneMouvement(IdArticle pIdArticle, Magasin pMagasin) {
    return lireIdMouvements(new Article(pIdArticle), pMagasin);
  }
  
  /**
   * Lire les données d'une ligne de mouvement de stock à partir d'un article et d'un magasin
   * Les données du mouvement sont complétées avec celles nouvellement chargées.
   */
  public List<IdLigneMouvement> lireIdMouvements(Article pArticle, Magasin pMagasin) {
    Article.controlerId(pArticle, true);
    
    // Générer la requête
    String requete =
        "SELECT H1ETB, H1ART, H1MAG, H1ORD, H1DAT FROM " + querymg.getLibrary() + '.' + EnumTableBDD.STOCK_HISTORIQUE
            + " WHERE H1ETB = " + RequeteSql.formaterStringSQL(pArticle.getId().getCodeEtablissement()) + " and H1ART = "
            + RequeteSql.formaterStringSQL(pArticle.getId().getCodeArticle()) + " and H1MAG = "
            + RequeteSql.formaterStringSQL(pMagasin.getId().getCode()) + " ORDER BY H1DAT DESC FETCH FIRST 1000 ROWS ONLY";
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    List<IdLigneMouvement> listeIdMouvements = new ArrayList<IdLigneMouvement>();
    for (GenericRecord genericRecord : listeRecord) {
      if (genericRecord != null) {
        listeIdMouvements.add(creerIdMouvement(genericRecord));
      }
    }
    
    return listeIdMouvements;
  }
  
  /**
   * Retourne la liste des mouvements avec toutes les informations
   */
  public ListeLigneMouvement chargerListeLigneMouvement(List<IdLigneMouvement> listeId, Integer pNombreDecimalesStock) {
    ListeLigneMouvement listeMouvements = new ListeLigneMouvement();
    
    // Générer la requête
    String requete =
        "SELECT * FROM " + querymg.getLibrary() + '.' + EnumTableBDD.STOCK_HISTORIQUE + " LEFT JOIN " + querymg.getLibrary()
            + '.' + EnumTableBDD.BORDEREAU_STOCK + " ON H1ETB = E1ETB AND H1NUM0 = E1NUM AND H1SUF0 = E1SUF AND H1DAT = E1HOM"
            + " WHERE H1ETB = " + RequeteSql.formaterStringSQL(listeId.get(0).getCodeEtablissement());
    
    // Ajouter les identifiants des lignes de mouvements de stocks à charger à la requête
    int index = 0;
    for (IdLigneMouvement idLigneMouvement : listeId) {
      if (index == 0) {
        requete += " and ((";
      }
      else {
        requete += " or (";
      }
      requete += "H1ART = " + RequeteSql.formaterStringSQL(idLigneMouvement.getIdArticle().getCodeArticle()) + " and H1MAG = "
          + RequeteSql.formaterStringSQL(idLigneMouvement.getIdMagasin().getCode()) + " and H1DAT= "
          + ConvertDate.dateToDb2(idLigneMouvement.getDateMouvement()) + " and H1ORD = " + idLigneMouvement.getNumeroOrdre();
      requete += ")";
      index++;
    }
    requete += " )";
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      throw new MessageErreurException("Aucun mouvement de stock trouvé pour cet article");
    }
    
    // on charge les données complètes
    for (GenericRecord genericRecord : listeRecord) {
      if (genericRecord != null) {
        listeMouvements.add(completerMouvement(genericRecord, pNombreDecimalesStock));
      }
    }
    
    return listeMouvements;
  }
  
  /**
   * Crée les ID de lignes mouvement à partir des données lues en base
   */
  private IdLigneMouvement creerIdMouvement(GenericRecord record) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("H1ETB", "", false));
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, record.getStringValue("H1ART"));
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, record.getStringValue("H1MAG"));
    Date dateMouvement = record.getDateValue("H1DAT");
    int numeroOrdre = record.getIntegerValue("H1ORD");
    if (dateMouvement != null) {
      dateMouvement.toString();
    }
    
    return IdLigneMouvement.getInstance(idEtablissement, idArticle, idMagasin, dateMouvement, numeroOrdre);
  }
  
  /**
   * Créer un mouvement à partir des champs de la base de données.
   */
  private LigneMouvement completerMouvement(GenericRecord record, Integer pNombreDecimalesStock) {
    LigneMouvement ligneMouvement = null;
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("H1ETB", "", false));
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, record.getStringValue("H1ART"));
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, record.getStringValue("H1MAG"));
    Date dateMouvement = record.getDateValue("H1DAT");
    int numeroOrdre = record.getIntegerValue("H1ORD");
    ligneMouvement = new LigneMouvement(IdLigneMouvement.getInstance(idEtablissement, idArticle, idMagasin, dateMouvement, numeroOrdre));
    ligneMouvement.setNombreDecimalesStock(pNombreDecimalesStock);
    ligneMouvement.setErlModificateurPump(record.getStringValue("H1MDP"));
    ligneMouvement.setCodeOperation(EnumCodeOperationLigneMouvement.valueOfByCode(record.getStringValue("H1OPE").charAt(0)));
    ligneMouvement.setQuantiteMouvement(record.getBigDecimalValue("H1QTM", BigDecimal.ZERO)); // quantité mouvement
    ligneMouvement.setQuantiteStock(record.getBigDecimalValue("H1QST", BigDecimal.ZERO)); // quantité en stock
    ligneMouvement.setPrixUnitaire(record.getBigDecimalValue("H1PRX", BigDecimal.ZERO));
    ligneMouvement.setPump(record.getBigDecimalValue("H1PMP", BigDecimal.ZERO));
    ligneMouvement.setPrixDeRevient(record.getBigDecimalValue("H1PRV", BigDecimal.ZERO));
    ligneMouvement.setPrixUnitaireDevise(record.getBigDecimalValue("H1PRD", BigDecimal.ZERO));
    ligneMouvement.setSigneOperation(record.getStringValue("H1SGN"));
    if (!record.getStringValue("H1MAG2").trim().isEmpty()) {
      ligneMouvement.setIdMagasinRecepteur(IdMagasin.getInstance(idEtablissement, record.getStringValue("H1MAG2")));
    }
    ligneMouvement.setNumeroOrdreTransfert(record.getIntegerValue("H1ORD2"));
    ligneMouvement.setOrigineOperation(EnumOrigineOperationLigneMouvement.valueOfByCode(record.getStringValue("H1ORI").charAt(0)));
    ligneMouvement.setCodeBonOrigine(record.getStringValue("H1COD0"));
    ligneMouvement.setNumeroBonOrigine(record.getIntegerValue("H1NUM0"));
    ligneMouvement.setSuffixeBonOrigine(record.getIntegerValue("H1SUF0"));
    ligneMouvement.setLigneBonOrigine(record.getIntegerValue("H1NLI0"));
    // Tiers
    if (record.getIntegerValue("H1TI2") > 0) {
      // Fournisseur
      if (record.getIntegerValue("H1TI1") > 0) {
        ligneMouvement.setIdClient(null);
        ligneMouvement.setIdFournisseur(
            IdFournisseur.getInstance(idEtablissement, record.getIntegerValue("H1TI1"), record.getIntegerValue("H1TI2")));
      }
      // Client
      else {
        ligneMouvement
            .setIdClient(IdClient.getInstance(idEtablissement, record.getIntegerValue("H1TI2"), record.getIntegerValue("H1TI3")));
        ligneMouvement.setIdFournisseur(null);
      }
    }
    ligneMouvement.setCollectifFournisseur(record.getIntegerValue("H1TI1"));
    ligneMouvement.setNumeroTiers(record.getIntegerValue("H1TI2"));
    ligneMouvement.setSuffixeLivraison(record.getIntegerValue("H1TI3"));
    ligneMouvement.setDatePrecedenteOperation(record.getDateValue("H1DATP"));
    ligneMouvement.setNumeroOrdrePrecedenteOperation(record.getIntegerValue("H1ORDP"));
    ligneMouvement.setDatePrecedentInventaire(record.getDateValue("H1DATI"));
    ligneMouvement.setNumeroOrdrePrecedentInventaire(record.getIntegerValue("H1ORDI"));
    ligneMouvement.setQuantitePrecedentInventaire(record.getBigDecimalValue("H1QIV", BigDecimal.ZERO));
    ligneMouvement.setCumulEntrees(record.getBigDecimalValue("H1QEI", BigDecimal.ZERO));
    ligneMouvement.setCumulSorties(record.getBigDecimalValue("H1QSI", BigDecimal.ZERO));
    ligneMouvement.setCumulDivers(record.getBigDecimalValue("H1QDI", BigDecimal.ZERO));
    ligneMouvement.setTopInventairePurge(record.getIntegerValue("H1PUR"));
    ligneMouvement.setDateReelleOperation(record.getDateValue("H1DRM"));
    ligneMouvement.setHeureReelleOperation(record.getDateValue("H1HRM"));
    ligneMouvement.setQuantiteReelle(record.getBigDecimalValue("H1QTR", BigDecimal.ZERO));
    if (record.getStringValue("E1UTI") != null && !record.getStringValue("E1UTI").trim().isEmpty()) {
      ligneMouvement.setIdMagasinier(IdVendeur.getInstance(idEtablissement, record.getStringValue("E1UTI")));
    }
    return ligneMouvement;
  }
  
}
