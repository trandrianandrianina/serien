/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.lot;

import java.lang.reflect.Method;
import java.math.BigDecimal;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.lot.ListeLot;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgSauverLot extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0025";
  
  /**
   * Appel du programme RPG qui va sauver une saisie de quantités par lot.
   */
  public IdLigneVente sauverListeLot(SystemeManager pSysteme, IdLigneVente pIdLigneVente, BigDecimal pQuantiteArticle,
      BigDecimal pQuantiteTotale, ListeLot pListeLot) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pIdLigneVente == null) {
      throw new MessageErreurException("Le paramètre pIdLigneVente du service est à null.");
    }
    if (pListeLot == null) {
      throw new MessageErreurException("Le paramètre pListeLot du service est à null.");
    }
    
    if (pListeLot.size() == 0) {
      return null;
    }
    
    // Préparation des paramètres du programme RPG
    Svgvx0025i entree = new Svgvx0025i();
    Svgvx0025o sortie = new Svgvx0025o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    //// identifiant de la ligne de vente
    entree.setPicod(pIdLigneVente.getCodeEntete().getCode());
    entree.setPietb(pIdLigneVente.getCodeEtablissement());
    entree.setPinum(pIdLigneVente.getNumero());
    entree.setPisuf(pIdLigneVente.getSuffixe());
    entree.setPinli(pIdLigneVente.getNumeroLigne());
    // Code article
    entree.setPiart(pListeLot.get(0).getId().getIdArticle().getCodeArticle());
    // Quantité d'articles lotis
    entree.setPiqtea(pQuantiteArticle);
    // Quantités affectées sur lot
    entree.setPiqtel(pQuantiteTotale);
    // Détail par lot
    // On construit le nom des méthodes dans la boucle pour ne pas écrire 3 x 100 méthodes (entree.setPil001, entree.setPil002, etc.)
    for (int i = 0; i < pListeLot.size(); i++) {
      String numerotationMethode = Constantes.convertirIntegerEnTexte(i + 1, 3);
      try {
        // Code lot
        Method methodePIL = Svgvx0025i.class.getMethod("setPil" + numerotationMethode, String.class);
        methodePIL.invoke(entree, pListeLot.get(i).getId().getCode());
        // Quantité initiale
        Method methodePIQ = Svgvx0025i.class.getMethod("setPiq" + numerotationMethode, BigDecimal.class);
        methodePIQ.invoke(entree, pListeLot.get(i).getQuantitePrecedentSaisi());
        // Quantité saisie
        Method methodePIN = Svgvx0025i.class.getMethod("setPin" + numerotationMethode, BigDecimal.class);
        methodePIN.invoke(entree, pListeLot.get(i).getQuantiteSaisi());
      }
      catch (Exception e) {
        throw new MessageErreurException(e, "Erreur d'écriture des quantités sur lots dans le service SVGVX0025");
      }
      
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      return null;
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Retourner l'identifiant de la ligne
    // IdEtablissement idEtablissement = IdEtablissement.getInstance(sortie.getPoetb());
    // EnumCodeEnteteDocumentVente code = EnumCodeEnteteDocumentVente.valueOfByCode(sortie.getPocod());
    // IdLigneVente idLigneSortie = IdLigneVente.getInstance(idEtablissement, code, sortie.getPonum(), sortie.getPosuf(),
    // sortie.getPonli());
    IdLigneVente idLigneSortie = null;
    return idLigneSortie;
  }
}
