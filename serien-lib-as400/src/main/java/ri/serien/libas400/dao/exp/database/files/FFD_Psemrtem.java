/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

public abstract class FFD_Psemrtem extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_REETB = 3;
  public static final int SIZE_RENUM = 6;
  public static final int DECIMAL_RENUM = 0;
  public static final int SIZE_RECIV = 3;
  public static final int SIZE_REPAC = 30;
  public static final int SIZE_RETEL = 20;
  public static final int SIZE_REFAX = 20;
  public static final int SIZE_RECAT = 3;
  public static final int SIZE_REOBS = 30;
  public static final int SIZE_RECL1 = 15;
  public static final int SIZE_REPOS = 5;
  public static final int SIZE_RECTL = 8;
  public static final int SIZE_RENET = 120;
  public static final int SIZE_REIN1 = 1;
  public static final int SIZE_REIN2 = 1;
  public static final int SIZE_REIN3 = 1;
  public static final int SIZE_RECL2 = 15;
  public static final int SIZE_RETEL2 = 20;
  public static final int SIZE_RETOP1 = 2;
  public static final int SIZE_RETOP2 = 2;
  public static final int SIZE_RETOP3 = 2;
  public static final int SIZE_RETOP4 = 2;
  public static final int SIZE_RETOP5 = 2;
  public static final int SIZE_REASPA = 1;
  public static final int SIZE_REAAPP = 1;
  public static final int SIZE_RETYZP = 1;
  public static final int SIZE_RENOM = 30;
  public static final int SIZE_REPRE = 30;
  public static final int SIZE_RENET2 = 120;
  public static final int SIZE_REPRF = 10;
  public static final int SIZE_REIN4 = 1;
  public static final int SIZE_REIN5 = 1;
  public static final int SIZE_REIN6 = 1;
  public static final int SIZE_REIN7 = 1;
  public static final int SIZE_REIN8 = 1;
  
  // Variables fichiers
  protected String REETB = null; // Code établissement
  protected int RENUM = 0; // Numéro d''ordre
  protected String RECIV = null; // Civilité
  protected String REPAC = null; // Personne à contacter
  protected String RETEL = null; // Téléphone
  protected String REFAX = null; // Fax
  protected String RECAT = null; // Catégorie
  protected String REOBS = null; // Observations
  protected String RECL1 = null; // Clé de classement 1
  protected String REPOS = null; // N° poste
  protected String RECTL = null; // Clé pour index tel
  protected String RENET = null; // Adresse Email
  protected char REIN1 = ' '; // 1=destinataire bon
  protected char REIN2 = ' '; // Copie Carbon cachée
  protected char REIN3 = ' '; // Non utilisé
  protected String RECL2 = null; // Clé de class.2
  protected String RETEL2 = null; // Téléphone 2
  protected String RETOP1 = null; // Critères d''analyse 1
  protected String RETOP2 = null; // Critères d''analyse 2
  protected String RETOP3 = null; // Critères d''analyse 3
  protected String RETOP4 = null; // Critères d''analyse 4
  protected String RETOP5 = null; // Critères d''analyse 5
  protected char REASPA = ' '; // 1=ANTI-SPAM
  protected char REAAPP = ' '; // 1=ANTI-APPEL
  protected char RETYZP = ' '; // 'TYPE ZP
  protected String RENOM = null; // Nom contact
  protected String REPRE = null; // Prénom contact
  protected String RENET2 = null; // Adresse Email2
  protected String REPRF = null; // Profil / ALIAS
  protected char REIN4 = ' '; // Non utilisé
  protected char REIN5 = ' '; // Non utilisé
  protected char REIN6 = ' '; // Non utilisé
  protected char REIN7 = ' '; // Non utilisé
  protected char REIN8 = ' '; // Non utilisé
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public FFD_Psemrtem(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    REETB = null;
    RENUM = 0;
    RECIV = null;
    REPAC = null;
    RETEL = null;
    REFAX = null;
    RECAT = null;
    REOBS = null;
    RECL1 = null;
    REPOS = null;
    RECTL = null;
    RENET = null;
    REIN1 = ' ';
    REIN2 = ' ';
    REIN3 = ' ';
    RECL2 = null;
    RETEL2 = null;
    RETOP1 = null;
    RETOP2 = null;
    RETOP3 = null;
    RETOP4 = null;
    RETOP5 = null;
    REASPA = ' ';
    REAAPP = ' ';
    RETYZP = ' ';
    RENOM = null;
    REPRE = null;
    RENET2 = null;
    REPRF = null;
    REIN4 = ' ';
    REIN5 = ' ';
    REIN6 = ' ';
    REIN7 = ' ';
    REIN8 = ' ';
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le rEETB
   */
  public String getREETB() {
    return REETB;
  }
  
  /**
   * @param aREETB l'établissement à définir
   */
  public void setREETB(String aREETB) {
    REETB = aREETB;
  }
  
  /**
   * @return le rENUM
   */
  public int getRENUM() {
    return RENUM;
  }
  
  /**
   * @param aRENUM le aRENUM à définir
   */
  public void setRENUM(int aRENUM) {
    RENUM = aRENUM;
  }
  
  /**
   * @return le aRECIV
   */
  public String getRECIV() {
    return RECIV;
  }
  
  /**
   * @param aRECIV le aRECIV à définir
   */
  public void setRECIV(String aRECIV) {
    RECIV = aRECIV;
  }
  
  /**
   * @return le aREPAC
   */
  public String getREPAC() {
    return REPAC;
  }
  
  /**
   * @param aREPAC le aREPAC à définir
   */
  public void setREPAC(String aREPAC) {
    REPAC = aREPAC;
  }
  
  /**
   * @return le aRETEL
   */
  public String getRETEL() {
    return RETEL;
  }
  
  /**
   * @param aRETEL le aRETEL à définir
   */
  public void setRETEL(String aRETEL) {
    RETEL = aRETEL;
  }
  
  /**
   * @return le aREFAX
   */
  public String getREFAX() {
    return REFAX;
  }
  
  /**
   * @param aREFAX le aREFAX à définir
   */
  public void setREFAX(String aREFAX) {
    REFAX = aREFAX;
  }
  
  /**
   * @return le aRECAT
   */
  public String getRECAT() {
    return RECAT;
  }
  
  /**
   * @param aRECAT le aRECAT à définir
   */
  public void setRECAT(String aRECAT) {
    RECAT = aRECAT;
  }
  
  /**
   * @return le aREOBS
   */
  public String getREOBS() {
    return REOBS;
  }
  
  /**
   * @param aREOBS le aREOBS à définir
   */
  public void setREOBS(String aREOBS) {
    REOBS = aREOBS;
  }
  
  /**
   * @return le aRECL1
   */
  public String getRECL1() {
    return RECL1;
  }
  
  /**
   * @param aRECL1 le aRECL1 à définir
   */
  public void setRECL1(String aRECL1) {
    RECL1 = aRECL1;
  }
  
  /**
   * @return le aREPOS
   */
  public String getREPOS() {
    return REPOS;
  }
  
  /**
   * @param aREPOS le aREPOS à définir
   */
  public void setREPOS(String aREPOS) {
    REPOS = aREPOS;
  }
  
  /**
   * @return le aRECTL
   */
  public String getRECTL() {
    return RECTL;
  }
  
  /**
   * @param aRECTL le aRECTL à définir
   */
  public void setRECTL(String aRECTL) {
    RECTL = aRECTL;
  }
  
  /**
   * @return le aRENET
   */
  public String getRENET() {
    return RENET;
  }
  
  /**
   * @param aRENET le aRENET à définir
   */
  public void setRENET(String aRENET) {
    RENET = aRENET;
  }
  
  /**
   * @return le aREIN1
   */
  public char getREIN1() {
    return REIN1;
  }
  
  /**
   * @param aREIN1 le aREIN1 à définir
   */
  public void setREIN1(char aREIN1) {
    REIN1 = aREIN1;
  }
  
  /**
   * @return le aREIN2
   */
  public char getREIN2() {
    return REIN2;
  }
  
  /**
   * @param aREIN2 le aREIN2 à définir
   */
  public void setREIN2(char aREIN2) {
    REIN2 = aREIN2;
  }
  
  /**
   * @return le aREIN3
   */
  public char getREIN3() {
    return REIN3;
  }
  
  /**
   * @param aREIN3 le aREIN3 à définir
   */
  public void setREIN3(char aREIN3) {
    REIN3 = aREIN3;
  }
  
  /**
   * @return le aRECL2
   */
  public String getRECL2() {
    return RECL2;
  }
  
  /**
   * @param aRECL2 le aRECL2 à définir
   */
  public void setRECL2(String aRECL2) {
    RECL2 = aRECL2;
  }
  
  /**
   * @return le aRETEL2
   */
  public String getRETEL2() {
    return RETEL2;
  }
  
  /**
   * @param aRETEL2 le aRETEL2 à définir
   */
  public void setRETEL2(String aRETEL2) {
    RETEL2 = aRETEL2;
  }
  
  /**
   * @return le aRETOP1
   */
  public String getRETOP1() {
    return RETOP1;
  }
  
  /**
   * @param aRETOP1 le aRETOP1 à définir
   */
  public void setRETOP1(String aRETOP1) {
    RETOP1 = aRETOP1;
  }
  
  /**
   * @return le aRETOP2
   */
  public String getRETOP2() {
    return RETOP2;
  }
  
  /**
   * @param aRETOP2 le aRETOP2 à définir
   */
  public void setRETOP2(String aRETOP2) {
    RETOP2 = aRETOP2;
  }
  
  /**
   * @return le aRETOP3
   */
  public String getRETOP3() {
    return RETOP3;
  }
  
  /**
   * @param aRETOP3 le aRETOP3 à définir
   */
  public void setRETOP3(String aRETOP3) {
    RETOP3 = aRETOP3;
  }
  
  /**
   * @return le aRETOP4
   */
  public String getRETOP4() {
    return RETOP4;
  }
  
  /**
   * @param aRETOP4 le aRETOP4 à définir
   */
  public void setRETOP4(String aRETOP4) {
    RETOP4 = aRETOP4;
  }
  
  /**
   * @return le aRETOP5
   */
  public String getRETOP5() {
    return RETOP5;
  }
  
  /**
   * @param aRETOP5 le aRETOP5 à définir
   */
  public void setRETOP5(String aRETOP5) {
    RETOP5 = aRETOP5;
  }
  
  /**
   * @return le aREASPA
   */
  public char getREASPA() {
    return REASPA;
  }
  
  /**
   * @param aREASPA le aREASPA à définir
   */
  public void setREASPA(char aREASPA) {
    REASPA = aREASPA;
  }
  
  /**
   * @return le aREAAPP
   */
  public char getREAAPP() {
    return REAAPP;
  }
  
  /**
   * @param aREAAPP le aREAAPP à définir
   */
  public void setREAAPP(char aREAAPP) {
    REAAPP = aREAAPP;
  }
  
  /**
   * @return le aRETYZP
   */
  public char getRETYZP() {
    return RETYZP;
  }
  
  /**
   * @param aRETYZP le aRETYZP à définir
   */
  public void setRETYZP(char aRETYZP) {
    RETYZP = aRETYZP;
  }
  
  /**
   * @return le aRENOM
   */
  public String getRENOM() {
    return RENOM;
  }
  
  /**
   * @param aRENOM le aRENOM à définir
   */
  public void setRENOM(String aRENOM) {
    RENOM = aRENOM;
  }
  
  /**
   * @return le aREPRE
   */
  public String getREPRE() {
    return REPRE;
  }
  
  /**
   * @param aREPRE le aREPRE à définir
   */
  public void setREPRE(String aREPRE) {
    REPRE = aREPRE;
  }
  
  /**
   * @return le aRENET2
   */
  public String getRENET2() {
    return RENET2;
  }
  
  /**
   * @param aRENET2 le aRENET2 à définir
   */
  public void setRENET2(String aRENET2) {
    RENET2 = aRENET2;
  }
  
  /**
   * @return le aREPRF
   */
  public String getREPRF() {
    return REPRF;
  }
  
  /**
   * @param aREPRF le aREPRF à définir
   */
  public void setREPRF(String aREPRF) {
    REPRF = aREPRF;
  }
  
  /**
   * @return le aREIN4
   */
  public char getREIN4() {
    return REIN4;
  }
  
  /**
   * @param aREIN4 le aREIN4 à définir
   */
  public void setREIN4(char aREIN4) {
    REIN4 = aREIN4;
  }
  
  /**
   * @return le aREIN5
   */
  public char getREIN5() {
    return REIN5;
  }
  
  /**
   * @param aREIN5 le aREIN5 à définir
   */
  public void setREIN5(char aREIN5) {
    REIN5 = aREIN5;
  }
  
  /**
   * @return le aREIN6
   */
  public char getREIN6() {
    return REIN6;
  }
  
  /**
   * @param aREIN6 le aREIN6 à définir
   */
  public void setREIN6(char aREIN6) {
    REIN6 = aREIN6;
  }
  
  /**
   * @return le aREIN7
   */
  public char getREIN7() {
    return REIN7;
  }
  
  /**
   * @param aREIN7 le aREIN7 à définir
   */
  public void setREIN7(char aREIN7) {
    REIN7 = aREIN7;
  }
  
  /**
   * @return le aREIN8
   */
  public char getREIN8() {
    return REIN8;
  }
  
  /**
   * @param aREIN8 le aREIN8 à définir
   */
  public void setREIN8(char aREIN8) {
    REIN8 = aREIN8;
  }
  
}
