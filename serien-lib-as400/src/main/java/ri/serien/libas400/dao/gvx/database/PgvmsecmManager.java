/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import java.sql.Connection;
import java.util.ArrayList;

import com.ibm.as400.access.Record;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.DataStructureRecord;
import ri.serien.libas400.database.record.ExtendRecord;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class PgvmsecmManager extends QueryManager {
  
  /**
   * Constructeur.
   */
  public PgvmsecmManager(Connection pDatabase) {
    super(pDatabase);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne l'établissement de l'utilisateur s'il existe, null sinon.
   */
  public IdEtablissement getEtablissementUser(String pProfil) {
    ArrayList<GenericRecord> records =
        select("Select SEZON1 from " + getLibrary() + ".pgvmsec where SEMDM='GVM' and SEUSR='" + pProfil + "' and SEETB=''");
    
    // A priori, il n'y a qu'un seul enregistrement par profil/etb (à vérifier)
    if (records.size() == 0) {
      return null;
    }
    
    // On initiaise les données
    GenericRecord record = records.get(0);
    return IdEtablissement.getInstance(record.getString("SEZON1").substring(0, 3));
  }
  
  /**
   * Retourne les enregistrements pour un utilisateur.
   */
  public ExtendRecord getRecordsbyUSERandETB(String pProfil, IdEtablissement pIdEtablissement) {
    pProfil = Constantes.normerTexte(pProfil);
    
    // Si l'utilisateur est à blanc alors on utilise la description pour la sécurité globale
    String ds;
    if (pProfil.isEmpty()) {
      ds = getClass().getPackage().getName() + ".Pgvmsecds_GLOBAL";
    }
    else {
      // sinon on utilise la description pour la sécurité user
      ds = getClass().getPackage().getName() + ".Pgvmsecds_USER";
    }
    // Vérifier si l'établissement existe
    if (pIdEtablissement == null || pIdEtablissement.getCodeEtablissement() == null) {
      return null;
    }
    String codeEtb = pIdEtablissement.getCodeEtablissement().trim();
    final ArrayList<Record> listeRecord = selectWithDecryption(
        "Select SETOP, SECRE, SEMOD, SETRT, SEMDM, SEUSR, SEETB, SEZON1, SEZON2 from " + getLibrary() + ".PGVMSEC where SEUSR = '"
            + pProfil + "' and SEETB='" + codeEtb + "' and SEMDM = 'GVM'",
        (DataStructureRecord) newObject(ds), 29, 400);
    
    // A priori, il n'y a qu'un seul enregistrement par profil/etb (à vérifier)
    if (listeRecord.size() == 0) {
      return null;
    }
    
    // Initialisation du record
    ExtendRecord record = new ExtendRecord();
    record.setRecord(listeRecord.get(0));
    return record;
  }
}
