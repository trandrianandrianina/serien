/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.database.field.FieldAlpha;
import ri.serien.libas400.database.field.FieldDecimal;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

public class GestionTarif {
  // Constantes
  private static final int LG_DATASTRUCT = 109;
  
  private static final int LG_ETB = 3;
  private static final int LG_NUMCLI = 6;
  private static final int LG_LIV = 3;
  private static final int LG_CODEART = 20;
  private static final int LG_QTE = 11;
  private static final int LG_QTE_DEC = 3;
  private static final int LG_CNV = 38;
  private static final int LG_TARIF = 9;
  private static final int LG_TARIF_DEC = 2;
  
  // Variables spécifiques du programme RPG
  private char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', ' ', ' ' };
  private FieldAlpha etablissement = new FieldAlpha(LG_ETB);
  private FieldDecimal numClient = new FieldDecimal(LG_NUMCLI, 0);
  private FieldDecimal livClient = new FieldDecimal(new BigDecimal(0), LG_LIV, 0);
  private FieldAlpha codeArticle = new FieldAlpha(LG_CODEART);
  private FieldDecimal quantite = new FieldDecimal(new BigDecimal(1), LG_QTE, LG_QTE_DEC);
  private FieldAlpha nope1 = new FieldAlpha("", 7);
  private FieldAlpha codeCNV = new FieldAlpha("", LG_CNV);
  private FieldAlpha nope2 = new FieldAlpha("", 2);
  private FieldDecimal tarif = new FieldDecimal(new BigDecimal(0), LG_TARIF, LG_TARIF_DEC);
  
  // Varibles
  private boolean init = false;
  private SystemeManager system = null;
  private ProgramParameter[] parameterList = new ProgramParameter[1];
  private AS400Text ppar1 = new AS400Text(LG_DATASTRUCT);
  private char lettre = 'W';
  private String curlib = null;
  private ContexteProgrammeRPG rpg = null;
  private StringBuilder sb = new StringBuilder();
  private Bibliotheque bibliothequeEnvironnement = null;
  
  /**
   * Constructeur
   * @param sys
   * @param let
   * @param curlib
   */
  public GestionTarif(SystemeManager sys, char let, String curlib, Bibliotheque pBibliothequeEnvironnement) {
    if (pBibliothequeEnvironnement == null) {
      throw new MessageErreurException("La bibliothèque d'environnement est invalide.");
    }
    system = sys;
    setLettre(let);
    setCurlib(curlib);
    bibliothequeEnvironnement = pBibliothequeEnvironnement;
  }
  
  /**
   * Initialise l'environnment d'execution du programme RPG
   * @return
   */
  public boolean init() {
    if (init) {
      return true;
    }
    
    parameterList[0] = new ProgramParameter(LG_DATASTRUCT);
    rpg = new ContexteProgrammeRPG(system.getSystem(), curlib);
    init = rpg.initialiserLettreEnvironnement(lettre);
    if (!init) {
      return false;
    }
    
    // init = rpg.addLibrary(curlib, true);
    rpg.ajouterBibliotheque(curlib, true);
    rpg.ajouterBibliotheque(bibliothequeEnvironnement.getNom(), false);
    rpg.ajouterBibliotheque(lettre + "EXPAS", false);
    rpg.ajouterBibliotheque(lettre + "GVMAS", false);
    
    // Pourquoi juste le test sur cet addlibl ? du coup ca fausse la partie. Peut-être faudrait-il juste changer et tester init sur
    // l'execute plus haut
    init = rpg.ajouterBibliotheque(lettre + "GVMX", false);
    
    return init;
  }
  
  /**
   * Effectue la recherche du tarif
   * @param valEtb
   * @param valNumCli
   * @param valLivCli
   * @param valCodeArt
   * @param valQte
   * @return
   */
  public boolean execute(IdEtablissement pIdEtablissement, BigDecimal valNumCli, BigDecimal valLivCli, String valCodeArt,
      BigDecimal valQte) {
    setValueEtablissement(pIdEtablissement.getCodeEtablissement());
    setValueNumClient(valNumCli);
    setValueLivClient(valLivCli);
    setValueCodeArticle(valCodeArt);
    setValueQuantite(valQte);
    setValueTarif(new BigDecimal(0));
    return execute();
  }
  
  /**
   * Effectue la recherche du tarif
   * @return
   */
  private boolean execute() {
    if (rpg == null) {
      return false;
    }
    
    // Construction du paramètre AS400 PPAR1
    sb.setLength(0);
    sb.append(getIndicateurs()).append(etablissement.toFormattedString()).append(numClient.toFormattedString())
        .append(livClient.toFormattedString()).append(codeArticle.toFormattedString()).append(quantite.toFormattedString())
        .append(nope1.toFormattedString()).append(codeCNV.toFormattedString()).append(nope2.toFormattedString())
        .append(tarif.toFormattedString());
    try {
      parameterList[0].setInputData(ppar1.toBytes(sb.toString()));
      // Appel du programme RPG
      Bibliotheque bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(lettre + "GVMAS"), EnumTypeBibliotheque.PROGRAMMES_SERIEN);
      if (rpg.executerProgramme("SGVMCCA1", bibliotheque, parameterList)) {
        // Récupération de la variable XPPVC de la DS PPAR1 (voir SGVMCCA1)
        String valeur = (String) ppar1.toObject(parameterList[0].getOutputData());
        if (valeur != null) {
          tarif.setValue(new BigDecimal(valeur.substring(100, 109)));
        }
        return true;
      }
    }
    catch (PropertyVetoException e) {
    }
    return false;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Formate une variable de type String (avec cadrage à droite, notamment pour l'établissement)
   * @param valeur
   * @param lg
   * @return
   */
  private String formateVar(String valeur, int lg) {
    if (valeur == null) {
      return String.format("%" + lg + "." + lg + "s", "");
    }
    else {
      return String.format("%" + lg + "." + lg + "s", valeur);
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le lettre
   */
  public char getLettre() {
    return lettre;
  }
  
  /**
   * @param lettre le lettre à définir
   */
  public void setLettre(char lettre) {
    if (lettre != ' ') {
      this.lettre = lettre;
    }
  }
  
  /**
   * @return le curlib
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib le curlib à définir
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  /**
   * @return le indicateurs
   */
  public char[] getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * @param indicateurs le indicateurs à définir
   */
  public void setIndicateurs(char[] indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  /**
   * Retourne le champ Etablissement
   * @return le etablissement
   */
  public FieldAlpha getEtablissement() {
    return etablissement;
  }
  
  /**
   * Initialise la valeur de l'établissement
   * @param etablissement le etablissement à définir
   */
  public void setValueEtablissement(String etablissement) {
    this.etablissement.setValue(formateVar(etablissement, LG_ETB));
  }
  
  /**
   * Retourne le champ Numéro client
   * @return le numClient
   */
  public FieldDecimal getNumClient() {
    return numClient;
  }
  
  /**
   * Initialise la valeur du numéro client
   * @param numClient le numClient à définir
   */
  public void setValueNumClient(BigDecimal numClient) {
    this.numClient.setValue(numClient);
  }
  
  /**
   * Retourne le champ suffixe liv
   * @return le liv
   */
  public FieldDecimal getLivClient() {
    return livClient;
  }
  
  /**
   * Initialise la valeur du suffixe liv
   * @param liv le liv à définir
   */
  public void setValueLivClient(BigDecimal liv) {
    this.livClient.setValue(liv);
  }
  
  /**
   * Retourne le champ code Article
   * @return le codeArticle
   */
  public FieldAlpha getCodeArticle() {
    return codeArticle;
  }
  
  /**
   * Initialise la valeur du code Article
   * @param codeArticle le codeArticle à définir
   */
  public void setValueCodeArticle(String codeArticle) {
    this.codeArticle.setValue(codeArticle);
  }
  
  /**
   * Retourne le champ Quantité
   * @return le quantite
   */
  public FieldDecimal getQuantite() {
    return quantite;
  }
  
  /**
   * Initialise la valeur de la quantité
   * @param quantite le quantite à définir
   */
  public void setValueQuantite(BigDecimal quantite) {
    this.quantite.setValue(quantite);
  }
  
  /**
   * Retourne le champ codeCNV
   * @return le codeCNV
   */
  public FieldAlpha getCodeCNV() {
    return codeCNV;
  }
  
  /**
   * Initialise la valeur du code CNV
   * @param codeCNV le codeCNV à définir
   */
  public void setValueCodeCNV(String codeCNV) {
    this.codeCNV.setValue(codeCNV);
  }
  
  /**
   * Récupère le champ tarif après l'appel du programme RPG
   * @return
   */
  public FieldDecimal getTarif() {
    return tarif;
  }
  
  /**
   * Initialise la valeur du tarif
   * @param tarif le tarif à définir
   */
  public void setValueTarif(BigDecimal tarif) {
    this.tarif.setValue(tarif);
  }
  
}
