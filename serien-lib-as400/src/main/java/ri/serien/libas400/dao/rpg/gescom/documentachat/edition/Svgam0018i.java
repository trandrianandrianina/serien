/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.edition;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0018i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIOPT = 3;
  public static final int SIZE_PIEDT = 3;
  public static final int SIZE_PINOM = 30;
  public static final int SIZE_PISTK = 1;
  public static final int SIZE_PITDSK = 2;
  public static final int DECIMAL_PITDSK = 0;
  public static final int SIZE_PIIDDC = 20;
  public static final int SIZE_PINML = 9;
  public static final int DECIMAL_PINML = 0;
  public static final int SIZE_PINFL = 9;
  public static final int DECIMAL_PINFL = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 106;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PIDAT = 5;
  public static final int VAR_PIOPT = 6;
  public static final int VAR_PIEDT = 7;
  public static final int VAR_PINOM = 8;
  public static final int VAR_PISTK = 9;
  public static final int VAR_PITDSK = 10;
  public static final int VAR_PIIDDC = 11;
  public static final int VAR_PINML = 12;
  public static final int VAR_PINFL = 13;
  public static final int VAR_PIARR = 14;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "E" *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal pidat = BigDecimal.ZERO; // Date de traitement
  private String piopt = ""; // Option de traitement *
  private String piedt = ""; // Option d édition *
  private String pinom = ""; // Non utilisé
  private String pistk = ""; // Stockage base documentaire
  private BigDecimal pitdsk = BigDecimal.ZERO; // Type du document stocké
  private String piiddc = ""; // Indicatif du doc à stocker
  private BigDecimal pinml = BigDecimal.ZERO; // Numéro id mail
  private BigDecimal pinfl = BigDecimal.ZERO; // Numéro id mail pour le fax
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "E" *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date de traitement
      new AS400Text(SIZE_PIOPT), // Option de traitement *
      new AS400Text(SIZE_PIEDT), // Option d édition *
      new AS400Text(SIZE_PINOM), // Non utilisé
      new AS400Text(SIZE_PISTK), // Stockage base documentaire
      new AS400ZonedDecimal(SIZE_PITDSK, DECIMAL_PITDSK), // Type du document stocké
      new AS400Text(SIZE_PIIDDC), // Indicatif du doc à stocker
      new AS400ZonedDecimal(SIZE_PINML, DECIMAL_PINML), // Numéro id mail
      new AS400ZonedDecimal(SIZE_PINFL, DECIMAL_PINFL), // Numéro id mail pour le fax
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, picod, pietb, pinum, pisuf, pidat, piopt, piedt, pinom, pistk, pitdsk, piiddc, pinml, pinfl, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pidat = (BigDecimal) output[5];
    piopt = (String) output[6];
    piedt = (String) output[7];
    pinom = (String) output[8];
    pistk = (String) output[9];
    pitdsk = (BigDecimal) output[10];
    piiddc = (String) output[11];
    pinml = (BigDecimal) output[12];
    pinfl = (BigDecimal) output[13];
    piarr = (String) output[14];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPiopt(String pPiopt) {
    if (pPiopt == null) {
      return;
    }
    piopt = pPiopt;
  }
  
  public String getPiopt() {
    return piopt;
  }
  
  public void setPiedt(String pPiedt) {
    if (pPiedt == null) {
      return;
    }
    piedt = pPiedt;
  }
  
  public String getPiedt() {
    return piedt;
  }
  
  public void setPinom(String pPinom) {
    if (pPinom == null) {
      return;
    }
    pinom = pPinom;
  }
  
  public String getPinom() {
    return pinom;
  }
  
  public void setPistk(Character pPistk) {
    if (pPistk == null) {
      return;
    }
    pistk = String.valueOf(pPistk);
  }
  
  public Character getPistk() {
    return pistk.charAt(0);
  }
  
  public void setPitdsk(BigDecimal pPitdsk) {
    if (pPitdsk == null) {
      return;
    }
    pitdsk = pPitdsk.setScale(DECIMAL_PITDSK, RoundingMode.HALF_UP);
  }
  
  public void setPitdsk(Integer pPitdsk) {
    if (pPitdsk == null) {
      return;
    }
    pitdsk = BigDecimal.valueOf(pPitdsk);
  }
  
  public Integer getPitdsk() {
    return pitdsk.intValue();
  }
  
  public void setPiiddc(String pPiiddc) {
    if (pPiiddc == null) {
      return;
    }
    piiddc = pPiiddc;
  }
  
  public String getPiiddc() {
    return piiddc;
  }
  
  public void setPinml(BigDecimal pPinml) {
    if (pPinml == null) {
      return;
    }
    pinml = pPinml.setScale(DECIMAL_PINML, RoundingMode.HALF_UP);
  }
  
  public void setPinml(Integer pPinml) {
    if (pPinml == null) {
      return;
    }
    pinml = BigDecimal.valueOf(pPinml);
  }
  
  public Integer getPinml() {
    return pinml.intValue();
  }
  
  public void setPinfl(BigDecimal pPinfl) {
    if (pPinfl == null) {
      return;
    }
    pinfl = pPinfl.setScale(DECIMAL_PINFL, RoundingMode.HALF_UP);
  }
  
  public void setPinfl(Integer pPinfl) {
    if (pPinfl == null) {
      return;
    }
    pinfl = BigDecimal.valueOf(pPinfl);
  }
  
  public Integer getPinfl() {
    return pinfl.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
