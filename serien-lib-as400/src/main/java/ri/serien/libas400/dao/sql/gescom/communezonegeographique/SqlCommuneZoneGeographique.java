/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.communezonegeographique;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.CommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.CritereCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.IdCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.ListeCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * SQL contenant les données des communes liée au zone géographique
 */
public class SqlCommuneZoneGeographique {
  
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlCommuneZoneGeographique(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  /**
   * Charge les communes qui constituent une zone géographique donnée
   */
  public ListeCommuneZoneGeographique chargerListeCommuneZoneGeographique(IdZoneGeographique pIdZoneGeographique) {
    if (pIdZoneGeographique == null) {
      throw new MessageErreurException("L'identifiant de la zone géographique est invalide");
    }
    
    String requete =
        "Select ZGETB, ZGMAG, ZGNOM, ZGCDP, ZGCOM from " + querymg.getLibrary() + '.' + EnumTableBDD.ZONE_GEOGRAPHIQUE
            + "  where ZGETB = " + RequeteSql.formaterStringSQL(pIdZoneGeographique.getIdEtablissement().getCodeEtablissement())
            + " and ZGNOM = " + RequeteSql.formaterStringSQL(pIdZoneGeographique.getCode());
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On alimente la liste avec les enregistrements trouvés
    ListeCommuneZoneGeographique listeCommuneZoneGeographique = new ListeCommuneZoneGeographique();
    
    for (GenericRecord record : listeRecord) {
      if (record != null) {
        listeCommuneZoneGeographique.add(initialiserCommuneZoneGeographique(record));
      }
    }
    
    return listeCommuneZoneGeographique;
  }
  
  /**
   * Charge les communes sans aucune distinction de zone géographique
   */
  public ListeCommuneZoneGeographique chargerListeCommuneZoneGeographique(CritereCommuneZoneGeographique pCriteres) {
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères de recherche sont invalide");
    }
    
    String requete = "Select * from " + querymg.getLibrary() + '.' + EnumTableBDD.ZONE_GEOGRAPHIQUE + " where ZGETB = "
        + RequeteSql.formaterStringSQL(pCriteres.getIdEtablissement().getCodeEtablissement());
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On alimente la liste avec les enregistrements trouvés
    ListeCommuneZoneGeographique listeCommuneZoneGeographique = new ListeCommuneZoneGeographique();
    
    for (GenericRecord record : listeRecord) {
      if (record != null) {
        listeCommuneZoneGeographique.add(initialiserCommuneZoneGeographique(record));
      }
    }
    return listeCommuneZoneGeographique;
  }
  
  /**
   * Initialise une commune à partir de l'enregistrement donnée et du type de la recherche
   */
  private CommuneZoneGeographique initialiserCommuneZoneGeographique(GenericRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance((pRecord.getString("ZGETB").trim()));
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, pRecord.getString("ZGMAG").trim());
    IdZoneGeographique idZoneGeographique = IdZoneGeographique.getInstance(idEtablissement, (pRecord.getString("ZGNOM").trim()));
    IdCommuneZoneGeographique idCommuneZoneGeographique =
        IdCommuneZoneGeographique.getInstance(idEtablissement, idMagasin, idZoneGeographique, pRecord.getIntegerValue("ZGCDP", 0));
    CommuneZoneGeographique communeZoneGeographique = new CommuneZoneGeographique(idCommuneZoneGeographique);
    communeZoneGeographique.setVille(pRecord.getString("ZGCOM").trim());
    return communeZoneGeographique;
  }
}
