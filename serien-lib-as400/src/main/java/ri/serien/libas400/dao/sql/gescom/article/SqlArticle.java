/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.article;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.SqlBlocNote;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.recherche.ArgumentRecherche;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.CritereArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.CriteresRechercheArticles;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleConsigne;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleSpecial;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleStock;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreDirectUsine;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreHorsGamme;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.EnumTypeDecoupeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libcommun.gescom.commun.article.ListeMarques;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.blocnotes.IdBlocNote;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.famille.IdFamille;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.IdSousFamille;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlArticle {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlArticle(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  /**
   * Lire les données de l'article dont on fourni l'identifiant.
   */
  public Article chargerArticle(IdArticle pIdArticle) {
    return chargerArticle(new Article(pIdArticle));
  }
  
  /**
   * Lire les données de l'article fourni.
   * Les données de l'article sont complétées avec celles nouvellement chargées.
   */
  public Article chargerArticle(Article pArticle) {
    // Contrôler l'identifiant
    Article.controlerId(pArticle, true);
    
    // Générer la requête
    String requete = "select art.*, A1LB1, A1LB2, A1LB3 from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " art"
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_EXTENSION
        + " eart on art.A1ETB = eart.A1ETB and art.A1ART = eart.A1ART" + " where art.A1ETB = '" + pArticle.getId().getCodeEtablissement()
        + "' and art.A1ART = '" + pArticle.getId().getCodeArticle() + "'";
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      throw new MessageErreurException("Aucun article trouvé avec l'identifiant suivant :" + pArticle.getId());
    }
    else if (listeRecord.size() > 1) {
      throw new MessageErreurException("Plusieurs articles ont été trouvé avec l'identifiant suivant :" + pArticle.getId());
    }
    try {
      return completerArticle(pArticle, listeRecord.get(0), true);
    }
    catch (Exception e) {
      Trace.alerte("Erreur d'instanciation de l'Article");
      return null;
    }
  }
  
  /**
   * vérifie l'existance en base d'un article donnée.
   * 
   */
  public boolean verifierExistenceArticle(IdArticle pIdArticle) {
    IdArticle.controlerId(pIdArticle, true);
    
    String requete = "select * from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " where A1ETB = '"
        + pIdArticle.getCodeEtablissement() + "' and A1ART = '" + pIdArticle.getCodeArticle() + "'";
    int nombreResultats = querymg.nbrEnrgSelect(requete);
    return nombreResultats >= 1;
  }
  
  /**
   * Créer un article à partir des champs de la base de données.
   */
  private Article completerArticle(Article pArticle, GenericRecord record, boolean pArticleCharge) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("A1ETB", "", false));
    if (pArticle == null) {
      pArticle = new Article(IdArticle.getInstance(idEtablissement, record.getStringValue("A1ART")));
    }
    pArticle.setArticleCharge(pArticleCharge);
    pArticle.setTopSysteme(record.getIntegerValue("A1TOP"));
    pArticle.setDateCreation(record.getDateValue("A1CRE"));
    pArticle.setDateModification(record.getDateValue("A1MOD"));
    pArticle.setDateTraitement(record.getDateValue("A1TRT"));
    pArticle.setTopNePlusUtiliser(record.getStringValue("A1NPU"));
    if (!record.getStringValue("A1FAM").trim().isEmpty()) {
      IdFamille idFamille = IdFamille.getInstance(idEtablissement, record.getStringValue("A1FAM"));
      pArticle.setIdFamille(idFamille);
    }
    else {
      pArticle.setIdFamille(null);
    }
    pArticle.setLibelle1(record.getStringValue("A1LIB", "", false));
    pArticle.setLibelle2(record.getStringValue("A1LB1", "", false)); // Vient de la lecture de l'extention article
    pArticle.setLibelle3(record.getStringValue("A1LB2", "", false)); // Vient de la lecture de l'extention article
    pArticle.setLibelle4(record.getStringValue("A1LB3", "", false)); // Vient de la lecture de l'extention article
    pArticle.setClefClassement1(record.getStringValue("A1CL1"));
    pArticle.setClefClassement2(record.getStringValue("A1CL2"));
    pArticle.setArticleRemplacementStk(record.getStringValue("A1ASB"));
    pArticle.setGencode(record.getBigDecimalValue("A1GCD").toPlainString());
    pArticle.setNomenclatureProduit(record.getStringValue("A1NOP"));
    pArticle.setArticleNomenclature(record.getIntegerValue("A1NMC"));
    pArticle.setRegroupementStatistique(record.getStringValue("A1RST"));
    pArticle.setCritereAnalyse1(record.getStringValue("A1TOP1"));
    pArticle.setCritereAnalyse2(record.getStringValue("A1TOP2"));
    pArticle.setCritereAnalyse3(record.getStringValue("A1TOP3"));
    pArticle.setCritereAnalyse4(record.getStringValue("A1TOP4"));
    pArticle.setCritereAnalyse5(record.getStringValue("A1TOP5"));
    pArticle.setTopDesignation1(record.getStringValue("A1DSG1"));
    pArticle.setTopDesignation2(record.getStringValue("A1DSG2"));
    pArticle.setTopDesignation3(record.getStringValue("A1DSG3"));
    pArticle.setTopDesignation4(record.getStringValue("A1DSG4"));
    pArticle.setCodeTarif(record.getStringValue("A1RTA"));
    pArticle.setRattachementCNV(record.getStringValue("A1CNV"));
    pArticle.setDateDerniereVente(record.getIntegerValue("A1DDV"));
    pArticle.setCodeTVA(record.getIntegerValue("A1TVA"));
    pArticle.setCodeTaxeParafiscale(record.getIntegerValue("A1TPF"));
    pArticle.setNonStocke(record.getIntegerValue("A1STK"));
    pArticle.setNombreDecimaleUS(record.getIntegerValue("A1DCS"));
    pArticle.setNombreDecimaleUV(record.getIntegerValue("A1DCV"));
    pArticle.setArticleUtiliseEnSaisie(record.getIntegerValue("A1SAI"));
    pArticle.setTopNonCommissionne(record.getIntegerValue("A1TNC"));
    pArticle.setCodeArrondi(record.getIntegerValue("A1RON"));
    if (!record.getStringValue("A1UNV").trim().isEmpty()) {
      IdUnite idUV = IdUnite.getInstance(record.getStringValue("A1UNV"));
      pArticle.setIdUV(idUV);
    }
    pArticle.setCoefficientUSParUV(record.getBigDecimalValue("A1KSV"));
    pArticle.setCoefficientMarge(record.getBigDecimalValue("A1CMA"));
    pArticle.setPourcentageMargeMini(record.getIntegerValue("A1PMM"));
    pArticle.setPourcentageRemiseMaxi(record.getIntegerValue("A1PMR"));
    if (record.getIntegerValue("A1COF") != 0 && record.getIntegerValue("A1FRS") != 0) {
      IdFournisseur idFournisseur =
          IdFournisseur.getInstance(idEtablissement, record.getIntegerValue("A1COF"), record.getIntegerValue("A1FRS"));
      pArticle.setIdFournisseur(idFournisseur);
    }
    pArticle.setDernierPrixAchat(record.getBigDecimalValue("A1PDA"));
    pArticle.setDerniereDateAchat(record.getDateValue("A1DDA"));
    pArticle.setTopReappro(record.getIntegerValue("A1REA"));
    pArticle.setDateDebutSaison(record.getDateValue("A1PERD"));
    pArticle.setDateFinSaison(record.getDateValue("A1PERF"));
    pArticle.setPourcentageConso(record.getIntegerValue("A1PERK"));
    pArticle.setDateDebutSaison2(record.getDateValue("A1PE2D"));
    pArticle.setDateFinSaison2(record.getDateValue("A1PE2F"));
    pArticle.setPourcentageConso2(record.getIntegerValue("A1PE2K"));
    pArticle.setProtectionPrixRevient(record.getIntegerValue("A1TPR"));
    pArticle.setCodePrixRevient(record.getStringValue("A1CPR"));
    
    pArticle.setPrixDeRevientStandardHT(record.getBigDecimalValue("A1PRS"));
    pArticle.setPrixDeRevientHT(record.getBigDecimalValue("A1PRV"));
    
    if (!record.getStringValue("A1UNS").trim().isEmpty()) {
      IdUnite idUNS = IdUnite.getInstance(record.getStringValue("A1UNS"));
      pArticle.setIdUS(idUNS);
    }
    pArticle.setPoidsEnUS(record.getBigDecimalValue("A1PDS"));
    pArticle.setVolume(record.getBigDecimalValue("A1VOL"));
    pArticle.setColisage(record.getIntegerValue("A1COL"));
    pArticle.setRegroupementColisage(record.getStringValue("A1RGC"));
    pArticle.setNombreUVParUCV(record.getBigDecimalValue("A1CND"));
    pArticle.setConditionnement2(record.getBigDecimalValue("A1CND2"));
    pArticle.setConditionnement3(record.getBigDecimalValue("A1CND3"));
    pArticle.setConditionnement4(record.getBigDecimalValue("A1CND4"));
    if (!record.getStringValue("A1UNL").trim().isEmpty()) {
      IdUnite idUCV = IdUnite.getInstance(record.getStringValue("A1UNL"));
      pArticle.setIdUCV(idUCV);
    }
    if (!record.getStringValue("A1UNL2").trim().isEmpty()) {
      IdUnite idUCV2 = IdUnite.getInstance(record.getStringValue("A1UNL2"));
      pArticle.setIdUCV2(idUCV2);
    }
    if (!record.getStringValue("A1UNL3").trim().isEmpty()) {
      IdUnite idUCV3 = IdUnite.getInstance(record.getStringValue("A1UNL3"));
      pArticle.setIdUCV3(idUCV3);
    }
    if (!record.getStringValue("A1UNL4").trim().isEmpty()) {
      IdUnite idUCV4 = IdUnite.getInstance(record.getStringValue("A1UNL4"));
      pArticle.setIdUCV4(idUCV4);
    }
    pArticle.setTopRattachementUnitaire2(record.getStringValue("A1TRU2"));
    pArticle.setTopRattachementUnitaire3(record.getStringValue("A1TRU3"));
    pArticle.setTopRattachementUnitaire4(record.getStringValue("A1TRU4"));
    pArticle.setPrixInventaire(record.getBigDecimalValue("A1PRI"));
    pArticle.setPrixFabricationOuMiniVente(record.getBigDecimalValue("A1PFB"));
    pArticle.setMontantDepreciation(record.getBigDecimalValue("A1MDP"));
    pArticle.setPourcentageDepreciation(record.getBigDecimalValue("A1PDP"));
    pArticle.setCodeDepreciation(record.getStringValue("A1CDP"));
    if (!record.getStringValue("A1MAG").trim().isEmpty()) {
      pArticle.setIdMagasin(IdMagasin.getInstance(idEtablissement, record.getStringValue("A1MAG")));
    }
    pArticle.setCodeModeExpedition(record.getStringValue("A1MEX"));
    pArticle.setTopNonSoumisEscompte(record.getStringValue("A1IN1"));
    pArticle.setTopGestionParLots(record.getStringValue("A1IN2"));
    pArticle.setTypeFicheArticle(record.getStringValue("A1IN3"));
    pArticle.setTypeSubstitutionASB(record.getStringValue("A1IN4"));
    pArticle.setTopNonGereEnStats(record.getStringValue("A1IN5"));
    pArticle.setTopNonEditionDetailNomenclature(record.getStringValue("A1IN6"));
    pArticle.setTypeExtensionArticleParLigne(record.getStringValue("A1IN7"));
    pArticle.setUniteConditionnementVenteNonScindable(!record.getStringValue("A1IN8").isEmpty());
    pArticle.setTopExclusionRemisesGlobales(record.getStringValue("A1IN9"));
    pArticle.setTopN11Specif1(record.getIntegerValue("A1N11"));
    pArticle.setTopZoneUtiliseeWspm001171(record.getIntegerValue("A1N12"));
    pArticle.setTopN51Specif1(record.getBigDecimalValue("A1N51"));
    pArticle.setTopN71Specif1(record.getBigDecimalValue("A1N71"));
    pArticle.setDateDebutCommercialisation(record.getDateValue("A1DAT1"));
    pArticle.setDateFinReappro(record.getDateValue("A1DAT2"));
    pArticle.setDateFinCommercialisation(record.getDateValue("A1DAT3"));
    
    pArticle.setTopEditionBonPreparation(!record.getStringValue("A1EDT1").isEmpty());
    pArticle.setTopAccuseReceptionCommande(!record.getStringValue("A1EDT2").isEmpty());
    pArticle.setTopBonExpedition(!record.getStringValue("A1EDT3").isEmpty());
    pArticle.setTopEditionBonTransporteur(!record.getStringValue("A1EDT4").isEmpty());
    pArticle.setTopFacture(!record.getStringValue("A1EDT5").isEmpty());
    pArticle.setTopDevis(!record.getStringValue("A1EDT6").isEmpty());
    if (!record.getStringValue("A1UCS").trim().isEmpty()) {
      IdUnite idUCS = IdUnite.getInstance(record.getStringValue("A1UCS"));
      pArticle.setIdUCS(idUCS);
    }
    pArticle.setNombreUSParUCS(record.getBigDecimalValue("A1KCS"));
    if (!record.getStringValue("A1SFA").trim().isEmpty()) {
      IdSousFamille idSousFamille = IdSousFamille.getInstance(idEtablissement, record.getStringValue("A1SFA"));
      pArticle.setIdSousFamille(idSousFamille);
    }
    pArticle.setTopExclusionRistournes(record.getStringValue("A1IN10"));
    pArticle.setTopSpecificiteExtraction(record.getStringValue("A1IN11"));
    pArticle.setTopEnsembleMateriels(record.getStringValue("A1IN12"));
    pArticle.setTopTaxeAdditionnelle(record.getStringValue("A1IN13"));
    pArticle.setTopTaxeTgap(record.getStringValue("A1IN14"));
    pArticle.setTopExclusionWeb(record.getStringValue("A1IN15"));
    pArticle.setReference1(record.getStringValue("A1REF1"));
    pArticle.setCoefficientConditionSurQuantite(record.getBigDecimalValue("A1CCO"));
    pArticle.setTopRegimeDouanier(record.getStringValue("A1REC"));
    pArticle.setSystemeVariable1(record.getStringValue("A1SV1"));
    pArticle.setSystemeVariable2(record.getStringValue("A1SV2"));
    pArticle.setCodeUnitePresentation(record.getStringValue("A1UNP"));
    pArticle.setTypeChoixFournisseur(record.getStringValue("A1IN16"));
    pArticle.setTypeExtensionMateriel(record.getStringValue("A1IN17"));
    pArticle.setCodeUniteTransport(record.getStringValue("A1UNT"));
    pArticle.setNombreUniteStockageParUniteTransport(record.getBigDecimalValue("A1KTS"));
    pArticle.setLongueur(record.getBigDecimalValue("A1LNG"));
    pArticle.setLargeur(record.getBigDecimalValue("A1LRG"));
    pArticle.setHauteur(record.getBigDecimalValue("A1HTR"));
    pArticle.setPoidsEnUniteConditionnement(record.getBigDecimalValue("A1PDSL"));
    pArticle.setVolumeEnUniteConditionnement(record.getBigDecimalValue("A1VOLL"));
    pArticle.setTypeDateLimiteUtilisationOptimale(record.getStringValue("A1TDUO"));
    pArticle.setDelaiUtilisationOptimale1(record.getIntegerValue("A1DUO1"));
    pArticle.setDelaiUtilisationOptimale2(record.getIntegerValue("A1DUO2"));
    pArticle.setDelaiUtilisationOptimale3(record.getIntegerValue("A1DUO3"));
    pArticle.setPoidsNetEnUniteStock(record.getBigDecimalValue("A1PDN"));
    pArticle.setNomenclatureDouniere(record.getStringValue("A1NDP"));
    pArticle.setCodeOrigine(record.getStringValue("A1OPA"));
    pArticle.setTopElementFrais(record.getStringValue("A1IN18"));
    pArticle.setTypeRepartition(record.getStringValue("A1IN19"));
    pArticle.setTypeProduit(record.getStringValue("A1IN20"));
    pArticle.setCodeDisponibilite(record.getStringValue("A1IN21"));
    pArticle.setTopDepositaire(record.getStringValue("A1IN23"));
    pArticle.setDateSpecifGGEDRUBIS(record.getDateValue("A1DAT5"));
    pArticle.setDateFinGarantieFixe(record.getDateValue("A1DAT6"));
    pArticle.setQuantiteMaxPourAlerteEnUVouMultiple(record.getIntegerValue("A1QT1"));
    pArticle.setCodeUniteConditionnementVente5(record.getStringValue("A1UNL5"));
    pArticle.setConditionnement5(record.getBigDecimalValue("A1CND5"));
    pArticle.setZonePersoA20(record.getStringValue("A1ZP20"));
    pArticle.setZonePersoA21(record.getStringValue("A1ZP21"));
    pArticle.setCodeNonSuivi(record.getStringValue("A1TNS"));
    pArticle.setTopAutorisationVenteNonDispo(record.getStringValue("A1IN26"));
    pArticle.setTopNonSaisieDirecteSurBon(record.getStringValue("A1IN27"));
    pArticle.setTypeUniteLongueur(record.getStringValue("A1IN28"));
    pArticle.setTypeUniteLargeur(record.getStringValue("A1IN29"));
    pArticle.setTypeUniteHauteur(record.getStringValue("A1IN30"));
    pArticle.setTopConditionRepriseArticle(record.getStringValue("A1IN31"));
    pArticle.setMotDirecteur1(record.getStringValue("A1MOT1"));
    pArticle.setMotDirecteur2(record.getStringValue("A1MOT2"));
    pArticle.setMotDirecteur3(record.getStringValue("A1MOT3"));
    pArticle.setObservationCourte(record.getStringValue("A1OBS"));
    pArticle.setRattachementCNV1(record.getStringValue("A1CNV1"));
    pArticle.setRattachementCNV2(record.getStringValue("A1CNV2"));
    pArticle.setRattachementCNV3(record.getStringValue("A1CNV3"));
    pArticle.setDirectUsine(!record.getStringValue("A1IN32").isEmpty());
    pArticle.setAffichageMemoObligatoire(!record.getStringValue("A1IN33").isEmpty());
    
    /**
     * TYPE D'ARTICLE ET DECOUPE
     * L'information est stockée dans plusieurs champs en base. On les regroupe de manière compréhensible dans l'objet métier Article.
     * 
     * Article spécial -> code spécial == 1 (A1SPE)
     * Article commentaire -> pas de famille (A1FAM), top ne plus utilisé == 0 (A1NPU), zoneABC différente de R (A1ABC)
     * Article palette -> top spécifique == 0 (A1TSP)
     * Article transport -> top spécifique == T (A1TSP)
     * Article géré par numéros de série -> top spécifique == S (A1TSP)
     * Article négoce -> tout ce qui n'est pas les 4 autres.
     * Article hors gamme -> code ABC = 'R' (A1ABC)
     */
    String topSpecifique = record.getStringValue("A1TSP");
    if (pArticle.getTopNePlusUtiliser().equals("1")) {
      pArticle.setTypeArticle(EnumTypeArticle.PLUS_UTILISE);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (topSpecifique.equals("T")) {
      pArticle.setTypeArticle(EnumTypeArticle.TRANSPORT);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (Constantes.equals(record.getStringValue("A1FAM").trim(), "")) {
      pArticle.setTypeArticle(EnumTypeArticle.COMMENTAIRE);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (topSpecifique.equals("S")) {
      pArticle.setTypeArticle(EnumTypeArticle.SERIE);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (topSpecifique.equals("0")) {
      pArticle.setTypeArticle(EnumTypeArticle.PALETTE);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (topSpecifique.equals("1")) {
      pArticle.setTypeArticle(EnumTypeArticle.STANDARD);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.LONGUEUR);
    }
    else if (topSpecifique.equals("3")) {
      pArticle.setTypeArticle(EnumTypeArticle.STANDARD);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.VOLUME);
    }
    else if (topSpecifique.equals("4")) {
      pArticle.setTypeArticle(EnumTypeArticle.STANDARD);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.SURFACE);
    }
    else if (record.getIntegerValue("A1SPE") > 0) {
      pArticle.setTypeArticle(EnumTypeArticle.SPECIAL);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else {
      pArticle.setTypeArticle(EnumTypeArticle.STANDARD);
      pArticle.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    
    // Renseigner la classe de rotation
    String classeRotation = record.getStringValue("A1ABC").trim();
    if (classeRotation.length() == 1) {
      pArticle.setClasseRotation(classeRotation.charAt(0));
    }
    
    // Compléter avec les informations de stock
    chargerInfosStockArticles(pArticle);
    
    return pArticle;
  }
  
  /**
   * Recherche des articles transports à partir des critères donnés.
   */
  public ListeArticle chargerListeArticleTransport(CriteresRechercheArticles pCriteres) {
    // Construire la requête
    String requete = "select art.*, A1LB1, A1LB2, A1LB3 from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " art"
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_EXTENSION
        + " eart on art.A1ETB = eart.A1ETB and art.A1ART = eart.A1ART" + " where art.A1TSP = 'T'";
    
    // Ajouter une sélection sur l'établissement
    if (pCriteres.getCodeEtablissement() != null) {
      requete += " and art.A1ETB = " + RequeteSql.formaterStringSQL(pCriteres.getCodeEtablissement());
    }
    
    // Ajouter une sélection sur la zone géographique
    if (pCriteres.getIdZoneGeographique() != null) {
      requete += " and (art.A1CL1 = " + RequeteSql.formaterStringSQL(pCriteres.getIdZoneGeographique().getCode()) + " or art.A1CL2 = "
          + RequeteSql.formaterStringSQL(pCriteres.getIdZoneGeographique().getCode()) + ")";
    }
    
    // Ajouter une sélection sur le libellé
    if (pCriteres.getTexteRecherche() != null && !pCriteres.getTexteRecherche().isEmpty()) {
      requete += " and exists(select * from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_INDEX + " IDX" + " where"
          + " IDX.A1ETB = ART.A1ETB and IDX.A1ART = ART.A1ART" + " and IDX.A1TX1 like '" + pCriteres.getTexteRecherche() + "%')";
    }
    
    // Lancer la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    
    // Remplir le résultat
    ListeArticle listeArticle = new ListeArticle();
    if (listeRecord != null) {
      for (GenericRecord record : listeRecord) {
        // A ce stade l'article transport n'est pas complètement chargé car il manque notamment le prix de vente, information que ne peut
        // donner cette requête.
        // On force false afin que l'article soit chargé via le service lireListeArticlesSimples côté client
        try {
          listeArticle.add(completerArticle(null, record, false));
        }
        catch (Exception e) {
          Trace.alerte("Erreur d'instanciation de l'Article " + record.getStringValue("A1ART"));
        }
      }
    }
    return listeArticle;
  }
  
  /**
   * Recherche de tous les articles commentaires.
   */
  public ListeArticle chargerListeArticleCommentaire(CritereArticleCommentaire pCritere) {
    // Construire la requête
    String requete =
        "select art.*, A1LB1, A1LB2, A1LB3 from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " ART" + " left join "
            + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_EXTENSION + " EART on art.A1ETB = eart.A1ETB and art.A1ART = eart.A1ART"
            + " where art.A1FAM = '' and art.A1NPU <> '1'" + " and art.A1ABC <> 'R'";
    
    // Ajouter une sélection sur l'établissement
    if (pCritere.getCodeEtablissement() != null) {
      requete += " and ART.A1ETB = " + RequeteSql.formaterStringSQL(pCritere.getCodeEtablissement());
    }
    
    // Articles valides
    if (pCritere.isArticleValideSeulement() != null && pCritere.isArticleValideSeulement()) {
      requete += " and ART.A1TOP = '0'";
    }
    
    // Ajouter le filtrage sur les mots recherchés
    if (pCritere.getRechercheGenerique() != null && pCritere.getRechercheGenerique().getListeArgumentRecherche() != null) {
      for (ArgumentRecherche argumentRecherche : pCritere.getRechercheGenerique().getListeArgumentRecherche()) {
        switch (argumentRecherche.getType()) {
          case IGNORE:
            break;
          
          case TEXTE:
          case EMAIL:
          case IDENTIFIANT:
          case TELEPHONE:
          case ENTIER:
          case DECIMAL:
          case CODE_POSTAL:
            requete += " and (";
            requete += " upper(ART.A1ART) like '%" + argumentRecherche.getValeur() + "%'";
            requete += " or upper(ART.A1LIB) like '%" + argumentRecherche.getValeur() + "%'";
            requete += " or upper(EART.A1LB1) like '%" + argumentRecherche.getValeur() + "%'";
            requete += " or upper(EART.A1LB2) like '%" + argumentRecherche.getValeur() + "%'";
            requete += " or upper(EART.A1LB3) like '%" + argumentRecherche.getValeur() + "%'";
            requete += " )";
        }
      }
    }
    
    // Lancer la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    
    // Remplir le résultat
    ListeArticle listeArticles = new ListeArticle();
    if (listeRecord != null) {
      for (GenericRecord record : listeRecord) {
        try {
          listeArticles.add(completerArticle(null, record, false));
        }
        catch (Exception e) {
          Trace.alerte("Erreur d'instanciation de l'Article " + record.getStringValue("A1ART"));
        }
      }
    }
    return listeArticles;
  }
  
  /**
   * Charger les articles palettes correspondants aux critères de recherche.
   */
  public ListeArticle chargerListeArticlePalette(CritereArticle pCritereArticle) {
    // Construire la requête
    String requete = "select art.*, A1LB1, A1LB2, A1LB3 from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " art"
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_EXTENSION
        + " eart on art.A1ETB = eart.A1ETB and art.A1ART = eart.A1ART" + " where art.A1ETB = "
        + RequeteSql.formaterStringSQL(pCritereArticle.getIdEtablissement().getCodeEtablissement()) + " and art.A1TSP = '0'";
    
    // Articles valides
    if (pCritereArticle.isArticleValideSeulement()) {
      requete += " and ART.A1TOP = '0'";
    }
    
    // Ajouter le filtrage sur les mots recherchés (recherche sur le début des mots)
    for (ArgumentRecherche argumentRecherche : pCritereArticle.getRechercheGenerique().getListeArgumentRecherche()) {
      switch (argumentRecherche.getType()) {
        case IGNORE:
          break;
        
        case TEXTE:
        case EMAIL:
        case TELEPHONE:
        case ENTIER:
        case DECIMAL:
        case CODE_POSTAL:
          requete += " and exists(" + " select * from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_INDEX + " IDX" + " where"
              + " IDX.A1ETB = ART.A1ETB and IDX.A1ART = ART.A1ART" + " and IDX.A1TX1 like '" + argumentRecherche.getValeur() + "%')";
          break;
        
        case IDENTIFIANT:
          requete += " and exists(" + " select * from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_INDEX + " IDX" + " where"
              + " IDX.A1ETB = ART.A1ETB and IDX.A1ART = ART.A1ART" + " and IDX.A1ART like '" + argumentRecherche.getValeur() + "%')";
          break;
      }
    }
    
    // Lancer la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    
    // Remplir le résultat
    ListeArticle listeArticles = new ListeArticle();
    if (listeRecord != null) {
      for (GenericRecord record : listeRecord) {
        try {
          listeArticles.add(completerArticle(null, record, false));
        }
        catch (Exception e) {
          Trace.alerte("Erreur d'instanciation de l'Article " + record.getStringValue("A1ART"));
        }
      }
    }
    return listeArticles;
  }
  
  /**
   * Lire l'ensemble de la lsite des marques du catalogue articles d'un établissement
   */
  public ListeMarques chargerListeMarques(IdEtablissement pIdEtablissement) {
    ListeMarques listeMarques = null;
    
    try {
      String request = "SELECT DISTINCT A1REF1 FROM " + querymg.getLibrary() + ".PGVMARTM " + " WHERE A1ETB = '"
          + pIdEtablissement.getCodeEtablissement() + "' AND A1REF1 <> '' ORDER BY A1REF1 ";
      
      ArrayList<GenericRecord> listrcd = querymg.select(request);
      
      if ((listrcd == null) || (listrcd.size() < 0)) {
        return null;
      }
      
      listeMarques = new ListeMarques();
      
      for (GenericRecord record : listrcd) {
        if (record.isPresentField("A1REF1")) {
          listeMarques.add(record.getField("A1REF1").toString().trim());
        }
      }
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Problème lors la requête de récupération des marques. Merci de contacter le support.");
    }
    
    return listeMarques;
  }
  
  /**
   * Lire le montant DEEE d'un article de Série N.
   * Récupération le dernier tarif <= à la date du jour.
   */
  public BigDecimal chargerMontantDEEEArticle(IdArticle pIdArticle) {
    return chargerMontantDEEEArticle(pIdArticle, Constantes.DEUX_DECIMALES);
  }
  
  /**
   * Lire le montant DEEE d'un article de Série N.
   * Récupération le dernier tarif <= à la date du jour.
   */
  public BigDecimal chargerMontantDEEEArticle(IdArticle pIdArticle, int pNombreDecimale) {
    // Contrôler existance table PGVMTEEM qui n'est pas systématiquement présente
    if (!querymg.isTableExiste(EnumTableBDD.ARTICLE_TAXE)) {
      Trace.alerte("La table " + querymg.getLibrary() + "/" + EnumTableBDD.ARTICLE_TAXE + " n'est pas présente.");
      return null;
    }
    
    // Contrôle de l'identifiant
    IdArticle.controlerId(pIdArticle, true);
    
    // Construction de la requête
    String dateJour = DateHeure.getJourHeure(13);
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter("select ATP01, A1UNV, TENBR from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_TAXE
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_TARIF + " on ATETB = TEETB and ATART = TEARTT"
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " on A1ETB = TEETB and A1ART = TEARTT where");
    requeteSql.ajouterConditionAnd("TEETB", "=", pIdArticle.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("TEARTD", "=", pIdArticle.getCodeArticle());
    requeteSql.ajouterConditionAnd("ATDAP", "<=", dateJour);
    requeteSql.ajouter(" order by ATDAP desc fetch first 1 rows only optimize for 1 rows");
    // @formatter:on
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      Trace.alerte("Aucun enregistrement taxe trouvé pour l'article " + pIdArticle + '.');
      return null;
    }
    
    // Calcul de la DEEE
    GenericRecord record = listeRecord.get(0);
    if (record.isPresentField("ATP01") && record.isPresentField("TENBR") && record.isPresentField("A1UNV")) {
      // Gérer les 4 décimales
      if (record.getField("A1UNV").toString().equals("U*")) {
        return (((BigDecimal) record.getField("TENBR")).multiply((BigDecimal) record.getField("ATP01")).divide(Constantes.VALEUR_CENT,
            MathContext.DECIMAL32)).setScale(pNombreDecimale, RoundingMode.HALF_UP);
      }
      else {
        return (((BigDecimal) record.getField("TENBR")).multiply((BigDecimal) record.getField("ATP01"))).setScale(pNombreDecimale,
            RoundingMode.HALF_UP);
      }
    }
    
    return null;
  }
  
  /**
   * Lire la référence fabricant d'un article Série N dans sa condition d'achat active
   */
  public String chargerTexteReferenceFabricant(IdArticle pIdArticle) {
    IdArticle.controlerId(pIdArticle, true);
    
    ArrayList<GenericRecord> liste = querymg.select(" SELECT MAX(CACRE) AS DATEC,CAREF FROM " + querymg.getLibrary() + ".PGVMARTM "
        + " LEFT JOIN " + querymg.getLibrary() + ".PGVMCNAM ON A1ETB = CAETB AND A1ART = CAART AND A1FRS = CAFRS AND A1COF = CACOL "
        + " WHERE A1ETB = '" + pIdArticle.getCodeEtablissement() + "' AND A1ART = '" + pIdArticle.getCodeArticle() + "' GROUP BY CAREF "
        + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
    
    if (liste == null || liste.size() < 0) {
      Trace.erreur("[GVC] lireReferenceFabricant() SQL:" + querymg.getMsgError());
      return null;
    }
    else if (liste.size() == 0) {
      return null;
    }
    
    if (liste.size() == 1) {
      if (liste.get(0).isPresentField("CAREF")) {
        return liste.get(0).getField("CAREF").toString().trim();
      }
    }
    
    return null;
  }
  
  /**
   * Charger le bloc-notes de la condition d'achat
   */
  public BlocNote chargerBlocNoteConditionAchat(IdArticle pIdArticle, IdFournisseur pIdFournisseur) {
    SqlBlocNote sqlBlocNote = new SqlBlocNote(querymg);
    IdBlocNote idBlocNote = IdBlocNote.getInstancePourConditionAchat(pIdArticle, pIdFournisseur);
    return sqlBlocNote.chargerBlocNote(idBlocNote);
  }
  
  /**
   * Lire le prix fabricant d'un article Série N dans sa condition d'achat active
   */
  public BigDecimal chargerPrixFabricantArticle(IdArticle pIdArticle) {
    IdArticle.controlerId(pIdArticle, true);
    
    String requete = " SELECT CAPRA,CAUNA FROM " + querymg.getLibrary() + ".PGVMCNAM " + " WHERE CAETB = '"
        + pIdArticle.getCodeEtablissement() + "' AND CAART = '" + pIdArticle.getCodeArticle() + "' " + " AND CAIN3 = 'F' "
        + " AND CADAP <= (SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) " + " CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) "
        + " CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) " + " FROM SYSIBM.SYSDUMMY1) ORDER BY CADAP DESC "
        + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ";
    
    ArrayList<GenericRecord> listrcd = querymg.select(requete);
    if (listrcd == null || listrcd.size() < 0) {
      return null;
    }
    else if (listrcd.size() == 0) {
      return null;
    }
    
    if (listrcd.get(0).isPresentField("CAPRA") && listrcd.get(0).isPresentField("CAUNA")) {
      if (listrcd.get(0).getField("CAUNA").toString().contains("*")) {
        return ((BigDecimal) listrcd.get(0).getField("CAPRA")).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32).setScale(4,
            RoundingMode.HALF_UP);
      }
      else {
        return ((BigDecimal) listrcd.get(0).getField("CAPRA")).setScale(4, RoundingMode.HALF_UP);
      }
    }
    else {
      return null;
    }
  }
  
  /**
   * Ecrit le top mémo obligatoire en base
   */
  public void sauverTopMemoObligatoire(Article aArticle) {
    if (aArticle == null) {
      throw new MessageErreurException("L'article est à null.");
    }
    
    String obligation = "";
    if (aArticle.isAffichageMemoObligatoire()) {
      obligation = "1";
    }
    
    // Préparation de la requête pour l'entête
    String requete =
        "update " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " set" + "  A1IN33 = " + RequeteSql.formaterStringSQL(obligation)
            + " where A1ETB = " + RequeteSql.formaterStringSQL(aArticle.getId().getCodeEtablissement()) + " and A1ART = "
            + RequeteSql.formaterStringSQL(aArticle.getId().getCodeArticle());
    // Lancement de la requête pour l'entête
    querymg.requete(requete);
  }
  
  /**
   * Rechercher une liste d'identifiants articles suivant les critères passés en paramètre
   */
  public List<IdArticle> chargerListeIdArticle(CritereArticle pCritereArticle) {
    if (pCritereArticle == null || pCritereArticle.getIdEtablissement() == null) {
      throw new MessageErreurException("Les critères de recherche des articles sont invalides.");
    }
    
    // SELECT : Ajouter la somme des poids
    String sommePoids = "";
    for (ArgumentRecherche argumentRecherche : pCritereArticle.getRechercheGenerique().getListeArgumentRecherche()) {
      switch (argumentRecherche.getType()) {
        case IGNORE:
          break;
        
        case TEXTE:
        case EMAIL:
        case IDENTIFIANT:
        case TELEPHONE:
        case ENTIER:
        case DECIMAL:
        case CODE_POSTAL:
          if (sommePoids.isEmpty()) {
            sommePoids += "(IDX" + argumentRecherche.getIndex() + ".POIDS" + argumentRecherche.getIndex() + ")";
          }
          else {
            sommePoids += "+ (IDX" + argumentRecherche.getIndex() + ".POIDS" + argumentRecherche.getIndex() + ")";
          }
          break;
      }
    }
    
    // SELECT : Générer la partie SELECT de la requête
    String requete;
    if (sommePoids.isEmpty()) {
      requete = "select ART.A1ART";
    }
    else {
      requete = "select ART.A1ART, sum(" + sommePoids + ") as POIDS";
    }
    
    // FROM : Ajouter la table article
    requete += " from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " ART";
    
    // FROM : Ajouter le filtrage sur les mot recherchés
    for (ArgumentRecherche argumentRecherche : pCritereArticle.getRechercheGenerique().getListeArgumentRecherche()) {
      switch (argumentRecherche.getType()) {
        case IGNORE:
          break;
        
        case TEXTE:
        case EMAIL:
        case TELEPHONE:
        case ENTIER:
        case DECIMAL:
        case CODE_POSTAL:
          if (pCritereArticle.getFiltreCommentaire() != EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES_SAUF_COMMENTAIRES) {
            requete += " left";
          }
          requete += " join (select A1ART , sum(A1PRT) as POIDS" + argumentRecherche.getIndex() + " from " + querymg.getLibrary()
              + ".PGVMDTA2 where A1TX1 like '" + argumentRecherche.getValeur() + "%' group by A1ART) as IDX"
              + argumentRecherche.getIndex() + " on IDX" + argumentRecherche.getIndex() + ".A1ART = ART.A1ART";
          break;
        case IDENTIFIANT:
          if (pCritereArticle.getFiltreCommentaire() != EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES_SAUF_COMMENTAIRES) {
            requete += " left";
          }
          requete += " join (select A1ART , sum(A1PRT) as POIDS" + argumentRecherche.getIndex() + " from " + querymg.getLibrary()
              + ".PGVMDTA2 where A1ART like '" + argumentRecherche.getValeur() + "%' group by A1ART) as IDX"
              + argumentRecherche.getIndex() + " on IDX" + argumentRecherche.getIndex() + ".A1ART = ART.A1ART";
          break;
      }
    }
    
    // WHERE : ajouter la partie WHERE
    requete += " where  ART.A1ETB = " + RequeteSql.formaterStringSQL(pCritereArticle.getIdEtablissement().getCodeEtablissement());
    
    // Articles valides
    if (pCritereArticle.isArticleValideSeulement()) {
      requete += " and ART.A1TOP = '0'";
    }
    
    // Ne pas sélectionner les articles inactifs
    requete += " and ART.A1NPU <> '1'";
    
    // Contrôle et ajoute des conditions si on veut afficher les articles gérés en stock
    if (pCritereArticle.getFiltreStock() != null && pCritereArticle.getFiltreStock().equals(EnumFiltreArticleStock.OPTION_GERE_STOCK)) {
      // Sélectionner les articles gérés en stock
      requete += " and ART.A1STK = 0";
      
      // Sélectionner les articles qui ne sont pas non spéciaux
      requete += " and ART.A1SPE = 0";
    }
    else if (pCritereArticle.getFiltreStock() != null
        && pCritereArticle.getFiltreStock().equals(EnumFiltreArticleStock.OPTION_NON_GERE_STOCK)) {
      // Sélectionner les articles non gérés en stock
      requete += " and ART.A1STK = 1";
      
      // Ne pas sélectionner les articles transport
      requete += " and ART.A1TSP <> 'T'";
      
    }
    else {
      
      // Ne pas sélectionner les articles transport
      requete += " and ART.A1TSP <> 'T'";
      
    }
    
    // Ne pas sélectionner les articles commentaires
    if (pCritereArticle.getFiltreCommentaire().equals(EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES_SAUF_COMMENTAIRES)) {
      requete += " and ART.A1FAM <> ' '";
      // Contrôle que le code famille soit égal à 3 caractères (élimine les articles corrompus - cf FMDEV)
      requete += " and Length(Trim(ART.A1FAM)) = 3";
    }
    else if (pCritereArticle.getFiltreCommentaire().equals(EnumFiltreArticleCommentaire.OPTION_LES_ARTICLES_COMMENTAIRES_UNIQUEMENT)) {
      requete += " and ART.A1FAM = ' '";
      if (!pCritereArticle.getRechercheGenerique().getTexteFinal().isEmpty()) {
        requete += " and ART.A1ART LIKE '" + pCritereArticle.getRechercheGenerique().getTexteFinal() + "%'";
      }
    }
    
    // Filtrer sur les articles spéciaux
    if (pCritereArticle.getFiltreSpecial() != null
        && pCritereArticle.getFiltreSpecial().equals(EnumFiltreArticleSpecial.OPTION_LES_ARTICLES_SPECIAUX_UNIQUEMENT)) {
      requete += " and ART.A1SPE = 1";
    }
    else if (pCritereArticle.getFiltreSpecial() != null
        && pCritereArticle.getFiltreSpecial().equals(EnumFiltreArticleSpecial.OPTION_TOUS_LES_ARTICLES_SAUF_SPECIAUX)) {
      requete += " and ART.A1SPE = 0";
    }
    
    // Filtrer sur les articles consignés
    if (pCritereArticle.getFiltreConsigne() != null
        && pCritereArticle.getFiltreConsigne().equals(EnumFiltreArticleConsigne.OPTION_LES_ARTICLES_CONSIGNES_UNIQUEMENT)) {
      requete += " and ART.A1TSP = '0'";
    }
    else if (pCritereArticle.getFiltreConsigne() != null
        && pCritereArticle.getFiltreConsigne().equals(EnumFiltreArticleConsigne.OPTION_TOUS_LES_ARTICLES_SAUF_CONSIGNE)) {
      requete += " and ART.A1TSP <> '0'";
    }
    
    // Ajouter le filtrage sur le hors gamme
    if (pCritereArticle.getFiltreHorsGamme() != null
        && pCritereArticle.getFiltreHorsGamme().equals(EnumFiltreHorsGamme.OPTION_LES_HORS_GAMME_UNIQUEMENT)) {
      requete += " and ART.A1ABC = 'R'";
    }
    else if (pCritereArticle.getFiltreHorsGamme() != null
        && pCritereArticle.getFiltreHorsGamme().equals(EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES_SAUF_HORS_GAMME)) {
      requete += " and ART.A1ABC <> 'R'";
    }
    
    // Ajouter le filtrage sur le direct usine
    if (pCritereArticle.getDirectUsine().equals(EnumFiltreDirectUsine.OPTION_TOUS_LES_ARTICLES_SAUF_DIRECT_USINE)) {
      requete += " and ART.A1IN32 = ' '";
    }
    else if (pCritereArticle.getDirectUsine().equals(EnumFiltreDirectUsine.OPTION_LES_ARTICLES_DIRECT_USINE_UNIQUEMENT)) {
      requete += " and ART.A1IN32 <> ' '";
    }
    
    // Filtre sur la famille
    if (pCritereArticle.getIdFamille() != null) {
      requete += " and ART.A1FAM = " + RequeteSql.formaterStringSQL(pCritereArticle.getIdFamille().getCode());
    }
    
    // Ajouter le filtrage sur le fournisseur principal de la fiche article
    if (pCritereArticle.getIdFournisseur() != null) {
      requete += " and ART.A1COF = " + pCritereArticle.getIdFournisseur().getCollectif();
      requete += " and ART.A1FRS = " + pCritereArticle.getIdFournisseur().getNumero();
    }
    
    // GROUP BY
    requete += " group by ART.A1ART";
    
    // ORDER BY
    if (requete.toUpperCase().contains("POIDS")) {
      requete += " order by POIDS desc, ART.A1ART";
    }
    else {
      requete += " order by ART.A1ART";
    }
    
    // Exécuter la requête
    List<GenericRecord> listeRecord = querymg.select(requete);
    
    // Générer la liste résultat
    List<IdArticle> listeIdArticles = new ArrayList<IdArticle>();
    for (GenericRecord record : listeRecord) {
      try {
        IdArticle idArticle = IdArticle.getInstance(pCritereArticle.getIdEtablissement(), record.getStringValue("A1ART"));
        listeIdArticles.add(idArticle);
      }
      catch (Exception e) {
        Trace.alerte("Erreur d'instanciation de l'ArticleBase " + record.getStringValue("A1ART"));
      }
    }
    return listeIdArticles;
  }
  
  /**
   * Charger la liste des articles base à partir d'une liste d'identifiants article.
   * Seules les informations de base des articles sont récupérées.
   */
  public ListeArticleBase chargerListeArticleBaseParID(List<IdArticle> pListeIdArticle) {
    if (pListeIdArticle == null) {
      throw new MessageErreurException("La liste des identifiants articles est invalide");
    }
    
    RequeteSql requete = new RequeteSql();
    
    for (IdArticle idArticle : pListeIdArticle) {
      IdArticle.controlerId(idArticle, true);
      requete.ajouterValeurAuGroupe("indicatif", idArticle.getCodeArticle());
    }
    
    // Construire la requête
    requete.ajouter("select ART.A1ART, ART.A1LIB, EXT.A1LB1, EXT.A1LB2, EXT.A1LB3, ART.A1FAM from " + querymg.getLibrary() + '.'
        + EnumTableBDD.ARTICLE + " ART" + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_EXTENSION + " EXT"
        + " on EXT.A1ETB = ART.A1ETB and EXT.A1ART = ART.A1ART" + " where" + " ART.A1ETB = "
        + RequeteSql.formaterStringSQL(pListeIdArticle.get(0).getCodeEtablissement()));
    
    // On ajoute la liste des valeurs
    requete.ajouter(" and ART.A1ART");
    requete.ajouterGroupeDansRequete("indicatif", "in");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    
    // Générer la liste résultat
    ListeArticleBase listeArticles = new ListeArticleBase();
    for (GenericRecord record : listeRecord) {
      try {
        IdArticle idArticle = IdArticle.getInstance(pListeIdArticle.get(0).getIdEtablissement(), record.getStringValue("A1ART"));
        ArticleBase articleBase = new ArticleBase(idArticle);
        articleBase.setLibelle1(record.getStringValue("A1LIB"));
        articleBase.setLibelle2(record.getStringValue("A1LB1"));
        articleBase.setLibelle3(record.getStringValue("A1LB2"));
        articleBase.setLibelle4(record.getStringValue("A1LB3"));
        if (!record.getStringValue("A1FAM").trim().isEmpty()) {
          articleBase.setIdFamille(IdFamille.getInstance(pListeIdArticle.get(0).getIdEtablissement(), record.getStringValue("A1FAM")));
        }
        listeArticles.add(articleBase);
      }
      catch (Exception e) {
        Trace.alerte("Erreur d'instanciation de l'ArticleBase " + record.getStringValue("A1LIB"));
      }
    }
    return listeArticles;
  }
  
  /**
   * Charger la liste des articles base à partir d'une liste d'identifiants article.
   * Seules les informations de base des articles sont récupérées.
   */
  public ArticleBase chargerArticleBase(IdArticle pIdArticle) {
    IdArticle.controlerId(pIdArticle, true);
    
    RequeteSql requete = new RequeteSql();
    
    // Construire la requête
    requete.ajouter("select ART.A1ART, ART.A1NPU, ART.A1TSP, ART.A1SPE, ART.A1LIB, EXT.A1LB1, EXT.A1LB2, EXT.A1LB3, ART.A1FAM from "
        + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " ART" + " left join " + querymg.getLibrary() + '.'
        + EnumTableBDD.ARTICLE_EXTENSION + " EXT" + " on EXT.A1ETB = ART.A1ETB and EXT.A1ART = ART.A1ART" + " where" + " ART.A1ETB = "
        + RequeteSql.formaterStringSQL(pIdArticle.getCodeEtablissement()));
    
    // On ajoute le critère sur le code article
    requete.ajouterConditionAnd("ART.A1ART", "=", pIdArticle.getCodeArticle());
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    
    // Générer la liste résultat
    ArticleBase articleBase = null;
    for (GenericRecord record : listeRecord) {
      if (record != null) {
        articleBase = new ArticleBase(pIdArticle);
        String topSpecifique = record.getStringValue("ART.A1TSP");
        if (record.getStringValue("ART.A1NPU").equals("1")) {
          articleBase.setTypeArticle(EnumTypeArticle.PLUS_UTILISE);
        }
        else if (topSpecifique.equals("T")) {
          articleBase.setTypeArticle(EnumTypeArticle.TRANSPORT);
        }
        else if (topSpecifique.equals("S")) {
          articleBase.setTypeArticle(EnumTypeArticle.SERIE);
        }
        else if (Constantes.equals(record.getStringValue("A1FAM").trim(), "")) {
          articleBase.setTypeArticle(EnumTypeArticle.COMMENTAIRE);
        }
        else if (topSpecifique.equals("0")) {
          articleBase.setTypeArticle(EnumTypeArticle.PALETTE);
        }
        else if (record.getIntegerValue("ART.A1SPE") > 0) {
          articleBase.setTypeArticle(EnumTypeArticle.SPECIAL);
        }
        else {
          articleBase.setTypeArticle(EnumTypeArticle.STANDARD);
        }
        articleBase.setLibelle1(record.getStringValue("A1LIB"));
        articleBase.setLibelle2(record.getStringValue("A1LB1"));
        articleBase.setLibelle3(record.getStringValue("A1LB2"));
        articleBase.setLibelle4(record.getStringValue("A1LB3"));
        if (Constantes.equals(record.getStringValue("A1FAM").trim(), "")) {
          articleBase.setIdFamille(null);
        }
        else {
          articleBase.setIdFamille(IdFamille.getInstance(pIdArticle.getIdEtablissement(), record.getStringValue("A1FAM")));
        }
      }
    }
    return articleBase;
  }
  
  /**
   * Renvoyer le libellé complet d'un article à partir de son identifiant
   */
  public String chargerLibelleCompletArticle(IdArticle pIdArticle) {
    if (pIdArticle == null) {
      throw new MessageErreurException("L'identifiant article est invalide");
    }
    
    RequeteSql requete = new RequeteSql();
    
    // Construire la requête
    requete.ajouter("select ART.A1LIB, EXT.A1LB1, EXT.A1LB2, EXT.A1LB3 from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " ART"
        + " left join " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_EXTENSION + " EXT"
        + " on EXT.A1ETB = ART.A1ETB and EXT.A1ART = ART.A1ART" + " where" + " ART.A1ETB = "
        + RequeteSql.formaterStringSQL(pIdArticle.getCodeEtablissement()));
    
    // On ajoute la liste des valeurs
    requete.ajouterConditionAnd("ART.A1ART", "=", pIdArticle.getCodeArticle());
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    // Générer le résultat
    String libelleArticle = "";
    for (GenericRecord record : listeRecord) {
      libelleArticle = record.getStringValue("A1LIB") + record.getStringValue("A1LB1") + record.getStringValue("A1LB2")
          + record.getStringValue("A1LB3");
    }
    
    return libelleArticle;
  }
  
  /**
   * Rechercher la liste des articles correspondants aux critères de recherche.
   * Seules les informations de base des articles sont récupérées.
   */
  public ListeArticleBase chargerListeArticleBase(CritereArticle pCritereArticle) {
    if (pCritereArticle == null || pCritereArticle.getIdEtablissement() == null) {
      throw new MessageErreurException("Les critères de recherche des articles sont invalides.");
    }
    
    // SELECT : Ajouter la somme des poids
    String sommePoids = "";
    for (ArgumentRecherche argumentRecherche : pCritereArticle.getRechercheGenerique().getListeArgumentRecherche()) {
      switch (argumentRecherche.getType()) {
        case IGNORE:
          break;
        
        case TEXTE:
        case EMAIL:
        case IDENTIFIANT:
        case TELEPHONE:
        case ENTIER:
        case DECIMAL:
        case CODE_POSTAL:
          if (sommePoids.isEmpty()) {
            sommePoids += "(IDX" + argumentRecherche.getIndex() + ".POIDS" + argumentRecherche.getIndex() + ")";
          }
          else {
            sommePoids += "+ (IDX" + argumentRecherche.getIndex() + ".POIDS" + argumentRecherche.getIndex() + ")";
          }
          break;
      }
    }
    
    // SELECT : Générer la partie SELECT de la requête
    String requete;
    if (sommePoids.isEmpty()) {
      requete = "select ART.A1ART, ART.A1LIB, EXT.A1LB1, EXT.A1LB2, EXT.A1LB3, ART.A1FAM";
    }
    else {
      requete = "select ART.A1ART, ART.A1LIB, EXT.A1LB1, EXT.A1LB2, EXT.A1LB3, ART.A1FAM, sum(" + sommePoids + ") as POIDS";
    }
    
    // FROM : Ajouter la table article
    requete += " from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " ART";
    
    // FROM : Ajouter la table extension article
    requete += " left join " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE_EXTENSION
        + " EXT on EXT.A1ETB = ART.A1ETB and EXT.A1ART = ART.A1ART";
    
    // FROM : Ajouter le filtrage sur les mot recherchés
    for (ArgumentRecherche argumentRecherche : pCritereArticle.getRechercheGenerique().getListeArgumentRecherche()) {
      switch (argumentRecherche.getType()) {
        case IGNORE:
          break;
        
        case TEXTE:
        case EMAIL:
        case TELEPHONE:
        case ENTIER:
        case DECIMAL:
        case CODE_POSTAL:
          requete += " join (select A1ART , sum(A1PRT) as POIDS" + argumentRecherche.getIndex() + " from " + querymg.getLibrary()
              + ".PGVMDTA2 where A1TX1 like '" + argumentRecherche.getValeur() + "%' group by A1ART) as IDX"
              + argumentRecherche.getIndex() + " on IDX" + argumentRecherche.getIndex() + ".A1ART = ART.A1ART";
          break;
        
        case IDENTIFIANT:
          requete += " join (select A1ART , sum(A1PRT) as POIDS" + argumentRecherche.getIndex() + " from " + querymg.getLibrary()
              + ".PGVMDTA2 where A1ART like '" + argumentRecherche.getValeur() + "%' group by A1ART) as IDX"
              + argumentRecherche.getIndex() + " on IDX" + argumentRecherche.getIndex() + ".A1ART = ART.A1ART";
          break;
      }
    }
    // WHERE : ajouter la partie WHERE
    requete += " where  ART.A1ETB = " + RequeteSql.formaterStringSQL(pCritereArticle.getIdEtablissement().getCodeEtablissement());
    
    // Articles valides
    if (pCritereArticle.isArticleValideSeulement()) {
      requete += " and ART.A1TOP = '0'";
    }
    
    // Ne pas sélectionner les articles palettes
    requete += " and ART.A1TSP <> '0'";
    
    // Ne pas sélectionner les articles inactifs
    requete += " and ART.A1NPU <> '1'";
    
    // Contrôle que le code famille soit égal à 3 caractères (élimine les articles corrompus - cf FMDEV)
    requete += " and Length(Trim(ART.A1FAM)) = 3";
    
    if (pCritereArticle.getFiltreStock() != null && pCritereArticle.getFiltreStock().equals(EnumFiltreArticleStock.OPTION_GERE_STOCK)) {
      // Sélectionner les articles gérés en stock
      requete += " and ART.A1STK = 0";
    }
    else if (pCritereArticle.getFiltreStock() != null
        && pCritereArticle.getFiltreStock().equals(EnumFiltreArticleStock.OPTION_NON_GERE_STOCK)) {
      // Sélectionner les articles non gérés en stock
      requete += " and ART.A1STK = 1";
    }
    else {
      
      // Ne pas sélectionner les articles transport
      requete += " and ART.A1TSP <> 'T'";
      
      // Ne pas sélectionner les articles commentaires
      requete += " and ART.A1FAM <> ' '";
    }
    
    // Filtrer sur les articles spéciaux
    if (pCritereArticle.getFiltreSpecial() != null
        && pCritereArticle.getFiltreSpecial().equals(EnumFiltreArticleSpecial.OPTION_LES_ARTICLES_SPECIAUX_UNIQUEMENT)) {
      requete += " and ART.A1SPE = 1";
    }
    else if (pCritereArticle.getFiltreSpecial() != null
        && pCritereArticle.getFiltreSpecial().equals(EnumFiltreArticleSpecial.OPTION_TOUS_LES_ARTICLES_SAUF_SPECIAUX)) {
      requete += " and ART.A1SPE = 0";
    }
    
    // Ajouter le filtrage sur le hors gamme
    if (pCritereArticle.getFiltreHorsGamme() != null
        && pCritereArticle.getFiltreHorsGamme().equals(EnumFiltreHorsGamme.OPTION_LES_HORS_GAMME_UNIQUEMENT)) {
      requete += " and ART.A1ABC = 'R'";
    }
    else if (pCritereArticle.getFiltreHorsGamme() != null
        && pCritereArticle.getFiltreHorsGamme().equals(EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES_SAUF_HORS_GAMME)) {
      requete += " and ART.A1ABC <> 'R'";
    }
    
    // Ajouter le filtrage sur le direct usine
    if (pCritereArticle.getDirectUsine().equals(EnumFiltreDirectUsine.OPTION_TOUS_LES_ARTICLES_SAUF_DIRECT_USINE)) {
      requete += " and ART.A1IN32 = ' '";
    }
    else if (pCritereArticle.getDirectUsine().equals(EnumFiltreDirectUsine.OPTION_LES_ARTICLES_DIRECT_USINE_UNIQUEMENT)) {
      requete += " and ART.A1IN32 <> ' '";
    }
    
    // Filtre sur la famille
    if (pCritereArticle.getIdFamille() != null) {
      requete += " and ART.A1FAM = " + RequeteSql.formaterStringSQL(pCritereArticle.getIdFamille().getCode());
    }
    
    // Ajouter le filtrage sur le fournisseur principal de la fiche article
    if (pCritereArticle.getIdFournisseur() != null) {
      requete += " and ART.A1COF = " + pCritereArticle.getIdFournisseur().getCollectif();
      requete += " and ART.A1FRS = " + pCritereArticle.getIdFournisseur().getNumero();
    }
    
    // GROUP BY
    requete += " group by ART.A1ART, ART.A1LIB, EXT.A1LB1, EXT.A1LB2, EXT.A1LB3, ART.A1FAM";
    
    // ORDER BY
    // Quels champs pour le tri ?
    boolean triSurCodeArticle = false;
    boolean triSurPoidsEtCodeArticle = false;
    for (ArgumentRecherche argumentRecherche : pCritereArticle.getRechercheGenerique().getListeArgumentRecherche()) {
      switch (argumentRecherche.getType()) {
        case IGNORE:
          triSurCodeArticle = true;
          break;
        
        case TEXTE:
        case EMAIL:
        case IDENTIFIANT:
        case TELEPHONE:
        case ENTIER:
        case DECIMAL:
        case CODE_POSTAL:
          triSurPoidsEtCodeArticle = true;
          break;
      }
    }
    // Ajout du order by dans la requête
    if (triSurPoidsEtCodeArticle) {
      requete += " order by POIDS desc, ART.A1ART";
    }
    else if (triSurCodeArticle) {
      requete += " order by ART.A1ART";
    }
    
    // Exécuter la requête
    List<GenericRecord> listeRecord = querymg.select(requete);
    
    // Générer la liste résultat
    ListeArticleBase listeArticles = new ListeArticleBase();
    for (GenericRecord record : listeRecord) {
      try {
        IdArticle idArticle = IdArticle.getInstance(pCritereArticle.getIdEtablissement(), record.getStringValue("A1ART"));
        ArticleBase articleBase = new ArticleBase(idArticle);
        articleBase.setLibelle1(record.getStringValue("A1LIB"));
        articleBase.setLibelle2(record.getStringValue("A1LB1"));
        articleBase.setLibelle3(record.getStringValue("A1LB2"));
        articleBase.setLibelle4(record.getStringValue("A1LB3"));
        articleBase.setIdFamille(IdFamille.getInstance(pCritereArticle.getIdEtablissement(), record.getStringValue("A1FAM")));
        listeArticles.add(articleBase);
      }
      catch (Exception e) {
        Trace.alerte("Erreur d'instanciation de l'ArticleBase " + record.getStringValue("A1LIB"));
      }
    }
    return listeArticles;
  }
  
  /**
   * Rechercher la liste des id articles en rupture de stock correspondants aux critères de recherche.
   */
  public ListeArticleBase chargerListeIdArticlesEnRupture(CritereArticle pCritereArticle) {
    if (pCritereArticle == null || pCritereArticle.getIdEtablissement() == null || pCritereArticle.getIdFournisseur() == null
        || pCritereArticle.getIdMagasin() == null) {
      throw new MessageErreurException("Les critères de recherche des articles sont invalides.");
    }
    
    // Construire la requête
    RequeteSql requete = new RequeteSql();
    requete.ajouter(
        "select A1ART, A1LIB from " + querymg.getLibrary() + '.' + EnumTableBDD.ARTICLE + " ART" + " left join " + querymg.getLibrary()
            + '.' + EnumTableBDD.STOCK + " STK on S1ETB = A1ETB and A1ART = S1ART where A1TOP = 0 and A1NPU = ' ' and A1FAM <> ' '");
    requete.ajouterConditionAnd("A1ETB", "=", pCritereArticle.getIdEtablissement().getCodeEtablissement());
    requete.ajouterConditionAnd("A1COF", "=", pCritereArticle.getIdFournisseur().getCollectif());
    requete.ajouterConditionAnd("A1FRS", "=", pCritereArticle.getIdFournisseur().getNumero());
    requete.ajouterConditionAnd("S1MAG", "=", pCritereArticle.getIdMagasin().getCode());
    // Si S1QTE1 = 0 alors S1QTE1 = S1MIN
    // L'article est en rupture si S1QTE1 - WSTK > 0
    // où WSTK = s1std + s1qee + s1qse + s1qde + s1qem + s1qsm + s1qdm + s1qes + s1qss + s1qds + s1att - s1res
    requete.ajouter(" and (case when s1qte1 = 0 then s1min - (s1std + s1qee + s1qse + s1qde + s1qem + s1qsm + s1qdm + s1qes + s1qss "
        + " + s1qds + s1att - s1res) else s1qte1 - (s1std + s1qee + s1qse + s1qde + s1qem + s1qsm + s1qdm + s1qes + s1qss + s1qds"
        + " + s1att - s1res) end > 0) ");
    
    // Articles valides
    if (pCritereArticle.isArticleValideSeulement()) {
      requete.ajouterConditionAnd("A1TOP", "=", 0);
    }
    
    // Filtrer sur les articles spéciaux
    if (pCritereArticle.getFiltreSpecial() != null
        && pCritereArticle.getFiltreSpecial().equals(EnumFiltreArticleSpecial.OPTION_LES_ARTICLES_SPECIAUX_UNIQUEMENT)) {
      requete.ajouterConditionAnd("A1SPE", "=", 1);
    }
    else if (pCritereArticle.getFiltreSpecial() != null
        && pCritereArticle.getFiltreSpecial().equals(EnumFiltreArticleSpecial.OPTION_TOUS_LES_ARTICLES_SAUF_SPECIAUX)) {
      requete.ajouterConditionAnd("A1SPE", "=", 0);
    }
    
    // Ajouter le filtrage sur le hors gamme
    if (pCritereArticle.getFiltreHorsGamme() != null
        && pCritereArticle.getFiltreHorsGamme().equals(EnumFiltreHorsGamme.OPTION_LES_HORS_GAMME_UNIQUEMENT)) {
      requete.ajouterConditionAnd("A1ABC", "=", 'R');
    }
    else if (pCritereArticle.getFiltreHorsGamme() != null
        && pCritereArticle.getFiltreHorsGamme().equals(EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES_SAUF_HORS_GAMME)) {
      requete.ajouterConditionAnd("A1ABC", "<>", 'R');
    }
    
    // Ajouter le order by
    requete.ajouter(" order by A1ART");
    
    // Exécuter la requête
    List<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    
    // Générer la liste résultat
    ListeArticleBase listeArticles = new ListeArticleBase();
    for (GenericRecord record : listeRecord) {
      IdArticle idArticle = IdArticle.getInstance(pCritereArticle.getIdEtablissement(), record.getStringValue("A1ART"));
      ArticleBase articleBase = new ArticleBase(idArticle);
      articleBase.setLibelle1(record.getStringValue("A1LIB"));
      listeArticles.add(articleBase);
    }
    return listeArticles;
  }
  
  /**
   * Charge les données de stock pour un article
   */
  public void chargerInfosStockArticles(Article pArticle) {
    if (pArticle == null) {
      throw new MessageErreurException("Impossible de charger les informations de l'article car il est invalide.");
    }
    // On controle que l'on a pas affiare à un article commentaire
    if (pArticle.isArticleCommentaire() || pArticle.getIdFournisseur() == null || pArticle.isArticleTransport()) {
      return;
    }
    
    BigDecimal coeff = BigDecimal.ZERO;
    
    // Construire la requête
    RequeteSql requeteCna = new RequeteSql();
    requeteCna.ajouter("select CAKSC from " + querymg.getLibrary() + '.' + EnumTableBDD.CONDITION_ACHAT + " where ");
    requeteCna.ajouterConditionAnd("CAETB", "=", pArticle.getId().getIdEtablissement().getCodeEtablissement());
    requeteCna.ajouterConditionAnd("CAART", "=", pArticle.getId().getCodeArticle());
    requeteCna.ajouterConditionAnd("CAFRS", "=", pArticle.getIdFournisseur().getNumero());
    requeteCna.ajouterConditionAnd("CACOL", "=", pArticle.getIdFournisseur().getCollectif());
    
    // Exécuter la requête
    List<GenericRecord> listeRecordCna = querymg.select(requeteCna.getRequete());
    
    // On complète la CNA
    for (GenericRecord recordCna : listeRecordCna) {
      coeff = recordCna.getBigDecimalValue("CAKSC");
      if (coeff == null || coeff.compareTo(BigDecimal.ZERO) == 0) {
        coeff = BigDecimal.ONE;
      }
    }
    
    // Construire la requête
    RequeteSql requete = new RequeteSql();
    requete.ajouter(
        "select S1QTE1,S1QTE2,S1MIN,S1MAX,S1ATT,S1RES,S1AFF  from " + querymg.getLibrary() + '.' + EnumTableBDD.STOCK + " STK where ");
    requete.ajouterConditionAnd("S1ETB", "=", pArticle.getId().getIdEtablissement().getCodeEtablissement());
    requete.ajouterConditionAnd("S1ART", "=", pArticle.getId().getCodeArticle());
    
    // Exécuter la requête
    List<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    
    // On complète l'article
    for (GenericRecord record : listeRecord) {
      pArticle.setConsommationMoyenneUCA(record.getBigDecimalValue("S1QTE2").multiply(coeff));
      pArticle.setQuantiteStockMinimumUCA(record.getBigDecimalValue("S1MIN"));
      pArticle.setQuantiteStockMaximumUCA(record.getBigDecimalValue("S1MAX"));
      pArticle.setQuantiteStockIdealUCA(record.getBigDecimalValue("S1QTE1").multiply(coeff));
      pArticle.setQuantiteAttendueUCA(record.getBigDecimalValue("S1ATT"));
      pArticle.setQuantiteReserveeUCA(record.getBigDecimalValue("S1RES"));
      pArticle.setQuantiteAffecteeUCA(record.getBigDecimalValue("S1AFF"));
    }
    
  }
}
