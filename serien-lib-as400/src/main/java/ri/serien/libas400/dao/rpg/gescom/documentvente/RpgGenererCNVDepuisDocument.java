/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgGenererCNVDepuisDocument extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0062";
  private static final int NEGOCATION_ACHAT = 1;
  
  /**
   * Appel du programme RPG qui va écrire une condition de vente à partir d'une ligne de document.
   */
  public void genererCNVDepuisDocument(SystemeManager pSysteme, IdLigneVente pIdLigneVente, Date pDateDebut, Date pDateFin,
      String pReference) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pIdLigneVente == null) {
      throw new MessageErreurException("Le paramètre pIdLigneVente du service est à null.");
    }
    if (pDateDebut == null) {
      throw new MessageErreurException("Le paramètre pDateDebut du service est à null.");
    }
    if (pReference == null) {
      throw new MessageErreurException("Le paramètre pReference du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0062i entree = new Svgvm0062i();
    Svgvm0062o sortie = new Svgvm0062o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPicod(pIdLigneVente.getCodeEntete().getCode());
    entree.setPietb(pIdLigneVente.getCodeEtablissement());
    entree.setPinum(pIdLigneVente.getNumero());
    entree.setPisuf(pIdLigneVente.getSuffixe());
    entree.setPinli(pIdLigneVente.getNumeroLigne());
    entree.setPifor('f');
    entree.setPideb(pDateDebut);
    entree.setPifin(pDateFin);
    entree.setPiref(pReference);
    entree.setPicha('X');
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
}
