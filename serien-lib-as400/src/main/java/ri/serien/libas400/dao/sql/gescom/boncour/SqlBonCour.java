/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.boncour;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.EnumEtatBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeCarnetBonCour;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Regrouper les accès SQL en relation avec les bons de cour.
 */
public class SqlBonCour {
  
  // Variables
  private QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlBonCour(QueryManager pQuerymg) {
    querymg = pQuerymg;
  }
  
  // -- Méthodes publiques
  
  /**
   * Créer un carnet de bons de cour.
   */
  public CarnetBonCour creerCarnetBonCour(CarnetBonCour pCarnetBonCour) {
    if (pCarnetBonCour == null) {
      throw new MessageErreurException("Le carnet de bons de cour est invalide.");
    }
    IdCarnetBonCour.controlerId(pCarnetBonCour.getId(), false);
    
    RequeteSql requeteSql = new RequeteSql();
    // Contrôles
    // Vérification que le numéro du carnet soit non attribué
    requeteSql.ajouter("select c4cdc from " + querymg.getLibrary() + '.' + EnumTableBDD.CARNET_BON_COUR + " where ");
    requeteSql.ajouterConditionAnd("c4etb", "=", pCarnetBonCour.getId().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("c4mag", "=", pCarnetBonCour.getId().getIdMagasin().getCode());
    requeteSql.ajouterConditionAnd("c4cdc", "=", pCarnetBonCour.getId().getNumero());
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord != null && !listeGenericRecord.isEmpty()) {
      throw new MessageErreurException("Impossible de créer ce carnet car le numéro a déjà été attribué.");
    }
    // Contrôle de la non existence des bons de cour
    requeteSql.effacerRequete();
    requeteSql
        .ajouter("select bdetb, bdmag, bdcdc, bdbdc from " + querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR + " where");
    requeteSql.ajouterConditionAnd("bdetb", "=", pCarnetBonCour.getId().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("bdmag", "=", pCarnetBonCour.getId().getIdMagasin().getCode());
    requeteSql.ajouterConditionAnd("bdbdc", ">=", pCarnetBonCour.getPremierNumero());
    requeteSql.ajouterConditionAnd("bdbdc", "<=", pCarnetBonCour.getDernierNumero());
    // Exécuter la requête
    listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord != null && !listeGenericRecord.isEmpty()) {
      // Des enregistrements de bons de cour ont été détectés ce qui n'est pas normal
      StringBuilder sb = new StringBuilder(listeGenericRecord.size() * 50);
      for (GenericRecord record : listeGenericRecord) {
        sb.append(initialiserIdBonCour(record)).append(", ");
      }
      sb.delete(sb.length() - 2, sb.length());
      Trace.erreur("Impossible de créer les bons de cour du carnet à créer " + pCarnetBonCour + " car " + listeGenericRecord.size()
          + " bons de cour existent déjà. La liste est visible dans les logs du serveur."
          + " Liste des bons de cour détéctés lors du contrôle avant leur création : " + sb.toString());
      throw new MessageErreurException("Vous essayez de créer des bons de cour dont le numéro est déjà utilisé dans un autre carnet."
          + " Choisissez un nombre de pages ou un numéro de premier bon différent.");
    }
    
    // Initialisation des variables avant sauvegarde
    String codeMagasinier = "";
    if (pCarnetBonCour.getIdMagasinier() != null && pCarnetBonCour.getIdMagasinier().getCode() != null) {
      codeMagasinier = pCarnetBonCour.getIdMagasinier().getCode();
    }
    int dateRemise = 0;
    if (pCarnetBonCour.getDateRemise() != null) {
      dateRemise = ConvertDate.dateToDb2(pCarnetBonCour.getDateRemise());
    }
    
    // Insertion du carnet
    // @formatter:off
    requeteSql.effacerRequete();
    requeteSql.ajouter("insert into " + querymg.getLibrary() + '.' + EnumTableBDD.CARNET_BON_COUR
        + " (c4etb, c4mag, c4cdc, c4vde, c4cre, c4npa, c4bdc1 ) values ("
        + RequeteSql.formaterStringSQL(pCarnetBonCour.getId().getCodeEtablissement())
        + ", " + RequeteSql.formaterStringSQL(pCarnetBonCour.getId().getIdMagasin().getCode())
        + ", " + pCarnetBonCour.getId().getNumero()
        + ", " + RequeteSql.formaterStringSQL(codeMagasinier)
        + ", " + dateRemise
        + ", " + pCarnetBonCour.getNombrePages()
        + ", " + pCarnetBonCour.getPremierNumero()
        + ")");
    // @formatter:on
    querymg.requete(requeteSql.getRequete());
    
    // Préparation de la liste des identifiants des bons de cour à créer
    List<IdBonCour> listeIdBonCour = new ArrayList<IdBonCour>();
    int numeroFinBonCour = pCarnetBonCour.getDernierNumero();
    for (int numeroBonCour = pCarnetBonCour.getPremierNumero(); numeroBonCour <= numeroFinBonCour; numeroBonCour++) {
      IdBonCour idBonCour = IdBonCour.getInstanceAvecCreationId(pCarnetBonCour.getId(), numeroBonCour);
      listeIdBonCour.add(idBonCour);
    }
    
    // Création de tous les bons de cour composants le carnet
    creerListeBonCour(listeIdBonCour);
    
    return pCarnetBonCour;
  }
  
  /**
   * Modifier un carnet de bons de cour.
   */
  public CarnetBonCour modifierCarnetBonCour(CarnetBonCour pCarnetBonCour) {
    if (pCarnetBonCour == null) {
      throw new MessageErreurException("Le carnet de bons de cour est invalide.");
    }
    IdCarnetBonCour.controlerId(pCarnetBonCour.getId(), true);
    
    RequeteSql requeteSql = new RequeteSql();
    // Contrôles
    // Contrôle du risque de collision (dans le cas d'un aggrandissement du nombre de page ou changement du numéro du premier bon)
    requeteSql
        .ajouter("select bdetb, bdmag, bdcdc, bdbdc from " + querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR + " where");
    requeteSql.ajouterConditionAnd("bdetb", "=", pCarnetBonCour.getId().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("bdmag", "=", pCarnetBonCour.getId().getIdMagasin().getCode());
    requeteSql.ajouterConditionAnd("bdcdc", "<>", pCarnetBonCour.getId().getNumero());
    requeteSql.ajouterConditionAnd("bdbdc", ">=", pCarnetBonCour.getPremierNumero());
    requeteSql.ajouterConditionAnd("bdbdc", "<=", pCarnetBonCour.getDernierNumero());
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord != null && !listeGenericRecord.isEmpty()) {
      // Des enregistrements de bons de cour ont été détectés donc la modification ne peut pas être prise en compte
      StringBuilder sb = new StringBuilder(listeGenericRecord.size() * 50);
      for (GenericRecord record : listeGenericRecord) {
        sb.append(initialiserIdBonCour(record)).append(", ");
      }
      sb.delete(sb.length() - 2, sb.length());
      Trace.erreur("Impossible de créer les bons de cour du carnet modifié " + pCarnetBonCour + " car " + listeGenericRecord.size()
          + " bons de cour d'un autre carnet existent déjà."
          + " Liste des numéros de bons de cour détéctés lors du contrôle de collision suite à la modification du carnet : "
          + sb.toString());
      throw new MessageErreurException("Vous essayez de créer des bons de cour dont le numéro est déjà utilisé dans un autre carnet."
          + " Choisissez un nombre de pages ou un numéro de premier bon différent.");
    }
    // Contrôle du risque de suppression de bon utilisés (dans le cas d'une diminution du nombre de page ou changement du numéro du
    // premier bon)
    requeteSql.effacerRequete();
    requeteSql.ajouter("select bdetb, bdmag, bdcdc, bdbdc from " + querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR + " where ");
    requeteSql.ajouterConditionAnd("bdetb", "=", pCarnetBonCour.getId().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("bdmag", "=", pCarnetBonCour.getId().getIdMagasin().getCode());
    requeteSql.ajouterConditionAnd("bdcdc", "=", pCarnetBonCour.getId().getNumero());
    requeteSql.ajouterConditionAnd("bdeta", "<>", EnumEtatBonCour.NON_UTILISE.getCode());
    requeteSql.ajouter(" and (bdbdc < " + pCarnetBonCour.getPremierNumero() + " or bdbdc > " + pCarnetBonCour.getDernierNumero() + ")");
    listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord != null && !listeGenericRecord.isEmpty()) {
      // Des enregistrements de bons de cour utilisés ont été détectés donc la modification ne peut pas être prise ne compte
      StringBuilder sb = new StringBuilder(listeGenericRecord.size() * 50);
      for (GenericRecord record : listeGenericRecord) {
        sb.append(initialiserIdBonCour(record)).append(", ");
      }
      sb.delete(sb.length() - 2, sb.length());
      Trace.erreur("Impossible de supprimer les bons de cour du carnet modifié " + pCarnetBonCour + " car " + listeGenericRecord.size()
          + " bons de cour de ce carnet sont utilisés."
          + " Liste des numéros de bons de cour détéctés lors du contrôle de suppression suite à la modification du carnet : "
          + sb.toString());
      throw new MessageErreurException("Vous essayez de supprimer des bons de cour dont le numéro est utilisé."
          + " Choisissez un nombre de pages ou un numéro de premier bon différent.");
    }
    
    // Initialisation des variables avant sauvegarde
    String codeMagasinier = "";
    if (pCarnetBonCour.getIdMagasinier() != null && pCarnetBonCour.getIdMagasinier().getCode() != null) {
      codeMagasinier = pCarnetBonCour.getIdMagasinier().getCode();
    }
    int dateRemise = 0;
    if (pCarnetBonCour.getDateRemise() != null) {
      dateRemise = ConvertDate.dateToDb2(pCarnetBonCour.getDateRemise());
    }
    
    // Mise à jour du carnet avant de traiter les bons
    // @formatter:off
    requeteSql.effacerRequete();
    requeteSql.ajouter("update " + querymg.getLibrary() + '.' + EnumTableBDD.CARNET_BON_COUR
        + " set c4vde = " + RequeteSql.formaterStringSQL(codeMagasinier)
        + ", c4cre = " + dateRemise
        + ", c4npa = " + pCarnetBonCour.getNombrePages()
        + ", c4bdc1 = " + pCarnetBonCour.getPremierNumero()
        + " where ");
    // @formatter:on
    requeteSql.ajouterConditionAnd("c4etb", "=", pCarnetBonCour.getId().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("c4mag", "=", pCarnetBonCour.getId().getIdMagasin().getCode());
    requeteSql.ajouterConditionAnd("c4cdc", "=", pCarnetBonCour.getId().getNumero());
    querymg.requete(requeteSql.getRequete());
    
    // Chargement des bons du carnet
    CritereBonCour criteres = new CritereBonCour();
    criteres.setIdCarnetBonCour(pCarnetBonCour.getId());
    criteres.setIgnorerCarnetNonAttribue(false);
    List<IdBonCour> listeIdBonCour = chargerListeIdBonCour(criteres);
    // Etonnant mais aucun bon trouvé donc création de tous les bons du carnet et écriture d'un message dans les logs
    if (listeIdBonCour == null) {
      Trace.alerte("Lors de la modification du carnet " + pCarnetBonCour
          + " aucun bon de cours n'a été trouvé. Ceci est anormal, les bons de cours vont être créés.");
      // Préparation de la liste des identifiants des bons de cour à créer
      listeIdBonCour = new ArrayList<IdBonCour>();
      int numeroFinBonCour = pCarnetBonCour.getDernierNumero();
      for (int numeroBonCour = pCarnetBonCour.getPremierNumero(); numeroBonCour <= numeroFinBonCour; numeroBonCour++) {
        // Création de l'identifiant du bon de cour
        IdBonCour idBonCour = IdBonCour.getInstance(pCarnetBonCour.getId(), numeroBonCour);
        listeIdBonCour.add(idBonCour);
      }
      // Création des bons de cour
      creerListeBonCour(listeIdBonCour);
      return pCarnetBonCour;
    }
    // Analyse de la liste des bons afin de déterminer les bons à supprimer
    List<IdBonCour> listeIdBonCourASupprimer = new ArrayList<IdBonCour>();
    int premierNumeroEnBase = (int) Constantes.valeurMaxZoneNumerique(IdBonCour.LONGUEUR_NUMERO);
    int dernierNumeroEnBase = 0;
    for (Iterator<IdBonCour> iterator = listeIdBonCour.iterator(); iterator.hasNext();) {
      IdBonCour id = iterator.next();
      // Récupération du premier numéro de bon
      if (premierNumeroEnBase > id.getNumero().intValue()) {
        premierNumeroEnBase = id.getNumero().intValue();
      }
      // Récupération du dernier numéro de bon
      if (dernierNumeroEnBase < id.getNumero().intValue()) {
        dernierNumeroEnBase = id.getNumero().intValue();
      }
      // Le numéro de bon en base est inférieur au premier numéro du bon du carnet ou s'il est supérieur au dernier numéro de bon du
      // carnet alors c'est une suppression
      if (id.getNumero().compareTo(pCarnetBonCour.getPremierNumero()) < 0
          || id.getNumero().compareTo(pCarnetBonCour.getDernierNumero()) > 0) {
        listeIdBonCourASupprimer.add(id);
      }
      iterator.remove();
    }
    listeIdBonCour.clear();
    // Analyse pour les bons à créer
    if (pCarnetBonCour.getPremierNumero() < premierNumeroEnBase) {
      for (int numeroBonCour = pCarnetBonCour.getPremierNumero(); numeroBonCour < premierNumeroEnBase; numeroBonCour++) {
        IdBonCour idBonCour = IdBonCour.getInstance(pCarnetBonCour.getId(), numeroBonCour);
        listeIdBonCour.add(idBonCour);
      }
    }
    if (pCarnetBonCour.getDernierNumero() > dernierNumeroEnBase) {
      for (int numeroBonCour = dernierNumeroEnBase + 1; numeroBonCour <= pCarnetBonCour.getDernierNumero(); numeroBonCour++) {
        IdBonCour idBonCour = IdBonCour.getInstance(pCarnetBonCour.getId(), numeroBonCour);
        listeIdBonCour.add(idBonCour);
      }
    }
    // Mise à jour de la base
    supprimerListeBonCour(listeIdBonCourASupprimer);
    creerListeBonCour(listeIdBonCour);
    
    return pCarnetBonCour;
  }
  
  /**
   * Charger la liste des carnets des bons de cour correspondants aux critères.
   */
  public List<IdCarnetBonCour> chargerListeIdCarnetBonCour(CritereCarnetBonCour pCriteres) {
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères de recherche des carnets de bons de cour sont invalides.");
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql
        .ajouter("select c4etb, c4mag, c4cdc from " + querymg.getLibrary() + '.' + EnumTableBDD.CARNET_BON_COUR + " where ");
    requeteSql.ajouterConditionAnd("c4etb", "=", pCriteres.getIdEtablissement().getCodeEtablissement());
    
    // Filtre sur le code magasin
    if (pCriteres.getIdMagasin() != null) {
      requeteSql.ajouterConditionAnd("c4mag", "=", pCriteres.getIdMagasin().getCode());
    }
    
    // Filtre sur le numéro de carnet
    if (pCriteres.getNumeroCarnet() != null && pCriteres.getNumeroCarnet() > 0) {
      requeteSql.ajouterConditionAnd("c4cdc", "=", pCriteres.getNumeroCarnet().intValue());
    }
    
    // Filtre sur le code magasinier
    if (pCriteres.getIdMagasinier() != null) {
      requeteSql.ajouterConditionAnd("c4vde", "=", pCriteres.getIdMagasinier().getCode());
    }
    
    // Filtre sur le numéro de bon
    if (pCriteres.getNumeroBonDeCour() != null && pCriteres.getNumeroBonDeCour() > 0) {
      requeteSql.ajouterConditionAnd("c4bdc1", "<=", pCriteres.getNumeroBonDeCour());
      requeteSql.ajouterConditionAnd("(c4bdc1 + c4npa - 1)", ">=", pCriteres.getNumeroBonDeCour());
    }
    
    // Ordre de tri
    requeteSql.ajouter(" order by c4cdc");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    
    // Création de la liste des identifiants trouvés
    List<IdCarnetBonCour> listeCarnetBonCour = new ArrayList<IdCarnetBonCour>();
    for (GenericRecord record : listeGenericRecord) {
      listeCarnetBonCour.add(initialiserIdCarnetBonCour(record));
    }
    return listeCarnetBonCour;
  }
  
  /**
   * Charger la liste des identifiants de carnets de bons de cour correspondants aux critères de recherche.
   */
  public List<IdCarnetBonCour> chargerListeIdCarnetBonCour(CritereBonCour pCritereBonCour) {
    if (pCritereBonCour == null) {
      throw new MessageErreurException("Les critères de recherche de bons de cour sont invalides.");
    }
    
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT C4ETB, C4MAG, C4CDC FROM ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.CARNET_BON_COUR);
    // jointure si on a un numéro de bon de cour
    if (pCritereBonCour.getNumeroBonCour() > 0) {
      requeteSql.ajouter(" INNER JOIN ");
      requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR);
      requeteSql.ajouter(" ON BDETB = C4ETB AND BDMAG = C4MAG AND BDCDC = C4CDC");
    }
    requeteSql.ajouter(" WHERE");
    requeteSql.ajouterConditionAnd("C4ETB", "=", pCritereBonCour.getIdEtablissement().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("C4MAG", "=", pCritereBonCour.getIdMagasin().getCode());
    
    // Si on a un numéro de carnet
    if (pCritereBonCour.getIdCarnetBonCour() != null && pCritereBonCour.getIdCarnetBonCour().getNumero() > 0) {
      requeteSql.ajouterConditionAnd("C4CDC", "=", pCritereBonCour.getIdCarnetBonCour().getNumero());
    }
    // Si on a un numéro de bon de cour
    if (pCritereBonCour.getNumeroBonCour() > 0) {
      requeteSql.ajouterConditionAnd("BDBDC", "=", pCritereBonCour.getNumeroBonCour());
    }
    // Si on a un identifiant de magasinier
    if (pCritereBonCour.getIdMagasinier() != null) {
      requeteSql.ajouterConditionAnd("C4VDE", "=", pCritereBonCour.getIdMagasinier().getCode());
    }
    requeteSql.ajouter(" ORDER BY C4CDC");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Création de la liste des id avec les données trouvées
    List<IdCarnetBonCour> listeIdCarnetBonCour = new ArrayList<IdCarnetBonCour>();
    for (GenericRecord record : listeGenericRecord) {
      listeIdCarnetBonCour.add(initialiserIdCarnetBonCour(record));
    }
    return listeIdCarnetBonCour;
  }
  
  /**
   * Charger la liste des identifiants des bons de cour correspondants aux critères de recherche.
   */
  public List<IdBonCour> chargerListeIdBonCour(CritereBonCour pCritereBonCour) {
    if (pCritereBonCour == null) {
      throw new MessageErreurException("Les critères de recherche de bons de cour sont invalides.");
    }
    
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT BDETB, BDMAG, BDCDC, BDBDC  FROM ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR);
    // jointure si on a un identifiant de magasinier
    requeteSql.ajouter(" INNER JOIN ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.CARNET_BON_COUR);
    requeteSql.ajouter(" ON BDETB = C4ETB AND BDMAG = C4MAG AND BDCDC = C4CDC WHERE ");
    
    // Si établissement est renseigné
    if (pCritereBonCour.getIdEtablissement() != null) {
      requeteSql.ajouterConditionAnd("BDETB", "=", pCritereBonCour.getIdEtablissement().getCodeEtablissement());
    }
    
    // Si un magasin est renseigné
    if (pCritereBonCour.getIdMagasin() != null) {
      requeteSql.ajouterConditionAnd("BDMAG", "=", pCritereBonCour.getIdMagasin().getCode());
    }
    
    // Si on a un numéro de carnet
    if (pCritereBonCour.getIdCarnetBonCour() != null) {
      requeteSql.ajouterConditionAnd("BDETB", "=", pCritereBonCour.getIdCarnetBonCour().getCodeEtablissement());
      requeteSql.ajouterConditionAnd("BDMAG", "=", pCritereBonCour.getIdCarnetBonCour().getIdMagasin().getCode());
      if (pCritereBonCour.getIdCarnetBonCour().getNumero() > 0) {
        requeteSql.ajouterConditionAnd("BDCDC", "=", pCritereBonCour.getIdCarnetBonCour().getNumero());
      }
    }
    // Si on a un numéro de bon de cour
    if (pCritereBonCour.getNumeroBonCour() > 0) {
      requeteSql.ajouterConditionAnd("BDBDC", "=", pCritereBonCour.getNumeroBonCour());
    }
    // Si on a un identifiant de magasinier
    if (pCritereBonCour.getIdMagasinier() != null) {
      requeteSql.ajouterConditionAnd("C4VDE", "=", pCritereBonCour.getIdMagasinier().getCode());
    }
    else if (pCritereBonCour.isIgnorerCarnetNonAttribue()) {
      requeteSql.ajouterConditionAnd("C4VDE", "<>", ' ');
    }
    
    // Si on a un état de bon de cour
    if (pCritereBonCour.getEtatBonCour() != null) {
      requeteSql.ajouterConditionAnd("BDETA", "=", pCritereBonCour.getEtatBonCour().getCode());
    }
    
    requeteSql.ajouter(" ORDER BY BDCDC, BDBDC");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Création de la liste des id avec les données trouvées
    List<IdBonCour> listeIdBonCour = new ArrayList<IdBonCour>();
    for (GenericRecord record : listeGenericRecord) {
      listeIdBonCour.add(initialiserIdBonCour(record));
    }
    return listeIdBonCour;
  }
  
  /**
   * Charger la liste des carnets des bons de cour correspondants à la liste de leurs identifiants.
   */
  public ListeCarnetBonCour chargerListeCarnetBonCour(List<IdCarnetBonCour> pListeIdCarnetBonCour) {
    if (pListeIdCarnetBonCour == null || pListeIdCarnetBonCour.isEmpty()) {
      throw new MessageErreurException("Les identifiants de carnets de bons de cour sont invalides.");
    }
    
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT *  FROM ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.CARNET_BON_COUR);
    requeteSql.ajouter(" WHERE");
    requeteSql.ajouterConditionAnd("C4ETB", "=", pListeIdCarnetBonCour.get(0).getIdEtablissement().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("C4MAG", "=", pListeIdCarnetBonCour.get(0).getIdMagasin().getCode());
    
    // Constitution de la liste des numéros de carnets
    for (IdCarnetBonCour idCarnetBonCour : pListeIdCarnetBonCour) {
      IdCarnetBonCour.controlerId(idCarnetBonCour, true);
      requeteSql.ajouterValeurAuGroupe("indicatif", idCarnetBonCour.getNumero());
    }
    
    // On ajoute la liste des numéros de carnet
    requeteSql.ajouter(" AND C4CDC ");
    requeteSql.ajouterGroupeDansRequete("indicatif", "in");
    requeteSql.ajouter(" ORDER BY C4ETB, C4MAG, C4CDC DESC");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Création de la liste objets avec les données trouvées
    ListeCarnetBonCour listeCarnetBonCour = new ListeCarnetBonCour();
    for (GenericRecord record : listeGenericRecord) {
      listeCarnetBonCour.add(initialiserCarnetBonCour(record));
    }
    return listeCarnetBonCour;
  }
  
  /**
   * Charger un carnet des bons de cour correspondants à l'identifiant donné.
   */
  public CarnetBonCour chargerCarnetBonCour(IdCarnetBonCour pIdCarnetBonCour) {
    if (pIdCarnetBonCour == null) {
      throw new MessageErreurException("Les identifiants de carnets de bon de cour sont invalides.");
    }
    
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT *  FROM ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.CARNET_BON_COUR);
    requeteSql.ajouter(" WHERE");
    requeteSql.ajouterConditionAnd("C4ETB", "=", pIdCarnetBonCour.getIdEtablissement().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("C4MAG", "=", pIdCarnetBonCour.getIdMagasin().getCode());
    requeteSql.ajouterConditionAnd("C4CDC", "=", pIdCarnetBonCour.getNumero());
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    
    // Création de la l'objet avec les données trouvées
    return initialiserCarnetBonCour(listeGenericRecord.get(0));
  }
  
  /**
   * Charger la liste des bons de cour correspondants à la liste de leurs identifiants.
   */
  public ListeBonCour chargerListeBonCour(List<IdBonCour> pListeIdBonCour) {
    if (pListeIdBonCour == null || pListeIdBonCour.isEmpty()) {
      throw new MessageErreurException("Les identifiants de carnets de bon de cour sont invalides.");
    }
    
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT *  FROM ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR);
    requeteSql.ajouter(" WHERE");
    requeteSql.ajouterConditionAnd("BDETB", "=", pListeIdBonCour.get(0).getIdCarnetBonCour().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("BDMAG", "=", pListeIdBonCour.get(0).getIdCarnetBonCour().getIdMagasin().getCode());
    
    // Constitution de la liste des numéros de carnets
    for (IdBonCour idBonCour : pListeIdBonCour) {
      IdBonCour.controlerId(idBonCour, true);
      requeteSql.ajouterValeurAuGroupe("indicatif", idBonCour.getNumero());
    }
    
    // On ajoute la liste des numéros de carnet
    requeteSql.ajouter(" AND BDBDC ");
    requeteSql.ajouterGroupeDansRequete("indicatif", "in");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Création de la liste objets avec les données trouvées
    ListeBonCour ListeBonCour = new ListeBonCour();
    for (GenericRecord record : listeGenericRecord) {
      ListeBonCour.add(initialiserBonCour(record));
    }
    return ListeBonCour;
  }
  
  public void modifierBonCour(BonCour pBonCour) {
    if (pBonCour == null) {
      throw new MessageErreurException("Le bon de cour est invalide.");
    }
    // QueryManager.
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("UPDATE ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR);
    requeteSql.ajouter(" SET");
    requeteSql.ajouter(" BDETA =" + pBonCour.getEtatBonCour().getCode());
    requeteSql.ajouter(" ,");
    if (pBonCour.getIdDocumentGenere() != null) {
      if (pBonCour.getIdDocumentGenere().getCodeEntete() != null) {
        requeteSql.ajouter(" BDCOD =" + RequeteSql.formaterCharacterSQL(pBonCour.getIdDocumentGenere().getCodeEntete().getCode()));
        requeteSql.ajouter(" ,");
      }
      if (pBonCour.getIdDocumentGenere().getNumero() != null) {
        requeteSql.ajouter(" BDNUM =" + pBonCour.getIdDocumentGenere().getNumero());
        requeteSql.ajouter(" ,");
        requeteSql.ajouter(" BDSUF =" + pBonCour.getIdDocumentGenere().getSuffixe());
        requeteSql.ajouter(" ,");
      }
      if (pBonCour.getIdDocumentGenere().getNumeroFacture() == null) {
        requeteSql.ajouter(" BDNFA = 0");
      }
      else {
        requeteSql.ajouter(" BDNFA =" + pBonCour.getIdDocumentGenere().getNumeroFacture());
      }
    }
    else {
      requeteSql.ajouter(" BDCOD = ' '");
      requeteSql.ajouter(" ,");
      requeteSql.ajouter(" BDNUM = 0");
      requeteSql.ajouter(" ,");
      requeteSql.ajouter(" BDSUF = 0");
      requeteSql.ajouter(" ,");
      requeteSql.ajouter(" BDNFA = 0");
    }
    requeteSql.ajouter(" ,");
    requeteSql.ajouter(" BDDAT =" + ConvertDate.dateToDb2(pBonCour.getDateDelivranceClient()));
    requeteSql.ajouter(" ,");
    requeteSql.ajouter(" BDNLI =" + pBonCour.getNumeroLigne());
    requeteSql.ajouter(" WHERE");
    requeteSql.ajouterConditionAnd("BDETB", "=", pBonCour.getId().getIdCarnetBonCour().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("BDMAG", "=", pBonCour.getId().getIdCarnetBonCour().getIdMagasin().getCode());
    requeteSql.ajouterConditionAnd("BDCDC", "=", pBonCour.getId().getIdCarnetBonCour().getNumero());
    requeteSql.ajouterConditionAnd("BDBDC", "=", pBonCour.getId().getNumero());
    
    // Exécuter la requête
    querymg.requete(requeteSql.getRequete());
  }
  
  // -- Méthodes privées
  
  /**
   * Création d'une liste de bons de cour à partir d'une liste d'identifiants.
   */
  private void creerListeBonCour(List<IdBonCour> pListeIdBonCour) {
    if (pListeIdBonCour == null || pListeIdBonCour.isEmpty()) {
      return;
    }
    
    // Insertion des bons de cour
    RequeteSql requeteSql = new RequeteSql();
    for (IdBonCour idBonCour : pListeIdBonCour) {
      // @formatter:off
      requeteSql.ajouter("insert into " + querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR
          + " (bdetb, bdmag, bdcdc, bdbdc, bdeta) values ("
          + RequeteSql.formaterStringSQL(idBonCour.getIdCarnetBonCour().getCodeEtablissement())
          + ", " + RequeteSql.formaterStringSQL(idBonCour.getIdCarnetBonCour().getIdMagasin().getCode())
          + ", " + idBonCour.getIdCarnetBonCour().getNumero()
          + ", " + idBonCour.getNumero()
          + ", " + EnumEtatBonCour.NON_UTILISE.getCode()
          + ")");
      // @formatter:on
      querymg.requete(requeteSql.getRequete());
      requeteSql.effacerRequete();
    }
  }
  
  /**
   * Suppression d'une liste de bons de cour à partir d'une liste d'identifiants.
   * Attention aucun contrôle n'est effectué, la liste est considérée comme validée.
   */
  private void supprimerListeBonCour(List<IdBonCour> pListeIdBonCour) {
    if (pListeIdBonCour == null || pListeIdBonCour.isEmpty()) {
      return;
    }
    
    // Suppression des bons de cour
    RequeteSql requeteSql = new RequeteSql();
    for (IdBonCour idBonCour : pListeIdBonCour) {
      // @formatter:off
      requeteSql.ajouter("delete " + querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR + " where ");
      requeteSql.ajouterConditionAnd("bdetb", "=", idBonCour.getIdCarnetBonCour().getCodeEtablissement());
      requeteSql.ajouterConditionAnd("bdmag", "=", idBonCour.getIdCarnetBonCour().getIdMagasin().getCode());
      requeteSql.ajouterConditionAnd("bdcdc", "=", idBonCour.getIdCarnetBonCour().getNumero());
      requeteSql.ajouterConditionAnd("bdbdc", "=", idBonCour.getNumero());
      // @formatter:on
      querymg.requete(requeteSql.getRequete());
      requeteSql.effacerRequete();
    }
  }
  
  /**
   * Constituer l'identifiant d'un carnet de bon de cour à partir de la lecture d'un record
   */
  private IdCarnetBonCour initialiserIdCarnetBonCour(GenericRecord pGenericRecord) {
    // Lire l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("C4ETB"));
    
    // Lire le magasin
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, pGenericRecord.getStringValue("C4MAG"));
    
    // Identifiant du carnet
    IdCarnetBonCour idCarnet = IdCarnetBonCour.getInstance(idEtablissement, idMagasin, pGenericRecord.getIntegerValue("C4CDC"));
    
    return idCarnet;
  }
  
  /**
   * Constituer l'identifiant d'un bon de cour à partir de la lecture d'un record
   */
  private IdBonCour initialiserIdBonCour(GenericRecord pGenericRecord) {
    // Lire l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("BDETB"));
    
    // Lire le magasin
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, pGenericRecord.getStringValue("BDMAG"));
    
    // Identifiant du carnet
    IdCarnetBonCour idCarnet = IdCarnetBonCour.getInstance(idEtablissement, idMagasin, pGenericRecord.getIntegerValue("BDCDC"));
    
    // Identifiant du bon de cour
    IdBonCour idBonCour = IdBonCour.getInstance(idCarnet, pGenericRecord.getIntegerValue("BDBDC"));
    
    return idBonCour;
  }
  
  /**
   * Constituer un objet CarnetBonCour à partir de la lecture d'un record.
   */
  private CarnetBonCour initialiserCarnetBonCour(GenericRecord pGenericRecord) {
    // Lire l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("C4ETB"));
    
    // Lire le magasin
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, pGenericRecord.getStringValue("C4MAG"));
    
    // Identifiant du carnet
    IdCarnetBonCour idCarnet = IdCarnetBonCour.getInstance(idEtablissement, idMagasin, pGenericRecord.getIntegerValue("C4CDC"));
    
    // Instanciation carnet
    CarnetBonCour carnet = new CarnetBonCour(idCarnet);
    if (pGenericRecord.isPresentField("C4VDE")) {
      String codeMagasinier = Constantes.normerTexte(pGenericRecord.getStringValue("C4VDE"));
      if (!codeMagasinier.isEmpty()) {
        carnet.setIdMagasinier(IdVendeur.getInstance(idEtablissement, codeMagasinier));
      }
    }
    if (pGenericRecord.isPresentField("C4CRE")) {
      carnet.setDateRemise(pGenericRecord.getDateValue("C4CRE"));
    }
    if (pGenericRecord.isPresentField("C4NPA")) {
      Integer valeur = pGenericRecord.getIntegerValue("C4NPA");
      if (valeur.intValue() > 0) {
        carnet.setNombrePages(valeur);
      }
    }
    if (pGenericRecord.isPresentField("C4BDC1")) {
      Integer valeur = pGenericRecord.getIntegerValue("C4BDC1");
      if (valeur.intValue() > 0) {
        carnet.setPremierNumero(pGenericRecord.getIntegerValue("C4BDC1"));
      }
    }
    
    carnet.setIdDernierBonCourUtilise(chargerDernierIdBonCourUtilise(idCarnet));
    
    return carnet;
  }
  
  /**
   * Constituer un objet BonCour à partir de la lecture d'un record
   */
  private BonCour initialiserBonCour(GenericRecord pGenericRecord) {
    // Lire l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("BDETB"));
    
    // Lire le magasin
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, pGenericRecord.getStringValue("BDMAG"));
    
    // Identifiant du carnet
    IdCarnetBonCour idCarnet = IdCarnetBonCour.getInstance(idEtablissement, idMagasin, pGenericRecord.getIntegerValue("BDCDC"));
    
    // identifiant du bon de cour
    IdBonCour idBonCour = IdBonCour.getInstance(idCarnet, pGenericRecord.getIntegerValue("BDBDC"));
    
    // identifiant document
    IdDocumentVente idDocument = null;
    if (pGenericRecord.getCharacterValue("BDCOD") != null) {
      if (pGenericRecord.getIntegerValue("BDNFA") > 0) {
        idDocument = IdDocumentVente.getInstancePourFacture(idEtablissement, pGenericRecord.getIntegerValue("BDNFA"));
      }
      else if (pGenericRecord.getCharacterValue("BDCOD") != ' ') {
        idDocument = IdDocumentVente.getInstanceGenerique(idEtablissement,
            EnumCodeEnteteDocumentVente.valueOfByCode(pGenericRecord.getCharacterValue("BDCOD")), pGenericRecord.getIntegerValue("BDNUM"),
            pGenericRecord.getIntegerValue("BDSUF"), pGenericRecord.getIntegerValue("BDNFA"));
      }
    }
    
    // Instanciation du bon de cour
    BonCour bonCour = new BonCour(idBonCour);
    if (pGenericRecord.getIntegerValue("BDETA").equals(EnumEtatBonCour.NON_UTILISE.getCode())
        && chargerDernierIdBonCourUtilise(idCarnet) != null
        && idBonCour.getNumero() < chargerDernierIdBonCourUtilise(idCarnet).getNumero()) {
      bonCour.setEtatBonCour(EnumEtatBonCour.MANQUANT);
    }
    else {
      bonCour.setEtatBonCour(EnumEtatBonCour.valueOfByCode(pGenericRecord.getIntegerValue("BDETA")));
    }
    
    bonCour.setIdDocumentGenere(idDocument);
    bonCour.setDateDelivranceClient(pGenericRecord.getDateValue("BDDAT"));
    bonCour.setNumeroLigne(pGenericRecord.getIntegerValue("BDNLI"));
    
    return bonCour;
  }
  
  /**
   * Renvoyer l'identifiant du dernier bon de cour utilisé dans un carnet
   */
  private IdBonCour chargerDernierIdBonCourUtilise(IdCarnetBonCour pIdCarnetBonCour) {
    if (pIdCarnetBonCour == null) {
      throw new MessageErreurException("L'identifiant de carnet de bons de cour est invalide.");
    }
    
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT * FROM ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.CARNET_BON_COUR);
    requeteSql.ajouter(" INNER JOIN ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR);
    requeteSql.ajouter(" ON BDETB = C4ETB AND BDMAG = C4MAG AND BDCDC = C4CDC");
    requeteSql.ajouter(" WHERE");
    requeteSql.ajouterConditionAnd("C4ETB", "=", pIdCarnetBonCour.getIdEtablissement().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("C4MAG", "=", pIdCarnetBonCour.getIdMagasin().getCode());
    requeteSql.ajouterConditionAnd("C4CDC", "=", pIdCarnetBonCour.getNumero());
    requeteSql.ajouterConditionAnd("BDETA", "=", EnumEtatBonCour.UTILISE.getCode());
    requeteSql.ajouter(" ORDER BY BDBDC DESC LIMIT 1");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Création de la liste objets avec les données trouvées
    IdBonCour idBonCour = null;
    for (GenericRecord record : listeGenericRecord) {
      if (record != null) {
        idBonCour = initialiserIdBonCour(record);
      }
    }
    return idBonCour;
  }
  
  /**
   * Renvoyer un bon de cour à partir de son numéro (les numéros de bon de cour sont uniques par établissement et magasin)
   */
  public BonCour chargerBonCour(IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, int pNumeroBonCour) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    IdMagasin.controlerId(pIdMagasin, true);
    if (pNumeroBonCour < 1) {
      throw new MessageErreurException("Le numéro de bon de cour est invalide.");
    }
    
    // Création de la requête principale
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT * FROM ");
    requeteSql.ajouter(querymg.getLibrary() + '.' + EnumTableBDD.BON_COUR);
    requeteSql.ajouter(" WHERE");
    requeteSql.ajouterConditionAnd("BDETB", "=", pIdEtablissement.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("BDMAG", "=", pIdMagasin.getCode());
    requeteSql.ajouterConditionAnd("BDBDC", "=", pNumeroBonCour);
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeGenericRecord == null || listeGenericRecord.isEmpty()) {
      return null;
    }
    // Création de la liste objets avec les données trouvées
    BonCour bonCour = null;
    for (GenericRecord record : listeGenericRecord) {
      if (record != null) {
        bonCour = initialiserBonCour(record);
      }
    }
    return bonCour;
  }
  
}
