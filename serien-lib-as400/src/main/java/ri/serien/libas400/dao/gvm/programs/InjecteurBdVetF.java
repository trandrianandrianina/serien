/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.sql.exploitation.environnement.BibliothequeSpecifique;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.job.SurveillanceJob;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

public class InjecteurBdVetF {
  // Variables
  private SystemeManager systemeManager = null;
  private ContexteProgrammeRPG contexteProgrammeRPG = null;
  private QueryManager queryManager = null;
  
  private char lettreEnvironnement = 'W';
  private String nomBaseDeDonnees = null;
  private String nomBibliothequeTravail = "QTEMP";
  private boolean gestionComplementEntete = false;
  
  // Liste des champs obligatoires
  private String[] requiredFieldDBE = { "BEETB", "BEMAG", "BEDAT", "BERBV", "BEERL", "BENLI" };
  private String[] requiredFieldDBF = { "BFETB", "BFMAG", "BFDAT", "BFRBV", "BFERL", "BFNLI" };
  private String[] requiredFieldDBL = { "BLETB", "BLMAGE", "BLDAT", "BLRBV", "BLERL", "BLNLI", "BLQTE" };
  
  private StringBuilder sbk = new StringBuilder(1024);
  private StringBuilder sbv = new StringBuilder(1024);
  private ArrayList<String> log = new ArrayList<String>();
  private Bibliotheque bibliothequeEnvironnement = null;
  
  protected String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur.
   * Si la bibliothèque de travail est la curlib renseigner qd même nomBibliothequeTravail.
   * @param pSystemeManager
   * @param pNomBibliothequeTravail
   * @param pNomCurlib
   * @param pLettreEnvironnement
   * @param pBibliothequeEnvironnement
   */
  public InjecteurBdVetF(SystemeManager pSystemeManager, String pNomBibliothequeTravail, String pNomCurlib, char pLettreEnvironnement,
      Bibliotheque pBibliothequeEnvironnement) {
    this(pSystemeManager, pNomBibliothequeTravail, pNomCurlib, pBibliothequeEnvironnement);
    lettreEnvironnement = pLettreEnvironnement;
  }
  
  /**
   * Constructeur.
   * Si la bibliothèque de travail est la curlib renseigner qd même nomBibliothequeTravail.
   * @param pSystemeManager.
   * @param pNomBibliothequeTravail
   * @param pNomCurlib
   * @param pBibliothequeEnvironnement
   */
  public InjecteurBdVetF(SystemeManager pSystemeManager, String pNomBibliothequeTravail, String pNomCurlib,
      Bibliotheque pBibliothequeEnvironnement) {
    if (pBibliothequeEnvironnement == null) {
      throw new MessageErreurException("La bibliothèque d'environnement est invalide.");
    }
    systemeManager = pSystemeManager;
    setWrklib(pNomBibliothequeTravail);
    setCurlib(pNomCurlib);
    bibliothequeEnvironnement = pBibliothequeEnvironnement;
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise le début des opérations.
   * @return
   */
  public boolean initialiser(SurveillanceJob pSurveillanceJob) {
    if (nomBaseDeDonnees == null) {
      return false;
    }
    
    queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
    queryManager.setLibrary(nomBaseDeDonnees);
    contexteProgrammeRPG = new ContexteProgrammeRPG(systemeManager.getSystem(), nomBaseDeDonnees);
    boolean retour = contexteProgrammeRPG.initialiserLettreEnvironnement(lettreEnvironnement);
    if (!retour) {
      return false;
    }
    contexteProgrammeRPG.setSurveillanceJob(pSurveillanceJob);
    
    // Chargement de la bibliothèque spécifique dans le cas où elle est renseignée
    BibliothequeSpecifique bibliothequeSpecifique = new BibliothequeSpecifique(systemeManager);
    String nomBibliothequeSpecifique = bibliothequeSpecifique.getBibliothequeSpecifique(bibliothequeEnvironnement);
    nomBibliothequeSpecifique = Constantes.normerTexte(nomBibliothequeSpecifique);
    
    // Mise en ligne des bibbliothèques
    retour = contexteProgrammeRPG.ajouterBibliotheque("QGPL", false);
    retour = contexteProgrammeRPG.ajouterBibliotheque(bibliothequeEnvironnement.getNom(), false);
    retour = contexteProgrammeRPG.ajouterBibliotheque(lettreEnvironnement + "EXMAS", false);
    retour = contexteProgrammeRPG.ajouterBibliotheque(lettreEnvironnement + "EXPAS", false);
    retour = contexteProgrammeRPG.ajouterBibliotheque(lettreEnvironnement + "GVMAS", false);
    // Commenter cette ligne pour planter le programme volontairement
    retour = contexteProgrammeRPG.ajouterBibliotheque(lettreEnvironnement + "GVMX", false);
    if (!nomBibliothequeSpecifique.isEmpty()) {
      retour = contexteProgrammeRPG.ajouterBibliotheque(nomBibliothequeSpecifique, false);
    }
    retour = contexteProgrammeRPG.ajouterBibliotheque(nomBaseDeDonnees, true);
    return retour;
  }
  
  /**
   * Nettoie et créer les fichiers d'injection PINJBDV* dans la bibliothèque de travail.
   * @return
   */
  public boolean clearAndCreateAllFilesInWrkLib() {
    return clearAndCreateAllFiles(nomBibliothequeTravail);
  }
  
  /**
   * Nettoie et créer les fichiers d'injection PINJBDV* dans la curlib.
   * @return
   */
  public boolean clearAndCreateAllFilesInCurLib() {
    return clearAndCreateAllFiles(nomBibliothequeTravail);
  }
  
  /**
   * Nettoie et créer les fichiers d'injection PINJBDV* dans la bibliothèque voulue.
   * @param bib
   * @return
   */
  public boolean clearAndCreateAllFiles(String bib) {
    boolean retour = clearAndCreateFile(bib, "PINJBDVDSE");
    if (retour) {
      if (gestionComplementEntete) {
        retour = clearAndCreateFile(bib, "PINJBDVDSF");
      }
      if (retour) {
        retour = clearAndCreateFile(bib, "PINJBDVDSL");
        if (retour) {
          retour = clearAndCreateFile(bib, "PINJBDV");
        }
      }
    }
    return retour;
  }
  
  /**
   * Enregistre une liste d'enregistrements de type E, F ou L dans le pinjbdvdse, pinjbdvsf ou pinjbdvdsl.
   */
  public boolean insertRecord(ArrayList<GenericRecord> data) {
    if ((data == null) || (data.size() == 0)) {
      return false;
    }
    
    String table = null;
    boolean erreur = false;
    int i = 0;
    for (GenericRecord rcd : data) {
      i++;
      // Détermination da la table concernée
      // L'entête
      if (rcd.isPresentField("BEERL")) {
        table = "PINJBDVDSE";
        rcd.setRequiredField(requiredFieldDBE);
      }
      // L'entête complémentaire
      else if (rcd.isPresentField("BFERL")) {
        table = "PINJBDVDSF";
        rcd.setRequiredField(requiredFieldDBF);
      }
      // Les lignes
      else if (rcd.isPresentField("BLERL")) {
        table = "PINJBDVDSL";
        if (rcd.isPresentField("BLART")) {
          rcd.setRequiredField(requiredFieldDBL);
        }
      }
      if (table == null) {
        msgErreur += "Le record n°" + i + " n'appartient ni au PINJBDVSE, PINJBDVSF ou PINJBDVSL.\n";
        continue;
      }
      
      // Insertion de l'enregistrement
      String requete = rcd.getInsertSQL(table, nomBibliothequeTravail);
      if (requete != null) {
        int retour = queryManager.requete(requete);
        if (retour == QueryManager.ERREUR) {
          msgErreur += "La requête (" + requete + ") n'a pas pu être exécuté.\n" + queryManager.getMsgError() + '\n';
          erreur = true;
        }
      }
      else {
        msgErreur += "La requête pour le record n°" + i + " n'a pas pu être générée.\n" + rcd.getMsgError() + '\n';
        erreur = true;
      }
    }
    return !erreur;
  }
  
  /**
   * Enregistre un seul enregistrement de type E ou L dans le pinjbdvdse ou pinjbdvdsl.
   * @param data
   * @return
   */
  public boolean insertRecord(HashMap<String, Object> data) {
    if ((data == null) || (data.size() == 0)) {
      return false;
    }
    
    // On concatène les données récoltées
    sbk.setLength(0);
    sbv.setLength(0);
    Object valeur;
    for (Entry<String, Object> entry : data.entrySet()) {
      sbk.append(entry.getKey()).append(',');
      valeur = entry.getValue();
      if (valeur instanceof String) {
        sbv.append('\'').append(valeur).append("', ");
      }
      else if (valeur instanceof BigDecimal) {
        sbv.append(((BigDecimal) valeur).doubleValue()).append(", ");
      }
      else {
        sbv.append(valeur).append(", ");
      }
    }
    sbk.deleteCharAt(sbk.length() - 1);
    sbv.deleteCharAt(sbv.length() - 2);
    
    // On détermine le fichier concerné
    String table;
    if (data.get("BEERL") != null) {
      table = "PINJBDVDSE";
    }
    else if (data.get("BFERL") != null) {
      table = "PINJBDVDSF";
    }
    else if (data.get("BLERL") != null) {
      table = "PINJBDVDSL";
    }
    else {
      return false;
    }
    
    // Construction de la requête
    sbk.insert(0, " (").insert(0, table).insert(0, '.').insert(0, nomBibliothequeTravail).insert(0, "INSERT INTO ").append(") VALUES (")
        .append(sbv).append(')');
    
    // Insertion de l'enregistrement
    int retour = queryManager.requete(sbk.toString());
    return (retour != QueryManager.ERREUR);
  }
  
  /**
   * Lancement du programme d'injection (SGVMIVWSCL qui effectue un contrôle des données à injecter).
   * @return
   */
  public boolean injectFile(LinkedHashMap<String, String> rapport) {
    // On crée le paramètre pour l'appel au RPG
    ProgramParameter[] parameterList = new ProgramParameter[2];
    AS400Text XNBON = new AS400Text(11);
    AS400Text BIBTMP = new AS400Text(10);
    parameterList[0] = new ProgramParameter(XNBON.toBytes(String.format("%11s", " ")), 11);
    parameterList[1] = new ProgramParameter(BIBTMP.toBytes(nomBibliothequeTravail), 10);
    Bibliotheque bibliotheque =
        new Bibliotheque(IdBibliotheque.getInstance(lettreEnvironnement + "GVMAS"), EnumTypeBibliotheque.PROGRAMMES_SERIEN);
    boolean retour = contexteProgrammeRPG.executerProgramme("SGVMIVWSCL", bibliotheque, parameterList);
    if (!retour) {
      msgErreur += "Erreur lors du lancement du programme d'injection du PINJBDV.";
      return retour;
    }
    
    // Analyse des erreurs possibles
    retour = analyseErreurInjection(rapport);
    if (!retour) {
      msgErreur += log.size() + " erreurs(s) détectée(s), consulter la hashmap qui contient le détail des erreurs.\n";
    }
    // On récupère les informations du bon généré (numéro, etb)
    else {
      recupereInfosBon(rapport, (String) XNBON.toObject(parameterList[0].getOutputData()));
    }
    
    return retour;
  }
  
  // -- Méthodes privées
  
  /**
   * Analyse le paramètre XNBON du CL SGVMIVWSCL afin de récupérer les informations sur le bon créé.
   * @param unbon
   * @param xnbon
   */
  private void recupereInfosBon(LinkedHashMap<String, String> rapport, String xnbon) {
    // rapport.clear();
    if ((xnbon == null) || (xnbon.trim().equals(""))) {
      return;
    }
    
    rapport.put("TYPE", "" + xnbon.charAt(0)); // D pour Devis, ...
    rapport.put("ETB", xnbon.substring(1, 4));
    rapport.put("NUMERO", xnbon.substring(4, 10));
    rapport.put("SUFFIXE", xnbon.substring(10));
  }
  
  /**
   * Lecture du fichier fchlog suite à l'injection.
   * @param alog
   */
  private void lectureFchlog() {
    log.clear();
    ArrayList<GenericRecord> fchlog = queryManager.select("select * from " + nomBibliothequeTravail + ".FCHLOG");
    if (fchlog == null) {
      return;
    }
    
    for (GenericRecord rcd : fchlog) {
      log.add((String) rcd.getField(0));
    }
  }
  
  /**
   * Analyse des erreurs possibles lors de l'injection (enregistrements du FCHLOG).
   * Détail des champs (CSV) du FCHLOG
   * 1: PINJBDV
   * 2: Date d'injecttion
   * 3: Heure d'injection
   * 4: Code erreur
   * 5: Clé de l'enregistrement en erreur
   * 6: Détail de l'erreur
   * @param detailErreurs
   * @return
   */
  private boolean analyseErreurInjection(LinkedHashMap<String, String> detailErreurs) {
    boolean retour = true;
    
    // Lecture du fichier de log généré par le RPG
    lectureFchlog();
    
    // Analyse des données
    int indice = 0;
    detailErreurs.clear();
    for (String chaine : log) {
      String[] champs = chaine.split(";");
      if (!champs[3].trim().equals("")) {
        retour = false;
        String ch_indice = String.format("%02d", ++indice);
        detailErreurs.put(ch_indice + "_CLEF", champs[4]);
        detailErreurs.put(ch_indice + "_CODE_ERREUR", champs[3]);
        detailErreurs.put(ch_indice + "_LIBELLE_ERREUR", champs[5]);
      }
    }
    
    return retour;
  }
  
  /**
   * Copie les fichiers PINJBDVDSE et PINJBDVDSL dans le PINJBDV.
   * @return
   */
  private boolean copy2Pinjbdv() {
    boolean retour = contexteProgrammeRPG.executerCommande("CPYF FROMFILE(" + nomBibliothequeTravail + "/PINJBDVDSE) TOFILE("
        + nomBibliothequeTravail + "/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)");
    String codeErreur = contexteProgrammeRPG.getCodeRetour();
    if (retour && (codeErreur.equals("CPC2955") || codeErreur.equals("CPF4011"))) {
      retour = contexteProgrammeRPG.executerCommande("CPYF FROMFILE(" + nomBibliothequeTravail + "/PINJBDVDSL) TOFILE("
          + nomBibliothequeTravail + "/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)");
      codeErreur = contexteProgrammeRPG.getCodeRetour();
      if (!(retour && (codeErreur.equals("CPC2955") || codeErreur.equals("CPF4011")))) {
        msgErreur += "Code erreur:" + codeErreur + " pour " + "CPYF FROMFILE(" + nomBibliothequeTravail + "/PINJBDVDSL) TOFILE("
            + nomBibliothequeTravail + "/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)";
        return false;
      }
    }
    else {
      msgErreur += "Code erreur:" + codeErreur + " pour " + "CPYF FROMFILE(" + nomBibliothequeTravail + "/PINJBDVDSE) TOFILE("
          + nomBibliothequeTravail + "/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)";
      return false;
    }
    
    return true;
  }
  
  /**
   * Injecte le fichier PINJBDV.
   * @return
   */
  private boolean injectePinjbdv() {
    boolean retour = true;
    String codeErreur = "";
    // On travaille dans la CURLIB
    if (nomBibliothequeTravail.equals(nomBaseDeDonnees)) {
      retour = contexteProgrammeRPG.executerCommande("CALL PGM(SGVMIVCL) PARM('X')");
      // <- Ca plante chez nous (msgo CPF4103) ca cache un truc !!
      // (10/01/2013) // Attention peut planter sur le SNDRCVFM (*REQUESTER)
      // car le fichier PSEMSEC n'est pas là où il faut... Et M_GPL doit être en ligne (sinon ça marche moins bien)
      codeErreur = contexteProgrammeRPG.getCodeRetour();
      if (!retour) {
        msgErreur += "Code erreur:" + codeErreur + " pour " + "CALL PGM(SGVMIVCL) PARM('X')";
      }
    }
    else {
      retour = contexteProgrammeRPG.executerCommande("CALL PGM(SGVMIVCM) PARM('X' '" + nomBibliothequeTravail + "')");
      codeErreur = contexteProgrammeRPG.getCodeRetour();
      if (!retour) {
        msgErreur += "Code erreur:" + codeErreur + " pour " + "CALL PGM(SGVMIVCM) PARM('X' '" + nomBibliothequeTravail + "')";
      }
    }
    
    return retour;
  }
  
  /**
   * Nettoie et créer le fichier d'injection PINJBDV.
   * @param bib
   * @param pinjbdv
   * @return
   */
  private boolean clearAndCreateFile(String bib, String pinjbdv) {
    if (contexteProgrammeRPG == null) {
      return false;
    }
    
    // On cleare le fichier
    boolean retour = contexteProgrammeRPG.executerCommande("CLRPFM FILE(" + bib + "/" + pinjbdv + ")");
    if (!retour) {
      // Il y a eu une erreur, il est possible que le fichier n'existe pas
      if (!contexteProgrammeRPG.getCodeRetour().equals("CPF3101")) {
        retour = contexteProgrammeRPG.executerCommande("CRTPF FILE(" + bib + "/" + pinjbdv + ") SRCFILE(" + lettreEnvironnement
            + "GVMAS/QDDSFCH) SRCMBR(*FILE) OPTION(*NOSRC *NOLIST) SIZE(*NOMAX) WAITFILE(*CLS) WAITRCD(*NOMAX)");
      }
    }
    
    return retour;
  }
  
  // -- Accesseurs
  
  /**
   * @return le wrklib
   */
  public String getWrklib() {
    return nomBibliothequeTravail;
  }
  
  /**
   * @param wlib la library de travail
   */
  public void setWrklib(String wlib) {
    this.nomBibliothequeTravail = wlib;
  }
  
  /**
   * @return le curlib
   */
  public String getCurlib() {
    return nomBaseDeDonnees;
  }
  
  /**
   * @param curlib le curlib à définir
   */
  public void setCurlib(String curlib) {
    this.nomBaseDeDonnees = curlib;
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  public boolean isGestionComplementEntete() {
    return gestionComplementEntete;
  }
  
  public void setGestionComplementEntete(boolean gestionComplementEntete) {
    this.gestionComplementEntete = gestionComplementEntete;
  }
  
}
