/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx003.v2;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.JsonSuiviCommandeMagentoV2;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;

public class ExportationSuiviCommandeV2 extends BaseGroupDB {
  // Variables
  private SystemeManager system = null;
  
  /**
   * Constructeur.
   */
  public ExportationSuiviCommandeV2(QueryManager quer) {
    super(quer);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner un suivi de commande Série N sous forme de message JSON
   */
  public String retournerUnSuiviCommandeMagentoJSON(FluxMagento record) {
    if (record == null || record.getFLETB() == null || record.getFLCOD() == null) {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeMagentoJSON() record Null ou corrompu");
      return null;
    }
    
    String retour = null;
    
    ArrayList<GenericRecord> suivisCommandeDB2 = retournerUnSuiviCommandeDB2(record.getFLETB(), record.getFLCOD());
    if (suivisCommandeDB2 != null) {
      // On créé un suivi commande spécifique Magento
      JsonSuiviCommandeMagentoV2 suiviCommandeMag = new JsonSuiviCommandeMagentoV2(record);
      Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), suiviCommandeMag.getEtb());
      if (numeroInstance == null) {
        majError(
            "[ExportSuiviCmdeMagento] retournerUnSuiviCommandeMagentoJSON() : probleme d'interpretation de l'ETb vers l'instance Magento");
        return null;
      }
      suiviCommandeMag.setIdInstanceMagento(numeroInstance);
      
      // Informations basiques de l'article
      if (suiviCommandeMag.mettreAJoursuiviCommande(suivisCommandeDB2)) {
        // debut de la construction du JSON
        if (initJSON()) {
          if (suiviCommandeMag.getCode() != null && suiviCommandeMag.getIdInstanceMagento() > 0) {
            try {
              retour = gson.toJson(suiviCommandeMag);
            }
            catch (Exception e) {
              // TODO Bloc catch généré automatiquement
              majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeMagentoJSON() Parse JSON: " + e.getMessage());
              return null;
            }
          }
          else {
            majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeMagentoJSON() valeurs nécessaires de suiviCommandeMag à NULL: ");
          }
        }
        else {
          majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeMagentoJSON() probleme initJSON()");
        }
      }
      else {
        majError(suiviCommandeMag.getMsgError());
      }
    }
    else {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeMagentoJSON() unSuiviCommandeDB2 à NULL");
    }
    
    return retour;
  }
  
  /**
   * Retourner le suivi d'une commande Série N sous forme GenericRecord : A AMELIORER AVEC LES CLASSES METIER COMMUNES DE STEPH.
   */
  public ArrayList<GenericRecord> retournerUnSuiviCommandeDB2(String pCodeEtablissement, String pNumeroDocument) {
    if (pCodeEtablissement == null || pNumeroDocument == null) {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeDB2() etb ou numBon Null ou corrompus");
      return null;
    }
    
    String numero = null;
    String suffixe = null;
    
    // Découpage du paramètre code entre numéro de commande et suffixe (6 caractères + 1) on élimine les 4 premiers caractères (type de
    // bon sur 1 + société sur 3)
    if (pNumeroDocument.length() == 11) {
      numero = pNumeroDocument.trim().substring(4, pNumeroDocument.length() - 1);
      suffixe = pNumeroDocument.trim().substring(pNumeroDocument.length() - 1);
    }
    else {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeDB2() numBon longueur != 11: " + pNumeroDocument);
      return null;
    }
    
    // Le test du E1COD doit se faire sur la valeur 'E' pour la commande normale ou '9' pour une commande annulée (si la PS66 = '2')
    // Changement à cause de l'introduction du 'e' pour le comptoir
    ArrayList<GenericRecord> listeCommandes = queryManager
        .select("SELECT E1COD, E1CCT, E1ETA, E1AVR, COTRK FROM " + queryManager.getLibrary() + ".PGVMEBCM LEFT JOIN " + queryManager.getLibrary()
            + ".PGVMNCOM ON E1ETB = COETB AND E1NUM = CONUM AND E1SUF = COSUF WHERE (E1COD = 'E' OR E1COD = '9') AND E1ETB = '"
            + pCodeEtablissement.trim() + "' AND E1NUM = '" + numero + "' AND E1SUF = '" + suffixe + "'  ");
    
    if (listeCommandes != null && listeCommandes.size() > 0) {
      return listeCommandes;
    }
    else {
      majError("[GM_Export_SuiviCommande] retournerUnSuiviCommandeDB2() listeCommandes à NULL ou 0 pour : " + pCodeEtablissement + "/"
          + pNumeroDocument);
      return null;
    }
  }
  
  @Override
  public void dispose() {
    queryManager = null;
    if (system != null) {
      system.deconnecter();
    }
  }
}
