/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.client.recherche;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.chantier.CritereChantier;
import ri.serien.libcommun.gescom.vente.chantier.EnumStatutChantier;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.chantier.ListeChantier;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlDocumentsClient {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlDocumentsClient(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  /**
   * Retourner la liste des documents de vente de type chantier
   */
  private List<IdDocumentVente> retournerListeIdChantier(String pChamps, String pJointure, String pConditionsBase,
      String pConditionsSupplementaires, String pOrderBase, List<IdDocumentVente> pListeIdDocuments) {
    if (pListeIdDocuments == null) {
      pListeIdDocuments = new ArrayList<IdDocumentVente>();
    }
    // Requete de base
    String requete = "SELECT " + pChamps + " from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE
        + " doc WHERE " + pConditionsBase + " AND doc.E1PFC = '*CH' " + pConditionsSupplementaires + pOrderBase;
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    
    for (GenericRecord record : listeRecord) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("E1ETB"));
      EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.DEVIS;
      IdDocumentVente idDocument = IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, record.getIntegerValue("E1NUM"),
          record.getIntegerValue("E1SUF"), record.getIntegerValue("E1NFA"));
      pListeIdDocuments.add(idDocument);
    }
    
    return pListeIdDocuments;
  }
  
  /**
   * Recherche de toutes les ID de chantiers pour un client.
   */
  public List<IdChantier> chargerListeIdChantier(CritereChantier pCriteres) {
    // Requete de base
    String requete =
        "SELECT distinct doc.E1ETB, doc.E1NUM from " + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc "
            + " where doc.E1PFC = '*CH' AND doc.E1ETB = " + RequeteSql.formaterStringSQL(pCriteres.getCodeEtablissement())
            + " and doc.E1COD <> 'e' and doc.E1COD <> 'd' and doc.E1COD <> ' '  and doc.E1COD <> 'X'"
            + " and doc.E1IN18 <> 'I' and doc.E1IN18 <> 'S'" + " and doc.E1TTC >= 0";
    
    // Client
    if (pCriteres.getIdClient() != null && pCriteres.getIdClient().getNumero() > 0) {
      requete += " and (doc.E1CLLP = " + pCriteres.getIdClient().getNumero() + " or doc.E1CLFP = " + pCriteres.getIdClient().getNumero()
          + ") and (doc.E1CLLS = " + pCriteres.getIdClient().getSuffixe() + " or doc.E1CLFS = " + pCriteres.getIdClient().getSuffixe()
          + ")";
    }
    
    // numéro de document
    if (pCriteres.getNumeroChantier() > 0) {
      requete += " and doc.E1NUM = " + Constantes.convertirIntegerEnTexte(pCriteres.getNumeroChantier(), 6).substring(0, 6);
    }
    
    // statut
    if (pCriteres.getStatut() != null) {
      // Récupérer la date du jour en enlevant l'heure
      Date maintenant = new Date();
      try {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        maintenant = formatter.parse(formatter.format(maintenant));
      }
      catch (Exception e) {
        Trace.erreur("Erreur pour convertir la date du jour : " + maintenant);
      }
      switch (pCriteres.getStatut()) {
        case VALIDE:
          requete += " and doc.E1TDV = " + pCriteres.getStatut().getCode();
          requete += " and (doc.E1DAT2 >= " + ConvertDate.dateToDb2(maintenant) + " or doc.E1DAT2 = 0)";
          break;
        case VALIDITE_DEPASSEE:
          requete += " and (doc.E1DAT2 < " + ConvertDate.dateToDb2(maintenant) + " and doc.E1DAT2 > 0)";
          break;
        
        default:
          requete += " and doc.E1TDV = " + pCriteres.getStatut().getCode();
          break;
      }
    }
    
    // Chantiers non bloqués seulement
    if (!pCriteres.isAvecBloque()) {
      requete += " and doc.E1NHO = ' '";
    }
    
    // Référence
    if (!pCriteres.getTexteRechercheGenerique().trim().isEmpty()) {
      requete += " and (UPPER(doc.E1NCC) like "
          + RequeteSql.formaterStringSQL("%" + pCriteres.getTexteRechercheGenerique().toUpperCase() + "%") + " or UPPER(doc.E1RCC) like "
          + RequeteSql.formaterStringSQL("%" + pCriteres.getTexteRechercheGenerique().toUpperCase() + "%") + ")";
    }
    
    // Plage de dates
    if (pCriteres.getDateCreationDebut() != null && pCriteres.getDateCreationFin() != null) {
      requete += " and (doc.E1CRE >= " + ConvertDate.dateToDb2(pCriteres.getDateCreationDebut()) + " and doc.E1CRE <= "
          + ConvertDate.dateToDb2(pCriteres.getDateCreationFin()) + ")";
    }
    
    // tri
    requete += " ORDER BY doc.E1NUM DESC";
    
    List<IdChantier> listeIdChantier = new ArrayList<IdChantier>();
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if (listeRecord == null) {
      return listeIdChantier;
    }
    
    for (GenericRecord record : listeRecord) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("E1ETB"));
      IdChantier idChantier = IdChantier.getInstance(idEtablissement, record.getIntegerValue("E1NUM"));
      listeIdChantier.add(idChantier);
    }
    
    return listeIdChantier;
  }
  
  /**
   * Charger la liste des chantiers dont les identifiants sont fournis en paramètre.
   * 
   * @param pListeIdChantier Liste d'identfifiants de chantier (ne doit pas être null).
   * @return Liste des chantiers correspondants aux identifiants.
   */
  public ListeChantier chargerListeChantier(List<IdChantier> pListeIdChantier) {
    // Vérifier les paramètres
    if (pListeIdChantier == null) {
      throw new MessageErreurException("Impossible de charger la liste des chantiers car les paramètres sont invalides");
    }
    
    // Sortir de suite si la liste des identifiants est vide
    ListeChantier listeChantier = new ListeChantier();
    if (pListeIdChantier.isEmpty()) {
      return listeChantier;
    }
    
    // Construire la requête
    String requete =
        "select distinct E1TOP, E1ETB, E1NUM, E1MAG, E1CRE, E1DCO, E1DAT2, E1SL1, E1SL2, E1SL3, E1TTC, E1RCC, E1NCC, E1VDE, E1NFA, E1ETA, "
            + "E1TDV, E1MEX, E1CLFP, E1CLFS, E1DLS, E1ACT, AVNOM, AVCPL, AVRUE, AVLOC, AVCDP, AVVIL, AVPAY " + " from "
            + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc INNER JOIN " + querymg.getLibrary() + '.'
            + EnumTableBDD.DOCUMENT_VENTE_ADRESSE
            + " adv ON doc.E1ETB = adv.AVETB AND doc.E1NUM = adv.AVNUM and AVCOD = 'C' and AVNLI = '0'" + " where doc.E1ETB = "
            + RequeteSql.formaterStringSQL(pListeIdChantier.get(0).getCodeEtablissement()) + " and doc.E1PFC = '*CH'"
            + " and doc.E1COD = '" + EnumCodeEnteteDocumentVente.DEVIS + "' and (";
    
    for (IdChantier idChantier : pListeIdChantier) {
      if (idChantier != null) {
        IdChantier.controlerId(idChantier, true);
        requete += "doc.E1NUM = " + idChantier.getNumero() + " or ";
      }
    }
    requete = requete.substring(0, requete.lastIndexOf(" or "));
    requete += ")";
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if (listeRecord == null) {
      return listeChantier;
    }
    
    // Remplir la liste des chantiers
    for (GenericRecord record : listeRecord) {
      IdChantier idChantier = null;
      try {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("E1ETB"));
        idChantier = IdChantier.getInstance(idEtablissement, record.getIntegerValue("E1NUM", 0));
        Chantier chantier = new Chantier(idChantier);
        
        chantier.setIdMagasin(IdMagasin.getInstance(idEtablissement, record.getStringValue("E1MAG")));
        chantier.setDateCreation(record.getDateValue("E1CRE"));
        chantier.setDateFinValidite(record.getDateValue("E1DAT2"));
        chantier.setReferenceCourte(record.getStringValue("E1NCC"));
        chantier.setVerrouille(record.getIntegerValue("E1TOP", 0) == 1);
        chantier.setStatutChantier(EnumStatutChantier.valueOfByCode(record.getIntegerValue("E1TDV", 0)));
        IdClient idClientFacture =
            IdClient.getInstance(idEtablissement, record.getIntegerValue("E1CLFP", 0), record.getIntegerValue("E1CLFS", 0));
        chantier.setIdClient(idClientFacture);
        chantier.setDateDebutValidite(record.getDateValue("E1DLS"));
        chantier.setLibelle(record.getStringValue("E1RCC"));
        
        // Adresse
        Adresse adresseFacturation = new Adresse();
        adresseFacturation.setNom(record.getStringValue("AVNOM"));
        adresseFacturation.setComplementNom(record.getStringValue("AVCPL"));
        adresseFacturation.setRue(record.getStringValue("AVRUE"));
        adresseFacturation.setLocalisation(record.getStringValue("AVLOC"));
        try {
          adresseFacturation.setCodePostal(record.getIntegerValue("AVCDP", 0));
        }
        catch (NumberFormatException e) {
        }
        adresseFacturation.setVille(record.getStringValue("AVVIL"));
        adresseFacturation.setCodePays(record.getStringValue("AVPAY"));
        
        chantier.setAdresse(adresseFacturation);
        
        listeChantier.add(chantier);
      }
      catch (Exception e) {
        // Une erreur sur un chantier ne doit pas empêcher de charger la liste
        Trace.erreur("Erreur sur un chantier lors du chargement d'une liste de chantiers : idChantier=" + idChantier);
      }
    }
    
    return listeChantier;
  }
  
  /**
   * Charge un chantier à partir de son identifiant
   */
  public Chantier chargerChantier(IdChantier pIdChantier) {
    Chantier chantier = null;
    String requete =
        "select distinct E1TOP, E1ETB, E1NUM, E1MAG, E1CRE, E1DCO, E1DAT2, E1SL1, E1SL2, E1SL3, E1TTC, E1RCC, E1NCC, E1VDE, E1NFA, E1ETA, "
            + "E1TDV, E1MEX, E1CLFP, E1CLFS, E1DLS, E1ACT, E1NHO, AVNOM, AVCPL, AVRUE, AVLOC, AVCDP, AVVIL, AVPAY" + " from "
            + querymg.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " doc INNER JOIN " + querymg.getLibrary() + '.'
            + EnumTableBDD.DOCUMENT_VENTE_ADRESSE
            + " adv ON doc.E1ETB = adv.AVETB AND doc.E1NUM = adv.AVNUM and AVCOD = 'C' and AVNLI = '0'" + " where doc.E1ETB = "
            + RequeteSql.formaterStringSQL(pIdChantier.getCodeEtablissement()) + " and doc.E1PFC = '*CH'" + " and doc.E1COD = '"
            + EnumCodeEnteteDocumentVente.DEVIS + "' and  doc.E1NUM = " + pIdChantier.getNumero();
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    for (GenericRecord record : listeRecord) {
      if (record != null) {
        
        IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("E1ETB"));
        chantier = new Chantier(IdChantier.getInstance(idEtablissement, record.getIntegerValue("E1NUM", 0)));
        
        chantier.setIdMagasin(IdMagasin.getInstance(idEtablissement, record.getStringValue("E1MAG")));
        chantier.setDateCreation(record.getDateValue("E1CRE"));
        chantier.setDateFinValidite(record.getDateValue("E1DAT2"));
        chantier.setReferenceCourte(record.getStringValue("E1NCC"));
        chantier.setVerrouille(record.getIntegerValue("E1TOP", 0) == 1);
        chantier.setStatutChantier(EnumStatutChantier.valueOfByCode(record.getIntegerValue("E1TDV", 0)));
        
        IdClient idClientFacture =
            IdClient.getInstance(idEtablissement, record.getIntegerValue("E1CLFP", 0), record.getIntegerValue("E1CLFS", 0));
        chantier.setIdClient(idClientFacture);
        chantier.setDateDebutValidite(record.getDateValue("E1DLS"));
        chantier.setLibelle(record.getStringValue("E1RCC"));
        chantier.setBloque(!record.getCharacterValue("E1NHO").equals(' '));
        
        // Adresse
        Adresse adresseChantier = new Adresse();
        adresseChantier.setNom(record.getStringValue("AVNOM"));
        adresseChantier.setComplementNom(record.getStringValue("AVCPL"));
        adresseChantier.setRue(record.getStringValue("AVRUE"));
        adresseChantier.setLocalisation(record.getStringValue("AVLOC"));
        try {
          adresseChantier.setCodePostal(record.getIntegerValue("AVCDP", 0));
        }
        catch (NumberFormatException e) {
        }
        adresseChantier.setVille(record.getStringValue("AVVIL"));
        adresseChantier.setCodePays(record.getStringValue("AVPAY"));
        
        chantier.setAdresse(adresseChantier);
        
      }
    }
    return chantier;
  }
  
  /**
   * Charge la ligne d'achat générée pour approvisionner une ligne de vente
   */
  public IdLigneAchat chargerLienLigneVenteLigneAchat(IdLigneVente pIdLigneVente) {
    
    String requete = "select distinct AAETB, AACOE, AANOE, AASOE, AALOE" + " from " + querymg.getLibrary() + '.'
        + EnumTableBDD.LIEN_LIGNE_VENTE_LIGNE_ACHAT + " where AAETB = "
        + RequeteSql.formaterStringSQL(pIdLigneVente.getCodeEtablissement()) + " and AACOS = "
        + RequeteSql.formaterCharacterSQL(pIdLigneVente.getCodeEntete().getCode()) + " and AANOS = " + pIdLigneVente.getNumero()
        + " and AASOS = " + pIdLigneVente.getSuffixe() + " and AALOS = " + pIdLigneVente.getNumeroLigne();
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    for (GenericRecord record : listeRecord) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("AAETB"));
      return IdLigneAchat.getInstance(idEtablissement, EnumCodeEnteteDocumentAchat.valueOfByCode(record.getCharacterValue("AACOE")),
          record.getIntegerValue("AANOE"), record.getIntegerValue("AASOE"), record.getIntegerValue("AALOE"));
    }
    return null;
  }
  
}
