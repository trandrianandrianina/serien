/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.database.record.GenericRecord;

public class Ligne_Cde_EDI_GP extends EntiteEDI {
  private GenericRecord donneesBrutes = null;
  
  private String id_ligne_1 = null;
  private String numero_ligne_2 = null;
  private String code_EAN_produit_3 = null;
  private String code_fournisseur_produit_4 = null;
  private String code_client_produit_5 = null;
  private String conditionnement_6 = null;
  private String quantite_ligne_7 = null;
  private String uniteQte_ligne_8 = null;
  private String prixU_net_ligne_9 = null;
  private String libelle_produit_10 = null;
  // private String poids_produit_11 = null;
  private String prixU_brut_ligne_20 = null;
  private String codeArticleSN = null;
  
  private String montantHT = null;
  private String codeTVA = null;
  private String tauxTVA = null;
  
  // dépend des champs ci dessus
  int nbChampsObligatoires = 11;
  
  /**
   * Constructeur avec champs pour import de données
   */
  public Ligne_Cde_EDI_GP(ParametresFluxBibli pParams, String ligne) {
    super(pParams);
    contenuBrut = ligne;
    
    if (contenuBrut == null) {
      return;
    }
    
    isValide = traitementContenu();
    // afficherLigneCde();
  }
  
  /**
   * Constructeur avec champs pour export de données
   */
  public Ligne_Cde_EDI_GP(ParametresFluxBibli pParams, GenericRecord record) {
    super(pParams);
    donneesBrutes = record;
    
    if (donneesBrutes == null) {
      return;
    }
    
    isValide = traitementContenuRecord();
  }
  
  /**
   * traitement du contenu de la commande
   */
  @Override
  protected boolean traitementContenu() {
    if (contenuBrut == null) {
      majError("[Ligne_Cde_EDI_GP] contenuBrut à NULL");
      return false;
    }
    
    boolean retour = false;
    
    // On decoupe en lignes d'enregistremeents avant tout
    if (contenuBrut.length() > 0) {
      tabDonnees = decoupageDeLigne();
    }
    // OK pour le découpage physique
    if (tabDonnees != null) {
      int compteur = 0;
      for (int i = 0; i < tabDonnees.length; i++) {
        switch (i) {
          case 0:
            id_ligne_1 = tabDonnees[i];
            compteur++;
            break;
          case 1:
            numero_ligne_2 = tabDonnees[i];
            compteur++;
            break;
          case 2:
            code_EAN_produit_3 = tabDonnees[i];
            compteur++;
            break;
          case 3:
            code_fournisseur_produit_4 = tabDonnees[i];
            compteur++;
            break;
          case 4:
            code_client_produit_5 = tabDonnees[i];
            compteur++;
            break;
          case 5:
            conditionnement_6 = tabDonnees[i];
            compteur++;
            break;
          case 6:
            quantite_ligne_7 = tabDonnees[i];
            compteur++;
            break;
          case 7:
            uniteQte_ligne_8 = tabDonnees[i];
            compteur++;
            break;
          case 8:
            prixU_net_ligne_9 = tabDonnees[i];
            compteur++;
            break;
          case 9:
            libelle_produit_10 = tabDonnees[i];
            compteur++;
            break;
          case 19:
            prixU_brut_ligne_20 = tabDonnees[i];
            compteur++;
            break;
          
          default:
            break;
        }
      }
      // Si toutes les variables ont été mises à jour
      if (compteur == nbChampsObligatoires) {
        retour = true;
      }
      else {
        majError("[EntiteEDI] tabDonnees length: " + tabDonnees.length + " -> compteur: " + compteur);
      }
      
    }
    else {
      majError("[Ligne_Cde_EDI_GP] tabDonnees à NULL");
      retour = false;
    }
    
    return retour;
  }
  
  /**
   * traitement du contenu de la commande
   */
  private boolean traitementContenuRecord() {
    if (donneesBrutes == null) {
      majError("[Ligne_Cde_EDI_GP] donneesBrutes à NULL");
      return false;
    }
    
    id_ligne_1 = "LIG";
    
    if (donneesBrutes.isPresentField("L1NLI") && !donneesBrutes.getField("L1NLI").toString().trim().equals("")) {
      numero_ligne_2 = donneesBrutes.getField("L1NLI").toString().trim();
    }
    else {
      majError("[Ligne_Cde_EDI_GP] L1NLI corrompu ");
      return false;
    }
    
    if (donneesBrutes.isPresentField("A1GCD") && !donneesBrutes.getField("A1GCD").toString().trim().equals("")) {
      code_EAN_produit_3 = donneesBrutes.getField("A1GCD").toString().trim();
    }
    else {
      majError("[Ligne_Cde_EDI_GP] A1GCD corrompu ligne " + numero_ligne_2);
      return false;
    }
    
    if (donneesBrutes.isPresentField("L1ART") && !donneesBrutes.getField("L1ART").toString().trim().equals("")) {
      code_fournisseur_produit_4 = donneesBrutes.getField("L1ART").toString().trim();
    }
    else {
      majError("[Ligne_Cde_EDI_GP] L1ART corrompu ligne " + numero_ligne_2);
      return false;
    }
    
    // Cette donnée n'est valable que dans le cas des factures ON NE BLOQUE PAS
    if (donneesBrutes.isPresentField("A1LIB")) {
      libelle_produit_10 = donneesBrutes.getField("A1LIB").toString().trim();
    }
    else {
      /*majError("[Ligne_Cde_EDI_GP] A1LIB corrompu ligne " + numero_ligne_2);
      return false;*/
      libelle_produit_10 = "";
    }
    
    if (donneesBrutes.isPresentField("L1QTE") && !donneesBrutes.getField("L1QTE").toString().trim().equals("")) {
      quantite_ligne_7 = donneesBrutes.getField("L1QTE").toString().trim();
    }
    else {
      majError("[Ligne_Cde_EDI_GP] L1QTE corrompu ligne " + numero_ligne_2);
      return false;
    }
    
    if (donneesBrutes.isPresentField("L1UNV") && !donneesBrutes.getField("L1UNV").toString().trim().equals("")) {
      uniteQte_ligne_8 = donneesBrutes.getField("L1UNV").toString().trim();
      // Forçage moisi demandé par T.Charlemagne car nous n'avons que 2 caractères...
      if (uniteQte_ligne_8.equals("PC")) {
        uniteQte_ligne_8 = parametresFlux.getUnite_vente_EDI();
      }
    }
    else {
      majError("[Ligne_Cde_EDI_GP] L1UNV corrompu ligne " + numero_ligne_2);
      return false;
    }
    
    if (donneesBrutes.isPresentField("L1PVN") && !donneesBrutes.getField("L1PVN").toString().trim().equals("")) {
      prixU_net_ligne_9 = donneesBrutes.getField("L1PVN").toString().trim();
    }
    else {
      majError("[Ligne_Cde_EDI_GP] L1PVN corrompu ligne " + numero_ligne_2);
      return false;
    }
    
    if (donneesBrutes.isPresentField("L1MHT") && !donneesBrutes.getField("L1MHT").toString().trim().equals("")) {
      montantHT = donneesBrutes.getField("L1MHT").toString().trim();
    }
    else {
      majError("[Ligne_Cde_EDI_GP] L1MHT corrompu ligne " + numero_ligne_2);
      return false;
    }
    
    if (donneesBrutes.isPresentField("L1TVA") && !donneesBrutes.getField("L1TVA").toString().trim().equals("")) {
      codeTVA = donneesBrutes.getField("L1TVA").toString().trim();
      // TODO va falloir faire propre pour la TVA (lire la TVA de l'entête en fonction du code de l'entête
      if (codeTVA.trim().equals("1")) {
        tauxTVA = "20";
      }
      else if (codeTVA.trim().equals("7")) {
        tauxTVA = "0";
      }
      else {
        tauxTVA = "20";
      }
    }
    else {
      majError("[Ligne_Cde_EDI_GP] L1TVA corrompu ligne " + numero_ligne_2);
      return false;
    }
    
    return true;
  }
  
  public String getId_ligne_1() {
    return id_ligne_1;
  }
  
  public void setId_ligne_1(String id_ligne_1) {
    this.id_ligne_1 = id_ligne_1;
  }
  
  public String getNumero_ligne_2() {
    return numero_ligne_2;
  }
  
  public void setNumero_ligne_2(String numero_ligne_2) {
    this.numero_ligne_2 = numero_ligne_2;
  }
  
  public String getCode_EAN_produit_3() {
    return code_EAN_produit_3;
  }
  
  public void setCode_EAN_produit_3(String code_EAN_produit_3) {
    this.code_EAN_produit_3 = code_EAN_produit_3;
  }
  
  public String getCode_fournisseur_produit_4() {
    return code_fournisseur_produit_4;
  }
  
  public void setCode_fournisseur_produit_4(String code_fournisseur_produit_4) {
    this.code_fournisseur_produit_4 = code_fournisseur_produit_4;
  }
  
  public String getCode_client_produit_5() {
    return code_client_produit_5;
  }
  
  public void setCode_client_produit_5(String code_client_produit_5) {
    this.code_client_produit_5 = code_client_produit_5;
  }
  
  public String getConditionnement_6() {
    return conditionnement_6;
  }
  
  public void setConditionnement_6(String conditionnement_6) {
    this.conditionnement_6 = conditionnement_6;
  }
  
  public String getQuantite_ligne_7() {
    return quantite_ligne_7;
  }
  
  public void setQuantite_ligne_7(String quantite_ligne_7) {
    this.quantite_ligne_7 = quantite_ligne_7;
  }
  
  public String getUniteQte_ligne_8() {
    return uniteQte_ligne_8;
  }
  
  public void setUniteQte_ligne_8(String uniteQte_ligne_8) {
    this.uniteQte_ligne_8 = uniteQte_ligne_8;
  }
  
  public String getPrixU_net_ligne_9() {
    return prixU_net_ligne_9;
  }
  
  public void setPrixU_net_ligne_9(String prixU_net_ligne_9) {
    this.prixU_net_ligne_9 = prixU_net_ligne_9;
  }
  
  public String getLibelle_produit_10() {
    return libelle_produit_10;
  }
  
  public void setLibelle_produit_10(String libelle_produit_10) {
    this.libelle_produit_10 = libelle_produit_10;
  }
  
  public String getPrixU_brut_ligne_20() {
    return prixU_brut_ligne_20;
  }
  
  public void setPrixU_brut_ligne_20(String prixU_brut_ligne_20) {
    this.prixU_brut_ligne_20 = prixU_brut_ligne_20;
  }
  
  public String getCodeArticleSN() {
    return codeArticleSN;
  }
  
  public void setCodeArticleSN(String codeArticleSN) {
    this.codeArticleSN = codeArticleSN;
  }
  
  public int getNbChampsObligatoires() {
    return nbChampsObligatoires;
  }
  
  public void setNbChampsObligatoires(int nbChampsObligatoires) {
    this.nbChampsObligatoires = nbChampsObligatoires;
  }
  
  public String getMontantHT() {
    return montantHT;
  }
  
  public void setMontantHT(String montantHT) {
    this.montantHT = montantHT;
  }
  
  public String getCodeTVA() {
    return codeTVA;
  }
  
  public void setCodeTVA(String codeTVA) {
    this.codeTVA = codeTVA;
  }
  
  public String getTauxTVA() {
    return tauxTVA;
  }
  
  public void setTauxTVA(String tauxTVA) {
    this.tauxTVA = tauxTVA;
  }
  
}
