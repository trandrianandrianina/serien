/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx004.v3;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonClientWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.ListeFluxBaseV3;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe permet de stocker les flux client V3 dans une liste afin de générer un fichier JSON.
 */
public class ListeFluxClientV3 extends ListeFluxBaseV3<JsonClientWebV3> {
  // Variables
  private List<JsonClientWebV3> listeFluxClient = new ArrayList<JsonClientWebV3>();
  
  /**
   * Constructeur.
   */
  public ListeFluxClientV3(EnumTypeFlux enumTypeFlux, String pPrefixeNomFichier) {
    super(enumTypeFlux, pPrefixeNomFichier);
    setListeFlux(listeFluxClient);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void transfererVersServeur() {
    transfererVersServeur(this);
  }
  
  /**
   * Contrôle que le code de l'élément à insérer n'existe pas déjà dans la liste (doublon).
   * Retourne l'indice du doublon sinon -1.
   */
  @Override
  protected int controlerExistenceElement(JsonClientWebV3 pFluxJson) {
    if (Constantes.normerTexte(pFluxJson.getIdClient()).isEmpty()) {
      Trace.alerte("Le contrôle du doublon avant insertion dans la liste des clients web est impossible car le champ Idclient est vide.");
      return -1;
    }
    for (int indice = 0; indice < listeFluxClient.size(); indice++) {
      if (listeFluxClient.get(indice).getIdClient().equals(pFluxJson.getIdClient())) {
        return indice;
      }
    }
    return -1;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la liste de flux client.
   */
  public List<JsonClientWebV3> getListeFluxClient() {
    return listeFluxClient;
  }
  
  /**
   * Initialise la liste de flux client.
   */
  public void setListeFluxClient(List<JsonClientWebV3> pListeFluxClient) {
    listeFluxClient = pListeFluxClient;
  }
  
}
