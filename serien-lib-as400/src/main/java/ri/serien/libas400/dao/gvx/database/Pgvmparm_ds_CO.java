/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_CO pour les CO
 */
public class Pgvmparm_ds_CO extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "CONCG"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "COLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CONSA"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "CODGR"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "COSAN"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "COLI1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "COLI2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "COCOT1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "COCOS1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "COCOT2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "COCOS2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "COCOL"));
    
    length = 300;
  }
}
