/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.database.field.Field;
import ri.serien.libas400.database.field.FieldAlpha;
import ri.serien.libas400.database.field.FieldDecimal;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;

/**
 * Permet la récupération du stock pour un article
 */
public class GestionStock {
  // Constantes
  private static final int LG_DATASTRUCT = 57;
  
  private static final int LG_ETB = 3;
  private static final int LG_CODEART = 20;
  private static final int LG_MAG1 = 2;
  private static final int LG_MAG2 = 2;
  private static final int LG_STOCK1 = 11;
  private static final int LG_STOCK1_DEC = 3;
  private static final int LG_STOCK2 = 11;
  private static final int LG_STOCK2_DEC = 3;
  
  // Variables spécifiques du programme RPG
  private char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0' };
  private FieldAlpha etablissement = new FieldAlpha(LG_ETB);
  private FieldAlpha codeArticle = new FieldAlpha(LG_CODEART);
  private FieldAlpha codeMag1 = new FieldAlpha(LG_MAG1);
  private FieldAlpha codeMag2 = new FieldAlpha(LG_MAG2);
  private FieldDecimal stock1 = new FieldDecimal(new BigDecimal(1), LG_STOCK1, LG_STOCK1_DEC);
  private FieldDecimal stock2 = new FieldDecimal(new BigDecimal(1), LG_STOCK2, LG_STOCK2_DEC);
  
  // Varibles
  private boolean init = false;
  private SystemeManager system = null;
  private ProgramParameter[] parameterList = new ProgramParameter[1];
  private AS400Text ppar1 = new AS400Text(LG_DATASTRUCT);
  private char lettre = 'W';
  private String curlib = null;
  private ContexteProgrammeRPG rpg = null;
  private StringBuilder sb = new StringBuilder();
  
  /**
   * Constructeur
   * @param sys
   * @param let
   * @param curlib
   */
  public GestionStock(SystemeManager sys, char let, String curlib) {
    system = sys;
    setLettre(let);
    setCurlib(curlib);
  }
  
  /**
   * Initialise l'environnment d'execution du programme RPG
   * @return
   */
  public boolean init() {
    if (init) {
      return true;
    }
    
    parameterList[0] = new ProgramParameter(LG_DATASTRUCT);
    // rpg = new CallProgram(system.getSystem(), "/QSYS.LIB/"+lettre+"GVMX.LIB/SGVX13WEB.PGM", null);
    rpg = new ContexteProgrammeRPG(system.getSystem(), curlib);
    init = rpg.initialiserLettreEnvironnement(lettre);
    if (!init) {
      return false;
    }
    rpg.ajouterBibliotheque(curlib, true);
    rpg.ajouterBibliotheque(ParametresFluxBibli.getBibliothequeEnvironnement().getNom(), false);
    rpg.ajouterBibliotheque(lettre + "EXPAS", false);
    rpg.ajouterBibliotheque(lettre + "GVMAS", false);
    init = rpg.ajouterBibliotheque(lettre + "GVMX", false);
    return init;
  }
  
  /**
   * Effectue la recherche du stock
   * @param valEtb
   * @param valArt
   * @param valMag1
   * @param valMag2
   * @param valStock1
   * @param valStock2
   * @return
   */
  public boolean execute(String valEtb, String valArt, String valMag1, String valMag2, BigDecimal valStock1, BigDecimal valStock2) {
    setValueEtablissement(valEtb);
    setValueCodeArticle(valArt);
    setValueMagasin1(valMag1);
    setValueMagasin2(valMag2);
    setValueStock1(valStock1);
    setValueStock2(valStock2);
    return execute();
  }
  
  /**
   * Effectue la recherche du stock
   * @return
   */
  private boolean execute() {
    if (rpg == null) {
      return false;
    }
    
    // Construction du paramètre AS400 PPAR1
    sb.setLength(0);
    sb.append(getIndicateurs()).append(etablissement.toFormattedString()).append(codeArticle.toFormattedString())
        .append(codeMag1.toFormattedString()).append(codeMag2.toFormattedString()).append(stock1.toFormattedString())
        .append(stock2.toFormattedString());
    try {
      parameterList[0].setInputData(ppar1.toBytes(sb.toString()));
      // Appel du programme RPG
      Bibliotheque bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(lettre + "GVMX"), EnumTypeBibliotheque.PROGRAMMES_SERIEN);
      if (rpg.executerProgramme("SGVX13W", bibliotheque, parameterList)) {
        // Récupération de la variable XPPVC de la DS PPAR1 (voir SGVMCCA1)
        String valeur = (String) ppar1.toObject(parameterList[0].getOutputData());
        if (valeur != null) {
          try {
            // TODO Attention des fois il y a un le symbole "é" à traiter dans ce substring -> valeur négative C'est
            // tout nouveau ça vient de sortir
            stock1.setValue(new BigDecimal(valeur.substring(35, 46)));
          }
          catch (Exception e) {
            stock1.setValue(new BigDecimal(0));
            e.printStackTrace();
          }
          try {
            // TODO Attention des fois il y a un le symbole "é" à traiter dans ce substring -> valeur négative C'est
            // tout nouveau ça vient de sortir
            stock2.setValue(new BigDecimal(valeur.substring(46, 57)));
          }
          catch (Exception e) {
            stock2.setValue(new BigDecimal(0));
            e.printStackTrace();
          }
        }
        return true;
      }
    }
    catch (PropertyVetoException e) {
    }
    return false;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Formate une variable de type String (avec cadrage à droite, notamment pour l'établissement)
   * @param valeur
   * @param lg
   * @return
   */
  private String formateVar(String valeur, int lg) {
    if (valeur == null) {
      return String.format("%" + lg + "." + lg + "s", "");
    }
    else {
      return String.format("%" + lg + "." + lg + "s", valeur);
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le lettre
   */
  public char getLettre() {
    return lettre;
  }
  
  /**
   * @param lettre le lettre à définir
   */
  public void setLettre(char lettre) {
    if (lettre != ' ') {
      this.lettre = lettre;
    }
  }
  
  /**
   * @return le curlib
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib le curlib à définir
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  /**
   * @return le indicateurs
   */
  public char[] getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * @param indicateurs le indicateurs à définir
   */
  public void setIndicateurs(char[] indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  /**
   * Retourne le champ Etablissement
   * @return le etablissement
   */
  public Field<String> getEtablissement() {
    return etablissement;
  }
  
  /**
   * Initialise la valeur de l'établissement
   * @param etablissement le etablissement à définir
   */
  public void setValueEtablissement(String etablissement) {
    this.etablissement.setValue(formateVar(etablissement, LG_ETB));
  }
  
  /**
   * Retourne le champ code Article
   * @return le codeArticle
   */
  public Field<String> getCodeArticle() {
    return codeArticle;
  }
  
  /**
   * Initialise la valeur du code Article
   * @param codMag1 le codMag1 à définir
   */
  public void setValueCodeArticle(String codeArticle) {
    this.codeArticle.setValue(codeArticle);
  }
  
  /**
   * Retourne le champ code magasin 1
   * @return le codMag1
   */
  public Field<String> getMagasin1() {
    return codeMag1;
  }
  
  /**
   * Initialise la valeur du magasin 2
   * @param codMag2 le codMag2 à définir
   */
  public void setValueMagasin1(String codeMag1) {
    this.codeMag1.setValue(codeMag1);
  }
  
  /**
   * Retourne le champ code magasin 2
   * @return le codMag2
   */
  public Field<String> getMagasin2() {
    return codeMag2;
  }
  
  /**
   * Initialise la valeur du code Article
   * @param codeArticle le codeArticle à définir
   */
  public void setValueMagasin2(String codeMag2) {
    this.codeMag2.setValue(codeMag2);
  }
  
  /**
   * Retourne le champ valStock1
   * @return le stock 1
   */
  public Field<BigDecimal> getStock1() {
    return stock1;
  }
  
  /**
   * Initialise la valeur de la quantité
   * @param stock1 le stock1 à définir
   */
  public void setValueStock1(BigDecimal stock1) {
    this.stock1.setValue(stock1);
  }
  
  /**
   * Retourne le champ valStock1
   * @return le stock 2
   */
  public Field<BigDecimal> getStock2() {
    return stock2;
  }
  
  /**
   * Initialise la valeur de la quantité
   * @param stock2 le stock2 à définir
   */
  public void setValueStock2(BigDecimal stock2) {
    this.stock2.setValue(stock2);
  }
}
