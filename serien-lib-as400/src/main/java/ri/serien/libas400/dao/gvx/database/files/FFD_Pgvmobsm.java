/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

public abstract class FFD_Pgvmobsm extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_OBTOP = 1;
  public static final int DECIMAL_OBTOP = 0;
  public static final int SIZE_OBCOD = 1;
  public static final int SIZE_OBETB = 3;
  public static final int SIZE_OBIND = 30;
  public static final int SIZE_OBSUF = 1;
  public static final int DECIMAL_OBSUF = 0;
  public static final int SIZE_OBZON = 120;
  
  // Variables fichiers
  protected int OBTOP = 0; //
  protected char OBCOD = ' '; //
  protected String OBETB = null; //
  protected String OBIND = null; //
  protected int OBSUF = 0; //
  protected String OBZON = null; //
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public FFD_Pgvmobsm(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    OBTOP = 0;
    OBCOD = ' ';
    OBETB = null;
    OBIND = null;
    OBSUF = 0;
    OBZON = null;
  }
  
  /**
   * @return le oBTOP
   */
  public int getOBTOP() {
    return OBTOP;
  }
  
  /**
   * @param oBTOP le oBTOP à définir
   */
  public void setOBTOP(int oBTOP) {
    OBTOP = oBTOP;
  }
  
  /**
   * @return le oBCOD
   */
  public char getOBCOD() {
    return OBCOD;
  }
  
  /**
   * @param oBCOD le oBCOD à définir
   */
  public void setOBCOD(char oBCOD) {
    OBCOD = oBCOD;
  }
  
  /**
   * @return le oBETB
   */
  public String getOBETB() {
    return OBETB;
  }
  
  /**
   * @param oBETB le oBETB à définir
   */
  public void setOBETB(String oBETB) {
    OBETB = oBETB;
  }
  
  /**
   * @return le oBIND
   */
  public String getOBIND() {
    return OBIND;
  }
  
  /**
   * @param oBIND le oBIND à définir
   */
  public void setOBIND(String oBIND) {
    OBIND = oBIND;
  }
  
  /**
   * @return le oBSUF
   */
  public int getOBSUF() {
    return OBSUF;
  }
  
  /**
   * @param oBSUF le oBSUF à définir
   */
  public void setOBSUF(int oBSUF) {
    OBSUF = oBSUF;
  }
  
  /**
   * @return le oBZON
   */
  public String getOBZON() {
    return OBZON;
  }
  
  /**
   * @param oBZON le oBZON à définir
   */
  public void setOBZON(String oBZON) {
    OBZON = oBZON;
  }
}
