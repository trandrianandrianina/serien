/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.validation;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.job.SurveillanceJob;
import ri.serien.libcommun.gescom.achat.document.EnumOptionEdition;
import ri.serien.libcommun.gescom.achat.document.EnumOptionValidation;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

public class RpgValiderDocumentAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGAM0017";
  
  /**
   * Appel du programme RPG qui va valider un document.
   */
  public void validerDocument(SystemeManager pSysteme, IdDocumentAchat pIdDocument, Date pDateTraitement,
      EnumOptionEdition pOptionEdition) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pIdDocument == null) {
      throw new MessageErreurException("Le paramètre pIdDocument du service est à null.");
    }
    if (pOptionEdition == null) {
      throw new MessageErreurException("L'option d'édition est invalide.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0017i entree = new Svgam0017i();
    Svgam0017o sortie = new Svgam0017o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    Trace.info("idDocument=" + pIdDocument);
    entree.setPietb(pIdDocument.getCodeEtablissement());
    entree.setPicod(pIdDocument.getCodeEntete().getCode());
    entree.setPinum(pIdDocument.getNumero());
    entree.setPisuf(pIdDocument.getSuffixe());
    entree.setPidat(pDateTraitement);
    entree.setPiopt(EnumOptionValidation.VALIDATION.getCode());
    entree.setPiedt(pOptionEdition.getCode());
    // entree.setPinom(pDocument.getIdFournisseur().get;
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    SurveillanceJob surveillanceJob = new SurveillanceJob(EnvironnementExecution.getGvmas());
    surveillanceJob.demarrer();
    rpg.setSurveillanceJob(surveillanceJob);
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va annuler un document.
   */
  public void annulerDocument(SystemeManager pSysteme, IdDocumentAchat pIdDocument, Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pIdDocument == null) {
      throw new MessageErreurException("Le paramètre pIdDocument du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0017i entree = new Svgam0017i();
    Svgam0017o sortie = new Svgam0017o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    Trace.info("idDocument=" + pIdDocument);
    entree.setPietb(pIdDocument.getCodeEtablissement());
    entree.setPicod(pIdDocument.getCodeEntete().getCode());
    entree.setPinum(pIdDocument.getNumero());
    entree.setPisuf(pIdDocument.getSuffixe());
    entree.setPidat(pDateTraitement);
    entree.setPiopt(EnumOptionValidation.ANNULATION.getCode());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va mettre en attente un document.
   */
  public void mettreEnAttenteDocument(SystemeManager pSysteme, IdDocumentAchat pIdDocument, Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pIdDocument == null) {
      throw new MessageErreurException("Le paramètre pIdDocument du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0017i entree = new Svgam0017i();
    Svgam0017o sortie = new Svgam0017o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    Trace.info("idDocument=" + pIdDocument);
    entree.setPietb(pIdDocument.getCodeEtablissement());
    entree.setPicod(pIdDocument.getCodeEntete().getCode());
    entree.setPinum(pIdDocument.getNumero());
    entree.setPisuf(pIdDocument.getSuffixe());
    entree.setPidat(pDateTraitement);
    entree.setPiopt(EnumOptionValidation.ATTENTE.getCode());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      controlerErreur();
    }
  }
  
}
