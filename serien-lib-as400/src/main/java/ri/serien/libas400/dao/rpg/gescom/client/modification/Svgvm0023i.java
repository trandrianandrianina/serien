/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.client.modification;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0023i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PICLI = 6;
  public static final int DECIMAL_PICLI = 0;
  public static final int SIZE_PILIV = 3;
  public static final int DECIMAL_PILIV = 0;
  public static final int SIZE_PINOM = 30;
  public static final int SIZE_PICPL = 30;
  public static final int SIZE_PIRUE = 30;
  public static final int SIZE_PILOC = 30;
  public static final int SIZE_PICDP = 5;
  public static final int SIZE_PIFIL0 = 1;
  public static final int SIZE_PIVIL = 24;
  public static final int SIZE_PIPAY = 26;
  public static final int SIZE_PIFIL = 1;
  public static final int SIZE_PICOP = 3;
  public static final int SIZE_PICDP1 = 5;
  public static final int DECIMAL_PICDP1 = 0;
  public static final int SIZE_PITOP1 = 2;
  public static final int SIZE_PITOP2 = 2;
  public static final int SIZE_PITOP3 = 2;
  public static final int SIZE_PITOP4 = 2;
  public static final int SIZE_PITOP5 = 2;
  public static final int SIZE_PICAT = 3;
  public static final int SIZE_PICLK = 15;
  public static final int SIZE_PIAPE = 4;
  public static final int SIZE_PITEL = 20;
  public static final int SIZE_PIFAX = 20;
  public static final int SIZE_PITLX = 7;
  public static final int SIZE_PILAN = 6;
  public static final int SIZE_PIOBS = 18;
  public static final int SIZE_PIREP = 2;
  public static final int SIZE_PIREP2 = 2;
  public static final int SIZE_PITRP = 1;
  public static final int SIZE_PIGEO = 5;
  public static final int SIZE_PIMEX = 2;
  public static final int SIZE_PICTR = 2;
  public static final int SIZE_PIMAG = 2;
  public static final int SIZE_PIFRP = 7;
  public static final int DECIMAL_PIFRP = 0;
  public static final int SIZE_PITFA = 1;
  public static final int SIZE_PITTC = 1;
  public static final int SIZE_PIDEV = 3;
  public static final int SIZE_PICLFP = 6;
  public static final int DECIMAL_PICLFP = 0;
  public static final int SIZE_PICLFS = 3;
  public static final int DECIMAL_PICLFS = 0;
  public static final int SIZE_PICLP = 6;
  public static final int DECIMAL_PICLP = 0;
  public static final int SIZE_PIAFA = 7;
  public static final int SIZE_PIESC = 3;
  public static final int DECIMAL_PIESC = 2;
  public static final int SIZE_PITAR = 1;
  public static final int DECIMAL_PITAR = 0;
  public static final int SIZE_PITA1 = 1;
  public static final int DECIMAL_PITA1 = 0;
  public static final int SIZE_PIREM1 = 4;
  public static final int DECIMAL_PIREM1 = 2;
  public static final int SIZE_PIREM2 = 4;
  public static final int DECIMAL_PIREM2 = 2;
  public static final int SIZE_PIREM3 = 4;
  public static final int DECIMAL_PIREM3 = 2;
  public static final int SIZE_PIRP1 = 4;
  public static final int DECIMAL_PIRP1 = 2;
  public static final int SIZE_PIRP2 = 4;
  public static final int DECIMAL_PIRP2 = 2;
  public static final int SIZE_PIRP3 = 4;
  public static final int DECIMAL_PIRP3 = 2;
  public static final int SIZE_PICNV = 6;
  public static final int SIZE_PIPFA = 1;
  public static final int SIZE_PIPRL = 1;
  public static final int SIZE_PIRLV = 1;
  public static final int SIZE_PIRBF = 1;
  public static final int DECIMAL_PIRBF = 0;
  public static final int SIZE_PIRGL = 2;
  public static final int SIZE_PIECH = 2;
  public static final int SIZE_PINCG = 6;
  public static final int DECIMAL_PINCG = 0;
  public static final int SIZE_PINCA = 6;
  public static final int DECIMAL_PINCA = 0;
  public static final int SIZE_PIACT = 4;
  public static final int SIZE_PIPLF = 7;
  public static final int DECIMAL_PIPLF = 0;
  public static final int SIZE_PIPLF2 = 7;
  public static final int DECIMAL_PIPLF2 = 0;
  public static final int SIZE_PITNS = 1;
  public static final int DECIMAL_PITNS = 0;
  public static final int SIZE_PIATT = 18;
  public static final int SIZE_PINOT = 2;
  public static final int SIZE_PIDDV = 7;
  public static final int DECIMAL_PIDDV = 0;
  public static final int SIZE_PIDVE = 7;
  public static final int DECIMAL_PIDVE = 0;
  public static final int SIZE_PISRN = 9;
  public static final int SIZE_PISRT = 5;
  public static final int SIZE_PIFIL1 = 1;
  public static final int SIZE_PICEE = 3;
  public static final int SIZE_PIVAE = 7;
  public static final int DECIMAL_PIVAE = 0;
  public static final int SIZE_PICDE = 9;
  public static final int DECIMAL_PICDE = 2;
  public static final int SIZE_PIEXP = 9;
  public static final int DECIMAL_PIEXP = 2;
  public static final int SIZE_PIFAC = 9;
  public static final int DECIMAL_PIFAC = 2;
  public static final int SIZE_PIPCO = 7;
  public static final int DECIMAL_PIPCO = 0;
  public static final int SIZE_PICGM = 1;
  public static final int DECIMAL_PICGM = 0;
  public static final int SIZE_PIMAJ = 1;
  public static final int DECIMAL_PIMAJ = 0;
  public static final int SIZE_PINEX1 = 1;
  public static final int DECIMAL_PINEX1 = 0;
  public static final int SIZE_PINEX2 = 1;
  public static final int DECIMAL_PINEX2 = 0;
  public static final int SIZE_PINEX3 = 1;
  public static final int DECIMAL_PINEX3 = 0;
  public static final int SIZE_PINLA = 4;
  public static final int SIZE_PIRST = 6;
  public static final int SIZE_PIIN1 = 1;
  public static final int SIZE_PIIN2 = 1;
  public static final int SIZE_PIIN3 = 1;
  public static final int SIZE_PIIN4 = 1;
  public static final int SIZE_PIIN5 = 1;
  public static final int SIZE_PIIN6 = 1;
  public static final int SIZE_PIIN7 = 1;
  public static final int SIZE_PIIN8 = 1;
  public static final int SIZE_PIIN9 = 1;
  public static final int SIZE_PIIN10 = 1;
  public static final int SIZE_PIIN11 = 1;
  public static final int SIZE_PIIN12 = 1;
  public static final int SIZE_PIIN13 = 1;
  public static final int SIZE_PIIN14 = 1;
  public static final int SIZE_PIIN15 = 1;
  public static final int SIZE_PIADH = 15;
  public static final int SIZE_PICNC = 4;
  public static final int SIZE_PICC1 = 6;
  public static final int DECIMAL_PICC1 = 0;
  public static final int SIZE_PICC2 = 6;
  public static final int DECIMAL_PICC2 = 0;
  public static final int SIZE_PICC3 = 6;
  public static final int DECIMAL_PICC3 = 0;
  public static final int SIZE_PIFIR = 5;
  public static final int SIZE_PITRA = 6;
  public static final int DECIMAL_PITRA = 0;
  public static final int SIZE_PICNR = 5;
  public static final int SIZE_PICNP = 5;
  public static final int SIZE_PICNB = 5;
  public static final int SIZE_PIDPL = 7;
  public static final int DECIMAL_PIDPL = 0;
  public static final int SIZE_PIIN16 = 1;
  public static final int SIZE_PIIN17 = 1;
  public static final int SIZE_PICRT = 5;
  public static final int SIZE_PIOTO = 5;
  public static final int DECIMAL_PIOTO = 0;
  public static final int SIZE_PICAN = 3;
  public static final int SIZE_PIPRN = 30;
  public static final int SIZE_PIREC = 1;
  public static final int SIZE_PIFRC = 1;
  public static final int SIZE_PIMTC = 1;
  public static final int SIZE_PINIP = 2;
  public static final int SIZE_PINIK = 2;
  public static final int SIZE_PICL1 = 1;
  public static final int SIZE_PICL2 = 2;
  public static final int SIZE_PICL3 = 2;
  public static final int SIZE_PICLA = 15;
  public static final int SIZE_PIIN18 = 1;
  public static final int SIZE_PIIN19 = 1;
  public static final int SIZE_PIIN20 = 1;
  public static final int SIZE_PIIN21 = 1;
  public static final int SIZE_PIIN22 = 1;
  public static final int SIZE_PIIN23 = 1;
  public static final int SIZE_PIIN24 = 1;
  public static final int SIZE_PIIN25 = 1;
  public static final int SIZE_PIIN26 = 1;
  public static final int SIZE_PIIN27 = 1;
  public static final int SIZE_PIAPEN = 5;
  public static final int SIZE_PIPLF3 = 7;
  public static final int DECIMAL_PIPLF3 = 0;
  public static final int SIZE_PIDPL3 = 7;
  public static final int DECIMAL_PIDPL3 = 0;
  public static final int SIZE_PIIDAS = 15;
  public static final int SIZE_PICAS = 5;
  public static final int SIZE_PIDVP3 = 7;
  public static final int DECIMAL_PIDVP3 = 0;
  public static final int SIZE_PIDAT1 = 7;
  public static final int DECIMAL_PIDAT1 = 0;
  public static final int SIZE_PIDAT2 = 7;
  public static final int DECIMAL_PIDAT2 = 0;
  public static final int SIZE_PIDAT3 = 7;
  public static final int DECIMAL_PIDAT3 = 0;
  public static final int SIZE_PIFFP1 = 4;
  public static final int DECIMAL_PIFFP1 = 2;
  public static final int SIZE_PIFFP2 = 4;
  public static final int DECIMAL_PIFFP2 = 2;
  public static final int SIZE_PIFFP3 = 4;
  public static final int DECIMAL_PIFFP3 = 2;
  public static final int SIZE_PIIN28 = 1;
  public static final int SIZE_PIIN29 = 1;
  public static final int SIZE_PIIN30 = 1;
  public static final int SIZE_PICIV = 3;
  public static final int SIZE_PINOMC = 30;
  public static final int SIZE_PIPRE = 30;
  public static final int SIZE_PINET = 120;
  public static final int SIZE_PINET2 = 120;
  public static final int SIZE_PITELC = 30;
  public static final int SIZE_PIFAXC = 20;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 1145;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PICLI = 2;
  public static final int VAR_PILIV = 3;
  public static final int VAR_PINOM = 4;
  public static final int VAR_PICPL = 5;
  public static final int VAR_PIRUE = 6;
  public static final int VAR_PILOC = 7;
  public static final int VAR_PICDP = 8;
  public static final int VAR_PIFIL0 = 9;
  public static final int VAR_PIVIL = 10;
  public static final int VAR_PIPAY = 11;
  public static final int VAR_PIFIL = 12;
  public static final int VAR_PICOP = 13;
  public static final int VAR_PICDP1 = 14;
  public static final int VAR_PITOP1 = 15;
  public static final int VAR_PITOP2 = 16;
  public static final int VAR_PITOP3 = 17;
  public static final int VAR_PITOP4 = 18;
  public static final int VAR_PITOP5 = 19;
  public static final int VAR_PICAT = 20;
  public static final int VAR_PICLK = 21;
  public static final int VAR_PIAPE = 22;
  public static final int VAR_PITEL = 23;
  public static final int VAR_PIFAX = 24;
  public static final int VAR_PITLX = 25;
  public static final int VAR_PILAN = 26;
  public static final int VAR_PIOBS = 27;
  public static final int VAR_PIREP = 28;
  public static final int VAR_PIREP2 = 29;
  public static final int VAR_PITRP = 30;
  public static final int VAR_PIGEO = 31;
  public static final int VAR_PIMEX = 32;
  public static final int VAR_PICTR = 33;
  public static final int VAR_PIMAG = 34;
  public static final int VAR_PIFRP = 35;
  public static final int VAR_PITFA = 36;
  public static final int VAR_PITTC = 37;
  public static final int VAR_PIDEV = 38;
  public static final int VAR_PICLFP = 39;
  public static final int VAR_PICLFS = 40;
  public static final int VAR_PICLP = 41;
  public static final int VAR_PIAFA = 42;
  public static final int VAR_PIESC = 43;
  public static final int VAR_PITAR = 44;
  public static final int VAR_PITA1 = 45;
  public static final int VAR_PIREM1 = 46;
  public static final int VAR_PIREM2 = 47;
  public static final int VAR_PIREM3 = 48;
  public static final int VAR_PIRP1 = 49;
  public static final int VAR_PIRP2 = 50;
  public static final int VAR_PIRP3 = 51;
  public static final int VAR_PICNV = 52;
  public static final int VAR_PIPFA = 53;
  public static final int VAR_PIPRL = 54;
  public static final int VAR_PIRLV = 55;
  public static final int VAR_PIRBF = 56;
  public static final int VAR_PIRGL = 57;
  public static final int VAR_PIECH = 58;
  public static final int VAR_PINCG = 59;
  public static final int VAR_PINCA = 60;
  public static final int VAR_PIACT = 61;
  public static final int VAR_PIPLF = 62;
  public static final int VAR_PIPLF2 = 63;
  public static final int VAR_PITNS = 64;
  public static final int VAR_PIATT = 65;
  public static final int VAR_PINOT = 66;
  public static final int VAR_PIDDV = 67;
  public static final int VAR_PIDVE = 68;
  public static final int VAR_PISRN = 69;
  public static final int VAR_PISRT = 70;
  public static final int VAR_PIFIL1 = 71;
  public static final int VAR_PICEE = 72;
  public static final int VAR_PIVAE = 73;
  public static final int VAR_PICDE = 74;
  public static final int VAR_PIEXP = 75;
  public static final int VAR_PIFAC = 76;
  public static final int VAR_PIPCO = 77;
  public static final int VAR_PICGM = 78;
  public static final int VAR_PIMAJ = 79;
  public static final int VAR_PINEX1 = 80;
  public static final int VAR_PINEX2 = 81;
  public static final int VAR_PINEX3 = 82;
  public static final int VAR_PINLA = 83;
  public static final int VAR_PIRST = 84;
  public static final int VAR_PIIN1 = 85;
  public static final int VAR_PIIN2 = 86;
  public static final int VAR_PIIN3 = 87;
  public static final int VAR_PIIN4 = 88;
  public static final int VAR_PIIN5 = 89;
  public static final int VAR_PIIN6 = 90;
  public static final int VAR_PIIN7 = 91;
  public static final int VAR_PIIN8 = 92;
  public static final int VAR_PIIN9 = 93;
  public static final int VAR_PIIN10 = 94;
  public static final int VAR_PIIN11 = 95;
  public static final int VAR_PIIN12 = 96;
  public static final int VAR_PIIN13 = 97;
  public static final int VAR_PIIN14 = 98;
  public static final int VAR_PIIN15 = 99;
  public static final int VAR_PIADH = 100;
  public static final int VAR_PICNC = 101;
  public static final int VAR_PICC1 = 102;
  public static final int VAR_PICC2 = 103;
  public static final int VAR_PICC3 = 104;
  public static final int VAR_PIFIR = 105;
  public static final int VAR_PITRA = 106;
  public static final int VAR_PICNR = 107;
  public static final int VAR_PICNP = 108;
  public static final int VAR_PICNB = 109;
  public static final int VAR_PIDPL = 110;
  public static final int VAR_PIIN16 = 111;
  public static final int VAR_PIIN17 = 112;
  public static final int VAR_PICRT = 113;
  public static final int VAR_PIOTO = 114;
  public static final int VAR_PICAN = 115;
  public static final int VAR_PIPRN = 116;
  public static final int VAR_PIREC = 117;
  public static final int VAR_PIFRC = 118;
  public static final int VAR_PIMTC = 119;
  public static final int VAR_PINIP = 120;
  public static final int VAR_PINIK = 121;
  public static final int VAR_PICL1 = 122;
  public static final int VAR_PICL2 = 123;
  public static final int VAR_PICL3 = 124;
  public static final int VAR_PICLA = 125;
  public static final int VAR_PIIN18 = 126;
  public static final int VAR_PIIN19 = 127;
  public static final int VAR_PIIN20 = 128;
  public static final int VAR_PIIN21 = 129;
  public static final int VAR_PIIN22 = 130;
  public static final int VAR_PIIN23 = 131;
  public static final int VAR_PIIN24 = 132;
  public static final int VAR_PIIN25 = 133;
  public static final int VAR_PIIN26 = 134;
  public static final int VAR_PIIN27 = 135;
  public static final int VAR_PIAPEN = 136;
  public static final int VAR_PIPLF3 = 137;
  public static final int VAR_PIDPL3 = 138;
  public static final int VAR_PIIDAS = 139;
  public static final int VAR_PICAS = 140;
  public static final int VAR_PIDVP3 = 141;
  public static final int VAR_PIDAT1 = 142;
  public static final int VAR_PIDAT2 = 143;
  public static final int VAR_PIDAT3 = 144;
  public static final int VAR_PIFFP1 = 145;
  public static final int VAR_PIFFP2 = 146;
  public static final int VAR_PIFFP3 = 147;
  public static final int VAR_PIIN28 = 148;
  public static final int VAR_PIIN29 = 149;
  public static final int VAR_PIIN30 = 150;
  public static final int VAR_PICIV = 151;
  public static final int VAR_PINOMC = 152;
  public static final int VAR_PIPRE = 153;
  public static final int VAR_PINET = 154;
  public static final int VAR_PINET2 = 155;
  public static final int VAR_PITELC = 156;
  public static final int VAR_PIFAXC = 157;
  public static final int VAR_PIARR = 158;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private BigDecimal picli = BigDecimal.ZERO; // Numéro client
  private BigDecimal piliv = BigDecimal.ZERO; // Suffixe client
  private String pinom = ""; // Nom ou raison sociale *
  private String picpl = ""; // Complément de nom *
  private String pirue = ""; // Adresse - rue *
  private String piloc = ""; // Adresse - localité *
  private String picdp = ""; // Code postal *
  private String pifil0 = ""; // filler
  private String pivil = ""; // Ville *
  private String pipay = ""; // Adresse - pays *
  private String pifil = ""; // filler
  private String picop = ""; // Code pays
  private BigDecimal picdp1 = BigDecimal.ZERO; // Code Postal
  private String pitop1 = ""; // Critères d'analyse 1
  private String pitop2 = ""; // Critères d'analyse 2
  private String pitop3 = ""; // Critères d'analyse 3
  private String pitop4 = ""; // Critères d'analyse 4
  private String pitop5 = ""; // Critères d'analyse 5
  private String picat = ""; // Code catégorie (CC) *
  private String piclk = ""; // Clé de classement alpha
  private String piape = ""; // Code APE
  private String pitel = ""; // Téléphone *
  private String pifax = ""; // Télécopie *
  private String pitlx = ""; // Télex
  private String pilan = ""; // Code Langue
  private String piobs = ""; // Observations *
  private String pirep = ""; // Code représentant *
  private String pirep2 = ""; // Code représentant 2 *
  private String pitrp = ""; // Type commissionnement rep.
  private String pigeo = ""; // Zone géographique *
  private String pimex = ""; // Mode d'expédition *
  private String pictr = ""; // Code transporteur habituel *
  private String pimag = ""; // Code magasin de rattachemen*
  private BigDecimal pifrp = BigDecimal.ZERO; // Montant franco de port
  private String pitfa = ""; // Type de facturation
  private String pittc = ""; // Facture ttc oui/non
  private String pidev = ""; // Code devise (PSEMPAR/DV)
  private BigDecimal piclfp = BigDecimal.ZERO; // Code client facturé
  private BigDecimal piclfs = BigDecimal.ZERO; // Code client facturé suffixe
  private BigDecimal piclp = BigDecimal.ZERO; // Code Client Payeur
  private String piafa = ""; // Code Affacturage
  private BigDecimal piesc = BigDecimal.ZERO; // Taux d'escompte
  private BigDecimal pitar = BigDecimal.ZERO; // Colonne tarif (0=10) *
  private BigDecimal pita1 = BigDecimal.ZERO; // 2ème colonne tarif
  private BigDecimal pirem1 = BigDecimal.ZERO; // Remise 1
  private BigDecimal pirem2 = BigDecimal.ZERO; // Remise 2
  private BigDecimal pirem3 = BigDecimal.ZERO; // Remise 3
  private BigDecimal pirp1 = BigDecimal.ZERO; // Remise Pied 1
  private BigDecimal pirp2 = BigDecimal.ZERO; // Remise Pied 2
  private BigDecimal pirp3 = BigDecimal.ZERO; // Remise Pied 3
  private String picnv = ""; // Code condition de vente
  private String pipfa = ""; // Périodicité de facturation
  private String piprl = ""; // Périodicité de relevé
  private String pirlv = ""; // Code relevé
  private BigDecimal pirbf = BigDecimal.ZERO; // Regroupement Bon sur Facture
  private String pirgl = ""; // Code règlement (RG)
  private String piech = ""; // Code échéance
  private BigDecimal pincg = BigDecimal.ZERO; // Collectif comptable
  private BigDecimal pinca = BigDecimal.ZERO; // Compte auxiliaire
  private String piact = ""; // Code affaire en cours
  private BigDecimal piplf = BigDecimal.ZERO; // Plafond encours autorisé
  private BigDecimal piplf2 = BigDecimal.ZERO; // Sur-Plafond encours autorisé
  private BigDecimal pitns = BigDecimal.ZERO; // Top Attn
  private String piatt = ""; // Texte d'attention pour cde
  private String pinot = ""; // Note CLient
  private BigDecimal piddv = BigDecimal.ZERO; // Date de dernière visite
  private BigDecimal pidve = BigDecimal.ZERO; // Date de dernière vente
  private String pisrn = ""; // Numéro SIREN
  private String pisrt = ""; // Complément SIRET
  private String pifil1 = ""; // DÉPASSEMENT ENCOURS
  private String picee = ""; // Code pays CEE
  private BigDecimal pivae = BigDecimal.ZERO; // Vente assimilé export
  private BigDecimal picde = BigDecimal.ZERO; // Encours / Commande
  private BigDecimal piexp = BigDecimal.ZERO; // Encours / Expédié
  private BigDecimal pifac = BigDecimal.ZERO; // Encours / Facturé
  private BigDecimal pipco = BigDecimal.ZERO; // Position comptable
  private BigDecimal picgm = BigDecimal.ZERO; // Zone système pour maj CGM
  private BigDecimal pimaj = BigDecimal.ZERO; // Top MAJ
  private BigDecimal pinex1 = BigDecimal.ZERO; // Nombre exemplaires Bon Hom.
  private BigDecimal pinex2 = BigDecimal.ZERO; // Nombre exemplaires Bon Exp.
  private BigDecimal pinex3 = BigDecimal.ZERO; // Nombre exemplaires facture
  private String pinla = ""; // Numéro libellé article/Edt
  private String pirst = ""; // Zone regroupement stats
  private String piin1 = ""; // Livraison partielle oui/non
  private String piin2 = ""; // Reliquats acceptés oui/non
  private String piin3 = ""; // BL chiffré oui/non
  private String piin4 = ""; // Non utilisation DGCNV
  private String piin5 = ""; // Code GBA à répercuter / bon
  private String piin6 = ""; // "C" rfa sur centrale
  private String piin7 = ""; // Non édition etq colis / BL
  private String piin8 = ""; // Edit. Fact,Bon Exp enchainé
  private String piin9 = ""; // Type client
  private String piin10 = ""; // Exclusion CNV Quantitative
  private String piin11 = ""; // Pas ETQ colis / Expédit°
  private String piin12 = ""; // Fac.Dif./ sur Cde soldée
  private String piin13 = ""; // Non Soumis à taxe addit.
  private String piin14 = ""; // Code PE particip. frais exp.
  private String piin15 = ""; // Gén.CNV si FAC avec prix net
  private String piadh = ""; // Code adhérent
  private String picnc = ""; // Code Cond. commissionnement
  private BigDecimal picc1 = BigDecimal.ZERO; // Centrale 1
  private BigDecimal picc2 = BigDecimal.ZERO; // Centrale 2
  private BigDecimal picc3 = BigDecimal.ZERO; // Centrale 3
  private String pifir = ""; // Code Firme
  private BigDecimal pitra = BigDecimal.ZERO; // Client Transitaire
  private String picnr = ""; // Code condition ristourne
  private String picnp = ""; // Code condition Promo
  private String picnb = ""; // Code remise pied de bon
  private BigDecimal pidpl = BigDecimal.ZERO; // Date Limite pour Sur-plafond
  private String piin16 = ""; // Application frais fixes (CC)
  private String piin17 = ""; // Application remise pied fact
  private String picrt = ""; // Code Regroupt.Transport
  private BigDecimal pioto = BigDecimal.ZERO; // Ordre dans la Tournée
  private String pican = ""; // Canal de vente
  private String piprn = ""; // Prénoms
  private String pirec = ""; // (R)Récence commande
  private String pifrc = ""; // (F)Fréquence commande
  private String pimtc = ""; // (M)Montant commande
  private String pinip = ""; // Code pays TVA Intracommun.
  private String pinik = ""; // Clé infor.TVA Intracommun.
  private String picl1 = ""; // Classe de client 1
  private String picl2 = ""; // Classe de client 2
  private String picl3 = ""; // Classe de client 3
  private String picla = ""; // Clé de classement 2 alpha
  private String piin18 = ""; // Client soumis à droitde prêt
  private String piin19 = ""; // Edition factures
  private String piin20 = ""; // Précommandes acceptées O/N
  private String piin21 = ""; // Nombre exemplaires avoirs
  private String piin22 = ""; // Type de vente paramètre VT
  private String piin23 = ""; // Tranche Effectif
  private String piin24 = ""; // Code PE particip. frais exp2
  private String piin25 = ""; // Code PE particip. frais exp3
  private String piin26 = ""; // Non édition remises
  private String piin27 = ""; // Non édition prix base
  private String piapen = ""; // Code APE v.2
  private BigDecimal piplf3 = BigDecimal.ZERO; // Plafond demandé
  private BigDecimal pidpl3 = BigDecimal.ZERO; // Date demande du plafond
  private String piidas = ""; // Identifiant assurance
  private String picas = ""; // Code assurance
  private BigDecimal pidvp3 = BigDecimal.ZERO; // Date validité du plafond
  private BigDecimal pidat1 = BigDecimal.ZERO; // CA Previsionnel
  private BigDecimal pidat2 = BigDecimal.ZERO; // Id. BIZY NOVA
  private BigDecimal pidat3 = BigDecimal.ZERO; // Plafond maxi en déblocage
  private BigDecimal piffp1 = BigDecimal.ZERO; // frais fact. pied 1
  private BigDecimal piffp2 = BigDecimal.ZERO; // frais fact. pied 2
  private BigDecimal piffp3 = BigDecimal.ZERO; // frais fact. pied 3
  private String piin28 = ""; // non édition numéros séries
  private String piin29 = ""; // SUSPECT = S
  private String piin30 = ""; // Réf.client/bon obligatoire
  private String piciv = ""; // Civilité (PSEMPAR/CV)
  private String pinomc = ""; // Nom contact
  private String pipre = ""; // Prénom contact
  private String pinet = ""; // Adresse Email
  private String pinet2 = ""; // Adresse Email2
  private String pitelc = ""; // Téléphone contact
  private String pifaxc = ""; // N°FAX contact
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PICLI, DECIMAL_PICLI), // Numéro client
      new AS400ZonedDecimal(SIZE_PILIV, DECIMAL_PILIV), // Suffixe client
      new AS400Text(SIZE_PINOM), // Nom ou raison sociale *
      new AS400Text(SIZE_PICPL), // Complément de nom *
      new AS400Text(SIZE_PIRUE), // Adresse - rue *
      new AS400Text(SIZE_PILOC), // Adresse - localité *
      new AS400Text(SIZE_PICDP), // Code postal *
      new AS400Text(SIZE_PIFIL0), // filler
      new AS400Text(SIZE_PIVIL), // Ville *
      new AS400Text(SIZE_PIPAY), // Adresse - pays *
      new AS400Text(SIZE_PIFIL), // filler
      new AS400Text(SIZE_PICOP), // Code pays
      new AS400ZonedDecimal(SIZE_PICDP1, DECIMAL_PICDP1), // Code Postal
      new AS400Text(SIZE_PITOP1), // Critères d'analyse 1
      new AS400Text(SIZE_PITOP2), // Critères d'analyse 2
      new AS400Text(SIZE_PITOP3), // Critères d'analyse 3
      new AS400Text(SIZE_PITOP4), // Critères d'analyse 4
      new AS400Text(SIZE_PITOP5), // Critères d'analyse 5
      new AS400Text(SIZE_PICAT), // Code catégorie (CC) *
      new AS400Text(SIZE_PICLK), // Clé de classement alpha
      new AS400Text(SIZE_PIAPE), // Code APE
      new AS400Text(SIZE_PITEL), // Téléphone *
      new AS400Text(SIZE_PIFAX), // Télécopie *
      new AS400Text(SIZE_PITLX), // Télex
      new AS400Text(SIZE_PILAN), // Code Langue
      new AS400Text(SIZE_PIOBS), // Observations *
      new AS400Text(SIZE_PIREP), // Code représentant *
      new AS400Text(SIZE_PIREP2), // Code représentant 2 *
      new AS400Text(SIZE_PITRP), // Type commissionnement rep.
      new AS400Text(SIZE_PIGEO), // Zone géographique *
      new AS400Text(SIZE_PIMEX), // Mode d'expédition *
      new AS400Text(SIZE_PICTR), // Code transporteur habituel *
      new AS400Text(SIZE_PIMAG), // Code magasin de rattachemen*
      new AS400ZonedDecimal(SIZE_PIFRP, DECIMAL_PIFRP), // Montant franco de port
      new AS400Text(SIZE_PITFA), // Type de facturation
      new AS400Text(SIZE_PITTC), // Facture ttc oui/non
      new AS400Text(SIZE_PIDEV), // Code devise (PSEMPAR/DV)
      new AS400ZonedDecimal(SIZE_PICLFP, DECIMAL_PICLFP), // Code client facturé
      new AS400ZonedDecimal(SIZE_PICLFS, DECIMAL_PICLFS), // Code client facturé suffixe
      new AS400ZonedDecimal(SIZE_PICLP, DECIMAL_PICLP), // Code Client Payeur
      new AS400Text(SIZE_PIAFA), // Code Affacturage
      new AS400PackedDecimal(SIZE_PIESC, DECIMAL_PIESC), // Taux d'escompte
      new AS400ZonedDecimal(SIZE_PITAR, DECIMAL_PITAR), // Colonne tarif (0=10) *
      new AS400ZonedDecimal(SIZE_PITA1, DECIMAL_PITA1), // 2ème colonne tarif
      new AS400ZonedDecimal(SIZE_PIREM1, DECIMAL_PIREM1), // Remise 1
      new AS400ZonedDecimal(SIZE_PIREM2, DECIMAL_PIREM2), // Remise 2
      new AS400ZonedDecimal(SIZE_PIREM3, DECIMAL_PIREM3), // Remise 3
      new AS400ZonedDecimal(SIZE_PIRP1, DECIMAL_PIRP1), // Remise Pied 1
      new AS400ZonedDecimal(SIZE_PIRP2, DECIMAL_PIRP2), // Remise Pied 2
      new AS400ZonedDecimal(SIZE_PIRP3, DECIMAL_PIRP3), // Remise Pied 3
      new AS400Text(SIZE_PICNV), // Code condition de vente
      new AS400Text(SIZE_PIPFA), // Périodicité de facturation
      new AS400Text(SIZE_PIPRL), // Périodicité de relevé
      new AS400Text(SIZE_PIRLV), // Code relevé
      new AS400ZonedDecimal(SIZE_PIRBF, DECIMAL_PIRBF), // Regroupement Bon sur Facture
      new AS400Text(SIZE_PIRGL), // Code règlement (RG)
      new AS400Text(SIZE_PIECH), // Code échéance
      new AS400PackedDecimal(SIZE_PINCG, DECIMAL_PINCG), // Collectif comptable
      new AS400PackedDecimal(SIZE_PINCA, DECIMAL_PINCA), // Compte auxiliaire
      new AS400Text(SIZE_PIACT), // Code affaire en cours
      new AS400PackedDecimal(SIZE_PIPLF, DECIMAL_PIPLF), // Plafond encours autorisé
      new AS400PackedDecimal(SIZE_PIPLF2, DECIMAL_PIPLF2), // Sur-Plafond encours autorisé
      new AS400ZonedDecimal(SIZE_PITNS, DECIMAL_PITNS), // Top Attn
      new AS400Text(SIZE_PIATT), // Texte d'attention pour cde
      new AS400Text(SIZE_PINOT), // Note CLient
      new AS400PackedDecimal(SIZE_PIDDV, DECIMAL_PIDDV), // Date de dernière visite
      new AS400PackedDecimal(SIZE_PIDVE, DECIMAL_PIDVE), // Date de dernière vente
      new AS400Text(SIZE_PISRN), // Numéro SIREN
      new AS400Text(SIZE_PISRT), // Complément SIRET
      new AS400Text(SIZE_PIFIL1), // DÉPASSEMENT ENCOURS
      new AS400Text(SIZE_PICEE), // Code pays CEE
      new AS400PackedDecimal(SIZE_PIVAE, DECIMAL_PIVAE), // Vente assimilé export
      new AS400PackedDecimal(SIZE_PICDE, DECIMAL_PICDE), // Encours / Commande
      new AS400PackedDecimal(SIZE_PIEXP, DECIMAL_PIEXP), // Encours / Expédié
      new AS400PackedDecimal(SIZE_PIFAC, DECIMAL_PIFAC), // Encours / Facturé
      new AS400PackedDecimal(SIZE_PIPCO, DECIMAL_PIPCO), // Position comptable
      new AS400ZonedDecimal(SIZE_PICGM, DECIMAL_PICGM), // Zone système pour maj CGM
      new AS400ZonedDecimal(SIZE_PIMAJ, DECIMAL_PIMAJ), // Top MAJ
      new AS400ZonedDecimal(SIZE_PINEX1, DECIMAL_PINEX1), // Nombre exemplaires Bon Hom.
      new AS400ZonedDecimal(SIZE_PINEX2, DECIMAL_PINEX2), // Nombre exemplaires Bon Exp.
      new AS400ZonedDecimal(SIZE_PINEX3, DECIMAL_PINEX3), // Nombre exemplaires facture
      new AS400Text(SIZE_PINLA), // Numéro libellé article/Edt
      new AS400Text(SIZE_PIRST), // Zone regroupement stats
      new AS400Text(SIZE_PIIN1), // Livraison partielle oui/non
      new AS400Text(SIZE_PIIN2), // Reliquats acceptés oui/non
      new AS400Text(SIZE_PIIN3), // BL chiffré oui/non
      new AS400Text(SIZE_PIIN4), // Non utilisation DGCNV
      new AS400Text(SIZE_PIIN5), // Code GBA à répercuter / bon
      new AS400Text(SIZE_PIIN6), // "C" rfa sur centrale
      new AS400Text(SIZE_PIIN7), // Non édition etq colis / BL
      new AS400Text(SIZE_PIIN8), // Edit. Fact,Bon Exp enchainé
      new AS400Text(SIZE_PIIN9), // Type client
      new AS400Text(SIZE_PIIN10), // Exclusion CNV Quantitative
      new AS400Text(SIZE_PIIN11), // Pas ETQ colis / Expédit°
      new AS400Text(SIZE_PIIN12), // Fac.Dif./ sur Cde soldée
      new AS400Text(SIZE_PIIN13), // Non Soumis à taxe addit.
      new AS400Text(SIZE_PIIN14), // Code PE particip. frais exp.
      new AS400Text(SIZE_PIIN15), // Gén.CNV si FAC avec prix net
      new AS400Text(SIZE_PIADH), // Code adhérent
      new AS400Text(SIZE_PICNC), // Code Cond. commissionnement
      new AS400ZonedDecimal(SIZE_PICC1, DECIMAL_PICC1), // Centrale 1
      new AS400ZonedDecimal(SIZE_PICC2, DECIMAL_PICC2), // Centrale 2
      new AS400ZonedDecimal(SIZE_PICC3, DECIMAL_PICC3), // Centrale 3
      new AS400Text(SIZE_PIFIR), // Code Firme
      new AS400ZonedDecimal(SIZE_PITRA, DECIMAL_PITRA), // Client Transitaire
      new AS400Text(SIZE_PICNR), // Code condition ristourne
      new AS400Text(SIZE_PICNP), // Code condition Promo
      new AS400Text(SIZE_PICNB), // Code remise pied de bon
      new AS400PackedDecimal(SIZE_PIDPL, DECIMAL_PIDPL), // Date Limite pour Sur-plafond
      new AS400Text(SIZE_PIIN16), // Application frais fixes (CC)
      new AS400Text(SIZE_PIIN17), // Application remise pied fact
      new AS400Text(SIZE_PICRT), // Code Regroupt.Transport
      new AS400ZonedDecimal(SIZE_PIOTO, DECIMAL_PIOTO), // Ordre dans la Tournée
      new AS400Text(SIZE_PICAN), // Canal de vente
      new AS400Text(SIZE_PIPRN), // Prénoms
      new AS400Text(SIZE_PIREC), // (R)Récence commande
      new AS400Text(SIZE_PIFRC), // (F)Fréquence commande
      new AS400Text(SIZE_PIMTC), // (M)Montant commande
      new AS400Text(SIZE_PINIP), // Code pays TVA Intracommun.
      new AS400Text(SIZE_PINIK), // Clé infor.TVA Intracommun.
      new AS400Text(SIZE_PICL1), // Classe de client 1
      new AS400Text(SIZE_PICL2), // Classe de client 2
      new AS400Text(SIZE_PICL3), // Classe de client 3
      new AS400Text(SIZE_PICLA), // Clé de classement 2 alpha
      new AS400Text(SIZE_PIIN18), // Client soumis à droitde prêt
      new AS400Text(SIZE_PIIN19), // Edition factures
      new AS400Text(SIZE_PIIN20), // Précommandes acceptées O/N
      new AS400Text(SIZE_PIIN21), // Nombre exemplaires avoirs
      new AS400Text(SIZE_PIIN22), // Type de vente paramètre VT
      new AS400Text(SIZE_PIIN23), // Tranche Effectif
      new AS400Text(SIZE_PIIN24), // Code PE particip. frais exp2
      new AS400Text(SIZE_PIIN25), // Code PE particip. frais exp3
      new AS400Text(SIZE_PIIN26), // Non édition remises
      new AS400Text(SIZE_PIIN27), // Non édition prix base
      new AS400Text(SIZE_PIAPEN), // Code APE v.2
      new AS400PackedDecimal(SIZE_PIPLF3, DECIMAL_PIPLF3), // Plafond demandé
      new AS400PackedDecimal(SIZE_PIDPL3, DECIMAL_PIDPL3), // Date demande du plafond
      new AS400Text(SIZE_PIIDAS), // Identifiant assurance
      new AS400Text(SIZE_PICAS), // Code assurance
      new AS400PackedDecimal(SIZE_PIDVP3, DECIMAL_PIDVP3), // Date validité du plafond
      new AS400PackedDecimal(SIZE_PIDAT1, DECIMAL_PIDAT1), // CA Previsionnel
      new AS400PackedDecimal(SIZE_PIDAT2, DECIMAL_PIDAT2), // Id. BIZY NOVA
      new AS400PackedDecimal(SIZE_PIDAT3, DECIMAL_PIDAT3), // Plafond maxi en déblocage
      new AS400ZonedDecimal(SIZE_PIFFP1, DECIMAL_PIFFP1), // frais fact. pied 1
      new AS400ZonedDecimal(SIZE_PIFFP2, DECIMAL_PIFFP2), // frais fact. pied 2
      new AS400ZonedDecimal(SIZE_PIFFP3, DECIMAL_PIFFP3), // frais fact. pied 3
      new AS400Text(SIZE_PIIN28), // non édition numéros séries
      new AS400Text(SIZE_PIIN29), // SUSPECT = S
      new AS400Text(SIZE_PIIN30), // Réf.client/bon obligatoire
      new AS400Text(SIZE_PICIV), // Civilité (PSEMPAR/CV)
      new AS400Text(SIZE_PINOMC), // Nom contact
      new AS400Text(SIZE_PIPRE), // Prénom contact
      new AS400Text(SIZE_PINET), // Adresse Email
      new AS400Text(SIZE_PINET2), // Adresse Email2
      new AS400Text(SIZE_PITELC), // Téléphone contact
      new AS400Text(SIZE_PIFAXC), // N°FAX contact
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, pietb, picli, piliv, pinom, picpl, pirue, piloc, picdp, pifil0, pivil, pipay, pifil, picop, picdp1, pitop1,
          pitop2, pitop3, pitop4, pitop5, picat, piclk, piape, pitel, pifax, pitlx, pilan, piobs, pirep, pirep2, pitrp, pigeo, pimex,
          pictr, pimag, pifrp, pitfa, pittc, pidev, piclfp, piclfs, piclp, piafa, piesc, pitar, pita1, pirem1, pirem2, pirem3, pirp1,
          pirp2, pirp3, picnv, pipfa, piprl, pirlv, pirbf, pirgl, piech, pincg, pinca, piact, piplf, piplf2, pitns, piatt, pinot, piddv,
          pidve, pisrn, pisrt, pifil1, picee, pivae, picde, piexp, pifac, pipco, picgm, pimaj, pinex1, pinex2, pinex3, pinla, pirst,
          piin1, piin2, piin3, piin4, piin5, piin6, piin7, piin8, piin9, piin10, piin11, piin12, piin13, piin14, piin15, piadh, picnc,
          picc1, picc2, picc3, pifir, pitra, picnr, picnp, picnb, pidpl, piin16, piin17, picrt, pioto, pican, piprn, pirec, pifrc, pimtc,
          pinip, pinik, picl1, picl2, picl3, picla, piin18, piin19, piin20, piin21, piin22, piin23, piin24, piin25, piin26, piin27,
          piapen, piplf3, pidpl3, piidas, picas, pidvp3, pidat1, pidat2, pidat3, piffp1, piffp2, piffp3, piin28, piin29, piin30, piciv,
          pinomc, pipre, pinet, pinet2, pitelc, pifaxc, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    picli = (BigDecimal) output[2];
    piliv = (BigDecimal) output[3];
    pinom = (String) output[4];
    picpl = (String) output[5];
    pirue = (String) output[6];
    piloc = (String) output[7];
    picdp = (String) output[8];
    pifil0 = (String) output[9];
    pivil = (String) output[10];
    pipay = (String) output[11];
    pifil = (String) output[12];
    picop = (String) output[13];
    picdp1 = (BigDecimal) output[14];
    pitop1 = (String) output[15];
    pitop2 = (String) output[16];
    pitop3 = (String) output[17];
    pitop4 = (String) output[18];
    pitop5 = (String) output[19];
    picat = (String) output[20];
    piclk = (String) output[21];
    piape = (String) output[22];
    pitel = (String) output[23];
    pifax = (String) output[24];
    pitlx = (String) output[25];
    pilan = (String) output[26];
    piobs = (String) output[27];
    pirep = (String) output[28];
    pirep2 = (String) output[29];
    pitrp = (String) output[30];
    pigeo = (String) output[31];
    pimex = (String) output[32];
    pictr = (String) output[33];
    pimag = (String) output[34];
    pifrp = (BigDecimal) output[35];
    pitfa = (String) output[36];
    pittc = (String) output[37];
    pidev = (String) output[38];
    piclfp = (BigDecimal) output[39];
    piclfs = (BigDecimal) output[40];
    piclp = (BigDecimal) output[41];
    piafa = (String) output[42];
    piesc = (BigDecimal) output[43];
    pitar = (BigDecimal) output[44];
    pita1 = (BigDecimal) output[45];
    pirem1 = (BigDecimal) output[46];
    pirem2 = (BigDecimal) output[47];
    pirem3 = (BigDecimal) output[48];
    pirp1 = (BigDecimal) output[49];
    pirp2 = (BigDecimal) output[50];
    pirp3 = (BigDecimal) output[51];
    picnv = (String) output[52];
    pipfa = (String) output[53];
    piprl = (String) output[54];
    pirlv = (String) output[55];
    pirbf = (BigDecimal) output[56];
    pirgl = (String) output[57];
    piech = (String) output[58];
    pincg = (BigDecimal) output[59];
    pinca = (BigDecimal) output[60];
    piact = (String) output[61];
    piplf = (BigDecimal) output[62];
    piplf2 = (BigDecimal) output[63];
    pitns = (BigDecimal) output[64];
    piatt = (String) output[65];
    pinot = (String) output[66];
    piddv = (BigDecimal) output[67];
    pidve = (BigDecimal) output[68];
    pisrn = (String) output[69];
    pisrt = (String) output[70];
    pifil1 = (String) output[71];
    picee = (String) output[72];
    pivae = (BigDecimal) output[73];
    picde = (BigDecimal) output[74];
    piexp = (BigDecimal) output[75];
    pifac = (BigDecimal) output[76];
    pipco = (BigDecimal) output[77];
    picgm = (BigDecimal) output[78];
    pimaj = (BigDecimal) output[79];
    pinex1 = (BigDecimal) output[80];
    pinex2 = (BigDecimal) output[81];
    pinex3 = (BigDecimal) output[82];
    pinla = (String) output[83];
    pirst = (String) output[84];
    piin1 = (String) output[85];
    piin2 = (String) output[86];
    piin3 = (String) output[87];
    piin4 = (String) output[88];
    piin5 = (String) output[89];
    piin6 = (String) output[90];
    piin7 = (String) output[91];
    piin8 = (String) output[92];
    piin9 = (String) output[93];
    piin10 = (String) output[94];
    piin11 = (String) output[95];
    piin12 = (String) output[96];
    piin13 = (String) output[97];
    piin14 = (String) output[98];
    piin15 = (String) output[99];
    piadh = (String) output[100];
    picnc = (String) output[101];
    picc1 = (BigDecimal) output[102];
    picc2 = (BigDecimal) output[103];
    picc3 = (BigDecimal) output[104];
    pifir = (String) output[105];
    pitra = (BigDecimal) output[106];
    picnr = (String) output[107];
    picnp = (String) output[108];
    picnb = (String) output[109];
    pidpl = (BigDecimal) output[110];
    piin16 = (String) output[111];
    piin17 = (String) output[112];
    picrt = (String) output[113];
    pioto = (BigDecimal) output[114];
    pican = (String) output[115];
    piprn = (String) output[116];
    pirec = (String) output[117];
    pifrc = (String) output[118];
    pimtc = (String) output[119];
    pinip = (String) output[120];
    pinik = (String) output[121];
    picl1 = (String) output[122];
    picl2 = (String) output[123];
    picl3 = (String) output[124];
    picla = (String) output[125];
    piin18 = (String) output[126];
    piin19 = (String) output[127];
    piin20 = (String) output[128];
    piin21 = (String) output[129];
    piin22 = (String) output[130];
    piin23 = (String) output[131];
    piin24 = (String) output[132];
    piin25 = (String) output[133];
    piin26 = (String) output[134];
    piin27 = (String) output[135];
    piapen = (String) output[136];
    piplf3 = (BigDecimal) output[137];
    pidpl3 = (BigDecimal) output[138];
    piidas = (String) output[139];
    picas = (String) output[140];
    pidvp3 = (BigDecimal) output[141];
    pidat1 = (BigDecimal) output[142];
    pidat2 = (BigDecimal) output[143];
    pidat3 = (BigDecimal) output[144];
    piffp1 = (BigDecimal) output[145];
    piffp2 = (BigDecimal) output[146];
    piffp3 = (BigDecimal) output[147];
    piin28 = (String) output[148];
    piin29 = (String) output[149];
    piin30 = (String) output[150];
    piciv = (String) output[151];
    pinomc = (String) output[152];
    pipre = (String) output[153];
    pinet = (String) output[154];
    pinet2 = (String) output[155];
    pitelc = (String) output[156];
    pifaxc = (String) output[157];
    piarr = (String) output[158];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPicli(BigDecimal pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = pPicli.setScale(DECIMAL_PICLI, RoundingMode.HALF_UP);
  }
  
  public void setPicli(Integer pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = BigDecimal.valueOf(pPicli);
  }
  
  public Integer getPicli() {
    return picli.intValue();
  }
  
  public void setPiliv(BigDecimal pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = pPiliv.setScale(DECIMAL_PILIV, RoundingMode.HALF_UP);
  }
  
  public void setPiliv(Integer pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = BigDecimal.valueOf(pPiliv);
  }
  
  public Integer getPiliv() {
    return piliv.intValue();
  }
  
  public void setPinom(String pPinom) {
    if (pPinom == null) {
      return;
    }
    pinom = pPinom;
  }
  
  public String getPinom() {
    return pinom;
  }
  
  public void setPicpl(String pPicpl) {
    if (pPicpl == null) {
      return;
    }
    picpl = pPicpl;
  }
  
  public String getPicpl() {
    return picpl;
  }
  
  public void setPirue(String pPirue) {
    if (pPirue == null) {
      return;
    }
    pirue = pPirue;
  }
  
  public String getPirue() {
    return pirue;
  }
  
  public void setPiloc(String pPiloc) {
    if (pPiloc == null) {
      return;
    }
    piloc = pPiloc;
  }
  
  public String getPiloc() {
    return piloc;
  }
  
  public void setPicdp(String pPicdp) {
    if (pPicdp == null) {
      return;
    }
    picdp = pPicdp;
  }
  
  public String getPicdp() {
    return picdp;
  }
  
  public void setPifil0(Character pPifil0) {
    if (pPifil0 == null) {
      return;
    }
    pifil0 = String.valueOf(pPifil0);
  }
  
  public Character getPifil0() {
    return pifil0.charAt(0);
  }
  
  public void setPivil(String pPivil) {
    if (pPivil == null) {
      return;
    }
    pivil = pPivil;
  }
  
  public String getPivil() {
    return pivil;
  }
  
  public void setPipay(String pPipay) {
    if (pPipay == null) {
      return;
    }
    pipay = pPipay;
  }
  
  public String getPipay() {
    return pipay;
  }
  
  public void setPifil(Character pPifil) {
    if (pPifil == null) {
      return;
    }
    pifil = String.valueOf(pPifil);
  }
  
  public Character getPifil() {
    return pifil.charAt(0);
  }
  
  public void setPicop(String pPicop) {
    if (pPicop == null) {
      return;
    }
    picop = pPicop;
  }
  
  public String getPicop() {
    return picop;
  }
  
  public void setPicdp1(BigDecimal pPicdp1) {
    if (pPicdp1 == null) {
      return;
    }
    picdp1 = pPicdp1.setScale(DECIMAL_PICDP1, RoundingMode.HALF_UP);
  }
  
  public void setPicdp1(Integer pPicdp1) {
    if (pPicdp1 == null) {
      return;
    }
    picdp1 = BigDecimal.valueOf(pPicdp1);
  }
  
  public Integer getPicdp1() {
    return picdp1.intValue();
  }
  
  public void setPitop1(String pPitop1) {
    if (pPitop1 == null) {
      return;
    }
    pitop1 = pPitop1;
  }
  
  public String getPitop1() {
    return pitop1;
  }
  
  public void setPitop2(String pPitop2) {
    if (pPitop2 == null) {
      return;
    }
    pitop2 = pPitop2;
  }
  
  public String getPitop2() {
    return pitop2;
  }
  
  public void setPitop3(String pPitop3) {
    if (pPitop3 == null) {
      return;
    }
    pitop3 = pPitop3;
  }
  
  public String getPitop3() {
    return pitop3;
  }
  
  public void setPitop4(String pPitop4) {
    if (pPitop4 == null) {
      return;
    }
    pitop4 = pPitop4;
  }
  
  public String getPitop4() {
    return pitop4;
  }
  
  public void setPitop5(String pPitop5) {
    if (pPitop5 == null) {
      return;
    }
    pitop5 = pPitop5;
  }
  
  public String getPitop5() {
    return pitop5;
  }
  
  public void setPicat(String pPicat) {
    if (pPicat == null) {
      return;
    }
    picat = pPicat;
  }
  
  public String getPicat() {
    return picat;
  }
  
  public void setPiclk(String pPiclk) {
    if (pPiclk == null) {
      return;
    }
    piclk = pPiclk;
  }
  
  public String getPiclk() {
    return piclk;
  }
  
  public void setPiape(String pPiape) {
    if (pPiape == null) {
      return;
    }
    piape = pPiape;
  }
  
  public String getPiape() {
    return piape;
  }
  
  public void setPitel(String pPitel) {
    if (pPitel == null) {
      return;
    }
    pitel = pPitel;
  }
  
  public String getPitel() {
    return pitel;
  }
  
  public void setPifax(String pPifax) {
    if (pPifax == null) {
      return;
    }
    pifax = pPifax;
  }
  
  public String getPifax() {
    return pifax;
  }
  
  public void setPitlx(String pPitlx) {
    if (pPitlx == null) {
      return;
    }
    pitlx = pPitlx;
  }
  
  public String getPitlx() {
    return pitlx;
  }
  
  public void setPilan(String pPilan) {
    if (pPilan == null) {
      return;
    }
    pilan = pPilan;
  }
  
  public String getPilan() {
    return pilan;
  }
  
  public void setPiobs(String pPiobs) {
    if (pPiobs == null) {
      return;
    }
    piobs = pPiobs;
  }
  
  public String getPiobs() {
    return piobs;
  }
  
  public void setPirep(String pPirep) {
    if (pPirep == null) {
      return;
    }
    pirep = pPirep;
  }
  
  public String getPirep() {
    return pirep;
  }
  
  public void setPirep2(String pPirep2) {
    if (pPirep2 == null) {
      return;
    }
    pirep2 = pPirep2;
  }
  
  public String getPirep2() {
    return pirep2;
  }
  
  public void setPitrp(Character pPitrp) {
    if (pPitrp == null) {
      return;
    }
    pitrp = String.valueOf(pPitrp);
  }
  
  public Character getPitrp() {
    return pitrp.charAt(0);
  }
  
  public void setPigeo(String pPigeo) {
    if (pPigeo == null) {
      return;
    }
    pigeo = pPigeo;
  }
  
  public String getPigeo() {
    return pigeo;
  }
  
  public void setPimex(String pPimex) {
    if (pPimex == null) {
      return;
    }
    pimex = pPimex;
  }
  
  public String getPimex() {
    return pimex;
  }
  
  public void setPictr(String pPictr) {
    if (pPictr == null) {
      return;
    }
    pictr = pPictr;
  }
  
  public String getPictr() {
    return pictr;
  }
  
  public void setPimag(String pPimag) {
    if (pPimag == null) {
      return;
    }
    pimag = pPimag;
  }
  
  public String getPimag() {
    return pimag;
  }
  
  public void setPifrp(BigDecimal pPifrp) {
    if (pPifrp == null) {
      return;
    }
    pifrp = pPifrp.setScale(DECIMAL_PIFRP, RoundingMode.HALF_UP);
  }
  
  public void setPifrp(Integer pPifrp) {
    if (pPifrp == null) {
      return;
    }
    pifrp = BigDecimal.valueOf(pPifrp);
  }
  
  public void setPifrp(Date pPifrp) {
    if (pPifrp == null) {
      return;
    }
    pifrp = BigDecimal.valueOf(ConvertDate.dateToDb2(pPifrp));
  }
  
  public Integer getPifrp() {
    return pifrp.intValue();
  }
  
  public Date getPifrpConvertiEnDate() {
    return ConvertDate.db2ToDate(pifrp.intValue(), null);
  }
  
  public void setPitfa(Character pPitfa) {
    if (pPitfa == null) {
      return;
    }
    pitfa = String.valueOf(pPitfa);
  }
  
  public Character getPitfa() {
    return pitfa.charAt(0);
  }
  
  public void setPittc(Character pPittc) {
    if (pPittc == null) {
      return;
    }
    pittc = String.valueOf(pPittc);
  }
  
  public Character getPittc() {
    return pittc.charAt(0);
  }
  
  public void setPidev(String pPidev) {
    if (pPidev == null) {
      return;
    }
    pidev = pPidev;
  }
  
  public String getPidev() {
    return pidev;
  }
  
  public void setPiclfp(BigDecimal pPiclfp) {
    if (pPiclfp == null) {
      return;
    }
    piclfp = pPiclfp.setScale(DECIMAL_PICLFP, RoundingMode.HALF_UP);
  }
  
  public void setPiclfp(Integer pPiclfp) {
    if (pPiclfp == null) {
      return;
    }
    piclfp = BigDecimal.valueOf(pPiclfp);
  }
  
  public Integer getPiclfp() {
    return piclfp.intValue();
  }
  
  public void setPiclfs(BigDecimal pPiclfs) {
    if (pPiclfs == null) {
      return;
    }
    piclfs = pPiclfs.setScale(DECIMAL_PICLFS, RoundingMode.HALF_UP);
  }
  
  public void setPiclfs(Integer pPiclfs) {
    if (pPiclfs == null) {
      return;
    }
    piclfs = BigDecimal.valueOf(pPiclfs);
  }
  
  public Integer getPiclfs() {
    return piclfs.intValue();
  }
  
  public void setPiclp(BigDecimal pPiclp) {
    if (pPiclp == null) {
      return;
    }
    piclp = pPiclp.setScale(DECIMAL_PICLP, RoundingMode.HALF_UP);
  }
  
  public void setPiclp(Integer pPiclp) {
    if (pPiclp == null) {
      return;
    }
    piclp = BigDecimal.valueOf(pPiclp);
  }
  
  public Integer getPiclp() {
    return piclp.intValue();
  }
  
  public void setPiafa(String pPiafa) {
    if (pPiafa == null) {
      return;
    }
    piafa = pPiafa;
  }
  
  public String getPiafa() {
    return piafa;
  }
  
  public void setPiesc(BigDecimal pPiesc) {
    if (pPiesc == null) {
      return;
    }
    piesc = pPiesc.setScale(DECIMAL_PIESC, RoundingMode.HALF_UP);
  }
  
  public void setPiesc(Double pPiesc) {
    if (pPiesc == null) {
      return;
    }
    piesc = BigDecimal.valueOf(pPiesc).setScale(DECIMAL_PIESC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiesc() {
    return piesc.setScale(DECIMAL_PIESC, RoundingMode.HALF_UP);
  }
  
  public void setPitar(BigDecimal pPitar) {
    if (pPitar == null) {
      return;
    }
    pitar = pPitar.setScale(DECIMAL_PITAR, RoundingMode.HALF_UP);
  }
  
  public void setPitar(Integer pPitar) {
    if (pPitar == null) {
      return;
    }
    pitar = BigDecimal.valueOf(pPitar);
  }
  
  public Integer getPitar() {
    return pitar.intValue();
  }
  
  public void setPita1(BigDecimal pPita1) {
    if (pPita1 == null) {
      return;
    }
    pita1 = pPita1.setScale(DECIMAL_PITA1, RoundingMode.HALF_UP);
  }
  
  public void setPita1(Integer pPita1) {
    if (pPita1 == null) {
      return;
    }
    pita1 = BigDecimal.valueOf(pPita1);
  }
  
  public Integer getPita1() {
    return pita1.intValue();
  }
  
  public void setPirem1(BigDecimal pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = pPirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem1(Double pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = BigDecimal.valueOf(pPirem1).setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem1() {
    return pirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(BigDecimal pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = pPirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(Double pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = BigDecimal.valueOf(pPirem2).setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem2() {
    return pirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(BigDecimal pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = pPirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(Double pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = BigDecimal.valueOf(pPirem3).setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem3() {
    return pirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirp1(BigDecimal pPirp1) {
    if (pPirp1 == null) {
      return;
    }
    pirp1 = pPirp1.setScale(DECIMAL_PIRP1, RoundingMode.HALF_UP);
  }
  
  public void setPirp1(Double pPirp1) {
    if (pPirp1 == null) {
      return;
    }
    pirp1 = BigDecimal.valueOf(pPirp1).setScale(DECIMAL_PIRP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirp1() {
    return pirp1.setScale(DECIMAL_PIRP1, RoundingMode.HALF_UP);
  }
  
  public void setPirp2(BigDecimal pPirp2) {
    if (pPirp2 == null) {
      return;
    }
    pirp2 = pPirp2.setScale(DECIMAL_PIRP2, RoundingMode.HALF_UP);
  }
  
  public void setPirp2(Double pPirp2) {
    if (pPirp2 == null) {
      return;
    }
    pirp2 = BigDecimal.valueOf(pPirp2).setScale(DECIMAL_PIRP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirp2() {
    return pirp2.setScale(DECIMAL_PIRP2, RoundingMode.HALF_UP);
  }
  
  public void setPirp3(BigDecimal pPirp3) {
    if (pPirp3 == null) {
      return;
    }
    pirp3 = pPirp3.setScale(DECIMAL_PIRP3, RoundingMode.HALF_UP);
  }
  
  public void setPirp3(Double pPirp3) {
    if (pPirp3 == null) {
      return;
    }
    pirp3 = BigDecimal.valueOf(pPirp3).setScale(DECIMAL_PIRP3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirp3() {
    return pirp3.setScale(DECIMAL_PIRP3, RoundingMode.HALF_UP);
  }
  
  public void setPicnv(String pPicnv) {
    if (pPicnv == null) {
      return;
    }
    picnv = pPicnv;
  }
  
  public String getPicnv() {
    return picnv;
  }
  
  public void setPipfa(Character pPipfa) {
    if (pPipfa == null) {
      return;
    }
    pipfa = String.valueOf(pPipfa);
  }
  
  public Character getPipfa() {
    return pipfa.charAt(0);
  }
  
  public void setPiprl(Character pPiprl) {
    if (pPiprl == null) {
      return;
    }
    piprl = String.valueOf(pPiprl);
  }
  
  public Character getPiprl() {
    return piprl.charAt(0);
  }
  
  public void setPirlv(Character pPirlv) {
    if (pPirlv == null) {
      return;
    }
    pirlv = String.valueOf(pPirlv);
  }
  
  public Character getPirlv() {
    return pirlv.charAt(0);
  }
  
  public void setPirbf(BigDecimal pPirbf) {
    if (pPirbf == null) {
      return;
    }
    pirbf = pPirbf.setScale(DECIMAL_PIRBF, RoundingMode.HALF_UP);
  }
  
  public void setPirbf(Integer pPirbf) {
    if (pPirbf == null) {
      return;
    }
    pirbf = BigDecimal.valueOf(pPirbf);
  }
  
  public Integer getPirbf() {
    return pirbf.intValue();
  }
  
  public void setPirgl(String pPirgl) {
    if (pPirgl == null) {
      return;
    }
    pirgl = pPirgl;
  }
  
  public String getPirgl() {
    return pirgl;
  }
  
  public void setPiech(String pPiech) {
    if (pPiech == null) {
      return;
    }
    piech = pPiech;
  }
  
  public String getPiech() {
    return piech;
  }
  
  public void setPincg(BigDecimal pPincg) {
    if (pPincg == null) {
      return;
    }
    pincg = pPincg.setScale(DECIMAL_PINCG, RoundingMode.HALF_UP);
  }
  
  public void setPincg(Integer pPincg) {
    if (pPincg == null) {
      return;
    }
    pincg = BigDecimal.valueOf(pPincg);
  }
  
  public Integer getPincg() {
    return pincg.intValue();
  }
  
  public void setPinca(BigDecimal pPinca) {
    if (pPinca == null) {
      return;
    }
    pinca = pPinca.setScale(DECIMAL_PINCA, RoundingMode.HALF_UP);
  }
  
  public void setPinca(Integer pPinca) {
    if (pPinca == null) {
      return;
    }
    pinca = BigDecimal.valueOf(pPinca);
  }
  
  public Integer getPinca() {
    return pinca.intValue();
  }
  
  public void setPiact(String pPiact) {
    if (pPiact == null) {
      return;
    }
    piact = pPiact;
  }
  
  public String getPiact() {
    return piact;
  }
  
  public void setPiplf(BigDecimal pPiplf) {
    if (pPiplf == null) {
      return;
    }
    piplf = pPiplf.setScale(DECIMAL_PIPLF, RoundingMode.HALF_UP);
  }
  
  public void setPiplf(Integer pPiplf) {
    if (pPiplf == null) {
      return;
    }
    piplf = BigDecimal.valueOf(pPiplf);
  }
  
  public void setPiplf(Date pPiplf) {
    if (pPiplf == null) {
      return;
    }
    piplf = BigDecimal.valueOf(ConvertDate.dateToDb2(pPiplf));
  }
  
  public Integer getPiplf() {
    return piplf.intValue();
  }
  
  public Date getPiplfConvertiEnDate() {
    return ConvertDate.db2ToDate(piplf.intValue(), null);
  }
  
  public void setPiplf2(BigDecimal pPiplf2) {
    if (pPiplf2 == null) {
      return;
    }
    piplf2 = pPiplf2.setScale(DECIMAL_PIPLF2, RoundingMode.HALF_UP);
  }
  
  public void setPiplf2(Integer pPiplf2) {
    if (pPiplf2 == null) {
      return;
    }
    piplf2 = BigDecimal.valueOf(pPiplf2);
  }
  
  public void setPiplf2(Date pPiplf2) {
    if (pPiplf2 == null) {
      return;
    }
    piplf2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPiplf2));
  }
  
  public Integer getPiplf2() {
    return piplf2.intValue();
  }
  
  public Date getPiplf2ConvertiEnDate() {
    return ConvertDate.db2ToDate(piplf2.intValue(), null);
  }
  
  public void setPitns(BigDecimal pPitns) {
    if (pPitns == null) {
      return;
    }
    pitns = pPitns.setScale(DECIMAL_PITNS, RoundingMode.HALF_UP);
  }
  
  public void setPitns(Integer pPitns) {
    if (pPitns == null) {
      return;
    }
    pitns = BigDecimal.valueOf(pPitns);
  }
  
  public Integer getPitns() {
    return pitns.intValue();
  }
  
  public void setPiatt(String pPiatt) {
    if (pPiatt == null) {
      return;
    }
    piatt = pPiatt;
  }
  
  public String getPiatt() {
    return piatt;
  }
  
  public void setPinot(String pPinot) {
    if (pPinot == null) {
      return;
    }
    pinot = pPinot;
  }
  
  public String getPinot() {
    return pinot;
  }
  
  public void setPiddv(BigDecimal pPiddv) {
    if (pPiddv == null) {
      return;
    }
    piddv = pPiddv.setScale(DECIMAL_PIDDV, RoundingMode.HALF_UP);
  }
  
  public void setPiddv(Integer pPiddv) {
    if (pPiddv == null) {
      return;
    }
    piddv = BigDecimal.valueOf(pPiddv);
  }
  
  public void setPiddv(Date pPiddv) {
    if (pPiddv == null) {
      return;
    }
    piddv = BigDecimal.valueOf(ConvertDate.dateToDb2(pPiddv));
  }
  
  public Integer getPiddv() {
    return piddv.intValue();
  }
  
  public Date getPiddvConvertiEnDate() {
    return ConvertDate.db2ToDate(piddv.intValue(), null);
  }
  
  public void setPidve(BigDecimal pPidve) {
    if (pPidve == null) {
      return;
    }
    pidve = pPidve.setScale(DECIMAL_PIDVE, RoundingMode.HALF_UP);
  }
  
  public void setPidve(Integer pPidve) {
    if (pPidve == null) {
      return;
    }
    pidve = BigDecimal.valueOf(pPidve);
  }
  
  public void setPidve(Date pPidve) {
    if (pPidve == null) {
      return;
    }
    pidve = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidve));
  }
  
  public Integer getPidve() {
    return pidve.intValue();
  }
  
  public Date getPidveConvertiEnDate() {
    return ConvertDate.db2ToDate(pidve.intValue(), null);
  }
  
  public void setPisrn(String pPisrn) {
    if (pPisrn == null) {
      return;
    }
    pisrn = pPisrn;
  }
  
  public String getPisrn() {
    return pisrn;
  }
  
  public void setPisrt(String pPisrt) {
    if (pPisrt == null) {
      return;
    }
    pisrt = pPisrt;
  }
  
  public String getPisrt() {
    return pisrt;
  }
  
  public void setPifil1(Character pPifil1) {
    if (pPifil1 == null) {
      return;
    }
    pifil1 = String.valueOf(pPifil1);
  }
  
  public Character getPifil1() {
    return pifil1.charAt(0);
  }
  
  public void setPicee(String pPicee) {
    if (pPicee == null) {
      return;
    }
    picee = pPicee;
  }
  
  public String getPicee() {
    return picee;
  }
  
  public void setPivae(BigDecimal pPivae) {
    if (pPivae == null) {
      return;
    }
    pivae = pPivae.setScale(DECIMAL_PIVAE, RoundingMode.HALF_UP);
  }
  
  public void setPivae(Integer pPivae) {
    if (pPivae == null) {
      return;
    }
    pivae = BigDecimal.valueOf(pPivae);
  }
  
  public void setPivae(Date pPivae) {
    if (pPivae == null) {
      return;
    }
    pivae = BigDecimal.valueOf(ConvertDate.dateToDb2(pPivae));
  }
  
  public Integer getPivae() {
    return pivae.intValue();
  }
  
  public Date getPivaeConvertiEnDate() {
    return ConvertDate.db2ToDate(pivae.intValue(), null);
  }
  
  public void setPicde(BigDecimal pPicde) {
    if (pPicde == null) {
      return;
    }
    picde = pPicde.setScale(DECIMAL_PICDE, RoundingMode.HALF_UP);
  }
  
  public void setPicde(Double pPicde) {
    if (pPicde == null) {
      return;
    }
    picde = BigDecimal.valueOf(pPicde).setScale(DECIMAL_PICDE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPicde() {
    return picde.setScale(DECIMAL_PICDE, RoundingMode.HALF_UP);
  }
  
  public void setPiexp(BigDecimal pPiexp) {
    if (pPiexp == null) {
      return;
    }
    piexp = pPiexp.setScale(DECIMAL_PIEXP, RoundingMode.HALF_UP);
  }
  
  public void setPiexp(Double pPiexp) {
    if (pPiexp == null) {
      return;
    }
    piexp = BigDecimal.valueOf(pPiexp).setScale(DECIMAL_PIEXP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiexp() {
    return piexp.setScale(DECIMAL_PIEXP, RoundingMode.HALF_UP);
  }
  
  public void setPifac(BigDecimal pPifac) {
    if (pPifac == null) {
      return;
    }
    pifac = pPifac.setScale(DECIMAL_PIFAC, RoundingMode.HALF_UP);
  }
  
  public void setPifac(Double pPifac) {
    if (pPifac == null) {
      return;
    }
    pifac = BigDecimal.valueOf(pPifac).setScale(DECIMAL_PIFAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifac() {
    return pifac.setScale(DECIMAL_PIFAC, RoundingMode.HALF_UP);
  }
  
  public void setPipco(BigDecimal pPipco) {
    if (pPipco == null) {
      return;
    }
    pipco = pPipco.setScale(DECIMAL_PIPCO, RoundingMode.HALF_UP);
  }
  
  public void setPipco(Integer pPipco) {
    if (pPipco == null) {
      return;
    }
    pipco = BigDecimal.valueOf(pPipco);
  }
  
  public void setPipco(Date pPipco) {
    if (pPipco == null) {
      return;
    }
    pipco = BigDecimal.valueOf(ConvertDate.dateToDb2(pPipco));
  }
  
  public Integer getPipco() {
    return pipco.intValue();
  }
  
  public Date getPipcoConvertiEnDate() {
    return ConvertDate.db2ToDate(pipco.intValue(), null);
  }
  
  public void setPicgm(BigDecimal pPicgm) {
    if (pPicgm == null) {
      return;
    }
    picgm = pPicgm.setScale(DECIMAL_PICGM, RoundingMode.HALF_UP);
  }
  
  public void setPicgm(Integer pPicgm) {
    if (pPicgm == null) {
      return;
    }
    picgm = BigDecimal.valueOf(pPicgm);
  }
  
  public Integer getPicgm() {
    return picgm.intValue();
  }
  
  public void setPimaj(BigDecimal pPimaj) {
    if (pPimaj == null) {
      return;
    }
    pimaj = pPimaj.setScale(DECIMAL_PIMAJ, RoundingMode.HALF_UP);
  }
  
  public void setPimaj(Integer pPimaj) {
    if (pPimaj == null) {
      return;
    }
    pimaj = BigDecimal.valueOf(pPimaj);
  }
  
  public Integer getPimaj() {
    return pimaj.intValue();
  }
  
  public void setPinex1(BigDecimal pPinex1) {
    if (pPinex1 == null) {
      return;
    }
    pinex1 = pPinex1.setScale(DECIMAL_PINEX1, RoundingMode.HALF_UP);
  }
  
  public void setPinex1(Integer pPinex1) {
    if (pPinex1 == null) {
      return;
    }
    pinex1 = BigDecimal.valueOf(pPinex1);
  }
  
  public Integer getPinex1() {
    return pinex1.intValue();
  }
  
  public void setPinex2(BigDecimal pPinex2) {
    if (pPinex2 == null) {
      return;
    }
    pinex2 = pPinex2.setScale(DECIMAL_PINEX2, RoundingMode.HALF_UP);
  }
  
  public void setPinex2(Integer pPinex2) {
    if (pPinex2 == null) {
      return;
    }
    pinex2 = BigDecimal.valueOf(pPinex2);
  }
  
  public Integer getPinex2() {
    return pinex2.intValue();
  }
  
  public void setPinex3(BigDecimal pPinex3) {
    if (pPinex3 == null) {
      return;
    }
    pinex3 = pPinex3.setScale(DECIMAL_PINEX3, RoundingMode.HALF_UP);
  }
  
  public void setPinex3(Integer pPinex3) {
    if (pPinex3 == null) {
      return;
    }
    pinex3 = BigDecimal.valueOf(pPinex3);
  }
  
  public Integer getPinex3() {
    return pinex3.intValue();
  }
  
  public void setPinla(String pPinla) {
    if (pPinla == null) {
      return;
    }
    pinla = pPinla;
  }
  
  public String getPinla() {
    return pinla;
  }
  
  public void setPirst(String pPirst) {
    if (pPirst == null) {
      return;
    }
    pirst = pPirst;
  }
  
  public String getPirst() {
    return pirst;
  }
  
  public void setPiin1(Character pPiin1) {
    if (pPiin1 == null) {
      return;
    }
    piin1 = String.valueOf(pPiin1);
  }
  
  public Character getPiin1() {
    return piin1.charAt(0);
  }
  
  public void setPiin2(Character pPiin2) {
    if (pPiin2 == null) {
      return;
    }
    piin2 = String.valueOf(pPiin2);
  }
  
  public Character getPiin2() {
    return piin2.charAt(0);
  }
  
  public void setPiin3(Character pPiin3) {
    if (pPiin3 == null) {
      return;
    }
    piin3 = String.valueOf(pPiin3);
  }
  
  public Character getPiin3() {
    return piin3.charAt(0);
  }
  
  public void setPiin4(Character pPiin4) {
    if (pPiin4 == null) {
      return;
    }
    piin4 = String.valueOf(pPiin4);
  }
  
  public Character getPiin4() {
    return piin4.charAt(0);
  }
  
  public void setPiin5(Character pPiin5) {
    if (pPiin5 == null) {
      return;
    }
    piin5 = String.valueOf(pPiin5);
  }
  
  public Character getPiin5() {
    return piin5.charAt(0);
  }
  
  public void setPiin6(Character pPiin6) {
    if (pPiin6 == null) {
      return;
    }
    piin6 = String.valueOf(pPiin6);
  }
  
  public Character getPiin6() {
    return piin6.charAt(0);
  }
  
  public void setPiin7(Character pPiin7) {
    if (pPiin7 == null) {
      return;
    }
    piin7 = String.valueOf(pPiin7);
  }
  
  public Character getPiin7() {
    return piin7.charAt(0);
  }
  
  public void setPiin8(Character pPiin8) {
    if (pPiin8 == null) {
      return;
    }
    piin8 = String.valueOf(pPiin8);
  }
  
  public Character getPiin8() {
    return piin8.charAt(0);
  }
  
  public void setPiin9(Character pPiin9) {
    if (pPiin9 == null) {
      return;
    }
    piin9 = String.valueOf(pPiin9);
  }
  
  public Character getPiin9() {
    return piin9.charAt(0);
  }
  
  public void setPiin10(Character pPiin10) {
    if (pPiin10 == null) {
      return;
    }
    piin10 = String.valueOf(pPiin10);
  }
  
  public Character getPiin10() {
    return piin10.charAt(0);
  }
  
  public void setPiin11(Character pPiin11) {
    if (pPiin11 == null) {
      return;
    }
    piin11 = String.valueOf(pPiin11);
  }
  
  public Character getPiin11() {
    return piin11.charAt(0);
  }
  
  public void setPiin12(Character pPiin12) {
    if (pPiin12 == null) {
      return;
    }
    piin12 = String.valueOf(pPiin12);
  }
  
  public Character getPiin12() {
    return piin12.charAt(0);
  }
  
  public void setPiin13(Character pPiin13) {
    if (pPiin13 == null) {
      return;
    }
    piin13 = String.valueOf(pPiin13);
  }
  
  public Character getPiin13() {
    return piin13.charAt(0);
  }
  
  public void setPiin14(Character pPiin14) {
    if (pPiin14 == null) {
      return;
    }
    piin14 = String.valueOf(pPiin14);
  }
  
  public Character getPiin14() {
    return piin14.charAt(0);
  }
  
  public void setPiin15(Character pPiin15) {
    if (pPiin15 == null) {
      return;
    }
    piin15 = String.valueOf(pPiin15);
  }
  
  public Character getPiin15() {
    return piin15.charAt(0);
  }
  
  public void setPiadh(String pPiadh) {
    if (pPiadh == null) {
      return;
    }
    piadh = pPiadh;
  }
  
  public String getPiadh() {
    return piadh;
  }
  
  public void setPicnc(String pPicnc) {
    if (pPicnc == null) {
      return;
    }
    picnc = pPicnc;
  }
  
  public String getPicnc() {
    return picnc;
  }
  
  public void setPicc1(BigDecimal pPicc1) {
    if (pPicc1 == null) {
      return;
    }
    picc1 = pPicc1.setScale(DECIMAL_PICC1, RoundingMode.HALF_UP);
  }
  
  public void setPicc1(Integer pPicc1) {
    if (pPicc1 == null) {
      return;
    }
    picc1 = BigDecimal.valueOf(pPicc1);
  }
  
  public Integer getPicc1() {
    return picc1.intValue();
  }
  
  public void setPicc2(BigDecimal pPicc2) {
    if (pPicc2 == null) {
      return;
    }
    picc2 = pPicc2.setScale(DECIMAL_PICC2, RoundingMode.HALF_UP);
  }
  
  public void setPicc2(Integer pPicc2) {
    if (pPicc2 == null) {
      return;
    }
    picc2 = BigDecimal.valueOf(pPicc2);
  }
  
  public Integer getPicc2() {
    return picc2.intValue();
  }
  
  public void setPicc3(BigDecimal pPicc3) {
    if (pPicc3 == null) {
      return;
    }
    picc3 = pPicc3.setScale(DECIMAL_PICC3, RoundingMode.HALF_UP);
  }
  
  public void setPicc3(Integer pPicc3) {
    if (pPicc3 == null) {
      return;
    }
    picc3 = BigDecimal.valueOf(pPicc3);
  }
  
  public Integer getPicc3() {
    return picc3.intValue();
  }
  
  public void setPifir(String pPifir) {
    if (pPifir == null) {
      return;
    }
    pifir = pPifir;
  }
  
  public String getPifir() {
    return pifir;
  }
  
  public void setPitra(BigDecimal pPitra) {
    if (pPitra == null) {
      return;
    }
    pitra = pPitra.setScale(DECIMAL_PITRA, RoundingMode.HALF_UP);
  }
  
  public void setPitra(Integer pPitra) {
    if (pPitra == null) {
      return;
    }
    pitra = BigDecimal.valueOf(pPitra);
  }
  
  public Integer getPitra() {
    return pitra.intValue();
  }
  
  public void setPicnr(String pPicnr) {
    if (pPicnr == null) {
      return;
    }
    picnr = pPicnr;
  }
  
  public String getPicnr() {
    return picnr;
  }
  
  public void setPicnp(String pPicnp) {
    if (pPicnp == null) {
      return;
    }
    picnp = pPicnp;
  }
  
  public String getPicnp() {
    return picnp;
  }
  
  public void setPicnb(String pPicnb) {
    if (pPicnb == null) {
      return;
    }
    picnb = pPicnb;
  }
  
  public String getPicnb() {
    return picnb;
  }
  
  public void setPidpl(BigDecimal pPidpl) {
    if (pPidpl == null) {
      return;
    }
    pidpl = pPidpl.setScale(DECIMAL_PIDPL, RoundingMode.HALF_UP);
  }
  
  public void setPidpl(Integer pPidpl) {
    if (pPidpl == null) {
      return;
    }
    pidpl = BigDecimal.valueOf(pPidpl);
  }
  
  public void setPidpl(Date pPidpl) {
    if (pPidpl == null) {
      return;
    }
    pidpl = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidpl));
  }
  
  public Integer getPidpl() {
    return pidpl.intValue();
  }
  
  public Date getPidplConvertiEnDate() {
    return ConvertDate.db2ToDate(pidpl.intValue(), null);
  }
  
  public void setPiin16(Character pPiin16) {
    if (pPiin16 == null) {
      return;
    }
    piin16 = String.valueOf(pPiin16);
  }
  
  public Character getPiin16() {
    return piin16.charAt(0);
  }
  
  public void setPiin17(Character pPiin17) {
    if (pPiin17 == null) {
      return;
    }
    piin17 = String.valueOf(pPiin17);
  }
  
  public Character getPiin17() {
    return piin17.charAt(0);
  }
  
  public void setPicrt(String pPicrt) {
    if (pPicrt == null) {
      return;
    }
    picrt = pPicrt;
  }
  
  public String getPicrt() {
    return picrt;
  }
  
  public void setPioto(BigDecimal pPioto) {
    if (pPioto == null) {
      return;
    }
    pioto = pPioto.setScale(DECIMAL_PIOTO, RoundingMode.HALF_UP);
  }
  
  public void setPioto(Integer pPioto) {
    if (pPioto == null) {
      return;
    }
    pioto = BigDecimal.valueOf(pPioto);
  }
  
  public Integer getPioto() {
    return pioto.intValue();
  }
  
  public void setPican(String pPican) {
    if (pPican == null) {
      return;
    }
    pican = pPican;
  }
  
  public String getPican() {
    return pican;
  }
  
  public void setPiprn(String pPiprn) {
    if (pPiprn == null) {
      return;
    }
    piprn = pPiprn;
  }
  
  public String getPiprn() {
    return piprn;
  }
  
  public void setPirec(Character pPirec) {
    if (pPirec == null) {
      return;
    }
    pirec = String.valueOf(pPirec);
  }
  
  public Character getPirec() {
    return pirec.charAt(0);
  }
  
  public void setPifrc(Character pPifrc) {
    if (pPifrc == null) {
      return;
    }
    pifrc = String.valueOf(pPifrc);
  }
  
  public Character getPifrc() {
    return pifrc.charAt(0);
  }
  
  public void setPimtc(Character pPimtc) {
    if (pPimtc == null) {
      return;
    }
    pimtc = String.valueOf(pPimtc);
  }
  
  public Character getPimtc() {
    return pimtc.charAt(0);
  }
  
  public void setPinip(String pPinip) {
    if (pPinip == null) {
      return;
    }
    pinip = pPinip;
  }
  
  public String getPinip() {
    return pinip;
  }
  
  public void setPinik(String pPinik) {
    if (pPinik == null) {
      return;
    }
    pinik = pPinik;
  }
  
  public String getPinik() {
    return pinik;
  }
  
  public void setPicl1(Character pPicl1) {
    if (pPicl1 == null) {
      return;
    }
    picl1 = String.valueOf(pPicl1);
  }
  
  public Character getPicl1() {
    return picl1.charAt(0);
  }
  
  public void setPicl2(String pPicl2) {
    if (pPicl2 == null) {
      return;
    }
    picl2 = pPicl2;
  }
  
  public String getPicl2() {
    return picl2;
  }
  
  public void setPicl3(String pPicl3) {
    if (pPicl3 == null) {
      return;
    }
    picl3 = pPicl3;
  }
  
  public String getPicl3() {
    return picl3;
  }
  
  public void setPicla(String pPicla) {
    if (pPicla == null) {
      return;
    }
    picla = pPicla;
  }
  
  public String getPicla() {
    return picla;
  }
  
  public void setPiin18(Character pPiin18) {
    if (pPiin18 == null) {
      return;
    }
    piin18 = String.valueOf(pPiin18);
  }
  
  public Character getPiin18() {
    return piin18.charAt(0);
  }
  
  public void setPiin19(Character pPiin19) {
    if (pPiin19 == null) {
      return;
    }
    piin19 = String.valueOf(pPiin19);
  }
  
  public Character getPiin19() {
    return piin19.charAt(0);
  }
  
  public void setPiin20(Character pPiin20) {
    if (pPiin20 == null) {
      return;
    }
    piin20 = String.valueOf(pPiin20);
  }
  
  public Character getPiin20() {
    return piin20.charAt(0);
  }
  
  public void setPiin21(Character pPiin21) {
    if (pPiin21 == null) {
      return;
    }
    piin21 = String.valueOf(pPiin21);
  }
  
  public Character getPiin21() {
    return piin21.charAt(0);
  }
  
  public void setPiin22(Character pPiin22) {
    if (pPiin22 == null) {
      return;
    }
    piin22 = String.valueOf(pPiin22);
  }
  
  public Character getPiin22() {
    return piin22.charAt(0);
  }
  
  public void setPiin23(Character pPiin23) {
    if (pPiin23 == null) {
      return;
    }
    piin23 = String.valueOf(pPiin23);
  }
  
  public Character getPiin23() {
    return piin23.charAt(0);
  }
  
  public void setPiin24(Character pPiin24) {
    if (pPiin24 == null) {
      return;
    }
    piin24 = String.valueOf(pPiin24);
  }
  
  public Character getPiin24() {
    return piin24.charAt(0);
  }
  
  public void setPiin25(Character pPiin25) {
    if (pPiin25 == null) {
      return;
    }
    piin25 = String.valueOf(pPiin25);
  }
  
  public Character getPiin25() {
    return piin25.charAt(0);
  }
  
  public void setPiin26(Character pPiin26) {
    if (pPiin26 == null) {
      return;
    }
    piin26 = String.valueOf(pPiin26);
  }
  
  public Character getPiin26() {
    return piin26.charAt(0);
  }
  
  public void setPiin27(Character pPiin27) {
    if (pPiin27 == null) {
      return;
    }
    piin27 = String.valueOf(pPiin27);
  }
  
  public Character getPiin27() {
    return piin27.charAt(0);
  }
  
  public void setPiapen(String pPiapen) {
    if (pPiapen == null) {
      return;
    }
    piapen = pPiapen;
  }
  
  public String getPiapen() {
    return piapen;
  }
  
  public void setPiplf3(BigDecimal pPiplf3) {
    if (pPiplf3 == null) {
      return;
    }
    piplf3 = pPiplf3.setScale(DECIMAL_PIPLF3, RoundingMode.HALF_UP);
  }
  
  public void setPiplf3(Integer pPiplf3) {
    if (pPiplf3 == null) {
      return;
    }
    piplf3 = BigDecimal.valueOf(pPiplf3);
  }
  
  public void setPiplf3(Date pPiplf3) {
    if (pPiplf3 == null) {
      return;
    }
    piplf3 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPiplf3));
  }
  
  public Integer getPiplf3() {
    return piplf3.intValue();
  }
  
  public Date getPiplf3ConvertiEnDate() {
    return ConvertDate.db2ToDate(piplf3.intValue(), null);
  }
  
  public void setPidpl3(BigDecimal pPidpl3) {
    if (pPidpl3 == null) {
      return;
    }
    pidpl3 = pPidpl3.setScale(DECIMAL_PIDPL3, RoundingMode.HALF_UP);
  }
  
  public void setPidpl3(Integer pPidpl3) {
    if (pPidpl3 == null) {
      return;
    }
    pidpl3 = BigDecimal.valueOf(pPidpl3);
  }
  
  public void setPidpl3(Date pPidpl3) {
    if (pPidpl3 == null) {
      return;
    }
    pidpl3 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidpl3));
  }
  
  public Integer getPidpl3() {
    return pidpl3.intValue();
  }
  
  public Date getPidpl3ConvertiEnDate() {
    return ConvertDate.db2ToDate(pidpl3.intValue(), null);
  }
  
  public void setPiidas(String pPiidas) {
    if (pPiidas == null) {
      return;
    }
    piidas = pPiidas;
  }
  
  public String getPiidas() {
    return piidas;
  }
  
  public void setPicas(String pPicas) {
    if (pPicas == null) {
      return;
    }
    picas = pPicas;
  }
  
  public String getPicas() {
    return picas;
  }
  
  public void setPidvp3(BigDecimal pPidvp3) {
    if (pPidvp3 == null) {
      return;
    }
    pidvp3 = pPidvp3.setScale(DECIMAL_PIDVP3, RoundingMode.HALF_UP);
  }
  
  public void setPidvp3(Integer pPidvp3) {
    if (pPidvp3 == null) {
      return;
    }
    pidvp3 = BigDecimal.valueOf(pPidvp3);
  }
  
  public void setPidvp3(Date pPidvp3) {
    if (pPidvp3 == null) {
      return;
    }
    pidvp3 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidvp3));
  }
  
  public Integer getPidvp3() {
    return pidvp3.intValue();
  }
  
  public Date getPidvp3ConvertiEnDate() {
    return ConvertDate.db2ToDate(pidvp3.intValue(), null);
  }
  
  public void setPidat1(BigDecimal pPidat1) {
    if (pPidat1 == null) {
      return;
    }
    pidat1 = pPidat1.setScale(DECIMAL_PIDAT1, RoundingMode.HALF_UP);
  }
  
  public void setPidat1(Integer pPidat1) {
    if (pPidat1 == null) {
      return;
    }
    pidat1 = BigDecimal.valueOf(pPidat1);
  }
  
  public void setPidat1(Date pPidat1) {
    if (pPidat1 == null) {
      return;
    }
    pidat1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat1));
  }
  
  public Integer getPidat1() {
    return pidat1.intValue();
  }
  
  public Date getPidat1ConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat1.intValue(), null);
  }
  
  public void setPidat2(BigDecimal pPidat2) {
    if (pPidat2 == null) {
      return;
    }
    pidat2 = pPidat2.setScale(DECIMAL_PIDAT2, RoundingMode.HALF_UP);
  }
  
  public void setPidat2(Integer pPidat2) {
    if (pPidat2 == null) {
      return;
    }
    pidat2 = BigDecimal.valueOf(pPidat2);
  }
  
  public void setPidat2(Date pPidat2) {
    if (pPidat2 == null) {
      return;
    }
    pidat2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat2));
  }
  
  public Integer getPidat2() {
    return pidat2.intValue();
  }
  
  public Date getPidat2ConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat2.intValue(), null);
  }
  
  public void setPidat3(BigDecimal pPidat3) {
    if (pPidat3 == null) {
      return;
    }
    pidat3 = pPidat3.setScale(DECIMAL_PIDAT3, RoundingMode.HALF_UP);
  }
  
  public void setPidat3(Integer pPidat3) {
    if (pPidat3 == null) {
      return;
    }
    pidat3 = BigDecimal.valueOf(pPidat3);
  }
  
  public void setPidat3(Date pPidat3) {
    if (pPidat3 == null) {
      return;
    }
    pidat3 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat3));
  }
  
  public Integer getPidat3() {
    return pidat3.intValue();
  }
  
  public Date getPidat3ConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat3.intValue(), null);
  }
  
  public void setPiffp1(BigDecimal pPiffp1) {
    if (pPiffp1 == null) {
      return;
    }
    piffp1 = pPiffp1.setScale(DECIMAL_PIFFP1, RoundingMode.HALF_UP);
  }
  
  public void setPiffp1(Double pPiffp1) {
    if (pPiffp1 == null) {
      return;
    }
    piffp1 = BigDecimal.valueOf(pPiffp1).setScale(DECIMAL_PIFFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiffp1() {
    return piffp1.setScale(DECIMAL_PIFFP1, RoundingMode.HALF_UP);
  }
  
  public void setPiffp2(BigDecimal pPiffp2) {
    if (pPiffp2 == null) {
      return;
    }
    piffp2 = pPiffp2.setScale(DECIMAL_PIFFP2, RoundingMode.HALF_UP);
  }
  
  public void setPiffp2(Double pPiffp2) {
    if (pPiffp2 == null) {
      return;
    }
    piffp2 = BigDecimal.valueOf(pPiffp2).setScale(DECIMAL_PIFFP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiffp2() {
    return piffp2.setScale(DECIMAL_PIFFP2, RoundingMode.HALF_UP);
  }
  
  public void setPiffp3(BigDecimal pPiffp3) {
    if (pPiffp3 == null) {
      return;
    }
    piffp3 = pPiffp3.setScale(DECIMAL_PIFFP3, RoundingMode.HALF_UP);
  }
  
  public void setPiffp3(Double pPiffp3) {
    if (pPiffp3 == null) {
      return;
    }
    piffp3 = BigDecimal.valueOf(pPiffp3).setScale(DECIMAL_PIFFP3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiffp3() {
    return piffp3.setScale(DECIMAL_PIFFP3, RoundingMode.HALF_UP);
  }
  
  public void setPiin28(Character pPiin28) {
    if (pPiin28 == null) {
      return;
    }
    piin28 = String.valueOf(pPiin28);
  }
  
  public Character getPiin28() {
    return piin28.charAt(0);
  }
  
  public void setPiin29(Character pPiin29) {
    if (pPiin29 == null) {
      return;
    }
    piin29 = String.valueOf(pPiin29);
  }
  
  public Character getPiin29() {
    return piin29.charAt(0);
  }
  
  public void setPiin30(Character pPiin30) {
    if (pPiin30 == null) {
      return;
    }
    piin30 = String.valueOf(pPiin30);
  }
  
  public Character getPiin30() {
    return piin30.charAt(0);
  }
  
  public void setPiciv(String pPiciv) {
    if (pPiciv == null) {
      return;
    }
    piciv = pPiciv;
  }
  
  public String getPiciv() {
    return piciv;
  }
  
  public void setPinomc(String pPinomc) {
    if (pPinomc == null) {
      return;
    }
    pinomc = pPinomc;
  }
  
  public String getPinomc() {
    return pinomc;
  }
  
  public void setPipre(String pPipre) {
    if (pPipre == null) {
      return;
    }
    pipre = pPipre;
  }
  
  public String getPipre() {
    return pipre;
  }
  
  public void setPinet(String pPinet) {
    if (pPinet == null) {
      return;
    }
    pinet = pPinet;
  }
  
  public String getPinet() {
    return pinet;
  }
  
  public void setPinet2(String pPinet2) {
    if (pPinet2 == null) {
      return;
    }
    pinet2 = pPinet2;
  }
  
  public String getPinet2() {
    return pinet2;
  }
  
  public void setPitelc(String pPitelc) {
    if (pPitelc == null) {
      return;
    }
    pitelc = pPitelc;
  }
  
  public String getPitelc() {
    return pitelc;
  }
  
  public void setPifaxc(String pPifaxc) {
    if (pPifaxc == null) {
      return;
    }
    pifaxc = pPifaxc;
  }
  
  public String getPifaxc() {
    return pifaxc;
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
