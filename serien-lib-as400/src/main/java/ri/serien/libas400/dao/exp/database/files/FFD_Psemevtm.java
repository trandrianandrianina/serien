/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.outils.dateheure.DateHeure;

/**
 * Classe contenant les zones du PSEMEVTM
 * @author Administrateur
 *
 */
public abstract class FFD_Psemevtm extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_ETID = 9;
  public static final int DECIMAL_ETID = 0;
  public static final int SIZE_ETTYP = 4;
  public static final int DECIMAL_ETTYP = 0;
  public static final int SIZE_ETDCR = 7;
  public static final int DECIMAL_ETDCR = 0;
  public static final int SIZE_ETHCR = 4;
  public static final int DECIMAL_ETHCR = 0;
  public static final int SIZE_ETDCL = 7;
  public static final int DECIMAL_ETDCL = 0;
  public static final int SIZE_ETHCL = 4;
  public static final int DECIMAL_ETHCL = 0;
  public static final int SIZE_ETDRL = 7;
  public static final int DECIMAL_ETDRL = 0;
  public static final int SIZE_ETHRL = 4;
  public static final int DECIMAL_ETHRL = 0;
  public static final int SIZE_ETDRP = 7;
  public static final int DECIMAL_ETDRP = 0;
  public static final int SIZE_ETHRP = 4;
  public static final int DECIMAL_ETHRP = 0;
  public static final int SIZE_ETCODP = 4;
  public static final int DECIMAL_ETCODP = 0;
  public static final int SIZE_ETETA = 4;
  public static final int DECIMAL_ETETA = 0;
  public static final int SIZE_ETTOTP = 9;
  public static final int DECIMAL_ETTOTP = 0;
  public static final int SIZE_ETOBS = 16352;
  public static final int SIZE_ETIDT = 9;
  public static final int DECIMAL_ETIDT = 0;
  public static final int SIZE_ETETB = 3;
  public static final int SIZE_ETIDO = 9;
  public static final int DECIMAL_ETIDO = 0;
  public static final int SIZE_ETSUFT = 9;
  public static final int DECIMAL_ETSUFT = 0;
  public static final int SIZE_ETETBT = 3;
  
  // Variables fichiers
  protected int ETID = 0; // L'ID
  protected byte ETTYP = 0;// TYPE_EVENEMENT; // Type (Actions commerciales, appels téléphoniques, ...)
  protected int ETDCR = 0; // Date de création (saammjj)
  protected int ETHCR = 0; // Heure de création (hhmm)
  protected int ETDCL = 0; // Date de fin (cloture, souhaitée) (saammjj)
  protected int ETHCL = 0; // Heure de fin (hhmm)
  protected int ETDRL = 0; // Date de réalisation (saammjj)
  protected int ETHRL = 0; // Heure de réalisation (hhmm)
  protected int ETDRP = 0; // Date de rappel (saammjj)
  protected int ETHRP = 0; // Heure de rappel (hhmm)
  protected byte ETCODP = 1; // Code priorité <- Potentiellement paramètrable
  protected byte ETETA = 0; // Etat (A traiter/En cours/Traitée/Problème)
  protected int ETTOTP = 0; // Temps passé (en minute)
  protected String ETOBS = null; // Observations
  protected int ETIDT = 0; // L'ID du client/prospect/fournisseur
  protected String ETETB = null; // Code établissement
  protected int ETIDO = 0; // L'ID de l'évènement original (s'il y en a un)
  protected int ETSUFT = 0; // Numéro lié au tiers
  protected String ETETBT = null; // Code établissement lié au tiers
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public FFD_Psemevtm(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    ETID = 0;
    ETTYP = 0; // TYPE_EVENEMENT;
    ETDCR = Integer.parseInt(DateHeure.getJourHeure(DateHeure.SAAMMJJ));
    ETHCR = Integer.parseInt(DateHeure.getJourHeure(DateHeure.HHMM));
    ETDCL = 0;
    ETHCL = 0;
    ETDRL = 0;
    ETHRL = 0;
    ETDRP = 0;
    ETHRP = 0;
    ETCODP = 1; // PRIORITY_NORMAL;
    ETETA = 0; // EVT_TO_TREAT;
    ETTOTP = 0;
    ETOBS = null;
    ETIDT = 0;
    ETETB = "";
    ETIDO = 0;
    ETSUFT = 0;
    ETETBT = null;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le eTID
   */
  public int getETID() {
    return ETID;
  }
  
  /**
   * @param eTID le eTID à définir
   */
  public void setETID(int eTID) {
    ETID = eTID;
  }
  
  /**
   * @return le eTTYP
   */
  public byte getETTYP() {
    return ETTYP;
  }
  
  /**
   * @param eTTYP le eTTYP à définir
   */
  public void setETTYP(byte eTTYP) {
    ETTYP = eTTYP;
  }
  
  /**
   * @return le eTDCR
   */
  public int getETDCR() {
    return ETDCR;
  }
  
  /**
   * @param eTDCR le eTDCR à définir
   */
  public void setETDCR(int eTDCR) {
    ETDCR = eTDCR;
  }
  
  /**
   * @return le eTHCR
   */
  public int getETHCR() {
    return ETHCR;
  }
  
  /**
   * @param eTHCR le eTHCR à définir
   */
  public void setETHCR(int eTHCR) {
    ETHCR = eTHCR;
  }
  
  /**
   * @return le eTDCL
   */
  public int getETDCL() {
    return ETDCL;
  }
  
  /**
   * @param eTDCL le eTDCL à définir
   */
  public void setETDCL(int eTDCL) {
    ETDCL = eTDCL;
  }
  
  /**
   * @return le eTHCL
   */
  public int getETHCL() {
    return ETHCL;
  }
  
  /**
   * @param eTHCL le eTHCL à définir
   */
  public void setETHCL(int eTHCL) {
    ETHCL = eTHCL;
  }
  
  /**
   * @return le eTDRL
   */
  public int getETDRL() {
    return ETDRL;
  }
  
  /**
   * @param eTDRL le eTDRL à définir
   */
  public void setETDRL(int eTDRL) {
    ETDRL = eTDRL;
  }
  
  /**
   * @return le eTHRL
   */
  public int getETHRL() {
    return ETHRL;
  }
  
  /**
   * @param eTHRL le eTHRL à définir
   */
  public void setETHRL(int eTHRL) {
    ETHRL = eTHRL;
  }
  
  /**
   * @return le eTDRP
   */
  public int getETDRP() {
    return ETDRP;
  }
  
  /**
   * @param eTDRP le eTDRP à définir
   */
  public void setETDRP(int eTDRP) {
    ETDRP = eTDRP;
  }
  
  /**
   * @return le eTHRP
   */
  public int getETHRP() {
    return ETHRP;
  }
  
  /**
   * @param eTHRP le eTHRP à définir
   */
  public void setETHRP(int eTHRP) {
    ETHRP = eTHRP;
  }
  
  /**
   * @return le eTCODP
   */
  public byte getETCODP() {
    return ETCODP;
  }
  
  /**
   * @param eTCODP le eTCODP à définir
   */
  public void setETCODP(byte eTCODP) {
    ETCODP = eTCODP;
  }
  
  /**
   * @return le eTETA
   */
  public byte getETETA() {
    return ETETA;
  }
  
  /**
   * @param eTETA le eTETA à définir
   */
  public void setETETA(byte eTETA) {
    ETETA = eTETA;
  }
  
  /**
   * @return le eTTOTP
   */
  public int getETTOTP() {
    return ETTOTP;
  }
  
  /**
   * @param eTTOTP le eTTOTP à définir
   */
  public void setETTOTP(int eTTOTP) {
    ETTOTP = eTTOTP;
  }
  
  /**
   * @return le eTOBS
   */
  public String getETOBS() {
    return ETOBS;
  }
  
  /**
   * @param eTOBS le eTOBS à définir
   */
  public void setETOBS(String eTOBS) {
    ETOBS = eTOBS;
  }
  
  /**
   * @return le eTIDT
   */
  public int getETIDT() {
    return ETIDT;
  }
  
  /**
   * @param eTIDT le eTIDT à définir
   */
  public void setETIDT(int eTIDT) {
    ETIDT = eTIDT;
  }
  
  /**
   * @return le eTETB
   */
  public String getETETB() {
    return ETETB;
  }
  
  /**
   * @param eTETB le eTETB à définir
   */
  public void setETETB(String eTETB) {
    ETETB = eTETB;
  }
  
  /**
   * @return le eTIDO
   */
  public int getETIDO() {
    return ETIDO;
  }
  
  /**
   * @param eTIDO le eTIDO à définir
   */
  public void setETIDO(int eTIDO) {
    ETIDO = eTIDO;
  }
  
  /**
   * @return le eTSUFT
   */
  public int getETSUFT() {
    return ETSUFT;
  }
  
  /**
   * @param eTSUFT le eTSUFT à définir
   */
  public void setETSUFT(int eTSUFT) {
    ETSUFT = eTSUFT;
  }
  
  /**
   * @return le eTETBT
   */
  public String getETETBT() {
    return ETETBT;
  }
  
  /**
   * @param eTETBT le eTETBT à définir
   */
  public void setETETBT(String eTETBT) {
    ETETBT = eTETBT;
  }
  
}
