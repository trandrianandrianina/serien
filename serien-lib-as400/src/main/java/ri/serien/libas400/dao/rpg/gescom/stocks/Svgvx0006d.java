/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.stocks;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvx0006d {
  // Constantes
  public static final int SIZE_WETB = 3;
  public static final int SIZE_WART = 20;
  public static final int SIZE_WLIB = 30;
  public static final int SIZE_WMAG = 2;
  public static final int SIZE_WUNS = 2;
  public static final int SIZE_WDCS = 1;
  public static final int DECIMAL_WDCS = 0;
  public static final int SIZE_WTYP = 1;
  public static final int DECIMAL_WTYP = 0;
  public static final int SIZE_WORI = 1;
  public static final int SIZE_WCOD = 1;
  public static final int SIZE_WNUM = 6;
  public static final int DECIMAL_WNUM = 0;
  public static final int SIZE_WSUF = 1;
  public static final int DECIMAL_WSUF = 0;
  public static final int SIZE_WNLI = 4;
  public static final int DECIMAL_WNLI = 0;
  public static final int SIZE_WCOF = 1;
  public static final int DECIMAL_WCOF = 0;
  public static final int SIZE_WFRS = 6;
  public static final int DECIMAL_WFRS = 0;
  public static final int SIZE_WEXT = 4;
  public static final int DECIMAL_WEXT = 0;
  public static final int SIZE_WCLI = 6;
  public static final int DECIMAL_WCLI = 0;
  public static final int SIZE_WLIV = 3;
  public static final int DECIMAL_WLIV = 0;
  public static final int SIZE_WNOM = 30;
  public static final int SIZE_WHOM = 7;
  public static final int DECIMAL_WHOM = 0;
  public static final int SIZE_WDLP = 7;
  public static final int DECIMAL_WDLP = 0;
  public static final int SIZE_WDLS = 7;
  public static final int DECIMAL_WDLS = 0;
  public static final int SIZE_WDLC = 7;
  public static final int DECIMAL_WDLC = 0;
  public static final int SIZE_WDRES = 7;
  public static final int DECIMAL_WDRES = 0;
  public static final int SIZE_WQTEUS = 11;
  public static final int DECIMAL_WQTEUS = 3;
  public static final int SIZE_WQTSTK = 11;
  public static final int DECIMAL_WQTSTK = 3;
  public static final int SIZE_ALAORI = 1;
  public static final int SIZE_ALACOD = 1;
  public static final int SIZE_ALANUM = 6;
  public static final int DECIMAL_ALANUM = 0;
  public static final int SIZE_ALASUF = 1;
  public static final int DECIMAL_ALASUF = 0;
  public static final int SIZE_ALANLI = 4;
  public static final int DECIMAL_ALANLI = 0;
  public static final int SIZE_ALAETA = 1;
  public static final int DECIMAL_ALAETA = 0;
  public static final int SIZE_ALAQTE = 11;
  public static final int DECIMAL_ALAQTE = 3;
  public static final int SIZE_ALACOF = 1;
  public static final int DECIMAL_ALACOF = 0;
  public static final int SIZE_ALAFRS = 6;
  public static final int DECIMAL_ALAFRS = 0;
  public static final int SIZE_ALAEXT = 4;
  public static final int DECIMAL_ALAEXT = 0;
  public static final int SIZE_ALACLI = 6;
  public static final int DECIMAL_ALACLI = 0;
  public static final int SIZE_ALALIV = 3;
  public static final int DECIMAL_ALALIV = 0;
  public static final int SIZE_ALANOM = 30;
  public static final int SIZE_WNOMFAC = 30;
  public static final int SIZE_TOTALE_DS = 284;
  public static final int SIZE_FILLER = 300 - 284;
  
  // Constantes indices Nom DS
  public static final int VAR_WETB = 0;
  public static final int VAR_WART = 1;
  public static final int VAR_WLIB = 2;
  public static final int VAR_WMAG = 3;
  public static final int VAR_WUNS = 4;
  public static final int VAR_WDCS = 5;
  public static final int VAR_WTYP = 6;
  public static final int VAR_WORI = 7;
  public static final int VAR_WCOD = 8;
  public static final int VAR_WNUM = 9;
  public static final int VAR_WSUF = 10;
  public static final int VAR_WNLI = 11;
  public static final int VAR_WCOF = 12;
  public static final int VAR_WFRS = 13;
  public static final int VAR_WEXT = 14;
  public static final int VAR_WCLI = 15;
  public static final int VAR_WLIV = 16;
  public static final int VAR_WNOM = 17;
  public static final int VAR_WHOM = 18;
  public static final int VAR_WDLP = 19;
  public static final int VAR_WDLS = 20;
  public static final int VAR_WDLC = 21;
  public static final int VAR_WDRES = 22;
  public static final int VAR_WQTEUS = 23;
  public static final int VAR_WQTSTK = 24;
  public static final int VAR_ALAORI = 25;
  public static final int VAR_ALACOD = 26;
  public static final int VAR_ALANUM = 27;
  public static final int VAR_ALASUF = 28;
  public static final int VAR_ALANLI = 29;
  public static final int VAR_ALAETA = 30;
  public static final int VAR_ALAQTE = 31;
  public static final int VAR_ALACOF = 32;
  public static final int VAR_ALAFRS = 33;
  public static final int VAR_ALAEXT = 34;
  public static final int VAR_ALACLI = 35;
  public static final int VAR_ALALIV = 36;
  public static final int VAR_ALANOM = 37;
  public static final int VAR_WNOMFAC = 38;
  
  // Variables AS400
  private String wetb = ""; //
  private String wart = ""; // Code Article
  private String wlib = ""; // Désignation article
  private String wmag = ""; // Code Magasin
  private String wuns = ""; // Unité de stock
  private BigDecimal wdcs = BigDecimal.ZERO; // Top décimalisation
  private BigDecimal wtyp = BigDecimal.ZERO; // "2", "3"=Attendu FRS,Cde CLI
  private String wori = ""; // "A" ,"V" "O" ACHAT,VENTE, OF
  private String wcod = ""; // Code erl
  private BigDecimal wnum = BigDecimal.ZERO; // Numéro de bon
  private BigDecimal wsuf = BigDecimal.ZERO; // suffixe bon
  private BigDecimal wnli = BigDecimal.ZERO; // N° de ligne
  private BigDecimal wcof = BigDecimal.ZERO; // collectif fournisseur
  private BigDecimal wfrs = BigDecimal.ZERO; // Fournisseur
  private BigDecimal wext = BigDecimal.ZERO; // adresse commande frs
  private BigDecimal wcli = BigDecimal.ZERO; // numéro client livré
  private BigDecimal wliv = BigDecimal.ZERO; // suffixe client livré
  private String wnom = ""; // Nom Tiers client ou frs
  private BigDecimal whom = BigDecimal.ZERO; // Date validité
  private BigDecimal wdlp = BigDecimal.ZERO; // Date Livraison prévue
  private BigDecimal wdls = BigDecimal.ZERO; // Date Livraison souhaitée
  private BigDecimal wdlc = BigDecimal.ZERO; // Date confirmation (attendu)
  private BigDecimal wdres = BigDecimal.ZERO; // Date reservation (commandé)
  private BigDecimal wqteus = BigDecimal.ZERO; // Qté attendue ou commandée US
  private BigDecimal wqtstk = BigDecimal.ZERO; // Qté stock glissé
  private String alaori = ""; // "A" ,"V" "O" ACHAT,VENTE, OF
  private String alacod = ""; // Code erl
  private BigDecimal alanum = BigDecimal.ZERO; // Numéro de bon
  private BigDecimal alasuf = BigDecimal.ZERO; // suffixe bon
  private BigDecimal alanli = BigDecimal.ZERO; // N° de ligne
  private BigDecimal alaeta = BigDecimal.ZERO; // code état document lié
  private BigDecimal alaqte = BigDecimal.ZERO; // Quantité affectée US
  private BigDecimal alacof = BigDecimal.ZERO; // collectif fournisseur
  private BigDecimal alafrs = BigDecimal.ZERO; // N°Tiers (CLI/FRS)
  private BigDecimal alaext = BigDecimal.ZERO; // adresse commande frs
  private BigDecimal alacli = BigDecimal.ZERO; // numéro client livré
  private BigDecimal alaliv = BigDecimal.ZERO; // suffixe client livré
  private String alanom = ""; // nom document tiers
  private String wnomfac = ""; // Nom du client facturé
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WETB), //
      new AS400Text(SIZE_WART), // Code Article
      new AS400Text(SIZE_WLIB), // Désignation article
      new AS400Text(SIZE_WMAG), // Code Magasin
      new AS400Text(SIZE_WUNS), // Unité de stock
      new AS400ZonedDecimal(SIZE_WDCS, DECIMAL_WDCS), // Top décimalisation
      new AS400ZonedDecimal(SIZE_WTYP, DECIMAL_WTYP), // "2", "3"=Attendu FRS,Cde CLI
      new AS400Text(SIZE_WORI), // "A" ,"V" "O" ACHAT,VENTE, OF
      new AS400Text(SIZE_WCOD), // Code erl
      new AS400ZonedDecimal(SIZE_WNUM, DECIMAL_WNUM), // Numéro de bon
      new AS400ZonedDecimal(SIZE_WSUF, DECIMAL_WSUF), // suffixe bon
      new AS400ZonedDecimal(SIZE_WNLI, DECIMAL_WNLI), // N° de ligne
      new AS400ZonedDecimal(SIZE_WCOF, DECIMAL_WCOF), // collectif fournisseur
      new AS400ZonedDecimal(SIZE_WFRS, DECIMAL_WFRS), // Fournisseur
      new AS400ZonedDecimal(SIZE_WEXT, DECIMAL_WEXT), // adresse commande frs
      new AS400ZonedDecimal(SIZE_WCLI, DECIMAL_WCLI), // numéro client livré
      new AS400ZonedDecimal(SIZE_WLIV, DECIMAL_WLIV), // suffixe client livré
      new AS400Text(SIZE_WNOM), // Nom Tiers client ou frs
      new AS400ZonedDecimal(SIZE_WHOM, DECIMAL_WHOM), // Date validité
      new AS400ZonedDecimal(SIZE_WDLP, DECIMAL_WDLP), // Date Livraison prévue
      new AS400ZonedDecimal(SIZE_WDLS, DECIMAL_WDLS), // Date Livraison souhaitée
      new AS400ZonedDecimal(SIZE_WDLC, DECIMAL_WDLC), // Date confirmation (attendu)
      new AS400ZonedDecimal(SIZE_WDRES, DECIMAL_WDRES), // Date reservation (commandé)
      new AS400ZonedDecimal(SIZE_WQTEUS, DECIMAL_WQTEUS), // Qté attendue ou commandée US
      new AS400ZonedDecimal(SIZE_WQTSTK, DECIMAL_WQTSTK), // Qté stock glissé
      new AS400Text(SIZE_ALAORI), // "A" ,"V" "O" ACHAT,VENTE, OF
      new AS400Text(SIZE_ALACOD), // Code erl
      new AS400ZonedDecimal(SIZE_ALANUM, DECIMAL_ALANUM), // Numéro de bon
      new AS400ZonedDecimal(SIZE_ALASUF, DECIMAL_ALASUF), // suffixe bon
      new AS400ZonedDecimal(SIZE_ALANLI, DECIMAL_ALANLI), // N° de ligne
      new AS400ZonedDecimal(SIZE_ALAETA, DECIMAL_ALAETA), // code état document lié
      new AS400ZonedDecimal(SIZE_ALAQTE, DECIMAL_ALAQTE), // Quantité affectée US
      new AS400ZonedDecimal(SIZE_ALACOF, DECIMAL_ALACOF), // collectif fournisseur
      new AS400ZonedDecimal(SIZE_ALAFRS, DECIMAL_ALAFRS), // N°Tiers (CLI/FRS)
      new AS400ZonedDecimal(SIZE_ALAEXT, DECIMAL_ALAEXT), // adresse commande frs
      new AS400ZonedDecimal(SIZE_ALACLI, DECIMAL_ALACLI), // numéro client livré
      new AS400ZonedDecimal(SIZE_ALALIV, DECIMAL_ALALIV), // suffixe client livré
      new AS400Text(SIZE_ALANOM), // nom document tiers
      new AS400Text(SIZE_WNOMFAC), // Nom du client facturé
      new AS400Text(SIZE_FILLER), // Filler
  };
  private AS400Structure ds = new AS400Structure(structure);
  public Object[] o = { wetb, wart, wlib, wmag, wuns, wdcs, wtyp, wori, wcod, wnum, wsuf, wnli, wcof, wfrs, wext, wcli, wliv, wnom, whom,
      wdlp, wdls, wdlc, wdres, wqteus, wqtstk, alaori, alacod, alanum, alasuf, alanli, alaeta, alaqte, alacof, alafrs, alaext, alacli,
      alaliv, alanom, wnomfac, filler, };
  
  // -- Accesseurs
  
  public void setWetb(String pWetb) {
    if (pWetb == null) {
      return;
    }
    wetb = pWetb;
  }
  
  public String getWetb() {
    return wetb;
  }
  
  public void setWart(String pWart) {
    if (pWart == null) {
      return;
    }
    wart = pWart;
  }
  
  public String getWart() {
    return wart;
  }
  
  public void setWlib(String pWlib) {
    if (pWlib == null) {
      return;
    }
    wlib = pWlib;
  }
  
  public String getWlib() {
    return wlib;
  }
  
  public void setWmag(String pWmag) {
    if (pWmag == null) {
      return;
    }
    wmag = pWmag;
  }
  
  public String getWmag() {
    return wmag;
  }
  
  public void setWuns(String pWuns) {
    if (pWuns == null) {
      return;
    }
    wuns = pWuns;
  }
  
  public String getWuns() {
    return wuns;
  }
  
  public void setWdcs(BigDecimal pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = pWdcs.setScale(DECIMAL_WDCS, RoundingMode.HALF_UP);
  }
  
  public void setWdcs(Integer pWdcs) {
    if (pWdcs == null) {
      return;
    }
    wdcs = BigDecimal.valueOf(pWdcs);
  }
  
  public Integer getWdcs() {
    return wdcs.intValue();
  }
  
  public void setWtyp(BigDecimal pWtyp) {
    if (pWtyp == null) {
      return;
    }
    wtyp = pWtyp.setScale(DECIMAL_WTYP, RoundingMode.HALF_UP);
  }
  
  public void setWtyp(Integer pWtyp) {
    if (pWtyp == null) {
      return;
    }
    wtyp = BigDecimal.valueOf(pWtyp);
  }
  
  public Integer getWtyp() {
    return wtyp.intValue();
  }
  
  public void setWori(Character pWori) {
    if (pWori == null) {
      return;
    }
    wori = String.valueOf(pWori);
  }
  
  public Character getWori() {
    return wori.charAt(0);
  }
  
  public void setWcod(Character pWcod) {
    if (pWcod == null) {
      return;
    }
    wcod = String.valueOf(pWcod);
  }
  
  public Character getWcod() {
    return wcod.charAt(0);
  }
  
  public void setWnum(BigDecimal pWnum) {
    if (pWnum == null) {
      return;
    }
    wnum = pWnum.setScale(DECIMAL_WNUM, RoundingMode.HALF_UP);
  }
  
  public void setWnum(Integer pWnum) {
    if (pWnum == null) {
      return;
    }
    wnum = BigDecimal.valueOf(pWnum);
  }
  
  public Integer getWnum() {
    return wnum.intValue();
  }
  
  public void setWsuf(BigDecimal pWsuf) {
    if (pWsuf == null) {
      return;
    }
    wsuf = pWsuf.setScale(DECIMAL_WSUF, RoundingMode.HALF_UP);
  }
  
  public void setWsuf(Integer pWsuf) {
    if (pWsuf == null) {
      return;
    }
    wsuf = BigDecimal.valueOf(pWsuf);
  }
  
  public Integer getWsuf() {
    return wsuf.intValue();
  }
  
  public void setWnli(BigDecimal pWnli) {
    if (pWnli == null) {
      return;
    }
    wnli = pWnli.setScale(DECIMAL_WNLI, RoundingMode.HALF_UP);
  }
  
  public void setWnli(Integer pWnli) {
    if (pWnli == null) {
      return;
    }
    wnli = BigDecimal.valueOf(pWnli);
  }
  
  public Integer getWnli() {
    return wnli.intValue();
  }
  
  public void setWcof(BigDecimal pWcof) {
    if (pWcof == null) {
      return;
    }
    wcof = pWcof.setScale(DECIMAL_WCOF, RoundingMode.HALF_UP);
  }
  
  public void setWcof(Integer pWcof) {
    if (pWcof == null) {
      return;
    }
    wcof = BigDecimal.valueOf(pWcof);
  }
  
  public Integer getWcof() {
    return wcof.intValue();
  }
  
  public void setWfrs(BigDecimal pWfrs) {
    if (pWfrs == null) {
      return;
    }
    wfrs = pWfrs.setScale(DECIMAL_WFRS, RoundingMode.HALF_UP);
  }
  
  public void setWfrs(Integer pWfrs) {
    if (pWfrs == null) {
      return;
    }
    wfrs = BigDecimal.valueOf(pWfrs);
  }
  
  public Integer getWfrs() {
    return wfrs.intValue();
  }
  
  public void setWext(BigDecimal pWext) {
    if (pWext == null) {
      return;
    }
    wext = pWext.setScale(DECIMAL_WEXT, RoundingMode.HALF_UP);
  }
  
  public void setWext(Integer pWext) {
    if (pWext == null) {
      return;
    }
    wext = BigDecimal.valueOf(pWext);
  }
  
  public Integer getWext() {
    return wext.intValue();
  }
  
  public void setWcli(BigDecimal pWcli) {
    if (pWcli == null) {
      return;
    }
    wcli = pWcli.setScale(DECIMAL_WCLI, RoundingMode.HALF_UP);
  }
  
  public void setWcli(Integer pWcli) {
    if (pWcli == null) {
      return;
    }
    wcli = BigDecimal.valueOf(pWcli);
  }
  
  public Integer getWcli() {
    return wcli.intValue();
  }
  
  public void setWliv(BigDecimal pWliv) {
    if (pWliv == null) {
      return;
    }
    wliv = pWliv.setScale(DECIMAL_WLIV, RoundingMode.HALF_UP);
  }
  
  public void setWliv(Integer pWliv) {
    if (pWliv == null) {
      return;
    }
    wliv = BigDecimal.valueOf(pWliv);
  }
  
  public Integer getWliv() {
    return wliv.intValue();
  }
  
  public void setWnom(String pWnom) {
    if (pWnom == null) {
      return;
    }
    wnom = pWnom;
  }
  
  public String getWnom() {
    return wnom;
  }
  
  public void setWhom(BigDecimal pWhom) {
    if (pWhom == null) {
      return;
    }
    whom = pWhom.setScale(DECIMAL_WHOM, RoundingMode.HALF_UP);
  }
  
  public void setWhom(Integer pWhom) {
    if (pWhom == null) {
      return;
    }
    whom = BigDecimal.valueOf(pWhom);
  }
  
  public void setWhom(Date pWhom) {
    if (pWhom == null) {
      return;
    }
    whom = BigDecimal.valueOf(ConvertDate.dateToDb2(pWhom));
  }
  
  public Integer getWhom() {
    return whom.intValue();
  }
  
  public Date getWhomConvertiEnDate() {
    return ConvertDate.db2ToDate(whom.intValue(), null);
  }
  
  public void setWdlp(BigDecimal pWdlp) {
    if (pWdlp == null) {
      return;
    }
    wdlp = pWdlp.setScale(DECIMAL_WDLP, RoundingMode.HALF_UP);
  }
  
  public void setWdlp(Integer pWdlp) {
    if (pWdlp == null) {
      return;
    }
    wdlp = BigDecimal.valueOf(pWdlp);
  }
  
  public void setWdlp(Date pWdlp) {
    if (pWdlp == null) {
      return;
    }
    wdlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdlp));
  }
  
  public Integer getWdlp() {
    return wdlp.intValue();
  }
  
  public Date getWdlpConvertiEnDate() {
    return ConvertDate.db2ToDate(wdlp.intValue(), null);
  }
  
  public void setWdls(BigDecimal pWdls) {
    if (pWdls == null) {
      return;
    }
    wdls = pWdls.setScale(DECIMAL_WDLS, RoundingMode.HALF_UP);
  }
  
  public void setWdls(Integer pWdls) {
    if (pWdls == null) {
      return;
    }
    wdls = BigDecimal.valueOf(pWdls);
  }
  
  public void setWdls(Date pWdls) {
    if (pWdls == null) {
      return;
    }
    wdls = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdls));
  }
  
  public Integer getWdls() {
    return wdls.intValue();
  }
  
  public Date getWdlsConvertiEnDate() {
    return ConvertDate.db2ToDate(wdls.intValue(), null);
  }
  
  public void setWdlc(BigDecimal pWdlc) {
    if (pWdlc == null) {
      return;
    }
    wdlc = pWdlc.setScale(DECIMAL_WDLC, RoundingMode.HALF_UP);
  }
  
  public void setWdlc(Integer pWdlc) {
    if (pWdlc == null) {
      return;
    }
    wdlc = BigDecimal.valueOf(pWdlc);
  }
  
  public void setWdlc(Date pWdlc) {
    if (pWdlc == null) {
      return;
    }
    wdlc = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdlc));
  }
  
  public Integer getWdlc() {
    return wdlc.intValue();
  }
  
  public Date getWdlcConvertiEnDate() {
    return ConvertDate.db2ToDate(wdlc.intValue(), null);
  }
  
  public void setWdres(BigDecimal pWdres) {
    if (pWdres == null) {
      return;
    }
    wdres = pWdres.setScale(DECIMAL_WDRES, RoundingMode.HALF_UP);
  }
  
  public void setWdres(Integer pWdres) {
    if (pWdres == null) {
      return;
    }
    wdres = BigDecimal.valueOf(pWdres);
  }
  
  public void setWdres(Date pWdres) {
    if (pWdres == null) {
      return;
    }
    wdres = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdres));
  }
  
  public Integer getWdres() {
    return wdres.intValue();
  }
  
  public Date getWdresConvertiEnDate() {
    return ConvertDate.db2ToDate(wdres.intValue(), null);
  }
  
  public void setWqteus(BigDecimal pWqteus) {
    if (pWqteus == null) {
      return;
    }
    wqteus = pWqteus.setScale(DECIMAL_WQTEUS, RoundingMode.HALF_UP);
  }
  
  public void setWqteus(Double pWqteus) {
    if (pWqteus == null) {
      return;
    }
    wqteus = BigDecimal.valueOf(pWqteus).setScale(DECIMAL_WQTEUS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqteus() {
    return wqteus.setScale(DECIMAL_WQTEUS, RoundingMode.HALF_UP);
  }
  
  public void setWqtstk(BigDecimal pWqtstk) {
    if (pWqtstk == null) {
      return;
    }
    wqtstk = pWqtstk.setScale(DECIMAL_WQTSTK, RoundingMode.HALF_UP);
  }
  
  public void setWqtstk(Double pWqtstk) {
    if (pWqtstk == null) {
      return;
    }
    wqtstk = BigDecimal.valueOf(pWqtstk).setScale(DECIMAL_WQTSTK, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqtstk() {
    return wqtstk.setScale(DECIMAL_WQTSTK, RoundingMode.HALF_UP);
  }
  
  public void setAlaori(Character pAlaori) {
    if (pAlaori == null) {
      return;
    }
    alaori = String.valueOf(pAlaori);
  }
  
  public Character getAlaori() {
    return alaori.charAt(0);
  }
  
  public void setAlacod(Character pAlacod) {
    if (pAlacod == null) {
      return;
    }
    alacod = String.valueOf(pAlacod);
  }
  
  public Character getAlacod() {
    return alacod.charAt(0);
  }
  
  public void setAlanum(BigDecimal pAlanum) {
    if (pAlanum == null) {
      return;
    }
    alanum = pAlanum.setScale(DECIMAL_ALANUM, RoundingMode.HALF_UP);
  }
  
  public void setAlanum(Integer pAlanum) {
    if (pAlanum == null) {
      return;
    }
    alanum = BigDecimal.valueOf(pAlanum);
  }
  
  public Integer getAlanum() {
    return alanum.intValue();
  }
  
  public void setAlasuf(BigDecimal pAlasuf) {
    if (pAlasuf == null) {
      return;
    }
    alasuf = pAlasuf.setScale(DECIMAL_ALASUF, RoundingMode.HALF_UP);
  }
  
  public void setAlasuf(Integer pAlasuf) {
    if (pAlasuf == null) {
      return;
    }
    alasuf = BigDecimal.valueOf(pAlasuf);
  }
  
  public Integer getAlasuf() {
    return alasuf.intValue();
  }
  
  public void setAlanli(BigDecimal pAlanli) {
    if (pAlanli == null) {
      return;
    }
    alanli = pAlanli.setScale(DECIMAL_ALANLI, RoundingMode.HALF_UP);
  }
  
  public void setAlanli(Integer pAlanli) {
    if (pAlanli == null) {
      return;
    }
    alanli = BigDecimal.valueOf(pAlanli);
  }
  
  public Integer getAlanli() {
    return alanli.intValue();
  }
  
  public void setAlaeta(BigDecimal pAlaeta) {
    if (pAlaeta == null) {
      return;
    }
    alaeta = pAlaeta.setScale(DECIMAL_ALAETA, RoundingMode.HALF_UP);
  }
  
  public void setAlaeta(Integer pAlaeta) {
    if (pAlaeta == null) {
      return;
    }
    alaeta = BigDecimal.valueOf(pAlaeta);
  }
  
  public Integer getAlaeta() {
    return alaeta.intValue();
  }
  
  public void setAlaqte(BigDecimal pAlaqte) {
    if (pAlaqte == null) {
      return;
    }
    alaqte = pAlaqte.setScale(DECIMAL_ALAQTE, RoundingMode.HALF_UP);
  }
  
  public void setAlaqte(Double pAlaqte) {
    if (pAlaqte == null) {
      return;
    }
    alaqte = BigDecimal.valueOf(pAlaqte).setScale(DECIMAL_ALAQTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getAlaqte() {
    return alaqte.setScale(DECIMAL_ALAQTE, RoundingMode.HALF_UP);
  }
  
  public void setAlacof(BigDecimal pAlacof) {
    if (pAlacof == null) {
      return;
    }
    alacof = pAlacof.setScale(DECIMAL_ALACOF, RoundingMode.HALF_UP);
  }
  
  public void setAlacof(Integer pAlacof) {
    if (pAlacof == null) {
      return;
    }
    alacof = BigDecimal.valueOf(pAlacof);
  }
  
  public Integer getAlacof() {
    return alacof.intValue();
  }
  
  public void setAlafrs(BigDecimal pAlafrs) {
    if (pAlafrs == null) {
      return;
    }
    alafrs = pAlafrs.setScale(DECIMAL_ALAFRS, RoundingMode.HALF_UP);
  }
  
  public void setAlafrs(Integer pAlafrs) {
    if (pAlafrs == null) {
      return;
    }
    alafrs = BigDecimal.valueOf(pAlafrs);
  }
  
  public Integer getAlafrs() {
    return alafrs.intValue();
  }
  
  public void setAlaext(BigDecimal pAlaext) {
    if (pAlaext == null) {
      return;
    }
    alaext = pAlaext.setScale(DECIMAL_ALAEXT, RoundingMode.HALF_UP);
  }
  
  public void setAlaext(Integer pAlaext) {
    if (pAlaext == null) {
      return;
    }
    alaext = BigDecimal.valueOf(pAlaext);
  }
  
  public Integer getAlaext() {
    return alaext.intValue();
  }
  
  public void setAlacli(BigDecimal pAlacli) {
    if (pAlacli == null) {
      return;
    }
    alacli = pAlacli.setScale(DECIMAL_ALACLI, RoundingMode.HALF_UP);
  }
  
  public void setAlacli(Integer pAlacli) {
    if (pAlacli == null) {
      return;
    }
    alacli = BigDecimal.valueOf(pAlacli);
  }
  
  public Integer getAlacli() {
    return alacli.intValue();
  }
  
  public void setAlaliv(BigDecimal pAlaliv) {
    if (pAlaliv == null) {
      return;
    }
    alaliv = pAlaliv.setScale(DECIMAL_ALALIV, RoundingMode.HALF_UP);
  }
  
  public void setAlaliv(Integer pAlaliv) {
    if (pAlaliv == null) {
      return;
    }
    alaliv = BigDecimal.valueOf(pAlaliv);
  }
  
  public Integer getAlaliv() {
    return alaliv.intValue();
  }
  
  public void setAlanom(String pAlanom) {
    if (pAlanom == null) {
      return;
    }
    alanom = pAlanom;
  }
  
  public String getAlanom() {
    return alanom;
  }
  
  public void setWnomfac(String pWnomfac) {
    if (pWnomfac == null) {
      return;
    }
    wnomfac = pWnomfac;
  }
  
  public String getWnomfac() {
    return wnomfac;
  }
}
