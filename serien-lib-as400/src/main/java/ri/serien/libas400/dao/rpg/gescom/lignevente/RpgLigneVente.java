/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.dao.sql.gescom.prixvente.SqlPrixVente;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.motifretour.IdMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.IdTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.PrixFlash;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.EnumOriginePrixVente;
import ri.serien.libcommun.gescom.vente.ligne.EnumTypeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneRetourArticle;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.Regroupement;
import ri.serien.libcommun.gescom.vente.prixvente.OutilCalculPrix;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.EnumTypeChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.InformationConditionVenteLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceCoefficient;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Centralise les accès aux programmes RPG permettant de manipuler les lignes des documents de ventes.
 */
public class RpgLigneVente extends RpgBase {
  // Constantes
  private static final String SVGVM0004_CONTROLER_LIGNE_VENTE = "SVGVM0004";
  private static final String SVGVM0005_INITIALISER_LIGNE_VENTE = "SVGVM0005";
  private static final String SVGVM0009_SAUVER_LIGNE_VENTE = "SVGVM0009";
  private static final String SVGVM0014_CHARGER_LISTE_LIGNE_VENTE = "SVGVM0014";
  private static final String SVGVM0027_MODIFIER_LIGNE_VENTE = "SVGVM0027";
  private static final String SVGVM0028_SAUVER_LIGNE_VENTE_GROUPE = "SVGVM0028";
  private static final String SVGVM0032_CHARGER_LIGNE_VENTE_GROUPE = "SVGVM0032";
  private static final String SVGVM0033_SUPPRIMER_LIGNE_VENTE_GROUPE = "SVGVM0033";
  private static final String SVGVM0045_SAUVER_LIGNE_VENTE_AVEC_EXTRACTION = "SVGVM0045";
  private static final String SVGVM0049_SAUVER_LIGNE_VENTE_COMMENTAIRE = "SVGVM0049";
  private static final String SVGVM0050_CHARGER_LIGNE_VENTE_COMMENTAIRE = "SVGVM0050";
  private static final String SVGVM0054_CHARGER_LISTE_LIGNE_VENTE_RETOUR = "SVGVM0054";
  private static final String SVGVM0055_SAUVER_LIGNE_VENTE_RETOUR = "SVGVM0055";
  
  private static final int LONGUEUR_LIBELLE = 30;
  
  private IdSession idSession = null;
  
  /**
   * Constructeur.
   */
  public RpgLigneVente(IdSession pIdSession) {
    idSession = pIdSession;
  }
  
  /**
   * Charger les lignes du document de ventes correspondant aux critères de recherche.
   */
  public ListeLigneVente chargerListeLigneVente(SystemeManager pSystemManager, IdDocumentVente pIdDocumentVente,
      CritereLigneVente pCritereLigneVente, boolean isTTC) {
    // Vérifier les pré-requis
    if (pSystemManager == null) {
      throw new MessageErreurException("Impossible de charger la liste des lignes de vente car les paramètres sont invalides.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparer le résultat
    ListeLigneVente listeLigneVente = new ListeLigneVente();
    
    // Charger les lignes 15 par 15
    int page = 1;
    boolean continuer = true;
    while (continuer) {
      continuer = chargerPlage15LigneVente(pSystemManager, pIdDocumentVente, pCritereLigneVente, isTTC, listeLigneVente, page++);
    }
    
    return listeLigneVente;
  }
  
  /**
   * Charger une page de 15 lignes de ventes.
   * Le numéro de page permet de déterminer la plage de 15 lignes à charger.
   */
  private boolean chargerPlage15LigneVente(SystemeManager pSystemManager, IdDocumentVente pIdDocumentVente,
      CritereLigneVente pCritereLigneVente, boolean isTTC, ListeLigneVente pListeLigneVente, int pPage) {
    // Vérifier les pré-requis
    if (pPage >= 1000) {
      throw new MessageErreurException(
          "Le nombre de pages maximum a été atteinte pour le programme " + SVGVM0014_CHARGER_LISTE_LIGNE_VENTE);
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0014i entree = new Svgvm0014i();
    Svgvm0014o sortie = new Svgvm0014o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialiser les paramètres d'entrée
    entree.setPiind("");
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    if (pCritereLigneVente != null) {
      entree.setPinli((int) pCritereLigneVente.getNumeroLigne());
    }
    entree.setPipag(pPage);
    
    // Initialiser des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparer de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSystemManager);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécuter le programme RPG
    if (!rpg.executerProgramme(SVGVM0014_CHARGER_LISTE_LIGNE_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors du chargement des lignes du document de ventes : " + pIdDocumentVente);
    }
    
    // Récupérer les paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Vérifier la présence d'erreurs
    controlerErreur();
    
    // Interpréter le résultat
    Object[] lignes = sortie.getValeursBrutesLignes();
    
    // Parcourir le résultat
    boolean continuer = true;
    int i = 0;
    while (i < lignes.length) {
      Object[] listeObject = (Object[]) lignes[i++];
      
      // Stopper à la première ligne vide
      if (listeObject == null) {
        continuer = false;
        break;
      }
      
      // Générer la ligne de ventes
      LigneVente ligneVente = traiterUneLigne(listeObject, isTTC, pIdDocumentVente);
      pListeLigneVente.add(ligneVente);
    }
    
    // Vérifier s'il reste des lignes de données derrière (ce qui n'est pas normal)
    while (i < lignes.length) {
      Object[] listeObject = (Object[]) lignes[i++];
      if (listeObject != null) {
        throw new MessageErreurException(String.format("Erreur lors du chargement de la ligne de vente %d du document %s.",
            pListeLigneVente.size(), pIdDocumentVente.toString()));
      }
    }
    
    return continuer;
  }
  
  /**
   * Découpage d'une ligne.
   */
  private LigneVente traiterUneLigne(Object[] ligneBrute, boolean pIsTTC, IdDocumentVente pIdDocumentVente) {
    if (ligneBrute == null) {
      return null;
    }
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) ligneBrute[Svgvm0014d.VAR_L1ETB]);
    // On génère l'Id de la ligne de vente
    EnumCodeEnteteDocumentVente codeEntete =
        EnumCodeEnteteDocumentVente.valueOfByCode(((String) ligneBrute[Svgvm0014d.VAR_L1COD]).charAt(0));
    Integer numero = ((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1NUM]).intValue();
    Integer suffixe = ((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1SUF]).intValue();
    Integer numeroLigne = ((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1NLI]).intValue();
    IdLigneVente idLigneVente = IdLigneVente.getInstance(idEtablissement, codeEntete, numero, suffixe, numeroLigne);
    LigneVente ligneVente = new LigneVente(idLigneVente);
    
    ligneVente.setIdDocumentVente(pIdDocumentVente);
    // Toutes les lignes d'un document ne comporte pas un code article
    String codeArticle = (String) ligneBrute[Svgvm0014d.VAR_L1ART];
    if (!codeArticle.trim().isEmpty()) {
      IdArticle idArticle = IdArticle.getInstance(idEtablissement, (String) ligneBrute[Svgvm0014d.VAR_L1ART]);
      ligneVente.setIdArticle(idArticle);
    }
    
    ligneVente.setTypeLigne(EnumTypeLigneVente.valueOfByCode(((String) ligneBrute[Svgvm0014d.VAR_L1ERL])));
    ligneVente.setCodeExtraction(((String) ligneBrute[Svgvm0014d.VAR_L1CEX]).charAt(0));
    ligneVente.setQuantiteAvantExtraction(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1QEX]));
    ligneVente.setLibelle((String) ligneBrute[Svgvm0014d.VAR_WLIGLB]);
    ligneVente.setNombreUSParUV(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1KSV]));
    if (!((String) ligneBrute[Svgvm0014d.VAR_L1UNV]).trim().isEmpty()) {
      IdUnite idUV = IdUnite.getInstance((String) ligneBrute[Svgvm0014d.VAR_L1UNV]);
      ligneVente.setIdUniteVente(idUV);
    }
    ligneVente.setNombreUVParUCV(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1CND]));
    if (!((String) ligneBrute[Svgvm0014d.VAR_L1MAG]).trim().isEmpty()) {
      ligneVente.setIdMagasin(IdMagasin.getInstance(idEtablissement, (String) ligneBrute[Svgvm0014d.VAR_L1MAG]));
    }
    ligneVente.setQuantitePieces(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1QTP]));
    ligneVente.setCodeTVA(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TVA]).intValue());
    ligneVente.setTopLigneEnValeur(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1VAL]).intValue());
    ligneVente.setNumeroColonneTVA(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1COL]).intValue());
    ligneVente.setCodeTPF(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TPF]).intValue());
    ligneVente.setCodeEtat(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TOP]).intValue());
    
    // Précision de l'unité de ventes
    int nombreDecimaleUV = ((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1DCV]).intValue();
    ligneVente.setNombreDecimaleUV(nombreDecimaleUV);
    
    ligneVente.setTopNonCommissionne(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TNC]).intValue());
    ligneVente.setSigneLigne(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1SGN]).intValue());
    ligneVente.setTopNumSerieOuLot(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1SER]).intValue());
    ligneVente.setExclusionRemisePied1(((String) ligneBrute[Svgvm0014d.VAR_L1RP1]).charAt(0));
    ligneVente.setExclusionRemisePied2(((String) ligneBrute[Svgvm0014d.VAR_L1RP2]).charAt(0));
    ligneVente.setExclusionRemisePied3(((String) ligneBrute[Svgvm0014d.VAR_L1RP3]).charAt(0));
    ligneVente.setExclusionRemisePied4(((String) ligneBrute[Svgvm0014d.VAR_L1RP4]).charAt(0));
    ligneVente.setExclusionRemisePied5(((String) ligneBrute[Svgvm0014d.VAR_L1RP5]).charAt(0));
    ligneVente.setExclusionRemisePied6(((String) ligneBrute[Svgvm0014d.VAR_L1RP6]).charAt(0));
    ligneVente.setPrixPromo((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PRP]);
    ligneVente.setCodeLigneAvoir(((String) ligneBrute[Svgvm0014d.VAR_L1AVR]).charAt(0));
    ligneVente.setInfoComplementaire((String) ligneBrute[Svgvm0014d.VAR_L1CPL]);
    ligneVente.setNombreUVParUCV((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1CND]);
    ligneVente.setNonSoumisEscompte(((String) ligneBrute[Svgvm0014d.VAR_L1IN1]).charAt(0));
    ligneVente.setPrixTransport((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PRT]);
    ligneVente.setDateLivraisonPrevue(ConvertDate.db2ToDate(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1DLP]).intValue(), null));
    
    // Renseigner le type de gratuit
    String codeTypeGratuit = (String) ligneBrute[Svgvm0014d.VAR_L1IN2];
    if (codeTypeGratuit != null) {
      ligneVente.setIdTypeGratuit(IdTypeGratuit.getInstance(codeTypeGratuit));
    }
    
    ligneVente.setCodeRegroupement(((String) ligneBrute[Svgvm0014d.VAR_L1IN21]).charAt(0));
    ligneVente.setTopPersonnalisation1(((String) ligneBrute[Svgvm0014d.VAR_L1TP1]).trim());
    ligneVente.setTopPersonnalisation2(((String) ligneBrute[Svgvm0014d.VAR_L1TP2]).trim());
    ligneVente.setTopPersonnalisation3(((String) ligneBrute[Svgvm0014d.VAR_L1TP3]).trim());
    ligneVente.setTopPersonnalisation4(((String) ligneBrute[Svgvm0014d.VAR_L1TP4]).trim());
    ligneVente.setTopPersonnalisation5(((String) ligneBrute[Svgvm0014d.VAR_L1TP5]).trim());
    ligneVente.setGenerationBonAchat(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1GBA]).intValue());
    String codeArticleSubstitue = (String) ligneBrute[Svgvm0014d.VAR_L1ARTS];
    if (!codeArticleSubstitue.trim().isEmpty()) {
      IdArticle idArticleSubstitue = IdArticle.getInstance(idEtablissement, codeArticleSubstitue);
      ligneVente.setIdArticleSubstitue(idArticleSubstitue);
    }
    ligneVente.setTypeSubstitution(((String) ligneBrute[Svgvm0014d.VAR_L1IN4]).charAt(0));
    ligneVente.setRemise50Commercial(((String) ligneBrute[Svgvm0014d.VAR_L1IN5]).charAt(0));
    ligneVente.setIndRecherchePrixKit(((String) ligneBrute[Svgvm0014d.VAR_L1IN7]).charAt(0));
    ligneVente.setQuantiteLivrable((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1QTL]);
    if (!((String) ligneBrute[Svgvm0014d.VAR_L1REP]).trim().isEmpty()) {
      ligneVente
          .setIdRepresentant(IdRepresentant.getInstance(idLigneVente.getIdEtablissement(), ((String) ligneBrute[Svgvm0014d.VAR_L1REP])));
    }
    ligneVente.setIndiceLivraison(((String) ligneBrute[Svgvm0014d.VAR_L1IN8]).charAt(0));
    ligneVente.setIndCumulL1QTE(((String) ligneBrute[Svgvm0014d.VAR_L1IN9]).charAt(0));
    ligneVente.setIndCumulL1QTP(((String) ligneBrute[Svgvm0014d.VAR_L1IN10]).charAt(0));
    ligneVente.setIndLigneAExtraire(((String) ligneBrute[Svgvm0014d.VAR_L1IN11]).charAt(0));
    ligneVente.setIndASDI(((String) ligneBrute[Svgvm0014d.VAR_L1IN12]).charAt(0));
    ligneVente.setNumeroLigneOrigine(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1NLI0]).intValue());
    ligneVente.setPrixAAjouterPrixRevient((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PRA]);
    ligneVente.setPrixAAjouterPrixVenteClient((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PVA]);
    ligneVente.setMontantTransport(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1MTR]));
    ligneVente.setTopArticleLoti(((String) ligneBrute[Svgvm0014d.VAR_L1SER3]).charAt(0));
    ligneVente.setTopArticleADS(((String) ligneBrute[Svgvm0014d.VAR_L1SER5]).charAt(0));
    ligneVente.setIndTypeVente(((String) ligneBrute[Svgvm0014d.VAR_L1IN17]).charAt(0));
    ligneVente.setOriginePrixVente(EnumOriginePrixVente.valueOfByCode(((String) ligneBrute[Svgvm0014d.VAR_L1IN18]).charAt(0)));
    ligneVente.setCodeGroupeCNV((String) ligneBrute[Svgvm0014d.VAR_WORIPR]);
    
    ligneVente.setIndApplicationConditionQuantitative(((String) ligneBrute[Svgvm0014d.VAR_L1IN19]).charAt(0));
    // Type condition chantier
    Character code = ((String) ligneBrute[Svgvm0014d.VAR_L1IN20]).charAt(0);
    ligneVente.setTypeConditionChantier(EnumTypeChantier.valueOfByCode(code));
    
    if (!((String) ligneBrute[Svgvm0014d.VAR_A1UNL]).trim().isEmpty()) {
      IdUnite idUC = IdUnite.getInstance((String) ligneBrute[Svgvm0014d.VAR_A1UNL]);
      ligneVente.setIdUniteConditionnementVente(idUC);
    }
    if (((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PVN]).compareTo(BigDecimal.ZERO) == 1) {
      PrixFlash prixFlash = new PrixFlash();
      prixFlash.setIdArticle(ligneVente.getIdArticle());
      prixFlash.setPrixVenteFlash((BigDecimal) ligneBrute[Svgvm0014d.VAR_WFPVN]);
      prixFlash.setCodeUtilisateurFlash(((String) ligneBrute[Svgvm0014d.VAR_WFUSR]).trim());
      prixFlash.setDatePrixFlash(ConvertDate.db2ToDate((String) ligneBrute[Svgvm0014d.VAR_L1IN17],
          ((BigDecimal) ligneBrute[Svgvm0014d.VAR_WFHEU]).intValue(), null));
      prixFlash.setHeurePrixFlash(((BigDecimal) ligneBrute[Svgvm0014d.VAR_WFHEU]).intValue());
      ligneVente.setPrixFlash(prixFlash);
    }
    ligneVente.setQuantiteDocumentOrigine((BigDecimal) ligneBrute[Svgvm0014d.VAR_WQTEOR]);
    ligneVente.setQuantiteDejaExtraite((BigDecimal) ligneBrute[Svgvm0014d.VAR_WQTEEX]);
    ligneVente.setMesure1((BigDecimal) ligneBrute[Svgvm0014d.VAR_L2QT1]);
    ligneVente.setMesure2((BigDecimal) ligneBrute[Svgvm0014d.VAR_L2QT2]);
    ligneVente.setMesure3((BigDecimal) ligneBrute[Svgvm0014d.VAR_L2QT3]);
    ligneVente.setNombreDecoupe(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L2NBR]).intValue());
    
    ligneVente.setTypeRemise(((String) ligneBrute[Svgvm0014d.VAR_L1TRL]).charAt(0));
    try {
      Character codeAssiette = ((String) ligneBrute[Svgvm0014d.VAR_L1BRL]).charAt(0);
      ligneVente.setAssietteTauxRemise(EnumAssietteTauxRemise.valueOfByCode(codeAssiette));
    }
    catch (Exception e) {
    }
    
    ligneVente.setTypeArticle(EnumTypeArticle.valueOfByCodeAlpha(((String) ligneBrute[Svgvm0014d.VAR_WTYPART]).trim()));
    ligneVente.setQuantiteArticlesFactures((BigDecimal) ligneBrute[Svgvm0014d.VAR_WQTEFAC]);
    
    // Adresse fournisseur
    if (((BigDecimal) ligneBrute[Svgvm0014d.VAR_WCOF]).compareTo(BigDecimal.ZERO) > 0
        && ((BigDecimal) ligneBrute[Svgvm0014d.VAR_WFRS]).compareTo(BigDecimal.ZERO) > 0) {
      IdFournisseur idFournisseur = IdFournisseur.getInstance(idEtablissement, ((BigDecimal) ligneBrute[Svgvm0014d.VAR_WCOF]).intValue(),
          ((BigDecimal) ligneBrute[Svgvm0014d.VAR_WFRS]).intValue());
      ligneVente.setIdAdresseFournisseur(
          IdAdresseFournisseur.getInstance(idFournisseur, ((BigDecimal) ligneBrute[Svgvm0014d.VAR_WFRE]).intValue()));
    }
    
    // Chargement des données concernants le prix de vente
    if (ligneVente.getTypeArticle() != EnumTypeArticle.COMMENTAIRE) {
      // Numéro colonne tarif
      ligneVente.setNumeroColonneTarif(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TAR]).intValue());
      try {
        ligneVente.setProvenanceColonneTarif(
            EnumProvenanceColonneTarif.valueOfByCode(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TT]).intValue()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      // Prix public
      // ligneVente.setPrixPublicHT((BigDecimal) ligneBrute[Svgvm0014d.VAR_WPT1HT]);
      // ligneVente.setPrixPublicTTC((BigDecimal) ligneBrute[Svgvm0014d.VAR_WPT1TTC]);
      // Prix de base
      ligneVente.setPrixBaseHT((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PVB]);
      ligneVente.setPrixBaseTTC((BigDecimal) ligneBrute[Svgvm0014d.VAR_WPBTTC]);
      try {
        ligneVente.setProvenancePrixBase(EnumProvenancePrixBase.valueOfByCode(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TB]).intValue()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      // Prix net
      ligneVente.setPrixNetSaisiHT((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PVN]);
      ligneVente.setPrixNetSaisiTTC((BigDecimal) ligneBrute[Svgvm0014d.VAR_WPNTTC]);
      try {
        ligneVente.setProvenancePrixNet(EnumProvenancePrixNet.valueOfByCode(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TN]).intValue()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      ligneVente.setPrixNetCalculeHT((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PVC]);
      ligneVente.setPrixNetCalculeTTC((BigDecimal) ligneBrute[Svgvm0014d.VAR_WPVTTC]);
      // Remises
      ligneVente.setTauxRemise1(new BigPercentage((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1REM1], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise2(new BigPercentage((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1REM2], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise3(new BigPercentage((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1REM3], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise4(new BigPercentage((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1REM4], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise5(new BigPercentage((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1REM5], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise6(new BigPercentage((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1REM6], EnumFormatPourcentage.POURCENTAGE));
      try {
        ligneVente
            .setProvenanceTauxRemise(EnumProvenanceTauxRemise.valueOfByCode(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TR]).intValue()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      // Coefficient
      ligneVente.setCoefficient((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1COE]);
      try {
        ligneVente
            .setProvenanceCoefficient(EnumProvenanceCoefficient.valueOfByCode(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1TC]).intValue()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      // TVA
      ligneVente.setPourcentageTVA((BigDecimal) ligneBrute[Svgvm0014d.VAR_WTAUTVA]);
      
      // Montant
      ligneVente.setMontantHT((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1MHT]);
      ligneVente.setMontantTTC((BigDecimal) ligneBrute[Svgvm0014d.VAR_WMTTTC]);
      
      // Quantité UV
      ligneVente.setQuantiteUV(((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1QTE]).setScale(nombreDecimaleUV));
      
      // Prix de revient
      ligneVente.setProvenancePrixDeRevient(((String) ligneBrute[Svgvm0014d.VAR_L1IN6]).charAt(0));
      BigDecimal prixRevientHT = ((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PRS]);
      ligneVente.setPrixDeRevientStandardHT(prixRevientHT);
      ligneVente.setPrixDeRevientStandardTTC(
          OutilCalculPrix.calculerPrixTTC(prixRevientHT, ligneVente.getPourcentageTVA(), Constantes.DEUX_DECIMALES));
      ligneVente.setPrixDeRevientPortIncluHT((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PRV]);
      // Si le prix de revient a été saisi alors on prend cette valeur
      if (ligneVente.isPrixRevientSaisi()) {
        ligneVente.setPrixDeRevientLigneHT((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PRV]);
      }
      // Sinon on l'initialise avec le prix de revient standard
      else {
        ligneVente.setPrixDeRevientLigneHT((BigDecimal) ligneBrute[Svgvm0014d.VAR_L1PRS]);
      }
      ligneVente.setPrixDeRevientLigneTTC(OutilCalculPrix.calculerPrixTTC(ligneVente.getPrixDeRevientLigneHT(),
          ligneVente.getPourcentageTVA(), Constantes.DEUX_DECIMALES));
    }
    
    return ligneVente;
  }
  
  /**
   * Appel du programme RPG qui va lire une ligne commentaire d'un document.
   */
  public LigneVente chargerLigneVenteCommentaire(SystemeManager pSysteme, IdLigneVente pIdLigneVente) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdLigneVente.controlerId(pIdLigneVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0050i entree = new Svgvm0050i();
    Svgvm0050o sortie = new Svgvm0050o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setL1etb(pIdLigneVente.getCodeEtablissement());
    entree.setL1cod(pIdLigneVente.getCodeEntete().getCode());
    entree.setL1num(pIdLigneVente.getNumero());
    entree.setL1suf(pIdLigneVente.getSuffixe());
    entree.setL1nli(pIdLigneVente.getNumeroLigne());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0050_CHARGER_LIGNE_VENTE_COMMENTAIRE, EnvironnementExecution.getGvmas(), parameterList)) {
      return null;
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Initialisation de la classe métier
    LigneVente ligneCommentaire = new LigneVente(pIdLigneVente);
    ligneCommentaire.setIndicateurs(sortie.getPoind().trim()); // Indicateurs
    ligneCommentaire.setNumeroLigneOrigine(sortie.getPonli()); // N° de ligne
    ligneCommentaire.setCodeRegroupement(sortie.getPoin21()); // Référence du regroupement
    ligneCommentaire.setTopPersonnalisation1(sortie.getPotp1().trim()); // Top personnal.N°1
    ligneCommentaire.setTopPersonnalisation2(sortie.getPotp2().trim()); // Top personnal.N°2
    ligneCommentaire.setTopPersonnalisation3(sortie.getPotp3().trim()); // Top personnal.N°3
    ligneCommentaire.setTopPersonnalisation4(sortie.getPotp4().trim()); // Top personnal.N°4
    ligneCommentaire.setTopPersonnalisation5(sortie.getPotp5().trim()); // Top personnal.N°5
    ligneCommentaire.setLibelle(sortie.getPolb().trim()); // Libellés
    if (sortie.getPocpy().equals(' ')) {
      ligneCommentaire.setGenerationBonAchat(0);
    }
    else {
      ligneCommentaire.setGenerationBonAchat(LigneVente.COMMENTAIRE_A_COPIER_DANS_COMMANDE_ACHAT);
    }
    return ligneCommentaire;
  }
  
  /**
   * Appel du programme RPG qui va rechercher les lignes en vue d'un retour article
   * Voir SNC-3809
   * TODO Il faut l'IdDocumentVente
   */
  public ListeLigneRetourArticle chargerListeLigneVenteRetour(SystemeManager pSysteme, BigDecimal pQuantite, Article pArticle,
      Client pClient, ListeLigneRetourArticle pListeOrigine) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pArticle == null) {
      throw new MessageErreurException("Le paramètre pArticle du service est à null.");
    }
    if (pClient == null) {
      throw new MessageErreurException("Le paramètre pClient du service est à null.");
    }
    
    ListeLigneRetourArticle listeRetour = null;
    ListeLigneVente listeLigneVente = null;
    
    // Préparation des paramètres du programme RPG
    Svgvm0054i entree = new Svgvm0054i();
    Svgvm0054o sortie = new Svgvm0054o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    if (pListeOrigine == null) {
      entree.setPiind("0100000000");
    }
    else {
      entree.setPiind("1110000000");
    }
    entree.setPietb("");
    entree.setPinum(0);
    entree.setPilig(15);
    entree.setPipag(1);
    entree.setPityp("EXF");
    entree.setPietb(pClient.getId().getCodeEtablissement());
    entree.setPicli(pClient.getId().getNumero());
    entree.setPiliv(pClient.getId().getSuffixe());
    entree.setPiart(pArticle.getId().getCodeArticle());
    entree.setPiqte(pQuantite);
    if (pListeOrigine != null && pListeOrigine.getDateDebut() != null) {
      entree.setPidebc(ConvertDate.dateToDb2(pListeOrigine.getDateDebut()));
      entree.setPifinc(ConvertDate.dateToDb2(pListeOrigine.getDateFin()));
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0054_CHARGER_LISTE_LIGNE_VENTE_RETOUR, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      listeLigneVente = new ListeLigneVente();
      Object[] lignes = sortie.getValeursBrutesLignes();
      for (int i = 0; i < lignes.length; i++) {
        LigneVente ligneVente = traiterUneLigne((Object[]) lignes[i], pClient);
        if (ligneVente != null) {
          listeLigneVente.add(ligneVente);
        }
      }
    }
    
    listeRetour = new ListeLigneRetourArticle();
    listeRetour.setListeLigneVente(listeLigneVente);
    listeRetour.setDateDebut(ConvertDate.db2ToDate(sortie.getPodebc(), null));
    listeRetour.setDateFin(ConvertDate.db2ToDate(sortie.getPofinc(), null));
    
    return listeRetour;
  }
  
  /**
   * Appel du programme RPG qui va lire un regroupement de lignes dans un document.
   */
  public Regroupement chargerLigneVenteGroupe(SystemeManager pSysteme, IdLigneVente pIdLigneVente, Regroupement regroupement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme est invalide.");
    }
    IdLigneVente.controlerId(pIdLigneVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0032i entree = new Svgvm0032i();
    Svgvm0032o sortie = new Svgvm0032o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdLigneVente.getCodeEtablissement());
    entree.setPicod(pIdLigneVente.getCodeEntete().getCode());
    entree.setPinum(pIdLigneVente.getNumero());
    entree.setPisuf(pIdLigneVente.getSuffixe());
    entree.setPinli(pIdLigneVente.getNumeroLigne());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0032_CHARGER_LIGNE_VENTE_GROUPE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      if (regroupement == null) {
        regroupement = new Regroupement();
      }
      
      IdLigneVente idLigneDebut = IdLigneVente.getInstance(pIdLigneVente.getIdEtablissement(), pIdLigneVente.getCodeEntete(),
          pIdLigneVente.getNumero(), pIdLigneVente.getSuffixe(), sortie.getPonlid().intValue());
      regroupement.setIdLigneVenteDebut(idLigneDebut);
      
      IdLigneVente idLigneFin = IdLigneVente.getInstance(pIdLigneVente.getIdEtablissement(), pIdLigneVente.getCodeEntete(),
          pIdLigneVente.getNumero(), pIdLigneVente.getSuffixe(), sortie.getPonlif().intValue());
      regroupement.setIdLigneVenteFin(idLigneFin);
      
      regroupement.setTexteTitre(Constantes.trimRight(sortie.getPotit1() + sortie.getPotit2() + sortie.getPotit3() + sortie.getPotit4()));
      regroupement.setTexteTotal(Constantes.trimRight(sortie.getPotot1() + sortie.getPotot2() + sortie.getPotot3() + sortie.getPotot4()));
      if (sortie.getPoedta().equals('0')) {
        regroupement.setEditionDetailArticles(false);
      }
      else {
        regroupement.setEditionDetailArticles(true);
      }
      if (sortie.getPoedtp().equals('0')) {
        regroupement.setEditionPrixArticles(false);
      }
      else {
        regroupement.setEditionPrixArticles(true);
      }
      regroupement.setMontantTotalHT(sortie.getPotmht());
      regroupement.setTotalPrixRevient(sortie.getPotprv());
      regroupement.setMontantMargeTotal(sortie.getPotmar());
      regroupement.setPourcentageMarge(sortie.getPopmar());
    }
    
    return regroupement;
  }
  
  /**
   * Découpage d'une ligne.
   */
  private LigneVente traiterUneLigne(Object[] ligneBrute, Client pClient) {
    if (ligneBrute == null) {
      return null;
    }
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) ligneBrute[Svgvm0054d.VAR_LOETB]);
    EnumCodeEnteteDocumentVente codeEntete =
        EnumCodeEnteteDocumentVente.valueOfByCode(((String) ligneBrute[Svgvm0054d.VAR_LOCOD]).charAt(0));
    Integer numero = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_LONUM]).intValue();
    Integer suffixe = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOSUF]).intValue();
    Integer numeroLigne = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_LONLI]).intValue();
    IdLigneVente idLigneVente = IdLigneVente.getInstance(idEtablissement, codeEntete, numero, suffixe, numeroLigne);
    LigneVente ligneVente = new LigneVente(idLigneVente);
    
    // L'id du document de vente
    Integer numeroFacture = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_WNFA]).intValue();
    IdDocumentVente idDocumentVente = IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, numero, suffixe, numeroFacture);
    ligneVente.setIdDocumentVente(idDocumentVente);
    
    // Toutes les lignes d'un document ne comporte pas un code article
    String codeArticle = (String) ligneBrute[Svgvm0054d.VAR_LOART];
    if (!codeArticle.trim().isEmpty()) {
      IdArticle idArticle = IdArticle.getInstance(idEtablissement, (String) ligneBrute[Svgvm0054d.VAR_LOART]);
      ligneVente.setIdArticle(idArticle);
    }
    
    ligneVente.setTypeLigne(EnumTypeLigneVente.valueOfByCode(((String) ligneBrute[Svgvm0054d.VAR_LOERL])));
    ligneVente.setCodeExtraction(((String) ligneBrute[Svgvm0054d.VAR_LOCEX]).charAt(0));
    ligneVente.setQuantiteAvantExtraction(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOQEX]));
    ligneVente.setLibelle((String) ligneBrute[Svgvm0054d.VAR_WLIGLB]);
    ligneVente.setNombreUSParUV(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOKSV]));
    if (!((String) ligneBrute[Svgvm0054d.VAR_LOUNV]).trim().isEmpty()) {
      IdUnite idUV = IdUnite.getInstance((String) ligneBrute[Svgvm0054d.VAR_LOUNV]);
      ligneVente.setIdUniteVente(idUV);
    }
    ligneVente.setNombreUVParUCV(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOCND]));
    if (!((String) ligneBrute[Svgvm0054d.VAR_LOMAG]).trim().isEmpty()) {
      ligneVente.setIdMagasin(IdMagasin.getInstance(idEtablissement, (String) ligneBrute[Svgvm0054d.VAR_LOMAG]));
    }
    ligneVente.setQuantitePieces(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOQTP]));
    ligneVente.setCodeTVA(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTVA]).intValue());
    ligneVente.setTopLigneEnValeur(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOVAL]).intValue());
    ligneVente.setNumeroColonneTVA(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOCOL]).intValue());
    ligneVente.setCodeTPF(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTPF]).intValue());
    ligneVente.setCodeEtat(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTOP]).intValue());
    
    // Précision de l'unité de ventes
    int nombreDecimaleUV = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_LODCV]).intValue();
    ligneVente.setNombreDecimaleUV(nombreDecimaleUV);
    
    ligneVente.setTopNonCommissionne(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTNC]).intValue());
    ligneVente.setSigneLigne(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOSGN]).intValue());
    ligneVente.setTopNumSerieOuLot(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOSER]).intValue());
    ligneVente.setExclusionRemisePied1(((String) ligneBrute[Svgvm0054d.VAR_LORP1]).charAt(0));
    ligneVente.setExclusionRemisePied2(((String) ligneBrute[Svgvm0054d.VAR_LORP2]).charAt(0));
    ligneVente.setExclusionRemisePied3(((String) ligneBrute[Svgvm0054d.VAR_LORP3]).charAt(0));
    ligneVente.setExclusionRemisePied4(((String) ligneBrute[Svgvm0054d.VAR_LORP4]).charAt(0));
    ligneVente.setExclusionRemisePied5(((String) ligneBrute[Svgvm0054d.VAR_LORP5]).charAt(0));
    ligneVente.setExclusionRemisePied6(((String) ligneBrute[Svgvm0054d.VAR_LORP6]).charAt(0));
    ligneVente.setPrixPromo(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPRP]));
    ligneVente.setCodeLigneAvoir(((String) ligneBrute[Svgvm0054d.VAR_LOAVR]).charAt(0));
    ligneVente.setInfoComplementaire((String) ligneBrute[Svgvm0054d.VAR_LOCPL]);
    ligneVente.setNombreUVParUCV(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOCND]));
    ligneVente.setNonSoumisEscompte(((String) ligneBrute[Svgvm0054d.VAR_LOIN1]).charAt(0));
    ligneVente.setPrixTransport(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPRT]));
    ligneVente.setDateLivraisonPrevue(ConvertDate.db2ToDate(((BigDecimal) ligneBrute[Svgvm0054d.VAR_WDAT]).intValue(), null));
    String codeTypeGratuit = (String) ligneBrute[Svgvm0054d.VAR_LOIN2];
    if (codeTypeGratuit != null) {
      ligneVente.setIdTypeGratuit(IdTypeGratuit.getInstance(codeTypeGratuit));
    }
    ligneVente.setCodeRegroupement(((String) ligneBrute[Svgvm0054d.VAR_LOIN21]).charAt(0));
    ligneVente.setTopPersonnalisation1(((String) ligneBrute[Svgvm0054d.VAR_LOTP1]).trim());
    ligneVente.setTopPersonnalisation2(((String) ligneBrute[Svgvm0054d.VAR_LOTP2]).trim());
    ligneVente.setTopPersonnalisation3(((String) ligneBrute[Svgvm0054d.VAR_LOTP3]).trim());
    ligneVente.setTopPersonnalisation4(((String) ligneBrute[Svgvm0054d.VAR_LOTP4]).trim());
    ligneVente.setTopPersonnalisation5(((String) ligneBrute[Svgvm0054d.VAR_LOTP5]).trim());
    ligneVente.setGenerationBonAchat(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOGBA]).intValue());
    String codeArticleSubstitue = (String) ligneBrute[Svgvm0054d.VAR_LOARTS];
    if (!codeArticleSubstitue.trim().isEmpty()) {
      IdArticle idArticleSubstitue = IdArticle.getInstance(idEtablissement, codeArticleSubstitue);
      ligneVente.setIdArticleSubstitue(idArticleSubstitue);
    }
    ligneVente.setTypeSubstitution(((String) ligneBrute[Svgvm0054d.VAR_LOIN4]).charAt(0));
    ligneVente.setRemise50Commercial(((String) ligneBrute[Svgvm0054d.VAR_LOIN5]).charAt(0));
    ligneVente.setIndRecherchePrixKit(((String) ligneBrute[Svgvm0054d.VAR_LOIN7]).charAt(0));
    ligneVente.setQuantiteLivrable(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOQTL]));
    if (!((String) ligneBrute[Svgvm0054d.VAR_LOREP]).trim().isEmpty()) {
      ligneVente
          .setIdRepresentant(IdRepresentant.getInstance(idLigneVente.getIdEtablissement(), ((String) ligneBrute[Svgvm0054d.VAR_LOREP])));
    }
    ligneVente.setIndiceLivraison(((String) ligneBrute[Svgvm0054d.VAR_LOIN8]).charAt(0));
    ligneVente.setIndCumulL1QTE(((String) ligneBrute[Svgvm0054d.VAR_LOIN9]).charAt(0));
    ligneVente.setIndCumulL1QTP(((String) ligneBrute[Svgvm0054d.VAR_LOIN10]).charAt(0));
    ligneVente.setIndLigneAExtraire(((String) ligneBrute[Svgvm0054d.VAR_LOIN11]).charAt(0));
    ligneVente.setIndASDI(((String) ligneBrute[Svgvm0054d.VAR_LOIN12]).charAt(0));
    ligneVente.setNumeroLigneOrigine(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LONLI0]).intValue());
    ligneVente.setPrixAAjouterPrixRevient(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPRA]));
    ligneVente.setPrixAAjouterPrixVenteClient(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPVA]));
    ligneVente.setMontantTransport(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOMTR]));
    ligneVente.setTopArticleLoti(((String) ligneBrute[Svgvm0054d.VAR_LOSER3]).charAt(0));
    ligneVente.setTopArticleADS(((String) ligneBrute[Svgvm0054d.VAR_LOSER5]).charAt(0));
    ligneVente.setIndTypeVente(((String) ligneBrute[Svgvm0054d.VAR_LOIN17]).charAt(0));
    ligneVente.setOriginePrixVente(EnumOriginePrixVente.valueOfByCode(((String) ligneBrute[Svgvm0054d.VAR_LOIN18]).charAt(0)));
    ligneVente.setIndApplicationConditionQuantitative(((String) ligneBrute[Svgvm0054d.VAR_LOIN19]).charAt(0));
    
    // Le type de condition chantier
    Character code = ((String) ligneBrute[Svgvm0054d.VAR_LOIN20]).charAt(0);
    ligneVente.setTypeConditionChantier(EnumTypeChantier.valueOfByCode(code));
    
    if (!((String) ligneBrute[Svgvm0054d.VAR_A1UNL]).trim().isEmpty()) {
      IdUnite idUC = IdUnite.getInstance((String) ligneBrute[Svgvm0054d.VAR_A1UNL]);
      ligneVente.setIdUniteConditionnementVente(idUC);
    }
    
    ligneVente.setQuantiteDocumentOrigine(((BigDecimal) ligneBrute[Svgvm0054d.VAR_WQTEOR]));
    ligneVente.setQuantiteDejaExtraite(((BigDecimal) ligneBrute[Svgvm0054d.VAR_WQTEEX]));
    ligneVente.setMesure1(((BigDecimal) ligneBrute[Svgvm0054d.VAR_L2QT1]));
    ligneVente.setMesure2(((BigDecimal) ligneBrute[Svgvm0054d.VAR_L2QT2]));
    ligneVente.setMesure3(((BigDecimal) ligneBrute[Svgvm0054d.VAR_L2QT3]));
    ligneVente.setNombreDecoupe(((BigDecimal) ligneBrute[Svgvm0054d.VAR_L2NBR]).intValue());
    
    ligneVente.setTypeRemise(((String) ligneBrute[Svgvm0054d.VAR_LOTRL]).charAt(0));
    try {
      Character codeAssiette = ((String) ligneBrute[Svgvm0054d.VAR_LOBRL]).charAt(0);
      ligneVente.setAssietteTauxRemise(EnumAssietteTauxRemise.valueOfByCode(codeAssiette));
    }
    catch (Exception e) {
    }
    
    ligneVente.setTypeArticle(EnumTypeArticle.valueOfByCodeAlpha("ART"));
    ligneVente.setQuantiteArticlesFactures(((BigDecimal) ligneBrute[Svgvm0054d.VAR_WQTEFAC]));
    ligneVente.setQuantiteDejaRetounee(((BigDecimal) ligneBrute[Svgvm0054d.VAR_WQTEAV]));
    ligneVente.setClientTTC(pClient.isFactureEnTTC());
    
    // Chargement des données concernants le prix
    if (ligneVente.getTypeArticle() != EnumTypeArticle.COMMENTAIRE) {
      ligneVente.setPourcentageTVA((BigDecimal) ligneBrute[Svgvm0054d.VAR_WTAUTVA]);
      
      // Quantité en UV, arrondie avec la précision stockée dans la ligne de ventes
      ligneVente.setQuantiteUV(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOQTE]).setScale(nombreDecimaleUV));
      
      ligneVente.setNumeroColonneTarif(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTAR]).intValue());
      try {
        int origineColonneTarif = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTT]).intValue();
        ligneVente.setProvenanceColonneTarif(EnumProvenanceColonneTarif.valueOfByCode(origineColonneTarif));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      // ligneVente.setPrixPublicHT((BigDecimal) ligneBrute[Svgvm0054d.VAR_WPT1HT]);
      // ligneVente.setPrixPublicTTC((BigDecimal) ligneBrute[Svgvm0054d.VAR_WPT1TTC]);
      ligneVente.setPrixBaseHT(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPVB]));
      ligneVente.setPrixBaseTTC((BigDecimal) ligneBrute[Svgvm0054d.VAR_WPBTTC]);
      try {
        int originePrixBase = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTB]).intValue();
        ligneVente.setProvenancePrixBase(EnumProvenancePrixBase.valueOfByCode(originePrixBase));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      ligneVente.setPrixNetSaisiHT((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPVN]);
      ligneVente.setPrixNetSaisiTTC((BigDecimal) ligneBrute[Svgvm0054d.VAR_WPNTTC]);
      try {
        int originePrixNetSaisi = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTN]).intValue();
        ligneVente.setProvenancePrixNet(EnumProvenancePrixNet.valueOfByCode(originePrixNetSaisi));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      ligneVente.setPrixNetCalculeHT((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPVC]);
      ligneVente.setPrixNetCalculeTTC((BigDecimal) ligneBrute[Svgvm0054d.VAR_WPVTTC]);
      ligneVente.setMontantHT((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOMHT]);
      ligneVente.setMontantTTC((BigDecimal) ligneBrute[Svgvm0054d.VAR_WMTTTC]);
      
      ligneVente.setTauxRemise1(new BigPercentage((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOREM1], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise2(new BigPercentage((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOREM2], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise3(new BigPercentage((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOREM3], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise4(new BigPercentage((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOREM4], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise5(new BigPercentage((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOREM5], EnumFormatPourcentage.POURCENTAGE));
      ligneVente.setTauxRemise6(new BigPercentage((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOREM6], EnumFormatPourcentage.POURCENTAGE));
      try {
        int origineRemise = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTR]).intValue();
        ligneVente.setProvenanceTauxRemise(EnumProvenanceTauxRemise.valueOfByCode(origineRemise));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      ligneVente.setPrixDeRevientStandardHT((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPRS]);
      ligneVente.setPrixDeRevientStandardTTC(OutilCalculPrix.calculerPrixTTC(ligneVente.getPrixDeRevientStandardHT(),
          ligneVente.getPourcentageTVA(), Constantes.DEUX_DECIMALES));
      ligneVente.setPrixDeRevientPortIncluHT(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPRV]));
      // Si le prix de revient a été saisi alors on prend cette valeur
      if (ligneVente.isPrixRevientSaisi()) {
        ligneVente.setPrixDeRevientLigneHT(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPRV]));
      }
      // Sinon on l'initialise avec le prix de revient standard
      else {
        ligneVente.setPrixDeRevientLigneHT(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOPRS]));
      }
      ligneVente.setPrixDeRevientLigneTTC(OutilCalculPrix.calculerPrixTTC(ligneVente.getPrixDeRevientLigneHT(),
          ligneVente.getPourcentageTVA(), Constantes.DEUX_DECIMALES));
      try {
        char origineprv = ((String) ligneBrute[Svgvm0054d.VAR_LOIN6]).charAt(0);
        ligneVente.setProvenancePrixDeRevient(origineprv);
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      ligneVente.setCoefficient(((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOCOE]));
      try {
        int origineCoefficient = ((BigDecimal) ligneBrute[Svgvm0054d.VAR_LOTC]).intValue();
        ligneVente.setProvenanceCoefficient(EnumProvenanceCoefficient.valueOfByCode(origineCoefficient));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
    
    return ligneVente;
  }
  
  /**
   * Appel du programme RPG qui créé une ligne dans un document de ventes.
   * 
   * Opérations réalisées :
   * - Prise éventuelle d'un n°de ligne
   * - Ecriture ligne en base de données
   * - Ecriture extension de ligne en base de données
   * - Préparation proposition articles liés éventuels
   */
  public void sauverLigneVente(SystemeManager pSysteme, QueryManager pQueryManager, LigneVente pLigneVente) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    LigneVente.controlerId(pLigneVente, false);
    
    // Préparation des paramètres du programme RPG
    Svgvm0009i entree = new Svgvm0009i();
    Svgvm0009o sortie = new Svgvm0009o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setL1etb(pLigneVente.getId().getCodeEtablissement());
    entree.setL1cod(pLigneVente.getId().getCodeEntete().getCode());
    entree.setL1num(pLigneVente.getId().getNumero());
    entree.setL1suf(pLigneVente.getId().getSuffixe());
    entree.setL1nli(pLigneVente.getId().getNumeroLigne());
    entree.setL1top(pLigneVente.getCodeEtat());
    entree.setL1erl(pLigneVente.getTypeLigne().getCode());
    entree.setL1cex(pLigneVente.getCodeExtraction());
    entree.setL1qex(pLigneVente.getQuantiteAvantExtraction());
    entree.setL1tva(pLigneVente.getCodeTVA());
    if (pLigneVente.getProvenancePrixBase() != null) {
      entree.setL1tb(pLigneVente.getProvenancePrixBase().getNumero());
    }
    else {
      entree.setL1tb(0);
    }
    if (pLigneVente.getProvenanceTauxRemise() != null) {
      entree.setL1tr(pLigneVente.getProvenanceTauxRemise().getNumero());
    }
    else {
      entree.setL1tr(0);
    }
    if (pLigneVente.getProvenancePrixNet() != null) {
      entree.setL1tn(pLigneVente.getProvenancePrixNet().getNumero());
    }
    else {
      entree.setL1tn(0);
    }
    if (pLigneVente.getProvenanceCoefficient() != null) {
      entree.setL1tc(pLigneVente.getProvenanceCoefficient().getNumero());
    }
    else {
      entree.setL1tc(0);
    }
    if (pLigneVente.getProvenanceColonneTarif() != null) {
      entree.setL1tt(pLigneVente.getProvenanceColonneTarif().getNumero());
    }
    else {
      entree.setL1tt(0);
    }
    entree.setL1val(pLigneVente.getTopLigneEnValeur());
    entree.setL1tar(pLigneVente.getNumeroColonneTarif());
    // Trace.info("L1TAR = " + pLigneVente.getNumeroColonneTarif());
    entree.setL1col(pLigneVente.getNumeroColonneTVA());
    entree.setL1tpf(pLigneVente.getCodeTPF());
    entree.setL1dcv(pLigneVente.getNombreDecimaleUV());
    entree.setL1tnc(pLigneVente.getTopNonCommissionne());
    entree.setL1sgn(pLigneVente.getSigneLigne());
    entree.setL1ser(pLigneVente.getTopNumSerieOuLot());
    entree.setL1qte(pLigneVente.getQuantiteUV());
    entree.setL1ksv(pLigneVente.getNombreUSParUV());
    
    // Si la remise vaut 100% alors la remise est initialisée à 0 car il s'agit d'un gratuit
    if (pLigneVente.getTauxRemise1() != null && !pLigneVente.getTauxRemise1().isCent()) {
      entree.setL1rem1(pLigneVente.getTauxRemise1().getTauxEnPourcentage());
    }
    else {
      entree.setL1rem1(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise2() != null && !pLigneVente.getTauxRemise2().isCent()) {
      entree.setL1rem2(pLigneVente.getTauxRemise2().getTauxEnPourcentage());
    }
    else {
      entree.setL1rem2(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise3() != null && !pLigneVente.getTauxRemise3().isCent()) {
      entree.setL1rem3(pLigneVente.getTauxRemise3().getTauxEnPourcentage());
    }
    else {
      entree.setL1rem3(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise4() != null && !pLigneVente.getTauxRemise4().isCent()) {
      entree.setL1rem4(pLigneVente.getTauxRemise4().getTauxEnPourcentage());
    }
    else {
      entree.setL1rem4(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise5() != null && !pLigneVente.getTauxRemise5().isCent()) {
      entree.setL1rem5(pLigneVente.getTauxRemise5().getTauxEnPourcentage());
    }
    else {
      entree.setL1rem5(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise6() != null && !pLigneVente.getTauxRemise6().isCent()) {
      entree.setL1rem6(pLigneVente.getTauxRemise6().getTauxEnPourcentage());
    }
    else {
      entree.setL1rem6(BigDecimal.ZERO);
    }
    entree.setL1trl(pLigneVente.getTypeRemise());
    if (pLigneVente.getAssietteTauxRemise() == null) {
      entree.setL1brl(EnumAssietteTauxRemise.PRIX_UNITAIRE.getCode());
    }
    else {
      entree.setL1brl(pLigneVente.getAssietteTauxRemise().getCode());
    }
    entree.setL1rp1(pLigneVente.getExclusionRemisePied1());
    entree.setL1rp2(pLigneVente.getExclusionRemisePied2());
    entree.setL1rp3(pLigneVente.getExclusionRemisePied3());
    entree.setL1rp4(pLigneVente.getExclusionRemisePied4());
    entree.setL1rp5(pLigneVente.getExclusionRemisePied5());
    entree.setL1rp6(pLigneVente.getExclusionRemisePied6());
    // En fonction du type de client (HT ou TTC), on initialise avec les prix correspondants
    // C'est important afin d'éviter les problèmes de différences de prix lors des conversions HT/TTC
    if (pLigneVente.isClientTTC()) {
      if (pLigneVente.getPrixBaseTTC() == null) {
        entree.setL1pvb(BigDecimal.ZERO);
      }
      else {
        entree.setL1pvb(pLigneVente.getPrixBaseTTC());
      }
      if (pLigneVente.getPrixNetSaisiTTC() == null) {
        entree.setL1pvn(BigDecimal.ZERO);
      }
      else {
        entree.setL1pvn(pLigneVente.getPrixNetSaisiTTC());
      }
      if (pLigneVente.getPrixNetCalculeTTC() == null) {
        entree.setL1pvc(BigDecimal.ZERO);
      }
      else {
        entree.setL1pvc(pLigneVente.getPrixNetCalculeTTC());
      }
    }
    else {
      if (pLigneVente.getPrixBaseHT() == null) {
        entree.setL1pvb(BigDecimal.ZERO);
      }
      else {
        entree.setL1pvb(pLigneVente.getPrixBaseHT());
      }
      if (pLigneVente.getPrixNetSaisiHT() == null) {
        entree.setL1pvn(BigDecimal.ZERO);
      }
      else {
        entree.setL1pvn(pLigneVente.getPrixNetSaisiHT());
      }
      if (pLigneVente.getPrixNetCalculeHT() == null) {
        entree.setL1pvc(BigDecimal.ZERO);
      }
      else {
        entree.setL1pvc(pLigneVente.getPrixNetCalculeHT());
      }
    }
    entree.setL1mht(pLigneVente.getMontantHT());
    entree.setL1prp(pLigneVente.getPrixPromo());
    entree.setL1avr(pLigneVente.getCodeLigneAvoir());
    if (pLigneVente.getIdUniteVente() != null) {
      entree.setL1unv(pLigneVente.getIdUniteVente().getCode());
    }
    entree.setL1art(pLigneVente.getIdArticle().getCodeArticle());
    entree.setL1cpl(pLigneVente.getInfoComplementaire());
    entree.setL1cnd(pLigneVente.getNombreUVParUCV());
    entree.setL1in1(pLigneVente.getNonSoumisEscompte());
    entree.setL1prt(pLigneVente.getPrixTransport());
    entree.setL1dlp(ConvertDate.dateToDb2(pLigneVente.getDateLivraisonPrevue()));
    
    if (pLigneVente.getIdTypeGratuit() != null) {
      entree.setL1in2(pLigneVente.getIdTypeGratuit().getCode());
    }
    
    entree.setL1in3(pLigneVente.getCodeRegroupement());
    entree.setL1tp1(pLigneVente.getTopPersonnalisation1());
    entree.setL1tp2(pLigneVente.getTopPersonnalisation2());
    entree.setL1tp3(pLigneVente.getTopPersonnalisation3());
    entree.setL1tp4(pLigneVente.getTopPersonnalisation4());
    entree.setL1tp5(pLigneVente.getTopPersonnalisation5());
    entree.setL1gba(pLigneVente.getGenerationBonAchat());
    if (pLigneVente.getIdArticleSubstitue() != null) {
      entree.setL1arts(pLigneVente.getIdArticleSubstitue().getCodeArticle());
    }
    entree.setL1in4(pLigneVente.getTypeSubstitution());
    entree.setL1in5(pLigneVente.getRemise50Commercial());
    entree.setL1in7(pLigneVente.getIndRecherchePrixKit());
    entree.setL1qtl(pLigneVente.getQuantiteLivrable());
    if (pLigneVente.getIdMagasin() != null) {
      entree.setL1mag(pLigneVente.getIdMagasin().getCode());
    }
    if (pLigneVente.getIdRepresentant() != null) {
      entree.setL1rep(pLigneVente.getIdRepresentant().getCode());
    }
    entree.setL1in8(pLigneVente.getIndiceLivraison());
    entree.setL1in9(pLigneVente.getIndCumulL1QTE());
    entree.setL1in10(pLigneVente.getIndCumulL1QTP());
    entree.setL1in11(pLigneVente.getIndLigneAExtraire());
    entree.setL1in12(pLigneVente.getIndASDI());
    entree.setL1nli0(pLigneVente.getNumeroLigneOrigine());
    entree.setL1pra(pLigneVente.getPrixAAjouterPrixRevient());
    entree.setL1pva(pLigneVente.getPrixAAjouterPrixVenteClient());
    entree.setL1qtp(pLigneVente.getQuantitePieces());
    entree.setL1mtr(pLigneVente.getMontantTransport());
    entree.setL1ser3(pLigneVente.getTopArticleLoti());
    entree.setL1ser5(pLigneVente.getTopArticleADS());
    entree.setL1in17(pLigneVente.getIndTypeVente());
    
    // Renseigner l'origine du prix de vente
    if (pLigneVente.getOriginePrixVente() != null) {
      EnumOriginePrixVente originePrixVente = pLigneVente.getOriginePrixVente();
      entree.setL1in18(originePrixVente.getCode());
    }
    
    entree.setL1in19(pLigneVente.getIndApplicationConditionQuantitative());
    // Le type de la condition chantier
    if (pLigneVente.getTypeConditionChantier() == null) {
      entree.setL1in20(' ');
    }
    else {
      entree.setL1in20(pLigneVente.getTypeConditionChantier().getCode());
    }
    entree.setL1in21(pLigneVente.getCodeRegroupement());
    entree.setL1in22(' ');
    entree.setPiqt1(pLigneVente.getMesure1());
    entree.setPiqt2(pLigneVente.getMesure2());
    entree.setPiqt3(pLigneVente.getMesure3());
    entree.setPinbr(pLigneVente.getNombreDecoupe());
    entree.setPilb(pLigneVente.getInfoComplementaire());
    entree.setL1prv(pLigneVente.getPrixDeRevientLigneHT());
    if (pLigneVente.getProvenancePrixDeRevient() == LigneVente.ORIGINE_NON_DEFINI) {
      entree.setL1in6(' ');
    }
    else {
      entree.setL1in6(pLigneVente.getProvenancePrixDeRevient());
    }
    if (pLigneVente.getIdAdresseFournisseur() != null) {
      entree.setPicol(pLigneVente.getIdAdresseFournisseur().getIdFournisseur().getCollectif());
      entree.setPifrs(pLigneVente.getIdAdresseFournisseur().getIdFournisseur().getNumero());
      entree.setPifre(pLigneVente.getIdAdresseFournisseur().getIndice());
    }
    
    // Sauver les conditions de ventes utilisées par la ligne de ventes avant l'appel du service RPG
    if (pQueryManager != null) {
      List<InformationConditionVenteLigneVente> listeConditionVenteLigneVente = pLigneVente.getListeConditionVenteLigneVente();
      if (listeConditionVenteLigneVente != null && !listeConditionVenteLigneVente.isEmpty()) {
        SqlPrixVente sqlPrixVente = new SqlPrixVente(pQueryManager);
        sqlPrixVente.sauverConditionVenteLigneVente(pLigneVente.getId(), listeConditionVenteLigneVente);
      }
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0009_SAUVER_LIGNE_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va créer ou modifier un article commentaire.
   */
  public int sauverLigneVenteCommentaire(SystemeManager pSysteme, LigneVente pLigneVente, DocumentVente pDocumentVente,
      boolean isEditionSurFactures, boolean pConserverSurAchat) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    LigneVente.controlerId(pLigneVente, true);
    IdDocumentVente.controlerId(pDocumentVente.getId(), true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0049i entree = new Svgvm0049i();
    Svgvm0049o sortie = new Svgvm0049o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind(pLigneVente.getIndicateurs());
    entree.setL1cod(pDocumentVente.getId().getEntete());
    entree.setL1etb(pDocumentVente.getId().getCodeEtablissement()); // Code établissement
    entree.setL1num(pDocumentVente.getId().getNumero()); // Numéro document
    entree.setL1suf(pDocumentVente.getId().getSuffixe()); // Suffixe document
    if (pLigneVente.getIdArticle() != null) {
      entree.setPiart(pLigneVente.getIdArticle().getCodeArticle()); // Pas de code article pour une ligne commentaire
    }
    else {
      entree.setPiart("");
    }
    entree.setL1nli(pLigneVente.getId().getNumeroLigne()); // Numéro de ligne. Si aucun numéro on est en création.
    entree.setPiin21(pLigneVente.getCodeRegroupement()); // référence du regroupement (code qui identifie le regroupement)
    if (isEditionSurFactures) {
      entree.setPitp1(""); // top personnalisation (édition)
      entree.setPitp2(""); // top personnalisation (édition)
      entree.setPitp3(""); // top personnalisation (édition)
      entree.setPitp4(""); // top personnalisation (édition)
      entree.setPitp5(""); // top personnalisation facture (position 1) et devis (position 2)
    }
    else {
      if (pDocumentVente.isDevis()) {
        entree.setPitp1("");
        entree.setPitp2("");
        entree.setPitp3("");
        entree.setPitp4("");
        entree.setPitp5(" X");
      }
      else if (pDocumentVente.isCommande()) {
        entree.setPitp1("X ");
        entree.setPitp2("X ");
        entree.setPitp3("");
        entree.setPitp4("");
        entree.setPitp5(" ");
      }
      else if (pDocumentVente.isBon()) {
        entree.setPitp1("");
        entree.setPitp2("");
        entree.setPitp3("X ");
        entree.setPitp4("X ");
        entree.setPitp5("");
      }
      else if (pDocumentVente.isFacture()) {
        entree.setPitp1("");
        entree.setPitp2("");
        entree.setPitp3("");
        entree.setPitp4("");
        entree.setPitp5("X ");
      }
    }
    entree.setPilb(pLigneVente.getLibelle()); // Libellé 1
    entree.setPityp(pDocumentVente.getTypeDocumentVente().getCodeAlpha());
    // Commentaire reporté dans les achats
    if (pConserverSurAchat) {
      entree.setPicpy('X');
    }
    else {
      entree.setPicpy(' ');
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0049_SAUVER_LIGNE_VENTE_COMMENTAIRE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      return sortie.getPonli();
    }
    return 0;
  }
  
  /**
   * Appel du programme RPG qui va créer une ligne avec extraction dans un document.
   */
  public int sauverLigneVenteAvecExtraction(SystemeManager pSysteme, IdLigneVente pIdLigneVenteOrigine,
      IdLigneVente pIdLigneVenteDestination, int pTypeTraitement, BigDecimal pQuantiteDemandee, boolean pAvecRecalculPrix) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pIdLigneVenteOrigine == null) {
      throw new MessageErreurException("Le paramètre pIdLigneArticleOrigine du service est à null.");
    }
    if (pIdLigneVenteDestination == null) {
      throw new MessageErreurException("Le paramètre pIdLigneArticleDestination du service est à null.");
    }
    if (pTypeTraitement < 1 && pTypeTraitement > 3) {
      throw new MessageErreurException("Le paramètre pTypeTraitement a une valeur incorrecte.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0045i entree = new Svgvm0045i();
    Svgvm0045o sortie = new Svgvm0045o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Si on demande le recalcul des prix au jour de l'extraction
    if (pAvecRecalculPrix) {
      entree.setPiind("1000000000");
    }
    else {
      entree.setPiind("0000000000");
    }
    
    // Initialisation des paramètres d'entrée
    entree.setPitrt(pTypeTraitement);
    
    entree.setPietbo(pIdLigneVenteOrigine.getCodeEtablissement());
    entree.setPicodo(pIdLigneVenteOrigine.getCodeEntete().getCode());
    entree.setPinumo(pIdLigneVenteOrigine.getNumero());
    entree.setPisufo(pIdLigneVenteOrigine.getSuffixe());
    entree.setPinlio(pIdLigneVenteOrigine.getNumeroLigne());
    
    entree.setPietb(pIdLigneVenteDestination.getCodeEtablissement());
    entree.setPicod(pIdLigneVenteDestination.getCodeEntete().getCode());
    entree.setPinum(pIdLigneVenteDestination.getNumero());
    entree.setPisuf(pIdLigneVenteDestination.getSuffixe());
    entree.setPinli(pIdLigneVenteDestination.getNumeroLigne());
    
    entree.setPiqte(pQuantiteDemandee.setScale(Svgvm0045i.DECIMAL_PIQTE, RoundingMode.HALF_UP));
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0045_SAUVER_LIGNE_VENTE_AVEC_EXTRACTION, EnvironnementExecution.getGvmas(), parameterList)) {
      
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      // Initialisation de la classe métier
      // Entrée
      // Sortie
    }
    
    return sortie.getPonli();
  }
  
  /**
   * Appel du programme RPG qui va enregistrer une ligne de retour d'article.
   */
  public boolean sauverLigneVenteRetour(SystemeManager pSysteme, BigDecimal pQuantiteRetour, BigDecimal pPrixNetRetour,
      IdMotifRetour pMotifRetour, LigneVente pLigneVente, DocumentVente pDocument) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdLigneVente idLigneVente = LigneVente.controlerId(pLigneVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0055i entree = new Svgvm0055i();
    Svgvm0055o sortie = new Svgvm0055o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiavr(pMotifRetour.getCode().charAt(0));
    // Document d'origine (la ligne)
    entree.setPicodo(idLigneVente.getCodeEntete().getCode());
    entree.setPietbo(idLigneVente.getCodeEtablissement());
    entree.setPinumo(idLigneVente.getNumero());
    entree.setPisufo(idLigneVente.getSuffixe());
    entree.setPinlio(idLigneVente.getNumeroLigne());
    // Document en cours (destination)
    entree.setPicod(pDocument.getId().getEntete());
    entree.setPietb(pDocument.getId().getCodeEtablissement());
    entree.setPinum(pDocument.getId().getNumero());
    entree.setPisuf(pDocument.getId().getSuffixe());
    entree.setPinli(0);
    // Quantité
    entree.setPiqte(pQuantiteRetour);
    // Prix retour
    entree.setPipvn(pPrixNetRetour);
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0055_SAUVER_LIGNE_VENTE_RETOUR, EnvironnementExecution.getGvmas(), parameterList)) {
      
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
    
    return true;
  }
  
  /**
   * Appel du programme RPG qui va créer ou modifier un regroupement de lignes dans un document.
   */
  public void sauverLigneVenteGroupe(SystemeManager pSysteme, Regroupement pRegroupement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pRegroupement == null) {
      throw new MessageErreurException("Le paramètre pRegroupement est à null.");
    }
    if (pRegroupement.getIdLigneVenteDebut() == null) {
      throw new MessageErreurException("L'id de la ligne de début est à null.");
    }
    if (pRegroupement.getIdLigneVenteFin() == null) {
      throw new MessageErreurException("L'id de la ligne de fin est à null.");
    }
    
    String[] decoupageTitre = Constantes.splitString(pRegroupement.getTexteTitre(), 4, LONGUEUR_LIBELLE);
    String[] decoupageTotal = Constantes.splitString(pRegroupement.getTexteTotal(), 4, LONGUEUR_LIBELLE);
    
    // Préparation des paramètres du programme RPG
    Svgvm0028i entree = new Svgvm0028i();
    Svgvm0028o sortie = new Svgvm0028o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    IdLigneVente idLigneVenteDebut = pRegroupement.getIdLigneVenteDebut();
    IdLigneVente idLigneVenteFin = pRegroupement.getIdLigneVenteFin();
    entree.setPietb(idLigneVenteDebut.getCodeEtablissement());
    entree.setPicod(idLigneVenteDebut.getCodeEntete().getCode());
    entree.setPinum(idLigneVenteDebut.getNumero());
    entree.setPisuf(idLigneVenteDebut.getSuffixe());
    // Numéro de ligne origine
    entree.setPinli(idLigneVenteDebut.getNumeroLigne());
    // Numéro de ligne fin (option)
    entree.setPinlif(idLigneVenteFin.getNumeroLigne());
    // Titre Texte 1
    entree.setPitit1(decoupageTitre[0]);
    // Titre Texte 2
    entree.setPitit2(decoupageTitre[1]);
    // Titre Texte 3
    entree.setPitit3(decoupageTitre[2]);
    // Titre Texte 4
    entree.setPitit4(decoupageTitre[3]);
    // Total Texte 1
    entree.setPitot1(decoupageTotal[0]);
    // Total Texte 2
    entree.setPitot2(decoupageTotal[1]);
    // Total Texte 3
    entree.setPitot3(decoupageTotal[2]);
    // Total Texte 4
    entree.setPitot4(decoupageTotal[3]);
    entree.setPilidc(pRegroupement.getNumeroLigneTitre());
    entree.setPilifc(pRegroupement.getNumeroLignePied());
    // Top édition détail articles
    if (pRegroupement.isEditionDetailArticles()) {
      entree.setPiedta('1');
    }
    else {
      entree.setPiedta('0');
    }
    // Top édition prix d"articles
    if (pRegroupement.isEditionPrixArticles()) {
      entree.setPiedtp('1');
    }
    else {
      entree.setPiedtp('0');
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0028_SAUVER_LIGNE_VENTE_GROUPE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va supprimer un regroupement de lignes dans un document.
   */
  public void supprimerLigneVenteGroupe(SystemeManager pSysteme, IdLigneVente pIdLigneVente) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdLigneVente.controlerId(pIdLigneVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0033i entree = new Svgvm0033i();
    Svgvm0033o sortie = new Svgvm0033o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdLigneVente.getCodeEtablissement());
    entree.setPicod(pIdLigneVente.getCodeEntete().getCode());
    entree.setPinum(pIdLigneVente.getNumero());
    entree.setPisuf(pIdLigneVente.getSuffixe());
    entree.setPinli(pIdLigneVente.getNumeroLigne()); // Numéro de ligne origine
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0033_SUPPRIMER_LIGNE_VENTE_GROUPE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appeler le programme RPG qui modifie une ligne dans un document de ventes.
   * 
   * Opérations réalisées :
   * - Chiffrage de la ligne à partir des données en entrée
   * - Mise à jour ligne et extensions en base de données
   * - Chiffrage du document
   */
  public boolean modifierLigneVente(SystemeManager pSysteme, QueryManager pQueryManager, LigneVente pLigneVente) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdLigneVente idLigneVente = LigneVente.controlerId(pLigneVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0027i entree = new Svgvm0027i();
    Svgvm0027o sortie = new Svgvm0027o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(idLigneVente.getCodeEtablissement());
    entree.setPicod(idLigneVente.getCodeEntete().getCode());
    entree.setPinum(idLigneVente.getNumero());
    entree.setPisuf(idLigneVente.getSuffixe());
    entree.setPinli(idLigneVente.getNumeroLigne());
    entree.setPitop(pLigneVente.getCodeEtat());
    entree.setPierl(pLigneVente.getTypeLigne().getCode());
    entree.setPiart(pLigneVente.getIdArticle().getCodeArticle());
    entree.setPiqte(pLigneVente.getQuantiteUV());
    entree.setPiksv(pLigneVente.getNombreUSParUV());
    
    // Renseigner l'origine du prix de vente
    if (pLigneVente.getOriginePrixVente() != null) {
      entree.setPiin18(pLigneVente.getOriginePrixVente().getCode());
    }
    
    // Si la remise vaut 100% alors la remise est initialisée à 0 car il s'agit d'un gratuit
    if (pLigneVente.getTauxRemise1() != null && !pLigneVente.getTauxRemise1().isCent()) {
      entree.setPirem1(pLigneVente.getTauxRemise1().getTauxEnPourcentage());
    }
    else {
      entree.setPirem1(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise2() != null && !pLigneVente.getTauxRemise2().isCent()) {
      entree.setPirem2(pLigneVente.getTauxRemise2().getTauxEnPourcentage());
    }
    else {
      entree.setPirem2(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise3() != null && !pLigneVente.getTauxRemise3().isCent()) {
      entree.setPirem3(pLigneVente.getTauxRemise3().getTauxEnPourcentage());
    }
    else {
      entree.setPirem3(BigDecimal.ZERO);
      
    }
    if (pLigneVente.getTauxRemise4() != null && !pLigneVente.getTauxRemise4().isCent()) {
      entree.setPirem4(pLigneVente.getTauxRemise4().getTauxEnPourcentage());
    }
    else {
      entree.setPirem4(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise5() != null && !pLigneVente.getTauxRemise5().isCent()) {
      entree.setPirem5(pLigneVente.getTauxRemise5().getTauxEnPourcentage());
    }
    else {
      entree.setPirem5(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise6() != null && !pLigneVente.getTauxRemise6().isCent()) {
      entree.setPirem6(pLigneVente.getTauxRemise6().getTauxEnPourcentage());
    }
    else {
      entree.setPirem6(BigDecimal.ZERO);
    }
    
    entree.setPitrl(pLigneVente.getTypeRemise());
    if (pLigneVente.getAssietteTauxRemise() == null) {
      entree.setPibrl(EnumAssietteTauxRemise.PRIX_UNITAIRE.getCode());
    }
    else {
      entree.setPibrl(pLigneVente.getAssietteTauxRemise().getCode());
    }
    // En fonction du type de client (Ht ou TTC), on initialise avec les prix correspondants
    // C'est important afin d'éviter les problèmes de différences de prix lors des conversions HT/TTC
    if (pLigneVente.isClientTTC()) {
      if (pLigneVente.getPrixBaseTTC() == null) {
        entree.setPipvb(BigDecimal.ZERO);
      }
      else {
        entree.setPipvb(pLigneVente.getPrixBaseTTC());
      }
      // Si le prix net vaut 0 alors il est initialisé avec le prix calculé
      BigDecimal prixNetCalculeTTC = pLigneVente.getPrixNetCalculeTTC();
      if (prixNetCalculeTTC == null) {
        prixNetCalculeTTC = BigDecimal.ZERO;
      }
      if (pLigneVente.getPrixNetSaisiTTC() == null || pLigneVente.getPrixNetSaisiTTC().compareTo(BigDecimal.ZERO) == 0) {
        entree.setPipvn(prixNetCalculeTTC);
      }
      else {
        entree.setPipvn(pLigneVente.getPrixNetSaisiTTC());
      }
      entree.setPipvc(prixNetCalculeTTC);
    }
    else {
      if (pLigneVente.getPrixBaseHT() == null) {
        entree.setPipvb(BigDecimal.ZERO);
      }
      else {
        entree.setPipvb(pLigneVente.getPrixBaseHT());
      }
      // Si le prix net vaut 0 alors il est initialisé avec le prix calculé
      BigDecimal prixNetCalculeHT = pLigneVente.getPrixNetCalculeHT();
      if (prixNetCalculeHT == null) {
        prixNetCalculeHT = BigDecimal.ZERO;
      }
      if (pLigneVente.getPrixNetSaisiHT() == null || pLigneVente.getPrixNetSaisiHT().compareTo(BigDecimal.ZERO) == 0) {
        entree.setPipvn(prixNetCalculeHT);
      }
      else {
        entree.setPipvn(pLigneVente.getPrixNetSaisiHT());
      }
      entree.setPipvc(prixNetCalculeHT);
    }
    
    entree.setPimht(pLigneVente.getMontantHT());
    if (pLigneVente.getIdUniteVente() != null) {
      entree.setPiunv(pLigneVente.getIdUniteVente().getCode());
    }
    entree.setPicnd(pLigneVente.getNombreUVParUCV());
    if (pLigneVente.getIdMagasin() != null) {
      entree.setPimag(pLigneVente.getIdMagasin().getCode());
    }
    entree.setPiqtp(pLigneVente.getQuantitePieces());
    entree.setPilb(pLigneVente.getLibelle());
    if (pLigneVente.getProvenanceColonneTarif() != null) {
      entree.setPitt(pLigneVente.getProvenanceColonneTarif().getNumero());
    }
    else {
      entree.setPitt(0);
    }
    entree.setPitar(pLigneVente.getNumeroColonneTarif());
    entree.setPiqt1(pLigneVente.getMesure1());
    entree.setPiqt2(pLigneVente.getMesure2());
    entree.setPiqt3(pLigneVente.getMesure3());
    entree.setPinbr(pLigneVente.getNombreDecoupe());
    if (pLigneVente.getProvenancePrixBase() != null) {
      entree.setPitb(pLigneVente.getProvenancePrixBase().getNumero());
    }
    else {
      entree.setPitb(0);
    }
    if (pLigneVente.getProvenancePrixNet() != null) {
      entree.setPitn(pLigneVente.getProvenancePrixNet().getNumero());
    }
    else {
      entree.setPitn(0);
    }
    if (pLigneVente.getProvenanceTauxRemise() != null) {
      entree.setPitr(pLigneVente.getProvenanceTauxRemise().getNumero());
    }
    else {
      entree.setPitr(0);
    }
    // Dans le cas où le prix de vente a été saisi on prend le prix de la ligne
    if (pLigneVente.isPrixRevientSaisi()) {
      entree.setPiprv(pLigneVente.getPrixDeRevientLigneHT());
    }
    // Sino on prend le prix de revient port inclus que l'on avait conservé
    else {
      entree.setPiprv(pLigneVente.getPrixDeRevientPortIncluHT());
    }
    Character provenancePrixDeRevient = pLigneVente.getProvenancePrixDeRevient();
    if (provenancePrixDeRevient == null) {
      provenancePrixDeRevient = LigneVente.ORIGINE_NON_DEFINI;
    }
    if (provenancePrixDeRevient == LigneVente.ORIGINE_NON_DEFINI) {
      entree.setPiin6(0);
    }
    else {
      entree.setPiin6(Character.digit(pLigneVente.getProvenancePrixDeRevient(), 10));
    }
    if (pLigneVente.getIdAdresseFournisseur() != null) {
      entree.setPicol(pLigneVente.getIdAdresseFournisseur().getIdFournisseur().getCollectif());
      entree.setPifrs(pLigneVente.getIdAdresseFournisseur().getIdFournisseur().getNumero());
      entree.setPifre(pLigneVente.getIdAdresseFournisseur().getIndice());
    }
    if (pLigneVente.getIdTypeGratuit() != null) {
      entree.setPiin2(pLigneVente.getIdTypeGratuit().getCode());
    }
    entree.setPiser(pLigneVente.getTopNumSerieOuLot());
    
    // Sauver les conditions de ventes utilisées par la ligne de ventes avant l'appel du service RPG
    if (pQueryManager != null) {
      List<InformationConditionVenteLigneVente> listeConditionVenteLigneVente = pLigneVente.getListeConditionVenteLigneVente();
      if (listeConditionVenteLigneVente != null && !listeConditionVenteLigneVente.isEmpty()) {
        SqlPrixVente sqlPrixVente = new SqlPrixVente(pQueryManager);
        sqlPrixVente.sauverConditionVenteLigneVente(pLigneVente.getId(), listeConditionVenteLigneVente);
      }
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0027_MODIFIER_LIGNE_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
    
    return true;
  }
  
  /**
   * Appeler le programme RPG qui controle une ligne dans un document de ventes.
   * 
   * Opérations effectuées :
   * - Chiffrage d'une ligne
   * - Contrôle ligne
   * - Retour ligne modifiée et erreur éventuelle
   */
  public AlertesLigneDocument controlerLigneVente(SystemeManager pSysteme, LigneVente pLigneVente, Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdLigneVente idLigneVente = LigneVente.controlerId(pLigneVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0004i entree = new Svgvm0004i();
    Svgvm0004o sortie = new Svgvm0004o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind(pLigneVente.getIndicateurs());
    entree.setPicod(idLigneVente.getCodeEntete().getCode());
    entree.setPietb(idLigneVente.getCodeEtablissement());
    entree.setPinum(idLigneVente.getNumero());
    entree.setPisuf(idLigneVente.getSuffixe());
    entree.setPinli(idLigneVente.getNumeroLigne());
    entree.setPidat(pDateTraitement);
    entree.setPiopt(LigneVente.OPTION_VALIDATION);
    
    // PIL2xxx, valeurs zones Surface/Volume saisies et gérées par XLI n° 7
    entree.setPil2qt1(pLigneVente.getMesure1());
    entree.setPil2qt2(pLigneVente.getMesure2());
    entree.setPil2qt3(pLigneVente.getMesure3());
    entree.setPil2nbr(pLigneVente.getNombreDecoupe());
    
    // PIL22xx, valeurs zones Date saisies et gérées par XLI n° 22
    entree.setPil22dls(pLigneVente.getDateLivraisonPrevue());
    
    // Description PGVMLBCM
    entree.setLitop(pLigneVente.getCodeEtat());
    entree.setLicod(idLigneVente.getCodeEntete().getCode());
    entree.setLietb(idLigneVente.getCodeEtablissement());
    entree.setLinum(idLigneVente.getNumero());
    entree.setLisuf(idLigneVente.getSuffixe());
    entree.setLinli(idLigneVente.getNumeroLigne());
    entree.setLierl(pLigneVente.getTypeLigne().getCode());
    // En attendant les enums
    if (pLigneVente.getCodeExtraction() != null) {
      entree.setLicex(pLigneVente.getCodeExtraction());
    }
    entree.setLiqex(pLigneVente.getQuantiteAvantExtraction());
    entree.setLitva(pLigneVente.getCodeTVA());
    if (pLigneVente.getProvenancePrixBase() != null) {
      entree.setLitb(pLigneVente.getProvenancePrixBase().getNumero());
    }
    else {
      entree.setLitb(0);
    }
    if (pLigneVente.getProvenanceTauxRemise() != null) {
      entree.setLitr(pLigneVente.getProvenanceTauxRemise().getNumero());
    }
    else {
      entree.setLitr(0);
    }
    if (pLigneVente.getProvenanceTauxRemise() != null) {
      entree.setLitr(pLigneVente.getProvenanceTauxRemise().getNumero());
    }
    else {
      entree.setLitr(0);
    }
    if (pLigneVente.getProvenancePrixNet() != null) {
      entree.setLitn(pLigneVente.getProvenancePrixNet().getNumero());
    }
    else {
      entree.setLitn(0);
    }
    if (pLigneVente.getProvenanceCoefficient() != null) {
      entree.setLitc(pLigneVente.getProvenanceCoefficient().getNumero());
    }
    else {
      entree.setLitc(0);
    }
    if (pLigneVente.getProvenanceColonneTarif() != null) {
      entree.setLitt(pLigneVente.getProvenanceColonneTarif().getNumero());
    }
    else {
      entree.setLitt(0);
    }
    entree.setLival(pLigneVente.getTopLigneEnValeur());
    entree.setLitar(pLigneVente.getNumeroColonneTarif());
    entree.setLicol(pLigneVente.getNumeroColonneTVA());
    entree.setLitpf(pLigneVente.getCodeTPF());
    entree.setLidcv(pLigneVente.getNombreDecimaleUV());
    entree.setLitnc(pLigneVente.getTopNonCommissionne());
    entree.setLisgn(pLigneVente.getSigneLigne());
    entree.setLiser(pLigneVente.getTopNumSerieOuLot());
    entree.setLiqte(pLigneVente.getQuantiteUV());
    entree.setLiksv(pLigneVente.getNombreUSParUV());
    
    // Si la remise vaut 100% alors la remise est initialisée à 0 car il s'agit d'un gratuit
    if (pLigneVente.getTauxRemise1() != null && !pLigneVente.getTauxRemise1().isCent()) {
      entree.setLirem1(pLigneVente.getTauxRemise1().getTauxEnPourcentage());
    }
    else {
      entree.setLirem1(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise2() != null && !pLigneVente.getTauxRemise2().isCent()) {
      entree.setLirem2(pLigneVente.getTauxRemise2().getTauxEnPourcentage());
    }
    else {
      entree.setLirem2(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise3() != null && !pLigneVente.getTauxRemise3().isCent()) {
      entree.setLirem3(pLigneVente.getTauxRemise3().getTauxEnPourcentage());
    }
    else {
      entree.setLirem3(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise4() != null && !pLigneVente.getTauxRemise4().isCent()) {
      entree.setLirem4(pLigneVente.getTauxRemise4().getTauxEnPourcentage());
    }
    else {
      entree.setLirem4(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise5() != null && !pLigneVente.getTauxRemise5().isCent()) {
      entree.setLirem5(pLigneVente.getTauxRemise5().getTauxEnPourcentage());
    }
    else {
      entree.setLirem5(BigDecimal.ZERO);
    }
    if (pLigneVente.getTauxRemise6() != null && !pLigneVente.getTauxRemise6().isCent()) {
      entree.setLirem6(pLigneVente.getTauxRemise6().getTauxEnPourcentage());
    }
    else {
      entree.setLirem6(BigDecimal.ZERO);
    }
    
    // En attendant les enums ou modification de la classe
    entree.setLitrl(pLigneVente.getTypeRemise());
    // En attendant les enums ou modification de la classe
    if (pLigneVente.getAssietteTauxRemise() == null) {
      entree.setLibrl(EnumAssietteTauxRemise.PRIX_UNITAIRE.getCode());
    }
    else {
      entree.setLibrl(pLigneVente.getAssietteTauxRemise().getCode());
    }
    // En attendant les enums ou modification de la classe
    entree.setLirp1(pLigneVente.getExclusionRemisePied1());
    // En attendant les enums ou modification de la classe
    entree.setLirp2(pLigneVente.getExclusionRemisePied2());
    // En attendant les enums ou modification de la classe
    entree.setLirp3(pLigneVente.getExclusionRemisePied3());
    // En attendant les enums ou modification de la classe
    entree.setLirp4(pLigneVente.getExclusionRemisePied4());
    // En attendant les enums ou modification de la classe
    entree.setLirp5(pLigneVente.getExclusionRemisePied5());
    // En attendant les enums ou modification de la classe
    entree.setLirp6(pLigneVente.getExclusionRemisePied6());
    // Il faut fournir les prix en HT même pour des clients TTC
    if (pLigneVente.getPrixNetSaisiHT() == null) {
      entree.setLipvn(BigDecimal.ZERO);
    }
    else {
      entree.setLipvn(pLigneVente.getPrixNetSaisiHT());
    }
    if (pLigneVente.getPrixNetCalculeHT() == null) {
      entree.setLipvc(BigDecimal.ZERO);
    }
    else {
      entree.setLipvc(pLigneVente.getPrixNetCalculeHT());
    }
    if (pLigneVente.getPrixBaseHT() == null) {
      entree.setLipvb(BigDecimal.ZERO);
    }
    else {
      entree.setLipvb(pLigneVente.getPrixBaseHT());
    }
    Character provenancePrixDeRevient = pLigneVente.getProvenancePrixDeRevient();
    if (provenancePrixDeRevient == null) {
      provenancePrixDeRevient = LigneVente.ORIGINE_NON_DEFINI;
    }
    if (provenancePrixDeRevient == LigneVente.ORIGINE_NON_DEFINI) {
      entree.setLiin6(' ');
    }
    else {
      entree.setLiin6(provenancePrixDeRevient);
    }
    
    entree.setLimht(pLigneVente.getMontantHT());
    entree.setLiprp(pLigneVente.getPrixPromo());
    entree.setLicoe(pLigneVente.getCoefficient());
    entree.setLiprv(pLigneVente.getPrixDeRevientLigneHT());
    // En attendant les enums ou modification de la classe
    entree.setLiavr(pLigneVente.getCodeLigneAvoir());
    if (pLigneVente.getIdUniteVente() != null) {
      entree.setLiunv(pLigneVente.getIdUniteVente().getCode());
    }
    entree.setLiart(pLigneVente.getIdArticle().getCodeArticle());
    entree.setLicpl(pLigneVente.getInfoComplementaire());
    entree.setLicnd(pLigneVente.getNombreUVParUCV());
    // En attendant les enums ou modification de la classe
    entree.setLiin1(pLigneVente.getNonSoumisEscompte());
    entree.setLiprt(pLigneVente.getPrixTransport());
    entree.setLidlp((pLigneVente.getDateLivraisonPrevue()));
    // En attendant les enums ou modification de la classe
    if (pLigneVente.getIdTypeGratuit() != null) {
      entree.setLiin2(pLigneVente.getIdTypeGratuit().getCode());
    }
    // En attendant les enums ou modification de la classe
    entree.setLiin3(pLigneVente.getCodeRegroupement());
    entree.setLitp1(pLigneVente.getTopPersonnalisation1());
    entree.setLitp2(pLigneVente.getTopPersonnalisation2());
    entree.setLitp3(pLigneVente.getTopPersonnalisation3());
    entree.setLitp4(pLigneVente.getTopPersonnalisation4());
    entree.setLitp5(pLigneVente.getTopPersonnalisation5());
    entree.setLigba(pLigneVente.getGenerationBonAchat());
    if (pLigneVente.getIdArticleSubstitue() != null) {
      entree.setLiarts(pLigneVente.getIdArticleSubstitue().getCodeArticle());
    }
    // En attendant les enums ou modification de la classe
    entree.setLiin4(pLigneVente.getTypeSubstitution());
    // En attendant les enums ou modification de la classe
    entree.setLiin5(pLigneVente.getRemise50Commercial());
    // En attendant les enums ou modification de la classe
    entree.setLiin7(pLigneVente.getIndRecherchePrixKit());
    entree.setLiqtl(pLigneVente.getQuantiteLivrable());
    if (pLigneVente.getIdMagasin() != null) {
      entree.setLimag(pLigneVente.getIdMagasin().getCode());
    }
    if (pLigneVente.getIdRepresentant() != null) {
      entree.setLirep(pLigneVente.getIdRepresentant().getCode());
    }
    
    // En attendant les enums ou modification de la classe
    entree.setLiin8(pLigneVente.getIndiceLivraison());
    // En attendant les enums ou modification de la classe
    entree.setLiin9(pLigneVente.getIndCumulL1QTE());
    // En attendant les enums ou modification de la classe
    entree.setLiin10(pLigneVente.getIndCumulL1QTP());
    // En attendant les enums ou modification de la classe
    entree.setLiin11(pLigneVente.getIndLigneAExtraire());
    // En attendant les enums ou modification de la classe
    entree.setLiin12(pLigneVente.getIndASDI());
    entree.setLinli0(pLigneVente.getNumeroLigneOrigine());
    entree.setLipra(pLigneVente.getPrixAAjouterPrixRevient());
    entree.setLipva(pLigneVente.getPrixAAjouterPrixVenteClient());
    entree.setLiqtp(pLigneVente.getQuantitePieces());
    entree.setLimtr(pLigneVente.getMontantTransport());
    // En attendant les enums ou modification de la classe
    entree.setLiser3(pLigneVente.getTopArticleLoti());
    // En attendant les enums ou modification de la classe
    entree.setLiser5(pLigneVente.getTopArticleADS());
    // En attendant les enums ou modification de la classe
    entree.setLiin17(pLigneVente.getIndTypeVente());
    
    // Renseigner l'origine du prix de vente
    if (pLigneVente.getOriginePrixVente() != null) {
      entree.setLiin18(pLigneVente.getOriginePrixVente().getCode());
    }
    
    // En attendant les enums ou modification de la classe
    entree.setLiin19(pLigneVente.getIndApplicationConditionQuantitative());
    // Le type de la condition chantier
    if (pLigneVente.getTypeConditionChantier() == null) {
      entree.setLiin20(' ');
    }
    else {
      entree.setLiin20(pLigneVente.getTypeConditionChantier().getCode());
    }
    // En attendant les enums ou modification de la classe
    entree.setLiin21(pLigneVente.getCodeRegroupement());
    entree.setLiin22(' ');
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0004_CONTROLER_LIGNE_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      return null;
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Initialisation de la classe métier
    pLigneVente.setId(idLigneVente);
    pLigneVente.setCodeNumSerieOuLot(sortie.getPowser());
    pLigneVente.setCodeEtat(sortie.getLotop());
    pLigneVente.setTypeLigne(EnumTypeLigneVente.valueOfByCode(sortie.getLoerl()));
    pLigneVente.setCodeExtraction(sortie.getLocex());
    pLigneVente.setQuantiteAvantExtraction(sortie.getLoqex());
    pLigneVente.setCodeTVA(sortie.getLotva());
    pLigneVente.setTopLigneEnValeur(sortie.getLoval());
    pLigneVente.setNumeroColonneTVA(sortie.getLocol());
    pLigneVente.setCodeTPF(sortie.getLotpf());
    
    // Précision de l'unité de ventes
    int nombreDecimaleUV = sortie.getLodcv();
    pLigneVente.setNombreDecimaleUV(nombreDecimaleUV);
    
    pLigneVente.setTopNonCommissionne(sortie.getLotnc());
    pLigneVente.setSigneLigne(sortie.getLosgn());
    pLigneVente.setTopNumSerieOuLot(sortie.getLoser());
    pLigneVente.setNombreUSParUV(sortie.getLoksv());
    pLigneVente.setTypeRemise(sortie.getLotrl());
    try {
      pLigneVente.setAssietteTauxRemise(EnumAssietteTauxRemise.valueOfByCode(sortie.getLobrl()));
    }
    catch (Exception e) {
    }
    pLigneVente.setExclusionRemisePied1(sortie.getLorp1());
    pLigneVente.setExclusionRemisePied2(sortie.getLorp2());
    pLigneVente.setExclusionRemisePied3(sortie.getLorp3());
    pLigneVente.setExclusionRemisePied4(sortie.getLorp4());
    pLigneVente.setExclusionRemisePied5(sortie.getLorp5());
    pLigneVente.setExclusionRemisePied6(sortie.getLorp6());
    pLigneVente.setPrixPromo(sortie.getLoprp());
    pLigneVente.setCodeLigneAvoir(sortie.getLoavr());
    if (!sortie.getLounv().isEmpty()) {
      IdUnite idUV = IdUnite.getInstance(sortie.getLounv());
      pLigneVente.setIdUniteVente(idUV);
    }
    IdArticle idArticle = IdArticle.getInstance(pLigneVente.getIdArticle().getIdEtablissement(), sortie.getLoart());
    pLigneVente.setIdArticle(idArticle);
    pLigneVente.setInfoComplementaire(sortie.getLocpl());
    pLigneVente.setNombreUVParUCV(sortie.getLocnd());
    pLigneVente.setNonSoumisEscompte(sortie.getLoin1());
    pLigneVente.setPrixTransport(sortie.getLoprt());
    pLigneVente.setDateLivraisonPrevue(sortie.getLodlpConvertiEnDate());
    Character codeTypeGratuit = sortie.getLoin2();
    if (codeTypeGratuit != null) {
      pLigneVente.setIdTypeGratuit(IdTypeGratuit.getInstance(codeTypeGratuit));
    }
    pLigneVente.setCodeRegroupement(sortie.getLoin21());
    pLigneVente.setTopPersonnalisation1(sortie.getLotp1());
    pLigneVente.setTopPersonnalisation2(sortie.getLotp2());
    pLigneVente.setTopPersonnalisation3(sortie.getLotp3());
    pLigneVente.setTopPersonnalisation4(sortie.getLotp4());
    pLigneVente.setTopPersonnalisation5(sortie.getLotp5());
    pLigneVente.setGenerationBonAchat(sortie.getLogba());
    if (!sortie.getLoarts().trim().isEmpty()) {
      IdArticle idArticleSubstitue = IdArticle.getInstance(idLigneVente.getIdEtablissement(), sortie.getLoarts());
      pLigneVente.setIdArticleSubstitue(idArticleSubstitue);
    }
    pLigneVente.setTypeSubstitution(sortie.getLoin4());
    pLigneVente.setRemise50Commercial(sortie.getLoin5());
    pLigneVente.setIndRecherchePrixKit(sortie.getLoin7());
    pLigneVente.setQuantiteLivrable(sortie.getLoqtl());
    if (!sortie.getLomag().trim().isEmpty()) {
      pLigneVente.setIdMagasin(IdMagasin.getInstance(idLigneVente.getIdEtablissement(), sortie.getLomag()));
    }
    if (!sortie.getLorep().trim().isEmpty()) {
      pLigneVente.setIdRepresentant(IdRepresentant.getInstance(idLigneVente.getIdEtablissement(), sortie.getLorep()));
    }
    pLigneVente.setIndiceLivraison(sortie.getLoin8());
    pLigneVente.setIndCumulL1QTE(sortie.getLoin9());
    pLigneVente.setIndCumulL1QTP(sortie.getLoin10());
    pLigneVente.setIndLigneAExtraire(sortie.getLoin11());
    pLigneVente.setIndASDI(sortie.getLoin12());
    pLigneVente.setNumeroLigneOrigine(sortie.getLonli0());
    pLigneVente.setPrixAAjouterPrixRevient(sortie.getLopra());
    pLigneVente.setPrixAAjouterPrixVenteClient(sortie.getLopva());
    pLigneVente.setQuantitePieces(sortie.getLoqtp());
    pLigneVente.setMontantTransport(sortie.getLomtr());
    pLigneVente.setTopArticleLoti(sortie.getLoser3());
    pLigneVente.setTopArticleADS(sortie.getLoser5());
    pLigneVente.setIndTypeVente(sortie.getLoin17());
    pLigneVente.setOriginePrixVente(EnumOriginePrixVente.valueOfByCode(sortie.getLoin18()));
    pLigneVente.setIndApplicationConditionQuantitative(sortie.getLoin19());
    
    // Le type de la condition chantier
    pLigneVente.setTypeConditionChantier(EnumTypeChantier.valueOfByCode(sortie.getLoin20()));
    
    // Chargement des données concernants le prix de vente
    if (!pLigneVente.isLigneCommentaire()) {
      // Quantité en UV, arrondie avec la précision stockée dans la ligne de ventes
      pLigneVente.setQuantiteUV(sortie.getLoqte().setScale(nombreDecimaleUV));
      pLigneVente.setNumeroColonneTarif(sortie.getLotar());
      try {
        pLigneVente.setProvenanceColonneTarif(EnumProvenanceColonneTarif.valueOfByCode(sortie.getLott()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      // pLigneVente.setPrixPublicHT(BigDecimal.ZERO);
      // pLigneVente.setPrixPublicTTC(BigDecimal.ZERO);
      pLigneVente.setPrixBaseHT(sortie.getLopvb());
      try {
        pLigneVente.setProvenancePrixBase(EnumProvenancePrixBase.valueOfByCode(sortie.getLotb()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      pLigneVente.setPrixNetSaisiHT(sortie.getLopvn());
      try {
        pLigneVente.setProvenancePrixNet(EnumProvenancePrixNet.valueOfByCode(sortie.getLotn()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      pLigneVente.setPrixNetCalculeHT(sortie.getLopvc());
      pLigneVente.setMontantHT(sortie.getLomht());
      // Remise unitaire
      pLigneVente.setTauxRemise1(new BigPercentage(sortie.getLorem1(), EnumFormatPourcentage.POURCENTAGE));
      pLigneVente.setTauxRemise2(new BigPercentage(sortie.getLorem2(), EnumFormatPourcentage.POURCENTAGE));
      pLigneVente.setTauxRemise3(new BigPercentage(sortie.getLorem3(), EnumFormatPourcentage.POURCENTAGE));
      pLigneVente.setTauxRemise4(new BigPercentage(sortie.getLorem4(), EnumFormatPourcentage.POURCENTAGE));
      pLigneVente.setTauxRemise5(new BigPercentage(sortie.getLorem5(), EnumFormatPourcentage.POURCENTAGE));
      pLigneVente.setTauxRemise6(new BigPercentage(sortie.getLorem6(), EnumFormatPourcentage.POURCENTAGE));
      try {
        pLigneVente.setProvenanceTauxRemise(EnumProvenanceTauxRemise.valueOfByCode(sortie.getLotr()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      // Coefficient
      pLigneVente.setCoefficient(sortie.getLocoe());
      try {
        pLigneVente.setProvenanceCoefficient(EnumProvenanceCoefficient.valueOfByCode(sortie.getLotc()));
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
      // Prix de revient
      pLigneVente.setProvenancePrixDeRevient(sortie.getLoin6());
      pLigneVente.setPrixDeRevientStandardHT(BigDecimal.ZERO);
      pLigneVente.setPrixDeRevientStandardTTC(BigDecimal.ZERO);
      pLigneVente.setPrixDeRevientPortIncluHT(sortie.getLoprv());
      // Si le prix de revient a été saisi alors on prend cette valeur
      if (pLigneVente.isPrixRevientSaisi()) {
        pLigneVente.setPrixDeRevientLigneHT(sortie.getLoprv());
      }
      // Sinon on l'initialise avec le prix de revient standard
      else {
        pLigneVente.setPrixDeRevientLigneHT(BigDecimal.ZERO);
        pLigneVente.setPrixDeRevientLigneTTC(BigDecimal.ZERO);
      }
    }
    
    // Les variables d'alertes
    AlertesLigneDocument alertesLigneDocument = new AlertesLigneDocument();
    
    initialiserAlerteLigneVente(alertesLigneDocument, sortie);
    return alertesLigneDocument;
  }
  
  /**
   * Initialise les alertes après l'appel du programme RPG.
   */
  private void initialiserAlerteLigneVente(AlertesLigneDocument pAlertes, Svgvm0004o pSortie) {
    // Quantité = à une des quantités suspectes du paramètre unité
    if (pSortie.getLoer01().intValue() == 1) {
      pAlertes.setQuantiteEgaleQuantiteSuspecteParamUnite(true);
    }
    // Quantité supérieure à la quantité maximum du paramètre unité
    if (pSortie.getLoer02().intValue() == 1) {
      pAlertes.setQuantiteSuperieureAuMaxDefiniParamUnite(true);
    }
    // Article en réappro non géré Qté < conditionnement
    if (pSortie.getLoer03().intValue() == 1) {
      pAlertes.setArticleEnReapproNonGere(true);
      pAlertes.setQuantiteConditionnement(pSortie.getLoer03a());
    }
    // Stock non disponible.Disponible sur magasin
    if (pSortie.getLoer04().intValue() == 1) {
      pAlertes.setStockNonDisponible_DisponibleEnMagasin(true);
      if (!pSortie.getLoer04a().trim().isEmpty()) {
        pAlertes.setIdMagasin(IdMagasin.getInstance(IdEtablissement.getInstance(pSortie.getLoetb()), pSortie.getLoer04a()));
      }
    }
    // Stock non disponible
    if (pSortie.getLoer05().intValue() == 1) {
      pAlertes.setStockNonDisponible(true);
    }
    // Quantité importante
    if (pSortie.getLoer06().intValue() == 1) {
      pAlertes.setQuantiteImportante(true);
    }
    // Commande éléphant
    if (pSortie.getLoer07() == 1) {
      pAlertes.setCommandeElephant(true);
    }
    // Quantité limite dépassée
    if (pSortie.getLoer08().intValue() == 1) {
      pAlertes.setQuantiteLimiteDepassee(true);
      pAlertes.setQuantiteLimite(pSortie.getLoer08a());
    }
    // Conditionnement, quantité forcée à
    if (pSortie.getLoer09().intValue() == 1) {
      pAlertes.setConditionnementEtQuantiteForcee(true);
      pAlertes.setConditionnement(pSortie.getLoer09a());
      pAlertes.setQuantiteForcee(pSortie.getLoer09b());
    }
    // Encours dépassé
    if (pSortie.getLoer10() == 1) {
      pAlertes.setEncoursDepasse(true);
    }
  }
  
  /**
   * Appeler le programme RPG qui initialise les informations d'une ligne de ventes
   * 
   * Ce service retourne les informations permettant d'initialiser une ligne de vente. A ce stade, la ligne de vente n'est pas encore
   * persitée dans la base de données. Elle le sera partir de l'appel au service sauverLigneVente(). Les informations sont issues
   * de l'entête du document de ventes et de l'article. Ce service est le premier à appeler lors d'une création de ligne de ventes.
   * 
   * Opérations effectuées :
   * - Contrôle article, stock...
   * - Chiffrage de ligne
   * - Retour ligne à créer ou erreur éventuelle
   */
  public LigneVente initialiserLigneVente(SystemeManager pSysteme, IdLigneVente pIdLigneVente, IdArticle pIdArticle,
      Date pDateTraitement) {
    // Vérifier les paramètres
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre système du service est invalide.");
    }
    IdLigneVente.controlerId(pIdLigneVente, true);
    IdArticle.controlerId(pIdArticle, true);
    
    // Préparer les paramètres du programme RPG
    Svgvm0005i entree = new Svgvm0005i();
    Svgvm0005o sortie = new Svgvm0005o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialiser les paramètres d'entrée
    entree.setPietb(pIdLigneVente.getCodeEtablissement());
    entree.setPicod(pIdLigneVente.getCodeEntete().getCode());
    entree.setPinum(pIdLigneVente.getNumero());
    entree.setPisuf(pIdLigneVente.getSuffixe());
    entree.setPinli(pIdLigneVente.getNumeroLigne());
    entree.setPiart(pIdArticle.getCodeArticle());
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPiopt(LigneVente.OPTION_VALIDATION);
    
    // Initialiser les paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparer l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécuter le programme RPG
    if (!rpg.executerProgramme(SVGVM0005_INITIALISER_LIGNE_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors de l'initialisation de la ligne de ventes pour l'article : " + pIdArticle);
    }
    
    // Récupération les retours du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Contrôler les erreurs
    controlerErreur();
    
    // Renseigner la ligne
    LigneVente ligneVente = new LigneVente(pIdLigneVente);
    ligneVente.setCodeNumSerieOuLot(sortie.getPowser());
    ligneVente.setAffectationStock(sortie.getPow60a10());
    ligneVente.setMesure1(sortie.getPol2qt1());
    ligneVente.setMesure2(sortie.getPol2qt2());
    ligneVente.setMesure3(sortie.getPol2qt3());
    ligneVente.setNombreDecoupe(sortie.getPol2nbr());
    if (sortie.getPoc3pvn().compareTo(BigDecimal.ZERO) == 1) {
      PrixFlash prixFlash = new PrixFlash();
      prixFlash.setIdArticle(ligneVente.getIdArticle());
      prixFlash.setPrixVenteFlash(sortie.getPoc3pvn());
      prixFlash.setCodeUtilisateurFlash(sortie.getPoc3user().trim());
      prixFlash.setDatePrixFlash(ConvertDate.db2ToDate(sortie.getPoc3date(), sortie.getPoc3heure(), null));
      prixFlash.setHeurePrixFlash(sortie.getPoc3heure());
      ligneVente.setPrixFlash(prixFlash);
    }
    ligneVente.setCodeEtat(sortie.getLotop());
    ligneVente.setTypeLigne(EnumTypeLigneVente.valueOfByCode(sortie.getLoerl()));
    ligneVente.setCodeExtraction(sortie.getLocex());
    ligneVente.setQuantiteAvantExtraction(sortie.getLoqex());
    ligneVente.setCodeTVA(sortie.getLotva());
    ligneVente.setTopLigneEnValeur(sortie.getLoval());
    ligneVente.setNumeroColonneTVA(sortie.getLocol());
    ligneVente.setCodeTPF(sortie.getLotpf());
    
    // Précision de l'unité de ventes
    int nombreDecimaleUV = sortie.getLodcv();
    ligneVente.setNombreDecimaleUV(nombreDecimaleUV);
    
    ligneVente.setTopNonCommissionne(sortie.getLotnc());
    ligneVente.setSigneLigne(sortie.getLosgn());
    ligneVente.setTopNumSerieOuLot(sortie.getLoser());
    ligneVente.setNombreUSParUV(sortie.getLoksv());
    ligneVente.setTypeRemise(sortie.getLotrl());
    try {
      ligneVente.setAssietteTauxRemise(EnumAssietteTauxRemise.valueOfByCode(sortie.getLobrl()));
    }
    catch (Exception e) {
    }
    ligneVente.setExclusionRemisePied1(sortie.getLorp1());
    ligneVente.setExclusionRemisePied2(sortie.getLorp2());
    ligneVente.setExclusionRemisePied3(sortie.getLorp3());
    ligneVente.setExclusionRemisePied4(sortie.getLorp4());
    ligneVente.setExclusionRemisePied5(sortie.getLorp5());
    ligneVente.setExclusionRemisePied6(sortie.getLorp6());
    ligneVente.setPrixPromo(sortie.getLoprp());
    ligneVente.setCodeLigneAvoir(sortie.getLoavr());
    if (!sortie.getLounv().trim().isEmpty()) {
      IdUnite idUnite = IdUnite.getInstance(sortie.getLounv());
      ligneVente.setIdUniteVente(idUnite);
    }
    IdArticle idArticle = IdArticle.getInstance(ligneVente.getId().getIdEtablissement(), sortie.getLoart());
    ligneVente.setIdArticle(idArticle);
    ligneVente.setInfoComplementaire(sortie.getLocpl());
    ligneVente.setNombreUVParUCV(sortie.getLocnd());
    ligneVente.setNonSoumisEscompte(sortie.getLoin1());
    ligneVente.setPrixTransport(sortie.getLoprt());
    ligneVente.setDateLivraisonPrevue(sortie.getLodlpConvertiEnDate());
    Character codeTypeGratuit = sortie.getLoin2();
    if (codeTypeGratuit != null) {
      ligneVente.setIdTypeGratuit(IdTypeGratuit.getInstance(codeTypeGratuit));
    }
    ligneVente.setTopPersonnalisation1(sortie.getLotp1().trim());
    ligneVente.setTopPersonnalisation2(sortie.getLotp2().trim());
    ligneVente.setTopPersonnalisation3(sortie.getLotp3().trim());
    ligneVente.setTopPersonnalisation4(sortie.getLotp4().trim());
    ligneVente.setTopPersonnalisation5(sortie.getLotp5().trim());
    ligneVente.setGenerationBonAchat(sortie.getLogba());
    if (!sortie.getLoarts().trim().isEmpty()) {
      IdArticle idArticleSubstitue = IdArticle.getInstance(ligneVente.getId().getIdEtablissement(), sortie.getLoarts());
      ligneVente.setIdArticleSubstitue(idArticleSubstitue);
    }
    ligneVente.setTypeSubstitution(sortie.getLoin4());
    ligneVente.setRemise50Commercial(sortie.getLoin5());
    ligneVente.setIndRecherchePrixKit(sortie.getLoin7());
    ligneVente.setQuantiteLivrable(sortie.getLoqtl());
    if (!sortie.getLomag().trim().isEmpty()) {
      ligneVente.setIdMagasin(IdMagasin.getInstance(ligneVente.getId().getIdEtablissement(), sortie.getLomag()));
    }
    if (!sortie.getLorep().trim().isEmpty()) {
      ligneVente.setIdRepresentant(IdRepresentant.getInstance(ligneVente.getId().getIdEtablissement(), sortie.getLorep()));
    }
    ligneVente.setIndiceLivraison(sortie.getLoin8());
    ligneVente.setIndCumulL1QTE(sortie.getLoin9());
    ligneVente.setIndCumulL1QTP(sortie.getLoin10());
    ligneVente.setIndLigneAExtraire(sortie.getLoin11());
    ligneVente.setIndASDI(sortie.getLoin12());
    ligneVente.setNumeroLigneOrigine(sortie.getLonli0());
    ligneVente.setPrixAAjouterPrixRevient(sortie.getLopra());
    ligneVente.setPrixAAjouterPrixVenteClient(sortie.getLopva());
    ligneVente.setQuantitePieces(sortie.getLoqtp());
    ligneVente.setMontantTransport(sortie.getLomtr());
    ligneVente.setTopArticleLoti(sortie.getLoser3());
    ligneVente.setTopArticleADS(sortie.getLoser5());
    ligneVente.setIndTypeVente(sortie.getLoin17());
    ligneVente.setIndApplicationConditionQuantitative(sortie.getLoin19());
    // Le type de la condition chantier
    ligneVente.setTypeConditionChantier(EnumTypeChantier.valueOfByCode(sortie.getLoin20()));
    
    ligneVente.setTypeArticle(EnumTypeArticle.valueOfByCodeAlpha(sortie.getWtypart().trim()));
    if (!sortie.getWunl().trim().isEmpty()) {
      ligneVente.setIdUniteConditionnementVente(IdUnite.getInstance(sortie.getWunl()));
    }
    ligneVente.setLibelle(sortie.getWlibg());
    
    // Chargement des données concernants le prix de vente
    if (!ligneVente.isLigneCommentaire()) {
      // Seules les informations sur le prix de revient sont récupérées du RPG, le reste sera initialisé côté desktop via CalculPrixVente
      ligneVente.setProvenancePrixDeRevient(sortie.getLoin6());
      ligneVente.setPrixDeRevientStandardHT(sortie.getLoprs());
      ligneVente.setPrixDeRevientPortIncluHT(sortie.getLoprv());
      // Si le prix de revient a été saisi alors on prend cette valeur
      if (ligneVente.isPrixRevientSaisi()) {
        ligneVente.setPrixDeRevientLigneHT(sortie.getLoprv());
      }
      // Sinon on l'initialise avec le prix de revient standard
      else {
        ligneVente.setPrixDeRevientLigneHT(sortie.getLoprs());
      }
      // Coefficent
      ligneVente.setCoefficient(sortie.getLocoe());
    }
    
    // Les variables d'alertes
    ligneVente.setAlertesLigneDocument(new AlertesLigneDocument());
    
    initialiserAlerteLigneVente(ligneVente.getAlertesLigneDocument(), sortie);
    
    return ligneVente;
  }
  
  /**
   * Initialise les alertes après l'appel du programme RPG.
   */
  private void initialiserAlerteLigneVente(AlertesLigneDocument pAlertes, Svgvm0005o pSortie) {
    // Fin de Réappro. pour l'article
    if (pSortie.getLoer01().compareTo(1) == 0) {
      pAlertes.setFinReapproPourArticle(true);
    }
    // Restauration avant extraction
    if (pSortie.getLoer02().compareTo(1) == 0) {
      pAlertes.setRestaurationAvantExtraction(true);
    }
    // Type de suivi inexistant
    if (pSortie.getLoer03().compareTo(1) == 0) {
      pAlertes.setTypeSuiviInexistant(true);
    }
    // Article déprécié à
    if (pSortie.getLoer04().compareTo(1) == 0) {
      pAlertes.setArticleDeprecie(true);
      pAlertes.setPourcentageDepreciation(pSortie.getLoer04a());
    }
  }
}
