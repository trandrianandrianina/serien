/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database.files;

import java.sql.Timestamp;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;

public abstract class FFD_Psemhflxm extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_FLID = 9;
  public static final int DECIMAL_FLID = 0;
  public static final int SIZE_FLIDF = 6;
  public static final int SIZE_FLETB = 3;
  public static final int SIZE_FLVER = 7;
  public static final int SIZE_FLCRE = 26;
  public static final int DECIMAL_FLCRE = 0;
  public static final int SIZE_FLCOD = 50;
  public static final int SIZE_FLSNS = 9;
  public static final int DECIMAL_FLSNS = 0;
  public static final int SIZE_FLSTT = 9;
  public static final int DECIMAL_FLSTT = 0;
  public static final int SIZE_FLTRT = 26;
  public static final int DECIMAL_FLTRT = 0;
  public static final int SIZE_FLCPT = 9;
  public static final int DECIMAL_FLCPT = 0;
  public static final int SIZE_FLJSN = 1048576; // A corriger (c'est uyne valeur par défaut du create table) 1048576
  public static final int SIZE_FLERR = 9;
  public static final int DECIMAL_FLERR = 0;
  public static final int SIZE_FLMSGERR = 44;
  
  // Variables fichiers
  private int FLID = 0; // L'ID record
  private String FLIDF = null; // L'ID du flux
  private String FLETB = null; // Etablissement concerné par le flux
  private String FLVER = null; // Version du flux
  private Timestamp FLCRE = null; // Date de création du flux
  private String FLCOD = null; // Code objet à traiter, l'indicatif venant du RPG (en gros la cle)
  private int FLSNS = 0; // Sens du flux (Envoi, Réception)
  private EnumStatutFlux FLSTT = null; // Status du flux
  private Timestamp FLTRT = null; // Date de traitement
  private int FLCPT = 0; // Nombre de tentatives
  private String FLJSN = null; // Message JSON
  private int FLERR = 0; // Code erreur
  protected String FLMSGERR = null; // Message d'erreur
  
  /**
   * Constructeur
   */
  public FFD_Psemhflxm(QueryManager pQueryManager) {
    super(pQueryManager);
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    FLID = 0;
    FLIDF = null;
    FLETB = null;
    FLCRE = null;
    FLCOD = null;
    FLSNS = 0;
    FLSTT = null;
    FLTRT = null;
    FLCPT = 0;
    FLJSN = null;
    FLERR = 0;
    FLMSGERR = null;
  }
  
  // -- Accesseurs
  
  /**
   * @return le fLID
   */
  public int getFLID() {
    return FLID;
  }
  
  /**
   * @param fLID le fLID à définir
   */
  public void setFLID(int fLID) {
    FLID = fLID;
  }
  
  /**
   * @return le fLIDF
   */
  public String getFLIDF() {
    return FLIDF;
  }
  
  /**
   * @param fLIDF le fLIDF à définir
   */
  public void setFLIDF(String fLIDF) {
    FLIDF = fLIDF;
  }
  
  /**
   * @return le fLETB
   */
  public String getFLETB() {
    return FLETB;
  }
  
  /**
   * @param fLETB le fLETB à définir
   */
  public void setFLETB(String fLETB) {
    FLETB = fLETB;
  }
  
  /**
   * @return le fLVER
   */
  public String getFLVER() {
    return FLVER;
  }
  
  /**
   * @param fLVER le fLVER à définir
   */
  public void setFLVER(String fLVER) {
    FLVER = fLVER;
  }
  
  /**
   * @return le fLCRE
   */
  public Timestamp getFLCRE() {
    return FLCRE;
  }
  
  /**
   * @param fLCRE le fLCRE à définir
   */
  public void setFLCRE(Timestamp fLCRE) {
    FLCRE = fLCRE;
  }
  
  /**
   * @return le fLCOD
   */
  public String getFLCOD() {
    return FLCOD;
  }
  
  /**
   * @param fLCOD le fLCOD à définir
   */
  public void setFLCOD(String fLCOD) {
    FLCOD = fLCOD;
  }
  
  /**
   * @return le fLSNS
   */
  public int getFLSNS() {
    return FLSNS;
  }
  
  /**
   * @param fLSNS le fLSNS à définir
   */
  public void setFLSNS(int fLSNS) {
    FLSNS = fLSNS;
  }
  
  /**
   * @return le fLSTT
   */
  public EnumStatutFlux getFLSTT() {
    return FLSTT;
  }
  
  /**
   * @param fLSTT le fLSTT à définir
   */
  public void setFLSTT(EnumStatutFlux fLSTT) {
    FLSTT = fLSTT;
  }
  
  /**
   * @return le fLTRT
   */
  public Timestamp getFLTRT() {
    return FLTRT;
  }
  
  /**
   * @param fLTRT le fLTRT à définir
   */
  public void setFLTRT(Timestamp fLTRT) {
    FLTRT = fLTRT;
  }
  
  /**
   * @return le fLCPT
   */
  public int getFLCPT() {
    return FLCPT;
  }
  
  /**
   * @param fLCPT le fLCPT à définir
   */
  public void setFLCPT(int fLCPT) {
    FLCPT = fLCPT;
  }
  
  /**
   * @return le fLJSN
   */
  public String getFLJSN() {
    return FLJSN;
  }
  
  /**
   * @param fLJSN le fLJSN à définir
   */
  public void setFLJSN(String fLJSN) {
    FLJSN = fLJSN;
  }
  
  /**
   * @return le fLERR
   */
  public int getFLERR() {
    return FLERR;
  }
  
  /**
   * @param fLERR le fLERR à définir
   */
  public void setFLERR(int fLERR) {
    FLERR = fLERR;
  }
  
  /**
   * @return le fLMSGERR
   */
  public String getFLMSGERR() {
    return FLMSGERR;
  }
  
  /**
   * @param fLMSGERR le fLMSGERR à définir
   */
  public void setFLMSGERR(String fLMSGERR) {
    FLMSGERR = fLMSGERR;
  }
  
}
