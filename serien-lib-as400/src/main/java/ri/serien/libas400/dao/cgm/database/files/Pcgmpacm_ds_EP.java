/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.cgm.database.files;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pcgmpacm_ds_EP pour les EP
 * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
 */

public class Pcgmpacm_ds_EP extends DataStructureRecord {

  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "EPNCGD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "EPNCGC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "EPLIBD"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "EPLIBC"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 2), "EPMTT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "EPJO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "EPSAN"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "EPACT"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "EPNDCD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "EPNDCC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "EPLDCD"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "EPLDCC"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "EPNTVA"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 2), "EPTTVA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EPNMT"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 2), "EPMTT2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "EPNMT2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "EPNAT"));

    length = 300;
  }
}
