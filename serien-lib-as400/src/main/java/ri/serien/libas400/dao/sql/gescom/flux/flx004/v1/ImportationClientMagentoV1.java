/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx004.v1;

import java.util.ArrayList;

import com.google.gson.JsonSyntaxException;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v1.JsonClientMagentoV1;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Version 1 des flux.
 * Cette classe permet d'importer un client Magento vers Série N.
 */
public class ImportationClientMagentoV1 extends BaseGroupDB {
  
  // Afficher les libellés articles sur les 2 premiers caractères
  private final String MODE_LIBS_ARTICLES = "**  ";
  private final String COMPTE_GENERAL_DEFAUT = "411000";
  protected ParametresFluxBibli parametresBibli = null;
  
  /**
   * Constructeur avec le queryManager en paramètre pour les requetes sur DB2
   */
  public ImportationClientMagentoV1(QueryManager aquerymg, ParametresFluxBibli pParam) {
    super(aquerymg);
    parametresBibli = pParam;
  }
  
  /**
   * Permet de vérifier le contenu du JSON pour le matcher avec un client Série N
   */
  public boolean recupererUnClientMagento(FluxMagento flux) {
    if (flux == null) {
      majError("[GM_Import_Commandes] recupererUnClientMagento() Flux NULL ");
      return false;
    }
    
    if (flux.getFLJSN() == null || flux.getFLJSN().trim().length() == 0) {
      majError("[GM_Import_Clients] recupererUnClientMagento() etb ou json corrompus ");
      return false;
    }
    
    boolean retour = false;
    
    // debut de la construction du JSON
    if (initJSON()) {
      // on récupère le client Magento
      JsonClientMagentoV1 client = null;
      try {
        client = gson.fromJson(flux.getFLJSN(), JsonClientMagentoV1.class);
      }
      catch (JsonSyntaxException e) {
        majError("[GM_Import_Clients] recupererUnClientMagento() Objet json corrompu: " + e.getMessage());
        return false;
      }
      
      if (client != null) {
        client.setEtb(flux.getFLETB());
        if (client.getIdClientMagento() != null) {
          flux.setFLCOD(client.getIdClientMagento());
        }
        // On met à jour le client dans la bdd Série N
        retour = mettreAJourUnClient(client);
        if (retour) {
          retour = mettreAJourUnClientCompta(client);
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Permet d'insérer ou de mettre à jour un client Série N avec les données importées.
   */
  private boolean mettreAJourUnClient(JsonClientMagentoV1 client) {
    if (client == null) {
      majError("[GM_Import_Clients] mettreAJourUnClient() client à NULL");
      return false;
    }
    
    boolean retour = false;
    String champs = "";
    String valeurs = "";
    String requete = "";
    String nomREPAC = "";
    
    // Suite à la COM-1332, tous les clients ont ce paramètre avec la valeur ' ' (pour le mode Calcul HT et édition HT) au lieu de '1'
    // En effet dans PGVMCLIM CLTTC
    // CLTTC = ' ' est un client HT éditions TTC
    // CLTTC = '1' est un client TTC éditions TTC
    // CLTTC = '2' est un client HT éditions TTC
    String clientTTC = " ";
    
    if (verifierLesDonneesRentrantes(client)) {
      int nbClientsDB2 = verifierSonExistenceDB2(client);
      // UPDATE EXISTANT
      if (nbClientsDB2 == 1) {
        // TODO contrôle supplémentaire sur le mail ou le SIRET
        // @formatter:off
        requete = "UPDATE " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " SET"
            + " CLAPEN = " + RequeteSql.formaterStringSQL(client.getCodeAPE())
            + ", CLCAT = "  + RequeteSql.formaterStringSQL(client.getCategorie())
            + ", CLRGL = " + RequeteSql.formaterStringSQL(client.getCodeReglement())
            + ", CLNOM = " + RequeteSql.formaterStringSQL(client.getCLNOM(), true)
            + ", CLCPL = " + RequeteSql.formaterStringSQL(client.getCLCPL(), true)
            + ", CLRUE = " + RequeteSql.formaterStringSQL(client.getCLRUE(), true)
            + ", CLLOC = " + RequeteSql.formaterStringSQL(client.getCLLOC(), true)
            + ", CLVIL = " + RequeteSql.formaterStringSQL(client.getCdpFact() + " " + Constantes.normerTexte(client.getVilleFact()))
            + ", CLCDP1 = " + RequeteSql.formaterStringSQL(client.getCdpFact())
            + ", CLCOP = " + RequeteSql.formaterStringSQL(client.getPaysFact())
            + ", CLPAY = " + RequeteSql.formaterStringSQL(client.getLibellePays())
            + ", CLTEL = " + RequeteSql.formaterStringSQL(client.getTelFact())
            + ", CLFAX = " + RequeteSql.formaterStringSQL(client.getFaxFact())
            + ", CLSRN = " + RequeteSql.formaterStringSQL(client.getCLSRN())
            + ", CLSRT = " + RequeteSql.formaterStringSQL(client.getCLSRT())
            + ", CLTFA = " + RequeteSql.formaterStringSQL(parametresBibli.getModeFacturationClient())
            + ", CLTOP1 = " + RequeteSql.formaterStringSQL(client.getMagClient())
            + ", CLTOP2 = " + RequeteSql.formaterStringSQL(client.getSiteClient())
            // Gestion du TTC pour les clients particuliers
            + ", CLTTC = " + RequeteSql.formaterStringSQL(clientTTC)
            + ", CLTAR = " + RequeteSql.formaterStringSQL(client.getCLTAR())
            + ", CLNLA = " + RequeteSql.formaterStringSQL(MODE_LIBS_ARTICLES)
            + ", CLIN9 = " + RequeteSql.formaterStringSQL(client.getCLIN9())
            + " WHERE CLETB = " + RequeteSql.formaterStringSQL(client.getEtb())
            + " AND CLADH = " + RequeteSql.formaterStringSQL(client.getIdClientMagento());
        // @formatter:on
        // UPDATE CLIENT
        if (queryManager.requete(requete) > 0) {
          String numeroContactP = null;
          GenericRecord record = recupererContactduClientMagento(client);
          // ON NE CREE PAS S'IL N'EXISTE PAS CAR UN CLIENT MAGENTO A AUTOMATIQUEMENT UN CONTACT ASSOCIE (mail)
          if (record != null) {
            if (record.isPresentField("RENUM")) {
              numeroContactP = record.getField("RENUM").toString().trim();
              nomREPAC = traiterREPAC(client.getNomFact(), client.getPrenomFact());
              
              // Mise à jour du contact
              // Mise à jour du téléphone et fax si client particulier uniquement (ici numéros de tél et fax différents de null)
              String numeroTelFaxContact = "";
              if (client.getNumeroTelephoneContact() != null) {
                numeroTelFaxContact += ", RETEL = " + RequeteSql.formaterStringSQL(client.getNumeroTelephoneContact());
              }
              if (client.getNumeroTelephoneContact() != null) {
                numeroTelFaxContact += ", REFAX = " + RequeteSql.formaterStringSQL(client.getNumeroFaxContact());
              }
              // @formatter:off
              requete = "UPDATE " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT + " SET"
                  + " RECIV = " + RequeteSql.formaterStringSQL(client.getCiviliteFact())
                  + ", REPAC = " + RequeteSql.formaterStringSQL(nomREPAC)
                  + ", RENOM = " + RequeteSql.formaterStringSQL(client.getNomFact(), true)
                  + ", REPRE = " + RequeteSql.formaterStringSQL(client.getPrenomFact(), true)
                  + ", RENET = " + RequeteSql.formaterStringSQL(client.getEmail())
                  + numeroTelFaxContact
                  + " WHERE RENUM = " + numeroContactP;
              // @formatter:on
              if (queryManager.requete(requete) > 0) {
                retour = gererActiviteWeb(client);
              }
              else {
                majError("[GM_Import_Clients] mettreAJourUnClient() UPDATE Contact en échec IDMagento : " + client.getIdClientMagento()
                    + " " + queryManager.getMsgError());
              }
            }
            else {
              majError("[GM_Import_Clients] mettreAJourUnClient() RECUP RENUM EN ECHEC client : " + client.getIdClientMagento());
            }
          }
          else {
            majError("[GM_Import_Clients] mettreAJourUnClient() SELECT contact EN ECHEC client : " + client.getIdClientMagento() + " "
                + queryManager.getMsgError());
          }
        }
        else {
          majError("[GM_Import_Clients] mettreAJourUnClient() UPDATE client " + client.getIdClientMagento() + " EN ERREUR "
              + queryManager.getMsgError());
        }
      }
      // CREATION
      else if (nbClientsDB2 == 0) {
        
        // TODO Paramètre numération automatique
        int numeroAutoClient = numerotationAutomatiqueClient(800000);
        
        if (numeroAutoClient > 0) {
          client.setCLLIV("000");
          champs = " CLCRE,CLETB,CLCLI,CLLIV,CLADH,CLAPEN,CLCAT,CLRGL,CLNOM,CLCPL,CLRUE,CLLOC,CLVIL,CLCDP1,CLCOP,CLPAY,CLTEL,CLFAX,"
              + "CLSRN,CLSRT,CLTFA,CLTTC,CLTAR,CLTOP1,CLTOP2,CLCLK,CLNLA,CLIN9 ";
          // @formatter:off
          valeurs = client.retournerDateDuJourSN()
            + ", " + RequeteSql.formaterStringSQL(client.getEtb())
            + ", " + numeroAutoClient
            + ", " + client.getCLLIV()
            + ", " + RequeteSql.formaterStringSQL(client.getIdClientMagento())
            + ", " + RequeteSql.formaterStringSQL(client.getCodeAPE())
            + ", " + RequeteSql.formaterStringSQL(client.getCategorie())
            + ", " + RequeteSql.formaterStringSQL(client.getCodeReglement())
            + ", " + RequeteSql.formaterStringSQL(client.getCLNOM(), true)
            + ", " + RequeteSql.formaterStringSQL(client.getCLCPL(), true)
            + ", " + RequeteSql.formaterStringSQL(client.getCLRUE(), true)
            + ", " + RequeteSql.formaterStringSQL(client.getCLLOC(), true)
            + ", " + RequeteSql.formaterStringSQL(client.getCdpFact() + " " + Constantes.normerTexte(client.getVilleFact()))
            + ", " + client.getCdpFact()
            + ", " + RequeteSql.formaterStringSQL(client.getPaysFact())
            + ", " + RequeteSql.formaterStringSQL(client.getLibellePays(), true)
            + ", " + RequeteSql.formaterStringSQL(client.getTelFact())
            + ", " + RequeteSql.formaterStringSQL(client.getFaxFact())
            + ", " + RequeteSql.formaterStringSQL(client.getCLSRN())
            + ", " + RequeteSql.formaterStringSQL(client.getCLSRT())
            + ", " + RequeteSql.formaterStringSQL(parametresBibli.getModeFacturationClient())
            + ", " + RequeteSql.formaterStringSQL(clientTTC)
            + ", " + client.getCLTAR()
            + ", " + RequeteSql.formaterStringSQL(client.getMagClient())
            + ", " + RequeteSql.formaterStringSQL(client.getSiteClient())
            + ", " + RequeteSql.formaterStringSQL(client.retournerUneCleDeClassement(client.getCLNOM(), 15), true)
            // CLNLA -> '****' le client se voit afficher les 4 libellés article dans la commande
            // On retire ce précédent paramètre pour mettre sur les 2 premiers libellés
            + ", " + RequeteSql.formaterStringSQL(MODE_LIBS_ARTICLES)
            + ", " + RequeteSql.formaterStringSQL(client.getCLIN9());
          // @formatter:on
          requete = "INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " (" + champs + ") VALUES ("
              + valeurs + ")";
          
          // ETAPE 1 INSERTION CLIENT
          if (queryManager.requete(requete) > 0) {
            client.setCLCLI("" + numeroAutoClient);
            // On retire le contrôle sur le client Particulier pour la création d'un contact
            int numeroAutoContact = numerotationAutomatiqueContact();
            // Si le numéro généré est cohérent
            if (numeroAutoContact > 0) {
              nomREPAC = traiterREPAC(client.getNomFact(), client.getPrenomFact());
              champs = " RENUM, RECIV, REPAC, RENOM, REPRE, RENET ";
              // Ajout du téléphone et fax si client particulier uniquement (ici numéros de tél et fax différents de null)
              String numeroTelFaxContact = "";
              if (client.getNumeroTelephoneContact() != null) {
                champs += ", RETEL ";
                numeroTelFaxContact += ", " + RequeteSql.formaterStringSQL(client.getNumeroTelephoneContact());
              }
              if (client.getNumeroTelephoneContact() != null) {
                champs += ", REFAX ";
                numeroTelFaxContact += ", " + RequeteSql.formaterStringSQL(client.getNumeroFaxContact());
              }
              
              // @formatter:off
              valeurs = numeroAutoContact
                  + ", " + RequeteSql.formaterStringSQL(client.getCiviliteFact(), true)
                  + ", " + RequeteSql.formaterStringSQL(nomREPAC)
                  + ", " + RequeteSql.formaterStringSQL(client.getNomFact(), true)
                  + ", " + RequeteSql.formaterStringSQL(client.getPrenomFact(), true)
                  + ", " + RequeteSql.formaterStringSQL(client.getEmail())
                  + numeroTelFaxContact;
              // @formatter:on
              requete = "INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT + " (" + champs + ") VALUES ("
                  + valeurs + ")";
              if (queryManager.requete(requete) > 0) {
                champs = " RLCOD,RLETB,RLIND,RLNUMT,RLIN1 ";
                // @formatter:off
                valeurs = "'C'"
                    + ", " + RequeteSql.formaterStringSQL(client.getEtb())
                    + ", " + RequeteSql.formaterStringSQL(formaterLeClientPourRLIND(numeroAutoClient) + "000")
                    + ", " + numeroAutoContact
                    + ", 'P'";
                // @formatter:on
                requete = "INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT_LIEN + " (" + champs
                    + ") VALUES (" + valeurs + ")";
                if (queryManager.requete(requete) > 0) {
                  retour = gererActiviteWeb(client);
                }
              }
              else {
                majError("[mettreAJourUnClient] Liaison Contact n'est pas insérée correctement");
              }
            }
          }
          else {
            majError("[GM_Import_Clients] INSERT Client EN ERREUR POUR :" + client.getIdClientMagento() + " /  "
                + queryManager.getMsgError() + " requete -> " + requete);
          }
        }
      }
      else if (nbClientsDB2 < 0) {
        majError("[GM_Import_Clients] PB DE RECUP DU CLIENT :" + client.getIdClientMagento() + " /  " + queryManager.getMsgError()
            + " requete -> " + requete);
      }
      else {
        majError("[GM_Import_Clients] PLUSIEURS CLIENTS POUR CE CODE :" + client.getIdClientMagento() + " /  "
            + queryManager.getMsgError() + " requete -> " + requete);
      }
    }
    
    return retour;
    
  }
  
  /**
   * Alors il faut formater l'affichage du fucking code client pour un insert dans le fucking RLIND de PSEMRTLM avant sa fucking
   * concaténation.....POUAAAAAH
   */
  private String formaterLeClientPourRLIND(int pNumeroClient) {
    String retour = "" + pNumeroClient;
    
    if (retour.length() > 0) {
      if (retour.length() == 1) {
        retour = "00000" + retour;
      }
      else if (retour.length() == 2) {
        retour = "0000" + retour;
      }
      else if (retour.length() == 3) {
        retour = "000" + retour;
      }
      else if (retour.length() == 4) {
        retour = "00" + retour;
      }
      else if (retour.length() == 5) {
        retour = "0" + retour;
      }
    }
    else {
      retour = "000000";
    }
    
    return retour;
  }
  
  /**
   * Alors il faut formater l'affichage du fucking compte général concaténé au code client pour un insert dans le fucking RLIND de
   * PSEMRTLM.....POUAAAAAH
   */
  private String formaterLeClientPourRLINDCompta(JsonClientMagentoV1 client) {
    if (client == null) {
      return null;
    }
    
    String retour = "";
    int compteNum = 0;
    try {
      compteNum = Integer.parseInt(client.getCLCLI());
    }
    catch (NumberFormatException e) {
      compteNum = 0;
    }
    String compte = formaterLeClientPourRLIND(compteNum);
    // On suppose que le compte général est toujours sur 6 Numériques
    retour = client.getCompteGeneral() + compte;
    
    return retour;
  }
  
  /**
   * Numérotation automatique de l'entité
   */
  private int numerotationAutomatiqueClient(int numeroMax) {
    int retour = -1;
    
    ArrayList<GenericRecord> liste = queryManager
        .select("SELECT (MAX(INT(CLCLI))+1) AS AUTO FROM " + queryManager.getLibrary() + ".PGVMCLIM WHERE CLCLI < " + numeroMax);
    if (liste != null && liste.size() > 0) {
      try {
        if (liste.get(0).isPresentField("AUTO")) {
          retour = Integer.parseInt(liste.get(0).getField("AUTO").toString().trim());
        }
        else {
          majError("[GM_Import_Clients] numerotationAutomatiqueClient() getField(AUTO) existe pas : " + numeroMax);
        }
      }
      catch (NumberFormatException e) {
        majError("[GM_Import_Clients] numerotationAutomatiqueClient() Probleme de ParseInt : "
            + liste.get(0).getField("AUTO").toString().trim());
        retour = 0;
      }
    }
    else {
      majError("[GM_Import_Clients] numerotationAutomatiqueClient() liste à NULL ou 0 : " + numeroMax);
    }
    
    return retour;
  }
  
  /**
   * Permet de numéroter automatiquement les contacts
   */
  private int numerotationAutomatiqueContact() {
    int retour = -1;
    
    ArrayList<GenericRecord> liste =
        queryManager.select("SELECT (MAX(INT(RENUM))+1) AS AUTO FROM " + queryManager.getLibrary() + ".PSEMRTEM ");
    if (liste != null && liste.size() > 0) {
      try {
        if (liste.get(0).isPresentField("AUTO")) {
          retour = Integer.parseInt(liste.get(0).getField("AUTO").toString().trim());
        }
        else {
          majError("[GM_Import_Clients] getField(AUTO) n'est pas présent: ");
        }
      }
      catch (NumberFormatException e) {
        majError("[GM_Import_Clients] Probleme de PARSEINT : " + liste.get(0).getField("AUTO").toString().trim());
        retour = 0;
      }
    }
    else {
      majError("[GM_Import_Clients] numerotationAutomatiqueContact() liste à NULL ou 0 : ");
    }
    
    return retour;
  }
  
  /**
   * Permet de vérifier si les données transmises à Série N sont bonnes
   */
  private boolean verifierLesDonneesRentrantes(JsonClientMagentoV1 client) {
    // TODO faudra rendre cette méthode dynamique sans avoir à recoder le nom des zones !!
    boolean retour = false;
    int nbErreurs = 0;
    String messageErreur = "";
    if (client == null) {
      return retour;
    }
    
    // TODO Sur toutes les zones il faudra tester (et alerter) puis trimer() sur la longueur des chaines de caractères si nécessaire
    
    // TODO RAJOUTER EXPRESSION REGULIERE NUMERIQUE AU TEST + CLADH MAX 15 CARACTERES
    if (client.getIdClientMagento() == null || client.getIdClientMagento().toString().trim().equals("")) {
      nbErreurs++;
      messageErreur += "IdClientMagento/";
    }
    
    // TODO RAJOUTER EXPRESSION REGULIERE NUMERIQUE AU TEST
    if (client.getEmail() == null || client.getEmail().toString().trim().equals("")) {
      nbErreurs++;
      messageErreur += "Email/";
    }
    
    // TODO RAJOUTER UN TEST DE TYPE CATEGORIE LOGIQUE
    if (client.getCategorie() == null || client.getCategorie().toString().trim().equals("")) {
      nbErreurs++;
      messageErreur += "getCategorie/";
    }
    
    if (!JsonEntiteMagento.controlerUnCodePostal(true, client.getCdpFact())) {
      nbErreurs++;
      messageErreur += "codePostal facturé/";
    }
    
    if (nbErreurs > 0) {
      majError("[GM_import_clients] verifierLesDonneesRentrantes() pb de données: " + nbErreurs + " erreurs: " + messageErreur);
      return false;
    }
    
    if (client.getNom() == null) {
      client.setNom("");
    }
    
    // TODO RAJOUTER EXPRESSION REGULIERE NUMERIQUE ET LONGUEUR SIREN
    if (client.getSiren() != null && client.getSiren().toString().trim().length() == 14) {
      // FO TESTER TOUT LE BOUZIN plus PROPREMENT
      client.setCLSRN(client.getSiren().substring(0, 9));
      client.setCLSRT(client.getSiren().substring(9).trim());
    }
    else {
      client.setSiren("");
      client.setCLSRN("");
      client.setCLSRT("");
    }
    
    // TODO Controler sur une réelle TVA
    if (client.getTvaClient() == null) {
      client.setTvaClient(parametresBibli.getModeFacturationClient());
    }
    
    if (client.getCodeAPE() == null) {
      client.setCodeAPE("");
    }
    
    // bloc adresse facturation
    if (client.getCiviliteFact() == null) {
      client.setCiviliteFact("");
    }
    if (client.getNomFact() == null) {
      client.setNomFact("");
    }
    if (client.getPrenomFact() == null) {
      client.setPrenomFact("");
    }
    if (client.getRueFact() == null) {
      client.setRueFact("");
    }
    if (client.getRue2Fact() == null) {
      client.setRue2Fact("");
    }
    // Le code postal ne doit pas contenir des espaces sinon la requête ne sera pas formatée correctement
    String codepostal = Constantes.normerTexte(client.getCdpFact());
    if (codepostal.isEmpty()) {
      client.setCdpFact("00000");
    }
    else {
      if (client.getCdpFact().length() > Adresse.LONGUEUR_CODEPOSTAL) {
        codepostal = codepostal.substring(0, Adresse.LONGUEUR_CODEPOSTAL);
      }
      int codepostalnum = Constantes.convertirTexteEnInteger(codepostal);
      client.setCdpFact(String.format("%0" + Adresse.LONGUEUR_CODEPOSTAL + "d", codepostalnum));
    }
    
    if (client.getVilleFact() == null) {
      client.setVilleFact("");
    }
    else {
      client.setVilleFact(client.formaterSaisieMax(client.getVilleFact(), 24));
    }
    
    if (client.getPaysFact() == null || client.getPaysFact().trim().equals("")) {
      client.setPaysFact(JsonClientMagentoV1.codePaysDefaut);
      client.setLibellePays(JsonClientMagentoV1.libPaysDefaut);
    }
    else {
      client.setLibellePays(recupererLibellePaysSerieN(client.getPaysFact()));
    }
    
    if (client.getTelFact() == null) {
      client.setTelFact("");
    }
    if (client.getFaxFact() == null) {
      client.setFaxFact("");
    }
    
    if (client.getMagClient() == null) {
      client.setMagClient("");
    }
    if (client.getSiteClient() == null) {
      client.setSiteClient("");
    }
    
    client.setCodeReglement(parametresBibli.getReglementParticulierMg());
    client.traitementsDivers(parametresBibli);
    client.miseEnFormeBlocAdresseImport(parametresBibli);
    
    if (nbErreurs == 0) {
      retour = true;
    }
    else {
      majError("[GM_import_clients] verifierLesDonneesRentrantes() pb de données: " + nbErreurs + " erreurs: " + messageErreur);
    }
    
    return retour;
  }
  
  /**
   * On récupère le libellé d'un pays sur la base de son code Série N
   */
  private String recupererLibellePaysSerieN(String pCodePays) {
    String retour = "";
    
    if (pCodePays == null) {
      return retour;
    }
    
    ArrayList<GenericRecord> liste = queryManager.select(" SELECT PARFIL FROM " + queryManager.getLibrary() + ".PSEMPARM "
        + " WHERE PARCLE LIKE '   CP%' AND SUBSTR(PARCLE, 6, 3) = '" + pCodePays + "' ");
    
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("PARFIL")) {
        return liste.get(0).getField("PARFIL").toString().trim();
      }
    }
    
    return retour;
  }
  
  /**
   * Permet de traiter la zone REPAC correctement.
   */
  private String traiterREPAC(String pNom, String pPrenom) {
    String repac = Constantes.normerTexte(pNom) + " " + Constantes.normerTexte(pPrenom);
    repac = repac.trim();
    
    if (repac.length() > JsonEntiteMagento.NB_MAX_CARACTERES_BLOC_ADRESSE) {
      repac = repac.substring(0, JsonEntiteMagento.NB_MAX_CARACTERES_BLOC_ADRESSE);
    }
    
    return repac;
  }
  
  /**
   * Permet de vérifier si le client importé existe déjà dans Série N ou non.
   * Cette méthode retourne le nombre d'enregistrements trouvés.
   */
  private int verifierSonExistenceDB2(JsonClientMagentoV1 client) {
    RequeteSql requete = new RequeteSql();
    // @formatter:off
    requete.ajouter("SELECT CLCLI, CLLIV, CLNCG FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT + " WHERE");
    // @formatter:on
    requete.ajouterConditionAnd("CLETB", "=", client.getEtb());
    requete.ajouterConditionAnd("CLADH", "=", client.getIdClientMagento());
    
    ArrayList<GenericRecord> listeClient = queryManager.select(requete.getRequete());
    if (listeClient != null) {
      // Le client existe en un exemplaire
      if (listeClient.size() == 1) {
        // TODO On contrôle sur l'adresse mail (pourquoi ?)
        if (client.getEmail() != null && client.getEmail().trim().length() > 4) {
          if (listeClient.get(0).isPresentField("CLCLI")) {
            client.setCLCLI(listeClient.get(0).getField("CLCLI").toString().trim());
          }
          if (listeClient.get(0).isPresentField("CLLIV")) {
            client.setCLLIV(listeClient.get(0).getField("CLLIV").toString().trim());
          }
          if (listeClient.get(0).isPresentField("CLNCG")) {
            client.setCLNCG(listeClient.get(0).getField("CLNCG").toString().trim());
          }
          
          return 1;
        }
        else {
          majError("[GM_import_clients] verifierSonExistenceDB2() email client NULL ou corrompu ");
          return -1;
        }
      }
      // Tiens tiens y en a plusieurs c'est rigolo
      else if (listeClient.size() > 1) {
        majError("[GM_import_clients] verifierSonExistenceDB2() On a " + listeClient.size()
            + " clients sous le même ID Magento pour le client " + client.getIdClientMagento());
        return listeClient.size();
      }
      else {
        return 0;
      }
      
    }
    else {
      majError("[GM_import_clients] verifierSonExistenceDB2() listeClient EXISTE PAS: " + queryManager.getMsgError());
      return -1;
    }
  }
  
  /**
   * Permet de vérifier si le client importé existe déjà dans la comptabilité de Série N ou non
   */
  private int verifierSonExistenceComptaDB2(JsonClientMagentoV1 client) {
    gererETBComptable(client);
    gererCompteGeneral(client);
    
    ArrayList<GenericRecord> listeClient =
        queryManager.select("" + " SELECT A1NCA FROM " + queryManager.getLibrary() + ".PCGMPCAM " + " LEFT JOIN "
            + queryManager.getLibrary() + ".PGVMCLIM ON CLETB = A1SOC AND CLCLI = A1NCA " + " WHERE A1SOC = '" + client.getEtbComptable()
            + "' AND CLADH = '" + client.getIdClientMagento() + "' AND A1NCG = '" + client.getCompteGeneral() + "' ");
    
    if (listeClient != null) {
      if (listeClient.size() > 1) {
        majError("[GM_import_clients] verifierSonExistenceComptaDB2() On a " + listeClient.size()
            + " clients sous le même ID Magento pour le client " + client.getIdClientMagento());
      }
      
      return listeClient.size();
    }
    else {
      majError("[GM_import_clients] verifierSonExistenceDB2() listeClient EXISTE PAS: " + queryManager.getMsgError());
      return -1;
    }
  }
  
  /**
   * Permet d'insérer ou de mettre à jour un client de Compta Série N avec les données importées
   */
  private boolean mettreAJourUnClientCompta(JsonClientMagentoV1 client) {
    int nbCLientsExistants = verifierSonExistenceComptaDB2(client);
    // On update le client compta
    if (nbCLientsExistants == 1) {
      return modifierUnClientCompta(client);
    }
    else if (nbCLientsExistants == 0) {
      return creerUnClientCompta(client);
      // Si y a un fucking souci
    }
    else {
      return false;
    }
  }
  
  /**
   * On modifie le client comptable sur la base du client Magento
   */
  private boolean modifierUnClientCompta(JsonClientMagentoV1 client) {
    if (client.getCLCLI() == null || client.getCLCLI().trim().equals("")) {
      return false;
    }
    
    String requete = " UPDATE " + queryManager.getLibrary() + ".PCGMPCAM SET " + " A1RGL = '" + client.getCodeReglement() + "',"
        + " A1MOD = '" + client.retournerDateDuJourSN() + "'," + " A1LIB = '" + client.nettoyerSaisies(client.getCLNOM()) + "',"
        + " A1CPL = '" + client.nettoyerSaisies(client.getCLCPL()) + "'," + " A1RUE = '" + client.nettoyerSaisies(client.getCLRUE())
        + "'," + " A1LOC = '" + client.nettoyerSaisies(client.getCLLOC()) + "'," + " A1VIL = '"
        + client.nettoyerSaisies(client.getVilleFact()) + "'," + " A1CDP = '" + client.getCdpFact() + "'," + " A1COP = '"
        + client.getPaysFact() + "'," + " A1PAY = '" + client.nettoyerSaisies(client.getLibellePays()) + "'," + " A1TEL = '"
        + client.getTelFact() + "'," + " A1FAX = '" + client.getFaxFact() + "'," + " A1SRN = '" + client.getCLSRN() + "'," + " A1SRT = '"
        + client.getCLSRT() + "'," + " A1NCG = '" + client.getCompteGeneral() + "' " + " WHERE A1SOC = '" + client.getEtbComptable()
        + "' AND A1NCA = '" + client.getCLCLI() + "' AND A1NCG = '" + client.getCompteGeneral() + "' ";
    
    // UPDATE CLIENT
    if (queryManager.requete(requete) > 0) {
      // On touche pas au contact on considère qu'il est bon pour le moment.
      return true;
    }
    else {
      majError("[GM_import_clients] modifierUnClientCompta() UPDATE client comptable en erreur : " + queryManager.getMsgError());
    }
    
    return false;
  }
  
  /**
   * Créer un client en comptabilité sur la base du client gestion commerciale
   */
  private boolean creerUnClientCompta(JsonClientMagentoV1 client) {
    if (client.getCLCLI() == null || client.getCLCLI().trim().equals("")) {
      return false;
    }
    
    String champs =
        "A1CRE,A1MOD,A1SOC,A1NCG,A1NCA,A1LIB,A1PTG,A1RPT,A1RGL,A1CPL,A1RUE,A1LOC,A1CDP,A1VIL,A1COP,A1PAY,A1TEL,A1FAX,A1SRN,A1SRT";
    String valeurs = "'" + client.retournerDateDuJourSN() + "','" + client.retournerDateDuJourSN() + "','" + client.getEtbComptable()
        + "','" + client.getCompteGeneral() + "','" + client.getCLCLI() + "','" + client.nettoyerSaisies(client.getCLNOM()) + "','D"
        + "','1" + "','" + client.getCodeReglement() + "','" + client.nettoyerSaisies(client.getCLCPL()) + "','"
        + client.nettoyerSaisies(client.getCLRUE()) + "','" + client.nettoyerSaisies(client.getCLLOC()) + "','" + client.getCdpFact()
        + "','" + client.nettoyerSaisies(client.getVilleFact()) + "','" + client.getPaysFact() + "','"
        + client.nettoyerSaisies(client.getLibellePays()) + "','" + client.getTelFact() + "','" + client.getFaxFact() + "','"
        + client.getCLSRN() + "','" + client.getCLSRT() + "'";
    
    String requete = "INSERT INTO " + queryManager.getLibrary() + ".PCGMPCAM (" + champs + ") VALUES (" + valeurs + ") ";
    
    if (queryManager.requete(requete) > 0) {
      // On va créer le lien compta avec le contact Série N gescom. Normalement un client magento possède automatiquement un contact lié.
      GenericRecord record = recupererContactduClientMagento(client);
      if (record != null) {
        if (record.isPresentField("RLNUMT")) {
          champs = " RLCOD,RLETB,RLIND,RLNUMT,RLIN1 ";
          valeurs = " 'A','" + client.getEtbComptable() + "','" + formaterLeClientPourRLINDCompta(client) + "','"
              + record.getField("RLNUMT").toString().trim() + "','' ";
          if (queryManager
              .requete("INSERT INTO " + queryManager.getLibrary() + ".PSEMRTLM (" + champs + ") VALUES (" + valeurs + ") ") > 0) {
            return true;
          }
          else {
            majError(
                "[GM_import_clients] creerUnClientCompta() INSERT nouvelle liaison Contact en erreur : " + queryManager.getMsgError());
          }
        }
        else {
          majError("[GM_import_clients] creerUnClientCompta() Zones RLETB,RLIND,RLNUMT absentes ");
        }
      }
      else {
        majError("[GM_import_clients] creerUnClientCompta() RECUP Contact en erreur : " + queryManager.getMsgError());
      }
    }
    else {
      majError("[GM_import_clients] creerUnClientCompta() echec creation client compta : " + queryManager.getMsgError());
    }
    
    return false;
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
  
  /**
   * Permet de retourner l'établissement comptable du client
   * En effet, celui n'est pas forcément l'établissement de gestion commerciale
   */
  private void gererETBComptable(JsonClientMagentoV1 client) {
    // On contrôle le paramètre CV s'il existe et pas blanc (CVETC de 121 à 123)
    String etb = null;
    
    // Execution de la requête se l'établissement comptable est renseigné
    if (client.getEtbComptable() != null) {
      // On prend le paramètre CV (CVCOL en position 79 à 84)
      // @formatter:off
      ArrayList<GenericRecord> listeParam =
          queryManager.select("SELECT SUBSTR(PARZ2, 98, 3) AS ETB FROM " + queryManager.getLibrary() + ".PGVMPARM"
              + " WHERE PARETB = " + RequeteSql.formaterStringSQL(client.getEtbComptable())
              + " AND PARTYP = 'CV'"
              + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
      // @formatter:on
      if (listeParam != null && listeParam.size() == 1) {
        if (listeParam.get(0).isPresentField("ETB") && !listeParam.get(0).getField("ETB").toString().trim().equals("")) {
          etb = listeParam.get(0).getField("ETB").toString().trim();
        }
      }
    }
    // Si le paramètre est existant on attribue sa valeur à l'établissement comptable
    if (etb != null) {
      client.setEtbComptable(etb);
    }
    else {
      // S'il n'existe pas on prend la valeur de l'établissement de l'utilisateur
      client.setEtbComptable(client.getEtb());
    }
  }
  
  /**
   * On retourne le compte général à associer automatiquement à client s'il n'en possède pas
   * On lit le paramètre CV dans la table de paramètre PGVMPARM
   */
  private void gererCompteGeneral(JsonClientMagentoV1 client) {
    // Si CLNCG est implémenté on prend cette valeur
    if (client.getCLNCG() != null && !client.getCLNCG().trim().equals("0")) {
      client.setCompteGeneral(client.getCLNCG());
    }
    else {
      String compte = null;
      // Sinon on prend le paramètre CV (CVCOL en position 56 sur 6)
      // @formatter:off
      ArrayList<GenericRecord> listeParam =
          queryManager.select("SELECT SUBSTR(PARZ2, 56, 6) AS CPT FROM " + queryManager.getLibrary() + ".PGVMPARM "
              + " WHERE PARETB = " + RequeteSql.formaterStringSQL(client.getEtbComptable())
              + " AND PARTYP = 'CV'"
              + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
      // @formatter:on
      if (listeParam != null && listeParam.size() == 1) {
        if (listeParam.get(0).isPresentField("CPT")) {
          compte = listeParam.get(0).getField("CPT").toString().trim();
        }
        if (compte == null || compte.trim().length() == 0) {
          compte = COMPTE_GENERAL_DEFAUT;
        }
      }
      else {
        majError("[GM_import_clients] retournerCompteGeneral() listeParam EXISTE PAS ou ZERO : " + queryManager.getMsgError());
        compte = COMPTE_GENERAL_DEFAUT;
      }
      
      client.setCompteGeneral(compte);
    }
  }
  
  /**
   * On récupère le contact principal du client Magento
   */
  private GenericRecord recupererContactduClientMagento(JsonClientMagentoV1 client) {
    if (client == null) {
      return null;
    }
    
    String requete = " SELECT RLETB,RENUM,RLCOD,RLIND,RLNUMT FROM " + queryManager.getLibrary() + ".PSEMRTEM " + " LEFT JOIN "
        + queryManager.getLibrary() + ".PSEMRTLM ON RENUM=RLNUMT AND RLCOD='C' " + " LEFT JOIN " + queryManager.getLibrary()
        + ".PGVMCLIM ON CLCLI=SUBSTR(RLIND, 1, 6) and CLLIV=SUBSTR(RLIND, 7, 3) " + " WHERE CLETB = '" + client.getEtb()
        + "' AND CLADH = '" + client.getIdClientMagento() + "' AND RLIN1 = 'P' ";
    
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    
    if (liste != null && liste.size() > 0) {
      return liste.get(0);
    }
    else {
      return null;
    }
  }
  
  /**
   * On met à jour l'extension du client si elle existe, sinon on la créé.
   */
  private boolean gererActiviteWeb(JsonClientMagentoV1 pClient) {
    if (pClient == null) {
      majError("[ImportClientMagento] majExtensionClient() client à NULL ");
      return false;
    }
    
    boolean retour = false;
    // @formatter:off
    ArrayList<GenericRecord> liste = queryManager.select("SELECT EBCLI FROM " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT_EXTENSION
        + " WHERE EBETB = " + RequeteSql.formaterStringSQL(pClient.getEtb())
        + " AND EBCLI = " + pClient.getCLCLI()
        + " AND EBLIV = " + pClient.getCLLIV());
    // @formatter:on
    if (liste == null) {
      majError("[gererActiviteWeb] majExtensionClient() liste à NULL pour client " + pClient.getEtb() + "/" + pClient.getCLCLI() + "/"
          + pClient.getCLLIV());
      return false;
    }
    
    // Si le client est trouvé l'extension n'est pas modifiée
    if (liste.size() == 1) {
      // retour = mettreAJourExtensionClient(pClient);
      return true;
    }
    // Si le client est trouvé l'extention est créé
    else if (liste.size() == 0) {
      // Il faut créer l'extension de ce client
      retour = insererUneNouvelleExtensionClient(pClient);
    }
    // Sinon erreur
    else {
      majError("[gererActiviteWeb] majExtensionClient() taille de liste: " + liste.size() + "  pour client " + pClient.getEtb() + "/"
          + pClient.getCLCLI() + "/" + pClient.getCLLIV());
    }
    
    return retour;
  }
  
  /**
   * Insertion d'une nouvelle extension client avec la gestion Web intégré.
   */
  private boolean insererUneNouvelleExtensionClient(JsonClientMagentoV1 pClient) {
    if (pClient == null) {
      majError("[insererUneNouvelleExtensionClient] insererUneNouvelleExtensionClient() pClient à NULL ");
      return false;
    }
    // @formatter:off
    int nb = queryManager.requete("INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT_EXTENSION
        + " (EBETB, EBCLI, EBLIV, EBIN09) VALUES ("
        + RequeteSql.formaterStringSQL(pClient.getEtb())
        + ", " + pClient.getCLCLI()
        + ", " + pClient.getCLLIV()
        + ", '1'"
        + ")");
    // @formatter:on
    if (nb > 0) {
      return true;
    }
    else {
      majError("[insererUneNouvelleExtensionClient] majExtensionClient() erreur intégration extension  pour client " + pClient.getEtb()
          + "/" + pClient.getCLCLI() + "/" + pClient.getCLLIV() + " - " + queryManager.getMsgError());
      return false;
    }
  }
  
  /**
   * On met à jour l'extension client avec la gestion Web intégré.
   */
  /*
  private boolean mettreAJourExtensionClient(JsonClientMagentoV1 pClient) {
    if (pClient == null) {
      majError("[mettreAJourExtensionClient] insererUneNouvelleExtensionClient() pClient à NULL ");
      return false;
    }
    // @formatter:off
    int nb = querymg.requete("UPDATE " + querymg.getLibrary() + "." + ListeTablesDb2.CLIENT_EXTENSION
        + " SET EBIN09 = " + RequeteSql.formaterStringSQL(pClient.getEBIN09())
        + " WHERE EBETB = " + RequeteSql.formaterStringSQL(pClient.getEtb())
        + " AND EBCLI = " + pClient.getCLCLI()
        + " AND EBLIV = " + pClient.getCLLIV());
    // @formatter:on
    if (nb > 0) {
      return true;
    }
    else {
      majError("[mettreAJourExtensionClient] majExtensionClient() erreur intégration extension  pour client " + pClient.getEtb() + "/"
          + pClient.getCLCLI() + "/" + pClient.getCLLIV() + " - " + querymg.getMsgError());
      return false;
    }
  }*/
}
