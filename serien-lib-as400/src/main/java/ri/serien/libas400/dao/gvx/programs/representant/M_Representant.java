/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs.representant;

import ri.serien.libas400.dao.gvx.database.files.FFD_Pgvmrepm;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.representant.CriteresRechercheRepresentant;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.commun.representant.Representant;

public class M_Representant extends FFD_Pgvmrepm {
  // Constantes
  private static final String DB2FILE_NAME = "PGVMREPM";
  
  /**
   * Constructeur.
   */
  public M_Representant(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Insère l'enregistrement dans le table.
   *
   */
  @Override
  public boolean insertInDatabase() {
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert(DB2FILE_NAME, querymg.getLibrary());
    return request(requete);
  }
  
  /**
   * Modifie l'enregistrement dans le table.
   *
   */
  @Override
  public boolean updateInDatabase() {
    initGenericRecord(genericrecord, false);
    String requete = genericrecord.createSQLRequestUpdate(DB2FILE_NAME, querymg.getLibrary(),
        "RPETB='" + getRPETB() + "' and RPREP='" + getRPREP() + "'");
    return request(requete);
  }
  
  /**
   * Suppression de l'enregistrement courant.
   *
   */
  @Override
  public boolean deleteInDatabase() {
    return deleteInDatabase(false);
  }
  
  /**
   * Suppression de l'enregistrement courant ou de tous.
   *
   */
  public boolean deleteInDatabase(boolean deleteAll) {
    String requete = "delete from " + querymg.getLibrary() + "." + DB2FILE_NAME + " where RPETB='" + getRPETB() + "'";
    if (!deleteAll) {
      requete += " and RPREP='" + getRPREP() + "'";
    }
    return request(requete);
  }
  
  /**
   * Initialise les champs de la classe Representant.
   * @return
   */
  public Representant initRepresentant(int typeRecherche) {
    switch (typeRecherche) {
      case CriteresRechercheRepresentant.RECHERCHE_COMPTOIR:
        return initRepresentantPourRechercheComptoir();
      default:
        return initRepresentantGlobal();
    }
  }
  
  /**
   * Initialise les champs nécessaire pour la recherche comptoir
   * (lié à RequetesRechercheRepresentants.getRequeteRechercheRepresentantComptoir).
   */
  private Representant initRepresentantPourRechercheComptoir() {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(getRPETB());
    Representant aRepresentant = new Representant(IdRepresentant.getInstance(idEtablissement, getRPREP()));
    
    aRepresentant.setNom(getRPNOM());
    
    Adresse adresse = new Adresse();
    adresse.setRue(getRPRUE());
    adresse.setLocalisation(getRPLOC());
    adresse.setCodePostal(getRPCDP());
    adresse.setVilleAvecCodePostal(getRPVIL());
    adresse.setPays(getRPPAY());
    aRepresentant.setAdresse(adresse);
    
    return aRepresentant;
  }
  
  /**
   * Initialise tous les champs de la classe Representant.
   */
  private Representant initRepresentantGlobal() {
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance(getRPETB());
    Representant aRepresentant = new Representant(IdRepresentant.getInstance(idEtablissement, getRPREP()));
    
    aRepresentant.setTopSysteme(getRPTOP());
    // aRepresentant.setDateCreation(getRPCRE());
    // aRepresentant.setDateModification(getRPMOD());
    // aRepresentant.setDateTraitement(getRPTRT());
    
    aRepresentant.setNom(getRPNOM());
    
    Adresse adresse = new Adresse();
    adresse.setRue(getRPRUE());
    adresse.setLocalisation(getRPLOC());
    adresse.setCodePostal(getRPCDP());
    adresse.setVilleAvecCodePostal(getRPVIL());
    adresse.setPays(getRPPAY());
    aRepresentant.setAdresse(adresse);
    
    // TODO à compléter
    
    return aRepresentant;
  }
  
  /**
   * Libère les ressources.
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
  }
  
}
