/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

public abstract class FFD_Pgvmeaam extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_A1TOP = 1;
  public static final int DECIMAL_A1TOP = 0;
  public static final int SIZE_A1ETB = 3;
  public static final int SIZE_A1ART = 20;
  public static final int SIZE_A1LB1 = 30;
  public static final int SIZE_A1LB2 = 30;
  public static final int SIZE_A1LB3 = 30;
  
  // Variables fichiers
  protected int A1TOP = 0; // Top système
  protected String A1ETB = null; // Code etablissement
  protected String A1ART = null; // Code article
  protected String A1LB1 = null; // Libelle n° 2
  protected String A1LB2 = null; // Libellé n° 3
  protected String A1LB3 = null; // Libellé n° 4
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public FFD_Pgvmeaam(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    A1TOP = 0;
    A1ETB = null;
    A1ART = null;
    A1LB1 = null;
    A1LB2 = null;
    A1LB3 = null;
  }
  
  /**
   * @return le a1TOP
   */
  public int getA1TOP() {
    return A1TOP;
  }
  
  /**
   * @param a1top le a1TOP à définir
   */
  public void setA1TOP(int a1top) {
    A1TOP = a1top;
  }
  
  /**
   * @return le a1ETB
   */
  public String getA1ETB() {
    return A1ETB;
  }
  
  /**
   * @param a1etb le a1ETB à définir
   */
  public void setA1ETB(String a1etb) {
    A1ETB = a1etb;
  }
  
  /**
   * @return le a1ART
   */
  public String getA1ART() {
    return A1ART;
  }
  
  /**
   * @param a1art le a1ART à définir
   */
  public void setA1ART(String a1art) {
    A1ART = a1art;
  }
  
  /**
   * @return le a1LB1
   */
  public String getA1LB1() {
    return A1LB1;
  }
  
  /**
   * @param a1lb1 le a1LB1 à définir
   */
  public void setA1LB1(String a1lb1) {
    A1LB1 = a1lb1;
  }
  
  /**
   * @return le a1LB2
   */
  public String getA1LB2() {
    return A1LB2;
  }
  
  /**
   * @param a1lb2 le a1LB2 à définir
   */
  public void setA1LB2(String a1lb2) {
    A1LB2 = a1lb2;
  }
  
  /**
   * @return le a1LB3
   */
  public String getA1LB3() {
    return A1LB3;
  }
  
  /**
   * @param a1lb3 le a1LB3 à définir
   */
  public void setA1LB3(String a1lb3) {
    A1LB3 = a1lb3;
  }
}
