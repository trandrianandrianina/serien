/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlespecial;

import java.math.BigDecimal;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgModifierArticleSpecial extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0044";
  private static final int NEGOCATION_ACHAT = 1;
  
  /**
   * Appel du programme RPG qui va écrire une négociation.
   */
  public void modifierArticleSpecial(SystemeManager pSysteme, Article pArticle, Client pClient, BigDecimal pPrixNetHT,
      ConditionAchat pCNA) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pArticle == null) {
      throw new MessageErreurException("Le paramètre pArticle du service est à null.");
    }
    if (pClient == null) {
      throw new MessageErreurException("Le paramètre pClient du service est à null.");
    }
    if (pPrixNetHT == null) {
      throw new MessageErreurException("Le prix net HT est invalide.");
    }
    if (pCNA == null) {
      throw new MessageErreurException("Le paramètre pCNA du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0044i entree = new Svgvm0044i();
    Svgvm0044o sortie = new Svgvm0044o();
    // Paramètres d'entrée, de sortie et d'erreur
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialisation des paramètres d'entrée
    // Code établissement
    entree.setPietb(pArticle.getId().getCodeEtablissement());
    // Code article
    entree.setPiart(pArticle.getId().getCodeArticle());
    // Numéro client
    entree.setPicli(pClient.getId().getNumero());
    // Suffixe client
    entree.setPiliv(pClient.getId().getSuffixe());
    // Libellé 1
    entree.setPili1(pArticle.getLibelle1() + pArticle.getLibelle2());
    // Libellé 2
    entree.setPili2(pArticle.getLibelle3() + pArticle.getLibelle4());
    // Unité de vente
    if (pArticle.getIdUV() != null) {
      entree.setPiunv(pArticle.getIdUV().getCode());
    }
    // Nombre d'UV par UCV
    entree.setPicnd(pArticle.getNombreUVParUCV());
    // Unité de conditionnement
    if (pArticle.getIdUCV() != null) {
      entree.setPiunl(pArticle.getIdUCV().getCode());
    }
    // Groupe/famille
    if (pArticle.getIdFamille() != null) {
      entree.setPifam(pArticle.getIdFamille().getCode());
    }
    // sous-famille
    if (pArticle.getIdSousFamille() != null) {
      entree.setPisfa(pArticle.getIdSousFamille().getCode());
    }
    // On fournit toujours les prix en HT
    entree.setPipxv(pPrixNetHT);
    // Prix de revient standard
    entree.setPiprs(pArticle.getPrixDeRevientStandardHT());
    // Indicateur Négociation achat
    entree.setPinga(NEGOCATION_ACHAT);
    entree.setPipra(pCNA.getPrixAchatBrutHT());
    // Remise 1
    entree.setPirem1(pCNA.getPourcentageRemise1());
    // Remise 2
    entree.setPirem2(pCNA.getPourcentageRemise2());
    // Remise 3
    entree.setPirem3(pCNA.getPourcentageRemise3());
    // Remise 4
    entree.setPirem4(pCNA.getPourcentageRemise4());
    // Coefficient ou le poids pour le port
    entree.setPifk1(pCNA.getPoidsPort());
    // Pourcentage de frais de port
    entree.setPifp1(pCNA.getPourcentagePort());
    // Montant Port
    entree.setPifv1(pCNA.getMontantAuPoidsPortHT());
    // Taxe ou majoration en %
    entree.setPifk3(pCNA.getPourcentageTaxeOuMajoration());
    // Montant Conditionnement
    entree.setPifv3(pCNA.getMontantConditionnement());
    // Frais d'exploitation
    entree.setPifp2(pCNA.getFraisExploitation());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
}
