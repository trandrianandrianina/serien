/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.negociation;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvx0010i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PITOP = 1;
  public static final int DECIMAL_PITOP = 0;
  public static final int SIZE_PICRE = 7;
  public static final int DECIMAL_PICRE = 0;
  public static final int SIZE_PIMOD = 7;
  public static final int DECIMAL_PIMOD = 0;
  public static final int SIZE_PITRT = 7;
  public static final int DECIMAL_PITRT = 0;
  public static final int SIZE_PITYP = 1;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PICOL = 1;
  public static final int DECIMAL_PICOL = 0;
  public static final int SIZE_PIFRS = 6;
  public static final int DECIMAL_PIFRS = 0;
  public static final int SIZE_PIDAP = 7;
  public static final int DECIMAL_PIDAP = 0;
  public static final int SIZE_PICPV = 1;
  public static final int SIZE_PIREM1 = 4;
  public static final int DECIMAL_PIREM1 = 2;
  public static final int SIZE_PIREM2 = 4;
  public static final int DECIMAL_PIREM2 = 2;
  public static final int SIZE_PIREM3 = 4;
  public static final int DECIMAL_PIREM3 = 2;
  public static final int SIZE_PIREM4 = 4;
  public static final int DECIMAL_PIREM4 = 2;
  public static final int SIZE_PIREM5 = 4;
  public static final int DECIMAL_PIREM5 = 2;
  public static final int SIZE_PIREM6 = 4;
  public static final int DECIMAL_PIREM6 = 2;
  public static final int SIZE_PIPRA = 11;
  public static final int DECIMAL_PIPRA = 2;
  public static final int SIZE_PIUNA = 2;
  public static final int SIZE_PINUA = 8;
  public static final int DECIMAL_PINUA = 3;
  public static final int SIZE_PIKAC = 8;
  public static final int DECIMAL_PIKAC = 3;
  public static final int SIZE_PIDEL = 2;
  public static final int DECIMAL_PIDEL = 0;
  public static final int SIZE_PIREF = 20;
  public static final int SIZE_PIUNC = 2;
  public static final int SIZE_PIKSC = 8;
  public static final int DECIMAL_PIKSC = 3;
  public static final int SIZE_PIDCC = 1;
  public static final int DECIMAL_PIDCC = 0;
  public static final int SIZE_PIDCA = 1;
  public static final int DECIMAL_PIDCA = 0;
  public static final int SIZE_PIDEV = 3;
  public static final int SIZE_PIQMI = 10;
  public static final int DECIMAL_PIQMI = 3;
  public static final int SIZE_PIQEC = 10;
  public static final int DECIMAL_PIQEC = 3;
  public static final int SIZE_PIKPV = 4;
  public static final int DECIMAL_PIKPV = 3;
  public static final int SIZE_PIDDP = 7;
  public static final int DECIMAL_PIDDP = 0;
  public static final int SIZE_PIRGA = 10;
  public static final int SIZE_PIDELS = 2;
  public static final int DECIMAL_PIDELS = 0;
  public static final int SIZE_PIKPR = 5;
  public static final int DECIMAL_PIKPR = 4;
  public static final int SIZE_PIIN1 = 1;
  public static final int SIZE_PIIN2 = 1;
  public static final int SIZE_PIIN3 = 1;
  public static final int SIZE_PIIN4 = 1;
  public static final int SIZE_PIIN5 = 1;
  public static final int SIZE_PITRL = 1;
  public static final int SIZE_PIRFC = 20;
  public static final int SIZE_PIGCD = 13;
  public static final int DECIMAL_PIGCD = 0;
  public static final int SIZE_PIDAF = 7;
  public static final int DECIMAL_PIDAF = 0;
  public static final int SIZE_PILIB = 30;
  public static final int SIZE_PIOPA = 3;
  public static final int SIZE_PIIN6 = 1;
  public static final int SIZE_PIIN7 = 1;
  public static final int SIZE_PIIN8 = 1;
  public static final int SIZE_PIIN9 = 1;
  public static final int SIZE_PIPDI = 11;
  public static final int DECIMAL_PIPDI = 2;
  public static final int SIZE_PIKAP = 5;
  public static final int DECIMAL_PIKAP = 4;
  public static final int SIZE_PIPRS = 9;
  public static final int DECIMAL_PIPRS = 2;
  public static final int SIZE_PIPGN = 1;
  public static final int SIZE_PIFV1 = 9;
  public static final int DECIMAL_PIFV1 = 2;
  public static final int SIZE_PIFK1 = 7;
  public static final int DECIMAL_PIFK1 = 4;
  public static final int SIZE_PIFP1 = 4;
  public static final int DECIMAL_PIFP1 = 2;
  public static final int SIZE_PIFU1 = 1;
  public static final int SIZE_PIFV2 = 9;
  public static final int DECIMAL_PIFV2 = 2;
  public static final int SIZE_PIFK2 = 7;
  public static final int DECIMAL_PIFK2 = 4;
  public static final int SIZE_PIFP2 = 4;
  public static final int DECIMAL_PIFP2 = 2;
  public static final int SIZE_PIFU2 = 1;
  public static final int SIZE_PIFV3 = 9;
  public static final int DECIMAL_PIFV3 = 2;
  public static final int SIZE_PIFK3 = 7;
  public static final int DECIMAL_PIFK3 = 4;
  public static final int SIZE_PIFU3 = 1;
  public static final int SIZE_PIPR1 = 11;
  public static final int DECIMAL_PIPR1 = 2;
  public static final int SIZE_PIPR2 = 11;
  public static final int DECIMAL_PIPR2 = 2;
  public static final int SIZE_PIDAT1 = 7;
  public static final int DECIMAL_PIDAT1 = 0;
  public static final int SIZE_PIDAT2 = 7;
  public static final int DECIMAL_PIDAT2 = 0;
  public static final int SIZE_TOTALE_DS = 425;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PITOP = 1;
  public static final int VAR_PICRE = 2;
  public static final int VAR_PIMOD = 3;
  public static final int VAR_PITRT = 4;
  public static final int VAR_PITYP = 5;
  public static final int VAR_PICOD = 6;
  public static final int VAR_PIETB = 7;
  public static final int VAR_PINUM = 8;
  public static final int VAR_PISUF = 9;
  public static final int VAR_PINLI = 10;
  public static final int VAR_PIART = 11;
  public static final int VAR_PICOL = 12;
  public static final int VAR_PIFRS = 13;
  public static final int VAR_PIDAP = 14;
  public static final int VAR_PICPV = 15;
  public static final int VAR_PIREM1 = 16;
  public static final int VAR_PIREM2 = 17;
  public static final int VAR_PIREM3 = 18;
  public static final int VAR_PIREM4 = 19;
  public static final int VAR_PIREM5 = 20;
  public static final int VAR_PIREM6 = 21;
  public static final int VAR_PIPRA = 22;
  public static final int VAR_PIUNA = 23;
  public static final int VAR_PINUA = 24;
  public static final int VAR_PIKAC = 25;
  public static final int VAR_PIDEL = 26;
  public static final int VAR_PIREF = 27;
  public static final int VAR_PIUNC = 28;
  public static final int VAR_PIKSC = 29;
  public static final int VAR_PIDCC = 30;
  public static final int VAR_PIDCA = 31;
  public static final int VAR_PIDEV = 32;
  public static final int VAR_PIQMI = 33;
  public static final int VAR_PIQEC = 34;
  public static final int VAR_PIKPV = 35;
  public static final int VAR_PIDDP = 36;
  public static final int VAR_PIRGA = 37;
  public static final int VAR_PIDELS = 38;
  public static final int VAR_PIKPR = 39;
  public static final int VAR_PIIN1 = 40;
  public static final int VAR_PIIN2 = 41;
  public static final int VAR_PIIN3 = 42;
  public static final int VAR_PIIN4 = 43;
  public static final int VAR_PIIN5 = 44;
  public static final int VAR_PITRL = 45;
  public static final int VAR_PIRFC = 46;
  public static final int VAR_PIGCD = 47;
  public static final int VAR_PIDAF = 48;
  public static final int VAR_PILIB = 49;
  public static final int VAR_PIOPA = 50;
  public static final int VAR_PIIN6 = 51;
  public static final int VAR_PIIN7 = 52;
  public static final int VAR_PIIN8 = 53;
  public static final int VAR_PIIN9 = 54;
  public static final int VAR_PIPDI = 55;
  public static final int VAR_PIKAP = 56;
  public static final int VAR_PIPRS = 57;
  public static final int VAR_PIPGN = 58;
  public static final int VAR_PIFV1 = 59;
  public static final int VAR_PIFK1 = 60;
  public static final int VAR_PIFP1 = 61;
  public static final int VAR_PIFU1 = 62;
  public static final int VAR_PIFV2 = 63;
  public static final int VAR_PIFK2 = 64;
  public static final int VAR_PIFP2 = 65;
  public static final int VAR_PIFU2 = 66;
  public static final int VAR_PIFV3 = 67;
  public static final int VAR_PIFK3 = 68;
  public static final int VAR_PIFU3 = 69;
  public static final int VAR_PIPR1 = 70;
  public static final int VAR_PIPR2 = 71;
  public static final int VAR_PIDAT1 = 72;
  public static final int VAR_PIDAT2 = 73;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private BigDecimal pitop = BigDecimal.ZERO; // Top système
  private BigDecimal picre = BigDecimal.ZERO; // Date de création
  private BigDecimal pimod = BigDecimal.ZERO; // Date de modification
  private BigDecimal pitrt = BigDecimal.ZERO; // Date de traitement
  private String pityp = ""; // "A" pour achat, "V" vente
  private String picod = ""; // Code ERL "E"
  private String pietb = ""; // Code Etablissement
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de Ligne
  private String piart = ""; // Code article
  private BigDecimal picol = BigDecimal.ZERO; // Collectif Fournisseur
  private BigDecimal pifrs = BigDecimal.ZERO; // Numéro Fournisseur
  private BigDecimal pidap = BigDecimal.ZERO; // Date application
  private String picpv = ""; // Top calcul prix de vente
  private BigDecimal pirem1 = BigDecimal.ZERO; // Remise 1
  private BigDecimal pirem2 = BigDecimal.ZERO; // Remise 2
  private BigDecimal pirem3 = BigDecimal.ZERO; // Remise 3
  private BigDecimal pirem4 = BigDecimal.ZERO; // Remise 4
  private BigDecimal pirem5 = BigDecimal.ZERO; // Remise 5
  private BigDecimal pirem6 = BigDecimal.ZERO; // Remise 6
  private BigDecimal pipra = BigDecimal.ZERO; // Prix catalogue
  private String piuna = ""; // Unité achat
  private BigDecimal pinua = BigDecimal.ZERO; // Conditionnement
  private BigDecimal pikac = BigDecimal.ZERO; // Nombre unité achat/unité Cde
  private BigDecimal pidel = BigDecimal.ZERO; // Délai de livraison
  private String piref = ""; // Référence fournisseur
  private String piunc = ""; // Unité de commande
  private BigDecimal piksc = BigDecimal.ZERO; // Nombre unité Stk/unité Cde
  private BigDecimal pidcc = BigDecimal.ZERO; // Zone système
  private BigDecimal pidca = BigDecimal.ZERO; // Zone système
  private String pidev = ""; // Devise
  private BigDecimal piqmi = BigDecimal.ZERO; // Quantité minimum de commande
  private BigDecimal piqec = BigDecimal.ZERO; // Quantité économique
  private BigDecimal pikpv = BigDecimal.ZERO; // Coeff. calcul prix de vente
  private BigDecimal piddp = BigDecimal.ZERO; // Date de dernier prix
  private String pirga = ""; // Regroupement Achat
  private BigDecimal pidels = BigDecimal.ZERO; // Délai supplémentaire
  private BigDecimal pikpr = BigDecimal.ZERO; // Coeff. calcul prix
  private String piin1 = ""; // P=Promo, C=Colonnes
  private String piin2 = ""; // Note qualité de 0 à 9
  private String piin3 = ""; // G = Fournisseur pour G.B.A
  private String piin4 = ""; // 3 remises pour calcul PV min
  private String piin5 = ""; // Ind. non utilisé
  private String pitrl = ""; // Type remise ligne, A=Ajout
  private String pirfc = ""; // Référence constructeur
  private BigDecimal pigcd = BigDecimal.ZERO; // Code gencod frs
  private BigDecimal pidaf = BigDecimal.ZERO; // Date limite validité
  private String pilib = ""; // Libellé
  private String piopa = ""; // origine
  private String piin6 = ""; // Ind. non utilisé
  private String piin7 = ""; // Delai en Jours et Non Semain
  private String piin8 = ""; // Ind. non utilisé
  private String piin9 = ""; // Ind. non utilisé
  private BigDecimal pipdi = BigDecimal.ZERO; // Prix distributeur
  private BigDecimal pikap = BigDecimal.ZERO; // Coeff. approche fixe
  private BigDecimal piprs = BigDecimal.ZERO; // Prix de revient standard
  private String pipgn = ""; // Top prix négocié
  private BigDecimal pifv1 = BigDecimal.ZERO; // Frais 1 valeur
  private BigDecimal pifk1 = BigDecimal.ZERO; // Frais 1 coefficient
  private BigDecimal pifp1 = BigDecimal.ZERO; // Frais 1 % de port
  private String pifu1 = ""; // Unité frais 1
  private BigDecimal pifv2 = BigDecimal.ZERO; // Frais 2 valeur
  private BigDecimal pifk2 = BigDecimal.ZERO; // Frais 2 coefficient
  private BigDecimal pifp2 = BigDecimal.ZERO; // Frais 2 % MAJORATION
  private String pifu2 = ""; // Unité frais 2
  private BigDecimal pifv3 = BigDecimal.ZERO; // Frais 3 valeur
  private BigDecimal pifk3 = BigDecimal.ZERO; // Frais 3 coefficient
  private String pifu3 = ""; // Unité frais 3
  private BigDecimal pipr1 = BigDecimal.ZERO; // Zone non utilisée
  private BigDecimal pipr2 = BigDecimal.ZERO; // Zone non utilisée
  private BigDecimal pidat1 = BigDecimal.ZERO; // Zone date non utilisée
  private BigDecimal pidat2 = BigDecimal.ZERO; // Zone date non utilisée
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_PITOP, DECIMAL_PITOP), // Top système
      new AS400PackedDecimal(SIZE_PICRE, DECIMAL_PICRE), // Date de création
      new AS400PackedDecimal(SIZE_PIMOD, DECIMAL_PIMOD), // Date de modification
      new AS400PackedDecimal(SIZE_PITRT, DECIMAL_PITRT), // Date de traitement
      new AS400Text(SIZE_PITYP), // "A" pour achat, "V" vente
      new AS400Text(SIZE_PICOD), // Code ERL "E"
      new AS400Text(SIZE_PIETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de Ligne
      new AS400Text(SIZE_PIART), // Code article
      new AS400ZonedDecimal(SIZE_PICOL, DECIMAL_PICOL), // Collectif Fournisseur
      new AS400ZonedDecimal(SIZE_PIFRS, DECIMAL_PIFRS), // Numéro Fournisseur
      new AS400ZonedDecimal(SIZE_PIDAP, DECIMAL_PIDAP), // Date application
      new AS400Text(SIZE_PICPV), // Top calcul prix de vente
      new AS400ZonedDecimal(SIZE_PIREM1, DECIMAL_PIREM1), // Remise 1
      new AS400ZonedDecimal(SIZE_PIREM2, DECIMAL_PIREM2), // Remise 2
      new AS400ZonedDecimal(SIZE_PIREM3, DECIMAL_PIREM3), // Remise 3
      new AS400ZonedDecimal(SIZE_PIREM4, DECIMAL_PIREM4), // Remise 4
      new AS400ZonedDecimal(SIZE_PIREM5, DECIMAL_PIREM5), // Remise 5
      new AS400ZonedDecimal(SIZE_PIREM6, DECIMAL_PIREM6), // Remise 6
      new AS400PackedDecimal(SIZE_PIPRA, DECIMAL_PIPRA), // Prix catalogue
      new AS400Text(SIZE_PIUNA), // Unité achat
      new AS400PackedDecimal(SIZE_PINUA, DECIMAL_PINUA), // Conditionnement
      new AS400PackedDecimal(SIZE_PIKAC, DECIMAL_PIKAC), // Nombre unité achat/unité Cde
      new AS400ZonedDecimal(SIZE_PIDEL, DECIMAL_PIDEL), // Délai de livraison
      new AS400Text(SIZE_PIREF), // Référence fournisseur
      new AS400Text(SIZE_PIUNC), // Unité de commande
      new AS400PackedDecimal(SIZE_PIKSC, DECIMAL_PIKSC), // Nombre unité Stk/unité Cde
      new AS400ZonedDecimal(SIZE_PIDCC, DECIMAL_PIDCC), // Zone système
      new AS400ZonedDecimal(SIZE_PIDCA, DECIMAL_PIDCA), // Zone système
      new AS400Text(SIZE_PIDEV), // Devise
      new AS400PackedDecimal(SIZE_PIQMI, DECIMAL_PIQMI), // Quantité minimum de commande
      new AS400PackedDecimal(SIZE_PIQEC, DECIMAL_PIQEC), // Quantité économique
      new AS400ZonedDecimal(SIZE_PIKPV, DECIMAL_PIKPV), // Coeff. calcul prix de vente
      new AS400PackedDecimal(SIZE_PIDDP, DECIMAL_PIDDP), // Date de dernier prix
      new AS400Text(SIZE_PIRGA), // Regroupement Achat
      new AS400ZonedDecimal(SIZE_PIDELS, DECIMAL_PIDELS), // Délai supplémentaire
      new AS400PackedDecimal(SIZE_PIKPR, DECIMAL_PIKPR), // Coeff. calcul prix
      new AS400Text(SIZE_PIIN1), // P=Promo, C=Colonnes
      new AS400Text(SIZE_PIIN2), // Note qualité de 0 à 9
      new AS400Text(SIZE_PIIN3), // G = Fournisseur pour G.B.A
      new AS400Text(SIZE_PIIN4), // 3 remises pour calcul PV min
      new AS400Text(SIZE_PIIN5), // Ind. non utilisé
      new AS400Text(SIZE_PITRL), // Type remise ligne, A=Ajout
      new AS400Text(SIZE_PIRFC), // Référence constructeur
      new AS400ZonedDecimal(SIZE_PIGCD, DECIMAL_PIGCD), // Code gencod frs
      new AS400ZonedDecimal(SIZE_PIDAF, DECIMAL_PIDAF), // Date limite validité
      new AS400Text(SIZE_PILIB), // Libellé
      new AS400Text(SIZE_PIOPA), // origine
      new AS400Text(SIZE_PIIN6), // Ind. non utilisé
      new AS400Text(SIZE_PIIN7), // Delai en Jours et Non Semain
      new AS400Text(SIZE_PIIN8), // Ind. non utilisé
      new AS400Text(SIZE_PIIN9), // Ind. non utilisé
      new AS400PackedDecimal(SIZE_PIPDI, DECIMAL_PIPDI), // Prix distributeur
      new AS400ZonedDecimal(SIZE_PIKAP, DECIMAL_PIKAP), // Coeff. approche fixe
      new AS400PackedDecimal(SIZE_PIPRS, DECIMAL_PIPRS), // Prix de revient standard
      new AS400Text(SIZE_PIPGN), // Top prix négocié
      new AS400PackedDecimal(SIZE_PIFV1, DECIMAL_PIFV1), // Frais 1 valeur
      new AS400PackedDecimal(SIZE_PIFK1, DECIMAL_PIFK1), // Frais 1 coefficient
      new AS400ZonedDecimal(SIZE_PIFP1, DECIMAL_PIFP1), // Frais 1 % de port
      new AS400Text(SIZE_PIFU1), // Unité frais 1
      new AS400PackedDecimal(SIZE_PIFV2, DECIMAL_PIFV2), // Frais 2 valeur
      new AS400PackedDecimal(SIZE_PIFK2, DECIMAL_PIFK2), // Frais 2 coefficient
      new AS400PackedDecimal(SIZE_PIFP2, DECIMAL_PIFP2), // Frais 2 % MAJORATION
      new AS400Text(SIZE_PIFU2), // Unité frais 2
      new AS400PackedDecimal(SIZE_PIFV3, DECIMAL_PIFV3), // Frais 3 valeur
      new AS400PackedDecimal(SIZE_PIFK3, DECIMAL_PIFK3), // Frais 3 coefficient
      new AS400Text(SIZE_PIFU3), // Unité frais 3
      new AS400PackedDecimal(SIZE_PIPR1, DECIMAL_PIPR1), // Zone non utilisée
      new AS400PackedDecimal(SIZE_PIPR2, DECIMAL_PIPR2), // Zone non utilisée
      new AS400PackedDecimal(SIZE_PIDAT1, DECIMAL_PIDAT1), // Zone date non utilisée
      new AS400PackedDecimal(SIZE_PIDAT2, DECIMAL_PIDAT2), // Zone date non utilisée
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pitop, picre, pimod, pitrt, pityp, picod, pietb, pinum, pisuf, pinli, piart, picol, pifrs, pidap, picpv,
          pirem1, pirem2, pirem3, pirem4, pirem5, pirem6, pipra, piuna, pinua, pikac, pidel, piref, piunc, piksc, pidcc, pidca, pidev,
          piqmi, piqec, pikpv, piddp, pirga, pidels, pikpr, piin1, piin2, piin3, piin4, piin5, pitrl, pirfc, pigcd, pidaf, pilib, piopa,
          piin6, piin7, piin8, piin9, pipdi, pikap, piprs, pipgn, pifv1, pifk1, pifp1, pifu1, pifv2, pifk2, pifp2, pifu2, pifv3, pifk3,
          pifu3, pipr1, pipr2, pidat1, pidat2, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pitop = (BigDecimal) output[1];
    picre = (BigDecimal) output[2];
    pimod = (BigDecimal) output[3];
    pitrt = (BigDecimal) output[4];
    pityp = (String) output[5];
    picod = (String) output[6];
    pietb = (String) output[7];
    pinum = (BigDecimal) output[8];
    pisuf = (BigDecimal) output[9];
    pinli = (BigDecimal) output[10];
    piart = (String) output[11];
    picol = (BigDecimal) output[12];
    pifrs = (BigDecimal) output[13];
    pidap = (BigDecimal) output[14];
    picpv = (String) output[15];
    pirem1 = (BigDecimal) output[16];
    pirem2 = (BigDecimal) output[17];
    pirem3 = (BigDecimal) output[18];
    pirem4 = (BigDecimal) output[19];
    pirem5 = (BigDecimal) output[20];
    pirem6 = (BigDecimal) output[21];
    pipra = (BigDecimal) output[22];
    piuna = (String) output[23];
    pinua = (BigDecimal) output[24];
    pikac = (BigDecimal) output[25];
    pidel = (BigDecimal) output[26];
    piref = (String) output[27];
    piunc = (String) output[28];
    piksc = (BigDecimal) output[29];
    pidcc = (BigDecimal) output[30];
    pidca = (BigDecimal) output[31];
    pidev = (String) output[32];
    piqmi = (BigDecimal) output[33];
    piqec = (BigDecimal) output[34];
    pikpv = (BigDecimal) output[35];
    piddp = (BigDecimal) output[36];
    pirga = (String) output[37];
    pidels = (BigDecimal) output[38];
    pikpr = (BigDecimal) output[39];
    piin1 = (String) output[40];
    piin2 = (String) output[41];
    piin3 = (String) output[42];
    piin4 = (String) output[43];
    piin5 = (String) output[44];
    pitrl = (String) output[45];
    pirfc = (String) output[46];
    pigcd = (BigDecimal) output[47];
    pidaf = (BigDecimal) output[48];
    pilib = (String) output[49];
    piopa = (String) output[50];
    piin6 = (String) output[51];
    piin7 = (String) output[52];
    piin8 = (String) output[53];
    piin9 = (String) output[54];
    pipdi = (BigDecimal) output[55];
    pikap = (BigDecimal) output[56];
    piprs = (BigDecimal) output[57];
    pipgn = (String) output[58];
    pifv1 = (BigDecimal) output[59];
    pifk1 = (BigDecimal) output[60];
    pifp1 = (BigDecimal) output[61];
    pifu1 = (String) output[62];
    pifv2 = (BigDecimal) output[63];
    pifk2 = (BigDecimal) output[64];
    pifp2 = (BigDecimal) output[65];
    pifu2 = (String) output[66];
    pifv3 = (BigDecimal) output[67];
    pifk3 = (BigDecimal) output[68];
    pifu3 = (String) output[69];
    pipr1 = (BigDecimal) output[70];
    pipr2 = (BigDecimal) output[71];
    pidat1 = (BigDecimal) output[72];
    pidat2 = (BigDecimal) output[73];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPitop(BigDecimal pPitop) {
    if (pPitop == null) {
      return;
    }
    pitop = pPitop.setScale(DECIMAL_PITOP, RoundingMode.HALF_UP);
  }
  
  public void setPitop(Integer pPitop) {
    if (pPitop == null) {
      return;
    }
    pitop = BigDecimal.valueOf(pPitop);
  }
  
  public Integer getPitop() {
    return pitop.intValue();
  }
  
  public void setPicre(BigDecimal pPicre) {
    if (pPicre == null) {
      return;
    }
    picre = pPicre.setScale(DECIMAL_PICRE, RoundingMode.HALF_UP);
  }
  
  public void setPicre(Integer pPicre) {
    if (pPicre == null) {
      return;
    }
    picre = BigDecimal.valueOf(pPicre);
  }
  
  public void setPicre(Date pPicre) {
    if (pPicre == null) {
      return;
    }
    picre = BigDecimal.valueOf(ConvertDate.dateToDb2(pPicre));
  }
  
  public Integer getPicre() {
    return picre.intValue();
  }
  
  public Date getPicreConvertiEnDate() {
    return ConvertDate.db2ToDate(picre.intValue(), null);
  }
  
  public void setPimod(BigDecimal pPimod) {
    if (pPimod == null) {
      return;
    }
    pimod = pPimod.setScale(DECIMAL_PIMOD, RoundingMode.HALF_UP);
  }
  
  public void setPimod(Integer pPimod) {
    if (pPimod == null) {
      return;
    }
    pimod = BigDecimal.valueOf(pPimod);
  }
  
  public void setPimod(Date pPimod) {
    if (pPimod == null) {
      return;
    }
    pimod = BigDecimal.valueOf(ConvertDate.dateToDb2(pPimod));
  }
  
  public Integer getPimod() {
    return pimod.intValue();
  }
  
  public Date getPimodConvertiEnDate() {
    return ConvertDate.db2ToDate(pimod.intValue(), null);
  }
  
  public void setPitrt(BigDecimal pPitrt) {
    if (pPitrt == null) {
      return;
    }
    pitrt = pPitrt.setScale(DECIMAL_PITRT, RoundingMode.HALF_UP);
  }
  
  public void setPitrt(Integer pPitrt) {
    if (pPitrt == null) {
      return;
    }
    pitrt = BigDecimal.valueOf(pPitrt);
  }
  
  public void setPitrt(Date pPitrt) {
    if (pPitrt == null) {
      return;
    }
    pitrt = BigDecimal.valueOf(ConvertDate.dateToDb2(pPitrt));
  }
  
  public Integer getPitrt() {
    return pitrt.intValue();
  }
  
  public Date getPitrtConvertiEnDate() {
    return ConvertDate.db2ToDate(pitrt.intValue(), null);
  }
  
  public void setPityp(Character pPityp) {
    if (pPityp == null) {
      return;
    }
    pityp = String.valueOf(pPityp);
  }
  
  public Character getPityp() {
    return pityp.charAt(0);
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPiart(String pPiart) {
    if (pPiart == null) {
      return;
    }
    piart = pPiart;
  }
  
  public String getPiart() {
    return piart;
  }
  
  public void setPicol(BigDecimal pPicol) {
    if (pPicol == null) {
      return;
    }
    picol = pPicol.setScale(DECIMAL_PICOL, RoundingMode.HALF_UP);
  }
  
  public void setPicol(Integer pPicol) {
    if (pPicol == null) {
      return;
    }
    picol = BigDecimal.valueOf(pPicol);
  }
  
  public Integer getPicol() {
    return picol.intValue();
  }
  
  public void setPifrs(BigDecimal pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = pPifrs.setScale(DECIMAL_PIFRS, RoundingMode.HALF_UP);
  }
  
  public void setPifrs(Integer pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = BigDecimal.valueOf(pPifrs);
  }
  
  public Integer getPifrs() {
    return pifrs.intValue();
  }
  
  public void setPidap(BigDecimal pPidap) {
    if (pPidap == null) {
      return;
    }
    pidap = pPidap.setScale(DECIMAL_PIDAP, RoundingMode.HALF_UP);
  }
  
  public void setPidap(Integer pPidap) {
    if (pPidap == null) {
      return;
    }
    pidap = BigDecimal.valueOf(pPidap);
  }
  
  public void setPidap(Date pPidap) {
    if (pPidap == null) {
      return;
    }
    pidap = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidap));
  }
  
  public Integer getPidap() {
    return pidap.intValue();
  }
  
  public Date getPidapConvertiEnDate() {
    return ConvertDate.db2ToDate(pidap.intValue(), null);
  }
  
  public void setPicpv(Character pPicpv) {
    if (pPicpv == null) {
      return;
    }
    picpv = String.valueOf(pPicpv);
  }
  
  public Character getPicpv() {
    return picpv.charAt(0);
  }
  
  public void setPirem1(BigDecimal pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = pPirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem1(Double pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = BigDecimal.valueOf(pPirem1).setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem1() {
    return pirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(BigDecimal pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = pPirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(Double pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = BigDecimal.valueOf(pPirem2).setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem2() {
    return pirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(BigDecimal pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = pPirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(Double pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = BigDecimal.valueOf(pPirem3).setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem3() {
    return pirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirem4(BigDecimal pPirem4) {
    if (pPirem4 == null) {
      return;
    }
    pirem4 = pPirem4.setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public void setPirem4(Double pPirem4) {
    if (pPirem4 == null) {
      return;
    }
    pirem4 = BigDecimal.valueOf(pPirem4).setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem4() {
    return pirem4.setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public void setPirem5(BigDecimal pPirem5) {
    if (pPirem5 == null) {
      return;
    }
    pirem5 = pPirem5.setScale(DECIMAL_PIREM5, RoundingMode.HALF_UP);
  }
  
  public void setPirem5(Double pPirem5) {
    if (pPirem5 == null) {
      return;
    }
    pirem5 = BigDecimal.valueOf(pPirem5).setScale(DECIMAL_PIREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem5() {
    return pirem5.setScale(DECIMAL_PIREM5, RoundingMode.HALF_UP);
  }
  
  public void setPirem6(BigDecimal pPirem6) {
    if (pPirem6 == null) {
      return;
    }
    pirem6 = pPirem6.setScale(DECIMAL_PIREM6, RoundingMode.HALF_UP);
  }
  
  public void setPirem6(Double pPirem6) {
    if (pPirem6 == null) {
      return;
    }
    pirem6 = BigDecimal.valueOf(pPirem6).setScale(DECIMAL_PIREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem6() {
    return pirem6.setScale(DECIMAL_PIREM6, RoundingMode.HALF_UP);
  }
  
  public void setPipra(BigDecimal pPipra) {
    if (pPipra == null) {
      return;
    }
    pipra = pPipra.setScale(DECIMAL_PIPRA, RoundingMode.HALF_UP);
  }
  
  public void setPipra(Double pPipra) {
    if (pPipra == null) {
      return;
    }
    pipra = BigDecimal.valueOf(pPipra).setScale(DECIMAL_PIPRA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipra() {
    return pipra.setScale(DECIMAL_PIPRA, RoundingMode.HALF_UP);
  }
  
  public void setPiuna(String pPiuna) {
    if (pPiuna == null) {
      return;
    }
    piuna = pPiuna;
  }
  
  public String getPiuna() {
    return piuna;
  }
  
  public void setPinua(BigDecimal pPinua) {
    if (pPinua == null) {
      return;
    }
    pinua = pPinua.setScale(DECIMAL_PINUA, RoundingMode.HALF_UP);
  }
  
  public void setPinua(Double pPinua) {
    if (pPinua == null) {
      return;
    }
    pinua = BigDecimal.valueOf(pPinua).setScale(DECIMAL_PINUA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPinua() {
    return pinua.setScale(DECIMAL_PINUA, RoundingMode.HALF_UP);
  }
  
  public void setPikac(BigDecimal pPikac) {
    if (pPikac == null) {
      return;
    }
    pikac = pPikac.setScale(DECIMAL_PIKAC, RoundingMode.HALF_UP);
  }
  
  public void setPikac(Double pPikac) {
    if (pPikac == null) {
      return;
    }
    pikac = BigDecimal.valueOf(pPikac).setScale(DECIMAL_PIKAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPikac() {
    return pikac.setScale(DECIMAL_PIKAC, RoundingMode.HALF_UP);
  }
  
  public void setPidel(BigDecimal pPidel) {
    if (pPidel == null) {
      return;
    }
    pidel = pPidel.setScale(DECIMAL_PIDEL, RoundingMode.HALF_UP);
  }
  
  public void setPidel(Integer pPidel) {
    if (pPidel == null) {
      return;
    }
    pidel = BigDecimal.valueOf(pPidel);
  }
  
  public Integer getPidel() {
    return pidel.intValue();
  }
  
  public void setPiref(String pPiref) {
    if (pPiref == null) {
      return;
    }
    piref = pPiref;
  }
  
  public String getPiref() {
    return piref;
  }
  
  public void setPiunc(String pPiunc) {
    if (pPiunc == null) {
      return;
    }
    piunc = pPiunc;
  }
  
  public String getPiunc() {
    return piunc;
  }
  
  public void setPiksc(BigDecimal pPiksc) {
    if (pPiksc == null) {
      return;
    }
    piksc = pPiksc.setScale(DECIMAL_PIKSC, RoundingMode.HALF_UP);
  }
  
  public void setPiksc(Double pPiksc) {
    if (pPiksc == null) {
      return;
    }
    piksc = BigDecimal.valueOf(pPiksc).setScale(DECIMAL_PIKSC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiksc() {
    return piksc.setScale(DECIMAL_PIKSC, RoundingMode.HALF_UP);
  }
  
  public void setPidcc(BigDecimal pPidcc) {
    if (pPidcc == null) {
      return;
    }
    pidcc = pPidcc.setScale(DECIMAL_PIDCC, RoundingMode.HALF_UP);
  }
  
  public void setPidcc(Integer pPidcc) {
    if (pPidcc == null) {
      return;
    }
    pidcc = BigDecimal.valueOf(pPidcc);
  }
  
  public Integer getPidcc() {
    return pidcc.intValue();
  }
  
  public void setPidca(BigDecimal pPidca) {
    if (pPidca == null) {
      return;
    }
    pidca = pPidca.setScale(DECIMAL_PIDCA, RoundingMode.HALF_UP);
  }
  
  public void setPidca(Integer pPidca) {
    if (pPidca == null) {
      return;
    }
    pidca = BigDecimal.valueOf(pPidca);
  }
  
  public Integer getPidca() {
    return pidca.intValue();
  }
  
  public void setPidev(String pPidev) {
    if (pPidev == null) {
      return;
    }
    pidev = pPidev;
  }
  
  public String getPidev() {
    return pidev;
  }
  
  public void setPiqmi(BigDecimal pPiqmi) {
    if (pPiqmi == null) {
      return;
    }
    piqmi = pPiqmi.setScale(DECIMAL_PIQMI, RoundingMode.HALF_UP);
  }
  
  public void setPiqmi(Double pPiqmi) {
    if (pPiqmi == null) {
      return;
    }
    piqmi = BigDecimal.valueOf(pPiqmi).setScale(DECIMAL_PIQMI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqmi() {
    return piqmi.setScale(DECIMAL_PIQMI, RoundingMode.HALF_UP);
  }
  
  public void setPiqec(BigDecimal pPiqec) {
    if (pPiqec == null) {
      return;
    }
    piqec = pPiqec.setScale(DECIMAL_PIQEC, RoundingMode.HALF_UP);
  }
  
  public void setPiqec(Double pPiqec) {
    if (pPiqec == null) {
      return;
    }
    piqec = BigDecimal.valueOf(pPiqec).setScale(DECIMAL_PIQEC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqec() {
    return piqec.setScale(DECIMAL_PIQEC, RoundingMode.HALF_UP);
  }
  
  public void setPikpv(BigDecimal pPikpv) {
    if (pPikpv == null) {
      return;
    }
    pikpv = pPikpv.setScale(DECIMAL_PIKPV, RoundingMode.HALF_UP);
  }
  
  public void setPikpv(Double pPikpv) {
    if (pPikpv == null) {
      return;
    }
    pikpv = BigDecimal.valueOf(pPikpv).setScale(DECIMAL_PIKPV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPikpv() {
    return pikpv.setScale(DECIMAL_PIKPV, RoundingMode.HALF_UP);
  }
  
  public void setPiddp(BigDecimal pPiddp) {
    if (pPiddp == null) {
      return;
    }
    piddp = pPiddp.setScale(DECIMAL_PIDDP, RoundingMode.HALF_UP);
  }
  
  public void setPiddp(Integer pPiddp) {
    if (pPiddp == null) {
      return;
    }
    piddp = BigDecimal.valueOf(pPiddp);
  }
  
  public void setPiddp(Date pPiddp) {
    if (pPiddp == null) {
      return;
    }
    piddp = BigDecimal.valueOf(ConvertDate.dateToDb2(pPiddp));
  }
  
  public Integer getPiddp() {
    return piddp.intValue();
  }
  
  public Date getPiddpConvertiEnDate() {
    return ConvertDate.db2ToDate(piddp.intValue(), null);
  }
  
  public void setPirga(String pPirga) {
    if (pPirga == null) {
      return;
    }
    pirga = pPirga;
  }
  
  public String getPirga() {
    return pirga;
  }
  
  public void setPidels(BigDecimal pPidels) {
    if (pPidels == null) {
      return;
    }
    pidels = pPidels.setScale(DECIMAL_PIDELS, RoundingMode.HALF_UP);
  }
  
  public void setPidels(Integer pPidels) {
    if (pPidels == null) {
      return;
    }
    pidels = BigDecimal.valueOf(pPidels);
  }
  
  public Integer getPidels() {
    return pidels.intValue();
  }
  
  public void setPikpr(BigDecimal pPikpr) {
    if (pPikpr == null) {
      return;
    }
    pikpr = pPikpr.setScale(DECIMAL_PIKPR, RoundingMode.HALF_UP);
  }
  
  public void setPikpr(Double pPikpr) {
    if (pPikpr == null) {
      return;
    }
    pikpr = BigDecimal.valueOf(pPikpr).setScale(DECIMAL_PIKPR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPikpr() {
    return pikpr.setScale(DECIMAL_PIKPR, RoundingMode.HALF_UP);
  }
  
  public void setPiin1(Character pPiin1) {
    if (pPiin1 == null) {
      return;
    }
    piin1 = String.valueOf(pPiin1);
  }
  
  public Character getPiin1() {
    return piin1.charAt(0);
  }
  
  public void setPiin2(Character pPiin2) {
    if (pPiin2 == null) {
      return;
    }
    piin2 = String.valueOf(pPiin2);
  }
  
  public Character getPiin2() {
    return piin2.charAt(0);
  }
  
  public void setPiin3(Character pPiin3) {
    if (pPiin3 == null) {
      return;
    }
    piin3 = String.valueOf(pPiin3);
  }
  
  public Character getPiin3() {
    return piin3.charAt(0);
  }
  
  public void setPiin4(Character pPiin4) {
    if (pPiin4 == null) {
      return;
    }
    piin4 = String.valueOf(pPiin4);
  }
  
  public Character getPiin4() {
    return piin4.charAt(0);
  }
  
  public void setPiin5(Character pPiin5) {
    if (pPiin5 == null) {
      return;
    }
    piin5 = String.valueOf(pPiin5);
  }
  
  public Character getPiin5() {
    return piin5.charAt(0);
  }
  
  public void setPitrl(Character pPitrl) {
    if (pPitrl == null) {
      return;
    }
    pitrl = String.valueOf(pPitrl);
  }
  
  public Character getPitrl() {
    return pitrl.charAt(0);
  }
  
  public void setPirfc(String pPirfc) {
    if (pPirfc == null) {
      return;
    }
    pirfc = pPirfc;
  }
  
  public String getPirfc() {
    return pirfc;
  }
  
  public void setPigcd(BigDecimal pPigcd) {
    if (pPigcd == null) {
      return;
    }
    pigcd = pPigcd.setScale(DECIMAL_PIGCD, RoundingMode.HALF_UP);
  }
  
  public void setPigcd(Integer pPigcd) {
    if (pPigcd == null) {
      return;
    }
    pigcd = BigDecimal.valueOf(pPigcd);
  }
  
  public Integer getPigcd() {
    return pigcd.intValue();
  }
  
  public void setPidaf(BigDecimal pPidaf) {
    if (pPidaf == null) {
      return;
    }
    pidaf = pPidaf.setScale(DECIMAL_PIDAF, RoundingMode.HALF_UP);
  }
  
  public void setPidaf(Integer pPidaf) {
    if (pPidaf == null) {
      return;
    }
    pidaf = BigDecimal.valueOf(pPidaf);
  }
  
  public void setPidaf(Date pPidaf) {
    if (pPidaf == null) {
      return;
    }
    pidaf = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidaf));
  }
  
  public Integer getPidaf() {
    return pidaf.intValue();
  }
  
  public Date getPidafConvertiEnDate() {
    return ConvertDate.db2ToDate(pidaf.intValue(), null);
  }
  
  public void setPilib(String pPilib) {
    if (pPilib == null) {
      return;
    }
    pilib = pPilib;
  }
  
  public String getPilib() {
    return pilib;
  }
  
  public void setPiopa(String pPiopa) {
    if (pPiopa == null) {
      return;
    }
    piopa = pPiopa;
  }
  
  public String getPiopa() {
    return piopa;
  }
  
  public void setPiin6(Character pPiin6) {
    if (pPiin6 == null) {
      return;
    }
    piin6 = String.valueOf(pPiin6);
  }
  
  public Character getPiin6() {
    return piin6.charAt(0);
  }
  
  public void setPiin7(Character pPiin7) {
    if (pPiin7 == null) {
      return;
    }
    piin7 = String.valueOf(pPiin7);
  }
  
  public Character getPiin7() {
    return piin7.charAt(0);
  }
  
  public void setPiin8(Character pPiin8) {
    if (pPiin8 == null) {
      return;
    }
    piin8 = String.valueOf(pPiin8);
  }
  
  public Character getPiin8() {
    return piin8.charAt(0);
  }
  
  public void setPiin9(Character pPiin9) {
    if (pPiin9 == null) {
      return;
    }
    piin9 = String.valueOf(pPiin9);
  }
  
  public Character getPiin9() {
    return piin9.charAt(0);
  }
  
  public void setPipdi(BigDecimal pPipdi) {
    if (pPipdi == null) {
      return;
    }
    pipdi = pPipdi.setScale(DECIMAL_PIPDI, RoundingMode.HALF_UP);
  }
  
  public void setPipdi(Double pPipdi) {
    if (pPipdi == null) {
      return;
    }
    pipdi = BigDecimal.valueOf(pPipdi).setScale(DECIMAL_PIPDI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipdi() {
    return pipdi.setScale(DECIMAL_PIPDI, RoundingMode.HALF_UP);
  }
  
  public void setPikap(BigDecimal pPikap) {
    if (pPikap == null) {
      return;
    }
    pikap = pPikap.setScale(DECIMAL_PIKAP, RoundingMode.HALF_UP);
  }
  
  public void setPikap(Double pPikap) {
    if (pPikap == null) {
      return;
    }
    pikap = BigDecimal.valueOf(pPikap).setScale(DECIMAL_PIKAP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPikap() {
    return pikap.setScale(DECIMAL_PIKAP, RoundingMode.HALF_UP);
  }
  
  public void setPiprs(BigDecimal pPiprs) {
    if (pPiprs == null) {
      return;
    }
    piprs = pPiprs.setScale(DECIMAL_PIPRS, RoundingMode.HALF_UP);
  }
  
  public void setPiprs(Double pPiprs) {
    if (pPiprs == null) {
      return;
    }
    piprs = BigDecimal.valueOf(pPiprs).setScale(DECIMAL_PIPRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiprs() {
    return piprs.setScale(DECIMAL_PIPRS, RoundingMode.HALF_UP);
  }
  
  public void setPipgn(Character pPipgn) {
    if (pPipgn == null) {
      return;
    }
    pipgn = String.valueOf(pPipgn);
  }
  
  public Character getPipgn() {
    return pipgn.charAt(0);
  }
  
  public void setPifv1(BigDecimal pPifv1) {
    if (pPifv1 == null) {
      return;
    }
    pifv1 = pPifv1.setScale(DECIMAL_PIFV1, RoundingMode.HALF_UP);
  }
  
  public void setPifv1(Double pPifv1) {
    if (pPifv1 == null) {
      return;
    }
    pifv1 = BigDecimal.valueOf(pPifv1).setScale(DECIMAL_PIFV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifv1() {
    return pifv1.setScale(DECIMAL_PIFV1, RoundingMode.HALF_UP);
  }
  
  public void setPifk1(BigDecimal pPifk1) {
    if (pPifk1 == null) {
      return;
    }
    pifk1 = pPifk1.setScale(DECIMAL_PIFK1, RoundingMode.HALF_UP);
  }
  
  public void setPifk1(Double pPifk1) {
    if (pPifk1 == null) {
      return;
    }
    pifk1 = BigDecimal.valueOf(pPifk1).setScale(DECIMAL_PIFK1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifk1() {
    return pifk1.setScale(DECIMAL_PIFK1, RoundingMode.HALF_UP);
  }
  
  public void setPifp1(BigDecimal pPifp1) {
    if (pPifp1 == null) {
      return;
    }
    pifp1 = pPifp1.setScale(DECIMAL_PIFP1, RoundingMode.HALF_UP);
  }
  
  public void setPifp1(Double pPifp1) {
    if (pPifp1 == null) {
      return;
    }
    pifp1 = BigDecimal.valueOf(pPifp1).setScale(DECIMAL_PIFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifp1() {
    return pifp1.setScale(DECIMAL_PIFP1, RoundingMode.HALF_UP);
  }
  
  public void setPifu1(Character pPifu1) {
    if (pPifu1 == null) {
      return;
    }
    pifu1 = String.valueOf(pPifu1);
  }
  
  public Character getPifu1() {
    return pifu1.charAt(0);
  }
  
  public void setPifv2(BigDecimal pPifv2) {
    if (pPifv2 == null) {
      return;
    }
    pifv2 = pPifv2.setScale(DECIMAL_PIFV2, RoundingMode.HALF_UP);
  }
  
  public void setPifv2(Double pPifv2) {
    if (pPifv2 == null) {
      return;
    }
    pifv2 = BigDecimal.valueOf(pPifv2).setScale(DECIMAL_PIFV2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifv2() {
    return pifv2.setScale(DECIMAL_PIFV2, RoundingMode.HALF_UP);
  }
  
  public void setPifk2(BigDecimal pPifk2) {
    if (pPifk2 == null) {
      return;
    }
    pifk2 = pPifk2.setScale(DECIMAL_PIFK2, RoundingMode.HALF_UP);
  }
  
  public void setPifk2(Double pPifk2) {
    if (pPifk2 == null) {
      return;
    }
    pifk2 = BigDecimal.valueOf(pPifk2).setScale(DECIMAL_PIFK2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifk2() {
    return pifk2.setScale(DECIMAL_PIFK2, RoundingMode.HALF_UP);
  }
  
  public void setPifp2(BigDecimal pPifp2) {
    if (pPifp2 == null) {
      return;
    }
    pifp2 = pPifp2.setScale(DECIMAL_PIFP2, RoundingMode.HALF_UP);
  }
  
  public void setPifp2(Double pPifp2) {
    if (pPifp2 == null) {
      return;
    }
    pifp2 = BigDecimal.valueOf(pPifp2).setScale(DECIMAL_PIFP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifp2() {
    return pifp2.setScale(DECIMAL_PIFP2, RoundingMode.HALF_UP);
  }
  
  public void setPifu2(Character pPifu2) {
    if (pPifu2 == null) {
      return;
    }
    pifu2 = String.valueOf(pPifu2);
  }
  
  public Character getPifu2() {
    return pifu2.charAt(0);
  }
  
  public void setPifv3(BigDecimal pPifv3) {
    if (pPifv3 == null) {
      return;
    }
    pifv3 = pPifv3.setScale(DECIMAL_PIFV3, RoundingMode.HALF_UP);
  }
  
  public void setPifv3(Double pPifv3) {
    if (pPifv3 == null) {
      return;
    }
    pifv3 = BigDecimal.valueOf(pPifv3).setScale(DECIMAL_PIFV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifv3() {
    return pifv3.setScale(DECIMAL_PIFV3, RoundingMode.HALF_UP);
  }
  
  public void setPifk3(BigDecimal pPifk3) {
    if (pPifk3 == null) {
      return;
    }
    pifk3 = pPifk3.setScale(DECIMAL_PIFK3, RoundingMode.HALF_UP);
  }
  
  public void setPifk3(Double pPifk3) {
    if (pPifk3 == null) {
      return;
    }
    pifk3 = BigDecimal.valueOf(pPifk3).setScale(DECIMAL_PIFK3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPifk3() {
    return pifk3.setScale(DECIMAL_PIFK3, RoundingMode.HALF_UP);
  }
  
  public void setPifu3(Character pPifu3) {
    if (pPifu3 == null) {
      return;
    }
    pifu3 = String.valueOf(pPifu3);
  }
  
  public Character getPifu3() {
    return pifu3.charAt(0);
  }
  
  public void setPipr1(BigDecimal pPipr1) {
    if (pPipr1 == null) {
      return;
    }
    pipr1 = pPipr1.setScale(DECIMAL_PIPR1, RoundingMode.HALF_UP);
  }
  
  public void setPipr1(Double pPipr1) {
    if (pPipr1 == null) {
      return;
    }
    pipr1 = BigDecimal.valueOf(pPipr1).setScale(DECIMAL_PIPR1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipr1() {
    return pipr1.setScale(DECIMAL_PIPR1, RoundingMode.HALF_UP);
  }
  
  public void setPipr2(BigDecimal pPipr2) {
    if (pPipr2 == null) {
      return;
    }
    pipr2 = pPipr2.setScale(DECIMAL_PIPR2, RoundingMode.HALF_UP);
  }
  
  public void setPipr2(Double pPipr2) {
    if (pPipr2 == null) {
      return;
    }
    pipr2 = BigDecimal.valueOf(pPipr2).setScale(DECIMAL_PIPR2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipr2() {
    return pipr2.setScale(DECIMAL_PIPR2, RoundingMode.HALF_UP);
  }
  
  public void setPidat1(BigDecimal pPidat1) {
    if (pPidat1 == null) {
      return;
    }
    pidat1 = pPidat1.setScale(DECIMAL_PIDAT1, RoundingMode.HALF_UP);
  }
  
  public void setPidat1(Integer pPidat1) {
    if (pPidat1 == null) {
      return;
    }
    pidat1 = BigDecimal.valueOf(pPidat1);
  }
  
  public void setPidat1(Date pPidat1) {
    if (pPidat1 == null) {
      return;
    }
    pidat1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat1));
  }
  
  public Integer getPidat1() {
    return pidat1.intValue();
  }
  
  public Date getPidat1ConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat1.intValue(), null);
  }
  
  public void setPidat2(BigDecimal pPidat2) {
    if (pPidat2 == null) {
      return;
    }
    pidat2 = pPidat2.setScale(DECIMAL_PIDAT2, RoundingMode.HALF_UP);
  }
  
  public void setPidat2(Integer pPidat2) {
    if (pPidat2 == null) {
      return;
    }
    pidat2 = BigDecimal.valueOf(pPidat2);
  }
  
  public void setPidat2(Date pPidat2) {
    if (pPidat2 == null) {
      return;
    }
    pidat2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat2));
  }
  
  public Integer getPidat2() {
    return pidat2.intValue();
  }
  
  public Date getPidat2ConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat2.intValue(), null);
  }
}
