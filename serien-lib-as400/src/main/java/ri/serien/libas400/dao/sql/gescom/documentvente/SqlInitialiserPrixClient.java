/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.documentvente;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

public class SqlInitialiserPrixClient {
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlInitialiserPrixClient(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  // -- Méthodes publiques
  
  /**
   * Supprime une demande de déblocage.
   */
  public void initialiserPrixClientArticle(Client pClient) {
    Client.controlerId(pClient, true);
    
    // Préparation et lancement de la requête
    String requete = "delete from " + querymg.getLibrary() + '.' + EnumTableBDD.PRIX_CLIENT_ARTICLE_SUR_DOC + " where L1ETB = '"
        + pClient.getId().getCodeEtablissement() + "' " + "and E1CLLP = '" + pClient.getId().getNumero() + "' " + "and E1CLLS = '"
        + pClient.getId().getSuffixe() + "'";
    
    // Lancement de la requête
    int retour = querymg.requete(requete);
    if (retour == Constantes.ERREUR) {
      throw new MessageErreurException(querymg.getMsgError());
    }
  }
}
