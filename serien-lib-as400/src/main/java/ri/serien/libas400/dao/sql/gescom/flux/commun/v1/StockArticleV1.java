/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v1;

public class StockArticleV1 {
  private String codeArticle = null;
  private String stockDispo = null;
  
  public StockArticleV1() {
    
  }
  
  public String getCodeArticle() {
    return codeArticle;
  }
  
  public void setCodeArticle(String codeArticle) {
    this.codeArticle = codeArticle;
  }
  
  public String getStockDispo() {
    return stockDispo;
  }
  
  public void setStockDispo(String stockDispo) {
    this.stockDispo = stockDispo;
  }
  
}
