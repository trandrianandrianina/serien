/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class PsemrtlmManager extends QueryManager {
  /**
   * Constructeur
   * @param database
   */
  public PsemrtlmManager(Connection database) {
    super(database);
  }
  
  /**
   * Retourne la liste des associations (pour les menus)
   * @param bibenv
   * @param prf
   * @return
   * 
   */
  public ArrayList<GenericRecord> getTousEnregistrements() {
    try {
      return select("Select * from fm500.psemrtlm");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
  }
  
}
