/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.conditionachat;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvx0019o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POTOP = 1;
  public static final int DECIMAL_POTOP = 0;
  public static final int SIZE_POCRE = 7;
  public static final int DECIMAL_POCRE = 0;
  public static final int SIZE_POMOD = 7;
  public static final int DECIMAL_POMOD = 0;
  public static final int SIZE_POTRT = 7;
  public static final int DECIMAL_POTRT = 0;
  public static final int SIZE_POETB = 3;
  public static final int SIZE_POART = 20;
  public static final int SIZE_POCOL = 1;
  public static final int DECIMAL_POCOL = 0;
  public static final int SIZE_POFRS = 6;
  public static final int DECIMAL_POFRS = 0;
  public static final int SIZE_PODAP = 7;
  public static final int DECIMAL_PODAP = 0;
  public static final int SIZE_POCPV = 1;
  public static final int SIZE_POREM1 = 4;
  public static final int DECIMAL_POREM1 = 2;
  public static final int SIZE_POREM2 = 4;
  public static final int DECIMAL_POREM2 = 2;
  public static final int SIZE_POREM3 = 4;
  public static final int DECIMAL_POREM3 = 2;
  public static final int SIZE_POREM4 = 4;
  public static final int DECIMAL_POREM4 = 2;
  public static final int SIZE_POREM5 = 4;
  public static final int DECIMAL_POREM5 = 2;
  public static final int SIZE_POREM6 = 4;
  public static final int DECIMAL_POREM6 = 2;
  public static final int SIZE_POPRA = 11;
  public static final int DECIMAL_POPRA = 2;
  public static final int SIZE_POUNA = 2;
  public static final int SIZE_PONUA = 8;
  public static final int DECIMAL_PONUA = 3;
  public static final int SIZE_POKAC = 8;
  public static final int DECIMAL_POKAC = 3;
  public static final int SIZE_PODEL = 2;
  public static final int DECIMAL_PODEL = 0;
  public static final int SIZE_POREF = 20;
  public static final int SIZE_POUNC = 2;
  public static final int SIZE_POKSC = 8;
  public static final int DECIMAL_POKSC = 3;
  public static final int SIZE_PODCC = 1;
  public static final int DECIMAL_PODCC = 0;
  public static final int SIZE_PODCA = 1;
  public static final int DECIMAL_PODCA = 0;
  public static final int SIZE_PODEV = 3;
  public static final int SIZE_POQMI = 10;
  public static final int DECIMAL_POQMI = 3;
  public static final int SIZE_POQEC = 10;
  public static final int DECIMAL_POQEC = 3;
  public static final int SIZE_POKPV = 4;
  public static final int DECIMAL_POKPV = 3;
  public static final int SIZE_PODDP = 7;
  public static final int DECIMAL_PODDP = 0;
  public static final int SIZE_PORGA = 10;
  public static final int SIZE_PODELS = 2;
  public static final int DECIMAL_PODELS = 0;
  public static final int SIZE_POKPR = 5;
  public static final int DECIMAL_POKPR = 4;
  public static final int SIZE_POIN1 = 1;
  public static final int SIZE_POIN2 = 1;
  public static final int SIZE_POIN3 = 1;
  public static final int SIZE_POIN4 = 1;
  public static final int SIZE_POIN5 = 1;
  public static final int SIZE_POTRL = 1;
  public static final int SIZE_PORFC = 20;
  public static final int SIZE_POGCD = 13;
  public static final int DECIMAL_POGCD = 0;
  public static final int SIZE_PODAF = 7;
  public static final int DECIMAL_PODAF = 0;
  public static final int SIZE_POLIB = 30;
  public static final int SIZE_POOPA = 3;
  public static final int SIZE_POIN6 = 1;
  public static final int SIZE_POIN7 = 1;
  public static final int SIZE_POIN8 = 1;
  public static final int SIZE_POIN9 = 1;
  public static final int SIZE_POPDI = 11;
  public static final int DECIMAL_POPDI = 2;
  public static final int SIZE_POKAP = 5;
  public static final int DECIMAL_POKAP = 4;
  public static final int SIZE_POPRS = 9;
  public static final int DECIMAL_POPRS = 2;
  public static final int SIZE_POPGN = 1;
  public static final int SIZE_POFV1 = 9;
  public static final int DECIMAL_POFV1 = 2;
  public static final int SIZE_POFK1 = 7;
  public static final int DECIMAL_POFK1 = 4;
  public static final int SIZE_POFP1 = 4;
  public static final int DECIMAL_POFP1 = 2;
  public static final int SIZE_POFU1 = 1;
  public static final int SIZE_POFV2 = 9;
  public static final int DECIMAL_POFV2 = 2;
  public static final int SIZE_POFK2 = 7;
  public static final int DECIMAL_POFK2 = 4;
  public static final int SIZE_POFP2 = 4;
  public static final int DECIMAL_POFP2 = 2;
  public static final int SIZE_POFU2 = 1;
  public static final int SIZE_POFV3 = 9;
  public static final int DECIMAL_POFV3 = 2;
  public static final int SIZE_POFK3 = 7;
  public static final int DECIMAL_POFK3 = 4;
  public static final int SIZE_POFU3 = 1;
  public static final int SIZE_POPR1 = 11;
  public static final int DECIMAL_POPR1 = 2;
  public static final int SIZE_POPR2 = 11;
  public static final int DECIMAL_POPR2 = 2;
  public static final int SIZE_PODAT1 = 7;
  public static final int DECIMAL_PODAT1 = 0;
  public static final int SIZE_PODAT2 = 7;
  public static final int DECIMAL_PODAT2 = 0;
  public static final int SIZE_POPRNET = 11;
  public static final int DECIMAL_POPRNET = 2;
  public static final int SIZE_POPRPRF = 11;
  public static final int DECIMAL_POPRPRF = 2;
  public static final int SIZE_POPRPRS = 11;
  public static final int DECIMAL_POPRPRS = 2;
  public static final int SIZE_POERR1 = 1;
  public static final int SIZE_POERR2 = 1;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 448;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POTOP = 1;
  public static final int VAR_POCRE = 2;
  public static final int VAR_POMOD = 3;
  public static final int VAR_POTRT = 4;
  public static final int VAR_POETB = 5;
  public static final int VAR_POART = 6;
  public static final int VAR_POCOL = 7;
  public static final int VAR_POFRS = 8;
  public static final int VAR_PODAP = 9;
  public static final int VAR_POCPV = 10;
  public static final int VAR_POREM1 = 11;
  public static final int VAR_POREM2 = 12;
  public static final int VAR_POREM3 = 13;
  public static final int VAR_POREM4 = 14;
  public static final int VAR_POREM5 = 15;
  public static final int VAR_POREM6 = 16;
  public static final int VAR_POPRA = 17;
  public static final int VAR_POUNA = 18;
  public static final int VAR_PONUA = 19;
  public static final int VAR_POKAC = 20;
  public static final int VAR_PODEL = 21;
  public static final int VAR_POREF = 22;
  public static final int VAR_POUNC = 23;
  public static final int VAR_POKSC = 24;
  public static final int VAR_PODCC = 25;
  public static final int VAR_PODCA = 26;
  public static final int VAR_PODEV = 27;
  public static final int VAR_POQMI = 28;
  public static final int VAR_POQEC = 29;
  public static final int VAR_POKPV = 30;
  public static final int VAR_PODDP = 31;
  public static final int VAR_PORGA = 32;
  public static final int VAR_PODELS = 33;
  public static final int VAR_POKPR = 34;
  public static final int VAR_POIN1 = 35;
  public static final int VAR_POIN2 = 36;
  public static final int VAR_POIN3 = 37;
  public static final int VAR_POIN4 = 38;
  public static final int VAR_POIN5 = 39;
  public static final int VAR_POTRL = 40;
  public static final int VAR_PORFC = 41;
  public static final int VAR_POGCD = 42;
  public static final int VAR_PODAF = 43;
  public static final int VAR_POLIB = 44;
  public static final int VAR_POOPA = 45;
  public static final int VAR_POIN6 = 46;
  public static final int VAR_POIN7 = 47;
  public static final int VAR_POIN8 = 48;
  public static final int VAR_POIN9 = 49;
  public static final int VAR_POPDI = 50;
  public static final int VAR_POKAP = 51;
  public static final int VAR_POPRS = 52;
  public static final int VAR_POPGN = 53;
  public static final int VAR_POFV1 = 54;
  public static final int VAR_POFK1 = 55;
  public static final int VAR_POFP1 = 56;
  public static final int VAR_POFU1 = 57;
  public static final int VAR_POFV2 = 58;
  public static final int VAR_POFK2 = 59;
  public static final int VAR_POFP2 = 60;
  public static final int VAR_POFU2 = 61;
  public static final int VAR_POFV3 = 62;
  public static final int VAR_POFK3 = 63;
  public static final int VAR_POFU3 = 64;
  public static final int VAR_POPR1 = 65;
  public static final int VAR_POPR2 = 66;
  public static final int VAR_PODAT1 = 67;
  public static final int VAR_PODAT2 = 68;
  public static final int VAR_POPRNET = 69;
  public static final int VAR_POPRPRF = 70;
  public static final int VAR_POPRPRS = 71;
  public static final int VAR_POERR1 = 72;
  public static final int VAR_POERR2 = 73;
  public static final int VAR_POARR = 74;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal potop = BigDecimal.ZERO; // Top système
  private BigDecimal pocre = BigDecimal.ZERO; // Date de création
  private BigDecimal pomod = BigDecimal.ZERO; // Date de modification
  private BigDecimal potrt = BigDecimal.ZERO; // Date de traitement
  private String poetb = ""; // Code établissement
  private String poart = ""; // Code article
  private BigDecimal pocol = BigDecimal.ZERO; // Collectif Fournisseur
  private BigDecimal pofrs = BigDecimal.ZERO; // Numéro Fournisseur
  private BigDecimal podap = BigDecimal.ZERO; // Date application
  private String pocpv = ""; // Top calcul prix de vente
  private BigDecimal porem1 = BigDecimal.ZERO; // Remise 1
  private BigDecimal porem2 = BigDecimal.ZERO; // Remise 2
  private BigDecimal porem3 = BigDecimal.ZERO; // Remise 3
  private BigDecimal porem4 = BigDecimal.ZERO; // Remise 4
  private BigDecimal porem5 = BigDecimal.ZERO; // Remise 5
  private BigDecimal porem6 = BigDecimal.ZERO; // Remise 6
  private BigDecimal popra = BigDecimal.ZERO; // Prix catalogue
  private String pouna = ""; // Unité achat
  private BigDecimal ponua = BigDecimal.ZERO; // Conditionnement
  private BigDecimal pokac = BigDecimal.ZERO; // Nombre unité achat/unité Cde
  private BigDecimal podel = BigDecimal.ZERO; // Délai de livraison
  private String poref = ""; // Référence fournisseur
  private String pounc = ""; // Unité de commande
  private BigDecimal poksc = BigDecimal.ZERO; // Nombre unité Stk/unité Cde
  private BigDecimal podcc = BigDecimal.ZERO; // Zone système
  private BigDecimal podca = BigDecimal.ZERO; // Zone système
  private String podev = ""; // Devise
  private BigDecimal poqmi = BigDecimal.ZERO; // Quantité minimum de commande
  private BigDecimal poqec = BigDecimal.ZERO; // Quantité économique
  private BigDecimal pokpv = BigDecimal.ZERO; // Coeff. calcul prix de vente
  private BigDecimal poddp = BigDecimal.ZERO; // Date de dernier prix
  private String porga = ""; // Regroupement Achat
  private BigDecimal podels = BigDecimal.ZERO; // Délai supplémentaire
  private BigDecimal pokpr = BigDecimal.ZERO; // Coeff. calcul prix
  private String poin1 = ""; // P=Promo, C=Colonnes
  private String poin2 = ""; // Note qualité de 0 à 9
  private String poin3 = ""; // G = Frs GBA / R= Frs Réf
  private String poin4 = ""; // 3 remises pour calcul PV min
  private String poin5 = ""; // Ind. non utilisé
  private String potrl = ""; // Type remise ligne
  private String porfc = ""; // Référence constructeur
  private BigDecimal pogcd = BigDecimal.ZERO; // Code gencod frs
  private BigDecimal podaf = BigDecimal.ZERO; // Date limite validité
  private String polib = ""; // Libellé
  private String poopa = ""; // origine
  private String poin6 = ""; // Ind. non utilisé
  private String poin7 = ""; // Delai en Jours et Non Semain
  private String poin8 = ""; // Ind. non utilisé
  private String poin9 = ""; // Ind. non utilisé
  private BigDecimal popdi = BigDecimal.ZERO; // Prix distributeur
  private BigDecimal pokap = BigDecimal.ZERO; // Coeff. approche fixe
  private BigDecimal poprs = BigDecimal.ZERO; // Prix de revient standard
  private String popgn = ""; // Top prix négocié
  private BigDecimal pofv1 = BigDecimal.ZERO; // Frais 1 valeur
  private BigDecimal pofk1 = BigDecimal.ZERO; // Frais 1 coefficient
  private BigDecimal pofp1 = BigDecimal.ZERO; // Frais 1 % PORT
  private String pofu1 = ""; // Unité frais 1
  private BigDecimal pofv2 = BigDecimal.ZERO; // Frais 2 valeur
  private BigDecimal pofk2 = BigDecimal.ZERO; // Frais 2 coefficient
  private BigDecimal pofp2 = BigDecimal.ZERO; // Frais 1 % MAJORATION
  private String pofu2 = ""; // Unité frais 2
  private BigDecimal pofv3 = BigDecimal.ZERO; // Frais 3 valeur
  private BigDecimal pofk3 = BigDecimal.ZERO; // Frais 3 coefficient
  private String pofu3 = ""; // Unité frais 3
  private BigDecimal popr1 = BigDecimal.ZERO; // Zone non utilisée
  private BigDecimal popr2 = BigDecimal.ZERO; // Zone non utilisée
  private BigDecimal podat1 = BigDecimal.ZERO; // Zone date non utilisée
  private BigDecimal podat2 = BigDecimal.ZERO; // Zone date non utilisée
  private BigDecimal poprnet = BigDecimal.ZERO; // Prix d"achat net
  private BigDecimal poprprf = BigDecimal.ZERO; // Prix de revient fournisseur
  private BigDecimal poprprs = BigDecimal.ZERO; // Prix de revient standard
  private String poerr1 = ""; // Article inexsitant = 1
  private String poerr2 = ""; // CNA non trouvée = 1
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_POTOP, DECIMAL_POTOP), // Top système
      new AS400PackedDecimal(SIZE_POCRE, DECIMAL_POCRE), // Date de création
      new AS400PackedDecimal(SIZE_POMOD, DECIMAL_POMOD), // Date de modification
      new AS400PackedDecimal(SIZE_POTRT, DECIMAL_POTRT), // Date de traitement
      new AS400Text(SIZE_POETB), // Code établissement
      new AS400Text(SIZE_POART), // Code article
      new AS400ZonedDecimal(SIZE_POCOL, DECIMAL_POCOL), // Collectif Fournisseur
      new AS400ZonedDecimal(SIZE_POFRS, DECIMAL_POFRS), // Numéro Fournisseur
      new AS400ZonedDecimal(SIZE_PODAP, DECIMAL_PODAP), // Date application
      new AS400Text(SIZE_POCPV), // Top calcul prix de vente
      new AS400ZonedDecimal(SIZE_POREM1, DECIMAL_POREM1), // Remise 1
      new AS400ZonedDecimal(SIZE_POREM2, DECIMAL_POREM2), // Remise 2
      new AS400ZonedDecimal(SIZE_POREM3, DECIMAL_POREM3), // Remise 3
      new AS400ZonedDecimal(SIZE_POREM4, DECIMAL_POREM4), // Remise 4
      new AS400ZonedDecimal(SIZE_POREM5, DECIMAL_POREM5), // Remise 5
      new AS400ZonedDecimal(SIZE_POREM6, DECIMAL_POREM6), // Remise 6
      new AS400PackedDecimal(SIZE_POPRA, DECIMAL_POPRA), // Prix catalogue
      new AS400Text(SIZE_POUNA), // Unité achat
      new AS400PackedDecimal(SIZE_PONUA, DECIMAL_PONUA), // Conditionnement
      new AS400PackedDecimal(SIZE_POKAC, DECIMAL_POKAC), // Nombre unité achat/unité Cde
      new AS400ZonedDecimal(SIZE_PODEL, DECIMAL_PODEL), // Délai de livraison
      new AS400Text(SIZE_POREF), // Référence fournisseur
      new AS400Text(SIZE_POUNC), // Unité de commande
      new AS400PackedDecimal(SIZE_POKSC, DECIMAL_POKSC), // Nombre unité Stk/unité Cde
      new AS400ZonedDecimal(SIZE_PODCC, DECIMAL_PODCC), // Zone système
      new AS400ZonedDecimal(SIZE_PODCA, DECIMAL_PODCA), // Zone système
      new AS400Text(SIZE_PODEV), // Devise
      new AS400PackedDecimal(SIZE_POQMI, DECIMAL_POQMI), // Quantité minimum de commande
      new AS400PackedDecimal(SIZE_POQEC, DECIMAL_POQEC), // Quantité économique
      new AS400ZonedDecimal(SIZE_POKPV, DECIMAL_POKPV), // Coeff. calcul prix de vente
      new AS400PackedDecimal(SIZE_PODDP, DECIMAL_PODDP), // Date de dernier prix
      new AS400Text(SIZE_PORGA), // Regroupement Achat
      new AS400ZonedDecimal(SIZE_PODELS, DECIMAL_PODELS), // Délai supplémentaire
      new AS400PackedDecimal(SIZE_POKPR, DECIMAL_POKPR), // Coeff. calcul prix
      new AS400Text(SIZE_POIN1), // P=Promo, C=Colonnes
      new AS400Text(SIZE_POIN2), // Note qualité de 0 à 9
      new AS400Text(SIZE_POIN3), // G = Frs GBA / R= Frs Réf
      new AS400Text(SIZE_POIN4), // 3 remises pour calcul PV min
      new AS400Text(SIZE_POIN5), // Ind. non utilisé
      new AS400Text(SIZE_POTRL), // Type remise ligne
      new AS400Text(SIZE_PORFC), // Référence constructeur
      new AS400ZonedDecimal(SIZE_POGCD, DECIMAL_POGCD), // Code gencod frs
      new AS400ZonedDecimal(SIZE_PODAF, DECIMAL_PODAF), // Date limite validité
      new AS400Text(SIZE_POLIB), // Libellé
      new AS400Text(SIZE_POOPA), // origine
      new AS400Text(SIZE_POIN6), // Ind. non utilisé
      new AS400Text(SIZE_POIN7), // Delai en Jours et Non Semain
      new AS400Text(SIZE_POIN8), // Ind. non utilisé
      new AS400Text(SIZE_POIN9), // Ind. non utilisé
      new AS400PackedDecimal(SIZE_POPDI, DECIMAL_POPDI), // Prix distributeur
      new AS400ZonedDecimal(SIZE_POKAP, DECIMAL_POKAP), // Coeff. approche fixe
      new AS400PackedDecimal(SIZE_POPRS, DECIMAL_POPRS), // Prix de revient standard
      new AS400Text(SIZE_POPGN), // Top prix négocié
      new AS400PackedDecimal(SIZE_POFV1, DECIMAL_POFV1), // Frais 1 valeur
      new AS400PackedDecimal(SIZE_POFK1, DECIMAL_POFK1), // Frais 1 coefficient
      new AS400ZonedDecimal(SIZE_POFP1, DECIMAL_POFP1), // Frais 1 % PORT
      new AS400Text(SIZE_POFU1), // Unité frais 1
      new AS400PackedDecimal(SIZE_POFV2, DECIMAL_POFV2), // Frais 2 valeur
      new AS400PackedDecimal(SIZE_POFK2, DECIMAL_POFK2), // Frais 2 coefficient
      new AS400PackedDecimal(SIZE_POFP2, DECIMAL_POFP2), // Frais 1 % MAJORATION
      new AS400Text(SIZE_POFU2), // Unité frais 2
      new AS400PackedDecimal(SIZE_POFV3, DECIMAL_POFV3), // Frais 3 valeur
      new AS400PackedDecimal(SIZE_POFK3, DECIMAL_POFK3), // Frais 3 coefficient
      new AS400Text(SIZE_POFU3), // Unité frais 3
      new AS400PackedDecimal(SIZE_POPR1, DECIMAL_POPR1), // Zone non utilisée
      new AS400PackedDecimal(SIZE_POPR2, DECIMAL_POPR2), // Zone non utilisée
      new AS400PackedDecimal(SIZE_PODAT1, DECIMAL_PODAT1), // Zone date non utilisée
      new AS400PackedDecimal(SIZE_PODAT2, DECIMAL_PODAT2), // Zone date non utilisée
      new AS400ZonedDecimal(SIZE_POPRNET, DECIMAL_POPRNET), // Prix d"achat net
      new AS400ZonedDecimal(SIZE_POPRPRF, DECIMAL_POPRPRF), // Prix de revient fournisseur
      new AS400ZonedDecimal(SIZE_POPRPRS, DECIMAL_POPRPRS), // Prix de revient standard
      new AS400Text(SIZE_POERR1), // Article inexsitant = 1
      new AS400Text(SIZE_POERR2), // CNA non trouvée = 1
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { poind, potop, pocre, pomod, potrt, poetb, poart, pocol, pofrs, podap, pocpv, porem1, porem2, porem3, porem4, porem5,
          porem6, popra, pouna, ponua, pokac, podel, poref, pounc, poksc, podcc, podca, podev, poqmi, poqec, pokpv, poddp, porga, podels,
          pokpr, poin1, poin2, poin3, poin4, poin5, potrl, porfc, pogcd, podaf, polib, poopa, poin6, poin7, poin8, poin9, popdi, pokap,
          poprs, popgn, pofv1, pofk1, pofp1, pofu1, pofv2, pofk2, pofp2, pofu2, pofv3, pofk3, pofu3, popr1, popr2, podat1, podat2,
          poprnet, poprprf, poprprs, poerr1, poerr2, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    potop = (BigDecimal) output[1];
    pocre = (BigDecimal) output[2];
    pomod = (BigDecimal) output[3];
    potrt = (BigDecimal) output[4];
    poetb = (String) output[5];
    poart = (String) output[6];
    pocol = (BigDecimal) output[7];
    pofrs = (BigDecimal) output[8];
    podap = (BigDecimal) output[9];
    pocpv = (String) output[10];
    porem1 = (BigDecimal) output[11];
    porem2 = (BigDecimal) output[12];
    porem3 = (BigDecimal) output[13];
    porem4 = (BigDecimal) output[14];
    porem5 = (BigDecimal) output[15];
    porem6 = (BigDecimal) output[16];
    popra = (BigDecimal) output[17];
    pouna = (String) output[18];
    ponua = (BigDecimal) output[19];
    pokac = (BigDecimal) output[20];
    podel = (BigDecimal) output[21];
    poref = (String) output[22];
    pounc = (String) output[23];
    poksc = (BigDecimal) output[24];
    podcc = (BigDecimal) output[25];
    podca = (BigDecimal) output[26];
    podev = (String) output[27];
    poqmi = (BigDecimal) output[28];
    poqec = (BigDecimal) output[29];
    pokpv = (BigDecimal) output[30];
    poddp = (BigDecimal) output[31];
    porga = (String) output[32];
    podels = (BigDecimal) output[33];
    pokpr = (BigDecimal) output[34];
    poin1 = (String) output[35];
    poin2 = (String) output[36];
    poin3 = (String) output[37];
    poin4 = (String) output[38];
    poin5 = (String) output[39];
    potrl = (String) output[40];
    porfc = (String) output[41];
    pogcd = (BigDecimal) output[42];
    podaf = (BigDecimal) output[43];
    polib = (String) output[44];
    poopa = (String) output[45];
    poin6 = (String) output[46];
    poin7 = (String) output[47];
    poin8 = (String) output[48];
    poin9 = (String) output[49];
    popdi = (BigDecimal) output[50];
    pokap = (BigDecimal) output[51];
    poprs = (BigDecimal) output[52];
    popgn = (String) output[53];
    pofv1 = (BigDecimal) output[54];
    pofk1 = (BigDecimal) output[55];
    pofp1 = (BigDecimal) output[56];
    pofu1 = (String) output[57];
    pofv2 = (BigDecimal) output[58];
    pofk2 = (BigDecimal) output[59];
    pofp2 = (BigDecimal) output[60];
    pofu2 = (String) output[61];
    pofv3 = (BigDecimal) output[62];
    pofk3 = (BigDecimal) output[63];
    pofu3 = (String) output[64];
    popr1 = (BigDecimal) output[65];
    popr2 = (BigDecimal) output[66];
    podat1 = (BigDecimal) output[67];
    podat2 = (BigDecimal) output[68];
    poprnet = (BigDecimal) output[69];
    poprprf = (BigDecimal) output[70];
    poprprs = (BigDecimal) output[71];
    poerr1 = (String) output[72];
    poerr2 = (String) output[73];
    poarr = (String) output[74];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPotop(BigDecimal pPotop) {
    if (pPotop == null) {
      return;
    }
    potop = pPotop.setScale(DECIMAL_POTOP, RoundingMode.HALF_UP);
  }
  
  public void setPotop(Integer pPotop) {
    if (pPotop == null) {
      return;
    }
    potop = BigDecimal.valueOf(pPotop);
  }
  
  public Integer getPotop() {
    return potop.intValue();
  }
  
  public void setPocre(BigDecimal pPocre) {
    if (pPocre == null) {
      return;
    }
    pocre = pPocre.setScale(DECIMAL_POCRE, RoundingMode.HALF_UP);
  }
  
  public void setPocre(Integer pPocre) {
    if (pPocre == null) {
      return;
    }
    pocre = BigDecimal.valueOf(pPocre);
  }
  
  public void setPocre(Date pPocre) {
    if (pPocre == null) {
      return;
    }
    pocre = BigDecimal.valueOf(ConvertDate.dateToDb2(pPocre));
  }
  
  public Integer getPocre() {
    return pocre.intValue();
  }
  
  public Date getPocreConvertiEnDate() {
    return ConvertDate.db2ToDate(pocre.intValue(), null);
  }
  
  public void setPomod(BigDecimal pPomod) {
    if (pPomod == null) {
      return;
    }
    pomod = pPomod.setScale(DECIMAL_POMOD, RoundingMode.HALF_UP);
  }
  
  public void setPomod(Integer pPomod) {
    if (pPomod == null) {
      return;
    }
    pomod = BigDecimal.valueOf(pPomod);
  }
  
  public void setPomod(Date pPomod) {
    if (pPomod == null) {
      return;
    }
    pomod = BigDecimal.valueOf(ConvertDate.dateToDb2(pPomod));
  }
  
  public Integer getPomod() {
    return pomod.intValue();
  }
  
  public Date getPomodConvertiEnDate() {
    return ConvertDate.db2ToDate(pomod.intValue(), null);
  }
  
  public void setPotrt(BigDecimal pPotrt) {
    if (pPotrt == null) {
      return;
    }
    potrt = pPotrt.setScale(DECIMAL_POTRT, RoundingMode.HALF_UP);
  }
  
  public void setPotrt(Integer pPotrt) {
    if (pPotrt == null) {
      return;
    }
    potrt = BigDecimal.valueOf(pPotrt);
  }
  
  public void setPotrt(Date pPotrt) {
    if (pPotrt == null) {
      return;
    }
    potrt = BigDecimal.valueOf(ConvertDate.dateToDb2(pPotrt));
  }
  
  public Integer getPotrt() {
    return potrt.intValue();
  }
  
  public Date getPotrtConvertiEnDate() {
    return ConvertDate.db2ToDate(potrt.intValue(), null);
  }
  
  public void setPoetb(String pPoetb) {
    if (pPoetb == null) {
      return;
    }
    poetb = pPoetb;
  }
  
  public String getPoetb() {
    return poetb;
  }
  
  public void setPoart(String pPoart) {
    if (pPoart == null) {
      return;
    }
    poart = pPoart;
  }
  
  public String getPoart() {
    return poart;
  }
  
  public void setPocol(BigDecimal pPocol) {
    if (pPocol == null) {
      return;
    }
    pocol = pPocol.setScale(DECIMAL_POCOL, RoundingMode.HALF_UP);
  }
  
  public void setPocol(Integer pPocol) {
    if (pPocol == null) {
      return;
    }
    pocol = BigDecimal.valueOf(pPocol);
  }
  
  public Integer getPocol() {
    return pocol.intValue();
  }
  
  public void setPofrs(BigDecimal pPofrs) {
    if (pPofrs == null) {
      return;
    }
    pofrs = pPofrs.setScale(DECIMAL_POFRS, RoundingMode.HALF_UP);
  }
  
  public void setPofrs(Integer pPofrs) {
    if (pPofrs == null) {
      return;
    }
    pofrs = BigDecimal.valueOf(pPofrs);
  }
  
  public Integer getPofrs() {
    return pofrs.intValue();
  }
  
  public void setPodap(BigDecimal pPodap) {
    if (pPodap == null) {
      return;
    }
    podap = pPodap.setScale(DECIMAL_PODAP, RoundingMode.HALF_UP);
  }
  
  public void setPodap(Integer pPodap) {
    if (pPodap == null) {
      return;
    }
    podap = BigDecimal.valueOf(pPodap);
  }
  
  public void setPodap(Date pPodap) {
    if (pPodap == null) {
      return;
    }
    podap = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodap));
  }
  
  public Integer getPodap() {
    return podap.intValue();
  }
  
  public Date getPodapConvertiEnDate() {
    return ConvertDate.db2ToDate(podap.intValue(), null);
  }
  
  public void setPocpv(Character pPocpv) {
    if (pPocpv == null) {
      return;
    }
    pocpv = String.valueOf(pPocpv);
  }
  
  public Character getPocpv() {
    return pocpv.charAt(0);
  }
  
  public void setPorem1(BigDecimal pPorem1) {
    if (pPorem1 == null) {
      return;
    }
    porem1 = pPorem1.setScale(DECIMAL_POREM1, RoundingMode.HALF_UP);
  }
  
  public void setPorem1(Double pPorem1) {
    if (pPorem1 == null) {
      return;
    }
    porem1 = BigDecimal.valueOf(pPorem1).setScale(DECIMAL_POREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPorem1() {
    return porem1.setScale(DECIMAL_POREM1, RoundingMode.HALF_UP);
  }
  
  public void setPorem2(BigDecimal pPorem2) {
    if (pPorem2 == null) {
      return;
    }
    porem2 = pPorem2.setScale(DECIMAL_POREM2, RoundingMode.HALF_UP);
  }
  
  public void setPorem2(Double pPorem2) {
    if (pPorem2 == null) {
      return;
    }
    porem2 = BigDecimal.valueOf(pPorem2).setScale(DECIMAL_POREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPorem2() {
    return porem2.setScale(DECIMAL_POREM2, RoundingMode.HALF_UP);
  }
  
  public void setPorem3(BigDecimal pPorem3) {
    if (pPorem3 == null) {
      return;
    }
    porem3 = pPorem3.setScale(DECIMAL_POREM3, RoundingMode.HALF_UP);
  }
  
  public void setPorem3(Double pPorem3) {
    if (pPorem3 == null) {
      return;
    }
    porem3 = BigDecimal.valueOf(pPorem3).setScale(DECIMAL_POREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPorem3() {
    return porem3.setScale(DECIMAL_POREM3, RoundingMode.HALF_UP);
  }
  
  public void setPorem4(BigDecimal pPorem4) {
    if (pPorem4 == null) {
      return;
    }
    porem4 = pPorem4.setScale(DECIMAL_POREM4, RoundingMode.HALF_UP);
  }
  
  public void setPorem4(Double pPorem4) {
    if (pPorem4 == null) {
      return;
    }
    porem4 = BigDecimal.valueOf(pPorem4).setScale(DECIMAL_POREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPorem4() {
    return porem4.setScale(DECIMAL_POREM4, RoundingMode.HALF_UP);
  }
  
  public void setPorem5(BigDecimal pPorem5) {
    if (pPorem5 == null) {
      return;
    }
    porem5 = pPorem5.setScale(DECIMAL_POREM5, RoundingMode.HALF_UP);
  }
  
  public void setPorem5(Double pPorem5) {
    if (pPorem5 == null) {
      return;
    }
    porem5 = BigDecimal.valueOf(pPorem5).setScale(DECIMAL_POREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPorem5() {
    return porem5.setScale(DECIMAL_POREM5, RoundingMode.HALF_UP);
  }
  
  public void setPorem6(BigDecimal pPorem6) {
    if (pPorem6 == null) {
      return;
    }
    porem6 = pPorem6.setScale(DECIMAL_POREM6, RoundingMode.HALF_UP);
  }
  
  public void setPorem6(Double pPorem6) {
    if (pPorem6 == null) {
      return;
    }
    porem6 = BigDecimal.valueOf(pPorem6).setScale(DECIMAL_POREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPorem6() {
    return porem6.setScale(DECIMAL_POREM6, RoundingMode.HALF_UP);
  }
  
  public void setPopra(BigDecimal pPopra) {
    if (pPopra == null) {
      return;
    }
    popra = pPopra.setScale(DECIMAL_POPRA, RoundingMode.HALF_UP);
  }
  
  public void setPopra(Double pPopra) {
    if (pPopra == null) {
      return;
    }
    popra = BigDecimal.valueOf(pPopra).setScale(DECIMAL_POPRA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPopra() {
    return popra.setScale(DECIMAL_POPRA, RoundingMode.HALF_UP);
  }
  
  public void setPouna(String pPouna) {
    if (pPouna == null) {
      return;
    }
    pouna = pPouna;
  }
  
  public String getPouna() {
    return pouna;
  }
  
  public void setPonua(BigDecimal pPonua) {
    if (pPonua == null) {
      return;
    }
    ponua = pPonua.setScale(DECIMAL_PONUA, RoundingMode.HALF_UP);
  }
  
  public void setPonua(Double pPonua) {
    if (pPonua == null) {
      return;
    }
    ponua = BigDecimal.valueOf(pPonua).setScale(DECIMAL_PONUA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPonua() {
    return ponua.setScale(DECIMAL_PONUA, RoundingMode.HALF_UP);
  }
  
  public void setPokac(BigDecimal pPokac) {
    if (pPokac == null) {
      return;
    }
    pokac = pPokac.setScale(DECIMAL_POKAC, RoundingMode.HALF_UP);
  }
  
  public void setPokac(Double pPokac) {
    if (pPokac == null) {
      return;
    }
    pokac = BigDecimal.valueOf(pPokac).setScale(DECIMAL_POKAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPokac() {
    return pokac.setScale(DECIMAL_POKAC, RoundingMode.HALF_UP);
  }
  
  public void setPodel(BigDecimal pPodel) {
    if (pPodel == null) {
      return;
    }
    podel = pPodel.setScale(DECIMAL_PODEL, RoundingMode.HALF_UP);
  }
  
  public void setPodel(Integer pPodel) {
    if (pPodel == null) {
      return;
    }
    podel = BigDecimal.valueOf(pPodel);
  }
  
  public Integer getPodel() {
    return podel.intValue();
  }
  
  public void setPoref(String pPoref) {
    if (pPoref == null) {
      return;
    }
    poref = pPoref;
  }
  
  public String getPoref() {
    return poref;
  }
  
  public void setPounc(String pPounc) {
    if (pPounc == null) {
      return;
    }
    pounc = pPounc;
  }
  
  public String getPounc() {
    return pounc;
  }
  
  public void setPoksc(BigDecimal pPoksc) {
    if (pPoksc == null) {
      return;
    }
    poksc = pPoksc.setScale(DECIMAL_POKSC, RoundingMode.HALF_UP);
  }
  
  public void setPoksc(Double pPoksc) {
    if (pPoksc == null) {
      return;
    }
    poksc = BigDecimal.valueOf(pPoksc).setScale(DECIMAL_POKSC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoksc() {
    return poksc.setScale(DECIMAL_POKSC, RoundingMode.HALF_UP);
  }
  
  public void setPodcc(BigDecimal pPodcc) {
    if (pPodcc == null) {
      return;
    }
    podcc = pPodcc.setScale(DECIMAL_PODCC, RoundingMode.HALF_UP);
  }
  
  public void setPodcc(Integer pPodcc) {
    if (pPodcc == null) {
      return;
    }
    podcc = BigDecimal.valueOf(pPodcc);
  }
  
  public Integer getPodcc() {
    return podcc.intValue();
  }
  
  public void setPodca(BigDecimal pPodca) {
    if (pPodca == null) {
      return;
    }
    podca = pPodca.setScale(DECIMAL_PODCA, RoundingMode.HALF_UP);
  }
  
  public void setPodca(Integer pPodca) {
    if (pPodca == null) {
      return;
    }
    podca = BigDecimal.valueOf(pPodca);
  }
  
  public Integer getPodca() {
    return podca.intValue();
  }
  
  public void setPodev(String pPodev) {
    if (pPodev == null) {
      return;
    }
    podev = pPodev;
  }
  
  public String getPodev() {
    return podev;
  }
  
  public void setPoqmi(BigDecimal pPoqmi) {
    if (pPoqmi == null) {
      return;
    }
    poqmi = pPoqmi.setScale(DECIMAL_POQMI, RoundingMode.HALF_UP);
  }
  
  public void setPoqmi(Double pPoqmi) {
    if (pPoqmi == null) {
      return;
    }
    poqmi = BigDecimal.valueOf(pPoqmi).setScale(DECIMAL_POQMI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoqmi() {
    return poqmi.setScale(DECIMAL_POQMI, RoundingMode.HALF_UP);
  }
  
  public void setPoqec(BigDecimal pPoqec) {
    if (pPoqec == null) {
      return;
    }
    poqec = pPoqec.setScale(DECIMAL_POQEC, RoundingMode.HALF_UP);
  }
  
  public void setPoqec(Double pPoqec) {
    if (pPoqec == null) {
      return;
    }
    poqec = BigDecimal.valueOf(pPoqec).setScale(DECIMAL_POQEC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoqec() {
    return poqec.setScale(DECIMAL_POQEC, RoundingMode.HALF_UP);
  }
  
  public void setPokpv(BigDecimal pPokpv) {
    if (pPokpv == null) {
      return;
    }
    pokpv = pPokpv.setScale(DECIMAL_POKPV, RoundingMode.HALF_UP);
  }
  
  public void setPokpv(Double pPokpv) {
    if (pPokpv == null) {
      return;
    }
    pokpv = BigDecimal.valueOf(pPokpv).setScale(DECIMAL_POKPV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPokpv() {
    return pokpv.setScale(DECIMAL_POKPV, RoundingMode.HALF_UP);
  }
  
  public void setPoddp(BigDecimal pPoddp) {
    if (pPoddp == null) {
      return;
    }
    poddp = pPoddp.setScale(DECIMAL_PODDP, RoundingMode.HALF_UP);
  }
  
  public void setPoddp(Integer pPoddp) {
    if (pPoddp == null) {
      return;
    }
    poddp = BigDecimal.valueOf(pPoddp);
  }
  
  public void setPoddp(Date pPoddp) {
    if (pPoddp == null) {
      return;
    }
    poddp = BigDecimal.valueOf(ConvertDate.dateToDb2(pPoddp));
  }
  
  public Integer getPoddp() {
    return poddp.intValue();
  }
  
  public Date getPoddpConvertiEnDate() {
    return ConvertDate.db2ToDate(poddp.intValue(), null);
  }
  
  public void setPorga(String pPorga) {
    if (pPorga == null) {
      return;
    }
    porga = pPorga;
  }
  
  public String getPorga() {
    return porga;
  }
  
  public void setPodels(BigDecimal pPodels) {
    if (pPodels == null) {
      return;
    }
    podels = pPodels.setScale(DECIMAL_PODELS, RoundingMode.HALF_UP);
  }
  
  public void setPodels(Integer pPodels) {
    if (pPodels == null) {
      return;
    }
    podels = BigDecimal.valueOf(pPodels);
  }
  
  public Integer getPodels() {
    return podels.intValue();
  }
  
  public void setPokpr(BigDecimal pPokpr) {
    if (pPokpr == null) {
      return;
    }
    pokpr = pPokpr.setScale(DECIMAL_POKPR, RoundingMode.HALF_UP);
  }
  
  public void setPokpr(Double pPokpr) {
    if (pPokpr == null) {
      return;
    }
    pokpr = BigDecimal.valueOf(pPokpr).setScale(DECIMAL_POKPR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPokpr() {
    return pokpr.setScale(DECIMAL_POKPR, RoundingMode.HALF_UP);
  }
  
  public void setPoin1(Character pPoin1) {
    if (pPoin1 == null) {
      return;
    }
    poin1 = String.valueOf(pPoin1);
  }
  
  public Character getPoin1() {
    return poin1.charAt(0);
  }
  
  public void setPoin2(Character pPoin2) {
    if (pPoin2 == null) {
      return;
    }
    poin2 = String.valueOf(pPoin2);
  }
  
  public Character getPoin2() {
    return poin2.charAt(0);
  }
  
  public void setPoin3(Character pPoin3) {
    if (pPoin3 == null) {
      return;
    }
    poin3 = String.valueOf(pPoin3);
  }
  
  public Character getPoin3() {
    return poin3.charAt(0);
  }
  
  public void setPoin4(Character pPoin4) {
    if (pPoin4 == null) {
      return;
    }
    poin4 = String.valueOf(pPoin4);
  }
  
  public Character getPoin4() {
    return poin4.charAt(0);
  }
  
  public void setPoin5(Character pPoin5) {
    if (pPoin5 == null) {
      return;
    }
    poin5 = String.valueOf(pPoin5);
  }
  
  public Character getPoin5() {
    return poin5.charAt(0);
  }
  
  public void setPotrl(Character pPotrl) {
    if (pPotrl == null) {
      return;
    }
    potrl = String.valueOf(pPotrl);
  }
  
  public Character getPotrl() {
    return potrl.charAt(0);
  }
  
  public void setPorfc(String pPorfc) {
    if (pPorfc == null) {
      return;
    }
    porfc = pPorfc;
  }
  
  public String getPorfc() {
    return porfc;
  }
  
  public void setPogcd(BigDecimal pPogcd) {
    if (pPogcd == null) {
      return;
    }
    pogcd = pPogcd.setScale(DECIMAL_POGCD, RoundingMode.HALF_UP);
  }
  
  public void setPogcd(Integer pPogcd) {
    if (pPogcd == null) {
      return;
    }
    pogcd = BigDecimal.valueOf(pPogcd);
  }
  
  public Integer getPogcd() {
    return pogcd.intValue();
  }
  
  public void setPodaf(BigDecimal pPodaf) {
    if (pPodaf == null) {
      return;
    }
    podaf = pPodaf.setScale(DECIMAL_PODAF, RoundingMode.HALF_UP);
  }
  
  public void setPodaf(Integer pPodaf) {
    if (pPodaf == null) {
      return;
    }
    podaf = BigDecimal.valueOf(pPodaf);
  }
  
  public void setPodaf(Date pPodaf) {
    if (pPodaf == null) {
      return;
    }
    podaf = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodaf));
  }
  
  public Integer getPodaf() {
    return podaf.intValue();
  }
  
  public Date getPodafConvertiEnDate() {
    return ConvertDate.db2ToDate(podaf.intValue(), null);
  }
  
  public void setPolib(String pPolib) {
    if (pPolib == null) {
      return;
    }
    polib = pPolib;
  }
  
  public String getPolib() {
    return polib;
  }
  
  public void setPoopa(String pPoopa) {
    if (pPoopa == null) {
      return;
    }
    poopa = pPoopa;
  }
  
  public String getPoopa() {
    return poopa;
  }
  
  public void setPoin6(Character pPoin6) {
    if (pPoin6 == null) {
      return;
    }
    poin6 = String.valueOf(pPoin6);
  }
  
  public Character getPoin6() {
    return poin6.charAt(0);
  }
  
  public void setPoin7(Character pPoin7) {
    if (pPoin7 == null) {
      return;
    }
    poin7 = String.valueOf(pPoin7);
  }
  
  public Character getPoin7() {
    return poin7.charAt(0);
  }
  
  public void setPoin8(Character pPoin8) {
    if (pPoin8 == null) {
      return;
    }
    poin8 = String.valueOf(pPoin8);
  }
  
  public Character getPoin8() {
    return poin8.charAt(0);
  }
  
  public void setPoin9(Character pPoin9) {
    if (pPoin9 == null) {
      return;
    }
    poin9 = String.valueOf(pPoin9);
  }
  
  public Character getPoin9() {
    return poin9.charAt(0);
  }
  
  public void setPopdi(BigDecimal pPopdi) {
    if (pPopdi == null) {
      return;
    }
    popdi = pPopdi.setScale(DECIMAL_POPDI, RoundingMode.HALF_UP);
  }
  
  public void setPopdi(Double pPopdi) {
    if (pPopdi == null) {
      return;
    }
    popdi = BigDecimal.valueOf(pPopdi).setScale(DECIMAL_POPDI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPopdi() {
    return popdi.setScale(DECIMAL_POPDI, RoundingMode.HALF_UP);
  }
  
  public void setPokap(BigDecimal pPokap) {
    if (pPokap == null) {
      return;
    }
    pokap = pPokap.setScale(DECIMAL_POKAP, RoundingMode.HALF_UP);
  }
  
  public void setPokap(Double pPokap) {
    if (pPokap == null) {
      return;
    }
    pokap = BigDecimal.valueOf(pPokap).setScale(DECIMAL_POKAP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPokap() {
    return pokap.setScale(DECIMAL_POKAP, RoundingMode.HALF_UP);
  }
  
  public void setPoprs(BigDecimal pPoprs) {
    if (pPoprs == null) {
      return;
    }
    poprs = pPoprs.setScale(DECIMAL_POPRS, RoundingMode.HALF_UP);
  }
  
  public void setPoprs(Double pPoprs) {
    if (pPoprs == null) {
      return;
    }
    poprs = BigDecimal.valueOf(pPoprs).setScale(DECIMAL_POPRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoprs() {
    return poprs.setScale(DECIMAL_POPRS, RoundingMode.HALF_UP);
  }
  
  public void setPopgn(Character pPopgn) {
    if (pPopgn == null) {
      return;
    }
    popgn = String.valueOf(pPopgn);
  }
  
  public Character getPopgn() {
    return popgn.charAt(0);
  }
  
  public void setPofv1(BigDecimal pPofv1) {
    if (pPofv1 == null) {
      return;
    }
    pofv1 = pPofv1.setScale(DECIMAL_POFV1, RoundingMode.HALF_UP);
  }
  
  public void setPofv1(Double pPofv1) {
    if (pPofv1 == null) {
      return;
    }
    pofv1 = BigDecimal.valueOf(pPofv1).setScale(DECIMAL_POFV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPofv1() {
    return pofv1.setScale(DECIMAL_POFV1, RoundingMode.HALF_UP);
  }
  
  public void setPofk1(BigDecimal pPofk1) {
    if (pPofk1 == null) {
      return;
    }
    pofk1 = pPofk1.setScale(DECIMAL_POFK1, RoundingMode.HALF_UP);
  }
  
  public void setPofk1(Double pPofk1) {
    if (pPofk1 == null) {
      return;
    }
    pofk1 = BigDecimal.valueOf(pPofk1).setScale(DECIMAL_POFK1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPofk1() {
    return pofk1.setScale(DECIMAL_POFK1, RoundingMode.HALF_UP);
  }
  
  public void setPofp1(BigDecimal pPofp1) {
    if (pPofp1 == null) {
      return;
    }
    pofp1 = pPofp1.setScale(DECIMAL_POFP1, RoundingMode.HALF_UP);
  }
  
  public void setPofp1(Double pPofp1) {
    if (pPofp1 == null) {
      return;
    }
    pofp1 = BigDecimal.valueOf(pPofp1).setScale(DECIMAL_POFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPofp1() {
    return pofp1.setScale(DECIMAL_POFP1, RoundingMode.HALF_UP);
  }
  
  public void setPofu1(Character pPofu1) {
    if (pPofu1 == null) {
      return;
    }
    pofu1 = String.valueOf(pPofu1);
  }
  
  public Character getPofu1() {
    return pofu1.charAt(0);
  }
  
  public void setPofv2(BigDecimal pPofv2) {
    if (pPofv2 == null) {
      return;
    }
    pofv2 = pPofv2.setScale(DECIMAL_POFV2, RoundingMode.HALF_UP);
  }
  
  public void setPofv2(Double pPofv2) {
    if (pPofv2 == null) {
      return;
    }
    pofv2 = BigDecimal.valueOf(pPofv2).setScale(DECIMAL_POFV2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPofv2() {
    return pofv2.setScale(DECIMAL_POFV2, RoundingMode.HALF_UP);
  }
  
  public void setPofk2(BigDecimal pPofk2) {
    if (pPofk2 == null) {
      return;
    }
    pofk2 = pPofk2.setScale(DECIMAL_POFK2, RoundingMode.HALF_UP);
  }
  
  public void setPofk2(Double pPofk2) {
    if (pPofk2 == null) {
      return;
    }
    pofk2 = BigDecimal.valueOf(pPofk2).setScale(DECIMAL_POFK2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPofk2() {
    return pofk2.setScale(DECIMAL_POFK2, RoundingMode.HALF_UP);
  }
  
  public void setPofp2(BigDecimal pPofp2) {
    if (pPofp2 == null) {
      return;
    }
    pofp2 = pPofp2.setScale(DECIMAL_POFP2, RoundingMode.HALF_UP);
  }
  
  public void setPofp2(Double pPofp2) {
    if (pPofp2 == null) {
      return;
    }
    pofp2 = BigDecimal.valueOf(pPofp2).setScale(DECIMAL_POFP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPofp2() {
    return pofp2.setScale(DECIMAL_POFP2, RoundingMode.HALF_UP);
  }
  
  public void setPofu2(Character pPofu2) {
    if (pPofu2 == null) {
      return;
    }
    pofu2 = String.valueOf(pPofu2);
  }
  
  public Character getPofu2() {
    return pofu2.charAt(0);
  }
  
  public void setPofv3(BigDecimal pPofv3) {
    if (pPofv3 == null) {
      return;
    }
    pofv3 = pPofv3.setScale(DECIMAL_POFV3, RoundingMode.HALF_UP);
  }
  
  public void setPofv3(Double pPofv3) {
    if (pPofv3 == null) {
      return;
    }
    pofv3 = BigDecimal.valueOf(pPofv3).setScale(DECIMAL_POFV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPofv3() {
    return pofv3.setScale(DECIMAL_POFV3, RoundingMode.HALF_UP);
  }
  
  public void setPofk3(BigDecimal pPofk3) {
    if (pPofk3 == null) {
      return;
    }
    pofk3 = pPofk3.setScale(DECIMAL_POFK3, RoundingMode.HALF_UP);
  }
  
  public void setPofk3(Double pPofk3) {
    if (pPofk3 == null) {
      return;
    }
    pofk3 = BigDecimal.valueOf(pPofk3).setScale(DECIMAL_POFK3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPofk3() {
    return pofk3.setScale(DECIMAL_POFK3, RoundingMode.HALF_UP);
  }
  
  public void setPofu3(Character pPofu3) {
    if (pPofu3 == null) {
      return;
    }
    pofu3 = String.valueOf(pPofu3);
  }
  
  public Character getPofu3() {
    return pofu3.charAt(0);
  }
  
  public void setPopr1(BigDecimal pPopr1) {
    if (pPopr1 == null) {
      return;
    }
    popr1 = pPopr1.setScale(DECIMAL_POPR1, RoundingMode.HALF_UP);
  }
  
  public void setPopr1(Double pPopr1) {
    if (pPopr1 == null) {
      return;
    }
    popr1 = BigDecimal.valueOf(pPopr1).setScale(DECIMAL_POPR1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPopr1() {
    return popr1.setScale(DECIMAL_POPR1, RoundingMode.HALF_UP);
  }
  
  public void setPopr2(BigDecimal pPopr2) {
    if (pPopr2 == null) {
      return;
    }
    popr2 = pPopr2.setScale(DECIMAL_POPR2, RoundingMode.HALF_UP);
  }
  
  public void setPopr2(Double pPopr2) {
    if (pPopr2 == null) {
      return;
    }
    popr2 = BigDecimal.valueOf(pPopr2).setScale(DECIMAL_POPR2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPopr2() {
    return popr2.setScale(DECIMAL_POPR2, RoundingMode.HALF_UP);
  }
  
  public void setPodat1(BigDecimal pPodat1) {
    if (pPodat1 == null) {
      return;
    }
    podat1 = pPodat1.setScale(DECIMAL_PODAT1, RoundingMode.HALF_UP);
  }
  
  public void setPodat1(Integer pPodat1) {
    if (pPodat1 == null) {
      return;
    }
    podat1 = BigDecimal.valueOf(pPodat1);
  }
  
  public void setPodat1(Date pPodat1) {
    if (pPodat1 == null) {
      return;
    }
    podat1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodat1));
  }
  
  public Integer getPodat1() {
    return podat1.intValue();
  }
  
  public Date getPodat1ConvertiEnDate() {
    return ConvertDate.db2ToDate(podat1.intValue(), null);
  }
  
  public void setPodat2(BigDecimal pPodat2) {
    if (pPodat2 == null) {
      return;
    }
    podat2 = pPodat2.setScale(DECIMAL_PODAT2, RoundingMode.HALF_UP);
  }
  
  public void setPodat2(Integer pPodat2) {
    if (pPodat2 == null) {
      return;
    }
    podat2 = BigDecimal.valueOf(pPodat2);
  }
  
  public void setPodat2(Date pPodat2) {
    if (pPodat2 == null) {
      return;
    }
    podat2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodat2));
  }
  
  public Integer getPodat2() {
    return podat2.intValue();
  }
  
  public Date getPodat2ConvertiEnDate() {
    return ConvertDate.db2ToDate(podat2.intValue(), null);
  }
  
  public void setPoprnet(BigDecimal pPoprnet) {
    if (pPoprnet == null) {
      return;
    }
    poprnet = pPoprnet.setScale(DECIMAL_POPRNET, RoundingMode.HALF_UP);
  }
  
  public void setPoprnet(Double pPoprnet) {
    if (pPoprnet == null) {
      return;
    }
    poprnet = BigDecimal.valueOf(pPoprnet).setScale(DECIMAL_POPRNET, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoprnet() {
    return poprnet.setScale(DECIMAL_POPRNET, RoundingMode.HALF_UP);
  }
  
  public void setPoprprf(BigDecimal pPoprprf) {
    if (pPoprprf == null) {
      return;
    }
    poprprf = pPoprprf.setScale(DECIMAL_POPRPRF, RoundingMode.HALF_UP);
  }
  
  public void setPoprprf(Double pPoprprf) {
    if (pPoprprf == null) {
      return;
    }
    poprprf = BigDecimal.valueOf(pPoprprf).setScale(DECIMAL_POPRPRF, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoprprf() {
    return poprprf.setScale(DECIMAL_POPRPRF, RoundingMode.HALF_UP);
  }
  
  public void setPoprprs(BigDecimal pPoprprs) {
    if (pPoprprs == null) {
      return;
    }
    poprprs = pPoprprs.setScale(DECIMAL_POPRPRS, RoundingMode.HALF_UP);
  }
  
  public void setPoprprs(Double pPoprprs) {
    if (pPoprprs == null) {
      return;
    }
    poprprs = BigDecimal.valueOf(pPoprprs).setScale(DECIMAL_POPRPRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoprprs() {
    return poprprs.setScale(DECIMAL_POPRPRS, RoundingMode.HALF_UP);
  }
  
  public void setPoerr1(Character pPoerr1) {
    if (pPoerr1 == null) {
      return;
    }
    poerr1 = String.valueOf(pPoerr1);
  }
  
  public Character getPoerr1() {
    return poerr1.charAt(0);
  }
  
  public void setPoerr2(Character pPoerr2) {
    if (pPoerr2 == null) {
      return;
    }
    poerr2 = String.valueOf(pPoerr2);
  }
  
  public Character getPoerr2() {
    return poerr2.charAt(0);
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
