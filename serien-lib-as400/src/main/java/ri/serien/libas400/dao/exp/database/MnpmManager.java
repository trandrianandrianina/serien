/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class MnpmManager extends QueryManager {
  /**
   * Constructeur
   * @param database
   */
  public MnpmManager(Connection database) {
    super(database);
  }
  
  /**
   * Retourne la liste des modules (pour les menus)
   * @param bibenv
   * @param prf
   * @return
   * 
   */
  public ArrayList<GenericRecord> getRecordForMenu(boolean isSerieN) {
    if (isSerieN) {
      return select("Select * from mnp1sgm where mnpp0 = '01' and mnpp1 <> '' and mnpp1 < '30' and mnpord <> 01268 and mnpp2 = ''");
    }
    // On sélectionne tous les groupes sauf LTM (01268)
    try {
      return select("Select * from mnp1 where mnpp0 = '01' and mnpp1 <> '' and mnpp1 < '30' and mnpord <> 01268 and mnpp2 = ''");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
  }
  
}
