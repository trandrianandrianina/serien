/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.dataarea.LdaSerien;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.EncoursClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.alertes.AlertesEncoursClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.IdModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.transporteur.IdTransporteur;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.IdTypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.IdTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesDocument;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatBonDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatDevisDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatExtractionDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.OptionsEditionVente;
import ri.serien.libcommun.gescom.vente.document.OptionsValidation;
import ri.serien.libcommun.gescom.vente.reglement.BanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.EnumTypeReglement;
import ri.serien.libcommun.gescom.vente.reglement.IdBanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.IdReglement;
import ri.serien.libcommun.gescom.vente.reglement.IdReglementAcompte;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.gescom.vente.reglement.Reglement;
import ri.serien.libcommun.gescom.vente.reglement.ReglementAcompte;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

/**
 * Centralise les accès aux programmes RPG permettant de manipuler les documents de ventes.
 * Les lignes de doucments de ventes sont traitées dans la classe RpgLigneDocumentVente.
 */
public class RpgDocumentVente extends RpgBase {
  // Constantes
  private static final String SVGVM0003_CONTROLER_FIN_DOCUMENT_VENTE = "SVGVM0003";
  private static final String SVGVM0006_CONTROLER_DOCUMENT_VENTE = "SVGVM0006";
  private static final String SVGVM0008_CHARGER_LISTE_REGLEMENT_DOCUMENT_VENTE = "SVGVM0008";
  private static final String SVGVM0010_CREER_DOCUMENT_VENTE = "SVGVM0010";
  // private static final String SVGVM0016_CHARGER_LISTE_DOCUMENT_VENTE = "SVGVM0016";
  private static final String SVGVM0017_MODIFIER_VALIDATION_DOCUMENT_VENTE = "SVGVM0017";
  private static final String SVGVM0018_EDITER_DOCUMENT_VENTE = "SVGVM0018";
  private static final String SVGVM0019_MODIFIER_CHIFFRAGE_DOCUMENT_VENTE = "SVGVM0019";
  private static final String SVGVM0021_CHARGER_ENTETE_DOCUMENT_VENTE = "SVGVM0021";
  private static final String SVGVM0024_SAUVER_ENTETE_DOCUMENT_VENTE = "SVGVM0024";
  private static final String SVGVM0025_SAUVER_ADRESSE_DOCUMENT_VENTE = "SVGVM0025";
  private static final String SVGVM0026_CONTROLER_ENCOURS_DOCUMENT_VENTE = "SVGVM0026";
  private static final String SVGVM0029_CHARGER_LISTE_ADRESSE_DOCUMENT_VENTE = "SVGVM0029";
  private static final String SVGVM0030_ANNULER_DOCUMENT_VENTE = "SVGVM0030";
  private static final String SVGVM0031_CREER_DOCUMENT_VENTE_PAR_DUPLICATION = "SVGVM0031";
  private static final String SVGVM0036_MODIFIER_CLIENT_DOCUMENT_VENTE = "SVGVM0036";
  private static final String SVGVM0041_SAUVER_REGLEMENT_MULTIPLE_DOCUMENT_VENTE = "SVGVM0041";
  private static final String SVGVM0042_SAUVER_LISTE_REGLEMENT_DOCUMENT_VENTE = "SVGVM0042";
  // private static final String SVGVM0046_CONSERVER_DOCUMENT_VENTE_ORIGINE = "SVGVM0046";
  private static final String SVGVM0048_VALIDER_EXTRACTION_DOCUMENT_VENTE = "SVGVM0048";
  private static final String SVGVM0061_CHANGER_CLIENT_DOCUMENT_VENTE = "SVGVM0061";
  private static final String SVGVM0063_SUPPRIMER_REGLEMENT_DOCUMENT_VENTE = "SVGVM0063";
  private static final String SVGVM0064_SOLDER_RELIQUAT_DOCUMENT_VENTE = "SVGVM0064";
  
  private static final int CHANGER_CLIENT_FACTURE = 0;
  private static final int CHANGER_CLIENT_LIVRE = 1;
  
  // Duplique un document sans changer son état (par exemple un devis en devis)
  public static final int DUPLICATION_DOCUMENT = 1;
  // Duplique un document en faisant évoluer son état (par exemple devis en commande ou commande en bon ou bon en facture)
  public static final int DUPLICATION_DOCUMENT_AVEC_ETAT__SUIVANT = 2;
  
  /**
   * Appeler le programme RPG qui charge l'entête d'un document de ventes.
   */
  public DocumentVente chargerEnteteDocumentVente(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente) {
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de charger le document de ventes car les paramètres sont invaldes.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0021i entree = new Svgvm0021i();
    Svgvm0021o sortie = new Svgvm0021o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0021_CHARGER_ENTETE_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors du  chargement de l'entête du document de ventes : " + pIdDocumentVente);
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Initialisation des indicateurs
    String indicateurs = "";
    if (sortie.getPoind() != null) {
      indicateurs = sortie.getPoind();
    }
    
    // Récupérer l'identifiant de l'établissement
    IdEtablissement idEtablissement = pIdDocumentVente.getIdEtablissement();
    
    // Créer le document de ventes
    DocumentVente documentVente = new DocumentVente(pIdDocumentVente);
    
    // Initialise si le document est modifiable ou non
    if (indicateurs.length() >= 1 && indicateurs.charAt(0) == '1') {
      documentVente.setModifiable(false);
    }
    
    Date dateCreation = ConvertDate.db2ToDate(sortie.getE1cre().intValue(), null);
    int heureCreation = sortie.getE1hre().intValue();
    if (heureCreation > 0) {
      Calendar cal = Calendar.getInstance();
      cal.setTime(dateCreation);
      cal.set(Calendar.HOUR_OF_DAY, heureCreation / 100);
      cal.set(Calendar.MINUTE, heureCreation % 100);
      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MILLISECOND, 0);
      dateCreation = cal.getTime();
    }
    documentVente.setDateCreation(dateCreation);
    
    documentVente.setDateValidationCommande(ConvertDate.db2ToDate(sortie.getE1hom().intValue(), null));
    documentVente.setDateExpeditionBon(ConvertDate.db2ToDate(sortie.getE1exp().intValue(), null));
    documentVente.setDateFacturation(ConvertDate.db2ToDate(sortie.getE1fac().intValue(), null));
    documentVente.setNumeroFacture(sortie.getE1nfa().intValue());
    documentVente.setNombreLignes(sortie.getE1nlig().intValue());
    documentVente.setEtat(EnumEtatBonDocumentVente.valueOfByCode(sortie.getE1eta().intValue()));
    documentVente.setEtatDevis(EnumEtatDevisDocumentVente.valueOfByCode(sortie.getE1tdv().intValue()));
    documentVente.setTopEdition(sortie.getE1edt().intValue());
    documentVente.setTopClientLivre(sortie.getE1tcl().intValue());
    documentVente.setTopClientFacture(sortie.getE1tcf().intValue());
    documentVente.setNumeroTVA1(sortie.getE1tva1().intValue());
    documentVente.setNumeroTVA2(sortie.getE1tva2().intValue());
    documentVente.setNumeroTVA3(sortie.getE1tva3().intValue());
    documentVente.setTopTaxeParafiscale(sortie.getE1tpf().intValue());
    documentVente.setTopReleve(sortie.getE1rlv().intValue());
    documentVente.setNombreExemplairesFactures(sortie.getE1nex().intValue());
    documentVente.setNumeroTarif(sortie.getE1tar().intValue());
    documentVente.setTopRegroupementBonFacture(sortie.getE1rbf().intValue());
    documentVente.setSigne(sortie.getE1sgn().intValue());
    documentVente.setMontantSoumisTVA1(sortie.getE1sl1());
    documentVente.setMontantSoumisTVA2(sortie.getE1sl2());
    documentVente.setMontantSoumisTVA3(sortie.getE1sl3());
    documentVente.setTotalHT(sortie.getE1thtl());
    documentVente.setMontantTVA1(sortie.getE1tl1());
    documentVente.setMontantTVA2(sortie.getE1tl2());
    documentVente.setMontantTVA3(sortie.getE1tl3());
    documentVente.setMontantSoumisTaxeParafiscale(sortie.getE1stp());
    documentVente.setMontantTaxeParafiscale(sortie.getE1mtp());
    documentVente.setMontantEscompte(sortie.getE1mes());
    documentVente.setTotalTTC(sortie.getE1ttc());
    documentVente.setTotalHTPrixVenteBase(sortie.getE1tpvl());
    documentVente.setTotalHTPrixRevient(sortie.getE1tprl());
    documentVente.setTopsTraitementSysteme(sortie.getE1to1g().intValue());
    documentVente.setBaseEscompte1(sortie.getE1se1());
    documentVente.setBaseEscompte2(sortie.getE1se2());
    documentVente.setBaseEscompte3(sortie.getE1se3());
    documentVente.setDateAffectation(ConvertDate.db2ToDate(sortie.getE1aff().intValue(), null));
    documentVente.setArrondiTTCEnCentimes(sortie.getE1arr().intValue());
    documentVente.setMontantCommandeInitiale(sortie.getE1mci());
    documentVente.setNumeroCompteAuxiliaireClient(sortie.getE1clcg().intValue());
    documentVente.setTypeGenerationCommandeAchat(sortie.getE1gba().intValue());
    documentVente.setDateCommande(ConvertDate.db2ToDate(sortie.getE1dco().intValue(), null));
    documentVente.getTransport().setDateLivraisonPrevue(ConvertDate.db2ToDate(sortie.getE1dlp().intValue(), null));
    // Correspond à la date de début du chantier
    documentVente.getTransport().setDateLivraisonSouhaitee(ConvertDate.db2ToDate(sortie.getE1dls().intValue(), null));
    // Correspond à la date de fin du chantier
    documentVente.setDateValiditeDevis(ConvertDate.db2ToDate(sortie.getE1dat2().intValue(), null));
    // Date de relance pour les devis
    documentVente.setDateRelanceDevis(ConvertDate.db2ToDate(sortie.getPodat3().intValue(), null));
    // L'identifiant client de la centrale à laquelle est lié le client du document de vente
    if (sortie.getE1cc1().intValue() != 0) {
      IdClient idClientCentrale = IdClient.getInstance(idEtablissement, sortie.getE1cc1().intValue(), 0);
      documentVente.setIdClientCentrale(idClientCentrale);
    }
    else {
      documentVente.setIdClientCentrale(null);
    }
    documentVente.setPourcentageEscompte(sortie.getE1esc());
    // C'est stocké en 5 dont 2 dans la table mais le programme RPG le redéfini en 5 dont 3 (donc on multiplie par 10)
    documentVente.setTauxTVA1(sortie.getE1tv1().divide(BigDecimal.TEN, MathContext.DECIMAL32));
    documentVente.setTauxTVA2(sortie.getE1tv2().divide(BigDecimal.TEN, MathContext.DECIMAL32));
    documentVente.setTauxTVA3(sortie.getE1tv3().divide(BigDecimal.TEN, MathContext.DECIMAL32));
    documentVente.setTauxTaxeParafiscale(sortie.getE1ttp());
    documentVente.setPourcentageCommissionnementRep1(sortie.getE1comg());
    documentVente.setPourcentageCommissionnementRep2(sortie.getE1co2g());
    IdClient idClientLivre = IdClient.getInstance(idEtablissement, sortie.getE1cllp().intValue(), sortie.getE1clls().intValue());
    documentVente.setIdClientLivre(idClientLivre);
    IdClient idClientFacture = IdClient.getInstance(idEtablissement, sortie.getE1clfp().intValue(), sortie.getE1clfs().intValue());
    documentVente.setIdClientFacture(idClientFacture);
    if (!sortie.getE1tfa().toString().trim().isEmpty()) {
      documentVente.setIdTypeFacturation(IdTypeFacturation.getInstance(idEtablissement, sortie.getE1tfa().toString()));
    }
    documentVente.setDelaiPreparation(sortie.getE1dpr().intValue());
    documentVente.setNombreColis(sortie.getE1col().intValue());
    documentVente.setPoidsKg(sortie.getE1pds());
    documentVente.setBaseDevise(sortie.getE1bas().intValue());
    documentVente.setTauxChange(sortie.getE1chg());
    documentVente.setReductionBaseCommissionRep1(sortie.getE1rbc());
    documentVente.setReductionBaseCommissionRep2(sortie.getE1rbc2());
    documentVente.setDateMiniFacturation(ConvertDate.db2ToDate(sortie.getE1dat1().intValue(), null));
    // aEnteteDocument.set (sortie.getE1dat2
    // aEnteteDocument.set (sortie.getE1dat3
    documentVente.setDateDernierTraitementDevis(ConvertDate.db2ToDate(sortie.getE1dat4().intValue(), null));
    documentVente.setPourcentageRemise1Ligne(sortie.getE1rem1());
    documentVente.setPourcentageRemise2Ligne(sortie.getE1rem2());
    documentVente.setPourcentageRemise3Ligne(sortie.getE1rem3());
    documentVente.setPourcentageRemise4Ligne(sortie.getE1rem4());
    documentVente.setPourcentageRemise5Ligne(sortie.getE1rem5());
    documentVente.setPourcentageRemise6Ligne(sortie.getE1rem6());
    documentVente.setPourcentageRemise1Pied(sortie.getE1rp1());
    documentVente.setPourcentageRemise2Pied(sortie.getE1rp2());
    documentVente.setPourcentageRemise3Pied(sortie.getE1rp3());
    documentVente.setPourcentageRemise4Pied(sortie.getE1rp4());
    documentVente.setPourcentageRemise5Pied(sortie.getE1rp5());
    documentVente.setPourcentageRemise6Pied(sortie.getE1rp6());
    documentVente.setNumeroTournee(sortie.getE1nto().intValue());
    documentVente.setRangDansTournee(sortie.getE1oto().intValue());
    documentVente.setVolume(sortie.getE1vol());
    documentVente.setLongueurMax(sortie.getE1lgm());
    documentVente.setSurfacePlancher(sortie.getE1m2p());
    // aEnteteDocument.set (sortie.getE1dat5()
    // aEnteteDocument.set (sortie.getE1dat6()
    // aEnteteDocument.set (sortie.getE1dat7()
    documentVente.setReferenceCourte(sortie.getE1ncc().trim());
    if (!sortie.getE1rep().trim().isEmpty()) {
      documentVente.setIdRepresentant1(IdRepresentant.getInstance(idEtablissement, sortie.getE1rep()));
    }
    if (!sortie.getE1rep2().trim().isEmpty()) {
      documentVente.setIdRepresentant2(IdRepresentant.getInstance(idEtablissement, sortie.getE1rep2()));
    }
    documentVente.setIdMagasin(IdMagasin.getInstance(idEtablissement, sortie.getE1mag()));
    if (!sortie.getE1maga().trim().isEmpty()) {
      documentVente.setIdMagasinSortieAncien(IdMagasin.getInstance(idEtablissement, sortie.getE1maga()));
    }
    
    // Mode d'expédition
    documentVente.setIdModeExpedition(IdModeExpedition.getInstance(sortie.getE1mex()));
    
    if (sortie.getE1ctr() != null && !sortie.getE1ctr().trim().isEmpty()) {
      IdTransporteur idTransporteur = IdTransporteur.getInstance(idEtablissement, sortie.getE1ctr());
      documentVente.getTransport().setIdTransporteur(idTransporteur);
    }
    documentVente.setSectionAnalytique(sortie.getE1san());
    documentVente.setCodeActionCommerciale(sortie.getE1act());
    documentVente.setCodeDevise(sortie.getE1dev());
    documentVente.setCodeConditionVente(sortie.getE1cnv());
    documentVente.setReferenceLongue(sortie.getE1rcc().trim());
    if (!sortie.getE1vde().trim().isEmpty()) {
      IdVendeur idVendeur = IdVendeur.getInstance(idEtablissement, sortie.getE1vde());
      documentVente.setIdVendeur(idVendeur);
    }
    documentVente.setTransportACalculer(sortie.getE1trc().toString());
    if (sortie.getE1ztr() != null && !sortie.getE1ztr().trim().isEmpty()) {
      IdZoneGeographique idZoneGeographique = IdZoneGeographique.getInstance(idEtablissement, sortie.getE1ztr());
      documentVente.getTransport().setIdZoneGeographique(idZoneGeographique);
    }
    documentVente.setTopEditionBonPreparation(sortie.getE1ebp().toString());
    documentVente.setTypeCommissionnementRep(sortie.getE1trp().toString());
    documentVente.setTopBonNonConfirmable(sortie.getE1nho().toString());
    documentVente.setTopPersonnalisable1(sortie.getE1tp1());
    documentVente.setTopPersonnalisable2(sortie.getE1tp2());
    documentVente.setTopPersonnalisable3(sortie.getE1tp3());
    documentVente.setTopPersonnalisable4(sortie.getE1tp4());
    documentVente.setTopPersonnalisable5(sortie.getE1tp5());
    documentVente.setImmatriculationVehiculeClient(sortie.getE1veh().trim());
    documentVente.setTopNePasFacturer(sortie.getE1in1().toString());
    documentVente.setTopIndicateurTournee(sortie.getE1in2().toString());
    documentVente.setTopTraiteEditee(sortie.getE1in3().toString());
    documentVente.setTopRemisesSpeciales(sortie.getE1in4().toString());
    documentVente.setTopExistenceRemisePied(sortie.getE1in5().toString());
    documentVente.setTopFacturecomptoir(sortie.getE1in6().toString());
    documentVente.setCodePreparationCommande(sortie.getE1in7().toString());
    documentVente.getTransport().setTopEditionBordereau(sortie.getE1in8().toString());
    documentVente.setBlocageExpedition(sortie.getE1in9());
    documentVente.setCodeContrat(sortie.getE1cct());
    documentVente.setTypeRemise(sortie.getE1trl().toString());
    documentVente.setBaseRemise(sortie.getE1brl().toString());
    documentVente.setTypeRemisePied(sortie.getE1tre().toString());
    documentVente.setTopModeMesure(sortie.getE1in11().toString());
    documentVente.setTopExtractionForceeExpedition(sortie.getE1in12().toString());
    documentVente.setCodeCanalVente(sortie.getE1can());
    documentVente.getTransport().setCodeReroupement(sortie.getE1crt());
    documentVente.setCodePreparateur(sortie.getE1pre());
    documentVente.setCodePlateformeChargement(sortie.getE1pfc());
    documentVente.setTopTypeVente(sortie.getE1in17().toString());
    
    documentVente.setTTC(sortie.getE1nat().equals('T'));
    
    // Mode d'expédition
    documentVente.setIdModeExpedition(IdModeExpedition.getInstance(sortie.getE1mex()));
    
    documentVente.setDirectUsine(sortie.getPodir().equals('1'));
    documentVente.setGenerationImmediateCommandeAchat(sortie.getPoimd().equals('1'));
    
    // Renseigner l'identifiant du chantier
    if (sortie.getPochan().intValue() != 0) {
      documentVente.setIdChantier(IdChantier.getInstance(idEtablissement, sortie.getPochan().intValue()));
    }
    
    // On modifie le type du document uniquement s'il est indéfini dans le cas contraire on ne fait rien car cette information n'est pas
    // stockée en base (on peut la déduire à partir où un document a été validé mais pas en cours de création comme par exemple lors
    // d'une
    // extraction)
    documentVente.deduireTypeDocumentVente();
    
    // On extrait le nom du contact (qui va prendre la marchandise) de la ligne commentaire
    sortie.setPoenlv(Constantes.normerTexte(sortie.getPoenlv()));
    if (!sortie.getPoenlv().isEmpty()) {
      int pos = sortie.getPoenlv().lastIndexOf(DocumentVente.MARQUEUR_ARTICLE_COMMENTAIRE_PRIS_PAR);
      if (pos > -1) {
        documentVente.setPrisPar(sortie.getPoenlv().substring(pos + DocumentVente.MARQUEUR_ARTICLE_COMMENTAIRE_PRIS_PAR.length()).trim());
      }
      else {
        documentVente.setPrisPar(sortie.getPoenlv());
      }
    }
    
    // Pour les documents direct usine multi-fournisseur
    if (sortie.getPofrs().intValue() > 0) {
      IdFournisseur idFournisseur = IdFournisseur.getInstance(documentVente.getId().getIdEtablissement(), sortie.getPocol().intValue(),
          sortie.getPofrs().intValue());
      IdAdresseFournisseur idAdresseFournisseur = IdAdresseFournisseur.getInstance(idFournisseur, sortie.getPofre().intValue());
      documentVente.setIdAdresseFournisseur(idAdresseFournisseur);
      documentVente.setMonoFournisseur(true);
    }
    else {
      documentVente.setIdAdresseFournisseur(null);
      documentVente.setMonoFournisseur(false);
      if (documentVente.isEnCoursCreation()) {
        documentVente.setMonoFournisseur(true);
      }
    }
    
    // Etat de l'extraction
    EnumEtatExtractionDocumentVente etatExtraction = EnumEtatExtractionDocumentVente.valueOfByCode(sortie.getPoinex().intValue());
    documentVente.setEtatExtraction(etatExtraction);
    
    // Document d'achat lié
    if (sortie.getPoban().intValue() > 0) {
      EnumCodeEnteteDocumentAchat codeEnteteAchat = null;
      if (sortie.getPobae().intValue() == 1) {
        codeEnteteAchat = EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION;
      }
      else {
        codeEnteteAchat = EnumCodeEnteteDocumentAchat.FACTURE;
      }
      IdDocumentAchat documentAchatLie = IdDocumentAchat.getInstance(documentVente.getId().getIdEtablissement(), codeEnteteAchat,
          sortie.getPoban().intValue(), sortie.getPobas().intValue());
      documentVente.setIdDocumentAchatLie(documentAchatLie);
    }
    
    return documentVente;
  }
  
  /**
   * Appeler le programme RPG qui charge les adresses de facturation et de livraison d'un document de ventes.
   */
  public List<Adresse> chargerListeAdresseDocumentVente(SystemeManager pSystemManager, IdDocumentVente pIdDocumentVente) {
    // Vérifier les pré-requis
    if (pSystemManager == null) {
      throw new MessageErreurException("Impossible de charger les adresses d'un document de ventes car les paramètres sont invalides.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparer le résultat
    List<Adresse> listeAdresse = new ArrayList<Adresse>();
    
    // Préparer les paramètres en entrée
    Svgvm0029i entree = new Svgvm0029i();
    Svgvm0029o sortie = new Svgvm0029o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialiser les paramètres d'entrée
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Exécuter le programme RPG
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSystemManager);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    boolean succes =
        rpg.executerProgramme(SVGVM0029_CHARGER_LISTE_ADRESSE_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList);
    
    // Récupérer le résultat du RPG
    if (succes) {
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Contrôler les erreurs
      controlerErreur();
      
      // Renseigner l'adresse de facturation
      Adresse adresseFacturation = new Adresse();
      adresseFacturation.setNom(sortie.getPonom3().trim());
      adresseFacturation.setComplementNom(sortie.getPocpl3().trim());
      adresseFacturation.setRue(sortie.getPorue3().trim());
      adresseFacturation.setLocalisation(sortie.getPoloc3().trim());
      try {
        adresseFacturation.setCodePostal(Integer.parseInt(sortie.getPocdp3().trim()));
      }
      catch (NumberFormatException e) {
      }
      adresseFacturation.setVille(sortie.getPovil3().trim());
      adresseFacturation.setCodePays(sortie.getPopay3().trim());
      listeAdresse.add(adresseFacturation);
      
      // Renseigner l'adresse de livraison
      Adresse adresseLivraison = new Adresse();
      adresseLivraison.setNom(sortie.getPonom2().trim());
      adresseLivraison.setComplementNom(sortie.getPocpl2().trim());
      adresseLivraison.setRue(sortie.getPorue2().trim());
      adresseLivraison.setLocalisation(sortie.getPoloc2().trim());
      try {
        adresseLivraison.setCodePostal(Integer.parseInt(sortie.getPocdp2().trim()));
      }
      catch (NumberFormatException e) {
      }
      adresseLivraison.setVille(sortie.getPovil2().trim());
      adresseLivraison.setCodePays(sortie.getPopay2().trim());
      listeAdresse.add(adresseLivraison);
      
    }
    return listeAdresse;
  }
  
  /**
   * Appel du programme RPG qui va lire les informations des règlements d'un document.
   */
  public ListeReglement chargerListeReglement(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente, Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de charger les règlements du document de ventes car les paramètres sont invalides.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0008i entree = new Svgvm0008i();
    Svgvm0008o sortie = new Svgvm0008o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0008_CHARGER_LISTE_REGLEMENT_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors du chargement des règlements du document de ventes : " + pIdDocumentVente);
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Initialisation de la classe métier
    ListeReglement listeReglement = new ListeReglement();
    Object[] lignes = sortie.getValeursBrutesLignes();
    for (int i = 0; i < lignes.length; i++) {
      if (lignes[i] != null) {
        listeReglement.add(traiterUneLigneReglement((Object[]) lignes[i]));
      }
    }
    
    // Important
    // Une fois le chargement des règlements, la liste des règlements est dupliquée afin d'être renvoyé telle quel lors de la sauvegarde
    // des règlements (le service RPG en a besoin cela facilite la vie du développeur RPG)
    listeReglement = ListeReglement.sauvegarderReglementOriginaux(listeReglement);
    
    return listeReglement;
  }
  
  /**
   * Découpage d'une ligne brute contenant les informations d'un règlement.
   */
  private Reglement traiterUneLigneReglement(Object[] ligneBrute) {
    if (ligneBrute == null) {
      return null;
    }
    
    // Création de l'identifiant
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) ligneBrute[Svgvm0008d.VAR_POETB]);
    Integer numero = ((BigDecimal) ligneBrute[Svgvm0008d.VAR_POIDU]).intValue();
    IdReglement idReglement = IdReglement.getInstance(idEtablissement, numero);
    
    // Création de l'objet métier
    Reglement reglement = new Reglement(idReglement);
    reglement.setDateCreation(ConvertDate.db2ToDate(((BigDecimal) ligneBrute[Svgvm0008d.VAR_POCRE]).intValue(), null));
    reglement.setMontant((BigDecimal) ligneBrute[Svgvm0008d.VAR_POMTT]);
    // Type de règlement
    EnumTypeReglement typeReglement = EnumTypeReglement.valueOfByCode(((String) ligneBrute[Svgvm0008d.VAR_POTRG]).charAt(0));
    reglement.setTypeReglement(typeReglement);
    reglement.setLibelle(typeReglement.getLibelleEnMinuscules());
    // Règlement modifiable
    char modifiable = ((String) ligneBrute[Svgvm0008d.VAR_PONMOD]).charAt(0);
    if (modifiable == ' ') {
      reglement.setModifiable(true);
    }
    else {
      reglement.setModifiable(false);
    }
    
    // Ce règlement concerne un acompte
    Integer numeroAcompte = Integer.valueOf(((BigDecimal) ligneBrute[Svgvm0008d.VAR_POIDE]).intValue());
    if (numeroAcompte.intValue() > 0) {
      // Création de l'identifiant de l'acompte
      IdReglementAcompte idAcompte = IdReglementAcompte.getInstance(idEtablissement, numeroAcompte);
      ReglementAcompte acompte = new ReglementAcompte(idAcompte);
      // L'acompte est associé au règlement
      reglement.setReglementAcompte(acompte);
      
      // Acompte pris
      char acomptePris = ((String) ligneBrute[Svgvm0008d.VAR_POACPT1]).charAt(0);
      if (acomptePris == 'A') {
        acompte.setPris(true);
      }
      else {
        acompte.setPris(false);
      }
      // Acompte consommé
      char acompteConsomme = ((String) ligneBrute[Svgvm0008d.VAR_POACPT2]).charAt(0);
      if (acompteConsomme == 'C') {
        acompte.setConsomme(true);
      }
      else {
        acompte.setConsomme(false);
      }
    }
    
    // Le numéro du chèque
    String referenceTiree = Constantes.normerTexte((String) ligneBrute[Svgvm0008d.VAR_PORTI]);
    if (!referenceTiree.isEmpty()) {
      reglement.setReferenceTiree(referenceTiree);
    }
    // Le nom de la banque de domiciliation (banque du client si chèque)
    String nomBanque = Constantes.normerTexte((String) ligneBrute[Svgvm0008d.VAR_PODOM]);
    if (!nomBanque.isEmpty()) {
      IdBanqueClient idBanqueClient = IdBanqueClient.getInstance(nomBanque);
      reglement.setBanqueClient(new BanqueClient(idBanqueClient));
    }
    
    return reglement;
  }
  
  /**
   * Appeler le programme RPG qui enregistre les informations d'un document de ventes.
   * 
   * Opérations réalisées :
   * - Prise de n°de document sur param. WS
   * - Ecriture entête document en base de données
   * - Ecriture adresses et extensions d'entête en base de données
   */
  public IdDocumentVente creerDocumentVente(SystemeManager pSysteme, DocumentVente pDocumentVente, Date pDateTraitement) {
    // Vérifier les pré-requis
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de créer le document de ventes car les paramètres sont invalides.");
    }
    DocumentVente.controlerId(pDocumentVente, false);
    if (pDocumentVente.getId().isExistant()) {
      throw new MessageErreurException("Impossible de créer le document de ventes car il existe déjà : " + pDocumentVente.getId());
    }
    if (pDateTraitement == null) {
      throw new MessageErreurException("Impossible de créer le document de ventes car la date de traitement est invalide.");
    }
    IdClient.controlerId(pDocumentVente.getIdClientFacture(), true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0010i entree = new Svgvm0010i();
    Svgvm0010o sortie = new Svgvm0010o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // S'il s'agit d'une extraction, l'id du document d'origine est renseignée
    if (pDocumentVente.getIdOrigine() != null) {
      entree.setPietbx(pDocumentVente.getIdOrigine().getCodeEtablissement());
      entree.setPicodx(pDocumentVente.getIdOrigine().getEntete());
      entree.setPinumx(pDocumentVente.getIdOrigine().getNumero());
      entree.setPisufx(pDocumentVente.getIdOrigine().getSuffixe());
      entree.setPinfax(pDocumentVente.getIdOrigine().getNumeroFacture());
    }
    entree.setPiusr(pDocumentVente.getProfilCreation());
    entree.setPietb(pDocumentVente.getId().getCodeEtablissement());
    entree.setPicli(pDocumentVente.getIdClientFacture().getNumero());
    entree.setPiliv(pDocumentVente.getIdClientFacture().getSuffixe());
    entree.setPimag(pDocumentVente.getIdMagasin().getCode());
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    if (pDocumentVente.geIdModeExpedition() != null) {
      entree.setPimex(pDocumentVente.geIdModeExpedition().getCode());
    }
    entree.setPityp(pDocumentVente.getTypeDocumentVente().getCodeAlpha());
    if (pDocumentVente.getTransport().getIdTransporteur() != null) {
      entree.setPictr(pDocumentVente.getTransport().getIdTransporteur().getCode());
    }
    entree.setPidls(ConvertDate.dateToDb2(pDocumentVente.getTransport().getDateLivraisonSouhaitee()));
    entree.setPidlp(ConvertDate.dateToDb2(pDocumentVente.getTransport().getDateLivraisonPrevue()));
    entree.setPitfa(pDocumentVente.getIdTypeFacturation().getCode().charAt(0));
    entree.setPinom3(pDocumentVente.getAdresseFacturation().getNom());
    entree.setPicpl3(pDocumentVente.getAdresseFacturation().getComplementNom());
    entree.setPirue3(pDocumentVente.getAdresseFacturation().getRue());
    entree.setPiloc3(pDocumentVente.getAdresseFacturation().getLocalisation());
    entree.setPicdp3(pDocumentVente.getAdresseFacturation().getCodePostalFormate());
    entree.setPivil3(pDocumentVente.getAdresseFacturation().getVille());
    if (!pDocumentVente.isEnlevement() && !pDocumentVente.isModeExpeditionNonDefini()) {
      entree.setPinom2(pDocumentVente.getTransport().getAdresseLivraison().getNom());
      entree.setPicpl2(pDocumentVente.getTransport().getAdresseLivraison().getComplementNom());
      entree.setPirue2(pDocumentVente.getTransport().getAdresseLivraison().getRue());
      entree.setPiloc2(pDocumentVente.getTransport().getAdresseLivraison().getLocalisation());
      entree.setPicdp2(pDocumentVente.getTransport().getAdresseLivraison().getCodePostalFormate());
      entree.setPivil2(pDocumentVente.getTransport().getAdresseLivraison().getVille());
      entree.setPienv('L');
    }
    else {
      entree.setPienv('E');
    }
    entree.setPicnt(pDocumentVente.getIdContact());
    entree.setPincc(pDocumentVente.getReferenceCourte());
    entree.setPircc(pDocumentVente.getReferenceLongue());
    entree.setPiact(pDocumentVente.getCodeActionCommerciale());
    entree.setPidat2(ConvertDate.dateToDb2(pDocumentVente.getDateValiditeDevis()));
    entree.setPidat3(ConvertDate.dateToDb2(pDocumentVente.getDateRelanceDevis()));
    entree.setPitar(pDocumentVente.getNumeroTarif());
    entree.setPirem1(pDocumentVente.getPourcentageRemise1Ligne());
    if (pDocumentVente.getIdVendeur() != null) {
      entree.setPivde(pDocumentVente.getIdVendeur().getCode());
    }
    if (pDocumentVente.isDirectUsine()) {
      entree.setPidir('1');
    }
    else {
      entree.setPidir(' ');
    }
    entree.setPiveh(pDocumentVente.getImmatriculationVehiculeClient());
    if (pDocumentVente.getIdChantier() != null) {
      entree.setPichan(pDocumentVente.getIdChantier().getNumero());
    }
    if ((pDocumentVente.getTransport() != null) && (pDocumentVente.getTransport().getIdZoneGeographique() != null)) {
      entree.setPiztr(pDocumentVente.getTransport().getIdZoneGeographique().getCode());
    }
    if (!pDocumentVente.getPrisPar().isEmpty()) {
      entree.setPienlv(String.format(DocumentVente.TEXTE_ARTICLE_COMMENTAIRE_PRIS_PAR, pDocumentVente.getPrisPar()));
    }
    entree.setPiin9(pDocumentVente.getBlocageExpedition());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0010_CREER_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors de la création du document de ventes : " + pDocumentVente.getId());
    }
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Remettre l'identifiant du document avec le numéro et le suffixe renseigné
    return IdDocumentVente.getInstanceHorsFacture(pDocumentVente.getId().getIdEtablissement(), pDocumentVente.getId().getCodeEntete(),
        sortie.getPonum().intValue(), sortie.getPosuf().intValue());
  }
  
  /**
   * Appel du programme RPG qui va dupliquer un document.
   */
  public IdDocumentVente creerDocumentVenteParDuplication(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente, int pTypeDuplication,
      boolean pAvecRecalculPrix) {
    // Vérifier les pré-requis
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de duppliquer un devis car les paramètres sont invalides.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0031i entree = new Svgvm0031i();
    Svgvm0031o sortie = new Svgvm0031o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Renseigner des paramètres d'entrée
    switch (pTypeDuplication) {
      case DUPLICATION_DOCUMENT:
        entree.setPiind("0010000000");
        // Si on demande le recalcul des prix au jour de la dupplication
        if (pAvecRecalculPrix) {
          entree.setPiind("0010001000");
        }
        break;
      case DUPLICATION_DOCUMENT_AVEC_ETAT__SUIVANT:
        entree.setPiind("1000000000");
        // Si on demande le recalcul des prix au jour de la dupplication
        if (pAvecRecalculPrix) {
          entree.setPiind("1000001000");
        }
        break;
      default:
        throw new MessageErreurException("Le paramètre aTypeDuplication est invalide.");
    }
    
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparer l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécuter du programme RPG
    if (!rpg.executerProgramme(SVGVM0031_CREER_DOCUMENT_VENTE_PAR_DUPLICATION, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors de dupplication d'un document.");
    }
    
    // Récupérer les paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Contrôler les erreus
    controlerErreur();
    
    // Retourner l'identifiant
    IdEtablissement idEtablissement = IdEtablissement.getInstance(sortie.getPoetb());
    EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(sortie.getPocod());
    IdDocumentVente idDocumentVente =
        IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, sortie.getPonum(), sortie.getPosuf(), sortie.getPonfa());
    return idDocumentVente;
  }
  
  /**
   * Appel du programme RPG qui va créer un avoir (en fait duplication d'un document).
   */
  public IdDocumentVente creerAvoir(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente, IdTypeAvoir pIdTypeAvoir) {
    // Vérifier les pré-requis
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de créer un avoir car les paramètres sont invalides.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0031i entree = new Svgvm0031i();
    Svgvm0031o sortie = new Svgvm0031o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Renseigner des paramètres d'entrée
    entree.setPiind("0010100000");
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    if (pIdTypeAvoir == null) {
      entree.setPitav(' ');
    }
    else {
      entree.setPitav(pIdTypeAvoir.getCode().charAt(0));
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparer l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécuter du programme RPG
    if (!rpg.executerProgramme(SVGVM0031_CREER_DOCUMENT_VENTE_PAR_DUPLICATION, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors de dupplication d'un document.");
    }
    
    // Récupérer les paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Contrôler les erreus
    controlerErreur();
    
    // Retourner l'identifiant
    IdEtablissement idEtablissement = IdEtablissement.getInstance(sortie.getPoetb());
    EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(sortie.getPocod());
    IdDocumentVente idDocumentVente =
        IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, sortie.getPonum(), sortie.getPosuf(), sortie.getPonfa());
    return idDocumentVente;
  }
  
  /**
   * Appeler le programme RPG qui modifie un document de ventes.
   * 
   * Opérations réalisées :
   * - Ecriture entête en paramètre en base de données.
   * - Ecriture extensions 'Enlevé par ' et n° de chantier.
   */
  public void sauverEnteteDocumentVente(SystemeManager pSysteme, DocumentVente pDocumentVente, Date pDateTraitement) {
    // Vérifier les pré-requis
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de sauver le document de ventes car les paramètres sont invalide.");
    }
    DocumentVente.controlerId(pDocumentVente, true);
    IdClient.controlerId(pDocumentVente.getIdClientFacture(), true);
    if (pDateTraitement == null) {
      throw new MessageErreurException("Impossible de sauver le document de ventes car la date de traitement est invalide.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0024i entree = new Svgvm0024i();
    Svgvm0024o sortie = new Svgvm0024o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind("");
    entree.setPietb(pDocumentVente.getId().getCodeEtablissement());
    entree.setPicod(pDocumentVente.getId().getEntete());
    entree.setPinum(pDocumentVente.getId().getNumero());
    entree.setPisuf(pDocumentVente.getId().getSuffixe());
    entree.setPinfa(pDocumentVente.getId().getNumeroFacture());
    
    entree.setE1nfa(pDocumentVente.getId().getNumeroFacture());
    
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPiopt("");
    entree.setPiedt("");
    entree.setPimex(pDocumentVente.geIdModeExpedition().getCode());
    
    entree.setE1cre(ConvertDate.dateToDb2(pDocumentVente.getDateCreation()));
    entree.setE1hom(ConvertDate.dateToDb2(pDocumentVente.getDateValidationCommande()));
    entree.setE1exp(ConvertDate.dateToDb2(pDocumentVente.getDateExpeditionBon()));
    entree.setE1fac(ConvertDate.dateToDb2(pDocumentVente.getDateFacturation()));
    entree.setE1nlig(pDocumentVente.getNombreLignes());
    
    entree.setE1eta(pDocumentVente.getEtat().getCode());
    entree.setE1edt(pDocumentVente.getTopEdition());
    entree.setE1tcl(pDocumentVente.getTopClientLivre());
    entree.setE1tcf(pDocumentVente.getTopClientFacture());
    entree.setE1tva1(pDocumentVente.getNumeroTVA1());
    entree.setE1tva2(pDocumentVente.getNumeroTVA2());
    entree.setE1tva3(pDocumentVente.getNumeroTVA3());
    entree.setE1tpf(pDocumentVente.getTopTaxeParafiscale());
    entree.setE1rlv(pDocumentVente.getTopReleve());
    entree.setE1nex(pDocumentVente.getNombreExemplairesFactures());
    entree.setE1tar(pDocumentVente.getNumeroTarif());
    entree.setE1rbf(pDocumentVente.getTopRegroupementBonFacture());
    entree.setE1sgn(pDocumentVente.getSigne());
    entree.setE1sl1(pDocumentVente.getMontantSoumisTVA1());
    entree.setE1sl2(pDocumentVente.getMontantSoumisTVA2());
    entree.setE1sl3(pDocumentVente.getMontantSoumisTVA3());
    entree.setE1thtl(pDocumentVente.getTotalHT());
    entree.setE1tl1(pDocumentVente.getMontantTVA1());
    entree.setE1tl2(pDocumentVente.getMontantTVA2());
    entree.setE1tl3(pDocumentVente.getMontantTVA3());
    entree.setE1stp(pDocumentVente.getMontantSoumisTaxeParafiscale());
    entree.setE1mtp(pDocumentVente.getMontantTaxeParafiscale());
    entree.setE1mes(pDocumentVente.getMontantEscompte());
    entree.setE1ttc(pDocumentVente.getTotalTTC());
    entree.setE1tpvl(pDocumentVente.getTotalHTPrixVenteBase());
    entree.setE1tprl(pDocumentVente.getTotalHTPrixRevient());
    entree.setE1to1g(pDocumentVente.getTopsTraitementSysteme());
    entree.setE1se1(pDocumentVente.getBaseEscompte1());
    entree.setE1se2(pDocumentVente.getBaseEscompte2());
    entree.setE1se3(pDocumentVente.getBaseEscompte3());
    entree.setE1aff(ConvertDate.dateToDb2(pDocumentVente.getDateAffectation()));
    entree.setE1arr(pDocumentVente.getArrondiTTCEnCentimes());
    entree.setE1tdv(pDocumentVente.getEtatDevis().getCode());
    entree.setE1mci(pDocumentVente.getMontantCommandeInitiale());
    entree.setE1clcg(pDocumentVente.getNumeroCompteAuxiliaireClient());
    entree.setE1gba(pDocumentVente.getTypeGenerationCommandeAchat());
    entree.setE1dco(ConvertDate.dateToDb2(pDocumentVente.getDateCommande()));
    entree.setE1dlp(ConvertDate.dateToDb2(pDocumentVente.getTransport().getDateLivraisonPrevue()));
    entree.setE1dls(ConvertDate.dateToDb2(pDocumentVente.getTransport().getDateLivraisonSouhaitee()));
    entree.setE1dat2(ConvertDate.dateToDb2(pDocumentVente.getDateValiditeDevis()));
    entree.setPidat3(ConvertDate.dateToDb2(pDocumentVente.getDateRelanceDevis()));
    if (pDocumentVente.getIdClientCentrale() != null) {
      entree.setE1cc1(pDocumentVente.getIdClientCentrale().getNumero());
    }
    entree.setE1esc(pDocumentVente.getPourcentageEscompte());
    // C'est stocké en 5 dont 2 dans la table mais le programme RPG le redéfini en 5 dont 3 (donc on multiplit par 10)
    entree.setE1tv1(pDocumentVente.getTauxTVA1().multiply(BigDecimal.TEN));
    entree.setE1tv2(pDocumentVente.getTauxTVA2().multiply(BigDecimal.TEN));
    entree.setE1tv3(pDocumentVente.getTauxTVA3().multiply(BigDecimal.TEN));
    entree.setE1ttp(pDocumentVente.getTauxTaxeParafiscale());
    entree.setE1comg(pDocumentVente.getPourcentageCommissionnementRep1());
    entree.setE1co2g(pDocumentVente.getPourcentageCommissionnementRep2());
    entree.setE1clfp(pDocumentVente.getIdClientFacture().getNumero());
    entree.setE1clfs(pDocumentVente.getIdClientFacture().getSuffixe());
    entree.setE1dpr(pDocumentVente.getDelaiPreparation());
    entree.setE1col(pDocumentVente.getNombreColis());
    entree.setE1pds(pDocumentVente.getPoidsTotalKg());
    entree.setE1bas(pDocumentVente.getBaseDevise());
    entree.setE1chg(pDocumentVente.getTauxChange());
    entree.setE1rbc(pDocumentVente.getReductionBaseCommissionRep1());
    entree.setE1rbc2(pDocumentVente.getReductionBaseCommissionRep2());
    entree.setE1dat1(ConvertDate.dateToDb2(pDocumentVente.getDateMiniFacturation()));
    entree.setPitfa(pDocumentVente.getIdTypeFacturation().getCode().charAt(0));
    // = aDocument.get (entree.setE1dat2
    // = aDocument.get (entree.setE1dat3
    if (pDocumentVente.getDateDernierTraitementDevis() != null) {
      entree.setE1dat4(ConvertDate.dateToDb2(pDocumentVente.getDateDernierTraitementDevis()));
    }
    entree.setE1rem1(pDocumentVente.getPourcentageRemise1Ligne());
    entree.setE1rem2(pDocumentVente.getPourcentageRemise2Ligne());
    entree.setE1rem3(pDocumentVente.getPourcentageRemise3Ligne());
    entree.setE1rem4(pDocumentVente.getPourcentageRemise4Ligne());
    entree.setE1rem5(pDocumentVente.getPourcentageRemise5Ligne());
    entree.setE1rem6(pDocumentVente.getPourcentageRemise6Ligne());
    entree.setE1rp1(pDocumentVente.getPourcentageRemise1Pied());
    entree.setE1rp2(pDocumentVente.getPourcentageRemise2Pied());
    entree.setE1rp3(pDocumentVente.getPourcentageRemise3Pied());
    entree.setE1rp4(pDocumentVente.getPourcentageRemise4Pied());
    entree.setE1rp5(pDocumentVente.getPourcentageRemise5Pied());
    entree.setE1rp6(pDocumentVente.getPourcentageRemise6Pied());
    
    if (pDocumentVente.getDateCreation() != null) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(pDocumentVente.getDateCreation());
      entree.setE1hre(new BigDecimal(calendar.get(Calendar.HOUR_OF_DAY) * 100 + calendar.get(Calendar.MINUTE)));
    }
    
    entree.setE1nto(pDocumentVente.getNumeroTournee());
    entree.setE1oto(pDocumentVente.getRangDansTournee());
    entree.setE1vol(pDocumentVente.getVolume());
    entree.setE1lgm(pDocumentVente.getLongueurMax());
    entree.setE1m2p(pDocumentVente.getSurfacePlancher());
    entree.setE1ncc(pDocumentVente.getReferenceCourte());
    if (pDocumentVente.getIdRepresentant1() != null) {
      entree.setE1rep(pDocumentVente.getIdRepresentant1().getCode());
    }
    if (pDocumentVente.getIdRepresentant2() != null) {
      entree.setE1rep2(pDocumentVente.getIdRepresentant2().getCode());
    }
    entree.setE1mag(pDocumentVente.getIdMagasin().getCode());
    entree.setE1maga(pDocumentVente.getIdMagasinSortieAncien().getCode());
    entree.setE1mex(pDocumentVente.geIdModeExpedition().getCode());
    if (pDocumentVente.getTransport().getIdTransporteur() != null) {
      entree.setE1ctr(pDocumentVente.getTransport().getIdTransporteur().getCode());
    }
    entree.setE1san(pDocumentVente.getSectionAnalytique());
    entree.setE1act(pDocumentVente.getCodeActionCommerciale());
    entree.setE1dev(pDocumentVente.getCodeDevise());
    entree.setE1cnv(pDocumentVente.getCodeConditionVente());
    entree.setE1rcc(Constantes.formaterStringStrict(pDocumentVente.getReferenceLongue(), Svgvm0024i.SIZE_E1RCC));
    if (pDocumentVente.getIdVendeur() != null) {
      entree.setE1vde(pDocumentVente.getIdVendeur().getCode());
    }
    entree.setE1trc(pDocumentVente.getTransportACalculer().charAt(0));
    if (pDocumentVente.getTransport() != null && pDocumentVente.getTransport().getIdZoneGeographique() != null) {
      entree.setE1ztr(pDocumentVente.getTransport().getIdZoneGeographique().getCode());
    }
    entree.setE1ebp(pDocumentVente.getTopEditionBonPreparation().charAt(0));
    entree.setE1trp(pDocumentVente.getTypeCommissionnementRep().charAt(0));
    entree.setE1nho(pDocumentVente.getTopBonNonConfirmable().charAt(0));
    entree.setE1tp1(pDocumentVente.getTopPersonnalisable1());
    entree.setE1tp2(pDocumentVente.getTopPersonnalisable2());
    entree.setE1tp3(pDocumentVente.getTopPersonnalisable3());
    entree.setE1tp4(pDocumentVente.getTopPersonnalisable4());
    entree.setE1tp5(pDocumentVente.getTopPersonnalisable5());
    entree.setE1veh(pDocumentVente.getImmatriculationVehiculeClient());
    entree.setE1in1(pDocumentVente.getTopNePasFacturer().charAt(0));
    entree.setE1in2(pDocumentVente.getTopIndicateurTournee().charAt(0));
    entree.setE1in3(pDocumentVente.getTopTraiteEditee().charAt(0));
    entree.setE1in4(pDocumentVente.getTopRemisesSpeciales().charAt(0));
    entree.setE1in5(pDocumentVente.getTopExistenceRemisePied().charAt(0));
    entree.setE1in6(pDocumentVente.getTopFacturecomptoir().charAt(0));
    entree.setE1in7(pDocumentVente.getCodePreparationCommande().charAt(0));
    entree.setE1in8(pDocumentVente.getTransport().getTopEditionBordereau().charAt(0));
    entree.setE1in9(pDocumentVente.getBlocageExpedition());
    entree.setE1cct(pDocumentVente.getCodeContrat());
    entree.setE1trl(pDocumentVente.getTypeRemise().charAt(0));
    entree.setE1brl(pDocumentVente.getBaseRemise().charAt(0));
    entree.setE1tre(pDocumentVente.getTypeRemisePied().charAt(0));
    entree.setE1in11(pDocumentVente.getTopModeMesure().charAt(0));
    entree.setE1in12(pDocumentVente.getTopExtractionForceeExpedition().charAt(0));
    entree.setE1can(pDocumentVente.getCodeCanalVente());
    entree.setE1crt(pDocumentVente.getTransport().getCodeReroupement());
    entree.setE1pre(pDocumentVente.getCodePreparateur());
    entree.setE1pfc(pDocumentVente.getCodePlateformeChargement());
    entree.setE1in17(pDocumentVente.getTopTypeVente().charAt(0));
    
    // Livraison
    if (pDocumentVente.isLivraison()) {
      entree.setPienv('L');
    }
    else {
      entree.setPienv('E');
    }
    
    // direct Usine
    if (pDocumentVente.isDirectUsine()) {
      entree.setPidir('1');
      entree.setE1in18('D');
    }
    else {
      entree.setPidir(' ');
      entree.setE1in18(' ');
    }
    
    // Generation immediate GBA
    if (pDocumentVente.isGenerationImmediateCommandeAchat()) {
      entree.setPiimd('1');
    }
    else {
      entree.setPiimd(' ');
    }
    
    if (pDocumentVente.getIdChantier() != null) {
      entree.setPichan(pDocumentVente.getIdChantier().getNumero());
    }
    // Le "Pris par" n'a de sens que pour les enlèvements
    if (!pDocumentVente.getPrisPar().isEmpty() && pDocumentVente.isEnlevement()) {
      entree.setPienlv(String.format(DocumentVente.TEXTE_ARTICLE_COMMENTAIRE_PRIS_PAR, pDocumentVente.getPrisPar()));
    }
    else {
      entree.setPienlv("");
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0024_SAUVER_ENTETE_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors de la sauvegarde du document de ventes : " + pDocumentVente.getId());
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
  }
  
  /**
   * Appel du programme RPG qui va modifier les adresses de facturation et de livraison d'un document.
   */
  public void sauverAdresseDocumentVente(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente, Adresse pAdresseFacturation,
      Adresse pAdresseLivraison) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    if ((pAdresseFacturation == null) && (pAdresseLivraison == null)) {
      return;
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0025i entree = new Svgvm0025i();
    Svgvm0025o sortie = new Svgvm0025o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Adresse facturation
    if (pAdresseFacturation != null) {
      entree.setPinom3(pAdresseFacturation.getNom());
      entree.setPicpl3(pAdresseFacturation.getComplementNom());
      entree.setPirue3(pAdresseFacturation.getRue());
      entree.setPiloc3(pAdresseFacturation.getLocalisation());
      entree.setPicdp3(pAdresseFacturation.getCodePostalFormate());
      entree.setPivil3(pAdresseFacturation.getVille());
      entree.setPipay3(pAdresseFacturation.getCodePays());
    }
    
    // Adresse livraison
    if (pAdresseLivraison != null) {
      entree.setPinom2(pAdresseLivraison.getNom());
      entree.setPicpl2(pAdresseLivraison.getComplementNom());
      entree.setPirue2(pAdresseLivraison.getRue());
      entree.setPiloc2(pAdresseLivraison.getLocalisation());
      entree.setPicdp2(pAdresseLivraison.getCodePostalFormate());
      entree.setPivil2(pAdresseLivraison.getVille());
      entree.setPipay2(pAdresseLivraison.getCodePays());
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0025_SAUVER_ADRESSE_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va écrire les informations du règlement d'un document.
   */
  public void sauverListeReglement(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente, ListeReglement pListeReglement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    if (pListeReglement == null) {
      throw new MessageErreurException("La liste des règlements est incorrecte.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0042i entree = new Svgvm0042i();
    Svgvm0042o sortie = new Svgvm0042o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind("0000000000");
    
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Copie des nouveaux règlements
    int indice = 0;
    for (Reglement reglement : pListeReglement) {
      initialiserStructureAvecReglement(indice, reglement, entree);
      indice++;
    }
    // Copie des règlements originaux (s'il y en a car lors de la création d'un nouveau document il n'y a pas de règlements d'origine)
    ListeReglement listeReglementOriginale = pListeReglement.getListeReglementOriginale();
    if (listeReglementOriginale != null) {
      indice = ListeReglement.NOMBRE_REGLEMENTS_MAX;
      for (Reglement reglement : listeReglementOriginale) {
        initialiserStructureAvecReglement(indice, reglement, entree);
        indice++;
      }
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0042_SAUVER_LISTE_REGLEMENT_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Initialise la structure de données des règlements en vue de la sauvegarde.
   */
  private void initialiserStructureAvecReglement(int pIndice, Reglement pReglement, Svgvm0042i pEntree) {
    if (pReglement == null) {
      throw new MessageErreurException("L'objet règlement est invalide.");
    }
    if (pEntree == null) {
      throw new MessageErreurException("La datastruture Svgvm0042i est invalide.");
    }
    if (pEntree.dsd.length < pIndice) {
      throw new MessageErreurException("L'indice d'accès est supérieur à la longueur de la datastruture Svgvm0042i.");
    }
    
    pEntree.dsd[pIndice].setPietb(pReglement.getId().getCodeEtablissement());
    pEntree.dsd[pIndice].setPiidu(pReglement.getId().getNumero());
    pEntree.dsd[pIndice].setPicre(ConvertDate.dateToDb2(pReglement.getDateCreation()));
    pEntree.dsd[pIndice].setPitrg(pReglement.getTypeReglement().getCode());
    if (pReglement.isModifiable()) {
      pEntree.dsd[pIndice].setPinmod(' ');
    }
    else {
      pEntree.dsd[pIndice].setPinmod('X');
    }
    pEntree.dsd[pIndice].setPimtt(pReglement.getMontant());
    // S'il s'agit d'un acompte
    if (pReglement.getReglementAcompte() != null) {
      ReglementAcompte acompte = pReglement.getReglementAcompte();
      if (acompte.isPris()) {
        pEntree.dsd[pIndice].setPiacpt1('A');
      }
      if (acompte.isConsomme()) {
        pEntree.dsd[pIndice].setPiacpt2('C');
      }
      if (acompte.isExistant()) {
        pEntree.dsd[pIndice].setPiide(acompte.getId().getNumero());
      }
    }
    
    // Le numéro de chèque
    pEntree.dsd[pIndice].setPirti(pReglement.getReferenceTiree());
    // Le nom de la banque de domiciliation
    if (pReglement.getBanqueClient() != null) {
      pEntree.dsd[pIndice].setPidom(pReglement.getBanqueClient().getNom());
    }
  }
  
  /**
   * Appel du programme RPG qui va ajouter un document à la liste du règlement multiple.
   */
  public void sauverReglementMultipleDocumentVente(SystemeManager pSysteme, boolean pViderListe, IdDocumentVente pIdDocumentVente) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0041i entree = new Svgvm0041i();
    Svgvm0041o sortie = new Svgvm0041o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind("0000000000");
    if (pViderListe) {
      entree.setPitrt('D');
    }
    
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0041_SAUVER_REGLEMENT_MULTIPLE_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va annuler un document.
   */
  public void annulerDocumentVente(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0030i entree = new Svgvm0030i();
    Svgvm0030o sortie = new Svgvm0030o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0030_ANNULER_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appeler le programme RPG qui contrôle l'entête d'un document de ventes.
   * 
   * Contrôles effectués :
   * - existence code établissement
   * - PIDAT comprise dans l'encours de gestion commerciale
   * - existence code client
   * - existence code magasin
   * - code transporteur obligatoire si PS N°235
   * - existence code transporteur si défini
   * - adresse facturation obligatoire : raison sociale , code postal et ville
   * - adresse de livraison obligatoire si livraison : raison sociale , rue ou localité , code postal et ville
   * - référence longue obligatoire (sauf devis) si paramétré sur fiche client
   * - référence longue obligatoire et unique sur chantier
   * - référence courte obligatoire si PS N°157 (sauf chantier et devis)
   * - référence courte obligatoire et unique si PS N° 157 = 2,4,5 ou 6 (sauf chantier et devis)
   * - référence longue et courte obligatoire si PS N°157 = 3
   * - contrôle existence contact si saisi
   * - date de validité du devis ou chantier obligatoire si PS 203 = 2 ou 3
   * - type de facturation obligatoire
   * - contrôle existence du type de facturation
   * - contrôle existence du document dans la table PGVMEBC
   * - Contrôle validité PIDLP : si différent de la date de livraison prévue du document et date d'expédition nulle -> PIDLP doit être
   * supérieur à PIDAT
   * - Contrôle validité PIDLS : si différent de la date de livraison souhaitée du document et date d'expédition nulle -> PIDLS doit être
   * supérieur à PIDAT
   * - Date de livraison prévue obligatoire (sauf chantier et et devis) si PS 154 = 1 ou 3
   * - Date de livraison souhaitée obligatoire (sauf chantier et et devis) si PS 154 = 2 ou 3
   * - contrôle existence code affaire ou chantier
   * - véhicule obligatoire si enlèvement (sauf devis et chantier) si paramétré sur client
   * - pris par obligatoire si paramétré sur client
   */
  public void controlerDocumentVente(SystemeManager pSysteme, DocumentVente pDocumentVente, Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de contrôler le document de ventes car les paramètres sont invalides.");
    }
    DocumentVente.controlerId(pDocumentVente, false);
    if (pDateTraitement == null) {
      throw new MessageErreurException("Impossible de contrôler le document de ventes car la date de traitement est invalide.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0006i entree = new Svgvm0006i();
    Svgvm0006o sortie = new Svgvm0006o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    // Initialisation des paramètres d'entrée
    entree.setPietb(pDocumentVente.getId().getCodeEtablissement());
    entree.setPicod(pDocumentVente.getId().getEntete());
    entree.setPinum(pDocumentVente.getId().getNumero());
    entree.setPisuf(pDocumentVente.getId().getSuffixe());
    entree.setPinfa(pDocumentVente.getId().getNumeroFacture());
    if (pDocumentVente.getIdClientFacture() != null) {
      entree.setPicli(pDocumentVente.getIdClientFacture().getNumero());
      entree.setPiliv(pDocumentVente.getIdClientFacture().getSuffixe());
    }
    entree.setPimag(pDocumentVente.getIdMagasin().getCode());
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPimex(pDocumentVente.geIdModeExpedition().getCode());
    entree.setPityp(pDocumentVente.getTypeDocumentVente().getCodeAlpha());
    if (pDocumentVente.getTransport().getIdTransporteur() != null) {
      entree.setPictr(pDocumentVente.getTransport().getIdTransporteur().getCode());
    }
    entree.setPidls(ConvertDate.dateToDb2(pDocumentVente.getTransport().getDateLivraisonSouhaitee()));
    entree.setPidlp(ConvertDate.dateToDb2(pDocumentVente.getTransport().getDateLivraisonPrevue()));
    entree.setPidat2(ConvertDate.dateToDb2(pDocumentVente.getDateValiditeDevis()));
    entree.setPitfa(pDocumentVente.getIdTypeFacturation().getCode().charAt(0));
    entree.setPinom3(pDocumentVente.getAdresseFacturation().getNom());
    entree.setPicpl3(pDocumentVente.getAdresseFacturation().getComplementNom());
    entree.setPirue3(pDocumentVente.getAdresseFacturation().getRue());
    entree.setPiloc3(pDocumentVente.getAdresseFacturation().getLocalisation());
    entree.setPicdp3(pDocumentVente.getAdresseFacturation().getCodePostalFormate());
    entree.setPivil3(pDocumentVente.getAdresseFacturation().getVille());
    if (!pDocumentVente.isEnlevement() && !pDocumentVente.isModeExpeditionNonDefini()) {
      entree.setPinom2(pDocumentVente.getTransport().getAdresseLivraison().getNom());
      entree.setPicpl2(pDocumentVente.getTransport().getAdresseLivraison().getComplementNom());
      entree.setPirue2(pDocumentVente.getTransport().getAdresseLivraison().getRue());
      entree.setPiloc2(pDocumentVente.getTransport().getAdresseLivraison().getLocalisation());
      entree.setPicdp2(pDocumentVente.getTransport().getAdresseLivraison().getCodePostalFormate());
      entree.setPivil2(pDocumentVente.getTransport().getAdresseLivraison().getVille());
    }
    entree.setPicnt(pDocumentVente.getIdContact());
    entree.setPincc(pDocumentVente.getReferenceCourte());
    entree.setPircc(pDocumentVente.getReferenceLongue());
    entree.setPiact(pDocumentVente.getCodeActionCommerciale());
    entree.setPiveh(pDocumentVente.getImmatriculationVehiculeClient());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0006_CONTROLER_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors du contrôle du document de ventes : " + pDocumentVente.getId());
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
  }
  
  /**
   * Appel du programme RPG qui va contrôler l'encours client.
   */
  public EncoursClient controlerEncoursClient(SystemeManager pSysteme, DocumentVente pDocumentVente, int pCollectifComptableClient) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    DocumentVente.controlerId(pDocumentVente, true);
    IdClient.controlerId(pDocumentVente.getIdClientFacture(), true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0026i entree = new Svgvm0026i();
    Svgvm0026o sortie = new Svgvm0026o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pDocumentVente.getId().getCodeEtablissement());
    entree.setPicod(pDocumentVente.getId().getEntete());
    entree.setPinum(pDocumentVente.getId().getNumero());
    entree.setPisuf(pDocumentVente.getId().getSuffixe());
    entree.setPicli(pDocumentVente.getIdClientFacture().getNumero());
    entree.setPincg(pCollectifComptableClient);
    entree.setPi0ttc(pDocumentVente.getTotalTTCPrecedent());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    EncoursClient encoursclient = null;
    if (rpg.executerProgramme(SVGVM0026_CONTROLER_ENCOURS_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      encoursclient = new EncoursClient();
      
      // Entrée
      // Sortie
      encoursclient.setIndicateurs(sortie.getPoind()); // Indicateurs (POIND)
      encoursclient.setEffets(sortie.getPoeff()); // Effets (PoEFF)
      encoursclient.setPositionComptable(sortie.getPopco().longValue()); // Position comptable (PoPCO)
      encoursclient.setTotalAcompte(sortie.getPoacc()); // Total accompte (PoACC)
      encoursclient.setTotalPortefeuille(sortie.getPopor()); // Total portefeuille (POPOR)
      encoursclient.setRemiseEscompte(sortie.getPoesc()); // Remise Escompte (Effets) (PoESC )
      encoursclient.setRemiseEncaissement(sortie.getPoenc()); // Remise Encaissement(POENC)
      encoursclient.setTotalImpayes(sortie.getPotim()); // Total impayés (POTIM)
      encoursclient.setLimiteImpayes(sortie.getPolim()); // Impayes si date de création > date dernier impaye (PoLIM)
      encoursclient.setDateDernierImpaye(sortie.getPodim()); // Date impayé (PoDIM)
      encoursclient.setNombreImpayes(sortie.getPonim()); // Nombre d"impayés (PONIM)
      encoursclient.setTotalAffacturage(sortie.getPoafm()); // Total affacturage (POAFM)
      encoursclient.setTopAffacturage(sortie.getPotaf()); // Top affacturage (POTAF)
      encoursclient.setTotalEcheancesDepassees(sortie.getPoecd()); // Total échéances dépassées (POECD)
      encoursclient.setEncoursCommande(sortie.getPocde()); // Encours / Commande (POCDE)
      encoursclient.setEncoursExpedie(sortie.getPoexp()); // Encours / Expédié (POEXP)
      encoursclient.setEncoursFacture(sortie.getPofac()); // Encours / Facturé (POFAC)
      encoursclient.setVenteAssimileeExport(sortie.getPovae()); // Vente assimilé export (POVAE)
      encoursclient.setPlafond(sortie.getPoplf()); // Plafond encours autorisé (POPLF)
      encoursclient.setPlafondExceptionnel(sortie.getPoplf2()); // Sur-Plafond encours autorisé (POPLF2)
      encoursclient.setDateLimitePlafondExceptionnel(sortie.getPodplConvertiEnDate()); // Date Limite pour
      // Sur-plafond
      // (PODPL)
      encoursclient.setPlafondMaxDeblocage(sortie.getPoplmx()); // Plafond maxi en déblocage (POPLMX)
      encoursclient.setDepassement(sortie.getPodepa()); // Dépassement (PODEPA)
      encoursclient.setEncours(sortie.getPopos()); // Position encours (PODPOS)
      
      // Les variables d'alertes
      initialiserAlertesDocument(encoursclient.getAlertesEncoursClient(), sortie);
    }
    
    return encoursclient;
  }
  
  /**
   * Initialise les alertes après l'appel du programme RPG.
   */
  private void initialiserAlertesDocument(AlertesEncoursClient pAlertes, Svgvm0026o pSortie) {
    // Dépassement plafond, demande autorisation générée
    if (pSortie.getPoer01().intValue() == 1) {
      pAlertes.setPlafondDepasseDemandeAutorisationGeneree(true);
      pAlertes.setMessageDemandeAutorisationGeneree(pSortie.getPoer01a().trim());
    }
    // Dépassement plafond, paiement comptant obligatoire
    if (pSortie.getPoer02().intValue() == 1) {
      pAlertes.setPlafondDepassePaiementComptantObligatoire(true);
      pAlertes.setMessagePaiementComptantObligatoire(pSortie.getPoer02a().trim());
    }
  }
  
  /**
   * Appeler le programme RPG qui contrôle un document de ventes.
   * 
   * Opérations effectuées :
   * - Contrôle entête et lignes du document.
   * - Chiffrage lignes et cumul dans totaux document.
   * - Proposition option de fin de document ou message d'erreur.
   */
  public OptionsValidation controlerFinDocumentVente(SystemeManager pSysteme, DocumentVente pDocumentVente,
      OptionsValidation pOptionsValidation, Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de contrôler le document de ventes car les paramètres sont invalides.");
    }
    if (pOptionsValidation == null) {
      throw new MessageErreurException("Impossible de contrôler le document de ventes car les paramètres sont invalides.");
    }
    DocumentVente.controlerId(pDocumentVente, true);
    if (pDateTraitement == null) {
      throw new MessageErreurException("Impossible de contrôler le document de ventes car la date de traitement est invalide.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0003i entree = new Svgvm0003i();
    Svgvm0003o sortie = new Svgvm0003o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pDocumentVente.getId().getCodeEtablissement());
    entree.setPicod(pDocumentVente.getId().getEntete());
    entree.setPinum(pDocumentVente.getId().getNumero());
    entree.setPisuf(pDocumentVente.getId().getSuffixe());
    entree.setPinfa(pDocumentVente.getId().getNumeroFacture());
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPiopt(pOptionsValidation.getCodePropose());
    if (pDocumentVente.getIdAdresseFournisseur() != null) {
      IdFournisseur idFournisseur = pDocumentVente.getIdAdresseFournisseur().getIdFournisseur();
      entree.setPicol(idFournisseur.getCollectif());
      entree.setPifrs(idFournisseur.getNumero());
      entree.setPifre(pDocumentVente.getIdAdresseFournisseur().getIndice());
    }
    else {
      entree.setPicol(BigDecimal.ZERO);
      entree.setPifrs(BigDecimal.ZERO);
      entree.setPifre(BigDecimal.ZERO);
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0003_CONTROLER_FIN_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors du contrôle du document de ventes : " + pDocumentVente.getId());
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Initialisation de la classe métier
    // Entrée
    // Sortie
    pOptionsValidation.setListeCodes(sortie.getPoopp());
    pOptionsValidation.setListeLibelles(sortie.getPooppl());
    pOptionsValidation.setCodePropose(sortie.getPoopp0());
    pOptionsValidation.setLibellePropose(sortie.getPooppl0());
    
    // Les variables d'alertes
    initialiserAlerteDocumentVente(pOptionsValidation.getAlertesDocument(), sortie);
    
    return pOptionsValidation;
  }
  
  /**
   * Initialise les alertes après l'appel du programme RPG.
   */
  private void initialiserAlerteDocumentVente(AlertesDocument pAlertes, Svgvm0003o pSortie) {
    // Alerte pour type avoir
    pAlertes.setTypeAvoir(pSortie.getPoer01() == 1);
    if (pAlertes.isTypeAvoir()) {
      pAlertes.setTypeAvoirAttendu(pSortie.getPoer01a().toString());
    }
    
    // Franco garanti à la Cmde
    pAlertes.setFrancoGarantiCommande(pSortie.getPoer02() == 1);
    
    // Franco non atteint
    pAlertes.setFrancoNonAtteint(pSortie.getPoer03() == 1);
    if (pAlertes.isFrancoNonAtteint()) {
      pAlertes.setFrancoAttendu(BigDecimal.valueOf(pSortie.getPoer03a()));
    }
    
    // Poids Franco garanti/Cmde
    pAlertes.setFrancoGarantiCommande(pSortie.getPoer04() == 1);
    
    // Franco non atteint
    pAlertes.setFrancoNonAtteint(pSortie.getPoer05() == 1);
    if (pAlertes.isFrancoNonAtteint()) {
      pAlertes.setFrancoAttendu(BigDecimal.valueOf(pSortie.getPoer05a()));
    }
    
    // Minimum.Cmd non atteint
    pAlertes.setMiniCommandeNonAtteint(pSortie.getPoer06() == 1);
    if (pAlertes.isMiniCommandeNonAtteint()) {
      pAlertes.setMiniCommandeAttendu(BigDecimal.valueOf(pSortie.getPoer06a()));
    }
    
    // Marge mini non atteinte
    pAlertes.setMargeMiniNonAtteinte(pSortie.getPoer07() == 1);
    if (pAlertes.isMargeMiniNonAtteinte()) {
      pAlertes.setPoucentageMargeMiniAttendu(BigDecimal.valueOf(pSortie.getPoer07a()));
    }
    
    // Marge anormalement haute
    pAlertes.setMargeAnormalementHaute(pSortie.getPoer08() == 1);
    if (pAlertes.isMargeAnormalementHaute()) {
      pAlertes.setPoucentageMargeHauteAttendu(BigDecimal.valueOf(pSortie.getPoer08a()));
    }
    
    // Attn Facture différée
    pAlertes.setAttentionFactureDifferee(pSortie.getPoer09() == 1);
    
    // Existe situation juridique
    pAlertes.setExisteSituationJuridique(pSortie.getPoer10() == 1);
    
    // Echéances dépassées
    pAlertes.setEcheanceDepassee(pSortie.getPoer11() == 1);
    
    // Stock non disponible
    pAlertes.setStockNonDisponible(pSortie.getPoer12() == 1);
    
    // Aucune ligne livrable
    pAlertes.setAucuneLigneLivrable(pSortie.getPoer13() == 1);
    
    // Nombre de lignes en erreur
    pAlertes.setNombreLignesEnErreur(pSortie.getPoer14() == 1);
    if (pAlertes.isNombreLignesEnErreur()) {
      pAlertes.setNombreLignesEnErreurAttendu(pSortie.getPoer14a().intValue());
    }
    
    // Au - un lot non en stock
    pAlertes.setAuMoinsUnLotNonEnStock(pSortie.getPoer15() == 1);
    
    // Plafond 1 dépassé pas le 2
    pAlertes.setPremierPlafondDepasse(pSortie.getPoer16() == 1);
    if (pAlertes.isPremierPlafondDepasse()) {
      pAlertes.setMessagePremierPlafondDepasse(pSortie.getPoer16a().trim());
    }
    
    // Plafond dépassé
    pAlertes.setTousLesPlafondsDepasses(pSortie.getPoer17() == 1);
    if (pAlertes.isTousLesPlafondsDepasses()) {
      pAlertes.setMessageTousLesPlafondsDepasses(pSortie.getPoer17a().trim());
    }
    
    // Acompte oblig. non reçu
    pAlertes.setAcompteObligatoireNonRecu(pSortie.getPoer18() == 1);
    
    // Acompte obligatoire
    pAlertes.setAcompteObligatoireNormal(pSortie.getPoer19() == 1);
    pAlertes.setPourcentageAcompteNormal(pSortie.getPoer19a());
    
    // Montant autorisé escompte dépassé
    pAlertes.setMontantAutoriseEscompteDepasse(pSortie.getPoer20() == 1);
    if (pAlertes.isMontantAutoriseEscompteDepasse()) {
      pAlertes.setMontantAutoriseEscompte(BigDecimal.valueOf(pSortie.getPoer20a()));
    }
    
    // Erreur de prix sur ligne
    pAlertes.setErreurPrixSurLigne(pSortie.getPoer21() == 1);
    if (pAlertes.isErreurPrixSurLigne()) {
      pAlertes.setNumeroLigneErreurPrixSurLigne(pSortie.getPoer21a().trim());
    }
    
    // Double quantité non saisie
    pAlertes.setDoubleQuantiteNonSaisie(pSortie.getPoer22() == 1);
    if (pAlertes.isDoubleQuantiteNonSaisie()) {
      pAlertes.setNumeroLigneDoubleQuantiteNonSaisie(pSortie.getPoer22a().trim());
    }
    // Numéro de série incorrect
    pAlertes.setNumeroSerieIncorrect(pSortie.getPoer23() == 1);
    if (pAlertes.isNumeroSerieIncorrect()) {
      pAlertes.setNumeroLigneNumeroSerieIncorrect(pSortie.getPoer23a().trim());
    }
    // Numéro de lot incorrect
    pAlertes.setNumeroLotIncorrect(pSortie.getPoer24() == 1);
    if (pAlertes.isNumeroLotIncorrect()) {
      pAlertes.setNumeroLigneNumeroLotIncorrect(pSortie.getPoer24a().trim());
    }
    // Adresse stock incorrecte
    pAlertes.setAdresseStockIncorrecte(pSortie.getPoer25() == 1);
    if (pAlertes.isAdresseStockIncorrecte()) {
      pAlertes.setNumeroLigneadresseStockIncorrecte(pSortie.getPoer25a().trim());
    }
    // Poids supérieur à 100 tonnes
    pAlertes.setAdresseStockIncorrecte(pSortie.getPoer26() == 1);
    if (pAlertes.isAdresseStockIncorrecte()) {
      pAlertes.setNumeroLigneadresseStockIncorrecte(pSortie.getPoer26a().trim());
    }
    
    // Acompte obligatoire lors de la détection d'articles spéciaux
    pAlertes.setAcompteObligatoireDetectionArticleSpecial(pSortie.getPoer27() == 1);
    pAlertes.setPourcentageAcompteDetectionArticleSpecial(pSortie.getPoer27a());
    
    // Prix de vente inférieur au PUMP
    pAlertes.setPrixInferieurPump(pSortie.getPoer28() == 1);
    pAlertes.setCodeArticlePrixInferieurPump(pSortie.getPoer28a());
    
    // Prix de vente inférieur au PRV
    pAlertes.setErreurPrixInferieurPrv(pSortie.getPoer29() == 1);
    pAlertes.setCodeArticlePrixInferieurPrv(pSortie.getPoer29a());
  }
  
  /**
   * Appeler le programme RPG qui chiffre un document de ventes.
   * 
   * Opéations réalisées :
   * - Lancement du service controlerFinDocument (SVGVM0003)
   * - Chiffrage externe (SGVM77)
   */
  public void modifierChiffrageDocumentVente(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente, boolean pConserverPrix) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0019i entree = new Svgvm0019i();
    Svgvm0019o sortie = new Svgvm0019o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
    // On souhaite conserver les prix
    if (pConserverPrix) {
      indicateurs[0] = '0';
    }
    // On souhaite actualiser les prix
    else {
      indicateurs[0] = '2';
    }
    entree.setPiind(new String(indicateurs));
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0019_MODIFIER_CHIFFRAGE_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va modifier le client du document.
   */
  public void modifierClientDocumentVente(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente, IdClient pIdClient,
      boolean pChangerClientFacture, boolean pChangerClientLivre) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    IdClient.controlerId(pIdClient, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0036i entree = new Svgvm0036i();
    Svgvm0036o sortie = new Svgvm0036o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    String ind = "00000";
    if (pChangerClientFacture) {
      ind += "1";
    }
    else {
      ind += "0";
    }
    if (pChangerClientLivre) {
      ind += "1";
    }
    else {
      ind += "0";
    }
    entree.setPiind(String.format("%s%0" + (Svgvm0036i.SIZE_PIIND - ind.length()) + "d", ind, 0));
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    entree.setPicli(pIdClient.getNumero());
    entree.setPiliv(pIdClient.getSuffixe());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0036_MODIFIER_CLIENT_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appeler le programme RPG qui valide un document de ventes.
   * Ne doit pas être appelé si le document est n'est pas modifiable.
   * Opérations réalisées :
   * - Traitement de l'option demandée sur un document existant
   * - Traitement stock éventuel pour chaque ligne
   * - Lancement des traitements de fin de document
   * - Edition éventuelle
   */
  public DocumentVente modifierValidationDocumentVente(SystemeManager pSysteme, DocumentVente pDocumentVente, OptionsValidation pOptions,
      Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de valider le document de ventes car les paramètres sont invalides.");
    }
    DocumentVente.controlerId(pDocumentVente, true);
    if (pDateTraitement == null) {
      throw new MessageErreurException("Impossible de valider le document de ventes car la date de traitement est invalide.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0017i entree = new Svgvm0017i();
    Svgvm0017o sortie = new Svgvm0017o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pDocumentVente.getId().getCodeEtablissement());
    entree.setPicod(pDocumentVente.getId().getEntete());
    entree.setPinum(pDocumentVente.getId().getNumero());
    entree.setPisuf(pDocumentVente.getId().getSuffixe());
    entree.setPinfa(pDocumentVente.getId().getNumeroFacture());
    
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPiopt(pOptions.getCodePropose());
    entree.setPieta0(pDocumentVente.getEtatOrigine().getCode());
    entree.setPittc0(pDocumentVente.getTotalTTCOrigine());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0017_MODIFIER_VALIDATION_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      throw new MessageErreurException("Erreur lors de la validation du document de ventes  : " + pDocumentVente.getId());
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Création de l'id document achat si le numéro et le suffixe sont renseignés
    if (sortie.getPonuma().intValue() > 0) {
      IdDocumentAchat documentAchatLie = IdDocumentAchat.getInstance(pDocumentVente.getId().getIdEtablissement(),
          EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION, sortie.getPonuma(), sortie.getPosufa());
      pDocumentVente.setIdDocumentAchatLie(documentAchatLie);
    }
    
    pDocumentVente.setValide(true);
    return pDocumentVente;
  }
  
  /**
   * Appel du programme RPG qui va valider un document.
   */
  public void validerExtractionDocumentVente(SystemeManager pSysteme, DocumentVente pDocumentVente, Date pDateTraitement,
      OptionsValidation pOptions) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pDateTraitement == null) {
      throw new MessageErreurException("Le paramètre pDateTraitement du service est à null.");
    }
    DocumentVente.controlerId(pDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0048i entree = new Svgvm0048i();
    Svgvm0048o sortie = new Svgvm0048o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pDocumentVente.getId().getCodeEtablissement());
    entree.setPicod(pDocumentVente.getId().getEntete());
    entree.setPinum(pDocumentVente.getId().getNumero());
    entree.setPisuf(pDocumentVente.getId().getSuffixe());
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPiopt(pOptions.getCodePropose());
    entree.setPieta0(pDocumentVente.getEtatOrigine().getCode());
    entree.setPittc0(pDocumentVente.getTotalTTCOrigine());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0048_VALIDER_EXTRACTION_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va éditer un document.
   */
  public String editerDocument(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente, boolean pDocumentModifiable,
      Date pDateTraitement, OptionsEditionVente pOptionsEdition, LdaSerien pLdaSerien) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    if (pDateTraitement == null) {
      throw new MessageErreurException("Le paramètre pDateTraitement est à null.");
    }
    if (pOptionsEdition == null) {
      throw new MessageErreurException("Le paramètre pOptionsEdition est à null.");
    }
    if (pLdaSerien == null) {
      throw new MessageErreurException("Le paramètre LdaSerieN est invalide.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvm0018i entree = new Svgvm0018i();
    Svgvm0018o sortie = new Svgvm0018o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    char[] indicateurs = new char[] { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
    if (!pDocumentModifiable) {
      indicateurs[0] = '1';
    }
    entree.setPiind(new String(indicateurs));
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPiopt(pOptionsEdition.getCodeValidation());
    entree.setPiedt(pOptionsEdition.getCodeEdition());
    entree.setPiin3(pOptionsEdition.getEditionChiffree().getCode());
    if (pOptionsEdition.isStockageBaseDocumentaire()) {
      entree.setPistk('1');
    }
    else {
      entree.setPistk(' ');
    }
    if (pOptionsEdition.getIdMail() != null) {
      entree.setPinml(pOptionsEdition.getIdMail().getNumero());
    }
    if (pOptionsEdition.getIdFaxMail() != null) {
      entree.setPinfl(pOptionsEdition.getIdFaxMail().getNumero());
    }
    if (pOptionsEdition.getTypeDocumentStocke() != null) {
      entree.setPitdsk(pOptionsEdition.getTypeDocumentStocke().getCode());
    }
    if (pOptionsEdition.getIndicatifDocument() != null) {
      entree.setPiiddc(pOptionsEdition.getIndicatifDocument());
    }
    if (pOptionsEdition.getIndicatifClient() != null) {
      entree.setPiidcl(pOptionsEdition.getIndicatifClient());
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Mise à jour des données dans la LDA
    String curlib = pSysteme.getCurlib();
    if (curlib != null && curlib.startsWith("FM")) {
      // Met le numéro de la FM, nécessaire pour construire un chemin correct pour le stockage du PDF
      curlib = String.format("%-3s", curlib.substring(2));
      rpg.executerCommande("CHGDTAARA DTAARA(*LDA (13 3)) VALUE('" + curlib + "')");
    }
    // Création ou mise à jour de la DTAARA QTEMP/LDASERIEN qui contient le code écran pour les affectations d'impression
    rpg.executerCommande(pLdaSerien.getCommandeCLCreerDataArea());
    rpg.executerCommande(pLdaSerien.getCommandeCLMettreAJourDataArea());
    
    // Mise à jour de la locale pour les options d'édition
    if (pOptionsEdition.getModeEdition() != null && pOptionsEdition.getModeEdition().getEnumModeEdition() != null) {
      switch (pOptionsEdition.getModeEdition().getEnumModeEdition()) {
        case VISUALISER:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE('@')");
          break;
        case IMPRIMER:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE(' ')");
          break;
        case ENVOYER_PAR_MAIL:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE('@')");
          break;
        case ENVOYER_PAR_FAX:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE('f')");
          break;
        default:
          rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE(' ')");
      }
    }
    else {
      rpg.executerCommande("CHGDTAARA DTAARA(*LDA (200 1)) VALUE(' ')");
    }
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(SVGVM0018_EDITER_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      return null;
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Nettoayge du chemin qui n'est pas toujours parfait...
    String fichierPDF = sortie.getPochm().trim();
    fichierPDF = fichierPDF.replaceAll("//", "/");
    Trace.info("Editer le document PDF : " + fichierPDF);
    return fichierPDF;
  }
  
  /**
   * Appel du programme RPG qui va modifier le client d'un document en cours de création.
   */
  public void changerClientDocument(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente, IdClient pIdClient,
      boolean isAvecRecalcul) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    IdClient.controlerId(pIdClient, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0061i entree = new Svgvm0061i();
    Svgvm0061o sortie = new Svgvm0061o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    if (isAvecRecalcul) {
      entree.setPiind("0000001000");
    }
    else {
      entree.setPiind("0000000000");
    }
    
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPicli2(pIdClient.getNumero());
    entree.setPiliv2(pIdClient.getSuffixe());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0061_CHANGER_CLIENT_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va supprimer les règlements d'un document.
   * Suppression des règlements effectués le même jour et non remis en banque.
   */
  public void supprimerReglement(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0063i entree = new Svgvm0063i();
    Svgvm0063o sortie = new Svgvm0063o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind("0000000000");
    
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    entree.setPinfa(pIdDocumentVente.getNumeroFacture());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0063_SUPPRIMER_REGLEMENT_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
  /**
   * Appel du programme RPG qui va solder le reliquat d'un devis ou d'une commande, puis clôturer le document.
   * Suppression des règlements effectués le même jour et non remis en banque.
   */
  public void solderReliquat(SystemeManager pSysteme, IdDocumentVente pIdDocumentVente) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0064i entree = new Svgvm0064i();
    Svgvm0064o sortie = new Svgvm0064o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind("0000000000");
    
    entree.setPietb(pIdDocumentVente.getCodeEtablissement());
    entree.setPicod(pIdDocumentVente.getEntete());
    entree.setPinum(pIdDocumentVente.getNumero());
    entree.setPisuf(pIdDocumentVente.getSuffixe());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(SVGVM0064_SOLDER_RELIQUAT_DOCUMENT_VENTE, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
}
