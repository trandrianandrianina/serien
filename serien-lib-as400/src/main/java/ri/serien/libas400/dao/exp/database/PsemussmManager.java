/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class PsemussmManager extends QueryManager {
  
  /**
   * Constructeur
   * @param database
   */
  public PsemussmManager(Connection database) {
    super(database);
  }
  
  /**
   * Retourner l'enregistrement pour un profil.
   * Attention la requête est sensible à la casse.
   * 
   * @param pBibliothequeEnvironnement Bibliothèque de l'environnement Série N.
   * @param pProfil Profil dont il faut charger l'enregistrement.
   * @return Enresgitrement du profil.
   * 
   */
  public GenericRecord getRecordForPrf(String pBibliothequeEnvironnement, String pProfil) {
    // Vérifier les paramètres
    if (pBibliothequeEnvironnement == null) {
      throw new MessageErreurException("La bibliothèque de l'environnement est invalide");
    }
    if (pProfil == null) {
      throw new MessageErreurException("Le profil est invalide");
    }
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord;
    try {
      listeRecord = select("Select * from " + pBibliothequeEnvironnement + ".psemussm where ussprf = '" + pProfil + "'");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
    
    // Lire le résultat
    GenericRecord psemussm = null;
    if (listeRecord != null && listeRecord.size() > 0) {
      psemussm = listeRecord.get(0);
    }
    
    return psemussm;
  }
  
  /**
   * Récupération de la bibliothèque utilisateur
   * @return
   * 
   */
  public String getBibliotheque(String bibenv, String prf) {
    GenericRecord psemussm = getRecordForPrf(bibenv, prf);
    if (psemussm == null) {
      return null;
    }
    
    return psemussm.getString("USSBIB");
  }
}
