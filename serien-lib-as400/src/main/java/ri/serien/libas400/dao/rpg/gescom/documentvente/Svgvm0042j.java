/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0042j {
  // Constantes
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PIIDU = 9;
  public static final int DECIMAL_PIIDU = 0;
  public static final int SIZE_PICRE = 7;
  public static final int DECIMAL_PICRE = 0;
  public static final int SIZE_PITRG = 1;
  public static final int SIZE_PINMOD = 1;
  public static final int SIZE_PIMTT = 11;
  public static final int DECIMAL_PIMTT = 2;
  public static final int SIZE_PIACPT1 = 1;
  public static final int SIZE_PIACPT2 = 1;
  public static final int SIZE_PIIDE = 10;
  public static final int DECIMAL_PIIDE = 0;
  public static final int SIZE_PIRTI = 10;
  public static final int SIZE_PIDOM = 24;
  public static final int SIZE_TOTALE_DS = 78;
  public static final int SIZE_FILLER = 100 - 78;
  
  // Constantes indices Nom DS
  public static final int VAR_PIETB = 0;
  public static final int VAR_PIIDU = 1;
  public static final int VAR_PICRE = 2;
  public static final int VAR_PITRG = 3;
  public static final int VAR_PINMOD = 4;
  public static final int VAR_PIMTT = 5;
  public static final int VAR_PIACPT1 = 6;
  public static final int VAR_PIACPT2 = 7;
  public static final int VAR_PIIDE = 8;
  public static final int VAR_PIRTI = 9;
  public static final int VAR_PIDOM = 10;
  
  // Variables AS400
  private String pietb = ""; // Code établissement
  private BigDecimal piidu = BigDecimal.ZERO; // Identifiant unique Règlement
  private BigDecimal picre = BigDecimal.ZERO; // Date de création
  private String pitrg = ""; // Type règlement
  private String pinmod = ""; // Si <> blanc , Non modifiable
  private BigDecimal pimtt = BigDecimal.ZERO; // Montant
  private String piacpt1 = ""; // Si = A , Acompte Pris
  private String piacpt2 = ""; // Si = C , Acompte Consommé
  private BigDecimal piide = BigDecimal.ZERO; // Ident. unique si acompte
  private String pirti = ""; // Référence tirée
  private String pidom = ""; // Domiciliation
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PIIDU, DECIMAL_PIIDU), // Identifiant unique Règlement
      new AS400ZonedDecimal(SIZE_PICRE, DECIMAL_PICRE), // Date de création
      new AS400Text(SIZE_PITRG), // Type règlement
      new AS400Text(SIZE_PINMOD), // Si <> blanc , Non modifiable
      new AS400ZonedDecimal(SIZE_PIMTT, DECIMAL_PIMTT), // Montant
      new AS400Text(SIZE_PIACPT1), // Si = A , Acompte Pris
      new AS400Text(SIZE_PIACPT2), // Si = C , Acompte Consommé
      new AS400ZonedDecimal(SIZE_PIIDE, DECIMAL_PIIDE), // Ident. unique si acompte
      new AS400Text(SIZE_PIRTI), // Référence tirée
      new AS400Text(SIZE_PIDOM), // Domiciliation
      new AS400Text(SIZE_FILLER), // Filler
  };
  
  // -- Accesseurs
  
  public Object[] getStructureObject() {
    Object[] o = { pietb, piidu, picre, pitrg, pinmod, pimtt, piacpt1, piacpt2, piide, pirti, pidom, filler, };
    return o;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPiidu(BigDecimal pPiidu) {
    if (pPiidu == null) {
      return;
    }
    piidu = pPiidu.setScale(DECIMAL_PIIDU, RoundingMode.HALF_UP);
  }
  
  public void setPiidu(Integer pPiidu) {
    if (pPiidu == null) {
      return;
    }
    piidu = BigDecimal.valueOf(pPiidu);
  }
  
  public Integer getPiidu() {
    return piidu.intValue();
  }
  
  public void setPicre(BigDecimal pPicre) {
    if (pPicre == null) {
      return;
    }
    picre = pPicre.setScale(DECIMAL_PICRE, RoundingMode.HALF_UP);
  }
  
  public void setPicre(Integer pPicre) {
    if (pPicre == null) {
      return;
    }
    picre = BigDecimal.valueOf(pPicre);
  }
  
  public void setPicre(Date pPicre) {
    if (pPicre == null) {
      return;
    }
    picre = BigDecimal.valueOf(ConvertDate.dateToDb2(pPicre));
  }
  
  public Integer getPicre() {
    return picre.intValue();
  }
  
  public Date getPicreConvertiEnDate() {
    return ConvertDate.db2ToDate(picre.intValue(), null);
  }
  
  public void setPitrg(Character pPitrg) {
    if (pPitrg == null) {
      return;
    }
    pitrg = String.valueOf(pPitrg);
  }
  
  public Character getPitrg() {
    return pitrg.charAt(0);
  }
  
  public void setPinmod(Character pPinmod) {
    if (pPinmod == null) {
      return;
    }
    pinmod = String.valueOf(pPinmod);
  }
  
  public Character getPinmod() {
    return pinmod.charAt(0);
  }
  
  public void setPimtt(BigDecimal pPimtt) {
    if (pPimtt == null) {
      return;
    }
    pimtt = pPimtt.setScale(DECIMAL_PIMTT, RoundingMode.HALF_UP);
  }
  
  public void setPimtt(Double pPimtt) {
    if (pPimtt == null) {
      return;
    }
    pimtt = BigDecimal.valueOf(pPimtt).setScale(DECIMAL_PIMTT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPimtt() {
    return pimtt.setScale(DECIMAL_PIMTT, RoundingMode.HALF_UP);
  }
  
  public void setPiacpt1(Character pPiacpt1) {
    if (pPiacpt1 == null) {
      return;
    }
    piacpt1 = String.valueOf(pPiacpt1);
  }
  
  public Character getPiacpt1() {
    return piacpt1.charAt(0);
  }
  
  public void setPiacpt2(Character pPiacpt2) {
    if (pPiacpt2 == null) {
      return;
    }
    piacpt2 = String.valueOf(pPiacpt2);
  }
  
  public Character getPiacpt2() {
    return piacpt2.charAt(0);
  }
  
  public void setPiide(BigDecimal pPiide) {
    if (pPiide == null) {
      return;
    }
    piide = pPiide.setScale(DECIMAL_PIIDE, RoundingMode.HALF_UP);
  }
  
  public void setPiide(Integer pPiide) {
    if (pPiide == null) {
      return;
    }
    piide = BigDecimal.valueOf(pPiide);
  }
  
  public Integer getPiide() {
    return piide.intValue();
  }
  
  public void setPirti(String pPirti) {
    if (pPirti == null) {
      return;
    }
    pirti = pPirti;
  }
  
  public String getPirti() {
    return pirti;
  }
  
  public void setPidom(String pPidom) {
    if (pPidom == null) {
      return;
    }
    pidom = pPidom;
  }
  
  public String getPidom() {
    return pidom;
  }
}
