/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

public class DsAffaires extends DataStructureRecord {
  
  @Override
  protected void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "PARETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "PARTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PARIND"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "ACLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "ACCNV"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "ACNATV"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "ACNATA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "ACTER"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "ACCLA"));
    
    length = 300;
  }
}
