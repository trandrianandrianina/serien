/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs.client;

import java.util.ArrayList;

import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.CritereClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeIdClient;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

/**
 * Centralise les méthodes proposant des accès SQL aux données des clients.
 */
public class GM_Client extends BaseGroupDB {
  // Variables
  private SystemeManager system = null;
  private String etablissement = "";
  private M_Client client = new M_Client(queryManager);
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public GM_Client(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques
  
  public void initialization(String etb) {
    // Initialisation des variables
    client.initialization();
    
    // Initialisation de l'établissement
    setEtablissement(etb);
  }
  
  /**
   * Lecture d'un lot de client
   * @param cond sans le "where"
   * @return
   */
  public M_Client[] readInDatabase(String cond) {
    String requete = "select * from " + queryManager.getLibrary() + ".PGVMCLIM cli where TRIM(CLNOM) <> '' AND LENGTH(TRIM(CLNOM))>1";
    if ((cond != null) && (!cond.trim().equals(""))) {
      if (!cond.trim().toLowerCase().startsWith("and")) {
        requete += " and " + cond;
      }
      else {
        requete += " " + cond;
      }
    }
    return request4ReadClient(requete);
  }
  
  /**
   * Requête permettant la lecture d'un lot de clients
   * @param requete
   * @return
   */
  public M_Client[] request4ReadClient(String requete) {
    if ((requete == null) || (requete.trim().length() == 0)) {
      msgErreur += "\nLa requête n'est pas correcte.";
      return null;
    }
    if (queryManager.getLibrary() == null) {
      msgErreur += "\nLa CURLIB n'est pas initialisée.";
      return null;
    }
    
    // Lecture de la base afin de récupérer les actions commerciales
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    if (listrcd == null) {
      return null;
    }
    // Chargement des classes avec les données de la base
    int i = 0;
    M_Client[] listcl = new M_Client[listrcd.size()];
    for (GenericRecord rcd : listrcd) {
      M_Client cl = new M_Client(queryManager);
      cl.initObject(rcd, true);
      cl.loadContacts();
      
      listcl[i++] = cl;
    }
    
    return listcl;
  }
  
  /**
   * Charger une liste des informations de base de clients en fonction des critères de recherche des clients.
   * 
   * @param pCritereClient Critères de recherche des clients.
   * @return Liste des informations de base des clients.
   */
  public ListeClientBase chargerListeClientBase(CritereClient pCritereClient) {
    // Construire la requête
    String sql =
        "select row_number() over() as numrow, cli.CLETB, cli.CLCLI, cli.CLLIV, cli.CLNOM, cli.CLCPL, cli.CLRUE, cli.CLLOC, cli.CLCDP1, cli.CLVIL, cli.CLPAY, cli.CLTEL, "
            + "cli.CLCAT, cli.CLIN9, cli.CLIN29, ecli.EBIN05 from " + curlib + '.' + EnumTableBDD.CLIENT + " cli"
            + " left join " + curlib + '.' + EnumTableBDD.CLIENT_EXTENSION
            + " ecli on cli.CLETB = ecli.EBETB and cli.CLCLI = ecli.EBCLI and cli.CLLIV = ecli.EBLIV";
    sql += " where " + pCritereClient.getRequeteWhere();
    sql += " order by " + pCritereClient.getRequeteOrderBy();
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = queryManager.select(sql);
    if (listeGenericRecord == null) {
      return null;
    }
    
    // Traitement des données collectées
    ListeClientBase listeClientBase = new ListeClientBase();
    for (GenericRecord genericRecord : listeGenericRecord) {
      // Une erreur lors du chargement d'un client n'empêche pas le chargement des autres clients de la liste
      try {
        // Renseigner le client
        M_Client mclient = new M_Client(queryManager);
        mclient.initObject(genericRecord, true);
        Client client = mclient.initClient();
        
        // Renseigner le type de compte client
        // Verrue qui disparaitra lorsque l'on ne se servira plus des classes M_Client et GM_Client
        if (Constantes.equals(genericRecord.getCharacterValue("CLIN29", ' '), 'P')) {
          client.setTypeCompteClient(EnumTypeCompteClient.PROSPECT);
        }
        else if (Constantes.equals(genericRecord.getStringValue("EBIN05", "null", true), "null")) {
          client.setTypeCompteClient(EnumTypeCompteClient.EN_COMPTE);
        }
        else {
          client.setTypeCompteClient(EnumTypeCompteClient.valueOfByCode(genericRecord.getStringValue("EBIN05")));
        }
        
        // Ajouter le client à la liste résultat
        listeClientBase.add(client);
      }
      catch (Exception e) {
        Trace.alerte("Un client invalide est ignoré lors du chargement de la liste des client : CLETB="
            + genericRecord.getField("CLETB").toString() + " CLCLI=" + genericRecord.getField("CLCLI").toString() + " CLLIV="
            + genericRecord.getField("CLLIV").toString());
      }
    }
    
    return listeClientBase;
  }
  
  /**
   * Charger une liste des informations de base de clients à partir d'une liste d'identifiants clients.
   * 
   * @param pListeIdClient Liste des identifiants clients à retourner.
   * @return Liste des informations de base des clients.
   */
  public ListeClientBase chargerListeClientBase(ListeIdClient pListeIdClient) {
    // Construire la requête
    String sql =
        "select row_number() over() as numrow, cli.CLETB, cli.CLCLI, cli.CLLIV, cli.CLNOM, cli.CLCPL, cli.CLRUE, cli.CLLOC, cli.CLCDP1, cli.CLVIL, cli.CLPAY, cli.CLTEL, "
            + "cli.CLCAT, cli.CLIN9, cli.CLIN29, ecli.EBIN05 from " + curlib + '.' + EnumTableBDD.CLIENT + " cli"
            + " left join " + curlib + '.' + EnumTableBDD.CLIENT_EXTENSION
            + " ecli on cli.CLETB = ecli.EBETB and cli.CLCLI = ecli.EBCLI and cli.CLLIV = ecli.EBLIV";
    sql += " where (" + pListeIdClient.getRequeteWhere() + ")";
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeGenericRecord = queryManager.select(sql);
    if (listeGenericRecord == null) {
      return null;
    }
    
    // Générer la liste des clients à retourner
    ListeClientBase listeClientBase = new ListeClientBase();
    for (GenericRecord genericRecord : listeGenericRecord) {
      // Une erreur lors du chargement d'un client n'empêche pas le chargement des autres clients de la liste
      try {
        // Renseigner le client
        M_Client mclient = new M_Client(queryManager);
        mclient.initObject(genericRecord, true);
        Client client = mclient.initClient();
        
        // Renseigner le type de compte client
        // Verrue qui disparaitra lorsque l'on ne se servira plus des classes M_Client et GM_Client
        if (Constantes.equals(genericRecord.getCharacterValue("CLIN29", ' '), 'P')) {
          client.setTypeCompteClient(EnumTypeCompteClient.PROSPECT);
        }
        else if (Constantes.equals(genericRecord.getStringValue("EBIN05", "null", true), "null")) {
          client.setTypeCompteClient(EnumTypeCompteClient.EN_COMPTE);
        }
        else {
          client.setTypeCompteClient(EnumTypeCompteClient.valueOfByCode(genericRecord.getStringValue("EBIN05")));
        }
        
        // Ajouter le client à la liste résultat
        listeClientBase.add(client);
      }
      catch (Exception e) {
        Trace.alerte("Un client invalide est ignoré lors du chargement de la liste des client : CLETB="
            + genericRecord.getField("CLETB").toString() + " CLCLI=" + genericRecord.getField("CLCLI").toString() + " CLLIV="
            + genericRecord.getField("CLLIV").toString());
      }
    }
    
    return listeClientBase;
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    queryManager = null;
    system = null;
    
    if (client != null) {
      client.dispose();
    }
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  /**
   * @return le etablissement
   */
  public String getEtablissement() {
    return etablissement;
  }
  
  /**
   * @param etablissement le etablissement à définir
   */
  public void setEtablissement(String etablissement) {
    boolean etbIsLoaded = (etablissement == this.etablissement);
    if (etablissement == null) {
      this.etablissement = "";
    }
    else {
      this.etablissement = etablissement;
    }
    
    if (etbIsLoaded) {
      // loadListObjectAction();
      // actionCommerciale.setListObjectAction(listObjectAction);
    }
  }
  
  /**
   * @return le system
   */
  public SystemeManager getSystem() {
    return system;
  }
  
  /**
   * @param system le system à définir
   */
  public void setSystem(SystemeManager system) {
    this.system = system;
  }
  
}
