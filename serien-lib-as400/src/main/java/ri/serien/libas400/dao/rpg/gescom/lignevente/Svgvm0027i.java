/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0027i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PITOP = 1;
  public static final int DECIMAL_PITOP = 0;
  public static final int SIZE_PIERL = 1;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PIQTE = 11;
  public static final int DECIMAL_PIQTE = 3;
  public static final int SIZE_PIKSV = 7;
  public static final int DECIMAL_PIKSV = 3;
  public static final int SIZE_PIPVB = 9;
  public static final int DECIMAL_PIPVB = 2;
  public static final int SIZE_PIREM1 = 4;
  public static final int DECIMAL_PIREM1 = 2;
  public static final int SIZE_PIREM2 = 4;
  public static final int DECIMAL_PIREM2 = 2;
  public static final int SIZE_PIREM3 = 4;
  public static final int DECIMAL_PIREM3 = 2;
  public static final int SIZE_PIREM4 = 4;
  public static final int DECIMAL_PIREM4 = 2;
  public static final int SIZE_PIREM5 = 4;
  public static final int DECIMAL_PIREM5 = 2;
  public static final int SIZE_PIREM6 = 4;
  public static final int DECIMAL_PIREM6 = 2;
  public static final int SIZE_PITRL = 1;
  public static final int SIZE_PIBRL = 1;
  public static final int SIZE_PIPVN = 9;
  public static final int DECIMAL_PIPVN = 2;
  public static final int SIZE_PIPVC = 9;
  public static final int DECIMAL_PIPVC = 2;
  public static final int SIZE_PIMHT = 9;
  public static final int DECIMAL_PIMHT = 2;
  public static final int SIZE_PIUNV = 2;
  public static final int SIZE_PICND = 9;
  public static final int DECIMAL_PICND = 3;
  public static final int SIZE_PIMAG = 2;
  public static final int SIZE_PIQTP = 11;
  public static final int DECIMAL_PIQTP = 3;
  public static final int SIZE_PILB = 124;
  public static final int SIZE_PITT = 1;
  public static final int DECIMAL_PITT = 0;
  public static final int SIZE_PITAR = 2;
  public static final int DECIMAL_PITAR = 0;
  public static final int SIZE_PITB = 1;
  public static final int DECIMAL_PITB = 0;
  public static final int SIZE_PITN = 1;
  public static final int DECIMAL_PITN = 0;
  public static final int SIZE_PITR = 1;
  public static final int DECIMAL_PITR = 0;
  public static final int SIZE_PIQT1 = 8;
  public static final int DECIMAL_PIQT1 = 3;
  public static final int SIZE_PIQT2 = 8;
  public static final int DECIMAL_PIQT2 = 3;
  public static final int SIZE_PIQT3 = 8;
  public static final int DECIMAL_PIQT3 = 3;
  public static final int SIZE_PINBR = 6;
  public static final int DECIMAL_PINBR = 0;
  public static final int SIZE_PIPRV = 9;
  public static final int DECIMAL_PIPRV = 2;
  public static final int SIZE_PIIN6 = 1;
  public static final int DECIMAL_PIIN6 = 0;
  public static final int SIZE_PICOL = 1;
  public static final int DECIMAL_PICOL = 0;
  public static final int SIZE_PIFRS = 6;
  public static final int DECIMAL_PIFRS = 0;
  public static final int SIZE_PIFRE = 4;
  public static final int DECIMAL_PIFRE = 0;
  public static final int SIZE_PIIN2 = 1;
  public static final int SIZE_PIUNL = 2;
  public static final int SIZE_PISER = 2;
  public static final int DECIMAL_PISER = 0;
  public static final int SIZE_PIIN18 = 1;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 339;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PICOD = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINLI = 5;
  public static final int VAR_PITOP = 6;
  public static final int VAR_PIERL = 7;
  public static final int VAR_PIART = 8;
  public static final int VAR_PIQTE = 9;
  public static final int VAR_PIKSV = 10;
  public static final int VAR_PIPVB = 11;
  public static final int VAR_PIREM1 = 12;
  public static final int VAR_PIREM2 = 13;
  public static final int VAR_PIREM3 = 14;
  public static final int VAR_PIREM4 = 15;
  public static final int VAR_PIREM5 = 16;
  public static final int VAR_PIREM6 = 17;
  public static final int VAR_PITRL = 18;
  public static final int VAR_PIBRL = 19;
  public static final int VAR_PIPVN = 20;
  public static final int VAR_PIPVC = 21;
  public static final int VAR_PIMHT = 22;
  public static final int VAR_PIUNV = 23;
  public static final int VAR_PICND = 24;
  public static final int VAR_PIMAG = 25;
  public static final int VAR_PIQTP = 26;
  public static final int VAR_PILB = 27;
  public static final int VAR_PITT = 28;
  public static final int VAR_PITAR = 29;
  public static final int VAR_PITB = 30;
  public static final int VAR_PITN = 31;
  public static final int VAR_PITR = 32;
  public static final int VAR_PIQT1 = 33;
  public static final int VAR_PIQT2 = 34;
  public static final int VAR_PIQT3 = 35;
  public static final int VAR_PINBR = 36;
  public static final int VAR_PIPRV = 37;
  public static final int VAR_PIIN6 = 38;
  public static final int VAR_PICOL = 39;
  public static final int VAR_PIFRS = 40;
  public static final int VAR_PIFRE = 41;
  public static final int VAR_PIIN2 = 42;
  public static final int VAR_PIUNL = 43;
  public static final int VAR_PISER = 44;
  public static final int VAR_PIIN18 = 45;
  public static final int VAR_PIARR = 46;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private String picod = ""; // Code ERL "D","E",X","9"
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de ligne
  private BigDecimal pitop = BigDecimal.ZERO; // Code Etat de la Ligne
  private String pierl = ""; // Code ERL "C"
  private String piart = ""; // Code Article
  private BigDecimal piqte = BigDecimal.ZERO; // Quantité Commandée
  private BigDecimal piksv = BigDecimal.ZERO; // Nbre d'US pour 1 UV
  private BigDecimal pipvb = BigDecimal.ZERO; // Prix de Vente de Base
  private BigDecimal pirem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal pirem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal pirem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal pirem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal pirem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal pirem6 = BigDecimal.ZERO; // % Remise 6
  private String pitrl = ""; // Type remise ligne, 1=cascade
  private String pibrl = ""; // Base remise ligne, 1=Montant
  private BigDecimal pipvn = BigDecimal.ZERO; // Prix de Vente Net
  private BigDecimal pipvc = BigDecimal.ZERO; // Prix de Vente Calculé
  private BigDecimal pimht = BigDecimal.ZERO; // Montant Hors Taxes
  private String piunv = ""; // Code Unité de Vente
  private BigDecimal picnd = BigDecimal.ZERO; // Conditionnement
  private String pimag = ""; // Magasin
  private BigDecimal piqtp = BigDecimal.ZERO; // Quantité en Pièces
  private String pilb = ""; // Libellé
  private BigDecimal pitt = BigDecimal.ZERO; // Top colonne tarif saisie
  private BigDecimal pitar = BigDecimal.ZERO; // Numéro colonne tarif
  private BigDecimal pitb = BigDecimal.ZERO; // Top prix de base saisi
  private BigDecimal pitn = BigDecimal.ZERO; // Top prix net saisi
  private BigDecimal pitr = BigDecimal.ZERO; // Top remise saisie
  private BigDecimal piqt1 = BigDecimal.ZERO; // A1TSP<>" " longueur
  private BigDecimal piqt2 = BigDecimal.ZERO; // A1TSP<>" " largeur
  private BigDecimal piqt3 = BigDecimal.ZERO; // A1TSP<>" " hauteur
  private BigDecimal pinbr = BigDecimal.ZERO; // A1TSP<>" " nombre
  private BigDecimal piprv = BigDecimal.ZERO; // Prix de revient
  private BigDecimal piin6 = BigDecimal.ZERO; // Top prix de revient saisi
  private BigDecimal picol = BigDecimal.ZERO; // Collectif fournisseur
  private BigDecimal pifrs = BigDecimal.ZERO; // Numéro fournisseur
  private BigDecimal pifre = BigDecimal.ZERO; // Lien FRS / FRE
  private String piin2 = ""; // Ind. Type de gratuit
  private String piunl = ""; // Unité conditionnement vente
  private BigDecimal piser = BigDecimal.ZERO; // Top n° de serie ou lot
  private String piin18 = ""; // Origine du prix
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9"
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de ligne
      new AS400ZonedDecimal(SIZE_PITOP, DECIMAL_PITOP), // Code Etat de la Ligne
      new AS400Text(SIZE_PIERL), // Code ERL "C"
      new AS400Text(SIZE_PIART), // Code Article
      new AS400ZonedDecimal(SIZE_PIQTE, DECIMAL_PIQTE), // Quantité Commandée
      new AS400ZonedDecimal(SIZE_PIKSV, DECIMAL_PIKSV), // Nbre d'US pour 1 UV
      new AS400ZonedDecimal(SIZE_PIPVB, DECIMAL_PIPVB), // Prix de Vente de Base
      new AS400ZonedDecimal(SIZE_PIREM1, DECIMAL_PIREM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_PIREM2, DECIMAL_PIREM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_PIREM3, DECIMAL_PIREM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_PIREM4, DECIMAL_PIREM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_PIREM5, DECIMAL_PIREM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_PIREM6, DECIMAL_PIREM6), // % Remise 6
      new AS400Text(SIZE_PITRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_PIBRL), // Base remise ligne, 1=Montant
      new AS400ZonedDecimal(SIZE_PIPVN, DECIMAL_PIPVN), // Prix de Vente Net
      new AS400ZonedDecimal(SIZE_PIPVC, DECIMAL_PIPVC), // Prix de Vente Calculé
      new AS400ZonedDecimal(SIZE_PIMHT, DECIMAL_PIMHT), // Montant Hors Taxes
      new AS400Text(SIZE_PIUNV), // Code Unité de Vente
      new AS400ZonedDecimal(SIZE_PICND, DECIMAL_PICND), // Conditionnement
      new AS400Text(SIZE_PIMAG), // Magasin
      new AS400ZonedDecimal(SIZE_PIQTP, DECIMAL_PIQTP), // Quantité en Pièces
      new AS400Text(SIZE_PILB), // Libellé
      new AS400ZonedDecimal(SIZE_PITT, DECIMAL_PITT), // Top colonne tarif saisie
      new AS400ZonedDecimal(SIZE_PITAR, DECIMAL_PITAR), // Numéro colonne tarif
      new AS400ZonedDecimal(SIZE_PITB, DECIMAL_PITB), // Top prix de base saisi
      new AS400ZonedDecimal(SIZE_PITN, DECIMAL_PITN), // Top prix net saisi
      new AS400ZonedDecimal(SIZE_PITR, DECIMAL_PITR), // Top remise saisie
      new AS400ZonedDecimal(SIZE_PIQT1, DECIMAL_PIQT1), // A1TSP<>" " longueur
      new AS400ZonedDecimal(SIZE_PIQT2, DECIMAL_PIQT2), // A1TSP<>" " largeur
      new AS400ZonedDecimal(SIZE_PIQT3, DECIMAL_PIQT3), // A1TSP<>" " hauteur
      new AS400ZonedDecimal(SIZE_PINBR, DECIMAL_PINBR), // A1TSP<>" " nombre
      new AS400ZonedDecimal(SIZE_PIPRV, DECIMAL_PIPRV), // Prix de revient
      new AS400ZonedDecimal(SIZE_PIIN6, DECIMAL_PIIN6), // Top prix de revient saisi
      new AS400ZonedDecimal(SIZE_PICOL, DECIMAL_PICOL), // Collectif fournisseur
      new AS400ZonedDecimal(SIZE_PIFRS, DECIMAL_PIFRS), // Numéro fournisseur
      new AS400ZonedDecimal(SIZE_PIFRE, DECIMAL_PIFRE), // Lien FRS / FRE
      new AS400Text(SIZE_PIIN2), // Ind. Type de gratuit
      new AS400Text(SIZE_PIUNL), // Unité conditionnement vente
      new AS400ZonedDecimal(SIZE_PISER, DECIMAL_PISER), // Top n° de serie ou lot
      new AS400Text(SIZE_PIIN18), // Origine du prix
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, pietb, picod, pinum, pisuf, pinli, pitop, pierl, piart, piqte, piksv, pipvb, pirem1, pirem2, pirem3, pirem4,
          pirem5, pirem6, pitrl, pibrl, pipvn, pipvc, pimht, piunv, picnd, pimag, piqtp, pilb, pitt, pitar, pitb, pitn, pitr, piqt1,
          piqt2, piqt3, pinbr, piprv, piin6, picol, pifrs, pifre, piin2, piunl, piser, piin18, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    picod = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinli = (BigDecimal) output[5];
    pitop = (BigDecimal) output[6];
    pierl = (String) output[7];
    piart = (String) output[8];
    piqte = (BigDecimal) output[9];
    piksv = (BigDecimal) output[10];
    pipvb = (BigDecimal) output[11];
    pirem1 = (BigDecimal) output[12];
    pirem2 = (BigDecimal) output[13];
    pirem3 = (BigDecimal) output[14];
    pirem4 = (BigDecimal) output[15];
    pirem5 = (BigDecimal) output[16];
    pirem6 = (BigDecimal) output[17];
    pitrl = (String) output[18];
    pibrl = (String) output[19];
    pipvn = (BigDecimal) output[20];
    pipvc = (BigDecimal) output[21];
    pimht = (BigDecimal) output[22];
    piunv = (String) output[23];
    picnd = (BigDecimal) output[24];
    pimag = (String) output[25];
    piqtp = (BigDecimal) output[26];
    pilb = (String) output[27];
    pitt = (BigDecimal) output[28];
    pitar = (BigDecimal) output[29];
    pitb = (BigDecimal) output[30];
    pitn = (BigDecimal) output[31];
    pitr = (BigDecimal) output[32];
    piqt1 = (BigDecimal) output[33];
    piqt2 = (BigDecimal) output[34];
    piqt3 = (BigDecimal) output[35];
    pinbr = (BigDecimal) output[36];
    piprv = (BigDecimal) output[37];
    piin6 = (BigDecimal) output[38];
    picol = (BigDecimal) output[39];
    pifrs = (BigDecimal) output[40];
    pifre = (BigDecimal) output[41];
    piin2 = (String) output[42];
    piunl = (String) output[43];
    piser = (BigDecimal) output[44];
    piin18 = (String) output[45];
    piarr = (String) output[46];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPitop(BigDecimal pPitop) {
    if (pPitop == null) {
      return;
    }
    pitop = pPitop.setScale(DECIMAL_PITOP, RoundingMode.HALF_UP);
  }
  
  public void setPitop(Integer pPitop) {
    if (pPitop == null) {
      return;
    }
    pitop = BigDecimal.valueOf(pPitop);
  }
  
  public Integer getPitop() {
    return pitop.intValue();
  }
  
  public void setPierl(Character pPierl) {
    if (pPierl == null) {
      return;
    }
    pierl = String.valueOf(pPierl);
  }
  
  public Character getPierl() {
    return pierl.charAt(0);
  }
  
  public void setPiart(String pPiart) {
    if (pPiart == null) {
      return;
    }
    piart = pPiart;
  }
  
  public String getPiart() {
    return piart;
  }
  
  public void setPiqte(BigDecimal pPiqte) {
    if (pPiqte == null) {
      return;
    }
    piqte = pPiqte.setScale(DECIMAL_PIQTE, RoundingMode.HALF_UP);
  }
  
  public void setPiqte(Double pPiqte) {
    if (pPiqte == null) {
      return;
    }
    piqte = BigDecimal.valueOf(pPiqte).setScale(DECIMAL_PIQTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqte() {
    return piqte.setScale(DECIMAL_PIQTE, RoundingMode.HALF_UP);
  }
  
  public void setPiksv(BigDecimal pPiksv) {
    if (pPiksv == null) {
      return;
    }
    piksv = pPiksv.setScale(DECIMAL_PIKSV, RoundingMode.HALF_UP);
  }
  
  public void setPiksv(Double pPiksv) {
    if (pPiksv == null) {
      return;
    }
    piksv = BigDecimal.valueOf(pPiksv).setScale(DECIMAL_PIKSV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiksv() {
    return piksv.setScale(DECIMAL_PIKSV, RoundingMode.HALF_UP);
  }
  
  public void setPipvb(BigDecimal pPipvb) {
    if (pPipvb == null) {
      return;
    }
    pipvb = pPipvb.setScale(DECIMAL_PIPVB, RoundingMode.HALF_UP);
  }
  
  public void setPipvb(Double pPipvb) {
    if (pPipvb == null) {
      return;
    }
    pipvb = BigDecimal.valueOf(pPipvb).setScale(DECIMAL_PIPVB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipvb() {
    return pipvb.setScale(DECIMAL_PIPVB, RoundingMode.HALF_UP);
  }
  
  public void setPirem1(BigDecimal pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = pPirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem1(Double pPirem1) {
    if (pPirem1 == null) {
      return;
    }
    pirem1 = BigDecimal.valueOf(pPirem1).setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem1() {
    return pirem1.setScale(DECIMAL_PIREM1, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(BigDecimal pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = pPirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem2(Double pPirem2) {
    if (pPirem2 == null) {
      return;
    }
    pirem2 = BigDecimal.valueOf(pPirem2).setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem2() {
    return pirem2.setScale(DECIMAL_PIREM2, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(BigDecimal pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = pPirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirem3(Double pPirem3) {
    if (pPirem3 == null) {
      return;
    }
    pirem3 = BigDecimal.valueOf(pPirem3).setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem3() {
    return pirem3.setScale(DECIMAL_PIREM3, RoundingMode.HALF_UP);
  }
  
  public void setPirem4(BigDecimal pPirem4) {
    if (pPirem4 == null) {
      return;
    }
    pirem4 = pPirem4.setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public void setPirem4(Double pPirem4) {
    if (pPirem4 == null) {
      return;
    }
    pirem4 = BigDecimal.valueOf(pPirem4).setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem4() {
    return pirem4.setScale(DECIMAL_PIREM4, RoundingMode.HALF_UP);
  }
  
  public void setPirem5(BigDecimal pPirem5) {
    if (pPirem5 == null) {
      return;
    }
    pirem5 = pPirem5.setScale(DECIMAL_PIREM5, RoundingMode.HALF_UP);
  }
  
  public void setPirem5(Double pPirem5) {
    if (pPirem5 == null) {
      return;
    }
    pirem5 = BigDecimal.valueOf(pPirem5).setScale(DECIMAL_PIREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem5() {
    return pirem5.setScale(DECIMAL_PIREM5, RoundingMode.HALF_UP);
  }
  
  public void setPirem6(BigDecimal pPirem6) {
    if (pPirem6 == null) {
      return;
    }
    pirem6 = pPirem6.setScale(DECIMAL_PIREM6, RoundingMode.HALF_UP);
  }
  
  public void setPirem6(Double pPirem6) {
    if (pPirem6 == null) {
      return;
    }
    pirem6 = BigDecimal.valueOf(pPirem6).setScale(DECIMAL_PIREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPirem6() {
    return pirem6.setScale(DECIMAL_PIREM6, RoundingMode.HALF_UP);
  }
  
  public void setPitrl(Character pPitrl) {
    if (pPitrl == null) {
      return;
    }
    pitrl = String.valueOf(pPitrl);
  }
  
  public Character getPitrl() {
    return pitrl.charAt(0);
  }
  
  public void setPibrl(Character pPibrl) {
    if (pPibrl == null) {
      return;
    }
    pibrl = String.valueOf(pPibrl);
  }
  
  public Character getPibrl() {
    return pibrl.charAt(0);
  }
  
  public void setPipvn(BigDecimal pPipvn) {
    if (pPipvn == null) {
      return;
    }
    pipvn = pPipvn.setScale(DECIMAL_PIPVN, RoundingMode.HALF_UP);
  }
  
  public void setPipvn(Double pPipvn) {
    if (pPipvn == null) {
      return;
    }
    pipvn = BigDecimal.valueOf(pPipvn).setScale(DECIMAL_PIPVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipvn() {
    return pipvn.setScale(DECIMAL_PIPVN, RoundingMode.HALF_UP);
  }
  
  public void setPipvc(BigDecimal pPipvc) {
    if (pPipvc == null) {
      return;
    }
    pipvc = pPipvc.setScale(DECIMAL_PIPVC, RoundingMode.HALF_UP);
  }
  
  public void setPipvc(Double pPipvc) {
    if (pPipvc == null) {
      return;
    }
    pipvc = BigDecimal.valueOf(pPipvc).setScale(DECIMAL_PIPVC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPipvc() {
    return pipvc.setScale(DECIMAL_PIPVC, RoundingMode.HALF_UP);
  }
  
  public void setPimht(BigDecimal pPimht) {
    if (pPimht == null) {
      return;
    }
    pimht = pPimht.setScale(DECIMAL_PIMHT, RoundingMode.HALF_UP);
  }
  
  public void setPimht(Double pPimht) {
    if (pPimht == null) {
      return;
    }
    pimht = BigDecimal.valueOf(pPimht).setScale(DECIMAL_PIMHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPimht() {
    return pimht.setScale(DECIMAL_PIMHT, RoundingMode.HALF_UP);
  }
  
  public void setPiunv(String pPiunv) {
    if (pPiunv == null) {
      return;
    }
    piunv = pPiunv;
  }
  
  public String getPiunv() {
    return piunv;
  }
  
  public void setPicnd(BigDecimal pPicnd) {
    if (pPicnd == null) {
      return;
    }
    picnd = pPicnd.setScale(DECIMAL_PICND, RoundingMode.HALF_UP);
  }
  
  public void setPicnd(Double pPicnd) {
    if (pPicnd == null) {
      return;
    }
    picnd = BigDecimal.valueOf(pPicnd).setScale(DECIMAL_PICND, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPicnd() {
    return picnd.setScale(DECIMAL_PICND, RoundingMode.HALF_UP);
  }
  
  public void setPimag(String pPimag) {
    if (pPimag == null) {
      return;
    }
    pimag = pPimag;
  }
  
  public String getPimag() {
    return pimag;
  }
  
  public void setPiqtp(BigDecimal pPiqtp) {
    if (pPiqtp == null) {
      return;
    }
    piqtp = pPiqtp.setScale(DECIMAL_PIQTP, RoundingMode.HALF_UP);
  }
  
  public void setPiqtp(Double pPiqtp) {
    if (pPiqtp == null) {
      return;
    }
    piqtp = BigDecimal.valueOf(pPiqtp).setScale(DECIMAL_PIQTP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqtp() {
    return piqtp.setScale(DECIMAL_PIQTP, RoundingMode.HALF_UP);
  }
  
  public void setPilb(String pPilb) {
    if (pPilb == null) {
      return;
    }
    pilb = pPilb;
  }
  
  public String getPilb() {
    return pilb;
  }
  
  public void setPitt(BigDecimal pPitt) {
    if (pPitt == null) {
      return;
    }
    pitt = pPitt.setScale(DECIMAL_PITT, RoundingMode.HALF_UP);
  }
  
  public void setPitt(Integer pPitt) {
    if (pPitt == null) {
      return;
    }
    pitt = BigDecimal.valueOf(pPitt);
  }
  
  public Integer getPitt() {
    return pitt.intValue();
  }
  
  public void setPitar(BigDecimal pPitar) {
    if (pPitar == null) {
      return;
    }
    pitar = pPitar.setScale(DECIMAL_PITAR, RoundingMode.HALF_UP);
  }
  
  public void setPitar(Integer pPitar) {
    if (pPitar == null) {
      return;
    }
    pitar = BigDecimal.valueOf(pPitar);
  }
  
  public Integer getPitar() {
    return pitar.intValue();
  }
  
  public void setPitb(BigDecimal pPitb) {
    if (pPitb == null) {
      return;
    }
    pitb = pPitb.setScale(DECIMAL_PITB, RoundingMode.HALF_UP);
  }
  
  public void setPitb(Integer pPitb) {
    if (pPitb == null) {
      return;
    }
    pitb = BigDecimal.valueOf(pPitb);
  }
  
  public Integer getPitb() {
    return pitb.intValue();
  }
  
  public void setPitn(BigDecimal pPitn) {
    if (pPitn == null) {
      return;
    }
    pitn = pPitn.setScale(DECIMAL_PITN, RoundingMode.HALF_UP);
  }
  
  public void setPitn(Integer pPitn) {
    if (pPitn == null) {
      return;
    }
    pitn = BigDecimal.valueOf(pPitn);
  }
  
  public Integer getPitn() {
    return pitn.intValue();
  }
  
  public void setPitr(BigDecimal pPitr) {
    if (pPitr == null) {
      return;
    }
    pitr = pPitr.setScale(DECIMAL_PITR, RoundingMode.HALF_UP);
  }
  
  public void setPitr(Integer pPitr) {
    if (pPitr == null) {
      return;
    }
    pitr = BigDecimal.valueOf(pPitr);
  }
  
  public Integer getPitr() {
    return pitr.intValue();
  }
  
  public void setPiqt1(BigDecimal pPiqt1) {
    if (pPiqt1 == null) {
      return;
    }
    piqt1 = pPiqt1.setScale(DECIMAL_PIQT1, RoundingMode.HALF_UP);
  }
  
  public void setPiqt1(Double pPiqt1) {
    if (pPiqt1 == null) {
      return;
    }
    piqt1 = BigDecimal.valueOf(pPiqt1).setScale(DECIMAL_PIQT1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqt1() {
    return piqt1.setScale(DECIMAL_PIQT1, RoundingMode.HALF_UP);
  }
  
  public void setPiqt2(BigDecimal pPiqt2) {
    if (pPiqt2 == null) {
      return;
    }
    piqt2 = pPiqt2.setScale(DECIMAL_PIQT2, RoundingMode.HALF_UP);
  }
  
  public void setPiqt2(Double pPiqt2) {
    if (pPiqt2 == null) {
      return;
    }
    piqt2 = BigDecimal.valueOf(pPiqt2).setScale(DECIMAL_PIQT2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqt2() {
    return piqt2.setScale(DECIMAL_PIQT2, RoundingMode.HALF_UP);
  }
  
  public void setPiqt3(BigDecimal pPiqt3) {
    if (pPiqt3 == null) {
      return;
    }
    piqt3 = pPiqt3.setScale(DECIMAL_PIQT3, RoundingMode.HALF_UP);
  }
  
  public void setPiqt3(Double pPiqt3) {
    if (pPiqt3 == null) {
      return;
    }
    piqt3 = BigDecimal.valueOf(pPiqt3).setScale(DECIMAL_PIQT3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqt3() {
    return piqt3.setScale(DECIMAL_PIQT3, RoundingMode.HALF_UP);
  }
  
  public void setPinbr(BigDecimal pPinbr) {
    if (pPinbr == null) {
      return;
    }
    pinbr = pPinbr.setScale(DECIMAL_PINBR, RoundingMode.HALF_UP);
  }
  
  public void setPinbr(Integer pPinbr) {
    if (pPinbr == null) {
      return;
    }
    pinbr = BigDecimal.valueOf(pPinbr);
  }
  
  public Integer getPinbr() {
    return pinbr.intValue();
  }
  
  public void setPiprv(BigDecimal pPiprv) {
    if (pPiprv == null) {
      return;
    }
    piprv = pPiprv.setScale(DECIMAL_PIPRV, RoundingMode.HALF_UP);
  }
  
  public void setPiprv(Double pPiprv) {
    if (pPiprv == null) {
      return;
    }
    piprv = BigDecimal.valueOf(pPiprv).setScale(DECIMAL_PIPRV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiprv() {
    return piprv.setScale(DECIMAL_PIPRV, RoundingMode.HALF_UP);
  }
  
  public void setPiin6(BigDecimal pPiin6) {
    if (pPiin6 == null) {
      return;
    }
    piin6 = pPiin6.setScale(DECIMAL_PIIN6, RoundingMode.HALF_UP);
  }
  
  public void setPiin6(Integer pPiin6) {
    if (pPiin6 == null) {
      return;
    }
    piin6 = BigDecimal.valueOf(pPiin6);
  }
  
  public Integer getPiin6() {
    return piin6.intValue();
  }
  
  public void setPicol(BigDecimal pPicol) {
    if (pPicol == null) {
      return;
    }
    picol = pPicol.setScale(DECIMAL_PICOL, RoundingMode.HALF_UP);
  }
  
  public void setPicol(Integer pPicol) {
    if (pPicol == null) {
      return;
    }
    picol = BigDecimal.valueOf(pPicol);
  }
  
  public Integer getPicol() {
    return picol.intValue();
  }
  
  public void setPifrs(BigDecimal pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = pPifrs.setScale(DECIMAL_PIFRS, RoundingMode.HALF_UP);
  }
  
  public void setPifrs(Integer pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = BigDecimal.valueOf(pPifrs);
  }
  
  public Integer getPifrs() {
    return pifrs.intValue();
  }
  
  public void setPifre(BigDecimal pPifre) {
    if (pPifre == null) {
      return;
    }
    pifre = pPifre.setScale(DECIMAL_PIFRE, RoundingMode.HALF_UP);
  }
  
  public void setPifre(Integer pPifre) {
    if (pPifre == null) {
      return;
    }
    pifre = BigDecimal.valueOf(pPifre);
  }
  
  public Integer getPifre() {
    return pifre.intValue();
  }
  
  public void setPiin2(Character pPiin2) {
    if (pPiin2 == null) {
      return;
    }
    piin2 = String.valueOf(pPiin2);
  }
  
  public Character getPiin2() {
    return piin2.charAt(0);
  }
  
  public void setPiunl(String pPiunl) {
    if (pPiunl == null) {
      return;
    }
    piunl = pPiunl;
  }
  
  public String getPiunl() {
    return piunl;
  }
  
  public void setPiser(BigDecimal pPiser) {
    if (pPiser == null) {
      return;
    }
    piser = pPiser.setScale(DECIMAL_PISER, RoundingMode.HALF_UP);
  }
  
  public void setPiser(Integer pPiser) {
    if (pPiser == null) {
      return;
    }
    piser = BigDecimal.valueOf(pPiser);
  }
  
  public Integer getPiser() {
    return piser.intValue();
  }
  
  public void setPiin18(Character pPiin18) {
    if (pPiin18 == null) {
      return;
    }
    piin18 = String.valueOf(pPiin18);
  }
  
  public Character getPiin18() {
    return piin18.charAt(0);
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
