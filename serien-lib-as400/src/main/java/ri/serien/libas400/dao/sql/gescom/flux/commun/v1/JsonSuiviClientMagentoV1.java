/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v1;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.database.record.GenericRecord;

/**
 * suivi encours et depassement client serie N au format Magento
 * @author Déborah
 *
 */
public class JsonSuiviClientMagentoV1 extends JsonEntiteMagento {
  
  private ArrayList<String> listeEncours = null;
  
  /**
   * Constructeur du suivi de l'encours client
   */
  public JsonSuiviClientMagentoV1(FluxMagento record) {
    idFlux = record.getFLIDF();
    versionFlux = record.getFLVER();
    etb = record.getFLETB();
    listeEncours = new ArrayList<String>();
  }
  
  public void MajDonneesClient(ArrayList<GenericRecord> encoursTousLesClientsDB2) {
    if (encoursTousLesClientsDB2 == null) {
      majError("[ClientMagento] majDesInfos() encoursTousLesClientsDB2 à NULL");
      return;
    }
    
    if (encoursTousLesClientsDB2.size() > 0) {
      for (int i = 0; i < encoursTousLesClientsDB2.size(); i++) {
        
        if (encoursTousLesClientsDB2.get(i).getField("DEPASSEMENT") != null
            && !encoursTousLesClientsDB2.get(i).getField("DEPASSEMENT").equals(0)) {
          listeEncours.add(encoursTousLesClientsDB2.get(i).getField("DEPASSEMENT").toString().trim());
        }
        else {
          majError("[ClientMagento] MajDonneesClient() clientDB2 corrompu DEPASSEMENT");
          return;
        }
        if (encoursTousLesClientsDB2.get(i).getField("ENCOURS") != null
            && (!(encoursTousLesClientsDB2.get(i).getField("ENCOURS").equals(0)))) {
          listeEncours.add(encoursTousLesClientsDB2.get(i).getField("ENCOURS").toString().trim());
        }
        else {
          majError("[ClientMagento] MajDonneesClient() clientDB2 corrompu ENCOURS");
          return;
        }
        
      }
    }
  }
  
  public ArrayList<String> getListeEncours() {
    return listeEncours;
  }
  
  public void setListeEncours(ArrayList<String> listeEncours) {
    this.listeEncours = listeEncours;
  }
  
}
