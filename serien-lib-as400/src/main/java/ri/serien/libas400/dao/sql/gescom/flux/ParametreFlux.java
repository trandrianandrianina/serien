/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux;

/**
 * Cette classe décrit un paramètre pour le gestionnaire de flux.
 * Ceux sont les paramètres stockés dans la table PSEMFXPA.
 */
public class ParametreFlux {
  // Variables
  private String PM_CLE = null;
  private String PM_VALEUR = null;
  private String PM_TYPE = null;
  private boolean PM_ACCES = false;
  private String PM_DESCR = null;
  
  /**
   * Constructeur.
   */
  public ParametreFlux(String pCle, String pValeur, String pType, boolean pAccessible, String pDescription) {
    PM_CLE = pCle;
    PM_VALEUR = pValeur;
    PM_TYPE = pType;
    PM_ACCES = pAccessible;
    PM_DESCR = pDescription;
  }
  
  // -- Accesseurs
  
  public String getPM_CLE() {
    return PM_CLE;
  }
  
  public void setPM_CLE(String pM_CLE) {
    PM_CLE = pM_CLE;
  }
  
  public String getPM_VALEUR() {
    return PM_VALEUR;
  }
  
  public void setPM_VALEUR(String pM_VALEUR) {
    PM_VALEUR = pM_VALEUR;
  }
  
  public String getPM_TYPE() {
    return PM_TYPE;
  }
  
  public void setPM_TYPE(String pM_TYPE) {
    PM_TYPE = pM_TYPE;
  }
  
  public boolean isPM_ACCES() {
    return PM_ACCES;
  }
  
  public void setPM_ACCES(boolean pM_ACCES) {
    PM_ACCES = pM_ACCES;
  }
  
  public String getPM_DESCR() {
    return PM_DESCR;
  }
  
  public void setPM_DESCR(String pM_DESCR) {
    PM_DESCR = pM_DESCR;
  }
  
}
