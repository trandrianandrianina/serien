/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0026o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POEFF = 13;
  public static final int DECIMAL_POEFF = 2;
  public static final int SIZE_POPCO = 13;
  public static final int DECIMAL_POPCO = 0;
  public static final int SIZE_POACC = 10;
  public static final int DECIMAL_POACC = 2;
  public static final int SIZE_POPOR = 10;
  public static final int DECIMAL_POPOR = 2;
  public static final int SIZE_POESC = 10;
  public static final int DECIMAL_POESC = 2;
  public static final int SIZE_POENC = 10;
  public static final int DECIMAL_POENC = 2;
  public static final int SIZE_POTIM = 17;
  public static final int DECIMAL_POTIM = 0;
  public static final int SIZE_POLIM = 6;
  public static final int DECIMAL_POLIM = 0;
  public static final int SIZE_PODIM = 8;
  public static final int SIZE_PONIM = 2;
  public static final int DECIMAL_PONIM = 0;
  public static final int SIZE_POAFM = 17;
  public static final int DECIMAL_POAFM = 0;
  public static final int SIZE_POTAF = 1;
  public static final int SIZE_POECD = 10;
  public static final int DECIMAL_POECD = 2;
  public static final int SIZE_POSOC = 3;
  public static final int SIZE_PONCG = 6;
  public static final int DECIMAL_PONCG = 0;
  public static final int SIZE_POCDE = 9;
  public static final int DECIMAL_POCDE = 2;
  public static final int SIZE_POEXP = 9;
  public static final int DECIMAL_POEXP = 2;
  public static final int SIZE_POFAC = 9;
  public static final int DECIMAL_POFAC = 2;
  public static final int SIZE_POVAE = 7;
  public static final int DECIMAL_POVAE = 0;
  public static final int SIZE_POPLF = 7;
  public static final int DECIMAL_POPLF = 0;
  public static final int SIZE_POPLF2 = 7;
  public static final int DECIMAL_POPLF2 = 0;
  public static final int SIZE_PODPL = 7;
  public static final int DECIMAL_PODPL = 0;
  public static final int SIZE_POPLMX = 7;
  public static final int DECIMAL_POPLMX = 0;
  public static final int SIZE_PODEPA = 7;
  public static final int DECIMAL_PODEPA = 0;
  public static final int SIZE_POPOS = 7;
  public static final int DECIMAL_POPOS = 0;
  public static final int SIZE_POER01 = 1;
  public static final int DECIMAL_POER01 = 0;
  public static final int SIZE_POER01A = 50;
  public static final int SIZE_POER02 = 1;
  public static final int DECIMAL_POER02 = 0;
  public static final int SIZE_POER02A = 50;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 325;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POEFF = 1;
  public static final int VAR_POPCO = 2;
  public static final int VAR_POACC = 3;
  public static final int VAR_POPOR = 4;
  public static final int VAR_POESC = 5;
  public static final int VAR_POENC = 6;
  public static final int VAR_POTIM = 7;
  public static final int VAR_POLIM = 8;
  public static final int VAR_PODIM = 9;
  public static final int VAR_PONIM = 10;
  public static final int VAR_POAFM = 11;
  public static final int VAR_POTAF = 12;
  public static final int VAR_POECD = 13;
  public static final int VAR_POSOC = 14;
  public static final int VAR_PONCG = 15;
  public static final int VAR_POCDE = 16;
  public static final int VAR_POEXP = 17;
  public static final int VAR_POFAC = 18;
  public static final int VAR_POVAE = 19;
  public static final int VAR_POPLF = 20;
  public static final int VAR_POPLF2 = 21;
  public static final int VAR_PODPL = 22;
  public static final int VAR_POPLMX = 23;
  public static final int VAR_PODEPA = 24;
  public static final int VAR_POPOS = 25;
  public static final int VAR_POER01 = 26;
  public static final int VAR_POER01A = 27;
  public static final int VAR_POER02 = 28;
  public static final int VAR_POER02A = 29;
  public static final int VAR_POARR = 30;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal poeff = BigDecimal.ZERO; // Effets
  private BigDecimal popco = BigDecimal.ZERO; // Position comptable
  private BigDecimal poacc = BigDecimal.ZERO; // Total accompte
  private BigDecimal popor = BigDecimal.ZERO; // Total portefeuille
  private BigDecimal poesc = BigDecimal.ZERO; //
  private BigDecimal poenc = BigDecimal.ZERO; //
  private BigDecimal potim = BigDecimal.ZERO; // Total impayés
  private BigDecimal polim = BigDecimal.ZERO; //
  private String podim = ""; // Date impayé
  private BigDecimal ponim = BigDecimal.ZERO; // Nombre d"impayés
  private BigDecimal poafm = BigDecimal.ZERO; // Total affacturage
  private String potaf = ""; // Top affacturage
  private BigDecimal poecd = BigDecimal.ZERO; // Total échéances dépassées
  private String posoc = ""; // Code établissement comptable
  private BigDecimal poncg = BigDecimal.ZERO; // Numéro compte général
  private BigDecimal pocde = BigDecimal.ZERO; // Encours / Commande
  private BigDecimal poexp = BigDecimal.ZERO; // Encours / Expédié
  private BigDecimal pofac = BigDecimal.ZERO; // Encours / Facturé
  private BigDecimal povae = BigDecimal.ZERO; // Vente assimilé export
  private BigDecimal poplf = BigDecimal.ZERO; // Plafond encours autorisé
  private BigDecimal poplf2 = BigDecimal.ZERO; // Sur-Plafond encours autorisé
  private BigDecimal podpl = BigDecimal.ZERO; // Date Limite pour Sur-plafond
  private BigDecimal poplmx = BigDecimal.ZERO; // Plafond maxi en déblocage
  private BigDecimal podepa = BigDecimal.ZERO; // Dépassement
  private BigDecimal popos = BigDecimal.ZERO; // Position Encours
  private BigDecimal poer01 = BigDecimal.ZERO; // Indicateur
  private String poer01a = ""; // Texte
  private BigDecimal poer02 = BigDecimal.ZERO; // Indicateur
  private String poer02a = ""; // Texte
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_POEFF, DECIMAL_POEFF), // Effets
      new AS400ZonedDecimal(SIZE_POPCO, DECIMAL_POPCO), // Position comptable
      new AS400ZonedDecimal(SIZE_POACC, DECIMAL_POACC), // Total accompte
      new AS400ZonedDecimal(SIZE_POPOR, DECIMAL_POPOR), // Total portefeuille
      new AS400ZonedDecimal(SIZE_POESC, DECIMAL_POESC), //
      new AS400ZonedDecimal(SIZE_POENC, DECIMAL_POENC), //
      new AS400ZonedDecimal(SIZE_POTIM, DECIMAL_POTIM), // Total impayés
      new AS400ZonedDecimal(SIZE_POLIM, DECIMAL_POLIM), //
      new AS400Text(SIZE_PODIM), // Date impayé
      new AS400PackedDecimal(SIZE_PONIM, DECIMAL_PONIM), // Nombre d"impayés
      new AS400ZonedDecimal(SIZE_POAFM, DECIMAL_POAFM), // Total affacturage
      new AS400Text(SIZE_POTAF), // Top affacturage
      new AS400ZonedDecimal(SIZE_POECD, DECIMAL_POECD), // Total échéances dépassées
      new AS400Text(SIZE_POSOC), // Code établissement comptable
      new AS400ZonedDecimal(SIZE_PONCG, DECIMAL_PONCG), // Numéro compte général
      new AS400PackedDecimal(SIZE_POCDE, DECIMAL_POCDE), // Encours / Commande
      new AS400PackedDecimal(SIZE_POEXP, DECIMAL_POEXP), // Encours / Expédié
      new AS400PackedDecimal(SIZE_POFAC, DECIMAL_POFAC), // Encours / Facturé
      new AS400PackedDecimal(SIZE_POVAE, DECIMAL_POVAE), // Vente assimilé export
      new AS400PackedDecimal(SIZE_POPLF, DECIMAL_POPLF), // Plafond encours autorisé
      new AS400PackedDecimal(SIZE_POPLF2, DECIMAL_POPLF2), // Sur-Plafond encours autorisé
      new AS400PackedDecimal(SIZE_PODPL, DECIMAL_PODPL), // Date Limite pour Sur-plafond
      new AS400PackedDecimal(SIZE_POPLMX, DECIMAL_POPLMX), // Plafond maxi en déblocage
      new AS400PackedDecimal(SIZE_PODEPA, DECIMAL_PODEPA), // Dépassement
      new AS400PackedDecimal(SIZE_POPOS, DECIMAL_POPOS), // Position Encours
      new AS400ZonedDecimal(SIZE_POER01, DECIMAL_POER01), // Indicateur
      new AS400Text(SIZE_POER01A), // Texte
      new AS400ZonedDecimal(SIZE_POER02, DECIMAL_POER02), // Indicateur
      new AS400Text(SIZE_POER02A), // Texte
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, poeff, popco, poacc, popor, poesc, poenc, potim, polim, podim, ponim, poafm, potaf, poecd, posoc, poncg,
          pocde, poexp, pofac, povae, poplf, poplf2, podpl, poplmx, podepa, popos, poer01, poer01a, poer02, poer02a, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    poeff = (BigDecimal) output[1];
    popco = (BigDecimal) output[2];
    poacc = (BigDecimal) output[3];
    popor = (BigDecimal) output[4];
    poesc = (BigDecimal) output[5];
    poenc = (BigDecimal) output[6];
    potim = (BigDecimal) output[7];
    polim = (BigDecimal) output[8];
    podim = (String) output[9];
    ponim = (BigDecimal) output[10];
    poafm = (BigDecimal) output[11];
    potaf = (String) output[12];
    poecd = (BigDecimal) output[13];
    posoc = (String) output[14];
    poncg = (BigDecimal) output[15];
    pocde = (BigDecimal) output[16];
    poexp = (BigDecimal) output[17];
    pofac = (BigDecimal) output[18];
    povae = (BigDecimal) output[19];
    poplf = (BigDecimal) output[20];
    poplf2 = (BigDecimal) output[21];
    podpl = (BigDecimal) output[22];
    poplmx = (BigDecimal) output[23];
    podepa = (BigDecimal) output[24];
    popos = (BigDecimal) output[25];
    poer01 = (BigDecimal) output[26];
    poer01a = (String) output[27];
    poer02 = (BigDecimal) output[28];
    poer02a = (String) output[29];
    poarr = (String) output[30];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPoeff(BigDecimal pPoeff) {
    if (pPoeff == null) {
      return;
    }
    poeff = pPoeff.setScale(DECIMAL_POEFF, RoundingMode.HALF_UP);
  }
  
  public void setPoeff(Double pPoeff) {
    if (pPoeff == null) {
      return;
    }
    poeff = BigDecimal.valueOf(pPoeff).setScale(DECIMAL_POEFF, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoeff() {
    return poeff;
  }
  
  public void setPopco(BigDecimal pPopco) {
    if (pPopco == null) {
      return;
    }
    popco = pPopco.setScale(DECIMAL_POPCO, RoundingMode.HALF_UP);
  }
  
  public void setPopco(Integer pPopco) {
    if (pPopco == null) {
      return;
    }
    popco = BigDecimal.valueOf(pPopco);
  }
  
  public Integer getPopco() {
    return popco.intValue();
  }
  
  public void setPoacc(BigDecimal pPoacc) {
    if (pPoacc == null) {
      return;
    }
    poacc = pPoacc.setScale(DECIMAL_POACC, RoundingMode.HALF_UP);
  }
  
  public void setPoacc(Double pPoacc) {
    if (pPoacc == null) {
      return;
    }
    poacc = BigDecimal.valueOf(pPoacc).setScale(DECIMAL_POACC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoacc() {
    return poacc;
  }
  
  public void setPopor(BigDecimal pPopor) {
    if (pPopor == null) {
      return;
    }
    popor = pPopor.setScale(DECIMAL_POPOR, RoundingMode.HALF_UP);
  }
  
  public void setPopor(Double pPopor) {
    if (pPopor == null) {
      return;
    }
    popor = BigDecimal.valueOf(pPopor).setScale(DECIMAL_POPOR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPopor() {
    return popor;
  }
  
  public void setPoesc(BigDecimal pPoesc) {
    if (pPoesc == null) {
      return;
    }
    poesc = pPoesc.setScale(DECIMAL_POESC, RoundingMode.HALF_UP);
  }
  
  public void setPoesc(Double pPoesc) {
    if (pPoesc == null) {
      return;
    }
    poesc = BigDecimal.valueOf(pPoesc).setScale(DECIMAL_POESC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoesc() {
    return poesc;
  }
  
  public void setPoenc(BigDecimal pPoenc) {
    if (pPoenc == null) {
      return;
    }
    poenc = pPoenc.setScale(DECIMAL_POENC, RoundingMode.HALF_UP);
  }
  
  public void setPoenc(Double pPoenc) {
    if (pPoenc == null) {
      return;
    }
    poenc = BigDecimal.valueOf(pPoenc).setScale(DECIMAL_POENC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoenc() {
    return poenc;
  }
  
  public void setPotim(BigDecimal pPotim) {
    if (pPotim == null) {
      return;
    }
    potim = pPotim.setScale(DECIMAL_POTIM, RoundingMode.HALF_UP);
  }
  
  public void setPotim(Integer pPotim) {
    if (pPotim == null) {
      return;
    }
    potim = BigDecimal.valueOf(pPotim);
  }
  
  public Integer getPotim() {
    return potim.intValue();
  }
  
  public void setPolim(BigDecimal pPolim) {
    if (pPolim == null) {
      return;
    }
    polim = pPolim.setScale(DECIMAL_POLIM, RoundingMode.HALF_UP);
  }
  
  public void setPolim(Integer pPolim) {
    if (pPolim == null) {
      return;
    }
    polim = BigDecimal.valueOf(pPolim);
  }
  
  public Integer getPolim() {
    return polim.intValue();
  }
  
  public void setPodim(String pPodim) {
    if (pPodim == null) {
      return;
    }
    podim = pPodim;
  }
  
  public String getPodim() {
    return podim;
  }
  
  public void setPonim(BigDecimal pPonim) {
    if (pPonim == null) {
      return;
    }
    ponim = pPonim.setScale(DECIMAL_PONIM, RoundingMode.HALF_UP);
  }
  
  public void setPonim(Integer pPonim) {
    if (pPonim == null) {
      return;
    }
    ponim = BigDecimal.valueOf(pPonim);
  }
  
  public Integer getPonim() {
    return ponim.intValue();
  }
  
  public void setPoafm(BigDecimal pPoafm) {
    if (pPoafm == null) {
      return;
    }
    poafm = pPoafm.setScale(DECIMAL_POAFM, RoundingMode.HALF_UP);
  }
  
  public void setPoafm(Integer pPoafm) {
    if (pPoafm == null) {
      return;
    }
    poafm = BigDecimal.valueOf(pPoafm);
  }
  
  public Integer getPoafm() {
    return poafm.intValue();
  }
  
  public void setPotaf(Character pPotaf) {
    if (pPotaf == null) {
      return;
    }
    potaf = String.valueOf(pPotaf);
  }
  
  public Character getPotaf() {
    return potaf.charAt(0);
  }
  
  public void setPoecd(BigDecimal pPoecd) {
    if (pPoecd == null) {
      return;
    }
    poecd = pPoecd.setScale(DECIMAL_POECD, RoundingMode.HALF_UP);
  }
  
  public void setPoecd(Double pPoecd) {
    if (pPoecd == null) {
      return;
    }
    poecd = BigDecimal.valueOf(pPoecd).setScale(DECIMAL_POECD, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoecd() {
    return poecd;
  }
  
  public void setPosoc(String pPosoc) {
    if (pPosoc == null) {
      return;
    }
    posoc = pPosoc;
  }
  
  public String getPosoc() {
    return posoc;
  }
  
  public void setPoncg(BigDecimal pPoncg) {
    if (pPoncg == null) {
      return;
    }
    poncg = pPoncg.setScale(DECIMAL_PONCG, RoundingMode.HALF_UP);
  }
  
  public void setPoncg(Integer pPoncg) {
    if (pPoncg == null) {
      return;
    }
    poncg = BigDecimal.valueOf(pPoncg);
  }
  
  public Integer getPoncg() {
    return poncg.intValue();
  }
  
  public void setPocde(BigDecimal pPocde) {
    if (pPocde == null) {
      return;
    }
    pocde = pPocde.setScale(DECIMAL_POCDE, RoundingMode.HALF_UP);
  }
  
  public void setPocde(Double pPocde) {
    if (pPocde == null) {
      return;
    }
    pocde = BigDecimal.valueOf(pPocde).setScale(DECIMAL_POCDE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPocde() {
    return pocde;
  }
  
  public void setPoexp(BigDecimal pPoexp) {
    if (pPoexp == null) {
      return;
    }
    poexp = pPoexp.setScale(DECIMAL_POEXP, RoundingMode.HALF_UP);
  }
  
  public void setPoexp(Double pPoexp) {
    if (pPoexp == null) {
      return;
    }
    poexp = BigDecimal.valueOf(pPoexp).setScale(DECIMAL_POEXP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoexp() {
    return poexp;
  }
  
  public void setPofac(BigDecimal pPofac) {
    if (pPofac == null) {
      return;
    }
    pofac = pPofac.setScale(DECIMAL_POFAC, RoundingMode.HALF_UP);
  }
  
  public void setPofac(Double pPofac) {
    if (pPofac == null) {
      return;
    }
    pofac = BigDecimal.valueOf(pPofac).setScale(DECIMAL_POFAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPofac() {
    return pofac;
  }
  
  public void setPovae(BigDecimal pPovae) {
    if (pPovae == null) {
      return;
    }
    povae = pPovae.setScale(DECIMAL_POVAE, RoundingMode.HALF_UP);
  }
  
  public void setPovae(Integer pPovae) {
    if (pPovae == null) {
      return;
    }
    povae = BigDecimal.valueOf(pPovae);
  }
  
  public void setPovae(Date pPovae) {
    if (pPovae == null) {
      return;
    }
    povae = BigDecimal.valueOf(ConvertDate.dateToDb2(pPovae));
  }
  
  public Integer getPovae() {
    return povae.intValue();
  }
  
  public Date getPovaeConvertiEnDate() {
    return ConvertDate.db2ToDate(povae.intValue(), null);
  }
  
  public void setPoplf(BigDecimal pPoplf) {
    if (pPoplf == null) {
      return;
    }
    poplf = pPoplf.setScale(DECIMAL_POPLF, RoundingMode.HALF_UP);
  }
  
  public void setPoplf(Integer pPoplf) {
    if (pPoplf == null) {
      return;
    }
    poplf = BigDecimal.valueOf(pPoplf);
  }
  
  public void setPoplf(Date pPoplf) {
    if (pPoplf == null) {
      return;
    }
    poplf = BigDecimal.valueOf(ConvertDate.dateToDb2(pPoplf));
  }
  
  public Integer getPoplf() {
    return poplf.intValue();
  }
  
  public Date getPoplfConvertiEnDate() {
    return ConvertDate.db2ToDate(poplf.intValue(), null);
  }
  
  public void setPoplf2(BigDecimal pPoplf2) {
    if (pPoplf2 == null) {
      return;
    }
    poplf2 = pPoplf2.setScale(DECIMAL_POPLF2, RoundingMode.HALF_UP);
  }
  
  public void setPoplf2(Integer pPoplf2) {
    if (pPoplf2 == null) {
      return;
    }
    poplf2 = BigDecimal.valueOf(pPoplf2);
  }
  
  public void setPoplf2(Date pPoplf2) {
    if (pPoplf2 == null) {
      return;
    }
    poplf2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pPoplf2));
  }
  
  public Integer getPoplf2() {
    return poplf2.intValue();
  }
  
  public Date getPoplf2ConvertiEnDate() {
    return ConvertDate.db2ToDate(poplf2.intValue(), null);
  }
  
  public void setPodpl(BigDecimal pPodpl) {
    if (pPodpl == null) {
      return;
    }
    podpl = pPodpl.setScale(DECIMAL_PODPL, RoundingMode.HALF_UP);
  }
  
  public void setPodpl(Integer pPodpl) {
    if (pPodpl == null) {
      return;
    }
    podpl = BigDecimal.valueOf(pPodpl);
  }
  
  public void setPodpl(Date pPodpl) {
    if (pPodpl == null) {
      return;
    }
    podpl = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodpl));
  }
  
  public Integer getPodpl() {
    return podpl.intValue();
  }
  
  public Date getPodplConvertiEnDate() {
    return ConvertDate.db2ToDate(podpl.intValue(), null);
  }
  
  public void setPoplmx(BigDecimal pPoplmx) {
    if (pPoplmx == null) {
      return;
    }
    poplmx = pPoplmx.setScale(DECIMAL_POPLMX, RoundingMode.HALF_UP);
  }
  
  public void setPoplmx(Integer pPoplmx) {
    if (pPoplmx == null) {
      return;
    }
    poplmx = BigDecimal.valueOf(pPoplmx);
  }
  
  public void setPoplmx(Date pPoplmx) {
    if (pPoplmx == null) {
      return;
    }
    poplmx = BigDecimal.valueOf(ConvertDate.dateToDb2(pPoplmx));
  }
  
  public Integer getPoplmx() {
    return poplmx.intValue();
  }
  
  public Date getPoplmxConvertiEnDate() {
    return ConvertDate.db2ToDate(poplmx.intValue(), null);
  }
  
  public void setPodepa(BigDecimal pPodepa) {
    if (pPodepa == null) {
      return;
    }
    podepa = pPodepa.setScale(DECIMAL_PODEPA, RoundingMode.HALF_UP);
  }
  
  public void setPodepa(Integer pPodepa) {
    if (pPodepa == null) {
      return;
    }
    podepa = BigDecimal.valueOf(pPodepa);
  }
  
  public void setPodepa(Date pPodepa) {
    if (pPodepa == null) {
      return;
    }
    podepa = BigDecimal.valueOf(ConvertDate.dateToDb2(pPodepa));
  }
  
  public Integer getPodepa() {
    return podepa.intValue();
  }
  
  public Date getPodepaConvertiEnDate() {
    return ConvertDate.db2ToDate(podepa.intValue(), null);
  }
  
  public void setPopos(BigDecimal pPopos) {
    if (pPopos == null) {
      return;
    }
    popos = pPopos.setScale(DECIMAL_POPOS, RoundingMode.HALF_UP);
  }
  
  public void setPopos(Integer pPopos) {
    if (pPopos == null) {
      return;
    }
    popos = BigDecimal.valueOf(pPopos);
  }
  
  public void setPopos(Date pPopos) {
    if (pPopos == null) {
      return;
    }
    popos = BigDecimal.valueOf(ConvertDate.dateToDb2(pPopos));
  }
  
  public Integer getPopos() {
    return popos.intValue();
  }
  
  public Date getPoposConvertiEnDate() {
    return ConvertDate.db2ToDate(popos.intValue(), null);
  }
  
  public void setPoer01(BigDecimal pPoer01) {
    if (pPoer01 == null) {
      return;
    }
    poer01 = pPoer01.setScale(DECIMAL_POER01, RoundingMode.HALF_UP);
  }
  
  public void setPoer01(Integer pPoer01) {
    if (pPoer01 == null) {
      return;
    }
    poer01 = BigDecimal.valueOf(pPoer01);
  }
  
  public Integer getPoer01() {
    return poer01.intValue();
  }
  
  public void setPoer01a(String pPoer01a) {
    if (pPoer01a == null) {
      return;
    }
    poer01a = pPoer01a;
  }
  
  public String getPoer01a() {
    return poer01a;
  }
  
  public void setPoer02(BigDecimal pPoer02) {
    if (pPoer02 == null) {
      return;
    }
    poer02 = pPoer02.setScale(DECIMAL_POER02, RoundingMode.HALF_UP);
  }
  
  public void setPoer02(Integer pPoer02) {
    if (pPoer02 == null) {
      return;
    }
    poer02 = BigDecimal.valueOf(pPoer02);
  }
  
  public Integer getPoer02() {
    return poer02.intValue();
  }
  
  public void setPoer02a(String pPoer02a) {
    if (pPoer02a == null) {
      return;
    }
    poer02a = pPoer02a;
  }
  
  public String getPoer02a() {
    return poer02a;
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
