/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

public class FactureEDI_GP extends EntiteEDI {
  
  // CONSTANTES
  private GenericRecord recordAssocie = null;
  
  // lignes d'entête
  private final String DECLA_ENT = "ENT";
  // lignes partenaires
  private final String DECLA_PAR = "PAR";
  private final String DECLA_DTM = "DTM";
  private String id_entete_1 = null;
  private String commande_client_2 = null;
  private String date_commande_3 = null;
  private String date_livraison_7 = null;
  private String bon_livraison_8 = null;
  private String numero_Facture_13 = null;
  private String date_Facture_14 = null;
  private String date_echeance_15 = null;
  private String typeDocument_16 = null;
  private String devise_facture_17 = null;
  private String document_test_26 = null;
  private String codePaiement_27 = null;
  private String natureDocument_28 = null;
  // Partenaires
  private String EANclientPayeur_2 = null;
  private String lib_clientPayeur_3 = null;
  private String EANfournisseur_4 = null;
  private String lib_fournisseur_5 = null;
  private String EANclientlivre_6 = null;
  private String lib_clientLivre_7 = null;
  private String EANclientFacture_8 = null;
  private String lib_clientFacture_9 = null;
  private String EAN_factor_10 = null;
  private String lib_factor_11 = null;
  // lignes
  private ArrayList<Ligne_Cde_EDI_GP> lignes = null;
  // pied de commande
  private final String DECLA_PIE = "PIE";
  private String montantTotalHT_2 = null;
  private String montantTotalTVA_3 = null;
  private String montantTotalTTC_4 = null;
  
  // TVA
  private final String DECLA_TVA = "TVA";
  private String tauxTVA_2 = null;
  private String totalSoumisTVA_3 = null;
  private String totalTVA_4 = null;
  
  private ArrayList<TVA_Cde_EDI_GP> tvas = null;
  
  /**
   * Constructeur principal de la facture EDI
   */
  public FactureEDI_GP(ParametresFluxBibli pParams, GenericRecord record, QueryManager que) {
    super(pParams);
    recordAssocie = record;
    query = que;
    isValide = traitementContenu();
  }
  
  @Override
  protected boolean traitementContenu() {
    if (recordAssocie == null) {
      majError("[ExpeditionsEDI_GP] recordAssocie à NULL");
      return false;
    }
    id_gp_1 = "@GP";
    logiciel_edi_2 = "WEB@EDI";
    type_fichier_3 = "INVOIC";
    format_fichier_4 = "STANDARD";
    
    // ENTETE ++++++++++++++++++++++++++++++++++++++++++++++
    
    id_entete_1 = DECLA_ENT;
    
    // Numéro de commande Client
    if (recordAssocie.isPresentField("E1CCT") && !recordAssocie.getField("E1CCT").toString().trim().equals("")) {
      commande_client_2 = recordAssocie.getField("E1CCT").toString().trim();
    }
    else {
      majError("[FactureEDI_GP] E1CCT corrompu ");
      return false;
    }
    // Date de commande
    if (recordAssocie.isPresentField("E1CRE") && !recordAssocie.getField("E1CRE").toString().trim().equals("0")) {
      date_commande_3 = formaterUneDateSN_EDI(recordAssocie.getField("E1CRE").toString().trim());
      if (date_commande_3 == null) {
        return false;
      }
    }
    else {
      majError("[FactureEDI_GP] E1CRE corrompu ");
      return false;
    }
    // Date de livraison
    if (recordAssocie.isPresentField("E1EXP") && !recordAssocie.getField("E1EXP").toString().trim().equals("0")) {
      date_livraison_7 = formaterUneDateSN_EDI(recordAssocie.getField("E1EXP").toString().trim());
      if (date_livraison_7 == null) {
        return false;
      }
    }
    else {
      // Si la date d'expe est la même que la fac ils vident E1EXP et on prend E1FAC.... et oui...
      if (recordAssocie.isPresentField("E1FAC") && !recordAssocie.getField("E1FAC").toString().trim().equals("0")) {
        date_livraison_7 = formaterUneDateSN_EDI(recordAssocie.getField("E1FAC").toString().trim());
        if (date_livraison_7 == null) {
          return false;
        }
      }
      else {
        majError("[FactureEDI_GP] E1EXP vide et E1FAC aussi !! ");
        return false;
      }
    }
    // Numéro de livraison
    if (recordAssocie.isPresentField("E1NUM") && !recordAssocie.getField("E1NUM").toString().trim().equals("")
        && recordAssocie.isPresentField("E1SUF") && !recordAssocie.getField("E1SUF").toString().trim().equals("")) {
      bon_livraison_8 = recordAssocie.getField("E1NUM").toString().trim() + recordAssocie.getField("E1SUF").toString().trim();
      if (bon_livraison_8 == null) {
        return false;
      }
    }
    else {
      majError("[FactureEDI_GP] E1NUM ou E1SUF corrompus ");
      return false;
    }
    // numéro de facture Série N
    if (recordAssocie.isPresentField("E1NFA") && !recordAssocie.getField("E1NFA").toString().trim().equals("0")) {
      numero_Facture_13 = recordAssocie.getField("E1NFA").toString().trim();
    }
    else {
      majError("[FactureEDI_GP] E1NFA corrompu ");
      return false;
    }
    
    // Date de facture
    if (recordAssocie.isPresentField("E1FAC") && !recordAssocie.getField("E1FAC").toString().trim().equals("0")) {
      date_Facture_14 = formaterUneDateSN_EDI(recordAssocie.getField("E1FAC").toString().trim());
      if (date_Facture_14 == null) {
        return false;
      }
    }
    else {
      majError("[FactureEDI_GP] E1FAC corrompu ");
      return false;
    }
    
    // TODO IL FAUT l'ALGO de la date d'échéance que j'attends par l'équipe métier 14/06/16 E1EC2
    // en attendant
    date_echeance_15 = date_Facture_14;
    
    typeDocument_16 = "Facture";
    // TODO encore une fois il faudra traiter cette fucking correspondance de devises
    devise_facture_17 = "EUR";
    
    document_test_26 = parametresFlux.getMode_test_EDI();
    
    // code de paiement
    if (recordAssocie.isPresentField("E1RG2") && !recordAssocie.getField("E1RG2").toString().trim().equals("")) {
      codePaiement_27 = interpreterCodePaiement(recordAssocie.getField("E1RG2").toString().trim());
      if (codePaiement_27 == null) {
        return false;
      }
    }
    else {
      majError("[FactureEDI_GP] E1RG2 corrompu ");
      return false;
    }
    
    natureDocument_28 = "MAR";
    
    // PARTENAIRES ++++++++++++++++++++++++++++
    EANfournisseur_4 = parametresFlux.getEAN_founisseur_EDI();
    lib_fournisseur_5 = parametresFlux.getLib_founisseur_EDI();
    
    GenericRecord recordPartenaire = null;
    // on met à jour le client livré
    if (recordAssocie.isPresentField("E1ETB") && recordAssocie.isPresentField("E1CLLP") && recordAssocie.isPresentField("E1CLLS")) {
      recordPartenaire = recupererInfosPartenaire(recordAssocie.getField("E1ETB").toString().trim(),
          recordAssocie.getField("E1CLLP").toString().trim(), recordAssocie.getField("E1CLLS").toString().trim());
      if (recordPartenaire != null) {
        if (recordPartenaire.isPresentField("GCGCD") && !recordPartenaire.getField("GCGCD").toString().trim().equals("")) {
          EANclientlivre_6 = recordPartenaire.getField("GCGCD").toString().trim();
        }
        else {
          majError("[FactureEDI_GP] traitementContenu() recordPartenaire GCGCD à NULL ");
          return false;
        }
        
        if (recordPartenaire.isPresentField("CLNOM") && !recordPartenaire.getField("CLNOM").toString().trim().equals("")) {
          lib_clientLivre_7 = recordPartenaire.getField("CLNOM").toString().trim();
        }
        else {
          majError("[FactureEDI_GP] traitementContenu() recordPartenaire CLNOM à NULL ");
          return false;
        }
      }
      else {
        return false;
      }
    }
    else {
      majError("[FactureEDI_GP] traitementContenu() E1ETB, E1CLLP ou E1CLLS à NULL ");
      return false;
    }
    
    recordPartenaire = null;
    // on met à jour le client facturé
    if (recordAssocie.isPresentField("E1ETB") && recordAssocie.isPresentField("E1CLFP") && recordAssocie.isPresentField("E1CLFS")) {
      recordPartenaire = recupererInfosPartenaire(recordAssocie.getField("E1ETB").toString().trim(),
          recordAssocie.getField("E1CLFP").toString().trim(), recordAssocie.getField("E1CLFS").toString().trim());
      if (recordPartenaire != null) {
        if (recordPartenaire.isPresentField("GCGCD") && !recordPartenaire.getField("GCGCD").toString().trim().equals("")) {
          EANclientPayeur_2 = recordPartenaire.getField("GCGCD").toString().trim();
        }
        else {
          majError("[FactureEDI_GP] traitementContenu() recordPartenaire GCGCD à NULL ");
          return false;
        }
        
        if (recordPartenaire.isPresentField("CLNOM") && !recordPartenaire.getField("CLNOM").toString().trim().equals("")) {
          lib_clientPayeur_3 = recordPartenaire.getField("CLNOM").toString().trim();
        }
        else {
          majError("[FactureEDI_GP] traitementContenu() recordPartenaire CLNOM à NULL ");
          return false;
        }
        // On va aller chercher un client Payeur s'il existe et comble du comble nous allons le mettre dans le facturé. Et oui parce qu'on
        // a
        // mis le facturé dans le payeur et le livré dans le livré...
        // C'est pluis rigolo de faire compliqué
        if (recordPartenaire.isPresentField("CLETB") && recordPartenaire.isPresentField("CLCLP")
            && !recordPartenaire.getField("CLCLP").toString().trim().equals("0")) {
          GenericRecord recordPayeur = recupererInfosPartenaire(recordAssocie.getField("E1ETB").toString().trim(),
              recordPartenaire.getField("CLCLP").toString().trim(), "0");
          if (recordPayeur != null) {
            if (recordPayeur.isPresentField("GCGCD") && !recordPayeur.getField("GCGCD").toString().trim().equals("")) {
              EANclientFacture_8 = recordPayeur.getField("GCGCD").toString().trim();
            }
            else {
              majError("[FactureEDI_GP] traitementContenu() recordPayeur GCGCD à NULL ");
              return false;
            }
            
            if (recordPayeur.isPresentField("CLNOM") && !recordPayeur.getField("CLNOM").toString().trim().equals("")) {
              lib_clientFacture_9 = recordPayeur.getField("CLNOM").toString().trim();
            }
            else {
              majError("[FactureEDI_GP] traitementContenu() recordPayeur CLNOM à NULL ");
              return false;
            }
          }
          else {
            return false;
          }
        }
        else {
          EANclientFacture_8 = EANclientPayeur_2;
          lib_clientFacture_9 = lib_clientPayeur_3;
        }
      }
      else {
        return false;
      }
    }
    else {
      majError("[FactureEDI_GP] traitementContenu() E1ETB, E1CLLP ou E1CLLS à NULL ");
      return false;
    }
    
    EAN_factor_10 = parametresFlux.getEAN_factor_EDI();
    lib_factor_11 = parametresFlux.getLib_factor_EDI();
    // Montant total HT
    if (recordAssocie.isPresentField("E1THTL") && !recordAssocie.getField("E1THTL").toString().trim().equals("0")) {
      montantTotalHT_2 = recordAssocie.getField("E1THTL").toString().trim();
      if (montantTotalHT_2 == null) {
        majError("[FactureEDI_GP] traitementContenu() montantTotalHT_2 à NULL ");
        return false;
      }
      else {
        totalSoumisTVA_3 = montantTotalHT_2;
      }
    }
    else {
      majError("[FactureEDI_GP] E1THTL corrompu ");
      return false;
    }
    // Total TVA
    if (recordAssocie.isPresentField("TTTVA") && !recordAssocie.getField("TTTVA").toString().trim().equals("0")) {
      montantTotalTVA_3 = recordAssocie.getField("TTTVA").toString().trim();
      if (montantTotalTVA_3 == null) {
        majError("[FactureEDI_GP] traitementContenu() montantTotalTVA_3 à NULL ");
        return false;
      }
      else {
        totalTVA_4 = montantTotalTVA_3;
      }
    }
    else {
      majError("[FactureEDI_GP] TTTVA corrompu ");
      return false;
    }
    
    // Total TTC
    if (recordAssocie.isPresentField("E1TTC") && !recordAssocie.getField("E1TTC").toString().trim().equals("0")) {
      montantTotalTTC_4 = recordAssocie.getField("E1TTC").toString().trim();
      if (montantTotalTTC_4 == null) {
        majError("[FactureEDI_GP] traitementContenu() montantTotalTTC_4 à NULL ");
        return false;
      }
    }
    else {
      majError("[FactureEDI_GP] E1TTC corrompu ");
      return false;
    }
    
    // On va calculer le taux de TVA de cette ENTETE
    tvas = new ArrayList<TVA_Cde_EDI_GP>();
    if (recordAssocie.isPresentField("E1TL1") && recordAssocie.isPresentField("E1TV1")) {
      try {
        float montant = Float.parseFloat(recordAssocie.getField("E1TL1").toString().trim());
        // On traite cette TVA ou non
        if (montant > 0) {
          TVA_Cde_EDI_GP tvaPartielle =
              new TVA_Cde_EDI_GP(parametresFlux, recordAssocie.getField("E1TV1").toString(), recordAssocie.getField("E1TL1").toString());
          if (tvaPartielle.isValide()) {
            tauxTVA_2 = tvaPartielle.getTaux();
            tvas.add(tvaPartielle);
          }
          else {
            majError("[FactureEDI_GP] traitementContenu()tva invalide: " + tvaPartielle.getMsgError());
            return false;
          }
        }
      }
      catch (NumberFormatException e) {
        majError("[FactureEDI_GP] traitementContenu() pb de parse de montant " + recordAssocie.getField("E1TL1").toString() + " -> "
            + e.getMessage());
        return false;
      }
    }
    
    if (recordAssocie.isPresentField("E1TL2") && recordAssocie.isPresentField("E1TV2")) {
      try {
        float montant = Float.parseFloat(recordAssocie.getField("E1TL2").toString().trim());
        // On traite cette TVA ou non
        if (montant > 0) {
          TVA_Cde_EDI_GP tvaPartielle =
              new TVA_Cde_EDI_GP(parametresFlux, recordAssocie.getField("E1TV2").toString(), recordAssocie.getField("E1TL2").toString());
          if (tvaPartielle.isValide()) {
            tvas.add(tvaPartielle);
          }
          else {
            majError("[FactureEDI_GP] traitementContenu()tva invalide: " + tvaPartielle.getMsgError());
            return false;
          }
        }
      }
      catch (NumberFormatException e) {
        majError("[FactureEDI_GP] traitementContenu() pb de parse de montant " + recordAssocie.getField("E1TL2").toString() + " -> "
            + e.getMessage());
        return false;
      }
    }
    
    if (recordAssocie.isPresentField("E1TL3") && recordAssocie.isPresentField("E1TV3")) {
      try {
        float montant = Float.parseFloat(recordAssocie.getField("E1TL3").toString().trim());
        // On traite cette TVA ou non
        if (montant > 0) {
          TVA_Cde_EDI_GP tvaPartielle =
              new TVA_Cde_EDI_GP(parametresFlux, recordAssocie.getField("E1TV3").toString(), recordAssocie.getField("E1TL3").toString());
          if (tvaPartielle.isValide()) {
            tvas.add(tvaPartielle);
          }
          else {
            majError("[FactureEDI_GP] traitementContenu()tva invalide: " + tvaPartielle.getMsgError());
            return false;
          }
        }
      }
      catch (NumberFormatException e) {
        majError("[FactureEDI_GP] traitementContenu() pb de parse de montant " + recordAssocie.getField("E1TL3").toString() + " -> "
            + e.getMessage());
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * Mise à jour des lignes d'article de la commande
   */
  public boolean majDesLignes(ArrayList<GenericRecord> lignesBrutes) {
    isValide = false;
    
    if (lignesBrutes == null) {
      majError("[FactureEDI_GP] majDesLignes() GenericRecord recordTemp lignesBrutes NULL");
      return isValide;
    }
    
    lignes = new ArrayList<Ligne_Cde_EDI_GP>();
    int compteur = 0;
    
    for (int i = 0; i < lignesBrutes.size(); i++) {
      Ligne_Cde_EDI_GP lignePartielle = new Ligne_Cde_EDI_GP(parametresFlux, lignesBrutes.get(i));
      if (lignePartielle.isValide()) {
        lignes.add(lignePartielle);
        compteur++;
      }
      else {
        majError("[FactureEDI_GP] majDesLignes() PB ligne invalide " + lignePartielle.getMsgError());
      }
    }
    
    if (compteur == lignesBrutes.size() && compteur > 0) {
      isValide = true;
    }
    else {
      majError("[FactureEDI_GP] majDesLignes() compteur <> lignesBrutes -> compteur: " + compteur);
      return isValide;
    }
    
    return isValide;
  }
  
  /**
   * Interpréter les codes de réglement Série N avec
   */
  private String interpreterCodePaiement(String sn) {
    String retour = null;
    if (sn == null) {
      return null;
    }
    
    if (sn.equals("CH")) {
      retour = "20";
    }
    else if (sn.equals("VI")) {
      retour = "42";
    }
    else {
      retour = "42";
    }
    
    return retour;
  }
  
  public String getId_entete_1() {
    return id_entete_1;
  }
  
  public void setId_entete_1(String id_entete_1) {
    this.id_entete_1 = id_entete_1;
  }
  
  public String getCommande_client_2() {
    return commande_client_2;
  }
  
  public void setCommande_client_2(String commande_client_2) {
    this.commande_client_2 = commande_client_2;
  }
  
  public String getDate_commande_3() {
    return date_commande_3;
  }
  
  public void setDate_commande_3(String date_commande_3) {
    this.date_commande_3 = date_commande_3;
  }
  
  public String getDate_livraison_7() {
    return date_livraison_7;
  }
  
  public void setDate_livraison_7(String date_livraison_7) {
    this.date_livraison_7 = date_livraison_7;
  }
  
  public String getBon_livraison_8() {
    return bon_livraison_8;
  }
  
  public void setBon_livraison_8(String bon_livraison_8) {
    this.bon_livraison_8 = bon_livraison_8;
  }
  
  public String getNumero_Facture_13() {
    return numero_Facture_13;
  }
  
  public void setNumero_Facture_13(String numero_Facture_13) {
    this.numero_Facture_13 = numero_Facture_13;
  }
  
  public String getDate_Facture_14() {
    return date_Facture_14;
  }
  
  public void setDate_Facture_14(String date_Facture_14) {
    this.date_Facture_14 = date_Facture_14;
  }
  
  public String getDate_echeance_15() {
    return date_echeance_15;
  }
  
  public void setDate_echeance_15(String date_echeance_15) {
    this.date_echeance_15 = date_echeance_15;
  }
  
  public String getTypeDocument_16() {
    return typeDocument_16;
  }
  
  public void setTypeDocument_16(String typeDocument_16) {
    this.typeDocument_16 = typeDocument_16;
  }
  
  public String getDevise_facture_17() {
    return devise_facture_17;
  }
  
  public void setDevise_facture_17(String devise_facture_17) {
    this.devise_facture_17 = devise_facture_17;
  }
  
  public String getDocument_test_26() {
    return document_test_26;
  }
  
  public void setDocument_test_26(String document_test_26) {
    this.document_test_26 = document_test_26;
  }
  
  public String getCodePaiement_27() {
    return codePaiement_27;
  }
  
  public void setCodePaiement_27(String codePaiement_27) {
    this.codePaiement_27 = codePaiement_27;
  }
  
  public String getNatureDocument_28() {
    return natureDocument_28;
  }
  
  public void setNatureDocument_28(String natureDocument_28) {
    this.natureDocument_28 = natureDocument_28;
  }
  
  public String getEANclientPayeur_2() {
    return EANclientPayeur_2;
  }
  
  public void setEANclientPayeur_2(String eANclientPayeur_2) {
    EANclientPayeur_2 = eANclientPayeur_2;
  }
  
  public String getLib_clientPayeur_3() {
    return lib_clientPayeur_3;
  }
  
  public void setLib_clientPayeur_3(String lib_clientPayeur_3) {
    this.lib_clientPayeur_3 = lib_clientPayeur_3;
  }
  
  public String getEANfournisseur_4() {
    return EANfournisseur_4;
  }
  
  public void setEANfournisseur_4(String eANfournisseur_4) {
    EANfournisseur_4 = eANfournisseur_4;
  }
  
  public String getLib_fournisseur_5() {
    return lib_fournisseur_5;
  }
  
  public void setLib_fournisseur_5(String lib_fournisseur_5) {
    this.lib_fournisseur_5 = lib_fournisseur_5;
  }
  
  public String getEANclientlivre_6() {
    return EANclientlivre_6;
  }
  
  public void setEANclientlivre_6(String eANclientlivre_6) {
    EANclientlivre_6 = eANclientlivre_6;
  }
  
  public String getLib_clientLivre_7() {
    return lib_clientLivre_7;
  }
  
  public void setLib_clientLivre_7(String lib_clientLivre_7) {
    this.lib_clientLivre_7 = lib_clientLivre_7;
  }
  
  public String getEANclientFacture_8() {
    return EANclientFacture_8;
  }
  
  public void setEANclientFacture_8(String eANclientFacture_8) {
    EANclientFacture_8 = eANclientFacture_8;
  }
  
  public String getLib_clientFacture_9() {
    return lib_clientFacture_9;
  }
  
  public void setLib_clientFacture_9(String lib_clientFacture_9) {
    this.lib_clientFacture_9 = lib_clientFacture_9;
  }
  
  public String getEAN_factor_10() {
    return EAN_factor_10;
  }
  
  public void setEAN_factor_10(String eAN_factor_10) {
    EAN_factor_10 = eAN_factor_10;
  }
  
  public String getLib_factor_11() {
    return lib_factor_11;
  }
  
  public void setLib_factor_11(String lib_factor_11) {
    this.lib_factor_11 = lib_factor_11;
  }
  
  public ArrayList<Ligne_Cde_EDI_GP> getLignes() {
    return lignes;
  }
  
  public void setLignes(ArrayList<Ligne_Cde_EDI_GP> lignes) {
    this.lignes = lignes;
  }
  
  public String getMontantTotalHT_2() {
    return montantTotalHT_2;
  }
  
  public void setMontantTotalHT_2(String montantTotalHT_2) {
    this.montantTotalHT_2 = montantTotalHT_2;
  }
  
  public String getMontantTotalTVA_3() {
    return montantTotalTVA_3;
  }
  
  public void setMontantTotalTVA_3(String montantTotalTVA_3) {
    this.montantTotalTVA_3 = montantTotalTVA_3;
  }
  
  public String getMontantTotalTTC_4() {
    return montantTotalTTC_4;
  }
  
  public void setMontantTotalTTC_4(String montantTotalTTC_4) {
    this.montantTotalTTC_4 = montantTotalTTC_4;
  }
  
  public String getTauxTVA_2() {
    return tauxTVA_2;
  }
  
  public void setTauxTVA_2(String tauxTVA_2) {
    this.tauxTVA_2 = tauxTVA_2;
  }
  
  public String getTotalSoumisTVA_3() {
    return totalSoumisTVA_3;
  }
  
  public void setTotalSoumisTVA_3(String totalSoumisTVA_3) {
    this.totalSoumisTVA_3 = totalSoumisTVA_3;
  }
  
  public String getTotalTVA_4() {
    return totalTVA_4;
  }
  
  public void setTotalTVA_4(String totalTVA_4) {
    this.totalTVA_4 = totalTVA_4;
  }
  
  public ArrayList<TVA_Cde_EDI_GP> getTvas() {
    return tvas;
  }
  
  public void setTvas(ArrayList<TVA_Cde_EDI_GP> tvas) {
    this.tvas = tvas;
  }
  
  public String getDECLA_ENT() {
    return DECLA_ENT;
  }
  
  public String getDECLA_PAR() {
    return DECLA_PAR;
  }
  
  public String getDECLA_PIE() {
    return DECLA_PIE;
  }
  
  public String getDECLA_TVA() {
    return DECLA_TVA;
  }
  
  public String getDECLA_DTM() {
    return DECLA_DTM;
  }
  
}
