/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux;

import ri.serien.libcommun.gescom.commun.adresse.Adresse;

public class OutilsMagento {
  
  /**
   * nettoyer des caractères spéciaux avant INSERT OU UPDATE
   */
  public static String nettoyerSaisiesSQL(String donnee) {
    if (donnee == null) {
      return donnee;
    }
    
    return (donnee.replace("'", "''")).trim();
  }
  
  /**
   * nettoyer des caractères spéciaux avant INSERT OU UPDATE
   */
  public static String nettoyerSaisiesSQLsansTrim(String donnee) {
    if (donnee == null) {
      return donnee;
    }
    
    return (donnee.replace("'", "''"));
  }
  
  /**
   * Formater un texte à la longueur maximale de la zone.
   */
  public static String formaterTexteLongueurMax(String pTexte, int pLongueurMax) {
    if (pTexte == null || pTexte.trim().length() <= pLongueurMax) {
      return pTexte;
    }
    
    return pTexte.trim().substring(0, pLongueurMax);
  }
  
  /**
   * Formater un fucking code postal rentrant pour une fucking interprétation en SN
   * C'est de la merde ne plus utiliser.
   */
  public static String formaterUnCodePostalpourSN(String pCode) {
    if (pCode == null || pCode.trim().length() == 0) {
      return "     ";
    }
    
    String retour = pCode.trim();
    
    if (retour.length() == 1) {
      retour = "    " + retour;
    }
    else if (retour.length() == 2) {
      retour = "   " + retour;
    }
    else if (retour.length() == 3) {
      retour = "  " + retour;
    }
    else if (retour.length() == 4) {
      retour = " " + retour;
    }
    else if (retour.length() > Adresse.LONGUEUR_CODEPOSTAL) {
      retour = retour.substring(0, Adresse.LONGUEUR_CODEPOSTAL);
    }
    
    return retour;
  }
  
  /**
   * On découpe un identifiant client au format xxxx/xxx en code client au format xxxxxx et suffixe client au format xxx
   */
  public static String[] decouperIdentifiantsClient(String pCode) {
    if (pCode == null || pCode.trim().equals("")) {
      return null;
    }
    
    String[] tab = pCode.split("/");
    if (tab == null || tab.length != 2) {
      return null;
    }
    
    return tab;
  }
}
