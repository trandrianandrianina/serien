/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.Record;

import ri.serien.libas400.dao.sql.gescom.communezonegeographique.SqlCommuneZoneGeographique;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.ExtendRecord;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.EnumTypeCumulConditionQuantitative;
import ri.serien.libcommun.gescom.commun.EnumTypeUniteConditionQuantitative;
import ri.serien.libcommun.gescom.commun.EnumTypeValeurPersonnalisation;
import ri.serien.libcommun.gescom.commun.EnumUnitePeriode;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.personnalisation.acheteur.Acheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.CriteresRechercheAcheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.IdAcheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.ListeAcheteur;
import ri.serien.libcommun.gescom.personnalisation.affaire.Affaire;
import ri.serien.libcommun.gescom.personnalisation.affaire.CriteresRechercheAffaires;
import ri.serien.libcommun.gescom.personnalisation.affaire.IdAffaire;
import ri.serien.libcommun.gescom.personnalisation.affaire.ListeAffaire;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.CanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.CriteresRechercheCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.IdCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.ListeCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.CategorieClient;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.CriteresRechercheCategoriesClients;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.IdCategorieClient;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.ListeCategorieClient;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.CategorieFournisseur;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.CriteresRechercheCategoriesFournisseur;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.IdCategorieFournisseur;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.ListeCategorieFournisseur;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.CommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.CritereCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.ListeCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.ConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.CriteresConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.IdConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.ListeConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.famille.CritereFamille;
import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.personnalisation.famille.IdFamille;
import ri.serien.libcommun.gescom.personnalisation.famille.ListeFamille;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.CritereFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.EnumTypeValeurFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.FormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.IdFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.LigneFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.ListeFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.groupe.CritereGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupe.Groupe;
import ri.serien.libcommun.gescom.personnalisation.groupe.IdGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupe.ListeGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.CritereGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.EnumModeApplicationConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.EnumTypeGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.GroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.IdGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.ListeGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.CritereLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.IdLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.LieuTransport;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.ListeLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.magasin.CritereMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.CriteresModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.IdModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.motifretour.IdMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.motifretour.ListeMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.motifretour.MotifRetour;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.IdParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.ListeParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.ParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.CriteresParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.IdParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.ListeParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.ParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.reglement.CritereModeReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.EnumSensReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.EnumTypeReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.IdModeReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.ListeModeReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.ModeReglement;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.CriteresRechercheSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.ListeSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.SectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.idSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.CritereSousFamilles;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.IdSousFamille;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille;
import ri.serien.libcommun.gescom.personnalisation.tarif.CriteresRechercheTarifs;
import ri.serien.libcommun.gescom.personnalisation.tarif.IdTarif;
import ri.serien.libcommun.gescom.personnalisation.tarif.ListeTarif;
import ri.serien.libcommun.gescom.personnalisation.tarif.Tarif;
import ri.serien.libcommun.gescom.personnalisation.transporteur.CriteresRechercheTransporteurs;
import ri.serien.libcommun.gescom.personnalisation.transporteur.IdTransporteur;
import ri.serien.libcommun.gescom.personnalisation.transporteur.ListeTransporteur;
import ri.serien.libcommun.gescom.personnalisation.transporteur.Transporteur;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.IdTypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.ListeTypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.TypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.IdTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.ListeTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.TypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.CriteresRechercheTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.EnumEditionLibelleCompletTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.EnumNonEditionTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.EnumNonGereStatistiqueTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.IdTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.ListeTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.TypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typestockagemagasin.IdTypeStockageMagasin;
import ri.serien.libcommun.gescom.personnalisation.typestockagemagasin.ListeTypeStockageMagasin;
import ri.serien.libcommun.gescom.personnalisation.typestockagemagasin.TypeStockageMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.CriteresRechercheUnites;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.Unite;
import ri.serien.libcommun.gescom.personnalisation.vendeur.CriteresRechercheVendeurs;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.CritereZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ListeZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.CategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.CritereCategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.IdCategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.ListeCategorieZonePersonnalisee;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.DataFormat;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlGescomParametres extends BaseGroupDB {
  private static final String PACKAGE_PERSONNALISATION = "ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm.";
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Constructeur.
   * @param pQueryManager
   */
  public SqlGescomParametres(QueryManager pQueryManager) {
    super(pQueryManager);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Libère les ressources.
   */
  @Override
  public void dispose() {
    queryManager = null;
    
  }
  
  /**
   * Retourne la liste des vendeurs pour un établissement donné.
   * @param pCriteres Critères de recherche
   * @return ListeVendeur
   */
  public ListeVendeur chargerListeVendeur(CriteresRechercheVendeurs pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        RequetesRechercheVendeurs.getRequete(queryManager.getLibrary(), pCriteres), PACKAGE_PERSONNALISATION + "DsVendeur");
    if (listeRecord == null || listeRecord.size() == 0) {
      return null;
    }
    
    ListeVendeur listeVendeur = new ListeVendeur();
    for (Record record : listeRecord) {
      ExtendRecord extendedRecord = new ExtendRecord();
      extendedRecord.setRecord(record);
      Vendeur vendeur = initRechercheVendeurs(pCriteres, extendedRecord);
      listeVendeur.add(vendeur);
    }
    
    return listeVendeur;
  }
  
  /**
   * Retourne la liste des acheteurs pour un établissement donnée.
   * @param pCriteres Critères de recherche
   * @return ListeAcheteur
   */
  public ListeAcheteur chargerListeAcheteur(CriteresRechercheAcheteur pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        RequetesRechercheAcheteur.getRequete(queryManager.getLibrary(), pCriteres), PACKAGE_PERSONNALISATION + "DsAcheteur");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeAcheteur listeAcheteur = new ListeAcheteur();
    for (Record record : listeRecord) {
      ExtendRecord extendedRecord = new ExtendRecord();
      extendedRecord.setRecord(record);
      Acheteur acheteur = initRechercheAcheteur(pCriteres, extendedRecord);
      listeAcheteur.add(acheteur);
    }
    return listeAcheteur;
  }
  
  /**
   * Retourne la liste des règlements pour un établissement donnée.
   * @param pCriteres Critères de recherche
   * @return ListeModeReglement
   */
  public ListeModeReglement chargerListeModeReglement(CritereModeReglement pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        RequetesRechercheReglements.getRequete(queryManager.getLibrary(), pCriteres), PACKAGE_PERSONNALISATION + "DsReglement");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On alimente le tableau avec les enregistrements trouvés
    ListeModeReglement listeReglement = new ListeModeReglement();
    for (Record rcd : listeRecord) {
      ExtendRecord record = new ExtendRecord();
      record.setRecord(rcd);
      ModeReglement reglement = initRechercheReglements(pCriteres, record);
      listeReglement.add(reglement);
    }
    return listeReglement;
  }
  
  /**
   * Charger la liste des groupes correspondant aux critères fournis en paramètre.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeGroupe
   */
  public ListeGroupe chargerListeGroupe(CritereGroupe pCritere) {
    // Vérifier les pré-requis
    if (pCritere == null) {
      throw new MessageErreurException("Impossible de charger les groupes car les critères de recherche sont invalides");
    }
    
    // Générer la requête
    String sql = RequetesRechercheGroupes.getRequete(queryManager.getLibrary(), pCritere);
    
    // Exécuter la requête
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(sql, PACKAGE_PERSONNALISATION + "DsGroupe");
    if (listeRecord == null || listeRecord.size() == 0) {
      return null;
    }
    
    // Remplir la liste des groupes
    ListeGroupe listeGroupe = new ListeGroupe();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      Groupe groupe = initialiserGroupe(pCritere, record);
      listeGroupe.add(groupe);
    }
    
    return listeGroupe;
  }
  
  /**
   * Retourne la liste des affaires pour un etablissement donnée.
   * @param pCriteres Critères de recherche
   * @return ListeAffaire
   */
  public ListeAffaire chargerListeAffaire(CriteresRechercheAffaires pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        RequetesRechercheAffaires.getRequete(queryManager.getLibrary(), pCriteres), PACKAGE_PERSONNALISATION + "DsAffaires");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeAffaire listeAffaire = new ListeAffaire();
    for (Record record : listeRecord) {
      ExtendRecord extendedRecord = new ExtendRecord();
      extendedRecord.setRecord(record);
      Affaire affaire = initRechercheAffaire(pCriteres, extendedRecord);
      listeAffaire.add(affaire);
    }
    return listeAffaire;
  }
  
  /**
   * Retourne la liste des sections analytiques pour un etablissement donnée.
   * @param pCriteres Critères de recherche
   * @return ListeSectionAnalytique
   */
  public ListeSectionAnalytique chargerListeSectionAnalytique(CriteresRechercheSectionAnalytique pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure(RequetesRechercheSectionAnalytique.getRequete(queryManager.getLibrary(), pCriteres),
            PACKAGE_PERSONNALISATION + "DsSectionAnalytique");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeSectionAnalytique listeSectionAnalytique = new ListeSectionAnalytique();
    for (Record record : listeRecord) {
      ExtendRecord extendedRecord = new ExtendRecord();
      extendedRecord.setRecord(record);
      SectionAnalytique section = initRechercheSectionAnalytique(pCriteres, extendedRecord);
      listeSectionAnalytique.add(section);
    }
    return listeSectionAnalytique;
  }
  
  /**
   * Retourne la liste des canaux de vente pour un etablissement donnée.
   * @param pCriteres Critères de recherche
   * @return ListeCanalDeVente
   */
  public ListeCanalDeVente chargerListeCanalDeVente(CriteresRechercheCanalDeVente pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        RequetesRechercheCanalDeVente.getRequete(queryManager.getLibrary(), pCriteres), PACKAGE_PERSONNALISATION + "DsCanalDeVente");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeCanalDeVente listeCanalDeVente = new ListeCanalDeVente();
    for (Record record : listeRecord) {
      ExtendRecord extendedRecord = new ExtendRecord();
      extendedRecord.setRecord(record);
      CanalDeVente canal = initRechercheCanalDeVente(pCriteres, extendedRecord);
      listeCanalDeVente.add(canal);
    }
    return listeCanalDeVente;
  }
  
  /**
   * Retourne la liste des type de gratuit pour un etablissement donnée.
   * @param pCriteres Critères de recherche
   * @return ListeTypeGratuit
   */
  public ListeTypeGratuit chargerListeTypeGratuit(CriteresRechercheTypeGratuit pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        RequetesRechercheTypeGratuit.getRequete(queryManager.getLibrary(), pCriteres), PACKAGE_PERSONNALISATION + "DsTypeGratuit");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeTypeGratuit listeTypeGratuit = new ListeTypeGratuit();
    for (Record record : listeRecord) {
      ExtendRecord extendedRecord = new ExtendRecord();
      extendedRecord.setRecord(record);
      TypeGratuit typeGratuit = initRechercheTypeGratuit(pCriteres, extendedRecord);
      listeTypeGratuit.add(typeGratuit);
    }
    return listeTypeGratuit;
  }
  
  /**
   * Charger la liste des familles correspondant aux critères furnis en paramètre.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeFamille
   */
  public ListeFamille chargerListeFamille(CritereFamille pCriteres) {
    // Vérifier les pré-requis
    if (pCriteres == null) {
      throw new MessageErreurException("Impossible de charger les familles car les critères de recherche sont invalides");
    }
    // Générer la requête
    String sql = RequetesRechercheFamilles.getRequete(queryManager.getLibrary(), pCriteres);
    
    // Exécuter la requete
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(sql, PACKAGE_PERSONNALISATION + "DsFamille");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On alimente le tableau avec les enregistrements trouvés
    ListeFamille listeFamille = new ListeFamille();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      Famille famille = initialiserFamilles(pCriteres, record);
      listeFamille.add(famille);
    }
    return listeFamille;
  }
  
  /**
   * Charger la liste des sous-familles correspondant aux critères fournis en paramètre.
   * l'ordre du résultat conditionne l'afficahge dans la liste déroulante (ordre alphabétique)
   * @param pCriteres Critères de recherche
   * @return ListeSousFamille
   */
  public ListeSousFamille chargerListeSousFamille(CritereSousFamilles pCriteres) {
    if (pCriteres == null) {
      throw new MessageErreurException("Impossible de charger les sous famille car les critères de recherche sont invalides");
    }
    
    // Générer la requête
    String sql = RequetesRechercheSousFamilles.getRequete(queryManager.getLibrary(), pCriteres);
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(sql, PACKAGE_PERSONNALISATION + "DsSousFamille");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // Remplire la liste des sous familles
    ListeSousFamille listeSousFamille = new ListeSousFamille();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      SousFamille sousFamille = initialiserSousFamilles(pCriteres, record);
      listeSousFamille.add(sousFamille);
    }
    return listeSousFamille;
  }
  
  /**
   * Retourner la liste des magasin pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeMagasin
   */
  public ListeMagasin chargerListeMagasin(CritereMagasin pCritereMagasin) {
    if (pCritereMagasin == null) {
      throw new MessageErreurException("Impossible de charger la liste des magasins car les critères de recherche sont invalides.");
    }
    
    String choixEtb = " ";
    if (pCritereMagasin.getIdEtablissement() != null) {
      choixEtb = " AND PARETB = '" + pCritereMagasin.getIdEtablissement().getCodeEtablissement() + "' ";
    }
    String requete = "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary() + '.'
        + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'MA' " + choixEtb + " ORDER BY PARZ2";
    
    // Initialiser la liste
    ListeMagasin listeMagasins = new ListeMagasin();
    
    // Lancer la requête SQL
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(requete, PACKAGE_PERSONNALISATION + "DsMagasin");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return listeMagasins;
    }
    
    // Lire le résultat
    ExtendRecord extendRecord = new ExtendRecord();
    for (Record record : listeRecord) {
      extendRecord.setRecord(record);
      Magasin magasin = initialiserMagasin(extendRecord);
      if (magasin != null) {
        listeMagasins.add(magasin);
      }
    }
    return listeMagasins;
  }
  
  /**
   * Retourner la liste des types de facturations pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pIdEtablissement identifiant de l'établissement
   * @return ListeTypeFacturation
   *         TODO ajouter un critère de recherche pour prendre en compte le magasin dans le champ ordre (cadré à droite) et si pas trouvé
   *         alors mettre ordre à blanc.
   */
  public ListeTypeFacturation chargerListeTypeFacturation(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
            + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'TF' and PARETB = '"
            + pIdEtablissement.getCodeEtablissement() + "' ORDER BY PARZ2", PACKAGE_PERSONNALISATION + "DsTypeFacturation");
    if (listeRecord == null) {
      return null;
    }
    
    // Alimentation des données avec l'enregistrement trouvé
    Record record = listeRecord.get(0);
    ListeTypeFacturation listeTypeFacturation = new ListeTypeFacturation();
    try {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) record.getField("TFETB")));
      
      for (int i = 1; i <= ListeTypeFacturation.NOMBREMAXTYPES; i++) {
        String code = Constantes.normerTexte((String) record.getField("TFTF" + i));
        String libelle = Constantes.normerTexte((String) record.getField("TFLI" + i));
        BigDecimal codeTVA = (BigDecimal) record.getField("TFTV" + i);
        if (!code.isEmpty() && !libelle.isEmpty()) {
          TypeFacturation detail = new TypeFacturation(IdTypeFacturation.getInstance(idEtablissement, code));
          detail.setLibelle(libelle);
          detail.setCodeTVA(Integer.valueOf(codeTVA.intValue()));
          listeTypeFacturation.add(detail);
        }
        // Les autres champs à alimenter qd on en aura besoin
      }
      
    }
    catch (UnsupportedEncodingException e) {
    }
    return listeTypeFacturation;
  }
  
  /**
   * Retourner la liste des type de stockage magasin pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pIdEtablissement identifiant de l'établissement
   * @return ListeTypeStockageMagasin
   */
  public ListeTypeStockageMagasin chargerListeTypeStockageMagasin(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return null;
    }
    
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary() + '.'
            + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'MS' ORDER BY PARZ2",
        PACKAGE_PERSONNALISATION + "DsTypeStockageMagasin");
    
    if (listeRecord == null) {
      return null;
    }
    
    ListeTypeStockageMagasin listeTypeStockageMagasin = new ListeTypeStockageMagasin();
    
    // On alimente la classe avec l'enregistrement trouvé
    for (Record record : listeRecord) {
      try {
        IdEtablissement idEtablissement;
        
        idEtablissement = IdEtablissement.getInstance(((String) record.getField("INDETB")));
        TypeStockageMagasin typeStockageMagasin =
            new TypeStockageMagasin(IdTypeStockageMagasin.getInstance(idEtablissement, ((String) record.getField("INDCOD"))));
        typeStockageMagasin.setLibelle(((String) record.getField("MSLIB")));
        listeTypeStockageMagasin.add(typeStockageMagasin);
      }
      catch (UnsupportedEncodingException e) {
        throw new MessageErreurException(e, e.getMessage());
      }
    }
    
    return listeTypeStockageMagasin;
  }
  
  /**
   * Lit les motifs de retour pour un établissement donnée.
   * @param pIdEtablissement identifiant de l'établissement
   * @return ListeMotifRetour
   */
  public ListeMotifRetour chargerListeMotifRetour(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
            + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'AV' and PARETB = '"
            + pIdEtablissement.getCodeEtablissement() + "'", PACKAGE_PERSONNALISATION + "DsMotifRetour");
    if (listeRecord == null) {
      return null;
    }
    
    // On alimente le tableau avec les enregistrements trouvés
    
    ListeMotifRetour listeMotifs = new ListeMotifRetour();
    ExtendRecord record = new ExtendRecord();
    
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      listeMotifs.add(initMotifRetour(record));
    }
    return listeMotifs;
    
  }
  
  /**
   * Retourne la liste des unités pour un établissement donnée.
   * @param pCriteres Critères de recherche
   * @return ListeUnite
   */
  public ListeUnite chargerListeUnite(CriteresRechercheUnites pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord = queryManager
        .selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary()
            + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'UN'", PACKAGE_PERSONNALISATION + "DsUnite");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On alimente le tableau avec les enregistrements trouvés
    ListeUnite listeUnite = new ListeUnite();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      listeUnite.add(initRechercheUnites(pCriteres, record));
    }
    return listeUnite;
  }
  
  /**
   * Retourner la liste des catégorie client pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeCategorieClient
   */
  public ListeCategorieClient chargerListeCategorieClient(CriteresRechercheCategoriesClients pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
            + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARETB = '"
            + pCriteres.getCodeEtablissement() + "' and PARTYP = 'CC' ORDER BY PARZ2", PACKAGE_PERSONNALISATION + "DsCategorieClient");
    
    // On alimente le tableau avec les enregistrements trouvés
    ListeCategorieClient listeCategorieClient = new ListeCategorieClient();
    ExtendRecord extendRecord = new ExtendRecord();
    for (Record record : listeRecord) {
      extendRecord.setRecord(record);
      listeCategorieClient.add(initRechercheCategoriesClients(pCriteres, extendRecord));
    }
    return listeCategorieClient;
  }
  
  /**
   * Retourner la liste des groupes des conditions de ventes pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeGroupeConditionVente
   */
  public ListeGroupeConditionVente chargerListeGroupeConditionVente(CritereGroupeConditionVente pCriteres) {
    // Contrôle des critères de recherche
    if (pCriteres == null) {
      pCriteres = new CritereGroupeConditionVente();
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary() + '.'
        + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'CN'");
    // Filtre sur l'établissement
    if (pCriteres.getIdEtablissement() != null) {
      requeteSql.ajouterConditionAnd("PARETB", "=", pCriteres.getIdEtablissement().getCodeEtablissement());
    }
    requeteSql.ajouter(" order by PARZ2");
    // Exécution de la requête
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure(requeteSql.getRequeteLectureSeule(), PACKAGE_PERSONNALISATION + "DsGroupeConditionVente");
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Alimentation de la liste avec les enregistrements trouvés et valides
    ListeGroupeConditionVente listeGroupeConditionVente = new ListeGroupeConditionVente();
    ExtendRecord extendRecord = new ExtendRecord();
    for (Record record : listeRecord) {
      extendRecord.setRecord(record);
      GroupeConditionVente groupeConditionVente = initialiserGroupeConditionVente(pCriteres, extendRecord);
      if (groupeConditionVente == null) {
        continue;
      }
      listeGroupeConditionVente.add(groupeConditionVente);
    }
    return listeGroupeConditionVente;
  }
  
  /**
   * Retourne le tableau des descriptions générales de tous les établissements.
   * @return Etablissement[]
   */
  public Etablissement[] chargerTableauEtablissement() {
    // On liste tous les codes établissements
    ArrayList<GenericRecord> listeRecord = queryManager.select("select paretb from " + queryManager.getLibrary() + '.'
        + EnumTableBDD.PARAMETRE_GESCOM + " where partyp = 'DG' and paretb <> ''");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On charge les informations de chaque établisseement
    int i = 0;
    Etablissement[] listeEtablissement = new Etablissement[listeRecord.size()];
    for (GenericRecord rcd : listeRecord) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(rcd.getString("PARETB"));
      listeEtablissement[i] = chargerEtablissement(idEtablissement);
      i++;
    }
    return listeEtablissement;
  }
  
  /**
   * Retourne la liste de tous les établissements
   * @return ListeEtablissement
   */
  public ListeEtablissement chargerListeEtablissement() {
    // On liste tous les codes établissements
    ArrayList<GenericRecord> listeRecord = queryManager.select("select paretb from " + queryManager.getLibrary() + '.'
        + EnumTableBDD.PARAMETRE_GESCOM + " where partyp = 'DG' and paretb <> ''");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On charge les informations de chaque établisseement
    ListeEtablissement listeEtablissement = new ListeEtablissement();
    for (GenericRecord rcd : listeRecord) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(rcd.getString("PARETB"));
      Etablissement etablissement = chargerEtablissement(idEtablissement);
      listeEtablissement.add(etablissement);
    }
    return listeEtablissement;
  }
  
  /**
   * Lit la Description Générale pour un établissement donné.
   * @param pIdEtablissement identifiant d'un établissement
   * @return Etablissement
   */
  public Etablissement chargerEtablissement(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord = queryManager
        .selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary()
            + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARETB = '" + pIdEtablissement.getCodeEtablissement()
            + "' and PARTYP = 'DG' and PARIND = ''", PACKAGE_PERSONNALISATION + "DsDescriptionGenerale");
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Alimentation de la description
    ExtendRecord record = new ExtendRecord();
    record.setRecord(listeRecord.get(0));
    
    Etablissement etablissement = new Etablissement(pIdEtablissement);
    // Nom ou raison sociale et complément de nom
    etablissement.setRaisonSociale(record.getField("DGNOM").toString().trim());
    etablissement.setComplementNom(record.getField("DGCPL").toString().trim());
    // Mois en cours
    BigDecimal date = (BigDecimal) record.getField("DGDATG");
    if (date.intValue() != 0) {
      etablissement.setMoisEnCours(date.toString().substring(0, 4));
    }
    // Taux de TVA
    etablissement.setTauxTVA1(new BigDecimal(record.getField("DGT01").toString()));
    etablissement.setTauxTVA2(new BigDecimal(record.getField("DGT02").toString()));
    etablissement.setTauxTVA3(new BigDecimal(record.getField("DGT03").toString()));
    etablissement.setTauxTVA4(new BigDecimal(record.getField("DGT04").toString()));
    etablissement.setTauxTVA5(new BigDecimal(record.getField("DGT05").toString()));
    etablissement.setTauxTVA6(new BigDecimal(record.getField("DGT06").toString()));
    etablissement.setMontantMaximumEspeces(new BigDecimal(record.getField("DGMTES").toString()));
    
    // Cas de la DG blanche
    if (pIdEtablissement.isEtablissementVide()) {
      // Code établissement pilote
      IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getField("DGETP").toString());
      etablissement.setId(idEtablissement);
      etablissement.setIdMagasinParDefaut(null);
    }
    // Cas d'un établissement normal
    else {
      etablissement.setId(pIdEtablissement);
      // L'arrondi
      BigDecimal divers = (BigDecimal) record.getField("DGDIVG");
      etablissement.setArrondi(Constantes.convertirTexteEnInteger(divers.toString().substring(0, 1)));
      etablissement.setDateExerciceDemarrageDebut(Constantes.convertirMoisAnneeEnDate(divers.toString().substring(1, 5)));
      etablissement.setDateExerciceDemarrageFin(Constantes.convertirMoisAnneeEnDate(divers.toString().substring(5, 9)));
      // La magasin
      if (!record.getField("DGETP").toString().trim().isEmpty()) {
        etablissement.setIdMagasinParDefaut(IdMagasin.getInstance(pIdEtablissement, record.getField("DGETP").toString().substring(0, 2)));
      }
      // Le code devise
      etablissement.setCodeDevise(record.getField("DGDEV").toString().trim());
      try {
        // Les identifiants des groupes de conditions de ventes générale et promotionnelle
        String valeur = Constantes.normerTexte(record.getField("DGCNG").toString());
        if (!valeur.isEmpty()) {
          etablissement.setIdGroupeConditionVenteGenerale(IdGroupeConditionVente.getInstance(pIdEtablissement, valeur));
        }
        valeur = Constantes.normerTexte(record.getField("DGCNP").toString());
        if (!valeur.isEmpty()) {
          etablissement.setIdGroupeConditionVentePromotionnelle(IdGroupeConditionVente.getInstance(pIdEtablissement, valeur));
        }
        // Le type de l'unité de vente des conditions de ventes quantitatives
        valeur = Constantes.normerTexte(record.getField("DGTC1").toString());
        if (!valeur.isEmpty()) {
          etablissement.setTypeUniteConditionQuantitative(EnumTypeUniteConditionQuantitative.valueOfByCode(valeur.charAt(0)));
        }
        // Le type du cumul des conditions de ventes quantitatives
        valeur = Constantes.normerTexte(record.getField("DGTC2").toString());
        if (!valeur.isEmpty()) {
          etablissement.setTypeCumulConditionQuantitative(EnumTypeCumulConditionQuantitative.valueOfByCode(valeur.charAt(0)));
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
    // TODO le reste des informations de la DG
    
    return etablissement;
  }
  
  /**
   * Retourner la liste des transporteur pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeTransporteur
   */
  public ListeTransporteur chargerListeTransporteur(CriteresRechercheTransporteurs pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
            + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' " + " and PARETB = '"
            + pCriteres.getCodeEtablissement() + "'" + " and PARTYP = 'TR' ORDER BY PARZ2", PACKAGE_PERSONNALISATION + "DsTransport");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeTransporteur listeTransporteur = new ListeTransporteur();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      listeTransporteur.add(initRechercheTransporteurs(pCriteres, record));
    }
    return listeTransporteur;
  }
  
  /**
   * Retourner la liste des lieu transport pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeLieuTransport
   */
  public ListeLieuTransport chargerListeLieuTransport(CritereLieuTransport pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
            + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' " + " and PARETB = '"
            + pCriteres.getIdEtablissement() + "'" + " and PARTYP = 'LV' ORDER BY PARZ2", PACKAGE_PERSONNALISATION + "DsLieuTransport");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeLieuTransport listeLieuTransport = new ListeLieuTransport();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      listeLieuTransport.add(initRechercheLieuTransport(pCriteres, record));
    }
    return listeLieuTransport;
  }
  
  /**
   * Retourne la liste des tarifs pour un établissement donnée.
   * @param pCriteres Critères de recherche
   * @return ListeTarif
   */
  public ListeTarif chargerListeTarif(CriteresRechercheTarifs pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        RequetesRechercheTarifs.getRequete(queryManager.getLibrary(), pCriteres), PACKAGE_PERSONNALISATION + "DsTarif");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeTarif listeTarif = new ListeTarif();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      Tarif tarif = initRechercheTarifs(pCriteres, record);
      listeTarif.add(tarif);
    }
    return listeTarif;
  }
  
  /**
   * Retourne un tarif en particulier.
   * @param pIdEtablissement identifiant de l'établissement
   * @param pCodeTarif code du tarif
   * @return Tarif
   */
  public Tarif chargerTarif(IdEtablissement pIdEtablissement, String pCodeTarif) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("Le paramètre aCodeEtablisssement du service est à null.");
    }
    if (pCodeTarif == null) {
      throw new MessageErreurException("Le paramètre aCodeTarif du service est à null.");
    }
    
    String requete = "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + curlib + '.'
        + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'TA'" + " and PARETB = '"
        + pIdEtablissement.getCodeEtablissement() + "'" + " and PARIND = '" + pCodeTarif + "'";
    
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(requete, PACKAGE_PERSONNALISATION + "DsTarif");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ExtendRecord record = new ExtendRecord();
    record.setRecord(listeRecord.get(0));
    return initRechercheTarifs(null, record);
  }
  
  /**
   * Retourner la liste des catégorie zone personnalisée pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeCategorieZonePersonnalisee
   */
  public ListeCategorieZonePersonnalisee chargerListeCategorieZonePersonnalisee(CritereCategorieZonePersonnalisee pCritere) {
    if (pCritere == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary() + '.'
            + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' " + " and PARETB = '"
            + pCritere.getIdEtablissement().getCodeEtablissement() + "'" + " and PARTYP = 'ZP' ORDER BY PARZ2",
        PACKAGE_PERSONNALISATION + "DsZonePersonnalisee");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeCategorieZonePersonnalisee listeCategorie = new ListeCategorieZonePersonnalisee();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      CategorieZonePersonnalisee categorieZP = new CategorieZonePersonnalisee(IdCategorieZonePersonnalisee
          .getInstance(IdEtablissement.getInstance(((String) record.getField("PARETB")).trim()), ((String) record.getField("PARIND"))));
      categorieZP.setNom(((String) record.getField("LIB")).trim());
      listeCategorie.add(categorieZP);
    }
    
    return listeCategorie;
  }
  
  /**
   * Charger la liste des paramètres systèmes d'un établissement.
   * @param pIdEtablissement identifiant de l'établissement
   * @return ListeParametreSysteme
   */
  public ListeParametreSysteme chargerListeParametreSysteme(IdEtablissement pIdEtablissement) {
    // Contrôler les paramètres
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    // Exécuter la requête
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary() + '.'
            + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARETB = "
            + RequeteSql.formaterStringSQL(pIdEtablissement.getCodeEtablissement()) + " and PARTYP = 'PS'",
        PACKAGE_PERSONNALISATION + "DsPersonnalisation");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // Concatèner les champs "PST" trouvés suite à la requête
    StringBuilder chaine = new StringBuilder();
    for (int r = 0; r < listeRecord.size(); r++) {
      ExtendRecord record = new ExtendRecord();
      record.setRecord(listeRecord.get(r));
      chaine.append((String) record.getField("PST"));
    }
    
    // Renseigner la liste des paramètres systèmes
    ListeParametreSysteme listeParametreSysteme = new ListeParametreSysteme();
    if (chaine.length() > 0) {
      for (int index = 0; index < chaine.length(); index++) {
        // Incrémenter le numéro car les PS vont de 1 à XXX et non de 0 à XXX
        int numero = index + 1;
        
        // Chercher l'enum correspondant à ce numéro de paramètre système
        // Si l'enum correspondant au numéro n'a pas été trouvé c'est soit un oubli soit qu'il n'existe pas
        // NB : Code pas indispensable car on pourrait aussi charger tous les PS même si l'Enum n'est pas renseigné.
        EnumParametreSysteme enumParametreSysteme = EnumParametreSysteme.valueOfByNumero(numero);
        if (enumParametreSysteme == null) {
          continue;
        }
        
        // Créer l'identifiant du paramètre système
        IdParametreSysteme idParametreSysteme = IdParametreSysteme.getInstance(pIdEtablissement, numero);
        
        // Créer le paramètre système
        ParametreSysteme parametreSysteme = new ParametreSysteme(idParametreSysteme);
        parametreSysteme.setValeur(chaine.charAt(index));
        
        // L'ajouter à la liste
        listeParametreSysteme.add(parametreSysteme);
      }
    }
    return listeParametreSysteme;
  }
  
  /**
   * Contrôler si un paramètre système de type A donné est actif.
   * @param pEnumParametreSysteme type de paramètre système
   * @return boolean
   */
  public boolean isParametreSystemeActifSurUnEtablissement(EnumParametreSysteme pEnumParametreSysteme) {
    // Contrôler les paramètres
    if (pEnumParametreSysteme == null) {
      return false;
    }
    if (pEnumParametreSysteme.getTypeValeur() != EnumTypeValeurPersonnalisation.TYPE_A) {
      throw new MessageErreurException("Le paramètre système " + pEnumParametreSysteme + "  n'est pas de type A.");
    }
    if (!isTableExiste(queryManager.getLibrary(), EnumTableBDD.PARAMETRE_GESCOM)) {
      return false;
    }
    
    // Exécuter la requête
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure(
            "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary() + '.'
                + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'PS'",
            PACKAGE_PERSONNALISATION + "DsPersonnalisation");
    if (listeRecord == null || listeRecord.isEmpty()) {
      return false;
    }
    
    // Concatèner les champs "PST" trouvés pour un même établissement puis stockage dans une liste
    ArrayList<String> listeChainePS = new ArrayList<String>();
    StringBuffer chaine = new StringBuffer();
    ExtendRecord record = new ExtendRecord();
    record.setRecord(listeRecord.get(0));
    String codeEtablissement = (String) record.getField("PSETB");
    for (int r = 0; r < listeRecord.size(); r++) {
      record = new ExtendRecord();
      record.setRecord(listeRecord.get(r));
      String codeEtablissementEnCours = (String) record.getField("PSETB");
      // Si l'établissement à changé
      if (!codeEtablissement.equals(codeEtablissementEnCours)) {
        listeChainePS.add(chaine.toString());
        chaine.setLength(0);
        codeEtablissement = codeEtablissementEnCours;
      }
      chaine.append((String) record.getField("PST"));
    }
    // Stockage de la dernière chaine traitée
    listeChainePS.add(chaine.toString());
    
    // Contrôler si le paramètre système est actif dans au moins un établissement
    int index = pEnumParametreSysteme.getNumero().intValue() - 1;
    for (String chainePs : listeChainePS) {
      if (chainePs.isEmpty()) {
        continue;
      }
      // Récupération de la valeur à la position du paramètre système
      char valeur = chainePs.charAt(index);
      if (valeur == '1') {
        return true;
      }
      
    }
    return false;
  }
  
  /**
   * Retourner la liste des catégorie fournisseur pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeCategorieFournisseur
   */
  public ListeCategorieFournisseur chargerListeCategorieFournisseur(CriteresRechercheCategoriesFournisseur pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord = queryManager
        .selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary()
            + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARETB = '" + pCriteres.getCodeEtablissement()
            + "' and PARTYP = 'CF' ORDER BY PARZ2 ", PACKAGE_PERSONNALISATION + "DsCategorieFournisseur");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On alimente le tableau avec les enregistrements trouvés
    ListeCategorieFournisseur listeCategorieFournisseur = new ListeCategorieFournisseur();
    ExtendRecord extendRecord = new ExtendRecord();
    for (Record record : listeRecord) {
      extendRecord.setRecord(record);
      listeCategorieFournisseur.add(initRechercheCategorieFournisseur(pCriteres, extendRecord));
    }
    return listeCategorieFournisseur;
  }
  
  /**
   * Charge les informations détaillées d'un mode d'expédition suivant son code
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pModeExpe mode d'expédition
   * @return ModeExpedition
   */
  public ModeExpedition chargerModeExpedition(ModeExpedition pModeExpe) {
    if (pModeExpe == null) {
      return null;
    }
    
    ModeExpedition modeExpedition = pModeExpe;
    
    if (modeExpedition.getId().getCode().trim().equals("")) {
      ModeExpedition nonDefini = new ModeExpedition(IdModeExpedition.getInstance(""));
      nonDefini.setLibelle("Non Défini");
      nonDefini.setIsEnlevement(false);
      return nonDefini;
    }
    
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary() + '.'
            + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'EX' ORDER BY PARZ2",
        PACKAGE_PERSONNALISATION + "DsModeExpedition");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    new ArrayList<ModeExpedition>();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      modeExpedition = initRechercheModeExpedition(new CriteresModeExpedition(), record);
      if (modeExpedition.getId().equals(pModeExpe.getId())) {
        return modeExpedition;
      }
    }
    return modeExpedition;
  }
  
  /**
   * Retourner la liste des mode d'expedition pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeModeExpedition
   */
  public ListeModeExpedition chargerListeModeExpedition(CriteresModeExpedition pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary() + '.'
            + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'EX' ORDER BY PARZ2 ",
        PACKAGE_PERSONNALISATION + "DsModeExpedition");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    ListeModeExpedition listeModesExpedition = new ListeModeExpedition();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      listeModesExpedition.add(initRechercheModeExpedition(pCriteres, record));
    }
    return listeModesExpedition;
  }
  
  /**
   * Retourne une unité sur la base de son identifiant
   * @param pIdUnite identifiant de l'unité
   * @return Unite
   */
  public Unite chargerUnite(IdUnite pIdUnite) {
    if (pIdUnite == null) {
      return null;
    }
    
    final ArrayList<GenericRecord> listeRecord = queryManager
        .select("SELECT SUBSTR(PARZ2, 1, 30) AS LIBELLE, SUBSTR(PARZ2, 82, 1) AS NBRDECIMALE  FROM " + queryManager.getLibrary() + '.'
            + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'UN' AND PARIND = '" + pIdUnite.getCode() + "' ");
    if ((listeRecord == null) || (listeRecord.size() != 1)) {
      return null;
    }
    
    GenericRecord record = listeRecord.get(0);
    if (record == null || record.getField("LIBELLE") == null) {
      return null;
    }
    Unite unite = new Unite(pIdUnite);
    unite.setLibelleCourt(record.getField("LIBELLE").toString().trim());
    unite.setNombreDecimale(Integer.parseInt(record.getField("NBRDECIMALE").toString()));
    return unite;
  }
  
  /**
   * Retourne la liste des types d'avoir (AV) d'un établissement donné
   * @param pIdEtablissement identifiant de l'établissement
   * @return ListeTypeAvoir
   */
  public ListeTypeAvoir chargerListeTypeAvoir(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
            + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'AV' and PARETB = '"
            + pIdEtablissement.getCodeEtablissement() + "' ORDER BY PARZ2", PACKAGE_PERSONNALISATION + "DsTypeAvoir");
    if (listeRecord == null) {
      return null;
    }
    
    ListeTypeAvoir listeTypeAvoir = new ListeTypeAvoir();
    try {
      for (Record record : listeRecord) {
        ExtendRecord extendedRecord = new ExtendRecord();
        extendedRecord.setRecord(record);
        IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) record.getField("AVETB")));
        IdTypeAvoir idTypeAvoir = IdTypeAvoir.getInstance(idEtablissement, (String) record.getField("AVCOD"));
        TypeAvoir typeAvoir = new TypeAvoir(idTypeAvoir);
        
        typeAvoir.setLibelle(((String) record.getField("AVLIB")).trim());
        typeAvoir.setTitreColonne(((String) record.getField("AVTIT")).trim());
        typeAvoir.setIncrementPostesComptabilisation(Integer.parseInt((String) record.getField("AVICO")));
        if (!((String) record.getField("AVMAT")).trim().isEmpty()) {
          typeAvoir.setIdMagasinTransfert(IdMagasin.getInstance(idEtablissement, (String) record.getField("AVMAT")));
        }
        typeAvoir.setIsTypeRetour(!((String) record.getField("AVRET")).trim().equals(""));
        listeTypeAvoir.add(typeAvoir);
      }
    }
    catch (UnsupportedEncodingException e) {
    }
    return listeTypeAvoir;
  }
  
  /**
   * Retourner la liste des zones géographiques correspondants aux critères.
   * @param pCriteres Critères de recherche
   * @return ListeZoneGeographique
   */
  public ListeZoneGeographique chargerListeZoneGeographique(CritereZoneGeographique pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    // Préparer la requête de récupération des zones géographiques
    // @formatter:off
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
        + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where ");
    requeteSql.ajouterConditionAnd("PARTOP", "<>", "9");
    requeteSql.ajouterConditionAnd("PARETB", "=", pCriteres.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("PARTYP", "=", "ZG");
    requeteSql.ajouter("ORDER BY PARZ2");
    // @formatter:on
    
    ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure(requeteSql.getRequeteLectureSeule(), PACKAGE_PERSONNALISATION + "DsZoneGeographique");
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Récupérer la liste des communes des zones géographiques d'un établissement
    // Après la récupération de cette liste, on fera la correspondance avec les zones géographiques
    CritereCommuneZoneGeographique critereCommuneZoneGeographique = new CritereCommuneZoneGeographique();
    critereCommuneZoneGeographique.setIdEtablissement(pCriteres.getIdEtablissement());
    SqlCommuneZoneGeographique sqlCommuneZoneGeographique = new SqlCommuneZoneGeographique(queryManager);
    ListeCommuneZoneGeographique listeCommuneZoneGeographique =
        sqlCommuneZoneGeographique.chargerListeCommuneZoneGeographique(critereCommuneZoneGeographique);
    
    // Initialisation de la liste
    ListeZoneGeographique listeZoneGeographique = new ListeZoneGeographique();
    // S'il existe des zones géographiques en base, alors alimentation le tableau avec les enregistrements trouvés
    ExtendRecord extendRecord = new ExtendRecord();
    for (Record record : listeRecord) {
      extendRecord.setRecord(record);
      // C'est plus performant de faire la correspondance des zones et communes en java
      // que d'appeler une requête sql pour chaque zone géographique
      listeZoneGeographique.add(initialiserZoneGeographique(extendRecord, listeCommuneZoneGeographique));
    }
    return listeZoneGeographique;
  }
  
  /**
   * Retourner la liste des zones géographiques à partir d'une liste d'IdZoneGeographique.
   * @param pListeIdZoneGeographique liste d'identifiants de zones géographiques
   * @return ListeZoneGeographique
   *         Méthode à revoir, le chargement des zones géographiques par un extendRecord force à recharger l'ensemble de la table à chaque
   *         fois,
   *         et à filtrer en conséquence sur les données de l'IdZoneGeoographique
   */
  public ListeZoneGeographique chargerListeZoneGeographique(List<IdZoneGeographique> pListeIdZoneGeographique) {
    if (pListeIdZoneGeographique == null) {
      return null;
    }
    // Le critère "Etablissement" est le critère unique et obligatoire actuellement pour charger la liste à partir de la table PGVPARM.
    // On récupère l'établissement dans le premier IdZoneGeographique de la liste.
    IdEtablissement idEtablissement = pListeIdZoneGeographique.get(0).getIdEtablissement();
    
    // Préparer la requête de récupération des zones géographiques
    // @formatter:off
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
        + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where ");
    requeteSql.ajouterConditionAnd("PARTOP", "<>", "9");
    requeteSql.ajouterConditionAnd("PARETB", "=", idEtablissement.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("PARTYP", "=", "ZG");
    requeteSql.ajouter("ORDER BY PARZ2");
    // @formatter:on
    
    ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure(requeteSql.getRequeteLectureSeule(), PACKAGE_PERSONNALISATION + "DsZoneGeographique");
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Récupérer la liste des communes des zones géographiques d'un établissement
    // Après la récupération de cette liste, on fera la correspondance avec les zones géographiques
    CritereCommuneZoneGeographique critereCommuneZoneGeographique = new CritereCommuneZoneGeographique();
    critereCommuneZoneGeographique.setIdEtablissement(idEtablissement);
    SqlCommuneZoneGeographique sqlCommuneZoneGeographique = new SqlCommuneZoneGeographique(queryManager);
    ListeCommuneZoneGeographique listeCommuneZoneGeographique =
        sqlCommuneZoneGeographique.chargerListeCommuneZoneGeographique(critereCommuneZoneGeographique);
    
    // Alimentation le tableau avec les enregistrements trouvés
    ListeZoneGeographique listeZoneGeographique = new ListeZoneGeographique();
    ExtendRecord extendRecord = new ExtendRecord();
    for (IdZoneGeographique idZoneGeographique : pListeIdZoneGeographique) {
      for (Record record : listeRecord) {
        extendRecord.setRecord(record);
        if (Constantes.equals(extendRecord.getField("ZGETB"), idZoneGeographique.getCodeEtablissement())
            && Constantes.equals(extendRecord.getField("ZGCOD").toString(), idZoneGeographique.getCode())) {
          // C'est plus performant de faire la correspondance des zones et communes en java
          // que d'appeler une requête sql pour chaque zone géographique
          listeZoneGeographique.add(initialiserZoneGeographique(extendRecord, listeCommuneZoneGeographique));
        }
      }
    }
    return listeZoneGeographique;
  }
  
  /**
   * Retourner la liste des IdZoneGeopgraphique correspondants aux critères.
   * @param pCriteres Critères de recherche
   * @return List<IdZoneGeographique>
   */
  public List<IdZoneGeographique> chargerListeIdZoneGeographique(CritereZoneGeographique pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    // Préparer la requête de récupération des id zones géographique
    // @formatter:off
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
        + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where ");
    requeteSql.ajouterConditionAnd("PARTOP", "<>", "9");
    requeteSql.ajouterConditionAnd("PARETB", "=", pCriteres.getCodeEtablissement());
    requeteSql.ajouterConditionAnd("PARTYP", "=", "ZG");
    requeteSql.ajouter("ORDER BY PARZ2");
    // @formatter:on
    
    ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure(requeteSql.getRequeteLectureSeule(), PACKAGE_PERSONNALISATION + "DsZoneGeographique");
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Alimenter la liste avec les enregistrements trouvés.
    List<IdZoneGeographique> listeIdZoneGeographique = new ArrayList<IdZoneGeographique>();
    ExtendRecord extendRecord = new ExtendRecord();
    
    for (Record record : listeRecord) {
      extendRecord.setRecord(record);
      listeIdZoneGeographique.add(completerIdZoneGeographique(extendRecord));
    }
    return listeIdZoneGeographique;
  }
  
  /**
   * Retourner la liste des formules prix correspondants aux critères.
   * @param pCriteres Critères de recherche
   * @return ListeFormulePrix
   */
  public ListeFormulePrix chargerListeFormulePrix(CritereFormulePrix pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    // @formatter:off
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
        + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where ");
    requeteSql.ajouterConditionAnd("PARTOP", "<>", "9");
    requeteSql.ajouterConditionAnd("PARTYP", "=", "FP");
    // Filtre sur l'établissement
    if (pCriteres.getIdEtablissement() != null) {
      requeteSql.ajouterConditionAnd("PARETB", "=", pCriteres.getIdEtablissement().getCodeEtablissement());
    }
    requeteSql.ajouter("ORDER BY PARZ2");
    // @formatter:on
    
    ArrayList<Record> listeRecord =
        queryManager.selectAvecDataStructure(requeteSql.getRequeteLectureSeule(), PACKAGE_PERSONNALISATION + "DsFormulePrix");
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Alimentation de la liste avec les enregistrements trouvés et valides
    ListeFormulePrix listeFormulePrix = new ListeFormulePrix();
    ExtendRecord extendRecord = new ExtendRecord();
    for (Record record : listeRecord) {
      extendRecord.setRecord(record);
      FormulePrix formulePrix = initialiserFormulePrix(extendRecord);
      if (formulePrix == null) {
        continue;
      }
      listeFormulePrix.add(formulePrix);
    }
    return listeFormulePrix;
  }
  
  /**
   * Retourner la liste des conditions de commissionnement pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * @param pCriteres Critères de recherche
   * @return ListeConditionCommissionnement
   */
  public ListeConditionCommissionnement chargerListeConditionCommissionnement(CriteresConditionCommissionnement pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord = queryManager
        .selectAvecDataStructure("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + queryManager.getLibrary()
            + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARETB = '" + pCriteres.getCodeEtablissement()
            + "' and PARTYP = 'CR' ORDER BY PARZ2", PACKAGE_PERSONNALISATION + "DsConditionCommissionnement");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    ListeConditionCommissionnement listeConditionCommissionnement = new ListeConditionCommissionnement();
    for (Record record : listeRecord) {
      ExtendRecord extendedRecord = new ExtendRecord();
      extendedRecord.setRecord(record);
      ConditionCommissionnement commissionnement = initConditionCommissionnement(pCriteres, extendedRecord);
      listeConditionCommissionnement.add(commissionnement);
    }
    return listeConditionCommissionnement;
  }
  
  /**
   * Charger une liste de participations aux frais d'expédition.
   * @param pCriteres : CriteresParticipationFraisExpedition
   * @return ListeParticipationFraisExpedition
   */
  public ListeParticipationFraisExpedition chargerListeParticipationFraisExpedition(CriteresParticipationFraisExpedition pCriteres) {
    if (pCriteres == null) {
      return null;
    }
    
    // @formatter:off
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
        + queryManager.getLibrary() + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where ");
    requeteSql.ajouterConditionAnd("PARTOP", "<>", "9");
    requeteSql.ajouterConditionAnd("PARETB", "=", pCriteres.getIdEtablissement().getCodeEtablissement());
    requeteSql.ajouterConditionAnd("PARTYP", "=", "PE");
    requeteSql.ajouter("ORDER BY PARZ2");
    // @formatter:on
    ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(requeteSql.getRequeteLectureSeule(),
        PACKAGE_PERSONNALISATION + "DsParticipationFraisExpedition");
    ListeParticipationFraisExpedition listeParticipationFraisExpedition = new ListeParticipationFraisExpedition();
    if (listeRecord == null || listeRecord.isEmpty()) {
      return listeParticipationFraisExpedition;
    }
    
    // Alimentation le tableau avec les enregistrements trouvés
    ExtendRecord extendRecord = new ExtendRecord();
    for (Record record : listeRecord) {
      extendRecord.setRecord(record);
      try {
        listeParticipationFraisExpedition.add(initialiserParticipationFraisExpedition(extendRecord));
      }
      catch (Exception e) {
        try {
          Trace.erreur(e, "Un enregistrement de personnalisation PE n'a pu être lu : "
              + pCriteres.getIdEtablissement().getCodeEtablissement() + " - " + ((String) record.getField("PARIND")));
        }
        catch (UnsupportedEncodingException e1) {
          Trace.erreur(e1, "Un enregistrement de personnalisation PE n'a pu être lu : code participation invalide");
        }
      }
    }
    return listeParticipationFraisExpedition;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Initialise un vendeur à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return Vendeur
   */
  private Vendeur initRechercheVendeurs(CriteresRechercheVendeurs pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("PARETB")));
    IdVendeur idVendeur = IdVendeur.getInstance(idEtablissement, ((String) pRecord.getField("PARIND")));
    Vendeur vendeur = new Vendeur(idVendeur);
    vendeur.setNom(((String) pRecord.getField("VDLIB")).trim());
    vendeur.setValiditeDevis(pRecord.getBigDecimalValue("VDVDV", BigDecimal.ZERO).intValue());
    try {
      vendeur.setUniteValiditeDevis(EnumUnitePeriode.valueOfByCode(((String) pRecord.getField("VDVDP")).trim()));
    }
    catch (Exception e) {
      throw new MessageErreurException("Dans le paramètre Vendeur dont le code est : " + vendeur.getId().getTexte()
          + "\nle champ 'unité de période de validité des devis' est invalide : " + ((String) pRecord.getField("VDVDP")).trim());
    }
    vendeur.setPourcentageMaximalRemise(pRecord.getBigDecimalValue("VDPMR", BigDecimal.ZERO).intValue());
    vendeur.setNumeroTelephone(((String) pRecord.getField("VDTEL")).trim());
    vendeur.setAdresseEmail(((String) pRecord.getField("VDMAI")).trim());
    
    return vendeur;
  }
  
  /**
   * Initialise un acheteur à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return Acheteur
   */
  private Acheteur initRechercheAcheteur(CriteresRechercheAcheteur pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("PARETB")));
    IdAcheteur idAcheteur = IdAcheteur.getInstance(idEtablissement, ((String) pRecord.getField("PARIND")));
    Acheteur acheteur = new Acheteur(idAcheteur);
    acheteur.setNom(((String) pRecord.getField("AHLIB")).trim());
    acheteur.setNumeroTelephone(((String) pRecord.getField("AHTEL")).trim());
    acheteur.setAdresseEmail(((String) pRecord.getField("AHMAI")).trim());
    
    return acheteur;
  }
  
  /**
   * Initialise un règlement à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return ModeReglement
   */
  private ModeReglement initRechercheReglements(CritereModeReglement pCriteres, ExtendRecord pRecord) {
    String codeReglement = ((String) pRecord.getField("PARIND")).trim();
    Character codeTypeReglement = ((String) pRecord.getField("RGTYP")).charAt(0);
    Character codeSensReglement = ((String) pRecord.getField("RGSNS")).charAt(0);
    
    IdModeReglement idReglement = IdModeReglement.getInstance(codeReglement);
    ModeReglement reglement = new ModeReglement(idReglement);
    reglement.setLibelle(((String) pRecord.getField("RGLIB")).trim());
    try {
      reglement.setTypeReglement(EnumTypeReglement.valueOfByCode(codeTypeReglement));
    }
    catch (Exception e) {
      throw new MessageErreurException("Dans le paramètre Mode de réglement dont le code est : " + reglement.getId().getTexte()
          + "\nle champ 'type de réglement' est invalide : " + codeTypeReglement);
    }
    try {
      reglement.setSensReglement(EnumSensReglement.valueOfByCode(codeSensReglement));
    }
    catch (Exception e) {
      throw new MessageErreurException("Dans le paramètre Mode de réglement dont le code est : " + reglement.getId().getTexte()
          + "\nle champ 'Sens du réglement' est invalide : " + codeSensReglement);
    }
    return reglement;
  }
  
  /**
   * Initialise un groupe à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return Groupe
   */
  private Groupe initialiserGroupe(CritereGroupe pCritere, ExtendRecord record) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) record.getField("GRETB")));
    IdGroupe idGroupe = IdGroupe.getInstance(idEtablissement, ((String) record.getField("GRCOD")));
    Groupe groupe = new Groupe(idGroupe);
    groupe.setLibelle(((String) record.getField("GRLIB")).trim());
    groupe.setCodeTVA(((BigDecimal) record.getField("GRTVA")).intValue());
    if (!((String) record.getField("GRUNS")).trim().isEmpty()) {
      IdUnite idUnite = IdUnite.getInstance(((String) record.getField("GRUNS")));
      groupe.setIdUniteVente(idUnite);
    }
    groupe.setCoefficientVente(record.getBigDecimalValue("GRCMAG", BigDecimal.ZERO));
    groupe.setCoefficientPRV(record.getBigDecimalValue("GRCPR", BigDecimal.ZERO));
    
    return groupe;
  }
  
  /**
   * Initialise une affaire à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return Affaire
   */
  private Affaire initRechercheAffaire(CriteresRechercheAffaires pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("PARETB")));
    IdAffaire idAffaire = IdAffaire.getInstance(idEtablissement, ((String) pRecord.getField("PARIND")));
    Affaire affaire = new Affaire(idAffaire);
    affaire.setNom(((String) pRecord.getField("ACLIB")).trim());
    affaire.setId(idAffaire);
    
    return affaire;
  }
  
  /**
   * Initialise une section analytique à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return SectionAnalytique
   */
  private SectionAnalytique initRechercheSectionAnalytique(CriteresRechercheSectionAnalytique pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("PARETB")));
    idSectionAnalytique idSection = idSectionAnalytique.getInstance(idEtablissement, ((String) pRecord.getField("PARIND")));
    SectionAnalytique section = new SectionAnalytique(idSection);
    section.setNom(((String) pRecord.getField("SALIB")).trim());
    
    return section;
  }
  
  /**
   * Initialise un canal de vente à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return CanalDeVente
   */
  private CanalDeVente initRechercheCanalDeVente(CriteresRechercheCanalDeVente pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("PARETB")));
    IdCanalDeVente idCanal = IdCanalDeVente.getInstance(idEtablissement, ((String) pRecord.getField("PARIND")));
    CanalDeVente canal = new CanalDeVente(idCanal);
    canal.setNom(((String) pRecord.getField("KVLIB")).trim());
    
    return canal;
  }
  
  /**
   * Initialise un type de gratuit à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return TypeGratuit
   */
  private TypeGratuit initRechercheTypeGratuit(CriteresRechercheTypeGratuit pCriteres, ExtendRecord pRecord) {
    IdTypeGratuit idGratuit = IdTypeGratuit.getInstance(((String) pRecord.getField("PARIND")));
    TypeGratuit typeGratuit = new TypeGratuit(idGratuit);
    Character codeEditionFacture = ((String) pRecord.getField("TGTOP1")).charAt(0);
    Character codeEditionLibelleComplet = ((String) pRecord.getField("TGTOP3")).charAt(0);
    Character codeEditionStatistique = ((String) pRecord.getField("TGIN5")).charAt(0);
    
    typeGratuit.setLibelle(((String) pRecord.getField("TGLIB")).trim());
    try {
      typeGratuit.setEditionFacture(EnumNonEditionTypeGratuit.valueOfByCode(codeEditionFacture));
    }
    catch (Exception e) {
      throw new MessageErreurException("Dans le paramètre type de gratuit (TG) dont le code est : " + typeGratuit.getId().getTexte()
          + "\nle champ 'non édition sur bons et factures' est invalide : " + codeEditionFacture);
    }
    typeGratuit.setEditionPrix(((String) pRecord.getField("TGTOP2")).trim());
    try {
      typeGratuit.setEditionLibelleComplet(EnumEditionLibelleCompletTypeGratuit.valueOfByCode(codeEditionLibelleComplet));
    }
    catch (Exception e) {
      throw new MessageErreurException("Dans le paramètre type de gratuit (TG) dont le code est : " + typeGratuit.getId().getTexte()
          + "\nle champ 'édition du libelle complet' est invalide : " + codeEditionLibelleComplet);
    }
    typeGratuit.setEditionMagTransfert(((String) pRecord.getField("TGMAG")).trim());
    try {
      typeGratuit.setEditionStatistique(EnumNonGereStatistiqueTypeGratuit.valueOfByCode(codeEditionStatistique));
    }
    catch (Exception e) {
      throw new MessageErreurException("Dans le paramètre type de gratuit (TG) dont le code est : " + typeGratuit.getId().getTexte()
          + "\nle champ 'non géré en statistiques' est invalide : " + codeEditionStatistique);
    }
    typeGratuit.setEditionEcoTaxe(((String) pRecord.getField("TGTOP4")).trim());
    typeGratuit.setEditionCommentaire(((String) pRecord.getField("TGARTC")).trim());
    
    return typeGratuit;
  }
  
  /**
   * Initialise une famille à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return Famille
   */
  private Famille initialiserFamilles(CritereFamille pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("FAETB")));
    IdFamille idFamille = IdFamille.getInstance(idEtablissement, ((String) pRecord.getField("FACOD")));
    Famille famille = new Famille(idFamille);
    if (pCriteres.isLibelleComplet()) {
      famille.setLibelle(((String) pRecord.getField("FACOD")).trim() + " - " + ((String) pRecord.getField("FALIB")).trim());
    }
    else {
      famille.setLibelle(((String) pRecord.getField("FALIB")).trim());
    }
    famille.setCodeTVA(((BigDecimal) pRecord.getField("FATVA")).intValue());
    if (!pRecord.getField("FAUNS").toString().trim().isEmpty()) {
      IdUnite idUnite = IdUnite.getInstance(pRecord.getField("FAUNS").toString());
      famille.setIdUniteVente(idUnite);
    }
    famille.setCoefficientVente(pRecord.getBigDecimalValue("FACPR", BigDecimal.ZERO));
    famille.setCoefficientPRV(pRecord.getBigDecimalValue("FACPR", BigDecimal.ZERO));
    
    return famille;
  }
  
  /**
   * Initialise une sous-famille à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return SousFamille
   */
  private SousFamille initialiserSousFamilles(CritereSousFamilles pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("SFETB")));
    IdSousFamille idSousFamille = IdSousFamille.getInstance(idEtablissement, ((String) pRecord.getField("SFCOD")).trim());
    SousFamille sousfamille = new SousFamille(idSousFamille);
    if (pCriteres.isLibelleComplet()) {
      sousfamille.setLibelle(((String) pRecord.getField("SFCOD")).trim() + " - " + ((String) pRecord.getField("SFLIB")).trim());
    }
    else {
      sousfamille.setLibelle(((String) pRecord.getField("SFLIB")).trim());
    }
    sousfamille.setTaxeParaFiscale(pRecord.getBigDecimalValue("SFTPF", BigDecimal.ZERO));
    sousfamille.setTypeIntervention(((String) pRecord.getField("SFTYI")).trim());
    sousfamille.setGarantie(((String) pRecord.getField("SFCG")).trim());
    
    return sousfamille;
  }
  
  /**
   * Initialise un magasin à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return Magasin
   */
  private Magasin initialiserMagasin(ExtendRecord record) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) record.getField("MAETB")));
    Magasin magasin = new Magasin(IdMagasin.getInstance(idEtablissement, ((String) record.getField("MACOD")).trim()));
    magasin.setNom(((String) record.getField("MALIB")));
    if (((String) record.getField("MAREP")) != null && !((String) record.getField("MAREP")).trim().isEmpty()) {
      IdRepresentant idRepresentant = IdRepresentant.getInstance(idEtablissement, ((String) record.getField("MAREP")).trim());
      magasin.setIdRepresentant(idRepresentant);
    }
    return magasin;
  }
  
  /**
   * Initialise un motif de retour à partir de l'enregistrement donné.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return MotifRetour
   */
  private MotifRetour initMotifRetour(ExtendRecord record) {
    if (record == null || ((String) record.getField("AVCOD")).trim().isEmpty()) {
      return null;
    }
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) record.getField("AVETB")));
    IdMotifRetour idMotifRetour = IdMotifRetour.getInstance(idEtablissement, ((String) record.getField("AVCOD")));
    MotifRetour motif = new MotifRetour(idMotifRetour);
    
    motif.setLibelle(((String) record.getField("AVLIB")).trim());
    motif.setTitreColonne(((String) record.getField("AVTIT")).trim());
    motif.setIncrementPostesComptabilisation(((BigDecimal) record.getField("AVICO")));
    if (!((String) record.getField("AVMAT")).trim().isEmpty()) {
      motif.setMagasinTransfert(IdMagasin.getInstance(idEtablissement, ((String) record.getField("AVMAT")).trim()));
    }
    motif.setRetour(!((String) record.getField("AVRET")).trim().isEmpty());
    
    return motif;
  }
  
  /**
   * Initialise une unité à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return Unite
   */
  private Unite initRechercheUnites(CriteresRechercheUnites pCriteres, ExtendRecord pRecord) {
    if (pRecord == null || ((String) pRecord.getField("UNCOD")).trim().isEmpty()) {
      return null;
    }
    
    IdUnite idUnite = IdUnite.getInstance(((String) pRecord.getField("UNCOD")));
    Unite unite = new Unite(idUnite);
    switch (pCriteres.getTypeRecherche()) {
      case CriteresRechercheUnites.RECHERCHE_COMPTOIR:
        unite.setLibelleLong(((String) pRecord.getField("UNLIB")).trim());
        unite.setLibelleCourt(((String) pRecord.getField("UNLIB2")).trim());
        unite.setNombreDecimale(((BigDecimal) pRecord.getField("UNDEC")).intValue());
        break;
    }
    
    return unite;
  }
  
  /**
   * Initialise une catégorie client à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return CategorieClient
   */
  private CategorieClient initRechercheCategoriesClients(CriteresRechercheCategoriesClients pCriteres, ExtendRecord pRecord) {
    
    switch (pCriteres.getTypeRecherche()) {
      case CriteresRechercheCategoriesClients.RECHERCHE_COMPTOIR:
        IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("CCETB")));
        IdCategorieClient idCategorieClient = IdCategorieClient.getInstance(idEtablissement, ((String) pRecord.getField("CCCOD")));
        CategorieClient categorie = new CategorieClient(idCategorieClient);
        categorie.setLibelle(((String) pRecord.getField("CCLIB")).trim());
        categorie.setChantierObligatoire(!((String) pRecord.getField("CCCHAN")).trim().isEmpty());
        categorie.setPourcentageAcompte(((BigDecimal) pRecord.getField("CCRGL")).intValue());
        categorie.setAcompteObligatoire(!((String) pRecord.getField("CCACO")).trim().isEmpty());
        categorie.setPourcentageAcompteDetectionArticleSpecial(((BigDecimal) pRecord.getField("CCRGLS")).intValue());
        categorie.setAcompteObligatoireArticleSpeciauxPresent(!((String) pRecord.getField("CCACOS")).trim().isEmpty());
        return categorie;
    }
    
    return null;
  }
  
  /**
   * Initialise un groupe de condition de vente à partir de l'enregistrement donné.
   * Le traitement du filtrage sur les critères (hors établissement) s'effectue ici.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return GroupeConditionVente
   */
  private GroupeConditionVente initialiserGroupeConditionVente(CritereGroupeConditionVente pCriteres, ExtendRecord pRecord) {
    try {
      // Lecture et conversion des dates pour faciliter le filtrage éventuel
      int valeurMax = (int) Constantes.valeurMaxZoneNumerique(7);
      int dateDebut = pRecord.getBigDecimalValue("CNPERD", BigDecimal.ZERO).intValue();
      int dateFin = pRecord.getBigDecimalValue("CNPERF", BigDecimal.valueOf(valeurMax)).intValue();
      if (dateFin == 0) {
        dateFin = valeurMax;
      }
      // Le type du groupe de la condition pour le filtrage éventuel
      String valeur = (String) pRecord.getField("CNTYP");
      EnumTypeGroupeConditionVente typeGroupeConditionVente = EnumTypeGroupeConditionVente.valueOfByCode(valeur.charAt(0));
      
      // Filtrage sur la date d'application
      if (pCriteres.getDateApplication() != null) {
        int filtreDate = ConvertDate.dateToDb2(pCriteres.getDateApplication());
        if (dateDebut > filtreDate || filtreDate > dateFin) {
          return null;
        }
      }
      // Filtrage sur le type du groupe
      if (pCriteres.getListeTypeGroupe() != null && !pCriteres.getListeTypeGroupe().contains(typeGroupeConditionVente)) {
        return null;
      }
      
      // Chargement des champs
      IdEtablissement idEtablissement = IdEtablissement.getInstance((String) pRecord.getField("PARETB"));
      IdGroupeConditionVente idConditionVente = IdGroupeConditionVente.getInstance(idEtablissement, (String) pRecord.getField("PARIND"));
      GroupeConditionVente groupeConditionVente = new GroupeConditionVente(idConditionVente);
      groupeConditionVente.setLibelle(((String) pRecord.getField("CNLIB")).trim());
      groupeConditionVente.setDateDebut(ConvertDate.db2ToDate(dateDebut, null));
      if (dateFin == valeurMax) {
        dateFin = 0;
      }
      groupeConditionVente.setDateFin(ConvertDate.db2ToDate(dateFin, null));
      groupeConditionVente.setTypeConditionVente(typeGroupeConditionVente);
      
      valeur = (String) pRecord.getField("CNMAP");
      groupeConditionVente.setModeApplication(EnumModeApplicationConditionVente.valueOfByCode(valeur.charAt(0)));
      return groupeConditionVente;
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    return null;
  }
  
  /**
   * Initialise un transporteur à partir de l'enregistrement donné et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return Transporteur
   */
  private Transporteur initRechercheTransporteurs(CriteresRechercheTransporteurs pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("TRETB")));
    IdTransporteur idTransporteur = IdTransporteur.getInstance(idEtablissement, ((String) pRecord.getField("TRCOD")));
    Transporteur transporteur = new Transporteur(idTransporteur);
    switch (pCriteres.getTypeRecherche()) {
      case CriteresRechercheTransporteurs.RECHERCHE_COMPTOIR:
        transporteur.setLibelle(((String) pRecord.getField("TRLIB")).trim());
        transporteur.setModeExpedition(((String) pRecord.getField("TRMEX")).trim());
        transporteur.setZoneGeographique(((String) pRecord.getField("TRZGEO")).trim());
        // TODO continuer la liste des autres champs
        break;
    }
    
    return transporteur;
  }
  
  /**
   * Initialise un lieu transport à partir de l'enregistrement donné et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return LieuTransport
   */
  private LieuTransport initRechercheLieuTransport(CritereLieuTransport pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("INDETB")));
    IdLieuTransport idLieuTransport = IdLieuTransport.getInstance(idEtablissement, ((String) pRecord.getField("INDIND")));
    LieuTransport lieuTransport = new LieuTransport(idLieuTransport);
    lieuTransport.setLibelle(((String) pRecord.getField("LVLB1")).trim());
    
    return lieuTransport;
  }
  
  /**
   * Initialise un tarif à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return Tarif
   */
  private Tarif initRechercheTarifs(CriteresRechercheTarifs pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("TAETB")));
    IdTarif idTarif = IdTarif.getInstance(idEtablissement, ((String) pRecord.getField("TACOD")).trim());
    Tarif tarif = new Tarif(idTarif);
    tarif.setDesignation(((String) pRecord.getField("TALIB")).trim());
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, ((String) pRecord.getField("TAART")));
    tarif.setIdArticleSupport(idArticle);
    String[] libellesPrix = tarif.getLibellesPrix();
    libellesPrix[0] = ((String) pRecord.getField("TALB1")).trim();
    libellesPrix[1] = ((String) pRecord.getField("TALB2")).trim();
    libellesPrix[2] = ((String) pRecord.getField("TALB3")).trim();
    libellesPrix[3] = ((String) pRecord.getField("TALB4")).trim();
    libellesPrix[4] = ((String) pRecord.getField("TALB5")).trim();
    libellesPrix[5] = ((String) pRecord.getField("TALB6")).trim();
    libellesPrix[6] = ((String) pRecord.getField("TALB7")).trim();
    libellesPrix[7] = ((String) pRecord.getField("TALB8")).trim();
    libellesPrix[8] = ((String) pRecord.getField("TALB9")).trim();
    libellesPrix[9] = ((String) pRecord.getField("TALB10")).trim();
    BigDecimal[] coefficientsPrix = tarif.getCoefficientsPrix();
    coefficientsPrix[0] = pRecord.getBigDecimalValue("TACO1", BigDecimal.ZERO);
    coefficientsPrix[1] = pRecord.getBigDecimalValue("TACO2", BigDecimal.ZERO);
    coefficientsPrix[2] = pRecord.getBigDecimalValue("TACO3", BigDecimal.ZERO);
    coefficientsPrix[3] = pRecord.getBigDecimalValue("TACO4", BigDecimal.ZERO);
    coefficientsPrix[4] = pRecord.getBigDecimalValue("TACO5", BigDecimal.ZERO);
    coefficientsPrix[5] = pRecord.getBigDecimalValue("TACO6", BigDecimal.ZERO);
    coefficientsPrix[6] = pRecord.getBigDecimalValue("TACO7", BigDecimal.ZERO);
    coefficientsPrix[7] = pRecord.getBigDecimalValue("TACO8", BigDecimal.ZERO);
    coefficientsPrix[8] = pRecord.getBigDecimalValue("TACO9", BigDecimal.ZERO);
    coefficientsPrix[9] = pRecord.getBigDecimalValue("TAC10", BigDecimal.ZERO);
    tarif.setDateApplicationTarifPreparation(ConvertDate.db2ToDate(((BigDecimal) pRecord.getField("TADAP")).intValue(), null));
    if (((String) pRecord.getField("TAIN1")).trim().length() == 0) {
      tarif.setMajAutomatiqueEuro_Devise(false);
    }
    else {
      tarif.setMajAutomatiqueEuro_Devise(true);
    }
    tarif.setCodeRattachementTarif(((String) pRecord.getField("TAIN2")).trim());
    tarif.setTitreRattachementTarif(((String) pRecord.getField("TATIR")).trim());
    if (((String) pRecord.getField("TAIN3")).trim().length() == 0) {
      tarif.setTarifEnPreparation(false);
    }
    else {
      tarif.setTarifEnPreparation(true);
    }
    
    return tarif;
  }
  
  /**
   * Initialise une catégorie fournisseur à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return CategorieFournisseur
   */
  private CategorieFournisseur initRechercheCategorieFournisseur(CriteresRechercheCategoriesFournisseur pCriteres, ExtendRecord pRecord) {
    
    switch (pCriteres.getTypeRecherche()) {
      case CriteresRechercheCategoriesFournisseur.RECHERCHE_CATEGORIE_FOURNISSEUR:
        IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("PARETB")));
        IdCategorieFournisseur idCategorieFournisseur =
            IdCategorieFournisseur.getInstance(idEtablissement, ((String) pRecord.getField("PARIND")));
        CategorieFournisseur categorie = new CategorieFournisseur(idCategorieFournisseur);
        categorie.setLibelle(((String) pRecord.getField("LIB")).trim());
        categorie.setMagasinTransfert(((String) pRecord.getField("CFMAT")).trim());
        categorie.setTraitementSpecifique(((String) pRecord.getField("CFGSP")).trim());
        return categorie;
    }
    return null;
  }
  
  /**
   * Initialise un mode d'expédition à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return ModeExpedition
   */
  private ModeExpedition initRechercheModeExpedition(CriteresModeExpedition pCriteres, ExtendRecord pRecord) {
    ModeExpedition modeExpe = new ModeExpedition(IdModeExpedition.getInstance(((String) pRecord.getField("EXCOD")).trim()));
    modeExpe.setLibelle(((String) pRecord.getField("EXLIB")).trim());
    modeExpe.setIsEnlevement(!((String) pRecord.getField("EXENL")).trim().equals(""));
    
    return modeExpe;
  }
  
  /**
   * Complète un IdZoneGeographique à partir d'un enregistrement.
   * @param pRecord record de base de données
   * @return IdZoneGeographique
   */
  private IdZoneGeographique completerIdZoneGeographique(ExtendRecord pRecord) {
    if (pRecord == null) {
      return null;
    }
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) pRecord.getField("ZGETB"));
    String codeZoneGeographique = (String) pRecord.getField("ZGCOD");
    return IdZoneGeographique.getInstance(idEtablissement, codeZoneGeographique);
  }
  
  /**
   * Initialise une zone geographique à partir de l'enregistrement donnée et du type de la recherche.
   * 
   * @param pRecord Enregistrement d'une zone géographique
   * @param pListeCommuneZoneGeographique Liste des communes des zones géographiques pour un établissement
   * @return ZoneGeographique
   */
  private ZoneGeographique initialiserZoneGeographique(ExtendRecord pRecord, ListeCommuneZoneGeographique pListeCommuneZoneGeographique) {
    // Création de l'identifiant
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("ZGETB")));
    IdZoneGeographique idZoneGeographique = IdZoneGeographique.getInstance(idEtablissement, ((String) pRecord.getField("ZGCOD")));
    
    // Création de l'objet métier
    ZoneGeographique zoneGeographique = new ZoneGeographique(idZoneGeographique);
    zoneGeographique.setNom(((String) pRecord.getField("ZGLIB")).trim());
    ListeCommuneZoneGeographique listeCommuneZoneGeographique = new ListeCommuneZoneGeographique();
    zoneGeographique.setListeCommuneZoneGeographique(listeCommuneZoneGeographique);
    
    // Association des communes concernées par la zone géographique
    if (pListeCommuneZoneGeographique != null && !pListeCommuneZoneGeographique.isEmpty()) {
      for (CommuneZoneGeographique communeZoneGeographique : pListeCommuneZoneGeographique) {
        if (communeZoneGeographique.getId() != null && communeZoneGeographique.getId().getIdZoneGeographique() != null) {
          // La commune et la zone corresponde si elles ont le même IdZoneGeographique
          // ou si la commune possède un IdZoneGeographique dont le code est *
          if (Constantes.equals(zoneGeographique.getId(), communeZoneGeographique.getId().getIdZoneGeographique())
              || Constantes.normerTexte(communeZoneGeographique.getId().getIdZoneGeographique().getCode()).equals("*")) {
            listeCommuneZoneGeographique.add(communeZoneGeographique);
          }
        }
      }
    }
    
    return zoneGeographique;
  }
  
  /**
   * Initialise une zone geographique à partir de son identifiant.
   * @param pIdZoneGeographique identifiant de zone géographique
   * @return ZoneGeographique
   */
  private ZoneGeographique initialiserZoneGeographique(IdZoneGeographique pIdZoneGeographique) {
    return null;
  }
  
  /**
   * Initialise une formule de prix à partir de l'enregistrement donnée et du type de la recherche.
   * @param pRecord enregistrement de base de données
   * @return FormulePrix
   */
  private FormulePrix initialiserFormulePrix(ExtendRecord pRecord) {
    // Création de l'identifiant
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("FPETB")));
    IdFormulePrix idFormulePrix = IdFormulePrix.getInstance(idEtablissement, ((String) pRecord.getField("FPCOD")));
    
    FormulePrix formulePrix = new FormulePrix(idFormulePrix);
    formulePrix.setLibelle(((String) pRecord.getField("FPLIB")).trim());
    // Récupération des lignes de formules de prix (il y en a 5 maximum)
    List<LigneFormulePrix> listeLigne = new ArrayList<LigneFormulePrix>();
    for (int i = 1; i <= 5; i++) {
      LigneFormulePrix ligne = new LigneFormulePrix();
      // Le libellé de la ligne
      ligne.setLibelle(Constantes.normerTexte((String) pRecord.getField("FPL" + i)));
      // La valeur ou le coefficient
      // La récupération est complexe car cette valeur est stockée sous forme de chaine de caractères et au format 'O' (SDA), c'est dire
      // 9999è pour un nombre négatif et 9999 pour un nombre positif
      String chaine = (String) pRecord.getField("FPC" + i);
      DataFormat dataFormat;
      if (DataFormat.isNegatifFormatO(chaine)) {
        dataFormat = new DataFormat(chaine, 'O', 7, 4);
      }
      else {
        dataFormat = new DataFormat(chaine, 'O', 9, 2);
      }
      chaine = dataFormat.getEdtNumerique(' ').replace(Constantes.SEPARATEUR_DECIMAL_CHAR, '.');
      if (chaine.isEmpty()) {
        chaine = "0";
      }
      BigDecimal valeur = new BigDecimal(chaine);
      ligne.setValeur(valeur.abs());
      // Le type de la valeur est déterminé par le signe de la valeur soit négative (coefficient) soit positive (valeur)
      if (valeur.compareTo(BigDecimal.ZERO) < 0) {
        ligne.setTypeValeur(EnumTypeValeurFormulePrix.COEFFICIENT);
      }
      else {
        ligne.setTypeValeur(EnumTypeValeurFormulePrix.VALEUR);
      }
      // L'opérande + ou -
      String operande = Constantes.normerTexte((String) pRecord.getField("FPS" + i));
      // Si l'opérande est vide alors la ligne est ignorée
      if (operande.isEmpty()) {
        continue;
      }
      ligne.setOperande(operande.charAt(0));
      
      // Ajout de la ligne traitée dans la liste
      listeLigne.add(ligne);
    }
    if (!listeLigne.isEmpty()) {
      formulePrix.setListeLigneFormulePrix(listeLigne);
    }
    
    return formulePrix;
  }
  
  /**
   * Initialise une ConditionCommissionnement à partir de l'enregistrement donnée et du type de la recherche.
   * @param pCriteres critères de recherche
   * @param pRecord record de base de données
   * @return ConditionCommissionnement
   */
  private ConditionCommissionnement initConditionCommissionnement(CriteresConditionCommissionnement pCriteres, ExtendRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("PARETB")));
    ConditionCommissionnement conditionCommissionnement =
        new ConditionCommissionnement(IdConditionCommissionnement.getInstance(idEtablissement, ((String) pRecord.getField("PARIND"))));
    conditionCommissionnement.setLibelle(((String) pRecord.getField("CRLIB")).trim());
    
    return conditionCommissionnement;
  }
  
  /**
   * Retourne si une table données existe.
   * @param pNomBibliotheque nom de la base de données
   * @param pTable table de base de données
   * @return boolean
   */
  private boolean isTableExiste(String pNomBibliotheque, EnumTableBDD pTable) {
    pNomBibliotheque = Constantes.normerTexte(pNomBibliotheque);
    if (pNomBibliotheque.isEmpty()) {
      return false;
    }
    String nomTable = Constantes.normerTexte(pTable.getNom());
    if (nomTable.isEmpty()) {
      return false;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select 1 from QSYS2.SYSTABLES where TABLE_SCHEMA = " + RequeteSql.formaterStringSQL(pNomBibliotheque)
        + " and TABLE_NAME = " + RequeteSql.formaterStringSQL(nomTable));
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return false;
    }
    return true;
  }
  
  /**
   * Initialiser une participation aux frais d'expédition.
   * @param pRecord enregistrement de base de données
   * @return ParticipationFraisExpedition
   */
  private ParticipationFraisExpedition initialiserParticipationFraisExpedition(ExtendRecord pRecord) {
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(((String) pRecord.getField("PARETB")));
    // Identifiant
    IdParticipationFraisExpedition idParticipationFraisExpedition =
        IdParticipationFraisExpedition.getInstance(idEtablissement, ((String) pRecord.getField("PARIND")));
    // Objet métier
    ParticipationFraisExpedition participationFraisExpedition = new ParticipationFraisExpedition(idParticipationFraisExpedition);
    // Seuils
    participationFraisExpedition.setSeuil1(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES01")));
    participationFraisExpedition.setSeuil2(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES02")));
    participationFraisExpedition.setSeuil3(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES03")));
    participationFraisExpedition.setSeuil4(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES04")));
    participationFraisExpedition.setSeuil5(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES05")));
    participationFraisExpedition.setSeuil6(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES06")));
    participationFraisExpedition.setSeuil7(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES07")));
    participationFraisExpedition.setSeuil8(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES08")));
    participationFraisExpedition.setSeuil9(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES09")));
    participationFraisExpedition.setSeuil10(Constantes.convertirTexteEnInteger((String) pRecord.getField("PES10")));
    // Pourcentages
    participationFraisExpedition.setPourcentage1(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP01"), true));
    participationFraisExpedition.setPourcentage2(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP02"), true));
    participationFraisExpedition.setPourcentage3(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP03"), true));
    participationFraisExpedition.setPourcentage4(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP04"), true));
    participationFraisExpedition.setPourcentage5(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP05"), true));
    participationFraisExpedition.setPourcentage6(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP06"), true));
    participationFraisExpedition.setPourcentage7(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP07"), true));
    participationFraisExpedition.setPourcentage8(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP08"), true));
    participationFraisExpedition.setPourcentage9(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP09"), true));
    participationFraisExpedition.setPourcentage10(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEP10"), true));
    // Montants
    participationFraisExpedition.setMontant1(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF01"), true));
    participationFraisExpedition.setMontant2(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF02"), true));
    participationFraisExpedition.setMontant3(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF03"), true));
    participationFraisExpedition.setMontant4(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF04"), true));
    participationFraisExpedition.setMontant5(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF05"), true));
    participationFraisExpedition.setMontant6(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF06"), true));
    participationFraisExpedition.setMontant7(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF07"), true));
    participationFraisExpedition.setMontant8(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF08"), true));
    participationFraisExpedition.setMontant9(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF09"), true));
    participationFraisExpedition.setMontant10(Constantes.convertirTexteEnBigDecimal((String) pRecord.getField("PEF10"), true));
    // Article
    String codeArticle = Constantes.normerTexte((String) pRecord.getField("PEART"));
    if (!codeArticle.trim().isEmpty()) {
      IdArticle idArticleAssocie = IdArticle.getInstance(idEtablissement, codeArticle);
      participationFraisExpedition.setIdArticleAssocie(idArticleAssocie);
    }
    
    return participationFraisExpedition;
  }
}
