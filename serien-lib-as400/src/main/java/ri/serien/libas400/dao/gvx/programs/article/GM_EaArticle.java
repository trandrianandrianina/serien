/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs.article;

import java.util.ArrayList;

import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

public class GM_EaArticle extends BaseGroupDB {
  /**
   * Constructeur
   * @param aquerymg
   */
  public GM_EaArticle(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Retourne un contact à partir de son id
   * @param id
   * @return
   * 
   */
  public M_EaArticle readOneArticle(String etb, String code) {
    if ((etb == null) || (etb.trim().length() == 0)) {
      msgErreur += "\nL'établissement est à null ou vide.";
      return null;
    }
    if ((code == null) || (code.trim().length() == 0)) {
      msgErreur += "\nLe code article est à null ou vide.";
      return null;
    }
    if (queryManager.getLibrary() == null) {
      msgErreur += "\nLa CURLIB n'est pas initialisée.";
      return null;
    }
    
    ArrayList<GenericRecord> listrcd =
        queryManager.select("select * from " + queryManager.getLibrary() + ".PGVMEAAM where A1ETB = '" + etb + "' and A1ART = '" + code + "'");
    if ((listrcd == null) || (listrcd.size() == 0)) {
      return null;
    }
    M_EaArticle a = new M_EaArticle(queryManager);
    a.initObject(listrcd.get(0), true);
    
    return a;
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
  
}
