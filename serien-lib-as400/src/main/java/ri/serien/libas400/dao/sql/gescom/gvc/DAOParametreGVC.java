/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.gvc;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.article.StatutArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.gvc.ListeParametresGVC;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

public class DAOParametreGVC {
  protected QueryManager queryManager = null;
  private static final String PR_CLE = "PR_CLE";
  private static final String PR_VAL = "PR_VAL";
  private static final String CLE_PROXYDY = "PROXYDY";
  private static final String CLE_NBARTPAGE = "NBARTPAGE";
  private static final String CLE_STATUTART = "STATUTART";
  private static final String CLE_MOINSCHER = "MOINSCHER";
  private static final String CLE_ETBDEFAUT = "ETBDEFAUT";
  private static final String CLE_HOSTSMTP = "HOSTSMTP";
  private static final String CLE_PORTSMTP = "PORTSMTP";
  private static final String CLE_PROFILSMTP = "PROFILSMTP";
  private static final String CLE_MPSMTP = "MPSMTP";

  /**
   * Constructeur.
   */
  public DAOParametreGVC(QueryManager aqueryManager) {
    queryManager = aqueryManager;
  }

  public ListeParametresGVC chargerListeParametresGVC() {
    ListeParametresGVC listeParametresGVC = new ListeParametresGVC();

    ArrayList<GenericRecord> listeGenericRecord = queryManager.select("SELECT * FROM " + queryManager.getLibrary() + ".PSEMPRGVCM");
    if (listeGenericRecord != null) {
      for (GenericRecord genericRecord : listeGenericRecord) {
        // On passe si on ne trouve pas la clé ou la valeur
        if (!genericRecord.isPresentField(PR_CLE) || !genericRecord.isPresentField(PR_VAL)) {
          continue;
        }

        String cle = genericRecord.getString(PR_CLE);
        String valeur = genericRecord.getString(PR_VAL);

        if (cle.equals(CLE_PROXYDY)) {
          listeParametresGVC.setProxyDynamiqueActif(valeur.equals("1"));
        }
        else if (cle.equals(CLE_NBARTPAGE)) {
          try {
            listeParametresGVC.setNombreArticlesParPage(Integer.parseInt(valeur));
          }
          catch (NumberFormatException e) {
            Trace.erreur("La valeur du paramètre GVC nombre d'articles par page n'est pas numérique : " + valeur);
            listeParametresGVC.setNombreArticlesParPage(0);
          }
        }
        else if (cle.equals(CLE_STATUTART)) {
          if (valeur.length() != 1) {
            Trace.erreur("La valeur du paramètre GVC nombre d'articles par page n'est pas numérique : " + valeur);
            valeur = "" + StatutArticle.TOUS;
          }

          StatutArticle statutArticle = new StatutArticle(valeur.charAt(0));
          listeParametresGVC.setFiltreStatutArticle(statutArticle);
        }
        else if (cle.equals(CLE_MOINSCHER)) {
          listeParametresGVC.setFiltreMoinsChersPriceJet(valeur.equals("1"));
        }
        else if (cle.equals(CLE_ETBDEFAUT)) {
          IdEtablissement idEtablissement = IdEtablissement.getInstance(valeur);
          listeParametresGVC.setFiltreEtablissement(idEtablissement);
        }
        else if (cle.equals(CLE_HOSTSMTP)) {
          listeParametresGVC.setHostSMTP(valeur);
        }
        else if (cle.equals(CLE_PORTSMTP)) {
          listeParametresGVC.setPortSMTP(valeur);
        }
        else if (cle.equals(CLE_PROFILSMTP)) {
          listeParametresGVC.setProfilSMTP(valeur);
        }
        else if (cle.equals(CLE_MPSMTP)) {
          listeParametresGVC.setMpSMTP(valeur);
        }
      }
    }

    return listeParametresGVC;
  }

  /**
   * On crée un paramètre dans la table PSEMPRGVCM
   */
  private boolean ecrireParametreGVC(String cle, String valeur, String type, String label, String libelle, String limite) {
    // Tester si le paramètre existe déjà
    ArrayList<GenericRecord> listeGenericRecord =
        queryManager.select("SELECT 1 FROM " + queryManager.getLibrary() + ".PSEMPRGVCM WHERE PR_CLE = '" + cle + "'");
    if (listeGenericRecord == null) {
      return false;
    }

    if (listeGenericRecord.isEmpty()) {
      int retour = queryManager.requete(""
          + "INSERT INTO " + queryManager.getLibrary() + ".PSEMPRGVCM "
          + "("
          + " PR_CLE,"
          + " PR_VAL,"
          + " PR_TYPE,"
          + " PR_LABEL,"
          + " PR_LIBEL,"
          + " PR_LIMIT"
          + ")"
          + " VALUES "
          + "( "
          + " '" + cle + "',"
          + " '" + valeur + "',"
          + " '" + type + "',"
          + " '" + label + "',"
          + " '" + libelle + "',"
          + " '" + limite + "'"
          + ")");

      if (retour <= 0) {
        throw new MessageErreurException("Impossible de sauvegarder un paramètre GVC dans la base de données.");
      }
    }
    else {
      int retour = queryManager.requete(""
          + "UPDATE " + queryManager.getLibrary() + ".PSEMPRGVCM SET "
          + PR_VAL + " = '" + valeur + "'"
          + " WHERE "
          + PR_CLE + " = '" + cle + "' ");

      if (retour <= 0) {
        throw new MessageErreurException("Impossible de sauvegarder un paramètre GVC dans la base de données.");
      }
    }

    return true;
  }

  /**
   * Récupère les élements de la table de paramètres du GVC pour les attribuer à la classe correspondante
   */
  public boolean sauverListeParametresGVC(ListeParametresGVC listeParametresGVC) {

    String valeur = "0";
    if (listeParametresGVC.isProxyDynamiqueActif()) {
      valeur = "1";
    }
    ecrireParametreGVC(CLE_PROXYDY, valeur, "BO", "Mode proxy dynamique", "On bascule le GVC en mode proxy dynamique", null);

    valeur = "" + listeParametresGVC.getNombreArticlesParPage();
    ecrireParametreGVC(CLE_NBARTPAGE, valeur, "IN", "Nombre d''articles par page",
        "Nombre d''articles affichés par page dans la recherche du GVC", null);

    StatutArticle statutArticle = listeParametresGVC.getFiltreStatutArticle();
    ecrireParametreGVC(CLE_STATUTART, "" + statutArticle.getCode(), "ST", "Statut de l''article par défaut",
        "On récupère le statut par défaut des articles triés dans la recherche", null);

    valeur = "0";
    if (listeParametresGVC.isFiltreMoinsChersPriceJet()) {
      valeur = "1";
    }
    ecrireParametreGVC(CLE_MOINSCHER, valeur, "BO", "Articles PriceJet moins chers",
        "On récupère par défaut les articles moins chers chez un concurrent défini dans PriceJet", null);

    valeur = listeParametresGVC.getFiltreEtablissement().getCodeEtablissement();
    ecrireParametreGVC(CLE_ETBDEFAUT, valeur, "ST", "Etablissement par défaut", "On récupère par l''etablissement par défaut du GVC", null);

    valeur = listeParametresGVC.getHostSMTP();
    ecrireParametreGVC(CLE_HOSTSMTP, valeur, "ST", "Host SMTP", "Host du serveur SMTP d''envoi des mails", null);
    
    valeur = listeParametresGVC.getPortSMTP();
    ecrireParametreGVC(CLE_PORTSMTP, valeur, "ST", "Port SMTP", "Port du serveur SMTP d''envoi des mails", null);
    
    valeur = listeParametresGVC.getProfilSMTP();
    ecrireParametreGVC(CLE_PROFILSMTP, valeur, "ST", "Profil SMTP", "Profil d''envoi des mails", null);

    valeur = listeParametresGVC.getMpSMTP();
    ecrireParametreGVC(CLE_MPSMTP, valeur, "ST", "Mot de passe SMTP", "Mot de passe profil envoi des mails", null);

    return true;
  }
}
