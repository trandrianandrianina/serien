/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.prixflash;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0037i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PICLF = 6;
  public static final int DECIMAL_PICLF = 0;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 52;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PICLF = 2;
  public static final int VAR_PIART = 3;
  public static final int VAR_PICOD = 4;
  public static final int VAR_PINUM = 5;
  public static final int VAR_PISUF = 6;
  public static final int VAR_PINLI = 7;
  public static final int VAR_PIARR = 8;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement *
  private BigDecimal piclf = BigDecimal.ZERO; // Numéro client facturé *
  private String piart = ""; // Code article *
  private String picod = ""; // Code ELR *
  private BigDecimal pinum = BigDecimal.ZERO; // N° document *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suf. document *
  private BigDecimal pinli = BigDecimal.ZERO; // N° ligne *
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PICLF, DECIMAL_PICLF), // Numéro client facturé *
      new AS400Text(SIZE_PIART), // Code article *
      new AS400Text(SIZE_PICOD), // Code ELR *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // N° document *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suf. document *
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // N° ligne *
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pietb, piclf, piart, picod, pinum, pisuf, pinli, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    piclf = (BigDecimal) output[2];
    piart = (String) output[3];
    picod = (String) output[4];
    pinum = (BigDecimal) output[5];
    pisuf = (BigDecimal) output[6];
    pinli = (BigDecimal) output[7];
    piarr = (String) output[8];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPiclf(BigDecimal pPiclf) {
    if (pPiclf == null) {
      return;
    }
    piclf = pPiclf.setScale(DECIMAL_PICLF, RoundingMode.HALF_UP);
  }
  
  public void setPiclf(Integer pPiclf) {
    if (pPiclf == null) {
      return;
    }
    piclf = BigDecimal.valueOf(pPiclf);
  }
  
  public Integer getPiclf() {
    return piclf.intValue();
  }
  
  public void setPiart(String pPiart) {
    if (pPiart == null) {
      return;
    }
    piart = pPiart;
  }
  
  public String getPiart() {
    return piart;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
