/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0018o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_PONHO = 3;
  public static final int DECIMAL_PONHO = 0;
  public static final int SIZE_POCHM = 200;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 214;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_PONHO = 1;
  public static final int VAR_POCHM = 2;
  public static final int VAR_POARR = 3;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal ponho = BigDecimal.ZERO; // Nb de lignes non honorables
  private String pochm = ""; // CHEMIN DU FICHIER PDF
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_PONHO, DECIMAL_PONHO), // Nb de lignes non honorables
      new AS400Text(SIZE_POCHM), // CHEMIN DU FICHIER PDF
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { poind, ponho, pochm, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    ponho = (BigDecimal) output[1];
    pochm = (String) output[2];
    poarr = (String) output[3];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPonho(BigDecimal pPonho) {
    if (pPonho == null) {
      return;
    }
    ponho = pPonho.setScale(DECIMAL_PONHO, RoundingMode.HALF_UP);
  }
  
  public void setPonho(Integer pPonho) {
    if (pPonho == null) {
      return;
    }
    ponho = BigDecimal.valueOf(pPonho);
  }
  
  public Integer getPonho() {
    return ponho.intValue();
  }
  
  public void setPochm(String pPochm) {
    if (pPochm == null) {
      return;
    }
    pochm = pPochm;
  }
  
  public String getPochm() {
    return pochm;
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
