/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0014d {
  // Constantes
  public static final int SIZE_L1TOP = 1;
  public static final int DECIMAL_L1TOP = 0;
  public static final int SIZE_L1COD = 1;
  public static final int SIZE_L1ETB = 3;
  public static final int SIZE_L1NUM = 6;
  public static final int DECIMAL_L1NUM = 0;
  public static final int SIZE_L1SUF = 1;
  public static final int DECIMAL_L1SUF = 0;
  public static final int SIZE_L1NLI = 4;
  public static final int DECIMAL_L1NLI = 0;
  public static final int SIZE_L1ERL = 1;
  public static final int SIZE_L1CEX = 1;
  public static final int SIZE_L1QEX = 11;
  public static final int DECIMAL_L1QEX = 3;
  public static final int SIZE_L1TVA = 1;
  public static final int DECIMAL_L1TVA = 0;
  public static final int SIZE_L1TB = 1;
  public static final int DECIMAL_L1TB = 0;
  public static final int SIZE_L1TR = 1;
  public static final int DECIMAL_L1TR = 0;
  public static final int SIZE_L1TN = 1;
  public static final int DECIMAL_L1TN = 0;
  public static final int SIZE_L1TC = 1;
  public static final int DECIMAL_L1TC = 0;
  public static final int SIZE_L1TT = 1;
  public static final int DECIMAL_L1TT = 0;
  public static final int SIZE_L1VAL = 1;
  public static final int DECIMAL_L1VAL = 0;
  public static final int SIZE_L1TAR = 2;
  public static final int DECIMAL_L1TAR = 0;
  public static final int SIZE_L1COL = 1;
  public static final int DECIMAL_L1COL = 0;
  public static final int SIZE_L1TPF = 1;
  public static final int DECIMAL_L1TPF = 0;
  public static final int SIZE_L1DCV = 1;
  public static final int DECIMAL_L1DCV = 0;
  public static final int SIZE_L1TNC = 1;
  public static final int DECIMAL_L1TNC = 0;
  public static final int SIZE_L1SGN = 1;
  public static final int DECIMAL_L1SGN = 0;
  public static final int SIZE_L1SER = 2;
  public static final int DECIMAL_L1SER = 0;
  public static final int SIZE_L1QTE = 11;
  public static final int DECIMAL_L1QTE = 3;
  public static final int SIZE_L1KSV = 7;
  public static final int DECIMAL_L1KSV = 3;
  public static final int SIZE_L1PVB = 9;
  public static final int DECIMAL_L1PVB = 2;
  public static final int SIZE_L1REM1 = 4;
  public static final int DECIMAL_L1REM1 = 2;
  public static final int SIZE_L1REM2 = 4;
  public static final int DECIMAL_L1REM2 = 2;
  public static final int SIZE_L1REM3 = 4;
  public static final int DECIMAL_L1REM3 = 2;
  public static final int SIZE_L1REM4 = 4;
  public static final int DECIMAL_L1REM4 = 2;
  public static final int SIZE_L1REM5 = 4;
  public static final int DECIMAL_L1REM5 = 2;
  public static final int SIZE_L1REM6 = 4;
  public static final int DECIMAL_L1REM6 = 2;
  public static final int SIZE_L1TRL = 1;
  public static final int SIZE_L1BRL = 1;
  public static final int SIZE_L1RP1 = 1;
  public static final int SIZE_L1RP2 = 1;
  public static final int SIZE_L1RP3 = 1;
  public static final int SIZE_L1RP4 = 1;
  public static final int SIZE_L1RP5 = 1;
  public static final int SIZE_L1RP6 = 1;
  public static final int SIZE_L1PVN = 9;
  public static final int DECIMAL_L1PVN = 2;
  public static final int SIZE_L1PVC = 9;
  public static final int DECIMAL_L1PVC = 2;
  public static final int SIZE_L1MHT = 9;
  public static final int DECIMAL_L1MHT = 2;
  public static final int SIZE_L1PRP = 9;
  public static final int DECIMAL_L1PRP = 2;
  public static final int SIZE_L1COE = 5;
  public static final int DECIMAL_L1COE = 4;
  public static final int SIZE_L1PRV = 9;
  public static final int DECIMAL_L1PRV = 2;
  public static final int SIZE_L1AVR = 1;
  public static final int SIZE_L1UNV = 2;
  public static final int SIZE_L1ART = 20;
  public static final int SIZE_L1CPL = 8;
  public static final int SIZE_L1CND = 9;
  public static final int DECIMAL_L1CND = 3;
  public static final int SIZE_L1IN1 = 1;
  public static final int SIZE_L1PRT = 7;
  public static final int DECIMAL_L1PRT = 2;
  public static final int SIZE_L1DLP = 7;
  public static final int DECIMAL_L1DLP = 0;
  public static final int SIZE_L1IN2 = 1;
  public static final int SIZE_L1IN3 = 1;
  public static final int SIZE_L1TP1 = 2;
  public static final int SIZE_L1TP2 = 2;
  public static final int SIZE_L1TP3 = 2;
  public static final int SIZE_L1TP4 = 2;
  public static final int SIZE_L1TP5 = 2;
  public static final int SIZE_L1GBA = 1;
  public static final int DECIMAL_L1GBA = 0;
  public static final int SIZE_L1ARTS = 20;
  public static final int SIZE_L1IN4 = 1;
  public static final int SIZE_L1IN5 = 1;
  public static final int SIZE_L1IN6 = 1;
  public static final int SIZE_L1IN7 = 1;
  public static final int SIZE_L1QTL = 11;
  public static final int DECIMAL_L1QTL = 3;
  public static final int SIZE_L1MAG = 2;
  public static final int SIZE_L1REP = 2;
  public static final int SIZE_L1IN8 = 1;
  public static final int SIZE_L1IN9 = 1;
  public static final int SIZE_L1IN10 = 1;
  public static final int SIZE_L1IN11 = 1;
  public static final int SIZE_L1IN12 = 1;
  public static final int SIZE_L1NLI0 = 4;
  public static final int DECIMAL_L1NLI0 = 0;
  public static final int SIZE_L1PRA = 9;
  public static final int DECIMAL_L1PRA = 2;
  public static final int SIZE_L1PVA = 9;
  public static final int DECIMAL_L1PVA = 2;
  public static final int SIZE_L1QTP = 11;
  public static final int DECIMAL_L1QTP = 3;
  public static final int SIZE_L1MTR = 7;
  public static final int DECIMAL_L1MTR = 2;
  public static final int SIZE_L1SER3 = 1;
  public static final int SIZE_L1SER5 = 1;
  public static final int SIZE_L1IN17 = 1;
  public static final int SIZE_L1IN18 = 1;
  public static final int SIZE_L1IN19 = 1;
  public static final int SIZE_L1IN20 = 1;
  public static final int SIZE_L1IN21 = 1;
  public static final int SIZE_L1IN22 = 1;
  
  /* DLC */
  public static final int SIZE_L1QTEI = 11;
  public static final int DECIMAL_L1QTEI = 3;
  public static final int SIZE_L1QTLI = 11;
  public static final int DECIMAL_L1QTLI = 3;
  public static final int SIZE_L1QTPI = 11;
  public static final int DECIMAL_L1QTPI = 3;
  public static final int SIZE_L1MHTI = 9;
  public static final int DECIMAL_L1MHTI = 2;
  /* DLC */
  
  public static final int SIZE_A1UNL = 2;
  public static final int SIZE_WLIGLB = 124;
  public static final int SIZE_WFPVN = 9;
  public static final int DECIMAL_WFPVN = 2;
  public static final int SIZE_WFUSR = 10;
  public static final int SIZE_WFDAT = 8;
  public static final int SIZE_WFHEU = 4;
  public static final int DECIMAL_WFHEU = 0;
  public static final int SIZE_WPBTTC = 9;
  public static final int DECIMAL_WPBTTC = 2;
  public static final int SIZE_WPNTTC = 9;
  public static final int DECIMAL_WPNTTC = 2;
  public static final int SIZE_WPVTTC = 9;
  public static final int DECIMAL_WPVTTC = 2;
  public static final int SIZE_WMTTTC = 9;
  public static final int DECIMAL_WMTTTC = 2;
  public static final int SIZE_WPT1HT = 9;
  public static final int DECIMAL_WPT1HT = 2;
  public static final int SIZE_WPT1TTC = 9;
  public static final int DECIMAL_WPT1TTC = 2;
  public static final int SIZE_WQTEOR = 11;
  public static final int DECIMAL_WQTEOR = 3;
  public static final int SIZE_WQTEEX = 11;
  public static final int DECIMAL_WQTEEX = 3;
  public static final int SIZE_L2QT1 = 8;
  public static final int DECIMAL_L2QT1 = 3;
  public static final int SIZE_L2QT2 = 8;
  public static final int DECIMAL_L2QT2 = 3;
  public static final int SIZE_L2QT3 = 8;
  public static final int DECIMAL_L2QT3 = 3;
  public static final int SIZE_L2NBR = 6;
  public static final int DECIMAL_L2NBR = 0;
  public static final int SIZE_L12QT = 9;
  public static final int DECIMAL_L12QT = 3;
  public static final int SIZE_L12UC = 2;
  public static final int SIZE_WORIPR = 10;
  public static final int SIZE_WTAUTVA = 5;
  public static final int DECIMAL_WTAUTVA = 3;
  public static final int SIZE_WTYPART = 3;
  public static final int SIZE_L1PRS = 9;
  public static final int DECIMAL_L1PRS = 2;
  public static final int SIZE_WQTEFAC = 11;
  public static final int DECIMAL_WQTEFAC = 3;
  public static final int SIZE_WTOP = 1;
  public static final int DECIMAL_WTOP = 0;
  public static final int SIZE_WCOF = 1;
  public static final int DECIMAL_WCOF = 0;
  public static final int SIZE_WFRS = 6;
  public static final int DECIMAL_WFRS = 0;
  public static final int SIZE_WFRE = 4;
  public static final int DECIMAL_WFRE = 0;
  public static final int SIZE_TOTALE_DS = 638;
  public static final int SIZE_FILLER = 700 - 638;
  
  // Constantes indices Nom DS
  public static final int VAR_L1TOP = 0;
  public static final int VAR_L1COD = 1;
  public static final int VAR_L1ETB = 2;
  public static final int VAR_L1NUM = 3;
  public static final int VAR_L1SUF = 4;
  public static final int VAR_L1NLI = 5;
  public static final int VAR_L1ERL = 6;
  public static final int VAR_L1CEX = 7;
  public static final int VAR_L1QEX = 8;
  public static final int VAR_L1TVA = 9;
  public static final int VAR_L1TB = 10;
  public static final int VAR_L1TR = 11;
  public static final int VAR_L1TN = 12;
  public static final int VAR_L1TC = 13;
  public static final int VAR_L1TT = 14;
  public static final int VAR_L1VAL = 15;
  public static final int VAR_L1TAR = 16;
  public static final int VAR_L1COL = 17;
  public static final int VAR_L1TPF = 18;
  public static final int VAR_L1DCV = 19;
  public static final int VAR_L1TNC = 20;
  public static final int VAR_L1SGN = 21;
  public static final int VAR_L1SER = 22;
  public static final int VAR_L1QTE = 23;
  public static final int VAR_L1KSV = 24;
  public static final int VAR_L1PVB = 25;
  public static final int VAR_L1REM1 = 26;
  public static final int VAR_L1REM2 = 27;
  public static final int VAR_L1REM3 = 28;
  public static final int VAR_L1REM4 = 29;
  public static final int VAR_L1REM5 = 30;
  public static final int VAR_L1REM6 = 31;
  public static final int VAR_L1TRL = 32;
  public static final int VAR_L1BRL = 33;
  public static final int VAR_L1RP1 = 34;
  public static final int VAR_L1RP2 = 35;
  public static final int VAR_L1RP3 = 36;
  public static final int VAR_L1RP4 = 37;
  public static final int VAR_L1RP5 = 38;
  public static final int VAR_L1RP6 = 39;
  public static final int VAR_L1PVN = 40;
  public static final int VAR_L1PVC = 41;
  public static final int VAR_L1MHT = 42;
  public static final int VAR_L1PRP = 43;
  public static final int VAR_L1COE = 44;
  public static final int VAR_L1PRV = 45;
  public static final int VAR_L1AVR = 46;
  public static final int VAR_L1UNV = 47;
  public static final int VAR_L1ART = 48;
  public static final int VAR_L1CPL = 49;
  public static final int VAR_L1CND = 50;
  public static final int VAR_L1IN1 = 51;
  public static final int VAR_L1PRT = 52;
  public static final int VAR_L1DLP = 53;
  public static final int VAR_L1IN2 = 54;
  public static final int VAR_L1IN3 = 55;
  public static final int VAR_L1TP1 = 56;
  public static final int VAR_L1TP2 = 57;
  public static final int VAR_L1TP3 = 58;
  public static final int VAR_L1TP4 = 59;
  public static final int VAR_L1TP5 = 60;
  public static final int VAR_L1GBA = 61;
  public static final int VAR_L1ARTS = 62;
  public static final int VAR_L1IN4 = 63;
  public static final int VAR_L1IN5 = 64;
  public static final int VAR_L1IN6 = 65;
  public static final int VAR_L1IN7 = 66;
  public static final int VAR_L1QTL = 67;
  public static final int VAR_L1MAG = 68;
  public static final int VAR_L1REP = 69;
  public static final int VAR_L1IN8 = 70;
  public static final int VAR_L1IN9 = 71;
  public static final int VAR_L1IN10 = 72;
  public static final int VAR_L1IN11 = 73;
  public static final int VAR_L1IN12 = 74;
  public static final int VAR_L1NLI0 = 75;
  public static final int VAR_L1PRA = 76;
  public static final int VAR_L1PVA = 77;
  public static final int VAR_L1QTP = 78;
  public static final int VAR_L1MTR = 79;
  public static final int VAR_L1SER3 = 80;
  public static final int VAR_L1SER5 = 81;
  public static final int VAR_L1IN17 = 82;
  public static final int VAR_L1IN18 = 83;
  public static final int VAR_L1IN19 = 84;
  public static final int VAR_L1IN20 = 85;
  public static final int VAR_L1IN21 = 86;
  public static final int VAR_L1IN22 = 87;
  
  /* DLC */
  public static final int VAR_L1QTEI = 88;
  public static final int VAR_L1QTLI = 89;
  public static final int VAR_L1QTPI = 90;
  public static final int VAR_L1MHTI = 91;
  /* DLC */
  
  public static final int VAR_A1UNL = 92;
  public static final int VAR_WLIGLB = 93;
  public static final int VAR_WFPVN = 94;
  public static final int VAR_WFUSR = 95;
  public static final int VAR_WFDAT = 96;
  public static final int VAR_WFHEU = 97;
  public static final int VAR_WPBTTC = 98;
  public static final int VAR_WPNTTC = 99;
  public static final int VAR_WPVTTC = 100;
  public static final int VAR_WMTTTC = 101;
  public static final int VAR_WPT1HT = 102;
  public static final int VAR_WPT1TTC = 103;
  public static final int VAR_WQTEOR = 104;
  public static final int VAR_WQTEEX = 105;
  public static final int VAR_L2QT1 = 106;
  public static final int VAR_L2QT2 = 107;
  public static final int VAR_L2QT3 = 108;
  public static final int VAR_L2NBR = 109;
  public static final int VAR_L12QT = 110;
  public static final int VAR_L12UC = 111;
  public static final int VAR_WORIPR = 112;
  public static final int VAR_WTAUTVA = 113;
  public static final int VAR_WTYPART = 114;
  public static final int VAR_L1PRS = 115;
  public static final int VAR_WQTEFAC = 116;
  public static final int VAR_WTOP = 117;
  public static final int VAR_WCOF = 118;
  public static final int VAR_WFRS = 119;
  public static final int VAR_WFRE = 120;
  
  // Variables AS400
  private BigDecimal l1top = BigDecimal.ZERO; // Code Etat de la Ligne
  private String l1cod = ""; // Code ERL "E"
  private String l1etb = ""; // Code Etablissement
  private BigDecimal l1num = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal l1suf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal l1nli = BigDecimal.ZERO; // Numéro de Ligne
  private String l1erl = ""; // Code ERL "C"
  private String l1cex = ""; // Code Extraction
  private BigDecimal l1qex = BigDecimal.ZERO; // Quantité extraite en UV
  private BigDecimal l1tva = BigDecimal.ZERO; // Code TVA
  private BigDecimal l1tb = BigDecimal.ZERO; // Top prix de base saisi
  private BigDecimal l1tr = BigDecimal.ZERO; // Top remises saisies
  private BigDecimal l1tn = BigDecimal.ZERO; // Top prix net saisi
  private BigDecimal l1tc = BigDecimal.ZERO; // Top coefficient saisi
  private BigDecimal l1tt = BigDecimal.ZERO; // Top colonne tarif saisie
  private BigDecimal l1val = BigDecimal.ZERO; // Top Ligne en Valeur
  private BigDecimal l1tar = BigDecimal.ZERO; // Numéro colonne tarif
  private BigDecimal l1col = BigDecimal.ZERO; // Colonne TVA/E1TVAG
  private BigDecimal l1tpf = BigDecimal.ZERO; // Code TPF
  private BigDecimal l1dcv = BigDecimal.ZERO; // Top décimalisation
  private BigDecimal l1tnc = BigDecimal.ZERO; // Top non commissionné
  private BigDecimal l1sgn = BigDecimal.ZERO; // Signe de la ligne
  private BigDecimal l1ser = BigDecimal.ZERO; // Top n° de serie ou lot
  private BigDecimal l1qte = BigDecimal.ZERO; // Quantité Commandée
  private BigDecimal l1ksv = BigDecimal.ZERO; // Nbre d'US pour 1 UV
  private BigDecimal l1pvb = BigDecimal.ZERO; // Prix de Vente de Base
  private BigDecimal l1rem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal l1rem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal l1rem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal l1rem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal l1rem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal l1rem6 = BigDecimal.ZERO; // % Remise 6
  private String l1trl = ""; // Type remise ligne, 1=cascade
  private String l1brl = ""; // Base remise ligne, 1=Montant
  private String l1rp1 = ""; // Exclusion remise de pied N°1
  private String l1rp2 = ""; // Exclusion remise de pied N°2
  private String l1rp3 = ""; // Exclusion remise de pied N°3
  private String l1rp4 = ""; // Exclusion remise de pied N°4
  private String l1rp5 = ""; // Exclusion remise de pied N°5
  private String l1rp6 = ""; // Exclusion remise de pied N°6
  private BigDecimal l1pvn = BigDecimal.ZERO; // Prix de Vente Net
  private BigDecimal l1pvc = BigDecimal.ZERO; // Prix de Vente Calculé
  private BigDecimal l1mht = BigDecimal.ZERO; // Montant Hors Taxes
  private BigDecimal l1prp = BigDecimal.ZERO; // Prix de Promo. (Utilisé)
  private BigDecimal l1coe = BigDecimal.ZERO; // Coeff. Mult. prix de base
  private BigDecimal l1prv = BigDecimal.ZERO; // Prix de Revient
  private String l1avr = ""; // Code Ligne Avoir
  private String l1unv = ""; // Code Unité de Vente
  private String l1art = ""; // Code Article
  private String l1cpl = ""; // Complément de Libellé
  private BigDecimal l1cnd = BigDecimal.ZERO; // Conditionnement
  private String l1in1 = ""; // Ind. non soumis à escompte
  private BigDecimal l1prt = BigDecimal.ZERO; // Prix de transport
  private BigDecimal l1dlp = BigDecimal.ZERO; // Date de livraison prévue
  private String l1in2 = ""; // Ind. Type de gratuits
  private String l1in3 = ""; // Ind. Kit,Taxe etc...
  private String l1tp1 = ""; // Top personnal.N°1
  private String l1tp2 = ""; // Top personnal.N°2
  private String l1tp3 = ""; // Top personnal.N°3
  private String l1tp4 = ""; // Top personnal.N°4
  private String l1tp5 = ""; // Top personnal.N°5
  private BigDecimal l1gba = BigDecimal.ZERO; // Génération Bon Achat
  private String l1arts = ""; // Code article substitué
  private String l1in4 = ""; // Ind. Type substitution
  private String l1in5 = ""; // Ind. remise 50/50 sur com/rp
  private String l1in6 = ""; // Ind. PRV saisi sur ligne
  private String l1in7 = ""; // Ind. recherche prix / kit
  private BigDecimal l1qtl = BigDecimal.ZERO; // Quantité Livrable
  private String l1mag = ""; // Magasin
  private String l1rep = ""; // Représentant
  private String l1in8 = ""; // Indice de livraison
  private String l1in9 = ""; // Ind cumul(pds,vol,col)/l1qte
  private String l1in10 = ""; // Ind cumul(pds,vol,col)/l1qtp
  private String l1in11 = ""; // Ligne à extraire
  private String l1in12 = ""; // Ind. ASDI =A1IN13
  private BigDecimal l1nli0 = BigDecimal.ZERO; // Numéro de Ligne origine
  private BigDecimal l1pra = BigDecimal.ZERO; // Prix à ajouter au PRV
  private BigDecimal l1pva = BigDecimal.ZERO; // Prix à ajouter au PVC
  private BigDecimal l1qtp = BigDecimal.ZERO; // Quantité en Pièces
  private BigDecimal l1mtr = BigDecimal.ZERO; // Montant transport
  private String l1ser3 = ""; // top article loti N.U
  private String l1ser5 = ""; // top article ads N.U
  private String l1in17 = ""; // Type de vente
  private String l1in18 = ""; // Prix garanti, affaire, dérog
  private String l1in19 = ""; // Applic. condition quantitat.
  private String l1in20 = ""; // Cond. Chantier Blanc,A F S G
  private String l1in21 = ""; // Ind. regroupement ligne
  private String l1in22 = ""; // N.U
  
  /* DLC */
  private BigDecimal l1qtei = BigDecimal.ZERO; // Quantité Commandée initiale
  private BigDecimal l1qtli = BigDecimal.ZERO; // Quantité Livrable initiale
  private BigDecimal l1qtpi = BigDecimal.ZERO; // Quantité en Pièces initiale
  private BigDecimal l1mhti = BigDecimal.ZERO; // Montant Hors Taxes initiale
  /* DLC */
  
  private String a1unl = ""; // Code Unité de conditionnemet
  private String wliglb = ""; // Libellé
  private BigDecimal wfpvn = BigDecimal.ZERO; // Prix de vente flash
  private String wfusr = ""; // Code utilisateur
  private String wfdat = ""; // Date prix flash
  private BigDecimal wfheu = BigDecimal.ZERO; // Heure
  private BigDecimal wpbttc = BigDecimal.ZERO; // Prix de base TTC
  private BigDecimal wpnttc = BigDecimal.ZERO; // Prix net TTC
  private BigDecimal wpvttc = BigDecimal.ZERO; // Prix de vente calculé TTC
  private BigDecimal wmtttc = BigDecimal.ZERO; // Montant TTC
  private BigDecimal wpt1ht = BigDecimal.ZERO; // Prix Tarif 1 H.T
  private BigDecimal wpt1ttc = BigDecimal.ZERO; // Prix Tarif 1 T.T.C
  private BigDecimal wqteor = BigDecimal.ZERO; // Quantité document origine
  private BigDecimal wqteex = BigDecimal.ZERO; // Quantité déjà extraites
  private BigDecimal l2qt1 = BigDecimal.ZERO; // Quantité 1
  private BigDecimal l2qt2 = BigDecimal.ZERO; // Quantité 2
  private BigDecimal l2qt3 = BigDecimal.ZERO; // Quantité 3
  private BigDecimal l2nbr = BigDecimal.ZERO; // Nombre
  private BigDecimal l12qt = BigDecimal.ZERO; // Quantité initiale
  private String l12uc = ""; // Code unité initiale
  private String woripr = ""; // Origine prix
  private BigDecimal wtautva = BigDecimal.ZERO; // Taux de TVA
  private String wtypart = ""; // Type ligne
  private BigDecimal l1prs = BigDecimal.ZERO; // Prix de revient standard
  private BigDecimal wqtefac = BigDecimal.ZERO; // Quantité déjà facturée
  private BigDecimal wtop = BigDecimal.ZERO; // Ligne totalement traitée
  private BigDecimal wcof = BigDecimal.ZERO; // Collectif fournisseur
  private BigDecimal wfrs = BigDecimal.ZERO; // Numéro fournisseur
  private BigDecimal wfre = BigDecimal.ZERO; // Lien FRS / FRE
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400ZonedDecimal(SIZE_L1TOP, DECIMAL_L1TOP), // Code Etat de la Ligne
      new AS400Text(SIZE_L1COD), // Code ERL "E"
      new AS400Text(SIZE_L1ETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_L1NUM, DECIMAL_L1NUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_L1SUF, DECIMAL_L1SUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_L1NLI, DECIMAL_L1NLI), // Numéro de Ligne
      new AS400Text(SIZE_L1ERL), // Code ERL "C"
      new AS400Text(SIZE_L1CEX), // Code Extraction
      new AS400PackedDecimal(SIZE_L1QEX, DECIMAL_L1QEX), // Quantité extraite en UV
      new AS400ZonedDecimal(SIZE_L1TVA, DECIMAL_L1TVA), // Code TVA
      new AS400ZonedDecimal(SIZE_L1TB, DECIMAL_L1TB), // Top prix de base saisi
      new AS400ZonedDecimal(SIZE_L1TR, DECIMAL_L1TR), // Top remises saisies
      new AS400ZonedDecimal(SIZE_L1TN, DECIMAL_L1TN), // Top prix net saisi
      new AS400ZonedDecimal(SIZE_L1TC, DECIMAL_L1TC), // Top coefficient saisi
      new AS400ZonedDecimal(SIZE_L1TT, DECIMAL_L1TT), // Top colonne tarif saisie
      new AS400ZonedDecimal(SIZE_L1VAL, DECIMAL_L1VAL), // Top Ligne en Valeur
      new AS400ZonedDecimal(SIZE_L1TAR, DECIMAL_L1TAR), // Numéro colonne tarif
      new AS400ZonedDecimal(SIZE_L1COL, DECIMAL_L1COL), // Colonne TVA/E1TVAG
      new AS400ZonedDecimal(SIZE_L1TPF, DECIMAL_L1TPF), // Code TPF
      new AS400ZonedDecimal(SIZE_L1DCV, DECIMAL_L1DCV), // Top décimalisation
      new AS400ZonedDecimal(SIZE_L1TNC, DECIMAL_L1TNC), // Top non commissionné
      new AS400ZonedDecimal(SIZE_L1SGN, DECIMAL_L1SGN), // Signe de la ligne
      new AS400ZonedDecimal(SIZE_L1SER, DECIMAL_L1SER), // Top n° de serie ou lot
      new AS400PackedDecimal(SIZE_L1QTE, DECIMAL_L1QTE), // Quantité Commandée
      new AS400PackedDecimal(SIZE_L1KSV, DECIMAL_L1KSV), // Nbre d'US pour 1 UV
      new AS400PackedDecimal(SIZE_L1PVB, DECIMAL_L1PVB), // Prix de Vente de Base
      new AS400ZonedDecimal(SIZE_L1REM1, DECIMAL_L1REM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_L1REM2, DECIMAL_L1REM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_L1REM3, DECIMAL_L1REM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_L1REM4, DECIMAL_L1REM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_L1REM5, DECIMAL_L1REM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_L1REM6, DECIMAL_L1REM6), // % Remise 6
      new AS400Text(SIZE_L1TRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_L1BRL), // Base remise ligne, 1=Montant
      new AS400Text(SIZE_L1RP1), // Exclusion remise de pied N°1
      new AS400Text(SIZE_L1RP2), // Exclusion remise de pied N°2
      new AS400Text(SIZE_L1RP3), // Exclusion remise de pied N°3
      new AS400Text(SIZE_L1RP4), // Exclusion remise de pied N°4
      new AS400Text(SIZE_L1RP5), // Exclusion remise de pied N°5
      new AS400Text(SIZE_L1RP6), // Exclusion remise de pied N°6
      new AS400PackedDecimal(SIZE_L1PVN, DECIMAL_L1PVN), // Prix de Vente Net
      new AS400PackedDecimal(SIZE_L1PVC, DECIMAL_L1PVC), // Prix de Vente Calculé
      new AS400PackedDecimal(SIZE_L1MHT, DECIMAL_L1MHT), // Montant Hors Taxes
      new AS400PackedDecimal(SIZE_L1PRP, DECIMAL_L1PRP), // Prix de Promo. (Utilisé)
      new AS400PackedDecimal(SIZE_L1COE, DECIMAL_L1COE), // Coeff. Mult. prix de base
      new AS400PackedDecimal(SIZE_L1PRV, DECIMAL_L1PRV), // Prix de Revient
      new AS400Text(SIZE_L1AVR), // Code Ligne Avoir
      new AS400Text(SIZE_L1UNV), // Code Unité de Vente
      new AS400Text(SIZE_L1ART), // Code Article
      new AS400Text(SIZE_L1CPL), // Complément de Libellé
      new AS400PackedDecimal(SIZE_L1CND, DECIMAL_L1CND), // Conditionnement
      new AS400Text(SIZE_L1IN1), // Ind. non soumis à escompte
      new AS400PackedDecimal(SIZE_L1PRT, DECIMAL_L1PRT), // Prix de transport
      new AS400PackedDecimal(SIZE_L1DLP, DECIMAL_L1DLP), // Date de livraison prévue
      new AS400Text(SIZE_L1IN2), // Ind. Type de gratuits
      new AS400Text(SIZE_L1IN3), // Ind. Kit,Taxe etc...
      new AS400Text(SIZE_L1TP1), // Top personnal.N°1
      new AS400Text(SIZE_L1TP2), // Top personnal.N°2
      new AS400Text(SIZE_L1TP3), // Top personnal.N°3
      new AS400Text(SIZE_L1TP4), // Top personnal.N°4
      new AS400Text(SIZE_L1TP5), // Top personnal.N°5
      new AS400ZonedDecimal(SIZE_L1GBA, DECIMAL_L1GBA), // Génération Bon Achat
      new AS400Text(SIZE_L1ARTS), // Code article substitué
      new AS400Text(SIZE_L1IN4), // Ind. Type substitution
      new AS400Text(SIZE_L1IN5), // Ind. remise 50/50 sur com/rp
      new AS400Text(SIZE_L1IN6), // Ind. PRV saisi sur ligne
      new AS400Text(SIZE_L1IN7), // Ind. recherche prix / kit
      new AS400PackedDecimal(SIZE_L1QTL, DECIMAL_L1QTL), // Quantité Livrable
      new AS400Text(SIZE_L1MAG), // Magasin
      new AS400Text(SIZE_L1REP), // Représentant
      new AS400Text(SIZE_L1IN8), // Indice de livraison
      new AS400Text(SIZE_L1IN9), // Ind cumul(pds,vol,col)/l1qte
      new AS400Text(SIZE_L1IN10), // Ind cumul(pds,vol,col)/l1qtp
      new AS400Text(SIZE_L1IN11), // Ligne à extraire
      new AS400Text(SIZE_L1IN12), // Ind. ASDI =A1IN13
      new AS400ZonedDecimal(SIZE_L1NLI0, DECIMAL_L1NLI0), // Numéro de Ligne origine
      new AS400PackedDecimal(SIZE_L1PRA, DECIMAL_L1PRA), // Prix à ajouter au PRV
      new AS400PackedDecimal(SIZE_L1PVA, DECIMAL_L1PVA), // Prix à ajouter au PVC
      new AS400PackedDecimal(SIZE_L1QTP, DECIMAL_L1QTP), // Quantité en Pièces
      new AS400PackedDecimal(SIZE_L1MTR, DECIMAL_L1MTR), // Montant transport
      new AS400Text(SIZE_L1SER3), // top article loti N.U
      new AS400Text(SIZE_L1SER5), // top article ads N.U
      new AS400Text(SIZE_L1IN17), // Type de vente
      new AS400Text(SIZE_L1IN18), // Prix garanti, affaire, dérog
      new AS400Text(SIZE_L1IN19), // Applic. condition quantitat.
      new AS400Text(SIZE_L1IN20), // Cond. Chantier Blanc,A F S G
      new AS400Text(SIZE_L1IN21), // Ind. regroupement ligne
      new AS400Text(SIZE_L1IN22), // N.U
      
      /* DLC */
      new AS400PackedDecimal(SIZE_L1QTEI, DECIMAL_L1QTEI), // Quantité Commandée initiale
      new AS400PackedDecimal(SIZE_L1QTLI, DECIMAL_L1QTLI), // Quantité Livrable initiale
      new AS400PackedDecimal(SIZE_L1QTPI, DECIMAL_L1QTPI), // Quantité en Pièces initiale
      new AS400PackedDecimal(SIZE_L1MHTI, DECIMAL_L1MHTI), // Montant Hors Taxes initiale
      /* DLC */
      
      new AS400Text(SIZE_A1UNL), // Code Unité de conditionnemet
      new AS400Text(SIZE_WLIGLB), // Libellé
      new AS400ZonedDecimal(SIZE_WFPVN, DECIMAL_WFPVN), // Prix de vente flash
      new AS400Text(SIZE_WFUSR), // Code utilisateur
      new AS400Text(SIZE_WFDAT), // Date prix flash
      new AS400ZonedDecimal(SIZE_WFHEU, DECIMAL_WFHEU), // Heure
      new AS400ZonedDecimal(SIZE_WPBTTC, DECIMAL_WPBTTC), // Prix de base TTC
      new AS400ZonedDecimal(SIZE_WPNTTC, DECIMAL_WPNTTC), // Prix net TTC
      new AS400ZonedDecimal(SIZE_WPVTTC, DECIMAL_WPVTTC), // Prix de vente calculé TTC
      new AS400ZonedDecimal(SIZE_WMTTTC, DECIMAL_WMTTTC), // Montant TTC
      new AS400ZonedDecimal(SIZE_WPT1HT, DECIMAL_WPT1HT), // Prix Tarif 1 H.T
      new AS400ZonedDecimal(SIZE_WPT1TTC, DECIMAL_WPT1TTC), // Prix Tarif 1 T.T.C
      new AS400ZonedDecimal(SIZE_WQTEOR, DECIMAL_WQTEOR), // Quantité document origine
      new AS400ZonedDecimal(SIZE_WQTEEX, DECIMAL_WQTEEX), // Quantité déjà extraites
      new AS400ZonedDecimal(SIZE_L2QT1, DECIMAL_L2QT1), // Quantité 1
      new AS400ZonedDecimal(SIZE_L2QT2, DECIMAL_L2QT2), // Quantité 2
      new AS400ZonedDecimal(SIZE_L2QT3, DECIMAL_L2QT3), // Quantité 3
      new AS400ZonedDecimal(SIZE_L2NBR, DECIMAL_L2NBR), // Nombre
      new AS400ZonedDecimal(SIZE_L12QT, DECIMAL_L12QT), // Quantité initiale
      new AS400Text(SIZE_L12UC), // Code unité initiale
      new AS400Text(SIZE_WORIPR), // Origine prix
      new AS400ZonedDecimal(SIZE_WTAUTVA, DECIMAL_WTAUTVA), // Taux de TVA
      new AS400Text(SIZE_WTYPART), // Type ligne
      new AS400ZonedDecimal(SIZE_L1PRS, DECIMAL_L1PRS), // Prix de revient standard
      new AS400ZonedDecimal(SIZE_WQTEFAC, DECIMAL_WQTEFAC), // Quantité déjà facturée
      new AS400ZonedDecimal(SIZE_WTOP, DECIMAL_WTOP), // Ligne totalement traitée
      new AS400ZonedDecimal(SIZE_WCOF, DECIMAL_WCOF), // Collectif fournisseur
      new AS400ZonedDecimal(SIZE_WFRS, DECIMAL_WFRS), // Numéro fournisseur
      new AS400ZonedDecimal(SIZE_WFRE, DECIMAL_WFRE), // Lien FRS / FRE
      new AS400Text(SIZE_FILLER), // Filler
  };
  private AS400Structure ds = new AS400Structure(structure);
  public Object[] o = { l1top, l1cod, l1etb, l1num, l1suf, l1nli, l1erl, l1cex, l1qex, l1tva, l1tb, l1tr, l1tn, l1tc, l1tt, l1val, l1tar,
      l1col, l1tpf, l1dcv, l1tnc, l1sgn, l1ser, l1qte, l1ksv, l1pvb, l1rem1, l1rem2, l1rem3, l1rem4, l1rem5, l1rem6, l1trl, l1brl, l1rp1,
      l1rp2, l1rp3, l1rp4, l1rp5, l1rp6, l1pvn, l1pvc, l1mht, l1prp, l1coe, l1prv, l1avr, l1unv, l1art, l1cpl, l1cnd, l1in1, l1prt, l1dlp,
      l1in2, l1in3, l1tp1, l1tp2, l1tp3, l1tp4, l1tp5, l1gba, l1arts, l1in4, l1in5, l1in6, l1in7, l1qtl, l1mag, l1rep, l1in8, l1in9,
      l1in10, l1in11, l1in12, l1nli0, l1pra, l1pva, l1qtp, l1mtr, l1ser3, l1ser5, l1in17, l1in18, l1in19, l1in20, l1in21, l1in22, l1qtei,
      l1qtli, l1qtpi, l1mhti, a1unl, wliglb, wfpvn, wfusr, wfdat, wfheu, wpbttc, wpnttc, wpvttc, wmtttc, wpt1ht, wpt1ttc, wqteor, wqteex,
      l2qt1, l2qt2, l2qt3, l2nbr, l12qt, l12uc, woripr, wtautva, wtypart, l1prs, wqtefac, wtop, wcof, wfrs, wfre, filler, };
  
  // -- Accesseurs
  
  public void setL1top(BigDecimal pL1top) {
    if (pL1top == null) {
      return;
    }
    l1top = pL1top.setScale(DECIMAL_L1TOP, RoundingMode.HALF_UP);
  }
  
  public void setL1top(Integer pL1top) {
    if (pL1top == null) {
      return;
    }
    l1top = BigDecimal.valueOf(pL1top);
  }
  
  public Integer getL1top() {
    return l1top.intValue();
  }
  
  public void setL1cod(Character pL1cod) {
    if (pL1cod == null) {
      return;
    }
    l1cod = String.valueOf(pL1cod);
  }
  
  public Character getL1cod() {
    return l1cod.charAt(0);
  }
  
  public void setL1etb(String pL1etb) {
    if (pL1etb == null) {
      return;
    }
    l1etb = pL1etb;
  }
  
  public String getL1etb() {
    return l1etb;
  }
  
  public void setL1num(BigDecimal pL1num) {
    if (pL1num == null) {
      return;
    }
    l1num = pL1num.setScale(DECIMAL_L1NUM, RoundingMode.HALF_UP);
  }
  
  public void setL1num(Integer pL1num) {
    if (pL1num == null) {
      return;
    }
    l1num = BigDecimal.valueOf(pL1num);
  }
  
  public Integer getL1num() {
    return l1num.intValue();
  }
  
  public void setL1suf(BigDecimal pL1suf) {
    if (pL1suf == null) {
      return;
    }
    l1suf = pL1suf.setScale(DECIMAL_L1SUF, RoundingMode.HALF_UP);
  }
  
  public void setL1suf(Integer pL1suf) {
    if (pL1suf == null) {
      return;
    }
    l1suf = BigDecimal.valueOf(pL1suf);
  }
  
  public Integer getL1suf() {
    return l1suf.intValue();
  }
  
  public void setL1nli(BigDecimal pL1nli) {
    if (pL1nli == null) {
      return;
    }
    l1nli = pL1nli.setScale(DECIMAL_L1NLI, RoundingMode.HALF_UP);
  }
  
  public void setL1nli(Integer pL1nli) {
    if (pL1nli == null) {
      return;
    }
    l1nli = BigDecimal.valueOf(pL1nli);
  }
  
  public Integer getL1nli() {
    return l1nli.intValue();
  }
  
  public void setL1erl(Character pL1erl) {
    if (pL1erl == null) {
      return;
    }
    l1erl = String.valueOf(pL1erl);
  }
  
  public Character getL1erl() {
    return l1erl.charAt(0);
  }
  
  public void setL1cex(Character pL1cex) {
    if (pL1cex == null) {
      return;
    }
    l1cex = String.valueOf(pL1cex);
  }
  
  public Character getL1cex() {
    return l1cex.charAt(0);
  }
  
  public void setL1qex(BigDecimal pL1qex) {
    if (pL1qex == null) {
      return;
    }
    l1qex = pL1qex.setScale(DECIMAL_L1QEX, RoundingMode.HALF_UP);
  }
  
  public void setL1qex(Double pL1qex) {
    if (pL1qex == null) {
      return;
    }
    l1qex = BigDecimal.valueOf(pL1qex).setScale(DECIMAL_L1QEX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1qex() {
    return l1qex.setScale(DECIMAL_L1QEX, RoundingMode.HALF_UP);
  }
  
  public void setL1tva(BigDecimal pL1tva) {
    if (pL1tva == null) {
      return;
    }
    l1tva = pL1tva.setScale(DECIMAL_L1TVA, RoundingMode.HALF_UP);
  }
  
  public void setL1tva(Integer pL1tva) {
    if (pL1tva == null) {
      return;
    }
    l1tva = BigDecimal.valueOf(pL1tva);
  }
  
  public Integer getL1tva() {
    return l1tva.intValue();
  }
  
  public void setL1tb(BigDecimal pL1tb) {
    if (pL1tb == null) {
      return;
    }
    l1tb = pL1tb.setScale(DECIMAL_L1TB, RoundingMode.HALF_UP);
  }
  
  public void setL1tb(Integer pL1tb) {
    if (pL1tb == null) {
      return;
    }
    l1tb = BigDecimal.valueOf(pL1tb);
  }
  
  public Integer getL1tb() {
    return l1tb.intValue();
  }
  
  public void setL1tr(BigDecimal pL1tr) {
    if (pL1tr == null) {
      return;
    }
    l1tr = pL1tr.setScale(DECIMAL_L1TR, RoundingMode.HALF_UP);
  }
  
  public void setL1tr(Integer pL1tr) {
    if (pL1tr == null) {
      return;
    }
    l1tr = BigDecimal.valueOf(pL1tr);
  }
  
  public Integer getL1tr() {
    return l1tr.intValue();
  }
  
  public void setL1tn(BigDecimal pL1tn) {
    if (pL1tn == null) {
      return;
    }
    l1tn = pL1tn.setScale(DECIMAL_L1TN, RoundingMode.HALF_UP);
  }
  
  public void setL1tn(Integer pL1tn) {
    if (pL1tn == null) {
      return;
    }
    l1tn = BigDecimal.valueOf(pL1tn);
  }
  
  public Integer getL1tn() {
    return l1tn.intValue();
  }
  
  public void setL1tc(BigDecimal pL1tc) {
    if (pL1tc == null) {
      return;
    }
    l1tc = pL1tc.setScale(DECIMAL_L1TC, RoundingMode.HALF_UP);
  }
  
  public void setL1tc(Integer pL1tc) {
    if (pL1tc == null) {
      return;
    }
    l1tc = BigDecimal.valueOf(pL1tc);
  }
  
  public Integer getL1tc() {
    return l1tc.intValue();
  }
  
  public void setL1tt(BigDecimal pL1tt) {
    if (pL1tt == null) {
      return;
    }
    l1tt = pL1tt.setScale(DECIMAL_L1TT, RoundingMode.HALF_UP);
  }
  
  public void setL1tt(Integer pL1tt) {
    if (pL1tt == null) {
      return;
    }
    l1tt = BigDecimal.valueOf(pL1tt);
  }
  
  public Integer getL1tt() {
    return l1tt.intValue();
  }
  
  public void setL1val(BigDecimal pL1val) {
    if (pL1val == null) {
      return;
    }
    l1val = pL1val.setScale(DECIMAL_L1VAL, RoundingMode.HALF_UP);
  }
  
  public void setL1val(Integer pL1val) {
    if (pL1val == null) {
      return;
    }
    l1val = BigDecimal.valueOf(pL1val);
  }
  
  public Integer getL1val() {
    return l1val.intValue();
  }
  
  public void setL1tar(BigDecimal pL1tar) {
    if (pL1tar == null) {
      return;
    }
    l1tar = pL1tar.setScale(DECIMAL_L1TAR, RoundingMode.HALF_UP);
  }
  
  public void setL1tar(Integer pL1tar) {
    if (pL1tar == null) {
      return;
    }
    l1tar = BigDecimal.valueOf(pL1tar);
  }
  
  public Integer getL1tar() {
    return l1tar.intValue();
  }
  
  public void setL1col(BigDecimal pL1col) {
    if (pL1col == null) {
      return;
    }
    l1col = pL1col.setScale(DECIMAL_L1COL, RoundingMode.HALF_UP);
  }
  
  public void setL1col(Integer pL1col) {
    if (pL1col == null) {
      return;
    }
    l1col = BigDecimal.valueOf(pL1col);
  }
  
  public Integer getL1col() {
    return l1col.intValue();
  }
  
  public void setL1tpf(BigDecimal pL1tpf) {
    if (pL1tpf == null) {
      return;
    }
    l1tpf = pL1tpf.setScale(DECIMAL_L1TPF, RoundingMode.HALF_UP);
  }
  
  public void setL1tpf(Integer pL1tpf) {
    if (pL1tpf == null) {
      return;
    }
    l1tpf = BigDecimal.valueOf(pL1tpf);
  }
  
  public Integer getL1tpf() {
    return l1tpf.intValue();
  }
  
  public void setL1dcv(BigDecimal pL1dcv) {
    if (pL1dcv == null) {
      return;
    }
    l1dcv = pL1dcv.setScale(DECIMAL_L1DCV, RoundingMode.HALF_UP);
  }
  
  public void setL1dcv(Integer pL1dcv) {
    if (pL1dcv == null) {
      return;
    }
    l1dcv = BigDecimal.valueOf(pL1dcv);
  }
  
  public Integer getL1dcv() {
    return l1dcv.intValue();
  }
  
  public void setL1tnc(BigDecimal pL1tnc) {
    if (pL1tnc == null) {
      return;
    }
    l1tnc = pL1tnc.setScale(DECIMAL_L1TNC, RoundingMode.HALF_UP);
  }
  
  public void setL1tnc(Integer pL1tnc) {
    if (pL1tnc == null) {
      return;
    }
    l1tnc = BigDecimal.valueOf(pL1tnc);
  }
  
  public Integer getL1tnc() {
    return l1tnc.intValue();
  }
  
  public void setL1sgn(BigDecimal pL1sgn) {
    if (pL1sgn == null) {
      return;
    }
    l1sgn = pL1sgn.setScale(DECIMAL_L1SGN, RoundingMode.HALF_UP);
  }
  
  public void setL1sgn(Integer pL1sgn) {
    if (pL1sgn == null) {
      return;
    }
    l1sgn = BigDecimal.valueOf(pL1sgn);
  }
  
  public Integer getL1sgn() {
    return l1sgn.intValue();
  }
  
  public void setL1ser(BigDecimal pL1ser) {
    if (pL1ser == null) {
      return;
    }
    l1ser = pL1ser.setScale(DECIMAL_L1SER, RoundingMode.HALF_UP);
  }
  
  public void setL1ser(Integer pL1ser) {
    if (pL1ser == null) {
      return;
    }
    l1ser = BigDecimal.valueOf(pL1ser);
  }
  
  public Integer getL1ser() {
    return l1ser.intValue();
  }
  
  public void setL1qte(BigDecimal pL1qte) {
    if (pL1qte == null) {
      return;
    }
    l1qte = pL1qte.setScale(DECIMAL_L1QTE, RoundingMode.HALF_UP);
  }
  
  public void setL1qte(Double pL1qte) {
    if (pL1qte == null) {
      return;
    }
    l1qte = BigDecimal.valueOf(pL1qte).setScale(DECIMAL_L1QTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1qte() {
    return l1qte.setScale(DECIMAL_L1QTE, RoundingMode.HALF_UP);
  }
  
  public void setL1ksv(BigDecimal pL1ksv) {
    if (pL1ksv == null) {
      return;
    }
    l1ksv = pL1ksv.setScale(DECIMAL_L1KSV, RoundingMode.HALF_UP);
  }
  
  public void setL1ksv(Double pL1ksv) {
    if (pL1ksv == null) {
      return;
    }
    l1ksv = BigDecimal.valueOf(pL1ksv).setScale(DECIMAL_L1KSV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1ksv() {
    return l1ksv.setScale(DECIMAL_L1KSV, RoundingMode.HALF_UP);
  }
  
  public void setL1pvb(BigDecimal pL1pvb) {
    if (pL1pvb == null) {
      return;
    }
    l1pvb = pL1pvb.setScale(DECIMAL_L1PVB, RoundingMode.HALF_UP);
  }
  
  public void setL1pvb(Double pL1pvb) {
    if (pL1pvb == null) {
      return;
    }
    l1pvb = BigDecimal.valueOf(pL1pvb).setScale(DECIMAL_L1PVB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1pvb() {
    return l1pvb.setScale(DECIMAL_L1PVB, RoundingMode.HALF_UP);
  }
  
  public void setL1rem1(BigDecimal pL1rem1) {
    if (pL1rem1 == null) {
      return;
    }
    l1rem1 = pL1rem1.setScale(DECIMAL_L1REM1, RoundingMode.HALF_UP);
  }
  
  public void setL1rem1(Double pL1rem1) {
    if (pL1rem1 == null) {
      return;
    }
    l1rem1 = BigDecimal.valueOf(pL1rem1).setScale(DECIMAL_L1REM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1rem1() {
    return l1rem1.setScale(DECIMAL_L1REM1, RoundingMode.HALF_UP);
  }
  
  public void setL1rem2(BigDecimal pL1rem2) {
    if (pL1rem2 == null) {
      return;
    }
    l1rem2 = pL1rem2.setScale(DECIMAL_L1REM2, RoundingMode.HALF_UP);
  }
  
  public void setL1rem2(Double pL1rem2) {
    if (pL1rem2 == null) {
      return;
    }
    l1rem2 = BigDecimal.valueOf(pL1rem2).setScale(DECIMAL_L1REM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1rem2() {
    return l1rem2.setScale(DECIMAL_L1REM2, RoundingMode.HALF_UP);
  }
  
  public void setL1rem3(BigDecimal pL1rem3) {
    if (pL1rem3 == null) {
      return;
    }
    l1rem3 = pL1rem3.setScale(DECIMAL_L1REM3, RoundingMode.HALF_UP);
  }
  
  public void setL1rem3(Double pL1rem3) {
    if (pL1rem3 == null) {
      return;
    }
    l1rem3 = BigDecimal.valueOf(pL1rem3).setScale(DECIMAL_L1REM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1rem3() {
    return l1rem3.setScale(DECIMAL_L1REM3, RoundingMode.HALF_UP);
  }
  
  public void setL1rem4(BigDecimal pL1rem4) {
    if (pL1rem4 == null) {
      return;
    }
    l1rem4 = pL1rem4.setScale(DECIMAL_L1REM4, RoundingMode.HALF_UP);
  }
  
  public void setL1rem4(Double pL1rem4) {
    if (pL1rem4 == null) {
      return;
    }
    l1rem4 = BigDecimal.valueOf(pL1rem4).setScale(DECIMAL_L1REM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1rem4() {
    return l1rem4.setScale(DECIMAL_L1REM4, RoundingMode.HALF_UP);
  }
  
  public void setL1rem5(BigDecimal pL1rem5) {
    if (pL1rem5 == null) {
      return;
    }
    l1rem5 = pL1rem5.setScale(DECIMAL_L1REM5, RoundingMode.HALF_UP);
  }
  
  public void setL1rem5(Double pL1rem5) {
    if (pL1rem5 == null) {
      return;
    }
    l1rem5 = BigDecimal.valueOf(pL1rem5).setScale(DECIMAL_L1REM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1rem5() {
    return l1rem5.setScale(DECIMAL_L1REM5, RoundingMode.HALF_UP);
  }
  
  public void setL1rem6(BigDecimal pL1rem6) {
    if (pL1rem6 == null) {
      return;
    }
    l1rem6 = pL1rem6.setScale(DECIMAL_L1REM6, RoundingMode.HALF_UP);
  }
  
  public void setL1rem6(Double pL1rem6) {
    if (pL1rem6 == null) {
      return;
    }
    l1rem6 = BigDecimal.valueOf(pL1rem6).setScale(DECIMAL_L1REM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1rem6() {
    return l1rem6.setScale(DECIMAL_L1REM6, RoundingMode.HALF_UP);
  }
  
  public void setL1trl(Character pL1trl) {
    if (pL1trl == null) {
      return;
    }
    l1trl = String.valueOf(pL1trl);
  }
  
  public Character getL1trl() {
    return l1trl.charAt(0);
  }
  
  public void setL1brl(Character pL1brl) {
    if (pL1brl == null) {
      return;
    }
    l1brl = String.valueOf(pL1brl);
  }
  
  public Character getL1brl() {
    return l1brl.charAt(0);
  }
  
  public void setL1rp1(Character pL1rp1) {
    if (pL1rp1 == null) {
      return;
    }
    l1rp1 = String.valueOf(pL1rp1);
  }
  
  public Character getL1rp1() {
    return l1rp1.charAt(0);
  }
  
  public void setL1rp2(Character pL1rp2) {
    if (pL1rp2 == null) {
      return;
    }
    l1rp2 = String.valueOf(pL1rp2);
  }
  
  public Character getL1rp2() {
    return l1rp2.charAt(0);
  }
  
  public void setL1rp3(Character pL1rp3) {
    if (pL1rp3 == null) {
      return;
    }
    l1rp3 = String.valueOf(pL1rp3);
  }
  
  public Character getL1rp3() {
    return l1rp3.charAt(0);
  }
  
  public void setL1rp4(Character pL1rp4) {
    if (pL1rp4 == null) {
      return;
    }
    l1rp4 = String.valueOf(pL1rp4);
  }
  
  public Character getL1rp4() {
    return l1rp4.charAt(0);
  }
  
  public void setL1rp5(Character pL1rp5) {
    if (pL1rp5 == null) {
      return;
    }
    l1rp5 = String.valueOf(pL1rp5);
  }
  
  public Character getL1rp5() {
    return l1rp5.charAt(0);
  }
  
  public void setL1rp6(Character pL1rp6) {
    if (pL1rp6 == null) {
      return;
    }
    l1rp6 = String.valueOf(pL1rp6);
  }
  
  public Character getL1rp6() {
    return l1rp6.charAt(0);
  }
  
  public void setL1pvn(BigDecimal pL1pvn) {
    if (pL1pvn == null) {
      return;
    }
    l1pvn = pL1pvn.setScale(DECIMAL_L1PVN, RoundingMode.HALF_UP);
  }
  
  public void setL1pvn(Double pL1pvn) {
    if (pL1pvn == null) {
      return;
    }
    l1pvn = BigDecimal.valueOf(pL1pvn).setScale(DECIMAL_L1PVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1pvn() {
    return l1pvn.setScale(DECIMAL_L1PVN, RoundingMode.HALF_UP);
  }
  
  public void setL1pvc(BigDecimal pL1pvc) {
    if (pL1pvc == null) {
      return;
    }
    l1pvc = pL1pvc.setScale(DECIMAL_L1PVC, RoundingMode.HALF_UP);
  }
  
  public void setL1pvc(Double pL1pvc) {
    if (pL1pvc == null) {
      return;
    }
    l1pvc = BigDecimal.valueOf(pL1pvc).setScale(DECIMAL_L1PVC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1pvc() {
    return l1pvc.setScale(DECIMAL_L1PVC, RoundingMode.HALF_UP);
  }
  
  public void setL1mht(BigDecimal pL1mht) {
    if (pL1mht == null) {
      return;
    }
    l1mht = pL1mht.setScale(DECIMAL_L1MHT, RoundingMode.HALF_UP);
  }
  
  public void setL1mht(Double pL1mht) {
    if (pL1mht == null) {
      return;
    }
    l1mht = BigDecimal.valueOf(pL1mht).setScale(DECIMAL_L1MHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1mht() {
    return l1mht.setScale(DECIMAL_L1MHT, RoundingMode.HALF_UP);
  }
  
  public void setL1prp(BigDecimal pL1prp) {
    if (pL1prp == null) {
      return;
    }
    l1prp = pL1prp.setScale(DECIMAL_L1PRP, RoundingMode.HALF_UP);
  }
  
  public void setL1prp(Double pL1prp) {
    if (pL1prp == null) {
      return;
    }
    l1prp = BigDecimal.valueOf(pL1prp).setScale(DECIMAL_L1PRP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1prp() {
    return l1prp.setScale(DECIMAL_L1PRP, RoundingMode.HALF_UP);
  }
  
  public void setL1coe(BigDecimal pL1coe) {
    if (pL1coe == null) {
      return;
    }
    l1coe = pL1coe.setScale(DECIMAL_L1COE, RoundingMode.HALF_UP);
  }
  
  public void setL1coe(Double pL1coe) {
    if (pL1coe == null) {
      return;
    }
    l1coe = BigDecimal.valueOf(pL1coe).setScale(DECIMAL_L1COE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1coe() {
    return l1coe.setScale(DECIMAL_L1COE, RoundingMode.HALF_UP);
  }
  
  public void setL1prv(BigDecimal pL1prv) {
    if (pL1prv == null) {
      return;
    }
    l1prv = pL1prv.setScale(DECIMAL_L1PRV, RoundingMode.HALF_UP);
  }
  
  public void setL1prv(Double pL1prv) {
    if (pL1prv == null) {
      return;
    }
    l1prv = BigDecimal.valueOf(pL1prv).setScale(DECIMAL_L1PRV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1prv() {
    return l1prv.setScale(DECIMAL_L1PRV, RoundingMode.HALF_UP);
  }
  
  public void setL1avr(Character pL1avr) {
    if (pL1avr == null) {
      return;
    }
    l1avr = String.valueOf(pL1avr);
  }
  
  public Character getL1avr() {
    return l1avr.charAt(0);
  }
  
  public void setL1unv(String pL1unv) {
    if (pL1unv == null) {
      return;
    }
    l1unv = pL1unv;
  }
  
  public String getL1unv() {
    return l1unv;
  }
  
  public void setL1art(String pL1art) {
    if (pL1art == null) {
      return;
    }
    l1art = pL1art;
  }
  
  public String getL1art() {
    return l1art;
  }
  
  public void setL1cpl(String pL1cpl) {
    if (pL1cpl == null) {
      return;
    }
    l1cpl = pL1cpl;
  }
  
  public String getL1cpl() {
    return l1cpl;
  }
  
  public void setL1cnd(BigDecimal pL1cnd) {
    if (pL1cnd == null) {
      return;
    }
    l1cnd = pL1cnd.setScale(DECIMAL_L1CND, RoundingMode.HALF_UP);
  }
  
  public void setL1cnd(Double pL1cnd) {
    if (pL1cnd == null) {
      return;
    }
    l1cnd = BigDecimal.valueOf(pL1cnd).setScale(DECIMAL_L1CND, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1cnd() {
    return l1cnd.setScale(DECIMAL_L1CND, RoundingMode.HALF_UP);
  }
  
  public void setL1in1(Character pL1in1) {
    if (pL1in1 == null) {
      return;
    }
    l1in1 = String.valueOf(pL1in1);
  }
  
  public Character getL1in1() {
    return l1in1.charAt(0);
  }
  
  public void setL1prt(BigDecimal pL1prt) {
    if (pL1prt == null) {
      return;
    }
    l1prt = pL1prt.setScale(DECIMAL_L1PRT, RoundingMode.HALF_UP);
  }
  
  public void setL1prt(Double pL1prt) {
    if (pL1prt == null) {
      return;
    }
    l1prt = BigDecimal.valueOf(pL1prt).setScale(DECIMAL_L1PRT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1prt() {
    return l1prt.setScale(DECIMAL_L1PRT, RoundingMode.HALF_UP);
  }
  
  public void setL1dlp(BigDecimal pL1dlp) {
    if (pL1dlp == null) {
      return;
    }
    l1dlp = pL1dlp.setScale(DECIMAL_L1DLP, RoundingMode.HALF_UP);
  }
  
  public void setL1dlp(Integer pL1dlp) {
    if (pL1dlp == null) {
      return;
    }
    l1dlp = BigDecimal.valueOf(pL1dlp);
  }
  
  public void setL1dlp(Date pL1dlp) {
    if (pL1dlp == null) {
      return;
    }
    l1dlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pL1dlp));
  }
  
  public Integer getL1dlp() {
    return l1dlp.intValue();
  }
  
  public Date getL1dlpConvertiEnDate() {
    return ConvertDate.db2ToDate(l1dlp.intValue(), null);
  }
  
  public void setL1in2(Character pL1in2) {
    if (pL1in2 == null) {
      return;
    }
    l1in2 = String.valueOf(pL1in2);
  }
  
  public Character getL1in2() {
    return l1in2.charAt(0);
  }
  
  public void setL1in3(Character pL1in3) {
    if (pL1in3 == null) {
      return;
    }
    l1in3 = String.valueOf(pL1in3);
  }
  
  public Character getL1in3() {
    return l1in3.charAt(0);
  }
  
  public void setL1tp1(String pL1tp1) {
    if (pL1tp1 == null) {
      return;
    }
    l1tp1 = pL1tp1;
  }
  
  public String getL1tp1() {
    return l1tp1;
  }
  
  public void setL1tp2(String pL1tp2) {
    if (pL1tp2 == null) {
      return;
    }
    l1tp2 = pL1tp2;
  }
  
  public String getL1tp2() {
    return l1tp2;
  }
  
  public void setL1tp3(String pL1tp3) {
    if (pL1tp3 == null) {
      return;
    }
    l1tp3 = pL1tp3;
  }
  
  public String getL1tp3() {
    return l1tp3;
  }
  
  public void setL1tp4(String pL1tp4) {
    if (pL1tp4 == null) {
      return;
    }
    l1tp4 = pL1tp4;
  }
  
  public String getL1tp4() {
    return l1tp4;
  }
  
  public void setL1tp5(String pL1tp5) {
    if (pL1tp5 == null) {
      return;
    }
    l1tp5 = pL1tp5;
  }
  
  public String getL1tp5() {
    return l1tp5;
  }
  
  public void setL1gba(BigDecimal pL1gba) {
    if (pL1gba == null) {
      return;
    }
    l1gba = pL1gba.setScale(DECIMAL_L1GBA, RoundingMode.HALF_UP);
  }
  
  public void setL1gba(Integer pL1gba) {
    if (pL1gba == null) {
      return;
    }
    l1gba = BigDecimal.valueOf(pL1gba);
  }
  
  public Integer getL1gba() {
    return l1gba.intValue();
  }
  
  public void setL1arts(String pL1arts) {
    if (pL1arts == null) {
      return;
    }
    l1arts = pL1arts;
  }
  
  public String getL1arts() {
    return l1arts;
  }
  
  public void setL1in4(Character pL1in4) {
    if (pL1in4 == null) {
      return;
    }
    l1in4 = String.valueOf(pL1in4);
  }
  
  public Character getL1in4() {
    return l1in4.charAt(0);
  }
  
  public void setL1in5(Character pL1in5) {
    if (pL1in5 == null) {
      return;
    }
    l1in5 = String.valueOf(pL1in5);
  }
  
  public Character getL1in5() {
    return l1in5.charAt(0);
  }
  
  public void setL1in6(Character pL1in6) {
    if (pL1in6 == null) {
      return;
    }
    l1in6 = String.valueOf(pL1in6);
  }
  
  public Character getL1in6() {
    return l1in6.charAt(0);
  }
  
  public void setL1in7(Character pL1in7) {
    if (pL1in7 == null) {
      return;
    }
    l1in7 = String.valueOf(pL1in7);
  }
  
  public Character getL1in7() {
    return l1in7.charAt(0);
  }
  
  public void setL1qtl(BigDecimal pL1qtl) {
    if (pL1qtl == null) {
      return;
    }
    l1qtl = pL1qtl.setScale(DECIMAL_L1QTL, RoundingMode.HALF_UP);
  }
  
  public void setL1qtl(Double pL1qtl) {
    if (pL1qtl == null) {
      return;
    }
    l1qtl = BigDecimal.valueOf(pL1qtl).setScale(DECIMAL_L1QTL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1qtl() {
    return l1qtl.setScale(DECIMAL_L1QTL, RoundingMode.HALF_UP);
  }
  
  public void setL1mag(String pL1mag) {
    if (pL1mag == null) {
      return;
    }
    l1mag = pL1mag;
  }
  
  public String getL1mag() {
    return l1mag;
  }
  
  public void setL1rep(String pL1rep) {
    if (pL1rep == null) {
      return;
    }
    l1rep = pL1rep;
  }
  
  public String getL1rep() {
    return l1rep;
  }
  
  public void setL1in8(Character pL1in8) {
    if (pL1in8 == null) {
      return;
    }
    l1in8 = String.valueOf(pL1in8);
  }
  
  public Character getL1in8() {
    return l1in8.charAt(0);
  }
  
  public void setL1in9(Character pL1in9) {
    if (pL1in9 == null) {
      return;
    }
    l1in9 = String.valueOf(pL1in9);
  }
  
  public Character getL1in9() {
    return l1in9.charAt(0);
  }
  
  public void setL1in10(Character pL1in10) {
    if (pL1in10 == null) {
      return;
    }
    l1in10 = String.valueOf(pL1in10);
  }
  
  public Character getL1in10() {
    return l1in10.charAt(0);
  }
  
  public void setL1in11(Character pL1in11) {
    if (pL1in11 == null) {
      return;
    }
    l1in11 = String.valueOf(pL1in11);
  }
  
  public Character getL1in11() {
    return l1in11.charAt(0);
  }
  
  public void setL1in12(Character pL1in12) {
    if (pL1in12 == null) {
      return;
    }
    l1in12 = String.valueOf(pL1in12);
  }
  
  public Character getL1in12() {
    return l1in12.charAt(0);
  }
  
  public void setL1nli0(BigDecimal pL1nli0) {
    if (pL1nli0 == null) {
      return;
    }
    l1nli0 = pL1nli0.setScale(DECIMAL_L1NLI0, RoundingMode.HALF_UP);
  }
  
  public void setL1nli0(Integer pL1nli0) {
    if (pL1nli0 == null) {
      return;
    }
    l1nli0 = BigDecimal.valueOf(pL1nli0);
  }
  
  public Integer getL1nli0() {
    return l1nli0.intValue();
  }
  
  public void setL1pra(BigDecimal pL1pra) {
    if (pL1pra == null) {
      return;
    }
    l1pra = pL1pra.setScale(DECIMAL_L1PRA, RoundingMode.HALF_UP);
  }
  
  public void setL1pra(Double pL1pra) {
    if (pL1pra == null) {
      return;
    }
    l1pra = BigDecimal.valueOf(pL1pra).setScale(DECIMAL_L1PRA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1pra() {
    return l1pra.setScale(DECIMAL_L1PRA, RoundingMode.HALF_UP);
  }
  
  public void setL1pva(BigDecimal pL1pva) {
    if (pL1pva == null) {
      return;
    }
    l1pva = pL1pva.setScale(DECIMAL_L1PVA, RoundingMode.HALF_UP);
  }
  
  public void setL1pva(Double pL1pva) {
    if (pL1pva == null) {
      return;
    }
    l1pva = BigDecimal.valueOf(pL1pva).setScale(DECIMAL_L1PVA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1pva() {
    return l1pva.setScale(DECIMAL_L1PVA, RoundingMode.HALF_UP);
  }
  
  public void setL1qtp(BigDecimal pL1qtp) {
    if (pL1qtp == null) {
      return;
    }
    l1qtp = pL1qtp.setScale(DECIMAL_L1QTP, RoundingMode.HALF_UP);
  }
  
  public void setL1qtp(Double pL1qtp) {
    if (pL1qtp == null) {
      return;
    }
    l1qtp = BigDecimal.valueOf(pL1qtp).setScale(DECIMAL_L1QTP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1qtp() {
    return l1qtp.setScale(DECIMAL_L1QTP, RoundingMode.HALF_UP);
  }
  
  public void setL1mtr(BigDecimal pL1mtr) {
    if (pL1mtr == null) {
      return;
    }
    l1mtr = pL1mtr.setScale(DECIMAL_L1MTR, RoundingMode.HALF_UP);
  }
  
  public void setL1mtr(Double pL1mtr) {
    if (pL1mtr == null) {
      return;
    }
    l1mtr = BigDecimal.valueOf(pL1mtr).setScale(DECIMAL_L1MTR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1mtr() {
    return l1mtr.setScale(DECIMAL_L1MTR, RoundingMode.HALF_UP);
  }
  
  public void setL1ser3(Character pL1ser3) {
    if (pL1ser3 == null) {
      return;
    }
    l1ser3 = String.valueOf(pL1ser3);
  }
  
  public Character getL1ser3() {
    return l1ser3.charAt(0);
  }
  
  public void setL1ser5(Character pL1ser5) {
    if (pL1ser5 == null) {
      return;
    }
    l1ser5 = String.valueOf(pL1ser5);
  }
  
  public Character getL1ser5() {
    return l1ser5.charAt(0);
  }
  
  public void setL1in17(Character pL1in17) {
    if (pL1in17 == null) {
      return;
    }
    l1in17 = String.valueOf(pL1in17);
  }
  
  public Character getL1in17() {
    return l1in17.charAt(0);
  }
  
  public void setL1in18(Character pL1in18) {
    if (pL1in18 == null) {
      return;
    }
    l1in18 = String.valueOf(pL1in18);
  }
  
  public Character getL1in18() {
    return l1in18.charAt(0);
  }
  
  public void setL1in19(Character pL1in19) {
    if (pL1in19 == null) {
      return;
    }
    l1in19 = String.valueOf(pL1in19);
  }
  
  public Character getL1in19() {
    return l1in19.charAt(0);
  }
  
  public void setL1in20(Character pL1in20) {
    if (pL1in20 == null) {
      return;
    }
    l1in20 = String.valueOf(pL1in20);
  }
  
  public Character getL1in20() {
    return l1in20.charAt(0);
  }
  
  public void setL1in21(Character pL1in21) {
    if (pL1in21 == null) {
      return;
    }
    l1in21 = String.valueOf(pL1in21);
  }
  
  public Character getL1in21() {
    return l1in21.charAt(0);
  }
  
  public void setL1in22(Character pL1in22) {
    if (pL1in22 == null) {
      return;
    }
    l1in22 = String.valueOf(pL1in22);
  }
  
  public Character getL1in22() {
    return l1in22.charAt(0);
  }
  
  /* DLC */
  
  public void setL1qtei(BigDecimal pL1qtei) {
    if (pL1qtei == null) {
      return;
    }
    l1qtei = pL1qtei.setScale(DECIMAL_L1QTEI, RoundingMode.HALF_UP);
  }
  
  public void setL1qtei(Double pL1qtei) {
    if (pL1qtei == null) {
      return;
    }
    l1qtei = BigDecimal.valueOf(pL1qtei).setScale(DECIMAL_L1QTEI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1qtei() {
    return l1qtei.setScale(DECIMAL_L1QTEI, RoundingMode.HALF_UP);
  }
  
  public void setL1qtli(BigDecimal pL1qtli) {
    if (pL1qtli == null) {
      return;
    }
    l1qtli = pL1qtli.setScale(DECIMAL_L1QTLI, RoundingMode.HALF_UP);
  }
  
  public void setL1qtli(Double pL1qtli) {
    if (pL1qtli == null) {
      return;
    }
    l1qtli = BigDecimal.valueOf(pL1qtli).setScale(DECIMAL_L1QTLI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1qtli() {
    return l1qtli.setScale(DECIMAL_L1QTLI, RoundingMode.HALF_UP);
  }
  
  public void setL1qtpi(BigDecimal pL1qtpi) {
    if (pL1qtpi == null) {
      return;
    }
    l1qtpi = pL1qtpi.setScale(DECIMAL_L1QTPI, RoundingMode.HALF_UP);
  }
  
  public void setL1qtpi(Double pL1qtpi) {
    if (pL1qtpi == null) {
      return;
    }
    l1qtpi = BigDecimal.valueOf(pL1qtpi).setScale(DECIMAL_L1QTPI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1qtpi() {
    return l1qtpi.setScale(DECIMAL_L1QTPI, RoundingMode.HALF_UP);
  }
  
  public void setL1mhti(BigDecimal pL1mhti) {
    if (pL1mhti == null) {
      return;
    }
    l1mhti = pL1mhti.setScale(DECIMAL_L1MHTI, RoundingMode.HALF_UP);
  }
  
  public void setL1mhti(Double pL1mhti) {
    if (pL1mhti == null) {
      return;
    }
    l1mhti = BigDecimal.valueOf(pL1mhti).setScale(DECIMAL_L1MHTI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1mhti() {
    return l1mhti.setScale(DECIMAL_L1MHTI, RoundingMode.HALF_UP);
  }
  /* DLC */
  
  public void setA1unl(String pA1unl) {
    if (pA1unl == null) {
      return;
    }
    a1unl = pA1unl;
  }
  
  public String getA1unl() {
    return a1unl;
  }
  
  public void setWliglb(String pWliglb) {
    if (pWliglb == null) {
      return;
    }
    wliglb = pWliglb;
  }
  
  public String getWliglb() {
    return wliglb;
  }
  
  public void setWfpvn(BigDecimal pWfpvn) {
    if (pWfpvn == null) {
      return;
    }
    wfpvn = pWfpvn.setScale(DECIMAL_WFPVN, RoundingMode.HALF_UP);
  }
  
  public void setWfpvn(Double pWfpvn) {
    if (pWfpvn == null) {
      return;
    }
    wfpvn = BigDecimal.valueOf(pWfpvn).setScale(DECIMAL_WFPVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWfpvn() {
    return wfpvn.setScale(DECIMAL_WFPVN, RoundingMode.HALF_UP);
  }
  
  public void setWfusr(String pWfusr) {
    if (pWfusr == null) {
      return;
    }
    wfusr = pWfusr;
  }
  
  public String getWfusr() {
    return wfusr;
  }
  
  public void setWfdat(String pWfdat) {
    if (pWfdat == null) {
      return;
    }
    wfdat = pWfdat;
  }
  
  public String getWfdat() {
    return wfdat;
  }
  
  public void setWfheu(BigDecimal pWfheu) {
    if (pWfheu == null) {
      return;
    }
    wfheu = pWfheu.setScale(DECIMAL_WFHEU, RoundingMode.HALF_UP);
  }
  
  public void setWfheu(Integer pWfheu) {
    if (pWfheu == null) {
      return;
    }
    wfheu = BigDecimal.valueOf(pWfheu);
  }
  
  public Integer getWfheu() {
    return wfheu.intValue();
  }
  
  public void setWpbttc(BigDecimal pWpbttc) {
    if (pWpbttc == null) {
      return;
    }
    wpbttc = pWpbttc.setScale(DECIMAL_WPBTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpbttc(Double pWpbttc) {
    if (pWpbttc == null) {
      return;
    }
    wpbttc = BigDecimal.valueOf(pWpbttc).setScale(DECIMAL_WPBTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpbttc() {
    return wpbttc.setScale(DECIMAL_WPBTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpnttc(BigDecimal pWpnttc) {
    if (pWpnttc == null) {
      return;
    }
    wpnttc = pWpnttc.setScale(DECIMAL_WPNTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpnttc(Double pWpnttc) {
    if (pWpnttc == null) {
      return;
    }
    wpnttc = BigDecimal.valueOf(pWpnttc).setScale(DECIMAL_WPNTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpnttc() {
    return wpnttc.setScale(DECIMAL_WPNTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpvttc(BigDecimal pWpvttc) {
    if (pWpvttc == null) {
      return;
    }
    wpvttc = pWpvttc.setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpvttc(Double pWpvttc) {
    if (pWpvttc == null) {
      return;
    }
    wpvttc = BigDecimal.valueOf(pWpvttc).setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpvttc() {
    return wpvttc.setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public void setWmtttc(BigDecimal pWmtttc) {
    if (pWmtttc == null) {
      return;
    }
    wmtttc = pWmtttc.setScale(DECIMAL_WMTTTC, RoundingMode.HALF_UP);
  }
  
  public void setWmtttc(Double pWmtttc) {
    if (pWmtttc == null) {
      return;
    }
    wmtttc = BigDecimal.valueOf(pWmtttc).setScale(DECIMAL_WMTTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmtttc() {
    return wmtttc.setScale(DECIMAL_WMTTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ht(BigDecimal pWpt1ht) {
    if (pWpt1ht == null) {
      return;
    }
    wpt1ht = pWpt1ht.setScale(DECIMAL_WPT1HT, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ht(Double pWpt1ht) {
    if (pWpt1ht == null) {
      return;
    }
    wpt1ht = BigDecimal.valueOf(pWpt1ht).setScale(DECIMAL_WPT1HT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpt1ht() {
    return wpt1ht.setScale(DECIMAL_WPT1HT, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ttc(BigDecimal pWpt1ttc) {
    if (pWpt1ttc == null) {
      return;
    }
    wpt1ttc = pWpt1ttc.setScale(DECIMAL_WPT1TTC, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ttc(Double pWpt1ttc) {
    if (pWpt1ttc == null) {
      return;
    }
    wpt1ttc = BigDecimal.valueOf(pWpt1ttc).setScale(DECIMAL_WPT1TTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpt1ttc() {
    return wpt1ttc.setScale(DECIMAL_WPT1TTC, RoundingMode.HALF_UP);
  }
  
  public void setWqteor(BigDecimal pWqteor) {
    if (pWqteor == null) {
      return;
    }
    wqteor = pWqteor.setScale(DECIMAL_WQTEOR, RoundingMode.HALF_UP);
  }
  
  public void setWqteor(Double pWqteor) {
    if (pWqteor == null) {
      return;
    }
    wqteor = BigDecimal.valueOf(pWqteor).setScale(DECIMAL_WQTEOR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqteor() {
    return wqteor.setScale(DECIMAL_WQTEOR, RoundingMode.HALF_UP);
  }
  
  public void setWqteex(BigDecimal pWqteex) {
    if (pWqteex == null) {
      return;
    }
    wqteex = pWqteex.setScale(DECIMAL_WQTEEX, RoundingMode.HALF_UP);
  }
  
  public void setWqteex(Double pWqteex) {
    if (pWqteex == null) {
      return;
    }
    wqteex = BigDecimal.valueOf(pWqteex).setScale(DECIMAL_WQTEEX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqteex() {
    return wqteex.setScale(DECIMAL_WQTEEX, RoundingMode.HALF_UP);
  }
  
  public void setL2qt1(BigDecimal pL2qt1) {
    if (pL2qt1 == null) {
      return;
    }
    l2qt1 = pL2qt1.setScale(DECIMAL_L2QT1, RoundingMode.HALF_UP);
  }
  
  public void setL2qt1(Double pL2qt1) {
    if (pL2qt1 == null) {
      return;
    }
    l2qt1 = BigDecimal.valueOf(pL2qt1).setScale(DECIMAL_L2QT1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL2qt1() {
    return l2qt1.setScale(DECIMAL_L2QT1, RoundingMode.HALF_UP);
  }
  
  public void setL2qt2(BigDecimal pL2qt2) {
    if (pL2qt2 == null) {
      return;
    }
    l2qt2 = pL2qt2.setScale(DECIMAL_L2QT2, RoundingMode.HALF_UP);
  }
  
  public void setL2qt2(Double pL2qt2) {
    if (pL2qt2 == null) {
      return;
    }
    l2qt2 = BigDecimal.valueOf(pL2qt2).setScale(DECIMAL_L2QT2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL2qt2() {
    return l2qt2.setScale(DECIMAL_L2QT2, RoundingMode.HALF_UP);
  }
  
  public void setL2qt3(BigDecimal pL2qt3) {
    if (pL2qt3 == null) {
      return;
    }
    l2qt3 = pL2qt3.setScale(DECIMAL_L2QT3, RoundingMode.HALF_UP);
  }
  
  public void setL2qt3(Double pL2qt3) {
    if (pL2qt3 == null) {
      return;
    }
    l2qt3 = BigDecimal.valueOf(pL2qt3).setScale(DECIMAL_L2QT3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL2qt3() {
    return l2qt3.setScale(DECIMAL_L2QT3, RoundingMode.HALF_UP);
  }
  
  public void setL2nbr(BigDecimal pL2nbr) {
    if (pL2nbr == null) {
      return;
    }
    l2nbr = pL2nbr.setScale(DECIMAL_L2NBR, RoundingMode.HALF_UP);
  }
  
  public void setL2nbr(Integer pL2nbr) {
    if (pL2nbr == null) {
      return;
    }
    l2nbr = BigDecimal.valueOf(pL2nbr);
  }
  
  public Integer getL2nbr() {
    return l2nbr.intValue();
  }
  
  public void setL12qt(BigDecimal pL12qt) {
    if (pL12qt == null) {
      return;
    }
    l12qt = pL12qt.setScale(DECIMAL_L12QT, RoundingMode.HALF_UP);
  }
  
  public void setL12qt(Double pL12qt) {
    if (pL12qt == null) {
      return;
    }
    l12qt = BigDecimal.valueOf(pL12qt).setScale(DECIMAL_L12QT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL12qt() {
    return l12qt.setScale(DECIMAL_L12QT, RoundingMode.HALF_UP);
  }
  
  public void setL12uc(String pL12uc) {
    if (pL12uc == null) {
      return;
    }
    l12uc = pL12uc;
  }
  
  public String getL12uc() {
    return l12uc;
  }
  
  public void setWoripr(String pWoripr) {
    if (pWoripr == null) {
      return;
    }
    woripr = pWoripr;
  }
  
  public String getWoripr() {
    return woripr;
  }
  
  public void setWtautva(BigDecimal pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = pWtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setWtautva(Double pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = BigDecimal.valueOf(pWtautva).setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWtautva() {
    return wtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setWtypart(String pWtypart) {
    if (pWtypart == null) {
      return;
    }
    wtypart = pWtypart;
  }
  
  public String getWtypart() {
    return wtypart;
  }
  
  public void setL1prs(BigDecimal pL1prs) {
    if (pL1prs == null) {
      return;
    }
    l1prs = pL1prs.setScale(DECIMAL_L1PRS, RoundingMode.HALF_UP);
  }
  
  public void setL1prs(Double pL1prs) {
    if (pL1prs == null) {
      return;
    }
    l1prs = BigDecimal.valueOf(pL1prs).setScale(DECIMAL_L1PRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL1prs() {
    return l1prs.setScale(DECIMAL_L1PRS, RoundingMode.HALF_UP);
  }
  
  public void setWqtefac(BigDecimal pWqtefac) {
    if (pWqtefac == null) {
      return;
    }
    wqtefac = pWqtefac.setScale(DECIMAL_WQTEFAC, RoundingMode.HALF_UP);
  }
  
  public void setWqtefac(Double pWqtefac) {
    if (pWqtefac == null) {
      return;
    }
    wqtefac = BigDecimal.valueOf(pWqtefac).setScale(DECIMAL_WQTEFAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqtefac() {
    return wqtefac.setScale(DECIMAL_WQTEFAC, RoundingMode.HALF_UP);
  }
  
  public void setWtop(BigDecimal pWtop) {
    if (pWtop == null) {
      return;
    }
    wtop = pWtop.setScale(DECIMAL_WTOP, RoundingMode.HALF_UP);
  }
  
  public void setWtop(Integer pWtop) {
    if (pWtop == null) {
      return;
    }
    wtop = BigDecimal.valueOf(pWtop);
  }
  
  public Integer getWtop() {
    return wtop.intValue();
  }
  
  public void setWcof(BigDecimal pWcof) {
    if (pWcof == null) {
      return;
    }
    wcof = pWcof.setScale(DECIMAL_WCOF, RoundingMode.HALF_UP);
  }
  
  public void setWcof(Integer pWcof) {
    if (pWcof == null) {
      return;
    }
    wcof = BigDecimal.valueOf(pWcof);
  }
  
  public Integer getWcof() {
    return wcof.intValue();
  }
  
  public void setWfrs(BigDecimal pWfrs) {
    if (pWfrs == null) {
      return;
    }
    wfrs = pWfrs.setScale(DECIMAL_WFRS, RoundingMode.HALF_UP);
  }
  
  public void setWfrs(Integer pWfrs) {
    if (pWfrs == null) {
      return;
    }
    wfrs = BigDecimal.valueOf(pWfrs);
  }
  
  public Integer getWfrs() {
    return wfrs.intValue();
  }
  
  public void setWfre(BigDecimal pWfre) {
    if (pWfre == null) {
      return;
    }
    wfre = pWfre.setScale(DECIMAL_WFRE, RoundingMode.HALF_UP);
  }
  
  public void setWfre(Integer pWfre) {
    if (pWfre == null) {
      return;
    }
    wfre = BigDecimal.valueOf(pWfre);
  }
  
  public Integer getWfre() {
    return wfre.intValue();
  }
}
