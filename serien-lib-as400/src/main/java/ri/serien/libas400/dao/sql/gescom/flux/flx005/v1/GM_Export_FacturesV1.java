/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx005.v1;

import java.io.File;
import java.util.ArrayList;

import com.google.gson.JsonSyntaxException;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v1.JsonFactureMagentoV1;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.ConvertFileToBytes;

public class GM_Export_FacturesV1 extends BaseGroupDB {
  private ConvertFileToBytes convert = null;
  protected ParametresFluxBibli parametresBibli = null;
  
  public GM_Export_FacturesV1(QueryManager queryManager, ParametresFluxBibli pParam) {
    super(queryManager);
    parametresBibli = pParam;
  }
  
  /**
   * On retourne sous forme binaire une facture dans le JSON
   */
  public String retournerUneFactureJSON(FluxMagento pRecord) {
    if (pRecord == null || pRecord.getFLETB() == null || pRecord.getFLCOD() == null) {
      Trace.erreur("[GM_Export_Factures] retournerUneFactureJSON() record null ou corrompu");
      return null;
    }
    
    String retour = null;
    
    String numeroFacture = isolerNumeroFacture(pRecord, pRecord.getFLCOD().trim());
    if (numeroFacture == null) {
      return null;
    }
    
    // Contrôler s'il s'agit bien d'une facture existante SERIE N
    String requete = "SELECT E1ETB, E1NFA, E1CCT, E1AVR FROM " + queryManager.getLibrary() + ".PGVMEBCM WHERE  E1COD = 'E' AND E1ETB = '"
        + pRecord.getFLETB() + "' AND E1NFA = '" + numeroFacture + "' AND (E1ETA = 6 OR E1ETA = 7) ";
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    // Contrôler si cette facture existe bien dans le bon dossier
    if (liste != null && liste.size() == 1) {
      
      String chemin = recupererLeBonCheminFacture(pRecord);
      if (chemin != null) {
        String nomFacture = recupererLeBonNomFacturePDF(pRecord, pRecord.getFLETB(), numeroFacture);
        if (nomFacture != null) {
          chemin = chemin + nomFacture;
          
          // On checke qu'il s'agisse bien d'un Fichier valide
          File fichierPhysique = new File(chemin);
          if (!fichierPhysique.isFile()) {
            pRecord.construireMessageErreur("[retournerUneFactureJSON] le fichier: " + chemin + " n'existe pas ");
            return null;
          }
          
          if (convert == null) {
            convert = new ConvertFileToBytes();
          }
          try {
            
            retour = convert.encodeFileToBase64Binary(chemin);
            // TODO Il faudra un peu mieux contrôler tout ce bouzin
            if (retour != null && Constantes.normerTexte(retour).length() > 10) {
              // Si la conversion s'est bien déroulée, créer un nouvel objet FactureMagento pour le JSONiser
              JsonFactureMagentoV1 objetFacture = new JsonFactureMagentoV1(pRecord);
              Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), objetFacture.getEtb());
              if (numeroInstance == null) {
                majError("[GM_Export_Factures] retournerUneFactureJSON() : probleme d'interpretation de l'Etb vers l'instance Magento");
                return null;
              }
              objetFacture.setIdInstanceMagento(numeroInstance);
              
              objetFacture.setNomFacture(nomFacture);
              // TODO Il va falloir contrôler l'existence de cette référence Magento E1CCT avant même d'envoyer la facture
              if (liste.get(0).isPresentField("E1CCT")) {
                objetFacture.setCommandeMagento(Constantes.normerTexte(liste.get(0).getString("E1CCT")));
              }
              
              objetFacture.setFactureClient(retour);
              
              // debut de la construction du JSON
              if (initJSON()) {
                // retour = "";
                if (objetFacture.getNomFacture() != null && objetFacture.getIdInstanceMagento() > 0
                    && objetFacture.getCommandeMagento() != null) {
                  retour = gson.toJson(objetFacture);
                }
                else {
                  pRecord.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() valeurs ESSENTIELLES à NULL");
                }
              }
            }
            else {
              pRecord.construireMessageErreur("[GM_Export_Factures] PB convert: " + retour);
            }
          }
          catch (JsonSyntaxException e) {
            pRecord.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() PB PARSE Objet JSON: " + e.getMessage());
            return null;
          }
        }
      }
      else {
        pRecord.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() PB de recup du chemin de facture SERIE N : "
            + " / " + pRecord.getFLETB() + " / " + pRecord.getFLCOD());
      }
    }
    else {
      pRecord.construireMessageErreur("[GM_Export_Factures] retournerUneFactureJSON() PB de recup de facture SERIE N : " + " / "
          + queryManager.getMsgError() + " / " + requete);
    }
    
    return retour;
  }
  
  /**
   * On récupère le bon chemin pour accéder à la facture
   */
  protected String recupererLeBonCheminFacture(FluxMagento record) {
    String cheminFacture = File.separator + parametresBibli.getRacineIFSeditions() + File.separator + queryManager.getLibrary()
        + File.separator + parametresBibli.getCheminIFSfactures() + File.separator;
    
    return cheminFacture;
  }
  
  /**
   * Renvoyer un numero de facture sans l'établissement
   */
  protected String isolerNumeroFacture(FluxMagento pRecord, String factureAvecEtablissement) {
    if (factureAvecEtablissement == null) {
      pRecord.construireMessageErreur("[GM_Export_Factures] retaillerCodeFacture() factureAvecEtablissement à NULL ");
      return factureAvecEtablissement;
    }
    
    String retour = null;
    
    if (factureAvecEtablissement.length() > 3) {
      retour = factureAvecEtablissement.substring(3);
    }
    else {
      pRecord.construireMessageErreur("[GM_Export_Factures] retaillerCodeFacture() longueur erronée de factureAvecEtablissement:"
          + factureAvecEtablissement.length());
    }
    
    return retour;
  }
  
  /**
   * On récupère le bon nom de facture
   */
  protected String recupererLeBonNomFacturePDF(FluxMagento pRecord, String codeEtablissement, String numeroFacture) {
    if (numeroFacture == null || codeEtablissement == null) {
      pRecord.construireMessageErreur("[GM_Export_Factures] retaillerCodeFacture() numeroFacture ou codeEtablissement à NULL ");
      return null;
    }
    
    if (parametresBibli.getPrefixeNomFactures() != null) {
      return codeEtablissement + parametresBibli.getPrefixeNomFactures() + numeroFacture + ".pdf";
    }
    else {
      pRecord.construireMessageErreur("[GM_Export_Factures] isolerNumeroFacture() PREFIXE_NOM_FAC à NULL :");
      return null;
    }
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
}
