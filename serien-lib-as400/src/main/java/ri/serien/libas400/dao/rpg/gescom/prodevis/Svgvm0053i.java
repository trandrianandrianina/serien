/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.prodevis;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0053i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PIKEY = 10;
  public static final int DECIMAL_PIKEY = 0;
  public static final int SIZE_PISTA = 1;
  public static final int DECIMAL_PISTA = 0;
  public static final int SIZE_PICDC = 1;
  public static final int SIZE_PICDE = 3;
  public static final int SIZE_PICDN = 6;
  public static final int DECIMAL_PICDN = 0;
  public static final int SIZE_PICDS = 1;
  public static final int DECIMAL_PICDS = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 36;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PIKEY = 2;
  public static final int VAR_PISTA = 3;
  public static final int VAR_PICDC = 4;
  public static final int VAR_PICDE = 5;
  public static final int VAR_PICDN = 6;
  public static final int VAR_PICDS = 7;
  public static final int VAR_PIARR = 8;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private BigDecimal pikey = BigDecimal.ZERO; // Clé unique
  private BigDecimal pista = BigDecimal.ZERO; // Nouveau Statut
  private String picdc = ""; // N° de commande : Code erl
  private String picde = ""; // N° de commande : Etb
  private BigDecimal picdn = BigDecimal.ZERO; // N° de commande : N° Bon
  private BigDecimal picds = BigDecimal.ZERO; // N° de commande : Suffixe Bon
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PIKEY, DECIMAL_PIKEY), // Clé unique
      new AS400ZonedDecimal(SIZE_PISTA, DECIMAL_PISTA), // Nouveau Statut
      new AS400Text(SIZE_PICDC), // N° de commande : Code erl
      new AS400Text(SIZE_PICDE), // N° de commande : Etb
      new AS400ZonedDecimal(SIZE_PICDN, DECIMAL_PICDN), // N° de commande : N° Bon
      new AS400ZonedDecimal(SIZE_PICDS, DECIMAL_PICDS), // N° de commande : Suffixe Bon
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pietb, pikey, pista, picdc, picde, picdn, picds, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    pikey = (BigDecimal) output[2];
    pista = (BigDecimal) output[3];
    picdc = (String) output[4];
    picde = (String) output[5];
    picdn = (BigDecimal) output[6];
    picds = (BigDecimal) output[7];
    piarr = (String) output[8];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPikey(BigDecimal pPikey) {
    if (pPikey == null) {
      return;
    }
    pikey = pPikey.setScale(DECIMAL_PIKEY, RoundingMode.HALF_UP);
  }
  
  public void setPikey(Integer pPikey) {
    if (pPikey == null) {
      return;
    }
    pikey = BigDecimal.valueOf(pPikey);
  }
  
  public Integer getPikey() {
    return pikey.intValue();
  }
  
  public void setPista(BigDecimal pPista) {
    if (pPista == null) {
      return;
    }
    pista = pPista.setScale(DECIMAL_PISTA, RoundingMode.HALF_UP);
  }
  
  public void setPista(Integer pPista) {
    if (pPista == null) {
      return;
    }
    pista = BigDecimal.valueOf(pPista);
  }
  
  public Integer getPista() {
    return pista.intValue();
  }
  
  public void setPicdc(Character pPicdc) {
    if (pPicdc == null) {
      return;
    }
    picdc = String.valueOf(pPicdc);
  }
  
  public Character getPicdc() {
    return picdc.charAt(0);
  }
  
  public void setPicde(String pPicde) {
    if (pPicde == null) {
      return;
    }
    picde = pPicde;
  }
  
  public String getPicde() {
    return picde;
  }
  
  public void setPicdn(BigDecimal pPicdn) {
    if (pPicdn == null) {
      return;
    }
    picdn = pPicdn.setScale(DECIMAL_PICDN, RoundingMode.HALF_UP);
  }
  
  public void setPicdn(Integer pPicdn) {
    if (pPicdn == null) {
      return;
    }
    picdn = BigDecimal.valueOf(pPicdn);
  }
  
  public Integer getPicdn() {
    return picdn.intValue();
  }
  
  public void setPicds(BigDecimal pPicds) {
    if (pPicds == null) {
      return;
    }
    picds = pPicds.setScale(DECIMAL_PICDS, RoundingMode.HALF_UP);
  }
  
  public void setPicds(Integer pPicds) {
    if (pPicds == null) {
      return;
    }
    picds = BigDecimal.valueOf(pPicds);
  }
  
  public Integer getPicds() {
    return picds.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
