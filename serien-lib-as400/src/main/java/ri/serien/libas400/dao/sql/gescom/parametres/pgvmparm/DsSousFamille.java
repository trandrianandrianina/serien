/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_SF pour les SF
 */
public class DsSousFamille extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "SFETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "SFTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "SFCOD"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "SFLIB"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 2), "SFTPF"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "SFTYI"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "SFCG"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "SFTKSV"));
    
    length = 300;
  }
}
