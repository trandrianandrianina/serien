/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.prixflash;

import java.math.BigDecimal;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.vente.document.PrixFlash;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgChargerPrixFlash extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0037";
  
  /**
   * Appel du programme RPG qui va rechercher un prix flash pour un article donné.
   */
  public PrixFlash chargerPrixFlash(SystemeManager pSysteme, IdLigneVente pIdLigneVente, IdArticle pIdArticle, IdClient pIdClient) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    IdArticle.controlerId(pIdArticle, true);
    
    PrixFlash prixFlash = null;
    
    // Préparation des paramètres du programme RPG
    Svgvm0037i entree = new Svgvm0037i();
    Svgvm0037o sortie = new Svgvm0037o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdArticle.getCodeEtablissement());
    entree.setPiart(pIdArticle.getCodeArticle());
    entree.setPiclf(pIdClient.getNumero());
    entree.setPicod(pIdLigneVente.getCodeEntete().getCode());
    entree.setPinum(pIdLigneVente.getNumero());
    entree.setPisuf(pIdLigneVente.getSuffixe());
    entree.setPinli(pIdLigneVente.getNumeroLigne());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      if ((sortie.getPopvn()).compareTo(BigDecimal.ZERO) == 1) {
        // Initialisation de la classe métier
        prixFlash = new PrixFlash();
        
        // Entrée
        prixFlash.setIdArticle(pIdArticle);
        prixFlash.setIdClient(pIdClient);
        prixFlash.setPrixVenteFlash(sortie.getPopvn());
        prixFlash.setCodeUtilisateurFlash(sortie.getPousr().trim());
        prixFlash.setDatePrixFlash(ConvertDate.db2ToDate(sortie.getPodat(), sortie.getPoheu(), null));
        prixFlash.setHeurePrixFlash(sortie.getPoheu());
      }
    }
    
    return prixFlash;
  }
}
