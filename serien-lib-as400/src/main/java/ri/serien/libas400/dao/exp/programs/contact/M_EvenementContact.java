/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.programs.contact;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.serien.libas400.dao.exp.database.files.FFD_Psemevtm;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.outils.dateheure.TimeOperations;

/**
 * Classe contenant les zones du PSEMEVTM
 * @author Administrateur
 * 
 */
public class M_EvenementContact extends FFD_Psemevtm {
  // Constantes
  public static final byte TYPE_EVENEMENT = 0;
  public static final byte TYPE_ACTION_COMMERCIALE = 1;
  
  public static final int EVT_TO_TREAT = 0;
  public static final int EVT_IN_PROGRESS = 1;
  public static final int EVT_TREATED = 2;
  public static final int EVT_PROBLEM = 3;
  public static final LinkedHashMap<Integer, String> STATE_FR = new LinkedHashMap<Integer, String>() {
    
    {
      put(EVT_TO_TREAT, "A traiter");
    }
    
    {
      put(EVT_IN_PROGRESS, "En cours");
    }
    
    {
      put(EVT_TREATED, "Trait\u00e9");
    }
    
    {
      put(EVT_PROBLEM, "Probl\u00e8me");
    }
  };
  
  // public static final byte PRIORITY_NOP = 0;
  public static final byte PRIORITY_HIGH = 0;
  public static final byte PRIORITY_NORMAL = 1;
  public static final byte PRIORITY_LOW = 2;
  public static final LinkedHashMap<Byte, String> PRIORITY_FR = new LinkedHashMap<Byte, String>() {
    
    // {put(PRIORITY_NOP, "Aucune");}
    {
      put(PRIORITY_HIGH, "Haute");
    }
    
    {
      put(PRIORITY_NORMAL, "Normale");
    }
    
    {
      put(PRIORITY_LOW, "Basse");
    }
  };
  
  // Variables
  private TimeOperations ctps = new TimeOperations();
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public M_EvenementContact(QueryManager aquerymg) {
    super(aquerymg);
    omittedField = new String[] { "ETID" };
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Insère un évènement dans la table
   * @return
   * 
   */
  @Override
  public boolean insertInDatabase() {
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert("PSEMEVTM", querymg.getLibrary());
    int id = querymg.insertWhoReturnId(requete);
    if (id < 0) {
      return false;
    }
    setETID(id);
    return true;
  }
  
  /**
   * Lecture d'un évènement dans la table
   * @param querymg
   * @return
   * 
   *         public boolean readInDatabase(QueryManager querymg)
   *         {
   *         // Lecture dans le PSEMEVTM
   *         //initGenericRecord(grEvenementContact, true);
   *         initialization();
   *         String requete = "select * from " + querymg.getLibrary() + " where ";
   *         ArrayList = querymg.select(requete);
   *         if( id < 0 ) {
   *         return false;
   *         }
   *         setETID(id);
   *         return true;
   *         }
   */
  
  /**
   * Modifie un évènement dans la table
   * @return
   * 
   */
  @Override
  public boolean updateInDatabase() {
    initGenericRecord(genericrecord, false);
    String requete = genericrecord.createSQLRequestUpdate("PSEMEVTM", querymg.getLibrary(), "ETID = " + getETID());
    return request(requete);
  }
  
  /**
   * Suppression de l'enregistrement courant
   * @return
   * 
   */
  @Override
  public boolean deleteInDatabase() {
    String requete = "delete from " + querymg.getLibrary() + ".PSEMEVTM where ETID=" + getETID();
    return request(requete);
  }
  
  /**
   * Retourne le libellé de la priorité
   * @return
   */
  public String getLabelETCODP() {
    String label = PRIORITY_FR.get(getETCODP());
    if (label == null) {
      return "Non d\u00e9fini";
    }
    return label;
  }
  
  /**
   * Retourne le libellé de l'état
   * @return
   */
  public String getLabelETETA() {
    String label = STATE_FR.get((int) getETETA());
    if (label == null) {
      return "Non d\u00e9fini";
    }
    return label;
  }
  
  /**
   * Effectue une opération sur le temps passé
   * @param operation
   */
  public void setOperationETTOTP(String operation) {
    setETTOTP(ctps.operationOnTime(ETTOTP, operation));
  }
  
  /**
   * Retourne le temps passé la forme d'une chaine compréhensible
   * @return
   */
  public String getLabelETTOTP() {
    return ctps.convertMinute2String(ETTOTP);
  }
  
  public static String[] getListPriority() {
    int i = 0;
    String[] lst = new String[PRIORITY_FR.size()];
    for (Entry<Byte, String> entry : PRIORITY_FR.entrySet()) {
      lst[i++] = entry.getValue();
    }
    return lst;
  }
  
  public static String[] getListState() {
    int i = 0;
    String[] lst = new String[STATE_FR.size()];
    for (Entry<Integer, String> entry : STATE_FR.entrySet()) {
      lst[i++] = entry.getValue();
    }
    return lst;
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
}
