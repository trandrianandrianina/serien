/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.tarif;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgChargerTarifArticle extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0012";
  
  /**
   * Appel du programme RPG qui va rechercher les articles.
   */
  public TarifArticle chargerTarifArticle(SystemeManager pSysteme, IdArticle pIdArticle, String pCodeDevise, Date pDateTarif,
      boolean isTTC) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    
    TarifArticle tarif = null;
    
    // Préparation des paramètres du programme RPG
    Svgvm0012i entree = new Svgvm0012i();
    Svgvm0012o sortie = new Svgvm0012o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pIdArticle.getCodeEtablissement());
    entree.setPiart(pIdArticle.getCodeArticle());
    // entree.setPidev = aCodeDevise;
    entree.setPidat(ConvertDate.dateToDb2(pDateTarif));
    if (isTTC) {
      entree.setPittc('1');
    }
    else {
      entree.setPittc(' ');
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      tarif = new TarifArticle();
      tarif.setTTC(isTTC);
      
      // Entrée
      tarif.setDateTarif(sortie.getPodapConvertiEnDate()); // Date d'application
      tarif.setIdArticle(pIdArticle);
      
      // Sortie
      tarif.setCoefficientTarif01(sortie.getPok01());
      tarif.setCoefficientTarif02(sortie.getPok02());
      tarif.setCoefficientTarif03(sortie.getPok03());
      tarif.setCoefficientTarif04(sortie.getPok04());
      tarif.setCoefficientTarif05(sortie.getPok05());
      tarif.setCoefficientTarif06(sortie.getPok06());
      tarif.setCoefficientTarif07(sortie.getPok07());
      tarif.setCoefficientTarif08(sortie.getPok08());
      tarif.setCoefficientTarif09(sortie.getPok09());
      tarif.setCoefficientTarif10(sortie.getPok10());
      tarif.setPrixVente01(sortie.getPop01());
      tarif.setPrixVente02(sortie.getPop02());
      tarif.setPrixVente03(sortie.getPop03());
      tarif.setPrixVente04(sortie.getPop04());
      tarif.setPrixVente05(sortie.getPop05());
      tarif.setPrixVente06(sortie.getPop06());
      tarif.setPrixVente07(sortie.getPop07());
      tarif.setPrixVente08(sortie.getPop08());
      tarif.setPrixVente09(sortie.getPop09());
      tarif.setPrixVente10(sortie.getPop10());
      tarif.setCodeArrondi(sortie.getPoron());
      tarif.setRattachementCNV(sortie.getPocnv());
      tarif.setDateValidationTarif(sortie.getPodat1ConvertiEnDate()); // Date de validation du tarif
      tarif.setArrondiSurTTC(sortie.getPoin11());
      tarif.setCodeIN12(sortie.getPoin12());
      tarif.setInitialesValideurTarif(sortie.getPoiva());
    }
    
    return tarif;
  }
  
}
