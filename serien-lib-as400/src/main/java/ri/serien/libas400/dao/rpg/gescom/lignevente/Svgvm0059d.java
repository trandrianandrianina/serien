/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0059d {
  // Constantes
  public static final int SIZE_WL1COD = 1;
  public static final int SIZE_WL1ETB = 3;
  public static final int SIZE_WL1NUM = 6;
  public static final int DECIMAL_WL1NUM = 0;
  public static final int SIZE_WL1SUF = 1;
  public static final int DECIMAL_WL1SUF = 0;
  public static final int SIZE_WL1NLI = 4;
  public static final int DECIMAL_WL1NLI = 0;
  public static final int SIZE_WE1NFA = 7;
  public static final int DECIMAL_WE1NFA = 0;
  public static final int SIZE_WE1NAT = 1;
  public static final int SIZE_WL1ERL = 1;
  public static final int SIZE_WL1CEX = 1;
  public static final int SIZE_WL1ART = 20;
  public static final int SIZE_WL1QTE = 11;
  public static final int DECIMAL_WL1QTE = 3;
  public static final int SIZE_WL1UNV = 2;
  public static final int SIZE_WL1KSV = 7;
  public static final int DECIMAL_WL1KSV = 3;
  public static final int SIZE_WL1CND = 9;
  public static final int DECIMAL_WL1CND = 3;
  public static final int SIZE_WL1TAR = 2;
  public static final int DECIMAL_WL1TAR = 0;
  public static final int SIZE_WL1TT = 1;
  public static final int DECIMAL_WL1TT = 0;
  public static final int SIZE_WL1TB = 1;
  public static final int DECIMAL_WL1TB = 0;
  public static final int SIZE_WL1TN = 1;
  public static final int DECIMAL_WL1TN = 0;
  public static final int SIZE_WL1TR = 1;
  public static final int DECIMAL_WL1TR = 0;
  public static final int SIZE_WL1IN6 = 1;
  public static final int SIZE_WL1IN21 = 1;
  public static final int SIZE_WL1PVB = 9;
  public static final int DECIMAL_WL1PVB = 2;
  public static final int SIZE_WL1PVN = 9;
  public static final int DECIMAL_WL1PVN = 2;
  public static final int SIZE_WL1PVC = 9;
  public static final int DECIMAL_WL1PVC = 2;
  public static final int SIZE_WL1MHT = 9;
  public static final int DECIMAL_WL1MHT = 2;
  public static final int SIZE_WL1REM1 = 4;
  public static final int DECIMAL_WL1REM1 = 2;
  public static final int SIZE_WL1REM2 = 4;
  public static final int DECIMAL_WL1REM2 = 2;
  public static final int SIZE_WL1REM3 = 4;
  public static final int DECIMAL_WL1REM3 = 2;
  public static final int SIZE_WL1REM4 = 4;
  public static final int DECIMAL_WL1REM4 = 2;
  public static final int SIZE_WL1REM5 = 4;
  public static final int DECIMAL_WL1REM5 = 2;
  public static final int SIZE_WL1REM6 = 4;
  public static final int DECIMAL_WL1REM6 = 2;
  public static final int SIZE_WL1PRV = 9;
  public static final int DECIMAL_WL1PRV = 2;
  public static final int SIZE_WL1DCV = 1;
  public static final int DECIMAL_WL1DCV = 0;
  public static final int SIZE_WA1DCS = 1;
  public static final int DECIMAL_WA1DCS = 0;
  public static final int SIZE_WLIGLB = 120;
  public static final int SIZE_WA1UNL = 2;
  public static final int SIZE_WL2QT1 = 8;
  public static final int DECIMAL_WL2QT1 = 3;
  public static final int SIZE_WL2QT2 = 8;
  public static final int DECIMAL_WL2QT2 = 3;
  public static final int SIZE_WL2QT3 = 8;
  public static final int DECIMAL_WL2QT3 = 3;
  public static final int SIZE_WL2NBR = 6;
  public static final int DECIMAL_WL2NBR = 0;
  public static final int SIZE_WL12QT = 9;
  public static final int DECIMAL_WL12QT = 3;
  public static final int SIZE_WL12UC = 2;
  public static final int SIZE_WPT1HT = 9;
  public static final int DECIMAL_WPT1HT = 2;
  public static final int SIZE_WPT1TTC = 9;
  public static final int DECIMAL_WPT1TTC = 2;
  public static final int SIZE_WPBTTC = 9;
  public static final int DECIMAL_WPBTTC = 2;
  public static final int SIZE_WPNTTC = 9;
  public static final int DECIMAL_WPNTTC = 2;
  public static final int SIZE_WPVTTC = 9;
  public static final int DECIMAL_WPVTTC = 2;
  public static final int SIZE_WMTTTC = 9;
  public static final int DECIMAL_WMTTTC = 2;
  public static final int SIZE_WL1PRS = 9;
  public static final int DECIMAL_WL1PRS = 2;
  public static final int SIZE_WORIPR = 10;
  public static final int SIZE_WTAUTVA = 5;
  public static final int DECIMAL_WTAUTVA = 3;
  public static final int SIZE_WTYPART = 3;
  public static final int SIZE_TOTALE_DS = 397;
  public static final int SIZE_FILLER = 400 - 397;
  
  // Constantes indices Nom DS
  public static final int VAR_WL1COD = 0;
  public static final int VAR_WL1ETB = 1;
  public static final int VAR_WL1NUM = 2;
  public static final int VAR_WL1SUF = 3;
  public static final int VAR_WL1NLI = 4;
  public static final int VAR_WE1NFA = 5;
  public static final int VAR_WE1NAT = 6;
  public static final int VAR_WL1ERL = 7;
  public static final int VAR_WL1CEX = 8;
  public static final int VAR_WL1ART = 9;
  public static final int VAR_WL1QTE = 10;
  public static final int VAR_WL1UNV = 11;
  public static final int VAR_WL1KSV = 12;
  public static final int VAR_WL1CND = 13;
  public static final int VAR_WL1TAR = 14;
  public static final int VAR_WL1TT = 15;
  public static final int VAR_WL1TB = 16;
  public static final int VAR_WL1TN = 17;
  public static final int VAR_WL1TR = 18;
  public static final int VAR_WL1IN6 = 19;
  public static final int VAR_WL1IN21 = 20;
  public static final int VAR_WL1PVB = 21;
  public static final int VAR_WL1PVN = 22;
  public static final int VAR_WL1PVC = 23;
  public static final int VAR_WL1MHT = 24;
  public static final int VAR_WL1REM1 = 25;
  public static final int VAR_WL1REM2 = 26;
  public static final int VAR_WL1REM3 = 27;
  public static final int VAR_WL1REM4 = 28;
  public static final int VAR_WL1REM5 = 29;
  public static final int VAR_WL1REM6 = 30;
  public static final int VAR_WL1PRV = 31;
  public static final int VAR_WL1DCV = 32;
  public static final int VAR_WA1DCS = 33;
  public static final int VAR_WLIGLB = 34;
  public static final int VAR_WA1UNL = 35;
  public static final int VAR_WL2QT1 = 36;
  public static final int VAR_WL2QT2 = 37;
  public static final int VAR_WL2QT3 = 38;
  public static final int VAR_WL2NBR = 39;
  public static final int VAR_WL12QT = 40;
  public static final int VAR_WL12UC = 41;
  public static final int VAR_WPT1HT = 42;
  public static final int VAR_WPT1TTC = 43;
  public static final int VAR_WPBTTC = 44;
  public static final int VAR_WPNTTC = 45;
  public static final int VAR_WPVTTC = 46;
  public static final int VAR_WMTTTC = 47;
  public static final int VAR_WL1PRS = 48;
  public static final int VAR_WORIPR = 49;
  public static final int VAR_WTAUTVA = 50;
  public static final int VAR_WTYPART = 51;
  
  // Variables AS400
  private String wl1cod = ""; // Code ERL "E" vente
  private String wl1etb = ""; // Code Etablissement
  private BigDecimal wl1num = BigDecimal.ZERO; // Numéro de Bon vente
  private BigDecimal wl1suf = BigDecimal.ZERO; // Suffixe du Bon vente
  private BigDecimal wl1nli = BigDecimal.ZERO; // Numéro de Ligne vente
  private BigDecimal we1nfa = BigDecimal.ZERO; // Numéro de facture vente
  private String we1nat = ""; // Nature Bon ("T" = TTC)
  private String wl1erl = ""; // Code ERL "C"
  private String wl1cex = ""; // Code Extraction
  private String wl1art = ""; // Code article
  private BigDecimal wl1qte = BigDecimal.ZERO; // Quantité Commandée UV
  private String wl1unv = ""; // Code Unité de Vente
  private BigDecimal wl1ksv = BigDecimal.ZERO; // Nbre d'US pour 1 UV
  private BigDecimal wl1cnd = BigDecimal.ZERO; // Conditionnement
  private BigDecimal wl1tar = BigDecimal.ZERO; // Numéro colonne tarif
  private BigDecimal wl1tt = BigDecimal.ZERO; // Top colonne tarif saisie
  private BigDecimal wl1tb = BigDecimal.ZERO; // Top prix de base saisi
  private BigDecimal wl1tn = BigDecimal.ZERO; // Top prix net saisi
  private BigDecimal wl1tr = BigDecimal.ZERO; // Top remises saisies
  private String wl1in6 = ""; // Ind. PRV saisi sur ligne
  private String wl1in21 = ""; // Ind. regroupement ligne
  private BigDecimal wl1pvb = BigDecimal.ZERO; // Prix de Vente de Base HT
  private BigDecimal wl1pvn = BigDecimal.ZERO; // Prix de Vente Net HT
  private BigDecimal wl1pvc = BigDecimal.ZERO; // Prix de Vente Calculé HT
  private BigDecimal wl1mht = BigDecimal.ZERO; // Montant Hors Taxes HT
  private BigDecimal wl1rem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal wl1rem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal wl1rem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal wl1rem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal wl1rem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal wl1rem6 = BigDecimal.ZERO; // % Remise 6
  private BigDecimal wl1prv = BigDecimal.ZERO; // Prix de Revient
  private BigDecimal wl1dcv = BigDecimal.ZERO; // Top décimalisation pour UV
  private BigDecimal wa1dcs = BigDecimal.ZERO; // Top décimalisation pour US
  private String wliglb = ""; // Libellé
  private String wa1unl = ""; // Code Unité de conditionnemet
  private BigDecimal wl2qt1 = BigDecimal.ZERO; // Quantité 1
  private BigDecimal wl2qt2 = BigDecimal.ZERO; // Quantité 2
  private BigDecimal wl2qt3 = BigDecimal.ZERO; // Quantité 3
  private BigDecimal wl2nbr = BigDecimal.ZERO; // Nombre
  private BigDecimal wl12qt = BigDecimal.ZERO; // Quantité initiale
  private String wl12uc = ""; // Code unité initiale
  private BigDecimal wpt1ht = BigDecimal.ZERO; // Prix Tarif 1 H.T
  private BigDecimal wpt1ttc = BigDecimal.ZERO; // Prix Tarif 1 T.T.C
  private BigDecimal wpbttc = BigDecimal.ZERO; // Prix de base TTC
  private BigDecimal wpnttc = BigDecimal.ZERO; // Prix net TTC
  private BigDecimal wpvttc = BigDecimal.ZERO; // Prix de vente calculé TTC
  private BigDecimal wmtttc = BigDecimal.ZERO; // Montant TTC
  private BigDecimal wl1prs = BigDecimal.ZERO; // Prix de revient standard
  private String woripr = ""; // Origine prix
  private BigDecimal wtautva = BigDecimal.ZERO; // Taux de TVA
  private String wtypart = ""; // Type ligne
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WL1COD), // Code ERL "E" vente
      new AS400Text(SIZE_WL1ETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_WL1NUM, DECIMAL_WL1NUM), // Numéro de Bon vente
      new AS400ZonedDecimal(SIZE_WL1SUF, DECIMAL_WL1SUF), // Suffixe du Bon vente
      new AS400ZonedDecimal(SIZE_WL1NLI, DECIMAL_WL1NLI), // Numéro de Ligne vente
      new AS400ZonedDecimal(SIZE_WE1NFA, DECIMAL_WE1NFA), // Numéro de facture vente
      new AS400Text(SIZE_WE1NAT), // Nature Bon ("T" = TTC)
      new AS400Text(SIZE_WL1ERL), // Code ERL "C"
      new AS400Text(SIZE_WL1CEX), // Code Extraction
      new AS400Text(SIZE_WL1ART), // Code article
      new AS400PackedDecimal(SIZE_WL1QTE, DECIMAL_WL1QTE), // Quantité Commandée UV
      new AS400Text(SIZE_WL1UNV), // Code Unité de Vente
      new AS400PackedDecimal(SIZE_WL1KSV, DECIMAL_WL1KSV), // Nbre d'US pour 1 UV
      new AS400PackedDecimal(SIZE_WL1CND, DECIMAL_WL1CND), // Conditionnement
      new AS400ZonedDecimal(SIZE_WL1TAR, DECIMAL_WL1TAR), // Numéro colonne tarif
      new AS400ZonedDecimal(SIZE_WL1TT, DECIMAL_WL1TT), // Top colonne tarif saisie
      new AS400ZonedDecimal(SIZE_WL1TB, DECIMAL_WL1TB), // Top prix de base saisi
      new AS400ZonedDecimal(SIZE_WL1TN, DECIMAL_WL1TN), // Top prix net saisi
      new AS400ZonedDecimal(SIZE_WL1TR, DECIMAL_WL1TR), // Top remises saisies
      new AS400Text(SIZE_WL1IN6), // Ind. PRV saisi sur ligne
      new AS400Text(SIZE_WL1IN21), // Ind. regroupement ligne
      new AS400PackedDecimal(SIZE_WL1PVB, DECIMAL_WL1PVB), // Prix de Vente de Base HT
      new AS400PackedDecimal(SIZE_WL1PVN, DECIMAL_WL1PVN), // Prix de Vente Net HT
      new AS400PackedDecimal(SIZE_WL1PVC, DECIMAL_WL1PVC), // Prix de Vente Calculé HT
      new AS400PackedDecimal(SIZE_WL1MHT, DECIMAL_WL1MHT), // Montant Hors Taxes HT
      new AS400ZonedDecimal(SIZE_WL1REM1, DECIMAL_WL1REM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_WL1REM2, DECIMAL_WL1REM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_WL1REM3, DECIMAL_WL1REM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_WL1REM4, DECIMAL_WL1REM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_WL1REM5, DECIMAL_WL1REM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_WL1REM6, DECIMAL_WL1REM6), // % Remise 6
      new AS400PackedDecimal(SIZE_WL1PRV, DECIMAL_WL1PRV), // Prix de Revient
      new AS400ZonedDecimal(SIZE_WL1DCV, DECIMAL_WL1DCV), // Top décimalisation pour UV
      new AS400ZonedDecimal(SIZE_WA1DCS, DECIMAL_WA1DCS), // Top décimalisation pour US
      new AS400Text(SIZE_WLIGLB), // Libellé
      new AS400Text(SIZE_WA1UNL), // Code Unité de conditionnemet
      new AS400ZonedDecimal(SIZE_WL2QT1, DECIMAL_WL2QT1), // Quantité 1
      new AS400ZonedDecimal(SIZE_WL2QT2, DECIMAL_WL2QT2), // Quantité 2
      new AS400ZonedDecimal(SIZE_WL2QT3, DECIMAL_WL2QT3), // Quantité 3
      new AS400ZonedDecimal(SIZE_WL2NBR, DECIMAL_WL2NBR), // Nombre
      new AS400ZonedDecimal(SIZE_WL12QT, DECIMAL_WL12QT), // Quantité initiale
      new AS400Text(SIZE_WL12UC), // Code unité initiale
      new AS400ZonedDecimal(SIZE_WPT1HT, DECIMAL_WPT1HT), // Prix Tarif 1 H.T
      new AS400ZonedDecimal(SIZE_WPT1TTC, DECIMAL_WPT1TTC), // Prix Tarif 1 T.T.C
      new AS400ZonedDecimal(SIZE_WPBTTC, DECIMAL_WPBTTC), // Prix de base TTC
      new AS400ZonedDecimal(SIZE_WPNTTC, DECIMAL_WPNTTC), // Prix net TTC
      new AS400ZonedDecimal(SIZE_WPVTTC, DECIMAL_WPVTTC), // Prix de vente calculé TTC
      new AS400ZonedDecimal(SIZE_WMTTTC, DECIMAL_WMTTTC), // Montant TTC
      new AS400ZonedDecimal(SIZE_WL1PRS, DECIMAL_WL1PRS), // Prix de revient standard
      new AS400Text(SIZE_WORIPR), // Origine prix
      new AS400ZonedDecimal(SIZE_WTAUTVA, DECIMAL_WTAUTVA), // Taux de TVA
      new AS400Text(SIZE_WTYPART), // Type ligne
      new AS400Text(SIZE_FILLER), // Filler
  };
  private AS400Structure ds = new AS400Structure(structure);
  public Object[] o = { wl1cod, wl1etb, wl1num, wl1suf, wl1nli, we1nfa, we1nat, wl1erl, wl1cex, wl1art, wl1qte, wl1unv, wl1ksv, wl1cnd,
      wl1tar, wl1tt, wl1tb, wl1tn, wl1tr, wl1in6, wl1in21, wl1pvb, wl1pvn, wl1pvc, wl1mht, wl1rem1, wl1rem2, wl1rem3, wl1rem4, wl1rem5,
      wl1rem6, wl1prv, wl1dcv, wa1dcs, wliglb, wa1unl, wl2qt1, wl2qt2, wl2qt3, wl2nbr, wl12qt, wl12uc, wpt1ht, wpt1ttc, wpbttc, wpnttc,
      wpvttc, wmtttc, wl1prs, woripr, wtautva, wtypart, filler, };
  
  // -- Accesseurs
  
  public void setWl1cod(Character pWl1cod) {
    if (pWl1cod == null) {
      return;
    }
    wl1cod = String.valueOf(pWl1cod);
  }
  
  public Character getWl1cod() {
    return wl1cod.charAt(0);
  }
  
  public void setWl1etb(String pWl1etb) {
    if (pWl1etb == null) {
      return;
    }
    wl1etb = pWl1etb;
  }
  
  public String getWl1etb() {
    return wl1etb;
  }
  
  public void setWl1num(BigDecimal pWl1num) {
    if (pWl1num == null) {
      return;
    }
    wl1num = pWl1num.setScale(DECIMAL_WL1NUM, RoundingMode.HALF_UP);
  }
  
  public void setWl1num(Integer pWl1num) {
    if (pWl1num == null) {
      return;
    }
    wl1num = BigDecimal.valueOf(pWl1num);
  }
  
  public Integer getWl1num() {
    return wl1num.intValue();
  }
  
  public void setWl1suf(BigDecimal pWl1suf) {
    if (pWl1suf == null) {
      return;
    }
    wl1suf = pWl1suf.setScale(DECIMAL_WL1SUF, RoundingMode.HALF_UP);
  }
  
  public void setWl1suf(Integer pWl1suf) {
    if (pWl1suf == null) {
      return;
    }
    wl1suf = BigDecimal.valueOf(pWl1suf);
  }
  
  public Integer getWl1suf() {
    return wl1suf.intValue();
  }
  
  public void setWl1nli(BigDecimal pWl1nli) {
    if (pWl1nli == null) {
      return;
    }
    wl1nli = pWl1nli.setScale(DECIMAL_WL1NLI, RoundingMode.HALF_UP);
  }
  
  public void setWl1nli(Integer pWl1nli) {
    if (pWl1nli == null) {
      return;
    }
    wl1nli = BigDecimal.valueOf(pWl1nli);
  }
  
  public Integer getWl1nli() {
    return wl1nli.intValue();
  }
  
  public void setWe1nfa(BigDecimal pWe1nfa) {
    if (pWe1nfa == null) {
      return;
    }
    we1nfa = pWe1nfa.setScale(DECIMAL_WE1NFA, RoundingMode.HALF_UP);
  }
  
  public void setWe1nfa(Integer pWe1nfa) {
    if (pWe1nfa == null) {
      return;
    }
    we1nfa = BigDecimal.valueOf(pWe1nfa);
  }
  
  public void setWe1nfa(Date pWe1nfa) {
    if (pWe1nfa == null) {
      return;
    }
    we1nfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pWe1nfa));
  }
  
  public Integer getWe1nfa() {
    return we1nfa.intValue();
  }
  
  public Date getWe1nfaConvertiEnDate() {
    return ConvertDate.db2ToDate(we1nfa.intValue(), null);
  }
  
  public void setWe1nat(Character pWe1nat) {
    if (pWe1nat == null) {
      return;
    }
    we1nat = String.valueOf(pWe1nat);
  }
  
  public Character getWe1nat() {
    return we1nat.charAt(0);
  }
  
  public void setWl1erl(Character pWl1erl) {
    if (pWl1erl == null) {
      return;
    }
    wl1erl = String.valueOf(pWl1erl);
  }
  
  public Character getWl1erl() {
    return wl1erl.charAt(0);
  }
  
  public void setWl1cex(Character pWl1cex) {
    if (pWl1cex == null) {
      return;
    }
    wl1cex = String.valueOf(pWl1cex);
  }
  
  public Character getWl1cex() {
    return wl1cex.charAt(0);
  }
  
  public void setWl1art(String pWl1art) {
    if (pWl1art == null) {
      return;
    }
    wl1art = pWl1art;
  }
  
  public String getWl1art() {
    return wl1art;
  }
  
  public void setWl1qte(BigDecimal pWl1qte) {
    if (pWl1qte == null) {
      return;
    }
    wl1qte = pWl1qte.setScale(DECIMAL_WL1QTE, RoundingMode.HALF_UP);
  }
  
  public void setWl1qte(Double pWl1qte) {
    if (pWl1qte == null) {
      return;
    }
    wl1qte = BigDecimal.valueOf(pWl1qte).setScale(DECIMAL_WL1QTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1qte() {
    return wl1qte.setScale(DECIMAL_WL1QTE, RoundingMode.HALF_UP);
  }
  
  public void setWl1unv(String pWl1unv) {
    if (pWl1unv == null) {
      return;
    }
    wl1unv = pWl1unv;
  }
  
  public String getWl1unv() {
    return wl1unv;
  }
  
  public void setWl1ksv(BigDecimal pWl1ksv) {
    if (pWl1ksv == null) {
      return;
    }
    wl1ksv = pWl1ksv.setScale(DECIMAL_WL1KSV, RoundingMode.HALF_UP);
  }
  
  public void setWl1ksv(Double pWl1ksv) {
    if (pWl1ksv == null) {
      return;
    }
    wl1ksv = BigDecimal.valueOf(pWl1ksv).setScale(DECIMAL_WL1KSV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1ksv() {
    return wl1ksv.setScale(DECIMAL_WL1KSV, RoundingMode.HALF_UP);
  }
  
  public void setWl1cnd(BigDecimal pWl1cnd) {
    if (pWl1cnd == null) {
      return;
    }
    wl1cnd = pWl1cnd.setScale(DECIMAL_WL1CND, RoundingMode.HALF_UP);
  }
  
  public void setWl1cnd(Double pWl1cnd) {
    if (pWl1cnd == null) {
      return;
    }
    wl1cnd = BigDecimal.valueOf(pWl1cnd).setScale(DECIMAL_WL1CND, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1cnd() {
    return wl1cnd.setScale(DECIMAL_WL1CND, RoundingMode.HALF_UP);
  }
  
  public void setWl1tar(BigDecimal pWl1tar) {
    if (pWl1tar == null) {
      return;
    }
    wl1tar = pWl1tar.setScale(DECIMAL_WL1TAR, RoundingMode.HALF_UP);
  }
  
  public void setWl1tar(Integer pWl1tar) {
    if (pWl1tar == null) {
      return;
    }
    wl1tar = BigDecimal.valueOf(pWl1tar);
  }
  
  public Integer getWl1tar() {
    return wl1tar.intValue();
  }
  
  public void setWl1tt(BigDecimal pWl1tt) {
    if (pWl1tt == null) {
      return;
    }
    wl1tt = pWl1tt.setScale(DECIMAL_WL1TT, RoundingMode.HALF_UP);
  }
  
  public void setWl1tt(Integer pWl1tt) {
    if (pWl1tt == null) {
      return;
    }
    wl1tt = BigDecimal.valueOf(pWl1tt);
  }
  
  public Integer getWl1tt() {
    return wl1tt.intValue();
  }
  
  public void setWl1tb(BigDecimal pWl1tb) {
    if (pWl1tb == null) {
      return;
    }
    wl1tb = pWl1tb.setScale(DECIMAL_WL1TB, RoundingMode.HALF_UP);
  }
  
  public void setWl1tb(Integer pWl1tb) {
    if (pWl1tb == null) {
      return;
    }
    wl1tb = BigDecimal.valueOf(pWl1tb);
  }
  
  public Integer getWl1tb() {
    return wl1tb.intValue();
  }
  
  public void setWl1tn(BigDecimal pWl1tn) {
    if (pWl1tn == null) {
      return;
    }
    wl1tn = pWl1tn.setScale(DECIMAL_WL1TN, RoundingMode.HALF_UP);
  }
  
  public void setWl1tn(Integer pWl1tn) {
    if (pWl1tn == null) {
      return;
    }
    wl1tn = BigDecimal.valueOf(pWl1tn);
  }
  
  public Integer getWl1tn() {
    return wl1tn.intValue();
  }
  
  public void setWl1tr(BigDecimal pWl1tr) {
    if (pWl1tr == null) {
      return;
    }
    wl1tr = pWl1tr.setScale(DECIMAL_WL1TR, RoundingMode.HALF_UP);
  }
  
  public void setWl1tr(Integer pWl1tr) {
    if (pWl1tr == null) {
      return;
    }
    wl1tr = BigDecimal.valueOf(pWl1tr);
  }
  
  public Integer getWl1tr() {
    return wl1tr.intValue();
  }
  
  public void setWl1in6(Character pWl1in6) {
    if (pWl1in6 == null) {
      return;
    }
    wl1in6 = String.valueOf(pWl1in6);
  }
  
  public Character getWl1in6() {
    return wl1in6.charAt(0);
  }
  
  public void setWl1in21(Character pWl1in21) {
    if (pWl1in21 == null) {
      return;
    }
    wl1in21 = String.valueOf(pWl1in21);
  }
  
  public Character getWl1in21() {
    return wl1in21.charAt(0);
  }
  
  public void setWl1pvb(BigDecimal pWl1pvb) {
    if (pWl1pvb == null) {
      return;
    }
    wl1pvb = pWl1pvb.setScale(DECIMAL_WL1PVB, RoundingMode.HALF_UP);
  }
  
  public void setWl1pvb(Double pWl1pvb) {
    if (pWl1pvb == null) {
      return;
    }
    wl1pvb = BigDecimal.valueOf(pWl1pvb).setScale(DECIMAL_WL1PVB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1pvb() {
    return wl1pvb.setScale(DECIMAL_WL1PVB, RoundingMode.HALF_UP);
  }
  
  public void setWl1pvn(BigDecimal pWl1pvn) {
    if (pWl1pvn == null) {
      return;
    }
    wl1pvn = pWl1pvn.setScale(DECIMAL_WL1PVN, RoundingMode.HALF_UP);
  }
  
  public void setWl1pvn(Double pWl1pvn) {
    if (pWl1pvn == null) {
      return;
    }
    wl1pvn = BigDecimal.valueOf(pWl1pvn).setScale(DECIMAL_WL1PVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1pvn() {
    return wl1pvn.setScale(DECIMAL_WL1PVN, RoundingMode.HALF_UP);
  }
  
  public void setWl1pvc(BigDecimal pWl1pvc) {
    if (pWl1pvc == null) {
      return;
    }
    wl1pvc = pWl1pvc.setScale(DECIMAL_WL1PVC, RoundingMode.HALF_UP);
  }
  
  public void setWl1pvc(Double pWl1pvc) {
    if (pWl1pvc == null) {
      return;
    }
    wl1pvc = BigDecimal.valueOf(pWl1pvc).setScale(DECIMAL_WL1PVC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1pvc() {
    return wl1pvc.setScale(DECIMAL_WL1PVC, RoundingMode.HALF_UP);
  }
  
  public void setWl1mht(BigDecimal pWl1mht) {
    if (pWl1mht == null) {
      return;
    }
    wl1mht = pWl1mht.setScale(DECIMAL_WL1MHT, RoundingMode.HALF_UP);
  }
  
  public void setWl1mht(Double pWl1mht) {
    if (pWl1mht == null) {
      return;
    }
    wl1mht = BigDecimal.valueOf(pWl1mht).setScale(DECIMAL_WL1MHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1mht() {
    return wl1mht.setScale(DECIMAL_WL1MHT, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem1(BigDecimal pWl1rem1) {
    if (pWl1rem1 == null) {
      return;
    }
    wl1rem1 = pWl1rem1.setScale(DECIMAL_WL1REM1, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem1(Double pWl1rem1) {
    if (pWl1rem1 == null) {
      return;
    }
    wl1rem1 = BigDecimal.valueOf(pWl1rem1).setScale(DECIMAL_WL1REM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1rem1() {
    return wl1rem1.setScale(DECIMAL_WL1REM1, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem2(BigDecimal pWl1rem2) {
    if (pWl1rem2 == null) {
      return;
    }
    wl1rem2 = pWl1rem2.setScale(DECIMAL_WL1REM2, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem2(Double pWl1rem2) {
    if (pWl1rem2 == null) {
      return;
    }
    wl1rem2 = BigDecimal.valueOf(pWl1rem2).setScale(DECIMAL_WL1REM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1rem2() {
    return wl1rem2.setScale(DECIMAL_WL1REM2, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem3(BigDecimal pWl1rem3) {
    if (pWl1rem3 == null) {
      return;
    }
    wl1rem3 = pWl1rem3.setScale(DECIMAL_WL1REM3, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem3(Double pWl1rem3) {
    if (pWl1rem3 == null) {
      return;
    }
    wl1rem3 = BigDecimal.valueOf(pWl1rem3).setScale(DECIMAL_WL1REM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1rem3() {
    return wl1rem3.setScale(DECIMAL_WL1REM3, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem4(BigDecimal pWl1rem4) {
    if (pWl1rem4 == null) {
      return;
    }
    wl1rem4 = pWl1rem4.setScale(DECIMAL_WL1REM4, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem4(Double pWl1rem4) {
    if (pWl1rem4 == null) {
      return;
    }
    wl1rem4 = BigDecimal.valueOf(pWl1rem4).setScale(DECIMAL_WL1REM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1rem4() {
    return wl1rem4.setScale(DECIMAL_WL1REM4, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem5(BigDecimal pWl1rem5) {
    if (pWl1rem5 == null) {
      return;
    }
    wl1rem5 = pWl1rem5.setScale(DECIMAL_WL1REM5, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem5(Double pWl1rem5) {
    if (pWl1rem5 == null) {
      return;
    }
    wl1rem5 = BigDecimal.valueOf(pWl1rem5).setScale(DECIMAL_WL1REM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1rem5() {
    return wl1rem5.setScale(DECIMAL_WL1REM5, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem6(BigDecimal pWl1rem6) {
    if (pWl1rem6 == null) {
      return;
    }
    wl1rem6 = pWl1rem6.setScale(DECIMAL_WL1REM6, RoundingMode.HALF_UP);
  }
  
  public void setWl1rem6(Double pWl1rem6) {
    if (pWl1rem6 == null) {
      return;
    }
    wl1rem6 = BigDecimal.valueOf(pWl1rem6).setScale(DECIMAL_WL1REM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1rem6() {
    return wl1rem6.setScale(DECIMAL_WL1REM6, RoundingMode.HALF_UP);
  }
  
  public void setWl1prv(BigDecimal pWl1prv) {
    if (pWl1prv == null) {
      return;
    }
    wl1prv = pWl1prv.setScale(DECIMAL_WL1PRV, RoundingMode.HALF_UP);
  }
  
  public void setWl1prv(Double pWl1prv) {
    if (pWl1prv == null) {
      return;
    }
    wl1prv = BigDecimal.valueOf(pWl1prv).setScale(DECIMAL_WL1PRV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1prv() {
    return wl1prv.setScale(DECIMAL_WL1PRV, RoundingMode.HALF_UP);
  }
  
  public void setWl1dcv(BigDecimal pWl1dcv) {
    if (pWl1dcv == null) {
      return;
    }
    wl1dcv = pWl1dcv.setScale(DECIMAL_WL1DCV, RoundingMode.HALF_UP);
  }
  
  public void setWl1dcv(Integer pWl1dcv) {
    if (pWl1dcv == null) {
      return;
    }
    wl1dcv = BigDecimal.valueOf(pWl1dcv);
  }
  
  public Integer getWl1dcv() {
    return wl1dcv.intValue();
  }
  
  public void setWa1dcs(BigDecimal pWa1dcs) {
    if (pWa1dcs == null) {
      return;
    }
    wa1dcs = pWa1dcs.setScale(DECIMAL_WA1DCS, RoundingMode.HALF_UP);
  }
  
  public void setWa1dcs(Integer pWa1dcs) {
    if (pWa1dcs == null) {
      return;
    }
    wa1dcs = BigDecimal.valueOf(pWa1dcs);
  }
  
  public Integer getWa1dcs() {
    return wa1dcs.intValue();
  }
  
  public void setWliglb(String pWliglb) {
    if (pWliglb == null) {
      return;
    }
    wliglb = pWliglb;
  }
  
  public String getWliglb() {
    return wliglb;
  }
  
  public void setWa1unl(String pWa1unl) {
    if (pWa1unl == null) {
      return;
    }
    wa1unl = pWa1unl;
  }
  
  public String getWa1unl() {
    return wa1unl;
  }
  
  public void setWl2qt1(BigDecimal pWl2qt1) {
    if (pWl2qt1 == null) {
      return;
    }
    wl2qt1 = pWl2qt1.setScale(DECIMAL_WL2QT1, RoundingMode.HALF_UP);
  }
  
  public void setWl2qt1(Double pWl2qt1) {
    if (pWl2qt1 == null) {
      return;
    }
    wl2qt1 = BigDecimal.valueOf(pWl2qt1).setScale(DECIMAL_WL2QT1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl2qt1() {
    return wl2qt1.setScale(DECIMAL_WL2QT1, RoundingMode.HALF_UP);
  }
  
  public void setWl2qt2(BigDecimal pWl2qt2) {
    if (pWl2qt2 == null) {
      return;
    }
    wl2qt2 = pWl2qt2.setScale(DECIMAL_WL2QT2, RoundingMode.HALF_UP);
  }
  
  public void setWl2qt2(Double pWl2qt2) {
    if (pWl2qt2 == null) {
      return;
    }
    wl2qt2 = BigDecimal.valueOf(pWl2qt2).setScale(DECIMAL_WL2QT2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl2qt2() {
    return wl2qt2.setScale(DECIMAL_WL2QT2, RoundingMode.HALF_UP);
  }
  
  public void setWl2qt3(BigDecimal pWl2qt3) {
    if (pWl2qt3 == null) {
      return;
    }
    wl2qt3 = pWl2qt3.setScale(DECIMAL_WL2QT3, RoundingMode.HALF_UP);
  }
  
  public void setWl2qt3(Double pWl2qt3) {
    if (pWl2qt3 == null) {
      return;
    }
    wl2qt3 = BigDecimal.valueOf(pWl2qt3).setScale(DECIMAL_WL2QT3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl2qt3() {
    return wl2qt3.setScale(DECIMAL_WL2QT3, RoundingMode.HALF_UP);
  }
  
  public void setWl2nbr(BigDecimal pWl2nbr) {
    if (pWl2nbr == null) {
      return;
    }
    wl2nbr = pWl2nbr.setScale(DECIMAL_WL2NBR, RoundingMode.HALF_UP);
  }
  
  public void setWl2nbr(Integer pWl2nbr) {
    if (pWl2nbr == null) {
      return;
    }
    wl2nbr = BigDecimal.valueOf(pWl2nbr);
  }
  
  public Integer getWl2nbr() {
    return wl2nbr.intValue();
  }
  
  public void setWl12qt(BigDecimal pWl12qt) {
    if (pWl12qt == null) {
      return;
    }
    wl12qt = pWl12qt.setScale(DECIMAL_WL12QT, RoundingMode.HALF_UP);
  }
  
  public void setWl12qt(Double pWl12qt) {
    if (pWl12qt == null) {
      return;
    }
    wl12qt = BigDecimal.valueOf(pWl12qt).setScale(DECIMAL_WL12QT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl12qt() {
    return wl12qt.setScale(DECIMAL_WL12QT, RoundingMode.HALF_UP);
  }
  
  public void setWl12uc(String pWl12uc) {
    if (pWl12uc == null) {
      return;
    }
    wl12uc = pWl12uc;
  }
  
  public String getWl12uc() {
    return wl12uc;
  }
  
  public void setWpt1ht(BigDecimal pWpt1ht) {
    if (pWpt1ht == null) {
      return;
    }
    wpt1ht = pWpt1ht.setScale(DECIMAL_WPT1HT, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ht(Double pWpt1ht) {
    if (pWpt1ht == null) {
      return;
    }
    wpt1ht = BigDecimal.valueOf(pWpt1ht).setScale(DECIMAL_WPT1HT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpt1ht() {
    return wpt1ht.setScale(DECIMAL_WPT1HT, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ttc(BigDecimal pWpt1ttc) {
    if (pWpt1ttc == null) {
      return;
    }
    wpt1ttc = pWpt1ttc.setScale(DECIMAL_WPT1TTC, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ttc(Double pWpt1ttc) {
    if (pWpt1ttc == null) {
      return;
    }
    wpt1ttc = BigDecimal.valueOf(pWpt1ttc).setScale(DECIMAL_WPT1TTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpt1ttc() {
    return wpt1ttc.setScale(DECIMAL_WPT1TTC, RoundingMode.HALF_UP);
  }
  
  public void setWpbttc(BigDecimal pWpbttc) {
    if (pWpbttc == null) {
      return;
    }
    wpbttc = pWpbttc.setScale(DECIMAL_WPBTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpbttc(Double pWpbttc) {
    if (pWpbttc == null) {
      return;
    }
    wpbttc = BigDecimal.valueOf(pWpbttc).setScale(DECIMAL_WPBTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpbttc() {
    return wpbttc.setScale(DECIMAL_WPBTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpnttc(BigDecimal pWpnttc) {
    if (pWpnttc == null) {
      return;
    }
    wpnttc = pWpnttc.setScale(DECIMAL_WPNTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpnttc(Double pWpnttc) {
    if (pWpnttc == null) {
      return;
    }
    wpnttc = BigDecimal.valueOf(pWpnttc).setScale(DECIMAL_WPNTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpnttc() {
    return wpnttc.setScale(DECIMAL_WPNTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpvttc(BigDecimal pWpvttc) {
    if (pWpvttc == null) {
      return;
    }
    wpvttc = pWpvttc.setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpvttc(Double pWpvttc) {
    if (pWpvttc == null) {
      return;
    }
    wpvttc = BigDecimal.valueOf(pWpvttc).setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpvttc() {
    return wpvttc.setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public void setWmtttc(BigDecimal pWmtttc) {
    if (pWmtttc == null) {
      return;
    }
    wmtttc = pWmtttc.setScale(DECIMAL_WMTTTC, RoundingMode.HALF_UP);
  }
  
  public void setWmtttc(Double pWmtttc) {
    if (pWmtttc == null) {
      return;
    }
    wmtttc = BigDecimal.valueOf(pWmtttc).setScale(DECIMAL_WMTTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmtttc() {
    return wmtttc.setScale(DECIMAL_WMTTTC, RoundingMode.HALF_UP);
  }
  
  public void setWl1prs(BigDecimal pWl1prs) {
    if (pWl1prs == null) {
      return;
    }
    wl1prs = pWl1prs.setScale(DECIMAL_WL1PRS, RoundingMode.HALF_UP);
  }
  
  public void setWl1prs(Double pWl1prs) {
    if (pWl1prs == null) {
      return;
    }
    wl1prs = BigDecimal.valueOf(pWl1prs).setScale(DECIMAL_WL1PRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWl1prs() {
    return wl1prs.setScale(DECIMAL_WL1PRS, RoundingMode.HALF_UP);
  }
  
  public void setWoripr(String pWoripr) {
    if (pWoripr == null) {
      return;
    }
    woripr = pWoripr;
  }
  
  public String getWoripr() {
    return woripr;
  }
  
  public void setWtautva(BigDecimal pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = pWtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setWtautva(Double pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = BigDecimal.valueOf(pWtautva).setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWtautva() {
    return wtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setWtypart(String pWtypart) {
    if (pWtypart == null) {
      return;
    }
    wtypart = pWtypart;
  }
  
  public String getWtypart() {
    return wtypart;
  }
}
