/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_VD pour les VD
 */
public class Pgvmparm_ds_VD extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "VDLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(48), "VDP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VDZP1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(40), "VDO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VDCJO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "VDRG4"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "VDVDV"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "VDHOM"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "VDTEL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "VDCHQ"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(40), "VDMAI"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "VDPMR"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "VDMAV"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "VDMS1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "VDMS2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "VDMS3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "VDMS4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "VDMS5"));
    
    length = 300;
  }
}
