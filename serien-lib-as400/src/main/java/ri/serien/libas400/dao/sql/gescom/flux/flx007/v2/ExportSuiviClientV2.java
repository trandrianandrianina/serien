/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx007.v2;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.JsonSuiviClientMagentoV2;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

public class ExportSuiviClientV2 extends BaseGroupDB {
  
  /**
   * Constructeur du flux suivi client
   */
  
  public ExportSuiviClientV2(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * retourner l'encours d'un client
   *
   */
  public GenericRecord retournerEncoursPourUnClientDB2(String etb, String numCli) {
    
    if (etb == null || numCli == null) {
      majError("[GM_Export_SuiviClient] retournerEncoursPourUnClientDB2() -> CLCLI  corrompus " + numCli);
      return null;
    }
    
    String requete = "SELECT SUM(CLPCO+CLFAC+CLEXP+CLCDE) AS ENCOURS, " + "SUM(CLPCO+CLFAC+CLEXP+CLCDE-CLPLF) AS DEPASSEMENT " + "FROM "
        + queryManager.getLibrary() + ".PGVMCLIM WHERE CLETB='" + etb + "' AND AND CLCLIV=0 AND CLCLI='" + numCli + "'";
    
    ArrayList<GenericRecord> encoursetDepassPourUnClient = queryManager.select(requete);
    
    if (encoursetDepassPourUnClient != null && encoursetDepassPourUnClient.size() == 1) {
      return encoursetDepassPourUnClient.get(0);
    }
    else {
      majError("[GM_Export_SuiviClient] retournerEncoursPourUnClientDB2() --> Liste à NULLE ou vide : " + etb + "/" + numCli
          + "requete : " + requete + "--> " + queryManager.getMsgError());
      return null;
      
    }
  }
  
  /**
   * retourner encours pour les clients
   *
   */
  public ArrayList<GenericRecord> retournerEncoursDB2(String etb) {
    if (etb == null) {
      majError("[GM_Export_SuiviClient] retournerEncoursDB2() -> Etablissement  corrompus " + etb);
      return null;
    }
    
    String requete = "SELECT CLCLI, SUM(CLPCO+CLFAC+CLEXP+CLCDE) AS ENCOURS, " + "SUM(CLPCO+CLFAC+CLEXP+CLCDE-CLPLF) AS DEPASSEMENT "
        + "FROM " + queryManager.getLibrary() + ".PGVMCLIM WHERE CLETB='" + etb + "' AND CLLIV=0 GROUP BY CLCLI"
        + " HAVING SUM (CLPCO+clfac+clexp+clcde) <> 0 AND SUM(CLPCO+clfac+clexp+clcde-clplf)<>0";
    
    ArrayList<GenericRecord> encoursetDepassPourTousLesClients = queryManager.select(requete);
    if (encoursetDepassPourTousLesClients != null && encoursetDepassPourTousLesClients.size() > 0) {
      return encoursetDepassPourTousLesClients;
    }
    else {
      majError("[GM_Export_SuiviClient] retournerEncoursDB2() --> Liste à NULLE ou vide : " + etb + " / requete : " + requete + "--> "
          + queryManager.getMsgError());
      return null;
    }
  }
  
  /**
   * retourner encours pour les clients
   *
   */
  public String retournerEnCoursClientsJSON(FluxMagento record) {
    if (record == null || record.getFLETB() == null) {
      return null;
    }
    String retour = null;
    
    ArrayList<GenericRecord> encoursTousLesClientsDB2 = retournerEncoursDB2(record.getFLETB());
    JsonSuiviClientMagentoV2 suiviClientMag = null;
    if (encoursTousLesClientsDB2 != null) {
      // debut de la construction du JSON
      if (initJSON()) {
        
        try {
          retour = gson.toJson(suiviClientMag);
        }
        catch (Exception e) {
          majError("[GM_Export_SuiviClient] retournerEncoursPourUnClientJSON() -> ERREUR JSON: " + e.getMessage());
          retour = null;
        }
      }
      else {
        majError("[GM_Export_SuiviClient] retournerEncoursPourUnClientJSON() -> initJSON en erreur");
      }
    }
    
    return retour;
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
}
