/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.blocnotes.EnumTypeTiersBlocNote;
import ri.serien.libcommun.gescom.commun.blocnotes.IdBlocNote;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Cette classe permet de charger ou de sauvegarder un bloc Notes en base de données
 */
public class SqlBlocNote {
  // Constantes
  public static final String[][] EQUIVALENCE_JAVA_DB_LIGNE = { { "OBTOP", "OBTOP" }, { "codeLignes", "OBCOD" },
      { "codeEtablissement", "OBETB" }, { "indicatifComplet", "OBIND" }, { "numeroLigne", "OBSUF" }, { "texte", "OBZON" } };
  
  // Variables
  protected QueryManager querymg = null;
  private String fichierLignes = "";
  
  /**
   * Constructeur.
   */
  public SqlBlocNote(QueryManager pQuerymg) {
    if (pQuerymg == null) {
      throw new MessageErreurException("Le paramètre pQuerymg est à null.");
    }
    querymg = pQuerymg;
    fichierLignes = querymg.getLibrary() + '.' + EnumTableBDD.BLOCNOTES;
  }
  
  // -- Méthodes publiques
  
  /**
   * Vérifie l'existence d'un bloc-notes donné.
   */
  public boolean isBlocNoteExiste(IdBlocNote pIdBlocnotes) {
    IdBlocNote.controlerId(pIdBlocnotes, false);
    
    // Préparation de la requête
    String requete = "select OBIND from " + fichierLignes + " where" + " OBCOD = "
        + RequeteSql.formaterCharacterSQL(pIdBlocnotes.getCodeTypeTiers().getCode()) + " and OBETB = "
        + RequeteSql.formaterStringSQL(pIdBlocnotes.getCodeEtablissement()) + " and OBIND = "
        + RequeteSql.formaterStringSQL(pIdBlocnotes.getIndicatif());
    // Lancement de la requête
    String champ = querymg.firstEnrgSelect(requete, 1);
    return champ != null;
  }
  
  /**
   * Lecture d'un bloc-notes.
   */
  public BlocNote chargerBlocNote(IdBlocNote pIdBlocNote) {
    IdBlocNote.controlerId(pIdBlocNote, true);
    
    // Préparation de la requête
    String requete = "select * from " + fichierLignes + " where" + " OBCOD = "
        + RequeteSql.formaterCharacterSQL(pIdBlocNote.getCodeTypeTiers().getCode()) + " and OBETB = "
        + RequeteSql.formaterStringSQL(pIdBlocNote.getCodeEtablissement()) + " and OBIND = "
        + RequeteSql.formaterStringSQL(pIdBlocNote.getIndicatif()) + " AND OBSUF < 7 order by OBSUF";
    
    // Lancement de la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // Lecture des champs
    BlocNote blocNote = new BlocNote(pIdBlocNote);
    initialiserBlocNote(listeRecord, blocNote);
    return blocNote;
  }
  
  /**
   * Créé un bloc-notes.
   */
  public void creerBlocNote(BlocNote pBlocNote) {
    BlocNote.controlerId(pBlocNote, false);
    
    // Insertion des lignes du bloc-notes
    insererLigneBlocNote(pBlocNote);
  }
  
  /**
   * Met à jour un bloc-notes.
   */
  public void modifierBlocNote(BlocNote pBlocNote) {
    BlocNote.controlerId(pBlocNote, true);
    
    // Suppression de toutes les lignes existantes
    supprimerBlocNote(pBlocNote.getId());
    
    // Insertion des lignes du bloc-notes
    insererLigneBlocNote(pBlocNote);
  }
  
  /**
   * Ecrit un bloc-notes (le créé si besoin ou le met à jour ou le supprime si vide)
   */
  public void sauverBlocNote(BlocNote pBlocNote) {
    BlocNote.controlerId(pBlocNote, false);
    
    // Si le bloc-notes est vide (et qu'il existe) alors on le supprime
    if (pBlocNote.isVide()) {
      supprimerBlocNote(pBlocNote.getId());
      return;
    }
    
    if (isBlocNoteExiste(pBlocNote.getId())) {
      modifierBlocNote(pBlocNote);
    }
    else {
      creerBlocNote(pBlocNote);
    }
  }
  
  /**
   * Supprime un bloc-notes.
   */
  public void supprimerBlocNote(IdBlocNote pIdBlocNote) {
    IdBlocNote.controlerId(pIdBlocNote, true);
    
    String requete = "delete from " + fichierLignes + " where " + " OBCOD = "
        + RequeteSql.formaterCharacterSQL(pIdBlocNote.getCodeTypeTiers().getCode()) + " and OBETB = "
        + RequeteSql.formaterStringSQL(pIdBlocNote.getCodeEtablissement()) + " and OBIND = "
        + RequeteSql.formaterStringSQL(pIdBlocNote.getIndicatif());
    // Lancement de la requête
    querymg.requete(requete);
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise la classe Blocnotes à partir du generic record.
   */
  private void initialiserBlocNote(ArrayList<GenericRecord> pListeRecord, BlocNote pBlocNote) {
    // Initialise les champs
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pListeRecord.get(0).getStringValue("OBETB", "", false));
    IdBlocNote idBlocnotes =
        IdBlocNote.getInstance(idEtablissement, EnumTypeTiersBlocNote.valueOfByCode(pListeRecord.get(0).getCharacterValue("OBCOD")),
            pListeRecord.get(0).getStringValue("OBIND", "", false));
    pBlocNote.setId(idBlocnotes);
    
    // On traite les lignes constituants le texte du bloc-notes (attention 2 lignes de bloc-notes par ligne fichier)
    StringBuilder lignes = new StringBuilder(BlocNote.LONGUEUR_TEXTE_MAX);
    for (GenericRecord record : pListeRecord) {
      String ligne = record.getStringValue("OBZON", "", false);
      lignes.append(ligne.substring(0, BlocNote.LONGUEUR_LIGNE_AFFICHEE).trim()).append('\n');
      lignes.append(ligne.substring(BlocNote.LONGUEUR_LIGNE_AFFICHEE).trim()).append('\n');
    }
    // Suppression des espaces à la fin du texte (ils ont été ajouté à cause de la logique actuelle)
    pBlocNote.setTexte(lignes);
  }
  
  /**
   * Insère les lignes du bloc-notes dans la base de données.
   */
  private void insererLigneBlocNote(BlocNote pBlocNote) {
    // Préparation du texte du bloc-notes pour son enregistrement
    String[] lignes = pBlocNote.preparerEnregistrementTexte();
    
    int suffixe = 1;
    for (String ligne : lignes) {
      // Préparation de la requête pour les lignes
      String requete = "insert into " + fichierLignes + " (OBCOD, OBETB, OBIND, OBSUF, OBZON) values " + "( "
          + RequeteSql.formaterCharacterSQL(pBlocNote.getId().getCodeTypeTiers().getCode()) + ", "
          + RequeteSql.formaterStringSQL(pBlocNote.getId().getCodeEtablissement()) + ", "
          + RequeteSql.formaterStringSQL(pBlocNote.getId().getIndicatif()) + ", " + suffixe + ", " + RequeteSql.formaterStringSQL(ligne)
          + ")";
      // Lancement de la requête
      querymg.requete(requete);
      suffixe++;
    }
  }
}
