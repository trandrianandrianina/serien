/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.stocks;

import java.math.BigDecimal;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.stock.IdStock;
import ri.serien.libcommun.gescom.commun.stockcomptoir.CritereStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.IdStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.StockComptoir;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Programme RPG pour lire les stocks des articles.
 */
public class RpgStock extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0008";
  
  /**
   * Charger le stock suivant les critères de recherche.
   * Utiliser le programme RPG qui va lire les lignes de stock.
   */
  public ListeStockComptoir chargerListeStock(SystemeManager pSystemManager, CritereStockComptoir pCritereStock) {
    // Vérifier les valeurs en entrée
    if (pSystemManager == null) {
      throw new MessageErreurException("Impossible de charger les stocks car le système est invalide.");
    }
    if (pCritereStock == null) {
      throw new MessageErreurException("Impossible de charger les stocks car les critères de recherche sont invalides.");
    }
    if (pCritereStock.getCodeArticle() == null || pCritereStock.getCodeArticle().isEmpty()) {
      throw new MessageErreurException("Impossible de charger les stocks car l'article est invalide.");
    }
    
    // Initialiser le résultat
    ListeStockComptoir listeStock = new ListeStockComptoir();
    
    // Lire le résultat pagae par page à partir du programme RPG
    boolean existeDautresLignes = true;
    int page = 1;
    while (existeDautresLignes) {
      existeDautresLignes = majListeStockParPage(pSystemManager, listeStock, pCritereStock, page);
      page++;
    }
    
    // Retourner le résultat
    return listeStock;
  }
  
  /**
   * Charger le stock suivant une liste d'identifiants de stock.
   * Utiliser le programme RPG qui va lire les lignes de stock.
   */
  public ListeStockComptoir chargerListeStock(SystemeManager pSystemManager, List<IdStock> pListeId) {
    // Vérifier les valeurs en entrée
    if (pSystemManager == null) {
      throw new MessageErreurException("Impossible de charger les stocks car le système est invalide.");
    }
    if (pListeId == null) {
      throw new MessageErreurException("Impossible de charger les stocks car la liste des identifiants est invalide.");
    }
    
    // Initialiser le résultat
    ListeStockComptoir listeStock = new ListeStockComptoir();
    
    // Lire le résultat pagae par page à partir du programme RPG
    boolean existeDautresLignes = true;
    int page = 1;
    while (existeDautresLignes) {
      CritereStockComptoir critereStock = new CritereStockComptoir();
      critereStock.setIdEtablissement(pListeId.get(page).getIdEtablissement());
      critereStock.setCodeArticle(pListeId.get(page).getIdArticle().getCodeArticle());
      critereStock.setIdMagasin(pListeId.get(page).getIdMagasin());
      existeDautresLignes = majListeStockParPage(pSystemManager, listeStock, critereStock, page);
      page++;
    }
    
    // Retourner le résultat
    return listeStock;
  }
  
  /**
   * Compléter la liste des stocks en fonction du numéro de page.
   */
  private boolean majListeStockParPage(SystemeManager pSystemManager, ListeStockComptoir pListeStock, CritereStockComptoir pCritereStock,
      int pPage) {
    boolean existeDautresLignes = true;
    
    // Préparation des paramètres du programme RPG
    Svgvx0008i entree = new Svgvx0008i();
    Svgvx0008o sortie = new Svgvx0008o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    if (pCritereStock.getIdEtablissement() != null) {
      entree.setPietb(pCritereStock.getIdEtablissement().getCodeEtablissement());
    }
    if (pCritereStock.getIdMagasin() != null) {
      if (pCritereStock.getIdEtablissement() == null) {
        throw new MessageErreurException(
            "Impossible de charger les stocks car l'établissement est manquant pour la recherche avec magasin.");
      }
      if (!Constantes.equals(pCritereStock.getIdMagasin().getCodeEtablissement(),
          pCritereStock.getIdEtablissement().getCodeEtablissement())) {
        throw new MessageErreurException("Impossible de charger les stocks car les établissements transmis sont différents.");
      }
      entree.setPimag(pCritereStock.getIdMagasin().getCode());
    }
    entree.setPiart(pCritereStock.getCodeArticle());
    entree.setPipag(pPage);
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSystemManager);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList);
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Initialisation de la classe métier
    Object[] lignes = sortie.getValeursBrutesLignes();
    for (int i = 0; i < lignes.length; i++) {
      Object[] tabObjets = (Object[]) lignes[i];
      StockComptoir stock = traiterUneLigne(tabObjets);
      if (stock == null) {
        existeDautresLignes = false;
        return existeDautresLignes;
      }
      else {
        pListeStock.add(stock);
      }
    }
    
    return existeDautresLignes;
  }
  
  /**
   * Découpage d'une ligne
   * @param ligne
   * @return
   */
  private StockComptoir traiterUneLigne(Object[] ligne) {
    if (ligne == null) {
      return null;
    }
    
    // Créer l'identifiant du stock pour l'établissement et le magasin concerné
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) ligne[Svgvx0008d.VAR_WETB]);
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, (String) ligne[Svgvx0008d.VAR_WMAG]);
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, (String) ligne[Svgvx0008d.VAR_WART]);
    IdStockComptoir idStock = IdStockComptoir.getInstance(idArticle, idMagasin);
    
    // Créer le stock
    StockComptoir stock = new StockComptoir(idStock);
    stock.setNombreDecimale(((BigDecimal) ligne[Svgvx0008d.VAR_WDCS]));
    stock.setQuantitePhysique(((BigDecimal) ligne[Svgvx0008d.VAR_WSTK]));
    stock.setQuantiteCommande(((BigDecimal) ligne[Svgvx0008d.VAR_WRES]));
    stock.setQuantiteAttendue(((BigDecimal) ligne[Svgvx0008d.VAR_WATT]));
    stock.setQuantiteAffecte(((BigDecimal) ligne[Svgvx0008d.VAR_WAFF]));
    stock.setQuantiteDisponibleVente(((BigDecimal) ligne[Svgvx0008d.VAR_WDIV]));
    stock.setQuantiteDisponibleAchat(((BigDecimal) ligne[Svgvx0008d.VAR_WDIA]));
    stock.setQuantiteMinimum(((BigDecimal) ligne[Svgvx0008d.VAR_WMINI]));
    stock.setQuantiteIdeale(((BigDecimal) ligne[Svgvx0008d.VAR_WIDEAL]));
    stock.setQuantiteConsoMoyenne(((BigDecimal) ligne[Svgvx0008d.VAR_WCONSM]));
    stock.setQuantiteMax(((BigDecimal) ligne[Svgvx0008d.VAR_WMAX]));
    return stock;
  }
}
