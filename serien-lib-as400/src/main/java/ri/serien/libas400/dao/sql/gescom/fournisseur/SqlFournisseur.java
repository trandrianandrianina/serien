/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.fournisseur;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.exploitation.SqlContact;
import ri.serien.libas400.dao.sql.gescom.SqlBlocNote;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libcommun.commun.recherche.ArgumentRecherche;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.achat.catalogue.ChampCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.CriteresRechercheCfgCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.EnumTypeZoneERP;
import ri.serien.libcommun.gescom.achat.catalogue.IdChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.IdConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ListeConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.document.EnumOptionEdition;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.blocnotes.IdBlocNote;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.EnumTypeAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.EnumTypePrixAchat;
import ri.serien.libcommun.gescom.commun.fournisseur.FiltreFournisseurBase;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.FournisseurBase;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseurBase;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlFournisseur {
  protected QueryManager querymg = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur avec le gestionnaire de requêtes.
   */
  public SqlFournisseur(QueryManager pQuerymg) {
    querymg = pQuerymg;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Lecture d'un fournisseur donné.
   * @param pIdFournisseur Identifiant du fournisseur à lire.
   * @return Fournisseur.
   */
  public Fournisseur chargerFournisseur(IdFournisseur pIdFournisseur) {
    IdFournisseur.controlerId(pIdFournisseur, true);
    Fournisseur Fournisseur = new Fournisseur(pIdFournisseur);
    return chargerFournisseur(Fournisseur);
  }
  
  /**
   * Lecture d'un fournisseur donné.
   * @param pFournisseur Fournisseur à lire.
   * @return Fournisseur.
   */
  public Fournisseur chargerFournisseur(Fournisseur pFournisseur) {
    IdFournisseur idFournisseur = Fournisseur.controlerId(pFournisseur, true);
    
    String codeEtablissement = idFournisseur.getCodeEtablissement();
    int collectif = idFournisseur.getCollectif();
    int codeFournisseur = idFournisseur.getNumero();
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select * from " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " where");
    requeteSql.ajouterConditionAnd("FRETB", "=", codeEtablissement);
    requeteSql.ajouterConditionAnd("FRCOL", "=", collectif);
    requeteSql.ajouterConditionAnd("FRFRS", "=", codeFournisseur);
    ArrayList<GenericRecord> listeRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      throw new MessageErreurException(
          "Aucun Fournisseur trouvé avec la clé suivante FRETB:" + codeEtablissement + " FRFRS:" + codeFournisseur);
    }
    else if (listeRecord.size() > 1) {
      throw new MessageErreurException(listeRecord.size() + " Fournisseurs ont été trouvé avec la clé suivante FRETB:" + codeEtablissement
          + " FRFRS:" + codeFournisseur);
    }
    
    // Lecture des champs
    pFournisseur = completerFournisseur(listeRecord.get(0));
    
    // Récupération de la liste des adresses du fournisseur
    List<AdresseFournisseur> liste = lireListeAdresseFournisseur(idFournisseur, false);
    if (liste != null && !liste.isEmpty()) {
      if (pFournisseur.getListeAdresseFournisseur() == null) {
        pFournisseur.setListeAdresse(liste);
      }
      else {
        pFournisseur.getListeAdresseFournisseur().addAll(liste);
      }
    }
    
    // Récupération de la liste des contacts du fournisseur
    SqlContact sqlContact = new SqlContact(querymg);
    pFournisseur.setListeContact(sqlContact.chargerListeContactFournisseur(idFournisseur));
    
    // Récupération du blocnote
    SqlBlocNote sqlBlocNote = new SqlBlocNote(querymg);
    IdBlocNote idBlocNote = IdBlocNote.getInstancePourFournisseur(idFournisseur);
    pFournisseur.setBlocNote(sqlBlocNote.chargerBlocNote(idBlocNote));
    return pFournisseur;
  }
  
  /**
   * Retourner la liste des fournisseurs correspondant aux critères de recherche.<br>
   * Une erreur lors du chargement d'un fournisseur n'empêche pas le chargement des autres fournisseurs de la liste. Le fournisseur
   * fautif est ignoré avec un warning dans les traces.
   * @param pCritereFournisseur Critères de recherche des fournisseurs.
   * @return Liste des fournisseurs correspondant aux critères de recherche.
   */
  public ListeFournisseur chargerListeFournisseur(CritereFournisseur pCritereFournisseur) {
    // Construire la requête SQL
    String requete = RequetesRechercheFournisseur.getRequete(querymg.getLibrary(), pCritereFournisseur);
    
    // Excécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if (listeRecord == null || listeRecord.size() == 0) {
      return null;
    }
    
    // Générer le résultat de la recherche
    ListeFournisseur listeFournisseur = new ListeFournisseur();
    for (GenericRecord record : listeRecord) {
      // Une erreur lors du chargement d'un fournisseur n'empêche pas le chargement des autres fournisseurs de la liste.
      try {
        Fournisseur frs = completerFournisseur(record);
        listeFournisseur.add(frs);
      }
      catch (Exception e) {
        Trace.alerte("Un fournisseur invalide est ignoré lors du chargement de la liste des fournisseurs : FRETB="
            + record.getField("FRETB").toString() + " FRCOL=" + record.getField("FRCOL").toString() + " FRFRS="
            + record.getField("FRFRS").toString());
      }
    }
    return listeFournisseur;
  }
  
  /**
   * Rechercher une liste d'identifiants fournisseur suivant les critères passés en paramètre.<br>
   * Une erreur lors du chargement d'un fournisseur n'empêche pas le chargement des autres fournisseurs de la liste. Le fournisseur
   * fautif est ignoré avec un warning dans les traces.
   * @param pCritereFournisseur Critères de recherche des fournisseurs.
   * @return Liste des identifiants de fournisseurs correspondants à la recherche.
   */
  public List<IdFournisseur> chargerListeIdFournisseur(CritereFournisseur pCritereFournisseur) {
    // Contrôler les paramètres
    if (pCritereFournisseur == null || pCritereFournisseur.getIdEtablissement() == null) {
      throw new MessageErreurException("Les critères de recherche des fournisseurs sont invalides.");
    }
    
    // Générer la requête SQL
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select FRCOL,FRFRS,FRETB from " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " where");
    if (pCritereFournisseur.getIdEtablissement() != null) {
      requeteSql.ajouterConditionAnd("FRETB", "=", pCritereFournisseur.getIdEtablissement().getCodeEtablissement());
    }
    requeteSql.ajouterConditionAnd("FRTOP", "<>", 9);
    
    // Définir les filtres pour les composants liés.
    if (pCritereFournisseur.getIdFournisseurDebut() != null) {
      requeteSql.ajouterConditionAnd("FRFRS", ">=", pCritereFournisseur.getIdFournisseurDebut().getNumero());
    }
    if (pCritereFournisseur.getIdFournisseurFin() != null) {
      requeteSql.ajouterConditionAnd("FRFRS", "<=", pCritereFournisseur.getIdFournisseurFin().getNumero());
    }
    
    // Ajouter le filtrage suivant le mot rentré par l'utilisateur
    if (pCritereFournisseur.getTexteRechercheGenerique() != null) {
      for (ArgumentRecherche argumentRecherche : pCritereFournisseur.getRechercheGenerique().getListeArgumentRecherche()) {
        switch (argumentRecherche.getType()) {
          case IGNORE:
          case EMAIL:
          case TEXTE:
            String argumentDebut = RequeteSql.formaterStringSQL(argumentRecherche.getValeur().toUpperCase() + "%");
            String argumentDebutMot = RequeteSql.formaterStringSQL("% " + argumentRecherche.getValeur().toUpperCase() + "%");
            String argumentComplet = RequeteSql.formaterStringSQL(argumentRecherche.getValeur().toUpperCase() + "%");
            requeteSql.ajouter(" and (");
            requeteSql.ajouterConditionOr("upper(FRNOM)", "like", argumentDebut, true, true);
            requeteSql.ajouterConditionOr("upper(FRNOM)", "like", argumentDebutMot, true, true);
            requeteSql.ajouterConditionOr("upper(FRVIL)", "like", argumentComplet, true, true);
            requeteSql.ajouter(")");
            break;
          case IDENTIFIANT:
            requeteSql.ajouterConditionAnd("FRFRS", "like", argumentRecherche.getValeur());
            break;
          case TELEPHONE:
          case ENTIER:
            requeteSql.ajouterConditionAnd("FRFRS", "=", argumentRecherche.getValeur());
            break;
          case DECIMAL:
          case CODE_POSTAL:
            requeteSql.ajouterConditionAnd("FRCDP", "=", argumentRecherche.getValeur());
            break;
        }
      }
    }
    
    // Executer la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Générer la liste des identifiants fournisseur à retourner
    List<IdFournisseur> listeIdFournisseur = new ArrayList<IdFournisseur>();
    for (GenericRecord record : listeRecord) {
      // Une erreur lors du chargement d'un fournisseur n'empêche pas le chargement des autres fournisseurs de la liste.
      try {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("FRETB", "", false));
        IdFournisseur idFournisseur =
            IdFournisseur.getInstance(idEtablissement, record.getIntValue("FRCOL", 0), record.getIntValue("FRFRS", 0));
        listeIdFournisseur.add(idFournisseur);
      }
      catch (Exception e) {
        Trace.alerte("Un fournisseur invalide est ignoré lors du chargement de la liste des fournisseurs : FRETB="
            + record.getField("FRETB").toString() + " FRCOL=" + record.getField("FRCOL").toString() + " FRFRS="
            + record.getField("FRFRS").toString());
      }
    }
    return listeIdFournisseur;
  }
  
  /**
   * Recherche les coordonnées des fournisseurs pour un établissement donné.
   * @param pCritereFournisseur Critères de recherche de fournisseurs.
   * @return Liste des identifiants des coordonnées de fournisseurs chargée.
   */
  public ArrayList<IdAdresseFournisseur> chargerListeIdAdresseFournisseur(CritereFournisseur pCritereFournisseur) {
    // Exécuter la requête
    String requete = RequetesRechercheFournisseur.getRequete(querymg.getLibrary(), pCritereFournisseur);
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    
    // Remplir la liste résultat
    ArrayList<IdAdresseFournisseur> listeIdAdresseFournisseur = new ArrayList<IdAdresseFournisseur>();
    if (listeRecord != null) {
      for (GenericRecord record : listeRecord) {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("FRETB", "", false));
        IdFournisseur idFournisseur =
            IdFournisseur.getInstance(idEtablissement, record.getIntValue("FRCOL", 0), record.getIntValue("FRFRS", 0));
        IdAdresseFournisseur coordonneesfrs = IdAdresseFournisseur.getInstance(idFournisseur, record.getIntValue("INDICE", 0));
        listeIdAdresseFournisseur.add(coordonneesfrs);
      }
    }
    
    return listeIdAdresseFournisseur;
  }
  
  /**
   * Retourne une liste d'adresse fournisseur suivant une liste d'id d'adresse fournisseur.
   * @param pListeIdAdresseFournisseur Liste des identifiants des coordonnées de fournisseurs à charger.
   * @return Liste des coordonnées de fournisseurs.
   */
  public ListeAdresseFournisseur chargerListeAdresseFournisseur(List<IdAdresseFournisseur> pListeIdAdresseFournisseur) {
    if (pListeIdAdresseFournisseur == null) {
      throw new MessageErreurException("La liste des id d'adresse fournisseur est vide.");
    }
    
    RequeteSql requete = new RequeteSql();
    RequeteSql requeteAdresses = new RequeteSql();
    ListeAdresseFournisseur listeAdresseFournisseur = new ListeAdresseFournisseur();
    
    String codeEtablissement = pListeIdAdresseFournisseur.get(0).getCodeEtablissement();
    
    // Si l'indice de l'id est égal à zéro alors il s'agit du siège du fournisseur
    requete.ajouter("select fretb, frcol, frfrs, frnom, frcpl, frrue, frloc, frcdp, frvil, 0 as indice, frtel, frfax, frtlx, frobs"
        + " from " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " where fretb = "
        + RequeteSql.formaterStringSQL(codeEtablissement) + " and (");
    for (IdAdresseFournisseur idAdresse : pListeIdAdresseFournisseur) {
      requete.ajouter(" (frcol = " + idAdresse.getIdFournisseur().getCollectif());
      requete.ajouterConditionAnd("frfrs", "=", idAdresse.getIdFournisseur().getNumero());
      requete.ajouter(") or");
    }
    requete.supprimerDernierOperateur("or");
    requete.ajouter(")");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      throw new MessageErreurException("Aucune adresse fournisseur trouvée");
    }
    
    // Lecture des champs
    for (GenericRecord record : listeRecord) {
      listeAdresseFournisseur.add(completerAdresseFournisseur(record));
    }
    
    // Si l'indice de l'id est différent de zéro alors il s'agit des autres adresses fournisseur
    requeteAdresses.ajouter(
        "select feetb, fecol, fefrs, fenom, fecpl, ferue, felocg, fecdp, fevil, feext as indice, fecmd, feexp, fefac, fetel, fefax, fetlx, feobs"
            + " from " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR_ADRESSE + " where feetb = "
            + RequeteSql.formaterStringSQL(codeEtablissement) + " and (");
    for (IdAdresseFournisseur idAdresse : pListeIdAdresseFournisseur) {
      requeteAdresses.ajouter(" (fecol = " + idAdresse.getIdFournisseur().getCollectif());
      requeteAdresses.ajouterConditionAnd("fefrs", "=", idAdresse.getIdFournisseur().getNumero());
      if (idAdresse.getIndice() > 0) {
        requeteAdresses.ajouterConditionAnd("feext", "=", idAdresse.getIndice());
      }
      requeteAdresses.ajouter(") or");
    }
    requeteAdresses.supprimerDernierOperateur("or");
    requeteAdresses.ajouter(")");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecordAdresses = querymg.select(requeteAdresses.getRequete());
    
    // Lecture des champs
    for (GenericRecord record : listeRecordAdresses) {
      listeAdresseFournisseur.add(completerAdresseFournisseur(record));
    }
    
    return listeAdresseFournisseur;
    
  }
  
  /**
   * Retourne une adresse fournisseur suivant un id d'adresse fournisseur.
   * @param pIdAdresseFournisseur Identifiant d'une coordonnée de fournisseur.
   * @return Adresse du fournisseur.
   */
  public AdresseFournisseur chargerAdresseFournisseur(IdAdresseFournisseur pIdAdresseFournisseur) {
    if (pIdAdresseFournisseur == null) {
      throw new MessageErreurException("L'id de l'adresse fournisseur est invalide.");
    }
    
    String requete;
    
    // Si l'indice de l'id est égal à zéro alors il s'agit du siège du fournisseur
    if (pIdAdresseFournisseur.getIndice().intValue() == 0) {
      requete = "select fretb, frcol, frfrs, frnom, frcpl, frrue, frloc, frcdp, frvil, 0 as indice, frtel, frfax, frtlx, frobs" + " from "
          + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " where fretb = "
          + RequeteSql.formaterStringSQL(pIdAdresseFournisseur.getCodeEtablissement()) + " and frcol = "
          + pIdAdresseFournisseur.getIdFournisseur().getCollectif() + " and frfrs = "
          + pIdAdresseFournisseur.getIdFournisseur().getNumero();
    }
    else {
      requete =
          "select feetb, fecol, fefrs, fenom, fecpl, ferue, felocg, fecdp, fevil, feext as indice, fecmd, feexp, fefac, fetel, fefax, fetlx, feobs"
              + " from " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR_ADRESSE + " where feetb = "
              + RequeteSql.formaterStringSQL(pIdAdresseFournisseur.getCodeEtablissement()) + " and fecol = "
              + pIdAdresseFournisseur.getIdFournisseur().getCollectif() + " and fefrs = "
              + pIdAdresseFournisseur.getIdFournisseur().getNumero() + " and feext = " + pIdAdresseFournisseur.getIndice();
    }
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      throw new MessageErreurException("Aucune adresse fournisseur trouvée avec la clé suivante FRETB:"
          + pIdAdresseFournisseur.getCodeEtablissement() + " FRCOL:" + pIdAdresseFournisseur.getIdFournisseur().getCollectif() + " FRFRS:"
          + pIdAdresseFournisseur.getIdFournisseur().getNumero());
    }
    else if (listeRecord.size() > 1) {
      throw new MessageErreurException(listeRecord.size() + " Fournisseurs ont été trouvé avec la clé suivante FRETB:"
          + pIdAdresseFournisseur.getCodeEtablissement() + " FRCOL:" + pIdAdresseFournisseur.getIdFournisseur().getCollectif() + " FRFRS:"
          + pIdAdresseFournisseur.getIdFournisseur().getNumero());
    }
    
    return completerAdresseFournisseur(listeRecord.get(0));
  }
  
  /**
   * Retourne une liste d'adresse fournisseur suivant un établissement.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @return Liste des adresses fournisseurs.
   */
  public ListeAdresseFournisseur chargerListeAdresseFournisseurEtablissement(IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    ListeAdresseFournisseur listeAdresseFournisseur = new ListeAdresseFournisseur();
    
    // Lister les adresses du siège
    String requeteSiege =
        "select fretb, frcol, frfrs, frnom, frcpl, frrue, frloc, frcdp, frvil, 0 as indice" + " from " + querymg.getLibrary() + '.'
            + EnumTableBDD.FOURNISSEUR + " where fretb = " + RequeteSql.formaterStringSQL(pIdEtablissement.getCodeEtablissement());
    ArrayList<GenericRecord> listeRecordSiege = querymg.select(requeteSiege);
    if (listeRecordSiege != null && listeRecordSiege.size() > 0) {
      for (GenericRecord recordSiege : listeRecordSiege) {
        // Lecture des champs
        if (recordSiege != null) {
          listeAdresseFournisseur.add(completerAdresseFournisseur(recordSiege));
        }
      }
    }
    
    // On effectue la recherche dans la table des extensions fournisseurs pour les autres adresses
    // Seules les adresses de type 'commande' ont été demandées pour l'instant
    String requete = "select feetb, fecol, fefrs, fenom, fecpl, ferue, felocg, fecdp, fevil, feext as indice" + " from "
        + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR_ADRESSE + " where fecmd <> ' ' and feetb = "
        + RequeteSql.formaterStringSQL(pIdEtablissement.getCodeEtablissement());
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if (listeRecord != null && listeRecord.size() > 0) {
      for (GenericRecord record : listeRecord) {
        // Lecture des champs
        if (record != null) {
          listeAdresseFournisseur.add(completerAdresseFournisseur(record));
        }
      }
    }
    return listeAdresseFournisseur;
  }
  
  /**
   * Lit toutes les coordonnées pour un fournisseur à partir de son id.
   * Avec ou sans le siège suivant si l'option est activée.
   * @param pIdFournisseur Identifiant du fournisseur.
   * @param pInclureSiege Indication d'inclusion du siège.
   * @return Liste des adresses fournisseurs.
   */
  public List<AdresseFournisseur> lireListeAdresseFournisseur(IdFournisseur pIdFournisseur, boolean pInclureSiege) {
    IdFournisseur.controlerId(pIdFournisseur, true);
    
    // Si l'on souhaite inclure le siège il faut effectuer une requête supplémentaire
    List<AdresseFournisseur> listeAdresseFournisseur = new ArrayList<AdresseFournisseur>();
    if (pInclureSiege) {
      String requete =
          "select fretb, frcol, frfrs, frnom, frcpl, frrue, frloc, frcdp, frvil, 0 as indice" + " from " + querymg.getLibrary() + '.'
              + EnumTableBDD.FOURNISSEUR + " where fretb = " + RequeteSql.formaterStringSQL(pIdFournisseur.getCodeEtablissement())
              + " and frcol = " + pIdFournisseur.getCollectif() + " and frfrs = " + pIdFournisseur.getNumero();
      ArrayList<GenericRecord> listeRecord = querymg.select(requete);
      if ((listeRecord == null) || (listeRecord.size() == 0)) {
        throw new MessageErreurException("Aucune adresse fournisseur trouvée avec la clé suivante FRETB:"
            + pIdFournisseur.getCodeEtablissement() + " FRCOL:" + pIdFournisseur.getCollectif() + " FRFRS:" + pIdFournisseur.getNumero());
      }
      else if (listeRecord.size() > 1) {
        throw new MessageErreurException(listeRecord.size() + " Fournisseurs ont été trouvé avec la clé suivante FRETB:"
            + pIdFournisseur.getCodeEtablissement() + " FRCOL:" + pIdFournisseur.getCollectif() + " FRFRS:" + pIdFournisseur.getNumero());
      }
      // Lecture des champs
      if (listeRecord.get(0) != null) {
        listeAdresseFournisseur.add(completerAdresseFournisseur(listeRecord.get(0)));
      }
    }
    
    // On effectue la recherche dans la table des extensions fournisseurs
    String requete = "select feetb, fecol, fefrs, fenom, fecpl, ferue, felocg, fecdp, fevil, feext as indice, fecmd, feexp, fefac"
        + " from " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR_ADRESSE + " where feetb = "
        + RequeteSql.formaterStringSQL(pIdFournisseur.getCodeEtablissement()) + " and fecol = " + pIdFournisseur.getCollectif()
        + " and fefrs = " + pIdFournisseur.getNumero();
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if (listeRecord != null && listeRecord.size() > 0) {
      for (GenericRecord record : listeRecord) {
        // Lecture des champs
        if (record != null) {
          AdresseFournisseur coordonneesFournisseur = completerAdresseFournisseur(record);
          listeAdresseFournisseur.add(coordonneesFournisseur);
        }
      }
    }
    
    return listeAdresseFournisseur;
  }
  
  /**
   * Retourner la liste de tous les fournisseurs d'un établissement.<br>
   * Une erreur lors du chargement d'un fournisseur n'empêche pas le chargement des autres fournisseurs de la liste. Le fournisseur
   * fautif est ignoré avec un warning dans les traces.
   * @pIdEtablissement Identifiant de l'établissement dont il faut charger les fournisseurs.
   * @return Liste des fournisseurs de l'établissement.
   */
  public ListeFournisseur chargerListeFournisseurEtablissement(IdEtablissement pIdEtablissement) {
    // Contrôler les paramètres
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    // Construire la requête
    String request = "SELECT CONCAT(FRCOL, FRFRS) as COLFRS, FRCOL,FRFRS, FRNOM " + "FROM " + querymg.getLibrary() + '.'
        + EnumTableBDD.FOURNISSEUR + " WHERE FRETB = '" + pIdEtablissement.getCodeEtablissement() + "' ";
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecords = querymg.select(request);
    if ((listeRecords == null) || (listeRecords.size() < 0)) {
      return null;
    }
    
    // Générer la liste des fournisseurs
    ListeFournisseur listeFournisseur = new ListeFournisseur();
    for (GenericRecord record : listeRecords) {
      // Une erreur lors du chargement d'un fournisseur n'empêche pas le chargement des autres fournisseurs de la liste. Le fournisseur
      try {
        IdFournisseur idFournisseur =
            IdFournisseur.getInstance(pIdEtablissement, record.getIntValue("FRCOL", 0), record.getIntValue("FRFRS", 0));
        
        Fournisseur fournisseur = new Fournisseur(idFournisseur);
        fournisseur.setNomFournisseur(record.getString("FRNOM"));
        
        listeFournisseur.rajouterUnFournisseur(fournisseur);
      }
      catch (Exception e) {
        Trace.alerte("Un fournisseur invalide est ignoré lors du chargement de la liste des fournisseurs : FRETB="
            + record.getField("FRETB").toString() + " FRCOL=" + record.getField("FRCOL").toString() + " FRFRS="
            + record.getField("FRFRS").toString());
      }
      
    }
    
    return listeFournisseur;
  }
  
  /**
   * Retourne la liste des configurations de catalogue fournisseurs.<br>
   * Le passage de l'établissement en paramètre est obligatoire.
   * L'id fournisseur est un paramètre optionnel.
   * @param pCriteres Critères de recherche de configuration de catalogue.
   * @return Liste de configuration de catalogue.
   */
  public ListeConfigurationCatalogue chargerListeConfigsCatalogue(CriteresRechercheCfgCatalogue pCriteres) {
    if (pCriteres == null) {
      throw new MessageErreurException("Les critères de recherche pour les configurations de catalogue ne sont pas renseignées");
    }
    
    if (pCriteres.getIdEtablissement() == null) {
      throw new MessageErreurException("L'établissement n'est pas renseigné pour les configurations de catalogue");
    }
    
    String critereFournisseur = "";
    
    if (pCriteres.getIdFournisseur() != null) {
      critereFournisseur = " AND CTCOL = '" + pCriteres.getIdFournisseur().getCollectif().toString() + "' AND CTFRS = '"
          + pCriteres.getIdFournisseur().getNumero().toString() + "' ";
    }
    
    ListeConfigurationCatalogue listeConfigurationCatalogue = new ListeConfigurationCatalogue();
    
    String requete = "SELECT CTETB,CTCOL,CTFRS,CTNUM,CTLIB,CTFIC,CTTIT,FRNOM " + " FROM " + querymg.getLibrary() + '.'
        + EnumTableBDD.CATALOGUE_FOURNISSEUR + " LEFT JOIN " + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR
        + " ON CTETB = FRETB AND CTCOL = FRCOL AND CTFRS = FRFRS " + " WHERE CTETB = "
        + RequeteSql.formaterStringSQL(pCriteres.getIdEtablissement().getCodeEtablissement()) + " " + critereFournisseur + " ";
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if (listeRecord != null && listeRecord.size() > 0) {
      for (GenericRecord record : listeRecord) {
        Integer id = Integer.parseInt(record.getField("CTNUM").toString().trim());
        IdConfigurationCatalogue idConfigurationCatalogue = IdConfigurationCatalogue.getInstance(id);
        ConfigurationCatalogue configurationCatalogue = new ConfigurationCatalogue(idConfigurationCatalogue);
        // Etablissement
        configurationCatalogue.setIdEtablissement(pCriteres.getIdEtablissement());
        // Fournisseur
        if (pCriteres.getIdFournisseur() != null) {
          configurationCatalogue.setIdFournisseur(pCriteres.getIdFournisseur());
        }
        else {
          Integer coll = Integer.parseInt(record.getField("CTCOL").toString().trim());
          Integer frs = Integer.parseInt(record.getField("CTFRS").toString().trim());
          IdFournisseur idFournisseur = IdFournisseur.getInstance(pCriteres.getIdEtablissement(), coll, frs);
          configurationCatalogue.setIdFournisseur(idFournisseur);
        }
        // Libellé de la configuration
        configurationCatalogue.setLibelle(record.getField("CTLIB").toString().trim());
        // Fichier associé à la configuration
        configurationCatalogue.setFichierCSV(record.getField("CTFIC").toString().trim());
        // Raison sociale du fournisseur
        if (record.getField("FRNOM") != null) {
          configurationCatalogue.setRaisonSocialeFournisseur(record.getField("FRNOM").toString().trim());
        }
        
        listeConfigurationCatalogue.add(configurationCatalogue);
      }
    }
    
    return listeConfigurationCatalogue;
  }
  
  /**
   * Retourne une configuration de catalogue fournisseur sur la base de son ID.
   * @param pIdSession Identifiant de la session.
   * @param pIdConfiguration Identifiant de la configuration de catalogue.
   * @return Configuration de catalogue.
   */
  public ConfigurationCatalogue chargerConfigurationCatalogueParId(IdSession pIdSession, IdConfigurationCatalogue pIdConfiguration) {
    if (pIdConfiguration == null) {
      throw new MessageErreurException("L'id de configuration n'est pas renseigné");
    }
    
    ConfigurationCatalogue configurationCatalogue = null;
    
    String requete = "SELECT e.CTETB,e.CTCOL,e.CTFRS,e.CTNUM,CTLIB,CTFIC,CTTIT,FRNOM,CTZON,CTZ01,CTD01,CTF01,CTOPE,CTZ02,CTD02,CTF02 "
        + " FROM " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR + " e LEFT JOIN " + querymg.getLibrary() + '.'
        + EnumTableBDD.FOURNISSEUR + " ON e.CTETB = FRETB AND e.CTCOL = FRCOL AND e.CTFRS = FRFRS " + " LEFT JOIN " + querymg.getLibrary()
        + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR_LIGNE + " d ON e.CTNUM = d.CTNUM " + " WHERE e.CTNUM = '"
        + pIdConfiguration.getNumero() + "' ORDER BY CTZON,CTZ01 ";
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if (listeRecord != null && listeRecord.size() > 0) {
      configurationCatalogue = new ConfigurationCatalogue(pIdConfiguration);
      GenericRecord record = listeRecord.get(0);
      // Etablissement
      if (record.getField("CTETB") != null) {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getField("CTETB").toString());
        configurationCatalogue.setIdEtablissement(idEtablissement);
        // Fournisseur
        if (record.getField("CTCOL") != null && record.getField("CTFRS") != null) {
          try {
            Integer collectif = Integer.parseInt((record.getField("CTCOL").toString().trim()));
            Integer numero = Integer.parseInt((record.getField("CTFRS").toString().trim()));
            IdFournisseur idFournisseur = IdFournisseur.getInstance(idEtablissement, collectif, numero);
            configurationCatalogue.setIdFournisseur(idFournisseur);
          }
          catch (Exception e) {
            throw new MessageErreurException("L'id du fournisseur n'est pas conforme");
          }
        }
        if (record.getField("CTLIB") != null) {
          configurationCatalogue.setLibelle(record.getField("CTLIB").toString().trim());
        }
        if (record.getField("CTFIC") != null) {
          configurationCatalogue.renseignerInfosFichierCSV(pIdSession, (record.getField("CTFIC").toString().trim()));
        }
        
        ListeChampERP listeChampERP = new ListeChampERP();
        for (GenericRecord unRecord : listeRecord) {
          if (unRecord.isPresentField("CTZON") && unRecord.isPresentField("CTZ01")) {
            IdChampERP idChampERP = IdChampERP.getInstance(unRecord.getField("CTZON").toString());
            // ChampERP champERP = new ChampERP(idChampERP);
            ChampERP champERP = chargerUnChampERPparId(idChampERP);
            Integer indexChampCatalogue1 = Integer.parseInt(unRecord.getField("CTZ01").toString().trim());
            ChampCatalogue champCatalogue1 = new ChampCatalogue();
            champCatalogue1.setOrdre(indexChampCatalogue1);
            ChampCatalogue.renseignerInfosFichier(configurationCatalogue.getListeChampCatalogue(), champCatalogue1);
            Integer debutDecoupe = Integer.parseInt(unRecord.getField("CTD01").toString().trim());
            champCatalogue1.setDebutDecoupe(debutDecoupe);
            Integer finDecoupe = Integer.parseInt(unRecord.getField("CTF01").toString().trim());
            champCatalogue1.setFinDecoupe(finDecoupe);
            champERP.ajouterUneCorrespondanceChampCatalogue(champCatalogue1);
            
            Integer indexChampCatalogue2 = Integer.parseInt(unRecord.getField("CTZ02").toString().trim());
            if (indexChampCatalogue2 != 0) {
              ChampCatalogue champCatalogue2 = new ChampCatalogue();
              champCatalogue2.setOrdre(indexChampCatalogue2);
              ChampCatalogue.renseignerInfosFichier(configurationCatalogue.getListeChampCatalogue(), champCatalogue2);
              Integer debutDecoupe2 = Integer.parseInt(unRecord.getField("CTD02").toString().trim());
              champCatalogue2.setDebutDecoupe(debutDecoupe2);
              Integer finDecoupe2 = Integer.parseInt(unRecord.getField("CTF02").toString().trim());
              champCatalogue2.setFinDecoupe(finDecoupe2);
              champERP.ajouterUneCorrespondanceChampCatalogue(champCatalogue2);
            }
            listeChampERP.add(champERP);
          }
        }
        configurationCatalogue.setListeChampERP(listeChampERP);
      }
    }
    else {
      throw new MessageErreurException("il existe plusieurs configurations pour cet Id " + pIdConfiguration);
    }
    
    return configurationCatalogue;
  }
  
  /**
   * Retourner la liste des champs de l'ERP susceptibles de correspondre à des champs catalogues fournisseurs lors d'un import ou un
   * export.
   * @return Liste des champs ERP.
   */
  public ListeChampERP chargerListeChampERP() {
    ListeChampERP listeChampERP = new ListeChampERP();
    String requete = "SELECT C1ZON, C1LIBC, C1LIBL, C1TYP, C1LNG, C1DEC, C1NUM FROM " + EnvironnementExecution.getGvmx() + '.'
        + EnumTableBDD.CONFIGURATION_ZONE_ERP + " ORDER BY C1NUM ";
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if (listeRecord != null && listeRecord.size() > 0) {
      for (GenericRecord record : listeRecord) {
        IdChampERP idChampERP = IdChampERP.getInstance(record.getField("C1ZON").toString());
        ChampERP champERP = new ChampERP(idChampERP);
        champERP.setLibelleCourt(record.getField("C1LIBC").toString().trim());
        champERP.setLibelleLong(record.getField("C1LIBL").toString().trim());
        EnumTypeZoneERP enumTypeZoneERP = EnumTypeZoneERP.valueOfByCode(record.getField("C1TYP").toString().trim());
        champERP.setType(enumTypeZoneERP);
        Integer longueur = Integer.parseInt(record.getField("C1LNG").toString().trim());
        champERP.setLongueur(longueur);
        listeChampERP.add(champERP);
      }
    }
    return listeChampERP;
  }
  
  /**
   * Permet de sauvegarder une configuration catlogue en base de données.<br>
   * Si la configuration possède un ID il s'agit d'une modification de configuration existante.
   * Sinon il s'agit d'une nouvelle configuration.
   * @param pConfigurationCatalogue Configuration de catalogue.
   */
  public void sauverUneConfigurationCatalogue(ConfigurationCatalogue pConfigurationCatalogue) {
    if (pConfigurationCatalogue == null || pConfigurationCatalogue.getId() == null) {
      throw new MessageErreurException("Il n'y a aucune configuration de catalogue à sauver");
    }
    Integer dernierId = 0;
    if (!pConfigurationCatalogue.getId().isExistant()) {
      // D'abord on récupère le dernier ID saisi en base de données
      String requete = "SELECT MAX(CTNUM) AS DERN FROM " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR;
      ArrayList<GenericRecord> listeRecord = querymg.select(requete);
      if (listeRecord == null) {
        throw new MessageErreurException("Erreur de récupération du dernier ID de configuration");
      }
      else if (listeRecord.size() == 1 && listeRecord.get(0).isPresentField("DERN")) {
        dernierId = Integer.parseInt(listeRecord.get(0).getField("DERN").toString().trim());
      }
      dernierId += 1;
      
      // Ensuite on rentre la nouvelle configuration avec le nouvel ID
      String CTETB = "";
      if (pConfigurationCatalogue.getIdEtablissement() != null) {
        CTETB = pConfigurationCatalogue.getIdEtablissement().getCodeEtablissement();
      }
      
      String CTCOL = "";
      String CTFRS = "";
      if (pConfigurationCatalogue.getIdFournisseur() != null) {
        CTCOL = pConfigurationCatalogue.getIdFournisseur().getCollectif().toString();
        CTFRS = pConfigurationCatalogue.getIdFournisseur().getNumero().toString();
      }
      String CTLIB = "";
      if (pConfigurationCatalogue.getLibelle() != null) {
        CTLIB = RequeteSql.traiterCaracteresSpeciauxSQL(pConfigurationCatalogue.getLibelle());
      }
      String CTFIC = "";
      if (pConfigurationCatalogue.getFichierCSV() != null) {
        CTFIC = RequeteSql.traiterCaracteresSpeciauxSQL(pConfigurationCatalogue.getFichierCSV());
      }
      
      requete = " INSERT INTO " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR
          + " (CTETB,CTCOL,CTFRS,CTNUM,CTLIB,CTFIC,CTTIT) " + " VALUES ('" + CTETB + "','" + CTCOL + "','" + CTFRS + "','" + dernierId
          + "','" + CTLIB + "','" + CTFIC + "','1') ";
      
      if (querymg.requete(requete) != 1) {
        throw new MessageErreurException(
            "La sauvegarde de la configuration " + CTCOL + "/" + CTFRS + " avec le fichier " + CTFIC + " est en échec");
      }
      else {
        IdConfigurationCatalogue idConfigurationCatalogue = IdConfigurationCatalogue.getInstance(dernierId);
        pConfigurationCatalogue.setId(idConfigurationCatalogue);
      }
      
      creerLesLignesCatalogueFournisseur(pConfigurationCatalogue);
    }
    else {
      // On va mettre à jour l'entête
      String id = null;
      if (pConfigurationCatalogue.getId() != null) {
        id = pConfigurationCatalogue.getId().getNumero().toString();
      }
      String CTETB = "";
      if (pConfigurationCatalogue.getIdEtablissement() != null) {
        CTETB = pConfigurationCatalogue.getIdEtablissement().getCodeEtablissement();
      }
      
      String CTCOL = "";
      String CTFRS = "";
      if (pConfigurationCatalogue.getIdFournisseur() != null) {
        CTCOL = pConfigurationCatalogue.getIdFournisseur().getCollectif().toString();
        CTFRS = pConfigurationCatalogue.getIdFournisseur().getNumero().toString();
      }
      String CTLIB = "";
      if (pConfigurationCatalogue.getLibelle() != null) {
        CTLIB = RequeteSql.traiterCaracteresSpeciauxSQL(pConfigurationCatalogue.getLibelle());
      }
      String CTFIC = "";
      if (pConfigurationCatalogue.getFichierCSV() != null) {
        CTFIC = RequeteSql.traiterCaracteresSpeciauxSQL(pConfigurationCatalogue.getFichierCSV());
      }
      String requete =
          " UPDATE " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR + " SET CTETB = '" + CTETB + "',CTCOL = '" + CTCOL
              + "', CTFRS = '" + CTFRS + "', CTLIB = '" + CTLIB + "', CTFIC='" + CTFIC + "' WHERE CTNUM ='" + id + "'  ";
      if (querymg.requete(requete) != 1) {
        throw new MessageErreurException("La sauvegarde de la configuration numéro " + id + " est en échec");
      }
      
      // Si ok on DELETE les lignes
      requete = " DELETE FROM " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR_LIGNE + " WHERE CTNUM = '" + id + "' ";
      if (querymg.requete(requete) < 0) {
        throw new MessageErreurException("La suppression des lignes de la configuration numéro " + id + " est en échec");
      }
      // Si OK on MET A JOUR LES LIGNES
      creerLesLignesCatalogueFournisseur(pConfigurationCatalogue);
    }
  }
  
  /**
   * Permet de supprimer une configuration de catalogue fournisseur.
   * @param pConfigurationCatalogue Configuration de catalogue.
   */
  public void supprimerUneConfigurationCatalogue(ConfigurationCatalogue pConfigurationCatalogue) {
    if (pConfigurationCatalogue == null) {
      return;
    }
    
    String idConfig = null;
    idConfig = pConfigurationCatalogue.getId().getNumero().toString();
    
    if (idConfig != null) {
      String requete =
          "DELETE FROM " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR + " WHERE CTNUM = '" + idConfig + "' ";
      if (querymg.requete(requete) != 1) {
        throw new MessageErreurException("La suppression de la configuration " + idConfig + " est en échec");
      }
      
      requete =
          "DELETE FROM " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR_LIGNE + " WHERE CTNUM = '" + idConfig + "' ";
      if (querymg.requete(requete) < 0) {
        throw new MessageErreurException("La suppression du détail de la configuration " + idConfig + " est en échec");
      }
    }
    else {
      throw new MessageErreurException("L'identifiant de la configuration " + idConfig + " n'est pas conforme");
    }
  }
  
  /**
   * Retourner une liste de fournisseurs de base à partir d'un filtre.<br>
   * Une erreur lors du chargement d'un fournisseur n'empêche pas le chargement des autres fournisseurs de la liste. Le fournisseur
   * fautif est ignoré avec un warning dans les traces.
   * @Param pFiltreFournisseurBase Filtre contenant les critères de recherche du fournisseur.
   * @return Liste des fournisseurs correspondant à la recherche.
   */
  public ListeFournisseurBase chargerListeFournisseurBaseParEtablissement(FiltreFournisseurBase pFiltreFournisseurBase) {
    // Contrôler les paramètres
    if (pFiltreFournisseurBase == null) {
      return null;
    }
    
    // Construire la partie WHERE de la requête SQL
    String where = "";
    
    // Ajouter le filtre sur l'établissement
    if (pFiltreFournisseurBase.getIdEtablissement() != null) {
      where = " AND FRETB = '" + pFiltreFournisseurBase.getIdEtablissement().getCodeEtablissement() + "' ";
    }
    
    // Construire la requête
    String requete = "SELECT FRETB, FRCOL, FRFRS, FRNOM, FRCPL, FRRUE, FRLOC, FRCDP, FRVIL, FRPAY, FRCLA, FRCL2 FROM "
        + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " WHERE FRTOP = 0 " + where + " ORDER BY FRETB,FRFRS,FRNOM ";
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    
    // Générer la liste des fournisseurs
    ListeFournisseurBase listeFournisseurBase = new ListeFournisseurBase();
    if (listeRecord != null && listeRecord.size() > 0) {
      for (GenericRecord record : listeRecord) {
        // Une erreur lors du chargement d'un fournisseur ne doit pas empêcher le chargement des autres fournisseurs de la liste.
        try {
          IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getField("FRETB").toString().trim());
          Integer collectif = Integer.parseInt(record.getField("FRCOL").toString().trim());
          Integer numero = Integer.parseInt(record.getField("FRFRS").toString().trim());
          IdFournisseur idFournisseur = IdFournisseur.getInstance(idEtablissement, collectif, numero);
          FournisseurBase fournisseurBase = new FournisseurBase(idFournisseur);
          fournisseurBase.setRaisonSociale(record.getField("FRNOM").toString().trim());
          fournisseurBase.setCodePostal(record.getField("FRCDP").toString().trim());
          fournisseurBase.setVille(record.getField("FRVIL").toString().trim());
          fournisseurBase.setCleClassementUn(record.getField("FRCLA").toString().trim());
          fournisseurBase.setCleClassementDeux(record.getField("FRCL2").toString().trim());
          Adresse adresseFournisseur = new Adresse();
          adresseFournisseur.setNom(record.getField("FRNOM").toString().trim());
          adresseFournisseur.setComplementNom(record.getField("FRCPL").toString().trim());
          adresseFournisseur.setRue(record.getField("FRRUE").toString().trim());
          adresseFournisseur.setLocalisation(record.getField("FRLOC").toString().trim());
          adresseFournisseur.setCodePostal(Integer.parseInt(record.getField("FRCDP").toString().trim()));
          adresseFournisseur.setVille(record.getField("FRVIL").toString().trim());
          adresseFournisseur.setPays(record.getField("FRPAY").toString().trim());
          fournisseurBase.setAdresse(adresseFournisseur);
          
          listeFournisseurBase.add(fournisseurBase);
        }
        catch (Exception e) {
          Trace.alerte("Un fournisseur invalide est ignoré lors du chargement de la liste des fournisseurs : FRETB="
              + record.getField("FRETB").toString() + " FRCOL=" + record.getField("FRCOL").toString() + " FRFRS="
              + record.getField("FRFRS").toString());
        }
      }
    }
    
    return listeFournisseurBase;
  }
  
  /**
   * Charger une liste des données de base des fournisseurs à partir d'une liste d'identifiant.<br>
   * Une erreur lors du chargement d'un fournisseur n'empêche pas le chargement des autres fournisseurs de la liste. Le fournisseur
   * fautif est ignoré avec un warning dans les traces.
   * @param pListeId Liste d'identifiants fournisseurs.
   * @return Liste de données de base de fournisseurs.
   */
  public ListeFournisseurBase chargerListeFournisseurBase(List<IdFournisseur> pListeId) {
    // Contrôler les paramètres
    if (pListeId == null) {
      throw new MessageErreurException("La liste des identifiants fournisseur est invalide");
    }
    
    // Construire la reqête SQL
    RequeteSql requete = new RequeteSql();
    requete.ajouter("SELECT FRETB, FRCOL, FRFRS, FRNOM, FRCPL, FRRUE, FRLOC, FRCDP, FRVIL, FRPAY, FRCLA, FRCL2 FROM "
        + querymg.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " WHERE FRETB = '" + pListeId.get(0).getCodeEtablissement() + "'");
    
    for (IdFournisseur idFournisseur : pListeId) {
      IdFournisseur.controlerId(idFournisseur, true);
      if (pListeId.get(0) == idFournisseur) {
        requete.ajouter(" AND (");
      }
      requete.ajouter("(FRCOL =" + idFournisseur.getCollectif());
      requete.ajouter(" AND FRFRS = " + idFournisseur.getNumero());
      requete.ajouter(") or");
    }
    requete.supprimerDernierOperateur("or");
    requete.ajouter(")");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = querymg.select(requete.getRequete());
    if (listeRecord == null) {
      return null;
    }
    
    // Générer la liste résultat
    ListeFournisseurBase listeFournisseur = new ListeFournisseurBase();
    for (GenericRecord record : listeRecord) {
      // Une erreur lors du chargement d'un fournisseur n'empêche pas le chargement des autres fournisseurs de la liste.
      try {
        Integer collectif = Integer.parseInt(record.getField("FRCOL").toString().trim());
        Integer numero = Integer.parseInt(record.getField("FRFRS").toString().trim());
        IdFournisseur idFournisseur = IdFournisseur.getInstance(pListeId.get(0).getIdEtablissement(), collectif, numero);
        FournisseurBase fournisseurBase = new FournisseurBase(idFournisseur);
        fournisseurBase.setRaisonSociale(record.getField("FRNOM").toString().trim());
        fournisseurBase.setCodePostal(record.getField("FRCDP").toString().trim());
        fournisseurBase.setVille(record.getField("FRVIL").toString().trim());
        fournisseurBase.setCleClassementUn(record.getField("FRCLA").toString().trim());
        fournisseurBase.setCleClassementDeux(record.getField("FRCL2").toString().trim());
        Adresse adresseFournisseur = new Adresse();
        adresseFournisseur.setNom(record.getField("FRNOM").toString().trim());
        adresseFournisseur.setComplementNom(record.getField("FRCPL").toString().trim());
        adresseFournisseur.setRue(record.getField("FRRUE").toString().trim());
        adresseFournisseur.setLocalisation(record.getField("FRLOC").toString().trim());
        adresseFournisseur.setCodePostal(Integer.parseInt(record.getField("FRCDP").toString().trim()));
        adresseFournisseur.setVille(record.getField("FRVIL").toString().trim());
        adresseFournisseur.setPays(record.getField("FRPAY").toString().trim());
        fournisseurBase.setAdresse(adresseFournisseur);
        listeFournisseur.add(fournisseurBase);
      }
      catch (Exception e) {
        Trace.alerte("Un fournisseur invalide est ignoré lors du chargement de la liste des fournisseurs : FRETB="
            + record.getField("FRETB").toString() + " FRCOL=" + record.getField("FRCOL").toString() + " FRFRS="
            + record.getField("FRFRS").toString());
      }
    }
    return listeFournisseur;
  }
  
  /**
   * Retourner un champ ERP sur la base de son identifiant.
   * @param pIdChampERP Identifiant d'un champ ERP.
   * @return Champ ERP.
   */
  public ChampERP chargerUnChampERPparId(IdChampERP pIdChampERP) {
    if (pIdChampERP == null) {
      throw new MessageErreurException("L'identifiant du champ ERP n'est pas renseigné");
    }
    
    ChampERP champERP = null;
    String requete = "SELECT C1ZON, C1LIBC, C1LIBL, C1TYP, C1LNG, C1DEC, C1NUM FROM " + EnvironnementExecution.getGvmx() + '.'
        + EnumTableBDD.CONFIGURATION_ZONE_ERP + " WHERE C1ZON = '" + pIdChampERP.getCode() + "' ";
    ArrayList<GenericRecord> listeRecord = querymg.select(requete);
    if (listeRecord != null && listeRecord.size() == 1) {
      champERP = new ChampERP(pIdChampERP);
      champERP.setLibelleCourt(listeRecord.get(0).getField("C1LIBC").toString().trim());
      champERP.setLibelleLong(listeRecord.get(0).getField("C1LIBL").toString().trim());
      EnumTypeZoneERP enumTypeZoneERP = EnumTypeZoneERP.valueOfByCode(listeRecord.get(0).getField("C1TYP").toString().trim());
      champERP.setType(enumTypeZoneERP);
      Integer longueur = Integer.parseInt(listeRecord.get(0).getField("C1LNG").toString().trim());
      champERP.setLongueur(longueur);
    }
    
    return champERP;
  }
  
  /**
   * Sauver en base de données l'association d'un champ ERP avec n champs catalogue.
   * @param pConfiguration Configuration de catalogue.
   * @param pIdChampERP Identifiant d'un champ ERP.
   */
  public void sauverAssociationChampsConfigurationCatalogue(ConfigurationCatalogue pConfiguration, IdChampERP pIdChampERP) {
    if (pConfiguration == null) {
      throw new MessageErreurException("La configuration n'est pas renseignée");
    }
    if (pIdChampERP == null) {
      throw new MessageErreurException("L'identifiant du champ ERP n'est pas renseigné");
    }
    
    // Si la config n'existe pas en création alors il faut la créer
    if (!pConfiguration.getId().isExistant()) {
      sauverUneConfigurationCatalogue(pConfiguration);
      return;
    }
    
    String CTETB = "";
    if (pConfiguration.getIdEtablissement() != null) {
      CTETB = pConfiguration.getIdEtablissement().getCodeEtablissement();
    }
    
    String CTCOL = "";
    String CTFRS = "";
    if (pConfiguration.getIdFournisseur() != null) {
      CTCOL = pConfiguration.getIdFournisseur().getCollectif().toString();
      CTFRS = pConfiguration.getIdFournisseur().getNumero().toString();
    }
    
    // On supprime les associations
    String idConfig = null;
    idConfig = pConfiguration.getId().getNumero().toString();
    
    if (idConfig != null) {
      String requete = "DELETE FROM " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR_LIGNE + " WHERE CTNUM = '"
          + idConfig + "' AND CTZON = '" + pIdChampERP.getCode() + "' ";
      if (querymg.requete(requete) < 0) {
        throw new MessageErreurException(
            "La suppression du détail de la configuration " + idConfig + " est en échec pour le champ " + pIdChampERP.getCode());
      }
    }
    
    ChampERP champERP = pConfiguration.getListeChampERP().recupererUnChampParId(pIdChampERP);
    if (champERP == null) {
      throw new MessageErreurException("Le champ ERP " + pIdChampERP + " n'existe pas dans la configuration de catalogue");
    }
    String CTZON = "";
    CTZON = champERP.getId().getCode();
    
    if (champERP.getListeChampCatalogue() == null || champERP.getListeChampCatalogue().size() == 0) {
      return;
    }
    
    String ordre1 = "0";
    String debut1 = "0";
    String fin1 = "0";
    String ordre2 = "0";
    String debut2 = "0";
    String fin2 = "0";
    
    int i = 0;
    for (ChampCatalogue champCatalogue : champERP.getListeChampCatalogue()) {
      if (i == 0) {
        if (champCatalogue != null) {
          ordre1 = champCatalogue.getOrdre().toString();
          debut1 = champCatalogue.getDebutDecoupe().toString();
          fin1 = champCatalogue.getFinDecoupe().toString();
        }
      }
      else if (i == 1) {
        if (champCatalogue != null) {
          ordre2 = champCatalogue.getOrdre().toString();
          debut2 = champCatalogue.getDebutDecoupe().toString();
          fin2 = champCatalogue.getFinDecoupe().toString();
        }
      }
      
      i++;
    }
    
    String requete = " INSERT INTO " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR_LIGNE
        + " (CTETB,CTCOL,CTFRS,CTNUM,CTZON,CTZ01,CTD01,CTF01,CTOPE,CTZ02,CTD02,CTF02) " + " VALUES ('" + CTETB + "','" + CTCOL + "','"
        + CTFRS + "','" + idConfig + "','" + CTZON + "','" + ordre1 + "','" + debut1 + "','" + fin1 + "','','" + ordre2 + "','" + debut2
        + "','" + fin2 + "') ";
    if (querymg.requete(requete) < 1) {
      throw new MessageErreurException("La sauvegarde de la zone " + CTZON + " pour la configuration " + idConfig + " est en échec");
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Créer un fournisseur à partir des champs de la base de données.
   * @param pRecord Information d'enregistrement d'un fournisseur.
   * @return Fournisseur complet.
   */
  private Fournisseur completerFournisseur(GenericRecord pRecord) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("FRETB", "", false));
    IdFournisseur idFournisseur =
        IdFournisseur.getInstance(idEtablissement, pRecord.getIntValue("FRCOL", 0), pRecord.getIntValue("FRFRS", 0));
    Fournisseur pFournisseur = new Fournisseur(idFournisseur);
    
    pFournisseur.setId(idFournisseur);
    pFournisseur.setTopSysteme(pRecord.getIntValue("FRTOP", 0));
    pFournisseur.setDateCreation(pRecord.getDateValue("FRCRE", 0));
    pFournisseur.setDateModification(pRecord.getDateValue("FRMOD", 0));
    pFournisseur.setDateTraitement(pRecord.getDateValue("FRTRT", 0));
    pFournisseur.setNomFournisseur(pRecord.getStringValue("FRNOM", "", true));
    if (pFournisseur.getListeAdresseFournisseur() == null) {
      pFournisseur.setListeAdresse(new ArrayList<AdresseFournisseur>());
    }
    AdresseFournisseur coordonneesFournisseur = new AdresseFournisseur(IdAdresseFournisseur.getInstance(idFournisseur, 0));
    coordonneesFournisseur.setType(EnumTypeAdresseFournisseur.SIEGE);
    coordonneesFournisseur.setAdresse(new Adresse());
    coordonneesFournisseur.getAdresse().setNom(pRecord.getStringValue("FRNOM", "", true));
    coordonneesFournisseur.getAdresse().setComplementNom(pRecord.getStringValue("FRCPL", "", true));
    coordonneesFournisseur.getAdresse().setRue(pRecord.getStringValue("FRRUE", "", true));
    coordonneesFournisseur.getAdresse().setLocalisation(pRecord.getStringValue("FRLOC", "", true));
    coordonneesFournisseur.getAdresse().setCodePostal(pRecord.getIntValue("FRCDP", 0));
    coordonneesFournisseur.getAdresse().setVille(pRecord.getStringValue("FRVIL", "", true));
    coordonneesFournisseur.getAdresse().setCodePays(pRecord.getStringValue("FRCOP", "", true));
    coordonneesFournisseur.getAdresse().setPays(pRecord.getStringValue("FRPAY", "", true));
    coordonneesFournisseur.setNumeroTelephone(pRecord.getStringValue("FRTEL", "", true));
    coordonneesFournisseur.setNumeroFax(pRecord.getStringValue("FRFAX", "", true));
    coordonneesFournisseur.setNumeroTelex(pRecord.getStringValue("FRTLX", "", true));
    coordonneesFournisseur.setObservations(pRecord.getStringValue("FROBS", "", true));
    pFournisseur.getListeAdresseFournisseur().add(coordonneesFournisseur);
    
    pFournisseur.setTopUtilisateur1(pRecord.getStringValue("FRTOP1", "", true));
    pFournisseur.setTopUtilisateur2(pRecord.getStringValue("FRTOP2", "", true));
    pFournisseur.setTopUtilisateur3(pRecord.getStringValue("FRTOP3", "", true));
    pFournisseur.setTopUtilisateur4(pRecord.getStringValue("FRTOP4", "", true));
    pFournisseur.setTopUtilisateur5(pRecord.getStringValue("FRTOP5", "", true));
    pFournisseur.setCodeCategorie(pRecord.getStringValue("FRCAT", "", true));
    pFournisseur.setCleClassement(pRecord.getStringValue("FRCLA", "", true));
    
    pFournisseur.setCodeLangue(pRecord.getStringValue("FRLAN", "", true));
    pFournisseur.setTypeFacturation(pRecord.getStringValue("FRTFA", "", true));
    pFournisseur.setDevise(pRecord.getStringValue("FRDEV", "", true));
    pFournisseur.setRemise1(pRecord.getBigDecimalValue("FRREM1"));
    pFournisseur.setRemise2(pRecord.getBigDecimalValue("FRREM2"));
    pFournisseur.setRemise3(pRecord.getBigDecimalValue("FRREM3"));
    pFournisseur.setPourcentageEscompte(pRecord.getBigDecimalValue("FRESC"));
    pFournisseur.setMinimumCommande(pRecord.getBigDecimalValue("FRMIN"));
    pFournisseur.setMontantFrancoPort(pRecord.getBigDecimalValue("FRFCO"));
    pFournisseur.setPeriodiciteCommande(pRecord.getBigDecimalValue("FRPER"));
    pFournisseur.setNotreNumeroClient(pRecord.getStringValue("FRNRO", "", true));
    pFournisseur.setCodeReglement(pRecord.getStringValue("FRRGL", "", true));
    pFournisseur.setEcheance(pRecord.getStringValue("FRECH", "", true));
    pFournisseur.setZoneSysteme(pRecord.getIntValue("FRCGM", 0));
    pFournisseur.setCodeLitige(pRecord.getStringValue("FRLIT", "", true));
    pFournisseur.setCodePaysCEE(pRecord.getStringValue("FRCEE", "", true));
    pFournisseur.setNumeroLibelleArticle(pRecord.getStringValue("FRNLA", "", true));
    pFournisseur.setTopStats(pRecord.getStringValue("FRSTQ", "", true));
    pFournisseur.setCumulRemises(pRecord.getStringValue("FRIN1", "", true));
    pFournisseur.setMargeSecurite(pRecord.getStringValue("FRIN2", "", true));
    pFournisseur.setNombreBonRec(pRecord.getStringValue("FRIN3", "", true));
    pFournisseur.setModeExpedition(pRecord.getStringValue("FRMEX", "", true));
    if (pFournisseur.getModeExpedition() != null) {
      pFournisseur.setModeEnlevement(false);
      if (pFournisseur.getModeExpedition().equals("*E")) {
        pFournisseur.setModeEnlevement(true);
      }
    }
    
    pFournisseur.setTransporteur(pRecord.getStringValue("FRCTR", "", true));
    pFournisseur.setCodeRistourne(pRecord.getStringValue("FRCNR", "", true));
    pFournisseur.setCodePaysTVAIntracom(pRecord.getStringValue("FRNIP", "", true));
    pFournisseur.setCodeInfoTVAIntracom(pRecord.getStringValue("FRNIK", "", true));
    pFournisseur.setNumeroSIREN(pRecord.getStringValue("FRSRN", "", true));
    pFournisseur.setComplementSIRET(pRecord.getStringValue("FRSRT", "", true));
    pFournisseur.setMaximumCommande(pRecord.getBigDecimalValue("FRMCO"));
    pFournisseur.setNoteFournisseur(pRecord.getIntValue("FRNOT", 0));
    pFournisseur.setRelicat(pRecord.getStringValue("FRIN4", "", true));
    pFournisseur.setExclusionWeb(pRecord.getStringValue("FRIN5", "", true));
    pFournisseur.setDateEcheance(pRecord.getStringValue("FRIN6", "", true));
    pFournisseur.setDelaiJours(pRecord.getStringValue("FRIN7", "", true));
    pFournisseur.setNumeroCompteGeneral(pRecord.getIntValue("FRNCG", 0));
    pFournisseur.setNumeroCompteAuxiliaire(pRecord.getIntValue("FRNCA", 0));
    pFournisseur.setCodeAPE(pRecord.getStringValue("FRAPE", "", true));
    pFournisseur.setFournisseurInterne(pRecord.getStringValue("FRIN9", "", true));
    pFournisseur.setCodeExpeDILICOM(pRecord.getStringValue("FREXDI", "", true));
    pFournisseur.setCodeNoteDILICOM(pRecord.getStringValue("FRNTDI", "", true));
    pFournisseur.setCodeAPEv2(pRecord.getStringValue("FRAPEN", "", true));
    pFournisseur.setMarque(pRecord.getStringValue("FRMRQ", "", true));
    pFournisseur.setCollectifRegroupement(pRecord.getIntValue("FRCOR", 0));
    pFournisseur.setFournisseurRegroupement(pRecord.getIntValue("FRFRR", 0));
    pFournisseur.setPrefixeArticle(pRecord.getStringValue("FRRAR", "", true));
    pFournisseur.setCoeffApproche(pRecord.getBigDecimalValue("FRKAP"));
    pFournisseur.setDelaiLivraison(pRecord.getIntValue("FRDEL1", 0));
    pFournisseur.setDelaiLivraison2(pRecord.getIntValue("FRDEL2", 0));
    pFournisseur.setCleClassement2(pRecord.getStringValue("FRCL2", "", true));
    if (pRecord.isPresentField("FRIN11")) {
      pFournisseur.setTypePrixAchat(EnumTypePrixAchat.valueOfByCode(pRecord.getCharacterValue("FRIN11")));
    }
    if (pRecord.isPresentField("FRIN13")) {
      char code = pRecord.getCharacterValue("FRIN13", ' ');
      switch (code) {
        case '1':
          pFournisseur.setTypeOptionChiffrageEdition(EnumOptionEdition.CHIFFRE_AVEC_TOTAL);
          break;
        case '2':
          pFournisseur.setTypeOptionChiffrageEdition(EnumOptionEdition.CHIFFRE);
          break;
        case '3':
          pFournisseur.setTypeOptionChiffrageEdition(EnumOptionEdition.NON_CHIFFRE);
          break;
        default:
          pFournisseur.setTypeOptionChiffrageEdition(EnumOptionEdition.CHIFFRE_AVEC_TOTAL);
      }
    }
    
    return pFournisseur;
  }
  
  /**
   * Renseigne une adresse fournisseur à partir des champs de la base de données.
   * @param pRecord Information d'enregistrement d'un fournisseur.
   * @return Adresse du fournisseur.
   */
  private AdresseFournisseur completerAdresseFournisseur(GenericRecord pRecord) {
    boolean tableExtension = pRecord.isPresentField("FEETB");
    
    // Si l'Id n'est pas déjà renseigné
    // On initialise les variables à partir des champs de la table des extensions fournisseur
    if (tableExtension) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("FEETB", "", false));
      IdFournisseur idFournisseur =
          IdFournisseur.getInstance(idEtablissement, pRecord.getIntValue("FECOL", 0), pRecord.getIntValue("FEFRS", 0));
      IdAdresseFournisseur idAdresseFournisseur = IdAdresseFournisseur.getInstance(idFournisseur, pRecord.getIntValue("INDICE", 0));
      AdresseFournisseur pAdresseFournisseur = new AdresseFournisseur(idAdresseFournisseur);
      pAdresseFournisseur.setAdresse(new Adresse());
      pAdresseFournisseur.getAdresse().setNom(pRecord.getStringValue("FENOM", "", true));
      pAdresseFournisseur.getAdresse().setComplementNom(pRecord.getStringValue("FECPL", "", true));
      pAdresseFournisseur.getAdresse().setRue(pRecord.getStringValue("FERUE", "", true));
      pAdresseFournisseur.getAdresse().setLocalisation(pRecord.getStringValue("FELOCG", "", true));
      pAdresseFournisseur.getAdresse().setCodePostal(pRecord.getIntValue("FECDP", 0));
      pAdresseFournisseur.getAdresse().setVille(pRecord.getStringValue("FEVIL", "", true));
      if (!pRecord.getCharacterValue("FECMD").equals(' ')) {
        pAdresseFournisseur.setType(EnumTypeAdresseFournisseur.COMMANDE);
      }
      else if (!pRecord.getCharacterValue("FEFAC").equals(' ')) {
        pAdresseFournisseur.setType(EnumTypeAdresseFournisseur.FACTURATION);
      }
      else if (!pRecord.getCharacterValue("FEEXP").equals(' ')) {
        pAdresseFournisseur.setType(EnumTypeAdresseFournisseur.EXPEDITION);
      }
      else {
        pAdresseFournisseur.setType(EnumTypeAdresseFournisseur.SIEGE);
      }
      pAdresseFournisseur.setNumeroTelephone(pRecord.getStringValue("FETEL", "", true));
      pAdresseFournisseur.setNumeroFax(pRecord.getStringValue("FEFAX", "", true));
      pAdresseFournisseur.setNumeroTelex(pRecord.getStringValue("FETLX", "", true));
      pAdresseFournisseur.setObservations(pRecord.getStringValue("FEOBS", "", true));
      
      return pAdresseFournisseur;
    }
    // On initialise les variables à partir des champs de la table des fournisseurs
    else {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("FRETB", "", false));
      IdFournisseur idFournisseur =
          IdFournisseur.getInstance(idEtablissement, pRecord.getIntValue("FRCOL", 0), pRecord.getIntValue("FRFRS", 0));
      IdAdresseFournisseur idAdresseFournisseur = IdAdresseFournisseur.getInstance(idFournisseur, 0);
      AdresseFournisseur pAdresseFournisseur = new AdresseFournisseur(idAdresseFournisseur);
      pAdresseFournisseur.setAdresse(new Adresse());
      pAdresseFournisseur.getAdresse().setNom(pRecord.getStringValue("FRNOM", "", true));
      pAdresseFournisseur.getAdresse().setComplementNom(pRecord.getStringValue("FRCPL", "", true));
      pAdresseFournisseur.getAdresse().setRue(pRecord.getStringValue("FRRUE", "", true));
      pAdresseFournisseur.getAdresse().setLocalisation(pRecord.getStringValue("FRLOC", "", true));
      pAdresseFournisseur.getAdresse().setCodePostal(pRecord.getIntValue("FRCDP", 0));
      pAdresseFournisseur.getAdresse().setVille(pRecord.getStringValue("FRVIL", "", true));
      pAdresseFournisseur.setType(EnumTypeAdresseFournisseur.SIEGE);
      pAdresseFournisseur.setNumeroTelephone(pRecord.getStringValue("FRTEL", "", true));
      pAdresseFournisseur.setNumeroFax(pRecord.getStringValue("FRFAX", "", true));
      pAdresseFournisseur.setNumeroTelex(pRecord.getStringValue("FRTLX", "", true));
      pAdresseFournisseur.setObservations(pRecord.getStringValue("FROBS", "", true));
      
      return pAdresseFournisseur;
    }
  }
  
  /**
   * Permet de créer en base de données les lignes de détail d'une configuration de catalogue fournisseur.
   * @param pConfigurationCatalogue Configuration de catalogue.
   */
  private void creerLesLignesCatalogueFournisseur(ConfigurationCatalogue pConfigurationCatalogue) {
    if (pConfigurationCatalogue.getListeChampCatalogue() == null) {
      return;
    }
    
    String CTETB = "";
    if (pConfigurationCatalogue.getIdEtablissement() != null) {
      CTETB = pConfigurationCatalogue.getIdEtablissement().getCodeEtablissement();
    }
    
    String CTCOL = "";
    String CTFRS = "";
    if (pConfigurationCatalogue.getIdFournisseur() != null) {
      CTCOL = pConfigurationCatalogue.getIdFournisseur().getCollectif().toString();
      CTFRS = pConfigurationCatalogue.getIdFournisseur().getNumero().toString();
    }
    
    String id = pConfigurationCatalogue.getId().getNumero().toString();
    
    for (ChampERP champERP : pConfigurationCatalogue.getListeChampERP()) {
      if (champERP.getListeChampCatalogue() != null) {
        String CTZON = "";
        if (champERP.getId() != null) {
          CTZON = champERP.getId().getCode();
        }
        
        String ordre1 = "0";
        String debut1 = "0";
        String fin1 = "0";
        String ordre2 = "0";
        String debut2 = "0";
        String fin2 = "0";
        
        int i = 0;
        for (ChampCatalogue champCatalogue : champERP.getListeChampCatalogue()) {
          if (i == 0) {
            if (champCatalogue != null) {
              ordre1 = champCatalogue.getOrdre().toString();
              debut1 = champCatalogue.getDebutDecoupe().toString();
              fin1 = champCatalogue.getFinDecoupe().toString();
            }
          }
          else if (i == 1) {
            if (champCatalogue != null) {
              ordre2 = champCatalogue.getOrdre().toString();
              debut2 = champCatalogue.getDebutDecoupe().toString();
              fin2 = champCatalogue.getFinDecoupe().toString();
            }
          }
          
          i++;
        }
        
        String requete = " INSERT INTO " + querymg.getLibrary() + '.' + EnumTableBDD.CATALOGUE_FOURNISSEUR_LIGNE
            + " (CTETB,CTCOL,CTFRS,CTNUM,CTZON,CTZ01,CTD01,CTF01,CTOPE,CTZ02,CTD02,CTF02) " + " VALUES ('" + CTETB + "','" + CTCOL + "','"
            + CTFRS + "','" + id + "','" + CTZON + "','" + ordre1 + "','" + debut1 + "','" + fin1 + "','','" + ordre2 + "','" + debut2
            + "','" + fin2 + "') ";
        if (querymg.requete(requete) < 1) {
          throw new MessageErreurException("La sauvegarde de la zone " + CTZON + " pour la configuration " + id + " est en échec");
        }
      }
    }
  }
}
