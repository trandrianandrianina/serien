/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

public abstract class FFD_Psemrtym extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_RYETB = 3;
  public static final int SIZE_RYNUM = 6;
  public static final int DECIMAL_RYNUM = 0;
  public static final int SIZE_RYSRVM = 15;
  public static final int SIZE_RYMAIL = 120;
  public static final int SIZE_RYSRVS = 60;
  public static final int SIZE_RYPORS = 5;
  public static final int DECIMAL_RYPORS = 0;
  public static final int SIZE_RYSRVE = 60;
  public static final int SIZE_RYPORE = 5;
  public static final int DECIMAL_RYPORE = 0;
  public static final int SIZE_RYCPTE = 60;
  public static final int SIZE_RYPASS = 60;
  public static final int SIZE_RYLIB1 = 60;
  public static final int SIZE_RYLIB2 = 60;
  public static final int SIZE_RYLIB3 = 60;
  public static final int SIZE_RYLIB4 = 60;
  public static final int SIZE_RYLIB5 = 60;
  public static final int SIZE_RYTOP1 = 1;
  public static final int SIZE_RYTOP2 = 1;
  public static final int SIZE_RYTOP3 = 1;
  public static final int SIZE_RYTOP4 = 1;
  public static final int SIZE_RYTOP5 = 1;
  public static final int SIZE_RYSMS1 = 60;
  public static final int SIZE_RYSMS2 = 60;
  public static final int SIZE_RYSMS3 = 60;
  public static final int SIZE_RYSMS4 = 60;
  public static final int SIZE_RYSMS5 = 60;
  public static final int SIZE_RYSMS6 = 60;
  public static final int SIZE_RYSMS7 = 60;
  public static final int SIZE_RYSMS8 = 60;
  public static final int SIZE_RYSMS9 = 60;
  public static final int SIZE_RYLIB6 = 60;
  public static final int SIZE_RYLIB7 = 60;
  public static final int SIZE_RYLIB8 = 60;
  public static final int SIZE_RYLIB9 = 60;
  
  // Variables fichiers
  protected String RYETB = null; // Code établissement
  protected int RYNUM = 0; // Numéro de contact
  protected String RYSRVM = null; // Alias Serveur de Mail
  protected String RYMAIL = null; // Adresse mail par défaut
  protected String RYSRVS = null; // Serveur Sortant
  protected int RYPORS = 0; // Port Sortant
  protected String RYSRVE = null; // Serveur Entrant
  protected int RYPORE = 0; // Port Entrant
  protected String RYCPTE = null; // Nom du Compte
  protected String RYPASS = null; // PassWord du compte
  protected String RYLIB1 = null; // Non utilisé
  protected String RYLIB2 = null; // @ecofax.fr envoi mail OVH
  protected String RYLIB3 = null; // N° lig. fax attrib.par OVH
  protected String RYLIB4 = null; // Mot passe attribué par OVH
  protected String RYLIB5 = null; // Non utilisé
  protected char RYTOP1 = ' '; // Logiciel de Mails
  protected char RYTOP2 = ' '; // Non utilisé
  protected char RYTOP3 = ' '; // Non utilisé
  protected char RYTOP4 = ' '; // Non utilisé
  protected char RYTOP5 = ' '; // Non utilisé
  protected String RYSMS1 = null; // Compte SMS OVH
  protected String RYSMS2 = null; // Utilisateur SMS OVH
  protected String RYSMS3 = null; // Mot de passe OVH
  protected String RYSMS4 = null; // N° expéditeur SMS OVH
  protected String RYSMS5 = null; // Non utilisé
  protected String RYSMS6 = null; // Non utilisé
  protected String RYSMS7 = null; // Non utilisé
  protected String RYSMS8 = null; // Non utilisé
  protected String RYSMS9 = null; // Non utilisé
  protected String RYLIB6 = null; // Non utilisé
  protected String RYLIB7 = null; // Non utilisé
  protected String RYLIB8 = null; // Non utilisé
  protected String RYLIB9 = null; // Non utilisé
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public FFD_Psemrtym(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    RYETB = null;
    RYNUM = 0;
    RYSRVM = null;
    RYMAIL = null;
    RYSRVS = null;
    RYPORS = 0;
    RYSRVE = null;
    RYPORE = 0;
    RYCPTE = null;
    RYPASS = null;
    RYLIB1 = null;
    RYLIB2 = null;
    RYLIB3 = null;
    RYLIB4 = null;
    RYLIB5 = null;
    RYTOP1 = ' ';
    RYTOP2 = ' ';
    RYTOP3 = ' ';
    RYTOP4 = ' ';
    RYTOP5 = ' ';
    RYSMS1 = null;
    RYSMS2 = null;
    RYSMS3 = null;
    RYSMS4 = null;
    RYSMS5 = null;
    RYSMS6 = null;
    RYSMS7 = null;
    RYSMS8 = null;
    RYSMS9 = null;
    RYLIB6 = null;
    RYLIB7 = null;
    RYLIB8 = null;
    RYLIB9 = null;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le rYETB
   */
  public String getRYETB() {
    return RYETB;
  }
  
  /**
   * @param rYETB le rYETB à définir
   */
  public void setRYETB(String rYETB) {
    RYETB = rYETB;
  }
  
  /**
   * @return le rYNUM
   */
  public int getRYNUM() {
    return RYNUM;
  }
  
  /**
   * @param rYNUM le rYNUM à définir
   */
  public void setRYNUM(int rYNUM) {
    RYNUM = rYNUM;
  }
  
  /**
   * @return le rYSRVM
   */
  public String getRYSRVM() {
    return RYSRVM;
  }
  
  /**
   * @param rYSRVM le rYSRVM à définir
   */
  public void setRYSRVM(String rYSRVM) {
    RYSRVM = rYSRVM;
  }
  
  /**
   * @return le rYMAIL
   */
  public String getRYMAIL() {
    return RYMAIL;
  }
  
  /**
   * @param rYMAIL le rYMAIL à définir
   */
  public void setRYMAIL(String rYMAIL) {
    RYMAIL = rYMAIL;
  }
  
  /**
   * @return le rYSRVS
   */
  public String getRYSRVS() {
    return RYSRVS;
  }
  
  /**
   * @param rYSRVS le rYSRVS à définir
   */
  public void setRYSRVS(String rYSRVS) {
    RYSRVS = rYSRVS;
  }
  
  /**
   * @return le rYPORS
   */
  public int getRYPORS() {
    return RYPORS;
  }
  
  /**
   * @param rYPORS le rYPORS à définir
   */
  public void setRYPORS(int rYPORS) {
    RYPORS = rYPORS;
  }
  
  /**
   * @return le rYSRVE
   */
  public String getRYSRVE() {
    return RYSRVE;
  }
  
  /**
   * @param rYSRVE le rYSRVE à définir
   */
  public void setRYSRVE(String rYSRVE) {
    RYSRVE = rYSRVE;
  }
  
  /**
   * @return le rYPORE
   */
  public int getRYPORE() {
    return RYPORE;
  }
  
  /**
   * @param rYPORE le rYPORE à définir
   */
  public void setRYPORE(int rYPORE) {
    RYPORE = rYPORE;
  }
  
  /**
   * @return le rYCPTE
   */
  public String getRYCPTE() {
    return RYCPTE;
  }
  
  /**
   * @param rYCPTE le rYCPTE à définir
   */
  public void setRYCPTE(String rYCPTE) {
    RYCPTE = rYCPTE;
  }
  
  /**
   * @return le rYPASS
   */
  public String getRYPASS() {
    return RYPASS;
  }
  
  /**
   * @param rYPASS le rYPASS à définir
   */
  public void setRYPASS(String rYPASS) {
    RYPASS = rYPASS;
  }
  
  /**
   * @return le rYLIB1
   */
  public String getRYLIB1() {
    return RYLIB1;
  }
  
  /**
   * @param rYLIB1 le rYLIB1 à définir
   */
  public void setRYLIB1(String rYLIB1) {
    RYLIB1 = rYLIB1;
  }
  
  /**
   * @return le rYLIB2
   */
  public String getRYLIB2() {
    return RYLIB2;
  }
  
  /**
   * @param rYLIB2 le rYLIB2 à définir
   */
  public void setRYLIB2(String rYLIB2) {
    RYLIB2 = rYLIB2;
  }
  
  /**
   * @return le rYLIB3
   */
  public String getRYLIB3() {
    return RYLIB3;
  }
  
  /**
   * @param rYLIB3 le rYLIB3 à définir
   */
  public void setRYLIB3(String rYLIB3) {
    RYLIB3 = rYLIB3;
  }
  
  /**
   * @return le rYLIB4
   */
  public String getRYLIB4() {
    return RYLIB4;
  }
  
  /**
   * @param rYLIB4 le rYLIB4 à définir
   */
  public void setRYLIB4(String rYLIB4) {
    RYLIB4 = rYLIB4;
  }
  
  /**
   * @return le rYLIB5
   */
  public String getRYLIB5() {
    return RYLIB5;
  }
  
  /**
   * @param rYLIB5 le rYLIB5 à définir
   */
  public void setRYLIB5(String rYLIB5) {
    RYLIB5 = rYLIB5;
  }
  
  /**
   * @return le rYTOP1
   */
  public char getRYTOP1() {
    return RYTOP1;
  }
  
  /**
   * @param rYTOP1 le rYTOP1 à définir
   */
  public void setRYTOP1(char rYTOP1) {
    RYTOP1 = rYTOP1;
  }
  
  /**
   * @return le rYTOP2
   */
  public char getRYTOP2() {
    return RYTOP2;
  }
  
  /**
   * @param rYTOP2 le rYTOP2 à définir
   */
  public void setRYTOP2(char rYTOP2) {
    RYTOP2 = rYTOP2;
  }
  
  /**
   * @return le rYTOP3
   */
  public char getRYTOP3() {
    return RYTOP3;
  }
  
  /**
   * @param rYTOP3 le rYTOP3 à définir
   */
  public void setRYTOP3(char rYTOP3) {
    RYTOP3 = rYTOP3;
  }
  
  /**
   * @return le rYTOP4
   */
  public char getRYTOP4() {
    return RYTOP4;
  }
  
  /**
   * @param rYTOP4 le rYTOP4 à définir
   */
  public void setRYTOP4(char rYTOP4) {
    RYTOP4 = rYTOP4;
  }
  
  /**
   * @return le rYTOP5
   */
  public char getRYTOP5() {
    return RYTOP5;
  }
  
  /**
   * @param rYTOP5 le rYTOP5 à définir
   */
  public void setRYTOP5(char rYTOP5) {
    RYTOP5 = rYTOP5;
  }
  
  /**
   * @return le rYSMS1
   */
  public String getRYSMS1() {
    return RYSMS1;
  }
  
  /**
   * @param rYSMS1 le rYSMS1 à définir
   */
  public void setRYSMS1(String rYSMS1) {
    RYSMS1 = rYSMS1;
  }
  
  /**
   * @return le rYSMS2
   */
  public String getRYSMS2() {
    return RYSMS2;
  }
  
  /**
   * @param rYSMS2 le rYSMS2 à définir
   */
  public void setRYSMS2(String rYSMS2) {
    RYSMS2 = rYSMS2;
  }
  
  /**
   * @return le rYSMS3
   */
  public String getRYSMS3() {
    return RYSMS3;
  }
  
  /**
   * @param rYSMS3 le rYSMS3 à définir
   */
  public void setRYSMS3(String rYSMS3) {
    RYSMS3 = rYSMS3;
  }
  
  /**
   * @return le rYSMS4
   */
  public String getRYSMS4() {
    return RYSMS4;
  }
  
  /**
   * @param rYSMS4 le rYSMS4 à définir
   */
  public void setRYSMS4(String rYSMS4) {
    RYSMS4 = rYSMS4;
  }
  
  /**
   * @return le rYSMS5
   */
  public String getRYSMS5() {
    return RYSMS5;
  }
  
  /**
   * @param rYSMS5 le rYSMS5 à définir
   */
  public void setRYSMS5(String rYSMS5) {
    RYSMS5 = rYSMS5;
  }
  
  /**
   * @return le rYSMS6
   */
  public String getRYSMS6() {
    return RYSMS6;
  }
  
  /**
   * @param rYSMS6 le rYSMS6 à définir
   */
  public void setRYSMS6(String rYSMS6) {
    RYSMS6 = rYSMS6;
  }
  
  /**
   * @return le rYSMS7
   */
  public String getRYSMS7() {
    return RYSMS7;
  }
  
  /**
   * @param rYSMS7 le rYSMS7 à définir
   */
  public void setRYSMS7(String rYSMS7) {
    RYSMS7 = rYSMS7;
  }
  
  /**
   * @return le rYSMS8
   */
  public String getRYSMS8() {
    return RYSMS8;
  }
  
  /**
   * @param rYSMS8 le rYSMS8 à définir
   */
  public void setRYSMS8(String rYSMS8) {
    RYSMS8 = rYSMS8;
  }
  
  /**
   * @return le rYSMS9
   */
  public String getRYSMS9() {
    return RYSMS9;
  }
  
  /**
   * @param rYSMS9 le rYSMS9 à définir
   */
  public void setRYSMS9(String rYSMS9) {
    RYSMS9 = rYSMS9;
  }
  
  /**
   * @return le rYLIB6
   */
  public String getRYLIB6() {
    return RYLIB6;
  }
  
  /**
   * @param rYLIB6 le rYLIB6 à définir
   */
  public void setRYLIB6(String rYLIB6) {
    RYLIB6 = rYLIB6;
  }
  
  /**
   * @return le rYLIB7
   */
  public String getRYLIB7() {
    return RYLIB7;
  }
  
  /**
   * @param rYLIB7 le rYLIB7 à définir
   */
  public void setRYLIB7(String rYLIB7) {
    RYLIB7 = rYLIB7;
  }
  
  /**
   * @return le rYLIB8
   */
  public String getRYLIB8() {
    return RYLIB8;
  }
  
  /**
   * @param rYLIB8 le rYLIB8 à définir
   */
  public void setRYLIB8(String rYLIB8) {
    RYLIB8 = rYLIB8;
  }
  
  /**
   * @return le rYLIB9
   */
  public String getRYLIB9() {
    return RYLIB9;
  }
  
  /**
   * @param rYLIB9 le rYLIB9 à définir
   */
  public void setRYLIB9(String rYLIB9) {
    RYLIB9 = rYLIB9;
  }
  
}
