/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Psemparm_ds_TG pour les TG
 * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
 */
public class Psemparm_ds_TG extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "TGLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGSNS"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD5"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF5"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN5"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGD6"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TGF6"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TGN6"));
    
    length = 200;
  }
}
