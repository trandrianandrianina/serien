/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.programs.contact;

import java.util.ArrayList;

import ri.serien.libas400.dao.gvm.database.files.FFD_Pgvmclim;
import ri.serien.libas400.dao.gvm.programs.client.M_Client;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;

public class GM_LienContactAvecTiers extends BaseGroupDB {
  /**
   * Constructeur
   * @param aquerymg
   */
  public GM_LienContactAvecTiers(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Controle qu'un contact soit lié à des tiers
   * @param contact
   * @return
   */
  public boolean checkContactWithoutLink(M_Contact contact) {
    if (contact == null) {
      msgErreur += "\nLa classe M_Contact est nulle";
      return false;
    }
    
    String requete = "select count(*) from " + queryManager.getLibrary() + ".PSEMRTLM where RLNUMT = " + contact.getRENUM() + " and RLETBT = '"
        + contact.getREETB() + "'";
    String resultat = queryManager.firstEnrgSelect(requete, 1);
    if (resultat == null) {
      return false;
    }
    else if (resultat.equals("0")) {
      return true;
    }
    return false;
  }
  
  /**
   * Lecture d'un lot de contacts pour un client
   * @param client
   * @return
   */
  public ArrayList<M_Contact> readContact4Client(M_Client client) {
    if (client == null) {
      msgErreur += "\nLa classe M_Client est nulle";
      return null;
    }
    return readContacts(M_LienContactAvecTiers.CLIENT, client.getCLETB(), client.getCLIandLIV());
  }
  
  /**
   * Lecture du contact principal pour un client
   * @param client
   * @return
   */
  public ArrayList<M_Contact> readContactPrincipal4Client(M_Client client) {
    if (client == null) {
      msgErreur += "\nLa classe M_Client est nulle";
      return null;
    }
    return readContactPrincipal(M_LienContactAvecTiers.CLIENT, client.getCLETB(), client.getCLIandLIV());
  }
  
  /**
   * Lecture du contact principal pour un client
   * @param etb
   * @param ind
   * @return
   */
  public Contact chargerContactPrincipalClient(IdClient pIdClient) {
    IdClient.controlerId(pIdClient, true);
    
    Contact contactprincipal = null;
    String ind = String.format("%0" + FFD_Pgvmclim.SIZE_CLCLI + "d%0" + FFD_Pgvmclim.SIZE_CLLIV + "d", pIdClient.getNumero(),
        pIdClient.getSuffixe());
    ArrayList<M_Contact> liste = readContactPrincipal(M_LienContactAvecTiers.CLIENT, pIdClient.getCodeEtablissement(), ind);
    if ((liste != null) && (liste.size() > 0)) {
      contactprincipal = liste.get(0).initContact();
      // contactprincipal.setContactPrincipal(true);
    }
    return contactprincipal;
  }
  
  /**
   * Lecture de la liste des contacts pour un client.
   */
  public ListeContact chargerListeContact(IdClient pIdClient) {
    ListeContact contacts = null;
    String cond = "RLCOD = '" + M_LienContactAvecTiers.CLIENT + "' and RLETB='" + pIdClient.getCodeEtablissement() + "' and RLIND='"
        + pIdClient.getIndicatif(IdClient.INDICATIF_NUM_SUF) + "'";
    ArrayList<M_Contact> liste = readInDatabase(cond);
    if ((liste != null) && (liste.size() > 0)) {
      contacts = new ListeContact();
      for (int i = 0; i < liste.size(); i++) {
        contacts.add(liste.get(i).initContact());
      }
    }
    else {
      return null;
    }
    return contacts;
  }
  
  /**
   * Lecture d'un lot de contacts pour un tiers
   * @param type
   * @param etb
   * @param ind
   * @return
   */
  public ArrayList<M_Contact> readContacts(char type, String etb, String ind) {
    if (etb == null) {
      msgErreur += "\nL'établissement est nul";
      return null;
    }
    if (ind == null) {
      msgErreur += "\nL'indicatif du tiers est nul";
      return null;
    }
    
    String cond = "RLCOD = '" + type + "' and RLETB='" + etb + "' and RLIND='" + ind + "'";
    return readInDatabase(cond);
  }
  
  /**
   * Lecture d'un lot de contacts pour un tiers
   * @param type
   * @param etb
   * @param ind
   * @return
   */
  public ArrayList<M_Contact> readContactPrincipal(char type, String etb, String ind) {
    if (etb == null) {
      msgErreur += "\nL'établissement est nul";
      return null;
    }
    if (ind == null) {
      msgErreur += "\nL'indicatif du tiers est nul";
      return null;
    }
    
    String cond = "RLCOD = '" + type + "' and RLETB='" + etb + "' and RLIND='" + ind + "' and RLIN1 = 'P'";
    return readInDatabase(cond);
  }
  
  /**
   * Lecture d'un lot de contacts
   * @param cond sans le "where"
   * @return
   */
  public ArrayList<M_Contact> readInDatabase(String cond) {
    String requete = "select rte.*, rtl.*  from " + queryManager.getLibrary() + ".PSEMRTLM rtl, " + queryManager.getLibrary()
        + ".PSEMRTEM rte where RENUM = RLNUMT and REETB = RLETBT ";
    if (!cond.trim().toLowerCase().startsWith("and")) {
      requete += " and " + cond;
    }
    else {
      requete += " " + cond;
    }
    return request4ReadContacts(requete);
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Requête permettant la lecture d'un lot de contacts
   * @param requete
   * @return
   */
  private ArrayList<M_Contact> request4ReadContacts(String requete) {
    
    if ((requete == null) || (requete.trim().length() == 0)) {
      msgErreur += "\nLa requête n'est pas correcte.";
      return null;
    }
    if (queryManager.getLibrary() == null) {
      msgErreur += "\nLa CURLIB n'est pas initialisée.";
      return null;
    }
    
    // Lecture de la base
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    if (listrcd == null) {
      return null;
    }
    // Chargement des classes avec les données de la base
    ArrayList<M_Contact> listc = new ArrayList<M_Contact>(listrcd.size());
    for (GenericRecord rcd : listrcd) {
      M_Contact c = new M_Contact(queryManager);
      // zones venant du PSEMTRLE
      c.initObject(rcd, true);
      // zones venant du PSEMTRLM
      M_LienContactAvecTiers lcat = new M_LienContactAvecTiers(queryManager);
      lcat.initObject(rcd, true);
      c.getListLienContactTiers().add(lcat);
      
      listc.add(c);
    }
    listrcd.clear();
    
    return listc;
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    queryManager = null;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
}
