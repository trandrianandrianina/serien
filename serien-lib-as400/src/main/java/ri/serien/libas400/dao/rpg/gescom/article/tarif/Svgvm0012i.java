/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.tarif;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0012i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PIDEV = 3;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PITTC = 1;
  public static final int SIZE_PITFA = 1;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 46;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PIART = 2;
  public static final int VAR_PIDEV = 3;
  public static final int VAR_PIDAT = 4;
  public static final int VAR_PITTC = 5;
  public static final int VAR_PITFA = 6;
  public static final int VAR_PIARR = 7;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement *
  private String piart = ""; // Code article *
  private String pidev = ""; // Code Devise éventuel
  private BigDecimal pidat = BigDecimal.ZERO; // Date
  private String pittc = ""; // 1 si TTC
  private String pitfa = ""; // Type de facturation si TTC
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400Text(SIZE_PIART), // Code article *
      new AS400Text(SIZE_PIDEV), // Code Devise éventuel
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date
      new AS400Text(SIZE_PITTC), // 1 si TTC
      new AS400Text(SIZE_PITFA), // Type de facturation si TTC
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pietb, piart, pidev, pidat, pittc, pitfa, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    piart = (String) output[2];
    pidev = (String) output[3];
    pidat = (BigDecimal) output[4];
    pittc = (String) output[5];
    pitfa = (String) output[6];
    piarr = (String) output[7];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPiart(String pPiart) {
    if (pPiart == null) {
      return;
    }
    piart = pPiart;
  }
  
  public String getPiart() {
    return piart;
  }
  
  public void setPidev(String pPidev) {
    if (pPidev == null) {
      return;
    }
    pidev = pPidev;
  }
  
  public String getPidev() {
    return pidev;
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPittc(Character pPittc) {
    if (pPittc == null) {
      return;
    }
    pittc = String.valueOf(pPittc);
  }
  
  public Character getPittc() {
    return pittc.charAt(0);
  }
  
  public void setPitfa(Character pPitfa) {
    if (pPitfa == null) {
      return;
    }
    pitfa = String.valueOf(pPitfa);
  }
  
  public Character getPitfa() {
    return pitfa.charAt(0);
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
