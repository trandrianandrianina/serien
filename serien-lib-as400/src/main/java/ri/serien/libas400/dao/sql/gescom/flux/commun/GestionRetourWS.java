/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun;

import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;

public class GestionRetourWS extends BaseGroupDB {
  
  public GestionRetourWS(QueryManager pQuerymg) {
    super(pQuerymg);
  }
  
  @Override
  public void dispose() {
  }
  
}
