/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;

public class TVA_Cde_EDI_GP extends EntiteEDI {
  private String taux = null;
  private String montant = null;
  
  public TVA_Cde_EDI_GP(ParametresFluxBibli pParams, String tx, String mt) {
    super(pParams);
    taux = tx;
    montant = mt;
    
    isValide = traitementContenu();
  }
  
  @Override
  protected boolean traitementContenu() {
    if (taux == null) {
      majError("[TVA_Cde_EDI_GP] taux null " + taux);
      return false;
    }
    else {
      try {
        float tauxFloat = Float.parseFloat(taux);
        // FO DIVISER PAR 10 ME DEMANDEZ PAS POURQUOI
        tauxFloat = tauxFloat / 10;
        
        taux = String.valueOf(tauxFloat);
      }
      catch (NumberFormatException e) {
        majError("[TVA_Cde_EDI_GP] traitementContenu() pb de parse de taux " + taux + " -> " + e.getMessage());
        return false;
      }
    }
    
    if (montant == null) {
      majError("[TVA_Cde_EDI_GP] montant null " + taux);
      return false;
    }
    
    return true;
  }
  
  public String getTaux() {
    return taux;
  }
  
  public void setTaux(String taux) {
    this.taux = taux;
  }
  
  public String getMontant() {
    return montant;
  }
  
  public void setMontant(String montant) {
    this.montant = montant;
  }
  
}
