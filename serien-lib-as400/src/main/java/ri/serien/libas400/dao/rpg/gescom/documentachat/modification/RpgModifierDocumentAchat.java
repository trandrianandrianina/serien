/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.modification;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.Reglements;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.document.ListeTva;
import ri.serien.libcommun.gescom.commun.document.ListeZonePersonnalisee;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgModifierDocumentAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGAM0024";
  
  /**
   * Appel du programme RPG qui va modifier un document.
   */
  public DocumentAchat modifierDocumentAchat(SystemeManager pSysteme, String pProfil, DocumentAchat pDocumentAchat) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    pProfil = Constantes.normerTexte(pProfil);
    if (pProfil.isEmpty()) {
      throw new MessageErreurException("Vous devez obligatoirement fournir le profil de l'utilisateur.");
    }
    DocumentAchat.controlerId(pDocumentAchat, true);
    IdFournisseur.controlerId(pDocumentAchat.getIdFournisseur(), true);
    
    // Préparation des paramètres du programme RPG
    Svgam0024i entree = new Svgam0024i();
    Svgam0024o sortie = new Svgam0024o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind("");
    entree.setPietb(pDocumentAchat.getId().getCodeEtablissement());
    entree.setPicod(pDocumentAchat.getId().getCodeEntete().getCode());
    entree.setPinum(pDocumentAchat.getId().getNumero());
    entree.setPisuf(pDocumentAchat.getId().getSuffixe());
    
    entree.setPicre(pDocumentAchat.getDateCreation());
    entree.setPidat(pDocumentAchat.getDateDernierTraitement());
    entree.setPihom(pDocumentAchat.getDateHomologation());
    entree.setPirec(pDocumentAchat.getDateReception());
    entree.setPifac(pDocumentAchat.getDateFacturation());
    entree.setPinfa(pDocumentAchat.getNumeroFacture());
    /* non utilisées pour l'instant
    entree.pinls = ;
    entree.pipli = ;
    entree.pidli = ;
    entree.pinli = ;
    entree.pitlv = ;
    entree.pitll = ;
    entree.pidsu = ;
    */
    entree.setPirfl(pDocumentAchat.getReferenceInterne());
    if (pDocumentAchat.getCodeEtat() != null) {
      entree.setPieta(pDocumentAchat.getCodeEtat().getCode());
    }
    entree.setPitfc(pDocumentAchat.getTopFournisseurDeCommande());
    entree.setPitt(pDocumentAchat.getTopMontantsFactureForces());
    entree.setPinrg(pDocumentAchat.getRangReglement());
    
    // Alimentation des champs concernant les TVA
    ListeTva liste = pDocumentAchat.getListeTva();
    entree.setPitva1(liste.getNumeroTVA1());
    entree.setPitva2(liste.getNumeroTVA2());
    entree.setPitva3(liste.getNumeroTVA3());
    entree.setPisl1(liste.getMontantSoumisTVA1());
    entree.setPisl2(liste.getMontantSoumisTVA2());
    entree.setPisl3(liste.getMontantSoumisTVA3());
    entree.setPitl1(liste.getMontantTVA1());
    entree.setPitl2(liste.getMontantTVA2());
    entree.setPitl3(liste.getMontantTVA3());
    entree.setPitv1(liste.getTauxTVA1());
    entree.setPitv2(liste.getTauxTVA2());
    entree.setPitv3(liste.getTauxTVA3());
    
    entree.setPirlv(pDocumentAchat.getTopReleve());
    entree.setPinex(pDocumentAchat.getNombreExemplaire());
    // entree.piexc = ;
    // entree.piexe = ;
    entree.setPinrr(pDocumentAchat.getNombreReglementsReels());
    entree.setPisgn(pDocumentAchat.getSigneGere());
    entree.setPiliv(pDocumentAchat.getTopLivraison());
    entree.setPicpt(pDocumentAchat.getTopComptabilisation());
    entree.setPipgc(pDocumentAchat.getTopPrixGaranti());
    entree.setPichf(pDocumentAchat.getTopChiffrage());
    entree.setPiina(pDocumentAchat.getTopAffectation());
    entree.setPicol(pDocumentAchat.getIdFournisseur().getCollectif());
    entree.setPifrs(pDocumentAchat.getIdFournisseur().getNumero());
    entree.setPidlp(ConvertDate.dateToDb2(pDocumentAchat.getDateLivraisonPrevue()));
    if (pDocumentAchat.getListeIdDocumentOrigine() != null && !pDocumentAchat.getListeIdDocumentOrigine().isEmpty()) {
      entree.setPinum0(pDocumentAchat.getListeIdDocumentOrigine().get(0).getNumero());
      entree.setPisuf0(pDocumentAchat.getListeIdDocumentOrigine().get(0).getSuffixe());
    }
    entree.setPiavr(pDocumentAchat.getCodeAvoir());
    entree.setPitfa(pDocumentAchat.getCodeTypeFacturation());
    entree.setPinat(pDocumentAchat.getCodeNatureAchat());
    if (pDocumentAchat.getReglement() != null) {
      Reglements reglement = pDocumentAchat.getReglement();
      entree.setPie1g(ConvertDate.dateToDb2(reglement.getDateEcheance1()));
      entree.setPie2g(ConvertDate.dateToDb2(reglement.getDateEcheance2()));
      entree.setPie3g(ConvertDate.dateToDb2(reglement.getDateEcheance3()));
      entree.setPie4g(ConvertDate.dateToDb2(reglement.getDateEcheance4()));
      entree.setPir1g(reglement.getMontant1());
      entree.setPir2g(reglement.getMontant2());
      entree.setPir3g(reglement.getMontant3());
      entree.setPir4g(reglement.getMontant4());
      if (reglement.getModeReglement1() != null) {
        entree.setPirg1(reglement.getModeReglement1().getId().getCode());
      }
      entree.setPiec1(reglement.getCodeEcheance1());
      entree.setPipc1(reglement.getPourcentageReglement1());
      if (reglement.getModeReglement2() != null) {
        entree.setPirg2(reglement.getModeReglement2().getId().getCode());
      }
      entree.setPiec2(reglement.getCodeEcheance2());
      entree.setPipc2(reglement.getPourcentageReglement2());
      if (reglement.getModeReglement3() != null) {
        entree.setPirg3(reglement.getModeReglement3().getId().getCode());
      }
      entree.setPiec3(reglement.getCodeEcheance3());
      entree.setPipc3(reglement.getPourcentageReglement3());
      if (reglement.getModeReglement4() != null) {
        entree.setPirg4(reglement.getModeReglement4().getId().getCode());
      }
      entree.setPiec4(reglement.getCodeEcheance4());
      entree.setPipc4(reglement.getPourcentageReglement4());
    }
    entree.setPithtl(pDocumentAchat.getTotalHTLignes());
    entree.setPiesc(pDocumentAchat.getPourcentageEscompte());
    entree.setPimes(pDocumentAchat.getMontantEscompte());
    if (pDocumentAchat.getIdMagasinSortie() != null) {
      entree.setPimag(pDocumentAchat.getIdMagasinSortie().getCode());
    }
    entree.setPirbl(pDocumentAchat.getReferenceBonLivraison());
    entree.setPisan(pDocumentAchat.getCodeSectionAnalytique());
    entree.setPiact(pDocumentAchat.getCodeAffaire());
    entree.setPillv(pDocumentAchat.getCodeLieuLivraison());
    if (pDocumentAchat.getIdAcheteur() != null) {
      entree.setPiach(pDocumentAchat.getIdAcheteur().getCode());
    }
    entree.setPicja(pDocumentAchat.getCodeJournalAchat());
    entree.setPidev(pDocumentAchat.getCodeDevise());
    entree.setPichg(pDocumentAchat.getTauxDeChange());
    entree.setPicpr(pDocumentAchat.getCoefficientAchat());
    entree.setPibas(pDocumentAchat.getBaseDevise());
    entree.setPidos(pDocumentAchat.getCodeDossierApprovisionnement());
    entree.setPicnt(pDocumentAchat.getCodeContainer());
    entree.setPipds(pDocumentAchat.getPoids());
    entree.setPivol(pDocumentAchat.getVolume());
    entree.setPimta(pDocumentAchat.getMontantFraisRepartir());
    if (pDocumentAchat.getListeRemiseLigne() != null) {
      ListeRemise listeRemiseLigne = pDocumentAchat.getListeRemiseLigne();
      entree.setPirem1(listeRemiseLigne.getPourcentageRemise1());
      entree.setPirem2(listeRemiseLigne.getPourcentageRemise2());
      entree.setPirem3(listeRemiseLigne.getPourcentageRemise3());
      entree.setPirem4(listeRemiseLigne.getPourcentageRemise4());
      entree.setPirem5(listeRemiseLigne.getPourcentageRemise5());
      entree.setPirem6(listeRemiseLigne.getPourcentageRemise6());
      entree.setPitrl(listeRemiseLigne.getTypeRemise());
      entree.setPibrl(listeRemiseLigne.getBaseRemise());
    }
    if (pDocumentAchat.getListeRemisePied() != null) {
      ListeRemise listeRemisePied = pDocumentAchat.getListeRemisePied();
      entree.setPirp1(listeRemisePied.getPourcentageRemise1());
      entree.setPirp2(listeRemisePied.getPourcentageRemise2());
      entree.setPirp3(listeRemisePied.getPourcentageRemise3());
      entree.setPirp4(listeRemisePied.getPourcentageRemise4());
      entree.setPirp5(listeRemisePied.getPourcentageRemise5());
      entree.setPirp6(listeRemisePied.getPourcentageRemise6());
      entree.setPitrp(listeRemisePied.getTypeRemise());
    }
    if (pDocumentAchat.getListeTopPersonnalisable() != null) {
      ListeZonePersonnalisee listeTopPersonnalisable = pDocumentAchat.getListeTopPersonnalisable();
      entree.setPitp1(listeTopPersonnalisable.getTopPersonnalisation1());
      entree.setPitp2(listeTopPersonnalisable.getTopPersonnalisation2());
      entree.setPitp3(listeTopPersonnalisable.getTopPersonnalisation3());
      entree.setPitp4(listeTopPersonnalisable.getTopPersonnalisation4());
      entree.setPitp5(listeTopPersonnalisable.getTopPersonnalisation5());
    }
    entree.setPiin1(pDocumentAchat.getCodeLitige());
    // entree.setPiin2(pDocument.getCodeLitige());
    if (pDocumentAchat.isReceptionDefinitive()) {
      entree.setPiin3('1');
    }
    if (pDocumentAchat.getComptabilisationEnStocksFlottants()) {
      entree.setPiin4('1');
    }
    if (pDocumentAchat.getBlocageComptabilisation()) {
      entree.setPiin5('1');
    }
    entree.setPicct(pDocumentAchat.getCodeContrat());
    entree.setPidat1(pDocumentAchat.getDateConfirmationFournisseur());
    entree.setPirbc(pDocumentAchat.getReferenceCommande());
    entree.setPiint1(pDocumentAchat.getDateIntermediaire1());
    entree.setPiint2(pDocumentAchat.getDateIntermediaire2());
    entree.setPietai(pDocumentAchat.getCodeEtatIntermediaire());
    if (pDocumentAchat.getTraiteParCommandeDilicom()) {
      entree.setPidili('1');
    }
    if (pDocumentAchat.getBlocageCommande()) {
      entree.setPiin6('1');
    }
    entree.setPiin7(pDocumentAchat.getCodeScenarioDates());
    entree.setPiin8(pDocumentAchat.getCodeEtatFraisFixes());
    if (pDocumentAchat.getCodeTypeCommande() != null) {
      entree.setPiin9(pDocumentAchat.getCodeTypeCommande().getCode());
    }
    // L'adresse fournisseur
    if (pDocumentAchat.getAdresseFournisseur() != null) {
      Adresse adresse = pDocumentAchat.getAdresseFournisseur();
      entree.setPinom2(adresse.getNom());
      entree.setPicpl2(adresse.getComplementNom());
      entree.setPirue2(adresse.getRue());
      entree.setPiloc2(adresse.getLocalisation());
      entree.setPicdp2(adresse.getCodePostalFormate());
      entree.setPivil2(adresse.getVille());
    }
    
    entree.setPipor0(pDocumentAchat.getMontantPortTheorique());
    entree.setPipor1(pDocumentAchat.getMontantPortFacture());
    if (pDocumentAchat.isFraisPortFournisseurFacture()) {
      entree.setPiin10(' ');
    }
    else {
      entree.setPiin10('1');
    }
    
    if (pDocumentAchat.getModeEnlevement()) {
      entree.setPienv('E');
    }
    else {
      entree.setPienv('L');
    }
    entree.setPicntct(pDocumentAchat.getIndicatifContact());
    if (pDocumentAchat.getIdDocumentPourExtraction() != null) {
      entree.setPicodx(pDocumentAchat.getIdDocumentPourExtraction().getCodeEntete().getCode());
      entree.setPietbx(pDocumentAchat.getIdDocumentPourExtraction().getCodeEtablissement());
      entree.setPinumx(pDocumentAchat.getIdDocumentPourExtraction().getNumero());
      entree.setPisufx(pDocumentAchat.getIdDocumentPourExtraction().getSuffixe());
    }
    
    // L'adresse de livraison ne concerne que les commandes usine
    if (pDocumentAchat.getAdresseLivraison() != null && pDocumentAchat.isDirectUsine()) {
      entree.setPillv("*GVM");
      Adresse adresse = pDocumentAchat.getAdresseLivraison();
      entree.setPinom3(adresse.getNom());
      entree.setPicpl3(adresse.getComplementNom());
      entree.setPirue3(adresse.getRue());
      entree.setPiloc3(adresse.getLocalisation());
      entree.setPicdp3(adresse.getCodePostalFormate());
      entree.setPivil3(adresse.getVille());
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      // Entrée
      // Sortie
    }
    return pDocumentAchat;
  }
}
