/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.negociation;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgSauverNegociationAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0010";
  
  /**
   * Appel du programme RPG qui va écrire une négociation.
   */
  public void sauverNegociationAchat(SystemeManager pSysteme, ConditionAchat pCNA) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pCNA == null) {
      throw new MessageErreurException("Le paramètre pCNA du service est à null.");
    }
    IdFournisseur.controlerId(pCNA.getIdFournisseur(), true);
    
    // Préparation des paramètres du programme RPG
    Svgvx0010i entree = new Svgvx0010i();
    Svgvx0010o sortie = new Svgvx0010o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pCNA.getIdEtablissement().getCodeEtablissement());
    entree.setPicod(pCNA.getCodeERL());
    entree.setPinum(pCNA.getNumeroBon());
    entree.setPisuf(pCNA.getSuffixeBon());
    entree.setPinli(pCNA.getNumeroLigne());
    entree.setPiart(pCNA.getIdArticle().getCodeArticle());
    entree.setPicre(ConvertDate.dateToDb2(pCNA.getDateCreation()));
    entree.setPitrt(ConvertDate.dateToDb2(pCNA.getDateTraitement()));
    entree.setPimod(ConvertDate.dateToDb2(pCNA.getDateModification()));
    entree.setPidap(ConvertDate.dateToDb2(pCNA.getDateApplication()));
    entree.setPityp(pCNA.getTypeCondition().getCode());
    entree.setPicol(pCNA.getIdFournisseur().getCollectif());
    entree.setPifrs(pCNA.getIdFournisseur().getNumero());
    entree.setPicpv(pCNA.getTopCalculPrixVente());
    entree.setPirem1(pCNA.getPourcentageRemise1());
    entree.setPirem2(pCNA.getPourcentageRemise2());
    entree.setPirem3(pCNA.getPourcentageRemise3());
    entree.setPirem4(pCNA.getPourcentageRemise4());
    entree.setPirem5(pCNA.getPourcentageRemise5());
    entree.setPirem6(pCNA.getPourcentageRemise6());
    entree.setPipra(pCNA.getPrixAchatBrutHT());
    if (pCNA.getIdUniteAchat() != null) {
      entree.setPiuna(pCNA.getIdUniteAchat().getCode());
    }
    entree.setPinua(pCNA.getConditionnement());
    entree.setPikac(pCNA.getCoefficientUAparUCA());
    entree.setPidel(pCNA.getDelaiLivraison());
    entree.setPiref(pCNA.getReferenceFournisseur());
    entree.setPiunc(pCNA.getUniteCommande());
    entree.setPiksc(pCNA.getCoefficientUSparUCA());
    entree.setPidev(pCNA.getCodeDevise());
    entree.setPiqmi(pCNA.getQuantiteMinimumCommande());
    entree.setPiqec(pCNA.getQuantiteEconomique());
    entree.setPikpv(pCNA.getCoefficientCalculPrixVente());
    entree.setPiddp(ConvertDate.dateToDb2(pCNA.getDateDernierPrix()));
    entree.setPirga(pCNA.getRegroupementAchat());
    entree.setPidels(pCNA.getDelaiSupplementaire());
    entree.setPikpr(pCNA.getCoefficientRemise());
    entree.setPiin1(pCNA.getOriginePrix());
    entree.setPiin2(pCNA.getNoteQualite());
    entree.setPiin3(pCNA.getTypeFournisseur());
    entree.setPiin4(pCNA.getRemiseCalculPVminimum());
    entree.setPitrl(pCNA.getTypeRemiseLigne());
    entree.setPirfc(pCNA.getReferenceConstructeur());
    entree.setPigcd(pCNA.getGencodFournisseur());
    entree.setPidaf(ConvertDate.dateToDb2(pCNA.getDateLimiteValidite()));
    entree.setPilib(pCNA.getLibelle());
    entree.setPiopa(pCNA.getOrigine());
    entree.setPiin7(pCNA.getDelaiJours());
    entree.setPipdi(pCNA.getPrixDistributeur());
    entree.setPikap(pCNA.getCoefficientApprocheFixe());
    entree.setPiprs(pCNA.getPrixRevientStandardHT());
    entree.setPipgn(pCNA.getTopPrixNegocie());
    entree.setPifv1(pCNA.getMontantAuPoidsPortHT());
    entree.setPifk1(pCNA.getPoidsPort());
    entree.setPifp1(pCNA.getPourcentagePort());
    entree.setPifu1(pCNA.getUniteFrais1());
    entree.setPifv2(pCNA.getFraisEnValeur2());
    entree.setPifk2(pCNA.getFraisEnCoeff2());
    entree.setPifp2(pCNA.getFraisExploitation());
    entree.setPifu2(pCNA.getUniteFrais2());
    entree.setPifv3(pCNA.getMontantConditionnement());
    entree.setPifk3(pCNA.getPourcentageTaxeOuMajoration());
    entree.setPifu3(pCNA.getUniteFrais3());
    entree.setPidcc(pCNA.getNombreDecimalesUCA());
    entree.setPidca(pCNA.getNombreDecimalesUA());
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
}
