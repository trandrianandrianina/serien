/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lienligne;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvx0022i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PITYPO = 1;
  public static final int SIZE_PICODO = 1;
  public static final int SIZE_PIETBO = 3;
  public static final int SIZE_PINUMO = 6;
  public static final int DECIMAL_PINUMO = 0;
  public static final int SIZE_PISUFO = 1;
  public static final int DECIMAL_PISUFO = 0;
  public static final int SIZE_PINLIO = 4;
  public static final int DECIMAL_PINLIO = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 27;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PITYPO = 1;
  public static final int VAR_PICODO = 2;
  public static final int VAR_PIETBO = 3;
  public static final int VAR_PINUMO = 4;
  public static final int VAR_PISUFO = 5;
  public static final int VAR_PINLIO = 6;
  public static final int VAR_PIARR = 7;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pitypo = ""; // A=ACHAT, V=VENTE
  private String picodo = ""; // Code ERL
  private String pietbo = ""; // Code Etablissement
  private BigDecimal pinumo = BigDecimal.ZERO; // Numéro
  private BigDecimal pisufo = BigDecimal.ZERO; // Suffixe
  private BigDecimal pinlio = BigDecimal.ZERO; // Numéro de Ligne
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PITYPO), // A=ACHAT, V=VENTE
      new AS400Text(SIZE_PICODO), // Code ERL
      new AS400Text(SIZE_PIETBO), // Code Etablissement
      new AS400ZonedDecimal(SIZE_PINUMO, DECIMAL_PINUMO), // Numéro
      new AS400ZonedDecimal(SIZE_PISUFO, DECIMAL_PISUFO), // Suffixe
      new AS400ZonedDecimal(SIZE_PINLIO, DECIMAL_PINLIO), // Numéro de Ligne
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pitypo, picodo, pietbo, pinumo, pisufo, pinlio, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pitypo = (String) output[1];
    picodo = (String) output[2];
    pietbo = (String) output[3];
    pinumo = (BigDecimal) output[4];
    pisufo = (BigDecimal) output[5];
    pinlio = (BigDecimal) output[6];
    piarr = (String) output[7];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPitypo(Character pPitypo) {
    if (pPitypo == null) {
      return;
    }
    pitypo = String.valueOf(pPitypo);
  }
  
  public Character getPitypo() {
    return pitypo.charAt(0);
  }
  
  public void setPicodo(Character pPicodo) {
    if (pPicodo == null) {
      return;
    }
    picodo = String.valueOf(pPicodo);
  }
  
  public Character getPicodo() {
    return picodo.charAt(0);
  }
  
  public void setPietbo(String pPietbo) {
    if (pPietbo == null) {
      return;
    }
    pietbo = pPietbo;
  }
  
  public String getPietbo() {
    return pietbo;
  }
  
  public void setPinumo(BigDecimal pPinumo) {
    if (pPinumo == null) {
      return;
    }
    pinumo = pPinumo.setScale(DECIMAL_PINUMO, RoundingMode.HALF_UP);
  }
  
  public void setPinumo(Integer pPinumo) {
    if (pPinumo == null) {
      return;
    }
    pinumo = BigDecimal.valueOf(pPinumo);
  }
  
  public Integer getPinumo() {
    return pinumo.intValue();
  }
  
  public void setPisufo(BigDecimal pPisufo) {
    if (pPisufo == null) {
      return;
    }
    pisufo = pPisufo.setScale(DECIMAL_PISUFO, RoundingMode.HALF_UP);
  }
  
  public void setPisufo(Integer pPisufo) {
    if (pPisufo == null) {
      return;
    }
    pisufo = BigDecimal.valueOf(pPisufo);
  }
  
  public Integer getPisufo() {
    return pisufo.intValue();
  }
  
  public void setPinlio(BigDecimal pPinlio) {
    if (pPinlio == null) {
      return;
    }
    pinlio = pPinlio.setScale(DECIMAL_PINLIO, RoundingMode.HALF_UP);
  }
  
  public void setPinlio(Integer pPinlio) {
    if (pPinlio == null) {
      return;
    }
    pinlio = BigDecimal.valueOf(pPinlio);
  }
  
  public Integer getPinlio() {
    return pinlio.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
