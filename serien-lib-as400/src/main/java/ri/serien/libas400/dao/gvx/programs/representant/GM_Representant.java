/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs.representant;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.representant.recherche.RequetesRechercheRepresentants;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.representant.CriteresRechercheRepresentant;
import ri.serien.libcommun.gescom.commun.representant.ListeRepresentant;
import ri.serien.libcommun.gescom.commun.representant.Representant;

public class GM_Representant extends BaseGroupDB {
  // Constantes
  private static final String DB2FILE_NAME = "PGVMREPM";
  
  // Variables
  // private SystemManager system = null;
  private String etablissement = "";
  private M_Representant representant = new M_Representant(queryManager);
  
  public GM_Representant(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  public void initialization(String etb) {
    // Initialisation des variables
    representant.initialization();
    
    // Initialisation de l'établissement
    setEtablissement(etb);
  }
  
  /**
   * Lecture d'un lot de representants.
   * @param cond sans le "where"
   * @return
   *
   */
  public M_Representant[] readInDatabase(String cond) {
    String requete = "select * from " + queryManager.getLibrary() + "." + DB2FILE_NAME + " rep where TRIM(RPNOM) <> ''";
    if ((cond != null) && (!cond.trim().equals(""))) {
      if (!cond.trim().toLowerCase().startsWith("and")) {
        requete += " and " + cond;
      }
      else {
        requete += " " + cond;
      }
    }
    return request4ReadRepresentant(requete);
  }
  
  /**
   * Requête permettant la lecture d'un lot de representants.
   * @param requete
   * @return
   *
   */
  public M_Representant[] request4ReadRepresentant(String requete) {
    if ((requete == null) || (requete.trim().length() == 0)) {
      msgErreur += "\nLa requête n'est pas correcte.";
      return null;
    }
    if (queryManager.getLibrary() == null) {
      msgErreur += "\nLa CURLIB n'est pas initialisée.";
      return null;
    }
    
    // Lecture de la base afin de récupérer les actions commerciales
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    if (listrcd == null) {
      return null;
    }
    // Chargement des classes avec les données de la base
    int i = 0;
    M_Representant[] listrep = new M_Representant[listrcd.size()];
    for (GenericRecord rcd : listrcd) {
      M_Representant rep = new M_Representant(queryManager);
      rep.initObject(rcd, true);
      
      listrep[i++] = rep;
    }
    
    return listrep;
  }
  
  /**
   * Retourne une liste de representants en fonction des critères passés en paramètre
   * @param criteres
   * @return
   *
   */
  public ListeRepresentant chargerListeRepresentant(CriteresRechercheRepresentant criteres) {
    // Préparation de la requête
    String requete = RequetesRechercheRepresentants.getRequete(queryManager.getLibrary(), criteres);
    if (requete == null) {
      return null;
    }
    // Lancement de la requête
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    if (listrcd == null) {
      return null;
    }
    
    ListeRepresentant listeRepresentant = new ListeRepresentant();
    for (GenericRecord rcd : listrcd) {
      M_Representant rep = new M_Representant(queryManager);
      if (rcd != null) {
        rep.initObject(rcd, true);
        Representant representant = rep.initRepresentant(CriteresRechercheRepresentant.RECHERCHE_COMPTOIR);
        listeRepresentant.add(representant);
      }
    }
    return listeRepresentant;
  }
  
  /**
   * Libère les ressources.
   */
  @Override
  public void dispose() {
    queryManager = null;
    // system = null;
    
    if (representant != null) {
      representant.dispose();
    }
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  // -- Accesseurs -----------------------------------------------------------
  
  /**
   * @return le etablissement
   */
  public String getEtablissement() {
    return etablissement;
  }
  
  /**
   * @param etablissement le etablissement à définir
   */
  public void setEtablissement(String etablissement) {
    if (etablissement == null) {
      this.etablissement = "";
    }
    else {
      this.etablissement = etablissement;
    }
  }
  
  /**
   * @return le system
   *
   *         public SystemManager getSystem()
   *         {
   *         return system;
   *         }
   *
   *         /**
   * @param system le system à définir
   *
   *          public void setSystem(SystemManager system)
   *          {
   *          this.system = system;
   *          }
   */
  
}
