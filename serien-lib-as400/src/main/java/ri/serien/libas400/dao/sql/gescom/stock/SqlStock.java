/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.stock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.stock.CritereStock;
import ri.serien.libcommun.gescom.commun.stock.IdStock;
import ri.serien.libcommun.gescom.commun.stock.ListeStock;
import ri.serien.libcommun.gescom.commun.stock.Stock;
import ri.serien.libcommun.gescom.commun.stock.StockDetaille;
import ri.serien.libcommun.gescom.commun.stockattenducommande.CritereStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockattenducommande.EnumTypeAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockattenducommande.IdStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockattenducommande.ListeStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockattenducommande.StockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockmouvement.CritereStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.EnumOrigineStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.EnumTypeStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.IdStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.ListeStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.StockMouvement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Couche d'accès aux données pour les classes métiers Stock et StockDetaille.
 */
public class SqlStock {
  // Variables
  protected QueryManager queryManager = null;
  
  /**
   * Constructeur.
   */
  public SqlStock(QueryManager pQueryManager) {
    queryManager = pQueryManager;
  }
  
  /**
   * Charger le stock détaillé d'un article pour un magasin ou un établissement.
   * 
   * Cela charge, pour le magasin ou l'établissement fourni en paramètre :
   * - le stock,
   * - le stock de tous les magasins de l'établissement,
   * - la liste des attendus/commandés,
   * - la liste des mouvements de stock.
   * 
   * Noter qu'on pourrait déduire le stock du magasin ou le stock de l'établissement à partir du stock détaillé par magasin chargé
   * juste après mais on préfère utiliser la méthode standard pour récupérer le stock afin de faire des recoupements et détecter
   * les écarts éventuels entre les différentes algorithmes.
   * 
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pIdMagasin Identifiant du magasin.
   * @param pIdArticle Identifiant de l'article.
   * @return Stock détaillé de l'article.
   */
  public StockDetaille chargerStockDetaille(IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, IdArticle pIdArticle) {
    // Charger le stock de l'article (pour le magasin ou pour l'établissement)
    // Si on ne peut pas charger le stock on ne poursuit pas le chargement du stock détaillé
    CritereStock critereStock = new CritereStock();
    critereStock.setIdEtablissement(pIdEtablissement);
    critereStock.setIdMagasin(pIdMagasin);
    critereStock.setIdArticle(pIdArticle);
    ListeStock listeStock = chargerListeStock(critereStock);
    if (listeStock == null || listeStock.size() != 1) {
      return null;
    }
    
    // Charger la date minimumu de prochain réappro possible
    Date dateMiniReappro = chargerDateMiniReappro(pIdArticle);
    
    // Charger le stock de l'article pour tous les magasins de l'établissement
    critereStock = new CritereStock();
    critereStock.setIdEtablissement(pIdEtablissement);
    critereStock.setDetailParMagasin(true);
    critereStock.setIdArticle(pIdArticle);
    ListeStock listeStockMagasin = chargerListeStock(critereStock);
    
    // Charger les attendus et commandés
    CritereStockAttenduCommande critereStockAttenduCommande = new CritereStockAttenduCommande();
    critereStockAttenduCommande.setIdEtablissement(pIdEtablissement);
    critereStockAttenduCommande.setIdMagasin(pIdMagasin);
    critereStockAttenduCommande.setIdArticle(pIdArticle);
    ListeStockAttenduCommande listeStockAttenduCommande = chargerListeStockAttenduCommande(critereStockAttenduCommande);
    
    // Renseigner les mouvements de stock
    CritereStockMouvement critereStockMouvement = new CritereStockMouvement();
    critereStockMouvement.setIdEtablissement(pIdEtablissement);
    critereStockMouvement.setIdMagasin(pIdMagasin);
    critereStockMouvement.setIdArticle(pIdArticle);
    ListeStockMouvement listeStockMouvement = chargerListeStockMouvement(critereStockMouvement);
    
    // Renseigner le stock détaillé à retourner
    StockDetaille stockDetaille = new StockDetaille(listeStock.get(0).getId());
    stockDetaille.setDateMiniReappro(dateMiniReappro);
    stockDetaille.setStock(listeStock.get(0));
    stockDetaille.setListeStockMagasin(listeStockMagasin);
    stockDetaille.setListeStockAttenduCommande(listeStockAttenduCommande);
    stockDetaille.setListeStockMouvement(listeStockMouvement);
    
    // Calculer le stock avant rupture
    stockDetaille.calculerStock();
    return stockDetaille;
  }
  
  /**
   * Charger la liste des stocks suivant les critères de recherche.
   * 
   * @param pCritereStock Critères de recherche des stocks.
   * @return Liste des stocks correspondants aux critères de recherche.
   */
  public ListeStock chargerListeStock(CritereStock pCritereStock) {
    // Contrôler les paramètres
    if (pCritereStock == null) {
      throw new MessageErreurException("Impossible de charger le stock car les critères de recherche sont invalides.");
    }
    if (pCritereStock.getListeIdArticle() == null || pCritereStock.getListeIdArticle().isEmpty()) {
      throw new MessageErreurException("Impossible de charger le stock si aucun article n'est défini.");
    }
    
    // Générer la requête SQL
    RequeteSql requeteSql = new RequeteSql();
    
    // Tester d'il faut charger le stock d'un seul magasin ou s'il faut charger le stock de tous les magasins d'un établissement
    if (pCritereStock.getIdMagasin() != null || pCritereStock.isDetailParMagasin()) {
      // Préparer le groupe sur les articles pour la table PGVMALAM
      for (IdArticle idArticle : pCritereStock.getListeIdArticle()) {
        requeteSql.ajouterValeurAuGroupe("groupeArticle", idArticle.getCodeArticle());
      }
      
      // Sous-requête sur la table PGVMALAM (commandes affectées sur achats)
      // Cela permet de faire la somme des quantités commandées liées à des achats par magasin.
      // La jointure avec les lignes de ventes permet de ne sélectionner que les commandes validées ou réservées (L1TOP = 1 et 3).
      // Le groupement par établissement, magasin et article permet d'avoir le cumul par magasin.
      // Le champ AATOE permet de connaître le type d'affectation sur achat :
      // - G = GBA (à compter)
      // - A = Affectation sur achat existant (à compter)
      // - S = Affectation sur stock existant (à ne pas compter)
      // Le champ L1ERL permet de savoir si l'article est géré en stock (C) ou non (S). Noté qu'il existe aussi la champ LAERL côté achat
      // mais les deux champs sont sensés être égaux.
      // Les champs AACOS et AACOE doivent être égaux à E pour refléter que les liés sur achats correspondent bien à des lignes de
      // commandes non réceptionnées.
      requeteSql.ajouter("WITH ALA AS (");
      requeteSql.ajouter("SELECT");
      requeteSql.ajouter("  AAETB,");
      requeteSql.ajouter("  AAMAG,");
      requeteSql.ajouter("  AAART,");
      requeteSql.ajouter("  SUM(AAQTE) AS QTEALA");
      requeteSql.ajouter("FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.LIEN_LIGNE_VENTE_LIGNE_ACHAT);
      requeteSql.ajouter("  JOIN  " + queryManager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_LIGNE + " ON");
      requeteSql.ajouter("    L1ETB=AAETB");
      requeteSql.ajouter("    AND L1COD=AACOS");
      requeteSql.ajouter("    AND L1NUM=AANOS");
      requeteSql.ajouter("    AND L1SUF=AASOS");
      requeteSql.ajouter("    AND L1NLI=AALOS");
      requeteSql.ajouter("    AND L1ART=AAART");
      requeteSql.ajouter("WHERE");
      requeteSql.ajouter("  AAETB = " + RequeteSql.formaterStringSQL(pCritereStock.getIdEtablissement().getCodeEtablissement()));
      if (pCritereStock.getIdMagasin() != null) {
        requeteSql.ajouter("  AND AAMAG = " + RequeteSql.formaterStringSQL(pCritereStock.getIdMagasin().getCode()));
      }
      requeteSql.ajouter("  AND L1TOP IN (1 , 3)");
      requeteSql.ajouter("  AND L1ERL = 'C'");
      requeteSql.ajouter("  AND AATOE IN ('G', 'A')");
      requeteSql.ajouter("  AND AACOS = 'E'");
      requeteSql.ajouter("  AND AACOE = 'E'");
      requeteSql.ajouter("  AND AAART");
      requeteSql.ajouterGroupeDansRequete("groupeArticle", "IN");
      requeteSql.ajouter("GROUP BY AAETB, AAMAG, AAART");
      requeteSql.ajouter(")");
      
      // Préparer le groupe sur les articles pour la table PGVMSTKM
      for (IdArticle idArticle : pCritereStock.getListeIdArticle()) {
        requeteSql.ajouterValeurAuGroupe("groupeArticle", idArticle.getCodeArticle());
      }
      
      // Requête principale sur la table PGVMSTKM (stock)
      // On additionne les valeurs car il y a une ligne par magasin.
      requeteSql.ajouter("SELECT");
      requeteSql.ajouter("  S1ETB,");
      requeteSql.ajouter("  S1MAG,");
      requeteSql.ajouter("  S1ART,");
      requeteSql.ajouter("  S1STD + S1QEE + S1QSE + S1QDE + S1QEM + S1QSM + S1QDM + S1QES + S1QSS + S1QDS as PHYSIQUE,");
      requeteSql.ajouter("  S1ATT AS ATTENDU,");
      requeteSql.ajouter("  S1RES AS COMMANDE,");
      requeteSql.ajouter("  S1AFF AS RESERVE,");
      requeteSql.ajouter("  QTEALA AS LIE");
      requeteSql.ajouter("FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.STOCK);
      requeteSql.ajouter("  LEFT JOIN ALA ON AAETB=S1ETB AND AAMAG=S1MAG AND AAART=S1ART");
      requeteSql.ajouter("WHERE");
      requeteSql.ajouter("  S1ETB = " + RequeteSql.formaterStringSQL(pCritereStock.getIdEtablissement().getCodeEtablissement()));
      if (pCritereStock.getIdMagasin() != null) {
        requeteSql.ajouter("  AND S1MAG = " + RequeteSql.formaterStringSQL(pCritereStock.getIdMagasin().getCode()));
      }
      requeteSql.ajouter("  AND S1ART");
      requeteSql.ajouterGroupeDansRequete("groupeArticle", "IN");
    }
    // Charger le stock de l'établissement
    else {
      // Préparer le groupe sur les articles pour la table PGVMALAM
      for (IdArticle idArticle : pCritereStock.getListeIdArticle()) {
        requeteSql.ajouterValeurAuGroupe("groupeArticle", idArticle.getCodeArticle());
      }
      
      // Sous-requête sur la table PGVMALAM (commandes affectées sur achats)
      // Cela permet de faire la somme des quantités commandées liées à des achats par magasin.
      // La jointure avec les lignes de ventes permet de ne sélectionner que les commandes validées ou réservées (L1TOP = 1 et 3).
      // Le groupement par établissement, magasin et article permet d'avoir le cumul par magasin.
      // Le champ AATOE permet de connaître le type d'affectation sur achat :
      // - G = GBA (à compter)
      // - A = Affectation sur achat existant (à compter)
      // - S = Affectation sur stock existant (à ne pas compter)
      // Le champ L1ERL permet de savoir si l'article est géré en stock (C) ou non (S). Noté qu'il existe aussi la champ LAERL côté achat
      // mais les deux champs sont sensés être égaux.
      // Les champs AACOS et AACOE doivent être égaux à E pour refléter que les liés sur achats correspondent bien à des lignes de
      // commandes non réceptionnées.
      requeteSql.ajouter("WITH ALA AS (");
      requeteSql.ajouter("SELECT");
      requeteSql.ajouter("  AAETB,");
      requeteSql.ajouter("  AAMAG,");
      requeteSql.ajouter("  AAART,");
      requeteSql.ajouter("  SUM(AAQTE) AS QTEALA");
      requeteSql.ajouter("FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.LIEN_LIGNE_VENTE_LIGNE_ACHAT);
      requeteSql.ajouter("  JOIN  " + queryManager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_LIGNE + " ON");
      requeteSql.ajouter("    L1ETB=AAETB");
      requeteSql.ajouter("    AND L1COD=AACOS");
      requeteSql.ajouter("    AND L1NUM=AANOS");
      requeteSql.ajouter("    AND L1SUF=AASOS");
      requeteSql.ajouter("    AND L1NLI=AALOS");
      requeteSql.ajouter("    AND L1ART=AAART");
      requeteSql.ajouter("WHERE");
      requeteSql.ajouter("  AAETB = " + RequeteSql.formaterStringSQL(pCritereStock.getIdEtablissement().getCodeEtablissement()));
      requeteSql.ajouter("  AND L1TOP IN (1 , 3)");
      requeteSql.ajouter("  AND L1ERL = 'C'");
      requeteSql.ajouter("  AND AATOE IN ('G', 'A')");
      requeteSql.ajouter("  AND AACOS = 'E'");
      requeteSql.ajouter("  AND AACOE = 'E'");
      requeteSql.ajouter("  AND AAART");
      requeteSql.ajouterGroupeDansRequete("groupeArticle", "IN");
      requeteSql.ajouter("GROUP BY AAETB, AAMAG, AAART");
      requeteSql.ajouter(")");
      
      // Préparer le groupe sur les articles pour la table PGVMSTKM
      for (IdArticle idArticle : pCritereStock.getListeIdArticle()) {
        requeteSql.ajouterValeurAuGroupe("groupeArticle", idArticle.getCodeArticle());
      }
      
      // Requête principale sur la table PGVMSTKM (stock)
      // On additionne les valeurs car il y a une ligne par magasin.
      requeteSql.ajouter("SELECT");
      requeteSql.ajouter("  S1ETB,");
      requeteSql.ajouter("  S1ART,");
      requeteSql.ajouter("  SUM(S1STD + S1QEE + S1QSE + S1QDE + S1QEM + S1QSM + S1QDM + S1QES + S1QSS + S1QDS) as PHYSIQUE,");
      requeteSql.ajouter("  SUM(S1ATT) AS ATTENDU,");
      requeteSql.ajouter("  SUM(S1RES) AS COMMANDE,");
      requeteSql.ajouter("  SUM(S1AFF) AS RESERVE,");
      requeteSql.ajouter("  SUM(QTEALA) AS LIE");
      requeteSql.ajouter("FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.STOCK);
      requeteSql.ajouter("  LEFT JOIN ALA ON AAETB=S1ETB AND AAMAG=S1MAG AND AAART=S1ART");
      requeteSql.ajouter("WHERE");
      requeteSql.ajouter("  S1ETB = " + RequeteSql.formaterStringSQL(pCritereStock.getIdEtablissement().getCodeEtablissement()));
      requeteSql.ajouter("  AND S1ART");
      requeteSql.ajouterGroupeDansRequete("groupeArticle", "IN");
      requeteSql.ajouter("  GROUP BY S1ETB, S1ART");
    }
    
    // Préparer le résultat
    ListeStock listeStock = new ListeStock();
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequete());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return listeStock;
    }
    
    // Générer la liste à partir du résultat de la requête
    for (GenericRecord record : listeRecord) {
      try {
        listeStock.add(completerStock(record));
      }
      catch (Exception e) {
        Trace.alerte(
            "Un stock invalide est ignoré lors du chargement de la liste des stocks : S1ETB=" + record.getField("S1ETB").toString()
                + " S1MAG=" + record.getField("S1MAG").toString() + " S1ART=" + record.getField("S1ART").toString());
      }
    }
    
    // Calculer le stock avant rupture si c'est demandé
    if (pCritereStock.isCalculerStockAvantRupture()) {
      for (Stock stock : listeStock) {
        // Charger les attendus et commandés
        CritereStockAttenduCommande critereStockAttenduCommande = new CritereStockAttenduCommande();
        critereStockAttenduCommande.setIdEtablissement(stock.getId().getIdEtablissement());
        critereStockAttenduCommande.setIdMagasin(stock.getId().getIdMagasin());
        critereStockAttenduCommande.setIdArticle(stock.getId().getIdArticle());
        ListeStockAttenduCommande listeStockAttenduCommande = chargerListeStockAttenduCommande(critereStockAttenduCommande);
        
        // Charger la date minimumu de prochain réappro possible
        Date dateMiniReappro = chargerDateMiniReappro(stock.getId().getIdArticle());
        
        // Calculer les valeurs de stock
        listeStockAttenduCommande.calculerStock(stock.getId().getIdArticle(), stock.getStockPhysique(), new Date(), dateMiniReappro);
        
        // Renseigner le stock
        stock.setStockAvantRupture(listeStockAttenduCommande.getStockAvantRupture());
        stock.setDateRupturePossible(listeStockAttenduCommande.getDateRupturePossible());
        stock.setDateFinRupture(listeStockAttenduCommande.getDateFinRupture());
      }
    }
    
    return listeStock;
  }
  
  /**
   * Compléter un objet métier Stock à partir des champs lus en base de données.
   * 
   * @param pRecord Ligne de résultat issue de la requête SQL.
   * @return Stock Objet métier complété.
   */
  private Stock completerStock(GenericRecord pRecord) {
    // Construire l'identifiant de l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("S1ETB", "", false));
    
    // Construire l'identifiant du magasin
    IdMagasin idMagasin = null;
    if (!pRecord.getStringValue("S1MAG", "", false).trim().isEmpty()) {
      idMagasin = IdMagasin.getInstance(idEtablissement, pRecord.getStringValue("S1MAG", "", false));
    }
    
    // Construire l'identifiant de l'article
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, pRecord.getStringValue("S1ART", "", false));
    
    // Construire l'identifiant du stock
    IdStock idStock = IdStock.getInstance(idEtablissement, idMagasin, idArticle);
    
    // Renseigner l'objet stock
    Stock stock = new Stock(idStock);
    stock.setStockPhysique(pRecord.getBigDecimalValue("PHYSIQUE"));
    stock.setStockAttendu(pRecord.getBigDecimalValue("ATTENDU"));
    stock.setStockCommande(pRecord.getBigDecimalValue("COMMANDE"));
    stock.setStockCommandeLieSurAchat(pRecord.getBigDecimalValue("LIE"));
    stock.setStockReserve(pRecord.getBigDecimalValue("RESERVE"));
    return stock;
  }
  
  /**
   * Charger les lignes attendus et commandés suivant les critères de recerche.
   * 
   * @param pCritereStockAttenduCommande Critères de recherche des attendus et commandés?
   * @return Liste d'attendus et commandés.
   */
  public ListeStockAttenduCommande chargerListeStockAttenduCommande(CritereStockAttenduCommande pCritereStockAttenduCommande) {
    // Vérifier les paramètres
    if (pCritereStockAttenduCommande == null) {
      throw new MessageErreurException("Les critères de stock attendu/commandés sont invalides.");
    }
    if (pCritereStockAttenduCommande.getIdArticle() == null) {
      throw new MessageErreurException("Impossible de charger le stock si aucun article n'est défini.");
    }
    
    // Générer la requête SQL
    RequeteSql requeteSql = new RequeteSql();
    
    // Générer la requête sur les attendus
    // La colonne TYPE sert à trier les attendus avant les commandés
    requeteSql.ajouter("SELECT");
    requeteSql.ajouter("  " + EnumTypeAttenduCommande.ATTENDU.getCode() + " AS TYPE,");
    requeteSql.ajouter("  EAETB AS ETB,");
    requeteSql.ajouter("  EACOD AS CODE,");
    requeteSql.ajouter("  LAART AS ARTICLE,");
    requeteSql.ajouter("  EAMAG AS MAGASIN,");
    requeteSql.ajouter("  LANUM AS NUMERO,");
    requeteSql.ajouter("  LASUF AS SUFFIXE,");
    requeteSql.ajouter("  LANLI AS LIGNE,");
    requeteSql.ajouter("  EAHOM AS DATE,");
    requeteSql.ajouter("  CASE WHEN LADLP>0 THEN LADLP ELSE EADLP END AS DLP,");
    requeteSql.ajouter("  EADAT1 AS DLS,");
    requeteSql.ajouter("  EAETA AS ETAT,");
    requeteSql.ajouter("  LAQTS AS ENTREE,");
    requeteSql.ajouter("  0 AS SORTIE,");
    requeteSql.ajouter("  0 AS COEFFICIENT_STOCK,");
    requeteSql.ajouter("  0 AS CONDITIONNEMENT,");
    requeteSql.ajouter("  FRNOM AS TIERS");
    requeteSql.ajouter("FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT_LIGNE);
    requeteSql.ajouter("LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_ACHAT + " ON");
    requeteSql.ajouter("  EAETB=LAETB");
    requeteSql.ajouter("  AND EACOD=LACOD");
    requeteSql.ajouter("  AND EANUM=LANUM");
    requeteSql.ajouter("  AND EASUF=LASUF ");
    requeteSql.ajouter("LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " ON");
    requeteSql.ajouter("  FRETB=EAETB");
    requeteSql.ajouter("  AND FRCOL=EACOL");
    requeteSql.ajouter("  AND FRFRS=EAFRS");
    requeteSql.ajouter("WHERE");
    requeteSql.ajouter("  LACOD='E'");
    requeteSql.ajouter("  AND LAERL = 'C'");
    requeteSql.ajouter("  AND LATOP = 1");
    requeteSql.ajouter("  AND LACEX <> 'A'");
    requeteSql.ajouter("  AND LAQTS<>0");
    requeteSql.ajouterConditionAnd("EAETB", "=", pCritereStockAttenduCommande.getIdEtablissement().getCodeEtablissement());
    if (pCritereStockAttenduCommande.getIdMagasin() != null) {
      requeteSql.ajouterConditionAnd("EAMAG", "=", pCritereStockAttenduCommande.getIdMagasin().getCode());
    }
    requeteSql.ajouterConditionAnd("LAART", "=", pCritereStockAttenduCommande.getIdArticle().getCodeArticle());
    
    // Générer la requête sur les commandés
    requeteSql.ajouter("UNION");
    requeteSql.ajouter("SELECT");
    requeteSql.ajouter("  " + EnumTypeAttenduCommande.COMMANDE.getCode() + " AS TYPE,");
    requeteSql.ajouter("  E1ETB AS ETB,");
    requeteSql.ajouter("  E1COD AS CODE,");
    requeteSql.ajouter("  L1ART AS ARTICLE,");
    requeteSql.ajouter("  E1MAG AS MAGASIN,");
    requeteSql.ajouter("  L1NUM AS NUMERO,");
    requeteSql.ajouter("  L1SUF AS SUFFIXE,");
    requeteSql.ajouter("  L1NLI AS LIGNE,");
    requeteSql.ajouter("  E1HOM AS DATE,");
    requeteSql.ajouter("  CASE WHEN L1DLP>0 THEN L1DLP ELSE E1DLP END AS DLP,");
    requeteSql.ajouter("  E1DLS AS DLS,");
    requeteSql.ajouter("  E1ETA AS ETAT,");
    requeteSql.ajouter("  0 AS ENTREE,");
    requeteSql.ajouter("  L1QTE AS SORTIE,");
    requeteSql.ajouter("  L1KSV AS COEFFICIENT_STOCK,");
    requeteSql.ajouter("  L1CND AS CONDITIONNEMENT,");
    requeteSql.ajouter("  CLNOM AS TIERS");
    requeteSql.ajouter("FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE_LIGNE);
    requeteSql.ajouter("LEFT JOIN  " + queryManager.getLibrary() + '.' + EnumTableBDD.DOCUMENT_VENTE + " ON");
    requeteSql.ajouter("  E1ETB=L1ETB");
    requeteSql.ajouter("  AND E1COD=L1COD");
    requeteSql.ajouter("  AND E1NUM=L1NUM");
    requeteSql.ajouter("  AND E1SUF=L1SUF");
    requeteSql.ajouter("LEFT JOIN " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT + " ON ");
    requeteSql.ajouter("  CLETB=E1ETB");
    requeteSql.ajouter("  AND CLCLI=E1CLFP");
    requeteSql.ajouter("  AND CLLIV=E1CLFS");
    requeteSql.ajouter("WHERE");
    requeteSql.ajouter("  L1COD='E'");
    requeteSql.ajouter("  AND L1ERL = 'C'");
    requeteSql.ajouter("  AND L1TOP in(1, 3)");
    requeteSql.ajouterConditionAnd("E1ETB", "=", pCritereStockAttenduCommande.getIdEtablissement().getCodeEtablissement());
    if (pCritereStockAttenduCommande.getIdMagasin() != null) {
      requeteSql.ajouterConditionAnd("E1MAG", "=", pCritereStockAttenduCommande.getIdMagasin().getCode());
    }
    requeteSql.ajouterConditionAnd("L1ART", "=", pCritereStockAttenduCommande.getIdArticle().getCodeArticle());
    
    // Ajouter l'odre de tri
    // D'abord par date de livraison, puis les attendus avant les commandés et enfin par date de validation du document
    requeteSql.ajouter("ORDER BY");
    requeteSql.ajouter("DLP,");
    requeteSql.ajouter("TYPE,");
    requeteSql.ajouter("DATE");
    
    // Préparer le résultat
    ListeStockAttenduCommande listeStockAttenduCommande = new ListeStockAttenduCommande();
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequete());
    if (listeRecord == null || listeRecord.size() == 0) {
      return listeStockAttenduCommande;
    }
    
    // Générer la liste à partir du résultat de la requête
    for (GenericRecord record : listeRecord) {
      try {
        StockAttenduCommande stockAttenduCommande = completerStockAttenduCommande(record);
        if (stockAttenduCommande != null) {
          listeStockAttenduCommande.add(stockAttenduCommande);
        }
      }
      catch (Exception e) {
        Trace.alerte("Un attendu/commandé invalide est ignoré lors du chargement de la liste des attendus/commandés");
      }
    }
    
    return listeStockAttenduCommande;
    
  }
  
  /**
   * Compléter un objet métier attendu et commandés à partir des champs lus en base de données.
   * 
   * @param pRecord Ligne de résultat issue de la requête SQL.
   * @return Stock Objet métier complété.
   */
  private StockAttenduCommande completerStockAttenduCommande(GenericRecord pRecord) {
    // Construire l'identifiant de l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("ETB", "", false));
    
    // Construire l'identifiant du magasin
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, pRecord.getStringValue("MAGASIN", "", false));
    
    // Construire l'identifiant de l'article
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, pRecord.getStringValue("ARTICLE", "", false));
    
    // Déterminer le type de l'attendu/commandé
    EnumTypeAttenduCommande typeAttenduCommande = EnumTypeAttenduCommande.valueOfByCode(pRecord.getIntValue("TYPE", 0));
    
    // Construire l'identifiant de l'attendu/commandé
    IdStockAttenduCommande idStockAttenduCommande = IdStockAttenduCommande.getInstance(typeAttenduCommande, idMagasin, idArticle,
        pRecord.getIntegerValue("NUMERO"), pRecord.getIntegerValue("SUFFIXE"), pRecord.getIntegerValue("LIGNE"));
    
    // Ne pas tenir compte de la ligne si les deux quantités sont nulles
    // Cela peut arriver si un traitement n'a pas pu aller au bout et a laissé des lignes sans quantités
    BigDecimal entree = pRecord.getBigDecimalValue("ENTREE");
    BigDecimal sortie = pRecord.getBigDecimalValue("SORTIE");
    if (entree.compareTo(BigDecimal.ZERO) == 0 && sortie.compareTo(BigDecimal.ZERO) == 0) {
      return null;
    }
    
    // Renseigner l'objet attendu/commandé
    StockAttenduCommande stockAttenduCommande = new StockAttenduCommande(idStockAttenduCommande);
    stockAttenduCommande.setEtat(pRecord.getIntegerValue("ETAT"));
    stockAttenduCommande.setDateValidation(pRecord.getDateValue("DATE"));
    stockAttenduCommande.setDateLivraisonPrevue(pRecord.getDateValue("DLP"));
    stockAttenduCommande.setDateLivraisonSouhaite(pRecord.getDateValue("DLS"));
    stockAttenduCommande.setQuantiteEntree(entree);
    stockAttenduCommande.setQuantiteSortieEnUniteVente(sortie, pRecord.getBigDecimalValue("COEFFICIENT_STOCK"),
        pRecord.getBigDecimalValue("CONDITIONNEMENT"));
    stockAttenduCommande.setLibelleTiers(pRecord.getStringValue("TIERS"));
    return stockAttenduCommande;
  }
  
  /**
   * Charger la liste des mouvements de stock suivant les critères de recherche.
   * 
   * @param pCritereStockMouvement Critères de recherche.
   * @return Liste de mouvements de stock.
   */
  public ListeStockMouvement chargerListeStockMouvement(CritereStockMouvement pCritereStockMouvement) {
    // Vérifier les paramètres
    if (pCritereStockMouvement == null) {
      throw new MessageErreurException("Les critères de mouvements stock sont invalides.");
    }
    
    // Générer la requête SQL
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT");
    requeteSql.ajouter("  H1ETB as ETB,");
    requeteSql.ajouter("  H1MAG as MAGASIN,");
    requeteSql.ajouter("  H1ART as ARTICLE,");
    requeteSql.ajouter("  H1DAT as DATE,");
    requeteSql.ajouter("  H1ORD as ORDRE,");
    requeteSql.ajouter("  H1OPE as OPERATION,");
    requeteSql.ajouter("  H1ORI as ORIGINE,");
    requeteSql.ajouter("  H1COD0 as CODE,");
    requeteSql.ajouter("  H1NUM0 as NUMERO,");
    requeteSql.ajouter("  H1SUF0 as SUFFIXE,");
    requeteSql.ajouter("  H1NLI0 as LIGNE,");
    requeteSql.ajouter("  H1QTM as QUANTITE,");
    requeteSql.ajouter("  H1QST as PHYSIQUE,");
    requeteSql.ajouter("  H1TI1,");
    requeteSql.ajouter("  H1TI2,");
    requeteSql.ajouter("  H1TI3,");
    requeteSql.ajouter("  CLNOM,");
    requeteSql.ajouter("  FRNOM");
    requeteSql.ajouter("FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.STOCK_HISTORIQUE);
    requeteSql.ajouter("  LEFT JOIN  " + queryManager.getLibrary() + '.' + EnumTableBDD.CLIENT);
    requeteSql.ajouter("    ON CLETB=H1ETB AND CLCLI=H1TI2 AND CLLIV=H1TI3");
    requeteSql.ajouter("  LEFT JOIN  " + queryManager.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR);
    requeteSql.ajouter("    ON FRETB=H1ETB AND FRCOL=H1TI1 AND FRFRS=H1TI2");
    requeteSql.ajouter("WHERE");
    requeteSql.ajouterConditionAnd("H1ETB", "=", pCritereStockMouvement.getIdEtablissement().getCodeEtablissement());
    if (pCritereStockMouvement.getIdMagasin() != null) {
      requeteSql.ajouterConditionAnd("H1MAG", "=", pCritereStockMouvement.getIdMagasin().getCode());
    }
    requeteSql.ajouterConditionAnd("H1ART", "=", pCritereStockMouvement.getIdArticle().getCodeArticle());
    requeteSql.ajouter("ORDER BY");
    requeteSql.ajouter("  H1DAT,");
    requeteSql.ajouter("  H1ORD");
    
    // Préparer le résultat
    ListeStockMouvement listeStockMouvement = new ListeStockMouvement();
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequete());
    if (listeRecord == null || listeRecord.size() == 0) {
      return listeStockMouvement;
    }
    
    // Générer la liste à partir du résultat de la requête
    for (GenericRecord record : listeRecord) {
      try {
        listeStockMouvement.add(completerStockMouvement(record));
      }
      catch (Exception e) {
        Trace.alerte("Un mouvement de stock invalide est ignoré lors du chargement de la liste des mouvements");
      }
    }
    
    return listeStockMouvement;
  }
  
  /**
   * 
   * Compléter un objet métier mouvement de stock à partir des champs lus en base de données.
   * 
   * @param pRecord Ligne de résultat issue de la requête SQL.
   * @return StockMouvement Objet métier complété.
   */
  private StockMouvement completerStockMouvement(GenericRecord pRecord) {
    // Construire l'identifiant de l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(pRecord.getStringValue("ETB", "", false));
    
    // Construire l'identifiant du magasin
    IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, pRecord.getStringValue("MAGASIN", "", false));
    
    // Construire l'identifiant de l'article
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, pRecord.getStringValue("ARTICLE", "", false));
    
    // Construire l'identifiant du mouvement de stock
    IdStockMouvement idStockMouvement =
        IdStockMouvement.getInstance(idMagasin, idArticle, pRecord.getDateValue("DATE"), pRecord.getIntegerValue("ORDRE"));
    
    // Construire le libellé du tiers (Suivant si entrée ou sortie et si on a trouvé un libellé).
    String libelleTiers = null;
    if (pRecord.getCharacterValue("OPERATION").equals('S')) {
      String nomClient = pRecord.getStringValue("CLNOM").trim();
      Integer numeroClient = pRecord.getIntegerValue("H1TI2");
      Integer suffixeClient = pRecord.getIntegerValue("H1TI3");
      
      // Renseigner le libellé du client si les informations obligatoires sont présentes
      if (numeroClient > 0) {
        IdClient idClient = IdClient.getInstance(idEtablissement, numeroClient, suffixeClient);
        libelleTiers = nomClient + " (" + idClient + ")";
      }
    }
    else if (pRecord.getCharacterValue("OPERATION").equals('E')) {
      String nomFournisseur = pRecord.getStringValue("FRNOM").trim();
      Integer collectifFournisseur = pRecord.getIntegerValue("H1TI1");
      Integer numeroFournisseur = pRecord.getIntegerValue("H1TI2");
      
      // Renseigner le libellé du fournisseurs si les informations obligatoires sont présentes
      if (collectifFournisseur > 0 && numeroFournisseur > 0) {
        IdFournisseur idFournisseur = IdFournisseur.getInstance(idEtablissement, collectifFournisseur, numeroFournisseur);
        libelleTiers = nomFournisseur + " (" + idFournisseur + ")";
      }
    }
    
    // Mouvement
    StockMouvement stockMouvement = new StockMouvement(idStockMouvement);
    stockMouvement.setType(EnumTypeStockMouvement.valueOfByCode(pRecord.getCharacterValue("OPERATION")));
    stockMouvement.setOrigine(EnumOrigineStockMouvement.valueOfByCode(pRecord.getCharacterValue("ORIGINE")));
    stockMouvement.setCodeEnteteDocument(pRecord.getCharacterValue("CODE"));
    stockMouvement.setNumeroDocument(pRecord.getIntegerValue("NUMERO"));
    stockMouvement.setSuffixe(pRecord.getIntegerValue("SUFFIXE"));
    stockMouvement.setNumeroLigne(pRecord.getIntegerValue("LIGNE"));
    stockMouvement.setLibelleTiers(libelleTiers);
    stockMouvement.setQuantite(pRecord.getBigDecimalValue("QUANTITE"));
    stockMouvement.setStockPhysique(pRecord.getBigDecimalValue("PHYSIQUE"));
    return stockMouvement;
  }
  
  /**
   * Charger la date minimum de réapprovisionnement possible.
   * 
   * La date est récupérée à partir des délais fournisseur persistés dans les conditions d'achats.
   * 
   * @param pIdArticle Identifiant de l'article.
   * @return Date minimum de reapprovisionnement.
   */
  public Date chargerDateMiniReappro(IdArticle pIdArticle) {
    // Vérifier les paramètres
    IdArticle.controlerId(pIdArticle, true);
    
    // Générer la date du jour au format DB2
    int dateJour = ConvertDate.dateToDb2(new Date());
    
    // Générer la requête SQL
    // Récupérer la dernière condition d'achat activée avant la date du jour
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("SELECT");
    requeteSql.ajouter("  CADEL,");
    requeteSql.ajouter("  CADELS,");
    requeteSql.ajouter("  CAIN7");
    requeteSql.ajouter("FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.CONDITION_ACHAT);
    requeteSql.ajouter("WHERE");
    requeteSql.ajouter("  CAETB = " + RequeteSql.formaterStringSQL(pIdArticle.getCodeEtablissement()));
    requeteSql.ajouter("  AND CAART = " + RequeteSql.formaterStringSQL(pIdArticle.getCodeArticle()));
    requeteSql.ajouter("  AND CADAP <= " + dateJour);
    requeteSql.ajouter("ORDER BY");
    requeteSql.ajouter(" CADAP DESC");
    requeteSql.ajouter("LIMIT 1");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequete());
    if (listeRecord == null || listeRecord.size() == 0) {
      return null;
    }
    if (listeRecord.size() > 1) {
      Trace.alerte("La requête permettant de déterminer la date minimum de réapprovisionnement a retourné plus d'une condition d'achat.");
    }
    
    // Récupérer les données
    GenericRecord record = listeRecord.get(0);
    int delaiLivraison = record.getIntValue("CADEL", 0);
    int delaiSupplementaire = record.getIntValue("CADELS", 0);
    Character uniteDelai = record.getCharacterValue("CAIN7");
    
    // Calculer le délai minimum de réappro
    Date dateMiniReappro = null;
    if (uniteDelai.equals(' ')) {
      // Calculer le délai en semaines
      dateMiniReappro = Constantes.getDateDelaiSemaine(new Date(), (delaiLivraison + delaiSupplementaire));
    }
    else {
      // Calculer le délai en jours
      dateMiniReappro = Constantes.getDateDelaiJour(new Date(), (delaiLivraison + delaiSupplementaire));
    }
    
    return dateMiniReappro;
  }
  
}
