/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database.files;

import ri.serien.libas400.database.BaseFileDB;
import ri.serien.libas400.database.QueryManager;

public abstract class FFD_Psempdmm extends BaseFileDB {
  // Constantes (valeurs récupérées via DSPFFD)
  public static final int SIZE_PDDOC = 3;
  public static final int SIZE_PDFIC = 3;
  public static final int SIZE_PDLIB = 30;
  public static final int SIZE_PDCHM0 = 30;
  public static final int SIZE_PDCHM1 = 3;
  public static final int SIZE_PDCHM2 = 3;
  public static final int SIZE_PDCHM3 = 3;
  public static final int SIZE_PDCHM4 = 3;
  public static final int SIZE_PDCHM5 = 3;
  public static final int SIZE_PDIN1 = 1;
  public static final int SIZE_PDIN2 = 1;
  public static final int SIZE_PDIN3 = 1;
  public static final int SIZE_PDIN4 = 1;
  public static final int SIZE_PDIN5 = 1;
  
  // Variables fichiers
  protected String PDDOC = null; // Type de Document
  protected String PDFIC = null; // Type de Fiche Série M
  protected String PDLIB = null; // Texte bouton JWALK
  protected String PDCHM0 = null; // Racine facultative
  protected String PDCHM1 = null; // Code chemin 1
  protected String PDCHM2 = null; // Code chemin 2
  protected String PDCHM3 = null; // Code chemin 3
  protected String PDCHM4 = null; // Code chemin 4
  protected String PDCHM5 = null; // Code chemin 5
  protected char PDIN1 = ' '; // 1=Bibli.dans chemin
  protected char PDIN2 = ' '; // 1=Etabl.dans chemin
  protected char PDIN3 = ' '; // Non utilisé
  protected char PDIN4 = ' '; // Non utilisé
  protected char PDIN5 = ' '; // Non utilisé
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public FFD_Psempdmm(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise les variables avec les valeurs par défaut
   */
  @Override
  public void initialization() {
    PDDOC = null;
    PDFIC = null;
    PDLIB = null;
    PDCHM0 = null;
    PDCHM1 = null;
    PDCHM2 = null;
    PDCHM3 = null;
    PDCHM4 = null;
    PDCHM5 = null;
    PDIN1 = ' ';
    PDIN2 = ' ';
    PDIN3 = ' ';
    PDIN4 = ' ';
    PDIN5 = ' ';
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le pDDOC
   */
  public String getPDDOC() {
    return PDDOC;
  }
  
  /**
   * @param pDDOC le pDDOC à définir
   */
  public void setPDDOC(String pDDOC) {
    PDDOC = pDDOC;
  }
  
  /**
   * @return le pDFIC
   */
  public String getPDFIC() {
    return PDFIC;
  }
  
  /**
   * @param pDFIC le pDFIC à définir
   */
  public void setPDFIC(String pDFIC) {
    PDFIC = pDFIC;
  }
  
  /**
   * @return le pDCHM0
   */
  public String getPDCHM0() {
    return PDCHM0;
  }
  
  /**
   * @param pDCHM0 le pDCHM0 à définir
   */
  public void setPDCHM0(String pDCHM0) {
    PDCHM0 = pDCHM0;
  }
  
  /**
   * @return le pDCHM1
   */
  public String getPDCHM1() {
    return PDCHM1;
  }
  
  /**
   * @param pDCHM1 le pDCHM1 à définir
   */
  public void setPDCHM1(String pDCHM1) {
    PDCHM1 = pDCHM1;
  }
  
  /**
   * @return le pDCHM2
   */
  public String getPDCHM2() {
    return PDCHM2;
  }
  
  /**
   * @param pDCHM2 le pDCHM2 à définir
   */
  public void setPDCHM2(String pDCHM2) {
    PDCHM2 = pDCHM2;
  }
  
  /**
   * @return le pDCHM3
   */
  public String getPDCHM3() {
    return PDCHM3;
  }
  
  /**
   * @param pDCHM3 le pDCHM3 à définir
   */
  public void setPDCHM3(String pDCHM3) {
    PDCHM3 = pDCHM3;
  }
  
  /**
   * @return le pDCHM4
   */
  public String getPDCHM4() {
    return PDCHM4;
  }
  
  /**
   * @param pDCHM4 le pDCHM4 à définir
   */
  public void setPDCHM4(String pDCHM4) {
    PDCHM4 = pDCHM4;
  }
  
  /**
   * @return le pDCHM5
   */
  public String getPDCHM5() {
    return PDCHM5;
  }
  
  /**
   * @param pDCHM5 le pDCHM5 à définir
   */
  public void setPDCHM5(String pDCHM5) {
    PDCHM5 = pDCHM5;
  }
  
  /**
   * @return le pDIN1
   */
  public char getPDIN1() {
    return PDIN1;
  }
  
  /**
   * @param pDIN1 le pDIN1 à définir
   */
  public void setPDIN1(char pDIN1) {
    PDIN1 = pDIN1;
  }
  
  /**
   * @return le pDIN2
   */
  public char getPDIN2() {
    return PDIN2;
  }
  
  /**
   * @param pDIN2 le pDIN2 à définir
   */
  public void setPDIN2(char pDIN2) {
    PDIN2 = pDIN2;
  }
  
  /**
   * @return le pDIN3
   */
  public char getPDIN3() {
    return PDIN3;
  }
  
  /**
   * @param pDIN3 le pDIN3 à définir
   */
  public void setPDIN3(char pDIN3) {
    PDIN3 = pDIN3;
  }
  
  /**
   * @return le pDIN4
   */
  public char getPDIN4() {
    return PDIN4;
  }
  
  /**
   * @param pDIN4 le pDIN4 à définir
   */
  public void setPDIN4(char pDIN4) {
    PDIN4 = pDIN4;
  }
  
  /**
   * @return le pDIN5
   */
  public char getPDIN5() {
    return PDIN5;
  }
  
  /**
   * @param pDIN5 le pDIN5 à définir
   */
  public void setPDIN5(char pDIN5) {
    PDIN5 = pDIN5;
  }
}
