/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.stocks;

import java.math.BigDecimal;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

public class Svgvx0003d {
  // Constantes
  public static final int SIZE_WETB = 3;
  public static final int SIZE_WART = 20;
  public static final int SIZE_WMAG = 2;
  public static final int SIZE_WDAT = 7;
  public static final int DECIMAL_WDAT = 0;
  public static final int SIZE_WORD = 4;
  public static final int DECIMAL_WORD = 0;
  public static final int SIZE_WMDP = 1;
  public static final int SIZE_WOPE = 1;
  public static final int SIZE_WQTM = 11;
  public static final int DECIMAL_WQTM = 3;
  public static final int SIZE_WQST = 11;
  public static final int DECIMAL_WQST = 3;
  public static final int SIZE_WPRX = 11;
  public static final int DECIMAL_WPRX = 4;
  public static final int SIZE_WPMP = 11;
  public static final int DECIMAL_WPMP = 4;
  public static final int SIZE_WPRV = 11;
  public static final int DECIMAL_WPRV = 4;
  public static final int SIZE_WPRD = 13;
  public static final int DECIMAL_WPRD = 5;
  public static final int SIZE_WSGN = 1;
  public static final int SIZE_WMAG2 = 2;
  public static final int SIZE_WORD2 = 4;
  public static final int DECIMAL_WORD2 = 0;
  public static final int SIZE_WORI = 1;
  public static final int SIZE_WCOD0 = 1;
  public static final int SIZE_WNUM0 = 6;
  public static final int DECIMAL_WNUM0 = 0;
  public static final int SIZE_WSUF0 = 1;
  public static final int DECIMAL_WSUF0 = 0;
  public static final int SIZE_WNLI0 = 4;
  public static final int DECIMAL_WNLI0 = 0;
  public static final int SIZE_WTI1 = 1;
  public static final int DECIMAL_WTI1 = 0;
  public static final int SIZE_WTI2 = 6;
  public static final int DECIMAL_WTI2 = 0;
  public static final int SIZE_WTI3 = 3;
  public static final int DECIMAL_WTI3 = 0;
  public static final int SIZE_WDATP = 7;
  public static final int DECIMAL_WDATP = 0;
  public static final int SIZE_WORDP = 4;
  public static final int DECIMAL_WORDP = 0;
  public static final int SIZE_WDATI = 7;
  public static final int DECIMAL_WDATI = 0;
  public static final int SIZE_WORDI = 4;
  public static final int DECIMAL_WORDI = 0;
  public static final int SIZE_WQIV = 11;
  public static final int DECIMAL_WQIV = 3;
  public static final int SIZE_WQEI = 11;
  public static final int DECIMAL_WQEI = 3;
  public static final int SIZE_WQSI = 11;
  public static final int DECIMAL_WQSI = 3;
  public static final int SIZE_WQDI = 11;
  public static final int DECIMAL_WQDI = 3;
  public static final int SIZE_WPUR = 1;
  public static final int DECIMAL_WPUR = 0;
  public static final int SIZE_WDRM = 7;
  public static final int DECIMAL_WDRM = 0;
  public static final int SIZE_WHRM = 4;
  public static final int DECIMAL_WHRM = 0;
  public static final int SIZE_WQTR = 11;
  public static final int DECIMAL_WQTR = 3;
  public static final int SIZE_WIN1 = 1;
  public static final int SIZE_WIN2 = 1;
  public static final int SIZE_WIN3 = 1;
  public static final int SIZE_TOTALE_DS = 228;
  public static final int SIZE_FILLER = 300 - 228;
  
  // Constantes indices Nom DS
  public static final int VAR_WETB = 0;
  public static final int VAR_WART = 1;
  public static final int VAR_WMAG = 2;
  public static final int VAR_WDAT = 3;
  public static final int VAR_WORD = 4;
  public static final int VAR_WMDP = 5;
  public static final int VAR_WOPE = 6;
  public static final int VAR_WQTM = 7;
  public static final int VAR_WQST = 8;
  public static final int VAR_WPRX = 9;
  public static final int VAR_WPMP = 10;
  public static final int VAR_WPRV = 11;
  public static final int VAR_WPRD = 12;
  public static final int VAR_WSGN = 13;
  public static final int VAR_WMAG2 = 14;
  public static final int VAR_WORD2 = 15;
  public static final int VAR_WORI = 16;
  public static final int VAR_WCOD0 = 17;
  public static final int VAR_WNUM0 = 18;
  public static final int VAR_WSUF0 = 19;
  public static final int VAR_WNLI0 = 20;
  public static final int VAR_WTI1 = 21;
  public static final int VAR_WTI2 = 22;
  public static final int VAR_WTI3 = 23;
  public static final int VAR_WDATP = 24;
  public static final int VAR_WORDP = 25;
  public static final int VAR_WDATI = 26;
  public static final int VAR_WORDI = 27;
  public static final int VAR_WQIV = 28;
  public static final int VAR_WQEI = 29;
  public static final int VAR_WQSI = 30;
  public static final int VAR_WQDI = 31;
  public static final int VAR_WPUR = 32;
  public static final int VAR_WDRM = 33;
  public static final int VAR_WHRM = 34;
  public static final int VAR_WQTR = 35;
  public static final int VAR_WIN1 = 36;
  public static final int VAR_WIN2 = 37;
  public static final int VAR_WIN3 = 38;
  
  // Variables AS400
  public String wetb = ""; //
  public String wart = ""; // Code Article
  public String wmag = ""; // Code Magasin
  public BigDecimal wdat = BigDecimal.ZERO; // Date mouvement
  public BigDecimal word = BigDecimal.ZERO; // N° Ordre
  public String wmdp = ""; // ERL Modificateur de PUMP
  public String wope = ""; // Code opération
  public BigDecimal wqtm = BigDecimal.ZERO; // Quantité Mouvement
  public BigDecimal wqst = BigDecimal.ZERO; // Quantité en stocks
  public BigDecimal wprx = BigDecimal.ZERO; // Prix unitaire en francs
  public BigDecimal wpmp = BigDecimal.ZERO; // P.U.M.P
  public BigDecimal wprv = BigDecimal.ZERO; // Prix de revient en US
  public BigDecimal wprd = BigDecimal.ZERO; // Prix unitaire en Devises/US
  public String wsgn = ""; // Signe de l'opération
  public String wmag2 = ""; // Magasin récepteur transfert
  public BigDecimal word2 = BigDecimal.ZERO; // Numéro d'ordre transfert
  public String wori = ""; // Origine opération
  public String wcod0 = ""; // Code Bon origine
  public BigDecimal wnum0 = BigDecimal.ZERO; // N°Bon origine
  public BigDecimal wsuf0 = BigDecimal.ZERO; // N°Suf.origine
  public BigDecimal wnli0 = BigDecimal.ZERO; // N°Lig.origine
  public BigDecimal wti1 = BigDecimal.ZERO; // Collectif si FRS
  public BigDecimal wti2 = BigDecimal.ZERO; // N°Tiers (CLI/FRS)
  public BigDecimal wti3 = BigDecimal.ZERO; // Livraison si CLI
  public BigDecimal wdatp = BigDecimal.ZERO; // Date de précédente opération
  public BigDecimal wordp = BigDecimal.ZERO; // Ord. de précédente opération
  public BigDecimal wdati = BigDecimal.ZERO; // Date de précédent inventaire
  public BigDecimal wordi = BigDecimal.ZERO; // Ord. de précédent inventaire
  public BigDecimal wqiv = BigDecimal.ZERO; // Quantité inventaire précéd.
  public BigDecimal wqei = BigDecimal.ZERO; // Cumul entrées depuis inv.pré
  public BigDecimal wqsi = BigDecimal.ZERO; // Cumul sorties depuis inv.pré
  public BigDecimal wqdi = BigDecimal.ZERO; // Cumul divers depuis inv.pré
  public BigDecimal wpur = BigDecimal.ZERO; // Top inventaire purgé
  public BigDecimal wdrm = BigDecimal.ZERO; // Date réelle opération
  public BigDecimal whrm = BigDecimal.ZERO; // Heure réelle opération
  public BigDecimal wqtr = BigDecimal.ZERO; // Quantité réelle
  public String win1 = ""; // Non utilisé
  public String win2 = ""; // Non utilisé
  public String win3 = ""; // Non utilisé
  public String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = {
      new AS400Text(SIZE_WETB), //
      new AS400Text(SIZE_WART), // Code Article
      new AS400Text(SIZE_WMAG), // Code Magasin
      new AS400ZonedDecimal(SIZE_WDAT, DECIMAL_WDAT), // Date mouvement
      new AS400ZonedDecimal(SIZE_WORD, DECIMAL_WORD), // N° Ordre
      new AS400Text(SIZE_WMDP), // ERL Modificateur de PUMP
      new AS400Text(SIZE_WOPE), // Code opération
      new AS400ZonedDecimal(SIZE_WQTM, DECIMAL_WQTM), // Quantité Mouvement
      new AS400ZonedDecimal(SIZE_WQST, DECIMAL_WQST), // Quantité en stocks
      new AS400ZonedDecimal(SIZE_WPRX, DECIMAL_WPRX), // Prix unitaire en francs
      new AS400ZonedDecimal(SIZE_WPMP, DECIMAL_WPMP), // P.U.M.P
      new AS400ZonedDecimal(SIZE_WPRV, DECIMAL_WPRV), // Prix de revient en US
      new AS400ZonedDecimal(SIZE_WPRD, DECIMAL_WPRD), // Prix unitaire en Devises/US
      new AS400Text(SIZE_WSGN), // Signe de l'opération
      new AS400Text(SIZE_WMAG2), // Magasin récepteur transfert
      new AS400ZonedDecimal(SIZE_WORD2, DECIMAL_WORD2), // Numéro d'ordre transfert
      new AS400Text(SIZE_WORI), // Origine opération
      new AS400Text(SIZE_WCOD0), // Code Bon origine
      new AS400ZonedDecimal(SIZE_WNUM0, DECIMAL_WNUM0), // N°Bon origine
      new AS400ZonedDecimal(SIZE_WSUF0, DECIMAL_WSUF0), // N°Suf.origine
      new AS400ZonedDecimal(SIZE_WNLI0, DECIMAL_WNLI0), // N°Lig.origine
      new AS400ZonedDecimal(SIZE_WTI1, DECIMAL_WTI1), // Collectif si FRS
      new AS400ZonedDecimal(SIZE_WTI2, DECIMAL_WTI2), // N°Tiers (CLI/FRS)
      new AS400ZonedDecimal(SIZE_WTI3, DECIMAL_WTI3), // Livraison si CLI
      new AS400ZonedDecimal(SIZE_WDATP, DECIMAL_WDATP), // Date de précédente opération
      new AS400ZonedDecimal(SIZE_WORDP, DECIMAL_WORDP), // Ord. de précédente opération
      new AS400ZonedDecimal(SIZE_WDATI, DECIMAL_WDATI), // Date de précédent inventaire
      new AS400ZonedDecimal(SIZE_WORDI, DECIMAL_WORDI), // Ord. de précédent inventaire
      new AS400ZonedDecimal(SIZE_WQIV, DECIMAL_WQIV), // Quantité inventaire précéd.
      new AS400ZonedDecimal(SIZE_WQEI, DECIMAL_WQEI), // Cumul entrées depuis inv.pré
      new AS400ZonedDecimal(SIZE_WQSI, DECIMAL_WQSI), // Cumul sorties depuis inv.pré
      new AS400ZonedDecimal(SIZE_WQDI, DECIMAL_WQDI), // Cumul divers depuis inv.pré
      new AS400ZonedDecimal(SIZE_WPUR, DECIMAL_WPUR), // Top inventaire purgé
      new AS400ZonedDecimal(SIZE_WDRM, DECIMAL_WDRM), // Date réelle opération
      new AS400ZonedDecimal(SIZE_WHRM, DECIMAL_WHRM), // Heure réelle opération
      new AS400ZonedDecimal(SIZE_WQTR, DECIMAL_WQTR), // Quantité réelle
      new AS400Text(SIZE_WIN1), // Non utilisé
      new AS400Text(SIZE_WIN2), // Non utilisé
      new AS400Text(SIZE_WIN3), // Non utilisé
      new AS400Text(SIZE_FILLER), // Filler
  };
  private AS400Structure ds = new AS400Structure(structure);
  public Object[] o = { wetb, wart, wmag, wdat, word, wmdp, wope, wqtm, wqst, wprx, wpmp, wprv, wprd, wsgn, wmag2, word2, wori, wcod0,
      wnum0, wsuf0, wnli0, wti1, wti2, wti3, wdatp, wordp, wdati, wordi, wqiv, wqei, wqsi, wqdi, wpur, wdrm, whrm, wqtr, win1, win2, win3,
      filler, };
}
