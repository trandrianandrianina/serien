/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_FI pour les FI
 */
public class Pgvmparm_ds_FI extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "FILIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "FICPL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "FIRUE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "FILOC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "FICDP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(24), "FIVIL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(26), "FILI2"));
    
    length = 300;
  }
}
