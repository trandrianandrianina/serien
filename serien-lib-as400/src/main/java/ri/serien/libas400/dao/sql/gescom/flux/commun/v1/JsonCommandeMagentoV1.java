/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxInit;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libcommun.outils.Constantes;

/**
 * Commande Série N au format Magento
 * Cette classe est une classe qui sert à l'intégration JSON vers JAVA
 * IL NE FAUT PAS RENOMMER OU MODIFIER SES CHAMPS
 * 
 * Note :
 * Attention cette version à le taux de TVA de 20% en dur dans le code. Dans les versions V2 et V3 ela a été corrigé pour Tecnifibre
 * (SNC-10032). Voir la méthode passerEnTTC() s'il faut adapter cette version.
 */
public class JsonCommandeMagentoV1 extends JsonEntiteMagento {
  
  // Client Série N
  private transient String BENCLP = null;
  private transient String BENCLS = null;
  // Zones de bloc adresse FAC formaté
  private transient String BENOMF = null;
  private transient String BECPLF = null;
  private transient String BERUEF = null;
  private transient String BELOCF = null;
  // Zones de bloc adresse LIV formaté
  private transient String BENOML = null;
  private transient String BECPLL = null;
  private transient String BERUEL = null;
  private transient String BELOCL = null;
  private transient boolean isTTC = true;
  private transient String RENUM = null;
  // Section analytique de la commande
  // Dépend de l'ID du site Web de la commande
  private transient String BESAN = null;
  
  private String idCommandeClient = null;
  private int idSite = 0;
  private String statut = null;
  private String modeLivraison = null;
  private String modeReglement = null;
  // Code service Expedito (transporteur + département) choisi par le client
  private String serviceLivraison = null;
  private String poids = null;
  private String blocNotes = null;
  // découpage du bloc notes pour une injection
  private transient ArrayList<String> listeBlocNotes = null;
  private String dateCreation = null;
  private String totalTTC = null;
  private String totalHT = null;
  private String fraisPortHT = null;
  private String fraisPortTTC = null;
  private transient String fraisPortCommande = null;
  private String remise = null;
  
  // contrôles client
  private String idClientMagento = null;
  private String mailClient = null;
  private String siretClient = null;
  
  // Client facturé
  private String societeClientFac = null;
  private String civiliteClientFac = null;
  private String nomClientFac = null;
  private String prenomClientFac = null;
  private String rueClientFac = null;
  private String rue2ClientFac = null;
  private String cdpClientFac = null;
  private String villeClientFac = null;
  private String paysClientFac = null;
  private String telClientFac = null;
  // Client livré
  private String societeClientLiv = null;
  private String civiliteClientLiv = null;
  private String nomClientLiv = null;
  private String prenomClientLiv = null;
  private String rueClientLiv = null;
  private String rue2ClientLiv = null;
  private String cdpClientLiv = null;
  private String villeClientLiv = null;
  private String paysClientLiv = null;
  private String telClientLiv = null;
  // Point relais
  private String pointRelais = null;
  private String paysPointRelais = null;
  
  private ArrayList<JsonLigneCommandeV1> listeArticlesCommandes = null;
  
  public JsonCommandeMagentoV1() {
    
  }
  
  /**
   * formater les données de base d'une commande (bloc adresses)
   */
  public void formaterDonnees() {
    // Entête de commande
    miseEnFormeBlocAdresseF();
    miseEnFormeBlocAdresseL();
    // mise en HT par défaut
    fraisPortCommande = fraisPortHT;
    
    // lignes de commande
    if (listeArticlesCommandes != null) {
      for (int i = 0; i < listeArticlesCommandes.size(); i++) {
        listeArticlesCommandes.get(i).formaterDonnees();
      }
    }
    // On découpe le bloc notes pour l'injection
    traitementDuBlocNotes();
  }
  
  /**
   * Mise en forme du bloc adresse
   * VA falloir voir ce un peu mieux
   */
  private void miseEnFormeBlocAdresseF() {
    BENOMF = "";
    BECPLF = "";
    // Si scoiété existe on remplit CLNOM
    if (societeClientFac != null && !societeClientFac.trim().equals("")) {
      BENOMF = societeClientFac;
    }
    
    // SI on a une société
    if (!BENOMF.trim().equals("")) {
      BECPLF = "";
      if (nomClientFac != null) {
        BECPLF = nomClientFac.trim();
      }
      if (prenomClientFac != null) {
        BECPLF += " " + prenomClientFac.trim();
      }
    }
    else {
      if (nomClientFac != null) {
        BENOMF = nomClientFac.trim();
      }
      // On ne laisse pas la zone de complément vide à cause des étiquettes transporteur
      if (prenomClientFac != null) {
        BENOMF += " " + prenomClientFac.trim();
      }
    }
    
    BERUEF = "";
    BELOCF = "";
    
    // TODO alog de découp propre
    if (rueClientFac != null) {
      BERUEF = rueClientFac;
    }
    if (rue2ClientFac != null) {
      BELOCF = rue2ClientFac;
    }
  }
  
  /**
   * Mise en forme du bloc adresse
   * VA falloir voir ce un peu mieux
   */
  private void miseEnFormeBlocAdresseL() {
    BENOML = "";
    BECPLL = "";
    // Si scoiété existe on remplit CLNOM
    if (societeClientLiv != null && !societeClientLiv.trim().equals("")) {
      BENOML = societeClientLiv;
    }
    
    // SI on a une société
    if (!BENOML.trim().equals("")) {
      BECPLL = "";
      if (nomClientLiv != null) {
        BECPLL = nomClientLiv.trim();
      }
      if (prenomClientLiv != null) {
        BECPLL += " " + prenomClientLiv.trim();
      }
    }
    else {
      if (nomClientLiv != null) {
        BENOML = nomClientLiv.trim();
      }
      // On ne laisse pas la zone de complément vide à cause des étiquettes transporteur
      if (prenomClientLiv != null) {
        BENOML += " " + prenomClientLiv.trim();
      }
    }
    
    // Si on a une société
    // if(CLCPL!=null)
    // {
    BERUEL = "";
    BELOCL = "";
    
    // TODO alog de découp propre
    if (rueClientLiv != null) {
      BERUEL = rueClientLiv;
    }
    if (rue2ClientLiv != null) {
      BELOCL = rue2ClientLiv;
    }
  }
  
  /**
   * on traite le bloc notes pour l'adapter à une injection Série N
   */
  private void traitementDuBlocNotes() {
    if (blocNotes == null) {
      return;
    }
    
    // TODO faudra faire mieux que tronquer les commentaires à terme
    if (blocNotes.length() > ParametresFluxInit.TAILLE_MAX_BLOC_NOTES) {
      blocNotes = blocNotes.substring(0, ParametresFluxInit.TAILLE_MAX_BLOC_NOTES);
    }
    
    // On formate pour le bouzin de Série N
    // On gère les commentaires de la commande
    if (blocNotes != null && !blocNotes.trim().isEmpty()) {
      BlocCommentairesV1 gestionCommentaires = new BlocCommentairesV1(blocNotes);
      listeBlocNotes = gestionCommentaires.getFormateCommentaire();
    }
  }
  
  /**
   * La commande devient TTC si le client est TTC.
   * Note : faire gaffe la TVA est en dur à 20% dans cette méthode.
   */
  public void passerEnTTC() {
    // Frais de port
    if (fraisPortTTC != null) {
      fraisPortCommande = fraisPortTTC;
    }
    else {
      try {
        if (fraisPortHT != null) {
          // TODO une méthode générique doit être mise en place pour l'arrondi Série N
          BigDecimal frais = new BigDecimal(fraisPortHT).multiply(new BigDecimal(1.2));
          frais = frais.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
          fraisPortCommande = frais.toString();
        }
      }
      catch (Exception e) {
        fraisPortCommande = null;
      }
    }
    // Remise
    try {
      if (remise != null) {
        BigDecimal rem = new BigDecimal(remise).multiply(new BigDecimal(1.2));
        rem = rem.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
        remise = rem.toString();
      }
    }
    catch (Exception e) {
      remise = null;
    }
    
    // Les tarifs
    if (listeArticlesCommandes != null) {
      for (int i = 0; i < listeArticlesCommandes.size(); i++) {
        listeArticlesCommandes.get(i).passerEnTTC();
      }
    }
  }
  
  public String retournerPoidsFormate() {
    String retour = null;
    if (poids == null) {
      return retour;
    }
    
    try {
      // TODO on peut formater les virgules en .
      
      BigDecimal valeur = new BigDecimal(poids);
      // Contrôles d'arrondis
      retour = valeur.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP).toString();
    }
    catch (Exception e) {
      retour = null;
      return retour;
    }
    
    return retour;
  }
  
  // +++++++++++++++++++++++ ACCESSEURS +++++++++++++++++++++++++
  
  public String getIdCommandeClient() {
    return idCommandeClient;
  }
  
  public void setIdCommandeClient(String idCommandeClient) {
    this.idCommandeClient = idCommandeClient;
  }
  
  public String getIdClientMagento() {
    return idClientMagento;
  }
  
  public void setIdClientMagento(String idClientMagento) {
    this.idClientMagento = idClientMagento;
  }
  
  public int getIdSite() {
    return idSite;
  }
  
  public void setIdSite(int idSite) {
    this.idSite = idSite;
  }
  
  public String getStatut() {
    return statut;
  }
  
  public void setStatut(String statut) {
    this.statut = statut;
  }
  
  public String getModeLivraison() {
    return modeLivraison;
  }
  
  public void setModeLivraison(String modeLivraison) {
    this.modeLivraison = modeLivraison;
  }
  
  public String getModeReglement() {
    return modeReglement;
  }
  
  public void setModeReglement(String modeReglement) {
    this.modeReglement = modeReglement;
  }
  
  public String getPoids() {
    return poids;
  }
  
  public void setPoids(String poids) {
    this.poids = poids;
  }
  
  public String getBlocNotes() {
    return blocNotes;
  }
  
  public void setBlocNotes(String blocNotes) {
    this.blocNotes = blocNotes;
  }
  
  public String getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(String dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public String getTotalTTC() {
    return totalTTC;
  }
  
  public void setTotalTTC(String totalTTC) {
    this.totalTTC = totalTTC;
  }
  
  public String getTotalHT() {
    return totalHT;
  }
  
  public void setTotalHT(String totalHT) {
    this.totalHT = totalHT;
  }
  
  public String getFraisPortHT() {
    return fraisPortHT;
  }
  
  public void setFraisPortHT(String fraisPortHT) {
    this.fraisPortHT = fraisPortHT;
  }
  
  public String getRemise() {
    return remise;
  }
  
  public void setRemise(String remise) {
    this.remise = remise;
  }
  
  public String getMailClient() {
    return mailClient;
  }
  
  public void setMailClient(String mailClient) {
    this.mailClient = mailClient;
  }
  
  public String getSiretClient() {
    return siretClient;
  }
  
  public void setSiretClient(String siretClient) {
    this.siretClient = siretClient;
  }
  
  public String getCiviliteClientFac() {
    return civiliteClientFac;
  }
  
  public void setCiviliteClientFac(String civiliteClientFac) {
    this.civiliteClientFac = civiliteClientFac;
  }
  
  public String getNomClientFac() {
    return nomClientFac;
  }
  
  public void setNomClientFac(String nomClientFac) {
    this.nomClientFac = nomClientFac;
  }
  
  public String getPrenomClientFac() {
    return prenomClientFac;
  }
  
  public void setPrenomClientFac(String prenomClientFac) {
    this.prenomClientFac = prenomClientFac;
  }
  
  public String getRueClientFac() {
    return rueClientFac;
  }
  
  public void setRueClientFac(String rueClientFac) {
    this.rueClientFac = rueClientFac;
  }
  
  public String getCdpClientFac() {
    return cdpClientFac;
  }
  
  public void setCdpClientFac(String cdpClientFac) {
    this.cdpClientFac = cdpClientFac;
  }
  
  public String getVilleClientFac() {
    return villeClientFac;
  }
  
  public void setVilleClientFac(String villeClientFac) {
    this.villeClientFac = villeClientFac;
  }
  
  public String getPaysClientFac() {
    return paysClientFac;
  }
  
  public void setPaysClientFac(String paysClientFac) {
    this.paysClientFac = paysClientFac;
  }
  
  public String getCiviliteClientLiv() {
    return civiliteClientLiv;
  }
  
  public void setCiviliteClientLiv(String civiliteClientLiv) {
    this.civiliteClientLiv = civiliteClientLiv;
  }
  
  public String getNomClientLiv() {
    return nomClientLiv;
  }
  
  public void setNomClientLiv(String nomClientLiv) {
    this.nomClientLiv = nomClientLiv;
  }
  
  public String getPrenomClientLiv() {
    return prenomClientLiv;
  }
  
  public void setPrenomClientLiv(String prenomClientLiv) {
    this.prenomClientLiv = prenomClientLiv;
  }
  
  public String getRueClientLiv() {
    return rueClientLiv;
  }
  
  public void setRueClientLiv(String rueClientLiv) {
    this.rueClientLiv = rueClientLiv;
  }
  
  public String getCdpClientLiv() {
    return cdpClientLiv;
  }
  
  public void setCdpClientLiv(String cdpClientLiv) {
    this.cdpClientLiv = cdpClientLiv;
  }
  
  public String getVilleClientLiv() {
    return villeClientLiv;
  }
  
  public void setVilleClientLiv(String villeClientLiv) {
    this.villeClientLiv = villeClientLiv;
  }
  
  public String getPaysClientLiv() {
    return paysClientLiv;
  }
  
  public void setPaysClientLiv(String paysClientLiv) {
    this.paysClientLiv = paysClientLiv;
  }
  
  public ArrayList<JsonLigneCommandeV1> getListeArticlesCommandes() {
    return listeArticlesCommandes;
  }
  
  public void setListeArticlesCommande(ArrayList<JsonLigneCommandeV1> listeArticlesCommandes) {
    this.listeArticlesCommandes = listeArticlesCommandes;
  }
  
  public String getBENCLP() {
    return BENCLP;
  }
  
  public void setBENCLP(String bENCLP) {
    BENCLP = bENCLP;
  }
  
  public String getBENCLS() {
    return BENCLS;
  }
  
  public void setBENCLS(String bENCLS) {
    BENCLS = bENCLS;
  }
  
  public String getRue2ClientFac() {
    return rue2ClientFac;
  }
  
  public void setRue2ClientFac(String rue2ClientFac) {
    this.rue2ClientFac = rue2ClientFac;
  }
  
  public String getRue2ClientLiv() {
    return rue2ClientLiv;
  }
  
  public void setRue2ClientLiv(String rue2ClientLiv) {
    this.rue2ClientLiv = rue2ClientLiv;
  }
  
  public String getSocieteClientFac() {
    return societeClientFac;
  }
  
  public void setSocieteClientFac(String societeClientFac) {
    this.societeClientFac = societeClientFac;
  }
  
  public void setListeArticlesCommandes(ArrayList<JsonLigneCommandeV1> listeArticlesCommandes) {
    this.listeArticlesCommandes = listeArticlesCommandes;
  }
  
  public String getSocieteClientLiv() {
    return societeClientLiv;
  }
  
  public void setSocieteClientLiv(String societeClientLiv) {
    this.societeClientLiv = societeClientLiv;
  }
  
  public String getBENOMF() {
    return BENOMF;
  }
  
  public void setBENOMF(String bENOMF) {
    BENOMF = bENOMF;
  }
  
  public String getBECPLF() {
    return BECPLF;
  }
  
  public void setBECPLF(String bECPLF) {
    BECPLF = bECPLF;
  }
  
  public String getBERUEF() {
    return BERUEF;
  }
  
  public void setBERUEF(String bERUEF) {
    BERUEF = bERUEF;
  }
  
  public String getBELOCF() {
    return BELOCF;
  }
  
  public void setBELOCF(String bELOCF) {
    BELOCF = bELOCF;
  }
  
  public String getBENOML() {
    return BENOML;
  }
  
  public void setBENOML(String bENOML) {
    BENOML = bENOML;
  }
  
  public String getBECPLL() {
    return BECPLL;
  }
  
  public void setBECPLL(String bECPLL) {
    BECPLL = bECPLL;
  }
  
  public String getBERUEL() {
    return BERUEL;
  }
  
  public void setBERUEL(String bERUEL) {
    BERUEL = bERUEL;
  }
  
  public String getBELOCL() {
    return BELOCL;
  }
  
  public void setBELOCL(String bELOCL) {
    BELOCL = bELOCL;
  }
  
  public String getTelClientFac() {
    return telClientFac;
  }
  
  public void setTelClientFac(String telClientFac) {
    this.telClientFac = telClientFac;
  }
  
  public String getTelClientLiv() {
    return telClientLiv;
  }
  
  public void setTelClientLiv(String telClientLiv) {
    this.telClientLiv = telClientLiv;
  }
  
  public ArrayList<String> getListeBlocNotes() {
    return listeBlocNotes;
  }
  
  public void setListeBlocNotes(ArrayList<String> listeBlocNotes) {
    this.listeBlocNotes = listeBlocNotes;
  }
  
  public boolean isTTC() {
    return isTTC;
  }
  
  public void setTTC(boolean isTTC) {
    this.isTTC = isTTC;
  }
  
  public String getRENUM() {
    return RENUM;
  }
  
  public void setRENUM(String rENUM) {
    RENUM = rENUM;
  }
  
  public String getServiceLivraison() {
    return serviceLivraison;
  }
  
  public void setServiceLivraison(String serviceLivraison) {
    this.serviceLivraison = serviceLivraison;
  }
  
  public String getPointRelais() {
    return pointRelais;
  }
  
  public void setPointRelais(String pointRelais) {
    this.pointRelais = pointRelais;
  }
  
  public String getPaysPointRelais() {
    return paysPointRelais;
  }
  
  public void setPaysPointRelais(String paysPointRelais) {
    this.paysPointRelais = paysPointRelais;
  }
  
  public String getBESAN() {
    return BESAN;
  }
  
  public void setBESAN(String bESAN) {
    BESAN = bESAN;
  }
  
  public String getFraisPortTTC() {
    return fraisPortTTC;
  }
  
  public void setFraisPortTTC(String fraisPortTTC) {
    this.fraisPortTTC = fraisPortTTC;
  }
  
  public String getFraisPortCommande() {
    return fraisPortCommande;
  }
  
  public void setFraisPortCommande(String fraisPortCommande) {
    this.fraisPortCommande = fraisPortCommande;
  }
  
}
