/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.database.record.GenericRecord;

/**
 * Article Série N au format Magento
 * Cette classe est une classe qui sert à l'intégration JAVA vers JSON
 * IL NE FAUT PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonArticleMagentoV2 extends JsonEntiteMagento {
  private transient String exclutWeb = null;
  // l'article a été mis sur le Web et visible
  public transient static final String SUR_LE_WEB = " ";
  // l'article n'a jamais été mis sur le Web et donc invisible
  public transient static final String JAMAIS_VU_SUR_LE_WEB = "1";
  // l'article a déjà été mis sur le Web mais doit être désormais invisible
  public transient static final String EXCLU_WEB = "2";
  private transient String A1UNV = null;
  
  private String code = null;
  private String codeEAN = null;
  private String libelle = null;
  private int statut = 0;
  private String referenceFabricant = null;
  private String marque = null;
  private String montantDEEE = null;
  private Float poids = null;
  private String largeur = null;
  private String longueur = null;
  private String profondeur = null;
  private boolean surPalette = false;
  private boolean nouveaute = false;
  private boolean gereEnStock = false;
  private boolean destockage = false;
  private int jeuAttributs = 0;
  private int pointsWatt = 0;
  
  private String tarif1 = null;
  private String tarif2 = null;
  private String tarif3 = null;
  private String tarif4 = null;
  private String tarif5 = null;
  private String tarif6 = null;
  private String tarif7 = null;
  private String tarif8 = null;
  private String tarif9 = null;
  private String tarif10 = null;
  
  private ArrayList<String> listeSites = null;
  
  /**
   * Constructeur de l'article Magento
   */
  public JsonArticleMagentoV2(FluxMagento record) {
    idFlux = record.getFLIDF();
    versionFlux = record.getFLVER();
    code = record.getFLCOD();
    etb = record.getFLETB();
    
    // Hors fiche article
    referenceFabricant = "";
  }
  
  /**
   * Permet de construire l'article sur le modèle Magento dans l'optique d'un
   * export typé
   */
  public boolean mettreAJourArticle(GenericRecord record) {
    if (record == null) {
      majError("[ArticleMagento] mettreAJourArticle() -> GenericRecord record NULL ");
      return false;
    }
    
    if (record.isPresentField("A1UNV")) {
      A1UNV = record.getField("A1UNV").toString().trim();
    }
    
    // CODE EAN de l'article
    if (record.isPresentField("GCD")) {
      codeEAN = record.getField("GCD").toString().trim();
    }
    else {
      codeEAN = "";
    }
    
    // LIBELLES DE L'ARTICLE
    if (!record.isPresentField("A1LB1")) {
      record.setField("A1LB1", "");
    }
    if (!record.isPresentField("A1LB2")) {
      record.setField("A1LB2", "");
    }
    if (!record.isPresentField("A1LB3")) {
      record.setField("A1LB3", "");
    }
    
    // TODO traitement spécifique pour un algo qui rend les libellés cohérents
    // libelle = record.getField("A1LIB").toString().trim() + " " + record.getField("A1LB1").toString().trim() + " " +
    // record.getField("A1LB2").toString().trim() + " " + record.getField("A1LB3").toString().trim();
    libelle = (record.getField("A1LIB").toString() + record.getField("A1LB1").toString() + record.getField("A1LB2").toString()
        + record.getField("A1LB3").toString()).trim();
    
    // STATUTS MAGENTO
    if (!record.isPresentField("A1NPU")) {
      record.setField("A1NPU", "1");
    }
    else if (record.isPresentField("A1NPU") && record.getField("A1NPU").toString().trim().length() == 0) {
      record.setField("A1NPU", "0");
    }
    
    try {
      statut = Integer.parseInt(record.getField("A1NPU").toString().trim());
      // Vu avec Marc le 27/04/16 Tous les statuts intermédiaires ou spécifiques n'ont pas de raison d'être passés à Magento on les rends
      // "actifs"
      // Du coup on inverse le bouzin pour Magento -> 0 pour inactif 1 pour actif
      // statuts 1inactif
      if (statut == 1) {
        statut = 0;
        // les autres sont considérés comme actifs
      }
      else {
        statut = 1;
      }
    }
    catch (Exception e) {
      // Si valeur incohérente
      statut = 0;
    }
    
    // marque
    if (record.isPresentField("A1REF1") && !record.getField("A1REF1").toString().trim().equals("")) {
      marque = record.getField("A1REF1").toString().trim();
    }
    else if (record.isPresentField("FRNOM") && !record.getField("FRNOM").toString().trim().equals("")) {
      marque = record.getField("FRNOM").toString().trim();
    }
    else {
      marque = "";
    }
    
    // DIMENSIONS
    if (record.isPresentField("A1PDS") && record.getField("A1PDS").toString().trim().length() > 0) {
      try {
        poids = Float.parseFloat(record.getField("A1PDS").toString().trim());
      }
      catch (Exception e) {
        poids = (float) 0;
      }
    }
    else {
      poids = (float) 0;
    }
    
    if (record.isPresentField("A1LRG")) {
      largeur = record.getField("A1LRG").toString().trim();
      // Accoler l'unité
      if (record.isPresentField("A1IN29")) {
        largeur += transformerUniteDeMesure(record.getField("A1IN29").toString().trim());
      }
    }
    else {
      largeur = "";
    }
    
    if (record.isPresentField("A1LNG")) {
      longueur = record.getField("A1LNG").toString().trim();
      // Accoler l'unité
      if (record.isPresentField("A1IN28")) {
        longueur += transformerUniteDeMesure(record.getField("A1IN28").toString().trim());
      }
    }
    else {
      longueur = "";
    }
    
    if (record.isPresentField("A1HTR")) {
      profondeur = record.getField("A1HTR").toString().trim();
      // Accoler l'unité
      if (record.isPresentField("A1IN30")) {
        profondeur += transformerUniteDeMesure(record.getField("A1IN30").toString().trim());
      }
    }
    else {
      profondeur = "";
    }
    
    // ZP et spécifiques MAGENTO
    // Article uniquement tranporté sur palette
    if (record.isPresentField("A1TOP3")) {
      surPalette = record.getField("A1TOP3").toString().trim().equals("PA");
    }
    // Nouvel article
    if (record.isPresentField("A1ABC")) {
      nouveaute = record.getField("A1ABC").toString().trim().equals("N");
    }
    // Article Départ fournisseur (si 0 pas géré en stock = départ fournisseur)
    if (record.isPresentField("A1STK")) {
      gereEnStock = record.getField("A1STK").toString().trim().equals("1");
    }
    // Articles Magento destockage
    // A1REA = 1 consommation moyenne ca semble être une connerie...
    // Du coup on a mis 8
    // Pascaline conseille d'utilisier A1NPU à 5 ou 6 pré fin de série / fin de série
    if (record.isPresentField("A1REA")) {
      destockage = record.getField("A1REA").toString().trim().equals("8");
    }
    // ZP jeu d'attributs
    if (record.isPresentField("A1TOP1")) {
      try {
        jeuAttributs = Integer.parseInt(record.getField("A1TOP1").toString().trim());
      }
      catch (NumberFormatException e) {
        jeuAttributs = 0;
      }
    }
    // ZP points Watt de l'article
    if (record.isPresentField("A1ZP21")) {
      try {
        // pointsWatt = Integer.parseInt(record.getField("A1TOP2").toString().trim());
        pointsWatt = Integer.parseInt(record.getField("A1ZP21").toString().trim());
      }
      catch (NumberFormatException e) {
        pointsWatt = 0;
      }
    }
    
    if (record.isPresentField("A1IN15")) {
      exclutWeb = record.getField("A1IN15").toString();
    }
    
    // TODO Pour l'instant on laisse tout passer faudra bien vérifier à l'avenir ce qui est restrictif ou non
    return true;
  }
  
  /**
   * permet de mettre à jour les tarifs de l'article pour interprétation dans Magento
   */
  public boolean gererLesTarifs(GenericRecord record) {
    if (record == null) {
      majError("Cet article ne possède pas encore de tarif valide.");
      // majError("[ArticleMagento] gererLesTarifs() -> Cet article ne possède pas encore de tarif valide ");
      return false;
    }
    
    if (record.isPresentField("ATP01") && !record.getField("ATP01").toString().trim().equals("")) {
      tarif1 = record.getField("ATP01").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif1 corrompu ");
    }
    
    if (record.isPresentField("ATP02") && !record.getField("ATP02").toString().trim().equals("")) {
      tarif2 = record.getField("ATP02").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif2 corrompu ");
    }
    
    if (record.isPresentField("ATP03") && !record.getField("ATP03").toString().trim().equals("")) {
      tarif3 = record.getField("ATP03").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif3 corrompu ");
    }
    
    if (record.isPresentField("ATP04") && !record.getField("ATP04").toString().trim().equals("")) {
      tarif4 = record.getField("ATP04").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif4 corrompu ");
    }
    
    if (record.isPresentField("ATP05") && !record.getField("ATP05").toString().trim().equals("")) {
      tarif5 = record.getField("ATP05").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif5 corrompu ");
    }
    
    if (record.isPresentField("ATP06") && !record.getField("ATP06").toString().trim().equals("")) {
      tarif6 = record.getField("ATP06").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif6 corrompu ");
    }
    
    if (record.isPresentField("ATP07") && !record.getField("ATP07").toString().trim().equals("")) {
      tarif7 = record.getField("ATP07").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif7 corrompu ");
    }
    
    if (record.isPresentField("ATP08") && !record.getField("ATP08").toString().trim().equals("")) {
      tarif8 = record.getField("ATP08").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif8 corrompu ");
    }
    
    if (record.isPresentField("ATP09") && !record.getField("ATP09").toString().trim().equals("")) {
      tarif9 = record.getField("ATP09").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif9 corrompu ");
    }
    
    if (record.isPresentField("ATP10") && !record.getField("ATP10").toString().trim().equals("")) {
      tarif10 = record.getField("ATP10").toString().trim();
    }
    else {
      majError("[ArticleMagento] gererLesTarifs() -> tarif10 corrompu ");
    }
    
    // isInitDesTarifs = (tarif1!=null && tarif2!=null && tarif3!=null && tarif4!=null && tarif5!=null && tarif6!=null && tarif7!=null &&
    // tarif8!=null && tarif9!=null && tarif10!=null);
    
    return (msgErreur != null);
  }
  
  /**
   * permet de remonter le montant DEEE de cet article pour la notion Magento
   */
  public void calculerMontantDEEE(String deee) {
    if (deee != null) {
      montantDEEE = deee;
    }
    else {
      montantDEEE = "0";
    }
  }
  
  /**
   * permet de récuperer les ID des sites Web qui rendent visibles cet article. Il peut ne pas y avoir de site coché.
   */
  public void gererVisibiliteDesSites(ArrayList<GenericRecord> liste) {
    // de toute manière on ne passe pas de NULL à Magento mais une liste vide le cas échéant
    listeSites = new ArrayList<String>();
    // Si on passe une liste existante
    if (liste != null) {
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).isPresentField("SICOD")) {
          listeSites.add(liste.get(i).getField("SICOD").toString().trim());
        }
      }
    }
  }
  
  /**
   * transforme un code d'unité de mesure en son libellé
   */
  private String transformerUniteDeMesure(String valeur) {
    if (valeur == null) {
      return "";
    }
    
    String retour = "";
    
    if (valeur.trim().equals("") || valeur.trim().equals("1")) {
      retour = "m";
    }
    else if (valeur.trim().equals("2")) {
      retour = "cm";
    }
    else if (valeur.trim().equals("3")) {
      retour = "mm";
    }
    
    return retour;
  }
  
  public String getCode() {
    return code;
  }
  
  public void setCode(String code) {
    this.code = code;
  }
  
  public String getCodeEAN() {
    return codeEAN;
  }
  
  public void setCodeEAN(String codeEAN) {
    this.codeEAN = codeEAN;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  public int getStatut() {
    return statut;
  }
  
  public void setStatut(int statut) {
    this.statut = statut;
  }
  
  public String getReferenceFabricant() {
    return referenceFabricant;
  }
  
  public void setReferenceFabricant(String referenceFabricant) {
    this.referenceFabricant = referenceFabricant;
  }
  
  public String getLargeur() {
    return largeur;
  }
  
  public void setLargeur(String largeur) {
    this.largeur = largeur;
  }
  
  public String getLongueur() {
    return longueur;
  }
  
  public void setLongueur(String longueur) {
    this.longueur = longueur;
  }
  
  public String getProfondeur() {
    return profondeur;
  }
  
  public void setProfondeur(String profondeur) {
    this.profondeur = profondeur;
  }
  
  public Float getPoids() {
    return poids;
  }
  
  public void setPoids(Float poids) {
    this.poids = poids;
  }
  
  public boolean isSurPalette() {
    return surPalette;
  }
  
  public void setSurPalette(boolean surPalette) {
    this.surPalette = surPalette;
  }
  
  public boolean isNouveaute() {
    return nouveaute;
  }
  
  public void setNouveaute(boolean nouveaute) {
    this.nouveaute = nouveaute;
  }
  
  public boolean isGereEnStock() {
    return gereEnStock;
  }
  
  public void setGereEnStock(boolean gereEnStock) {
    this.gereEnStock = gereEnStock;
  }
  
  public boolean isDestockage() {
    return destockage;
  }
  
  public void setDestockage(boolean destockage) {
    this.destockage = destockage;
  }
  
  public int getJeuAttributs() {
    return jeuAttributs;
  }
  
  public void setJeuAttributs(int jeuAttributs) {
    this.jeuAttributs = jeuAttributs;
  }
  
  public int getPointsWatt() {
    return pointsWatt;
  }
  
  public void setPointsWatt(int pointsWatt) {
    this.pointsWatt = pointsWatt;
  }
  
  public ArrayList<String> getListeSites() {
    return listeSites;
  }
  
  public void setListeSites(ArrayList<String> listeSites) {
    this.listeSites = listeSites;
  }
  
  public String getMontantDEEE() {
    return montantDEEE;
  }
  
  public void setMontantDEEE(String montantDEEE) {
    this.montantDEEE = montantDEEE;
  }
  
  public String getTarif1() {
    return tarif1;
  }
  
  public void setTarif1(String tarif1) {
    this.tarif1 = tarif1;
  }
  
  public String getTarif2() {
    return tarif2;
  }
  
  public void setTarif2(String tarif2) {
    this.tarif2 = tarif2;
  }
  
  public String getTarif3() {
    return tarif3;
  }
  
  public void setTarif3(String tarif3) {
    this.tarif3 = tarif3;
  }
  
  public String getTarif4() {
    return tarif4;
  }
  
  public void setTarif4(String tarif4) {
    this.tarif4 = tarif4;
  }
  
  public String getTarif5() {
    return tarif5;
  }
  
  public void setTarif5(String tarif5) {
    this.tarif5 = tarif5;
  }
  
  public String getTarif6() {
    return tarif6;
  }
  
  public void setTarif6(String tarif6) {
    this.tarif6 = tarif6;
  }
  
  public String getTarif7() {
    return tarif7;
  }
  
  public void setTarif7(String tarif7) {
    this.tarif7 = tarif7;
  }
  
  public String getTarif8() {
    return tarif8;
  }
  
  public void setTarif8(String tarif8) {
    this.tarif8 = tarif8;
  }
  
  public String getTarif9() {
    return tarif9;
  }
  
  public void setTarif9(String tarif9) {
    this.tarif9 = tarif9;
  }
  
  public String getTarif10() {
    return tarif10;
  }
  
  public void setTarif10(String tarif10) {
    this.tarif10 = tarif10;
  }
  
  public String getMarque() {
    return marque;
  }
  
  public void setMarque(String marque) {
    this.marque = marque;
  }
  
  public String getExclutWeb() {
    return exclutWeb;
  }
  
  public void setExclutWeb(String exclutWeb) {
    this.exclutWeb = exclutWeb;
  }
  
  public String getA1UNV() {
    return A1UNV;
  }
  
  public void setA1UNV(String a1unv) {
    A1UNV = a1unv;
  }
}
