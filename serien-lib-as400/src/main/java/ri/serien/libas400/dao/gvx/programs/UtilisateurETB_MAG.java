/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs;

import java.io.IOException;

import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.serien.libas400.dao.gvx.database.PgvmparmManager;
import ri.serien.libas400.dao.gvx.database.PgvmsecmManager;
import ri.serien.libas400.database.record.ExtendRecord;
import ri.serien.libas400.parameters.EtablissementMobilite;
import ri.serien.libas400.parameters.MagasinMobilite;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.acheteur.IdAcheteur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe provisoire permet de récupérer l'établisement et le magasin pour un utilisateur.
 * Il faut revoir complètement cette classe sur le modèle des classes que l'on fait actuellement.
 */
public class UtilisateurETB_MAG {
  private SystemeManager systeme = null;
  
  /**
   * Constructeur
   * @param system
   */
  public UtilisateurETB_MAG(SystemeManager system) {
    systeme = system;
  }
  
  /**
   * Indiquer la présence du module GVM dans une base de données.
   * 
   * La présence du module GVM est contrôlée via l'existence du fichier PGVMPARM (même si ce n'est pas la meilleure méthode).
   * 
   * @param pCurlib Base de données.
   * @return true=module GVM présent, false,module GVM absent.
   */
  private boolean isModuleGVMPresent(String pCurlib) {
    // Contrôler les paramètres
    if (Constantes.normerTexte(pCurlib).isEmpty()) {
      throw new MessageErreurException("La base de données est invalide.");
    }
    
    // Tester la présence du fichier PGVMPARM
    IFSFile pgvmparm = new IFSFile(systeme.getSystem(), QSYSObjectPathName.toPath(pCurlib, "PGVMPARM", "FILE"));
    try {
      return pgvmparm.exists();
    }
    catch (IOException e) {
      return false;
    }
  }
  
  /**
   * Lit les informations d'un utilisateur en gescom.
   */
  public UtilisateurGescom chargerUtilisateurGescom(String user, String curlib, IdEtablissement pIdEtablissement) {
    UtilisateurGescom utilisateur = new UtilisateurGescom();
    if (Constantes.normerTexte(curlib).isEmpty()) {
      return null;
    }
    
    if (!isModuleGVMPresent(curlib)) {
      return null;
    }
    user = Constantes.normerTexte(user).toUpperCase();
    
    PgvmsecmManager security = new PgvmsecmManager(systeme.getDatabaseManager().getConnection());
    security.setLibrary(curlib);
    PgvmparmManager pgvmparm = new PgvmparmManager(systeme.getDatabaseManager().getConnection());
    pgvmparm.setLibrary(curlib);
    
    // Lecture de la DG blanche
    EtablissementMobilite etablissementPilote = new EtablissementMobilite();
    pgvmparm.chargerInfosEtablissementPilote(etablissementPilote);
    
    // On recherche l'établissement de l'utilisateur
    EtablissementMobilite etablissementUser = new EtablissementMobilite();
    if (pIdEtablissement == null) {
      etablissementUser.setId(security.getEtablissementUser(user));
    }
    else {
      etablissementUser.setId(pIdEtablissement);
    }
    
    // Il en a pas, on prend celui de la DG blanche
    if (etablissementUser.getId() == null) {
      etablissementUser.setId(etablissementPilote.getId());
    }
    // On va récupérer les informations complémentaires de l'établissement
    pgvmparm.getInfosEtablissement(etablissementUser);
    
    ExtendRecord record = security.getRecordsbyUSERandETB(user, etablissementUser.getId());
    IdMagasin idMagasin = null;
    
    if (record != null) {
      // Par défaut, si le user n'a pas de magasin associé, on prend celui de l'établissement
      if (Constantes.convertirObjetEnTexte(record.getField("SEMAUS")).isEmpty()) {
        idMagasin = etablissementUser.getMagasinGeneral();
      }
      // Renseigner l'identifiant du magasin
      else {
        idMagasin = IdMagasin.getInstance(etablissementUser.getId(), Constantes.convertirObjetEnTexte(record.getField("SEMAUS")));
      }
      // Renseigner l'identifiant de l'acheteur
      String codeAcheteur = Constantes.convertirObjetEnTexte(record.getField("SEAHUS"));
      if (!codeAcheteur.isEmpty()) {
        IdAcheteur idAcheteur = IdAcheteur.getInstance(etablissementUser.getId(), codeAcheteur);
        utilisateur.setIdAcheteur(idAcheteur);
      }
      
      // Renseigner l'identifiant du vendeur
      String codeVendeur = Constantes.convertirObjetEnTexte(record.getField("SEPEUS"));
      if (!codeVendeur.isEmpty()) {
        IdVendeur idVendeur = IdVendeur.getInstance(etablissementUser.getId(), codeVendeur);
        utilisateur.setIdVendeur(idVendeur);
      }
    }
    // Par défaut, si le user n'a pas de magasin associé, on prend celui de l'établissement
    else {
      idMagasin = etablissementUser.getMagasinGeneral();
    }
    
    // Initialisation des infos de l'utilisateur
    utilisateur.setIdEtablissementPrincipal(etablissementUser.getId());
    utilisateur.setProfil(user);
    utilisateur.setIdMagasin(idMagasin);
    
    return utilisateur;
  }
  
  /**
   * Retourne l'établissement d'un utilisateur.
   */
  public IdEtablissement getEtablissement(String user, String curlib) {
    if (Constantes.normerTexte(curlib).isEmpty()) {
      return null;
    }
    
    if (!isModuleGVMPresent(curlib)) {
      return null;
    }
    
    try {
      PgvmsecmManager security = new PgvmsecmManager(systeme.getDatabaseManager().getConnection());
      security.setLibrary(curlib);
      PgvmparmManager pgvmparm = new PgvmparmManager(systeme.getDatabaseManager().getConnection());
      pgvmparm.setLibrary(curlib);
      
      // Lecture de la DG blanche
      EtablissementMobilite etablissementPilote = new EtablissementMobilite();
      pgvmparm.chargerInfosEtablissementPilote(etablissementPilote);
      
      // On recherche l'établissement de l'utilisateur
      EtablissementMobilite etbuser = new EtablissementMobilite();
      etbuser.setId(security.getEtablissementUser(user));
      // Il en a pas, on prend celui de la DG blanche
      if (etbuser.getId() == null) {
        etbuser.setId(etablissementPilote.getId());
      }
      
      return etbuser.getId();
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du chargement de l'établissement.");
      return null;
    }
  }
  
  /**
   * Charger et retourner l'identifiant établissement d'un utilisateur.
   * 
   * Cela récupére l'établissement paramétré dans la sécurité Gescom associée au profil. Si ce profil n'a pas de sécurité ou
   * d'établissement dans sa sécurité, cela retourne l'établissement par défaut récupéré dans la DG dite "blanche".
   * 
   * @param pProfil Profil de l'utilisateur dont il faut charger l'établissement.
   * @param pCurLib Base de données.
   * @return Identifiant de l'établissement de l'utilisateur.
   */
  public IdEtablissement chargerEtablissementUtilisateur(String pProfil, String pCurlib) {
    // Contrôler les paramètres
    if (Constantes.normerTexte(pProfil).isEmpty()) {
      throw new MessageErreurException("Le profil est invalide.");
    }
    if (Constantes.normerTexte(pCurlib).isEmpty()) {
      throw new MessageErreurException("La base de données est invalide.");
    }
    
    // Tester si le module GVM est présent
    if (!isModuleGVMPresent(pCurlib)) {
      throw new MessageErreurException("Le module GVM n'est pas présent.");
    }
    
    // Charger l'identifiant de l'établissement de l'utilisateur
    PgvmsecmManager security = new PgvmsecmManager(systeme.getDatabaseManager().getConnection());
    security.setLibrary(pCurlib);
    IdEtablissement idEtablissement = security.getEtablissementUser(pProfil);
    
    // Si le profil n'a pas d'établissement associé, on prend celui de la DG blanche
    if (idEtablissement == null) {
      // Lecture de la DG blanche
      PgvmparmManager pgvmparm = new PgvmparmManager(systeme.getDatabaseManager().getConnection());
      pgvmparm.setLibrary(pCurlib);
      EtablissementMobilite etablissementBlanc = new EtablissementMobilite();
      pgvmparm.chargerInfosEtablissementPilote(etablissementBlanc);
      idEtablissement = etablissementBlanc.getId();
    }
    return idEtablissement;
  }
  
  /**
   * Charger et retourner l'identifiant magasin d'un utilisateur.
   * 
   * Cette méthode est utilisée pour la Mobilité uniquement. Il faut refaire tous ces traitements proprement.
   * 
   * Cela récupére la magasin paramétré dans la sécurité Gescom associée au profil. Si ce profil n'a pas de sécurité ou
   * de magasin dans sa sécurité, cela retourne le magasin par défaut de l'établissement.
   * 
   * @param pProfil Profil de l'utilisateur dont il faut charger le magasin.
   * @param pCurLib Base de données.
   * @return Identifiant du magasin de l'utilisateur (null si aucun).
   */
  public IdMagasin chargerMagasinUtilisateur(String pProfil, String pCurlib) {
    // Contrôler les paramètres
    if (Constantes.normerTexte(pProfil).isEmpty()) {
      throw new MessageErreurException("L'utilisateur est invalide.");
    }
    if (Constantes.normerTexte(pCurlib).isEmpty()) {
      throw new MessageErreurException("La base de données est invalide.");
    }
    
    // Tester si le module GVM est présent
    if (!isModuleGVMPresent(pCurlib)) {
      throw new MessageErreurException("Le module GVM n'est pas présent.");
    }
    
    // Charger l'identifiant de l'établissement de l'utilisateur
    IdEtablissement idEtablissement = chargerEtablissementUtilisateur(pProfil, pCurlib);
    
    // Charger les informations de l'utilisateur
    PgvmsecmManager security = new PgvmsecmManager(systeme.getDatabaseManager().getConnection());
    security.setLibrary(pCurlib);
    ExtendRecord record = security.getRecordsbyUSERandETB(pProfil, idEtablissement);
    
    // Récupérer le magasin paramétré dans la sécurité de l'utilisateur
    IdMagasin idMagasin = null;
    if (record != null) {
      String codeMagasin = Constantes.convertirObjetEnTexte(record.getField("SEMAUS"));
      if (!codeMagasin.isEmpty()) {
        idMagasin = IdMagasin.getInstance(idEtablissement, codeMagasin);
      }
    }
    
    // Sinon, récupérer le magasin par défaut de l'établissement
    if (idMagasin == null) {
      // Créer un établissement pour charger ses paramètres
      EtablissementMobilite etablissement = new EtablissementMobilite();
      etablissement.setId(idEtablissement);
      
      // Charger les informations de l'établissement
      PgvmparmManager pgvmparm = new PgvmparmManager(systeme.getDatabaseManager().getConnection());
      pgvmparm.setLibrary(pCurlib);
      pgvmparm.getInfosEtablissement(etablissement);
      
      // Récupérer l'identifiant du magasin de l'établissement
      idMagasin = etablissement.getMagasinGeneral();
    }
    return idMagasin;
  }
  
  /**
   * Retourne les informations pour la barre de titre de la fenêtre de Série N.
   */
  public String getTitleBarInfos(String user, String curlib) {
    if (Constantes.normerTexte(curlib).isEmpty()) {
      return "";
    }
    
    if (!isModuleGVMPresent(curlib)) {
      return "";
    }
    
    PgvmsecmManager security = new PgvmsecmManager(systeme.getDatabaseManager().getConnection());
    security.setLibrary(curlib);
    PgvmparmManager pgvmparm = new PgvmparmManager(systeme.getDatabaseManager().getConnection());
    pgvmparm.setLibrary(curlib);
    
    // Lecture de la DG blanche
    EtablissementMobilite etablissementPilote = new EtablissementMobilite();
    pgvmparm.chargerInfosEtablissementPilote(etablissementPilote);
    
    // On recherche l'établissement de l'utilisateur
    EtablissementMobilite etbuser = new EtablissementMobilite();
    etbuser.setId(security.getEtablissementUser(user));
    // Il en a pas, on prend celui de la DG blanche
    if (etbuser.getId() == null) {
      etbuser.setId(etablissementPilote.getId());
    }
    // On va récupérer les informations complémentaires de l'établissement
    pgvmparm.getInfosEtablissement(etbuser);
    
    // Récupération des infos sur le magasin
    MagasinMobilite magasin = new MagasinMobilite();
    ExtendRecord record = security.getRecordsbyUSERandETB(user, etbuser.getId());
    // De l'établissement si le user n'en a pas
    if (record == null) {
      if (etbuser.getMagasinGeneral() != null) {
        magasin.setCodeMag(etbuser.getMagasinGeneral().getCode());
      }
    }
    else {
      magasin.setCodeMag(Constantes.convertirObjetEnTexte(record.getField("SEMAUS")));
    }
    // Récupération du libellé du code magasin
    pgvmparm.getInfosMagasin(etbuser.getId(), magasin);
    
    String mag = magasin.getCodeMag() + " " + magasin.getMALIB();
    if (Constantes.normerTexte(mag).isEmpty()) {
      return " - " + etablissementPilote.getId().getCodeEtablissement() + " " + etablissementPilote.getNom();
    }
    return " - " + etablissementPilote.getId().getCodeEtablissement() + " " + etablissementPilote.getNom() + " - " + mag;
  }
}
