/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.negociation;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgSauverNegociation extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0002";
  
  /**
   * Appel du programme RPG qui va écrire une négociation.
   */
  public void sauverNegociation(SystemeManager pSysteme, LigneVente pLigneVente, Date pDateTraitement, String pOptionTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    LigneVente.controlerId(pLigneVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0002i entree = new Svgvm0002i();
    Svgvm0002o sortie = new Svgvm0002o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pLigneVente.getId().getCodeEtablissement());
    entree.setPicod(pLigneVente.getId().getCodeEntete().getCode());
    entree.setPinum(pLigneVente.getId().getNumero());
    entree.setPisuf(pLigneVente.getId().getSuffixe());
    entree.setPinli(pLigneVente.getId().getNumeroLigne());
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPiopt(pOptionTraitement);
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
  }
}
