/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.flux.OutilsMagento;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxInit;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Ligne de commande Série N au format SFCC.
 * Cette classe sert à l'intégration JSON vers JAVA.
 * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS.
 */
public class JsonCommandeWebV3 extends JsonEntiteMagento {
  // Constantes
  private static final transient int LONGUEUR_VILLE = 30;
  
  // Variables
  // DONNEES PRIVEES NON ECHANGEES DANS LE FLUX
  // Code Client Série N livré
  private transient String BENCLP = null;
  // Suffixe client livré
  private transient String BENCLS = null;
  // Code client facturé sur 9 numérique et DIGITS
  private transient String BECLF = null;
  // Zones de bloc adresse FAC formaté
  private transient String BENOMF = null;
  private transient String BECPLF = null;
  private transient String BERUEF = null;
  private transient String BELOCF = null;
  // Zones de bloc adresse LIV formaté
  private transient String BENOML = null;
  private transient String BECPLL = null;
  private transient String BERUEL = null;
  private transient String BELOCL = null;
  private transient boolean isTTC = true;
  private transient String RENUM = null;
  // Section analytique de la commande
  // Dépend de l'ID du site Web de la commande
  private transient String BESAN = null;
  // Découpage du bloc notes pour une injection
  private transient ArrayList<String> listeBlocNotes = null;
  private transient String fraisPortCommande = null;
  // Découpage du commentaire de fin pour une injection
  private transient List<String> listeLibelleCommentaireFin = null;
  
  // DONNEES ECHANGEES DANS LE FLUX
  private int idSite = 0;
  private String idCommandeClient = null;
  private String statut = null;
  private String modeReglement = null;
  private String poids = null;
  private String blocNotes = null;
  private String dateCreation = null;
  private String totalTTC = null;
  private String totalHT = null;
  private String fraisPortHT = null;
  private String fraisPortTTC = null;
  private String remise = null;
  // livraison
  private String modeLivraison = null;
  private String codeTransporteur = null;
  // Code service Expedito (transporteur + département) choisi par le client
  private String serviceLivraison = null;
  // Point relais
  private String pointRelais = null;
  private String paysPointRelais = null;
  
  // Contrôles client créateur
  private String idClientWeb = null;
  private String idClient = null;
  private String siretClient = null;
  private String mailClient = null;
  
  // Client facturé
  private String idClientFac = null;
  private String societeFac = null;
  private String civiliteFac = null;
  private String complementNomFac = null;
  private String nomFac = null;
  private String rueFac = null;
  private String rue2Fac = null;
  private String cdpFac = null;
  private String villeFac = null;
  private String paysFac = null;
  private String telFac = null;
  // Client livré
  private String idClientLiv = null;
  private String societeLiv = null;
  private String civiliteLiv = null;
  private String nomLiv = null;
  private String complementNomLiv = null;
  private String rueLiv = null;
  private String rue2Liv = null;
  private String cdpLiv = null;
  private String villeLiv = null;
  private String paysLiv = null;
  private String telLiv = null;
  
  private ArrayList<JsonLigneCommandeV3> listeArticlesCommandes = null;
  // Ligne commentaire à afficher à la fin sur la commande
  private String commentaireFin = null;
  
  /**
   * Constructeur.
   */
  public JsonCommandeWebV3() {
  }
  
  /**
   * formater les données de base d'une commande (bloc adresses)
   */
  public void formaterDonnees() {
    // Entête de commande
    miseEnFormeBlocAdresseF();
    miseEnFormeBlocAdresseL();
    majCdpVilleImportFact();
    majCdpVilleImportLivr();
    // Mise en HT par défaut
    fraisPortCommande = fraisPortHT;
    
    // lignes de commande
    if (listeArticlesCommandes != null) {
      for (int i = 0; i < listeArticlesCommandes.size(); i++) {
        listeArticlesCommandes.get(i).formaterDonnees();
      }
    }
    
    // Découpe le bloc notes pour l'injection
    traiterBlocNotes();
    // Découpe le commentaire de fin
    traiterCommentaireFin();
  }
  
  /**
   * Mise en forme du bloc adresse facturé.
   */
  private void miseEnFormeBlocAdresseF() {
    // Modifier le nom du bloc adresse pour une société
    BENOMF = "";
    if (societeFac != null && !societeFac.trim().equals("")) {
      // Le nom de la société est renseigné
      BENOMF = societeFac;
    }
    // Modifier le nom du bloc adresse pour un particulier
    else if (nomFac != null) {
      // Le nom de la personne est renseigné
      BENOMF = nomFac.trim();
    }
    
    // Modifier le complément de nom du bloc adresse
    BECPLF = "";
    if (complementNomFac != null) {
      // Le complément de nom est mis tel quel sans modification que ce soit pour une société ou un particulier
      BECPLF = complementNomFac.trim();
    }
    
    // Modifier l'adresse
    BERUEF = "";
    if (rueFac != null) {
      BERUEF = rueFac;
    }
    // Modifier la localisation
    BELOCF = "";
    if (rue2Fac != null) {
      BELOCF = rue2Fac;
    }
  }
  
  /**
   * Mise en forme du bloc adresse livraison.
   */
  private void miseEnFormeBlocAdresseL() {
    // Modifier le nom du bloc adresse pour une société
    BENOML = "";
    if (societeLiv != null && !societeLiv.trim().equals("")) {
      // Le nom de la société est renseigné
      BENOML = societeLiv;
    }
    // Modifier le nom du bloc adresse pour un particulier
    else if (nomLiv != null) {
      // Le nom de la personne est renseigné
      BENOML = nomLiv.trim();
    }
    
    // Modifier le complément de nom du bloc adresse
    BECPLL = "";
    if (complementNomLiv != null) {
      // Le complément de nom est mis tel quel sans modification que ce soit pour une société ou un particulier
      BECPLL = complementNomLiv.trim();
    }
    
    // Modifier l'adresse
    BERUEL = "";
    if (rueLiv != null) {
      BERUEL = rueLiv;
    }
    // Modifier la localisation
    BELOCL = "";
    if (rue2Liv != null) {
      BELOCL = rue2Liv;
    }
  }
  
  /**
   * On formate les zones de codes postaux et de villes pour la gestion de Série N :
   * Si on a un code postal numérique on peut mettre le code postal dans la bonne zone cdpFact
   * alors la zone ville c'est [NNNNN][ ][AAAAAAAAA....] où N est numérique sur 5 DIGITS calé à droite
   * et représente le code postal du client et A est le nom de la ville sur 24 caractères Max
   * Si on a un code postal Alpha on colle du 00000 dans cdpFact
   * et on fait le découpage suivant dans la zone de ville CLVIL [00000][ ][AAAAAAAA....]
   * où 00000 signifie qu'on ne contrôle pas ce code postal et qu'il est intégré dans la partie AAA
   * Sous la forme AAAAA AAAAA....
   * Il faut donc le nom de la ville ne fasse plus que 16 caractères maximum
   * Ex : 00000 31840 AUSSONNE
   */
  private void majCdpVilleImportFact() {
    if (cdpFac == null) {
      cdpFac = "";
    }
    if (villeFac == null) {
      villeFac = "";
    }
    
    String vraiCodePostal = "";
    boolean cdpNumerique = true;
    
    // Formatage du code postal
    cdpFac = cdpFac.trim();
    if (cdpFac.length() > Adresse.LONGUEUR_CODEPOSTAL) {
      cdpFac = cdpFac.substring(0, Adresse.LONGUEUR_CODEPOSTAL);
    }
    vraiCodePostal = cdpFac;
    cdpNumerique = codePostalEstUnEntier(cdpFac);
    if (!cdpNumerique) {
      cdpFac = "00000";
    }
    vraiCodePostal = OutilsMagento.formaterUnCodePostalpourSN(vraiCodePostal);
    
    // Formatage de la ville
    villeFac = villeFac.trim();
    if (villeFac.length() > LONGUEUR_VILLE) {
      villeFac = villeFac.substring(0, LONGUEUR_VILLE);
    }
    
    // Code postal alphanumérique
    if (!cdpNumerique) {
      villeFac = vraiCodePostal.trim() + " " + villeFac;
      if (villeFac.length() > LONGUEUR_VILLE) {
        villeFac = villeFac.substring(0, LONGUEUR_VILLE);
      }
    }
  }
  
  private void majCdpVilleImportLivr() {
    if (cdpLiv == null) {
      cdpLiv = "";
    }
    if (villeLiv == null) {
      villeLiv = "";
    }
    
    String vraiCodePostal = "";
    boolean cdpNumerique = true;
    
    // Formatage du code postal
    cdpLiv = cdpLiv.trim();
    if (cdpLiv.length() > Adresse.LONGUEUR_CODEPOSTAL) {
      cdpLiv = cdpLiv.substring(0, Adresse.LONGUEUR_CODEPOSTAL);
    }
    vraiCodePostal = cdpLiv;
    cdpNumerique = codePostalEstUnEntier(cdpLiv);
    if (!cdpNumerique) {
      cdpLiv = "00000";
    }
    vraiCodePostal = OutilsMagento.formaterUnCodePostalpourSN(vraiCodePostal);
    
    // Formatage de la ville
    villeLiv = villeLiv.trim();
    if (villeLiv.length() > LONGUEUR_VILLE) {
      villeLiv = villeLiv.substring(0, LONGUEUR_VILLE);
    }
    
    // Code postal alphanumérique
    if (!cdpNumerique) {
      villeLiv = vraiCodePostal.trim() + " " + villeLiv;
      if (villeLiv.length() > LONGUEUR_VILLE) {
        villeLiv = villeLiv.substring(0, LONGUEUR_VILLE);
      }
    }
  }
  
  /**
   * Découpe le bloc notes pour l'adapter à une injection Série N.
   */
  private void traiterBlocNotes() {
    if (blocNotes == null) {
      return;
    }
    
    // TODO faudra faire mieux que tronquer les commentaires à terme
    if (blocNotes.length() > ParametresFluxInit.TAILLE_MAX_BLOC_NOTES) {
      blocNotes = blocNotes.substring(0, ParametresFluxInit.TAILLE_MAX_BLOC_NOTES);
    }
    
    // On formate pour le bouzin de Série N
    // On gère les commentaires de la commande
    if (blocNotes != null && !blocNotes.trim().isEmpty()) {
      GestionCommentaireCdeV3 gestionCommentaires = new GestionCommentaireCdeV3(blocNotes);
      listeBlocNotes = gestionCommentaires.getFormateCommentaire();
    }
  }
  
  /**
   * Découpe le commentaire de fin afin de faciliter son injection.
   * Ce commentaire est découpé en segments de 30 caractères (longueur libellé) et en autant de lignes complètes que nécessaires. Chaque
   * ligne est composée de 4 libellés. Donc le nombre de segments retournés sera obligatoirement un multiple de 4.
   */
  private void traiterCommentaireFin() {
    if (commentaireFin == null || commentaireFin.trim().length() == 0) {
      return;
    }
    
    try {
      // Calcul du nombre de segment nécessaire (où 30 est la longueur d'un libellé d'une ligne de bon et 4 et le nombre de libellés)
      int nombreLigne = commentaireFin.length() / (30 * 4);
      int modulo = commentaireFin.length() % (30 * 4);
      // Si la longueur du texte n'est pas égal à un multiple de 30 * 4 alors le numéro de ligne est incrémenté de 1 afin d'avoir le
      // nombre de lignes nécessaire
      if (modulo != 0) {
        nombreLigne = nombreLigne + 1;
      }
      int nombreSegment = nombreLigne * 4;
      
      // Découpage de la chaine de caractère en segments de longueur fixe
      String[] tableauLibelleCommentaireFin = Constantes.splitString(commentaireFin, nombreSegment, 30);
      if (tableauLibelleCommentaireFin == null || tableauLibelleCommentaireFin.length == 0) {
        return;
      }
      listeLibelleCommentaireFin = new ArrayList<String>(Arrays.asList(tableauLibelleCommentaireFin));
      
      // Insertion d'une ligne blanche au dessus et au dessous
      for (int i = 0; i < 4; i++) {
        listeLibelleCommentaireFin.add(0, "");
        listeLibelleCommentaireFin.add("");
      }
    }
    catch (Exception e) {
      throw new MessageErreurException("Erreur lors du traitement du commentaireFin: " + e.getMessage());
    }
  }
  
  /**
   * La commande devient TTC si le client est TTC.
   */
  public void passerEnTTC(BigDecimal pTauxTVAClient) {
    // Contrôle du taux de TVA
    if (pTauxTVAClient == null) {
      pTauxTVAClient = new BigDecimal(20);
    }
    pTauxTVAClient = pTauxTVAClient.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    // Calcul du coefficient pour appliquer la TVA
    BigDecimal coefficientTVA = BigDecimal.ONE.add(pTauxTVAClient.divide(new BigDecimal(100), MathContext.DECIMAL32));
    
    // Frais de port
    if (fraisPortTTC != null) {
      fraisPortCommande = fraisPortTTC;
    }
    else {
      try {
        if (fraisPortHT != null) {
          // TODO une méthode générique doit être mise en place pour l'arrondi Série N
          BigDecimal frais = new BigDecimal(fraisPortHT).multiply(coefficientTVA);
          frais = frais.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
          fraisPortCommande = frais.toString();
        }
      }
      catch (Exception e) {
        fraisPortCommande = null;
      }
    }
    // Remise
    try {
      if (remise != null) {
        BigDecimal remiseGlobale = new BigDecimal(remise).multiply(coefficientTVA);
        remiseGlobale = remiseGlobale.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
        remise = remiseGlobale.toString();
      }
    }
    catch (Exception e) {
      remise = null;
    }
    
    // Les tarifs
    if (listeArticlesCommandes != null) {
      for (int i = 0; i < listeArticlesCommandes.size(); i++) {
        listeArticlesCommandes.get(i).passerEnTTC();
      }
    }
  }
  
  /**
   * Retourne le poids formaté sous forme de chaine de caractères.
   */
  public String retournerPoidsFormate() {
    String retour = null;
    if (poids == null) {
      return retour;
    }
    
    try {
      // TODO on peut formater les virgules en .
      BigDecimal valeur = new BigDecimal(poids);
      // Contrôles d'arrondis
      retour = valeur.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP).toString();
    }
    catch (Exception e) {
      return null;
    }
    
    return retour;
  }
  
  // -- Accesseurs
  
  public String getIdCommandeClient() {
    return idCommandeClient;
  }
  
  public void setIdCommandeClient(String idCommandeClient) {
    this.idCommandeClient = idCommandeClient;
  }
  
  public String getIdClientWeb() {
    return idClientWeb;
  }
  
  public void setIdClientWeb(String idClientWeb) {
    this.idClientWeb = idClientWeb;
  }
  
  public int getIdSite() {
    return idSite;
  }
  
  public void setIdSite(int idSite) {
    this.idSite = idSite;
  }
  
  public String getStatut() {
    return statut;
  }
  
  public void setStatut(String statut) {
    this.statut = statut;
  }
  
  public String getModeLivraison() {
    return modeLivraison;
  }
  
  public void setModeLivraison(String modeLivraison) {
    this.modeLivraison = modeLivraison;
  }
  
  public String getModeReglement() {
    return modeReglement;
  }
  
  public void setModeReglement(String modeReglement) {
    this.modeReglement = modeReglement;
  }
  
  public String getPoids() {
    return poids;
  }
  
  public void setPoids(String poids) {
    this.poids = poids;
  }
  
  public String getBlocNotes() {
    return blocNotes;
  }
  
  public void setBlocNotes(String blocNotes) {
    this.blocNotes = blocNotes;
  }
  
  public String getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(String dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public String getTotalTTC() {
    return totalTTC;
  }
  
  public void setTotalTTC(String totalTTC) {
    this.totalTTC = totalTTC;
  }
  
  public String getTotalHT() {
    return totalHT;
  }
  
  public void setTotalHT(String totalHT) {
    this.totalHT = totalHT;
  }
  
  public String getFraisPortHT() {
    return fraisPortHT;
  }
  
  public void setFraisPortHT(String fraisPortHT) {
    this.fraisPortHT = fraisPortHT;
  }
  
  public String getRemise() {
    return remise;
  }
  
  public void setRemise(String remise) {
    this.remise = remise;
  }
  
  public String getMailClient() {
    return mailClient;
  }
  
  public void setMailClient(String mailClient) {
    this.mailClient = mailClient;
  }
  
  public String getSiretClient() {
    return siretClient;
  }
  
  public void setSiretClient(String siretClient) {
    this.siretClient = siretClient;
  }
  
  public String getCiviliteFac() {
    return civiliteFac;
  }
  
  public void setCiviliteFac(String civiliteFac) {
    this.civiliteFac = civiliteFac;
  }
  
  public String getNomFac() {
    return nomFac;
  }
  
  public void setNomFac(String nomFac) {
    this.nomFac = nomFac;
  }
  
  public String getComplementNomFac() {
    return complementNomFac;
  }
  
  public void setComplementNomFac(String complementNomFac) {
    this.complementNomFac = complementNomFac;
  }
  
  public String getRueFac() {
    return rueFac;
  }
  
  public void setRueFac(String rueFac) {
    this.rueFac = rueFac;
  }
  
  public String getCdpFac() {
    return cdpFac;
  }
  
  public void setCdpFac(String cdpFac) {
    this.cdpFac = cdpFac;
  }
  
  public String getVilleFac() {
    return villeFac;
  }
  
  public void setVilleFac(String villeFac) {
    this.villeFac = villeFac;
  }
  
  public String getPaysFac() {
    return paysFac;
  }
  
  public void setPaysFac(String paysFac) {
    this.paysFac = paysFac;
  }
  
  public String getCiviliteLiv() {
    return civiliteLiv;
  }
  
  public void setCiviliteLiv(String civiliteLiv) {
    this.civiliteLiv = civiliteLiv;
  }
  
  public String getNomLiv() {
    return nomLiv;
  }
  
  public void setNomLiv(String nomLiv) {
    this.nomLiv = nomLiv;
  }
  
  public String getComplementNomLiv() {
    return complementNomLiv;
  }
  
  public void setComplementNomLiv(String complementNomLiv) {
    this.complementNomLiv = complementNomLiv;
  }
  
  public String getRueLiv() {
    return rueLiv;
  }
  
  public void setRueLiv(String rueLiv) {
    this.rueLiv = rueLiv;
  }
  
  public String getCdpLiv() {
    return cdpLiv;
  }
  
  public void setCdpLiv(String cdpLiv) {
    this.cdpLiv = cdpLiv;
  }
  
  public String getVilleLiv() {
    return villeLiv;
  }
  
  public void setVilleLiv(String villeLiv) {
    this.villeLiv = villeLiv;
  }
  
  public String getPaysLiv() {
    return paysLiv;
  }
  
  public void setPaysLiv(String paysLiv) {
    this.paysLiv = paysLiv;
  }
  
  public ArrayList<JsonLigneCommandeV3> getListeArticlesCommandes() {
    return listeArticlesCommandes;
  }
  
  public void setListeArticlesCommande(ArrayList<JsonLigneCommandeV3> listeArticlesCommandes) {
    this.listeArticlesCommandes = listeArticlesCommandes;
  }
  
  public String getBENCLP() {
    return BENCLP;
  }
  
  public void setBENCLP(String bENCLP) {
    BENCLP = bENCLP;
  }
  
  public String getBENCLS() {
    return BENCLS;
  }
  
  public void setBENCLS(String bENCLS) {
    BENCLS = bENCLS;
  }
  
  public String getRue2Fac() {
    return rue2Fac;
  }
  
  public void setRue2Fac(String rue2Fac) {
    this.rue2Fac = rue2Fac;
  }
  
  public String getRue2Liv() {
    return rue2Liv;
  }
  
  public void setRue2Liv(String rue2Liv) {
    this.rue2Liv = rue2Liv;
  }
  
  public String getSocieteFac() {
    return societeFac;
  }
  
  public void setSocieteFac(String pSocieteFac) {
    this.societeFac = pSocieteFac;
  }
  
  public void setListeArticlesCommandes(ArrayList<JsonLigneCommandeV3> listeArticlesCommandes) {
    this.listeArticlesCommandes = listeArticlesCommandes;
  }
  
  public String getSocieteLiv() {
    return societeLiv;
  }
  
  public void setSocieteLiv(String societeLiv) {
    this.societeLiv = societeLiv;
  }
  
  public String getBENOMF() {
    return BENOMF;
  }
  
  public void setBENOMF(String bENOMF) {
    BENOMF = bENOMF;
  }
  
  public String getBECPLF() {
    return BECPLF;
  }
  
  public void setBECPLF(String bECPLF) {
    BECPLF = bECPLF;
  }
  
  public String getBERUEF() {
    return BERUEF;
  }
  
  public void setBERUEF(String bERUEF) {
    BERUEF = bERUEF;
  }
  
  public String getBELOCF() {
    return BELOCF;
  }
  
  public void setBELOCF(String bELOCF) {
    BELOCF = bELOCF;
  }
  
  public String getBENOML() {
    return BENOML;
  }
  
  public void setBENOML(String bENOML) {
    BENOML = bENOML;
  }
  
  public String getBECPLL() {
    return BECPLL;
  }
  
  public void setBECPLL(String bECPLL) {
    BECPLL = bECPLL;
  }
  
  public String getBERUEL() {
    return BERUEL;
  }
  
  public void setBERUEL(String bERUEL) {
    BERUEL = bERUEL;
  }
  
  public String getBELOCL() {
    return BELOCL;
  }
  
  public void setBELOCL(String bELOCL) {
    BELOCL = bELOCL;
  }
  
  public String getTelFac() {
    return telFac;
  }
  
  public void setTelFac(String telFac) {
    this.telFac = telFac;
  }
  
  public String getTelLiv() {
    return telLiv;
  }
  
  public void setTelLiv(String telLiv) {
    this.telLiv = telLiv;
  }
  
  public ArrayList<String> getListeBlocNotes() {
    return listeBlocNotes;
  }
  
  public void setListeBlocNotes(ArrayList<String> listeBlocNotes) {
    this.listeBlocNotes = listeBlocNotes;
  }
  
  public boolean isTTC() {
    return isTTC;
  }
  
  public void setTTC(boolean isTTC) {
    this.isTTC = isTTC;
  }
  
  public String getRENUM() {
    return RENUM;
  }
  
  public void setRENUM(String rENUM) {
    RENUM = rENUM;
  }
  
  public String getServiceLivraison() {
    return serviceLivraison;
  }
  
  public void setServiceLivraison(String serviceLivraison) {
    this.serviceLivraison = serviceLivraison;
  }
  
  public String getPointRelais() {
    return pointRelais;
  }
  
  public void setPointRelais(String pointRelais) {
    this.pointRelais = pointRelais;
  }
  
  public String getPaysPointRelais() {
    return paysPointRelais;
  }
  
  public void setPaysPointRelais(String paysPointRelais) {
    this.paysPointRelais = paysPointRelais;
  }
  
  public String getBESAN() {
    return BESAN;
  }
  
  public void setBESAN(String bESAN) {
    BESAN = bESAN;
  }
  
  public String getFraisPortTTC() {
    return fraisPortTTC;
  }
  
  public void setFraisPortTTC(String fraisPortTTC) {
    this.fraisPortTTC = fraisPortTTC;
  }
  
  public String getFraisPortCommande() {
    return fraisPortCommande;
  }
  
  public void setFraisPortCommande(String fraisPortCommande) {
    this.fraisPortCommande = fraisPortCommande;
  }
  
  public String getCodeTransporteur() {
    return codeTransporteur;
  }
  
  public void setCodeTransporteur(String codeTransporteur) {
    this.codeTransporteur = codeTransporteur;
  }
  
  public String getIdClient() {
    return idClient;
  }
  
  public void setIdClient(String pIdClient) {
    this.idClient = pIdClient;
  }
  
  public String getIdClientFac() {
    return idClientFac;
  }
  
  public void setIdClientFac(String pIdClientFac) {
    this.idClientFac = pIdClientFac;
  }
  
  public String getIdClientLiv() {
    return idClientLiv;
  }
  
  public void setIdClientLiv(String idClientLiv) {
    this.idClientLiv = idClientLiv;
  }
  
  public String getBECLF() {
    return BECLF;
  }
  
  public void setBECLF(String bECLF) {
    BECLF = bECLF;
  }
  
  public String getCommentaireFin() {
    return commentaireFin;
  }
  
  public void setCommentaireFin(String commentaireFin) {
    this.commentaireFin = commentaireFin;
  }
  
  public List<String> getListeLibelleCommentaireFin() {
    return listeLibelleCommentaireFin;
  }
}
