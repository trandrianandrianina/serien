/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.cgm.database.files;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pcgmpacm_ds_DG pour les DG
 * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
 */

public class Pcgmpacm_ds_DG extends DataStructureRecord {

  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGNOM"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGAD1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGETP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "DGNC"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 0), "DGMEP"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEXD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(15, 0), "DGNUM"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX1"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGPR1"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX2"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGPR2"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(15, 0), "DGSYS"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "DGPCE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGNAT"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "DGMAS"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "DGA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGVB1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGVB2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGEUR"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGDEV"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGDEP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGCGE"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "DGNMH"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGPRE"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), "DGBLO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DGNCO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DG445"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(13), "DGICS"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "DGID1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "DGID2"));

    length = 300;
  }
}
