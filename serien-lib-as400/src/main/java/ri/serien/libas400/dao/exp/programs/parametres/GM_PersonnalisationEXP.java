/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.programs.parametres;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.ibm.as400.access.Record;

import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.ExtendRecord;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class GM_PersonnalisationEXP extends BaseGroupDB {
  /**
   * Constructeur
   * @param database
   */
  public GM_PersonnalisationEXP(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Retourne la liste des civilitées possibles
   * @param etb
   * @return
   * 
   **/
  public LinkedHashMap<String, String> getListCivility(String etb) {
    LinkedHashMap<String, String> listCivility = new LinkedHashMap<String, String>();
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARFIL1, PARCLE, PARFIL from " + queryManager.getLibrary() + ".PSEMPARM where substring(PARCLE, 4, 2) = 'CI'",
        "ri.serien.libas400.dao.exp.database.Psemparm_ds_CI");
    
    // On alimente la hashmap avec les enregistrements trouvés
    if (listeRecord == null) {
      return listCivility;
    }
    
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      listCivility.put(((String) record.getField("INDIC")).substring(5, 8).trim(), ((String) record.getField("CILIB")).trim());
    }
    
    return listCivility;
  }
  
  /**
   * Retourne la liste des fonctions possibles
   * @param etb
   * @return
   * 
   **/
  public LinkedHashMap<String, String> getListClass(String etb) {
    LinkedHashMap<String, String> listClass = new LinkedHashMap<String, String>();
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(
        "Select PARTOP, PARFIL1, PARCLE, PARFIL from " + queryManager.getLibrary() + ".PSEMPARM where substring(PARCLE, 4, 2) = 'CC'",
        "ri.serien.libas400.dao.exp.database.Psemparm_ds_CC");
    
    // On alimente la hashmap avec les enregistrements trouvés
    if (listeRecord == null) {
      return listClass;
    }
    
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      listClass.put(((String) record.getField("INDIC")).substring(5, 8).trim(), ((String) record.getField("CCLIB")).trim());
    }
    
    return listClass;
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    queryManager = null;
    
  }
  
}
