/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

public class ExpeditionsEDI_GP extends EntiteEDI {
  // CONSTANTES
  // lignes d'entête
  private final String DECLA_ENT = "ENT";
  // lignes partenaires
  private final String DECLA_PAR = "PAR";
  
  private GenericRecord recordAssocie = null;
  
  private String id_entete_1 = null;
  private String commande_client_2 = null;
  private String date_commande_3 = null;
  private String date_livraison_7 = null;
  private String bon_livraison_10 = null;
  
  private String EANclientPayeur_2 = null;
  private String lib_clientPayeur_3 = null;
  private String EANfournisseur_4 = null;
  private String lib_fournisseur_5 = null;
  private String EANclientlivre_6 = null;
  private String lib_clientLivre_7 = null;
  private String EANexpediteur_8 = null;
  private String lib_expediteur_9 = null;
  
  private String devise_expedition = null;
  // private QueryManager query = null;
  
  private ArrayList<Ligne_Cde_EDI_GP> lignes = null;
  
  public ExpeditionsEDI_GP(ParametresFluxBibli pParams, GenericRecord record, QueryManager que) {
    super(pParams);
    recordAssocie = record;
    query = que;
    isValide = traitementContenu();
  }
  
  @Override
  protected boolean traitementContenu() {
    if (recordAssocie == null) {
      majError("[ExpeditionsEDI_GP] recordAssocie à NULL");
      return false;
    }
    id_gp_1 = "@GP";
    logiciel_edi_2 = "WEB@EDI";
    type_fichier_3 = "DESADV";
    format_fichier_4 = "STANDARD";
    
    id_entete_1 = DECLA_ENT;
    
    if (recordAssocie.isPresentField("E1CCT") && !recordAssocie.getField("E1CCT").toString().trim().equals("")) {
      commande_client_2 = recordAssocie.getField("E1CCT").toString().trim();
    }
    else {
      majError("[ExpeditionsEDI_GP] E1CCT corrompu ");
      return false;
    }
    
    if (recordAssocie.isPresentField("E1CRE") && !recordAssocie.getField("E1CRE").toString().trim().equals("0")) {
      date_commande_3 = formaterUneDateSN_EDI(recordAssocie.getField("E1CRE").toString().trim());
      if (date_commande_3 == null) {
        return false;
      }
    }
    else {
      majError("[ExpeditionsEDI_GP] E1CRE corrompu ");
      return false;
    }
    
    if (recordAssocie.isPresentField("E1EXP") && !recordAssocie.getField("E1EXP").toString().trim().equals("0")) {
      date_livraison_7 = formaterUneDateSN_EDI(recordAssocie.getField("E1EXP").toString().trim());
      if (date_livraison_7 == null) {
        return false;
      }
    }
    else {
      // Si la date d'expe est la même que la fac ils vident E1EXP et on prend E1FAC.... et oui...
      if (recordAssocie.isPresentField("E1FAC") && !recordAssocie.getField("E1FAC").toString().trim().equals("0")) {
        date_livraison_7 = formaterUneDateSN_EDI(recordAssocie.getField("E1FAC").toString().trim());
        if (date_livraison_7 == null) {
          return false;
        }
      }
      else {
        majError("[ExpeditionsEDI_GP] E1EXP vide et E1FAC aussi !! ");
        return false;
      }
    }
    
    if (recordAssocie.isPresentField("E1NUM") && !recordAssocie.getField("E1NUM").toString().trim().equals("")
        && recordAssocie.isPresentField("E1SUF") && !recordAssocie.getField("E1SUF").toString().trim().equals("")) {
      bon_livraison_10 = recordAssocie.getField("E1NUM").toString().trim() + recordAssocie.getField("E1SUF").toString().trim();
      if (bon_livraison_10 == null) {
        return false;
      }
    }
    else {
      majError("[ExpeditionsEDI_GP] E1NUM ou E1SUF corrompus ");
      return false;
    }
    
    GenericRecord recordPartenaire = null;
    // on met à jour le client livré
    if (recordAssocie.isPresentField("E1ETB") && recordAssocie.isPresentField("E1CLLP") && recordAssocie.isPresentField("E1CLLS")) {
      recordPartenaire = recupererInfosPartenaire(recordAssocie.getField("E1ETB").toString().trim(),
          recordAssocie.getField("E1CLLP").toString().trim(), recordAssocie.getField("E1CLLS").toString().trim());
      if (recordPartenaire != null) {
        if (recordPartenaire.isPresentField("GCGCD") && !recordPartenaire.getField("GCGCD").toString().trim().equals("")) {
          EANclientlivre_6 = recordPartenaire.getField("GCGCD").toString().trim();
        }
        else {
          majError("[ExpeditionsEDI_GP] traitementContenu() recordPartenaire GCGCD à NULL ");
          return false;
        }
        
        if (recordPartenaire.isPresentField("CLNOM") && !recordPartenaire.getField("CLNOM").toString().trim().equals("")) {
          lib_clientLivre_7 = recordPartenaire.getField("CLNOM").toString().trim();
        }
        else {
          majError("[ExpeditionsEDI_GP] traitementContenu() recordPartenaire CLNOM à NULL ");
          return false;
        }
      }
      else {
        return false;
      }
    }
    else {
      majError("[ExpeditionsEDI_GP] traitementContenu() E1ETB, E1CLLP ou E1CLLS à NULL ");
      return false;
    }
    
    recordPartenaire = null;
    // on met à jour le client facturé
    if (recordAssocie.isPresentField("E1ETB") && recordAssocie.isPresentField("E1CLFP") && recordAssocie.isPresentField("E1CLFS")) {
      recordPartenaire = recupererInfosPartenaire(recordAssocie.getField("E1ETB").toString().trim(),
          recordAssocie.getField("E1CLFP").toString().trim(), recordAssocie.getField("E1CLFS").toString().trim());
      if (recordPartenaire != null) {
        if (recordPartenaire.isPresentField("GCGCD") && !recordPartenaire.getField("GCGCD").toString().trim().equals("")) {
          EANclientPayeur_2 = recordPartenaire.getField("GCGCD").toString().trim();
        }
        else {
          majError("[ExpeditionsEDI_GP] traitementContenu() recordPartenaire 2 GCGCD à NULL ");
          return false;
        }
        
        if (recordPartenaire.isPresentField("CLNOM") && !recordPartenaire.getField("CLNOM").toString().trim().equals("")) {
          lib_clientPayeur_3 = recordPartenaire.getField("CLNOM").toString().trim();
        }
        else {
          majError("[ExpeditionsEDI_GP] traitementContenu() recordPartenaire 2 CLNOM à NULL ");
          return false;
        }
      }
      else {
        return false;
      }
    }
    else {
      majError("[ExpeditionsEDI_GP] traitementContenu() 2 E1ETB, E1CLFP ou E1CLFS à NULL ");
      return false;
    }
    
    EANfournisseur_4 = parametresFlux.getEAN_founisseur_EDI();
    lib_fournisseur_5 = parametresFlux.getLib_founisseur_EDI();
    
    // TODO FAUDRA FAIRE UN TRUC PROPRE AUTOUR DE LA DEVISE
    devise_expedition = "EUR";
    
    return true;
  }
  
  /**
   * On met à jour le slignes de la commande
   */
  public boolean majDesLignes(ArrayList<GenericRecord> lignesBrutes) {
    isValide = false;
    
    if (lignesBrutes == null) {
      majError("[ExpeditionsEDI_GP] majDesLignes() GenericRecord recordTemp lignesBrutes NULL");
      return isValide;
    }
    
    lignes = new ArrayList<Ligne_Cde_EDI_GP>();
    int compteur = 0;
    
    for (int i = 0; i < lignesBrutes.size(); i++) {
      Ligne_Cde_EDI_GP lignePartielle = new Ligne_Cde_EDI_GP(parametresFlux, lignesBrutes.get(i));
      if (lignePartielle.isValide()) {
        lignes.add(lignePartielle);
        compteur++;
      }
      else {
        majError("[ExpeditionsEDI_GP] majDesLignes() PB ligne invalide " + lignePartielle.getMsgError());
      }
    }
    
    if (compteur == lignesBrutes.size() && compteur > 0) {
      isValide = true;
    }
    else {
      majError("[ExpeditionsEDI_GP] majDesLignes() compteur <> lignesBrutes -> compteur: " + compteur);
      return isValide;
    }
    
    return isValide;
  }
  
  public String getCommande_client_2() {
    return commande_client_2;
  }
  
  public void setCommande_client_2(String commande_client_2) {
    this.commande_client_2 = commande_client_2;
  }
  
  public String getDate_commande_3() {
    return date_commande_3;
  }
  
  public void setDate_commande_3(String date_commande_3) {
    this.date_commande_3 = date_commande_3;
  }
  
  public String getDate_livraison_7() {
    return date_livraison_7;
  }
  
  public void setDate_livraison_7(String date_livraison_7) {
    this.date_livraison_7 = date_livraison_7;
  }
  
  public String getBon_livraison_10() {
    return bon_livraison_10;
  }
  
  public void setBon_livraison_10(String bon_livraison_10) {
    this.bon_livraison_10 = bon_livraison_10;
  }
  
  public String getEANfournisseur_4() {
    return EANfournisseur_4;
  }
  
  public void setEANfournisseur_4(String eANfournisseur_4) {
    EANfournisseur_4 = eANfournisseur_4;
  }
  
  public String getLib_fournisseur_5() {
    return lib_fournisseur_5;
  }
  
  public void setLib_fournisseur_5(String lib_fournisseur_5) {
    this.lib_fournisseur_5 = lib_fournisseur_5;
  }
  
  public String getEANexpediteur_8() {
    return EANexpediteur_8;
  }
  
  public void setEANexpediteur_8(String eANexpediteur_8) {
    EANexpediteur_8 = eANexpediteur_8;
  }
  
  public String getLib_expediteur_9() {
    return lib_expediteur_9;
  }
  
  public void setLib_expediteur_9(String lib_expediteur_9) {
    this.lib_expediteur_9 = lib_expediteur_9;
  }
  
  public ArrayList<Ligne_Cde_EDI_GP> getLignes() {
    return lignes;
  }
  
  public void setLignes(ArrayList<Ligne_Cde_EDI_GP> lignes) {
    this.lignes = lignes;
  }
  
  public String getId_entete_1() {
    return id_entete_1;
  }
  
  public void setId_entete_1(String id_entete_1) {
    this.id_entete_1 = id_entete_1;
  }
  
  public String getDECLA_ENT() {
    return DECLA_ENT;
  }
  
  public String getDECLA_PAR() {
    return DECLA_PAR;
  }
  
  public String getDevise_expedition() {
    return devise_expedition;
  }
  
  public void setDevise_expedition(String devise_expedition) {
    this.devise_expedition = devise_expedition;
  }
  
  public String getEANclientPayeur_2() {
    return EANclientPayeur_2;
  }
  
  public void setEANclientPayeur_2(String eANclientPayeur_2) {
    EANclientPayeur_2 = eANclientPayeur_2;
  }
  
  public String getLib_clientPayeur_3() {
    return lib_clientPayeur_3;
  }
  
  public void setLib_clientPayeur_3(String lib_clientPayeur_3) {
    this.lib_clientPayeur_3 = lib_clientPayeur_3;
  }
  
  public String getEANclientlivre_6() {
    return EANclientlivre_6;
  }
  
  public void setEANclientlivre_6(String eANclientlivre_6) {
    EANclientlivre_6 = eANclientlivre_6;
  }
  
  public String getLib_clientLivre_7() {
    return lib_clientLivre_7;
  }
  
  public void setLib_clientLivre_7(String lib_clientLivre_7) {
    this.lib_clientLivre_7 = lib_clientLivre_7;
  }
  
}
