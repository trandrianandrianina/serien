/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlesaapprovisionner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.prix.ParametrePrixAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.achat.reapprovisionnement.ReapprovisionnementClient;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.EnumTypeDecoupeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

/**
 * Classe qui encapsule le programme RPG SVGAM0008.
 */
public class RpgChargerListeArticlesAApprovisionner extends RpgBase {
  // Constantes
  private static final String PROGRAMME = "SVGAM0008";
  private static final int NOMBRE_MAX_LIGNE_VENTE = 65;
  
  /**
   * Utilise un programme RPG pour lire les données d'une liste de ligne de vente dont les articles sont en rupture.
   * Le programme RPG peut traiter 65 lignes de vente à la fois, il est appelé en boucle pour l'intégralité des lignes de vente à lire.
   */
  public List<ReapprovisionnementClient> chargerListeArticlesAApprovisionner(SystemeManager pSysteme,
      List<IdLigneVente> pListeIdLigneVente, ParametresLireArticle pParametres) {
    List<ReapprovisionnementClient> listeReapprovisionnement = new ArrayList<ReapprovisionnementClient>();
    int indexDepart = 0;
    while (indexDepart < pListeIdLigneVente.size()) {
      int indexFin = Math.min(indexDepart + NOMBRE_MAX_LIGNE_VENTE - 1, pListeIdLigneVente.size() - 1);
      lireListe65LigneVente(pSysteme, listeReapprovisionnement, pListeIdLigneVente, indexDepart, indexFin, pParametres);
      indexDepart = indexFin + 1;
    }
    return listeReapprovisionnement;
  }
  
  /**
   * Appeler le programme RPG qui peut lire les données de 65 articles.
   * TODO Il faut l'IdDocumentVente
   */
  private void lireListe65LigneVente(SystemeManager pSysteme, List<ReapprovisionnementClient> pListeReapprovisionnement,
      List<IdLigneVente> pListeIdLigneVente, int indexDepart, int indexFin, ParametresLireArticle pParametres) {
    if (pSysteme == null || pListeIdLigneVente == null || pParametres == null || pListeReapprovisionnement == null) {
      throw new MessageErreurException("Erreur lors de la lecture des données pour le réapprovisionnement.");
    }
    if (indexFin - indexDepart + 1 > NOMBRE_MAX_LIGNE_VENTE) {
      throw new MessageErreurException("Il n'est pas possible de lire plus de " + NOMBRE_MAX_LIGNE_VENTE + " lignes de vente à la fois.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0008i entree = new Svgam0008i();
    Svgam0008o sortie = new Svgam0008o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialisation des paramètres d'entrée
    char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
    entree.setPiind(new String(indicateurs));
    entree.setPietb(pParametres.getIdEtablissement().getCodeEtablissement());
    entree.setPimag(pParametres.getIdMagasin().getCode());
    
    if (pParametres.getIdFournisseur() != null) {
      entree.setPicol(pParametres.getIdFournisseur().getCollectif());
      entree.setPifrs(pParametres.getIdFournisseur().getNumero());
    }
    
    int index = indexDepart;
    entree.setPiid01(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid02(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid03(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid04(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid05(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid06(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid07(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid08(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid09(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid10(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid11(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid12(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid13(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid14(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid15(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid16(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid17(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid18(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid19(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid20(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid21(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid22(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid23(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid24(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid25(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid26(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid27(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid28(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid29(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid30(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid31(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid32(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid33(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid34(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid35(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid36(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid37(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid38(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid39(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid40(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid41(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid42(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid43(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid44(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid45(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid46(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid47(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid48(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid49(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid50(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid51(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid52(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid53(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid54(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid55(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid56(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid57(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid58(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid59(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid60(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid61(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid62(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid63(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid64(alimenterIdLigneVente(pListeIdLigneVente, index++));
    entree.setPiid65(alimenterIdLigneVente(pListeIdLigneVente, index++));
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécuter le programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Renseigner le résultat
      for (Object ligne : sortie.getValeursBrutesLignes()) {
        if (ligne == null) {
          break;
        }
        ReapprovisionnementClient reapprovisionnementClient = completerInfoReappro((Object[]) ligne, pParametres.getIdFournisseur());
        pListeReapprovisionnement.add(reapprovisionnementClient);
      }
    }
  }
  
  /**
   * Méthode interne pour alimenter facilement les 65 champs comportant l'id des lignes de vente.
   */
  private String alimenterIdLigneVente(List<IdLigneVente> pListeIdLigneVente, int index) {
    if (index < pListeIdLigneVente.size()) {
      return pListeIdLigneVente.get(index).getIndicatif();
    }
    else {
      return "";
    }
  }
  
  /**
   * Créer une classe ReapprovisionnementClient à partir des données d'une ligne renvoyée par le programme RPG.
   */
  private ReapprovisionnementClient completerInfoReappro(Object[] line, IdFournisseur pIdFournisseur) {
    ReapprovisionnementClient reapprovisionnement = new ReapprovisionnementClient();
    
    // Les données de la ligne de vente
    IdEtablissement idEtablissement = pIdFournisseur.getIdEtablissement();
    EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.valueOfByCode(((String) line[Svgam0008d.VAR_WCOD]).charAt(0));
    Integer numero = ((BigDecimal) line[Svgam0008d.VAR_WNUM]).intValue();
    Integer suffixe = ((BigDecimal) line[Svgam0008d.VAR_WSUF]).intValue();
    Integer numeroLigne = ((BigDecimal) line[Svgam0008d.VAR_WNLI]).intValue();
    IdLigneVente idLigneVente = IdLigneVente.getInstance(idEtablissement, codeEntete, numero, suffixe, numeroLigne);
    reapprovisionnement.setIdLigneVente(idLigneVente);
    
    // L'id du document de vente
    Integer numeroFacture = ((BigDecimal) line[Svgam0008d.VAR_WNFA]).intValue();
    IdDocumentVente idDocumentVente = IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, numero, suffixe, numeroFacture);
    reapprovisionnement.setIdDocumentVente(idDocumentVente);
    
    // Les données du client
    numero = ((BigDecimal) line[Svgam0008d.VAR_WCLLP]).intValue();
    suffixe = ((BigDecimal) line[Svgam0008d.VAR_WCLLS]).intValue();
    IdClient idClient = IdClient.getInstance(idEtablissement, numero, suffixe);
    Client client = new Client(idClient);
    client.getAdresse().setNom(((String) line[Svgam0008d.VAR_WNOM]));
    reapprovisionnement.setClient(client);
    
    // Date de livraison prévue
    reapprovisionnement.setDateLivraisonPrevue(ConvertDate.db2ToDate(((BigDecimal) line[Svgam0008d.VAR_WDLP]).intValue(), null));
    
    // Les données de l'article
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, ((String) line[Svgam0008d.VAR_WART]));
    Article article = new Article(idArticle);
    article.setArticleCharge(false);
    article.setLibelle1(((String) line[Svgam0008d.VAR_WLIB]));
    
    /**
     * TYPE D'ARTICLE ET DECOUPE
     * L'information est stockée dans plusieurs champs en base. On les regroupe de manière compréhensible dans l'objet métier Article.
     * 
     * Article spécial -> code spécial == 1 (A1SPE)
     * Article commentaire -> pas de famille (A1FAM), top ne plus utilisé == 0 (A1NPU), zoneABC différente de R (A1ABC)
     * Article palette -> top spécifique == 0 (A1TSP)
     * Article transport -> top spécifique == T (A1TSP)
     * Article négoce -> tout ce qui n'est pas les 4 autres.
     * Article hors gamme -> code ABC == 'R' (A1ABC)
     * Article non stocké -> WA1STK == 1 && VAR_WSPE <= 0 (car les spéciaux sont aussi non stockés et il faut les différencier)
     */
    article.setTopNePlusUtiliser((String) line[Svgam0008d.VAR_WNPU]);
    String topSpecifique = (String) line[Svgam0008d.VAR_WTSP];
    if (article.getTopNePlusUtiliser().equals("1")) {
      article.setTypeArticle(EnumTypeArticle.PLUS_UTILISE);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (topSpecifique.equals("T")) {
      article.setTypeArticle(EnumTypeArticle.TRANSPORT);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (topSpecifique.equals("*")) {
      article.setTypeArticle(EnumTypeArticle.COMMENTAIRE);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (topSpecifique.equals("0")) {
      article.setTypeArticle(EnumTypeArticle.PALETTE);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (topSpecifique.equals("1")) {
      article.setTypeArticle(EnumTypeArticle.STANDARD);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.LONGUEUR);
    }
    else if (topSpecifique.equals("3")) {
      article.setTypeArticle(EnumTypeArticle.STANDARD);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.VOLUME);
    }
    else if (topSpecifique.equals("4")) {
      article.setTypeArticle(EnumTypeArticle.STANDARD);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.SURFACE);
    }
    else if (((BigDecimal) line[Svgam0008d.VAR_WA1STK]).compareTo(BigDecimal.ONE) == 0
        && ((BigDecimal) line[Svgam0008d.VAR_WSPE]).intValue() <= 0) {
      article.setTypeArticle(EnumTypeArticle.NON_STOCKE);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else if (((BigDecimal) line[Svgam0008d.VAR_WSPE]).intValue() > 0) {
      article.setTypeArticle(EnumTypeArticle.SPECIAL);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    else {
      article.setTypeArticle(EnumTypeArticle.STANDARD);
      article.setTypeDecoupe(EnumTypeDecoupeArticle.IMPOSSIBLE);
    }
    
    // Renseigner la classe de rotation
    String classeRotation = ((String) line[Svgam0008d.VAR_WABC]).trim();
    if (classeRotation.length() == 1) {
      article.setClasseRotation(classeRotation.charAt(0));
    }
    
    article.setIdFournisseur(pIdFournisseur);
    article.setQuantitePhysique(((BigDecimal) line[Svgam0008d.VAR_WSTK]));
    article.setQuantiteReserveeUCA(((BigDecimal) line[Svgam0008d.VAR_WRES]));
    article.setQuantiteAffecteeUCA(((BigDecimal) line[Svgam0008d.VAR_WAFF]));
    article.setQuantiteAttendueUCA(((BigDecimal) line[Svgam0008d.VAR_WATT]));
    article.setQuantiteDisponibleVente(((BigDecimal) line[Svgam0008d.VAR_WDIV]));
    article.setQuantiteDisponibleAchat(((BigDecimal) line[Svgam0008d.VAR_WDIA]));
    article.setQuantiteStockMaximumUCA(((BigDecimal) line[Svgam0008d.VAR_WMAX]));
    if (!((String) line[Svgam0008d.VAR_WUS]).trim().isEmpty()) {
      IdUnite idUS = IdUnite.getInstance((String) line[Svgam0008d.VAR_WUS]);
      article.setIdUS(idUS);
    }
    
    ParametrePrixAchat parametre = new ParametrePrixAchat();
    parametre.setQuantiteReliquatUCA(((BigDecimal) line[Svgam0008d.VAR_WQTC]));
    if (!((String) line[Svgam0008d.VAR_WUCA]).trim().isEmpty()) {
      IdUnite idUCA = IdUnite.getInstance((String) line[Svgam0008d.VAR_WUCA]);
      parametre.setIdUCA(idUCA);
    }
    if (!((String) line[Svgam0008d.VAR_WUA]).trim().isEmpty()) {
      IdUnite idUA = IdUnite.getInstance((String) line[Svgam0008d.VAR_WUA]);
      parametre.setIdUA(idUA);
    }
    
    parametre.setNombreUAParUCA(((BigDecimal) line[Svgam0008d.VAR_WKUAUCA]));
    parametre.setNombreUSParUCA(((BigDecimal) line[Svgam0008d.VAR_WKUSUCA]));
    parametre.setNombreUCAParUCS(((BigDecimal) line[Svgam0008d.VAR_WKUCAUCS]));
    parametre.setNombreUSParUCS(((BigDecimal) line[Svgam0008d.VAR_WKUSUCS]));
    
    parametre.setNombreDecimaleUA(((BigDecimal) line[Svgam0008d.VAR_WDCA]).intValue());
    parametre.setNombreDecimaleUCA(((BigDecimal) line[Svgam0008d.VAR_WDCC]).intValue());
    parametre.setNombreDecimaleUS(((BigDecimal) line[Svgam0008d.VAR_WDCS]).intValue());
    
    article.setPrixAchat(new PrixAchat());
    article.getPrixAchat().initialiser(parametre);
    reapprovisionnement.setArticle(article);
    
    return reapprovisionnement;
  }
}
