/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_DG pour les DG
 * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
 */
public class Pgvmparm_ds_DG extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGNOM"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(25), "DGCPL"));
    // A contrôler car à l'origine c'est une zone packed
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(18, 0), "DGT"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "DGTPF"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(11, 0), "DGDIVG"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX1G"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGDATG"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGEX0G"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "DGETP"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(9, 0), "DGCMAG"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 0), "DGNUM"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(3, 0), "DGNBS"));
    
    length = 200;
  }
}
