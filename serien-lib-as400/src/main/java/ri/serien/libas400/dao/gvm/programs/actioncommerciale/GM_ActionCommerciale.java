/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs.actioncommerciale;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import ri.serien.libas400.dao.gvm.programs.parametres.GM_PersonnalisationGVM;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

/**
 * Gestion des actions commerciales
 */
public class GM_ActionCommerciale extends BaseGroupDB {
  // Variables
  private LinkedHashMap<String, String> listObjectAction = new LinkedHashMap<String, String>();
  // ETB à blanc car pour l'instant son intérêt n'est pas démontré et EXP ne gère pas vraiment l'ETB
  private String etablissement = "";
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public GM_ActionCommerciale(QueryManager aquerymg) {
    super(aquerymg);
    loadListObjectAction();
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Initialise l'établissemment
   * @param etb
   *
   *          public void initialization(String etb)
   *          {
   *          // Initialisation des variables
   *          //actionCommerciale.initialization();
   *
   *          // Initialisation de l'établissement
   *          setEtablissement(etb);
   *          }
   */
  
  /**
   * Insère une action commerciale dans les tables
   * @param ac
   * @return
   */
  public boolean insertInDatabase(M_ActionCommerciale ac) {
    if (ac == null) {
      msgErreur += "\nLa classe ActionCommerciale est nulle.";
      return false;
    }
    
    if (!ac.insertInDatabase()) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Insère une liste d'action commerciale dans les tables
   * @param lac
   * @return
   */
  public boolean insertInDatabase(ArrayList<M_ActionCommerciale> lac) {
    if (lac == null) {
      msgErreur += "\nLa liste d'ActionCommerciale est nulle.";
      return false;
    }
    
    for (M_ActionCommerciale ac : lac) {
      if (!insertInDatabase(ac)) {
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * Lecture d'un lot d'actions commeciales
   * @param cond sans le "where"
   * @return
   */
  public M_ActionCommerciale[] readInDatabase(String cond) {
    String requete =
        "select * from " + queryManager.getLibrary() + ".PSEMEVTM evt, " + queryManager.getLibrary() + ".PGVMACCM acc where ETID = ACID";
    if ((cond != null) && (!cond.trim().equals(""))) {
      if (!cond.trim().toLowerCase().startsWith("and")) {
        requete += " and " + cond;
      }
      else {
        requete += " " + cond;
      }
    }
    return request4ReadActionCommercial(requete);
  }
  
  /**
   * Modifie une action commerciale dans les tables
   * @param ac
   * @return
   */
  public boolean modifyInDatabase(M_ActionCommerciale ac) {
    if (ac == null) {
      msgErreur += "\nLa classe ActionCommerciale est nulle.";
      return false;
    }
    
    if (!ac.updateInDatabase()) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Supprime une action commerciale dans les tables
   * @param ac
   * @return
   */
  public boolean deleteInDatabase(M_ActionCommerciale ac) {
    if (ac == null) {
      msgErreur += "\nLa classe ActionCommerciale est nulle.";
      return false;
    }
    
    if (!ac.deleteInDatabase()) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Supprime une liste d'action commerciale dans les tables
   * @param lac
   * @return
   */
  public boolean deleteInDatabase(ArrayList<M_ActionCommerciale> lac) {
    if (lac == null) {
      msgErreur += "\nLa liste d'ActionCommerciale est nulle.";
      return false;
    }
    
    for (M_ActionCommerciale ac : lac) {
      if (!deleteInDatabase(ac)) {
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * Charge la liste des objets pour les actions commerciales depuis le fichier paramètre de GVM
   */
  public void loadListObjectAction() {
    GM_PersonnalisationGVM pgvmparm = new GM_PersonnalisationGVM(queryManager);
    listObjectAction = pgvmparm.getListObjectAction(etablissement);
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    queryManager = null;
    
    if (listObjectAction != null) {
      listObjectAction.clear();
    }
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Requête permettant la lecture d'un lot d'actions commeciales
   * @param requete
   * @return
   */
  private M_ActionCommerciale[] request4ReadActionCommercial(String requete) {
    if ((requete == null) || (requete.trim().length() == 0)) {
      msgErreur += "\nLa requête n'est pas correcte.";
      return null;
    }
    if (queryManager.getLibrary() == null) {
      msgErreur += "\nLa CURLIB n'est pas initialisée.";
      return null;
    }
    
    // Lecture de la base
    ArrayList<GenericRecord> listrcd = queryManager.select(requete);
    if (listrcd == null) {
      return null;
    }
    // Chargement des classes avec les données de la base
    int i = 0;
    M_ActionCommerciale[] listac = new M_ActionCommerciale[listrcd.size()];
    for (GenericRecord rcd : listrcd) {
      M_ActionCommerciale ac = new M_ActionCommerciale(queryManager);
      ac.initObject(rcd, true);
      // Recupération des contacts liés
      ac.loadContacts(queryManager);
      // Récupération des infos du client/prospect
      ac.loadClient(queryManager);
      // Chargement des objets et de l'établissement
      ac.setListObjectAction(listObjectAction);
      ac.getEvenementContact().setETETB(this.etablissement);
      
      listac[i++] = ac;
    }
    
    return listac;
  }
  
  // -- Accesseurs -----------------------------------------------------------
  
  /**
   * @return le etablissement
   */
  public String getEtablissement() {
    return etablissement;
  }
  
  /**
   * @param etablissement le etablissement à définir
   *
   *          public void setEtablissement(String etablissement)
   *          {
   *          this.etablissement = etablissement;
   *          loadListObjectAction();
   *          }
   */
  
  /**
   * @return le listObjectAction
   */
  public LinkedHashMap<String, String> getListObjectAction() {
    return listObjectAction;
  }
  
  /**
   * @param listObjectAction le listObjectAction à définir
   */
  public void setListObjectAction(LinkedHashMap<String, String> listObjectAction) {
    this.listObjectAction = listObjectAction;
  }
  
}
