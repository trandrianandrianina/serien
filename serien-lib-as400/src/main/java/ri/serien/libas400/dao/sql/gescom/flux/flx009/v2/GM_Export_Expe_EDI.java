/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx009.v2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxInit;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi.ExpeditionsEDI_GP;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.outils.ftp.FTPManager;

/**
 * Permet d'exporter une expedition de commande au format DESADV EDI @GP
 */
public class GM_Export_Expe_EDI extends BaseGroupDB {
  FTPManager serveurFTP = null;
  // QueryManager querymg = null;
  SystemeManager system = null;
  private String lettreEnv = null;
  private ParametresFluxBibli parametresFlux = null;
  
  /**
   * Constructeur principal
   */
  public GM_Export_Expe_EDI(ParametresFluxBibli pParams, QueryManager aquerymg, SystemeManager sys, String lettre) {
    super(aquerymg);
    parametresFlux = pParams;
    system = sys;
    lettreEnv = lettre;
  }
  
  /**
   * Envoyer le détail d'une expédition de commande
   */
  public boolean envoyerDESADV(FluxMagento record) {
    if (record == null) {
      majError("(GM_Export_Expe_EDI] envoyerDESADV() record NULL");
      return false;
    }
    
    boolean retour = false;
    
    // 1 Récupérer la commande et vérifier ses données et son statut
    ExpeditionsEDI_GP expedition = recupererLaCommandeSN(record);
    
    // 2 Construire un fichier de retour
    if (expedition != null && expedition.isValide()) {
      // On construit un fichier sur la base de ces donénes dans la zone d'envoi FTP
      File fichier = construireUnDESADV(expedition);
      
      // 3 Envoyer en FTP
      if (fichier != null && fichier.isFile()) {
        record.setFLCOD(fichier.getName());
        return envoyerLeFichierFTP(fichier);
      }
      else {
        majError("(GM_Export_Expe_EDI] fichier NULL ou invalide ");
      }
    }
    else {
      if (expedition == null) {
        majError("(GM_Export_Expe_EDI] envoyerDESADV expedition NULL ");
      }
      else {
        majError("(GM_Export_Expe_EDI] envoyerDESADV PB expedition invalide: " + expedition.getMsgError());
      }
    }
    
    return retour;
  }
  
  /**
   * Récupérer la commande Série N sur la base du code fourni dans le FLUX
   */
  private ExpeditionsEDI_GP recupererLaCommandeSN(FluxMagento record) {
    if (record == null || record.getFLETB() == null || record.getFLCOD() == null) {
      majError("(GM_Export_Expe_EDI] recupererLaCommandeSN() record NULL ou corrompu");
      return null;
    }
    ExpeditionsEDI_GP retour = null;
    
    String typeBon = null;
    String etb = null;
    String numero = null;
    String suffixe = null;
    // on découpe le code moisi EONE0003050
    if (record.getFLCOD().length() == 11) {
      typeBon = record.getFLCOD().substring(0, 1);
      etb = record.getFLCOD().substring(1, 4);
      numero = record.getFLCOD().substring(4, 10);
      suffixe = record.getFLCOD().substring(10, 11);
      
      if (etb == null || !etb.equals(record.getFLETB())) {
        majError("(GM_Export_Expe_EDI] recupererLaCommandeSN() les établissements sont différents " + record.getFLETB() + "/" + etb);
        return null;
      }
      if ((numero == null || numero.trim().length() != 6)) {
        majError("(GM_Export_Expe_EDI] recupererLaCommandeSN() le numero de commande est corrompu" + numero);
        return null;
      }
      if ((suffixe == null || suffixe.trim().length() != 1)) {
        majError("(GM_Export_Expe_EDI] recupererLaCommandeSN() le suffixe de commande est corrompu" + suffixe);
        return null;
      }
    }
    else {
      majError("(GM_Export_Expe_EDI] recupererLaCommandeSN() FLCOD (commande SN) pas à la bonne taille " + record.getFLCOD());
      return null;
    }
    
    ArrayList<GenericRecord> liste =
        queryManager.select("" + " SELECT E1ETB,E1NUM,E1SUF,E1CCT,E1ETA,E1CRE,E1EXP,E1FAC,E1DLP,E1CLLP,E1CLLS,E1CLFP,E1CLFS FROM "
            + queryManager.getLibrary() + ".PGVMEBCM WHERE E1COD = '" + typeBon + "' AND E1ETB ='" + etb + "' AND E1NUM = '" + numero
            + "' AND E1SUF = '" + suffixe + "' ");
    // E1CLLP E1CLLS E1CLFP E1CLFS
    
    if (liste != null && liste.size() == 1) {
      GenericRecord recordTemp = liste.get(0);
      if (!recordTemp.isPresentField("E1ETB") || !recordTemp.isPresentField("E1NUM") || !recordTemp.isPresentField("E1SUF")) {
        majError("[GM_Export_Expe_EDI] recupererLaCommandeSN() GenericRecord recordTemp E1ETB, E1NUM ou E1SUF NULL ");
        return null;
      }
      
      // On crée l'entête de l'expédition et on laisse l'objet gérer sa validité
      ExpeditionsEDI_GP expedition = new ExpeditionsEDI_GP(parametresFlux, recordTemp, queryManager);
      if (expedition.isValide()) {
        liste = queryManager.select(" SELECT L1NLI,L1ART,DIGITS(A1GCD) AS A1GCD,A1LIB,L1QTE,L1UNV,L1PVN,L1TVA,L1MHT FROM "
            + queryManager.getLibrary() + ".PGVMLBCM LEFT JOIN " + queryManager.getLibrary()
            + ".PGVMARTM ON A1ETB = L1ETB AND A1ART = L1ART WHERE L1ERL IN ('C' , 'S') AND L1ETB = '"
            + liste.get(0).getField("E1ETB").toString().trim() + "' AND L1COD ='E' AND L1NUM='"
            + liste.get(0).getField("E1NUM").toString().trim() + "' AND L1SUF = '" + liste.get(0).getField("E1SUF").toString().trim()
            + "' ");
        if (liste != null && liste.size() > 0) {
          // On met à jour les lignes de cette commande
          expedition.majDesLignes(liste);
          if (expedition.isValide()) {
            retour = expedition;
          }
          else {
            majError("[GM_Export_Expe_EDI] recupererLaCommandeSN() les lignes sont en erreur " + record.getFLCOD() + ": "
                + expedition.getMsgError());
          }
        }
        else {
          majError("[GM_Export_Expe_EDI] recupererLaCommandeSN() les lignes sont nulles ou vides " + record.getFLCOD());
        }
      }
      else {
        majError(
            "[GM_Export_Expe_EDI] recupererLaCommandeSN() expedition invalide " + record.getFLCOD() + ": " + expedition.getMsgError());
      }
    }
    else {
      majError("(GM_Export_Expe_EDI] Pb de récupération de la commande " + queryManager.getMsgError());
    }
    
    return retour;
  }
  
  /**
   * On construit un fichier sur la base de l'expédition
   */
  private File construireUnDESADV(ExpeditionsEDI_GP expedition) {
    File fichier = null;
    if (expedition == null) {
      majError("(GM_Export_Expe_EDI] construireUnDESADV() expedition NULL ou corrompu");
      return fichier;
    }
    
    String nomFichier =
        parametresFlux.getPrefixe_desadv_EDI() + expedition.getBon_livraison_10() + " " + expedition.getLib_clientPayeur_3();
    if (nomFichier.length() > 46) {
      nomFichier = nomFichier.substring(0, 46) + ".txt";
    }
    
    fichier = new File(parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator
        + parametresFlux.getDossier_local_expe_EDI() + nomFichier);
    
    PrintWriter out;
    try {
      out = new PrintWriter(new BufferedWriter(new FileWriter(fichier)));
      
      String retour = retournerContenuExpedition(expedition);
      if (retour == null) {
        majError("(GM_Export_Expe_EDI] construireUnDESADV() retour NUUUUL ");
        out.close();
        return null;
      }
      else {
        out.write(retour);
      }
      out.close(); // Ferme le flux du fichier, sauvegardant ainsi les données.
      
      return fichier;
    }
    catch (IOException e) {
      majError("(GM_Export_Expe_EDI] construireUnDESADV() pb d'écriture de fichier " + e.getMessage());
      return null;
    }
  }
  
  /**
   * Retourner le contenu de l'expédition sous forme de CSV
   */
  private String retournerContenuExpedition(ExpeditionsEDI_GP expedition) {
    if (expedition == null) {
      majError("(GM_Export_Expe_EDI] retournerContenuExpedition() expedition NUUUUL "); // ou invalide ");
      return null;
    }
    StringBuilder retour = new StringBuilder();
    
    // Entête de document
    retour.append(expedition.getId_gp_1() + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getLogiciel_edi_2()
        + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getType_fichier_3() + ParametresFluxInit.EDI_FIN_CHAMP
        + expedition.getFormat_fichier_4() + ParametresFluxInit.EDI_FIN_LIGNE);
    
    // Entête de l'expedition
    retour.append(expedition.getId_entete_1() + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getCommande_client_2()
        + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getDate_commande_3() + ParametresFluxInit.EDI_FIN_CHAMP
        + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP
        + expedition.getDate_livraison_7() + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP
        + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getBon_livraison_10() + ParametresFluxInit.EDI_FIN_CHAMP
        + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP
        + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + parametresFlux.getMode_test_EDI()
        + ParametresFluxInit.EDI_FIN_LIGNE);
    
    // Partenaires de l'expédition
    retour.append(expedition.getDECLA_PAR() + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getEANclientPayeur_2()
        + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getLib_clientPayeur_3() + ParametresFluxInit.EDI_FIN_CHAMP
        + expedition.getEANfournisseur_4() + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getLib_fournisseur_5()
        + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getEANclientlivre_6() + ParametresFluxInit.EDI_FIN_CHAMP
        + expedition.getLib_clientLivre_7() + ParametresFluxInit.EDI_FIN_LIGNE); // 7
    
    // les lignes
    for (int i = 0; i < expedition.getLignes().size(); i++) {
      retour.append(expedition.getLignes().get(i).getId_ligne_1() + ParametresFluxInit.EDI_FIN_CHAMP
          + expedition.getLignes().get(i).getNumero_ligne_2() + ParametresFluxInit.EDI_FIN_CHAMP
          + expedition.getLignes().get(i).getCode_EAN_produit_3() + ParametresFluxInit.EDI_FIN_CHAMP
          + expedition.getLignes().get(i).getCode_fournisseur_produit_4() + ParametresFluxInit.EDI_FIN_CHAMP
          + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getLignes().get(i).getQuantite_ligne_7() + ParametresFluxInit.EDI_FIN_CHAMP
          + expedition.getLignes().get(i).getUniteQte_ligne_8() + ParametresFluxInit.EDI_FIN_CHAMP
          + expedition.getLignes().get(i).getQuantite_ligne_7() + ParametresFluxInit.EDI_FIN_CHAMP
          + expedition.getLignes().get(i).getPrixU_net_ligne_9() + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getDevise_expedition()
          + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + expedition.getCommande_client_2()
          + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_CHAMP
          + expedition.getLignes().get(i).getNumero_ligne_2() + ParametresFluxInit.EDI_FIN_CHAMP + ParametresFluxInit.EDI_FIN_LIGNE);
    }
    
    retour.append("END" + ParametresFluxInit.EDI_FIN_LIGNE);
    retour.append("@ND");
    
    return retour.toString();
  }
  
  /**
   * Envoyer le DESADV sur le serveur FTP
   */
  private boolean envoyerLeFichierFTP(File fichier) {
    if (fichier == null || !fichier.isFile()) {
      majError("[GM_Export_Expe_EDI] envoyerLeFichierFTP() fichier NULL ou invalide ");
      return false;
    }
    
    if (serveurFTP == null) {
      serveurFTP = new FTPManager();
    }
    
    boolean retour = false;
    
    if (serveurFTP.connexion(parametresFlux.getUrl_ftp_EDI(), parametresFlux.getLogin_ftp_EDI(), parametresFlux.getPassword_ftp_EDI())) {
      try {
        if (serveurFTP.getConnexion().changeWorkingDirectory(parametresFlux.getDossierExpe_ftp_EDI())) {
          retour = serveurFTP.uploadFile(fichier.getAbsolutePath(), fichier.getName());
        }
        else {
          majError("[GM_Export_Expe_EDI] envoyerLeFichierFTP()  PB changeWorkingDirectory ");
        }
      }
      catch (Exception e) {
        majError("[GM_Export_Expe_EDI] envoyerLeFichierFTP() connexion invalide " + e.getMessage());
      }
      
      serveurFTP.disconnect();
    }
    else {
      majError("[GM_Export_Expe_EDI] envoyerLeFichierFTP()  " + serveurFTP.getMsgError());
    }
    
    return retour;
  }
  
  @Override
  public void dispose() {
    // TODO Stub de la méthode généré automatiquement
    
  }
}
