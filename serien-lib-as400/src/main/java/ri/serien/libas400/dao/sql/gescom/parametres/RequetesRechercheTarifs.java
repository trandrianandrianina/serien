/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.personnalisation.reglement.CritereModeReglement;
import ri.serien.libcommun.gescom.personnalisation.tarif.CriteresRechercheTarifs;

public class RequetesRechercheTarifs {
  
  /**
   * Retourne la requête en fonction du type de recherche.
   */
  public static String getRequete(String curlib, CriteresRechercheTarifs criteres) {
    String requete = null;
    if (criteres == null) {
      return requete;
    }
    
    switch (criteres.getTypeRecherche()) {
      case CritereModeReglement.TOUT_RETOURNER_POUR_UN_ETABLISSEMENT:
        criteres.setNumeroPage(1);
        criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
        requete = getRequeteRechercheTousTarifs(curlib, criteres);
        break;
      case CritereModeReglement.RECHERCHER_EXPRESSION_DANS_LIBELLE:
        criteres.setNumeroPage(1);
        criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
        requete = getRequeteRechercheDansDesignation(curlib, criteres);
        break;
    }
    
    return ajouterPagination(requete, criteres);
  }
  
  /**
   * Construit la requête qui permet de recherher toutes les tarifs.
   */
  private static String getRequeteRechercheTousTarifs(String curlib, CriteresRechercheTarifs aCriteres) {
    return "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + curlib + '.'
        + EnumTableBDD.PARAMETRE_GESCOM + " where PARTOP <> '9' and PARTYP = 'TA'"
        + " and PARETB = '" + aCriteres.getCodeEtablissement() + "'";
  }
  
  /**
   * Construit la requête qui permet de rechercher une expression dans la désignation tarif.
   */
  private static String getRequeteRechercheDansDesignation(String curlib, CriteresRechercheTarifs criteres) {
    String conditions;
    String critere = criteres.getCriterePourRequete();
    if (criteres.isIgnoreCasse()) {
      conditions = "UPPER(SUBSTR(PARZ2, 1, 30)) like '" + critere + "'";
    }
    else {
      conditions = "SUBSTR(PARZ2, 1, 30) like '" + critere + "'";
    }
    return "select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from "
        + curlib + '.' + EnumTableBDD.PARAMETRE_GESCOM
        + " where PARTOP <> '9' and PARTYP = 'TA' and PARETB = '" + criteres.getCodeEtablissement() + "' and (" + conditions + ")";
  }
  
  /**
   * Ajoute les ordres afin de récupérer qu'une quantité limité de lignes pour la pagination.
   */
  private static String ajouterPagination(String requete, CriteresBaseRecherche criteres) {
    if (criteres.getNbrLignesParPage() != CriteresBaseRecherche.TOUTES_LES_LIGNES) {
      int nbreltpage = criteres.getNbrLignesParPage();
      int indexdeb = (criteres.getNumeroPage() * nbreltpage) - (nbreltpage - 1);
      int indexfin = (indexdeb + nbreltpage) - 1;
      requete = "with vue as (" + requete + ") select * from vue where numrow between " + indexdeb + " and " + indexfin;
    }
    return requete;
  }
}
