/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0028i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PINLIF = 4;
  public static final int DECIMAL_PINLIF = 0;
  public static final int SIZE_PITIT1 = 30;
  public static final int SIZE_PITIT2 = 30;
  public static final int SIZE_PITIT3 = 30;
  public static final int SIZE_PITIT4 = 30;
  public static final int SIZE_PITOT1 = 30;
  public static final int SIZE_PITOT2 = 30;
  public static final int SIZE_PITOT3 = 30;
  public static final int SIZE_PITOT4 = 30;
  public static final int SIZE_PIEDTA = 1;
  public static final int SIZE_PIEDTP = 1;
  public static final int SIZE_PILIDC = 4;
  public static final int DECIMAL_PILIDC = 0;
  public static final int SIZE_PILIFC = 4;
  public static final int DECIMAL_PILIFC = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 280;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINLI = 5;
  public static final int VAR_PINLIF = 6;
  public static final int VAR_PITIT1 = 7;
  public static final int VAR_PITIT2 = 8;
  public static final int VAR_PITIT3 = 9;
  public static final int VAR_PITIT4 = 10;
  public static final int VAR_PITOT1 = 11;
  public static final int VAR_PITOT2 = 12;
  public static final int VAR_PITOT3 = 13;
  public static final int VAR_PITOT4 = 14;
  public static final int VAR_PIEDTA = 15;
  public static final int VAR_PIEDTP = 16;
  public static final int VAR_PILIDC = 17;
  public static final int VAR_PILIFC = 18;
  public static final int VAR_PIARR = 19;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "D","E",X","9"
  private String pietb = ""; // Code établissement
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de ligne origine
  private BigDecimal pinlif = BigDecimal.ZERO; // Numéro de ligne fin (option)
  private String pitit1 = ""; // Titre Texte 1
  private String pitit2 = ""; // Titre Texte 2
  private String pitit3 = ""; // Titre Texte 3
  private String pitit4 = ""; // Titre Texte 4
  private String pitot1 = ""; // Total Texte 1
  private String pitot2 = ""; // Total Texte 2
  private String pitot3 = ""; // Total Texte 3
  private String pitot4 = ""; // Total Texte 4
  private String piedta = ""; // Top édition détail articles
  private String piedtp = ""; // Top édition prix d"articles
  private BigDecimal pilidc = BigDecimal.ZERO; // N°ligne début Commen.Regroup
  private BigDecimal pilifc = BigDecimal.ZERO; // N°ligne fin Commen.Regroup
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9"
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de ligne origine
      new AS400ZonedDecimal(SIZE_PINLIF, DECIMAL_PINLIF), // Numéro de ligne fin (option)
      new AS400Text(SIZE_PITIT1), // Titre Texte 1
      new AS400Text(SIZE_PITIT2), // Titre Texte 2
      new AS400Text(SIZE_PITIT3), // Titre Texte 3
      new AS400Text(SIZE_PITIT4), // Titre Texte 4
      new AS400Text(SIZE_PITOT1), // Total Texte 1
      new AS400Text(SIZE_PITOT2), // Total Texte 2
      new AS400Text(SIZE_PITOT3), // Total Texte 3
      new AS400Text(SIZE_PITOT4), // Total Texte 4
      new AS400Text(SIZE_PIEDTA), // Top édition détail articles
      new AS400Text(SIZE_PIEDTP), // Top édition prix d"articles
      new AS400ZonedDecimal(SIZE_PILIDC, DECIMAL_PILIDC), // N°ligne début Commen.Regroup
      new AS400ZonedDecimal(SIZE_PILIFC, DECIMAL_PILIFC), // N°ligne fin Commen.Regroup
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, picod, pietb, pinum, pisuf, pinli, pinlif, pitit1, pitit2, pitit3, pitit4, pitot1, pitot2, pitot3, pitot4,
          piedta, piedtp, pilidc, pilifc, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinli = (BigDecimal) output[5];
    pinlif = (BigDecimal) output[6];
    pitit1 = (String) output[7];
    pitit2 = (String) output[8];
    pitit3 = (String) output[9];
    pitit4 = (String) output[10];
    pitot1 = (String) output[11];
    pitot2 = (String) output[12];
    pitot3 = (String) output[13];
    pitot4 = (String) output[14];
    piedta = (String) output[15];
    piedtp = (String) output[16];
    pilidc = (BigDecimal) output[17];
    pilifc = (BigDecimal) output[18];
    piarr = (String) output[19];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPinlif(BigDecimal pPinlif) {
    if (pPinlif == null) {
      return;
    }
    pinlif = pPinlif.setScale(DECIMAL_PINLIF, RoundingMode.HALF_UP);
  }
  
  public void setPinlif(Integer pPinlif) {
    if (pPinlif == null) {
      return;
    }
    pinlif = BigDecimal.valueOf(pPinlif);
  }
  
  public Integer getPinlif() {
    return pinlif.intValue();
  }
  
  public void setPitit1(String pPitit1) {
    if (pPitit1 == null) {
      return;
    }
    pitit1 = pPitit1;
  }
  
  public String getPitit1() {
    return pitit1;
  }
  
  public void setPitit2(String pPitit2) {
    if (pPitit2 == null) {
      return;
    }
    pitit2 = pPitit2;
  }
  
  public String getPitit2() {
    return pitit2;
  }
  
  public void setPitit3(String pPitit3) {
    if (pPitit3 == null) {
      return;
    }
    pitit3 = pPitit3;
  }
  
  public String getPitit3() {
    return pitit3;
  }
  
  public void setPitit4(String pPitit4) {
    if (pPitit4 == null) {
      return;
    }
    pitit4 = pPitit4;
  }
  
  public String getPitit4() {
    return pitit4;
  }
  
  public void setPitot1(String pPitot1) {
    if (pPitot1 == null) {
      return;
    }
    pitot1 = pPitot1;
  }
  
  public String getPitot1() {
    return pitot1;
  }
  
  public void setPitot2(String pPitot2) {
    if (pPitot2 == null) {
      return;
    }
    pitot2 = pPitot2;
  }
  
  public String getPitot2() {
    return pitot2;
  }
  
  public void setPitot3(String pPitot3) {
    if (pPitot3 == null) {
      return;
    }
    pitot3 = pPitot3;
  }
  
  public String getPitot3() {
    return pitot3;
  }
  
  public void setPitot4(String pPitot4) {
    if (pPitot4 == null) {
      return;
    }
    pitot4 = pPitot4;
  }
  
  public String getPitot4() {
    return pitot4;
  }
  
  public void setPiedta(Character pPiedta) {
    if (pPiedta == null) {
      return;
    }
    piedta = String.valueOf(pPiedta);
  }
  
  public Character getPiedta() {
    return piedta.charAt(0);
  }
  
  public void setPiedtp(Character pPiedtp) {
    if (pPiedtp == null) {
      return;
    }
    piedtp = String.valueOf(pPiedtp);
  }
  
  public Character getPiedtp() {
    return piedtp.charAt(0);
  }
  
  public void setPilidc(BigDecimal pPilidc) {
    if (pPilidc == null) {
      return;
    }
    pilidc = pPilidc.setScale(DECIMAL_PILIDC, RoundingMode.HALF_UP);
  }
  
  public void setPilidc(Integer pPilidc) {
    if (pPilidc == null) {
      return;
    }
    pilidc = BigDecimal.valueOf(pPilidc);
  }
  
  public Integer getPilidc() {
    return pilidc.intValue();
  }
  
  public void setPilifc(BigDecimal pPilifc) {
    if (pPilifc == null) {
      return;
    }
    pilifc = pPilifc.setScale(DECIMAL_PILIFC, RoundingMode.HALF_UP);
  }
  
  public void setPilifc(Integer pPilifc) {
    if (pPilifc == null) {
      return;
    }
    pilifc = BigDecimal.valueOf(pPilifc);
  }
  
  public Integer getPilifc() {
    return pilifc.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
