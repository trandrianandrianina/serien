/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.controle;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeExtractionDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.prix.ParametrePrixAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.document.ListeZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

public class RpgControlerLigneDocumentAchat extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGAM0004";
  
  /**
   * Appel du programme RPG qui va controler une ligne d'un document.
   */
  public LigneAchatArticle controlerLigneArticleDocument(SystemeManager pSysteme, LigneAchatArticle pLigneArticle, Date pDateTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pLigneArticle == null) {
      throw new MessageErreurException("Le paramètre pLigneArticle du service est à null.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0004i entree = new Svgam0004i();
    Svgam0004o sortie = new Svgam0004o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPiind4(pLigneArticle.getIndicateurs());
    entree.setPicod(pLigneArticle.getId().getCodeEntete().getCode());
    entree.setPietb(pLigneArticle.getId().getCodeEtablissement());
    entree.setPinum(pLigneArticle.getId().getNumero());
    entree.setPisuf(pLigneArticle.getId().getSuffixe());
    entree.setPinli(pLigneArticle.getId().getNumeroLigne());
    entree.setPidat(pDateTraitement);
    entree.setPiopt(LigneVente.OPTION_VALIDATION);
    entree.setPinata(pLigneArticle.getCodeNatureAnalytique());
    
    if (!pLigneArticle.isLigneCommentaire()) {
      PrixAchat prixAchatEntree = pLigneArticle.getPrixAchat();
      
      // Renseigner les zones Li (utilisée pour la mise à jour)
      // Renseigner les unités
      if (prixAchatEntree.getIdUCA() != null) {
        entree.setLiunc(prixAchatEntree.getIdUCA().getCode());
      }
      if (prixAchatEntree.getIdUA() != null) {
        entree.setLiuna(prixAchatEntree.getIdUA().getCode());
      }
      entree.setLidca(prixAchatEntree.getNombreDecimaleUA());
      entree.setLidcc(prixAchatEntree.getNombreDecimaleUCA());
      entree.setLidcs(prixAchatEntree.getNombreDecimaleUS());
      
      // Renseigner les ratios
      entree.setLikac(prixAchatEntree.getNombreUAParUCA());
      entree.setLiksc(prixAchatEntree.getNombreUSParUCA());
      
      // Renseigner les quantités
      entree.setLiqta(prixAchatEntree.getQuantiteReliquatUA());
      entree.setLiqtc(prixAchatEntree.getQuantiteReliquatUCA());
      entree.setLiqts(prixAchatEntree.getQuantiteReliquatUS());
      
      // Renseigner les montants
      entree.setLipab(prixAchatEntree.getPrixAchatBrutHT());
      ListeRemise listeRemise = prixAchatEntree.getListeRemise();
      entree.setLirem1(listeRemise.getPourcentageRemise1());
      entree.setLirem2(listeRemise.getPourcentageRemise2());
      entree.setLirem3(listeRemise.getPourcentageRemise3());
      entree.setLirem4(listeRemise.getPourcentageRemise4());
      entree.setLirem5(listeRemise.getPourcentageRemise5());
      entree.setLirem6(listeRemise.getPourcentageRemise6());
      entree.setLitrl(listeRemise.getTypeRemise());
      entree.setLibrl(listeRemise.getBaseRemise());
      entree.setPilfk3(prixAchatEntree.getPourcentageTaxeOuMajoration());
      entree.setPilfv3(prixAchatEntree.getMontantConditionnement());
      entree.setLipan(prixAchatEntree.getPrixAchatNetHT());
      entree.setLipac(prixAchatEntree.getPrixAchatCalculeHT());
      entree.setLiprnet(prixAchatEntree.getPrixAchatNetHT()); // Prix net HT à sauver également dans ce champ
      entree.setPilfv1(prixAchatEntree.getMontantAuPoidsPortHT());
      entree.setPilfk1(prixAchatEntree.getPoidsPort());
      entree.setPilfp1(prixAchatEntree.getPourcentagePort().getTauxAuFormatOriginal());
      entree.setLiportf(prixAchatEntree.getMontantPortHT());
      entree.setLiprvfr(prixAchatEntree.getPrixRevientFournisseurHT());
      entree.setLimht(prixAchatEntree.getMontantReliquatHT());
      
      // Renseigner les exclusions de remises de pied
      entree.setLirp1(prixAchatEntree.getExclusionRemisePied(PrixAchat.REMISE_1));
      entree.setLirp2(prixAchatEntree.getExclusionRemisePied(PrixAchat.REMISE_2));
      entree.setLirp3(prixAchatEntree.getExclusionRemisePied(PrixAchat.REMISE_3));
      entree.setLirp4(prixAchatEntree.getExclusionRemisePied(PrixAchat.REMISE_4));
      entree.setLirp5(prixAchatEntree.getExclusionRemisePied(PrixAchat.REMISE_5));
      entree.setLirp6(prixAchatEntree.getExclusionRemisePied(PrixAchat.REMISE_6));
      
      // Renseigner les indicateurs
      entree.setLitb(prixAchatEntree.getTopPrixBaseSaisie());
      entree.setLitr(prixAchatEntree.getTopRemiseSaisie());
      entree.setLitn(prixAchatEntree.getTopPrixNetSaisi());
      entree.setLitu(prixAchatEntree.getTopUniteSaisie());
      entree.setLitq(prixAchatEntree.getTopQuantiteSaisie());
      entree.setLith(prixAchatEntree.getTopMontantHTSaisi());
      
      // Renseigner les éléments de TVA
      entree.setLitva(prixAchatEntree.getCodeTVA());
      entree.setLicol(prixAchatEntree.getColonneTVA());
    }
    
    // Description PGVMLBCM
    entree.setLitop(pLigneArticle.getCodeEtat());
    entree.setLicod(pLigneArticle.getId().getCodeEntete().getCode());
    entree.setLietb(pLigneArticle.getId().getCodeEtablissement());
    entree.setLinum(pLigneArticle.getId().getNumero());
    entree.setLisuf(pLigneArticle.getId().getSuffixe());
    entree.setLinli(pLigneArticle.getId().getNumeroLigne());
    entree.setLierl(pLigneArticle.getTypeLigne().getCode());
    entree.setLicex(pLigneArticle.getCodeExtraction().getCode());
    entree.setLiqcx(pLigneArticle.getQuantiteExtraiteUV());
    entree.setLitva(pLigneArticle.getCodeTVA());
    
    entree.setLival(pLigneArticle.getTopLigneEnValeur());
    entree.setLicol(pLigneArticle.getNumeroColonneTVA());
    entree.setLisgn(pLigneArticle.getSigneLigne());
    entree.setLiavr(pLigneArticle.getCodeAvoir());
    
    entree.setLiart(pLigneArticle.getIdArticle().getCodeArticle());
    if (pLigneArticle.getIdMagasin() != null) {
      entree.setLimag(pLigneArticle.getIdMagasin().getCode());
    }
    if (pLigneArticle.getIdMagasinAvantModif() != null) {
      entree.setLimaga(pLigneArticle.getIdMagasinAvantModif().getCode());
    }
    IdLigneAchat idLigneRapprochee = pLigneArticle.getIdLigneRapprochee();
    if (idLigneRapprochee != null) {
      entree.setLinumr(idLigneRapprochee.getNumero());
      entree.setLisufr(idLigneRapprochee.getSuffixe());
      entree.setLinlir(idLigneRapprochee.getNumeroLigne());
    }
    entree.setLidath(pLigneArticle.getDateHistoriqueStock());
    entree.setLiordh(pLigneArticle.getOrdreHistoriqueStock());
    entree.setLisan(pLigneArticle.getCodeSectionAnalytique());
    entree.setLiact(pLigneArticle.getCodeAffaire());
    entree.setLinat(pLigneArticle.getCodeNatureAchat());
    entree.setLimta(pLigneArticle.getMontantAffecte());
    entree.setLidlp(pLigneArticle.getDateLivraisonPrevue());
    entree.setLiser(pLigneArticle.getEtatSaisieNumeroSerieOuLot());
    if (pLigneArticle.getListeTopPersonnalisable() != null) {
      ListeZonePersonnalisee liste = pLigneArticle.getListeTopPersonnalisable();
      entree.setLitp1(liste.getTopPersonnalisation1());
      entree.setLitp2(liste.getTopPersonnalisation2());
      entree.setLitp3(liste.getTopPersonnalisation3());
      entree.setLitp4(liste.getTopPersonnalisation4());
      entree.setLitp5(liste.getTopPersonnalisation5());
    }
    entree.setLiin1(pLigneArticle.getImputationFrais());
    entree.setLiin2(pLigneArticle.getTypeFrais());
    entree.setLiin3(pLigneArticle.getRegroupementArticle());
    entree.setLiin4(pLigneArticle.getComptabilisationStockFlottant());
    entree.setLiin5(pLigneArticle.getConditionEnNombreGratuit());
    entree.setLidlc(pLigneArticle.getDateLivraisonCalculee());
    
    // Informations de la ligne de vente si celle-ci est renseignée
    IdLigneVente idLigneVente = pLigneArticle.getIdLigneVente();
    if (idLigneVente != null && idLigneVente.isExistant()) {
      entree.setPitos('V');
      entree.setPicos('E');
      entree.setPinos(idLigneVente.getNumero());
      entree.setPisos(idLigneVente.getSuffixe());
      entree.setPilos(idLigneVente.getNumeroLigne());
    }
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Initialisation de la classe métier
      IdLigneAchat idLigneDocument = IdLigneAchat.getInstance(pLigneArticle.getId().getIdDocumentAchat(), sortie.getLonli());
      pLigneArticle.setId(idLigneDocument);
      if (sortie.getPowser() == 'L') {
        pLigneArticle.setOptionSaisieLot(true);
      }
      else {
        pLigneArticle.setOptionSaisieLot(false);
      }
      if (sortie.getPowser() == 'S') {
        pLigneArticle.setOptionSaisieNumeroSerie(true);
      }
      else {
        pLigneArticle.setOptionSaisieNumeroSerie(false);
      }
      pLigneArticle.setCodeEtat(sortie.getLotop());
      pLigneArticle.setTypeLigne(EnumTypeLigneAchat.valueOfByCode(sortie.getLoerl()));
      pLigneArticle.setCodeExtraction(EnumCodeExtractionDocumentAchat.valueOfByCode(sortie.getLocex()));
      pLigneArticle.setQuantiteExtraiteUV(sortie.getLoqcx());
      pLigneArticle.setCodeTVA(sortie.getLotva());
      pLigneArticle.setNumeroColonneTVA(sortie.getLocol());
      pLigneArticle.setSigneLigne(sortie.getLosgn());
      pLigneArticle.setCodeAvoir(sortie.getLoavr());
      IdArticle idArticle = IdArticle.getInstance(pLigneArticle.getId().getIdEtablissement(), sortie.getLoart());
      pLigneArticle.setIdArticle(idArticle);
      if (!sortie.getLomag().trim().isEmpty()) {
        pLigneArticle.setIdMagasin(IdMagasin.getInstance(pLigneArticle.getId().getIdEtablissement(), sortie.getLomag()));
      }
      if (!sortie.getLomaga().trim().isEmpty()) {
        pLigneArticle.setIdMagasinAvantModif(IdMagasin.getInstance(pLigneArticle.getId().getIdEtablissement(), sortie.getLomaga()));
      }
      // Le rapprochement est toujours lié à son code Entete opposé : Facture avec un bon et un bon avec une facture
      EnumCodeEnteteDocumentAchat codeEntete;
      if (pLigneArticle.getId().getCodeEntete() == EnumCodeEnteteDocumentAchat.FACTURE) {
        codeEntete = EnumCodeEnteteDocumentAchat.FACTURE;
      }
      else {
        codeEntete = EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION;
      }
      if (sortie.getLonumr() > 0) {
        idLigneRapprochee = IdLigneAchat.getInstance(pLigneArticle.getId().getIdEtablissement(), codeEntete, sortie.getLonumr(),
            sortie.getLosufr(), sortie.getLonlir());
        pLigneArticle.setIdLigneRapprochee(idLigneRapprochee);
      }
      pLigneArticle.setDateHistoriqueStock(sortie.getLodathConvertiEnDate());
      pLigneArticle.setOrdreHistoriqueStock(sortie.getLoordh());
      pLigneArticle.setCodeSectionAnalytique(sortie.getLosan());
      pLigneArticle.setCodeAffaire(sortie.getLoact());
      pLigneArticle.setCodeNatureAchat(sortie.getLonat());
      pLigneArticle.setMontantAffecte(sortie.getLomta());
      pLigneArticle.setDateLivraisonPrevue(sortie.getLodlpConvertiEnDate());
      pLigneArticle.setEtatSaisieNumeroSerieOuLot(sortie.getLoser());
      if (pLigneArticle.getListeTopPersonnalisable() == null) {
        pLigneArticle.setListeTopPersonnalisable(new ListeZonePersonnalisee());
      }
      ListeZonePersonnalisee liste = pLigneArticle.getListeTopPersonnalisable();
      liste.setTopPersonnalisation1(sortie.getLotp1());
      liste.setTopPersonnalisation2(sortie.getLotp2());
      liste.setTopPersonnalisation3(sortie.getLotp3());
      liste.setTopPersonnalisation4(sortie.getLotp4());
      liste.setTopPersonnalisation5(sortie.getLotp5());
      pLigneArticle.setImputationFrais(sortie.getLoin1());
      pLigneArticle.setTypeFrais(sortie.getLoin2());
      pLigneArticle.setRegroupementArticle(sortie.getLoin3());
      pLigneArticle.setComptabilisationStockFlottant(sortie.getLoin4());
      pLigneArticle.setDateLivraisonCalculee(sortie.getLodlcConvertiEnDate());
      
      // Alimentation de la classe ParamètrePrixAchat avec les données retournées
      if (!pLigneArticle.isLigneCommentaire()) {
        ParametrePrixAchat parametre = new ParametrePrixAchat();
        
        // Renseigner les unités
        if (!sortie.getLouna().trim().isEmpty()) {
          IdUnite idUA = IdUnite.getInstance(sortie.getLouna());
          parametre.setIdUA(idUA);
        }
        if (!sortie.getLounc().trim().isEmpty()) {
          IdUnite idUCA = IdUnite.getInstance(sortie.getLounc());
          parametre.setIdUCA(idUCA);
        }
        parametre.setNombreDecimaleUA(sortie.getLodca());
        parametre.setNombreDecimaleUCA(sortie.getLodcc());
        parametre.setNombreDecimaleUS(sortie.getLodcs());
        
        // Renseigner les ratios
        parametre.setNombreUAParUCA(sortie.getLokac());
        parametre.setNombreUSParUCA(sortie.getLoksc());
        
        // Renseigner les quantités
        parametre.setQuantiteReliquatUA(sortie.getLoqta());
        parametre.setQuantiteReliquatUCA(sortie.getLoqtc());
        parametre.setQuantiteReliquatUS(sortie.getLoqts());
        
        // Renseigner les montants
        parametre.setPrixAchatBrutHT(sortie.getLopab());
        ListeRemise listeRemise = parametre.getListeRemise();
        listeRemise.setPourcentageRemise1(sortie.getLorem1());
        listeRemise.setPourcentageRemise2(sortie.getLorem2());
        listeRemise.setPourcentageRemise3(sortie.getLorem3());
        listeRemise.setPourcentageRemise4(sortie.getLorem4());
        listeRemise.setPourcentageRemise5(sortie.getLorem5());
        listeRemise.setPourcentageRemise6(sortie.getLorem6());
        listeRemise.setTypeRemise(sortie.getLotrl());
        listeRemise.setBaseRemise(sortie.getLobrl());
        parametre.setPourcentageTaxeOuMajoration(sortie.getPolfk3());
        parametre.setMontantConditionnement(sortie.getPolfv3());
        parametre.setPrixNetHT(sortie.getLopan());
        parametre.setPrixCalculeHT(sortie.getLopac());
        parametre.setMontantAuPoidsPortHT(sortie.getPolfv1());
        parametre.setPoidsPort(sortie.getPolfk1());
        parametre.setPourcentagePort(new BigPercentage(sortie.getPolfp1(), EnumFormatPourcentage.POURCENTAGE));
        parametre.setMontantPortHT(sortie.getLoportf());
        parametre.setPrixRevientFournisseurHT(sortie.getLoprvfr());
        parametre.setMontantReliquatHT(sortie.getLomht());
        
        // Renseigner les exclusions de remises de pied
        Character[] exlusionremise = parametre.getExclusionRemisePied();
        exlusionremise[0] = sortie.getLorp1();
        exlusionremise[1] = sortie.getLorp2();
        exlusionremise[2] = sortie.getLorp3();
        exlusionremise[3] = sortie.getLorp4();
        exlusionremise[4] = sortie.getLorp5();
        exlusionremise[5] = sortie.getLorp6();
        
        // Renseigner les indicateurs
        parametre.setTopPrixBaseSaisie(sortie.getLotb());
        parametre.setTopRemiseSaisie(sortie.getLotr());
        parametre.setTopPrixNetSaisi(sortie.getLotn());
        parametre.setTopUniteSaisie(sortie.getLotu());
        parametre.setTopQuantiteSaisie(sortie.getLotq());
        parametre.setTopMontantHTSaisi(sortie.getLoth());
        
        // Renseigner les éléments de TVA
        parametre.setCodeTVA(sortie.getLotva());
        parametre.setColonneTVA(sortie.getLocol());
        
        // Remettre l'information concernant la ligne de vente rattachée s'il y en a une
        pLigneArticle.setIdLigneVente(idLigneVente);
        
        // Mettre à jour l'article
        if (pLigneArticle.getPrixAchat() == null) {
          pLigneArticle.setPrixAchat(new PrixAchat());
        }
        pLigneArticle.getPrixAchat().initialiser(parametre);
      }
    }
    return pLigneArticle;
  }
}
