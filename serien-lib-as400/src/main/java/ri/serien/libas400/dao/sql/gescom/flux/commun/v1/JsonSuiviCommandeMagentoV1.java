/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v1;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.database.record.GenericRecord;

/**
 * Suivi de commande Série N au format Magento
 * Cette classe sert à l'intégration JAVA vers JSON
 * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonSuiviCommandeMagentoV1 extends JsonEntiteMagento {
  private String idCommande = null;
  private String statut = null;
  private ArrayList<String> numeroColis = null;
  private String versionDuFlux = "1.1";
  
  /**
   * Constructeur du suivi de commande Magento
   */
  public JsonSuiviCommandeMagentoV1(FluxMagento record) {
    idFlux = record.getFLIDF();
    
    // TODO paramétrer les versions
    versionFlux = versionDuFlux;
    
    etb = record.getFLETB();
    numeroColis = new ArrayList<String>();
  }
  
  /**
   * Permet de construire le suivi commande sur le modèle Magento dans l'optique d'un
   * export typé
   */
  public boolean mettreAJoursuiviCommande(ArrayList<GenericRecord> records) {
    if (records == null || records.size() < 1) {
      majError("[SuiviCommandeMagento] mettreAJoursuiviCommande()records à NULL ou corrompu");
      return false;
    }
    
    if (records.get(0).isPresentField("E1CCT") && records.get(0).getField("E1CCT").toString().trim().length() > 1) {
      idCommande = records.get(0).getField("E1CCT").toString().trim();
    }
    else {
      majError("[SuiviCommandeMagento] mettreAJoursuiviCommande() E1CCT à NULL ou corrompu ");
      return false;
    }
    if (records.get(0).isPresentField("E1ETA") && records.get(0).isPresentField("E1COD")) {
      if (records.get(0).getField("E1COD").toString().trim().equals("9")) {
        statut = "9";
      }
      else {
        statut = records.get(0).getField("E1ETA").toString().trim();
      }
    }
    else {
      statut = "0";
      majError("[SuiviCommandeMagento] mettreAJoursuiviCommande() E1ETA à NULL ");
      return false;
    }
    
    for (int i = 0; i < records.size(); i++) {
      if (records.get(i).isPresentField("COTRK")) {
        numeroColis.add(records.get(i).getField("COTRK").toString().trim());
      }
    }
    
    return true;
  }
  
  public String getCode() {
    return idCommande;
  }
  
  public void setCode(String code) {
    this.idCommande = code;
  }
  
  public String getStatut() {
    return statut;
  }
  
  public void setStatut(String statut) {
    this.statut = statut;
  }
  
}
