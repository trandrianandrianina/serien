/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0004i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PIDAT = 7;
  public static final int DECIMAL_PIDAT = 0;
  public static final int SIZE_PIOPT = 3;
  public static final int SIZE_PITTC0 = 13;
  public static final int DECIMAL_PITTC0 = 2;
  public static final int SIZE_L0TAR = 2;
  public static final int DECIMAL_L0TAR = 0;
  public static final int SIZE_L0PVB = 9;
  public static final int DECIMAL_L0PVB = 2;
  public static final int SIZE_L0PVN = 9;
  public static final int DECIMAL_L0PVN = 2;
  public static final int SIZE_L0PVC = 9;
  public static final int DECIMAL_L0PVC = 2;
  public static final int SIZE_L0REM1 = 4;
  public static final int DECIMAL_L0REM1 = 2;
  public static final int SIZE_L0REM2 = 4;
  public static final int DECIMAL_L0REM2 = 2;
  public static final int SIZE_L0REM3 = 4;
  public static final int DECIMAL_L0REM3 = 2;
  public static final int SIZE_L0REM4 = 4;
  public static final int DECIMAL_L0REM4 = 2;
  public static final int SIZE_L0REM5 = 4;
  public static final int DECIMAL_L0REM5 = 2;
  public static final int SIZE_L0REM6 = 4;
  public static final int DECIMAL_L0REM6 = 2;
  public static final int SIZE_L0COE = 5;
  public static final int DECIMAL_L0COE = 4;
  public static final int SIZE_L0CND = 9;
  public static final int DECIMAL_L0CND = 3;
  public static final int SIZE_L0QTE = 11;
  public static final int DECIMAL_L0QTE = 3;
  public static final int SIZE_L0QTC = 11;
  public static final int DECIMAL_L0QTC = 3;
  public static final int SIZE_PIL1ACOL = 1;
  public static final int DECIMAL_PIL1ACOL = 0;
  public static final int SIZE_PIL1AFRS = 6;
  public static final int DECIMAL_PIL1AFRS = 0;
  public static final int SIZE_PIL1AMAG = 2;
  public static final int SIZE_PIL1AQTS = 9;
  public static final int DECIMAL_PIL1AQTS = 2;
  public static final int SIZE_PIL1ADLP = 7;
  public static final int DECIMAL_PIL1ADLP = 0;
  public static final int SIZE_PIL1AETB = 3;
  public static final int SIZE_PIL2QT1 = 8;
  public static final int DECIMAL_PIL2QT1 = 3;
  public static final int SIZE_PIL2QT2 = 8;
  public static final int DECIMAL_PIL2QT2 = 3;
  public static final int SIZE_PIL2QT3 = 8;
  public static final int DECIMAL_PIL2QT3 = 3;
  public static final int SIZE_PIL2NBR = 6;
  public static final int DECIMAL_PIL2NBR = 0;
  public static final int SIZE_PIL22DLS = 7;
  public static final int DECIMAL_PIL22DLS = 0;
  public static final int SIZE_LITOP = 1;
  public static final int DECIMAL_LITOP = 0;
  public static final int SIZE_LICOD = 1;
  public static final int SIZE_LIETB = 3;
  public static final int SIZE_LINUM = 6;
  public static final int DECIMAL_LINUM = 0;
  public static final int SIZE_LISUF = 1;
  public static final int DECIMAL_LISUF = 0;
  public static final int SIZE_LINLI = 4;
  public static final int DECIMAL_LINLI = 0;
  public static final int SIZE_LIERL = 1;
  public static final int SIZE_LICEX = 1;
  public static final int SIZE_LIQEX = 11;
  public static final int DECIMAL_LIQEX = 3;
  public static final int SIZE_LITVA = 1;
  public static final int DECIMAL_LITVA = 0;
  public static final int SIZE_LITB = 1;
  public static final int DECIMAL_LITB = 0;
  public static final int SIZE_LITR = 1;
  public static final int DECIMAL_LITR = 0;
  public static final int SIZE_LITN = 1;
  public static final int DECIMAL_LITN = 0;
  public static final int SIZE_LITC = 1;
  public static final int DECIMAL_LITC = 0;
  public static final int SIZE_LITT = 1;
  public static final int DECIMAL_LITT = 0;
  public static final int SIZE_LIVAL = 1;
  public static final int DECIMAL_LIVAL = 0;
  public static final int SIZE_LITAR = 2;
  public static final int DECIMAL_LITAR = 0;
  public static final int SIZE_LICOL = 1;
  public static final int DECIMAL_LICOL = 0;
  public static final int SIZE_LITPF = 1;
  public static final int DECIMAL_LITPF = 0;
  public static final int SIZE_LIDCV = 1;
  public static final int DECIMAL_LIDCV = 0;
  public static final int SIZE_LITNC = 1;
  public static final int DECIMAL_LITNC = 0;
  public static final int SIZE_LISGN = 1;
  public static final int DECIMAL_LISGN = 0;
  public static final int SIZE_LISER = 2;
  public static final int DECIMAL_LISER = 0;
  public static final int SIZE_LIQTE = 11;
  public static final int DECIMAL_LIQTE = 3;
  public static final int SIZE_LIKSV = 7;
  public static final int DECIMAL_LIKSV = 3;
  public static final int SIZE_LIPVB = 9;
  public static final int DECIMAL_LIPVB = 2;
  public static final int SIZE_LIREM1 = 4;
  public static final int DECIMAL_LIREM1 = 2;
  public static final int SIZE_LIREM2 = 4;
  public static final int DECIMAL_LIREM2 = 2;
  public static final int SIZE_LIREM3 = 4;
  public static final int DECIMAL_LIREM3 = 2;
  public static final int SIZE_LIREM4 = 4;
  public static final int DECIMAL_LIREM4 = 2;
  public static final int SIZE_LIREM5 = 4;
  public static final int DECIMAL_LIREM5 = 2;
  public static final int SIZE_LIREM6 = 4;
  public static final int DECIMAL_LIREM6 = 2;
  public static final int SIZE_LITRL = 1;
  public static final int SIZE_LIBRL = 1;
  public static final int SIZE_LIRP1 = 1;
  public static final int SIZE_LIRP2 = 1;
  public static final int SIZE_LIRP3 = 1;
  public static final int SIZE_LIRP4 = 1;
  public static final int SIZE_LIRP5 = 1;
  public static final int SIZE_LIRP6 = 1;
  public static final int SIZE_LIPVN = 9;
  public static final int DECIMAL_LIPVN = 2;
  public static final int SIZE_LIPVC = 9;
  public static final int DECIMAL_LIPVC = 2;
  public static final int SIZE_LIMHT = 9;
  public static final int DECIMAL_LIMHT = 2;
  public static final int SIZE_LIPRP = 9;
  public static final int DECIMAL_LIPRP = 2;
  public static final int SIZE_LICOE = 5;
  public static final int DECIMAL_LICOE = 4;
  public static final int SIZE_LIPRV = 9;
  public static final int DECIMAL_LIPRV = 2;
  public static final int SIZE_LIAVR = 1;
  public static final int SIZE_LIUNV = 2;
  public static final int SIZE_LIART = 20;
  public static final int SIZE_LICPL = 8;
  public static final int SIZE_LICND = 9;
  public static final int DECIMAL_LICND = 3;
  public static final int SIZE_LIIN1 = 1;
  public static final int SIZE_LIPRT = 7;
  public static final int DECIMAL_LIPRT = 2;
  public static final int SIZE_LIDLP = 7;
  public static final int DECIMAL_LIDLP = 0;
  public static final int SIZE_LIIN2 = 1;
  public static final int SIZE_LIIN3 = 1;
  public static final int SIZE_LITP1 = 2;
  public static final int SIZE_LITP2 = 2;
  public static final int SIZE_LITP3 = 2;
  public static final int SIZE_LITP4 = 2;
  public static final int SIZE_LITP5 = 2;
  public static final int SIZE_LIGBA = 1;
  public static final int DECIMAL_LIGBA = 0;
  public static final int SIZE_LIARTS = 20;
  public static final int SIZE_LIIN4 = 1;
  public static final int SIZE_LIIN5 = 1;
  public static final int SIZE_LIIN6 = 1;
  public static final int SIZE_LIIN7 = 1;
  public static final int SIZE_LIQTL = 11;
  public static final int DECIMAL_LIQTL = 3;
  public static final int SIZE_LIMAG = 2;
  public static final int SIZE_LIREP = 2;
  public static final int SIZE_LIIN8 = 1;
  public static final int SIZE_LIIN9 = 1;
  public static final int SIZE_LIIN10 = 1;
  public static final int SIZE_LIIN11 = 1;
  public static final int SIZE_LIIN12 = 1;
  public static final int SIZE_LINLI0 = 4;
  public static final int DECIMAL_LINLI0 = 0;
  public static final int SIZE_LIPRA = 9;
  public static final int DECIMAL_LIPRA = 2;
  public static final int SIZE_LIPVA = 9;
  public static final int DECIMAL_LIPVA = 2;
  public static final int SIZE_LIQTP = 11;
  public static final int DECIMAL_LIQTP = 3;
  public static final int SIZE_LIMTR = 7;
  public static final int DECIMAL_LIMTR = 2;
  public static final int SIZE_LISER3 = 1;
  public static final int SIZE_LISER5 = 1;
  public static final int SIZE_LIIN17 = 1;
  public static final int SIZE_LIIN18 = 1;
  public static final int SIZE_LIIN19 = 1;
  public static final int SIZE_LIIN20 = 1;
  public static final int SIZE_LIIN21 = 1;
  public static final int SIZE_LIIN22 = 1;
  public static final int SIZE_TOTALE_DS = 516;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINLI = 5;
  public static final int VAR_PIDAT = 6;
  public static final int VAR_PIOPT = 7;
  public static final int VAR_PITTC0 = 8;
  public static final int VAR_L0TAR = 9;
  public static final int VAR_L0PVB = 10;
  public static final int VAR_L0PVN = 11;
  public static final int VAR_L0PVC = 12;
  public static final int VAR_L0REM1 = 13;
  public static final int VAR_L0REM2 = 14;
  public static final int VAR_L0REM3 = 15;
  public static final int VAR_L0REM4 = 16;
  public static final int VAR_L0REM5 = 17;
  public static final int VAR_L0REM6 = 18;
  public static final int VAR_L0COE = 19;
  public static final int VAR_L0CND = 20;
  public static final int VAR_L0QTE = 21;
  public static final int VAR_L0QTC = 22;
  public static final int VAR_PIL1ACOL = 23;
  public static final int VAR_PIL1AFRS = 24;
  public static final int VAR_PIL1AMAG = 25;
  public static final int VAR_PIL1AQTS = 26;
  public static final int VAR_PIL1ADLP = 27;
  public static final int VAR_PIL1AETB = 28;
  public static final int VAR_PIL2QT1 = 29;
  public static final int VAR_PIL2QT2 = 30;
  public static final int VAR_PIL2QT3 = 31;
  public static final int VAR_PIL2NBR = 32;
  public static final int VAR_PIL22DLS = 33;
  public static final int VAR_LITOP = 34;
  public static final int VAR_LICOD = 35;
  public static final int VAR_LIETB = 36;
  public static final int VAR_LINUM = 37;
  public static final int VAR_LISUF = 38;
  public static final int VAR_LINLI = 39;
  public static final int VAR_LIERL = 40;
  public static final int VAR_LICEX = 41;
  public static final int VAR_LIQEX = 42;
  public static final int VAR_LITVA = 43;
  public static final int VAR_LITB = 44;
  public static final int VAR_LITR = 45;
  public static final int VAR_LITN = 46;
  public static final int VAR_LITC = 47;
  public static final int VAR_LITT = 48;
  public static final int VAR_LIVAL = 49;
  public static final int VAR_LITAR = 50;
  public static final int VAR_LICOL = 51;
  public static final int VAR_LITPF = 52;
  public static final int VAR_LIDCV = 53;
  public static final int VAR_LITNC = 54;
  public static final int VAR_LISGN = 55;
  public static final int VAR_LISER = 56;
  public static final int VAR_LIQTE = 57;
  public static final int VAR_LIKSV = 58;
  public static final int VAR_LIPVB = 59;
  public static final int VAR_LIREM1 = 60;
  public static final int VAR_LIREM2 = 61;
  public static final int VAR_LIREM3 = 62;
  public static final int VAR_LIREM4 = 63;
  public static final int VAR_LIREM5 = 64;
  public static final int VAR_LIREM6 = 65;
  public static final int VAR_LITRL = 66;
  public static final int VAR_LIBRL = 67;
  public static final int VAR_LIRP1 = 68;
  public static final int VAR_LIRP2 = 69;
  public static final int VAR_LIRP3 = 70;
  public static final int VAR_LIRP4 = 71;
  public static final int VAR_LIRP5 = 72;
  public static final int VAR_LIRP6 = 73;
  public static final int VAR_LIPVN = 74;
  public static final int VAR_LIPVC = 75;
  public static final int VAR_LIMHT = 76;
  public static final int VAR_LIPRP = 77;
  public static final int VAR_LICOE = 78;
  public static final int VAR_LIPRV = 79;
  public static final int VAR_LIAVR = 80;
  public static final int VAR_LIUNV = 81;
  public static final int VAR_LIART = 82;
  public static final int VAR_LICPL = 83;
  public static final int VAR_LICND = 84;
  public static final int VAR_LIIN1 = 85;
  public static final int VAR_LIPRT = 86;
  public static final int VAR_LIDLP = 87;
  public static final int VAR_LIIN2 = 88;
  public static final int VAR_LIIN3 = 89;
  public static final int VAR_LITP1 = 90;
  public static final int VAR_LITP2 = 91;
  public static final int VAR_LITP3 = 92;
  public static final int VAR_LITP4 = 93;
  public static final int VAR_LITP5 = 94;
  public static final int VAR_LIGBA = 95;
  public static final int VAR_LIARTS = 96;
  public static final int VAR_LIIN4 = 97;
  public static final int VAR_LIIN5 = 98;
  public static final int VAR_LIIN6 = 99;
  public static final int VAR_LIIN7 = 100;
  public static final int VAR_LIQTL = 101;
  public static final int VAR_LIMAG = 102;
  public static final int VAR_LIREP = 103;
  public static final int VAR_LIIN8 = 104;
  public static final int VAR_LIIN9 = 105;
  public static final int VAR_LIIN10 = 106;
  public static final int VAR_LIIN11 = 107;
  public static final int VAR_LIIN12 = 108;
  public static final int VAR_LINLI0 = 109;
  public static final int VAR_LIPRA = 110;
  public static final int VAR_LIPVA = 111;
  public static final int VAR_LIQTP = 112;
  public static final int VAR_LIMTR = 113;
  public static final int VAR_LISER3 = 114;
  public static final int VAR_LISER5 = 115;
  public static final int VAR_LIIN17 = 116;
  public static final int VAR_LIIN18 = 117;
  public static final int VAR_LIIN19 = 118;
  public static final int VAR_LIIN20 = 119;
  public static final int VAR_LIIN21 = 120;
  public static final int VAR_LIIN22 = 121;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "D","E",X","9" *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de ligne
  private BigDecimal pidat = BigDecimal.ZERO; // Date de traitement
  private String piopt = ""; // Option de traitement *
  private BigDecimal pittc0 = BigDecimal.ZERO; // Montant total bon avant
  private BigDecimal l0tar = BigDecimal.ZERO; // Numéro colonne tarif
  private BigDecimal l0pvb = BigDecimal.ZERO; // Prix de Vente de Base
  private BigDecimal l0pvn = BigDecimal.ZERO; // Prix de Vente Net
  private BigDecimal l0pvc = BigDecimal.ZERO; // Prix de Vente Net
  private BigDecimal l0rem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal l0rem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal l0rem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal l0rem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal l0rem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal l0rem6 = BigDecimal.ZERO; // % Remise 6
  private BigDecimal l0coe = BigDecimal.ZERO; // Coeff. Mult. prix de base
  private BigDecimal l0cnd = BigDecimal.ZERO; // Conditionnement
  private BigDecimal l0qte = BigDecimal.ZERO; // Quantité Commandée
  private BigDecimal l0qtc = BigDecimal.ZERO; // Quantité Commandée / Cnd
  private BigDecimal pil1acol = BigDecimal.ZERO; // Collectif fournisseur
  private BigDecimal pil1afrs = BigDecimal.ZERO; // Numéro fournisseur
  private String pil1amag = ""; // Magasin
  private BigDecimal pil1aqts = BigDecimal.ZERO; // Qté en US
  private BigDecimal pil1adlp = BigDecimal.ZERO; // Date livraison prévue
  private String pil1aetb = ""; // Etablissement
  private BigDecimal pil2qt1 = BigDecimal.ZERO; // Quantité 1
  private BigDecimal pil2qt2 = BigDecimal.ZERO; // Quantité 2
  private BigDecimal pil2qt3 = BigDecimal.ZERO; // Quantité 3
  private BigDecimal pil2nbr = BigDecimal.ZERO; // Nombre
  private BigDecimal pil22dls = BigDecimal.ZERO; // Date de livraison prévue
  private BigDecimal litop = BigDecimal.ZERO; // Code Etat de la Ligne
  private String licod = ""; // Code ERL "E"
  private String lietb = ""; // Code Etablissement
  private BigDecimal linum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal lisuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal linli = BigDecimal.ZERO; // Numéro de Ligne
  private String lierl = ""; // Code ERL "C"
  private String licex = ""; // Code Extraction
  private BigDecimal liqex = BigDecimal.ZERO; // Quantité extraite en UV
  private BigDecimal litva = BigDecimal.ZERO; // Code TVA
  private BigDecimal litb = BigDecimal.ZERO; // Top prix de base saisi
  private BigDecimal litr = BigDecimal.ZERO; // Top remises saisies
  private BigDecimal litn = BigDecimal.ZERO; // Top prix net saisi
  private BigDecimal litc = BigDecimal.ZERO; // Top coefficient saisi
  private BigDecimal litt = BigDecimal.ZERO; // Top colonne tarif saisie
  private BigDecimal lival = BigDecimal.ZERO; // Top Ligne en Valeur
  private BigDecimal litar = BigDecimal.ZERO; // Numéro colonne tarif
  private BigDecimal licol = BigDecimal.ZERO; // Colonne TVA/E1TVAG
  private BigDecimal litpf = BigDecimal.ZERO; // Code TPF
  private BigDecimal lidcv = BigDecimal.ZERO; // Top décimalisation
  private BigDecimal litnc = BigDecimal.ZERO; // Top non commissionné
  private BigDecimal lisgn = BigDecimal.ZERO; // Signe de la ligne
  private BigDecimal liser = BigDecimal.ZERO; // Top n° de serie ou lot
  private BigDecimal liqte = BigDecimal.ZERO; // Quantité Commandée
  private BigDecimal liksv = BigDecimal.ZERO; // Nbre d'US pour 1 UV
  private BigDecimal lipvb = BigDecimal.ZERO; // Prix de Vente de Base
  private BigDecimal lirem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal lirem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal lirem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal lirem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal lirem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal lirem6 = BigDecimal.ZERO; // % Remise 6
  private String litrl = ""; // Type remise ligne, 1=cascade
  private String librl = ""; // Base remise ligne, 1=Montant
  private String lirp1 = ""; // Exclusion remise de pied N°1
  private String lirp2 = ""; // Exclusion remise de pied N°2
  private String lirp3 = ""; // Exclusion remise de pied N°3
  private String lirp4 = ""; // Exclusion remise de pied N°4
  private String lirp5 = ""; // Exclusion remise de pied N°5
  private String lirp6 = ""; // Exclusion remise de pied N°6
  private BigDecimal lipvn = BigDecimal.ZERO; // Prix de Vente Net
  private BigDecimal lipvc = BigDecimal.ZERO; // Prix de Vente Calculé
  private BigDecimal limht = BigDecimal.ZERO; // Montant Hors Taxes
  private BigDecimal liprp = BigDecimal.ZERO; // Prix de Promo. (Utilisé)
  private BigDecimal licoe = BigDecimal.ZERO; // Coeff. Mult. prix de base
  private BigDecimal liprv = BigDecimal.ZERO; // Prix de Revient
  private String liavr = ""; // Code Ligne Avoir
  private String liunv = ""; // Code Unité de Vente
  private String liart = ""; // Code Article
  private String licpl = ""; // Complément de Libellé
  private BigDecimal licnd = BigDecimal.ZERO; // Conditionnement
  private String liin1 = ""; // Ind. non soumis à escompte
  private BigDecimal liprt = BigDecimal.ZERO; // Prix de transport
  private BigDecimal lidlp = BigDecimal.ZERO; // Date de livraison prévue
  private String liin2 = ""; // Ind. Type de gratuits
  private String liin3 = ""; // Ind. Kit,Taxe etc...
  private String litp1 = ""; // Top personnal.N°1
  private String litp2 = ""; // Top personnal.N°2
  private String litp3 = ""; // Top personnal.N°3
  private String litp4 = ""; // Top personnal.N°4
  private String litp5 = ""; // Top personnal.N°5
  private BigDecimal ligba = BigDecimal.ZERO; // Génération Bon Achat
  private String liarts = ""; // Code article substitué
  private String liin4 = ""; // Ind. Type substitution
  private String liin5 = ""; // Ind. remise 50/50 sur com/rp
  private String liin6 = ""; // Ind. PRV saisi sur ligne
  private String liin7 = ""; // Ind. recherche prix / kit
  private BigDecimal liqtl = BigDecimal.ZERO; // Quantité Livrable
  private String limag = ""; // Magasin
  private String lirep = ""; // Représentant
  private String liin8 = ""; // Indice de livraison
  private String liin9 = ""; // Ind cumul(pds,vol,col)/l1qte
  private String liin10 = ""; // Ind cumul(pds,vol,col)/l1qtp
  private String liin11 = ""; // Ligne à extraire
  private String liin12 = ""; // Ind. ASDI =A1IN13
  private BigDecimal linli0 = BigDecimal.ZERO; // Numéro de Ligne origine
  private BigDecimal lipra = BigDecimal.ZERO; // Prix à ajouter au PRV
  private BigDecimal lipva = BigDecimal.ZERO; // Prix à ajouter au PVC
  private BigDecimal liqtp = BigDecimal.ZERO; // Quantité en Pièces
  private BigDecimal limtr = BigDecimal.ZERO; // Montant transport
  private String liser3 = ""; // top article loti N.U
  private String liser5 = ""; // top article ads N.U
  private String liin17 = ""; // Type de vente
  private String liin18 = ""; // Prix garanti, affaire, dérog
  private String liin19 = ""; // Applic. condition quantitat.
  private String liin20 = ""; // Cond. Chantier Blanc,A F S G
  private String liin21 = ""; // Ind. regroupement ligne
  private String liin22 = ""; // N.U
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9" *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de ligne
      new AS400ZonedDecimal(SIZE_PIDAT, DECIMAL_PIDAT), // Date de traitement
      new AS400Text(SIZE_PIOPT), // Option de traitement *
      new AS400ZonedDecimal(SIZE_PITTC0, DECIMAL_PITTC0), // Montant total bon avant
      new AS400ZonedDecimal(SIZE_L0TAR, DECIMAL_L0TAR), // Numéro colonne tarif
      new AS400ZonedDecimal(SIZE_L0PVB, DECIMAL_L0PVB), // Prix de Vente de Base
      new AS400ZonedDecimal(SIZE_L0PVN, DECIMAL_L0PVN), // Prix de Vente Net
      new AS400ZonedDecimal(SIZE_L0PVC, DECIMAL_L0PVC), // Prix de Vente Net
      new AS400ZonedDecimal(SIZE_L0REM1, DECIMAL_L0REM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_L0REM2, DECIMAL_L0REM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_L0REM3, DECIMAL_L0REM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_L0REM4, DECIMAL_L0REM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_L0REM5, DECIMAL_L0REM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_L0REM6, DECIMAL_L0REM6), // % Remise 6
      new AS400ZonedDecimal(SIZE_L0COE, DECIMAL_L0COE), // Coeff. Mult. prix de base
      new AS400ZonedDecimal(SIZE_L0CND, DECIMAL_L0CND), // Conditionnement
      new AS400ZonedDecimal(SIZE_L0QTE, DECIMAL_L0QTE), // Quantité Commandée
      new AS400ZonedDecimal(SIZE_L0QTC, DECIMAL_L0QTC), // Quantité Commandée / Cnd
      new AS400ZonedDecimal(SIZE_PIL1ACOL, DECIMAL_PIL1ACOL), // Collectif fournisseur
      new AS400ZonedDecimal(SIZE_PIL1AFRS, DECIMAL_PIL1AFRS), // Numéro fournisseur
      new AS400Text(SIZE_PIL1AMAG), // Magasin
      new AS400ZonedDecimal(SIZE_PIL1AQTS, DECIMAL_PIL1AQTS), // Qté en US
      new AS400ZonedDecimal(SIZE_PIL1ADLP, DECIMAL_PIL1ADLP), // Date livraison prévue
      new AS400Text(SIZE_PIL1AETB), // Etablissement
      new AS400ZonedDecimal(SIZE_PIL2QT1, DECIMAL_PIL2QT1), // Quantité 1
      new AS400ZonedDecimal(SIZE_PIL2QT2, DECIMAL_PIL2QT2), // Quantité 2
      new AS400ZonedDecimal(SIZE_PIL2QT3, DECIMAL_PIL2QT3), // Quantité 3
      new AS400ZonedDecimal(SIZE_PIL2NBR, DECIMAL_PIL2NBR), // Nombre
      new AS400ZonedDecimal(SIZE_PIL22DLS, DECIMAL_PIL22DLS), // Date de livraison prévue
      new AS400ZonedDecimal(SIZE_LITOP, DECIMAL_LITOP), // Code Etat de la Ligne
      new AS400Text(SIZE_LICOD), // Code ERL "E"
      new AS400Text(SIZE_LIETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_LINUM, DECIMAL_LINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_LISUF, DECIMAL_LISUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_LINLI, DECIMAL_LINLI), // Numéro de Ligne
      new AS400Text(SIZE_LIERL), // Code ERL "C"
      new AS400Text(SIZE_LICEX), // Code Extraction
      new AS400PackedDecimal(SIZE_LIQEX, DECIMAL_LIQEX), // Quantité extraite en UV
      new AS400ZonedDecimal(SIZE_LITVA, DECIMAL_LITVA), // Code TVA
      new AS400ZonedDecimal(SIZE_LITB, DECIMAL_LITB), // Top prix de base saisi
      new AS400ZonedDecimal(SIZE_LITR, DECIMAL_LITR), // Top remises saisies
      new AS400ZonedDecimal(SIZE_LITN, DECIMAL_LITN), // Top prix net saisi
      new AS400ZonedDecimal(SIZE_LITC, DECIMAL_LITC), // Top coefficient saisi
      new AS400ZonedDecimal(SIZE_LITT, DECIMAL_LITT), // Top colonne tarif saisie
      new AS400ZonedDecimal(SIZE_LIVAL, DECIMAL_LIVAL), // Top Ligne en Valeur
      new AS400ZonedDecimal(SIZE_LITAR, DECIMAL_LITAR), // Numéro colonne tarif
      new AS400ZonedDecimal(SIZE_LICOL, DECIMAL_LICOL), // Colonne TVA/E1TVAG
      new AS400ZonedDecimal(SIZE_LITPF, DECIMAL_LITPF), // Code TPF
      new AS400ZonedDecimal(SIZE_LIDCV, DECIMAL_LIDCV), // Top décimalisation
      new AS400ZonedDecimal(SIZE_LITNC, DECIMAL_LITNC), // Top non commissionné
      new AS400ZonedDecimal(SIZE_LISGN, DECIMAL_LISGN), // Signe de la ligne
      new AS400ZonedDecimal(SIZE_LISER, DECIMAL_LISER), // Top n° de serie ou lot
      new AS400PackedDecimal(SIZE_LIQTE, DECIMAL_LIQTE), // Quantité Commandée
      new AS400PackedDecimal(SIZE_LIKSV, DECIMAL_LIKSV), // Nbre d'US pour 1 UV
      new AS400PackedDecimal(SIZE_LIPVB, DECIMAL_LIPVB), // Prix de Vente de Base
      new AS400ZonedDecimal(SIZE_LIREM1, DECIMAL_LIREM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_LIREM2, DECIMAL_LIREM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_LIREM3, DECIMAL_LIREM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_LIREM4, DECIMAL_LIREM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_LIREM5, DECIMAL_LIREM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_LIREM6, DECIMAL_LIREM6), // % Remise 6
      new AS400Text(SIZE_LITRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_LIBRL), // Base remise ligne, 1=Montant
      new AS400Text(SIZE_LIRP1), // Exclusion remise de pied N°1
      new AS400Text(SIZE_LIRP2), // Exclusion remise de pied N°2
      new AS400Text(SIZE_LIRP3), // Exclusion remise de pied N°3
      new AS400Text(SIZE_LIRP4), // Exclusion remise de pied N°4
      new AS400Text(SIZE_LIRP5), // Exclusion remise de pied N°5
      new AS400Text(SIZE_LIRP6), // Exclusion remise de pied N°6
      new AS400PackedDecimal(SIZE_LIPVN, DECIMAL_LIPVN), // Prix de Vente Net
      new AS400PackedDecimal(SIZE_LIPVC, DECIMAL_LIPVC), // Prix de Vente Calculé
      new AS400PackedDecimal(SIZE_LIMHT, DECIMAL_LIMHT), // Montant Hors Taxes
      new AS400PackedDecimal(SIZE_LIPRP, DECIMAL_LIPRP), // Prix de Promo. (Utilisé)
      new AS400PackedDecimal(SIZE_LICOE, DECIMAL_LICOE), // Coeff. Mult. prix de base
      new AS400PackedDecimal(SIZE_LIPRV, DECIMAL_LIPRV), // Prix de Revient
      new AS400Text(SIZE_LIAVR), // Code Ligne Avoir
      new AS400Text(SIZE_LIUNV), // Code Unité de Vente
      new AS400Text(SIZE_LIART), // Code Article
      new AS400Text(SIZE_LICPL), // Complément de Libellé
      new AS400PackedDecimal(SIZE_LICND, DECIMAL_LICND), // Conditionnement
      new AS400Text(SIZE_LIIN1), // Ind. non soumis à escompte
      new AS400PackedDecimal(SIZE_LIPRT, DECIMAL_LIPRT), // Prix de transport
      new AS400PackedDecimal(SIZE_LIDLP, DECIMAL_LIDLP), // Date de livraison prévue
      new AS400Text(SIZE_LIIN2), // Ind. Type de gratuits
      new AS400Text(SIZE_LIIN3), // Ind. Kit,Taxe etc...
      new AS400Text(SIZE_LITP1), // Top personnal.N°1
      new AS400Text(SIZE_LITP2), // Top personnal.N°2
      new AS400Text(SIZE_LITP3), // Top personnal.N°3
      new AS400Text(SIZE_LITP4), // Top personnal.N°4
      new AS400Text(SIZE_LITP5), // Top personnal.N°5
      new AS400ZonedDecimal(SIZE_LIGBA, DECIMAL_LIGBA), // Génération Bon Achat
      new AS400Text(SIZE_LIARTS), // Code article substitué
      new AS400Text(SIZE_LIIN4), // Ind. Type substitution
      new AS400Text(SIZE_LIIN5), // Ind. remise 50/50 sur com/rp
      new AS400Text(SIZE_LIIN6), // Ind. PRV saisi sur ligne
      new AS400Text(SIZE_LIIN7), // Ind. recherche prix / kit
      new AS400PackedDecimal(SIZE_LIQTL, DECIMAL_LIQTL), // Quantité Livrable
      new AS400Text(SIZE_LIMAG), // Magasin
      new AS400Text(SIZE_LIREP), // Représentant
      new AS400Text(SIZE_LIIN8), // Indice de livraison
      new AS400Text(SIZE_LIIN9), // Ind cumul(pds,vol,col)/l1qte
      new AS400Text(SIZE_LIIN10), // Ind cumul(pds,vol,col)/l1qtp
      new AS400Text(SIZE_LIIN11), // Ligne à extraire
      new AS400Text(SIZE_LIIN12), // Ind. ASDI =A1IN13
      new AS400ZonedDecimal(SIZE_LINLI0, DECIMAL_LINLI0), // Numéro de Ligne origine
      new AS400PackedDecimal(SIZE_LIPRA, DECIMAL_LIPRA), // Prix à ajouter au PRV
      new AS400PackedDecimal(SIZE_LIPVA, DECIMAL_LIPVA), // Prix à ajouter au PVC
      new AS400PackedDecimal(SIZE_LIQTP, DECIMAL_LIQTP), // Quantité en Pièces
      new AS400PackedDecimal(SIZE_LIMTR, DECIMAL_LIMTR), // Montant transport
      new AS400Text(SIZE_LISER3), // top article loti N.U
      new AS400Text(SIZE_LISER5), // top article ads N.U
      new AS400Text(SIZE_LIIN17), // Type de vente
      new AS400Text(SIZE_LIIN18), // Prix garanti, affaire, dérog
      new AS400Text(SIZE_LIIN19), // Applic. condition quantitat.
      new AS400Text(SIZE_LIIN20), // Cond. Chantier Blanc,A F S G
      new AS400Text(SIZE_LIIN21), // Ind. regroupement ligne
      new AS400Text(SIZE_LIIN22), // N.U
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, picod, pietb, pinum, pisuf, pinli, pidat, piopt, pittc0, l0tar, l0pvb, l0pvn, l0pvc, l0rem1, l0rem2, l0rem3,
          l0rem4, l0rem5, l0rem6, l0coe, l0cnd, l0qte, l0qtc, pil1acol, pil1afrs, pil1amag, pil1aqts, pil1adlp, pil1aetb, pil2qt1,
          pil2qt2, pil2qt3, pil2nbr, pil22dls, litop, licod, lietb, linum, lisuf, linli, lierl, licex, liqex, litva, litb, litr, litn,
          litc, litt, lival, litar, licol, litpf, lidcv, litnc, lisgn, liser, liqte, liksv, lipvb, lirem1, lirem2, lirem3, lirem4, lirem5,
          lirem6, litrl, librl, lirp1, lirp2, lirp3, lirp4, lirp5, lirp6, lipvn, lipvc, limht, liprp, licoe, liprv, liavr, liunv, liart,
          licpl, licnd, liin1, liprt, lidlp, liin2, liin3, litp1, litp2, litp3, litp4, litp5, ligba, liarts, liin4, liin5, liin6, liin7,
          liqtl, limag, lirep, liin8, liin9, liin10, liin11, liin12, linli0, lipra, lipva, liqtp, limtr, liser3, liser5, liin17, liin18,
          liin19, liin20, liin21, liin22, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinli = (BigDecimal) output[5];
    pidat = (BigDecimal) output[6];
    piopt = (String) output[7];
    pittc0 = (BigDecimal) output[8];
    l0tar = (BigDecimal) output[9];
    l0pvb = (BigDecimal) output[10];
    l0pvn = (BigDecimal) output[11];
    l0pvc = (BigDecimal) output[12];
    l0rem1 = (BigDecimal) output[13];
    l0rem2 = (BigDecimal) output[14];
    l0rem3 = (BigDecimal) output[15];
    l0rem4 = (BigDecimal) output[16];
    l0rem5 = (BigDecimal) output[17];
    l0rem6 = (BigDecimal) output[18];
    l0coe = (BigDecimal) output[19];
    l0cnd = (BigDecimal) output[20];
    l0qte = (BigDecimal) output[21];
    l0qtc = (BigDecimal) output[22];
    pil1acol = (BigDecimal) output[23];
    pil1afrs = (BigDecimal) output[24];
    pil1amag = (String) output[25];
    pil1aqts = (BigDecimal) output[26];
    pil1adlp = (BigDecimal) output[27];
    pil1aetb = (String) output[28];
    pil2qt1 = (BigDecimal) output[29];
    pil2qt2 = (BigDecimal) output[30];
    pil2qt3 = (BigDecimal) output[31];
    pil2nbr = (BigDecimal) output[32];
    pil22dls = (BigDecimal) output[33];
    litop = (BigDecimal) output[34];
    licod = (String) output[35];
    lietb = (String) output[36];
    linum = (BigDecimal) output[37];
    lisuf = (BigDecimal) output[38];
    linli = (BigDecimal) output[39];
    lierl = (String) output[40];
    licex = (String) output[41];
    liqex = (BigDecimal) output[42];
    litva = (BigDecimal) output[43];
    litb = (BigDecimal) output[44];
    litr = (BigDecimal) output[45];
    litn = (BigDecimal) output[46];
    litc = (BigDecimal) output[47];
    litt = (BigDecimal) output[48];
    lival = (BigDecimal) output[49];
    litar = (BigDecimal) output[50];
    licol = (BigDecimal) output[51];
    litpf = (BigDecimal) output[52];
    lidcv = (BigDecimal) output[53];
    litnc = (BigDecimal) output[54];
    lisgn = (BigDecimal) output[55];
    liser = (BigDecimal) output[56];
    liqte = (BigDecimal) output[57];
    liksv = (BigDecimal) output[58];
    lipvb = (BigDecimal) output[59];
    lirem1 = (BigDecimal) output[60];
    lirem2 = (BigDecimal) output[61];
    lirem3 = (BigDecimal) output[62];
    lirem4 = (BigDecimal) output[63];
    lirem5 = (BigDecimal) output[64];
    lirem6 = (BigDecimal) output[65];
    litrl = (String) output[66];
    librl = (String) output[67];
    lirp1 = (String) output[68];
    lirp2 = (String) output[69];
    lirp3 = (String) output[70];
    lirp4 = (String) output[71];
    lirp5 = (String) output[72];
    lirp6 = (String) output[73];
    lipvn = (BigDecimal) output[74];
    lipvc = (BigDecimal) output[75];
    limht = (BigDecimal) output[76];
    liprp = (BigDecimal) output[77];
    licoe = (BigDecimal) output[78];
    liprv = (BigDecimal) output[79];
    liavr = (String) output[80];
    liunv = (String) output[81];
    liart = (String) output[82];
    licpl = (String) output[83];
    licnd = (BigDecimal) output[84];
    liin1 = (String) output[85];
    liprt = (BigDecimal) output[86];
    lidlp = (BigDecimal) output[87];
    liin2 = (String) output[88];
    liin3 = (String) output[89];
    litp1 = (String) output[90];
    litp2 = (String) output[91];
    litp3 = (String) output[92];
    litp4 = (String) output[93];
    litp5 = (String) output[94];
    ligba = (BigDecimal) output[95];
    liarts = (String) output[96];
    liin4 = (String) output[97];
    liin5 = (String) output[98];
    liin6 = (String) output[99];
    liin7 = (String) output[100];
    liqtl = (BigDecimal) output[101];
    limag = (String) output[102];
    lirep = (String) output[103];
    liin8 = (String) output[104];
    liin9 = (String) output[105];
    liin10 = (String) output[106];
    liin11 = (String) output[107];
    liin12 = (String) output[108];
    linli0 = (BigDecimal) output[109];
    lipra = (BigDecimal) output[110];
    lipva = (BigDecimal) output[111];
    liqtp = (BigDecimal) output[112];
    limtr = (BigDecimal) output[113];
    liser3 = (String) output[114];
    liser5 = (String) output[115];
    liin17 = (String) output[116];
    liin18 = (String) output[117];
    liin19 = (String) output[118];
    liin20 = (String) output[119];
    liin21 = (String) output[120];
    liin22 = (String) output[121];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPidat(BigDecimal pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = pPidat.setScale(DECIMAL_PIDAT, RoundingMode.HALF_UP);
  }
  
  public void setPidat(Integer pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(pPidat);
  }
  
  public void setPidat(Date pPidat) {
    if (pPidat == null) {
      return;
    }
    pidat = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidat));
  }
  
  public Integer getPidat() {
    return pidat.intValue();
  }
  
  public Date getPidatConvertiEnDate() {
    return ConvertDate.db2ToDate(pidat.intValue(), null);
  }
  
  public void setPiopt(String pPiopt) {
    if (pPiopt == null) {
      return;
    }
    piopt = pPiopt;
  }
  
  public String getPiopt() {
    return piopt;
  }
  
  public void setPittc0(BigDecimal pPittc0) {
    if (pPittc0 == null) {
      return;
    }
    pittc0 = pPittc0.setScale(DECIMAL_PITTC0, RoundingMode.HALF_UP);
  }
  
  public void setPittc0(Double pPittc0) {
    if (pPittc0 == null) {
      return;
    }
    pittc0 = BigDecimal.valueOf(pPittc0).setScale(DECIMAL_PITTC0, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPittc0() {
    return pittc0.setScale(DECIMAL_PITTC0, RoundingMode.HALF_UP);
  }
  
  public void setL0tar(BigDecimal pL0tar) {
    if (pL0tar == null) {
      return;
    }
    l0tar = pL0tar.setScale(DECIMAL_L0TAR, RoundingMode.HALF_UP);
  }
  
  public void setL0tar(Integer pL0tar) {
    if (pL0tar == null) {
      return;
    }
    l0tar = BigDecimal.valueOf(pL0tar);
  }
  
  public Integer getL0tar() {
    return l0tar.intValue();
  }
  
  public void setL0pvb(BigDecimal pL0pvb) {
    if (pL0pvb == null) {
      return;
    }
    l0pvb = pL0pvb.setScale(DECIMAL_L0PVB, RoundingMode.HALF_UP);
  }
  
  public void setL0pvb(Double pL0pvb) {
    if (pL0pvb == null) {
      return;
    }
    l0pvb = BigDecimal.valueOf(pL0pvb).setScale(DECIMAL_L0PVB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0pvb() {
    return l0pvb.setScale(DECIMAL_L0PVB, RoundingMode.HALF_UP);
  }
  
  public void setL0pvn(BigDecimal pL0pvn) {
    if (pL0pvn == null) {
      return;
    }
    l0pvn = pL0pvn.setScale(DECIMAL_L0PVN, RoundingMode.HALF_UP);
  }
  
  public void setL0pvn(Double pL0pvn) {
    if (pL0pvn == null) {
      return;
    }
    l0pvn = BigDecimal.valueOf(pL0pvn).setScale(DECIMAL_L0PVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0pvn() {
    return l0pvn.setScale(DECIMAL_L0PVN, RoundingMode.HALF_UP);
  }
  
  public void setL0pvc(BigDecimal pL0pvc) {
    if (pL0pvc == null) {
      return;
    }
    l0pvc = pL0pvc.setScale(DECIMAL_L0PVC, RoundingMode.HALF_UP);
  }
  
  public void setL0pvc(Double pL0pvc) {
    if (pL0pvc == null) {
      return;
    }
    l0pvc = BigDecimal.valueOf(pL0pvc).setScale(DECIMAL_L0PVC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0pvc() {
    return l0pvc.setScale(DECIMAL_L0PVC, RoundingMode.HALF_UP);
  }
  
  public void setL0rem1(BigDecimal pL0rem1) {
    if (pL0rem1 == null) {
      return;
    }
    l0rem1 = pL0rem1.setScale(DECIMAL_L0REM1, RoundingMode.HALF_UP);
  }
  
  public void setL0rem1(Double pL0rem1) {
    if (pL0rem1 == null) {
      return;
    }
    l0rem1 = BigDecimal.valueOf(pL0rem1).setScale(DECIMAL_L0REM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem1() {
    return l0rem1.setScale(DECIMAL_L0REM1, RoundingMode.HALF_UP);
  }
  
  public void setL0rem2(BigDecimal pL0rem2) {
    if (pL0rem2 == null) {
      return;
    }
    l0rem2 = pL0rem2.setScale(DECIMAL_L0REM2, RoundingMode.HALF_UP);
  }
  
  public void setL0rem2(Double pL0rem2) {
    if (pL0rem2 == null) {
      return;
    }
    l0rem2 = BigDecimal.valueOf(pL0rem2).setScale(DECIMAL_L0REM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem2() {
    return l0rem2.setScale(DECIMAL_L0REM2, RoundingMode.HALF_UP);
  }
  
  public void setL0rem3(BigDecimal pL0rem3) {
    if (pL0rem3 == null) {
      return;
    }
    l0rem3 = pL0rem3.setScale(DECIMAL_L0REM3, RoundingMode.HALF_UP);
  }
  
  public void setL0rem3(Double pL0rem3) {
    if (pL0rem3 == null) {
      return;
    }
    l0rem3 = BigDecimal.valueOf(pL0rem3).setScale(DECIMAL_L0REM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem3() {
    return l0rem3.setScale(DECIMAL_L0REM3, RoundingMode.HALF_UP);
  }
  
  public void setL0rem4(BigDecimal pL0rem4) {
    if (pL0rem4 == null) {
      return;
    }
    l0rem4 = pL0rem4.setScale(DECIMAL_L0REM4, RoundingMode.HALF_UP);
  }
  
  public void setL0rem4(Double pL0rem4) {
    if (pL0rem4 == null) {
      return;
    }
    l0rem4 = BigDecimal.valueOf(pL0rem4).setScale(DECIMAL_L0REM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem4() {
    return l0rem4.setScale(DECIMAL_L0REM4, RoundingMode.HALF_UP);
  }
  
  public void setL0rem5(BigDecimal pL0rem5) {
    if (pL0rem5 == null) {
      return;
    }
    l0rem5 = pL0rem5.setScale(DECIMAL_L0REM5, RoundingMode.HALF_UP);
  }
  
  public void setL0rem5(Double pL0rem5) {
    if (pL0rem5 == null) {
      return;
    }
    l0rem5 = BigDecimal.valueOf(pL0rem5).setScale(DECIMAL_L0REM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem5() {
    return l0rem5.setScale(DECIMAL_L0REM5, RoundingMode.HALF_UP);
  }
  
  public void setL0rem6(BigDecimal pL0rem6) {
    if (pL0rem6 == null) {
      return;
    }
    l0rem6 = pL0rem6.setScale(DECIMAL_L0REM6, RoundingMode.HALF_UP);
  }
  
  public void setL0rem6(Double pL0rem6) {
    if (pL0rem6 == null) {
      return;
    }
    l0rem6 = BigDecimal.valueOf(pL0rem6).setScale(DECIMAL_L0REM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0rem6() {
    return l0rem6.setScale(DECIMAL_L0REM6, RoundingMode.HALF_UP);
  }
  
  public void setL0coe(BigDecimal pL0coe) {
    if (pL0coe == null) {
      return;
    }
    l0coe = pL0coe.setScale(DECIMAL_L0COE, RoundingMode.HALF_UP);
  }
  
  public void setL0coe(Double pL0coe) {
    if (pL0coe == null) {
      return;
    }
    l0coe = BigDecimal.valueOf(pL0coe).setScale(DECIMAL_L0COE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0coe() {
    return l0coe.setScale(DECIMAL_L0COE, RoundingMode.HALF_UP);
  }
  
  public void setL0cnd(BigDecimal pL0cnd) {
    if (pL0cnd == null) {
      return;
    }
    l0cnd = pL0cnd.setScale(DECIMAL_L0CND, RoundingMode.HALF_UP);
  }
  
  public void setL0cnd(Double pL0cnd) {
    if (pL0cnd == null) {
      return;
    }
    l0cnd = BigDecimal.valueOf(pL0cnd).setScale(DECIMAL_L0CND, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0cnd() {
    return l0cnd.setScale(DECIMAL_L0CND, RoundingMode.HALF_UP);
  }
  
  public void setL0qte(BigDecimal pL0qte) {
    if (pL0qte == null) {
      return;
    }
    l0qte = pL0qte.setScale(DECIMAL_L0QTE, RoundingMode.HALF_UP);
  }
  
  public void setL0qte(Double pL0qte) {
    if (pL0qte == null) {
      return;
    }
    l0qte = BigDecimal.valueOf(pL0qte).setScale(DECIMAL_L0QTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0qte() {
    return l0qte.setScale(DECIMAL_L0QTE, RoundingMode.HALF_UP);
  }
  
  public void setL0qtc(BigDecimal pL0qtc) {
    if (pL0qtc == null) {
      return;
    }
    l0qtc = pL0qtc.setScale(DECIMAL_L0QTC, RoundingMode.HALF_UP);
  }
  
  public void setL0qtc(Double pL0qtc) {
    if (pL0qtc == null) {
      return;
    }
    l0qtc = BigDecimal.valueOf(pL0qtc).setScale(DECIMAL_L0QTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL0qtc() {
    return l0qtc.setScale(DECIMAL_L0QTC, RoundingMode.HALF_UP);
  }
  
  public void setPil1acol(BigDecimal pPil1acol) {
    if (pPil1acol == null) {
      return;
    }
    pil1acol = pPil1acol.setScale(DECIMAL_PIL1ACOL, RoundingMode.HALF_UP);
  }
  
  public void setPil1acol(Integer pPil1acol) {
    if (pPil1acol == null) {
      return;
    }
    pil1acol = BigDecimal.valueOf(pPil1acol);
  }
  
  public Integer getPil1acol() {
    return pil1acol.intValue();
  }
  
  public void setPil1afrs(BigDecimal pPil1afrs) {
    if (pPil1afrs == null) {
      return;
    }
    pil1afrs = pPil1afrs.setScale(DECIMAL_PIL1AFRS, RoundingMode.HALF_UP);
  }
  
  public void setPil1afrs(Integer pPil1afrs) {
    if (pPil1afrs == null) {
      return;
    }
    pil1afrs = BigDecimal.valueOf(pPil1afrs);
  }
  
  public Integer getPil1afrs() {
    return pil1afrs.intValue();
  }
  
  public void setPil1amag(String pPil1amag) {
    if (pPil1amag == null) {
      return;
    }
    pil1amag = pPil1amag;
  }
  
  public String getPil1amag() {
    return pil1amag;
  }
  
  public void setPil1aqts(BigDecimal pPil1aqts) {
    if (pPil1aqts == null) {
      return;
    }
    pil1aqts = pPil1aqts.setScale(DECIMAL_PIL1AQTS, RoundingMode.HALF_UP);
  }
  
  public void setPil1aqts(Double pPil1aqts) {
    if (pPil1aqts == null) {
      return;
    }
    pil1aqts = BigDecimal.valueOf(pPil1aqts).setScale(DECIMAL_PIL1AQTS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPil1aqts() {
    return pil1aqts.setScale(DECIMAL_PIL1AQTS, RoundingMode.HALF_UP);
  }
  
  public void setPil1adlp(BigDecimal pPil1adlp) {
    if (pPil1adlp == null) {
      return;
    }
    pil1adlp = pPil1adlp.setScale(DECIMAL_PIL1ADLP, RoundingMode.HALF_UP);
  }
  
  public void setPil1adlp(Integer pPil1adlp) {
    if (pPil1adlp == null) {
      return;
    }
    pil1adlp = BigDecimal.valueOf(pPil1adlp);
  }
  
  public void setPil1adlp(Date pPil1adlp) {
    if (pPil1adlp == null) {
      return;
    }
    pil1adlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pPil1adlp));
  }
  
  public Integer getPil1adlp() {
    return pil1adlp.intValue();
  }
  
  public Date getPil1adlpConvertiEnDate() {
    return ConvertDate.db2ToDate(pil1adlp.intValue(), null);
  }
  
  public void setPil1aetb(String pPil1aetb) {
    if (pPil1aetb == null) {
      return;
    }
    pil1aetb = pPil1aetb;
  }
  
  public String getPil1aetb() {
    return pil1aetb;
  }
  
  public void setPil2qt1(BigDecimal pPil2qt1) {
    if (pPil2qt1 == null) {
      return;
    }
    pil2qt1 = pPil2qt1.setScale(DECIMAL_PIL2QT1, RoundingMode.HALF_UP);
  }
  
  public void setPil2qt1(Double pPil2qt1) {
    if (pPil2qt1 == null) {
      return;
    }
    pil2qt1 = BigDecimal.valueOf(pPil2qt1).setScale(DECIMAL_PIL2QT1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPil2qt1() {
    return pil2qt1.setScale(DECIMAL_PIL2QT1, RoundingMode.HALF_UP);
  }
  
  public void setPil2qt2(BigDecimal pPil2qt2) {
    if (pPil2qt2 == null) {
      return;
    }
    pil2qt2 = pPil2qt2.setScale(DECIMAL_PIL2QT2, RoundingMode.HALF_UP);
  }
  
  public void setPil2qt2(Double pPil2qt2) {
    if (pPil2qt2 == null) {
      return;
    }
    pil2qt2 = BigDecimal.valueOf(pPil2qt2).setScale(DECIMAL_PIL2QT2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPil2qt2() {
    return pil2qt2.setScale(DECIMAL_PIL2QT2, RoundingMode.HALF_UP);
  }
  
  public void setPil2qt3(BigDecimal pPil2qt3) {
    if (pPil2qt3 == null) {
      return;
    }
    pil2qt3 = pPil2qt3.setScale(DECIMAL_PIL2QT3, RoundingMode.HALF_UP);
  }
  
  public void setPil2qt3(Double pPil2qt3) {
    if (pPil2qt3 == null) {
      return;
    }
    pil2qt3 = BigDecimal.valueOf(pPil2qt3).setScale(DECIMAL_PIL2QT3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPil2qt3() {
    return pil2qt3.setScale(DECIMAL_PIL2QT3, RoundingMode.HALF_UP);
  }
  
  public void setPil2nbr(BigDecimal pPil2nbr) {
    if (pPil2nbr == null) {
      return;
    }
    pil2nbr = pPil2nbr.setScale(DECIMAL_PIL2NBR, RoundingMode.HALF_UP);
  }
  
  public void setPil2nbr(Integer pPil2nbr) {
    if (pPil2nbr == null) {
      return;
    }
    pil2nbr = BigDecimal.valueOf(pPil2nbr);
  }
  
  public Integer getPil2nbr() {
    return pil2nbr.intValue();
  }
  
  public void setPil22dls(BigDecimal pPil22dls) {
    if (pPil22dls == null) {
      return;
    }
    pil22dls = pPil22dls.setScale(DECIMAL_PIL22DLS, RoundingMode.HALF_UP);
  }
  
  public void setPil22dls(Integer pPil22dls) {
    if (pPil22dls == null) {
      return;
    }
    pil22dls = BigDecimal.valueOf(pPil22dls);
  }
  
  public void setPil22dls(Date pPil22dls) {
    if (pPil22dls == null) {
      return;
    }
    pil22dls = BigDecimal.valueOf(ConvertDate.dateToDb2(pPil22dls));
  }
  
  public Integer getPil22dls() {
    return pil22dls.intValue();
  }
  
  public Date getPil22dlsConvertiEnDate() {
    return ConvertDate.db2ToDate(pil22dls.intValue(), null);
  }
  
  public void setLitop(BigDecimal pLitop) {
    if (pLitop == null) {
      return;
    }
    litop = pLitop.setScale(DECIMAL_LITOP, RoundingMode.HALF_UP);
  }
  
  public void setLitop(Integer pLitop) {
    if (pLitop == null) {
      return;
    }
    litop = BigDecimal.valueOf(pLitop);
  }
  
  public Integer getLitop() {
    return litop.intValue();
  }
  
  public void setLicod(Character pLicod) {
    if (pLicod == null) {
      return;
    }
    licod = String.valueOf(pLicod);
  }
  
  public Character getLicod() {
    return licod.charAt(0);
  }
  
  public void setLietb(String pLietb) {
    if (pLietb == null) {
      return;
    }
    lietb = pLietb;
  }
  
  public String getLietb() {
    return lietb;
  }
  
  public void setLinum(BigDecimal pLinum) {
    if (pLinum == null) {
      return;
    }
    linum = pLinum.setScale(DECIMAL_LINUM, RoundingMode.HALF_UP);
  }
  
  public void setLinum(Integer pLinum) {
    if (pLinum == null) {
      return;
    }
    linum = BigDecimal.valueOf(pLinum);
  }
  
  public Integer getLinum() {
    return linum.intValue();
  }
  
  public void setLisuf(BigDecimal pLisuf) {
    if (pLisuf == null) {
      return;
    }
    lisuf = pLisuf.setScale(DECIMAL_LISUF, RoundingMode.HALF_UP);
  }
  
  public void setLisuf(Integer pLisuf) {
    if (pLisuf == null) {
      return;
    }
    lisuf = BigDecimal.valueOf(pLisuf);
  }
  
  public Integer getLisuf() {
    return lisuf.intValue();
  }
  
  public void setLinli(BigDecimal pLinli) {
    if (pLinli == null) {
      return;
    }
    linli = pLinli.setScale(DECIMAL_LINLI, RoundingMode.HALF_UP);
  }
  
  public void setLinli(Integer pLinli) {
    if (pLinli == null) {
      return;
    }
    linli = BigDecimal.valueOf(pLinli);
  }
  
  public Integer getLinli() {
    return linli.intValue();
  }
  
  public void setLierl(Character pLierl) {
    if (pLierl == null) {
      return;
    }
    lierl = String.valueOf(pLierl);
  }
  
  public Character getLierl() {
    return lierl.charAt(0);
  }
  
  public void setLicex(Character pLicex) {
    if (pLicex == null) {
      return;
    }
    licex = String.valueOf(pLicex);
  }
  
  public Character getLicex() {
    return licex.charAt(0);
  }
  
  public void setLiqex(BigDecimal pLiqex) {
    if (pLiqex == null) {
      return;
    }
    liqex = pLiqex.setScale(DECIMAL_LIQEX, RoundingMode.HALF_UP);
  }
  
  public void setLiqex(Double pLiqex) {
    if (pLiqex == null) {
      return;
    }
    liqex = BigDecimal.valueOf(pLiqex).setScale(DECIMAL_LIQEX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqex() {
    return liqex.setScale(DECIMAL_LIQEX, RoundingMode.HALF_UP);
  }
  
  public void setLitva(BigDecimal pLitva) {
    if (pLitva == null) {
      return;
    }
    litva = pLitva.setScale(DECIMAL_LITVA, RoundingMode.HALF_UP);
  }
  
  public void setLitva(Integer pLitva) {
    if (pLitva == null) {
      return;
    }
    litva = BigDecimal.valueOf(pLitva);
  }
  
  public Integer getLitva() {
    return litva.intValue();
  }
  
  public void setLitb(BigDecimal pLitb) {
    if (pLitb == null) {
      return;
    }
    litb = pLitb.setScale(DECIMAL_LITB, RoundingMode.HALF_UP);
  }
  
  public void setLitb(Integer pLitb) {
    if (pLitb == null) {
      return;
    }
    litb = BigDecimal.valueOf(pLitb);
  }
  
  public Integer getLitb() {
    return litb.intValue();
  }
  
  public void setLitr(BigDecimal pLitr) {
    if (pLitr == null) {
      return;
    }
    litr = pLitr.setScale(DECIMAL_LITR, RoundingMode.HALF_UP);
  }
  
  public void setLitr(Integer pLitr) {
    if (pLitr == null) {
      return;
    }
    litr = BigDecimal.valueOf(pLitr);
  }
  
  public Integer getLitr() {
    return litr.intValue();
  }
  
  public void setLitn(BigDecimal pLitn) {
    if (pLitn == null) {
      return;
    }
    litn = pLitn.setScale(DECIMAL_LITN, RoundingMode.HALF_UP);
  }
  
  public void setLitn(Integer pLitn) {
    if (pLitn == null) {
      return;
    }
    litn = BigDecimal.valueOf(pLitn);
  }
  
  public Integer getLitn() {
    return litn.intValue();
  }
  
  public void setLitc(BigDecimal pLitc) {
    if (pLitc == null) {
      return;
    }
    litc = pLitc.setScale(DECIMAL_LITC, RoundingMode.HALF_UP);
  }
  
  public void setLitc(Integer pLitc) {
    if (pLitc == null) {
      return;
    }
    litc = BigDecimal.valueOf(pLitc);
  }
  
  public Integer getLitc() {
    return litc.intValue();
  }
  
  public void setLitt(BigDecimal pLitt) {
    if (pLitt == null) {
      return;
    }
    litt = pLitt.setScale(DECIMAL_LITT, RoundingMode.HALF_UP);
  }
  
  public void setLitt(Integer pLitt) {
    if (pLitt == null) {
      return;
    }
    litt = BigDecimal.valueOf(pLitt);
  }
  
  public Integer getLitt() {
    return litt.intValue();
  }
  
  public void setLival(BigDecimal pLival) {
    if (pLival == null) {
      return;
    }
    lival = pLival.setScale(DECIMAL_LIVAL, RoundingMode.HALF_UP);
  }
  
  public void setLival(Integer pLival) {
    if (pLival == null) {
      return;
    }
    lival = BigDecimal.valueOf(pLival);
  }
  
  public Integer getLival() {
    return lival.intValue();
  }
  
  public void setLitar(BigDecimal pLitar) {
    if (pLitar == null) {
      return;
    }
    litar = pLitar.setScale(DECIMAL_LITAR, RoundingMode.HALF_UP);
  }
  
  public void setLitar(Integer pLitar) {
    if (pLitar == null) {
      return;
    }
    litar = BigDecimal.valueOf(pLitar);
  }
  
  public Integer getLitar() {
    return litar.intValue();
  }
  
  public void setLicol(BigDecimal pLicol) {
    if (pLicol == null) {
      return;
    }
    licol = pLicol.setScale(DECIMAL_LICOL, RoundingMode.HALF_UP);
  }
  
  public void setLicol(Integer pLicol) {
    if (pLicol == null) {
      return;
    }
    licol = BigDecimal.valueOf(pLicol);
  }
  
  public Integer getLicol() {
    return licol.intValue();
  }
  
  public void setLitpf(BigDecimal pLitpf) {
    if (pLitpf == null) {
      return;
    }
    litpf = pLitpf.setScale(DECIMAL_LITPF, RoundingMode.HALF_UP);
  }
  
  public void setLitpf(Integer pLitpf) {
    if (pLitpf == null) {
      return;
    }
    litpf = BigDecimal.valueOf(pLitpf);
  }
  
  public Integer getLitpf() {
    return litpf.intValue();
  }
  
  public void setLidcv(BigDecimal pLidcv) {
    if (pLidcv == null) {
      return;
    }
    lidcv = pLidcv.setScale(DECIMAL_LIDCV, RoundingMode.HALF_UP);
  }
  
  public void setLidcv(Integer pLidcv) {
    if (pLidcv == null) {
      return;
    }
    lidcv = BigDecimal.valueOf(pLidcv);
  }
  
  public Integer getLidcv() {
    return lidcv.intValue();
  }
  
  public void setLitnc(BigDecimal pLitnc) {
    if (pLitnc == null) {
      return;
    }
    litnc = pLitnc.setScale(DECIMAL_LITNC, RoundingMode.HALF_UP);
  }
  
  public void setLitnc(Integer pLitnc) {
    if (pLitnc == null) {
      return;
    }
    litnc = BigDecimal.valueOf(pLitnc);
  }
  
  public Integer getLitnc() {
    return litnc.intValue();
  }
  
  public void setLisgn(BigDecimal pLisgn) {
    if (pLisgn == null) {
      return;
    }
    lisgn = pLisgn.setScale(DECIMAL_LISGN, RoundingMode.HALF_UP);
  }
  
  public void setLisgn(Integer pLisgn) {
    if (pLisgn == null) {
      return;
    }
    lisgn = BigDecimal.valueOf(pLisgn);
  }
  
  public Integer getLisgn() {
    return lisgn.intValue();
  }
  
  public void setLiser(BigDecimal pLiser) {
    if (pLiser == null) {
      return;
    }
    liser = pLiser.setScale(DECIMAL_LISER, RoundingMode.HALF_UP);
  }
  
  public void setLiser(Integer pLiser) {
    if (pLiser == null) {
      return;
    }
    liser = BigDecimal.valueOf(pLiser);
  }
  
  public Integer getLiser() {
    return liser.intValue();
  }
  
  public void setLiqte(BigDecimal pLiqte) {
    if (pLiqte == null) {
      return;
    }
    liqte = pLiqte.setScale(DECIMAL_LIQTE, RoundingMode.HALF_UP);
  }
  
  public void setLiqte(Double pLiqte) {
    if (pLiqte == null) {
      return;
    }
    liqte = BigDecimal.valueOf(pLiqte).setScale(DECIMAL_LIQTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqte() {
    return liqte.setScale(DECIMAL_LIQTE, RoundingMode.HALF_UP);
  }
  
  public void setLiksv(BigDecimal pLiksv) {
    if (pLiksv == null) {
      return;
    }
    liksv = pLiksv.setScale(DECIMAL_LIKSV, RoundingMode.HALF_UP);
  }
  
  public void setLiksv(Double pLiksv) {
    if (pLiksv == null) {
      return;
    }
    liksv = BigDecimal.valueOf(pLiksv).setScale(DECIMAL_LIKSV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiksv() {
    return liksv.setScale(DECIMAL_LIKSV, RoundingMode.HALF_UP);
  }
  
  public void setLipvb(BigDecimal pLipvb) {
    if (pLipvb == null) {
      return;
    }
    lipvb = pLipvb.setScale(DECIMAL_LIPVB, RoundingMode.HALF_UP);
  }
  
  public void setLipvb(Double pLipvb) {
    if (pLipvb == null) {
      return;
    }
    lipvb = BigDecimal.valueOf(pLipvb).setScale(DECIMAL_LIPVB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipvb() {
    return lipvb.setScale(DECIMAL_LIPVB, RoundingMode.HALF_UP);
  }
  
  public void setLirem1(BigDecimal pLirem1) {
    if (pLirem1 == null) {
      return;
    }
    lirem1 = pLirem1.setScale(DECIMAL_LIREM1, RoundingMode.HALF_UP);
  }
  
  public void setLirem1(Double pLirem1) {
    if (pLirem1 == null) {
      return;
    }
    lirem1 = BigDecimal.valueOf(pLirem1).setScale(DECIMAL_LIREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem1() {
    return lirem1.setScale(DECIMAL_LIREM1, RoundingMode.HALF_UP);
  }
  
  public void setLirem2(BigDecimal pLirem2) {
    if (pLirem2 == null) {
      return;
    }
    lirem2 = pLirem2.setScale(DECIMAL_LIREM2, RoundingMode.HALF_UP);
  }
  
  public void setLirem2(Double pLirem2) {
    if (pLirem2 == null) {
      return;
    }
    lirem2 = BigDecimal.valueOf(pLirem2).setScale(DECIMAL_LIREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem2() {
    return lirem2.setScale(DECIMAL_LIREM2, RoundingMode.HALF_UP);
  }
  
  public void setLirem3(BigDecimal pLirem3) {
    if (pLirem3 == null) {
      return;
    }
    lirem3 = pLirem3.setScale(DECIMAL_LIREM3, RoundingMode.HALF_UP);
  }
  
  public void setLirem3(Double pLirem3) {
    if (pLirem3 == null) {
      return;
    }
    lirem3 = BigDecimal.valueOf(pLirem3).setScale(DECIMAL_LIREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem3() {
    return lirem3.setScale(DECIMAL_LIREM3, RoundingMode.HALF_UP);
  }
  
  public void setLirem4(BigDecimal pLirem4) {
    if (pLirem4 == null) {
      return;
    }
    lirem4 = pLirem4.setScale(DECIMAL_LIREM4, RoundingMode.HALF_UP);
  }
  
  public void setLirem4(Double pLirem4) {
    if (pLirem4 == null) {
      return;
    }
    lirem4 = BigDecimal.valueOf(pLirem4).setScale(DECIMAL_LIREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem4() {
    return lirem4.setScale(DECIMAL_LIREM4, RoundingMode.HALF_UP);
  }
  
  public void setLirem5(BigDecimal pLirem5) {
    if (pLirem5 == null) {
      return;
    }
    lirem5 = pLirem5.setScale(DECIMAL_LIREM5, RoundingMode.HALF_UP);
  }
  
  public void setLirem5(Double pLirem5) {
    if (pLirem5 == null) {
      return;
    }
    lirem5 = BigDecimal.valueOf(pLirem5).setScale(DECIMAL_LIREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem5() {
    return lirem5.setScale(DECIMAL_LIREM5, RoundingMode.HALF_UP);
  }
  
  public void setLirem6(BigDecimal pLirem6) {
    if (pLirem6 == null) {
      return;
    }
    lirem6 = pLirem6.setScale(DECIMAL_LIREM6, RoundingMode.HALF_UP);
  }
  
  public void setLirem6(Double pLirem6) {
    if (pLirem6 == null) {
      return;
    }
    lirem6 = BigDecimal.valueOf(pLirem6).setScale(DECIMAL_LIREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLirem6() {
    return lirem6.setScale(DECIMAL_LIREM6, RoundingMode.HALF_UP);
  }
  
  public void setLitrl(Character pLitrl) {
    if (pLitrl == null) {
      return;
    }
    litrl = String.valueOf(pLitrl);
  }
  
  public Character getLitrl() {
    return litrl.charAt(0);
  }
  
  public void setLibrl(Character pLibrl) {
    if (pLibrl == null) {
      return;
    }
    librl = String.valueOf(pLibrl);
  }
  
  public Character getLibrl() {
    return librl.charAt(0);
  }
  
  public void setLirp1(Character pLirp1) {
    if (pLirp1 == null) {
      return;
    }
    lirp1 = String.valueOf(pLirp1);
  }
  
  public Character getLirp1() {
    return lirp1.charAt(0);
  }
  
  public void setLirp2(Character pLirp2) {
    if (pLirp2 == null) {
      return;
    }
    lirp2 = String.valueOf(pLirp2);
  }
  
  public Character getLirp2() {
    return lirp2.charAt(0);
  }
  
  public void setLirp3(Character pLirp3) {
    if (pLirp3 == null) {
      return;
    }
    lirp3 = String.valueOf(pLirp3);
  }
  
  public Character getLirp3() {
    return lirp3.charAt(0);
  }
  
  public void setLirp4(Character pLirp4) {
    if (pLirp4 == null) {
      return;
    }
    lirp4 = String.valueOf(pLirp4);
  }
  
  public Character getLirp4() {
    return lirp4.charAt(0);
  }
  
  public void setLirp5(Character pLirp5) {
    if (pLirp5 == null) {
      return;
    }
    lirp5 = String.valueOf(pLirp5);
  }
  
  public Character getLirp5() {
    return lirp5.charAt(0);
  }
  
  public void setLirp6(Character pLirp6) {
    if (pLirp6 == null) {
      return;
    }
    lirp6 = String.valueOf(pLirp6);
  }
  
  public Character getLirp6() {
    return lirp6.charAt(0);
  }
  
  public void setLipvn(BigDecimal pLipvn) {
    if (pLipvn == null) {
      return;
    }
    lipvn = pLipvn.setScale(DECIMAL_LIPVN, RoundingMode.HALF_UP);
  }
  
  public void setLipvn(Double pLipvn) {
    if (pLipvn == null) {
      return;
    }
    lipvn = BigDecimal.valueOf(pLipvn).setScale(DECIMAL_LIPVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipvn() {
    return lipvn.setScale(DECIMAL_LIPVN, RoundingMode.HALF_UP);
  }
  
  public void setLipvc(BigDecimal pLipvc) {
    if (pLipvc == null) {
      return;
    }
    lipvc = pLipvc.setScale(DECIMAL_LIPVC, RoundingMode.HALF_UP);
  }
  
  public void setLipvc(Double pLipvc) {
    if (pLipvc == null) {
      return;
    }
    lipvc = BigDecimal.valueOf(pLipvc).setScale(DECIMAL_LIPVC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipvc() {
    return lipvc.setScale(DECIMAL_LIPVC, RoundingMode.HALF_UP);
  }
  
  public void setLimht(BigDecimal pLimht) {
    if (pLimht == null) {
      return;
    }
    limht = pLimht.setScale(DECIMAL_LIMHT, RoundingMode.HALF_UP);
  }
  
  public void setLimht(Double pLimht) {
    if (pLimht == null) {
      return;
    }
    limht = BigDecimal.valueOf(pLimht).setScale(DECIMAL_LIMHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLimht() {
    return limht.setScale(DECIMAL_LIMHT, RoundingMode.HALF_UP);
  }
  
  public void setLiprp(BigDecimal pLiprp) {
    if (pLiprp == null) {
      return;
    }
    liprp = pLiprp.setScale(DECIMAL_LIPRP, RoundingMode.HALF_UP);
  }
  
  public void setLiprp(Double pLiprp) {
    if (pLiprp == null) {
      return;
    }
    liprp = BigDecimal.valueOf(pLiprp).setScale(DECIMAL_LIPRP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiprp() {
    return liprp.setScale(DECIMAL_LIPRP, RoundingMode.HALF_UP);
  }
  
  public void setLicoe(BigDecimal pLicoe) {
    if (pLicoe == null) {
      return;
    }
    licoe = pLicoe.setScale(DECIMAL_LICOE, RoundingMode.HALF_UP);
  }
  
  public void setLicoe(Double pLicoe) {
    if (pLicoe == null) {
      return;
    }
    licoe = BigDecimal.valueOf(pLicoe).setScale(DECIMAL_LICOE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLicoe() {
    return licoe.setScale(DECIMAL_LICOE, RoundingMode.HALF_UP);
  }
  
  public void setLiprv(BigDecimal pLiprv) {
    if (pLiprv == null) {
      return;
    }
    liprv = pLiprv.setScale(DECIMAL_LIPRV, RoundingMode.HALF_UP);
  }
  
  public void setLiprv(Double pLiprv) {
    if (pLiprv == null) {
      return;
    }
    liprv = BigDecimal.valueOf(pLiprv).setScale(DECIMAL_LIPRV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiprv() {
    return liprv.setScale(DECIMAL_LIPRV, RoundingMode.HALF_UP);
  }
  
  public void setLiavr(Character pLiavr) {
    if (pLiavr == null) {
      return;
    }
    liavr = String.valueOf(pLiavr);
  }
  
  public Character getLiavr() {
    return liavr.charAt(0);
  }
  
  public void setLiunv(String pLiunv) {
    if (pLiunv == null) {
      return;
    }
    liunv = pLiunv;
  }
  
  public String getLiunv() {
    return liunv;
  }
  
  public void setLiart(String pLiart) {
    if (pLiart == null) {
      return;
    }
    liart = pLiart;
  }
  
  public String getLiart() {
    return liart;
  }
  
  public void setLicpl(String pLicpl) {
    if (pLicpl == null) {
      return;
    }
    licpl = pLicpl;
  }
  
  public String getLicpl() {
    return licpl;
  }
  
  public void setLicnd(BigDecimal pLicnd) {
    if (pLicnd == null) {
      return;
    }
    licnd = pLicnd.setScale(DECIMAL_LICND, RoundingMode.HALF_UP);
  }
  
  public void setLicnd(Double pLicnd) {
    if (pLicnd == null) {
      return;
    }
    licnd = BigDecimal.valueOf(pLicnd).setScale(DECIMAL_LICND, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLicnd() {
    return licnd.setScale(DECIMAL_LICND, RoundingMode.HALF_UP);
  }
  
  public void setLiin1(Character pLiin1) {
    if (pLiin1 == null) {
      return;
    }
    liin1 = String.valueOf(pLiin1);
  }
  
  public Character getLiin1() {
    return liin1.charAt(0);
  }
  
  public void setLiprt(BigDecimal pLiprt) {
    if (pLiprt == null) {
      return;
    }
    liprt = pLiprt.setScale(DECIMAL_LIPRT, RoundingMode.HALF_UP);
  }
  
  public void setLiprt(Double pLiprt) {
    if (pLiprt == null) {
      return;
    }
    liprt = BigDecimal.valueOf(pLiprt).setScale(DECIMAL_LIPRT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiprt() {
    return liprt.setScale(DECIMAL_LIPRT, RoundingMode.HALF_UP);
  }
  
  public void setLidlp(BigDecimal pLidlp) {
    if (pLidlp == null) {
      return;
    }
    lidlp = pLidlp.setScale(DECIMAL_LIDLP, RoundingMode.HALF_UP);
  }
  
  public void setLidlp(Integer pLidlp) {
    if (pLidlp == null) {
      return;
    }
    lidlp = BigDecimal.valueOf(pLidlp);
  }
  
  public void setLidlp(Date pLidlp) {
    if (pLidlp == null) {
      return;
    }
    lidlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pLidlp));
  }
  
  public Integer getLidlp() {
    return lidlp.intValue();
  }
  
  public Date getLidlpConvertiEnDate() {
    return ConvertDate.db2ToDate(lidlp.intValue(), null);
  }
  
  public void setLiin2(Character pLiin2) {
    if (pLiin2 == null) {
      return;
    }
    liin2 = String.valueOf(pLiin2);
  }
  
  public Character getLiin2() {
    return liin2.charAt(0);
  }
  
  public void setLiin3(Character pLiin3) {
    if (pLiin3 == null) {
      return;
    }
    liin3 = String.valueOf(pLiin3);
  }
  
  public Character getLiin3() {
    return liin3.charAt(0);
  }
  
  public void setLitp1(String pLitp1) {
    if (pLitp1 == null) {
      return;
    }
    litp1 = pLitp1;
  }
  
  public String getLitp1() {
    return litp1;
  }
  
  public void setLitp2(String pLitp2) {
    if (pLitp2 == null) {
      return;
    }
    litp2 = pLitp2;
  }
  
  public String getLitp2() {
    return litp2;
  }
  
  public void setLitp3(String pLitp3) {
    if (pLitp3 == null) {
      return;
    }
    litp3 = pLitp3;
  }
  
  public String getLitp3() {
    return litp3;
  }
  
  public void setLitp4(String pLitp4) {
    if (pLitp4 == null) {
      return;
    }
    litp4 = pLitp4;
  }
  
  public String getLitp4() {
    return litp4;
  }
  
  public void setLitp5(String pLitp5) {
    if (pLitp5 == null) {
      return;
    }
    litp5 = pLitp5;
  }
  
  public String getLitp5() {
    return litp5;
  }
  
  public void setLigba(BigDecimal pLigba) {
    if (pLigba == null) {
      return;
    }
    ligba = pLigba.setScale(DECIMAL_LIGBA, RoundingMode.HALF_UP);
  }
  
  public void setLigba(Integer pLigba) {
    if (pLigba == null) {
      return;
    }
    ligba = BigDecimal.valueOf(pLigba);
  }
  
  public Integer getLigba() {
    return ligba.intValue();
  }
  
  public void setLiarts(String pLiarts) {
    if (pLiarts == null) {
      return;
    }
    liarts = pLiarts;
  }
  
  public String getLiarts() {
    return liarts;
  }
  
  public void setLiin4(Character pLiin4) {
    if (pLiin4 == null) {
      return;
    }
    liin4 = String.valueOf(pLiin4);
  }
  
  public Character getLiin4() {
    return liin4.charAt(0);
  }
  
  public void setLiin5(Character pLiin5) {
    if (pLiin5 == null) {
      return;
    }
    liin5 = String.valueOf(pLiin5);
  }
  
  public Character getLiin5() {
    return liin5.charAt(0);
  }
  
  public void setLiin6(Character pLiin6) {
    if (pLiin6 == null) {
      return;
    }
    liin6 = String.valueOf(pLiin6);
  }
  
  public Character getLiin6() {
    return liin6.charAt(0);
  }
  
  public void setLiin7(Character pLiin7) {
    if (pLiin7 == null) {
      return;
    }
    liin7 = String.valueOf(pLiin7);
  }
  
  public Character getLiin7() {
    return liin7.charAt(0);
  }
  
  public void setLiqtl(BigDecimal pLiqtl) {
    if (pLiqtl == null) {
      return;
    }
    liqtl = pLiqtl.setScale(DECIMAL_LIQTL, RoundingMode.HALF_UP);
  }
  
  public void setLiqtl(Double pLiqtl) {
    if (pLiqtl == null) {
      return;
    }
    liqtl = BigDecimal.valueOf(pLiqtl).setScale(DECIMAL_LIQTL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqtl() {
    return liqtl.setScale(DECIMAL_LIQTL, RoundingMode.HALF_UP);
  }
  
  public void setLimag(String pLimag) {
    if (pLimag == null) {
      return;
    }
    limag = pLimag;
  }
  
  public String getLimag() {
    return limag;
  }
  
  public void setLirep(String pLirep) {
    if (pLirep == null) {
      return;
    }
    lirep = pLirep;
  }
  
  public String getLirep() {
    return lirep;
  }
  
  public void setLiin8(Character pLiin8) {
    if (pLiin8 == null) {
      return;
    }
    liin8 = String.valueOf(pLiin8);
  }
  
  public Character getLiin8() {
    return liin8.charAt(0);
  }
  
  public void setLiin9(Character pLiin9) {
    if (pLiin9 == null) {
      return;
    }
    liin9 = String.valueOf(pLiin9);
  }
  
  public Character getLiin9() {
    return liin9.charAt(0);
  }
  
  public void setLiin10(Character pLiin10) {
    if (pLiin10 == null) {
      return;
    }
    liin10 = String.valueOf(pLiin10);
  }
  
  public Character getLiin10() {
    return liin10.charAt(0);
  }
  
  public void setLiin11(Character pLiin11) {
    if (pLiin11 == null) {
      return;
    }
    liin11 = String.valueOf(pLiin11);
  }
  
  public Character getLiin11() {
    return liin11.charAt(0);
  }
  
  public void setLiin12(Character pLiin12) {
    if (pLiin12 == null) {
      return;
    }
    liin12 = String.valueOf(pLiin12);
  }
  
  public Character getLiin12() {
    return liin12.charAt(0);
  }
  
  public void setLinli0(BigDecimal pLinli0) {
    if (pLinli0 == null) {
      return;
    }
    linli0 = pLinli0.setScale(DECIMAL_LINLI0, RoundingMode.HALF_UP);
  }
  
  public void setLinli0(Integer pLinli0) {
    if (pLinli0 == null) {
      return;
    }
    linli0 = BigDecimal.valueOf(pLinli0);
  }
  
  public Integer getLinli0() {
    return linli0.intValue();
  }
  
  public void setLipra(BigDecimal pLipra) {
    if (pLipra == null) {
      return;
    }
    lipra = pLipra.setScale(DECIMAL_LIPRA, RoundingMode.HALF_UP);
  }
  
  public void setLipra(Double pLipra) {
    if (pLipra == null) {
      return;
    }
    lipra = BigDecimal.valueOf(pLipra).setScale(DECIMAL_LIPRA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipra() {
    return lipra.setScale(DECIMAL_LIPRA, RoundingMode.HALF_UP);
  }
  
  public void setLipva(BigDecimal pLipva) {
    if (pLipva == null) {
      return;
    }
    lipva = pLipva.setScale(DECIMAL_LIPVA, RoundingMode.HALF_UP);
  }
  
  public void setLipva(Double pLipva) {
    if (pLipva == null) {
      return;
    }
    lipva = BigDecimal.valueOf(pLipva).setScale(DECIMAL_LIPVA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLipva() {
    return lipva.setScale(DECIMAL_LIPVA, RoundingMode.HALF_UP);
  }
  
  public void setLiqtp(BigDecimal pLiqtp) {
    if (pLiqtp == null) {
      return;
    }
    liqtp = pLiqtp.setScale(DECIMAL_LIQTP, RoundingMode.HALF_UP);
  }
  
  public void setLiqtp(Double pLiqtp) {
    if (pLiqtp == null) {
      return;
    }
    liqtp = BigDecimal.valueOf(pLiqtp).setScale(DECIMAL_LIQTP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLiqtp() {
    return liqtp.setScale(DECIMAL_LIQTP, RoundingMode.HALF_UP);
  }
  
  public void setLimtr(BigDecimal pLimtr) {
    if (pLimtr == null) {
      return;
    }
    limtr = pLimtr.setScale(DECIMAL_LIMTR, RoundingMode.HALF_UP);
  }
  
  public void setLimtr(Double pLimtr) {
    if (pLimtr == null) {
      return;
    }
    limtr = BigDecimal.valueOf(pLimtr).setScale(DECIMAL_LIMTR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLimtr() {
    return limtr.setScale(DECIMAL_LIMTR, RoundingMode.HALF_UP);
  }
  
  public void setLiser3(Character pLiser3) {
    if (pLiser3 == null) {
      return;
    }
    liser3 = String.valueOf(pLiser3);
  }
  
  public Character getLiser3() {
    return liser3.charAt(0);
  }
  
  public void setLiser5(Character pLiser5) {
    if (pLiser5 == null) {
      return;
    }
    liser5 = String.valueOf(pLiser5);
  }
  
  public Character getLiser5() {
    return liser5.charAt(0);
  }
  
  public void setLiin17(Character pLiin17) {
    if (pLiin17 == null) {
      return;
    }
    liin17 = String.valueOf(pLiin17);
  }
  
  public Character getLiin17() {
    return liin17.charAt(0);
  }
  
  public void setLiin18(Character pLiin18) {
    if (pLiin18 == null) {
      return;
    }
    liin18 = String.valueOf(pLiin18);
  }
  
  public Character getLiin18() {
    return liin18.charAt(0);
  }
  
  public void setLiin19(Character pLiin19) {
    if (pLiin19 == null) {
      return;
    }
    liin19 = String.valueOf(pLiin19);
  }
  
  public Character getLiin19() {
    return liin19.charAt(0);
  }
  
  public void setLiin20(Character pLiin20) {
    if (pLiin20 == null) {
      return;
    }
    liin20 = String.valueOf(pLiin20);
  }
  
  public Character getLiin20() {
    return liin20.charAt(0);
  }
  
  public void setLiin21(Character pLiin21) {
    if (pLiin21 == null) {
      return;
    }
    liin21 = String.valueOf(pLiin21);
  }
  
  public Character getLiin21() {
    return liin21.charAt(0);
  }
  
  public void setLiin22(Character pLiin22) {
    if (pLiin22 == null) {
      return;
    }
    liin22 = String.valueOf(pLiin22);
  }
  
  public Character getLiin22() {
    return liin22.charAt(0);
  }
}
