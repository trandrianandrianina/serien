/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database;

import ri.serien.libas400.database.record.GenericRecord;

/**
 * Lorsque l'on créé une classe basée sur la définition d'un fichier DB2, il faut dériver depuis cette classe de base
 * (voir SérieNMetier dans exp.program.contact)
 */
public abstract class BaseFileDB {
  // Variables de travail
  protected QueryManager querymg = null;
  protected String[] omittedField = null;
  protected GenericRecord genericrecord = new GenericRecord();

  // Conserve le dernier message d'erreur émit et non lu
  protected String msgErreur = "";

  /**
   * Constructeur.
   */
  public BaseFileDB(QueryManager aquerymg) {
    setQuerymg(aquerymg);
  }

  // -- Méthodes abstract ----------------------------------------------------

  public abstract void initialization();

  public abstract boolean insertInDatabase();

  public abstract boolean updateInDatabase();

  public abstract boolean deleteInDatabase();

  public abstract void dispose();

  // -- Méthodes publiques --------------------------------------------------

  /**
   * @return le querymg.
   */
  public QueryManager getQuerymg() {
    return querymg;
  }

  /**
   * @param querymg Le querymg à définir.
   */
  public void setQuerymg(QueryManager querymg) {
    this.querymg = querymg;
  }

  /**
   * Initialise les données du record avec cet objet.
   */
  public boolean initGenericRecord(GenericRecord rcd, boolean clearBefore) {
    if (rcd == null) {
      return false;
    }

    rcd.setOmittedField(omittedField);
    return rcd.fromObject(this, clearBefore);
  }

  /**
   * Initialise l'objet avec les données du record.
   */
  public boolean initObject(GenericRecord rcd, boolean doinit) {
    if (rcd == null) {
      return false;
    }

    if (doinit) {
      initialization();
    }
    return rcd.toObject(this);
  }

  public String[] getOmittedField() {
    return omittedField;
  }

  // -- Méthodes protégées --------------------------------------------------

  /**
   * Requête permettant de lancer une requête.
   */
  protected boolean request(String requete) {
    int ret = querymg.requete(requete);
    if (ret == -1) {
      msgErreur += '\n' + querymg.getMsgError();
      return false;
    }
    return true;
  }

  /**
   * Requête permettant de lancer une requête.
   */
  protected boolean securerequest(String requete) {
    int ret = querymg.requete(requete, genericrecord);
    if (ret == -1) {
      msgErreur += '\n' + querymg.getMsgError();
      return false;
    }
    return true;
  }

  // -- Accesseurs ----------------------------------------------------------

  /**
   * @param omittedField Le omittedField à définir.
   */
  public void setOmittedField(String[] omittedField) {
    this.omittedField = omittedField;
  }

  /**
   * @return le genericrecord.
   */
  public GenericRecord getGenericrecord() {
    return genericrecord;
  }

  /**
   * @param genericrecord Le genericrecord à définir.
   */
  public void setGenericrecord(GenericRecord genericrecord) {
    this.genericrecord = genericrecord;
  }

  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";

    return chaine;
  }

}
