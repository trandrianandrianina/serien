/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database.field;

import java.sql.Timestamp;
import java.text.DecimalFormat;

/**
 * Description de base d'un champ alphanumérique.
 */
public class FieldTimestamp extends Field<Timestamp> {
  // Variable
  public Timestamp valeur;
  
  /**
   * Constructeur.
   */
  public FieldTimestamp() {
    sqltype = "java.sql.Timestamp";
  }
  
  /**
   * Constructeur.
   */
  public FieldTimestamp(Timestamp valeur) {
    sqltype = "java.sql.Timestamp";
    setValue(valeur);
  }
  
  /**
   * Constructeur.
   */
  public FieldTimestamp(String nom, Timestamp valeur) {
    name = nom;
    sqltype = "java.sql.Timestamp";
    setValue(valeur);
  }
  
  /**
   * Constructeur.
   */
  public FieldTimestamp(Timestamp valeur, int asize, int adecimal) {
    sqltype = "java.sql.Timestamp";
    setValue(valeur);
    setSize(asize, adecimal);
  }
  
  /**
   * Constructeur.
   */
  public FieldTimestamp(int asize, int adecimal) {
    sqltype = "java.sql.Timestamp";
    setSize(asize, adecimal);
  }
  
  @Override
  public Timestamp getValue() {
    return valeur;
  }
  
  @Override
  public void setValue(Timestamp valeur) {
    this.valeur = valeur;
  }
  
  @Override
  public String toString() {
    return valeur != null ? valeur.toString() : null;
  }
  
  @Override
  public String toFormattedString(int size, int adecimal) {
    if (valeur == null) {
      return toString();
    }
    // DecimalFormat df = new DecimalFormat(format);
    // return df.format(valeur.doubleValue()).replace(",", "");
    DecimalFormat df = new DecimalFormat("#");
    df.setMinimumIntegerDigits(size - adecimal);
    df.setMinimumFractionDigits(adecimal);
    return df.format(valeur).replace(",", "");
  }
  
  @Override
  public String toFormattedString() {
    return toFormattedString(length, decimal);
  }
  
  @Override
  public Timestamp getTrimValue() {
    return valeur;
  }
  
  @Override
  public void setSize(int asize) {
    length = asize;
    decimal = 0;
  }
  
  @Override
  public void setSize(int asize, int adecimal) {
    length = asize;
    if (adecimal < 0) {
      decimal = 0;
    }
    decimal = adecimal;
  }
  
  @Override
  public String getSQLType() {
    return sqltype;
  }
  
  @Override
  public void setSQLType(String sqltype) {
    this.sqltype = sqltype;
  }
  
}
