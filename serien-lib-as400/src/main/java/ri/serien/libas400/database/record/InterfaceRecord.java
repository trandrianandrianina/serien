/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database.record;

import com.google.gson.JsonObject;

public interface InterfaceRecord {
  public void setField(String namefield, Object value);
  
  public Object getField(int indice);
  
  public Object getField(String namefield);
  
  /**
   * Retourne le record sous forme d'une ligne (string) avec les données à la bonne place (offset).
   */
  public StringBuffer getFlat();
  
  /**
   * Initialise l'entête avec un enregistrement plat.
   */
  public boolean setFlat(String record);
  
  /**
   * Retourne un Objet de type JSON.
   */
  public JsonObject getJSON(boolean trim);
  
  /**
   * Initialise l'entête avec un enregistrement au format JSON.
   */
  public boolean setJSON(String record);
  
}
