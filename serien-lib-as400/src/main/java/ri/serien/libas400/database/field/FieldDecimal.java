/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database.field;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Description de base d'un champ alphanumérique.
 */
public class FieldDecimal extends Field<BigDecimal> {
  // Variable
  public BigDecimal valeur;
  
  /**
   * Constructeur.
   */
  public FieldDecimal() {
    sqltype = "java.math.BigDecimal";
  }
  
  /**
   * Constructeur.
   */
  public FieldDecimal(String nom, BigDecimal valeur, int asize, int adecimal) {
    name = nom;
    sqltype = "java.math.BigDecimal";
    setValue(valeur);
    setSize(asize, adecimal);
  }
  
  /**
   * Constructeur.
   */
  public FieldDecimal(BigDecimal valeur, int asize, int adecimal) {
    sqltype = "java.math.BigDecimal";
    setValue(valeur);
    setSize(asize, adecimal);
  }
  
  /**
   * Constructeur.
   */
  public FieldDecimal(int asize, int adecimal) {
    sqltype = "java.math.BigDecimal";
    setSize(asize, adecimal);
  }
  
  @Override
  public BigDecimal getValue() {
    return valeur;
  }
  
  @Override
  public void setValue(BigDecimal valeur) {
    this.valeur = valeur;
  }
  
  @Override
  public String toString() {
    return valeur != null ? valeur.toString() : null;
  }
  
  @Override
  public String toFormattedString(int size, int adecimal) {
    if (valeur == null) {
      return toString();
    }
    DecimalFormat df = new DecimalFormat("#");
    df.setMinimumIntegerDigits(size - adecimal);
    df.setMinimumFractionDigits(adecimal);
    return df.format(valeur.doubleValue()).replaceAll("[.,]+", "");
  }
  
  @Override
  public String toFormattedString() {
    return toFormattedString(length, decimal);
  }
  
  @Override
  public BigDecimal getTrimValue() {
    return valeur;
  }
  
  @Override
  public void setSize(int asize) {
    length = asize;
    decimal = 0;
  }
  
  @Override
  public void setSize(int asize, int adecimal) {
    length = asize;
    if (adecimal < 0) {
      decimal = 0;
    }
    decimal = adecimal;
  }
  
  @Override
  public String getSQLType() {
    return sqltype;
  }
  
  @Override
  public void setSQLType(String sqltype) {
    this.sqltype = sqltype;
  }
  
}
