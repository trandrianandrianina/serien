/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database.record;

import java.sql.Connection;

import ri.serien.libas400.database.QueryManager;

/**
 * Gère les opérations de base sur les records génériques (TODO A voir son utilité).
 */
public class GenericRecordManager extends QueryManager {
  
  /**
   * Constructeur.
   */
  public GenericRecordManager(Connection database) {
    super(database);
  }
  
}
