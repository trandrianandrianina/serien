/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database.record;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.FieldDescription;
import com.ibm.as400.access.Record;
import com.ibm.as400.access.RecordFormat;

public abstract class DataStructureRecord {
  // Variables
  // Longueur du record (et non de la datastructure)
  public int length = 0;
  protected RecordFormat rf = new RecordFormat();
  protected int[] offsetField;
  protected ArrayList<byte[]> listFields;
  
  /**
   * Constructeur.
   */
  public DataStructureRecord() {
    initRecord();
    prepareAnalyze();
  }
  
  /**
   * Initialise le record avec la data structure.
   */
  protected abstract void initRecord();
  
  /**
   * Initialise l'enregistrement avec les données.
   */
  public Record setBytes(byte[] data) {
    try {
      return rf.getNewRecord(analyzeNumeric(data));
    }
    catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }
  }
  
  /**
   * Retourne le format du record.
   */
  public RecordFormat getRecordFormat() {
    return rf;
  }
  
  /**
   * Prépare les variables pour l'analyse des champs lors des requêtes.
   */
  private void prepareAnalyze() {
    // Préparation des masques pour détecter si les zones numériques ne contiennent pas de l'alpha
    FieldDescription[] descriptions = rf.getFieldDescriptions();
    // On triche plus simple pour la boucle for ensuite
    offsetField = new int[descriptions.length + 1];
    offsetField[0] = 0;
    listFields = new ArrayList<byte[]>();
    
    for (int i = 0; i < descriptions.length; i++) {
      if (descriptions[i].getDataType().getInstanceType() == AS400DataType.TYPE_ZONED) {
        listFields.add(new byte[descriptions[i].getLength()]);
        offsetField[i + 1] = offsetField[i] + descriptions[i].getLength();
      }
      else if (descriptions[i].getDataType().getInstanceType() == AS400DataType.TYPE_PACKED) {
        listFields.add(null);
        offsetField[i + 1] = offsetField[i] + (descriptions[i].getLength() + 1) / 2;
      }
      else {
        listFields.add(null);
        offsetField[i + 1] = offsetField[i] + descriptions[i].getLength();
      }
    }
  }
  
  /**
   * Analyse les données numériques à appliquer sur la datastructure afin d'éviter les crashs.
   */
  private byte[] analyzeNumeric(byte[] contents) {
    byte[] field;
    
    // On contrôle la validité des champs décimaux (parfois on trouve de l'alpha dedans !!)
    for (int i = 0; i < rf.getNumberOfFields(); i++) {
      // Si le champs est numérique alors on lui fait subir le traitement
      if (listFields.get(i) != null) {
        field = listFields.get(i);
        System.arraycopy(contents, offsetField[i], field, 0, field.length);
        if (!testNumericContent(field)) {
          System.arraycopy(field, 0, contents, offsetField[i], field.length);
        }
      }
    }
    return contents;
  }
  
  /**
   * Test le contenu du tableau de byte s'il ne contient pas de valeur numérique on le force à zéro.
   */
  private boolean testNumericContent(byte[] field) {
    for (int i = 0; i < field.length; i++) {
      // String.format("%02X ", (byte)(contents[i] ^ 0xff)) + " " + (byte)(contents[i] ^ 0xff));
      // Si le résultat du masque binaire est supérieur à 15 (0x0f) alors ce n'est pas un numérique
      if ((field[i] ^ 0xff) > 0x0f) {
        Arrays.fill(field, (byte) 0xf0);
        return false;
      }
    }
    return true;
  }
  
}
