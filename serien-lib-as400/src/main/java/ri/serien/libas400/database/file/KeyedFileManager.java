/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database.file;

import java.util.ArrayList;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400File;
import com.ibm.as400.access.AS400FileRecordDescription;
import com.ibm.as400.access.KeyedFile;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.Record;
import com.ibm.as400.access.RecordFormat;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Gère les accès direct au fichier DB2.
 */
public class KeyedFileManager {
  // Variables
  private AS400 systeme = null;
  private KeyedFile fichierDB = null;
  private RecordFormat recordFormat = null;
  private int nbrEnregistrement = 100;
  
  // Conserve le dernier message d'erreur émit et non lu
  private String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public KeyedFileManager(AS400 asysteme) {
    systeme = asysteme;
  }
  
  /**
   * Ouverture du fichier.
   */
  public boolean openFile(String bibliotheque, String fichier) {
    final QSYSObjectPathName fileName = new QSYSObjectPathName(bibliotheque, fichier, "*FILE", "MBR");
    fichierDB = new KeyedFile(systeme, fileName.getPath());
    try {
      systeme.connectService(AS400.RECORDACCESS);
      AS400FileRecordDescription recordDescription = new AS400FileRecordDescription(systeme, fileName.getPath());
      recordFormat = recordDescription.retrieveRecordFormat()[0];
      fichierDB.setRecordFormat(recordFormat);
      fichierDB.open(AS400File.READ_WRITE, nbrEnregistrement, AS400File.COMMIT_LOCK_LEVEL_NONE);
    }
    catch (Exception e) {
      msgErreur += "\nEchec de l'ouverture du fichier : " + e;
      return false;
    }
    
    return true;
  }
  
  /**
   * Fermeture du fichier.
   */
  public void closeFile() {
    try {
      if (fichierDB != null) {
        fichierDB.close();
      }
    }
    catch (Exception e) {
      // @todo Traiter correctement l'exception.
    }
  }
  
  /**
   * Récupérer la liste des records.
   */
  public ArrayList<Record> getListRecord(Object[] theKey, DataStructureRecord dsrecord) {
    if ((fichierDB == null) || (theKey == null)) {
      return null;
    }
    
    ArrayList<Record> liste = new ArrayList<Record>();
    try {
      Record data = fichierDB.read(theKey);
      while (data != null) {
        liste.add(dsrecord.setBytes(data.getContents()));
        data = fichierDB.readNextEqual(theKey);
      }
    }
    catch (Exception e) {
      msgErreur += "\nErreur durant la lecture du fichier " + e;
    }
    return liste;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
