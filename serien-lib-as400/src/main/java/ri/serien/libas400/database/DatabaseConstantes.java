/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database;

/**
 * Constantes pour la base de données.
 */
public class DatabaseConstantes {
  // Constantes
  public static final float VERSION = 9.70f;
  public static final int RELEASE = 01;
}
