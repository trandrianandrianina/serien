/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * Permet de lancer des requêtes sur un I5. Le jar db2_classes.jar est terouvé dans
 * \\172.31.1.249\qibm\ProdData\OS400\Java400\ext.
 * 
 * <p>A Voir: https://publib.boulder.ibm.com/iseries/v5r1/ic2924/index.htm?info/rzaha/jdbc.htm
 * https://publib.boulder.ibm.com/iseries/v5r1/ic2924/index.htm?info/rzaha/conjdbc.htm
 * https://publib.boulder.ibm.com/iseries/v5r1/ic2924/index.htm?info/rzaha/conjdbc.htm
 * http://www.sqlthing.com/resources/UsingQAQQINI.htm
 * http://www.ibm.com/developerworks/data/library/techarticle/dm-1001maxsqewithdds/
 * http://www.itjungle.com/mpo/mpo052203-story02.html
 * http://publib.boulder.ibm.com/iseries/v5r2/ic2924/index.htm?info/rzahh/javadoc/JDBCProperties.html
 */
public class DatabaseManager {
  // Constantes
  public static final int NODEBUG = 0;
  // Pour ces valeurs n'utiliser que des puissances de 2
  public static final int DEB_VERBOSE = 1;
  // c'est nécessaire pour retrouver ensuite les options
  public static final int DEB_NOCACHE = 2;
  
  private static final String REQ_VERBOSE = "UPDATE QTEMP.QAQQINI SET QQVAL='*YES' WHERE QQPARM='MESSAGES_DEBUG'";
  private static final String REQ_NOCACHE = "UPDATE QTEMP.QAQQINI SET QQVAL='*NONE' WHERE QQPARM='CACHE_RESULTS'";
  
  // Variables
  private boolean driverNative = true;
  // Indique si la connexion a été initialisé avec un context (Tomcat)
  private boolean initWithContext = false;
  private Connection connection = null;
  // Conserve le dernier message d'erreur émit et non lu
  private String msgErreur = "";
  // Stocke les propriétés (options) que l'on souhaite activer
  private Properties properties = new Properties();
  private ArrayList<String> qaqqiniRequetes = new ArrayList<String>();
  
  /**
   * Constructeur.
   */
  public DatabaseManager() {
  }
  
  /**
   * Constructeur.
   */
  public DatabaseManager(boolean drivernative) {
    driverNative = drivernative;
  }
  
  /**
   * Enregistrement du driver natif.
   */
  private boolean registerDriverNative() {
    try {
      // Class.forName("com.ibm.db2.jdbc.app.DB2Driver");
      DatabaseManager.class.getClassLoader().loadClass("com.ibm.db2.jdbc.app.DB2Driver");
      // Class.forName("com.ibm.db2.jcc.DB2Driver");
      // DriverManager.registerDriver(new com.ibm.db2.jcc.DB2Driver());
      // DriverManager.registerDriver(new com.ibm.db2.jdbc.app.DB2Driver());
    }
    catch (Exception e) {
      msgErreur += "Erreur: les drivers JDBC natif n'ont pas été chargé.\n" + e;
      driverNative = false;
      return false;
    }
    
    return true;
  }
  
  /**
   * Enregistrement du driver toolbox.
   */
  private boolean registerDriverToolbox() {
    // DriverManager.registerDriver(new com.ibm.as400.access.AS400JDBCDriver());
    try {
      // Class.forName("com.ibm.as400.access.AS400JDBCDriver");
      DriverManager.registerDriver(new com.ibm.as400.access.AS400JDBCDriver());
    }
    catch (SQLException e) {
      msgErreur += "Erreur: les drivers JDBC toolbox n'ont pas été chargé.\n" + e;
      return false;
    }
    return true;
  }
  
  /**
   * Connection au serveur.
   */
  public boolean connexion(String serveur, String user, String password) {
    if (driverNative) {
      try {
        if (registerDriverNative()) {
          if (serveur.toLowerCase().equals("localhost") || serveur.toLowerCase().equals("127.0.0.1")) {
            properties.setProperty("user", user);
            properties.setProperty("password", password);
            properties.setProperty("prompt", "false");
            // properties.setProperty("libraries", "QTEMP");
            connection = DriverManager.getConnection("jdbc:db2://*LOCAL", properties);
          }
          else {
            properties.setProperty("user", user);
            properties.setProperty("password", password);
            connection = DriverManager.getConnection("jdbc:db2://" + serveur, properties);
          }
        }
        else {
          driverNative = false;
        }
      }
      catch (Exception exc) {
        msgErreur += "\nEchec de l'initialisation des drivers natifs (on tente avec la Toolbox)";
        driverNative = false;
      }
    }
    
    if (!driverNative) {
      try {
        if (registerDriverToolbox()) {
          if (serveur.trim().equalsIgnoreCase("*local")) {
            serveur = "LOCALHOST";
          }
          properties.setProperty("user", user);
          properties.setProperty("password", password);
          
          // Permet de convertir en unicode si CCSID du fichier AS400 à 65535
          properties.setProperty("translate binary", "true");
          
          // properties.setProperty("ccsid", "297");
          
          // properties.setProperty("char.encoding", "ISO-8859-15");
          // properties.setProperty("char.encoding", "UTF-8");
          // properties.setProperty("characterEncoding", "UTF-8");
          // properties.setProperty("characterEncoding", "CP858");
          // properties.setProperty("characterEncoding", "WINDOWS-1252");
          
          connection = DriverManager.getConnection("jdbc:as400://" + serveur, properties);
          // connection = DriverManager.getConnection("jdbc:as400://" + serveur +
          // ";user="+user+";password="+password+";translate binary=true;ccsid=870;");
        }
        else {
          return false;
        }
      }
      catch (Exception exc) {
        msgErreur += "\nEchec de la connexion: " + exc.getMessage();
        return false;
      }
    }
    
    msgErreur += "\nConnexion avec les drivers natifs: " + driverNative;
    
    // Initialise un Qaqqini si nécessaire
    prepareQaqqini();
    
    return true;
  }
  
  /**
   * Connection au serveur.
   */
  public boolean connexion() {
    if (driverNative) {
      try {
        if (registerDriverNative()) {
          properties.setProperty("prompt", "false");
          // properties.setProperty("libraries", ",QTEMP,QGPL");
          // <-- on peut utiliser *LOCAL si on veut
          connection = DriverManager.getConnection("jdbc:db2://LOCALHOST", properties);
        }
      }
      catch (Exception e) {
        msgErreur += "\nEchec de l'initialisation des drivers natifs (on tente avec la Toolbox)";
        driverNative = false;
      }
    }
    
    if (!driverNative) {
      try {
        if (registerDriverToolbox()) {
          properties.setProperty("prompt", "false");
          // properties.setProperty("libraries", "QTEMP");
          // <-- ne pas utiliser *LOCAL car ça crashe
          connection = DriverManager.getConnection("jdbc:as400://LOCALHOST", properties);
        }
        else {
          return false;
        }
      }
      catch (Exception exc) {
        msgErreur += "\nEchec de la connexion: " + exc.getMessage();
        return false;
      }
    }
    
    msgErreur += "\nConnexion avec les drivers natifs: " + driverNative;
    
    // Initialise un Qaqqini si nécessaire
    prepareQaqqini();
    
    return true;
  }
  
  /**
   * Connexion au serveur depuis un ficher context.xml (config Tomcat). voir
   * http://www.itjungle.com/mgo/mgo062602-story01.html
   */
  public boolean connexion(String resourcename) {
    try {
      Context ctx = (Context) new InitialContext().lookup("java:comp/env");
      connection = ((DataSource) ctx.lookup(resourcename)).getConnection();
      initWithContext = true;
    }
    catch (Exception e) {
      msgErreur += "\nEchec de la connexion: " + e.getMessage();
      return false;
    }
    
    // Initialise un Qaqqini si nécessaire
    prepareQaqqini();
    
    return true;
  }
  
  /**
   * Retourne le pointeur sur la base de données.
   */
  public Connection getConnection() {
    return connection;
  }
  
  /**
   * Retourne le type de driver utilisé.
   */
  public boolean isDriverNative() {
    return driverNative;
  }
  
  /**
   * Déconnexion du serveur base de données Force permet de déconnecter lorsque l'on a initialisé la connexion avec un
   * context.
   */
  public void disconnect(boolean force) {
    if (!force && initWithContext) {
      return;
    }
    try {
      if (connection != null) {
        connection.close();
      }
      connection = null;
    }
    catch (Exception exc) {
      msgErreur += "\nEchec de la déconnexion: " + exc.getMessage();
    }
  }
  
  /**
   * Déconnexion du serveur base de données.
   */
  public void disconnect() {
    disconnect(false);
  }
  
  /**
   * Permet d'activer des options dans le QAQQINI.
   */
  public void setQaqqini(int qaqqini) {
    qaqqiniRequetes.clear();
    if (qaqqini <= 0) {
      return;
    }
    
    properties.setProperty("extended dynamic", "true");
    // On parcourt les options possibles (de la valeur la plus grande à la plus petite - Important
    // !!)
    while (qaqqini > 0) {
      if (qaqqini >= DEB_NOCACHE) {
        qaqqini -= DEB_NOCACHE;
        qaqqiniRequetes.add(REQ_NOCACHE);
      }
      else if (qaqqini >= DEB_VERBOSE) {
        qaqqini -= DEB_VERBOSE;
        qaqqiniRequetes.add(REQ_VERBOSE);
      }
    }
  }
  
  /**
   * Génère un Qaqqini dans la QTEMP si nécessaire.
   */
  public void prepareQaqqini() {
    if ((qaqqiniRequetes.size() == 0) || (connection == null)) {
      return;
    }
    
    try {
      Statement stmt = connection.createStatement();
      stmt.executeUpdate(
          "CALL QSYS.QCMDEXC('CRTDUPOBJ OBJ(QAQQINI) FROMLIB(QSYS) OBJTYPE(*FILE) TOLIB(QTEMP) DATA(*YES)',0000000075.00000)");
      for (String requete : qaqqiniRequetes) {
        stmt.executeUpdate(requete);
      }
      stmt.executeUpdate("CALL QSYS.QCMDEXC('CHGQRYA QRYOPTLIB(QTEMP)',0000000024.00000)");
      
      stmt.executeUpdate(
          "CALL QSYS.QCMDEXC('QSYS/DSPJOBLOG OUTPUT(*OUTFILE) OUTFILE(QTEMP/QAUGDBJOBN) OUTMBR(*FIRST *REPLACE)',0000000081.00000)");
      ResultSet rs =
          stmt.executeQuery(
              "SELECT SUBSTRING(QMHJOB,1,10) AS JOBNAME,SUBSTRING(QMHJOB,11,10) AS USERNAME,SUBSTRING(QMHJOB,21,6) AS JOBNUM FROM QTEMP.QAUGDBJOBN WHERE QMHMID = 'CPIAD02'");
      rs.next();
    }
    catch (SQLException e) {
    }
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
