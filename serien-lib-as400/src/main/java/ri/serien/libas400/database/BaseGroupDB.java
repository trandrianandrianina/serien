/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class BaseGroupDB {
  // Variables
  protected QueryManager queryManager = null;
  protected String curlib = null;
  protected GsonBuilder builderJSON = null;
  protected Gson gson = null;
  // Conserve le dernier message d'erreur émit et non lu
  protected String msgErreur = "";
  
  /**
   * Constructeur à utiliser sur l'AS400.
   */
  public BaseGroupDB(QueryManager pQueryManager) {
    setQuerymg(pQueryManager);
    if (pQueryManager != null) {
      setCurlib(pQueryManager.getLibrary());
    }
  }
  
  /**
   * Initialisation JSON.
   */
  public boolean initJSON() {
    if (builderJSON == null) {
      builderJSON = new GsonBuilder();
    }
    if (gson == null) {
      gson = builderJSON.create();
    }
    
    return (builderJSON != null && gson != null);
  }
  
  // -- Méthodes abstract
  
  public abstract void dispose();
  
  // -- Accesseurs
  
  /**
   * @return le querymg.
   */
  public QueryManager getQuerymg() {
    return queryManager;
  }
  
  /**
   * @param querymg Le querymg à définir.
   */
  public void setQuerymg(QueryManager querymg) {
    this.queryManager = querymg;
  }
  
  /**
   * @return le curlib.
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib Le curlib à définir.
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  public Gson getGson() {
    return gson;
  }
  
  public void setGson(Gson gson) {
    this.gson = gson;
  }
  
  /**
   * Construit le message d'erreur proprement.
   */
  protected void majError(String nvMessage) {
    if (msgErreur == null) {
      msgErreur = nvMessage;
    }
    else {
      msgErreur += "\n" + nvMessage;
    }
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = null;
    
    return chaine;
  }
  
}
