/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database.record;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import com.ibm.as400.access.FieldDescription;
import com.ibm.as400.access.Record;

import ri.serien.libcommun.outils.MessageErreurException;

public class ExtendRecord extends Record {
  
  private Record record;
  private FieldDescription[] descriptions = null;
  
  /**
   * @param record the record to set.
   */
  public void setRecord(Record record) {
    this.record = record;
    if (descriptions == null) {
      descriptions = record.getRecordFormat().getFieldDescriptions();
    }
  }
  
  public Record getRecord() {
    return record;
  }
  
  /**
   * Retourne le nombre de champs.
   */
  @Override
  public int getNumberOfFields() {
    return record.getNumberOfFields();
  }
  
  /**
   * Redéfinition afin d'intégrer le try/catch.
   */
  @Override
  public Object getField(int indice) {
    try {
      return record.getField(indice);
    }
    catch (UnsupportedEncodingException e) {
      return "";
    }
  }
  
  /**
   * Redéfinition afin d'intégrer le try/catch.
   */
  @Override
  public Object getField(String field) {
    try {
      return record.getField(field);
    }
    catch (UnsupportedEncodingException e) {
      return "";
    }
  }
  
  /**
   * Retourne le nom du champ à l'indice donné.
   */
  public String getFieldName(int indice) {
    return descriptions[indice].getFieldName();
  }
  
  /**
   * Retourner la valeur d'un champ décimal en précisant une valeur par défaut.
   */
  public BigDecimal getBigDecimalValue(String pNomChamp, BigDecimal pValeurParDefaut) {
    try {
      BigDecimal obj = (BigDecimal) getField(pNomChamp);
      if (obj == null) {
        return pValeurParDefaut;
      }
      return obj;
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Le champ " + pNomChamp + " ne contient pas un nombre décimal valide.");
    }
  }
  
}
