/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database;

import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.ibm.as400.access.Record;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import ri.serien.libas400.database.field.Field;
import ri.serien.libas400.database.record.DataStructureRecord;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.encodage.DeChiffreEbcdic;
import ri.serien.libcommun.outils.fichier.GestionFichierSQL;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Gère les opérations de base sur la base de données (création, suppression, lecture, ...).
 */
public class QueryManager {
  // Constantes
  private static final String PREFIXE_SQL = "[SQL] ";
  private static final String MARKER_CURLIB = "##CURLIB##.";
  public static final int ERREUR = -1;
  
  // Variables
  private Connection connection = null;
  private String librairie = "";
  protected String librairieEnvironnement = "";
  private String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public QueryManager(Connection pDatabase) {
    setDatabaseOperation(pDatabase);
  }
  
  /**
   * Initialise le pointeur vers la base de données.
   */
  public void setDatabaseOperation(Connection pConnection) {
    connection = pConnection;
  }
  
  /**
   * Retourne la library (mais ce n'est pas forcément la curlib de la session).
   */
  public String getLibrary() {
    return librairie;
  }
  
  /**
   * Initialise la library (mais ne change pas la curlib de la session).
   */
  public void setLibrary(String pLibrairie) {
    librairie = pLibrairie;
  }
  
  /**
   * Initialise la library (mais ne change pas la curlib de la session).
   */
  public void setLibrary(IdBibliotheque pLibrairie) {
    if (pLibrairie == null) {
      return;
    }
    librairie = pLibrairie.getNom();
  }
  
  public String getLibrairieEnvironnement() {
    return librairieEnvironnement;
  }
  
  public void setLibrairieEnvironnement(String librairieEnvironnement) {
    this.librairieEnvironnement = librairieEnvironnement;
  }
  
  /**
   * Retourner le message d'erreur.
   * La récupération du message est à usage unique, sa lecture l'efface.
   */
  public String getMsgError() {
    final String chaine = msgErreur;
    msgErreur = "";
    return chaine;
  }
  
  /**
   * Instancie une classe.
   */
  public Object newObject(String pClasse) {
    try {
      return Class.forName(pClasse, true, this.getClass().getClassLoader()).getConstructor().newInstance();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Impossible d'instancier un objet pour la classe : " + pClasse);
    }
  }
  
  /**
   * Déconnecte de la base.
   */
  public void deconnecter() {
    if (connection == null) {
      return;
    }
    try {
      connection.close();
    }
    catch (SQLException e) {
      throw new MessageErreurException(e, "Erreur lors de la fermeture de l'accès à la base de données");
    }
  }
  
  /**
   * Lancement d'une requête SELECT avec retour des données dans une arraylist.
   */
  public ArrayList<GenericRecord> select(String pRequete) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Exécuter la requête
      preparedStatement = connection.prepareStatement(pRequete);
      resultSet = preparedStatement.executeQuery();
      
      // Mettre le résultat dans un tableau
      final ArrayList<GenericRecord> listeRecord = new ArrayList<GenericRecord>();
      if (resultSet != null) {
        while (resultSet.next()) {
          GenericRecord record = new GenericRecord();
          for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
            record.setField(resultSet.getMetaData().getColumnName(i), resultSet.getObject(i), resultSet.getMetaData().getPrecision(i),
                resultSet.getMetaData().getScale(i));
          }
          listeRecord.add(record);
        }
        resultSet.close();
      }
      preparedStatement.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + listeRecord.size() + " lignes (" + duree + " ms)");
      return listeRecord;
    }
    catch (Exception e) {
      try {
        if (resultSet != null) {
          resultSet.close();
        }
        if (preparedStatement != null) {
          preparedStatement.close();
        }
      }
      catch (Exception e2) {
        // RAS
      }
      
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException(e, "Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
  }
  
  /**
   * Lancement d'une requête Select avec retour des données dans une arraylist.
   */
  public ArrayList<Record> selectAvecDataStructure(String requete, String datastructure) {
    return selectAvecDataStructure(requete, (DataStructureRecord) newObject(datastructure));
  }
  
  /**
   * Lancement d'une requête Select avec retour des données dans une arraylist.
   */
  public ArrayList<Record> selectAvecDataStructure(String pRequete, DataStructureRecord datastructure) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    PreparedStatement select = null;
    ResultSet rs = null;
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Exécuter la requête
      select = connection.prepareStatement(pRequete);
      rs = select.executeQuery();
      
      // Parcourir le résultat pour le mettre dans un tableau
      final ArrayList<Record> listeRecord = new ArrayList<Record>();
      if (rs != null) {
        byte[] content = new byte[datastructure.length];
        while (rs.next()) {
          int offset = 0;
          for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.arraycopy(rs.getBytes(i), 0, content, offset, rs.getBytes(i).length);
            offset += rs.getBytes(i).length;
          }
          listeRecord.add(datastructure.setBytes(content));
        }
        rs.close();
      }
      select.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + listeRecord.size() + " lignes (" + duree + " ms)");
      return listeRecord;
    }
    catch (SQLException exc) {
      try {
        if (rs != null) {
          rs.close();
        }
        if (select != null) {
          select.close();
        }
      }
      catch (Exception e) {
      }
      
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
  }
  
  /**
   * Lancement d'une requête Select avec retour des données dans une arraylist et des champs à déchiffrer.
   *
   * @param idxdeb indice de début du tableau (tableau java donc indice_RPG-1)
   * @param length longueur du buffer
   */
  public ArrayList<Record> selectWithDecryption(String pRequete, DataStructureRecord datastructure, int idxdeb, int length) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    PreparedStatement select = null;
    ResultSet rs = null;
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Exécuter la requête
      select = connection.prepareStatement(pRequete);
      rs = select.executeQuery();
      
      // Parcourir le résultat pour le mettre dans un tableau (avec décryptage)
      final ArrayList<Record> listeRecord = new ArrayList<Record>();
      if (rs != null) {
        byte[] content = new byte[datastructure.length];
        while (rs.next()) {
          int offset = 0;
          for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.arraycopy(rs.getBytes(i), 0, content, offset, rs.getBytes(i).length);
            offset += rs.getBytes(i).length;
          }
          
          // On déchiffre la partion des octets si besoin (on ne peut pas champ par champ car la cle
          // fonctionne pour un ensemble)
          if (idxdeb != -1) {
            byte[] tabconv = new byte[length];
            final DeChiffreEbcdic dechiffrage = new DeChiffreEbcdic();
            System.arraycopy(content, idxdeb, tabconv, 0, length);
            System.arraycopy(dechiffrage.getDecryptSecurity(tabconv), 0, content, idxdeb, length);
            tabconv = null;
          }
          listeRecord.add(datastructure.setBytes(content));
        }
        rs.close();
      }
      select.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + listeRecord.size() + " lignes (" + duree + " ms)");
      return listeRecord;
    }
    catch (SQLException exc) {
      try {
        if (rs != null) {
          rs.close();
        }
        if (select != null) {
          select.close();
        }
      }
      catch (Exception e) {
        // RAS
      }
      
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
  }
  
  /**
   * Retourne le nombre d'enregistrement suite à un Select.
   */
  public int nbrEnrgSelect(String pRequete) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    PreparedStatement select = null;
    ResultSet rs = null;
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Exécuter la requête
      select = connection.prepareStatement(pRequete);
      rs = select.executeQuery();
      
      // Parcourir le résultat pour le comptage
      int count = 0;
      if (rs != null) {
        while (rs.next()) {
          count++;
        }
        rs.close();
      }
      select.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + "Compter " + count + " lignes (" + duree + " ms)");
      return count;
    }
    catch (SQLException exc) {
      try {
        if (rs != null) {
          rs.close();
        }
        if (select != null) {
          select.close();
        }
      }
      catch (SQLException e) {
        // RAS
      }
      
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
  }
  
  /**
   * Retourne le premier enregistrement de la colonne voulu suite à un Select (col doit être supérieur à 0).
   */
  public String firstEnrgSelect(String pRequete, int pColonne) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    PreparedStatement select = null;
    ResultSet rs = null;
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Exécuter la requête
      select = connection.prepareStatement(pRequete);
      rs = select.executeQuery();
      
      // Parcourir le résultat pour lire la première ligne
      String valeur = null;
      if (rs.next()) {
        valeur = rs.getString(pColonne);
        rs.close();
      }
      select.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + "Lire la première ligne (" + duree + " ms)");
      return valeur;
    }
    catch (SQLException exc) {
      try {
        if (rs != null) {
          rs.close();
        }
        if (select != null) {
          select.close();
        }
      }
      catch (SQLException e) {
        // RAS
      }
      
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
  }
  
  /**
   * Retourne le premier enregistrement de la colonne voulu suite à un Select.
   * @
   */
  public String firstEnrgSelect(String pRequete, String pLibelleColonne) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    PreparedStatement select = null;
    ResultSet rs = null;
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Exécuter la requête
      select = connection.prepareStatement(pRequete);
      rs = select.executeQuery();
      
      // Parcourir le résultat pour lire la première ligne
      String valeur = null;
      if (rs != null) {
        rs.next();
        valeur = rs.getString(pLibelleColonne);
        rs.close();
      }
      select.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + "Lire la première ligne (" + duree + " ms)");
      return valeur;
    }
    catch (SQLException exc) {
      try {
        if (rs != null) {
          rs.close();
        }
        if (select != null) {
          select.close();
        }
      }
      catch (SQLException e) {
        // RAS
      }
      
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
  }
  
  /**
   * Lancement d'une requête insert, delete, update.
   */
  public int requete(String pRequete) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    Statement stmt = null;
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Exécuter la requête
      stmt = connection.createStatement();
      int resultat = stmt.executeUpdate(pRequete);
      stmt.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + resultat + " lignes (" + duree + " ms)");
      return resultat;
    }
    catch (SQLException exc) {
      if (stmt != null) {
        try {
          stmt.close();
        }
        catch (SQLException e) {
        }
      }
      
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
  }
  
  /**
   * Lancement d'une requête du type insert, update.
   *
   * @todo à améliorer afin qu'elle soit plus générique
   */
  public int requete(String pRequete, GenericRecord pGenericRecord) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    PreparedStatement stmt = null;
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Préparer la requête
      stmt = connection.prepareStatement(pRequete);
      for (int i = 1; i <= stmt.getParameterMetaData().getParameterCount(); i++) {
        if (stmt.getParameterMetaData().getParameterClassName(i).equals("java.sql.Timestamp")) {
          Field<?> champ = pGenericRecord.getFieldWithTypeAndNumber(stmt.getParameterMetaData().getParameterClassName(i), i);
          stmt.setTimestamp(i, (Timestamp) champ.getValue());
          
        }
        else {
          String clobLocator = "com.ibm.as400.access.AS400JDBCClobLocator";
          Field<?> champ = pGenericRecord.getFieldWithTypeAndNumber(clobLocator, i);
          if (champ != null) {
            if (champ.getSQLType().equals(clobLocator)) {
              Reader reader = new StringReader((String) champ.getValue());
              stmt.setCharacterStream(i, reader, champ.length);
            }
          }
        }
      }
      
      // Exécuter la requête
      int resultat = stmt.executeUpdate();
      stmt.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + resultat + " lignes (" + duree + " ms)");
      return resultat;
    }
    catch (SQLException exc) {
      if (stmt != null) {
        try {
          stmt.close();
        }
        catch (SQLException e) {
          // RAS
        }
      }
      
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
  }
  
  /**
   * Lancement d'une requête insert, delete, update Permet de récupérer la valeur de l'autoincément (ID).
   */
  public int insertWhoReturnId(String pRequete) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    int id = Constantes.ERREUR;
    Statement stmt = null;
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Exécuter la requête
      stmt = connection.createStatement();
      stmt.execute(pRequete);
      
      // Parcourir le résultat pour lire la première ligne
      // Attention si il y a un commit juste avant cette requête c'est mort
      ResultSet rs = stmt.executeQuery("SELECT IDENTITY_VAL_LOCAL() FROM SYSIBM.SYSDUMMY1");
      if (rs.next()) {
        // ID auto généré
        id = Integer.parseInt(rs.getString(1));
      }
      stmt.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + "Autoincrément " + id + " (" + duree + " ms)");
      return id;
    }
    catch (SQLException exc) {
      if (stmt != null) {
        try {
          stmt.close();
        }
        catch (SQLException e) {
          // RAS
        }
      }
      
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
  }
  
  /**
   * Retourne le résultat de la requête sous forme de Document XML.
   *
   * @tag Tag sans les < >.
   */
  public Document select2XML(String pRequete, String[] pChemin, String pTag, Document pDocument) {
    if (connection == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL si la base de données n'est pas définie.");
    }
    if (pRequete == null) {
      throw new MessageErreurException("Impossible d'exécuter une requête SQL vide.");
    }
    
    try {
      // Tracer la requête
      Trace.info(PREFIXE_SQL + pRequete);
      long debut = System.currentTimeMillis();
      
      // Exécuter la requête
      Statement stmt = connection.createStatement();
      ResultSet rs = stmt.executeQuery(pRequete);
      
      // Convertir le résultat en document XML
      pDocument = toDocument(rs, pChemin, pTag, pDocument);
      rs.close();
      stmt.close();
      
      // Tracer le résultat et la durée de la requête
      long duree = System.currentTimeMillis() - debut;
      Trace.info(PREFIXE_SQL + "select2XML (" + duree + " ms)");
    }
    catch (Exception e) {
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
    
    return pDocument;
  }
  
  /**
   * Exécuter les requêtes contenues dans un fichier SQL.
   */
  public void executeSQLFile(String pNomFichierSQL, String pCurlib) {
    if (pCurlib == null) {
      pCurlib = "";
    }
    else if (!pCurlib.trim().endsWith(".")) {
      pCurlib += '.';
    }
    
    // Charger les requêtes depuis un fichier
    GestionFichierSQL sql = new GestionFichierSQL(pNomFichierSQL);
    if (!sql.treatmentFile()) {
      throw new MessageErreurException(sql.getMsgErreur());
    }
    
    // Exécuter chaque requête
    ArrayList<String> listeRequetes = sql.getListQueries();
    for (String requete : listeRequetes) {
      requete = requete.replaceAll(MARKER_CURLIB, pCurlib);
      requete(requete);
    }
  }
  
  /**
   * Vérifie la présence d'une table dans la base de données courante.
   */
  public boolean isTableExiste(EnumTableBDD pEnumTableBDD) {
    String nomTable = Constantes.normerTexte(pEnumTableBDD.getNom());
    if (nomTable.isEmpty()) {
      Trace.erreur("La nom de la table est invalide.");
      return false;
    }
    librairie = Constantes.normerTexte(librairie);
    if (librairie.isEmpty()) {
      Trace.erreur("La nom de la base de données est invalide.");
      return false;
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select 1 from QSYS2.SYSTABLES where");
    requeteSql.ajouterConditionAnd("TABLE_SCHEMA", "=", librairie);
    requeteSql.ajouterConditionAnd("TABLE_NAME", "=", nomTable);
    
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return false;
    }
    return true;
  }
  
  // -- Méthodes privées
  
  private static Document toDocument(ResultSet rs, String[] path, String tag, Document doc)
      throws ParserConfigurationException, SQLException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    
    Element[] noeud = new Element[path.length + 1];
    int elt = 0;
    if (doc == null) {
      doc = builder.newDocument();
      noeud[0] = doc.createElement(path[0]);
      doc.appendChild(noeud[0]);
    }
    else {
      noeud[0] = doc.getDocumentElement();
    }
    
    // On construit l'arborescence
    for (elt = 1; elt < path.length; elt++) {
      noeud[elt] = doc.createElement(path[elt]);
      noeud[elt - 1].appendChild(noeud[elt]);
    }
    // On se positionne sur le dernier noeud
    elt--;
    
    ResultSetMetaData rsmd = rs.getMetaData();
    int colCount = rsmd.getColumnCount();
    
    while (rs.next()) {
      Element row = doc.createElement(tag);
      noeud[elt].appendChild(row);
      
      for (int i = 1; i <= colCount; i++) {
        String columnName = rsmd.getColumnName(i);
        Object value = rs.getObject(i);
        
        Element node = doc.createElement(columnName);
        if (value instanceof String) {
          String str = value.toString().trim();
          if (str.length() == 0) {
            node.appendChild(doc.createTextNode(str));
          }
          else {
            node.appendChild(doc.createCDATASection(str));
          }
          
        }
        else {
          node.appendChild(doc.createTextNode(value.toString().trim()));
        }
        
        row.appendChild(node);
      }
    }
    return doc;
  }
}
