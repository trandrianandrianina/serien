/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database.record;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.ibm.as400.access.AS400JDBCClob;
import com.ibm.as400.access.AS400JDBCClobLocator;
import com.ibm.as400.access.AS400Text;
import com.ibm.db2.jdbc.app.DB2Clob;
import com.ibm.db2.jdbc.app.DB2ClobLocator;

import ri.serien.libas400.database.field.Field;
import ri.serien.libas400.database.field.FieldAlpha;
import ri.serien.libas400.database.field.FieldDecimal;
import ri.serien.libas400.database.field.FieldTimestamp;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

/**
 * Description d'un enregistrement générique (utile pour les requêtes avec jointure).
 */
public class GenericRecord implements InterfaceRecord {
  // Pour marquer les valeurs des types du genre BLOB, CLOB
  private static final String TYPE_PARTICULIER = "##??##";
  
  // Variables
  private final LinkedHashMap<String, Field<?>> listFields = new LinkedHashMap<String, Field<?>>();
  private StringBuffer buffer = null;
  private int length = 0;
  private String[] requiredField = null;
  private String[] omittedField = null;
  private int nbrOmitField = 0;
  private JsonParser parser = new JsonParser();
  
  // Conserve le dernier message d'erreur émit et non lu
  protected String msgErreur = "";
  
  /**
   * Met à jour la valeur d'un champ.
   */
  public void setField(String namefield, Object value, int size, int decimal) {
    if (value instanceof BigDecimal) {
      setField(namefield, (BigDecimal) value, size, decimal);
    }
    else if (value instanceof Integer) {
      // setField(namefield, String.valueOf(value), size);
      setField(namefield, BigDecimal.valueOf(((Integer) value).intValue()), size, 0);
    }
    else if (value instanceof byte[]) {
      // Cas de lecture des fichiers plats
      AS400Text champs = new AS400Text(size);
      setField(namefield, (String) champs.toObject((byte[]) value), size);
    }
    else if (value instanceof Timestamp) {
      setField(namefield, (Timestamp) value);
    }
    else if (value instanceof AS400JDBCClobLocator) {
      // C'est un CLOB
      try {
        char[] contenu = new char[(int) ((AS400JDBCClobLocator) value).length()];
        ((AS400JDBCClobLocator) value).getCharacterStream().read(contenu);
        // byte[] tab = new byte[t];
        // ((AS400JDBCClobLocator)value).getAsciiStream().read(contenu);
        setField(namefield, new String(contenu), true);
      }
      catch (Exception e) {
        msgErreur += "Erreur sur AS400JDBCClobLocator: " + e;
      }
    }
    else if (value instanceof AS400JDBCClob) {
      // C'est un CLOB
      try {
        char[] contenu = new char[(int) ((AS400JDBCClob) value).length()];
        ((AS400JDBCClob) value).getCharacterStream().read(contenu);
        setField(namefield, new String(contenu), true);
      }
      catch (Exception e) {
        e.printStackTrace();
        msgErreur += "Erreur sur AS400JDBCClob: " + e;
      }
    }
    else if (value instanceof DB2ClobLocator) {
      // C'est un CLOB natif
      try {
        char[] contenu = new char[(int) ((DB2ClobLocator) value).length()];
        ((DB2ClobLocator) value).getCharacterStream().read(contenu);
        // byte[] tab = new byte[t];
        // ((AS400JDBCClobLocator)value).getAsciiStream().read(contenu);
        setField(namefield, new String(contenu), true);
      }
      catch (Exception e) {
        msgErreur += "Erreur sur DB2ClobLocator: " + e;
      }
    }
    else if (value instanceof DB2Clob) {
      // C'est un CLOB natif
      try {
        char[] contenu = new char[(int) ((DB2Clob) value).length()];
        ((DB2Clob) value).getCharacterStream().read(contenu);
        setField(namefield, new String(contenu), true);
      }
      catch (Exception e) {
        e.printStackTrace();
        msgErreur += "Erreur sur DB2Clob: " + e;
      }
    }
    else if (value instanceof Long) {
      setField(namefield, value);
    }
    else {
      setField(namefield, (String) value, size);
    }
  }
  
  /**
   * Met à jour la valeur d'un champ.
   */
  // @Override
  @Override
  public void setField(String namefield, Object value) {
    if (value instanceof BigDecimal) {
      setField(namefield, (BigDecimal) value);
    }
    else if (value instanceof Integer) {
      setField(namefield, (BigDecimal.valueOf(((Integer) value).intValue())));
    }
    else if (value instanceof Long) {
      setField(namefield, BigDecimal.valueOf(((Long) value).longValue()));
    }
    else if (value instanceof byte[]) {
      int size = ((byte[]) value).length;
      AS400Text champs = new AS400Text(size);
      setField(namefield, (String) champs.toObject((byte[]) value), size);
      // setField(namefield, new String((byte[])value));
    }
    else if (value instanceof Timestamp) {
      setField(namefield, (Timestamp) value);
    }
    else if (value instanceof AS400JDBCClobLocator) {
      // C'est un CLOB
      try {
        char[] contenu = new char[(int) ((AS400JDBCClobLocator) value).length()];
        ((AS400JDBCClobLocator) value).getCharacterStream().read(contenu);
        // byte[] tab = new byte[t];
        // ((AS400JDBCClobLocator)value).getAsciiStream().read(contenu);
        setField(namefield, new String(contenu), true);
      }
      catch (Exception e) {
        msgErreur += "Erreur sur AS400JDBCClobLocator: " + e;
      }
    }
    else if (value instanceof AS400JDBCClob) {
      // C'est un CLOB
      try {
        char[] contenu = new char[(int) ((AS400JDBCClob) value).length()];
        ((AS400JDBCClob) value).getCharacterStream().read(contenu);
        setField(namefield, new String(contenu), true);
      }
      catch (Exception e) {
        msgErreur += "Erreur sur AS400JDBCClob: " + e;
      }
    }
    else if (value instanceof DB2ClobLocator) {
      // C'est un CLOB natif
      try {
        char[] contenu = new char[(int) ((DB2ClobLocator) value).length()];
        ((DB2ClobLocator) value).getCharacterStream().read(contenu);
        // byte[] tab = new byte[t];
        // ((AS400JDBCClobLocator)value).getAsciiStream().read(contenu);
        setField(namefield, new String(contenu), true);
      }
      catch (Exception e) {
        msgErreur += "Erreur sur DB2ClobLocator : " + e;
      }
    }
    else if (value instanceof DB2Clob) {
      // C'est un CLOB natif
      try {
        char[] contenu = new char[(int) ((DB2Clob) value).length()];
        ((DB2Clob) value).getCharacterStream().read(contenu);
        setField(namefield, new String(contenu), true);
      }
      catch (Exception e) {
        msgErreur += "Erreur sur DB2Clob: " + e;
      }
    }
    else {
      setField(namefield, (String) value);
    }
  }
  
  /**
   * Met à jour la valeur d'un champ de type String.
   */
  @SuppressWarnings("unchecked")
  public void setField(String namefield, String valeur, int size) {
    if (namefield == null) {
      return;
    }
    Field<String> field = (Field<String>) listFields.get(namefield);
    if (field == null) {
      field = new FieldAlpha();
      listFields.put(namefield, field);
    }
    field.setValue(valeur);
    field.name = namefield;
    field.length = size;
    length += size;
  }
  
  /**
   * Met à jour la valeur d'un champ de type String.
   */
  @SuppressWarnings("unchecked")
  public void setField(String namefield, String valeur) {
    if (namefield == null) {
      return;
    }
    Field<String> field = (Field<String>) listFields.get(namefield);
    if (field == null) {
      field = new FieldAlpha();
      listFields.put(namefield, field);
    }
    field.setValue(valeur);
    field.name = namefield;
  }
  
  /**
   * Met à jour la valeur d'un champ de type BigDecimal.
   */
  @SuppressWarnings("unchecked")
  public void setField(String namefield, BigDecimal valeur, int size, int decimal) {
    if (namefield == null) {
      return;
    }
    Field<BigDecimal> field = (Field<BigDecimal>) listFields.get(namefield);
    if (field == null) {
      field = new FieldDecimal();
      listFields.put(namefield, field);
    }
    field.setValue(valeur);
    field.name = namefield;
    field.length = size;
    field.decimal = decimal;
    length += size;
  }
  
  /**
   * Met à jour la valeur d'un champ de type BigDecimal.
   */
  @SuppressWarnings("unchecked")
  public void setField(String namefield, BigDecimal valeur) {
    if (namefield == null) {
      return;
    }
    Field<BigDecimal> field = (Field<BigDecimal>) listFields.get(namefield);
    if (field == null) {
      field = new FieldDecimal();
      listFields.put(namefield, field);
      
    }
    field.setValue(valeur);
    field.name = namefield;
  }
  
  /**
   * Met à jour la valeur d'un champ (Spécial pour les clob).
   */
  @SuppressWarnings("unchecked")
  public void setField(String namefield, String valeur, boolean isClob) {
    if (namefield == null) {
      return;
    }
    Field<String> field = (Field<String>) listFields.get(namefield);
    if (field == null) {
      field = new FieldAlpha();
      listFields.put(namefield, field);
    }
    field.setValue(valeur);
    field.name = namefield;
    field.length = valeur != null ? valeur.length() : 0;
    if (isClob) {
      field.setSQLType("com.ibm.as400.access.AS400JDBCClobLocator");
      // field.setSQLType("com.ibm.as400.access.AS400JDBCClob");
    }
  }
  
  /**
   * Met à jour la valeur d'un champ.
   */
  @SuppressWarnings("unchecked")
  public void setField(String namefield, Timestamp valeur) {
    if (namefield == null) {
      return;
    }
    Field<Timestamp> field = (Field<Timestamp>) listFields.get(namefield);
    if (field == null) {
      field = new FieldTimestamp();
      listFields.put(namefield, field);
      
    }
    field.setValue(valeur);
    field.name = namefield;
  }
  
  /**
   * Retourne si le champ est présent dans l'enregistrement.
   */
  public boolean isPresentField(String namefield) {
    if (namefield == null) {
      return false;
    }
    
    namefield = namefield.toUpperCase();
    // On teste d'abord la présence du Field
    if (listFields.containsKey(namefield)) {
      // Puis sa valeur si ce n'est pas un champ Clob
      if (!(listFields.get(namefield).getSQLType().equals("com.ibm.as400.access.AS400JDBCClobLocator"))) {
        return listFields.get(namefield).getValue() != null;
      }
      else {
        return true;
      }
    }
    else {
      return false;
    }
  }
  
  /**
   * On supprime un champ du record.
   */
  public void removeField(String namefield) {
    if (namefield == null) {
      return;
    }
    
    // on teste d'abord la présence du Field
    if (listFields.containsKey(namefield.toUpperCase())) {
      listFields.remove(namefield.toUpperCase());
    }
  }
  
  /**
   * Retourne la valeur générique d'un champ.
   */
  @Override
  public Object getField(int indice) {
    if (indice < 0) {
      return null;
    }
    return listFields.get(getCle(indice)).getValue();
  }
  
  /**
   * Retourne la valeur générique d'un champ (attention le nom doit être en majuscule).
   */
  // @Override
  @Override
  public Object getField(String namefield) {
    if (namefield == null) {
      return null;
    }
    return listFields.get(namefield.trim().toUpperCase()).getValue();
  }
  
  /**
   * Retourner la valeur générique d'un champ.
   */
  private Object getValue(String pNomChamp) {
    if (pNomChamp == null) {
      return null;
    }
    else {
      pNomChamp = pNomChamp.trim().toUpperCase();
    }
    Field<?> field = listFields.get(pNomChamp);
    if (field == null) {
      return null;
    }
    return field.getValue();
  }
  
  /**
   * Retourner la valeur d'un champ entier.
   */
  public int getIntValue(String pNomChamp, int pValeurParDefaut) {
    Integer valeur = getIntegerValue(pNomChamp, pValeurParDefaut);
    if (valeur == null) {
      return pValeurParDefaut;
    }
    return valeur.intValue();
  }
  
  /**
   * Retourner la valeur d'un champ entier en précisant une valeur par défaut.
   * Les types binaires sont retournés comme des string (comprendre pourquoi plus tard).
   */
  public Integer getIntegerValue(String pNomChamp, int pValeurParDefaut) {
    try {
      Object valeurtempo = getValue(pNomChamp);
      if (valeurtempo == null) {
        return Integer.valueOf(pValeurParDefaut);
      }
      return Integer.valueOf(((BigDecimal) valeurtempo).intValueExact());
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Le champ " + pNomChamp + " ne contient pas un nombre entier valide.");
    }
  }
  
  /**
   * Retourner la valeur d'un champ entier avec 0 comme valeur par défaut.
   */
  public Integer getIntegerValue(String pNomChamp) {
    return getIntegerValue(pNomChamp, 0);
  }
  
  /**
   * Retourner la valeur d'un champ entier en précisant une valeur par défaut.
   * Les types binaires sont retournés comme des string (comprendre pourquoi plus tard).
   */
  public Long getLongValue(String pNomChamp, long pValeurParDefaut) {
    try {
      Object valeurtempo = getValue(pNomChamp);
      if (valeurtempo == null) {
        return Long.valueOf(pValeurParDefaut);
      }
      return Long.valueOf(((BigDecimal) valeurtempo).longValueExact());
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Le champ " + pNomChamp + " ne contient pas un nombre entier valide.");
    }
  }
  
  /**
   * Retourner la valeur d'un champ entier avec 0 comme valeur par défaut.
   */
  public Long getLongValue(String pNomChamp) {
    return getLongValue(pNomChamp, 0);
  }
  
  /**
   * Retourner la valeur d'un champ décimal.
   */
  public double getDoubleValue(String pNomChamp, double pValeurParDefaut) {
    BigDecimal obj = (BigDecimal) getValue(pNomChamp);
    if (obj == null) {
      return pValeurParDefaut;
    }
    return obj.doubleValue();
  }
  
  /**
   * Retourner la valeur d'un champ décimal en précisant une valeur par défaut.
   */
  public BigDecimal getBigDecimalValue(String pNomChamp, BigDecimal pValeurParDefaut) {
    try {
      BigDecimal obj = (BigDecimal) getValue(pNomChamp);
      if (obj == null) {
        return pValeurParDefaut;
      }
      return obj;
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Le champ " + pNomChamp + " ne contient pas un nombre décimal valide.");
    }
  }
  
  /**
   * Retourner la valeur d'un champ décimal avec 0 comme valeur par défaut.
   */
  public BigDecimal getBigDecimalValue(String pNomChamp) {
    return getBigDecimalValue(pNomChamp, BigDecimal.ZERO);
  }
  
  /**
   * Retourne la valeur d'un champ (attention le nom doit être en majuscule).
   */
  public String getString(String namefield) {
    if (namefield == null) {
      return null;
    }
    return ((FieldAlpha) listFields.get(namefield)).getValue();
  }
  
  /**
   * Retourner la valeur d'un champ textuel en précisant une valeur par défaut.
   */
  public String getStringValue(String pNomChamp, String pValeurParDefaut, boolean pTrim) {
    try {
      String obj = (String) getValue(pNomChamp);
      if (obj == null) {
        obj = pValeurParDefaut;
      }
      if (pTrim && obj != null) {
        return obj.trim();
      }
      return obj;
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Le champ " + pNomChamp + " ne contient pas un texte valide.");
    }
  }
  
  /**
   * Retourner la valeur d'un champ textuel de 1 caractère avec ' ' comme valeur par défaut.
   */
  public Character getCharacterValue(String pNomChamp) {
    return getCharacterValue(pNomChamp, ' ');
  }
  
  /**
   * Retourner la valeur d'un champ textuel de 1 caractère en précisant une valeur par défaut.
   */
  public Character getCharacterValue(String pNomChamp, Character pValeurParDefaut) {
    try {
      String obj = (String) getValue(pNomChamp);
      if (obj == null) {
        return pValeurParDefaut;
      }
      else if (obj.length() > 1) {
        throw new MessageErreurException("Le champ " + pNomChamp + " contient plus qu'un caractère.");
      }
      return obj.charAt(0);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Le champ " + pNomChamp + " ne contient pas un texte valide.");
    }
  }
  
  /**
   * Retourner la valeur d'un champ textuel avec "" comme valeur par défaut.
   * Les espaces sont automatiquement retirés en début et fin de texte.
   */
  public String getStringValue(String pNomChamp) {
    return getStringValue(pNomChamp, "", true);
  }
  
  /**
   * Retourner la valeur d'un champ date avec une valeur par défaut.
   * Concerne les dates Serie N, cad des champs numériques de 6 ou 7 caractères
   */
  public Date getDateValue(String pNomChamp, int pValeurPardefaut) {
    try {
      int date = getIntValue(pNomChamp, pValeurPardefaut);
      return ConvertDate.db2ToDate(date, null);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Le champ " + pNomChamp + " ne contient pas une date valide.");
    }
  }
  
  /**
   * Retourner la valeur d'un champ date avec 0 comme valeur valeur par défaut.
   * Concerne les dates Serie N, cad des champs numériques de 6 ou 7 caractères
   */
  public Date getDateValue(String pNomChamp) {
    return getDateValue(pNomChamp, 0);
  }
  
  /**
   * Retourner la valeur d'un champ date avec une valeur par défaut.
   * Concerne les dates pures timestamp.
   */
  public Timestamp getTimestampValue(String pNomChamp, Timestamp pValeurParDefaut) {
    try {
      Object valeurtempo = getValue(pNomChamp);
      if (valeurtempo == null) {
        return pValeurParDefaut;
      }
      return ((Timestamp) valeurtempo);
      
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Le champ " + pNomChamp + " ne contient pas une date valide.");
    }
  }
  
  /**
   * Retourner la valeur d'un champ date avec 0 comme valeur valeur par défaut.
   * Concerne les dates pures timestamp.
   */
  public Timestamp getTimestampValue(String pNomChamp) {
    return getTimestampValue(pNomChamp, null);
  }
  
  /**
   * Retourne la valeur générique d'un champ pour l'affichage avec possibilité de définir une valeur par défaut.
   */
  public Object getDspValueField(int indice, Object defaultvalue) {
    Object object = getField(indice);
    if (object == null) {
      if (defaultvalue == null) {
        return "";
      }
      return defaultvalue;
    }
    return object;
  }
  
  /**
   * Retourne la valeur générique d'un champ (attention le nom doit être en majuscule) pour l'affichage avec possibilité
   * de définir une valeur par défaut.
   */
  public Object getDspValueField(String namefield, Object defaultvalue) {
    Object object = getField(namefield);
    if (object == null) {
      if (defaultvalue == null) {
        return "";
      }
      return defaultvalue;
    }
    return object;
  }
  
  /**
   * Retourne la valeur d'un champ (attention le nom doit être en majuscule).
   */
  public BigDecimal getDecimal(String namefield) {
    if (namefield == null) {
      return null;
    }
    return ((FieldDecimal) listFields.get(namefield)).getValue();
  }
  
  /**
   * Retourne la valeur d'un champ (attention le nom doit être en majuscule).
   */
  public Timestamp getTimestamp(String namefield) {
    if (namefield == null) {
      return null;
    }
    return ((FieldTimestamp) listFields.get(namefield)).getValue();
  }
  
  /**
   * Retourne le champ (attention le nom doit être en majuscule).
   */
  public Field<?> getInfosField(String namefield) {
    if (namefield == null) {
      return null;
    }
    return listFields.get(namefield);
  }
  
  /**
   * Retourne le champ.
   */
  public Field<?> getInfosField(int indice) {
    if (indice < 0) {
      return null;
    }
    return listFields.get(getCle(indice));
  }
  
  /**
   * Retourne le nombre de champs.
   */
  public int getNumberOfFields() {
    return listFields.size();
  }
  
  /**
   * Retourne la longueur du record.
   */
  public int getRecordLength() {
    return length;
  }
  
  /**
   * Retourne une chaine avec les champs au bon offset.
   * TODO Voué à disparaitre.
   */
  @Override
  public StringBuffer getFlat() {
    if (buffer == null) {
      buffer = new StringBuffer(length);
    }
    else {
      // On initialise à blanc
      buffer.setLength(0);
    }
    
    String chaine = null;
    Set<String> cles = listFields.keySet();
    Iterator<String> it = cles.iterator();
    while (it.hasNext()) {
      String cle = it.next();
      Field<?> valeur = listFields.get(cle);
      
      chaine = valeur.toFormattedString(valeur.length, valeur.decimal);
      if (chaine != null) {
        buffer.append(chaine);
      }
    }
    return buffer;
  }
  
  /**
   * Retourne une chaine avec les champs séparés avec un caractère au choix.
   */
  public StringBuffer getFlatWithSeparator(String separator) {
    if (buffer == null) {
      buffer = new StringBuffer(length);
    }
    else {
      // On initialise à blanc
      buffer.setLength(0);
    }
    
    Set<String> cles = listFields.keySet();
    Iterator<String> it = cles.iterator();
    while (it.hasNext()) {
      String cle = it.next();
      Field<?> valeur = listFields.get(cle);
      if (valeur != null) {
        buffer.append(valeur.getTrimValue()).append(separator);
      }
      else {
        buffer.append(separator);
      }
    }
    buffer.deleteCharAt(buffer.lastIndexOf(separator));
    
    return buffer;
  }
  
  /**
   * Initialise l'entête avec un enregistrement plat.
   */
  // @Override
  @Override
  public boolean setFlat(String record) {
    if (record == null) {
      return false;
    }
    
    final int tailleRecord = record.length();
    int offsetDeb = 0;
    int offsetFin = 0;
    
    Set<String> cles = listFields.keySet();
    Iterator<String> it = cles.iterator();
    while (it.hasNext()) {
      String cle = it.next();
      Field<?> valeur = listFields.get(cle);
      
      offsetDeb = offsetFin;
      offsetFin += valeur.length;
      
      if (offsetFin <= tailleRecord) {
        setField(cle, record.substring(offsetDeb, offsetFin).trim());
      }
      else {
        setField(cle, record.substring(offsetDeb).trim());
      }
    }
    return true;
  }
  
  /**
   * Retourne une chaine du type Objet JSON (il y a à la fois les clefs et les valeurs).
   */
  // @Override
  @Override
  public JsonObject getJSON(boolean trim) {
    JsonObject objjson = new JsonObject();
    Set<String> cles = listFields.keySet();
    Iterator<String> it = cles.iterator();
    if (trim) {
      while (it.hasNext()) {
        String cle = it.next();
        Object value = listFields.get(cle).getTrimValue();
        if (value instanceof String) {
          objjson.add(cle, new JsonPrimitive((String) value));
        }
        else {
          objjson.add(cle, new JsonPrimitive((BigDecimal) value));
        }
      }
    }
    else {
      while (it.hasNext()) {
        String cle = it.next();
        Object value = listFields.get(cle).getValue();
        if (value instanceof String) {
          objjson.add(cle, new JsonPrimitive((String) value));
        }
        else {
          objjson.add(cle, new JsonPrimitive((BigDecimal) value));
        }
      }
    }
    
    return objjson;
  }
  
  /**
   * Retourne une chaine du type Array JSON (il y n'y a que les clefs).
   */
  public JsonArray getJSONKey() {
    JsonArray arrayjson = new JsonArray();
    Set<String> cles = listFields.keySet();
    Iterator<String> it = cles.iterator();
    while (it.hasNext()) {
      arrayjson.add(new JsonPrimitive(it.next()));
    }
    
    return arrayjson;
  }
  
  /**
   * Retourne une chaine du type Array JSON (il n'y a que les valeurs).
   */
  public JsonArray getJSONValue(boolean trim) {
    JsonArray arrayjson = new JsonArray();
    Set<String> cles = listFields.keySet();
    Iterator<String> it = cles.iterator();
    if (trim) {
      while (it.hasNext()) {
        String cle = it.next();
        Object value = listFields.get(cle).getTrimValue();
        if (value instanceof String) {
          arrayjson.add(new JsonPrimitive((String) value));
        }
        else {
          arrayjson.add(new JsonPrimitive((BigDecimal) value));
        }
        
      }
    }
    else {
      while (it.hasNext()) {
        String cle = it.next();
        Object value = listFields.get(cle).getValue();
        if (value instanceof String) {
          arrayjson.add(new JsonPrimitive((String) value));
        }
        else {
          arrayjson.add(new JsonPrimitive((BigDecimal) value));
        }
      }
    }
    
    return arrayjson;
  }
  
  /**
   * Initialise l'entête avec un enregistrement au format JSON.
   */
  @Override
  public boolean setJSON(String record) {
    if (record == null) {
      return false;
    }
    
    JsonObject objet;
    
    try {
      objet = (JsonObject) parser.parse(record);
      
      Set<String> cles = listFields.keySet();
      Iterator<String> it = cles.iterator();
      while (it.hasNext()) {
        String cle = it.next();
        // Object valeur = listFields.get(cle);
        setField(cle, objet.get(cle));
      }
    }
    catch (Exception e) {
      // e.printStackTrace();
      return false;
    }
    return true;
  }
  
  /**
   * Recherche le nom de la cle avec l'indice.
   */
  private String getCle(int indice) {
    int cur = 0;
    
    Set<String> cles = listFields.keySet();
    Iterator<String> it = cles.iterator();
    while (it.hasNext()) {
      String cle = it.next();
      if (cur == indice) {
        return cle;
      }
      cur++;
    }
    return null;
  }
  
  /**
   * Retourne un tableau avec les titres des records.
   */
  public String[] getArrayTitle() {
    return getArrayTitle(false);
  }
  
  /**
   * Retourne un tableau avec les titres des records.
   */
  public String[] getArrayTitle(boolean omitfields) {
    int cur = 0;
    
    String[] title = null;
    if (omitfields) {
      title = new String[listFields.size() - nbrOmitField];
    }
    else {
      title = new String[listFields.size()];
    }
    
    for (Map.Entry<String, Field<?>> entry : listFields.entrySet()) {
      Field<?> value = entry.getValue();
      if (omitfields && value.isOmitRequest()) {
        continue;
      }
      title[cur++] = entry.getKey();
    }
    
    return title;
  }
  
  /**
   * Retourne un tableau avec les données.
   */
  public Object[] getArrayValue(boolean trim) {
    return getArrayValue(false, trim, true);
  }
  
  /**
   * Retourne un tableau avec les données. Evite le remplacement de la valeur par ##??##.
   */
  public Object[] getArrayValue(boolean omitfields, boolean trim, boolean justvalue) {
    int cur = 0;
    Object[] data = null;
    
    if (omitfields) {
      data = new Object[listFields.size() - nbrOmitField];
    }
    else {
      data = new Object[listFields.size()];
    }
    
    if (trim) {
      for (Map.Entry<String, Field<?>> entry : listFields.entrySet()) {
        Field<?> value = entry.getValue();
        if (omitfields && value.isOmitRequest()) {
          continue;
        }
        if (!justvalue && value.getSQLType().equals("com.ibm.as400.access.AS400JDBCClobLocator")) {
          // à
          // corriger
          // via
          // un
          // Fieldclob
          // if (listFields.get(cle).getSQLType().equals("com.ibm.as400.access.AS400JDBCClob"))
          data[cur++] = TYPE_PARTICULIER;
        }
        else {
          data[cur++] = value.getTrimValue();
        }
      }
    }
    else {
      for (Map.Entry<String, Field<?>> entry : listFields.entrySet()) {
        Field<?> value = entry.getValue();
        if (omitfields && value.isOmitRequest()) {
          continue;
        }
        if (!justvalue && value.getSQLType().equals("com.ibm.as400.access.AS400JDBCClobLocator")) {
          // à
          // corriger
          // via
          // un
          // Fieldclob
          // if (listFields.get(cle).getSQLType().equals("com.ibm.as400.access.AS400JDBCClob"))
          data[cur++] = "?";
        }
        else {
          data[cur++] = value.getValue();
        }
      }
    }
    
    return data;
  }
  
  /**
   * Vérifie que tous les champs obligatoires soient présents dans le record et que leur valeur ne soit pas nulle.
   */
  public boolean verifRequiredField() {
    if (requiredField == null) {
      return true;
    }
    
    for (String chaine : requiredField) {
      if (!isPresentField(chaine)) {
        msgErreur += "Le champ " + chaine + " n'est pas présent ou sa valeur n'est pas correcte.\n";
        return false;
      }
    }
    return true;
  }
  
  /**
   * Retourne une requête SQL (UPDATE) à partir des données du record.
   */
  public String createSQLRequestUpdate(String table, String lib, String condition) {
    StringBuilder sb = new StringBuilder(512);
    
    // On vérifie d'abord les champs obligatoires
    if (!verifRequiredField()) {
      return null;
    }
    
    String[] title = getArrayTitle(true);
    Object[] value = getArrayValue(true, true, false);
    
    // On charge les cles et les valeurs
    sb.append("UPDATE ").append(lib).append('.').append(table).append(" SET ");
    for (int i = 0; i < title.length; i++) {
      if (value[i] == null) {
        continue;
      }
      
      sb.append(title[i]).append('=');
      Object valeur = value[i];
      if (valeur instanceof Timestamp) {
        sb.append("?, ");
      }
      else if (valeur instanceof String) {
        if (valeur.equals(TYPE_PARTICULIER)) {
          sb.append("?, ");
        }
        else {
          sb.append('\'').append(FieldAlpha.format4Request((String) valeur)).append("', ");
        }
      }
      else if (valeur instanceof BigDecimal) {
        sb.append(((BigDecimal) valeur).doubleValue()).append(", ");
      }
      else {
        sb.append(valeur).append(", ");
      }
    }
    sb.deleteCharAt(sb.length() - 2);
    
    // Construction de la requête
    if ((condition != null) && (!condition.trim().equals(""))) {
      if (condition.toUpperCase().contains("WHERE")) {
        sb.append(' ').append(condition);
      }
      else {
        sb.append(" WHERE ").append(condition);
      }
    }
    
    return sb.toString();
  }
  
  /**
   * Retourne une requête SQL (INSERT) à partir des données du record.
   */
  public String createSQLRequestInsert(String table, String lib) {
    StringBuilder sbk = new StringBuilder(512);
    StringBuilder sbv = new StringBuilder(512);
    
    // On vérifie d'abord les champs obligatoires
    if (!verifRequiredField()) {
      return null;
    }
    
    String[] title = getArrayTitle(true);
    Object[] value = getArrayValue(true, true, false);
    
    // On charge les cles et les valeurs
    for (int i = 0; i < title.length; i++) {
      if (value[i] == null) {
        continue;
      }
      
      sbk.append(title[i]).append(',');
      Object valeur = value[i];
      if (valeur instanceof Timestamp) {
        sbv.append("?, ");
      }
      else if (valeur instanceof String) {
        if (valeur.equals(TYPE_PARTICULIER)) {
          sbv.append("?, ");
        }
        else {
          sbv.append('\'').append(FieldAlpha.format4Request((String) valeur)).append("', ");
        }
      }
      else if (valeur instanceof BigDecimal) {
        sbv.append(((BigDecimal) valeur).doubleValue()).append(", ");
      }
      else {
        sbv.append(valeur).append(", ");
      }
    }
    sbk.deleteCharAt(sbk.length() - 1);
    sbv.deleteCharAt(sbv.length() - 2);
    
    // Construction de la requête
    sbk.insert(0, " (").insert(0, table).insert(0, '.').insert(0, lib).insert(0, "INSERT INTO ").append(") VALUES (").append(sbv)
        .append(')');
    
    return sbk.toString();
  }
  
  /**
   * Pour la compatibilité mais ne pas utiliser dans le futur, préférer createSQLRequestInsert.
   */
  public String getInsertSQL(String table, String lib) {
    return createSQLRequestInsert(table, lib);
  }
  
  /**
   * Retourne le champ avec le type voulu et son rang (qui commence à 1).
   */
  public Field<?> getFieldWithTypeAndNumber(String type, int num) {
    int rang = 1;
    for (Entry<String, Field<?>> entry : listFields.entrySet()) {
      // On recherche le bon type
      if (!entry.getValue().getSQLType().equals(type)) {
        continue;
      }
      // On vérifie que ce soit le bon rang
      if (rang == num) {
        return entry.getValue();
      }
      rang++;
    }
    return null;
  }
  
  /**
   * Permet d'initialiser un objet à partir des données du record.
   * 
   * <p>Les types gérés our l'objet: char, String, int, long, short, byte, BigDecimal. Pour le record: String,
   * BigDecimal. Note: il faudrait tenter d'utiliser les annotations à la place du nom des méthodes (solution plus
   * souple).
   */
  public boolean toObject(Object object) {
    if (object == null) {
      return false;
    }
    
    Class<?> classe = object.getClass();
    // Récupération des méthodes
    Method[] methods = classe.getMethods();
    
    // On va initialiser les variables de la classe object avec les accesseurs de cette même classe
    for (Method method : methods) {
      // On ne sélectionne que les accesseurs d'init
      if (!method.getName().startsWith("set")) {
        continue;
      }
      String variable = method.getName().substring(3).trim();
      // On recherche la variable dans le record
      if (!isPresentField(variable)) {
        continue;
      }
      ri.serien.libas400.database.field.Field<?> champ = getInfosField(variable);
      if (champ.getValue() == null) {
        continue;
      }
      
      // On alimente la variable de la classe avec la valeur du record
      try {
        Type[] tv = method.getGenericParameterTypes();
        // Le type du champ est un String
        String typeChamp = champ.getSQLType();
        // tv:" + tv[0]);
        if (typeChamp.equals("java.lang.String")) {
          String value = (String) champ.getValue();
          // if( tv[0].toString().lastIndexOf("char") != -1 )
          if (tv[0] == char.class) {
            method.invoke(object, value.charAt(0));
          }
          else if (tv[0] == String.class) {
            method.invoke(object, value.trim());
          }
          else if (tv[0] == Integer.class) {
            method.invoke(object, Integer.parseInt(value.trim()));
          }
          else if (tv[0] == int.class) {
            method.invoke(object, Integer.parseInt(value.trim()));
          }
          else if (tv[0] == double.class) {
            method.invoke(object, Double.parseDouble(value.trim()));
          }
          else if (tv[0] == byte.class) {
            method.invoke(object, Byte.parseByte(value.trim()));
          }
          else if (tv[0] == short.class) {
            method.invoke(object, Short.parseShort(value.trim()));
          }
        }
        else if (typeChamp.equals("java.math.BigDecimal")) {
          // Le type du champ est un BigDecimal
          BigDecimal value = (BigDecimal) champ.getValue();
          
          if (tv[0] == java.math.BigDecimal.class) {
            method.invoke(object, value);
          }
          else if (tv[0] == int.class) {
            method.invoke(object, value.intValue());
          }
          else if (tv[0] == double.class) {
            method.invoke(object, value.doubleValue());
          }
          else if (tv[0] == long.class) {
            method.invoke(object, value.longValue());
          }
          else if (tv[0] == byte.class) {
            method.invoke(object, value.byteValue());
          }
          else if (tv[0] == short.class) {
            method.invoke(object, value.shortValue());
          }
        }
        else if (typeChamp.equals("java.sql.Timestamp")) {
          Timestamp value = (Timestamp) champ.getValue();
          method.invoke(object, value);
        }
        else if (typeChamp.equals("com.ibm.as400.access.AS400JDBCClobLocator")) {
          method.invoke(object, champ.getValue());
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        return false;
      }
    }
    return true;
  }
  
  /**
   * Permet d'initialiser les données du record à partir d'un objet (Attention les variables de doivent être
   * obligatoirement en majuscules).
   * 
   * <p>Les types gérés pour l'objet: char, String, int, long, short, byte, BigDecimal Pour le record: String,
   * BigDecimal Note: il faudrait tenter d'utiliser les annotations à la place du nom des méthodes (solution plus
   * souple)
   */
  public boolean fromObject(Object object, boolean clearBefore) {
    if (object == null) {
      return false;
    }
    if (clearBefore) {
      listFields.clear();
    }
    
    String variable = null;
    Class<?> classe = object.getClass();
    // Récupération des méthodes
    Method[] methods = classe.getMethods();
    
    // On va initialiser les variables de la classe object avec les accesseurs de cette même classe
    for (Method method : methods) {
      // On ne sélectionne que les accesseurs de retour
      if (method.getName().startsWith("get")) {
        variable = method.getName().substring(3).trim();
      }
      else if (method.getName().startsWith("is")) {
        variable = method.getName().substring(2).trim();
      }
      else {
        continue;
      }
      // On vérifie qu'il s'agit bien d'un champ fichier (car obligatoirement en majuscule)
      if (!variable.toUpperCase().equals(variable)) {
        continue;
      }
      
      // On alimente les données du record avec la variable de la classe
      try {
        Type type = method.getGenericReturnType();
        Object obj = method.invoke(object);
        if (obj == null) {
          continue;
        }
        
        // Le type du champ est un char
        if (type == char.class) {
          listFields.put(variable, new FieldAlpha(obj.toString(), 1));
        }
        else if (type == String.class) {
          // Le type du champ est un String
          java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
          listFields.put(variable, new FieldAlpha(obj.toString(), size.getInt(object)));
        }
        else if (type == java.math.BigDecimal.class) {
          // Le type du champ est un BigDecimal
          java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
          java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
          listFields.put(variable, new FieldDecimal((BigDecimal) obj, size.getInt(object), decimal.getInt(object)));
        }
        else if (type == int.class) {
          // Le type du champ est un int
          java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
          java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
          listFields.put(variable, new FieldDecimal(new BigDecimal((Integer) obj), size.getInt(object), decimal.getInt(object)));
        }
        else if (type == long.class) {
          // Le type du champ est un long
          java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
          java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
          listFields.put(variable, new FieldDecimal(new BigDecimal((Long) obj), size.getInt(object), decimal.getInt(object)));
        }
        else if (type == byte.class) {
          // Le type du champ est un byte
          java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
          java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
          listFields.put(variable, new FieldDecimal(new BigDecimal((Byte) obj), size.getInt(object), decimal.getInt(object)));
        }
        else if (type == short.class) {
          // Le type du champ est un short
          java.lang.reflect.Field size = classe.getField("SIZE_" + variable);
          java.lang.reflect.Field decimal = classe.getField("DECIMAL_" + variable);
          listFields.put(variable, new FieldDecimal(new BigDecimal((Short) obj), size.getInt(object), decimal.getInt(object)));
        }
        else if (type == Timestamp.class) {
          // Timestamp
          classe.getField("SIZE_" + variable);
          listFields.put(variable, new FieldTimestamp((Timestamp) obj));
        }
        
      }
      catch (Exception e) {
        e.printStackTrace();
        return false;
      }
      
    }
    
    // Gestion de l'attribut Omit dans les champs
    if (omittedField != null) {
      for (String field : omittedField) {
        listFields.get(field).omitRequest = true;
      }
    }
    
    return true;
  }
  
  /**
   * Retourner la valeur du champ en base en fonction de son type déclaré dans la table.
   * @param pNom Le nom du champ de la table.
   * @param pType Le type du champ en table.
   * @return La valeur.
   */
  public Object getValeurChampMetier(String pNom, Class pType) {
    pNom = Constantes.normerTexte(pNom);
    // Contrôler le nom du champ
    if (pNom.isEmpty()) {
      return null;
    }
    // Contrôler le type du champ
    if (pType == null) {
      return getValue(pNom);
    }
    
    // Conversion du type de la valeur
    if (pType.equals(String.class)) {
      return getStringValue(pNom);
    }
    else if (pType.equals(Character.class)) {
      return getCharacterValue(pNom);
    }
    else if (pType.equals(Integer.class)) {
      return getIntegerValue(pNom);
    }
    else if (pType.equals(BigDecimal.class)) {
      return getBigDecimalValue(pNom);
    }
    
    // Si le type n'est pas référencé alors une erreur est déclenchée pour que le développeur enrichisse cette méthode
    throw new MessageErreurException(
        "Le type " + pType + " n'est pas référencé dans la méthode getValeurChampMetier() de GenericRecord.");
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le requiredField.
   */
  public String[] getRequiredField() {
    return requiredField;
  }
  
  /**
   * @param requiredField le requiredField à définir.
   */
  public void setRequiredField(String[] requiredField) {
    this.requiredField = requiredField;
  }
  
  /**
   * @return le omittedField.
   */
  public String[] getOmittedField() {
    return omittedField;
  }
  
  /**
   * @param omittedField le omittedField à définir.
   */
  public void setOmittedField(String[] omittedField) {
    this.omittedField = omittedField;
    nbrOmitField = omittedField != null ? omittedField.length : 0;
  }
  
  /**
   * Libère les ressources.
   */
  public void dispose() {
    listFields.clear();
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
