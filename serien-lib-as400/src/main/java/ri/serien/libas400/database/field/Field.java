/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.database.field;

/**
 * Description de base d'un champ.
 */
public abstract class Field<TypeT> {
  public String name = null;
  public int length = 0;
  public int decimal = 0;
  public String sqltype = null;
  public boolean omitRequest = false;
  
  public abstract void setValue(TypeT valeur);
  
  public abstract TypeT getValue();
  
  public abstract TypeT getTrimValue();
  
  @Override
  public abstract String toString();
  
  public abstract String toFormattedString(int asize, int adecimal);
  
  public abstract String toFormattedString();
  
  public abstract void setSize(int asize);
  
  public abstract void setSize(int asize, int adecimal);
  
  public abstract void setSQLType(String sqltype);
  
  public abstract String getSQLType();
  
  public void setOmitRequest(boolean omit) {
    this.omitRequest = omit;
  }
  
  public boolean isOmitRequest() {
    return omitRequest;
  }
  
}
