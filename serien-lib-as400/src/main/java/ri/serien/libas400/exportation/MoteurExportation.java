/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.exportation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ri.serien.libas400.dao.sql.exploitation.export.SqlExport;
import ri.serien.libas400.dao.sql.exploitation.parametres.SqlExploitationParametres;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.objetmetier.champmetier.ChampMetier;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.export.Export;
import ri.serien.libcommun.exploitation.export.IdExport;
import ri.serien.libcommun.exploitation.export.colonneexport.ColonneExport;
import ri.serien.libcommun.exploitation.export.colonneexport.ListeColonneExport;
import ri.serien.libcommun.exploitation.personnalisation.parametrent.ParametreNT;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.GestionFichierCSV;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Le but de cette classe est d'exporter des données à partir de l'identifiant d'un export.
 */
public class MoteurExportation {
  
  private QueryManager queryManager = null;
  private SqlExport sqlExport = null;
  private Export export = null;
  private boolean sortieCsv = false;
  
  private HashMap<Integer, ChampMetier> mapChampMetier = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pQueryManager Le queryManager.
   */
  public MoteurExportation(QueryManager pQueryManager) {
    if (pQueryManager == null) {
      throw new MessageErreurException("Le queryManager est invalide.");
    }
    queryManager = pQueryManager;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Lancer l'exportation.
   * @param pIdExport L'identifiant de l'export à traiter.
   * @param pSortieCsv true=Enregistrer le résultat au format CSV dans l'IFS, false=Aucun enregistrement du résultat.
   */
  public void exporter(IdExport pIdExport, boolean pSortieCsv) {
    // Charger les données nécessaires
    chargerDonnees(pIdExport);
    
    // Exporter
    exporter(export, pSortieCsv);
  }
  
  /**
   * Lancer l'exportation.
   * @param pExport L'export à traiter.
   * @param pSortieCsv true=Enregistrer le résultat au format CSV dans l'IFS, false=Aucun enregistrement du résultat.
   */
  public void exporter(Export pExport, boolean pSortieCsv) {
    sortieCsv = pSortieCsv;
    
    // Construction de la requête
    RequeteSql requeteSql = construireRequete();
    
    // Exécution de la requête
    /*ArrayList<GenericRecord> listeRecord =*/ executerRequete(requeteSql);
    
    // Récupération de la liste des valeurs des champs métiers
    // List<List<Object>> listeAExporter = creerListeValeurAExporter(listeRecord);
    
    // Formatage des données en sortie
    if (pSortieCsv) {
      // formaterEnCvs(listeAExporter);
    }
    
    // Mise à jour de la date de dernier export
    miseAJourDateExport();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Charger les données nécessaires depuis la base.
   * @param pIdExport L'identifiant de l'export.
   */
  private void chargerDonnees(IdExport pIdExport) {
    // Contrôler l'identifiant
    if (pIdExport == null) {
      throw new MessageErreurException("L'identifiant de l'export est invalide.");
    }
    
    // Chargement de l'export
    sqlExport = new SqlExport(queryManager);
    export = sqlExport.chargerExport(pIdExport);
  }
  
  /**
   * Construire la requête à partir des données de l'export.
   */
  private RequeteSql construireRequete() {
    // Contrôler l'export
    if (export == null) {
      throw new MessageErreurException("L'export est invalide, il n'a pas pu être chargé.");
    }
    // Contrôler les colonnes à exporter de l'export
    ListeColonneExport listeColonneExport = export.getListeColonneExport();
    if (listeColonneExport == null || listeColonneExport.isEmpty()) {
      throw new MessageErreurException("Il n'y a aucun champ à exporter pour l'export " + export.getId() + '.');
    }
    // Contrôler l'objet metier concerné
    if (export.getIdObjetMetier() == null) {
      throw new MessageErreurException("L'identifiant de l'objet métier à exporter est invalide.");
    }
    
    // Récupération du nom de la table base de données
    EnumTableBDD tableBdd = null;
    if (listeColonneExport.get(0).getChampMetier() != null && listeColonneExport.get(0).getChampMetier().getChampBDD() != null) {
      tableBdd = listeColonneExport.get(0).getChampMetier().getChampBDD().getTableBDD();
    }
    if (tableBdd == null) {
      throw new MessageErreurException("Le nom de la table de la base de données est invalide.");
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select ");
    // Insertion des champs des colonnes à exporter
    mapChampMetier = new HashMap<Integer, ChampMetier>();
    int i = 0;
    for (ColonneExport colonneExport : listeColonneExport) {
      // Contrôler que le champ métier existe
      if (colonneExport.getChampMetier() == null) {
        Trace.erreur("La colonne d'export " + colonneExport.getId() + " n'a pas de champ métier associé.");
        continue;
      }
      mapChampMetier.put(i++, colonneExport.getChampMetier());
      requeteSql.ajouter(colonneExport.getChampMetier().getChampBDD().getNom() + ", ");
    }
    requeteSql.supprimerMot(",", true);
    requeteSql.ajouter(" from " + queryManager.getLibrary() + '.' + tableBdd.getNom());
    
    // A SUPPRIMER
    requeteSql.ajouter(" where CLCLI < 10");
    
    return requeteSql;
  }
  
  /**
   * Exécuter la requête.
   * @param pRequeteSql La requête.
   */
  private ArrayList<GenericRecord> executerRequete(RequeteSql pRequeteSql) {
    if (pRequeteSql == null) {
      throw new MessageErreurException("La requête est invalide.");
    }
    if (mapChampMetier == null || mapChampMetier.isEmpty()) {
      throw new MessageErreurException("La liste des champs métiers est invalide.");
    }
    
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(pRequeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    return listeRecord;
  }
  
  /**
   * Créer une liste de valeurs pour une exportation.
   * @param pListeRecord
   */
  /*
  private List<List<Object>> creerListeValeurAExporter(ArrayList<GenericRecord> pListeRecord) {
    if (pListeRecord == null || pListeRecord.isEmpty()) {
      return null;
    }
    
    List<List<Object>> listeExportation = new ArrayList<List<Object>>();
    for (GenericRecord record : pListeRecord) {
      List<Object> ligne = completerValeurChampMetier(record);
      // Stockage de chaque ligne dans l'exportation
      if (ligne != null && !ligne.isEmpty()) {
        listeExportation.add(ligne);
      }
    }
    
    return listeExportation;
  }*/
  
  /**
   * Créer une liste de classes métiers alimentées résultant de la requête.
   * @param pListeRecord
   */
  /*
  private List<InterfaceClasseMetier> creerListeClasseMetier(ArrayList<GenericRecord> pListeRecord) {
    if (pListeRecord == null || pListeRecord.isEmpty()) {
      return null;
    }
    
    List<InterfaceClasseMetier> listeClasseMetier = new ArrayList<InterfaceClasseMetier>();
    for (GenericRecord record : pListeRecord) {
      InterfaceClasseMetier classeMetier = completerValeurClasseMetier(record);
      // Créer une hashmap d'identifiant champ metier et de sa valeur associée
      if (!listeClasseMetier.contains(classeMetier)) {
        listeClasseMetier.add(classeMetier);
      }
    }
    
    return listeClasseMetier;
  }*/
  
  /**
   * Récupérer la valeur du champ métier et la stocker dans une liste.
   * @param pRecord Le record.
   * @return La liste des valeur.
   */
  /*
  private List<Object> completerValeurChampMetier(GenericRecord pRecord) {
    if (pRecord == null) {
      return null;
    }
    
    // Création de la liste des valeurs
    // Map<IdChampMetier, Object> mapValeurChampMetier = new HashMap<IdChampMetier, Object>();
    List<Object> listeValeur = new ArrayList<Object>();
    
    // Parcours des champs du record
    for (int i = 0; i < pRecord.getNumberOfFields(); i++) {
      // Récupération du champ du record en cours
      ChampMetier champMetier = mapChampMetier.get(i);
      
      // Récupération de la valeur associée au champ métier
      Object valeur = pRecord.getValeurChampMetier(champMetier.getChampBDD().getNom(), champMetier.getChampBDD().getType());
      listeValeur.add(valeur);
    }
    
    return listeValeur;
  }*/
  
  /**
   * Créer et compléter une instance de classe métier à partir du record.
   * @param pRecord Le record.
   * @return
   */
  /*
  private InterfaceClasseMetier completerValeurClasseMetier(GenericRecord pRecord) {
    if (pRecord == null) {
      return null;
    }
    
    // Création de la map qui stocke la valeur associée à un champ métier
    Map<IdChampMetier, Object> mapValeurChampMetier = new HashMap<IdChampMetier, Object>();
    
    // Parcours des champs du record
    IdChampMetier idChampMetier = null;
    for (int i = 0; i < pRecord.getNumberOfFields(); i++) {
      // Récupération du champ du record en cours
      ChampMetier champMetier = mapChampMetier.get(i);
      
      // Récupération de la valeur associée au champ métier
      Object valeur = pRecord.getValeurChampMetier(champMetier.getChampBDD().getNom(), champMetier.getChampBDD().getType());
      mapValeurChampMetier.put(champMetier.getId(), valeur);
      
      if (idChampMetier == null) {
        idChampMetier = champMetier.getId();
      }
    }
    
    // contrôler l'identifiant du champ métier
    if (idChampMetier == null) {
      return null;
    }
    
    // Créer une instance de l'objet concerné
    ObjetMetier objetMetier = ManagerObjetMetier.getInstance().getObjetMetier(idChampMetier.getIdObjetMetier());
    Object classeMetier = objetMetier.instancierClasseMetier(mapValeurChampMetier);
    
    // Mettre à jour les variables correspondantes aux champs issus de la requête
    ((InterfaceClasseMetier) classeMetier).completerListeChampMetier(mapValeurChampMetier);
    
    return (InterfaceClasseMetier) classeMetier;
  }*/
  
  private void formaterEnCvs(List<List<Object>> pListeAExporter) {
    if (pListeAExporter == null || pListeAExporter.isEmpty()) {
      return;
    }
    
    // Récupération du dossier racine dans l'IFS (Paramètre NT)
    String dossierRacineIfs = retournerDossierRacineIfs();
    
    // Construction du nom du fichier à exporter
    String nomFichierExporte = export.construireCheminCompletExport(dossierRacineIfs, GestionFichierCSV.FILTER_EXT_CSV);
    GestionFichierCSV fichierCsv = new GestionFichierCSV(nomFichierExporte);
    
    // Parcours de la liste
    StringBuffer ligneCsv = new StringBuffer();
    for (List<Object> ligne : pListeAExporter) {
      // Création d'une ligne au format csv
      ligneCsv.setLength(0);
      for (Object cellule : ligne) {
        ligneCsv.append(cellule).append(';');
      }
      fichierCsv.add(ligneCsv.toString());
    }
    
    // Enregistrement du fichier
    fichierCsv.ecritureFichier();
  }
  
  /**
   * Sauver l'export.
   */
  private void miseAJourDateExport() {
    // Mise à jour de la date du dernier export
    export.setDateDernierExport(new Date());
    
    // Sauvegarde en base
    sqlExport.sauverExport(export);
  }
  
  /**
   * Lire en base le dossier racine dans l'IFS.
   * @return Le dossier racine.
   */
  private String retournerDossierRacineIfs() {
    String dossierRacine = null;
    
    // Chargement du paramètre NT
    SqlExploitationParametres operation = new SqlExploitationParametres(queryManager);
    ParametreNT parametreNT = operation.lireParametreNT();
    if (parametreNT != null) {
      dossierRacine = parametreNT.getRacine();
    }
    
    return dossierRacine;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
}
