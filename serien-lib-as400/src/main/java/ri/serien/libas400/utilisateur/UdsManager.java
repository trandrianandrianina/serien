/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.utilisateur;

import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.dataarea.LdaManager;

/**
 * Gestion des informations contenues dans la *LDA.
 */
public class UdsManager extends LdaManager {
  // Constantes
  public static final int OFFSET_LOCASP = 1024;
  public static final int LONG_LOCASP = 1;
  
  // Variables
  private char locasp = ' ';
  
  /**
   * Constructeur.
   */
  public UdsManager(SystemeManager psysteme) {
    super(psysteme);
  }
  
  /**
   * Découpe la dataara pour alimenter les zones.
   */
  public boolean parseUds() {
    // Lecture de la *LDA
    String uds = getData();
    if (uds == null) {
      return false;
    }
    
    // Découpage
    if (uds.length() >= OFFSET_LOCASP) {
      setLocasp(uds.charAt(OFFSET_LOCASP - 1));
    }
    
    return true;
  }
  
  /**
   * the locasp to set.
   */
  public void setLocasp(char locasp) {
    this.locasp = locasp;
  }
  
  public char getLocasp() {
    return locasp;
  }
  
}
