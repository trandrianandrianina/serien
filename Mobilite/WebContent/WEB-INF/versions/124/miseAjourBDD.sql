
INSERT INTO vue VALUES (6,"images/bonslivraison.png","clients?vue=6", "<span class='accueil'><span class='superf'>Clients: </span>bons de livraison</span><span class='metier'><span class='superf'>Vue </span>bons de livraison</span>",10, "Bons de livraison", 1, NULL, 1);;;

INSERT INTO vue_catalogue VALUES (1,10);;;

INSERT INTO zone VALUES (92,1,0,'BLACTIFS','Nombre de BL', 6, 0, "(SELECT COUNT(E1NUM) FROM <code>CODEBIBLI</code>.PGVMCLIM c LEFT OUTER JOIN <code>CODEBIBLI</code>.PGVMEBCM ON c.CLCLI = E1CLLP AND c.CLLIV = E1CLLS AND c.CLETB = E1ETB WHERE c.CLCLI = cl.CLCLI AND c.CLLIV = cl.CLLIV AND E1COD = 'E' AND E1TOP = 0 AND E1ETA = 4) ",'Nombre de BL actifs',-1,0,1,'','');;;

INSERT INTO zones_vues VALUES(10,7,1,2,1,0,1,2);;;
INSERT INTO zones_vues VALUES(10,8,2,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(10,9,3,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(10,10,4,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(10,11,5,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(10,14,6,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(10,4,-1,0,0,0,0,2);;;
INSERT INTO zones_vues VALUES(10,5,-1,0,2,0,0,2);;;
INSERT INTO zones_vues VALUES(10,6,-1,0,0,0,0,2);;;
INSERT INTO zones_vues VALUES(10,92,-1,0,3,0,0,2);;;
INSERT INTO zones_vues VALUES (10,76,0,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (10,77,1,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (10,78,-1,0,0,0,0,1);;;
INSERT INTO zones_vues VALUES (10,79,-1,0,0,0,0,1);;;
INSERT INTO zones_vues VALUES (10,84,2,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (10,85,3,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (10,86,4,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (10,88,5,1,0,0,0,1);;;

INSERT INTO vues_fichier VALUES (10,10," DIGITS(cv.E1NUM)||DIGITS(cv.E1SUF) = (SELECT DIGITS(E1NUM)||(E1SUF) FROM <code>CODEBIBLI</code>.PGVMEBCM cv WHERE E1COD = 'E' AND E1TOP = 0 AND E1ETB = cl.CLETB AND E1CLLP = cl.CLCLI AND E1CLLS = cl.CLLIV AND E1ETA = 4 ORDER BY E1NUM DESC, E1SUF DESC FETCH FIRST 1 ROWS ONLY)",'');;;         

UPDATE vues_fichier SET fic_from = " DIGITS(cv.E1NUM)||DIGITS(cv.E1SUF) = (SELECT DIGITS(E1NUM)||(E1SUF) FROM <code>CODEBIBLI</code>.PGVMEBCM cv WHERE E1COD = 'E' AND E1TOP = 0 AND E1ETB = cl.CLETB AND E1CLLP = cl.CLCLI AND E1CLLS = cl.CLLIV AND E1ETA < 4 ORDER BY E1NUM DESC, E1SUF DESC FETCH FIRST 1 ROWS ONLY)" WHERE vue_id = 8;;;

UPDATE zone SET zone_calcul = "(SELECT COUNT(E1NUM) FROM <code>CODEBIBLI</code>.PGVMCLIM c LEFT OUTER JOIN <code>CODEBIBLI</code>.PGVMEBCM ON c.CLCLI = E1CLLP AND c.CLLIV = E1CLLS AND c.CLETB = E1ETB WHERE c.CLCLI = cl.CLCLI AND c.CLLIV = cl.CLLIV AND E1COD = 'E' AND E1TOP = 0 AND E1ETA < 4) " WHERE zone_id = 83;;;

DELETE from zones_vues WHERE zone_id = 77 AND vue_id = 10;;;

UPDATE zones_vues SET ordre_zones = ordre_zones - 1 WHERE vue_id = 10 AND bloc_zones = 1 AND ordre_zones > 0;;;

UPDATE zones_vues SET zone_id = 89 WHERE zone_id = 76 AND vue_id = 10;;;

INSERT INTO vue VALUES (-1,'','','',11,'Liste de bons de livraison',4,NULL,'');;;

INSERT INTO zones_vues VALUES (11,76,-1,0,1,0,0,10);;;
INSERT INTO zones_vues VALUES (11,77,-1,0,2,0,0,10);;;
INSERT INTO zones_vues VALUES (11,84,-1,0,3,0,0,10);;;
INSERT INTO zones_vues VALUES (11,86,-1,0,4,0,0,10);;;
INSERT INTO zones_vues VALUES (11,88,-1,0,5,0,0,10);;;

UPDATE vue SET module = 11 WHERE vue_id = 10;;;

DELETE from zones_vues WHERE zone_id = 77 AND vue_id = 11;;;

UPDATE zones_vues SET liste_zones = liste_zones - 1 WHERE vue_id = 11 AND liste_zones > 1;;;

UPDATE zones_vues SET zone_id = 89 WHERE zone_id = 76 AND vue_id = 11;;;

INSERT INTO zone VALUES (93,10,0,'E1EXP','Date de livraison',7,0,'','Date de livraison',0,0,4,'','');;;

UPDATE zones_vues SET zone_id = 93 WHERE zone_id = 86 AND vue_id = 10;;;

UPDATE zones_vues SET zone_id = 93 WHERE zone_id = 86 AND vue_id = 11;;;

INSERT INTO zone VALUES (94,10,0,'E1NFA','Numéro de facture',7,0,'','Numéro de facture',0,0,1,'','');;;

INSERT INTO zone VALUES (95,1,0,'NBFACTURES','Nombre factures actives', 6, 0, "(SELECT COUNT(E1NUM) FROM <code>CODEBIBLI</code>.PGVMCLIM c LEFT OUTER JOIN <code>CODEBIBLI</code>.PGVMEBCM ON c.CLCLI = E1CLLP AND c.CLLIV = E1CLLS AND c.CLETB = E1ETB WHERE c.CLCLI = cl.CLCLI AND c.CLLIV = cl.CLLIV AND E1COD = 'E' AND E1TOP = 0 AND E1ETA > 4) ",'Nombre factures actives',-1,0,1,'','');;;

INSERT INTO vue VALUES (7,"images/factures.png","clients?vue=7", "<span class='accueil'><span class='superf'>Clients: les </span>factures</span><span class='metier'><span class='superf'>Vue </span>factures </span>",12, "Factures client", 1, '', 1);;;

INSERT INTO vue_catalogue VALUES (1,12);;;

INSERT INTO zones_vues VALUES(12,7,1,2,1,0,1,2);;;
INSERT INTO zones_vues VALUES(12,8,2,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(12,9,3,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(12,10,4,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(12,11,5,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(12,14,6,2,0,0,0,2);;;
INSERT INTO zones_vues VALUES(12,4,-1,0,0,0,0,2);;;
INSERT INTO zones_vues VALUES(12,5,-1,0,2,0,0,2);;;
INSERT INTO zones_vues VALUES(12,6,-1,0,0,0,0,2);;;
INSERT INTO zones_vues VALUES(12,95,-1,0,3,0,0,2);;;
INSERT INTO zones_vues VALUES (12,89,0,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (12,94,1,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (12,78,-1,0,0,0,0,1);;;
INSERT INTO zones_vues VALUES (12,79,-1,0,0,0,0,1);;;
INSERT INTO zones_vues VALUES (12,84,2,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (12,85,3,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (12,93,4,1,0,0,0,1);;;
INSERT INTO zones_vues VALUES (12,88,5,1,0,0,0,1);;;

INSERT INTO vues_fichier VALUES (12,10," DIGITS(cv.E1NUM)||DIGITS(cv.E1SUF) = (SELECT DIGITS(E1NUM)||(E1SUF) FROM <code>CODEBIBLI</code>.PGVMEBCM cv WHERE E1COD = 'E' AND E1TOP = 0 AND E1ETB = cl.CLETB AND E1CLLP = cl.CLCLI AND E1CLLS = cl.CLLIV AND E1ETA > 4 ORDER BY E1NUM DESC, E1SUF DESC FETCH FIRST 1 ROWS ONLY)",'');;;         

INSERT INTO vue VALUES (-1,'','','',13,'Liste de factures',4,NULL,'');;;

INSERT INTO zones_vues VALUES (13,89,-1,0,1,0,0,10);;;
INSERT INTO zones_vues VALUES (13,84,-1,0,2,0,0,10);;;
INSERT INTO zones_vues VALUES (13,93,-1,0,3,0,0,10);;;
INSERT INTO zones_vues VALUES (13,88,-1,0,4,0,0,10);;;

UPDATE vue SET module = 13 WHERE vue_id = 12;;;

UPDATE zone SET zone_type = 0 WHERE zone_id = 94;;;


UPDATE constante SET const_valeur = '124' WHERE const_cle = 'VERSION';;;