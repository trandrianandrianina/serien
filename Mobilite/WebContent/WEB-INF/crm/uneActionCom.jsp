<%@page import="ri.serien.libas400.dao.gvm.programs.actioncommerciale.M_ActionCommerciale"%>
<%@page import="ri.serien.libas400.dao.gvm.programs.client.M_Client"%>
<%@page import="ri.serien.libas400.dao.exp.programs.contact.M_EvenementContact"%>
<%@page import="ri.serien.libas400.dao.exp.programs.contact.M_Contact"%>
<%@page import="ri.serien.libcommun.outils.DateHeure"%>
<%@page import="ri.serien.mobilite.crm.GestionFormulaire"%>
<% 
	M_ActionCommerciale uneAction	= (M_ActionCommerciale)request.getAttribute("uneAction");
	GestionFormulaire	formulaire	= (GestionFormulaire)request.getAttribute("formulaire");
%>
<jsp:include page="head.jsp" />

<section class='secContenu' >
	<form action='ActionsCom' method='post' id='formActionCom' name='formActionCom' >

	<h1>
		<span class='titreH1'>Action commerciale<%=(uneAction.getACID()==0?" (Nouvelle)":"") %></span>
		<span class='optionsListe'>
			<input type='hidden' name='idaction' value='<%=uneAction.getACID() %>'/>
			<% if( formulaire.isConsultation() ){ %>
				<%-- <a href='ActionsCom?idaction=<%=uneAction.getACID() %>&action2do=<%=GestionFormulaire.MODIFICATION %>' id='action2Do'>Modifier</a> --%>
			<%} else {%>
				<input type='hidden' name='action2do' value='<%=GestionFormulaire.VALIDATION %>'/>
			<% } %>
		</span>
	</h1>
	
	<%-- <!-- BLOCS DE MESSAGES INFORMATION -->
	<% if( formulaire.isModification() ){ %>
		<p class='messageInfo'>Action en modification</p>
	<%} else if(formulaire.isValidation()) {%>
		<p class='messageInfo'>Action valid�e</p>
	<%} else if(formulaire.isValidation()) {%>
		<p class='messageInfo'>Action valid�e</p>
	<%}%> --%>

		<% if(uneAction == null ) {%>
			<p class='messageInfo'>Pas d'action commerciale pour ce client</p>
		<%} else {%>
		<div id = 'presentationAction'>
			<div class='miniBlocs'>
				<label class='lbAction'>Objet</label>
				<div class='responsive' >
					<select name="ac_objetaction" class='mesSelectsCRM' <%=formulaire.getState4OGfxbject() %> >
						<%	for(int i=0; i< uneAction.getListLabelObjectAction().length; i++ ) {%>
							<option value="<%=i %>" <%=(uneAction.getListCodeObjectAction()[i].equals(uneAction.getACOBJ()))?"selected=\"selected\"":"" %> >
								<%=uneAction.getListLabelObjectAction()[i] %>
							</option>
						<% } %>
					</select>
				</div>
			</div>
			<div class='miniBlocs'>
				<label class='lbAction'>Etat</label>
				<div class='responsive' id='pousseSuivant'>
					<select name="ac_etataction" class='mesSelectsCRM' <%=formulaire.getState4OGfxbject() %> >
						<% for(int i=0; i< M_EvenementContact.getListState().length; i++ ) {%>
							<option value="<%=i %>" <%=(i==uneAction.getEvenementContact().getETETA())?"selected=\"selected\"":"" %> >
							<%=M_EvenementContact.getListState()[i] %></option>
						<% } %>
					</select>
				</div>
				
				<label class='lbAction'>Priorit�</label>
				<div class='responsive'>
					<select name="ac_prioriteaction"  class='mesSelectsCRM' <%=formulaire.getState4OGfxbject() %> >
						<% for(int i=0; i< M_EvenementContact.getListPriority().length; i++ ) {%>
							<option value="<%=i %>" <%=(i==uneAction.getEvenementContact().getETCODP())?"selected=\"selected\"":"" %> >
							<%=M_EvenementContact.getListPriority()[i] %></option>
						<% } %>
					</select>
				</div>
			</div>
			<div  class='miniBlocs'>
				<label class='lbComplets'>Observations</label>
				<textarea name="ac_obsaction" maxlength="<%=M_EvenementContact.SIZE_ETOBS %>" id='observationsCRM' <%=formulaire.getState4OGfxbject() %> ><%=uneAction.getEvenementContact().getETOBS() %></textarea>
			</div>
			<div  class='miniBlocs'>
				<label  class='lbAction'>Attribu&eacute; par</label>
					<%
						M_Contact[] executant = uneAction.getFiltreContacts("CR");
						String valeur = "";
						if( (executant != null) && (executant.length > 0) ) {
							valeur = executant[0].getREPAC(); 
						}
					%>
				<div class='responsive'>
					<input type="text" name="par" maxlength="100" value="<%=valeur %>" class='inputResponsive' <%=formulaire.getState4OGfxbject() %> />
				</div>
			</div>
		</div>
		<h1 class='tetiereListes' id='accordeon4'>
			<span class='titreTetiere'></span>
			<span class='apportListe'></span>
			<div class='triTetieres'>
				<a class='teteListe' id='triClePrincip'>Contact(s)</a>
			</div>
			
			<% if( !formulaire.isConsultation() ){  %>
				<a id='lienContacts' href='#' onClick="saveActionForm('Contact?action2do=10&idclient=<%=uneAction.getEvenementContact().getETIDT() %>&clsuf=<%=uneAction.getEvenementContact().getETSUFT() %>');" >+ Associer des contacts +</a>
			<%} %>
		</h1>
			<%	M_Contact[] cible = uneAction.getFiltreContacts("CI");
				if( (cible == null) || (cible.length == 0) ){%>
					<p class="messageListes">Pas de contact d�fini pour cette action
					<% if( !formulaire.isConsultation() ){  %>
			<!-- <div class='miniBlocs'> -->
				<%-- <a href='#' onClick="saveActionForm('Contact?action2do=10&idclient=<%=uneAction.getEvenementContact().getETIDT() %>&clsuf=<%=uneAction.getEvenementContact().getETSUFT() %>');" id='lienContacts' >Associer des contacts</a> --%>
			<!-- </div> -->
			<%} %>
					
					</p>
				<%} else {
				for( M_Contact contact: cible){ %>
					<div class='listesClassiques'>
						<a href='Contact?action2do=<%=GestionFormulaire.CONSULTATION %>&idcontact=<%=contact.getRENUM() %>' class='detailsListe'>
							<div class="clePrincipCRM2"><%=contact.getRECIV() + " " +contact.getREPAC() %></div>
							<div class="valeurPrincipCRM2"><%=contact.getRETEL() %></div>
						</a>
					</div>
				<%}} %>

		<h1 class="tetiereListes" id='accordeon2'>
			<span class="titreTetiere"></span>
			<span class="apportListe"></span>
			<div class="triTetieres">
				<a class="teteListe" id='triClePrincip'>Gestion du temps</a>
			</div>
		</h1>

		<div id='tempsAction'>
			<div  class='miniBlocs'>
				<div class='responsive2'>
					<label class='lbAction2'>Date cr�ation</label>
					<input type="text" name="ac_dateCreation" maxlength="10" value="<%=DateHeure.getConvertJourHeure(13, 12, ""+uneAction.getEvenementContact().getETDCR(), "") %>" class='dateCRM' <%=formulaire.getState4OGfxbject() %>/>
				</div>
				<label class='lbDates'> Heure</label>
				<input type="text" name="ac_heureCreation" maxlength="5" value="<%=DateHeure.getConvertJourHeure(14, 15, ""+uneAction.getEvenementContact().getETHCR(), "") %>" class='heureCRM' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div  class='miniBlocs'>
				<div class='responsive2'>
					<label class='lbAction2'>Date limite</label>
					<input type='text' name='ac_dateLimite' maxlength="10" value="<%=DateHeure.getConvertJourHeure(13, 12, ""+uneAction.getEvenementContact().getETDCL(), "") %>" class='dateCRM' <%=formulaire.getState4OGfxbject() %>/>
				</div>
				<label class='lbDates'>Heure</label>
				<input type='text' name='ac_heureLimite' maxlength="5" value="<%=DateHeure.getConvertJourHeure(14, 15, ""+uneAction.getEvenementContact().getETHCL(), "") %>" class='heureCRM' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div  class='miniBlocs'>
				<div class='responsive2'>
					<label class='lbAction2'>Date r�alisa.</label>
					<input type='text' name='ac_dateRealisation' maxlength="10" value="<%=DateHeure.getConvertJourHeure(13, 12, ""+uneAction.getEvenementContact().getETDRL(), "") %>" class='dateCRM' <%=formulaire.getState4OGfxbject() %>/>
				</div>
				<label class='lbDates'>Heure</label>
				<input type='text' name='ac_heureRealisation' maxlength="5" value="<%=DateHeure.getConvertJourHeure(14, 15, ""+uneAction.getEvenementContact().getETHRL(), "") %>" class='heureCRM' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div  class='miniBlocs'>
				<div class='responsive2'>
					<label class='lbAction2'>Rappel</label>
					<input type='text' name='ac_dateRappel' maxlength="10" value="<%=DateHeure.getConvertJourHeure(13, 12, ""+uneAction.getEvenementContact().getETDRP(), "") %>" class='dateCRM' <%=formulaire.getState4OGfxbject() %>/>
				</div>
				<label class='lbDates'>Heure</label>
				<input type='text' name='ac_heureRappel' maxlength="5" value="<%=DateHeure.getConvertJourHeure(14, 15, ""+uneAction.getEvenementContact().getETHRP(), "") %>" class='heureCRM' <%=formulaire.getState4OGfxbject() %>/>
			</div>
			<div  class='miniBlocs'>
				<label  class='lbAction'>Temps pass&eacute;</label>
				<%if( !formulaire.isConsultation() ){%>
					<input type="text" name="ac_tpspasseaction" maxlength="<%=M_EvenementContact.SIZE_ETTOTP %>" value="<%=uneAction.getEvenementContact().getETTOTP() %>" class='dateCRM' <%=formulaire.getState4OGfxbject() %>/>
				<% } %>
				<label  class='lbAction'><%=uneAction.getEvenementContact().getLabelETTOTP() %></label>
				
			</div>
		</div>
			
		<h1 class='tetiereListes' id='accordeon3'>
			<span class='titreTetiere'></span>
			<span class='apportListe'></span>
			<div class='triTetieres'>
				<a class='teteListe' id='triClePrincip'>Soci&eacute;t&eacute;</a>
			</div>
		</h1>
		<div id='contactAction'>
			<div class='miniBlocs'>
				<label class='lbAction'>Num&eacute;ro soci&eacute;t&eacute;</label>
				<input type="text" name="numerosoc" value="<%=uneAction.getClient().getCLCLI()+" / "+uneAction.getClient().getCLLIV() %>" class="inputsComplementaires" disabled />
			</div>
			<div class='miniBlocs'>
				<label class='lbAction'>Soci&eacute;t&eacute;</label>
				<input type="text" name="societe" value="<%=uneAction.getClient().getCLNOM() %>" class="inputsComplementaires" disabled />
			</div>
			<div class='miniBlocs'>
				<label class='lbAction'>Adresse</label>
				<input type="text" name="adresse" value="<%=uneAction.getClient().getCLRUE()+" "+uneAction.getClient().getCLVIL() %>" class="inputsComplementaires" disabled />
			</div>
			<div class='miniBlocs'>
				<label class='lbAction'>T&eacute;l&eacute;phone</label>
				<input type="text" name="telephone" value="<%=uneAction.getClient().getCLTEL() %>" class="inputsComplementaires" disabled />
			</div>
		</div>

		<%-- <div id="divBtModif">
			<% if( formulaire.isCreation() ){ %>
				<a href='ActionsCom?idaction=<%=uneAction.getACID() %>&action2do=<%=GestionFormulaire.ANNULATION %>' id='btModification'>Annuler l'action</a>
				<%} else { %>
				<a href='ActionsCom?idaction=<%=uneAction.getACID() %>&action2do=<%=GestionFormulaire.SUPPRESSION %>' id='btModification'>Supprimer l'action</a>
			<%}%>
		</div> --%>
		
		<%} %>
		</form>
		
		<div id='navFlottanteCRM'>
		<% if( formulaire.isCreation()){ %>
			<a class='lienNavFlottanteCRM' href='ActionsCom?client=<%=uneAction.getClient().getCLCLI()%>&suffixe=<%=uneAction.getClient().getCLLIV()%>' id='urlRetour'><img src='images/urlretour.png'/></a>
			<a class='lienNavFlottanteCRM' href='#' onClick='soumettreUnFormulaire("formActionCom")' id='validerAction'><img src='images/validerAction.png'/></a>
		<%} else if(formulaire.isModification()){ %>
			<a class='lienNavFlottanteCRM' href='ActionsCom?idaction=<%=uneAction.getACID() %>' id='urlretour3'><img src='images/urlretour.png'/></a>
			<a class='lienNavFlottanteCRM' href='#' onClick='soumettreUnFormulaire("formActionCom")' id='validerAction3'><img src='images/validerAction.png'/></a>
			<a class='lienNavFlottanteCRM' href='ActionsCom?idaction=<%=uneAction.getACID() %>&action2do=<%=GestionFormulaire.SUPPRESSION %>' id='supprimerAction'><img src='images/supprimerAction.png'/></a>
		<%} else if(formulaire.isConsultation()){ %>
			<a class='lienNavFlottanteCRM' href='ActionsCom?client=<%=uneAction.getClient().getCLCLI()%>&suffixe=<%=uneAction.getClient().getCLLIV()%>' id='urlretour3'><img src='images/urlretour.png'/></a>
			<a class='lienNavFlottanteCRM' href='ActionsCom?idaction=<%=uneAction.getACID() %>&action2do=<%=GestionFormulaire.MODIFICATION %>' id='modifierAction'><img src='images/modifierAction.png'/></a>
			<a class='lienNavFlottanteCRM' href='#' onClick = "confirmerUnLien('ActionsCom?idaction=<%=uneAction.getACID() %>&action2do=<%=GestionFormulaire.SUPPRESSION %>','Voulez vous supprimer cette action ?');" id='supprimerAction'><img src='images/supprimerAction.png'/></a>
			<%-- <a class='lienNavFlottanteCRM' href='ActionsCom?idaction=<%=uneAction.getACID() %>&action2do=<%=GestionFormulaire.SUPPRESSION %>' id='supprimerAction'><img src='images/supprimerAction.png'/></a> --%>
			
		<%}%>
		</div>
		
</section>

<script>
	loadActionForm();
	readUrlRetour();
</script>


<jsp:include page='footer.jsp' />




    		