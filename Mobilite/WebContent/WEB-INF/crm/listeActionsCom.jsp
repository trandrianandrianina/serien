<%@page import="java.util.ArrayList"%>
<%@page import="ri.serien.libas400.dao.gvm.programs.actioncommerciale.M_ActionCommerciale"%>
<%@page import="ri.serien.libas400.dao.gvm.programs.client.M_Client"%>

<% 
	M_ActionCommerciale[]	listeActions	= (M_ActionCommerciale[])request.getAttribute("listeActions");
	M_Client				client			= (M_Client)request.getAttribute("mclient");
%>

<jsp:include page="head.jsp" />

<section class='secContenu' id='listeMetier'>
	<h1 class='tetiereListes'>
		<span class='titreTetiere'>actions <%=client.getCLNOM() %></span><span class='apportListe'><%=listeActions.length %> r�sultats</span>
		<%-- <span class='apportListe2'><a href='#' id='urlretour'>Back</a></span>
		<span class='titreTetiere'><%=client.getCLNOM() %></span><a href='ActionsCom?idaction=0' class='apportListe' id='nouvelleAction'>+</a> --%>
		<div class='triTetieres'>
			<a href='#' class='teteListe' id='triClePrincip'>Objet</a>
			<a href='#' class='teteListe' id='trivaleurPrincip'>Priorite</a>
			<!-- Autres ent�tes de champs � r�cup�rer et afficher ici -->
		</div>
	</h1>
	
		<%-- <% if(request.getAttribute("action2Do")!=null) 
		{
			if(request.getAttribute("action2Do").equals("4"))
			{ %>
				<p class="messageListes">Action supprim�e</p>
			<%}
			else if(request.getAttribute("action2Do").equals("3"))
			{ %>
			<p class="messageListes">Nouvelle action valid�e</p>
			<%}
		} %> --%>
	
		<% if( listeActions.length > 0 ) {%>
			<% for(M_ActionCommerciale elt : listeActions ) {%>	
			<div class='listesClassiques'>
				<a href='ActionsCom?idaction=<%=elt.getACID() %>' class='detailsListe'>
					<div class="clePrincipCRM"><%=elt.getLabelACOBJ() %></div>
					<div class="valeurPrincipCRM"><%=elt.getEvenementContact().getLabelETCODP() %></div>
					<!-- Autres champs � r�cup�rer et afficher ici -->
				</a>
			</div>
			<%} %>
		<%} else {%>
			<p class="messageListes">Pas de d'actions pour ces crit�res</p>
		<%} %>
		
		<div id='navFlottanteCRM'>
			<a class='lienNavFlottanteCRM' href='ActionsCom' id='urlRetour'><img src='images/urlretour.png'/></a>
			<a class='lienNavFlottanteCRM' href='ActionsCom?idaction=0' id='nouvAction'><img src='images/nouvAction.png'/></a>
		</div>
	
</section>

<script>
	cleanActionFromSessionStorage();
	readUrlRetour();
</script>

<jsp:include page="footer.jsp" />