<%@page import="java.util.ArrayList"%>
<%@page import="ri.serien.libas400.database.record.GenericRecord"%>
<% 
	String tailleListe = "0";
	ArrayList<GenericRecord> listeClients = (ArrayList<GenericRecord>)request.getAttribute("listeClients"); 
	if(listeClients!=null)
		tailleListe = String.valueOf(listeClients.size()); 
%>

<jsp:include page="head.jsp" />

<section class='secContenu' id='listeMetier'>
	<h1 class='tetiereListes'>
		<span class='titreTetiere'>Liste de clients</span><span class='apportListe'><%=tailleListe %> r�sultats</span>
		<div class='triTetieres'>
			<a href='#' class='teteListe' id='triClePrincip'>Nom ou raison sociale</a>
			<a href='#' class='teteListe' id='trivaleurPrincip'>Ville</a>
			<!-- Autres ent�tes de champs � r�cup�rer et afficher ici -->
		</div>
	</h1>
	<% if(listeClients != null && listeClients.size()>0) {%>
		<% for(int i = 0; i < listeClients.size(); i++) {%>	
		<div class='listesClassiques'>
			<a href='itineraire?numClientSaisi=<%=listeClients.get(i).getField("CLCLI") %>&sufClientSaisi=<%=listeClients.get(i).getField("CLLIV") %>' class='detailsListe'>
				<div class="clePrincipCRM"><%=listeClients.get(i).getField("CLNOM") %></div>
				<div class="cleSecondCRM"><%=listeClients.get(i).getField("CLCLI") %>/<%=listeClients.get(i).getField("CLLIV") %></div>
				<div class="listeFavVide"></div>
				<div class="valeurPrincipCRM"><%=listeClients.get(i).getField("CLVIL") %></div>
				<!-- Autres champs � r�cup�rer et afficher ici -->
			</a>
		</div>
		<%} %>
	<%} else {%>
		<p class="messageListes">Pas de clients pour ces crit�res</p>
	<%} %>
</section>

<jsp:include page="footer.jsp" />