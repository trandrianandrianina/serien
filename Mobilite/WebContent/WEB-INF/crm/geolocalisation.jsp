<%@page import="com.ibm.as400.util.commtrace.EchoRequest"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ri.serien.libas400.database.record.GenericRecord"%>
<%
	String listeAdresses = (String)request.getAttribute("listeAdresses");
	ArrayList<GenericRecord> clientsProx = (ArrayList<GenericRecord>)request.getAttribute("clientsProx");

	
/*	if(clientsProx != null)
	{
		int j = 0;
		for(int i = 0; i < clientsProx.size(); i ++)
		{
			if(j>0)
				listeAdresses +=",";
			else listeAdresses = "";
			listeAdresses += "['" + clientsProx.get(i).getField("CLRUE").toString().trim() + "," + clientsProx.get(i).getField("CLVIL").toString().trim() + ", FRANCE', '" + clientsProx.get(i).getField("CLNOM").toString().trim() + "']";
			j++;
		}
	}
*/
%>
<jsp:include page="head.jsp" />





<section class='secContenu'>
	<h1>
		<span class='titreH1'>Carte</span> <a class='optionsListe' href='#'></a>
	</h1>
	<script>
		// LES SCRIPTS SUIVANTS PERMETTENT :

		// - D'AFFICHER LA CARTE DANS LA PAGE
		// - DE PLACER LA POSITION ACTUELLE DE L'UTILISATEUR (DISQUE ROUGE AVEC TROU AU MILIEU)
		// - DE PLACER LA POSITION DE TOUTE ADRESSE VOULUE
		// - DE METTRE EN VALEUR LES ADRESSES DANS UN RAYON D'ACTION FIXE
		// - DE TRACER LE RAYON D'ACTION AUTOUR DE LA POSITION DE L'UTILISATEUR


		
		// carte google map
		var map;
		// g�ocodeur
		var geocoder;
		// position actuelle de l'utilisateur
		var pos;
		// objet pour API direction
		var direction;
		// titre des markers
		var titre;
		//liste des adresse � afficher (sous forme de tableau pour test)
		
		var markers = [];
		var cercleRayon;
		
		 var locations = [<%=listeAdresses %>];  
		/* var locations = [
							[ '1 place olivier, toulouse, france', 'Carson city' ],
							[ '54 rue R�guelongue, toulouse, france', 'Restaurant le Vietnam' ],
							[ '5 Rue Gustave Flaubert, blagnac, france', 'KFC' ],
							[ '7 Boulevard de l\'Europe, Portet surGaronne, france', 'KFC' ],
							[ '308 Avenue des �tats Unis, Fenouillet, france', 'KFC' ],
							[ '21 Chemin de la Salvetat, Colomiers, france', 'KFC' ],
							[ '81 Rue Saint-Jean, Balma, france', 'KFC' ],
							[ 'Voie Communale Giratoire de la Libert�, Narbonne, france', 'KFC' ],
							[ '62 boulevard Garibaldi, paris, france', 'RI Paris' ]
					];
		*/			


		//rayon d'action en KM
		var rayon;


		directionsDisplay = new google.maps.DirectionsRenderer({
			draggable : true
		});

		// Au chargement on lance l'initialisation du processus de trac� de la carte
		onload = 'initialize()';
		
		function changeRayon()
		{
				select = document.getElementById("rayonAction");
				choice = select.selectedIndex;
				valeur_cherchee = select.options[choice].value;
				ChangeRayonAction(valeur_cherchee);
		}
		

	</script>
	<form action='geolocalisation' method='post' id='formRayonAction' name='formRayonAction'>
		<!-- ATTENTION : il faut obligatoirement fixer la taille de la FRAME : � variabiliser pour responsive design -->
		<!-- <div id="map-canvas" style="height: 600px; width: 1000px"></div> -->
		<label>Rayon d'action</label>
		<select name='rayonAction' id='rayonAction' onchange='changeRayon()'>
			<option value='0'>Aucun</option>
			<option value='5'>5 km</option>
			<option value='10'>10 km</option>
			<option value='20'>20 km</option>
			<option value='30'>30 km</option>
			<option value='40'>40 km</option>
			<option value='50'>50 km</option>
			<option value='100'>100 km</option>
		</select>
		

		
			
	</form>
	
	<div id="map-canvas" ></div>
</section>

<section class='secContenu' id='listeMetier'>
	<h1 class='tetiereListes'>
		<span class='titreTetiere'>Clients � proximit�</span><span
			class='apportListe'>0 r�sultats</span>
		<div class='triTetieres'>
			<a href='#' class='teteListe' id='triClePrincip'>Nom ou raison sociale</a>
		</div>
	</h1>
	<%
		if(clientsProx != null && clientsProx.size()>0) 
		{
			for(int i = 0; i < clientsProx.size(); i++) 
			{
	%>
	<div class='listesClassiques'>
		<a href='itineraire?numClientSaisi=<%=clientsProx.get(i).getField("CLCLI")%>&sufClientSaisi=<%=clientsProx.get(i).getField("CLLIV")%>' class='detailsListe'>
			<div class="clePrincip"><%=clientsProx.get(i).getField("CLNOM")%></div>
			<!-- Autres champs � r�cup�rer et afficher ici -->
		</a>
		 <!-- <a href="geo:48,1587,38,5895"> gps </a> -->
	</div>
	<%
			}
		} 
		else 
		{
	%>
	<p class="messageListes">Pas de clients � proximit�</p>
	<%
		}
	%>
</section>

<script>
	// MAP GOOGLE //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function gpsToAddr(geocoder, pos, mapAddress)
	{
		geocoder.geocode({'latLng':pos}, function(data)
										 {
											// Fonctionne mais je ne sais pas comment sortir l'info de l�... Pour l'instant.
											address = data[0].formatted_address;
											//alert(address);
										 });
	}

	// Cr�ation de la carte en entr�e sur la page
	function initialize(rayonAction) {
		//D�claration du g�ocodeur
		geocoder = new google.maps.Geocoder();
		
		
		
		if(rayonAction == null || rayonAction == ""){
			rayon = 10;
		} else {
			rayon = rayonAction;
		}
		
	//	alert("AFTER CHOICE : " + rayon);

		//options de la carte
		var mapOptions = {
			zoom : 11
		};

		//int�gration de la carte � la DIV
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		

		//Ajout des possibilit�s de l'API direction
		directionsDisplay.setMap(map);

		// si la geolocation est accept�e par le navigateur
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				//On cherche la position actuelle de l'utilisateur   
				pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				//centrer sur Paris pour tests
				//pos = new google.maps.LatLng(48.858565, 2.347198);
				//On cr�e un marker sp�cial pour la position actuelle de l'utilisateur
				var infowindow = new google.maps.Marker({
					map : map,
					animation : google.maps.Animation.DROP,
					icon : {
						path : google.maps.SymbolPath.CIRCLE,
						fillColor : '#BF0000',
						strokeColor : '#BF0000',
						scale : 8
					},
					title : 'Votre position actuelle',
					position : pos
				});
				var mapAddress = {
					adresse : ""
				};

	//			gpsToAddr(geocoder, pos, mapAddress);
	//			alert(mapAddress.adresse);

				//On fixe le centre de la carte � la position d�tect�e par le sensor (position actuelle de l'utilisateur)
				map.setCenter(pos);
				
				//Mat�rialisation du rayon d'action
			    cercleRayon = new google.maps.Circle({
			      strokeColor: '#808080',
			      strokeOpacity: 0.3,
			      strokeWeight: 2,
			      fillColor: '#808080',
			      fillOpacity: 0.09,
			      center: pos,
			      radius: rayon * 1000,
			      map: map
			    });
				
				
			}, function() {
				handleNoGeolocation(true);
			});
		} else {
			// Browser doesn't support Geolocation
			handleNoGeolocation(false);
		}
		// Lecture des adresses : on fait appel � la fonction TrouverAdresse(adresseMarker, nomAafficher) qui va cr�er les markers pour chaque adresse
	//	for (i = 0; i < locations.length; i++) {
		for (indReq = 0; indReq < locations.length; indReq++) {
	//	for (indReq = 0; indReq < 10; indReq++) {
			if(locations[indReq] != null){
				AjouterAdresse(locations[indReq][0], locations[indReq][1]);
				wait(100);
			}
		}



	}

	// Si pas de g�oloc gestion des messages d'erreur
	function handleNoGeolocation(errorFlag) {
		if (errorFlag) {
			var content = 'Erreur: Le service de g�olocalisation ne fonctionne pas.';
		} else {
			var content = 'Erreur: Votre navigateur ne supporte pas la g�olocalisation.';
		}

		var options = {
			map : map,
			position : new google.maps.LatLng(48.833, 2.333),
			content : content
		};

		var infowindow = new google.maps.InfoWindow(options);
		map.setCenter(options.position);
	}

	//Place un marker correspondant � une adresse sur la carte sous la forme : "adresse, ville, pays" + titre � afficher sur l'�tiquette du marqueur
	function TrouverAdresse(adresse, titre) {
		var marker, i;
		// G�ocodage de l'adresse humainement lisible en positions latitude et longitude
		geocoder.geocode({
			'address' : adresse
		}, function code(results, status) {
			// Si le g�ocodage est OK on est sur terre et on cr�e un marker
			if (status == google.maps.GeocoderStatus.OK) {
				//cr�ation du marqueur
				marker = new google.maps.Marker({
					position : results[0].geometry.location,
					title : titre,
					map : map
				});
				markers.push(marker);
				//Calcul de distance en KM entre la position actuelle et les markers
				var targetLat = marker.getPosition().lat();
				var targetLng = marker.getPosition().lng();
				var targetLoc = new google.maps.LatLng(targetLat, targetLng);
				var center = map.getCenter();
				var distanceInkm = google.maps.geometry.spherical
						.computeDistanceBetween(center, targetLoc) / 1000;

				//Si la distance est sup�rieur au rayon d'action fix�, on change l'icone (petit et bleu comme un schtroumpf)
				if (distanceInkm > rayon) 
				{
					//marker.setVisible(false);
					marker.setIcon({
						path : google.maps.SymbolPath.CIRCLE,
						scale : 5,
						fillColor : "#00F",
						fillOpacity : 0.4,
						strokeWeight : 1,
					})
				}
			} else {
				// Si pas de r�sultat on est dans la merde
				alert('Adresse introuvable: ' + status);
			}
		});
	}
	
	// impl�mente le wait en javascript
	function wait(ms){
	   var start = new Date().getTime();
	   var end = start;
	   while(end < start + ms) {
	     end = new Date().getTime();
	  }
	}
	
	// envoi en post
	function envoyerDonneesEnPOST(page,nameData1,data1)
	{
	    var form = document.createElement('form');
	    form.setAttribute('action', page);
	    form.setAttribute('method', 'post');
	    form.setAttribute('accept-charset', 'utf-8');
	    var inputvar1 = document.createElement('input');
	    inputvar1.setAttribute('type', 'hidden');
	    inputvar1.setAttribute('name', nameData1);
	    inputvar1.setAttribute('value', data1);
	    form.appendChild(inputvar1);
	   
	    document.body.appendChild(form);
	    form.submit();
	}
	
	
	function ChangeRayonAction(rayon)
	{
		var rayonAction = 0;
		rayonAction = rayon;
		var pinImage = new google.maps.MarkerImage("http://maps.google.com/mapfiles/ms/icons/red-dot.png");
		 
		cercleRayon.setMap(null);
	 	cercleRayon = new google.maps.Circle({
		      strokeColor: '#808080',
		      strokeOpacity: 0.3,
		      strokeWeight: 2,
		      fillColor: '#808080',
		      fillOpacity: 0.09,
		      center: pos,
		      radius: rayonAction * 1000,
		      map: map
		    });
		  for (var indexMarker = 0; indexMarker < markers.length; indexMarker++) {
			    markers[indexMarker].setMap(null);
				//Calcul de distance en KM entre la position actuelle et les markers
				var LatCiblee = markers[indexMarker].getPosition().lat();
				var LngCiblee = markers[indexMarker].getPosition().lng();
				var LocCiblee = new google.maps.LatLng(LatCiblee, LngCiblee);
				var centreCiblee = pos;
				var distanceEnkm = google.maps.geometry.spherical
						.computeDistanceBetween(centreCiblee, LocCiblee) / 1000;
				
				// alert(indexMarker+ " : " + distanceEnkm+ " sur "+ rayonAction);
				
				//Si la distance est sup�rieur au rayon d'action fix�, on change l'icone (petit et bleu comme un schtroumpf)
				if (distanceEnkm <= rayonAction) 
				{
					markers[indexMarker].setIcon(pinImage);
				}
				else {
					markers[indexMarker].setIcon({
						path : google.maps.SymbolPath.CIRCLE,
						scale : 5,
						fillColor : "#00F",
						fillOpacity : 0.4,
						strokeWeight : 1,
					});
				}
				markers[indexMarker].setMap(map); 
				distanceEnkm = 0;
		  }

	}
	
	function AjouterAdresse(adresse, titre) {
		var marker, i;
		// G�ocodage de l'adresse humainement lisible en positions latitude et longitude
		geocoder.geocode({
			'address' : adresse
		}, function code(results, status) {
			// Si le g�ocodage est OK on est sur terre et on cr�e un marker
			if (status == google.maps.GeocoderStatus.OK) {
				//cr�ation du marqueur
				marker = new google.maps.Marker({
					position : results[0].geometry.location,
					title : titre,
					map : map
				});
				markers.push(marker);

			} else {
				 if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT){
					 wait(2500);
					// next(indReq);
				 }
				// Si pas de r�sultat on est dans la merde
				alert('Adresse introuvable: ' + status);
			}
		});
	}
	



	google.maps.event.addDomListener(window, 'load', initialize);
</script>


<jsp:include page="footer.jsp" />