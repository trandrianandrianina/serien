<%@page import="java.util.ArrayList"%>
<%@page import="ri.serien.libas400.database.record.GenericRecord"%>
<%@page import="ri.serien.libas400.dao.gvm.programs.actioncommerciale.M_ActionCommerciale"%>
<%@page import="ri.serien.libcommun.outils.DateHeure"%>
<% 
	M_ActionCommerciale[]	listeActions	= (M_ActionCommerciale[])request.getAttribute("listeActions");
%>

<jsp:include page="head.jsp" />

<section class='secContenu' id='listeMetier'>
	<h1 class='tetiereListes'>
		<span class='titreTetiere'>Planning</span><span class='apportListe'></span>
		<div class='triTetieres'>
			<a href='#' class='teteListe' id='triClePrincip'>Objet</a>
			<a href='#' class='teteListe' id='trivaleurPrincip'>Date</a>
		</div>
	</h1>
	<%  if( listeActions.length > 0 ){ %>
		<% for(M_ActionCommerciale elt : listeActions ){ %>
		<div class='listesClassiques'>
				<a href='ActionsCom?idaction=<%=elt.getACID() %>' class='detailsListe'>
					<div class="clePrincipCRM"><%=elt.getLabelACOBJ() %></div>
					<div class="cleSecondCRM"><%=elt.getClient().getCLNOM() %></div>
					<div class="valeurPrincipCRM"><%=DateHeure.getConvertJourHeure(13, 12, ""+elt.getEvenementContact().getETDCL(), "") + " - " + DateHeure.getConvertJourHeure(14, 15, ""+elt.getEvenementContact().getETHCL(), "")%></div>
				</a>
			<!-- Autres champs � r�cup�rer et afficher ici -->
		</div>
		<%} %>
	<%} else {%>
		<p class="messageListes">Pas d'�v�nement pour ces crit�res</p>
	<%} %>
</section>

<jsp:include page="footer.jsp" />