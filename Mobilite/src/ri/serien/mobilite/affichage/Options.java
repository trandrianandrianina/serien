/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

/**
 * Gestion de la popup Option qui permet de générer des PDF, mailer et ajouter dans les favoris
 */
public class Options {
  
  protected String retournerFenetreOptions(String titre, String[] liens) {
    if (liens == null) {
      return null;
    }
    String chaine = "";
    
    chaine = "<div id='filtreTotal'></div>";
    
    chaine += "<div id='fenOptions'>";
    chaine += "<h2>" + titre
        + "<a id='fermerOptions' href='#' onClick=\"switchOptionsListe();\"><img src='images/optionsListeRetour.png'/></a></h2>";
    if (liens[0] != null) {
      chaine += "<a class='possibilitesListe' id='optionPDF' href='" + liens[0] + "'><img src='images/optionsPDF.png'/></a>";
      // chaine += "<a class='possibilitesListe' id='optionStock' href='" + liens[0] + "'>stock</a>";
      // <img src='images/optionsStock.png'/></a>";
    }
    if (liens[1] != null) {
      chaine += "<a class='possibilitesListe' id='optionMail' href='" + liens[1] + "'><img src='images/optionsMail.png'/></a>";
    }
    if (liens[2] != null) {
      chaine += "<a class='possibilitesListe' id='optionFavori' href='" + liens[2] + "'><img src='images/optionsFavoris.png'/></a>";
    }
    /* if (liens[3] != null) {
      chaine += "<a class='possibilitesListe' id='optionStock' href='" + liens[3] + "'>stock</a>";//<img src='images/optionsStock.png'/></a>";
    }*/
    
    chaine += "</div>";
    return chaine;
  }
}
