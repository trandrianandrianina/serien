/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Connexion_SQLite;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Affichage et gestion de la connexion Utilisateur
 */
public class AffichageConnexion extends HttpServlet {
  private static final String PATTERN_ADRESSE_IP =
      "\\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b";
  
  private boolean okPourIntro = false;
  private Connexion_SQLite sqlite = null;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageConnexion() {
    super();
    
    // A paramétrer pour supprimer l'intro dans un cookies
    okPourIntro = true;
  }
  
  /**
   * Appelée lors de la construction du servlet par le serveur d'applications.
   * 
   * Cette méthode est appelée une seule fois après l'instanciation de la servlet. Aucun traitement ne peut être effectué par la
   * servlet tant que l'exécution de cette méthode n'est pas terminée.
   */
  @Override
  public void init(ServletConfig config) {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode);
      
      // Appeler la classe parente
      super.init(config);
      
      // Lire l'adresse IP du serveur IBM
      chargerAdresseIPServeurIBM();
      
      // Lire les paramètres NB_SESSIONS_FLOT et NB_SESSIONS_FIX dans le fichier SQLite
      File fichierBDD = new File(ConstantesEnvironnement.getNomCompletFichierSQLite());
      if (fichierBDD.isFile()) {
        // Ouvrir le fichier SQLite
        sqlite = new Connexion_SQLite(ConstantesEnvironnement.getNomCompletFichierSQLite(), null);
        if (sqlite != null) {
          // Lire le paramètre NB_SESSIONS_FLOT
          try {
            ConstantesEnvironnement.nbSessionFlot = Integer.valueOf(sqlite.recupererConstante("SESSIONS_FLOT"));
          }
          catch (Exception e) {
            ConstantesEnvironnement.nbSessionFlot = 0;
          }
          Trace.info(ConstantesEnvironnement.PREFIXE_PAREMETRE_SQLITE + "SESSIONS_FLOT=" + ConstantesEnvironnement.nbSessionFlot);
          
          // Lire le paramètre NB_SESSIONS_FIX
          try {
            ConstantesEnvironnement.nbSessionFix = Integer.valueOf(sqlite.recupererConstante("SESSIONS_FIX"));
          }
          catch (Exception e) {
            ConstantesEnvironnement.nbSessionFix = 1;
          }
          Trace.info(ConstantesEnvironnement.PREFIXE_PAREMETRE_SQLITE + "NB_SESSIONS_FIX=" + ConstantesEnvironnement.nbSessionFix);
          
          // Fermer le fichier SQLite
          sqlite.stopConnection();
        }
      }
      
      // Tracer de succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Traiter de manière générique le GET et le POST de la servlet.
   * @throws IOException
   * @throws ServletException
   */
  private void traiterPOSTouGET(HttpServletRequest pHttpServletRequest, HttpServletResponse pHttpServletResponse)
      throws IOException, ServletException {
    // Effacer tous les éléments de réponse de la servlet
    pHttpServletResponse.reset();
    
    // Lire des paramètres inclus dans dans la requête HTTP (? après l'URL)
    String connexion = pHttpServletRequest.getParameter("connexion");
    Trace.info("[HttpServletRequest] connexion = " + connexion);
    
    String echec = pHttpServletRequest.getParameter("echec");
    Trace.info("[HttpServletRequest] echec = " + echec);
    
    String loginCo = pHttpServletRequest.getParameter("loginCo");
    Trace.info("[HttpServletRequest] loginCo = " + loginCo);
    
    String mpCo = pHttpServletRequest.getParameter("mpCo");
    Trace.info("[HttpServletRequest] mpCo = " + mpCo);
    
    String ipmachine = pHttpServletRequest.getParameter("ipmachine");
    Trace.info("[HttpServletRequest] ipmachine = " + ipmachine);
    
    String arborescence = pHttpServletRequest.getParameter("arborescence");
    Trace.info("[HttpServletRequest] arborescence = " + arborescence);
    
    String profilAdmin = pHttpServletRequest.getParameter("profilAdmin");
    Trace.info("[HttpServletRequest] profilAdmin = " + profilAdmin);
    
    // Effacer la variable connexion en fonction du retour contenu dans la variable echec
    if (echec != null && echec.equals("1")) {
      connexion = null;
    }
    
    // Procédure de configuration lors du premier lancement
    // Tester si l'adresse IP du serveur IBM est connue
    ServletOutputStream out = pHttpServletResponse.getOutputStream();
    if (ConstantesEnvironnement.getAdresseIPServeurIBM() == null || ConstantesEnvironnement.getAdresseIPServeurIBM().trim().equals("")) {
      // Tester si l'adresse IP du serveur IP est connue
      if (ipmachine != null) {
        if (traiterParametreIpMachine(ipmachine, profilAdmin)) {
          afficherPageConnexion(pHttpServletRequest.getSession(), out, okPourIntro, "Connexion utilisateur 1", false, false);
        }
        else {
          afficherPageSaisieAdresseIPServeur(out, "Erreur de saisie IP + profil : " + ipmachine + "/" + profilAdmin);
        }
      }
      else if (arborescence != null) {
        creerArborescence();
        afficherPageSaisieAdresseIPServeur(out, "IP de la machine");
      }
      else {
        if (chargerAdresseIPServeurIBM()) {
          afficherPageConnexion(pHttpServletRequest.getSession(), out, okPourIntro, "Connexion utilisateur 2", false, false);
        }
        else {
          afficherPageCreationArborescence(out);
        }
      }
    }
    else if (connexion != null && connexion.equals("go")) {
      Utilisateur utilisateur = null;
      // Ouverture d'une nouvelle session
      if (pHttpServletRequest.getParameter("ecraser") == null) {
        utilisateur = gererLaSession(loginCo, mpCo);
      }
      // Cas d'écrasement de session doublon
      else {
        utilisateur = (Utilisateur) pHttpServletRequest.getSession().getAttribute("utilisateur");
      }
      
      // Utilisateur est valide, il est attribué à la session
      if (utilisateur != null) {
        // Créer la liste de sessions s'il existe pas dans le contexte
        if (getServletContext().getAttribute("sessions") == null) {
          getServletContext().setAttribute("sessions", new ArrayList<HttpSession>());
        }
        
        // Récupèration des sessions déjà actives
        ArrayList<HttpSession> sessions = ((ArrayList<HttpSession>) getServletContext().getAttribute("sessions"));
        
        Trace.info("Sessions : " + sessions);
        Trace.info("Profil super-utilisateur : " + utilisateur.isSuperUtilisateur());
        
        // Tester si on ne dépasse pas la limite de sessions OU si aucune limite de session n'a été fixée OU si l'utilisateur est un
        // utilisateur à session fixe
        if (sessions != null && (ConstantesEnvironnement.nbSessionFlot == 0 || sessions.size() <= ConstantesEnvironnement.nbSessionFlot
            || utilisateur.isSuperUtilisateur())) {
          // Gestion des cookies
          nettoyageCookies(pHttpServletRequest, pHttpServletResponse);
          
          // Récupération de la session en cours et y attribuer tous les attributs dont l'utilisateur
          HttpSession sessionPrincipale = pHttpServletRequest.getSession();
          
          // Contrôle si l'utilisateur n'est pas déjà connecté sous une autre session ou la même (onglets navigateurs)
          boolean sessionIdentique = false;
          boolean memeUtilisateur = false;
          if (pHttpServletRequest.getParameter("ecraser") == null) {
            for (int i = 0; i < sessions.size(); i++) {
              if (((Utilisateur) sessions.get(i).getAttribute("utilisateur")).getLogin().equals(utilisateur.getLogin())) {
                memeUtilisateur = true;
                if (sessions.get(i).getId().equals(sessionPrincipale.getId())) {
                  sessionIdentique = true;
                }
              }
              else {
                if (sessions.get(i).getId().equals(sessionPrincipale.getId())) {
                  sessionIdentique = true;
                }
              }
            }
          }
          // L'utilisateur est déjà connecté mais il choisit d'écraser la session
          else {
            for (int i = 0; i < sessions.size(); i++) {
              if (((Utilisateur) sessions.get(i).getAttribute("utilisateur")).getLogin().equals(utilisateur.getLogin())) {
                sessions.get(i).invalidate();
                break;
              }
            }
          }
          
          // Fixer la deconnexion automatique de la session et passer l'utilisateur
          sessionPrincipale.setMaxInactiveInterval(ConstantesEnvironnement.DECO_AUTO);
          sessionPrincipale.setAttribute("utilisateur", utilisateur);
          
          Trace.info("Mobilité - traiterPOSTouGET - Durée de vie de la session: " + ConstantesEnvironnement.DECO_AUTO);
          
          if (!sessionIdentique && memeUtilisateur) {
            afficherPageConnexion(pHttpServletRequest.getSession(), out, false, "Utilisateur d&eacute;j&agrave; connect&eacute", true,
                false);
          }
          else {
            if (sessionIdentique && !memeUtilisateur) {
              afficherPageConnexion(pHttpServletRequest.getSession(), out, false, "Session d&eacute;j&agrave; utilis&eacute;e", false,
                  false);
            }
            else {
              // Ne rajouter l'utilisateur que si c'est un nouvel utilisateur
              if (!memeUtilisateur) {
                sessions.add(sessionPrincipale);
              }
              
              // Basculer vers la page d'accueil
              Trace.info("Début de session " + sessionPrincipale.getId());
              getServletContext().getRequestDispatcher("/accueil?menu=G").forward(pHttpServletRequest, pHttpServletResponse);
              Trace.info("Mobilité - traiterPOSTouGET - Ouverture de la page : /accueil?menu=G");
            }
          }
        }
        // Le nombre max de sessions est dépassé
        else {
          afficherPageConnexion(pHttpServletRequest.getSession(), out, false, "Nombre maximum de sessions atteint", false, true);
        }
      }
      // Erreur d'authentification (SI JAMAIS ON s'est JAMAIS CONNECTE mettre un truc dans sqlite qui détecte que
      // c'est l'adresse IP de la machine qui est pourrie et pas les logins -> retourner l'interface de saisie IP)
      else {
        afficherPageConnexion(pHttpServletRequest.getSession(), out, false, "Authentification incorrecte", false, true);
      }
    }
    // Déconnexion volontaire
    else if (connexion != null && connexion.equals("stop")) {
      afficherPageConnexion(pHttpServletRequest.getSession(), out, false, "Utilisateur d&eacute;connect&eacute", false, true);
      // Affichage classique page à vide
    }
    else {
      afficherPageConnexion(pHttpServletRequest.getSession(), out, okPourIntro, "Utilisateur d&eacute;connect&eacute", false, false);
    }
  }
  
  /**
   * Afficher la page de création de l'arborescence.
   * @throws IOException
   */
  private void afficherPageCreationArborescence(ServletOutputStream out) throws IOException {
    // Tracer
    Trace.info("Afficher la page de création de l'arborescence");
    
    // Générer la page
    out.println("<div id='ajust'></div>");
    out.println("<article id='intro'>");
    out.println("<p>S&eacute;rie N <span>mobilit&eacute;</span></p>");
    out.println("</article>");
    out.println("<article id='arborescence'>");
    out.println("<p id='messageConnexion'>Créer l'arborescence</p>");
    out.println("<form action='connexion' method='post' name='formArborescence'>");
    out.println("<div id='champs'>");
    out.println("<input name ='arborescence' id='arborescence' type='hidden'>");
    out.println("<input id='validation' type='submit' value='valider'>");
    out.println("</div>");
    out.println("</form>");
    out.println("</article>");
  }
  
  /**
   * Gérer la connexion avec le système Serveur et l'authentification de l'utilisateur.
   */
  private Utilisateur gererLaSession(String pProfil, String pMdp) {
    // Tous les traitements qui permettent de se connecter à l'AS400 retourne OK si on est connecté au système
    SystemeManager systeme = new SystemeManager(ConstantesEnvironnement.getAdresseIPServeurIBM(), pProfil, pMdp, true);
    if (systeme.getSystem() == null) {
      Trace.erreur("Mobilité - gererLaSession - Connexion au système impossible.");
      return null;
    }
    
    // Vérification que l'utilisateur est bien connecté à DB2 avant de valider l'utilisateur
    Utilisateur utilisateur = new Utilisateur(pProfil, systeme);
    if (utilisateur.getMaconnection() == null) {
      Trace.erreur("L'utilisateur est invalide : " + pProfil);
      return null;
    }
    
    if (utilisateur.getNiveauAcces() == ConstantesEnvironnement.ACCES_NON) {
      utilisateur = null;
      Trace.erreur("L'utilisateur n'est pas autorisé : " + pProfil);
    }
    else {
      utilisateur.setMdp(pMdp);
    }
    
    return utilisateur;
  }
  
  /**
   * Réafficher la fenêtre de connexion avec les nouveaux paramètres
   * @throws IOException
   */
  protected void afficherPageConnexion(HttpSession session, ServletOutputStream out, boolean intro, String message, boolean isConf,
      boolean invalider) throws IOException {
    // invalider la session si nécessaire
    if (invalider) {
      session.invalidate();
    }
    
    // Tracer l'affichage de cette page
    Trace.info("Afficher la page de connexion");
    
    // Générer la page
    out.println("<!DOCTYPE html>");
    out.println("<html lang='fr'>");
    out.println("<head>");
    out.println(gestionModeApp());
    out.println("<meta charset='utf-8'>");
    out.println(
        "<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale = 1, maximum-scale=1.0, target-densityDpi=device-dpi' />");
    
    out.println("<meta name='ROBOTS' content='NONE'> ");
    out.println("<meta name='GOOGLEBOT' content='NOARCHIVE'>");
    /*web APP Apple*/
    out.println("<meta name='apple-mobile-web-app-capable' content='yes'>");
    out.println("<meta name='apple-mobile-web-app-status-bar-style' content='black-translucent' />");
    /* ------    */
    out.println("<title>Connexion S&eacuterie N mobilit&eacute;</title>");
    out.println("<link href='css/connexion.css' rel='stylesheet'/>");
    if (intro) {
      out.println("<script src='scripts/scriptsConnexion.js'></script>");
    }
    else {
      out.println("<link href='css/connexionRapide.css' rel='stylesheet'/>");
    }
    out.println("<link rel=\"apple-touch-icon\" href=\"touch-icon-iphone.png\">");
    out.println("<link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"touch-icon-ipad.png\">");
    out.println("<link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"touch-icon-iphone-retina.png\">");
    out.println("<link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"touch-icon-ipad-retina.png\">");
    out.println("<link rel=\"shortcut icon\" href=\"images/favicon.png\" />");
    out.println("</head>");
    out.println("<body>");
    
    out.println("<div id='ajust'></div>");
    out.println("<article id='intro'>");
    out.println("<p><i id='suiteH1'>Suite</i>S&eacute;rie N <span>mobilit&eacute;</span></p>");
    out.println("</article>");
    out.println("<article id='connexion'>");
    out.println("<p id='messageConnexion'>" + message + "</p>");
    if (isConf) {
      out.println("<p id='messageConf'><span id='texteMessageConf'>Supprimer l'ancienne connexion ?</span>");
      out.println(
          "<a class='liensConf' id='validerConf' href='connexion?connexion=go&ecraser=1'><img src='images/ficheValider.png'/></a>");
      out.println("<a class='liensConf' id='annulerConf' href='connexion'><img src='images/ficheAnnuler.png'/></a>");
      out.println("</p>");
    }
    else {
      out.println("<form action='connexion' method='post' name='formConnexion'>");
      out.println("<div id='champs'>");
      out.println(
          "<input name ='loginCo' class='champ' id='login' type='text' placeholder='login' required autofocus autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false'><br/>");
      out.println(
          "<input name ='mpCo' class='champ' id='motPasse' type= 'password' placeholder='mot de passe' required autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false'>");
      out.println("<input name = 'connexion' type='hidden' value='go'>");
      out.println("<input id='validation' type='submit' value='&nbsp;'>");
      out.println("</div>");
      out.println("</form>");
    }
    out.println("</article>");
    out.println("<section id='mobile'><i id='suite'>Suite</i>S&eacute;rie N <span> mobilit&eacute;</span></section>");
    if (ConstantesEnvironnement.getLibelleServeurTomcat() != null) {
      out.println("<p id='LibelleServeur'>" + ConstantesEnvironnement.getLibelleServeurTomcat() + "</p>");
    }
    out.println("<img id='logoRi' src='images/ri.png' alt='logo RI'>");
    
    out.println("</body>");
    out.println("</html>");
  }
  
  /**
   * Afficher l'interface de saisie de l'adresse IP de l' AS400. Seul l'admin principal peut la saisir
   * @throws IOException
   */
  private void afficherPageSaisieAdresseIPServeur(ServletOutputStream out, String message) throws IOException {
    // Tracer
    Trace.info("Afficher la page de saisie de l'adresse IP du serveur IBM");
    
    // Générer la page
    out.println("<div id='ajust'></div>");
    out.println("<article id='intro'>");
    out.println("<p>S&eacute;rie N <span>mobilit&eacute;</span></p>");
    out.println("</article>");
    out.println("<article id='connexion'>");
    out.println("<p id='messageConnexion'>" + message + "</p>");
    out.println("<form action='connexion' method='post' name='formIpMachine'>");
    out.println("<div id='champs'>");
    out.println(
        "<input name ='ipmachine' class='champ' id='ipmachine' type='text' placeholder='adresse ip' required autofocus autocomplete='off' autocorrect='off' autocapitalize='off' pattern='"
            + PATTERN_ADRESSE_IP + "' spellcheck='false'><br/>");
    out.println(
        "<input name ='profilAdmin' class='champ' id='login' type='text' placeholder='profil admin' required autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false'>");
    out.println("<input id='validation' type='submit' value='valider'>");
    out.println("</div>");
    out.println("</form>");
    out.println("</article>");
  }
  
  /**
   * Traiter l'adresse IP saisie pour l'insérer dans la bdd
   */
  boolean traiterParametreIpMachine(String pAdresseIP, String pProfil) {
    // Se connecter au fichier SQLite
    if (sqlite == null) {
      sqlite = new Connexion_SQLite(ConstantesEnvironnement.getNomCompletFichierSQLite(), null);
    }
    
    // Sauver l'adresse IP du serveur IBM
    if (sqlite.sauverAdresseIPServeurIBM(pAdresseIP, pProfil)) {
      return chargerAdresseIPServeurIBM();
    }
    else {
      return false;
    }
  }
  
  /**
   * Lire l'adresse IP du serveur IBM afin de la mémoriser en paramètre général.
   * @return true=le paramètre existe, false=le paramètre n''existe pas.
   */
  private boolean chargerAdresseIPServeurIBM() {
    // Vérifier si le fichier SQLite est présent
    File fichierBDD = new File(ConstantesEnvironnement.getNomCompletFichierSQLite());
    if (fichierBDD.isFile()) {
      // Ouvrir le fichier SQLite
      sqlite = new Connexion_SQLite(ConstantesEnvironnement.getNomCompletFichierSQLite(), null);
      if (sqlite != null) {
        // Récupérer l'adresse IP du serveur
        ConstantesEnvironnement.setAdresseIPServeurIBM(sqlite.recupererConstante("IP_AS400"));
        
        // Fermer le fichier SQLite
        sqlite.stopConnection();
        sqlite = null;
      }
    }
    
    // Tracer la paramètre
    Trace.info(ConstantesEnvironnement.PREFIXE_PAREMETRE_SQLITE + "IP_AS400 = " + ConstantesEnvironnement.getAdresseIPServeurIBM());
    
    // Vérifier que l'adresse IP est valide
    return ConstantesEnvironnement.getAdresseIPServeurIBM() != null && !ConstantesEnvironnement.getAdresseIPServeurIBM().trim().isEmpty();
  }
  
  /**
   * Créer l'arborescence de dossiers utilisée par le logiciel.
   * En même temps, le fichier pour la base de données SQLite est créé.
   */
  private void creerArborescence() {
    // Tester si le dossier de la base de données SQLLite existe
    File dossierSQLite = new File(ConstantesEnvironnement.getDossierSQLite());
    if (!dossierSQLite.isDirectory()) {
      // Tracer
      Trace.info("Créer le dossier SQLite : " + dossierSQLite);
      
      // Créer le dossier SQLite s'il n'existe pas
      if (!dossierSQLite.mkdirs()) {
        throw new MessageErreurException("Impossible de créer le dossier SQLite : " + dossierSQLite);
      }
    }
    
    // Tester si le dossier de sauvegarde existe
    File dossierSauvegarde = new File(ConstantesEnvironnement.getDossierSauvegardeSQLite());
    if (!dossierSauvegarde.isDirectory()) {
      // Tracer
      Trace.info("Créer le dossier sauvegarde : " + dossierSauvegarde);
      
      // Créer le dossier sauvegarde s'il n'existe pas
      if (!dossierSauvegarde.mkdirs()) {
        throw new MessageErreurException("Impossible de créer le dossier sauvegarde : " + dossierSQLite);
      }
    }
    
    // Tester si le fichier SQLite existe
    File fichierSQLite = new File(ConstantesEnvironnement.getNomCompletFichierSQLite());
    if (!fichierSQLite.isFile()) {
      // Tracer
      Trace.info("Créer le fichier SQLite : " + fichierSQLite);
      
      // Créer le fichier SQLite
      creerFichierSQLite();
    }
  }
  
  /**
   * Copier le fichier de BDD initial pour SQLite.
   */
  private void creerFichierSQLite() {
    // Construire le chemin du fichier original
    File fichierOriginal = new File(ConstantesEnvironnement.getNomCompletFichierSQLiteOriginal());
    
    // Tracer
    Trace.info("Créer le fichier SQLite à partir du modèle situé : " + fichierOriginal);
    
    // Tester si le fichier original est présent
    if (!fichierOriginal.isFile()) {
      throw new MessageErreurException(
          "Impossible de trouyver le fichier original pour initialiser la base de données SQLite : " + fichierOriginal);
    }
    
    // Copier le fichier original à l'emplacement attendu par le logiciel
    FileInputStream filesource = null;
    FileOutputStream fileDestination = null;
    try {
      filesource = new FileInputStream(fichierOriginal);
      fileDestination = new FileOutputStream(ConstantesEnvironnement.getNomCompletFichierSQLite());
      byte buffer[] = new byte[512 * 1024];
      int nblecture;
      while ((nblecture = filesource.read(buffer)) != -1) {
        fileDestination.write(buffer, 0, nblecture);
      }
    }
    catch (Exception e) {
      throw new MessageErreurException(e,
          "Impossible de trouyver le fichier original pour initialiser la base de données SQLite : " + fichierOriginal);
    }
    finally {
      // Fermer le fichier source
      try {
        if (filesource != null) {
          filesource.close();
        }
      }
      catch (Exception e) {
        // RAS
      }
      
      // Fermer le fichier destination
      try {
        if (fileDestination != null) {
          fileDestination.close();
        }
      }
      catch (Exception e) {
        // RAS
      }
    }
  }
  
  /**
   * récupère les cookies du navigateur pour en récupérer les valeurs correspondantes à notre page
   */
  private void nettoyageCookies(HttpServletRequest request, HttpServletResponse response) {
    // récupération des cookies du navigateur
    Cookie[] cookies = request.getCookies();
    if (cookies == null) {
      return;
    }
    
    // trouver notre cookie et récupérer ses valeurs
    for (int i = 0; i < cookies.length; i++) {
      // cookie éphémere des sélections
      if (cookies[i].getName().equals("selectionClient") || cookies[i].getName().equals("selectionArticle")) {
        cookies[i].setValue(null);
        cookies[i].setMaxAge(0);
        cookies[i].setPath("/");
        response.addCookie(cookies[i]);
      }
    }
  }
  
  /**
   * Afficher un script en début de meta qui force le mode Web-App
   */
  private String gestionModeApp() {
    String retour = "";
    
    retour += "<script type=\"text/javascript\">";
    // liens internes
    retour += "(function(document,navigator,standalone) {";
    retour += "if ((standalone in navigator) && navigator [standalone] ) { ";
    retour += "var curnode, location=document.location, stop=/^(a|html)$/i;";
    retour += "document.addEventListener('click', function(e) {";
    retour += "curnode=e.target;";
    retour += "while (!(stop).test(curnode.nodeName)) {";
    retour += "curnode=curnode.parentNode;}";
    retour += "if('href' in curnode && ( curnode.href.indexOf('http') || ~curnode.href.indexOf(location.host) ) ) {";
    retour += "e.preventDefault();";
    retour += "location.href = curnode.href;}";
    retour += "},false);}";
    retour += "})(document,window.navigator,'standalone');";
    
    retour += "</script>";
    
    return retour;
  }
}
