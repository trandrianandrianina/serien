/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.backoffice.GestionParametres;
import ri.serien.mobilite.backoffice.Parametre;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class AffichageParametres
 */
public class AffichageParametres extends HttpServlet {
  
  private PatternErgonomie pattern = null;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageParametres() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Méthode générique qui traite le GET et le POST
   */
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      int niveau = 1;
      // String[] liensOptions = {"#","#","#"};
      String metaSpecifiques = "";
      String titreOptions = "Options";
      
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        // On créé un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        if (utilisateur.getGestionParametres() == null) {
          utilisateur.setGestionParametres(new GestionParametres(utilisateur));
        }
        
        // charger le look correspondant
        metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
        metaSpecifiques += "<link href='css/listes.css' rel='stylesheet'/>";
        
        ServletOutputStream out = response.getOutputStream();
        out.println(pattern.afficherEnTete(metaSpecifiques, null));
        out.println(pattern.afficherHeader(-2, niveau, null, utilisateur));
        out.println("<article id='contenu'>");
        out.println("<section class='secContenu' id='gestionParametres'>");
        
        out.println(afficherLesParametres(out, utilisateur));
        
        out.println("</section>");
        out.println("</article>");
        out.println(pattern.afficherFin(titreOptions, null));
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Afficher la liste des paramètres stockés dans la BDD SQLITE
   */
  private String afficherLesParametres(ServletOutputStream out, Utilisateur utilisateur) {
    ArrayList<Parametre> liste = utilisateur.getGestionParametres().recupererLesParametres();
    String retour = "";
    try {
      
      retour = "<h1><span class='titreH1'>Paramètres</h1>";
      
      if (liste == null || liste.size() == 0) {
        retour += "Pas de paramètre disponible";
      }
      else {
        for (int i = 0; i < liste.size(); i++) {
          if (i % 2 == 0) {
            retour += "<div class='listesClassiques'>";
          }
          else {
            retour += "<div class='listesClassiquesF'>";
          }
          
          retour += "<a class='labelParametre' href='" + liste.get(i).getPara_lien() + "'><span class='lblPara'>"
              + liste.get(i).getPara_libelle() + "</span></a>";
          
          retour += "</div>";
        }
      }
      
      return retour;
      
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherLesParametres()");
      return null;
    }
  }
  
}
