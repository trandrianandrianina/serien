/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.constantes.ConstantesAffichage;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.article.GestionArticles;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class AffichageArticles
 */
public class AffichageArticles extends HttpServlet {
  
  private String[] infosRecherche = { "articles", "formArticles", "Recherche articles" };
  private PatternErgonomie pattern = null;
  private StringBuilder divListeClient = new StringBuilder(100);
  
  /**
   * @see HttpServlet#HttpServlet()
   *      Constructeur par défaut d'une page d'affichage articles
   */
  
  public AffichageArticles() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Appelée lors de la construction du servlet par le serveur d'applications.
   * 
   * Cette méthode est appelée une seule fois après l'instanciation de la servlet. Aucun traitement ne peut être effectué par la
   * servlet tant que l'exécution de cette méthode n'est pas terminée.
   */
  @Override
  public void init(ServletConfig config) {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode);
      
      // Effectuer le traitement
      super.init(config);
      
      // Tracer de succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      String[] liensOptions = { "#", "mailto:", "#" };
      String metaSpecifiques = null;
      String titreOptions = "Options";
      
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        // On créé un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        // si ce n'est pas fait, accéder à la gestion métier des articles
        if (utilisateur.getGestionArticles() == null) {
          utilisateur.setGestionArticles(new GestionArticles(utilisateur));
        }
        
        // int vue = ConstantesAffichage.METIER_ARTICLES;
        int vue = 0;
        
        // ++++++++++++++++++++++++++++++++++++ paramètres passés ++++++++++++++++++++++++++++++++++++++++
        String nomclient = request.getParameter("rechclient");
        // Traitement AJAX sur la recherche client
        if (nomclient != null) {
          response.getWriter().write(rechercheclient(utilisateur, nomclient));
          return;
        }
        
        // mot clé de recherche -> recherche stockée
        String recherche = request.getParameter("recherche");
        String special = request.getParameter("special");
        // données d'un établissement
        String etb = request.getParameter("A1ETB");
        String code = request.getParameter("A1ART");
        String mag = request.getParameter("chgMagasin");
        
        String options = request.getParameter("options");
        int nbLignes = utilisateur.getNbLignesListes() + 1;
        
        String ancrage = request.getParameter("ancrage");
        // Gestion de pages
        String page = request.getParameter("page");
        // mode modification
        boolean isEnModif = request.getParameter("isEnModif") != null && request.getParameter("isEnModif").equals("1");
        // tri sur les listes
        String tri = request.getParameter("tri");
        // niveau métier pour le fil rouge (liste, fiche...)
        if (request.getParameter("niveau") != null) {
          utilisateur.getGestionArticles().setNiveauMetier(Integer.parseInt(request.getParameter("niveau")));
        }
        // passer la vue demandée à la gestion des clients
        if (request.getParameter("vue") != null) {
          try {
            // pour garder les variables de la précédente vue
            if (vue != 0) {
              tri = "MEMELIGNES";
            }
            else {
              tri = "VUE";
            }
            vue = Integer.parseInt(request.getParameter("vue"));
            
          }
          catch (Exception e) {
            vue = 1;
            Trace.erreur(e, "Echec ParseInt" + request.getParameter("vue"));
          }
          
          // attribuer la vue à la gestion
          utilisateur.getGestionArticles().setVue(vue);
          
        }
        // Récupération du numéro client et du numéro livré
        utilisateur.getInfosClient().setWithUrlParameters(request.getParameter("clcli"), request.getParameter("clliv"));
        
        // Gestion du nombre de lignes des listes
        if ((page != null && page.equals("retour")) || (tri != null && !tri.equals("PLUSLIGNES"))
            || request.getParameter("vue") != null) {
          nbLignes = utilisateur.getGestionArticles().getNbLignesMax();
        }
        else if (request.getParameter("plusDeLignes") != null) {
          nbLignes = Integer.parseInt(request.getParameter("plusDeLignes")) + utilisateur.getNbLignesListes();
        }
        
        // ++++++++++++++++++++++++++++++++++++ Traitements ++++++++++++++++++++++++++++++++++++++++
        
        // Gestion des cookies articles
        utilisateur.getGestionArticles().setListeSelection(gestionCookies(request));
        
        // si on rafraichit la page inititialiser la gestion des articles
        if (page != null && page.equals("refresh")) {
          utilisateur.getGestionArticles().initDonnes();
        }
        
        // Changer le niveau du métier vers la fiche article si un code article est passé ou si on change juste de vue
        if (etb != null && code != null) {
          utilisateur.getGestionArticles().setNiveauMetier(2);
        }
        else if (utilisateur.getGestionArticles().getNiveauMetier() == 2 && request.getParameter("vue") != null) {
          // pas bo pas propre. Le métier a rien à foutre là bordel !! UN message perso de David pour David, je pense
          // qu'il se reconnaitra.
          etb = utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim();
          code = utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim();
          // ajout
          
        }
        // sinon changer le niveau métier vers la liste
        else {
          utilisateur.getGestionArticles().setNiveauMetier(1);
        }
        
        // charger le CSS pour la liste de clients et les infos pour les options de la page
        if (utilisateur.getGestionArticles().getNiveauMetier() == 1) {
          metaSpecifiques = "<link href='css/listes.css' rel='stylesheet'/>";
          liensOptions = new String[] { "#", "mailto:", null };
        }
        // CSS et infos pour la fiche articles et les infos pour les options de la page
        else {
          metaSpecifiques = "<link href='css/fiches.css' rel='stylesheet'/>";
        }
        
        switch (utilisateur.getGestionArticles().getTypeVue()) {
          case 1:
            break;
          case 2:
            metaSpecifiques += "<link href='css/stockDispo.css' rel='stylesheet'/>";
            break;
          case 3:
            metaSpecifiques += "<link href='css/prixVente.css' rel='stylesheet'/><script src='scripts/rechercheClient.js?"
                + ConstantesEnvironnement.VERSION_JS + "'></script>";
            break;
        }
        
        ServletOutputStream out = response.getOutputStream();
        out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
        out.println(pattern.afficherHeader(ConstantesAffichage.METIER_ARTICLES, utilisateur.getGestionArticles().getNiveauMetier(),
            infosRecherche, utilisateur));
        out.println(pattern.afficherOptions(utilisateur, ConstantesAffichage.METIER_ARTICLES,
            utilisateur.getGestionArticles().getTypeVue(), utilisateur.getGestionArticles().getNiveauMetier(), false));
        
        // on affiche la liste que si le code article n'est passé en paramètre et si on ne change pas de vue
        if (utilisateur.getGestionArticles().getNiveauMetier() == 1) {
          if (request.getParameter("chgMagasin") != null) {
            mag = request.getParameter("chgMagasin");
            utilisateur.changerMag(mag);
          }
          else if (!ConstantesEnvironnement.stockDG) {
            mag = utilisateur.getMagasin().getCodeMagasin();
          }
          /* else {
             mag=utilisateur.getMagasin().getCodeMagasin();
          
          }*/
          afficherLesArticles(out, utilisateur, recherche, tri, special, page, nbLignes);
          // liensOptions[1] = "mailto:?subject=liste articles";
          liensOptions[1] = "#";
          // mag = null;
          
        }
        // -------------------ARTICLES
        else if (utilisateur.getGestionArticles().getNiveauMetier() == 2) {
          
          // utilisateur.changerMag(mag);
          afficherUnArticle(out, utilisateur, etb, code, page, options, isEnModif);
          
          liensOptions[1] = "mailto:?subject=fiche article&body=" + utilisateur.getGestionArticles().retournerInfosBrutesFiche();
          liensOptions[2] = "articles?A1ETB=" + utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim()
              + "&A1ART=" + utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim() + "&S1MAG="
              + request.getParameter("chgMagasin") + "&options=favoris";
          
        }
        
        out.println(pattern.afficherFin(titreOptions, liensOptions));
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Traitement pour la recherche client
   * @throws IOException
   */
  private String rechercheclient(Utilisateur utilisateur, String nomclient) {
    divListeClient.setLength(0);
    
    // Cas particulier : on ré-initialise les infos client
    /*
    if (nomclient.equals("*reset"))
    
    {
      utilisateur.getInfosClient().reset();
      return " ";
    }*/
    
    // On recherche les clients sur les premières lettres
    ArrayList<GenericRecord> listeClients = utilisateur.getGestionArticles().traitementRechercheClient(nomclient);
    if ((listeClients != null) && (listeClients.size() > 0)) {
      for (int i = 0; i < listeClients.size(); i++) {
        divListeClient.append("<a class='detailClient' href=\"articles?vue=3&clcli=").append(listeClients.get(i).getField("CLCLI"))
            .append("&clliv=").append(listeClients.get(i).getField("CLLIV")).append("\">")
            .append(((String) listeClients.get(i).getField("CLNOM")).trim()).append("</a>");
      }
    }
    
    return divListeClient.toString();
  }
  
  /**
   * récupère les cookies du navigateur pour en récupérer les valeurs correspondantes à notre page
   */
  private String[] gestionCookies(HttpServletRequest request) {
    String[] idArticles = null;
    // récupération des cookies du navigateur
    Cookie[] cookies = request.getCookies();
    // trouver notre cookie et récupérer ses valeurs
    for (int i = 0; i < cookies.length; i++) {
      if (cookies[i].getName().equals("selectionArticle")) {
        String valeurs = cookies[i].getValue();
        if (valeurs != null && !valeurs.trim().equals("")) {
          idArticles = valeurs.split("_");
        }
      }
    }
    
    return idArticles;
  }
  
  /**
   * affiche la liste des articles en fonction des vues, types de recherche et des tris passés
   */
  private void afficherLesArticles(ServletOutputStream out, Utilisateur utilisateur, String recherche, String tri, String special,
      String page, int nbLignes) {
    try {
      String apport = "";
      out.println("<article id='contenu'>");
      out.println("<section class='secContenu' id='listeMetier'>");
      
      // Si on tri la liste précédente
      if (tri != null) {
        if (recherche == null) {
          recherche = utilisateur.getGestionArticles().getMotCle();
        }
        if (special == null) {
          special = utilisateur.getGestionArticles().getRechercheSpeciale();
        }
        
      }
      // si on est pas en mode tri
      else {
        // si on passe un mot de recherche -> péter la recherche spéciale stockée
        if (recherche != null) {
          special = null;
        }
      }
      
      if (utilisateur.getGestionArticles().getListeRecords() == null) {
        apport = "Mes favoris";
      }
      
      // calculer une nouvelle liste si on ne retourne pas sur l'ancienne
      if (page == null || !page.equals("retour") || tri != null) {
        // si on récupère les même lignes
        if (tri != null && (tri.equals("MEMELIGNES") || tri.equals("VUE"))
            && utilisateur.getGestionArticles().getListeRecords() != null) {
          utilisateur.getGestionArticles().setListeRecords(utilisateur.getGestionArticles().recuperationDesMemeRecords(tri));
        }
        else {
          utilisateur.getGestionArticles().chercherDesRecords(recherche, tri, special, nbLignes);
        }
      }
      // on retourne sur l'ancienne mais avec d'éventuelles nouvelles données
      else {
        utilisateur.getGestionArticles().setListeRecords(utilisateur.getGestionArticles().recuperationDesMemeRecords(tri));
      }
      
      // traiter le texte apport
      if (utilisateur.getGestionArticles().getRechercheSpeciale() != null) {
        if (utilisateur.getGestionArticles().getRechercheSpeciale().equals("fav")) {
          apport = "Mes favoris";
        }
        if (utilisateur.getGestionArticles().getRechercheSpeciale().equals("sel")) {
          apport = "Ma s&eacute;lection";
        }
      }
      
      // affichage des clients par vue demandée
      switch (utilisateur.getGestionArticles().getTypeVue()) {
        case 1:
          break;
        case 2:
          out.println(utilisateur.getGestionArticles().getStockdispo().afficherListeRecords(apport, nbLignes));
          break;
        case 3:
          out.println(utilisateur.getGestionArticles().getPrixVente().afficherListeRecords(apport, nbLignes));
          break;
      }
      out.println("</section>");
      out.println("</article>");
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherLesArticles()");
    }
  }
  
  /**
   * Affiche l'article sélectionné ou recherché
   */
  private void afficherUnArticle(ServletOutputStream out, Utilisateur utilisateur, String etb, String code, String page, String options,
      boolean isEnModif) {
    try {
      boolean isUnFavori = false;
      out.println("<article id='contenu'>");
      out.println("<section class='secContenu' id='ficheMetier'>");
      utilisateur.getGestionArticles().recupererUnRecord(etb, code);
      
      // traiter les favoris
      isUnFavori = utilisateur.getGestionArticles().isUnFavori(options != null && options.equals("favoris"));
      
      if (utilisateur.getGestionArticles().getRecordActuel() == null) {
        out.println("L'article n'a pas &eacute;t&eacute; trouv&eacute;");
      }
      else {
        // récupérer la vue demandée
        switch (utilisateur.getGestionArticles().getTypeVue()) {
          case 1:
            break;
          case 2:
            out.println(utilisateur.getGestionArticles().getStockdispo().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 3:
            out.println(utilisateur.getGestionArticles().getPrixVente().afficherLeRecord(isUnFavori, isEnModif));
            break;
        }
        
        out.println(majTailleFiche("ficheMetier"));
      }
      out.println("</section>");
      out.println("</article>");
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherUnArticle()");
    }
  }
  
  /**
   * retailler la zone de fiche principale de manière dynamiiiiique
   */
  private String majTailleFiche(String id) {
    String retour = "<script>";
    retour += "majTailleFiche(\"" + id + "\");";
    retour += "</script>";
    
    return retour;
  }
  
}
