/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.constantes.ConstantesAffichage;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.client.GestionClients;
import ri.serien.mobilite.outils.Outils;

/**
 * Classe d'affichage des clients
 */
public class AffichageClients extends HttpServlet {
  
  private String[] infosRecherche = { "clients", "formClients", "RECHERCHE CLIENT" };
  private PatternErgonomie pattern = null;
  
  /**
   * Constructeur par défaut d'une page d'affichage clients
   * @see HttpServlet#HttpServlet()
   */
  public AffichageClients() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Appelée lors de la construction du servlet par le serveur d'applications.
   * 
   * Cette méthode est appelée une seule fois après l'instanciation de la servlet. Aucun traitement ne peut être effectué par la
   * servlet tant que l'exécution de cette méthode n'est pas terminée.
   */
  @Override
  public void init(ServletConfig config) {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode);
      
      // Effectuer le traitement
      super.init(config);
      
      // Tracer de succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * méthode générique au traitements des paramètres passés dans le GET ou le POST
   * @param request
   * @param response
   */
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      String[] liensOptions = { "#", "mailto:", "#" };
      String metaSpecifiques = null;
      String titreOptions = "Options";
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        ServletOutputStream out = response.getOutputStream();
        // On créé un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        // si ce n'est pas fait, accéder à la gestion métier des clients
        if (utilisateur.getGestionClients() == null) {
          utilisateur.setGestionClients(new GestionClients(utilisateur));
        }
        
        // variables
        int vue = 0;
        
        // ++++++++++++++++++++++++++++++++++++ paramètres passés ++++++++++++++++++++++++++++++++++++++++
        
        // mot clé de recherche -> recherche stockée
        String recherche = request.getParameter("recherche");
        String special = request.getParameter("special");
        // Données d'un établissement
        String etb = request.getParameter("CLETB");
        String idSoc = request.getParameter("CLCLI");
        String suff = request.getParameter("CLLIV");
        // Gestion de pages
        String page = request.getParameter("page");
        String module = request.getParameter("module");
        // Gestion des options de fiche ou de liste
        String options = request.getParameter("options");
        // ancrage dans la page
        String ancrage = request.getParameter("ancrage");
        // mode modification
        boolean isEnModif = request.getParameter("isEnModif") != null && request.getParameter("isEnModif").equals("1");
        // nombre de lignes des listes
        int nbLignes = utilisateur.getNbLignesListes() + 1;
        // tri sur les listes
        String tri = request.getParameter("tri");
        // niveau métier pour le fil rouge (liste, fiche...)
        if (request.getParameter("niveau") != null) {
          utilisateur.getGestionClients().setNiveauMetier(Integer.parseInt(request.getParameter("niveau")));
        }
        // passer la vue demandée à la gestion des clients
        if (request.getParameter("vue") != null) {
          try {
            if (vue != 0) {
              tri = "MEMELIGNES";
            }
            else {
              tri = "VUE";
            }
            vue = Integer.parseInt(request.getParameter("vue"));
          }
          catch (Exception e) {
            vue = 1;
            e.printStackTrace();
          }
          // attribuer la vue à la gestion clients
          utilisateur.getGestionClients().setVue(vue);
        }
        // Gestion du nombre de lignes des listes
        if ((page != null && page.equals("retour")) || (tri != null && !tri.equals("PLUSLIGNES"))
            || request.getParameter("vue") != null) {
          nbLignes = utilisateur.getGestionClients().getNbLignesMax();
        }
        else if (request.getParameter("plusDeLignes") != null) {
          nbLignes = Integer.parseInt(request.getParameter("plusDeLignes")) + utilisateur.getNbLignesListes();
        }
        
        // ++++++++++++++++++++++++++++++++++++ Traitements ++++++++++++++++++++++++++++++++++++++++
        
        // Gestion des cookies client
        utilisateur.getGestionClients().setListeSelection(gestionCookies(request));
        
        // si on rafraichit la page inititialiser la gestion des articles
        if (page != null && page.equals("refresh")) {
          utilisateur.getGestionClients().initDonnes();
        }
        
        // Changer le niveau du métier vers la fiche article si un code article est passé ou si on change juste de vue
        if (module != null) {
          utilisateur.getGestionClients().setNiveauMetier(3);
        }
        else if (etb != null && idSoc != null && suff != null) {
          utilisateur.getGestionClients().setNiveauMetier(2);
        }
        // si on change juste de vue dans l'affichage d'un client
        else if (utilisateur.getGestionClients().getNiveauMetier() == 2 && request.getParameter("vue") != null) {
          // pas bo pas propre. Le métier a rien à foutre là bordel !! Un message perso de David pour David, je pense
          // qu'il se reconnaitra.
          etb = utilisateur.getGestionClients().getRecordActuel().getField("CLETB").toString().trim();
          idSoc = utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim();
          suff = utilisateur.getGestionClients().getRecordActuel().getField("CLLIV").toString().trim();
          
        }
        // sinon changer le niveau métier vers la liste
        else {
          utilisateur.getGestionClients().setNiveauMetier(1);
        }
        
        // charger le CSS pour la liste de clients
        if (utilisateur.getGestionClients().getNiveauMetier() == 1) {
          metaSpecifiques = "<link href='css/listes.css' rel='stylesheet'/>";
          liensOptions = new String[] { "#", "mailto:", null };
        }
        
        // CSS pour la fiche clients
        else if (utilisateur.getGestionClients().getNiveauMetier() == 2) {
          metaSpecifiques = "<link href='css/fiches.css' rel='stylesheet'/>";
        }
        else {
          metaSpecifiques = "<link href='css/fiches.css' rel='stylesheet'/>";
          metaSpecifiques += "<link href='css/module.css' rel='stylesheet'/>";
          liensOptions = new String[] { "#", "mailto:", null };
        }
        switch (utilisateur.getGestionClients().getTypeVue()) {
          case 1:
            break;
          case 2:
            metaSpecifiques += "<link href='css/encoursClients.css' rel='stylesheet'/>";
            break;
          case 3:
            metaSpecifiques += "<link href='css/contactsClients.css' rel='stylesheet'/>";
            break;
          case 4:
            metaSpecifiques += "<link href='css/devisClients.css' rel='stylesheet'/>";
            break;
          case 5:
            metaSpecifiques += "<link href='css/commandesClients.css' rel='stylesheet'/>";
            break;
          case 6:
            metaSpecifiques += "<link href='css/BLClients.css' rel='stylesheet'/>";
            break;
          case 7:
            metaSpecifiques += "<link href='css/facturesClients.css' rel='stylesheet'/>";
            break;
          case 8:
            metaSpecifiques += "<link href='css/facturesClients.css' rel='stylesheet'/>";
            break;
        }
        
        out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
        out.println(pattern.afficherHeader(ConstantesAffichage.METIER_CLIENTS, utilisateur.getGestionClients().getNiveauMetier(),
            infosRecherche, utilisateur));
        out.println(pattern.afficherOptions(utilisateur, ConstantesAffichage.METIER_CLIENTS, utilisateur.getGestionClients().getTypeVue(),
            utilisateur.getGestionClients().getNiveauMetier(), false));
        // charger la liste ou le détail
        if (utilisateur.getGestionClients().getNiveauMetier() == 1) {
          afficherLesClients(out, utilisateur, recherche, tri, special, page, nbLignes);
          // TODO variabiliser cette liste de string en fonction de la vue
          // afficher le lien qui va afficher la liste
          // liensOptions[1] = "mailto:?subject=liste de clients";
          liensOptions[1] = "#";
        }
        // Afficher le client sélectionné
        else if (utilisateur.getGestionClients().getNiveauMetier() == 2) {
          
          if (request.getParameter("isEnModif") != null && request.getParameter("isEnModif").equals("2")) {
            utilisateur.getGestionClients().traiterDonnees(request.getParameterMap());
          }
          afficherUnClient(out, utilisateur, etb, idSoc, suff, page, options, isEnModif);
          liensOptions[1] = "mailto:?subject=fiche client&body=" + utilisateur.getGestionClients().retournerInfosBrutesFiche();
          liensOptions[2] = "clients?CLETB=" + utilisateur.getGestionClients().getRecordActuel().getField("CLETB").toString().trim()
              + "&CLCLI=" + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim() + "&CLLIV="
              + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV").toString().trim() + "&RENUM="
              + request.getParameter("numberContact") + "&options=favoris";
          
        }
        else {
          // afficherListeContactParClient(utilisateur,idSoc,request);
          afficherDetailModule(out, utilisateur, module, etb, idSoc, suff, page, options, request.getParameterMap());
          liensOptions[1] = "mailto:?subject=fiche client&body=" + utilisateur.getGestionClients().retournerInfosBrutesFiche();
          // liensOptions[2] = "clients?CLETB=" +
          // utilisateur.getGestionClients().getRecordActuel().getField("CLETB").toString().trim() + "&CLCLI=" +
          // utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim() + "&CLLIV=" +
          // utilisateur.getGestionClients().getRecordActuel().getField("CLLIV").toString().trim() + "&options=favoris";
        }
        
        out.println(pattern.afficherFin(titreOptions, liensOptions));
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher le détail du client sélectionné
   * @param out
   * @param etb
   * @param idSoc
   * @param suff
   */
  public void afficherUnClient(ServletOutputStream out, Utilisateur utilisateur, String etb, String idSoc, String suff, String page,
      String options, boolean isEnModif) {
    try {
      boolean isUnFavori = false;
      out.println("<article id='contenu'>");
      out.println("<section class='secContenu' id='ficheMetier'>");
      
      utilisateur.getGestionClients().recupererUnRecord(etb, idSoc, suff);
      // traiter les favoris
      isUnFavori = utilisateur.getGestionClients().isUnFavori(options != null && options.equals("favoris"));
      // out.println(afficherListeContactParClient(utilisateur,idSoc, request));
      
      if (utilisateur.getGestionClients().getRecordActuel() == null) {
        out.println("Le client n'a pas &eacute;t&eacute; trouv&eacute;");
      }
      else {
        // récupérer la vue demandée
        switch (utilisateur.getGestionClients().getTypeVue()) {
          case 1:
            break;
          case 2:
            out.println(utilisateur.getGestionClients().getEncours().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 3:
            out.println(utilisateur.getGestionClients().getContacts().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 4:
            out.println(utilisateur.getGestionClients().getDevis().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 5:
            out.println(utilisateur.getGestionClients().getCommandes().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 6:
            out.println(utilisateur.getGestionClients().getBonsLivr().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 7:
            out.println(utilisateur.getGestionClients().getFactures().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 8:
            out.println(utilisateur.getGestionClients().getFacturesNR().afficherLeRecord(isUnFavori, isEnModif));
            break;
        }
        // out.println(afficherListeContactParClient(utilisateur,idSoc, request));
        out.println(majTailleFiche("ficheMetier"));
        
      }
      out.println("</section>");
      
      out.println("</article>");
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherUnClient()");
    }
  }
  
  /**
   * Afficher le détail du client sélectionné
   * @param out
   * @param etb
   * @param idSoc
   * @param suff
   */
  public void afficherDetailModule(ServletOutputStream out, Utilisateur utilisateur, String module, String etb, String idSoc, String suff,
      String page, String options, Map<String, String[]> mapDonnees) {
    try {
      int intModule = 0;
      
      if (module != null) {
        intModule = Integer.parseInt(module);
      }
      // out.println("<a id='boutonRetourModule' href='javascript:window.history.go(-1)'>Retour</a>");
      out.println("<a id='boutonRetourModule' href='javascript:history.back()'>Retour</a>");
      
      // boolean isUnFavori = false;
      out.println("<article id='contenu'>");
      
      out.println("<section class='secContenu' id='moduleMetier'>");
      // PARAMETRER LE MODUUUUUULE PAS BOOOOOOO !!
      utilisateur.getGestionClients().gestionModule(intModule, mapDonnees);
      
      out.println(utilisateur.getGestionClients().getModuleEnCours().afficherLeRecord());
      
      out.println(majTailleFiche("moduleMetier"));
      
      out.println("</section>");
      
      out.println("</article>");
      
    }
    catch (Exception e) {
      Trace.erreur("Erreur sur afficherDetailModule()");
    }
  }
  
  /**
   * afficher une liste de clients en fonction des paramètres souhaités
   * @param out
   * @param clients
   * @throws IOException
   */
  private void afficherLesClients(ServletOutputStream out, Utilisateur utilisateur, String recherche, String tri, String special,
      String page, int nbLignes) {
    try {
      String apport = "";
      out.println("<article id='contenu'>");
      out.println("<section class='secContenu' id='listeMetier'>");
      
      // Si on tri la liste précédente
      if (tri != null) {
        if (recherche == null) {
          recherche = utilisateur.getGestionClients().getMotCle();
        }
        if (special == null) {
          special = utilisateur.getGestionClients().getRechercheSpeciale();
        }
      }
      // si on est pas en mode tri
      else {
        // si on passe un mot de recherche -> péter la recherche spéciale stockée
        if (recherche != null) {
          special = null;
        }
      }
      
      // calculer une nouvelle liste si on ne retourne pas sur l'ancienne
      if (page == null || !page.equals("retour") || tri != null) {
        // si on récupère les même lignes
        if (tri != null && (tri.equals("MEMELIGNES") || tri.equals("VUE")) && utilisateur.getGestionClients().getListeRecords() != null) {
          utilisateur.getGestionClients().setListeRecords(utilisateur.getGestionClients().recuperationDesMemeRecords(tri));
        }
        else {
          utilisateur.getGestionClients().chercherDesRecords(recherche, tri, special, nbLignes);
        }
      }
      else {
        utilisateur.getGestionClients().setListeRecords(utilisateur.getGestionClients().recuperationDesMemeRecords(tri));
      }
      
      // traiter le texte apport
      if (utilisateur.getGestionClients().getRechercheSpeciale() != null) {
        if (utilisateur.getGestionClients().getRechercheSpeciale().equals("fav")) {
          apport = "Mes favoris";
        }
        if (utilisateur.getGestionClients().getRechercheSpeciale().equals("sel")) {
          apport = "Ma s&eacute;lection";
        }
      }
      
      // affichage des clients par vue demandée
      switch (utilisateur.getGestionClients().getTypeVue()) {
        case 1:
          break;
        case 2:
          out.println(utilisateur.getGestionClients().getEncours().afficherListeRecords(apport, nbLignes));
          break;
        case 3:
          out.println(utilisateur.getGestionClients().getContacts().afficherListeRecords(apport, nbLignes));
          break;
        case 4:
          out.println(utilisateur.getGestionClients().getDevis().afficherListeRecords(apport, nbLignes));
          break;
        case 5:
          out.println(utilisateur.getGestionClients().getCommandes().afficherListeRecords(apport, nbLignes));
          break;
        case 6:
          out.println(utilisateur.getGestionClients().getBonsLivr().afficherListeRecords(apport, nbLignes));
          break;
        case 7:
          out.println(utilisateur.getGestionClients().getFactures().afficherListeRecords(apport, nbLignes));
          break;
        case 8:
          out.println(utilisateur.getGestionClients().getFacturesNR().afficherListeRecords(apport, nbLignes));
          break;
      }
      out.println("</section>");
      
      out.println("</article>");
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherLesClients()");
    }
    
  }
  
  /**
   * récupère les cookies du navigateur pour en récupérer les valeurs correspondantes à notre page
   */
  private String[] gestionCookies(HttpServletRequest request) {
    String[] idClients = null;
    // récupération des cookies du navigateur
    Cookie[] cookies = request.getCookies();
    // trouver notre cookie et récupérer ses valeurs
    for (int i = 0; i < cookies.length; i++) {
      if (cookies[i].getName().equals("selectionClient")) {
        String valeurs = cookies[i].getValue();
        if (valeurs != null && !valeurs.trim().equals("")) {
          idClients = valeurs.split("_");
        }
      }
    }
    
    return idClients;
  }
  
  private String majTailleFiche(String id) {
    String retour = "<script>";
    retour += "majTailleFiche(\"" + id + "\");";
    retour += "</script>";
    
    return retour;
  }
  
  private void afficherClientParContact(ServletOutputStream out, Utilisateur utilisateur, String etb, String idSoc, String suff,
      String page, String options, boolean isEnModif, String numCont) {
    try {
      boolean isUnFavori = false;
      out.println("<article id='contenu'>");
      out.println("<section class='secContenu' id='ficheMetier'>");
      utilisateur.getGestionClients().recupererUnRecord(etb, idSoc, suff);
      // traiter les favoris
      isUnFavori = utilisateur.getGestionClients().isUnFavori(options != null && options.equals("favoris"));
      // out.println(afficherListeContactParClient(utilisateur,idSoc, request));
      
      if (utilisateur.getGestionClients().getRecordActuel() == null) {
        out.println("Le client n'a pas &eacute;t&eacute; trouv&eacute;");
      }
      else {
        // récupérer la vue devis
        // if(utilisateur.getGestionClients().getTypeVue()==4)
        // {
        out.println(utilisateur.getGestionClients().getDevis().afficherLeRecord(isUnFavori, isEnModif));
        // }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherUnClient()");
    }
  }
  
}
