/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

public class FilRouge {
  private int niveauFil = 0;
  private int metier = 0;
  private String denomination = null;
  private String lien = null;
  private String image = null;
  private int niveauAcces = 0;
  
  public FilRouge(int niveau, int ref, String deno, String link, String icone, int niveauA) {
    niveauFil = niveau;
    metier = ref;
    denomination = deno;
    lien = link;
    image = icone;
    niveauAcces = niveauA;
  }
  
  public int getMetier() {
    return metier;
  }
  
  public int getNiveauFil() {
    return niveauFil;
  }
  
  public void setNiveauFil(int niveauFil) {
    this.niveauFil = niveauFil;
  }
  
  public String getDenomination() {
    return denomination;
  }
  
  public String getLien() {
    return lien;
  }
  
  public String getImage() {
    if (image == null) {
      return "";
    }
    else {
      return image;
    }
  }
  
  public int getNiveauAcces() {
    return niveauAcces;
  }
  
  public void setNiveauAcces(int niveauAcces) {
    this.niveauAcces = niveauAcces;
  }
  
}
