/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.Catalogue;
import ri.serien.mobilite.metier.GestionCatalogue;
import ri.serien.mobilite.metier.Metier;
import ri.serien.mobilite.metier.Vue;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class Catalogue
 */
public class AffichageCatalogue extends HttpServlet {
  
  private PatternErgonomie pattern = null;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageCatalogue() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Méthode générique qui traite le GET et le POST
   */
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        int niveau = 1;
        // String[] liensOptions = {"#","#","#"};
        String metaSpecifiques = "";
        String titreOptions = "Options";
        
        // On créé un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        if (utilisateur.getGestionCatalogue() == null) {
          utilisateur.setGestionCatalogue(new GestionCatalogue(utilisateur));
        }
        
        // mot clé de recherche -> recherche stockée
        String catalogue = request.getParameter("catalogue");
        String metier = request.getParameter("metier");
        String changement = request.getParameter("changement");
        // ancrage dans la page
        String ancrage = request.getParameter("ancrage");
        String page = request.getParameter("page");
        if (page != null) {
          if (page.equals("metier")) {
            metier = String.valueOf(utilisateur.getGestionCatalogue().getMetierEnCours().getIdMetier());
          }
          else if (page.equals("catalogue")) {
            catalogue = String.valueOf(utilisateur.getGestionCatalogue().getCatalogueEnCours().getId());
          }
        }
        
        if (catalogue != null) {
          niveau = 2;
        }
        else if (metier != null) {
          niveau = 3;
        }
        
        // Si on est mode AJAX
        if (changement != null) {
          // Changer les données en BDD et sur la liste de l'utilisateur
          utilisateur.getGestionCatalogue().controlerPresenceVue(changement);
          // afficher en LIVE
          response.getWriter().write(pattern.afficherOptions(utilisateur, 0, -1, 0, true));
        }
        // si on est mode normal (pas AJAX)
        else {
          ServletOutputStream out = response.getOutputStream();
          metaSpecifiques += "<script src='scripts/catalogue.js'></script>";
          metaSpecifiques += "<link href='css/catalogues.css' rel='stylesheet'/>";
          metaSpecifiques += "<link href='css/listes.css' rel='stylesheet'/>";
          
          // +++++++++++++++ AFFICHAGE +++++++++++++++++++++++++++
          try {
            out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
            out.println(pattern.afficherHeader(-1, niveau, null, utilisateur));
            out.println(pattern.afficherOptions(utilisateur, 0, -1, 0, false));
            out.println("<article id='contenu'>");
            out.println("<section class='secContenu' id='gestionCatalogue'>");
            
            if (catalogue != null) {
              out.println(afficherUnCatalogue(catalogue, out, utilisateur));
            }
            else if (metier != null) {
              out.println(afficherUnMetier(utilisateur.getGestionCatalogue().getCatalogueEnCours(), metier, out, utilisateur));
            }
            else {
              out.println(afficherLesCatalogues(out, utilisateur));
            }
            
            out.println("</section>");
            out.println("</article>");
            
            out.println(pattern.afficherFin(titreOptions, null));
          }
          catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Permet d'afficher les métiers disponibles dont les vues correspondent au catalogue choisi
   */
  private String afficherUnCatalogue(String catalogue, ServletOutputStream out, Utilisateur utilisateur) {
    ArrayList<Metier> liste = utilisateur.getGestionCatalogue().recupererLesMetiers(catalogue);
    String retour = "";
    
    try {
      retour = "<h1><span class='titreH1'><span class='superflus'>Fiches du </span>"
          + utilisateur.getGestionCatalogue().getCatalogueEnCours().getLibelle().toUpperCase() + "</span></h1>";
      
      if (liste == null || liste.size() == 0) {
        retour += "Pas de vues disponibles pour ce catalogue";
      }
      else {
        for (int i = 0; i < liste.size(); i++) {
          if (i % 2 == 0) {
            retour += "<div class='listesClassiques'>";
          }
          else {
            retour += "<div class='listesClassiquesF'>";
          }
          
          retour += "<a class='labelCatalogue' href='catalogue?metier=" + liste.get(i).getIdMetier()
              + "'><span class='lblCata'><span class='superflus'>Fiche </span>" + liste.get(i).getLabelMetier().toUpperCase()
              + "</span></a>";
          retour += "<a class='iconeCatalogue' href='catalogue?metier=" + liste.get(i).getIdMetier() + "'><img src='"
              + liste.get(i).getIconeMetier() + "' alt='" + liste.get(i).getLabelMetier() + "'/></a>";
          
          retour += "</div>";
        }
      }
      
      return retour;
      
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherUnCatalogue()");
      return null;
    }
  }
  
  /**
   * Permet d'afficher les vues disponibles pour le métier sélectionné
   */
  private String afficherUnMetier(Catalogue catalogue, String metier, ServletOutputStream out, Utilisateur utilisateur) {
    if (catalogue == null || metier == null) {
      return null;
    }
    
    ArrayList<Vue> liste = utilisateur.getGestionCatalogue().recupererLesVues(String.valueOf(catalogue.getId()), metier);
    String retour = "";
    try {
      retour = "<h1><span class='titreH1'><span class='superflus'>Vues </span>"
          + utilisateur.getGestionCatalogue().getMetierEnCours().getLabelMetier().toUpperCase() + "</h1>";
      
      if (liste == null || liste.size() == 0) {
        retour += "Pas de vue disponible";
      }
      else {
        for (int i = 0; i < liste.size(); i++) {
          if (i % 2 == 0) {
            retour += "<div class='listesClassiques'>";
          }
          else {
            retour += "<div class='listesClassiquesF'>";
          }
          
          retour += "<a class='labelVue' href='#'><span class='lblCata'>" + liste.get(i).getLibelle() + "</span></a>";
          retour += "<a class='iconeVue' href='#'><img src='"
              + utilisateur.getGestionCatalogue().accederImageVersionCatalogue(liste.get(i).getUrlIcone()) + "' /></a>";
          retour += "<a class='" + utilisateur.getGestionCatalogue().retournerClasseSelection(liste.get(i).getId())
              + "' href='#' onClick='controlerVues(" + liste.get(i).getId() + ");' id='" + liste.get(i).getId() + "'></a>";
          
          retour += "</div>";
        }
      }
      
      return retour;
      
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherUnMetier()");
      return null;
    }
  }
  
  /**
   * Permet d'afficher la liste des catalogues disponibles pour l'utilisateur
   */
  private String afficherLesCatalogues(ServletOutputStream out, Utilisateur utilisateur) {
    List<Catalogue> liste = utilisateur.getGestionCatalogue().recupererLesCatalogues();
    String retour = "";
    try {
      
      retour = "<h1><span class='titreH1'>Catalogues</h1>";
      
      if (liste == null || liste.size() == 0) {
        retour += "Pas de catalogue disponible";
      }
      else {
        for (int i = 0; i < liste.size(); i++) {
          if (i % 2 == 0) {
            retour += "<div class='listesClassiques'>";
          }
          else {
            retour += "<div class='listesClassiquesF'>";
          }
          
          retour += "<a class='labelCatalogue' href='catalogue?catalogue=" + liste.get(i).getId() + "'><span class='lblCata'>"
              + liste.get(i).getLibelle().toUpperCase() + "</span></a>";
          retour += "<a class='iconeCatalogue' href='catalogue?catalogue=" + liste.get(i).getId() + "'><img src='"
              + liste.get(i).getUrlIcone() + "' alt='" + liste.get(i).getLibelle() + "'/></a>";
          
          retour += "</div>";
        }
      }
      
      return retour;
      
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherLesCatalogue()");
      return null;
    }
  }
}
