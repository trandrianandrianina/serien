/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

public class Metier {
  private int idMetier = 0;
  private String labelMetier = null;
  private String iconeMetier = null;
  
  public Metier(String icone, int id, String label) {
    iconeMetier = icone;
    idMetier = id;
    labelMetier = label;
  }
  
  public int getIdMetier() {
    return idMetier;
  }
  
  public void setIdMetier(int idMetier) {
    this.idMetier = idMetier;
  }
  
  public String getLabelMetier() {
    return labelMetier;
  }
  
  public void setLabelMetier(String labelMetier) {
    this.labelMetier = labelMetier;
  }
  
  public String getIconeMetier() {
    return iconeMetier;
  }
  
  public void setIconeMetier(String iconeMetier) {
    this.iconeMetier = iconeMetier;
  }
  
}
