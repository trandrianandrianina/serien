/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

public class Fichier {
  
  private int fichier_id = 0;
  private String fichier_libelle = null;
  private String fichier_raccou = null;
  private String fichier_descri = null;
  private String fichier_from = null;
  private String fichier_where = null;
  private int fichier_lie = 0;
  
  /**
   * Constructeur Fichier
   */
  public Fichier(int id, String libelle, String raccou, String descri, String from, String where, int fic) {
    fichier_id = id;
    fichier_libelle = libelle;
    fichier_raccou = raccou;
    fichier_descri = descri;
    fichier_from = from;
    fichier_where = where;
    fichier_lie = fic;
  }
  
  public int getFichier_id() {
    return fichier_id;
  }
  
  public void setFichier_id(int fichier_id) {
    this.fichier_id = fichier_id;
  }
  
  public String getFichier_libelle() {
    return fichier_libelle;
  }
  
  public void setFichier_libelle(String fichier_libelle) {
    this.fichier_libelle = fichier_libelle;
  }
  
  public String getFichier_raccou() {
    return fichier_raccou;
  }
  
  public void setFichier_raccou(String fichier_raccou) {
    this.fichier_raccou = fichier_raccou;
  }
  
  public String getFichier_descri() {
    return fichier_descri;
  }
  
  public void setFichier_descri(String fichier_descri) {
    this.fichier_descri = fichier_descri;
  }
  
  public String getFichier_from() {
    return fichier_from;
  }
  
  public void setFichier_from(String fichier_from) {
    this.fichier_from = fichier_from;
  }
  
  public String getFichier_where() {
    return fichier_where;
  }
  
  public void setFichier_where(String fichier_where) {
    this.fichier_where = fichier_where;
  }
  
  public int getFichier_lie() {
    return fichier_lie;
  }
  
  public void setFichier_lie(int fichier_lie) {
    this.fichier_lie = fichier_lie;
  }
  
}
