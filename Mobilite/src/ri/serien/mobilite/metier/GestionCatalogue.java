/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

import java.util.ArrayList;
import java.util.List;

import ri.serien.mobilite.environnement.Utilisateur;

public class GestionCatalogue {
  private Utilisateur utilisateur = null;
  private Metier metierEnCours = null;
  private Catalogue catalogueEnCours = null;
  
  public GestionCatalogue(Utilisateur util) {
    this.utilisateur = util;
  }
  
  /**
   * 
   * */
  public List<Catalogue> recupererLesCatalogues() {
    return utilisateur.getBaseSQLITE().chargerListeCatalogue();
  }
  
  /**
   * Récupérer la liste des métiers dont les vues correspondent au catalogue choisi
   * 
   */
  public ArrayList<Metier> recupererLesMetiers(String catalogue) {
    // attribuer le métier au métier en cours
    if (catalogue == null) {
      return null;
    }
    else {
      catalogueEnCours = utilisateur.getBaseSQLITE().chargerCatalogue(catalogue);
    }
    // appel à la base SQLITE
    return utilisateur.getBaseSQLITE().retournerListeMetiers(catalogue);
  }
  
  /**
   * Récuperer la liste des vues correspondants au métier sélectionné
   * 
   */
  public ArrayList<Vue> recupererLesVues(String catalogue, String metier) {
    // attribuer le métier au métier en cours
    if (metier == null) {
      return null;
    }
    else {
      metierEnCours = utilisateur.getBaseSQLITE().retournerUnMetier(metier);
    }
    // appel à la base SQLITE
    return utilisateur.getBaseSQLITE().retournerListeVues(catalogue, metier);
  }
  
  /**
   * permet de changer dynamiquement la classe de la vue dans la liste afin de voir s'il fait partie de la sélection ou
   * non
   */
  public String retournerClasseSelection(int id) {
    String selection = "selectionVue";
    // controler que la selection de clients ne soit pas vide
    if (utilisateur.getListeVue() != null) {
      int i = 0;
      while (i < utilisateur.getListeVue().size()) {
        // si on trouve changer la classe du client
        if (id == utilisateur.getListeVue().get(i).getId()) {
          selection = "selectionVueF";
          i = utilisateur.getListeVue().size();
        }
        i++;
      }
    }
    
    return selection;
  }
  
  /**
   * permet d'accéder à la version catalogue de l'image demandée
   */
  public String accederImageVersionCatalogue(String image) {
    if (image == null) {
      return null;
    }
    
    image = image.replace(".png", "_cata.png");
    
    return image;
  }
  
  /**
   * contrôler la présence de la vue dans la liste des vues de l'utilisateur et la virer ou la rajouter en fonction
   */
  public boolean controlerPresenceVue(String id) {
    if (id == null) {
      return false;
    }
    
    boolean isOk = false;
    // contrôler la présence de la vue dans la liste des vues de l'utilisateur
    int i = 0;
    int indexOk = -1;
    while (i < utilisateur.getListeVue().size() && indexOk < 0) {
      if (utilisateur.getListeVue().get(i).getId() == Integer.parseInt(id)) {
        indexOk = i;
      }
      i++;
    }
    
    // Si la vue existe dans les vues utilisateur alors la virer
    if (indexOk >= 0) {
      if (utilisateur.getBaseSQLITE().sEnleverUneVue(utilisateur, id)) {
        utilisateur.getListeVue().remove(indexOk);
      }
    }
    // sinon la rajouter au vues
    else {
      if (utilisateur.getBaseSQLITE().seRajouterUneVue(utilisateur, id)) {
        utilisateur.getListeVue().add(utilisateur.getBaseSQLITE().retournerUneVue(Integer.parseInt(id)));
      }
    }
    
    return isOk;
  }
  
  public Metier getMetierEnCours() {
    return metierEnCours;
  }
  
  public void setMetierEnCours(Metier metier) {
    this.metierEnCours = metier;
  }
  
  public Catalogue getCatalogueEnCours() {
    return catalogueEnCours;
  }
  
  public void setCatalogueEnCours(Catalogue catalogue) {
    this.catalogueEnCours = catalogue;
  }
  
  // ++++++++++++++++++++++ GETTERS ET SETTERS ++++++++++++++++++++++
  
}
