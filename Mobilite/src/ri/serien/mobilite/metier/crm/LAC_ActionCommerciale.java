/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.crm;

import java.util.ArrayList;

import ri.serien.libas400.dao.exp.programs.contact.GM_Contact;
import ri.serien.libas400.dao.exp.programs.contact.M_Contact;
import ri.serien.libas400.dao.exp.programs.contact.M_EvenementContact;
import ri.serien.libas400.dao.exp.programs.contact.M_LienEvenementContact;
import ri.serien.libas400.dao.gvm.programs.actioncommerciale.GM_ActionCommerciale;
import ri.serien.libas400.dao.gvm.programs.actioncommerciale.M_ActionCommerciale;
import ri.serien.libas400.dao.gvm.programs.client.GM_Client;
import ri.serien.libas400.dao.gvm.programs.client.M_Client;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.field.FieldAlpha;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.mobilite.environnement.Utilisateur;

public class LAC_ActionCommerciale {
  // Variables
  private int nbrLignesMax = 0;
  protected GM_ActionCommerciale acm = null;
  protected GM_Client clm = null;
  protected GM_Contact cm = null;
  protected Utilisateur utilisateur = null;
  protected QueryManager querymg = null;
  protected M_ActionCommerciale acCourante = null;
  protected M_Client clCourant = null;
  protected M_Client[] listeClients = null;
  protected M_Contact cCourant = null;
  
  private String triOnClients = "CLNOM ASC";
  private String rechOnClients = null;
  
  protected String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Initialisation de l'utilisateur
   * @param autilisateur
   */
  public void setUtilisateur(Utilisateur autilisateur) {
    if (utilisateur == autilisateur) {
      return;
    }
    
    utilisateur = autilisateur;
    querymg = utilisateur.getManager();
    querymg.setLibrary(utilisateur.getBaseDeDonnees());
    // acm = new M_ActionCommercialeManager(utilisateur.getSysteme(), utilisateur.getBibli(), utilisateur.getEtb());
    acm = new GM_ActionCommerciale(querymg);
    clm = new GM_Client(querymg);
    cm = new GM_Contact(querymg);
    
    nbrLignesMax = utilisateur.getNbLignesListes();
    
  }
  
  /**
   * Retourne la liste des clients
   * @return
   * 
   *         public M_Client[] getListeClients()
   *         {
   *         //String requete = "SELECT CLCLI, CLLIV, CLNOM, CLVIL FROM " + utilisateur.getBibli() + ".PGVMCLIM, " +
   *         utilisateur.getBibli() + ".PSEMEVTM WHERE ETIDT = CLCLI AND TRIM(CLNOM) <> '' AND LENGTH(TRIM(CLNOM))>1
   *         ORDER BY CLNOM FETCH FIRST 10 ROWS ONLY ";
   *         /*
   *         String requete = "SELECT CLCLI, CLLIV, CLNOM, CLVIL FROM " + utilisateur.getBibli() + ".PGVMCLIM WHERE
   *         TRIM(CLNOM) <> '' AND LENGTH(TRIM(CLNOM))>1 ORDER BY CLNOM FETCH FIRST 10 ROWS ONLY ";
   *         listeClients = clm.request4ReadClient( requete );
   *         if( listeClients == null)
   *         listeClients = new M_Client[0];
   *         return listeClients;
   * 
   *         return new M_Client[0];
   *         }
   */
  
  /**
   * Retourner la liste des clients pour un critère de recherche
   * TODO à revoir
   */
  public M_Client[] getListeClients(String tri, String recherche) {
    // Gestion de la recherche
    if ((recherche == null) && (rechOnClients == null)) {
      return new M_Client[0];
    }
    if ((recherche != null) && !recherche.trim().equals("")) {
      rechOnClients = recherche.trim().toUpperCase();
    }
    
    // Gestion du tri
    String sens = "ASC";
    if (triOnClients.indexOf("ASC") == -1) {
      sens = "ASC";
    }
    else {
      sens = "DESC";
    }
    if (tri == null) {
      tri = "CLNOM";
    }
    triOnClients = tri + ' ' + sens;
    
    // TODO Traitement propre à la récupération de la requête A MODIFIER CORRECTEMENT
    // A revoir : il faut créer une méthode plus fine et surtout la mettre dans GM_Client
    String requete = "SELECT CLCLI, CLLIV, CLNOM, CLVIL, CLETB FROM " + utilisateur.getBaseDeDonnees() + ".PGVMCLIM WHERE CLNOM LIKE '"
        + FieldAlpha.format4Request(rechOnClients) + "%' ORDER BY " + triOnClients + " FETCH FIRST " + nbrLignesMax + " ROWS ONLY ";
    listeClients = clm.request4ReadClient(requete);
    if ((listeClients == null) || (listeClients.length == 0)) {
      requete = "SELECT CLCLI, CLLIV, CLNOM, CLVIL, CLETB FROM " + utilisateur.getBaseDeDonnees() + ".PGVMCLIM WHERE CLNOM LIKE '%"
          + FieldAlpha.format4Request(rechOnClients) + "%' ORDER BY " + triOnClients + " FETCH FIRST " + nbrLignesMax + " ROWS ONLY ";
      listeClients = clm.request4ReadClient(requete);
    }
    else if ((listeClients == null) || (listeClients.length == 0)) {
      if (triOnClients.startsWith("CLNOM")) {
        triOnClients = "CLVIL ASC";
      }
      requete = "SELECT CLCLI, CLLIV, CLNOM, CLVIL, CLETB FROM " + utilisateur.getBaseDeDonnees() + ".PGVMCLIM WHERE CLVIL LIKE '%"
          + FieldAlpha.format4Request(rechOnClients) + "%' ORDER BY " + triOnClients + " FETCH FIRST " + nbrLignesMax + " ROWS ONLY ";
      listeClients = clm.request4ReadClient(requete);
    }
    if (listeClients == null) {
      listeClients = new M_Client[0];
    }
    
    return listeClients;
  }
  
  /**
   * Retourne la liste des actions commerciales
   * @return
   */
  public M_ActionCommerciale[] getListeActionsCommerciales(boolean forplanning) {
    String cond = null;
    if (forplanning) {
      cond = "ETETA <>  " + M_EvenementContact.EVT_TREATED + " ORDER BY ETDCL, ETHCL FETCH FIRST 10 ROWS ONLY ";
    }
    else {
      cond = "ETIDT = " + clCourant.getCLCLI() + " and ETSUFT = " + clCourant.getCLLIV() + " ORDER BY ETCODP FETCH FIRST 10 ROWS ONLY ";
    }
    M_ActionCommerciale[] lst = acm.readInDatabase(cond);
    if (lst == null) {
      lst = new M_ActionCommerciale[0];
    }
    return lst;
  }
  
  /**
   * Supprime une action commerciale
   * @param idaction
   * @return
   */
  public boolean supprimeActionCom(int id) {
    if (id <= 0) {
      msgErreur += "\nL'id de l'action commerciale à supprimer est incorrecte.";
      return false;
    }
    // On vérifie que ce soit la bonne action
    if (acCourante.getACID() != id) {
      acCourante = retournerUneActionCom(id);
    }
    return acCourante.deleteInDatabase();
  }
  
  /**
   * Prépare une action commerciale vierge
   */
  public void initNewActionCommerciale() {
    if (acCourante == null) {
      acCourante = new M_ActionCommerciale(querymg);
    }
    else {
      acCourante.initialization();
    }
    
    // Information propre à l'action
    acCourante.setListObjectAction(acm.getListObjectAction());
    acCourante.getEvenementContact().setETCODP(M_EvenementContact.PRIORITY_NORMAL);
    acCourante.getEvenementContact().setETOBS("");
    acCourante.getEvenementContact().setETDCR(Integer.parseInt(DateHeure.getJourHeure(DateHeure.SAAMMJJ)));
    acCourante.getEvenementContact().setETHCR(Integer.parseInt(DateHeure.getJourHeure(DateHeure.HHMM)));
    
    // Chargement du client
    acCourante.getEvenementContact().setETIDT(clCourant.getCLCLI());
    acCourante.getEvenementContact().setETSUFT(clCourant.getCLLIV());
    acCourante.getEvenementContact().setETETBT(utilisateur.getEtablissement().getCodeEtablissement());
    acCourante.loadClient(querymg);
    
    // Chargement des contacts
    M_Contact createurAc = utilisateur.getContact();
    createurAc.setCodeTypeContact(M_LienEvenementContact.TYPE_CONTACT[0]);
    acCourante.getListContact().clear();
    acCourante.getListContact().add(createurAc);
  }
  
  /**
   * Méthode qui retourne une action commerciale via son id
   * @param idAction
   * @return
   */
  public M_ActionCommerciale retournerUneActionCom(int idAction) {
    if (idAction < 0) {
      return null;
    }
    
    if (idAction == 0) { // Enregistrement vierge
      initNewActionCommerciale();
    }
    else { // Enregistrement venant de la BD
      M_ActionCommerciale[] lst = acm.readInDatabase("ACID = " + idAction);
      if ((lst != null) && (lst.length > 0)) {
        acCourante = lst[0];
      }
    }
    
    return acCourante;
  }
  
  /**
   * Mise à jour d'un enregistrement d'une action commerciale
   * @param ac
   * @return
   */
  public boolean insertActionCommerciale(M_ActionCommerciale ac) {
    if (ac == null) {
      msgErreur += "\nLa classe M_ActionCommerciale est nulle.";
      return false;
    }
    
    boolean ret = ac.insertInDatabase();
    if (!ret) {
      msgErreur += "\n" + ac.getMsgError();
    }
    return ret;
  }
  
  /**
   * Mise à jour d'un enregistrement d'une action commerciale
   * @param ac
   * @return
   */
  public boolean updateActionCommerciale(M_ActionCommerciale ac) {
    if (ac == null) {
      msgErreur += "\nLa classe M_ActionCommerciale est nulle.";
      return false;
    }
    
    boolean ret = ac.updateInDatabase();
    if (!ret) {
      msgErreur += "\n" + ac.getMsgError();
    }
    return ret;
  }
  
  /**
   * Retourne la liste des contacts pour un client
   * @param idclient
   * @param xclliv
   * @return
   */
  public ArrayList<M_Contact> getListeContactsClient(String idclient, String xclliv, boolean refresh) {
    ArrayList<M_Contact> lst = clCourant.getListContact(refresh);
    return lst;
  }
  
  /**
   * @return le acCourante
   */
  public M_ActionCommerciale getAcCourante(int idaction) {
    if (acCourante == null) {
      return retournerUneActionCom(idaction);
    }
    return acCourante;
  }
  
  /**
   * @return le acCourante
   */
  public M_ActionCommerciale getAcCourante() {
    return acCourante;
  }
  
  /**
   * @param acCourante le acCourante à définir
   */
  public void setAcCourante(M_ActionCommerciale acCourante) {
    this.acCourante = acCourante;
  }
  
  // ------------------------------------------------------------------------
  
  /**
   * @return le clCourant
   */
  public M_Client getClCourant() {
    return clCourant;
  }
  
  /**
   * @param clCourant le clCourant à définir
   */
  public void setClCourant(String aclcli, String aclliv) {
    int clcli = Integer.parseInt(aclcli);
    int clliv = Integer.parseInt(aclliv);
    
    if ((listeClients != null) && (listeClients.length > 0)) {
      for (M_Client client : listeClients) {
        if ((client.getCLCLI() == clcli) && (client.getCLLIV() == clliv)) {
          clCourant = client;
        }
      }
    }
  }
  
  /**
   * @param clCourant le clCourant à définir
   */
  public void setClCourant(M_Client clCourant) {
    this.clCourant = clCourant;
  }
  
  // ------------------------------------------------------------------------
  
  /**
   * Insertion d'un enregistrement d'un contact lié à un client
   * @param c
   * @return
   */
  public boolean insertContact(M_Contact c) {
    if (c == null) {
      msgErreur += "\nLa classe M_Contact est nulle.";
      return false;
    }
    
    boolean ret = cm.insertContact4Client(c, getClCourant());
    if (!ret) {
      msgErreur += "\n" + c.getMsgError();
    }
    return ret;
  }
  
  /**
   * Mise à jour d'un enregistrement d'un contact
   * @param c
   * @return
   */
  public boolean updateContact(M_Contact c) {
    if (c == null) {
      msgErreur += "\nLa classe M_Contact est nulle.";
      return false;
    }
    
    boolean ret = c.updateInDatabase();
    if (!ret) {
      msgErreur += "\n" + c.getMsgError();
    }
    return ret;
  }
  
  /**
   * Supprime un contact ou son lien avec le client
   * @param idaction
   * @return
   */
  public boolean supprimeContact(M_Contact c) {
    if (c == null) {
      msgErreur += "\nLa classe M_Contact est nulle.";
      return false;
    }
    
    return cm.deleteContact4Client(c, clCourant);
  }
  
  /**
   * Méthode qui retourne un contact via son id
   * @param id
   * @return
   */
  public M_Contact retournerUnContact(int id) {
    if (id < 0) {
      return null;
    }
    
    if (id == 0) { // Enregistrement vierge
      initNewContact();
    }
    else { // Enregistrement venant de la BD
      cCourant = cm.readOneContact(id);
    }
    
    return cCourant;
  }
  
  /**
   * Prépare un contact vierge
   */
  public void initNewContact() {
    if (cCourant == null) {
      cCourant = new M_Contact(querymg);
    }
    else {
      cCourant.initialization();
    }
    
    cCourant.setRENUM(0);
    cCourant.setREPAC("");
    cCourant.setRENOM("");
    cCourant.setREPRE("");
    cCourant.setRETEL("");
    cCourant.setRETEL2("");
    cCourant.setREFAX("");
    cCourant.setRENET("");
    cCourant.setRENET2("");
  }
  
  /**
   * @return le clCourant
   */
  public M_Contact getCCourant(int idcontact) {
    if (cCourant == null) {
      return retournerUnContact(idcontact);
    }
    return cCourant;
  }
  
  /**
   * @return le clCourant
   */
  public M_Contact getCCourant() {
    return cCourant;
  }
  
  // ------------------------------------------------------------------------
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
