/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.crm;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.environnement.Utilisateur;

public class GestionItineraire {
  /**
   * Méthode qui retourne les données propre à un itinéraire
   */
  public GenericRecord retournerUnClient(Utilisateur utilisateur, String numClient, String suffixeClient) {
    if (utilisateur == null) {
      return null;
    }
    
    ArrayList<GenericRecord> clients = null;
    if (utilisateur.getManager() != null) {
      // Traitement propre à la récupération de la requête
      clients = utilisateur.getManager()
          .select("SELECT CLCLI, CLLIV, CLNOM, CLCPL, CLRUE, CLLOC, CLCDP1, CLVIL FROM " + utilisateur.getBaseDeDonnees()
              + ".PGVMCLIM WHERE CLETB = '" + utilisateur.getEtablissement().getCodeEtablissement() + "' AND CLCLI = '" + numClient
              + "' AND CLLIV = '" + suffixeClient + "' FETCH FIRST 1 ROWS ONLY ");
    }
    
    if (clients == null || clients.size() == 0) {
      return null;
    }
    
    return clients.get(0);
  }
  
  /**
   * Retourner la liste des clients par défaut
   */
  public ArrayList<GenericRecord> retournerClients(Utilisateur utilisateur) {
    if (utilisateur == null) {
      return null;
    }
    
    ArrayList<GenericRecord> clients = null;
    if (utilisateur.getManager() != null) {
      // TODO Traitement propre à la récupération de la requête A MODIFIER
      clients = utilisateur.getManager().select("SELECT CLCLI, CLLIV, CLNOM FROM " + utilisateur.getBaseDeDonnees()
          + ".PGVMCLIM WHERE TRIM(CLNOM) <> '' ORDER BY CLNOM FETCH FIRST 10 ROWS ONLY ");
    }
    
    return clients;
  }
  
  /**
   * Retourner la liste des clients pour un critère de recherche
   */
  public ArrayList<GenericRecord> retournerClients(Utilisateur utilisateur, String recherche, int nbElementsMax) {
    if (utilisateur == null || recherche == null) {
      return null;
    }
    
    if (recherche.trim().equals("")) {
      return null;
    }
    else {
      recherche = recherche.trim().toUpperCase();
    }
    
    ArrayList<GenericRecord> clients = null;
    if (utilisateur.getManager() != null) {
      // TODO Traitement propre à la récupération de la requête A MODIFIER CORRECTEMENT
      clients = utilisateur.getManager().select("SELECT CLCLI, CLLIV, CLNOM, CLVIL FROM " + utilisateur.getBaseDeDonnees()
          + ".PGVMCLIM WHERE CLNOM LIKE '" + recherche.trim() + "%' ORDER BY CLNOM FETCH FIRST " + nbElementsMax + " ROWS ONLY ");
      if (clients == null || clients.size() == 0) {
        clients = utilisateur.getManager().select("SELECT CLCLI, CLLIV, CLNOM, CLVIL FROM " + utilisateur.getBaseDeDonnees()
            + ".PGVMCLIM WHERE CLNOM LIKE '%" + recherche.trim() + "%' ORDER BY CLNOM FETCH FIRST " + nbElementsMax + " ROWS ONLY ");
      }
      if (clients == null || clients.size() == 0) {
        clients = utilisateur.getManager().select("SELECT CLCLI, CLLIV, CLNOM, CLVIL FROM " + utilisateur.getBaseDeDonnees()
            + ".PGVMCLIM WHERE CLVIL LIKE '%" + recherche.trim() + "%' ORDER BY CLVIL FETCH FIRST " + nbElementsMax + " ROWS ONLY ");
      }
    }
    
    return clients;
  }
}
