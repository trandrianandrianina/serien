/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.constantes.ConstantesAffichage;
import ri.serien.mobilite.constantes.ConstantesMetier;
import ri.serien.mobilite.environnement.Utilisateur;

public class Module extends Specifiques {
  private GenericRecord recordActuel = null;
  private String titreModule = null;
  private String labelModule = null;
  private ArrayList<Zone> matriceZonesFicheModules = null;
  private Map<String, String[]> conditionsWhere = null;
  
  public Module(Utilisateur util) {
    utilisateur = util;
  }
  
  public void majModule(int module, Map<String, String[]> donnees) {
    vue = module;
    conditionsWhere = donnees;
    
    titreModule = utilisateur.getBaseSQLITE().retournerTitreVue(vue);
    labelModule = utilisateur.getBaseSQLITE().retournerLabelVue(vue);
    
    String requete = "SELECT ";
    
    matriceZonesFicheModules = utilisateur.getBaseSQLITE().getChampsFiche(0, module);
    // recupération des champs à sélectionner
    requete += recupererCodeZones(matriceZonesFicheModules);
    
    // récupération du FROM
    Fichier fichierParent = utilisateur.getBaseSQLITE().getFichierParentModule(module);
    requete += recupererFichiersFrom(fichierParent, module, true, true);
    
    // Préparation du WHERE
    requete += " WHERE ";
    int i = 0;
    String and = " ";
    for (Entry<String, String[]> entry : donnees.entrySet()) {
      if (!entry.getKey().equals("module")) {
        if (i > 0) {
          and = " AND ";
        }
        requete += and + entry.getKey() + "= '" + entry.getValue()[0] + "'";
        i++;
      }
    }
    
    ArrayList<GenericRecord> liste = null;
    try {
      liste = utilisateur.getManager().select(requete);
    }
    catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    if (liste != null && liste.size() != 0) {
      recordActuel = liste.get(0);
    }
  }
  
  /**
   * Afficher le record
   */
  public String afficherLeRecord() {
    String retour = "";
    retour += "<h1><span class='titreH1'><span class='superflus'>Détail " + titreModule + "</span></span></h1>";
    retour += "<form action='clients' id='formFiche' method='post'>";
    
    // Gestion de l'état de la fiche et du message d'alerte
    boolean isUnFavori = false;
    boolean chgtBoite = false;
    boolean isGere = true;
    boolean affPasGere = false;
    
    for (int i = 0; i < matriceZonesFicheModules.size(); i++) {
      // Gestion des boites +++++++++++++++++++
      if (i == 0) {
        retour += "<div class='sousBoite' id='deuxBoites1'>";
        retour += "<h2>Infos " + labelModule + "</h2>";
      }
      
      if (matriceZonesFicheModules.get(i).getBloc_zones() == 2 && !chgtBoite) {
        chgtBoite = true;
        retour += "</div>";
        retour += "<div class='sousBoite' id='deuxBoites2'>";
        // A PARAMETRER ABSOOOOLUMENT
        if (isUnFavori) {
          retour += "<h2>Détails client<span id='isUnFavori'></span></h2>";
        }
        else {
          retour += "<h2>Détails client<span id='isPasUnFavori'></span></h2>";
        }
      }
      
      // Gestion du contenu ++++++++++++++++++++
      if (matriceZonesFicheModules.get(i).getBloc_zones() == 1 && !isGere) {
        if (!affPasGere) {
          retour += "<p>Pas de détail pour cette fiche</p>";
          affPasGere = true;
        }
      }
      else {
        // Spécifique
        if (recordActuel != null) {
          retour += retournerTypeZone(recordActuel, matriceZonesFicheModules.get(i), false);
        }
      }
      
      if (i == matriceZonesFicheModules.size() - 1) {
        retour += "</div>";
      }
    }
    
    retour += "</form>";
    retour += afficherSousModule(vue);
    
    return retour;
  }
  
  /**
   * Afficher le module demandé pour un record de cette vue
   */
  public String afficherSousModule(int vue) {
    String retour = "";
    
    int sousModule = utilisateur.getBaseSQLITE().retournerIdSousModule(vue);
    
    // récupérer les zones à afficher
    ArrayList<Zone> matriceZones = utilisateur.getBaseSQLITE().retournerZonesListeModule(vue);
    /*for(int i = 0; i < matriceZones.size(); i ++)
    }*/
    
    String requete = "SELECT ";
    
    requete += recupererCodeZones(matriceZones);
    
    requete += recupererFichiersFrom(utilisateur.getBaseSQLITE().getFichierParentModule(sousModule), sousModule, false, true);
    
    // Préparation du WHERE
    requete += " WHERE ";
    String spec = recupererWhere(utilisateur.getBaseSQLITE().retournerListeFichiers(false,
        utilisateur.getBaseSQLITE().getFichierParentModule(sousModule), sousModule));
    if (spec != null && !spec.trim().equals("")) {
      requete += " " + spec;
    }
    
    for (Entry<String, String[]> entry : conditionsWhere.entrySet()) {
      if (!entry.getKey().equals("module")) {
        requete += " AND " + entry.getKey() + "= '" + entry.getValue()[0] + "'";
      }
    }
    
    ArrayList<GenericRecord> listeModule = null;
    try {
      listeModule = utilisateur.getManager().select(requete);
    }
    catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    // ++++++++++++++++++++++++ A FACTORISER CAR DOUBLON SPECIFIQUE ++++++++++++++++++++++++++++++
    // afficher les données
    if (listeModule != null && listeModule.size() > 0) {
      int i = 0;
      retour = "<div id='module' class='sousBoite'>";
      retour += "<h2>" + utilisateur.getBaseSQLITE().retournerTitreVue(sousModule) + "</h2>";
      
      // Entête de liste de module
      retour += "<div class='enteteListeModule'>";
      for (i = 0; i < matriceZones.size(); i++) {
        if (matriceZones.get(i).getListe_zones() > 0) {
          if (matriceZones.get(i).getZone_type() == ConstantesAffichage.TYPE_TEL) {
            retour += "<div ></div>";
          }
          else if (matriceZones.get(i).getZone_type() == ConstantesAffichage.TYPE_PARA) {
            retour += "<div class='module100'>" + matriceZones.get(i).getZone_libelle() + "</div>";
          }
          else if (matriceZones.get(i).getZone_type() == ConstantesAffichage.TYPE_MAIL) {
            retour += "<div></div>";
          }
          else if (matriceZones.get(i).getZone_type() == ConstantesAffichage.TYPE_DATE) {
            retour += "<div class='moduleTel'>" + matriceZones.get(i).getZone_libelle() + "</div>";
          }
          else if (matriceZones.get(i).getZone_type() == ConstantesAffichage.TYPE_MONTANT
              || matriceZones.get(i).getZone_type() == ConstantesAffichage.TYPE_NUME) {
            retour += "<div style=\"text-align:right;\" class='" + traitementTailleModule(matriceZones.get(i).getZone_long()) + "'>"
                + matriceZones.get(i).getZone_libelle() + "</div>";
          }
          else {
            retour += "<div class='" + traitementTailleModule(matriceZones.get(i).getZone_long()) + "'>"
                + matriceZones.get(i).getZone_libelle() + "</div>";
          }
        }
      }
      retour += "</div>";
      
      // pour chaque enregistrement
      for (i = 0; i < listeModule.size(); i++) {
        if (i % 2 == 0) {
          retour += "<div class='ligneModule'>";
        }
        else {
          retour += "<div class='ligneModuleF'>";
        }
        // afficher les données en fonction de la matrice
        for (int j = 0; j < matriceZones.size(); j++) {
          // matriceZonesModule.get(j).getListe_zones());
          if (matriceZones.get(j).getListe_zones() > 0 && listeModule.get(i).isPresentField(matriceZones.get(j).getZone_code())) {
            if (matriceZones.get(j).getZone_type() == ConstantesAffichage.TYPE_PARA) {
              if (!listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim().equals("")) {
                retour += "<a class='module100' >" + gestionParametres(listeModule.get(i), matriceZones.get(j), false) + "</a>";
              }
              else {
                retour += "<div class='module100'></div>";
              }
            }
            else if (matriceZones.get(j).getZone_type() == ConstantesAffichage.TYPE_TEL) {
              if (!listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim().equals("")) {
                retour += "<a class='modulePhone' ></a>";
              }
              else {
                retour += "<div class='modulePhoneVide'></div>";
              }
            }
            else if (matriceZones.get(j).getZone_type() == ConstantesAffichage.TYPE_MAIL) {
              if (!listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim().equals("")) {
                retour += "<a class='moduleMail' ></a>";
              }
              else {
                retour += "<div class='enTetemoduleMailVide'></div>";
              }
            }
            else if (matriceZones.get(j).getZone_type() == ConstantesAffichage.TYPE_DATE) {
              retour += "<a class='moduleTel'>" + utilisateur.getOutils()
                  .TransformerEnDateHumaine(listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim()) + "</a>";
            }
            else if (matriceZones.get(j).getZone_type() == ConstantesAffichage.TYPE_NUME) {
              retour += "<a style=\"text-align:right;\" class='" + traitementTailleModule(matriceZones.get(j).getZone_long()) + "' >"
                  + gererDecimale(
                      utilisateur.getOutils().gererAffichageNumerique(listeModule.get(i).getField(matriceZones.get(j).getZone_code())))
                  + "</a>"; // listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim()
            }
            else if (matriceZones.get(j).getZone_type() == ConstantesAffichage.TYPE_MONTANT) {
              retour += "<a style=\"text-align:right;\" class='" + traitementTailleModule(matriceZones.get(j).getZone_long()) + "' >"
                  + utilisateur.getOutils().gererAffichageNumerique(listeModule.get(i).getField(matriceZones.get(j).getZone_code())) + " "
                  + ConstantesMetier.MONNAIE + "</a>"; // listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim()
            }
            else {
              retour += "<a class='" + traitementTailleModule(matriceZones.get(j).getZone_long()) + "'  >"
                  + listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim() + "</a>";
            }
          }
        }
        
        retour += "</div>";
      }
      
      retour += "</div>";
    }
    
    return retour;
  }
  
  @Override
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  @Override
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public ArrayList<Zone> getMatriceZonesFicheModules() {
    return matriceZonesFicheModules;
  }
  
  public void setMatriceZonesFicheModules(ArrayList<Zone> matriceZonesFicheModules) {
    this.matriceZonesFicheModules = matriceZonesFicheModules;
  }
  
  public GenericRecord getRecordActuel() {
    return recordActuel;
  }
  
  public void setRecordActuel(GenericRecord recordActuel) {
    this.recordActuel = recordActuel;
  }
  
}
