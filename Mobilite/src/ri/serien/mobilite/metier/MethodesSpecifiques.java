/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

public interface MethodesSpecifiques {
  /**
   * Construire la requête pour récupérer un record
   */
  public void construireRequeteUnRecord();
  
  /**
   * Construire la requête pour récupérer une liste de records
   */
  public void construireRequeteListeRecords();
  
  /**
   * Afficher la liste des records récupérés
   */
  public String afficherListeRecords(String apport, int nbLignes);
  
  /**
   * Récupérer le record récupéré
   */
  public String afficherLeRecord(boolean isUnFavori, boolean isEnModif);
  
}
