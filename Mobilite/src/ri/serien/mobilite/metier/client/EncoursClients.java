/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.client;

import java.math.BigDecimal;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.constantes.ConstantesMetier;
import ri.serien.mobilite.metier.MethodesSpecifiques;
import ri.serien.mobilite.metier.Specifiques;

public class EncoursClients extends Specifiques implements MethodesSpecifiques {
  public EncoursClients() {
    vue = 1;
  }
  
  @Override
  public void construireRequeteListeRecords() {
    // maj des fichiers concernés par cette vue
    mettreAjourFichiersListe(utilisateur.getGestionClients().getFichierParent());
    // maj de la matrice des zones qui sont des ids
    if (matriceZonesIds == null) {
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionClients().getFichierParent(), vue);
    }
    // maj de la matrice des zones de la liste
    if (matriceZonesListe == null) {
      matriceZonesListe = utilisateur.getBaseSQLITE().getChampsListe(utilisateur.getGestionClients().getMetier(), vue);
    }
    
    // maj des zones de clé et de valeurs de la liste
    majCleValeursListe(matriceZonesListe);
    
    // maj des champs de bases nécessaires à cette vue
    utilisateur.getGestionClients().setChampsBase(recupererCodeZones(matriceZonesListe));
    // maj des fichiers attaqués en SQL et leurs jointures
    // utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(listeFichiersListe));
    utilisateur.getGestionClients()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionClients().getFichierParent(), vue, false, true));
    // maj des critéres de tri et de sélection
    // utilisateur.getGestionClients().setCriteresBase(" cl.CLTNS <> 9 " );
    // utilisateur.getGestionClients().setCriteresBase(recupererWhere(utilisateur.getGestionClients().getFichierParent(),vue));
    utilisateur.getGestionClients().setCriteresBase(recupererWhere(listeFichiersListe));
  }
  
  @Override
  public void construireRequeteUnRecord() {
    mettreAjourFichiersFiche(utilisateur.getGestionClients().getFichierParent());
    
    if (matriceZonesIds == null) {
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionClients().getFichierParent(), vue);
    }
    
    if (matriceZonesFiche == null) {
      matriceZonesFiche = utilisateur.getBaseSQLITE().getChampsFiche(utilisateur.getGestionClients().getMetier(), vue);
    }
    
    utilisateur.getGestionClients().setChampsBase(recupererCodeZones(matriceZonesFiche));
    
    // utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(listeFichiersFiche));
    utilisateur.getGestionClients()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionClients().getFichierParent(), vue, true, true));
    
    utilisateur.getGestionClients().setCriteresBase(recupererWhere(listeFichiersFiche));
  }
  
  /**
   * Afficher la liste des clients en vue encours
   */
  @Override
  public String afficherListeRecords(String apport, int nbLignes) {
    if (apport.equals("")) {
      if (utilisateur.getGestionClients().getListeRecords().size() < nbLignes) {
        apport = utilisateur.getGestionClients().getListeRecords().size() + " r&eacute;sultats";
      }
      else {
        apport = "Plus de " + (utilisateur.getGestionClients().getListeRecords().size() - 1) + " r&eacute;sultats";
      }
    }
    String retour = "";
    retour = "<h1 class='tetiereListes'><span class='titreTetiere'>Encours clients</span><span class='apportListe'>" + apport
        + "</span><div class='triTetieres'><a href='clients?tri=" + clePrincipale.getZone_code()
        + "' class='teteListe' id='triClePrincip'>" + clePrincipale.getZone_libelle() + "</a><a href='clients?tri="
        + valeurPrincipale.getZone_code() + "' class='teteListe' id='triValeur'>" + valeurPrincipale.getZone_libelle()
        + "</a></div><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    
    // Si la liste est vide afficher un message
    if (utilisateur.getGestionClients().getListeRecords().size() == 0) {
      retour += "<p class='messageListes'>Pas de client pour ces critères</p>";
    }
    
    boolean isFavori = false;
    String etatFiche = null;
    
    // si la liste n'est pas vide
    for (int i = 0; i < utilisateur.getGestionClients().getListeRecords().size(); i++) {
      // Gestion des favoris
      isFavori = utilisateur.getGestionClients().isUnFavori(utilisateur.getGestionClients().getListeRecords().get(i));
      // Gestion de l'état du client
      etatFiche = gestionEtatFiche(utilisateur.getGestionClients(), utilisateur.getGestionClients().getListeRecords().get(i));
      
      if (i % 2 == 0) {
        retour += "<div class='listesClassiques'>";
      }
      else {
        retour += "<div class='listesClassiquesF'>";
      }
      // si la ligne est inférieur au nombre maximum de lignes
      if (i < nbLignes - 1) {
        retour += "<a id='client" + i + "' href='"
            + retournerLienVersDetail("clients?", utilisateur.getGestionClients().getListeRecords().get(i), matriceZonesIds)
            + "' class='detailsListe'>"
            + retournerStructureAffichageListe(utilisateur.getGestionClients().getListeRecords().get(i), isFavori, etatFiche) + "</a>";
        retour += "<a href='#' class='"
            + utilisateur.getGestionClients().retournerClasseSelection(
                utilisateur.getGestionClients().getListeRecords().get(i).getField("CLETB").toString().trim() + "-"
                    + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLCLI").toString().trim() + "-"
                    + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLLIV").toString().trim(),
                "selectionClient", "selectionClientF")
            + "' onClick=\"switchSelection(this,'selectionClient','"
            + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLETB").toString().trim() + "-"
            + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLCLI").toString().trim() + "-"
            + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLLIV").toString().trim() + "');\"></a>";
      }
      // si il existe plus d'enregistrements que le max affichable
      else {
        retour += "<a id='plusResultats' href='clients?ancrage=client" + i + "&tri=PLUSLIGNES&plusDeLignes=" + nbLignes
            + "'>Afficher plus de résultats</a>";
      }
      
      retour += "</div>";
    }
    return retour;
  }
  
  /**
   * Afficher un client en vue Encours
   */
  @Override
  public String afficherLeRecord(boolean isUnFavori, boolean isEnModif) {
    String retour = "";
    retour += "<h1><span class='titreH1'><span class='superflus'>Encours client: </span> "
        + utilisateur.getGestionClients().getRecordActuel().getField("CLNOM")
        + "</span><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    retour += "<form action='clients' id='formFiche' method='post'>";
    
    // Gestion de l'état de la fiche et du message d'alerte
    String etatFiche = gestionEtatFiche(utilisateur.getGestionClients(), utilisateur.getGestionClients().getRecordActuel());
    if (etatFiche != null) {
      retour += "<div id='alerteFiche'>" + etatFiche + "</div>";
    }
    
    boolean chgtBoite = false;
    boolean gestionEncours = utilisateur.getGestionClients().getRecordActuel().isPresentField("ENCOURS");
    boolean affPasDencours = false;
    
    for (int i = 0; i < matriceZonesFiche.size(); i++) {
      // Gestion des boites +++++++++++++++++++
      if (i == 0) {
        retour += "<div class='sousBoite' id='deuxBoites1'>";
        retour += "<h2>Encours<span id='bonusBoite1'>" + ConstantesMetier.MONNAIE + " / TTC</span></h2>";
      }
      
      if (matriceZonesFiche.get(i).getBloc_zones() == 2 && !chgtBoite) {
        chgtBoite = true;
        retour += "</div>";
        retour += "<div class='sousBoite' id='deuxBoites2'>";
        if (isUnFavori) {
          retour += "<h2>Détails client<span id='isUnFavori'></span></h2>";
        }
        else {
          retour += "<h2>Détails client<span id='isPasUnFavori'></span></h2>";
        }
      }
      
      // Gestion du contenu ++++++++++++++++++++
      if (matriceZonesFiche.get(i).getBloc_zones() == 1 && !gestionEncours) {
        if (!affPasDencours) {
          retour += "<p>Pas de gestion d'encours pour ce client</p>";
          affPasDencours = true;
        }
      }
      // Spécifique
      // n'afficher le dépassement que si il est =! 0
      else if (matriceZonesFiche.get(i).getZone_code().equals("DEPASS")
          && !retournerEtatEncours(utilisateur.getGestionClients().getRecordActuel())) {
        retour += "";
      }
      else {
        // tester si il existe un traitement spécifique et retourner la bonne valeur
        retour += utilisateur.getGestionClients().traitementSpecMetier(this, matriceZonesFiche.get(i), isEnModif);
      }
      
      if (i == matriceZonesFiche.size() - 1) {
        retour += "</div>";
      }
    }
    
    retour += "</form>";
    
    return retour;
  }
  
  /**
   * Methode qui spécifie si l'encours du client dépasse le plafond
   */
  public boolean retournerEtatEncours(GenericRecord client) {
    if (client == null) {
      return false;
    }
    BigDecimal encours = null;
    if (client.getField("ENCOURS") != null && client.getField("CLPLF") != null) {
      
      encours = (BigDecimal) client.getField("ENCOURS");
      if (encours.compareTo((BigDecimal) client.getField("CLPLF")) > 0) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
  
  /**
   * Retourner le dépassement entre l'encours et le plafond
   */
  public BigDecimal retournerDepassementEncours(GenericRecord client) {
    if (retournerEtatEncours(client)) {
      return ((BigDecimal) client.getField("ENCOURS")).subtract((BigDecimal) client.getField("CLPLF"));
    }
    else {
      return BigDecimal.ZERO;
    }
  }
  
}
