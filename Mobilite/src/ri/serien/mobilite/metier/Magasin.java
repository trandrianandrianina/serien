/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.environnement.Utilisateur;

public class Magasin {
  private String codeMagasin = "";
  private String libelleMagasin = "";
  private Utilisateur utilisateur = null;
  
  public Magasin(String code) {
    if (code != null) {
      codeMagasin = code;
    }
    
  }
  
  public Magasin() {
    
  }
  
  public String getCodeMagasin() {
    return codeMagasin;
  }
  
  public void setCodeMagasin(String codeMagasin) {
    this.codeMagasin = codeMagasin;
  }
  
  public String getLibelleMagasin() {
    return libelleMagasin;
  }
  
  public void setLibelleMagasin(String libelleMagasin) {
    this.libelleMagasin = libelleMagasin;
  }
  
  /*//recuperation de tous les stocks magasins
  public String getStockDesMagasins(Utilisateur utilisateur, String article, String etb) {
    
   if (utilisateur == null || article == null) {
    return null;
   }
   //String retour = "";
   
  
   ArrayList<GenericRecord>listeMag = retournerListeDesMagasins(utilisateur);
   ArrayList<GenericRecord> stockTousMagasin = utilisateur.getGestionArticles().retourneStock(etb, article, listeMag);
   
   if (stockTousMagasin != null && stockTousMagasin.size()>0) {
     for (int i=0; i<stockTousMagasin.size();i++) {
       +stockTousMagasin.get(i).getField("STK").toString().trim()) ;
     }
   }
   else
   {
   }
   return retour;
  }
   */
  /**
   * Retournerla liste complete des magasins
   */
  public ArrayList<GenericRecord> retournerListeDesMagasins(Utilisateur utilisateur) {
    if (utilisateur == null) {
      return null;
    }
    String requete = ("Select PARIND, SUBSTR(PARZ2, 1, 24) AS LIBETB from " + utilisateur.getBaseDeDonnees()
        + ".PGVMPARM where PARETB = '" + utilisateur.getEtablissement().getCodeEtablissement() + "' and PARTYP = 'MA' ORDER BY PARIND "
        + "FETCH FIRST 4 ROWS ONLY ");
    
    return utilisateur.getManager().select(requete);
    
  }
  
  /**
   * retourne le stock d'un magasin choisi
   */
  public GenericRecord retourneUnMagasin(Utilisateur utilisateur, String mag) {
    if (utilisateur == null) {
      return null;
    }
    
    String requete =
        ("SELECT PARIND, SUBSTR(PARZ2, 1, 24) AS LIBETB from " + utilisateur.getBaseDeDonnees() + ".PGVMPARM WHERE PARETB = '"
            + utilisateur.getEtablissement().getCodeEtablissement() + "' and PARTYP = 'MA' AND LIBETB = '" + mag + "'");
    
    ArrayList<GenericRecord> unMag = utilisateur.getManager().select(requete);
    
    return unMag.get(0);
  }
}
