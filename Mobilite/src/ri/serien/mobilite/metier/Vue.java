/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

/**
 * Une vue représente une page d'informations métiers accessible à l'utilisateur.
 * 
 * Les vues sont présentées sous forme de boutons dans la page d'accueil de la Mobilité. Toute les vues accessibles à l'utilisateur
 * sont affichées sur la page d'accueil, quelque soit le groupe des vues.
 * 
 * Les vues sont également accessibles sous forme d'onglets (à gauche) lorsqu'une vue est affichée de manière à pouvoir passer d'une vue à
 * l'autre sans repasser par la page d'accueil. Seules les vues appartenant au même groupe de vues sont affichées dans ce cadre. Il faut
 * repasser par l'écran d'accueil si on veut afficher la vue d'un autre groupe.
 * 
 * Les vues accessibles à un utilisateur sont paramétrables.
 * 
 * Vues du groupe clients :
 * - encours client
 * - contacts
 * - devis
 * - commandes
 * - bons de livraison
 * - factures
 * - factures à régler
 * 
 * Vues du groupe articles :
 * - stock disponible
 * - prix de vente
 */
public class Vue {
  private int id = 0;
  private int idGroupeMetier = 0;
  private int code = 0;
  private String libelle = null;
  private String libelleHTML = null;
  private String url = null;
  private String urlIcone = null;
  
  /**
   * Constructeur.
   * @param pIdVue Identifiant de la vue.
   */
  public Vue(int pIdVue) {
    id = pIdVue;
  }
  
  /**
   * Identifiant unique de la vue.
   * @return Identifiant de la vue.
   */
  public int getId() {
    return id;
  }
  
  /**
   * Identifiant du groupe métier de la vue.
   * 
   * Cette information correspond au champ "vue.metier_id" de la base SQLite.
   * Cela permet probablement de ranger la vue dans des groupes de vues correspondant à un "métier".
   * 
   * Il semble y en avoir deux :
   * - le groupe 1 avec les vues : encours cliet, contacts, devis, commandes, bons de livraison, factures et factures à régler.
   * - le groupe 2 avec les vues : stock disponible et prix de vente.
   * 
   * @return Identifiant du groupe métier.
   */
  public int getIdGroupeMetier() {
    return idGroupeMetier;
  }
  
  /**
   * Modifier l'identifiant du groupe métier de la vue.
   * @param pIdGroupeMetier Nouvel identifiant de groupe métier.
   */
  public void setIdGroupeMetier(int pIdGroupeMetier) {
    idGroupeMetier = pIdGroupeMetier;
  }
  
  /**
   * Code de la vue.
   * 
   * Cette information correspond au champ "vue.vue_code" de la base SQLite.
   * Le code vue à l'air de correspondre à l'identifiant d'une vue au sein d'un groupe de vues. L'identifiant 1 n'est jamais attribué,
   * il doit avoir un rôle particulier.
   * 
   * Voici les positions :
   * - Pour le groupe 1 : 2=encours cliet, 3=contacts, 4=devis, 5=commandes, 6=bons de livraison, 7=factures, 8=factures à régler.
   * - Pour le groupe 2 : 2=stock disponible, 3=prix de vente.
   * 
   * @return Code de la vue.
   */
  public int getCode() {
    return code;
  }
  
  /**
   * Modifier le code de la vue.
   * @param Nouveau code de la vue.
   */
  public void setCode(int pCode) {
    code = pCode;
  }
  
  /**
   * Libellé de la vue.
   * 
   * Cette information correspond au champ "vue.vue_name" de la base SQLite. Le champ "vue.vue_name" contient le
   * libellé de la vue au format brut tandis que le champ "vue.vue_label" contient le libellé de la vue formatté en HTML.
   * Suivant le cas, c'est un champ ou l'autre qui est utilisé.
   * 
   * Cela correspond à au libellé affiché dans les boutons ou les onglets.
   * 
   * @return Code de la vue.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé de la vue.
   * @param Nouveau libellé de la vue.
   */
  public void setLibelle(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Libellé HTML de la vue.
   * 
   * Cette information correspond au champ "vue.vue_label" de la base SQLite. Le champ "vue.vue_label" contient le libellé de la vue
   * formatté en HTML tandis que Le champ "vue.vue_name" contient le libellé de la vue au format brut.
   * Suivant le cas, c'est un champ ou l'autre qui est utilisé.
   * 
   * Cela correspond à au libellé affiché dans les boutons ou les onglets.
   * 
   * @return Code de la vue.
   */
  public String getLibelleHTML() {
    // Retourner le libellé standard si le libellé HTML n'est pas renseigné.
    if (libelleHTML == null || libelleHTML.isEmpty()) {
      return libelle;
    }
    return libelleHTML;
  }
  
  /**
   * Modifier le libellé HTML de la vue.
   * @param Nouveau libellé HTML de la vue.
   */
  public void setLibelleHTML(String pLibelleHTML) {
    libelleHTML = pLibelleHTML;
  }
  
  /**
   * URL de la vue.
   * 
   * Cette information correspond au champ "vue.vue_link" de la base SQLite.
   * Cela correspond à l'URL qui est associée au bouton ou à l'onglet de la vue.
   * 
   * Exemples :
   * - pour la vue "Contacts" du groupe 1 : clients?vue=3
   * - pour la vue "Stocks" du groupe 2 : articles?vue=2
   * 
   * @return Lien de la vue.
   */
  public String getUrl() {
    return url;
  }
  
  /**
   * Modifier l'URL de la vue.
   * @param Nouvelle URL de la vue.
   */
  public void setUrl(String pUrl) {
    url = pUrl;
  }
  
  /**
   * URL de l'icône de la vue.
   * 
   * Cette information correspond au champ "vue.vue_icone" de la base SQLite.
   * Cela correspond à l'URL de l'icone qui est affiché dans les boutons et les onglets de la vue.
   * 
   * @return URL de l'icône de la vue.
   */
  public String getUrlIcone() {
    return urlIcone;
  }
  
  /**
   * Modifier l'URL de l'icône de la vue.
   * @param Nouvelle URL de l'icône de la vue.
   */
  public void setUrlIcone(String pUrlIcone) {
    urlIcone = pUrlIcone;
  }
}
