/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.constantes.ConstantesAffichage;
import ri.serien.mobilite.constantes.ConstantesMetier;
import ri.serien.mobilite.environnement.Utilisateur;

public abstract class Specifiques {
  protected Utilisateur utilisateur = null;
  // Contient le détail des zones de la vue concernée en phase de liste
  protected ArrayList<Zone> matriceZonesListe = null;
  // contient le détail des zones de la vue concernée en phase fiche
  protected ArrayList<Zone> matriceZonesFiche = null;
  // contient le détail des zones IDS de la vue concernée
  protected ArrayList<Zone> matriceZonesIds = null;
  // contient le détail des zones de la vue concernée en phase fiche
  protected ArrayList<Zone> matriceZonesModule = null;
  // contient le détail des zones de la vue concernée en phase fiche
  protected ArrayList<Zone> matriceZonesIdsModule = null;
  // liste de fichiers concernés par la vue en phase liste
  protected ArrayList<Fichier> listeFichiersListe = null;
  // liste de fichiers concernés par la vue en phase fiche
  protected ArrayList<Fichier> listeFichiersFiche = null;
  private ArrayList<Magasin> touslesMag = null;
  
  protected Zone clePrincipale = null;
  protected Zone cleSecondaire = null;
  protected Zone valeurPrincipale = null;
  protected Zone valeurSecondaire = null;
  
  protected LinkedHashMap<String, Zone> cleValPriSec = new LinkedHashMap<String, Zone>();
  
  // id de la vue
  protected int vue = 0;
  
  /**
   * Mettre à jour la liste des fichiers concernés par l'affichage de la liste du métier concerné
   */
  public void mettreAjourFichiersListe(Fichier fichierParent) {
    if (listeFichiersListe == null) {
      listeFichiersListe = utilisateur.getBaseSQLITE().retournerListeFichiers(false, fichierParent, vue);
      // si on a récupéré aucun fichier
      if (listeFichiersListe == null) {
        listeFichiersListe = new ArrayList<Fichier>();
      }
      listeFichiersListe.add(0, fichierParent);
    }
  }
  
  /**
   * Mettre à jour la liste des fichiers concernés par l'affichage de la fiche du métier concerné
   */
  public void mettreAjourFichiersFiche(Fichier fichierParent) {
    if (listeFichiersFiche == null) {
      listeFichiersFiche = utilisateur.getBaseSQLITE().retournerListeFichiers(true, fichierParent, vue);
      // si on a récupéré aucun fichier
      if (listeFichiersFiche == null) {
        listeFichiersFiche = new ArrayList<Fichier>();
      }
      listeFichiersFiche.add(0, fichierParent);
    }
  }
  
  /**
   * Retourner l'ensemble des zones à sélectionner dans le SQL
   */
  public String recupererCodeZones(ArrayList<Zone> matrice) {
    if (matrice == null || matrice.size() == 0) {
      return null;
    }
    String retour = null;
    String detailZone = "";
    
    for (int i = 0; i < matrice.size(); i++) {
      if (matrice.get(i).getZone_calcul() != null && !matrice.get(i).getZone_calcul().trim().equals("")) {
        detailZone = matrice.get(i).getZone_calcul() + " AS " + matrice.get(i).getZone_code();
      }
      else {
        detailZone = matrice.get(i).getRaccourciFichier() + "." + matrice.get(i).getZone_code();
      }
      if (retour != null) {
        retour += "," + detailZone;
      }
      else {
        retour = detailZone;
      }
      
      retour = decrypterVariables(retour);
    }
    
    return retour;
  }
  
  /**
   * Recuperer les fichiers et les jointures concernés par les zones appelées
   */
  public String recupererFichiersFrom(Fichier fichierParent, int vue, boolean isFiche, boolean isRacine) {
    if (fichierParent == null || vue == 0) {
      return null;
    }
    String retour = "";
    ArrayList<Fichier> listeEnfants = null;
    
    if (isRacine) {
      retour = " FROM " + utilisateur.getBaseDeDonnees() + "." + fichierParent.getFichier_libelle() + " "
          + fichierParent.getFichier_raccou() + " ";
    }
    
    listeEnfants = utilisateur.getBaseSQLITE().retournerListeFichiers(isFiche, fichierParent, vue);
    
    if (listeEnfants != null && listeEnfants.size() != 0) {
      // lister les enfants de ce fichier type pour extraire les jointures
      for (int i = 0; i < listeEnfants.size(); i++) {
        // retourner la jointure
        retour += " LEFT OUTER JOIN " + utilisateur.getBaseDeDonnees() + "." + listeEnfants.get(i).getFichier_libelle() + " "
            + listeEnfants.get(i).getFichier_raccou();
        retour += " ON " + utilisateur.getBaseSQLITE().retournerJointureFichiers(fichierParent, listeEnfants.get(i), vue);
        
        if (listeEnfants.get(i).getFichier_from() != null && !listeEnfants.get(i).getFichier_from().trim().equals("")) {
          retour += " AND " + listeEnfants.get(i).getFichier_from();
          // retour += " AND " + decrypterVariables(listeEnfants.get(i).getFichier_from());
        }
        
        retour = decrypterVariables(retour);
        
        // chercher de manière récursive s'il a des enfants aussi
        retour += recupererFichiersFrom(listeEnfants.get(i), vue, isFiche, false);
      }
    }
    
    return retour;
  }
  
  /**
   * Récuperer les conditions propres aux fichiers
   */
  public String recupererWhere(ArrayList<Fichier> listeFichiers) {
    String retour = "";
    
    for (int i = 0; i < listeFichiers.size(); i++) {
      if (listeFichiers.get(i).getFichier_where() != null && !listeFichiers.get(i).getFichier_where().trim().equals("")) {
        if (i != 0) {
          retour += " AND ";
        }
        
        retour += listeFichiers.get(i).getFichier_where();
      }
    }
    
    retour = decrypterVariables(retour);
    
    return retour;
  }
  
  /**
   * Remplacer un code spécifique par une variable Java
   */
  private String decrypterVariables(String pCondition) {
    if (pCondition == null) {
      return null;
    }
    if (utilisateur == null) {
      Trace.erreur("Mobilité - L'utilisateur est invalide.");
    }
    
    // TODO mettre un tableau de variables globales en place et le scanner avec le replace
    if (utilisateur.getMagasin() != null) {
      pCondition = pCondition.replaceAll("<code>CODEMAG</code>", "'" + utilisateur.getMagasin().getCodeMagasin() + "'");
    }
    if (utilisateur.getEtablissement() != null) {
      pCondition = pCondition.replaceAll("<code>CODEETB</code>", "'" + utilisateur.getEtablissement().getCodeEtablissement() + "'");
    }
    if (utilisateur.getOutils() != null) {
      pCondition = pCondition.replaceAll("<code>DATEJOUR</code>", utilisateur.getOutils().getDateDuJour());
    }
    if (utilisateur.getBaseDeDonnees() != null) {
      pCondition = pCondition.replaceAll("<code>CODEBIBLI</code>", utilisateur.getBaseDeDonnees());
    }
    pCondition = pCondition.replaceAll("<code>CODEDEVISE</code>", "'" + ConstantesMetier.CODE_DEVISE + "'");
    
    return pCondition;
  }
  
  /**
   * Retourner lien d'IDs du métier qui pointe vers la fiche détail de ce métier
   */
  protected String retournerLienVersDetail(String page, GenericRecord record, ArrayList<Zone> zonesIds) {
    if (page == null || record == null) {
      return null;
    }
    
    String retour = page;
    
    for (int i = 0; i < zonesIds.size(); i++) {
      if (i != 0) {
        retour += "&";
      }
      
      retour += zonesIds.get(i).getZone_code() + "=" + record.getField(zonesIds.get(i).getZone_code()).toString().trim();
    }
    
    return retour;
  }
  
  /**
   * Retourner la structure des données au sein même d'une ligne de la liste
   * *
   * protected String retournerStructureAffichageListe(GenericRecord record)
   * {
   * if(record==null || matriceZonesListe==null)
   * return null;
   * 
   * String retour = "";
   * int j = 0;
   * String donnee = null;
   * String style = "";
   * 
   * for(int i=0; i < matriceZonesListe.size(); i++)
   * {
   * //Si la matrice contient bien des élements de liste
   * if(matriceZonesListe.get(i).getListe_zones()>ConstantesAffichage.PAS_DANS_LISTE && j <
   * ConstantesAffichage.structureListe.length)
   * {
   * //Si le record est bien présent
   * if(record.isPresentField(matriceZonesListe.get(i).getZone_code()))
   * {
   * //Format numérique
   * if(matriceZonesListe.get(i).getZone_type()== ConstantesAffichage.TYPE_NUME || matriceZonesListe.get(i).getZone_type()==
   * ConstantesAffichage.TYPE_MONTANT)
   * {
   * style = " style='text-align:right; '";
   * if(matriceZonesListe.get(i).getZone_type()==ConstantesAffichage.TYPE_NUME)
   * donnee =
   * gererDecimale(utilisateur.getOutils().gererAffichageNumerique(record.getField(matriceZonesListe.get(i).getZone_code())));
   * else
   * donnee = utilisateur.getOutils().gererAffichageNumerique(record.getField(matriceZonesListe.get(i).getZone_code()));
   * }
   * //De type paramètre
   * else if(matriceZonesListe.get(i).getZone_type()==ConstantesAffichage.TYPE_PARA)
   * {
   * style = "";
   * donnee = gestionParametres(record,matriceZonesListe.get(i),false);
   * }
   * //zone "classique" alphanumérique
   * else
   * {
   * style= "";
   * donnee = record.getField(matriceZonesListe.get(i).getZone_code()).toString().trim();
   * }
   * retour+= "<div class='" + ConstantesAffichage.structureListe[j] + "' "+ style + " >" + donnee + "</div>";
   * }
   * //Si le record est vide
   * else
   * {
   * if(matriceZonesListe.get(i).getZone_type()==ConstantesAffichage.TYPE_NUME ||
   * matriceZonesListe.get(i).getZone_type()==ConstantesAffichage.TYPE_MONTANT)
   * style = " style='text-align:right; '";
   * 
   * retour+= "<div class='" + ConstantesAffichage.structureListe[j] + "Vide' "+ style + " >" + ConstantesAffichage.MESSAGE_VIDE
   * + "</div>";
   * //ne pas afficher la valeur secondaire si la valeur principale n'est pas présente
   * if(j==2) j= ConstantesAffichage.structureListe.length;
   * }
   * 
   * j++;
   * }
   * }
   * return retour;
   * }
   */
  
  /**
   * Retourner la structure des données au sein même d'une ligne de la liste
   */
  protected String retournerStructureAffichageListe(GenericRecord record, boolean isFavori, String etatFiche) {
    if (record == null || cleValPriSec.size() == 0) {
      return null;
    }
    
    String retour = "";
    String donnee = null;
    String style = "";
    
    // Si la matrice contient bien des élements de liste
    for (Entry<String, Zone> entry : cleValPriSec.entrySet()) {
      // Si le record est bien présent
      if (record.isPresentField(entry.getValue().getZone_code())) {
        switch (entry.getValue().getZone_type()) {
          case ConstantesAffichage.TYPE_NUME: // Format numérique
            style = " style='text-align:right; '";
            donnee = gererDecimale(utilisateur.getOutils().gererAffichageNumerique(record.getField(entry.getValue().getZone_code())));
            break;
          case ConstantesAffichage.TYPE_MONTANT: // Format numérique
            style = " style='text-align:right; '";
            donnee = utilisateur.getOutils().gererAffichageNumerique(record.getField(entry.getValue().getZone_code()));
            break;
          case ConstantesAffichage.TYPE_PARA: // De type paramètre
            style = "";
            donnee = gestionParametres(record, entry.getValue(), false);
            break;
          default: // zone 'classique' alphanumérique
            style = "";
            donnee = record.getField(entry.getValue().getZone_code()).toString().trim();
            break;
        }
        // on choisit l'affichage sur la clé secondaire: clé secondaire ou état de la fiche ??
        if (entry.getKey().equals(ConstantesAffichage.structureListe[1])) {
          if (etatFiche != null) {
            retour += "<div class='" + entry.getKey() + "' " + style + " ><span class='alerteListe'>" + etatFiche + "</span></div>";
          }
          else {
            retour += "<div class='" + entry.getKey() + "' " + style + " >" + donnee + "</div>";
          }
        }
        else {
          retour += "<div class='" + entry.getKey() + "' " + style + " >" + donnee + "</div>";
        }
      }
      else {
        if (entry.getValue().getZone_type() == ConstantesAffichage.TYPE_NUME
            || entry.getValue().getZone_type() == ConstantesAffichage.TYPE_MONTANT) {
          style = " style='text-align:right; '";
        }
        retour += "<div class='" + entry.getKey() + "Vide' " + style + " >" + ConstantesAffichage.MESSAGE_VIDE + "</div>";
        
        // ne pas afficher la valeur secondaire si la valeur principale n'est pas présente
        if (entry.getKey().equals(ConstantesAffichage.structureListe[2])) {
          break;
        }
      }
      // Gestion des favoris
      if (isFavori) {
        retour += "<div class= 'listeFav'></div>";
      }
      else {
        retour += "<div class= 'listeFavVide'></div>";
      }
    }
    return retour;
  }
  
  /**
   * Retourner le type de la zone
   */
  public String retournerTypeZone(GenericRecord record, Zone zone, boolean isEnModif) {
    String retour = "";
    
    // si on veut que la zone s'affiche
    if (zone.getBloc_zones() != 0) {
      // Gestion paramètres
      if (zone.getZone_type() == ConstantesAffichage.TYPE_PARA) {
        retour = gestionParametres(record, zone, true);
      }
      else if (zone.getZone_type() == ConstantesAffichage.TYPE_DATE) {
        if (record.isPresentField(zone.getZone_code())) {
          retour = "<label style='width:215px;' >" + zone.getZone_libelle() + "</label><input type='text' name='" + zone.getZone_code()
              + "' value='" + utilisateur.getOutils().TransformerEnDateHumaine(record.getField(zone.getZone_code()).toString()) + "' id='"
              + zone.getZone_code() + "'  alt='" + zone.getZone_descri() + "' title='" + zone.getZone_descri() + "' class='"
              + gestionClasse(zone) + "' maxlength='" + zone.getZone_long() + "' alt='" + zone.getZone_descri()
              + "' style='width:95px; text-align:center;' disabled><br/>";
        }
        else {
          retour = "<label style='width:290px;' >Pas de " + zone.getZone_libelle().toLowerCase() + "</label>";
        }
      }
      // Si on a affaire à un numéro de téléphone
      else if (zone.getZone_type() == ConstantesAffichage.TYPE_TEL
          && !record.getField(zone.getZone_code()).toString().trim().equals("")) {
        retour = gestionLabel(zone) + "<a class='telephone' href='tel:" + record.getField(zone.getZone_code()).toString().trim() + "' "
            + gestionStyle(zone) + " >" + record.getField(zone.getZone_code()).toString().trim() + "</a><br/>";
      }
      else if (zone.getZone_type() == ConstantesAffichage.TYPE_MAIL
          && !record.getField(zone.getZone_code()).toString().trim().equals("")) {
        retour = gestionLabel(zone) + "<a class='adresseMail' href='mailto:" + record.getField(zone.getZone_code()).toString().trim()
            + "' " + gestionStyle(zone) + " >" + record.getField(zone.getZone_code()).toString().trim() + "</a><br/>";
      }
      else {
        // Si la zone n'existe pas ou est vide
        if (!record.isPresentField(zone.getZone_code().toString()) || record.getField(zone.getZone_code()).toString().trim().equals("")) {
          retour = gestionLabel(zone) + "<div id='" + zone.getZone_code() + "' class='zonePlaceHolder' alt='" + zone.getZone_descri()
              + "' title='" + zone.getZone_descri() + "' " + gestionStyle(zone) + " >" + gestionValeur(record, zone) + "</div><br/>";
        }
        else {
          retour = gestionLabel(zone) + "<input type='text' name='" + zone.getZone_code() + "' " + gestionValeur(record, zone) + " id='"
              + zone.getZone_code() + "'  alt='" + zone.getZone_descri() + "' title='" + zone.getZone_descri() + "' class='"
              + gestionClasse(zone) + "' maxlength='" + zone.getZone_long() + "' " + gestionStyle(zone) + " "
              + retournerModeAffichage(zone, isEnModif) + " ><br/>";
        }
      }
    }
    else if (zone.getZone_isId() > 0) {
      if (record.isPresentField(zone.getZone_code())) {
        retour = "<input type='hidden' name='" + zone.getZone_code() + "' value='"
            + record.getField(zone.getZone_code()).toString().trim() + "' > ";
      }
    }
    
    return retour;
  }
  
  /**
   * Retourner l'état de la zone en fonction de sa nature et du mode actif
   */
  public String retournerModeAffichage(Zone zone, boolean isEnModif) {
    if (zone.getIs_modif() == 1 && isEnModif) {
      return "";
    }
    else {
      return " disabled ";
    }
  }
  
  /**
   * retourner le label de la zone (type, longueur, taille...)
   */
  public String gestionLabel(Zone zone) {
    String retour = "";
    
    if (zone.getZone_lb_int() == 0) {
      retour = "<label " + gestionStyleLabel(zone) + " >" + zone.getZone_libelle() + "</label>";
    }
    else {
      retour = "<label class='labelsInvisibles' >" + zone.getZone_libelle() + "</label>";
    }
    
    return retour;
  }
  
  /**
   * Retourner la classe de la zone
   */
  public String gestionClasse(Zone zone) {
    String retour = "sortie";
    
    if (zone.getIs_modif() == 1) {
      retour = "entree";
    }
    
    if (zone.getZone_type() == ConstantesAffichage.TYPE_NUME || zone.getZone_type() == ConstantesAffichage.TYPE_MONTANT) {
      retour += "Num";
    }
    
    // si c'est une zone jugée importante ou d'alerte
    if (zone.getMode_aff() == ConstantesAffichage.AFFICH_IMPORT) {
      retour += "Imp";
    }
    else if (zone.getMode_aff() == ConstantesAffichage.AFFICH_ALERTE) {
      retour += "Pb";
    }
    
    return retour;
  }
  
  /**
   * Retourner le valeur d'une zone
   */
  public String gestionValeur(GenericRecord record, Zone zone) {
    String retour = "value=\"";
    
    // si la zone existe
    if (record.isPresentField(zone.getZone_code().toString().trim())) {
      // Format numérique
      if (zone.getZone_type() == ConstantesAffichage.TYPE_NUME || zone.getZone_type() == ConstantesAffichage.TYPE_MONTANT) {
        if (zone.getZone_type() == ConstantesAffichage.TYPE_NUME) {
          retour += gererDecimale(utilisateur.getOutils().gererAffichageNumerique(record.getField(zone.getZone_code())));
        }
        else {
          retour += utilisateur.getOutils().gererAffichageNumerique(record.getField(zone.getZone_code()));
        }
        retour += "\" ";
      }
      else {
        // Gestion placeholder (si le contenu de la zone est vide et qu'il s'agit d'un label interne
        if (record.getField(zone.getZone_code()).toString().trim().equals("")
            && zone.getZone_lb_int() == ConstantesAffichage.LABEL_INTERN) {
          retour = zone.getZone_descri();
        }
        else if (record.getField(zone.getZone_code()).toString().trim().equals("")
            && zone.getZone_lb_int() != ConstantesAffichage.LABEL_INTERN) {
          retour = " &nbsp; ";
          // remplir normalement la zone
        }
        else {
          retour += record.getField(zone.getZone_code()).toString().trim() + "\"";
        }
      }
    }
    else {
      // Cas de la zone Div pour remplacer le placeholder
      retour = zone.getZone_descri();
    }
    
    return retour;
  }
  
  /**
   * Gérer la décimale
   */
  public String gererDecimale(String valeur) {
    valeur = valeur.replaceAll(",00", " ");
    
    return valeur;
  }
  
  /**
   * Retourner le style de la zone: longueur....
   */
  public String gestionStyle(Zone zone) {
    String retour = "";
    
    // longueur d'affichage
    retour += " style='width:" + retournerTailleZone(zone) + "px; ";
    
    // centrage exceptionnel
    if (zone.getZone_type() == ConstantesAffichage.TYPE_DATE || zone.getZone_type() == ConstantesAffichage.TYPE_TEL
        || zone.getZone_type() == ConstantesAffichage.TYPE_GENCOD) {
      retour += " text-align:center; ";
    }
    
    retour += " ' ";
    
    return retour;
  }
  
  /**
   * Retourner le style de la zone: longueur....
   */
  public String gestionStyleLabel(Zone zone) {
    String retour = "";
    
    int taille = 0;
    
    // taille réelle du label
    taille = ConstantesAffichage.LONGUEUR_MAX_ZONES - retournerTailleZone(zone);
    
    retour += " class= 'label" + (taille) + "' ";
    
    return retour;
  }
  
  /**
   * Retourner la taille affichable de la zone
   */
  public int retournerTailleZone(Zone zone) {
    int taille = 0;
    
    // Cas d'une zone de montant
    if (zone.getZone_type() == ConstantesAffichage.TYPE_MONTANT) {
      taille = 110;
    }
    else if (zone.getZone_type() == ConstantesAffichage.TYPE_GENCOD) {
      taille = 120;
    }
    else {
      // Formule de base pour le calcul de taille de zone
      taille = (zone.getZone_long() * 10 + 10);
      
      // formatage d'affichage uniforme
      if (taille > 70 && taille <= 110) {
        taille = 110;
      }
      else if (taille > 100 && taille <= 210) {
        taille = 210;
      }
      if (taille > 210) {
        taille = ConstantesAffichage.LONGUEUR_MAX_ZONES;
      }
    }
    return taille;
  }
  
  /**
   * Retourner les libellés spécifiques aux paramètres
   */
  protected String gestionParametres(GenericRecord record, Zone zone, boolean fiche) {
    String retour = null;
    
    if (zone == null) {
      return null;
    }
    
    // Gestion famille
    if (zone.getZone_code().equals(ConstantesMetier.PARAM_FAM)) {
      try {
        if (fiche) {
          retour = "<label class='label70'>Famille</label><input type='text' name='A1FAM' style='width:240px' value=\""
              + utilisateur.getFparametre()
                  .getRecordsByType_Code(utilisateur.getEtablissement().getId(), "FA", record.getField("A1FAM").toString().trim()).get(0)
                  .getField(5).toString().trim()
              + "\" id='A1FAM' class='sortie' maxlength='20' disabled><br/>";
        }
        else {
          retour = utilisateur.getFparametre()
              .getRecordsByType_Code(utilisateur.getEtablissement().getId(), "FA", record.getField("A1FAM").toString().trim()).get(0)
              .getField(5).toString().trim();
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "ECHEC recup A1FAM");
      }
    }
    // Gestion Unité
    else if (zone.getZone_code().equals(ConstantesMetier.PARAM_UNIT)) {
      try {
        if (fiche) {
          retour = "<label class='label70'>Unit&eacute;</label><input type='text' name='A1UNS' style='width:240px' value=\""
              + utilisateur.getFparametre()
                  .getRecordsByType_Code(utilisateur.getEtablissement().getId(), "UN", record.getField("A1UNS").toString().trim()).get(0)
                  .getField(5).toString().trim()
              + "\" id='A1UNS' class='sortie' maxlength='20' disabled><br/>";
        }
        else {
          IdEtablissement idEtablissement = IdEtablissement.getInstancePourEtablissementVide();
          retour = utilisateur.getFparametre().getRecordsByType_Code(idEtablissement, "UN", record.getField("A1UNS").toString().trim())
              .get(0).getField(5).toString().trim();
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "ECHEC recup A1UNS");
      }
      
    }
    else if (zone.getZone_code().equals(ConstantesMetier.PARAM_UNIT_L)) {
      try {
        if (fiche) {
          retour = "<label class='label70'>Unit&eacute;</label><input type='text' name='" + ConstantesMetier.PARAM_UNIT_L
              + "' style='width:240px' value=\""
              + utilisateur.getFparametre()
                  .getRecordsByType_Code(utilisateur.getEtablissement().getId(), "UN",
                      record.getField(ConstantesMetier.PARAM_UNIT_L).toString().trim())
                  .get(0).getField(5).toString().trim()
              + "\" id='" + ConstantesMetier.PARAM_UNIT_L + "' class='sortie' maxlength='20' disabled><br/>";
        }
        else {
          IdEtablissement idEtablissement = IdEtablissement.getInstancePourEtablissementVide();
          retour = utilisateur.getFparametre()
              .getRecordsByType_Code(idEtablissement, "UN", record.getField(ConstantesMetier.PARAM_UNIT_L).toString().trim()).get(0)
              .getField(5).toString().trim();
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "ECHEC recup L1UNS");
      }
      
    }
    // Gestion catégorie utilisateur
    else if (zone.getZone_code().equals(ConstantesMetier.PARAM_RECAT)) {
      try {
        if (fiche) {
          retour = "<label class='label270'>Fonction contact</label><input type='text' name='RECAT' style='width:40px' value=\""
              + record.getField("RECAT") + "\"  class='sortie' maxlength='3' disabled><br/>";
          // retour = "<label class='label70'>Fonction</label><input type='text' name='RECAT' style='width:240px'
          // value=\"" + utilisateur.getFparametre().getRecordsByType_Code(utilisateur.getEtb(), "RECAT",
          // record.getField("RECAT").toString().trim()).get(0).getField(5).toString().trim() +"\" id='RECAT'
          // class='sortie' maxlength='20' disabled><br/>";
          // else retour = utilisateur.getFparametre().getRecordsByType_Code("", "RECAT",
          // record.getField("RECAT").toString().trim()).get(0).getField(5).toString().trim();
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "ECHEC recup RECAT");
      }
      
    }
    // Gestion Devise
    else if (zone.getZone_code().equals(ConstantesMetier.PARAM_DEV_T) || zone.getZone_code().equals(ConstantesMetier.PARAM_DEV_C)) {
      // TODO prendre en compte rééllement la devise (Fichier PSEMDEVM )
      if (!fiche) {
        retour = ConstantesMetier.MONNAIE;
      }
    }
    
    return retour;
    
  }
  
  /**
   * Mettre à jour les clés/valeurs de la liste sur la base des zones de la liste
   * *
   * public void majCleValeursListe(ArrayList<Zone> matrice)
   * {
   * if(matrice==null) return;
   * 
   * for(int i =0; i < matrice.size(); i++)
   * {
   * if(matrice.get(i).getListe_zones()== ConstantesAffichage.CLE_PRIMAIRE)
   * clePrincipale = matrice.get(i);
   * else if(matrice.get(i).getListe_zones()== ConstantesAffichage.CLE_SECOND)
   * cleSecondaire = matrice.get(i);
   * else if(matrice.get(i).getListe_zones()== ConstantesAffichage.VAL_PRIMAIRE)
   * valeurPrincipale = matrice.get(i);
   * else if(matrice.get(i).getListe_zones()== ConstantesAffichage.VAL_SECOND)
   * valeurSecondaire = matrice.get(i);
   * }
   * }
   */
  
  /**
   * Mettre à jour les clés/valeurs de la liste sur la base des zones de la liste
   */
  public void majCleValeursListe(ArrayList<Zone> matrice) {
    if (matrice == null) {
      return;
    }
    
    cleValPriSec.clear();
    for (int i = 0; i < matrice.size(); i++) {
      if (matrice.get(i).getListe_zones() == ConstantesAffichage.CLE_PRIMAIRE) {
        clePrincipale = matrice.get(i);
        cleValPriSec.put(ConstantesAffichage.structureListe[0], clePrincipale);
      }
      else if (matrice.get(i).getListe_zones() == ConstantesAffichage.CLE_SECOND) {
        cleSecondaire = matrice.get(i);
        cleValPriSec.put(ConstantesAffichage.structureListe[1], cleSecondaire);
      }
      else if (matrice.get(i).getListe_zones() == ConstantesAffichage.VAL_PRIMAIRE) {
        valeurPrincipale = matrice.get(i);
        cleValPriSec.put(ConstantesAffichage.structureListe[2], valeurPrincipale);
      }
      else if (matrice.get(i).getListe_zones() == ConstantesAffichage.VAL_SECOND) {
        valeurSecondaire = matrice.get(i);
        cleValPriSec.put(ConstantesAffichage.structureListe[3], valeurSecondaire);
      }
    }
  }
  
  /**
   * Afficher le module demandé pour un record de cette vue
   */
  public String afficherLeModule(String metier, GenericRecord record, String titre, String requete) {
    String retour = "";
    ArrayList<GenericRecord> listeModule = null;
    // récupérer les zones à afficher
    matriceZonesModule = utilisateur.getBaseSQLITE().retournerZonesListeModule(vue);
    // récuperer les données
    if (matriceZonesModule != null && matriceZonesModule.size() > 0) {
      // Gestion des matrice d'IDS du module
      matriceZonesIdsModule = new ArrayList<Zone>();
      for (int i = 0; i < matriceZonesModule.size(); i++) {
        if (matriceZonesModule.get(i).getZone_isId() > 0) {
          matriceZonesIdsModule.add(matriceZonesModule.get(i));
        }
      }
      
      requete = "SELECT " + recupererCodeZones(matriceZonesModule) + " " + requete;
      
      try {
        listeModule = utilisateur.getManager().select(requete);
      }
      catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    
    // afficher les données
    if (listeModule != null && listeModule.size() > 0) {
      retour = "<div id='module' class='sousBoite'>";
      retour += "<h2>Les " + ConstantesAffichage.NB_LIGNES_MODULES + " " + titre + "</h2>";
      
      // Entête de liste de module
      retour += "<div class='enteteListeModule'>";
      for (int i = 0; i < matriceZonesModule.size(); i++) {
        if (matriceZonesModule.get(i).getListe_zones() > 0) {
          if (matriceZonesModule.get(i).getZone_type() == ConstantesAffichage.TYPE_TEL) {
            retour += "<div ></div>";
          }
          else if (matriceZonesModule.get(i).getZone_type() == ConstantesAffichage.TYPE_MAIL) {
            retour += "<div></div>";
          }
          else if (matriceZonesModule.get(i).getZone_type() == ConstantesAffichage.TYPE_DATE) {
            retour += "<div class='moduleTel'>" + matriceZonesModule.get(i).getZone_libelle() + "</div>";
          }
          else if (matriceZonesModule.get(i).getZone_type() == ConstantesAffichage.TYPE_MONTANT
              || matriceZonesModule.get(i).getZone_type() == ConstantesAffichage.TYPE_NUME) {
            retour += "<div style=\"text-align:right;\" class='" + traitementTailleModule(matriceZonesModule.get(i).getZone_long()) + "'>"
                + matriceZonesModule.get(i).getZone_libelle() + "</div>";
          }
          else {
            retour += "<div class='" + traitementTailleModule(matriceZonesModule.get(i).getZone_long()) + "'>"
                + matriceZonesModule.get(i).getZone_libelle() + "</div>";
          }
        }
      }
      retour += "</div>";
      
      // pour chaque enregistrement
      for (int i = 0; i < listeModule.size(); i++) {
        if (i % 2 == 0) {
          retour += "<div class='ligneModule'>";
        }
        else {
          retour += "<div class='ligneModuleF'>";
        }
        // afficher les données en fonction de la matrice
        for (int j = 0; j < matriceZonesModule.size(); j++) {
          // matriceZonesModule.get(j).getListe_zones());
          if (matriceZonesModule.get(j).getListe_zones() > 0
              && listeModule.get(i).isPresentField(matriceZonesModule.get(j).getZone_code())) {
            if (matriceZonesModule.get(j).getZone_type() == ConstantesAffichage.TYPE_TEL) {
              if (!listeModule.get(i).getField(matriceZonesModule.get(j).getZone_code()).toString().trim().equals("")) {
                retour += "<a class='modulePhone' href='tel:"
                    + listeModule.get(i).getField(matriceZonesModule.get(j).getZone_code()).toString().trim() + "' ></a>";
              }
              else {
                retour += "<div class='modulePhoneVide'></div>";
              }
            }
            else if (matriceZonesModule.get(j).getZone_type() == ConstantesAffichage.TYPE_MAIL) {
              if (!listeModule.get(i).getField(matriceZonesModule.get(j).getZone_code()).toString().trim().equals("")) {
                retour += "<a class='moduleMail' href='mailto:"
                    + listeModule.get(i).getField(matriceZonesModule.get(j).getZone_code()).toString().trim() + "'></a>";
              }
              else {
                retour += "<div class='enTetemoduleMailVide'></div>";
              }
            }
            else if (matriceZonesModule.get(j).getZone_type() == ConstantesAffichage.TYPE_DATE) {
              retour += "<a class='moduleTel' href='"
                  + retournerLienVersDetail(metier, listeModule.get(i), matriceZonesIdsModule) + "'>" + utilisateur.getOutils()
                      .TransformerEnDateHumaine(listeModule.get(i).getField(matriceZonesModule.get(j).getZone_code()).toString().trim())
                  + "</a>";
            }
            else if (matriceZonesModule.get(j).getZone_type() == ConstantesAffichage.TYPE_NUME) {
              retour +=
                  "<a style=\"text-align:right;\" class='" + traitementTailleModule(matriceZonesModule.get(j).getZone_long()) + "' href='"
                      + retournerLienVersDetail(metier, listeModule.get(i), matriceZonesIdsModule) + "'>" + gererDecimale(utilisateur
                          .getOutils().gererAffichageNumerique(listeModule.get(i).getField(matriceZonesModule.get(j).getZone_code())))
                      + "</a>"; // listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim()
            }
            else if (matriceZonesModule.get(j).getZone_type() == ConstantesAffichage.TYPE_MONTANT) {
              retour += "<a style=\"text-align:right;\" class='" + traitementTailleModule(matriceZonesModule.get(j).getZone_long())
                  + "' href='" + retournerLienVersDetail(metier, listeModule.get(i), matriceZonesIdsModule) + "'>"
                  + utilisateur.getOutils().gererAffichageNumerique(listeModule.get(i).getField(matriceZonesModule.get(j).getZone_code()))
                  + " " + ConstantesMetier.MONNAIE + "</a>"; // listeModule.get(i).getField(matriceZones.get(j).getZone_code()).toString().trim()
            }
            else {
              retour += "<a class='" + traitementTailleModule(matriceZonesModule.get(j).getZone_long()) + "' href='"
                  + retournerLienVersDetail(metier, listeModule.get(i), matriceZonesIdsModule) + "'>"
                  + listeModule.get(i).getField(matriceZonesModule.get(j).getZone_code()).toString().trim() + "</a>";
            }
          }
        }
        
        retour += "</div>";
      }
      
      retour += "</div>";
    }
    
    return retour;
  }
  
  /**
   * retourner le style WIDTH d'une zone de module à partir de sa taille
   */
  protected String traitementTailleModule(int longueur) {
    String retour = "module";
    
    longueur = longueur * 10;
    
    if (longueur > 300) {
      longueur = 300;
    }
    
    retour += longueur;
    
    return retour;
  }
  
  /**
   * Retourner l'état de la fiche métier (interdit, annulé, bloqué...)
   */
  public String gestionEtatFiche(Gestion gestion, GenericRecord record) {
    String retour = null;
    String cle = null;
    
    if (record.isPresentField(gestion.getZoneEtatFiche().getZone_code())) {
      cle = record.getField(gestion.getZoneEtatFiche().getZone_code()).toString().trim();
      
      for (Entry<String, String> entry : gestion.getTousEtatsFiches().entrySet()) {
        if (entry.getKey().equals(cle)) {
          retour = entry.getValue();
        }
        break;
      }
    }
    
    return retour;
  }
  
  // ++++++++++++++++++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++++++++++++++++++++
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public int getVue() {
    return vue;
  }
  
  public void setVue(int vue) {
    this.vue = vue;
  }
  
  public ArrayList<Zone> getMatriceZonesListe() {
    return matriceZonesListe;
  }
  
  public void setMatriceZonesListe(ArrayList<Zone> matriceZonesListe) {
    this.matriceZonesListe = matriceZonesListe;
  }
  
  public ArrayList<Zone> getMatriceZonesFiche() {
    return matriceZonesFiche;
  }
  
  public void setMatriceZonesFiche(ArrayList<Zone> matriceZonesFiche) {
    this.matriceZonesFiche = matriceZonesFiche;
  }
  
  public ArrayList<Fichier> getListeFichiersListe() {
    return listeFichiersListe;
  }
  
  public void setListeFichiersListe(ArrayList<Fichier> listeFichiersListe) {
    this.listeFichiersListe = listeFichiersListe;
  }
  
  public ArrayList<Fichier> getListeFichiersFiche() {
    return listeFichiersFiche;
  }
  
  public void setListeFichiersFiche(ArrayList<Fichier> listeFichiersFiche) {
    this.listeFichiersFiche = listeFichiersFiche;
  }
  
  public ArrayList<Zone> getMatriceZonesIds() {
    return matriceZonesIds;
  }
  
  public void setMatriceZonesIds(ArrayList<Zone> matriceZonesIds) {
    this.matriceZonesIds = matriceZonesIds;
  }
  
  public ArrayList<Magasin> getTouslesMag() {
    return touslesMag;
  }
  
  public void setTouslesMag(ArrayList<Magasin> touslesMag) {
    this.touslesMag = touslesMag;
  }
  
}
