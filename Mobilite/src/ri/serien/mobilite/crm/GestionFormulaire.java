/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.crm;

public class GestionFormulaire {
  // Constantes
  public static final byte CONSULTATION = 0;
  public static final byte CREATION = 1;
  public static final byte MODIFICATION = 2;
  public static final byte VALIDATION = 3;
  public static final byte SUPPRESSION = 4;
  public static final byte ANNULATION = 5;
  
  // Variables
  private byte modeFormulaire = CONSULTATION;
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Retourne l'état pour un objet graphique en fonction de mode en cours
   * @return
   */
  public String getState4OGfxbject() {
    switch (modeFormulaire) {
      case CONSULTATION:
        return "disabled"; // "disabled=\"disabled\"";
      case MODIFICATION:
        return "";
      case CREATION:
        return "";
      case SUPPRESSION:
        return "";
      case ANNULATION:
        return "";
      
      default:
        return "disabled"; // "disabled=\"disabled\"";
    }
  }
  
  /**
   * Initialise l'etat du formulaire
   * @return
   */
  public void setState(String aetat) {
    if (aetat == null) {
      activeConsultation();
      return;
    }
    
    int etat = Integer.parseInt(aetat);
    switch (etat) {
      case CONSULTATION:
        activeConsultation();
        break;
      case MODIFICATION:
        activeModification();
        break;
      case CREATION:
        activeCreation();
        break;
      case VALIDATION:
        activeValidation();
        break;
      case SUPPRESSION:
        activeSuppression();
        break;
      case ANNULATION:
        activeAnnulation();
        break;
      default:
        activeConsultation();
    }
  }
  
  public boolean isConsultation() {
    return modeFormulaire == CONSULTATION;
  }
  
  public boolean isModification() {
    return modeFormulaire == MODIFICATION;
  }
  
  public boolean isCreation() {
    return modeFormulaire == CREATION;
  }
  
  public boolean isValidation() {
    return modeFormulaire == VALIDATION;
  }
  
  public boolean isSuppression() {
    return modeFormulaire == SUPPRESSION;
  }
  
  public boolean isAnnulation() {
    return modeFormulaire == ANNULATION;
  }
  
  public void activeConsultation() {
    modeFormulaire = CONSULTATION;
  }
  
  public void activeModification() {
    modeFormulaire = MODIFICATION;
  }
  
  public void activeCreation() {
    modeFormulaire = CREATION;
  }
  
  public void activeValidation() {
    modeFormulaire = VALIDATION;
  }
  
  public void activeSuppression() {
    modeFormulaire = SUPPRESSION;
  }
  
  public void activeAnnulation() {
    modeFormulaire = ANNULATION;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le modeFormulaire
   */
  public byte getModeFormulaire() {
    return modeFormulaire;
  }
  
  /**
   * @param modeFormulaire le modeFormulaire à définir
   */
  public void setModeFormulaire(byte modeFormulaire) {
    this.modeFormulaire = modeFormulaire;
  }
  
}
