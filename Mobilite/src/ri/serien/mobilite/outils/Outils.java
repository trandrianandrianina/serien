/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.outils;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.mobilite.environnement.Utilisateur;

/**
 * Cette classe est une classe d'outils génériques à toutes les classes
 */
public class Outils {
  String separateurDate = "/";
  private Utilisateur utilisateur = null;
  // A filer ultérieurement à la classe responsable de ces valeurs
  private char separateurMiliers;
  private int nombreDecimales;
  
  /**
   * Constructeur recevant l'utilisateur
   */
  public Outils(Utilisateur gudule) {
    utilisateur = gudule;
    // En attendant qu'on récupère les paramètres suivants via l'utilisateur
    separateurMiliers = ' ';
    nombreDecimales = 2;
  }
  
  /**
   * Met en forme une date pourrie (numérique) en date humainement lisible
   */
  public String TransformerEnDateHumaine(String datePourrie) {
    String retour = "";
    if (datePourrie == null) {
      return retour;
    }
    
    // date pourrave du siècle dernier
    if (datePourrie.length() == 6) {
      // récupération du jour
      retour = datePourrie.substring(4, 6) + separateurDate;
      
      // récupération du mois
      retour += datePourrie.substring(2, 4) + separateurDate;
      
      retour += "19" + datePourrie.substring(0, 2);
    }
    // mise à jour avec le siècle
    else if (datePourrie.length() == 7) {
      // récupération du jour
      retour = datePourrie.substring(5, 7) + separateurDate;
      
      // récupération du mois
      retour += datePourrie.substring(3, 5) + separateurDate;
      
      // siècle si 1 = 2000 si 0 = 1900
      String chaine = datePourrie.substring(0, 1);
      if (chaine.equals("1")) {
        chaine = "20";
      }
      else {
        chaine = "19";
      }
      
      retour += chaine;
      
      // année
      retour += datePourrie.substring(1, 3);
    }
    
    return retour;
  }
  
  /**
   * gestion du neméro client
   */
  public String affichageNumeroClient(String clcli) {
    String retour = "";
    if (clcli == null) {
      return retour;
    }
    
    if (clcli.length() == 6) {
      clcli = clcli + "000";
    }
    else if (clcli.length() == 5) {
      clcli = "0" + clcli + "000";
    }
    else if (clcli.length() == 4) {
      clcli = "00" + clcli + "000";
    }
    else if (clcli.length() == 3) {
      clcli = "000" + clcli + "000";
    }
    else if (clcli.length() == 2) {
      clcli = "0000" + clcli + "000";
    }
    else {
      clcli = "00000" + clcli + "000";
    }
    
    return clcli;
    
  }
  
  /**
   * Afficher une valeur décimale (stock, conditionnement) correctement HORS PRIX
   */
  public String afficherValeurCorrectement(String donnee) {
    if (donnee == null || donnee.trim().equals("")) {
      return null;
    }
    String retour = donnee;
    
    if (donnee.endsWith(".000000")) {
      donnee = donnee.replaceAll(".000000", "");
    }
    else if (donnee.endsWith(".00000")) {
      donnee = donnee.replaceAll(".00000", "");
    }
    else if (donnee.endsWith(".0000")) {
      donnee = donnee.replaceAll(".0000", "");
    }
    else if (donnee.endsWith(".000")) {
      donnee = donnee.replaceAll(".000", "");
    }
    else if (donnee.endsWith(".00")) {
      donnee = donnee.replaceAll(".00", "");
    }
    else if (donnee.endsWith(".0")) {
      donnee = donnee.replaceAll(".0", "");
    }
    retour = donnee;
    
    return retour;
  }
  
  /**
   * Pour tout champs, renvoit un string formaté avec miliers séparé par le symbole en paramètre 1, et nombre de
   * décimales fixé par le paramètre 2
   * il faudra prendre ces deux informations dans le paramétrage de l'application quand il existera.
   */
  public String gererAffichageNumerique(Object monField, char separateurMiliers, int nombreDecimales) {
    AffichageNumerique an = new AffichageNumerique(monField);
    return an.putMiseEnForme(separateurMiliers, nombreDecimales);
  }
  
  public String gererAffichageNumerique(Object monField, int nombreDecimales) {
    AffichageNumerique an = new AffichageNumerique(monField);
    return an.putMiseEnForme(separateurMiliers, nombreDecimales);
  }
  
  /**
   * en attendant de variabiliser on utilise la redefinition ci-dessous
   */
  public String gererAffichageNumerique(Object monField) {
    AffichageNumerique an = new AffichageNumerique(monField);
    return an.putMiseEnForme(separateurMiliers, nombreDecimales);
  }
  
  /**
   * Retourner la date du jour siècle/année/mois/jour
   */
  public String getDateDuJour() {
    SimpleDateFormat formater = new SimpleDateFormat("1" + "yyMMdd");
    Date aujourdhui = new Date();
    
    return formater.format(aujourdhui);
  }
  
  /**
   * Traduire de manière lisible le numéro de version
   */
  public String traduireVersion(int version) {
    String chaine = String.valueOf(version);
    
    if (chaine.length() == 3) {
      chaine = "v." + chaine.substring(0, 1) + "." + chaine.substring(1, 3);
    }
    else if (chaine.length() == 4) {
      chaine = "v." + chaine.substring(0, 2) + "." + chaine.substring(2, 4);
    }
    else {
      chaine = "v." + chaine;
    }
    
    return chaine;
  }
  
  /**
   * Retourner l'URL complète, paramètres compris, reçue par la servlet.
   */
  public static String getURL(HttpServletRequest request) {
    // Vérifier les paramètres
    if (request == null) {
      return "";
    }
    
    // Tracer l'URL
    StringBuilder adresse = new StringBuilder(request.getRequestURL().toString());
    String parametre = request.getQueryString();
    if (parametre == null) {
      return adresse.toString();
    }
    else {
      return adresse.append('?').append(parametre).toString();
    }
  }
  
  /**
   * Générer une page web qui affiche le contenu de la stacktrace en cas d'exception.
   * 
   * @param pHttpServletResponse Réponse servlet.
   * @param pException Exception à afficher dans la page.
   */
  public static void afficherPageErreur(HttpServletResponse pHttpServletResponse, Exception pException) {
    try {
      // Effacer la réponse en cours
      pHttpServletResponse.reset();
      
      // Construire la page
      ServletOutputStream out = pHttpServletResponse.getOutputStream();
      out.println("<div id='ajust'></div>");
      out.println("<article id='intro'>");
      out.println("<p>S&eacute;rie N <span>mobilit&eacute;</span></p>");
      out.println("</article>");
      out.println("<article id='message'>");
      
      // Afficher un résumé des messages en début de page
      StringBuilder messageResume = new StringBuilder();
      for (Throwable cause = pException; cause != null; cause = cause.getCause()) {
        messageResume.append(cause.getMessage() + "<br>");
      }
      if (messageResume.length() > 0) {
        messageResume.append("<br>");
      }
      out.println("<p>" + messageResume + "</p>");
      
      // Afficher les messages empilés dans les exceptions
      StringBuilder messageComplet = new StringBuilder();
      for (Throwable cause = pException; cause != null; cause = cause.getCause()) {
        messageComplet.append(cause.getClass().getSimpleName() + " : " + cause.getMessage() + "<br>");
        for (StackTraceElement element : cause.getStackTrace()) {
          messageComplet.append(element.toString());
          messageComplet.append("<br>");
        }
        messageComplet.append("<br>");
      }
      out.println("<p>" + messageComplet + "</p>");
      
      out.println("</article>");
    }
    catch (Exception e) {
      // Pas de traitement particulier si on ne parvient pas à générer la page d'erreur
    }
  }
  
  public char getSeparateurMiliers() {
    return separateurMiliers;
  }
  
  public void setSeparateurMiliers(char separateurMiliers) {
    this.separateurMiliers = separateurMiliers;
  }
  
  public int getNombreDecimales() {
    return nombreDecimales;
  }
  
  public void setNombreDecimales(int nombreDecimales) {
    this.nombreDecimales = nombreDecimales;
  }
  
  public void setSeparateurDate(String separateurDate) {
    this.separateurDate = separateurDate;
  }
  
}
