/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.environnement;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.exp.database.PsemrtemManager;
import ri.serien.libas400.dao.exp.database.PsemussmManager;
import ri.serien.libas400.dao.exp.programs.contact.M_Contact;
import ri.serien.libas400.dao.gvx.database.PgvmparmManager;
import ri.serien.libas400.dao.gvx.database.PgvmsecmManager;
import ri.serien.libas400.dao.gvx.programs.UtilisateurETB_MAG;
import ri.serien.libas400.database.record.ExtendRecord;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.database.record.GenericRecordManager;
import ri.serien.libas400.parameters.EtablissementMobilite;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.backoffice.GestionBObdd;
import ri.serien.mobilite.backoffice.GestionBOlogs;
import ri.serien.mobilite.backoffice.GestionBOutilisateurs;
import ri.serien.mobilite.backoffice.GestionBOversion;
import ri.serien.mobilite.backoffice.GestionParametres;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.crm.GestionFormulaire;
import ri.serien.mobilite.metier.GestionCatalogue;
import ri.serien.mobilite.metier.InfosClient;
import ri.serien.mobilite.metier.Magasin;
import ri.serien.mobilite.metier.Vue;
import ri.serien.mobilite.metier.article.GestionArticles;
import ri.serien.mobilite.metier.client.GestionClients;
import ri.serien.mobilite.metier.contact.GestionContacts;
import ri.serien.mobilite.metier.crm.LAF_ActionCommerciale;
import ri.serien.mobilite.outils.Outils;

public class Utilisateur {
  // Variables
  private int id = 0;
  private String login = null;
  private String mdp = null;
  private String machine = null;
  private String baseDeDonnees = null;
  private String lettre_env = null;
  private IdEtablissement idEtablissement = null;
  private Magasin magasin = null;
  private String codeRepresentant = null;
  private GestionClients gestionClients = null;
  private GestionArticles gestionArticles = null;
  private GestionCatalogue gestionCatalogue = null;
  private GestionParametres gestionParametres = null;
  private GestionContacts gestionContacts = null;
  private GestionBOutilisateurs gestionBOutilisateur = null;
  private GestionBOversion gestionBOversion = null;
  private GestionBOlogs gestionBOlogs = null;
  private Bibliotheque bibliothequeEnvironnement = null;
  
  private GestionBObdd gestionBObdd = null;
  private SystemeManager systemeManager = null;
  private Connection maconnection = null;
  private Connexion_SQLite baseSQLITE = null;
  private List<Vue> listeVue = null;
  private GenericRecordManager manager = null;
  private PgvmparmManager fparametre = null;
  private Outils outils = null;
  private boolean superUtilisateur = false;
  private EtablissementMobilite etablissement = new EtablissementMobilite();
  private int niveauAcces = 0;
  private String msgErreur = "";
  private int nbLignesListes = 0;
  private InfosClient infosClient = null;
  private M_Contact contact = null;
  private ArrayList<GenericRecord> listeClientsCRM = null;
  private LAF_ActionCommerciale gestionAC = new LAF_ActionCommerciale();
  private GestionFormulaire formulaireCourant = new GestionFormulaire();
  private PgvmsecmManager pgvmsecm = null;
  
  /**
   * Constructeur avec les paramètres de connexion de l'utilisateur.
   */
  public Utilisateur(String pLogin, SystemeManager pSystemeManager) {
    login = pLogin.toUpperCase();
    machine = ConstantesEnvironnement.getAdresseIPServeurIBM();
    systemeManager = pSystemeManager;
    infosClient = new InfosClient(this);
    // Niveau d'accès par défaut
    niveauAcces = 3;
    nbLignesListes = 50;
    
    // Vérifier les paramètres
    ConstantesEnvironnement.controlerBibliothequeEnvironnement();
    
    // Gestion de la connection à DB2 de l'AS
    try {
      if (systemeManager != null && systemeManager.getSystem() != null) {
        maconnection = systemeManager.getDatabaseManager().getConnection();
      }
      
      if (maconnection != null) {
        manager = new GenericRecordManager(maconnection);
      }
      else {
        Trace.erreur("ECHEC Connexion DB2 de l'utilisateur 1");
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "ECHEC Connexion DB2 de l'utilisateur 2");
    }
    
    // Connexion à la SQLITE du serveur WEB
    if (maconnection != null) {
      baseSQLITE = new Connexion_SQLite(ConstantesEnvironnement.getNomCompletFichierSQLite(), this);
      if (baseSQLITE != null) {
        baseSQLITE.chargerUtilisateur(this);
        // Récupération de la bibliothèque (curlib) de l'utilisateur
        baseDeDonnees = getBibliotheque(login);
        baseSQLITE.gererBibli(this);
      }
    }
    
    // Si le manager est ok et si la lettre d'environnement n'a pas été initialisée
    if (manager != null && ConstantesEnvironnement.lettreEnvironnement == null) {
      ArrayList<GenericRecord> liste = manager
          .select("SELECT ENVVER FROM QGPL.PSEMENVM WHERE ENVCLI = '" + ConstantesEnvironnement.getBibliothequeEnvironnement() + "' ");
      
      if (liste != null && liste.size() == 1) {
        if (liste.get(0).isPresentField("ENVVER")) {
          ConstantesEnvironnement.lettreEnvironnement = liste.get(0).getField("ENVVER").toString().trim();
        }
      }
    }
    lettre_env = ConstantesEnvironnement.lettreEnvironnement;
    
    // Initialisation de l'objet BibliothèqueEnvironnement
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(ConstantesEnvironnement.getBibliothequeEnvironnement());
    bibliothequeEnvironnement = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.SYSTEME_SERIEN);
    
    fparametre = new PgvmparmManager(maconnection);
    pgvmsecm = new PgvmsecmManager(systemeManager.getDatabaseManager().getConnection());
    pgvmsecm.setLibrary(baseDeDonnees);
    
    // Récupération de l'établissement et du magasin (bricolage rapide afin que cela fonctionne si pas de DG blanche)
    UtilisateurETB_MAG utilisateurETB_MAG = new UtilisateurETB_MAG(systemeManager);
    idEtablissement = utilisateurETB_MAG.getEtablissement(login, baseDeDonnees);
    if (idEtablissement != null) {
      etablissement.setId(idEtablissement);
    }
    try {
      IdMagasin idMagasin = utilisateurETB_MAG.chargerMagasinUtilisateur(login, baseDeDonnees);
      if (idMagasin != null) {
        magasin = new Magasin(idMagasin.getCode());
      }
    }
    catch (Exception e) {
      magasin = new Magasin();
    }
    
    // Recherche des informations principales sur l'utilisateur
    if (recuperationInfosDGSociete(baseDeDonnees, etablissement)) {
      // Si l'utilisateur a un magasin on lui associe sinon on prend ...
      try {
        ExtendRecord record = pgvmsecm.getRecordsbyUSERandETB(login, idEtablissement);
        // De l'établissement si le user n'en a pas
        String codeMagasin = null;
        String stockMobilite = null;
        if (record == null) {
          if (etablissement.getMagasinGeneral() != null) {
            magasin = new Magasin(etablissement.getMagasinGeneral().getCode());
            getInfosMagasin(idEtablissement, magasin);
          }
        }
        else {
          stockMobilite = Constantes.normerTexte(record.getField("SET176").toString());
          
          // on va lire le paramètre pour afficher le stock (set176, si blanc ou 0 = affiche magasin dans la DG,
          // si 1 affiche le magasin de l'utilisateur.
          
          if (stockMobilite.isEmpty() || stockMobilite.equals("0")) {
            if (etablissement.getMagasinGeneral() != null) {
              magasin = new Magasin(etablissement.getMagasinGeneral().getCode());
              getInfosMagasin(idEtablissement, magasin);
            }
            ConstantesEnvironnement.stockDG = true;
            ConstantesEnvironnement.afficheTousLesStocks = false;
            
          }
          else if (stockMobilite.equals("1")) {
            magasin = new Magasin();
            codeMagasin = record.getField("SEMAUS").toString().trim();
            magasin.setCodeMagasin(codeMagasin);
            ConstantesEnvironnement.stockUtilisateur = true;
            ConstantesEnvironnement.afficheTousLesStocks = false;
            getInfosMagasin(idEtablissement, magasin);
          }
          else {
            magasin = new Magasin();
            codeMagasin = record.getField("SEMAUS").toString().trim();
            magasin.setCodeMagasin(codeMagasin);
            ConstantesEnvironnement.afficheTousLesStocks = true;
          }
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "ECHEC recup&eagrave;ration du magasin");
      }
    }
    else {
      idEtablissement = etablissement.getId();
      magasin = new Magasin();
    }
    // Création de la boite à outils de l'utilisateur
    outils = new Outils(this);
    
    // Recherche des informations Contact(psemrtem) de l'utilisateur (TODO voir le pb d'ETB RIS ou blanc)
    recuperationInfoContact("");
  }
  
  public void deconnecterAS400() {
    if (systemeManager != null) {
      
      if (maconnection != null) {
        try {
          maconnection.close();
          maconnection = null;
        }
        catch (SQLException e) {
          e.printStackTrace();
        }
      }
      
      systemeManager.deconnecter();
      systemeManager = null;
    }
  }
  
  /**
   * Constructeur pour utilsisateur lambda
   */
  public Utilisateur(int pId, String pLogin, int pNiveau, int pSuperUtilisateur, String pRepresentant) {
    id = pId;
    this.login = pLogin;
    this.niveauAcces = pNiveau;
    this.setUnSuperUtilisateur(pSuperUtilisateur == 1);
    this.codeRepresentant = pRepresentant;
  }
  
  /**
   * Récupération de la base de données de l'utilisateur.
   */
  private String getBibliotheque(String pLogin) {
    // Vérifier les paramètres
    ConstantesEnvironnement.controlerBibliothequeEnvironnement();
    
    // Récupérer la base de données de l'utilisateur
    final PsemussmManager psemussmManager = new PsemussmManager(maconnection);
    String baseDeDonnees = psemussmManager.getBibliotheque(ConstantesEnvironnement.getBibliothequeEnvironnement(), pLogin);
    if (baseDeDonnees == null) {
      throw new MessageErreurException("L'utilisateur n'a pas de base de données associée : " + pLogin);
    }
    
    return baseDeDonnees;
  }
  
  /**
   * Changer le magasin.
   */
  public void changerMag(String pMagasin) {
    
    ArrayList<GenericRecord> listeMag = this.getMagasin().retournerListeDesMagasins(this);
    for (int i = 0; i < listeMag.size(); i++) {
      if (pMagasin.equals(listeMag.get(i).getField("PARIND").toString().trim())) {
        magasin.setLibelleMagasin(listeMag.get(i).getField("LIBETB").toString().trim());
      }
    }
    magasin.setCodeMagasin(pMagasin);
  }
  
  /**
   * Recupère les informations de la DG de la société lié à l'établissementri.
   */
  private boolean recuperationInfosDGSociete(String pBaseDeDonnees, EtablissementMobilite pEtablissement) {
    if (fparametre == null) {
      return false;
    }
    fparametre.setLibrary(pBaseDeDonnees);
    // if (fparametre.chargerInfosEtablissementPilote(pEtablissement)) {
    if (pEtablissement != null && pEtablissement.getId() != null) {
      return fparametre.getInfosEtablissement(pEtablissement);
    }
    else {
      return false;
    }
  }
  
  /**
   * Initialise la classe Contact à partir des infos du PSEMRTEM pour cet utilisateur.
   */
  private void recuperationInfoContact(String aetb) {
    final PsemrtemManager rtem = new PsemrtemManager(maconnection);
    contact = rtem.getContact(baseDeDonnees, aetb, login.toUpperCase());
    if (contact == null) {
      Trace.erreur("ECHEC recup&eagrave;ration des infos du contact");
    }
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * Lecture des infos pour un magasin.
   */
  public boolean getInfosMagasin(IdEtablissement pIdEtablissement, Magasin pMagasin) {
    if (pMagasin == null) {
      return false;
    }
    
    String libMag = "";
    ArrayList<GenericRecord> liste = this.getManager()
        .select("Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, SUBSTR(PARZ2, 1, 24) AS LIBETB, PARZ3, PARZ4 from "
            + this.getBaseDeDonnees() + ".PGVMPARM where PARETB = '" + idEtablissement + "' and PARTYP = 'MA' and PARIND = '"
            + pMagasin.getCodeMagasin() + "'");
    
    // On alimente les infos si on trouve l'enregistrement
    if (liste.size() == 1) {
      libMag = liste.get(0).getField("LIBETB").toString().trim();
      pMagasin.setLibelleMagasin(libMag);
      return true;
    }
    
    return false;
  }
  
  // -- Accesseurs
  
  public Connection getMaconnection() {
    return maconnection;
  }
  
  public void setMaconnection(Connection maconnection) {
    this.maconnection = maconnection;
  }
  
  public String getLogin() {
    return login;
  }
  
  public void setLogin(String login) {
    this.login = login;
  }
  
  public String getMachine() {
    return machine;
  }
  
  public void setMachine(String machine) {
    this.machine = machine;
  }
  
  public String getBaseDeDonnees() {
    if (baseDeDonnees == null) {
      baseDeDonnees = getBibliotheque(getLogin());
    }
    return baseDeDonnees;
  }
  
  public void setBaseDeDonnees(String pBaseDeDonnees) {
    this.baseDeDonnees = pBaseDeDonnees;
  }
  
  /**
   * @return le lettre_env
   */
  public String getLettre_env() {
    return lettre_env;
  }
  
  /**
   * @param lettre_env le lettre_env à définir
   */
  public void setLettre_env(String lettre_env) {
    this.lettre_env = lettre_env;
  }
  
  public GestionClients getGestionClients() {
    return gestionClients;
  }
  
  public GestionArticles getGestionArticles() {
    return gestionArticles;
  }
  
  public void setGestionArticles(GestionArticles gestionArticles) {
    this.gestionArticles = gestionArticles;
  }
  
  public void setGestionClients(GestionClients gestionClients) {
    this.gestionClients = gestionClients;
  }
  
  public Connexion_SQLite getBaseSQLITE() {
    return baseSQLITE;
  }
  
  public void setBaseSQLITE(Connexion_SQLite baseSQLITE) {
    this.baseSQLITE = baseSQLITE;
  }
  
  /**
   * Liste des vues de l'utilisateurs.
   */
  public List<Vue> getListeVue() {
    return listeVue;
  }
  
  /**
   * Modifier la liste des vues de l'utilisateur.
   */
  public void setListeVue(List<Vue> pListeVue) {
    listeVue = pListeVue;
  }
  
  /**
   * Identifiant unique de l'utilisateur.
   */
  public int getId() {
    return id;
  }
  
  /**
   * Modifier l'identfiant unique de l'utilisateur.
   */
  public void setId(int pId) {
    id = pId;
  }
  
  public GestionCatalogue getGestionCatalogue() {
    return gestionCatalogue;
  }
  
  public void setGestionCatalogue(GestionCatalogue gestionCatalogue) {
    this.gestionCatalogue = gestionCatalogue;
  }
  
  public GenericRecordManager getManager() {
    return manager;
  }
  
  public void setManager(GenericRecordManager manager) {
    this.manager = manager;
  }
  
  public Magasin getMagasin() {
    return magasin;
  }
  
  public void setMagasin(Magasin magasin) {
    this.magasin = magasin;
  }
  
  public Outils getOutils() {
    return outils;
  }
  
  public void setOutils(Outils outils) {
    this.outils = outils;
  }
  
  /**
   * Tester si l'utilisateur est un superutilisateur.
   * Si c'est le cas, l'uutilisateur peut se connecter quelque soit le nombre de sessions déjà connectées.
   */
  public boolean isSuperUtilisateur() {
    return superUtilisateur;
  }
  
  /**
   * Définir si l'utilisateur est un superutilisateur.
   */
  public void setUnSuperUtilisateur(boolean pSuperUtilisateur) {
    superUtilisateur = pSuperUtilisateur;
  }
  
  /**
   * Niveau d'accès aux paramètres de la mobilité.
   */
  public int getNiveauAcces() {
    return niveauAcces;
  }
  
  /**
   * Modifier le niveau d'accès aux paramètres de la mobilité.
   */
  public void setNiveauAcces(int niveauAcces) {
    this.niveauAcces = niveauAcces;
  }
  
  public GestionParametres getGestionParametres() {
    return gestionParametres;
  }
  
  public void setGestionParametres(GestionParametres gestionParametres) {
    this.gestionParametres = gestionParametres;
  }
  
  public GestionBOutilisateurs getGestionBOutilisateur() {
    return gestionBOutilisateur;
  }
  
  public void setGestionBOutilisateur(GestionBOutilisateurs gestionBOutilisateur) {
    this.gestionBOutilisateur = gestionBOutilisateur;
  }
  
  public GestionBOlogs getGestionBOlogs() {
    return gestionBOlogs;
  }
  
  public void setGestionBOlogs(GestionBOlogs gestionBOlogs) {
    this.gestionBOlogs = gestionBOlogs;
  }
  
  public int getNbLignesListes() {
    // On en rajoute une ligne pour tester si le nb de lignes réelles dépasse le nb de lignes affichées
    return nbLignesListes;
  }
  
  public void setNbLignesListes(int nbLignesListes) {
    this.nbLignesListes = nbLignesListes;
  }
  
  public PgvmparmManager getFparametre() {
    return fparametre;
  }
  
  public void setFparametre(PgvmparmManager fparametre) {
    this.fparametre = fparametre;
  }
  
  public GestionBOversion getGestionBOversion() {
    return gestionBOversion;
  }
  
  public void setGestionBOversion(GestionBOversion gestionBOversion) {
    this.gestionBOversion = gestionBOversion;
  }
  
  public SystemeManager getSysteme() {
    return systemeManager;
  }
  
  public void setSysteme(SystemeManager systeme) {
    this.systemeManager = systeme;
  }
  
  public GestionBObdd getGestionBObdd() {
    return gestionBObdd;
  }
  
  public void setGestionBObdd(GestionBObdd gestionBObdd) {
    this.gestionBObdd = gestionBObdd;
  }
  
  public String getCodeRepresentant() {
    return codeRepresentant;
  }
  
  public void setCodeRepresentant(String codeRepresentant) {
    this.codeRepresentant = codeRepresentant;
  }
  
  public EtablissementMobilite getEtablissement() {
    return etablissement;
  }
  
  /**
   * @return le infosClient
   */
  public InfosClient getInfosClient() {
    return infosClient;
  }
  
  /**
   * @param infosClient le infosClient à définir
   */
  public void setInfosClient(InfosClient infosClient) {
    this.infosClient = infosClient;
  }
  
  public M_Contact getContact() {
    return contact;
  }
  
  public void setContact(M_Contact contact) {
    this.contact = contact;
  }
  
  public ArrayList<GenericRecord> getListeClientsCRM() {
    return listeClientsCRM;
  }
  
  public void setListeClientsCRM(ArrayList<GenericRecord> listeClientsCRM) {
    this.listeClientsCRM = listeClientsCRM;
  }
  
  /**
   * @return le gestionAC
   */
  public LAF_ActionCommerciale getGestionAC() {
    return gestionAC;
  }
  
  /**
   * @param gestionAC le gestionAC à définir
   */
  public void setGestionAC(LAF_ActionCommerciale gestionAC) {
    this.gestionAC = gestionAC;
  }
  
  /**
   * @return le formulaireCourant
   */
  public GestionFormulaire getFormulaireCourant() {
    return formulaireCourant;
  }
  
  /**
   * @param formulaireCourant le formulaireCourant à définir
   */
  public void setFormulaireCourant(GestionFormulaire formulaireCourant) {
    this.formulaireCourant = formulaireCourant;
  }
  
  public String getMdp() {
    return mdp;
  }
  
  public void setMdp(String mdp) {
    this.mdp = mdp;
  }
  
  public GestionContacts getGestionContacts() {
    return gestionContacts;
  }
  
  public void setGestionContacts(GestionContacts gestionContacts) {
    this.gestionContacts = gestionContacts;
  }
  
  public Bibliotheque getBibliothequeEnvironnement() {
    return bibliothequeEnvironnement;
  }
  
}
