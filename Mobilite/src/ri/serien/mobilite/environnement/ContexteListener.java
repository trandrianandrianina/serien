/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.environnement;

import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;

/**
 * Permet d'écouter les changements d'état du ServletContext. Le conteneur Web avertit l'application de la création du ServletContext
 * grâce à la méthode contextInitialized et de la destruction du ServletContext grâce à la méthode contextDestroyed.
 */
public class ContexteListener implements ServletContextListener {
  /**
   * Appelé lorsque l'application web est en cours de démarrage.
   */
  @Override
  public void contextInitialized(ServletContextEvent sce) {
    // Récupérer le contexte Servlet
    ServletContext servletContext = sce.getServletContext();
    
    // Lire les paramètres du fichier context.xml
    ConstantesEnvironnement.lireParametreContextXml(servletContext);
    
    // Tracer le démarrage du logiciel
    // Après le chargement des paramètres car c'est grâce à eux que l'on connaît l'emplacement du fichier de trace
    Trace.demarrerLogiciel(ConstantesEnvironnement.getDossierTravail(), "mobilite", "MOBILITE");
    
    // Tracer les paramètres globaux
    ConstantesEnvironnement.tracer();
  }
  
  /**
   * Appelé lorsque l'application web est en cours d'arrêt.
   */
  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    // Tracer le début de l'arrêt du Webshop
    Trace.titre("DEBUT DE L'ARRET DE MOBILITE");
    
    // Désenregsitrer les pilotes
    Enumeration<Driver> drivers = null;
    try {
      drivers = DriverManager.getDrivers();
      while (drivers.hasMoreElements()) {
        DriverManager.deregisterDriver(drivers.nextElement());
      }
      
      // Tracer l'arrêt du logiciel
      Trace.arreterLogiciel();
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "Erreur lors de l'arrêt de Mobilité");
      
      // Remonter l'erreur au serveur d'application
      throw new RuntimeException(e);
    }
  }
}
