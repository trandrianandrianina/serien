/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.constantes;

import javax.servlet.ServletContext;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Centralise les paramètres généraux.
 * 
 * Une partie des paramètres est fixée en dur dans cette classe, une autre partie des paramètres est récupérée dans le fichier context.xml
 * de Tomcat, un autre partie est déterminée à partir des paramètres précédents.
 */
public class ConstantesEnvironnement {
  // Constantes
  public static final String SEPARATEUR = System.getProperty("file.separator");
  public static final String PREFIXE_PAREMETRE_CONTEXT_XML = "[Paramètre context.xml] ";
  public static final String PREFIXE_PAREMETRE_SQLITE = "[Paramètre SQLite] ";
  public static final String PREFIXE_PAREMETRE = "[Paramètre] ";
  public static final String SQLITE_ISOK = "$$OKrequeteBDD$$";
  
  // Nom des paramètres du fichier context.xml
  private static final String PARAMETRE_IS_ECLIPSE = "IS_ECLIPSE";
  private static final String PARAMETRE_LIBELLE_SERVEUR_TOMCAT = "LIBELLE_SERVEUR";
  private static final String PARAMETRE_DOSSIER_RACINE_TOMCAT = "DOSSIER_RACINE_TOMCAT";
  private static final String PARAMETRE_DOSSIER_RACINE_MOBILITE = "DOSSIER_RACINE_MOBILITE";
  private static final String PARAMETRE_BIBLIOTHEQUE_ENVIRONNEMENT = "BIBLI_ENV_MOBILITE";
  
  // droits de base d'un utilisateur
  public static final int ACCES_MOBILITE = 3;
  
  public static final int ACCES_RI = 1;
  public static final int ACCES_ADMIN = 2;
  public static final int ACCES_UTIL = 3;
  public static final int ACCES_NON = 4;
  
  // PARAMETRES TOMCAT
  private static String dossierRacineTomcat = null;
  private static boolean isEclipse = false;
  private static String libelleServeurTomcat = null;
  
  // PARAMETRES MOBILITE
  private static String dossierRacineServlet = null;
  private static String dossierRacineMobilite = null;
  private static String dossierTravail = null;
  
  // PARAMETRES VERSION
  private static String dossierVersion = null;
  private static String nomCompletVersionActuelle = dossierVersion + "versionActuelle.txt";
  public static int versionActuelle = 0;
  
  // PARAMETRES SQLITE
  private static String dossierSQLite = null;
  private static String dossierSauvegardeSQLLite = null;
  private static final String NOM_COURT_FICHIER_SQLITE = "bdd_mobilite";
  private static String nomCompletFichierSQLite = null;
  private static String nomCompletFichierSQLiteOriginal = null;
  
  // PARAMETRES SERVEUR IBM
  private static String adresseIPServeurIBM = null;
  private static String bibliothequeEnvironnement = null;
  public static String lettreEnvironnement = null;
  
  // DIVERS
  
  // Dossier de déploiement du WAR sur le serveur tomcat
  public static final String DOSSIER_DEPLOIE = dossierRacineMobilite;
  
  // Dossier version sur AS
  public static final String DOSSIER_VERSION_AS = "";
  public static final String FICHIER_MAJBDD = "miseAjourBDD.sql";
  
  // SERVEUR FTP
  public static final String SERVEUR_FTP = "ftp.seriem-support.com";
  public static final String CLIENT_FTP = "seriem-support.com";
  public static final String MP_FTP = "bv28z3yf";
  public static final String DOSSIER_VERSIONS_FTP = "/majMobilite/214";
  
  // DIVERS
  
  // 10mn
  public static final int DECO_AUTO = 600;
  public static int nbSessionFlot = 0;
  public static int nbSessionFix = 0;
  
  public final static String VERSION_JS = "19";
  
  public static boolean afficheTousLesStocks = false;
  public static boolean stockDG = false;
  public static boolean stockUtilisateur = false;
  
  /**
   * Afficher dans les traces l'ensemble des paramètres gérés par cette classe.
   */
  public static void tracer() {
    // Tomcat
    Trace.info("TOMCAT_DOSSIER_RACINE = " + dossierRacineTomcat);
    Trace.info("TOMCAT_LIBELLE_SERVEUR = " + libelleServeurTomcat);
    Trace.info("TOMCAT_IS_ECLIPSE = " + isEclipse);
    
    // Mobilité
    Trace.info("MOBILITE_DOSSIER_RACINE_SERVLET = " + dossierRacineServlet);
    Trace.info("MOBILITE_DOSSIER_RACINE = " + dossierRacineMobilite);
    Trace.info("MOBILITE_DOSSIER_TRAVAIL = " + dossierTravail);
    
    // Versions
    Trace.info("VERSION_DOSSIER = " + dossierVersion);
    Trace.info("VERSION_NOM_COMPLET_VERSION_ACTUELLE = " + nomCompletVersionActuelle);
    Trace.info("VERSION_ACTUELLE = " + versionActuelle);
    
    // SQLite
    Trace.info("SQLITE_DOSSIER = " + dossierSQLite);
    Trace.info("SQLITE_DOSSIER_SAUVEGARDE = " + dossierSauvegardeSQLLite);
    Trace.info("SQLITE_NOM_COURT_FICHIER = " + NOM_COURT_FICHIER_SQLITE);
    Trace.info("SQLITE_NOM_COMPLET_FICHIER = " + nomCompletFichierSQLite);
    Trace.info("SQLITE_NOM_COMPLET_FICHIER_ORIGINAL =" + nomCompletFichierSQLiteOriginal);
    
    // Serveur IBM
    Trace.info("AS400_ADRESSE_IP = " + adresseIPServeurIBM);
    Trace.info("BIBLIOTHEQUE_ENVIRONNEMENT = " + bibliothequeEnvironnement);
    Trace.info("LETTRE_ENVIRONNEMENT = " + lettreEnvironnement);
    
    // Divers
    Trace.info("DOSSIER_DEPLOIE = " + DOSSIER_DEPLOIE);
    Trace.info("DOSSIER_VERSION_AS = " + DOSSIER_VERSION_AS);
    
    Trace.info("FICHIER_MAJBDD = " + FICHIER_MAJBDD);
    Trace.info("SERVEUR_FTP = " + SERVEUR_FTP);
    Trace.info("CLIENT_FTP = " + CLIENT_FTP);
    Trace.info("DOSSIER_VERSIONS_FTP = " + DOSSIER_VERSIONS_FTP);
    Trace.info("VERSION_JS = " + VERSION_JS);
    
    // Métier
    Trace.info("AFFICHE_TOUS_LES_STOCKS = " + afficheTousLesStocks);
    Trace.info("STOCK_DG = " + stockDG);
    Trace.info("STOCK_UTILISATEUR = " + stockUtilisateur);
  }
  
  /**
   * Lire les paramètres dans le fichier context.xml afin de renseigner les paramètres globaux correspondants.
   * 
   * @param pServletContext Contexte Servlet.
   */
  public static void lireParametreContextXml(ServletContext pServletContext) {
    // Vérifier les paramètres
    if (pServletContext == null) {
      throw new MessageErreurException("Le contexte de Servlet est invalide.");
    }
    
    // Récupérer le dossier racine du contexte Servlet
    dossierRacineServlet = pServletContext.getRealPath("/");
    Trace.info("DOSSIER_RACINE_SERVLET = " + ConstantesEnvironnement.dossierRacineServlet);
    
    // Récupérer le libellé du serveur Tomcat
    libelleServeurTomcat = pServletContext.getInitParameter(PARAMETRE_LIBELLE_SERVEUR_TOMCAT);
    Trace.info(PREFIXE_PAREMETRE_CONTEXT_XML + PARAMETRE_LIBELLE_SERVEUR_TOMCAT + " = " + libelleServeurTomcat);
    
    // Récupérer l’emplacement du répertoire racine de Tomcat sur le poste de travail
    dossierRacineTomcat = pServletContext.getInitParameter(PARAMETRE_DOSSIER_RACINE_TOMCAT);
    Trace.info(PREFIXE_PAREMETRE_CONTEXT_XML + PARAMETRE_DOSSIER_RACINE_TOMCAT + " = " + dossierRacineTomcat);
    if (dossierRacineTomcat == null || dossierRacineTomcat.isEmpty()) {
      // Si le le dossier racine de Tomcat n'a pas été intialisé avec le fichier context.xml,
      // le renseigner avec le dossier racine du contexte Servlet
      dossierRacineTomcat = dossierRacineServlet;
    }
    if (!dossierRacineTomcat.endsWith(SEPARATEUR)) {
      // Ajouter un séparateur au dossier s'il n'en a pas
      dossierRacineTomcat += SEPARATEUR;
    }
    
    // Récupérer le dossier de Mobilité dans l’arborescence Tomcat
    dossierRacineMobilite = pServletContext.getInitParameter(PARAMETRE_DOSSIER_RACINE_MOBILITE);
    Trace.info(PREFIXE_PAREMETRE_CONTEXT_XML + PARAMETRE_DOSSIER_RACINE_MOBILITE + " = " + dossierRacineMobilite);
    if (dossierRacineMobilite == null || dossierRacineMobilite.isEmpty()) {
      // Si le le dossier n'a pas été intialisé avec le fichier context.xml, le renseigner avec le dossier racine du contexte Servlet
      dossierRacineMobilite = dossierRacineServlet;
    }
    if (!dossierRacineMobilite.endsWith(SEPARATEUR)) {
      // Ajouter un séparateur au dossier s'il n'en a pas
      dossierRacineMobilite += SEPARATEUR;
    }
    
    // Récupérer la variable indiquant si on s'exécute sous Eclipse ou non
    String parametreIsEclipse = pServletContext.getInitParameter(PARAMETRE_IS_ECLIPSE);
    Trace.info(PREFIXE_PAREMETRE_CONTEXT_XML + PARAMETRE_IS_ECLIPSE + " = " + parametreIsEclipse);
    if (parametreIsEclipse != null) {
      isEclipse = parametreIsEclipse.trim().equalsIgnoreCase("true");
    }
    
    // Récupérer la bibliotheque de l'environnement
    bibliothequeEnvironnement = pServletContext.getInitParameter(PARAMETRE_BIBLIOTHEQUE_ENVIRONNEMENT);
    Trace.info(PREFIXE_PAREMETRE_CONTEXT_XML + PARAMETRE_BIBLIOTHEQUE_ENVIRONNEMENT + " = " + bibliothequeEnvironnement);
    
    // Déterminer lea paramètres de des versions
    dossierVersion = dossierRacineMobilite + "WEB-INF" + SEPARATEUR + "versions" + SEPARATEUR;
    nomCompletVersionActuelle = dossierVersion + "versionActuelle.txt";
    
    // Déterminer le dossier de travail
    dossierTravail = dossierRacineTomcat + "work" + SEPARATEUR;
    
    // Déterminer les paramètres SQLite
    dossierSQLite = dossierTravail + "bdd" + SEPARATEUR;
    dossierSauvegardeSQLLite = dossierTravail + "sauvegardes" + SEPARATEUR;
    nomCompletFichierSQLite = dossierSQLite + ConstantesEnvironnement.NOM_COURT_FICHIER_SQLITE;
    nomCompletFichierSQLiteOriginal =
        dossierVersion + "1" + ConstantesEnvironnement.SEPARATEUR + ConstantesEnvironnement.NOM_COURT_FICHIER_SQLITE;
  }
  
  /**
   * Dossier racine du serveur Tomcat (c'est le répertoire d'installation de Tomcat).
   * Ce paramètre se configure dans le fichier context.xml de Tomcat. S'il n'est pas renseigné, il est déduit automatiquement à partir
   * du contexte de Servlet envoyé par Tomcat.
   * @return Dossier d'installation du serveur Tomcat.
   */
  public static String getDossierRacineTomcat() {
    return dossierRacineTomcat;
  }
  
  /**
   * Tester si l'application s'exécute dans l'envrionnement Eclipse.
   * Ce paramètre se configure dans le fichier context.xml de Tomcat.
   * @return true=exécution sous Eclipse, false=exécution hors Eclipse.
   */
  public static boolean isEclipse() {
    return isEclipse;
  }
  
  /**
   * Libellé du serveur Tomcat.
   * Ce paramètre se configure dans le fichier context.xml de Tomcat. Il est uniquement utilisé à titre informatif afin d'être affiché
   * sur certaines pages. Il est donc optionnel.
   * @return Libéllé du serveur Tomcat.
   */
  public static String getLibelleServeurTomcat() {
    return libelleServeurTomcat;
  }
  
  /**
   * Dossier racine de la servlet.
   * Ce paramètre est déduit à partir du contexte de Servlet envoyé par Tomcat. Il permet de récupérer automatique le dossier racine
   * de l'application Tomcat.
   * @return Libéllé du serveur Tomcat.
   */
  public static String getDossierRacineServlet() {
    return dossierRacineServlet;
  }
  
  /**
   * Dossier racine de l'application web Mobilité (à l'intérieur de l'arborescence Tomcat donc).
   * Cela permet d'accéder aux ressources qui ont été mises dans le fichier WAR et ainsi déployées dans l'arborescence Tomcat.
   * Ce paramètre se configure dans le fichier context.xml de Tomcat. S'il n'est pas renseigné, il est déduit automatiquement à partir
   * du contexte de Servlet envoyé par Tomcat. Sauf exception, il n'est donc pas utile de le configurer dans le fichier context.xml.
   * @return Dossier racine de l'application Mobilité au sein de l'arboresence Tomcat.
   */
  public static String getDossierRacineMobilite() {
    return dossierRacineMobilite;
  }
  
  /**
   * Dossier de travail du logiciel Mobilité.
   * Le dossier de travail est destiné à recevoir les fichiers qui doivent être conservé d'une version à l'autre.
   * Le logiciel Mobilité utilise deux arboresences pour travailler :
   * 1) La première est l'arborescence de la servlet dans Tomcat qui contient les fichiers fournis dans le WAR. Les fichiers sont écrasés
   * à chaque installation. Cette arboresence est destinée à être lue mais il est déconseillé d'y écrire.
   * 2) La seconde arboresence est le dossier de travail dont les fichiers restent d'une installatino à une autre.
   * @param Dossier de travail.
   */
  public static String getDossierTravail() {
    return dossierTravail;
  }
  
  /**
   * Dossiers contenant les fichiers liés aux changements de versions.
   * Il s'agit notemment de la base de données vierges et des scripts SQL à passer lors d'un changement de version.
   * @return Dossier contenant les fichiers liés aux version.
   */
  public static String getDossierVersion() {
    return dossierVersion;
  }
  
  /**
   * Nom complet, chemin compris, du fichier contenant la version actuelle du logiciel Mobilité.
   */
  public static String getNomCompletVersionActuelle() {
    return nomCompletVersionActuelle;
  }
  
  /**
   * Dossier pour la base de données SQLite.
   */
  public static String getDossierSQLite() {
    return dossierSQLite;
  }
  
  /**
   * Dossier pour les sauvegardes des anciens fichiers SQLite lorsqu'une mise à jour est effectuée.
   */
  public static String getDossierSauvegardeSQLite() {
    return dossierSauvegardeSQLLite;
  }
  
  /**
   * Nom court du fichier hébergeant la base de données SQLite.
   */
  public static String getNomCourtFichierSQLite() {
    return NOM_COURT_FICHIER_SQLITE;
  }
  
  /**
   * Nom complet (avec le chemin) du fichier hébergeant la base de données SQLite.
   */
  public static String getNomCompletFichierSQLite() {
    return nomCompletFichierSQLite;
  }
  
  /**
   * Nom complet du fichier original de la base de données SQLite.
   * C'est ce fichier qui est copié dans l'arboresence Mobilité la première fois que le logiciel est lancé suit eà une installation.
   * Il contient une base de données presque vide.
   */
  public static String getNomCompletFichierSQLiteOriginal() {
    return nomCompletFichierSQLiteOriginal;
  }
  
  /**
   * Générer une exception si la bibliothèque d'environnement n'est pas ou mal configurée.
   */
  public static void controlerBibliothequeEnvironnement() {
    // Vérifier que la bibliothèque de l'environnement a été configurée
    if (bibliothequeEnvironnement == null || bibliothequeEnvironnement.trim().isEmpty()) {
      throw new MessageErreurException("La bibliothèque de l'environnement n'a pas été configurée. Renseigner le paramètre "
          + PARAMETRE_BIBLIOTHEQUE_ENVIRONNEMENT + " dans le fichier context.xml du serveur Tomcat.");
    }
  }
  
  /**
   * Bibliothèque de l'environnement auquel doit se connecter la Mobilité (X_GPL par exemple).
   * Ce paramètre se configure dans le fichier context.xml de Tomcat.
   * @param Bibliothèque de l'environnement.
   */
  public static String getBibliothequeEnvironnement() {
    return bibliothequeEnvironnement;
  }
  
  /**
   * Adresse IP du serveur IBM.
   * Ce paramètre est lu dans la base de données SQLite.
   * @param Adresse Ip du serveur.
   */
  public static String getAdresseIPServeurIBM() {
    return adresseIPServeurIBM;
  }
  
  /**
   * Modifier l'adresse IP du serveur IBM.
   * @param pAdresseIPServeurIBM Nouvelle adresse IP.
   */
  public static void setAdresseIPServeurIBM(String pAdresseIPServeurIBM) {
    adresseIPServeurIBM = pAdresseIPServeurIBM;
  }
}
