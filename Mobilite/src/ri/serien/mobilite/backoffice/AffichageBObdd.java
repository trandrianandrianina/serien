/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.backoffice;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.affichage.PatternErgonomie;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class AffichageBObdd
 */
public class AffichageBObdd extends HttpServlet {
  
  private int niveauAcces = ConstantesEnvironnement.ACCES_RI;
  private PatternErgonomie pattern = null;
  private int metier = -4;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageBObdd() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      int niveau = 2;
      String metaSpecifiques = "";
      String titreOptions = "Options";
      
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        // On créé un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        if (utilisateur.getNiveauAcces() <= niveauAcces) {
          // ancrage dans la page
          String ancrage = request.getParameter("ancrage");
          String requeteBDD = request.getParameter("requeteBDD");
          
          if (utilisateur.getGestionBObdd() == null) {
            utilisateur.setGestionBObdd(new GestionBObdd(utilisateur));
          }
          
          metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
          
          ServletOutputStream out = response.getOutputStream();
          out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
          out.println(pattern.afficherHeader(metier, niveau, null, utilisateur));
          out.println("<article id='contenu'>");
          
          if (requeteBDD != null) {
            out.println(afficherRetourRequete(utilisateur, requeteBDD));
          }
          else {
            out.println(afficherFormulaire(null));
          }
          
          out.println("</article>");
          out.println(pattern.afficherFin(titreOptions, null));
        }
        else {
          request.getSession().invalidate();
          getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
        }
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private String afficherFormulaire(String requete) {
    if (requete == null) {
      requete = "";
    }
    
    String chaine = "<section class='secContenu' id='blocFormulaireBDD'>";
    chaine += "<h1><span class='titreH1'>Saisie de requête SQLITE</span></h1>";
    chaine += "<form action='AffichageBObdd' method='POST' name='formRequeteBDD' id='formRequeteBDD'>";
    chaine += "<textarea name='requeteBDD' id='requeteBDD'>" + requete + "</textarea></br>";
    chaine += "<input type='submit' value='Envoyer' id='EnvoyerSQLITE'/>";
    chaine += "</form>";
    chaine += "</section>";
    
    return chaine;
  }
  
  private String afficherRetourRequete(Utilisateur utilisateur, String requete) {
    String chaine = "";
    
    chaine += afficherFormulaire(requete);
    
    chaine += "<section class='secContenu'>";
    chaine += "<h1><span class='titreH1'>Résultat de la requête</span></h1>";
    
    ArrayList<ArrayList<String>> listeResultat = null;
    listeResultat = utilisateur.getGestionBObdd().envoyerRequeteSQLITE(requete);
    
    if (listeResultat != null) {
      if (listeResultat.size() > 0) {
        if (listeResultat.size() == 1 && listeResultat.get(0).get(0).equals(ConstantesEnvironnement.SQLITE_ISOK)) {
          chaine += "<p id='retourSQLITE'>Succès de la requête</p>";
        }
        else {
          chaine += "<table id='tabSQLITE'>";
          int j = 0;
          for (int i = 0; i < listeResultat.size(); i++) {
            if (i == 0) {
              chaine += "<tr class='lignesSQLITE_th'>";
              
              for (j = 0; j < listeResultat.get(i).size(); j++) {
                chaine += "<th>" + listeResultat.get(i).get(j) + "</th>";
              }
              
              chaine += "</tr>";
            }
            else {
              if (i % 2 == 0) {
                chaine += "<tr class='lignesSQLITE_pyg'>";
              }
              else {
                chaine += "<tr class='lignesSQLITE'>";
              }
              for (j = 0; j < listeResultat.get(i).size(); j++) {
                chaine += "<td class='cellSQLITE'>" + listeResultat.get(i).get(j) + "</td>";
              }
              chaine += "</tr>";
            }
          }
          chaine += "</table>";
        }
      }
      else {
        chaine += "<p id='retourSQLITE'>Pas de résultat pour cette requête</p>";
      }
    }
    else {
      chaine += "<p id='retourSQLITE'>Problème avec la requête</p>";
    }
    
    chaine += "</section>";
    
    return chaine;
  }
}
