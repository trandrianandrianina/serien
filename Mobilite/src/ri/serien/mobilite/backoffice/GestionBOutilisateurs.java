/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.serien.mobilite.environnement.Utilisateur;

public class GestionBOutilisateurs {
  private Utilisateur utilisateur = null;
  private Utilisateur utilisateurSelectionne = null;
  
  /**
   * Constructeur par défaut
   */
  public GestionBOutilisateurs(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  /**
   * récupérer la liste des utilisateurs
   */
  public ArrayList<Utilisateur> recupererLesUtilisateurs() {
    ArrayList<Utilisateur> liste = new ArrayList<Utilisateur>();
    
    liste = utilisateur.getBaseSQLITE().recupererLesUtilisateurs();
    
    return liste;
  }
  
  /**
   * Modifier les propriétés d'un utilisateur sur le serveur
   */
  public boolean modifierUnUtilisateur(HttpServletRequest request) {
    // il faut effectuer les contrôles ici et trouver un maoyen de les retourner dans le message principal
    if (request != null && request.getParameter("modifie") != null) {
      return utilisateur.getBaseSQLITE().modifierUnUtilisateur(request);
    }
    else {
      return false;
    }
    
  }
  
  public Utilisateur recupererUnUtilisateur(int id) {
    utilisateurSelectionne = utilisateur.getBaseSQLITE().recupererUnUtilisateur(id);
    initUtilisateurSelectionne();
    return utilisateurSelectionne;
  }
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public Utilisateur getUtilisateurSelectionne() {
    return utilisateurSelectionne;
  }
  
  public void setUtilisateurSelectionne(Utilisateur utilisateurSelectionne) {
    this.utilisateurSelectionne = utilisateurSelectionne;
  }
  
  /**
   * Initialise les données génériques de l'utilisateur sélectioné avec celles de l'utiliasteur courant
   */
  private void initUtilisateurSelectionne() {
    // On teste pour si on a pas déjà fait cet init
    if (utilisateurSelectionne.getMaconnection() != null) {
      return;
    }
    
    utilisateurSelectionne.setMaconnection(utilisateur.getMaconnection());
  }
}
