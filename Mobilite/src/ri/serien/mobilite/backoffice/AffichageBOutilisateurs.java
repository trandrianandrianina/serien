/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.backoffice;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.affichage.PatternErgonomie;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class AffichageBOutilisateurs
 */
public class AffichageBOutilisateurs extends HttpServlet {
  
  private int niveauAcces = ConstantesEnvironnement.ACCES_ADMIN;
  private PatternErgonomie pattern = null;
  private int metier = -2;
  
  // private StringBuilder sb = new StringBuilder(16000);
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageBOutilisateurs() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      int niveau = 2;
      boolean modeModif = false;
      // String[] liensOptions = {"#","#","#"};
      String metaSpecifiques = "";
      String titreOptions = "Options";
      
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        // On créé un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        if (utilisateur.getNiveauAcces() <= niveauAcces) {
          // ancrage dans la page
          String ancrage = request.getParameter("ancrage");
          
          if (utilisateur.getGestionBOutilisateur() == null) {
            utilisateur.setGestionBOutilisateur(new GestionBOutilisateurs(utilisateur));
          }
          
          // passer en mode modification le cas échéant
          modeModif = (request.getParameter("modeModif") != null && request.getParameter("modeModif").equals("1"));
          
          // charger le look correspondant
          metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
          if (request.getParameter("utilId") == null) {
            metaSpecifiques += "<link href='css/listes.css' rel='stylesheet'/>";
            metaSpecifiques += "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"30\">";
          }
          else {
            metaSpecifiques += "<link href='css/fiches.css' rel='stylesheet'/>";
            metaSpecifiques += "<link href='css/BOunUtilisateur.css' rel='stylesheet'/>";
            niveau = 3;
          }
          
          ServletOutputStream out = response.getOutputStream();
          out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
          out.println(pattern.afficherHeader(metier, niveau, null, utilisateur));
          out.println("<article id='contenu'>");
          
          if (request.getParameter("utilId") == null) {
            // out.println("<section class='secContenu' id='gestionParametres'>");
            out.println(afficherLesUtilisateurs(request, out, utilisateur));
          }
          else {
            if (request.getParameter("modifie") != null) {
              if (utilisateur.getGestionBOutilisateur().modifierUnUtilisateur(request)) {
                out.println("<p class='messagePrincipal' id='messageConfirmation'>Utilisateur modifi&eacute; avec succ&egrave;s</p>");
              }
              else {
                out.println("<p class='messagePrincipal' id='messageErreur'>Echec de la modification de cet utilisateur</p>");
              }
            }
            
            // out.println("<section class='secContenu' id='gestionParametres'>");
            out.println(afficherUnUtilisateur(out, utilisateur, Integer.parseInt(request.getParameter("utilId")), modeModif));
          }
          out.println("</article>");
          out.println(pattern.afficherFin(titreOptions, null));
        }
        else {
          request.getSession().invalidate();
          getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
        }
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la fiche d'un utilisateur
   */
  private String afficherUnUtilisateur(ServletOutputStream out, Utilisateur utilisateur, int id, boolean enModif) {
    Utilisateur util = utilisateur.getGestionBOutilisateur().recupererUnUtilisateur(id);
    if (util == null) {
      return null;
    }
    
    String retour = "";
    try {
      retour += ("<section class='secContenu' id='gestionParametres'>");
      if (enModif) {
        retour += "<h1 class='h1Modification'>";
        retour += "<span id='modeModif'>Modification - </span>";
      }
      else {
        retour += "<h1>";
      }
      retour += "<span class='titreH1'>" + util.getLogin() + "</h1>";
      retour += "<form action='affichageBOutilisateurs' method='post' name='formBOutilisateurs'>";
      retour += "<input type='hidden' name='utilId' value='" + util.getId() + "'/>";
      retour += "<input type='hidden' name='modifie' value='1'/>";
      retour += "<div class='sousBoite' id='uneBoite'>";
      retour += "<h2>Droits</h2>";
      
      // Type de license (flottante ou fixe)
      retour += "<label>Session fixe</label><input type='checkbox' name='util_super' id='util_super' value='1' class='znCheckBox' ";
      
      if (util.isSuperUtilisateur()) {
        retour += "checked ";
      }
      if (enModif) {
        retour += "/><a href='#' class='";
      }
      else {
        retour += "/><div class='";
      }
      // look checkbox si paramètre coché
      if (util.isSuperUtilisateur()) {
        retour += "decorCheckboxB";
      }
      else {
        retour += "decorCheckbox";
      }
      if (enModif) {
        retour += "' onClick=\"switchCheckbox(this,'util_super');\"></a><br/>";
      }
      else {
        retour += "' ></div><br/>";
      }
      
      // Droit d'accès
      retour += "<label>Droits d'acc&egrave;s</label><select name='util_acces' class='znSelect' ";
      if (!enModif) {
        retour += "disabled>";
      }
      else {
        retour += ">";
      }
      if (util.getNiveauAcces() < utilisateur.getNiveauAcces() || utilisateur.getNiveauAcces() <= ConstantesEnvironnement.ACCES_RI) {
        retour += "<option value='" + ConstantesEnvironnement.ACCES_RI + "'";
        if (util.getNiveauAcces() == ConstantesEnvironnement.ACCES_RI) {
          retour += " selected ";
        }
        retour += ">R&eacute;solution informatique</option>";
      }
      if (util.getNiveauAcces() < utilisateur.getNiveauAcces() || utilisateur.getNiveauAcces() <= ConstantesEnvironnement.ACCES_ADMIN) {
        retour += "<option value='" + ConstantesEnvironnement.ACCES_ADMIN + "'";
        if (util.getNiveauAcces() == ConstantesEnvironnement.ACCES_ADMIN) {
          retour += " selected ";
        }
        retour += ">Administrateur</option>";
      }
      // utilisateur classique
      retour += "<option value='" + ConstantesEnvironnement.ACCES_UTIL + "'";
      if (util.getNiveauAcces() == ConstantesEnvironnement.ACCES_UTIL) {
        retour += " selected ";
      }
      retour += ">Utilisateur</option>";
      // utilisateur interdit
      retour += "<option value='" + ConstantesEnvironnement.ACCES_NON + "'";
      if (util.getNiveauAcces() == ConstantesEnvironnement.ACCES_NON) {
        retour += " selected ";
      }
      retour += ">Non autoris&eacute;</option>";
      
      retour += "</select><br/>";
      
      // Visiblité du représentant
      String valueRepr = "";
      if (util.getCodeRepresentant() != null && !util.getCodeRepresentant().trim().equals("")) {
        valueRepr = util.getCodeRepresentant();
      }
      retour += "<label>Visibilité représentant</label><input type='text' name='util_repres' id='util_repres' value='" + valueRepr
          + "' class='entree' maxlength='2'";
      if (!enModif) {
        retour += " disabled />";
      }
      
      // La bibliothèque
      retour += "<br><label>Biblioth&egrave;que</label><input type='text' name='util_bib' id='util_bib' value='" + util.getBaseDeDonnees()
          + "' class='entree' maxlength='10' disabled />";
      retour += "</div>";
      retour += "</form>";
      // Ne laisser la possibilité de modifier qu'à un utilisateur qui a de meilleurs droits que le profil à modifier
      if (util.getNiveauAcces() >= utilisateur.getNiveauAcces() && util.getId() != 1) {
        retour += "<div id='navigationFiche'>";
        if (enModif) {
          retour += "<a href='affichageBOutilisateurs?utilId=" + util.getId()
              + "' id='ficheAnnuler'></a><a href='#' id='ficheValider' onClick=\"soumettreUnFormulaire('formBOutilisateurs')\"></a>";
        }
        else {
          retour += "<a href='affichageBOutilisateurs?utilId=" + util.getId() + "&modeModif=1' id='ficheModifier'></a>";
        }
        
        retour += "</div>";
      }
      
      retour += "</section>";
      
      return retour;
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  
  /**
   * Afficher la liste des utilisateurs du serveur de la mobilité
   */
  private String afficherLesUtilisateurs(HttpServletRequest request, ServletOutputStream out, Utilisateur utilisateur) {
    ArrayList<HttpSession> sessions = ((ArrayList<HttpSession>) getServletContext().getAttribute("sessions"));
    if (getServletContext().getAttribute("sessions") != null) {
      sessions = ((ArrayList<HttpSession>) getServletContext().getAttribute("sessions"));
    }
    
    ArrayList<Utilisateur> liste = utilisateur.getGestionBOutilisateur().recupererLesUtilisateurs();
    String retour = "";
    try {
      retour += "<section class='secContenu' id='gestionParametres'>";
      retour += "<h1><span class='titreH1'>Utilisateurs: " + sessions.size();
      if (sessions.size() > 1) {
        retour += " sessions actives</h1>";
      }
      else {
        retour += " session active</h1>";
      }
      
      if (liste == null || liste.size() == 0) {
        retour += "Pas d'utilisateur disponible";
      }
      else {
        for (int i = 0; i < liste.size(); i++) {
          if (i % 2 == 0) {
            retour += "<div class='listesClassiques'>";
          }
          else {
            retour += "<div class='listesClassiquesF'>";
          }
          
          retour += "<a class='labelParametre' href=\"affichageBOutilisateurs?utilId=" + liste.get(i).getId()
              + "\"><span class='lblPara'>" + liste.get(i).getLogin() + "</span>";
          if (testerPresenceUtilisateur(liste.get(i), sessions)) {
            retour += "<span class='utilConnecte'>Connecté</span>";
          }
          retour += "</a>";
          retour += "</div>";
        }
      }
      
      retour += "</section>";
      
      return retour;
      
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  
  private boolean testerPresenceUtilisateur(Utilisateur utilisateur, ArrayList<HttpSession> sessions) {
    boolean isPresent = false;
    if (sessions == null) {
      return false;
    }
    
    int i = 0;
    while (i < sessions.size() && !isPresent) {
      if (((Utilisateur) sessions.get(i).getAttribute("utilisateur")).getId() == utilisateur.getId()) {
        isPresent = true;
      }
      i++;
    }
    
    return isPresent;
  }
  
}
