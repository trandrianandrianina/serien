/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.backoffice;

import java.util.ArrayList;

import ri.serien.mobilite.environnement.Utilisateur;

public class GestionParametres {
  private Utilisateur utilisateur = null;
  private Parametre parametreEnCours = null;
  
  /**
   * Constructeur de la gestion des paramètres
   */
  public GestionParametres(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  /**
   * récupérer les paramètres auquels accède l'utilisateur
   */
  public ArrayList<Parametre> recupererLesParametres() {
    ArrayList<Parametre> liste = new ArrayList<Parametre>();
    
    liste = utilisateur.getBaseSQLITE().recupererLesParametres(utilisateur);
    
    return liste;
  }
  
  public Parametre getParametreEnCours() {
    return parametreEnCours;
  }
  
  public void setParametreEnCours(Parametre parametreEnCours) {
    this.parametreEnCours = parametreEnCours;
  }
  
}
