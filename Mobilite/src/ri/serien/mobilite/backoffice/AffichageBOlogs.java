/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.backoffice;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.affichage.PatternErgonomie;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class AffichageBOlogs
 */
public class AffichageBOlogs extends HttpServlet {
  
  private int niveauAcces = ConstantesEnvironnement.ACCES_ADMIN;
  private PatternErgonomie pattern = null;
  private int metier = -3;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageBOlogs() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Traiter de manière générique le POST ou le GET
   */
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      int niveau = 2;
      String metaSpecifiques = "";
      String titreOptions = "Options";
      
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        // On créé un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        if (utilisateur.getNiveauAcces() <= niveauAcces) {
          // ancrage dans la page
          String ancrage = request.getParameter("ancrage");
          
          if (utilisateur.getGestionBOlogs() == null) {
            utilisateur.setGestionBOlogs(new GestionBOlogs(utilisateur));
          }
          
          metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
          metaSpecifiques += "<link href='css/listes.css' rel='stylesheet'/>";
          // sélection de logs par date
          if (request.getParameter("idLog") != null) {
            niveau = 3;
            metaSpecifiques += "<link href='css/detailLog.css' rel='stylesheet'/>";
            metaSpecifiques += "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"30\">";
          }
          
          ServletOutputStream out = response.getOutputStream();
          out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
          out.println(pattern.afficherHeader(metier, niveau, null, utilisateur));
          out.println("<article id='contenu'>");
          
          // afficher la liste des logs
          if (niveau == 2) {
            out.println(afficherListeLogs(out, utilisateur));
            // ou le contenu d'un log
          }
          else {
            out.println(afficherUnLog(out, utilisateur, request.getParameter("idLog")));
          }
          
          out.println("</article>");
          out.println(pattern.afficherFin(titreOptions, null));
        }
        else {
          request.getSession().invalidate();
          getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
        }
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher les logs d'une date donnée
   */
  private String afficherUnLog(ServletOutputStream out, Utilisateur utilisateur, String idLog) {
    String retour = "";
    
    /*    if (idLog != null && utilisateur != null) {
      ArrayList<Log> listeLogs = utilisateur.getLogs().getUneDate(idLog);
      ArrayList<Log> listeLogs = null;
      
      retour = "<section class='secContenu' id='gestionParametres'>";
      retour += "<h1><span class='titreH1'>Logs du " + traductionDateHumaine(idLog) + "</span></h1>";
      
      if (listeLogs != null && listeLogs.size() > 0) {
        for (int i = listeLogs.size() - 1; i > -1; i--) {
          if (i % 2 == 0) {
            if (listeLogs.get(i).getTypeLog().equals("E")) {
              retour += "<div class='listesClassiquesE'>";
            }
            else {
              retour += "<div class='listesClassiques'>";
            }
          }
          else {
            if (listeLogs.get(i).getTypeLog().equals("E")) {
              retour += "<div class='listesClassiquesFE'>";
            }
            else {
              retour += "<div class='listesClassiquesF'>";
            }
          }
          retour += "<div class='dateLogs'> " + listeLogs.get(i).getDateLog() + "</div>";
          retour += "<div class='loginLogs'>" + listeLogs.get(i).getLoginLog() + "</div>";
          retour += "<div class='classeLogs'>" + listeLogs.get(i).getClasseLog() + "</div>";
          retour += "<div class='messageLogs'>" + listeLogs.get(i).getMessageLog() + "</div>";
          retour += "</div>";
        }
      }
      else {
        retour += "<p>Pas de logs pour la date choisie</p>";
      }
    }
    else {
      retour += "<p>Erreur sur le fichier de logs</p>";
    }
    
    retour += "</section>";*/
    
    return retour;
  }
  
  /**
   * Afficher la liste complète des logs par date
   */
  private String afficherListeLogs(ServletOutputStream out, Utilisateur utilisateur) {
    String retour = "";
    /*    String[] fichiers = utilisateur.getLogs().getTousLogs();
    
    retour = "<section class='secContenu' id='gestionParametres'>";
    if (fichiers != null) {
      int j = 0;
      retour += "<h1><span class='titreH1'>Liste des logs</span></h1>";
      // for(int i = 0; i < fichiers.length; i++)
      for (int i = fichiers.length - 1; i > -1; i--) {
        if (fichiers[i].endsWith(".xml")) {
          if (j % 2 == 0) {
            retour += "<div class='listesClassiques'>";
          }
          else {
            retour += "<div class='listesClassiquesF'>";
          }
          
          retour += "<a class='labelParametre' href=\"affichageBOlogs?idLog=" + fichiers[i].substring(0, 6) + "\"><span class='lblPara'>"
              + traductionDateHumaine(fichiers[i]) + "</span></a>";
          
          retour += "</div>";
          j++;
        }
      }
      if (j == 0) {
        retour += "<p>Pas de fichier logs dans ce dossier</p>";
      }
    }
    else {
      retour += "<p>Pas de fichier logs dans ce dossier</p>";
    }
    
    retour += "</section>";*/
    
    return retour;
  }
  
  /**
   * Traduire le nom du fichier en date humainement lisible
   */
  private String traductionDateHumaine(String date) {
    String retour = "";
    
    retour = date.substring(4, 6) + "/" + date.substring(2, 4) + "/" + date.substring(0, 2);
    
    return retour;
  }
}
