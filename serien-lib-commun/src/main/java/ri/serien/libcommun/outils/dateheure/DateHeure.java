/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.dateheure;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Retourne la date et l'heure.
 */
public class DateHeure {
  // Constantes
  public static final String FORMAT8_DATE = "dd.MM.yy";
  
  public static final int AAAAMMJJ = 0;
  public static final int AAMMJJ = 1;
  public static final int JJMMAAAA = 2;
  public static final int JJMMAA = 3;
  public static final int HHMMSS = 4;
  public static final int HH_MM_SS = 5;
  public static final int HHMMSSMS = 6;
  public static final int HH_MM_SS_MS = 7;
  
  // Où '_' doit être remplacé par '/'
  public static final int JJ_MM_AAAA = 12;
  public static final int SAAMMJJ = 13;
  public static final int HHMM = 14;
  // Où '_' doit être remplacé par ':'
  public static final int HH_MM = 15;
  // Où '_' doit être remplacé par ':'
  public static final int HH_MM_SS_ = 16;
  // Où '_' doit être remplacé par '/' et ':'
  public static final int JJ_MM_AAAA_A_HH_MM_SS = 17;
  // Où '_' doit être remplacé par '-' (utilisé dans les requêtes avec des timestamps)
  public static final int AAAA_MM_JJ = 18;
  // Où '_' doit être remplacé par '-' (utilisé dans les requêtes avec des timestamps)
  public static final int AAAA_MM_JJ_HH_MM_SS = 19;
  
  /**
   * Formate la date et l'heure sous forme de chaine de caractères.
   */
  public static String getJourHeure(int type) {
    return getJourHeure(type, new Date());
  }
  
  /**
   * Formate la date et l'heure sous forme de chaine de caractères.
   */
  public static String getJourHeure(int pType, Date pDate) {
    if (pDate == null) {
      return "";
    }
    switch (pType) {
      case 0:
        // (jour+"/"+mois+"/"+annee+""+heure+":"+minute+":"+seconde+"."+milliseconde);
        return String.format("%1$td/%1$tm/%1$tY %1$tH:%1$tM:%1$tS.%1$tL", pDate);
      case 1:
        // (heure+":"+minute+":"+seconde+"."+milliseconde);
        return String.format("%1$tH:%1$tM:%1$tS.%1$tL", pDate);
      case 2:
        // (annee+mois+jour+","+heure+minute+seconde);
        return String.format("%1$tY%1$tm%1$td,%1$tH%1$tM%1$tS", pDate);
      case 3:
        // (annee+mois+jour+heure+minute);
        return String.format("%1$tY%1$tm%1$td%1$tH%1$tM", pDate);
      case 4:
        // (annee+mois+jour);
        return String.format("%1$tY%1$tm%1$td", pDate);
      case 5:
        // (annee.substring(2, 4)+mois+jour);
        return String.format("%1$ty%1$tm%1$td", pDate);
      case 6:
        // (jour+mois+annee);
        return String.format("%1$td%1$tm%1$tY", pDate);
      case 7:
        // (jour+mois+annee.substring(2, 4));
        return String.format("%1$td%1$tm%1$ty", pDate);
      case 8:
        // (heure+minute+seconde);
        return String.format("%1$tH%1$tM%1$tS", pDate);
      case 9:
        // (heure+'.'+minute+'.'+seconde);
        return String.format("%1$tH.%1$tM.%1$tS", pDate);
      case 10:
        // (heure+minute+seconde+milliseconde);
        return String.format("%1$tH%1$tM%1$tS%1$tL", pDate);
      case 11:
        // (heure+'.'+minute+'.'+seconde+'.'+milliseconde);
        return String.format("%1$tH.%1$tM.%1$tS.%1$tL", pDate);
      case 12:
        // (jour+"/"+mois+"/"+annee);
        return String.format("%1$td/%1$tm/%1$tY", pDate);
      case 13:
        // (siecle+annee+mois+jour);
        String chaine = String.format("%1$tY%1$tm%1$td", pDate);
        if (chaine.substring(0, 2).equals("19")) {
          return "0" + chaine.substring(2);
        }
        else {
          return "1" + chaine.substring(2);
        }
      case 14:
        // (heure+minute);
        return String.format("%1$tH%1$tM", pDate);
      case 15:
        // (heure+":"+minute);
        return String.format("%1$tH:%1$tM", pDate);
      case 16:
        // (heure+":"+minute+":"+seconde);
        return String.format("%1$tH:%1$tM:%1$tS", pDate);
      case 17:
        // (jour+"/"+mois+"/"+annee+" à "+heure+":"+minute+":"+seconde);
        return String.format("%1$td/%1$tm/%1$tY à %1$tH:%1$tM:%1$tS", pDate);
      case 18:
        // (année+"-"+mois+"-"+jour");
        return String.format("%1$tY-%1$tm-%1$td", pDate);
      case 19:
        // (année+"-"+mois+"-"+jour heure+":"+minute+":"+seconde+"."+milliseconde);
        return String.format("%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS", pDate);
      case 20:
        // (annee+mois+jour+"-"+heure+minute+seconde);
        return String.format("%1$tY%1$tm%1$td-%1$tH%1$tM%1$tS", pDate);
      
      default:
        break;
    }
    return "";
  }
  
  /**
   * Converti une chaine dateheure d'un format vers un autre.
   */
  public static String getConvertJourHeure(int formatori, int formatconv, String dateheure, String valeurpardefaut) {
    if (dateheure == null) {
      return valeurpardefaut;
    }
    
    switch (formatori) {
      // (siecle+annee+mois+jour)
      case 13:
        if (dateheure.length() == 7) {
          switch (formatconv) {
            case 12:
              // (jour+"/"+mois+"/"+annee)
              dateheure = dateheure.substring(5) + '/' + dateheure.substring(3, 5) + '/' + (dateheure.length() == 6 ? "19" : "20")
                  + dateheure.substring(1, 3);
              break;
            default:
              break;
          }
        }
        else {
          dateheure = valeurpardefaut;
        }
        break;
      
      // (heure+minute)
      case 14:
        switch (formatconv) {
          case 15:
            // (heure+":"+minute)
            if (dateheure.length() == 4) {
              dateheure = dateheure.substring(0, 2) + ':' + dateheure.substring(2);
            }
            else if (dateheure.length() == 3) {
              dateheure = '0' + dateheure.substring(0, 1) + ':' + dateheure.substring(1);
            }
            else if (dateheure.equals("0")) {
              dateheure = "";
            }
            else {
              dateheure = "Err";
            }
            break;
          default:
            dateheure = valeurpardefaut;
        }
        break;
      
      default:
        break;
    }
    
    return dateheure;
  }
  
  /**
   * Formate la date et l'heure sous forme d'entier: AAMMJJHHMM.
   */
  public long getintJourHeure() {
    return Long.parseLong(getJourHeure(3).substring(2));
  }
  
  /**
   * Remplace la variable date par la date exacte formatée à la demande.
   */
  public static String getFormateDateHeure(String var) {
    if (var == null) {
      return "";
    }
    var = var.trim();
    if (var.equals("@_AAAAMMJJ_@")) {
      return getJourHeure(4);
    }
    else if (var.equals("@_AAMMJJ_@")) {
      return getJourHeure(5);
    }
    else if (var.equals("@_JJMMAAAA_@")) {
      return getJourHeure(6);
    }
    else if (var.equals("@_JJMMAA_@")) {
      return getJourHeure(7);
    }
    else if (var.equals("@_HHMMSS_@")) {
      return getJourHeure(8);
    }
    else if (var.equals("@_HH.MM.SS_@")) {
      return getJourHeure(9);
    }
    else if (var.equals("@_HHMMSSMS_@")) {
      return getJourHeure(10);
    }
    else if (var.equals("@_HH.MM.SS.MS_@")) {
      return getJourHeure(11);
    }
    return "";
  }
  
  /**
   * Formate la date.
   */
  public static String getFormateDateHeure(int type) {
    switch (type) {
      case AAAAMMJJ:
        return getJourHeure(4);
      case AAMMJJ:
        return getJourHeure(5);
      case JJMMAAAA:
        return getJourHeure(6);
      case JJMMAA:
        return getJourHeure(7);
      case HHMMSS:
        return getJourHeure(8);
      case HH_MM_SS:
        return getJourHeure(9);
      case HHMMSSMS:
        return getJourHeure(10);
      case HH_MM_SS_MS:
        return getJourHeure(11);
      case JJ_MM_AAAA:
        return getJourHeure(JJ_MM_AAAA);
      case JJ_MM_AAAA_A_HH_MM_SS:
        return getJourHeure(JJ_MM_AAAA_A_HH_MM_SS);
      default:
        break;
    }
    return "";
  }
  
  /**
   * Scanne une chaine et remplace les variables date par les dates exactes formatées à la demande.
   */
  public static String getScanDateHeure(String var) {
    if (var == null) {
      return "";
    }
    int pos = var.indexOf("@_AAAAMMJJ_@");
    if (pos != -1) {
      var = var.replaceAll("@_AAAAMMJJ_@", getJourHeure(4));
    }
    pos = var.indexOf("@_AAMMJJ_@");
    if (pos != -1) {
      var = var.replaceAll("@_AAMMJJ_@", getJourHeure(5));
    }
    pos = var.indexOf("@_JJMMAAAA_@");
    if (pos != -1) {
      var = var.replaceAll("@_JJMMAAAA_@", getJourHeure(6));
    }
    pos = var.indexOf("@_JJMMAA_@");
    if (pos != -1) {
      var = var.replaceAll("@_JJMMAA_@", getJourHeure(7));
    }
    
    pos = var.indexOf("@_HHMMSS_@");
    if (pos != -1) {
      var = var.replaceAll("@_HHMMSS_@", getJourHeure(8));
    }
    pos = var.indexOf("@_HH.MM.SS_@");
    if (pos != -1) {
      var = var.replaceAll("@_HH.MM.SS_@", getJourHeure(9));
    }
    pos = var.indexOf("@_HHMMSSMS_@");
    if (pos != -1) {
      var = var.replaceAll("@_HHMMSSMS_@", getJourHeure(10));
    }
    pos = var.indexOf("@_HH.MM.SS.MS_@");
    if (pos != -1) {
      var = var.replaceAll("@_HH.MM.SS.MS_@", getJourHeure(11));
    }
    
    return var;
  }
  
  /**
   * Retourne la variable formatée.
   */
  public static String getNomVariable(int type) {
    switch (type) {
      // Date
      case JJMMAAAA:
        return "@_JJMMAAAA_@";
      case JJMMAA:
        return "@_JJMMAA_@";
      case AAAAMMJJ:
        return "@_AAAAMMJJ_@";
      case AAMMJJ:
        return "@_AAMMJJ_@";
      // Heure
      case HHMMSS:
        return "@_HHMMSS_@";
      case HH_MM_SS:
        return "@_HH.MM.SS_@";
      case HHMMSSMS:
        return "@_HHMMSSMS_@";
      case HH_MM_SS_MS:
        return "@_HH.MM.SS.MS_@";
      
      default:
        return "";
    }
  }
  
  /**
   * Effectue une soustraction entre pDate1 et pDate2 et retoune la différence en seconde (date1 - date2).
   */
  public static long getDeltaEnSecondeEntreDate(Date pDate1, Date pDate2) {
    if (pDate1 == null || pDate2 == null) {
      throw new MessageErreurException("Une des deux dates est invalide.");
    }
    long d = pDate1.getTime() - pDate2.getTime();
    return d / 1000;
  }
  
  /**
   * Converti une date au format JJ.MM.AA en Date.
   * 
   * @param pDateFormat8
   * @return
   */
  public static Date convertirFormat8VersDate(String pDateFormat8) {
    pDateFormat8 = Constantes.normerTexte(pDateFormat8);
    if (pDateFormat8.isEmpty() || pDateFormat8.length() < 8) {
      return null;
    }
    try {
      return new SimpleDateFormat(FORMAT8_DATE).parse(pDateFormat8);
    }
    catch (ParseException e) {
    }
    return null;
  }
}
