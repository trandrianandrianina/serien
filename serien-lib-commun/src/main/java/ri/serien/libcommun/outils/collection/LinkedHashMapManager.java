/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.collection;

import java.util.EventListener;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.swing.event.EventListenerList;

/**
 * Gestion dd'évènement sur une LinkedHashMap.
 */
public class LinkedHashMapManager {
  // Variables
  private EventListenerList listeners = new EventListenerList();
  private LinkedHashMap<Object, Object> map = new LinkedHashMap<Object, Object>();
  private boolean avtClear = false;
  private boolean avtAdd = false;
  private boolean avtRemove = true;
  
  /**
   * Constructeur.
   */
  public LinkedHashMapManager() {
    this(false, false, true);
  }
  
  /**
   * Constructeur.
   */
  public LinkedHashMapManager(boolean avtclear, boolean avtadd, boolean avtremove) {
    avtClear = avtclear;
    avtAdd = avtadd;
    avtRemove = avtremove;
  }
  
  public static interface ILinkedHashMapListener extends EventListener {
    public void onDataCleared();
    
    public void onDataAdded(Object cle, Object val);
    
    public void onDataRemoved(Object cle);
  }
  
  public void addListener(ILinkedHashMapListener l) {
    listeners.add(ILinkedHashMapListener.class, l);
  }
  
  public void removeListener(ILinkedHashMapListener l) {
    listeners.remove(ILinkedHashMapListener.class, l);
  }
  
  public LinkedHashMap<Object, Object> getHashMap() {
    return map;
  }
  
  /**
   * Effacer les objets de la hashmpap.
   */
  public void clearObject() {
    if (avtClear) {
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(ILinkedHashMapListener.class);
      for (int i = 0; i < el.length; i++) {
        ((ILinkedHashMapListener) el[i]).onDataCleared();
      }
      map.clear();
    }
    else {
      map.clear();
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(ILinkedHashMapListener.class);
      for (int i = 0; i < el.length; i++) {
        ((ILinkedHashMapListener) el[i]).onDataCleared();
      }
    }
  }
  
  /**
   * Ajouter un objet à la hashmap.
   */
  public void addObject(Object cle, Object val) {
    if (avtAdd) {
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(ILinkedHashMapListener.class);
      for (int i = 0; i < el.length; i++) {
        ((ILinkedHashMapListener) el[i]).onDataAdded(cle, val);
      }
      map.put(cle, val);
    }
    else {
      map.put(cle, val);
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(ILinkedHashMapListener.class);
      for (int i = 0; i < el.length; i++) {
        ((ILinkedHashMapListener) el[i]).onDataAdded(cle, val);
      }
    }
  }
  
  /**
   * Supprimer un objet de la hashmap.
   */
  public void removeObject(Object cle) {
    if (avtRemove) {
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(ILinkedHashMapListener.class);
      for (int i = 0; i < el.length; i++) {
        ((ILinkedHashMapListener) el[i]).onDataRemoved(cle);
      }
      map.remove(cle);
    }
    else {
      map.remove(cle);
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(ILinkedHashMapListener.class);
      for (int i = 0; i < el.length; i++) {
        ((ILinkedHashMapListener) el[i]).onDataRemoved(cle);
      }
    }
  }
  
  /**
   * Récupérer la clé d'un objet.
   */
  public Object getObject(Object cle) {
    return map.get(cle);
  }
  
  /**
   * Récupérer un objet via sa clé.
   */
  public Object getValue(Object val) {
    Set<Object> myKeys = map.keySet();
    Iterator<Object> myKeysIterator = myKeys.iterator();
    
    while (myKeysIterator.hasNext()) {
      Object cle = myKeysIterator.next();
      Object valeur = map.get(cle);
      if (valeur.equals(val)) {
        return cle;
      }
    }
    return null;
  }
  
  /**
   * Récupérer un objet via son index.
   */
  public Object getKeyAtIndex(int index) {
    if (index < map.size()) {
      Set<Object> myKeys = map.keySet();
      Iterator<Object> myKeysIterator = myKeys.iterator();
      
      while (myKeysIterator.hasNext()) {
        Object cle = myKeysIterator.next();
        if (index-- == 0) {
          return cle;
        }
      }
    }
    return null;
  }
  
  /**
   * Récupérer la clé associée à un index.
   */
  public Object getValueAtIndex(int index) {
    return map.get(getKeyAtIndex(index));
  }
  
}
