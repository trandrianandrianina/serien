/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session;

import java.io.File;
import java.io.Serializable;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

public class Dossier implements Serializable {
  // Variables
  private boolean coteServeur = true;
  private String uuid = null;
  private char separateur = '/';
  private String nomDossierRacine = separateur + Constantes.DOSSIER_RACINE;
  private File dossierRacine = null;
  private String nomDossierConfig = null;
  private File dossierConfig = null;
  // Chemin de travail de l'environnement choisit
  private String nomDossierTravail = null;
  private File dossierTravail = null;
  private String nomDossierMenu = null;
  private File dossierMenu = null;
  private String nomDossierTemp = null;
  private File dossierTemp = null;
  private String nomDossierTempUtilisateur = null;
  private File dossierTempUtilisateur = null;
  private String nomDossierWeb = null;
  private File dossierWeb = null;
  private String nomDossierRT = null;
  private File dossierRT = null;
  
  /**
   * Constructeur.
   */
  public Dossier(boolean pCoteServeur, String pUudi) {
    coteServeur = pCoteServeur;
    if (!coteServeur) {
      separateur = File.separatorChar;
      // Coté desktop le nom du dossier est tronqué à 8 caractères car sinon dans certains cas la longueur du chemin est trop
      // longue et windows affiche un message d'erreur (ou rien ce qui n'est pas mieux car le document n'est jamais affiché)
      if (pUudi != null) {
        uuid = pUudi.substring(0, 8);
      }
    }
    else {
      uuid = pUudi;
    }
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise et créer le dossier racine.
   */
  public void setDossierRacine(String pNomDossier) {
    nomDossierRacine = Constantes.normerTexte(pNomDossier);
    if (nomDossierRacine.isEmpty()) {
      return;
    }
    
    // Le dossier contenant les fichiers de configuration
    nomDossierConfig = nomDossierRacine + separateur + Constantes.DOSSIER_CONFIG;
    
    // Le dossier contenant les fichiers des menus personnalisés
    nomDossierMenu = nomDossierConfig + separateur + Constantes.DOSSIER_MENUS;
    
    // Le dossier contenant les fichiers temporaires
    nomDossierTemp = nomDossierRacine + separateur + Constantes.DOSSIER_TMP;
    
    // Le dossier contenant les fichiers temporaires pour un utilisateur
    nomDossierTempUtilisateur = nomDossierTemp + separateur + uuid;
    
    // Le dossier contenant les fichiers du desktop
    nomDossierWeb = nomDossierRacine + separateur + Constantes.DOSSIER_WEB;
    
    // Le dossier contenant les fichiers des différents environnements
    nomDossierRT = nomDossierRacine + separateur + Constantes.DOSSIER_RT;
  }
  
  /**
   * Initialise le dossier de l'environnement de travail sélectionné.
   */
  public void initialiserDossierTravail(String pNomDossierEnvironnement) {
    pNomDossierEnvironnement = Constantes.normerTexte(pNomDossierEnvironnement);
    if (pNomDossierEnvironnement.isEmpty()) {
      throw new MessageErreurException("Le dossier contenant l'environnement sélectionné est invalide.");
    }
    
    // Le dossier contenant les fichiers de l'environnement
    nomDossierTravail = nomDossierRacine + separateur + Constantes.DOSSIER_RT + separateur + pNomDossierEnvironnement + separateur;
  }
  
  /**
   * Permet de tester les droits d'écriture sur le dossier racine (cas du home sur certaines configurations de sessions Windows).
   */
  public boolean isCanWrite() {
    File dossierRacine = getDossierRacine();
    if (dossierRacine == null) {
      return false;
    }
    return dossierRacine.canWrite();
  }
  
  // -- Accesseurs
  
  public String getNomDossierRacine() {
    return nomDossierRacine;
  }
  
  public File getDossierRacine() {
    if (dossierRacine == null && nomDossierRacine != null) {
      dossierRacine = new File(nomDossierRacine);
      if (!dossierRacine.exists()) {
        dossierRacine.mkdirs();
      }
    }
    return dossierRacine;
  }
  
  public String getNomDossierConfig() {
    return nomDossierConfig;
  }
  
  public File getDossierConfig() {
    if (dossierConfig == null && nomDossierConfig != null) {
      dossierConfig = new File(nomDossierConfig);
      if (!dossierConfig.exists()) {
        dossierConfig.mkdirs();
      }
    }
    return dossierConfig;
  }
  
  public String getNomDossierTravail() {
    return nomDossierTravail;
  }
  
  public File getDossierTravail() {
    if (dossierTravail == null && nomDossierTravail != null) {
      dossierTravail = new File(nomDossierTravail);
      if (!dossierTravail.exists()) {
        dossierTravail.mkdirs();
      }
    }
    return dossierTravail;
  }
  
  public String getNomDossierMenu() {
    return nomDossierMenu;
  }
  
  public File getDossierMenu() {
    if (dossierMenu == null && nomDossierMenu != null) {
      dossierMenu = new File(nomDossierMenu);
      if (!dossierMenu.exists()) {
        dossierMenu.mkdirs();
      }
    }
    return dossierMenu;
  }
  
  public String getNomDossierTemp() {
    return nomDossierTemp;
  }
  
  public File getDossierTemp() {
    if (dossierTemp == null && nomDossierTemp != null) {
      dossierTemp = new File(nomDossierTemp);
      if (!dossierTemp.exists()) {
        dossierTemp.mkdirs();
      }
    }
    return dossierTemp;
  }
  
  public String getNomDossierTempUtilisateur() {
    return nomDossierTempUtilisateur;
  }
  
  public File getDossierTempUtilisateur() {
    if (dossierTempUtilisateur == null && nomDossierTempUtilisateur != null) {
      dossierTempUtilisateur = new File(nomDossierTempUtilisateur);
      if (!dossierTempUtilisateur.exists()) {
        dossierTempUtilisateur.mkdirs();
      }
    }
    return dossierTempUtilisateur;
  }
  
  public String getNomDossierWeb() {
    return nomDossierWeb;
  }
  
  public File getDossierWeb() {
    if (dossierWeb == null && nomDossierWeb != null) {
      dossierWeb = new File(nomDossierWeb);
      if (!dossierWeb.exists()) {
        dossierWeb.mkdirs();
      }
    }
    return dossierWeb;
  }
  
  public String getNomDossierRT() {
    return nomDossierRT;
  }
  
  public File getDossierRT() {
    if (dossierRT == null && nomDossierRT != null) {
      dossierRT = new File(nomDossierRT);
      if (!dossierRT.exists()) {
        dossierRT.mkdirs();
      }
    }
    return dossierRT;
  }
  
  public char getSeparateur() {
    return separateur;
  }
}
