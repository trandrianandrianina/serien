/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.protocolemessage;

public class BaseRequest {
  // Constantes
  public static final int NOACTION = 0;
  
  // Variables
  protected int actions = NOACTION;
  protected boolean success = false;
  protected String msgError = null;
  
  // --> Accesseurs <--------------------------------------------------------
  
  /**
   * @return le action.
   */
  public int getActions() {
    return actions;
  }
  
  /**
   * action à définir.
   */
  public void setActions(int actions) {
    this.actions = actions;
  }
  
  /**
   * @return le success.
   */
  public boolean isSuccess() {
    return success;
  }
  
  /**
   * @param success le success à définir.
   */
  public void setSuccess(boolean success) {
    this.success = success;
  }
  
  /**
   * @return le msgError.
   */
  public String getMsgError() {
    return msgError;
  }
  
  /**
   * @param msgError le msgError à définir.
   */
  public void setMsgError(String msgError) {
    this.msgError = msgError;
  }
  
}
