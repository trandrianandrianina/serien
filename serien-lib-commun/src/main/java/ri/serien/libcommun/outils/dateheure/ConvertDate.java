/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.dateheure;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

/**
 * Permet de convertir une date au format JJ.MM.AA en 0/1AAMMJJ et inversement.
 */
public class ConvertDate {
  // Constantes
  public static final char SEPARATOR = '.';
  
  /**
   * Converti une date de type JJ.MM.AA ou JJ.MM.AAAA en 0/1AAMMJJ.
   */
  public static String toDb2(String pDate) {
    return toDb2(pDate, "\\" + SEPARATOR);
  }
  
  /**
   * Converti une date de type JJ.MM.AA ou JJ.MM.AAAA en 0/1AAMMJJ.
   */
  public static String toDb2(String pDate, String pSeparator) {
    if (pDate == null) {
      return "";
    }
    
    String[] decoupage = pDate.split(pSeparator);
    if (decoupage.length != 3) {
      return "";
    }
    
    StringBuilder newdate = new StringBuilder(7);
    
    // Siècle
    // Dans le cas où c'est codé en AA
    if (decoupage[2].length() < 4) {
      int siecle = Integer.parseInt(decoupage[2]);
      if (siecle < 30) {
        newdate.append('1');
      }
      else {
        newdate.append('0');
      }
      newdate.append(decoupage[2]);
      // Dans le cas où c'est codé en AAAA
    }
    else {
      if (decoupage[2].charAt(0) == '2') {
        newdate.append('1');
      }
      else {
        newdate.append('0');
      }
      newdate.append(decoupage[2].substring(2));
    }
    
    // Mois (TODO ajouter un contrôle sur le nombre <= 12)
    newdate.append(decoupage[1]);
    
    // Jour (TODO ajouter un contrôle sur le nombre <= 31)
    newdate.append(decoupage[0]);
    
    return newdate.toString();
  }
  
  /**
   * Converti une date de type 0/1AAMMJJ en JJ.MM.AA ou JJ.MM.AAAA.
   */
  public static String toNormal(String pDate, boolean pComplete) {
    return toNormal(pDate, SEPARATOR, pComplete);
  }
  
  /**
   * Converti une date de type 0/1AAMMJJ ou AAAAMMJJ en JJ.MM.AA ou JJ.MM.AAAA.
   */
  public static String toNormal(String pDate, char pSeparator, boolean pComplete) {
    if ((pDate == null) || (pDate.length() < 6)) {
      return "";
    }
    
    StringBuilder newdate = new StringBuilder(9);
    
    if (pDate.length() == 6) { // 0AAMMJJ
      // Jour
      newdate.append(pDate.substring(4)).append(pSeparator);
      // Mois
      newdate.append(pDate.substring(2, 4)).append(pSeparator);
      // Année
      newdate.append(pDate.substring(0, 2));
    }
    else if (pDate.length() == 7) { // 1AAMMJJ
      // Jour
      newdate.append(pDate.substring(5)).append(pSeparator);
      // Mois
      newdate.append(pDate.substring(3, 5)).append(pSeparator);
      // Année
      newdate.append(pDate.substring(1, 3));
    }
    else if (pDate.length() == 8) { // AAAAMMJJ
      // Jour
      newdate.append(pDate.substring(6)).append(pSeparator);
      // Mois
      newdate.append(pDate.substring(4, 6)).append(pSeparator);
      // Année
      newdate.append(pDate.substring(0, 4));
    }
    
    // Si besoin on met le siècle sur 4 chiffres
    if (pComplete) {
      if (pDate.length() == 6) {
        newdate.insert(6, "19");
      }
      else if (pDate.length() == 7) {
        newdate.insert(6, pDate.charAt(0) == '1' ? "20" : "19");
      }
    }
    
    return newdate.toString();
  }
  
  /**
   * Converti une date de type 0/1AAMMJJ ou AAAAMMJJ en objet date (timestamp).
   */
  public static Date db2ToDate(int pDate, TimeZone pTimeZone) {
    String datetmp = toNormal("" + pDate, true);
    if (datetmp.equals("")) {
      return null;
    }
    DateFormat dfm = new SimpleDateFormat("dd.MM.yyyy");
    if (pTimeZone != null) {
      // dfm.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
      dfm.setTimeZone(pTimeZone);
    }
    try {
      return dfm.parse(datetmp);
    }
    catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }
  
  /**
   * Converti une date de type 0/1AAMMJJ, JJ.MM.AAAA ou AAAAMMJJ et une heure (HHMM ou HHMMSS) en objet date (timestamp).
   */
  public static Date db2ToDate(String pDate, int pHeure, TimeZone pTimeZone) {
    if (pDate == null || pDate.replace(".", "").trim().isEmpty() || pDate.trim().equals("0")) {
      return null;
    }
    Date date = null;
    DateFormat dfm = null;
    String heure = "";
    String dateheure = "";
    int longueurHeure = 4;
    // Analyse de l'heure
    if (pHeure > 0) {
      // Heure au format HHMM
      if (pHeure < Constantes.valeurMaxZoneNumerique(4)) {
        longueurHeure = 4;
      }
      // Heure au format HHMMSS
      else if (pHeure < Constantes.valeurMaxZoneNumerique(6)) {
        longueurHeure = 6;
      }
    }
    // Si date sous la forme JJ.MM.YY
    if (pDate.contains(".")) {
      dfm = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
      heure = String.format("%0" + longueurHeure + "d", pHeure);
      if (longueurHeure == 4) {
        heure = heure.substring(0, 2) + ":" + heure.substring(2) + ":" + "00";
      }
      else {
        heure = heure.substring(0, 2) + ":" + heure.substring(2, 4) + ":" + heure.substring(4);
      }
      dateheure = pDate + " " + heure;
    }
    else {
      dfm = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
      heure = String.format("%0" + longueurHeure + "d", pHeure);
      if (longueurHeure == 4) {
        dateheure = String.format("%s %s:%s:00", toNormal(pDate, '.', true), heure.substring(0, 2), heure.substring(2));
      }
      else {
        dateheure =
            String.format("%s %s:%s:%s", toNormal(pDate, '.', true), heure.substring(0, 2), heure.substring(2, 4), heure.substring(4));
      }
    }
    try {
      date = dfm.parse(dateheure);
    }
    catch (ParseException e) {
      Trace.erreur(e, "Erreur lors de la conversion de la date en Timestamp.");
    }
    
    return date;
  }
  
  /**
   * Converti un objet date (timestamp) en entier 0/1AAMMJJ.
   */
  public static int dateToDb2(Date pDate) {
    int date = 0;
    if (pDate == null) {
      return date;
    }
    String datestr = String.format("%1$tY%1$tm%1$td", pDate);
    if (datestr.substring(0, 2).equals("19")) {
      datestr = datestr.substring(2);
    }
    else {
      datestr = '1' + datestr.substring(2);
    }
    date = Integer.parseInt(datestr);
    return date;
  }
  
  /**
   * Converti un objet date (timestamp) en entier HHMMSS ou HHMM.
   */
  public static int dateToHeureDb2(Date pDate, boolean pAvecSecondes) {
    int date = 0;
    if (pDate == null) {
      return date;
    }
    SimpleDateFormat dfm = null;
    if (pAvecSecondes) {
      dfm = new SimpleDateFormat("HHmmss");
    }
    else {
      dfm = new SimpleDateFormat("HHmm");
    }
    try {
      date = Integer.parseInt(dfm.format(pDate.getTime()));
    }
    catch (Exception e) {
    }
    return date;
  }
  
  /**
   * Contrôle que la chaine soit numérique.
   */
  public static boolean isNumeric(String chaine) {
    if (chaine == null) {
      return false;
    }
    
    StringBuilder sb = new StringBuilder(chaine.trim());
    for (int i = 0; i < sb.length(); i++) {
      if ((sb.charAt(i) != '0') && (sb.charAt(i) != '1') && (sb.charAt(i) != '2') && (sb.charAt(i) != '3') && (sb.charAt(i) != '4')
          && (sb.charAt(i) != '5') && (sb.charAt(i) != '6') && (sb.charAt(i) != '7') && (sb.charAt(i) != '8') && (sb.charAt(i) != '9')
          && (sb.charAt(i) != Constantes.SEPARATEUR_DECIMAL_CHAR) && (sb.charAt(i) != Constantes.SEPARATEUR_MILLIER_CHAR)) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Converti une date pour db2 (Série N).
   */
  public static int convertDate4DataBase(String pDate) {
    if (pDate.isEmpty() || ((pDate.length() == 1) && (pDate.charAt(0)) == '0')) {
      return 0;
    }
    if (pDate.length() < 8) {
      return -1;
    }
    
    StringBuilder sb = new StringBuilder(7);
    if (pDate.length() == 10) {
      if (pDate.charAt(6) == '2') {
        sb.append('1');
      }
      sb.append(pDate.substring(8));
    }
    else if (pDate.length() == 8) {
      // Si date sur 8 alors on part du principe que l'on est au dela de l'an 2000
      sb.append('1');
      sb.append(pDate.substring(6));
    }
    sb.append(pDate.substring(3, 5));
    sb.append(pDate.substring(0, 2));
    
    try {
      return Integer.parseInt(sb.toString());
    }
    catch (Exception e) {
      return -1;
    }
  }
  
  /**
   * Converti une heure pour db2 (Série N).
   */
  public static int convertHeure4DataBase(String pHeure) {
    if (pHeure.isEmpty() || ((pHeure.length() == 1) && (pHeure.charAt(0)) == '0')) {
      return 0;
    }
    StringBuilder sb = new StringBuilder(pHeure);
    if (pHeure.length() == 3) {
      sb.insert(0, '0');
    }
    else if (pHeure.length() == 2) {
      sb.append("00");
    }
    else if (pHeure.length() == 1) {
      sb.insert(0, '0').append("00");
    }
    else if (pHeure.length() >= 5) {
      sb.deleteCharAt(2);
    }
    
    try {
      return Integer.parseInt(sb.toString());
    }
    catch (Exception e) {
      return -1;
    }
  }
  
  public static String formateObjetDateEnString(Date pDate, String pFormat) {
    SimpleDateFormat formater = null;
    // Si le format est null on en propose un
    if (pFormat == null) {
      pFormat = "jj/MM/yy";
    }
    // par défaut on prend la date du jour
    if (pDate == null) {
      pDate = new Date();
    }
    
    try {
      formater = new SimpleDateFormat(pFormat);
      return formater.format(pDate);
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    
    /*formater = new SimpleDateFormat("dd-MM-yy");  27-06-06
    formater = new SimpleDateFormat("ddMMyy"); 270606
    formater = new SimpleDateFormat("yyMMdd");  060627
    formater = new SimpleDateFormat("h:mm a"); 9:37 PM
    formater = new SimpleDateFormat("K:mm a, z"); 9:37 PM, CEST
    formater = new SimpleDateFormat("hh:mm a, zzzz"); 09:37 PM, Heure d'été d'Europe centrale
    formater = new SimpleDateFormat("EEEE, d MMM yyyy"); mardi, 27 juin 2006
    formater = new SimpleDateFormat("'le' dd/MM/yyyy 'à' hh:mm:ss"); le 27/06/2006 à 09:37:10
    formater = new SimpleDateFormat("'le' dd MMMM yyyy 'à' hh:mm:ss");
    formater = new SimpleDateFormat("dd MMMMM yyyy GGG, hh:mm aaa");27 juin 2006 ap. J.-C., 09:37 PM
    formater = new SimpleDateFormat("yyyyMMddHHmmss");20060627213710*/
  }
  
}
