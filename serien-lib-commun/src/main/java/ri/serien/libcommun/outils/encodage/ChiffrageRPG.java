/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.encodage;

/**
 * Cette classe contient l'algorithme de chiffrage/déchiffrage de texte utilisé par les programmes RPG.
 */
public class ChiffrageRPG {
  // Constantes
  // Le caractère \u00a5 remplace le caracère \u00a8 original car le code caractère est différent entre UTF-8 et ISO-8859
  // L'idée est de changer ce caractère à la volée lors de l'écriture et de la lecture du champ DRT en table. Ainsi ensuite on peut
  // utiliser cette clé sans problème car le code \u00a5 est le même dans les 2 codes page à la différence de \u00a8
  private static final String cle = " <>1AQW2ZSX3EDC4RFV5TGB6YHN7UJ?8IK.9OL/0PM+°\u00a5%_*£&é\"'(§è!çà)-^$ùµ,;:=   ";
  
  /**
   * Permet de chiffrer et déchiffrer une chaine avec l'algorithme utilisé dans les programmes RPG.
   * Note importante: la chaine en clair à chiffrer ne doit comporter que des chiffres et des lettres majuscules.
   * Par contre la chaine à déchiffrer peut comporter des minuscules.
   */
  public static String chiffrer(String pChaineISO8859) {
    String chaine = "";
    for (int i = 0; i < pChaineISO8859.length(); i++) {
      int xt = i + 1;
      int l1 = xt % 2;
      int l2 = xt % 3;
      int xs = cle.indexOf(pChaineISO8859.charAt(i));
      if (xs > -1) {
        xs = cle.length() - (xs + 1);
        if (l1 == 0) {
          xs = xs - 1;
        }
        if (l2 == 0) {
          xs = xs + 1;
        }
        chaine = chaine + cle.charAt(xs - 1);
      }
    }
    return chaine;
  }
  
  /**
   * Converti la chaine avant l'enregistrement en table.
   */
  public static String versDb2(String pChaineChiffree) {
    if (pChaineChiffree == null || pChaineChiffree.trim().isEmpty()) {
      return pChaineChiffree;
    }
    return pChaineChiffree.replace("\u00a5", "\u00a8");
  }
  
  /**
   * Converti la chaine après lecture en table.
   */
  public static String depuisDb2(String pChaineChiffree) {
    if (pChaineChiffree == null || pChaineChiffree.trim().isEmpty()) {
      return pChaineChiffree;
    }
    return pChaineChiffree.replace("\u00a8", "\u00a5");
  }
}
