/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session;

import java.io.Serializable;
import java.util.UUID;

import ri.serien.libcommun.outils.chaine.StringIP;

/**
 * Gestion des informations importantes pour un utilisateur.
 */
public class User implements Serializable {
  // Variables
  // Profil de l'utilisateur
  protected String profil = "";
  // Mot de passe
  protected String mdp = "";
  // Adresse IP de l'AS400
  protected StringIP adresseIP = new StringIP("");
  protected String codepageServeur = System.getProperty("file.encoding");
  protected String id = UUID.randomUUID().toString();
  
  /**
   * Initialise le profil.
   */
  public void setProfil(String valeur) {
    if (valeur != null) {
      profil = valeur.trim().toUpperCase();
    }
  }
  
  /**
   * Retourne le profil.
   */
  public String getProfil() {
    return profil;
  }
  
  /**
   * Initialise le mot de passe.
   */
  public void setMotDePasse(String valeur) {
    mdp = valeur;
  }
  
  /**
   * Retourne le mot de passe.
   */
  public String getMotDePasse() {
    return mdp;
  }
  
  /**
   * @return codepageServeur.
   */
  public String getCodepageServeur() {
    return codepageServeur;
  }
  
  /**
   * @param codepageServeur codepageServeur à définir.
   */
  public void setCodepageServeur(String codepageServeur) {
    this.codepageServeur = codepageServeur;
  }
  
  /**
   * Renseigner d.
   */
  public void setId(String id) {
    this.id = id;
  }
  
  /**
   * return the id.
   */
  public String getId() {
    return id;
  }
  
  /**
   * Retourne l'adresse IP du client.
   */
  public String getAdresseIP() {
    return adresseIP.getIP();
  }
  
  /**
   * Initialise l'adresse IP du client.
   */
  public void setAdresseIP(String adresseIP) {
    this.adresseIP.setIP(adresseIP);
  }
  
  /**
   * Libère les ressources.
   */
  public void dispose() {
    adresseIP.dispose();
    adresseIP = null;
  }
}
