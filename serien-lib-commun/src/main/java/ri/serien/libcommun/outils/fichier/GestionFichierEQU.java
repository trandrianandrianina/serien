/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JFileChooser;

import ri.serien.libcommun.outils.Constantes;

/**
 * Gestion des fichiers de description EQU.
 * Gère les fichiers de type EQU (Clef-tabulation-Valeur).
 */
public class GestionFichierEQU extends GestionFichierTexte {
  // Constantes
  private static final String FILTER_EXT_EQU = "equ";
  private static final String FILTER_DESC_EQU = "Fichier EQU (*.equ)";
  
  /**
   * Constructeur de la classe.
   */
  public GestionFichierEQU(String fchequ) {
    super(fchequ);
  }
  
  /**
   * Constructeur de la classe.
   */
  public GestionFichierEQU(URL pUrl) {
    super(pUrl);
  }
  
  /**
   * Initialise la liste des équivalences.
   */
  public void setListe(HashMap<String, String> lstequ) {
    if (lstequ == null) {
      return;
    }
    
    ArrayList<String> liste = new ArrayList<String>(lstequ.size());
    Set<Entry<String, String>> donnee = lstequ.entrySet();
    Iterator<Entry<String, String>> it = donnee.iterator();
    while (it.hasNext()) {
      Map.Entry<String, String> e = it.next();
      liste.add(e.getKey() + Constantes.SEPARATEUR + e.getValue().replace(File.separatorChar, Constantes.SEPARATEUR_DOSSIER_CHAR));
    }
    super.setContenuFichier(liste);
  }
  
  /**
   * Retourne la liste des equivalences.
   */
  public HashMap<String, String> getListe(boolean trfpath) {
    String chaine = null;
    String[] tabchaine = null;
    HashMap<String, String> listeEqu = new HashMap<String, String>();
    ArrayList<String> liste = super.getContenuFichier();
    
    if (trfpath) {
      for (int i = 0; i < liste.size(); i++) {
        chaine = Constantes.code2unicode(liste.get(i));
        tabchaine = chaine.split(Constantes.SEPARATEUR);
        listeEqu.put(tabchaine[0], tabchaine[1].replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
      }
    }
    else {
      for (int i = 0; i < liste.size(); i++) {
        chaine = Constantes.code2unicode(liste.get(i));
        tabchaine = chaine.split(Constantes.SEPARATEUR);
        listeEqu.put(tabchaine[0], tabchaine[1]);
      }
    }
    
    return listeEqu;
  }
  
  /**
   * Retourne les filtres possible pour les boites de dialogue.
   */
  public static void initFiltre(JFileChooser pJFileChooser) {
    GestionFichierTexte.initFiltre(pJFileChooser);
    pJFileChooser.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { FILTER_EXT_EQU }, FILTER_DESC_EQU));
  }
  
}
