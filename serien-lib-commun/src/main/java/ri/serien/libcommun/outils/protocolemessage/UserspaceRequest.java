/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.protocolemessage;

import java.util.ArrayList;

public class UserspaceRequest extends BaseRequest {
  // Constantes
  public static final int ISEXISTS = 1;
  public static final int CREATE = 2;
  public static final int READ = 4;
  public static final int WRITE = 8;
  public static final int LIST = 16;
  
  public static final ArrayList<Integer> ACTIONS = new ArrayList<Integer>() {
    {
      add(NOACTION);
    }
    
    {
      add(ISEXISTS);
    }
    
    {
      add(CREATE);
    }
    
    {
      add(READ);
    }
    
    {
      add(WRITE);
    }
    // { add(LIST); }
  };
  
  // Variables
  private String userspace = null;
  private String library = null;
  private String data = null;
  private String text = null;
  private boolean exist = false;
  private String typeVariant = null;
  // Permet de stocker des données différentes défini par le typeVariant
  private ArrayList<Object> variant = new ArrayList<Object>();
  
  public UserspaceRequest() {
  }
  
  /**
   * Constructeur.
   */
  public UserspaceRequest(String auserspace, String alibrary, int anaction) {
    setUserspace(auserspace);
    setLibrary(alibrary);
    setActions(anaction);
  }
  
  // --> Méthodes publiques <------------------------------------------------
  
  // --> Méthodes privées <--------------------------------------------------
  
  // --> Accesseurs <--------------------------------------------------------
  
  /**
   * @return le userspace.
   */
  public String getUserspace() {
    return userspace;
  }
  
  /**
   * @param userspace le userspace à définir.
   */
  public void setUserspace(String userspace) {
    if (userspace != null) {
      this.userspace = userspace.trim();
      
    }
    else {
      this.userspace = userspace;
    }
  }
  
  /**
   * @return le library.
   */
  public String getLibrary() {
    return library;
  }
  
  /**
   * @param library le library à définir.
   */
  public void setLibrary(String library) {
    if (library != null) {
      this.library = library.trim();
    }
    else {
      this.library = library;
    }
  }
  
  /**
   * @return le data.
   */
  public String getData() {
    return data;
  }
  
  /**
   * @param data le data à définir.
   */
  public void setData(String data) {
    this.data = data;
  }
  
  /**
   * @return le text.
   */
  public String getText() {
    return text;
  }
  
  /**
   * @param text le text à définir.
   */
  public void setText(String text) {
    this.text = text;
  }
  
  /**
   * @return le exist.
   */
  public boolean isExist() {
    return exist;
  }
  
  /**
   * @param exist le exist à définir.
   */
  public void setExist(boolean exist) {
    this.exist = exist;
  }
  
  /**
   * @return le typeVariant.
   */
  public String getTypeVariant() {
    return typeVariant;
  }
  
  /**
   * @param typeVariant le typeVariant à définir.
   */
  public void setTypeVariant(String typeVariant) {
    this.typeVariant = typeVariant;
  }
  
  /**
   * @return le variant.
   */
  public ArrayList<Object> getVariant() {
    return variant;
  }
  
  /**
   * @param variant le variant à définir.
   */
  public void setVariant(ArrayList<Object> variant) {
    this.variant = variant;
  }
  
}
