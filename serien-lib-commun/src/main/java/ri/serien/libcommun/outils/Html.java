/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

/**
 * Cette classe a pour vocation de regrouper tous les traitements qui concernent le format Html.
 * Elle utilise l'API JSoup en version 1.10.3 car c'est la dernière compatible java 6.
 */
public class Html {
  
  /**
   * Retourne si un texte est au format Html.
   * En aucun cas cette méthode valide une page html, elle indique juste si c'est du code html.
   * Cet méthode est perfectible, il faudra l'améliorer avec le temps.
   */
  public static boolean isHtml(String pTexte) {
    pTexte = Constantes.normerTexte(pTexte);
    if (pTexte.isEmpty()) {
      return false;
    }
    
    // Recherche des tags cad des marqueurs génériques de type <???>
    Pattern p = Pattern.compile("\\<(.*?)\\>", Pattern.MULTILINE);
    Matcher m = p.matcher(pTexte);
    int nombreTags = 0;
    while (m.find()) {
      nombreTags++;
    }
    
    // Deux tags est le minimum pour considérer qu'il s'agit de code html
    if (nombreTags >= 2) {
      return true;
    }
    return false;
  }
  
  /**
   * Formatte un texte Html proprement en vue de l'afficher.
   * Le texte brut tel qu'il a été lu en base est retourné si une erreur est détectée.
   * Le message d'erreur est récupérable via un paramètre si besoin.
   */
  public static String mettreEnForme(String pTextHtml) {
    pTextHtml = Constantes.normerTexte(pTextHtml);
    if (pTextHtml.isEmpty()) {
      return pTextHtml;
    }
    
    Document doc = Jsoup.parse(pTextHtml, "", Parser.xmlParser());
    doc.outputSettings().indentAmount(Constantes.ESPACES_TABULATION);
    return doc.html();
  }
  
  /* Fonctionne mais c'est perfectible (1° version)
  public static String mettreEnforme(String pTextHtml, boolean pSupprimerLigneVide, StringBuffer pRapportFormatage) {
    pTextHtml = Constantes.normerTexte(pTextHtml);
    if (pTextHtml.isEmpty()) {
      return pTextHtml;
    }
    
    try {
      // Tentative de formattage
      Source xmlInput = new StreamSource(new StringReader(pTextHtml));
      StringWriter stringWriter = new StringWriter();
      StreamResult xmlOutput = new StreamResult(stringWriter);
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      transformerFactory.setAttribute("indent-number", Constantes.ESPACES_TABULATION);
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.transform(xmlInput, xmlOutput);
      String texte = xmlOutput.getWriter().toString();
      
      // Supprime les lignes vides
      if (pSupprimerLigneVide) {
        texte = texte.replaceAll("(?m)^[ ]*\r?\n", "");
      }
      return texte;
    }
    catch (Exception e) {
      // Si une erreur est détectée : le texte original est retourné mais l'erreur est logguée
      Trace.erreur(e, "Erreur lors de la mise en forme du texte Html.");
      if (pRapportFormatage != null) {
        pRapportFormatage.append(e);
      }
    }
    return pTextHtml;
  }*/
  
}
