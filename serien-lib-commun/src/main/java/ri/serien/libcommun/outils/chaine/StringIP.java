/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.chaine;

import java.io.Serializable;

import ri.serien.libcommun.outils.Constantes;

/**
 * Permet de mettre en forme une adresse IP.
 */
public class StringIP implements Serializable {
  // Variables
  private String ip = null;
  
  /**
   * Constructeur.
   */
  public StringIP(String aip) {
    setIP(aip);
  }
  
  /**
   * Retourne l'adresse IP sans les points sous forme: 192168001001.
   */
  public String getIPwoPoint() {
    String[] tab = null;
    StringBuffer ipwopoint = new StringBuffer();
    int lg = 0;
    
    if (ip == null) {
      return null;
    }
    
    tab = Constantes.splitString(ip, '.');
    for (int i = 0; i < tab.length; i++) {
      if (tab[i].length() >= 3) {
        ipwopoint.append(tab[i]);
      }
      else {
        lg = 3 - tab[i].length();
        for (int j = 0; j < lg; j++) {
          ipwopoint.append('0');
        }
        ipwopoint.append(tab[i]);
      }
    }
    return ipwopoint.toString();
  }
  
  /**
   * Libère les resources.
   */
  public void dispose() {
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * Initialise l'adresse IP.
   */
  public void setIP(String aip) {
    ip = aip.substring(aip.indexOf("/") + 1);
  }
  
  /**
   * Retourne l'adresse IP.
   */
  public String getIP() {
    return ip;
  }
  
}
