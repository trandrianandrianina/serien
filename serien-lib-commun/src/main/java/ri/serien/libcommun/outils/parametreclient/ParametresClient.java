/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.parametreclient;

import java.io.File;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Ensemble des paramètres pour Serie N.
 * Le fichier SerieN.ini va être rechercher dans le dossier d'exécution de SerieN.jar.
 */
public class ParametresClient {
  // Constantes
  public static final int NO_OK = -1;
  public static final int NO_TESTED = 0;
  public static final int OK = 1;
  
  private static final String DOSSIER_RACINE_PAR_DEFAUT = Constantes.getUserHome() + File.separator + Constantes.DOSSIER_RACINE;
  private static final int DELAI_CONSERVATION_FICHIERS_PAR_DEFAUT = 10;
  
  // Variables
  private String nom = "Réseau local";
  private boolean parametresParDefaut = false;
  private String adresseServeur = "";
  private int portServeur = Constantes.SERVER_PORT;
  private String dossierRacineDesktop = DOSSIER_RACINE_PAR_DEFAUT;
  private boolean modeDebug = false;
  private int delaiConservationFichiers = DELAI_CONSERVATION_FICHIERS_PAR_DEFAUT;
  private int status = NO_TESTED;
  private String parametresJVM = "-jar";
  private String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public ParametresClient() {
  }
  
  /**
   * Constructeur.
   * @param rootfolder Dossier root par défaut mais passé en paramètres car parfois user.home ne ramène pas le bon.
   */
  public ParametresClient(String rootfolder) {
    setDossierRacineDesktop(rootfolder);
  }
  
  /**
   * Noter les paramètres clients dans les traces.
   */
  public void tracer() {
    Trace.titre("PARAMETRES CLIENT");
    Trace.info("Nom du jeu de paramètres                  : " + nom);
    Trace.info("Paramètres par défaut                     : " + parametresParDefaut);
    Trace.info("Dossier racine (-dir=)                    : " + dossierRacineDesktop);
    Trace.info("Adresse du serveur (-ip=)                 : " + adresseServeur);
    Trace.info("Port du serveur (-p=)                     : " + portServeur);
    Trace.info("Mode debug (-d)                           : " + modeDebug);
    Trace.info("Délai de conservation fichiers (-kplogs=) : " + delaiConservationFichiers);
    Trace.info("Paramètres JVM (jvmparameters)            : " + parametresJVM);
  }
  
  /**
   * Contrôle la validité des paramètres.
   */
  public boolean isValide() {
    if (nom == null || nom.trim().isEmpty()) {
      msgErreur = "La référence n'est pas renseignée.";
      return false;
    }
    
    if (adresseServeur == null || adresseServeur.trim().isEmpty()) {
      msgErreur = "Une adresse IP ou un nom de host est obligatoire.";
      return false;
    }
    
    if (portServeur <= 0 || portServeur > 65536) {
      msgErreur = "Le port est invalide.";
      return false;
    }
    
    if (dossierRacineDesktop == null || dossierRacineDesktop.trim().isEmpty()) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Renseigner les paramètres à partir d'une ligne de commande.
   */
  public void setParametres(String pParametres) {
    if (pParametres == null) {
      throw new MessageErreurException("Les paramètres clients sont invalides.");
    }
    
    String[] listeparametres = null;
    StringBuffer param = new StringBuffer(pParametres.trim());
    String chaine = null;
    
    // On arrange la chaine pour récupérer les paramètres correctement
    int pos = param.indexOf(" ");
    while (pos != -1) {
      if (param.charAt(pos + 1) != '-') {
        param.replace(pos, pos + 1, "%20");
      }
      pos = param.indexOf(" ", pos + 1);
    }
    
    // On découpe la chaine et on stocke les différents paramètres dans un tableau
    listeparametres = Constantes.splitString(param.toString(), ' ');
    
    // On parcourt la liste afin d'en extraire les infos
    for (int i = 0; i < listeparametres.length; i++) {
      chaine = listeparametres[i].replaceAll("%20", " ").trim();
      
      if (chaine.toLowerCase().startsWith("-ip=")) {
        // On recherche l'adresse IP
        adresseServeur = chaine.substring(4);
      }
      else if (chaine.toLowerCase().startsWith("-p=")) {
        // On recherche le port
        portServeur = Integer.parseInt(chaine.substring(3));
      }
      else if (chaine.toLowerCase().startsWith("-dir=")) {
        // On recherche le dossier utilisateur
        dossierRacineDesktop = chaine.substring(5);
      }
      else if (chaine.toLowerCase().startsWith("-kplogs=")) {
        // On recherche le delai de conservation des logs (en jours) celui du jour inclu
        delaiConservationFichiers = Integer.parseInt(chaine.substring(8));
      }
      else if (chaine.toLowerCase().startsWith("-d")) {
        // On recherche si on veut débogguer ou pas
        modeDebug = true;
      }
    }
  }
  
  /**
   * Retourner la ligne de commande correspondant aux paramètres.
   */
  public String getLigneCommande() {
    StringBuffer parameters = new StringBuffer();
    parameters.append("-ip=").append(adresseServeur).append(' ');
    if (portServeur != Constantes.SERVER_PORT) {
      parameters.append("-p=").append(portServeur).append(' ');
    }
    if (!dossierRacineDesktop.equals(DOSSIER_RACINE_PAR_DEFAUT)) {
      parameters.append("-dir=").append(dossierRacineDesktop).append(' ');
    }
    if (delaiConservationFichiers != DELAI_CONSERVATION_FICHIERS_PAR_DEFAUT) {
      parameters.append("-kplogs=").append(delaiConservationFichiers).append(' ');
    }
    if (modeDebug) {
      parameters.append("-d").append(' ');
    }
    
    return parameters.toString();
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur(boolean html) {
    String chaine;
    
    // La récupération du message est à usage unique
    if (html) {
      chaine = "<html>" + msgErreur.replaceAll("\n", "<br>") + "</html>";
    }
    else {
      chaine = msgErreur;
    }
    msgErreur = "";
    
    return chaine;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * Nom du jeu de paramètres.
   * Plusieurs jeux de paramètres peuvent être définis dans le fichier .ini. Le nom permet de les distinguer.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * @param name le nom à définir.
   */
  public void setNom(String pNom) {
    if (nom == null || nom.trim().isEmpty()) {
      throw new MessageErreurException("Le nom du jeu de paramètres client n'est pas défini.");
    }
    
    nom = pNom;
  }
  
  /**
   * Indiquer si ce sont les paramètres par défaut (lancement automatique).
   */
  public boolean isParametresParDefaut() {
    return parametresParDefaut;
  }
  
  /**
   * @param pdefaultParam le default_param à définir.
   */
  public void setParametresParDefaut(boolean pConfigurationParDefaut) {
    this.parametresParDefaut = pConfigurationParDefaut;
  }
  
  /**
   * Adresse du serveur (nom du host ou adresse IP).
   */
  public String getAdresseServeur() {
    return adresseServeur;
  }
  
  /**
   * Renseigner l'adresse du serveur.
   */
  public void setAdresseServeur(String pAdresseServeur) {
    pAdresseServeur = Constantes.normerTexte(pAdresseServeur);
    if (pAdresseServeur.isEmpty()) {
      throw new MessageErreurException(
          "L'adresse IP ou le nom du host du serveur Série N ne sont pas définis pour la configuration " + nom);
    }
    
    this.adresseServeur = pAdresseServeur;
  }
  
  /**
   * Port du serveur.
   */
  public int getPortServeur() {
    return portServeur;
  }
  
  /**
   * Renseigner le port du serveur.
   */
  public void setPortServeur(int pPortServeur) {
    if (pPortServeur <= 0 || pPortServeur > 65536) {
      throw new MessageErreurException("Le numéro de port du serveur Série N est invalide pour la configuration " + nom);
    }
    portServeur = pPortServeur;
  }
  
  /**
   * Dossier racine du client.
   */
  public String getDossierRacineDesktop() {
    return dossierRacineDesktop;
  }
  
  /**
   * Renseigner le dossier racine du client.
   */
  public void setDossierRacineDesktop(String pDossierRacine) {
    if (pDossierRacine == null || pDossierRacine.trim().isEmpty()) {
      throw new MessageErreurException("Le dossier racine du client n'est pas défini pour la configuration " + nom);
    }
    dossierRacineDesktop = pDossierRacine;
  }
  
  /**
   * Le mode debug doit être activé.
   */
  public boolean isModeDebug() {
    return modeDebug;
  }
  
  /**
   * Renseigner si le mode debug doit être activé.
   */
  public void setModeDebug(boolean debug) {
    this.modeDebug = debug;
  }
  
  /**
   * Durée de conservation des fichiers.
   */
  public int getDelaiConservationFichiers() {
    return delaiConservationFichiers;
  }
  
  /**
   * Renseigner la durée de conservation des fichiers.
   */
  public void setDelaiConservationFichiers(int pDelaiConservationFichiers) {
    if (pDelaiConservationFichiers <= 0 || pDelaiConservationFichiers > 365) {
      throw new MessageErreurException("Le délai de conservation des fichiers est invalide pour la configuration " + nom);
    }
    delaiConservationFichiers = pDelaiConservationFichiers;
  }
  
  /**
   * @return le status.
   */
  public int getStatus() {
    return status;
  }
  
  /**
   * @param status le status à définir.
   */
  public void setStatus(int status) {
    this.status = status;
  }
  
  /**
   * @return le jvmParam.
   */
  public String getJvmParam() {
    return parametresJVM;
  }
  
  /**
   * @param jvmParam le jvmParam à définir.
   */
  public void setJvmParam(String jvmParam) {
    if (jvmParam != null) {
      this.parametresJVM = jvmParam + " " + this.parametresJVM;
    }
    else {
      this.parametresJVM = jvmParam;
    }
  }
  
}
