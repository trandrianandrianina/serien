/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.composants;

import java.awt.Component;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.JComponent;

import ri.serien.libcommun.outils.Constantes;

/**
 * ATTENTION !! Tout objet a obligatoirement un NAME et un TYPEOBJ dans description PNL
 */
public class GenericBase implements ioGeneric {
  // Constantes erreurs
  private final static String ERREUR_ID_INCONNU = "ID inconnu.";
  
  // Constantes ID, il y a 100 variables possibles pour l'objet GenericBase
  // Chaque variable doit obligatoirement avoir un ID (pour la compilation)
  private final static int ID_GENERICBASE = 500;
  private final static int ID_NAME = ID_GENERICBASE + 1;
  private final static int ID_PARENT = ID_GENERICBASE + 2;
  
  private final static String GENERE_NOM = "NG_";
  
  // Variables
  private static int compteurNom = 0;
  // Type d'objet: oLabel, oData, ...
  private String typeObjet = getClass().getName();
  // Nom du Objet
  private String name = null;
  // Nom du parent de l'objet
  protected String parent = null;
  private StringBuffer msgErreur = new StringBuffer();
  // Le composant Maitre oRecord, oFrame, ...
  protected ioGeneric root = null;
  protected ArrayList<Integer> tabCompilation = new ArrayList<Integer>(Constantes.MAXVAR);
  // Voir si +tard on peut pas le rendre plus bô via methode (pour l'instant performance !!)
  public LinkedHashMap<String, ioGeneric> listeElement = null;
  // Ne sert pas en execution juste pour la compilation et analyse des PNL
  protected HashMap<String, ioGeneric> listeParent = null;
  protected HashMap<String, Object> listeAttributes = null;
  
  /**
   * Constructeur
   * @param namec
   */
  public GenericBase() {
  }
  
  /**
   * Initialise le composant maitre (oFrame, oRecord)
   * @param aroot
   */
  @Override
  public void setRoot(ioGeneric aroot) {
    root = aroot;
  }
  
  /**
   * Retourne le composant maitre (oFrame, oRecord)
   * @param aroot
   */
  public ioGeneric getRoot() {
    return root;
  }
  
  /**
   * Initialise le type de l'objet
   * @param valeur
   */
  public void setTypeObjet(String valeur) {
    if (valeur != null) {
      typeObjet = valeur.trim();
    }
  }
  
  /**
   * Retourne le type de l'objet
   * @return
   */
  public String getTypeObjet() {
    return typeObjet;
  }
  
  /**
   * Initialise le nom
   * @param valeur
   */
  public void setName(String valeur) {
    if (valeur != null) {
      name = valeur.trim();
    }
  }
  
  /**
   * Retourne le nom
   * @return
   */
  @Override
  public String getName() {
    // Si pas de nom alors on en génére un (obligatoire d'en avoir un)
    if (name == null) {
      name = GENERE_NOM + compteurNom++;
    }
    return name;
  }
  
  /**
   * Retourne le nom du parent
   * @return
   */
  public String getParent() {
    return parent;
  }
  
  /**
   * Initialise le nom du parent
   * @param valeur
   */
  public void setParent(String valeur) {
    if (valeur != null) {
      parent = valeur.trim();
    }
  }
  
  /**
   * Initialise le message d'erreur
   */
  protected void setMsgErreur(String valeur) {
    msgErreur.append(valeur);
  }
  
  /**
   * Retourne le message d'erreur
   */
  public String getMsgErreur() {
    return msgErreur.toString();
  }
  
  /**
   * Ajoute une valeur entiere dans la liste de compilation
   */
  protected void addTabCompilation(int id, ArrayList<Integer> tabCompilation, int valeur) {
    if (id != Constantes.NONE) {
      tabCompilation.add(id);
    }
    tabCompilation.add(1);
    tabCompilation.add(valeur);
  }
  
  /**
   * Ajoute une valeur chaine dans la liste de compilation
   */
  protected void addTabCompilation(int id, ArrayList<Integer> tabCompilation, String valeur) {
    int i;
    byte tabchaine[] = null;
    
    if (valeur == null) {
      return;
    }
    
    if (id != Constantes.NONE) {
      tabCompilation.add(id);
    }
    tabCompilation.add(valeur.length());
    // tabchaine = valeur.getBytes(Constantes.CODEPAGE);
    tabchaine = valeur.getBytes();
    i = 0;
    while (i < tabchaine.length) {
      tabCompilation.add((int) tabchaine[i]);
      i++;
    }
  }
  
  /**
   * Ajoute une valeur booléene dans la liste de compilation
   */
  protected void addTabCompilation(int id, ArrayList<Integer> tabCompilation, boolean valeur) {
    if (id != Constantes.NONE) {
      tabCompilation.add(id);
    }
    tabCompilation.add(1);
    if (valeur) {
      tabCompilation.add(Constantes.TRUE);
    }
    else {
      tabCompilation.add(Constantes.FALSE);
    }
  }
  
  /**
   * Insertion de la taille de la description de l'objet
   */
  protected void insertTailleObjet() {
    // Calcul position longueur du tableau (longueur du type d'objet + sa longueur)
    int offset = getTypeObjet().length() + 1;
    // Insertion de la taille du tableau en début (après le type d'objet)
    tabCompilation.add(offset, tabCompilation.size() - offset);
  }
  
  /**
   * Met à jour les propriétes utilisées par les objets finaux
   * @return
   */
  public void setProperties(Object[][] alisteProperties) {
    int i = 0;
    
    // Mise à jour des attributs
    for (i = 0; i < alisteProperties.length; i++) {
      listeAttributes.put((String) alisteProperties[i][0], alisteProperties[i][0]);
    }
  }
  
  /**
   * Retourne les propriétes utilisées par cet objet
   * @return
   */
  public Object[][] getProperties(Object[][] alisteProperties) {
    int i = 0;
    
    // Récupération des attributs
    getAttributes();
    
    // Séléction des propriétés pour cet objet
    for (i = 0; i < alisteProperties.length; i++) {
      alisteProperties[i][1] = listeAttributes.get(alisteProperties[i][0]);
    }
    
    return alisteProperties;
  }
  
  /**
   * Initialise les attributs et leurs types d'un objet
   */
  protected void setAttributes(HashMap<String, Object> alisteAttributes) {
    listeAttributes = alisteAttributes;
    
    // TYPE
    if (listeAttributes.containsKey("TYPEOBJ")) {
      typeObjet = (String) listeAttributes.get("TYPEOBJ");
    }
    // NAME
    if (listeAttributes.containsKey("NAME")) {
      name = (String) listeAttributes.get("NAME");
    }
    // PARENT
    if (alisteAttributes.containsKey("PARENT")) {
      parent = (String) listeAttributes.get("PARENT");
    }
  }
  
  /**
   * Retourne les attributs et leurs types d'un objet
   */
  protected HashMap<String, Object> getAttributes() {
    listeAttributes = new HashMap<String, Object>();
    listeAttributes.put("TYPEOBJ", typeObjet);
    listeAttributes.put("NAME", getName());
    listeAttributes.put("PARENT", parent);
    
    return listeAttributes;
  }
  
  /**
   * Initialise les valeurs avec un tableau venant du PNL
   * @param valeur
   */
  @Override
  public void pnlToValeurs(LinkedHashMap<String, String> valeur) {
    // TYPE
    if (valeur.containsKey("TYPEOBJ")) {
      typeObjet = valeur.get("TYPEOBJ");
    }
    // NAME
    if (valeur.containsKey("NAME")) {
      name = valeur.get("NAME");
    }
    // PARENT
    if (valeur.containsKey("PARENT")) {
      parent = valeur.get("PARENT");
      // ((GenericBase)((GenericBase)root).listeParent.get(parent)).listeElement.size());
      if (root != null) {
        ((GenericBase) ((GenericBase) root).listeParent.get(parent)).listeElement.put(getName(), this);
      }
    }
  }
  
  /**
   * Convertie l'objet au format binaire
   */
  @Override
  public ArrayList<Integer> valeursToBinaire(ArrayList<String> tabListFic) throws UnsupportedEncodingException {
    // On indique le nom (typeObjet) de la classe (pour la réflexion)
    addTabCompilation(Constantes.NONE, tabCompilation, typeObjet);
    addTabCompilation(ID_NAME, tabCompilation, getName());
    if (parent != null) {
      addTabCompilation(ID_PARENT, tabCompilation, parent);
    }
    
    return tabCompilation;
  }
  
  /**
   * Convertie l'objet au format valeur
   */
  @Override
  public int binaireToValeurs(int tabInt[], String ListFic[]) {
    return binaireToValeurs(tabInt, 0, ListFic);
  }
  
  @Override
  public int binaireToValeurs(int tabInt[], int offst, String ListFic[]) {
    int i = offst;
    int j = 0;
    int k = 0;
    int lg = 0;
    // byte tabByte[]=null;
    byte tabByte[] = new byte[Constantes.DATAQLONG];
    
    switch (tabInt[i]) {
      case ID_NAME:
        // longueur
        lg = tabInt[++i];
        // tabByte = new byte[k];
        // récupération de la chaine
        i++;
        k = lg + i;
        for (j = 0; i < k; i++) {
          tabByte[j++] = (byte) tabInt[i];
        }
        setName(new String(tabByte, 0, lg));
        break;
      case ID_PARENT:
        // longueur
        lg = tabInt[++i];
        // tabByte = new byte[k];
        // récupération de la chaine
        i++;
        k = lg + i;
        for (j = 0; i < k; i++) {
          tabByte[j++] = (byte) tabInt[i];
        }
        setParent(new String(tabByte, 0, lg));
        break;
      
      // Gestion d'un ID inconnu
      default:
        setMsgErreur(ERREUR_ID_INCONNU + " " + tabInt[i]);
        k = tabInt[++i];
        i = i + k + 1;
        break;
    }
    
    return i;
  }
  
  /**
   * Initialise le composant Swing avec les attributs de cette classe
   * @param composant
   */
  public void setSwing(Object composant, Component fenetre) {
    ((JComponent) composant).setName(getName());
    
  }
  
  /**
   * Libération de la mémoire
   */
  public void dispose() {
    if (listeParent != null) {
      listeParent.clear();
    }
    listeParent = null;
    if (listeElement != null) {
      listeElement.clear();
    }
    listeElement = null;
    if (tabCompilation != null) {
      tabCompilation.clear();
    }
    tabCompilation = null;
    if (listeAttributes != null) {
      listeAttributes.clear();
    }
    listeAttributes = null;
    // if( (root != null) && (root instanceof GenericBase) ) ((GenericBase)root).dispose();
    root = null;
  }
}
