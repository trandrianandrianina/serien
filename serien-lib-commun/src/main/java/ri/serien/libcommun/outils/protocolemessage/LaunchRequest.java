/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.protocolemessage;

import ri.serien.libcommun.outils.collection.ArrayListManager.IArrayListListener;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;

/**
 * Lanceur de requête SQL / Dataarea.
 */
public abstract class LaunchRequest {
  // Variables
  private int hashcodecaller = this.hashCode();
  private MessageManager mc = new MessageManager(hashcodecaller);
  private IArrayListListener listener = null;
  
  // private long t1=0;
  
  /**
   * Constructeur.
   */
  public LaunchRequest() {
    initSystem();
  }
  
  /**
   * Initialisation du système.
   */
  private void initSystem() {
    listener = new IArrayListListener() {
      // @Override
      @Override
      public void onDataRemoved(Object val) {
      }
      
      // @Override
      @Override
      public void onDataRemoved(int indice) {
      }
      
      // @Override
      @Override
      public void onDataCleared() {
      }
      
      // @Override
      @Override
      public void onDataAdded(Object val) {
        traitementMessage((String) val);
      }
    };
    ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().getEvenementSysteme().addListener(listener);
  }
  
  /**
   * Traitement d'un message reçu.
   */
  protected void traitementMessage(String message) {
    // infoUser.getLog().ecritureMessage(message);
    if (!mc.getMessageReceived(message, hashcodecaller)) {
      return;
    }
    
    if (mc.getBody() != null) {
      if (mc.getBody() instanceof Db2Request) {
        Db2Request db2 = (Db2Request) mc.getBody();
        traitementData(db2);
      }
      else if (mc.getBody() instanceof DataareaRequest) {
        DataareaRequest dta = (DataareaRequest) mc.getBody();
        traitementData(dta);
      }
      else if (mc.getBody() instanceof UserspaceRequest) {
        UserspaceRequest uspc = (UserspaceRequest) mc.getBody();
        traitementUserspace(uspc);
      } // else
      
    }
    
    // Suppression de l'objet de la liste, une fois traité
    ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().getEvenementSysteme().removeObject(message);
    
    fintraitement();
    // double delai = System.currentTimeMillis() - t1;
    // sec"));
  }
  
  protected abstract boolean traitementData(Db2Request db2);
  
  protected abstract boolean traitementData(DataareaRequest dta);
  
  protected abstract boolean traitementUserspace(UserspaceRequest uspc);
  
  /**
   * Opérations à effecter en fin de Traitement.
   */
  protected abstract void fintraitement();
  
  /**
   * Envoi une requête Db2 système au serveur.
   */
  public void sendRequestDb2(String requete, int action) {
    mc.createMessage(false, new Db2Request(requete, action));
    // listeRequete.put(compteurID, null);
    
    // t1 = System.currentTimeMillis();
    // infoUser.getTransfertSession().EnvoiMessageSocket(Constantes.ENVOI_REQUETE_SYSTEME, mc.getMessageToSend());
    ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().effectuerRequeteSysteme(mc.getMessageToSend());
  }
  
  /**
   * Envoi une requête Dataarea système au serveur (à éliminer).
   */
  public void sendRequestDta(String adataarea, String alibrary, int action) {
    mc.createMessage(false, new DataareaRequest(adataarea, alibrary, action));
    
    ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().effectuerRequeteSysteme(mc.getMessageToSend());
  }
  
  /**
   * Envoi une requête Dataarea système au serveur (non testé).
   */
  public void sendRequestDta(String adataarea, String alibrary, String data, int action) {
    DataareaRequest dta = new DataareaRequest(adataarea, alibrary, action);
    dta.setData(data);
    mc.createMessage(false, dta);
    
    ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().effectuerRequeteSysteme(mc.getMessageToSend());
  }
  
  /**
   * Envoi une requête Userspace système au serveur.
   */
  public void sendRequestUspc(String auserspace, String alibrary, String data, int action) {
    UserspaceRequest uspc = new UserspaceRequest(auserspace, alibrary, action);
    uspc.setData(data);
    mc.createMessage(false, uspc);
    
    ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().effectuerRequeteSysteme(mc.getMessageToSend());
  }
  
  /**
   * Libère la mémoire.
   */
  public void dispose() {
    ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().getEvenementSysteme().removeListener(listener);
  }
  
}
