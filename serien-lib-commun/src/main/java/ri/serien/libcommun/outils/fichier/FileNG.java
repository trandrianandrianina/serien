/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URI;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Routines concernant les fichiers & dossiers.
 */
public class FileNG extends File {
  
  // Variable
  private String extension = null; // ne contient pas le point
  private String namewoextension = null; // ne contient pas le point
  
  /**
   * Constructeur.
   */
  public FileNG(String fichier) {
    super(fichier);
  }
  
  /**
   * Constructeur.
   */
  public FileNG(URI fichier) {
    super(fichier);
  }
  
  /**
   * Constructeur.
   */
  public FileNG(File fichier) {
    super(fichier.getAbsolutePath());
  }
  
  /**
   * Copie le fichier vers le chemin spécifié.
   * @param pCheminComplet avec le nom du fichier destination.
   */
  public boolean copyTo(String pCheminComplet) {
    if (Constantes.normerTexte(pCheminComplet).isEmpty()) {
      return false;
    }
    return copyTo(new File(pCheminComplet));
  }
  
  /**
   * Copie le fichier vers le chemin spécifié.
   * @param pCheminComplet avec le nom du fichier destination.
   */
  public boolean copyTo(File pDestFile) {
    if (pDestFile == null || pDestFile.isDirectory()) {
      return false;
    }
    
    RandomAccessFile source = null;
    RandomAccessFile destination = null;
    try {
      // On teste si le fichier existe et si on a les droits de lecture
      source = new RandomAccessFile(this, "r");
      
      if (!pDestFile.exists()) {
        pDestFile.getParentFile().mkdirs();
      }
      else {
        pDestFile.delete();
      }
      
      // On effectue la copie
      byte[] bytes = new byte[(int) source.length()];
      destination = new RandomAccessFile(pDestFile, "rw");
      source.readFully(bytes);
      source.close();
      destination.write(bytes);
      destination.close();
    }
    catch (Exception e) {
      try {
        if (source != null) {
          source.close();
        }
        if (destination != null) {
          destination.close();
        }
      }
      catch (IOException ioException) {
        // @todo Gérer correctement le'exception.
      }
      return false;
    }
    
    return true;
  }
  
  /**
   * Retourne l'extension du fichier avec ou sans point, blanc sinon.
   */
  public String getExtension(boolean point) {
    if (isDirectory()) {
      return "";
    }
    
    if (extension == null) {
      // On recherche le point
      final String nom = getName();
      int pos = nom.lastIndexOf('.');
      if (pos == -1) {
        extension = "";
      }
      else {
        extension = nom.substring(pos + 1);
      }
    }
    
    // On retourne l'extension
    if (point) {
      return '.' + extension;
    }
    
    return extension;
  }
  
  /**
   * Retourne le nom sans l'extension.
   */
  public String getNamewoExtension(boolean pAvecLePoint) {
    if (namewoextension == null) {
      // On recherche le point
      final String nom = getName();
      int pos = nom.lastIndexOf('.');
      if (pos == -1) {
        namewoextension = nom;
      }
      else {
        namewoextension = nom.substring(0, pos);
      }
    }
    // On retourne le nom sans extension
    if (pAvecLePoint && !isDirectory()) {
      return namewoextension + '.';
    }
    
    return namewoextension;
  }
  
  /**
   * Supprime le fichier, le dossier et son contenu.
   * 
   */
  public void remove() {
    remove(this.getAbsoluteFile());
  }
  
  // -- Méthodes statiques
  
  /**
   * Suppression récursive d'un dossier ou un fichier.
   */
  public static void remove(File file) {
    
    if (file.isFile()) {
      file.delete();
    }
    else if (file.isDirectory()) {
      File[] listing = file.listFiles();
      for (int i = 0; i < listing.length; i++) {
        if (listing[i].isDirectory()) {
          remove(listing[i]);
        }
        else {
          listing[i].delete();
        }
      }
      file.delete();
    }
  }
  
  /**
   * Suppression récursive du contenu d'un dossier.
   */
  public static void removeContents(File file) {
    
    if (file.isFile()) {
      file.delete();
    }
    else if (file.isDirectory()) {
      File[] listing = file.listFiles();
      for (int i = 0; i < listing.length; i++) {
        if (listing[i].isDirectory()) {
          remove(listing[i]);
        }
        else {
          listing[i].delete();
        }
      }
    }
  }
  
  /**
   * Liste tous les fichiers d'un répertoire de manière récursive.
   */
  public List<File> listeFichier(ArrayList<File> tousLesFichiers) {
    if (tousLesFichiers == null) {
      tousLesFichiers = new ArrayList<File>();
    }
    if (exists()) {
      if (isDirectory()) {
        final File[] list = listFiles();
        if (list != null) {
          for (int i = 0; i < list.length; i++) {
            if (list[i].isDirectory()) {
              listeFichier(list[i], tousLesFichiers);
            }
            else {
              tousLesFichiers.add(list[i]);
            }
          }
        }
      }
    }
    return tousLesFichiers;
  }
  
  /**
   * Liste tous les fichiers d'un répertoire de manière récursive.
   */
  public static List<File> listeFichier(File repertoire, ArrayList<File> tousLesFichiers) {
    if (tousLesFichiers == null) {
      tousLesFichiers = new ArrayList<File>();
    }
    if (repertoire.exists()) {
      if (repertoire.isDirectory()) {
        final File[] list = repertoire.listFiles();
        if (list != null) {
          for (int i = 0; i < list.length; i++) {
            if (list[i].isDirectory()) {
              listeFichier(list[i], tousLesFichiers);
            }
            else {
              tousLesFichiers.add(list[i]);
            }
          }
        }
      }
    }
    return tousLesFichiers;
  }
  
  /**
   * Créer un FilenameFilter filtrant uniquement les fichiers dont le nom se termine par l'extension.
   */
  public static FilenameFilter avecExtension(final String extension) {
    if (extension == null) {
      return null;
    }
    else {
      return new FilenameFilter() {
        // @Override
        @Override
        public boolean accept(File repertoire, String nom) {
          return nom.endsWith(extension);
        }
      };
    }
  }
  
  /**
   * Filtre tous les fichiers d'un répertoire de manière récursive.
   */
  public List<File> filtreFichier(ArrayList<File> tousLesFichiers, FilenameFilter filtre) {
    if (tousLesFichiers == null) {
      tousLesFichiers = new ArrayList<File>();
    }
    if (exists()) {
      if (isDirectory()) {
        final File[] list = listFiles();
        if (list != null) {
          for (int i = 0; i < list.length; i++) {
            if (list[i].isDirectory()) {
              filtreFichier(list[i], tousLesFichiers, filtre);
            }
            else if (filtre.accept(list[i], list[i].getName())) {
              tousLesFichiers.add(list[i]);
            }
          }
        }
      }
    }
    return tousLesFichiers;
  }
  
  /**
   * Filtre tous les fichiers d'un répertoire de manière récursive.
   */
  public static List<File> filtreFichier(File repertoire, ArrayList<File> tousLesFichiers, FilenameFilter filtre) {
    if (tousLesFichiers == null) {
      tousLesFichiers = new ArrayList<File>();
    }
    if (repertoire.exists()) {
      if (repertoire.isDirectory()) {
        final File[] list = repertoire.listFiles();
        if (list != null) {
          for (int i = 0; i < list.length; i++) {
            if (list[i].isDirectory()) {
              filtreFichier(list[i], tousLesFichiers, filtre);
            }
            else if (filtre.accept(list[i], list[i].getName())) {
              tousLesFichiers.add(list[i]);
            }
          }
        }
      }
    }
    return tousLesFichiers;
  }
  
  /**
   * Nettoie le nom du dossier (suppression du séparateur à la fin, ...).
   */
  public static String cleanNameFolder(String folder) {
    if ((folder == null) || (folder.trim().equals(""))) {
      return folder;
    }
    
    folder = folder.trim();
    int pos = folder.lastIndexOf(File.separatorChar);
    if (pos == folder.length() - 1) {
      folder = folder.substring(0, pos);
    }
    
    return folder;
  }
  
  /**
   * Copie de fichiers.
   */
  public static void copy(final File from, final File to) throws IOException {
    if (from.isFile()) {
      copyFile(from, to);
    }
    else if (from.isDirectory()) {
      copyDirectory(from, to);
    }
    else {
      throw new FileNotFoundException(from.toString() + " does not exist");
    }
  }
  
  /**
   * Permet de copier un repertoire en passant en paramètre le repertoire source vers celui de destination.
   */
  public static void copyDirectory(final File from, final File to) throws IOException {
    if (!to.exists()) {
      to.mkdir();
    }
    final File[] inDir = from.listFiles();
    for (int i = 0; i < inDir.length; i++) {
      final File file = inDir[i];
      copy(file, new File(to, file.getName()));
      
    }
  }
  
  /**
   * Copie de fichiers.
   */
  public static void copyFile(final File from, final File to) throws IOException {
    FileInputStream in = null;
    FileOutputStream out = null;
    FileChannel sourceChannel = null;
    FileChannel destChannel = null;
    try {
      in = new FileInputStream(from);
      sourceChannel = in.getChannel();
      out = new FileOutputStream(to);
      destChannel = out.getChannel();
      destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
    }
    catch (Exception e) {
      new MessageErreurException(e, "Erreur lors de la copie du fichier.");
    }
    if (in != null) {
      in.close();
    }
    if (out != null) {
      out.close();
    }
    if (sourceChannel != null) {
      sourceChannel.close();
    }
    if (destChannel != null) {
      destChannel.close();
    }
  }
  
  /**
   * Calcule la clé de hashage en MD5 d'un fichier.
   */
  public static String calculerCleHashageMD5(File pFichier) {
    if (pFichier == null) {
      throw new MessageErreurException("Le chemin du fichier est invalide.");
    }
    if (!pFichier.exists()) {
      throw new MessageErreurException("Le fichier n'existe pas.");
    }
    if (!pFichier.isFile()) {
      throw new MessageErreurException("Le chemin du fichier ne pointe pas vers un fichier.");
    }
    
    try {
      InputStream inputStream = new FileInputStream(pFichier);
      return DigestUtils.md5Hex(inputStream);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Impossible de calculer la clé MD5 du fichier " + pFichier.getAbsolutePath());
    }
  }
  
}
