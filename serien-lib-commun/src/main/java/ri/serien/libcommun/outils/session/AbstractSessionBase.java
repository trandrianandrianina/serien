/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session;

import java.io.Serializable;

import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;

/**
 * Cette classe permet d'avoir une référence de SessionBase dans le projet LibCommun.
 * Car SessionBase du fait de ses dépendances avec le projet LibSwing ne peut être déplacée.
 */
public abstract class AbstractSessionBase implements Serializable {
  // Variables
  protected IdSession idSession = null;
  
  // -- Méthodes publiques
  
  public abstract void lancerPointMenu(IdEnregistrementMneMnp pIdEnregistrementMneMnp, Object pParametreMenu);
  
  // -- Accesseurs
  
  /**
   * Retourne l'id de la session.
   */
  public IdSession getIdSession() {
    return idSession;
  }
}
