/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.collection;

import java.util.ArrayList;
import java.util.EventListener;

import javax.swing.event.EventListenerList;

/**
 * Gestion d'évènement sur une ArrayList.
 */
public class ArrayListManager {
  // Variables
  private final EventListenerList listeners = new EventListenerList();
  private final ArrayList<Object> map = new ArrayList<Object>();
  // Paramètre par défaut (avant la modif), l'execution des écouteurs avant l'action
  private boolean avtClear = false;
  private boolean avtAdd = false;
  private boolean avtRemove = true;
  
  /**
   * Constructeur.
   */
  public ArrayListManager() {
    this(false, false, true);
  }
  
  /**
   * Constructeur.
   */
  public ArrayListManager(boolean avtclear, boolean avtadd, boolean avtremove) {
    avtClear = avtclear;
    avtAdd = avtadd;
    avtRemove = avtremove;
  }
  
  public static interface IArrayListListener extends EventListener {
    public void onDataCleared();
    
    public void onDataAdded(Object val);
    
    public void onDataRemoved(Object val);
    
    public void onDataRemoved(int indice);
  }
  
  public void addListener(IArrayListListener listener) {
    listeners.add(IArrayListListener.class, listener);
  }
  
  public void removeListener(IArrayListListener listener) {
    listeners.remove(IArrayListListener.class, listener);
  }
  
  public ArrayList<Object> getArrayList() {
    return map;
  }
  
  /**
   * Effacer les données de la liste.
   */
  public void clearObject() {
    if (avtClear) {
      final EventListener[] el = listeners.getListeners(IArrayListListener.class);
      for (int i = 0; i < el.length; i++) {
        ((IArrayListListener) el[i]).onDataCleared();
      }
      map.clear();
    }
    else {
      map.clear();
      final EventListener[] el = listeners.getListeners(IArrayListListener.class);
      for (int i = 0; i < el.length; i++) {
        ((IArrayListListener) el[i]).onDataCleared();
      }
    }
  }
  
  /**
   * Ajouter un objet de la liste.
   */
  public void addObject(Object val) {
    if (avtAdd) {
      final EventListener[] el = listeners.getListeners(IArrayListListener.class);
      for (int i = 0; i < el.length; i++) {
        ((IArrayListListener) el[i]).onDataAdded(val);
      }
      map.add(val);
    }
    else {
      map.add(val);
      final EventListener[] el = listeners.getListeners(IArrayListListener.class);
      for (int i = 0; i < el.length; i++) {
        ((IArrayListListener) el[i]).onDataAdded(val);
      }
    }
  }
  
  /**
   * Supprimer un objet de la liste.
   */
  public void removeObject(Object val) {
    if (avtRemove) {
      final EventListener[] el = listeners.getListeners(IArrayListListener.class);
      for (int i = 0; i < el.length; i++) {
        ((IArrayListListener) el[i]).onDataRemoved(val);
      }
      map.remove(val);
    }
    else {
      map.remove(val);
      final EventListener[] el = listeners.getListeners(IArrayListListener.class);
      for (int i = 0; i < el.length; i++) {
        ((IArrayListListener) el[i]).onDataRemoved(val);
      }
    }
  }
  
  /**
   * Supprimer un objet de la liste en le désignant via son indice.
   */
  public void removeObject(int indice) {
    if (avtRemove) {
      final EventListener[] el = listeners.getListeners(IArrayListListener.class);
      for (int i = 0; i < el.length; i++) {
        ((IArrayListListener) el[i]).onDataRemoved(indice);
      }
      map.remove(indice);
    }
    else {
      map.remove(indice);
      final EventListener[] el = listeners.getListeners(IArrayListListener.class);
      for (int i = 0; i < el.length; i++) {
        ((IArrayListListener) el[i]).onDataRemoved(indice);
      }
    }
  }
  
  /**
   * Récupérer un objet par son indice.
   */
  public Object getObject(int indice) {
    return map.get(indice);
  }
  
  /**
   * Retourner le nombre de listener actifs.
   */
  public int getNumberListeners() {
    return listeners.getListeners(IArrayListListener.class).length;
  }
  
  /**
   * Retourne le nombre d'éléments dans la liste d'objets.
   * @return
   */
  public int size() {
    return map.size();
  }
}
