/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils;

import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.apache.commons.codec.digest.DigestUtils;

import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

/**
 * Liste des constantes génériques utilisées dans le projet.
 * @todo : Récupérer le séparateur décimal & millier de manière dynamique pour la localisation.
 */
public class Constantes {
  // Génériques
  public static final int ON = 1;
  public static final int OFF = 0;
  public static final int OK = 0;
  public static final int ERREUR = -1;
  public static final int TRUE = 0;
  public static final int FALSE = -1;
  public static final int TRAITEE = -2;
  public static final int EN_COURS_TRAITEMENT = -3;
  public static final int BOTH = 3;
  public static final int HORIZONTAL = 2;
  public static final int VERTICAL = 1;
  public static final int NONE = -1;
  public static final int LEFT = 0;
  public static final int CENTER = 1;
  public static final int RIGHT = 2;
  public static final int TOP = 3;
  public static final int BOTTOM = 4;
  public static final int TAILLE_BUFFER = 32768;
  public static final int DATAQLONG = 5120;
  public static final int ESPACES_TABULATION = 4;
  
  // Délai attente sur dataqueue en seconde
  public static final int DELAI_ATTENTE_DATAQ_PROGRAMME = 15 * 60;
  public static final int DELAI_ATTENTE_DATAQ_MENU = -1;
  public static final int MAXVAR = 1000;
  public static final int DEBUG = TRUE;
  public static final String SEPARATEUR = "\t";
  public static final String SEPARATEUR_CHAINE = "|";
  public static final String SEPARATEUR_DOSSIER = "&";
  public static final char SEPARATEUR_CHAR = '\t';
  public static final char SEPARATEUR_CHAINE_CHAR = '|';
  public static final char SEPARATEUR_DOSSIER_CHAR = '&';
  public static final String COD_CRLF = "#CRLF#";
  public static final String crlf = System.getProperty("line.separator");
  public static final char[] SAUTLIGNE_CHAR = crlf.toCharArray();
  public static final String SAUTPAGE = "µ&&";
  public static final char[] SAUTPAGE_CHAR = SAUTPAGE.toCharArray();
  
  public static final char SEPARATEUR_DECIMAL_CHAR = ',';
  public static final String SEPARATEUR_DECIMAL = ",";
  public static final char SEPARATEUR_MILLIER_CHAR = '.';
  public static final String SEPARATEUR_MILLIER = ".";
  public static final int IMG_SYS = 0;
  public static final int IMG_EQU = 1;
  public static final int IMG_VAR = 2;
  public static final int PLAIN = 0;
  public static final int BOLD = 1;
  public static final int ITALIC = 2;
  public static final String THUMBAIL = "p_";
  public static final char SEPARATEUR_DATE_CHAR = '/';
  
  public static final char TYPE_ERR_SIMPLE = ' ';
  public static final char TYPE_WRNG_SIMPLE = 'W';
  
  // Numéros des requêtes
  public static final int STOP = 0;
  public static final int CONNEXION_OK = 1;
  public static final int DEMANDE_OUVERTURE_PANEL_SESSION = 2;
  public static final int DEMANDE_LISTE_RT = 3;
  public static final int ENVOI_LISTE_RT = 4;
  public static final int DEMANDE_FICHIER_RT = 5;
  public static final int ENVOI_FICHIER_RT = 6;
  public static final int RECOIT_FICHIER_RT = ENVOI_FICHIER_RT;
  public static final int ENVOI_BUFFER_GFX = 7;
  public static final int RECOIT_BUFFER_GFX = ENVOI_BUFFER_GFX;
  public static final int ENVOI_BUFFER_RPG = 8;
  public static final int RECOIT_BUFFER_RPG = ENVOI_BUFFER_RPG;
  public static final int DEMANDE_LISTE_RTCONFIG = 9;
  public static final int ENVOI_LISTE_RTCONFIG = 10;
  public static final int OUVERTURE_SESSION_OK = 11;
  public static final int DEMANDE_DEMARRE_SESSION = 12;
  public static final int DEMANDE_OUVERTURE_TRANSFERT_SESSION = 19;
  public static final int FORCE_AFFICHE_PANEL = 20;
  public static final int TRF_LIGNE_FIC_CSV = 21;
  public static final int ENVOI_ENREGISTREMENT_MENU = 22;
  public static final int RECOIT_ENREGISTREMENT_MENU = ENVOI_ENREGISTREMENT_MENU;
  public static final int RECOIT_CHOIX_MENU = 23;
  public static final int ENVOI_CHOIX_MENU = RECOIT_CHOIX_MENU;
  public static final int DEMANDE_STOP_SESSION = 24;
  public static final int RECOIT_INFOS_USER = 25;
  public static final int ENVOI_INFOS_USER = RECOIT_INFOS_USER;
  public static final int DEMANDE_OUVERTURE_MIRE_SESSION = 26;
  public static final int DEMANDE_FICHIER = 27;
  public static final int ENVOI_FICHIER = 28;
  public static final int RECOIT_FICHIER = ENVOI_FICHIER;
  public static final int DEMANDE_CONTENU_DOSSIER = 29;
  public static final int ENVOI_CONTENU_DOSSIER = 30;
  public static final int RECOIT_CONTENU_DOSSIER = ENVOI_CONTENU_DOSSIER;
  public static final int DEMANDE_PROTOCOLE_SERVEUR = 31;
  public static final int RECOIT_PROTOCOLE_SERVEUR = DEMANDE_PROTOCOLE_SERVEUR;
  public static final int ENVOI_DSPF = 32;
  public static final int RECOIT_DSPF = ENVOI_DSPF;
  public static final int ENVOI_REQUETE_EDITION = 33;
  public static final int RECOIT_REQUETE_EDITION = ENVOI_REQUETE_EDITION;
  public static final int PING = 34; // Permet de tester la validité d'une connexion de la part du serveur
  public static final int PONG = PING;
  public static final int UPLOAD_FICHIER = 35;
  public static final int SUPPRIME_FICHIER = 36;
  public static final int ENVOI_REQUETE_SYSTEME = 37;
  public static final int RECOIT_REQUETE_SYSTEME = ENVOI_REQUETE_SYSTEME;
  public static final int NOTIFICATION_FROM_RPG = 38;
  
  public static final int ANALYSE_DSPF = 1000000;
  public static final int HTTP_GET = 1195725856;
  public static final int HTTP_POST = 1347375956;
  public static final int HTTP_HEAD = 1212498244;
  
  // Valeur 100 en BigDecimal
  public static final BigDecimal VALEUR_CENT = BigDecimal.valueOf(100).setScale(0, RoundingMode.HALF_UP);
  public static final BigDecimal VALEUR_99 = BigDecimal.valueOf(99).setScale(0, RoundingMode.HALF_UP);
  public static final int DEUX_DECIMALES = 2;
  
  // Code message DTAQ
  public static final int LONGUEUR_CODE_RPGMSG = 5;
  public static final String RPGMSG_BUFFER = "00001";
  public static final String RPGMSG_BUFFER_FRC = "00002";
  public static final String RPGMSG_ERR = "00003";
  public static final String RPGMSG_CSV_TITLE = "10001";
  public static final String RPGMSG_CSV_DETAIL = "10002";
  public static final String RPGMSG_CSV_END = "10003";
  public static final String RPGMSG_MNU_GRP = "20001";
  public static final String RPGMSG_MNU_GRP_END = "20011";
  public static final String RPGMSG_MNU_MOD = "20002";
  public static final String RPGMSG_MNU_MOD_END = "20012";
  public static final String RPGMSG_MNU_REFRESH = "20013";
  public static final String RPGMSG_USR_INFOS = "20000";
  public static final String RPGMSG_NOTIFICATION = "30000";
  public static final String RPGMSG_DELAI_ATTENTE_EXPIRE = "99997";
  public static final String RPGMSG_JOBENDED = "99998";
  public static final String RPGMSG_ENDJOB = "99999";
  
  // Codes notification
  public static final int NOTIF_DLFILE = 1;
  
  // Propres au projet
  public static final String MAILTO = "assistance@resolution-informatique.com";
  // en ms = 1H si =0 alors ne coupe jamais
  public static final int SOCKET_TIMEOUT = 0;
  public static final int DATAQ_TIMEOUT = 3600000;
  public static final int MAXRECONNEXION = 1000;
  
  public static final String VERSION_PROTOCOLE = "01.02";
  public static final String VERSION_SERVER = VERSION_PROTOCOLE + ".02";
  public static final String VERSION_CLIENT = VERSION_PROTOCOLE + ".02";
  public static final int SERVER_PORT = 8888;
  
  public static final String IMG_ICON_APPLICATION = "images/ri_logo.png";
  public static final String IMG_FOND = "images/fond.png";
  public static final String INIT_LIBIMG = "libsimages.equ";
  public static final String INIT_TRADUCTION = "traduction_fr.equ";
  public static final String DOSSIER_IMAGE = "images";
  public static final String DOSSIER_RACINE = "serien";
  public static final String DOSSIER_LIB = "lib";
  public static final String DOSSIER_RT = "rt";
  public static final String DOSSIER_WEB = "www";
  public static final String DOSSIER_TMP = "tmp";
  public static final String DOSSIER_CONFIG = "config";
  public static final String DOSSIER_CTRL = "ctrl";
  public static final String DOSSIER_OTHERS = "others";
  public static final String DOSSIER_METIER = "metier";
  public static final String DOSSIER_MENUS = "menus";
  public static final String DOSSIER_SIM = "sim";
  public static final String DOSSIER_TMP_SIM = DOSSIER_TMP + File.separatorChar + DOSSIER_SIM;
  public static final String FIC_CONFIG = "rt.ini";
  
  // Couleurs définies dans le nouveau look.
  public static final Color COULEUR_MENUS = new Color(238, 239, 241);
  public static final Color COULEUR_F1 = new Color(90, 90, 90);
  public static final Color COULEUR_ERREURS = new Color(106, 23, 21);
  public static final Color COULEUR_NEGATIF = new Color(204, 0, 0);
  public static final Color COULEUR_LISTE_LETTRAGE = new Color(160, 70, 160);
  public static final Color COULEUR_LISTE_COMMENTAIRE = new Color(0, 102, 255);
  public static final Color COULEUR_LISTE_ANNULATION = new Color(140, 140, 140);
  public static final Color COULEUR_LISTE_FOND_BILAN = new Color(57, 105, 138);
  public static final Color COULEUR_LISTE_FOND_COMMENTAIRE = new Color(230, 240, 250);
  public static final Color COULEUR_LISTE_FOND_PROSPECT = new Color(230, 230, 240);
  public static final Color COULEUR_LISTE_FOND_ANNULATION = new Color(250, 210, 210);
  public static final Color COULEUR_LISTE_BILAN = Color.white;
  public static final Color COULEUR_LISTE_ERREUR = new Color(255, 44, 47);
  public static final Color COULEUR_TEXTE_DESACTIVE = new Color(77, 77, 80);
  public static final Color CL_ZONE_SORTIE = new Color(243, 243, 236);
  public static final Color CL_TEXT_SORTIE = new Color(0, 0, 0);
  
  // Constantes pour la conversion des String en Date.
  // Les longueurs sans séparateur servent pour parse les String en Date
  private static final int LONGUEUR_JJMMAAAA_SANS_SEPARATEUR = 8;
  private static final int LONGUEUR_JJMMAA_SANS_SEPARATEUR = 6;
  // Patterns de contrôle pour le formatage.
  private static final DateTimeFormatter PATTERN_CONTROLE_JJMMAAAA =
      DateTimeFormatter.ofPattern("ddMMyyyy").withResolverStyle(ResolverStyle.LENIENT);
  private static final DateTimeFormatter PATTERN_CONTROLE_JJMMAA =
      DateTimeFormatter.ofPattern("ddMMyy").withResolverStyle(ResolverStyle.LENIENT);
  private static final String PATTERN_FORMAT_JJMMAAA = "dd/MM/yyyy";
  
  // Liste contenant les correspondances des codes couleurs utilisés notamment dans les listes
  // Voir la doc http://resolution-informatique.com/documentation/?p=6417190 pour la mise à jour
  // l'ordre est couleur du texte puis couleur du fond
  public static final HashMap<String, Color> CORRESPONDANCE_COULEURS = new HashMap<String, Color>() {
    
    {
      put("PK", new Color(230, 0, 136));
    } // ROSE
    
    {
      // put("RD", new Color(205, 110, 110));
      put("RD", new Color(255, 0, 0));
    } // ROUGE
    
    {
      put("TQ", new Color(135, 255, 250));
    } // TURQUOISE
    
    {
      put("YL", new Color(240, 230, 150));
    } // JAUNE
    
    {
      // put("BL", new Color(180, 210, 255));
      put("BL", new Color(140, 140, 140));
    } // BLEU mais en fait gris
    
    {
      put("GR", new Color(200, 230, 110));
    } // VERT
    
    {
      put("OR", new Color(255, 210, 120));
    } // ORANGE
    
    {
      put("WH", new Color(255, 255, 255));
    } // BLANC
  };
  
  // Numéro des requêtes traitées par la classe TraitementRequeteSystemeMetier
  public static final int ID_TEXTTITLEBARINFORMATIONS = 1;
  
  public static final String EXT_BIN = ".dat";
  
  // Caractères interdits pour les noms de fichers
  public static char[] caracteresNonAutorisesPourFichier = { '\\', '/', ':', '*', '?', '"', '<', '>', '|' };
  
  // Variables pour la conversion des caractères unicodes
  private static String[] codeMinuscule = { "\\u00e9", "\\u00e8", "\\u00e0", "\\u00f4", "\\u00e2", "\\u00e4", "\\u00e7", "\\u00ea",
      "\\u00eb", "\\u00ee", "\\u00ef", "\\u00f6", "\\u00f9", "\\u00fb", "\\u00fc", "\\u00b0", "\\u00f3", "\\u20ac", "\\u2019" };
  private static String[] codeMajuscule = { "\\u00E9", "\\u00E8", "\\u00E0", "\\u00F4", "\\u00E2", "\\u00E4", "\\u00E7", "\\u00EA",
      "\\u00EB", "\\u00EE", "\\u00EF", "\\u00F6", "\\u00F9", "\\u00FB", "\\u00FC", "\\u00B0", "\\u00F3", "\\u20AC" };
  private static String[] lettre = { "\u00e9", "\u00e8", "\u00e0", "\u00F4", "\u00e2", "\u00e4", "\u00e7", "\u00ea", "\u00eb", "\u00ee",
      "\u00ef", "\u00f6", "\u00f9", "\u00fb", "\u00fc", "\u00b0", "\u00f3", "\u20ac", "\u2019" };
  private static String[] lettreori = { "é", "è", "à", "ô", "â", "ä", "ç", "ê", "ë", "î", "ï", "ö", "ù", "û", "ü", "°", "ó", "€", "’" };
  
  public static char[] spaces = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', // 260 espaces
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', };
  
  /**
   * Conversion des caractères unicodes.
   */
  public static StringBuffer code2unicode(StringBuffer bchaine) {
    // Analyse de la liste en minucule
    for (int j = 0; j < codeMinuscule.length; j++) {
      int k = bchaine.indexOf(codeMinuscule[j]);
      while (k != -1) {
        bchaine.replace(k, k + 6, lettre[j]);
        k = bchaine.indexOf(codeMinuscule[j], k);
      }
    }
    // Analyse de la liste en majuscule
    for (int j = 0; j < codeMajuscule.length; j++) {
      int k = bchaine.indexOf(codeMajuscule[j]);
      while (k != -1) {
        bchaine.replace(k, k + 6, lettre[j]);
        k = bchaine.indexOf(codeMajuscule[j], k);
      }
    }
    return bchaine;
  }
  
  /**
   * Conversion des caractères unicodes.
   */
  public static String[] code2unicode(String[] liste) {
    // Analyse de la liste
    for (int i = 0; i < liste.length; i++) {
      liste[i] = code2unicode(new StringBuffer(liste[i])).toString();
    }
    return liste;
  }
  
  /**
   * Conversion des caractères unicodes.
   */
  public static String code2unicode(String chaine) {
    return code2unicode(new StringBuffer(chaine)).toString();
  }
  
  /**
   * Conversion des caractères ascii en code.
   * @todo renommer la méthode en ascii2code ou mieux.
   */
  public static StringBuffer lettreori2code(StringBuffer bchaine) {
    // Analyse da la chaine
    for (int j = 0; j < codeMinuscule.length; j++) {
      int k = bchaine.indexOf(lettreori[j]);
      while (k != -1) {
        bchaine.replace(k, k + 1, codeMinuscule[j]);
        k = bchaine.indexOf(lettreori[j], k);
      }
    }
    
    return bchaine;
  }
  
  /**
   * Conversion des caractères ascii en code.
   */
  public static String lettreori2code(String chaine) {
    return lettreori2code(new StringBuffer(chaine)).toString();
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static int[] splitString2Int(String chaine, char separateur) {
    ArrayList<String> vlisting = splitString2List(chaine, separateur);
    int[] liste = new int[vlisting.size()];
    for (int i = 0; i < liste.length; i++) {
      liste[i] = Integer.parseInt(vlisting.get(i));
    }
    
    return liste;
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static String[] splitString(String chaine, char separateur) {
    ArrayList<String> vlisting = splitString2List(chaine, separateur);
    String[] liste = new String[vlisting.size()];
    for (int i = 0; i < liste.length; i++) {
      liste[i] = vlisting.get(i);
    }
    
    return liste;
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static String[] splitString(String chaine, String separateur) {
    ArrayList<String> vlisting = splitString2List(chaine, separateur);
    String[] liste = new String[vlisting.size()];
    for (int i = 0; i < liste.length; i++) {
      liste[i] = vlisting.get(i);
    }
    
    return liste;
  }
  
  /**
   * Retourne un tableau de String à partir d'une chaine découpée en morceaux avec la longueur définit.
   */
  public static String[] splitString(String pChaine, int pLongueurElement) {
    if (normerTexte(pChaine).isEmpty() || pLongueurElement < 1) {
      return null;
    }
    return pChaine.split("(?<=\\G.{" + pLongueurElement + "})");
  }
  
  /**
   * Découpe un texte pour en faire un tableau où chaque élément aura la même longueur voulue.
   * La variable pNombreElements doit contenir -1 si l'on a pas de besoin d'un nombre défini d'éléments.
   */
  public static String[] splitString(String pTexte, int pNombreElements, int pLongueurElement) {
    if (pTexte == null) {
      pTexte = "";
    }
    // Test sur le nombre d'éléments souhaités
    if (pNombreElements == 0) {
      throw new MessageErreurException("Pour le découpage de la chaine le nombre d'éléments ne peut être égal à 0.");
    }
    
    List<String> tokens = new ArrayList<String>();
    // Découpage à la longueur souhaitée sans limite du nombre d'élements
    if (pNombreElements < 0) {
      for (int start = 0; start < pTexte.length(); start += pLongueurElement) {
        tokens.add(pTexte.substring(start, Math.min(pTexte.length(), start + pLongueurElement)));
      }
    }
    // Découpage à la longueur souhaitée avec un nombre d'élements limités
    else {
      pTexte = String.format("%-" + pNombreElements * pLongueurElement + "s", pTexte);
      int index = 0;
      for (int start = 0; start < pTexte.length(); start += pLongueurElement) {
        tokens.add(pTexte.substring(start, Math.min(pTexte.length(), start + pLongueurElement)));
        index++;
        if (index >= pNombreElements) {
          break;
        }
      }
    }
    return tokens.toArray(new String[0]);
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static ArrayList<String> splitString2List(String chaine, char separateur) {
    int position = 0;
    int i = 0;
    ArrayList<String> vlisting = new ArrayList<String>(50);
    
    position = chaine.indexOf(separateur, i);
    while (position != -1) {
      if (i == position) {
        vlisting.add("");
      }
      else {
        vlisting.add(chaine.substring(i, position));
      }
      i = position + 1;
      position = chaine.indexOf(separateur, i);
    }
    vlisting.add(chaine.substring(i));
    
    return vlisting;
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static ArrayList<String> splitString2List(String chaine, String separateur) {
    int position = 0;
    int i = 0;
    int lgSep = separateur.length();
    ArrayList<String> vlisting = new ArrayList<String>(50);
    
    position = chaine.indexOf(separateur, i);
    while (position != -1) {
      if (i == position) {
        vlisting.add("");
      }
      else {
        vlisting.add(chaine.substring(i, position));
      }
      i = position + lgSep;
      position = chaine.indexOf(separateur, i);
    }
    vlisting.add(chaine.substring(i));
    
    return vlisting;
  }
  
  /**
   * Conversion des couleurs RGB en entier.
   */
  public static int rgb2int(int r, int g, int b) {
    return (r << 16) + (g << 8) + b;
  }
  
  /**
   * Affichage d'un tableau de byte sous forme de chaine hexadécimale.
   */
  public static String hexString(byte[] buf) {
    char[] tabByteHex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    
    StringBuffer sb = new StringBuffer(buf.length * 2);
    
    for (int i = 0; i < buf.length; i++) {
      sb.append(tabByteHex[(buf[i] >>> 4) & 0xf]);
      sb.append(tabByteHex[buf[i] & 0x0f]);
    }
    return sb.toString();
  }
  
  /**
   * Forcer le look et feel Nimbus.
   */
  public static void forcerLookAndFeelNimbus() {
    // On force le L&F Nimbus s'il est disponible
    try {
      for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    }
    catch (Exception e) {
      // @todo Gérer l'exception correctement.
    }
  }
  
  /**
   * Retourne la date avec gestion d'un délai en jour (en plus ou moins).
   */
  public static int getDate(int delai) {
    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.DAY_OF_MONTH, delai);
    
    return c.get(Calendar.YEAR) * 10000 + (c.get(Calendar.MONTH) + 1) * 100 + c.get(Calendar.DAY_OF_MONTH);
  }
  
  /**
   * Retourne la date.
   */
  public static int getDate() {
    return getDate(0);
  }
  
  /**
   * Retourne une date separée de la date donnée en paramètre suivant le délai donné en paramètre en années (en plus ou moins).
   */
  public static Date getDateDelaiAnnee(Date pDateDepart, int pNombreAnnees) {
    // si la date est à null on prend la date du jour.
    if (pDateDepart == null) {
      pDateDepart = new Date();
    }
    Calendar cal = Calendar.getInstance();
    cal.setTime(pDateDepart);
    cal.add(Calendar.YEAR, pNombreAnnees);
    
    return cal.getTime();
  }
  
  /**
   * Retourne une date separée de la date donnée en paramètre suivant le délai donné en paramètre en mois (en plus ou moins).
   */
  public static Date getDateDelaiMois(Date pDateDepart, int pNombreMois) {
    // si la date est à null on prend la date du jour.
    if (pDateDepart == null) {
      pDateDepart = new Date();
    }
    Calendar cal = Calendar.getInstance();
    cal.setTime(pDateDepart);
    cal.add(Calendar.MONTH, pNombreMois);
    
    return cal.getTime();
  }
  
  /**
   * Retourne une date separée de la date donnée en paramètre suivant le délai donné en paramètre en semaines (en plus ou moins).
   */
  public static Date getDateDelaiSemaine(Date pDateDepart, int pNombreSemaines) {
    // si la date est à null on prend la date du jour.
    if (pDateDepart == null) {
      pDateDepart = new Date();
    }
    Calendar cal = Calendar.getInstance();
    cal.setTime(pDateDepart);
    cal.add(Calendar.WEEK_OF_YEAR, pNombreSemaines);
    
    return cal.getTime();
  }
  
  /**
   * Retourne une date separée de la date donnée en paramètre suivant le délai donné en paramètre en jours (en plus ou moins).
   */
  public static Date getDateDelaiJour(Date pDateDepart, int pNombreJours) {
    // si la date est à null on prend la date du jour.
    if (pDateDepart == null) {
      pDateDepart = new Date();
    }
    Calendar cal = Calendar.getInstance();
    cal.setTime(pDateDepart);
    cal.add(Calendar.DAY_OF_MONTH, pNombreJours);
    
    return cal.getTime();
  }
  
  /**
   * Retourner le premier jour du mois précédent la date fournie en paramètre.
   */
  public static Date getDateDebutMoisPrecedent(Date pDate) {
    // si la date est à null on prend la date du jour.
    if (pDate == null) {
      pDate = new Date();
    }
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(pDate);
    calendar.add(Calendar.MONTH, -1);
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    return calendar.getTime();
  }
  
  /**
   * Retourne l'heure avec gestion d'un délai en seconde (en plus ou moins).
   */
  public static int getHeure(int delai) {
    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.SECOND, delai);
    
    return c.get(Calendar.HOUR_OF_DAY) * 10000 + (c.get(Calendar.MINUTE) + 1) * 100 + c.get(Calendar.SECOND);
  }
  
  /**
   * Retourne l'heure.
   */
  public static int getHeure() {
    return getHeure(0);
  }
  
  /**
   * Convertit une string sous forme AAMM en date (avec le 1° jour du mois).
   */
  public static Date convertirMoisAnneeEnDate(String pDateAconvertir) {
    if (pDateAconvertir != null) {
      try {
        // Conversion de la date de AAMM en 1AAMMJJ
        int value = Integer.parseInt(pDateAconvertir);
        value = ((value + 10000) * 100) + 1;
        return ConvertDate.db2ToDate(value, null);
      }
      catch (Exception e) {
      }
    }
    
    return null;
  }
  
  /**
   * Mettre à zéros les heures, minutes, seconde afin de ne conserver que le jour, mois, années.
   * @param pDate La date à modifier.
   * @return La date.
   */
  public static Date mettreHeureAZero(Date pDate) {
    if (pDate == null) {
      return null;
    }
    
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(pDate);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    
    return calendar.getTime();
  }
  
  /**
   * Retourne le dossier racine du serveur Série N en cours d'exécution.
   */
  public static String getApplicationRootDirectory(final Class<?> classe) {
    String u = classe.getResource('/' + classe.getName().replace('.', '/') + ".class").toString();
    int deb = u.lastIndexOf(':');
    int fin = u.indexOf(Constantes.DOSSIER_RT);
    return u.substring(deb + 1, fin - 1);
  }
  
  /**
   * Retourne le dossier home de l'utilisateur.
   * (quelques soit le système et surtout la configuration de Windows).
   */
  public static String getUserHome() {
    String home = System.getenv("USERPROFILE");
    if (home == null || Constantes.normerTexte(home).isEmpty()) {
      return System.getProperty("user.home");
    }
    else {
      return home;
    }
  }
  
  /**
   * Retourne le chemin vers le dossier du bureau si possible, null sinon.
   */
  public static String getDossierBureau() {
    String cheminUserHome = Constantes.normerTexte(Constantes.getUserHome());
    if (cheminUserHome.isEmpty()) {
      return null;
    }
    return cheminUserHome + File.separatorChar + "Desktop";
  }
  
  /**
   * Analyse et récupère les paramètres contenus dans une chaine.
   */
  public static void analyseParametres(String ligne, HashMap<String, String> parametres) {
    if ((ligne == null) || (ligne.trim().equals(""))) {
      return;
    }
    
    ligne = ligne.trim();
    String[] liste = ligne.split(" ");
    String key = null;
    String value = null;
    int pos = -1;
    for (String chaine : liste) {
      if (chaine.startsWith("-")) {
        pos = chaine.indexOf('=');
        if (pos != -1) {
          key = chaine.substring(1, pos);
          value = chaine.substring(pos + 1);
          if (value.charAt(0) != '"') {
            parametres.put(key, value);
            key = null;
            value = null;
          }
          else if (value.charAt(value.length() - 1) == '"') {
            parametres.put(key, value.substring(1, value.length() - 1));
            key = null;
            value = null;
          }
        }
        else {
          key = chaine.substring(1);
          value = "true";
          parametres.put(key, value);
          key = null;
          value = null;
        }
      }
      else if (key != null) {
        value += " " + chaine;
        if (value.charAt(value.length() - 1) == '"') {
          parametres.put(key, value.substring(1, value.length() - 1));
          key = null;
          value = null;
        }
      }
    }
  }
  
  /**
   * Retourne s'il s'agit de l'OS400.
   */
  public static boolean isOs400() {
    return System.getProperty("os.name").equalsIgnoreCase("OS/400");
  }
  
  /**
   * Retourne s'il s'agit de Windows.
   */
  public static boolean isWindows() {
    return (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0);
  }
  
  /**
   * Retourne s'il s'agit de Os X.
   */
  public static boolean isMac() {
    return (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0);
  }
  
  /**
   * Retourne s'il s'agit d'unix.
   */
  public static boolean isUnix() {
    String os = System.getProperty("os.name").toLowerCase();
    return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0);
  }
  
  /**
   * Convertir une chaîne en valeur numérique décimale.
   */
  public static BigDecimal convertirTexteEnBigDecimal(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("La valeur à convertir n'est pas un nombre décimal valide.");
    }
    
    // Remplacer le point comme séparateur décimal par une virgule
    pValeur = pValeur.replace('.', ',');
    
    // Supprimer les séparateurs de milliers
    pValeur = normerTexte(pValeur).replaceAll(" ", "");
    
    // Un texte vide équivaut à la valeur 0.
    if (pValeur.isEmpty()) {
      return BigDecimal.ZERO;
    }
    
    // Convertir le texte en nombre
    try {
      NumberFormat formatNombreDecimal = NumberFormat.getInstance(Locale.FRANCE);
      return new BigDecimal(formatNombreDecimal.parse(pValeur).toString());
    }
    catch (ParseException e) {
      throw new MessageErreurException("La valeur '" + pValeur + "' n'est pas un nombre décimal valide.");
    }
  }
  
  /**
   * Convertir une chaîne en valeur numérique décimale.
   * Cette méthode offre la prossiblité de retourner la valeur 0 en cas d'exception.
   */
  public static BigDecimal convertirTexteEnBigDecimal(String pValeur, boolean pZeroSiException) {
    // Pas de protection dans le cas d'une exeption
    if (!pZeroSiException) {
      return convertirTexteEnBigDecimal(pValeur);
    }
    
    // Protection contre une éventuelle exception : la valeur zero est retournée
    try {
      return convertirTexteEnBigDecimal(pValeur);
    }
    catch (Exception e) {
      return BigDecimal.ZERO;
    }
  }
  
  /**
   * Convertir une valeur décimale en chaîne de caractère en respectant la précision de l'objet BigDecimal.
   * 
   * @param pValeur Nombre à convertir sous forme de chaîne de caractères.
   * @return Chaîne de caractères avec le nombre représenté avec autant de décimales que sa précision intrinsèque.
   */
  public static String convertirBigDecimalEnTexte(BigDecimal pValeur) {
    if (pValeur == null) {
      return "";
    }
    return String.format(Locale.FRANCE, "%." + pValeur.scale() + "f", pValeur);
  }
  
  /**
   * Convertir une valeur décimale en chaîne de caractère en imposant une précision fournie.
   * 
   * @param pValeur Nombre à convertir sous forme de chaîne de caractères.
   * @param pNombreDecimale Nombre de décimales à utiliser pour formater le nombre.
   * @return Chaîne de caractères avec le nombre représenté avec autant de décimales que la précision fournie en paramètre.
   */
  public static String convertirBigDecimalEnTexte(BigDecimal pValeur, int pNombreDecimale) {
    if (pValeur == null) {
      return "";
    }
    return String.format(Locale.FRANCE, "%." + pNombreDecimale + "f", pValeur);
  }
  
  /**
   * Convertir une chaîne en pourcentage.
   */
  public static BigPercentage convertirTexteEnBigPercentage(String pValeur, EnumFormatPourcentage pFormatPourcentage) {
    return new BigPercentage(convertirTexteEnBigDecimal(pValeur), pFormatPourcentage);
  }
  
  /**
   * Formater un pourcentage en chaîne de caractères.
   * 
   * @param pTaux Valeur du pourcentage (null est accepté).
   * @param pAvecZeroNonSignificafifs true=conserver les zéros non significatifs, false=supprimer les zéros non significatifs.
   * @return Pourcentage sous forme de chaîne de caractères.
   */
  public static String formater(BigPercentage pTaux, boolean pAvecZeroNonSignificafifs) {
    if (pTaux == null) {
      return "";
    }
    return formater(pTaux.getTauxEnPourcentage(), pAvecZeroNonSignificafifs);
  }
  
  /**
   * Formater une valeur décimale en chaîne de caractères en respectant la précision de l'objet BigDecimal.
   * On peut conserver (true) ou supprimer (false) les zéros non significatifs.<br><br>
   * 
   * Le formatage de la précision est volontairement codé jusqu'à 6 caractères. Cela permet de mettre en valeur les éventuels calculs qui
   * ne respecteraient pas la précision maximale. En effet si le résultat d'un calcul a une précision trop élevée, cela ne doit pas être
   * masqué par l'affichage. Il faut pouvoir le détecter et le corriger pour éviter les bugs dûs à des arrondis non maîtrisés.
   */
  public static String formater(BigDecimal pValeur, boolean pAvecZeroNonSignificafifs) {
    if (pValeur == null) {
      return "";
    }
    if (pAvecZeroNonSignificafifs) {
      // Par défaut, on soumet deux chiffres après la virgule
      int decimalApresVirgule = 2;
      // Si la valeur possède sa précision, on le prend
      if (pValeur.scale() > 1) {
        decimalApresVirgule = pValeur.scale();
      }
      return String.format(Locale.FRANCE, "%." + decimalApresVirgule + "f", pValeur);
    }
    else {
      DecimalFormat formatDecimal = new DecimalFormat("0.######");
      return formatDecimal.format(pValeur);
    }
  }
  
  /**
   * Convertie une chaine en valeur numérique entière.
   * TODO A améliorer
   */
  public static int convertirTexteEnInteger(String pValeur) {
    pValeur = normerTexte(pValeur);
    if (pValeur.isEmpty()) {
      return 0;
    }
    try {
      return Integer.parseInt(pValeur.trim());
    }
    catch (NumberFormatException e) {
      try {
        NumberFormat format = NumberFormat.getInstance();
        Number number = format.parse(pValeur);
        return number.intValue();
      }
      catch (ParseException e1) {
      }
    }
    return 0;
  }
  
  /**
   * Convertie une chaine en valeur numérique entière.
   */
  public static String convertirIntegerEnTexte(int pValeur, int pNombreChiffres) {
    if (pNombreChiffres > 0) {
      return String.format("%0" + pNombreChiffres + "d", pValeur);
    }
    return String.format("%d", pValeur);
  }
  
  /**
   * Convertir une date en texte pour affichage à l'écran.
   * Le format est : JJ/MM/AAAA.
   */
  public static String convertirDateEnTexte(Date pDate) {
    if (pDate == null) {
      return "";
    }
    return String.format("%1$td/%1$tm/%1$tY", pDate);
  }
  
  /**
   * Convertir une date et heure en texte pour affichage à l'écran.
   * Le format est : JJ/MM/AAAA hh:mm.
   */
  public static String convertirDateHeureEnTexte(Date pDate) {
    return String.format("%1$td/%1$tm/%1$tY %1$tH:%1$tM", pDate);
  }
  
  /**
   * Convertir une chaine de caractères en Date au format jj/mm/aaaa. <br><br>
   * 
   * La chaîne de caractères doit contenir une date au format jour, mois, année.<br>
   * - Le jour est supporté sur 2 caractères.<br>
   * - Le mois est supporté sur 2 caractères.<br>
   * - L'année est supportée sur 2 ou 4 caractères.<br>
   * - Le séparateur n'est pas obligatoire.<br>
   *
   * @param pStringDate Date au format String.
   * @return Date au format jj/mm/aaaa.
   */
  public static Date convertirTexteEnDate(String pStringDate) {
    // Contrôle des null.
    if (pStringDate == null) {
      return null;
    }
    DateTimeFormatter controlPattern = null;
    // On supprime tous les caractères non numériques de la chaîne de caractères.
    String dateChiffreSeulement = pStringDate.replaceAll("[^0-9]", "");
    
    // Si la nouvelle chaîne est vide, on retourne null.
    if (Constantes.normerTexte(dateChiffreSeulement).isEmpty()) {
      return null;
    }
    try {
      // On contrôle la longueur de la nouvelle chaine de caractères pour déterminer le pattern de contrôle à appliquer.
      if (dateChiffreSeulement.length() == LONGUEUR_JJMMAAAA_SANS_SEPARATEUR) {
        controlPattern = PATTERN_CONTROLE_JJMMAAAA;
      }
      else if (dateChiffreSeulement.length() == LONGUEUR_JJMMAA_SANS_SEPARATEUR) {
        controlPattern = PATTERN_CONTROLE_JJMMAA;
      }
      // Si la longueur de la chaîne ne correspond à aucun des deux formats, on retourne null.
      else {
        return null;
      }
      
      // On parse la date avec le formatter.
      LocalDate dateControle = LocalDate.parse(dateChiffreSeulement, controlPattern);
      // On crée un formatter pour uniformiser le format des dates.
      DateTimeFormatter formatterPattern = DateTimeFormatter.ofPattern(PATTERN_FORMAT_JJMMAAA);
      // On formate la date contrôlée.
      String dateFormate = formatterPattern.format(dateControle);
      // On retourne la date on format Date et syntaxe jj/mm/aaaa.
      return new SimpleDateFormat(PATTERN_FORMAT_JJMMAAA).parse(dateFormate);
      
    }
    catch (Exception e) {
      return null;
    }
  }
  
  /**
   * Formate une date pour lui retirer sa partie horaire. <br><br>
   * 
   * En réalité, passe toutes les composantes de la partie horaire à 0.
   * 
   * @param pDate Date à formater
   * @return Date formatée sans heure.
   */
  public static Date formaterDateSansHeure(Date pDate) {
    // La dat est stockée dans un objet calendar modifiable.
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(pDate);
    // Toutes les données horaires de la date sont mises à 0.
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    
    return calendar.getTime();
  }
  
  /**
   * Permet de normaliser le contenu d'une chaîne de texte saisie.
   * Remplacer null par une chaîne vide, enlever les espaces, gérer les caractères spéciaux.
   */
  public static String normerTexte(String pTexte) {
    if (pTexte == null) {
      return "";
    }
    return pTexte.trim();
  }
  
  /**
   * Permet de normaliser le contenu d'une date.
   * Remplacer null par la date du jour.
   */
  public static Date normerDate(Date pDate) {
    if (pDate == null) {
      return new Date();
    }
    return pDate;
  }
  
  /**
   * Retourne un double avec un nombre de chiffres après la virgule donné.
   * Utilise la règle classique de l'arrondi. Exemple :
   * 17.12465454 -> 17.12
   * 17.12545454 -> 17.13
   */
  public static double arrondiClassique(double pValeur, int pNombreChiffres) {
    if (Double.isNaN(pValeur)) {
      return 0;
    }
    if (pNombreChiffres < 0) {
      throw new IllegalArgumentException();
    }
    
    BigDecimal bd = new BigDecimal(pValeur);
    return bd.setScale(pNombreChiffres, RoundingMode.HALF_UP).doubleValue();
  }
  
  /**
   * Retourne un double avec un nombre de chiffres après la virgule donné.
   * Arrondi à la valeur inférieure. Exemple:
   * 17.56655454 -> 17.56
   * 17.52445454 -> 17.52
   */
  public static double arrondiInferieur(double pValeur, int pNombreChiffres) {
    if (Double.isNaN(pValeur)) {
      return 0;
    }
    if (pNombreChiffres < 0) {
      throw new IllegalArgumentException();
    }
    
    BigDecimal bd = new BigDecimal(pValeur);
    return bd.setScale(pNombreChiffres, RoundingMode.DOWN).doubleValue();
  }
  
  /**
   * Retourne la valeur maximum possible d'une zone numérique (RPG).
   */
  public static long valeurMaxZoneNumerique(int pNombreChiffres) {
    return (long) (Math.pow(10, pNombreChiffres) - 1);
  }
  
  /**
   * Formate simplement une chaine de caractères (trim et controle de la longueur) afin que sa longueur ne dépasse pas
   * la longueur voulu.
   */
  public static String formaterStringMax(String pString, int pLongueur, boolean pTrim) {
    if (pTrim) {
      pString = normerTexte(pString);
    }
    else {
      if (pString == null) {
        pString = "";
      }
    }
    if (pString.length() > pLongueur) {
      pString = pString.substring(0, pLongueur);
    }
    return pString;
  }
  
  /**
   * Formate strictement une chaine de caractères à la longueur voulue.
   */
  public static String formaterStringStrict(String pString, int pLongueur) {
    if (pString == null) {
      pString = "";
    }
    if (pString.length() > pLongueur) {
      pString = pString.substring(0, pLongueur);
    }
    else {
      pString = String.format("%-" + pLongueur + "s", pString);
    }
    return pString;
  }
  
  /**
   * Supprime les espaces et équivalents à la fin du texte.
   */
  public static String trimRight(String pTexte) {
    if (pTexte == null) {
      return "";
    }
    return trimRight(new StringBuilder(pTexte));
  }
  
  /**
   * Supprime les espaces et équivalents à la fin du texte.
   */
  public static String trimRight(StringBuilder pTexte) {
    if (pTexte == null) {
      return "";
    }
    
    // Parcourt du texte de la droite vers la gauche
    for (int i = pTexte.length() - 1; i > 0; i--) {
      if (pTexte.charAt(i) == ' ' || pTexte.charAt(i) == '\n' || pTexte.charAt(i) == '\t' || pTexte.charAt(i) == '\r') {
        pTexte.deleteCharAt(i);
      }
      else {
        break;
      }
    }
    return pTexte.toString();
  }
  
  /**
   * Parcourt un tableau et retoune le nombre d'éléments non null consécutifs.
   */
  public static int controlerElementsTableau(Object[] pTableau) {
    if (pTableau == null) {
      return 0;
    }
    // On controle le nombre d'élements non null
    int nombreElements = 0;
    for (int i = 0; i < pTableau.length; i++) {
      if (pTableau[i] == null) {
        break;
      }
      nombreElements++;
    }
    return nombreElements;
  }
  
  /**
   * Conversion du fichier xml de la sérialisation des classes pour la 2.14.
   * @throws Exception
   */
  public static boolean convert213to214(String pFichierAConvertir, String pNomPackage) throws Exception {
    String fichierXML = Constantes.normerTexte(pFichierAConvertir);
    if (fichierXML.isEmpty()) {
      throw new Exception("Le nom du fichier est vide.");
    }
    
    // Chargement du fichier xml en mode texte
    GestionFichierTexte gft = new GestionFichierTexte(fichierXML);
    ArrayList<String> contenu = gft.getContenuFichier();
    
    // Modification des noms des packages non officiels
    boolean enregistrer = false;
    for (int i = 0; i < contenu.size(); i++) {
      if (contenu.get(i).contains("class=\"") && !contenu.get(i).contains("class=\"java")
          && !contenu.get(i).contains("class=\"" + pNomPackage)) {
        contenu.set(i, contenu.get(i).replaceFirst("class=\"", "class=\"" + pNomPackage));
        enregistrer = true;
      }
    }
    if (enregistrer) {
      if (gft.ecritureFichier() == Constantes.ERREUR) {
        throw new Exception(gft.getMsgErreur());
      }
    }
    return enregistrer;
  }
  
  /**
   * Tester l'égalité entre deux objets en gérant correctement les valeurs null.
   * Si la deux valeurs passées sont null, la métohde retourne true.
   * Si les deux valeurs, sont des BigDecimal, cela compare les valeurs.
   */
  public static boolean equals(Object pObject1, Object pObject2) {
    if (pObject1 == null) {
      return pObject2 == null;
    }
    else if (pObject1 instanceof BigDecimal && pObject2 instanceof BigDecimal) {
      return ((BigDecimal) pObject1).compareTo((BigDecimal) pObject2) == 0;
    }
    else {
      return pObject1.equals(pObject2);
    }
  }
  
  /**
   * Controle la validité de la syntaxe d'une adresse mail.
   */
  public static boolean controlerEmail(String pEmail) {
    pEmail = normerTexte(pEmail).toLowerCase();
    if (pEmail.isEmpty()) {
      return false;
    }
    
    // Contrôle plus poussé qui respecte la norme RFC 5322
    // Règle regex trouvé sur le site https://emailregex.com (fonctionne dans 99.99% des cas)
    return pEmail.matches("(?:[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+)*"
        + "|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@"
        + "(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
        + "|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])"
        + "|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
  }
  
  /**
   * Mettre la première lettre d'une chaîne de caractère en majuscule et les autres en minuscules.
   *
   * @param pTexte Texte dont il faut mettre la première lettre en majuscules.
   * @return Texte transformé.
   */
  public static String capitaliserPremiereLettre(String pTexte) {
    if (pTexte == null) {
      return null;
    }
    else if (pTexte.isEmpty()) {
      return "";
    }
    else if (pTexte.length() == 1) {
      return pTexte.substring(0, 1).toUpperCase();
    }
    else {
      return pTexte.substring(0, 1).toUpperCase() + pTexte.substring(1).toLowerCase();
    }
  }
  
  /**
   * Permet d'extraire une chaine Base 64 lorqu'elle est encadrée par des balises du genre "----" (peut importe le nombre).
   */
  public static String extraireChaineBase64(String pChaine) {
    pChaine = normerTexte(pChaine);
    if (pChaine.isEmpty()) {
      return null;
    }
    
    // Suppression des caractères \r et \t
    pChaine = pChaine.replaceAll("\r|\t", "");
    
    // Contrôle de la présence de balises
    if (pChaine.indexOf('-') == -1) {
      return pChaine;
    }
    
    // Extraction de la licence entre les balises (à l'origine il y a {4} et non {0,4} car la chaine Base64 devrait être un multiple de 4)
    Pattern pattern = Pattern.compile("(?:[\\nA-Za-z0-9+\\/]{0,4})*(?:[A-Za-z0-9+\\/]{2}==|[A-Za-z0-9+\\/]{3}=)");
    Matcher matcher = pattern.matcher(pChaine);
    if (matcher.find()) {
      pChaine = matcher.group(0);
    }
    return pChaine.replaceAll("\n", "").trim();
  }
  
  /**
   * Calcule le hashcode d'une chaine de caractères au format MD5.
   */
  public static String calculerHashcodeMD5(String pChaine) {
    // Contrôle de la chaine
    if (Constantes.normerTexte(pChaine).isEmpty()) {
      return null;
    }
    
    // Calcul du hashcode
    try {
      return DigestUtils.md5Hex(pChaine);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Impossible de calculer la clé MD5 de la chaine " + pChaine);
    }
  }
  
  /**
   * Génère une chaine de caractères aléatoires de longueur voulue.
   * La chaine contient :
   * - des chiffres
   * - des lettres minuscules
   */
  public static String genererRandomString(int pLongueur) {
    String uuid = "";
    do {
      uuid += UUID.randomUUID().toString().replace("-", "");
    }
    while (uuid.length() < pLongueur);
    return uuid.substring(0, pLongueur);
  }
  
  /**
   * Affiche un fichier avec l'application associé à son extension.
   */
  public static void afficherFichierAvecApplication(String pCheminFichier, boolean pOuvrirAvecNavigateur) {
    pCheminFichier = normerTexte(pCheminFichier);
    if (pCheminFichier.isEmpty()) {
      return;
    }
    
    // Contrôle du fichier
    File fichierAAfficher = new File(pCheminFichier);
    if (!fichierAAfficher.exists()) {
      throw new MessageErreurException("Le fichier " + pCheminFichier + " n'existe pas.");
    }
    
    if (!Desktop.isDesktopSupported()) {
      throw new MessageErreurException("Le fichier " + pCheminFichier + " n'est pas affichable.");
    }
    
    // Ouverture avec le navigateur
    if (pOuvrirAvecNavigateur) {
      try {
        Desktop.getDesktop().browse(new URI(pCheminFichier));
      }
      catch (IOException e) {
        throw new MessageErreurException("Le fichier " + pCheminFichier + " n'est pas affichable.");
      }
      catch (URISyntaxException e) {
        throw new MessageErreurException("Le chemin du fichier n'est pas valide : " + pCheminFichier + '.');
      }
    }
    // Ouverture avec l'application associée
    else {
      try {
        Desktop.getDesktop().open(fichierAAfficher);
      }
      catch (IOException e) {
        throw new MessageErreurException("Le fichier " + pCheminFichier + " n'est pas affichable.");
      }
    }
  }
  
  /**
   * Convertie et normalise un objet en texte.
   * Idéale pour convertir un objet d'un champ d'enregistrement résultant d'une requête.
   * 
   * @param pObjet Objet à convertir
   * @return
   */
  public static String convertirObjetEnTexte(Object pObjet) {
    if (pObjet == null) {
      return "";
    }
    
    if (pObjet instanceof BigDecimal) {
      return normerTexte(pObjet.toString());
    }
    else {
      return normerTexte((String) pObjet);
    }
  }
  
  /**
   * Teste la validité d'une adresse IPv4.
   * 
   * @param pAdresseIp Addresse IP à valider
   * @return true si l'adresse est valide, autrement false
   */
  public static boolean isAdresseIpV4(String pAdresseIp) {
    if (normerTexte(pAdresseIp).isEmpty()) {
      return false;
    }
    Pattern pattern = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    Matcher matcher = pattern.matcher(pAdresseIp);
    return matcher.matches();
  }
}
