/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.pourcentage;

/**
 * Format d'un nombre exprimant un pourcentage.
 * 
 * Un pourcentage peut être exprimé sous la forme d'un nombre décimal (0,2000) ou d'un pourcentage (20,00%). Cette énumération est
 * utilisée dans la classe BigPercentage pour indiquer la forme du nombre fourni comme pourcentage.
 * 
 * DECIMALE = 0,20000
 * POURCENTAGE = 20,00
 */
public enum EnumFormatPourcentage {
  DECIMAL,
  POURCENTAGE;
}
