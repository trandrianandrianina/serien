/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.composants;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import ri.serien.libcommun.outils.Constantes;

/**
 * Description de l'objet Key
 */
public class oKey extends GenericBase {
  // Constantes erreurs
  private final static String ERREUR_DATA_NULL = "Tableau binaire non initialisé.";
  private final static String ERREUR_LONGUEUR_KEY_ERRONNE = "La longueur du tableau descriptif est supérieure à celle du record.";
  private final static String ERREUR_CONVERT_INDICATOR = "INDICATOR impossible à convertir en numérique.";
  
  // Constantes ID, il y a 500 variables possibles pour l'objet oKey
  // Chaque variable doit obligatoirement avoir un ID (pour la compilation)
  private final static int ID_OKEY = 2500;
  private final static int ID_KEY = ID_OKEY + 1;
  // private final static int ID_INDICATEUR=ID_OKEY+2;
  private final static int ID_VISIBILITY = ID_OKEY + 2;
  private final static int ID_REFINDICATEUR = ID_OKEY + 3;
  private final static int ID_MAJDATA = ID_OKEY + 4;
  
  static private String ListeToucheFonction[][] = { { "CA01", "F1" }, { "CA02", "F2" }, { "CA03", "F3" }, { "CA04", "F4" },
      { "CA05", "F5" }, { "CA06", "F6" }, { "CA07", "F7" }, { "CA08", "F8" }, { "CA09", "F9" }, { "CA10", "F10" }, { "CA11", "F11" },
      { "CA12", "F12" }, { "CA13", "F13" }, { "CA14", "F14" }, { "CA15", "F15" }, { "CA16", "F16" }, { "CA17", "F17" }, { "CA18", "F18" },
      { "CA19", "F19" }, { "CA20", "F20" }, { "CA21", "F21" }, { "CA22", "F22" }, { "CA23", "F23" }, { "CA24", "F24" }, { "CF01", "F1" },
      { "CF02", "F2" }, { "CF03", "F3" }, { "CF04", "F4" }, { "CF05", "F5" }, { "CF06", "F6" }, { "CF07", "F7" }, { "CF08", "F8" },
      { "CF09", "F9" }, { "CF10", "F10" }, { "CF11", "F11" }, { "CF12", "F12" }, { "CF13", "F13" }, { "CF14", "F14" }, { "CF15", "F15" },
      { "CF16", "F16" }, { "CF17", "F17" }, { "CF18", "F18" }, { "CF19", "F19" }, { "CF20", "F20" }, { "CF21", "F21" }, { "CF22", "F22" },
      { "CF23", "F23" }, { "CF24", "F24" }, { "ROLLUP", "PGDOWN" }, { "ROLLDOWN", "PGUP" }, { "PRINT", "PRINT" }, { "HELP", "HELP" }, };
  
  // Variables
  private iIndicator iindicateur = null;
  
  private String key = null; // Nom de la touche
  private boolean majData = true; // Controle la maj des données en fonction du CA (pas de maj) & CF (maj)
  private String indicateur[] = null;
  private byte refindicator = -1; // L'indicateur lié à la touche (par ex: 89 /90 pour les pgup & pgdown)
  private String visibility[] = null;
  
  /**
   * Retourne le nom de l'objet
   * @return
   */
  public String getNom() {
    return getName();
  }
  
  /**
   * Initialise le nom de l'objet
   * @param valeur
   */
  public void setNom(String valeur) {
    setName(valeur.trim());
  }
  
  /**
   * Initialise le nom du Record
   * @param valeur
   */
  public void setRecord(String valeur) {
    setParent(valeur.trim());
  }
  
  /**
   * Initialise la touche
   * @param valeur
   */
  public void setKey(String valeur) {
    for (int i = 0; i < ListeToucheFonction.length; i++) {
      if (valeur.equals(ListeToucheFonction[i][0])) {
        key = ListeToucheFonction[i][1];
        return;
      }
    }
    key = valeur;
  }
  
  /**
   * Retourne la touche
   * @return
   */
  public String getKey() {
    return key;
  }
  
  /**
   * @param majData the majData to set
   */
  public void setMajData(boolean majData) {
    this.majData = majData;
  }
  
  /**
   * @return the majData
   */
  public boolean isMajData() {
    return majData;
  }
  
  /**
   * Initialise l'interpréteur des indicateurs
   * @param aiidicateur
   */
  public void setInterpreteurI(iIndicator aiindicateur) {
    iindicateur = aiindicateur;
  }
  
  /**
   * Initialise les indicateurs
   * @param valeur
   */
  public void setIndicateur(String valeur) {
    if (valeur == null) {
      return;
    }
    if (valeur.trim().equals("")) {
      return;
    }
    iIndicator ind = new iIndicator();
    indicateur = ind.tabconvert2notpol(valeur.trim());
  }
  
  public void setIndicateur(String valeur[]) {
    visibility = valeur;
  }
  
  /**
   * Retourne les indicateurs
   * @return
   * 
   *         public String[] getIndicateur()
   *         {
   *         return indicateur;
   *         }
   */
  
  /**
   * Initialise les indicateurs sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public void setIndicateurPol(String valeur) {
    indicateur = valeur.split(" ");
  }
  
  /**
   * Retourne les indicateurs sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public String getIndicateurPol() {
    if (indicateur == null) {
      return null;
    }
    
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < indicateur.length; i++) {
      sb.append(indicateur[i]).append(' ');
    }
    return sb.toString().trim();
  }
  
  /**
   * Initialise l'indicateur
   * @param val
   * @return
   */
  public int setRefIndicator(String val) {
    try {
      refindicator = (byte) Integer.parseInt(val);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_INDICATOR + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setRefIndicator(int val) {
    refindicator = (byte) val;
    return Constantes.OK;
  }
  
  /**
   * Retourne l'indicateur lié
   * @return
   */
  public byte getRefIndicator() {
    return refindicator;
  }
  
  /**
   * Initialise les indicateurs de la visibility
   * @param valeur
   */
  public void setVisibility(String valeur) {
    if (valeur == null) {
      return;
    }
    if (valeur.trim().equals("")) {
      return;
    }
    if (iindicateur == null) {
      iindicateur = new iIndicator();
    }
    visibility = iindicateur.tabconvert2notpol(valeur.trim());
  }
  
  /**
   * Retourne les indicateurs de la Visibility
   * @return
   */
  public String[] getVisibility() {
    return visibility;
  }
  
  /**
   * Initialise les indicateurs de la visibility sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public void setVisibilityPol(String valeur) {
    visibility = valeur.split(" ");
  }
  
  /**
   * Retourne les indicateurs de la visibility sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public String getVisibilityPol() {
    if (visibility == null) {
      return null;
    }
    
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < visibility.length; i++) {
      sb.append(visibility[i]).append(' ');
    }
    return sb.toString().trim();
  }
  
  /**
   * Evalue les indicateurs de la visibility avec l'interpréteur du record courant
   * Par défaut on envoie ON (la variable est visible sauf si c'est explicite)
   * @return
   */
  public int getEvalVisibility() {
    if (visibility == null) {
      return Constantes.ON;
    }
    else {
      return iindicateur.evaluation(visibility);
    }
  }
  
  /**
   * Initialise les attributs et leurs types d'un objet
   */
  @Override
  public void setAttributes(HashMap<String, Object> alisteAttributes) {
    super.setAttributes(alisteAttributes);
    
    // KEY
    if (listeAttributes.containsKey("KEY")) {
      setKey((String) listeAttributes.get("KEY"));
    }
    // INDICATOR
    // if (listeAttributes.containsKey("INDICATOR"))
    // setIndicateurPol(((String)listeAttributes.get("INDICATOR")));
    // VISIBILITY - INDICATOR
    if (listeAttributes.containsKey("INDICATOR")) {
      setVisibilityPol((String) listeAttributes.get("VISIBILITY"));
    }
    // INDICATOR REF
    if (listeAttributes.containsKey("REF_INDICATOR")) {
      setRefIndicator(((String) listeAttributes.get("REF_INDICATOR")));
    }
    // MAJ DATA
    if (listeAttributes.containsKey("MAJDATA")) {
      setMajData(((Boolean) listeAttributes.get("MAJDATA")));
    }
  }
  
  /**
   * Retourne les attributs et leurs types d'un objet
   */
  @Override
  public HashMap<String, Object> getAttributes() {
    super.getAttributes();
    
    if (getKey() != null) {
      listeAttributes.put("KEY", new String(getKey()));
    }
    // if (getIndicateurPol() != null)
    // listeAttributes.put("INDICATOR", new String(getIndicateurPol()));
    if (getVisibilityPol() != null) {
      listeAttributes.put("VISIBILITY", new String(getVisibilityPol()));
    }
    if (getRefIndicator() != -1) {
      listeAttributes.put("REF_INDICATOR", new String("" + getRefIndicator()));
    }
    if (isMajData()) {
      listeAttributes.put("MAJDATA", Boolean.valueOf(isMajData()));
    }
    
    return listeAttributes;
  }
  
  /**
   * Retourne les infos pour générer un fichier PNL
   * @return
   */
  public ArrayList<String> getPNL() {
    ArrayList<String> liste = new ArrayList<String>();
    liste.add("OBJECT_KEY");
    liste.add("\tTYPEOBJ=composants.oKey");
    liste.add("\tKEY=" + getKey());
    liste.add("\tNAME=" + getNom());
    liste.add("\tPARENT=" + getParent());
    if (getIndicateurPol() != null) {
      liste.add("\tVISIBILITY=" + getVisibilityPol());
    }
    if (getRefIndicator() != -1) {
      liste.add("\tREF_INDICATOR=" + getRefIndicator());
    }
    liste.add("\tMAJDATA=" + (isMajData() ? "TRUE" : "FALSE"));
    liste.add("END;");
    
    return liste;
  }
  
  /**
   * Initialise les valeurs avec un tableau venant du PNL
   * @param valeur
   */
  @Override
  public void pnlToValeurs(LinkedHashMap<String, String> valeur) {
    String chaine = null;
    super.pnlToValeurs(valeur);
    
    // KEY
    if (valeur.containsKey("KEY")) {
      chaine = valeur.get("KEY");
      setKey(chaine);
    }
    // INDICATOR
    // if (valeur.containsKey("INDICATOR"))
    // setIndicateur((String)valeur.get("INDICATOR"));
    // VISIBILITY
    if (valeur.containsKey("VISIBILITY")) {
      setVisibility(valeur.get("VISIBILITY"));
    }
    // INDICATOR REF
    if (valeur.containsKey("REF_INDICATOR")) {
      setRefIndicator(valeur.get("REF_INDICATOR"));
    }
    // MAJDATA
    if (valeur.containsKey("MAJDATA")) {
      setMajData(valeur.get("MAJDATA").trim().equals("TRUE"));
    }
  }
  
  /**
   * Convertie l'objet oKey au format binaire
   * Retourne le nombre d'integer du tableau
   * @param tabListFic
   * @throws UnsupportedEncodingException
   */
  @Override
  public ArrayList<Integer> valeursToBinaire(ArrayList<String> tabListFic) throws UnsupportedEncodingException {
    int i = 0;
    
    super.valeursToBinaire(tabListFic);
    
    // KEY
    if (key != null) {
      addTabCompilation(ID_KEY, tabCompilation, key);
    }
    // INDICATEURS
    // if (indicateur != null)
    // {
    // addTabCompilation(ID_INDICATEUR, tabCompilation, indicateur.length);
    // for (i=0; i<indicateur.length; i++)
    // {
    // addTabCompilation(Constantes.NONE, tabCompilation, indicateur[i]);
    // }
    // }
    // VISIBILITY
    if (visibility != null) {
      addTabCompilation(ID_VISIBILITY, tabCompilation, visibility.length);
      for (i = 0; i < visibility.length; i++) {
        addTabCompilation(Constantes.NONE, tabCompilation, visibility[i]);
      }
    }
    // INDICATOR REF
    if (refindicator != -1) {
      addTabCompilation(ID_REFINDICATEUR, tabCompilation, refindicator);
    }
    // MAJDATA
    addTabCompilation(ID_MAJDATA, tabCompilation, majData);
    
    insertTailleObjet();
    return tabCompilation;
  }
  
  /**
   * Initialise les variable de l'objet oKey à partir d'un tableau binaire
   * Retourne si erreur
   * @param tabInt
   * @param ListFic
   * @return
   */
  @Override
  public int binaireToValeurs(int tabInt[], String ListFic[]) {
    return binaireToValeurs(tabInt, 0, null);
  }
  
  @Override
  public int binaireToValeurs(int tabInt[], int offst, String ListFic[]) {
    int i = 0;
    int j = 0;
    int k = 0;
    int l = 0;
    int m = 0;
    int lg = 0;
    // byte tabByte[]=null;
    byte tabByte[] = new byte[Constantes.DATAQLONG];
    String tabchaine[] = null;
    int lngdescdata = 0;
    
    // Vérification du tableau
    if (tabInt == null) {
      setMsgErreur(ERREUR_DATA_NULL);
      return Constantes.ERREUR;
    }
    
    i = offst;
    lngdescdata = tabInt[i++];
    if (lngdescdata > tabInt.length) {
      setMsgErreur(ERREUR_LONGUEUR_KEY_ERRONNE);
      return Constantes.ERREUR;
    }
    
    lngdescdata = lngdescdata + i;
    while (i < lngdescdata) {
      switch (tabInt[i]) {
        case ID_KEY:
          // longueur de la touche
          lg = tabInt[++i];
          // tabByte = new byte[k];
          // récupération du nom
          i++;
          k = lg + i;
          for (j = 0; i < k; i++) {
            tabByte[j++] = (byte) tabInt[i];
          }
          // récupération du nom
          setKey(new String(tabByte, 0, lg));
          break;
        case ID_VISIBILITY:
          i += 2;
          m = tabInt[i];
          tabchaine = new String[m];
          for (j = 0; j < m; j++) {
            // longueur de l'indicateur
            lg = tabInt[++i];
            // tabByte = new byte[k];
            // récupération du nom
            i++;
            k = lg + i;
            for (l = 0; i < k; i++) {
              tabByte[l++] = (byte) tabInt[i];
            }
            i--;
            tabchaine[j] = new String(tabByte, 0, lg);
          }
          // récupération du nom
          setIndicateur(tabchaine);
          i++;
          break;
        
        /*
        case ID_INDICATEUR :
        i+=2;
        m=tabInt[i];
        tabchaine = new String[m];
        for (j=0; j<m; j++)
        {
          // longueur de l'indicateur
          lg = tabInt[++i];
          //tabByte = new byte[k];
          // récupération du nom
          i++;
          k = lg + i;
          for (l=0; i<k; i++)
            tabByte[l++] = (byte)tabInt[i];
          i--;
          tabchaine[j] = new String(tabByte, 0, lg);
        }
        // récupération du nom
        setIndicateur(tabchaine);
        i++;
        break;*/
        case ID_REFINDICATEUR:
          i += 2;
          setRefIndicator(tabInt[i]);
          i++;
          break;
        case ID_MAJDATA:
          i += 2;
          setMajData(tabInt[i] == Constantes.TRUE ? true : false);
          i++;
          break;
        
        // Valeur non traitée <-----------------------------------------------------
        default:
          i = super.binaireToValeurs(tabInt, i, null);
      }
    }
    tabByte = null;
    tabInt = null;
    
    return i;
  }
  
}
