/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session;

import java.io.File;

import ri.serien.libcommun.exploitation.environnement.FichierRuntime;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.collection.ArrayListManager;
import ri.serien.libcommun.outils.fichier.GestionFichierByte;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceSession;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;

/**
 * Gère les transferts de fichiers en parallèle entre un client et le serveur.
 */
public class SessionTransfert {
  // Variables
  private IdSession idSession = null;
  private IdSousEnvironnement idSousEnvironnement = null;
  
  private SousEnvironnement sousEnvironnementClient = null;
  private SousEnvironnement sousEnvironnementServeur = null;
  private int isConnect = Constantes.FALSE;
  
  // Liste des évènements concernant le système package protocoleMsg
  private ArrayListManager evenementSysteme = new ArrayListManager();
  
  /**
   * Constructeur.
   */
  public SessionTransfert() {
    idSousEnvironnement = ManagerSessionClient.getInstance().getEnvUser().getIdSousEnvironnement();
    idSession = ManagerSessionClient.getInstance().creerSessionClient();
    ManagerServiceSession.ouvrirSession(idSession, EnumTypeSession.SESSION_TRANSFERT, null);
  }
  
  /**
   * Retourne l'id de la session.
   */
  public IdSession getIdSession() {
    return idSession;
  }
  
  /**
   * Initialisation des listes de RT Serveur & Client.
   */
  public void setSousEnvironnement(SousEnvironnement pSousEnvironnementServeur, SousEnvironnement pSousSystemeClient) {
    sousEnvironnementServeur = pSousEnvironnementServeur;
    sousEnvironnementClient = pSousSystemeClient;
  }
  
  /**
   * Télécharger un fichier runtime.
   */
  public boolean telechargerFichierRuntime(String pNomFichier) {
    if (pNomFichier == null || pNomFichier.indexOf('@') != -1) {
      return false;
    }
    String nomFichier = pNomFichier.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR);
    
    // Tester d'abord si le fichier est présent sur le serveur
    // Si ce n'est pas le cas, on retourne false car il ne sera pas possible de le télécharger
    long versionServeur = sousEnvironnementServeur.getVersionFichierRuntime(nomFichier);
    if (versionServeur == Constantes.FALSE) {
      return false;
    }
    
    // Comparer les versions de fichiers entre le serveur et le client
    // Si elles sont identiques, on retourne true car le fichier est déjà téléchargé sur le client
    long versionClient = sousEnvironnementClient.getVersionFichierRuntime(nomFichier);
    if (versionClient == versionServeur) {
      return true;
    }
    
    // Tracer
    Trace.info("Télécharger un fichier runtime : nomFichier= " + nomFichier + " versionClient=" + versionClient + " versionServeur="
        + versionServeur);
    
    // Télécharger le fichier runtime
    FichierRuntime fichierRuntime = ManagerServiceTechnique.telechargerFichierRuntime(idSession, nomFichier);
    if (fichierRuntime.getMsgErreur().length() > 0) {
      Trace.erreur("Impossible de télécharger le fichier runtime : " + fichierRuntime.getNomFichier());
      return false;
    }
    
    // Mettre à jour le fichier runtime
    sousEnvironnementClient.modifierFichierRuntime(fichierRuntime);
    return true;
  }
  
  /**
   * Télécharge un fichier donné vers le dossier temporaire sur le poste de l'utilisateur.
   * ATTENTION à la casse suivant les OS (à corriger +tard).
   */
  public void fichierArecup(String fichier) {
    // On effectue une vérification du fichier à récupérer
    if (fichier == null || fichier.indexOf('@') != -1) {
      return;
    }
    
    // On formate correctement le fichier à télécharger
    String fichierNormalise = fichier.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR);
    // On vérifie qu'il ne soit pas déjà dans la liste
    demarreTelechargement(fichierNormalise);
  }
  
  /**
   * Télécharge un fichier de l'IFS vers un dossier choisi sur le poste de l'utilisateur.
   */
  public void telechargerFichier(String pFichierSourceIFS, File pFichierDestination) {
    // On effectue une vérification du fichier à récupérer
    pFichierSourceIFS = Constantes.normerTexte(pFichierSourceIFS);
    if (pFichierSourceIFS.isEmpty()) {
      throw new MessageErreurException("Le chemin distant du fichier à télécharger sur le poste de travail est invalide.");
    }
    if (pFichierDestination == null) {
      throw new MessageErreurException("Le chemin local du fichier à télécharger sur le poste de travail est invalide.");
    }
    
    // Téléchargement du fichier dans le dossier local souhaité
    Trace.info("Début du téléchargement du fichier " + pFichierSourceIFS + " vers " + pFichierDestination.getAbsolutePath());
    GestionFichierByte gestionFichierByte = ManagerServiceTechnique.telechargerFichier(idSession, pFichierSourceIFS);
    gestionFichierByte.changeNomFichier(pFichierDestination.getAbsolutePath());
    if (gestionFichierByte.ecritureFichier() == Constantes.OK) {
      gestionFichierByte.videContenuFichier();
      Trace.info("Téléchargement terminé du fichier " + pFichierSourceIFS + " vers " + pFichierDestination.getAbsolutePath());
    }
    else {
      Trace.erreur("Téléchargement en erreur du fichier " + pFichierSourceIFS + " vers " + pFichierDestination.getAbsolutePath());
    }
  }
  
  /**
   * Ajoute un fichier à uploader dans la liste.
   * ATTENTION à la casse suivant les OS (à corriger +tard).
   */
  public void fichierAUploader(File pFichierSource, String pFichierDestination) {
    // On effectue une vérification du fichier à uploader
    if (pFichierSource == null) {
      return;
    }
    String sfichier = pFichierSource.getAbsolutePath();
    if (sfichier.indexOf('@') != -1) {
      return;
    }
    
    // On construit correctement le fichier à uploader
    String fichierServeur =
        pFichierDestination.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR)
            + pFichierSource.getName();
    
    // Récupération du nom du fichier et de sa destination
    Trace.info("Début du téléversement du fichier " + pFichierSource.getAbsolutePath() + " vers " + fichierServeur);
    GestionFichierByte gfb = new GestionFichierByte(pFichierSource);
    gfb.lectureFichier();
    ManagerServiceTechnique.televerserFichier(idSession, fichierServeur, gfb);
    Trace.info("Téléversement terminé du fichier " + pFichierSource.getAbsolutePath());
  }
  
  /**
   * Vérifie si le fichier a bien été uploadé.
   */
  public boolean fichierUploade(File fichier, String chemindest) {
    if (fichier == null) {
      return false;
    }
    
    chemindest.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR);
    fichier.getName();
    
    return true;
  }
  
  /**
   * Ajoute une liste de fichiers à supprimer dans la liste.
   * ATTENTION à la casse suivant les OS (à corriger +tard).
   */
  public boolean fichierASupprimer(String lstfichier) {
    // On effectue une vérification de la lste des fichiers à supprimer
    if (lstfichier == null) {
      return false;
    }
    if (lstfichier.indexOf('@') != -1) {
      return false;
    }
    
    // On construit correctement le fichier à supprimer
    lstfichier = lstfichier.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR).replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR);
    Trace.info("Supprimer le fichier " + lstfichier);
    return ManagerServiceTechnique.supprimerFichier(idSession, lstfichier);
  }
  
  /**
   * Vérifie si on peut démarrer un téléchargement de fichier.
   */
  private boolean demarreTelechargement(String chaine) {
    try {
      if (chaine == null) {
        return false;
      }
      Trace.info("Demande la récupération de " + chaine);
      
      FichierRuntime fichierRuntime = ManagerServiceTechnique.demanderFichier(idSession, chaine);
      // S'il y a eu une erreur lors de la récupération du fichier
      if (fichierRuntime.getMsgErreur().length() > 0) {
        Trace.erreur("Fichier non récupéré : " + chaine);
        // On le supprime de la liste
        return false;
      }
      else {
        // Sinon récupération du fichier
        // Décomposition des infos
        String nomDossierTempoUtilisateur =
            ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
        String dossier = nomDossierTempoUtilisateur + File.separatorChar
            + fichierRuntime.getNomFichierRuntime().replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_');
        fichierRuntime.changeNomFichier(dossier);
        fichierRuntime.ecritureFichier();
        Trace.info("Fichier récupéré : " + dossier);
      }
    }
    catch (Exception e) {
      // @todo Gérer correctement l'exception.
    }
    return true;
  }
  
  /**
   * Effectuer une réquête système.
   */
  public String effectuerRequeteSysteme(String chaine) {
    String data = null;
    try {
      data = ManagerServiceTechnique.effectuerRequeteSysteme(idSession, chaine);
    }
    catch (Exception e) {
      // @todo Gérer correctmeent l'exception.
    }
    return data;
  }
  
  /**
   * Fermeture de la session.
   */
  public void fermeture() {
    isConnect = Constantes.FALSE;
    ManagerSessionClient.getInstance().detruireSessionClient(idSession);
  }
  
  /**
   * Indique si la session est connectée ou pas.
   */
  public int isConnected() {
    return isConnect;
  }
  
  public ArrayListManager getEvenementSysteme() {
    return evenementSysteme;
  }
  
  /**
   * Permet de lancer le balayage des runtimes sur le poste de travail de l'utilisateur.
   * C'est principalement utilisé lorsque l'on supprime des icones par le menu car il faut
   * rafraichir la liste pour une gestion correcte de l'affichage des icones.
   */
  public void balayerDossierRuntime() {
    if (sousEnvironnementClient == null) {
      return;
    }
    sousEnvironnementClient.balayerDossierRuntime();
  }
  
  /**
   * Lister le contenu d'un dossier local ou sur le serveur.
   * @param pCheminDossier Chemin du sossier dont il faut lister les fichiers (fichier local ou distant).
   * @return Liste des fichiers du dossier (le premier élément du tableau contient le nom du dossier listé).
   */
  public String[] listerDossier(String pCheminDossier) {
    // Vérifier les paramètres
    if (pCheminDossier == null) {
      throw new MessageErreurException("Impossible de lister les fichiers d'un dossier invalide.");
    }
    if (pCheminDossier.indexOf('@') != -1) {
      throw new MessageErreurException("Le chemin du dossier dont il faut lister les fichiers comporte le caractère invalide '@'.");
    }
    
    // Tracer l'opération
    Trace.info("Lister le contenu du dossier : " + pCheminDossier);
    
    // Tranformer le chemin si besoin
    pCheminDossier = pCheminDossier.trim();
    pCheminDossier = pCheminDossier.replace('/', Constantes.SEPARATEUR_DOSSIER_CHAR);
    pCheminDossier = pCheminDossier.replace('\\', Constantes.SEPARATEUR_DOSSIER_CHAR);
    if (!pCheminDossier.endsWith(Constantes.SEPARATEUR_DOSSIER)) {
      pCheminDossier += Constantes.SEPARATEUR_DOSSIER_CHAR;
    }
    
    // Tester si le chemin est un chemin local ou réseau
    String[] listeFichier = null;
    if (pCheminDossier.indexOf(':') != -1 || pCheminDossier.startsWith("&&")) {
      // C'est un chemin Windows
      // Remplacer par les séparateurs Windows
      pCheminDossier = pCheminDossier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '\\');
      
      // Récupérer l'objet File du dossier
      File fileDossier = new File(pCheminDossier);
      
      // Vérifier que le dossier existe et que ce n'est pas un fichier
      if (!fileDossier.exists() || fileDossier.isFile()) {
        return null;
      }
      
      // Lister les fichiers du dossier
      File[] listeFichierTemp = fileDossier.listFiles();
      listeFichier = new String[listeFichierTemp.length + 1];
      int i = 1;
      listeFichier[0] = pCheminDossier;
      for (File f : listeFichierTemp) {
        listeFichier[i++] = f.getName();
      }
    }
    else {
      // C'est un chemin IFS
      // Récupérer la liste des fichiers du dossier
      String contenuDossier = ManagerServiceTechnique.demanderContenuDossier(idSession, pCheminDossier);
      if (contenuDossier == null) {
        throw new MessageErreurException("Erreur lors de la récupération de la liste des fichiers du dossier : " + pCheminDossier);
      }
      
      // Convertir la liste, exprimée sous forme d'une chaîne unique avec séparateurs, sous forme de tableau de chaîne de caractères
      listeFichier = Constantes.splitString(contenuDossier, Constantes.SEPARATEUR_CHAINE_CHAR);
    }
    
    return listeFichier;
  }
  
}
