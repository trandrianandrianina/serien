/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.pourcentage;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Représente un taux ou, par abus de language, une valeur en pourcentage.
 * 
 * Un taux peux s’exprimer en notation décimale (0,2000), en notation fractionnaire (20/100), en pourcentage (20,00%) ou sous forme de
 * coefficient multiplicateur (1,2000).
 * 
 * La méthode permettant de passer de la notation décimale à la notation en pourcentage est simple :
 * 1) Multiplier le nombre décimal par 100.
 * 2) Ajouter le symbole % à droite du résultat.
 * 0,2000 => 20,00%
 * 
 * La méthode permettant de passer de la notation en pourcentage à sa représentation en notation décimale est :
 * 1) Enlever le symbole de pourcentage (%).
 * 2) Diviser le nombre par 100.
 * 20,00% => 0,2000
 * 
 * Dans la base de données, les taux sont représentés sous deux formes, la notation décimale et la notation en pourcentage. Le but de
 * cette classe est d'homogénéiser la persistance et la manipulation des taux dans le code Java. Dans cette classe, un taux est stocké
 * en notation décimale avec 4 chiffres après la virgule, soit 2 chiffres après la virgule avec la notation en pourcentage.
 * 
 * Lorsqu'on utilise cette classe, on doit indiquer si le taux fourni est en notation décimale (EnumFormatPourcentage.DECIMAL) ou
 * en notation en pourcentage (EnumFormatPourcentage.POURCENTAGE).
 */
public class BigPercentage implements Serializable, Comparable<BigPercentage> {
  // Constantes
  private static final int NOMBRE_DECIMALE_SOUS_FORME_DECIMALE = 4;
  public static final BigPercentage ZERO = new BigPercentage(BigDecimal.ZERO, EnumFormatPourcentage.DECIMAL);
  public static final BigPercentage CENT = new BigPercentage(BigDecimal.ONE, EnumFormatPourcentage.DECIMAL);
  
  // Variable
  private final BigDecimal taux;
  private final EnumFormatPourcentage formatPourcentageOriginal;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur à partir d'un BigDecimal.
   * 
   * @param pTaux Valeur du taux.
   * @param pFormatPourcentage
   *          EnumFormatPourcentage.DECIMAL = notation décimale (0.XXXX),
   *          EnumFormatPourcentage.POURCENTAGE = notation en pourcentage (XX.XX).
   */
  public BigPercentage(BigDecimal pTaux, EnumFormatPourcentage pFormatPourcentage) {
    // Mettre la valeur à zéro en cas de null
    if (pTaux == null) {
      pTaux = BigDecimal.ZERO;
    }
    
    // Stocker le format du taux d'origine
    formatPourcentageOriginal = pFormatPourcentage;
    
    // Tester le format du taux
    if (formatPourcentageOriginal == EnumFormatPourcentage.DECIMAL) {
      //
      taux = pTaux;
    }
    else {
      // Convertir le taux en notation décimale (divisé par 100)
      taux = pTaux.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32);
    }
    
    // Arrondir à 4 décimales
    taux.setScale(NOMBRE_DECIMALE_SOUS_FORME_DECIMALE, RoundingMode.HALF_UP);
  }
  
  /**
   * Constructeur à partir d'un Integer.
   * 
   * @param pTaux Valeur du taux.
   * @param pFormatPourcentage
   *          EnumFormatPourcentage.DECIMAL = notation décimale (0.XXXX),
   *          EnumFormatPourcentage.POURCENTAGE = notation en pourcentage (XX.XX).
   */
  public BigPercentage(Integer pTaux, EnumFormatPourcentage pFormatPourcentage) {
    this(new BigDecimal(pTaux.doubleValue()), pFormatPourcentage);
  }
  
  /**
   * Constructeur à partir d'une chaîne de caractères.
   * 
   * @param pTaux Valeur du taux sous forme textuelle.
   * @param pFormatPourcentage
   *          EnumFormatPourcentage.DECIMAL = notation décimale (0.XXXX),
   *          EnumFormatPourcentage.POURCENTAGE = notation en pourcentage (XX.XX).
   */
  public BigPercentage(String pTaux, EnumFormatPourcentage pFormatPourcentage) {
    this(Constantes.convertirTexteEnBigDecimal(pTaux), pFormatPourcentage);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + taux.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof BigPercentage)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux BigPercentage.");
    }
    BigPercentage bigPercentage = (BigPercentage) pObject;
    return Constantes.equals(taux, bigPercentage.taux);
  }
  
  @Override
  public int compareTo(BigPercentage pObject) {
    return taux.compareTo(pObject.taux);
  }
  
  /**
   * Formater le taux avec la notation en pourcentage (nombre sur 2 décimales avec le symbole % à la fin).
   * @return Chaînes de caractères représentant le taux ("58,31%" pour 58,31% par exemple).
   */
  @Override
  public String toString() {
    return Constantes.formater(getTauxEnPourcentage(), true) + "%";
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Convertir le taux en chaîne de caractères avec la notation en pourcentage sans le symbole pourcentage à la fin.
   * @return Taux sous forme de chaînes de caractères ("58,31" pour 58,31% par exemple).
   */
  public String getTexteSansSymbole() {
    return Constantes.formater(getTauxEnPourcentage(), true);
  }
  
  /**
   * Retourner le taux avec la notation en pourcentage (multiplié par cent).
   * @return Taux avec la notation en pourcentage (58,31 pour 58,31% par exemple).
   */
  public BigDecimal getTauxEnPourcentage() {
    return taux.multiply(Constantes.VALEUR_CENT).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Retourner le taux avec la notation décimale (divisé par cent).
   * @return Taux avec la notation décimale (0,5831 pour 58,31% par exemple).
   */
  public BigDecimal getTauxEnDecimal() {
    return taux;
  }
  
  /**
   * Retourner le taux avec la notation d'origine (divisé par cent ou non).
   * @return Taux avec la notation originale : en décimale ou en pourcentage.
   */
  public BigDecimal getTauxAuFormatOriginal() {
    if (formatPourcentageOriginal == EnumFormatPourcentage.DECIMAL) {
      return taux;
    }
    else {
      return getTauxEnPourcentage();
    }
  }
  
  /**
   * Retourner le taux sous forme de coefficent multiplicateur.
   * @return Taux sous forme de coefficent multiplicateur (1,5831 pour 58,31% par exemple).
   */
  public BigDecimal getCoefficient() {
    return BigDecimal.ONE.add(taux);
  }
  
  /**
   * Indiquer si le taux est égal à 0%.
   * @return true=taux égal à 0%, false=taux différent de 0%.
   */
  public boolean isZero() {
    return taux == null || taux.compareTo(BigDecimal.ZERO) == 0;
  }
  
  /**
   * Indiquer si le taux est égal à 100%.
   * @return true=taux égal à 100%, false=taux différent de 100%.
   */
  public boolean isCent() {
    return taux == null || taux.compareTo(BigDecimal.ONE) == 0;
  }
  
  /**
   * Indiquer si le taux est supérieur ou égal à zéro.
   * La valeur null est considérée comme équivalente à zéro donc positive.
   * @return true=taux supérieur ou égal à 0, false=taux strictement inférieur à zéro.
   */
  public boolean isPositif() {
    return taux == null || taux.compareTo(BigDecimal.ZERO) >= 0;
  }
}
