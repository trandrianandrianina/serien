/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import ri.serien.libcommun.outils.Constantes;

/**
 * Gestion des fichiers texte.
 */
public class GestionFichierTexte {
  // Constantes erreurs de chargement
  private static final String ERREUR_FICHIER_INTROUVABLE = "Le fichier est introuvable.";
  private static final String ERREUR_LECTURE_FICHIER = "Erreur lors de la lecture du fichier.";
  private static final String ERREUR_ENREGISTREMENT_FICHIER = "Erreur lors de l'enregistrement du fichier.";
  private static final String ERREUR_BUFFER_VIDE = "Le buffer est vide.";
  public static final String ENCODAGE_UTF8 = "UTF-8";
  public static final String ENCODAGE_8859_15 = "ISO-8859-15";
  public static final String ENCODAGE_WINDOWS_1252 = "Windows-1252";
  
  // Constantes
  public static final String FILTER_EXT_TXT = "txt";
  public static final String FILTER_DESC_TXT = "Fichier texte (*.txt)";
  
  // Variables
  private String crlf = Constantes.crlf;
  private String nomFichier = null;
  private URL urlFichier = null;
  protected ArrayList<String> contenuFichier = null;
  protected File fichier = null;
  private String forceEncodage = null;
  
  protected String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur de la classe.
   */
  public GestionFichierTexte() {
  }
  
  /**
   * Constructeur de la classe.
   */
  public GestionFichierTexte(String pNomFichier) {
    setNomFichier(pNomFichier);
  }
  
  /**
   * Constructeur de la classe.
   */
  public GestionFichierTexte(File pFichier) {
    setNomFichier(pFichier);
  }
  
  /**
   * Constructeur de la classe.
   */
  public GestionFichierTexte(URL pUrlFichier) {
    nomFichier = null;
    fichier = null;
    this.urlFichier = pUrlFichier;
  }
  
  /**
   * Initialise le nom du fichier.
   */
  public void setNomFichier(String pNomFichier) {
    setNomFichier(pNomFichier, true);
  }
  
  /**
   * Initialise le nom du fichier.
   */
  public void setNomFichier(File pFichier) {
    setNomFichier(pFichier, true);
  }
  
  /**
   * Initialise le nom du fichier.
   */
  public void setNomFichier(String pNomFichier, boolean pViderBuffer) {
    setNomFichier(new File(pNomFichier), pViderBuffer);
  }
  
  /**
   * Initialise le nom du fichier.
   */
  public void setNomFichier(File pFichier, boolean pVideBuffer) {
    if (pFichier == null) {
      fichier = null;
      nomFichier = null;
      return;
    }
    nomFichier = pFichier.getAbsolutePath();
    fichier = pFichier;
    if (pVideBuffer) {
      videContenuFichier();
    }
  }
  
  /**
   * Retourne le nom du fichier.
   */
  public String getNomFichier() {
    return nomFichier;
  }
  
  /**
   * Retourne l'objet File.
   */
  public File getFichier() {
    return fichier;
  }
  
  /**
   * Vérifie l'existence d'un fichier.
   */
  public boolean isPresent() {
    if (fichier == null) {
      return false;
    }
    return fichier.exists();
  }
  
  /**
   * Vide le contenu du buffer.
   */
  public void videContenuFichier() {
    // if( this.contenuFichier != null ) this.contenuFichier.clear();
    this.contenuFichier = null;
  }
  
  /**
   * Initialise le buffer.
   */
  public void setContenuFichier(ArrayList<String> contenuFichier) {
    this.contenuFichier = contenuFichier;
  }
  
  /**
   * Initialise le buffer.
   */
  public void setContenuFichier(String[] contenuFichier) {
    this.contenuFichier = new ArrayList<String>(contenuFichier.length);
    for (int i = 0; i < contenuFichier.length; i++) {
      this.contenuFichier.add(contenuFichier[i]);
    }
  }
  
  /**
   * Initialise le buffer.
   */
  public void setContenuFichier(String contenuFichier) {
    if (contenuFichier == null) {
      return;
    }
    String[] liste = contenuFichier.split("\n");
    setContenuFichier(liste);
  }
  
  /**
   * Retourne le buffer.
   */
  public ArrayList<String> getContenuFichier() {
    if (contenuFichier == null) {
      if (lectureFichier() == Constantes.ERREUR) {
        return null;
      }
    }
    
    return this.contenuFichier;
  }
  
  /**
   * Retourne le buffer.
   */
  public String[] getContenuFichierTab() {
    if (contenuFichier == null) {
      if (lectureFichier() == Constantes.ERREUR) {
        return null;
      }
    }
    
    String[] tab = new String[contenuFichier.size()];
    return contenuFichier.toArray(tab);
  }
  
  /**
   * Retourne le buffer.
   */
  public String getContenuFichierString(boolean crlf) {
    if (contenuFichier == null) {
      if (lectureFichier() == Constantes.ERREUR) {
        return null;
      }
    }
    
    // On concatène les lignes
    StringBuilder sb = new StringBuilder(contenuFichier.size() * 512);
    if (crlf) {
      for (String chaine : contenuFichier) {
        sb.append(chaine).append('\n');
      }
    }
    else {
      for (String chaine : contenuFichier) {
        sb.append(chaine);
      }
    }
    return sb.toString();
  }
  
  /**
   * Lecture du fichier texte.
   */
  public int lectureFichier() {
    String chaine = null;
    
    // On vérifie que le fichier existe
    if ((urlFichier == null) && !isPresent()) {
      msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + ERREUR_FICHIER_INTROUVABLE;
      return Constantes.FALSE;
    }
    
    // On lit le fichier
    if (contenuFichier == null) {
      contenuFichier = new ArrayList<String>();
    }
    try {
      BufferedReader f = null;
      if (fichier != null) {
        // Si on ne précise pas l'encodage du fichier à lire ce sera celui du système d'exploitation
        if (forceEncodage == null) {
          f = new BufferedReader(new FileReader(fichier));
        }
        // Dans le cas où l'encodage du fichier est différent de celui du système d'exploitation
        else {
          f = new BufferedReader(new InputStreamReader(new FileInputStream(fichier), forceEncodage));
        }
      }
      else {
        f = new BufferedReader(new InputStreamReader(urlFichier.openStream()));
      }
      
      // Lecture du fichier
      chaine = f.readLine();
      while (chaine != null) {
        contenuFichier.add(chaine);
        chaine = f.readLine();
        // nombreLignes++;
      }
      f.close();
    }
    catch (Exception e) {
      msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + e;
      return Constantes.FALSE;
    }
    
    return Constantes.OK;
  }
  
  /**
   * Retourne le nombre de lignes contenues par le fichier.
   */
  public int getNombreLignes() {
    if (contenuFichier == null) {
      return 0;
    }
    return contenuFichier.size();
  }
  
  /**
   * Détermine le retour chariot.
   */
  public void setRetourChariot(String acrlf) {
    crlf = acrlf;
  }
  
  /**
   * Ecrit le fichier texte.
   */
  public int ecritureFichier() {
    int i = 0;
    
    if (contenuFichier == null) {
      msgErreur = ERREUR_BUFFER_VIDE;
      return Constantes.ERREUR;
    }
    
    // Vérification de l'existence du dossier
    if (fichier != null && !fichier.getParentFile().exists()) {
      fichier.getParentFile().mkdirs();
    }
    
    // Si on ne précise pas l'encodage du fichier à lire ce sera celui du système d'exploitation
    if (forceEncodage == null) {
      
      FileWriter f;
      try {
        f = new FileWriter(nomFichier);
        for (i = 0; i < contenuFichier.size(); i++) {
          f.write(contenuFichier.get(i) + crlf);
        }
        f.flush();
        f.close();
      }
      catch (IOException e) {
        msgErreur = ERREUR_ENREGISTREMENT_FICHIER + Constantes.crlf + e;
        return Constantes.ERREUR;
      }
    }
    
    // Dans le cas où l'encodage du fichier est différent de celui du système d'exploitation
    else {
      Writer f;
      try {
        f = new OutputStreamWriter(new FileOutputStream(nomFichier), forceEncodage);
        for (i = 0; i < contenuFichier.size(); i++) {
          f.write(contenuFichier.get(i) + crlf);
        }
        f.flush();
        f.close();
      }
      catch (IOException e) {
        msgErreur = ERREUR_ENREGISTREMENT_FICHIER + Constantes.crlf + e;
        return Constantes.ERREUR;
      }
    }
    return Constantes.OK;
    
  }
  
  /**
   * Retourne les filtres possible pour les boites de dialogue.
   */
  public static void initFiltre(JFileChooser pJFileChooser) {
    FileFilter fileFilterTxt = new FiltreFichierParExtension(new String[] { FILTER_EXT_TXT }, FILTER_DESC_TXT);
    pJFileChooser.addChoosableFileFilter(fileFilterTxt);
    
    // Définit ce filtre comme le filtre par défaut
    pJFileChooser.setFileFilter(fileFilterTxt);
  }
  
  /**
   * Contrôle la présence de l'extension au nom du fichier et l'ajoute si nécessaire.
   * @param pJFileChooser
   */
  public static String controlerExtensionFichier(JFileChooser pJFileChooser) {
    if (pJFileChooser == null) {
      return null;
    }
    
    String extension = FILTER_EXT_TXT;
    if (pJFileChooser.getFileFilter() instanceof FiltreFichierParExtension) {
      extension = ((FiltreFichierParExtension) pJFileChooser.getFileFilter()).getFiltreExtension();
    }
    String fichierComplet = pJFileChooser.getSelectedFile().getAbsolutePath();
    
    // Si l'extension n'est pas l'extension par défaut
    if (!extension.equals(FILTER_EXT_TXT)) {
      // Remplacement de l'extension par défaut par la nouvelle
      fichierComplet = fichierComplet.replace("." + FILTER_EXT_TXT, "." + extension);
    }
    // Contrôle de présence de l'extension
    if (!fichierComplet.toLowerCase().endsWith(extension)) {
      if (fichierComplet.endsWith(".")) {
        fichierComplet += extension;
      }
      else {
        fichierComplet += '.' + extension;
      }
    }
    return fichierComplet;
  }
  
  public void initUnicode() {
    forceEncodage = ENCODAGE_UTF8;
  }
  
  public String getForceEncodage() {
    return forceEncodage;
  }
  
  public void setForceEncodage(String forceEncodage) {
    this.forceEncodage = forceEncodage;
  }
  
  /**
   * Libère la mémoire.
   */
  public void dispose() {
    if (contenuFichier != null) {
      contenuFichier.clear();
    }
    contenuFichier = null;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
