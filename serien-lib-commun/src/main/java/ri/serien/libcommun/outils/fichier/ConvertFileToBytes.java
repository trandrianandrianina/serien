/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.encodage.Base64;

public class ConvertFileToBytes {
  public ConvertFileToBytes() {
  }
  
  /**
   * Convertir un fichier au format base64.
   */
  public String encodeFileToBase64Binary(String fileName) {
    File file = new File(fileName);
    byte[] bytes = loadFile(file);
    byte[] encoded = Base64.encodeToByte(bytes, false);
    String encodedString = new String(encoded);
    
    return encodedString;
  }
  
  private byte[] loadFile(File file) {
    byte[] bytes = null;
    InputStream is = null;
    try {
      is = new FileInputStream(file);
      long length = file.length();
      bytes = new byte[(int) length];
      
      int offset = 0;
      int numRead = 0;
      while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
        offset += numRead;
      }
      
      if (offset < bytes.length) {
        throw new IOException("Impossible de lire le fichier complètement " + file.getName());
      }
    }
    catch (FileNotFoundException e) {
      Trace.erreur(e, "Erreur lors de l'ouverture du fichier.");
    }
    catch (IOException e) {
      Trace.erreur(e, "Erreur lors de la lecture du fichier.");
    }
    finally {
      if (is != null) {
        try {
          is.close();
        }
        catch (IOException e) {
          Trace.erreur(e, "Erreur lors de la fermeture du fichier.");
        }
      }
    }
    
    return bytes;
  }
}
