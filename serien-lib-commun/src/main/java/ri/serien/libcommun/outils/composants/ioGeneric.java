/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.composants;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Description des méthodes les plus génériques de SGM
 */
public interface ioGeneric {
  /**
   * Initialise le composant maitre
   * @param aroot
   */
  public void setRoot(ioGeneric aroot);
  
  /**
   * Retourne le nom de l'objet
   * @return
   */
  public String getName();
  
  /**
   * Initialise les attributs et leurs types d'un objet
   */
  // public void setAttributes(HashMap<String, Object> alisteAttributes);
  
  /**
   * Retourne les attributs et leurs types d'un objet
   */
  // protected HashMap<String, Object> getAttributes();
  
  /**
   * Initialise les valeurs avec un tableau venant du PNL
   * @param valeur
   */
  public void pnlToValeurs(LinkedHashMap<String, String> valeur);
  
  /**
   * Convertie l'objet au format binaire
   * @param tabListFic
   * @return
   * @throws UnsupportedEncodingException
   */
  public ArrayList<Integer> valeursToBinaire(ArrayList<String> tabListFic) throws UnsupportedEncodingException;
  
  /**
   * Initialise les variable de l'objet à partir d'un tableau binaire
   * @param tabInt
   * @param ListFic
   * @return
   */
  
  public int binaireToValeurs(int tabInt[], String ListFic[]);
  
  /**
   * Initialise les variable de l'objet à partir d'un tableau binaire
   * @param tabInt
   * @param offst
   * @param ListFic
   * @return
   */
  public int binaireToValeurs(int tabInt[], int offst, String ListFic[]);
}
