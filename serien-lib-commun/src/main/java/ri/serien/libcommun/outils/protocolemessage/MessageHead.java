/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.protocolemessage;

/**
 * Description de l'entête du message.
 */
public class MessageHead {
  // Constantes
  // Le message ne contient pas aucun message
  public static final byte BODY_NO = 0;
  // Le message contient un message sous la forme d'une chaine simple
  public static final byte BODY_SIMPLESTRING = 1;
  // Le message contient un message sous la forme d'une chaine qui contient des séparateurs du type | ou du JSON mais
  // sans classe associée
  public static final byte BODY_COMPLEXSTRING = 2;
  // Le message contient un message sous la forme de JSON dont le nom de la classe est dans bodydesc
  public static final byte BODY_JSON = 3;
  // Version courante du protocole
  public static final int VERSION = 1;
  
  // Variables
  private int id;
  // Hascode du demandeur afin de faciliter l'identification pour le traitement au retour
  private int hashcodeCaller = 0;
  private int version = VERSION;
  // True ou false en fonction si l'on souhate une compression ou pas
  private boolean bcomp = false;
  private byte bcode;
  // La longueur de la chaine ou du body original
  private int blength;
  // Contient la chaine de caractères ou le nom de la classe contenu dans body
  private String bdesc;
  
  /**
   * @return le id.
   */
  public int getId() {
    return id;
  }
  
  /**
   * @param id le id à définir.
   */
  public void setId(int id) {
    this.id = id;
  }
  
  /**
   * @return le hashcodeCaller.
   */
  public int getHashcodeCaller() {
    return hashcodeCaller;
  }
  
  /**
   * @param hashcodeCaller le hashcodeCaller à définir.
   */
  public void setHashcodeCaller(int hashcodeCaller) {
    this.hashcodeCaller = hashcodeCaller;
  }
  
  /**
   * @return le version.
   */
  public int getVersion() {
    return version;
  }
  
  /**
   * @param version le version à définir.
   */
  public void setVersion(int version) {
    this.version = version;
  }
  
  /**
   * @return le bodycomp.
   */
  public boolean isBodycomp() {
    return bcomp;
  }
  
  /**
   * @param bodycomp le bodycomp à définir.
   */
  public void setBodycomp(boolean bodycomp) {
    this.bcomp = bodycomp;
  }
  
  /**
   * @return le bodycode.
   */
  public byte getBodycode() {
    return bcode;
  }
  
  /**
   * @param bodycode le bodycode à définir.
   */
  public void setBodycode(byte bodycode) {
    this.bcode = bodycode;
  }
  
  /**
   * @return le bodylength.
   */
  public int getBodylength() {
    return blength;
  }
  
  /**
   * @param bodylength le bodylength à définir.
   */
  public void setBodylength(int bodylength) {
    this.blength = bodylength;
  }
  
  /**
   * @return le bodydesc.
   */
  public String getBodydesc() {
    return bdesc;
  }
  
  /**
   * @param bodydesc le bodydesc à définir.
   */
  public void setBodydesc(String bodydesc) {
    this.bdesc = bodydesc;
  }
  
}
