/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;

/**
 * Cette classe regroupe toute les bibliothèques programmes utilisées par Série N.
 * Les bibliothèques sont stockées sans la lettre d'environnement. Certaines bibliothèques comme les bibliothèque spécifiques ne sont pas
 * stockées sous forme entière mais seul le radical est stocké comme par exemple S00 -> S000886 pour les spécifiques de l'Outil Parfait.
 * C'est la variable "nomACompleter" qui indique si le nom est complet ou s'il s'agit juste d'un radical.
 * La variable "obsolete" indique si une bibliothèque n'est plus utilisée comme par exemple EXMAS, elle est conservée dans la classe car
 * il peut y avoir peut être encore des références en base de données (tables des menus).
 * Il faut mettre les bibliothèques à compléter à la fin de la liste.
 */
public enum EnumBibliothequeProgramme {
  COMPTABILITE_CGM("CGMAS", "Comptabilité", false, false),
  EXPLOITATION_EXM("EXMAS", "Exploitation", false, true),
  EXPLOITATION_EXP("EXPAS", "Exploitation", false, false),
  GESTION_COMMERCIALE_GVM("GVMAS", "Gestion commerciale", false, false),
  GESTION_COMMERCIALE_GVX("GVMX", "Gestion commerciale", false, false),
  GESTION_COMMERCIALE_GAM("GAMAS", "Gestion commerciale", false, false),
  GESTION_EFFETS_GEM("GEMAS", "Gestion des effets", false, false),
  GESTION_IMMOBILISATION_GIM("GIMAS", "Gestion des immobilisations", false, false),
  GESTION_LIASSE_FISCALE_GLF("GLFAS", "Gestion liasse fiscale", false, false),
  GESTION_MAINTENANCE_GMM("GMMAS", "Gestion maintenance", false, false),
  GESTION_PRODUCTION_GPM("GPMAS", "Gestion de la production", false, false),
  GESTION_TRESORERIE_GTM("GTMAS", "Gestion trésorerie", false, false),
  GESTION_PROSPECTION_PRM("PRMAS", "Gestion prospection", false, false),
  GESTION_TABLEAU_BORD_TBM("TBMAS", "Gestion tableau de bord", false, false),
  GESTION_TELEMAINTENANCE_TLM("TELM", "Gestion télémaintenance", false, false),
  GESTION_IMPAYES_TIM("TIMAS", "Gestion des impayés", false, false),
  GESTION_TRANSPORTS_TRM("TRMAS", "Gestion des transports", false, false),
  GESTION_WEB_SERVICES_WBS("WBSAS", "Gestion des web services", false, false),
  SPECIFIQUE_SERIEN_SPM("SPMAS", "Spécifiques de Série N", false, false),
  SPECIFIQUE_CLIENT_SPM("SPM", "Spécifiques clients", true, false),
  SPECIFIQUE_CLIENT_S00("S00", "Spécifiques clients", true, false);
  
  private final String nomSansLettre;
  private final String libelle;
  private final boolean nomACompleter;
  private final boolean obsolete;
  
  /**
   * Constructeur.
   */
  EnumBibliothequeProgramme(String pNomSansLettre, String pLibelle, boolean pNomACompleter, boolean pObsolete) {
    nomSansLettre = pNomSansLettre;
    libelle = pLibelle;
    nomACompleter = pNomACompleter;
    obsolete = pObsolete;
  }
  
  /**
   * Retourne le nom sans la lettre d'environnement.
   */
  @Override
  public String toString() {
    return nomSansLettre;
  }
  
  /**
   * Retourner l'objet enum par son nom qui doi être exactement le même.
   * Le mode silencieux permet d'éviter de déclencher une exception qui est plutôt contraignante dans certains cas d'utilisation. Si le
   * mode silencieux est activé alors la méthode retourne null à la place de l'exception.
   */
  public static EnumBibliothequeProgramme valueOfByNom(String pNomSansLettre, boolean pModeSilencieux) {
    for (EnumBibliothequeProgramme value : values()) {
      if (pNomSansLettre.equals(value.getNomSansLettre())) {
        return value;
      }
    }
    if (pModeSilencieux) {
      return null;
    }
    throw new MessageErreurException("Le nom de la bibliothèque programme est invalide : " + pNomSansLettre);
  }
  
  /**
   * Recherche et retourne l'objet enum par son nom en fonction de la variable "nomACompleter".
   */
  public static EnumBibliothequeProgramme rechercherBibliothequeProgramme(String pNomSansLettre) {
    for (EnumBibliothequeProgramme value : values()) {
      // Cas des noms des bibliothèques entières (le contenu doit être exact)
      if (!value.isNomACompleter()) {
        if (pNomSansLettre.equals(value.getNomSansLettre())) {
          return value;
        }
      }
      // Cas des noms de bibliothèques partiels (le contenu doit avoir le même début: S00 -> S000886)
      else {
        if (pNomSansLettre.startsWith(value.getNomSansLettre())) {
          return value;
        }
      }
    }
    
    // Le nom de la bibliothèque n'a pas été trouvé
    return null;
  }
  
  /**
   * Construit le nom de la bibliothèque avec la lettre.
   */
  public static String retournerNomBibliotheque(char pLettre, EnumBibliothequeProgramme pBibliothequeProgramme, String pTexteAAjouter) {
    if (pBibliothequeProgramme == null) {
      throw new MessageErreurException("La bibliothèque programme est invalide.");
    }
    
    String nomBibliotheque = null;
    if (!pBibliothequeProgramme.isNomACompleter()) {
      nomBibliotheque = pLettre + pBibliothequeProgramme.getNomSansLettre();
    }
    else {
      pTexteAAjouter = Constantes.normerTexte(pTexteAAjouter);
      if (pTexteAAjouter.isEmpty()) {
        throw new MessageErreurException("Le texte à ajouter, pour construire le nom de la bibiothèque ("
            + pBibliothequeProgramme.getNomSansLettre() + "), est invalide.");
      }
      nomBibliotheque = pLettre + pBibliothequeProgramme.getNomSansLettre() + pTexteAAjouter;
    }
    
    return nomBibliotheque;
  }
  
  /**
   * Retourne la bibliothèque dont le nom complet a été construit.
   */
  public static Bibliotheque retournerBibliotheque(char pLettre, EnumBibliothequeProgramme pBibliothequeProgramme,
      String pTexteAAjouter) {
    String nomBibliotheque = retournerNomBibliotheque(pLettre, pBibliothequeProgramme, pTexteAAjouter);
    return new Bibliotheque(IdBibliotheque.getInstance(nomBibliotheque), EnumTypeBibliotheque.PROGRAMMES_SERIEN);
  }
  
  /**
   * Modifie la lettre d'environnement de la bibliothèque avec la lettre fournit.
   */
  public static String modifierLettreEnvironnement(char pLettre, String pNomBibliothequeComplet) {
    if (pLettre == ' ') {
      return pNomBibliothequeComplet;
    }
    pNomBibliothequeComplet = Constantes.normerTexte(pNomBibliothequeComplet);
    if (pNomBibliothequeComplet.isEmpty()) {
      return "";
    }
    
    try {
      String nomSansLettre = pNomBibliothequeComplet.substring(1);
      EnumBibliothequeProgramme enumBibliothequeProgramme = rechercherBibliothequeProgramme(nomSansLettre);
      if (enumBibliothequeProgramme != null) {
        return pLettre + nomSansLettre;
      }
    }
    catch (Exception e) {
    }
    
    return pNomBibliothequeComplet;
  }
  
  // -- Accesseurs
  
  public String getNomSansLettre() {
    return nomSansLettre;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public boolean isNomACompleter() {
    return nomACompleter;
  }
  
  public boolean isObsolete() {
    return obsolete;
  }
  
}
