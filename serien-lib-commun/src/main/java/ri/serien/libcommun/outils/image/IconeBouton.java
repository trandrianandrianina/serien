/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.image;

import javax.swing.ImageIcon;

import ri.serien.libcommun.outils.Trace;

/**
 * Icône destinée aux boutons, disponible dans quatres variantes.
 * 
 * Cette classe permet de charger et mémoriser les icônes affichées dans les boutons. Les icônes sont chargées uniquement la première fois
 * qu'elles sont utilisés. Ensuite, c'est la version en mémoire qui est utilisée.
 * 
 * Chaque icône existe en 4 versions :
 * - la version standard utilisée lorsque l'icône est active.
 * - la version grisée lorsque l'icône est inactive.
 * - la version sombre utilisée lorsque le pointeur de la souris passe au dessus d'une icône active.
 * - la version claire utilisée pour mettre l'icône en surbrillance lorsqu'on clique dessus.
 * 
 * Seules la version standard est récupérée à partir des ressources. Les autres versions sont déterminées automatiquement à partir de
 * la version standard.
 */
public class IconeBouton {
  private static float POURCENTAGE_ASSOMBRISSEMENT = 0.15f;
  private static float POURCENTAGE_ECLAIRCISSEMENT = 0.25f;
  
  private String ressource = null;
  private ImageIcon iconeStandard = null;
  private ImageIcon iconeGrise = null;
  private ImageIcon iconeSombre = null;
  private ImageIcon iconeClaire = null;
  
  /**
   * Constructeur.
   * @param pResource Nom de la resource contentant l'icône standard.
   */
  public IconeBouton(String pResource) {
    ressource = pResource;
  }
  
  /**
   * Icône standard utilisée lorsque l'icône est active.
   * @return Icone.
   */
  public ImageIcon getIconeStandard() {
    // Tester si l'icône existe déjà
    if (iconeStandard == null && ressource != null) {
      try {
        // Charger l'icône standard à partir des ressources
        iconeStandard = new ImageIcon(IconeBouton.class.getClassLoader().getResource(ressource));
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur lors du chargement de l'icône : " + ressource);
      }
    }
    
    return iconeStandard;
  }
  
  /**
   * Icône grisée lorsque l'icône est inactive.
   * @return Icone.
   */
  public ImageIcon getIconeGrisee() {
    // Tester si l'icône existe déjà
    if (iconeGrise == null) {
      // Créer l'icone grisée
      iconeGrise = OutilImage.griserIcone(getIconeStandard());
    }
    
    return iconeGrise;
  }
  
  /**
   * Icône sombre utilisée lorsque le pointeur de la souris passe au dessus d'une icône active.
   * @return Icone.
   */
  public ImageIcon getIconeSombre() {
    // Tester si l'icône existe déjà
    if (iconeSombre == null) {
      // Créer l'icone sombre
      iconeSombre = OutilImage.assombrirIcone(getIconeStandard(), POURCENTAGE_ASSOMBRISSEMENT);
    }
    
    return iconeSombre;
  }
  
  /**
   * Icône en surbrillance utilisée pour mettre l'icône en surbrillance lorsqu'on clique dessus.
   * @return Icone.
   */
  public ImageIcon getIconeClaire() {
    // Tester si l'icône existe déjà
    if (iconeClaire == null) {
      // Créer l'icone claire
      iconeClaire = OutilImage.eclaircirIcone(getIconeStandard(), POURCENTAGE_ECLAIRCISSEMENT);
    }
    
    return iconeClaire;
  }
}
