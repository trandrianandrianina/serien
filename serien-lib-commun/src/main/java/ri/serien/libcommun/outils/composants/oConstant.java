/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.composants;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import ri.serien.libcommun.outils.Constantes;

/**
 * Description de l'objet Constant
 */
public class oConstant extends GenericBase {
  // Constantes erreurs de compilation
  private final static String ERREUR_CONVERT_OFFSET = "OFFSET impossible à convertir en numérique.";
  private final static String ERREUR_CONVERT_LONGUEUR = "LONGUEUR impossible à convertir en numérique.";
  private final static String ERREUR_CONVERT_LONGUEUR5250 = "LONGUEUR5250 impossible à convertir en numérique.";
  private final static String ERREUR_CONVERT_LIGNE = "LIGNE impossible à convertir en numérique.";
  private final static String ERREUR_CONVERT_COLONNE = "COLONNE impossible à convertir en numérique.";
  private final static String ERREUR_DATA_NULL = "Tableau binaire non initialisé.";
  private final static String ERREUR_LONGUEUR_CONSTANT_ERRONNE = "La longueur du tableau descriptif est supérieure à celle du record.";
  
  // Constantes ID, il y a 500 variables possibles pour l'objet oConstant
  // Chaque variable doit obligatoirement avoir un ID (pour la compilation)
  private final static int ID_OCONSTANT = 2000;
  private final static int ID_OFFSET = ID_OCONSTANT + 1;
  private final static int ID_LONGUEUR = ID_OCONSTANT + 2;
  private final static int ID_LONGUEUR5250 = ID_OCONSTANT + 3;
  private final static int ID_TEXTE = ID_OCONSTANT + 4;
  private final static int ID_TEXTE5250 = ID_OCONSTANT + 5;
  private final static int ID_LIGNE = ID_OCONSTANT + 6;
  private final static int ID_COLONNE = ID_OCONSTANT + 7;
  private final static int ID_INDICATEUR = ID_OCONSTANT + 8;
  
  // Variables
  private int offset = -1; // Position dans le buffer de données transmis par le serveur
  private int longueur = -1; // Longueur théorique de la constante
  private int longueur5250 = -1; // Longueur théorique de la constante 5250 dans le SDA
  private String texte = null; // Constante
  private String texte5250 = null; // Constante dans le SDA
  private byte ligne = -1; // Numéro de la ligne dans sa représentation 5250
  private int colonne = -1; // Numéro de la colonne dans sa représentation 5250
  private String indicateur[] = null;
  
  /**
   * Constructeur de la classe
   * 
   */
  public oConstant() {
  }
  
  /**
   * Initialise le nom de l'objet
   * @param valeur
   */
  public void setNom(String valeur) {
    setName(valeur.trim());
  }
  
  /**
   * Initialise le nom du Record
   * @param valeur
   */
  public void setRecord(String valeur) {
    setParent(valeur.trim());
  }
  
  /**
   * Initialise l'offset
   * @param valeur
   * @return
   */
  public int setOffset(String valeur) {
    try {
      offset = Integer.parseInt(valeur);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_OFFSET + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setOffset(int valeur) {
    offset = valeur;
    return Constantes.OK;
  }
  
  /**
   * Initialise la longueur de la constante
   * @param valeur
   * @return
   */
  public int setLongueur(String valeur) {
    try {
      longueur = Integer.parseInt(valeur);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_LONGUEUR + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setLongueur(int valeur) {
    longueur = valeur;
    return Constantes.OK;
  }
  
  /**
   * Initialise la longueur 5250 de la constante
   * @param valeur
   * @return
   */
  public int setLongueur5250(String valeur) {
    try {
      longueur5250 = Integer.parseInt(valeur);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_LONGUEUR5250 + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setLongueur5250(int valeur) {
    longueur5250 = valeur;
    return Constantes.OK;
  }
  
  /**
   * Initialise la ligne dans l'écran 5250
   * @param valeur
   * @return
   */
  public int setLigne(String valeur) {
    try {
      ligne = (byte) Integer.parseInt(valeur);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_LIGNE + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setLigne(int valeur) {
    ligne = (byte) valeur;
    return Constantes.OK;
  }
  
  /**
   * Initialise la colonne dans l'écran 5250
   * @param valeur
   * @return
   */
  public int setColonne(String valeur) {
    try {
      colonne = Integer.parseInt(valeur);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_COLONNE + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setColonne(int valeur) {
    colonne = valeur;
    return Constantes.OK;
  }
  
  /**
   * Initialise le texte de la constante
   * @param valeur
   */
  public void setTexte(String valeur) {
    texte = valeur;
  }
  
  /**
   * Initialise le texte 5250 de la constante
   * @param valeur
   */
  public void setTexte5250(String valeur) {
    texte5250 = valeur;
  }
  
  /**
   * Initialise les indicateurs
   * @param valeur
   */
  public void setIndicateur(String valeur) {
    if (valeur == null) {
      return;
    }
    if (valeur.trim().equals("")) {
      return;
    }
    iIndicator ind = new iIndicator();
    indicateur = ind.tabconvert2notpol(valeur.trim());
  }
  
  public void setIndicateur(String valeur[]) {
    indicateur = valeur;
  }
  
  /**
   * Retourne le nom de l'objet
   * @return
   */
  public String getNom() {
    return getName();
  }
  
  /**
   * Retourne le nom du Record
   * @return
   */
  public String getRecord() {
    return getParent();
  }
  
  /**
   * Retourne l'offset
   * @return
   */
  public int getOffset() {
    return offset;
  }
  
  /**
   * Retourne la longueur de la constante
   * @return
   */
  public int getLongueur() {
    return longueur;
  }
  
  /**
   * Retourne la longueur 5250 de la constante
   * @return
   */
  public int getLongueur5250() {
    return longueur5250;
  }
  
  /**
   * Retourne la ligne dans l'écran 5250
   * @return
   */
  public byte getLigne() {
    return ligne;
  }
  
  /**
   * Retourne la colonne dans l'écran 5250
   * @return
   */
  public int getColonne() {
    return colonne;
  }
  
  /**
   * Retourne le texte de la constante
   * @return
   */
  public String getTexte() {
    return texte;
  }
  
  /**
   * Retourne le texte 5250 de la constante
   * @return
   */
  public String getTexte5250() {
    int i = 0;
    char tabChar[] = null;
    
    if (texte5250 == null) {
      tabChar = new char[longueur5250];
      for (i = 0; i < tabChar.length; i++) {
        tabChar[i] = ' ';
      }
      return new String(tabChar);
    }
    else if (longueur5250 > texte5250.length()) {
      tabChar = new char[longueur5250 - texte5250.length()];
      for (i = 0; i < tabChar.length; i++) {
        tabChar[i] = ' ';
      }
      return texte5250 + new String(tabChar);
    }
    else {
      return texte5250;
    }
  }
  
  /**
   * Retourne les indicateurs
   * @return
   */
  public String[] getIndicateur() {
    return indicateur;
  }
  
  /**
   * Initialise les indicateurs sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public void setIndicateurPol(String valeur) {
    indicateur = valeur.split(" ");
  }
  
  /**
   * Retourne les indicateurs sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public String getIndicateurPol() {
    if (indicateur == null) {
      return null;
    }
    
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < indicateur.length; i++) {
      sb.append(indicateur[i]).append(' ');
    }
    return sb.toString().trim();
  }
  
  /**
   * Initialise les attributs et leurs types d'un objet
   */
  @Override
  public void setAttributes(HashMap<String, Object> alisteAttributes) {
    super.setAttributes(alisteAttributes);
    
    // OFFSET
    if (listeAttributes.containsKey("OFFSET")) {
      setOffset((Integer) listeAttributes.get("OFFSET"));
    }
    // LENGTH5250
    if (listeAttributes.containsKey("LENGTH5250")) {
      setLongueur5250((Integer) listeAttributes.get("LENGTH5250"));
    }
    // LENGTH
    if (listeAttributes.containsKey("LENGTH")) {
      setLongueur((Integer) listeAttributes.get("LENGTH"));
    }
    // TEXT5250
    if (listeAttributes.containsKey("TEXT5250")) {
      setTexte5250((String) listeAttributes.get("TEXT5250"));
    }
    // TEXT
    if (listeAttributes.containsKey("TEXT")) {
      setTexte((String) listeAttributes.get("TEXT"));
    }
    // LINE
    if (listeAttributes.containsKey("LINE")) {
      setLigne((Byte) listeAttributes.get("LINE"));
    }
    // COLUMN
    if (listeAttributes.containsKey("COLUMN")) {
      setColonne((Integer) listeAttributes.get("COLUMN"));
    }
    // INDICATOR
    if (listeAttributes.containsKey("INDICATOR")) {
      setIndicateurPol((String) listeAttributes.get("INDICATOR"));
    }
  }
  
  /**
   * Retourne les attributs et leurs types d'un objet
   */
  @Override
  public HashMap<String, Object> getAttributes() {
    super.getAttributes();
    
    listeAttributes.put("OFFSET", Integer.valueOf(getOffset()));
    listeAttributes.put("LENGTH5250", Integer.valueOf(getLongueur5250()));
    listeAttributes.put("LENGTH", Integer.valueOf(getLongueur()));
    listeAttributes.put("TEXT5250", new String(getTexte5250()));
    listeAttributes.put("TEXT", new String(getTexte()));
    listeAttributes.put("LINE", Byte.valueOf(getLigne()));
    listeAttributes.put("COLUMN", Integer.valueOf(getColonne()));
    listeAttributes.put("INDICATOR", new String(getIndicateurPol()));
    
    return listeAttributes;
  }
  
  /**
   * Retourne les infos pour générer un fichier PNL
   * @return
   */
  public ArrayList<String> getPNL() {
    ArrayList<String> liste = new ArrayList<String>();
    liste.add("OBJECT_CONSTANT");
    liste.add("\tTYPEOBJ=composants.oConstant");
    liste.add("\tPARENT=" + getParent());
    // liste.add("\tOFFSET=" + getOffSet);
    liste.add("\tLENGTH=" + getLongueur());
    liste.add("\tLENGTH5250=" + getLongueur5250());
    if (getTexte() != null) {
      liste.add("\tTEXT=" + getTexte());
    }
    if (getTexte5250() != null) {
      liste.add("\tTEXT5250=" + getTexte5250());
    }
    liste.add("\tLINE=" + getLigne());
    liste.add("\tCOLUMN=" + getColonne());
    if (getIndicateurPol() != null) {
      liste.add("\tVISIBILITY=" + getIndicateurPol());
    }
    liste.add("END;");
    
    return liste;
  }
  
  /**
   * Initialise les valeurs avec un tableau venant du PNL
   * @param valeur
   */
  @Override
  public void pnlToValeurs(LinkedHashMap<String, String> valeur) {
    super.pnlToValeurs(valeur);
    
    // OFFSET
    if (valeur.containsKey("OFFSET")) {
      setOffset(valeur.get("OFFSET"));
    }
    // LENGTH5250
    if (valeur.containsKey("LENGTH5250")) {
      setLongueur5250(valeur.get("LENGTH5250"));
    }
    // LENGTH
    if (valeur.containsKey("LENGTH")) {
      setLongueur(valeur.get("LENGTH"));
    }
    // TEXT5250
    if (valeur.containsKey("TEXT5250")) {
      setTexte5250(valeur.get("TEXT5250"));
    }
    // TEXT
    if (valeur.containsKey("TEXT")) {
      setTexte(valeur.get("TEXT"));
    }
    // LINE
    if (valeur.containsKey("LINE")) {
      setLigne(valeur.get("LINE"));
    }
    // COLUMN
    if (valeur.containsKey("COLUMN")) {
      setColonne(valeur.get("COLUMN"));
    }
    // INDICATOR
    if (valeur.containsKey("INDICATOR")) {
      setIndicateur(valeur.get("INDICATOR"));
    }
  }
  
  /**
   * Convertie l'objet au format binaire
   * @param tabListFic
   * @throws UnsupportedEncodingException
   */
  @Override
  public ArrayList<Integer> valeursToBinaire(ArrayList<String> tabListFic) throws UnsupportedEncodingException {
    int i = 0;
    
    super.valeursToBinaire(tabListFic);
    
    // OFFSET
    addTabCompilation(ID_OFFSET, tabCompilation, offset);
    // LONGUEUR
    addTabCompilation(ID_LONGUEUR, tabCompilation, longueur);
    // LONGUEUR5250
    addTabCompilation(ID_LONGUEUR5250, tabCompilation, longueur5250);
    // TEXT5250
    if (texte5250 != null) {
      addTabCompilation(ID_TEXTE5250, tabCompilation, texte5250);
    }
    // TEXT
    if (texte != null) {
      addTabCompilation(ID_TEXTE, tabCompilation, texte);
    }
    // LIGNE
    addTabCompilation(ID_LIGNE, tabCompilation, ligne);
    // COLONNE
    addTabCompilation(ID_COLONNE, tabCompilation, colonne);
    // INDICATEURS
    if (indicateur != null) {
      addTabCompilation(ID_INDICATEUR, tabCompilation, indicateur.length);
      for (i = 0; i < indicateur.length; i++) {
        addTabCompilation(Constantes.NONE, tabCompilation, indicateur[i]);
      }
    }
    
    insertTailleObjet();
    return tabCompilation;
  }
  
  /**
   * Initialise les variables de l'objet oConstant à partir d'un tableau binaire
   * Retourne si erreur
   * @param tabInt
   * @param ListFic
   * @return
   */
  @Override
  public int binaireToValeurs(int tabInt[], String ListFic[]) {
    return binaireToValeurs(tabInt, 0, null);
  }
  
  @Override
  public int binaireToValeurs(int tabInt[], int offst, String ListFic[]) {
    int i = 0;
    int j = 0;
    int k = 0;
    int l = 0;
    int m = 0;
    int lg = 0;
    // byte tabByte[]=null;
    byte tabByte[] = new byte[Constantes.DATAQLONG];
    String tabchaine[] = null;
    int lngdescdata = 0;
    
    // Vérification du tableau
    if (tabInt == null) {
      setMsgErreur(ERREUR_DATA_NULL);
      return Constantes.ERREUR;
    }
    
    i = offst;
    lngdescdata = tabInt[i++];
    if (lngdescdata > tabInt.length) {
      setMsgErreur(ERREUR_LONGUEUR_CONSTANT_ERRONNE);
      return Constantes.ERREUR;
    }
    
    lngdescdata = lngdescdata + i;
    while (i < lngdescdata) {
      switch (tabInt[i]) {
        case ID_OFFSET:
          i += 2;
          setOffset(tabInt[i]);
          i++;
          break;
        case ID_LONGUEUR:
          i += 2;
          setLongueur(tabInt[i]);
          i++;
          break;
        case ID_LONGUEUR5250:
          i += 2;
          setLongueur5250(tabInt[i]);
          i++;
          break;
        case ID_TEXTE:
          // longueur du texte
          lg = tabInt[++i];
          // tabByte = new byte[k];
          // récupération du texte
          i++;
          k = lg + i;
          for (j = 0; i < k; i++) {
            tabByte[j++] = (byte) tabInt[i];
          }
          // récupération du texte
          setTexte(new String(tabByte, 0, lg));
          break;
        case ID_TEXTE5250:
          // longueur du texte5250
          lg = tabInt[++i];
          // tabByte = new byte[k];
          // récupération du texte5250
          i++;
          k = lg + i;
          for (j = 0; i < k; i++) {
            tabByte[j++] = (byte) tabInt[i];
          }
          // récupération du texte5250
          setTexte5250(new String(tabByte, 0, lg));
          break;
        case ID_LIGNE:
          i += 2;
          setLigne(tabInt[i]);
          i++;
          break;
        case ID_COLONNE:
          i += 2;
          setColonne(tabInt[i]);
          i++;
          break;
        case ID_INDICATEUR:
          i += 2;
          m = tabInt[i];
          tabchaine = new String[m];
          for (j = 0; j < m; j++) {
            // longueur de l'indicateur
            lg = tabInt[++i];
            // tabByte = new byte[k];
            // récupération du nom
            i++;
            k = lg + i;
            for (l = 0; i < k; i++) {
              tabByte[l++] = (byte) tabInt[i];
            }
            i--;
            tabchaine[j] = new String(tabByte, 0, lg);
          }
          // récupération du nom
          setIndicateur(tabchaine);
          i++;
          break;
        
        // Valeur non traitée <-----------------------------------------------------
        default:
          i = super.binaireToValeurs(tabInt, i, null);
      }
    }
    tabByte = null;
    tabInt = null;
    
    return i;
  }
  
}
