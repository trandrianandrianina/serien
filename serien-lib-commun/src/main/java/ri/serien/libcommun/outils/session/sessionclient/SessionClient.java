/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session.sessionclient;

import java.io.Serializable;

import ri.serien.libcommun.exploitation.parametreavance.CritereParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.EnumCleParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.ListeParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.ParametreAvance;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.exploitation.securiteutilisateurgescom.IdSecuriteUtilisateurGescom;
import ri.serien.libcommun.exploitation.securiteutilisateurgescom.ListeSecuriteUtilisateurGescom;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.ListeParametreSysteme;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Session côté client.
 * 
 * Une session côté client correspond à un point de menu lancé dans un onglet.
 * 
 * La plupart des sessions sont associées à un établissement (nommé l'établissement courant). C'est la méthode setIdEtablissement() qui
 * permet d'associer un établissement à la session. La méthode getIdEtablissement() permet de connaître l'établissement de la session.
 * Toutefois, il est possible que certaines sessions ne soient pas associées à un établissement car cela n'a pas de sens pour elles
 * (par exemple, la gestion des licences est indépendante des établissements). Dans ce cas, les méthodes de la classe qui nécessitent
 * un établissement pas défaut ne peuvent pas être utilisées.
 * 
 * La session sert de cache pour un certains nombres d'information :
 * - les paramètres systèmes (PS),
 * - les sécurités des utilisateurs,
 * - les paramètres avancés (table PSEMPAVM).
 * 
 * Ces paramètres sont lus lors de la première utilisation (lazy initialisation) et ne sont pas rechargés pendant toute la durée de vie
 * de la session. Si la valeur de l'un de ces paramètres change, elle ne sera pris en compte qu'après fermeture et révouverture
 * d'une nouvelle session.
 */
public class SessionClient implements Serializable {
  private IdSession idSession = null;
  private IdEtablissement idEtablissement = null;
  private ListeSecuriteUtilisateurGescom listeSecuriteUtilisateurGescom = new ListeSecuriteUtilisateurGescom();
  private ListeParametreSysteme listeParametreSysteme = new ListeParametreSysteme();
  private ListeParametreAvance listeParametreAvance = new ListeParametreAvance();
  
  /**
   * Constructeur.
   */
  public SessionClient(IdSession pIdSession) {
    idSession = pIdSession;
    IdSession.controlerId(idSession, true);
  }
  
  // -- Méthodes publiques
  
  /**
   * Contrôler qu'un établissement a été associé à la session.
   * Un message d'erreur est généré si ce n'est pas le cas.
   */
  private void controlerEtablissement() {
    if (idEtablissement == null) {
      throw new MessageErreurException(
          "La session n'est associée à aucun établissement. L'opération en cours nécessite un établissement pour être effectuée. Merci de contacter le service assistance.");
    }
  }
  
  /**
   * Vérifier un droit donné.
   * Chargement de la liste des sécurités à la première vérification d'un droit ou si on change d'établissement.
   */
  public boolean verifierDroitGescom(String pProfil, EnumDroitSecurite pDroitSecurite) {
    // Contrôler les paramètres
    controlerEtablissement();
    if (pDroitSecurite == null) {
      throw new MessageErreurException("La sécurité passée en paramètre est invalide. Il est impossible de vérifier les droits.");
    }
    
    // Charger la liste des sécurités de l'établissement si elle ne sont pas chargées (au premier appel pour un établissement)
    if (!listeSecuriteUtilisateurGescom.isEtablissementCharge(idEtablissement)) {
      listeSecuriteUtilisateurGescom =
          ListeSecuriteUtilisateurGescom.charger(idSession, listeSecuriteUtilisateurGescom, idEtablissement, pProfil);
    }
    
    // Construire l'identifiant de la sécurité Gescom
    IdSecuriteUtilisateurGescom idSecuriteUtilisateurGescom = IdSecuriteUtilisateurGescom.getInstance(idEtablissement, pProfil);
    
    // Vérifier le droit
    return listeSecuriteUtilisateurGescom.verifierDroit(idSession, idSecuriteUtilisateurGescom, pDroitSecurite);
  }
  
  /**
   * Retourner la valeur d'un paramètre système (PS) pour cette session.
   */
  public Character getValeurParametreSysteme(EnumParametreSysteme pEnumParametreSysteme) {
    // Contrôler les paramètres
    controlerEtablissement();
    if (pEnumParametreSysteme == null) {
      throw new MessageErreurException("Le paramètre système est invalide. Il est impossible de récupérer sa valeur.");
    }
    
    // Charger la liste des paramètres systèmes de l'établissement s'ils ne sont pas chargés (au premier appel pour un établissement)
    if (!listeParametreSysteme.isEtablissementCharge(idEtablissement)) {
      // Charger la liste pour l'établissement
      ListeParametreSysteme liste = listeParametreSysteme.charger(idSession, idEtablissement);
      
      // Ajouter cette liste à la liste multi-établissements de la session
      if (liste != null) {
        listeParametreSysteme.addAll(liste);
      }
    }
    
    // Lire le paramètre système dans la session client
    return listeParametreSysteme.getValeurParametreSysteme(idEtablissement, pEnumParametreSysteme);
  }
  
  /**
   * Vérifier si un paramètre système de type A est actif dans la liste des paramètres systèmes.
   */
  public boolean isParametreSystemeActif(EnumParametreSysteme pEnumParametreSysteme) {
    // Contrôler les paramètres
    controlerEtablissement();
    if (pEnumParametreSysteme == null) {
      throw new MessageErreurException("Le paramètre système est invalide. Il est impossible de récupérer sa valeur.");
    }
    
    // Charger la liste des paramètres systèmes de l'établissement s'ils ne sont pas chargés (au premier appel pour un établissement)
    if (!listeParametreSysteme.isEtablissementCharge(idEtablissement)) {
      // Charger la liste pour l'établissement
      ListeParametreSysteme liste = listeParametreSysteme.charger(idSession, idEtablissement);
      
      // Ajouter cette liste à la liste multi-établissements de la session
      if (liste != null) {
        listeParametreSysteme.addAll(liste);
      }
    }
    
    // Lire le paramètre système dans la session client
    return listeParametreSysteme.isParametreSystemeActif(idEtablissement, pEnumParametreSysteme);
  }
  
  /**
   * Retourner la valeur d'un paramètre avancé.
   * 
   * @param pEnumCleParametreAvance Clé du paramètre avancé.
   * @return Valeur du parmaètre avancé dont la clé a été passée en paramètre.
   */
  public String getValeurParametreAvance(EnumCleParametreAvance pEnumCleParametreAvance) {
    // Contrôler les paramètres
    controlerEtablissement();
    if (pEnumCleParametreAvance == null) {
      throw new MessageErreurException(
          "Le paramètre avancé passé en paramètre est invalide. Il est impossible de vérifier le paramétrage.");
    }
    
    // Charger la liste des paramètres avancés s'ils ne sont pas chargées (au premier appel pour un établissement).
    if (!listeParametreAvance.isEtablissementCharge(idEtablissement)) {
      CritereParametreAvance critereParametreAvance = new CritereParametreAvance();
      critereParametreAvance.setIdEtablissement(idEtablissement);
      ListeParametreAvance liste = listeParametreAvance.charger(idSession, critereParametreAvance);
      
      // Ajouter cette liste à la liste multi-établissements de la session
      if (liste != null) {
        listeParametreAvance.addAll(liste);
      }
    }
    
    // Récupérer le paramètre avancé correspondant à la clé
    ParametreAvance parametreAvance = listeParametreAvance.get(idEtablissement, pEnumCleParametreAvance);
    if (parametreAvance == null) {
      return null;
    }
    
    // Retourner la valeur si le paramètre existe.
    return parametreAvance.getValeur();
  }
  
  /**
   * Identifiant de session
   */
  public IdSession getIdSession() {
    return idSession;
  }
  
  /**
   * Modifier l'établissement en cours pour la session.
   * @param pIdEtablissement Identifiant de l'établissement (null si aucun).
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourner l'identifiant de l'établissement en cours pour la session.
   * @return Etablissement associé à la session (null si aucun).
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
}
