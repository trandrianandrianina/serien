/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Utilitaire pour gestion des jars.
 */
public class JarUtils {
  private static final String FICHIER_VERSION = "version.txt";
  
  /**
   * Retourne le numéro d'implémentation.
   */
  public static String getJarImplementationVersion(String jar) {
    JarFile jarfile;
    Manifest manifest;
    try {
      jarfile = new JarFile(jar);
      manifest = jarfile.getManifest();
      jarfile.close();
    }
    catch (IOException e) {
      return "";
    }
    Attributes att = manifest.getMainAttributes();
    return att.getValue("Implementation-Version");
  }
  
  /**
   * Retourne le numéro de spécification.
   */
  public static String getJarSpecificationVersion(String jar) {
    JarFile jarfile;
    Manifest manifest;
    try {
      jarfile = new JarFile(jar);
      manifest = jarfile.getManifest();
      jarfile.close();
    }
    catch (IOException e) {
      return "";
    }
    
    Attributes att = manifest.getMainAttributes();
    return att.getValue("Specification-Version");
  }
  
  /**
   * Retourne le numéro de spécification sous forme d'entier.
   */
  public static int getJarSpecificationVersionToInt(String jar) {
    try {
      return Integer.parseInt(getJarSpecificationVersion(jar));
    }
    catch (Exception e) {
      return 0;
    }
  }
  
  /**
   * Retourner le numéro de version issu du fichier "version.txt" inclus dans le JAR.
   * 
   * La méthode génére des MessageErreurException si la lecture du numéro de version échoue.
   * 
   * @param pClassLoader Fournir le Classloader d'une des classes du JAR dont on veut charger la version.
   * @return Numéro de version (null si pas trouvé).
   */
  public static String getVersionJar(ClassLoader pClassLoader) {
    // Ouvrir le fichier version
    InputStream inputStream = pClassLoader.getResourceAsStream(FICHIER_VERSION);
    if (inputStream == null) {
      throw new MessageErreurException("Impossible de trouver le fichier contenant la version du JAR : " + FICHIER_VERSION);
    }
    
    InputStreamReader streamReader = new InputStreamReader(inputStream);
    BufferedReader reader = new BufferedReader(streamReader);
    String version = null;
    try {
      version = reader.readLine();
      if (version == null) {
        throw new MessageErreurException("Le fichier contenant la version du JAR est vide : " + FICHIER_VERSION);
      }
    }
    catch (IOException e) {
      throw new MessageErreurException("Impossible de lire le fichier contenant la version du JAR : " + FICHIER_VERSION);
    }
    
    return version;
  }
}
