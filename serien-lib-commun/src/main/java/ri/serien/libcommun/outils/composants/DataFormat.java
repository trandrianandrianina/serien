/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.composants;

import java.util.Arrays;

import ri.serien.libcommun.outils.Constantes;

/**
 * Gestion des chaines particulières avec des longueurs fixes, ajout de blancs à droite ou à gauche
 */
public class DataFormat {
  // Variables
  private StringBuffer sb = null;
  private char output_edt = ' ';
  private int longueur = 0;
  private int decimal = 0;
  private boolean negatif = false;
  // private boolean isME=false, isER=false, isMF=false, isFE=false, isRB=false, isRZ=false, isRL=false;
  
  private char[] zero = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', };
  private char[] spaces = new char[2048];
  
  /**
   * Constructeur
   */
  public DataFormat() {
    sb = new StringBuffer();
    Arrays.fill(spaces, ' ');
  }
  
  /**
   * Constructeur
   */
  public DataFormat(String str) {
    sb = new StringBuffer(str.trim());
    Arrays.fill(spaces, ' ');
  }
  
  /**
   * Constructeur
   */
  public DataFormat(String str, char aoutput_edt, int lg, int dec/*, String check_edt*/) {
    sb = new StringBuffer(str.trim());
    output_edt = aoutput_edt == ' ' ? 'Z' : aoutput_edt; // On force la suppression des zéros si pas de output défini
    longueur = lg;
    decimal = dec;
    Arrays.fill(spaces, ' ');
    // +"/////////////////////////////////////////////////////////////");
    /*
    // On recherche les différents controles
    if (!check_edt.trim().equals(""))
    {
      isME = check_edt.contains("ME");
      isER = check_edt.contains("ER");
      isMF = check_edt.contains("MF");
      isFE = check_edt.contains("FE");
      isRB = check_edt.contains("RB");
      isRZ = check_edt.contains("RZ");
      isRL = check_edt.contains("RL");
    }*/
  }
  
  /**
   * Renvoie une chaine avec une longueur fixe complété avec des blancs à droite
   * (maximum 250 caractères blancs)
   * 
   * private void FixeBlancD(int lngfx)
   * {
   * int lng = lngfx-sb.length();
   * 
   * if (lng > 0)
   * sb.append(Constantes.spaces, 0, lng);
   * else
   * sb.delete(lngfx, sb.length());//sb.toString().substring(0, lngfx);
   * }
   */
  
  /**
   * Renvoie une chaine avec une longueur fixe complété avec des blancs à droite
   * (maximum 260 caractères blancs : ATTENTION cause d'exception déjà arrivé sur le RiTextArea de 256 de long alors que
   * le tableau ne faisait que 250)
   */
  public String getFixeBlancD(String str, int lngfx) {
    int lng = lngfx - sb.length() - str.length();
    if (lng > 0) {
      return sb.append(str).append(spaces, 0, lng).toString();
    }
    else {
      return sb.append(str).toString().substring(0, lngfx);
    }
  }
  
  /**
   * Renvoie une chaine avec une longueur fixe complété avec des blancs à gauche
   * (maximum 250 blancs)
   */
  public String getFixeBlancG(String str, int lngfx) {
    int lng = lngfx - sb.length() - str.length();
    
    if (lng > 0) {
      // Constantes.spaces ne fait que 256 char, on instancie un tableau de 2048 à la place
      // return sb.insert(0, Constantes.spaces, 0, lng).append(str).toString();
      Arrays.fill(spaces, ' ');
      return sb.insert(0, spaces, 0, lng).append(str).toString();
    }
    else {
      return sb.append(str).toString().substring(str.length() + lng); // TODO Vérifier si OK
    }
  }
  
  /**
   * Renvoie une chaine avec une longueur fixe complété avec des zeros à gauche
   * (maximum 20 zeros)
   */
  private void FixeZeroG(int lngfx) {
    int lng = lngfx - sb.length();
    
    if (lng > 0) {
      sb.insert(0, zero, 0, lng);
    }
    else {
      sb.delete(lngfx, sb.length());// sb.substring(0, lngfx);
    }
  }
  
  /**
   * Renvoie une chaine avec une longueur fixe complété avec des zeros à gauche
   * (maximum 20 zeros)
   * 
   * private String getFixeZeroG(String str, int lngfx)
   * {
   * int lng = lngfx-sb.length()-str.length();
   * 
   * if (lng > 0)
   * return sb.append(zero, 0, lng).append(str).toString();
   * else
   * return sb.append(str).toString().substring(0, lngfx);
   * }
   */
  
  /**
   * Renvoie une chaine où l'on a supprimé les zeros à gauche
   * (car valeur numérique)
   * 
   * private String getRemoveZeroG()
   * {
   * int j=sb.length(), i=0;
   * 
   * for (i=0; i<j; i++)
   * if ((sb.charAt(i) != '0') && (sb.charAt(i) != ' '))
   * break;
   * return sb.delete(0, i).toString();
   * }
   */
  /**
   * Supprime les zéros de gauche et les blancs
   */
  private void removeZeroG() {
    int j = sb.length();
    int i = 0;
    for (i = 0; i < j; i++) {
      if ((sb.charAt(i) != '0') && (sb.charAt(i) != ' ')) {
        break;
      }
    }
    sb.delete(0, i).toString();
  }
  
  /**
   * Insère le symbole des milliers
   * 
   */
  private void insertMillier() {
    // On contrôle s'il y a le séparateur des décimales
    int dec = sb.lastIndexOf(Constantes.SEPARATEUR_DECIMAL);
    if (dec >= 0) {
      dec = 1;
    }
    else {
      dec = 0;
    }
    // On calcule la première position du séparateur des milliers
    int i = sb.length() - decimal - dec;
    i = i - 3;
    while (i > 0) {
      sb.insert(i, Constantes.SEPARATEUR_MILLIER);
      i = i - 3;
    }
  }
  
  /**
   * Retourne si la chaine contient une valeur négative pour le format 'O'.
   * 
   * @return
   */
  public static boolean isNegatifFormatO(String pValeur) {
    pValeur = Constantes.normerTexte(pValeur);
    if (pValeur.isEmpty()) {
      return false;
    }
    if (pValeur.toString().endsWith("è")) {
      return true;
    }
    return false;
  }
  
  /**
   * Contrôle si le carcatère est un chiffre (0..9, )
   * @return
   */
  public boolean isNumber(char c) {
    if ((c != '0') && (c != '1') && (c != '2') && (c != '3') && (c != '4') && (c != '5') && (c != '6') && (c != '7') && (c != '8')
        && (c != '9')) {
      return false;
    }
    return true;
  }
  
  /**
   * Contrôle si la valeur est bien numérique (0..9, )
   * @return
   */
  public boolean isNumeric() {
    for (int i = 0; i < sb.length(); i++) {
      if ((sb.charAt(i) != '0') && (sb.charAt(i) != '1') && (sb.charAt(i) != '2') && (sb.charAt(i) != '3') && (sb.charAt(i) != '4')
          && (sb.charAt(i) != '5') && (sb.charAt(i) != '6') && (sb.charAt(i) != '7') && (sb.charAt(i) != '8') && (sb.charAt(i) != '9')
          && (sb.charAt(i) != ' ')) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Contrôle si la valeur est bien numérique (0..9, +,-, ,,,.)
   * @return
   */
  public boolean isNumericPlus() {
    for (int i = 0; i < sb.length(); i++) {
      if ((sb.charAt(i) != '0') && (sb.charAt(i) != '1') && (sb.charAt(i) != '2') && (sb.charAt(i) != '3') && (sb.charAt(i) != '4')
          && (sb.charAt(i) != '5') && (sb.charAt(i) != '6') && (sb.charAt(i) != '7') && (sb.charAt(i) != '8') && (sb.charAt(i) != '9')
          && (sb.charAt(i) != ' ') && (sb.charAt(i) != Constantes.SEPARATEUR_DECIMAL_CHAR) && (sb.charAt(i) != '+')
          && (sb.charAt(i) != '-') && (sb.charAt(i) != Constantes.SEPARATEUR_MILLIER_CHAR)) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Transforme un numérique d'un panel en numérique pour variable RPG
   * @return
   */
  public String getRPGNumerique() {
    int i = 0;
    int pos_dec = -1;
    boolean neg = false;
    
    // On recherche la position du séparateur décimal
    pos_dec = sb.lastIndexOf(Character.toString(Constantes.SEPARATEUR_DECIMAL_CHAR));
    neg = ((sb.indexOf("-") != -1) || (sb.lastIndexOf("CR") != -1));
    // On vérifie les décimales
    if (decimal != 0) {
      if (pos_dec == -1) {
        for (i = 0; i < decimal; i++) {
          sb.append('0');
        }
      }
      // avec virgule
      else {
        // Calcul du nbr de décimales manquantes
        int nbr = decimal - (sb.length() - (pos_dec + 1));
        for (i = 0; i < nbr; i++) {
          sb.append('0');
          /*
          for (j=pos_dec+1, i=0; i<decimal; i++)
          {
            if (!isNumber(sb.charAt(j+i))) sb.append('0'); En commentaire à cause du VGVM05FM ZL61 <= 1,12 en crash
          }*/
        }
      }
    }
    // On supprime les caractères non souhaités différent des chiffres
    for (i = 0; i < sb.length(); i++) {
      if (!isNumber(sb.charAt(i))) {
        sb.deleteCharAt(i);
      }
    }
    FixeZeroG(longueur);
    
    // Traitement si négatif
    if (neg) {
      switch (sb.charAt(sb.length() - 1)) {
        case '1':
          sb.setCharAt(sb.length() - 1, 'J');
          break;
        case '2':
          sb.setCharAt(sb.length() - 1, 'K');
          break;
        case '3':
          sb.setCharAt(sb.length() - 1, 'L');
          break;
        case '4':
          sb.setCharAt(sb.length() - 1, 'M');
          break;
        case '5':
          sb.setCharAt(sb.length() - 1, 'N');
          break;
        case '6':
          sb.setCharAt(sb.length() - 1, 'O');
          break;
        case '7':
          sb.setCharAt(sb.length() - 1, 'P');
          break;
        case '8':
          sb.setCharAt(sb.length() - 1, 'Q');
          break;
        case '9':
          sb.setCharAt(sb.length() - 1, 'R');
          break;
        case '0':
          sb.setCharAt(sb.length() - 1, 'è'); // Sous windows en 1252/1147
          break;
      }
    }
    
    return sb.toString();
  }
  
  /**
   * Transforme une chaine formatée en Y (SDA) vers une chaine numérique normale
   * @param str
   * @param lg
   * @param dec
   * @return
   * 
   *         public String getTypeYToNumerique(int lg, int dec)
   *         {
   *         // On commence par virer les 0 à gauche jusqu'à la fin ou plus de 0 ou blanc
   *         removeZeroG();
   *         int longueur = sb.length();
   *         if (((longueur != 0)) && (dec != 0))
   *         {
   *         // Cas où il ne reste plus de zéros devant la virgule
   *         if (longueur == dec)
   *         sb.insert(0, "0"+Constantes.SEPARATEUR_DECIMAL);
   *         else
   *         sb.insert(longueur-dec, Constantes.SEPARATEUR_DECIMAL);
   *         }
   *         //if (sb.length() == 0) sb.insert(0, "0"); Non car affiche le zéro
   * 
   *         return sb.toString();
   *         }
   * 
   *         /**
   *         Transforme une chaine numérique normale vers une chaine formatée en Y (SDA)
   * @param str
   * @param lg
   * @param dec
   * @return
   *         
   *         public String getNumeriqueToTypeY(String val, int lg, int dec)
   *         {
   *         String chaine=null;
   *         int pos=-1;
   *         val = val.trim();
   * 
   *         // Recherche du séparateur décimal
   *         pos = val.indexOf(Constantes.SEPARATEUR_DECIMAL);
   *         if (pos == -1)
   *         pos = val.indexOf(Constantes.SEPARATEUR_DECIMAL);
   * 
   *         // Sans séparateur décimal
   *         if (pos == -1)
   *         {
   *         chaine = sb.append(zero, 0, lg-val.length()-dec).append(val).append(zero, lg-dec, dec).toString();
   *         return chaine;
   *         }
   *         else // Avec séparateur decimal
   *         {
   *         String entier = val.substring(0, pos);
   *         String decimal = val.substring(pos+1);
   *         // On ajuste les décimales
   *         pos = dec - decimal.length();
   *         if (pos == 0)
   *         {
   *         chaine = sb.append(zero, 0, lg-entier.length()-dec).append(entier).append(decimal).toString();
   *         return chaine;
   *         }
   *         else
   *         if (pos > 0)
   *         {
   *         chaine = sb.append(zero, 0, lg-entier.length()-dec).append(entier).append(zero, lg-pos, pos).toString();
   *         return chaine;
   *         }
   *         else
   *         {
   *         chaine = sb.append(zero, 0, lg-entier.length()-dec).append(entier).append(decimal.substring(0,
   *         dec)).toString();
   *         return chaine;
   *         }
   *         }
   *         }
   * 
   *         /**
   *         Transforme une chaine formatée en S (SDA) vers une chaine numérique normale
   *         ATTENTION AU SIGNE A TESTER
   * @param str
   * @param lg
   * @param dec
   * @return
   *         
   *         public String getTypeSToNumerique(int lg, int dec)
   *         {
   *         // On commence par virer les 0 à gauche jusqu'à la fin ou plus de 0 ou blanc
   *         removeZeroG();
   *         int longueur = sb.length();
   *         if (((longueur != 0)) && (dec != 0))
   *         {
   *         // Cas où il ne reste plus de zéros devant la virgule
   *         if (longueur == dec)
   *         sb.insert(0, "0"+Constantes.SEPARATEUR_DECIMAL);
   *         else
   *         sb.insert(longueur-dec, Constantes.SEPARATEUR_DECIMAL);
   *         }
   *         //if (sb.length() == 0) sb.insert(0, "0"); Non car affiche le zéro
   * 
   *         return sb.toString();
   *         }
   * 
   *         /**
   *         Transforme une chaine numérique normale vers une chaine formatée en S (SDA)
   *         ATTENTION AU SIGNE A TESTER
   * @param str
   * @param lg
   * @param dec
   * @return
   *         
   *         public String getNumeriqueToTypeS(String val, int lg, int dec)
   *         {
   *         String chaine=null;
   *         int pos=-1;
   *         val = val.trim();
   * 
   *         // Recherche du séparateur décimal
   *         pos = val.indexOf(Constantes.SEPARATEUR_DECIMAL);
   *         if (pos == -1)
   *         pos = val.indexOf(Constantes.SEPARATEUR_DECIMAL);
   * 
   *         // Sans séparateur décimal
   *         if (pos == -1)
   *         {
   *         chaine = sb.append(zero, 0, lg-val.length()-dec).append(val).append(zero, lg-dec, dec).toString();
   *         return chaine;
   *         }
   *         else // Avec séparateur decimal
   *         {
   *         String entier = val.substring(0, pos);
   *         String decimal = val.substring(pos+1);
   *         // On ajuste les décimales
   *         pos = dec - decimal.length();
   *         if (pos == 0)
   *         {
   *         chaine = sb.append(zero, 0, lg-entier.length()-dec).append(entier).append(decimal).toString();
   *         return chaine;
   *         }
   *         else
   *         if (pos > 0)
   *         {
   *         chaine = sb.append(zero, 0, lg-entier.length()-dec).append(entier).append(zero, lg-pos, pos).toString();
   *         return chaine;
   *         }
   *         else
   *         {
   *         chaine = sb.append(zero, 0, lg-entier.length()-dec).append(entier).append(decimal.substring(0,
   *         dec)).toString();
   *         return chaine;
   *         }
   *         }
   *         }
   */
  /**
   * Analyse la chaine afin de voir si la valeur est négative ou pas
   * 
   */
  private void negatif() {
    // On teste si la valeur est négative
    int lg = sb.length() - 1;
    if (lg <= 0) {
      negatif = false;
      return;
    }
    char derniercar = sb.charAt(lg);
    
    if (derniercar == '-') {
      negatif = true;
      sb.deleteCharAt(lg);
    }
    else if (!isNumber(derniercar)) {
      negatif = true;
      // Valeur négative
      switch (derniercar) {
        case 'J':
          sb.setCharAt(lg, '1');
          break;
        case 'K':
          sb.setCharAt(lg, '2');
          break;
        case 'L':
          sb.setCharAt(lg, '3');
          break;
        case 'M':
          sb.setCharAt(lg, '4');
          break;
        case 'N':
          sb.setCharAt(lg, '5');
          break;
        case 'O':
          sb.setCharAt(lg, '6');
          break;
        case 'P':
          sb.setCharAt(lg, '7');
          break;
        case 'Q':
          sb.setCharAt(lg, '8');
          break;
        case 'R':
          sb.setCharAt(lg, '9');
          break;
        case 'è':
          sb.setCharAt(lg, '0');
          break;
        default: // car code non significatif ('è' sous Windows, à voir pour Linux & Mac)
          // TODO si la lettre est la même alors remplacer les cases par une hashmap
          sb.setCharAt(lg, '0');
      }
    }
  }
  
  /**
   * Formate une chaine numérique (RPG) en fonction du code d'édition
   * @param str
   * @param lg
   * @param dec
   * @return
   */
  public String getEdtNumerique(char input_ctrl) {
    // On controle si c'est négatif
    negatif();
    
    // On Supprime les zéros de gauche
    removeZeroG(); // On les enlève pour tous les cas (si soucis il faudra la mettre dans chacune des getEdt_)
    
    // On sélèctionne le bon formatage
    switch (output_edt) {
      case 'A':
        return getEdt_A();
      case 'B':
        return getEdt_B();
      case 'C':
        return getEdt_C();
      case 'D':
        return getEdt_D();
      case 'J':
        return getEdt_J();
      case 'K':
        return getEdt_K();
      case 'L':
        return getEdt_L();
      case 'M':
        return getEdt_M();
      case 'N':
        return getEdt_N();
      case 'O':
        return getEdt_O();
      case 'P':
        return getEdt_P();
      case 'Q':
        return getEdt_Q();
      case 'W':
        return getEdt_W();
      case 'Y':
        return getEdt_Y();
      case 'Z':
        return getEdt_Z();
      case '1':
        return getEdt_1();
      case '2':
        return getEdt_2();
      case '3':
        return getEdt_3();
      case '4':
        return getEdt_4();
      case '#':
        return getEdt_AngloSaxon();
    }
    
    return sb.toString();
  }
  
  /**
   * Formate une chaine numérique au format A (SDA) - Format type comptable ex: 1.123,05CR, 0 affiché
   * @return
   */
  private String getEdt_A() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      if (decimal == 0) {
        return "0  ";
      }
      else {
        return '0' + Constantes.SEPARATEUR_DECIMAL + "00  ";
      }
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    // On insère le séparateur de millier
    insertMillier();
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.append("CR").toString();
    }
    else {
      return sb.append("  ").toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format B (SDA) - Format type comptable ex: 1.123,05CR, 0 non affiché
   * @return
   */
  private String getEdt_B() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      return "";
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    // On insère le séparateur de millier
    insertMillier();
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.append("CR").toString();
    }
    else {
      return sb.append("  ").toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format C (SDA) - Format type comptable ex: 1123,50CR, 0 affiché
   * @return
   */
  private String getEdt_C() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      if (decimal == 0) {
        return "0  ";
      }
      else {
        return '0' + Constantes.SEPARATEUR_DECIMAL + "00  ";
      }
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.append("CR").toString();
    }
    else {
      return sb.append("  ").toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format D (SDA) - Format type comptable ex: 1123,50CR, 0 non affiché
   * @return
   */
  private String getEdt_D() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      return "";
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.append("CR").toString();
    }
    else {
      return sb.append("  ").toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format J (SDA) - Format type comptable ex: 1.123,05-, 0 affiché
   * @return
   */
  private String getEdt_J() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      if (decimal == 0) {
        return "0 ";
      }
      else {
        return '0' + Constantes.SEPARATEUR_DECIMAL + "00 ";
      }
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    // On insère le séparateur de millier
    insertMillier();
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.append('-').toString();
    }
    else {
      return sb.append(' ').toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format K (SDA) - Format type comptable ex: 1.123,05-, 0 non affiché
   * @return
   */
  private String getEdt_K() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      return "";
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    // On insère le séparateur de millier
    insertMillier();
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.append('-').toString();
    }
    else {
      return sb.append(' ').toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format L (SDA) - Format type comptable ex: 1123,50-, 0 affiché
   * @return
   */
  private String getEdt_L() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      if (decimal == 0) {
        return "0 ";
      }
      else {
        return '0' + Constantes.SEPARATEUR_DECIMAL + "00 ";
      }
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.append('-').toString();
    }
    else {
      return sb.append(' ').toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format M (SDA) - C'est de l'alpha avec numérique uniquement en saisie
   * @return
   */
  /*
  private String getEdt_M()
  {
    return sb.toString().trim();
  }
  */
  private String getEdt_M() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      return "";
    }
    else {
      // On insère le séparateur décimal
      if (decimal != 0) {
        while (sb.length() <= decimal) {
          sb.insert(0, '0');
        }
        sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
      }
    }
    // Fignolage avant renvoi
    if (negatif) {
      return sb.append('-').toString();
    }
    else {
      return sb.append(' ').toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format N (SDA) - Format type comptable ex: -1.123,05, 0 affiché
   * @return
   */
  private String getEdt_N() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      if (decimal == 0) {
        return "0";
      }
      else {
        return '0' + Constantes.SEPARATEUR_DECIMAL + "00";
      }
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    // On insère le séparateur de millier
    insertMillier();
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.insert(0, '-').toString();
    }
    else {
      return sb.toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format O (SDA) - Format type comptable ex: -1.123,05, 0 non affiché
   * @return
   */
  private String getEdt_O() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      return "";
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    // On insère le séparateur de millier
    insertMillier();
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.insert(0, '-').toString();
    }
    else {
      return sb.toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format P (SDA) - Format type comptable ex: -1123,50, 0 affiché
   * @return
   */
  private String getEdt_P() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      if (decimal == 0) {
        return "0";
      }
      else {
        return '0' + Constantes.SEPARATEUR_DECIMAL + "00";
      }
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    
    // Fignolage avant renvoi
    if (negatif) {
      return sb.insert(0, '-').toString();
    }
    else {
      return sb.toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format Q (SDA) - Format type comptable ex: -1123,50, 0 non affiché
   * @return
   */
  private String getEdt_Q() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      return "";
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      if (sb.indexOf(Constantes.SEPARATEUR_DECIMAL) == -1) {
        sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL_CHAR);
      }
    }
    // Fignolage avant renvoi
    if (negatif) {
      return sb.insert(0, '-').toString();
    }
    else {
      return sb.toString();
    }
  }
  
  /**
   * Formate une chaine numérique au format W (SDA) - Format type comptable ex: XXXX/XX/XX
   * @return
   */
  private String getEdt_W() {
    while (sb.length() < 5) {
      sb.insert(0, '0');
    }
    sb.insert(sb.length() - 2, Constantes.SEPARATEUR_DATE_CHAR).insert(sb.length() - 5, Constantes.SEPARATEUR_DATE_CHAR);
    
    return sb.toString();
  }
  
  /**
   * Formate une chaine numérique au format Y (SDA) - Format type comptable ex: XX/XX/XXXX ou XX/XX/XX
   * @return
   */
  private String getEdt_Y() {
    while (sb.length() < longueur) {
      sb.insert(0, '0');
    }
    // Cas date XX/XX/XX
    if (longueur == 6) {
      sb.insert(sb.length() - 2, Constantes.SEPARATEUR_DATE_CHAR).insert(2, Constantes.SEPARATEUR_DATE_CHAR);
    }
    else {
      sb.insert(sb.length() - 4, Constantes.SEPARATEUR_DATE_CHAR).insert(2, Constantes.SEPARATEUR_DATE_CHAR);
    }
    
    return sb.toString();
  }
  
  /**
   * Formate une chaine numérique au format Z (SDA) - Format type comptable ex: 123400 (6 dt 2), 0 non affiché à gauche
   * (pas de signe)
   * @return
   */
  private String getEdt_Z() {
    // Si la valeur numérique vaut 0
    int lg = sb.length();
    if (lg == 0) {
      return "";
    }
    
    return sb.toString();
  }
  
  /**
   * Formate une chaine numérique au format 1 (SDA) - Format type comptable ex: 1.123,05, 0 affiché
   * @return
   */
  private String getEdt_1() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      if (decimal == 0) {
        return "0";
      }
      else {
        return '0' + Constantes.SEPARATEUR_DECIMAL + "00";
      }
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    // On insère le séparateur de millier
    insertMillier();
    
    return sb.toString();
  }
  
  /**
   * Formate une chaine numérique au format 2 (SDA) - Format type comptable ex: 1.123,05, 0 non affiché
   * @return
   */
  private String getEdt_2() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      return "";
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    // On insère le séparateur de millier
    insertMillier();
    
    return sb.toString();
  }
  
  /**
   * Formate une chaine numérique au format 3 (SDA) - Format type comptable ex: 1123,50, 0 affiché
   * @return
   */
  private String getEdt_3() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      if (decimal == 0) {
        return "0";
      }
      else {
        return '0' + Constantes.SEPARATEUR_DECIMAL + "00";
      }
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    
    return sb.toString();
  }
  
  /**
   * Formate une chaine numérique au format 4 (SDA) - Format type comptable ex: 1123,50, 0 non affiché
   * @return
   */
  private String getEdt_4() {
    // Si la valeur numérique vaut 0
    if (sb.length() == 0) {
      return "";
    }
    // On insère le séparateur décimal
    else if (decimal != 0) {
      while (sb.length() <= decimal) {
        sb.insert(0, '0');
      }
      sb.insert(sb.length() - decimal, Constantes.SEPARATEUR_DECIMAL);
    }
    
    return sb.toString();
  }
  
  /**
   * Formate une chaine numérique anglosaxon: -1123.50, 0 affiché
   * @return
   */
  private String getEdt_AngloSaxon() {
    String chaine = getEdt_Q().replace(Constantes.SEPARATEUR_DECIMAL_CHAR, '.');
    return chaine.equals("") ? "0" : chaine;
  }
  
}
