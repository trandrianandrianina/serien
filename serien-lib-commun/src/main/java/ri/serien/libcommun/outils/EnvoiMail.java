/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EnvoiMail {
  private Session mailSession = null;
  private Properties props = null;
  private InternetAddress recipientTo = null;
  private InternetAddress recipientFrom = null;
  private Message message = null;
  private String smtp_host = null;
  private int smtp_port = 0;
  private String profil_mail_envoi = null;
  private String mp_mail_envoi = null;
  
  /**
   * Envoyer un mail avec une adresse mail le sujet et le contenu. basé sur les paramètres SMTP du serveur
   */
  public void envoyerUnMail(String adresseDest, String sujet, String contenuMessage, String smtp, String port, String adresseExpe,
      String mpExpe, boolean isDEBUG) {
    if (adresseDest == null || smtp == null || adresseExpe == null || mpExpe == null) {
      return;
    }
    
    smtp_host = smtp;
    try {
      smtp_port = Integer.parseInt(port);
    }
    catch (Exception e) {
    }
    
    profil_mail_envoi = adresseExpe;
    mp_mail_envoi = mpExpe;
    
    try {
      // On met en place les propriétés de base
      props = new Properties();
      props.put("mail.smtp.host", smtp_host);
      props.put("mail.smtp.port", smtp_port);
      props.put("mail.smtp.auth", true);
      
      if (mailSession == null) {
        mailSession = Session.getDefaultInstance(props, new Authenticator() {
          @Override
          protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(profil_mail_envoi, mp_mail_envoi.trim());
          }
        });
      }
      
      message = new MimeMessage(mailSession);
      
      mailSession.setDebug(isDEBUG);
      
      recipientTo = new InternetAddress(adresseDest);
      recipientFrom = new InternetAddress(profil_mail_envoi);
      
      message.setRecipient(Message.RecipientType.TO, recipientTo);
      message.setFrom(recipientFrom);
      
      message.setSubject(sujet);
      message.setContent(contenuMessage, "text/html");
      
      Transport.send(message);
      
    }
    catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    catch (AddressException e) {
      e.printStackTrace();
    }
    catch (MessagingException e) {
      e.printStackTrace();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
}
