/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import ri.serien.libcommun.exploitation.edition.ConstantesNewSim;

/**
 * Gestion des fichiers zip (en chantier).
 */
public class GestionFichierZip {
  // Constantes erreurs de chargement
  // private static final String ERREUR_FICHIER_INTROUVABLE = "Le fichier est introuvable.";
  // private static final String ERREUR_LECTURE_FICHIER = "Erreur lors de la lecture du fichier.";
  
  // Variables
  private FileNG nomFichier = null;
  protected byte[] contenuFichier = null;
  private ZipOutputStream ficZIP = null;
  private List<String> filesListInDir = new ArrayList<String>();
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit
  
  // et non lu
  
  /**
   * Initialise le nom du fichier.
   */
  public void setNomFichier(String nomFichier) {
    if (nomFichier != null) {
      this.nomFichier = new FileNG(nomFichier);
    }
  }
  
  /**
   * Retourne le nom du fichier.
   */
  public String getNomFichier() {
    return nomFichier.getAbsolutePath();
  }
  
  /**
   * Créé l'archive vide.
   * 
   * @param taux de compression de 1 à 9.
   */
  public boolean createZip(int taux, File nomFichier) {
    // Déclaration de l'archive ZIP
    if ((taux < 0) || (taux > 9)) {
      taux = Deflater.BEST_COMPRESSION;
    }
    try {
      ficZIP = new ZipOutputStream(new FileOutputStream(nomFichier));
      // Méthode de compression DEFLATED ou STORED
      ficZIP.setMethod(ZipOutputStream.DEFLATED);
      // Niveau de compression
      // de 1 (NO_COMPRESSION) à 9 (BEST_COMPRESSION)
      ficZIP.setLevel(taux);
      // Fermeture de l'archive et des flux
      ficZIP.flush();
      // ficZIP.close();
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  private void listFilesDir(File dir) throws IOException {
    File[] files = dir.listFiles();
    
    for (File file : files) {
      if (file.isFile()) {
        filesListInDir.add(file.getAbsolutePath());
      }
      else {
        listFilesDir(file);
      }
    }
  }
  
  /**
   * Ajoute un repertoire à l'archive zip.
   * @param dir = nom du dossier qu'il faut zipper
   * @param zipDirName = nom du dossier zip
   */
  public void zipDirectory(File dir, String zipDirName) {
    try {
      
      listFilesDir(dir);
      // now zip files one by one
      // create ZipOutputStream to write to the zip file
      FileOutputStream fos = new FileOutputStream(zipDirName);
      ZipOutputStream zos = new ZipOutputStream(fos);
      for (String filePath : filesListInDir) {
        
        // for ZipEntry we need to keep only relative file path, so we used substring on absolute path
        ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length() + 1, filePath.length()));
        zos.putNextEntry(ze);
        
        // read the file and write to ZipOutputStream
        FileInputStream fis = new FileInputStream(filePath);
        
        byte[] buffer = new byte[1024];
        int len;
        while ((len = fis.read(buffer)) > 0) {
          zos.write(buffer, 0, len);
          
        }
        zos.closeEntry();
        fis.close();
      }
      zos.close();
      fos.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Ajout d'un fichier dans l'archive.
   * 
   * @param entreeFichier nom du fichier à zipper.
   */
  public boolean addFichier(String entreeFichier) {
    File file = new File(entreeFichier);
    return addFichier(entreeFichier, file.getName());
  }
  
  /**
   * Ajout d'un fichier dans l'archive.²
   * 
   * @param entreeFichier nom du fichier à zipper.
   * @param nameinthezip nom du fichier que l'on souhaite dans le zip (cas d'un renommage).
   */
  public boolean addFichier(String entreeFichier, String nameinthezip) {
    if (entreeFichier == null) {
      return false;
    }
    File fichier = new File(entreeFichier);
    if (!fichier.exists()) {
      return false;
    }
    
    try {
      ZipEntry entreeZIP = new ZipEntry(nameinthezip);
      ficZIP.putNextEntry(entreeZIP);
      // Envoie du contenu de la premiere entrée dans l'archive à travers
      // un flux
      DataOutputStream ficDonnees = new DataOutputStream(new BufferedOutputStream(ficZIP));
      FileInputStream fin = new FileInputStream(entreeFichier);
      
      byte[] buffer = new byte[ConstantesNewSim.TAILLE_BUFFER];
      while (true) {
        int bytesRead = fin.read(buffer);
        if (bytesRead == -1) {
          break;
        }
        ficDonnees.write(buffer, 0, bytesRead);
      }
      ficDonnees.close();
      fin.close();
      // Fermeture de l'archive jusqu'à la prochaîne entrée
      // ficZIP.closeEntry();
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  /**
   * Lister le contenu d'un fichier zip.
   */
  public void listZip() {
    try {
      // Open the ZIP file
      ZipFile zf = new ZipFile(nomFichier);
      // Enumerate each entry
      for (Enumeration<?> entries = zf.entries(); entries.hasMoreElements();) {
        ((ZipEntry) entries.nextElement()).getName();
      }
      zf.close();
    }
    catch (Exception e) {
      // @todo Gérer correctement l'exception.
    }
  }
  
  /**
   * Supprime un fichier dans l'archive.
   */
  public boolean removeFichier(String entreeFichier) {
    if (entreeFichier == null) {
      return false;
    }
    File fichier = new File(entreeFichier);
    if (!fichier.exists()) {
      return false;
    }
    
    try {
      ZipEntry entreeZIP = new ZipEntry(fichier.getName());
      ficZIP.putNextEntry(entreeZIP);
      // Envoie du contenu de la premiere entrée dans l'archive à travers
      // un flux
      DataOutputStream ficDonnees = new DataOutputStream(new BufferedOutputStream(ficZIP));
      FileInputStream fin = new FileInputStream(entreeFichier);
      
      byte[] buffer = new byte[ConstantesNewSim.TAILLE_BUFFER];
      while (true) {
        int bytesRead = fin.read(buffer);
        if (bytesRead == -1) {
          break;
        }
        ficDonnees.write(buffer, 0, bytesRead);
      }
      ficDonnees.close();
      fin.close();
      // Fermeture de l'archive jusqu'à la prochaîne entrée
      // ficZIP.closeEntry();
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  /**
   * Ajouter un fichier à un fichier zip.
   */
  public boolean addFilesToExistingZip(File zipFile, String filename, boolean fullPath, String pathFolderInZip) throws IOException {
    // On crée l'objet File représentant le fichier à insérer dans le ZIP :
    final File file = new File(pathFolderInZip + File.separator + filename);
    
    // On détermine le nom de l'entré ZIP selon le paramètre 'fullPath' :
    // final String entryName = fullPath ? filename : file.getName();
    final String entryName = fullPath ? filename : file.getName();
    
    // On crée le fichier temporaire de travail (dans le même répertoire que
    // le ZIP)
    final File tmpFile = File.createTempFile("tmp", ".zip", zipFile.getParentFile());
    
    try {
      // On ouvre le fichier temporaire en ecriture :
      final ZipOutputStream output = new ZipOutputStream(new FileOutputStream(tmpFile));
      try {
        final byte[] buf = new byte[8192];
        int len;
        
        // On ouvre le fichier ZIP en lecture :
        final ZipInputStream input = new ZipInputStream(new FileInputStream(zipFile));
        try {
          ZipEntry entry;
          // Pour chaque fichier du ZIP :
          while ((entry = input.getNextEntry()) != null) {
            // Si le nom est différent de celui du fichier à ajouter
            // :
            // if (!entryName.equals(entry.getName()))
            // {
            
            // On recopie le fichier dans le fichier ZIP
            // temporaire :
            // output.putNextEntry(new ZipEntry(entry.getName()));
            output.putNextEntry(new ZipEntry(entry.getName()));
            
            while ((len = input.read(buf)) > 0) {
              output.write(buf, 0, len);
            }
            // }
          }
        }
        finally {
          input.close();
        }
        
        // Puis on ouvre le fichier pour l'ajouter au ZIP :
        
        final FileInputStream fis = new FileInputStream(file);
        
        try {
          output.putNextEntry(new ZipEntry(entryName));
          while ((len = fis.read(buf)) > 0) {
            output.write(buf, 0, len);
          }
        }
        finally {
          fis.close();
        }
      }
      finally {
        output.close();
      }
      
      // Si on arrive ici c'est que tout s'est bien passé
      // => On supprime le fichier ZIP pour le remplacer par le fichier
      // temporaire :
      if (!(zipFile.delete() && tmpFile.renameTo(zipFile))) {
        throw new IOException("Unable to replace " + zipFile);
      }
    }
    finally {
      // On force la suppression du fichier temporaire s'il existe encore
      // (en cas d'erreur)
      tmpFile.delete();
    }
    return true;
  }
  
  /**
   * Décompresse le fichier zip dans le répertoire donné.
   * 
   * @param folder le répertoire où les fichiers seront extraits
   * @param zipfile le fichier zip à décompresser
   */
  public static boolean unzip(File zipfile, File folder) {
    try {
      
      // création de la ZipInputStream qui va servir à lire les données du fichier zip
      ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipfile.getCanonicalFile())));
      
      // extractions des entrées du fichiers zip (i.e. le contenu du zip)
      ZipEntry ze = null;
      try {
        while ((ze = zis.getNextEntry()) != null) {
          
          // Pour chaque entrée, on crée un fichier
          // dans le répertoire de sortie "folder"
          File f = new File(folder.getCanonicalPath(), ze.getName());
          
          // Si l'entrée est un répertoire,
          // on le crée dans le répertoire de sortie
          // et on passe à l'entrée suivante (continue)
          if (ze.isDirectory()) {
            f.mkdirs();
            continue;
          }
          
          // L'entrée est un fichier, on crée une OutputStream
          // pour écrire le contenu du nouveau fichier
          f.getParentFile().mkdirs();
          OutputStream fos = new BufferedOutputStream(new FileOutputStream(f));
          
          // On écrit le contenu du nouveau fichier
          // qu'on lit à partir de la ZipInputStream
          // au moyen d'un buffer (byte[])
          try {
            try {
              final byte[] buf = new byte[8192];
              int bytesRead;
              while (-1 != (bytesRead = zis.read(buf))) {
                fos.write(buf, 0, bytesRead);
              }
            }
            finally {
              fos.close();
            }
          }
          catch (final IOException ioe) {
            // en cas d'erreur on efface le fichier
            f.delete();
            ioe.printStackTrace();
            return false;
          }
        }
      }
      finally {
        // fermeture de la ZipInputStream
        zis.close();
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
