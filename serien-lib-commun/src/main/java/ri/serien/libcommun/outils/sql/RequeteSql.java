/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.sql;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;

/**
 * Permet de construire, formater et générer une requête SQL.
 * 
 * 
 * Idées :
 * Dès l'appel du conntructeur de la requête, on précise si la requête à opbtenir est un SELECT, un UPDATE, un INSERT ou un DELETE.
 * Il faudrait pourvoir gérer des sous conditions cad des conditions entre paranthèses. Ex: ... and ((champ1 = X) or (champ2 > 0)) and ...
 * Améliorer la méthode ajouterPlageDateConditionAnd comme expliqué dans le TODO de l'entête.
 */
public class RequeteSql {
  private StringBuilder requete = new StringBuilder();
  private LinkedHashMap<String, Object> listeGroupe = new LinkedHashMap<String, Object>();
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Traite les caractères spéciaux des Strings à injecter en SQL
   * @param pTexteOrigine Texte à traiter.
   * @return La chaine de caractères traitée.
   */
  public static String traiterCaracteresSpeciauxSQL(String pTexteOrigine) {
    if (pTexteOrigine == null) {
      return null;
    }
    
    return pTexteOrigine.replace("'", "''");
  }
  
  /**
   * Formate les Strings à injecter en SQL.
   * Si null, la valeur est transformée en "" car les champs de type char sont rarement initialisés avec null.
   * Si la longueur max vaut -1 alors aucun contrôle de longueur.
   * @param pTexte Texte à formater.
   * @param pTrim True pour trim la chaine de caractères, false sinon.
   * @param pLongueurMax Longueur max de la chaine de caractères, -1 pour ne pas contrôler.
   * @return La chaine de caractères formatée.
   */
  public static String formaterStringSQL(String pTexte, boolean pTrim, int pLongueurMax) {
    if (pTexte == null) {
      return "''";
    }
    
    // Suppression des espaces
    if (pTrim) {
      pTexte = pTexte.trim();
    }
    
    // Contrôle des caractères spéciaux
    pTexte = traiterCaracteresSpeciauxSQL(pTexte);
    
    // Contrôle de la longueur maximum
    if (pLongueurMax > 0 && pTexte.length() > pLongueurMax) {
      pTexte = pTexte.substring(0, pLongueurMax);
    }
    
    return "'" + pTexte + "'";
  }
  
  /**
   * Formate les Strings à injecter en SQL.
   * Si null, la valeur est transformée en "" car les champs de type char sont rarement initialisés avec null.
   * @param pTexte Chaine de caractère à formater.
   * @param pTrim True si la chaîne doit être trim, false sinon.
   * @return La chaine de caractères à injecter en SQL.
   */
  public static String formaterStringSQL(String pTexte, boolean pTrim) {
    return formaterStringSQL(pTexte, pTrim, -1);
  }
  
  /**
   * Formate les Strings à injecter en SQL.
   * @param pTexte La chaine de caractères à formater.
   * @return La chaîne de caractères formatée à injecter
   */
  public static String formaterStringSQL(String pTexte) {
    return formaterStringSQL(pTexte, false, -1);
  }
  
  /**
   * Formate les char en Strings à injecter en SQL.
   * @param Le Char à formater en String.
   * @return Le String à injecter en SQL.
   */
  public static String formaterCharacterSQL(Character pTexteOrigine) {
    if (pTexteOrigine == null) {
      return null;
    }
    
    return "'" + pTexteOrigine + "'";
  }
  
  /**
   * Formate les Timestamp à injecter en SQL.
   * @param pDate La date à transformer en timestamp.
   * @return Le timestamp à injecter en SQL.
   */
  public static String formaterTimestampSQL(Date pDate) {
    if (pDate == null) {
      return null;
    }
    return "TIMESTAMP('" + DateHeure.getJourHeure(DateHeure.AAAA_MM_JJ_HH_MM_SS, pDate) + "')";
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Effacer la requête en cours et les groupes.
   */
  public void effacerRequete() {
    requete.setLength(0);
    listeGroupe.clear();
  }
  
  /**
   * Retourner la requête générée.
   * @return La requete au format String.
   * 
   */
  public String getRequete() {
    return requete.toString().trim();
  }
  
  /**
   * Retourner la requête avec l'option lecture seule qui permet d'optimiser l'exécution.
   * @return La requete au format String.
   */
  public String getRequeteLectureSeule() {
    String chaine = requete.toString().trim().toLowerCase();
    if (chaine.startsWith("select") && !chaine.endsWith("for read only")) {
      requete.append(" for read only");
    }
    return requete.toString().trim();
  }
  
  /**
   * Ajouter un élément à la fin de la requête.
   * 
   * Pour l'instant, cette méthode est surtout utilisée pour ajouter la partie SELECT et FROM de la requête. A terme, cette classe sera
   * également outillée pour construire le SELECT et le WHERE.
   * Exemple :
   * ajouter("SELECT FLID, FLIDF, FLETB, FLCOD, FLSNS FROM " + queryManager.getLibrary() + "." + ListeTablesDb2.FLUX + " WHERE")
   * @param pTexte L'élément à ajouter.
   */
  public void ajouter(String pTexte) {
    requete.append(' ').append(Constantes.normerTexte(pTexte));
  }
  
  /**
   * Supprimer le dernier opérateur donné.
   *
   * Exemple :
   * requete.supprimerDernierOperateur("or");
   * requete.supprimerDernierOperateur("where");
   * @param pOperateur L'opérateur à supprimer.
   */
  public void supprimerDernierOperateur(String pOperateur) {
    pOperateur = Constantes.normerTexte(pOperateur).toLowerCase();
    if (pOperateur.isEmpty()) {
      return;
    }
    int pos = requete.toString().trim().toLowerCase().lastIndexOf(' ' + pOperateur);
    if (pos >= 0) {
      requete.delete(pos + 1, requete.length());
    }
  }
  
  /**
   * Ajouter une instruction SET pour les requêtes de type UPDATE avec formatage des chaînes et caractères.
   * @param pNomChamp Nom du champ à mettre à jour.
   * @param pValeur Nouvelle valeur. Les espaces avant et après la valeur sont retirés. Si la valeur est une String, la méthode traite les
   *          caractères spéciaux et ajoute des apostrophes avant et après. Si la valeur est un Character, la méthode ajoute des
   *          apostrophes avant et après.
   */
  public void ajouterValeurUpdate(String pNomChamp, Object pValeur) {
    ajouterValeurUpdate(pNomChamp, pValeur, false);
  }
  
  /**
   * Ajouter une instruction SET pour les requêtes de type UPDATE (avec ou sans formatage des chaînes et caractères).
   * @param pNomChamp Nom du champ à mettre à jour.
   * @param pValeur Nouvelle valeur. Les espaces avant et après la valeur sont retirés. Si la valeur est une String, la méthode traite les
   *          caractères spéciaux.
   * @param pSansFormatage Si false, ajoute des apostrophes avant et après les chaînes de caractères et les caractères simples. Si true,
   *          ne modifie rien, le contenu de la valeur doit être correctement formaté car elle ne sera pas modifiée.
   */
  public void ajouterValeurUpdate(String pNomChamp, Object pValeur, boolean pSansFormatage) {
    String chaine = retournerAffectationValeur(pNomChamp, pValeur, pSansFormatage);
    if (chaine.trim().isEmpty()) {
      return;
    }
    if (!isVirguleEstPresente(requete.toString(), "set")) {
      requete.append(',');
    }
    requete.append(' ').append(chaine);
  }
  
  /**
   * Ajouter une condition de type AND (avec formatage et en tenant compte des accents dans la comparaison).
   * Idée : Vérifier l'opérateur fourni.
   * @param pNomChamp Nom du champ à comparer.
   * @param pSymbole Opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   * @param pValeur Valeur à comparer. Les apostrophes sont ajoutés automatiquement autour des chaînes et caractères.
   */
  public void ajouterConditionAnd(String pNomChamp, String pSymbole, Object pValeur) {
    ajouterConditionAnd(pNomChamp, pSymbole, pValeur, false, false);
  }
  
  /**
   * Ajouter une condition de type AND.
   * @param pNomChamp Nom du champ à comparer.
   * @param pSymbole Opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   * @param pValeur Valeur à comparer.
   * @param pSansFormatage true=n'ajoute pas d'apostrophes aux chaînes de caractères et caractères.
   * @param pIgnorerAccent true=la requête est modifiée afin d'intégrer une conversion de chaînes de caractères pour rendre la comparaison
   *          insensible aux accents.
   */
  public void ajouterConditionAnd(String pNomChamp, String pSymbole, Object pValeur, boolean pSansFormatage, boolean pIgnorerAccent) {
    String condition = retournerCondition(pNomChamp, pSymbole, pValeur, pSansFormatage, pIgnorerAccent);
    if (condition.trim().isEmpty()) {
      return;
    }
    if (!isOperateurEstPresent(requete.toString())) {
      requete.append(" and");
    }
    requete.append(' ').append(condition);
  }
  
  /**
   * Ajouter une condition de type AND pour une liste de valeurs.
   * Les différents tests seront séparés par un OR. Des parenthèses seront ajoutées si la liste comporte plus d'une valeur.
   * @param pNomChamp Nom du champ à comparer.
   * @param pSymbole Opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   *          pListeValeur Liste de valeurs auxquelles il faut comparer le champ.
   */
  public void ajouterConditionAnd(String pNomChamp, String pSymbole, List<?> pListeValeur) {
    String condition = retournerCondition(pNomChamp, pSymbole, pListeValeur);
    if (!condition.trim().isEmpty()) {
      if (!isOperateurEstPresent(requete.toString())) {
        requete.append(" and");
      }
      requete.append(' ').append(condition);
    }
  }
  
  /**
   * Ajouter une condition de type AND pour une date.
   * La comparaison peut se faire sur le timestamp complet pour une date exacte ou juste sur le jour (en ignorant l'heure).
   * @param pNomChamp Nom du champ à comparer.
   * @param pSymbole Opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   * @param pValeur Date à comparer.
   * @param pIgnorerHeure Ignore l'heure dans la comparaison de dates.
   */
  public void ajouterDateConditionAnd(String pNomChamp, String pSymbole, Date pValeur, boolean pIgnorerHeure) {
    if (pValeur == null) {
      Trace.alerte("La date à ajouter à la condition est invalide.");
      return;
    }
    pNomChamp = Constantes.normerTexte(pNomChamp);
    if (pNomChamp.isEmpty()) {
      Trace.alerte("Le nom du champ correspondant à la date pour la condition est invalide.");
      return;
    }
    
    // Permet de ne comparer que les jours, mois et années (et ignore les heures)
    if (pIgnorerHeure) {
      pNomChamp = "date(" + pNomChamp + ")";
      String dateFormatee = DateHeure.getJourHeure(DateHeure.AAAA_MM_JJ, pValeur);
      ajouterConditionAnd(pNomChamp, pSymbole, dateFormatee, false, false);
    }
    // Permet de ne comparer que les dates exactes (en ignorant les millisecondes car l'AS400 en stocke 6 et java en retourne 3)
    // Il faudra creuser un jour en vue d'une optimisation ou si c'est nécessaire
    else {
      pNomChamp = "Varchar_Format(" + pNomChamp + ", 'YYYY-MM-DD hh24:mi:ss')";
      String dateFormatee = DateHeure.getJourHeure(DateHeure.AAAA_MM_JJ_HH_MM_SS, pValeur);
      ajouterConditionAnd(pNomChamp, pSymbole, dateFormatee, false, false);
    }
  }
  
  /**
   * Ajouter une condition de type AND pour une plage de dates.
   * @param pNomChamp Nom du champ à comparer.
   * @param pValeurDebut Date de début à comparer.
   * @param pValeurFin Date de fin à comparer.
   * @param pIgnorerHeure Ignore l'heure dans la comparaison de dates.
   *          TODO: il faut remplacer pIgnorerHeure par un enum qui permettra de choisir entre : timestamp_date_heure,
   *          timestamp_date, date_serien_SAAMM
   */
  public void ajouterPlageDateConditionAnd(String pNomChamp, Date pValeurDebut, Date pValeurFin, boolean pIgnorerHeure) {
    // Date de début
    if (pValeurDebut != null) {
      ajouterDateConditionAnd(pNomChamp, ">=", pValeurDebut, pIgnorerHeure);
    }
    // Date de fin
    if (pValeurFin != null) {
      ajouterDateConditionAnd(pNomChamp, "<=", pValeurFin, pIgnorerHeure);
    }
  }
  
  /**
   * Ajouter une condition de type AND pour une plage de dates.
   * @param pNomChamp Nom du champ à comparer.
   * @param pValeurDebut Date de début à comparer au format SAAMMJJ (format Série N db2).
   * @param pValeurFin Date de fin à comparer au format SAAMMJJ (format Série N db2).
   * @param pIgnorerHeure Ignore l'heure dans la comparaison de dates.
   *          TODO: il faut remplacer pIgnorerHeure par un enum qui permettra de choisir entre : timestamp_date_heure,
   *          timestamp_date, date_serien_SAAMM
   */
  public void ajouterPlageDateConditionAnd(String pNomChamp, Integer pValeurDebut, Integer pValeurFin) {
    // Date de début
    if (pValeurDebut != null) {
      ajouterConditionAnd(pNomChamp, ">=", pValeurDebut);
    }
    // Date de fin
    if (pValeurFin != null) {
      ajouterConditionAnd(pNomChamp, "<=", pValeurFin);
    }
  }
  
  /**
   * Ajouter une condition de type AND pour contrôler si un champ est NULL ou non.
   * @param pNomChamp Le nom du champ à comparer.
   * @param pIsNull True si on recherche les null, false si on recherche les non null.
   */
  public void ajouterConditionNullAnd(String pNomChamp, boolean pIsNull) {
    pNomChamp = Constantes.normerTexte(pNomChamp);
    if (pNomChamp.isEmpty()) {
      Trace.alerte("Le nom du champ correspondant à la date pour la condition est invalide.");
      return;
    }
    if (!isOperateurEstPresent(requete.toString())) {
      requete.append(" and");
    }
    if (pIsNull) {
      requete.append(" " + pNomChamp + " IS NULL");
      return;
    }
    requete.append(" " + pNomChamp + " IS NOT NULL");
  }
  
  /**
   * Ajouter une condition de type OR pour un champ donné et une valeur.
   * La valeur si c'est une String sera obligatoirement formatée cad qu'elle sera encapsulée par des apostrophes.
   * @param pNomChamp Nom du champ à comparer.
   * @param pSymbole Opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   * @param pValeur Valeur à comparer.
   */
  public void ajouterConditionOr(String pNomChamp, String pSymbole, Object pValeur) {
    ajouterConditionOr(pNomChamp, pSymbole, pValeur, false, false);
  }
  
  /**
   * Retourne une condition de type OR pour un champ donné à partir d'une valeur.
   * La valeur si c'est une String sera formatée au choix de l'utilisateur mais la valeur traitera quand mêmes les caractères spéciaux.
   * S'il s'agit d'une string la requête sera modifiée afin d'intégrer une conversion de caractères pour la rendre insensible aux accents.
   * @param pNomChamp Nom du champ à comparer.
   * @param pSymbole Opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   * @param pValeur Valeur à comparer.
   * @param pSansFormatage true=n'ajoute pas d'apostrophes aux chaînes de caractères et caractères.
   * @param pIgnorerAccent true=la requête est modifiée afin d'intégrer une conversion de chaînes de caractères pour rendre la comparaison
   *          insensible aux accents.
   */
  public void ajouterConditionOr(String pNomChamp, String pSymbole, Object pValeur, boolean pSansFormatage, boolean pIgnorerAccent) {
    String condition = retournerCondition(pNomChamp, pSymbole, pValeur, pSansFormatage, pIgnorerAccent);
    if (!condition.trim().isEmpty()) {
      if (!isOperateurEstPresent(requete.toString())) {
        requete.append(" or");
      }
      requete.append(' ').append(condition);
    }
  }
  
  /**
   * Supprime un groupe de valeurs ou une sous condition.
   * Un groupe de valeur est un ensemble de valeurs séparées par des virgules.
   * Une sous condition est ensemble de condition dont l'opérateur est or.
   * @param pNomGroupe Le nom du groupe.
   */
  public void supprimerGroupe(String pNomGroupe) {
    pNomGroupe = Constantes.normerTexte(pNomGroupe);
    if (pNomGroupe.isEmpty()) {
      throw new MessageErreurException("Le nom du groupe de valeurs ne doit pas être vide.");
    }
    listeGroupe.remove(pNomGroupe);
  }
  
  /**
   * Ajoute une condition à un groupe donné s'il n'existe pas le groupe sera créé.
   * Le nom doit être unique pour chaque groupe et est au choix du développeur.
   * @param pNomGroupe Le nom du groupe.
   * @param pNomChamp Le nom du champ.
   * @param pSymbole L'opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   * @param pValeur La valeur à ajouter.
   */
  public void ajouterConditionOrAuGroupe(String pNomGroupe, String pNomChamp, String pSymbole, Object pValeur) {
    pNomGroupe = Constantes.normerTexte(pNomGroupe);
    if (pNomGroupe.isEmpty()) {
      throw new MessageErreurException("Le nom du groupe des sous conditions ne doit pas être vide.");
    }
    // On récupère la sous condition stockée dans la liste
    StringBuilder sousCondition = (StringBuilder) listeGroupe.get(pNomGroupe);
    if (sousCondition == null) {
      sousCondition = new StringBuilder();
      listeGroupe.put(pNomGroupe, sousCondition);
    }
    // On ajoute la nouvelle condition à cette sous condition
    String condition = retournerCondition(pNomChamp, pSymbole, pValeur, false, false);
    if (!condition.trim().isEmpty()) {
      if (!isOperateurEstPresent(sousCondition.toString())) {
        sousCondition.append(" or");
      }
      sousCondition.append(' ').append(condition);
    }
  }
  
  /**
   * Ajoute une valeur à un groupe donné s'il n'existe pas le groupe sera créé.
   * Le nom doit être unique pour chaque groupe et est au choix du développeur.
   * @param pNomGroupe Le nom du groupe.
   * @param pValeur La valeur à ajouter.
   */
  public void ajouterValeurAuGroupe(String pNomGroupe, Object pValeur) {
    pNomGroupe = Constantes.normerTexte(pNomGroupe);
    if (pNomGroupe.isEmpty()) {
      throw new MessageErreurException("Le nom du groupe de valeurs ne doit pas être vide.");
    }
    List<Object> liste = (List<Object>) listeGroupe.get(pNomGroupe);
    if (liste == null) {
      liste = new ArrayList<Object>();
      listeGroupe.put(pNomGroupe, liste);
    }
    liste.add(pValeur);
  }
  
  /**
   * Formate le groupe puis l'insère dans la requête et supprime le groupe.
   * @param pNomGroupe Le nom du groupe à ajouter à la requête.
   * @param pOperateur Opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   */
  public void ajouterGroupeDansRequete(String pNomGroupe, String pOperateur) {
    pNomGroupe = Constantes.normerTexte(pNomGroupe);
    if (pNomGroupe.isEmpty()) {
      throw new MessageErreurException("Le nom du groupe ne doit pas être vide.");
    }
    // Construction du groupe au format texte
    StringBuilder groupe = new StringBuilder();
    
    // Pour les groupes de type valeurs
    if (listeGroupe.get(pNomGroupe) instanceof List) {
      List<?> listeValeur = (List<?>) listeGroupe.get(pNomGroupe);
      if (listeValeur == null) {
        throw new MessageErreurException("Les valeurs liées au groupe " + pNomGroupe + " n'ont pas été trouvé.");
      }
      for (Object valeur : listeValeur) {
        if (valeur instanceof String) {
          groupe.append(formaterStringSQL((String) valeur));
        }
        else if (valeur instanceof Enum) {
          groupe.append("'").append(valeur).append("'");
        }
        else if (valeur instanceof Character) {
          groupe.append(formaterCharacterSQL((Character) valeur));
        }
        else {
          groupe.append(valeur);
        }
        groupe.append(", ");
      }
      // Suppression de la dernière virgule qui ne sert pas puisqu'il y la la parenthèse fermante
      groupe.deleteCharAt(groupe.length() - 2);
      // Ajout des parenthèses qui englobe le groupe
      groupe.insert(0, '(').append(')');
      
      // Ajout de la condition à la requête
      requete.append(' ').append(pOperateur).append(' ').append(groupe.toString());
    }
    // Pour les groupes de type sous condition or
    else {
      StringBuilder sousCondition = (StringBuilder) listeGroupe.get(pNomGroupe);
      if (sousCondition == null) {
        throw new MessageErreurException("Les sous conditions liées au groupe " + pNomGroupe + " n'ont pas été trouvé.");
      }
      // Ajout des parenthèses qui englobe le groupe
      sousCondition.insert(0, '(').append(')');
      
      // Ajout de la condition à la requête
      requete.append(' ').append(pOperateur).append(' ').append(sousCondition.toString());
    }
    
    // Suppression du groupe
    listeGroupe.remove(pNomGroupe);
  }
  
  /**
   * Permet de supprimer un mot dans la requête quelque soit sa position en précisant si la recherche se fait par le début (par défaut) ou
   * par la fin.
   * @param pMot Le mot à supprimer de la requête.
   * @param pRechercheParLaFin True si la recherche se fait par la fin, false sinon (par défaut)
   */
  public void supprimerMot(String pMot, boolean pRechercheParLaFin) {
    pMot = Constantes.normerTexte(pMot);
    if (requete.length() == 0 || pMot.isEmpty()) {
      return;
    }
    // Conversion du mot et de la requete en chaine avec des lettres minuscule
    pMot = pMot.toLowerCase();
    String requetetempo = requete.toString().toLowerCase();
    // Recherche du mot en commençant la recherche par la fin
    int position;
    if (pRechercheParLaFin) {
      position = requetetempo.lastIndexOf(pMot);
    }
    else {
      position = requetetempo.indexOf(pMot);
    }
    // Le mot n'a pas été trouvé
    if (position == -1) {
      return;
    }
    // Suppression du mot
    requete = requete.delete(position, position + pMot.length());
  }
  
  /**
   * Contrôle s'il y a une requête (valide ou non).
   * @return True si la chaîne est vide, false sinon.
   */
  public boolean isEmpty() {
    return requete.toString().trim().isEmpty();
  }
  
  /**
   * Controle si à la fin de la requête en cours de construction les opérateurs in, or, and, on ou where sont présents.
   * Le but est de savoir s'il faut ajouter or ou and.
   * @param pChaine L'opérateur dont il faut contrôler la présence.
   * @return True si l'opérateur est présent, false sinon.
   */
  private static boolean isOperateurEstPresent(String pChaine) {
    pChaine = pChaine.toLowerCase().trim();
    // Ne teste pas la parenthèse fermante car dans ce cas il faut un opérateur
    return pChaine.isEmpty() || pChaine.endsWith("(") || pChaine.endsWith("where") || pChaine.endsWith("or") || pChaine.endsWith("and")
        || pChaine.endsWith("in") || pChaine.endsWith("on");
  }
  
  /**
   * Controle si à la fin de la requête en cours de construction une virgule est présente ainsi qu'éventuellement un opérateur.
   * @param pChaine La chaine à contrôler.
   * @param pOperateurPossible L'opérateur à rechercher.
   * @return True si une virugle est présente, false sinon.
   */
  private static boolean isVirguleEstPresente(String pChaine, String pOperateurPossible) {
    pChaine = pChaine.trim().toLowerCase();
    if (pOperateurPossible != null) {
      pOperateurPossible = pOperateurPossible.trim().toLowerCase();
      if (pChaine.endsWith(pOperateurPossible)) {
        return true;
      }
    }
    return pChaine.endsWith(",");
  }
  
  /**
   * Générer le code SQL pour une instruction de type SET pour une requête de type UPDATE.
   * @param pNomChamp Nom du champ à mettre à jour.
   * @param pValeur Nouvelle valeur. Les espaces avant et après la valeur sont retirés.
   * @param SansFormatage Si false, ajoute des apostrophes avant et après les chaînes de caractères et les caractères simples. Si true,
   *          ne modifie rien, le contenu de la valeur doit être correctement formaté car elle ne sera pas modifiée.
   * @return Le code SQL pour ajout d'une instruction de type SET
   */
  private static String retournerAffectationValeur(String pNomChamp, Object pValeur, boolean pSansFormatage) {
    if (pNomChamp == null || pNomChamp.isEmpty()/* || pValeur == null*/) {
      return "";
    }
    
    // Enlever les espaces avant et après
    pNomChamp = Constantes.normerTexte(pNomChamp);
    
    // Si la valeur est nulle
    if (pValeur == null) {
      return pNomChamp + "=  null";
    }
    // Tester si la valeur à mettre à jour est une chaîne de caractère
    else if (pValeur instanceof String) {
      if (pSansFormatage) {
        // Ne pas insèrer les apostrophes
        return pNomChamp + '=' + (String) pValeur;
      }
      else {
        // Insèrer les apostrophes
        return pNomChamp + '=' + formaterStringSQL((String) pValeur);
      }
    }
    // Tester si la valeur à mettre à jour est un caractère simple
    else if (pValeur instanceof Character) {
      if (pSansFormatage) {
        // Ne pas insèrer les apostrophes
        return pNomChamp + '=' + pValeur;
      }
      else {
        // Insèrer les apostrophes
        return pNomChamp + '=' + formaterCharacterSQL((Character) pValeur);
      }
    }
    // Tester si la valeur à mettre à jour est une util.Date
    else if (pValeur instanceof Date) {
      return pNomChamp + '=' + formaterTimestampSQL((Date) pValeur);
    }
    return pNomChamp + '=' + pValeur;
  }
  
  /**
   * Générer le code SQL pour une condition WHERE à partir d'un champ et d'une valeur.
   * @param pNomChamp Nom du champ à comparer.
   * @param pSymbole Opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   * @param pValeur Valeur à comparer.
   * @param pSansFormatage true=n'ajoute pas d'apostrophes aux chaînes de caractères et caractères.
   * @param pIgnorerAccent true=la requête est modifiée afin d'intégrer une conversion de chaînes de caractères pour rendre la comparaison
   *          insensible aux accents.
   * @return Le code SQL pour ajout d'une condition where
   */
  private static String retournerCondition(String pNomChamp, String pSymbole, Object pValeur, boolean pSansFormatage,
      boolean pIgnorerAccent) {
    if (pValeur == null) {
      return "";
    }
    
    // Enlever les espaces avant et après
    pNomChamp = Constantes.normerTexte(pNomChamp);
    pSymbole = Constantes.normerTexte(pSymbole);
    
    // Tester si la valeur est une chaîne de caractères
    if (pValeur instanceof String) {
      String valeur = (String) pValeur;
      
      // Protection des apostrophes si présentent dans la chaine
      if (!pSansFormatage) {
        valeur = formaterStringSQL(valeur);
      }
      // Permet de faire des recherches en ignorant les accents
      if (pIgnorerAccent) {
        pNomChamp = "translate(" + pNomChamp
            + ", 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')";
        valeur = "translate(" + valeur
            + ", 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')";
      }
      return pNomChamp + ' ' + pSymbole + ' ' + valeur;
    }
    // Tester si la valeur est un caractère simple
    // Pour le type Character (pour l'instant la gestion des accents est ignoré à voir plus tard si nécessaire)
    else if (pValeur instanceof Character) {
      if (pSansFormatage) {
        // N'insère pas les apostrophes
        return pNomChamp + ' ' + pSymbole + ' ' + pValeur;
      }
      return pNomChamp + ' ' + pSymbole + ' ' + formaterCharacterSQL((Character) pValeur);
    }
    // Tester si la valeur à mettre à jour est une util.Date
    else if (pValeur instanceof Date) {
      return pNomChamp + ' ' + pSymbole + ' ' + formaterTimestampSQL((Date) pValeur);
    }
    
    return pNomChamp + ' ' + pSymbole + ' ' + pValeur;
  }
  
  /**
   * Générer le code SQL pour une condition WHERE à partir d'une liste de valeur.
   * @param pNomChamp Nom du champ à comparer.
   * @param pSymbole Opérateur du test : "<", "<=", "=", ">=", ">", "<>" ou "like".
   * @param pListeValeur Liste de valeurs à ajouter à comparer.
   * @return La chaine de caractères correpondant à l'ajout d'une condition pour une requete SQL.
   */
  private static String retournerCondition(String pNomChamp, String pSymbole, List<?> pListeValeur) {
    if (pListeValeur == null || pListeValeur.isEmpty()) {
      return "";
    }
    
    // Enlever les espaces avant et après
    pNomChamp = Constantes.normerTexte(pNomChamp);
    pSymbole = Constantes.normerTexte(pSymbole);
    
    // Construire le code SQL pour la liste de velaleurs
    StringBuilder condition = new StringBuilder();
    int i;
    for (i = 0; i < pListeValeur.size(); i++) {
      if (i > 0) {
        condition.append(" or ");
      }
      condition.append(pNomChamp).append(' ').append(pSymbole).append(' ');
      if (pListeValeur.get(i) instanceof String) {
        condition.append(formaterStringSQL((String) pListeValeur.get(i)));
      }
      else if (pListeValeur.get(i) instanceof Enum) {
        condition.append("'").append(pListeValeur.get(i)).append("'");
      }
      else if (pListeValeur.get(i) instanceof Character) {
        condition.append(formaterCharacterSQL((Character) pListeValeur.get(i)));
      }
      // Tester si la valeur à mettre à jour est une util.Date
      else if (pListeValeur.get(i) instanceof Date) {
        condition.append(formaterTimestampSQL((Date) pListeValeur.get(i)));
      }
      else {
        condition.append(pListeValeur.get(i));
      }
    }
    if (i > 1) {
      condition.insert(0, '(').append(')');
    }
    return condition.toString();
  }
  
}
