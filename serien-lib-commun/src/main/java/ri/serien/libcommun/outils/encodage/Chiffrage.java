/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.encodage;

import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/**
 * Cette classe propose des méthodes permettant de crypter et décrypter des messages avec l'algorithme de Blowfish.
 * Et un chiffrage en MD5 et en SHA-1
 */
public class Chiffrage {
  public static final int KEY_SIZE = 128;
  public static final byte[] nop = { 97, 22, 28, 120, 66, 30, 101, 109, 109, 15, 39, 44, 74, 107, 64, 9 };
  private Key secretKey;
  private String charsetName = System.getProperty("file.encoding");
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur.
   */
  public Chiffrage() {
    setSecretKey(nop);
  }
  
  /**
   * Construteur.
   */
  public Chiffrage(String acharsetName) {
    this();
    if (acharsetName != null) {
      charsetName = acharsetName;
    }
  }
  
  public Key getSecretKey() {
    return secretKey;
  }
  
  /**
   * Retourne toutes les informations de la clé sous forme d'un tableau de
   * bytes. Elle peut ainsi être stockée puis reconstruite ultérieurement en
   * utilisant la méthode setSecretKey(byte[] keyData)
   */
  public byte[] getSecretKeyInBytes() {
    return secretKey.getEncoded();
  }
  
  public void setSecretKey(Key secretKey) {
    this.secretKey = secretKey;
  }
  
  /**
   * Permet de reconstruire la clé secrète à partir de ses données, stockées
   * dans un tableau de bytes.
   */
  public void setSecretKey(byte[] keyData) {
    if (keyData == null) {
      keyData = nop;
    }
    secretKey = new SecretKeySpec(keyData, "Blowfish");
  }
  
  /**
   * Générer la clé privé.
   */
  public void generateKey() {
    try {
      KeyGenerator keyGen = KeyGenerator.getInstance("Blowfish");
      keyGen.init(KEY_SIZE);
      secretKey = keyGen.generateKey();
    }
    catch (Exception e) {
      msgErreur += e.getMessage();
    }
  }
  
  /**
   * Crypter un tableau d'octets sous forme d'un tableau d'octets.
   */
  public byte[] cryptInBytes(byte[] plaintext) {
    try {
      Cipher cipher = Cipher.getInstance("Blowfish");
      cipher.init(Cipher.ENCRYPT_MODE, secretKey);
      return cipher.doFinal(plaintext);
    }
    catch (Exception e) {
      msgErreur += e.getMessage();
    }
    return null;
  }
  
  /**
   * Crypter une chaîne de texte sous forme d'un tableau d'octets.
   */
  public byte[] cryptInBytes(String plaintext) {
    try {
      return cryptInBytes(plaintext.getBytes(charsetName));
    }
    catch (Exception e) {
      return null;
    }
  }
  
  /**
   * Crypter une chaîne de texte sous forme d'une chaîne de texte.
   */
  public String cryptInString(String plaintext) {
    try {
      byte[] tabchaine = cryptInBytes(plaintext.getBytes(charsetName));
      return new String(tabchaine, 0, tabchaine.length, charsetName);
    }
    catch (Exception e) {
      return null;
    }
  }
  
  /**
   * Décrypter un tableau d'octets sous forme d'un tableau d'octets.
   */
  public byte[] decryptInBytes(byte[] ciphertext) {
    try {
      Cipher cipher = Cipher.getInstance("Blowfish");
      cipher.init(Cipher.DECRYPT_MODE, secretKey);
      return cipher.doFinal(ciphertext);
    }
    catch (Exception e) {
      msgErreur += e.getMessage();
    }
    return null;
  }
  
  /**
   * Décrypter une chaîne de texte sous forme d'un tableau d'octets.
   */
  public byte[] decryptInBytes(String ciphertext) {
    if ((ciphertext == null) || (ciphertext.equals(""))) {
      return null;
    }
    try {
      Cipher cipher = Cipher.getInstance("Blowfish");
      cipher.init(Cipher.DECRYPT_MODE, secretKey);
      return cipher.doFinal(ciphertext.getBytes(charsetName));
    }
    catch (Exception e) {
      e.printStackTrace();
      msgErreur += e.getMessage();
    }
    return null;
  }
  
  /**
   * Décrypter un tableau d'octets sous forme d'une chaîne de texte.
   */
  public String decryptInString(byte[] ciphertext) {
    byte[] tab = decryptInBytes(ciphertext);
    if (tab == null) {
      return null;
    }
    return new String(tab);
  }
  
  /**
   * Décrypter une chaîne de texte sous forme d'une chaîne.
   */
  public String decryptInString(String ciphertext) {
    byte[] tab = decryptInBytes(ciphertext);
    if (tab == null) {
      return null;
    }
    return new String(tab);
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * Chiffrage en MD5.
   */
  public static String md5(String input) {
    String result = input;
    if (input != null) {
      try {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(input.getBytes());
        BigInteger hash = new BigInteger(1, md.digest());
        result = hash.toString(16);
        while (result.length() < 32) {
          result = "0" + result;
        }
      }
      catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      }
    }
    return result;
  }
  
  /**
   * Chiffrage en SHA-1.
   */
  public static String sha1(String input) {
    String result = input;
    if (input != null) {
      try {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(input.getBytes());
        BigInteger hash = new BigInteger(1, md.digest());
        result = hash.toString(16);
        while (result.length() < 40) {
          result = "0" + result;
        }
      }
      catch (NoSuchAlgorithmException e) {
        e.printStackTrace();
      }
    }
    return result;
  }
}
