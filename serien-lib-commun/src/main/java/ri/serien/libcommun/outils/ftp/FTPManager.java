/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.ftp;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

/**
 * Gestion du FTP.
 * Note: Ne fonctionne pas correctement avec l'AS400 (fichier corrompu dans l'IFS lors d'un DL).
 */
public class FTPManager {
  // Variables
  private FTPClient connexion = null;
  protected String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur.
   */
  public FTPManager() {
    connexion = new FTPClient();
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Connexion au serveur.
   */
  public boolean connexion(String serveur, String user, String password) {
    if (!testStringVariable(serveur) || !testStringVariable(user) || !testStringVariable(password)) {
      return false;
    }
    try {
      connexion.connect(serveur);
      connexion.login(user, password);
      
      if (connexion.isConnected()) {
        msgErreur += "\nConnexion FTP établie";
        return true;
      }
      else {
        msgErreur += "\nConnexion FTP non établie";
        return false;
      }
    }
    catch (Exception e) {
      msgErreur += "\nErreur lors de la connexion : " + e;
      return false;
    }
  }
  
  /**
   * Teste l'existance d'un fichier.
   */
  public boolean exists(String remoteFile) {
    try {
      FTPFile file = connexion.mlistFile(remoteFile);
      return file != null;
    }
    catch (IOException e) {
      msgErreur += "\n" + e;
    }
    return false;
  }
  
  /**
   * Liste le contenu d'un dossier.
   */
  public String[] listFolder(String folder) {
    try {
      return connexion.listNames(folder);
    }
    catch (IOException e) {
      msgErreur += "\n" + e;
    }
    return null;
  }
  
  /**
   * Télécharge un fichier.
   */
  public boolean downloadFile(String remoteFile, String localFile) {
    boolean ret = false;
    try {
      OutputStream output = new FileOutputStream(localFile);
      ret = connexion.retrieveFile(remoteFile, output);
    }
    catch (Exception e) {
      msgErreur += "\n" + e;
    }
    return ret;
  }
  
  /**
   * Téléverse un fichier.
   */
  public boolean uploadFile(String localFile, String remoteFile) {
    boolean ret = false;
    try {
      InputStream input = new FileInputStream(localFile);
      ret = connexion.storeFile(remoteFile, input);
      input.close();
    }
    catch (Exception e) {
      msgErreur += "\n" + e;
    }
    return ret;
  }
  
  /**
   * Déconnexion.
   */
  public void disconnect() {
    try {
      connexion.logout();
      connexion.disconnect();
    }
    catch (Exception e) {
      msgErreur += "\nErreur déconnexion: " + e;
    }
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Teste la validité des variables de type String.
   */
  private boolean testStringVariable(String varvalue) {
    if ((varvalue == null) || (varvalue.trim().equals(""))) {
      msgErreur += "\nUne ou plusieurs variables incorrectes.";
      return false;
    }
    return true;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  public FTPClient getConnexion() {
    return connexion;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
