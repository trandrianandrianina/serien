/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.composants;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import ri.serien.libcommun.outils.Constantes;

/**
 * Description du record (plus accès vers la description des données - hors execution)
 */
public class oRecord extends GenericBase {
  // Constantes erreurs
  private final static String ERREUR_DATA_NULL = "Tableau binaire non initialis\u00e9.";
  private final static String ERREUR_LONGUEUR_TABLEAU_ERRONNE =
      "La longueur du tableau descriptif n'est pas \u00e9gale \u00e0 celle du fichier.";
  
  // Constantes
  private final static int ECRAN_80 = 80;
  public final static String RACINEPACKAGE = "ri.serien.libcommun.outils.";
  
  // Constantes ID, il y a 500 variables possibles pour l'objet oRecord
  // Chaque variable doit obligatoirement avoir un ID (pour la compilation)
  private final static int ID_ORECORD = 1000;
  private final static int ID_LARGEURECRAN = ID_ORECORD + 1;
  private final static int ID_DEBLIGNECLR = ID_ORECORD + 2;
  private final static int ID_NBRLIGNECLR = ID_ORECORD + 3;
  private final static int ID_FORMAT = ID_ORECORD + 4;
  private final static int ID_CHILD = ID_ORECORD + 5;
  
  // Variables
  private int largeurEcran = ECRAN_80;
  private int debligneClr = 1;
  private int nbrligneClr = 0;
  private String nomFormat = null;
  
  /**
   * Constructeur de la classe
   * 
   */
  public oRecord() {
    setRoot(this);
    // oRecord est l'objet principal (d'où le root) donc c'est lui qui contient la liste des container (ou parent)
    listeParent = new HashMap<String, ioGeneric>();
    listeElement = new LinkedHashMap<String, ioGeneric>();
  }
  
  /**
   * Initialise le nom du format
   * @param valeur
   */
  public void setFormat(String valeur) {
    nomFormat = valeur.trim();
  }
  
  /**
   * Initialise le nom du record
   * @param valeur
   * 
   *          public void setRecord(String valeur)
   *          {
   *          setName(valeur.trim());
   *          }
   * 
   *          /**
   *          Initialise le nom du record
   * @param valeur
   * 
   *          public void setNom(String valeur)
   *          {
   *          setName(valeur.trim());
   *          }
   */
  
  /**
   * Initialise la largeur de l'écran 5250 du record
   * @param largeur
   */
  public void setLargeur5250(int largeur) {
    largeurEcran = largeur;
  }
  
  /**
   * Initialise la largeur de l'écran 5250 du record
   * @param largeurX
   */
  public void setLargeur5250(String largeurX) {
    largeurEcran = Integer.parseInt(largeurX);
  }
  
  /**
   * Initialise le début de la zone à clearer
   * @param numligneX
   */
  public void setDebLigneCLR(String numligneX) {
    int numligne = 1;
    
    numligne = Integer.parseInt(numligneX);
    if ((numligne < 1) || (numligne > 27)) {
      debligneClr = 1;
    }
    else {
      debligneClr = numligne;
    }
  }
  
  public void setDebLigneCLR(int numligne) {
    if ((numligne < 1) || (numligne > 27)) {
      debligneClr = 1;
    }
    else {
      debligneClr = numligne;
    }
  }
  
  /**
   * Initialise le nombre de ligne à clearer
   * @param nbrligneX
   */
  public void setNbrLigneCLR(String nbrligneX) {
    int nbrligne = 0;
    
    nbrligne = Integer.parseInt(nbrligneX);
    if ((nbrligne < 1) || (nbrligne > 27)) {
      nbrligneClr = 0;
    }
    else {
      nbrligneClr = nbrligne;
    }
  }
  
  public void setNbrLigneCLR(int nbrligne) {
    if ((nbrligne < 1) || (nbrligne > 27)) {
      nbrligneClr = 0;
    }
    else {
      nbrligneClr = nbrligne;
    }
  }
  
  /**
   * Retourne le nom du format décrit dans le fichier
   * @return
   */
  public String getFormat() {
    return nomFormat;
  }
  
  /**
   * Retourne le nom du record
   * @return
   * 
   *         public String getRecord()
   *         {
   *         return getName();
   *         }
   * 
   *         /**
   *         Retourne le nom du record
   * @return
   * 
   *         public String getNom()
   *         {
   *         return getName();
   *         }
   */
  
  /**
   * Retourne le nombre de colonne du Record
   * @return
   */
  public int getLargeur5250() {
    return largeurEcran;
  }
  
  /**
   * Retourne le début de la zone à clearer
   * @return
   */
  public int getDebLigneCLR() {
    return debligneClr;
  }
  
  /**
   * Retourne le nombre de ligne à clearer
   * @return
   */
  public int getNbrLigneCLR() {
    return nbrligneClr;
  }
  
  /**
   * Initialise les attributs et leurs types d'un objet
   */
  @Override
  public void setAttributes(HashMap<String, Object> alisteAttributes) {
    super.setAttributes(alisteAttributes);
    
    // DEBLINECLR
    if (listeAttributes.containsKey("DEBLINECLR")) {
      setDebLigneCLR((Integer) listeAttributes.get("DEBLINECLR"));
    }
    // NBRLINECLR
    if (listeAttributes.containsKey("NBRLINECLR")) {
      setNbrLigneCLR((Integer) listeAttributes.get("NBRLINECLR"));
    }
    // WIDTH
    if (listeAttributes.containsKey("WIDTH")) {
      setLargeur5250((Integer) listeAttributes.get("WIDTH"));
    }
    // FORMAT
    if (listeAttributes.containsKey("FORMAT")) {
      setFormat((String) listeAttributes.get("FORMAT"));
    }
  }
  
  /**
   * Retourne les attributs et leurs types d'un objet
   */
  @Override
  public HashMap<String, Object> getAttributes() {
    super.getAttributes();
    
    listeAttributes.put("DEBLINECLR", Integer.valueOf(getDebLigneCLR()));
    listeAttributes.put("NBRLINECLR", Integer.valueOf(getNbrLigneCLR()));
    listeAttributes.put("WIDTH", Integer.valueOf(getLargeur5250()));
    listeAttributes.put("FORMAT", new String(getFormat()));
    
    return listeAttributes;
  }
  
  /**
   * Retourne les infos pour générer un fichier PNL
   * @return
   */
  public ArrayList<String> getPNL() {
    ArrayList<String> liste = new ArrayList<String>();
    liste.add("OBJECT_RECORD");
    liste.add("\tTYPEOBJ=composants.oRecord");
    liste.add("\tNAME=" + getName());
    liste.add("\tDEBLINECLR=" + getDebLigneCLR());
    if (getNbrLigneCLR() > 0) {
      liste.add("\tNBRLINECLR=" + getNbrLigneCLR());
    }
    liste.add("\tFORMAT=" + getFormat());
    liste.add("\tWIDTH=" + getLargeur5250());
    liste.add("END;");
    
    return liste;
  }
  
  /**
   * Initialise les valeurs avec un tableau venant du PNL
   * @param valeur
   */
  @Override
  public void pnlToValeurs(LinkedHashMap<String, String> valeur) {
    super.pnlToValeurs(valeur);
    
    // On met à jour la liste des Objets parents (container) de l'objet maitre (principal)
    listeParent.put(getName(), this);
    
    // DEBLINECLR
    if (valeur.containsKey("DEBLINECLR")) {
      setDebLigneCLR(valeur.get("DEBLINECLR"));
    }
    // NBRLINECLR
    if (valeur.containsKey("NBRLINECLR")) {
      setNbrLigneCLR(valeur.get("NBRLINECLR"));
    }
    // WIDTH
    if (valeur.containsKey("WIDTH")) {
      setLargeur5250(valeur.get("WIDTH"));
    }
    // FORMAT
    if (valeur.containsKey("FORMAT")) {
      setFormat(valeur.get("FORMAT"));
    }
  }
  
  /**
   * Convertie l'objet oRecord au format binaire
   * Retourne le nombre d'integer du tableau
   * @throws UnsupportedEncodingException
   */
  public ArrayList<Integer> valeursToBinaire() throws UnsupportedEncodingException {
    int k = 0;
    ArrayList<Integer> tabInt = null;
    ioGeneric composant = null;
    
    super.valeursToBinaire(null);
    
    // largeur écran
    addTabCompilation(ID_LARGEURECRAN, tabCompilation, largeurEcran);
    // debligneClr
    addTabCompilation(ID_DEBLIGNECLR, tabCompilation, debligneClr);
    // nbrligneClr
    addTabCompilation(ID_NBRLIGNECLR, tabCompilation, nbrligneClr);
    // format
    if (nomFormat != null) {
      addTabCompilation(ID_FORMAT, tabCompilation, nomFormat);
    }
    // Child
    if (!listeElement.isEmpty()) {
      tabCompilation.add(ID_CHILD);
      tabCompilation.add(listeElement.size());
      for (Map.Entry<String, ioGeneric> entry : listeElement.entrySet()) {
        // Pour chaque enfant
        composant = entry.getValue();
        tabInt = composant.valeursToBinaire(null);
        // Enregistrement des données dans le tableau du parent
        if ((tabInt != null) && (!tabInt.isEmpty())) {
          for (k = 0; k < tabInt.size(); k++) {
            tabCompilation.add(tabInt.get(k));
          }
        }
      }
    }
    
    insertTailleObjet();
    return tabCompilation;
  }
  
  /**
   * Initialise les variables de l'objet oRecord à partir d'un tableau binaire
   * Retourne si erreur
   * @param tabInt
   * @return
   */
  @Override
  public int binaireToValeurs(int tabInt[], String ListFic[]) {
    return binaireToValeurs(tabInt, 0, ListFic);
  }
  
  @Override
  public int binaireToValeurs(int tabInt[], int offset, String ListFic[]) {
    int i = 0;
    int j = 0;
    int k = 0;
    int m = 0;
    int nbrchild = 0;
    int lg = 0;
    // byte tabByte[]=null;
    byte tabByte[] = new byte[Constantes.DATAQLONG];
    int lngdescdata = 0;
    ioGeneric composant = null;
    
    // Vérification du tableau
    if (tabInt == null) {
      setMsgErreur(ERREUR_DATA_NULL);
      return Constantes.ERREUR;
    }
    
    i = offset;
    lngdescdata = tabInt[i++];
    if (lngdescdata > tabInt.length) {
      setMsgErreur(ERREUR_LONGUEUR_TABLEAU_ERRONNE);
      return Constantes.ERREUR;
    }
    
    lngdescdata = lngdescdata + i;
    while (i < lngdescdata) {
      switch (tabInt[i]) {
        case ID_LARGEURECRAN:
          i += 2;
          setLargeur5250(tabInt[i]);
          i++;
          break;
        case ID_DEBLIGNECLR:
          i += 2;
          setDebLigneCLR(tabInt[i]);
          i++;
          break;
        case ID_NBRLIGNECLR:
          i += 2;
          setNbrLigneCLR(tabInt[i]);
          i++;
          break;
        case ID_FORMAT:
          // longueur de la valeur
          lg = tabInt[++i];
          // tabByte = new byte[k];
          // récupération de la valeur
          i++;
          k = lg + i;
          for (j = 0; i < k; i++) {
            tabByte[j++] = (byte) tabInt[i];
          }
          // récupération du nom
          setFormat(new String(tabByte, 0, lg));
          break;
        case ID_CHILD:
          i++;
          nbrchild = tabInt[i++];
          for (m = 0; m < nbrchild; m++) {
            // Récupération du nom de la classe de l'objet à instancier
            lg = tabInt[i++];
            // tabByte = new byte[k];
            // récupération de la chaine
            k = lg + i;
            for (j = 0; i < k; i++) {
              tabByte[j++] = (byte) tabInt[i];
            }
            
            // Instanciation de l'objet par réflexion
            try {
              // Class<?> classe = Class.forName(new String(tabByte, 0, lg));
              String nomClasseObjet = new String(tabByte, 0, lg);
              if (!nomClasseObjet.startsWith("ri.")) {
                nomClasseObjet = oRecord.RACINEPACKAGE + nomClasseObjet;
              }
              Class<?> classe = oRecord.class.getClassLoader().loadClass(nomClasseObjet);
              // Récupération du constructeur
              Constructor<?> constructeur = classe.getConstructor();
              composant = (ioGeneric) constructeur.newInstance();
            }
            catch (Exception e) {
              JOptionPane.showMessageDialog(null, "Erreur\n" + e.getMessage(), "[oRecord] (binaireToValeurs)", JOptionPane.ERROR_MESSAGE);
            }
            /*
            catch (NoSuchMethodException e)
            {
              JOptionPane.showMessageDialog(null, "La classe n'a pas le constructeur recherché\n" + e.getMessage(), "[oRecord] (binaireToValeurs)", JOptionPane.ERROR_MESSAGE);
            }
            catch (InstantiationException e)
            {
              JOptionPane.showMessageDialog(null, "La classe est abstract ou est une interface\n" + e.getMessage(), "[oRecord] (binaireToValeurs)", JOptionPane.ERROR_MESSAGE);
            }
            catch (IllegalAccessException e)
            {
              JOptionPane.showMessageDialog(null, "La classe n'est pas accessible\n" + e.getMessage(), "[oRecord] (binaireToValeurs)", JOptionPane.ERROR_MESSAGE);
            }
            catch (java.lang.reflect.InvocationTargetException e)
            {
              JOptionPane.showMessageDialog(null, "Le constructeur invoqué a lui-même déclenché une exception\n" + e.getMessage(), "[oRecord] (binaireToValeurs)", JOptionPane.ERROR_MESSAGE);
            }
            catch (IllegalArgumentException e)
            {
              JOptionPane.showMessageDialog(null, "Mauvais type de paramètre (Pas obligatoire d'intercepter IllegalArgumentException)\n" + e.getMessage(), "[oRecord] (binaireToValeurs)", JOptionPane.ERROR_MESSAGE);
            }
            catch (ClassNotFoundException e)
            {
              JOptionPane.showMessageDialog(null, "La classe n'existe pas\n" + e.getMessage(), "[oRecord] (binaireToValeurs)", JOptionPane.ERROR_MESSAGE);
            }*/
            
            // On interprète le tableau d'entier du fichier BIN
            if (composant != null) {
              composant.setRoot(getRoot());
              i = composant.binaireToValeurs(tabInt, i, null);
              // On stocke le composant dans la liste de l'objet parent
              listeElement.put(composant.getName(), composant);
            }
          }
          break;
        
        // Valeur non traitée <-----------------------------------------------------
        default:
          i = super.binaireToValeurs(tabInt, i, null);
      }
    }
    tabByte = null;
    tabInt = null;
    
    return Constantes.OK;
  }
}
