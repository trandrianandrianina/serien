/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe statique contenant des méthodes utilitaires pour la manipulation des URLs.
 */
public class OutilURL {
  private static final Pattern PATTERN_NOM_DOMAINE = Pattern
      .compile("(http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_.]+)?([\\w\\-\\.,@?^=%&amp;:/~\\+#]*" + "[\\w\\-\\@?^=%&amp;/~\\+#])?");
  
  /**
   * Retourner le nom de domaine à partir d'une URL.
   * 
   * @param pURL URL.
   * @return Nom de domaine.
   */
  public static String getNomDomaine(String pURL) {
    if ((pURL == null) || (pURL.trim().equals(""))) {
      return "";
    }
    
    try {
      Matcher matcher = PATTERN_NOM_DOMAINE.matcher(pURL);
      if (matcher.find()) {
        return matcher.group(2).substring(1);
      }
    }
    catch (NullPointerException e) {
      // RAS
    }
    return "";
  }
}
