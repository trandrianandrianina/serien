/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils;

import java.io.PrintStream;

import org.apache.log4j.Logger;

/**
 * Cette classe permet de rediriger les flux de la console vers un logger nommé (en dur) "consoleFichier" définit dans la classe Trace.
 */
public class StdOutErrTrace {
  // Variables
  private static Logger loggerConsole = null;
  
  public static void redirigerSystemOutAndErr() {
    loggerConsole = Logger.getLogger("consoleFichier");
    System.setOut(createLoggingProxy(System.out));
    System.setErr(createLoggingProxy(System.err));
  }
  
  public static PrintStream createLoggingProxy(final PrintStream realPrintStream) {
    return new PrintStream(realPrintStream) {
      @Override
      public void print(final String string) {
        loggerConsole.info(string);
      }
      
      @Override
      public void println(final String string) {
        loggerConsole.info(string);
      }
    };
  }
}
