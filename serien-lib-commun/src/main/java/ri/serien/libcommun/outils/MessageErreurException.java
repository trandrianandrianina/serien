/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils;

/**
 * Message d'erreur destiné à l'utilisateur du logiciel.
 * 
 * Cette exception permet de générer un message d'erreur qui sera affiché à l'utilisateur. Il doit donc être rédigé dans un français
 * clair et simple. Il faut limiter le jargon technique. Ce message ne peut correspondre qu'à un message d'erreur car le fait de lever une
 * exception interrompt le traitement en cours. Ce mécanisme n'est donc pas adapté pour des messages d'informations ou d'alertes.
 * 
 * Ce message peut-être généré côté client ou serveur. Côté serveur, si c'est dans le cadre d'un appel de méthode RMI, l'exception est
 * envoyée au client qui l'affiche à l'utilisateur.
 * 
 * Les éléments de messages sont générés dès la construction de MessageErreurException car on n'envoit pas les exceptions
 * côté client. En effet, une exception peut être générée côté serveur par une librairie qui n'est pas présente côté client. Du coup,
 * le client ne parviendra pas à désérialiser le MessageErreurException et générera un "UnmarshalException". Ce cas se produit par
 * exemple avec l'exception "com.ibm.as400.access.ExtendedIllegalArgumentException" généré par jt400 si le format des données envoyées
 * est invalide.
 * 
 * Trois cas possibles :
 * - Un MessageErreurException est directement créé par le développeur.
 * - Une exception quelconque est levée par une API, capturée par un try catch, puis un MessageErreurException est émis avec un message
 * plus parlant pour l'utilisateur.
 * - Un MessageErreurException est émis suivant une des deux méthodes suivantes et est capturé par un try catch. Un autre
 * MessageErreurException est émis avec un message plus haut niveau pour l'utilisateur.
 */
public class MessageErreurException extends RuntimeException {
  private static final String PREFIXE = "[MessageErreurException] ";
  public static final String MESSAGE_ERREUR_TECHNIQUE = "Une erreur technique est survenue. Merci de contacter le service assistance.";
  private String messageUtilisateur = null;
  private String messageTechnique = null;
  private String messageStackTrace = null;
  
  /**
   * Constructeur avec un message utilisateur simple.
   * 
   * Ce type de message permet de remonter des erreurs fonctionnels, par exemple "La date du document est supérieure à la date du jour".
   * 
   * @param pMessage Message d'erreur à afficher à l'utilisateur.
   */
  public MessageErreurException(String pMessage) {
    super(pMessage);
    
    genererMessage(null, pMessage);
  }
  
  /**
   * Constructeur avec un message utilisateur accompagné d'une exception.
   * 
   * Ce type de message est adapté aux erreurs techniques pour lesquelles ont veut afficher un message plus compréhensible pour
   * l'utilisateur, par exemple "Erreur lors de la lecture des informations du client".
   * 
   * Les informations plus techniques contenues dans les exceptions ainsi que la stacktrace seront consultables via l'icône engrenage de
   * la boîte d'erreur standard.
   * 
   * @param pThrowable Exception qui a provoqué l'erreur.
   * @param pMessage Message d'erreur à afficher à l'utilisateur.
   */
  public MessageErreurException(Throwable pThrowable, String pMessage) {
    super(pMessage);
    
    genererMessage(pThrowable, pMessage);
  }
  
  /**
   * Générer un message personnalisé
   * .
   * 
   * @param pThrowable
   * @param pMessage
   */
  public void genererMessage(Throwable pThrowable, String pMessage) {
    // Tracer le message d'erreur au moment de sa création
    Trace.erreur(pThrowable, "[" + this.getClass().getSimpleName() + "] " + pMessage);
    
    // Générer les messages résumés et détaillés
    genererMessageUtilisateur(pThrowable, pMessage);
    genererMessageTechnique(pThrowable, pMessage);
    genererMessageStackTrace(pThrowable, pMessage);
  }
  
  /**
   * Générer le message utilisateur.
   *
   * Il peut arriver que plusieurs MessageErreurException soient empilés, chacun avec un message utilisateur différent. Tous les messages
   * sont empilés du message le plus général en haut de la liste au message le plus précis en bas de la liste.
   *
   * @param pThrowable Exception qui a provoqué l'erreur.
   * @param pMessage Message d'erreur à afficher à l'utilisateur.
   */
  private void genererMessageUtilisateur(Throwable pThrowable, String pMessage) {
    try {
      // Commencer par le message fourni lors de la création de cette exception
      if (pMessage != null) {
        messageUtilisateur = pMessage;
      }
      else {
        messageUtilisateur = "";
      }
      
      // Rechercher les éventuels MessageUtilisateurException dans la pile d'exceptions afin d'ajouter leur propre message
      for (Throwable cause = pThrowable; cause != null; cause = cause.getCause()) {
        if (cause instanceof MessageErreurException) {
          MessageErreurException messageErreurException = (MessageErreurException) cause;
          
          // Ajouter un saut de ligne s'il y a déjà un message
          if (!messageUtilisateur.trim().isEmpty()) {
            messageUtilisateur += "\n";
          }
          
          // Ajouter le message utilisateur précédent
          messageUtilisateur += messageErreurException.getMessageUtilisateur();
        }
      }
    }
    catch (Exception e) {
      // Erreur dans le traitement de l'erreur, on laisse tomber la génération de ce message
    }
  }
  
  /**
   * Générer le message technique.
   *
   * Le message technique empile les messages de toutes les exceptions de la liste, que le message soit destiné à l'utilisateur ou
   * plus technique.
   *
   * @param pThrowable Exception qui a provoqué l'erreur.
   * @param pMessage Message d'erreur à afficher à l'utilisateur.
   */
  private void genererMessageTechnique(Throwable pThrowable, String pMessage) {
    try {
      // Commencer par le message fourni lors de la création de cette exception
      if (pMessage != null) {
        messageTechnique = getClass().getSimpleName() + " : " + pMessage;
      }
      else {
        messageTechnique = "";
      }
      
      // Parcourir les exceptions dans la pile d'exceptions afin d'ajouter leur propre message
      for (Throwable cause = pThrowable; cause != null; cause = cause.getCause()) {
        // Ajouter un saut de ligne s'il y a déjà un message
        if (!messageTechnique.trim().isEmpty()) {
          messageTechnique += "\n";
        }
        
        // Ajouter le message technique précédent
        if (cause instanceof MessageErreurException) {
          // Le message technique de MessageErreurException est déjà préfixé correctement
          messageTechnique += ((MessageErreurException) cause).getMessageTechnique();
        }
        else {
          // Préfixer le message avec la classe de l'exception
          messageTechnique += cause.getClass().getSimpleName() + " : " + cause.getMessage();
        }
      }
    }
    catch (Exception e) {
      // Erreur dans le traitement de l'erreur, on laisse tomber la génération de ce message
    }
  }
  
  /**
   * Générer le message stacktrace.
   *
   * Ce message contient la stacktrace de la première exception.
   *
   * @param pThrowable Exception qui a provoqué l'erreur.
   * @param pMessage Message d'erreur à afficher à l'utilisateur.
   */
  private void genererMessageStackTrace(Throwable pThrowable, String pMessage) {
    try {
      // Récupérer la stacktrace de l'exception fournie s'il y en a une
      Throwable throwable = null;
      if (pThrowable != null) {
        // Rechercher la stacktrace de la première exception qui a été remontée
        throwable = pThrowable;
        while (throwable.getCause() != null) {
          throwable = throwable.getCause();
        }
      }
      else {
        // Afficher la stacktrace de cette exception par défaut
        throwable = this;
      }
      
      // Ajouter le message stacktrace du MessageErreurException précédent, qui contient la stacktrace de l'exception initiale.
      // Si le message stacktrace est vide, c'est que c'est le premier MessageErreurException généré, on lit alors sa stacktrace
      // comme une exception standard
      if (throwable instanceof MessageErreurException && ((MessageErreurException) throwable).getMessageStackTrace() != null) {
        messageStackTrace = ((MessageErreurException) throwable).getMessageStackTrace();
      }
      else {
        // Ajouter la stacktrace de l'exception
        messageStackTrace = "";
        for (StackTraceElement element : throwable.getStackTrace()) {
          messageStackTrace += element.toString() + "\n";
        }
      }
    }
    catch (Exception e) {
      // Erreur dans le traitement de l'erreur, on laisse tomber la génération de ce message
    }
  }
  
  /**
   * Message utilisateur.
   * 
   * Le message utilisateur contient tous les messages destinés à l'utilisateur de la liste. Les messages techniques des exceptions
   * n'y sont pas présent.
   * 
   * @return Message.
   */
  public String getMessageUtilisateur() {
    return messageUtilisateur;
  }
  
  /**
   * Message technique.
   * 
   * Le message technique empile les messages de toutes les exceptions de la liste, que le message soit destiné à l'utilisateur ou
   * plus technique.
   * 
   * @return Message.
   */
  public String getMessageTechnique() {
    return messageTechnique;
  }
  
  /**
   * Message stacktrace.
   * 
   * Ce message contient les stacktraces des exceptions empilées, le sunes à la suite des autres.
   * 
   * @return Message.
   */
  public String getMessageStackTrace() {
    return messageStackTrace;
  }
}
