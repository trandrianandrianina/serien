/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.encodage;

import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Encodage/décodage des objets en XML.
 */
public final class XMLTools {
  // Variables
  private static ArrayList<String> listeMsgExceptionDecoder = null;
  
  /**
   * Sérialisation d'un objet dans un fichier.
   * 
   * @param object objet a serialiser.
   * @param fileName chemin du fichier.
   */
  public static void encodeToFile(Object object, String fileName) throws FileNotFoundException, IOException {
    // Ouverture de l'encodeur vers le fichier
    XMLEncoder encoder = new XMLEncoder(new FileOutputStream(fileName));
    try {
      // Sérialisation de l'objet
      encoder.writeObject(object);
      encoder.flush();
    }
    finally {
      // Fermeture de l'encodeur
      encoder.close();
    }
  }
  
  /**
   * Désérialisation d'un objet dans un fichier.
   * @param fileName chemin du fichier.
   */
  public static Object decodeFromFile(String fileName) throws FileNotFoundException, IOException {
    Object object = null;
    if (listeMsgExceptionDecoder == null) {
      listeMsgExceptionDecoder = new ArrayList<String>();
    }
    else {
      listeMsgExceptionDecoder.clear();
    }
    
    // Ouverture de décodeur
    XMLDecoder decoder = new XMLDecoder(new FileInputStream(fileName), null, new ExceptionListener() {
      @Override
      public void exceptionThrown(Exception exception) {
        listeMsgExceptionDecoder.add(exception.getMessage());
      }
    });
    try {
      // Désérialisation de l'objet
      object = decoder.readObject();
    }
    catch (Exception e) {
      throw new MessageErreurException("Erreur de dé-serialisation du fichier.\nPour NewSim, modifier le fichier " + fileName
          + "\nen remplaçant ri.serien.libcommun.exploitation.edition.Spool par ri.serien.libcommun.exploitation.edition.SpoolOld");
    }
    finally {
      // Fermeture du decodeur
      decoder.close();
    }
    return object;
  }
  
  /**
   * Sérialisation d'un objet dans un StringBuffer.
   * @param object objet a sérialiser.
   */
  public static StringBuffer encodeToStringBuffer(Object object) throws FileNotFoundException, IOException {
    return new StringBuffer(encodeToString(object));
  }
  
  /**
   * Désérialisation d'un objet dans un StringBuffer.
   * @param sb StringBuffer.
   */
  public static Object decodeFromStringBuffer(final StringBuffer sb) throws FileNotFoundException, IOException {
    return decodeFromString(sb.toString());
  }
  
  /**
   * Sérialisation d'un objet dans un String.
   * @param object objet a sérialiser.
   */
  public static String encodeToString(Object object) throws IOException {
    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    XMLEncoder encoder = new XMLEncoder(baos);
    
    // Ouverture de l'encodeur vers le flux
    try {
      // Sérialisation de l'objet
      encoder.writeObject(object);
      encoder.flush();
    }
    finally {
      // Fermeture de l'encodeur
      encoder.close();
    }
    return baos.toString("UTF-8");
  }
  
  /**
   * Désérialisation d'un objet dans un String.
   * @param s String.
   */
  public static Object decodeFromString(final String s) throws IOException {
    Object object = null;
    
    // ouverture de decodeur
    XMLDecoder decoder = new XMLDecoder(new ByteArrayInputStream(s.getBytes("UTF-8")), null, new ExceptionListener() {
      // @Override
      @Override
      public void exceptionThrown(Exception exception) {
        if (listeMsgExceptionDecoder == null) {
          listeMsgExceptionDecoder = new ArrayList<String>();
        }
        listeMsgExceptionDecoder.add(exception.getMessage());
      }
    });
    try {
      // désérialisation de l'objet
      object = decoder.readObject();
    }
    finally {
      // fermeture du decodeur
      decoder.close();
    }
    return object;
  }
  
  /**
   * Retourne la liste des exceptions qui ont pu se déclencher lors du chargement du fichier XML.
   */
  public static ArrayList<String> getListeMsgExceptionDecoder() {
    return listeMsgExceptionDecoder;
  }
  
  /**
   * Sérialise un document.
   * Attention : ne fonctionne pas sur AS400 avec java 5.
   */
  /* A priori ne sert pas - Commenté après le forçage de la compilation en Java 6
  public static String serialize(Document doc) {
    if (doc == null) {
      return null;
    }
    
    StringWriter writer = new StringWriter();
    OutputFormat format = new OutputFormat();
    format.setIndenting(true);
    
    XMLSerializer serializer = new XMLSerializer(writer, format);
    try {
      serializer.serialize(doc);
    }
    catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    
    return writer.getBuffer().toString();
  }*/
  
  /**
   * Enregistre un document dans un fichier xml.
   */
  public static boolean toFile(Document doc, String xmlfile) {
    try {
      Transformer transformer = TransformerFactory.newInstance().newTransformer();
      // Result output = new StreamResult(new File(file));
      Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(xmlfile), "UTF-8"));
      Result output = new StreamResult(out);
      Source input = new DOMSource(doc);
      
      transformer.transform(input, output);
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }
}
