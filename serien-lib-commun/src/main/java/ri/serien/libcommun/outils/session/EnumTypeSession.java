/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Les différents type de session.
 */
public enum EnumTypeSession {
  SESSION_INCONNUE(0, "Session inconnue"),
  SESSION_TRANSFERT(1, "Session transfert"),
  SESSION_IDENTIFICATION(2, "Session identification"),
  SESSION_RPG(3, "Session RPG"),
  SESSION_MENU(4, "Session menu"),
  SESSION_JAVA(5, "Session Java");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeSession(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeSession valueOfByCode(Integer pCode) {
    for (EnumTypeSession value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de la session est invalide : " + pCode);
  }
}
