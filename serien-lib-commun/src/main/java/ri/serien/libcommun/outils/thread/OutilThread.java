/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.thread;

import java.lang.management.ManagementFactory;
import java.util.Set;

import ri.serien.libcommun.outils.Trace;

/**
 * Classe contenant les méthodes outils en relation avec les Threads.
 */
public class OutilThread {
  private static Set<Thread> setThread;
  
  /**
   * Ajouter une informations dans les traces avec le nombre total de threads.
   */
  public static void tracerNombreThread() {
    int nombreThread = ManagementFactory.getThreadMXBean().getThreadCount();
    Trace.info("Nb Threads = " + nombreThread);
  }
  
  /**
   * Ajouter une informations dans les traces avec le nombre total de threads.
   */
  public static void tracerNombreThread(String pNomMethode, int pIndex) {
    int nombreThread = ManagementFactory.getThreadMXBean().getThreadCount();
    ajouterTrace("Nb threads = ", nombreThread, pNomMethode, pIndex);
  }
  
  /**
   * Ecrire une ligne dans les traces pour le nombre de threads.
   */
  private static void ajouterTrace(String pPrefixe, int pNombreThread, String pNomMethode, int pIndex) {
    // Initier le texte à écrire tans les traces
    StringBuffer trace = new StringBuffer("");
    
    // Ajouter le préfixe s'il y en a un
    if (pPrefixe != null) {
      trace.append(pPrefixe);
    }
    
    // Ajouter le nombre de threads
    trace.append(pNombreThread);
    
    // Ajouter le nom de la méthode
    if (pNomMethode != null && !pNomMethode.isEmpty()) {
      trace.append(", Méthode=" + pNomMethode);
    }
    
    // Ajouter le nom de l'index
    if (pIndex > 0) {
      trace.append(", Index=" + pIndex);
    }
    
    // Ajouter la trace
    Trace.info(trace.toString());
  }
  
  /**
   * Ajouter une informations dans les traces avec la liste des threads créées ou supprimées depuis le dernier appel.
   */
  public static void traceThreadDifference() {
    tracerDifferenceThread(null, 0);
  }
  
  /**
   * Ajouter une informations dans les traces avec la liste des threads créées ou supprimées depuis le dernier appel.
   */
  public static synchronized void tracerDifferenceThread(String pNomMethode, int pIndex) {
    // Compter le nombre de threads
    // int nombreThread = ManagementFactory.getThreadMXBean().getThreadCount();
    // ajouterTrace("Nb threads pour différences = ", nombreThread, pNomMethode, pIndex);
    
    // Récupérer la liste des threads de l'application
    Set<Thread> setThreadEnCours = Thread.getAllStackTraces().keySet();
    
    // Tracer les différences
    if (setThread != null && setThreadEnCours != null) {
      // Tracer les threads en moins depuis la dernière fois
      for (Thread thread : setThread) {
        if (!setThreadEnCours.contains(thread)) {
          ajouterTrace("Thread : - ", thread, pNomMethode, pIndex);
        }
      }
      
      // Tracer les threads en plus depuis la dernière fois
      for (Thread thread : setThreadEnCours) {
        if (!setThread.contains(thread)) {
          ajouterTrace("Thread : + ", thread, pNomMethode, pIndex);
        }
      }
    }
    
    // Mémoriser les threads pour la prochaine fois
    setThread = setThreadEnCours;
  }
  
  /**
   * Ecrire une ligne dans les traces pour les threads créées ou supprimées.
   */
  private static void ajouterTrace(String pPrefixe, Thread pThread, String pNomMethode, int pIndex) {
    if (pThread == null) {
      return;
    }
    
    // Initier le texte à écrire tans les traces
    StringBuffer trace = new StringBuffer("");
    
    // Ajouter le préfixe s'il y en a un
    if (pPrefixe != null) {
      trace.append(pPrefixe);
    }
    
    // Ajouter le nom de la thread
    trace.append(pThread.getName());
    
    // Ajouter l'état de la thread
    trace.append(", Etat=" + pThread.getState());
    
    // Ajouter le nom de la méthode
    if (pNomMethode != null && !pNomMethode.isEmpty()) {
      trace.append(", Méthode=" + pNomMethode);
    }
    
    // Ajouter le nom de l'index
    if (pIndex > 0) {
      trace.append(", Index=" + pIndex);
    }
    
    // Ajouter la trace
    Trace.info(trace.toString());
  }
}
