/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.composants;

/**
 * Description de l'interface gérant le flux
 */
public interface ioData {
  /**
   * Recherche le oData correspondant au Hostfield du panel en cours
   * et fait les mise à jour de données nécessaire
   */
  public void setData();
}
