/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.protocolemessage;

import java.util.ArrayList;
import java.util.Arrays;

public class Db2Request extends BaseRequest {
  // Constantes
  public static final int EXECUTE = 1;
  
  public static final ArrayList<Integer> ACTIONS = new ArrayList<Integer>() {
    {
      add(NOACTION);
    }
    
    {
      add(EXECUTE);
    }
  };
  
  // Variables
  private String request = null;
  private boolean select = false;
  // Mis en commentaire car en V5R4 et version en dessous l'API de Google plante avec des tableaux
  // private String[] title=null;
  // private Object[][] data=null;
  // Nouvelle méthode
  private ArrayList<String> title = null;
  private ArrayList<LineCells> data = null;
  
  /**
   * Constructeur.
   */
  public Db2Request() {
  }
  
  /**
   * Constructeur.
   */
  public Db2Request(String arequest, int anaction) {
    setRequest(arequest);
    setActions(anaction);
  }
  
  // --> Accesseurs <--------------------------------------------------------
  
  /**
   * @return le request.
   */
  public String getRequest() {
    return request;
  }
  
  /**
   * @param request le request à définir.
   */
  public void setRequest(String request) {
    this.request = request;
    setSelect(request.toLowerCase().startsWith("select"));
  }
  
  /**
   * @return le select.
   */
  public boolean isSelect() {
    return select;
  }
  
  /**
   * @param select le select à définir.
   */
  public void setSelect(boolean select) {
    this.select = select;
  }
  
  /**
   * Title. Fait pour garder la compatibilité avec les autres classes.
   */
  public String[] getTTitle() {
    if (title == null) {
      return null;
    }
    
    String[] tab = new String[title.size()];
    return title.toArray(tab);
  }
  
  /**
   * @param atitle le title à définir. Fait pour garder la compatibilité avec les autres classes.
   */
  public void setTTitle(String[] atitle) {
    if (atitle == null) {
      return;
    }
    if (title != null) {
      title.clear();
    }
    title = new ArrayList<String>(Arrays.asList(atitle));
  }
  
  /**
   * @param title le title à définir.
   */
  public void setTitle(ArrayList<String> title) {
    this.title = title;
  }
  
  /**
   * @return le title.
   */
  public ArrayList<String> getTitle() {
    return title;
  }
  
  /**
   * @return le data. Fait pour garder la compatibilité avec les autres classes.
   */
  public Object[][] getTData() {
    if (data == null) {
      return null;
    }
    Object[][] tab = new Object[data.size()][data.get(0).tableau.size()];
    for (int i = 0; i < tab.length; i++) {
      for (int j = 0; j < tab[i].length; j++) {
        tab[i][j] = data.get(i).tableau.get(j);
      }
    }
    
    return tab;
  }
  
  /**
   * @param adata le data à définir. Fait pour garder la compatibilité avec les autres classes.
   */
  public void setTData(Object[][] adata) {
    if (adata == null) {
      return;
    }
    if (data != null) {
      for (int i = 0; i < data.size(); i++) {
        data.get(i).tableau.clear();
        // data.get(i).tableau.trimToSize();
      }
      data.clear();
      // data.trimToSize();
    }
    else {
      data = new ArrayList<LineCells>();
    }
    
    for (int i = 0; i < adata.length; i++) {
      if (data.size() < adata.length) {
        data.add(new LineCells());
      }
      for (int j = 0; j < adata[i].length; j++) {
        // if( data.get(i).tableau == null )
        // data.get(i).tableau = new ArrayList<Object>();
        data.get(i).tableau.add(adata[i][j]);
      }
    }
  }
  
  /**
   * @param data le data à définir.
   */
  public void setData(ArrayList<LineCells> data) {
    this.data = data;
  }
  
  /**
   * @return le data.
   */
  public ArrayList<LineCells> getData() {
    return data;
  }
}
