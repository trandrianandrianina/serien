/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

/**
 * Tracer les messages de l'application.
 * 
 * Centralise les traces des logiciels. C'est une couche d'abstraction qui rend le code indépendant de la technique utilisée pour gérer
 * les traces. Les traces sont actuellement gérées par Log4j.
 */
public class Trace {
  // Format des traces sur la console
  private static final String FORMAT_CONSOLE = "%d{yyyy-MM-dd HH:mm:ss,SSS}\t%-5p\t%m%n";
  
  // Format des traces dans le fichier
  private static final String FORMAT_FICHIER = "%d{yyyy-MM-dd HH:mm:ss,SSS}\t%-40t\t%-5p\t%m%n";
  
  // Format des traces sur la console fichier
  private static final String FORMAT_CONSOLE_FICHIER = "%d{yyyy-MM-dd HH:mm:ss,SSS}\t%m%n";
  
  // Dossier par défaut pour la génération des fichiers traces
  private static final String DOSSIER_RACINE_PAR_DEFAUT = ".";
  
  // Nom par défaut pour le nom du logiciel
  private static final String NOM_FICHIER_PAR_DEFAUT = "serien";
  
  // Nom par défaut pour le nom du logiciel
  private static final String NOM_LOGICIEL_PAR_DEFAUT = "SERIE N";
  
  // Dossier logs
  private static final String DOSSIER_LOG = "logs";
  
  // Extension des fichiers de traces
  private static final String EXTENSION_LOG = ".log";
  
  // Espaces ajoutés devant les traces pour les indenter
  private static final String TABULATION = "  ";
  
  // Logger Log4j
  final static Logger logger = Logger.getRootLogger();
  
  // Sortie console.
  private static ConsoleAppender consoleAppender = null;
  
  // Sortie fichier.
  private static RollingFileAppender fileAppender = null;
  
  // Sortie fichier console.
  private static RollingFileAppender consoleFileAppender = null;
  
  // Dossier racine du logiciel
  private static String dossierRacine = DOSSIER_RACINE_PAR_DEFAUT;
  
  // Nom du fichier de traces
  private static String fichier = NOM_FICHIER_PAR_DEFAUT;
  private static String fichierConsole = "console";
  
  // Nom du logiciel dont on suite les traces
  private static String logiciel = NOM_LOGICIEL_PAR_DEFAUT;
  
  /**
   * Activer la sortie console très rapidement pour avoir des informations dès le démarrage du logiciel.
   */
  static {
    // Supprimer les appenders déjà existants, il arrive que certaines librairies externes configurent des appenders par le biais du
    // fichier log4j.properties.
    Logger.getRootLogger().removeAllAppenders();
    
    // Afficher les traces de niveau INFO par défaut
    Logger.getRootLogger().setLevel(Level.INFO);
    
    // Activer la sortie console (pour capturer les traces avant l'activation de la sortie fichier)
    activerSortieConsole(true);
  }
  
  /**
   * Constructeur par défaut mis en "private" pour empêcher l'instanciation de cette classe.
   */
  private Trace() {
  }
  
  /**
   * Enregistrer un nouveau gestionnaire pour les exceptions non capturées afin les tracer.
   */
  private static void tracerExceptionsNonCapturees() {
    Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
      @Override
      public void uncaughtException(Thread thread, Throwable e) {
        Trace.erreur(e, "Exception non capturée :");
      }
    });
  }
  
  /**
   * Démarrer le stockage des traces dans un fichiers.
   * 
   * Cette méthode active la sortie fichier et désactive la sortie console. La sortie console n'est pas conservée car elle n'est pas utile
   * en production, elle est consommatrice de ressources systèmes et, surtout, elle remplie la QPRINT sur le serveur, ce qui finit par
   * provoquer le blocage du logiciel. Par ailleurs, la mire de démarrage est affichée dans les traces.
   */
  public static void demarrerLogiciel(String pdossierRacine, String pfichier, String plogiciel) {
    // Traiter les paramètres
    if (pdossierRacine != null && !pdossierRacine.isEmpty()) {
      dossierRacine = pdossierRacine;
    }
    else {
      dossierRacine = DOSSIER_RACINE_PAR_DEFAUT;
    }
    
    if (pfichier != null && !pfichier.isEmpty()) {
      fichier = pfichier;
    }
    else {
      fichier = NOM_FICHIER_PAR_DEFAUT;
    }
    
    if (plogiciel != null && !plogiciel.isEmpty()) {
      logiciel = plogiciel;
    }
    else {
      logiciel = NOM_LOGICIEL_PAR_DEFAUT;
    }
    
    // Activer la sortie fichier
    activerSortieFichier(true);
    
    // Tracer les exceptions non capturées
    tracerExceptionsNonCapturees();
    
    // Ecrire la mire de démarrage
    titre("");
    soustitre("DEMARRAGE " + logiciel.toUpperCase());
    titre("");
    
    // Désactiver la sortie console
    activerSortieConsole(false);
  }
  
  /**
   * Tracer l'arrêt du logiciel.
   */
  public static void arreterLogiciel() {
    afficherEtatMemoire();
    Trace.titre("ARRET " + logiciel.toUpperCase());
  }
  
  /**
   * Affiche l'état de la mémoire utilisée par la JVM.
   */
  public static void afficherEtatMemoire() {
    Trace.info(
        "Mémoire utilisée : " + String.format("%10d octets", (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())));
    Trace.info("Mémoire libre    : " + String.format("%10d octets", Runtime.getRuntime().freeMemory()));
    Trace.info("Mémoire totale   : " + String.format("%10d octets", Runtime.getRuntime().totalMemory()));
    Trace.info("Mémoire max      : " + String.format("%10d octets", Runtime.getRuntime().maxMemory()));
  }
  
  /**
   * Configurer l'affichage des traces sur la console.
   * Ce code doit être simple et robuste car la console sera l'unique source d'informations en cas de problème tôt lors du démarrage
   * du logiciel (les traces sur fichiers ne seront paeut-être pas opérationnelles).
   */
  public static void activerSortieConsole(boolean pActif) {
    // Tester s'il faut activer ou désactiver la sortie console
    if (pActif && consoleAppender == null) {
      activerSortieFichierConsole(false);
      // Il faut activer la sortie console si celle-ci n'a pas déjà été configurée
      consoleAppender = new ConsoleAppender(new PatternLayout(FORMAT_CONSOLE), ConsoleAppender.SYSTEM_OUT);
      Logger.getRootLogger().addAppender(consoleAppender);
      Trace.info("Activer la sortie console des traces.");
    }
    else if (!pActif && consoleAppender != null) {
      // Il faut désactiver la sortie console est celle-ci est active
      Trace.info("Désactiver la sortie console des traces.");
      Logger.getRootLogger().removeAppender(consoleAppender);
      consoleAppender = null;
      
      // Redirection de la console vers le fichier de log indépendant des traces classiques
      activerSortieFichierConsole(true);
    }
  }
  
  /**
   * Configurer l'écriture des logs dans un fichier.
   */
  public static void activerSortieFichier(boolean pActif) {
    if (pActif && fileAppender == null) {
      // Il faut activer la sortie fichier est celle-ci n'a pas déjà été configurée
      try {
        Trace.info("Activer la sortie fichier des traces : fichier=" + getFichierTraceEncours());
        fileAppender = new RollingFileAppender(new PatternLayout(FORMAT_FICHIER), getFichierTraceEncours());
        fileAppender.setMaxBackupIndex(199);
        fileAppender.rollOver();
        Logger.getRootLogger().addAppender(fileAppender);
      }
      catch (IOException e) {
        Trace.erreur("Erreur lors de la configuration du fichier de traces.");
      }
    }
    else if (!pActif && fileAppender != null) {
      // Il faut désactiver la sortie fichier est celle-ci est active
      Trace.info("Désactiver la sortie fichier des traces.");
      Logger.getRootLogger().removeAppender(fileAppender);
      fileAppender = null;
    }
  }
  
  /**
   * Configurer l'écriture des logs de la console dans un fichier.
   * Il est surement possible de faire mieux mais pour l'instant le fonctionnement de log4j m'est assez obscur.
   */
  private static void activerSortieFichierConsole(boolean pActif) {
    if (pActif && consoleFileAppender == null) {
      // Il faut activer la sortie fichier console est celle-ci n'a pas déjà été configurée
      try {
        Trace.info("Activer la sortie fichier console : fichier=" + getFichierConsole());
        consoleFileAppender = new RollingFileAppender(new PatternLayout(FORMAT_CONSOLE_FICHIER), getFichierConsole());
        consoleFileAppender.setMaxBackupIndex(199);
        consoleFileAppender.rollOver();
        // Ajoute le fichier de sortie dans le logger
        Logger.getLogger("consoleFichier").addAppender(consoleFileAppender);
        // Désactive le fichier de sortie pour les traces classiques sinon écriture en double (dans le root et le consoleFichier)
        Logger.getLogger("consoleFichier").setAdditivity(false);
        // Redirige le flux de la console vers ce fichier de sortie
        StdOutErrTrace.redirigerSystemOutAndErr();
      }
      catch (IOException e) {
        Trace.erreur("Erreur lors de la configuration du fichier console.");
      }
    }
    else if (!pActif && consoleFileAppender != null) {
      // Il faut désactiver la sortie fichier console est celle-ci est active
      Trace.info("Désactiver la sortie fichier console.");
      Logger.getLogger("consoleFichier").removeAppender(consoleFileAppender);
      consoleFileAppender = null;
    }
  }
  
  /**
   * Activer ou désactiver le mode débug.
   */
  public static void setModeDebug(boolean pDebug) {
    // Activer le mode débug
    if (pDebug && Logger.getRootLogger().getLevel() != Level.DEBUG) {
      Trace.info("Activer le mode débug pour les traces.");
      Logger.getRootLogger().setLevel(Level.DEBUG);
    }
    // Désactiver le mode débug
    else if (!pDebug && Logger.getRootLogger().getLevel() == Level.DEBUG) {
      Trace.info("Désactiver le mode débug pour les traces.");
      Logger.getRootLogger().setLevel(Level.INFO);
    }
  }
  
  /**
   * Indiquer si le mode débug est activé.
   */
  public static boolean isModeDebug() {
    return Logger.getRootLogger().getLevel() == Level.DEBUG;
  }
  
  /**
   * Tracer un message de titre.
   * Le titre est en majuscules et centré dans une ligne de 80 tirets '-'.
   */
  public static void titre(String message) {
    if (message == null || message.isEmpty()) {
      logger.info(new String(new char[80]).replace("\0", "-"));
    }
    else {
      String traitGauche = new String(new char[(78 - message.length()) / 2]).replace("\0", "-");
      String traitDroite = new String(new char[78 - message.length() - traitGauche.length()]).replace("\0", "-");
      logger.info(traitGauche + " " + message.toUpperCase() + " " + traitDroite);
    }
  }
  
  /**
   * Tracer un sous-titre.
   * Un sous-titre n'est pas indenté.
   */
  public static void soustitre(String message) {
    logger.info(message);
  }
  
  /**
   * Tracer un message de debug en indiquant un message.
   */
  public static void debug(String pMessage) {
    logger.debug(TABULATION + pMessage);
  }
  
  /**
   * Tracer un message de debug en indiquant la classe, la méthode et un message.
   * Pour le nom de la classe, il est recommandé d'utiliser [NomClasse].class.
   */
  public static void debug(Class<?> pClasse, String pMethode, String pMessage) {
    if (Logger.getRootLogger().getLevel() != Level.DEBUG) {
      return;
    }
    if (pClasse != null) {
      logger.debug(TABULATION + "[" + pClasse.getSimpleName() + "." + pMethode + "] " + pMessage);
    }
    else {
      logger.debug(TABULATION + "[" + pMethode + "] " + pMessage);
    }
  }
  
  /**
   * Tracer un message de debug en indiquant la classe, la méthode et un message.
   * Pour le nom de la classe, il est recommandé d'utiliser [NomClasse].class.
   */
  public static void debug(Class<?> pClasse, String pMethode, Object... pValeur) {
    if (Logger.getRootLogger().getLevel() != Level.DEBUG) {
      return;
    }
    
    int index = 0;
    StringBuffer texte = new StringBuffer("");
    
    // La première valeur du tableau est un commentaire si le tableau a une taille impair
    if ((pValeur.length & 1) == 1) {
      texte.append(pValeur[index++].toString() + " : ");
    }
    
    // Les couples de valeurs suivants sont sont la forme "Nom valeur=valeur"
    while (index < pValeur.length) {
      // Ajouter le nom de la valeur
      if (pValeur[index] != null) {
        texte.append(pValeur[index].toString() + "=");
      }
      else {
        texte.append("null=");
      }
      index++;
      
      // Ajouter la valeur
      if (pValeur[index] != null) {
        texte.append(pValeur[index].toString() + " ");
      }
      else {
        texte.append("null ");
      }
      index++;
    }
    
    // Générer la trace (en se protégeant contre une éventuelle classe null)
    if (pClasse != null) {
      logger.debug(TABULATION + "[" + pClasse.getSimpleName() + "." + pMethode + "] " + texte);
    }
    else {
      logger.debug(TABULATION + "[" + pMethode + "] " + texte);
    }
  }
  
  /**
   * Tracer les informations sur l'usage de la mémoire (en niveau débug).
   * Exemple de trace : "[ClientSerieN.FermerApplication] 25 de 56 Mo / 247 Mo max"
   */
  public static void debugMemoire(Class<?> pClasse, String pMethode) {
    if (Logger.getRootLogger().getLevel() != Level.DEBUG) {
      return;
    }
    
    long totalMemory = Runtime.getRuntime().totalMemory() / 1024 / 1024;
    long freeMemory = Runtime.getRuntime().freeMemory() / 1024 / 1024;
    long usedMemory = totalMemory - freeMemory;
    long maxMemory = Runtime.getRuntime().maxMemory() / 1024 / 1024;
    debug(pClasse, pMethode, "" + usedMemory + " de " + totalMemory + " Mo / " + maxMemory + " Mo max");
  }
  
  /**
   * Tracer la stacktrace de la thread courante.
   * Les traces sont générées avec le niveau debug.
   */
  public static void debugStackTrace() {
    for (StackTraceElement stackTraceElement : Thread.currentThread().getStackTrace()) {
      debug(stackTraceElement.toString());
    }
  }
  
  /**
   * Tracer un message d'information.
   * Ce message doit être simple et exprimé clairement.
   */
  public static void info(String pMessage) {
    logger.info(TABULATION + pMessage);
  }
  
  /**
   * Tracer un message d'avertissement.
   */
  public static void alerte(String message) {
    logger.warn(TABULATION + message);
  }
  
  /**
   * Tracer un message d'erreur.
   */
  public static void erreur(String message) {
    logger.error(TABULATION + message);
  }
  
  /**
   * Tracer un message d'erreur accompagné d'une exception.
   * Les messages empilés dans la pile d'exceptions sont ajoutés au message initial.
   */
  public static void erreur(Throwable e, String message) {
    logger.error(TABULATION + formaterMessageException(message), e);
  }
  
  /**
   * Tracer une erreur fatale impliquant l'arrêt du logiciel.
   */
  public static void fatal(String message) {
    logger.fatal(TABULATION + message);
  }
  
  /**
   * Tracer une erreur fatale impliquant l'arrêt du logiciel.
   * Les messages empilés dans la pile d'exceptions sont ajoutés au message initial.
   */
  public static void fatal(Throwable e, String message) {
    logger.fatal(TABULATION + formaterMessageException(message), e);
  }
  
  /**
   * Chemin du dossier contenant les traces du logiciel.
   */
  public static String getDossierTraces() {
    return dossierRacine + File.separator + DOSSIER_LOG;
  }
  
  /**
   * Nom complet du fichier traces en cours d'écriture.
   */
  private static String getFichierTraceEncours() {
    return getDossierTraces() + File.separator + fichier + EXTENSION_LOG;
  }
  
  /**
   * Nom complet du fichier console d'écriture.
   */
  private static String getFichierConsole() {
    return getDossierTraces() + File.separator + fichierConsole + EXTENSION_LOG;
  }
  
  /**
   * Formater la présentation d'un message et d'une exception pour le tracer.
   */
  private static String formaterMessageException(String message) {
    if (message == null) {
      return "";
    }
    return message.replaceAll("(\\r|\\n)", "").trim();
  }
  
  /**
   * Permet de tracer lors de la recherche d'un problème de lenteur.
   * Cette méthode s'utilise en 2 temps afin de calculer le delta du temps passé.
   * Affiche des marqueurs personnalisés avec les temps et l'occupation mémoire.
   * 
   * @param pFin indique s'il s'agit du dernier appel de la méthode.
   * @param pNomMethode nom de la méthode dans laquelle tracerPourDebug est appelée.
   * @param pClasse la classe dans laquelle tracerPourDebug est appelée.
   * @param pTemps le temps retourné lors du premier appel par tracerPourDebug, -1 sinon.
   * @return la difference, mesurée en millisecondes, entre maintenant et le 1 janvier 1970 à 00:00:00 UTC.
   */
  public static long tracerPourDebug(boolean pFin, String pNomMethode, Class pClasse, long pTemps) {
    String pNomFormat = pClasse.getName();
    long t = System.currentTimeMillis();
    
    // Marqueur calcul occupation mémoire
    long totalMemory = Runtime.getRuntime().totalMemory() / 1024 / 1024;
    long freeMemory = Runtime.getRuntime().freeMemory() / 1024 / 1024;
    long usedMemory = totalMemory - freeMemory;
    long maxMemory = Runtime.getRuntime().maxMemory() / 1024 / 1024;
    String memoire = " - Mémoire: " + usedMemory + " de " + totalMemory + " Mo / " + maxMemory + " Mo max";
    
    // Marqueur de début
    if (!pFin && pTemps < 0) {
      Trace.info("** " + pNomFormat + " " + pNomMethode + " début" + memoire);
      return t;
    }
    
    // Marqueur intermédiaire
    String duree = " - Durée: " + (t - pTemps) + " ms";
    if (!pFin) {
      Trace.info("** " + pNomFormat + " " + pNomMethode + " inter" + memoire + duree);
    }
    // Marqueur de fin
    else {
      Trace.info("** " + pNomFormat + " " + pNomMethode + " fin" + "  " + memoire + duree);
    }
    
    return t;
  }
}
