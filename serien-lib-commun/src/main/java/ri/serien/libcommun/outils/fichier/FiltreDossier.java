/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.io.File;
import java.io.FilenameFilter;

public class FiltreDossier implements FilenameFilter {
  // @Override
  public boolean accept(File dir, String name) {
    File folder = new File(dir.getAbsoluteFile() + File.separator + name);
    return (folder.isDirectory());
  }
}
