/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.protocolemessage;

import java.util.ArrayList;

public class DataareaRequest extends BaseRequest {
  // Constantes
  public static final int ISEXISTS = 1;
  public static final int CREATE = 2;
  public static final int READ = 4;
  public static final int WRITE = 8;
  public static final int LIST = 16;
  // Mis en commentaire car en V5R4 et version en dessous l'API de Google plante avec des tableaux
  // public static final int[] ACTIONS={NOACTION, GETTEXT, CREATE, ISEXISTS, LIST, LISTINTO};
  public static final ArrayList<Integer> ACTIONS = new ArrayList<Integer>() {
    {
      add(NOACTION);
    }
    
    {
      add(ISEXISTS);
    }
    
    {
      add(CREATE);
    }
    
    {
      add(READ);
    }
    
    {
      add(WRITE);
    }
    // { add(LIST); }
  };
  
  // Variables
  private String dataarea = null;
  private String library = null;
  private String data = null;
  private String text = null;
  private boolean exist = false;
  private String typeVariant = null;
  // Permet de stocker des données différentes défini par le typeVariant
  private ArrayList<Object> variant = new ArrayList<Object>();
  
  /**
   * Constructeur.
   */
  public DataareaRequest() {
  }
  
  /**
   * Constructeur.
   */
  public DataareaRequest(String adataarea, String alibrary, int anaction) {
    setDataarea(adataarea);
    setLibrary(alibrary);
    setActions(anaction);
  }
  
  // --> Méthodes publiques <------------------------------------------------
  
  // --> Méthodes privées <--------------------------------------------------
  
  // --> Accesseurs <--------------------------------------------------------
  
  public String getDataarea() {
    return dataarea;
  }
  
  /**
   * Renseigner le dataarea.
   */
  public void setDataarea(String dataarea) {
    if (dataarea != null) {
      this.dataarea = dataarea.trim();
      
    }
    else {
      this.dataarea = dataarea;
    }
  }
  
  public String getLibrary() {
    return library;
  }
  
  /**
   * Renseigner la library.
   */
  public void setLibrary(String library) {
    if (library != null) {
      this.library = library.trim();
    }
    else {
      this.library = library;
    }
  }
  
  public String getData() {
    return data;
  }
  
  public void setData(String data) {
    this.data = data;
  }
  
  public String getText() {
    return text;
  }
  
  public void setText(String text) {
    this.text = text;
  }
  
  public boolean isExist() {
    return exist;
  }
  
  public void setExist(boolean exist) {
    this.exist = exist;
  }
  
  public String getTypeVariant() {
    return typeVariant;
  }
  
  public void setTypeVariant(String typeVariant) {
    this.typeVariant = typeVariant;
  }
  
  public ArrayList<Object> getVariant() {
    return variant;
  }
  
  public void setVariant(ArrayList<Object> variant) {
    this.variant = variant;
  }
}
