/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;

import javax.swing.JFileChooser;

import ri.serien.libcommun.outils.Constantes;

/**
 * Gestion des fichiers XML.
 * A modifier comme GestionFichierTexte.
 */
public class GestionFichierXML {
  // Constantes erreurs de chargement
  private static final String ERREUR_FICHIER_INTROUVABLE = "Le fichier est introuvable.";
  private static final String ERREUR_LECTURE_FICHIER = "Erreur lors de la lecture du fichier.";
  private static final String ERREUR_ENREGISTREMENT_FICHIER = "Erreur lors de l'enregistrement du fichier.";
  private static final String ERREUR_BUFFER_VIDE = "Le buffer est vide.";
  
  // Constantes
  private static final String FILTER_EXT_XML = "xml";
  private static final String FILTER_DESC_XML = "Fichier XML (*.xml)";
  
  // Variables
  private File nomFichier = null;
  private URL urlFichier = null;
  private Object contenuFichier = null;
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur.
   */
  public GestionFichierXML() {
  }
  
  /**
   * Constructeur.
   */
  public GestionFichierXML(String nomfichier) {
    setNomFichier(nomfichier);
  }
  
  /**
   * Constructeur.
   */
  public GestionFichierXML(File fichier) {
    setNomFichier(fichier.getAbsolutePath());
  }
  
  /**
   * Constructeur de la classe.
   */
  public GestionFichierXML(URL urlFichier) {
    nomFichier = null;
    this.urlFichier = urlFichier;
  }
  
  /**
   * Initialise le nom du fichier.
   */
  public void setNomFichier(File nomFichier) {
    this.nomFichier = nomFichier;
  }
  
  /**
   * Initialise le nom du fichier.
   */
  public void setNomFichier(String nomFichier) {
    if (nomFichier == null) {
      return;
    }
    this.nomFichier = new File(nomFichier);
  }
  
  /**
   * Retourne le nom du fichier.
   */
  public String getNomFichier() {
    return nomFichier.getAbsolutePath();
  }
  
  /**
   * Vérifie l'existence d'un fichier.
   */
  private boolean isPresent() {
    if (nomFichier == null) {
      return false;
    }
    
    return nomFichier.exists();
  }
  
  /**
   * Initialise le buffer.
   */
  public void setContenuFichier(Object contenuFichier) {
    this.contenuFichier = contenuFichier;
  }
  
  /**
   * Retourne le buffer.
   */
  public Object getContenuFichier() {
    if (contenuFichier == null) {
      if (lectureFichier() == Constantes.ERREUR) {
        return null;
      }
    }
    
    return this.contenuFichier;
  }
  
  /**
   * Lecture du fichier XML.
   */
  public int lectureFichier() {
    XMLDecoder decoder = null;
    
    // On vérifie que le fichier existe
    if ((urlFichier == null) && !isPresent()) {
      msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + ERREUR_FICHIER_INTROUVABLE;
      return Constantes.FALSE;
    }
    
    // Lecture du fichier XML
    try {
      if (nomFichier != null) {
        decoder = new XMLDecoder(new FileInputStream(nomFichier));
      }
      else {
        decoder = new XMLDecoder(urlFichier.openStream());
      }
      contenuFichier = decoder.readObject();
      decoder.close();
    }
    catch (Exception e) {
      if (decoder != null) {
        decoder.close();
      }
      msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + e;
      return Constantes.FALSE;
    }
    
    return Constantes.OK;
  }
  
  /**
   * Ecrit le fichier texte.
   */
  public int ecritureFichier() {
    XMLEncoder encoder = null;
    
    if (contenuFichier == null) {
      msgErreur = ERREUR_BUFFER_VIDE;
      return Constantes.ERREUR;
    }
    
    // On créé le fichier
    try {
      encoder = new XMLEncoder(new FileOutputStream(nomFichier));
      // serialisation de l'objet
      encoder.writeObject(contenuFichier);
      encoder.flush();
      encoder.close();
    }
    catch (Exception e) {
      if (encoder != null) {
        encoder.close();
      }
      msgErreur = ERREUR_ENREGISTREMENT_FICHIER + Constantes.crlf + e;
      return Constantes.ERREUR;
    }
    
    return Constantes.OK;
  }
  
  /**
   * Retourne les filtres possible pour les boites de dialogue.
   */
  public void initFiltre(JFileChooser jfc) {
    jfc.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { FILTER_EXT_XML }, FILTER_DESC_XML));
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
