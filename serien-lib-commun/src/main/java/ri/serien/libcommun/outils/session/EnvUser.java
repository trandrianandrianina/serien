/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session;

import java.awt.GraphicsEnvironment;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.edition.ServerEditionInfos;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.exploitation.menu.MenuDetail;
import ri.serien.libcommun.exploitation.version.Version;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.GestionFichierEQU;

/**
 * Gestion de l'environnement de l'utilisateur sur l'I5.
 * @todo Faire un test sur la validité de la chaine pour le constructeur.
 * @todo Améliorer set2Psemussm via une descption externe.
 * @todo Améliorer la gestion de la définition de la police (faire un popup pour personnalisation + stockage par
 *       profil).
 */
public class EnvUser extends User {
  // Constantes
  // Délai d'attente pour la reconnexion avant d'envoyer un message à l'utilisateur
  public static final int DELAY_WAIT = 30;
  
  // Sécurités profil
  public static final char TOUS_LES_DROITS = ' '; // Administrateur (droit à tout)
  public static final char DROITS_RESTREINTS = '1';
  public static final char DROITS_RESTREINTS_SAUF_CHGBIB = '2';
  // Administrateur ASP droit à tout sauf le changement de bibliothèques
  public static final char TOUS_LES_DROITS_ASP = '3';
  
  // Variables
  // TODO Intégrer la classe SousEnvironnement
  private IdClientDesktop idClientDesktop = null;
  private IdSousEnvironnement idSousEnvironnement = null; // Nom du sous environnement sélectionné
  private String prefixdevice = ""; // Préfixe pour le nom des DTAQs & des JOBs
  private String device = ""; // Nom de la device (DataQueue) sur 9 carcatères max (donc sans le 'S' ou le 'R')
  private String host = null; // Adresse (IP) de l'AS400 sur lequel l'utilisateur va travailler
  private String nomSousSysteme = ""; // Nom du sous système dans lequelle l'utilisateur va travailler
  private String program = ""; // Programme de démarrage pour la session Menu
  private Bibliotheque bibliothequeEnvironnementClient = null; // Bibliothèque d'environnement client
  private Bibliotheque bibliothequeEnvironnement = null; // Bibliothèque de l'environnement de démarrage
  private String folder = ""; // Nom du dossier contenant les runtimes sur le serveur
  private BDD curlib = null; // La curlib de l'utilisateur
  private BDD curlibParDefault = null; // Bibliothèque fichier par défaut de l'utilisateur
  protected String texttitlebarinformations = ""; // Texte contenant l'établissement et le magasin
  protected char letter = ' '; // Lettre pour l'ASP (bibliothèques)
  private boolean titleBarInformations = false;
  private String typemenu = null; // Le type de menu utilisé par l'environnement définit dans le rt.ini
  
  private String menuPersonnalise = ""; // Menu personnalisé
  private char cUSSPOR = ' '; // Possibilités restreintes
  private char cUSSDSP = ' '; // Type émulation
  protected ArrayList<String> listeBibSpe = new ArrayList<String>();
  
  private String serveurSGM = null; // Adresse IP du serveur SGM
  private int portSGM = -1; // Port du serveur SGM
  private InetAddress iaServeurSGM = null; // InetAdresse du serveur SGM
  private String fichierTraduction = Constantes.INIT_TRADUCTION;
  private String langue = "fr";
  private HashMap<String, String> translationtable = null;
  private HashMap<String, String> libimg = null;
  private int deltaPersoTailleFont = 0;
  private HashMap<String, String> config = null;
  private String versionServeur = null;
  private String[] availFonts = null;
  private ServerEditionInfos infosServerEdition = null; // Contient les infos sur le serveur Newsim
  // Indique si l'utilisateur souhaite continuer à attendre que la reconnexion se fasse
  private boolean stopReconnexion = false;
  // Comptabilise le nombre de sessions à reconnecter (si =0 alors les connexions sont ok)
  private byte compteurReconnexion = 0;
  private String deviceImpression = null;
  // Indique si le profil est présent dans la table PSEMUSSM de la bibliothèque
  private boolean utilisableAvecLibraryEnv = false;
  private Dossier dossierDesktop = null;
  private Dossier dossierServeur = null;
  private Version versionSerieN = null;
  private boolean debugstatus = true;
  private List<Bibliotheque> listeBibliothequeUtilisateur = null;
  
  private transient SessionTransfert trfSession = null;
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  public EnvUser() {
    dossierDesktop = new Dossier(false, id);
    dossierServeur = new Dossier(true, id);
  }
  
  /**
   * Retourne l'id du client desktop.
   */
  public IdClientDesktop getIdClientDesktop() {
    return idClientDesktop;
  }
  
  /**
   * Initialise l'id du client desktop.
   */
  public void setIdClientDesktop(IdClientDesktop idClientDesktop) {
    this.idClientDesktop = idClientDesktop;
  }
  
  /**
   * Retourne le nom du menu personnalisé s'il y en a un, vide sinon.
   */
  public String retournerNomMenuPersonnalise() {
    if (menuPersonnalise.isEmpty()) {
      return "";
    }
    return menuPersonnalise + MenuDetail.EXTENSION_MENU;
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise le dossier travail.
   */
  public void setDossierTravail(String pValeur) {
    dossierDesktop.initialiserDossierTravail(pValeur);
    dossierServeur.initialiserDossierTravail(pValeur);
  }
  
  public Dossier getDossierDesktop() {
    return dossierDesktop;
  }
  
  public Dossier getDossierServeur() {
    return dossierServeur;
  }
  
  // -- Méthodes privées
  
  /**
   * Liste les polices disponibles sur le système.
   */
  private void listFontSystem() {
    GraphicsEnvironment graphicsEvn = GraphicsEnvironment.getLocalGraphicsEnvironment();
    availFonts = graphicsEvn.getAvailableFontFamilyNames();
  }
  
  // -- Accesseurs
  
  /**
   * Initialise le nom de la Config.
   */
  public void setIdSousEnvironnement(IdSousEnvironnement pIdSousEnvironnement) {
    idSousEnvironnement = pIdSousEnvironnement;
    if (folder == null || folder.trim().equals("")) {
      setFolder(idSousEnvironnement.getNom());
    }
  }
  
  /**
   * Initialise la device.
   */
  public void setDevice(String valeur) {
    device = valeur.trim();
  }
  
  /**
   * Initialise le préfixe de la device.
   */
  public void setPrefixDevice(String valeur) {
    if (valeur != null) {
      prefixdevice = valeur.trim();
    }
  }
  
  /**
   * Initialise le nom du Host.
   */
  public void setHost(String valeur) {
    if (valeur != null) {
      host = valeur.trim();
    }
  }
  
  /**
   * Initialise la jobq.
   */
  public void setNomSousSysteme(String pNomSousSysteme) {
    if (pNomSousSysteme != null) {
      nomSousSysteme = pNomSousSysteme.trim();
    }
  }
  
  /**
   * Initialise le programme initial.
   */
  public void setProgram(String valeur) {
    if (valeur != null) {
      program = valeur.trim();
    }
  }
  
  /**
   * Initialise la bibliothèque d'environnement du client.
   */
  public void setBibliothequeEnvironnementClient(Bibliotheque pBibliotheque) {
    if (pBibliotheque != null) {
      bibliothequeEnvironnementClient = pBibliotheque;
    }
  }
  
  /**
   * Initialise la bibliothèque d'environnement.
   */
  public void setBibliothequeEnvironnement(Bibliotheque pBibliotheque) {
    if (pBibliotheque != null) {
      bibliothequeEnvironnement = pBibliotheque;
    }
  }
  
  /**
   * Initialise l'adresse IP ou host du serveur SGM.
   */
  public void setServeurSGM(String valeur) {
    if (valeur != null) {
      serveurSGM = valeur.trim();
      try {
        setIa_serveurSGM(InetAddress.getByName(serveurSGM));
      }
      catch (UnknownHostException e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * Initialise le port du serveur SGM.
   */
  public void setPortSGM(int valeur) {
    portSGM = valeur;
  }
  
  /**
   * Initialisation la session transfert.
   */
  public void initTransfertSession(SousEnvironnement pSousEnvironnementServeur, SousEnvironnement pSousEnvironnementClient) {
    trfSession = new SessionTransfert();
    trfSession.setSousEnvironnement(pSousEnvironnementServeur, pSousEnvironnementClient);
  }
  
  /**
   * Initialise le fichier de traduction.
   */
  public void setTranslationTable() {
    try {
      // Lecture du fichier des traductions
      GestionFichierEQU gfe = new GestionFichierEQU(this.getClass().getClassLoader().getResource(fichierTraduction));
      if (gfe.lectureFichier() == Constantes.OK) {
        translationtable = gfe.getListe(true);
      }
      else {
        Trace.erreur(gfe.getMsgErreur());
      }
      // Dans tous les cas on part pas avec la liste vide (sert plus tard, en vue d'éviter un test sur null dans oData
      // notamment)
      if (translationtable == null) {
        translationtable = new HashMap<String, String>();
      }
    }
    catch (Exception e) {
      throw new MessageErreurException("Le fichier de la table de traduction n'a pas pu être chargé.");
    }
  }
  
  /**
   * Initialise le fichier de traduction des images (équivalence V07F).
   */
  public void setTranslationImage() {
    // Lecture du fichier des équivalences
    try {
      GestionFichierEQU gfe = new GestionFichierEQU(this.getClass().getClassLoader().getResource(Constantes.INIT_LIBIMG));
      if (gfe.lectureFichier() == Constantes.OK) {
        libimg = gfe.getListe(true);
      }
      else {
        Trace.erreur(gfe.getMsgErreur());
      }
    }
    catch (Exception e) {
      throw new MessageErreurException("Le fichier de traduction des images n'a pas pu être chargé.");
    }
  }
  
  /**
   * Initialise la langue.
   */
  public void setLangue(String lg) {
    int pos = fichierTraduction.indexOf('_');
    
    langue = lg.trim();
    
    // On modifie le nom du fichier en conséquence
    if (pos == -1) {
      return;
    }
    fichierTraduction = fichierTraduction.substring(0, pos + 1) + langue + fichierTraduction.substring(pos + 1 + langue.length());
    if (trfSession != null) {
      setTranslationTable();
    }
  }
  
  /**
   * Initialise le delta pour la personnalisation de la taille de la police.
   */
  public void setDeltaPersoTailleFont(int delta) {
    deltaPersoTailleFont = delta;
  }
  
  /**
   * Retourne le nom de la Config.
   */
  public IdSousEnvironnement getIdSousEnvironnement() {
    return idSousEnvironnement;
  }
  
  /**
   * Retourne la device.
   */
  public String getDevice() {
    return device;
  }
  
  /**
   * Retourne le préfixe de la device.
   */
  public String getPrefixDevice() {
    if (prefixdevice == null || prefixdevice.isEmpty()) {
      return "QPADEV";
    }
    else {
      return prefixdevice;
    }
  }
  
  /**
   * Retourne le nom du Host.
   */
  public String getHost() {
    return host;
  }
  
  /**
   * Retourne le nom dud sous système.
   */
  public String getNomSousSysteme() {
    return nomSousSysteme;
  }
  
  /**
   * Retourne le programme initial.
   */
  public String getProgram() {
    return program;
  }
  
  /**
   * Retourne la bibliothèque d'environnement du client.
   */
  public Bibliotheque getBibliothequeEnvironnementClient() {
    return bibliothequeEnvironnementClient;
  }
  
  /**
   * Retourne la bibliothèque d'environnement.
   */
  public Bibliotheque getBibliothequeEnvironnement() {
    return bibliothequeEnvironnement;
  }
  
  /**
   * @return le folder.
   */
  public String getFolder() {
    return folder;
  }
  
  /**
   * @param folder le folder à définir.
   */
  public void setFolder(String folder) {
    this.folder = folder;
  }
  
  /**
   * Retourne la lettre d'environnement Série N.
   */
  public char getLetter() {
    return letter;
  }
  
  /**
   * @param listeBibSpe the listeBibSpe to set.
   */
  public void setListeBibSpe(ArrayList<String> listeBibSpe) {
    this.listeBibSpe = listeBibSpe;
  }
  
  public ArrayList<String> getListeBibSpe() {
    return listeBibSpe;
  }
  
  /**
   * Retourne l'adresse IP ou host du serveur Série N.
   */
  public String getServeurSGM() {
    return serveurSGM;
  }
  
  /**
   * @return le ia_serveurSGM.
   */
  public InetAddress getIa_serveurSGM() {
    return iaServeurSGM;
  }
  
  /**
   * @param iaServeurSGM le ia_serveurSGM à définir.
   */
  public void setIa_serveurSGM(InetAddress iaServeurSGM) {
    this.iaServeurSGM = iaServeurSGM;
  }
  
  /**
   * Retourne le port du serveur SGM.
   */
  public int getPortSGM() {
    return portSGM;
  }
  
  /**
   * Retourne la session transfert.
   */
  public SessionTransfert getTransfertSession() {
    return trfSession;
  }
  
  /**
   * Retourne la table de traduction.
   */
  public HashMap<String, String> getTranslationTable() {
    return translationtable;
  }
  
  /**
   * Retourne la table de traduction des équivalences.
   */
  public HashMap<String, String> getTranslationImage() {
    return libimg;
  }
  
  /**
   * Retourne la langue utilisé.
   */
  public String getLangue() {
    return langue;
  }
  
  /**
   * Retourne le delta pour la personnalisation de la taille de la police.
   */
  public int getDeltaPersoTailleFont() {
    return deltaPersoTailleFont;
  }
  
  /**
   * Initialise les variables de la config (logo, couleur, etc...).
   * PAS UTILISE voir +tard.
   */
  public void setConfig(HashMap<String, String> config) {
    this.config = config;
  }
  
  /**
   * Retourne les variables de la config (logo, couleur, etc...).
   * PAS UTILISE voir +tard.
   */
  public HashMap<String, String> getConfig() {
    return config;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * @return uSSDSP.
   */
  public char getUSSDSP() {
    return cUSSDSP;
  }
  
  /**
   * @param ussdsp uSSDSP à définir.
   */
  public void setUSSDSP(char ussdsp) {
    cUSSDSP = ussdsp;
  }
  
  public char getUSSPOR() {
    return cUSSPOR;
  }
  
  /**
   * @param usspor uSSPOR à définir.
   */
  public void setUSSPOR(char usspor) {
    cUSSPOR = usspor;
  }
  
  /**
   * @param usspor uSSPOR à définir.
   */
  public void setUSSPOR(String usspor) {
    if (usspor.trim().equals("")) {
      cUSSPOR = ' ';
    }
    else {
      cUSSPOR = usspor.charAt(0);
    }
  }
  
  /**
   * @return le uSSMNP.
   */
  public String getMenuPersonnalise() {
    return menuPersonnalise;
  }
  
  /**
   * @param pUSSMNP le uSSMNP à définir.
   */
  public void setMenuPersonnalise(String pUSSMNP) {
    menuPersonnalise = Constantes.normerTexte(pUSSMNP);
  }
  
  /**
   * @return le versionServeur.
   */
  public String getVersionServeur() {
    return versionServeur;
  }
  
  /**
   * @param versionServeur le versionServeur à définir.
   */
  public void setVersionServeur(String versionServeur) {
    this.versionServeur = versionServeur;
  }
  
  /**
   * Retourne une chaine avec toutes les infos nécessaire pour l'ouverture d'une session.
   */
  public String getDemandeSession(String demandesession) {
    StringBuffer sb = new StringBuffer(1024);
    
    // EnvUser
    sb.append(getIdSousEnvironnement()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getProfil())
        .append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getMotDePasse()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
        .append(getPrefixDevice()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getNomSousSysteme())
        .append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getProgram()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
        .append(getBibliothequeEnvironnement()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getBibliothequeEnvironnementClient())
        .append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getCurlib().getNom()).append(Constantes.SEPARATEUR_CHAINE_CHAR)
        .append(getFolder()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(getDeviceImpression())
        .append(Constantes.SEPARATEUR_CHAINE_CHAR)
        // EnvProgram
        .append(demandesession);
    return sb.toString();
  }
  
  /**
   * Recherche si un police donnée est bien presente sur le système.
   */
  public boolean isFontPresent(String font) {
    if (font == null) {
      return false;
    }
    font = font.trim();
    if (availFonts == null) {
      listFontSystem();
    }
    for (int i = 0; i < availFonts.length; i++) {
      if (font.equalsIgnoreCase(availFonts[i])) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Vérifie que le profil soit bien valide, notamment que ce soit un profil Série N.
   */
  public boolean isProfilEstGraphique() {
    return (getUSSDSP() == 'N');
  }
  
  /**
   * Vérifie que le profil soit bien valide, notamment que la curlib soit bien renseignée.
   */
  public boolean isCurlibValide() {
    if (curlib == null) {
      return false;
    }
    return true;
  }
  
  /**
   * Contrôle que le profil soit utilisable pour la version graphique.
   */
  public void controlerProfil() {
    if (!utilisableAvecLibraryEnv) {
      throw new MessageErreurException(
          "Cet utilisateur n'est pas utilisable car il n'est pas lié à la bilbiothèque d'environnement " + bibliothequeEnvironnement);
    }
    if (!isCurlibValide()) {
      throw new MessageErreurException("Cet utilisateur n'est lié à aucune bibliothèque fichier (CURLIB)");
    }
    if (!isProfilEstGraphique()) {
      throw new MessageErreurException("Cet utilisateur n'est pas configuré pour utiliser la version graphique de Série N");
    }
  }
  
  /**
   * @param infosServerEdition the infosServerEdition to set.
   */
  public void setInfosServerEdition(ServerEditionInfos infosServerEdition) {
    this.infosServerEdition = infosServerEdition;
  }
  
  public ServerEditionInfos getInfosServerEdition() {
    if (infosServerEdition == null) {
      infosServerEdition = new ServerEditionInfos();
    }
    return infosServerEdition;
  }
  
  /**
   * Retourner le dossier racine côté Serveur.
   */
  /*
  public String getDossierRacineServeur() {
    if (dossierRacineServeur == null) {
      dossierRacineServeur = '/' + getLetter() + Constantes.DOSSIER_RACINE;
      Trace.info("Le dossier racine du serveur n'est pas renseigné, on l'initialise avec " + dossierRacineServeur);
    }
    return dossierRacineServeur;
  }*/
  
  /**
   * @return le titleBarInformations.
   */
  public boolean isTitleBarInformations() {
    return titleBarInformations;
  }
  
  /**
   * @param titleBarInformations le titleBarInformations à définir.
   */
  public void setTitleBarInformations(boolean titleBarInformations) {
    this.titleBarInformations = titleBarInformations;
  }
  
  /**
   * @return le texttitlebarinformations.
   */
  public String getTexttitlebarinformations() {
    return texttitlebarinformations;
  }
  
  /**
   * texttitlebarinformations à définir.
   */
  public void setTexttitlebarinformations(String valeur) {
    if (valeur != null) {
      texttitlebarinformations = valeur.trim();
    }
    else {
      texttitlebarinformations = "";
    }
  }
  
  /**
   * @return le typemenu.
   */
  public String getTypemenu() {
    return typemenu;
  }
  
  /**
   * @param typemenu le typemenu à définir.
   */
  public void setTypemenu(String typemenu) {
    this.typemenu = typemenu;
  }
  
  /**
   * @return le stopReconnexion.
   */
  public boolean isStopReconnexion() {
    return stopReconnexion;
  }
  
  /**
   * @param stopReconnexion le stopReconnexion à définir.
   */
  public void setStopReconnexion(boolean stopReconnexion) {
    this.stopReconnexion = stopReconnexion;
  }
  
  /**
   * @return le compteurReconnexion.
   */
  public byte getCompteurReconnexion() {
    return compteurReconnexion;
  }
  
  /**
   * Incrémente le compteur des reconnexions.
   */
  public void incCompteurReconnexion() {
    this.compteurReconnexion++;
    if (compteurReconnexion > Constantes.MAXRECONNEXION) {
      setStopReconnexion(true);
    }
  }
  
  /**
   * Remet à 0 le compteur des reconnexions.
   */
  public void resetCompteurReconnexion() {
    this.compteurReconnexion = 0;
  }
  
  /**
   * @return le deviceImpression.
   */
  public String getDeviceImpression() {
    return deviceImpression;
  }
  
  /**
   * @param deviceImpression le deviceImpression à définir.
   */
  public void setDeviceImpression(String deviceImpression) {
    this.deviceImpression = deviceImpression;
  }
  
  public void setUtilisableAvecLibraryEnv(boolean utilisableAvecLibraryEnv) {
    this.utilisableAvecLibraryEnv = utilisableAvecLibraryEnv;
  }
  
  public boolean isUtilisableAvecLibraryEnv() {
    return utilisableAvecLibraryEnv;
  }
  
  /**
   * Libère les ressources.
   */
  @Override
  public void dispose() {
    super.dispose();
    
    if (listeBibSpe != null) {
      listeBibSpe.clear();
      listeBibSpe = null;
    }
    if (translationtable != null) {
      translationtable.clear();
      translationtable = null;
    }
    if (libimg != null) {
      libimg.clear();
      libimg = null;
    }
    if (config != null) {
      config.clear();
      config = null;
    }
    if (infosServerEdition != null) {
      infosServerEdition.dispose();
      infosServerEdition = null;
    }
    iaServeurSGM = null;
    trfSession = null;
  }
  
  public void setLetter(char letter) {
    this.letter = letter;
  }
  
  public BDD getCurlib() {
    return curlib;
  }
  
  public void setCurlib(BDD curlib) {
    this.curlib = curlib;
  }
  
  public BDD getCurlibParDefault() {
    return curlibParDefault;
  }
  
  public void setCurlibParDefault(BDD curlibParDefault) {
    this.curlibParDefault = curlibParDefault;
  }
  
  public Version getVersionSerieN() {
    return versionSerieN;
  }
  
  public void setVersionSerieN(Version versionSerieN) {
    this.versionSerieN = versionSerieN;
  }
  
  public boolean isDebugStatus() {
    return debugstatus;
  }
  
  public void setDebugStatus(boolean pDebugStatus) {
    debugstatus = pDebugStatus;
  }
  
  public List<Bibliotheque> getListeBibliothequeUtilisateur() {
    return listeBibliothequeUtilisateur;
  }
  
  public void setListeBibliothequeUtilisateur(List<Bibliotheque> listeBibliothequeUtilisateur) {
    this.listeBibliothequeUtilisateur = listeBibliothequeUtilisateur;
  }
}
