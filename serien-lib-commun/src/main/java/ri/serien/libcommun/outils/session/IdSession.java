/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session;

import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * L'identifiant unique d'une session.
 *
 * L'identifiant est composé d'un numéro de session.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdSession extends AbstractId {
  // Constantes
  public static final IdSession AUCUNE_SESSION = new IdSession();
  
  // Variables
  private final IdClientDesktop idClientDesktop;
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdSession(EnumEtatObjetMetier pEnumEtatObjetMetier, IdClientDesktop pIdClientDesktop, Integer pNumero) {
    super(pEnumEtatObjetMetier);
    numero = controlerNumero(pNumero);
    idClientDesktop = pIdClientDesktop;
  }
  
  /**
   * Constructeur privé pour la session nulle.
   */
  private IdSession() {
    super(EnumEtatObjetMetier.CREE);
    numero = 0;
    idClientDesktop = IdClientDesktop.AUCUN;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdSession getInstance(IdClientDesktop pIdClientDesktop, Integer pNumero) {
    return new IdSession(EnumEtatObjetMetier.CREE, pIdClientDesktop, pNumero);
  }
  
  /**
   * Contrôler la validité du numéro.
   * Le numéro doit être compris entre 1 et 9 999 inclus.
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de session n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro de session est inférieur ou égal à zéro.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdSession controlerId(IdSession pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de la session est invalide.");
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + idClientDesktop.hashCode();
    cle = 37 * cle + numero.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdSession)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de session.");
    }
    IdSession id = (IdSession) pObject;
    return idClientDesktop.equals(id.getIdClientDesktop()) && numero.equals(id.getNumero());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdSession)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdSession id = (IdSession) pObject;
    int comparaison = idClientDesktop.compareTo(id.idClientDesktop);
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return idClientDesktop.toString() + SEPARATEUR_ID + numero;
  }
  
  // Accesseurs
  
  /**
   * Identifiant du client desktop.
   */
  public IdClientDesktop getIdClientDesktop() {
    return idClientDesktop;
  }
  
  /**
   * Numéro de la session.
   * Le numéro est un nombre entier supérieur à zéro.
   */
  public Integer getNumero() {
    return numero;
  }
}
