/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.protocolemessage;

import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.encodage.Base64Coder;

/**
 * Description du message complet.
 */
public class MessageManager {
  private MessageHead head = null;
  private Object body = null;
  private Class<?> classbody = null;
  
  private static transient int id = 0;
  private transient int hashcodecaller = 0;
  private transient Gson gson = new Gson();
  private transient JsonParser parser = new JsonParser();
  private transient StringBuilder sb = new StringBuilder();
  
  /**
   * Constructeur.
   */
  public MessageManager(int ahashcodecaller) {
    hashcodecaller = ahashcodecaller;
  }
  
  /**
   * Génère un message complet pour un message de type String.
   */
  public int createMessage(boolean simpleString, String message) {
    if (head == null) {
      head = new MessageHead();
      head.setId(id++);
    }
    if (hashcodecaller != 0) {
      head.setHashcodeCaller(hashcodecaller);
    }
    if ((message == null) || (message.length() == 0)) {
      head.setBodycode(MessageHead.BODY_NO);
      head.setBodydesc(null);
      head.setBodylength(0);
      head.setBodycomp(false);
    }
    else {
      if (simpleString) {
        head.setBodycode(MessageHead.BODY_SIMPLESTRING);
      }
      else {
        head.setBodycode(MessageHead.BODY_COMPLEXSTRING);
      }
      head.setBodylength(message.length());
      head.setBodydesc(message);
      if (head.getBodylength() > Constantes.TAILLE_BUFFER) {
        head.setBodycomp(true);
      }
      else {
        head.setBodycomp(false);
      }
    }
    body = null;
    return head.getId();
  }
  
  /**
   * Génère un message complet pour un message de type String.
   */
  public int createMessage(boolean compress, boolean simpleString, String message) {
    createMessage(simpleString, message);
    head.setBodycomp(compress);
    return head.getId();
  }
  
  /**
   * Génère un message complet pour un message de type JSON.
   */
  public int createMessage(Object message) {
    if (head == null) {
      head = new MessageHead();
      head.setId(id++);
    }
    if (hashcodecaller != 0) {
      head.setHashcodeCaller(hashcodecaller);
    }
    head.setBodycode(MessageHead.BODY_JSON);
    if (message == null) {
      return Constantes.ERREUR;
    }
    head.setBodydesc(message.getClass().getName());
    body = message;
    
    return head.getId();
  }
  
  /**
   * Génère un message complet pour un message de type JSON.
   */
  public int createMessage(boolean compress, Object message) {
    int idmsg = createMessage(message);
    head.setBodycomp(compress);
    
    return idmsg;
  }
  
  /**
   * Retourne une chaine contenant le message sous forme d'objet JSON.
   */
  public String getMessageToSend() {
    sb.setLength(0);
    sb.append("{\"head\":");
    // C'est ici que l'on prépare le message à envoyer
    // Notamment si l'option compress a été activé
    if (head.isBodycomp()) {
      if (head.getBodycode() == MessageHead.BODY_JSON) {
        String chaine = gson.toJson(body);
        head.setBodylength(chaine.length());
        sb.append(gson.toJson(head)).append(",\"body\":\"").append(encode(chaine)).append('\"');
      }
      else if (head.getBodycode() == MessageHead.BODY_NO) {
        sb.append(gson.toJson(head));
      }
      else {
        head.setBodydesc(encode(head.getBodydesc()));
        sb.append(gson.toJson(head));
      }
    }
    else {
      // Pas de compression active
      if (head.getBodycode() == MessageHead.BODY_JSON) {
        String chaine = gson.toJson(body);
        head.setBodylength(chaine.length());
        sb.append(gson.toJson(head)).append(",\"body\":").append(chaine);
      }
      else {
        sb.append(gson.toJson(head));
      }
    }
    
    return sb.append('}').toString();
  }
  
  /**
   * Retourne le message généré à partir de l'objet JSON.
   * 
   * @param hashcodecaller s'il est égal à zéro alors on en tient pas compte dans le traitement.
   */
  public boolean getMessageReceived(String amsg, int hashcodecaller) {
    if (amsg == null) {
      return false;
    }
    
    boolean ret = true;
    JsonObject obj = (JsonObject) parser.parse(amsg);
    
    // On désérialise l'entête
    head = gson.fromJson(obj.get("head"), MessageHead.class);
    
    // On vérifie que ce message est bien destiné à la bonne classe
    if ((hashcodecaller != 0) && (head.getHashcodeCaller() != hashcodecaller)) {
      return false;
    }
    
    // On désérialise le corps du message en fonction de la version de son protocole
    switch (head.getVersion()) {
      case 1:
        ret = treatmentVersion1(obj);
        break;
      default:
        break;
    }
    return ret;
  }
  
  // --> Méthodes privées <--------------------------------------------------
  
  /**
   * Traitement des messages avec le protocole de la version 1.
   */
  private boolean treatmentVersion1(JsonObject obj) {
    String chaine = null;
    
    // On vérifie s'il y a un objet JSON à désérialiser
    if (head.getBodycode() == MessageHead.BODY_JSON) {
      try {
        classbody = Class.forName(head.getBodydesc());
        // classbody = MessageManager.class.getClassLoader().loadClass(head.getBodydesc());
        if (head.isBodycomp()) {
          chaine = gson.fromJson(obj.get("body"), String.class);
          chaine = decode(chaine, head.getBodylength());
          body = gson.fromJson(chaine, classbody);
        }
        else {
          body = gson.fromJson(obj.get("body"), classbody);
        }
      }
      catch (JsonSyntaxException e) {
        return false;
      }
      catch (ClassNotFoundException e) {
        return false;
      }
      catch (Exception e) {
        e.printStackTrace();
        return false;
      }
    }
    else {
      // Cinon c'est juste une chaine de caractères
      if (head.isBodycomp()) {
        chaine = gson.fromJson(head.getBodydesc(), String.class);
        head.setBodydesc(decode(chaine, head.getBodylength()));
      }
      body = null;
      classbody = null;
    }
    return true;
  }
  
  /**
   * Compresse la chaine avec zip puis l'encode en base64.
   */
  private String encode(String chaine) {
    if (chaine == null) {
      return "";
    }
    Deflater compresser = new Deflater();
    // Car lors de la compression parfois la taille peut etre plus grande que l'originale
    byte[] output = new byte[chaine.length() * 2];
    
    compresser.setInput(chaine.getBytes());
    compresser.finish();
    int compressedDataLength = compresser.deflate(output);
    return Base64Coder.encodeString(output, compressedDataLength);
  }
  
  /**
   * Décompresse la chaine afin de retrouver la valeur originale.
   */
  private String decode(String chaine, int tailleori) {
    if ((chaine == null) || (chaine.equals(""))) {
      return "";
    }
    
    byte[] input = Base64Coder.decode(chaine);
    byte[] output = new byte[tailleori];
    Inflater decompresser = new Inflater();
    decompresser.setInput(input, 0, input.length);
    int compressedDataLength = 0;
    try {
      compressedDataLength = decompresser.inflate(output);
    }
    catch (DataFormatException e) {
      // @todo A gérer correctement.
    }
    decompresser.end();
    return new String(output, 0, compressedDataLength);
  }
  
  // --> Accesseurs <---------------------------------------------------------
  
  /**
   * @return le head.
   */
  public MessageHead getHead() {
    return head;
  }
  
  /**
   * @param head le head à définir.
   */
  public void setHead(MessageHead head) {
    this.head = head;
  }
  
  /**
   * @return le body.
   */
  public Object getBody() {
    return body;
  }
  
  /**
   * @param body le body à définir.
   */
  public void setBody(Object body) {
    this.body = body;
  }
  
  /**
   * @return le classbody.
   */
  public Class<?> getClassbody() {
    return classbody;
  }
  
  /**
   * @param classbody le classbody à définir.
   */
  public void setClassbody(Class<?> classbody) {
    this.classbody = classbody;
  }
  
  /**
   * Libère la mémoire.
   */
  public void dispose() {
    head = null;
    body = null;
    classbody = null;
    gson = null;
    parser = null;
  }
}
