/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.composants;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import ri.serien.libcommun.outils.Constantes;

/**
 * Description de l'objet Data
 */
public class oData extends GenericBase {
  // Constantes erreurs
  // private final static String ERREUR_TYPEVARIABLE_INCONNU="Type de variable inconnu.";
  private final static String ERREUR_SENSVARIABLE_INCONNU = "Sens de variable inconnu.";
  private final static String ERREUR_NOMVARIABLE_INCORRECT = "Nom de variable incorrect.";
  private final static String ERREUR_CONVERT_OFFSET = "OFFSET impossible à convertir en numérique.";
  private final static String ERREUR_CONVERT_LONGUEUR = "LONGUEUR impossible à convertir en numérique.";
  private final static String ERREUR_CONVERT_DECIMALE = "DECIMALE impossible à convertir en numérique.";
  private final static String ERREUR_CONVERT_LIGNE = "LIGNE impossible à convertir en numérique.";
  private final static String ERREUR_CONVERT_COLONNE = "COLONNE impossible à convertir en numérique.";
  private final static String ERREUR_DATA_NULL = "Tableau binaire non initialisé.";
  private final static String ERREUR_LONGUEUR_DATA_ERRONNE = "La longueur du tableau descriptif est supérieure à celle du record.";
  
  // Constantes ID, il y a 500 variables possibles pour l'objet oData
  // Chaque variable doit obligatoirement avoir un ID (pour la compilation)
  private final static int ID_ODATA = 1500;
  private final static int ID_OFFSET = ID_ODATA + 1;
  private final static int ID_LONGUEUR = ID_ODATA + 2;
  private final static int ID_DECIMALE = ID_ODATA + 3;
  private final static int ID_INPUT_CTRL = ID_ODATA + 4;
  private final static int ID_ZONETYPE = ID_ODATA + 5;
  private final static int ID_LIGNE = ID_ODATA + 6;
  private final static int ID_COLONNE = ID_ODATA + 7;
  private final static int ID_VALEUR = ID_ODATA + 8;
  private final static int ID_VISIBILITY = ID_ODATA + 9;
  private final static int ID_READONLY = ID_ODATA + 10;
  private final static int ID_LOWERCASE = ID_ODATA + 11;
  private final static int ID_TRANSLATE = ID_ODATA + 12;
  private final static int ID_OUTPUT_EDT = ID_ODATA + 13;
  private final static int ID_CHECK_EDT = ID_ODATA + 14;
  private final static int ID_FOCUS = ID_ODATA + 15;
  
  // Constantes
  private final static char ICTRL_N = 'N';
  private final static char ICTRL_A = 'A';
  private final static char ICTRL_X = 'X';
  private final static char ICTRL_I = 'I';
  private final static char ICTRL_M = 'M';
  private final static char ICTRL_D = 'D';
  private final static char ICTRL_S = 'S';
  private final static char ICTRL_Y = 'Y';
  
  private final static char OEDT_A = 'A';
  private final static char OEDT_B = 'B';
  private final static char OEDT_C = 'C';
  private final static char OEDT_D = 'D';
  private final static char OEDT_J = 'J';
  private final static char OEDT_K = 'K';
  private final static char OEDT_L = 'L';
  private final static char OEDT_M = 'M';
  private final static char OEDT_N = 'N';
  private final static char OEDT_O = 'O';
  private final static char OEDT_P = 'P';
  private final static char OEDT_Q = 'Q';
  private final static char OEDT_W = 'W';
  private final static char OEDT_Y = 'Y';
  private final static char OEDT_Z = 'Z';
  private final static char OEDT_1 = '1';
  private final static char OEDT_2 = '2';
  private final static char OEDT_3 = '3';
  private final static char OEDT_4 = '4';
  
  private final static String SENS_S_ENTREE = "B";
  public final static byte SENS_ENTREE = 1;
  private final static String SENS_S_SORTIE = "O";
  public final static byte SENS_SORTIE = 2;
  private final static String SENS_S_CACHE = "H";
  public final static byte SENS_CACHE = 3;
  
  private final static String SEPARATEUR_MILLIERS = " ";
  
  // Variables
  private iIndicator iindicateur = null;
  private HashMap<String, String> translationtable = null;
  
  private int offset = -1;
  private int longueur = -1;
  private byte decimale = -1;
  private char input_ctrl = ' ';
  private char output_edt = ' ';
  private byte zonetype = -1;
  private byte ligne = 0;
  private int colonne = 0;
  private String valeur = null;
  private String visibility[] = null;
  private String readonly[] = null;
  private boolean lowercase = false;
  private boolean traduction = false;
  private String check_edt = "";
  private boolean isME = false;
  private boolean isER = false;
  private boolean isMF = false;
  private boolean isFE = false;
  private boolean isRB = false;
  private boolean isRZ = false;
  private boolean isRL = false;
  private boolean isDebug = false;
  private String focus[] = null;
  private int longueur_formatee = -1;
  
  private String nomFormat = null;
  
  /**
   * Initialise le status debug
   * @param debug
   */
  public void setDebug(boolean debug) {
    isDebug = debug;
  }
  
  /**
   * Initialise le nom du Record
   * @param val
   */
  public void setRecord(String val) {
    setParent(val);
  }
  
  /**
   * Initialise l'interpréteur des indicateurs
   * @param aiidicateur
   */
  public void setInterpreteurI(iIndicator aiindicateur) {
    iindicateur = aiindicateur;
  }
  
  /**
   * Retourne l'interpréteur des indicateurs
   * @param aiidicateur
   */
  public iIndicator getInterpreteurI() {
    return iindicateur;
  }
  
  /**
   * Initialise la table des traductions
   * @param atranslationtable
   */
  public void setTranslationTable(HashMap<String, String> atranslationtable) {
    translationtable = atranslationtable;
  }
  
  /**
   * Indique si les coordonnées sont bien dans la zone
   * @param xxlig
   * @param xxcol
   * @param decalageRecord, décalage de lignes pour les popup
   * @return
   */
  public boolean cursorIsInto(int xxlig, int xxcol, int decalageRecord) {
    return (((ligne - decalageRecord + 1) == xxlig) && (colonne <= xxcol) && (xxcol < (colonne + getLongueurFormatee())));
  }
  
  /**
   * Retourne si la variable est alphanumérique
   * @return
   */
  public boolean isAlpha() {
    // return (decimale==-1) && !(input_ctrl=='M');
    return (decimale == -1);// || (input_ctrl=='M');
  }
  
  /**
   * Retourne une clé Format + Record pour éviter les ambiguités d'appartenance
   * @return
   */
  public String getFormatRecord() {
    return nomFormat + parent;
  }
  
  /**
   * Initialise le nom du format qui contient son record (son parent)
   * @param nomFormat the nomFormat to set
   */
  public void setNomFormat(String nomFormat) {
    this.nomFormat = nomFormat;
  }
  
  /**
   * Retourne le nom du format qui contient son record (son parent)
   * @return the nomFormat
   */
  public String getNomFormat() {
    return nomFormat;
  }
  
  /**
   * Initialise la valeur de la variable depuis le databuffer
   * @param pValeur
   */
  public void setValeurFromBuffer(String pValeur) {
    valeur = pValeur;
  }
  
  /**
   * Initialise la valeur de la variable (à partir de la saisie dans le panel) en la reformatant
   * Attention:
   * On doit avoir une chaine de la meme longueur que défini dans setLongueur sinon pb lors de la création du buffer
   * Ssi valeur avec des blancs peut y avoir des soucis
   * ex: | 0203 | dc voir si on peut améliorer le truc en trimant ici au lieu de le faire dans le composant
   * @param pValeur
   */
  public void setValeurFromFrame(String pValeur) {
    if (isAlpha()) {
      if (isRB) {
        valeur = new DataFormat().getFixeBlancG(pValeur, getLongueur());
      }
      else {
        valeur = new DataFormat().getFixeBlancD(pValeur, getLongueur());
      }
    }
    else {
      valeur = new DataFormat(pValeur, output_edt, longueur, decimale).getRPGNumerique();
    }
  }
  
  /**
   * Initialise l'offset
   * @param val
   * @return
   */
  public int setOffset(String val) {
    try {
      offset = Integer.parseInt(val);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_OFFSET + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setOffset(int val) {
    offset = val;
    return Constantes.OK;
  }
  
  /**
   * Initialise la longueur de la variable
   * @param val
   * @return
   */
  public int setLongueur(String val) {
    try {
      longueur = Integer.parseInt(val);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_LONGUEUR + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setLongueur(int val) {
    longueur = val;
    return Constantes.OK;
  }
  
  /**
   * Initialise le nombre de décimale
   * @param val
   * @return
   */
  public int setDecimale(String val) {
    try {
      decimale = (byte) Integer.parseInt(val);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_DECIMALE + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setDecimale(int val) {
    decimale = (byte) val;
    return Constantes.OK;
  }
  
  /**
   * Initialise le contrôle de saisie
   * @param val
   * @return
   */
  public int setInputCtrl(String val) {
    input_ctrl = val.charAt(0);
    return Constantes.OK;
  }
  
  public int setInputCtrl(int val) {
    input_ctrl = (char) val;
    return Constantes.OK;
  }
  
  public int setInputCtrl(char val) {
    input_ctrl = val;
    return Constantes.OK;
  }
  
  /**
   * Retourne la liste des caractères autorisés (null sinon pas de limite) en saisie
   * @return
   */
  public char[] getCaractereAutorises() {
    if ((input_ctrl == 'Y') || (input_ctrl == 'M')) {
      return new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '.', ',', '+', '-' };
    }
    else if (input_ctrl == 'D') {
      return new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    }
    else if (input_ctrl == 'S') {
      return new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '+' };
    }
    else if (input_ctrl == 'X') {
      return new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
          'W', 'X', 'Y', 'Z', '.', ',', '-' };
    }
    
    return null;
  }
  
  /**
   * Initialise le formattage de l'édition
   * @param val
   * @return
   */
  public int setOutputEdt(String val) {
    output_edt = val.charAt(0);
    return Constantes.OK;
  }
  
  /**
   * Initialise le formattage de l'édition (à l'éxecution de SGM)
   * @param val
   * @return
   */
  public int setOutputEdt(int val) {
    output_edt = (char) val;
    return Constantes.OK;
  }
  
  /**
   * Initialise les controles d'édition
   * @param val
   * @return
   */
  public int setCheckEdt(String val) {
    check_edt = val.trim();
    if (!check_edt.trim().equals("")) {
      isME = check_edt.contains("ME");
      isER = check_edt.contains("ER");
      isMF = check_edt.contains("MF");
      isFE = check_edt.contains("FE");
      isRB = check_edt.contains("RB");
      isRZ = check_edt.contains("RZ");
      isRL = check_edt.contains("RL");
    }
    
    return Constantes.OK;
  }
  
  /**
   * Initialise le type de la zone (Entrée/Sortie)
   */
  public int setZoneType(String val) {
    val = val.trim();
    if (val.equals(SENS_S_ENTREE)) {
      zonetype = SENS_ENTREE;
    }
    else if (val.equals(SENS_S_SORTIE)) {
      zonetype = SENS_SORTIE;
    }
    else if (val.equals(SENS_S_CACHE)) {
      zonetype = SENS_ENTREE;
    }
    else {
      setMsgErreur(ERREUR_SENSVARIABLE_INCONNU + " " + val);
      return Constantes.ERREUR;
    }
    
    return Constantes.OK;
  }
  
  public int setZoneType(int val) {
    zonetype = (byte) val;
    return Constantes.OK;
  }
  
  /**
   * Initialise la ligne dans l'écran 5250
   * @param val
   * @return
   */
  public int setLigne(String val) {
    try {
      ligne = (byte) Integer.parseInt(val);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_LIGNE + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setLigne(int val) {
    ligne = (byte) val;
    return Constantes.OK;
  }
  
  /**
   * Initialise la colonne dans l'écran 5250
   * @param val
   * @return
   */
  public int setColonne(String val) {
    try {
      colonne = Integer.parseInt(val);
      return Constantes.OK;
    }
    catch (NumberFormatException e) {
      setMsgErreur(ERREUR_CONVERT_COLONNE + " " + e.toString());
      return Constantes.ERREUR;
    }
  }
  
  public int setColonne(int val) {
    colonne = val;
    return Constantes.OK;
  }
  
  /**
   * Initialise le nom de la variable
   * @param val
   * @return
   */
  public int setNom(String val) {
    val = val.trim();
    if (val.length() == 0) {
      setMsgErreur(ERREUR_NOMVARIABLE_INCORRECT + " " + val);
      return Constantes.ERREUR;
    }
    
    setName(val);
    return Constantes.OK;
  }
  
  /**
   * Initialise les indicateurs de la visibility
   * @param valeur
   */
  public void setVisibility(String valeur) {
    if (valeur == null) {
      return;
    }
    if (valeur.trim().equals("")) {
      return;
    }
    if (iindicateur == null) {
      iindicateur = new iIndicator();
    }
    visibility = iindicateur.tabconvert2notpol(valeur.trim());
  }
  
  public void setIndicateur(String valeur[]) {
    visibility = valeur;
  }
  
  /**
   * Initialise les indicateurs du ReadOnly
   * @param valeur
   */
  public void setReadOnly(String valeur) {
    // if (valeur == null) return;
    // if (valeur.trim().equals("")) return;
    if (iindicateur == null) {
      iindicateur = new iIndicator();
    }
    readonly = iindicateur.tabconvert2notpol(valeur.trim());
  }
  
  public void setReadOnly(String valeur[]) {
    readonly = valeur;
  }
  
  /**
   * Initialise les indicateurs du focus
   * @param valeur
   */
  public void setFocus(String valeur) {
    if (iindicateur == null) {
      iindicateur = new iIndicator();
    }
    focus = iindicateur.tabconvert2notpol(valeur.trim());
  }
  
  public void setFocus(String valeur[]) {
    focus = valeur;
  }
  
  /**
   * Initialise la casse
   */
  public void setLowerCase(boolean alowercase) {
    lowercase = alowercase;
  }
  
  /**
   * Initialise la translation table
   */
  public void setTraduction(boolean atraduction) {
    traduction = atraduction;
  }
  
  /**
   * Retourne le nom du Record
   * @return
   */
  public String getRecord() {
    return parent;
  }
  
  /**
   * Retourne la valeur pour initialiser le buffer
   * @return
   */
  public String getValeurToBuffer() {
    // Cas où aucune saisie dans le buffer et donc il se retrouve vide (ex: VEXPD2FM)
    if (valeur.length() == 0) {
      if (isAlpha()) {
        valeur = new DataFormat().getFixeBlancD("", getLongueur());
      }
      else {
        valeur = new DataFormat("0", output_edt, longueur, decimale).getRPGNumerique();
      }
    }
    return valeur;
  }
  
  /**
   * Retourne la valeur pour initialiser le panel
   * @return
   */
  public String getValeurToFrame() {
    String chaine = null;
    // translationtable);
    // Valeur Alphanumérique à vérifer dans le SDA mais pas de format d'édition
    if (isAlpha()) {
      if (traduction) {
        chaine = translationtable.get(valeur);
        if (chaine != null) {
          return chaine;
        }
      }
      // TODO améliorer en ajoutant le masque de sortie
      return valeur;
    }
    // Valeur Numérique
    else {
      // TODO améliorer en ajoutant le masque de sortie
      // decimale+"|"+input_ctrl);
      // return setMasqueSeparateursMilliers(new DataFormat(valeur, output_edt, longueur, decimale).getEdtNumerique(input_ctrl));
      return new DataFormat(valeur, output_edt, longueur, decimale).getEdtNumerique(input_ctrl);
    }
  }
  
  /**
   * Retourne la valeur trimée à droite pour initialiser le panel
   * @return
   */
  public String getValeurToFrameWithTrimR() {
    String chaine = getValeurToFrame();
    if (chaine == null) {
      return chaine;
    }
    
    // On supprime les espaces de droites
    for (int i = chaine.length() - 1; i >= 0; i--) {
      if (chaine.charAt(i) != ' ') {
        // return setMasqueSeparateursMilliers(chaine.substring(0, i + 1));
        return chaine.substring(0, i + 1);
      }
    }
    // return setMasqueSeparateursMilliers(chaine.trim());
    return chaine.trim();
  }
  
  /*
   * insère un caractère de séparation des milliers dans les datas numériques (pas utilisé pour l'instant ! danger)
   */
  public String setMasqueSeparateursMilliers(String pChaine) {
    StringBuffer stbuf = new StringBuffer();
    stbuf.append(pChaine.trim());
    
    if (!isAlpha()) {
      int position = stbuf.length() - 3;
      int nbrdecimales = getDecimale();
      if (nbrdecimales > 0) {
        nbrdecimales++;
        position -= nbrdecimales;
      }
      int nbrIterations = ((stbuf.length() - nbrdecimales) / 3);
      for (int i = 0; i < nbrIterations; i++) {
        stbuf.insert(position, SEPARATEUR_MILLIERS);
        position -= 3;
      }
    }
    
    return stbuf.toString();
  }
  
  /**
   * Retourne l'offset
   * @return
   */
  public int getOffset() {
    return offset;
  }
  
  /**
   * Retourne la longueur de la variable
   * @return
   */
  public int getLongueur() {
    return longueur;
  }
  
  /**
   * Retourne la longueur de la variable formatée
   * @return
   */
  public int getLongueurFormatee() {
    if (longueur_formatee == -1) {
      longueur_formatee = longueur;
      if ((decimale != -1) && (decimale != 0)) {
        longueur_formatee++;
      }
      switch (output_edt) {
        case OEDT_A:
          longueur_formatee += 3;
          break; // . CR
        case OEDT_B:
          longueur_formatee += 3;
          break; // . CR
        case OEDT_C:
          longueur_formatee += 2;
          break; // CR
        case OEDT_D:
          longueur_formatee += 2;
          break; // CR
        case OEDT_J:
          longueur_formatee += 2;
          break; // . -
        case OEDT_K:
          longueur_formatee += 2;
          break; // . -
        case OEDT_L:
          longueur_formatee++;
          break; // -
        case OEDT_M:
          longueur_formatee++;
          break; // -
        case OEDT_N:
          longueur_formatee += 2;
          break; // . -
        case OEDT_O:
          longueur_formatee += 2;
          break; // . -
        case OEDT_P:
          longueur_formatee++;
          break; // -
        case OEDT_Q:
          longueur_formatee++;
          break; // -
        case OEDT_W:
          longueur_formatee += 2;
          break; // / /
        case OEDT_Y:
          longueur_formatee += 2;
          break; // / /
          
        case OEDT_1:
          longueur_formatee++;
          break; // .
        case OEDT_2:
          longueur_formatee++;
          break; // .
      }
    }
    return longueur_formatee;
  }
  
  /**
   * Retourne le nombre de décimale
   * @return
   */
  public byte getDecimale() {
    return decimale;
  }
  
  /**
   * Retourne le code du contrôle de saisie
   * @return
   */
  public byte getInputCtrl() {
    return (byte) input_ctrl;
  }
  
  /**
   * Retourne le code du formatage d'édition
   * @return
   */
  public byte getOutputEdt() {
    return (byte) output_edt;
  }
  
  /**
   * Retourne les controles d'édition
   * @return
   */
  public String getCheckEdt() {
    return check_edt;
  }
  
  /**
   * Retourne le sens de la variable (Entrée/Sortie)
   * @return
   */
  public byte getZoneType() {
    return zonetype;
  }
  
  /**
   * Retourne la ligne dans l'écran 5250
   * @return
   */
  public byte getLigne() {
    return ligne;
  }
  
  /**
   * Retourne la colonne dans l'écran 5250
   * @return
   */
  public int getColonne() {
    return colonne;
  }
  
  /**
   * Retourne le nom de la variable
   * @return
   */
  public String getNom() {
    return getName();
  }
  
  /**
   * Retourne les indicateurs de la Visibility
   * @return
   */
  public String[] getVisibility() {
    return visibility;
  }
  
  /**
   * Initialise les indicateurs de la visibility sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public void setVisibilityPol(String valeur) {
    visibility = valeur.split(" ");
  }
  
  /**
   * Retourne les indicateurs de la visibility sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public String getVisibilityPol() {
    if (visibility == null) {
      return null;
    }
    
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < visibility.length; i++) {
      sb.append(visibility[i]).append(' ');
    }
    return sb.toString().trim();
  }
  
  /**
   * Evalue les indicateurs de la visibility avec l'interpréteur du record courant
   * Par défaut on envoie ON (la variable est visible sauf si c'est explicite)
   * @return
   */
  public int getEvalVisibility() {
    if (visibility == null) {
      return Constantes.ON;
    }
    else {
      return iindicateur.evaluation(visibility);
    }
  }
  
  /**
   * Retourne les indicateurs du ReadOnly
   * @return
   */
  public String[] getReadOnly() {
    return readonly;
  }
  
  /**
   * Initialise les indicateurs du readonly sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public void setReadOnlyPol(String valeur) {
    readonly = valeur.split(" ");
  }
  
  /**
   * Retourne les indicateurs du readonly sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public String getReadOnlyPol() {
    if (readonly == null) {
      return null;
    }
    
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < readonly.length; i++) {
      sb.append(readonly[i]).append(' ');
    }
    return sb.toString().trim();
  }
  
  /**
   * Evalue les indicateurs du readonly avec l'interpréteur du record courant.
   * On regarde le type de la zone puis les indicateurs
   * @return
   */
  public int getEvalReadOnly() {
    if (readonly == null) {
      if (zonetype == SENS_ENTREE) {
        return Constantes.OFF;
      }
      else {
        return Constantes.ON;
      }
    }
    else if (readonly[0].equals("")) {
      return Constantes.ON;
    }
    else {
      return iindicateur.evaluation(readonly);
    }
  }
  
  /**
   * Retourne les indicateurs du Focus
   * @return
   */
  public String[] getFocus() {
    return focus;
  }
  
  /**
   * Initialise les indicateurs du focus sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public void setFocusPol(String valeur) {
    focus = valeur.split(" ");
  }
  
  /**
   * Retourne les indicateurs du focus sous forme d'une chaine en notation polonaise inverse
   * @return
   */
  public String getFocusPol() {
    if (focus == null) {
      return null;
    }
    
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < focus.length; i++) {
      sb.append(focus[i]).append(' ');
    }
    return sb.toString().trim();
  }
  
  /**
   * Evalue les indicateurs du focus avec l'interpréteur du record courant.
   * On regarde le type de la zone puis les indicateurs
   * @return
   */
  public int getEvalFocus() {
    if (focus == null) {
      return Constantes.OFF;
    }
    else if (focus[0].equals("")) {
      return Constantes.ON;
    }
    else {
      return iindicateur.evaluation(focus);
    }
  }
  
  /**
   * Retourne la casse
   * @return
   */
  public boolean isLowerCase() {
    return lowercase;
  }
  
  /**
   * Retourne si translation table
   * @return
   */
  public boolean isTraduction() {
    return traduction;
  }
  
  /**
   * Initialise les attributs et leurs types d'un objet
   */
  @Override
  public void setAttributes(HashMap<String, Object> alisteAttributes) {
    super.setAttributes(alisteAttributes);
    
    // OFFSET
    if (listeAttributes.containsKey("OFFSET")) {
      setOffset((Integer) listeAttributes.get("OFFSET"));
    }
    // LENGTH
    if (listeAttributes.containsKey("LENGTH")) {
      setLongueur((Integer) listeAttributes.get("LENGTH"));
    }
    // DECIMAL
    if (listeAttributes.containsKey("DECIMAL")) {
      setDecimale((Integer) listeAttributes.get("DECIMAL"));
    }
    // INPUT CTRL
    if (listeAttributes.containsKey("INPUT_CTRL")) {
      setInputCtrl((Byte) listeAttributes.get("INPUT_CTRL"));
    }
    // ZONE TYPE
    if (listeAttributes.containsKey("ZONETYPE")) {
      setZoneType((Byte) listeAttributes.get("ZONETYPE"));
    }
    // LINE
    if (listeAttributes.containsKey("LINE")) {
      setLigne((Byte) listeAttributes.get("LINE"));
    }
    // COLUMN
    if (listeAttributes.containsKey("COLUMN")) {
      setColonne((Integer) listeAttributes.get("COLUMN"));
    }
    // VISIBILITY - INDICATOR
    if (listeAttributes.containsKey("INDICATOR")) {
      setVisibilityPol((String) listeAttributes.get("VISIBILITY"));
    }
    // READONLY
    if (listeAttributes.containsKey("READONLY")) {
      setReadOnlyPol((String) listeAttributes.get("READONLY"));
    }
    // FOCUS
    if (listeAttributes.containsKey("FOCUS")) {
      setFocusPol((String) listeAttributes.get("FOCUS"));
    }
    // LOWERCASE
    if (listeAttributes.containsKey("LOWERCASE")) {
      setLowerCase((Boolean) listeAttributes.get("LOWERCASE"));
    }
    // TRANSLATION TABLE
    if (listeAttributes.containsKey("TRANSLATE")) {
      setTraduction((Boolean) listeAttributes.get("TRANSLATE"));
    }
    // OUTPUT EDT
    if (listeAttributes.containsKey("OUTPUT_EDT")) {
      setOutputEdt((Byte) listeAttributes.get("OUTPUT_EDT"));
    }
    // CHECK EDT
    if (listeAttributes.containsKey("CHECKEDIT")) {
      setCheckEdt((String) listeAttributes.get("CHECKEDIT"));
    }
  }
  
  /**
   * Retourne les attributs et leurs types d'un objet
   */
  @Override
  public HashMap<String, Object> getAttributes() {
    super.getAttributes();
    
    listeAttributes.put("OFFSET", Integer.valueOf(getOffset()));
    listeAttributes.put("LENGTH", Integer.valueOf(getLongueur()));
    if (getDecimale() != -1) {
      listeAttributes.put("DECIMAL", Integer.valueOf(getDecimale()));
    }
    listeAttributes.put("INPUT_CTRL", input_ctrl);
    switch (getZoneType()) {
      case SENS_ENTREE:
        listeAttributes.put("ZONETYPE", SENS_S_ENTREE);
        break;
      case SENS_SORTIE:
        listeAttributes.put("ZONETYPE", SENS_S_SORTIE);
        break;
      case SENS_CACHE:
        listeAttributes.put("ZONETYPE", SENS_S_CACHE);
        break;
      default:
    }
    if (getLigne() > 0) {
      listeAttributes.put("LINE", Byte.valueOf(getLigne()));
    }
    if (getColonne() > 0) {
      listeAttributes.put("COLUMN", Integer.valueOf(getColonne()));
    }
    if (getVisibilityPol() != null) {
      listeAttributes.put("VISIBILITY", new String(getVisibilityPol()));
    }
    if (getReadOnlyPol() != null) {
      listeAttributes.put("READONLY", new String(getReadOnlyPol()));
    }
    if (getFocusPol() != null) {
      listeAttributes.put("FOCUS", new String(getFocusPol()));
    }
    if (isLowerCase()) {
      listeAttributes.put("LOWERCASE", Boolean.valueOf(isLowerCase()));
    }
    listeAttributes.put("TRANSLATE", Boolean.valueOf(isTraduction()));
    if (output_edt != ' ') {
      listeAttributes.put("OUTPUT_EDT", Byte.valueOf(getOutputEdt()));
    }
    if (!getCheckEdt().equals("")) {
      listeAttributes.put("CHECKEDIT", new String(getCheckEdt()));
    }
    
    return listeAttributes;
  }
  
  /**
   * Retourne les infos pour générer un fichier PNL
   * @return
   */
  public ArrayList<String> getPNL() {
    ArrayList<String> liste = new ArrayList<String>();
    liste.add("OBJECT_DATA");
    liste.add("\tTYPEOBJ=composants.oData");
    liste.add("\tPARENT=" + getParent());
    liste.add("\tNAME=" + getNom());
    liste.add("\tOFFSET=" + offset);
    liste.add("\tLENGTH=" + getLongueur());
    if (getDecimale() != -1) {
      liste.add("\tDECIMAL=" + getDecimale());
    }
    liste.add("\tINPUT_CTRL=" + input_ctrl);
    if (output_edt != ' ') {
      liste.add("\tOUTPUT_CTRL=" + output_edt);
    }
    switch (zonetype) {
      case SENS_ENTREE:
        liste.add("\tZONETYPE=" + SENS_S_ENTREE);
        break;
      case SENS_SORTIE:
        liste.add("\tZONETYPE=" + SENS_S_SORTIE);
        break;
      case SENS_CACHE:
        liste.add("\tZONETYPE=" + SENS_S_CACHE);
        break;
      default:
    }
    if (isLowerCase()) {
      liste.add("\tLOWERCASE=TRUE");
    }
    if (!getCheckEdt().equals("")) {
      liste.add("\tCHECKEDIT=" + getCheckEdt());
    }
    if (getReadOnlyPol() != null) {
      liste.add("\tREADONLY=" + getReadOnlyPol());
    }
    if (getLigne() > 0) {
      liste.add("\tLINE=" + getLigne());
    }
    if (getColonne() > 0) {
      liste.add("\tCOLUMN=" + getColonne());
    }
    if (getVisibilityPol() != null) {
      liste.add("\tVISIBILITY=" + getVisibilityPol());
    }
    if (getFocusPol() != null) {
      liste.add("\tFOCUS=" + getFocusPol());
    }
    
    liste.add("END;");
    
    return liste;
  }
  
  /**
   * Initialise les valeurs avec un tableau venant du PNL
   * @param valeur
   */
  @Override
  public void pnlToValeurs(LinkedHashMap<String, String> valeur) {
    super.pnlToValeurs(valeur);
    
    // OFFSET
    if (valeur.containsKey("OFFSET")) {
      setOffset(valeur.get("OFFSET"));
    }
    // LENGTH
    if (valeur.containsKey("LENGTH")) {
      setLongueur(valeur.get("LENGTH"));
    }
    // DECIMAL
    if (valeur.containsKey("DECIMAL")) {
      setDecimale(valeur.get("DECIMAL"));
    }
    // INPUT CTRL
    if (valeur.containsKey("INPUT_CTRL")) {
      setInputCtrl(valeur.get("INPUT_CTRL"));
    }
    // ZONE TYPE
    if (valeur.containsKey("ZONETYPE")) {
      setZoneType(valeur.get("ZONETYPE"));
    }
    // LINE
    if (valeur.containsKey("LINE")) {
      setLigne(valeur.get("LINE"));
    }
    // COLUMN
    if (valeur.containsKey("COLUMN")) {
      setColonne(valeur.get("COLUMN"));
    }
    // VISIBILITY
    if (valeur.containsKey("VISIBILITY")) {
      setVisibility(valeur.get("VISIBILITY"));
    }
    // READONLY
    if (valeur.containsKey("READONLY")) {
      setReadOnly(valeur.get("READONLY"));
    }
    // FOCUS
    if (valeur.containsKey("FOCUS")) {
      setFocus(valeur.get("FOCUS"));
    }
    // LOWERCASE
    if (valeur.containsKey("LOWERCASE")) {
      setLowerCase(valeur.get("LOWERCASE").trim().equals("TRUE"));
    }
    // TRANSLATION TABLE
    if (valeur.containsKey("TRANSLATE")) {
      setTraduction(valeur.get("TRANSLATE").trim().equals("TRUE"));
    }
    // OUTPUT EDT
    if (valeur.containsKey("OUTPUT_EDT")) {
      setOutputEdt(valeur.get("OUTPUT_EDT"));
    }
    // CHECK EDT
    if (valeur.containsKey("CHECKEDIT")) {
      setCheckEdt(valeur.get("CHECKEDIT"));
    }
  }
  
  /**
   * Convertie l'objet au format binaire
   * @param tabListFic
   * @throws UnsupportedEncodingException
   */
  @Override
  public ArrayList<Integer> valeursToBinaire(ArrayList<String> tabListFic) throws UnsupportedEncodingException {
    int i = 0;
    
    super.valeursToBinaire(tabListFic);
    
    // OFFSET
    addTabCompilation(ID_OFFSET, tabCompilation, offset);
    // LONGUEUR
    addTabCompilation(ID_LONGUEUR, tabCompilation, longueur);
    // DECIMALE
    addTabCompilation(ID_DECIMALE, tabCompilation, decimale);
    // INPUT CTRL
    addTabCompilation(ID_INPUT_CTRL, tabCompilation, input_ctrl);
    // ZONE TYPE
    addTabCompilation(ID_ZONETYPE, tabCompilation, zonetype);
    // LIGNE
    if (ligne > 0) {
      addTabCompilation(ID_LIGNE, tabCompilation, ligne);
    }
    // COLONNE
    if (colonne > 0) {
      addTabCompilation(ID_COLONNE, tabCompilation, colonne);
    }
    // VALEUR
    if (valeur != null) {
      addTabCompilation(ID_VALEUR, tabCompilation, valeur);
    }
    // VISIBILITY
    if (visibility != null) {
      addTabCompilation(ID_VISIBILITY, tabCompilation, visibility.length);
      for (i = 0; i < visibility.length; i++) {
        addTabCompilation(Constantes.NONE, tabCompilation, visibility[i]);
      }
    }
    // READONLY
    if (readonly != null) {
      addTabCompilation(ID_READONLY, tabCompilation, readonly.length);
      for (i = 0; i < readonly.length; i++) {
        addTabCompilation(Constantes.NONE, tabCompilation, readonly[i]);
      }
    }
    // FOCUS
    if (focus != null) {
      addTabCompilation(ID_FOCUS, tabCompilation, focus.length);
      for (i = 0; i < focus.length; i++) {
        addTabCompilation(Constantes.NONE, tabCompilation, focus[i]);
      }
    }
    // LOWERCASE
    addTabCompilation(ID_LOWERCASE, tabCompilation, lowercase);
    // TRANSLATION TABLE
    addTabCompilation(ID_TRANSLATE, tabCompilation, traduction);
    // OUTPUT EDT
    addTabCompilation(ID_OUTPUT_EDT, tabCompilation, output_edt);
    // CHECK EDT
    if (!check_edt.trim().equals("")) {
      addTabCompilation(ID_CHECK_EDT, tabCompilation, check_edt);
    }
    insertTailleObjet();
    return tabCompilation;
  }
  
  /**
   * Initialise les variables de l'objet oData à partir d'un tableau binaire
   * Retourne si erreur
   * @param tabInt
   * @param ListFic
   * @return
   */
  @Override
  public int binaireToValeurs(int tabInt[], String ListFic[]) {
    return binaireToValeurs(tabInt, 0, null);
  }
  
  @Override
  public int binaireToValeurs(int tabInt[], int offst, String ListFic[]) {
    int i = 0;
    int j = 0;
    int k = 0;
    int l = 0;
    int m = 0;
    int lg = 0;
    byte tabByte[] = new byte[Constantes.DATAQLONG];
    String tabchaine[] = null;
    int lngdescdata = 0;
    
    // Vérification du tableau
    if (tabInt == null) {
      setMsgErreur(ERREUR_DATA_NULL);
      return Constantes.ERREUR;
    }
    
    i = offst;
    lngdescdata = tabInt[i++];
    if (lngdescdata > tabInt.length) {
      setMsgErreur(ERREUR_LONGUEUR_DATA_ERRONNE);
      return Constantes.ERREUR;
    }
    
    lngdescdata = lngdescdata + i;
    while (i < lngdescdata) {
      switch (tabInt[i]) {
        case ID_OFFSET:
          i += 2;
          setOffset(tabInt[i]);
          i++;
          break;
        case ID_LONGUEUR:
          i += 2;
          setLongueur(tabInt[i]);
          i++;
          break;
        case ID_DECIMALE:
          i += 2;
          setDecimale(tabInt[i]);
          i++;
          break;
        case ID_INPUT_CTRL:
          i += 2;
          setInputCtrl(tabInt[i]);
          i++;
          break;
        case ID_ZONETYPE:
          i += 2;
          setZoneType(tabInt[i]);
          i++;
          break;
        case ID_LIGNE:
          i += 2;
          setLigne(tabInt[i]);
          i++;
          break;
        case ID_COLONNE:
          i += 2;
          setColonne(tabInt[i]);
          i++;
          break;
        case ID_VALEUR:
          // longueur de la valeur
          lg = tabInt[++i];
          // tabByte = new byte[k];
          // récupération de la valeur
          i++;
          k = lg + i;
          for (j = 0; i < k; i++) {
            tabByte[j++] = (byte) tabInt[i];
          }
          // récupération du nom
          setValeurFromFrame(new String(tabByte, 0, lg));
          break;
        case ID_VISIBILITY:
          i += 2;
          m = tabInt[i];
          tabchaine = new String[m];
          for (j = 0; j < m; j++) {
            // longueur de l'indicateur
            lg = tabInt[++i];
            // tabByte = new byte[k];
            // récupération du nom
            i++;
            k = lg + i;
            for (l = 0; i < k; i++) {
              tabByte[l++] = (byte) tabInt[i];
            }
            i--;
            tabchaine[j] = new String(tabByte, 0, lg);
          }
          // récupération du nom
          setIndicateur(tabchaine);
          i++;
          break;
        case ID_READONLY:
          i += 2;
          m = tabInt[i];
          tabchaine = new String[m];
          for (j = 0; j < m; j++) {
            // longueur de l'indicateur
            lg = tabInt[++i];
            // tabByte = new byte[k];
            // récupération du nom
            i++;
            k = lg + i;
            for (l = 0; i < k; i++) {
              tabByte[l++] = (byte) tabInt[i];
            }
            i--;
            tabchaine[j] = new String(tabByte, 0, lg);
          }
          // récupération du nom
          setReadOnly(tabchaine);
          i++;
          break;
        case ID_LOWERCASE:
          i += 2;
          setLowerCase(tabInt[i] == Constantes.TRUE ? true : false);
          i++;
          break;
        case ID_TRANSLATE:
          i += 2;
          setTraduction(tabInt[i] == Constantes.TRUE ? true : false);
          // tabInt[i]);
          i++;
          break;
        case ID_OUTPUT_EDT:
          i += 2;
          setOutputEdt(tabInt[i]);
          i++;
          break;
        case ID_CHECK_EDT:
          // longueur de la valeur
          lg = tabInt[++i];
          // tabByte = new byte[k];
          // récupération de la valeur
          i++;
          k = lg + i;
          for (j = 0; i < k; i++) {
            tabByte[j++] = (byte) tabInt[i];
          }
          // récupération du nom
          setCheckEdt(new String(tabByte, 0, lg));
          break;
        case ID_FOCUS:
          i += 2;
          m = tabInt[i];
          tabchaine = new String[m];
          for (j = 0; j < m; j++) {
            // longueur de l'indicateur
            lg = tabInt[++i];
            // tabByte = new byte[k];
            // récupération du nom
            i++;
            k = lg + i;
            for (l = 0; i < k; i++) {
              tabByte[l++] = (byte) tabInt[i];
            }
            i--;
            tabchaine[j] = new String(tabByte, 0, lg);
          }
          // récupération du nom
          setFocus(tabchaine);
          i++;
          break;
        
        // Valeur non traitée <-----------------------------------------------------
        default:
          i = super.binaireToValeurs(tabInt, i, null);
      }
    }
    tabByte = null;
    tabInt = null;
    
    return i;
  }
  
}
