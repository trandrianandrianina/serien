/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.util.ArrayList;

import javax.swing.JFileChooser;

/**
 * Gestion des fichiers SQL.
 */
public class GestionFichierSQL extends GestionFichierTexte {
  // Constantes
  private static final String FILTER_EXT_SQL = "sql";
  private static final String FILTER_DESC_SQL = "Fichier SQL (*.sql)";
  private static final String[] KEYWORDS = { "INSERT", "UPDATE", "DELETE", "SELECT", "CREATE", "ALTER", "DROP" };
  
  // Variables
  private ArrayList<String> listQueries = new ArrayList<String>();
  
  /**
   * Constructeur de la classe.
   */
  public GestionFichierSQL(String fch) {
    super(fch);
  }
  
  /**
   * Retourne les filtres possible pour les boites de dialogue.
   */
  public static void initFiltre(JFileChooser pJFileChooser) {
    GestionFichierTexte.initFiltre(pJFileChooser);
    pJFileChooser.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { FILTER_EXT_SQL }, FILTER_DESC_SQL));
  }
  
  // -- Méthodes publiques -------------------------------------------------
  
  /**
   * Analyse et traite le fichier texte afion d'en extraire les requêtes.
   */
  public boolean treatmentFile() {
    // Chargement du fichier
    ArrayList<String> lines = getContenuFichier();
    if (lines == null) {
      return false;
    }
    
    // On élimine les lignes qui ne servent à rien
    removeBlanksLine(lines);
    removeComments(lines);
    removeSpecificString(";;;");
    
    // for( String line : lines )
    
    // On constitue une liste avec chaque requête entière
    extractRequest(lines);
    cleanRequest();
    
    return true;
  }
  
  /**
   * Supprime les chaines caractères voulues.
   */
  public void removeSpecificString(String toremove) {
    if (toremove == null) {
      return;
    }
    
    ArrayList<String> liste = getContenuFichier();
    for (int i = 0; i < liste.size(); i++) {
      liste.set(i, liste.get(i).replaceAll(toremove, ""));
    }
  }
  
  @Override
  public void dispose() {
    super.dispose();
    if (listQueries != null) {
      listQueries.clear();
    }
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Supprime les lignes vides du source.
   */
  private void removeBlanksLine(ArrayList<String> lines) {
    int i = 0;
    // int max = lines.size();
    
    while (i < lines.size()) {
      if (lines.get(i).trim().equals("")) {
        lines.remove(i);
      }
      else {
        i++;
      }
    }
  }
  
  /**
   * Supprime les commentaires du source.
   */
  private void removeComments(ArrayList<String> lines) {
    int i = 0;
    // int max = lines.size();
    boolean remove = false;
    boolean lastremove = true;
    
    while (i < lines.size()) {
      // On détecte les marqueurs de commentaires
      if (lines.get(i).trim().startsWith("//")) {
        remove = true;
      }
      else {
        if (lines.get(i).trim().startsWith("/*")) {
          remove = true;
          lastremove = false;
        }
        if (lines.get(i).trim().endsWith("*/")) {
          lastremove = true;
        }
      }
      
      // On supprime effectivement la ligne
      if (remove) {
        lines.remove(i);
        if (lastremove) {
          remove = false;
        }
      }
      else {
        i++;
      }
    }
  }
  
  /**
   * Constitue une liste contenant les requêtes entières.
   */
  public void extractRequest(ArrayList<String> lines) {
    boolean currentrequest = false;
    boolean nextrequest = false;
    StringBuilder sb = new StringBuilder(1024);
    
    for (String line : lines) {
      if (testKeywords(line)) {
        if (!currentrequest) {
          currentrequest = true;
        }
        else {
          nextrequest = true;
          currentrequest = false;
        }
      }
      else {
        nextrequest = false;
        currentrequest = true;
      }
      
      if (nextrequest) {
        listQueries.add(sb.toString());
        sb.setLength(0);
        sb.append(line);
      }
      else if (currentrequest) {
        sb.append(line);
      }
      
    }
    listQueries.add(sb.toString());
  }
  
  /**
   * Teste si une chaine commence par un mot clef SQL.
   */
  public boolean testKeywords(String line) {
    line = line.trim().toUpperCase();
    for (String word : KEYWORDS) {
      if (line.startsWith(word)) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Nettoyage final des requêtes afin qu'elles soient utilisables.
   */
  private void cleanRequest() {
    String request;
    int max = listQueries.size();
    
    if (max == 0) {
      return;
    }
    
    for (int i = 0; i < max; i++) {
      request = listQueries.get(i).trim();
      // On suppprime le point-virgule à la fin de la requête
      if (request.endsWith(";")) {
        request = request.substring(0, request.length() - 1);
      }
      listQueries.set(i, request);
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  public ArrayList<String> getListQueries() {
    return listQueries;
  }
  
  public void setListQueries(ArrayList<String> listQueries) {
    this.listQueries = listQueries;
  }
}
