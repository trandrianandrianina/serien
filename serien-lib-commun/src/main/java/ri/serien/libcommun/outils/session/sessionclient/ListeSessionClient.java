/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session.sessionclient;

import java.util.ArrayList;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Liste de session client.
 * L'utilisation de cette classe est à privilégier dès qu'on manipule une liste de session client.
 */
public class ListeSessionClient extends ArrayList<SessionClient> {
  
  /**
   * Constructeur.
   */
  public ListeSessionClient() {
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner un SessionClient de la liste à partir de son identifiant.
   */
  public SessionClient retournerSessionClientParId(IdSession pIdSession) {
    if (pIdSession == null) {
      return null;
    }
    for (SessionClient sessionClient : this) {
      if (sessionClient != null && Constantes.equals(sessionClient.getIdSession(), pIdSession)) {
        return sessionClient;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un SessionBase est présent dans la liste.
   */
  public boolean isPresent(IdSession pIdSession) {
    return retournerSessionClientParId(pIdSession) != null;
  }
  
}
