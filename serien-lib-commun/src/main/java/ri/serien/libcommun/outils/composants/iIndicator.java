/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.composants;

import java.util.Arrays;

import ri.serien.libcommun.outils.Constantes;

/**
 * Interprétation des indicateurs
 */
public class iIndicator {
  // Constantes erreurs de chargement
  private final static String ERREUR_INDICATEUR_NULL = "[iIndicator] La liste des indicateurs est vide.";
  private final static String ERREUR_EXPRESSION_NULL = "[iIndicator] L'expression a évaluer est vide.";
  private final static String ERREUR_EVALUATION_CONVERT_EXPRESSION =
      "[iIndicator] L'expression a convertir n'est pas une valeur numérique.";
  private final static String ERREUR_EVALUATION_EXPRESSION_PILE = "[iIndicator] Erreur lors de l'évaluation, erreur de pile.";
  
  // Constantes
  private final static int LONGUEUR_PILE = 10;
  
  // Variables
  private StringBuffer sb = new StringBuffer(); // Variable globale afin d'optimiser un peu
  private char indicateur[] = null;
  private String msgErreur = null;
  
  /**
   * Constructeur de la classe
   */
  public iIndicator() {
  }
  
  /**
   * Constructeur de la classe
   * @param tabindic
   */
  public iIndicator(char[] tabindic) {
    // On recopie le tableau pour ne pas avoir un passage par adresse
    indicateur = Arrays.copyOf(tabindic, tabindic.length);
  }
  
  /**
   * Initialise la tables des indicateurs
   * @param tabindic
   */
  public void setIndicateur(char[] tabindic) {
    indicateur = tabindic;
  }
  
  /**
   * Convertit les indicateurs du source en notation polonaise
   * ATTENTION les indicateurs doivent être encadrés par des parenthèses ex: (70) AND (69) AND (N42)
   * @param indicsrc
   * @return
   */
  public String convert2notpol(String indicsrc) {
    if (indicsrc == null) {
      return null;
    }
    
    int i = 0;
    int nbrparenthese = 0;
    char[] operateur = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' };
    boolean testoperateur = false;
    
    indicsrc = indicsrc.toUpperCase();
    sb.setLength(0);
    i = 0;
    while (i < indicsrc.length()) {
      if (indicsrc.charAt(i) == ' ') {
        if (testoperateur) {
          testoperateur = false;
          if (operateur[nbrparenthese] != ' ') {
            sb.append(operateur[nbrparenthese]).append(' ');
            operateur[nbrparenthese] = ' ';
          }
        }
      }
      else if (indicsrc.charAt(i) == '(') {
        nbrparenthese++;
      }
      else if (indicsrc.charAt(i) == ')') {
        if (operateur[nbrparenthese] != ' ') {
          sb.append(operateur[nbrparenthese]).append(' ');
          operateur[nbrparenthese] = ' ';
        }
        nbrparenthese--;
      }
      // Indicateur baissé
      else if (indicsrc.charAt(i) == 'N') {
        sb.append('-');
      }
      // Opérateur AND
      else if (indicsrc.charAt(i) == 'A') {
        operateur[nbrparenthese] = '&';
        i += 3;
      }
      // On teste la fin d'une variable
      else if (indicsrc.charAt(i) == 'O') {
        operateur[nbrparenthese] = '|';
        i += 2;
      }
      // L'indicateur
      else {
        testoperateur = true;
        sb.append(indicsrc.charAt(i++)).append(indicsrc.charAt(i)).append(' ');
      }
      i++;
    }
    if (operateur[0] != ' ') {
      sb.append(operateur[nbrparenthese]);
    }
    
    return sb.toString().trim();
  }
  
  /**
   * Convertit les indicateurs du source en notation polonaise
   */
  public String[] tabconvert2notpol(String indicsrc) {
    if (indicsrc == null) {
      return null;
    }
    else {
      return Constantes.splitString(convert2notpol(indicsrc), ' ');
    }
  }
  
  /**
   * Evalue les indicateurs
   * Attention return ON ou OFF pas confondre avec TRUE ou FALSE (par défaut ON)
   * @param indicbin
   * @return
   */
  public int evaluation(String[] indicbin) {
    int i = 0;
    int ip = 0;
    int ind = 0;
    int aind = 0;
    int pile[] = null;
    
    // Vérification avant de commencer l'évaluation
    if (indicateur == null) {
      msgErreur = ERREUR_INDICATEUR_NULL;
      return Constantes.ON;
    }
    if (indicbin == null) {
      msgErreur = ERREUR_EXPRESSION_NULL;
      return Constantes.ON;
    }
    
    // Evaluation
    pile = new int[LONGUEUR_PILE];
    ip = -1;
    for (i = 0; i < indicbin.length; i++) {
      if (indicbin[i].equals("&")) {
        ip--;
        pile[ip] = pile[ip] & pile[ip + 1];
      }
      else if (indicbin[i].equals("|")) {
        ip--;
        pile[ip] = pile[ip] | pile[ip + 1];
      }
      else {
        if (!indicbin[i].trim().equals("")) {
          try {
            
            ind = Integer.parseInt(indicbin[i].trim());
            aind = Math.abs(ind) - 1;
            if (((ind > 0) && (indicateur[aind] == '1')) || ((ind < 0) && (indicateur[aind] == '0'))) {
              pile[++ip] = Constantes.ON;
            }
            else {
              pile[++ip] = Constantes.OFF;
              /*
                        if (ind > 0)
                            if (indicateur[aind] == '1') pile[++ip] = ON;
                            else pile[++ip] = OFF;
                        else
                            if (indicateur[aind] == '0') pile[++ip] = ON;
                            else pile[++ip] = OFF;
              */
            }
          }
          catch (NumberFormatException e) {
            e.printStackTrace();
            msgErreur = ERREUR_EVALUATION_CONVERT_EXPRESSION + e.toString();
            return Constantes.ERREUR;
          }
        }
      }
    }
    if (ip == 0) {
      return pile[ip];
    }
    else {
      msgErreur = ERREUR_EVALUATION_EXPRESSION_PILE;
      return Constantes.ERREUR;
    }
  }
  
  /**
   * Retourne une liste généré depuis une chaine
   * @param chaine
   * @param separateur
   * @return
   *         REMPLACER par celui de Constantes
   *         private String[] splitString(String chaine, char separateur)
   *         {
   *         int position=0, i=0;
   *         String liste[];
   *         ArrayList<String> vlisting = new ArrayList<String>(50);
   * 
   *         position=chaine.indexOf(separateur, i);
   *         while (position != -1)
   *         {
   *         if (i == position)
   *         vlisting.add("");
   *         else
   *         vlisting.add(chaine.substring(i, position));
   *         i = position+1;
   *         position=chaine.indexOf(separateur, i);
   *         }
   *         vlisting.add(chaine.substring(i));
   * 
   *         // On recopie les données du Vector vers le tableau liste
   *         liste = new String[vlisting.size()];
   *         for (i=0; i<vlisting.size(); i++)
   *         {
   *         liste[i] = (String)vlisting.get(i);
   *         }
   * 
   *         return liste;
   *         }
   */
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
}
