/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import ri.serien.libcommun.outils.Constantes;

/**
 * Gestion des fichiers de description CSV 27/10/2006 - 04/03/2014
 * Gère les fichiers de type CSV
 * A faire:
 * Améliorer la sortie HTML avec gestion des caractères accentués (UTF-8)
 * Variabiliser les couleurs de l'entête des colonnes
 * Rajouter le type TXT (champs séparés par des tabulations)
 * 
 * ATTENTION LES CLES SONT SENSIBLES A LA CASSE
 */
public class GestionFichierCSV extends GestionFichierTexte {
  // Constantes
  public final static String FILTER_EXT_CSV = "csv";
  public final static String FILTER_DESC_CSV = "CSV (séparateur: point-virgule) (*.csv)";
  public final static String FILTER_EXT_XLS = "xls";
  public final static String FILTER_DESC_XLS = "Fichier Microsoft Excel (*.xls)";
  public final static String FILTER_EXT_HTM = "htm";
  public final static String FILTER_DESC_HTM = "Page web (*.htm)";
  
  /**
   * Constructeur de la classe
   * @param fchcsv
   */
  public GestionFichierCSV(String fchcsv) {
    setNomFichier(fchcsv);
  }
  
  /**
   * Ajoute une ligne du fichier
   * @param ligne
   */
  public void add(String ligne) {
    if (contenuFichier == null) {
      contenuFichier = new ArrayList<String>(Constantes.MAXVAR);
    }
    
    contenuFichier.add(ligne);
  }
  
  /**
   * Convertit le fichier CSV en HTM (sans enregistrement du fichier à la fin)
   * @return
   */
  public int CSVtoHTM() {
    if (contenuFichier == null) {
      if (lectureFichier() == Constantes.ERREUR) {
        return Constantes.ERREUR;
      }
    }
    
    // Récupération juste du nom du fichier
    String nomfichier = getNomFichier().substring(getNomFichier().lastIndexOf('_') + 1, getNomFichier().lastIndexOf('.'));
    
    boolean isGuillemet = true;
    String chaine = null;
    String separateur = "\";\"";
    
    ArrayList<String> listeLigneHTM = new ArrayList<String>(Constantes.MAXVAR);
    // Ecriture de l'entête du fichier System.getProperty("file.encoding")
    // listeLigneHTM.add("<HTML><HEAD><META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html;
    // charset="+Constantes.CODEPAGE+"\">");
    listeLigneHTM
        .add("<HTML><HEAD><META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=" + System.getProperty("file.encoding") + "\">");
    // listeLigneHTM.add("<HTML><HEAD><META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html; charset=utf-8\">");
    // listeLigneHTM.add("<HTML><HEAD><META HTTP-EQUIV=\"CONTENT-TYPE\" CONTENT=\"text/html;CHARSET=iso-8859-1\">");
    listeLigneHTM.add("<TITLE>" + nomfichier + "</TITLE>");
    listeLigneHTM.add("<BODY LANG=\"fr-FR\" DIR=\"LTR\"><TABLE BORDER=1 CELLPADDING=2 CELLSPACING=0>");
    
    // Avant de commencer le traitement des lignes
    if (contenuFichier.size() != 0) {
      // Test pour savoir si le séparateur est la virgule ou le point-virgule (avec ou sans guillemet)
      isGuillemet = (contenuFichier.get(0).indexOf("\"") != -1);
      if (!isGuillemet) {
        separateur = ";";
        if (contenuFichier.get(0).indexOf(",") != -1) {
          separateur = ",";
        }
      }
      else if (contenuFichier.get(0).indexOf("\",\"") != -1) {
        separateur = "\",\"";
      }
      
      // On arrange l'entête des colonnes
      listeLigneHTM.add("<TR>");
      chaine = contenuFichier.get(0).replaceAll(separateur, "</FONT></TD><TD BGCOLOR=\"#0066FF\"><FONT COLOR=\"#FFFF99\">").trim();
      if ((chaine.charAt(0) == '"') && (chaine.charAt(chaine.length() - 1) == '"')) {
        chaine = chaine.substring(1, chaine.length() - 1);
      }
      listeLigneHTM.add("<TD BGCOLOR=\"#0066FF\"><FONT COLOR=\"#FFFF99\">" + chaine + "</FONT></TD>");
      listeLigneHTM.add("</TR>");
    }
    
    // Détail des lignes
    for (int i = 1; i < contenuFichier.size(); i++) {
      listeLigneHTM.add("<TR>");
      chaine = contenuFichier.get(i).replaceAll(separateur, "</TD><TD>").trim();
      if ((chaine.charAt(0) == '"') && (chaine.charAt(chaine.length() - 1) == '"')) {
        chaine = chaine.substring(1, chaine.length() - 1);
      }
      listeLigneHTM.add("<TD>" + chaine + "</TD>");
      listeLigneHTM.add("</TR>");
    }
    
    // Fin de fichier
    listeLigneHTM.add("</TABLE><P><BR><BR></P></BODY></HTML>");
    contenuFichier = listeLigneHTM;
    
    return Constantes.OK;
  }
  
  /**
   * Convertit le fichier CSV en XLS (avec enregistrement du fichier à la fin)
   * A revoir car l'algo est pourrave
   * @return
   */
  public int CSVtoXLS() {
    
    if (contenuFichier == null) {
      if (lectureFichier() == Constantes.ERREUR) {
        return Constantes.ERREUR;
      }
    }
    
    // Récupération juste du nom du fichier
    // String nomfichier = getNomFichier().substring(getNomFichier().lastIndexOf('_') + 1,
    // getNomFichier().lastIndexOf('.'));
    
    boolean isGuillemet = true;
    String chaine = null;
    String separateur = "\";\"";
    
    // Avant de commencer le traitement des lignes
    if (contenuFichier.size() != 0) {
      // Test pour savoir si le séparateur est la virgule ou le point-virgule (avec ou sans guillemet)
      isGuillemet = (contenuFichier.get(0).indexOf("\"") != -1);
      if (!isGuillemet) {
        separateur = ";";
        if (contenuFichier.get(0).indexOf(",") != -1) {
          separateur = ",";
        }
      }
      else if (contenuFichier.get(0).indexOf("\",\"") != -1) {
        separateur = "\",\"";
      }
    }
    
    // Détail des lignes
    HSSFWorkbook hwb = new HSSFWorkbook();
    HSSFSheet sheet = hwb.createSheet("new sheet");
    for (int k = 0; k < contenuFichier.size(); k++) {
      String ligne = contenuFichier.get(k);
      // On supprime les guillemets de début et fin de chaine qui restent après le split
      if (ligne.length() > 2) {
        if ((ligne.charAt(0) == '"') && (ligne.charAt(ligne.length() - 1) == '"')) {
          ligne = ligne.substring(1, ligne.length() - 1);
        }
      }
      
      String[] dataligne = ligne.split(separateur);
      HSSFRow row = sheet.createRow((short) 0 + k);
      for (int p = 0; p < dataligne.length; p++) {
        HSSFCell cell = row.createCell(p);
        chaine = dataligne[p].trim();
        // On teste si c'est une valeur numérique
        // Entier
        if (chaine.matches("(\\+|-)?(\\d*|0)")) {
          cell.setCellType(Cell.CELL_TYPE_NUMERIC);
          if (chaine.equals("")) {
            cell.setCellValue("");
          }
          else {
            try {
              cell.setCellValue(Integer.parseInt(chaine));
            }
            catch (Exception e) {
              cell.setCellValue(chaine);
            }
          }
        }
        // Décimal
        else if (chaine.matches("(\\+|-)?(\\d*\\,\\d+|\\d+\\,\\d*)((e|E)(\\+|-){0,1}\\d+){0,1}")) {
          cell.setCellType(Cell.CELL_TYPE_NUMERIC);
          if (chaine.equals("")) {
            cell.setCellValue("");
          }
          else {
            try {
              cell.setCellValue(Double.parseDouble(chaine.replace(',', '.')));
            }
            catch (Exception e) {
              cell.setCellValue(chaine);
            }
          }
        }
        // String
        else {
          cell.setCellType(Cell.CELL_TYPE_STRING);
          cell.setCellValue(chaine);
        }
      }
    }
    
    // Construction du nom du fichier sortant
    String nomfichier = getNomFichier();
    int pos = nomfichier.lastIndexOf('.');
    if (pos != -1) {
      nomfichier = nomfichier.substring(0, pos) + ".xls";
    }
    else {
      nomfichier += ".xls";
    }
    setNomFichier(nomfichier);
    
    // Enregistrement du fichier xls
    try {
      FileOutputStream fileOut = new FileOutputStream(nomfichier);
      hwb.write(fileOut);
      fileOut.close();
    }
    catch (Exception e) {
      return Constantes.ERREUR;
    }
    
    return Constantes.OK;
  }
  
  /**
   * Retourne les filtres possible pour les boites de dialogue.
   * @param pJFileChooser
   */
  public static void initFiltre(JFileChooser pJFileChooser) {
    // Ajout des extensions dans le composant
    FileFilter fileFilterCsv = new FiltreFichierParExtension(new String[] { FILTER_EXT_CSV }, FILTER_DESC_CSV);
    pJFileChooser.addChoosableFileFilter(fileFilterCsv);
    pJFileChooser.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { FILTER_EXT_HTM }, FILTER_DESC_HTM));
    pJFileChooser.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { FILTER_EXT_XLS }, FILTER_DESC_XLS));
    
    // Définit ce filtre comme le filtre par défaut
    pJFileChooser.setFileFilter(fileFilterCsv);
  }
  
  /**
   * Contrôle la présence de l'extension au nom du fichier et l'ajoute si nécessaire.
   * @param pJFileChooser
   */
  public static String controlerExtensionFichier(JFileChooser pJFileChooser) {
    if (pJFileChooser == null) {
      return null;
    }
    
    String extension = FILTER_EXT_CSV;
    if (pJFileChooser.getFileFilter() instanceof FiltreFichierParExtension) {
      extension = ((FiltreFichierParExtension) pJFileChooser.getFileFilter()).getFiltreExtension();
    }
    String fichierComplet = pJFileChooser.getSelectedFile().getAbsolutePath();
    
    // Si l'extension n'est pas l'extension par défaut
    if (!extension.equals(FILTER_EXT_CSV)) {
      // Remplacement de l'extension par défaut par la nouvelle
      fichierComplet = fichierComplet.replace("." + FILTER_EXT_CSV, "." + extension);
    }
    // Contrôle de présence de l'extension
    if (!fichierComplet.toLowerCase().endsWith(extension)) {
      if (fichierComplet.endsWith(".")) {
        fichierComplet += extension;
      }
      else {
        fichierComplet += '.' + extension;
      }
    }
    return fichierComplet;
  }
}
