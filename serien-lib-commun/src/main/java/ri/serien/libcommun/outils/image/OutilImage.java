/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.image;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

/**
 * Classe statique contenant des méthodes utilitaires pour la manipulation des images.
 * 
 * Des méthodes statiques permettent de travailler directement avec des instances de Color :
 * - isCouleurSombre(...)
 * - assombrirCouleur(...)
 * - eclaircirCouleur(...)
 * - getCouleurComplementaire(...)
 * 
 * Des méthodes statiques permettent de manipuler des instances de ImageIcon :
 * - assombriIcone()
 * - eclaircirIcone()
 * - griserIcone()
 * - redimensionnerIcone()
 * - reduireIcone()
 */
public class OutilImage {
  /**
   * Indiquer si une couleur est considérée comme sombre (luminosité < 50%)
   * @return true=la couleur est sombre, false=la couleur est claire.
   */
  public static boolean isCouleurSombre(Color pColorRGB) {
    // Convertir la couleur en HSL
    HSLColor hslColor = new HSLColor(pColorRGB);
    
    // Tester la luminosité
    return hslColor.getLuminosite() < 50;
  }
  
  /**
   * Assombir une couleur RGB d'un certain pourcentage.
   *
   * @param pColorRGB La couleur RGB à éclaircir.
   * @param pPourcentage Pourcentage d'assombrissement entre 0 et 1 (0=ne change rien, 1=couleur noire).
   * @return La couleur éclaircie au format RGB.
   */
  public static Color assombrirCouleur(Color pColorRGB, float pPourcentage) {
    // Retourner du noir si aucune couleur n'est fournie en paramètre
    if (pColorRGB == null) {
      return Color.BLACK;
    }
    
    // Retourner la couleur inchangée si la pourcentage est invalide
    if (pPourcentage < 0) {
      return pColorRGB;
    }
    
    // Calculer la couleur assombrie
    HSLColor hslColor = new HSLColor(pColorRGB);
    hslColor.assombrir(pPourcentage);
    return hslColor.getRGB();
  }
  
  /**
   * Eclaircir une couleur RGB d'un certain pourcentage.
   *
   * @param pColorRGB La couleur RGB à assombrir.
   * @param pPourcentage Pourcentage d'éclaircissement entre 0 et 1 (0=ne change rien, 1=couleur blanche).
   * @return La couleur assombrie au format RGB.
   */
  public static Color eclaircirCouleur(Color pColorRGB, float pPourcentage) {
    // Retourner du blanc si aucune couleur n'est fournie en paramètre
    if (pColorRGB == null) {
      return Color.WHITE;
    }
    
    // Retourner la couleur inchangée si la pourcentage est invalide
    if (pPourcentage < 0) {
      return pColorRGB;
    }
    
    // Calculer la couleur éclaircie
    HSLColor hslColor = new HSLColor(pColorRGB);
    hslColor.eclaircir(pPourcentage);
    return hslColor.getRGB();
  }
  
  /**
   * Griser une couleur RGB.
   *
   * @param pColorRGB La couleur RGB à assombrir. *
   * @return La couleur grisée au format RGB.
   */
  public static Color griserCouleur(Color pColorRGB) {
    // Retourner du blanc si aucune couleur n'est fournie en paramètre
    if (pColorRGB == null) {
      return null;
    }
    
    // Calculer la couleur éclaircie
    HSLColor hslColor = new HSLColor(pColorRGB);
    hslColor.griser();
    return hslColor.getRGB();
  }
  
  /**
   * Retourner la couleur RGB complémentaire.
   * La couleur complémentaire est déterminée en ajoutant 180 degré à la teinte (modulo 360).
   * @return Couleur complémentaire au format RGB.
   */
  public static Color getCouleurComplementaire(Color pColorRGB) {
    // Convertir la couleur en HSL
    HSLColor hslColor = new HSLColor(pColorRGB);
    
    // Calculer la couleur complémentaire
    float nouvelleTeinte = (hslColor.getTeinte() + 180.0f) % 360.0f;
    hslColor.setTeinte(nouvelleTeinte);
    return hslColor.getRGB();
  }
  
  /**
   * Assombir l'image d'une icône.
   * 
   * @Param pImageIcon L'icône à assombrir.
   * @param pPourcentage Pourcentage d'assombrissement entre 0 et 1 (0=ne change rien, 1=couleur noire).
   * @return Icône modifiée.
   */
  public static ImageIcon assombrirIcone(ImageIcon pImageIcon, float pPourcentage) {
    // Vérifier les paramètres
    if (pImageIcon == null) {
      return null;
    }
    
    // Récupérer les dimensions de l'icône à modifier
    int largeur = pImageIcon.getIconWidth();
    int hauteur = pImageIcon.getIconHeight();
    
    // Créer une image manipulable en mémoire de la même taille
    BufferedImage bufferedImage = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_ARGB);
    
    // Copier l'image de l'icône dans cette image
    Graphics g = bufferedImage.createGraphics();
    g.drawImage(pImageIcon.getImage(), 0, 0, null);
    g.dispose();
    
    // Parcourir les pixels un par un
    for (int y = 0; y < hauteur; y++) {
      for (int x = 0; x < largeur; x++) {
        int rgb = bufferedImage.getRGB(x, y);
        Color c = new Color(rgb, true);
        c = OutilImage.assombrirCouleur(c, pPourcentage);
        bufferedImage.setRGB(x, y, c.getRGB());
      }
    }
    
    // Convertir le résultat en ImageIcon
    return new ImageIcon(bufferedImage);
  }
  
  /**
   * Eclaircir l'image d'une icône.
   * 
   * @Param pImageIcon L'icône à éclaircir.
   * @param pPourcentage Pourcentage d'éclaircissement entre 0 et 1 (0=ne change rien, 1=couleur blanche).
   * @return Icône modifiée.
   */
  public static ImageIcon eclaircirIcone(ImageIcon pImageIcon, float pPourcentage) {
    // Vérifier les paramètres
    if (pImageIcon == null) {
      return null;
    }
    
    // Récupérer les dimensions de l'icône à modifier
    int largeur = pImageIcon.getIconWidth();
    int hauteur = pImageIcon.getIconHeight();
    
    // Créer une image manipulable en mémoire de la même taille
    BufferedImage bufferedImage = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_ARGB);
    
    // Copier l'image de l'icône dans cette image
    Graphics g = bufferedImage.createGraphics();
    g.drawImage(pImageIcon.getImage(), 0, 0, null);
    g.dispose();
    
    // Parcourir les pixels un par un
    for (int y = 0; y < hauteur; y++) {
      for (int x = 0; x < largeur; x++) {
        int rgb = bufferedImage.getRGB(x, y);
        Color c = new Color(rgb, true);
        c = eclaircirCouleur(c, pPourcentage);
        bufferedImage.setRGB(x, y, c.getRGB());
      }
    }
    
    // Convertir le résultat en ImageIcon
    return new ImageIcon(bufferedImage);
  }
  
  /**
   * Griser l'image d'une icône.
   * 
   * @Param pImageIcon L'icône à griser.
   * @return Icône modifiée.
   */
  public static ImageIcon griserIcone(ImageIcon pImageIcon) {
    // Vérifier les paramètres
    if (pImageIcon == null) {
      return null;
    }
    
    // Récupérer les dimensions de l'icône à modifier
    int largeur = pImageIcon.getIconWidth();
    int hauteur = pImageIcon.getIconHeight();
    
    // Créer une image manipulable en mémoire de la même taille
    BufferedImage bufferedImage = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_ARGB);
    
    // Copier l'image de l'icône dans cette image
    Graphics g = bufferedImage.createGraphics();
    g.drawImage(pImageIcon.getImage(), 0, 0, null);
    g.dispose();
    
    // Parcourir les pixels un par un
    for (int y = 0; y < hauteur; y++) {
      for (int x = 0; x < largeur; x++) {
        int rgb = bufferedImage.getRGB(x, y);
        Color c = new Color(rgb, true);
        c = griserCouleur(c);
        bufferedImage.setRGB(x, y, c.getRGB());
      }
    }
    
    // Convertir le résultat en ImageIcon
    return new ImageIcon(bufferedImage);
  }
  
  /**
   * Agrandir ou diminuer la taille d'une image en fonction d'un ratio.
   *
   * @param pImageIcon Image dont il faut changer la taille.
   * @param pRatio Si > 0, pourcentage d'agrandissement de l'image. Si < 0, pourcentage de diminution de l'image.
   * @return Image redimensionnée.
   */
  static public ImageIcon redimensionnerIcone(ImageIcon pImageIcon, double pRatio) {
    // Vérifier les paramètres
    if (pImageIcon == null) {
      return null;
    }
    
    // Calculer la taille de la nouvelle image
    int largeur = pRatio > 0 ? (int) (pImageIcon.getIconWidth() * pRatio) : (int) (pImageIcon.getIconWidth() * (1 + pRatio));
    int hauteur = pRatio > 0 ? (int) (pImageIcon.getIconHeight() * pRatio) : (int) (pImageIcon.getIconHeight() * (1 + pRatio));
    
    // Créer une nouvelle image
    BufferedImage bufferedImage = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_ARGB);
    
    // Dessiner sur le Graphics2D de l'image bufferisée
    Graphics2D graphics2D = bufferedImage.createGraphics();
    graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    graphics2D.drawImage(pImageIcon.getImage(), 0, 0, largeur, hauteur, null);
    graphics2D.dispose();
    
    // Retourner l'image modifiée
    return new ImageIcon(bufferedImage);
  }
  
  /**
   * Diminuer la taille d'une image pour atteindre une largeur et une hauteur maximum.
   * 
   * L'image est redimensionnée seulement si elle est plus grande que la largeur ou la hauteur souhaitée. Elle est redimensionnée en
   * respectant les proportions (la hauteur et la largeur évoluent proportionnellement).
   *
   * @param pImageIcon Image dont il faut changer la taille.
   * @param pLargeurMax Largeur maximum de la nouvelle image.
   * @param pHauteurMax Hauteur maximum de la nouvelle image.
   * @return Image redimensionnée.
   */
  static public ImageIcon reduireIcone(ImageIcon pImageIcon, int pLargeurMax, int pHauteurMax) {
    // Vérifier les paramètres
    if (pImageIcon == null || pLargeurMax < 0 || pHauteurMax < 0) {
      return null;
    }
    
    // Calculer le ratio pour ne pas dépasser la hauteur maximum
    double ratioHauteur = 0;
    if (pImageIcon.getIconHeight() > pHauteurMax) {
      ratioHauteur = (double) pHauteurMax / pImageIcon.getIconHeight();
    }
    else {
      ratioHauteur = 1;
    }
    
    // Calculer le ratio pour ne pas dépasser la largeur maximum
    double ratioLargeur = 0;
    if (pImageIcon.getIconHeight() > pLargeurMax) {
      ratioLargeur = (double) pLargeurMax / pImageIcon.getIconWidth();
    }
    else {
      ratioLargeur = 1;
    }
    
    // Redimensionner l'image suivant le plus petit des ratios
    if (ratioHauteur < ratioLargeur) {
      return redimensionnerIcone(pImageIcon, ratioHauteur);
    }
    else {
      return redimensionnerIcone(pImageIcon, ratioLargeur);
    }
  }
}
