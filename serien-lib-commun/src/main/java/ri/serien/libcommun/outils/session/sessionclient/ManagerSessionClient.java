/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session.sessionclient;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;
import ri.serien.libcommun.exploitation.parametreavance.EnumCleParametreAvance;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.session.AbstractSessionBase;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceSession;

/**
 * Gestionnaire des sessions côtés clients.
 * 
 * Ce gestionnaire contient la liste des sessions ouvertes sur le logiciel. La plupart du temps, une session correspond à un onglet
 * dans la barre de sessions en bas de l'écran. Certaines sessions peuvent exister sans onglet.
 * 
 */
public class ManagerSessionClient {
  // Constantes
  public static final int NOMBRE_MAX_SESSION = 20;
  // Délai de confirmation de présence du desktop auprès du serveur toutes les 5 mn
  public static final int DELAI_CONFIRMATION_PRESENCE_EN_SEC = 300;
  
  // Singleton
  private static ManagerSessionClient managerSessionClient = null;
  
  // Variables
  private EnvUser envUser = null;
  private ListeSessionClient listeSessionClient = new ListeSessionClient();
  
  /**
   * Constructeur.
   */
  private ManagerSessionClient() {
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne l'instance de la classe.
   */
  public static ManagerSessionClient getInstance() {
    if (managerSessionClient == null) {
      managerSessionClient = new ManagerSessionClient();
    }
    return managerSessionClient;
  }
  
  /**
   * Créer une nouvelle session client.
   * 
   * @return Identifiant de la nouvelle session client.
   */
  public synchronized IdSession creerSessionClient() {
    // Contrôler les parmaètres
    controlerEnvironnementUtilisateur();
    
    // Créer la session client
    IdSession idSession = ManagerServiceSession.creerSession(envUser.getIdClientDesktop());
    SessionClient sessionClient = new SessionClient(idSession);
    
    // Ajouter la session client à la liste
    listeSessionClient.add(sessionClient);
    
    // Tracer
    Trace.info("Création de la session client " + idSession);
    Trace.info("Nombre de sessions clients en cours = " + listeSessionClient.size());
    return idSession;
  }
  
  /**
   * Supprimer une session client.
   * 
   * Cela clôture également toutes les connexions associées à la session client.
   * 
   * @param pIdSession Identifiant de la session client à supprimer.
   */
  public synchronized void detruireSessionClient(IdSession pIdSession) {
    // Contrôler les parmaètres
    IdSession.controlerId(pIdSession, true);
    
    // Tracer
    Trace.info("Destruction de la session client " + pIdSession);
    
    // Récupérer la session client à supprimer
    SessionClient sessionClient = getSessionClient(pIdSession);
    if (sessionClient == null) {
      throw new MessageErreurException("Impossible de trouver la session client a supprimer à partir de son identifiant : " + pIdSession);
    }
    
    // Fermer la session côté serveur
    ManagerServiceSession.fermerSession(pIdSession);
    
    // Supprimer la session client de la liste
    listeSessionClient.remove(sessionClient);
    
    // Tracer
    Trace.info("Nombre de sessions clients restantes  " + listeSessionClient.size());
  }
  
  /**
   * Lancer un point de menu dans une nouvelle session depuis une session active.
   */
  public void lancerPointMenu(AbstractSessionBase pAbstractSessionBase, IdEnregistrementMneMnp pIdEnregistrementMneMnp,
      Object pParametreMenu) {
    if (pAbstractSessionBase == null) {
      throw new MessageErreurException("Impossible de lancer un point de menu car la session est invalide.");
    }
    pAbstractSessionBase.lancerPointMenu(pIdEnregistrementMneMnp, pParametreMenu);
  }
  
  /**
   * Vérifier si un droit est accordé pour cette session client suivant la valeur de la sécurité associée dans la gescom.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pDroitSecurite Constante du droit sécurité à lire.
   * @return true=le droit est accordé, false=le droit est refusé.
   */
  public boolean verifierDroitGescom(IdSession pIdSession, EnumDroitSecurite pDroitSecurite) {
    // Contrôler les paramètres
    controlerEnvironnementUtilisateur();
    IdSession.controlerId(pIdSession, true);
    
    // Récupérer la session client correspondant à l'id
    SessionClient sessionClient = ManagerSessionClient.getInstance().getSessionClient(pIdSession);
    if (sessionClient == null) {
      Trace.erreur("Aucune session client avec l'identifiant : " + pIdSession);
      return false;
    }
    
    // Vérifier le droit dans la session client
    return sessionClient.verifierDroitGescom(envUser.getProfil(), pDroitSecurite);
  }
  
  /**
   * Retourner la valeur d'un paramètre système (PS) pour la session en cours.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pEnumParametreSysteme Constante du paramètre système à lire.
   * @return Valeur du paramètre système (' ' si aucun paramètre ne correspond à la clé).
   */
  public Character getValeurParametreSysteme(IdSession pIdSession, EnumParametreSysteme pEnumParametreSysteme) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    
    // Récupérer la session client correspondant à l'id
    SessionClient sessionClient = ManagerSessionClient.getInstance().getSessionClient(pIdSession);
    if (sessionClient == null) {
      Trace.erreur("Aucune session client avec l'identifiant : " + pIdSession);
      return ' ';
    }
    
    // Lire la valeur du paramètre système dans la session client
    return sessionClient.getValeurParametreSysteme(pEnumParametreSysteme);
  }
  
  /**
   * Vérifier si un paramètre système (PS) de type A est actif dans la liste des paramètres systèmes.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pEnumParametreSysteme Constante du paramètre système à lire.
   * @return Valeur du paramètre système (false si aucun paramètre ne correspond à la clé).
   */
  public boolean isParametreSystemeActif(IdSession pIdSession, EnumParametreSysteme pEnumParametreSysteme) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    
    // Récupérer la session client correspondant à l'id
    SessionClient sessionClient = ManagerSessionClient.getInstance().getSessionClient(pIdSession);
    if (sessionClient == null) {
      Trace.erreur("Aucune session client avec l'identifiant : " + pIdSession);
      return false;
    }
    
    // Lire la valeur du paramètre système dans la session client
    return sessionClient.isParametreSystemeActif(pEnumParametreSysteme);
  }
  
  /**
   * Retourner la valeur d'un paramètre avancé pour la session client.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pEnumCleParametreAvance Clé du paramètre avancé à lire.
   * @return Valeur du paramètre avancé (null si aucun paramètre ne correspond à la clé).
   */
  public String getValeurParametreAvance(IdSession pIdSession, EnumCleParametreAvance pEnumCleParametreAvance) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    
    if (pEnumCleParametreAvance == null) {
      throw new MessageErreurException("Le paramètre avancé n'est pas valide.");
    }
    
    // Récupérer la session client correspondant à l'id
    SessionClient sessionClient = ManagerSessionClient.getInstance().getSessionClient(pIdSession);
    if (sessionClient == null) {
      Trace.erreur("Aucune session client avec l'identifiant : " + pIdSession);
      return null;
    }
    
    // Lire la valeur du paramètre système dans la session client
    return sessionClient.getValeurParametreAvance(pEnumCleParametreAvance);
  }
  
  /**
   * Retourner l'établissement associé à une session client.
   * 
   * @param pIdSession Identifiant de la session.
   * @return Identifiant de l'établissement associé à la session client.
   */
  public IdEtablissement getIdEtablissement(IdSession pIdSession) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    
    // Récupérer la session client correspondant à l'id
    SessionClient sessionClient = ManagerSessionClient.getInstance().getSessionClient(pIdSession);
    if (sessionClient == null) {
      Trace.erreur("Aucune session client avec l'identifiant : " + pIdSession);
      return null;
    }
    
    // Retourner l'établissement associé la session client
    return sessionClient.getIdEtablissement();
  }
  
  /**
   * Modifier l'établissement associé à une session client.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdEtablissement Identifiant de l'établissement à associer à la session client.
   */
  public void setIdEtablissement(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    
    // Récupérer la session client correspondant à l'id
    SessionClient sessionClient = ManagerSessionClient.getInstance().getSessionClient(pIdSession);
    if (sessionClient == null) {
      Trace.erreur("Aucune session client avec l'identifiant : " + pIdSession);
      return;
    }
    
    // Retourner l'établissement associé la session client
    sessionClient.setIdEtablissement(pIdEtablissement);
  }
  
  /**
   * Retourner la bibliothèque utilisateur en cours
   */
  public BDD getCurlib() {
    if (envUser == null) {
      Trace.erreur("EnvUser == null dans le manager de sessions client (curlib).");
      return null;
    }
    return envUser.getCurlib();
  }
  
  /**
   * Retourner le profil utilisateur en cours
   */
  public String getProfil() {
    if (envUser == null) {
      Trace.erreur("EnvUser == null dans le manager de sessions client (profil).");
      return null;
    }
    return envUser.getProfil();
  }
  
  /**
   * Charger une image à partir de son nom.
   * 
   * Les images sont séparées en deux catégories : les images systèmes qui font partie intégrante du logiciel (image d'une icône par
   * exemple) et les images clients (logo d'un établissement par exemple). Les stratégies de stockage sont différentes entre ces deux
   * catégories.
   * 
   * Les images sont recherchées à plusieurs endroits :
   * 1) Dans les ressources Java pour les images dites systèmes.
   * 2) Dans les dossiers locaux sur le poste de travail (si le paramètre correspondant est à true).
   * 3) Téléchargées à partir du serveur.
   * 
   * @param pNomImage Nom de l'image à charger.
   * @param pImageSysteme true si c'est un image système, false si c'est une image client.
   * @param pRechercheLocale true=l'image est recherchée dans les dossiers locaux du poste de travail.
   * @return Image.
   */
  public ImageIcon chargerImage(String pNomImage, boolean pImageSysteme, boolean pRechercheLocale) {
    // Contrôler les paramètres
    controlerEnvironnementUtilisateur();
    
    // Rechercher si une équivalence existe (dans ce cas, on l'utilise)
    // --> ambiguité avec certaines chaine ex: Créa./Sélec (avant c'était un lastIndexOf)
    int positionSeparateur = pNomImage.indexOf('/');
    if (positionSeparateur != -1) {
      String nomEquivalent = envUser.getTranslationImage().get(pNomImage.substring(positionSeparateur + 1));
      if (nomEquivalent != null) {
        pNomImage = nomEquivalent;
      }
    }
    
    // Rechercher en priorité dans les ressources (jar) s'il s'agit d'une image système
    if (pImageSysteme) {
      URL urlFichierImage = getClass().getClassLoader().getResource(pNomImage);
      if (urlFichierImage != null) {
        return new ImageIcon(urlFichierImage);
      }
    }
    
    // Chercher l'image sur le disque local si c'est demandé
    if (pRechercheLocale) {
      // Construire le chemin complet du fichier image
      String cheminCompletImage = null;
      if (pImageSysteme) {
        String nomDossierTravail = envUser.getDossierDesktop().getNomDossierTravail();
        cheminCompletImage = nomDossierTravail + pNomImage.replace("/", File.separator);
      }
      else {
        String nomDossierTempUser = envUser.getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
        cheminCompletImage = nomDossierTempUser + File.separatorChar + pNomImage.replace(Constantes.SEPARATEUR_DOSSIER, "_");
      }
      
      // Charger l'image à partir du fichier
      File fichierImage = new File(cheminCompletImage);
      if (fichierImage.exists()) {
        try {
          return new ImageIcon(ImageIO.read(fichierImage));
        }
        catch (IOException e) {
          Trace.erreur(e, "Erreur lors de la lecture sur le disque local de l'image : " + pNomImage);
          return null;
        }
      }
    }
    
    // Télécharger le fichier image à partir deu serveur si on ne l'a pas trouvé en local
    File fichierImage = null;
    if (pImageSysteme) {
      // Télécharger le fichier image
      envUser.getTransfertSession().telechargerFichierRuntime(pNomImage);
      
      // Charger le fichier image qui doit désormais être présent en local
      String nomDossierTravail = envUser.getDossierDesktop().getDossierTravail().getAbsolutePath() + File.separatorChar;
      fichierImage = new File(nomDossierTravail + pNomImage);
    }
    else {
      // Télécharger le fichier image
      envUser.getTransfertSession().fichierArecup(pNomImage);
      
      // Charger le fichier image qui doit désormais être présent en local
      String nomDossierTempUser = envUser.getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
      fichierImage = new File(nomDossierTempUser + File.separatorChar + pNomImage.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_'));
    }
    
    // Charger l'image à partir du fichier
    if (fichierImage.exists() && fichierImage.isFile()) {
      try {
        return new ImageIcon(ImageIO.read(fichierImage));
      }
      catch (IOException e) {
        Trace.erreur(e, "Erreur lors de la lecture sur le disque local de l'image : " + pNomImage);
        return null;
      }
    }
    
    // Retourner null en désespoir de cause
    return null;
  }
  
  /**
   * Charger le logo de l'établissement.
   * 
   * @param pCodeEtablissement Code de l'établissement.
   * @return Image.
   */
  public ImageIcon chargerLogoEtablissement(String pCodeEtablissement) {
    // Contrôler les paramètres
    controlerEnvironnementUtilisateur();
    
    // Déterminer le nom du logo
    String nomLogo = null;
    if (pCodeEtablissement != null && !pCodeEtablissement.trim().isEmpty()) {
      nomLogo = "images/logo_" + pCodeEtablissement.trim().toLowerCase() + ".png";
    }
    else {
      nomLogo = "images/logo.png";
    }
    
    // Récupérer l'image du logo de l'établissement
    ImageIcon pImageIcon = chargerImage(nomLogo, true, true);
    if (pImageIcon != null) {
      return pImageIcon;
    }
    
    // C'est une optimisation
    // Pour éviter les demandes incessantes au serveur, on renomme notre logo par défaut (il faudra améliorer le fonctionnement plus tard)
    String nomDossierTravail = envUser.getDossierServeur().getNomDossierTravail() + File.separatorChar;
    FileNG fileLogoRI = new FileNG(nomDossierTravail + "images/logo.png".replace("/", File.separator));
    if (fileLogoRI.exists()) {
      FileNG fileLogoClient = new FileNG(nomDossierTravail + nomLogo.replace("/", File.separator));
      if (!fileLogoClient.exists()) {
        fileLogoRI.copyTo(fileLogoClient.getAbsolutePath());
      }
      return getInstance().chargerImage(nomLogo, true, true);
    }
    else {
      return getInstance().chargerImage("images/logo.png", true, false);
    }
  }
  
  /**
   * Retourner l'URL d'une image.
   * 
   * @param pNomImage Nom de l'image à charger.
   * @param pImageSysteme true si c'est un image système, false si c'est une image client.
   * @return URL de l'image.
   */
  public URL getUrlImage(String pNomImage, boolean pImageSysteme) {
    // Contrôler les paramètres
    controlerEnvironnementUtilisateur();
    
    // Vérifier les paramètres.
    if (pNomImage == null) {
      return null;
    }
    try {
      // L'URL est différente pour les images systèmes et les images clients
      if (pImageSysteme) {
        String nomDossierTravail = envUser.getDossierDesktop().getNomDossierTravail() + File.separatorChar;
        return new File(nomDossierTravail + pNomImage).toURI().toURL();
      }
      else {
        String nomDossierTempUser = envUser.getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
        pNomImage = nomDossierTempUser + File.separatorChar + pNomImage.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_');
        return new File(pNomImage).toURI().toURL();
      }
    }
    catch (MalformedURLException e) {
      return null;
    }
  }
  
  /**
   * Retourner l'URL d'une image.
   * 
   * @param pCheminCompletImage Chemin complet du fichier image.
   * @return URL de l'image.
   */
  public URL getUrlImage(String pCheminCompletImage) {
    if (pCheminCompletImage == null) {
      return null;
    }
    try {
      return new File(pCheminCompletImage).toURI().toURL();
    }
    catch (MalformedURLException e) {
      return null;
    }
  }
  
  /**
   * 
   * Vérifier le nombre de sessions disponibles.
   * 
   * Indique s'il est possible d'ouvrir le nombre de sessions passé en paramètre en tenant compte du nombre de sessions déjà ouvertes
   * et du nombre maximal de sessions.
   * 
   * @param pNombreSessionsAOuvrir Nombre de session dont on veut tester la disponibilité.
   * @return Il reste assez de place pour le nombre de sessions passé en paramètre, false=il ne reste plus assez de place.
   */
  public boolean verifierNombreSessionDisponible(int pNombreSessionsAOuvrir) {
    return ((NOMBRE_MAX_SESSION - getNombreSessionClient()) >= pNombreSessionsAOuvrir);
  }
  
  /**
   * Contrôler que l'environnement utilsiateur a bien été renseigné.
   */
  private void controlerEnvironnementUtilisateur() {
    if (envUser == null) {
      throw new MessageErreurException("L'environnement utilisateur n'est pas renseigné dans le gestionnaire de session.");
    }
  }
  
  /**
   * Environnement de l'utilisateur.
   * @return Environnement de l'utilisateur (peut être null).
   */
  public EnvUser getEnvUser() {
    return envUser;
  }
  
  /**
   * Modifier l'environnement de l'utilisateur.
   * @param pEnvUser Nouvel environnement de l'utilisateur (peut être null).
   */
  public void setEnvUser(EnvUser pEnvUser) {
    envUser = pEnvUser;
  }
  
  /**
   * Retourner une session client à partir de son identifiant.
   * 
   * @param pIdSession Identifiant de la session.
   * @return Session client correspondant à l'identifiant (null si aucune ne correspond).
   */
  public SessionClient getSessionClient(IdSession pIdSession) {
    return listeSessionClient.retournerSessionClientParId(pIdSession);
  }
  
  /**
   * Retourner le nombre de session clients en cours.
   * 
   * @return Nombre de sessions clients en cours.
   */
  public int getNombreSessionClient() {
    return listeSessionClient.size();
  }
  
}
