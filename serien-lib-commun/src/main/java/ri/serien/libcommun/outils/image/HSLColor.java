/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.image;

import java.awt.Color;

/**
 * Couleur au format HSL (Hue=Teinte, S=saturation et L=Luminosité)
 *
 * La classe permet de manipuler une couleur au format HSL et de la convertir de et vers une couleur au format RGB. Elle est en
 * particulier pratique pour modifier la luminosité d'une couleur. En résumé :
 * - La teinte H est une valeur comprise entre 0 et 359.
 * - La saturation S est une valeur comprise entre 0 et 100.
 * - La luminosité L est une valeur comprise entre 0 et 100.
 * 
 * La teinte est la couleur. La teinte est exprimée entre 0 et 359 degré où le rouge correspond à 0, le vert à 120 et le bleu à 240.
 * Entre les deux, ce sont les couleurs de l'arc en ciel. La teinte correspond à la position dans le spectre lumineux.
 * 
 * La saturation est la pureté de la couleur, du moins "gris" si la saturation est maximale au plus "gris" si la saturation est minimale.
 * La saturation est exprimée sous forme d'un pourcentage entre 0 et 100 où 100 est la saturation maximale et zéro est la couleur la
 * plus fade (grise).
 *
 * La luminosité est la brillance de la couleur (de noire si aucune à blanche si maximum). La luminosité est exprimée sous forme d'un
 * pourcentage entre 0 et 100 où 0 correspond à la couleur noire tandis que que 100 correspond à couleur blanche.
 */
public class HSLColor {
  private Color colorRGB;
  private float[] hsl;
  private float alpha;
  
  /**
   * Créer une couleur HSL à partir d'une couleur RGB.
   * @param pColorRGB the RGB Color object
   */
  public HSLColor(Color pColorRGB) {
    // Mémoriser la couleur RGB
    colorRGB = pColorRGB;
    
    // Calculer les composantes hsl
    hsl = fromRGB(pColorRGB);
    
    // Calculer la valeur alpha
    alpha = pColorRGB.getAlpha() / 255.0f;
  }
  
  /**
   * Créer une couleur HSL à partir de ses composantes H, S et L (valeur alpha égale à 1).
   *
   * @param h Teinte entre 0 et 359.
   * @param s Saturation entre 0 et 100.
   * @param l Luminosité entre 0 et 100.
   */
  public HSLColor(float h, float s, float l) {
    this(h, s, l, 1.0f);
  }
  
  /**
   * Créer une couleur HSL à partir de ses composantes H, S, L et alpha.
   *
   * @param h Teinte entre 0 et 359.
   * @param s Saturation entre 0 et 100.
   * @param l Luminosité entre 0 et 100.
   * @param pAlpha Valeur alpha entre 0 et 1.
   */
  public HSLColor(float h, float s, float l, float pAlpha) {
    // Mémoriser les composantes HSL
    hsl = new float[] { h, s, l };
    
    // Mémoriser la valeur alpha
    alpha = pAlpha;
    
    // Calculer la couleur RGB
    colorRGB = toRGB(hsl, pAlpha);
  }
  
  /**
   * Retourner une représentatino texuelle de la couleur HSL.
   * Exemple : HSLColor[h=278,l=40,l=87]
   */
  @Override
  public String toString() {
    return "HSLColor[h=" + hsl[0] + ",s=" + hsl[1] + ",l=" + hsl[2] + ",alpha=" + alpha + "]";
  }
  
  /**
   * Composantes HSL de la couleur dans un tableau.
   * @return Tableau de valeurs HSL (index 0=teinte, 1=saturation, 2=luminosité).
   */
  public float[] getHSL() {
    return hsl;
  }
  
  /**
   * Couleur au format RGB correspondant à cette couleur HSL.
   * @return Couleur RGB.
   */
  public Color getRGB() {
    return colorRGB;
  }
  
  /**
   * Teinte de la couleur HSL.
   * @return Valeur de la teinte.
   */
  public float getTeinte() {
    return hsl[0];
  }
  
  /**
   * Modifier la teinte de la couleur HSL.
   * @param pTeinte Nouvelle teinte entre 0 et 359.
   */
  public void setTeinte(float pTeinte) {
    hsl[0] = pTeinte % 360.0f;
    colorRGB = toRGB(hsl, alpha);
  }
  
  /**
   * Saturation de la couleur HSL.
   * @return Valeur de la saturation.
   */
  public float getSaturation() {
    return hsl[1];
  }
  
  /**
   * Modifier la saturation de la couleur HSL.
   * @param pTeinte Nouvelle saturation entre 0 et 100.
   */
  public void setSaturation(float pSaturation) {
    if (pSaturation < 0.0f) {
      hsl[1] = 0.0f;
    }
    else if (pSaturation > 100.0f) {
      hsl[1] = 100.0f;
    }
    else {
      hsl[1] = pSaturation;
    }
    colorRGB = toRGB(hsl, alpha);
  }
  
  /**
   * Luminosité de la couleur HSL.
   * @return Valeur de la luminosité.
   */
  public float getLuminosite() {
    return hsl[2];
  }
  
  /**
   * Modifier la luminosité de la couleur HSL.
   * @param pLuminosite Nouvelle luminosité entre 0 et 100.
   */
  public void setLuminosite(float pLuminosite) {
    if (pLuminosite < 0.0f) {
      hsl[2] = 0.0f;
    }
    else if (pLuminosite > 100.0f) {
      hsl[2] = 100.0f;
    }
    else {
      hsl[2] = pLuminosite;
    }
    colorRGB = toRGB(hsl, alpha);
  }
  
  /**
   * Assombrir la couleur HSL suivant un pourcentage.
   * @param pPourcentage Pourcentage d'assombrissement entre 0 et 1 (0=ne change rien, 1=couleur noire).
   */
  public void assombrir(float pPourcentage) {
    hsl[2] = Math.max(0.0f, hsl[2] - hsl[2] * pPourcentage);
    colorRGB = toRGB(hsl, alpha);
  }
  
  /**
   * Eclaircir la couleur HSL suivant un pourcentage.
   * @param pPourcentage Pourcentage d'éclaircissement entre 0 et 1 (0=ne change rien, 1=couleur blanche).
   */
  public void eclaircir(float pPourcentage) {
    hsl[2] = Math.min(100.0f, hsl[2] + (100 - hsl[2]) * pPourcentage);
    colorRGB = toRGB(hsl, alpha);
  }
  
  /**
   * Griser la couleur HSL.
   */
  public void griser() {
    hsl[1] = 0;
    colorRGB = toRGB(hsl, alpha);
  }
  
  /**
   * Valeur alpha de la couleur HSL.
   * @return Valeur alpha.
   */
  public float getAlpha() {
    return alpha;
  }
  
  /**
   * Convertir une couleur RGB en ses composantes HSL (Hue=Teinte, Saturation, Lightness=Luminosité).
   *
   * La teinte H est une valeur comprise entre 0 et 359.
   * La saturation S est une valeur comprise entre 0 et 100.
   * La luminosité L est une valeur comprise entre 0 et 100.
   *
   * @param pColorRGB La couleur a convertir.
   * @return Un tableau contenant les 3 valeurs HSL.
   */
  public static float[] fromRGB(Color pColorRGB) {
    if (pColorRGB == null) {
      return new float[] { 0, 0, 0 };
    }
    
    // Récupérer les composants RGB avec des valeurs entre 0 et 1
    float[] rgb = pColorRGB.getRGBColorComponents(null);
    float r = rgb[0];
    float g = rgb[1];
    float b = rgb[2];
    
    // Déterminer les valeurs minimums et maximums des 3 composantes
    float min = Math.min(r, Math.min(g, b));
    float max = Math.max(r, Math.max(g, b));
    
    // Calculer la teinte
    float h = 0;
    if (max == min) {
      h = 0;
    }
    else if (max == r) {
      h = ((60 * (g - b) / (max - min)) + 360) % 360;
    }
    else if (max == g) {
      h = (60 * (b - r) / (max - min)) + 120;
    }
    else if (max == b) {
      h = (60 * (r - g) / (max - min)) + 240;
    }
    
    // Calculer la luminosité
    float l = (max + min) / 2;
    
    // Calculer la saturation
    float s = 0;
    
    if (max == min) {
      s = 0;
    }
    else if (l <= .5f) {
      s = (max - min) / (max + min);
    }
    else {
      s = (max - min) / (2 - max - min);
    }
    
    // Retourner le résultat
    return new float[] { h, s * 100, l * 100 };
  }
  
  /**
   * Convertir les composantes HSL en couleur RGB.
   * 
   * @param hsl Tableau avec les composants HSL.
   * @param alpha Valeur alpha.
   * @returns Couleur au format RGB.
   */
  public static Color toRGB(float[] hsl, float alpha) {
    return toRGB(hsl[0], hsl[1], hsl[2], alpha);
  }
  
  /**
   * Convertir les composantes HSL en couleur RGB.
   *
   * @param h La teinte H est une valeur comprise entre 0 et 359.
   * @param s La saturation S est une valeur comprise entre 0 et 100.
   * @param La luminosité L est une valeur comprise entre 0 et 100.
   * @param La valeur alpha est comprise entre 0 et 1.
   * @returns Couleur au format RGB.
   */
  public static Color toRGB(float h, float s, float l, float alpha) {
    // Corriger la teinte
    h = h % 360.0f;
    
    // Corriger la saturation
    if (s < 0.0f) {
      s = 0.0f;
    }
    else if (s > 100.0f) {
      s = 100.0f;
    }
    
    // Corriger la luminosité
    if (l < 0.0f) {
      l = 0.0f;
    }
    else if (l > 100.0f) {
      l = 100.0f;
    }
    
    // Corriger la valeur alpha
    if (alpha < 0.0f) {
      alpha = 0.0f;
    }
    else if (alpha > 1.0f) {
      alpha = 1.0f;
    }
    
    // Les calculs nécessitent des valeurs entre 0 et 1
    h /= 360f;
    s /= 100f;
    l /= 100f;
    
    float q = 0;
    
    if (l < 0.5) {
      q = l * (1 + s);
    }
    else {
      q = (l + s) - (s * l);
    }
    
    float p = 2 * l - q;
    
    float r = Math.max(0, HueToRGB(p, q, h + (1.0f / 3.0f)));
    float g = Math.max(0, HueToRGB(p, q, h));
    float b = Math.max(0, HueToRGB(p, q, h - (1.0f / 3.0f)));
    
    r = Math.min(r, 1.0f);
    g = Math.min(g, 1.0f);
    b = Math.min(b, 1.0f);
    
    return new Color(r, g, b, alpha);
  }
  
  /**
   * Convertir la teinte en couleur RGB.
   */
  private static float HueToRGB(float p, float q, float h) {
    if (h < 0) {
      h += 1;
    }
    
    if (h > 1) {
      h -= 1;
    }
    
    if (6 * h < 1) {
      return p + ((q - p) * 6 * h);
    }
    
    if (2 * h < 1) {
      return q;
    }
    
    if (3 * h < 2) {
      return p + ((q - p) * 6 * ((2.0f / 3.0f) - h));
    }
    
    return p;
  }
}
