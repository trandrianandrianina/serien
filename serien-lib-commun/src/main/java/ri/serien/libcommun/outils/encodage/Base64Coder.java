/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.encodage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * A Base64 Encoder/Decoder.
 * This class is used to encode and decode data in Base64 format as described in RFC 1521.
 * 
 * <p>Home page: <a href="http://www.source-code.biz">www.source-code.biz</a><br>
 * Author: Christian d'Heureuse, Inventec Informatik AG, Zurich, Switzerland<br>
 * Multi-licensed: EPL/LGPL/AL/BSD.
 */

public class Base64Coder {
  
  // Mapping table from 6-bit nibbles to Base64 characters.
  private static char[] map1 = new char[64];
  
  static {
    int i = 0;
    for (char c = 'A'; c <= 'Z'; c++) {
      map1[i++] = c;
    }
    for (char c = 'a'; c <= 'z'; c++) {
      map1[i++] = c;
    }
    for (char c = '0'; c <= '9'; c++) {
      map1[i++] = c;
    }
    map1[i++] = '+';
    map1[i++] = '/';
  }
  
  // Mapping table from Base64 characters to 6-bit nibbles.
  private static byte[] map2 = new byte[128];
  
  static {
    for (int i = 0; i < map2.length; i++) {
      map2[i] = -1;
    }
    for (int i = 0; i < 64; i++) {
      map2[map1[i]] = (byte) i;
    }
  }
  
  /**
   * Encodes a string into Base64 format.
   * No blanks or line breaks are inserted.
   * 
   * @param in A String to be encoded.
   * @return A String with the Base64 encoded data.
   */
  public static String encodeString(byte[] in) {
    return new String(encode(in));
  }
  
  public static String encodeString(byte[] in, int iLen) {
    return new String(encode(in, iLen));
  }
  
  /**
   * Encodes a string into Base64 format.
   * No blanks or line breaks are inserted.
   * @param s a String to be encoded.
   * @return A String with the Base64 encoded data.
   */
  public static String encodeString(String s) {
    return new String(encode(s.getBytes()));
  }
  
  /**
   * Encodes a byte array into Base64 format.
   * No blanks or line breaks are inserted.
   * @param in an array containing the data bytes to be encoded.
   * @return A character array with the Base64 encoded data.
   */
  public static char[] encode(byte[] in) {
    return encode(in, in.length);
  }
  
  /**
   * Encodes a byte array into Base64 format.
   * No blanks or line breaks are inserted.
   * @param in an array containing the data bytes to be encoded.
   * @param iLen number of bytes to process in <code>in</code>.
   * @return A character array with the Base64 encoded data.
   */
  public static char[] encode(byte[] in, int iLen) {
    int oDataLen = (iLen * 4 + 2) / 3; // output length without padding
    int oLen = ((iLen + 2) / 3) * 4; // output length including padding
    char[] out = new char[oLen];
    int ip = 0;
    int op = 0;
    while (ip < iLen) {
      int i0 = in[ip++] & 0xff;
      int i1 = ip < iLen ? in[ip++] & 0xff : 0;
      int i2 = ip < iLen ? in[ip++] & 0xff : 0;
      
      int o0 = i0 >>> 2;
      out[op++] = map1[o0];
      
      int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
      out[op++] = map1[o1];
      
      int o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
      out[op] = op < oDataLen ? map1[o2] : '=';
      op++;
      
      int o3 = i2 & 0x3F;
      out[op] = op < oDataLen ? map1[o3] : '=';
      op++;
    }
    return out;
  }
  
  /**
   * Decodes a string from Base64 format.
   * @param s a Base64 String to be decoded.
   * @return A String containing the decoded data.
   * @throws IllegalArgumentException if the input is not valid Base64 encoded data.
   */
  public static String decodeString(String s) {
    return new String(decode(s));
  }
  
  /**
   * Decodes a byte array from Base64 format.
   * @param s a Base64 String to be decoded.
   * @return An array containing the decoded data bytes.
   * @throws IllegalArgumentException if the input is not valid Base64 encoded data.
   */
  public static byte[] decode(String s) {
    return decode(s.toCharArray());
  }
  
  /**
   * Decodes a byte array from Base64 format.
   * No blanks or line breaks are allowed within the Base64 encoded data.
   * @param in a character array containing the Base64 encoded data.
   * @return An array containing the decoded data bytes.
   * @throws IllegalArgumentException if the input is not valid Base64 encoded data.
   */
  public static byte[] decode(char[] in) {
    int iLen = in.length;
    if (iLen % 4 != 0) {
      throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
    }
    while (iLen > 0 && in[iLen - 1] == '=') {
      iLen--;
    }
    int oLen = (iLen * 3) / 4;
    byte[] out = new byte[oLen];
    int ip = 0;
    int op = 0;
    while (ip < iLen) {
      int i0 = in[ip++];
      int i1 = in[ip++];
      int i2 = ip < iLen ? in[ip++] : 'A';
      int i3 = ip < iLen ? in[ip++] : 'A';
      if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127) {
        throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
      }
      int b0 = map2[i0];
      int b1 = map2[i1];
      int b2 = map2[i2];
      int b3 = map2[i3];
      if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0) {
        throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
      }
      int o0 = (b0 << 2) | (b1 >>> 4);
      int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
      int o2 = ((b2 & 3) << 6) | b3;
      out[op++] = (byte) o0;
      if (op < oLen) {
        out[op++] = (byte) o1;
      }
      if (op < oLen) {
        out[op++] = (byte) o2;
      }
    }
    return out;
  }
  
  /**
   * Décompresse une chaine contenant des données zipées puis encodé en base64 en chaine lisible.
   */
  public static String decompress(String zip64Text, String encodage) throws IOException {
    int size = 0;
    byte[] gzipBuff = Base64.decode(zip64Text);
    
    ByteArrayInputStream memstream = new ByteArrayInputStream(gzipBuff);
    GZIPInputStream gzin = new GZIPInputStream(memstream);
    
    final int buffSize = 32768;
    byte[] tempBuffer = new byte[buffSize];
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    while ((size = gzin.read(tempBuffer, 0, buffSize)) != -1) {
      baos.write(tempBuffer, 0, size);
    }
    
    byte[] buffer = baos.toByteArray();
    baos.close();
    
    return new String(buffer, encodage);
  }
  
  /**
   * Compresse une chaine de caractère en chaine zipées puis encodé en base64.
   */
  public static String compress(String text, String encodage) {
    try {
      byte[] gzipBuff = text.getBytes(encodage);
      ByteArrayOutputStream bs = new ByteArrayOutputStream();
      GZIPOutputStream gzin = new GZIPOutputStream(bs);
      gzin.write(gzipBuff);
      gzin.finish();
      bs.close();
      
      byte[] buffer = bs.toByteArray();
      gzin.close();
      
      // return Base64.encode(buffer);
      return new String(Base64.encodeToChar(buffer, false));
    }
    catch (IOException e) {
      // @todo Gérer correctement l'exception.
    }
    return "";
  }
  
} // end class Base64Coder
