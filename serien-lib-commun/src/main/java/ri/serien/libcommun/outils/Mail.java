/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils;

import java.util.ArrayList;
import java.util.regex.Pattern;

/*
 * Classe générique pour la constitution d'un email
 */

public class Mail {
  
  private String from = null;
  private ArrayList<String> to = null;
  private ArrayList<String> cc = null;
  private String subject = null;
  private String message = null;
  private ArrayList<String> joint = null;
  private boolean accuse = false;
  
  public Mail(ArrayList<String> destinataires, ArrayList<String> destinatairesCaches, String sujet, String corps,
      ArrayList<String> fichiersJoints, boolean accuseReception) {
    
    to = destinataires;
    cc = destinatairesCaches;
    subject = sujet;
    message = corps;
    joint = fichiersJoints;
    accuse = accuseReception;
    
  }
  
  public ArrayList<String> getTo() {
    return to;
  }
  
  public void setTo(ArrayList<String> to) {
    if (isEmailAdress(to)) {
      this.to = to;
    }
  }
  
  public ArrayList<String> getCc() {
    return cc;
  }
  
  public void setCc(ArrayList<String> cc) {
    this.cc = cc;
  }
  
  public String getSubject() {
    return subject;
  }
  
  public void setSubject(String subject) {
    this.subject = subject;
  }
  
  public String getMessage() {
    return message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public ArrayList<String> getJoint() {
    return joint;
  }
  
  public void setJoint(ArrayList<String> joint) {
    this.joint = joint;
  }
  
  public boolean isAccuse() {
    return accuse;
  }
  
  public void setAccuse(boolean accuse) {
    this.accuse = accuse;
  }
  
  /*
   * teste la validité d'une adresse mail (envoyée sous forme de string)
   */
  public static boolean isEmailAdress(String email) {
    
    String pat = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$";
    
    return Pattern.matches(pat, email);
  }
  
  /*
   *  teste la validité d'une liste d'adresses mails : renvoit FALSE si une adresse est fausse dans l'arrayList
   */
  public static boolean isEmailAdress(ArrayList<String> email) {
    
    String pat = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$";
    boolean ok = true;
    
    for (int i = 0; i < email.size(); i++) {
      if (!Pattern.matches(pat, email.get(i))) {
        ok = false;
      }
    }
    
    return ok;
  }
}
