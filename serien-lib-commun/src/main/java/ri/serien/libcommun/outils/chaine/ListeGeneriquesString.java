/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.chaine;

import java.util.HashMap;
import java.util.Map.Entry;

import ri.serien.libcommun.outils.MessageErreurException;

public class ListeGeneriquesString {
  // Variables
  protected GeneriqueString[] listeGeneriques = null;
  protected HashMap<String, String> listeLibelle = new HashMap<String, String>();
  
  // protected boolean exactCode = true;
  
  /**
   * Charge toutes les données.
   */
  public void chargerGeneriqueString(String[][] aDonnees/*, boolean aExactCode*/) {
    try {
      if (aDonnees == null) {
        throw new MessageErreurException("Aucune donnée disponible pour initialiser la liste des GeneriqueString.");
      }
      // exactCode = aExactCode;
      listeGeneriques = new GeneriqueString[aDonnees.length];
      for (int i = 0; i < aDonnees.length; i++) {
        listeGeneriques[i] = new GeneriqueString();
        for (int j = 0; j < aDonnees[i].length; j++) {
          listeGeneriques[i].setCode(aDonnees[i][0]);
          listeGeneriques[i].setLibelle(aDonnees[i][1]);
        }
      }
      chargerListeLibelleGeneriqueString();
    }
    catch (Exception e) {
      
    }
  }
  
  /**
   * Charge la liste des libellés des GeneriqueString.
   */
  private void chargerListeLibelleGeneriqueString() {
    if (listeGeneriques == null) {
      // TODO Message
      return;
    }
    
    listeLibelle.clear();
    for (GeneriqueString generique : listeGeneriques) {
      listeLibelle.put(generique.getCode(), generique.getLibelle());
    }
  }
  
  /**
   * Retourne la liste des libellés.
   */
  public String[] retournerListeLibelle() {
    String[] liste = new String[listeLibelle.size()];
    int i = 0;
    for (Entry<String, String> entry : listeLibelle.entrySet()) {
      liste[i++] = entry.getValue();
    }
    return liste;
  }
  
  /**
   * Retourne le code à partir de son indice dans la liste.
   */
  public String retournerCode(int indice) {
    int cpt = 0;
    for (Entry<String, String> entry : listeLibelle.entrySet()) {
      if (cpt == indice) {
        return entry.getKey();
      }
      cpt++;
    }
    return null;
  }
  
  /**
   * Retourne l'indice du code dans la liste.
   */
  public int retournerIndiceCode(String aCode) {
    aCode = aCode.trim();
    int indice = 0;
    for (Entry<String, String> entry : listeLibelle.entrySet()) {
      if (entry.getKey().equalsIgnoreCase(aCode)) {
        return indice;
      }
      indice++;
    }
    return 0;
  }
  
  /**
   * Retourne le libellé du GeneriqueString à partir de son code.
   */
  public String retournerLibelle(String aCode) {
    if ((aCode == null) || (aCode.trim().equals(""))) {
      return "";
    }
    for (GeneriqueString generique : listeGeneriques) {
      if (generique.getCode().equals(aCode)) {
        return generique.getLibelle();
      }
    }
    return "";
  }
  
}
