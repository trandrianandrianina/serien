/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.session;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.EnumBibliothequeProgramme;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Gestion de l'environnement du programme lancé sur l'I5.
 * @todo Faire un test sur la validité de la chaine pour le constructeur
 */
public class EnvProgram {
  // Constantes
  public static final char JAVA = 'J';
  public static final char AS400 = ' ';
  
  // Variables
  private String program = "";
  private String libraryEnv = "";
  private String library = "";
  private ArrayList<String> listeparam = null;
  private ArrayList<String> listebib = null;
  private String ptmenu = "inconnu";
  private String libmenu = "inconnu";
  private char type = AS400;
  // Conserve le dernier message d'erreur émit et non lu
  private String msgErreur = "";
  
  protected EnvUser infoUser = null;
  
  /**
   * Constructeur.
   * Note: on met la liste des bibliothèques utilisateurs et la bibliothèque d'environnement en ligne systèmatiquement.
   */
  public EnvProgram(final EnvUser pEnvUser) {
    setEnvUser(pEnvUser);
  }
  
  /**
   * Constructeur (Attention il manque la lettre d'environnement : mais normalement pas la peine car c'est le SEXG03CL
   * qui fait le boulot dans les IXXX.
   * Par contre, il faut s'assurer que le IXXX soit executé en premier)
   * Note : ce constructeur devra disparaitre car depuis RMI on peut faire transiter des objets.
   */
  public EnvProgram(String pValeur) {
    String[] infos = null;
    int nbrElt = 0;
    int i = 0;
    int k = 0;
    
    // Déchiffrage de la chaine
    // Chiffrage bf = new Chiffrage();
    // valeur = bf.decryptInString(valeur);
    
    // On découpe la chaine et on constitue un tableau de string à partir des différents éléments
    // 0:Config RT 1:Profil 2:Mdp 3:prefixdevice 4:subsystem 5:program 6:libraryEnv 7:library 8:curlib 9:folder
    // 10:device 11:programme 12:bibliothèque 13:nbr de param...suivi des params ??:nbr de bib...suivi des bib xx:Point
    // de menu
    infos = Constantes.splitString(pValeur, Constantes.SEPARATEUR_CHAINE_CHAR);
    
    // TODO Vérifier si tout ceci est encore utile avec la classe ClientDesktop qui rend unique la classe EnvUser
    if (infos != null && infos.length > 16) {
      infoUser = new EnvUser();
      IdSousEnvironnement idSousEnvironnement = IdSousEnvironnement.getInstance(infos[0]);
      infoUser.setIdSousEnvironnement(idSousEnvironnement);
      infoUser.setProfil(infos[1]);
      infoUser.setMotDePasse(infos[2]);
      infoUser.setPrefixDevice(infos[3]);
      infoUser.setNomSousSysteme(infos[4]);
      infoUser.setProgram(infos[5]);
      IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(infos[6]);
      infoUser.setBibliothequeEnvironnement(new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.SYSTEME_SERIEN));
      idBibliotheque = IdBibliotheque.getInstance(infos[7]);
      infoUser.setBibliothequeEnvironnementClient(new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.SYSTEME_SERIEN));
      idBibliotheque = IdBibliotheque.getInstance(infos[8]);
      infoUser.setCurlib(new BDD(idBibliotheque, infoUser.getLetter()));
      infoUser.setFolder(infos[9]);
      infoUser.setDeviceImpression(infos[10]);
      infoUser.setDevice(infos[11]);
      // On teste s'il s'agit du programme pour les menus et qu'il est java alors on le traite comme un programme normal
      if (infos[12].equals("")
          && ((infos[5].toLowerCase().lastIndexOf(".jar") != -1) || (infos[5].toLowerCase().lastIndexOf(".class") != -1))) {
        setProgram(infos[5]);
        setLibrary(infos[7]);
      }
      else {
        setProgram(infos[12]);
        setLibrary(infos[13]);
      }
      // On passe aux listes
      nbrElt = Integer.parseInt(infos[14]);
      for (i = 0; i < nbrElt; i++) {
        addParametre(infos[i + 15], false);
      }
      k = i + 15;
      nbrElt = Integer.parseInt(infos[k]);
      for (i = 0, k++; i < nbrElt; i++) {
        Trace.debug("-EnvProgram-> " + infos[i + k]);
        addBib(infos[i + k]);
      }
      
      k += i;
      // Le point de menu que l'on formate à la sauce SERIE N
      setPointMenu(infos[k].replace(" ", "").replace('.', ' '));
    }
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise l'environnement utilisateur.
   */
  public void setEnvUser(final EnvUser pEnvUser) {
    Trace.debug("=================================> setEnvUser");
    infoUser = pEnvUser;
    if (infoUser.getBibliothequeEnvironnementClient() == null) {
      return;
    }
    // Ajout systèmatique dans la liste des bibliothèque à mettre en ligne, attention l'ordre des bibliothèques est important
    // Les bibliothèques utilisateurs (en sens inverse de leur stockage dans la valeur système afin qu'elles soient bien ordonnées
    // ensuite)
    List<Bibliotheque> listeBibliothequeUtilisateur = infoUser.getListeBibliothequeUtilisateur();
    if (listeBibliothequeUtilisateur != null && !listeBibliothequeUtilisateur.isEmpty()) {
      for (int i = listeBibliothequeUtilisateur.size(); --i >= 0;) {
        addBib(listeBibliothequeUtilisateur.get(i).getNom());
      }
    }
    // Par mesure de sécurité, sinon les programmes RPG vont plantés
    else {
      addBib("QTEMP");
      addBib("QGPL");
    }
    // La bibliothèque d'environnement
    addBib(infoUser.getBibliothequeEnvironnement().getNom());
    // La base de données
    addBib(infoUser.getBibliothequeEnvironnementClient().getNom());
  }
  
  /**
   * Ajoute une bibliothèque à la liste (toujours en tête pour qu'elle soit avant QTEMP/QGPL/...).
   * Cette méthode modifie la première lettre des bibliothèques programmes avec la lettre d'environnement en cours.
   */
  public void addBib(String pNomBibliotheque) {
    if (listebib == null) {
      listebib = new ArrayList<String>();
    }
    pNomBibliotheque = Constantes.normerTexte(pNomBibliotheque);
    // Un contrôle est effectué afin de changer la première lettre de la bibliothèque
    // La lettre n'est pas modifiée:
    // - si la première lettre est la lettre Q elle est considèrée comme une bibliothèque système
    // - si son nom est "SPLSERIEM"
    // - si son nom est le même que celui de la bibliothèque d'environnement
    // - si son nom est le même que celui de la base de donnée
    // - si ce n'est pas une bibliothèque programme de Série N
    if (pNomBibliotheque.charAt(0) != 'Q' && !pNomBibliotheque.contains("SPLSERIEM")
        && !pNomBibliotheque.equals(infoUser.getBibliothequeEnvironnementClient().getNom())
        && !pNomBibliotheque.equals(infoUser.getBibliothequeEnvironnement().getNom())) {
      pNomBibliotheque = EnumBibliothequeProgramme.modifierLettreEnvironnement(infoUser.getLetter(), pNomBibliotheque);
    }
    
    if (!listebib.contains(pNomBibliotheque)) {
      listebib.add(0, pNomBibliotheque);
    }
  }
  
  /**
   * Ajoute un paramètre à la liste.
   */
  public void addParametre(String pParametre, boolean pTraiterApostrophe) {
    if (listeparam == null) {
      listeparam = new ArrayList<String>();
    }
    if (pParametre == null) {
      throw new MessageErreurException("Le paramètre à ajouter est incorrect.");
    }
    // Si les apostrophes ne doivent pas être traités alors le paramètre est stocké tel quel
    if (!pTraiterApostrophe) {
      listeparam.add(pParametre);
      return;
    }
    // Si les apostrophes doivent être traités ces dernières sont doublées sauf celles de début et de fin
    if (pParametre.length() > 2 && pParametre.charAt(0) == '\'' && pParametre.charAt(pParametre.length() - 1) == '\'') {
      listeparam.add("'" + traiterCaracteresSpeciaux(pParametre.substring(1, pParametre.length() - 1)) + "'");
    }
    else {
      listeparam.add(pParametre);
    }
  }
  
  /**
   * Retourne une chaine avec toutes les infos nécessaire pour l'ouverture d'une session.
   */
  public String getDemandeSession() {
    int i = 0;
    final StringBuffer sb = new StringBuffer(1024);
    
    sb.append(infoUser.getDevice()).append(Constantes.SEPARATEUR_CHAINE_CHAR).append(program).append(Constantes.SEPARATEUR_CHAINE_CHAR)
        .append(library).append(Constantes.SEPARATEUR_CHAINE_CHAR);
    if (listeparam != null) {
      sb.append(listeparam.size()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
      for (i = 0; i < listeparam.size(); i++) {
        Trace.debug("-listeparam->" + listeparam.get(i));
        sb.append(listeparam.get(i)).append(Constantes.SEPARATEUR_CHAINE_CHAR);
      }
    }
    else {
      sb.append("0").append(Constantes.SEPARATEUR_CHAINE_CHAR);
    }
    if (listebib != null) {
      sb.append(listebib.size()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
      // On écrit les bibliothèques en ordre inversé afin qu'elles soient dans le bon ordre sur le serveur à cause de la
      // méthode addBib
      for (i = listebib.size() - 1; i >= 0; i--) {
        Trace.debug("-listebib->" + listebib.get(i));
        sb.append(listebib.get(i)).append(Constantes.SEPARATEUR_CHAINE_CHAR);
      }
    }
    else {
      sb.append("0").append(Constantes.SEPARATEUR_CHAINE_CHAR);
    }
    
    // Ajout du point de menu
    sb.append(getPointMenu()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
    
    // A enlever plus tard (c'est pour trouver le bug rare qui envoi les bibs avec la mauvaise lettre de l'environnement)
    String chaine = sb.toString();
    Trace.debug(EnvProgram.class, "getDemandeSession", chaine);
    return chaine;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * Libère les ressources.
   */
  public void dispose() {
    infoUser = null;
    if (listeparam != null) {
      listeparam.clear();
      listeparam = null;
    }
    if (listebib != null) {
      listebib.clear();
      listebib = null;
    }
  }
  
  // -- Méthodes protégées
  
  // -- Méthodes privées
  
  /**
   * Traite les caractères spéciaux des Strings comme les apostrophes.
   */
  public static String traiterCaracteresSpeciaux(String texteOrigine) {
    if (texteOrigine == null) {
      return null;
    }
    
    return texteOrigine.replace("'", "''");
  }
  
  // -- Accesseurs
  
  /**
   * Initialise le programme initial.
   */
  public void setProgram(String valeur) {
    program = valeur.trim();
    
    // On vérifie le type du programme (Java ou autres)
    if (program == null) {
      return;
    }
    
    boolean isClass = (program.toLowerCase().lastIndexOf(".class") != -1) || (program.toLowerCase().lastIndexOf(".jar") != -1);
    if (isClass) {
      setType(JAVA);
      /*if (isClass)*/
      program = program.replaceAll(".class", "").replaceAll(".jar", "");
    }
    else {
      setType(AS400);
    }
  }
  
  /**
   * Initialise la bibliothèque du programme.
   */
  public void setLibraryEnv(String valeur) {
    libraryEnv = valeur.trim();
  }
  
  /**
   * Initialise la bibliothèque du programme.
   */
  public void setLibrary(String valeur) {
    library = valeur.trim();
  }
  
  /**
   * Initialise le point de menu.
   */
  public void setPointMenu(String valeur) {
    ptmenu = valeur.trim();
  }
  
  /**
   * Retourne l'environnement utilisateur.
   */
  public EnvUser getEnvUser() {
    return infoUser;
  }
  
  /**
   * Retourne la liste des bibliothèques.
   */
  public ArrayList<String> getListeBib() {
    return listebib;
  }
  
  /**
   * Retourne la liste des paramètres.
   */
  public ArrayList<String> getListeParam() {
    return listeparam;
  }
  
  /**
   * Retourne le programme.
   */
  public String getProgram() {
    return program;
  }
  
  /**
   * Retourne la bibliothèque du programme.
   */
  public String getLibraryEnv() {
    return libraryEnv;
  }
  
  /**
   * Retourne la bibliothèque du programme.
   */
  public String getLibrary() {
    return library;
  }
  
  /**
   * Retourne le point de menu.
   */
  public String getPointMenu() {
    return ptmenu;
  }
  
  /**
   * @param type the type to set.
   */
  public void setType(char type) {
    this.type = type;
  }
  
  /**
   * @return type.
   */
  public char getType() {
    return type;
  }
  
  /**
   * @param libmenu the libmenu to set.
   */
  public void setLibMenu(String libmenu) {
    if (libmenu != null) {
      this.libmenu = libmenu.trim();
    }
  }
  
  public String getLibMenu() {
    return libmenu;
  }
  
}
