/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.fichier;

import java.lang.reflect.Constructor;
import java.net.URL;

import javax.swing.JOptionPane;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioGeneric;
import ri.serien.libcommun.outils.composants.oRecord;

/**
 * ==> Extension de la gestion des fichiers binaires simple 13/06/2008 - 13/06/2008
 * ==> (version light de GestionFichierBIN)
 * ==> A faire:
 * ==> Ajouter test qui determine qu'on lit bien un fichier BIN car sinon il se plante ...
 * ==> Attention la classe est SENSIBLE à la CASSE (pb sur AS/400 & linux) à corriger
 * ==> Améliorer de manière générale le source comme GestionFichierTexte
 * ==> Ajouter test de validité lors de la lecture d'un fichier BIN
 */
public class GestionFichierDAText extends GestionFichierDAT {
  
  // ---------------------------------------------------------------------------------------------
  // --> Constructeur de la classe <--------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public GestionFichierDAText(String fchdat, int tableau[]) {
    super(fchdat, tableau);
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Constructeur de la classe <--------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public GestionFichierDAText(String fchdat, Integer tableau[]) {
    super(fchdat, tableau);
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Constructeur de la classe <--------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public GestionFichierDAText(String fchdat) {
    super(fchdat);
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Constructeur de la classe <--------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public GestionFichierDAText(URL ufchdat) {
    super(ufchdat);
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Constructeur de la classe <--------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public GestionFichierDAText() {
  }
  
  /**
   * Retourne l'objet principal après lecture du fichier BIN
   * ATTENTION aux importations: ici composants...
   */
  public Object getObject() {
    if (dataFic == null) {
      if (lectureFichier() == Constantes.ERREUR) {
        return null;
      }
    }
    
    // Récupération du nom de la classe de l'objet à instancier
    ioGeneric composant = null;
    int i = 0;
    int j = 0;
    byte tabByte[] = null;
    int lg = dataFic[i++];
    tabByte = new byte[lg];
    // récupération de la chaine
    lg = lg + i;
    for (j = 0; i < lg; i++) {
      tabByte[j++] = (byte) dataFic[i];
    }
    
    // Instanciation de l'objet par réflexion
    try {
      // Class<?> classe = Class.forName(new String(tabByte));
      String nomClasseObjet = new String(tabByte);
      if (!nomClasseObjet.startsWith("ri.")) {
        nomClasseObjet = oRecord.RACINEPACKAGE + nomClasseObjet;
      }
      Class<?> classe = GestionFichierDAText.class.getClassLoader().loadClass(nomClasseObjet);
      // Récupération du constructeur
      Constructor<?> constructeur = classe.getConstructor();
      composant = (ioGeneric) constructeur.newInstance();
    }
    catch (NoSuchMethodException e) {
      JOptionPane.showMessageDialog(null, "La classe n'a pas le constructeur recherché\n" + e.getMessage(),
          "[GestionFichierDAText] (getObject)", JOptionPane.ERROR_MESSAGE);
    }
    catch (InstantiationException e) {
      JOptionPane.showMessageDialog(null, "La classe est abstract ou est une interface\n" + e.getMessage(),
          "[GestionFichierDAText] (getObject)", JOptionPane.ERROR_MESSAGE);
    }
    catch (IllegalAccessException e) {
      JOptionPane.showMessageDialog(null, "La classe n'est pas accessible\n" + e.getMessage(), "[GestionFichierDAText] (getObject)",
          JOptionPane.ERROR_MESSAGE);
    }
    catch (java.lang.reflect.InvocationTargetException e) {
      JOptionPane.showMessageDialog(null, "Le constructeur invoqué a lui-même déclenché une exception\n" + e.getMessage(),
          "[GestionFichierDAText] (getObject)", JOptionPane.ERROR_MESSAGE);
    }
    catch (IllegalArgumentException e) {
      JOptionPane.showMessageDialog(null,
          "Mauvais type de paramètre (Pas obligatoire d'intercepter IllegalArgumentException)\n" + e.getMessage(),
          "[GestionFichierDAText] (getObject)", JOptionPane.ERROR_MESSAGE);
    }
    catch (ClassNotFoundException e) {
      JOptionPane.showMessageDialog(null, "La classe n'existe pas\n" + e.getMessage(), "[GestionFichierDAText] (getObject)",
          JOptionPane.ERROR_MESSAGE);
    }
    
    // On interprète le tableau d'entier du fichier BIN
    if (composant != null) {
      composant.binaireToValeurs(dataFic, i, tabListFichier);
    }
    
    return composant;
  }
  
}
