/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.outils.collection;

import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.swing.event.EventListenerList;

/**
 * Gestion dd'évènement sur une HashMap.
 */
public class HashMapManager {
  // Variables
  private EventListenerList listeners = new EventListenerList();
  private HashMap<Object, Object> map = new HashMap<Object, Object>();
  private boolean avtClear = false;
  private boolean avtAdd = false;
  private boolean avtRemove = true;
  
  /**
   * Constructeur.
   */
  public HashMapManager() {
    this(false, false, true);
  }
  
  /**
   * Constructeur.
   */
  public HashMapManager(boolean avtclear, boolean avtadd, boolean avtremove) {
    avtClear = avtclear;
    avtAdd = avtadd;
    avtRemove = avtremove;
  }
  
  public static interface Listener extends EventListener {
    public void onDataCleared();
    
    public void onDataAdded(Object cle, Object val);
    
    public void onDataRemoved(Object cle);
  }
  
  public void addListener(Listener l) {
    listeners.add(Listener.class, l);
  }
  
  public void removeListener(Listener l) {
    listeners.remove(Listener.class, l);
  }
  
  public HashMap<Object, Object> getHashMap() {
    return map;
  }
  
  /**
   * Effacer le contenu de la hashmap.
   */
  public void clearObject() {
    if (avtClear) {
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(Listener.class);
      for (int i = 0; i < el.length; i++) {
        ((Listener) el[i]).onDataCleared();
      }
      map.clear();
    }
    else {
      map.clear();
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(Listener.class);
      for (int i = 0; i < el.length; i++) {
        ((Listener) el[i]).onDataCleared();
      }
    }
  }
  
  /**
   * Ajouter un objet à la hashmap.
   */
  public void addObject(Object cle, Object val) {
    if (avtAdd) {
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(Listener.class);
      for (int i = 0; i < el.length; i++) {
        ((Listener) el[i]).onDataAdded(cle, val);
      }
      map.put(cle, val);
    }
    else {
      map.put(cle, val);
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(Listener.class);
      for (int i = 0; i < el.length; i++) {
        ((Listener) el[i]).onDataAdded(cle, val);
      }
    }
  }
  
  /**
   * Supprimer un objet de la hashmap.
   */
  public void removeObject(Object cle) {
    if (avtRemove) {
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(Listener.class);
      for (int i = 0; i < el.length; i++) {
        ((Listener) el[i]).onDataRemoved(cle);
      }
      map.remove(cle);
    }
    else {
      map.remove(cle);
      // for (Listener l : listeners.getListeners(Listener.class))
      EventListener[] el = listeners.getListeners(Listener.class);
      for (int i = 0; i < el.length; i++) {
        ((Listener) el[i]).onDataRemoved(cle);
      }
    }
  }
  
  /**
   * Récupérer un objet de la hashmap via sa clé.
   */
  public Object getObject(Object cle) {
    return map.get(cle);
  }
  
  /**
   * Récupérer une clé de la hashmap via son index.
   */
  public Object getKeyAtIndex(int index) {
    if (index < map.size()) {
      Set<Object> myKeys = map.keySet();
      Iterator<Object> myKeysIterator = myKeys.iterator();
      
      while (myKeysIterator.hasNext()) {
        Object cle = myKeysIterator.next();
        if (index-- == 0) {
          return cle;
        }
      }
    }
    return null;
  }
  
  /**
   * Récupérer un objet de la hashmap via son index.
   */
  public Object getValueAtIndex(int index) {
    return map.get(getKeyAtIndex(index));
  }
}
