/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Origine d'un document de vente.
 */
public enum EnumOrigineDocumentVente {
  NOUVEAU(0, "Nouveau"),
  EXTRACTION_DEVIS_EN_COMMANDE(1, "Extraction de devis en commande"),
  EXTRACTION_DEVIS_EN_BON(2, "Extraction de devis en bon"),
  EXTRACTION_COMMANDE_EN_BON(3, "Extraction de commande en bon"),
  EXTRACTION_DEVIS_EN_FACTURE(4, "Extraction de devis en facture"),
  EXTRACTION_COMMANDE_EN_FACTURE(5, "Extraction de commande en facture"),
  PRO_DEVIS(6, "Pro-devis");
  
  private final Integer code;
  private final String libelle;

  /**
   * Constructeur.
   */
  EnumOrigineDocumentVente(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }

  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }

  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }

  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumOrigineDocumentVente valueOfByCode(Integer pCode) {
    for (EnumOrigineDocumentVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'origine du document de vente est invalide : " + pCode);
  }
}
