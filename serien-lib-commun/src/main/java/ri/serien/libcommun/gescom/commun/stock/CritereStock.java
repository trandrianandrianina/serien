/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stock;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

/**
 * Critères de recherche pour le stock des articles.
 * 
 * Tous les critères sont optionnels à l'exception de l'identifiant article qui doit être renseigné. La recherche peut être définie
 * pour un seul article via la méthode setIdArticle() ou pour une liste d'articles via la méthode setListeIdArticle().
 * 
 * Il est possible de récupérer le stock :
 * - pour un magasin avec setIdMagasin(),
 * - pour un établissement avec setIdEtablissement(),
 * - pour tous les magasins d'un établissement avec setIdEtablissement() et setDetailParMagasin().
 */
public class CritereStock implements Serializable {
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private List<IdArticle> listeIdArticle = null;
  private boolean detailParMagasin = false;
  private boolean calculerStockAvantRupture = false;
  
  /**
   * Retourner l'identifiant de l'établissement sur lequel la recherche est effectuée.
   * @return Identifiant de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourner l'identifiant du magasin sur lequel la recherche est effectuée.
   * Si aucun magasin n'est renseigné, on recherche sur tous les magasins de l'établissement.
   * @return Identifiant du magasin.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier le filtre sur le magasin.
   * 
   * L'établissement est également mis à jour si le magasin n'est pas null. Si aucun magasin n'est renseigné, la recherche est
   * effectuée sur tous les magasins de l'établissement.
   * 
   * @param pIdMagasin Identifiant ud magasin.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
    if (idMagasin != null) {
      idEtablissement = idMagasin.getIdEtablissement();
    }
  }
  
  /**
   * Retourner la liste des identifiants articles sur lesquels la recherche est effectuée.
   * @return Liste d'identifiants articles.
   */
  public List<IdArticle> getListeIdArticle() {
    return listeIdArticle;
  }
  
  /**
   * Modifier le filtre sur les articles.
   * @param pListeIdArticle Liste d'identifiants articles.
   */
  public void setListeIdArticle(List<IdArticle> pListeIdArticle) {
    listeIdArticle = pListeIdArticle;
  }
  
  /**
   * Modifier le filtre sur les articles pour y mettre un seul article.
   * 
   * L'article dont l'identifiant est fourni en paramètre se subsitue à la liste d'articles. Cette méthode fournit une manière simple de
   * définir une liste d'articles composées d'un seul article.
   * 
   * @param pIdArticle Identifiant de l'article.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    IdArticle.controlerId(pIdArticle, false);
    listeIdArticle = new ArrayList<IdArticle>();
    listeIdArticle.add(pIdArticle);
  }
  
  /**
   * Vérifier si le détail par magasin a été demandé.
   * @return true=le stock est détaillé pas magasin, false=le stock est cumulé pour l'établissement.
   */
  public boolean isDetailParMagasin() {
    return detailParMagasin;
  }
  
  /**
   * Demander ou non le détail par magasin pour un établissement.
   * 
   * Ce paramètre est utile uniquement si on a demandé le stock d'un établissement. Ce paramètre est ignoré si l'identifiant du magasin
   * est renseigné.
   * 
   * @param pDetailParMagasin true=stock détaillé par magasin pour l'établissement, false=stock cumulé pour l'établissement.
   */
  public void setDetailParMagasin(boolean pDetailParMagasin) {
    detailParMagasin = pDetailParMagasin;
  }
  
  /**
   * Vérifier si le stock avant rupture est calculé.
   * @return true=le stock avant rupture est calculé, false=sinon.
   */
  public boolean isCalculerStockAvantRupture() {
    return calculerStockAvantRupture;
  }
  
  /**
   * Indiquer s'il faut calculer ou non le stock avant rupture.
   * @param pCalculerStockAvantRupture true=le stock avant rupture est calculé, false=sinon.
   */
  public void setCalculerStockAvantRupture(boolean pCalculerStockAvantRupture) {
    calculerStockAvantRupture = pCalculerStockAvantRupture;
  }
  
}
