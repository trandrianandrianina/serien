/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.groupe;

import ri.serien.libcommun.commun.recherche.CritereAvecEtablissement;

/**
 * Critères de recherche des groupes d'articles.
 */
public class CritereGroupe extends CritereAvecEtablissement {
  private boolean avecAnnule = false;
  
  /**
   * Retourner si les groupes annulés sont inclus.
   */
  public boolean isAvecAnnule() {
    return avecAnnule;
  }
  
  /**
   * Indiquer si on souhaite ou non inclure les groupes annulés.
   * @param pAvecAnnule true=inclure les groupes annulés, false est la valeur par défaut.
   */
  public void setAvecAnnule(boolean pAvecAnnule) {
    avecAnnule = pAvecAnnule;
  }
}
