/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.avoir;

import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste d'avoirs.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste d'avoirs.
 */
public class ListeAvoir extends ArrayList<Avoir> {
  /**
   * Constructeur.
   */
  public ListeAvoir() {
  }
  
  /**
   * Charger la liste des avoirs non réglés pour un client
   */
  public static ListeAvoir chargerListeAvoirsNonRegles(IdSession pIdSession, IdClient pIdClient) {
    return ManagerServiceDocumentVente.chargerListeAvoir(pIdSession, pIdClient);
  }
  
  /**
   * Retourner un Avoir de la liste à partir de son identifiant.
   */
  public Avoir retournerAvoirParId(IdAvoir pIdAvoir) {
    if (pIdAvoir == null) {
      return null;
    }
    for (Avoir avoir : this) {
      if (avoir != null && Constantes.equals(avoir.getId(), pIdAvoir)) {
        return avoir;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un Avoir est présent dans la liste.
   */
  public boolean isPresent(IdAvoir pIdAvoir) {
    return retournerAvoirParId(pIdAvoir) != null;
  }
}
