/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.document.ListeZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;

public class LigneAchatArticle extends LigneAchat implements Comparable<LigneAchatArticle> {
  
  // Constantes
  public static final String OPTION_VALIDATION = "VAL";
  public static final String OPTION_EXPEDITION = "EXP";
  public static final String OPTION_FACTURATION = "FAC";
  
  public static final int TAILLE_MAX_LIBELLE_ARTICLE = 120;
  
  public static final String TYPE_UNITE_VENTE_NORMALE = "";
  public static final String TYPE_UNITE_VENTE_PLAQUE = "M";
  public static final String TYPE_UNITE_VENTE_COLIS = "C";
  public static final String TYPE_UNITE_VENTE_PALETTE = "P";
  public static final String LIBELLE_UNITE_VENTE_NORMALE = "";
  public static final String LIBELLE_UNITE_VENTE_PLAQUE = "m²";
  public static final String LIBELLE_UNITE_VENTE_COLIS = "colis";
  public static final String LIBELLE_UNITE_VENTE_PALETTE = "palette";
  
  public static final int ORIGINE_NON_DEFINI = 0;
  public static final int ORIGINE_TARIF = 1;
  public static final int ORIGINE_CONDITION_DE_VENTE = 2;
  public static final int ORIGINE_SAISI = 3;
  
  // Cette variable définit le niveau de saisi pour les lots ou les numéros de série (il y aurait 3 niveaux)
  private Integer etatSaisieNumeroSerieOuLot = null;
  private Boolean optionSaisieNumeroSerie = Boolean.FALSE;
  private Boolean optionSaisieLot = Boolean.FALSE;
  private BigDecimal quantiteExtraiteUV = null;
  
  private Integer codeTVA = null;
  
  private Integer topLigneEnValeur = null;
  private Integer numeroColonneTVA = 0;
  private Integer signeLigne = 0; // Signe de la ligne
  
  // private BigDecimal quantiteUCA = null;
  // private BigDecimal quantiteUS = null;
  private Character codeAvoir = null;
  // private BigDecimal coefficientStockParCommande = null;
  // private BigDecimal coefficientAchatParCommande = null;
  private IdMagasin idMagasin = null;
  private IdMagasin idMagasinAvantModif = null;
  // Permet de faire le lien entre une commande d'achat et une commande d'achat
  private IdLigneAchat idLigneRapprochee = null;
  
  private Date dateHistoriqueStock = null;
  private Integer ordreHistoriqueStock = null;
  private String codeNatureAnalytique = null;
  
  private String codeSectionAnalytique = null;
  private String codeAffaire = null;
  private Character codeNatureAchat = null;
  private BigDecimal montantAffecte = null;
  
  private ListeZonePersonnalisee listeTopPersonnalisable = null;
  
  private Character imputationFrais = null;
  private Character typeFrais = null;
  private Character regroupementArticle = null;
  private Character comptabilisationStockFlottant = null;
  private Character conditionEnNombreGratuit = null;
  
  private Date dateLivraisonCalculee = null;
  
  private BigDecimal quantiteDocumentOrigine = null;
  private BigDecimal quantiteDejaExtraite = null;
  // Permet de faire le lien entre une ligne de commande d'achat et une ligne de vente
  private IdLigneVente idLigneVente = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public LigneAchatArticle(IdLigneAchat pIdLigneAchat) {
    super(pIdLigneAchat);
  }
  
  // -- Méthodes publiques
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  
  @Override
  public LigneAchatArticle clone() {
    LigneAchatArticle o = null;
    // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
    o = (LigneAchatArticle) super.clone();
    o.setId(id);
    if (getPrixAchat() != null) {
      o.setPrixAchat(getPrixAchat().clone());
    }
    
    return o;
  }
  
  /**
   * Comparer 2 lignes.
   */
  
  @Override
  public boolean equals(Object ligneAComparer) {
    // Tester si l'objet est nous-même (optimisation)
    if (ligneAComparer == this) {
      return true;
    }
    
    if (ligneAComparer == null) {
      return false;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(ligneAComparer instanceof LigneAchatArticle)) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    LigneAchatArticle ligne = (LigneAchatArticle) ligneAComparer;
    // On teste le prix de vente
    boolean pa = false;
    if (getPrixAchat() == null && ligne.getPrixAchat() == null) {
      pa = true;
    }
    else {
      pa = getPrixAchat().equals(ligne.getPrixAchat());
    }
    return id.equals(ligne.getId()) && pa;
  }
  
  /**
   * Permet de comparer avec une autre ligne article.
   */
  
  @Override
  public int compareTo(LigneAchatArticle pLigneAComparer) {
    return id.getNumeroLigne().compareTo(pLigneAComparer.getId().getNumeroLigne());
  }
  
  /**
   * Retourner le hashcode de l'objet.
   */
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getIdArticle().hashCode();
    // code = (int) (37 * code + getQuantiteCommandeeUV());
    // code = (int) (37 * code + getPrixAchatNetHT());
    // code = (int) (37 * code + getMontantHT());
    return code;
  }
  
  // -- Méthodes privées
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin pIdMagasin) {
    if (pIdMagasin == null) {
      idMagasin = null;
      return;
    }
    this.idMagasin = pIdMagasin;
  }
  
  public BigDecimal getQuantiteExtraiteUV() {
    return quantiteExtraiteUV;
  }
  
  public void setQuantiteExtraiteUV(BigDecimal quantiteExtraiteUV) {
    this.quantiteExtraiteUV = quantiteExtraiteUV;
  }
  
  public Character getCodeAvoir() {
    return codeAvoir;
  }
  
  public void setCodeAvoir(Character codeAvoir) {
    this.codeAvoir = codeAvoir;
  }
  
  public IdMagasin getIdMagasinAvantModif() {
    return idMagasinAvantModif;
  }
  
  public void setIdMagasinAvantModif(IdMagasin pMagasinAvantModif) {
    if (pMagasinAvantModif == null) {
      idMagasinAvantModif = null;
      return;
    }
    this.idMagasinAvantModif = pMagasinAvantModif;
  }
  
  public IdLigneAchat getIdLigneRapprochee() {
    return idLigneRapprochee;
  }
  
  public void setIdLigneRapprochee(IdLigneAchat idLigneRapprochee) {
    this.idLigneRapprochee = idLigneRapprochee;
  }
  
  public Date getDateHistoriqueStock() {
    return dateHistoriqueStock;
  }
  
  public void setDateHistoriqueStock(Date dateHistoriqueStock) {
    this.dateHistoriqueStock = dateHistoriqueStock;
  }
  
  public Integer getOrdreHistoriqueStock() {
    return ordreHistoriqueStock;
  }
  
  public void setOrdreHistoriqueStock(Integer ordreHistoriqueStock) {
    this.ordreHistoriqueStock = ordreHistoriqueStock;
  }
  
  public String getCodeSectionAnalytique() {
    return codeSectionAnalytique;
  }
  
  public void setCodeSectionAnalytique(String codeSectionAnalytique) {
    if (codeSectionAnalytique == null) {
      this.codeSectionAnalytique = null;
      return;
    }
    this.codeSectionAnalytique = codeSectionAnalytique.trim();
  }
  
  public String getCodeAffaire() {
    return codeAffaire;
  }
  
  public void setCodeAffaire(String codeAffaire) {
    if (codeAffaire == null) {
      this.codeAffaire = null;
      return;
    }
    this.codeAffaire = codeAffaire.trim();
  }
  
  public Character getCodeNatureAchat() {
    return codeNatureAchat;
  }
  
  public void setCodeNatureAchat(Character codeNatureAchat) {
    this.codeNatureAchat = codeNatureAchat;
  }
  
  public BigDecimal getMontantAffecte() {
    return montantAffecte;
  }
  
  public void setMontantAffecte(BigDecimal montantAffecte) {
    this.montantAffecte = montantAffecte;
  }
  
  public Integer getEtatSaisieNumeroSerieOuLot() {
    return etatSaisieNumeroSerieOuLot;
  }
  
  public void setEtatSaisieNumeroSerieOuLot(Integer etatSaisieNumeroSerieOuLot) {
    this.etatSaisieNumeroSerieOuLot = etatSaisieNumeroSerieOuLot;
  }
  
  public Character getImputationFrais() {
    return imputationFrais;
  }
  
  public void setImputationFrais(Character imputationFrais) {
    this.imputationFrais = imputationFrais;
  }
  
  public Character getTypeFrais() {
    return typeFrais;
  }
  
  public void setTypeFrais(Character typeFrais) {
    this.typeFrais = typeFrais;
  }
  
  public Character getRegroupementArticle() {
    return regroupementArticle;
  }
  
  public void setRegroupementArticle(Character regroupementArticle) {
    this.regroupementArticle = regroupementArticle;
  }
  
  public Character getComptabilisationStockFlottant() {
    return comptabilisationStockFlottant;
  }
  
  public void setComptabilisationStockFlottant(Character comptabilisationStockFlottant) {
    this.comptabilisationStockFlottant = comptabilisationStockFlottant;
  }
  
  public Character getConditionEnNombreGratuit() {
    return conditionEnNombreGratuit;
  }
  
  public void setConditionEnNombreGratuit(Character conditionEnNombreGratuit) {
    this.conditionEnNombreGratuit = conditionEnNombreGratuit;
  }
  
  public Date getDateLivraisonCalculee() {
    return dateLivraisonCalculee;
  }
  
  public void setDateLivraisonCalculee(Date dateLivraisonCalculee) {
    this.dateLivraisonCalculee = dateLivraisonCalculee;
  }
  
  public void setCodeTVA(Integer codeTVA) {
    this.codeTVA = codeTVA;
  }
  
  public void setTopLigneEnValeur(Integer topLigneEnValeur) {
    this.topLigneEnValeur = topLigneEnValeur;
  }
  
  public void setNumeroColonneTVA(Integer numeroColonneTVA) {
    this.numeroColonneTVA = numeroColonneTVA;
  }
  
  public void setSigneLigne(Integer signeLigne) {
    this.signeLigne = signeLigne;
  }
  
  public Integer getCodeTVA() {
    return codeTVA;
  }
  
  public Integer getTopLigneEnValeur() {
    return topLigneEnValeur;
  }
  
  public Integer getNumeroColonneTVA() {
    return numeroColonneTVA;
  }
  
  public Integer getSigneLigne() {
    return signeLigne;
  }
  
  public String getCodeNatureAnalytique() {
    return codeNatureAnalytique;
  }
  
  public void setCodeNatureAnalytique(String codeNatureAnalytique) {
    if (codeNatureAnalytique == null) {
      this.codeNatureAnalytique = null;
      return;
    }
    this.codeNatureAnalytique = codeNatureAnalytique.trim();
  }
  
  public ListeZonePersonnalisee getListeTopPersonnalisable() {
    return listeTopPersonnalisable;
  }
  
  public void setListeTopPersonnalisable(ListeZonePersonnalisee listeTopPersonnalisable) {
    this.listeTopPersonnalisable = listeTopPersonnalisable;
  }
  
  public Boolean getOptionSaisieNumeroSerie() {
    return optionSaisieNumeroSerie;
  }
  
  public void setOptionSaisieNumeroSerie(Boolean optionSaisieNumeroSerie) {
    this.optionSaisieNumeroSerie = optionSaisieNumeroSerie;
  }
  
  public Boolean getOptionSaisieLot() {
    return optionSaisieLot;
  }
  
  public void setOptionSaisieLot(Boolean optionSaisieLot) {
    this.optionSaisieLot = optionSaisieLot;
  }
  
  public BigDecimal getQuantiteDocumentOrigine() {
    return quantiteDocumentOrigine;
  }
  
  public void setQuantiteDocumentOrigine(BigDecimal quantiteDocumentOrigine) {
    this.quantiteDocumentOrigine = quantiteDocumentOrigine;
  }
  
  public BigDecimal getQuantiteDejaExtraite() {
    return quantiteDejaExtraite;
  }
  
  public void setQuantiteDejaExtraite(BigDecimal quantiteDejaExtraite) {
    this.quantiteDejaExtraite = quantiteDejaExtraite;
  }
  
  public IdLigneVente getIdLigneVente() {
    return idLigneVente;
  }
  
  public void setIdLigneVente(IdLigneVente idLigneVente) {
    this.idLigneVente = idLigneVente;
  }
  
}
