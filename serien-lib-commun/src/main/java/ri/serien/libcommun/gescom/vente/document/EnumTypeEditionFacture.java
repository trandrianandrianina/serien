/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'édition d'une facture ou d'un avoir
 */
public enum EnumTypeEditionFacture {
  EDITION_FACTURE('F', "Facture"),
  EDITION_TICKET('T', "Ticket de caisse");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeEditionFacture(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Le libellé associé au code en minuscules.
   */
  public String getLibelleEnMinuscules() {
    return libelle.toLowerCase();
  }
  
  /**
   * Retourner le libellé associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeEditionFacture valueOfByCode(Character pCode) {
    for (EnumTypeEditionFacture value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type d'édition de facture ou d'avoir est invalide : " + pCode);
  }
  
}
