/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.devise;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Devise.
 * La devise n'est pas lié à un établissement.
 */
public class Devise extends AbstractClasseMetier<IdDevise> {
  
  // Variables
  private String libelle = null;
  
  /**
   * Constructeur
   */
  public Devise(IdDevise pId) {
    super(pId);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  // --Accesseur
  /**
   * retourne le libelle
   */
  public String getLibelle() {
    return libelle;
  }
  
  /*
   * Modiffie le libelle
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
}
