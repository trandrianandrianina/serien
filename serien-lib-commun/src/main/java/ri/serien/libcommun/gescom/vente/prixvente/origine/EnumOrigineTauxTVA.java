/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.origine;

/**
 * Liste des origines possibles du taux de TVA.
 * Les origines sont classées par ordre de priorité (du plus prioritaire vers le moins prioritaire).
 */
public enum EnumOrigineTauxTVA {
  LIGNE_VENTE_AVEC_COLONNE_ENTETE("Ligne de vente (entête)"),
  LIGNE_VENTE_AVEC_COLONNE_ETABLISSEMENT("Ligne de vente (établissement)"),
  ARTICLE("Article"),
  ARTICLE_AVEC_COLONNE_ZERO("Article (colonne 0)"),
  DEFAUT("Par défaut"),;
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrigineTauxTVA(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
