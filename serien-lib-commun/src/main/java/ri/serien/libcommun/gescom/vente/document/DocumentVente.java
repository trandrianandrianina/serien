/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import ri.serien.libcommun.commun.memo.EnumTypeMemo;
import ri.serien.libcommun.commun.memo.IdMemo;
import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.exploitation.parametreavance.EnumCleParametreAvance;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.IdTypeFacturation;
import ri.serien.libcommun.gescom.vente.ligne.ActionRegroupement;
import ri.serien.libcommun.gescom.vente.ligne.EnumTypeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.Regroupement;
import ri.serien.libcommun.gescom.vente.prixvente.OutilCalculPrix;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Document de ventes.
 */
public class DocumentVente extends DocumentVenteBase {
  // Constantes
  public static final int LONGUEUR_PETIT_BLOCNOTE = 240;
  public static final int LONGUEUR_REFERENCE_COURTE = 8;
  
  public static final int INCREMENT_LIGNE_NORMAL = 40;
  
  // Constantes de l'entête
  public static final String MARQUEUR_ARTICLE_COMMENTAIRE_PRIS_PAR = ":";
  public static final String TEXTE_ARTICLE_COMMENTAIRE_PRIS_PAR = "Marchandises enlevées par : %s";
  
  public static final int TAILLE_ZONE_PRIS_PAR = 120;
  public static final int TAILLE_ZONE_IMMATRICULATION = 10;
  
  public static final String INDICATEURTOURNEE_BON_PAS_TOURNEE = "0";
  public static final String INDICATEURTOURNEE_BON_SUR_TOURNEE = "1";
  public static final String INDICATEURTOURNEE_BON_MULTI_TOURNEE = "2";
  public static final String INDICATEURTOURNEE_BON_MULTI_MAGASIN = "3";
  
  public static final boolean AVEC_EXTENTIONS = true;
  public static final boolean SANS_EXTENTIONS = false;
  
  // Variables
  private Boolean documentCharge = Boolean.FALSE;
  
  private BlocNote petitBlocNote = null;
  private Date dateRelanceClient = null; // Semaine choisi pour relancer le client (dans le cas d'un devis)
  private Adresse adresseFacturation = new Adresse(); // Adresse de facturation
  private String idContact = ""; // Clé du numéro du contact (etablissement + numero)
  private ListeLigneVente listeLigneVente = new ListeLigneVente();
  private ListeLigneVente listeLigneVenteSupprimee = new ListeLigneVente();
  private int nombreLigneNonHonorables = 0; // Nombre de lignes non honorables
  private boolean valide = false; // Le bon a été validé
  private String cheminPDF = null;
  // Numéro et suffixe du bon d'achat lié
  private IdDocumentAchat idDocumentAchatLie = null;
  // Etat lors du chargement du document ou en attente
  private EnumEtatBonDocumentVente etatOrigine = EnumEtatBonDocumentVente.ATTENTE;
  // Total TTC lors du chargement du document ou 0 en création
  private BigDecimal totalTTCOrigine = BigDecimal.ZERO;
  // Total TTC après chaque modification du document (insertion/suppression lignes articles)
  private BigDecimal totalTTCPrecedent = BigDecimal.ZERO;
  // Stocke l'id du document d'origine (Attention le contenu de cette variable n'est pas persisté en base, à utiliser
  // avec prudence)
  private IdDocumentVente idDocumentVenteOrigine = null;
  // La liste des regroupements (des lignes articles)
  private Map<Character, Regroupement> listeRegroupements = new HashMap<Character, Regroupement>();
  
  // Variables
  private String profilCreation = null;
  private Date dateDernierTraitementDevis = null;
  private int nombreLignes = 0;
  private int topEdition = 0;
  private int topClientLivre = 0;
  private int topClientFacture = 0;
  private int numeroTVA1 = 0; // de la DG
  private int numeroTVA2 = 0;
  private int numeroTVA3 = 0;
  private int topTaxeParafiscale = 0;
  private int topReleve = 0;
  private int nombreExemplairesFactures = 0;
  private int numeroTarif = 0;
  private int topRegroupementBonFacture = 0;
  private int signe = 0; // Signe du bon (type de document: si =1 c'est un doc normal, si =-1 c'est un avoir)
  private BigDecimal montantSoumisTVA1 = null;
  private BigDecimal montantSoumisTVA2 = null;
  private BigDecimal montantSoumisTVA3 = null;
  private BigDecimal montantTVA1 = null;
  private BigDecimal montantTVA2 = null;
  private BigDecimal montantTVA3 = null;
  private BigDecimal montantSoumisTaxeParafiscale = null;
  private BigDecimal montantTaxeParafiscale = null;
  private BigDecimal montantEscompte = null;
  private BigDecimal totalHTPrixRevient = null;
  private BigDecimal totalHTPrixVenteBase = null;
  private int topsTraitementSysteme = 0;
  private BigDecimal baseEscompte1 = null;
  private BigDecimal baseEscompte2 = null;
  private BigDecimal baseEscompte3 = null;
  private Date dateAffectation = null;
  private int arrondiTTCEnCentimes = 0;
  private int topEditionDevis = 0; // 0, 1 ou 2 (sic!)
  private BigDecimal montantCommandeInitiale = null;
  private int numeroFacture = 0; // NFA à 6 si un jour on a besoin
  private int numeroCompteAuxiliaireClient = 0;
  private int typeGenerationCommandeAchat = 0;
  private Date dateCommande = null;
  private IdClient idClientCentrale = null;
  private BigDecimal pourcentageEscompte = null;
  private BigDecimal tauxTVA1 = BigDecimal.ZERO;
  private BigDecimal tauxTVA2 = BigDecimal.ZERO;
  private BigDecimal tauxTVA3 = BigDecimal.ZERO;
  private BigDecimal tauxTaxeParafiscale = null;
  private BigDecimal pourcentageCommissionnementRep1 = null;
  private BigDecimal pourcentageCommissionnementRep2 = null;
  private IdClient idClientLivre = null;
  private IdTypeFacturation idTypeFacturation = null;
  private int delaiPreparation = 0; // Heure/jour ???
  private int nombreColis = 0;
  private BigDecimal poidsTotalKg = null;
  private int baseDevise = 0;
  private BigDecimal tauxChange = null;
  private BigDecimal reductionBaseCommissionRep1 = null;
  private BigDecimal reductionBaseCommissionRep2 = null;
  private Date dateMiniFacturation = null;
  private BigDecimal pourcentageRemise1Ligne = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise2Ligne = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise3Ligne = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise4Ligne = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise5Ligne = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise6Ligne = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise1Pied = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise2Pied = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise3Pied = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise4Pied = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise5Pied = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise6Pied = BigDecimal.ZERO;
  
  private int numeroTournee = 0;
  private int rangDansTournee = 0;
  private BigDecimal volume = BigDecimal.ZERO;
  private BigDecimal longueurMax = BigDecimal.ZERO;
  private BigDecimal surfacePlancher = BigDecimal.ZERO; // m² plancher
  
  private String referenceCourte = "";
  private ListeReglement listeReglement = null;
  private IdRepresentant idRepresentant1 = null;
  private IdRepresentant idRepresentant2 = null;
  private IdMagasin idMagasinSortieAncien = null;
  private Transport transport = null;
  private String sectionAnalytique = "";
  private String codeActionCommerciale = ""; // Affaire ou chantier - voir le code dans le paramètre AC
  private String codeDevise = "";
  private String codeConditionVente = "";
  private String transportACalculer = "";
  private String topEditionBonPreparation = "";
  private String typeCommissionnementRep = "";
  private String topBonNonConfirmable = "";
  private String topPersonnalisable1 = "";
  private String topPersonnalisable2 = "";
  private String topPersonnalisable3 = "";
  private String topPersonnalisable4 = "";
  private String topPersonnalisable5 = "";
  private String immatriculationVehiculeClient = "";
  private String topNePasFacturer = "";
  private String topIndicateurTournee = "";
  private String topTraiteEditee = "";
  private String topRemisesSpeciales = "";
  private String topExistenceRemisePied = "";
  private String topFacturecomptoir = "";
  private String codePreparationCommande = ""; // Code sélection / préparation commande
  private Character blocageExpedition = null;
  private String codeContrat = "";
  private String typeRemise = ""; // Type de remise ligne, 1=cascade
  private String baseRemise = ""; // Base de remise ligne, 1=Montant
  private String typeRemisePied = ""; // Type de remise/pied, 1=cascade
  private String topModeMesure = ""; // Poids/Colis/Volume saisis
  private String topExtractionForceeExpedition = "";
  private String codeCanalVente = "";
  private String codePreparateur = "";
  private String codePlateformeChargement = "";
  private String topTypeVente = "";
  // Id de l'adresse Fournisseur dans le cas des direct usine multi-fournisseur
  private IdAdresseFournisseur idAdresseFournisseur = null;
  private String prisPar = "";
  // Indique si le document est TTC ou HT
  private boolean ttc = false;
  private BigDecimal totalTVA = BigDecimal.ZERO;
  private Memo memoLivraisonEnlevement = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public DocumentVente(IdDocumentVente pIdDocumentVente) {
    super(pIdDocumentVente);
  }
  
  /**
   * Constructeur.
   */
  public DocumentVente(IdDocumentVente pIdDocumentVente, EnumTypeDocumentVente pTypeDocument) {
    super(pIdDocumentVente);
    setTypeDocumentVente(pTypeDocument);
  }
  
  @Override
  public String getTexte() {
    return referenceCourte;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdDocumentVente controlerId(DocumentVente pDocumentVente, boolean pVerifierExistance) {
    if (pDocumentVente == null) {
      throw new MessageErreurException("Le document de ventes est invalide.");
    }
    IdDocumentVente idDocumentVente = pDocumentVente.getId();
    if (idDocumentVente == null) {
      throw new MessageErreurException("L'identifiant du document de ventes est invalide.");
    }
    if (pVerifierExistance && !idDocumentVente.isExistant()) {
      throw new MessageErreurException("L'identifiant du document de ventes n'existe pas dans la base de données : " + idDocumentVente);
    }
    return idDocumentVente;
  }
  
  /**
   * Comparer les données de deux documents de ventes.
   */
  public boolean equalsComplet(DocumentVente pDocumentVente) {
    // Tester si l'objet est nous-même (optimisation)
    if (pDocumentVente == this) {
      return true;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (pDocumentVente == null) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    boolean champsEgaux = (nombreLigneNonHonorables == pDocumentVente.getNombreLigneNonHonorables() && valide == pDocumentVente.isValide()
        && etatOrigine == pDocumentVente.getEtatOrigine() && totalTTCOrigine == pDocumentVente.getTotalTTCOrigine()
        && totalTTCPrecedent == pDocumentVente.getTotalTTCPrecedent()
        && getId().getCodeEtablissement().equals(pDocumentVente.getId().getCodeEtablissement())
        && getTotalHT() == pDocumentVente.getTotalHT() && getTotalTTC() == pDocumentVente.getTotalTTC()
        && getTotalHTPrixRevient() == pDocumentVente.getTotalHTPrixRevient()
        && getTotalHTPrixVenteBase() == pDocumentVente.getTotalHTPrixVenteBase()
        && Constantes.equals(getIdClientFacture(), pDocumentVente.getIdClientFacture()) && isLivraison() == pDocumentVente.isLivraison());
    
    if (champsEgaux && getReferenceLongue() != null) {
      champsEgaux = getReferenceLongue().equals(pDocumentVente.getReferenceLongue());
    }
    if (champsEgaux && getReferenceCourte() != null) {
      champsEgaux = getReferenceCourte().equals(pDocumentVente.getReferenceCourte());
    }
    if (champsEgaux && getPrisPar() != null) {
      champsEgaux = getPrisPar().equals(pDocumentVente.getPrisPar());
    }
    if (champsEgaux && getPrisPar() != null) {
      champsEgaux = getPrisPar().equals(pDocumentVente.getPrisPar());
    }
    if (champsEgaux && getImmatriculationVehiculeClient() != null) {
      champsEgaux = getImmatriculationVehiculeClient().equals(pDocumentVente.getImmatriculationVehiculeClient());
    }
    if (champsEgaux && getDateCreation() != null) {
      champsEgaux = getDateCreation().compareTo(pDocumentVente.getDateCreation()) == 0;
    }
    if (champsEgaux && getDateValiditeDevis() != null) {
      champsEgaux = getDateValiditeDevis().compareTo(pDocumentVente.getDateValiditeDevis()) == 0;
    }
    
    return champsEgaux;
  }
  
  /**
   * Génère une ligne article à partir d'un article.
   */
  public LigneVente ajouterLigneArticleCommentaire(IdSession pIdSession, Article pArticle, int pNumeroLigne) {
    // Calculer le numéro de la ligne
    IdLigneVente idLigneVente =
        IdLigneVente.getInstance(id.getIdEtablissement(), id.getCodeEntete(), id.getNumero(), id.getSuffixe(), pNumeroLigne);
    
    // Créer la ligne de ventes
    LigneVente ligneVente = new LigneVente(idLigneVente);
    ligneVente.setIdArticle(pArticle.getId());
    ligneVente.setTypeLigne(EnumTypeLigneVente.COMMENTAIRE);
    ligneVente.setLibelle(pArticle.getLibelleComplet());
    ligneVente.setTypeArticle(EnumTypeArticle.COMMENTAIRE);
    
    // Contrôler si la ligne est intégrée à un regroupement en comparant son numéro de ligne
    if (pNumeroLigne > 0) {
      for (Entry<Character, Regroupement> entry : listeRegroupements.entrySet()) {
        if (entry.getValue().isContient(pNumeroLigne)) {
          ligneVente.setCodeRegroupement(entry.getKey());
          break;
        }
      }
    }
    
    return ligneVente;
  }
  
  /**
   * Génèrer une ligne de vente à partir d'un article avec les données de bases.
   */
  public LigneVente ajouterLigneArticle(IdSession pIdSession, Article pArticle, BigDecimal pQuantite, EnumTypeQuantite pEnumTypeQuantite,
      int pLigneInsertion, Date pDateTraitement) {
    // Vérifier les pré-requis
    if (pArticle.isArticleCommentaire()) {
      throw new MessageErreurException("Impossible d'insérer un article commentaire dans le document de ventes dans ce cas.");
    }
    
    // Calculer le numéro de la ligne suivant l'indice d'insertion
    int numeroLigne = calculerNumeroLigneArticle(pLigneInsertion);
    
    // Construire l'identifiant de la ligne de ventes
    IdLigneVente idLigneVente =
        IdLigneVente.getInstance(id.getIdEtablissement(), id.getCodeEntete(), id.getNumero(), id.getSuffixe(), numeroLigne);
    
    // Initialiser la ligne de ventes à partir de l'entête du document de ventes et de l'article
    // La ligne n'est pas créée en base de données à ce stade, le service initialise simplement les données de la ligne de vente
    LigneVente ligneVente =
        ManagerServiceDocumentVente.initialiserLigneVente(pIdSession, idLigneVente, pArticle.getId(), pDateTraitement);
    
    // Renseigner le magasin de la ligne
    ligneVente.setIdMagasin(getIdMagasin());
    
    // Renseigner le code regroupement de la ligne si elle est insérée dans un regroupement
    if (numeroLigne > 0) {
      for (Entry<Character, Regroupement> entry : listeRegroupements.entrySet()) {
        if (entry.getValue().isContient(numeroLigne)) {
          ligneVente.setCodeRegroupement(entry.getKey());
          break;
        }
      }
    }
    
    // Renseigner la quantité de la ligne en fonction du type de saisie
    switch (pEnumTypeQuantite) {
      // Saisie de la quantité en UCV
      case UNITE_CONDITIONNEMENT_VENTES:
        ligneVente.setQuantiteUCV(pQuantite);
        break;
      
      // Saisie de la quantité en UV
      case UNITE_VENTES:
        // Si c'est une article avec UCV non scindable il faut arrondir à la quantité en UCV supérieure
        if (pArticle.isUniteConditionnementVenteNonScindable()) {
          BigDecimal division[] = null;
          // Controle que le conditionnment ne vaut pas 0 afin d'éviter une division par zéro
          if (pArticle.isDefiniNombreUVParUCV()) {
            division = pQuantite.divideAndRemainder(pArticle.getNombreUVParUCV());
          }
          if (division != null && division[1].compareTo(BigDecimal.ZERO) != 0) {
            ligneVente.setQuantiteUV(division[0].add(BigDecimal.ONE).multiply(pArticle.getNombreUVParUCV())
                .setScale(ligneVente.getNombreDecimaleUV(), RoundingMode.HALF_UP));
          }
        }
        // Si c'est une article avec UCV scindable, pas d'arrondi de la quantité en UCV
        else {
          ligneVente.setQuantiteUV(pQuantite);
        }
        break;
      
      case UNITE_CONDITIONNEMENT_STOCKS:
        // Initialiser avec la quantité saisie (en UCS)
        BigDecimal quantite = pQuantite;
        
        // Convertir de UCS en US (US = USC par défaut)
        if (pArticle.isDefiniNombreUSParUCS()) {
          quantite = quantite.multiply(pArticle.getNombreUSParUCS(true));
        }
        
        // Convertir de US en UV (UV = US par défaut)
        if (ligneVente.isDefiniNombreUSParUV()) {
          quantite = quantite.divide(ligneVente.getNombreUSParUV(), 3, RoundingMode.HALF_UP);
        }
        
        // Mettre à jour la quantité de la ligne
        ligneVente.setQuantiteUV(quantite);
    }
    
    // Renseigner le nombre de découpe dans le cas d'un article à dimension variable (TODO A amémiorer)
    if (pArticle.isArticleDecoupable()) {
      ligneVente.setNombreDecoupe(pQuantite.intValue());
    }
    
    // Définir le type de montant (HT ou TTC)
    // Pas stocké au niveau de la ligne mais issue de l'entête (E1NAT = T)
    ligneVente.setClientTTC(isTTC());
    
    return ligneVente;
  }
  
  /**
   * Indique si au moins un article autre qu'un commentaire est présent dans le document de ventes.
   * Certaines opérations, comme la négociation totale par exemple, ne sont possibles que si au moins un article non
   * commentaire est présent dans le document de ventes.
   */
  public boolean isArticleHorsCommentairePresent() {
    if (listeLigneVente == null) {
      return false;
    }
    for (LigneVente ligneVente : listeLigneVente) {
      if (!ligneVente.isLigneCommentaire()) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Calcule le numéro de ligne de l'article dans le document à partir de son indice dans la table.
   * TODO l'algo est perfectible si on insère plusieurs fois au même endroit car les numéros sont mémorisés.
   * Idéalement il faudrait un outil qui recalcule les numéros de lignes du document.
   */
  public int calculerNumeroLigneArticle(int pIndiceLigne) {
    int numeroLigne;
    
    // La liste des lignes est vide
    if (listeLigneVente.isEmpty() && listeLigneVenteSupprimee.isEmpty()) {
      return INCREMENT_LIGNE_NORMAL;
    }
    
    // La ligne est insérée en tête de liste
    if (pIndiceLigne == 0) {
      numeroLigne = listeLigneVente.get(0).getId().getNumeroLigne().intValue() / 2;
      if (numeroLigne == 0) {
        throw new MessageErreurException(
            "L'article ne peut pas être inséré à cet endroit car il n'y a plus de numéro de ligne disponible.");
      }
      return numeroLigne;
    }
    
    // La ligne est insérée entre 2 lignes
    if (pIndiceLigne > -1 && pIndiceLigne < listeLigneVente.size()) {
      int numeroPrecedent = listeLigneVente.get(pIndiceLigne - 1).getId().getNumeroLigne().intValue();
      int numeroSuivant = listeLigneVente.get(pIndiceLigne).getId().getNumeroLigne().intValue();
      int increment = (numeroSuivant - numeroPrecedent) / 2;
      if (increment == 0) {
        throw new MessageErreurException(
            "L'article ne peut pas être inséré à cet endroit car il n'y a plus de numéro de ligne disponible.");
      }
      
      int numeroNouvelleLigne = numeroPrecedent + increment;
      
      // Vérifier que le numéro n'est pas déjà pris par une ligne supprimée
      while (!verifierNumeroLigneArticle(numeroNouvelleLigne)) {
        numeroNouvelleLigne = numeroNouvelleLigne + ((numeroSuivant - numeroNouvelleLigne) / 2);
      }
      return numeroNouvelleLigne;
    }
    
    // Sinon la ligne est insérée à la fin (hors numéro de lignes réservés pour les remises (sous les 9699))
    // Recherche la dernière ligne article hors remises spéciales
    int i = listeLigneVente.size() - 1;
    while (i >= 0 && listeLigneVente.get(i).isLigneRemiseSpeciale()) {
      i--;
    }
    if (i < 0) {
      numeroLigne = INCREMENT_LIGNE_NORMAL;
    }
    else {
      numeroLigne = listeLigneVente.get(i).getId().getNumeroLigne();
    }
    int reste = numeroLigne % INCREMENT_LIGNE_NORMAL;
    if (reste != 0) {
      numeroLigne = ((numeroLigne / INCREMENT_LIGNE_NORMAL) * INCREMENT_LIGNE_NORMAL);
    }
    numeroLigne += INCREMENT_LIGNE_NORMAL;
    
    // Contrôle que le numéro de ligne ne soit pas déjà utilisé
    while (!verifierNumeroLigneArticle(numeroLigne)) {
      numeroLigne += INCREMENT_LIGNE_NORMAL;
    }
    
    return numeroLigne;
  }
  
  /**
   * Vérifier que le numéro de ligne ne soit pas déjà utilisé (dans les lignes supprimées et lignes document).
   */
  public boolean verifierNumeroLigneArticle(int pNumero) {
    for (LigneVente ligneSupprimee : listeLigneVenteSupprimee) {
      if (pNumero == ligneSupprimee.getId().getNumeroLigne()) {
        return false;
      }
    }
    for (LigneVente ligneVente : listeLigneVente) {
      if (pNumero == ligneVente.getId().getNumeroLigne()) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Vide la liste des lignes (pour initialisation).
   */
  public void viderListes() {
    listeLigneVente.clear();
    listeLigneVenteSupprimee.clear();
    // On vide la liste d'Id de chaque regroupement
    for (Entry<Character, Regroupement> entry : listeRegroupements.entrySet()) {
      entry.getValue().viderListeLignesArticles();
    }
    listeRegroupements.clear();
  }
  
  /**
   * Indique si le document de vente est direct usine
   * et mono fournisseur
   */
  public boolean isDirectUsineMonoFournisseur() {
    return isDirectUsine() && isMonoFournisseur();
  }
  
  /**
   * Copie le contenu de l'adresse de facturation dans l'adresse de livraison
   */
  public void copierAdresseFacturationVersAdresseLivraison() {
    Transport transport = getTransport();
    if (transport.getAdresseLivraison() == null) {
      transport.setAdresseLivraison(new Adresse());
    }
    transport.getAdresseLivraison().setIdCivilite(adresseFacturation.getIdCivilite());
    transport.getAdresseLivraison().setNom(adresseFacturation.getNom());
    transport.getAdresseLivraison().setComplementNom(adresseFacturation.getComplementNom());
    transport.getAdresseLivraison().setRue(adresseFacturation.getRue());
    transport.getAdresseLivraison().setLocalisation(adresseFacturation.getLocalisation());
    transport.getAdresseLivraison().setCodePostal(adresseFacturation.getCodePostal());
    transport.getAdresseLivraison().setVille(adresseFacturation.getVille());
    transport.getAdresseLivraison().setPays(adresseFacturation.getPays());
    
  }
  
  /**
   * Indique s'il faut afficher la colonne quantité d'origine pour les lignes articles.
   * Les quantités d'origine sont affichées si le document est en modification ou lors d'une extraction.
   * Attention, ne fonctionne pas dans le cas d'une première extraction (lié au champ quantiteDocumentOrigine).
   */
  public boolean isAfficherColonneOrigine() {
    // Document en cours de modification affichage de la colonne
    if (!isEnCoursCreation()) {
      return true;
    }
    // Cas de l'extraction, la variable idDocumentVenteOrigine ne peut être utilisée
    // donc il y aura une colonne d'origine si la quantité restante d'au moins une ligne est supérieur à 0
    if (!listeLigneVente.isEmpty()) {
      for (LigneVente ligneVente : listeLigneVente) {
        BigDecimal quantiteOrigine = ligneVente.getQuantiteDocumentOrigineUC();
        BigDecimal quantiteActuelle = ligneVente.getQuantiteUCV();
        if (quantiteActuelle != null && quantiteOrigine != null && quantiteOrigine.compareTo(quantiteActuelle) > 0) {
          return true;
        }
      }
    }
    return false;
  }
  
  /**
   * Indique si le document de vente contient bien des lignes de vente dont tous les articles sont
   * tous direct usine
   */
  public boolean possedeArticlesDirectUsine(IdSession pIdSession) {
    if (pIdSession == null) {
      throw new MessageErreurException("L'id de la session est invalide.");
    }
    
    Boolean isTousArticlesDirectUsine = true;
    
    if (listeLigneVente != null && listeLigneVente.size() > 0) {
      isTousArticlesDirectUsine = ManagerServiceDocumentVente.existeArticleDirectUsine(pIdSession, listeLigneVente);
    }
    
    return isTousArticlesDirectUsine;
  }
  
  /**
   * Contrôle si le petit bloc-notes est vide.
   */
  public boolean isPetitBlocNoteVide() {
    return (petitBlocNote == null || Constantes.normerTexte(petitBlocNote.getTexte()).isEmpty());
  }
  
  /**
   * Retourner une ligne de vente à partir de son identifiant.
   */
  public LigneVente getLigneVente(IdLigneVente pIdLigneVente) {
    if (pIdLigneVente == null) {
      return null;
    }
    
    for (LigneVente ligneVente : listeLigneVente) {
      if (ligneVente != null && ligneVente.getId().equals(pIdLigneVente)) {
        return ligneVente;
      }
    }
    return null;
  }
  
  /**
   * Retourner une ligne de vente à partir de son index dans la liste.
   */
  public LigneVente getLigneVente(int pIndex) {
    if (pIndex < 0 || pIndex >= listeLigneVente.size()) {
      return null;
    }
    return listeLigneVente.get(pIndex);
  }
  
  /**
   * Retourner une ligne de vente à partir de l'identifiant de son article.
   */
  public LigneVente getLigneVente(IdArticle pIdArticle) {
    if (pIdArticle == null) {
      return null;
    }
    
    for (int index = 0; index < listeLigneVente.size(); index++) {
      LigneVente ligneVente = listeLigneVente.get(index);
      if (ligneVente != null && Constantes.equals(pIdArticle, ligneVente.getIdArticle())) {
        return ligneVente;
      }
    }
    return null;
  }
  
  /**
   * Retourne l'index d'une ligne de vente à partir de son identifiant.
   */
  public int getIndexLigneVente(IdLigneVente pIdLigneVente) {
    if (pIdLigneVente == null) {
      return -1;
    }
    
    for (int index = 0; index < listeLigneVente.size(); index++) {
      LigneVente ligneVente = listeLigneVente.get(index);
      if (ligneVente != null && ligneVente.getId().equals(pIdLigneVente)) {
        return index;
      }
    }
    return -1;
  }
  
  /**
   * Recherche et met à jour les données des regroupements en parcourant les lignes de vente.
   */
  public void actualiserRegroupement(IdSession pIdSession) {
    if (pIdSession == null) {
      throw new MessageErreurException("L'id de la session est invalide.");
    }
    
    listeRegroupements.clear();
    for (LigneVente ligne : listeLigneVente) {
      if (ligne.isLigneRegroupee() && !listeRegroupements.containsKey(ligne.getCodeRegroupement())) {
        Regroupement regroupement = new Regroupement(ligne.getCodeRegroupement());
        regroupement = ManagerServiceDocumentVente.chargerRegroupement(pIdSession, ligne.getId(), regroupement);
        chargerLignesPourUnRegroupement(regroupement);
      }
    }
  }
  
  /**
   * Supprime des lignes articles du documents.
   */
  public ActionRegroupement supprimerLignesArticles(ListeLigneVente listeLigneVenteASupprimer) {
    if (listeLigneVenteASupprimer == null) {
      throw new MessageErreurException("La liste des lignes articles à supprimer est à null.");
    }
    if (listeLigneVenteASupprimer.isEmpty()) {
      return null;
    }
    
    Character referenceRegroupement = listeLigneVenteASupprimer.get(0).getCodeRegroupement();
    
    // S'il y a plusieurs lignes à supprimer, on la cohérence de la sélection
    if (listeLigneVenteASupprimer.size() > 1) {
      // On controle que les lignes articles ne soient pas un mélange de lignes regroupées, de lignes non regroupées ou
      // de regroupements différents
      for (LigneVente ligneVente : listeLigneVenteASupprimer) {
        if (ligneVente != null && !Constantes.equals(referenceRegroupement, ligneVente.getCodeRegroupement())) {
          throw new MessageErreurException(
              "La sélection des lignes articles à supprimer contient à la fois des lignes regroupées et des lignes non regroupées"
                  + " ou des lignes appartenants à des regroupements différents.\nVeuillez modifier votre sélection.");
        }
      }
    }
    
    // S'il s'agit de lignes articles regroupées
    if (referenceRegroupement != ' ') {
      // On analyse le regroupment afin de savoir si on doit le supprimer ou si une mise à jour suffit
      Regroupement regroupement = listeRegroupements.get(referenceRegroupement);
      if (regroupement == null) {
        throw new MessageErreurException(
            String.format("Le regroupement '%s' n'a pas été trouvé dans la liste des regroupements du document.", referenceRegroupement));
      }
      return new ActionRegroupement(regroupement.supprimerLignes(listeLigneVenteASupprimer), regroupement);
    }
    
    return null;
  }
  
  /**
   * Contrôle si une extraction est possible sur le document.
   */
  public void controlerExtractionPossible() {
    // Dans le cas d'un devis
    if (isDevis()) {
      if (getEtatDevis().equals(EnumEtatDevisDocumentVente.ENREGISTRE) || getEtatDevis().equals(EnumEtatDevisDocumentVente.EDITE)
          || getEtatDevis().equals(EnumEtatDevisDocumentVente.SIGNE)) {
        // On peut faire l'extraction
        return;
      }
      else {
        String etat = "l'état du devis est inconnu.";
        switch (getEtatDevis()) {
          case ATTENTE:
            etat = "le devis est en attente.";
            break;
          case VALIDITE_DEPASSEE:
            etat = "la date de validité du devis est dépassée.";
            break;
          case PERDU:
            etat = "le devis est perdu.";
            break;
          case CLOTURE:
            etat = "le devis est cloturé.";
            break;
          default:
            break;
        }
        throw new MessageErreurException("Vous ne pouvez pas faire d'extraction sur ce document, car " + etat);
      }
    }
    // Dans le cas d'une commande ou d'un bon
    else {
      if (getEtat() == EnumEtatBonDocumentVente.VALIDE || getEtat() == EnumEtatBonDocumentVente.RESERVE) {
        // On peut faire l'extraction
        return;
      }
      else {
        String etat = "l'état du devis est inconnu.";
        switch (getEtat()) {
          case ATTENTE:
            etat = "le document est en attente.";
            break;
          case EXPEDIE:
            etat = "le bon a été expédié.";
            break;
          case FACTURE:
            etat = "le document a été facturé.";
            break;
          case COMPTABILISE:
            etat = "le document a été comptabilisé.";
            break;
          default:
            break;
        }
        throw new MessageErreurException("Vous ne pouvez pas faire d'extraction sur ce document, car " + etat);
      }
    }
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public DocumentVente clone() {
    DocumentVente documentVente = null;
    // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
    documentVente = (DocumentVente) super.clone();
    documentVente.setAdresseFacturation(adresseFacturation.clone());
    ListeLigneVente listeLigneVenteTemp = new ListeLigneVente();
    for (LigneVente ligneVente : listeLigneVente) {
      listeLigneVenteTemp.add(ligneVente.clone());
    }
    documentVente.setListeLignesArticles(listeLigneVenteTemp);
    listeLigneVenteTemp = new ListeLigneVente();
    for (LigneVente ligneVente : listeLigneVenteSupprimee) {
      listeLigneVenteTemp.add(ligneVente.clone());
    }
    documentVente.setListeLignesArticlesSupprimees(listeLigneVenteTemp);
    if (idDocumentVenteOrigine != null) {
      documentVente.setIdOrigine(idDocumentVenteOrigine);
    }
    
    return documentVente;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne le total des TVA
   */
  private void calculerTotalTVA() {
    if (montantTVA1 == null) {
      montantTVA1 = BigDecimal.ZERO;
    }
    if (montantTVA2 == null) {
      montantTVA2 = BigDecimal.ZERO;
    }
    if (montantTVA3 == null) {
      montantTVA3 = BigDecimal.ZERO;
    }
    setTotalTVA(montantTVA1.add(montantTVA2).add(montantTVA3));
  }
  
  /**
   * Charge les lignes articles dans un regroupement.
   */
  private boolean chargerLignesPourUnRegroupement(Regroupement pRegroupement) {
    if (pRegroupement == null) {
      return false;
    }
    
    // On récupère les indices des lignes articles appartenant au regroupement
    boolean charger = false;
    for (LigneVente ligneVente : listeLigneVente) {
      if ((!charger) && ligneVente.getId().equals(pRegroupement.getIdLigneVenteDebut())) {
        ligneVente.setDebutRegroupement(true);
        charger = true;
      }
      if (charger) {
        pRegroupement.getListeLigneVente().add(ligneVente);
        if (ligneVente.getId().equals(pRegroupement.getIdLigneVenteFin())) {
          ligneVente.setFinRegroupement(true);
          break;
        }
      }
    }
    listeRegroupements.put(pRegroupement.getReference(), pRegroupement);
    
    return true;
  }
  
  /**
   * Retourne si toutes les lignes du document sélectionnées appartiennent au même regroupement.
   */
  public boolean isLignesVenteAppartiennentMemeGroupe(int[] pLignesSelectionnees) {
    if (!isModifiable() || pLignesSelectionnees == null) {
      return false;
    }
    // On controle les lignes sélectionnées
    Regroupement regroupement = null;
    for (int indice : pLignesSelectionnees) {
      if (indice < getListeLigneVente().size()) {
        LigneVente ligneVente = getListeLigneVente().get(indice);
        // Si une ligne sélectionnée n'appartient pas a un groupe, alors les lignes ne sont pas regroupées
        if (!ligneVente.isLigneRegroupee()) {
          return false;
        }
        // Il s'agit de la première ligne sélectionnée donc onstocke simplement son regroupement
        if (regroupement == null) {
          regroupement = getListeRegroupements().get(ligneVente.getCodeRegroupement());
        }
        // Controle que la ligne appartienne au même regroupement que les autres
        else {
          if (!Constantes.equals(regroupement, getListeRegroupements().get(ligneVente.getCodeRegroupement()))) {
            return false;
          }
        }
      }
    }
    return true;
  }
  
  /**
   * Retourne si les lignes du document sélectionnées sont regroupables.
   */
  public boolean isLignesVenteSontRegroupables(int[] pLignesSelectionnees) {
    if (!isModifiable() || (pLignesSelectionnees == null) || (pLignesSelectionnees.length < 2)) {
      return false;
    }
    // On controle les lignes sélectionnées
    int cpt = 0;
    for (int indice : pLignesSelectionnees) {
      if (indice < getListeLigneVente().size()) {
        LigneVente ligneVente = getListeLigneVente().get(indice);
        // Si une ligne sélectionnée appartient déjà a un groupe, on signifie que les lignes ne sont pas regroupables
        if (ligneVente.isLigneRegroupee()) {
          return false;
        }
        // Comptage des lignes non régroupées et non commentaires
        if (!ligneVente.isLigneRegroupee() && !ligneVente.isLigneCommentaire()) {
          cpt++;
        }
      }
    }
    // Si au moins 2 lignes "normales" on peut regrouper
    if (cpt >= 2) {
      return true;
    }
    return false;
  }
  
  /**
   * Contrôle si le document contient au moins un article spécial.
   */
  public boolean isContientArticleSpecial() {
    if (listeLigneVente != null && !listeLigneVente.isEmpty()) {
      return listeLigneVente.isContientArticleSpecial();
    }
    return false;
  }
  
  /**
   * Contrôle si le document contient au moins un article "physique" (standard, spécial, palette).
   */
  public boolean isContientArticle() {
    if (listeLigneVente != null && !listeLigneVente.isEmpty()) {
      return listeLigneVente.isContientArticle();
    }
    return false;
  }
  
  /**
   * Contrôler si le franco de port n'est pas atteint dans un document de livraison.
   * 
   * @param pMontantFrancoPort Montant du franco de port, récupéré sur le client.
   * @return true=franco de port non atteint, false=franco de port atteint ou contrôle pas nécessaire.
   */
  public boolean isFrancoPortNonAtteint(BigDecimal pMontantFrancoPort) {
    // Ne pas tester si le montant du franco de port est null ou égal à zéro
    if (pMontantFrancoPort == null || pMontantFrancoPort.equals(BigDecimal.ZERO)) {
      return false;
    }
    
    // Ne pas tester si le document n'est pas une livraison
    if (!isLivraison()) {
      return false;
    }
    
    // Tester si le montant HT du document est strictement inférieur au franco de port
    return getTotalHT().compareTo(pMontantFrancoPort) < 0;
  }
  
  /**
   * Indiquer s'il faut afficher une alerte pour un franco de port non atteint.
   *
   * L'alerte n'est pas affichée si :
   * - le total HT du document est supérieur ou égal au franco de port (franco de port atteint),
   * - le paramètre définissant l'écart en dessous duquel il faut afficher l'alerte n'est pas définit, est nul ou invalide,
   * - l'écart entre le franco de port et le total HT du document est supérieur au seuil définit par le paramètre.
   * 
   * L'alerte est affichée si le frnaco de port n'est pas atteint et que l'écrat est inférieur au paramètre
   * "documentvente.ecartfrancoport" des paramètres avancés (PSEMPAVM).
   *
   * @param pIdSession Identifiant de la session.
   * @param pMontantFrancoPort Montant du franco de port, récupéré sur le client.
   * @return true=afficher l'alerte, false=ne pas afficher l'alerte.
   */
  public boolean isAlerteFrancoPortNonAtteint(IdSession pIdSession, BigDecimal pMontantFrancoPort) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    
    // Ne pas afficher d'alerte si le franco de port a été atteint
    if (!isFrancoPortNonAtteint(pMontantFrancoPort)) {
      return false;
    }
    
    // Récupérer le paramètre définissant la valeur de l'écart provquant l'affichage de l'alerte
    String parametreAffichageAlerte =
        ManagerSessionClient.getInstance().getValeurParametreAvance(pIdSession, EnumCleParametreAvance.ECART_FRANCO_PORT);
    if (parametreAffichageAlerte == null) {
      return false;
    }
    
    // Convertir le paramètre en décimal
    BigDecimal ecartAffichageAlerte = BigDecimal.ZERO;
    try {
      ecartAffichageAlerte = Constantes.convertirTexteEnBigDecimal(parametreAffichageAlerte);
    }
    catch (Exception e) {
      // Ne rien faire si le paramètre est invalide, on considère qu'il n'est pas définit
    }
    if (ecartAffichageAlerte == null || ecartAffichageAlerte == BigDecimal.ZERO) {
      return false;
    }
    
    // Calculer l'écart entre le montant du franco de port et le total HT du document
    BigDecimal ecartDocument = pMontantFrancoPort.subtract(getTotalHT());
    
    // Ne pas afficher d'arlerte si l'écart est supérieur à l'écart défini par le paramètre
    if (ecartDocument.compareTo(ecartAffichageAlerte) > 0) {
      return false;
    }
    
    // Afficher la boîte de dialogue
    return true;
  }
  
  // -- Accesseurs
  
  /**
   * Date du dernier traitement du devis.
   * Cette information n'a de sens que pour un document de ventes de type devis.
   */
  public Date getDateDernierTraitementDevis() {
    return dateDernierTraitementDevis;
  }
  
  /**
   * Modifier la date du dernier traitement du devis.
   * Cette information n'a de sens que pour un document de ventes de type devis.
   */
  public void setDateDernierTraitementDevis(Date pDateDernierTraitementDevis) {
    dateDernierTraitementDevis = pDateDernierTraitementDevis;
  }
  
  /**
   * Date du document.
   * 
   * La date du document contient une date clé par rapport au type du document :
   * - Pour un devis, c'est la date d'établissement du devis.
   * - Pour une commande, c'est la date de validation de la commande, non modifiable lorsque la commande a été validée
   * (E1HOM).
   * - Pour un bon, c'est la date d'expédition ou d'enlèvement, non modifiable une fois que le bon a été livré (E1EXP).
   * - Pour une facture, c'est la date de la facture, non modifiable une fois que la facture a été générée (E1FAC).
   * 
   * Cette date est donc renseignée à un moment clé de la vbie du document de ventes. Elle est renseignée avec la date
   * de traitement
   * (la date du jour ou une date calculée si la date de l'exercice en cours est dépassée, date exercice + 2 mois).
   */
  public Date getDateDocument() {
    switch (getTypeDocumentVente()) {
      case DEVIS:
        return getDateDernierTraitementDevis();
      
      case COMMANDE:
        return getDateValidationCommande();
      
      case BON:
        return getDateExpeditionBon();
      
      case FACTURE:
        return getDateFacturation();
      
      case NON_DEFINI:
        return null;
    }
    
    return null;
  }
  
  public BlocNote getPetitBlocNote() {
    return petitBlocNote;
  }
  
  public void setPetitBlocNote(BlocNote pBlocNote) {
    this.petitBlocNote = pBlocNote;
  }
  
  public Date getDateRelanceClient() {
    return dateRelanceClient;
  }
  
  public void setDateRelanceClient(Date dateRelanceClient) {
    this.dateRelanceClient = dateRelanceClient;
  }
  
  public Adresse getAdresseFacturation() {
    return adresseFacturation;
  }
  
  public void setAdresseFacturation(Adresse adresseFacturation) {
    this.adresseFacturation = adresseFacturation;
  }
  
  public String getIdContact() {
    return idContact;
  }
  
  public void setIdContact(String idContact) {
    this.idContact = idContact;
  }
  
  public ListeLigneVente getListeLigneVente() {
    return listeLigneVente;
  }
  
  public void setListeLignesArticles(ListeLigneVente pListeLigneVente) {
    this.listeLigneVente = pListeLigneVente;
  }
  
  public int getNombreLigneNonHonorables() {
    return nombreLigneNonHonorables;
  }
  
  public void setNombreLigneNonHonorables(int nombreLigneNonHonorables) {
    this.nombreLigneNonHonorables = nombreLigneNonHonorables;
  }
  
  public boolean isValide() {
    return valide;
  }
  
  public void setValide(boolean valide) {
    this.valide = valide;
  }
  
  public ListeLigneVente getListeLigneVenteSupprimee() {
    return listeLigneVenteSupprimee;
  }
  
  public void setListeLignesArticlesSupprimees(ListeLigneVente pListeLigneVenteSupprimee) {
    this.listeLigneVenteSupprimee = pListeLigneVenteSupprimee;
  }
  
  public String getCheminPDF() {
    return cheminPDF;
  }
  
  public void setCheminPDF(String cheminPDF) {
    this.cheminPDF = cheminPDF;
  }
  
  /**
   * Identifiant du document d'achats lié.
   */
  public IdDocumentAchat getIdDocumentAchatLie() {
    return idDocumentAchatLie;
  }
  
  /**
   * Modifier identifiant du document d'achats lié.
   */
  public void setIdDocumentAchatLie(IdDocumentAchat pIdDocumentAchatLie) {
    idDocumentAchatLie = pIdDocumentAchatLie;
  }
  
  public BigDecimal getTotalTTCOrigine() {
    return totalTTCOrigine;
  }
  
  public void setTotalTTCOrigine(BigDecimal totalTTCOrigine) {
    this.totalTTCOrigine = totalTTCOrigine;
  }
  
  public EnumEtatBonDocumentVente getEtatOrigine() {
    return etatOrigine;
  }
  
  public void setEtatOrigine(EnumEtatBonDocumentVente etatOrigine) {
    this.etatOrigine = etatOrigine;
  }
  
  public BigDecimal getTotalTTCPrecedent() {
    return totalTTCPrecedent;
  }
  
  public void setTotalTTCPrecedent(BigDecimal totalTTCPrecedent) {
    this.totalTTCPrecedent = totalTTCPrecedent;
  }
  
  /**
   * Retourne l'id du document d'origine dans le cas d'une extraction.
   * Attention le contenu de cette variable n'est pas persisté en base, à utiliser avec prudence.
   */
  public IdDocumentVente getIdOrigine() {
    return idDocumentVenteOrigine;
  }
  
  /**
   * Initialise l'id du document d'origine dans le cas d'une extraction.
   * Attention le contenu de cette variable n'est pas persisté en base, à utiliser avec prudence.
   */
  public void setIdOrigine(IdDocumentVente idOrigine) {
    idDocumentVenteOrigine = idOrigine;
  }
  
  public Map<Character, Regroupement> getListeRegroupements() {
    return listeRegroupements;
  }
  
  public void setListeRegroupements(Map<Character, Regroupement> listeRegroupements) {
    this.listeRegroupements = listeRegroupements;
  }
  
  public Boolean isDocumentCharge() {
    return documentCharge;
  }
  
  public void setDocumentCharge(Boolean documentCharge) {
    this.documentCharge = documentCharge;
  }
  
  /**
   * Nom du profil ayant créé le document de ventes.
   */
  public String getProfilCreation() {
    return profilCreation;
  }
  
  /**
   * Modifier le profil ayant créé le document de ventes.
   */
  public void setProfilCreation(String pProfilCreation) {
    profilCreation = pProfilCreation;
  }
  
  public int getNombreLignes() {
    return nombreLignes;
  }
  
  public void setNombreLignes(int nombreLignes) {
    this.nombreLignes = nombreLignes;
  }
  
  public int getTopEdition() {
    return topEdition;
  }
  
  public void setTopEdition(int topEdition) {
    this.topEdition = topEdition;
  }
  
  public int getTopClientLivre() {
    return topClientLivre;
  }
  
  public void setTopClientLivre(int topClientLivre) {
    this.topClientLivre = topClientLivre;
  }
  
  public int getTopClientFacture() {
    return topClientFacture;
  }
  
  public void setTopClientFacture(int topClientFacture) {
    this.topClientFacture = topClientFacture;
  }
  
  public int getNumeroTVA1() {
    return numeroTVA1;
  }
  
  public void setNumeroTVA1(int numeroTVA1) {
    this.numeroTVA1 = numeroTVA1;
  }
  
  public int getNumeroTVA2() {
    return numeroTVA2;
  }
  
  public void setNumeroTVA2(int numeroTVA2) {
    this.numeroTVA2 = numeroTVA2;
  }
  
  public int getNumeroTVA3() {
    return numeroTVA3;
  }
  
  public void setNumeroTVA3(int numeroTVA3) {
    this.numeroTVA3 = numeroTVA3;
  }
  
  public int getTopTaxeParafiscale() {
    return topTaxeParafiscale;
  }
  
  public void setTopTaxeParafiscale(int topTaxeParafiscale) {
    this.topTaxeParafiscale = topTaxeParafiscale;
  }
  
  public int getTopReleve() {
    return topReleve;
  }
  
  public void setTopReleve(int topReleve) {
    this.topReleve = topReleve;
  }
  
  public int getNombreExemplairesFactures() {
    return nombreExemplairesFactures;
  }
  
  public void setNombreExemplairesFactures(int nombreExemplairesFactures) {
    this.nombreExemplairesFactures = nombreExemplairesFactures;
  }
  
  public int getNumeroTarif() {
    return numeroTarif;
  }
  
  public void setNumeroTarif(int numeroTarif) {
    this.numeroTarif = numeroTarif;
  }
  
  public int getTopRegroupementBonFacture() {
    return topRegroupementBonFacture;
  }
  
  public void setTopRegroupementBonFacture(int topRegroupementBonFacture) {
    this.topRegroupementBonFacture = topRegroupementBonFacture;
  }
  
  public int getSigne() {
    return signe;
  }
  
  public void setSigne(int signe) {
    this.signe = signe;
  }
  
  public BigDecimal getMontantSoumisTVA1() {
    return montantSoumisTVA1;
  }
  
  public void setMontantSoumisTVA1(BigDecimal montantSoumisTVA1) {
    this.montantSoumisTVA1 = montantSoumisTVA1;
  }
  
  public BigDecimal getMontantSoumisTVA2() {
    return montantSoumisTVA2;
  }
  
  public void setMontantSoumisTVA2(BigDecimal montantSoumisTVA2) {
    this.montantSoumisTVA2 = montantSoumisTVA2;
  }
  
  public BigDecimal getMontantSoumisTVA3() {
    return montantSoumisTVA3;
  }
  
  public void setMontantSoumisTVA3(BigDecimal montantSoumisTVA3) {
    this.montantSoumisTVA3 = montantSoumisTVA3;
  }
  
  public BigDecimal getMontantTVA1() {
    return montantTVA1;
  }
  
  public void setMontantTVA1(BigDecimal montantTVA1) {
    this.montantTVA1 = montantTVA1;
    calculerTotalTVA();
  }
  
  public BigDecimal getMontantTVA2() {
    return montantTVA2;
  }
  
  public void setMontantTVA2(BigDecimal montantTVA2) {
    this.montantTVA2 = montantTVA2;
    calculerTotalTVA();
  }
  
  public BigDecimal getMontantTVA3() {
    return montantTVA3;
  }
  
  public void setMontantTVA3(BigDecimal montantTVA3) {
    this.montantTVA3 = montantTVA3;
    calculerTotalTVA();
  }
  
  public BigDecimal getMontantSoumisTaxeParafiscale() {
    return montantSoumisTaxeParafiscale;
  }
  
  public void setMontantSoumisTaxeParafiscale(BigDecimal montantSoumisTaxeParafiscale) {
    this.montantSoumisTaxeParafiscale = montantSoumisTaxeParafiscale;
  }
  
  public BigDecimal getMontantTaxeParafiscale() {
    return montantTaxeParafiscale;
  }
  
  public void setMontantTaxeParafiscale(BigDecimal montantTaxeParafiscale) {
    this.montantTaxeParafiscale = montantTaxeParafiscale;
  }
  
  public BigDecimal getMontantEscompte() {
    return montantEscompte;
  }
  
  public void setMontantEscompte(BigDecimal montantEscompte) {
    this.montantEscompte = montantEscompte;
  }
  
  public BigDecimal getTotalHTPrixRevient() {
    return totalHTPrixRevient;
  }
  
  public void setTotalHTPrixRevient(BigDecimal totalHTPrixRevient) {
    this.totalHTPrixRevient = totalHTPrixRevient;
  }
  
  /**
   * Total HT des prix de vente de base
   */
  public BigDecimal getTotalHTPrixVenteBase() {
    return totalHTPrixVenteBase;
  }
  
  /**
   * Modifier le total HT des prix de vente de base.
   */
  public void setTotalHTPrixVenteBase(BigDecimal totalHTPrixVenteBase) {
    this.totalHTPrixVenteBase = totalHTPrixVenteBase;
  }
  
  /**
   * Indique si le document est un avoir.
   */
  public boolean isAvoir() {
    if (totalHTPrixVenteBase == null) {
      return false;
    }
    return totalHTPrixVenteBase.compareTo(BigDecimal.ZERO) < 0;
  }
  
  public int getTopsTraitementSysteme() {
    return topsTraitementSysteme;
  }
  
  public void setTopsTraitementSysteme(int topsTraitementSysteme) {
    this.topsTraitementSysteme = topsTraitementSysteme;
  }
  
  public BigDecimal getBaseEscompte1() {
    return baseEscompte1;
  }
  
  public void setBaseEscompte1(BigDecimal baseEscompte1) {
    this.baseEscompte1 = baseEscompte1;
  }
  
  public BigDecimal getBaseEscompte2() {
    return baseEscompte2;
  }
  
  public void setBaseEscompte2(BigDecimal baseEscompte2) {
    this.baseEscompte2 = baseEscompte2;
  }
  
  public BigDecimal getBaseEscompte3() {
    return baseEscompte3;
  }
  
  public void setBaseEscompte3(BigDecimal baseEscompte3) {
    this.baseEscompte3 = baseEscompte3;
  }
  
  public Date getDateAffectation() {
    return dateAffectation;
  }
  
  public void setDateAffectation(Date dateAffectation) {
    this.dateAffectation = dateAffectation;
  }
  
  public int getArrondiTTCEnCentimes() {
    return arrondiTTCEnCentimes;
  }
  
  public void setArrondiTTCEnCentimes(int arrondiTTCEnCentimes) {
    this.arrondiTTCEnCentimes = arrondiTTCEnCentimes;
  }
  
  public int getTopEditionDevis() {
    return topEditionDevis;
  }
  
  public void setTopEditionDevis(int topEditionDevis) {
    this.topEditionDevis = topEditionDevis;
  }
  
  public BigDecimal getMontantCommandeInitiale() {
    return montantCommandeInitiale;
  }
  
  public void setMontantCommandeInitiale(BigDecimal montantCommandeInitiale) {
    this.montantCommandeInitiale = montantCommandeInitiale;
  }
  
  public int getNumeroFacture() {
    return numeroFacture;
  }
  
  public void setNumeroFacture(int pNumeroFacture) {
    this.numeroFacture = pNumeroFacture;
  }
  
  public int getNumeroCompteAuxiliaireClient() {
    return numeroCompteAuxiliaireClient;
  }
  
  public void setNumeroCompteAuxiliaireClient(int numeroCompteAuxiliaireClient) {
    this.numeroCompteAuxiliaireClient = numeroCompteAuxiliaireClient;
  }
  
  public int getTypeGenerationCommandeAchat() {
    return typeGenerationCommandeAchat;
  }
  
  public void setTypeGenerationCommandeAchat(int typeGenerationCommandeAchat) {
    this.typeGenerationCommandeAchat = typeGenerationCommandeAchat;
  }
  
  public Date getDateCommande() {
    return dateCommande;
  }
  
  public void setDateCommande(Date dateCommande) {
    this.dateCommande = dateCommande;
  }
  
  public IdClient getIdClientCentrale() {
    return idClientCentrale;
  }
  
  public void setIdClientCentrale(IdClient pIdClientCentrale) {
    idClientCentrale = pIdClientCentrale;
  }
  
  public BigDecimal getPourcentageEscompte() {
    return pourcentageEscompte;
  }
  
  public void setPourcentageEscompte(BigDecimal pourcentageEscompte) {
    this.pourcentageEscompte = pourcentageEscompte;
  }
  
  public BigDecimal getTauxTVA1() {
    return tauxTVA1;
  }
  
  public void setTauxTVA1(BigDecimal tauxTVA1) {
    this.tauxTVA1 = tauxTVA1;
  }
  
  public BigDecimal getTauxTVA2() {
    return tauxTVA2;
  }
  
  public void setTauxTVA2(BigDecimal tauxTVA2) {
    this.tauxTVA2 = tauxTVA2;
  }
  
  public BigDecimal getTauxTVA3() {
    return tauxTVA3;
  }
  
  public void setTauxTVA3(BigDecimal tauxTVA3) {
    this.tauxTVA3 = tauxTVA3;
  }
  
  public BigDecimal getTauxTaxeParafiscale() {
    return tauxTaxeParafiscale;
  }
  
  public void setTauxTaxeParafiscale(BigDecimal tauxTaxeParafiscale) {
    this.tauxTaxeParafiscale = tauxTaxeParafiscale;
  }
  
  public BigDecimal getPourcentageCommissionnementRep1() {
    return pourcentageCommissionnementRep1;
  }
  
  public void setPourcentageCommissionnementRep1(BigDecimal pourcentageCommissionnementRep1) {
    this.pourcentageCommissionnementRep1 = pourcentageCommissionnementRep1;
  }
  
  public BigDecimal getPourcentageCommissionnementRep2() {
    return pourcentageCommissionnementRep2;
  }
  
  public void setPourcentageCommissionnementRep2(BigDecimal pourcentageCommissionnementRep2) {
    this.pourcentageCommissionnementRep2 = pourcentageCommissionnementRep2;
  }
  
  public IdClient getIdClientLivre() {
    return idClientLivre;
  }
  
  public void setIdClientLivre(IdClient idClientLivre) {
    this.idClientLivre = idClientLivre;
  }
  
  public IdTypeFacturation getIdTypeFacturation() {
    return idTypeFacturation;
  }
  
  public void setIdTypeFacturation(IdTypeFacturation pIdTypeFacturation) {
    this.idTypeFacturation = pIdTypeFacturation;
  }
  
  public int getDelaiPreparation() {
    return delaiPreparation;
  }
  
  public void setDelaiPreparation(int delaiPreparation) {
    this.delaiPreparation = delaiPreparation;
  }
  
  public int getNombreColis() {
    return nombreColis;
  }
  
  public void setNombreColis(int nombreColis) {
    this.nombreColis = nombreColis;
  }
  
  /**
   * Poids total du documents de ventes en kilogrammes.
   */
  public BigDecimal getPoidsTotalKg() {
    return poidsTotalKg;
  }
  
  /**
   * Modifier le poids total du documents de ventes en kilogrammes.
   */
  public void setPoidsKg(BigDecimal pPoidsTotalKg) {
    if (pPoidsTotalKg != null) {
      poidsTotalKg = pPoidsTotalKg.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
    else {
      poidsTotalKg = null;
    }
  }
  
  public int getBaseDevise() {
    return baseDevise;
  }
  
  public void setBaseDevise(int baseDevise) {
    this.baseDevise = baseDevise;
  }
  
  public BigDecimal getTauxChange() {
    return tauxChange;
  }
  
  public void setTauxChange(BigDecimal tauxChange) {
    this.tauxChange = tauxChange;
  }
  
  public BigDecimal getReductionBaseCommissionRep1() {
    return reductionBaseCommissionRep1;
  }
  
  public void setReductionBaseCommissionRep1(BigDecimal reductionBaseCommissionRep1) {
    this.reductionBaseCommissionRep1 = reductionBaseCommissionRep1;
  }
  
  public BigDecimal getReductionBaseCommissionRep2() {
    return reductionBaseCommissionRep2;
  }
  
  public void setReductionBaseCommissionRep2(BigDecimal reductionBaseCommissionRep2) {
    this.reductionBaseCommissionRep2 = reductionBaseCommissionRep2;
  }
  
  public Date getDateMiniFacturation() {
    return dateMiniFacturation;
  }
  
  public void setDateMiniFacturation(Date dateMiniFacturation) {
    this.dateMiniFacturation = dateMiniFacturation;
  }
  
  public BigDecimal getPourcentageRemise1Ligne() {
    return pourcentageRemise1Ligne;
  }
  
  public void setPourcentageRemise1Ligne(BigDecimal pourcentageRemise1Ligne) {
    this.pourcentageRemise1Ligne = pourcentageRemise1Ligne;
  }
  
  public BigDecimal getPourcentageRemise2Ligne() {
    return pourcentageRemise2Ligne;
  }
  
  public void setPourcentageRemise2Ligne(BigDecimal pourcentageRemise2Ligne) {
    this.pourcentageRemise2Ligne = pourcentageRemise2Ligne;
  }
  
  public BigDecimal getPourcentageRemise3Ligne() {
    return pourcentageRemise3Ligne;
  }
  
  public void setPourcentageRemise3Ligne(BigDecimal pourcentageRemise3Ligne) {
    this.pourcentageRemise3Ligne = pourcentageRemise3Ligne;
  }
  
  public BigDecimal getPourcentageRemise4Ligne() {
    return pourcentageRemise4Ligne;
  }
  
  public void setPourcentageRemise4Ligne(BigDecimal pourcentageRemise4Ligne) {
    this.pourcentageRemise4Ligne = pourcentageRemise4Ligne;
  }
  
  public BigDecimal getPourcentageRemise5Ligne() {
    return pourcentageRemise5Ligne;
  }
  
  public void setPourcentageRemise5Ligne(BigDecimal pourcentageRemise5Ligne) {
    this.pourcentageRemise5Ligne = pourcentageRemise5Ligne;
  }
  
  public BigDecimal getPourcentageRemise6Ligne() {
    return pourcentageRemise6Ligne;
  }
  
  public void setPourcentageRemise6Ligne(BigDecimal pourcentageRemise6Ligne) {
    this.pourcentageRemise6Ligne = pourcentageRemise6Ligne;
  }
  
  public BigDecimal getPourcentageRemise1Pied() {
    return pourcentageRemise1Pied;
  }
  
  public BigDecimal getPourcentageRemise1Pied(Integer pNombreDecimales) {
    if (pourcentageRemise1Pied != null && pNombreDecimales != null) {
      return pourcentageRemise1Pied = pourcentageRemise1Pied.setScale(pNombreDecimales, RoundingMode.HALF_UP);
    }
    return pourcentageRemise1Pied;
  }
  
  public void setPourcentageRemise1Pied(BigDecimal pourcentageRemise1Pied) {
    this.pourcentageRemise1Pied = pourcentageRemise1Pied;
  }
  
  public BigDecimal getPourcentageRemise2Pied() {
    return pourcentageRemise2Pied;
  }
  
  public void setPourcentageRemise2Pied(BigDecimal pourcentageRemise2Pied) {
    this.pourcentageRemise2Pied = pourcentageRemise2Pied;
  }
  
  public BigDecimal getPourcentageRemise3Pied() {
    return pourcentageRemise3Pied;
  }
  
  public void setPourcentageRemise3Pied(BigDecimal pourcentageRemise3Pied) {
    this.pourcentageRemise3Pied = pourcentageRemise3Pied;
  }
  
  public BigDecimal getPourcentageRemise4Pied() {
    return pourcentageRemise4Pied;
  }
  
  public void setPourcentageRemise4Pied(BigDecimal pourcentageRemise4Pied) {
    this.pourcentageRemise4Pied = pourcentageRemise4Pied;
  }
  
  public BigDecimal getPourcentageRemise5Pied() {
    return pourcentageRemise5Pied;
  }
  
  public void setPourcentageRemise5Pied(BigDecimal pourcentageRemise5Pied) {
    this.pourcentageRemise5Pied = pourcentageRemise5Pied;
  }
  
  public BigDecimal getPourcentageRemise6Pied() {
    return pourcentageRemise6Pied;
  }
  
  public void setPourcentageRemise6Pied(BigDecimal pourcentageRemise6Pied) {
    this.pourcentageRemise6Pied = pourcentageRemise6Pied;
  }
  
  public int getNumeroTournee() {
    return numeroTournee;
  }
  
  public void setNumeroTournee(int numeroTournee) {
    this.numeroTournee = numeroTournee;
  }
  
  public int getRangDansTournee() {
    return rangDansTournee;
  }
  
  public void setRangDansTournee(int rangDansTournee) {
    this.rangDansTournee = rangDansTournee;
  }
  
  public BigDecimal getVolume() {
    return volume;
  }
  
  public void setVolume(BigDecimal pVolume) {
    volume = pVolume;
    if (volume != null) {
      volume = volume.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
  }
  
  public BigDecimal getLongueurMax() {
    return longueurMax;
  }
  
  public void setLongueurMax(BigDecimal longueurMax) {
    this.longueurMax = longueurMax;
  }
  
  public BigDecimal getSurfacePlancher() {
    return surfacePlancher;
  }
  
  public void setSurfacePlancher(BigDecimal surfacePlancher) {
    this.surfacePlancher = surfacePlancher;
  }
  
  public ListeReglement getListeReglement() {
    return listeReglement;
  }
  
  public void setListeReglement(ListeReglement listeReglement) {
    this.listeReglement = listeReglement;
  }
  
  public IdRepresentant getIdRepresentant1() {
    return idRepresentant1;
  }
  
  public void setIdRepresentant1(IdRepresentant pIdRepresentant) {
    this.idRepresentant1 = pIdRepresentant;
  }
  
  public IdRepresentant getIdRepresentant2() {
    return idRepresentant2;
  }
  
  public void setIdRepresentant2(IdRepresentant pIdRepresentant) {
    this.idRepresentant2 = pIdRepresentant;
  }
  
  public IdMagasin getIdMagasinSortieAncien() {
    return idMagasinSortieAncien;
  }
  
  public void setIdMagasinSortieAncien(IdMagasin pIdMagasinSortieAncien) {
    this.idMagasinSortieAncien = pIdMagasinSortieAncien;
  }
  
  public Transport getTransport() {
    if (transport == null) {
      setTransport(new Transport());
    }
    return transport;
  }
  
  public void setTransport(Transport transport) {
    this.transport = transport;
  }
  
  public String getSectionAnalytique() {
    return sectionAnalytique;
  }
  
  public void setSectionAnalytique(String sectionAnalytique) {
    this.sectionAnalytique = sectionAnalytique;
  }
  
  public String getCodeActionCommerciale() {
    return codeActionCommerciale;
  }
  
  public void setCodeActionCommerciale(String codeActionCommerciale) {
    this.codeActionCommerciale = codeActionCommerciale;
  }
  
  public String getCodeDevise() {
    return codeDevise;
  }
  
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  public String getCodeConditionVente() {
    return codeConditionVente;
  }
  
  public void setCodeConditionVente(String codeConditionVente) {
    this.codeConditionVente = codeConditionVente;
  }
  
  /**
   * Référence courte du document de ventes.
   * La référence longue est une zone de saisie libre pour l'utilisateur. Le logiciel l'initialise parfois avec les
   * références
   * d'autres documents.
   */
  public String getReferenceCourte() {
    return referenceCourte;
  }
  
  /**
   * Modifier la référence courte du document de ventes.
   * La valeur est tronquée à 8 caractères et les espaces au début et à la fin sont enlevés.
   */
  public void setReferenceCourte(String pReferenceCourte) {
    if (pReferenceCourte != null) {
      referenceCourte = Constantes.formaterStringMax(pReferenceCourte, LONGUEUR_REFERENCE_COURTE, true);
    }
    else {
      referenceCourte = "";
    }
  }
  
  public String getTransportACalculer() {
    return transportACalculer;
  }
  
  public void setTransportACalculer(String transportACalculer) {
    this.transportACalculer = transportACalculer;
  }
  
  public String getTopEditionBonPreparation() {
    return topEditionBonPreparation;
  }
  
  public void setTopEditionBonPreparation(String topEditionBonPreparation) {
    this.topEditionBonPreparation = topEditionBonPreparation;
  }
  
  public String getTypeCommissionnementRep() {
    return typeCommissionnementRep;
  }
  
  public void setTypeCommissionnementRep(String typeCommissionnementRep) {
    this.typeCommissionnementRep = typeCommissionnementRep;
  }
  
  public String getTopBonNonConfirmable() {
    return topBonNonConfirmable;
  }
  
  public void setTopBonNonConfirmable(String topBonNonConfirmable) {
    this.topBonNonConfirmable = topBonNonConfirmable;
  }
  
  public String getTopPersonnalisable1() {
    return topPersonnalisable1;
  }
  
  public void setTopPersonnalisable1(String topPersonnalisable1) {
    this.topPersonnalisable1 = topPersonnalisable1;
  }
  
  public String getTopPersonnalisable2() {
    return topPersonnalisable2;
  }
  
  public void setTopPersonnalisable2(String topPersonnalisable2) {
    this.topPersonnalisable2 = topPersonnalisable2;
  }
  
  public String getTopPersonnalisable3() {
    return topPersonnalisable3;
  }
  
  public void setTopPersonnalisable3(String topPersonnalisable3) {
    this.topPersonnalisable3 = topPersonnalisable3;
  }
  
  public String getTopPersonnalisable4() {
    return topPersonnalisable4;
  }
  
  public void setTopPersonnalisable4(String topPersonnalisable4) {
    this.topPersonnalisable4 = topPersonnalisable4;
  }
  
  public String getTopPersonnalisable5() {
    return topPersonnalisable5;
  }
  
  public void setTopPersonnalisable5(String topPersonnalisable5) {
    this.topPersonnalisable5 = topPersonnalisable5;
  }
  
  public String getImmatriculationVehiculeClient() {
    return immatriculationVehiculeClient;
  }
  
  public void setImmatriculationVehiculeClient(String immatriculationVehiculeClient) {
    this.immatriculationVehiculeClient = immatriculationVehiculeClient;
  }
  
  public String getTopNePasFacturer() {
    return topNePasFacturer;
  }
  
  public void setTopNePasFacturer(String topNePasFacturer) {
    this.topNePasFacturer = topNePasFacturer;
  }
  
  public String getTopIndicateurTournee() {
    return topIndicateurTournee;
  }
  
  public void setTopIndicateurTournee(String topIndicateurTournee) {
    this.topIndicateurTournee = topIndicateurTournee;
  }
  
  public String getTopTraiteEditee() {
    return topTraiteEditee;
  }
  
  public void setTopTraiteEditee(String topTraiteEditee) {
    this.topTraiteEditee = topTraiteEditee;
  }
  
  public String getTopRemisesSpeciales() {
    return topRemisesSpeciales;
  }
  
  public void setTopRemisesSpeciales(String topRemisesSpeciales) {
    this.topRemisesSpeciales = topRemisesSpeciales;
  }
  
  public String getTopExistenceRemisePied() {
    return topExistenceRemisePied;
  }
  
  public void setTopExistenceRemisePied(String topExistenceRemisePied) {
    this.topExistenceRemisePied = topExistenceRemisePied;
  }
  
  public boolean isRemisePiedExiste() {
    return !getTopExistenceRemisePied().trim().equals("");
  }
  
  public void setRemisePiedExiste(boolean existenceRemisePied) {
    if (existenceRemisePied) {
      setTopExistenceRemisePied("1");
    }
    else {
      setTopExistenceRemisePied("");
    }
  }
  
  public String getTopFacturecomptoir() {
    return topFacturecomptoir;
  }
  
  public void setTopFacturecomptoir(String topFacturecomptoir) {
    this.topFacturecomptoir = topFacturecomptoir;
  }
  
  public String getCodePreparationCommande() {
    return codePreparationCommande;
  }
  
  public void setCodePreparationCommande(String codePreparationCommande) {
    this.codePreparationCommande = codePreparationCommande;
  }
  
  public Character getBlocageExpedition() {
    return blocageExpedition;
  }
  
  public void setBlocageExpedition(Character blocageExpedition) {
    this.blocageExpedition = blocageExpedition;
  }
  
  public String getCodeContrat() {
    return codeContrat;
  }
  
  public void setCodeContrat(String codeContrat) {
    this.codeContrat = codeContrat;
  }
  
  public String getTypeRemise() {
    return typeRemise;
  }
  
  public void setTypeRemise(String typeRemise) {
    this.typeRemise = typeRemise;
  }
  
  public String getBaseRemise() {
    return baseRemise;
  }
  
  public void setBaseRemise(String baseRemise) {
    this.baseRemise = baseRemise;
  }
  
  public String getTypeRemisePied() {
    return typeRemisePied;
  }
  
  public void setTypeRemisePied(String typeRemisePied) {
    this.typeRemisePied = typeRemisePied;
  }
  
  public String getTopModeMesure() {
    return topModeMesure;
  }
  
  public void setTopModeMesure(String topModeMesure) {
    this.topModeMesure = topModeMesure;
  }
  
  public String getTopExtractionForceeExpedition() {
    return topExtractionForceeExpedition;
  }
  
  public void setTopExtractionForceeExpedition(String topExtractionForceeExpedition) {
    this.topExtractionForceeExpedition = topExtractionForceeExpedition;
  }
  
  public String getCodeCanalVente() {
    return codeCanalVente;
  }
  
  public void setCodeCanalVente(String codeCanalVente) {
    this.codeCanalVente = codeCanalVente;
  }
  
  public String getCodePreparateur() {
    return codePreparateur;
  }
  
  public void setCodePreparateur(String codePreparateur) {
    this.codePreparateur = codePreparateur;
  }
  
  public String getCodePlateformeChargement() {
    return codePlateformeChargement;
  }
  
  public void setCodePlateformeChargement(String codePlateformeChargement) {
    this.codePlateformeChargement = codePlateformeChargement;
  }
  
  public String getTopTypeVente() {
    return topTypeVente;
  }
  
  public void setTopTypeVente(String topTypeVente) {
    this.topTypeVente = topTypeVente;
  }
  
  public BigDecimal getTotalTVA() {
    return totalTVA;
  }
  
  public void setTotalTVA(BigDecimal pTotalTVA) {
    totalTVA = pTotalTVA;
    if (totalTVA != null) {
      totalTVA = totalTVA.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
  }
  
  public IdAdresseFournisseur getIdAdresseFournisseur() {
    return idAdresseFournisseur;
  }
  
  public void setIdAdresseFournisseur(IdAdresseFournisseur pIdAdresseFournisseur) {
    idAdresseFournisseur = pIdAdresseFournisseur;
  }
  
  public String getPrisPar() {
    return prisPar;
  }
  
  public void setPrisPar(String prisPar) {
    this.prisPar = prisPar;
  }
  
  public boolean isTTC() {
    return ttc;
  }
  
  public void setTTC(boolean ttc) {
    this.ttc = ttc;
  }
  
  /**
   * Mémo des informations de livraison ou d'enlèvement.
   * Le texte du mémo peut être lu directement via la méthode getTexteLivraisonEnlevement() et modifié via la méthode
   * setTexteLivraisonEnlevement().
   */
  public Memo getMemoLivraisonEnlevement() {
    return memoLivraisonEnlevement;
  }
  
  /**
   * Modifier le mémo des informations de livraison ou d'enlèvement.
   * Le texte du mémo peut être lu directement via la méthode getTexteLivraisonEnlevement() et modifié via la méthode
   * setTexteLivraisonEnlevement().
   */
  public void setMemoLivraisonEnlevement(Memo pMemo) {
    memoLivraisonEnlevement = pMemo;
  }
  
  /**
   * Texte des informations de livraison ou d'enlèvement.
   * Le mémo correspondant peut être accédé via la méthode getMemoLivraisonEnlèvement().
   */
  public String getTexteLivraisonEnlevement() {
    if (memoLivraisonEnlevement == null) {
      return "";
    }
    return memoLivraisonEnlevement.getTexte();
  }
  
  /**
   * Modifier le texte des informations de livraison ou d'enlèvement.
   * Le mémo correspondant peut être accédé via la méthode getMemoLivraisonEnlèvement().
   */
  public void setTexteLivraisonEnlevement(String pTexte) {
    // Créer le mémo s'il n'existe pas
    if (memoLivraisonEnlevement == null) {
      IdMemo idMemo = IdMemo.getInstancePourDocumentVente(id, 1);
      memoLivraisonEnlevement = new Memo(idMemo);
      memoLivraisonEnlevement.setType(EnumTypeMemo.LIVRAISON);
    }
    
    // Modifier le texte
    if (pTexte != null) {
      memoLivraisonEnlevement.setTexte(pTexte);
    }
    else {
      // Modifier le texte
      memoLivraisonEnlevement.setTexte("");
    }
  }
  
  /**
   * Calculer le pourcentage de marge réalisé sur l'ensemble du document.
   * Si le document ne comporte aucune ligne, la valeur retournée est à zéro.
   * @return Le pourcentage de marge.
   */
  public BigDecimal getPourcentageMargeTotale() {
    if (getListeLigneVente().isEmpty()) {
      return BigDecimal.ZERO;
    }
    BigDecimal sommePRS = BigDecimal.ZERO;
    BigDecimal sommePrix = BigDecimal.ZERO;
    for (LigneVente ligneVente : getListeLigneVente()) {
      if (ligneVente.getQuantiteUV() != null && ligneVente.getPrixDeRevientLigneHT() != null && ligneVente.getPrixNet() != null) {
        sommePRS = sommePRS.add(ligneVente.getPrixDeRevientLigneHT().multiply(ligneVente.getQuantiteUV()));
        sommePrix = sommePrix.add(ligneVente.getPrixNet().multiply(ligneVente.getQuantiteUV()));
      }
    }
    
    return OutilCalculPrix.calculerMarge(sommePrix, sommePRS, OutilCalculPrix.NOMBRE_DECIMALE_POURCENTAGE);
  }
  
  /**
   * Construire le titre complet du document.
   * Le titre complet comprend le type (devis, commande, bon ou facture), le mode d'éxpédition (enlèvement ou
   * livraison), l'identifiant
   * du document, le status et l'information direct usine.
   * Exemple : Commande enlèvement 419221-1 (validé) - Direct usine
   */
  public String getTitre() {
    if (getTypeDocumentVente() == null) {
      return "Type de document indéfini";
    }
    
    String titre = getTypeDocumentVente().getLibelle();
    if (getTypeDocumentVente().equals(EnumTypeDocumentVente.NON_DEFINI)) {
      titre = "Facture";
    }
    if (isEnlevement()) {
      titre += " enlèvement";
    }
    else {
      titre += " livraison";
    }
    
    if (getTotalTTC().compareTo(BigDecimal.ZERO) < 0) {
      titre = "Avoir";
    }
    
    if (isFacture() || getTypeDocumentVente().equals(EnumTypeDocumentVente.NON_DEFINI)) {
      if (getId().getNumeroFacture() != null) {
        titre += " " + getId().getNumeroFacture();
      }
      else {
        titre += " " + getNumeroFacture();
      }
    }
    else {
      titre += " " + getId();
    }
    
    if (getLibelleEtat() != null && !getLibelleEtat().isEmpty()) {
      titre += " (" + getLibelleEtat() + ")";
    }
    
    if (isDirectUsine()) {
      titre += " - Direct usine";
    }
    return titre;
  }
  
}
