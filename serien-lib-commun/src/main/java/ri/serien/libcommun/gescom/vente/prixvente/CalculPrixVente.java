/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;

import ri.serien.libcommun.gescom.vente.ligne.EnumOriginePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.ParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.ListeParametreChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.ParametreChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.EnumRegleExclusionConditionVenteEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.ParametreClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.IdRattachementArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.InformationConditionVenteLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ListeParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.ParametreDocumentVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceCoefficient;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Cette classe regroupe tous les traitements nécessaires au calcul d'un prix de vente.
 * 
 * TODO
 * - Voir l'application des CNV cumulatives qui ne repecte pas la règle du RPG
 * - Voir la formule de prix PrixGaranti qu'il serait plus judicieux de traiter en données brutes issues de la base. Mais il faudra
 * modifier le chargement de la ligne afin de récupérer les prix TTC dans la table PGVMXLIM.
 * - Créer un composant afin d'afficher les données de la classe articleChantier dans l'onglet des CNV.
 */
public class CalculPrixVente implements Serializable, Cloneable {
  private ParametreEtablissement parametreEtablissement = null;
  private ParametreLigneVente parametreLigneVente = null;
  private ParametreDocumentVente parametreDocumentVente = null;
  private ParametreClient parametreClientLivre = null;
  private ParametreClient parametreClientFacture = null;
  private ParametreArticle parametreArticle = null;
  private ParametreTarif parametreTarif = null;
  private ListeParametreChantier parametreChantier = null;
  private ParametreConditionVente parametreConditionVentePrioritaire = null;
  private ListeParametreConditionVente listeParametreConditionVenteNormale = null;
  private ListeParametreConditionVente listeParametreConditionVenteQuantitative = null;
  private ListeParametreConditionVente listeParametreConditionVenteCumulative = null;
  private Date dateApplication = null;
  
  private List<FormulePrixVente> listeFormulePrixVente = new ArrayList<FormulePrixVente>();
  private FormulePrixVente formulePrixVenteHeritee = null;
  private FormulePrixVente formulePrixVenteFinale = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public CalculPrixVente() {
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Cloner l'objet.
   */
  @Override
  public CalculPrixVente clone() {
    Gson gson = new Gson();
    return gson.fromJson(gson.toJson(this), CalculPrixVente.class);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Calcul du prix de vente d'un article avec chargement des données.
   * 
   * @param pParametreChargement
   */
  public void calculer(ParametreChargement pParametreChargement) {
    // Contrôle les paramètres chargés
    if (pParametreChargement == null) {
      throw new MessageErreurException("Le paramètre de chargement est invalide.");
    }
    pParametreChargement.controlerParametreObligatoirePourCalcul();
    
    dateApplication = pParametreChargement.getDateApplication();
    parametreEtablissement = pParametreChargement.getParametreEtablissement();
    parametreLigneVente = pParametreChargement.getParametreLigneVente();
    parametreDocumentVente = pParametreChargement.getParametreDocumentVente();
    parametreClientLivre = pParametreChargement.getParametreClientLivre();
    parametreClientFacture = pParametreChargement.getParametreClientFacture();
    parametreArticle = pParametreChargement.getParametreArticle();
    parametreTarif = pParametreChargement.getParametreTarif();
    parametreChantier = pParametreChargement.getParametreChantier();
    parametreConditionVentePrioritaire = pParametreChargement.getParametreConditionVentePrioritaire();
    listeParametreConditionVenteNormale = pParametreChargement.getListeParametreConditionVenteNormale();
    listeParametreConditionVenteQuantitative = pParametreChargement.getListeParametreConditionVenteQuantitative();
    listeParametreConditionVenteCumulative = pParametreChargement.getListeParametreConditionVenteCumulative();
    
    // Calcule le prix de vente
    calculer();
  }
  
  /**
   * Calculer le prix de vente d'un article.
   * 
   * A ce stade, il est important de comprendre que le calcul d'un prix est :
   * - Soit un prix recalculé à partir des informations de la ligne de ventes car ce dernier est garanti et donc ne doit pas utiliser les
   * données d'autres sources. Il n'y a qu'un seul prix de calculé et il est nommé "prix garanti". Le prix final est le prix garanti
   * - Soit carrément un nouveau prix avec les données actuelles. Dans ce cas plusieurs prix sont calculés : le prix standard (sans
   * condition de ventes, dérogation, chantier, ...) et un prix pour chaque condition de ventes trouvées. Le prix final est le meilleur
   * prix de l'ensemble des prix calculés.
   * Par contre la classe utilisée pour calculer un prix (FormuleCalculPrix) est la même pour tous les cas cités.
   * 
   * @param pParametreChargement
   */
  public void calculer() {
    // Effacer le résultat du calcul précédent
    listeFormulePrixVente = new ArrayList<FormulePrixVente>();
    formulePrixVenteHeritee = null;
    formulePrixVenteFinale = null;
    
    // Calculer le prix standard
    calculerPrixStandard();
    
    // Calculer le prix consigné
    calculerPrixConsigne();
    
    // Calculer le prix chantier (si présent)
    calculerPrixChantier();
    
    // Calculer le prix de la condition de vente prioritaire (si présent)
    calculerPrixConditionVentePrioritaire();
    
    // Calculer le prix des conditions de ventes normales
    // Même dans le cas de la présence d'un prix chantier le prix des conditions de ventes est calculé pour pouvoir comparer
    calculerPrixConditionVenteNormale();
    
    // Calculer le prix des conditions de ventes quantitatives
    // Même dans le cas de la présence d'un prix chantier le prix des conditions de ventes est calculé pour pouvoir comparer
    calculerPrixConditionVenteQuantitative();
    
    // Déterminer le meilleur prix de vente (prix final) après l'application des conditions de ventes normales et quantitatives
    determinerPrixFinalAvantConditionCumulative();
    
    // Calculer le prix de vente en appliquant les conditions de ventes cumulatives (en ajout)
    calculerPrixFinalAvecConditionCumulative();
    
    // Déterminer le prix final après calcul des conditions de ventes cumulatives
    determinerPrixFinal();
  }
  
  /**
   * Initialiser la valeur de toutes les provenances avec 0.
   * Utile lors des négociations de prix.
   */
  public void mettreAZeroProvenance() {
    if (parametreLigneVente == null) {
      return;
    }
    parametreLigneVente.setProvenanceCoefficient(EnumProvenanceCoefficient.DEFAUT);
    parametreLigneVente.setProvenanceColonneTarif(EnumProvenanceColonneTarif.DEFAUT);
    parametreLigneVente.setProvenancePrixBase(EnumProvenancePrixBase.DEFAUT);
    parametreLigneVente.setProvenancePrixNet(EnumProvenancePrixNet.DEFAUT);
    parametreLigneVente.setProvenanceTauxRemise(EnumProvenanceTauxRemise.DEFAUT);
  }
  
  /**
   * Afficher le détail du calcul dans les traces.
   */
  public void tracerCalcul() {
    Gson gson = new Gson();
    String json = gson.toJson(this);
    Trace.info(json);
  }
  
  /**
   * Retourner la liste des conditions de ventes utilisées une ligne de ventes donnée.
   * @param pIdLigneVente L'identifiant de la ligne de ventes.
   */
  public List<InformationConditionVenteLigneVente> getListeConditionVenteLigneVente() {
    // Contrôle de la liste des formules
    if (listeFormulePrixVente == null || listeFormulePrixVente.isEmpty()) {
      return null;
    }
    
    // Récupération de la formule qui calcule le prix standard
    FormulePrixVente formulePrixVenteStandard = getFormulePrixVenteStandard();
    if (formulePrixVenteStandard == null) {
      return null;
    }
    // Récupération du prix net HT standard
    BigDecimal prixNetHTStandard = formulePrixVenteStandard.getPrixNetHT();
    if (prixNetHTStandard == null) {
      prixNetHTStandard = BigDecimal.ZERO;
    }
    
    // Création de la liste qui va contenir les conditions de ventes utilisées pour calculer le prix de vente de la ligne de ventes
    List<InformationConditionVenteLigneVente> listeConditionVenteLigneVente = new LinkedList<InformationConditionVenteLigneVente>();
    
    // Initialisation de l'ordre d'application
    int ordreApplication = 0;
    
    // Parcourt des formules
    for (FormulePrixVente formulePrixVente : listeFormulePrixVente) {
      // La formule a été ignorée dans le calcul du prix de vente
      if (!formulePrixVente.isPrisEnComptePrixVente()) {
        continue;
      }
      // La formule n'a pas utilisée un conditon de vente pour le calcul du prix
      if (formulePrixVente.getParametreConditionVente() == null) {
        continue;
      }
      
      // Création de l'objet
      InformationConditionVenteLigneVente informationConditionVenteLigneVente = new InformationConditionVenteLigneVente();
      
      // Récupération des parammètres de la condition de ventes
      ParametreConditionVente parametreConditionVente = formulePrixVente.getParametreConditionVente();
      informationConditionVenteLigneVente.initialiserAvecParametreConditionVente(parametreConditionVente);
      
      // L'ordre d'application
      ordreApplication++;
      informationConditionVenteLigneVente.setOrdreApplication(ordreApplication);
      
      // Différence du prix unitaire par rapport au prix standard
      BigDecimal prixNetHT = formulePrixVente.getPrixNetHT();
      if (prixNetHT == null) {
        prixNetHT = BigDecimal.ZERO;
      }
      informationConditionVenteLigneVente.setDifferencePrixUnitaire(prixNetHTStandard.subtract(prixNetHT));
      
      // La condition est-elle promotionnelle ?
      // TODO
      
      // Ajout à la liste
      if (!listeConditionVenteLigneVente.contains(informationConditionVenteLigneVente)) {
        listeConditionVenteLigneVente.add(informationConditionVenteLigneVente);
      }
    }
    
    // Contrôle de l'ordre d'application, s'il n'y a qu'un seul enregistrement alors l'ordre vaut 0 (en table il vaudra blanc)
    if (listeConditionVenteLigneVente.size() == 1) {
      listeConditionVenteLigneVente.get(0).setOrdreApplication(0);
    }
    
    return listeConditionVenteLigneVente;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Calculer le prix standard.
   * 
   * Le prix standard de l'article est calculé sans appliquer les éventuelles conditions de ventes ou éventuels chantiers. C'est le prix
   * par défaut de l'article pour ce client.
   */
  private void calculerPrixStandard() {
    // Ne pas calculer le prix standard pour les articles consignés
    if (isConsigne()) {
      return;
    }
    
    // Calculer le prix
    FormulePrixVente formulePrixVente =
        new FormulePrixVente(EnumTypeFormulePrixVente.STANDARD, parametreEtablissement, parametreLigneVente, parametreDocumentVente,
            parametreClientFacture, parametreClientLivre, parametreArticle, parametreTarif, null, null);
    formulePrixVente.calculer();
    
    // Ajouter la formule prix de vente à la liste
    listeFormulePrixVente.add(formulePrixVente);
  }
  
  /**
   * Calcule le prix consigné.
   * 
   * Un article consigné ne suit pas la logique tarifaire habituelle. Le tarif sert uniquement à définir 2 prix : le prix consigné qui
   * est dans la colonne 1 et le prix déconsigné qui est dans la colonne 2. Ce sont les prix de base de l'article. La notion de
   * colonnes de tarifs n'est donc pas utilisé ici. Par ailleurs, le chantier et les conditions de ventes ne sont pas appliquées.
   * 
   * Suivant le signe de la quantité, la formule de calcul alimente le prix de base du résultat :
   * - à partir du prix consigné si la quantité est positive (on vend l'article consigné),
   * - à partir du prix déconsigné si la quantité est négative (on reprend l'article consigné).
   * Les prix de base, net et montant s'adaptent donc au contexte.
   */
  private void calculerPrixConsigne() {
    // Ne pas calculer le prix consigné pour les articles qui ne sont pas consignés (et oui)
    if (!isConsigne()) {
      return;
    }
    
    // Calculer le prix
    FormulePrixVente formulePrixVente = new FormulePrixVente(EnumTypeFormulePrixVente.CONSIGNE, parametreEtablissement,
        parametreLigneVente, parametreDocumentVente, null, null, parametreArticle, parametreTarif, null, null);
    formulePrixVente.calculer();
    
    // Ajouter la formule prix de vente à la liste
    listeFormulePrixVente.add(formulePrixVente);
  }
  
  /**
   * Calcule le prix chantier.
   */
  private void calculerPrixChantier() {
    // Tester si un chantier existe
    if (parametreChantier == null || parametreArticle == null) {
      return;
    }
    
    // Ne pas calculer le prix chantier pour les articles consignés
    if (isConsigne()) {
      return;
    }
    
    // Récupérer la quantité
    BigDecimal quantiteUV = null;
    if (parametreLigneVente != null) {
      quantiteUV = parametreLigneVente.getQuantiteUV();
    }
    
    // Récupérer l'article référencé dans le chantier
    // Si l'article n'est pas référencé dans le chantier, on ne calcule pas de prix chantier
    ParametreChantier articleChantier = parametreChantier.get(parametreArticle.getIdArticle(), quantiteUV);
    if (articleChantier == null) {
      return;
    }
    
    // Calculer le prix
    FormulePrixVente formulePrixVente =
        new FormulePrixVente(EnumTypeFormulePrixVente.CHANTIER, parametreEtablissement, parametreLigneVente, parametreDocumentVente,
            parametreClientFacture, parametreClientLivre, parametreArticle, parametreTarif, null, articleChantier);
    formulePrixVente.calculer();
    
    // Ajouter la formule prix de vente à la liste
    listeFormulePrixVente.add(formulePrixVente);
  }
  
  /**
   * Calculer le prix de la condition de vente prioritaire.
   * 
   * Note : attention dans certains cas, une CNV définit un prix net "en dur" alors la CNV est considérée comme prioritaire même si le
   * prix est supérieur au prix standard (c'est conditionnée sur une dataarea PGVMSPEM position 112)
   */
  private void calculerPrixConditionVentePrioritaire() {
    // Tester si une condition de vente prioritaire existe
    if (parametreConditionVentePrioritaire == null) {
      return;
    }
    
    // Ne pas appliquer les CNVs pour les articles consignés
    if (isConsigne()) {
      return;
    }
    
    // Calculer le prix
    FormulePrixVente formulePrixVente = new FormulePrixVente(EnumTypeFormulePrixVente.CNV_PRIORITAIRE, parametreEtablissement,
        parametreLigneVente, parametreDocumentVente, parametreClientFacture, parametreClientLivre, parametreArticle, parametreTarif,
        parametreConditionVentePrioritaire, null);
    formulePrixVente.calculer();
    
    // Ajouter la formule prix de vente à la liste
    listeFormulePrixVente.add(formulePrixVente);
  }
  
  /**
   * Calculer tous les prix des conditions de ventes normales.
   */
  private void calculerPrixConditionVenteNormale() {
    // Tester s'il y a au moins une condition de vente normale
    if (listeParametreConditionVenteNormale == null || listeParametreConditionVenteNormale.isEmpty()) {
      return;
    }
    
    // Ne pas appliquer les CNVs pour les articles consignés
    if (isConsigne()) {
      return;
    }
    
    // Parcourir les paramètres
    for (ParametreConditionVente parametreConditionVente : listeParametreConditionVenteNormale) {
      // Mettre à jour la colonne tarif héritée issue des CNVs précédentes
      if (formulePrixVenteHeritee != null) {
        parametreConditionVente.setNumeroColonneTarifHeritee(formulePrixVenteHeritee.getNumeroColonneTarif());
      }
      
      // Calculer le prix
      FormulePrixVente formulePrixVente =
          new FormulePrixVente(EnumTypeFormulePrixVente.CNV_NORMALE, parametreEtablissement, parametreLigneVente, parametreDocumentVente,
              parametreClientFacture, parametreClientLivre, parametreArticle, parametreTarif, parametreConditionVente, null);
      formulePrixVente.calculer();
      
      // Déterminer si la colonne tarif de cette condition de vente doit être conservée pour les autres conditions de vente
      selectionnerMeilleureColonneTarif(formulePrixVente);
      
      // Ajouter la formule prix de vente à la liste
      listeFormulePrixVente.add(formulePrixVente);
    }
  }
  
  /**
   * Calculer tous les prix des conditions de ventes quantitatives.
   */
  private void calculerPrixConditionVenteQuantitative() {
    // Tester s'il y a au moins une condition de vente quantitative
    if (listeParametreConditionVenteQuantitative == null || listeParametreConditionVenteQuantitative.isEmpty()) {
      return;
    }
    
    // Ne pas appliquer les CNVs pour les articles consignés
    if (isConsigne()) {
      return;
    }
    
    // Parcourir les paramètres
    for (ParametreConditionVente parametreConditionVente : listeParametreConditionVenteQuantitative) {
      // Mettre à jour la oclonne tarif héritée issue des CNVs précédentes
      if (formulePrixVenteHeritee != null) {
        parametreConditionVente.setNumeroColonneTarifHeritee(formulePrixVenteHeritee.getNumeroColonneTarif());
      }
      
      // Calculer le prix
      FormulePrixVente formulePrixVente = new FormulePrixVente(EnumTypeFormulePrixVente.CNV_QUANTITATIVE, parametreEtablissement,
          parametreLigneVente, parametreDocumentVente, parametreClientFacture, parametreClientLivre, parametreArticle, parametreTarif,
          parametreConditionVente, null);
      formulePrixVente.calculer();
      
      // Déterminer si la colonne tarif de cette condition de vente doit être conservée pour les autres conditions de vente
      selectionnerMeilleureColonneTarif(formulePrixVente);
      
      // Ajouter la formule prix de vente à la liste
      listeFormulePrixVente.add(formulePrixVente);
    }
  }
  
  /**
   * Déterminer la formule de calcul sur laquelle vont être appliquées les conditions cumulatives.
   * 
   * Souvent, c'est le meilleur prix qui gagne mais il y a des exceptions. Il y a le cas des conditions de ventes associées
   * à un client et un article qui sont prioritaires devant tous les autres conditions de vente. Il y a des cas ou les conditions de
   * ventes issues de l'établissement sont ignorés. Il y a des cas ou le prix standard est ignoré si une condition en prix net existe.
   * 
   * Les formules de prix chantier sont ignorées à cette étape car les conditions cumulatives ne s'appliquent pas sur les prix chantier.
   * Les prix chantiers sont pris en compte dans determinerPrixFinal().
   * 
   * Le commentaire des formules prix de vente est mis à jour au cours de la sélection afin d'expliquer pourquoi chaque formule n'a pas
   * été sélectionnée.
   * 
   * La sélection est effectuée dans l'ordre de priorité suivant :
   * 1- Sélectionner la condition de vente prioritaire client/article.
   * 2- Sélectionner le meilleur prix entre le prix standard, les conditions de ventes normales et quantitatives
   * 3- Sélectionner le prix standard par défaut.
   */
  private void determinerPrixFinalAvantConditionCumulative() {
    // Récupérer les formules prix utilisées
    FormulePrixVente formulePrixVenteStandard = getFormulePrixVenteStandard();
    List<FormulePrixVente> listeFormulePrixconditionVente = getListeFormulePrixVenteConditionVenteNormaleEtQuantitative();
    
    // Sélectionner le prix de la condition de vente prioritaire si elle existe
    FormulePrixVente formulePrixVenteConditionVentePrioritaire = getFormulePrixVenteConditionVentePrioritaire();
    if (formulePrixVenteConditionVentePrioritaire != null) {
      formulePrixVenteConditionVentePrioritaire.setUtilise(Boolean.TRUE);
      formulePrixVenteFinale = formulePrixVenteConditionVentePrioritaire;
      
      // Commenter toutes les formules ignorées car il existe une condition prioritaire
      String commentaire =
          "Ignoré : la condition de vente " + formulePrixVenteFinale.getLibelle() + " pour 1 client et 1 article est prioritaire";
      if (formulePrixVenteStandard != null) {
        formulePrixVenteStandard.setCommentaire(commentaire);
      }
      for (FormulePrixVente formulePrixVente : listeFormulePrixconditionVente) {
        formulePrixVente.setCommentaire(commentaire);
      }
      return;
    }
    
    // Sélectionner le meilleur prix entre le prix standard et les conditions de ventes normales et quantitatives
    if (!listeFormulePrixconditionVente.isEmpty()) {
      // Commencer par prendre la formule de prix standard
      formulePrixVenteFinale = formulePrixVenteStandard;
      
      // Rechercher le meilleur prix en comparant les montants HT des conditions de ventes
      for (FormulePrixVente formulePrixVente : listeFormulePrixconditionVente) {
        // Vérifier que la condition de vente est présente
        if (formulePrixVente.getParametreConditionVente() == null) {
          formulePrixVente.setCommentaire("Ignoré : la condition de vente est invalide");
          continue;
        }
        
        // Vérifier que le montant HT est renseigné
        if (formulePrixVente.getMontantHT() == null) {
          formulePrixVente.setCommentaire("Ignoré : le montant HT est invalide");
          continue;
        }
        
        // Vérifier que la condition de ventes normale de la formule est éligible à la sélection finale
        if (formulePrixVente.getParametreConditionVente() != null && formulePrixVente.getParametreConditionVente().isNormale()
            && !controlerExistenceRattachementArticlePlusFin(formulePrixVente.getParametreConditionVente(),
                listeParametreConditionVenteNormale)) {
          formulePrixVente.setCommentaire("Ignoré : une CNV a un rattachement article plus fin");
          continue;
        }
        
        // Vérifier que les conditions quantitatives sont applicables
        if (formulePrixVente.getTypeFormulePrixVente() == EnumTypeFormulePrixVente.CNV_QUANTITATIVE
            && !isConditionVenteQuantitativeApplicable(formulePrixVente.getParametreConditionVente())) {
          formulePrixVente.setCommentaire("Ignoré : la condition quantitative n'est pas applicable dans ce cas");
          continue;
        }
        
        // Vérifier que la condition de ventes quantitative de la formule est éligible à la sélection finale
        if (formulePrixVente.getParametreConditionVente() != null && formulePrixVente.getParametreConditionVente().isQuantitative()
            && !controlerExistenceRattachementArticlePlusFin(formulePrixVente.getParametreConditionVente(),
                listeParametreConditionVenteQuantitative)) {
          formulePrixVente.setCommentaire("Ignoré : une CNV a un rattachement article plus fin");
          continue;
        }
        
        // Sélectionner le premier prix non null
        if (formulePrixVenteFinale == null || formulePrixVenteFinale.getMontantHT() == null) {
          formulePrixVenteFinale = formulePrixVente;
          formulePrixVenteFinale.setUtilise(Boolean.TRUE);
          continue;
        }
        
        // Suivant la valeur du paramètre getRegleExclusionConditionVenteEtablissement() du client, cela rend prioritaire les
        // conditions de vente avec un prix net HT sur le prix standard, y compris si le prix standard est moins cher que le prix
        // de la condition de vente.
        //
        // Ce comportement n'a rien à voir avec le rôle de ce paramètre mais c'est une verrue qui existe dans le calcul RPG.
        //
        // Le comportement dépend donc du champ "Condition générale sur la DG" dans "Client|Fatcuration|Conditions de vente" :
        // - Pas d'exclusion => règle non active
        // - Exclusion condition de vente générale (ParamDG) => règle active
        // - Exclusion condition de vente sur les articles liés => règle non active
        // - Exclusion condition de vente générale et les articles liés => règle active
        // - Si une condition client existe, elle prime => règle non active
        EnumRegleExclusionConditionVenteEtablissement regleExclusion =
            parametreClientFacture.getRegleExclusionConditionVenteEtablissement();
        if (parametreClientFacture != null
            && (regleExclusion == EnumRegleExclusionConditionVenteEtablissement.EXCLUSION_CNV_ETABLISSEMENT
                || regleExclusion == EnumRegleExclusionConditionVenteEtablissement.EXCLUSION_CNV_ETABLISSEMENT_ET_ARTICLE_LIE)
            && formulePrixVenteFinale.getTypeFormulePrixVente() == EnumTypeFormulePrixVente.STANDARD
            && formulePrixVente.getMontantHT().compareTo(formulePrixVenteFinale.getMontantHT()) >= 0
            && formulePrixVente.getParametreConditionVente().getPrixNetHT() != null
            && formulePrixVente.getParametreConditionVente().getPrixNetHT().compareTo(BigDecimal.ZERO) > 0) {
          formulePrixVenteFinale.setCommentaire("Ignoré : paramètre client '" + regleExclusion.getLibelle() + "'");
          formulePrixVenteFinale = formulePrixVente;
          continue;
        }
        
        // Sélectionner la formule dont le montant est le plus petit
        if (formulePrixVente.getMontantHT().compareTo(formulePrixVenteFinale.getMontantHT()) < 0) {
          formulePrixVenteFinale = formulePrixVente;
          continue;
        }
      }
      
      // Commenter toutes les formules ignorées parce que leur montant est plus élevé ou égal
      // Ne pas modifier les autres commentaires qui expliquent les autres cas d'exclusion
      String commentaire = "Ignoré : le montant de " + formulePrixVenteFinale.getLibelle() + " est inférieur ou égal";
      if (formulePrixVenteStandard != null && formulePrixVenteStandard != formulePrixVenteFinale
          && formulePrixVenteStandard.getCommentaire() == null) {
        formulePrixVenteStandard.setCommentaire(commentaire);
      }
      for (FormulePrixVente formulePrixVente : listeFormulePrixconditionVente) {
        if (formulePrixVente != formulePrixVenteFinale && formulePrixVente.getCommentaire() == null) {
          formulePrixVente.setCommentaire(commentaire);
        }
      }
      return;
    }
    
    // Sélectionner le prix standard par défaut
    formulePrixVenteFinale = formulePrixVenteStandard;
  }
  
  /**
   * Appliquer les conditions de ventes cumulatives au prix final.
   * 
   * En théorie, les conditions de ventes cumulatives ne devraient concerner que :
   * - les remises/ajouts en valeur,
   * - les coefficients,
   * - les remises en %.
   * 
   * Mais le SGVM77R autorise tous les types de calcul. Donc, le programme de calcul en java reproduit ce schéma. Il serait plus logique
   * de n'autoriser que les 3 énoncés mais désormais des clients se servent de ce "bug" comme l'Outil Parfait par exemple.
   * Conséquence, il se peut que le résultat des autres formules de calcul aboutisse à un prix null ou égal 0.00 mais il faut quand même
   * exécuter le calcul de la condition cumulative car si la condition cumulative est en prix net, cela va donner un résultat.
   */
  private void calculerPrixFinalAvecConditionCumulative() {
    // Tester s'il y a au moins une condition de vente cumulative
    if (listeParametreConditionVenteCumulative == null || listeParametreConditionVenteCumulative.isEmpty()) {
      return;
    }
    
    // Parcourir les paramètres des conditions de ventes cumulatives et appliquer chaque cumulative au prix final
    for (ParametreConditionVente parametreConditionVente : listeParametreConditionVenteCumulative) {
      // Contrôler que si la condition de ventes est quantitative elle soit bien applicable
      if (parametreConditionVente.isQuantitative() && !isConditionVenteQuantitativeApplicable(parametreConditionVente)) {
        continue;
      }
      
      // Créer la formule prix de vente
      FormulePrixVente formulePrixVente = new FormulePrixVente(EnumTypeFormulePrixVente.CNV_CUMULATIVE, parametreEtablissement,
          parametreLigneVente, parametreDocumentVente, parametreClientFacture, parametreClientLivre, parametreArticle, parametreTarif,
          parametreConditionVente, null);
      
      // Récupérer le prix de base HT hérité, prix sélectionné issu des autres formules de calcul
      // Le prix de base hérité peut être null suivant la présence et le résultat des autres formules de calcul
      // Cela ne doit pas empêcher le calcul de cette condition cumulative
      if (formulePrixVenteFinale != null) {
        formulePrixVente.setPrixBaseHeriteHT(formulePrixVenteFinale.getPrixNetHT());
        formulePrixVente.setPrixBaseHeriteTTC(formulePrixVenteFinale.getPrixNetTTC());
      }
      
      // Calculer le prix
      formulePrixVente.calculer();
      
      // Vérifier que la condition de ventes cumulative de la formule est éligible à son utilisation
      if (formulePrixVente.getParametreConditionVente() != null
          && !controlerExistenceRattachementArticlePlusFin(formulePrixVente.getParametreConditionVente(),
              listeParametreConditionVenteCumulative)) {
        formulePrixVente.setCommentaire("Ignoré : une CNV a un rattachement article plus fin");
        continue;
      }
      
      // Ajouter la formule prix de vente à la liste des formules de prix
      listeFormulePrixVente.add(formulePrixVente);
      
      // Mise à jour du prix final
      formulePrixVenteFinale.setCommentaire("Sert de prix de base HT pour la CNV cumulative " + formulePrixVente.getLibelle());
      formulePrixVenteFinale = formulePrixVente;
      formulePrixVenteFinale.setUtilise(Boolean.TRUE);
    }
  }
  
  /**
   * Déterminer la formule prix de vente finale.
   * 
   * La formule prix de vente finale est déterminée suivant l'ordre de priorité suivant :
   * 1- Le prix consigné.
   * 2- Le prix chantier.
   * 3- Le prix issu de l'application des conditions cumulatives.
   */
  private void determinerPrixFinal() {
    // Sélectionner le prix consigné s'il existe
    // Aucune autre formule prix de vente n'est appliquée dans ce cas donc rien à commenter
    FormulePrixVente formulePrixVenteConsigne = getFormulePrixVenteConsigne();
    if (formulePrixVenteConsigne != null) {
      formulePrixVenteFinale = formulePrixVenteConsigne;
      formulePrixVenteFinale.setCommentaire("Prix final");
      return;
    }
    
    // Sélectionner le prix du chantier s'il existe
    // Note : peut-il y avoir plusieurs chantier en même temps car le code n'est pas prévu pour ?
    FormulePrixVente formulePrixVenteChantier = getFormulePrixVenteChantier();
    if (formulePrixVenteChantier != null) {
      if (formulePrixVenteFinale != null) {
        formulePrixVenteFinale.setCommentaire("Ignoré : le prix chantier est prioritaire");
      }
      formulePrixVenteFinale = formulePrixVenteChantier;
    }
    
    // Ajouter un commentaire au prix final
    if (formulePrixVenteFinale != null) {
      formulePrixVenteFinale.setCommentaire("Prix final");
    }
  }
  
  /**
   * Retourner si la condition de ventes a le rattachement article le plus fin par rapport à une liste donnée.
   * @param pParametreConditionVente La condition de vente à tester.
   * @param pListeParametreConditionVente La liste des condition de ventes.
   * @return true=La condition de ventes a le rattachement article le plus fin, false=La condition de vente n'a pas le rattachement
   *         article le plus fin.
   */
  private boolean controlerExistenceRattachementArticlePlusFin(ParametreConditionVente pParametreConditionVente,
      ListeParametreConditionVente pListeParametreConditionVente) {
    // Contrôle la condition de ventes à tester
    if (pParametreConditionVente == null) {
      return false;
    }
    // Contrôle la liste des conditions de ventes
    if (pListeParametreConditionVente == null || pListeParametreConditionVente.isEmpty()) {
      return false;
    }
    
    // Parcourt la liste des conditions de ventes à la recherche d'une condition qui a un rattachement article plus fin
    for (ParametreConditionVente parametreConditionVente : pListeParametreConditionVente) {
      // Filtre sur le même rattachement client
      if (!pParametreConditionVente.getIdRattachementClient().equals(parametreConditionVente.getIdRattachementClient())) {
        continue;
      }
      // Contrôle les identifiants rattachements articles
      if (pParametreConditionVente.getIdRattachementArticle() == null || parametreConditionVente.getIdRattachementArticle() == null) {
        continue;
      }
      
      // Compare le rattachement article de la condition de ventes avec celui de la condition de ventes de la liste en cours
      if (!pParametreConditionVente.isRattachementArticlePrioritaire(parametreConditionVente)) {
        return false;
      }
    }
    
    // Au final la condition de vente a bien le rattachement le plus fin
    return true;
  }
  
  /**
   * Déterminer si la colonne tarif de cette condition de vente doit être conservée pour les autres conditions de vente.
   * 
   * Si le prix net HT unitaire issu de la condition de vente est inférieur au prix net HT issu des conditions de ventes précédentes,
   * alors on conserve la colonne de tarif de cette condition de vente pour l'appliquer aux conditions de ventes ultérieures qui n'ont pas
   * leur propre colonne de tarif.
   * 
   * @param pFormulePrixVente Formule prix de vente de la CNV.
   */
  private void selectionnerMeilleureColonneTarif(FormulePrixVente pFormulePrixVente) {
    // Vérifier si la formule prix de vente possède un prix net HT et une colonne de tarif
    if (pFormulePrixVente == null || pFormulePrixVente.getPrixNetHT() == null || pFormulePrixVente.getParametreConditionVente() == null
        || pFormulePrixVente.getParametreConditionVente().getNumeroColonneTarif() == null
        || pFormulePrixVente.getParametreConditionVente().getNumeroColonneTarif() == 0) {
      return;
    }
    
    // Tester si le prix net HT est plus bas que celui de la formule prix de vente héritée
    if (formulePrixVenteHeritee == null || pFormulePrixVente.getPrixNetHT().compareTo(formulePrixVenteHeritee.getPrixNetHT()) < 0) {
      formulePrixVenteHeritee = pFormulePrixVente;
    }
  }
  
  /**
   * Indiquer si une condition de vente quantitative est applicable.
   * 
   * @return true=condition de vente quantitative applicable, false=condition de vente quantitative non applicable.
   */
  private boolean isConditionVenteQuantitativeApplicable(ParametreConditionVente pParametreConditionVente) {
    // Contrôler que ce soit bien une condition de ventes quantitative
    if (pParametreConditionVente == null || !pParametreConditionVente.isQuantitative()) {
      return false;
    }
    
    // Récupérer la quantité minimale à atteindre
    BigDecimal quantiteMinimale = pParametreConditionVente.getQuantiteMinimale();
    if (quantiteMinimale == null) {
      quantiteMinimale = BigDecimal.ONE;
    }
    
    // Récupérer l'identifiant de rattachement de la condition de ventes (voir +tard si judicieux de partir dans ce cas)
    IdRattachementArticle idRattachement = pParametreConditionVente.getIdRattachementArticle();
    if (idRattachement == null) {
      return false;
    }
    
    // Récupérer les quantités rattachements du document de vente si elles existent
    MapQuantiteRattachement mapQuantiteRattachement = null;
    if (parametreDocumentVente != null) {
      mapQuantiteRattachement = parametreDocumentVente.getCloneMapQuantiteParRattachement();
    }
    
    // Créer les quantités rattachements du document de vente si besoin
    if (mapQuantiteRattachement == null) {
      mapQuantiteRattachement = parametreArticle.initialiserQuantiteRattachement();
    }
    
    // Ajouter la quantité aux quantités rattachement
    parametreArticle.ajouterQuantitePourRattachement(getQuantiteUV(), mapQuantiteRattachement);
    
    // Récupérer de la quantité liée à ce rattachement sur l'ensemble du document
    BigDecimal quantiteDocument = mapQuantiteRattachement.get(idRattachement);
    
    // Contrôler que les quantités minimum soient atteintes
    if (quantiteDocument != null && quantiteDocument.compareTo(quantiteMinimale) >= 0) {
      return true;
    }
    return false;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs pour les paramètres
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner la date qui sera utilisée pour le calcul du prix de vente.
   * Contient la date du jour ou la date de la session car cette dernière peut être modifié (sauf au Comptoir à l'heure actuelle).
   * @return Date d'application.
   */
  public Date getDateApplication() {
    return dateApplication;
  }
  
  /**
   * Retourner les paramètres généraux.
   * 
   * @return
   */
  public ParametreEtablissement getParametreEtablissement() {
    return parametreEtablissement;
  }
  
  /**
   * Retourner les paramètres de la ligne de vente.
   * @return Paramètres ligne de vente.
   */
  public ParametreLigneVente getParametreLigneVente() {
    return parametreLigneVente;
  }
  
  /**
   * Modifier les paramètres de la ligne de vente.
   * @param pParametreLigneVente Paramètres ligne de vente.
   */
  public void setParametreLigneVente(ParametreLigneVente pParametreLigneVente) {
    parametreLigneVente = pParametreLigneVente;
  }
  
  /**
   * Retourner les paramètres du document de ventes.
   * 
   * @return
   */
  public ParametreDocumentVente getParametreDocumentVente() {
    return parametreDocumentVente;
  }
  
  /**
   * Retourner les paramètres du client facturé.
   * 
   * @return
   */
  public ParametreClient getParametreClientFacture() {
    return parametreClientFacture;
  }
  
  /**
   * Retourner les paramètres du client livré.
   * 
   * @return
   */
  public ParametreClient getParametreClientLivre() {
    return parametreClientLivre;
  }
  
  /**
   * Retourner les paramètres de l'article.
   * 
   * @return
   */
  public ParametreArticle getParametreArticle() {
    return parametreArticle;
  }
  
  /**
   * Retourner les paramètres du tarif de l'article.
   * 
   * @return
   */
  public ParametreTarif getParametreTarif() {
    return parametreTarif;
  }
  
  /**
   * Retourner la liste des paramètres des conditions de ventes normales.
   *
   * @return
   */
  public ListeParametreConditionVente getListeParametreConditionVenteNormale() {
    return listeParametreConditionVenteNormale;
  }
  
  /**
   * Retourner la liste des paramètres des conditions de ventes quantitatives.
   *
   * @return
   */
  public ListeParametreConditionVente getListeParametreConditionVenteQuantitative() {
    return listeParametreConditionVenteQuantitative;
  }
  
  /**
   * Retourner la liste des paramètres des conditions de ventes cumulatives.
   *
   * @return
   */
  public ListeParametreConditionVente getListeParametreConditionVenteCumulative() {
    return listeParametreConditionVenteCumulative;
  }
  
  /**
   * Retourner la liste des formules prix de ventes de ce calcul de prix.
   * @return Liste de formules prix de ventes.
   */
  public List<FormulePrixVente> getListeFormulePrixVente() {
    return listeFormulePrixVente;
  }
  
  /**
   * Retourner la formule prix de vente qui correspond au prix standard.
   * @return Formule prix de vente.
   */
  public FormulePrixVente getFormulePrixVenteStandard() {
    for (FormulePrixVente formulePrixVente : listeFormulePrixVente) {
      if (formulePrixVente.getTypeFormulePrixVente() == EnumTypeFormulePrixVente.STANDARD) {
        return formulePrixVente;
      }
    }
    return null;
  }
  
  /**
   * Retourner la formule prix de vente qui correspond au prix consigné.
   * @return Formule prix de vente.
   */
  public FormulePrixVente getFormulePrixVenteConsigne() {
    for (FormulePrixVente formulePrixVente : listeFormulePrixVente) {
      if (formulePrixVente.getTypeFormulePrixVente() == EnumTypeFormulePrixVente.CONSIGNE) {
        return formulePrixVente;
      }
    }
    return null;
  }
  
  /**
   * Retourner la formule prix de vente qui correspond au prix chantier.
   * @return Formule prix de vente.
   */
  public FormulePrixVente getFormulePrixVenteChantier() {
    for (FormulePrixVente formulePrixVente : listeFormulePrixVente) {
      if (formulePrixVente.getTypeFormulePrixVente() == EnumTypeFormulePrixVente.CHANTIER) {
        return formulePrixVente;
      }
    }
    return null;
  }
  
  /**
   * Retourner la formule prix de vente qui correspond à la CNV prioritaire.
   * @return Formule prix de vente.
   */
  public FormulePrixVente getFormulePrixVenteConditionVentePrioritaire() {
    for (FormulePrixVente formulePrixVente : listeFormulePrixVente) {
      if (formulePrixVente.getTypeFormulePrixVente() == EnumTypeFormulePrixVente.CNV_PRIORITAIRE) {
        return formulePrixVente;
      }
    }
    return null;
  }
  
  /**
   * Retourner la liste des formules des prix des conditions de ventes normales et quantitatives.
   * @return Liste de formules prix de vente.
   */
  public List<FormulePrixVente> getListeFormulePrixVenteConditionVenteNormaleEtQuantitative() {
    // Copie des formules de prix dans une liste dédiée
    List<FormulePrixVente> liste = new ArrayList<FormulePrixVente>();
    for (FormulePrixVente formulePrixVente : listeFormulePrixVente) {
      if (formulePrixVente.getTypeFormulePrixVente() == EnumTypeFormulePrixVente.CNV_NORMALE
          || formulePrixVente.getTypeFormulePrixVente() == EnumTypeFormulePrixVente.CNV_QUANTITATIVE) {
        liste.add(formulePrixVente);
      }
    }
    return liste;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs pour le résulat final
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Indiquer si le calcul de prix est en mode négoce ou non.
   * @return true=mode négoce, false=mode standard.
   */
  public boolean isModeNegoce() {
    if (parametreEtablissement == null) {
      return false;
    }
    
    return parametreEtablissement.isModeNegocePS287();
  }
  
  /**
   * Retourner la formule correspondant au prix final.
   * @return Formule prix de vente (peut être null si aucun prix n'est valide).
   */
  public FormulePrixVente getFormulePrixVenteFinale() {
    return formulePrixVenteFinale;
  }
  
  /**
   * Retourner le type de la formule prix de vente finale.
   * Exemples : standard, chantier, CNV prioritaire, CNV normale, CNV quantitative, CNV cumulative...
   * @return Type de la formule prix de vente.
   */
  public EnumTypeFormulePrixVente getTypeFormulePrixVente() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getTypeFormulePrixVente();
  }
  
  /**
   * Retourner le paramètre CNV associé à la formule prix de vente finale (ou null si aucune).
   * @return Paramètre CNV.
   */
  public ParametreConditionVente getParametreCNV() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getParametreConditionVente();
  }
  
  /**
   * Indiquer si l'article est consigné.
   * @return true=article consigné, false=article non consigné.
   */
  public boolean isConsigne() {
    if (parametreArticle == null || parametreArticle.getIdReferenceTarif() == null) {
      return false;
    }
    return parametreArticle.getIdReferenceTarif().isConsigne();
  }
  
  /**
   * Retourner l'origine du prix de vente.
   * 
   * Détermine l'origine du prix de vente dans la formule de prix de vente finale. L'origine du prix de vente est une information qui
   * est stockée dans la ligne de vente.
   * 
   * @return Origine du prix de vente.
   */
  public EnumOriginePrixVente getOriginePrixVente() {
    // Vérifier qu'il y a un prix final
    if (formulePrixVenteFinale == null) {
      return null;
    }
    
    // Définir l'origine en fonction du type de formule prix de vente
    switch (formulePrixVenteFinale.getTypeFormulePrixVente()) {
      case STANDARD:
        // Afficher "Négocié" si le prix a été négocié, "Standard" sinon
        if (!Constantes.equals(formulePrixVenteFinale.getPrixBaseColonneTarifHT(), formulePrixVenteFinale.getPrixNetHT())) {
          return EnumOriginePrixVente.NEGOCIE;
        }
        else if (formulePrixVenteFinale.getNumeroColonneTarif() != null
            && formulePrixVenteFinale.getNumeroColonneTarif().intValue() == 1) {
          return EnumOriginePrixVente.PUBLIC;
        }
        else {
          return EnumOriginePrixVente.STANDARD;
        }
        
      case CONSIGNE:
        // Afficher "Consignation" ou "Déconsignation" pour les articles consignés
        if (formulePrixVenteFinale.getQuantiteUV().compareTo(BigDecimal.ZERO) >= 0) {
          return EnumOriginePrixVente.CONSIGNATION;
        }
        else {
          return EnumOriginePrixVente.DECONSIGNATION;
        }
        
      case CHANTIER:
        // Afficher "Chantier" pour les chantiers
        return EnumOriginePrixVente.CHANTIER;
      
      case CNV_PRIORITAIRE:
      case CNV_NORMALE:
      case CNV_QUANTITATIVE:
      case CNV_CUMULATIVE:
        if (getParametreCNV() != null) {
          // Afficher "CNV Client" ou "CNV" suivant le type de rattachement client
          if (getParametreCNV().isConditionVenteClient()) {
            return EnumOriginePrixVente.CNV_CLIENT;
          }
          if (getParametreCNV().isConditionVenteGroupeClient()) {
            return EnumOriginePrixVente.GROUPE_CNV;
          }
        }
        break;
      
      default:
        break;
    }
    
    return null;
  }
  
  /**
   * Indiquer si le calcul en mode HT ou TTC.
   * @return true=TTC, false=HT.
   */
  public boolean isModeTTC() {
    if (formulePrixVenteFinale == null) {
      return false;
    }
    return formulePrixVenteFinale.isModeTTC();
  }
  
  /**
   * Retourne le nombre de décimales à utiliser pour arrondir les prix.
   * @return Le nombre de décimale.
   */
  public int getArrondiNombreDecimale() {
    if (formulePrixVenteFinale == null || formulePrixVenteFinale.getArrondiPrix() == null) {
      return Constantes.DEUX_DECIMALES;
    }
    return formulePrixVenteFinale.getArrondiPrix().getNombreDecimale();
  }
  
  /**
   * Retourner le numéro de colonne de la formule prix de vente finale.
   * @return Numéro de la colonne tarif.
   */
  public Integer getNumeroColonneTarif() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getNumeroColonneTarif();
  }
  
  /**
   * Retourner le prix de base HT de la formule prix de vente finale.
   * @return Prix net HT.
   */
  public BigDecimal getPrixBaseHT() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getPrixBaseCalculHT();
  }
  
  /**
   * Retourner le prix base TTC de la formule prix de vente finale.
   * @return Prix net TTC.
   */
  public BigDecimal getPrixBaseTTC() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getPrixBaseCalculTTC();
  }
  
  /**
   * Retourner le taux de remise unitaire de la formule prix de vente finale.
   * @return Le taux unitaire.
   */
  public BigPercentage getTauxRemiseUnitaire() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getTauxRemiseUnitaire();
  }
  
  /**
   * Retourner le prix net HT de la formule prix de vente finale.
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetHT() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getPrixNetHT();
  }
  
  /**
   * Retourner le prix net TTC de la formule prix de vente finale.
   * @return Prix net TTC.
   */
  public BigDecimal getPrixNetTTC() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getPrixNetTTC();
  }
  
  /**
   * Retourner la quantité en UV.
   * 
   * La quantité est lue dans la ligne de vente. Si la quantité lue est nulle ou égale à zéro, la quantité 1 est utilisée par défaut.
   * S'il n'y a pas de lignes de vente (cas d'un calcu ld eprix hors d'u ndocument de vente), la quantité 1 est utilisée par défaut.
   * 
   * @return Quantité en UV.
   */
  public BigDecimal getQuantiteUV() {
    if (parametreLigneVente != null && parametreLigneVente.getQuantiteUV() != null
        && parametreLigneVente.getQuantiteUV().compareTo(BigDecimal.ZERO) != 0) {
      return parametreLigneVente.getQuantiteUV();
    }
    else {
      return BigDecimal.ONE;
    }
  }
  
  /**
   * Retourner le montant HT de la formule prix de vente finale.
   * @return Montant HT.
   */
  public BigDecimal getMontantHT() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getMontantHT();
  }
  
  /**
   * Retourner le montant de la TVA de la formule prix de vente finale.
   * 
   * @return
   */
  public BigDecimal getMontantTVA() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getMontantTVA();
  }
  
  /**
   * Retourner le montant TTC de la formule prix de vente finale.
   * 
   * @return
   */
  public BigDecimal getMontantTTC() {
    if (formulePrixVenteFinale == null) {
      return null;
    }
    return formulePrixVenteFinale.getMontantTTC();
  }
  
}
