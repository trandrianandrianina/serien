/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;

/**
 * Liste de ReceptionAchat.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de lignes de vente.
 */
public class ListeReceptionAchat extends ArrayList<ReceptionAchat> {
  /**
   * Constructeur.
   */
  public ListeReceptionAchat() {
  }
  
  public ListeReceptionAchat chargerTout(IdSession pIdSession, List<IdLigneAchat> pListeIdLigneAchat) {
    return ManagerServiceDocumentAchat.chargerListeReceptionAchat(pIdSession, pListeIdLigneAchat);
  }
  
  /**
   * Retourner une ReceptionAchat de la liste à partir de son identifiant.
   */
  public ReceptionAchat retournerReceptionAchatParId(IdReceptionAchat pIdReceptionAchat) {
    if (pIdReceptionAchat == null) {
      return null;
    }
    for (ReceptionAchat receptionAchat : this) {
      if (receptionAchat != null && Constantes.equals(receptionAchat.getId(), pIdReceptionAchat)) {
        return receptionAchat;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner une ReceptionAchat de la liste à partir de l'ID de l'article qu'elle contient.
   */
  public ReceptionAchat retournerReceptionAchatParArticle(IdArticle pIdArticle) {
    if (pIdArticle == null) {
      return null;
    }
    for (ReceptionAchat ReceptionAchat : this) {
      if (ReceptionAchat != null && Constantes.equals(ReceptionAchat.getIdArticle(), pIdArticle)) {
        return ReceptionAchat;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un ReceptionAchat est présent dans la liste.
   */
  public boolean isPresent(IdReceptionAchat pIdReceptionAchat) {
    return retournerReceptionAchatParId(pIdReceptionAchat) != null;
  }
}
