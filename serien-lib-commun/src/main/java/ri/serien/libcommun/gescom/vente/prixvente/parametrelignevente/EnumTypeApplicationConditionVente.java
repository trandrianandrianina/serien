/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les différents types d'application des conditions de ventes.
 */
public enum EnumTypeApplicationConditionVente {
  APPLIQUE(' ', "Application normale"),
  IGNORE_CNV_QUANTITATIVE('1', "Conditions de ventes quantitatives non appliquées et conditions de ventes normale appliquées."),
  A_LA_DEMANDE('2', "Au choix de l'utilisateur lors de la création de la ligne."),
  FORCE_CNV_QUANTITATIVE('3', "Force les conditions de ventes quantitatives et les conditions de ventes normales sont ignorées.");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeApplicationConditionVente(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumTypeApplicationConditionVente valueOfByCode(Character pCode) {
    for (EnumTypeApplicationConditionVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type d'application des conditions de ventes est invalide : " + pCode);
  }
  
}
