/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.sousfamille;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.famille.IdFamille;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de sous-familles.
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de sous famille. Elle contient toutes les méthodes
 * oeuvrant sur la liste tandis que la classe Sous famille contient les méthodes oeuvrant sur un seul groupe.
 */
public class ListeSousFamille extends ListeClasseMetier<IdSousFamille, SousFamille, ListeSousFamille> {
  /**
   * Constructeur.
   */
  public ListeSousFamille() {
  }
  
  /**
   * Charger la liste des sous familles suivant une liste d'identifiant.
   */
  @Override
  public ListeSousFamille charger(IdSession pIdSession, List<IdSousFamille> pListeId) {
    return null;
  }
  
  /**
   * Charger toutes les sous-familles actifs de l'établissement donné.
   */
  @Override
  public ListeSousFamille charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    IdSession.controlerId(pIdSession, true);
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    CritereSousFamilles criteres = new CritereSousFamilles();
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeSousFamille(pIdSession, criteres);
  }
  
  /**
   * Retourne la liste des sous familles d'une même famille.
   */
  public ListeSousFamille getListeSousFamillePourFamille(IdFamille pIdFamille) {
    IdFamille.controlerId(pIdFamille, true);
    
    // On sélectionne les familles qui appartiennent au groupe
    ListeSousFamille listeSousFamille = new ListeSousFamille();
    for (SousFamille sousFamille : this) {
      if (sousFamille.getId().getCode().contains(pIdFamille.getCode())) {
        listeSousFamille.add(sousFamille);
      }
    }
    return listeSousFamille;
  }
}
