/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.etablissement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste d'établissements.
 */
public class ListeEtablissement extends ArrayList<Etablissement> {
  /**
   * Constructeur par défaut.
   */
  public ListeEtablissement() {
  }
  
  /**
   * Charger tous les établissements.
   * Un message d'erreur est généré si quacun établissement n'est trouvé.
   */
  public static ListeEtablissement charger(IdSession pIdSession) {
    ListeEtablissement listeEtablissement = ManagerServiceParametre.chargerListeEtablissement(pIdSession);
    if (listeEtablissement == null) {
      throw new MessageErreurException(
          "Aucun établissement n'a été trouvé dans la base de données. Veuillez contacter le service assistance.");
    }
    return listeEtablissement;
  }
  
  /**
   * Retourner un établissement de la liste à partir de son identifiant.
   */
  public Etablissement getEtablissementParId(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return null;
    }
    for (Etablissement etablissement : this) {
      if (etablissement != null && Constantes.equals(etablissement.getId(), pIdEtablissement)) {
        return etablissement;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner la raison sociale d'un établissement de la liste à partir de son identifiant.
   */
  public String getRaisonSocialeEtablissement(IdEtablissement pIdEtablissement) {
    Etablissement etablissement = getEtablissementParId(pIdEtablissement);
    if (etablissement == null || etablissement.getRaisonSociale() == null) {
      return "";
    }
    return etablissement.getRaisonSociale();
  }
  
  /**
   * Trier la liste par la raison sociale de l'établissement (ordre alphabétique croissant).
   */
  public void trierParRaisonSociale() {
    Collections.sort(this, new Comparator<Etablissement>() {
      @Override
      public int compare(Etablissement o1, Etablissement o2) {
        return o1.getRaisonSociale().compareTo(o2.getRaisonSociale());
      }
    });
  }
}
