/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.commun.champbdd.EnumChampPGVMCLIM;
import ri.serien.libcommun.commun.champbdd.EnumChampPGVMECBM;
import ri.serien.libcommun.commun.champbdd.InterfaceChampBDD;
import ri.serien.libcommun.commun.objetmetier.champmetier.InterfaceEnumChampMetier;
import ri.serien.libcommun.commun.objetmetier.champmetier.InterfacePersistanceChampMetier;

/**
 * Persistance des champs métiers d'un client.
 * Voir InterfacePersistanceChampMetier.
 */
public enum EnumPersistanceChampClient implements InterfacePersistanceChampMetier {
  NUMERO(EnumChampClient.NUMERO, "Numéro", EnumChampPGVMCLIM.NUMERO, 60, JUSTIFICATION_DROITE, "Identifiant unique du client"),
  // CIVILITE(EnumChampClient.CIVILITE, "Civilité", EnumChampPGVMCLIM.CIVILITE, 90, JUSTIFICATION_GAUCHE, "Civilité du client"),
  NOM(EnumChampClient.NOM, "Nom", EnumChampPGVMCLIM.NOM, 250, JUSTIFICATION_GAUCHE, "Nom ou raison sociale du client"),
  COMPLEMENT(EnumChampClient.COMPLEMENT, "Complément", EnumChampPGVMCLIM.COMPLEMENT, 200, JUSTIFICATION_GAUCHE,
      "Complément de nom ou prénom"),
  COMPTANT(EnumChampClient.COMPTANT, "Compte", EnumChampPGVMECBM.COMPTANT, 100, JUSTIFICATION_GAUCHE,
      "Type de compte (comptant ou en compte)"),
  RUE(EnumChampClient.RUE, "Rue 1", EnumChampPGVMECBM.RUE, 200, JUSTIFICATION_GAUCHE, "Rue du client (ligne 1)"),
  LOCALISATION(EnumChampClient.LOCALISATION, "Rue 2", EnumChampPGVMCLIM.LOCALISATION, 250, JUSTIFICATION_GAUCHE,
      "Rue du client (ligne 2)"),
  CODE_POSTAL(EnumChampClient.CODE_POSTAL, "CP", EnumChampPGVMCLIM.CODE_POSTAL, 50, JUSTIFICATION_GAUCHE,
      "Code postal de l'adresse principale"),
  COMMUNE(EnumChampClient.COMMUNE, "Commune", EnumChampPGVMCLIM.COMMUNE, 200, JUSTIFICATION_GAUCHE, "Commune de l'adresse principale"),
  PAYS(EnumChampClient.PAYS, "Pays", EnumChampPGVMCLIM.PAYS, 180, JUSTIFICATION_GAUCHE, "Pays de l'adresse principale"),
  TELEPHONE(EnumChampClient.TELEPHONE, "Téléphone", EnumChampPGVMCLIM.TELEPHONE, 100, JUSTIFICATION_CENTRE,
      "Numéro de téléphone principal"),
  CATEGORIE(EnumChampClient.CATEGORIE, "Catégorie", EnumChampPGVMCLIM.CATEGORIE, 150, JUSTIFICATION_GAUCHE, "Catégorie de client"),
  TYPE(EnumChampClient.TYPE, "Type", EnumChampPGVMCLIM.TYPE, JUSTIFICATION_GAUCHE, 80, "Type de client"),
  ETABLISSEMENT(EnumChampClient.ETABLISSEMENT, "Etablissement", EnumChampPGVMCLIM.ETABLISSEMENT, 3, JUSTIFICATION_GAUCHE,
      "Etablissement (Identifiant)"),
  NUMERO_LIVRE(EnumChampClient.NUMERO_LIVRE, "Suffixe", EnumChampPGVMCLIM.NUMERO_LIVRE, 3, JUSTIFICATION_DROITE,
      "Numéro livré du client (Identifiant)");
  
  private final InterfaceEnumChampMetier enumChampMetier;
  private final String libelle;
  private final InterfaceChampBDD champBDD;
  private final Integer largeurAffichage;
  private final Integer justification;
  private final String description;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pLibelle Libellé court du champ métier.
   * @param pChampBDD Champ base de données associé au champ métier.
   * @param pLargeurAffichage Largeur d'affichage en pixels.
   * @param pJustification JUSTIFICATION_GAUCHE, JUSTIFICATION_CENTRE ou JUSTIFICATION_DROITE.
   * @param pDescription Description en une ligne du champ métier.
   */
  EnumPersistanceChampClient(InterfaceEnumChampMetier pEnumChampMetier, String pLibelle, InterfaceChampBDD pChampBDD,
      Integer pLargeurAffichage, int pJustification, String pDescription) {
    enumChampMetier = pEnumChampMetier;
    libelle = pLibelle;
    champBDD = pChampBDD;
    largeurAffichage = pLargeurAffichage;
    justification = pJustification;
    description = pDescription;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public String toString() {
    return libelle + " : " + description;
  }
  
  @Override
  public InterfaceEnumChampMetier getEnumChampMetier() {
    return enumChampMetier;
  }
  
  @Override
  public String getLibelle() {
    return libelle;
  }
  
  @Override
  public InterfaceChampBDD getChampBDD() {
    return champBDD;
  }
  
  @Override
  public Integer getLargeurAffichage() {
    return largeurAffichage;
  }
  
  @Override
  public Integer getJustification() {
    return justification;
  }
  
  @Override
  public String getDescription() {
    return description;
  }
}
