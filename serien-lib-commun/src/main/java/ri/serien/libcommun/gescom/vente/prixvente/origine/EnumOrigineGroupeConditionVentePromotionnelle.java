/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.origine;

/**
 * Liste des origines possibles d'un groupe de condition de ventes promotionnelles.
 */
public enum EnumOrigineGroupeConditionVentePromotionnelle {
  GROUPE_CNV_CLIENT_LIVRE("Groupe CNV client livré"),
  GROUPE_CNV_CLIENT_FACTURE("Groupe CNV client facturé"),
  GROUPE_CNV_ETABLISSEMENT("Groupe CNV établissement");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrigineGroupeConditionVentePromotionnelle(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
