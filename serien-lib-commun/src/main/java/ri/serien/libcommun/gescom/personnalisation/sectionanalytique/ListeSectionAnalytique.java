/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.sectionanalytique;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste des sections analytiques.
 * L'utilisation de cette classe est à privilégiée dès qu'on manipule une liste de SectionsAnalytique. Elle contient
 * toutes les
 * méthodes oeuvrant
 * sur la liste tandis que la classe SectionAnalytique contient les méthodes oeuvrant sur une seul SectionAnalytique.
 */
public class ListeSectionAnalytique extends ListeClasseMetier<idSectionAnalytique, SectionAnalytique, ListeSectionAnalytique> {
  
  /**
   * Charger toute les sections analytiques suivant une liste d'IdAffaire.
   */
  @Override
  public ListeSectionAnalytique charger(IdSession pIdSession, List<idSectionAnalytique> pListeId) {
    return null;
  }
  
  /**
   * Charge les sections analytiques suivant l'établissement
   */
  @Override
  public ListeSectionAnalytique charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas défini.");
    }
    CriteresRechercheSectionAnalytique criteres = new CriteresRechercheSectionAnalytique();
    criteres.setTypeRecherche(CriteresRechercheSectionAnalytique.RECHERCHE_SECTION_ANALYTIQUE);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    ListeSectionAnalytique listeSection = ManagerServiceParametre.chargerListeSectionAnalytique(pIdSession, criteres);
    return listeSection;
  }
  
}
