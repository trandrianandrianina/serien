/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une configuration de catalogue
 *
 * L'identifiant est composé du numéro unique de la configuration. A noter que l'objet métier ConfigurationCatalogue, bien qu'il soit
 * lié à un établissement, ne comporte pas l'identifiant de l'établissement dans son identifiant. Le numéro est attribué de façon
 * unique pour toute la table de la base de données.
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdConfigurationCatalogue extends AbstractId {
  // Constantes
  public static final int LONGUEUR_NUMERO = 10;
  // Variables
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdConfigurationCatalogue(EnumEtatObjetMetier pEnumEtatObjetMetier, Integer pNumero) {
    super(pEnumEtatObjetMetier);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   */
  private IdConfigurationCatalogue() {
    super(EnumEtatObjetMetier.CREE);
    numero = null;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdConfigurationCatalogue getInstance(Integer pNumero) {
    return new IdConfigurationCatalogue(EnumEtatObjetMetier.MODIFIE, pNumero);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdConfigurationCatalogue getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdConfigurationCatalogue();
  }
  
  /**
   * Contrôler la validité du numéro de configuration.
   * Le numéro de configuration doit être supérieur à zéro et doit comporter au maximum 10 chiffres (entre 1 et 9 999 999 999).
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de configuration catalogue n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro de configuration catalogue est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro de configuration catalogue est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdConfigurationCatalogue controlerId(IdConfigurationCatalogue pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de la configuration catalogue est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de la configuration catalogue n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + numero.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdConfigurationCatalogue)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de configurations de catalogue.");
    }
    IdConfigurationCatalogue id = (IdConfigurationCatalogue) pObject;
    return Constantes.equals(numero, id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdConfigurationCatalogue)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdConfigurationCatalogue id = (IdConfigurationCatalogue) pObject;
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    if (numero == null) {
      return null;
    }
    return numero.toString();
  }
  
  /**
   * Numéro de la configuration catalogue.
   * Ce numéro est unique quelquesoit l'établissement.
   */
  public Integer getNumero() {
    return numero;
  }
  
}
