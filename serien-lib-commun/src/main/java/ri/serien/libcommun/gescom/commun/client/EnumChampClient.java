/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.commun.objetmetier.EnumObjetMetier;
import ri.serien.libcommun.commun.objetmetier.IdObjetMetier;
import ri.serien.libcommun.commun.objetmetier.champmetier.InterfaceEnumChampMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des numéros permettant d'identifier les champs clients.
 * Voir InterfaceEnumChampMetier.
 */
public enum EnumChampClient implements InterfaceEnumChampMetier {
  NUMERO(1),
  CIVILITE(2),
  NOM(3),
  COMPLEMENT(4),
  COMPTANT(5),
  RUE(6),
  LOCALISATION(7),
  CODE_POSTAL(8),
  COMMUNE(9),
  PAYS(10),
  TELEPHONE(11),
  CATEGORIE(12),
  TYPE(13),
  ETABLISSEMENT(14),
  NUMERO_LIVRE(15);
  
  private final IdObjetMetier idObjetMetier = IdObjetMetier.getInstance(EnumObjetMetier.CLIENT);
  private final Integer numero;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pNumero Numéro unique du champ métier.
   */
  EnumChampClient(Integer pNumero) {
    numero = pNumero;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'énumération par son numéro.
   * @return Eumération du champ métier.
   */
  static public EnumChampClient valueOfByCode(Integer pNumero) {
    for (EnumChampClient value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le numéro ne correspond pas à un champ métier existant : " + pNumero);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public String toString() {
    return "" + numero;
  }
  
  @Override
  public IdObjetMetier getIdObjetMetier() {
    return idObjetMetier;
  }
  
  @Override
  public Integer getNumero() {
    return numero;
  }
}
