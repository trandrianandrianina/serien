/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc.pricejet;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class TarifPriceJet implements Serializable {
  private BigDecimal tarifPJ = null;
  private Date dateTarif = null;
  
  private ConcurrentPriceJet concurrent = null;
  
  public TarifPriceJet(BigDecimal tarif) {
    tarifPJ = tarif;
    
    majDateTarif();
  }
  
  /**
   * Mettre à jour la date du tarif à la date du jour
   */
  private void majDateTarif() {
    dateTarif = new Date();
  }
  
  /**
   * Retourner la date du tarif formatée pour un affichage
   */
  public String retournerDateFormatee(String format) {
    if (dateTarif == null) {
      return null;
    }
    
    return ConvertDate.formateObjetDateEnString(dateTarif, format);
  }
  
  // ++++++++++++++++++ ACCESSEURS ++++++++++++++++++++ //
  
  public BigDecimal getTarifPJ() {
    return tarifPJ;
  }
  
  public void setTarifPJ(BigDecimal tarifPJ) {
    this.tarifPJ = tarifPJ;
  }
  
  public ConcurrentPriceJet getConcurrent() {
    return concurrent;
  }
  
  public void setConcurrent(ConcurrentPriceJet concurrent) {
    this.concurrent = concurrent;
  }
  
  public Date getDateTarif() {
    return dateTarif;
  }
}
