/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une personnalisation participation aux frais d'expédition.
 *
 * L'identifiant est composé du code participation aux frais d'expédition.
 * Le code participation aux frais d'expédition correspond au paramètre PE du menu des ventes.
 * Il est constitué de 1 caractère et se présente sous la forme A.
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 */
public class IdParticipationFraisExpedition extends AbstractIdAvecEtablissement {
  private static final int LONGUEUR_CODE_MIN = 1;
  
  // Variables
  private final String code;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdParticipationFraisExpedition(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdParticipationFraisExpedition getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new IdParticipationFraisExpedition(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  /**
   * Contrôler la validité du code participation aux frais d'expédition.
   * Le code participation aux frais d'expédition est une chaîne alphanumérique de 1 caractère.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code participation aux frais d'expédition n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MIN) {
      throw new MessageErreurException(
          "Le code participation aux frais d'expédition doit comporter " + LONGUEUR_CODE_MIN + " caractère : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdParticipationFraisExpedition controlerId(IdParticipationFraisExpedition pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant participation aux frais d'expédition est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException(
          "L'identifiant du code participation aux frais d'expédition n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdParticipationFraisExpedition)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants participation aux frais d'expédition.");
    }
    IdParticipationFraisExpedition id = (IdParticipationFraisExpedition) pObject;
    return code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdParticipationFraisExpedition)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdParticipationFraisExpedition id = (IdParticipationFraisExpedition) pObject;
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
  // -- Accesseurs
  
  /**
   * Code participation aux frais d'expédition.
   * Le code participation aux frais d'expédition est une chaîne alphanumérique de 1 caractère.
   */
  public String getCode() {
    return code;
  }
  
}
