/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Détermine quelle donnée envoyer en sélectionnant une date (Utiliser dans le SGVM87FM_B1)
 */
public enum EnumCategorieDate {
  // Sélectionner la variable AUCUNE_DATE signifie que l'on séléctionne la date prévus,si aucune date prévus est enregistrer on
  // sélectionne la date souhaite et si celle ci n'est pas enregistrer on prend la date de validité
  AUCUNE_DATE(" ", "Date prévue, sinon date souhaitée, sinon date validité"),
  // Sélectionner la variable DEUX_DATE signifie que l'on séléctionne la date souhaité par défaut, si aucune date prévus est enregistrer
  // Si aucune date souhaité est enregistrer, on ne fait aucun choix
  DEUX_DATE("X", "Date prévue, sinon date souhaitée"),
  DATE_PREVUS("P", "Date prévue"),
  DATE_SOUHAITE("S", "Date souhaitée");
  
  private final String code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumCategorieDate(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retournee le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  static public EnumCategorieDate valueOfByCode(String pCode) {
    for (EnumCategorieDate value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("La date préenregistré est inconnu : " + pCode);
  }
}
