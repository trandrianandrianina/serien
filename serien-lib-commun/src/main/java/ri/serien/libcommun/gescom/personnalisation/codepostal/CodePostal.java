/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.codepostal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Code postal.
 * Le code postal n'est pas lié à un établissement.
 */
public class CodePostal extends AbstractClasseMetier<IdCodePostal> {
  
  // Variables
  private Integer departement = null;
  private String nomVille = null;
  
  /**
   * Constructeur.
   */
  public CodePostal(IdCodePostal pId) {
    super(pId);
  }
  
  @Override
  public String getTexte() {
    // Convertir le code postal en chaîne de caractère
    String codePostalString = String.valueOf(id.getCodePostal());
    
    // Ajouter des 0 devant les code postaux afin qu'il fasse 5 caractères
    for (int i = codePostalString.length(); i < 5; i++) {
      codePostalString = '0' + codePostalString;
    }
    
    return codePostalString;
  }
  
  /**
   * Cet objet métier est une exception à la règle appliquée sur les toString().
   * Il n'est pas nécessaire de retourner l'identifiant entre parenthèse derrière le libellé, le code postal seul suffit.
   */
  @Override
  public String toString() {
    return getTexte();
  }
  
  /**
   * Recupérer le département
   */
  public Integer getDepartement() {
    return departement;
  }
  
  /**
   * Modifier le département.
   */
  public void setDepartement(Integer pDepartement) {
    this.departement = pDepartement;
  }
  
  /**
   * Recupérer le nom de la ville
   */
  public String getNomVille() {
    return nomVille;
  }
  
  /**
   * Modifier le nom de la ville.
   */
  public void setNomVille(String pNomVille) {
    nomVille = pNomVille;
  }
  
}
