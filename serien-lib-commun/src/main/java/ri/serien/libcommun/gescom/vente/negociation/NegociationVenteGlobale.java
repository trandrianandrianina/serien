/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.negociation;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.OutilCalculPrix;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceArticle;

/**
 * Centralise les calculs sur la négociation du prix de vente total d'un document de vente.
 */
public class NegociationVenteGlobale {
  private IdSession idSession;
  private HashMap<LigneVente, CalculPrixVente> mapPrixVente = new HashMap<LigneVente, CalculPrixVente>();
  private HashMap<IdLigneVente, Integer> mapPoidsLigneVente = new HashMap<IdLigneVente, Integer>();
  private boolean enTTC = false;
  private boolean modeNegoce = false;
  private int numeroColonne = 0;
  // En mode négoce, le prix de base est le prix public
  private BigDecimal totalBaseHT = BigDecimal.ZERO;
  private BigDecimal totalBaseTTC = BigDecimal.ZERO;
  private BigDecimal tauxRemise = BigDecimal.ZERO;
  private BigDecimal totalNetTTC = BigDecimal.ZERO;
  private BigDecimal totalNetHT = BigDecimal.ZERO;
  private BigDecimal indiceDeMarge = BigDecimal.ZERO;
  private BigDecimal tauxMarge = BigDecimal.ZERO;
  private BigDecimal totalRevientHT = BigDecimal.ZERO;
  private BigDecimal totalRevientTTC = BigDecimal.ZERO;
  private EnumTypePrixSaisiInvalide typePrixSaisiInvalide = null;
  
  /**
   * Constructeur.
   */
  public NegociationVenteGlobale(IdSession pIdSession, boolean pEnTTC, boolean pModeNegoce) {
    idSession = pIdSession;
    enTTC = pEnTTC;
    modeNegoce = pModeNegoce;
  }
  
  /**
   * Initialiser la négociation à partir des lignes articles.
   * Les prix de vente sont clonés afin de ne pas modifier les données originales.
   * 
   * @param pListeLigneVente Liste des lignes de ventes faisant l'objet de la néogciation.
   */
  public void initialiser(ListeLigneVente pListeLigneVente) {
    // Tester les paramètres
    if (pListeLigneVente == null) {
      throw new MessageErreurException("La liste des lignes de ventes est invalide.");
    }
    
    // Effacer les attributs de l'objet
    effacerVariables();
    
    // Crée une négociation de vente pour chaque ligne du document soumise à négociation
    for (LigneVente ligneVente : pListeLigneVente) {
      // Ignorer totalement les lignes commentaires et les lignes de remise spéciale
      if (ignorerLigneVente(ligneVente)) {
        continue;
      }
      
      // Contrôle que le prix de revient de la ligne de vente soit supérieur à zéro uniquement si l'article est remisable et s'il ne
      // s'agit pas d'un article transport
      if (ligneVente.isPrixNegociable() && !ligneVente.isLigneArticleTransport()) {
        LigneVente.controlerPrixDeRevient(idSession, ligneVente, null, null);
      }
      
      // Ajouter un clone du prix de vente dans notre liste de travail et un calcul de prix de vente pour chaque ligne
      ParametreChargement parametreChargement = new ParametreChargement();
      parametreChargement.setIdEtablissement(ligneVente.getId().getIdEtablissement());
      parametreChargement.setIdLigneDocumentVente(ligneVente.getId());
      CalculPrixVente calculPrixVente = ManagerServiceArticle.calculerPrixVente(idSession, parametreChargement);
      mapPrixVente.put(ligneVente.clone(), calculPrixVente);
      mapPoidsLigneVente.put(ligneVente.getId(), Integer.valueOf(0));
    }
    
    // Ne pas faire de calcul si la liste est vide
    if (mapPrixVente.isEmpty()) {
      return;
    }
    
    // Lancer les calculs
    calculer();
    calculerPoidsLignes(totalNetHT);
  }
  
  /**
   * Modifier la colonne tarif.
   * 
   * @param pNumeroColonne Nouvelle colonne de tarif à appliquer (entre 1 et NOMBRE_COLONNES_MODE_NEGOCE).
   */
  public void modifierColonneTarif(int pNumeroColonne) {
    // Initialiser la raison de l'invalidité du prix saisi
    typePrixSaisiInvalide = null;
    
    // Corriger le numéro de colonne s'il est hors bornes
    if (pNumeroColonne < 1) {
      pNumeroColonne = 1;
    }
    else if (modeNegoce && pNumeroColonne > TarifArticle.NOMBRE_COLONNES_MODE_NEGOCE) {
      pNumeroColonne = TarifArticle.NOMBRE_COLONNES_MODE_NEGOCE;
    }
    else if (!modeNegoce && pNumeroColonne > TarifArticle.NOMBRE_COLONNES_MODE_CLASSIQUE) {
      pNumeroColonne = TarifArticle.NOMBRE_COLONNES_MODE_CLASSIQUE;
    }
    
    // Appliquer le numéro de colonne sur l'ensemble des prix de vente
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      LigneVente ligneVente = entry.getKey();
      CalculPrixVente calculPrixVente = entry.getValue();
      
      // Ne pas modifier les prix non négociables ou pour un article transport
      if (!ligneVente.isPrixNegociable() || ligneVente.isLigneArticleTransport()) {
        continue;
      }
      
      // Contrôler et mettre à jour le numéro de colonne du prix de vente
      ligneVente.modifierNumeroColonneTarif(calculPrixVente, pNumeroColonne, enTTC, modeNegoce);
    }
    
    // Mettre à jour la colonne négociée
    numeroColonne = pNumeroColonne;
    
    // Refaire les calculs
    calculer();
  }
  
  /**
   * Modifier le taux de remise sur le total du document en tenant compte des lignes non remisables.
   * @param pTauxRemise Nouveau taux de remise à appliquer.
   */
  public void modifierTauxRemise(BigDecimal pTauxRemise) {
    // Contrôler la valeur saisie
    LigneVente.controlerValiditeTauxRemise(pTauxRemise, false);
    if (Constantes.equals(pTauxRemise, tauxRemise)) {
      return;
    }
    
    // Appliquer le taux de remise sur toutes les lignes de ventes
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      LigneVente ligneVente = entry.getKey();
      CalculPrixVente calculPrixVente = entry.getValue();
      
      // Ligne non remisable si l'article est non négociable
      if (!ligneVente.isPrixNegociable()) {
        continue;
      }
      // Ligne non remisable si l'article est un article transport
      if (ligneVente.isLigneArticleTransport()) {
        continue;
      }
      
      // Contrôler et mettre à jour le taux de remise du prix de vente
      ligneVente.modifierTauxRemise(calculPrixVente, pTauxRemise, enTTC, modeNegoce);
      if (enTTC) {
        // Contrôle que le nouveau prix en soit pas en dessous du prix de revient
        if (ligneVente.getPrixDeRevientStandardTTC() != null
            && ligneVente.getPrixDeRevientStandardTTC().compareTo(ligneVente.getPrixNetTTC()) > 0) {
          ligneVente.modifierPrixNet(calculPrixVente, ligneVente.getPrixDeRevientStandardTTC(), enTTC, modeNegoce);
        }
      }
      else {
        // Contrôle que le nouveau prix en soit pas en dessous du prix de revient
        if (ligneVente.getPrixDeRevientStandardHT() != null
            && ligneVente.getPrixDeRevientStandardHT().compareTo(ligneVente.getPrixNetHT()) > 0) {
          ligneVente.modifierPrixNet(calculPrixVente, ligneVente.getPrixDeRevientStandardHT(), enTTC, modeNegoce);
        }
      }
    }
    // Refaire les calculs
    calculer();
  }
  
  /**
   * Modifier le total net HT.
   * @param pTotalNetHT Nouveau total net HT.
   */
  public void modifierTotalNetHT(BigDecimal pTotalNetHT) {
    // Calculer la différence entre le nouveau prix net et l'actuel
    BigDecimal delta = totalNetHT.subtract(pTotalNetHT);
    
    // Répartir le delta entre les prix de vente
    repartirDeltaMontant(delta);
    
    // Refaire les calculs
    calculer();
  }
  
  /**
   * Modifie le total net TTC.
   * @param pTotalNetTTC Nouveau total net TTC.
   */
  public void modifierTotalNetTTC(BigDecimal pTotalNetTTC) {
    // Calculer la différence entre le nouveau prix net et l'actuel
    BigDecimal delta = totalNetTTC.subtract(pTotalNetTTC);
    
    // Répartir le delta entre les prix de vente
    repartirDeltaMontant(delta);
    
    // Refaire les calculs
    calculer();
  }
  
  /**
   * Modifier l'indice de marge.
   * 
   * On n'utilise pas les formules de prix de vente car les coefficients avec les totaux sont trop importants et cela donne des résultats
   * aberrants
   * 
   * @param pIndiceMarge Nouvel indice de marge.
   */
  public void modifierIndiceMarge(BigDecimal pIndiceMarge) {
    // Indique si on va appliquer la remise maximale
    boolean procederRemiseMaximale = false;
    
    // Calculer la différence entre le nouveau prix net et l'actuel
    BigDecimal totalNetActuel = BigDecimal.ZERO;
    BigDecimal delta = BigDecimal.ZERO;
    if (enTTC) {
      totalNetActuel = totalNetTTC;
      totalNetTTC = OutilCalculPrix.calculerPrixAvecIndiceDeMarge(totalRevientTTC, pIndiceMarge, Constantes.DEUX_DECIMALES);
      
      // Si le total net est inférieur ou égale au total revient, on applique la remise maximale
      if (totalNetTTC.compareTo(totalRevientTTC) <= 0) {
        procederRemiseMaximale = true;
      }
      // Sinon, on répartie le prix total négocié
      else {
        delta = totalNetActuel.subtract(totalNetTTC);
      }
    }
    else {
      totalNetActuel = totalNetHT;
      totalNetHT = OutilCalculPrix.calculerPrixAvecIndiceDeMarge(totalRevientHT, pIndiceMarge, Constantes.DEUX_DECIMALES);
      
      // Si le total net est inférieur ou égale au total revient, on applique la remise maximale
      if (totalNetHT.compareTo(totalRevientHT) <= 0) {
        procederRemiseMaximale = true;
      }
      // Sinon, on répartie le prix total négocié
      else {
        delta = totalNetActuel.subtract(totalNetHT);
      }
    }
    
    if (procederRemiseMaximale) {
      // Appliquer la remise maximale aux articles
      appliquerRemiseMaximale();
    }
    else {
      // Répartir le delta entre les prix de vente
      repartirDeltaMontant(delta);
    }
    
    // Refaire les calculs
    calculer();
  }
  
  /**
   * Modifier le taux de marge.
   * 
   * @param pTauxMarge Nouveau taux de marge.
   */
  public void modifierTauxMarge(BigDecimal pTauxMarge) {
    // Indique si on va appliquer la remise maximale
    boolean isPrixSaisiInferieurAuPrixRevient = false;
    
    // Calculer la différence entre le nouveau prix net et l'actuel
    BigDecimal totalNetActuel = BigDecimal.ZERO;
    BigDecimal delta = BigDecimal.ZERO;
    if (enTTC) {
      totalNetActuel = totalNetTTC;
      totalNetTTC = OutilCalculPrix.calculerPrixAvecMarge(totalRevientTTC, pTauxMarge, Constantes.DEUX_DECIMALES);
      
      // Si le total net est inférieur ou égale au total revient, on applique la remise maximale
      if (totalNetTTC.compareTo(totalRevientTTC) <= 0) {
        isPrixSaisiInferieurAuPrixRevient = true;
      }
      // Sinon, on répartie le prix total négocié
      else {
        delta = totalNetActuel.subtract(totalNetTTC);
      }
    }
    else {
      totalNetActuel = totalNetHT;
      totalNetHT = OutilCalculPrix.calculerPrixAvecMarge(totalRevientHT, pTauxMarge, Constantes.DEUX_DECIMALES);
      
      // Si le total net est inférieur ou égale au total revient, on applique la remise maximale
      if (totalNetHT.compareTo(totalRevientHT) <= 0) {
        isPrixSaisiInferieurAuPrixRevient = true;
      }
      // Sinon, on répartie le prix total négocié
      else {
        delta = totalNetActuel.subtract(totalNetHT);
      }
    }
    
    if (isPrixSaisiInferieurAuPrixRevient) {
      // Appliquer la remise maximale aux articles
      appliquerRemiseMaximale();
    }
    else {
      // Répartir le delta entre les prix de vente
      repartirDeltaMontant(delta);
    }
    
    // Refaire les calculs
    calculer();
  }
  
  /**
   * Vérifie la validité du prix total net saisi.
   * La vérification des autres paramètres passe par cette méthode pour centraliser le contrôle
   * 
   * @param pTotalNet : Le prix total net saisi
   * @return
   */
  public boolean verifierTotalNetSaisi(BigDecimal pTotalNet) {
    // Initialiser la raison de l'invalidité du prix saisi
    typePrixSaisiInvalide = null;
    if (pTotalNet.compareTo(BigDecimal.ZERO) <= 0) {
      return false;
    }
    
    if (enTTC) {
      // En mode négoce
      if (modeNegoce) {
        // Le prix saisi est invalide s'il est supérieur au prix public
        if (pTotalNet.compareTo(totalBaseTTC) > 0) {
          typePrixSaisiInvalide = EnumTypePrixSaisiInvalide.PRIX_NET_SUPERIEUR_PRIX_PUBLIC;
          return false;
        }
      }
      else {
        // Le prix saisi est invalide s'il est supérieur au prix public
        if (pTotalNet.compareTo(totalBaseTTC) > 0) {
          typePrixSaisiInvalide = EnumTypePrixSaisiInvalide.PRIX_NET_SUPERIEUR_PRIX_PUBLIC;
          return false;
        }
      }
      // Le prix saisi est considéré comme invalide s'il est inférieur ou égal au prix de revient
      if (pTotalNet.compareTo(totalRevientTTC) <= 0) {
        typePrixSaisiInvalide = EnumTypePrixSaisiInvalide.PRIX_NET_INFERIEUR_PRIX_REVIENT;
        return false;
      }
    }
    else {
      // Le prix saisi est invalide s'il est supérieur au prix public
      if (pTotalNet.compareTo(totalBaseHT) > 0) {
        typePrixSaisiInvalide = EnumTypePrixSaisiInvalide.PRIX_NET_SUPERIEUR_PRIX_PUBLIC;
        return false;
      }
      // Le prix saisi est considéré comme invalide s'il est inférieur ou égal au prix de revient
      if (pTotalNet.compareTo(totalRevientHT) <= 0) {
        typePrixSaisiInvalide = EnumTypePrixSaisiInvalide.PRIX_NET_INFERIEUR_PRIX_REVIENT;
        return false;
      }
    }
    return true;
  }
  
  /**
   * Vérifie la validité de l'indice de marge saisi
   * .
   * 
   * @param pIndiceDeMarge : L'indice de marge saisi
   * @return
   */
  public boolean verifierIndiceDeMargeSaisi(BigDecimal pIndiceDeMarge) {
    // Initialiser la raison de l'invalidité de l'indice de marge saisi
    typePrixSaisiInvalide = null;
    BigDecimal totalNet = BigDecimal.ZERO;
    
    if (pIndiceDeMarge.compareTo(BigDecimal.ZERO) <= 0) {
      return false;
    }
    
    if (enTTC) {
      totalNet = OutilCalculPrix.calculerPrixAvecIndiceDeMarge(totalRevientTTC, pIndiceDeMarge, Constantes.DEUX_DECIMALES);
    }
    else {
      totalNet = OutilCalculPrix.calculerPrixAvecIndiceDeMarge(totalRevientHT, pIndiceDeMarge, Constantes.DEUX_DECIMALES);
    }
    
    return verifierTotalNetSaisi(totalNet);
  }
  
  /**
   * Vérifie la validité de la marge saisie
   * .
   * 
   * @param pMarge : La valeur de la marge saisie
   * @return
   */
  public boolean verifierMargeSaisi(BigDecimal pMarge) {
    // Initialiser la raison de l'invalidité de la marge saisie
    typePrixSaisiInvalide = null;
    BigDecimal totalNet = BigDecimal.ZERO;
    
    if (pMarge.compareTo(BigDecimal.ZERO) < 0) {
      return false;
    }
    
    if (enTTC) {
      totalNet = OutilCalculPrix.calculerPrixAvecMarge(totalRevientTTC, pMarge, Constantes.DEUX_DECIMALES);
    }
    else {
      totalNet = OutilCalculPrix.calculerPrixAvecMarge(totalRevientHT, pMarge, Constantes.DEUX_DECIMALES);
    }
    
    return verifierTotalNetSaisi(totalNet);
  }
  
  /**
   * Vérifie la validité de la remise saisie.
   * 
   * @param pRemise : La valeur de la remise saisie
   * @return
   */
  public boolean verifierRemiseSaisi(BigDecimal pRemise) {
    // Initialiser la raison de l'invalidité de la remise saisie
    typePrixSaisiInvalide = null;
    String messageErreur = "";
    BigDecimal totalNet = BigDecimal.ZERO;
    
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_CENT) > 0) {
      messageErreur += "La valeur du taux est de " + pRemise;
      messageErreur += ".\nLe taux de la remise doit être compris entre 0 et 100 inclus.";
      throw new MessageErreurException(messageErreur);
    }
    
    if (enTTC) {
      totalNet = OutilCalculPrix.appliquerTauxRemiseSurPrix(totalBaseTTC, pRemise, Constantes.DEUX_DECIMALES);
    }
    else {
      totalNet = OutilCalculPrix.appliquerTauxRemiseSurPrix(totalBaseHT, pRemise, Constantes.DEUX_DECIMALES);
    }
    
    return verifierTotalNetSaisi(totalNet);
  }
  
  // -- Méthodes privées
  
  /**
   * Effacer tous les attributs pour revnir à une situation initiale avant calcul.
   */
  private void effacerVariables() {
    if (mapPrixVente == null) {
      mapPrixVente = new HashMap<LigneVente, CalculPrixVente>();
    }
    else {
      mapPrixVente.clear();
    }
    if (mapPoidsLigneVente == null) {
      mapPoidsLigneVente = new HashMap<IdLigneVente, Integer>();
    }
    else {
      mapPoidsLigneVente.clear();
    }
    numeroColonne = TarifArticle.COLONNE_TARIF_PUBLIC;
    totalBaseHT = BigDecimal.ZERO;
    totalBaseTTC = BigDecimal.ZERO;
    tauxRemise = BigDecimal.ZERO;
    totalNetTTC = BigDecimal.ZERO;
    totalNetHT = BigDecimal.ZERO;
    indiceDeMarge = BigDecimal.ZERO;
    tauxMarge = BigDecimal.ZERO;
    setTotalRevientHT(BigDecimal.ZERO);
    typePrixSaisiInvalide = null;
  }
  
  /**
   * Lance tous les calculs.
   */
  private void calculer() {
    controlerColonneTarif();
    calculerTotalBase();
    calculerTotalNet();
    calculerTotalPrixRevient();
    calculerTauxRemiseNegoce();
    calculerIndiceDeMarge();
    calculerTauxMarge();
  }
  
  /**
   * Vérifie les colonnes de toutes les lignes.
   */
  private void controlerColonneTarif() {
    if (mapPrixVente.size() == 0) {
      return;
    }
    
    // Extraction des colonnes dans une liste pour faciliter le traitement
    int index = 0;
    ArrayList<Integer> listeNumeroColonne = new ArrayList<Integer>();
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      LigneVente ligneVente = entry.getKey();
      
      // Si la ligne n'est pas négociable elle est ignorée
      if (!ligneVente.isPrixNegociable() || ligneVente.isLigneArticleTransport()) {
        continue;
      }
      // Sinon stockage de son numéro de colonne
      if (ligneVente.getNumeroColonneTarif() != null) {
        listeNumeroColonne.add(ligneVente.getNumeroColonneTarif());
      }
    }
    // Comparaison des numéros de colonne
    numeroColonne = 0;
    if (!listeNumeroColonne.isEmpty()) {
      boolean colonnesIdentique = true;
      int numero = listeNumeroColonne.get(0);
      for (index = 1; index < listeNumeroColonne.size(); index++) {
        if (numero != listeNumeroColonne.get(index)) {
          colonnesIdentique = false;
          break;
        }
      }
      if (colonnesIdentique) {
        numeroColonne = numero;
      }
    }
  }
  
  /**
   * Calculer les montants des prix de bases d'une ligne de vente.
   * @param pLigneVente
   * @return
   */
  private BigDecimal calculerMontantBase(LigneVente pLigneVente, CalculPrixVente pCalculPrixVente, boolean pTTC) {
    int nombreDecimale = Constantes.DEUX_DECIMALES;
    if (pCalculPrixVente != null) {
      nombreDecimale = pCalculPrixVente.getArrondiNombreDecimale();
    }
    
    if (pLigneVente.getQuantiteUV() == null) {
      return BigDecimal.ZERO.setScale(nombreDecimale);
    }
    
    if (pTTC && pLigneVente.getPrixBaseTTC() != null) {
      BigDecimal montant = pLigneVente.getQuantiteUV().multiply(pLigneVente.getPrixBaseTTC(), MathContext.DECIMAL32);
      return ArrondiPrix.appliquer(montant, nombreDecimale);
    }
    if (!pTTC && pLigneVente.getPrixBaseHT() != null) {
      BigDecimal montant = pLigneVente.getQuantiteUV().multiply(pLigneVente.getPrixBaseHT(), MathContext.DECIMAL32);
      return ArrondiPrix.appliquer(montant, nombreDecimale);
    }
    return BigDecimal.ZERO.setScale(nombreDecimale);
  }
  
  /**
   * Calculer le total des montants avec les prix de bases.
   */
  private void calculerTotalBase() {
    totalBaseHT = BigDecimal.ZERO;
    totalBaseTTC = BigDecimal.ZERO;
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      LigneVente ligneVente = entry.getKey();
      CalculPrixVente calculPrixVente = entry.getValue();
      
      totalBaseHT = totalBaseHT.add(calculerMontantBase(ligneVente, calculPrixVente, false));
      totalBaseTTC = totalBaseTTC.add(calculerMontantBase(ligneVente, calculPrixVente, true));
    }
  }
  
  /**
   * Calculer le total des montants avec les prix net.
   * En fonction du type du client (TTC ou non) le mode de calcul est différent.
   */
  private void calculerTotalNet() {
    totalNetHT = BigDecimal.ZERO;
    totalNetTTC = BigDecimal.ZERO;
    HashMap<BigDecimal, BigDecimal> listeTotalNetHTParTVA = new HashMap<BigDecimal, BigDecimal>();
    
    // Parcourt des lignes de ventes du document
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      LigneVente ligneVente = entry.getKey();
      
      totalNetHT = totalNetHT.add(ligneVente.getMontantHT());
      // Si le client est TTC : le montants TTC de la ligne vente est sommé (unité * quantité) car le taux de TVA peut être différent
      // suivant les articles
      if (enTTC) {
        totalNetTTC = totalNetTTC.add(ligneVente.getMontantTTC());
      }
      // Si le client est HT : le montants HT de la ligne de vente est sommé en fonction de leur taux de TVA
      else {
        // Récupération du taux de TVA de la ligne
        BigDecimal tva = ligneVente.getPourcentageTVA();
        BigDecimal totalProvisoire = listeTotalNetHTParTVA.get(tva);
        // Cette TVA n'existe pas encore dans la liste
        if (totalProvisoire == null) {
          totalProvisoire = BigDecimal.ZERO;
        }
        // Le montant HT de la ligne de vente est additionné au total HT déjà existant pour la même TVA
        listeTotalNetHTParTVA.put(tva, totalProvisoire.add(ligneVente.getMontantHT()));
      }
    }
    // Si le client est HT, le TTC est calculé sur la totalité par groupe de TVA puis ces montants calculés sont sommés
    if (!enTTC) {
      // Parcourt de la liste des totaux net HT générée précédemment
      for (Entry<BigDecimal, BigDecimal> entry : listeTotalNetHTParTVA.entrySet()) {
        BigDecimal tva = entry.getKey();
        BigDecimal sommeTotauxHT = entry.getValue();
        totalNetTTC = totalNetTTC.add(OutilCalculPrix.calculerPrixTTC(sommeTotauxHT, tva, Constantes.DEUX_DECIMALES));
      }
    }
  }
  
  /**
   * Calculer le total de la somme des prix de revient.
   */
  private void calculerTotalPrixRevient() {
    setTotalRevientHT(BigDecimal.ZERO);
    setTotalRevientTTC(BigDecimal.ZERO);
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      LigneVente ligneVente = entry.getKey();
      setTotalRevientHT(totalRevientHT.add(ligneVente.getPrixDeRevientLigneHT().multiply(ligneVente.getQuantiteUV())));
      // Somme des montants de revient (unité * quantité) car le taux de TVA peut être différent suivant les articles
      setTotalRevientTTC(totalRevientTTC.add(ligneVente.getPrixDeRevientLigneTTC().multiply(ligneVente.getQuantiteUV())));
    }
  }
  
  /**
   * Calcul de la remise à partir du montant total net HT et du montant total public HT.
   * Ce calcul est propre au mode négoce.
   */
  private void calculerTauxRemiseNegoce() {
    if (totalNetHT.compareTo(BigDecimal.ZERO) == 0) {
      tauxRemise = BigDecimal.ZERO;
    }
    else if (totalBaseHT.compareTo(BigDecimal.ZERO) == 0) {
      tauxRemise = BigDecimal.ZERO;
    }
    else {
      BigPercentage remise = OutilCalculPrix.calculerTauxRemise(totalBaseHT, totalNetHT);
      if (remise != null) {
        tauxRemise = remise.getTauxEnPourcentage();
      }
      else {
        tauxRemise = null;
      }
    }
  }
  
  /**
   * Calcul de l'indice de marge (à l'origine il devait s'appelait coefficient mais il est en base 100 - SNC-3123).
   */
  private void calculerIndiceDeMarge() {
    if (totalRevientHT.compareTo(BigDecimal.ZERO) == 0) {
      return;
    }
    indiceDeMarge = OutilCalculPrix.calculerIndiceDeMarge(totalNetHT, totalRevientHT, Constantes.DEUX_DECIMALES);
  }
  
  /**
   * Calcule de la marge.
   */
  private void calculerTauxMarge() {
    if (totalNetHT.compareTo(BigDecimal.ZERO) == 0) {
      return;
    }
    tauxMarge = OutilCalculPrix.calculerMarge(totalNetHT, totalRevientHT, Constantes.DEUX_DECIMALES);
  }
  
  /**
   * Répartir du delta sur les lignes de ventes.
   */
  private void repartirDeltaMontant(BigDecimal pDelta) {
    // On vérifie que l'on est bien un montant à répartir
    if (pDelta.compareTo(BigDecimal.ZERO) == 0) {
      return;
    }
    
    // Mise à jour des lignes articles
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      LigneVente ligneVente = entry.getKey();
      CalculPrixVente calculPrixVente = entry.getValue();
      
      IdLigneVente id = ligneVente.getId();
      Integer poids = mapPoidsLigneVente.get(id);
      // Si le poids de la ligne vaut zéro alors on ne fait rien
      if (poids == 0) {
        continue;
      }
      BigDecimal coefficient = new BigDecimal(poids).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32);
      BigDecimal valeurASoustraire = BigDecimal.ZERO;
      valeurASoustraire = pDelta.multiply(coefficient).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
      BigDecimal nouveauMontant = BigDecimal.ZERO;
      BigDecimal prixUnitaire;
      if (enTTC) {
        nouveauMontant = ligneVente.getMontantTTC().subtract(valeurASoustraire);
        prixUnitaire = ArrondiPrix.appliquer(nouveauMontant.divide(ligneVente.getQuantiteUV(), MathContext.DECIMAL32),
            calculPrixVente.getArrondiNombreDecimale());
        if (ligneVente.getPrixDeRevientStandardTTC() != null && ligneVente.getPrixDeRevientStandardTTC().compareTo(prixUnitaire) > 0) {
          prixUnitaire = ligneVente.getPrixDeRevientStandardTTC();
        }
      }
      else {
        nouveauMontant = ligneVente.getMontantHT().subtract(valeurASoustraire);
        prixUnitaire = ArrondiPrix.appliquer(nouveauMontant.divide(ligneVente.getQuantiteUV(), MathContext.DECIMAL32),
            calculPrixVente.getArrondiNombreDecimale());
        if (ligneVente.getPrixDeRevientStandardHT() != null && ligneVente.getPrixDeRevientStandardHT().compareTo(prixUnitaire) > 0) {
          prixUnitaire = ligneVente.getPrixDeRevientStandardHT();
        }
      }
      ligneVente.modifierPrixNet(calculPrixVente, prixUnitaire, enTTC, modeNegoce);
    }
  }
  
  /**
   * Appliquer la remise maximale pour chaque article.
   */
  private void appliquerRemiseMaximale() {
    // Mise à jour des lignes articles
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      LigneVente ligneVente = entry.getKey();
      CalculPrixVente calculPrixVente = entry.getValue();
      if (enTTC) {
        ligneVente.modifierPrixNet(calculPrixVente, ligneVente.getPrixDeRevientStandardTTC(), true, modeNegoce);
      }
      else {
        ligneVente.modifierPrixNet(calculPrixVente, ligneVente.getPrixDeRevientStandardHT(), false, modeNegoce);
      }
    }
  }
  
  /**
   * Calcule le poids de chaque ligne par rapport au montant total.
   */
  private void calculerPoidsLignes(BigDecimal pTotalNetHT) {
    int pourcentageRestant = 100;
    // Ligne qui a le poids le plus important
    IdLigneVente idLigneVentePoidsFort = null;
    // Ligne qui a le poids le plus faible mais supérieur à zéro
    IdLigneVente idLigneVentePoidsFaible = null;
    
    // Calcul du poids de chaque lignes
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      LigneVente ligneVente = entry.getKey();
      BigDecimal poids = BigDecimal.ZERO;
      // On contrôle que la ligne soit bien négociable
      if (!ligneVente.isPrixNegociable() || ligneVente.isLigneArticleTransport()) {
        continue;
      }
      if (pourcentageRestant > 0) {
        poids = OutilCalculPrix.calculerPourcentage(pTotalNetHT, ligneVente.getMontantHT()).setScale(0, RoundingMode.HALF_UP);
        pourcentageRestant = pourcentageRestant - poids.intValue();
      }
      mapPoidsLigneVente.put(ligneVente.getId(), Integer.valueOf(poids.intValue()));
    }
    
    do {
      Integer poidsFort = 0;
      Integer poidsFaible = 0;
      int nombreLignesNegociables = 0;
      // On parcourt la liste afin de récupérer les id des poids fort et faible
      for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
        LigneVente ligneVente = entry.getKey();
        if (!ligneVente.isPrixNegociable() || ligneVente.isLigneArticleTransport()) {
          continue;
        }
        nombreLignesNegociables++;
        Integer poids = mapPoidsLigneVente.get(ligneVente.getId());
        if (poids == null) {
          poids = Integer.valueOf(0);
        }
        if (poidsFort == 0 && poidsFort == poidsFaible) {
          poidsFort = poids;
          poidsFaible = poids;
          idLigneVentePoidsFort = ligneVente.getId();
          idLigneVentePoidsFaible = ligneVente.getId();
        }
        else {
          // Test pour le poids fort
          if (poids > poidsFort) {
            poidsFort = poids;
            idLigneVentePoidsFort = ligneVente.getId();
          }
          // Test pour le poids faible
          else if (0 < poids && poids < poidsFaible) {
            poidsFaible = poids;
            idLigneVentePoidsFaible = ligneVente.getId();
          }
        }
      }
      // On enregistre les poids calculés
      if (idLigneVentePoidsFort != null) {
        mapPoidsLigneVente.put(idLigneVentePoidsFort, poidsFort);
      }
      if (idLigneVentePoidsFaible != null) {
        mapPoidsLigneVente.put(idLigneVentePoidsFaible, poidsFaible);
      }
      
      // Le pourcentage restant est supérieur à zéro il est ajouté au poids le plus fort
      if (pourcentageRestant > 0) {
        Integer poids = mapPoidsLigneVente.get(idLigneVentePoidsFort);
        if (poids == null) {
          poids = Integer.valueOf(0);
        }
        poidsFort = poids + pourcentageRestant;
        mapPoidsLigneVente.put(idLigneVentePoidsFort, poidsFort);
        pourcentageRestant = 0;
      }
      // Le pourcentage restant est inférieur à zéro il est soustré au poids le plus faible
      else if (pourcentageRestant < 0) {
        Integer poids = mapPoidsLigneVente.get(idLigneVentePoidsFaible);
        if (poids == null) {
          poids = Integer.valueOf(0);
        }
        pourcentageRestant = poids - Math.abs(pourcentageRestant);
        if (pourcentageRestant < 0) {
          mapPoidsLigneVente.put(idLigneVentePoidsFaible, Integer.valueOf(0));
        }
        else {
          mapPoidsLigneVente.put(idLigneVentePoidsFaible, Integer.valueOf(Math.abs(pourcentageRestant)));
        }
      }
      
      // Contrôle de la validité des poids pour le cas particulier où il n'y a qu'une seule ligne
      if (nombreLignesNegociables == 1) {
        mapPoidsLigneVente.put(idLigneVentePoidsFort, Integer.valueOf(100));
        idLigneVentePoidsFaible = idLigneVentePoidsFort;
      }
    }
    while (pourcentageRestant != 0);
  }
  
  /**
   * Permet de controler si la ligne de vente doit être ignorée ou non, c'est à dire qu'elle n'est pas prise en compte dans le calcul du
   * total.
   * Les articles commentaires et les lignes de ventes remise spéciale sont ignorées également.
   */
  private boolean ignorerLigneVente(LigneVente pLigneVente) {
    if (pLigneVente == null) {
      return true;
    }
    if (pLigneVente.isLigneCommentaire() || pLigneVente.isLigneRemiseSpeciale()) {
      return true;
    }
    return false;
  }
  
  private void setTotalRevientHT(BigDecimal pTotalRevientHT) {
    totalRevientHT = pTotalRevientHT;
    if (totalRevientHT != null) {
      totalRevientHT = totalRevientHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
  }
  
  private void setTotalRevientTTC(BigDecimal pTotalRevientTTC) {
    totalRevientTTC = pTotalRevientTTC;
    if (totalRevientTTC != null) {
      totalRevientTTC = totalRevientTTC.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Retourner une ligne de vente de la map à partir de son identifiant.
   * @param pIdLigneVente L'identifiant de la ligne de vente.
   * @return
   */
  public LigneVente getLigneVente(IdLigneVente pIdLigneVente) {
    if (pIdLigneVente == null) {
      return null;
    }
    for (Entry<LigneVente, CalculPrixVente> entry : mapPrixVente.entrySet()) {
      if (pIdLigneVente.equals(entry.getKey().getId())) {
        return entry.getKey();
      }
    }
    return null;
  }
  
  /**
   * Retourner le prix de base HT.
   * Ce prix de base est le prix public en mode négoce.
   * @return
   */
  public BigDecimal getTotalBaseHT() {
    return totalBaseHT;
  }
  
  /**
   * Retourner le prix de base TTC.
   * Ce prix de base est le prix public en mode négoce.
   * @return
   */
  public BigDecimal getTotalBaseTTC() {
    return totalBaseTTC;
  }
  
  public int getNumeroColonne() {
    return numeroColonne;
  }
  
  public BigDecimal getTotalNetTTC() {
    return totalNetTTC;
  }
  
  public BigDecimal getTotalNetHT() {
    return totalNetHT;
  }
  
  public BigDecimal getTauxRemise() {
    return tauxRemise;
  }
  
  public BigDecimal getTauxRemise(Integer pNombreDecimales) {
    if (tauxRemise != null && pNombreDecimales != null) {
      return tauxRemise = tauxRemise.setScale(pNombreDecimales, RoundingMode.HALF_UP);
    }
    return tauxRemise;
  }
  
  public BigDecimal getIndiceMarge() {
    return indiceDeMarge;
  }
  
  public BigDecimal getTauxMarge() {
    return tauxMarge;
  }
  
  public BigDecimal getTotalRevientHT() {
    return totalRevientHT;
  }
  
  public BigDecimal getTotalRevientTTC() {
    return totalRevientTTC;
  }
  
  /**
   * Retourne la raison pour laquelle le prix saisi est invalide
   */
  public EnumTypePrixSaisiInvalide getTypePrixSaisiInvalide() {
    return typePrixSaisiInvalide;
  }
  
  /**
   * Modifier le mode TTC/HT.
   * @param pEnTTC
   */
  public void setEnTTC(boolean pEnTTC) {
    enTTC = pEnTTC;
  }
  
}
