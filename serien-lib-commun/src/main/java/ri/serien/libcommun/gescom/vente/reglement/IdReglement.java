/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un règlement.
 * 
 * L'identifiant est composé du code établissement, du numéro du règlement.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdReglement extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_NUMERO = 9;
  
  // Variables
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdReglement(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, Integer pNumero) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   */
  private IdReglement(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    numero = Integer.valueOf(0);
  }
  
  // -- Méthodes publiques
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdReglement getInstance(IdEtablissement pIdEtablissement, Integer pNumero) {
    return new IdReglement(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pNumero);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   */
  public static IdReglement getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdReglement(pIdEtablissement);
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdReglement controlerId(IdReglement pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du règlement est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du règlement n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du numéro du règlement.
   * Il doit être supérieur à zéro et doit comporter au maximum 9 chiffres (entre 1 et 999 999 999).
   */
  private Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du règlement n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du règlement est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du règlement est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  // -- Méthodes surchargées
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getIdEtablissement().hashCode();
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdReglement)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de règlement.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdReglement id = (IdReglement) pObject;
    return getIdEtablissement().equals(id.getIdEtablissement()) && numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdReglement)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdReglement id = (IdReglement) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return numero.toString();
  }
  
  // -- Accesseurs
  
  public Integer getNumero() {
    return numero;
  }
  
}
