/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Etat de l'extraction d'un document de ventes.
 */
public enum EnumEtatExtractionDocumentVente {
  AUCUNE(0, "Aucune"),
  PARTIELLE(1, "Partielle"),
  COMPLETE(2, "Complète");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumEtatExtractionDocumentVente(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Indique si l'extraction est partielle.
   */
  public boolean isPartielle() {
    return this.equals(PARTIELLE);
  }
  
  /**
   * Indique si l'extraction est complète.
   */
  public boolean isComplete() {
    return this.equals(COMPLETE);
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumEtatExtractionDocumentVente valueOfByCode(Integer pCode) {
    for (EnumEtatExtractionDocumentVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'état d'extraction du document de ventes est invalide : " + pCode);
  }
}
