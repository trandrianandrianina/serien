/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.util.ArrayList;

import ri.serien.libcommun.outils.Constantes;

/**
 * Liste de clients.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de client.
 */
public class ListeClient extends ArrayList<Client> {
  /**
   * Constructeur.
   */
  public ListeClient() {
  }
  
  /**
   * Retourner un Client de la liste à partir de son identifiant.
   */
  public Client retournerClientParId(IdClient pIdClient) {
    if (pIdClient == null) {
      return null;
    }
    for (Client Client : this) {
      if (Client != null && Constantes.equals(Client.getId(), pIdClient)) {
        return Client;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner un Client de la liste à partir de son nom.
   */
  public Client retournerClientParNom(String pNom) {
    if (pNom == null) {
      return null;
    }
    for (Client Client : this) {
      if (Client != null && Client.getAdresse() != null && Client.getAdresse().getNom().equals(pNom)) {
        return Client;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un Client est présent dans la liste.
   */
  public boolean isPresent(IdClient pIdClient) {
    return retournerClientParId(pIdClient) != null;
  }
}
