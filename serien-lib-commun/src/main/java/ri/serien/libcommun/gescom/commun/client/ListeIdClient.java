/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.util.ArrayList;

/**
 * Liste d'identifiants de clients.
 * L'utilisation de cette classe est à privilégier dès qu'on manipule une liste d'identifiants de clients.
 */
public class ListeIdClient extends ArrayList<IdClient> {
  /**
   * Constructeur par défaut.
   */
  public ListeIdClient() {
  }
  
  /**
   * Construire la partie WHERE d'une requête SQL sur les clients à partir des identifiants de la liste.
   */
  public String getRequeteWhere() {
    String sql = "";
    
    // Parcourir les identifiants de la liste
    for (IdClient idClient : this) {
      // Ajouter un OR s'il y a déjà des éléments dans la requête
      if (!sql.isEmpty()) {
        sql += " OR ";
      }
      
      // Ajouter un critère sur l'identifiant
      sql += "(cli.CLETB='" + idClient.getCodeEtablissement() + "' AND cli.CLCLI=" + idClient.getNumero() + " AND cli.CLLIV="
          + idClient.getSuffixe() + ")";
    }
    
    return sql;
  }
}
