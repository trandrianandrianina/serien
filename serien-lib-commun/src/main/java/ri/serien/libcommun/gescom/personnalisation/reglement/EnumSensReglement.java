/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.reglement;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de valeur possible pour le sens du règlement.
 */
public enum EnumSensReglement {
  A_RECEVOIR('R', "A recevoir"),
  A_PAYER('P', "A payer"),
  RELEVE(' ', "Relevé");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumSensReglement(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return code.toString();
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  static public EnumSensReglement valueOfByCode(Character pCode) {
    for (EnumSensReglement value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le send du règlement est inconnu : " + pCode);
  }
  
}
