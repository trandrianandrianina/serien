/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrearticle;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les différents types de prix de revient.
 * Cela permet de déterminer le prix de base dans le calcul du prix pour certaines conditions de ventes.
 */
public enum EnumTypePrv {
  ENTREE_STOCK(0, "Saisissable et modifié à chaque entrée en stock"),
  JAMAIS_CALCULE(1, "Saisi et jamais calculé"),
  ENTREE_CALCUL_PARAMETRE(2, "Calculé à chaque entrée avec mode de calcul paramétré"),
  NON_MODIFIE(3, "Saisi, jamais calculé & PUMP non modifié par système");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypePrv(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(numero);
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumTypePrv valueOfByCode(Integer pNumero) {
    for (EnumTypePrv value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("La source du prix de revient est invalide : " + pNumero);
  }
  
}
