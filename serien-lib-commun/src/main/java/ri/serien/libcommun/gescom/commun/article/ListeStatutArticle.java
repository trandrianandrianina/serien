/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.util.ArrayList;

/**
 * Liste de statuts articles.
 */
public class ListeStatutArticle extends ArrayList<StatutArticle> {
  /**
   * Initialiser cette liste de statuts articles avec les statuts de base Série N
   */
  public void initialiserTousLesStatuts() {
    add(new StatutArticle(StatutArticle.TOUS));
    add(new StatutArticle(StatutArticle.ACTIF));
    add(new StatutArticle(StatutArticle.FIN_DE_SERIE));
    add(new StatutArticle(StatutArticle.EPUISE));
  }
}
