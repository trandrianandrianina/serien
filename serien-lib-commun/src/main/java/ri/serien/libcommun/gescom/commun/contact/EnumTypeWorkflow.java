/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Les différents types de workflow possibles.
 */
public enum EnumTypeWorkflow {
  AUCUN(' ', "Aucun"),
  MAIL('C', "Par mail"),
  TELEPHONE('P', "Par téléphone");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeWorkflow(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans uen chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle + "(" + code + ")";
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeWorkflow valueOfByCode(Character pCode) {
    for (EnumTypeWorkflow value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de workflow est invalide : " + pCode);
  }
}
