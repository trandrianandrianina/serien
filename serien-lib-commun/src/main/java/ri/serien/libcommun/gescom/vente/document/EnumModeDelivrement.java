/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'édition pour les bons et les commandes.
 * 
 */
public enum EnumModeDelivrement {
  AUCUN(' ', "Pas de mode de délivrement"),
  ENLEVEMENT('1', "Enlèvement"),
  LIVRAISON('2', "Livraison");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumModeDelivrement(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return Integer.toString(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumModeDelivrement valueOfByCode(Character pCode) {
    for (EnumModeDelivrement value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type mode de délivrement est invalide : " + pCode);
  }
  
}
