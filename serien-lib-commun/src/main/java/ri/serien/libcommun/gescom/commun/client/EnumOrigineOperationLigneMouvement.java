/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête d'un document de vente.
 */
public enum EnumOrigineOperationLigneMouvement {
  ACHAT('A', "Achat"),
  VENTE('V', "Vente"),
  FABRICATION('F', "Fabrication"),
  STOCK('S', "Stock");

  private final Character code;
  private final String libelle;

  /**
   * Constructeur.
   */
  EnumOrigineOperationLigneMouvement(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }

  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }

  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }

  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumOrigineOperationLigneMouvement valueOfByCode(Character pCode) {
    for (EnumOrigineOperationLigneMouvement value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'origine de l'opération du mouvement de stock est invalide : " + pCode);
  }
}
