/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.lot;

import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;

/**
 * Lot.
 * 
 * Un lot d'articles représente des articles de même référence identifiés par un code. Un stock propre au lot est géré.
 */
public class Lot extends AbstractClasseMetier<IdLot> {
  // Variables
  private BigDecimal quantiteSaisi = null;
  private BigDecimal quantitePrecedentSaisi = null;
  private BigDecimal stockEntree = null;
  private BigDecimal stockSortie = null;
  private BigDecimal stockReserve = null;
  private Date datePeremption = null;
  private BigDecimal montantAchat = null;
  private BigDecimal montantVente = null;
  private BigDecimal stockEntreeExercice2 = null;
  private BigDecimal stockSortieExercice2 = null;
  private BigDecimal stockReserveExercice2 = null;
  private BigDecimal stockDisponible = null;
  private Date dateFabrication = null;
  private Integer numeroPalette = null;
  private IdUnite IdUniteConditionnement = null;
  
  /**
   * Constructeur.
   */
  public Lot(IdLot pIdLot) {
    super(pIdLot);
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public Lot clone() throws CloneNotSupportedException {
    Lot o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (Lot) super.clone();
      o.setQuantiteSaisi(quantiteSaisi);
      o.setQuantitePrecedentSaisi(quantitePrecedentSaisi);
      o.setStockEntree(stockEntree);
      o.setStockSortie(stockSortie);
      o.setStockReserve(stockReserve);
      o.setDatePeremption(datePeremption);
      o.setMontantAchat(montantAchat);
      o.setMontantVente(montantVente);
      o.setStockEntreeExercice2(stockEntreeExercice2);
      o.setStockSortieExercice2(stockSortieExercice2);
      o.setStockReserveExercice2(stockReserveExercice2);
      o.setStockDisponible(stockDisponible);
      o.setDateFabrication(dateFabrication);
      o.setNumeroPalette(numeroPalette);
      o.setIdUniteConditionnement(IdUniteConditionnement);
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Accesseurs
  /**
   * 
   * Retourner la quantité saisie sur le lot.
   * 
   * @return BigDecimal
   */
  public BigDecimal getQuantiteSaisi() {
    return quantiteSaisi;
  }
  
  /**
   * 
   * Modifier la quantité saisie sur le lot.
   * 
   * @param pQuantiteSaisi
   */
  public void setQuantiteSaisi(BigDecimal pQuantiteSaisi) {
    quantiteSaisi = pQuantiteSaisi;
  }
  
  /**
   * 
   * Retourner la quantité précédemment saisie sur le lot (saisie partielle).
   * 
   * @return BigDecimal
   */
  public BigDecimal getQuantitePrecedentSaisi() {
    return quantitePrecedentSaisi;
  }
  
  /**
   * 
   * Modifier la quantité précédemment saisie sur le lot (saisie partielle).
   * 
   * @param pQuantitePrecedentSaisi
   */
  public void setQuantitePrecedentSaisi(BigDecimal pQuantitePrecedentSaisi) {
    quantitePrecedentSaisi = pQuantitePrecedentSaisi;
  }
  
  /**
   * 
   * Retourner la quantité entrée en stock sur l'exercice en cours.
   * 
   * @return BigDecimal
   */
  public BigDecimal getStockEntree() {
    return stockEntree;
  }
  
  /**
   * 
   * Modifier la quantité entrée en stock sur l'exercice en cours.
   * 
   * @param pStockEntree
   */
  public void setStockEntree(BigDecimal pStockEntree) {
    stockEntree = pStockEntree;
  }
  
  /**
   * 
   * Retourner la quantité sortie de stock sur l'exercice en cours.
   * 
   * @return BigDecimal
   */
  public BigDecimal getStockSortie() {
    return stockSortie;
  }
  
  /**
   * 
   * Modifier la quantité sortie de stock sur l'exercice en cours.
   * 
   * @param pStockSortie
   */
  public void setStockSortie(BigDecimal pStockSortie) {
    stockSortie = pStockSortie;
  }
  
  /**
   * 
   * Retourner la quantité réservée en stock sur l'exercice en cours.
   * 
   * @return BigDecimal
   */
  public BigDecimal getStockReserve() {
    return stockReserve;
  }
  
  /**
   * 
   * Modifier la quantité réservée en stock sur l'exercice en cours.
   * 
   * @param pStockReserve
   */
  public void setStockReserve(BigDecimal pStockReserve) {
    stockReserve = pStockReserve;
  }
  
  /**
   * 
   * Retourner la date de péremption commune aux articles du lot.
   * 
   * @return Date
   */
  public Date getDatePeremption() {
    return datePeremption;
  }
  
  /**
   * 
   * Modifier la date de péremption commune aux articles du lot.
   * 
   * @param pDatePeremption
   */
  public void setDatePeremption(Date pDatePeremption) {
    datePeremption = pDatePeremption;
  }
  
  /**
   * 
   * Retourner le montant total d'achat du lot.
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontantAchat() {
    return montantAchat;
  }
  
  /**
   * 
   * Modifier le montant total d'achat du lot.
   * 
   * @param pMontantAchat
   */
  public void setMontantAchat(BigDecimal pMontantAchat) {
    montantAchat = pMontantAchat;
  }
  
  /**
   * 
   * Retourner le montant total de vente du lot.
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontantVente() {
    return montantVente;
  }
  
  /**
   * 
   * Modifier le montant total de vente du lot.
   * 
   * @param pMontantVente
   */
  public void setMontantVente(BigDecimal pMontantVente) {
    montantVente = pMontantVente;
  }
  
  /**
   * 
   * Retourner la quantité entrée en stock sur l'exercice précédent.
   * 
   * @return BigDecimal
   */
  public BigDecimal getStockEntreeExercice2() {
    return stockEntreeExercice2;
  }
  
  /**
   * 
   * Modifier la quantité entrée en stock sur l'exercice précédent.
   * 
   * @param pStockEntreeExercice2
   */
  public void setStockEntreeExercice2(BigDecimal pStockEntreeExercice2) {
    stockEntreeExercice2 = pStockEntreeExercice2;
  }
  
  /**
   * 
   * Retourner la quantité sortie de stock sur l'exercice précédent.
   * 
   * @return BigDecimal
   */
  public BigDecimal getStockSortieExercice2() {
    return stockSortieExercice2;
  }
  
  /**
   * 
   * Modifier la quantité sortie de stock sur l'exercice précédent.
   * 
   * @param pStockSortieExercice2
   */
  public void setStockSortieExercice2(BigDecimal pStockSortieExercice2) {
    stockSortieExercice2 = pStockSortieExercice2;
  }
  
  /**
   * 
   * Retourner le stock réservé sur l'exercice précédent.
   * 
   * @return BigDecimal
   */
  public BigDecimal getStockReserveExercice2() {
    return stockReserveExercice2;
  }
  
  /**
   * 
   * Modifier le stock réservé sur l'exercice précédent.
   * 
   * @param pStockReserve
   */
  public void setStockReserveExercice2(BigDecimal pStockReserve) {
    stockReserveExercice2 = pStockReserve;
  }
  
  /**
   * 
   * Retourner le stock disponible pour ce lot.
   * 
   * @return BigDecimal
   */
  public BigDecimal getStockDisponible() {
    return stockDisponible;
  }
  
  /**
   * 
   * Modifier le stock disponible pour ce lot.
   * 
   * @param pStockDisponible
   */
  public void setStockDisponible(BigDecimal pStockDisponible) {
    stockDisponible = pStockDisponible;
  }
  
  /**
   * 
   * Retourner la date de fabrication des articles du lot.
   * 
   * @return Date
   */
  public Date getDateFabrication() {
    return dateFabrication;
  }
  
  /**
   * 
   * Modifier la date de fabrication des articles du lot.
   * 
   * @param pDateFabrication
   */
  public void setDateFabrication(Date pDateFabrication) {
    dateFabrication = pDateFabrication;
  }
  
  /**
   * 
   * Retourner le numéro de palette associé au lot.
   * 
   * @return Integer
   */
  public Integer getNumeroPalette() {
    return numeroPalette;
  }
  
  /**
   * 
   * Modifier le numéro de palette associé au lot.
   * 
   * @param pNumeroPalette
   */
  public void setNumeroPalette(Integer pNumeroPalette) {
    numeroPalette = pNumeroPalette;
  }
  
  /**
   * 
   * Retourner l'unité de conditionnement du lot.
   * 
   * @return IdUnite
   */
  public IdUnite getIdUniteConditionnement() {
    return IdUniteConditionnement;
  }
  
  /**
   * 
   * Modifier l'unité de conditionnement du lot.
   * 
   * @param pIdUniteConditionnement
   */
  public void setIdUniteConditionnement(IdUnite pIdUniteConditionnement) {
    IdUniteConditionnement = pIdUniteConditionnement;
  }
  
}
