/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les assiettes d'application des taux de remises.
 */
public enum EnumAssietteTauxRemise {
  PRIX_UNITAIRE(' ', "Prix base calcul HT"),
  MONTANT_LIGNE('M', "Montant HT");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumAssietteTauxRemise(Character pNumero, String pLibelle) {
    code = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumAssietteTauxRemise valueOfByCode(Character pNumero) {
    for (EnumAssietteTauxRemise value : values()) {
      if (pNumero.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'assiette d'application des remises est invalide : " + pNumero);
  }
  
}
