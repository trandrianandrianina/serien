/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;

/**
 * Condition d'achat active d'une ligne de gestion de veille concurrentielle
 * TODO conscient que l'objet ConditionAchat existe déjà avec appel RPG. Celui-ci est trop éloigné de l'existant sql pour en modifier
 * la structure (risque d'impact sur les démos HOLLOCO)
 */
public class ConditionAchatGVC implements Serializable {
  private LigneGvc ligne = null;
  private Fournisseur fournisseur = null;
  private BigDecimal prixAchatBrut = null;
  private BigDecimal prixAchatRemise = null;
  private BigDecimal remiseTotaleDevise = null;
  private BigDecimal remiseTotalePourCent = null;
  private BigDecimal remise1 = null;
  private BigDecimal remise2 = null;
  private BigDecimal remise3 = null;
  private BigDecimal remise4 = null;
  private BigDecimal remise5 = null;
  private BigDecimal remise6 = null;
  private String uniteAchat = null;
  // Ne pas descendre au dessous prix de vente minimum
  private String CAIN4 = null;
  // Ne pas prendre les remises arrière
  private int CATRL = -1;
  
  private BigDecimal CAKSC = null;
  private BigDecimal CAKAC = null;
  private BigDecimal CAPRS = null;
  
  private final BigDecimal CENT = new BigDecimal(100);
  
  // private String dateApplication = null;
  private int nbDecimales = 2;
  
  /**
   * Constructeur d'une CNA GVC
   */
  public ConditionAchatGVC(LigneGvc pLigne, Fournisseur pFourniss) {
    ligne = pLigne;
    fournisseur = pFourniss;
  }

  /* public ConditionAchatGVC(
      LigneGvc art, String col, String cod, String nom, BigDecimal prixB, BigDecimal rem1,
      BigDecimal rem2, BigDecimal rem3, BigDecimal rem4, BigDecimal rem5, BigDecimal rem6,
      String unit, String date, String cain4, String catrl, BigDecimal caksc, BigDecimal cakac, BigDecimal caprs) {
    article = art;
  
    uniteAchat = unit;
    prixAchatBrut = checkerDecimalesPrix(prixB);
    remise1 = rem1;
    remise2 = rem2;
    remise3 = rem3;
    remise4 = rem4;
    remise5 = rem5;
    remise6 = rem6;
    CAIN4 = cain4;
    try {
      if (catrl != null && !catrl.trim().equals("")) {
        CATRL = Integer.parseInt(catrl);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  
    CAPRS = caprs;
    CAKAC = cakac;
    CAKSC = caksc;
  
    // dateApplication = date;
  
    majPrixAchatRemise();
  }*/
  
  /**
   * On convertit notre tarif à 4 décimales si nécessaire.
   */
  private BigDecimal checkerDecimalesPrix(BigDecimal prix) {
    if (prix == null || uniteAchat == null) {
      return prix;
    }
    BigDecimal retour = null;
    if (uniteAchat.contains("*")) {
      retour = prix.divide(CENT);
      nbDecimales = 4;
    }
    else {
      retour = prix;
    }
    
    return retour;
  }
  
  /**
   * Retourner le coefficient pour convertir une unité de stock en d'unité d'achat
   */
  protected BigDecimal retournerCoeffUniteAchat() {
    if (CAKAC == null || CAKSC == null) {
      return BigDecimal.ONE;
    }
    
    if (CAKSC.compareTo(BigDecimal.ZERO) == 0) {
      CAKSC = BigDecimal.ONE;
    }
    // Gestion 4 décimales
    if (uniteAchat != null && uniteAchat.contains("*")) {
      return CAKSC.divide(CAKAC.multiply(CENT), nbDecimales, RoundingMode.HALF_UP).setScale(nbDecimales, RoundingMode.HALF_UP);
    }
    else {
      return CAKSC.divide(CAKAC, nbDecimales, RoundingMode.HALF_UP).setScale(nbDecimales, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Retourner le prix de revient standard de la condition d'achat
   * avec le coefficient d'unité correspondant
   */
  protected BigDecimal retournerPrixRevientStandard() {
    if (CAPRS == null) {
      return null;
    }
    
    return CAPRS.multiply(retournerCoeffUniteAchat()).setScale(nbDecimales, RoundingMode.HALF_UP);
  }
  
  /**
   * Mettre à jour le prix remisé de notre condition
   */
  public void majPrixAchatRemise() {
    if (prixAchatBrut == null) {
      return;
    }
    
    BigDecimal montantPartiel = prixAchatBrut;
    
    // Remises en cascade
    montantPartiel = calculerUnMontantRemise(montantPartiel, remise1);
    montantPartiel = calculerUnMontantRemise(montantPartiel, remise2);
    montantPartiel = calculerUnMontantRemise(montantPartiel, remise3);
    if ((CAIN4 == null || !CAIN4.trim().equals("1")) && CATRL < 1) {
      montantPartiel = calculerUnMontantRemise(montantPartiel, remise4);
      montantPartiel = calculerUnMontantRemise(montantPartiel, remise5);
      montantPartiel = calculerUnMontantRemise(montantPartiel, remise6);
    }
    
    prixAchatRemise = montantPartiel;
    
    // on calcule la remise par rapport au prix fabricant
    BigDecimal prixDeBase = null;
    if (ligne.getPrixFabricantHT() != null && ligne.getPrixFabricantHT().compareTo(BigDecimal.ZERO) > 0) {
      prixDeBase = ligne.getPrixFabricantHT();
    }
    else {
      prixDeBase = prixAchatBrut;
    }
    
    remiseTotaleDevise = prixDeBase.subtract(prixAchatRemise);
    
    if (prixDeBase.compareTo(BigDecimal.ZERO) > 0) {
      try {
        remiseTotalePourCent =
            (remiseTotaleDevise.divide(prixDeBase, nbDecimales, RoundingMode.HALF_UP)).multiply(CENT).setScale(1, RoundingMode.HALF_UP);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * Calculer une remise en devise à partir d'un montant et d'un pourcentage
   */
  private BigDecimal calculerUnMontantRemise(BigDecimal montant, BigDecimal remise) {
    if (montant == null) {
      return montant;
    }
    BigDecimal reduction = null;
    if (remise != null && remise.compareTo(BigDecimal.ZERO) > 0) {
      reduction = montant.multiply(remise).divide(CENT);
    }
    else {
      reduction = BigDecimal.ZERO;
    }
    
    montant = montant.subtract(reduction).setScale(nbDecimales, RoundingMode.HALF_UP);
    
    return montant;
  }
  
  public void setPrixAchatBrut(BigDecimal prixAchatBrut) {
    this.prixAchatBrut = checkerDecimalesPrix(prixAchatBrut);
  }
  
  public BigDecimal getPrixAchatRemise() {
    return prixAchatRemise;
  }
  
  public BigDecimal getRemiseTotalePourCent() {
    return remiseTotalePourCent;
  }
  
  public String getUniteAchat() {
    return uniteAchat;
  }

  public void setRemise1(BigDecimal remise1) {
    this.remise1 = remise1;
  }

  public void setRemise2(BigDecimal remise2) {
    this.remise2 = remise2;
  }

  public void setRemise3(BigDecimal remise3) {
    this.remise3 = remise3;
  }

  public void setRemise4(BigDecimal remise4) {
    this.remise4 = remise4;
  }

  public void setRemise5(BigDecimal remise5) {
    this.remise5 = remise5;
  }

  public void setRemise6(BigDecimal remise6) {
    this.remise6 = remise6;
  }

  public void setUniteAchat(String uniteAchat) {
    this.uniteAchat = uniteAchat;
  }

  public void setCAIN4(String cAIN4) {
    CAIN4 = cAIN4;
  }

  public void setCATRL(int cATRL) {
    CATRL = cATRL;
  }

  public void setCAKSC(BigDecimal cAKSC) {
    CAKSC = cAKSC;
  }

  public void setCAKAC(BigDecimal cAKAC) {
    CAKAC = cAKAC;
  }

  public void setCAPRS(BigDecimal cAPRS) {
    CAPRS = cAPRS;
  }

  public Fournisseur getFournisseur() {
    return fournisseur;
  }
  
  public void setFournisseur(Fournisseur fournisseur) {
    this.fournisseur = fournisseur;
  }
  
}
