/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Permet de déterminer si une période est spécifiée en nombre de jours, de mois ou d'années
 */
public enum EnumUnitePeriode {
  AUCUNE("", "Aucune"),
  JOUR("J", "En jours"),
  SEMAINE("S", "En semaines"),
  MOIS("M", "En mois"),
  ANNEE("A", "En années");
  
  private final String code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumUnitePeriode(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  static public EnumUnitePeriode valueOfByCode(String pCode) {
    for (EnumUnitePeriode value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'unité de période est inconnue : " + pCode);
  }
}
