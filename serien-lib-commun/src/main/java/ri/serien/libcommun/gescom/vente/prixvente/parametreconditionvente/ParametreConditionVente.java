/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.IdFormulePrix;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineConditionVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Classe regroupant tous les paramètres d'une condition de vente pour le calcul du prix de vente.
 */
public class ParametreConditionVente extends AbstractClasseMetier<IdParametreConditionVente> {
  // Constantes
  private static final BigDecimal MARQUEUR_PUMP = BigDecimal.valueOf(98).setScale(0, RoundingMode.HALF_UP);
  private static final BigDecimal MARQUEUR_PRIX_DE_REVIENT = Constantes.VALEUR_99;
  private static final String PREFIXE_GRATUIT = "*TG";
  
  // Description
  private IdEtablissement idEtablissement = null;
  private String libelle = null;
  
  // Rattachements
  private IdRattachementClient idRattachementClient = null;
  private IdRattachementArticle idRattachementArticle = null;
  
  // Catégorisation et type
  private EnumCategorieConditionVente categorie = null;
  private EnumTypeConditionVente type = null;
  private EnumOrigineConditionVente origine = null;
  private Boolean cumulative = null;
  
  // Restriction d'application
  private Date dateDebutValidite = null;
  private Date dateFinValidite = null;
  private String codeDevise = null;
  private BigDecimal quantiteMinimale = null;
  
  // Caractéristiques de modification du prix
  private Integer numeroColonneTarif = null;
  private Integer numeroColonneTarifHeritee = null;
  private BigDecimal prixBaseHT = null;
  private boolean prixBaseHTAvecPump = false;
  private boolean prixBaseHTAvecPrv = false;
  private BigDecimal remiseEnValeur = null;
  private BigPercentage tauxRemise1 = null;
  private BigPercentage tauxRemise2 = null;
  private BigPercentage tauxRemise3 = null;
  private BigPercentage tauxRemise4 = null;
  private BigPercentage tauxRemise5 = null;
  private BigPercentage tauxRemise6 = null;
  private BigDecimal coefficient = null;
  private IdFormulePrix idFormulePrix = null;
  private Boolean gratuit = null;
  private BigDecimal prixNetHT = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * L'identifiant est autogénéré car il n'a aucun lien avec la base.
   */
  public ParametreConditionVente() {
    super(IdParametreConditionVente.getInstance());
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le texte identifiant le paramètre.
   */
  @Override
  public String getTexte() {
    String texte = "";
    if (idRattachementClient != null) {
      texte = idRattachementClient.getCode();
    }
    if (idRattachementArticle != null) {
      texte = texte + '-' + idRattachementArticle.getTexte();
    }
    return texte;
  }
  
  /**
   * Modifier la valeur de la condition de vente.
   * 
   * La signification fonctionnelle de cette valeur varie en fonction du type de condition de vente. Il n'y a pas de champ dédié
   * dans base de données pour le numéro de colonne tarif, le prix de base HT, la remise en valeur et le prix net HT.
   * 
   * @param pValeur Valeur de la condition de vente.
   * @param pTypeConditionVente Type de la condition de vente.
   */
  public void modifierValeur(BigDecimal pValeur, EnumTypeConditionVente pTypeConditionVente) {
    if (pValeur == null) {
      throw new MessageErreurException("La valeur de la condition de vente est invalide");
    }
    if (pTypeConditionVente == null) {
      return;
    }
    
    // Effacer les valeurs
    prixBaseHTAvecPump = false;
    prixBaseHTAvecPrv = false;
    numeroColonneTarif = null;
    prixBaseHT = null;
    remiseEnValeur = null;
    prixNetHT = null;
    
    // Tester si c'est un numéro de colonne tarif ou un marqueur spécial
    if (pTypeConditionVente == EnumTypeConditionVente.REMISE_EN_TAUX || pTypeConditionVente == EnumTypeConditionVente.COEFFICIENT
        || pTypeConditionVente == EnumTypeConditionVente.FORMULE_PRIX) {
      // Tester si la condition de vente définit son prix de base HT avec le PUMP de l'article.
      if (pValeur.compareTo(MARQUEUR_PUMP) == 0) {
        prixBaseHTAvecPump = true;
      }
      // Tester si la condition de vente définit son prix de base HT avec le prix de revient de l'article.
      else if (pValeur.compareTo(MARQUEUR_PUMP) == 0) {
        prixBaseHTAvecPrv = true;
      }
      // Sinon, c'est une colonne de tarif
      // Le numéro de colonne est récupéré dans le champ valeur si la condition de ventes est de type coefficent, remise en taux ou
      // formule prix. Les valeurs 1 à 10 désignent les colonnes tarifs 1 à 10. La valeur 0 signifie qu'il n'y aucune colonne tarif. La
      // valeur null signifie que l'information n'a pas été renseignée.
      else {
        numeroColonneTarif = Integer.valueOf(pValeur.intValue());
      }
    }
    // Tester si c'est un prix de base HT
    else if (pTypeConditionVente == EnumTypeConditionVente.PRIX_DE_BASE) {
      prixBaseHT = pValeur;
    }
    // Tester si c'est une remise en valeur
    else if (pTypeConditionVente == EnumTypeConditionVente.REMISE_EN_VALEUR) {
      remiseEnValeur = pValeur.negate();
    }
    // Tester si c'est un ajout en valeur
    else if (pTypeConditionVente == EnumTypeConditionVente.AJOUT_EN_VALEUR) {
      remiseEnValeur = pValeur;
    }
    // Tester si c'est un prix net HT
    else if (pTypeConditionVente == EnumTypeConditionVente.PRIX_NET_OU_GRATUIT) {
      prixNetHT = pValeur;
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void genererLibelle() {
    libelle = "";
    
    // Ajouter une lettre la catégorie
    if (categorie != null) {
      switch (categorie) {
        case NORMALE:
          libelle += "N";
          break;
        
        case QUANTITATIVE:
          libelle += "Q";
          break;
        
        default:
          break;
      }
    }
    
    // Ajouter la lettre 'C' si les conditions sont cumulatives
    if (cumulative) {
      libelle += "C";
    }
    
    // Ajouter le code du rattachement client
    if (idRattachementClient != null) {
      libelle += "-" + idRattachementClient.getCode();
    }
    
    // Ajouter le rattachement article
    if (idRattachementArticle != null && idRattachementArticle.isConditionVentePourTousArticle()) {
      libelle += "Tous";
    }
    else if (idRattachementArticle != null) {
      libelle += "-" + idRattachementArticle.getCodeRattachement();
    }
  }
  
  /**
   * Retourner si la condition de ventes est une condition client.
   * @return true=Condition client, false=Ce n'est pas une condition client.
   */
  public boolean isConditionVenteClient() {
    if (idRattachementClient != null && idRattachementClient.isClient()) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourner si la condition de ventes est une condition pour un groupe de clients.
   * @return true=Condition groupe client, false=Ce n'est pas une condition groupe client.
   */
  public boolean isConditionVenteGroupeClient() {
    if (idRattachementClient != null && idRattachementClient.isGroupeClient()) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourner si la condition de ventes est une condition emboitée.
   * @return true=Condition emboitée, false=Condition non emboitée.
   */
  public boolean isConditionVenteEmboitee() {
    if (idRattachementArticle != null && idRattachementArticle.isConditionVenteEmboitee()) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourner si le rattachement article de cette condition de ventes est prioritaire à celui de la conditions de ventes passée en
   * paramètre.
   * @param pParametreConditionVenteAComparer La condition de ventes à comparer.
   * @return true=Le rattachement de cette condition est prioritaire, false=Le rattachement de la condition passée en paramètre est
   *         prioritaire.
   */
  public boolean isRattachementArticlePrioritaire(ParametreConditionVente pParametreConditionVenteAComparer) {
    // Si la condition de vente à comparer est invalide alors son rattachement article n'est pas prioritaire
    if (pParametreConditionVenteAComparer == null) {
      return true;
    }
    // Si la priorité de cette condition est invalide alors son rattachement article n'est pas prioritaire
    if (getPrioriteRattachementArticle() == null) {
      return false;
    }
    // Si la priorité de la condition à comparer est supérieure à la priorité de cette condition alors le rattachement article de la
    // condition à comparer est prioritaire
    Integer prioriteAComparer = pParametreConditionVenteAComparer.getPrioriteRattachementArticle();
    if (prioriteAComparer != null && getPrioriteRattachementArticle().compareTo(prioriteAComparer) > 0) {
      return false;
    }
    // Sinon le rattachement article de cette condition est prioritaire
    return true;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'identifiant de l'établissement de la condition de vente.
   * @return Identifiant établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'identifiant de l'établissement de la condition de vente.
   * @param pIdEtablissement Identifiant établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourner le libellé de la condition de vente.
   * @return Libellé.
   */
  public String getLibelle() {
    genererLibelle();
    return libelle;
  }
  
  /**
   * Retourner l'identifiant du rattachement client.
   * @return L'identifiant.
   */
  public IdRattachementClient getIdRattachementClient() {
    return idRattachementClient;
  }
  
  /**
   * Modifier l'identifiant du rattachement client.
   * @param pIdRattachementClient L'identifiant.
   */
  public void setIdRattachementClient(IdRattachementClient pIdRattachementClient) {
    idRattachementClient = pIdRattachementClient;
  }
  
  /**
   * Retourner l'identifiant du rattachement article de la condition de vente.
   * @return Identifiant du rattachement article.
   */
  public IdRattachementArticle getIdRattachementArticle() {
    return idRattachementArticle;
  }
  
  /**
   * Retourner l'identifiant du rattachement article.
   * @param pIdRattachementArticle
   */
  public void setIdRattachementArticle(IdRattachementArticle pIdRattachementArticle) {
    this.idRattachementArticle = pIdRattachementArticle;
  }
  
  /**
   * Retourner la catégorie de la condition de vente.
   * @return Catégorie : normale, quantitative, ...
   */
  public EnumCategorieConditionVente getCategorie() {
    return categorie;
  }
  
  /**
   * Modifier la catégorie de la condition de vente.
   * @param pCategorie Catégorie : normale, quantitative, ...
   */
  public void setCategorie(EnumCategorieConditionVente pCategorie) {
    categorie = pCategorie;
  }
  
  /**
   * Indiquer s'il s'agit d'une condition de vente quantitative.
   * @return true=quantitative, false=sinon.
   */
  public boolean isQuantitative() {
    return categorie == EnumCategorieConditionVente.QUANTITATIVE;
  }
  
  /**
   * Indiquer s'il s'agit d'une condition de vente normale.
   * @return true=normale, false=sinon.
   */
  public boolean isNormale() {
    return categorie == EnumCategorieConditionVente.NORMALE;
  }
  
  /**
   * Retourner le type de la condition de vente.
   * @return Type condition de vente : prix de base, remise, prix net, ...
   */
  public EnumTypeConditionVente getTypeCondition() {
    return type;
  }
  
  /**
   * Modifier le type de la condition de vente.
   * @param pType Type condition de vente : prix de base, remise, prix net, ...
   */
  public void setTypeCondition(EnumTypeConditionVente pType) {
    type = pType;
  }
  
  /**
   * Indiquer si la condition est de type prix de base.
   * @return true=prix base, false=sinon.
   */
  public boolean isTypePrixBase() {
    return type != null && type == EnumTypeConditionVente.PRIX_DE_BASE;
  }
  
  /**
   * Indiquer si la condition est de type coefficient.
   * @return true=coefficient, false=sinon.
   */
  public boolean isTypeCoefficient() {
    return type != null && type == EnumTypeConditionVente.COEFFICIENT;
  }
  
  /**
   * Indiquer si la condition est de type remise en taux.
   * @return true=remise en taux, false=sinon.
   */
  public boolean isTypeRemiseEnTaux() {
    return type != null && type == EnumTypeConditionVente.REMISE_EN_TAUX;
  }
  
  /**
   * Indiquer si la condition est de type remise en valeur.
   * @return true=remise en valeur, false=sinon.
   */
  public boolean isTypeRemiseEnValeur() {
    return type != null && type == EnumTypeConditionVente.REMISE_EN_VALEUR;
  }
  
  /**
   * Indiquer si la condition est de type ajout en valeur.
   * @return true=ajout en valeur, false=sinon.
   */
  public boolean isTypeAjoutEnValeur() {
    return type != null && type == EnumTypeConditionVente.AJOUT_EN_VALEUR;
  }
  
  /**
   * Indiquer si la condition est de type formule prix.
   * @return true=formule prix, false=sinon.
   */
  public boolean isTypeFormulePrix() {
    return type != null && type == EnumTypeConditionVente.FORMULE_PRIX;
  }
  
  /**
   * Indiquer si la condition est de type prix net.
   * @return true=prix net, false=sinon.
   */
  public boolean isTypePrixNet() {
    return type != null && type == EnumTypeConditionVente.PRIX_NET_OU_GRATUIT && !isGratuit();
  }
  
  /**
   * Retourner l'origine de la condition de vente.
   * @return Origine : saisie, document de vente, ...
   */
  public EnumOrigineConditionVente getOrigine() {
    return origine;
  }
  
  /**
   * Modifier l'origine de la condition de vente.
   * @param pOrigine Origine : saisie, document de vente, ...
   */
  public void setOrigine(EnumOrigineConditionVente pOrigine) {
    origine = pOrigine;
  }
  
  /**
   * Retourner si la condition de ventes est cumulative ou non.
   * 
   * @return
   */
  public Boolean getCumulative() {
    return cumulative;
  }
  
  /**
   * Modifier si la condition de ventes est en ajout ou non.
   * 
   * @param pCumulative
   */
  public void setCumulative(boolean pCumulative) {
    this.cumulative = pCumulative;
  }
  
  /**
   * Retourner le début de la période de validité de la condition de vente.
   * @return Date.
   */
  public Date getDateDebutValidite() {
    return dateDebutValidite;
  }
  
  /**
   * Modifier le début de la période de validité de la condition de vente.
   * @param pDateDebutValidite Date.
   */
  public void setDateDebutValidite(Date pDateDebutValidite) {
    this.dateDebutValidite = pDateDebutValidite;
  }
  
  /**
   * Retourner la fin de la période de validité de la condition de vente.
   * @return Date
   */
  public Date getDateFinValidite() {
    return dateFinValidite;
  }
  
  /**
   * Modifier la fin de la période de validité de la condition de vente.
   * @param pDateFinValidite Date.
   */
  public void setDateFinValidite(Date pDateFinValidite) {
    dateFinValidite = pDateFinValidite;
  }
  
  /**
   * Retourne la quantité minimale pour une condition de vente quantitative.
   * @return Quantité.
   */
  public BigDecimal getQuantiteMinimale() {
    return quantiteMinimale;
  }
  
  /**
   * Modifier la quantité minimale pour une condition de vente quantitative.
   * @param pQuantiteMinimale Quantité.
   */
  public void setQuantiteMinimale(BigDecimal pQuantiteMinimale) {
    quantiteMinimale = pQuantiteMinimale;
  }
  
  /**
   * Retourner le code de la devise de la condition de vente.
   * @return Code devise.
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Modifier le code de la devise de la condition de vente.
   * @param codeDevise Code devise.
   */
  public void setCodeDevise(String pCodeDevise) {
    codeDevise = pCodeDevise;
  }
  
  /**
   * Retourner le numéro de la colonne tarif de la condition de vente.
   * @return Numéro de la colonne tarif (0=aucune, 1-10=colonne tarif, null=non renseigné).
   */
  public Integer getNumeroColonneTarif() {
    return numeroColonneTarif;
  }
  
  /**
   * Modifier le numéro de la colonne tarif de la condition de vente.
   * @param pNumeroColonneTarif Numéro de colonne tarif (0=aucune, 1-10=colonne tarif, null=non renseigné).
   */
  public void setNumeroColonneTarif(Integer pNumeroColonneTarif) {
    numeroColonneTarif = pNumeroColonneTarif;
  }
  
  /**
   * Numéro de la colonne tarif héritée des CNVs précédentes.
   * @return Numéro de colonne tarif.
   */
  public Integer getNumeroColonneTarifHeritee() {
    return numeroColonneTarifHeritee;
  }
  
  /**
   * Modifier le numéro de colonne tarif héritée des CNVs précédentes.
   * @param pNumeroColonneTarifHeritee Numéro de colonne tarif.
   */
  public void setNumeroColonneTarifHeritee(Integer pNumeroColonneTarifHeritee) {
    numeroColonneTarifHeritee = pNumeroColonneTarifHeritee;
  }
  
  /**
   * Retourner le prix de base HT de la condition de vente.
   * @return Prix base HT.
   */
  public BigDecimal getPrixBaseHT() {
    return prixBaseHT;
  }
  
  /**
   * Modifier le prix de base HT de la condition de vente.
   * @param prixBaseHT Prix base HT.
   */
  public void setPrixBaseHT(BigDecimal prixBaseHT) {
    this.prixBaseHT = prixBaseHT;
  }
  
  /**
   * Indiquer si la condition de vente définit son prix de base HT avec le PUMP de l'article.
   * @return true=prix de base HT avec PUMP de l'article, false=sinon.
   */
  public boolean isPrixBaseHTAvecPump() {
    return prixBaseHTAvecPump;
  }
  
  /**
   * Modifier si la condition de vente définit son prix de base HT avec le PUMP de l'article.
   * @param pPrixBaseHTAvecPump true=prix de base HT avec PUMP de l'article, false=sinon.
   */
  public void setPrixBaseHTAvecPump(boolean pPrixBaseHTAvecPump) {
    prixBaseHTAvecPump = pPrixBaseHTAvecPump;
  }
  
  /**
   * Indiquer si la condition de vente définit son prix de base avec le prix de revient de l'article.
   * @return true=prix de base HT avec prix de revient de l'article, false=sinon.
   */
  public boolean isPrixBaseHTAvecPrv() {
    return prixBaseHTAvecPrv;
  }
  
  /**
   * Modifier si la condition de vente définit son prix de base avec le prix de revient de l'article.
   * @param pPrixBaseHTAvecPrv true=prix de base HT avec prix de revient de l'article, false=sinon.
   */
  public void setPrixBaseHTAvecPrv(boolean pPrixBaseHTAvecPrv) {
    prixBaseHTAvecPrv = pPrixBaseHTAvecPrv;
  }
  
  /**
   * Retourner la remise en valeur de la condition de vente.
   * @return Remise en valeur (négatif pour une remise, positif pour un ajout).
   */
  public BigDecimal getRemiseEnValeur() {
    return remiseEnValeur;
  }
  
  /**
   * Modifier la remise/ajout en valeur de la condition de vente.
   * C'est une remise si la valeur est négative et un ajout si la valeur est positive.
   * @param pRemiseEnValeur Remise en valeur (négatif pour une remise, positif pour un ajout).
   */
  public void setRemiseEnValeur(BigDecimal pRemiseEnValeur) {
    remiseEnValeur = pRemiseEnValeur;
  }
  
  /**
   * Indiquer si au moins un taux de remise est présent.
   * @return true=il y a au moins un taux de remise, false=sinon.
   */
  public boolean isTauxRemisePresent() {
    return tauxRemise1 != null || tauxRemise2 != null || tauxRemise3 != null || tauxRemise4 != null || tauxRemise5 != null
        || tauxRemise6 != null;
  }
  
  /**
   * Retourner le taux de remise, dont l'indice est mentionné en paramètre, de la condition de vente.
   * @param pIndex Indice du taux de remise à modifier (entre 1 et 6).
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise(int pIndex) {
    if (pIndex == 1) {
      return getTauxRemise1();
    }
    else if (pIndex == 2) {
      return getTauxRemise2();
    }
    else if (pIndex == 3) {
      return getTauxRemise3();
    }
    else if (pIndex == 4) {
      return getTauxRemise4();
    }
    else if (pIndex == 5) {
      return getTauxRemise5();
    }
    else if (pIndex == 6) {
      return getTauxRemise6();
    }
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise, dont l'indice est mentionné en paramètre, de la condition de vente.
   * @param pTauxRemise Taux de remise.
   * @param pIndex Indice du taux de remise à modifier.
   */
  public void setTauxRemise(BigPercentage pTauxRemise, int pIndex) {
    if (pIndex == 1) {
      setTauxRemise1(pTauxRemise);
    }
    else if (pIndex == 2) {
      setTauxRemise2(pTauxRemise);
    }
    else if (pIndex == 3) {
      setTauxRemise3(pTauxRemise);
    }
    else if (pIndex == 4) {
      setTauxRemise4(pTauxRemise);
    }
    else if (pIndex == 5) {
      setTauxRemise5(pTauxRemise);
    }
    else if (pIndex == 6) {
      setTauxRemise6(pTauxRemise);
    }
  }
  
  /**
   * Retourner le taux de remise 1 de la condition de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise1() {
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise 1 de la condition de vente.
   * @param pTauxRemise1 Taux de remise.
   */
  public void setTauxRemise1(BigPercentage pTauxRemise1) {
    tauxRemise1 = pTauxRemise1;
  }
  
  /**
   * Retourner le taux de remise 1 de la condition de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise2() {
    return tauxRemise2;
  }
  
  /**
   * Modifier le taux de remise 2 de la condition de vente.
   * @param pTauxRemise1 Taux de remise.
   */
  public void setTauxRemise2(BigPercentage pTauxRemise2) {
    tauxRemise2 = pTauxRemise2;
  }
  
  /**
   * Retourner le taux de remise 3 de la condition de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise3() {
    return tauxRemise3;
  }
  
  /**
   * Modifier le taux de remise 3 de la condition de vente.
   * @param pTauxRemise3 Taux de remise.
   */
  public void setTauxRemise3(BigPercentage pTauxRemise3) {
    tauxRemise3 = pTauxRemise3;
  }
  
  /**
   * Retourner le taux de remise 4 de la condition de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise4() {
    return tauxRemise4;
  }
  
  /**
   * Modifier le taux de remise 4 de la condition de vente.
   * @param pTauxRemise4 Taux de remise.
   */
  public void setTauxRemise4(BigPercentage pTauxRemise4) {
    tauxRemise4 = pTauxRemise4;
  }
  
  /**
   * Retourner le taux de remise 5 de la condition de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise5() {
    return tauxRemise5;
  }
  
  /**
   * Modifier le taux de remise 5 de la condition de vente.
   * @param pTauxRemise5 Taux de remise.
   */
  public void setTauxRemise5(BigPercentage pTauxRemise5) {
    tauxRemise5 = pTauxRemise5;
  }
  
  /**
   * Retourner le taux de remise 6 de la condition de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise6() {
    return tauxRemise6;
  }
  
  /**
   * Modifier le taux de remise 6 de la condition de vente.
   * @param pTauxRemise6 Taux de remise.
   */
  public void setTauxRemise6(BigPercentage pTauxRemise6) {
    tauxRemise6 = pTauxRemise6;
  }
  
  /**
   * Retourner le coefficient de la condition de vente.
   * @return Coefficient.
   */
  public BigDecimal getCoefficient() {
    return coefficient;
  }
  
  /**
   * Modifier le coefficient de la condition de vente.
   * @param pCoefficient Coefficient.
   */
  public void setCoefficient(BigDecimal pCoefficient) {
    coefficient = pCoefficient;
  }
  
  /**
   * Retourner l'identifiant de la formule prix (FP).
   * @return Identifiant formule prix (FP).
   */
  public IdFormulePrix getIdFormulePrix() {
    return idFormulePrix;
  }
  
  /**
   * Modifier l'identifiant de la formule prix (FP).
   * @param pIdFormulePrix Identifiant formule prix (FP).
   */
  public void setIdFormulePrix(IdFormulePrix pIdFormulePrix) {
    idFormulePrix = pIdFormulePrix;
  }
  
  /**
   * Modifier le code de la formule prix (FP).
   *
   * Le code formule prix est également utilisé pour indiquer le caractère gratuit d'une condition de vente. Pour cela, il faut mettre
   * le mot clé *TG au début du code. Le code est dans ce cas suivi par le code du paramètre type de gratuit (TG).
   * 
   * @param pCodeFormulePrix Code formule prix (FP).
   */
  public void setCodeFormulePrix(String pCodeFormulePrix) {
    pCodeFormulePrix = Constantes.normerTexte(pCodeFormulePrix);
    if (pCodeFormulePrix.startsWith(PREFIXE_GRATUIT)) {
      idFormulePrix = null;
      gratuit = Boolean.TRUE;
    }
    else if (!pCodeFormulePrix.isEmpty()) {
      idFormulePrix = IdFormulePrix.getInstance(idEtablissement, pCodeFormulePrix);
      gratuit = null;
    }
    else {
      idFormulePrix = null;
      gratuit = null;
    }
  }
  
  /**
   * Indiquer si la condition est de type gratuit.
   * @return true=gratuit, false=sinon.
   */
  public boolean isGratuit() {
    return gratuit != null && gratuit;
  }
  
  /**
   * Modifier si la condition est de type gratuit.
   * @return true=gratuit, false=sinon.
   */
  public void setGratuit(boolean pGratuit) {
    gratuit = pGratuit;
  }
  
  /**
   * Retourner le prix net HT de la condition de vente.
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetHT() {
    return prixNetHT;
  }
  
  /**
   * Modifier le prix net HT de la condition de vente.
   * @param pPrixNetHT Prix net HT.
   */
  public void setPrixNetHT(BigDecimal pPrixNetHT) {
    prixNetHT = pPrixNetHT;
  }
  
  /**
   * Retourner la valeur.
   * Cette valeur a plusieurs significations suivant le type de la condition de ventes.
   * @return La valeur.
   */
  public BigDecimal getValeur() {
    if (numeroColonneTarif != null) {
      return new BigDecimal(numeroColonneTarif);
    }
    if (prixBaseHT != null) {
      return prixBaseHT;
    }
    if (remiseEnValeur != null) {
      // Tester si c'est une remise en valeur
      if (type == EnumTypeConditionVente.REMISE_EN_VALEUR) {
        return remiseEnValeur.negate();
      }
      // Tester si c'est un ajout en valeur
      else if (type == EnumTypeConditionVente.AJOUT_EN_VALEUR) {
        return remiseEnValeur;
      }
    }
    if (prixNetHT != null) {
      return prixNetHT;
    }
    return null;
  }
  
  /**
   * Retourne la priorité du rattachement article s'il existe.
   * @return La priorité.
   */
  public Integer getPrioriteRattachementArticle() {
    if (idRattachementArticle == null) {
      return null;
    }
    return idRattachementArticle.getTypeRattachement().getPriorite();
  }
}
