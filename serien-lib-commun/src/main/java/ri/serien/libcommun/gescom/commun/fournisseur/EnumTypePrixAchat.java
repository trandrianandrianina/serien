/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Les différents types d'adresse fournisseur possibles.
 */
public enum EnumTypePrixAchat {
  // Sans frais de port, le fournisseur ne facture pas les frais de port
  PRIX_ACHAT_NET(' ', "Prix d'achat net HT", "Prix d'achat net HT"),
  // PRIX_ACHAT_BRUT('1', "Prix d'achat brut HT", "Prix d'achat brut HT (prix catalogue)"), // <- Ce n'est plus utilisé
  // Avec frais de port inclus, le fournisseur facture les frais de port
  PRIX_DE_REVIENT('2', "Prix revient HT", "Prix revient HT (prix d'achat net HT + port HT)");
  
  private final Character code;
  private final String libelle;
  private final String libelleEtendu;
  
  /**
   * Constructeur.
   */
  EnumTypePrixAchat(Character pCode, String pLibelle, String pLibelleEtendu) {
    code = pCode;
    libelle = pLibelle;
    libelleEtendu = pLibelleEtendu;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Le libellé étendu associé au code.
   */
  public String getLibelleEtendu() {
    return libelleEtendu;
  }
  
  /**
   * Retourne le code associé dans uen chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypePrixAchat valueOfByCode(Character pCode) {
    for (EnumTypePrixAchat value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de l'adresse fournisseur est invalide : " + pCode);
  }
}
