/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.chantier;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Trace;

/**
 * Chantier.
 * 
 * Un chantier permet d'associer des conditions préférentielles à des articles pour un client. Cela permet de définir un
 * prix de revient
 * et un prix de vente spécifique pour une quantité donnée. Le chantier peut être borné par une date de début et une
 * date de fin.
 * Les chantiers sont réservés aux clients en compte (professionnel ou particulier). Si un client comptant souhaite
 * avoir des
 * conditions chantier, il faudra lui créer un compte.
 */
public class Chantier extends AbstractClasseMetier<IdChantier> {
  // Variables
  private IdMagasin idMagasin = null;
  private IdClient idClient = null;
  private EnumStatutChantier statutChantier = null;
  private String libelle;
  private String referenceCourte;
  private Adresse adresse = new Adresse();
  private Date dateCreation = null;
  private Date dateValidation = null;
  private Date dateDebutValidite = null;
  private Date dateFinValidite = null;
  private boolean bloque = false;
  private boolean verrouille = false;
  
  /**
   * Constructeur.
   */
  public Chantier(IdChantier pIdChantier) {
    super(pIdChantier);
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    if (libelle != null) {
      return libelle;
    }
    else {
      return id.getTexte();
    }
  }
  
  /**
   * Retourne si le chantier est actif.
   * On controle sont état, ses dates de début et de fin.
   */
  public boolean isActif() {
    // On contôle le status
    if (statutChantier.equals(EnumStatutChantier.ATTENTE)) {
      return false;
    }
    // On contrôle les dates
    Date dateDebut = dateDebutValidite;
    Date dateFin = dateFinValidite;
    
    // Récupérer la date du joru en enlevant l'heure
    Date maintenant = new Date();
    try {
      DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
      maintenant = formatter.parse(formatter.format(maintenant));
    }
    catch (Exception e) {
      Trace.erreur("Erreur pour convertir la date du jour : " + maintenant);
    }
    
    if ((dateDebut != null) && (maintenant.before(dateDebut))) {
      return false;
    }
    if ((dateFin != null) && (maintenant.after(dateFin))) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public Chantier clone() throws CloneNotSupportedException {
    Chantier o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (Chantier) super.clone();
      o.setDateCreation(dateCreation);
      o.setDateValidation(dateValidation);
      o.setStatutChantier(statutChantier);
      o.setAdresse(adresse);
      o.setIdClient(idClient);
      o.setReferenceCourte(referenceCourte);
      o.setIdMagasin(idMagasin);
      o.setLibelle(libelle);
      o.setDateDebutValidite(dateDebutValidite);
      o.setDateFinValidite(dateFinValidite);
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Accesseurs
  
  /**
   * Identifiant du magasin concerné par le chantier.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier l'identifiant du magasin concerné par le chantier.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  /**
   * Identifiant du client concerné par le chantier.
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Modifier l'identifiant du client concerné par le chantier.
   */
  public void setIdClient(IdClient pIdClientFacture) {
    idClient = pIdClientFacture;
  }
  
  /**
   * Statut du chantier.
   */
  public EnumStatutChantier getStatutChantier() {
    return statutChantier;
  }
  
  /**
   * Modifier le statut du chantier.
   */
  public void setStatutChantier(EnumStatutChantier pStatutChantier) {
    statutChantier = pStatutChantier;
    if (!pStatutChantier.equals(EnumStatutChantier.ATTENTE) && !isActif()) {
      statutChantier = EnumStatutChantier.VALIDITE_DEPASSEE;
    }
  }
  
  /**
   * Libellé du chantier.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé du chantier.
   */
  public void setLibelle(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Référence courte du chantier.
   */
  public String getReferenceCourte() {
    return referenceCourte;
  }
  
  /**
   * Modifier la référence courte du chantier.
   */
  public void setReferenceCourte(String pReferenceCourte) {
    referenceCourte = pReferenceCourte;
  }
  
  /**
   * Adresse du chantier.
   */
  public Adresse getAdresse() {
    return adresse;
  }
  
  /**
   * Modifier l'adresse du chantier.
   */
  public void setAdresse(Adresse pAdresse) {
    adresse = pAdresse;
  }
  
  /**
   * Date de création du chantier.
   */
  public Date getDateCreation() {
    return dateCreation;
  }
  
  /**
   * Modifier la date de création du chantier.
   */
  public void setDateCreation(Date pDateCreation) {
    dateCreation = pDateCreation;
  }
  
  /**
   * Date de validation du chantier.
   */
  public Date getDateValidation() {
    return dateValidation;
  }
  
  /**
   * Modifier la date de validation du chantier.
   */
  public void setDateValidation(Date pDateValidation) {
    dateValidation = pDateValidation;
  }
  
  /**
   * Date de début de validité du chantier.
   */
  public Date getDateDebutValidite() {
    return dateDebutValidite;
  }
  
  /**
   * Modifier la date de début de validité du chantier.
   */
  public void setDateDebutValidite(Date pDateDebutValidite) {
    dateDebutValidite = pDateDebutValidite;
  }
  
  /**
   * Date de fin de validité du chantier.
   */
  public Date getDateFinValidite() {
    return dateFinValidite;
  }
  
  /**
   * Modifier la date de fin de validité du chantier.
   */
  public void setDateFinValidite(Date pDateValidite) {
    dateFinValidite = pDateValidite;
  }
  
  /**
   * Valeur "bloqué" pour un chantier.
   */
  public boolean isBloque() {
    return bloque;
  }
  
  /**
   * Modifier la valeur "bloqué" pour un chantier.
   */
  public void setBloque(boolean pIsBloque) {
    bloque = pIsBloque;
  }
  
  /**
   * Renvoyer la valeur "verrouillé" pour un chantier.
   */
  public boolean isVerrouille() {
    return verrouille;
  }
  
  /**
   * Modifier la valeur "verrouillé" pour un chantier.
   */
  public void setVerrouille(boolean pIsVerrouille) {
    verrouille = pIsVerrouille;
  }
}
