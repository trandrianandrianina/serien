/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Différents type de zones ERP dans la configuration d'un catalogue fournisseur
 */
public enum EnumTypeZoneERP {
  TYPE_ALPHANUMERIQUE("A", "Alphanumérique"),
  TYPE_NUMERIQUE("N", "Numérique"),
  TYPE_DATE("D", "Date");
  
  private final String code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeZoneERP(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeZoneERP valueOfByCode(String pCode) {
    for (EnumTypeZoneERP value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code du type de zone est invalide : " + pCode);
  }
  
}
