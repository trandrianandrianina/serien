/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.sipe;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un plan de pose Sipe.
 * 
 * L'identifiant est composé d'un numéro unique.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdPlanPoseSipe extends AbstractId {
  // Constantes
  public static final int LONGUEUR_NUMERO = 10;
  
  // Variables
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdPlanPoseSipe(EnumEtatObjetMetier pEnumEtatObjetMetier, Integer pNumero) {
    super(pEnumEtatObjetMetier);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   * Le numéro et n'est pas à fournir. Il est renseigné avec la valeur 0 dans ce cas.
   */
  private IdPlanPoseSipe() {
    super(EnumEtatObjetMetier.CREE);
    numero = 0;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdPlanPoseSipe getInstance(Integer pNumero) {
    return new IdPlanPoseSipe(EnumEtatObjetMetier.MODIFIE, pNumero);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdPlanPoseSipe getInstanceAvecCreationId() {
    return new IdPlanPoseSipe();
  }
  
  /**
   * Contrôler la validité du numéro du plan de pose Sipe.
   * Il doit être supérieur à zéro et doit comporter au maximum 10 chiffres (entre 1 et 9 999 999 999).
   */
  public static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du plan de pose Sipe n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du plan de pose Sipe est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du plan de pose Sipe est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdPlanPoseSipe controlerId(IdPlanPoseSipe pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du plan de pose Sipe est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du plan de pose Sipe n'existe pas dans la base de données : " + pId.toString());
    }
    return pId;
  }
  
  // -- Méthodes publiques
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdPlanPoseSipe)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de plans de pose Sipe.");
    }
    IdPlanPoseSipe id = (IdPlanPoseSipe) pObject;
    return numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdPlanPoseSipe)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdPlanPoseSipe id = (IdPlanPoseSipe) pObject;
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return "" + numero;
  }
  
  // -- Accesseurs
  
  /**
   * Numéro du plan de pose Sipe.
   * Le numéro du plan de pose Sipe est compris entre entre 0 et 9 999 999 999.
   */
  public Integer getNumero() {
    return numero;
  }
  
}
