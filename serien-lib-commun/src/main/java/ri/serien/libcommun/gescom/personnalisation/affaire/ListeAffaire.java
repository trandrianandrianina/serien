/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.affaire;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste des affaires.
 * L'utilisation de cette classe est à privilégiée dès qu'on manipule une liste d'affaire. Elle contient toutes les
 * méthodes oeuvrant
 * sur la liste tandis que la classe Affaires contient les méthodes oeuvrant sur uns seul Affaire.
 */
public class ListeAffaire extends ListeClasseMetier<IdAffaire, Affaire, ListeAffaire> {
  
  /**
   * Charger toute les affaires suivant une liste d'IdAffaire.
   */
  @Override
  public ListeAffaire charger(IdSession pIdSession, List<IdAffaire> pListeId) {
    return null;
  }
  
  /**
   * Charger les affaires suivant l'établissement.
   */
  @Override
  public ListeAffaire charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas défini.");
    }
    CriteresRechercheAffaires criteres = new CriteresRechercheAffaires();
    criteres.setTypeRecherche(CriteresRechercheAffaires.RECHERCHE_AFFAIRE);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    ListeAffaire listeAffaire = ManagerServiceParametre.chargerListeAffaire(pIdSession, criteres);
    return listeAffaire;
  }
  
}
