/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Etat de la gestion WORKFLOW de l'article dans le GVC
 */
public enum EnumEtatWorkFlow {
  TOUS(-1, "Tous"),
  NOUVEAU(0, "Nouveau"),
  A_MODIFIER(1, "A modifier"),
  TERMINE(2, "Terminé"),
  SUPPRIME(3, "Supprimé");

  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumEtatWorkFlow(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumEtatWorkFlow valueOfByCode(Integer pCode) {
    for (EnumEtatWorkFlow value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'état de la gestion workflow est invalide : " + pCode);
  }
}
