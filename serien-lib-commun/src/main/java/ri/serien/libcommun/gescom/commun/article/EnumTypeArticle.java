/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de l'article
 * 
 * L'information est stockée dans plusieurs champs en base qui sont regroupés de manière compréhensible dans l'objet métier Article
 * Article spécial -> code spécial == 1 (A1SPE)
 * Article commentaire -> pas de famille (A1FAM), top ne plus utilisé == 0 (A1NPU), zoneABC différente de R (A1ABC)
 * Article palette -> top spécifique == 0 (A1TSP)
 * Article transport -> top spécifique == T (A1TSP)
 * Article non stocké -> WA1STK == 1
 * Article standard -> tout ce qui n'est pas les 5 autres.
 */
public enum EnumTypeArticle {
  STANDARD(0, "Standard", "ART"),
  SPECIAL(1, "Spécial", "SPE"),
  COMMENTAIRE(2, "Commentaire", "COM"),
  TRANSPORT(3, "Transport", "TRS"),
  PALETTE(4, "Palette", "PAL"),
  REMISE(5, "Remise", "REM"),
  PLUS_UTILISE(6, "Ne plus utiliser", ""),
  NON_STOCKE(7, "Non stocké", ""),
  SERIE(8, "Géré par numéro de série", "SER");
  
  private final Integer code;
  private final String libelle;
  private final String codeAlpha;
  
  /**
   * Constructeur.
   */
  EnumTypeArticle(Integer pCode, String pLibelle, String pCodeAlpha) {
    code = pCode;
    libelle = pLibelle;
    codeAlpha = pCodeAlpha;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCodeAlpha() {
    return codeAlpha;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle + " (" + code + ")";
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeArticle valueOfByCode(Integer pCode) {
    for (EnumTypeArticle value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de l'article est invalide : " + pCode);
  }
  
  /**
   * Retourner l'objet énum par son code alphabétique.
   */
  static public EnumTypeArticle valueOfByCodeAlpha(String pCodeAlpha) {
    for (EnumTypeArticle value : values()) {
      if (pCodeAlpha.equals(value.getCodeAlpha())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de l'article est invalide : " + pCodeAlpha);
  }
}
