/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TarifArticle implements Serializable {
  // Constantes
  public static final int NOMBRE_COLONNES_MODE_CLASSIQUE = 10;
  public static final int NOMBRE_COLONNES_MODE_NEGOCE = 6;
  public static final int COLONNE_TARIF_PUBLIC = 1;
  
  public static final int COLONNE_CONSIGNATION = 1;
  public static final int COLONNE_DECONSIGNATION = 2;
  
  // Variables fichiers
  private IdArticle idArticle = null; // Code article
  private String codeDevise = "";// Devise du tarif
  private Date dateTarif = null; // Date d'application du tarif
  private String typeFacturation = ""; // Type de facturation si TTC
  private BigDecimal coefficientTarif01 = BigDecimal.ZERO; // Coeff. tarifs 1 N.U.
  private BigDecimal coefficientTarif02 = BigDecimal.ZERO; // Coeff. tarifs 2
  private BigDecimal coefficientTarif03 = BigDecimal.ZERO; // Coeff. tarifs 3
  private BigDecimal coefficientTarif04 = BigDecimal.ZERO; // Coeff. tarifs 4
  private BigDecimal coefficientTarif05 = BigDecimal.ZERO; // Coeff. tarifs 5
  private BigDecimal coefficientTarif06 = BigDecimal.ZERO; // Coeff. tarifs 6
  private BigDecimal coefficientTarif07 = BigDecimal.ZERO; // Coeff. tarifs 7
  private BigDecimal coefficientTarif08 = BigDecimal.ZERO; // Coeff. tarifs 8
  private BigDecimal coefficientTarif09 = BigDecimal.ZERO; // Coeff. tarifs 9
  private BigDecimal coefficientTarif10 = BigDecimal.ZERO; // Coeff. tarifs 10
  private BigDecimal prixVente01 = BigDecimal.ZERO; // Prix de vente 1
  private BigDecimal prixVente02 = BigDecimal.ZERO; // Prix de vente 2
  private BigDecimal prixVente03 = BigDecimal.ZERO; // Prix de vente 3
  private BigDecimal prixVente04 = BigDecimal.ZERO; // Prix de vente 4
  private BigDecimal prixVente05 = BigDecimal.ZERO; // Prix de vente 5
  private BigDecimal prixVente06 = BigDecimal.ZERO; // Prix de vente 6
  private BigDecimal prixVente07 = BigDecimal.ZERO; // Prix de vente 7
  private BigDecimal prixVente08 = BigDecimal.ZERO; // Prix de vente 8
  private BigDecimal prixVente09 = BigDecimal.ZERO; // Prix de vente 9
  private BigDecimal prixVente10 = BigDecimal.ZERO; // Prix de vente 10
  private Integer codeArrondi = 0; // Code d'arrondi
  private String rattachementCNV = ""; // Code de rattachement condition de vente
  private Date dateValidationTarif = null; // Date de validation du tarif
  private Character arrondiSurTTC = Character.valueOf(' '); // Arrondi sur le TTC
  private Character codeIN12 = Character.valueOf(' '); // coef/remi/HT/TTC,marge (pour l'instant on ne sait pas à quoi ça sert)
  private String initialesValideurTarif = ""; // Initiales de celui qui a validé le tarif
  
  private String critereAnalyse1 = ""; // Critères d'analyse 1
  private String critereAnalyse2 = ""; // Critères d'analyse 2
  private String critereAnalyse3 = ""; // Critères d'analyse 3
  private String critereAnalyse4 = ""; // Critères d'analyse 4
  private String critereAnalyse5 = ""; // Critères d'analyse 5
  private String topDesignation1 = ""; // Top désignation 1
  private String topDesignation2 = ""; // Top désignation 2
  private String topDesignation3 = ""; // Top désignation 3
  private String topDesignation4 = ""; // Top désignation 4
  private String codeTarif = ""; // Code tarif de l'article
  
  private boolean ttc = false;
  
  // -- Méthodes publiques
  
  /**
   * Retourner le nombre de colonnes de tarifs maximum en focntino du mode de calcul
   * @param pModeNegoce true=mode négoce, false=mode standard.
   * @return Nombre maximum de colonnes de tarifs.
   */
  static public int getNombreColonneTarifMax(boolean pModeNegoce) {
    if (pModeNegoce) {
      return NOMBRE_COLONNES_MODE_NEGOCE;
    }
    else {
      return NOMBRE_COLONNES_MODE_CLASSIQUE;
    }
  }
  
  /**
   * Retroune le prix d'une colonne donnée.
   */
  public BigDecimal retournerPrix(int pColonne) {
    switch (pColonne) {
      case 1:
        return prixVente01;
      case 2:
        return prixVente02;
      case 3:
        return prixVente03;
      case 4:
        return prixVente04;
      case 5:
        return prixVente05;
      case 6:
        return prixVente06;
      case 7:
        return prixVente07;
      case 8:
        return prixVente08;
      case 9:
        return prixVente09;
      case 10:
        return prixVente10;
    }
    return BigDecimal.ZERO;
  }
  
  /**
   * Retroune le coefficient d'une colonne donnée.
   */
  public BigDecimal retournerCoefficient(int pColonne) {
    switch (pColonne) {
      case 1:
        return coefficientTarif01;
      case 2:
        return coefficientTarif02;
      case 3:
        return coefficientTarif03;
      case 4:
        return coefficientTarif04;
      case 5:
        return coefficientTarif05;
      case 6:
        return coefficientTarif06;
      case 7:
        return coefficientTarif07;
      case 8:
        return coefficientTarif08;
      case 9:
        return coefficientTarif09;
      case 10:
        return coefficientTarif10;
    }
    return BigDecimal.ZERO;
  }
  
  // -- Accesseurs
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  public String getCodeDevise() {
    return codeDevise;
  }
  
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  public Date getDateTarif() {
    return dateTarif;
  }
  
  public void setDateTarif(Date dateTarif) {
    this.dateTarif = dateTarif;
  }
  
  public String getTypeFacturation() {
    return typeFacturation;
  }
  
  public void setTypeFacturation(String typeFacturation) {
    this.typeFacturation = typeFacturation;
  }
  
  public BigDecimal getCoefficientTarif01() {
    return coefficientTarif01;
  }
  
  public void setCoefficientTarif01(BigDecimal coefficientTarif01) {
    this.coefficientTarif01 = coefficientTarif01;
  }
  
  public BigDecimal getCoefficientTarif02() {
    return coefficientTarif02;
  }
  
  public void setCoefficientTarif02(BigDecimal coefficientTarif02) {
    this.coefficientTarif02 = coefficientTarif02;
  }
  
  public BigDecimal getCoefficientTarif03() {
    return coefficientTarif03;
  }
  
  public void setCoefficientTarif03(BigDecimal coefficientTarif03) {
    this.coefficientTarif03 = coefficientTarif03;
  }
  
  public BigDecimal getCoefficientTarif04() {
    return coefficientTarif04;
  }
  
  public void setCoefficientTarif04(BigDecimal coefficientTarif04) {
    this.coefficientTarif04 = coefficientTarif04;
  }
  
  public BigDecimal getCoefficientTarif05() {
    return coefficientTarif05;
  }
  
  public void setCoefficientTarif05(BigDecimal coefficientTarif05) {
    this.coefficientTarif05 = coefficientTarif05;
  }
  
  public BigDecimal getCoefficientTarif06() {
    return coefficientTarif06;
  }
  
  public void setCoefficientTarif06(BigDecimal coefficientTarif06) {
    this.coefficientTarif06 = coefficientTarif06;
  }
  
  public BigDecimal getCoefficientTarif07() {
    return coefficientTarif07;
  }
  
  public void setCoefficientTarif07(BigDecimal coefficientTarif07) {
    this.coefficientTarif07 = coefficientTarif07;
  }
  
  public BigDecimal getCoefficientTarif08() {
    return coefficientTarif08;
  }
  
  public void setCoefficientTarif08(BigDecimal coefficientTarif08) {
    this.coefficientTarif08 = coefficientTarif08;
  }
  
  public BigDecimal getCoefficientTarif09() {
    return coefficientTarif09;
  }
  
  public void setCoefficientTarif09(BigDecimal coefficientTarif09) {
    this.coefficientTarif09 = coefficientTarif09;
  }
  
  public BigDecimal getCoefficientTarif10() {
    return coefficientTarif10;
  }
  
  public void setCoefficientTarif10(BigDecimal coefficientTarif10) {
    this.coefficientTarif10 = coefficientTarif10;
  }
  
  public BigDecimal getPrixVente01() {
    return prixVente01;
  }
  
  public void setPrixVente01(BigDecimal prixVente01) {
    this.prixVente01 = prixVente01;
  }
  
  public BigDecimal getPrixVente02() {
    return prixVente02;
  }
  
  public void setPrixVente02(BigDecimal prixVente02) {
    this.prixVente02 = prixVente02;
  }
  
  public BigDecimal getPrixVente03() {
    return prixVente03;
  }
  
  public void setPrixVente03(BigDecimal prixVente03) {
    this.prixVente03 = prixVente03;
  }
  
  public BigDecimal getPrixVente04() {
    return prixVente04;
  }
  
  public void setPrixVente04(BigDecimal prixVente04) {
    this.prixVente04 = prixVente04;
  }
  
  public BigDecimal getPrixVente05() {
    return prixVente05;
  }
  
  public void setPrixVente05(BigDecimal prixVente05) {
    this.prixVente05 = prixVente05;
  }
  
  public BigDecimal getPrixVente06() {
    return prixVente06;
  }
  
  public void setPrixVente06(BigDecimal prixVente06) {
    this.prixVente06 = prixVente06;
  }
  
  public BigDecimal getPrixVente07() {
    return prixVente07;
  }
  
  public void setPrixVente07(BigDecimal prixVente07) {
    this.prixVente07 = prixVente07;
  }
  
  public BigDecimal getPrixVente08() {
    return prixVente08;
  }
  
  public void setPrixVente08(BigDecimal prixVente08) {
    this.prixVente08 = prixVente08;
  }
  
  public BigDecimal getPrixVente09() {
    return prixVente09;
  }
  
  public void setPrixVente09(BigDecimal prixVente09) {
    this.prixVente09 = prixVente09;
  }
  
  public BigDecimal getPrixVente10() {
    return prixVente10;
  }
  
  public void setPrixVente10(BigDecimal prixVente10) {
    this.prixVente10 = prixVente10;
  }
  
  public Integer getCodeArrondi() {
    return codeArrondi;
  }
  
  public void setCodeArrondi(Integer codeArrondi) {
    this.codeArrondi = codeArrondi;
  }
  
  public String getRattachementCNV() {
    return rattachementCNV;
  }
  
  public void setRattachementCNV(String rattachementCNV) {
    this.rattachementCNV = rattachementCNV;
  }
  
  public Date getDateValidationTarif() {
    return dateValidationTarif;
  }
  
  public void setDateValidationTarif(Date dateValidationTarif) {
    this.dateValidationTarif = dateValidationTarif;
  }
  
  public Character getArrondiSurTTC() {
    return arrondiSurTTC;
  }
  
  public void setArrondiSurTTC(Character arrondiSurTTC) {
    this.arrondiSurTTC = arrondiSurTTC;
  }
  
  public Character getCodeIN12() {
    return codeIN12;
  }
  
  public void setCodeIN12(Character codeIN12) {
    this.codeIN12 = codeIN12;
  }
  
  public String getInitialesValideurTarif() {
    return initialesValideurTarif;
  }
  
  public void setInitialesValideurTarif(String initialesValideurTarif) {
    this.initialesValideurTarif = initialesValideurTarif;
  }
  
  public String getCritereAnalyse1() {
    return critereAnalyse1;
  }
  
  public void setCritereAnalyse1(String critereAnalyse1) {
    this.critereAnalyse1 = critereAnalyse1;
  }
  
  public String getCritereAnalyse2() {
    return critereAnalyse2;
  }
  
  public void setCritereAnalyse2(String critereAnalyse2) {
    this.critereAnalyse2 = critereAnalyse2;
  }
  
  public String getCritereAnalyse3() {
    return critereAnalyse3;
  }
  
  public void setCritereAnalyse3(String critereAnalyse3) {
    this.critereAnalyse3 = critereAnalyse3;
  }
  
  public String getCritereAnalyse4() {
    return critereAnalyse4;
  }
  
  public void setCritereAnalyse4(String critereAnalyse4) {
    this.critereAnalyse4 = critereAnalyse4;
  }
  
  public String getCritereAnalyse5() {
    return critereAnalyse5;
  }
  
  public void setCritereAnalyse5(String critereAnalyse5) {
    this.critereAnalyse5 = critereAnalyse5;
  }
  
  public String getTopDesignation1() {
    return topDesignation1;
  }
  
  public void setTopDesignation1(String topDesignation1) {
    this.topDesignation1 = topDesignation1;
  }
  
  public String getTopDesignation2() {
    return topDesignation2;
  }
  
  public void setTopDesignation2(String topDesignation2) {
    this.topDesignation2 = topDesignation2;
  }
  
  public String getTopDesignation3() {
    return topDesignation3;
  }
  
  public void setTopDesignation3(String topDesignation3) {
    this.topDesignation3 = topDesignation3;
  }
  
  public String getTopDesignation4() {
    return topDesignation4;
  }
  
  public void setTopDesignation4(String topDesignation4) {
    this.topDesignation4 = topDesignation4;
  }
  
  public String getCodeTarif() {
    return codeTarif;
  }
  
  public void setCodeTarif(String codeTarif) {
    this.codeTarif = codeTarif;
  }
  
  public boolean isTTC() {
    return ttc;
  }
  
  public void setTTC(boolean ttc) {
    this.ttc = ttc;
  }
}
