/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.formuleprix;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une formule de prix.
 *
 * L'identifiant est composé d'un code et est lié à l'établissement.
 * Il est constitué de 5 caractères et se présente sous la forme AAAAA
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique
 * facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdFormulePrix extends AbstractIdAvecEtablissement {
  // Constantes
  private static final int LONGUEUR = 5;
  
  // Variables
  private final String code;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdFormulePrix(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCodeFormulePrix) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCodeFormulePrix);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdFormulePrix getInstance(IdEtablissement pIdEtablissement, String pCodeFormulePrix) {
    return new IdFormulePrix(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCodeFormulePrix);
  }
  
  /**
   * Contrôler la validité du code.
   * Le code de la formule de prix est une chaîne numérique de 5 caractères.
   */
  private static String controlerCode(String pCodeFormulePrix) {
    if (pCodeFormulePrix == null) {
      throw new MessageErreurException("Le code de la formule prix n'est pas renseigné.");
    }
    String valeur = pCodeFormulePrix.trim();
    if (valeur.length() > LONGUEUR) {
      throw new MessageErreurException("Le code de la formule prix doit avoir entre 1 et " + LONGUEUR + " caractères" + valeur);
    }
    return valeur;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdFormulePrix)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux codes de formules prix.");
    }
    IdFormulePrix id = (IdFormulePrix) pObject;
    return code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdFormulePrix)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdFormulePrix id = (IdFormulePrix) pObject;
    return code.compareTo(id.code);
  }
  
  /**
   * Retourner le texte de l'identifiant.
   * @param Le texte.
   */
  @Override
  public String getTexte() {
    return code;
  }
  
  /**
   * Retourner le code de la formule de prix.
   * @return Le code.
   */
  public String getCode() {
    return code;
  }
  
}
