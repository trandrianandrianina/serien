/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.formuleprix;

import java.math.BigDecimal;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe contient toutes les données décrivant une formule de prix.
 */
public class FormulePrix extends AbstractClasseMetier<IdFormulePrix> {
  // Variables
  private String libelle = null;
  private List<LigneFormulePrix> listeLigneFormulePrix = null;
  
  /**
   * Constructeur
   */
  public FormulePrix(IdFormulePrix pId) {
    super(pId);
  }
  
  // -- Méthodes publiques
  
  /**
   * Calcule un prix à partir de la formule.
   * 
   * @return
   */
  public BigDecimal calculerPrix(BigDecimal pPrixBase) {
    if (listeLigneFormulePrix == null || listeLigneFormulePrix.isEmpty()) {
      Trace.alerte("Aucune formule de prix dipsonible.");
      return null;
    }
    
    // Les lignes de la formules sont traitées les une après les autres de manière indépendante
    BigDecimal prix = pPrixBase;
    for (LigneFormulePrix ligneFormulePrix : listeLigneFormulePrix) {
      BigDecimal montantLigne = ligneFormulePrix.calculLigne(pPrixBase);
      if (montantLigne != null) {
        prix = prix.add(montantLigne);
      }
    }
    
    return prix;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le texte qui représente l'objet.
   */
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Retourne le libelle.
   * 
   * @return
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Initialise le libellé.
   * 
   * @param pLibelle
   */
  public void setLibelle(String pLibelle) {
    this.libelle = pLibelle;
  }
  
  /**
   * Retourne une liste de ligne pour la formule de prix.
   * 
   * @return
   */
  public List<LigneFormulePrix> getListeLigneFormulePrix() {
    return listeLigneFormulePrix;
  }
  
  /**
   * Initialise une liste de ligne pour la formule de prix.
   * 
   * @param pListeLigneFormulePrix
   */
  public void setListeLigneFormulePrix(List<LigneFormulePrix> pListeLigneFormulePrix) {
    this.listeLigneFormulePrix = pListeLigneFormulePrix;
  }
  
}
