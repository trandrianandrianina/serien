/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.recherche.CritereAvecEtablissement;
import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEtatDocument;
import ri.serien.libcommun.gescom.achat.document.EnumTypeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

/**
 * Critères de recherche des LigneAchat
 */
public class CritereLigneAchat extends CritereAvecEtablissement {
  // Variables
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  private List<EnumTypeDocumentAchat> listeTypeDocument = new ArrayList<EnumTypeDocumentAchat>();
  private List<EnumCodeEtatDocument> listeCodeEtat = new ArrayList<EnumCodeEtatDocument>();
  private IdMagasin idMagasin = null;
  private IdLigneAchat idLigneAchat = null;
  private IdDocumentAchat idDocumentAchat = null;
  private Integer numeroDocumentAchat = null;
  private IdArticle idArticle = null;
  private IdFournisseur idFournisseur = null;
  private boolean rechercherReception = false;
  private boolean exclureLigneSupprimee = true;
  private boolean exclureLigneCommentaire = false;
  private boolean exclureLigneReceptionnee = false;
  private Date dateCreationDebut = null;
  private Date dateCreationFin = null;
  private boolean lignesOrigine = false;
  
  // -- Méthodes publiques
  
  /**
   * Supprime le contenu de la liste des types de document.
   */
  public void viderListeTypeDocument() {
    listeTypeDocument.clear();
  }
  
  /**
   * Ajoute un type de document d'achat à la liste.
   */
  public void ajouterTypeDocumentAchat(EnumTypeDocumentAchat pTypeDocumentAchat) {
    if (pTypeDocumentAchat == null) {
      return;
    }
    if (!listeTypeDocument.contains(pTypeDocumentAchat)) {
      listeTypeDocument.add(pTypeDocumentAchat);
    }
  }
  
  /**
   * Supprime tout le contenu de la liste des codes états.
   */
  public void viderListeCodeEtat() {
    listeCodeEtat.clear();
  }
  
  /**
   * Ajoute un code états à la liste.
   */
  public void ajouterCodeEtat(EnumCodeEtatDocument pCodeEtat) {
    if (pCodeEtat == null) {
      return;
    }
    if (!listeCodeEtat.contains(pCodeEtat)) {
      listeCodeEtat.add(pCodeEtat);
    }
  }
  
  /**
   * Ajoute une list de codes états à la liste.
   */
  public void ajouterCodeEtat(EnumCodeEtatDocument... pListeCodeEtat) {
    if (pListeCodeEtat == null) {
      return;
    }
    for (EnumCodeEtatDocument codeEtat : pListeCodeEtat) {
      ajouterCodeEtat(codeEtat);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Texte de la recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void setTexteRechercheGenerique(String pTexteRecherche) {
    rechercheGenerique.setTexte(pTexteRecherche);
  }
  
  /**
   * Retourner la liste des types de documents d'achats.
   * 
   * @return List<EnumTypeDocumentAchat>
   */
  public List<EnumTypeDocumentAchat> getListeTypeDocument() {
    return listeTypeDocument;
  }
  
  /**
   * Retourner la liste des codes état de documents d'achats.
   * 
   * @return List<EnumCodeEtatDocument>
   */
  public List<EnumCodeEtatDocument> getListeCodeEtat() {
    return listeCodeEtat;
  }
  
  /**
   * Retourner l'identifiant de la ligne d'achats recherchée.
   * 
   * @return IdLigneAchat
   */
  public IdLigneAchat getIdLigneAchat() {
    return idLigneAchat;
  }
  
  /**
   * Modifier l'identifiant de la ligne d'achats recherchée.
   */
  public void setIdLigneAchat(IdLigneAchat idLigneAchat) {
    this.idLigneAchat = idLigneAchat;
  }
  
  /**
   * Retourner l'identifiant du document pour lequel on recherche des lignes d'achats.
   * 
   * @return IdDocumentAchat
   */
  public IdDocumentAchat getIdDocumentAchat() {
    return idDocumentAchat;
  }
  
  /**
   * Modifier l'identifiant du document pour lequel on recherche des lignes d'achats.
   * 
   */
  public void setIdDocumentAchat(IdDocumentAchat idDocumentAchat) {
    this.idDocumentAchat = idDocumentAchat;
  }
  
  /**
   * Retourner le numéro du document pour lequel on recherche des lignes d'achats.
   * 
   * @return Integer
   */
  public Integer getNumeroDocumentAchat() {
    return numeroDocumentAchat;
  }
  
  /**
   * Modifier le numéro du document pour lequel on recherche des lignes d'achats.
   */
  public void setNumeroDocumentAchat(Integer pNumeroDocumentAchat) {
    numeroDocumentAchat = pNumeroDocumentAchat;
  }
  
  /**
   * Retourner l'identifiant de l'article de la ligne d'achats recherchée.
   * 
   * @return idArticle
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Modifier l'identifiant de l'article de la ligne d'achats recherchée.
   * 
   */
  public void setIdArticle(IdArticle idArticle) {
    this.idArticle = idArticle;
  }
  
  /**
   * Retourner l'identifiant du fournisseur de la ligne d'achats recherchée.
   * 
   * @return IdFournisseur
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Modifier l'identifiant du fournisseur de la ligne d'achats recherchée.
   * 
   */
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
  
  /**
   * Retourner si l'on recherche ou pas une ligne réceptionnée.
   * 
   * @return boolean
   */
  public boolean isRechercherReception() {
    return rechercherReception;
  }
  
  /**
   * Modifier si l'on recherche ou pas une ligne réceptionnée.
   * 
   */
  public void setRechercherReception(Boolean rechercherReception) {
    this.rechercherReception = rechercherReception;
  }
  
  /**
   * Retourner si l'on exclue ou pas les lignes suprimées dans la recherche.
   * 
   * @return boolean
   */
  public boolean isExclureLigneSupprimee() {
    return exclureLigneSupprimee;
  }
  
  /**
   * Modifier si l'on exclue ou pas les lignes suprimées dans la recherche.
   * 
   */
  public void setExclureLigneSupprimee(Boolean exclureLigneSupprimee) {
    this.exclureLigneSupprimee = exclureLigneSupprimee;
  }
  
  /**
   * Retourner si l'on exclue ou pas les lignes commentaires dans la recherche.
   * 
   * @return boolean
   */
  public boolean isExclureLigneCommentaire() {
    return exclureLigneCommentaire;
  }
  
  /**
   * Modifier si l'on exclue ou pas les lignes commentaires dans la recherche.
   * 
   */
  public void setExclureLigneCommentaire(boolean exclureLigneCommentaire) {
    this.exclureLigneCommentaire = exclureLigneCommentaire;
  }
  
  /**
   * Retourner si l'on exclue ou pas les lignes réceptionnées dans la recherche.
   * 
   * @return boolean
   */
  public boolean isExclureLigneReceptionnee() {
    return exclureLigneReceptionnee;
  }
  
  /**
   * Modifier si l'on exclue ou pas les lignes commentaires dans la recherche.
   * 
   */
  public void setExclureLigneReceptionnee(boolean exclureLigneReceptionnee) {
    this.exclureLigneReceptionnee = exclureLigneReceptionnee;
  }
  
  /**
   * Retourner l'identifiant du magasin de la ligne d'achats recherchée.
   * 
   * @return IdMagasin
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Retourner l'identifiant du magasin de la ligne d'achats recherchée.
   * 
   */
  public void setIdMagasin(IdMagasin idMagasin) {
    this.idMagasin = idMagasin;
  }
  
  /**
   * Retourner la date de début dans la plage de dates de création recherchée.
   * 
   * @return Date
   */
  public Date getDateCreationDebut() {
    return dateCreationDebut;
  }
  
  /**
   * Modifier la date de début dans la plage de dates de création recherchée.
   * 
   */
  public void setDateCreationDebut(Date dateCreationDebut) {
    this.dateCreationDebut = dateCreationDebut;
  }
  
  /**
   * Retourner la date de fin dans la plage de dates de création recherchée.
   * 
   * @return Date
   */
  public Date getDateCreationFin() {
    return dateCreationFin;
  }
  
  /**
   * Modifier la date de fin dans la plage de dates de création recherchée.
   * 
   */
  public void setDateCreationFin(Date dateCreationFin) {
    this.dateCreationFin = dateCreationFin;
  }
  
  /**
   * Indique si l'on recherche les lignes d'origine d'un document (avant extraction)
   */
  public boolean isLignesOrigine() {
    return lignesOrigine;
  }
  
  /**
   * Met à jour si l'on recherche les lignes d'origine d'un document (avant extraction)
   */
  public void setLignesOrigine(boolean lignesOrigine) {
    this.lignesOrigine = lignesOrigine;
  }
  
}
