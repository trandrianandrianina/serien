/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.reglement;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un mode de règlement.
 *
 * L'identifiant est composé du code établissement et du code règlement.
 * Le code règlement correspond au paramètre RG du menu des ventes ou des achats.
 * Il est constitué de 1 à 2 caractères et se présente sous la forme AX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdModeReglement extends AbstractId {
  private static final int LONGUEUR_CODE_MIN = 1;
  private static final int LONGUEUR_CODE_MAX = 2;
  
  // Ces modes de règlement sont définit en dur bien qu'ils soient en réalité paramètrable
  // Cela nécessite que ces modes de règlement soit correctement paramétré pour que cela fonctionne.
  public static final IdModeReglement CHEQUE = new IdModeReglement(EnumEtatObjetMetier.MODIFIE, "CH");
  public static final IdModeReglement CARTE_BANCAIRE = new IdModeReglement(EnumEtatObjetMetier.MODIFIE, "CB");
  public static final IdModeReglement ESPECE = new IdModeReglement(EnumEtatObjetMetier.MODIFIE, "ES");
  public static final IdModeReglement ACOMPTE = new IdModeReglement(EnumEtatObjetMetier.MODIFIE, "AC");
  public static final IdModeReglement VIREMENT = new IdModeReglement(EnumEtatObjetMetier.MODIFIE, "VI");
  public static final IdModeReglement AVOIR = new IdModeReglement(EnumEtatObjetMetier.MODIFIE, "AV");
  
  // Variables
  private final String code;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdModeReglement(EnumEtatObjetMetier pEnumEtatObjetMetier, String pCode) {
    super(pEnumEtatObjetMetier);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdModeReglement getInstance(String pCode) {
    return new IdModeReglement(EnumEtatObjetMetier.MODIFIE, pCode);
  }
  
  /**
   * Contrôler la validité du code réglement.
   * Le code réglement est une chaîne alphanumérique de 1 à 2 caractères.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code réglement n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MAX) {
      throw new MessageErreurException(
          "Le mode de réglement doit comporter entre " + LONGUEUR_CODE_MIN + " et " + LONGUEUR_CODE_MAX + " caractères : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdModeReglement controlerId(IdModeReglement pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du code réglement est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du code réglement n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // -- Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdModeReglement)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de réglement.");
    }
    IdModeReglement id = (IdModeReglement) pObject;
    return code.equals(id.getCode());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdModeReglement)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdModeReglement id = (IdModeReglement) pObject;
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
  // -- Accesseurs
  
  /**
   * Code unité.
   * Le code de l'unité est une chaîne alphanumérique de 1 à 2 caractères.
   */
  public String getCode() {
    return code;
  }
  
}
