/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.groupeconditionvente;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les modes d'application d'un groupe de conditions de ventes.
 */
public enum EnumModeApplicationConditionVente {
  NON_RENSEIGNE(' ', "Non renseigné"),
  AJOUT_CONDITION('A', "Ajout aux conditions trouvées"),
  MEILLEUR_PRIX('M', "Au Meilleur prix");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumModeApplicationConditionVente(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet Enum par son code.
   * 
   * @param pCode
   * @return
   */
  static public EnumModeApplicationConditionVente valueOfByCode(Character pCode) {
    for (EnumModeApplicationConditionVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le mode d'application du groupe de conditions de vente est invalide : " + pCode);
  }
  
}
