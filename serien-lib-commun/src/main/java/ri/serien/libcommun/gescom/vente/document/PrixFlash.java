/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.outils.Constantes;

public class PrixFlash implements Serializable, Cloneable {
  
  // Variables
  private IdClient idClient = null;
  private IdArticle idArticle = null; // code article
  private BigDecimal prixVenteFlash = BigDecimal.ZERO; // prix de vente flash
  private String codeUtilisateurFlash = ""; // code utilisateur qui crée le prix flash
  private Date datePrixFlash = null; // date du prix flash
  private int heurePrixFlash = 0; // heure du prix flash
  
  // -- Méthodes publiques
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public PrixFlash clone() {
    PrixFlash o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (PrixFlash) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Accesseurs
  
  public IdClient getIdClient() {
    return idClient;
  }
  
  public void setIdClient(IdClient pIdClient) {
    idClient = pIdClient;
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public void setIdArticle(IdArticle pIdArticle) {
    this.idArticle = pIdArticle;
  }
  
  public BigDecimal getPrixVenteFlash() {
    return prixVenteFlash;
  }
  
  public void setPrixVenteFlash(BigDecimal pPrixVenteFlash) {
    prixVenteFlash = pPrixVenteFlash;
    if (prixVenteFlash != null) {
      prixVenteFlash = prixVenteFlash.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
  }
  
  public String getCodeUtilisateurFlash() {
    return codeUtilisateurFlash;
  }
  
  public void setCodeUtilisateurFlash(String codeUtilisateurFlash) {
    this.codeUtilisateurFlash = codeUtilisateurFlash;
  }
  
  public Date getDatePrixFlash() {
    return datePrixFlash;
  }
  
  public void setDatePrixFlash(Date datePrixFlash) {
    this.datePrixFlash = datePrixFlash;
  }
  
  public int getHeurePrixFlash() {
    return heurePrixFlash;
  }
  
  public void setHeurePrixFlash(int heurePrixFlash) {
    this.heurePrixFlash = heurePrixFlash;
  }
  
}
