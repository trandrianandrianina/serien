/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockcomptoir;

import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Stock d'un article pour un magasin.
 * L'article et le magasin sont stockés dans l'identifiant.
 */
public class StockComptoir extends AbstractClasseMetier<IdStockComptoir> {
  // Variables
  private BigDecimal quantitePhysique = BigDecimal.ZERO;
  private BigDecimal quantiteCommandee = BigDecimal.ZERO;
  private BigDecimal quantiteAttendue = BigDecimal.ZERO;
  private BigDecimal quantiteAffectee = BigDecimal.ZERO;
  private BigDecimal quantiteDisponibleVente = BigDecimal.ZERO;
  private BigDecimal quantiteDisponibleAchat = BigDecimal.ZERO;
  private BigDecimal quantiteMinimum = BigDecimal.ZERO;
  private BigDecimal quantiteIdeale = BigDecimal.ZERO;
  private BigDecimal quantiteConsoMoyenne = BigDecimal.ZERO;
  private BigDecimal quantiteMax = BigDecimal.ZERO;
  private Integer nombreDecimale = null;
  
  /**
   * Constructeur avec l'identififant du stock (obligatoire).
   */
  public StockComptoir(IdStockComptoir pIdStock) {
    super(pIdStock);
  }
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  // -- Méthodes publiques
  
  /**
   * Permet de savoir si l'article est en surstock.
   * Le surstock c'est lorsque le stock physique est supérieur au stock idéal.
   */
  public boolean isArticleEnSurStock() {
    boolean enSurStock = false;
    if (quantitePhysique != null && quantiteIdeale != null && quantiteIdeale.compareTo(BigDecimal.ZERO) > 0) {
      enSurStock = quantitePhysique.compareTo(quantiteIdeale) > 0;
    }
    return enSurStock;
  }
  
  // -- Méthodes privées
  
  /**
   * Permet de mettre à jour l'ensemble des quantités suite à une mise à jour du nombre de décimales.
   */
  private void mettreAJourQuantites() {
    setQuantitePhysique(quantitePhysique);
    setQuantiteCommande(quantiteCommandee);
    setQuantiteAffecte(quantiteAffectee);
    setQuantiteAttendue(quantiteAttendue);
    setQuantiteDisponibleVente(quantiteDisponibleVente);
    setQuantiteDisponibleAchat(quantiteDisponibleAchat);
    setQuantiteMinimum(quantiteMinimum);
    setQuantiteIdeale(quantiteIdeale);
    setQuantiteConsoMoyenne(quantiteConsoMoyenne);
    setQuantiteMax(quantiteMax);
  }
  
  // -- Accesseurs
  
  /**
   * Quantité physiquement en stock.
   */
  public BigDecimal getQuantitePhysique() {
    return quantitePhysique;
  }
  
  /**
   * Modifier la quantité physiquement en stock.
   */
  public void setQuantitePhysique(BigDecimal pQuantitePhysique) {
    quantitePhysique = pQuantitePhysique;
    if (quantitePhysique != null && nombreDecimale != null) {
      quantitePhysique = quantitePhysique.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Quantité commandée par les clients.
   */
  public BigDecimal getQuantiteCommandee() {
    return quantiteCommandee;
  }
  
  /**
   * Modifier la quantité commandée par les clients.
   */
  public void setQuantiteCommande(BigDecimal pQuantiteCommandee) {
    quantiteCommandee = pQuantiteCommandee;
    if (quantiteCommandee != null && nombreDecimale != null) {
      quantiteCommandee = quantiteCommandee.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Quantité en cours de commande chez les fournisseurs.
   */
  public BigDecimal getQuantiteAttendue() {
    return quantiteAttendue;
  }
  
  /**
   * Modifier la quantité en cours de commande chez les fournisseurs.
   */
  public void setQuantiteAttendue(BigDecimal pQuantiteAttendue) {
    quantiteAttendue = pQuantiteAttendue;
    if (quantiteAttendue != null && nombreDecimale != null) {
      quantiteAttendue = quantiteAttendue.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Quantité affectée.
   */
  public BigDecimal getQuantiteAffectee() {
    return quantiteAffectee;
  }
  
  /**
   * Modifier la quantité affectée.
   */
  public void setQuantiteAffecte(BigDecimal pQuantiteAffectee) {
    quantiteAffectee = pQuantiteAffectee;
    if (quantiteAffectee != null && nombreDecimale != null) {
      quantiteAffectee = quantiteAffectee.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Quantité disponible à la vente.
   * La quantité disponible à la vente est égale à la quantité physique moins la quantité commandée plus la quantitée attendue.
   */
  public BigDecimal getQuantiteDisponibleVente() {
    return quantiteDisponibleVente;
  }
  
  /**
   * Modifier la quantité disponible à la vente.
   */
  public void setQuantiteDisponibleVente(BigDecimal pQuantiteDisponibleVente) {
    quantiteDisponibleVente = pQuantiteDisponibleVente;
    if (quantiteDisponibleVente != null && nombreDecimale != null) {
      quantiteDisponibleVente = quantiteDisponibleVente.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Quantité disponible à l'achat.
   * La quantité disponible à la vente est égale à la quantité physique moins la quantité commandée. On en compte pas la quantité attendue
   * côté achat. Par ailleurs, la quantité commandée ne prend pas en compte les commandes directes usines.
   */
  public BigDecimal getQuantiteDisponibleAchat() {
    return quantiteDisponibleAchat;
  }
  
  /**
   * Modifier la quantité disponible à l'achat.
   */
  public void setQuantiteDisponibleAchat(BigDecimal pQuantiteDisponibleAchat) {
    quantiteDisponibleAchat = pQuantiteDisponibleAchat;
    if (quantiteDisponibleAchat != null && nombreDecimale != null) {
      quantiteDisponibleAchat = quantiteDisponibleAchat.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Quantité minimum.
   */
  public BigDecimal getQuantiteMinimum() {
    return quantiteMinimum;
  }
  
  /**
   * Modifier la quantité minimum.
   */
  public void setQuantiteMinimum(BigDecimal pQuantiteMinimum) {
    quantiteMinimum = pQuantiteMinimum;
    if (quantiteMinimum != null && nombreDecimale != null) {
      quantiteMinimum = quantiteMinimum.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Quantité idéale.
   */
  public BigDecimal getQuantiteIdeale() {
    return quantiteIdeale;
  }
  
  /**
   * Modifier la quantité idéale.
   */
  public void setQuantiteIdeale(BigDecimal pQuantiteIdeale) {
    quantiteIdeale = pQuantiteIdeale;
    if (quantiteIdeale != null && nombreDecimale != null) {
      quantiteIdeale = quantiteIdeale.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Consommation moyenne mensuelle.
   */
  public BigDecimal getQuantiteConsoMoyenne() {
    return quantiteConsoMoyenne;
  }
  
  /**
   * Modifier la consommation moyenne mensuelle.
   */
  public void setQuantiteConsoMoyenne(BigDecimal pQuantiteConsoMoyenne) {
    quantiteConsoMoyenne = pQuantiteConsoMoyenne;
    if (quantiteConsoMoyenne != null && nombreDecimale != null) {
      quantiteConsoMoyenne = quantiteConsoMoyenne.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Quantité du stock maximum.
   */
  public BigDecimal getQuantiteMax() {
    return quantiteMax;
  }
  
  /**
   * Modifier la quantité du stock maximum.
   */
  public void setQuantiteMax(BigDecimal pQuantiteMax) {
    quantiteMax = pQuantiteMax;
    if (quantiteMax != null && nombreDecimale != null) {
      quantiteMax = quantiteMax.setScale(nombreDecimale, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Modifier le nombre de décimales propre à ce stock.
   * On met à jour toutes les quantités correspondantes après modification du nombre de décimales
   */
  public void setNombreDecimale(Integer pNombreDecimale) {
    if (pNombreDecimale == null) {
      return;
    }
    nombreDecimale = pNombreDecimale;
  }
  
  /**
   * Modifier le nombre de décimales propre à ce stock.
   * On met à jour toutes les quantités correspondantes après modification du nombre de décimales
   * A partir d'un BigDecimal souvent utilisées en lecture de base de données
   */
  public void setNombreDecimale(BigDecimal pNombreDecimale) {
    if (pNombreDecimale == null) {
      return;
    }
    nombreDecimale = pNombreDecimale.intValue();
  }
  
}
