/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.prix;

import java.math.BigDecimal;

import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Utiliser pour initialiser le prix d'achat à partir des informations issues de la base de données.
 */
public class ParametrePrixAchat {
  // Unités
  private IdUnite idUA = null;
  private IdUnite idUCA = null;
  private Integer nombreDecimaleUA = 2;
  private Integer nombreDecimaleUCA = 2;
  private Integer nombreDecimaleUS = 2;
  
  // Ratios
  private BigDecimal nombreUAParUCA = BigDecimal.ONE; // WKUAUCA
  private BigDecimal nombreUSParUCA = BigDecimal.ONE; // WKUSUCA
  private BigDecimal nombreUCAParUCS = BigDecimal.ONE; // WKUCAUCS
  private BigDecimal nombreUSParUCS = BigDecimal.ONE; // WKUSUCS
  
  // Quantités en reliquat
  private BigDecimal quantiteReliquatUA = BigDecimal.ZERO;
  private BigDecimal quantiteReliquatUCA = BigDecimal.ZERO;
  private BigDecimal quantiteReliquatUS = BigDecimal.ZERO;
  // Quantités initiales
  private BigDecimal quantiteInitialeUA = BigDecimal.ZERO;
  private BigDecimal quantiteInitialeUCA = BigDecimal.ZERO;
  private BigDecimal quantiteInitialeUS = BigDecimal.ZERO;
  // Quantités traitées
  private BigDecimal quantiteTraiteeUA = BigDecimal.ZERO;
  private BigDecimal quantiteTraiteeUCA = BigDecimal.ZERO;
  private BigDecimal quantiteTraiteeUS = BigDecimal.ZERO;
  
  // Montants
  private BigDecimal prixAchatBrutHT = BigDecimal.ZERO;
  private ListeRemise listeRemise = new ListeRemise();
  private BigDecimal pourcentageTaxeOuMajoration = BigDecimal.ZERO;
  private BigDecimal montantConditionnement = BigDecimal.ZERO;
  private BigDecimal prixNetHT = BigDecimal.ZERO;
  private BigDecimal prixCalculeHT = BigDecimal.ZERO;
  private BigDecimal montantAuPoidsPortHT = BigDecimal.ZERO;
  private BigDecimal poidsPort = BigDecimal.ZERO;
  private BigPercentage pourcentagePort = BigPercentage.ZERO;
  private BigDecimal montantPortHT = BigDecimal.ZERO;
  private BigDecimal prixRevientFournisseurHT = BigDecimal.ZERO;
  private BigPercentage pourcentageMajoration = BigPercentage.ZERO;
  private BigDecimal prixRevientStandardHT = BigDecimal.ZERO;
  private BigDecimal prixRevientStandardUSHT = BigDecimal.ZERO;
  private BigDecimal montantLigneHT = BigDecimal.ZERO;
  private BigDecimal montantInitialHT = BigDecimal.ZERO;
  private BigDecimal montantTraiteHT = BigDecimal.ZERO;
  
  // Indicateurs
  private Integer topPrixBaseSaisie = 0;
  private Integer topRemiseSaisie = 0;
  private Integer topPrixNetSaisi = 0;
  private Integer topUniteSaisie = 0;
  private Integer topQuantiteSaisie = 0;
  private Integer topMontantHTSaisi = 0;
  private boolean quantiteNonMultipleConditionnement = false;
  
  // Exclusion remsies de pied
  private Character[] exclusionRemisePied = new Character[PrixAchat.NOMBRE_REMISES];
  
  // TVA
  private Integer codeTVA = 0;
  private Integer colonneTVA = 0;
  
  // -- Accesseurs
  
  public Integer getTopPrixBaseSaisie() {
    return topPrixBaseSaisie;
  }
  
  public void setTopPrixBaseSaisie(Integer topPrixBaseSaisie) {
    this.topPrixBaseSaisie = topPrixBaseSaisie;
  }
  
  public Integer getTopRemiseSaisie() {
    return topRemiseSaisie;
  }
  
  public void setTopRemiseSaisie(Integer topRemiseSaisie) {
    this.topRemiseSaisie = topRemiseSaisie;
  }
  
  public Integer getTopPrixNetSaisi() {
    return topPrixNetSaisi;
  }
  
  public void setTopPrixNetSaisi(Integer topPrixNetSaisi) {
    this.topPrixNetSaisi = topPrixNetSaisi;
  }
  
  public Integer getTopUniteSaisie() {
    return topUniteSaisie;
  }
  
  public void setTopUniteSaisie(Integer topUniteSaisie) {
    this.topUniteSaisie = topUniteSaisie;
  }
  
  public Integer getTopQuantiteSaisie() {
    return topQuantiteSaisie;
  }
  
  public void setTopQuantiteSaisie(Integer topQuantiteSaisie) {
    this.topQuantiteSaisie = topQuantiteSaisie;
  }
  
  public Integer getTopMontantHTSaisi() {
    return topMontantHTSaisi;
  }
  
  public void setTopMontantHTSaisi(Integer topMontantHTSaisi) {
    this.topMontantHTSaisi = topMontantHTSaisi;
  }
  
  public BigDecimal getPrixNetHT() {
    return prixNetHT;
  }
  
  public void setPrixNetHT(BigDecimal pPrixNetHT) {
    prixNetHT = pPrixNetHT;
  }
  
  public BigDecimal getPrixCalculeHT() {
    return prixCalculeHT;
  }
  
  public void setPrixCalculeHT(BigDecimal prixCalculeHT) {
    this.prixCalculeHT = prixCalculeHT;
  }
  
  public BigDecimal getMontantLigneHT() {
    return montantLigneHT;
  }
  
  public void setMontantReliquatHT(BigDecimal pMontantLigneHT) {
    montantLigneHT = pMontantLigneHT;
  }
  
  public BigDecimal getQuantiteReliquatUA() {
    return quantiteReliquatUA;
  }
  
  public void setQuantiteReliquatUA(BigDecimal quantite) {
    this.quantiteReliquatUA = quantite;
  }
  
  public BigDecimal getQuantiteReliquatUCA() {
    return quantiteReliquatUCA;
  }
  
  public void setQuantiteReliquatUCA(BigDecimal quantiteUCA) {
    this.quantiteReliquatUCA = quantiteUCA;
  }
  
  public BigDecimal getQuantiteReliquatUS() {
    return quantiteReliquatUS;
  }
  
  public void setQuantiteReliquatUS(BigDecimal quantiteUS) {
    this.quantiteReliquatUS = quantiteUS;
  }
  
  public IdUnite getIdUA() {
    return idUA;
  }
  
  public void setIdUA(IdUnite pIdUA) {
    if (pIdUA == null) {
      this.idUA = null;
      return;
    }
    this.idUA = pIdUA;
  }
  
  public IdUnite getIdUCA() {
    return idUCA;
  }
  
  public void setIdUCA(IdUnite pIdUCA) {
    if (pIdUCA == null) {
      this.idUCA = null;
      return;
    }
    this.idUCA = pIdUCA;
  }
  
  public BigDecimal getNombreUAParUCA() {
    return nombreUAParUCA;
  }
  
  public void setNombreUAParUCA(BigDecimal nombreUAParUCA) {
    this.nombreUAParUCA = nombreUAParUCA;
  }
  
  public BigDecimal getNombreUSParUCA() {
    return nombreUSParUCA;
  }
  
  public void setNombreUSParUCA(BigDecimal nombreUSParUCA) {
    this.nombreUSParUCA = nombreUSParUCA;
  }
  
  public BigDecimal getNombreUCAParUCS() {
    return nombreUCAParUCS;
  }
  
  public void setNombreUCAParUCS(BigDecimal nombreUCAParUCS) {
    this.nombreUCAParUCS = nombreUCAParUCS;
  }
  
  public ListeRemise getListeRemise() {
    return listeRemise;
  }
  
  public void setListeRemise(ListeRemise pListeRemise) {
    listeRemise = pListeRemise;
  }
  
  public Character[] getExclusionRemisePied() {
    return exclusionRemisePied;
  }
  
  public void setExclusionRemisePied(Character[] exclusionRemisePied) {
    this.exclusionRemisePied = exclusionRemisePied;
  }
  
  public BigDecimal getPrixRevientStandardHTEnUS() {
    return prixRevientStandardUSHT;
  }
  
  public void setPrixRevientStandardHTEnUS(BigDecimal pPrixRevientStandardHTEnUS) {
    prixRevientStandardUSHT = pPrixRevientStandardHTEnUS;
  }
  
  public BigDecimal getPrixRevientStandardHT() {
    return prixRevientStandardHT;
  }
  
  public void setPrixRevientStandardHT(BigDecimal pPrixRevientStandardHT) {
    prixRevientStandardHT = pPrixRevientStandardHT;
  }
  
  public Integer getNombreDecimaleUA() {
    return nombreDecimaleUA;
  }
  
  public void setNombreDecimaleUA(Integer pNombreDecimaleUA) {
    nombreDecimaleUA = pNombreDecimaleUA;
  }
  
  public Integer getNombreDecimaleUCA() {
    return nombreDecimaleUCA;
  }
  
  public void setNombreDecimaleUCA(Integer nombreDecimaleUCA) {
    this.nombreDecimaleUCA = nombreDecimaleUCA;
  }
  
  public Integer getNombreDecimaleUS() {
    return nombreDecimaleUS;
  }
  
  public void setNombreDecimaleUS(Integer nombreDecimaleUS) {
    this.nombreDecimaleUS = nombreDecimaleUS;
  }
  
  public BigDecimal getPrixAchatBrutHT() {
    return prixAchatBrutHT;
  }
  
  public void setPrixAchatBrutHT(BigDecimal pPrixAchatBrutHT) {
    prixAchatBrutHT = pPrixAchatBrutHT;
  }
  
  public BigDecimal getPrixRevientFournisseurHT() {
    return prixRevientFournisseurHT;
  }
  
  public void setPrixRevientFournisseurHT(BigDecimal prixRevientFournisseurHT) {
    this.prixRevientFournisseurHT = prixRevientFournisseurHT;
  }
  
  public BigDecimal getMontantPortHT() {
    return montantPortHT;
  }
  
  public void setMontantPortHT(BigDecimal pMontantPortHT) {
    montantPortHT = pMontantPortHT;
  }
  
  public BigPercentage getPourcentagePort() {
    return pourcentagePort;
  }
  
  public void setPourcentagePort(BigPercentage pPourcentagePort) {
    pourcentagePort = pPourcentagePort;
  }
  
  public BigDecimal getPoidsPort() {
    return poidsPort;
  }
  
  public void setPoidsPort(BigDecimal pPoidsPort) {
    poidsPort = pPoidsPort;
  }
  
  public BigDecimal getMontantAuPoidsPortHT() {
    return montantAuPoidsPortHT;
  }
  
  public void setMontantAuPoidsPortHT(BigDecimal pMontantAuPoidsPortHT) {
    this.montantAuPoidsPortHT = pMontantAuPoidsPortHT;
  }
  
  public BigPercentage getPourcentageMajoration() {
    return pourcentageMajoration;
  }
  
  public void setPourcentageMajoration(BigPercentage pPourcentageMajoration) {
    pourcentageMajoration = pPourcentageMajoration;
  }
  
  public BigDecimal getPourcentageTaxeOuMajoration() {
    return pourcentageTaxeOuMajoration;
  }
  
  public void setPourcentageTaxeOuMajoration(BigDecimal pPourcentageTaxeOuMajoration) {
    pourcentageTaxeOuMajoration = pPourcentageTaxeOuMajoration;
  }
  
  public BigDecimal getMontantConditionnement() {
    return montantConditionnement;
  }
  
  public void setMontantConditionnement(BigDecimal pMontantConditionnement) {
    montantConditionnement = pMontantConditionnement;
  }
  
  public BigDecimal getNombreUSParUCS() {
    return nombreUSParUCS;
  }
  
  public void setNombreUSParUCS(BigDecimal nombreUSParUCS) {
    this.nombreUSParUCS = nombreUSParUCS;
  }
  
  public Integer getCodeTVA() {
    return codeTVA;
  }
  
  public void setCodeTVA(Integer pCodeTVA) {
    codeTVA = pCodeTVA;
  }
  
  public Integer getColonneTVA() {
    return colonneTVA;
  }
  
  public void setColonneTVA(Integer pColonneTVA) {
    colonneTVA = pColonneTVA;
  }
  
  public boolean isQuantiteNonMultipleConditionnement() {
    return quantiteNonMultipleConditionnement;
  }
  
  public void setQuantiteNonMultipleConditionnement(boolean quantiteNonMultipleConditionnement) {
    this.quantiteNonMultipleConditionnement = quantiteNonMultipleConditionnement;
  }
  
  public BigDecimal getQuantiteInitialeUA() {
    return quantiteInitialeUA;
  }
  
  public void setQuantiteInitialeUA(BigDecimal quantiteInitialeUA) {
    this.quantiteInitialeUA = quantiteInitialeUA;
  }
  
  public BigDecimal getQuantiteInitialeUCA() {
    return quantiteInitialeUCA;
  }
  
  public void setQuantiteInitialeUCA(BigDecimal quantiteInitialeUCA) {
    this.quantiteInitialeUCA = quantiteInitialeUCA;
  }
  
  public BigDecimal getQuantiteInitialeUS() {
    return quantiteInitialeUS;
  }
  
  public void setQuantiteInitialeUS(BigDecimal quantiteInitialeUS) {
    this.quantiteInitialeUS = quantiteInitialeUS;
  }
  
  public BigDecimal getQuantiteTraiteeUA() {
    return quantiteTraiteeUA;
  }
  
  public void setQuantiteTraiteeUA(BigDecimal quantiteTraiteeUA) {
    this.quantiteTraiteeUA = quantiteTraiteeUA;
  }
  
  public BigDecimal getQuantiteTraiteeUCA() {
    return quantiteTraiteeUCA;
  }
  
  public void setQuantiteTraiteeUCA(BigDecimal quantiteTraiteeUCA) {
    this.quantiteTraiteeUCA = quantiteTraiteeUCA;
  }
  
  public BigDecimal getQuantiteTraiteeUS() {
    return quantiteTraiteeUS;
  }
  
  public void setQuantiteTraiteeUS(BigDecimal quantiteTraiteeUS) {
    this.quantiteTraiteeUS = quantiteTraiteeUS;
  }
  
  public BigDecimal getPrixRevientStandardUSHT() {
    return prixRevientStandardUSHT;
  }
  
  public void setPrixRevientStandardUSHT(BigDecimal prixRevientStandardUSHT) {
    this.prixRevientStandardUSHT = prixRevientStandardUSHT;
  }
  
  public BigDecimal getMontantInitialHT() {
    return montantInitialHT;
  }
  
  public void setMontantInitialHT(BigDecimal montantInitialHT) {
    this.montantInitialHT = montantInitialHT;
  }
  
  public BigDecimal getMontantTraiteHT() {
    return montantTraiteHT;
  }
  
  public void setMontantTraiteHT(BigDecimal montantTraiteHT) {
    this.montantTraiteHT = montantTraiteHT;
  }
}
