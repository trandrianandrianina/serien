/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockmouvement;

import java.util.Date;

import ri.serien.libcommun.commun.id.AbstractIdAvecMagasin;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'un mouvement de stock.
 *
 * L'identifiant est composé de l'identifiant d'un article et d'un magasin car le stock est ventilé par article et par magasin.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdStockMouvement extends AbstractIdAvecMagasin {
  private IdArticle idArticle;
  private Date date = null;
  private Integer numeroOrdre = 0;
  
  /**
   * Constructeur interne.
   */
  private IdStockMouvement(EnumEtatObjetMetier pEnumEtatObjetMetier, IdMagasin pIdMagasin, IdArticle pIdArticle, Date pDate,
      Integer pNumeroOrdre) {
    super(pEnumEtatObjetMetier, pIdMagasin);
    
    // Contrôler les identifiants
    IdArticle.controlerId(pIdArticle, false);
    if (pIdMagasin != null && !pIdArticle.getIdEtablissement().equals(pIdMagasin.getIdEtablissement())) {
      throw new MessageErreurException("Stock invalide car les établissements de l'article et du magasin sont différents.");
    }
    
    // Renseigner les attributs
    idArticle = pIdArticle;
    date = pDate;
    numeroOrdre = pNumeroOrdre;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée pour un commandé.
   */
  public static IdStockMouvement getInstance(IdMagasin pIdMagasin, IdArticle pIdArticle, Date pDate, Integer pNumeroOrdre) {
    return new IdStockMouvement(EnumEtatObjetMetier.MODIFIE, pIdMagasin, pIdArticle, pDate, pNumeroOrdre);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getIdMagasin().hashCode();
    code = 37 * code + idArticle.hashCode();
    code = 37 * code + date.hashCode();
    code = 37 * code + numeroOrdre.hashCode();
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdStockMouvement)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de stock attendu/commandé.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdStockMouvement id = (IdStockMouvement) pObject;
    return getIdMagasin().equals(id.getIdMagasin()) && idArticle.equals(id.idArticle) && date.equals(id.date)
        && numeroOrdre.equals(id.numeroOrdre);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdStockMouvement)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdStockMouvement id = (IdStockMouvement) pObject;
    int comparaison = getIdMagasin().compareTo(id.getIdMagasin());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idArticle.compareTo(id.idArticle);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = date.compareTo(id.date);
    if (comparaison != 0) {
      return comparaison;
    }
    return numeroOrdre.compareTo(id.numeroOrdre);
  }
  
  @Override
  public String getTexte() {
    return getIdMagasin().toString() + SEPARATEUR_ID + idArticle.toString() + SEPARATEUR_ID + date + SEPARATEUR_ID + numeroOrdre;
  }
  
  /**
   * Identifiant de l'article associé au mouvement de stock.
   * @return Identifiant article du mouvement de stock.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Retourner la date du mouvement du stock.
   * @return Date du mouvement de stock.
   */
  public Date getDate() {
    return date;
  }
  
  /**
   * Retourner le numéro d'ordre du mouvement de stock.
   * 
   * Le numéro d'ordre numérote les opérations de la journée pour un couple magasin/article. Les entrées
   * sont numérotées de 1 à 999. Les sorties sont numérotées à partir de 1001. La numérotation repart à 1 et à 1001 au début de chaque
   * journée. Le numéro d'ordre des inventaires est égal à 0.
   * 
   * @return Numéro d'ordre.
   */
  public Integer getNumeroOrdre() {
    return numeroOrdre;
  }
}
