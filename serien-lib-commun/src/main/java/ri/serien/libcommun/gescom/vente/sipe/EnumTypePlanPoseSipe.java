/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.sipe;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les différents types de plans de pose Sipe.
 */
public enum EnumTypePlanPoseSipe {
  DALLE('1', "Dalles"),
  POUTRELLE('2', "Poutrelles");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypePlanPoseSipe(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  static public EnumTypePlanPoseSipe valueOfByCode(Character pCode) {
    for (EnumTypePlanPoseSipe value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type du plan de pose Sipe est invalide : " + pCode);
  }
}
