/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Etat d'un document de vente (hors devis).
 * Cet état détermine le niveau de progression dans le traitement d'un document qui n'est ni un devis, ni un chantier.
 * Pour déterminer la progression de traitement d'un devis ou d'un chantier, on utilise EnumEtatDevisDocumentVente
 */
public enum EnumEtatBonDocumentVente {
  ATTENTE(0, "En attente"),
  VALIDE(1, "Validé"),
  RESERVE(3, "Réservé"),
  EXPEDIE(4, "Expédié"),
  FACTURE(6, "Facturé"),
  COMPTABILISE(7, "Comptabilisé");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumEtatBonDocumentVente(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumEtatBonDocumentVente valueOfByCode(Integer pCode) {
    for (EnumEtatBonDocumentVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'état du document de vente est invalide : " + pCode);
  }
}
