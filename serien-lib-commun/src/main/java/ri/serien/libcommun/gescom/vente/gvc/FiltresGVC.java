/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.article.GroupeArticles;
import ri.serien.libcommun.gescom.commun.article.StatutArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille;

public final class FiltresGVC implements Serializable {
  
  private ListeParametresGVC listeParametresGVC = null;
  private IdEtablissement etablissementEnCours = null;
  private StatutArticle statutSNArticle = null;
  
  private GroupeArticles groupeArticle = null;
  private Famille familleArticle = null;
  private SousFamille sousFamilleArticle = null;
  private String marqueArticle = null;
  private Fournisseur fournisseurArticle = null;
  private int indexDebut = -1;
  private int indexFin = -1;
  private int numeroPage = 1;
  private boolean moinsCherAilleurs = false;
  private String saisieUtilisateur = null;
  
  // Workflow
  private String expediteurArt = null;
  private String destinataireArt = null;
  private EtatWorkFlow etatWorkFlow = null;
  private String etatWeb = null;
  
  public FiltresGVC(ListeParametresGVC pListe) {
    if (pListe != null) {
      listeParametresGVC = pListe;
    }
    else {
      listeParametresGVC = new ListeParametresGVC();
    }
    
    initPagination();
  }
  
  /**
   * On réinitialise tous les filtres (enfin sauf l'établissement bien sûr)
   */
  public void initTousLesFiltres(String pDestinataire) {
    initPagination();
    statutSNArticle = listeParametresGVC.getFiltreStatutArticle();
    etatWeb = null;
    groupeArticle = null;
    familleArticle = null;
    sousFamilleArticle = null;
    marqueArticle = null;
    fournisseurArticle = null;
    moinsCherAilleurs = listeParametresGVC.isFiltreMoinsChersPriceJet();
    
    expediteurArt = null;
    destinataireArt = pDestinataire;
    etatWorkFlow = null;
    etatWeb = null;
    saisieUtilisateur = null;
  }
  
  public void initPagination() {
    indexDebut = 1;
    indexFin = (indexDebut + listeParametresGVC.getNombreArticlesParPage() - 1);
    numeroPage = 1;
  }
  
  /**
   * Incrémenter les indexs SQL de filtres de nombre d'articles à retourner
   */
  public void incrementerPage() {
    indexDebut += listeParametresGVC.getNombreArticlesParPage();
    indexFin = indexDebut + listeParametresGVC.getNombreArticlesParPage() - 1;
  }
  
  /**
   * Décrémenter les indexs SQL de filtres de nombre d'articles à retourner
   */
  public void decrementerPage() {
    if (indexDebut < listeParametresGVC.getNombreArticlesParPage()) {
      return;
    }
    
    indexDebut -= listeParametresGVC.getNombreArticlesParPage();
    indexFin = indexDebut + listeParametresGVC.getNombreArticlesParPage() - 1;
  }
  
  public void choisirPage(int numero) {
    numeroPage = numero;
    indexDebut = ((numeroPage - 1) * listeParametresGVC.getNombreArticlesParPage()) + 1;
    indexFin = indexDebut + listeParametresGVC.getNombreArticlesParPage() - 1;
  }

  /**
   * On contrôle et on formate la saisie utilisateur dans la zone de recherche article du GVC
   */
  public boolean controlerSaisieUtilisateur(String pSaisie) {
    boolean isOK = false;
    
    if (pSaisie == null || pSaisie.trim().equals("") || pSaisie.trim().length() <= 2) {
      return isOK;
    }
    
    pSaisie = pSaisie.toUpperCase().trim();
    
    if (pSaisie.length() > 20) {
      pSaisie.substring(0, 20);
    }

    // Pas de contrôle supplémentaire pour le moment
    isOK = true;
    saisieUtilisateur = pSaisie;
    
    return isOK;
  }

  /**
   * On RAZ les filtres qui ne peuvent être actifs pour une recherche utilisateur et laisse actif la saisie
   */
  public void razPourSaisie() {
    initPagination();
    statutSNArticle = listeParametresGVC.getFiltreStatutArticle();
    etatWeb = null;
    groupeArticle = null;
    familleArticle = null;
    sousFamilleArticle = null;
    marqueArticle = null;
    fournisseurArticle = null;
    moinsCherAilleurs = false;
    
    expediteurArt = null;
    destinataireArt = null;
    etatWorkFlow = null;
    etatWeb = null;
  }
  
  /**************************************************************************
   * ACCESSEURS
   *************************************************************************/
  
  public IdEtablissement getEtablissementEnCours() {
    return etablissementEnCours;
  }
  
  public void setEtablissementEnCours(IdEtablissement etablissementEnCours) {
    this.etablissementEnCours = etablissementEnCours;
  }
  
  public int getIndexDebut() {
    return indexDebut;
  }
  
  public void setIndexDebut(int indexDebut) {
    this.indexDebut = indexDebut;
  }
  
  public int getIndexFin() {
    return indexFin;
  }
  
  public void setIndexFin(int indexFin) {
    this.indexFin = indexFin;
  }
  
  public int getNumeroPage() {
    return numeroPage;
  }
  
  public void setNumeroPage(int numeroPage) {
    this.numeroPage = numeroPage;
  }
  
  public StatutArticle getStatutSNArticle() {
    return statutSNArticle;
  }
  
  public void setStatutSNArticle(StatutArticle statutSNArticle) {
    this.statutSNArticle = statutSNArticle;
  }
  
  public GroupeArticles getGroupeArticle() {
    return groupeArticle;
  }
  
  public void setGroupeArticle(GroupeArticles groupeArticle) {
    indexDebut = 1;
    indexFin = (indexDebut + listeParametresGVC.getNombreArticlesParPage() - 1);
    this.groupeArticle = groupeArticle;
  }
  
  public Famille getFamilleArticle() {
    return familleArticle;
  }
  
  public void setFamilleArticle(Famille familleArticle) {
    indexDebut = 1;
    indexFin = (indexDebut + listeParametresGVC.getNombreArticlesParPage() - 1);
    this.familleArticle = familleArticle;
  }
  
  public SousFamille getSousFamilleArticle() {
    return sousFamilleArticle;
  }
  
  public void setSousFamilleArticle(SousFamille sousFamilleArticle) {
    indexDebut = 1;
    indexFin = (indexDebut + listeParametresGVC.getNombreArticlesParPage() - 1);
    this.sousFamilleArticle = sousFamilleArticle;
  }
  
  public String getMarqueArticle() {
    return marqueArticle;
  }
  
  public void setMarqueArticle(String marqueArticle) {
    indexDebut = 1;
    indexFin = (indexDebut + listeParametresGVC.getNombreArticlesParPage() - 1);
    this.marqueArticle = marqueArticle;
  }
  
  public Fournisseur getFournisseurArticle() {
    return fournisseurArticle;
  }
  
  public void setFournisseurArticle(Fournisseur fournisseurArticle) {
    indexDebut = 1;
    indexFin = (indexDebut + listeParametresGVC.getNombreArticlesParPage() - 1);
    this.fournisseurArticle = fournisseurArticle;
  }
  
  public boolean isMoinsCherAilleurs() {
    return moinsCherAilleurs;
  }
  
  public void setMoinsCherAilleurs(boolean moinsCherAilleurs) {
    this.moinsCherAilleurs = moinsCherAilleurs;
  }
  
  public String getEtatWeb() {
    return etatWeb;
  }
  
  public void setEtatWeb(String etatWeb) {
    this.etatWeb = etatWeb;
  }
  
  public String getExpediteurArt() {
    return expediteurArt;
  }
  
  public void setExpediteurArt(String expediteurArt) {
    this.expediteurArt = expediteurArt;
  }
  
  public String getDestinataireArt() {
    return destinataireArt;
  }
  
  public void setDestinataireArt(String destinataireArt) {
    this.destinataireArt = destinataireArt;
  }
  
  public EtatWorkFlow getEtatWorkFlow() {
    return etatWorkFlow;
  }
  
  public void setEtatWorkFlow(EtatWorkFlow etatWorkFlow) {
    this.etatWorkFlow = etatWorkFlow;
  }
  
  public String getSaisieUtilisateur() {
    return saisieUtilisateur;
  }
  
}
