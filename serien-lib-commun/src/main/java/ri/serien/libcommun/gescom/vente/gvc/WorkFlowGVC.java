/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc;

import java.io.Serializable;

/**
 * Cette classe permet de définir le workflow du GVC : Un expéditeur de tâche, un destinataire de tâche et une tâche (état WorkFlow)
 */
public class WorkFlowGVC implements Serializable {
  
  private String expediteur = null;
  private String destinataire = null;
  private EtatWorkFlow etatWF = null;
  
  public String getExpediteur() {
    return expediteur;
  }

  public void setExpediteur(String expediteur) {
    this.expediteur = expediteur;
  }

  public String getDestinataire() {
    return destinataire;
  }

  public void setDestinataire(String destinataire) {
    this.destinataire = destinataire;
  }

  public EtatWorkFlow getEtatWF() {
    return etatWF;
  }

  public void setEtatWF(EtatWorkFlow etatWF) {
    this.etatWF = etatWF;
  }
  
}
