/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceCommun;

/**
 * Liste de contacts.
 */
public class ListeContact extends ListeClasseMetier<IdContact, Contact, ListeContact> {
  /**
   * Constructeur.
   */
  public ListeContact() {
  }
  
  /**
   * Construire une liste de contacts correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des contacts ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeContact creerListeNonChargee(List<IdContact> pListeIdContact) {
    ListeContact listeContact = new ListeContact();
    if (pListeIdContact != null) {
      for (IdContact idContact : pListeIdContact) {
        Contact contact = new Contact(idContact);
        contact.setCharge(false);
        listeContact.add(contact);
      }
    }
    return listeContact;
  }
  
  /**
   * Charge les contacts à partir des id.
   */
  @Override
  public ListeContact charger(IdSession pIdSession, List<IdContact> pListeId) {
    return ManagerServiceCommun.chargerListeContact(pIdSession, pListeId);
  }
  
  /**
   * Charge les contacts d'un établissement.
   */
  @Override
  public ListeContact charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Retourner le contact principal pour un client.
   * 
   * @pIdClient Identifiant du client dont il faut retourner le contact principal.
   * @return Contact principal du client (null si aucun).
   */
  public Contact getContactPrincipal(IdClient pIdClient) {
    if (pIdClient == null) {
      return null;
    }
    for (Contact contact : this) {
      if (contact != null && contact.isContactPrincipal(pIdClient)) {
        return contact;
      }
    }
    return null;
  }
  
  /**
   * Retourner le contact principal pour un fournisseur.
   * 
   * pIdFournisseur Identifiant du fournisseur dont il faut retourner le contact principal.
   * @return Contact principal du fournisseur (null si aucun).
   */
  public Contact getContactPrincipal(IdFournisseur pIdFournisseur) {
    if (pIdFournisseur == null) {
      return null;
    }
    for (Contact contact : this) {
      if (contact != null && contact.isContactPrincipal(pIdFournisseur)) {
        return contact;
      }
    }
    return null;
  }
  
  /**
   * Retourner un Contact de la liste à partir de son nom.
   */
  public Contact retournerContactParPrisPar(String pPrisPar) {
    if (pPrisPar == null) {
      return null;
    }
    for (Contact contact : this) {
      if (contact != null && contact.toString().equals(pPrisPar)) {
        return contact;
      }
    }
    
    return null;
  }
  
}
