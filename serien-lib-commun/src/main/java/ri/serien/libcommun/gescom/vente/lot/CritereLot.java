/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.lot;

import java.io.Serializable;

import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;

/**
 * Critères de recherche sur les chantiers.
 */
public class CritereLot implements Serializable {
  
  // Variables
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  private IdEtablissement idEtablissement = null;
  private IdArticle idArticle = null;
  private IdMagasin idMagasin = null;
  private IdLigneVente idLigneVente = null;
  private IdDocumentVente IdDocumentOrigine = null;
  
  /**
   * Vide les critères de recherche
   */
  public void initialiser() {
    rechercheGenerique.initialiser();
    idEtablissement = null;
    idArticle = null;
    idMagasin = null;
    idLigneVente = null;
    IdDocumentOrigine = null;
  }
  
  // -- Accesseurs
  /**
   * Identifiant de l'établissement dans lequel est recherché le client.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Filtre sur l'établissement.
   */
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void setTexteRechercheGenerique(String pTexteRecherche) {
    rechercheGenerique.setTexte(pTexteRecherche);
  }
  
  /**
   * 
   * Filtre sur l'identifiant article.
   * 
   * @return IdArticle
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * 
   * Modifier le filtre sur l'identifiant article.
   * 
   * @param pIdArticle
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  /**
   * 
   * Filtre sur l'identifiant du magasin.
   * 
   * @return IdMagasin
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * 
   * Modifier le filtre sur l'identifiant du magasin.
   * 
   * @param pIdMagasin
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  /**
   * 
   * .
   * 
   * @return
   */
  public IdLigneVente getIdLigneVente() {
    return idLigneVente;
  }
  
  /**
   * 
   * .
   * 
   * @param idLigneVente
   */
  public void setIdLigneVente(IdLigneVente idLigneVente) {
    this.idLigneVente = idLigneVente;
  }
  
  /**
   * 
   * Retourner l'identifiant du document d'origine (pour les avoirs).
   * 
   * @return IdDocumentVente
   */
  public IdDocumentVente getIdDocumentOrigine() {
    return IdDocumentOrigine;
  }
  
  /**
   * 
   * Modifier l'identifiant du document d'origine (pour les avoirs).
   * 
   * @param pIdDocumentOrigineAvoir
   */
  public void setIdDocumentOrigine(IdDocumentVente pIdDocumentOrigineAvoir) {
    IdDocumentOrigine = pIdDocumentOrigineAvoir;
  }
  
}
