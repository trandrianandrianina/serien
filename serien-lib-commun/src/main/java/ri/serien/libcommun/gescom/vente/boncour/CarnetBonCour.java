/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.boncour;

import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Carnet de bons de cour.
 * 
 * Les carnets de bons de cour sont la représentation informatique de carnets papiers. Ces carnets contiennent des bons de cour numérotés.
 * Ces carnets sont eux-mêmes numérotés de manière unique et attribués à un et un seul magasinier. Une copie carbone des bons de cour est
 * conservée dans le carnet de bons de cour. Cela permet de garder une traçabilité sur les marchandises remises directement à un client au
 * parc matériaux.
 *
 * Pour plus de précisions, voir la COM-965 pour le client Allot.
 *
 */
public class CarnetBonCour extends AbstractClasseMetier<IdCarnetBonCour> {
  // Constantes
  public static final int LONGUEUR_NOMBRE_PAGE = 4;
  
  // Variables
  private IdVendeur idMagasinier = null;
  private Integer nombrePages = null;
  private Integer premierNumero = null;
  private Integer dernierNumero = null;
  private Date dateRemise = null;
  private IdBonCour idDernierBonCourUtilise = null;
  
  /**
   * Constructeur.
   */
  public CarnetBonCour(IdCarnetBonCour pIdCarnetBonCour) {
    super(pIdCarnetBonCour);
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    return id.getNumero() + "";
  }
  
  @Override
  public CarnetBonCour clone() {
    CarnetBonCour o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (CarnetBonCour) super.clone();
      o.setIdMagasinier(idMagasinier);
      o.setIdDernierBonCourUtilise(idDernierBonCourUtilise);
    }
    catch (CloneNotSupportedException cnse) {
      throw new MessageErreurException(cnse, "Erreur lors du clonage de l'objet métier.");
    }
    
    return o;
  }
  
  /**
   * Comparer toutes les données de deux carnets.
   */
  public boolean equalsComplet(CarnetBonCour pCarnetAComparer) {
    // Tester si l'objet est nous-même (optimisation)
    if (pCarnetAComparer == this) {
      return true;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (pCarnetAComparer == null) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    boolean retour = true;
    retour &= Constantes.equals(this, pCarnetAComparer);
    retour &= Constantes.equals(idMagasinier, pCarnetAComparer.getIdMagasinier());
    retour &= Constantes.equals(nombrePages, pCarnetAComparer.getNombrePages());
    retour &= Constantes.equals(premierNumero, pCarnetAComparer.getPremierNumero());
    retour &= Constantes.equals(dateRemise, pCarnetAComparer.getDateRemise());
    retour &= Constantes.equals(idDernierBonCourUtilise, pCarnetAComparer.getIdDernierBonCourUtilise());
    return retour;
  }
  
  /**
   * Charger les données du bon de cour
   */
  public void charger(IdSession pIdSession) {
    CarnetBonCour carnet = ManagerServiceDocumentVente.chargerCarnetBonCour(pIdSession, getId());
    idMagasinier = carnet.getIdMagasinier();
    nombrePages = carnet.getNombrePages();
    premierNumero = carnet.premierNumero;
    dateRemise = carnet.getDateRemise();
  }
  
  /**
   * Calcul du dernier numéro de bon de cour du carnet (si on a les information permettant de le calculer).
   */
  public static Integer calculerDernierNumero(Integer pPremierNumero, Integer pNombrePage) {
    if (pNombrePage == null || pNombrePage.intValue() == 0 || pPremierNumero == null || pPremierNumero.intValue() == 0) {
      return null;
    }
    return Integer.valueOf(pPremierNumero + pNombrePage - 1);
  }
  
  /**
   * Contrôler les données obligatoires pour la sauvegarde en base.
   * Seuls la date de remise et le code du magasinier ne sont pas obigatoires.
   */
  public static void controler(CarnetBonCour pCarnetBonCour) {
    if (pCarnetBonCour == null) {
      throw new MessageErreurException("Le carnet est invalide.");
    }
    // Contrôle de l'identifiant
    IdCarnetBonCour.controlerId(pCarnetBonCour.getId(), false);
    
    // Controler le nombre de page
    if (pCarnetBonCour.getNombrePages() == null) {
      throw new MessageErreurException("Le nombre de pages du carnet est invalide.");
    }
    long valeurMax = Constantes.valeurMaxZoneNumerique(LONGUEUR_NOMBRE_PAGE);
    if (pCarnetBonCour.getNombrePages() <= 0 || pCarnetBonCour.getNombrePages() > valeurMax) {
      throw new MessageErreurException("Le nombre de pages du carnet est doit être compris entre 1 et " + valeurMax + '.');
    }
    
    // Contrôle le premier numéro du bon
    IdBonCour.controlerNumero(pCarnetBonCour.getPremierNumero());
  }
  
  // -- Méthodes privées
  
  /**
   * Calcul du dernier numéro de bon de cour du carnet (si on a les information permettant de le calculer).
   */
  private void calculerDernierNumero() {
    dernierNumero = calculerDernierNumero(premierNumero, nombrePages);
  }
  
  // -- Accesseurs
  
  /**
   * Identifiant du magasinier à qui a été remis le carnet. L'identifiant du magasinier est un IdVendeur.
   */
  public IdVendeur getIdMagasinier() {
    return idMagasinier;
  }
  
  /**
   * Identifiant du magasinier à qui a été remis le carnet. L'identifiant du magasinier est un IdVendeur.
   */
  public void setIdMagasinier(IdVendeur pIdMagasinier) {
    idMagasinier = pIdMagasinier;
  }
  
  /**
   * Nombre de pages constituant le carnet (nombre de bons de cour)
   */
  public Integer getNombrePages() {
    return nombrePages;
  }
  
  /**
   * Nombre de pages constituant le carnet (nombre de bons de cour)
   */
  public void setNombrePages(Integer pNombrePages) {
    nombrePages = pNombrePages;
  }
  
  /**
   * Premier numéro de bons de cour contenu dans le carnet
   */
  public Integer getPremierNumero() {
    return premierNumero;
  }
  
  /**
   * Premier numéro de bons de cour contenu dans le carnet
   */
  public void setPremierNumero(Integer pPremierNumero) {
    premierNumero = pPremierNumero;
  }
  
  /**
   * Dernier numéro de bons de cour contenu dans le carnet
   */
  public Integer getDernierNumero() {
    if (dernierNumero == null || dernierNumero == 0) {
      calculerDernierNumero();
    }
    return dernierNumero;
  }
  
  /**
   * Date à laquelle le carnet a été remis au magasinier
   */
  public Date getDateRemise() {
    return dateRemise;
  }
  
  /**
   * Date à laquelle le carnet a été remis au magasinier
   */
  public void setDateRemise(Date pDateRemise) {
    dateRemise = pDateRemise;
  }
  
  /**
   * Identifiant du dernier bon de cour utilisé dans ce carnet
   */
  public IdBonCour getIdDernierBonCourUtilise() {
    return idDernierBonCourUtilise;
  }
  
  /**
   * Identifiant du dernier bon de cour utilisé dans ce carnet
   */
  public void setIdDernierBonCourUtilise(IdBonCour pIdDernierBonCourUtilise) {
    idDernierBonCourUtilise = pIdDernierBonCourUtilise;
  }
}
