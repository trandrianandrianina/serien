/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.origine;

/**
 * Liste des origines possibles pour la devise du tarif article utilisé pour le calcul.
 * Les origines sont classées par ordre de priorité (du plus prioritaire vers le moins prioritaire).
 */
public enum EnumOrigineDevise {
  DOCUMENT_VENTE("Document de vente"),
  CLIENT("Client"),
  ETABLISSEMENT("Etablissement"),
  DEFAUT("Par défaut");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrigineDevise(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
