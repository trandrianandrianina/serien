/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.conditionachat;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de l'article
 *
 * le code 2 n'est pas pris en compte car il ne s'agit que de la surface mais sans la quantité
 *
 */
public enum EnumModeSaisiePort {
  MONTANT_FOIS_POIDS(0, "Port proportionnel au poids"),
  POURCENTAGE_PRIX_NET(1, "Port au pourcentage du prix net");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumModeSaisiePort(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumModeSaisiePort valueOfByCode(Integer pCode) {
    for (EnumModeSaisiePort value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le mode de saisie du port est invalide : " + pCode);
  }
}
