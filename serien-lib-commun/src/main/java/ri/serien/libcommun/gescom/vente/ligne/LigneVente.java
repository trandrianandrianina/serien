/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.IdTypeGratuit;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.PrixFlash;
import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.OutilCalculPrix;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.EnumTypeChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.InformationConditionVenteLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceCoefficient;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

public class LigneVente extends LigneVenteBase {
  // Constantes
  public static final String OPTION_VALIDATION = "VAL";
  public static final String OPTION_EXPEDITION = "EXP";
  public static final String OPTION_FACTURATION = "FAC";
  public static final char TYPE_REMISE_CASCADE = '1';
  public static final char ORIGINE_NON_DEFINI = '0';
  public static final char ORIGINE_SAISI = '3';
  
  public static final int COMMENTAIRE_A_COPIER_DANS_COMMANDE_ACHAT = 1;
  
  // Id du document de vente à qui appartient cette ligne de vente
  private IdDocumentVente idDocumentVente = null;
  // Variables fichiers lireLigneDocument (SVGVM11LL)
  private String indicateurs = ""; // Indicateurs (POIND)
  private Character affectationStock = Character.valueOf(' '); // Demande l'affectation sur stock
  private int codeEtat = 0; // Code état de la ligne
  
  private BigDecimal nombreUSParUV = BigDecimal.ZERO;
  
  private String libelleTarif = ""; // Libellé du tarif
  private Character typeRemise = Character.valueOf(' '); // Type de remise ligne, 1=cascade
  private EnumAssietteTauxRemise assietteTauxRemise = null;
  private Character exclusionRemisePied1 = Character.valueOf(' '); // Exclusion remise de pied n°1
  private Character exclusionRemisePied2 = Character.valueOf(' '); // Exclusion remise de pied n°2
  private Character exclusionRemisePied3 = Character.valueOf(' '); // Exclusion remise de pied n°3
  private Character exclusionRemisePied4 = Character.valueOf(' '); // Exclusion remise de pied n°4
  private Character exclusionRemisePied5 = Character.valueOf(' '); // Exclusion remise de pied n°5
  private Character exclusionRemisePied6 = Character.valueOf(' '); // Exclusion remise de pied n°6
  private BigDecimal prixPromo = BigDecimal.ZERO; // Prix de promo
  private BigDecimal coefficient = BigDecimal.ZERO; // Coefficient multiplicateur du prix de base
  private Character codeLigneAvoir = Character.valueOf(' ');
  
  private IdMagasin idMagasin = null;
  private IdRepresentant idRepresentant = null; // code du représentant
  private BigDecimal quantitePieces = BigDecimal.ZERO; // Quantité en Pièces
  
  private BigDecimal mesure1 = BigDecimal.ZERO; // Surface ou Volume
  private BigDecimal mesure2 = BigDecimal.ZERO; // Surface ou Volume
  private BigDecimal mesure3 = BigDecimal.ZERO; // Surface ou Volume
  
  private PrixFlash prixFlash = null; // Prix flash
  
  private BigDecimal quantiteAvantExtraction = BigDecimal.ZERO; // C'est la quantité qu'il y avait avant la modification
  private int codeTVA = 0;
  private int topLigneEnValeur = 0; // Top ligne en valeur
  private int numeroColonneTVA = 0; // Numéro de la colonne TVA/E1TVAG de l'entete du document
  private int codeTPF = 0; // ???
  private int topNonCommissionne = 0; // Top non commissionné
  private int signeLigne = 0; // Signe de la ligne
  private Character codeNumSerieOuLot = Character.valueOf(' '); // Saisi S: numéro de série, L: lot
  private int topNumSerieOuLot = 0; // Top numéro de série ou lot
  private Character nonSoumisEscompte = Character.valueOf(' '); // Non soumis à l'escompte
  private BigDecimal prixTransport = BigDecimal.ZERO;
  private BigDecimal montantTransport = BigDecimal.ZERO;
  private Date dateLivraisonPrevue = null;
  private IdTypeGratuit idTypeGratuit = null;
  
  private String topPersonnalisation1 = "";
  private String topPersonnalisation2 = "";
  private String topPersonnalisation3 = "";
  private String topPersonnalisation4 = "";
  private String topPersonnalisation5 = "";
  // Mise à jour des tops personnalisation de manière humainement compréhensible
  private boolean editionAucunDocument = false;
  private boolean editionTousTypeDocuments = false;
  private boolean editionSurDevis = false;
  private boolean editionSurPreparation = false;
  private boolean editionSurCommande = false;
  private boolean editionSurBon = false;
  private boolean editionSurBonTransporteur = false;
  private boolean editionSurFacture = false;
  
  private int generationBonAchat = 0;
  private IdArticle idArticleSubstitue = null; // Code article substitué
  private Character typeSubstitution = Character.valueOf(' ');
  private Character remise50Commercial = Character.valueOf(' '); // Remise 50/50 sur com/rep
  private Character indRecherchePrixKit = Character.valueOf(' '); // Recherche prix / kit
  private BigDecimal quantiteLivrable = BigDecimal.ZERO;
  private Character indiceLivraison = Character.valueOf(' '); // Indice de livraison
  private Character indCumulL1QTE = Character.valueOf(' '); // Cumul (pds, vol, col) / L1QTE
  private Character indCumulL1QTP = Character.valueOf(' '); // Cumul (pds, vol, col) / L1QTP
  private Character indLigneAExtraire = Character.valueOf(' '); // Ligne à extraire
  private Character indASDI = Character.valueOf(' '); // ASDI = A1IN13 ???
  private int numeroLigneOrigine = 0;
  private BigDecimal prixAAjouterPrixRevient = BigDecimal.ZERO; // Prix à ajouter au PRV
  private BigDecimal prixAAjouterPrixVenteClient = BigDecimal.ZERO; // Prix à ajouter au PVC
  private Character indTypeVente = Character.valueOf(' ');
  private Character indApplicationConditionQuantitative = Character.valueOf(' ');
  private EnumTypeChantier typeConditionChantier = null;
  private Character topArticleLoti = Character.valueOf(' ');
  private Character topArticleADS = Character.valueOf(' ');
  private AlertesLigneDocument alertesLigneDocument = null;
  private BigDecimal quantiteCommandeStock = BigDecimal.ZERO;
  private String uniteCommandeStock = "";
  private BigDecimal quantiteDocumentOrigine = BigDecimal.ZERO;
  private BigDecimal quantiteDejaExtraite = BigDecimal.ZERO;
  private BigDecimal quantiteDejaRetounee = BigDecimal.ZERO;
  private IdAdresseFournisseur idAdresseFournisseur = null;
  
  // Variable de travail (alimenté via le numéro de colonne de la TVA dans l'entête du document)
  private BigDecimal pourcentageTVA = BigDecimal.ZERO;
  
  // Variable héritée de l'article
  private boolean ligneConditionneeEnPalette = false;
  
  // Permet de savoir si le client est TTC ou HT afin de retourner les bons champs lors de l'initialisation des services RPG
  private boolean isClientTTC = false;
  
  // Type de l'article contenu dans la ligne
  private EnumTypeArticle typeArticle = null;
  private BigDecimal quantiteArticlesFactures = BigDecimal.ZERO;
  
  // Ligne d'achat générée pour approvisionner cette ligne de vente
  private IdLigneAchat idLigneAchatLiee = null;
  
  // Variables issues de prix de vente java (mais de la ligne de vente stockée en base)
  private boolean consignable = false;
  
  private boolean autoriserPrixNetInferieurPrixRevient = false;
  
  private Integer numeroColonneTarif = null;
  private EnumProvenanceColonneTarif provenanceColonneTarif = null;
  private EnumProvenancePrixBase provenancePrixBase = null;
  private EnumProvenanceCoefficient provenanceCoefficient = null;
  private EnumProvenanceTauxRemise provenanceTauxRemise = null;
  private Character provenancePrixDeRevient = null;
  
  private BigDecimal prixBaseTTC = null;
  private BigDecimal prixNetSaisiTTC = null;
  private BigDecimal prixNetCalculeTTC = null;
  private BigDecimal montantTTC = null;
  
  // Prix de revient standard issu du calcul de la CNA (valable pour un article)
  private BigDecimal prixDeRevientStandardHT = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES);
  private BigDecimal prixDeRevientStandardTTC = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES);
  private BigDecimal prixDeRevientPortIncluHT = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES);
  
  private BigDecimal prixDeRevientLigneHT = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES);
  private BigDecimal prixDeRevientLigneTTC = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES);
  // Indique dans quel mode le calcul des prix de vente s'effectue (Negoce ou classique)
  private Boolean modeNegoce = null;
  private String codeGroupeCNV = null;
  
  // Présent lorsque les données doivent être enregistrées en base
  private List<InformationConditionVenteLigneVente> listeConditionVenteLigneVente = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public LigneVente(IdLigneVente pIdLigneVente) {
    super(pIdLigneVente);
  }
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdLigneVente controlerId(LigneVente pLigneVente, boolean pVerifierExistance) {
    if (pLigneVente == null) {
      throw new MessageErreurException("La ligne de ventes est invalide.");
    }
    IdLigneVente idLigneVente = pLigneVente.getId();
    if (idLigneVente == null) {
      throw new MessageErreurException("L'identifiant de la ligne de ventes est invalide.");
    }
    if (pVerifierExistance && !idLigneVente.isExistant()) {
      throw new MessageErreurException("L'identifiant de la ligne de ventes n'existe pas dans la base de données : " + idLigneVente);
    }
    return idLigneVente;
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise le taux de TVA à partir de l'entête du document.
   */
  public void initialiserTauxTVA(DocumentVente pDocumentVente, Etablissement pEtablissement) {
    // On tente de récupérer la TVA dans l'entête du document
    if (pDocumentVente != null) {
      switch (numeroColonneTVA) {
        case 1:
          pourcentageTVA = pDocumentVente.getTauxTVA1();
          break;
        case 2:
          pourcentageTVA = pDocumentVente.getTauxTVA2();
          break;
        case 3:
          pourcentageTVA = pDocumentVente.getTauxTVA3();
          break;
      }
    }
    // Sinon dans la description générale
    if ((pourcentageTVA.compareTo(BigDecimal.ZERO) == 0) && pEtablissement != null) {
      pourcentageTVA = pEtablissement.getTauxTVA(codeTVA);
    }
  }
  
  /**
   * Retourne si le prix flash existe.
   */
  public boolean avoirPrixFlash() {
    if (prixFlash != null) {
      return true;
    }
    return false;
  }
  
  /**
   * Permet de comparer avec une autre ligne article.
   */
  public int compareTo(LigneVente pLigneArticle) {
    return id.getNumeroLigne().compareTo(pLigneArticle.getId().getNumeroLigne());
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public LigneVente clone() {
    LigneVente ligneVente = null;
    // Récupère l'instance à renvoyer par l'appel de la méthode super.clone()
    try {
      ligneVente = (LigneVente) super.clone();
      if (prixFlash != null) {
        ligneVente.setPrixFlash(prixFlash.clone());
      }
      ligneVente.setAlertesLigneDocument(null);
    }
    catch (Exception e) {
    }
    
    return ligneVente;
  }
  
  /**
   * Indiquer s'il s'agit d'une ligne gratuite.
   * Une ligne est considérée comme gratuite si un type de gratuit est renseigné.
   * @return true=ligne gratuite, false=ligne non gratuite.
   */
  public boolean isLigneGratuite() {
    return idTypeGratuit != null && idTypeGratuit.isGratuit();
  }
  
  /**
   * Retourne s'il s'agit d'une ligne palette (c'est à dire contenant un article palette).
   */
  public boolean isLignePalette() {
    return typeArticle != null && typeArticle.equals(EnumTypeArticle.PALETTE);
  }
  
  /**
   * Retourner s'il s'agit d'une ligne contenant un article spécial.
   */
  public boolean isLigneArticleSpecial() {
    return typeArticle != null && typeArticle.equals(EnumTypeArticle.SPECIAL);
  }
  
  /**
   * Retourner s'il s'agit d'une ligne contenant un article standard.
   */
  public boolean isLigneArticleStandard() {
    return typeArticle != null && typeArticle.equals(EnumTypeArticle.STANDARD);
  }
  
  /**
   * Retourner s'il s'agit d'une ligne contnant un article transport.
   */
  public boolean isLigneArticleTransport() {
    return typeArticle != null && typeArticle.equals(EnumTypeArticle.TRANSPORT);
  }
  
  /**
   * Retourner s'il s'agit d'une ligne contenant un article géré par numéros de série.
   */
  public boolean isLigneArticleSerie() {
    return typeArticle != null && typeArticle.equals(EnumTypeArticle.SERIE);
  }
  
  /**
   * Retourner s'il s'agit d'une ligne contenant un article commentaire.
   */
  public boolean isLigneArticleCommentaire() {
    return typeArticle != null && typeArticle.equals(EnumTypeArticle.COMMENTAIRE);
  }
  
  /**
   * Retourner si la ligne est un retour palette.
   */
  public boolean isRetourPalette() {
    return isLignePalette() && getQuantiteUV().compareTo(BigDecimal.ZERO) < 0;
  }
  
  /**
   * Indique si une ligne de vente est une ligne commentaire à copier dans la commande d'achat.
   *
   * @return
   */
  public boolean isLigneCommentaireACopier() {
    if (isLigneCommentaire() && generationBonAchat > 0) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne la quantité d'orgine en UC.
   * 
   * La quantité d'orgine en unités de conditionnement de ventes n'est pas stockée dans la ligne de ventes. Il faut la recalculer à partir
   * de la quantité en unités de ventes (UV).
   */
  public BigDecimal getQuantiteDocumentOrigineUC() {
    // S'il s'agit d'un article découpable, on retourne le nombre d'éléments
    if (isArticleDecoupable()) {
      return quantiteDocumentOrigine;
    }
    // Sinon c'est un article normal : On calcule la quantité en UCV à partir de la quantiteDocumentOrigine qui est en UV si le rapport
    // nombre d'UV par UCV existe
    if (getNombreUVParUCV() != null && getNombreUVParUCV().compareTo(BigDecimal.ZERO) != 0) {
      
      // On applique la précision maximale lors du calcul pour éviter les mauvais arrondis
      BigDecimal quantiteDocumentOrigineUC = quantiteDocumentOrigine.divide(getNombreUVParUCV(), MathContext.DECIMAL32)
          .setScale(PRECISION_MAX_QUANTITE, RoundingMode.HALF_UP);
      
      return quantiteDocumentOrigineUC;
    }
    
    // Si le rapport nombre d'UV par UCV n'existe pas, la quantité en UCV == la quantité en UV
    return quantiteDocumentOrigine;
  }
  
  /**
   * Retourne la quantité extraite en UC.
   * 
   * La quantité en unités de conditionnement de ventes n'est pas stockée dans la ligne de ventes. Il faut la recalculer à partir de la
   * quantité en unités de ventes (UV).
   */
  public BigDecimal getQuantiteDejaExtraiteUC() {
    // S'il s'agit d'un article découpable, on retourne le nombre d'éléments
    if (isArticleDecoupable()) {
      return quantiteDejaExtraite;
    }
    // Sinon c'est un article normal : On calcule la quantité en UCV à partir de la quantiteDocumentOrigine qui est en UV si le rapport
    // nombre d'UV par UCV existe
    if (getNombreUVParUCV() != null && getNombreUVParUCV().compareTo(BigDecimal.ZERO) != 0) {
      
      // On applique la précision maximale lors du calcul pour éviter les mauvais arrondis
      BigDecimal quantiteDejaExtraiteUC =
          quantiteDejaExtraite.divide(getNombreUVParUCV(), MathContext.DECIMAL32).setScale(PRECISION_MAX_QUANTITE, RoundingMode.HALF_UP);
      
      return quantiteDejaExtraiteUC;
    }
    
    // Si le rapport nombre d'UV par UCV n'existe pas, la quantité en UCV == la quantité en UV
    return quantiteDejaExtraite;
  }
  
  /**
   * Indique si la ligne est entièrement extraite.
   */
  public boolean isEntierementExtraite() {
    return getQuantiteUV().equals(quantiteDocumentOrigine);
  }
  
  /**
   * Indique si le prix de revient a été saisi.
   */
  public boolean isPrixRevientSaisi() {
    if (provenancePrixDeRevient == null) {
      return false;
    }
    return provenancePrixDeRevient == ORIGINE_SAISI;
  }
  
  /**
   * Initialiser les champs concernant les prix de ventes de la ligne de vente.
   * @param pCalculPrixVente Le calcul du prix de vente.
   */
  public void initialiserPrixVente(CalculPrixVente pCalculPrixVente) {
    if (pCalculPrixVente == null) {
      throw new MessageErreurException("Le calcul du prix de vente est incorrect.");
    }
    
    // Récupération et contrôle de la formule de prix finale
    FormulePrixVente formulePrixVente = pCalculPrixVente.getFormulePrixVenteFinale();
    if (formulePrixVente == null) {
      return;
    }
    
    // Le mode de calcul (négoce ou classique)
    modeNegoce = formulePrixVente.getModeNegoce();
    
    // Renseigner l'origine du prix de vente
    setOriginePrixVente(pCalculPrixVente.getOriginePrixVente());
    
    // Renseigner le numéro de la colonne tarif
    numeroColonneTarif = formulePrixVente.getNumeroColonneTarif();
    provenanceColonneTarif = formulePrixVente.determinerProvenanceColonneTarif();
    
    // Renseigner le prix de base
    setPrixBaseHT(formulePrixVente.getPrixBaseCalculHT());
    prixBaseTTC = formulePrixVente.getPrixBaseCalculTTC();
    provenancePrixBase = formulePrixVente.determinerProvenancePrixBase();
    
    // Renseigne les taux de remises
    // Dans le cas d'une initialisation de la ligne, nous n'avons pas forcément la remise de renseignée donc la remise unitaire est
    // utilisée. Le taux de remise unitaire est favorisé car il contient une valeur plus précise (nombre de décimales)
    if (modeNegoce && formulePrixVente.getTauxRemiseUnitaire() != null) {
      setTauxRemise1(formulePrixVente.getTauxRemiseUnitaire());
    }
    else if (formulePrixVente.getTauxRemise1() != null) {
      setTauxRemise1(formulePrixVente.getTauxRemise1());
    }
    else {
      setTauxRemise1(BigPercentage.ZERO);
    }
    if (formulePrixVente.getTauxRemise2() != null) {
      setTauxRemise2(formulePrixVente.getTauxRemise2());
    }
    else {
      setTauxRemise2(BigPercentage.ZERO);
    }
    if (formulePrixVente.getTauxRemise3() != null) {
      setTauxRemise3(formulePrixVente.getTauxRemise3());
    }
    else {
      setTauxRemise3(BigPercentage.ZERO);
    }
    if (formulePrixVente.getTauxRemise4() != null) {
      setTauxRemise4(formulePrixVente.getTauxRemise4());
    }
    else {
      setTauxRemise4(BigPercentage.ZERO);
    }
    if (formulePrixVente.getTauxRemise5() != null) {
      setTauxRemise5(formulePrixVente.getTauxRemise5());
    }
    else {
      setTauxRemise5(BigPercentage.ZERO);
    }
    if (formulePrixVente.getTauxRemise6() != null) {
      setTauxRemise6(formulePrixVente.getTauxRemise6());
    }
    else {
      setTauxRemise6(BigPercentage.ZERO);
    }
    provenanceTauxRemise = formulePrixVente.determinerProvenanceTauxRemise();
    
    // Renseigner le coefficient
    coefficient = formulePrixVente.getCoefficient();
    provenanceCoefficient = formulePrixVente.determinerProvenanceCoefficient();
    
    // Renseigner le type de gratuit
    if (formulePrixVente.isGratuit() && pCalculPrixVente.getParametreLigneVente() != null) {
      idTypeGratuit = pCalculPrixVente.getParametreLigneVente().getIdTypeGratuit();
    }
    else {
      idTypeGratuit = null;
    }
    
    // Renseigner le prix net saisi par l'utilisateur
    if (formulePrixVente.determinerProvenancePrixNet() == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
      setPrixNetSaisiHT(formulePrixVente.getPrixNetHT());
      prixNetSaisiTTC = formulePrixVente.getPrixNetTTC();
    }
    else {
      setPrixNetSaisiHT(null);
      prixNetSaisiTTC = null;
    }
    
    // Renseigner le prix net
    setPrixNetCalculeHT(formulePrixVente.getPrixNetHT());
    prixNetCalculeTTC = formulePrixVente.getPrixNetTTC();
    setProvenancePrixNet(formulePrixVente.determinerProvenancePrixNet());
    
    // Renseigner le taux de TVA
    if (formulePrixVente.getTauxTVA() != null) {
      pourcentageTVA = formulePrixVente.getTauxTVA().getTauxEnPourcentage();
    }
    else {
      pourcentageTVA = null;
    }
    
    // Renseigner le montant
    setMontantHT(formulePrixVente.getMontantHT());
    montantTTC = formulePrixVente.getMontantTTC();
    
    // Initialiser la liste des conditions de ventes utilisées par cette ligne
    listeConditionVenteLigneVente = pCalculPrixVente.getListeConditionVenteLigneVente();
    
    // Renseigner le type de chantier
    typeConditionChantier = formulePrixVente.getTypeConditionChantier();
  }
  
  /**
   * Contôle la validité le prix net saisi passé en paramètre et correction de la valeur en cas d'erreur.
   */
  public BigDecimal controlerValiditePrixNetSaisiHT(BigDecimal pPrixNetSaisiHT, boolean pAutoCorrection) {
    // Contrôle que le prix saisi ne soit pas négatif
    if (pPrixNetSaisiHT.compareTo(BigDecimal.ZERO) < 0) {
      if (pAutoCorrection) {
        pPrixNetSaisiHT = getPrixBaseHT();
      }
      else {
        throw new MessageErreurException("Le prix net saisi HT est négatif " + pPrixNetSaisiHT + '.');
      }
    }
    // Contrôle que le prix ne soit pas inférieur au prix de revient
    else if (pPrixNetSaisiHT.compareTo(prixDeRevientStandardHT) < 0 && !autoriserPrixNetInferieurPrixRevient) {
      if (pAutoCorrection) {
        pPrixNetSaisiHT = prixDeRevientStandardHT;
      }
      else {
        throw new MessageErreurException(
            "Le prix net saisi HT est inférieur au prix de revient HT " + pPrixNetSaisiHT + " < " + prixDeRevientStandardHT + '.');
      }
    }
    
    // Retourne une valeur valide (corrigée dans le cas d'une erreur)
    return pPrixNetSaisiHT;
  }
  
  /**
   * Contôle la validité le prix net saisi TTC passé en paramètre et correction de la valeur en cas d'erreur.
   */
  public BigDecimal controlerValiditePrixNetSaisiTTC(BigDecimal pPrixNetSaisiTTC, boolean pAutoCorrection) {
    // Contrôle que le prix saisi ne soit pas négatif
    if (pPrixNetSaisiTTC.compareTo(BigDecimal.ZERO) < 0) {
      if (pAutoCorrection) {
        Trace.alerte("Le prix net saisi TTC est négatif " + pPrixNetSaisiTTC + ", après correction il est de " + getPrixBaseTTC() + '.');
        pPrixNetSaisiTTC = getPrixBaseTTC();
      }
      else {
        throw new MessageErreurException("Le prix net saisi TTC est négatif " + pPrixNetSaisiTTC + '.');
      }
    }
    // Contrôle que le prix ne soit pas inférieur au prix de revient
    else if (pPrixNetSaisiTTC.compareTo(prixDeRevientStandardTTC) < 0 && !autoriserPrixNetInferieurPrixRevient) {
      if (pAutoCorrection) {
        Trace.alerte("Le prix net saisi TTC est inférieur au prix de revient TTC " + pPrixNetSaisiTTC + " < " + prixDeRevientStandardTTC
            + ", après correction il est de " + prixDeRevientStandardTTC + '.');
        pPrixNetSaisiTTC = prixDeRevientStandardTTC;
      }
      else {
        throw new MessageErreurException(
            "Le prix net saisi TTC est inférieur au prix de revient TTC " + pPrixNetSaisiTTC + " < " + prixDeRevientStandardTTC + '.');
      }
    }
    
    // Retourne une valeur valide (corrigée dans le cas d'une erreur)
    return pPrixNetSaisiTTC;
  }
  
  /**
   * Contôle la validité le prix base saisi passé en paramètre et correction de la valeur en cas d'erreur.
   */
  public BigDecimal controlerValiditePrixBaseSaisiHT(BigDecimal pPrixBaseSaisiHT, boolean pAutoCorrection) {
    // Contrôle que le prix saisi ne soit pas négatif
    if (pPrixBaseSaisiHT.compareTo(BigDecimal.ZERO) < 0) {
      if (pAutoCorrection) {
        pPrixBaseSaisiHT = getPrixBaseHT();
      }
      else {
        throw new MessageErreurException("Le prix base saisi HT est négatif " + pPrixBaseSaisiHT + '.');
      }
    }
    // Contrôle que le prix ne soit pas inférieur au prix de revient
    else if (pPrixBaseSaisiHT.compareTo(prixDeRevientStandardHT) < 0 && !autoriserPrixNetInferieurPrixRevient) {
      if (pAutoCorrection) {
        pPrixBaseSaisiHT = prixDeRevientStandardHT;
      }
      else {
        throw new MessageErreurException(
            "Le prix base saisi HT est inférieur au prix de revient HT " + pPrixBaseSaisiHT + " < " + prixDeRevientStandardHT + '.');
      }
    }
    
    // Retourne une valeur valide (corrigée dans le cas d'une erreur)
    return pPrixBaseSaisiHT;
  }
  
  /**
   * Contôle la validité le prix base saisi TTC passé en paramètre et correction de la valeur en cas d'erreur.
   */
  public BigDecimal controlerValiditePrixBaseSaisiTTC(BigDecimal pPrixBaseSaisiTTC, boolean pAutoCorrection) {
    // Contrôle que le prix saisi ne soit pas négatif
    if (pPrixBaseSaisiTTC.compareTo(BigDecimal.ZERO) < 0) {
      if (pAutoCorrection) {
        Trace
            .alerte("Le prix base saisi TTC est négatif " + pPrixBaseSaisiTTC + ", après correction il est de " + getPrixBaseTTC() + '.');
        pPrixBaseSaisiTTC = getPrixBaseTTC();
      }
      else {
        throw new MessageErreurException("Le prix base saisi TTC est négatif " + pPrixBaseSaisiTTC + '.');
      }
    }
    // Contrôle que le prix ne soit pas inférieur au prix de revient
    else if (pPrixBaseSaisiTTC.compareTo(prixDeRevientStandardTTC) < 0 && !autoriserPrixNetInferieurPrixRevient) {
      if (pAutoCorrection) {
        Trace.alerte("Le prix base saisi TTC est inférieur au prix de revient TTC " + pPrixBaseSaisiTTC + " < " + prixDeRevientStandardTTC
            + ", après correction il est de " + prixDeRevientStandardTTC + '.');
        pPrixBaseSaisiTTC = prixDeRevientStandardTTC;
      }
      else {
        throw new MessageErreurException(
            "Le prix base saisi TTC est inférieur au prix de revient TTC " + pPrixBaseSaisiTTC + " < " + prixDeRevientStandardTTC + '.');
      }
    }
    
    // Retourne une valeur valide (corrigée dans le cas d'une erreur)
    return pPrixBaseSaisiTTC;
  }
  
  /**
   * Contôle la validité du taux de remise passé en paramètre et correction de la valeur en cas d'erreur.
   */
  public static BigDecimal controlerValiditeTauxRemise(BigDecimal pTaux, boolean pAutoCorrection) {
    if (pTaux.compareTo(BigDecimal.ZERO) < 0) {
      if (pAutoCorrection) {
        pTaux = BigDecimal.ZERO;
      }
      else {
        throw new MessageErreurException(
            "La valeur du taux est de " + pTaux + ". Le taux de la remise doit être compris entre 0 et 100 inclus.");
      }
    }
    else if (pTaux.compareTo(Constantes.VALEUR_CENT) > 0) {
      if (pAutoCorrection) {
        pTaux = Constantes.VALEUR_CENT;
      }
      else {
        throw new MessageErreurException(
            "La valeur du taux est de " + pTaux + ". Le taux de la remise doit être compris entre 0 et 100 inclus.");
      }
    }
    
    // Retourne une valeur valide (corrigée dans le cas d'une erreur)
    return pTaux;
  }
  
  /**
   * Contôle la validité du numero de colonne passé en paramètre et correction de la valeur en cas d'erreur.
   */
  public int controlerValiditeNumeroColonne(int pNumero, boolean pAutoCorrection) {
    // Contrôle que le numéro de la colonne ne soit pas inférieur à 1
    if (pNumero < 1) {
      if (pAutoCorrection) {
        pNumero = 1;
      }
      else {
        throw new MessageErreurException("La valeur de la colonne est de " + pNumero
            + ". Le numéro de la colonne des tarifs doit être compris entre 1 et " + 10 + '.');
      }
    }
    // Contrôle que le numéro de lma colonne ne soit pas supérueur au nombre maximum de colonnes
    else if (pNumero > retournerNombreMaxColonnesTarifs()) {
      if (pAutoCorrection) {
        pNumero = retournerNombreMaxColonnesTarifs();
      }
      else {
        throw new MessageErreurException("La valeur de la colonne est de " + pNumero
            + ". Le numéro de la colonne des tarifs doit être compris entre 1 et " + 10 + '.');
      }
    }
    
    // Retourne une valeur valide (corrigée dans le cas d'une erreur)
    return pNumero;
  }
  
  /**
   * Contôle la validité de l'indice de marge passé en paramètre et correction de la valeur en cas d'erreur.
   */
  public BigDecimal controlerValiditeIndiceDeMarge(BigDecimal pIndiceMarge, boolean pAutoCorrection) {
    if (pIndiceMarge.compareTo(BigDecimal.ZERO) < 0) {
      if (pAutoCorrection) {
        Trace.alerte(
            "La valeur de l'indice est de " + pIndiceMarge + ", après correction elle est de 1. L'indice de marge doit être positif.");
        pIndiceMarge = BigDecimal.ONE;
      }
      else {
        throw new MessageErreurException("La valeur de l'indice est de " + pIndiceMarge + ". L'indice de marge doit être positif.");
      }
    }
    
    // Retourne une valeur valide (corrigée dans le cas d'une erreur)
    return pIndiceMarge;
  }
  
  /**
   * Contôle la validité du taux de marque passé en paramètre et correction de la valeur en cas d'erreur.
   */
  public BigDecimal controlerValiditeTauxDeMarque(BigDecimal pTauxDeMarque, boolean pAutoCorrection) {
    if (pTauxDeMarque.compareTo(BigDecimal.ZERO) < 0) {
      
      if (pAutoCorrection) {
        Trace.alerte("La valeur du taux est de " + pTauxDeMarque
            + ", après correction elle est de 0. Le taux de marque doit être compris entre 0 et 100 inclus.");
        pTauxDeMarque = BigDecimal.ZERO;
      }
      else {
        throw new MessageErreurException(
            "La valeur du taux est de " + pTauxDeMarque + ". Le taux de marque doit être compris entre 0 et 100 inclus.");
      }
    }
    else if (pTauxDeMarque.compareTo(Constantes.VALEUR_CENT) > 0) {
      if (pAutoCorrection) {
        Trace.alerte("La valeur du taux est de " + pTauxDeMarque
            + ", après correction elle est de 100. Le taux de marque doit être compris entre 0 et 100 inclus.");
        pTauxDeMarque = new BigDecimal(100);
      }
      else {
        throw new MessageErreurException(
            "La valeur du taux est de " + pTauxDeMarque + ". Le taux de marque doit être compris entre 0 et 100 inclus.");
      }
    }
    
    // Retourne une valeur valide (corrigée dans le cas d'une erreur)
    return pTauxDeMarque;
  }
  
  /**
   * Calculer l'indice de marge.
   * @return L'indice de marge.
   */
  public BigDecimal calculerIndiceDeMarge(int pArrondiPrix) {
    // Détermination du prix de revient à utiliser
    BigDecimal prixDeRevientHT = prixDeRevientStandardHT;
    if (prixDeRevientLigneHT != null && prixDeRevientLigneHT.compareTo(BigDecimal.ZERO) != 0) {
      prixDeRevientHT = prixDeRevientLigneHT;
    }
    // Contrôle des données avant le calcul
    if (getPrixNetHT() == null || prixDeRevientHT == null || prixDeRevientHT.compareTo(BigDecimal.ZERO) == 0) {
      return null;
    }
    // Calcul de l'indice
    return ArrondiPrix.appliquer(getPrixNetHT().divide(prixDeRevientHT, MathContext.DECIMAL32).multiply(Constantes.VALEUR_CENT),
        pArrondiPrix);
  }
  
  /**
   * Calcule le taux de marque (marge).
   */
  public BigDecimal calculerTauxDeMarque(int pArrondiPrix) {
    // Détermination du prix de revient à utiliser
    BigDecimal prixDeRevientHT = prixDeRevientStandardHT;
    if (prixDeRevientLigneHT != null && prixDeRevientLigneHT.compareTo(BigDecimal.ZERO) != 0) {
      prixDeRevientHT = prixDeRevientLigneHT;
    }
    // Contrôle des données avant le calcul
    if (getPrixNetHT() == null || getPrixNetHT().compareTo(BigDecimal.ZERO) == 0 || prixDeRevientHT == null) {
      return null;
    }
    BigDecimal valeur = getPrixNetHT().subtract(prixDeRevientHT);
    return ArrondiPrix.appliquer(valeur.divide(getPrixNetHT(), MathContext.DECIMAL32).multiply(Constantes.VALEUR_CENT), pArrondiPrix);
  }
  
  /**
   * Modifier la quantité en unité de vente d'articles.
   */
  public void modifierQuantiteUV(CalculPrixVente pCalculPrixVente, BigDecimal pQuantite) {
    boolean changerSens = false;
    if ((pQuantite.compareTo(BigDecimal.ZERO) < 0 && getQuantiteUV().compareTo(BigDecimal.ZERO) >= 0)
        || (pQuantite.compareTo(BigDecimal.ZERO) >= 0 && getQuantiteUV().compareTo(BigDecimal.ZERO) < 0)) {
      changerSens = true;
    }
    
    setQuantiteUV(pQuantite);
    
    // Pour les articles consignés, il faut mettre à jour les prix si la quantité change de signe car on passe d'un prix consigné à un
    // prix déconsigné ou vice versa
    if (consignable && changerSens) {
      // TODO Voir quoi faire, en théorie le composant de calcul de prix devrait faire le taf
      // initialiserPrixArticleConsignable();
    }
    
    // Contrôle avant de lancer un recalul
    if (pCalculPrixVente == null || pCalculPrixVente.getParametreLigneVente() == null
        || Constantes.equals(pCalculPrixVente.getParametreLigneVente().getQuantiteUV(), pQuantite)) {
      return;
    }
    else {
      // Recalcul du prix de vente
      pCalculPrixVente.getParametreLigneVente().setQuantiteUV(pQuantite);
      pCalculPrixVente.calculer();
      initialiserPrixVente(pCalculPrixVente);
    }
  }
  
  /**
   * Modifier le prix base (HT ou TTC) et relancer le calcul de prix.
   * @param pCalculPrixVente Le calcul de prix.
   * @param pPrixBaseSaisi Le prix de base saisi.
   * @param pModeFacturationTTC La facturation est-elle en TTC ? true ou false.
   * @param pModeNegoce Le mode de calcul de prix est-il en mode négoce ? true ou false.
   */
  public void modifierPrixBase(CalculPrixVente pCalculPrixVente, BigDecimal pPrixBaseSaisi, boolean pModeFacturationTTC,
      boolean pModeNegoce) {
    // En mode négoce, cette modification est ignorée
    if (pModeNegoce) {
      return;
    }
    if (pCalculPrixVente == null) {
      throw new MessageErreurException("Le paramètre pCalculPrixVente est incorrect.");
    }
    
    // Relancer un calcul suite à la modification du prix net
    if (pModeFacturationTTC) {
      controlerValiditePrixBaseSaisiTTC(pPrixBaseSaisi, false);
      pCalculPrixVente.getParametreLigneVente().setPrixBaseTTC(pPrixBaseSaisi);
    }
    else {
      controlerValiditePrixBaseSaisiHT(pPrixBaseSaisi, false);
      pCalculPrixVente.getParametreLigneVente().setPrixBaseHT(pPrixBaseSaisi);
    }
    pCalculPrixVente.getParametreLigneVente().setProvenancePrixBase(EnumProvenancePrixBase.SAISIE_LIGNE_VENTE);
    pCalculPrixVente.calculer();
    
    // Mise à jour des données de prix après le recalcul
    initialiserPrixVente(pCalculPrixVente);
  }
  
  /**
   * Modifie le numéro de la colonne de la ligne de ventes.
   * @param pCalculPrixVente Le calcul du prix de vente.
   * @param pNumeroColonne Le nouveau numéro de la colonne tarif.
   * @param pModeFacturationTTC La facturation est-elle en TTC ? true ou false.
   * @param pModeNegoce Le mode de calcul de prix est-il en mode négoce ? true ou false.
   */
  public void modifierNumeroColonneTarif(CalculPrixVente pCalculPrixVente, Integer pNumeroColonne, boolean pModeFacturationTTC,
      boolean pModeNegoce) {
    if (pCalculPrixVente == null) {
      throw new MessageErreurException("Le paramètre pCalculPrixVente est incorrect.");
    }
    
    // Contrôler le numéro de la colonne tarif
    controlerValiditeNumeroColonne(pNumeroColonne, false);
    if (Constantes.equals(numeroColonneTarif, pNumeroColonne)) {
      return;
    }
    
    // En mode négoce
    if (pModeNegoce) {
      // Mise à zéro des provenances
      pCalculPrixVente.mettreAZeroProvenance();
    }
    
    // Mettre à jour le numéro de la colonne tarif et sa provenance
    pCalculPrixVente.getParametreLigneVente().setNumeroColonneTarif(pNumeroColonne);
    pCalculPrixVente.getParametreLigneVente().setProvenanceColonneTarif(EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE);
    pCalculPrixVente.calculer();
    
    // Mise à jour des données de prix après le recalcul
    initialiserPrixVente(pCalculPrixVente);
  }
  
  /**
   * Modifie le taux de la remise du rang 1 et relancer le calcul de prix.
   * @param pCalculPrixVente Le calcul de prix.
   * @param pTauxRemiseUnitaireSaisi Le taux de remise saisi.
   * @param pModeFacturationTTC La facturation est-elle en TTC ? true ou false.
   * @param pModeNegoce Le mode de calcul de prix est-il en mode négoce ? true ou false.
   */
  public void modifierTauxRemise(CalculPrixVente pCalculPrixVente, BigDecimal pTauxRemiseUnitaireSaisi, boolean pModeFacturationTTC,
      boolean pModeNegoce) {
    if (pCalculPrixVente == null) {
      throw new MessageErreurException("Le paramètre pCalculPrixVente est incorrect.");
    }
    
    // Contrôle du taux de remise
    BigDecimal tauxRemiseUnitaireActuel = null;
    if (pCalculPrixVente.getTauxRemiseUnitaire() != null) {
      tauxRemiseUnitaireActuel = pCalculPrixVente.getTauxRemiseUnitaire().getTauxEnPourcentage();
    }
    if (Constantes.equals(pTauxRemiseUnitaireSaisi, tauxRemiseUnitaireActuel)) {
      return;
    }
    pTauxRemiseUnitaireSaisi = controlerValiditeTauxRemise(pTauxRemiseUnitaireSaisi, false);
    
    // En mode négoce
    if (pModeNegoce) {
      pCalculPrixVente.mettreAZeroProvenance();
      // Force la colonne à 1
      pCalculPrixVente.getParametreLigneVente().setNumeroColonneTarif(1);
      pCalculPrixVente.getParametreLigneVente().setProvenanceColonneTarif(EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE);
      
      // Calculer le prix net avec cette nouvelle remise
      if (pModeFacturationTTC) {
        BigDecimal prixNetTTC = OutilCalculPrix.appliquerTauxRemiseSurPrix(getPrixBaseTTC(), pTauxRemiseUnitaireSaisi,
            pCalculPrixVente.getArrondiNombreDecimale());
        pCalculPrixVente.getParametreLigneVente().setPrixNetSaisiTTC(prixNetTTC);
      }
      else {
        BigDecimal prixNetHT = OutilCalculPrix.appliquerTauxRemiseSurPrix(getPrixBaseHT(), pTauxRemiseUnitaireSaisi,
            pCalculPrixVente.getArrondiNombreDecimale());
        pCalculPrixVente.getParametreLigneVente().setPrixNetSaisiHT(prixNetHT);
      }
      pCalculPrixVente.getParametreLigneVente().setProvenancePrixNet(EnumProvenancePrixNet.SAISIE_LIGNE_VENTE);
      pCalculPrixVente.calculer();
    }
    // En mode classique
    else {
      pCalculPrixVente.getParametreLigneVente()
          .setTauxRemise1(new BigPercentage(pTauxRemiseUnitaireSaisi, EnumFormatPourcentage.POURCENTAGE));
      pCalculPrixVente.getParametreLigneVente().setTauxRemise2(BigPercentage.ZERO);
      pCalculPrixVente.getParametreLigneVente().setTauxRemise3(BigPercentage.ZERO);
      pCalculPrixVente.getParametreLigneVente().setTauxRemise4(BigPercentage.ZERO);
      pCalculPrixVente.getParametreLigneVente().setTauxRemise5(BigPercentage.ZERO);
      pCalculPrixVente.getParametreLigneVente().setTauxRemise6(BigPercentage.ZERO);
      pCalculPrixVente.getParametreLigneVente().setAssietteTauxRemise(EnumAssietteTauxRemise.PRIX_UNITAIRE);
      pCalculPrixVente.getParametreLigneVente().setProvenanceTauxRemise(EnumProvenanceTauxRemise.SAISIE_LIGNE_VENTE);
      pCalculPrixVente.calculer();
    }
    
    // Mise à jour des données de prix après le recalcul
    initialiserPrixVente(pCalculPrixVente);
  }
  
  /**
   * Modifier le prix net (HT ou TTC) et relancer le calcul de prix.
   * @param pCalculPrixVente Le calcul de prix.
   * @param pPrixNetSaisi Le prix net saisi.
   * @param pModeFacturationTTC La facturation est-elle en TTC ? true ou false.
   * @param pModeNegoce Le mode de calcul de prix est-il en mode négoce ? true ou false.
   */
  public void modifierPrixNet(CalculPrixVente pCalculPrixVente, BigDecimal pPrixNetSaisi, boolean pModeFacturationTTC,
      boolean pModeNegoce) {
    if (pCalculPrixVente == null) {
      throw new MessageErreurException("Le paramètre pCalculPrixVente est incorrect.");
    }
    
    // Relancer un calcul suite à la modification du prix net
    if (pModeFacturationTTC) {
      controlerValiditePrixNetSaisiTTC(pPrixNetSaisi, false);
      pCalculPrixVente.getParametreLigneVente().setPrixNetSaisiTTC(pPrixNetSaisi);
    }
    else {
      controlerValiditePrixNetSaisiHT(pPrixNetSaisi, false);
      pCalculPrixVente.getParametreLigneVente().setPrixNetSaisiHT(pPrixNetSaisi);
    }
    
    // En mode négoce
    if (pModeNegoce) {
      pCalculPrixVente.mettreAZeroProvenance();
    }
    pCalculPrixVente.getParametreLigneVente().setProvenancePrixNet(EnumProvenancePrixNet.SAISIE_LIGNE_VENTE);
    pCalculPrixVente.calculer();
    
    // Mise à jour des données de prix après le recalcul
    initialiserPrixVente(pCalculPrixVente);
  }
  
  // -- Méthodes statiques
  
  /**
   * Contrôle que tous les prix de revient soit supérieurs à zéro, si ce n'est pas le cas on récupère celui de la condition d'achat.
   * Les paramètres pIdSession et pLigneVente sont obligatoires.
   * Les paramètres pArticle et pConditionAchat sont facultatifs.
   */
  public static void controlerPrixDeRevient(IdSession pIdSession, LigneVente pLigneVente, Article pArticle,
      ConditionAchat pConditionAchat) {
    if (pLigneVente == null) {
      return;
    }
    
    if (pLigneVente.getPrixDeRevientLigneHT() != null && pLigneVente.getPrixDeRevientLigneHT().compareTo(BigDecimal.ZERO) > 0) {
      return;
    }
    
    // Récupération des informations de l'article si besoin
    if (pArticle == null || pArticle.getCoefficientUSParUV() == null) {
      pArticle = new Article(pLigneVente.getIdArticle());
      pArticle = ManagerServiceArticle.completerArticleStandard(pIdSession, pArticle);
      if (pArticle == null) {
        throw new MessageErreurException("Les données de l'article n'ont pas pu être récupérées.");
      }
    }
    
    // Récupération de la condition d'achat si besoin
    if (pConditionAchat == null) {
      pConditionAchat = ManagerServiceDocumentVente.chargerConditionAchat(pIdSession, pLigneVente);
      if (pConditionAchat == null) {
        throw new MessageErreurException("La condition d'achat de la ligne de vente n'a pas pu être récupérée.");
      }
    }
    
    // Mise à jour du prix de revient de la ligne de vente
    if (pArticle.getCoefficientUSParUV() == null) {
      throw new MessageErreurException(
          "Le nombre d'unité de stocks par unité de vente pour l'article " + pArticle.getId() + " est invalide.");
    }
    
    BigDecimal prixRevient = pConditionAchat.getPrixRevientStandardHTEnUV(pArticle.getCoefficientUSParUV());
    pLigneVente.setPrixDeRevientLigneHT(prixRevient);
    pLigneVente.setPrixDeRevientLigneTTC(
        OutilCalculPrix.calculerPrixTTC(prixRevient, pLigneVente.getPourcentageTVA(), Constantes.DEUX_DECIMALES));
  }
  
  /**
   * Retourner le taux de remise unitaire.
   * Ce taux est calculé systématiquement en mode classique car il n'est pas persisté en base. Puisqu'il y a 6 remises possibles avec des
   * modes d'applications différents. Donc un taux de remise unitaire entre le prix de base et le prix net est calculé.
   * Par contre en mode négoce, c'est le contenu du champ de la remise1 qui est retournée puisqu'un seul de taux de remise est possible et
   * non les 6.
   * @return
   */
  public BigPercentage getTauxRemiseUnitaire(boolean pModeNegoce) {
    // En mode négoce c'est la remise 1 qui est affichée
    if (pModeNegoce) {
      return getTauxRemise1();
    }
    
    // En mode classique
    // Si la remise a été saisi et que seule la remise 1 est renseignée alors c'est la remise 1 qui est affichée
    if (getTauxRemise1() != null && !getTauxRemise1().isZero() && (getTauxRemise2() == null || getTauxRemise2().isZero())
        && (getTauxRemise3() == null || getTauxRemise3().isZero()) && (getTauxRemise4() == null || getTauxRemise4().isZero())
        && (getTauxRemise5() == null || getTauxRemise5().isZero()) && (getTauxRemise6() == null || getTauxRemise6().isZero())) {
      return getTauxRemise1();
    }
    
    // Sinon le taux de la remise unitaire est calculé
    if (isClientTTC) {
      return OutilCalculPrix.calculerTauxRemise(getPrixBaseTTC(), getPrixNetTTC());
    }
    return OutilCalculPrix.calculerTauxRemise(getPrixBaseHT(), getPrixNetHT());
  }
  
  /**
   * Modifier la quantité de la ligne en unités de ventes (UV).
   * 
   * La précision du BigDecimal fourni est ignorée. En effet, la quantité fournie est arrondie avec la précision de l'unité de ventes
   * (UV) stockée dans la ligne de ventes. Si la précision n'est pas définie, la méthode retourne une exception.
   */
  @Override
  public void setQuantiteUV(BigDecimal pQuantiteUV) {
    super.setQuantiteUV(pQuantiteUV);
    
    // Modifier le montant TTC de la ligne (mais sans relancer un calcul de prix)
    if (getPrixNetTTC() != null) {
      montantTTC = ArrondiPrix.appliquer(getQuantiteUV().multiply(getPrixNetTTC()), Constantes.DEUX_DECIMALES);
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Calcule la dimension unitaire d'un article découpable.
   * Attention les dimensions sont stockées en mètre dans la XLI de la ligne de vente.
   */
  private BigDecimal getDimensionUnitaire(Article pArticle) {
    if (pArticle == null) {
      throw new MessageErreurException("L'article est invalide.");
    }
    if (pArticle.getTypeDecoupe() == null) {
      throw new MessageErreurException("Le type de dimension de l'article découpable est invalide.");
    }
    
    // Conversion de mesure1, mesure2 et mesure3 en mêtre s'ils sont dans une unité différente
    BigDecimal longueur = mesure1;
    if (pArticle.getTypeUniteLongueur() != null && !pArticle.getTypeUniteLongueur().equals(Article.TYPE_UNITE_DIMENSION_M)) {
      if (mesure1 != null && mesure1.compareTo(BigDecimal.ZERO) != 0) {
        longueur = Article.convertirEnMetres(mesure1, pArticle.getTypeUniteLongueur(), false);
      }
    }
    BigDecimal largeur = mesure2;
    if (pArticle.getTypeUniteLargeur() != null && !pArticle.getTypeUniteLargeur().equals(Article.TYPE_UNITE_DIMENSION_M)) {
      if (mesure2 != null && mesure2.compareTo(BigDecimal.ZERO) != 0) {
        largeur = Article.convertirEnMetres(mesure2, pArticle.getTypeUniteLargeur(), false);
      }
    }
    BigDecimal hauteur = mesure3;
    if (pArticle.getTypeUniteHauteur() != null && !pArticle.getTypeUniteHauteur().equals(Article.TYPE_UNITE_DIMENSION_M)) {
      if (mesure3 != null && mesure3.compareTo(BigDecimal.ZERO) != 0) {
        hauteur = Article.convertirEnMetres(mesure3, pArticle.getTypeUniteHauteur(), false);
      }
    }
    
    // Choix de la formule en fonction du type de la dimension de l'article
    BigDecimal dimension = BigDecimal.ZERO;
    switch (pArticle.getTypeDecoupe()) {
      case IMPOSSIBLE:
        throw new MessageErreurException("L'article n'est pas un article découpable.");
      case LONGUEUR:
        dimension = longueur.setScale(getNombreDecimaleUV(), RoundingMode.HALF_UP);
        break;
      case SURFACE:
        dimension = longueur.multiply(largeur).setScale(getNombreDecimaleUV(), RoundingMode.HALF_UP);
        break;
      case VOLUME:
        dimension = longueur.multiply(largeur).multiply(hauteur).setScale(getNombreDecimaleUV(), RoundingMode.HALF_UP);
        break;
    }
    if (dimension.compareTo(BigDecimal.ZERO) == 0) {
      throw new MessageErreurException(
          "La dimension unitaire de l'article vaut zéro ce qui n'est pas normal pour un article découpable.");
    }
    return dimension;
  }
  
  /**
   * Retourne le nombre de colonnes du tarif max en fonction du mode.
   */
  private int retournerNombreMaxColonnesTarifs() {
    if (modeNegoce != null && modeNegoce.booleanValue()) {
      return TarifArticle.NOMBRE_COLONNES_MODE_NEGOCE;
    }
    return TarifArticle.NOMBRE_COLONNES_MODE_CLASSIQUE;
  }
  
  // -- Accesseurs
  
  /**
   * Retourner le code etat de la ligne.
   */
  public int getCodeEtat() {
    return codeEtat;
  }
  
  /**
   * Modifier le code etat de la ligne.
   */
  public void setCodeEtat(Integer pCodeEtat) {
    codeEtat = pCodeEtat;
  }
  
  /**
   * Initialise les quantités pour un article découpable.
   */
  public void setQuantiteArticlesDecoupe(BigDecimal quantiteCommandeeUC, Article pArticle) {
    if (!isArticleDecoupable()) {
      return;
    }
    // On initialise le nombre d'élément avec la quantité UC
    BigDecimal dimensionTotale = quantiteCommandeeUC;
    setNombreDecoupe(dimensionTotale.intValue());
    // On calcule la quantité UV
    dimensionTotale = dimensionTotale.multiply(getDimensionUnitaire(pArticle));
    setQuantiteUV(dimensionTotale);
  }
  
  /**
   * Retourne true si le nombre d'unité de stocks (US) par unités de de ventes (UV) est défini est différent de 1.
   * On considère que si ce coefficient n'est pas définit ou qu'il a la valeur zéro, cela équivaut à la valeur 1.
   */
  public boolean isDefiniNombreUSParUV() {
    return nombreUSParUV != null && nombreUSParUV.compareTo(BigDecimal.ZERO) != 0 && nombreUSParUV.compareTo(BigDecimal.ONE) != 0;
  }
  
  /**
   * Nombre d'unités de stocks (US) par unité de ventes (UV).
   * Il est conseillé de vérifier que le coefficient est définit via la méthode isDefiniNombreUSParUV() avant de l'utiliser.
   * 
   * Cela correspond au coefficient KSV.
   * 
   * Il y a une subtilité lors de la lecture de ce coefficient. Ce coefficient étant stocké avec 3 chiffres après la virgule, on peut
   * perdre en précision lorsqu'une UV contient plusieurs US. Cela peut amener des erreurs d'arrondis lorsqu'on veut recalculer l'UV
   * à partir d'une US. Afin de retrouver la précision perdue, on utile l'inverse du nombre d'UV par UCV si celui-ci correspond au
   * nombre US par UV.
   * 
   * Exemple avec des plaques de 3 m² :
   * - US = plaque
   * - UV = m²
   * - UCV = plaque
   * - UV/UCV = 3.000
   * - US/UV = 0,333
   * Dans un calcul basique : 1 US = 1 US / 0.333 US/UV = 3.003 UV (c'est faux)
   * Dans le calcul amélioré, comme UV/UCV est l'inverse de US/UV : 1 US = 1 US / (1 / 3.000) = 3.000 UV
   */
  public BigDecimal getNombreUSParUV() {
    if (isDefiniNombreUVParUCV()) {
      // Calculer l'inverse du nombre d'UV par UCV avec une précision de 9 chiffres après la virgule
      BigDecimal inverseNombreUVParUCV = BigDecimal.ONE.divide(getNombreUVParUCV(), 9, RoundingMode.HALF_UP);
      
      // Arrondir le résultat à 3 chiffres après la virgule pour le comparer au nombre d'US par UV
      BigDecimal inverseArrondiNombreUVParUCV = inverseNombreUVParUCV.setScale(3, RoundingMode.HALF_UP);
      
      // Si les deux valeurs sont égales, on retourne l'inverse du nombre d'UV par UCV avec une précision de 9 chiffres après la virgule
      if (inverseArrondiNombreUVParUCV.equals(nombreUSParUV)) {
        return inverseNombreUVParUCV;
      }
    }
    return nombreUSParUV;
  }
  
  /**
   * Modifier le nombre d'unités de stocks (US) par unité de ventes (UV).
   */
  public void setNombreUSParUV(BigDecimal pNombreUSParUV) {
    nombreUSParUV = pNombreUSParUV;
  }
  
  /**
   * retourne le libellé du tarif
   */
  public String getLibelleTarif() {
    return libelleTarif;
  }
  
  /**
   * Met à jour le libellé du tarif
   */
  public void setLibelleTarif(String libelleTarif) {
    this.libelleTarif = libelleTarif;
  }
  
  /**
   * retourne le type de remise
   */
  public Character getTypeRemise() {
    return typeRemise;
  }
  
  /**
   * Met à jour le type de remise
   */
  public void setTypeRemise(Character typeRemise) {
    this.typeRemise = typeRemise;
  }
  
  /**
   * Retourner l'assiette du taux de remise.
   * @return
   */
  public EnumAssietteTauxRemise getAssietteTauxRemise() {
    return assietteTauxRemise;
  }
  
  /**
   * Modifier l'assiette du taux de remise
   * @param pAssietteTauxRemise L'assiette.
   */
  public void setAssietteTauxRemise(EnumAssietteTauxRemise pAssietteTauxRemise) {
    assietteTauxRemise = pAssietteTauxRemise;
  }
  
  /**
   * Retourner le prix net TTC.
   * 
   * Le prix net de la ligne de vente, celui qui est affiché dans le document de vente remis au client, est stocké dans :
   * - le prix net saisi lorsque le prix net a été saisi par l'utilisateur,
   * - le prix net calculé dans les autres cas.
   * C'est la provenance qui permet de sélectionner le cas de figure.
   * 
   * @return Prix net TTC.
   */
  public BigDecimal getPrixNetTTC() {
    if (getProvenancePrixNet() == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
      return prixNetSaisiTTC;
    }
    return prixNetCalculeTTC;
  }
  
  /**
   * Retourner le prix net HT ou TTC en fonction du mode de tarification de la ligne de vente.
   * 
   * Le prix net de la ligne de vente, celui qui est affiché dans le document de vente remis au client, est stocké dans :
   * - le prix net saisi lorsque le prix net a été saisi par l'utilisateur,
   * - le prix net calculé dans les autres cas.
   * C'est la provenance qui permet de sélectionner le cas de figure.
   * 
   * @return Prix net (HT ou TTC).
   */
  public BigDecimal getPrixNet() {
    if (getProvenancePrixNet() == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
      return getPrixNetSaisi();
    }
    else {
      return getPrixNetCalcule();
    }
  }
  
  /**
   * Retourner le prix net saisi TTC.
   * 
   * Il contient la valeur du prix net TTC saisi par l'utilisateur (EnumProvenancePrixNetHT = SAISIE_LIGNE_VENTE dans ce cas) ou
   * le prix net TTC issu d'une condition de vente (EnumProvenancePrixNetHT = CONDITION_VENTE dans ce cas). Dans les autres cas, le prix
   * net est stocké dans le prix net calculé.
   * 
   * @return Prix net saisi TTC.
   */
  public BigDecimal getPrixNetSaisiTTC() {
    return prixNetSaisiTTC;
  }
  
  /**
   * Modifier le prix net saisi TTC.
   * @param pPrixNetTTC Prix net saisi TTC.
   */
  public void setPrixNetSaisiTTC(BigDecimal pPrixNetTTC) {
    prixNetSaisiTTC = pPrixNetTTC;
  }
  
  /**
   * Retourner le prix net saisi HT ou TTC en fonction du mode de tarification de la ligne de vente.
   * 
   * Il contient la valeur du prix net saisi par l'utilisateur (EnumProvenancePrixNetHT = SAISIE_LIGNE_VENTE dans ce cas) ou
   * le prix net TTC issu d'une condition de vente (EnumProvenancePrixNetHT = CONDITION_VENTE dans ce cas). Dans les autres cas, le prix
   * net est stocké dans le prix net calculé. Le prix net saisi est retourné en HT ou en TTC suivant le mode de tarification de la ligne
   * de vente.
   * 
   * @return Prix net saisi (HT ou TTC).
   */
  public BigDecimal getPrixNetSaisi() {
    if (isClientTTC) {
      return prixNetSaisiTTC;
    }
    return getPrixNetSaisiHT();
  }
  
  /**
   * Retourner le prix net calculé TTC.
   * 
   * C'est le prix net TTC de la ligne de vente lorsqu'il a été calculé par l'algorithme de calcul du prix de vente.
   * 
   * @return Prix net calculé TTC.
   */
  public BigDecimal getPrixNetCalculeTTC() {
    return prixNetCalculeTTC;
  }
  
  /**
   * Modifier le prix net calculé TTC.
   * @param pPrixNetCalculeTTC Prix net calculé TTC.
   */
  public void setPrixNetCalculeTTC(BigDecimal pPrixNetCalculeTTC) {
    prixNetCalculeTTC = pPrixNetCalculeTTC;
  }
  
  /**
   * Retourner le prix net calculé HT ou TTC en fonction du mode de tarification de la ligne de vente.
   * 
   * C'est le prix net HT ou TTC de la ligne de vente lorsqu'il a été calculé par l'algorithme de calcul du prix de vente.
   * Le prix net calculé est retourné en HT ou en TTC suivant le mode de tarification de la ligne de vente.
   * 
   * @return Prix net calculé (HT ou TTC).
   */
  public BigDecimal getPrixNetCalcule() {
    if (isClientTTC) {
      return prixNetCalculeTTC;
    }
    return getPrixNetCalculeHT();
  }
  
  /**
   * Identifiant du magasin associé à la ligne de ventes.
   * 
   * Si l'identifiant du magasin associé à la ligne de ventes n'est pas définit (valeur null), c'est celui du document de ventes qui
   * doit être utilisé (cas le plus courant). Il est uniquement renseigné lorsqu'il est différent de celui de l'entête.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier l'identifiant du magasin associé à la ligne de ventes.
   * 
   * Rappel : si l'identifiant du magasin associé à la ligne de ventes n'est pas définit, c'est celui du document de ventes qui doit être
   * utilisé. Dans la plupart des cas, le magasin associé à la ligne de ventes n'est pas définit (valeur null). Définir un magasin pour
   * la ligne de ventes est utile lorsque le magasin de la ligne de ventes n'est pas le même que celui de l'entête du document.
   * C'est un cas rare qui sert lorsqu'un document de ventes est honoré par plusieurs magasins.
   * 
   * Remarque : recopier le magasin du document de ventes dans la ligne de ventes n'est pas équivalent à ne pas définir le magasin de la
   * ligne de ventes. Dans le premier cas, la ligne de ventes ne sera pas impacté si le magasin change dans le document de ventes. Dans
   * le second cas, la ligne de ventes suivra le changement puisque son magasin est null.
   * 
   * Cette logique "Série N" pose des problèmes pour les requêtes SQL. Faut-il la modifier ? Faut-il recopier systématiquement le
   * magasin dans toutes les lignes même s'il est identique à celui du document de ventes ?
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  /**
   * retourne la quantité en nombre de pièces
   */
  public BigDecimal getQuantitePieces() {
    return quantitePieces;
  }
  
  /**
   * Met à jour la quantité en nombre de pièces
   */
  public void setQuantitePieces(BigDecimal quantitePieces) {
    this.quantitePieces = quantitePieces;
  }
  
  /**
   * Retourner le numéro de la colonne tarif.
   * @return Le numéro de la colonne tarif.
   */
  public Integer getNumeroColonneTarif() {
    return numeroColonneTarif;
  }
  
  /**
   * Modifier le numéro de la colonne tarif.
   * @param pNumeroColonneTarif Le numero de la colonne tarif.
   */
  public void setNumeroColonneTarif(Integer pNumeroColonneTarif) {
    numeroColonneTarif = pNumeroColonneTarif;
  }
  
  /**
   * Retourner les indicateurs (?).
   */
  public String getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * Modifier les indicateurs.
   */
  public void setIndicateurs(String pIndicateurs) {
    this.indicateurs = pIndicateurs;
  }
  
  /**
   * Retourne la première mesure (longueur) pour les articles découpables.
   * La longueur est stockée en mètre.
   */
  public BigDecimal getMesure1() {
    return mesure1;
  }
  
  /**
   * Met à jour la première mesure (longueur) pour les articles découpables.
   * La longueur est stockée en mètre.
   */
  public void setMesure1(BigDecimal pMesure1) {
    if (getNombreDecimaleUV() != null) {
      mesure1 = pMesure1.setScale(getNombreDecimaleUV(), RoundingMode.HALF_UP);
    }
    else {
      mesure1 = pMesure1;
    }
  }
  
  /**
   * Retourne la deuxième mesure (largeur) pour les articles découpables.
   * La largeur est stockée en mètre.
   */
  public BigDecimal getMesure2() {
    return mesure2;
  }
  
  /**
   * Met à jour la deuxième mesure (largeur) pour les articles découpables
   * La largeur est stockée en mètre.
   */
  public void setMesure2(BigDecimal pMesure2) {
    if (getNombreDecimaleUV() != null) {
      mesure2 = pMesure2.setScale(getNombreDecimaleUV(), RoundingMode.HALF_UP);
    }
    else {
      mesure2 = pMesure2;
    }
  }
  
  /**
   * Retourne la troisième mesure (hauteur) pour les articles découpables.
   * La hauteur est stockée en mètre.
   */
  public BigDecimal getMesure3() {
    return mesure3;
  }
  
  /**
   * Met à jour la troisième mesure (hauteur) pour les articles découpables.
   * La hauteur est stockée en mètre.
   */
  public void setMesure3(BigDecimal pMesure3) {
    if (getNombreDecimaleUV() != null) {
      mesure3 = pMesure3.setScale(getNombreDecimaleUV(), RoundingMode.HALF_UP);
    }
    else {
      mesure3 = pMesure3;
    }
  }
  
  /**
   * retourne la première exclusion de remise en pied de document
   */
  public Character getExclusionRemisePied1() {
    return exclusionRemisePied1;
  }
  
  /**
   * Met à jour la première exclusion de remise en pied de document
   */
  public void setExclusionRemisePied1(Character exclusionRemisePied1) {
    this.exclusionRemisePied1 = exclusionRemisePied1;
  }
  
  /**
   * retourne la deuxième exclusion de remise en pied de document
   */
  public Character getExclusionRemisePied2() {
    return exclusionRemisePied2;
  }
  
  /**
   * Met à jour la deuxième exclusion de remise en pied de document
   */
  public void setExclusionRemisePied2(Character exclusionRemisePied2) {
    this.exclusionRemisePied2 = exclusionRemisePied2;
  }
  
  /**
   * retourne la troisième exclusion de remise en pied de document
   */
  public Character getExclusionRemisePied3() {
    return exclusionRemisePied3;
  }
  
  /**
   * Met à jour la troisième exclusion de remise en pied de document
   */
  public void setExclusionRemisePied3(Character exclusionRemisePied3) {
    this.exclusionRemisePied3 = exclusionRemisePied3;
  }
  
  /**
   * retourne la quatrième exclusion de remise en pied de document
   */
  public Character getExclusionRemisePied4() {
    return exclusionRemisePied4;
  }
  
  /**
   * Met à jour la quatrième exclusion de remise en pied de document
   */
  public void setExclusionRemisePied4(Character exclusionRemisePied4) {
    this.exclusionRemisePied4 = exclusionRemisePied4;
  }
  
  /**
   * retourne la cinquième exclusion de remise en pied de document
   */
  public Character getExclusionRemisePied5() {
    return exclusionRemisePied5;
  }
  
  /**
   * Met à jour la cinquième exclusion de remise en pied de document
   */
  public void setExclusionRemisePied5(Character exclusionRemisePied5) {
    this.exclusionRemisePied5 = exclusionRemisePied5;
  }
  
  /**
   * retourne la sixième exclusion de remise en pied de document
   */
  public Character getExclusionRemisePied6() {
    return exclusionRemisePied6;
  }
  
  /**
   * Met à jour la sixième exclusion de remise en pied de document
   */
  public void setExclusionRemisePied6(Character exclusionRemisePied6) {
    this.exclusionRemisePied6 = exclusionRemisePied6;
  }
  
  /**
   * retourne le prix promotionnel
   */
  public BigDecimal getPrixPromo() {
    return prixPromo;
  }
  
  /**
   * Met à jour le prix promotionnel
   */
  public void setPrixPromo(BigDecimal prixPromo) {
    this.prixPromo = prixPromo;
  }
  
  /**
   * Retourner le coefficient multiplicateur du prix de base.
   */
  public BigDecimal getCoefficient() {
    return coefficient;
  }
  
  /**
   * Mettre à jour le coefficient multiplicateur du prix de base
   */
  public void setCoefficient(BigDecimal pCoefficient) {
    this.coefficient = pCoefficient;
  }
  
  /**
   * Retourner le code de la ligne d'avoir.
   */
  public Character getCodeLigneAvoir() {
    return codeLigneAvoir;
  }
  
  /**
   * Met à jour le code de la ligne d'avoir
   */
  public void setCodeLigneAvoir(Character codeLigneAvoir) {
    this.codeLigneAvoir = codeLigneAvoir;
  }
  
  /**
   * Retourner l'identifiant du représentant.
   */
  public IdRepresentant getIdRepresentant() {
    return idRepresentant;
  }
  
  /**
   * Modifier l'identifiant du représentant.
   */
  public void setIdRepresentant(IdRepresentant pIdRepresentant) {
    this.idRepresentant = pIdRepresentant;
  }
  
  /**
   * retourne le prix flash
   */
  public PrixFlash getPrixFlash() {
    return prixFlash;
  }
  
  /**
   * Met à jour le prix flash
   */
  public void setPrixFlash(PrixFlash prixFlash) {
    this.prixFlash = prixFlash;
  }
  
  /**
   * retourne le code TVA
   */
  public int getCodeTVA() {
    return codeTVA;
  }
  
  /**
   * Met à jour le code TVA
   */
  public void setCodeTVA(int codeTVA) {
    this.codeTVA = codeTVA;
  }
  
  /**
   * Retourner la provenance du coefficient.
   * @return La provenance du coefficient.
   */
  public EnumProvenanceCoefficient getProvenanceCoefficient() {
    return provenanceCoefficient;
  }
  
  /**
   * Modifier la provenance du coefficient.
   * @param pProvenanceCoefficient La provenance du coefficient.
   */
  public void setProvenanceCoefficient(EnumProvenanceCoefficient pProvenanceCoefficient) {
    provenanceCoefficient = pProvenanceCoefficient;
  }
  
  /**
   * Retourner la provenance de la colonne tarif.
   * @return La provenance de la colonne tarif.
   */
  public EnumProvenanceColonneTarif getProvenanceColonneTarif() {
    return provenanceColonneTarif;
  }
  
  /**
   * Modifier la provenance de la colonne tarif.
   * @param pProvenanceColonneTarif Provenance de la colonne tarif.
   */
  public void setProvenanceColonneTarif(EnumProvenanceColonneTarif pProvenanceColonneTarif) {
    provenanceColonneTarif = pProvenanceColonneTarif;
  }
  
  /**
   * Retourner la provenance du prix de base.
   * @return La provenance du prix de base.
   */
  public EnumProvenancePrixBase getProvenancePrixBase() {
    return provenancePrixBase;
  }
  
  /**
   * Modifier la provenance du prix de base.
   * @param pProvenancePrixBase La provenance du prix de base.
   */
  public void setProvenancePrixBase(EnumProvenancePrixBase pProvenancePrixBase) {
    provenancePrixBase = pProvenancePrixBase;
  }
  
  /**
   * Retourner la provenance du taux de remise.
   * @return La provenance du taux de remise.
   */
  public EnumProvenanceTauxRemise getProvenanceTauxRemise() {
    return provenanceTauxRemise;
  }
  
  /**
   * Modifier la provenance du taux de remise.
   * @param pProvenanceTauxRemise La provenance du taux de remise.
   */
  public void setProvenanceTauxRemise(EnumProvenanceTauxRemise pProvenanceTauxRemise) {
    provenanceTauxRemise = pProvenanceTauxRemise;
  }
  
  /**
   * Retourner la provenance du prix de revient.
   * @return La provenance du prix de revient.
   */
  public Character getProvenancePrixDeRevient() {
    return provenancePrixDeRevient;
  }
  
  /**
   * Modifier la provenance du prix de revient.
   * @param pProvenancePrixDeRevient La provenance du prix de revient.
   */
  public void setProvenancePrixDeRevient(Character pProvenancePrixDeRevient) {
    provenancePrixDeRevient = pProvenancePrixDeRevient;
  }
  
  /**
   * Retourner le top indiquant que la ligne est en valeur.
   */
  public int getTopLigneEnValeur() {
    return topLigneEnValeur;
  }
  
  /**
   * Met à jour le top indiquant que la ligne est en valeur
   */
  public void setTopLigneEnValeur(int topLigneEnValeur) {
    this.topLigneEnValeur = topLigneEnValeur;
  }
  
  /**
   * retourne le numéro de colonne de la TVA
   */
  public int getNumeroColonneTVA() {
    return numeroColonneTVA;
  }
  
  /**
   * Met à jour le numéro de colonne de la TVA
   */
  public void setNumeroColonneTVA(int numeroColonneTVA) {
    this.numeroColonneTVA = numeroColonneTVA;
  }
  
  /**
   * retourne le code TPF (taxe parafiscale)
   */
  public int getCodeTPF() {
    return codeTPF;
  }
  
  /**
   * Met à jour le code TPF (taxe parafiscale)
   */
  public void setCodeTPF(int codeTPF) {
    this.codeTPF = codeTPF;
  }
  
  /**
   * retourne le top indiquant que la ligne ne sera pas prise en compte pour le commissionnemment du représentant
   */
  public int getTopNonCommissionne() {
    return topNonCommissionne;
  }
  
  /**
   * Met à jour le top indiquant que la ligne ne sera pas prise en compte pour le commissionnemment du représentant
   */
  public void setTopNonCommissionne(int topNonCommissionne) {
    this.topNonCommissionne = topNonCommissionne;
  }
  
  /**
   * retourne le signe de la ligne (négatif = ligne d'avoir)
   */
  public int getSigneLigne() {
    return signeLigne;
  }
  
  /**
   * Met à jour le signe de la ligne (négatif = ligne d'avoir)
   */
  public void setSigneLigne(int signeLigne) {
    this.signeLigne = signeLigne;
  }
  
  /**
   * retourne le top indiquant que la ligne comprend un numéro de série ou de lot
   */
  public int getTopNumSerieOuLot() {
    return topNumSerieOuLot;
  }
  
  /**
   * Met à jour le top indiquant que la ligne comprend un numéro de série ou de lot
   */
  public void setTopNumSerieOuLot(int topNumSerieOuLot) {
    this.topNumSerieOuLot = topNumSerieOuLot;
  }
  
  /**
   * retourne le caractère spécifiant que la ligne n'est pas soumise à l'escompte
   */
  public Character getNonSoumisEscompte() {
    return nonSoumisEscompte;
  }
  
  /**
   * Met à jour le caractère spécifiant que la ligne n'est pas soumise à l'escompte
   */
  public void setNonSoumisEscompte(Character nonSoumisEscompte) {
    this.nonSoumisEscompte = nonSoumisEscompte;
  }
  
  /**
   * retourne le prix de transport
   */
  public BigDecimal getPrixTransport() {
    return prixTransport;
  }
  
  /**
   * Met à jour le prix de transport
   */
  public void setPrixTransport(BigDecimal prixTransport) {
    this.prixTransport = prixTransport;
  }
  
  /**
   * retourne le montant de transport
   */
  public BigDecimal getMontantTransport() {
    return montantTransport;
  }
  
  /**
   * Met à jour le montant de transport
   */
  public void setMontantTransport(BigDecimal montantTransport) {
    this.montantTransport = montantTransport;
  }
  
  /**
   * retourne la date de livraison prévue
   */
  public Date getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  /**
   * Met à jour la date de livraison prévue
   */
  public void setDateLivraisonPrevue(Date dateLivraisonPrevue) {
    this.dateLivraisonPrevue = dateLivraisonPrevue;
  }
  
  /**
   * Retourner l'identifiant du type de gratuit de la ligne de vente.
   * @return Identifiant du type de gratuit.
   */
  public IdTypeGratuit getIdTypeGratuit() {
    return idTypeGratuit;
  }
  
  /**
   * Modifier l'identifiant du type de gratuit de la ligne de vente.
   * @return Identifiant du type de gratuit.
   */
  public void setIdTypeGratuit(IdTypeGratuit pIdTypeGratuit) {
    idTypeGratuit = pIdTypeGratuit;
  }
  
  /**
   * retourne le top personnalisation 1
   */
  public String getTopPersonnalisation1() {
    return topPersonnalisation1;
  }
  
  /**
   * Met à jour le top personnalisation 1
   */
  public void setTopPersonnalisation1(String topPersonnalisation1) {
    this.topPersonnalisation1 = topPersonnalisation1;
  }
  
  /**
   * retourne le top personnalisation 2
   */
  public String getTopPersonnalisation2() {
    return topPersonnalisation2;
  }
  
  /**
   * Met à jour le top personnalisation 2
   */
  public void setTopPersonnalisation2(String topPersonnalisation2) {
    this.topPersonnalisation2 = topPersonnalisation2;
  }
  
  /**
   * retourne le top personnalisation 3
   */
  public String getTopPersonnalisation3() {
    return topPersonnalisation3;
  }
  
  /**
   * Met à jour le top personnalisation 3
   */
  public void setTopPersonnalisation3(String topPersonnalisation3) {
    this.topPersonnalisation3 = topPersonnalisation3;
  }
  
  /**
   * retourne le top personnalisation 4
   */
  public String getTopPersonnalisation4() {
    return topPersonnalisation4;
  }
  
  /**
   * Met à jour le top personnalisation 4
   */
  public void setTopPersonnalisation4(String topPersonnalisation4) {
    this.topPersonnalisation4 = topPersonnalisation4;
  }
  
  /**
   * retourne le top personnalisation 5
   */
  public String getTopPersonnalisation5() {
    return topPersonnalisation5;
  }
  
  /**
   * Met à jour le top personnalisation 5
   */
  public void setTopPersonnalisation5(String topPersonnalisation5) {
    this.topPersonnalisation5 = topPersonnalisation5;
  }
  
  /**
   * Ligne non éditable (sur aucun document) seulement si il y a un 9 en première position du 1er top de personnalisation
   */
  public boolean isEditionAucunDocument() {
    return getTopPersonnalisation1().equals("9 ");
  }
  
  /**
   * Ligne non éditable (sur aucun document) seulement si il y a un 9 en première position du 1er top de personnalisation
   */
  public void setEditionAucunDocument(boolean pEditionAucunDocument) {
    if (pEditionAucunDocument) {
      setTopPersonnalisation1("9 ");
    }
    else {
      setTopPersonnalisation1("");
      setTopPersonnalisation2("");
      setTopPersonnalisation3("");
      setTopPersonnalisation4("");
      setTopPersonnalisation5("");
    }
  }
  
  /**
   * Edition de la ligne sur tous les documents si les tops personnalisation sont tous à blanc ou tous remplis.
   */
  public boolean isEditionTousTypeDocuments() {
    return (getTopPersonnalisation1().equals("") && getTopPersonnalisation2().equals("") && getTopPersonnalisation3().equals("")
        && getTopPersonnalisation4().equals("") && getTopPersonnalisation5().equals(""))
        || (getTopPersonnalisation1().equals("X ") && getTopPersonnalisation2().equals("X ") && getTopPersonnalisation3().equals("X ")
            && getTopPersonnalisation4().equals("X ") && getTopPersonnalisation5().equals("XX"));
  }
  
  /**
   * Edition de la ligne sur tous les documents si les tops personnalisation sont tous à blanc (ou tous remplis, mais ici on met à blanc).
   */
  public void setEditionTousTypeDocuments(boolean pEditionTousTypeDocuments) {
    if (pEditionTousTypeDocuments) {
      setTopPersonnalisation1("");
      setTopPersonnalisation2("");
      setTopPersonnalisation3("");
      setTopPersonnalisation4("");
      setTopPersonnalisation5("");
    }
  }
  
  /**
   * Edition de la ligne sur les documents de type devis si le top personnalisation 5 a un X en deuxième position.
   */
  public boolean isEditionSurDevis() {
    if (getTopPersonnalisation5().length() == 2) {
      return getTopPersonnalisation5().substring(1).equals("X");
    }
    else {
      return false;
    }
  }
  
  /**
   * Edition de la ligne sur les documents de type devis si le top personnalisation 5 a un X en deuxième position.
   */
  public void setEditionSurDevis(boolean pEditionSurDevis) {
    if (pEditionSurDevis) {
      if (isEditionSurFacture()) {
        setTopPersonnalisation5("XX");
      }
      else {
        setTopPersonnalisation5(" X");
      }
    }
    else {
      if (isEditionSurFacture()) {
        setTopPersonnalisation5("X ");
      }
      else {
        setTopPersonnalisation5("  ");
      }
    }
  }
  
  /**
   * Edition de la ligne sur les documents de type commande si le top personnalisation 1 a un X en première position.
   */
  public boolean isEditionSurCommande() {
    return getTopPersonnalisation1().substring(1).equals("X");
  }
  
  /**
   * Edition de la ligne sur les documents de type commande si le top personnalisation 1 a un X en première position.
   */
  public void setEditionSurCommande(boolean pEditionSurCommande) {
    if (pEditionSurCommande) {
      setTopPersonnalisation1("X ");
    }
    else {
      setTopPersonnalisation1("  ");
    }
  }
  
  /**
   * Edition de la ligne sur les documents de type préparation si le top personnalisation 2 a un X en première position.
   */
  public boolean isEditionSurPreparation() {
    return getTopPersonnalisation2().substring(1).equals("X");
  }
  
  /**
   * Edition de la ligne sur les documents de type préparation si le top personnalisation 2 a un X en première position.
   */
  public void setEditionSurPreparation(boolean pEditionSurPreparation) {
    if (pEditionSurPreparation) {
      setTopPersonnalisation2("X ");
    }
    else {
      setTopPersonnalisation2("  ");
    }
  }
  
  /**
   * Edition de la ligne sur les documents de type bon si le top personnalisation 3 a un X en première position.
   */
  public boolean isEditionSurBon() {
    return getTopPersonnalisation3().substring(1).equals("X");
  }
  
  /**
   * Edition de la ligne sur les documents de type bon si le top personnalisation 3 a un X en première position.
   */
  public void setEditionSurBon(boolean pEditionSurBon) {
    if (pEditionSurBon) {
      setTopPersonnalisation3("X ");
    }
    else {
      setTopPersonnalisation3("  ");
    }
  }
  
  /**
   * Edition de la ligne sur les documents de type bon transporteur si le top personnalisation 4 a un X en première position.
   */
  public boolean isEditionSurBonTransporteur() {
    return getTopPersonnalisation4().substring(1).equals("X");
  }
  
  /**
   * Edition de la ligne sur les documents de type bon transporteur si le top personnalisation 4 a un X en première position.
   */
  public void setEditionSurBonTransporteur(boolean pEditionSurBonTransporteur) {
    if (pEditionSurBonTransporteur) {
      setTopPersonnalisation4("X ");
    }
    else {
      setTopPersonnalisation4("  ");
    }
  }
  
  /**
   * Edition de la ligne sur les documents de type facture si le top personnalisation 5 a un X en première position.
   */
  public boolean isEditionSurFacture() {
    if (getTopPersonnalisation5().length() == 2) {
      return getTopPersonnalisation5().substring(0, 1).equals("X");
    }
    else {
      return false;
    }
  }
  
  /**
   * Edition de la ligne sur les documents de type facture si le top personnalisation 5 a un X en première position.
   */
  public void setEditionSurFacture(boolean editionSurFacture) {
    if (editionSurFacture) {
      if (isEditionSurDevis()) {
        setTopPersonnalisation5("XX");
      }
      else {
        setTopPersonnalisation5("X ");
      }
    }
    else {
      if (isEditionSurDevis()) {
        setTopPersonnalisation5(" X");
      }
      else {
        setTopPersonnalisation5("  ");
      }
    }
  }
  
  public int getGenerationBonAchat() {
    return generationBonAchat;
  }
  
  public void setGenerationBonAchat(int generationBonAchat) {
    this.generationBonAchat = generationBonAchat;
  }
  
  public IdArticle getIdArticleSubstitue() {
    return idArticleSubstitue;
  }
  
  public void setIdArticleSubstitue(IdArticle pIdArticleSubstitue) {
    idArticleSubstitue = pIdArticleSubstitue;
  }
  
  public Character getTypeSubstitution() {
    return typeSubstitution;
  }
  
  public void setTypeSubstitution(Character typeSubstitution) {
    this.typeSubstitution = typeSubstitution;
  }
  
  public Character getRemise50Commercial() {
    return remise50Commercial;
  }
  
  public void setRemise50Commercial(Character remise50Commercial) {
    this.remise50Commercial = remise50Commercial;
  }
  
  public Character getIndRecherchePrixKit() {
    return indRecherchePrixKit;
  }
  
  public void setIndRecherchePrixKit(Character indRecherchePrixKit) {
    this.indRecherchePrixKit = indRecherchePrixKit;
  }
  
  public BigDecimal getQuantiteLivrable() {
    return quantiteLivrable;
  }
  
  public void setQuantiteLivrable(BigDecimal quantiteLivrable) {
    this.quantiteLivrable = quantiteLivrable;
  }
  
  public Character getIndiceLivraison() {
    return indiceLivraison;
  }
  
  public void setIndiceLivraison(Character indiceLivraison) {
    this.indiceLivraison = indiceLivraison;
  }
  
  public Character getIndCumulL1QTE() {
    return indCumulL1QTE;
  }
  
  public void setIndCumulL1QTE(Character indCumulL1QTE) {
    this.indCumulL1QTE = indCumulL1QTE;
  }
  
  public Character getIndCumulL1QTP() {
    return indCumulL1QTP;
  }
  
  public void setIndCumulL1QTP(Character indCumulL1QTP) {
    this.indCumulL1QTP = indCumulL1QTP;
  }
  
  public Character getIndLigneAExtraire() {
    return indLigneAExtraire;
  }
  
  public void setIndLigneAExtraire(Character indLigneAExtraire) {
    this.indLigneAExtraire = indLigneAExtraire;
  }
  
  public Character getIndASDI() {
    return indASDI;
  }
  
  public void setIndASDI(Character indASDI) {
    this.indASDI = indASDI;
  }
  
  public int getNumeroLigneOrigine() {
    return numeroLigneOrigine;
  }
  
  public void setNumeroLigneOrigine(int numeroLigneOrigine) {
    this.numeroLigneOrigine = numeroLigneOrigine;
  }
  
  public BigDecimal getPrixAAjouterPrixRevient() {
    return prixAAjouterPrixRevient;
  }
  
  public void setPrixAAjouterPrixRevient(BigDecimal prixAAjouterPrixRevient) {
    this.prixAAjouterPrixRevient = prixAAjouterPrixRevient;
  }
  
  public BigDecimal getPrixAAjouterPrixVenteClient() {
    return prixAAjouterPrixVenteClient;
  }
  
  public void setPrixAAjouterPrixVenteClient(BigDecimal prixAAjouterPrixVenteClient) {
    this.prixAAjouterPrixVenteClient = prixAAjouterPrixVenteClient;
  }
  
  public Character getIndTypeVente() {
    return indTypeVente;
  }
  
  public void setIndTypeVente(Character indTypeVente) {
    this.indTypeVente = indTypeVente;
  }
  
  /**
   * Indiquer si le prix de vente est isssu d'une dérogation.
   * @param true=prix de vente issu d'une dérogation, false=sinon.
   */
  public boolean isDerogation() {
    return getOriginePrixVente() == EnumOriginePrixVente.DEROGATION;
  }
  
  /**
   * Retourner un texte décrivant l'origine du prix de vente.
   * 
   * Cela retourne le libellé de l'origine du prix de vente en le le précisant avec des données contextuelles dans certaines situations.
   * Par exemple, la colonne utilisée est précisé dans le cas d'un prix standard.
   * 
   * @return Texte décrivant l'origine du prix de vente.
   */
  public String getTexteOriginePrixVente() {
    // Vérifier que l'origine prix de vente est renseignée
    if (getOriginePrixVente() == null) {
      return null;
    }
    
    // Afficher "Colonne n" dans le cas d'un prix standard
    if (getOriginePrixVente() == EnumOriginePrixVente.STANDARD && numeroColonneTarif != null) {
      return "Colonne " + numeroColonneTarif;
    }
    else if (getOriginePrixVente() == EnumOriginePrixVente.GROUPE_CNV && codeGroupeCNV != null) {
      return "CNV " + codeGroupeCNV;
    }
    // Afficher le libellé de l'origine du prix de vente remonté par la formule de calcul
    else {
      return getOriginePrixVente().getLibelle();
    }
  }
  
  public Character getIndApplicationConditionQuantitative() {
    return indApplicationConditionQuantitative;
  }
  
  public void setIndApplicationConditionQuantitative(Character indApplicationConditionQuantitative) {
    this.indApplicationConditionQuantitative = indApplicationConditionQuantitative;
  }
  
  public Character getAffectationStock() {
    return affectationStock;
  }
  
  public void setAffectationStock(Character affectationStock) {
    this.affectationStock = affectationStock;
  }
  
  public Character getCodeNumSerieOuLot() {
    return codeNumSerieOuLot;
  }
  
  public void setCodeNumSerieOuLot(Character codeNumSerieOuLot) {
    this.codeNumSerieOuLot = codeNumSerieOuLot;
  }
  
  public BigDecimal getQuantiteAvantExtraction() {
    return quantiteAvantExtraction;
  }
  
  public void setQuantiteAvantExtraction(BigDecimal quantiteAvantExtraction) {
    this.quantiteAvantExtraction = quantiteAvantExtraction;
  }
  
  public Character getTopArticleLoti() {
    return topArticleLoti;
  }
  
  public void setTopArticleLoti(Character topArticleLoti) {
    this.topArticleLoti = topArticleLoti;
  }
  
  public Character getTopArticleADS() {
    return topArticleADS;
  }
  
  public void setTopArticleADS(Character topArticleADS) {
    this.topArticleADS = topArticleADS;
  }
  
  /**
   * Retourner le type de la condition chantier.
   * @return Le type.
   */
  public EnumTypeChantier getTypeConditionChantier() {
    return typeConditionChantier;
  }
  
  /**
   * Modifier le type de la condition chantier.
   * @param pTypeConditionChantier Le type.
   */
  public void setTypeConditionChantier(EnumTypeChantier pTypeConditionChantier) {
    typeConditionChantier = pTypeConditionChantier;
  }
  
  public AlertesLigneDocument getAlertesLigneDocument() {
    return alertesLigneDocument;
  }
  
  public void setAlertesLigneDocument(AlertesLigneDocument alertesLigneDocument) {
    this.alertesLigneDocument = alertesLigneDocument;
  }
  
  public BigDecimal getQuantiteCommandeStock() {
    return quantiteCommandeStock;
  }
  
  public void setQuantiteCommandeStock(BigDecimal quantiteCommandeStock) {
    this.quantiteCommandeStock = quantiteCommandeStock;
  }
  
  public String getUniteCommandeStock() {
    return uniteCommandeStock;
  }
  
  public void setUniteCommandeStock(String uniteCommandeStock) {
    this.uniteCommandeStock = Constantes.normerTexte(uniteCommandeStock);
  }
  
  public BigDecimal getPourcentageTVA() {
    return pourcentageTVA;
  }
  
  public void setPourcentageTVA(BigDecimal tauxTVA) {
    this.pourcentageTVA = tauxTVA;
  }
  
  public BigDecimal getQuantiteDocumentOrigine() {
    return quantiteDocumentOrigine;
  }
  
  public void setQuantiteDocumentOrigine(BigDecimal quantiteDocumentOrigine) {
    this.quantiteDocumentOrigine = quantiteDocumentOrigine;
  }
  
  public BigDecimal getQuantiteDejaExtraite() {
    return quantiteDejaExtraite;
  }
  
  public void setQuantiteDejaExtraite(BigDecimal quantiteDejaExtraite) {
    this.quantiteDejaExtraite = quantiteDejaExtraite;
  }
  
  public BigDecimal getQuantiteDejaRetounee() {
    return quantiteDejaRetounee;
  }
  
  public void setQuantiteDejaRetounee(BigDecimal quantiteDejaRetounee) {
    this.quantiteDejaRetounee = quantiteDejaRetounee;
  }
  
  public boolean isClientTTC() {
    return isClientTTC;
  }
  
  public void setClientTTC(boolean isClientTTC) {
    this.isClientTTC = isClientTTC;
  }
  
  /**
   * Retourner le type de l'article.
   * @return Le type de l'article.
   */
  public EnumTypeArticle getTypeArticle() {
    return typeArticle;
  }
  
  /**
   * Modifier le type de l'article.
   * Ce champ est à titre indicatif et il n'est jamais sauvegardé en base.
   * @param pTypeArticle Le type de l'article.
   */
  public void setTypeArticle(EnumTypeArticle pTypeArticle) {
    typeArticle = pTypeArticle;
    if (isLignePalette()) {
      setExclusionRemisePied1('1');
      consignable = true;
    }
  }
  
  public BigDecimal getQuantiteArticlesFactures() {
    return quantiteArticlesFactures;
  }
  
  public void setQuantiteArticlesFactures(BigDecimal quantiteArticlesFactures) {
    this.quantiteArticlesFactures = quantiteArticlesFactures;
  }
  
  public IdLigneAchat getIdLigneAchatLiee() {
    return idLigneAchatLiee;
  }
  
  public void setIdLigneAchatLiee(IdLigneAchat idLigneAchatLiee) {
    this.idLigneAchatLiee = idLigneAchatLiee;
  }
  
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  public void setIdDocumentVente(IdDocumentVente idDocumentVente) {
    this.idDocumentVente = idDocumentVente;
  }
  
  /**
   * Renvoyer l'identifiant de l'adresse du fournisseur de l'article de la ligne
   */
  public IdAdresseFournisseur getIdAdresseFournisseur() {
    return idAdresseFournisseur;
  }
  
  /**
   * Mettre à jour l'identifiant de l'adresse du fournisseur de l'article de la ligne
   */
  public void setIdAdresseFournisseur(IdAdresseFournisseur pIdAdresseFournisseur) {
    idAdresseFournisseur = pIdAdresseFournisseur;
  }
  
  /**
   * Retourner le prix de base TTC.
   * @return Le prix de base TTC.
   */
  public BigDecimal getPrixBaseTTC() {
    return prixBaseTTC;
  }
  
  /**
   * Modifier le prix de base TTC.
   * @param pPrixBaseTTC Le prix de base TTC.
   */
  public void setPrixBaseTTC(BigDecimal pPrixBaseTTC) {
    this.prixBaseTTC = pPrixBaseTTC;
  }
  
  /**
   * Retourner si le prix est négociable.
   * A affiner plus tard
   * @return
   */
  public boolean isPrixNegociable() {
    if (getExclusionRemisePied1() != null && getExclusionRemisePied1() != ' ') {
      return false;
    }
    return true;
  }
  
  /**
   * Retourner le prix de revient standard HT.
   * @return Le prix de revient standard HT.
   */
  public BigDecimal getPrixDeRevientStandardHT() {
    return prixDeRevientStandardHT;
  }
  
  /**
   * Modifier le prix de revient standard HT de la ligne.
   * @param pPrixDeRevientStandardHT Le prix de revient standard HT.
   */
  public void setPrixDeRevientStandardHT(BigDecimal pPrixDeRevientStandardHT) {
    this.prixDeRevientStandardHT = pPrixDeRevientStandardHT;
  }
  
  /**
   * Retourner le prix de revient standard TTC de la ligne.
   * @return Le prix de revient standard TTC.
   */
  public BigDecimal getPrixDeRevientStandardTTC() {
    return prixDeRevientStandardTTC;
  }
  
  /**
   * Modifier le prix de revient standard TTC de la ligne.
   * @param pPrixDeRevientStandardTTC Le prix de revient standard HT.
   */
  public void setPrixDeRevientStandardTTC(BigDecimal prixDeRevientStandardTTC) {
    this.prixDeRevientStandardTTC = prixDeRevientStandardTTC;
  }
  
  /**
   * Retourner le prix de revient HT contenant les frais de port.
   * @return Le prix de revient HT.
   */
  public BigDecimal getPrixDeRevientPortIncluHT() {
    return prixDeRevientPortIncluHT;
  }
  
  /**
   * Retourner le prix de revient HT contenant les frais de port.
   * @param pPrixDeRevientPortIncluHT Le prix de revient HT.
   */
  public void setPrixDeRevientPortIncluHT(BigDecimal pPrixDeRevientPortIncluHT) {
    prixDeRevientPortIncluHT = pPrixDeRevientPortIncluHT;
  }
  
  /**
   * Retourner le prix de revient HT de la ligne.
   * @return Le prix de revient HT.
   */
  public BigDecimal getPrixDeRevientLigneHT() {
    return prixDeRevientLigneHT;
  }
  
  /**
   * Modifier le prix de revient HT de la ligne.
   * @param pPrixDeRevientLigneHT Le prix de revient.
   */
  public void setPrixDeRevientLigneHT(BigDecimal pPrixDeRevientLigneHT) {
    prixDeRevientLigneHT = pPrixDeRevientLigneHT;
  }
  
  /**
   * Retourner le prix de revient TTC de la ligne.
   * @return Le prix de revient TTC.
   */
  public BigDecimal getPrixDeRevientLigneTTC() {
    return prixDeRevientLigneTTC;
  }
  
  /**
   * Modifier le prix de revient TTC de la ligne.
   * @param pPrixDeRevientLigneTTC Le prix de revient.
   */
  public void setPrixDeRevientLigneTTC(BigDecimal pPrixDeRevientLigneTTC) {
    prixDeRevientLigneTTC = pPrixDeRevientLigneTTC;
  }
  
  /**
   * Retourner le montant TTC.
   * @return Le montant TTC.
   */
  public BigDecimal getMontantTTC() {
    return montantTTC;
  }
  
  /**
   * Modifier le montant TTC.
   * @param pMontantTTC Le montant TTC.
   */
  public void setMontantTTC(BigDecimal pMontantTTC) {
    montantTTC = pMontantTTC;
  }
  
  /**
   * Modifier si le prix net peut être inférieur au prix de revient.
   * @param autoriserPrixNetInferieurPrixRevient.
   */
  public void setAutoriserPrixNetInferieurPrixRevient(boolean pAutoriserPrixNetInferieurPrixRevient) {
    autoriserPrixNetInferieurPrixRevient = pAutoriserPrixNetInferieurPrixRevient;
  }
  
  /**
   * Retourner le mode dans lequel les calculs de pris de vente s'effectuent.
   * @param pModeNegoce
   */
  public void setModeNegoce(Boolean pModeNegoce) {
    modeNegoce = pModeNegoce;
  }
  
  /**
   * Retourner le code du groupe de CNV qui a eté applqiué pour calculer le prix de la ligne de vente.
   * @return Code groupe CNV.
   */
  public String getCodeGroupeCNV() {
    return codeGroupeCNV;
  }
  
  /**
   * Modifier le code du groupe de CNV qui a eté applqiué pour calculer le prix de la ligne de vente.
   * @param pCodeGroupeCNV Code groupe CNV.
   */
  public void setCodeGroupeCNV(String pCodeGroupeCNV) {
    codeGroupeCNV = pCodeGroupeCNV;
  }
  
  /**
   * Retourner la liste des conditions de ventes utilisées par la ligne de ventes.
   * Cette liste n'est pas systématiquement présente.
   * @return La liste des conditions de ventes.
   */
  public List<InformationConditionVenteLigneVente> getListeConditionVenteLigneVente() {
    return listeConditionVenteLigneVente;
  }
}
