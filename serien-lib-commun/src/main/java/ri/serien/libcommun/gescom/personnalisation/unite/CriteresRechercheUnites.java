/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.unite;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;

public class CriteresRechercheUnites extends CriteresBaseRecherche {
  // Constantes
  // Retour: code etablissement, code unite, libellé, libellé court
  public static final int RECHERCHE_COMPTOIR = 1;
  
  // Variables
  
}
