/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;

/**
 * Liste de coordonnées fournisseurs.
 */
public class ListeAdresseFournisseur extends ListeClasseMetier<IdAdresseFournisseur, AdresseFournisseur, ListeAdresseFournisseur> {
  /**
   * Constructeurs.
   */
  public ListeAdresseFournisseur() {
  }
  
  /**
   * Construire une liste d'adresses fournisseur correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des adresses fournisseur ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeAdresseFournisseur creerListeNonChargee(List<IdAdresseFournisseur> pListeIdAdresseFournisseur) {
    ListeAdresseFournisseur listeAdresseFournisseur = new ListeAdresseFournisseur();
    if (pListeIdAdresseFournisseur != null) {
      for (IdAdresseFournisseur idAdresseFournisseur : pListeIdAdresseFournisseur) {
        AdresseFournisseur adresseFournisseur = new AdresseFournisseur(idAdresseFournisseur);
        adresseFournisseur.setCharge(false);
        listeAdresseFournisseur.add(adresseFournisseur);
      }
    }
    return listeAdresseFournisseur;
  }
  
  /**
   * Charge les coordonnées fournisseurs pour l'expression donnée.
   */
  public static ListeAdresseFournisseur charger(IdSession pIdSession, IdEtablissement pIdEtablissement, String pExpression) {
    if (pIdEtablissement == null || !pIdEtablissement.isExistant()) {
      throw new MessageErreurException("L'établissement est invalide.");
    }
    CritereFournisseur criteres = new CritereFournisseur();
    criteres.setTypeRecherche(CritereFournisseur.RECHERCHER_ADRESSE_FOURNISSEURS_DOCUMENTACHAT);
    criteres.setTexteRechercheGenerique(pExpression);
    criteres.setIdEtablissement(pIdEtablissement);
    List<IdAdresseFournisseur> listeId = ManagerServiceFournisseur.chargerListeIdAdresseFournisseur(pIdSession, criteres);
    if (listeId == null || listeId.isEmpty()) {
      return null;
    }
    return ManagerServiceFournisseur.chargerListeAdresseFournisseur(pIdSession, listeId);
  }
  
  /**
   * Charge toutes les coordonnées fournisseurs correspondant à un établissement.
   */
  @Override
  public ListeAdresseFournisseur charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null || !pIdEtablissement.isExistant()) {
      throw new MessageErreurException("L'établissement est invalide.");
    }
    
    return ManagerServiceFournisseur.chargerListeAdresseFournisseur(pIdSession, pIdEtablissement);
  }
  
  /**
   * Charger les adresses fournisseurs dont les identifiants sont fournis.
   */
  @Override
  public ListeAdresseFournisseur charger(IdSession pIdSession, List<IdAdresseFournisseur> pListeId) {
    return ManagerServiceFournisseur.chargerListeAdresseFournisseur(pIdSession, pListeId);
  }
  
  /**
   * Retourne les coordonnees fournisseur à partir identifiant.
   */
  public AdresseFournisseur retournerCoordonneesFournisseurParId(IdAdresseFournisseur pIdAdresseFournisseur) {
    for (AdresseFournisseur coordonnees : this) {
      if (Constantes.equals(pIdAdresseFournisseur, coordonnees.getId())) {
        return coordonnees;
      }
    }
    return null;
  }
  
  /**
   * Retourne toutes les coordonnees d'un fournisseur à partir identifiant.
   */
  public ListeAdresseFournisseur retournerCoordonneesUnFournisseur(IdFournisseur pIdFournisseur) {
    ListeAdresseFournisseur liste = new ListeAdresseFournisseur();
    for (AdresseFournisseur adresseFournisseur : this) {
      if (Constantes.equals(pIdFournisseur, adresseFournisseur.getId().getIdFournisseur())) {
        liste.add(adresseFournisseur);
      }
    }
    if (liste.size() == 0) {
      return null;
    }
    return liste;
  }
  
  /**
   * trier les adresses fournisseur suivant leurs noms
   */
  public void trierParNom() {
    Collections.sort(this, new Comparator<AdresseFournisseur>() {
      @Override
      public int compare(AdresseFournisseur o1, AdresseFournisseur o2) {
        return o1.getAdresse().getNom().compareTo(o2.getAdresse().getNom());
      }
    });
  }
  
  /**
   * Trier les adresses fournisseur suivant leurs identifiants
   */
  public void trierParId() {
    Collections.sort(this, new Comparator<AdresseFournisseur>() {
      @Override
      public int compare(AdresseFournisseur o1, AdresseFournisseur o2) {
        int collectif1 = o1.getId().getIdFournisseur().getCollectif();
        int collectif2 = o2.getId().getIdFournisseur().getCollectif();
        int numero1 = o1.getId().getIdFournisseur().getNumero();
        int numero2 = o2.getId().getIdFournisseur().getNumero();
        int indice1 = o1.getId().getIndice();
        int indice2 = o2.getId().getIndice();
        
        if (collectif1 > collectif2) {
          return 1;
        }
        else if (collectif1 < collectif2) {
          return -1;
        }
        else {
          if (numero1 > numero2) {
            return 1;
          }
          else if (numero1 < numero2) {
            return -1;
          }
          else {
            if (indice1 > indice2) {
              return 1;
            }
            else if (indice1 < indice2) {
              return -1;
            }
            else {
              return 0;
            }
          }
        }
      }
    });
  }
  
}
