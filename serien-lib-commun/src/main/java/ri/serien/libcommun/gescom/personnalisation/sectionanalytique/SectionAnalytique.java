/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.sectionanalytique;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

public class SectionAnalytique extends AbstractClasseMetier<idSectionAnalytique> {
  
  private String nom = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public SectionAnalytique(idSectionAnalytique pId) {
    super(pId);
  }
  
  @Override
  public String getTexte() {
    return nom;
  }
  
  // -- Accesseurs
  /**
   * Nom de l'affaire.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Nom de l'affaire suivi de son code entre parenthèse
   * Exemple : reprise (01).
   * Par sécurité, le code est retourné seul au cas où le nom ne serait pas renseigné.
   */
  public String getNomAvecCode() {
    if (nom != null) {
      return nom + " (" + getId().getCode() + ")";
    }
    else {
      return getId().getCode();
    }
  }
  
  /**
   * Renseigner le nom de la section analytique.
   */
  public void setNom(String pNom) {
    this.nom = pNom;
  }
  
}
