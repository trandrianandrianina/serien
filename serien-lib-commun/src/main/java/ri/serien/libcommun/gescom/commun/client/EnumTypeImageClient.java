/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Définit si le client est un particulier ou un professionnel
 */
public enum EnumTypeImageClient {
  PROFESSIONNEL(' ', "Professionnel"),
  PARTICULIER('1', "Particulier"),
  VPC('3', "Vente par correspondance"),
  DONNEUR_ORDRE('4', "Donneur d'ordres");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeImageClient(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeImageClient valueOfByCode(Character pCode) {
    for (EnumTypeImageClient value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de client est invalide : " + pCode);
  }
}
