/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.prix;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import ri.serien.libcommun.gescom.commun.conditionachat.EnumModeSaisiePort;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Cette classe est détentrice de la méthode de calcul d'un prix d'achats.
 * 
 * Sauf explicitement mentionné, tous les montants de cette classe sont exprimés en unité d'achats et à l'unité (pour un article).
 */
public class PrixAchat implements Serializable, Cloneable {
  // Constantes
  protected static final int NOMBRE_REMISES = 6;
  public static final int REMISE_1 = 0;
  public static final int REMISE_2 = 1;
  public static final int REMISE_3 = 2;
  public static final int REMISE_4 = 3;
  public static final int REMISE_5 = 4;
  public static final int REMISE_6 = 5;
  public static final int NOMBRE_DECIMALE_REMISE = 2;
  
  public static final int ORIGINE_NON_DEFINI = 0;
  public static final int ORIGINE_TARIF = 1;
  public static final int ORIGINE_CONDITION_ACHAT = 2;
  public static final int ORIGINE_SAISI = 3;
  
  public static final int PRECISION_MAX_QUANTITE = 3;
  
  // Indicateurs
  private Integer topPrixBaseSaisie = null;
  private Integer topRemiseSaisie = null;
  private Integer topPrixNetSaisi = null;
  private Integer topUniteSaisie = null;
  private Integer topQuantiteSaisie = null;
  private Integer topMontantHTSaisi = null;
  private Boolean quantiteNonMultipleConditionnement = false;
  
  // Unités
  private IdUnite idUA = null;
  private IdUnite idUCA = null;
  private BigDecimal nombreUAParUCA = BigDecimal.ONE;
  private BigDecimal nombreUSParUCA = BigDecimal.ONE;
  private BigDecimal nombreUCAParUCS = BigDecimal.ONE;
  
  // Ratios
  private Integer nombreDecimaleUA = null;
  private Integer nombreDecimaleUCA = null;
  private Integer nombreDecimaleUS = null;
  
  // Quantités en reliquat
  private BigDecimal quantiteReliquatUCA = BigDecimal.ZERO;
  private BigDecimal quantiteReliquatUA = BigDecimal.ZERO;
  private BigDecimal quantiteReliquatUS = BigDecimal.ZERO;
  // Quantités initiales
  private BigDecimal quantiteInitialeUA = BigDecimal.ZERO;
  private BigDecimal quantiteInitialeUCA = BigDecimal.ZERO;
  private BigDecimal quantiteInitialeUS = BigDecimal.ZERO;
  // Quantités traitées
  private BigDecimal quantiteTraiteeUA = BigDecimal.ZERO;
  private BigDecimal quantiteTraiteeUCA = BigDecimal.ZERO;
  private BigDecimal quantiteTraiteeUS = BigDecimal.ZERO;
  
  // Eléments unitaires du prix d'achat
  private BigDecimal prixAchatBrutHT = BigDecimal.ZERO;
  private ListeRemise listeRemise = new ListeRemise();
  private BigDecimal coefficientRemise = BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP);
  private BigDecimal pourcentageTaxeOuMajoration = BigDecimal.ZERO;
  private BigDecimal montantConditionnement = BigDecimal.ZERO;
  private BigDecimal prixAchatNetHT = BigDecimal.ZERO;
  private BigDecimal prixAchatCalculeHT = BigDecimal.ZERO;
  private EnumModeSaisiePort modeSaisiePort = EnumModeSaisiePort.MONTANT_FOIS_POIDS;
  private BigDecimal montantAuPoidsPortHT = BigDecimal.ZERO;
  private BigDecimal poidsPort = BigDecimal.ZERO;
  private BigPercentage pourcentagePort = BigPercentage.ZERO;
  private BigDecimal montantPortHT = BigDecimal.ZERO;
  private BigDecimal prixRevientFournisseurHT = BigDecimal.ZERO;
  private BigDecimal prixRevientFournisseurHTEnUS = BigDecimal.ZERO;
  private BigPercentage pourcentageMajoration = BigPercentage.ZERO;
  private BigDecimal coefficientApprocheFixe = BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP);
  private BigDecimal prixRevientStandardHT = BigDecimal.ZERO;
  private BigDecimal prixRevientStandardHTEnUS = BigDecimal.ZERO;
  private BigDecimal montantReliquatHT = BigDecimal.ZERO;
  private BigDecimal montantInitialHT = BigDecimal.ZERO;
  private BigDecimal montantTraiteHT = BigDecimal.ZERO;
  
  // Exclusions de remises de pied
  private Character[] exclusionRemisePied = new Character[NOMBRE_REMISES];
  
  // Eléments de TVA
  private Integer codeTVA = null;
  private Integer colonneTVA = null;
  
  // -- Méthodes publiques
  
  /**
   * Dupliquer l'objet avec ses informations.
   */
  @Override
  public PrixAchat clone() {
    PrixAchat o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (PrixAchat) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  /**
   * Initialise les données avec la classe parametrePrixAchat.
   */
  public void initialiser(ParametrePrixAchat pParametre) {
    if (pParametre == null) {
      return;
    }
    
    // Renseigner les unités
    idUA = pParametre.getIdUA();
    idUCA = pParametre.getIdUCA();
    nombreDecimaleUA = pParametre.getNombreDecimaleUA();
    nombreDecimaleUCA = pParametre.getNombreDecimaleUCA();
    nombreDecimaleUS = pParametre.getNombreDecimaleUS();
    
    // Renseigner les ratios
    nombreUAParUCA = pParametre.getNombreUAParUCA();
    nombreUSParUCA = pParametre.getNombreUSParUCA();
    nombreUCAParUCS = pParametre.getNombreUCAParUCS();
    
    // Renseigner les quantités en reliquat
    setQuantiteReliquatUA(pParametre.getQuantiteReliquatUA(), false);
    setQuantiteReliquatUCA(pParametre.getQuantiteReliquatUCA(), false);
    setQuantiteReliquatUS(pParametre.getQuantiteReliquatUS(), false);
    // Renseigner les quantités initiales
    setQuantiteInitialeUA(pParametre.getQuantiteInitialeUA(), false);
    setQuantiteInitialeUCA(pParametre.getQuantiteInitialeUCA(), false);
    setQuantiteInitialeUS(pParametre.getQuantiteInitialeUS(), false);
    // Renseigner les quantités traitées
    setQuantiteTraiteeUA(pParametre.getQuantiteTraiteeUA(), false);
    setQuantiteTraiteeUCA(pParametre.getQuantiteTraiteeUCA(), false);
    setQuantiteTraiteeUS(pParametre.getQuantiteTraiteeUS(), false);
    
    // Renseigner les montants
    setPrixAchatBrutHT(pParametre.getPrixAchatBrutHT(), false);
    setListeRemise(pParametre.getListeRemise(), false);
    setPourcentageTaxeOuMajoration(pParametre.getPourcentageTaxeOuMajoration(), false);
    setMontantConditionnement(pParametre.getMontantConditionnement(), false);
    setPrixAchatNetHT(pParametre.getPrixNetHT());
    setPrixAchatCalculeHT(pParametre.getPrixCalculeHT());
    setMontantAuPoidsPortHT(pParametre.getMontantAuPoidsPortHT(), false);
    setPoidsPort(pParametre.getPoidsPort(), false);
    setPourcentagePort(pParametre.getPourcentagePort(), false);
    setMontantPortHT(pParametre.getMontantPortHT());
    setPrixRevientFournisseurHT(pParametre.getPrixRevientFournisseurHT());
    setPourcentageMajoration(pParametre.getPourcentageMajoration(), false);
    setPrixRevientStandardHT(pParametre.getPrixRevientStandardHT());
    setPrixRevientStandardHTEnUS(pParametre.getPrixRevientStandardHTEnUS());
    setMontantReliquatHT(pParametre.getMontantLigneHT());
    setMontantInitialHT(pParametre.getMontantInitialHT());
    setMontantTraiteHT(pParametre.getMontantTraiteHT());
    
    // Renseigner les exclusions de remises de pied
    exclusionRemisePied = pParametre.getExclusionRemisePied();
    
    // Renseigner les indicateurs
    topPrixBaseSaisie = pParametre.getTopPrixBaseSaisie();
    topRemiseSaisie = pParametre.getTopRemiseSaisie();
    topPrixNetSaisi = pParametre.getTopPrixNetSaisi();
    topUniteSaisie = pParametre.getTopUniteSaisie();
    topQuantiteSaisie = pParametre.getTopQuantiteSaisie();
    topMontantHTSaisi = pParametre.getTopMontantHTSaisi();
    
    // Renseigner les éléments de TVA
    codeTVA = pParametre.getCodeTVA();
    colonneTVA = pParametre.getColonneTVA();
    
    // Calculer les données manquantss
    deduireModeSaisiePort();
    calculer(false);
  }
  
  /**
   * Retourne si le prix calculé doit être forcé.
   */
  public boolean isForcerPrixCalcule() {
    if (topPrixNetSaisi == ORIGINE_SAISI && prixAchatCalculeHT.compareTo(BigDecimal.ZERO) != 0) {
      return true;
    }
    return false;
  }
  
  // --------------------------------------------------------------------------
  // Méthodes privées pour les calculs
  // --------------------------------------------------------------------------
  
  /**
   * Déduire le mode de saisie du port à partir des données.
   * 
   * Le mode de saisie du port n'est pas sauvé dans la base de données. Il est donc nécessaire de le déduire à partir
   * des données
   * après un chargement issu de la base de données. Ensuite, la présence de données et le mode de saisie du port
   * peuvent diverger
   * car cela dépendra des saisies utilisateurs.
   * 
   * C'est la présence d'un montant dans le poids au port qui décide du mode de saisie du port. Cette émthode est utile
   */
  private void deduireModeSaisiePort() {
    // Mettre en mode saisie poids si un montant au poids est présent
    if (montantAuPoidsPortHT != null && montantAuPoidsPortHT.compareTo(BigDecimal.ZERO) > 0) {
      modeSaisiePort = EnumModeSaisiePort.MONTANT_FOIS_POIDS;
    }
    // Mettre en mode saisie pourcnetage si un pourcentage est présent
    else if (pourcentagePort != null && !pourcentagePort.isZero()) {
      modeSaisiePort = EnumModeSaisiePort.POURCENTAGE_PRIX_NET;
    }
    // Mettre en mode saisie poids par défaut
    else {
      modeSaisiePort = EnumModeSaisiePort.MONTANT_FOIS_POIDS;
    }
  }
  
  /**
   * Calculer tous les éléments de prix.
   * Si le paramètre forcerRecalcul est à false, uniquement les montants égaux à zéro sont calculés. C'est utile lorsque
   * seulement une partie des données est initialisée. Le recalcule complète les valeurs manquantes.
   * Si le paramètre forcerRecalcul est à true, tous les montants sont recalculés. Cela écrase donc les éventuelles
   * valeurs précédentes.
   */
  private void calculer(boolean forcerRecalcul) {
    calculerPrixAchatNetHT(forcerRecalcul);
    calculerMontantPortHT(forcerRecalcul);
    calculerPrixRevientFournisseurHT(forcerRecalcul);
    calculerPrixRevientFournisseurHTEnUS(forcerRecalcul);
    calculerPrixRevientStandardHT(forcerRecalcul);
    calculerPrixRevientStandardHTEnUS(forcerRecalcul);
    calculerQuantiteReliquatUA(forcerRecalcul);
    calculerMontantReliquatHT(forcerRecalcul);
  }
  
  /**
   * Calculer le prix d'achat net HT.
   */
  private void calculerPrixAchatNetHT(boolean forcerRecalcul) {
    // Calculer la valeur si celle-ci est égale à zéro ou si c'est explicitement demandé
    if (!forcerRecalcul && prixAchatNetHT.compareTo(BigDecimal.ZERO) != 0) {
      return;
    }
    
    // 2 cas où l'on affecte directement le prix calculé (dans les négocations d'achat et lors d'un prix dérogé client/fournisseur) :
    // - Si le recalcul des prix n'est pas forcé
    // - Si le top prix net saisi vaut ORIGINE_SAISI(3) et que le prix calculé est supérieur à 0 (en gros le prix calculé est forcé)
    if (!forcerRecalcul && isForcerPrixCalcule()) {
      prixAchatNetHT = prixAchatCalculeHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
      return;
    }
    
    // Appliquer les pourcentages de remise
    // UPNET0=
    // CAPRA*(100-CAREM1)/100*(100-CAREM2)/100*(100-CAREM3)/100*(100-CAREM4)/100*(100-CAREM5)/100*(100-CAREM6)/100
    prixAchatNetHT = prixAchatBrutHT
        .multiply(
            (Constantes.VALEUR_CENT.subtract(listeRemise.getPourcentageRemise1()).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply(
            (Constantes.VALEUR_CENT.subtract(listeRemise.getPourcentageRemise2()).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply(
            (Constantes.VALEUR_CENT.subtract(listeRemise.getPourcentageRemise3()).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply(
            (Constantes.VALEUR_CENT.subtract(listeRemise.getPourcentageRemise4()).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply(
            (Constantes.VALEUR_CENT.subtract(listeRemise.getPourcentageRemise5()).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply(
            (Constantes.VALEUR_CENT.subtract(listeRemise.getPourcentageRemise6()).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)));
    
    // Appliquer le coefficient de remise
    // UPNET1=UPNET0*CAKPR
    if (coefficientRemise.compareTo(BigDecimal.ZERO) > 0) {
      prixAchatNetHT = prixAchatNetHT.multiply(coefficientRemise);
    }
    
    // Appliquer le pourcentage de taxe ou de majoration
    // UPNET2= UPNET1+(UPNET1*CAFK3/100)
    BigDecimal coefficientTaxeOuMajoration =
        BigDecimal.ONE.add(pourcentageTaxeOuMajoration.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32));
    prixAchatNetHT = prixAchatNetHT.multiply(coefficientTaxeOuMajoration);
    
    // Appliquer le montant de conditionnement
    // CAPANET=UPNET2+CAFV3
    prixAchatNetHT = prixAchatNetHT.add(montantConditionnement);
    
    // Arrondir
    prixAchatNetHT = prixAchatNetHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le montant total du port HT.
   * Cela peut se faire sous deux formes :
   * - soit une valeur (elle même calculée à partir d'une valeur unitaire multipliée par un coefficient représentant le
   * poids)
   * - soit un pourcentage appliqué au prix net HT.
   */
  private void calculerMontantPortHT(boolean forcerRecalcul) {
    // Calculer la valeur si celle-ci est égale à zéro ou si c'est explicitement demandé
    if (!forcerRecalcul && montantPortHT.compareTo(BigDecimal.ZERO) != 0) {
      return;
    }
    
    // Tester le mode de saisie du port
    switch (modeSaisiePort) {
      case MONTANT_FOIS_POIDS:
        // Mettre la valeur 1 dans le poids si celui-ci est égal à zéro et que l montant poids port est renseigné
        if (montantAuPoidsPortHT.compareTo(BigDecimal.ZERO) != 0 && poidsPort.compareTo(BigDecimal.ZERO) == 0) {
          poidsPort = BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP);
        }
        
        // FRAIS PORT=CAFV1*CAFK1 (si CAFK1 = 0 -> CAFK1=1)
        montantPortHT = montantAuPoidsPortHT.multiply(poidsPort);
        
        // Effacer le pourcentage (pour ne pas garder des données inutiles au cas de calcul appliqué)
        pourcentagePort = BigPercentage.ZERO;
        break;
      
      case POURCENTAGE_PRIX_NET:
        // FRAIS PORT=CAPANET*(CAFP1/100)
        montantPortHT = prixAchatNetHT.multiply(pourcentagePort.getTauxEnDecimal());
        
        // Effacer le mopntant au poids et le poids (pour ne pas garder des données inutiles au cas de calcul appliqué)
        montantAuPoidsPortHT = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
        poidsPort = BigDecimal.ZERO.setScale(4, RoundingMode.HALF_UP);
        break;
    }
    
    // Arrondir
    montantPortHT = montantPortHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le prix de revient fournisseur HT.
   */
  private void calculerPrixRevientFournisseurHT(boolean forcerRecalcul) {
    // Calculer la valeur si celle-ci est égale à zéro ou si c'est explicitement demandé
    if (!forcerRecalcul && prixRevientFournisseurHT.compareTo(BigDecimal.ZERO) != 0) {
      return;
    }
    
    // Appliquer le montant du port HT
    // CAPRF_UA=CAPANET+FRAIS PORT
    prixRevientFournisseurHT = prixAchatNetHT.add(montantPortHT);
    
    // Arrondir
    prixRevientFournisseurHT = prixRevientFournisseurHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le prix de revient fournisseur HT en US.
   * prixRevientFournisseurHTENUS = prixRevientFournisseurHT(UA) * US/UA.
   */
  private void calculerPrixRevientFournisseurHTEnUS(boolean forcerRecalcul) {
    // Calculer la valeur si celle-ci est égale à zéro ou si c'est explicitement demandé
    if (!forcerRecalcul && prixRevientFournisseurHTEnUS.compareTo(BigDecimal.ZERO) != 0) {
      return;
    }
    
    // Calculer le prix de revient fournisseur en US.
    // prixUS = prixUA * (UA/UCA) / (US/UCA)
    prixRevientFournisseurHTEnUS =
        prixRevientFournisseurHT.multiply(getNombreUAParUCA()).divide(getNombreUSParUCA(), MathContext.DECIMAL32);
    
    // Arrondir
    prixRevientFournisseurHTEnUS = prixRevientFournisseurHTEnUS.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le prix de revient standard HT.
   */
  private void calculerPrixRevientStandardHT(boolean forcerRecalcul) {
    // Calculer la valeur si celle-ci est égale à zéro ou si c'est explicitement demandé
    if (!forcerRecalcul && prixRevientStandardHT.compareTo(BigDecimal.ZERO) != 0) {
      return;
    }
    
    // Appliquer le pourcentage de majoration
    prixRevientStandardHT = prixRevientFournisseurHT.multiply(pourcentageMajoration.getCoefficient());
    
    // Appliquer le coefficient d'approche fixe
    if (coefficientApprocheFixe.compareTo(BigDecimal.ZERO) > 0) {
      prixRevientStandardHT = prixRevientStandardHT.multiply(coefficientApprocheFixe);
    }
    
    // Arrondir
    prixRevientStandardHT = prixRevientStandardHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le prix de revient standard HT en US.
   * Le prix de revient standard en US n'est pas calculé à partir du prix de revient standard en UA. Il est calculé à
   * partir du
   * prix de revient fournisseur en US suivant la même formule.
   */
  private void calculerPrixRevientStandardHTEnUS(boolean forcerRecalcul) {
    // Calculer la valeur si celle-ci est égale à zéro ou si c'est explicitement demandé
    if (!forcerRecalcul && prixRevientStandardHTEnUS.compareTo(BigDecimal.ZERO) != 0) {
      return;
    }
    
    // Appliquer le pourcentage de majoration
    prixRevientStandardHTEnUS = prixRevientFournisseurHTEnUS.multiply(pourcentageMajoration.getCoefficient());
    
    // Appliquer le coefficient d'approche fixe
    if (coefficientApprocheFixe.compareTo(BigDecimal.ZERO) > 0) {
      prixRevientStandardHTEnUS = prixRevientFournisseurHTEnUS.multiply(coefficientApprocheFixe);
    }
    
    // Arrondir
    prixRevientStandardHTEnUS = prixRevientFournisseurHTEnUS.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer les quantités en reliquat UA partir de la quantité UCA.
   */
  private void calculerQuantiteReliquatUA(boolean forcerRecalcul) {
    // Calculer la valeur si celle-ci est égale à zéro ou si c'est explicitement demandé
    if (!forcerRecalcul && quantiteReliquatUA.compareTo(BigDecimal.ZERO) != 0) {
      return;
    }
    
    // Calculer la quantité en UCA
    if (quantiteReliquatUCA == null) {
      quantiteReliquatUCA = BigDecimal.ZERO;
    }
    
    quantiteReliquatUA = quantiteReliquatUCA.multiply(getNombreUAParUCA(), MathContext.DECIMAL32);
    quantiteReliquatUA = quantiteReliquatUA.setScale(nombreDecimaleUA, RoundingMode.HALF_UP);
    quantiteReliquatUS = quantiteReliquatUCA.multiply(getNombreUSParUCA(), MathContext.DECIMAL32);
    quantiteReliquatUS = quantiteReliquatUS.setScale(nombreDecimaleUS, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le montant en reliquat HT de la ligne.
   */
  private void calculerMontantReliquatHT(boolean forcerRecalcul) {
    // Calculer la valeur si celle-ci est égale à zéro ou si c'est explicitement demandé
    if (!forcerRecalcul && montantReliquatHT.compareTo(BigDecimal.ZERO) != 0) {
      return;
    }
    
    // Calculer montant total de la ligne
    montantReliquatHT = quantiteReliquatUA.multiply(prixAchatNetHT);
    montantReliquatHT = montantReliquatHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  // --------------------------------------------------------------------------
  // Accesseurs sur les unités
  // --------------------------------------------------------------------------
  
  /**
   * Identifiant de l'unité d'achats.
   * Correspond aux champs UNA de la base de données.
   */
  public IdUnite getIdUA() {
    return idUA;
  }
  
  /**
   * Identifiant de l'unité de commande d'achats.
   * Correspond aux champs UNC de la base de données.
   */
  public IdUnite getIdUCA() {
    return idUCA;
  }
  
  /**
   * Nombre de décimales de l'unité d'achats.
   * Correspond aux champs DCA de la base de données.
   */
  public Integer getNombreDecimaleUA() {
    return nombreDecimaleUA;
  }
  
  /**
   * Nombre de décimales de l'unité de commandes d'achats.
   * Correspond aux champs DCC de la base de données.
   */
  public Integer getNombreDecimaleUCA() {
    return nombreDecimaleUCA;
  }
  
  /**
   * Nombre de décimales de l'unité de stock.
   * Correspond aux champs DCS de la base de données.
   */
  public Integer getNombreDecimaleUS() {
    return nombreDecimaleUS;
  }
  
  // --------------------------------------------------------------------------
  // Accesseurs sur les ratios
  // --------------------------------------------------------------------------
  
  /**
   * Nombre d'unités d'achats par unité de commande d'achats (KAC).
   * 
   * Le coefficient KAC est le nombre d'unités d'achats pour une unité de commandes d'achats. Il est utilisé pour
   * convertir une quantité
   * exprimée en unité de commandes d'achats en une quantité exprimée en unités d'achats (et vice et versa).
   * Correspond aux champs KAC de la base de données.
   * 
   * Formulaire :
   * KAC = QTUA / QTUCA
   * QTUA = QTUCA * KAC
   * QTUCA = QTUA / KAC
   */
  public BigDecimal getNombreUAParUCA() {
    if (nombreUAParUCA == null || nombreUAParUCA.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ONE;
    }
    return nombreUAParUCA;
  }
  
  /**
   * Libellé décrivant le ratio entre les unités d'achats et les unités de commandes d'achats.
   * Le libellé retourné est vide ("") si les unités sont identiques.
   * Exemple : Conditionnement de 3,20 m²/U"
   */
  public String getLibelleNombreUAParUCA() {
    if (nombreUAParUCA == null || nombreUAParUCA.compareTo(BigDecimal.ZERO) <= 0 || idUA == null || idUCA == null || idUA.equals(idUCA)) {
      return "";
    }
    return String.format("Conditionnement de %s %s/%s", Constantes.formater(nombreUAParUCA, false), idUA.getCode(), idUCA.getCode());
  }
  
  /**
   * Nombre d'unités de stocks par unité de commandes d'achats (KSC).
   * 
   * Le coefficient KSC est le nombre d'unité de stocks pour une unité de commande d'achats. Il est utilisé pour
   * convertir une quantité
   * exprimée en unités de commandes d'achats en une quantité exprimée en unités de stocks (et vice et versa).
   * Correspond aux champs KSC de la base de données.
   * 
   * Formulaire :
   * KSC = QTUS / QTUCA
   * QTUS = QTUC * KSC
   * QTUC = QTUS / KSC
   */
  public BigDecimal getNombreUSParUCA() {
    if (nombreUSParUCA == null || nombreUSParUCA.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ONE;
    }
    return nombreUSParUCA;
  }
  
  /**
   * Nombre d'unité de commande d'ahcats par unité de conditionnement de stock.
   */
  public BigDecimal getNombreUCAParUCS() {
    return nombreUCAParUCS;
  }
  
  // --------------------------------------------------------------------------
  // Accesseurs sur les quantités
  // --------------------------------------------------------------------------
  
  /**
   * Quantité en reliquat de la ligne en unité de commande d'achats (UCA).
   * Correspond aux champs QTC de la base de données.
   */
  public BigDecimal getQuantiteReliquatUCA() {
    return quantiteReliquatUCA;
  }
  
  /**
   * Modifier la quantité de la ligne en unité de commande d'achats (UCA).
   * 
   * Il faut préserver la quantité stockée en base de donnée, et ne pas la dénaturer avec les arrondis, car on peut avoir des quantités
   * qui sont issues des ventes et pour lesquelles la précision ne correspond pas au nombre de décimales de l'unité (dans le cas de lignes
   * de ventes avec UCV scindable par exemple).
   * Du coup, on traite la quantité avec le maximum de précision (3 décimales) puis on retire les zéros non significatifs si la quantité
   * passée en paramètre a une précision supérieure au nombre de décimales de l'UCA.
   */
  public void setQuantiteReliquatUCA(BigDecimal pQuantiteReliquatUCA, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pQuantiteReliquatUCA == null) {
      throw new MessageErreurException("La quantité en reliquat en UCA est invalide.");
    }
    
    // Modifier la valeur
    if (nombreDecimaleUCA != null) {
      // Arrondir à la précision maximum si la précision du résultat du calcul dépasse le nombre de décimales en paramètre (articles
      // scindables)
      BigDecimal quantiteSansZeros = pQuantiteReliquatUCA.stripTrailingZeros();
      if (quantiteSansZeros.scale() > nombreDecimaleUCA) {
        quantiteReliquatUCA = pQuantiteReliquatUCA.setScale(PRECISION_MAX_QUANTITE, RoundingMode.HALF_UP);
        nombreDecimaleUCA = PRECISION_MAX_QUANTITE;
        nombreDecimaleUA = PRECISION_MAX_QUANTITE;
      }
      else {
        quantiteReliquatUCA = pQuantiteReliquatUCA.setScale(nombreDecimaleUCA, RoundingMode.HALF_UP);
      }
    }
    else {
      quantiteReliquatUCA = pQuantiteReliquatUCA.setScale(0, RoundingMode.HALF_UP);
    }
    
    // Recalculer la quantité en UA à partir de la quantité en UCA
    quantiteReliquatUA = quantiteReliquatUCA.multiply(nombreUAParUCA, MathContext.DECIMAL32);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Quantité en reliquat de la ligne en unité d'achats (UA).
   * Correspond aux champs QTA de la base de données.
   */
  public BigDecimal getQuantiteReliquatUA() {
    return quantiteReliquatUA;
  }
  
  /**
   * Modifier la quantité en reliquat en unité d'achats (UA).
   */
  public void setQuantiteReliquatUA(BigDecimal pQuantiteReliquatUA, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pQuantiteReliquatUA == null) {
      throw new MessageErreurException("La quantité en reliquat en UA est invalide.");
    }
    
    // Modifier la valeur
    quantiteReliquatUA = pQuantiteReliquatUA.setScale(nombreDecimaleUA, RoundingMode.HALF_UP);
    
    // Recalculer la quantité en UCA à partir de la quantité en UA
    quantiteReliquatUCA = quantiteReliquatUA.divide(getNombreUAParUCA(), MathContext.DECIMAL32);
    if (nombreDecimaleUCA != null) {
      // Arrondir à la précision maximum si la précision du résultat du calcul dépasse le nombre de décimales en paramètre (articles
      // scindables)
      BigDecimal quantiteSansZeros = quantiteReliquatUCA.stripTrailingZeros();
      if (quantiteSansZeros.scale() > nombreDecimaleUCA) {
        quantiteReliquatUCA = quantiteReliquatUCA.setScale(PRECISION_MAX_QUANTITE, RoundingMode.HALF_UP);
        nombreDecimaleUCA = PRECISION_MAX_QUANTITE;
        nombreDecimaleUA = PRECISION_MAX_QUANTITE;
      }
      else {
        quantiteReliquatUCA = quantiteReliquatUCA.setScale(nombreDecimaleUCA, RoundingMode.HALF_UP);
      }
    }
    else {
      quantiteReliquatUCA = quantiteReliquatUCA.setScale(0, RoundingMode.HALF_UP);
    }
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      // Relancer le calcul total (qui part de la quantité UCA)
      calculer(true);
    }
  }
  
  /**
   * Quantité en reliquat de la ligne en unité de stocks (US).
   * Correspond aux champs QTS de la base de données.
   */
  public BigDecimal getQuantiteReliquatUS() {
    return quantiteReliquatUS;
  }
  
  /**
   * Modifier la quantité en reliquat en unité de stocks (US).
   */
  public void setQuantiteReliquatUS(BigDecimal pQuantiteReliquatUS, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pQuantiteReliquatUS == null) {
      throw new MessageErreurException("La quantité en reliquat en US est invalide.");
    }
    
    // Modifier la valeur
    quantiteReliquatUS = pQuantiteReliquatUS.setScale(nombreDecimaleUS, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Quantité initiale de la ligne en unité de commande d'achats (UCA).
   * Correspond aux champs QTC de la base de données.
   */
  public BigDecimal getQuantiteInitialeUCA() {
    return quantiteInitialeUCA;
  }
  
  /**
   * Modifier la quantité initiale de la ligne en unité de commande d'achats (UCA).
   */
  public void setQuantiteInitialeUCA(BigDecimal pQuantiteInitialeUCA, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pQuantiteInitialeUCA == null) {
      throw new MessageErreurException("La quantité initiale en UCA est invalide.");
    }
    
    // Modifier la valeur
    if (nombreDecimaleUCA != null) {
      quantiteInitialeUCA = pQuantiteInitialeUCA.setScale(nombreDecimaleUCA, RoundingMode.HALF_UP);
    }
    else {
      quantiteInitialeUCA = pQuantiteInitialeUCA.setScale(0, RoundingMode.HALF_UP);
    }
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Quantité initiale de la ligne en unité d'achats (UA).
   * Correspond aux champs QTA de la base de données.
   */
  public BigDecimal getQuantiteInitialeUA() {
    return quantiteInitialeUA;
  }
  
  /**
   * Modifier la quantité initiale en unité d'achats (UA).
   */
  public void setQuantiteInitialeUA(BigDecimal pQuantiteInitialeUA, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pQuantiteInitialeUA == null) {
      throw new MessageErreurException("La quantité initiale en UA est invalide.");
    }
    
    // Modifier la valeur
    quantiteInitialeUA = pQuantiteInitialeUA.setScale(nombreDecimaleUA, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      // Recalculer la quantité en UCA à partir de la quanaité en UA
      quantiteInitialeUCA = quantiteInitialeUA.divide(getNombreUAParUCA(), MathContext.DECIMAL32);
      quantiteInitialeUCA = quantiteInitialeUCA.setScale(nombreDecimaleUCA, RoundingMode.HALF_UP);
      
      // Relancer le calcul total (qui part de la quantité UCA)
      calculer(true);
    }
  }
  
  /**
   * Quantité initiale de la ligne en unité de stocks (US).
   * Correspond aux champs QTS de la base de données.
   */
  public BigDecimal getQuantiteInitialeUS() {
    return quantiteInitialeUS;
  }
  
  /**
   * Modifier la quantité initiale en unité de stocks (US).
   */
  public void setQuantiteInitialeUS(BigDecimal pQuantiteInitialeUS, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pQuantiteInitialeUS == null) {
      throw new MessageErreurException("La quantité initiale en US est invalide.");
    }
    
    // Modifier la valeur
    quantiteInitialeUS = pQuantiteInitialeUS.setScale(nombreDecimaleUS, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Quantité traitée de la ligne en unité de commande d'achats (UCA).
   * Correspond aux champs QTC de la base de données.
   */
  public BigDecimal getQuantiteTraiteeUCA() {
    if (nombreDecimaleUCA != null) {
      quantiteTraiteeUCA = quantiteTraiteeUCA.stripTrailingZeros();
      if (quantiteTraiteeUCA.scale() > nombreDecimaleUCA) {
        quantiteTraiteeUCA = quantiteTraiteeUCA.setScale(PRECISION_MAX_QUANTITE, RoundingMode.HALF_UP);
      }
      else {
        quantiteTraiteeUCA = quantiteTraiteeUCA.setScale(quantiteReliquatUCA.scale(), RoundingMode.HALF_UP);
      }
    }
    else {
      quantiteTraiteeUCA = quantiteTraiteeUCA.setScale(0, RoundingMode.HALF_UP);
    }
    return quantiteTraiteeUCA;
  }
  
  /**
   * Modifier la quantité traitée de la ligne en unité de commande d'achats (UCA).
   * 
   * Il faut préserver la quantité stockée en base de donnée, et ne pas la dénaturer avec les arrondis, car on peut avoir des quantités
   * qui sont issues des ventes et pour lesquelles la précision ne correspond pas au nombre de décimales de l'unité (dans le cas de lignes
   * de ventes avec UCV scindable par exemple).
   * Du coup, on traite la quantité avec le maximum de précision (3 décimales) puis on retire les zéros non significatifs si la quantité
   * passée en paramètre a une précision supérieure au nombre de décimales de l'UCA.
   */
  public void setQuantiteTraiteeUCA(BigDecimal pQuantiteTraiteeUCA, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pQuantiteTraiteeUCA == null) {
      throw new MessageErreurException("La quantité traitée en UCA est invalide.");
    }
    
    // Modifier la valeur
    if (nombreDecimaleUCA != null) {
      // Arrondir à la précision maximum si la précision du résultat du calcul dépasse le nombre de décimales en paramètre (articles
      // scindables)
      BigDecimal quantiteSansZeros = pQuantiteTraiteeUCA.stripTrailingZeros();
      if (quantiteSansZeros.scale() > nombreDecimaleUCA) {
        quantiteTraiteeUCA = pQuantiteTraiteeUCA.setScale(PRECISION_MAX_QUANTITE, RoundingMode.HALF_UP);
        nombreDecimaleUCA = PRECISION_MAX_QUANTITE;
        nombreDecimaleUA = PRECISION_MAX_QUANTITE;
      }
      else {
        quantiteTraiteeUCA = pQuantiteTraiteeUCA.setScale(nombreDecimaleUCA, RoundingMode.HALF_UP);
      }
    }
    else {
      quantiteTraiteeUCA = pQuantiteTraiteeUCA.setScale(0, RoundingMode.HALF_UP);
    }
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Quantité traitée de la ligne en unité d'achats (UA).
   * Correspond aux champs QTA de la base de données.
   */
  public BigDecimal getQuantiteTraiteeUA() {
    return quantiteTraiteeUA;
  }
  
  /**
   * Modifier la quantité traitée en unité d'achats (UA).
   */
  public void setQuantiteTraiteeUA(BigDecimal pQuantiteTraiteeUA, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pQuantiteTraiteeUA == null) {
      throw new MessageErreurException("La quantité traitée en UA est invalide.");
    }
    
    // Modifier la valeur
    quantiteTraiteeUA = pQuantiteTraiteeUA.setScale(nombreDecimaleUA, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      // Recalculer la quantité en UCA à partir de la quanaité en UA
      quantiteTraiteeUCA = quantiteTraiteeUA.divide(getNombreUAParUCA(), MathContext.DECIMAL32);
      quantiteTraiteeUCA = quantiteTraiteeUCA.setScale(nombreDecimaleUCA, RoundingMode.HALF_UP);
      
      // Relancer le calcul total (qui part de la quantité UCA)
      calculer(true);
    }
  }
  
  /**
   * Quantité traitée de la ligne en unité de stocks (US).
   * Correspond aux champs QTS de la base de données.
   */
  public BigDecimal getQuantiteTraiteeUS() {
    return quantiteTraiteeUS;
  }
  
  /**
   * Modifier la quantité traitée en unité de stocks (US).
   */
  public void setQuantiteTraiteeUS(BigDecimal pQuantiteTraiteeUS, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pQuantiteTraiteeUS == null) {
      throw new MessageErreurException("La quantité traitée en US est invalide.");
    }
    
    // Modifier la valeur
    quantiteTraiteeUS = pQuantiteTraiteeUS.setScale(nombreDecimaleUS, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  // --------------------------------------------------------------------------
  // Accesseurs sur les montants
  // --------------------------------------------------------------------------
  
  /**
   * Prix d'achat brut HT en unités d'achats (2 chiffres après la virgules).
   * Lorsqu'elle est sauvé dans une ligne de documents, cette valeur est nommé "Prix de base HT".
   * Correspond aux champs PAB de la base de données.
   */
  public BigDecimal getPrixAchatBrutHT() {
    return prixAchatBrutHT;
  }
  
  /**
   * Modifier le prix d'achat brut HT (en unités d'achats).
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPrixAchatBrutHT(BigDecimal pPrixAchatBrutHT, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pPrixAchatBrutHT == null) {
      throw new MessageErreurException("Le prix d'achat brut HT est invalide");
    }
    if (pPrixAchatBrutHT.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le prix d'achat brut HT doit être supérieur ou égal à 0");
    }
    
    // Modifier la valeur
    prixAchatBrutHT = pPrixAchatBrutHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Liste des remises fournisseurs.
   */
  public ListeRemise getListeRemise() {
    return listeRemise;
  }
  
  /**
   * Liste des remises fournisseurs.
   */
  public void setListeRemise(ListeRemise pListeRemise, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pListeRemise == null) {
      throw new MessageErreurException("La liste des remises fournisseurs est invalide.");
    }
    
    // Modifier la valeur
    listeRemise = pListeRemise;
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Pourcentage de remise 1 (2 chiffres après la virgules).
   * Correspond aux champs REM1 de la base de données.
   */
  public BigDecimal getPourcentageRemise1() {
    return listeRemise.getPourcentageRemise1();
  }
  
  /**
   * Modifier le pourcentage de remise 1.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise1(BigDecimal pRemise, boolean pRecalculer) {
    listeRemise.setPourcentageRemise1(pRemise);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Pourcentage de remise 2 (2 chiffres après la virgules).
   * Correspond aux champs REM2 de la base de données.
   */
  public BigDecimal getPourcentageRemise2() {
    return listeRemise.getPourcentageRemise2();
  }
  
  /**
   * Modifier le pourcentage de remise 2 (2 chiffres après la virgules).
   */
  public void setPourcentageRemise2(BigDecimal pRemise, boolean pRecalculer) {
    listeRemise.setPourcentageRemise2(pRemise);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Pourcentage de remise 3 (2 chiffres après la virgules).
   * Correspond aux champs REM3 de la base de données.
   */
  public BigDecimal getPourcentageRemise3() {
    return listeRemise.getPourcentageRemise3();
  }
  
  /**
   * Modifier le pourcentage de remise 3.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise3(BigDecimal pRemise, boolean pRecalculer) {
    listeRemise.setPourcentageRemise3(pRemise);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Pourcentage de remise 4 (2 chiffres après la virgules).
   * Correspond aux champs REM4 de la base de données.
   */
  public BigDecimal getPourcentageRemise4() {
    return listeRemise.getPourcentageRemise4();
  }
  
  /**
   * Modifier le pourcentage de remise 4.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise4(BigDecimal pRemise, boolean pRecalculer) {
    listeRemise.setPourcentageRemise4(pRemise);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Pourcentage de remise 5 (2 chiffres après la virgules).
   * Correspond aux champs REM5 de la base de données.
   */
  public BigDecimal getPourcentageRemise5() {
    return listeRemise.getPourcentageRemise5();
  }
  
  /**
   * Modifier le pourcentage de remise 5.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise5(BigDecimal pRemise, boolean pRecalculer) {
    listeRemise.setPourcentageRemise5(pRemise);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Pourcentage de remise 6 (2 chiffres après la virgules).
   * Correspond aux champs REM6 de la base de données.
   */
  public BigDecimal getPourcentageRemise6() {
    return listeRemise.getPourcentageRemise6();
  }
  
  /**
   * Modifier le pourcentage de remise 6.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise6(BigDecimal pRemise, boolean pRecalculer) {
    listeRemise.setPourcentageRemise6(pRemise);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Coefficient de remise (4 chiffres après la virgules).
   * 
   * Le coefficient de remise n'apparaît pas dans un document d'achat. Il s'applique avant et impacte le prix d'achat
   * brut HT du document.
   * Il y a donc en réalité deux prix d'achats bruts HT :
   * - Prix d'achat brut HT non remisé : c'est celui affiché dans la condition d'achats.
   * - Prix d'achat brut HT remisé : c'est celui affiché dans la négociation achat d'un document d'achats.
   * A court terme, nous allons sûrement supprimer ce coefficient de remise qui n'apporte pas grand chose si ce n'est de
   * la confusion.
   * En attendant, j'ai déplacé ce coefficient au dessus du "Prix d'achat brut HT" dans la condition d'achats. C'est
   * surprenant mais il
   * est
   * positionné ainsi pour se rappeler qu'il influence le prix d'achat brut HT avant d'être affiché dans un document
   * d'achats.
   */
  public BigDecimal getCoefficientRemise() {
    return coefficientRemise;
  }
  
  /**
   * Modifier le coefficient de remise.
   * La valeur fournie est arrondie à 4 chiffres après la virgule.
   */
  public void setCoefficientRemise(BigDecimal pCoefficientRemise, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pCoefficientRemise == null) {
      throw new MessageErreurException("Le coefficient de remise est invalide.");
    }
    if (pCoefficientRemise.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le coefficient de remise doit être supérieur ou égal à 0.");
    }
    if (pCoefficientRemise.compareTo(BigDecimal.ONE) > 0) {
      throw new MessageErreurException("Le coefficient de remise doit être inférieur ou égal à 1.");
    }
    
    // Modifier la valeur
    if (coefficientRemise.compareTo(BigDecimal.ZERO) == 0) {
      coefficientRemise = BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP);
    }
    else {
      coefficientRemise = pCoefficientRemise.setScale(4, RoundingMode.HALF_UP);
    }
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Pourcentage de taxe ou de majoration (4 chiffres après la virgules).
   * Correspond aux champs FK3 de la base de données.
   */
  public BigDecimal getPourcentageTaxeOuMajoration() {
    return pourcentageTaxeOuMajoration;
  }
  
  /**
   * Modifier le pourcentage de taxe ou de majoration.
   * La valeur fournie est arrondie à 4 chiffres après la virgule.
   */
  public void setPourcentageTaxeOuMajoration(BigDecimal pPourcentageTaxeOuMajoration, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pPourcentageTaxeOuMajoration == null) {
      throw new MessageErreurException("Le pourcentage de taxe ou de majoration est invalide.");
    }
    if (pPourcentageTaxeOuMajoration.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le pourcentage de taxe ou de majoration doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    pourcentageTaxeOuMajoration = pPourcentageTaxeOuMajoration.setScale(4, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Montant de conditionnement (2 chiffres après la virgules).
   * Correspond aux champs FV3 de la base de données.
   */
  public BigDecimal getMontantConditionnement() {
    return montantConditionnement;
  }
  
  /**
   * Modifier le montant du conditionnement
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setMontantConditionnement(BigDecimal pMontantConditionnement, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pMontantConditionnement == null) {
      throw new MessageErreurException("Le montant du conditionnement est invalide.");
    }
    if (pMontantConditionnement.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le montant du conditionnement doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    montantConditionnement = pMontantConditionnement.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Prix d'achat net HT par unités d'achats (2 chiffres après la virgules).
   * C'est le prix d'achat net calculé à partir des éléments tarifaires.
   * Correspond aux champs ...PAC de la base de données. Attention, le champs PAN de la base de données correspondant à
   * un prix net saisi.
   * Correspond aux champs PAC de la base de données.
   */
  public BigDecimal getPrixAchatNetHT() {
    return prixAchatNetHT;
  }
  
  /**
   * Modifier le prix d'achat net HT.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   * Cette valeur ne peut pas être saisie. Elle est uniquement initialisée à partir de la base de données. Le reste
   * du temps, elle est recalculée à partir des autres éléments.
   */
  public void setPrixAchatNetHT(BigDecimal pPrixAchatNetHT) {
    // Contrôler la valeur fournie
    if (pPrixAchatNetHT == null) {
      throw new MessageErreurException("Le prix d'achat net HT est invalide.");
    }
    
    // Modifier la valeur
    prixAchatNetHT = pPrixAchatNetHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPrixAchatCalculeHT() {
    return prixAchatCalculeHT;
  }
  
  public void setPrixAchatCalculeHT(BigDecimal prixAchatCalculeHT) {
    this.prixAchatCalculeHT = prixAchatCalculeHT;
  }
  
  /**
   * Indiquer le mode de saisie du port.
   * Deux mode spossibles :
   * - Soit à partir du montant au poids multiplié par le poids.
   * - Soit à partir du pourcentage de port appliqué au prix net HT
   */
  public EnumModeSaisiePort getModeSaisiePort() {
    return modeSaisiePort;
  }
  
  /**
   * Modifier le mode de saisie du port.
   */
  public void setModeSaisiePort(EnumModeSaisiePort pModeSaisiePort, boolean pRecalculer) {
    // Mettre le mode par défaut si rien n'est fourni
    if (pModeSaisiePort == null) {
      pModeSaisiePort = EnumModeSaisiePort.MONTANT_FOIS_POIDS;
    }
    
    // Mémoriser le mode
    modeSaisiePort = pModeSaisiePort;
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Montant au poids du port HT (2 chiffres après la virgules).
   * Ce montant est multiplié par le poids pour calculer le montant total HT du port.
   * MontantPortHT = MontantAuPoidsHT x PoidsPort
   * Correspond aux champs FV1 de la base de données.
   */
  public BigDecimal getMontantAuPoidsPortHT() {
    return montantAuPoidsPortHT;
  }
  
  /**
   * Modifier le montant au poids du port HT.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setMontantAuPoidsPortHT(BigDecimal pMontantAuPoidsPortHT, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pMontantAuPoidsPortHT == null) {
      throw new MessageErreurException("Le montant au poids du port HT est invalide.");
    }
    if (pMontantAuPoidsPortHT.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le montant au poids du port HT doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    montantAuPoidsPortHT = pMontantAuPoidsPortHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Poids associé au port (4 chiffres après la virgules).
   * Cette valeur est multipliée par le montant au poids pour calculer le montant total HT du port.
   * MontantPortHT = MontantAuPoidsHT x PoidsPort
   * Correspond aux champs FK1 de la base de données.
   */
  public BigDecimal getPoidsPort() {
    return poidsPort;
  }
  
  /**
   * Modifier le poids associé au port.
   * La valeur fournie est arrondie à 4 chiffres après la virgule.
   */
  public void setPoidsPort(BigDecimal pPoidsPort, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pPoidsPort == null) {
      throw new MessageErreurException("Le poids associé au port est invalide.");
    }
    if (pPoidsPort.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le poids associé au port doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    poidsPort = pPoidsPort.setScale(4, RoundingMode.HALF_UP);
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Pourcentage de port (2 chiffres après la virgules).
   * Ce pourcentage est appliqué au prix d'achat net HT pour calculer le montant total HT du port.
   * MontantTotalPortHT = PrixAchatNetHT * (1 + PourcentagePort / 100)
   * Correspond aux champs FP1 de la base de données.
   */
  public BigPercentage getPourcentagePort() {
    return pourcentagePort;
  }
  
  /**
   * Modifier le pourcentage de port.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentagePort(BigPercentage pPourcentagePort, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pPourcentagePort == null) {
      throw new MessageErreurException("Le pourcentage du port est invalide.");
    }
    if (!pPourcentagePort.isPositif()) {
      throw new MessageErreurException("Le pourcentage du port doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    pourcentagePort = pPourcentagePort;
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Montant unitaire du port HT (2 chiffres après la virgules).
   * Ce montant est calculé de deux façon, soit à partir du pourcentage de port appliqué au prix net HT, soit à partir
   * du montant au
   * poids multiplié par le poids.
   * MontantPortHT = MontantAuPoidsHT x PoidsPort
   * MontantPortHT = PrixAchatNetHT * (1 + PourcentagePort / 100)
   * Correspond aux champs PORTF de la base de données.
   */
  public BigDecimal getMontantPortHT() {
    return montantPortHT;
  }
  
  /**
   * Modifier le montant du port HT.
   * Cette valeur ne peut pas être saisie. Elle est uniquement initialisée à partir de la base de données. Le reste
   * du temps, elle est recalculée à partir des autres éléments.
   */
  public void setMontantPortHT(BigDecimal pMontantPortHT) {
    // Contrôler la valeur fournie
    if (pMontantPortHT == null) {
      throw new MessageErreurException("Le montant du port est invalide.");
    }
    if (pMontantPortHT.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le montant du port doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    montantPortHT = pMontantPortHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Prix de revient forunisseur HT en unités d'achats (2 chiffres après la virgules).
   * Correspond aux champs PRVFR de la base de données.
   */
  public BigDecimal getPrixRevientFournisseurHT() {
    return prixRevientFournisseurHT;
  }
  
  /**
   * Modifier le prix de revient forunisseur HT.
   * Cette valeur ne peut pas être saisie. Elle est uniquement initialisée à partir de la base de données. Le reste
   * du temps, elle est recalculée à partir des autres éléments.
   */
  private void setPrixRevientFournisseurHT(BigDecimal pPrixRevientFournisseurHT) {
    // Contrôler la valeur fournie
    if (pPrixRevientFournisseurHT == null) {
      throw new MessageErreurException("Le prix de revient fournisseur est invalide.");
    }
    if (pPrixRevientFournisseurHT.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le prix de revient fournisseur doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    prixRevientFournisseurHT = pPrixRevientFournisseurHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Prix de revient fournisseur HT en unités de stocks (2 chiffres après la virgules).
   */
  public BigDecimal getPrixRevientFournisseurHTEnUS() {
    return prixRevientFournisseurHTEnUS;
  }
  
  /**
   * Pourcentage de majoration (2 chiffres après la virgules).
   */
  public BigPercentage getPourcentageMajoration() {
    return pourcentageMajoration;
  }
  
  /**
   * Modifier le pourcentage de majoration.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageMajoration(BigPercentage pPourcentageMajoration, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pPourcentageMajoration == null) {
      throw new MessageErreurException("Le pourcentage de majoration est invalide.");
    }
    if (!pPourcentageMajoration.isPositif()) {
      throw new MessageErreurException("Le pourcentage de majoration doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    pourcentageMajoration = pPourcentageMajoration;
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Coefficient d'approche fixe (4 chiffres après la virgules).
   */
  public BigDecimal getCoefficientApprocheFixe() {
    return coefficientApprocheFixe;
  }
  
  /**
   * Modifier le coefficient d'approche fixe.
   * La valeur fournie est arrondie à 4 chiffres après la virgule.
   */
  public void setCoefficientApprocheFixe(BigDecimal pCoefficientApprocheFixe, boolean pRecalculer) {
    // Contrôler la valeur fournie
    if (pCoefficientApprocheFixe == null) {
      throw new MessageErreurException("Le coefficient d'approche fixe est invalide.");
    }
    if (pCoefficientApprocheFixe.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le coefficient d'approche fixe doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    if (coefficientApprocheFixe.compareTo(BigDecimal.ZERO) == 0) {
      coefficientApprocheFixe = BigDecimal.ONE;
    }
    else {
      coefficientApprocheFixe = pCoefficientApprocheFixe.setScale(4, RoundingMode.HALF_UP);
    }
    
    // Recalculer les éléments de prix d'achats si c'est demandé
    if (pRecalculer) {
      calculer(true);
    }
  }
  
  /**
   * Prix de revient standard en unité d'achats (2 chiffres après la virgules).
   */
  public BigDecimal getPrixRevientStandardHT() {
    return prixRevientStandardHT;
  }
  
  /**
   * Modifir le prix de revient standard HT.
   * Cette valeur ne peut pas être saisie. Elle est uniquement initialisée à partir de la base de données. Le reste
   * du temps, elle est recalculée à partir des autres éléments. *
   */
  public void setPrixRevientStandardHT(BigDecimal pPrixRevientStandardHT) {
    // Contrôler la valeur fournie
    if (pPrixRevientStandardHT == null) {
      throw new MessageErreurException("Le prix de revient standard HT est invalide.");
    }
    if (pPrixRevientStandardHT.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le prix de revient standard HT doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    prixRevientStandardHT = pPrixRevientStandardHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Prix de revient standard en unité de stocks (2 chiffres après la virgules).
   */
  public BigDecimal getPrixRevientStandardHTEnUS() {
    return prixRevientStandardHTEnUS;
  }
  
  /**
   * Modifier le prix de revient standard HT en US.
   * Cette valeur ne peut pas être saisie. Elle est uniquement initialisée à partir de la base de données. Le reste
   * du temps, elle est recalculée à partir des autres éléments. *
   */
  public void setPrixRevientStandardHTEnUS(BigDecimal pPrixRevientStandardHTEnUS) {
    // Contrôler la valeur fournie
    if (pPrixRevientStandardHTEnUS == null) {
      throw new MessageErreurException("Le prix de revient standard HT EN US est invalide.");
    }
    if (pPrixRevientStandardHTEnUS.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le prix de revient standard HT en US doit être supérieur ou égal à 0.");
    }
    
    // Modifier la valeur
    prixRevientStandardHTEnUS = pPrixRevientStandardHTEnUS.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Montant en reliquat HT de la ligne.
   * Correspond aux champs MHT de la base de données.
   */
  public BigDecimal getMontantReliquatHT() {
    return montantReliquatHT;
  }
  
  /**
   * Modifier le montant en reliquat HT de la ligne.
   * Cette valeur ne peut pas être saisie. Elle est uniquement initialisée à partir de la base de données. Le reste
   * du temps, elle est recalculée à partir des autres éléments.
   */
  public void setMontantReliquatHT(BigDecimal pMontantReliquatHT) {
    // Contrôler la valeur fournie
    if (pMontantReliquatHT == null) {
      throw new MessageErreurException("Le montant HT du reliquat de la ligne est invalide.");
    }
    
    // Modifier la valeur
    montantReliquatHT = pMontantReliquatHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Montant initial HT de la ligne.
   * Correspond aux champs MHTI de la base de données.
   */
  public BigDecimal getMontantInitialHT() {
    return montantInitialHT;
  }
  
  /**
   * Modifier le montant initial HT de la ligne.
   * Cette valeur ne peut pas être saisie. Elle est uniquement initialisée à partir de la base de données. Le reste
   * du temps, elle est recalculée à partir des autres éléments.
   */
  public void setMontantInitialHT(BigDecimal pMontantInitialHT) {
    // Contrôler la valeur fournie
    if (pMontantInitialHT == null) {
      throw new MessageErreurException("Le montant HT initial de la ligne est invalide.");
    }
    
    // Modifier la valeur
    montantInitialHT = pMontantInitialHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Montant traité HT de la ligne.
   * Correspond aux champs MHTT de la base de données.
   */
  public BigDecimal getMontantTraiteHT() {
    return montantTraiteHT;
  }
  
  /**
   * Modifier le montant traité HT de la ligne.
   * Cette valeur ne peut pas être saisie. Elle est uniquement initialisée à partir de la base de données. Le reste
   * du temps, elle est recalculée à partir des autres éléments.
   */
  public void setMontantTraiteHT(BigDecimal pMontantTraiteHT) {
    // Contrôler la valeur fournie
    if (pMontantTraiteHT == null) {
      throw new MessageErreurException("Le montant HT traité de la ligne est invalide.");
    }
    
    // Modifier la valeur
    montantTraiteHT = pMontantTraiteHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  // --------------------------------------------------------------------------
  // Accesseurs sur les exclusions de remises de pied
  // --------------------------------------------------------------------------
  
  /**
   * Liste des exclusions de remises de pied.
   */
  public Character[] getExclusionRemisePied() {
    return exclusionRemisePied;
  }
  
  /**
   * Exlusion de remise de pied à l'index voulu.
   */
  public Character getExclusionRemisePied(int pIndexRemise) {
    if (0 < pIndexRemise && pIndexRemise >= NOMBRE_REMISES) {
      return null;
    }
    if (exclusionRemisePied[pIndexRemise] == null) {
      return ' ';
    }
    return exclusionRemisePied[pIndexRemise];
  }
  
  // --------------------------------------------------------------------------
  // Accesseurs sur les indicateurs
  // --------------------------------------------------------------------------
  
  /**
   * Indique que l'utilisateur a saisit manuellement le prix d'achat brut HT.
   * Correspond aux champs TB de la base de données.
   */
  public Integer getTopPrixBaseSaisie() {
    return topPrixBaseSaisie;
  }
  
  /**
   * Indique que l'utilisateur a saisit des remises différentes de cells de la condition d'achats.
   * Correspond aux champs TR de la base de données.
   */
  public Integer getTopRemiseSaisie() {
    return topRemiseSaisie;
  }
  
  /**
   * Indique que l'utilisateur a saisit manuellement le prix net HT.
   * Correspond aux champs TN de la base de données.
   */
  public Integer getTopPrixNetSaisi() {
    return topPrixNetSaisi;
  }
  
  /**
   * Indique que l'utilisateur a saisit manuellement des unité différentes.
   * Il n'y a plus de corélation entre l'unité d'achats et l'unité de commande d'achats.
   * Correspond aux champs TU de la base de données.
   */
  public Integer getTopUniteSaisie() {
    return topUniteSaisie;
  }
  
  /**
   * A précise.
   * Correspond aux champs TQ de la base de données.
   */
  
  public Integer getTopQuantiteSaisie() {
    return topQuantiteSaisie;
  }
  
  /**
   * Indique que l'utilisateur a saisit manuellement un montant HT total pour la ligne.
   * Correspond aux champs TH de la base de données.
   */
  
  public Integer getTopMontantHTSaisi() {
    return topMontantHTSaisi;
  }
  
  // --------------------------------------------------------------------------
  // Accesseurs sur la TVA
  // --------------------------------------------------------------------------
  
  /**
   * Code TVA appliqué à ce prix.
   * Correspond aux champs TVA de la base de données.
   */
  public Integer getCodeTVA() {
    return codeTVA;
  }
  
  /**
   * Modifier le code TVA appliqué à ce prix.
   */
  public void setCodeTVA(Integer pCodeTVA) {
    codeTVA = pCodeTVA;
  }
  
  /**
   * Numéro de la colonne de TVA appliqué à ce prix.
   * Correspond aux champs COL de la base de données.
   */
  public Integer getColonneTVA() {
    return colonneTVA;
  }
  
  /**
   * Modifier le numéro de la colonne de TVA appliqué à ce prix.
   */
  public void setColonneTVA(Integer pColonneTVA) {
    colonneTVA = pColonneTVA;
  }
  
  public Boolean isQuantiteNonMultipleConditionnement() {
    return quantiteNonMultipleConditionnement;
  }
}
