/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.lienligne;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEtatDocument;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatBonDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Un lien est une ligne à l'origine ou au départ d'une autre ligne (d'achat ou de vente) observée.
 */
public class LienLigne extends AbstractClasseMetier<IdLienLigne> implements Comparable<LienLigne> {
  // Variables
  private LigneVente ligneVente = null;
  private LigneAchat ligneAchat = null;
  private DocumentVente documentVente = null;
  private DocumentAchat documentAchat = null;
  private ListeUnite listeUnite = null;
  private boolean documentOrigine = false;
  private BigDecimal quantiteDisponibleVente = null;
  private IdMagasin idMagasin = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public LienLigne(IdLienLigne pIdLienLigne) {
    super(pIdLienLigne);
    switch (pIdLienLigne.getType()) {
      case LIEN_LIGNE_VENTE:
        documentVente = new DocumentVente(IdDocumentVente.getInstanceGenerique(getId().getIdEtablissement(),
            EnumCodeEnteteDocumentVente.valueOfByCode(getId().getCodeEnteteDocument()), getId().getNumeroDocument(),
            getId().getSuffixeDocument(), getId().getNumeroFacture()));
        break;
      
      case LIEN_LIGNE_ACHAT:
        documentAchat = new DocumentAchat(IdDocumentAchat.getInstance(getId().getIdEtablissement(),
            EnumCodeEnteteDocumentAchat.valueOfByCode(getId().getCodeEnteteDocument()), getId().getNumeroDocument(),
            getId().getSuffixeDocument()));
        break;
      
      default:
        break;
    }
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  @Override
  public int compareTo(LienLigne o) {
    if (getDocumentVente().equals(o.getDocumentVente()) && getDocumentAchat().equals(o.getDocumentAchat())
        && getLigne().equals(o.getLigne())) {
      return 0;
    }
    return 1;
  }
  
  /**
   * Retourne un message décrivant le statut du lien suivant le type de lien.
   */
  public List<Message> getStatut() {
    List<Message> listeMessage = new ArrayList<Message>();
    
    switch (id.getType()) {
      case LIEN_LIGNE_VENTE:
        ajouterMessageLivraison(listeMessage);
        ajouterMessageReglement(listeMessage);
        break;
      
      case LIEN_LIGNE_ACHAT:
        switch (documentAchat.getCodeEtat()) {
          case VALIDE:
            ajouterMessageAchatValide(listeMessage);
            break;
          
          case RECU:
          case RECU_ET_RAPPROCHE_FACTURE:
            ajouterMessageAchatReceptionne(listeMessage);
            break;
          
          case FACTURE:
          case COMPTABILISE:
            ajouterMessageAchatComptabilise(listeMessage);
            break;
          
          default:
            break;
        }
        break;
      
      default:
        break;
    }
    
    return listeMessage;
    
  }
  
  /**
   * Retourne l'état d'extraction.
   */
  public String getEtatExtraction() {
    if (ligneVente.getQuantiteDocumentOrigineUC().compareTo(ligneVente.getQuantiteDejaExtraiteUC()) == 0) {
      return "";
    }
    else if (retournerQuantiteRestante(ligneVente).compareTo(BigDecimal.ZERO) > 0) {
      return "JAUNE";
    }
    return "VERT";
  }
  
  /**
   * Calculer la quantité d'origine pour les documents d'achat.
   */
  public BigDecimal calculerQuantiteOrigine() {
    LigneAchatArticle ligne = (LigneAchatArticle) ligneAchat;
    BigDecimal quantiteDepart = ligne.getQuantiteDocumentOrigine();
    if (quantiteDepart.compareTo(ligne.getQuantiteDejaExtraite()) < 0) {
      quantiteDepart = ligne.getQuantiteDejaExtraite();
    }
    BigDecimal quantiteOrigine = quantiteDepart.divide(ligne.getPrixAchat().getNombreUAParUCA());
    if (documentAchat.getId().getSuffixe() == 0 && ligne.getPrixAchat().getQuantiteReliquatUS().compareTo(quantiteOrigine) >= 0) {
      return ligne.getPrixAchat().getQuantiteReliquatUCA();
    }
    else {
      return quantiteOrigine;
    }
  }
  
  /**
   * Calculer la quantité extraite sur une commande d'achat.
   */
  public BigDecimal calculerQuantiteExtraite() {
    LigneAchatArticle ligne = (LigneAchatArticle) ligneAchat;
    BigDecimal quantiteExtraite = ligne.getQuantiteDejaExtraite().divide(ligne.getPrixAchat().getNombreUAParUCA());
    
    return quantiteExtraite;
    
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne l'état de livraison, commande ou facturation d'une ligne.
   */
  private void ajouterMessageLivraison(List<Message> pListeMessage) {
    String message = null;
    if (documentVente.isDevis()) {
      return;
    }
    
    if (ligneVente.isLigneArticleSpecial()) {
      message = "Article spécial";
      pListeMessage.add(Message.getMessageNormal(message));
    }
    if (ligneVente.getQuantiteUCV().compareTo(BigDecimal.ZERO) < 0) {
      if (ligneVente.isLignePalette()) {
        message = "Déconsignation";
        pListeMessage.add(Message.getMessageNormal(message));
      }
      else {
        message = "Retour";
        pListeMessage.add(Message.getMessageNormal(message));
      }
    }
    else {
      if (ligneVente.isLignePalette()) {
        message = "Consignation ";
        pListeMessage.add(Message.getMessageNormal(message));
      }
      else {
        if (documentVente.isCommande()) {
          if (documentVente.isLivraison()) {
            if (documentVente.getEtat().equals(EnumEtatBonDocumentVente.COMPTABILISE)
                || documentVente.getEtat().equals(EnumEtatBonDocumentVente.FACTURE)
                || documentVente.getEtat().equals(EnumEtatBonDocumentVente.EXPEDIE)) {
              if (getId().isLigneSoldee()) {
                if (retournerQuantiteRestante(ligneVente).equals(BigDecimal.ZERO)) {
                  message = "Totalement traité";
                }
                else {
                  message = "Reliquat abandonné : " + retournerQuantiteRestante(ligneVente) + " "
                      + ligneVente.getIdUniteConditionnementVente().getCode();
                }
                pListeMessage.add(Message.getMessageNormal(message));
              }
              else if (documentVente.getEtatExtraction().isPartielle()
                  && retournerQuantiteRestante(ligneVente).compareTo(BigDecimal.ZERO) > 0) {
                message = "Reste à livrer : " + retournerQuantiteRestante(ligneVente) + " "
                    + ligneVente.getIdUniteConditionnementVente().getCode();
                pListeMessage.add(Message.getMessageImportant(message));
              }
              else {
                message = "Livré le " + Constantes.convertirDateEnTexte(documentVente.getDateExpeditionBon());
                pListeMessage.add(Message.getMessageNormal(message));
              }
            }
            else {
              if (getId().isLigneSoldee()) {
                if (retournerQuantiteRestante(ligneVente).equals(BigDecimal.ZERO)) {
                  message = "Totalement traité";
                }
                else {
                  message = "Reliquat abandonné : " + retournerQuantiteRestante(ligneVente) + " "
                      + ligneVente.getIdUniteConditionnementVente().getCode();
                }
                pListeMessage.add(Message.getMessageNormal(message));
              }
              else if (documentVente.getEtatExtraction().isPartielle()
                  && retournerQuantiteRestante(ligneVente).compareTo(BigDecimal.ZERO) > 0) {
                BigDecimal quantiteRestante = retournerQuantiteRestante(ligneVente);
                message = "Reste à livrer : " + Constantes.formater(quantiteRestante, false) + " "
                    + ligneVente.getIdUniteConditionnementVente().getCode();
                pListeMessage.add(Message.getMessageImportant(message));
              }
              else if (documentVente.getEtatExtraction().isComplete()
                  || retournerQuantiteRestante(ligneVente).compareTo(BigDecimal.ZERO) == 0) {
                message = "Totalement livré";
                pListeMessage.add(Message.getMessageNormal(message));
              }
              else {
                message = "Non livré";
                pListeMessage.add(Message.getMessageImportant(message));
                if (documentVente.getIdDocumentAchatLie() == null && ligneVente.getIdLigneAchatLiee() == null) {
                  message = "Quantité disponible à la vente : " + Constantes.formater(getQuantiteDisponibleVente(), false) + " "
                      + ligneVente.getIdUniteConditionnementVente().getCode();
                  pListeMessage.add(Message.getMessageImportant(message));
                }
              }
            }
          }
          // enlèvement
          else {
            if (documentVente.getEtat().equals(EnumEtatBonDocumentVente.COMPTABILISE)
                || documentVente.getEtat().equals(EnumEtatBonDocumentVente.FACTURE)
                || documentVente.getEtat().equals(EnumEtatBonDocumentVente.EXPEDIE)) {
              if (getId().isLigneSoldee()) {
                if (retournerQuantiteRestante(ligneVente).equals(BigDecimal.ZERO)) {
                  message = "Totalement traité";
                }
                else {
                  message = "Reliquat abandonné : " + retournerQuantiteRestante(ligneVente) + " "
                      + ligneVente.getIdUniteConditionnementVente().getCode();
                }
                pListeMessage.add(Message.getMessageNormal(message));
              }
              else if (documentVente.getEtatExtraction().isPartielle()
                  && retournerQuantiteRestante(ligneVente).compareTo(BigDecimal.ZERO) > 0) {
                if (getId().isLigneSoldee()) {
                  if (retournerQuantiteRestante(ligneVente).equals(BigDecimal.ZERO)) {
                    message = "Totalement traité";
                  }
                  else {
                    message = "Reliquat abandonné : " + retournerQuantiteRestante(ligneVente) + " "
                        + ligneVente.getIdUniteConditionnementVente().getCode();
                  }
                  pListeMessage.add(Message.getMessageNormal(message));
                }
                else {
                  message = "Reste à enlever : " + retournerQuantiteRestante(ligneVente);
                  pListeMessage.add(Message.getMessageImportant(message));
                }
                
              }
              else {
                message = "Enlevé le " + Constantes.convertirDateEnTexte(documentVente.getDateExpeditionBon());
                if (documentVente.getPrisPar() != null && !documentVente.getPrisPar().trim().isEmpty()) {
                  message += " par " + documentVente.getPrisPar();
                }
                pListeMessage.add(Message.getMessageNormal(message));
              }
            }
            else {
              // livraison
              if (getId().isLigneSoldee()) {
                if (retournerQuantiteRestante(ligneVente).equals(BigDecimal.ZERO)) {
                  message = "Totalement traité";
                }
                else {
                  message = "Reliquat abandonné : " + retournerQuantiteRestante(ligneVente) + " "
                      + ligneVente.getIdUniteConditionnementVente().getCode();
                }
                pListeMessage.add(Message.getMessageNormal(message));
              }
              else if (documentVente.getEtatExtraction().isPartielle()
                  && retournerQuantiteRestante(ligneVente).compareTo(BigDecimal.ZERO) > 0) {
                BigDecimal quantiteRestante = retournerQuantiteRestante(ligneVente);
                message = "Reste à enlever : " + Constantes.formater(quantiteRestante, false) + " "
                    + ligneVente.getIdUniteConditionnementVente().getCode();
                pListeMessage.add(Message.getMessageImportant(message));
              }
              else if (documentVente.getEtatExtraction().isComplete()
                  || retournerQuantiteRestante(ligneVente).compareTo(BigDecimal.ZERO) == 0) {
                message = "Totalement enlevé";
                pListeMessage.add(Message.getMessageNormal(message));
              }
              else {
                message = "Non enlevé";
                pListeMessage.add(Message.getMessageImportant(message));
                if (documentVente.getIdDocumentAchatLie() == null && ligneVente.getIdLigneAchatLiee() == null) {
                  message = "Quantité disponible à la vente : " + Constantes.formater(getQuantiteDisponibleVente(), false) + " "
                      + ligneVente.getIdUniteConditionnementVente().getCode();
                  pListeMessage.add(Message.getMessageImportant(message));
                }
              }
            }
          }
          
        }
        else {
          if (documentVente.getDateExpeditionBon() != null) {
            if (documentVente.isLivraison()) {
              message = "Livré le " + Constantes.convertirDateEnTexte(documentVente.getDateExpeditionBon());
              pListeMessage.add(Message.getMessageNormal(message));
            }
            else {
              message = "Enlevé le " + Constantes.convertirDateEnTexte(documentVente.getDateExpeditionBon());
              pListeMessage.add(Message.getMessageNormal(message));
            }
          }
        }
        
      }
      
      // Date de livraison prévue
      if (documentVente.isCommande() && documentVente.isLivraison()) {
        if (documentVente.getTransport().getDateLivraisonPrevue() != null) {
          message = "Livraison prévue le " + Constantes.convertirDateEnTexte(documentVente.getTransport().getDateLivraisonPrevue());
          pListeMessage.add(Message.getMessageNormal(message));
        }
      }
      
      // facturation
      if (!documentVente.isCommande()) {
        if (ligneVente.getQuantiteArticlesFactures().compareTo(ligneVente.getQuantiteUV()) == 0
            || documentVente.getEtat().equals(EnumEtatBonDocumentVente.COMPTABILISE)
            || documentVente.getEtat().equals(EnumEtatBonDocumentVente.FACTURE)) {
          if (getId().getNumeroFacture() > 0) {
            message =
                "Facture " + getId().getNumeroFacture() + " du " + Constantes.convertirDateEnTexte(documentVente.getDateFacturation());
          }
          else {
            message = "Facture " + getId().getNumeroFactureOrigine() + " du "
                + Constantes.convertirDateEnTexte(documentVente.getDateFacturation());
          }
          
          pListeMessage.add(Message.getMessageNormal(message));
        }
        else if (ligneVente.getQuantiteArticlesFactures().compareTo(BigDecimal.ZERO) == 0
            && ligneVente.getQuantiteUV().compareTo(BigDecimal.ZERO) > 0) {
          message = "Non facturé";
          pListeMessage.add(Message.getMessageImportant(message));
        }
        else {
          BigDecimal quantiteArticlesFactures = ligneVente.getQuantiteArticlesFactures();
          message = Constantes.formater(quantiteArticlesFactures, false) + ligneVente.getIdUniteVente().getCode() + "/"
              + Constantes.formater(ligneVente.getQuantiteUV(), false) + ligneVente.getIdUniteVente().getCode() + " facturés";
          pListeMessage.add(Message.getMessageImportant(message));
        }
      }
    }
  }
  
  /**
   * Génère un message sur l'état des réglement d'un document (seulement sur les factures).
   */
  private void ajouterMessageReglement(List<Message> listeMessage) {
    String message = null;
    if (documentVente != null && documentVente.isFacture() && documentVente.getListeReglement() != null
        && documentVente.getListeReglement().getEtatReglement() != null) {
      switch (documentVente.getListeReglement().getEtatReglement()) {
        case REGLE:
          message = "Document entièrement réglé ";
          listeMessage.add(Message.getMessageNormal(message));
          break;
        case PARTIELLEMENT_REGLE:
          // Si on a payé avec un avoir supérieur au montant, le restant dû apparait négatif, en fait il faut dire entièrement réglé.
          if (documentVente.getListeReglement().getMontantTotalARegler()
              .subtract(documentVente.getListeReglement().getMontantTotalCalcule()).compareTo(BigDecimal.ZERO) <= 0) {
            message = "Entièrement réglé";
            listeMessage.add(Message.getMessageNormal(message));
          }
          else {
            message = "Reste à régler : " + Constantes.formater(documentVente.getListeReglement().getMontantTotalARegler()
                .subtract(documentVente.getListeReglement().getMontantTotalCalcule()), true);
            listeMessage.add(Message.getMessageImportant(message));
          }
          break;
        case NON_REGLE:
          message = "Non réglé";
          listeMessage.add(Message.getMessageImportant(message));
          break;
      }
    }
  }
  
  /**
   * Retourne la quantité restant à prendre.
   */
  private BigDecimal retournerQuantiteRestante(LigneVente pLigne) {
    if (pLigne != null) {
      if (pLigne.isLigneCommentaire()) {
        if ((documentVente.isFacture() && pLigne.getTopPersonnalisation5().isEmpty())) {
          return BigDecimal.ZERO;
        }
        return BigDecimal.ONE;
      }
      // La valeur n'est pas juste dans certains cas comme avec les plaques de platres (problème d'arrondi)
      BigDecimal reste = pLigne.getQuantiteDocumentOrigineUC().subtract(pLigne.getQuantiteDejaExtraiteUC());
      return reste.setScale(listeUnite.getPrecisionUnite(ligneVente.getIdUniteConditionnementVente()), RoundingMode.HALF_UP);
    }
    return BigDecimal.ZERO;
  }
  
  /**
   * Retourne la quantité restant à réceptionner.
   */
  private BigDecimal retournerQuantiteRestanteAReceptionner(LigneAchat pLigne) {
    LigneAchatArticle ligneAchat = (LigneAchatArticle) pLigne;
    if (pLigne != null) {
      // La valeur n'est pas juste dans certains cas comme avec les plaques de platres (problème d'arrondi)
      BigDecimal reste = ligneAchat.getQuantiteDocumentOrigine().subtract(ligneAchat.getQuantiteDejaExtraite());
      return reste;
    }
    return BigDecimal.ZERO;
  }
  
  private void ajouterMessageStatut(DocumentAchat pDocumentAchat, List<Message> listeMessage) {
    Message message = null;
    switch (pDocumentAchat.getCodeEtat()) {
      case VALIDE:
        message = Message.getMessageNormal(
            pDocumentAchat.getCodeEtat().getLibelle() + " le " + Constantes.convertirDateEnTexte(pDocumentAchat.getDateHomologation()));
        break;
      
      case RECU:
      case RECU_ET_RAPPROCHE_FACTURE:
        message = Message.getMessageNormal(pDocumentAchat.getCodeEtat().getLibelle());
        break;
      
      case FACTURE:
      case COMPTABILISE:
        message = Message.getMessageNormal(
            pDocumentAchat.getCodeEtat().getLibelle() + " le " + Constantes.convertirDateEnTexte(pDocumentAchat.getDateFacturation()));
        break;
      
      default:
        break;
    }
    if (message != null) {
      listeMessage.add(message);
    }
  }
  
  private void ajouterMessageAchatValide(List<Message> listeMessage) {
    String message = null;
    if (!documentAchat.getCodeEtat().equals(EnumCodeEtatDocument.VALIDE)) {
      return;
    }
    
    // Message "Reste à réceptionner"
    LigneAchatArticle ligne = (LigneAchatArticle) ligneAchat;
    BigDecimal quantiteExtraite = calculerQuantiteExtraite();
    BigDecimal quantiteOrigine = calculerQuantiteOrigine();
    if (quantiteExtraite.equals(BigDecimal.ZERO)) {
      message = "Non réceptionné";
      listeMessage.add(Message.getMessageImportant(message));
    }
    else if (quantiteExtraite.compareTo(quantiteOrigine) >= 0) {
      message = "Entièrement réceptionnée";
      listeMessage.add(Message.getMessageNormal(message));
    }
    else {
      message = "Reste " + Constantes.formater(quantiteOrigine.subtract(quantiteExtraite), false) + " "
          + ligne.getPrixAchat().getIdUCA().getCode();
      message += " à réceptionner";
      listeMessage.add(Message.getMessageImportant(message));
      // Message "Réception prévue le"
      if (documentAchat.getDateLivraisonPrevue() != null) {
        message = "Réception prévue le " + Constantes.convertirDateEnTexte(documentAchat.getDateLivraisonPrevue());
        listeMessage.add(Message.getMessageNormal(message));
      }
    }
  }
  
  private void ajouterMessageAchatReceptionne(List<Message> listeMessage) {
    String message = null;
    if (documentAchat.getCodeEtat().equals(EnumCodeEtatDocument.RECU)
        && documentAchat.getCodeEtat().equals(EnumCodeEtatDocument.RECU_ET_RAPPROCHE_FACTURE)) {
      return;
    }
    
    if (documentAchat.isReceptionDefinitive()) {
      message = "Entièrement réceptionné le " + Constantes.convertirDateEnTexte(documentAchat.getDateReception());
      listeMessage.add(Message.getMessageNormal(message));
    }
    else {
      message = "Réceptionné le " + Constantes.convertirDateEnTexte(documentAchat.getDateReception());
      listeMessage.add(Message.getMessageNormal(message));
    }
  }
  
  private void ajouterMessageAchatComptabilise(List<Message> listeMessage) {
    String message = null;
    if (documentAchat.getCodeEtat().equals(EnumCodeEtatDocument.FACTURE)
        && documentAchat.getCodeEtat().equals(EnumCodeEtatDocument.COMPTABILISE)) {
      return;
    }
    
    message = documentAchat.getCodeEtat().getLibelle() + " le " + Constantes.convertirDateEnTexte(documentAchat.getDateFacturation());
    listeMessage.add(Message.getMessageNormal(message));
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la ligne liée.
   */
  public Object getLigne() {
    switch (id.getType()) {
      case LIEN_LIGNE_VENTE:
        return ligneVente;
      case LIEN_LIGNE_ACHAT:
        return ligneAchat;
      default:
        return null;
    }
  }
  
  /**
   * Met à jour la ligne liée.
   */
  public void setLigne(Object pLigneDocument) {
    if (pLigneDocument instanceof LigneVente) {
      ligneVente = (LigneVente) pLigneDocument;
      ligneAchat = null;
    }
    else if (pLigneDocument instanceof LigneAchat) {
      ligneVente = null;
      ligneAchat = (LigneAchat) pLigneDocument;
    }
    else {
      throw new MessageErreurException("Vous devez initialiser un lien avec une ligne d'achat ou une ligne de vente.");
    }
  }
  
  /**
   * Retourne l'identifiant de l'article.
   */
  public IdArticle getIdArticle() {
    switch (id.getType()) {
      case LIEN_LIGNE_VENTE:
        return ligneVente.getIdArticle();
      case LIEN_LIGNE_ACHAT:
        return ligneAchat.getIdArticle();
      default:
        return null;
    }
  }
  
  /**
   * Retourne le document de vente lié.
   */
  public DocumentVente getDocumentVente() {
    return documentVente;
  }
  
  /**
   * Met à jour le document de vente lié.
   */
  public void setDocumentVente(DocumentVente documentVente) {
    this.documentVente = documentVente;
    if (documentVente != null) {
      setIdMagasin(documentVente.getIdMagasin());
    }
  }
  
  /**
   * Retourne le document d'achat lié.
   */
  public DocumentAchat getDocumentAchat() {
    return documentAchat;
  }
  
  /**
   * Met à jour le document d'achat lié.
   */
  public void setDocumentAchat(DocumentAchat pDocumentAchat) {
    documentAchat = pDocumentAchat;
    if (documentAchat != null) {
      setIdMagasin(documentAchat.getIdMagasinSortie());
    }
  }
  
  /**
   * Indique si on a à faire au document de départ, c'est à dire celui à partir duquel on va chercher les liens. Dans ce cas les données
   * origines sont égales aux données lien.
   */
  public boolean isDocumentOrigine() {
    return (id.getCodeEnteteOrigine().equals(id.getCodeEnteteDocument()) && (id.getNumeroOrigine().compareTo(id.getNumeroDocument()) == 0)
        && (id.getSuffixeOrigine().compareTo(id.getSuffixeDocument()) == 0)
        && id.getNumeroFactureOrigine().compareTo(id.getNumeroFacture()) == 0
        && (id.getNumeroLigneOrigine().compareTo(id.getNumeroLigne()) == 0 && (id.getTypeOrigine().equals(id.getType()))));
  }
  
  /**
   * Retourne la liste des unités.
   */
  public ListeUnite getListeUnite() {
    return listeUnite;
  }
  
  /**
   * Met à jour la liste des unités.
   */
  public void setListeUnite(ListeUnite listeUnite) {
    this.listeUnite = listeUnite;
  }
  
  /**
   * Retourne la quantité disponible à la vente.
   */
  public BigDecimal getQuantiteDisponibleVente() {
    return quantiteDisponibleVente;
  }
  
  /**
   * Met à jour la quantité disponible à la vente.
   */
  public void setQuantiteDisponibleVente(BigDecimal quantiteDisponibleVente) {
    this.quantiteDisponibleVente = quantiteDisponibleVente;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin idMagasin) {
    this.idMagasin = idMagasin;
  }
  
}
