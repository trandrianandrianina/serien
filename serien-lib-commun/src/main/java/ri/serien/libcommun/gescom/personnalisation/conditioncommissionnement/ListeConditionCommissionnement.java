/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de CategorieClient.
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de CategorieClient. Elle contient
 * toutes les méthodes oeuvrant sur la liste tandis que la classe CategorieClient contient les méthodes oeuvrant sur une
 * seule
 * CategorieClient.
 */
public class ListeConditionCommissionnement
    extends ListeClasseMetier<IdConditionCommissionnement, ConditionCommissionnement, ListeConditionCommissionnement> {
  /**
   * Constructeur.
   */
  public ListeConditionCommissionnement() {
  }
  
  /**
   * Charge la liste de CategorieClient suivant une liste d'IdCategorieClient
   */
  @Override
  public ListeConditionCommissionnement charger(IdSession pIdSession, List<IdConditionCommissionnement> pListeId) {
    return null;
  }
  
  /**
   * Charger le liste de CatégorieClient suivant l'établissement.
   */
  @Override
  public ListeConditionCommissionnement charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CriteresConditionCommissionnement criteres = new CriteresConditionCommissionnement();
    criteres.setTypeRecherche(CriteresConditionCommissionnement.RECHERCHE_COMPTOIR);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeConditionCommissionnement(pIdSession, criteres);
  }
  
}
