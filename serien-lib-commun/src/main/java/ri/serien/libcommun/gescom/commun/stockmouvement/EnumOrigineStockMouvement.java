/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockmouvement;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Origine d'un mouvement de stock
 * Persisté dans le champ H1ORI de PGVMHISM.
 */
public enum EnumOrigineStockMouvement {
  ACHAT('A', "Achat"),
  VENTE('V', "Vente"),
  STOCK('S', "Bordereau de stock"),
  COMPOSITION('C', "Bordereau de composition"),
  GENERATION('*', "Inventaire généré");
  
  private final Character type;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrigineStockMouvement(Character pType, String pLibelle) {
    type = pType;
    libelle = pLibelle;
  }
  
  /**
   * Le type sous lequel la valeur est persistée en base de données.
   */
  public Character getType() {
    return type;
  }
  
  /**
   * Le libellé associé au type.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le type associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return type.toString();
  }
  
  /**
   * Retourner l'objet enum par son type.
   */
  static public EnumOrigineStockMouvement valueOfByCode(Character pType) {
    for (EnumOrigineStockMouvement value : values()) {
      if (pType.equals(value.getType())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de mouvement de stock est inconnu : " + pType);
  }
  
}
