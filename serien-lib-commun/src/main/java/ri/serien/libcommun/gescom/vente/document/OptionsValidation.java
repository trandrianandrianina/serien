/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.io.Serializable;

import ri.serien.libcommun.gescom.vente.alertedocument.AlertesDocument;

public class OptionsValidation implements Serializable {
  // Constantes
  public static final int NOMBRE_OPTIONS = 20;
  public static final int LONGUEUR_CODE = 3;
  public static final int LONGUEUR_LIBELLE = 36;
  
  public static final String OPTION_DEVIS = "DEV";
  public static final String OPTION_BON_LIVRAISON = "EXP";
  public static final String OPTION_FACTURE = "FAC";
  public static final String OPTION_COMMANDE = "VAL";
  public static final String OPTION_ATTENTE = "ATT";
  public static final String OPTION_ANNULATION = "ANN";
  
  // Variables
  private String[] listeCodes = new String[NOMBRE_OPTIONS];
  private String[] listeLibelles = new String[NOMBRE_OPTIONS];
  private String codePropose = "";
  private String libellePropose = "";
  private AlertesDocument alertesDocument = new AlertesDocument();
  
  /**
   * Remplit un tableau à partir d'une chaine.
   */
  private void decouperChaine(String chaine, String[] tableau, int longueurValeur) {
    // Calcul de la longueur de la sous chaine
    int lg = chaine.length() / longueurValeur;
    // On découpe la chaine
    for (int i = 0; i < lg; i++) {
      int posd = i * longueurValeur;
      int posf = posd + longueurValeur;
      if (posf < chaine.length()) {
        tableau[i] = chaine.substring(posd, posf);
      }
      else {
        tableau[i] = chaine.substring(posd);
      }
    }
  }
  
  // -- Accesseurs
  
  public void setListeCodes(String chaineListe) {
    decouperChaine(chaineListe, listeCodes, LONGUEUR_CODE);
  }
  
  public String[] getListeCodes() {
    return listeCodes;
  }
  
  public void setListeLibelles(String chaineListe) {
    decouperChaine(chaineListe, listeLibelles, LONGUEUR_LIBELLE);
  }
  
  public String[] getListeLibelles() {
    return listeLibelles;
  }
  
  public String getCodePropose() {
    return codePropose;
  }
  
  public void setCodePropose(String codePropose) {
    this.codePropose = codePropose;
  }
  
  public String getLibellePropose() {
    return libellePropose;
  }
  
  public void setLibellePropose(String libellePropose) {
    this.libellePropose = libellePropose;
  }
  
  public AlertesDocument getAlertesDocument() {
    return alertesDocument;
  }
  
  public void setAlertesDocument(AlertesDocument alertesDocument) {
    this.alertesDocument = alertesDocument;
  }
}
