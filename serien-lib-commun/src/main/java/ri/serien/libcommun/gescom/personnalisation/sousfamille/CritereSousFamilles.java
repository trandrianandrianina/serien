/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.sousfamille;

import ri.serien.libcommun.commun.recherche.CritereAvecEtablissement;

public class CritereSousFamilles extends CritereAvecEtablissement {
  
  // Variables
  
  private boolean libelleComplet = false;
  
  private boolean avecAnnule = false;
  
  /**
   * Retourner si les groupes annulés sont inclus.
   */
  public boolean isAvecAnnule() {
    return avecAnnule;
  }
  
  /**
   * Indiquer si on souhaite ou non inclure les groupes annulés.
   * @param pAvecAnnule true=inclure les groupes annulés, false est la valeur par défaut.
   */
  public void setAvecAnnule(boolean pAvecAnnule) {
    avecAnnule = pAvecAnnule;
  }
  
  /**
   * Retourner si le libellé est complet
   */
  public boolean isLibelleComplet() {
    return libelleComplet;
  }
  
  /**
   * Indiquer si on souhaite ou non inclure le libellé complet.
   * @param pLibelleComplet true=inclure le libellé complet, false est la valeur par défaut.
   */
  public void setLibelleComplet(boolean pLibelleComplet) {
    pLibelleComplet = libelleComplet;
  }
  
}
