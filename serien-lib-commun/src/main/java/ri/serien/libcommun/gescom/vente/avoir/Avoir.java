/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.avoir;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;

public class Avoir extends AbstractClasseMetier<IdAvoir> {
  // Variables
  private Date dateCreation = null;
  private IdDocumentVente idDocumentRegle = null;
  private BigDecimal suffixeReglement = null;
  private BigDecimal montantAvoirInitial = null;
  private BigDecimal montantRegleParAvoir = null;
  
  /**
   * Constructeur.
   */
  public Avoir(IdAvoir pIdAvoir) {
    super(pIdAvoir);
  }
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  // -- Méthodes publiques
  /**
   * Renvoit le montant consomable sur l'avoir, c'est à dire le montant d'origine moins le montant déjà consommé sur cet avoir
   */
  public BigDecimal getMontantRestant() {
    return montantAvoirInitial.subtract(montantRegleParAvoir);
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public Avoir clone() {
    Avoir o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (Avoir) super.clone();
      o.setId(id);
      o.setDateCreation(dateCreation);
      o.setIdDocumentRegle(idDocumentRegle);
      o.setSuffixeReglement(suffixeReglement);
      o.setMontantAvoirInitial(montantAvoirInitial);
      o.setMontantRegleParAvoir(montantRegleParAvoir);
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Accesseurs
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public IdDocumentVente getIdDocumentRegle() {
    return idDocumentRegle;
  }
  
  public void setIdDocumentRegle(IdDocumentVente documentRegle) {
    this.idDocumentRegle = documentRegle;
  }
  
  public BigDecimal getSuffixeReglement() {
    return suffixeReglement;
  }
  
  public void setSuffixeReglement(BigDecimal suffixeReglement) {
    this.suffixeReglement = suffixeReglement;
  }
  
  public BigDecimal getMontantAvoirInitial() {
    return montantAvoirInitial;
  }
  
  public void setMontantAvoirInitial(BigDecimal pMontantAvoirInitial) {
    montantAvoirInitial = pMontantAvoirInitial;
    if (montantAvoirInitial != null) {
      montantAvoirInitial = montantAvoirInitial.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
  }
  
  public BigDecimal getMontantRegleParAvoir() {
    return montantRegleParAvoir;
  }
  
  public void setMontantRegleParAvoir(BigDecimal montantRegleParAvoir) {
    this.montantRegleParAvoir = montantRegleParAvoir;
  }
  
}
