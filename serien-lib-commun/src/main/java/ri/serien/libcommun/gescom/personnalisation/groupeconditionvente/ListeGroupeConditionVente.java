/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.groupeconditionvente;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de GroupeConditionVente.
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de GroupeConditionVente. Elle contient
 * toutes les méthodes oeuvrant sur la liste tandis que la classe GroupeConditionVente contient les méthodes oeuvrant sur
 * une seule GroupeConditionVente.
 */
public class ListeGroupeConditionVente extends ListeClasseMetier<IdGroupeConditionVente, GroupeConditionVente, ListeGroupeConditionVente> {
  /**
   * Constructeur.
   */
  public ListeGroupeConditionVente() {
  }
  
  /**
   * Charge la liste de ConditionDeVente suivant une liste d'IdConditionVente
   */
  @Override
  public ListeGroupeConditionVente charger(IdSession pIdSession, List<IdGroupeConditionVente> pListeId) {
    return null;
  }
  
  /**
   * Charger le liste de ConditionDeVente suivant l'établissement.
   */
  @Override
  public ListeGroupeConditionVente charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CritereGroupeConditionVente criteres = new CritereGroupeConditionVente();
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeGroupeConditionVente(pIdSession, criteres);
  }
  
}
