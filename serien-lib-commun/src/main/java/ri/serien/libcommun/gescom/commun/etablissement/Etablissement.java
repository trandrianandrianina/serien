/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.etablissement;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.EnumTypeCumulConditionQuantitative;
import ri.serien.libcommun.gescom.commun.EnumTypeUniteConditionQuantitative;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.IdGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Etablissement.
 * 
 * Les paramètres des établissements sont stockés dans la personnalisation DG "Description générale". Chaque établissement a sa propre
 * DG contenant sa configuration. Il existe également une DG "blanche" qui contient le paramètres par défaut d'un établissement.
 */
public class Etablissement extends AbstractClasseMetier<IdEtablissement> {
  // Variables
  private IdMagasin idMagasinParDefaut = null;
  private String raisonSociale = "";
  private String complementNom = "";
  private Date moisEnCours = null;
  private Date moisEnCoursMax = null;
  private BigDecimal tauxTVA1 = BigDecimal.ZERO;
  private BigDecimal tauxTVA2 = BigDecimal.ZERO;
  private BigDecimal tauxTVA3 = BigDecimal.ZERO;
  private BigDecimal tauxTVA4 = BigDecimal.ZERO;
  private BigDecimal tauxTVA5 = BigDecimal.ZERO;
  private BigDecimal tauxTVA6 = BigDecimal.ZERO;
  private Boolean moisEnCoursChange = false;
  private BigDecimal montantMaximumEspeces = BigDecimal.ZERO;
  private String codeDevise = null;
  private Integer arrondi = null;
  private Date dateExerciceDemarrageDebut = null;
  private Date dateExerciceDemarrageFin = null;
  private IdGroupeConditionVente idGroupeConditionVenteGenerale = null;
  private IdGroupeConditionVente idGroupeConditionVentePromotionnelle = null;
  private EnumTypeUniteConditionQuantitative typeUniteConditionQuantitative = null;
  private EnumTypeCumulConditionQuantitative typeCumulConditionQuantitative = null;
  
  /**
   * Constructeur.
   */
  public Etablissement(IdEtablissement pIdEtablissement) {
    super(pIdEtablissement);
  }
  
  @Override
  public String getTexte() {
    return raisonSociale;
  }
  
  // -- Méthodes publiques
  
  /**
   * Charger un établissement par son identifiant.
   */
  public static Etablissement charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    Etablissement etablissement = ManagerServiceParametre.chargerEtablissement(pIdSession, pIdEtablissement);
    if (etablissement == null) {
      throw new MessageErreurException("Impossible de charger l'établissement " + pIdEtablissement);
    }
    return etablissement;
  }
  
  /**
   * Contrôler si la date passée en paramètre est dans le mois en cours.
   */
  public boolean isDateDansPeriodeEnCours(Date pDate) {
    if (pDate == null || pDate.before(moisEnCours) || pDate.after(getMoisEnCoursMax())) {
      return false;
    }
    return true;
  }
  
  /**
   * Fournir une date de traitement valide par rapport à l'exercie en cours.
   * 
   * La date de traitement est la date du jour ou une date calculée si la date de l'exercice en cours est dépassée
   * (date exercice + 2 mois).
   */
  public Date getDateTraitement() {
    // Initialiser la date de traitement avec la date du jour et heure à minuit (pour les comparaisons de dates)
    Calendar cal = new GregorianCalendar(TimeZone.getDefault());
    cal.setTime(new Date());
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    
    Date dateTraitement = cal.getTime();
    
    // Prendre le mois en cours si la date du jour est antérieure
    if (dateTraitement.before(moisEnCours)) {
      return moisEnCours;
    }
    
    // Prendre le mois maximum si la date du jour est postérieure
    if (dateTraitement.after(getMoisEnCoursMax())) {
      return getMoisEnCoursMax();
    }
    
    return dateTraitement;
  }
  
  // -- Méthodes privées
  
  /**
   * Iniatalise un objetCalendar à partir d'un objet Date.
   */
  private Calendar initialiserCalendarAvecDate(Date pDate) {
    Calendar date = Calendar.getInstance();
    date.setTime(pDate);
    return date;
  }
  
  /**
   * Calcule la date maximum acceptable par rapport au mois encours.
   * dateMax = dateMoisEnCours(1° jour du mois) + 2 mois
   */
  private Date calculerMoisEncoursMaximumAcceptable() {
    Calendar dateMaxAcceptable = initialiserCalendarAvecDate(getMoisEnCours());
    dateMaxAcceptable.add(Calendar.MONTH, 2);
    dateMaxAcceptable.add(Calendar.DAY_OF_MONTH, -1);
    return dateMaxAcceptable.getTime();
  }
  
  // -- Accesseurs
  
  /**
   * Retourne l'identifiant du magasin par défaut.
   * 
   * @return
   */
  public IdMagasin getIdMagasinParDefaut() {
    return idMagasinParDefaut;
  }
  
  /**
   * Initialise l'identifiant du magasin par défaut.
   * 
   * @param pMagasin
   */
  public void setIdMagasinParDefaut(IdMagasin pMagasin) {
    this.idMagasinParDefaut = pMagasin;
  }
  
  /**
   * Retourne la raison sociale.
   * 
   * @return
   */
  public String getRaisonSociale() {
    return raisonSociale;
  }
  
  /**
   * Initialise la raison sociale.
   * 
   * @param pRaisonSociale
   */
  public void setRaisonSociale(String pRaisonSociale) {
    this.raisonSociale = pRaisonSociale;
  }
  
  /**
   * Retourne le complément de nom.
   * 
   * @return
   */
  public String getComplementNom() {
    return complementNom;
  }
  
  /**
   * Initialise le complément de nom.
   * 
   * @param pComplementNom
   */
  public void setComplementNom(String pComplementNom) {
    this.complementNom = pComplementNom;
  }
  
  /**
   * Retourne le mois en cours.
   * 
   * @return
   */
  public Date getMoisEnCours() {
    return moisEnCours;
  }
  
  /**
   * Initialise le mois en cours.
   * 
   * @param pMoisEnCours
   */
  public void setMoisEnCours(Date pMoisEnCours) {
    this.moisEnCours = pMoisEnCours;
    moisEnCoursChange = true;
  }
  
  /**
   * Initialise le mois en cours.
   * 
   * @param pMoisEnCours
   */
  public void setMoisEnCours(String pMoisEnCours) {
    this.moisEnCours = Constantes.convertirMoisAnneeEnDate(pMoisEnCours);
  }
  
  /**
   * Retourne l'exercice en cours max.
   * 
   * @return
   */
  public Date getMoisEnCoursMax() {
    if (moisEnCoursChange || moisEnCoursMax == null) {
      moisEnCoursMax = calculerMoisEncoursMaximumAcceptable();
      moisEnCoursChange = false;
    }
    return moisEnCoursMax;
  }
  
  /**
   * Initialise l'exercice en cours max.
   * 
   * @param pMoisEnCoursMax
   */
  public void setMoisEnCoursMax(Date pMoisEnCoursMax) {
    this.moisEnCoursMax = pMoisEnCoursMax;
  }
  
  /**
   * Retourne le taux de TVA 1.
   * 
   * @return
   */
  public BigDecimal getTauxTVA1() {
    return tauxTVA1;
  }
  
  /**
   * Initialise le taux de TVA 1.
   * 
   * @param pTauxTVA1
   */
  public void setTauxTVA1(BigDecimal pTauxTVA1) {
    this.tauxTVA1 = pTauxTVA1;
  }
  
  /**
   * Retourne le taux de TVA 2.
   * 
   * @return
   */
  public BigDecimal getTauxTVA2() {
    return tauxTVA2;
  }
  
  /**
   * Retourne le taux de TVA 2.
   * 
   * @param pTauxTVA
   */
  public void setTauxTVA2(BigDecimal pTauxTVA) {
    this.tauxTVA2 = pTauxTVA;
  }
  
  /**
   * Retourne le taux de TVA 3.
   * 
   * @return
   */
  public BigDecimal getTauxTVA3() {
    return tauxTVA3;
  }
  
  /**
   * Initialise le taux de TVA 3.
   * 
   * @param pTauxTVA
   */
  public void setTauxTVA3(BigDecimal pTauxTVA) {
    this.tauxTVA3 = pTauxTVA;
  }
  
  /**
   * Retourne le taux de TVA 4.
   * 
   * @return
   */
  public BigDecimal getTauxTVA4() {
    return tauxTVA4;
  }
  
  /**
   * Initialise le taux de TVA 4.
   * 
   * @param pTauxTVA
   */
  public void setTauxTVA4(BigDecimal pTauxTVA) {
    this.tauxTVA4 = pTauxTVA;
  }
  
  /**
   * Retourne le taux de TVA 5.
   * 
   * @return
   */
  public BigDecimal getTauxTVA5() {
    return tauxTVA5;
  }
  
  /**
   * Initialise le taux de TVA 5.
   * 
   * @param pTauxTVA
   */
  public void setTauxTVA5(BigDecimal pTauxTVA) {
    this.tauxTVA5 = pTauxTVA;
  }
  
  /**
   * Retourne le taux de TVA 6.
   * 
   * @return
   */
  public BigDecimal getTauxTVA6() {
    return tauxTVA6;
  }
  
  /**
   * Retourne le taux de TVA 6.
   * 
   * @param pTauxTVA
   */
  public void setTauxTVA6(BigDecimal pTauxTVA) {
    this.tauxTVA6 = pTauxTVA;
  }
  
  /**
   * Retourne le taux de TVA correspondant au code TVA passé en paramètre.
   */
  public BigDecimal getTauxTVA(int pCodeTVA) {
    BigDecimal taux = BigDecimal.ZERO;
    switch (pCodeTVA) {
      case 1:
        taux = getTauxTVA1();
        break;
      case 2:
        taux = getTauxTVA2();
        break;
      case 3:
        taux = getTauxTVA3();
        break;
      case 4:
        taux = getTauxTVA4();
        break;
      case 5:
        taux = getTauxTVA5();
        break;
      case 6:
        taux = getTauxTVA6();
        break;
      default:
        taux = getTauxTVA1();
        break;
    }
    return taux;
  }
  
  /**
   * Retourne le montant maximal accepté pour un réglement en espèces
   */
  public BigDecimal getMontantMaximumEspeces() {
    return montantMaximumEspeces;
  }
  
  /**
   * Fixe le montant maximal accepté pour un réglement en espèces
   */
  public void setMontantMaximumEspeces(BigDecimal pMontantMaximumEspeces) {
    montantMaximumEspeces = pMontantMaximumEspeces;
  }
  
  /**
   * Retourne le code de la devise.
   * 
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Initialise le code de la devise.
   * 
   * @param pCodeDevise
   */
  public void setCodeDevise(String pCodeDevise) {
    this.codeDevise = pCodeDevise;
  }
  
  /**
   * Retourne l'arrondi.
   * 
   * @return
   */
  public Integer getArrondi() {
    return arrondi;
  }
  
  /**
   * Initialise l'arrondi.
   * 
   * @param pArrondi
   */
  public void setArrondi(Integer pArrondi) {
    this.arrondi = pArrondi;
  }
  
  /**
   * Retourne l'exercice de démarrage de début.
   * 
   * @return
   */
  public Date getDateExerciceDemarrageDebut() {
    return dateExerciceDemarrageDebut;
  }
  
  /**
   * Initialise la date de démarrage de début.
   * 
   * @param pDateExerciceDemarrageDebut
   */
  public void setDateExerciceDemarrageDebut(Date pDateExerciceDemarrageDebut) {
    this.dateExerciceDemarrageDebut = pDateExerciceDemarrageDebut;
  }
  
  /**
   * Retourne la date de démarrage de fin.
   * 
   * @return
   */
  public Date getDateExerciceDemarrageFin() {
    return dateExerciceDemarrageFin;
  }
  
  /**
   * Initialise la date de démarrage de fin.
   * 
   * @param pDateExerciceDemarrageFin
   */
  public void setDateExerciceDemarrageFin(Date pDateExerciceDemarrageFin) {
    this.dateExerciceDemarrageFin = pDateExerciceDemarrageFin;
  }
  
  /**
   * Initialise l'identifiant du groupe de la condition de ventes générale.
   * 
   * @return
   */
  public IdGroupeConditionVente getIdGroupeConditionVenteGenerale() {
    return idGroupeConditionVenteGenerale;
  }
  
  /**
   * Retourne l'identifiant du groupe de la condition de ventes générale.
   * 
   * @param pIdGroupeConditionVenteGenerale
   */
  public void setIdGroupeConditionVenteGenerale(IdGroupeConditionVente pIdGroupeConditionVenteGenerale) {
    this.idGroupeConditionVenteGenerale = pIdGroupeConditionVenteGenerale;
  }
  
  /**
   * Initialise l'identifiant du groupe de la condition de ventes promotionnelle.
   * 
   * @return
   */
  public IdGroupeConditionVente getIdGroupeConditionVentePromotionnelle() {
    return idGroupeConditionVentePromotionnelle;
  }
  
  /**
   * Retourne l'identifiant du groupe de la condition de ventes promotionnelle.
   * 
   * @param pIdGroupeConditionVentePromotionnelle
   */
  public void setIdGroupeConditionVentePromotionnelle(IdGroupeConditionVente pIdGroupeConditionVentePromotionnelle) {
    this.idGroupeConditionVentePromotionnelle = pIdGroupeConditionVentePromotionnelle;
  }
  
  /**
   * Retourne le type d'unité pour les conditions quantitatives.
   * 
   * @return
   */
  public EnumTypeUniteConditionQuantitative getTypeUniteConditionQuantitative() {
    return typeUniteConditionQuantitative;
  }
  
  /**
   * Initialise le type d'unité pour les conditions quantitatives.
   * 
   * @param pTypeUniteConditionQuantitative
   */
  public void setTypeUniteConditionQuantitative(EnumTypeUniteConditionQuantitative pTypeUniteConditionQuantitative) {
    this.typeUniteConditionQuantitative = pTypeUniteConditionQuantitative;
  }
  
  /**
   * Retourne le type du cumul pour les conditions quantitatives.
   * 
   * @return
   */
  public EnumTypeCumulConditionQuantitative getTypeCumulConditionQuantitative() {
    return typeCumulConditionQuantitative;
  }
  
  /**
   * Initialise le type du cumul pour les conditions quantitatives.
   * 
   * @param pTypeCumulConditionQuantitative
   */
  public void setTypeCumulConditionQuantitative(EnumTypeCumulConditionQuantitative pTypeCumulConditionQuantitative) {
    this.typeCumulConditionQuantitative = pTypeCumulConditionQuantitative;
  }
}
