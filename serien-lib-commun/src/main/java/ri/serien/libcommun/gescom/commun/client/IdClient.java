/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un client.
 *
 * L'identifiant est composé du code établissement, du numéro client et du suffixe client.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation. *
 */
public class IdClient extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_NUMERO = 6;
  public static final int LONGUEUR_SUFFIXE = 3;
  public static final int LONGUEUR_INDICATIF = LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_NUMERO + LONGUEUR_SUFFIXE;
  
  public static final int INDICATIF_ETB_NUM_SUF = 0;
  public static final int INDICATIF_NUM_SUF = 1;
  public static final int INDICATIF_NUM = 2;
  
  // Variables
  private final Integer numero;
  private final Integer suffixe;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdClient(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, Integer pNumero, Integer pSuffixe) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    numero = controlerNumero(pNumero);
    suffixe = controlerSuffixe(pSuffixe);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   */
  private IdClient(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    numero = 0;
    suffixe = 0;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   * 
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pNumero Numéro du client.
   * @param pSuffixe Suffixe d elivraison du client.
   * @return Identifiant du client.
   */
  public static IdClient getInstance(IdEtablissement pIdEtablissement, Integer pNumero, Integer pSuffixe) {
    return new IdClient(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pNumero, pSuffixe);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée et en chaînes de caractères.
   * 
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pNumero Numéro du client.
   * @param pSuffixe Suffixe d elivraison du client.
   * @return Identifiant du client.
   */
  public static IdClient getInstance(IdEtablissement pIdEtablissement, String pNumeroClient, String pSuffixeClient) {
    // Contrôler les paramètres
    if (pNumeroClient == null) {
      throw new MessageErreurException("Impossible de construire l'identifiant du client car son numéro est invalide.");
    }
    if (pSuffixeClient == null) {
      throw new MessageErreurException("Impossible de construire l'identifiant du client car son suffixe de livraison est invalide.");
    }
    
    // Convertir le numéro
    Integer numeroClient = 0;
    try {
      numeroClient = Integer.parseInt(pNumeroClient);
    }
    catch (NumberFormatException e) {
      throw new MessageErreurException("Impossible de construire l'identifiant du client car son numéro est invalide : " + pNumeroClient);
    }
    
    // Convertir le suffixe (un suffixe vide équivaut à 0)
    Integer suffixeClient = 0;
    if (!pSuffixeClient.trim().isEmpty()) {
      try {
        suffixeClient = Integer.parseInt(pSuffixeClient);
      }
      catch (NumberFormatException e) {
        throw new MessageErreurException(
            "Impossible de construire l'identifiant du client car son suffixe de livraison est invalide :" + pSuffixeClient);
      }
    }
    
    // Construire l'identifiant du client
    return new IdClient(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, numeroClient, suffixeClient);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdClient getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdClient(pIdEtablissement);
  }
  
  /**
   * Créer un identifiant à partir de l'indicatif d'un client.
   */
  public static IdClient getInstanceParIndicatif(String pIndicatif) {
    return new IdClient(EnumEtatObjetMetier.MODIFIE, extraireEtablissementDeIndicatif(pIndicatif), extraireNumeroDeIndicatif(pIndicatif),
        extraireSuffixeDeIndicatif(pIndicatif));
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdClient controlerId(IdClient pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du client est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du client n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + numero.hashCode();
    cle = 37 * cle + suffixe.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdClient)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de client.");
    }
    IdClient id = (IdClient) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && numero.equals(id.numero) && suffixe.equals(id.suffixe);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdClient)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdClient id = (IdClient) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numero.compareTo(id.numero);
    if (comparaison != 0) {
      return comparaison;
    }
    return suffixe.compareTo(id.suffixe);
  }
  
  @Override
  public String getTexte() {
    return "" + numero + SEPARATEUR_ID + suffixe;
  }
  
  /**
   * Détecte si la chaine correspond à un numéro client (sans le suffixe).
   * 
   * @param pCode Le code à contrôler.
   * @return
   */
  public static boolean isNumeroClient(String pCodeClient) {
    pCodeClient = Constantes.normerTexte(pCodeClient);
    
    // Contrôle la longueur de la chaine
    if (pCodeClient.length() != LONGUEUR_NUMERO) {
      return false;
    }
    
    // Contrôle si la chaine ne contient que des chiffres
    char[] listeCaractere = pCodeClient.toCharArray();
    for (char caractere : listeCaractere) {
      if (!Character.isDigit(caractere)) {
        return false;
      }
    }
    return true;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du numéro client.
   * Le numéro de client doit être supérieur à zéro et doit comportr au maximum 6 chiffres (entre 1 et 999 999).
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de client n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du client est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du client est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du suffixe client.
   * Le suffixe client doit être supérieur ou égal à zéro et doit comportr au maximum 3 chiffres (entre 0 et 999).
   */
  private static Integer controlerSuffixe(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de client n'est pas renseigné.");
    }
    if (pValeur < 0) {
      throw new MessageErreurException("Le suffixe du client est inférieur à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_SUFFIXE)) {
      throw new MessageErreurException("Le suffixe du client est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler l'indicatif d'un client.
   */
  private static String controlerIndicatif(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("L'indicatif du client n'est pas renseigné.");
    }
    if (pValeur.length() < LONGUEUR_INDICATIF) {
      throw new MessageErreurException("L'indicatif du document n'est pas correct car il est inférieur à la longueur attendu.");
    }
    return pValeur.trim();
  }
  
  /**
   * Extraire le code établissement de l'indicatif du client.
   */
  private static IdEtablissement extraireEtablissementDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    int offsetDeb = 0;
    int offsetFin = offsetDeb + LONGUEUR_CODE_ETABLISSEMENT;
    return IdEtablissement.getInstance(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  /**
   * Extraire le numéro de l'indicatif du client.
   */
  private static Integer extraireNumeroDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    int offsetDeb = LONGUEUR_CODE_ETABLISSEMENT;
    int offsetFin = offsetDeb + LONGUEUR_NUMERO;
    return Integer.valueOf(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  /**
   * Extraire le suffixe de l'indicatif du client.
   */
  private static Integer extraireSuffixeDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    int offsetDeb = LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_NUMERO;
    int offsetFin = offsetDeb + LONGUEUR_SUFFIXE;
    return Integer.valueOf(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  // -- Accesseurs
  
  /**
   * Numéro du client.
   * Le numéro de client est compris entre 1 et 999 999. Le numéro peut être égal à 0 pour un identifiant en cours de création.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Suffixe du client.
   * Le suffixe client est compris entre 0 et 999.
   */
  public Integer getSuffixe() {
    return suffixe;
  }
  
  /**
   * Indicatif du client en fonction de l'indicatif souhaité.
   */
  public String getIndicatif(int pTypeIndicatif) {
    switch (pTypeIndicatif) {
      case INDICATIF_ETB_NUM_SUF:
        return String.format("%" + LONGUEUR_CODE_ETABLISSEMENT + "s%0" + LONGUEUR_NUMERO + "d%0" + LONGUEUR_SUFFIXE + "d",
            getCodeEtablissement(), numero, suffixe);
      case INDICATIF_NUM_SUF:
        return String.format("%0" + LONGUEUR_NUMERO + "d%0" + LONGUEUR_SUFFIXE + "d", numero, suffixe);
      case INDICATIF_NUM:
        return String.format("%0" + LONGUEUR_NUMERO + "d", numero);
    }
    return "";
  }
}
