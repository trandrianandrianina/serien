/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.origine;

/**
 * Liste des origines possibles de la colonne tarif utilisée pour le calcul.
 * Les origines sont classées par ordre de priorité (du plus prioritaire vers le moins prioritaire).
 */
public enum EnumOrigineColonneTarif {
  LIGNE_VENTE("Ligne de vente"),
  CHANTIER("Chantier"),
  CONDITION_VENTE("Condition de vente"),
  HERITE_CNVS_PRECEDENTES("Héritée des CNV précédentes"),
  DOCUMENT_VENTE("Document de vente"),
  CLIENT("Client"),
  PS105("PS105"),
  COLONNE_UN_PAR_DEFAUT("Colonne 1 par défaut"),
  PRIX_NET("Prix net");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrigineColonneTarif(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
