/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Cette classe contient les données minimales nécessaires à l'affichage d'une ligne de de ventes dans une liste.
 */
public class LigneVenteBase extends AbstractClasseMetier<IdLigneVente> implements Comparable<LigneVenteBase> {
  // Constantes
  private static final int TAILLE_MAX_LIBELLE = 120;
  public static final int PRECISION_MAX_QUANTITE = 3;
  public static final int NUMERO_LIGNE_POUR_REMISE_SPECIALE = 9699;
  
  public static final String TEXTE_ORIGINE_PRIX_NON_DEFINI = "";
  public static final String TEXTE_ORIGINE_PRIX_COLONNE = "Col.";
  public static final String TEXTE_ORIGINE_PRIX_NEGOCIATION = "N\u00e9goci\u00e9";
  
  // Variables
  private EnumTypeLigneVente typeLigne = null;
  private Integer numeroFacture = null;
  private IdArticle idArticle = null;
  private String libelle = null;
  private String infoComplementaire = null;
  private IdUnite idUniteVente = null;
  private Integer nombreDecimaleUV = null;
  private BigDecimal nombreUVParUCV = null;
  private IdUnite idUniteConditionnementVente = null;
  private Integer nombreDecoupe = null;
  
  // Variables concernant les regroupements
  private Character codeRegroupement = Character.valueOf(' ');
  private boolean debutRegroupement = false;
  private boolean finRegroupement = false;
  
  private Character codeExtraction = Character.valueOf(' '); // A: annuler, M:modifier, ... code traitement de la ligne
  
  // Variables issues de prix de vente java (mais de la ligne de vente stockée en base)
  private BigDecimal quantiteUV = null;
  private BigDecimal prixBaseHT = null;
  private EnumProvenancePrixNet provenancePrixNet = null;
  private BigDecimal prixNetSaisiHT = null;
  private BigDecimal prixNetCalculeHT = null;
  private BigPercentage tauxRemise1 = null;
  private BigPercentage tauxRemise2 = null;
  private BigPercentage tauxRemise3 = null;
  private BigPercentage tauxRemise4 = null;
  private BigPercentage tauxRemise5 = null;
  private BigPercentage tauxRemise6 = null;
  private BigDecimal montantHT = null;
  private EnumOriginePrixVente originePrixVente = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public LigneVenteBase(IdLigneVente pIdLigneVente) {
    super(pIdLigneVente);
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdLigneVente controlerId(LigneVenteBase pLigneVente, boolean pVerifierExistance) {
    if (pLigneVente == null) {
      throw new MessageErreurException("La ligne de ventes est invalide.");
    }
    IdLigneVente idLigneVente = pLigneVente.getId();
    if (idLigneVente == null) {
      throw new MessageErreurException("L'identifiant de la ligne de ventes est invalide.");
    }
    if (pVerifierExistance && !idLigneVente.isExistant()) {
      throw new MessageErreurException("L'identifiant de la ligne de ventes n'existe pas dans la base de données : " + idLigneVente);
    }
    return idLigneVente;
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    // Retourner le texte de l'identifiant à défaut de mieux car je ne vois pas quel libellé pertinent on a pour l'instant.
    return id.getTexte();
  }
  
  /**
   * Indique si les données de la ligne sont chargées.
   */
  @Override
  public boolean isCharge() {
    return (typeLigne != null);
  }
  
  /**
   * Permet de comparer avec une autre ligne article.
   */
  @Override
  public int compareTo(LigneVenteBase pLigneArticle) {
    return id.getNumeroLigne().compareTo(pLigneArticle.getId().getNumeroLigne());
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public LigneVenteBase clone() {
    LigneVenteBase ligneVenteBase = null;
    try {
      // Récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      ligneVenteBase = (LigneVenteBase) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return ligneVenteBase;
  }
  
  /**
   * Retourne s'il s'agit d'une ligne de remise spéciale (les lignes au dela de cette valeur).
   * Au dela de cette valeur, il s'agit de lignes consacrées aux remises et autres.
   */
  public boolean isLigneRemiseSpeciale() {
    return id.getNumeroLigne().compareTo(NUMERO_LIGNE_POUR_REMISE_SPECIALE) > 0;
  }
  
  // -- Accesseurs
  
  /**
   * Type de la ligne de ventes (article stocké, article spécial, commentaire, palette, ...)
   * Le type de la ligne est stocké dans le champ L1ERL.
   */
  public EnumTypeLigneVente getTypeLigne() {
    return typeLigne;
  }
  
  /**
   * Modifier le type de la ligne de ventes.
   */
  public void setTypeLigne(EnumTypeLigneVente pTypeLigne) {
    typeLigne = pTypeLigne;
  }
  
  /**
   * Numéro de facture auquel la ligne est liée.
   */
  public Integer getNumeroFacture() {
    return numeroFacture;
  }
  
  /**
   * Modifier le numéro de facture auquel la ligne est liée.
   */
  public void setNumeroFacture(Integer pNumeroFacture) {
    numeroFacture = pNumeroFacture;
  }
  
  /**
   * Indique s'il s'agit d'une ligne de type commentaire.
   */
  public boolean isLigneCommentaire() {
    return typeLigne != null && typeLigne.equals(EnumTypeLigneVente.COMMENTAIRE);
  }
  
  /**
   * Indique s'il s'agit d'une ligne de type commentaire de bon de cour.
   */
  public boolean isLigneBonCour() {
    return isLigneCommentaire() && libelle != null && libelle.startsWith("Bon de cour numéro ");
  }
  
  /**
   * Identifiant de l'article associé à la ligne.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Renseigner l'identifiant de l'article associé à la ligne.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  /**
   * Libellé de la ligne.
   * Le libellé de la ligne contient suivant le cas le libellé de l'article, un commentaire ou toute autre information textuelle saisie
   * sur la ligne et décrivant l'objet de la vente.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé de la ligne.
   * 
   * Nativement, le libellé de l'article n'est pas stocké dans la ligne s'il est identique au libellé article. Le libellé est stocké dans
   * la ligne uniquement s'il a été modifié.
   * Le libellé est tronqué s'il dépasse la taille maximum. Les espaces en debut et fin de libellé sont supprimés.
   */
  public void setLibelle(String pLibelle) {
    if (pLibelle == null) {
      libelle = "";
    }
    else if (pLibelle.length() > TAILLE_MAX_LIBELLE) {
      libelle = pLibelle.substring(0, TAILLE_MAX_LIBELLE).trim();
    }
    else {
      libelle = pLibelle.trim();
    }
  }
  
  /**
   * Complément d'information de la ligne.
   * Cette information est utilisée de diverse manière suivant le contexte :
   * - Initialement utilisé comme complément du libellé 1 (pour ajouter une couleur au libellé de l'article par exemple).
   * - Pour stocker la deuxième quantité dans le cas d'articles en double quantité (poulet à l'unité vendu au kg).
   * - Dans le cas d'une ligne de pied de regroupement, le montant total du regroupement est stocké dans le libellé complémentaire.
   */
  public String getInfoComplementaire() {
    return infoComplementaire;
  }
  
  /**
   * Modifier l'information complémentaire de la ligne.
   */
  public void setInfoComplementaire(String pInfoComplementaire) {
    infoComplementaire = pInfoComplementaire;
  }
  
  /**
   * Identifiant de l'unité de ventes (UV).
   * C'est l'identifiant de l'unité de ventes de l'article au moment où la ligne a été saisie, sauvegardé dans la ligne pour palier aux
   * changements de paramètrage. La précision de l'unité de ventes a également été sauvegardée dans la ligne, voir
   * getNombreDecimaleUV().
   */
  public IdUnite getIdUniteVente() {
    return idUniteVente;
  }
  
  /**
   * Modifier l'identifiant de l'unité de ventes (UV).
   */
  public void setIdUniteVente(IdUnite pIdUnite) {
    idUniteVente = pIdUnite;
  }
  
  /**
   * Nombre de décimales de l'unité de ventes.
   * 
   * C'est la précision de l'unité de ventes de l'article au moment où la ligne a été saisie, sauvegardée dans la ligne pour palier aux
   * changements de paramètrage.
   */
  public Integer getNombreDecimaleUV() {
    return nombreDecimaleUV;
  }
  
  /**
   * Modifier le nombre de décimales de l'unité de ventes (UV).
   * 
   * La quantité en unités de ventes est également modifiée lorsque cette valeur est modifiée. Elle est arrondie suivant la nouvelle
   * précision. En effet, la quantité doit être stockée dans l'objet avec la bonne précision.
   * 
   * Lors de l'ajout d'une ligne article à un document de ventes, la précision utilisée pour l'unité de ventes (UV) est celle stockée
   * dans le champ A1DCV de l'article. Cela permet de modifier la précision de l'unité dans les personnalisations sans impacter les
   * articles précédemment saisis. Le choix est laissé à l'utilisateur lorsqu'il modifie l'unité.
   */
  public void setNombreDecimaleUV(Integer pNombreDecimaleUV) {
    // Mémoriser la nouvelle précision
    nombreDecimaleUV = pNombreDecimaleUV;
    
    // Arrondir la quantité en UV avec la nouvelle précision
    if (pNombreDecimaleUV != null && quantiteUV != null) {
      quantiteUV = quantiteUV.setScale(nombreDecimaleUV, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Retourne true si nombre d'unités de ventes (UV) par unité de conditionnement de ventes (UCV) est défini est différent de 1.
   * On considère que si ce coefficient n'est pas définit ou qu'il a la valeur zéro, cela équivaut à la valeur 1.
   */
  public boolean isDefiniNombreUVParUCV() {
    return nombreUVParUCV != null && nombreUVParUCV.compareTo(BigDecimal.ZERO) != 0 && nombreUVParUCV.compareTo(BigDecimal.ONE) != 0;
  }
  
  /**
   * Nombre d'unités de ventes (UV) par unité de conditionnement de ventes (UCV).
   * C'est le coefficient de conditionnement de l'article au moment où la ligne a été saisie, sauvegardée dans la ligne pour palier aux
   * changements de paramètrage. Si ce coefficient est égale à zéro, on considère qu'il vaut 1.
   */
  public BigDecimal getNombreUVParUCV() {
    return nombreUVParUCV;
  }
  
  /**
   * Modifier le nombre d'unités de ventes (UV) par unité de conditionnement de ventes (UCV).
   */
  public void setNombreUVParUCV(BigDecimal pNombreUVParUCV) {
    nombreUVParUCV = pNombreUVParUCV;
  }
  
  /**
   * Indiquer si l'unité de conditionnement de ventes est la même que l'unité de ventes.
   * On considère également que les unités sont identiques si l'unité de conditionnement de ventes n'est pas renseignée.
   */
  public boolean isUniteVenteIdentique() {
    return idUniteConditionnementVente == null || idUniteConditionnementVente.getCode().isEmpty()
        || Constantes.equals(idUniteConditionnementVente, idUniteVente);
  }
  
  /**
   * Identifiant de l'unité de conditionnement de ventes (UCV).
   * C'est l'identifiant de l'unité de conditionnement de ventes de l'article au moment où la ligne a été saisie, sauvegardé dans la
   * ligne pour palier aux changements de paramètrage. Contrairement à l'unité de ventes, la précision de l'unité de conditionnement de
   * ventes n'a pas été sauvegardée dans la ligne.
   */
  public IdUnite getIdUniteConditionnementVente() {
    return idUniteConditionnementVente;
  }
  
  /**
   * Modifier l'identifiant de l'unité de conditionnement de ventes (' ' si la ligne n'est pas regroupée).
   */
  public void setIdUniteConditionnementVente(IdUnite pIdUnite) {
    idUniteConditionnementVente = pIdUnite;
  }
  
  /**
   * Quantité de la ligne en unités de ventes (UV).
   * 
   * La quantité devant être stockée dans l'objet avec la bonne précision, elle est retournée sans transformation ni arrondi.
   * Les accesseurs en écritures doivent s'assurer que la quantité est toujours stocké avec la bonne précision.
   */
  public BigDecimal getQuantiteUV() {
    return quantiteUV;
  }
  
  /**
   * Modifier la quantité de la ligne en unités de ventes (UV).
   * 
   * La précision du BigDecimal fourni est ignorée. En effet, la quantité fournie est arrondie avec la précision de l'unité de ventes
   * (UV) stockée dans la ligne de ventes. Si la précision n'est pas définie, la méthode retourne une exception.
   */
  public void setQuantiteUV(BigDecimal pQuantiteUV) {
    // Vérifier que la précision est renseignée
    if (nombreDecimaleUV == null) {
      throw new MessageErreurException("La précision de l'unité de ventes de la ligne n'est pas renseignée.");
    }
    
    // Modifier la quantité
    quantiteUV = pQuantiteUV.setScale(nombreDecimaleUV, RoundingMode.HALF_UP);
    
    // Modifier le montant HT de la ligne (mais sans relancer un calcul de prix)
    if (getPrixNetHT() != null) {
      montantHT = ArrondiPrix.appliquer(quantiteUV.multiply(getPrixNetHT()), Constantes.DEUX_DECIMALES);
    }
  }
  
  /**
   * Quantité de la ligne en unité de conditionnement de ventes (UCV) avec un nombre de décimales donné.
   * 
   * La quantité en unités de conditionnement de ventes n'est pas stockée dans la ligne de ventes. Il faut la recalculer à partir de la
   * quantité en unités de ventes (UV).
   * 
   * Par ailleurs, la précision de l'unité de conditionnement de ventes (UCV) n'est pas non plus stockée dans la ligne.
   * 
   */
  public BigDecimal getQuantiteUCV() {
    // Retourner le nombre de découpes pour un article découpable
    if (isArticleDecoupable()) {
      return new BigDecimal(nombreDecoupe);
    }
    
    // Initialiser avec la quantité en unités de ventes (si la quantité est null, c'est que les données ne sont pas
    // chargées)
    BigDecimal quantiteUCV = getQuantiteUV();
    if (quantiteUCV == null) {
      return null;
    }
    
    // Convertir la quantité si le coefficient de conditionnement est renseigné
    if (nombreUVParUCV != null && nombreUVParUCV.compareTo(BigDecimal.ZERO) > 0) {
      quantiteUCV = (quantiteUCV.divide(nombreUVParUCV, MathContext.DECIMAL64)).setScale(PRECISION_MAX_QUANTITE, RoundingMode.HALF_UP);
    }
    
    return quantiteUCV;
  }
  
  /**
   * Modifier la quantité de la ligne en unité de conditionnement de ventes (UCV).
   */
  public void setQuantiteUCV(BigDecimal pQuantiteUCV) {
    if (isArticleDecoupable()) {
      return;
    }
    
    // Calculer la quantité en UV
    BigDecimal quantiteUV = null;
    if (nombreUVParUCV != null && nombreUVParUCV.compareTo(BigDecimal.ZERO) != 0) {
      quantiteUV = pQuantiteUCV.multiply(nombreUVParUCV);
    }
    else {
      quantiteUV = pQuantiteUCV;
    }
    
    // Mémoriser la quantité en UV
    setQuantiteUV(quantiteUV);
  }
  
  /**
   * Nombre d'articles découpés.
   */
  public Integer getNombreDecoupe() {
    return nombreDecoupe;
  }
  
  /**
   * Modifier le nombre d'articles découpés.
   */
  public void setNombreDecoupe(Integer pNombreDecoupe) {
    nombreDecoupe = pNombreDecoupe;
  }
  
  /**
   * Indique si la ligne concerne un article découpable.
   * Pas forcément le meilleur test mais pour l'instant.
   */
  public boolean isArticleDecoupable() {
    return nombreDecoupe != null && nombreDecoupe != 0;
  }
  
  /**
   * Caractère identifiant le regroupement dans lequel la ligne est inclue (' ' si la ligne n'est pas regroupée).
   */
  public Character getCodeRegroupement() {
    if (codeRegroupement == null) {
      codeRegroupement = ' ';
    }
    return codeRegroupement;
  }
  
  /**
   * Modifier le caractère identifiant le regroupement dans lequel la ligne est inclue.
   */
  public void setCodeRegroupement(Character pCodeRegroupement) {
    codeRegroupement = pCodeRegroupement;
  }
  
  /**
   * Indiquer si la ligne est regroupée.
   */
  public boolean isLigneRegroupee() {
    return codeRegroupement != null && codeRegroupement != ' ';
  }
  
  /**
   * Indiquer si la ligne est la première d'un regroupement.
   */
  public boolean isDebutRegroupement() {
    return debutRegroupement;
  }
  
  /**
   * Met à jour si la ligne est la première d'un regroupement.
   */
  public void setDebutRegroupement(boolean pDebutRegroupement) {
    debutRegroupement = pDebutRegroupement;
  }
  
  /**
   * Indiquer si la ligne est la dernière d'un regroupement.
   */
  public boolean isFinRegroupement() {
    return finRegroupement;
  }
  
  /**
   * Met à jour si la ligne est la dernière d'un regroupement.
   */
  public void setFinRegroupement(boolean finRegroupement) {
    this.finRegroupement = finRegroupement;
  }
  
  /**
   * retourne le code extraction
   */
  public Character getCodeExtraction() {
    return codeExtraction;
  }
  
  /**
   * Met à jour le code extraction
   */
  public void setCodeExtraction(Character codeExtraction) {
    this.codeExtraction = codeExtraction;
  }
  
  /**
   * Retourne s'il s'agit d'une ligne annulée.
   */
  public boolean isAnnulee() {
    return codeExtraction == 'A';
  }
  
  /**
   * Retourner le taux de remise 1.
   * @return Le taux de remise 1.
   */
  public BigPercentage getTauxRemise1() {
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise 1.
   * @param pTauxRemise1 Le taux de remise 1.
   */
  public void setTauxRemise1(BigPercentage pTauxRemise) {
    tauxRemise1 = pTauxRemise;
  }
  
  /**
   * Retourner le taux de remise 2.
   * @return Le taux de remise 2.
   */
  public BigPercentage getTauxRemise2() {
    return tauxRemise2;
  }
  
  /**
   * Modifier le taux de remise 2.
   * @param pTauxRemise Le taux de remise 2.
   */
  public void setTauxRemise2(BigPercentage pTauxRemise) {
    this.tauxRemise2 = pTauxRemise;
  }
  
  /**
   * Retourner le taux de remise 3.
   * @return Le taux de remise 3.
   */
  public BigPercentage getTauxRemise3() {
    return tauxRemise3;
  }
  
  /**
   * Modifier le taux de remise 3.
   * @param pTauxRemise Le taux de remise 3.
   */
  public void setTauxRemise3(BigPercentage pTauxRemise) {
    this.tauxRemise3 = pTauxRemise;
  }
  
  /**
   * Retourner le taux de remise 4.
   * @return Le taux de remise 4.
   */
  public BigPercentage getTauxRemise4() {
    return tauxRemise4;
  }
  
  /**
   * Modifier le taux de remise 4.
   * @param pTauxRemise Le taux de remise 4.
   */
  public void setTauxRemise4(BigPercentage pTauxRemise) {
    this.tauxRemise4 = pTauxRemise;
  }
  
  /**
   * Retourner le taux de remise 5.
   * @return Le taux de remise 5.
   */
  public BigPercentage getTauxRemise5() {
    return tauxRemise5;
  }
  
  /**
   * Modifier le taux de remise 5.
   * @param pTauxRemise Le taux de remise 5.
   */
  public void setTauxRemise5(BigPercentage pTauxRemise) {
    this.tauxRemise5 = pTauxRemise;
  }
  
  /**
   * Retourner le taux de remise 6.
   * @return Le taux de remise 6.
   */
  public BigPercentage getTauxRemise6() {
    return tauxRemise6;
  }
  
  /**
   * Modifier le taux de remise 6.
   * @param pTauxRemise Le taux de remise 6.
   */
  public void setTauxRemise6(BigPercentage pTauxRemise) {
    this.tauxRemise6 = pTauxRemise;
  }
  
  /**
   * Retourner le prix de base HT.
   * @return Le prix de base HT.
   */
  public BigDecimal getPrixBaseHT() {
    return prixBaseHT;
  }
  
  /**
   * Modifier le prix de base HT.
   * @param pPrixBaseHT Le prix de base HT.
   */
  public void setPrixBaseHT(BigDecimal pPrixBaseHT) {
    this.prixBaseHT = pPrixBaseHT;
  }
  
  /**
   * Retourner la provenance du prix net.
   * @return La provenance du prix net.
   */
  public EnumProvenancePrixNet getProvenancePrixNet() {
    return provenancePrixNet;
  }
  
  /**
   * Modifier la provenance du prix net.
   * @param pProvenancePrixNet La provenance du prix net.
   */
  public void setProvenancePrixNet(EnumProvenancePrixNet pProvenancePrixNet) {
    provenancePrixNet = pProvenancePrixNet;
  }
  
  /**
   * Retourner le prix net HT.
   * 
   * Le prix net de la ligne de vente, celui qui est affiché dans le document de vente remis au client, est stocké dans :
   * - le prix net saisi lorsque le prix net a été saisi par l'utilisateur,
   * - le prix net calculé dans les autres cas.
   * C'est la provenance qui permet de sélectionner le cas de figure.
   * 
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetHT() {
    if (provenancePrixNet == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
      return prixNetSaisiHT;
    }
    return prixNetCalculeHT;
  }
  
  /**
   * Retourner le prix net saisi HT.
   * 
   * Il contient la valeur du prix net HT saisi par l'utilisateur (EnumProvenancePrixNetHT = SAISIE_LIGNE_VENTE dans ce cas) ou
   * le prix net TTC issu d'une condition de vente (EnumProvenancePrixNetHT = CONDITION_VENTE dans ce cas). Dans les autres cas, le prix
   * net est stocké dans le prix net calculé.
   * 
   * @return Prix net saisi HT.
   */
  public BigDecimal getPrixNetSaisiHT() {
    return prixNetSaisiHT;
  }
  
  /**
   * Modifier le prix net saisi HT.
   * @param pPrixNetHT Prix net saisi HT.
   */
  public void setPrixNetSaisiHT(BigDecimal pPrixNetHT) {
    prixNetSaisiHT = pPrixNetHT;
  }
  
  /**
   * Retourner le prix net calculé HT.
   * 
   * C'est le prix net TTC de la ligne de vente lorsqu'il a été calculé par l'algorithme de calcul du prix de vente.
   * 
   * @return Prix net calculé HT.
   */
  public BigDecimal getPrixNetCalculeHT() {
    return prixNetCalculeHT;
  }
  
  /**
   * Modifier le prix net HT calculé.
   * @param pPrixNetHT Le prix net calculé HT.
   */
  public void setPrixNetCalculeHT(BigDecimal pPrixNetHT) {
    prixNetCalculeHT = pPrixNetHT;
  }
  
  /**
   * Retourner le montant HT.
   * Le montant HT de ligne est égal au prix net HT multiplié par la quantité en UV.
   * @return Le montant HT.
   */
  public BigDecimal getMontantHT() {
    return montantHT;
  }
  
  /**
   * Modifier le montant HT.
   * @param pMontantHT Le montant HT.
   */
  public void setMontantHT(BigDecimal pMontantHT) {
    montantHT = pMontantHT;
  }
  
  /**
   * Retourner l'origine du prix de vente.
   * 
   * Cela correspond au champ L1IN18 de la ligne de vente. Ce champ servait initialement à indiquer si un prix est garanti (le prix ne
   * doit plus être modifié par la suite sauf sous certaines conditions). Son usage a été étendu afin de stocker l'origine du calcul
   * du prix de la ligne de vente.
   * 
   * Noter que la méthode getTexteOriginePrixVente() retourne le libellé de l'origine du prix de vente en le précisant avec des données
   * contextuelles dans certaines situations. Par exemple, la colonne utilisée est précisé dans le cas d'un prix standard.
   * 
   * @return Origine du prix de vente.
   */
  public EnumOriginePrixVente getOriginePrixVente() {
    return originePrixVente;
  }
  
  /**
   * Modifier l'origine du prix de vente.
   * @param pOriginePrixVente Origine du prix de vente.
   */
  public void setOriginePrixVente(EnumOriginePrixVente pOriginePrixVente) {
    originePrixVente = pOriginePrixVente;
  }
  
}
