/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.canaldevente;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

public class CanalDeVente extends AbstractClasseMetier<IdCanalDeVente> {
  private String nom = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public CanalDeVente(IdCanalDeVente pIdCanalDeVente) {
    super(pIdCanalDeVente);
  }
  
  @Override
  public String getTexte() {
    return nom;
  }
  
  // -- Accesseurs
  /**
   * Nom du canal de vente.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Nom du canal de vente suivi de son code entre parenthèse
   * Exemple : Principal (A00).
   * Par sécurité, le code est retourné seul au cas où le nom ne serait pas renseigné.
   */
  public String getNomAvecCode() {
    if (nom != null) {
      return nom + " (" + getId().getCode() + ")";
    }
    else {
      return getId().getCode();
    }
  }
  
  /**
   * Renseigner le nom de l'affaire.
   */
  public void setNom(String pNom) {
    this.nom = pNom;
  }
  
}
