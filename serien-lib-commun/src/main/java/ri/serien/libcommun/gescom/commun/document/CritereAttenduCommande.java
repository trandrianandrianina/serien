/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.document;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

/**
 * classe qui regroupe les critères de filtrage pour les commandes de vente.
 */
public class CritereAttenduCommande implements Serializable {
  // Variables
  private EnumAttenduCommande typefiltrage = EnumAttenduCommande.ATTENDU_ET_COMMANDE;
  private IdEtablissement idEtablissement = null;
  private IdArticle idArticle = null;
  private IdMagasin idMagasin = null;
  
  // -- Accesseurs
  
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  public void setIdEtablissement(IdEtablissement idEtablissement) {
    this.idEtablissement = idEtablissement;
  }
  
  public EnumAttenduCommande getTypefiltrage() {
    return typefiltrage;
  }
  
  public void setTypefiltrage(EnumAttenduCommande typefiltrage) {
    this.typefiltrage = typefiltrage;
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public void setIdArticle(IdArticle idArticle) {
    this.idArticle = idArticle;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin idMagasin) {
    this.idMagasin = idMagasin;
  }
  
}
