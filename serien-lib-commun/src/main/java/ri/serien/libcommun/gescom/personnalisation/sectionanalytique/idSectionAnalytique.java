/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.sectionanalytique;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une section analytique.
 *
 * L'identifiant est composé du code établissement et du code section.
 * Le code section correspond au paramètre SA du menu des ventes ou des achats.
 * Il est constitué de 2 caractères et se présente sous la forme AXXX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique
 * facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class idSectionAnalytique extends AbstractIdAvecEtablissement {
  
  private static final int LONGUEUR_MIN = 1;
  private static final int LONGUEUR_MAX = 4;
  
  // Variables
  private final String code;
  
  // Méthode publique
  /**
   * Constructeur privé pour un identifiant complet.
   */
  protected idSectionAnalytique(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static idSectionAnalytique getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new idSectionAnalytique(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  /**
   * Contrôler la validité du code section analytique.
   * Le code SectionAnalytique est une chaîne alphanumérique de 1 caractère obligatoire à 4 caractères maximum (3
   * facultatif).
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code section analytique n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_MIN || valeur.length() > LONGUEUR_MAX) {
      throw new MessageErreurException("Le code section analytique doit avoir minimum " + LONGUEUR_MIN
          + " caractère et ne doit pas dépasser " + LONGUEUR_MIN + "caractères " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static idSectionAnalytique controlerId(idSectionAnalytique pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du code section analytique est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du code ssection analytique n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthode surchargée
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof idSectionAnalytique)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de section analytique.");
    }
    idSectionAnalytique id = (idSectionAnalytique) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof idSectionAnalytique)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    idSectionAnalytique id = (idSectionAnalytique) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return getCodeEtablissement().trim() + SEPARATEUR_ID + code;
  }
  
  // -- Accesseurs
  
  /**
   * Code affaire.
   * Le code affaire est une chaîne alphanumérique de 2 caractères.
   */
  public String getCode() {
    return code;
  }
}
