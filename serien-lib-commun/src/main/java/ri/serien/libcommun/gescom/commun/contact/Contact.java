/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact;

import java.lang.reflect.InvocationTargetException;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.IdCategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.liencontact.LienContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.ListeLienContact;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.clonnage.ClonnageObject;

/**
 * Classe définissant un contact (personne physique) lié à un tiers
 * Cette classe n'est pas liée à un établissement car un même contact peut être utilisé sur plusieurs établissements et par plusieurs
 * tiers
 * le lien entre tiers et contact est fait par un fichier de lien et stocké dans le contact dans la liste des liens (listeLienContact)
 */
public class Contact extends AbstractClasseMetier<IdContact> {
  private IdCivilite idCivilite = null;
  // Téléphone (RETEL)
  private String numeroTelephone1 = "";
  // Fax (REFAX)
  private String numeroFax = "";
  // Catégorie (RECAT)
  private IdCategorieContact idCategorie = null;
  // Observations (REOBS)
  private String observations = "";
  // Clé de classement 1 (RECL1)
  private String cleClassement1 = "";
  // N° poste (REPOS)
  private String numeroPoste = "";
  // Clé pour index tel (RECTL)
  private String cleIndexTelephone = "";
  // Adresse Email (RENET)
  private String email = "";
  // 1=destinataire bon (REIN1)
  private char destinataire = ' ';
  // Copie Carbon cachée (REIN2)
  private char emailCopieCachee = ' ';
  // Non utilisé (REIN3)
  private char rein3 = ' ';
  // Clé de class.2 (RECL2)
  private String cleClassement2 = "";
  // Téléphone 2 (RETEL2)
  private String numeroTelephone2 = "";
  // Critères d''analyse 1 (RETOP1)
  private String criteresAnalyse1 = "";
  // Critères d''analyse 2 (RETOP2)
  private String criteresAnalyse2 = "";
  // Critères d''analyse 3 (RETOP3)
  private String criteresAnalyse3 = "";
  // Critères d''analyse 4 (RETOP4)
  private String criteresAnalyse4 = "";
  // Critères d''analyse 5 (RETOP5)
  private String criteresAnalyse5 = "";
  // 1=ANTI-SPAM (REASPA)
  private char emailAntiSpam = ' ';
  // 1=ANTI-APPEL (REAAPP)
  private char antiAppel = ' ';
  // 'TYPE ZP (RETYZP)
  private char typeZonePersonnalisee = ' ';
  // Nom contact (RENOM)
  private String nom = "";
  // Prénom contact (REPRE)
  private String prenom = "";
  // Adresse Email2 (RENET2)
  private String email2 = "";
  // Profil / ALIAS (REPRF) lien avec le psemussm
  private String profilAlias = "";
  // Non utilisé (REIN4)
  private char destinataireFactures = ' ';
  // Non utilisé (REIN5)
  private char rein5 = ' ';
  // Non utilisé (REIN6)
  private char rein6 = ' ';
  // Non utilisé (REIN7)
  private char rein7 = ' ';
  // Non utilisé (REIN8)
  private char rein8 = ' ';
  
  // Droits Webshop
  private boolean droitAccesWebshop = false;
  private boolean droitVisualisationStockWebshop = false;
  private boolean droitVisualisationPrixWebshop = false;
  private boolean droitSaisieDevisWebshop = false;
  private boolean droitSaisieCommandeWebshop = false;
  private boolean droitVisualisationDocumentSociete = false;
  private boolean droitAccesRestreintWebshop = false;
  
  // Adresse contact
  private Adresse adresse = null;
  
  // Liens au contact
  private ListeLienContact listeLienContact = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public Contact(IdContact pIdContact) {
    super(pIdContact);
    listeLienContact = new ListeLienContact();
  }
  
  /**
   * Renvoit le nom complet du contact suivi de son numéro entre parenthèses.
   * C'est cette méthode qui définit le format d'affichage des clients dans les composants graphiques.
   * Format : NomComplet (CodeContact)
   */
  @Override
  public String getTexte() {
    return getNomComplet();
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public Contact clone() {
    Contact contactTemporaire = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      contactTemporaire = (Contact) super.clone();
      if (contactTemporaire != null) {
        ClonnageObject.copyProperties(contactTemporaire, this);
      }
    }
    catch (CloneNotSupportedException cnse) {
      throw new MessageErreurException(cnse, "Clonnage de l'objet Contact non supporté");
    }
    catch (IllegalAccessException e) {
      throw new MessageErreurException(e, "Clonnage de l'objet Contact non supporté en raison de non accès à une propriété");
    }
    catch (InvocationTargetException e) {
      throw new MessageErreurException(e, "Clonnage de l'objet Contact non supporté en raison d'une erreur d'appel à une fonction");
    }
    
    return contactTemporaire;
  }
  
  /**
   * Retourner le lien du contact au client passé en paramètre, si ce lien existe
   * 
   * @param pIdClient
   * @return LienContact si le lien avec le client existe, null si le lien n'existe pas
   */
  public LienContact getLien(IdClient pIdClient) {
    // Tester si la liste des liens est présente
    if (listeLienContact == null) {
      return null;
    }
    
    return listeLienContact.getLien(pIdClient);
  }
  
  /**
   * Retourner le lien du contact au fournisseur passé en paramètre, si ce lien existe
   * 
   * @param pIdFournisseur
   * @return LienContact si le lien avec le fournisseur existe, null si le lien n'existe pas
   */
  public LienContact getLien(IdFournisseur pIdFournisseur) {
    // Tester si la liste des liens est présente
    if (listeLienContact == null) {
      return null;
    }
    
    return listeLienContact.getLien(pIdFournisseur);
  }
  
  /**
   * Indiquer si le contact est le contact principal d'un tiers, quelque soit le tiers.
   * 
   * @return true=le contact est contact principal, false=le contact n'est le contact principal d'aucun tiers.
   */
  public boolean isContactPrincipal() {
    // Tester si la liste des liens est présente
    if (listeLienContact == null) {
      return false;
    }
    
    // Parcourir les liens pour voir s'il existe un lien marqué comme principal
    for (LienContact lienContact : listeLienContact) {
      if (listeLienContact.get(lienContact.getId()).isContactPrincipal()) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Indiquer si le contact est le contact principal d'un client.
   * 
   * @param pIdClient Identifiant du client pour lequel on souhaite savoir si le contact est principal.
   * @return true=le contact est le contact principal du client, false=le contact n'est pas le contact principal du client.
   */
  public boolean isContactPrincipal(IdClient pIdClient) {
    // Tester les paramètres
    if (pIdClient == null) {
      return false;
    }
    
    // Tester si la liste des liens est présente
    if (listeLienContact == null) {
      return false;
    }
    
    // Parcourir les liens pour voir s'il existe un lien client marqué comme principal
    for (LienContact lienContact : listeLienContact) {
      if (lienContact.getId().isLienClient() && lienContact.getId().getIdClient().equals(pIdClient)
          && listeLienContact.get(lienContact.getId()).isContactPrincipal()) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Indiquer si le contact est le contact principal d'un fournisseur.
   * 
   * @param pIdFournisseur Identifiant du fournisseur pour lequel on souhaite savoir si le contact est principal.
   * @return true=le contact est le contact principal du fournisseur, false=le contact n'est pas le contact principal du fournisseur.
   */
  public boolean isContactPrincipal(IdFournisseur pIdFournisseur) {
    // Tester les paramètres
    if (pIdFournisseur == null) {
      return false;
    }
    
    // Tester si la liste des liens est présente
    if (listeLienContact == null) {
      return false;
    }
    
    // Parcourir les liens pour voir s'il existe un lien fournisseur marqué comme principal
    for (LienContact lienContact : listeLienContact) {
      if (lienContact.getId().isLienFournisseur() && lienContact.getId().getIdFournisseur().equals(pIdFournisseur)
          && listeLienContact.get(lienContact.getId()).isContactPrincipal()) {
        return true;
      }
    }
    return false;
  }
  
  // -- Accesseurs
  
  /**
   * Civilité du contact (peut être null car ce n'est pas une donnée obligatoire).(RECIV)
   */
  public IdCivilite getIdCivilite() {
    return idCivilite;
  }
  
  /**
   * Modifier la civilité du contact.
   */
  public void setIdCivilite(IdCivilite pIdCivilite) {
    idCivilite = pIdCivilite;
  }
  
  /**
   * Nom complet du contact constitué de la cvilité, du nom et du prénom.
   * 
   * Pour information, cette information est stockée dans la champ REPAC de la base de données (mais juste le nom + prénom). Nous faisons
   * le choix d'ignorer ce champ de la base de données afin de recontruire l'information à la volée à partir des autres informations du
   * contact.
   */
  public String getNomComplet() {
    if (idCivilite != null) {
      if (nom != null && prenom != null) {
        return idCivilite.getCode() + ' ' + nom + ' ' + prenom;
      }
      else if (nom != null) {
        return idCivilite.getCode() + ' ' + nom;
      }
      else if (prenom != null) {
        return idCivilite.getCode() + ' ' + prenom;
      }
    }
    else {
      if (nom != null && prenom != null) {
        return nom + ' ' + prenom;
      }
      else if (nom != null) {
        return nom;
      }
      else if (prenom != null) {
        return prenom;
      }
    }
    
    return "";
  }
  
  /**
   * Retourne la liste des LienContact filtrée pour un établissement
   */
  public ListeLienContact retournerListeLienContactEtablissement(IdEtablissement pIdEtablissement) {
    ListeLienContact listeLienContactFiltree = new ListeLienContact();
    
    for (LienContact lienContact : listeLienContact) {
      if (lienContact.getId().getCodeEtablissementLien() != null
          && lienContact.getId().getCodeEtablissementLien().equals(pIdEtablissement.getCodeEtablissement())) {
        listeLienContactFiltree.add(lienContact);
      }
    }
    return listeLienContactFiltree;
  }
  
  /**
   * Retourner un des numéros de téléphone du contact (le premier s'i lest renseigné, le second sinon).
   */
  public String getNumeroTelephone() {
    if (numeroTelephone1 != null && !numeroTelephone1.isEmpty()) {
      return numeroTelephone1;
    }
    else {
      return numeroTelephone2;
    }
  }
  
  /**
   * Premier numéro de téléphone du contact.
   * Voir aussi getNumeroTelephone().
   */
  public String getNumeroTelephone1() {
    return numeroTelephone1;
  }
  
  /**
   * Modifier le premier numéro de téléphone du contact.
   */
  public void setNumeroTelephone1(String pNumeroTelephone) {
    this.numeroTelephone1 = pNumeroTelephone;
  }
  
  /**
   * Deuxième numéro de téléphone du contact.
   * Voir aussi getNumeroTelephone().
   */
  public String getNumeroTelephone2() {
    return numeroTelephone2;
  }
  
  /**
   * Modifier le second numéro de téléphone du contact.
   */
  public void setNumeroTelephone2(String pNumeroTelephone2) {
    this.numeroTelephone2 = pNumeroTelephone2;
  }
  
  /**
   * Numéro de fax du contact
   */
  public String getNumeroFax() {
    return numeroFax;
  }
  
  /**
   * Numéro de fax du contact
   */
  public void setNumeroFax(String numeroFax) {
    this.numeroFax = numeroFax;
  }
  
  /**
   * Observations sur le contact (texte interne)
   */
  public String getObservations() {
    return observations;
  }
  
  /**
   * Observations sur le contact (texte interne)
   */
  public void setObservations(String observations) {
    this.observations = observations;
  }
  
  /**
   * Première clé de classement du contact
   */
  public String getCleClassement1() {
    return cleClassement1;
  }
  
  /**
   * Première clé de classement du contact
   */
  public void setCleClassement1(String cleClassement1) {
    this.cleClassement1 = cleClassement1;
  }
  
  /**
   * Numéro du poste téléphonique du contact
   */
  public String getNumeroPoste() {
    return numeroPoste;
  }
  
  /**
   * Numéro du poste téléphonique du contact
   */
  public void setNumeroPoste(String numeroPoste) {
    this.numeroPoste = numeroPoste;
  }
  
  /**
   * Index téléphone
   */
  public String getCleIndexTelephone() {
    return cleIndexTelephone;
  }
  
  /**
   * Index téléphone
   */
  public void setCleIndexTelephone(String cleIndexTelephone) {
    this.cleIndexTelephone = cleIndexTelephone;
  }
  
  /**
   * Adresse e-mail du contact
   */
  public String getEmail() {
    return email;
  }
  
  /**
   * Adresse e-mail du contact
   */
  public void setEmail(String email) {
    this.email = email;
  }
  
  /**
   * Le contact est-il destinataire des documents de vente ?
   */
  public char getDestinataire() {
    return destinataire;
  }
  
  /**
   * Le contact est-il destinataire des documents de vente ?
   */
  public void setDestinataire(char destinataire) {
    this.destinataire = destinataire;
  }
  
  /**
   * Envoi des mails en copie cachée
   */
  public char getEmailCopieCachee() {
    return emailCopieCachee;
  }
  
  /**
   * Envoi des mails en copie cachée
   */
  public void setEmailCopieCachee(char emailCopieCachee) {
    this.emailCopieCachee = emailCopieCachee;
  }
  
  public char getREIN3() {
    return rein3;
  }
  
  public void setREIN3(char prein3) {
    rein3 = prein3;
  }
  
  /**
   * Deuxième clé de classement du contact
   */
  public String getCleClassement2() {
    return cleClassement2;
  }
  
  /**
   * Deuxième clé de classement du contact
   */
  public void setCleClassement2(String cleClassement2) {
    this.cleClassement2 = cleClassement2;
  }
  
  public String getCriteresAnalyse1() {
    return criteresAnalyse1;
  }
  
  public void setCriteresAnalyse1(String criteresAnalyse1) {
    this.criteresAnalyse1 = criteresAnalyse1;
  }
  
  public String getCriteresAnalyse2() {
    return criteresAnalyse2;
  }
  
  public void setCriteresAnalyse2(String criteresAnalyse2) {
    this.criteresAnalyse2 = criteresAnalyse2;
  }
  
  public String getCriteresAnalyse3() {
    return criteresAnalyse3;
  }
  
  public void setCriteresAnalyse3(String criteresAnalyse3) {
    this.criteresAnalyse3 = criteresAnalyse3;
  }
  
  public String getCriteresAnalyse4() {
    return criteresAnalyse4;
  }
  
  public void setCriteresAnalyse4(String criteresAnalyse4) {
    this.criteresAnalyse4 = criteresAnalyse4;
  }
  
  public String getCriteresAnalyse5() {
    return criteresAnalyse5;
  }
  
  public void setCriteresAnalyse5(String criteresAnalyse5) {
    this.criteresAnalyse5 = criteresAnalyse5;
  }
  
  public char getEmailAntiSpam() {
    return emailAntiSpam;
  }
  
  public void setEmailAntiSpam(char emailAntiSpam) {
    this.emailAntiSpam = emailAntiSpam;
  }
  
  public char getAntiAppel() {
    return antiAppel;
  }
  
  public void setAntiAppel(char antiAppel) {
    this.antiAppel = antiAppel;
  }
  
  public char getTypeZonePersonnalisee() {
    return typeZonePersonnalisee;
  }
  
  public void setTypeZonePersonnalisee(char typeZonePersonnalisee) {
    this.typeZonePersonnalisee = typeZonePersonnalisee;
  }
  
  public String getNom() {
    return nom;
  }
  
  public void setNom(String nom) {
    if (nom == null) {
      nom = "";
    }
    this.nom = nom.trim();
  }
  
  public String getPrenom() {
    return prenom;
  }
  
  public void setPrenom(String prenom) {
    if (prenom == null) {
      prenom = "";
    }
    this.prenom = prenom.trim();
  }
  
  public String getEmail2() {
    return email2;
  }
  
  public void setEmail2(String email2) {
    this.email2 = email2;
  }
  
  public String getProfilAlias() {
    return profilAlias;
  }
  
  public void setProfilAlias(String profilAlias) {
    this.profilAlias = profilAlias;
  }
  
  public char getDestinataireFactures() {
    return destinataireFactures;
  }
  
  public void setDestinataireFactures(char prein4) {
    destinataireFactures = prein4;
  }
  
  public char getREIN5() {
    return rein5;
  }
  
  public void setREIN5(char prein5) {
    rein5 = prein5;
  }
  
  public char getREIN6() {
    return rein6;
  }
  
  public void setREIN6(char prein6) {
    rein6 = prein6;
  }
  
  public char getREIN7() {
    return rein7;
  }
  
  public void setREIN7(char prein7) {
    rein7 = prein7;
  }
  
  public char getREIN8() {
    return rein8;
  }
  
  public void setREIN8(char prein8) {
    rein8 = prein8;
  }
  
  public IdCategorieContact getIdCategorie() {
    return idCategorie;
  }
  
  public void setIdCategorie(IdCategorieContact idCategorie) {
    this.idCategorie = idCategorie;
  }
  
  /**
   * Liste des liens à un tiers
   */
  public ListeLienContact getListeLienContact() {
    return listeLienContact;
  }
  
  /**
   * Liste des liens à un tiers
   */
  public void setListeLienContact(ListeLienContact listeLienContact) {
    this.listeLienContact = listeLienContact;
  }
  
  /**
   * Adresse du contact
   */
  public Adresse getAdresse() {
    return adresse;
  }
  
  /**
   * Adresse du contact
   */
  public void setAdresse(Adresse pAdresse) {
    adresse = pAdresse;
  }
  
  /**
   * Le contact a t il le droit d'accéder au WebShop ?
   */
  public boolean isDroitAccesWebshop() {
    return droitAccesWebshop;
  }
  
  /**
   * Fixe si le contact a le droit d'accéder au WebShop ?
   */
  public void setDroitAccesWebshop(boolean droitAccesWebshop) {
    this.droitAccesWebshop = droitAccesWebshop;
  }
  
  /**
   * Le contact a t il le droit de visualiser les stocks au WebShop ?
   */
  public boolean isDroitVisualisationStockWebshop() {
    return droitVisualisationStockWebshop;
  }
  
  /**
   * Fixe si le contact a le droit de visualiser les stocks au WebShop ?
   */
  public void setDroitVisualisationStockWebshop(boolean droitVisualisationStockWebshop) {
    this.droitVisualisationStockWebshop = droitVisualisationStockWebshop;
  }
  
  /**
   * Le contact a t il le droit de visualiser les prix à partir du WebShop ?
   */
  public boolean isDroitVisualisationPrixWebshop() {
    return droitVisualisationPrixWebshop;
  }
  
  /**
   * Fixe si le contact ale droit de visualiser les prix à partir du WebShop ?
   */
  public void setDroitVisualisationPrixWebshop(boolean droitVisualisationPrixWebshop) {
    this.droitVisualisationPrixWebshop = droitVisualisationPrixWebshop;
  }
  
  /**
   * Le contact a t il le droit de saisir des devis dans le WebShop ?
   */
  public boolean isDroitSaisieDevisWebshop() {
    return droitSaisieDevisWebshop;
  }
  
  /**
   * Fixe si le contact a le droit de saisir des devis dans le WebShop ?
   */
  public void setDroitSaisieDevisWebshop(boolean droitSaisieDevisWebshop) {
    this.droitSaisieDevisWebshop = droitSaisieDevisWebshop;
  }
  
  /**
   * Le contact a t il le droit de saisir des commandes dans le WebShop ?
   */
  public boolean isDroitSaisieCommandeWebshop() {
    return droitSaisieCommandeWebshop;
  }
  
  /**
   * Fixe si le contact a le droit de saisir des commandes dans le WebShop ?
   */
  public void setDroitSaisieCommandeWebshop(boolean droitSaisieCommandeWebshop) {
    this.droitSaisieCommandeWebshop = droitSaisieCommandeWebshop;
  }
  
  public boolean isDroitAccesRestreintWebshop() {
    return droitAccesRestreintWebshop;
  }
  
  public void setDroitAccesRestreintWebshop(boolean droitAccesRestreintWebshop) {
    this.droitAccesRestreintWebshop = droitAccesRestreintWebshop;
  }
  
  public boolean isDroitVisualisationDocumentSociete() {
    return droitVisualisationDocumentSociete;
  }
  
  public void setDroitVisualisationDocumentSociete(boolean droitVisualisationDocumentSociete) {
    this.droitVisualisationDocumentSociete = droitVisualisationDocumentSociete;
  }
  
  /**
   * Retourne le premier lien si le contact en cours est son contact principal
   */
  public LienContact getLienAyantContactCommeContactPrincipal() {
    LienContact lien = null;
    if (getListeLienContact() != null && !getListeLienContact().isEmpty()) {
      // Parcourir les liens de contact
      for (LienContact lienTemporaire : getListeLienContact()) {
        if (lienTemporaire.getId() != null) {
          // Mettre le client comme lien si le contact en cours est son contact principal
          if (lienTemporaire.getId().getIdClient() != null && isContactPrincipal(lienTemporaire.getId().getIdClient())) {
            lien = lienTemporaire;
            break;
          }
          // Mettre le fournisseur comme lien si le contact en cours est son contact principal
          else if (lienTemporaire.getId().getIdFournisseur() != null && isContactPrincipal(lienTemporaire.getId().getIdFournisseur())) {
            lien = lienTemporaire;
            break;
          }
        }
      }
      
      // Sinon on prend le premier lien qui existe pour le contact
      if (lien == null) {
        lien = getListeLienContact().get(0);
      }
    }
    
    return lien;
  }
  
}
