/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.boncour;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste de CarnetBonCour.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de carnets de bons de cour.
 */
public class ListeCarnetBonCour extends ListeClasseMetier<IdCarnetBonCour, CarnetBonCour, ListeCarnetBonCour> {
  /**
   * Constructeur.
   */
  public ListeCarnetBonCour() {
  }
  
  /**
   * Construire une liste de CarnetBonCour correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des CarnetBonCour ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeCarnetBonCour creerListeNonChargee(List<IdCarnetBonCour> pListeIdCarnetBonCour) {
    ListeCarnetBonCour listeCarnetBonCour = new ListeCarnetBonCour();
    if (pListeIdCarnetBonCour != null) {
      for (IdCarnetBonCour idCarnetBonCour : pListeIdCarnetBonCour) {
        CarnetBonCour carnetBonCour = new CarnetBonCour(idCarnetBonCour);
        carnetBonCour.setCharge(false);
        listeCarnetBonCour.add(carnetBonCour);
      }
    }
    return listeCarnetBonCour;
  }
  
  /**
   * Charger la liste des objets métiers suivant la liste d'identifiants donnée.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeCarnetBonCour charger(IdSession pIdSession, List<IdCarnetBonCour> pListeIdCarnetBonCour) {
    return ManagerServiceDocumentVente.chargerListeCarnetBonCour(pIdSession, pListeIdCarnetBonCour);
  }
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeCarnetBonCour charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
}
