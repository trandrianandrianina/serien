/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import java.math.BigDecimal;

public class ValeurInitialeCommandeAchat {
  // Variables
  // Montant total HT (port compris)
  private BigDecimal montantHT = BigDecimal.ZERO;
  // Montant total TVA (port compris)
  private BigDecimal montantTVA = BigDecimal.ZERO;
  // Montant total TTC (port compris)
  private BigDecimal montantTTC = BigDecimal.ZERO;
  // Montant du port HT
  private BigDecimal montantPortHT = BigDecimal.ZERO;
  // Y a t-il eu au moins une réception sur la commande ?
  private EnumEtapeExtraction etapeExtraction = null;
  
  // -- Méthodes privées
  
  public BigDecimal getMontantHT() {
    return montantHT;
  }
  
  public void setMontantHT(BigDecimal montantHT) {
    this.montantHT = montantHT;
  }
  
  public BigDecimal getMontantTVA() {
    return montantTVA;
  }
  
  public void setMontantTVA(BigDecimal montantTVA) {
    this.montantTVA = montantTVA;
  }
  
  public BigDecimal getMontantTTC() {
    return montantTTC;
  }
  
  public void setMontantTTC(BigDecimal montantTTC) {
    this.montantTTC = montantTTC;
  }
  
  public BigDecimal getMontantPortHT() {
    return montantPortHT;
  }
  
  public void setMontantPortHT(BigDecimal montantPortHT) {
    this.montantPortHT = montantPortHT;
  }
  
  public EnumEtapeExtraction getEtapeExtraction() {
    return etapeExtraction;
  }
  
  public void setEtapeExtraction(EnumEtapeExtraction etapeExtraction) {
    this.etapeExtraction = etapeExtraction;
  }
  
}
