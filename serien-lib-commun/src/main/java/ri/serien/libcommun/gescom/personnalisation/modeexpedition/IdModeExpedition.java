/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.modeexpedition;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un mode d'expédition.
 *
 * L'identifiant est composé du code mode d'expédition. Le mode d'expédition n'est pas lié à un établissement.
 * Le code mode d'expédition correspond au paramètre EX du menu des ventes ou des achats.
 * Il est constitué de 0 à 2 caractères et se présente sous la forme AX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdModeExpedition extends AbstractId {
  private static final int LONGUEUR_CODE_MIN = 0;
  private static final int LONGUEUR_CODE_MAX = 2;
  public static final String CODE_LIVRAISON = "*L";
  public static final String CODE_ENLEVEMENT = "*E";
  
  // Variables
  private final String code;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdModeExpedition(EnumEtatObjetMetier pEnumEtatObjetMetier, String pCode) {
    super(pEnumEtatObjetMetier);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdModeExpedition getInstance(String pCode) {
    return new IdModeExpedition(EnumEtatObjetMetier.MODIFIE, pCode);
  }
  
  /**
   * Contrôler la validité du code mode d'expédition.
   * Le code mode d'expédition est une chaîne alphanumérique de 1 à 2 caractères.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code du mode d'expédition n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MAX) {
      throw new MessageErreurException("Le code du mode d'expédition doit comporter entre " + LONGUEUR_CODE_MIN + " et "
          + LONGUEUR_CODE_MAX + " caractères : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdModeExpedition controlerId(IdModeExpedition pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du code mode d'expédition est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du code mode d'expédition n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdModeExpedition)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants mode d'expédition.");
    }
    IdModeExpedition id = (IdModeExpedition) pObject;
    return code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdModeExpedition)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdModeExpedition id = (IdModeExpedition) pObject;
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
  // -- Accesseurs
  
  /**
   * Code mode d'expédition.
   * Le code mode d'expédition est une chaîne alphanumérique de 1 à 2 caractères.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Retourne true si le mode d'expédition n'est pas définit.
   */
  public Boolean isNonDefini() {
    return code.isEmpty();
  }
  
  /**
   * Retourne true si le mode d'expédition est livraison.
   */
  public Boolean isLivraison() {
    return code.equals(CODE_LIVRAISON);
  }
  
  /**
   * Retourne true si le mode d'expédition est enlèvement.
   */
  public Boolean isEnlevement() {
    return code.equals(CODE_ENLEVEMENT);
  }
}
