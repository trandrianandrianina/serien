/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Filtre sur les articles gérés en stock
 */
public enum EnumFiltreArticleStock {
  OPTION_GERE_STOCK(0, "Tous les articles sauf les non gérés en stock"),
  OPTION_NON_GERE_STOCK(1, "Non gérés en stock seulement"),
  OPTION_TOUS_LES_ARTICLES(2, "Tous les articles");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumFiltreArticleStock(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumFiltreArticleStock valueOfByCode(Integer pCode) {
    for (EnumFiltreArticleStock value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("La valeur du filtre stock sur les articles est invalide : " + pCode);
  }
}
