/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette;

import java.math.BigDecimal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Ligne de résultat d'une recherche article de type palette.
 * 
 * Cette classe, utilisée par le comptoir, permet d'afficher le résultat lors d'une recherche article de type palette.
 * L'identifiant est important car la première recherche retourne la liste complète mais uniquement avec l'identifiant de renseigné.
 * Lorsqu'une nouvelle page est affichée, la liste des identifiants est utilisée pour charger le reste des informations.
 * 
 * Voici la liste des colonnes du résultat d'une recherche article de type palette au comptoir :
 * 1) Qté
 * 2) Libellé
 * 3) Stock disponible
 * 4) Qté chez le client
 * 5) Prix consignation
 * 6) Prix déconsignation
 */
public class ResultatRechercheArticlePalette extends AbstractClasseMetier<IdResultatRechercheArticlePalette> {
  // Colonne 2 : libellé article
  private String libelle1 = null;
  private String libelle2 = null;
  private String libelle3 = null;
  private String libelle4 = null;
  
  // Colonne 3 : stock physique
  private BigDecimal stockPhysique = null;
  
  // Colonne 4 : stock client
  private BigDecimal stockClient = null;
  
  // Colonne 5 : prix consignation
  private BigDecimal prixConsignation = null;
  
  // Colonne 6 : prix déconsignation
  private BigDecimal prixDeconsignation = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur avec l'identifiant de l'information de vente.
   */
  public ResultatRechercheArticlePalette(IdResultatRechercheArticlePalette pIdResultatRechercheArticle) {
    super(pIdResultatRechercheArticle);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  // --
  
  @Override
  public String getTexte() {
    return getLibelle1();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs pours les colonnes du tableau de résultat d'un article standard
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le libellé complet de l'article.
   * Cette méthode concaténe les 4 libellés articles en un seul libellé.
   */
  public String getLibelle() {
    if (libelle1 == null || libelle2 == null || libelle3 == null || libelle4 == null) {
      return null;
    }
    String libelle = "";
    if (libelle1 != null) {
      libelle += libelle1;
    }
    if (libelle2 != null) {
      libelle += libelle2;
    }
    if (libelle3 != null) {
      libelle += libelle3;
    }
    if (libelle4 != null) {
      libelle += libelle4;
    }
    return libelle;
  }
  
  /**
   * Retourner la première partie du libellé de l'article.
   */
  public String getLibelle1() {
    return libelle1;
  }
  
  /**
   * Modifier la première partie du libellé de l'article.
   */
  public void setLibelle1(String pLibelle) {
    libelle1 = pLibelle;
  }
  
  /**
   * Retourner la partie 2 du libellé de l'article.
   */
  public String getLibelle2() {
    return libelle2;
  }
  
  /**
   * Modifier la partie 2 du libellé de l'article.
   */
  public void setLibelle2(String pLibelle) {
    libelle2 = pLibelle;
  }
  
  /**
   * Retourner la partie 3 du libellé de l'article.
   */
  public String getLibelle3() {
    return libelle3;
  }
  
  /**
   * Modifier la partie 3 du libellé de l'article.
   */
  public void setLibelle3(String pLibelle) {
    libelle3 = pLibelle;
  }
  
  /**
   * Retourner la partie 4 du libellé de l'article.
   */
  public String getLibelle4() {
    return libelle4;
  }
  
  /**
   * Modifier la partie 4 du libellé de l'article.
   */
  public void setLibelle4(String pLibelle) {
    libelle4 = pLibelle;
  }
  
  /**
   * Retourner la quantité en stock physique de l'article.
   * @return Quantité en stock physique.
   */
  public BigDecimal getStockPhysique() {
    return stockPhysique;
  }
  
  /**
   * Modifier la quantité en stock physique de l'article.
   * @param pStockPhysique Quantité en stock physique.
   */
  public void setQuantitePhysique(BigDecimal pStockPhysique) {
    stockPhysique = pStockPhysique;
  }
  
  /**
   * Retourner la quantité en stock de l'article chez le client.
   * @return Quantité en stock chez le client.
   */
  public BigDecimal getQuantiteStockClient() {
    return stockClient;
  }
  
  /**
   * Modifier la quantité en stock de l'article chez le client.
   * 
   * Le stock client est lu dans le stock client par chantier si le stock est géré par chantier. Il est lu dans le stock client
   * standard dans le cas contraire.
   * 
   * @param pStock Quantité en stock chez le client.
   */
  public void setQuantiteStockClient(BigDecimal pStockClient) {
    stockClient = pStockClient;
  }
  
  /**
   * Retourner le prix consignation de l'article.
   * @return Prix consignation.
   */
  public BigDecimal getPrixConsignation() {
    return prixConsignation;
  }
  
  /**
   * Modifier le prix consignation de l'article.
   * @param pPrixConsignation Prix consignation.
   */
  public void setPrixConsignation(BigDecimal pPrixConsignation) {
    prixConsignation = pPrixConsignation;
  }
  
  /**
   * Retourner le prix déconsignation de l'article.
   * @return Prix déconsignation.
   */
  public BigDecimal getPrixDeconsignation() {
    return prixDeconsignation;
  }
  
  /**
   * Modifier le prix déconsignation de l'article.
   * @param pPrixConsignation Prix consignation.
   */
  public void setPrixDeconsignation(BigDecimal pPrixDeconsignation) {
    prixDeconsignation = pPrixDeconsignation;
  }
}
