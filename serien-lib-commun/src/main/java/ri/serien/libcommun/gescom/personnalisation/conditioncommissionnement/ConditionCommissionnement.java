/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Catégorie client.
 */
public class ConditionCommissionnement extends AbstractClasseMetier<IdConditionCommissionnement> {
  
  // Variables
  private String libelle = "";
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public ConditionCommissionnement(IdConditionCommissionnement pIdConditionCommissionnement) {
    super(pIdConditionCommissionnement);
  }
  
  // -- Méthodes publiques
  
  // -- Accesseurs
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Libellé de la catégorie client.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Renseigner le libellé de la catégorie client.
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
}
