/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.document.EnumEtatReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.ModeReglement;
import ri.serien.libcommun.gescom.vente.avoir.IdAvoir;

/**
 * Contient les règlements d'un document d'achat.
 * A voir si besoin s'il faut effectuer le même travail que dans les ventes.
 */
public class Reglements implements Serializable, Cloneable {
  // Constantes
  public static final int NOMBRE_REGLEMENTS = 4;
  
  // Variables
  // Zones de l'entête du document
  private EnumEtatReglement etatReglement = null;
  private ModeReglement modeReglement1 = null;
  private String codeEcheance1 = "";
  private String pourcentageReglement1 = ""; // valeur ou "**" pour 100%
  private ModeReglement modeReglement2 = null;
  private String codeEcheance2 = "";
  private String pourcentageReglement2 = "";
  private ModeReglement modeReglement3 = null;
  private String codeEcheance3 = "";
  private String pourcentageReglement3 = "";
  private ModeReglement modeReglement4 = null;
  private String codeEcheance4 = "";
  private String pourcentageReglement4 = "";
  private int groupeRGL1 = 0;
  private int groupeRGL2 = 0;
  private int groupeRGL3 = 0;
  private int groupeRGL4 = 0;
  private int numeroLigneReglement = 0;
  private int nombreReglements = 0;
  
  private boolean acompteObligatoire = false;
  private BigDecimal montantAcompteRegle = null;
  private String libelleFacture = "";
  
  private BigDecimal montant1 = BigDecimal.ZERO;
  private BigDecimal montant2 = BigDecimal.ZERO;
  private BigDecimal montant3 = BigDecimal.ZERO;
  private BigDecimal montant4 = BigDecimal.ZERO;
  
  private Date dateEcheance1 = null;
  private Date dateEcheance2 = null;
  private Date dateEcheance3 = null;
  private Date dateEcheance4 = null;
  
  // Montant TTC du document d'origine
  private BigDecimal montantTTCDocumentOrigine = null;
  // Montant total TTC de tous les documents liés au document d'origine
  private BigDecimal montantTTCTotalLie = null;
  // Montant total TTC réglé depuis le document d'origine
  private BigDecimal montantTTCTotalRegle = null;
  
  // Champs si reglement par avoir (4 avoirs possibles)
  private IdAvoir idAvoir1 = null;
  private BigDecimal montantAvoir1 = BigDecimal.ZERO;
  private IdAvoir idAvoir2 = null;
  private BigDecimal montantAvoir2 = BigDecimal.ZERO;
  private IdAvoir idAvoir3 = null;
  private BigDecimal montantAvoir3 = BigDecimal.ZERO;
  private IdAvoir idAvoir4 = null;
  private BigDecimal montantAvoir4 = BigDecimal.ZERO;
  
  // Variables de travail
  private boolean modifReglement1 = false;
  private boolean modifReglement2 = false;
  private boolean modifReglement3 = false;
  private boolean modifReglement4 = false;
  
  /**
   * Retourne le montant de l'acompte.
   */
  public BigDecimal retournerMontantAcompte() {
    if (modeReglement1 != null && modeReglement1.isAcompte()) {
      return montant1;
    }
    else if (modeReglement2 != null && modeReglement2.isAcompte()) {
      return montant2;
    }
    else if (modeReglement3 != null && modeReglement3.isAcompte()) {
      return montant3;
    }
    else if (modeReglement4 != null && modeReglement4.isAcompte()) {
      return montant4;
    }
    
    return BigDecimal.ZERO;
  }
  
  /**
   * Supprime les codes de règlement lorsque les montants sont égals à zéro
   */
  public void nettoyerCodesAvecMontantsAZero() {
    if (modeReglement1 != null && montant1.compareTo(BigDecimal.ZERO) == 0) {
      modeReglement1 = null;
    }
    if (modeReglement2 != null && montant2.compareTo(BigDecimal.ZERO) == 0) {
      modeReglement2 = null;
    }
    if (modeReglement3 != null && montant3.compareTo(BigDecimal.ZERO) == 0) {
      modeReglement3 = null;
    }
    if (modeReglement4 != null && montant4.compareTo(BigDecimal.ZERO) == 0) {
      modeReglement4 = null;
    }
  }
  
  /**
   * Ajoute le code règlement au premier emplacement libre.
   */
  public int ajouterModeReglement(ModeReglement pModeReglement) {
    if (pModeReglement == null) {
      return 0;
    }
    
    if (modeReglement1 == null || montant1.compareTo(BigDecimal.ZERO) == 0) {
      modeReglement1 = pModeReglement;
      return 1;
    }
    if (modeReglement2 == null || montant2.compareTo(BigDecimal.ZERO) == 0) {
      modeReglement2 = pModeReglement;
      return 2;
    }
    if (modeReglement3 == null || montant3.compareTo(BigDecimal.ZERO) == 0) {
      modeReglement3 = pModeReglement;
      return 3;
    }
    if (modeReglement4 == null || montant4.compareTo(BigDecimal.ZERO) == 0) {
      modeReglement4 = pModeReglement;
      return 4;
    }
    return 0;
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public Reglements clone() {
    Reglements o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (Reglements) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  public EnumEtatReglement getEtatReglement() {
    return etatReglement;
  }
  
  public void setEtatReglement(EnumEtatReglement etatReglement) {
    this.etatReglement = etatReglement;
  }
  
  // -- Accesseurs
  public BigDecimal getMontant1() {
    return montant1;
  }
  
  public void setMontant1(BigDecimal montant1) {
    this.montant1 = montant1;
  }
  
  public BigDecimal getMontant2() {
    return montant2;
  }
  
  public void setMontant2(BigDecimal montant2) {
    this.montant2 = montant2;
  }
  
  public BigDecimal getMontant3() {
    return montant3;
  }
  
  public void setMontant3(BigDecimal montant3) {
    this.montant3 = montant3;
  }
  
  public BigDecimal getMontant4() {
    return montant4;
  }
  
  public void setMontant4(BigDecimal montant4) {
    this.montant4 = montant4;
  }
  
  public Date getDateEcheance1() {
    return dateEcheance1;
  }
  
  public void setDateEcheance1(Date dateEcheance1) {
    this.dateEcheance1 = dateEcheance1;
  }
  
  public Date getDateEcheance2() {
    return dateEcheance2;
  }
  
  public void setDateEcheance2(Date dateEcheance2) {
    this.dateEcheance2 = dateEcheance2;
  }
  
  public Date getDateEcheance3() {
    return dateEcheance3;
  }
  
  public void setDateEcheance3(Date dateEcheance3) {
    this.dateEcheance3 = dateEcheance3;
  }
  
  public Date getDateEcheance4() {
    return dateEcheance4;
  }
  
  public void setDateEcheance4(Date dateEcheance4) {
    this.dateEcheance4 = dateEcheance4;
  }
  
  public ModeReglement getModeReglement1() {
    return modeReglement1;
  }
  
  public void setModeReglement1(ModeReglement modeReglement1) {
    this.modeReglement1 = modeReglement1;
  }
  
  public String getCodeEcheance1() {
    return codeEcheance1;
  }
  
  public void setCodeEcheance1(String codeEcheance1) {
    this.codeEcheance1 = codeEcheance1;
  }
  
  public String getPourcentageReglement1() {
    return pourcentageReglement1;
  }
  
  public void setPourcentageReglement1(String pourcentageReglement1) {
    this.pourcentageReglement1 = pourcentageReglement1;
  }
  
  public ModeReglement getModeReglement2() {
    return modeReglement2;
  }
  
  public void setModeReglement2(ModeReglement modeReglement2) {
    this.modeReglement2 = modeReglement2;
  }
  
  public String getCodeEcheance2() {
    return codeEcheance2;
  }
  
  public void setCodeEcheance2(String codeEcheance2) {
    this.codeEcheance2 = codeEcheance2;
  }
  
  public String getPourcentageReglement2() {
    return pourcentageReglement2;
  }
  
  public void setPourcentageReglement2(String pourcentageReglement2) {
    this.pourcentageReglement2 = pourcentageReglement2;
  }
  
  public ModeReglement getModeReglement3() {
    return modeReglement3;
  }
  
  public void setModeReglement3(ModeReglement modeReglement3) {
    this.modeReglement3 = modeReglement3;
  }
  
  public String getCodeEcheance3() {
    return codeEcheance3;
  }
  
  public void setCodeEcheance3(String codeEcheance3) {
    this.codeEcheance3 = codeEcheance3;
  }
  
  public String getPourcentageReglement3() {
    return pourcentageReglement3;
  }
  
  public void setPourcentageReglement3(String pourcentageReglement3) {
    this.pourcentageReglement3 = pourcentageReglement3;
  }
  
  public ModeReglement getModeReglement4() {
    return modeReglement4;
  }
  
  public void setModeReglement4(ModeReglement modeReglement4) {
    this.modeReglement4 = modeReglement4;
  }
  
  public String getCodeEcheance4() {
    return codeEcheance4;
  }
  
  public void setCodeEcheance4(String codeEcheance4) {
    this.codeEcheance4 = codeEcheance4;
  }
  
  public String getPourcentageReglement4() {
    return pourcentageReglement4;
  }
  
  public void setPourcentageReglement4(String pourcentageReglement4) {
    this.pourcentageReglement4 = pourcentageReglement4;
  }
  
  public int getGroupeRGL1() {
    return groupeRGL1;
  }
  
  public void setGroupeRGL1(int groupeRGL1) {
    this.groupeRGL1 = groupeRGL1;
  }
  
  public int getGroupeRGL2() {
    return groupeRGL2;
  }
  
  public void setGroupeRGL2(int groupeRGL2) {
    this.groupeRGL2 = groupeRGL2;
  }
  
  public int getGroupeRGL3() {
    return groupeRGL3;
  }
  
  public void setGroupeRGL3(int groupeRGL3) {
    this.groupeRGL3 = groupeRGL3;
  }
  
  public int getGroupeRGL4() {
    return groupeRGL4;
  }
  
  public void setGroupeRGL4(int groupeRGL4) {
    this.groupeRGL4 = groupeRGL4;
  }
  
  public int getNombreReglements() {
    return nombreReglements;
  }
  
  public void setNombreReglements(int nombreReglements) {
    this.nombreReglements = nombreReglements;
  }
  
  public int getNumeroLigneReglement() {
    return numeroLigneReglement;
  }
  
  public void setNumeroLigneReglement(int numeroLigneReglement) {
    this.numeroLigneReglement = numeroLigneReglement;
  }
  
  public boolean isAcompteObligatoire() {
    return acompteObligatoire;
  }
  
  public void setAcompteObligatoire(boolean acompteObligatoire) {
    this.acompteObligatoire = acompteObligatoire;
  }
  
  public BigDecimal getMontantAcompteRegle() {
    return montantAcompteRegle;
  }
  
  public void setMontantAcompteRegle(BigDecimal montantAcompteRegle) {
    this.montantAcompteRegle = montantAcompteRegle;
  }
  
  public String getLibelleFacture() {
    return libelleFacture;
  }
  
  public void setLibelleFacture(String libelleFacture) {
    this.libelleFacture = libelleFacture;
    if (this.libelleFacture != null) {
      this.libelleFacture = libelleFacture.trim();
    }
  }
  
  public boolean isModifReglement1() {
    return modifReglement1;
  }
  
  public void setModifReglement1(boolean modifReglement1) {
    this.modifReglement1 = modifReglement1;
  }
  
  public boolean isModifReglement2() {
    return modifReglement2;
  }
  
  public void setModifReglement2(boolean modifReglement2) {
    this.modifReglement2 = modifReglement2;
  }
  
  public boolean isModifReglement3() {
    return modifReglement3;
  }
  
  public void setModifReglement3(boolean modifReglement3) {
    this.modifReglement3 = modifReglement3;
  }
  
  public boolean isModifReglement4() {
    return modifReglement4;
  }
  
  public void setModifReglement4(boolean modifReglement4) {
    this.modifReglement4 = modifReglement4;
  }
  
  public BigDecimal getMontantTTCDocumentOrigine() {
    return montantTTCDocumentOrigine;
  }
  
  public void setMontantTTCDocumentOrigine(BigDecimal montantTTCDocumentOrigine) {
    this.montantTTCDocumentOrigine = montantTTCDocumentOrigine;
  }
  
  public BigDecimal getMontantTTCTotalLie() {
    return montantTTCTotalLie;
  }
  
  public void setMontantTTCTotalLie(BigDecimal montantTTCTotalLie) {
    this.montantTTCTotalLie = montantTTCTotalLie;
  }
  
  public BigDecimal getMontantTTCTotalRegle() {
    return montantTTCTotalRegle;
  }
  
  public void setMontantTTCTotalRegle(BigDecimal montantTTCTotalRegle) {
    this.montantTTCTotalRegle = montantTTCTotalRegle;
  }
  
  public IdAvoir getIdAvoir1() {
    return idAvoir1;
  }
  
  public void setIdAvoir1(IdAvoir pIdAvoir) {
    idAvoir1 = pIdAvoir;
  }
  
  public BigDecimal getMontantAvoir1() {
    return montantAvoir1;
  }
  
  public void setMontantAvoir1(BigDecimal pMontantAvoir) {
    montantAvoir1 = pMontantAvoir;
  }
  
  public IdAvoir getIdAvoir2() {
    return idAvoir2;
  }
  
  public void setIdAvoir2(IdAvoir pIdAvoir) {
    idAvoir2 = pIdAvoir;
  }
  
  public BigDecimal getMontantAvoir2() {
    return montantAvoir2;
  }
  
  public void setMontantAvoir2(BigDecimal pMontantAvoir) {
    montantAvoir2 = pMontantAvoir;
  }
  
  public IdAvoir getIdAvoir3() {
    return idAvoir3;
  }
  
  public void setIdAvoir3(IdAvoir pIdAvoir) {
    idAvoir3 = pIdAvoir;
  }
  
  public BigDecimal getMontantAvoir3() {
    return montantAvoir3;
  }
  
  public void setMontantAvoir3(BigDecimal pMontantAvoir) {
    montantAvoir3 = pMontantAvoir;
  }
  
  public IdAvoir getIdAvoir4() {
    return idAvoir4;
  }
  
  public void setIdAvoir4(IdAvoir pIdAvoir) {
    idAvoir4 = pIdAvoir;
  }
  
  public BigDecimal getMontantAvoir4() {
    return montantAvoir4;
  }
  
  public void setMontantAvoir4(BigDecimal pMontantAvoir) {
    montantAvoir4 = pMontantAvoir;
  }
}
