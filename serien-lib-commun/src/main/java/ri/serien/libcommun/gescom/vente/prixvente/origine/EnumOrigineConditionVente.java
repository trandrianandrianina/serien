/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.origine;

/**
 * Liste des origines possibles d'une condition de ventes.
 */
public enum EnumOrigineConditionVente {
  GROUPE_CNV_STANDARD_ETABLISSEMENT("Groupe CNV standard établissement"),
  CNV_CLIENT_STANDARD("Condition client standard"),
  GROUPE_CNV_STANDARD("Groupe CNV standard"),
  GROUPE_CNV_PROMOTIONNELLE("Groupe CNV Promotionnelle"),
  CLIENT_FACTURE("Client facturé"),
  SAISI("Saisi"),
  CENTRALE("Centrale");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrigineConditionVente(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
