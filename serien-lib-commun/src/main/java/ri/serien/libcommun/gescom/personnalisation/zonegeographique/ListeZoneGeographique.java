/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.zonegeographique;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.CommuneZoneGeographique;
import ri.serien.libcommun.gescom.vente.commune.Commune;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de zones géographiques.
 * 
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de zone géographique. Elle contient toutes les
 * méthodes oeuvrant sur la liste tandis que la classe ZoneGeographique contient les méthodes oeuvrant sur une seule zone géographique.
 */
public class ListeZoneGeographique extends ListeClasseMetier<IdZoneGeographique, ZoneGeographique, ListeZoneGeographique> {
  
  /**
   * Constructeur
   */
  public ListeZoneGeographique() {
  }
  
  // -- Méthodes publiques
  
  /**
   * Charger la liste avec les zones géographiques corespondant aux identifiants fournis.
   * Méthode non utilisée, nécessite de charger complètement les tables, pas optimisé avec le fonctionnement actuel.
   */
  @Override
  public ListeZoneGeographique charger(IdSession pIdSession, List<IdZoneGeographique> pListeId) {
    // return ManagerServiceParametre.chargerListeZoneGeographique(pIdSession, pListeId);
    return null;
  }
  
  /**
   * Charger la liste des zones géographiques d'un établissement.
   */
  @Override
  public ListeZoneGeographique charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CritereZoneGeographique critere = new CritereZoneGeographique();
    critere.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeZoneGeographique(pIdSession, critere);
  }
  
  /**
   * Charge une liste d'IdZoneGeographique à partir de critères de recherche.
   * 
   * @param pIdSession
   * @param pCritere
   * @return
   */
  public List<IdZoneGeographique> chargerListeIdZoneGeographique(IdSession pIdSession, CritereZoneGeographique pCritere) {
    return ManagerServiceParametre.chargerListeIdZoneGeographique(pIdSession, pCritere);
  }
  
  /**
   * Retourner une zone géographique de son code postal et sa ville.
   */
  public ZoneGeographique getZoneGeographiqueParCodePostalEtVille(int pCodePostal, String pVille) {
    if (pCodePostal <= 0 || pVille == null) {
      return null;
    }
    pVille = Commune.normerNomVillePourComparaison(pVille);
    
    // Parcourt de la liste des zones géographiques
    for (ZoneGeographique zoneGeographique : this) {
      if (zoneGeographique == null || zoneGeographique.getListeCommuneZoneGeographique() == null) {
        continue;
      }
      // Parcourt de la liste des communues
      for (CommuneZoneGeographique communeZoneGeographique : zoneGeographique.getListeCommuneZoneGeographique()) {
        // Si le code postal ne correspond pas, cette zone géographique est ignorée
        if (communeZoneGeographique == null || !Constantes.equals(communeZoneGeographique.getId().getCodePostal(), pCodePostal)) {
          continue;
        }
        // Comparaison du nom de la ville
        String villeZoneGeographique = Commune.normerNomVillePourComparaison(communeZoneGeographique.getVille());
        if (villeZoneGeographique.equals(pVille)) {
          return zoneGeographique;
        }
      }
    }
    return null;
  }
  
}
