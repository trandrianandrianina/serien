/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.io.Serializable;
import java.util.Date;

import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

/**
 * Critères de recherche sur les documents de ventes.
 */
public class CritereDocumentVente implements Serializable {
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private EnumTypeCompteClient typeClient = null;
  private IdClient idClient = null;
  private EnumTypeDocumentVente typeDocumentVente = null;
  private boolean sansHistorique = false;
  private int numeroFacture = 0;
  private int numeroDocument = 0;
  private int suffixeDocument = -1;
  private Date dateCreationDebut = null;
  private Date dateCreationFin = null;
  private String codeArticle = null;
  private EnumEtatBonDocumentVente etatBonDocumentVente = null;
  private IdVendeur idVendeur = null;
  private boolean extractionPartielle = false;
  private Chantier chantier = null;
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  private int numeroDocumentDebut = 0;
  private int numeroDocumentFin = 0;
  private int numeroFactureDebut = 0;
  private int numeroFactureFin = 0;
  private boolean isComposantDePlage = false;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialiser les critères de recherches.
   */
  public void initialiser() {
    typeClient = null;
    idClient = null;
    typeDocumentVente = EnumTypeDocumentVente.NON_DEFINI;
    sansHistorique = false;
    numeroDocument = 0;
    numeroFacture = 0;
    chantier = null;
    rechercheGenerique.initialiser();
    idVendeur = null;
  }
  
  /**
   * Retourner la requête de liaison sur les autres tables liées au document de ventes.
   * @param pBaseDeDonnees Base de données concernée.
   * @return La requête de liaison sur les autres tables.
   */
  public String getRequeteFrom(String pBaseDeDonnees) {
    String requete = "";
    
    // Filtrer sur le code article
    if (codeArticle != null && !Constantes.normerTexte(codeArticle).isEmpty()) {
      requete = " left join " + pBaseDeDonnees + ".PGVMLBCM lig "
          + " on (doc.E1ETB = lig.L1ETB and doc.E1NUM = lig.L1NUM and doc.E1SUF = lig.L1SUF) ";
    }
    
    if (chantier != null) {
      requete += " left join " + pBaseDeDonnees + ".PGVMXLIM xli on" + "   xli.XIETB = doc.E1ETB" + "   and xli.XICOD = doc.E1COD"
          + "   and xli.XINUM = doc.E1NUM" + "   and xli.XISUF = doc.E1SUF" + "   and xli.XITYP = '40'";
    }
    
    // Filtrer sur le type de client (comptant ou en compte)
    if (typeClient != null) {
      requete += " inner join " + pBaseDeDonnees + ".PGVMECBM ecli "
          + " on (doc.E1ETB = ecli.EBETB and (doc.E1CLLP = ecli.EBCLI or doc.E1CLFP = ecli.EBCLI)"
          + " and (doc.E1CLLS = ecli.EBLIV or doc.E1CLFS = ecli.EBLIV) ";
      if (typeClient.equals(EnumTypeCompteClient.EN_COMPTE)) {
        requete += " and (ecli.EBIN05 = ' ' or ecli.EBIN05 is null))";
      }
      else if (typeClient.equals(EnumTypeCompteClient.COMPTANT)) {
        requete += " and ecli.EBIN05 = '1')";
      }
    }
    return requete;
  }
  
  /**
   * Retourner la partie WHERE de la requête SQL correspondant aux critères de recherches générales.
   * @param pBaseDeDonnees Base de données concernée.
   * @return La condition sur la requête générale.
   */
  public String getRequeteWhere(String pBaseDeDonnees) {
    String requete = "";
    
    // Filtrer sur l'établissement
    if (getCodeEtablissement() == null) {
      throw new MessageErreurException("Impossible de lister les documents de ventes car l'établissement est invalide.");
    }
    requete += "doc.E1ETB = '" + getCodeEtablissement() + "'";
    
    // Ne pas sélectionner les documents internes (documents entre agences et sièges)
    requete += " and doc.E1IN18 <> 'I'";
    
    // Ne pas sélectionner les documents de SAV (spécifique)
    requete += " and doc.E1IN18 <> 'S'";
    
    // Filtrer sur le type de documents de vente
    requete += " and (doc.E1COD = 'D' or doc.E1COD = 'E')";
    
    // Ne pas avoir les chantiers
    requete += " and doc.E1PFC <> '*CH'";
    
    // Filtrer sur le numéro de document
    // Si c'est un composant de plage, prendre en compte le numéro de document de début et de fin.
    if (isComposantDePlage) {
      if (numeroDocumentDebut > 0) {
        requete += " and doc.E1NUM >= " + Constantes.convertirIntegerEnTexte(numeroDocumentDebut, 6).substring(0, 6);
      }
      if (numeroDocumentFin > 0) {
        requete += " and doc.E1NUM <= " + Constantes.convertirIntegerEnTexte(numeroDocumentFin, 6).substring(0, 6);
      }
      if (numeroDocument > 0) {
        requete += " and doc.E1NUM = " + Constantes.convertirIntegerEnTexte(numeroDocument, 6).substring(0, 6);
      }
    }
    // Sinon prendre en compte le numéro du document si c'est renseigné.
    else {
      if (numeroDocument > 0) {
        requete += " and ((doc.E1NUM = " + Constantes.convertirIntegerEnTexte(numeroDocument, 6).substring(0, 6) + " and doc.E1NFA = 0)"
            + " or (doc.E1NFA = " + Constantes.convertirIntegerEnTexte(numeroFacture, 7) + "))";
      }
    }
    
    // Filtrer sur le suffixe document
    if (suffixeDocument >= 0) {
      requete += " and doc.E1SUF = " + suffixeDocument;
    }
    
    // Ne pas sélectionner les factures avec un numéro de tournée (gestion des coursiers)
    requete += " and doc.E1NTO = 0";
    
    // Filtrer sur le magasin
    if (idMagasin != null) {
      requete += " and doc.E1MAG = '" + idMagasin.getCode() + "'";
    }
    
    // Filtrer sur le client (livré ou facturé)
    if (idClient != null) {
      requete += " and (doc.E1CLLP = " + idClient.getNumero() + " or doc.E1CLFP = " + idClient.getNumero() + ")";
      requete += " and (doc.E1CLLS = " + idClient.getSuffixe() + " or doc.E1CLFS = " + idClient.getSuffixe() + ")";
    }
    
    // Filtrer sur le code du vendeur
    if (idVendeur != null) {
      requete += " and doc.E1VDE = '" + idVendeur.getCode() + "'";
    }
    
    // Filtrer sur la date de création début
    if (dateCreationDebut != null) {
      requete += " and doc.E1CRE >= " + ConvertDate.dateToDb2(dateCreationDebut);
    }
    
    // Filtrer sur la date de création fin
    if (dateCreationFin != null) {
      requete += " and doc.E1CRE <= " + ConvertDate.dateToDb2(dateCreationFin);
    }
    
    // Flitrer sur chantier
    if (chantier != null) {
      requete += " and xli.XILIB = " + chantier.getId().getNumero();
    }
    
    // Ne pas afficher les documents historisés
    if (sansHistorique) {
      requete += " and  1 = (CASE ";
      requete += " WHEN (doc.E1COD = 'D' and doc.E1TDV <> 6) THEN 1";
      requete += " WHEN (doc.E1COD = 'E' and doc.E1EXP = 0 and doc.E1TDV <> 6 and doc.E1NFA=0) THEN 1";
      requete += " WHEN (doc.E1COD = 'E' and doc.E1EXP > 0 and doc.E1NFA=0) THEN 1";
      requete += " WHEN (doc.E1COD = 'E' and E1NFA <> 9999999 and E1FAC > 0 and doc.E1NFA > 0 and (select ext.IXREF1 from "
          + pBaseDeDonnees + ".PGVMIDXM ext where ext.IXETB = '" + getCodeEtablissement() + "' and ext.IXTYP = 'cf'"
          + " and decimal(substr(ext.IXREF1, 1, 6)) = doc.E1CLFP and decimal(substr(ext.IXREF2, 1, 7)) = doc.E1NFA"
          + " and ext.IXCODG <> 0) <> ' ') THEN 1";
      requete += " ELSE 0 END)";
    }
    
    // Filtrer sur la référence courte ou sur la référence longue
    if (!rechercheGenerique.getTexteFinal().isEmpty()) {
      requete +=
          " and (TRANSLATE(doc.E1NCC,'AAAAAABCCDEEEEEEEFGHIIIIIJKLMNOOOOOPQRSTUUUUUUVWXYZ','aàâÂäÄbcçdeéèêÊëËfghiîÎïÏjklmnoôÔöÖpqrstuùûÛüÜvwxyz')"
              + " like " + "'%" + rechercheGenerique.getTexteFinal().toUpperCase() + "%'"
              + " or TRANSLATE(doc.E1RCC,'AAAAAABCCDEEEEEEEFGHIIIIIJKLMNOOOOOPQRSTUUUUUUVWXYZ','aàâÂäÄbcçdeéèêÊëËfghiîÎïÏjklmnoôÔöÖpqrstuùûÛüÜvwxyz') like "
              + "'%" + rechercheGenerique.getTexteFinal().toUpperCase() + "%')";
    }
    
    // Filtrer sur le code article
    if (!Constantes.normerTexte(codeArticle).isEmpty()) {
      requete += " and (lig.L1ART = '" + codeArticle
          + "' and lig.L1CEX <> 'A' and (UPPER(lig.L1COD) = doc.E1COD OR lig.L1COD = doc.E1COD) and lig.L1ERL <> '*')";
    }
    
    return requete;
  }
  
  /**
   * Retourner la partie WHERE de la requête SQL correspondant aux critères de recherche pour les devis.<br>
   * Remarque : le code de cette méthode doit être cohérent avec celui de la méthode
   * DocumentVenteBase.deduireTypeDocumentVente().<br>
   * On considère comme devis tout document de ventes avec un entête de type 'D' et qui n'est pas un chantier.
   * @param pBaseDeDonnees Base de données concernée.
   * @return La condition sur la requête des devis.
   */
  public String getRequeteWherePourDevis(String pBaseDeDonnees) {
    String requete = getRequeteWhere(pBaseDeDonnees);
    
    // Filtrer sur le type de documents de vente
    requete += " and doc.E1COD = 'D'";
    
    // Ne pas avoir les chantiers
    requete += " and doc.E1PFC <> '*CH'";
    
    // Filtrer sur le numéro de document
    // Si c'est un composant de plage, prendre en compte le numéro de document de début et de fin.
    if (isComposantDePlage) {
      if (numeroDocumentDebut > 0) {
        requete += " and doc.E1NUM >= " + Constantes.convertirIntegerEnTexte(numeroDocumentDebut, 6).substring(0, 6);
      }
      if (numeroDocumentFin > 0) {
        requete += " and doc.E1NUM <= " + Constantes.convertirIntegerEnTexte(numeroDocumentFin, 6).substring(0, 6);
      }
    }
    // Prendre en compte le numéro du devis si c'est renseigné.
    if (numeroDocument > 0) {
      requete += " and doc.E1NUM = " + Constantes.convertirIntegerEnTexte(numeroDocument, 6).substring(0, 6);
    }
    
    // Filtrer sur le suffixe document
    if (suffixeDocument >= 0) {
      requete += " and doc.E1SUF = " + suffixeDocument;
    }
    
    // Ne pas afficher les devis clôturés si l'historique n'est pas demandé
    // Un devis clôturé a le champ E1TDV = 6.
    if (sansHistorique) {
      requete += " and doc.E1TDV <> 6";
    }
    
    // Filtrer sur le code du vendeur
    if (idVendeur != null) {
      requete += " and doc.E1VDE = '" + idVendeur.getCode() + "'";
    }
    
    // Filtrer sur l'état d'extraction
    if (extractionPartielle) {
      requete += " and substr(digits(ext.IXCODG),1,1) = '" + EnumEtatExtractionDocumentVente.PARTIELLE.getCode() + "'";
    }
    
    return requete;
  }
  
  /**
   * Retourner la partie WHERE de la requête SQL correspondant aux critères de recherche pour les commandes.<br>
   * Remarque : le code de cette méthode doit être cohérent avec celui de la méthode
   * DocumentVenteBase.deduireTypeDocumentVente().<br>
   * On considère comme commande tout document de ventes avec un entête de type 'E' qui n'est ni un bon ni une facture.
   * Un document de ventes avec le statut "En attente" n'a aucune date de renseigné et est donc considéré comme une
   * commande.
   * @param pBaseDeDonnees Base de données concernée.
   * @return La condition sur le requête des commandes.
   */
  public String getRequeteWherePourCommande(String pBaseDeDonnees) {
    String requete = getRequeteWhere(pBaseDeDonnees);
    
    // Filtrer sur le type de documents de vente
    requete += " and doc.E1COD = 'E'";
    
    // Les commandes n'ont pas de date d'expédition (ce sont des bons dans ce cas)
    requete += " and doc.E1EXP = 0";
    
    // Les commandes n'ont pas de date de facture (ce sont des factures directes dans ce cas)
    requete += " and doc.E1FAC = 0";
    
    // Filtrer sur le numéro de document
    // Si c'est un composant de plage, prendre en compte le numéro de document de début et de fin.
    if (isComposantDePlage) {
      if (numeroDocumentDebut > 0) {
        requete += " and doc.E1NUM >= " + Constantes.convertirIntegerEnTexte(numeroDocumentDebut, 6).substring(0, 6);
      }
      if (numeroDocumentFin > 0) {
        requete += " and doc.E1NUM <= " + Constantes.convertirIntegerEnTexte(numeroDocumentFin, 6).substring(0, 6);
      }
    }
    // Prendre en compte le numéro de la commande si c'est renseigné.
    if (numeroDocument > 0) {
      requete += "  and doc.E1NUM = " + Constantes.convertirIntegerEnTexte(numeroDocument, 6).substring(0, 6);
    }
    
    // Filtrer sur le suffixe document
    if (suffixeDocument >= 0) {
      requete += " and doc.E1SUF = " + suffixeDocument;
    }
    
    // Ne pas afficher les commandes totalement extraites si l'historique n'est pas demandé
    // Une commande totalement extraite a le champ E1TDV = 6.
    if (sansHistorique) {
      requete += " and doc.E1TDV <> 6";
    }
    
    // Filtrer sur le code du vendeur
    if (idVendeur != null) {
      requete += " and doc.E1VDE = '" + idVendeur.getCode() + "'";
    }
    
    // Filtrer sur l'état d'extraction
    if (extractionPartielle) {
      requete += " and substr(digits(ext.IXCODG),1,1) = '" + EnumEtatExtractionDocumentVente.PARTIELLE.getCode() + "'";
    }
    
    return requete;
  }
  
  /**
   * Retourner la partie WHERE de la requête SQL correspondant aux critères de recherche pour les bons.<br>
   * Remarque : le code de cette méthode doit être cohérent avec celui de la méthode
   * DocumentVenteBase.deduireTypeDocumentVente().<br>
   * On considère comme bon tout document de ventes avec un entête de type 'E' qui a une date d'expédition.
   * @param pBaseDeDonnees Base de données concernée.
   * @return La condition sur la requête des bons.
   */
  public String getRequeteWherePourBon(String pBaseDeDonnees) {
    String requete = getRequeteWhere(pBaseDeDonnees);
    
    // Filtrer sur le type de documents de vente
    requete += " and doc.E1COD = 'E'";
    
    // Seuls les documents de ventes avec une date d'expédition renseignée sont des bons
    // Noter qu'un bon peut également être simultanément une facture si E1FAC > 0.
    requete += " and doc.E1EXP > 0";
    
    // Filtrer sur le numéro de document
    // Si c'est un composant de plage, prendre en compte le numéro de document de début et de fin.
    if (isComposantDePlage) {
      if (numeroDocumentDebut > 0) {
        requete += " and doc.E1NUM >= " + Constantes.convertirIntegerEnTexte(numeroDocumentDebut, 6).substring(0, 6);
      }
      if (numeroDocumentFin > 0) {
        requete += " and doc.E1NUM <= " + Constantes.convertirIntegerEnTexte(numeroDocumentFin, 6).substring(0, 6);
      }
    }
    // Prendre en compte le numéro du bon si c'est renseigné.
    if (numeroDocument > 0) {
      requete += " and doc.E1NUM = " + Constantes.convertirIntegerEnTexte(numeroDocument, 6).substring(0, 6);
    }
    
    // Filtrer sur le suffixe document
    if (suffixeDocument >= 0) {
      requete += " and doc.E1SUF = " + suffixeDocument;
    }
    
    // Ne pas afficher les bons facturés si l'historique n'est pas demandé
    // Un bon facturé a le champ E1NFA <> 0.
    if (sansHistorique) {
      requete += " and doc.E1NFA=0";
    }
    
    // Filtrer sur le code du vendeur
    if (idVendeur != null) {
      requete += " and doc.E1VDE = '" + idVendeur.getCode() + "'";
    }
    
    return requete;
  }
  
  /**
   * Retourner la partie WHERE de la requête SQL correspondant aux critères de recherche pour les factures.<br>
   * Remarque : le code de cette méthode doit être cohérent avec celui de la méthode
   * DocumentVenteBase.deduireTypeDocumentVente().<br>
   * On considère comme facture tout document de ventes possédant un numéro de facture et une date de facture.
   * @param pBaseDeDonnees Base de données concernée.
   * @return La condition sur la requête des factures.
   */
  public String getRequeteWherePourFacture(String pBaseDeDonnees) {
    String requete = getRequeteWhere(pBaseDeDonnees);
    
    // Filtrer sur le numéro de document
    // Si c'est un composant de plage, prendre en compte le numéro de document de début et de fin.
    if (isComposantDePlage) {
      if (numeroFactureDebut > 0) {
        requete += " and E1NFA >= " + Constantes.convertirIntegerEnTexte(numeroFactureDebut, 7);
      }
      if (numeroFactureFin > 0) {
        requete += " and E1NFA <= " + Constantes.convertirIntegerEnTexte(numeroFactureFin, 7);
      }
    }
    // Pendre en compte le numéro de la facture si c'est renseigné.
    if (numeroFacture > 0) {
      requete += " and E1NFA = " + Constantes.convertirIntegerEnTexte(numeroFacture, 7);
    }
    
    // Filtrer sur les documents possédant un numéro de facture
    requete += " and E1NFA > 0";
    
    // Filtrer les documents avec le numéro de facture 9999999 qui signifie que le document de ventes n'est pas facturable
    requete += " and E1NFA <> " + IdDocumentVente.NUMERO_FACTURE_NON_FACTURABLE;
    
    // Seuls les documents de ventes avec une date de facturation renseignée sont des factures
    requete += " and E1FAC > 0";
    
    // Filtrage sur l'état de la facture
    if (etatBonDocumentVente != null) {
      requete += " and E1ETA = " + etatBonDocumentVente.getCode().intValue();
    }
    
    // Ne pas afficher les factures entièrement réglées si l'historique n'est pas demandé.
    // Une facture non entièrement régléé a une information de type 'cf' dans la table PGVMIDXM
    if (sansHistorique) {
      requete += " and exists(select 1 from " + pBaseDeDonnees + ".PGVMIDXM ext where ext.IXETB = '" + getCodeEtablissement()
          + "' and ext.IXTYP = 'cf'"
          + " and decimal(substr(ext.IXREF1, 1, 6)) = doc.E1CLFP and decimal(substr(ext.IXREF2, 1, 7)) = doc.E1NFA"
          + " and ext.IXCODG <> 0)";
    }
    
    // Filtrer sur le code du vendeur
    if (idVendeur != null) {
      requete += " and doc.E1VDE = '" + idVendeur.getCode() + "'";
    }
    
    return requete;
  }
  
  /**
   * Retourner la partie ORDER BY de la requête SQL correspond aux critères de recherche générale.
   * @return La partie ORDER BY de la requête pour la recherche générale.
   */
  public String getRequeteOrderBy() {
    return "doc.E1CRE DESC, doc.E1NUM DESC";
  }
  
  /**
   * Retourner la partie ORDER BY de la requête SQL pour les composants de plage.
   * @return Texte order by pour les composants de plage.
   */
  public String getRequeteOrderByPourComposantPlage() {
    if (typeDocumentVente != null && Constantes.equals(typeDocumentVente, EnumTypeDocumentVente.FACTURE)) {
      // Pour les factures.
      return " ORDER BY doc.E1NFA DESC";
    }
    // Pour les autres documents.
    else {
      return " ORDER BY doc.E1NUM DESC";
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner le filtre sur l'établissement.
   * @return L'identifiant de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Retourner le code de l'établissement servant de critère de recherche.
   * @return Le code de l'établissement.
   */
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourner le filtre sur l'identifiant du magasin.
   * @return L'identifiant du magasin.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier le filtre sur l'identifiant du magasin.
   * @param pIdMagasin Identifiant du magasin.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  /**
   * Retourner la recherche générique.
   * @return L'objet recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Retourner le texte de la recherche générique.
   * @return Le texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   * @param pTexteRecherche Nouveau texte de la recherche.
   */
  public void setTexteRechercheGenerique(String pTexteRecherche) {
    rechercheGenerique.setTexte(pTexteRecherche);
  }
  
  /**
   * Retourner le filtre sur le type de document de ventes.
   * @return Le type de document de ventes.
   */
  public EnumTypeDocumentVente getTypeDocumentVente() {
    return typeDocumentVente;
  }
  
  /**
   * Modifier le filtre sur le type de document de ventes.
   * @param pTypeDocumentVente Type de document de ventes.
   */
  public void setTypeDocumentVente(EnumTypeDocumentVente pTypeDocumentVente) {
    typeDocumentVente = pTypeDocumentVente;
  }
  
  /**
   * Retourne l'indicatif d'inclusion de documents historisés.
   * @return
   *         true si on n'inclut pas les documents de ventes historisés, false sinon.
   */
  public boolean isSansHistorique() {
    return sansHistorique;
  }
  
  /**
   * Modifier le filtre sur les documents de ventes historisés.<br>
   * Par défaut, les critères de recherche retournent tous les documents de ventes. Activer ce filtre restreint la recherche aux documents
   * de ventes non historisés.
   * @param pSansHistorique Indicatif d'inclusion des documents historisés.
   */
  public void setSansHistorique(boolean pSansHistorique) {
    sansHistorique = pSansHistorique;
  }
  
  /**
   * Retourner le filtre sur le numéro de facture.
   * @return Le numéro de facture.
   */
  public int getNumeroFacture() {
    return numeroFacture;
  }
  
  /**
   * Modifier le filtre sur le numéro de facture.
   * @param pNumeroFacture Numéro de facture.
   */
  public void setNumeroFacture(int pNumeroFacture) {
    numeroFacture = pNumeroFacture;
  }
  
  /**
   * Retourner le filtre sur le numéro de document.
   * @return Le numéro de document.
   */
  public int getNumeroDocument() {
    return numeroDocument;
  }
  
  /**
   * Modifier le filtre sur le numéro de document.
   * @param pNumeroDocument Numéro de document.
   */
  public void setNumeroDocument(int pNumeroDocument) {
    numeroDocument = pNumeroDocument;
  }
  
  /**
   * Retourner le filtre sur le suffixe du document.
   * @return Le suffixe du document.
   */
  public int getSuffixeDocument() {
    return suffixeDocument;
  }
  
  /**
   * Modifier le filtre sur le suffixe du document.
   * @param pSuffixeDocument Suffixe du document.
   */
  public void setSuffixeDocument(int pSuffixeDocument) {
    suffixeDocument = pSuffixeDocument;
  }
  
  /**
   * Retourner le filtre sur la date de début de la période de création du document.
   * @return La date de création de début.
   */
  public Date getDateCreationDebut() {
    return dateCreationDebut;
  }
  
  /**
   * Modifier le filtre sur la date de début de la période de création du document.
   * @param pDateCreationDebut Date de création de début.
   */
  public void setDateCreationDebut(Date pDateCreationDebut) {
    dateCreationDebut = pDateCreationDebut;
  }
  
  /**
   * Retourner le filtre sur la date de fin de la période de création du document.
   * @return La date de création de fin.
   */
  public Date getDateCreationFin() {
    return dateCreationFin;
  }
  
  /**
   * Modifier le filtre sur la date de fin de la période de création du document.
   * @param pDateCreationFin Date de création de fin.
   */
  public void setDateCreationFin(Date pDateCreationFin) {
    dateCreationFin = pDateCreationFin;
  }
  
  /**
   * Retourner le filtre sur le code article.
   * @return Le code article.
   */
  public String getCodeArticle() {
    return codeArticle;
  }
  
  /**
   * Modifier le filtre sur le code article.
   * @param pCodeArticle Code article.
   */
  public void setCodeArticle(String pCodeArticle) {
    if (pCodeArticle == null) {
      codeArticle = "";
    }
    else {
      codeArticle = pCodeArticle;
    }
  }
  
  /**
   * Retourner le filtre de le type de client.
   * @return Le type de client.
   */
  public EnumTypeCompteClient getTypeClient() {
    return typeClient;
  }
  
  /**
   * Modifier le filtre de le type de client.
   * @param pTypeClient Type de client.
   */
  public void setTypeClient(EnumTypeCompteClient pTypeClient) {
    typeClient = pTypeClient;
  }
  
  /**
   * Retourner le filtre sur le cLient.
   * @return L'identifiant du client.
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Modifier le filtre sur le client.
   * @param pIdClient Identifiant du client.
   */
  public void setIdClient(IdClient pIdClient) {
    idClient = pIdClient;
    if (idEtablissement == null && pIdClient != null) {
      idEtablissement = pIdClient.getIdEtablissement();
    }
  }
  
  /**
   * Retourner le filtre sur l'état du bon.
   * @return L'état du bon.
   */
  public EnumEtatBonDocumentVente getEtatBonDocumentVente() {
    return etatBonDocumentVente;
  }
  
  /**
   * Modifier le filtre sur l'état du bon.
   * @param pEtatBonDocumentVente Etat du bon.
   */
  public void setEtatBonDocumentVente(EnumEtatBonDocumentVente pEtatBonDocumentVente) {
    etatBonDocumentVente = pEtatBonDocumentVente;
  }
  
  /**
   * Retourner le filtre sur l'identifiant du vendeur.
   * @return L'identifiant du vendeur.
   */
  public IdVendeur getIdVendeur() {
    return idVendeur;
  }
  
  /**
   * Modifier le filtre sur l'identifiant du vendeur.
   * @param pIdVendeur Identifiant du vendeur.
   */
  public void setIdVendeur(IdVendeur pIdVendeur) {
    idVendeur = pIdVendeur;
  }
  
  /**
   * Retourner le filtre sur l'extraction partielle.
   * @return Le filtre d'extraction partielle
   */
  public boolean isExtractionPartielle() {
    return extractionPartielle;
  }
  
  /**
   * Modifier le filtre sur l'extraction partielle.
   * @param pExtractionPartielle Indicatif d'extraction partielle.
   */
  public void setExtractionPartielle(boolean pExtractionPartielle) {
    extractionPartielle = pExtractionPartielle;
  }
  
  /**
   * Retourner le filtre sur le chantier.
   * @return Le chantier.
   */
  public Chantier getChantier() {
    return chantier;
  }
  
  /**
   * Modifier le filtre sur le chantier.
   * @param pChantier Chantier.
   */
  public void setChantier(Chantier pChantier) {
    chantier = pChantier;
  }
  
  /**
   * Retourner le numéro de document de début.
   * @return Le numéro de document de début.
   */
  public int getNumeroDocumentDebut() {
    return numeroDocumentDebut;
  }
  
  /**
   * Définir le numéro de document de début.
   * @param pNumeroDocumentDebut Numéro de document de début.
   */
  public void setNumeroDocumentDebut(int pNumeroDocumentDebut) {
    numeroDocumentDebut = pNumeroDocumentDebut;
  }
  
  /**
   * Retourner le numéro de document de fin.
   * @return Le numéro de document de fin.
   */
  public int getNumeroDocumentFin() {
    return numeroDocumentFin;
  }
  
  /**
   * Définir le numéro de document de fin.
   * @param pNumeroDocumentFin Numéro de document de fin.
   */
  public void setNumeroDocumentFin(int pNumeroDocumentFin) {
    numeroDocumentFin = pNumeroDocumentFin;
  }
  
  /**
   * Indiquer un composant de plage.
   * @return
   *         true si c'est un composant de plage, false sinon.
   */
  public boolean isComposantDePlage() {
    return isComposantDePlage;
  }
  
  /**
   * Définir l'indicateur de composant de plage.
   * @param pIsComposantDePlage Indicateur de composant de plage.
   */
  public void setComposantDePlage(boolean pIsComposantDePlage) {
    isComposantDePlage = pIsComposantDePlage;
  }
  
  /**
   * Retourner le numéro facture de début.
   * @return Le numéro facture de début.
   */
  public int getNumeroFactureDebut() {
    return numeroFactureDebut;
  }
  
  /**
   * Définir le numéro facture de début.
   * @param pNumeroFactureDebut Numéro facture de début.
   */
  public void setNumeroFactureDebut(int pNumeroFactureDebut) {
    numeroFactureDebut = pNumeroFactureDebut;
  }
  
  /**
   * Retourner le numéro facture de fin.
   * @return Le numéro facture de fin.
   */
  public int getNumeroFactureFin() {
    return numeroFactureFin;
  }
  
  /**
   * Définir le numéro facture de fin.
   * @param pNumeroFactureFin Numéro facture de fin.
   */
  public void setNumeroFactureFin(int pNumeroFactureFin) {
    numeroFactureFin = pNumeroFactureFin;
  }
  
}
