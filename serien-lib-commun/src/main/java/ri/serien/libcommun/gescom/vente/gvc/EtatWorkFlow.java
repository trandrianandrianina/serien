/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc;

import java.io.Serializable;

public class EtatWorkFlow implements Serializable {
  private EnumEtatWorkFlow codeEtat = EnumEtatWorkFlow.NOUVEAU;
  private String libelleEtat = null;
  
  /**
   * Constructeur avec état en paramètre
   */
  public EtatWorkFlow(EnumEtatWorkFlow pCode) {
    codeEtat = pCode;
    attribuerLeLibelle();
  }
  
  /**
   * Constructeur avec état par défaut
   */
  public EtatWorkFlow() {
    codeEtat = EnumEtatWorkFlow.NOUVEAU;
    attribuerLeLibelle();
  }
  
  @Override
  public String toString() {
    return libelleEtat;
  }
  
  @Override
  public boolean equals(Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof EtatWorkFlow)) {
      return false;
    }
    EtatWorkFlow etat = (EtatWorkFlow) object;
    return codeEtat == etat.codeEtat && libelleEtat == etat.libelleEtat;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + codeEtat.hashCode();
    cle = 37 * cle + libelleEtat.hashCode();
    return cle;
  }
  
  /**
   * On attribue un libellé à l'état du workflow transmis
   */
  private void attribuerLeLibelle() {
    libelleEtat = codeEtat.getLibelle();
  }
  
  public EnumEtatWorkFlow getCodeEtat() {
    return codeEtat;
  }
  
  public String getLibelleEtat() {
    return libelleEtat;
  }
}
