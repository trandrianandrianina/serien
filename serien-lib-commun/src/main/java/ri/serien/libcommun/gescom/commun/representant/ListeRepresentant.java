/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.representant;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceRepresentant;

/**
 * Liste de représentants.
 */
public class ListeRepresentant extends ListeClasseMetier<IdRepresentant, Representant, ListeRepresentant> {
  /**
   * Constructeur.
   */
  public ListeRepresentant() {
  }
  
  /**
   * Charge tous les vendeurs de l'établissement donnée.
   */
  @Override
  public ListeRepresentant charger(IdSession pIdSession, List<IdRepresentant> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charge tous les representants de l'établissement donnée.
   */
  @Override
  public ListeRepresentant charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas défini.");
    }
    
    CriteresRechercheRepresentant criteres = new CriteresRechercheRepresentant();
    criteres.setTypeRecherche(CriteresRechercheRepresentant.RECHERCHE_COMPTOIR);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    // En attendant de gérer la pagination dans cet écran
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceRepresentant.chargerListeRepresentant(pIdSession, criteres);
  }
  
  /**
   * Retourner un représentant de la liste à partir de son nom.
   */
  public Representant retournerRepresentantParNom(String pNom) {
    if (pNom == null) {
      return null;
    }
    for (Representant representant : this) {
      if (representant != null && representant.getNom().equals(pNom)) {
        return representant;
      }
    }
    
    return null;
  }
  
}
