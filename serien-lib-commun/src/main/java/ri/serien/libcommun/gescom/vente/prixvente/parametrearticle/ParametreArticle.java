/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrearticle;

import java.io.Serializable;
import java.math.BigDecimal;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.referencetarif.IdReferenceTarif;
import ri.serien.libcommun.gescom.vente.prixvente.MapQuantiteRattachement;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.EnumTypeRattachementArticle;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

/**
 * Classe regroupant tous les paramètres de l' article pour le calcul du prix de vente.
 */
public class ParametreArticle implements Serializable {
  // Constantes
  public static final int CODE_NON_REMISABLE = 99;
  
  // Description
  private IdArticle idArticle = null;
  private Integer numeroTVAEtablissement = null;
  private Boolean articleCommentaire = null;
  
  // Données pour CalculPrixVente
  private String famille = null;
  private String sousFamille = null;
  private IdFournisseur idFournisseur = null;
  private IdReferenceTarif idReferenceTarif = null;
  private String codeRegroupementAchat = null;
  private String codeRattachementCNV = null;
  private String codeRattachementCNV1 = null;
  private String codeRattachementCNV2 = null;
  private String codeRattachementCNV3 = null;
  
  // Données pour FormulePrixVente
  private String codeUniteVente = null;
  private Boolean remisable = null;
  private Integer tauxRemiseMaximum = null;
  private EnumOriginePump originePump = null;
  private BigDecimal pump = null;
  private EnumOriginePrv originePrv = null;
  private EnumTypePrv typePrv = null;
  private BigDecimal prv = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialise la map des rattachement/quantite à partir de l'article dans le but d'ajouter les quantités avec les données des lignes de
   * ventes du document de ventes.
   */
  public MapQuantiteRattachement initialiserQuantiteRattachement() {
    MapQuantiteRattachement mapQuantiteParRattachement = new MapQuantiteRattachement();
    
    // Rattachement article
    mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.ARTICLE, getCodeArticle());
    
    // Rattachement fournisseur
    if (idFournisseur != null) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.REGROUPEMENT_FOURNISSEUR,
          idFournisseur.getIndicatifCourt());
    }
    
    // Rattachement regroupement achat article
    if (!Constantes.normerTexte(codeRegroupementAchat).isEmpty()) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.REGROUPEMENT_ACHAT, codeRegroupementAchat);
    }
    
    // Rattachement regroupement CNV article
    if (!Constantes.normerTexte(codeRattachementCNV).isEmpty()) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.REGROUPEMENT_CNV, codeRattachementCNV);
    }
    if (!Constantes.normerTexte(codeRattachementCNV1).isEmpty()) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.REGROUPEMENT_CNV, codeRattachementCNV1);
    }
    if (!Constantes.normerTexte(codeRattachementCNV2).isEmpty()) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.REGROUPEMENT_CNV, codeRattachementCNV2);
    }
    if (!Constantes.normerTexte(codeRattachementCNV3).isEmpty()) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.REGROUPEMENT_CNV, codeRattachementCNV3);
    }
    
    // Rattachement référence tarif article
    if (idReferenceTarif != null) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.TARIF, idReferenceTarif.getCode());
    }
    
    // Rattachement sous famille
    if (!Constantes.normerTexte(sousFamille).isEmpty()) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.SOUS_FAMILLE, sousFamille);
    }
    
    // Rattachement famille
    if (!Constantes.normerTexte(famille).isEmpty()) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.FAMILLE, famille);
    }
    
    // Rattachement sous famille
    String groupe = getGroupe();
    if (groupe != null) {
      mapQuantiteParRattachement.creerNouveauRattachement(EnumTypeRattachementArticle.GROUPE, groupe);
    }
    
    return mapQuantiteParRattachement;
  }
  
  /**
   * Met à jour les quantités des rattachements avec la quantité de l'article.
   * 
   * @param pQuantite
   * @param pMapQuantiteParRattachement
   */
  public void ajouterQuantitePourRattachement(BigDecimal pQuantiteArticle, MapQuantiteRattachement pMapQuantiteParRattachement) {
    // Contrôle que la quantité soit valide
    if (pQuantiteArticle == null) {
      Trace.alerte("La quantité est invalide.");
      return;
    }
    if (pMapQuantiteParRattachement == null) {
      Trace.alerte("La map quantité par rattachement est invalide.");
      return;
    }
    
    // Rattachement article
    pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.ARTICLE, getCodeArticle(), pQuantiteArticle);
    // Rattachement fournisseur
    if (getIdFournisseur() != null) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.REGROUPEMENT_FOURNISSEUR,
          getIdFournisseur().getIndicatifCourt(), pQuantiteArticle);
    }
    // Rattachement regroupement achat article
    if (!Constantes.normerTexte(getCodeRegroupementAchat()).isEmpty()) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.REGROUPEMENT_ACHAT, getCodeRegroupementAchat(),
          pQuantiteArticle);
    }
    // Rattachement regroupements CNV article
    if (!Constantes.normerTexte(getCodeRattachementCNV()).isEmpty()) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.REGROUPEMENT_CNV, getCodeRattachementCNV(),
          pQuantiteArticle);
    }
    if (!Constantes.normerTexte(getCodeRattachementCNV1()).isEmpty()) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.REGROUPEMENT_CNV, getCodeRattachementCNV1(),
          pQuantiteArticle);
    }
    if (!Constantes.normerTexte(getCodeRattachementCNV2()).isEmpty()) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.REGROUPEMENT_CNV, getCodeRattachementCNV2(),
          pQuantiteArticle);
    }
    if (!Constantes.normerTexte(getCodeRattachementCNV3()).isEmpty()) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.REGROUPEMENT_CNV, getCodeRattachementCNV3(),
          pQuantiteArticle);
    }
    // Rattachement référence tarif article
    if (idReferenceTarif != null) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.TARIF, idReferenceTarif.getCode(), pQuantiteArticle);
    }
    // Rattachement sous famille
    if (!Constantes.normerTexte(getSousFamille()).isEmpty()) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.SOUS_FAMILLE, getSousFamille(), pQuantiteArticle);
    }
    // Rattachement famille
    if (!Constantes.normerTexte(getFamille()).isEmpty()) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.FAMILLE, getFamille(), pQuantiteArticle);
    }
    // Rattachement groupe
    if (getGroupe() != null) {
      pMapQuantiteParRattachement.ajouterQuantite(EnumTypeRattachementArticle.GROUPE, getGroupe(), pQuantiteArticle);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs données générales
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'identifiant de l'article.
   * @return Id article.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Modifier l'identifiant de l'article.
   * @param pIdArticle Id article.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  /**
   * Retourner le code de l'article (à partir de l'identifiant).
   * @return Code article.
   */
  public String getCodeArticle() {
    if (idArticle == null) {
      return null;
    }
    return idArticle.getCodeArticle();
  }
  
  /**
   * Retourne le numéro de la colonne du taux de TVA de la DG.
   * 
   * @return
   */
  public Integer getNumeroTVAEtablissement() {
    return numeroTVAEtablissement;
  }
  
  /**
   * Initialise le numéro de la colonne du taux de TVA de la DG.
   * 
   * @param pNumeroTVAEtablissement
   */
  public void setNumeroTVAEtablissement(Integer pNumeroTVAEtablissement) {
    numeroTVAEtablissement = pNumeroTVAEtablissement;
  }
  
  /**
   * Retourner si l'article est un article commentaire.
   * @return true=article commentaire, false=sinon.
   */
  public boolean isArticleCommentaire() {
    if (articleCommentaire != null && articleCommentaire) {
      return true;
    }
    return false;
  }
  
  /**
   * Modifier si l'article est un commentaire.
   * @param pArticleCommentaire true=article commentaire, false=sinon.
   */
  public void setArticleCommentaire(Boolean pArticleCommentaire) {
    articleCommentaire = pArticleCommentaire;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs données rattachement (CalculPrixVente)
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le groupe de l'article.
   * @return Code groupe.
   */
  public String getGroupe() {
    if (Constantes.normerTexte(famille).isEmpty()) {
      return null;
    }
    return "" + famille.charAt(0);
  }
  
  /**
   * Retourner la famille de l'article.
   * @return Code famille.
   */
  public String getFamille() {
    return famille;
  }
  
  /**
   * Modifier la famille de l'article.
   * @param pFamille Famille.
   */
  public void setFamille(String pFamille) {
    famille = pFamille;
  }
  
  /**
   * Retourner la sous-famille de l'article.
   * @return Sous-famille.
   */
  public String getSousFamille() {
    return sousFamille;
  }
  
  /**
   * Modifier la sous-famille de l'article.
   * @param pSousFamille Sous-famille.
   */
  public void setSousFamille(String pSousFamille) {
    sousFamille = pSousFamille;
  }
  
  /**
   * Retourner l'identifiant du fournisseur de l'article.
   * @return Identifiant fournisseur
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Initialise l'identifiant fournisseur.
   * 
   * @param pIdentifiantFournisseur
   */
  public void setIdFournisseur(IdFournisseur pIdentifiantFournisseur) {
    idFournisseur = pIdentifiantFournisseur;
  }
  
  /**
   * Retourner l'identifiant de la référence tarif de l'article.
   * La référence tarif de l'article permet de qualifier le tarif associé à l'article.
   * @return Identifiant référence tarif.
   */
  public IdReferenceTarif getIdReferenceTarif() {
    return idReferenceTarif;
  }
  
  /**
   * Modifier l'identifiant de la référence tarif de l'article.
   * @param pIdReferenceTarif Identifiant référence tarif.
   */
  public void setIdReferenceTarif(IdReferenceTarif pIdReferenceTarif) {
    idReferenceTarif = pIdReferenceTarif;
  }
  
  /**
   * Retourner le code du regroupement d'achats de l'article.
   * @return Code regroupement d'achats.
   */
  public String getCodeRegroupementAchat() {
    return codeRegroupementAchat;
  }
  
  /**
   * Modifier le code du regroupement d'achats de l'article.
   * @param pCodeRegroupementAchat Code regroupement d'achats.
   */
  public void setCodeRegroupementAchat(String pCodeRegroupementAchat) {
    codeRegroupementAchat = pCodeRegroupementAchat;
  }
  
  /**
   * Retourne le code rattachement aux conditions de vente.
   * 
   * @return
   */
  public String getCodeRattachementCNV() {
    return codeRattachementCNV;
  }
  
  /**
   * Initialise le code rattachement aux conditions de vente.
   * 
   * @param pCodeRattachementCNV
   */
  public void setCodeRattachementCNV(String pCodeRattachementCNV) {
    codeRattachementCNV = pCodeRattachementCNV;
  }
  
  /**
   * Retourne le code rattachement 1 aux conditions de vente.
   * 
   * @return
   */
  public String getCodeRattachementCNV1() {
    return codeRattachementCNV1;
  }
  
  /**
   * Initialise le code rattachement 1 aux conditions de vente.
   * 
   * @param pCodeRattachementCNV1
   */
  public void setCodeRattachementCNV1(String pCodeRattachementCNV1) {
    codeRattachementCNV1 = pCodeRattachementCNV1;
  }
  
  /**
   * Retourne le code rattachement 2 aux conditions de vente.
   * 
   * @return
   */
  public String getCodeRattachementCNV2() {
    return codeRattachementCNV2;
  }
  
  /**
   * Initialise le code rattachement 2 aux conditions de vente.
   * 
   * @param pCodeRattachementCNV2
   */
  public void setCodeRattachementCNV2(String pCodeRattachementCNV2) {
    codeRattachementCNV2 = pCodeRattachementCNV2;
  }
  
  /**
   * Retourne le code rattachement 3 aux conditions de vente.
   * 
   * @return
   */
  public String getCodeRattachementCNV3() {
    return codeRattachementCNV3;
  }
  
  /**
   * Initialise le code rattachement 3 aux conditions de vente.
   * 
   * @param pCodeRattachementCNV3
   */
  public void setCodeRattachementCNV3(String pCodeRattachementCNV3) {
    codeRattachementCNV3 = pCodeRattachementCNV3;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs données calcul (FormulePrixVente)
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le code de l'unité de vente.
   * @return Code de l'unité de vente.
   */
  public String getCodeUniteVente() {
    return codeUniteVente;
  }
  
  /**
   * Initialise l'unité de vente.
   * 
   * @param pCodeUniteVente
   */
  public void setCodeUniteVente(String pCodeUniteVente) {
    codeUniteVente = pCodeUniteVente;
  }
  
  /**
   * Retourner la remise maximum autorisée pour l'article.
   * La remise maximum n'est pas utilisée pour plafonner le calcul mais afficher un message d'avertissement si elle est dépassée.
   * @return Remise maximum.
   */
  public Integer getTauxRemiseMaximum() {
    return tauxRemiseMaximum;
  }
  
  /**
   * Modifier la remise maximum autorisée pour l'article.
   * Si la valeur fournie est égale à 99, cela signifi que l'article n'est pas remisable.
   * @param pTauxRemiseMaximum Remise maximum.
   */
  public void setTauxRemiseMaximum(Integer pTauxRemiseMaximum) {
    if (pTauxRemiseMaximum != null && pTauxRemiseMaximum == CODE_NON_REMISABLE) {
      tauxRemiseMaximum = null;
      remisable = false;
    }
    else {
      tauxRemiseMaximum = pTauxRemiseMaximum;
      remisable = true;
    }
  }
  
  /**
   * Indiquer si l'article es remisable.
   * L'article est remisable par défaut si la valeur est inconnue.
   * @return true=remisable, false=non remisable.
   */
  public Boolean isRemisable() {
    return remisable == null || remisable;
  }
  
  /**
   * Modifier si l'article es remisable.
   * @param pRemisable true=remisable, false=non remisable.
   */
  public void setRemisable(Boolean pRemisable) {
    remisable = pRemisable;
  }
  
  /**
   * Récupérer l'origine du PUMP.
   * @return Origine PUMP : magasin ou établissement.
   */
  public EnumOriginePump getOriginePump() {
    return originePump;
  }
  
  /**
   * Modifier l'origine du PUMP.
   * @param pOriginePump Origine PUMP : magasin ou établissement.
   */
  public void setOriginePump(EnumOriginePump pOriginePump) {
    originePump = pOriginePump;
  }
  
  /**
   * Retourner le PUMP de l'article.
   * 
   * Suivant la valeur du PS124, le PUMP d'un article est géré par établissement (appelé aussi mono-PUMP) ou par magasin (issu de
   * l'historique du stock). A ce stade, on ne souci plus de la provenance du PUMP, on a juste besoin de sa valeur.
   * 
   * @return PUMP.
   */
  public BigDecimal getPump() {
    return pump;
  }
  
  /**
   * Modifier le PUMP de l'article.
   * @param pPump PUMP.
   */
  public void setPump(BigDecimal pPump) {
    pump = pPump;
  }
  
  /**
   * Retourner le type de prix de revient.
   * @return Type prix de revient.
   */
  public EnumTypePrv getTypePrv() {
    return typePrv;
  }
  
  /**
   * Modifier le type de prix de revient.
   * @param pTypePrv Type prix de revient.
   */
  public void setTypePrv(EnumTypePrv pTypePrv) {
    typePrv = pTypePrv;
  }
  
  /**
   * Récupérer l'origine du prix de revient.
   * @return Origine PRV : CNA, article, magasin.
   */
  public EnumOriginePrv getOriginePrv() {
    return originePrv;
  }
  
  /**
   * Modifier l'origine du prix de revient.
   * @param pOriginePrv Origine PRV : CNA, article, magasin.
   */
  public void setOriginePrv(EnumOriginePrv pOriginePrv) {
    originePrv = pOriginePrv;
  }
  
  /**
   * Retourner le prix de revient.
   * @return Prix de revient.
   */
  public BigDecimal getPrv() {
    return prv;
  }
  
  /**
   * Modifier le prix de revient.
   * @param pPrv Prix de revient.
   */
  public void setPrv(BigDecimal pPrv) {
    prv = pPrv;
  }
}
