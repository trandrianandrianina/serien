/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc.pricejet;

import java.io.Serializable;
import java.util.ArrayList;

public class ObjetPricejet implements Serializable {
  private String typeObjet = null;
  private ArrayList<ElementPriceJet> listeElements = null;

  public String getTypeObjet() {
    return typeObjet;
  }

  public void setTypeObjet(String typeObjet) {
    this.typeObjet = typeObjet;
  }

  public ArrayList<ElementPriceJet> getListeElements() {
    return listeElements;
  }

  public void setListeElements(ArrayList<ElementPriceJet> listeElements) {
    this.listeElements = listeElements;
  }

}
