/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;

/**
 * Configuration de catalogue
 * Cette classe permet de décrire la configuration de l'import/export d'un catalogue articles fournisseur via un fichier
 * On y décrit le fournisseur concerné, le fichier à récupérer ainsi que le mapping des zones entre Série N et le fichier
 */
public class ConfigurationCatalogue extends AbstractClasseMetier<IdConfigurationCatalogue> {
  public static int LONGUEUR_LIBELLE = 50;
  
  private IdEtablissement idEtablissement = null;
  private IdFournisseur idFournisseur = null;
  private String raisonSocialeFournisseur = null;
  private String libelle = null;
  private String fichierCSV = null;
  private ListeChampCatalogue listeChampCatalogue = null;
  private ListeChampERP listeChampERP = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public ConfigurationCatalogue(IdConfigurationCatalogue pIdConfigurationCatalogue) {
    super(pIdConfigurationCatalogue);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Charger une configuration de catalogue à partir de son identifiant.
   */
  public static ConfigurationCatalogue chargerParId(IdSession pIdSession, IdConfigurationCatalogue pIdConfiguration) {
    IdConfigurationCatalogue.controlerId(pIdConfiguration, true);
    ConfigurationCatalogue configurationCatalogue =
        ManagerServiceFournisseur.chargerUneConfigCatalogueParId(pIdSession, pIdConfiguration);
    return configurationCatalogue;
  }
  
  /**
   * Comparer l'intégralité des données.
   */
  public static boolean equalsComplet(ConfigurationCatalogue pObjet1, ConfigurationCatalogue pObjet2) {
    if (pObjet1 == pObjet2) {
      return true;
    }
    if (pObjet1 == null || pObjet2 == null) {
      return false;
    }
    return Constantes.equals(pObjet1.id, pObjet2.id) && Constantes.equals(pObjet1.idEtablissement, pObjet2.idEtablissement)
        && Constantes.equals(pObjet1.idFournisseur, pObjet2.idFournisseur)
        && Constantes.equals(pObjet1.raisonSocialeFournisseur, pObjet2.raisonSocialeFournisseur)
        && Constantes.equals(pObjet1.libelle, pObjet2.libelle) && Constantes.equals(pObjet1.fichierCSV, pObjet2.fichierCSV)
        && ListeChampCatalogue.equalsComplet(pObjet1.listeChampCatalogue, pObjet2.listeChampCatalogue)
        && ListeChampERP.equalsComplet(pObjet1.listeChampERP, pObjet2.listeChampERP);
  }
  
  @Override
  public ConfigurationCatalogue clone() {
    ConfigurationCatalogue copie = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      copie = (ConfigurationCatalogue) super.clone();
      
      ListeChampCatalogue listeChampCatalogueCopie = new ListeChampCatalogue();
      if (listeChampCatalogue != null) {
        for (ChampCatalogue champCatalogue : listeChampCatalogue) {
          listeChampCatalogueCopie.add(champCatalogue.clone());
        }
        copie.setListeChampCatalogue(listeChampCatalogueCopie);
      }
      
      ListeChampERP listeChampERPCopie = new ListeChampERP();
      if (listeChampERP != null) {
        for (ChampERP champERP : listeChampERP) {
          listeChampERPCopie.add(champERP.clone());
        }
        copie.setListeChampERP(listeChampERPCopie);
      }
    }
    catch (CloneNotSupportedException cnse) {
      return null;
    }
    
    // on renvoie le clone
    return copie;
  }
  
  /**
   * Sauvegarder une configuration de catalogue fournisseur.
   */
  public void sauver(IdSession pIdSession) {
    ManagerServiceFournisseur.sauverUneConfigurationCatalogue(pIdSession, this);
  }
  
  /**
   * Supprimer une configuration catalogue.
   */
  public void supprimer(IdSession pIdSession) {
    ManagerServiceFournisseur.supprimerUneConfigurationCatalogue(pIdSession, this);
  }
  
  /**
   * Associer un champ ERP à un champ Catalogue
   */
  public void ajouterUneAssociationChamp(ChampERP pChampERP) {
    if (listeChampERP == null) {
      listeChampERP = new ListeChampERP();
    }
    
    int i = 0;
    for (ChampERP champERP : listeChampERP) {
      if (Constantes.equals(champERP.getId(), pChampERP.getId())) {
        listeChampERP.remove(i);
        break;
      }
      i++;
    }
    
    listeChampERP.add(pChampERP);
  }
  
  /**
   * Retirer une association champ ERP / champ catalogue
   */
  public void retirerUneAssociationChamp(ChampERP pChampERP) {
    if (listeChampERP == null) {
      return;
    }
    
    int i = 0;
    for (ChampERP champERP : listeChampERP) {
      if (Constantes.equals(champERP.getId(), pChampERP.getId())) {
        listeChampERP.remove(i);
        break;
      }
      i++;
    }
    
    if (listeChampERP.size() == 0) {
      listeChampERP = null;
    }
  }
  
  /**
   * Renseigner le nom du fichier CSV associé à la configuration catalogue.
   * Le nom du fichier doit obligatoirement se terminer par l'extension .csv.
   */
  public void renseignerInfosFichierCSV(IdSession pIdSession, String pFichierCSV) {
    fichierCSV = null;
    listeChampCatalogue = null;
    listeChampERP = null;
    if (pFichierCSV != null && !pFichierCSV.isEmpty()) {
      fichierCSV = pFichierCSV;
      ListeChampCatalogue listeChampCatalogue = ListeChampCatalogue.chargerTout(pIdSession, pFichierCSV);
      setListeChampCatalogue(listeChampCatalogue);
    }
  }
  
  /**
   * Sauver en base de données une association 1 champ ERP à n champ catalogue dans la configuration de catalogue active
   */
  public void sauverAssociationChamps(IdSession pIdSession, ChampERP pChampERP) {
    IdChampERP.controlerId(pChampERP.getId(), false);
    ManagerServiceFournisseur.sauverAssociationChampsConfigurationCatalogue(pIdSession, this, pChampERP.getId());
  }
  
  /**
   * Retourner l'identifiant de l'établissement de la configuration de catalogue
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'identifiant de l'établissement de la configuration de catalogue
   */
  public void setIdEtablissement(IdEtablissement idEtablissement) {
    this.idEtablissement = idEtablissement;
  }
  
  /**
   * Retourner l'identifiant du fournisseur associé à la configuration du catalogue
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Modifier l'identifiant du fournisseur associé à la configuration de catalogue
   */
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
  
  /**
   * Retourner libellé de la configuration d'un catalogue
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé de la configuration de catalogue
   */
  public void setLibelle(String pLibelle) {
    if (pLibelle != null && pLibelle.trim().length() > ConfigurationCatalogue.LONGUEUR_LIBELLE) {
      pLibelle = pLibelle.substring(0, ConfigurationCatalogue.LONGUEUR_LIBELLE);
    }
    
    libelle = pLibelle;
  }
  
  /**
   * Retourner la raison sociale du fournisseur de la configuration de catalogue
   */
  public String getRaisonSocialeFournisseur() {
    return raisonSocialeFournisseur;
  }
  
  /**
   * Modifier la raison sociale du fournisseur de la configuration de catalogue
   */
  public void setRaisonSocialeFournisseur(String raisonSocialeFournisseur) {
    this.raisonSocialeFournisseur = raisonSocialeFournisseur;
  }
  
  /**
   * Retourner le nom du fichier CSV associé à la configuration de catalogue
   */
  public String getFichierCSV() {
    return fichierCSV;
  }
  
  /**
   * Renseigner le nom du fichier CSV associé à la configuration de catalogue
   */
  public void setFichierCSV(String fichierCSV) {
    this.fichierCSV = fichierCSV;
  }
  
  /**
   * Retourner la liste des champs du catalogue que contient le fichier CSV associé à la configuration catalogue
   */
  public ListeChampCatalogue getListeChampCatalogue() {
    return listeChampCatalogue;
  }
  
  /**
   * Modifier la liste des champs catalogue que contient le fichier CSV associé à la configuration catalogue
   */
  public void setListeChampCatalogue(ListeChampCatalogue pListeChampCatalogue) {
    listeChampCatalogue = pListeChampCatalogue;
  }
  
  /**
   * Retourner la liste des champs de Série N associés à la configuration de catalogue
   */
  public ListeChampERP getListeChampERP() {
    return listeChampERP;
  }
  
  /**
   * Modifier la liste des champs ERP associés à la configuration de catalogue
   */
  public void setListeChampERP(ListeChampERP pListeChampERP) {
    listeChampERP = pListeChampERP;
  }
  
}
