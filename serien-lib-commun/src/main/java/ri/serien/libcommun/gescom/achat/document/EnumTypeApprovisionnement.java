/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Choix édition
 * Liste l'ensemble des choix possibles lors de l'édition d'un document de vente ou d'achat.
 */
public enum EnumTypeApprovisionnement {
  CLIENT(0, "Approvisionnement pour client"),
  DIRECT_USINE(1, "Approvisionnement direct usine");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeApprovisionnement(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeApprovisionnement valueOfByCode(Integer pCode) {
    for (EnumTypeApprovisionnement value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le choix du type d'approvisionnement est invalide : " + pCode);
  }
  
}
