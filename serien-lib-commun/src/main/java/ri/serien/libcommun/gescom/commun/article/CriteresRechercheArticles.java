/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

public class CriteresRechercheArticles extends CriteresBaseRecherche {
  // Constantes
  public static final int RECHERCHE_TYPE_ARTICLES_NEGOCE = 0;
  public static final int RECHERCHE_TYPE_ARTICLES_TRANSPORTS = 1;
  public static final int RECHERCHE_TYPE_ARTICLES_COMMENTAIRES = 2;
  public static final int RECHERCHE_TYPE_ARTICLES_PALETTES = 3;
  
  // Retour: code etablissement, code article, libellé, code barre, référence fournisseur, raison sociale fournisseur,
  // mots de classement
  public static final int RECHERCHE_COMPTOIR = 1;
  
  public static final int OPTION_TOUS_LES_ARTICLES_SAUF_DIRECT_USINE = 0;
  public static final int OPTION_LES_ARTICLES_DIRECT_USINE_UNIQUEMENT = 1;
  public static final int OPTION_TOUS_LES_ARTICLES = 2; // Pas de restriction sur le direct usine
  
  // Variables
  private int typeRecherche = RECHERCHE_TYPE_ARTICLES_NEGOCE;
  private boolean horsGamme = false;
  private IdClient idClient = null;
  private IdMagasin idMagasin = null;
  private IdFournisseur idFournisseur = null;
  private BigPercentage tauxRemiseForcee = BigPercentage.ZERO;
  private int optionDirectUsine = OPTION_TOUS_LES_ARTICLES_SAUF_DIRECT_USINE;
  private IdChantier idChantier = null;
  private IdDocumentVente idDocumentVente = null;
  private IdZoneGeographique idZoneGeographique = null;
  private Boolean isArticleValideSeulement = null;
  
  // -- Accesseurs
  
  @Override
  public int getTypeRecherche() {
    return typeRecherche;
  }
  
  @Override
  public void setTypeRecherche(int typeRecherche) {
    this.typeRecherche = typeRecherche;
  }
  
  public boolean isHorsGamme() {
    return horsGamme;
  }
  
  public void setHorsGamme(boolean horsGamme) {
    this.horsGamme = horsGamme;
  }
  
  public IdClient getIdClient() {
    return idClient;
  }
  
  public void setIdClient(IdClient pIdClient) {
    idClient = pIdClient;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin pIdMagasin) {
    this.idMagasin = pIdMagasin;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur pIdFournisseur) {
    idFournisseur = pIdFournisseur;
  }
  
  public int getOptionDirectUsine() {
    return optionDirectUsine;
  }
  
  public void setOptionDirectUsine(int optionDirectUsine) {
    this.optionDirectUsine = optionDirectUsine;
  }
  
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  public void setIdDocumentVente(IdDocumentVente pIdDocumentVente) {
    this.idDocumentVente = pIdDocumentVente;
  }
  
  public IdChantier getIdChantier() {
    return idChantier;
  }
  
  public void setIdChantier(IdChantier pIdChantier) {
    this.idChantier = pIdChantier;
  }
  
  /**
   * Identifiant de la zone géographique (pour les articles transport).
   */
  public IdZoneGeographique getIdZoneGeographique() {
    return idZoneGeographique;
  }
  
  /**
   * Modifier l'identifiant de la zone géographique (pour les articles transport).
   */
  public void setZoneGeographique(IdZoneGeographique pIdZoneGeographique) {
    idZoneGeographique = pIdZoneGeographique;
  }
  
  /**
   * Retourner le taux de remise forcée.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemiseForcee() {
    return tauxRemiseForcee;
  }
  
  /**
   * Modifier le taux de remise forcée.
   * @param pTauxRemiseForcee Taux de remise.
   */
  public void setTauxRemiseForcee(BigPercentage pTauxRemiseForcee) {
    tauxRemiseForcee = pTauxRemiseForcee;
  }
  
  /**
   * Retourner si la recherche concerne les seuls articles valides (A1TOP == 0)
   */
  public Boolean isArticleValideSeulement() {
    return isArticleValideSeulement;
  }
  
  /**
   * Modifier si la recherche doit concerner les seuls articles valides (A1TOP == 0)
   */
  public void setIsArticleValideSeulement(Boolean pIsArticleValideSeulement) {
    isArticleValideSeulement = pIsArticleValideSeulement;
    if (isArticleValideSeulement == null) {
      isArticleValideSeulement = false;
    }
  }
  
}
