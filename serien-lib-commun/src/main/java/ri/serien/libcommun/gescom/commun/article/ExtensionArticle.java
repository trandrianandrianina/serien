/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.io.Serializable;

public class ExtensionArticle implements Serializable {
  // Constantes
  public static final int TAILLE_TYPE_ZONE_PERSONNALISEE = 1;
  public static final int TAILLE_ZONE_PERSONNALISEE = 30;
  
  // Variables fichier
  private String typeSysteme = "";
  private String libelle2 = "";
  private String libelle3 = "";
  private String libelle4 = "";
  
  // To be continued ...
  
  // -- Accesseurs
  public String getTypeSysteme() {
    return typeSysteme;
  }
  
  public void setTypeSysteme(String typeSysteme) {
    this.typeSysteme = typeSysteme;
  }
  
  public String getLibelle2() {
    return libelle2;
  }
  
  public void setLibelle2(String libelle2) {
    this.libelle2 = libelle2;
  }
  
  public String getLibelle3() {
    return libelle3;
  }
  
  public void setLibelle3(String libelle3) {
    this.libelle3 = libelle3;
  }
  
  public String getLibelle4() {
    return libelle4;
  }
  
  public void setLibelle4(String libelle4) {
    this.libelle4 = libelle4;
  }
  
}
