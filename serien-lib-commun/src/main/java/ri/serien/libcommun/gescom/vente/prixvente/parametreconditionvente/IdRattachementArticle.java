/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'un rattachement article pour une condition de ventes.
 * Cet identifiant n'a aucune valeur en base, il est créé à la volée.
 * 
 * L'identifiant est composé du type de rattachement article et du code rattachement article.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdRattachementArticle extends AbstractId {
  // Variables
  private final EnumTypeRattachementArticle typeRattachementArticle;
  private final String codeRattachementArticle;
  
  /**
   * Constructeur.
   */
  private IdRattachementArticle(EnumEtatObjetMetier pEnumEtatObjetMetier, EnumTypeRattachementArticle pTypeRattachementArticle,
      String pCodeRattachement) {
    super(pEnumEtatObjetMetier);
    typeRattachementArticle = controlerType(pTypeRattachementArticle);
    codeRattachementArticle = controlerCode(pCodeRattachement);
  }
  
  /**
   * Créer un identifiant.
   */
  public static IdRattachementArticle getInstance(EnumTypeRattachementArticle pTypeRattachementArticle, String pCodeRattachementArticle) {
    return new IdRattachementArticle(EnumEtatObjetMetier.MODIFIE, pTypeRattachementArticle, pCodeRattachementArticle);
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du type du rattachement article.
   */
  private EnumTypeRattachementArticle controlerType(EnumTypeRattachementArticle pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le type de rattachement n'est pas renseigné.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du code rattachement article.
   * Note : la valeur du code peut être vide, elle correspond à un rattachement global.
   */
  private String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code de rattachement n'est pas renseigné.");
    }
    return Constantes.normerTexte(pValeur);
  }
  
  // -- Méthodes publiques
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdRattachementArticle controlerId(IdRattachementArticle pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du rattachement article est invalide.");
    }
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du rattachement article n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + typeRattachementArticle.hashCode();
    code = 37 * code + codeRattachementArticle.hashCode();
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdRattachementArticle)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de rattachement article.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdRattachementArticle id = (IdRattachementArticle) pObject;
    return typeRattachementArticle.equals(id.typeRattachementArticle) && codeRattachementArticle.equals(id.codeRattachementArticle);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdRattachementArticle)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdRattachementArticle id = (IdRattachementArticle) pObject;
    int comparaison = typeRattachementArticle.compareTo(id.typeRattachementArticle);
    if (comparaison != 0) {
      return comparaison;
    }
    return codeRattachementArticle.compareTo(id.codeRattachementArticle);
  }
  
  @Override
  public String getTexte() {
    return typeRattachementArticle.getCode() + SEPARATEUR_ID + codeRattachementArticle;
  }
  
  /**
   * Indiquer si le rattachement de la conditon de vente est une condition de vente emboitée.
   * @return true=emboitée, false=sinon.
   */
  public boolean isConditionVenteEmboitee() {
    if (typeRattachementArticle == EnumTypeRattachementArticle.EMBOITEE_OU_GLOBALE && !codeRattachementArticle.isEmpty()) {
      return true;
    }
    return false;
  }
  
  /**
   * Indiquer si la conditon de vente s'applique à tous les articles.
   * @return true=pour tous les articles, false=sinon.
   */
  public boolean isConditionVentePourTousArticle() {
    if (typeRattachementArticle == EnumTypeRattachementArticle.EMBOITEE_OU_GLOBALE && codeRattachementArticle.isEmpty()) {
      return true;
    }
    return false;
  }
  
  // -- Accesseurs
  
  /**
   * Retourner le type de rattachement article.
   * @return Le type.
   */
  public EnumTypeRattachementArticle getTypeRattachement() {
    return typeRattachementArticle;
  }
  
  /**
   * Retourner le code de rattachement article.
   * @return Le code.
   */
  public String getCodeRattachement() {
    return codeRattachementArticle;
  }
  
}
