/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'une ligne de document de ventes.
 *
 * L'identifiant est composé de l'identiant de l'établissement et du numéro de ligne dans le document.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdReceptionAchat extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_NUMERO = 6;
  
  // Variables
  private IdDocumentAchat idDocumentAchat;
  private Integer numeroLigne;
  
  /**
   * Constructeur avec l'identifiant document et le numéro de ligne.
   */
  private IdReceptionAchat(EnumEtatObjetMetier pEnumEtatObjetMetier, IdDocumentAchat pIdDocument, Integer pNumeroLigne) {
    super(pEnumEtatObjetMetier, pIdDocument.getIdEtablissement());
    idDocumentAchat = pIdDocument;
    numeroLigne = controlerNumeroLigne(pNumeroLigne);
  }
  
  /**
   * Créer un identifiant à partir de l'identifiant document et le numéro de ligne.
   */
  public static IdReceptionAchat getInstance(IdDocumentAchat pIdDocument, Integer pNumeroLigne) {
    return new IdReceptionAchat(EnumEtatObjetMetier.MODIFIE, pIdDocument, pNumeroLigne);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdReceptionAchat getInstance(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentAchat pCodeEntete, Integer pNumero,
      Integer pSuffixe, Integer pNumeroLigne) {
    IdDocumentAchat idDocumentAchat = IdDocumentAchat.getInstance(pIdEtablissement, pCodeEntete, pNumero, pSuffixe);
    return new IdReceptionAchat(EnumEtatObjetMetier.MODIFIE, idDocumentAchat, pNumeroLigne);
  }
  
  /**
   * Contrôler la validité du numéro de ligne.
   * Le numéro de ligne est compris entre 0 et 999 999.
   */
  private Integer controlerNumeroLigne(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de la ligne n'est pas renseigné.");
    }
    if (pValeur < 0) {
      throw new MessageErreurException("Le numéro de la ligne est inférieur à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro de ligne est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + idDocumentAchat.hashCode();
    code = 37 * code + numeroLigne.hashCode();
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    
    if (pObject == null) {
      return false;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdReceptionAchat)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de réception d'achat.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdReceptionAchat id = (IdReceptionAchat) pObject;
    return idDocumentAchat.equals(id.idDocumentAchat) && numeroLigne.equals(id.numeroLigne);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdReceptionAchat)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdReceptionAchat id = (IdReceptionAchat) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idDocumentAchat.compareTo(id.idDocumentAchat);
    if (comparaison != 0) {
      return comparaison;
    }
    return numeroLigne.compareTo(id.numeroLigne);
  }
  
  @Override
  public String getTexte() {
    return "" + getCodeEntete() + +SEPARATEUR_ID + getNumero() + SEPARATEUR_ID + getSuffixe() + SEPARATEUR_ID + numeroLigne;
  }
  
  // -- Accesseurs
  
  /**
   * Identifiant du document de ventes associé à la ligne.
   */
  public IdDocumentAchat getIdDocumentAchat() {
    return idDocumentAchat;
  }
  
  /**
   * Entête du document contenant la ligne.
   */
  public EnumCodeEnteteDocumentAchat getCodeEntete() {
    return idDocumentAchat.getCodeEntete();
  }
  
  /**
   * Numéro du document contenant la ligne.
   * Le numéro du document est compris entre entre 0 et 999 999.
   */
  public Integer getNumero() {
    return idDocumentAchat.getNumero();
  }
  
  /**
   * Suffixe du document contenant la ligne.
   */
  public Integer getSuffixe() {
    return idDocumentAchat.getSuffixe();
  }
  
  /**
   * Numéro de ligne au sein du document.
   */
  public Integer getNumeroLigne() {
    return numeroLigne;
  }
}
