/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prodevis;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;

public class CriteresRechercheProDevis extends CriteresBaseRecherche {
  // Constantes
  public static final String EXTENSION_DEFAULT = "txt";
  // Variables
  private String extensionFichier = EXTENSION_DEFAULT;
  
  // -- Accesseurs
  
  public String getExtensionFichier() {
    return extensionFichier;
  }
  
  public void setExtensionFichier(String extensionFichier) {
    this.extensionFichier = extensionFichier;
  }
  
}
