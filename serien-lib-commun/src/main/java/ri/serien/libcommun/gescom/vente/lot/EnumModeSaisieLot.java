/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.lot;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Mode de saisie des lots.
 * 
 */
public enum EnumModeSaisieLot {
  MODE_SAISIE_LOT(0, "Saisie des quantités sur lots"),
  MODE_RETOUR_LOT(1, "Retour de quantités sur lots"),
  MODE_LECTURE_SEULE(2, "Quantités sur lots (non modifiable)"),
  MODE_CREATION_LOT(3, "Création de lots"),
  MODE_EXTRACTION_LOT(4, "Extractions d'articles avec quantités sur lots");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumModeSaisieLot(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumModeSaisieLot valueOfByCode(Integer pCode) {
    for (EnumModeSaisieLot value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le mode de saisie des lots est invalide : " + pCode);
  }
}
