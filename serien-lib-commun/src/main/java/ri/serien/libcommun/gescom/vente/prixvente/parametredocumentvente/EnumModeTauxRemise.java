/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente;

import ri.serien.libcommun.outils.Trace;

/**
 * Liste les modes d'application des taux de remises.
 * 
 * // Liste des codes trouvés en base mais ignorés pour le calcul du prix (pour l'instant)
 */
public enum EnumModeTauxRemise {
  CASCADE(' ', "En cascade"),
  AJOUT('A', "En ajout"),
  EN_AVOIR_SEPARE('S', "En avoir séparé");
  // REMISE_FORCEE('F', "Remise ou condition d'entête saisie et forcée sur lignes")
  
  private static final String CODE_A_IGNORER = "FCEX";
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumModeTauxRemise(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son numéro.
   */
  static public EnumModeTauxRemise valueOfByCode(Character pCode) {
    // Des valeurs non prises en compte pour le calcul du prix sont trouvables en base
    if (pCode == null || CODE_A_IGNORER.indexOf("" + pCode) != -1) {
      Trace.alerte("Le mode d'application des taux de remises contient le code " + pCode + " qui n'est pas prix en compte.");
      return null;
    }
    // Cas normal
    for (EnumModeTauxRemise value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    Trace.erreur("Le mode d'application des taux de remises est invalide : " + pCode);
    return null;
  }
  
}
