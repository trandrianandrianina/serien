/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.boncour;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste de BonCour.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de bons de cour.
 */
public class ListeBonCour extends ListeClasseMetier<IdBonCour, BonCour, ListeBonCour> {
  /**
   * Constructeur.
   */
  public ListeBonCour() {
  }
  
  /**
   * Construire une liste de BonCour correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des BonCour ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeBonCour creerListeNonChargee(List<IdBonCour> pListeIdBonCour) {
    ListeBonCour listeBonCour = new ListeBonCour();
    if (pListeIdBonCour != null) {
      for (IdBonCour idBonCour : pListeIdBonCour) {
        BonCour bonCour = new BonCour(idBonCour);
        bonCour.setCharge(false);
        listeBonCour.add(bonCour);
      }
    }
    return listeBonCour;
  }
  
  /**
   * Charger la liste des objets métiers suivant la liste d'identifiants donnée.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeBonCour charger(IdSession pIdSession, List<IdBonCour> pListeIdBonCour) {
    return ManagerServiceDocumentVente.chargerListeBonCour(pIdSession, pListeIdBonCour);
  }
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeBonCour charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
}
