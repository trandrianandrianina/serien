/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un document d'achats.
 * 
 * L'identifiant est composé du code établissement, du type de document, du numéro du document et du suffixe.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdDocumentAchat extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_ENTETE = 1;
  public static final int LONGUEUR_NUMERO = 6;
  public static final int LONGUEUR_SUFFIXE = 1;
  public static final int LONGUEUR_NUMERO_COMPLET = 8;
  public static final int LONGUEUR_INDICATIF = LONGUEUR_ENTETE + LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_NUMERO + LONGUEUR_SUFFIXE;
  
  public static final int INDICATIF_NUM = 0;
  public static final int INDICATIF_ENTETE_ETB_NUM_SUF = 1;
  public static final int INDICATIF_NUM_SUF = 2;
  
  // Variables
  private final EnumCodeEnteteDocumentAchat codeEntete;
  private final Integer numero;
  private final Integer suffixe;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdDocumentAchat(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement,
      EnumCodeEnteteDocumentAchat pCodeEntete, Integer pNumero, Integer pSuffixe) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    codeEntete = controlerCodeEnteteDocumentAchat(pCodeEntete);
    numero = controlerNumero(pNumero);
    suffixe = controlerSuffixe(pSuffixe);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   * Le numéro et le suffixe ne sont pas à fournir. Ils sont renseignés avec la valeur 0 dans ce cas.
   */
  private IdDocumentAchat(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentAchat pCodeEntete) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    codeEntete = controlerCodeEnteteDocumentAchat(pCodeEntete);
    numero = 0;
    suffixe = 0;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdDocumentAchat getInstance(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentAchat pCodeEntete, Integer pNumero,
      Integer pSuffixe) {
    return new IdDocumentAchat(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCodeEntete, pNumero, pSuffixe);
  }
  
  /**
   * Créer un identifiant à partir de l'indicatif d'un document de achat.
   */
  public static IdDocumentAchat getInstanceParIndicatif(String pIndicatif) {
    return new IdDocumentAchat(EnumEtatObjetMetier.MODIFIE, extraireEtablissementDeIndicatif(pIndicatif),
        extraireEnteteDeIndicatif(pIndicatif), extraireNumeroDeIndicatif(pIndicatif), extraireSuffixeDeIndicatif(pIndicatif));
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdDocumentAchat getInstanceAvecCreationId(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentAchat pCodeEntete) {
    return new IdDocumentAchat(pIdEtablissement, pCodeEntete);
  }
  
  /**
   * Générer l'idenfiant d'origine d'un document.
   * L'identifiant d'origine d'un document est le même que l'identifiant du document si ce n'est que l'entête est en minuscule au lieu
   * d'être en majucscule.
   */
  public static IdDocumentAchat getInstanceIdOrigine(IdDocumentAchat idDocumentAchat) {
    switch (idDocumentAchat.getCodeEntete()) {
      case FACTURE:
        if (idDocumentAchat.isExistant()) {
          return new IdDocumentAchat(EnumEtatObjetMetier.MODIFIE, idDocumentAchat.getIdEtablissement(),
              EnumCodeEnteteDocumentAchat.FACTURE, idDocumentAchat.getNumero(), idDocumentAchat.getSuffixe());
        }
        else {
          return new IdDocumentAchat(EnumEtatObjetMetier.CREE, idDocumentAchat.getIdEtablissement(), EnumCodeEnteteDocumentAchat.FACTURE,
              idDocumentAchat.getNumero(), idDocumentAchat.getSuffixe());
        }
        
      case COMMANDE_OU_RECEPTION:
        if (idDocumentAchat.isExistant()) {
          return new IdDocumentAchat(EnumEtatObjetMetier.MODIFIE, idDocumentAchat.getIdEtablissement(),
              EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION, idDocumentAchat.getNumero(), idDocumentAchat.getSuffixe());
        }
        else {
          return new IdDocumentAchat(EnumEtatObjetMetier.CREE, idDocumentAchat.getIdEtablissement(),
              EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION, idDocumentAchat.getNumero(), idDocumentAchat.getSuffixe());
        }
        
      default:
        throw new MessageErreurException("Impossible de générer l'identifiant d'origine d'un document pour ce type de document.");
    }
  }
  
  /**
   * Contrôler la validité du code entête du document.
   */
  private static EnumCodeEnteteDocumentAchat controlerCodeEnteteDocumentAchat(EnumCodeEnteteDocumentAchat pValeur) {
    // Renseigner l'indicatif tiers
    if (pValeur == null) {
      throw new MessageErreurException("Le code entête du document n'est pas renseigné.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du numéro du document.
   * Il doit être supérieur à zéro et doit comporter au maximum 6 chiffres (entre 1 et 999 999).
   */
  private Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du document n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du document est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du document est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du suffixe du document.
   * Il doit être supérieur ou égal à zéro et doit comporter au maximum 1 chiffre (entre 0 et 9).
   */
  private Integer controlerSuffixe(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le suffixe du document n'est pas renseigné.");
    }
    if (pValeur < 0 || pValeur > 9) {
      throw new MessageErreurException("Le suffixe du document soit être compris entre 0 et 9 inclus.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_SUFFIXE)) {
      throw new MessageErreurException("Le suffixe du document est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler l'indicatif d'un document.
   */
  private static String controlerIndicatif(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("L'indicatif du document n'est pas renseigné.");
    }
    if (pValeur.length() < LONGUEUR_INDICATIF) {
      throw new MessageErreurException("L'indicatif du document n'est pas correct car il est inférieur à la longueur attendu.");
    }
    return pValeur.trim();
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdDocumentAchat controlerId(IdDocumentAchat pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du document d'achats est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du document d'achats n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  /**
   * Extraire le code entête de l'indicatif du document.
   */
  private static EnumCodeEnteteDocumentAchat extraireEnteteDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    return EnumCodeEnteteDocumentAchat.valueOfByCode(pIndicatif.charAt(0));
  }
  
  /**
   * Extraire le code établissement de l'indicatif du document.
   */
  private static IdEtablissement extraireEtablissementDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    int offsetDeb = LONGUEUR_ENTETE;
    int offsetFin = offsetDeb + LONGUEUR_CODE_ETABLISSEMENT;
    return IdEtablissement.getInstance(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  /**
   * Extraire le numéro de l'indicatif du document.
   */
  private static Integer extraireNumeroDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    int offsetDeb = LONGUEUR_ENTETE + LONGUEUR_CODE_ETABLISSEMENT;
    int offsetFin = offsetDeb + LONGUEUR_NUMERO;
    return Integer.valueOf(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  /**
   * Extraire le suffixe de l'indicatif du document.
   */
  private static Integer extraireSuffixeDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    int offsetDeb = LONGUEUR_ENTETE + LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_NUMERO;
    int offsetFin = offsetDeb + LONGUEUR_SUFFIXE;
    return Integer.valueOf(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  // -- Méthodes publiques
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getCodeEtablissement().hashCode();
    code = 37 * code + codeEntete.hashCode();
    code = 37 * code + numero.hashCode();
    code = 37 * code + suffixe.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    
    if (pObject == null) {
      return false;
    }
    
    if (!(pObject instanceof IdDocumentAchat)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de document d'achat.");
    }
    IdDocumentAchat id = (IdDocumentAchat) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && codeEntete.equals(id.codeEntete) && numero.equals(id.numero)
        && suffixe.equals(id.suffixe);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdDocumentAchat)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdDocumentAchat id = (IdDocumentAchat) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = codeEntete.compareTo(id.codeEntete);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numero.compareTo(id.numero);
    if (comparaison != 0) {
      return comparaison;
    }
    return suffixe.compareTo(id.suffixe);
  }
  
  @Override
  public String getTexte() {
    return "" + numero + SEPARATEUR_ID + suffixe;
  }
  
  // -- Accesseurs
  
  /**
   * Code entête du document.
   */
  public EnumCodeEnteteDocumentAchat getCodeEntete() {
    return codeEntete;
  }
  
  /**
   * Numéro du document.
   * Le numéro du document est compris entre entre 0 et 999 999.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Suffixe du document.
   */
  public Integer getSuffixe() {
    return suffixe;
  }
  
  /**
   * Retourner l'indicatif du document en fonction du type souhaité.
   */
  public String getIndicatif(int pTypeIndicatif) {
    switch (pTypeIndicatif) {
      case INDICATIF_NUM:
        return String.format("%0" + LONGUEUR_NUMERO + "d", numero);
      case INDICATIF_ENTETE_ETB_NUM_SUF:
        return String.format("%c%" + LONGUEUR_CODE_ETABLISSEMENT + "s%0" + LONGUEUR_NUMERO + "d%d", codeEntete.getCode(),
            getCodeEtablissement(), numero, suffixe);
      case INDICATIF_NUM_SUF:
        return String.format("%0" + LONGUEUR_NUMERO + "d%d", numero, suffixe);
    }
    return "";
  }
}
