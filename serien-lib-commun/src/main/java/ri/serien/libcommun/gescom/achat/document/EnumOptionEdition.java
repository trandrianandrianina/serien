/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Options d'édition pour un document d'achat.
 */
public enum EnumOptionEdition {
  CHIFFRE_AVEC_TOTAL("CHF", "Chiffr\ue009 avec total"),
  NON_CHIFFRE("NCH", "Non chiffr\ue009"),
  CHIFFRE("CHS", "Chiffr\ue009");
  // Cette option indique que l'on va choisir l'affectation d'impression d'un code état suivi de l'arobase, par exemple CDA@
  // C'est un chantier qu'il faudra faire en RPG : dissocier les chiffrage et le type denvoi par mail (qui est un choix d'affecttation)
  // TYPE_MAIL("@  ", "Type mail");
  
  private final String code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOptionEdition(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumOptionEdition valueOfByCode(String pCode) {
    for (EnumOptionEdition value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code d'édition est invalide : " + pCode);
  }
  
}
