/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.groupe;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de groupes d'articles.
 * 
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de groupes. Elle contient toutes les
 * méthodes oeuvrant sur la liste tandis que la classe Groupe contient les méthodes oeuvrant sur un seul groupe.
 */
public class ListeGroupe extends ListeClasseMetier<IdGroupe, Groupe, ListeGroupe> {
  /**
   * Constructeur.
   */
  public ListeGroupe() {
  }
  
  /**
   * Charger la liste des groupes suivant une liste d'identifiant.
   */
  @Override
  public ListeGroupe charger(IdSession pIdSession, List<IdGroupe> pListeId) {
    return null;
  }
  
  /**
   * Charger tout les groupes actifs de l'établissement.
   * Les groupes qui ont été annulés ne font pas partis de la liste retournée.
   */
  @Override
  public ListeGroupe charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    IdSession.controlerId(pIdSession, true);
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    CritereGroupe critere = new CritereGroupe();
    critere.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeGroupe(pIdSession, critere);
  }
}
