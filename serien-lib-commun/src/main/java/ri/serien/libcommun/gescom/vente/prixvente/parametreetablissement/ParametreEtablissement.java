/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.EnumTypeCumulConditionQuantitative;
import ri.serien.libcommun.gescom.commun.EnumTypeUniteConditionQuantitative;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.ListeFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.EnumTypeGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.GroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.IdGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.ListeGroupeConditionVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

/**
 * Classe regroupant tous les paramètres généraux pour le calcul du prix de vente.
 */
public class ParametreEtablissement implements Serializable {
  // Généralités
  private IdEtablissement idEtablissement = null;
  private Date dateSession = null;
  private Date moisEnCours = null;
  private String codeDevise = null;
  private IdGroupeConditionVente idGroupeConditionVenteGenerale = null;
  private IdGroupeConditionVente idGroupeConditionVentePromotionnelle = null;
  private EnumTypeUniteConditionQuantitative typeUniteConditionQuantitative = null;
  private EnumTypeCumulConditionQuantitative typeCumulConditionQuantitative = null;
  
  // Taux de TVA
  private BigPercentage tauxTVA1 = BigPercentage.ZERO;
  private BigPercentage tauxTVA2 = BigPercentage.ZERO;
  private BigPercentage tauxTVA3 = BigPercentage.ZERO;
  private BigPercentage tauxTVA4 = BigPercentage.ZERO;
  private BigPercentage tauxTVA5 = BigPercentage.ZERO;
  private BigPercentage tauxTVA6 = BigPercentage.ZERO;
  
  // Paramètres systèmes
  private Boolean modeNegocePS287 = null;
  private Character numeroColonneTarifPS105 = null;
  private Boolean rechercheColonneTarifNonNullePS305 = null;
  private Boolean prixPublicCommePrixBasePS316 = null;
  
  // Arrêt à la première condition trouvée
  private Character ps032 = null;
  // Prise en compte des conditions de vente quantitatives
  private Character ps047 = null;
  // Date d'application de la condition de vente
  private Character ps119 = null;
  // PUMP par magasin ou établissement
  private Boolean ps124 = null;
  // Emboîtage des conditions de ventes
  private Character ps216 = null;
  // Recherche des conditions de ventes sur fournisseur
  private Boolean ps253 = null;
  // Article non remisable
  private Character articleNonRemisablePS281 = null;
  
  // Condition prioritaire si la condition de ventes client/article définit un prix net (stocké en position 112 de la DTAAREA PGVMSPEM)
  // Si false alors cette condition de vente n'a pas la priorité et les autres conditions de ventes sont prises en compte
  private boolean conditionVenteClientArticlePourPrixNetPrioritaire = true;
  // Indique si les conditions de ventes référencées par le numéro du client doivent être ignorées lors de la récupération en table
  private boolean ignoreConditionVenteSurNumeroClient = false;
  // Liste des groupes de condition de ventes
  private ListeGroupeConditionVente listeGroupeConditionVente = null;
  // Liste des formules de prix
  private ListeFormulePrix listeFormulePrix = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier les informations liées à l'établisssment.
   * @param pEtablissement Etablissement.
   */
  public void modifierInformationEtablissement(Etablissement pEtablissement) {
    if (pEtablissement != null) {
      idEtablissement = pEtablissement.getId();
      moisEnCours = pEtablissement.getMoisEnCours();
      codeDevise = pEtablissement.getCodeDevise();
      idGroupeConditionVenteGenerale = pEtablissement.getIdGroupeConditionVenteGenerale();
      idGroupeConditionVentePromotionnelle = pEtablissement.getIdGroupeConditionVentePromotionnelle();
      typeUniteConditionQuantitative = pEtablissement.getTypeUniteConditionQuantitative();
      typeCumulConditionQuantitative = pEtablissement.getTypeCumulConditionQuantitative();
      tauxTVA1 = new BigPercentage(pEtablissement.getTauxTVA1(), EnumFormatPourcentage.POURCENTAGE);
      tauxTVA2 = new BigPercentage(pEtablissement.getTauxTVA2(), EnumFormatPourcentage.POURCENTAGE);
      tauxTVA3 = new BigPercentage(pEtablissement.getTauxTVA3(), EnumFormatPourcentage.POURCENTAGE);
      tauxTVA4 = new BigPercentage(pEtablissement.getTauxTVA4(), EnumFormatPourcentage.POURCENTAGE);
      tauxTVA5 = new BigPercentage(pEtablissement.getTauxTVA5(), EnumFormatPourcentage.POURCENTAGE);
      tauxTVA6 = new BigPercentage(pEtablissement.getTauxTVA6(), EnumFormatPourcentage.POURCENTAGE);
    }
    else {
      idEtablissement = null;
      moisEnCours = null;
      codeDevise = null;
      idGroupeConditionVenteGenerale = null;
      idGroupeConditionVentePromotionnelle = null;
      typeUniteConditionQuantitative = null;
      typeCumulConditionQuantitative = null;
      tauxTVA1 = null;
      tauxTVA2 = null;
      tauxTVA3 = null;
      tauxTVA4 = null;
      tauxTVA5 = null;
      tauxTVA6 = null;
    }
  }
  
  /**
   * Contrôle la validité d'un groupe de conditions de ventes.
   * Le contrôle s'effectue sur sa présence dans la liste des groupes qui a été filtré sur la période lors de son chargement.
   * 
   * @return
   */
  public boolean isGroupeConditionValide(IdGroupeConditionVente pIdGroupeConditionVente,
      EnumTypeGroupeConditionVente pTypeGroupeConditionVente) {
    if (pIdGroupeConditionVente == null) {
      return false;
    }
    if (listeGroupeConditionVente == null || listeGroupeConditionVente.isEmpty()) {
      return false;
    }
    GroupeConditionVente groupeConditionVente = listeGroupeConditionVente.get(pIdGroupeConditionVente);
    if (groupeConditionVente == null) {
      return false;
    }
    // Contrôle le type du groupe de la condition de ventes
    if (pTypeGroupeConditionVente != null && pTypeGroupeConditionVente != groupeConditionVente.getTypeConditionVente()) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Indiquer si la date passée en paramètre est dans le mois en cours.
   * @return true=dans le mois en cours, false=en dehors du mois en cours.
   */
  public boolean isDateDansPeriodeEnCours(Date pDate) {
    if (pDate == null || pDate.before(moisEnCours) || pDate.after(getMoisEnCoursMax())) {
      return false;
    }
    return true;
  }
  
  /**
   * Retourner l'exercice en cours maximum.
   * @return Exercice maximum.
   */
  public Date getMoisEnCoursMax() {
    Calendar moisEncoursMax = Calendar.getInstance();
    moisEncoursMax.setTime(getMoisEnCours());
    moisEncoursMax.add(Calendar.MONTH, 2);
    moisEncoursMax.add(Calendar.DAY_OF_MONTH, -1);
    return moisEncoursMax.getTime();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'identifiant de l'établisssement.
   * @return Identifiant de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'identifiant de l'établisssement.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourne la date de la session de l'utilisateur.
   * Contient la date du jour ou la date de la session car cette dernière peut être modifié (sauf au Comptoir à l'heure actuelle).
   * 
   * @return
   */
  public Date getDateSession() {
    return dateSession;
  }
  
  /**
   * Initialise la date de la session de l'utilisateur.
   * 
   * @param pDateSession
   */
  public void setDateSession(Date pDateSession) {
    this.dateSession = pDateSession;
  }
  
  /**
   * Retourne le mois en cours.
   * 
   * @return
   */
  public Date getMoisEnCours() {
    return moisEnCours;
  }
  
  /**
   * Initialise le mois en cours.
   * 
   * @param pMoisEnCours
   */
  public void setMoisEnCours(Date pMoisEnCours) {
    this.moisEnCours = pMoisEnCours;
  }
  
  /**
   * Initialise le mois en cours.
   * 
   * @param pMoisEnCours
   */
  public void setMoisEnCours(String pMoisEnCours) {
    this.moisEnCours = Constantes.convertirMoisAnneeEnDate(pMoisEnCours);
  }
  
  /**
   * Retourne le code de la devise.
   * 
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Initialise le code de la devise.
   * 
   * @param pCodeDevise
   */
  public void setCodeDevise(String pCodeDevise) {
    this.codeDevise = pCodeDevise;
  }
  
  /**
   * Initialise l'identifiant du groupe de la condition de ventes générale.
   * 
   * @return
   */
  public IdGroupeConditionVente getIdGroupeConditionVenteGenerale() {
    return idGroupeConditionVenteGenerale;
  }
  
  /**
   * Retourne l'identifiant du groupe de la condition de ventes générale.
   * 
   * @param pIdGroupeConditionVenteGenerale
   */
  public void setIdGroupeConditionVenteGenerale(IdGroupeConditionVente pIdGroupeConditionVenteGenerale) {
    this.idGroupeConditionVenteGenerale = pIdGroupeConditionVenteGenerale;
  }
  
  /**
   * Initialise l'identifiant du groupe de la condition de ventes promotionnelle.
   * 
   * @return
   */
  public IdGroupeConditionVente getIdGroupeConditionVentePromotionnelle() {
    return idGroupeConditionVentePromotionnelle;
  }
  
  /**
   * Retourne l'identifiant du groupe de la condition de ventes promotionnelle.
   * 
   * @param pIdGroupeConditionVentePromotionnelle
   */
  public void setIdGroupeConditionVentePromotionnelle(IdGroupeConditionVente pIdGroupeConditionVentePromotionnelle) {
    this.idGroupeConditionVentePromotionnelle = pIdGroupeConditionVentePromotionnelle;
  }
  
  /**
   * Retourne le type d'unité pour les conditions quantitatives.
   * 
   * @return
   */
  public EnumTypeUniteConditionQuantitative getTypeUniteConditionQuantitative() {
    return typeUniteConditionQuantitative;
  }
  
  /**
   * Initialise le type d'unité pour les conditions quantitatives.
   * 
   * @param pTypeUniteConditionQuantitative
   */
  public void setTypeUniteConditionQuantitative(EnumTypeUniteConditionQuantitative pTypeUniteConditionQuantitative) {
    this.typeUniteConditionQuantitative = pTypeUniteConditionQuantitative;
  }
  
  /**
   * Retourne le type du cumul pour les conditions quantitatives.
   * 
   * @return
   */
  public EnumTypeCumulConditionQuantitative getTypeCumulConditionQuantitative() {
    return typeCumulConditionQuantitative;
  }
  
  /**
   * Initialise le type du cumul pour les conditions quantitatives.
   * 
   * @param pTypeCumulConditionQuantitative
   */
  public void setTypeCumulConditionQuantitative(EnumTypeCumulConditionQuantitative pTypeCumulConditionQuantitative) {
    this.typeCumulConditionQuantitative = pTypeCumulConditionQuantitative;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs taux de TVA
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le taux de TVA correspondant au numéro de TVA passé en paramètre.
   * @param numeroTVA Numéro de TVA.
   * @return Taux de TVA (null si pas trouve).
   */
  public BigPercentage getTauxTVA(int pNumeroTVA) {
    switch (pNumeroTVA) {
      case 1:
        return getTauxTVA1();
      case 2:
        return getTauxTVA2();
      case 3:
        return getTauxTVA3();
      case 4:
        return getTauxTVA4();
      case 5:
        return getTauxTVA5();
      case 6:
        return getTauxTVA6();
      default:
        Trace.alerte("Le numéro de TVA des paramètres établissement est invalide : " + pNumeroTVA);
        return null;
    }
  }
  
  /**
   * Modifier le taux de TVA correspondant au numéro de TVA fourni.
   * @param pTauxTVA Taux de TVA (null si exempté).
   * @param numeroTVA Numéro de TVA.
   */
  public void setTauxTVA(BigPercentage pTauxTVA, int pNumeroTVA) {
    switch (pNumeroTVA) {
      case 1:
        setTauxTVA1(pTauxTVA);
        break;
      case 2:
        setTauxTVA2(pTauxTVA);
        break;
      case 3:
        setTauxTVA3(pTauxTVA);
        break;
      case 4:
        setTauxTVA4(pTauxTVA);
        break;
      case 5:
        setTauxTVA5(pTauxTVA);
        break;
      case 6:
        setTauxTVA6(pTauxTVA);
        break;
      default:
        Trace.alerte("Le numéro de TVA des paramètres établissement est invalide : " + pNumeroTVA);
        break;
    }
  }
  
  /**
   * Retourner le taux de TVA 1.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA1() {
    return tauxTVA1;
  }
  
  /**
   * Modifier le taux de TVA 1.
   * @param pTauxTVA Taux de TVA.
   */
  public void setTauxTVA1(BigPercentage pTauxTVA) {
    tauxTVA1 = pTauxTVA;
  }
  
  /**
   * Retourner le taux de TVA 2.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA2() {
    return tauxTVA2;
  }
  
  /**
   * Modifier le taux de TVA 2.
   * @param pTauxTVA Taux de TVA.
   */
  public void setTauxTVA2(BigPercentage pTauxTVA) {
    tauxTVA2 = pTauxTVA;
  }
  
  /**
   * Retourner le taux de TVA 3.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA3() {
    return tauxTVA3;
  }
  
  /**
   * Modifier le taux de TVA 3.
   * @param pTauxTVA Taux de TVA.
   */
  public void setTauxTVA3(BigPercentage pTauxTVA) {
    tauxTVA3 = pTauxTVA;
  }
  
  /**
   * Retourner le taux de TVA 4.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA4() {
    return tauxTVA4;
  }
  
  /**
   * Modifier le taux de TVA 4.
   * @param pTauxTVA Taux de TVA.
   */
  public void setTauxTVA4(BigPercentage pTauxTVA) {
    tauxTVA4 = pTauxTVA;
  }
  
  /**
   * Retourner le taux de TVA 5.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA5() {
    return tauxTVA5;
  }
  
  /**
   * Modifier le taux de TVA 5.
   * @param pTauxTVA Taux de TVA.
   */
  public void setTauxTVA5(BigPercentage pTauxTVA) {
    tauxTVA5 = pTauxTVA;
  }
  
  /**
   * Retourner le taux de TVA 6.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA6() {
    return tauxTVA6;
  }
  
  /**
   * Modifier le taux de TVA 6.
   * @param pTauxTVA Taux de TVA.
   */
  public void setTauxTVA6(BigPercentage pTauxTVA) {
    tauxTVA6 = pTauxTVA;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs paramètres systèmes
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Indiquer si le mode négoce est actif (PS287).
   * 
   * Si le PS287 est en mode négoce, le calcul de prix s'effectue suivant les spécificités négoce : 6 colonnes tarifs au lieu de 10,
   * calcul du taux de remise par rapport au prix de base HT de la colonne 1 plutôt que le prix de base HT de la colonne en cours.
   * 
   * @return true=mode négoce actif, false=mode standard actif.
   */
  public boolean isModeNegocePS287() {
    return modeNegocePS287 != null && modeNegocePS287;
  }
  
  /**
   * Modifier si le mode négoce est actif (PS287).
   * @param pPS287 true=mode négoce actif, false=mode standard actif.
   */
  public void setModeNegocePS287(Boolean pPS287) {
    modeNegocePS287 = pPS287;
  }
  
  /**
   * Retourner le numéro de la colonne tarif par défaut (PS105).
   * 
   * Le PS105 contient la colonne tarifaire par défaut si aucune autre colonne n'a pas être déterminée. Les valeurs 1 à 10 désignent les
   * colonnes tarifs 1 à 10. La valeur 0 signifie qu'il n'y aucune colonne tarif. La valeur null signifie que l'information n'a pas
   * été renseignée.
   * 
   * @return Numéro de la colonne tarif (0=aucune, 1-10=colonne tarif, null=non renseigné).
   */
  public Integer getNumeroColonneTarifPS105() {
    if (numeroColonneTarifPS105 != null) {
      return Constantes.convertirTexteEnInteger(numeroColonneTarifPS105.toString());
    }
    else {
      return null;
    }
  }
  
  /**
   * Modifier le numéro de la colonne tarif par défaut (PS105).
   * @param pNumeroColonneTarif Numéro de colonne tarif.
   */
  public void setNumeroColonneTarifPS105(Integer pNumeroColonneTarif) {
    if (pNumeroColonneTarif != null) {
      numeroColonneTarifPS105 = Character.forDigit(pNumeroColonneTarif, 10);
    }
    else {
      numeroColonneTarifPS105 = null;
    }
  }
  
  /**
   * Indiquer s'il faut rechercher la première colonne de tarif non null (PS305).
   * 
   * Lorsque le PS305 est actif, le logiciel teste si le prix de base HT de la colonne tarif utilisée pour le calcul est null ou égal à 0.
   * Si c'est le cas, on recherche le premier prix de base HT dans les colonnes tarifs précédentes jusqu'à trouver un prix supérieur
   * à 0.
   * 
   * @return true=recherche la première colonne de tarif précédente non nulle, false=sinon.
   */
  public boolean isRechercheColonneTarifNonNullePS305() {
    return rechercheColonneTarifNonNullePS305 != null && rechercheColonneTarifNonNullePS305;
  }
  
  /**
   * Modifier s'il faut rechercher la première colonne de tarif non null (PS305).
   * @param pPS305 true=recherche la première colonne de tarif précédente non nulle, false=sinon.
   */
  public void setRechercheColonneTarifNonNullePS305(Boolean pPS305) {
    rechercheColonneTarifNonNullePS305 = pPS305;
  }
  
  /**
   * Indiquer s'il faut utiliser le prix public (colonne tarif 1) comme prix de base HT (PS316).
   * 
   * Le prix de base HT est celui qui est affiché sur le document de vente et celui qui sert de référence pour le calcul du
   * taux de remise unitaire. Suivant la valeur du PS316, le prix de base HT est :
   * - PS316=false : le prix de base calcul HT (fonctionnement standard de Série N).
   * - PS316=true : le prix de base HT de la colonne 1 du tarif (désigné prix public dans ce cas).
   * 
   * @return false=le prix de base HT est le prix de base HT calculé, true=le prix de base HT est le prix public HT.
   */
  public boolean isPrixPublicCommePrixBasePS316() {
    return prixPublicCommePrixBasePS316 != null && prixPublicCommePrixBasePS316;
  }
  
  /**
   * Modifier s'il faut utiliser le prix public (colonne tarif 1) comme prix de base HT (PS316).
   * @param pPrixPublicCommePrixBasePS316 false=prix de base HT est le prix de base HT calculé, true=prix de base HT est le prix public.
   */
  public void setPrixPublicCommePrixBasePS316(Boolean pPrixPublicCommePrixBasePS316) {
    prixPublicCommePrixBasePS316 = pPrixPublicCommePrixBasePS316;
  }
  
  /**
   * Récupérer le mode de prise en compte des CNV pour les articles non remisables (PS281).
   * @return Nouvelle valeur PS281.
   */
  public Character getArticleNonRemisablePS281() {
    return articleNonRemisablePS281;
  }
  
  /**
   * Modifier le mode de prise en compte des CNV pour les articles non remisables (PS281).
   * @param pArticleNonRemisablePS281 Nouvelle valeur PS281.
   */
  public void setArticleNonRemisablePS281(Character pArticleNonRemisablePS281) {
    articleNonRemisablePS281 = pArticleNonRemisablePS281;
  }
  
  /**
   * Retourne le paramètre système PS032.
   * 
   * @return
   */
  public Character getPs032() {
    return ps032;
  }
  
  /**
   * Initialise le paramètre système PS032.
   * 
   * @param pPs032
   */
  public void setPs032(Character pPs032) {
    this.ps032 = pPs032;
  }
  
  /**
   * Retourne le paramètre système PS047.
   * 
   * @return
   */
  public Character getPs047() {
    return ps047;
  }
  
  /**
   * Initialise le paramètre système PS047.
   * 
   * @param pPs047
   */
  public void setPs047(Character pPs047) {
    this.ps047 = pPs047;
  }
  
  /**
   * Modifier le numéro de la colonne tarif du PS105.
   * @param pPS105 Valeur du PS105.
   */
  public void setNumeroColonneTarifPS105(Character pPS105) {
    numeroColonneTarifPS105 = pPS105;
  }
  
  /**
   * Retourne le paramètre système PS119.
   * 
   * @return
   */
  public Character getPs119() {
    return ps119;
  }
  
  /**
   * Initialise le paramètre système PS119.
   * 
   * @param pPs119
   */
  public void setPs119(Character pPs119) {
    this.ps119 = pPs119;
  }
  
  /**
   * Retourne le paramètre système PS124.
   * 
   * @return
   */
  public Boolean getPs124() {
    return ps124;
  }
  
  /**
   * Initialise le paramètre système PS124.
   * 
   * @param pPs124
   */
  public void setPs124(Boolean pPs124) {
    this.ps124 = pPs124;
  }
  
  /**
   * Retourne le paramètre système PS216.
   * 
   * @return
   */
  public Character getPs216() {
    return ps216;
  }
  
  /**
   * Initialise le paramètre système PS216.
   * 
   * @param pPs216
   */
  public void setPs216(Character pPs216) {
    this.ps216 = pPs216;
  }
  
  /**
   * Retourne le paramètre système PS253.
   * 
   * @return
   */
  public Boolean getPs253() {
    return ps253;
  }
  
  /**
   * Initialise le paramètre système PS253.
   * 
   * @param pPs253
   */
  public void setPs253(Boolean pPs253) {
    this.ps253 = pPs253;
  }
  
  /**
   * Retourne si la condition de vente Client/Article définissant un prix de vente est prioritaire si elle existe.
   * 
   * @return
   */
  public boolean isConditionVenteClientArticlePourPrixNetPrioritaire() {
    return conditionVenteClientArticlePourPrixNetPrioritaire;
  }
  
  /**
   * Initialise si la condition de vente Client/Article définissant un prix de vente est prioritaire si elle existe.
   * 
   * @param pConditionVenteClientArticlePourPrixNetPrioritaire
   */
  public void setConditionVenteClientArticlePourPrixNetPrioritaire(boolean pConditionVenteClientArticlePourPrixNetPrioritaire) {
    this.conditionVenteClientArticlePourPrixNetPrioritaire = pConditionVenteClientArticlePourPrixNetPrioritaire;
  }
  
  /**
   * Retourne si les conditions de ventes référencées par le numéro du client doivent être ignorées lors de la récupération en table.
   * 
   * @return
   */
  public boolean isIgnoreConditionVenteSurNumeroClient() {
    return ignoreConditionVenteSurNumeroClient;
  }
  
  /**
   * Initialise les conditions de ventes référencées par le numéro du client doivent être ignorées lors de la récupération en table.
   * 
   * @param pIgnoreConditionVenteSurNumeroClient
   */
  public void setIgnoreConditionVenteSurNumeroClient(boolean pIgnoreConditionVenteSurNumeroClient) {
    this.ignoreConditionVenteSurNumeroClient = pIgnoreConditionVenteSurNumeroClient;
  }
  
  /**
   * Retourne une liste de groupes de condition de ventes.
   * 
   * @return
   */
  public ListeGroupeConditionVente getListeGroupeConditionVente() {
    return listeGroupeConditionVente;
  }
  
  /**
   * Initialise une liste de groupes de condition de ventes.
   * 
   * @param pListeGroupeConditionVente
   */
  public void setListeGroupeConditionVente(ListeGroupeConditionVente pListeGroupeConditionVente) {
    this.listeGroupeConditionVente = pListeGroupeConditionVente;
  }
  
  /**
   * Retourne la liste des formules de prix.
   * 
   * @return
   */
  public ListeFormulePrix getListeFormulePrix() {
    return listeFormulePrix;
  }
  
  /**
   * Initialise la liste des formules de prix.
   * 
   * @param pListeFormulePrix
   */
  public void setListeFormulePrix(ListeFormulePrix pListeFormulePrix) {
    this.listeFormulePrix = pListeFormulePrix;
  }
  
}
