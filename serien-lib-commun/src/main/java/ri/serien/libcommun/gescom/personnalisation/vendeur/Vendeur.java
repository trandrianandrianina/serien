/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.vendeur;

import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.EnumUnitePeriode;
import ri.serien.libcommun.outils.Constantes;

/**
 * Vendeur.
 */
public class Vendeur extends AbstractClasseMetier<IdVendeur> {
  // Constantes
  public static final int LONGUEUR_NOM = 30;
  public static final int LONGUEUR_TELEPHONE = 20;
  public static final int LONGUEUR_EMAIL = 40;
  
  // Variables
  private String nom = null;
  private Integer validiteDevis = null;
  private EnumUnitePeriode uniteValiditeDevis = null;
  private Integer pourcentageMaximalRemise = null;
  private String numeroTelephone = null;
  private String adresseEmail = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public Vendeur(IdVendeur pIdVendeur) {
    super(pIdVendeur);
  }
  
  @Override
  public String getTexte() {
    return nom;
  }
  
  /**
   * Renvoyer la date de validité d'un devis par défaut pour ce vendeur.
   */
  public Date getDateDefautValiditeDevis() {
    // Date du jour
    Date maintenant = new Date();
    
    if (validiteDevis == null || validiteDevis.intValue() == 0) {
      return null;
    }
    
    if (uniteValiditeDevis == null) {
      uniteValiditeDevis = EnumUnitePeriode.JOUR;
    }
    
    switch (uniteValiditeDevis) {
      case JOUR:
        return Constantes.getDateDelaiJour(maintenant, validiteDevis);
      
      case SEMAINE:
        return Constantes.getDateDelaiSemaine(maintenant, validiteDevis);
      
      case MOIS:
        return Constantes.getDateDelaiMois(maintenant, validiteDevis);
      
      case ANNEE:
        return Constantes.getDateDelaiAnnee(maintenant, validiteDevis);
      
      default:
        return null;
    }
  }
  
  // -- Accesseurs
  
  /**
   * Nom du vendeur.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Nom du vendeur suivi de son code entre parenthèse
   * Exemple : Jean DUPOND (JDD).
   * Par sécurité, le code est retourné seul au cas où le nom ne serait pas renseigné.
   */
  public String getNomAvecCode() {
    if (nom != null) {
      return nom + " (" + getId().getCode() + ")";
    }
    else {
      return getId().getCode();
    }
  }
  
  /**
   * Renseigner le nom du vendeur.
   */
  public void setNom(String pNom) {
    nom = pNom;
  }
  
  /**
   * Longueur de la période de validité d'un devis par défaut
   */
  public Integer getValiditeDevis() {
    return validiteDevis;
  }
  
  /**
   * Renseigner la longueur de la période de validité d'un devis par défaut
   */
  public void setValiditeDevis(Integer pValiditeDevis) {
    validiteDevis = pValiditeDevis;
  }
  
  /**
   * Unité de la période de validité d'un devis par défaut
   */
  public EnumUnitePeriode getUniteValiditeDevis() {
    return uniteValiditeDevis;
  }
  
  /**
   * Renseigner l'unité de la période de validité d'un devis par défaut
   */
  public void setUniteValiditeDevis(EnumUnitePeriode pUniteValiditeDevis) {
    uniteValiditeDevis = pUniteValiditeDevis;
  }
  
  /**
   * Pourcentage de remise maximal autorisé à un vendeur
   */
  public Integer getPourcentageMaximalRemise() {
    return pourcentageMaximalRemise;
  }
  
  /**
   * Renseigner le pourcentage de remise maximal autorisé à un vendeur
   */
  public void setPourcentageMaximalRemise(Integer pPourcentageMaximalRemise) {
    pourcentageMaximalRemise = pPourcentageMaximalRemise;
  }
  
  /**
   * Retourner le numéro de téléphone du vendeur.
   */
  public String getNumeroTelephone() {
    return numeroTelephone;
  }
  
  /**
   * Renseigner le numéro de téléphone du vendeur.
   */
  public void setNumeroTelephone(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }
  
  /**
   * Retourner l'adresse email du vendeur.
   */
  public String getAdresseEmail() {
    return adresseEmail;
  }
  
  /**
   * Renseigner l'adresse email du vendeur.
   */
  public void setAdresseEmail(String adresseEmail) {
    this.adresseEmail = adresseEmail;
  }
  
}
