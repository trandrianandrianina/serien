/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatCommentaire;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.document.ListeTva;
import ri.serien.libcommun.gescom.commun.document.ListeZonePersonnalisee;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.IdAcheteur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;

public class DocumentAchat extends AbstractClasseMetier<IdDocumentAchat> {
  // Constantes
  public static final int LONGUEUR_REFERENCE_COMMANDE = 10;
  public static final int LONGUEUR_REFERENCE_LIVRAISON = 10;
  public static final int LONGUEUR_REFERENCE_INTERNE = 50;
  public static final int ETAPE_CREATION_DOCUMENT = 0;
  public static final int ETAPE_MODIFICATION_DOCUMENT = 1;
  public static final int ETAPE_CONSULTATION_DOCUMENT = 2;
  
  public static final int NOMBRE_LIGNES_ARTICLES_PAR_BLOC = 15;
  public static final int INCREMENT_LIGNE_NORMAL = 40;
  
  // Variables
  private Date dateCreation = null;
  private Date dateDernierTraitement = null;
  private Date dateHomologation = null;
  private Date dateReception = null;
  private Date dateFacturation = null;
  private EnumCodeEtatDocument codeEtat = null;
  private IdFournisseur idFournisseur = null;
  private BigDecimal totalTTC = null;
  private BigDecimal totalHTLignes = null;
  private IdMagasin idMagasinSortie = null;
  private IdAcheteur idAcheteur = null;
  private String referenceBonLivraison = null;
  private String referenceCommande = null;
  private Date dateLivraisonPrevue = null;
  private String referenceInterne = null;
  private Integer topSysteme = null;
  private Integer numeroFacture = null;
  private Integer topEdition = null;
  private Integer topFournisseurDeCommande = null;
  private Integer topMontantsFactureForces = null;
  private Integer rangReglement = null;
  private Integer topReleve = null;
  private Integer nombreExemplaire = null;
  private Integer nombreReglementsReels = null;
  private Integer signeGere = null;
  private Integer topLivraison = null;
  private Integer topComptabilisation = null;
  private Integer topPrixGaranti = null;
  private Integer topChiffrage = null;
  private Integer topAffectation = null;
  private List<IdDocumentAchat> listeIdDocumentOrigine = null;
  private Character codeAvoir = null;
  private Character codeTypeFacturation = null;
  private Character codeNatureAchat = null;
  private Reglements reglement = new Reglements();
  
  private ListeTva listeTva = new ListeTva();
  private BigDecimal pourcentageEscompte = null;
  private BigDecimal montantEscompte = null;
  private String codeSectionAnalytique = null;
  private String codeAffaire = null;
  private String codeLieuLivraison = null;
  private String codeJournalAchat = null;
  private String codeDevise = null;
  private BigDecimal tauxDeChange = null;
  // Frais d'approvisionnement
  private BigDecimal coefficientAchat = null;
  private Integer baseDevise = null;
  private String codeDossierApprovisionnement = null;
  private String codeContainer = null;
  private BigDecimal poids = null;
  private BigDecimal volume = null;
  private BigDecimal montantFraisRepartir = null;
  private ListeRemise listeRemiseLigne = null;
  private ListeRemise listeRemisePied = null;
  private ListeZonePersonnalisee listeTopPersonnalisable = null;
  private Character codeLitige = null;
  // private Character codeS33 = null;
  private Boolean receptionDefinitive = Boolean.FALSE;
  private Boolean comptabilisationEnStocksFlottants = Boolean.FALSE;
  private Boolean blocageComptabilisation = Boolean.FALSE;
  private String codeContrat = null;
  private Date dateConfirmationFournisseur = null;
  private Date dateIntermediaire1 = null;
  private Date dateIntermediaire2 = null;
  private Character codeEtatIntermediaire = null;
  private String codeModeExpedition = null;
  private String codeTransporteur = null;
  private Boolean traiteParCommandeDilicom = Boolean.FALSE;
  private Boolean blocageCommande = Boolean.FALSE;
  private Character codeScenarioDates = null;
  private Character codeEtatFraisFixes = null;
  private EnumTypeCommande codeTypeCommande = null;
  private Adresse adresseLivraison = null;
  private Adresse adresseFournisseur = null;
  private String indicatifContact = null;
  private IdDocumentAchat idDocumentPourExtraction = null;
  private EnumEtapeExtraction etapeExtraction = null;
  private Character codeTypePrixAchat = null;
  private BigDecimal montantPortTheorique = null;
  private BigDecimal montantPortFacture = null;
  private boolean fraisPortFournisseurFacture = true;
  
  // En fonction du mode d'expédition (comme pour le comptoir) *E ou *L
  private Boolean modeEnlevement = Boolean.FALSE;
  private int etape = ETAPE_CREATION_DOCUMENT;
  private ListeLigneAchat listeLigneAchat = new ListeLigneAchat();
  private ListeLigneAchat listeLigneAchatSupprimee = new ListeLigneAchat();
  private String libelleTypeDocument = "";
  // Permet de conserver le temps de la vie du document l'adresse de livraison si on alterne entre le mode d'enlèvement
  // et de livraison
  private Adresse sauvegardeAdresseLivraison = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public DocumentAchat(IdDocumentAchat pIdDocumentAchat) {
    super(pIdDocumentAchat);
  }
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  // -- Méthodes publiques
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdDocumentAchat controlerId(DocumentAchat pDocumentAchat, boolean pVerifierExistance) {
    if (pDocumentAchat == null) {
      throw new MessageErreurException("Le document d'achats est invalide.");
    }
    IdDocumentAchat idDocumentAchat = pDocumentAchat.getId();
    if (idDocumentAchat == null) {
      throw new MessageErreurException("L'identifiant du document d'achats est invalide.");
    }
    if (pVerifierExistance && !idDocumentAchat.isExistant()) {
      throw new MessageErreurException("L'identifiant du document d'achats n'existe pas dans la base de données : " + idDocumentAchat);
    }
    return idDocumentAchat;
  }
  
  /**
   * Vide la liste des lignes (pour initialisation).
   */
  public void viderListes() {
    listeLigneAchat.clear();
    listeLigneAchatSupprimee.clear();
  }
  
  /**
   * Retourne si le document est en cours de création.
   */
  public boolean isEnCoursCreation() {
    return etape == ETAPE_CREATION_DOCUMENT;
  }
  
  /**
   * Retourne si le document est en cours de modification.
   */
  public boolean isEnCoursModification() {
    return etape == ETAPE_MODIFICATION_DOCUMENT;
  }
  
  /**
   * Retourne si le document est modifiable.
   */
  public boolean isModifiable() {
    if (isEnCoursCreation()) {
      return true;
    }
    if (isEnCoursModification() && !isCommandeReceptionnee()) {
      return true;
    }
    
    return false;
  }
  
  /**
   * Retourne si le document est une facture d'achat. False, c'est une commande, une réception ou un transfert.
   */
  public boolean isFacture() {
    return id.getCodeEntete().equals(EnumCodeEnteteDocumentAchat.FACTURE);
  }
  
  /**
   * Retourne si le document est une commande d'achat. False, c'est une facture, une réception ou un transfert.
   */
  public boolean isCommande() {
    return id.getSuffixe() == 0 && (id.getCodeEntete().equals(EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION)
        || id.getCodeEntete().equals(EnumCodeEnteteDocumentAchat.COMMANDE_ORIGINE));
  }
  
  /**
   * Retourne si le document est une réception d'achat. False, c'est une facture, une commande ou un transfert.
   */
  public boolean isReception() {
    return id.getSuffixe() > 0 && (id.getCodeEntete().equals(EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION)
        || id.getCodeEntete().equals(EnumCodeEnteteDocumentAchat.COMMANDE_ORIGINE));
  }
  
  /**
   * Retourne le type du document d'achat.
   */
  public EnumTypeDocumentAchat retournerTypeDocument() {
    if (isCommande()) {
      return EnumTypeDocumentAchat.COMMANDE;
    }
    else if (isReception()) {
      return EnumTypeDocumentAchat.RECEPTION;
    }
    else if (isFacture()) {
      return EnumTypeDocumentAchat.FACTURE;
    }
    
    return EnumTypeDocumentAchat.NON_DEFINI;
  }
  
  /**
   * Indique s'il faut afficher la colonne quantité d'orgine pour les lignes articles.
   */
  public boolean isAfficherColonneOrigine() {
    return isEnCoursModification() || listeIdDocumentOrigine != null;
  }
  
  /**
   * Retourne la liste des id des documents d'origine formaté sous forme de chaine.
   */
  public String retournerChaineIdDocumentOrigine() {
    if (listeIdDocumentOrigine == null || listeIdDocumentOrigine.isEmpty()) {
      return "";
    }
    
    String chaine = "";
    for (IdDocumentAchat id : listeIdDocumentOrigine) {
      chaine += id.getTexte() + ", ";
    }
    chaine = chaine.trim();
    return chaine.substring(0, chaine.length() - 1);
  }
  
  /**
   * Calcule le numéro de ligne de l'article dans le document à partir de son indice dans la table.
   * TODO l'algo est perfectible si on insère plusieurs fois au même endroit car les numéros sont mémorisés.
   * Idéalement il faudrait un outil qui recalcule les numéros de lignes du document.
   */
  public int calculerNumeroLigneArticle(int pIndiceLigne) {
    int numeroLigne;
    
    // La liste des lignes est vide
    if (listeLigneAchat.isEmpty() && listeLigneAchatSupprimee.isEmpty()) {
      return INCREMENT_LIGNE_NORMAL;
    }
    
    // La ligne est insérée en tête de liste
    if (pIndiceLigne == 0) {
      numeroLigne = listeLigneAchat.get(0).getId().getNumeroLigne().intValue() / 2;
      // Vérifier que le numéro n'est pas déjà pris par une ligne supprimée
      while (!verifierNumeroLigneArticle(numeroLigne)) {
        numeroLigne = numeroLigne / 2;
      }
      if (numeroLigne == 0) {
        throw new MessageErreurException(
            "L'article ne peut pas être inséré à cet endroit car il n'y a plus de numéro de ligne disponible.");
      }
      return numeroLigne;
    }
    
    // La ligne est insérée entre 2 lignes
    if (pIndiceLigne > -1 && pIndiceLigne < listeLigneAchat.size()) {
      int numeroPrecedent = listeLigneAchat.get(pIndiceLigne - 1).getId().getNumeroLigne().intValue();
      int numeroSuivant = listeLigneAchat.get(pIndiceLigne).getId().getNumeroLigne().intValue();
      int increment = (numeroSuivant - numeroPrecedent) / 2;
      if (increment == 0) {
        throw new MessageErreurException(
            "L'article ne peut pas être inséré à cet endroit car il n'y a plus de numéro de ligne disponible.");
      }
      
      int numeroNouvelleLigne = numeroPrecedent + increment;
      
      // Vérifier que le numéro n'est pas déjà pris par une ligne supprimée
      while (!verifierNumeroLigneArticle(numeroNouvelleLigne)) {
        numeroNouvelleLigne = numeroNouvelleLigne + ((numeroSuivant - numeroNouvelleLigne) / 2);
      }
      return numeroNouvelleLigne;
    }
    
    // Sinon la ligne est insérée à la fin (hors numéro de lignes réservés pour les remises (sous les 9699))
    // Recherche la dernière ligne article hors remises spéciales
    int i = listeLigneAchat.size() - 1;
    while (i >= 0 && listeLigneAchat.get(i).isLigneRemiseSpeciales()) {
      i--;
    }
    if (i < 0) {
      numeroLigne = INCREMENT_LIGNE_NORMAL;
    }
    else {
      numeroLigne = listeLigneAchat.get(i).getId().getNumeroLigne();
    }
    int reste = numeroLigne % INCREMENT_LIGNE_NORMAL;
    if (reste != 0) {
      numeroLigne = ((numeroLigne / INCREMENT_LIGNE_NORMAL) * INCREMENT_LIGNE_NORMAL);
    }
    numeroLigne += INCREMENT_LIGNE_NORMAL;
    
    // Contrôle que le numéro de ligne ne soit pas déjà utilisé
    while (!verifierNumeroLigneArticle(numeroLigne)) {
      numeroLigne += INCREMENT_LIGNE_NORMAL;
    }
    
    return numeroLigne;
  }
  
  /**
   * Vérifier que le numéro de ligne ne soit pas déjà utilisé (dans les lignes supprimées et lignes document).
   */
  public boolean verifierNumeroLigneArticle(int pNumero) {
    for (LigneAchat ligneAnnulee : listeLigneAchatSupprimee) {
      if (pNumero == ligneAnnulee.getId().getNumeroLigne()) {
        return false;
      }
    }
    for (LigneAchat ligneAchat : listeLigneAchat) {
      if (pNumero == ligneAchat.getId().getNumeroLigne()) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Retourne l'index d'une ligne achat à partir de son id.
   */
  public int retournerIndexLigneAchatAvecId(IdLigneAchat pIdLigneAchat) {
    if ((pIdLigneAchat == null) || (listeLigneAchat == null)) {
      return -1;
    }
    
    for (int index = 0; index < listeLigneAchat.size(); index++) {
      if (listeLigneAchat.get(index).getId().equals(pIdLigneAchat)) {
        return index;
      }
    }
    return -1;
  }
  
  /**
   * Retourne une ligne achat à partir de son id.
   */
  public LigneAchat retournerLigneAchatAvecId(IdLigneAchat pIdLigneAchat) {
    if ((pIdLigneAchat == null) || (listeLigneAchat == null)) {
      return null;
    }
    
    for (LigneAchat ligne : listeLigneAchat) {
      if (ligne.getId().equals(pIdLigneAchat)) {
        return ligne;
      }
    }
    return null;
  }
  
  /**
   * Génère une ligne article à partir d'un article.
   */
  public LigneAchatArticle convertirArticleEnLigne(Article pArticle, BigDecimal pQuantite, int pNumeroLigne) {
    IdLigneAchat idLigneAchat = IdLigneAchat.getInstance(id, pNumeroLigne);
    
    LigneAchatArticle ligneAchat = new LigneAchatArticle(idLigneAchat);
    ligneAchat.setTypeLigne(EnumTypeLigneAchat.COURANTE_STOCKEE);
    if (pArticle.isArticlePalette()) {
      ligneAchat.setTypeArticle(EnumTypeArticle.PALETTE);
    }
    ligneAchat.setIdArticle(pArticle.getId());
    // ligneAchat.setLibelleArticle(pArticle.getLibelleComplet());
    PrixAchat prixAchat = ligneAchat.getPrixAchat();
    if (prixAchat == null) {
      // prixAchat = new PrixAchat();
      prixAchat = pArticle.getPrixAchat().clone();
      ligneAchat.setPrixAchat(prixAchat);
    }
    
    prixAchat.setQuantiteReliquatUCA(pQuantite, true);
    
    ligneAchat.setIdMagasin(idMagasinSortie);
    
    return ligneAchat;
  }
  
  /**
   * Génère une ligne commentaire.
   */
  public LigneAchatCommentaire ajouterLigneArticleCommentaire(IdSession pIdSession, Article pArticle, int pNumeroLigne) {
    // Calculer le numéro de la ligne
    IdLigneAchat idLigneAchat =
        IdLigneAchat.getInstance(id.getIdEtablissement(), id.getCodeEntete(), id.getNumero(), id.getSuffixe(), pNumeroLigne);
    
    // Créer la ligne de ventes
    LigneAchatCommentaire ligneAchat = new LigneAchatCommentaire(idLigneAchat);
    ligneAchat.setIdArticle(pArticle.getId());
    ligneAchat.setLibelleArticle(pArticle.getLibelleComplet());
    ligneAchat.setTypeArticle(EnumTypeArticle.COMMENTAIRE);
    
    return ligneAchat;
  }
  
  /**
   * Calcule l'indice d'une ligne dans le document à partir de son numéro de ligne
   */
  public int getIndiceLigne(int pNumeroLigne) {
    int indice = -1;
    for (LigneAchat ligneAchat : listeLigneAchat) {
      indice++;
      if (ligneAchat.getId().getNumeroLigne().compareTo(pNumeroLigne) == 0) {
        return indice;
      }
    }
    return -1;
  }
  
  /**
   * Retourne le total HT des lignes sans le montant du port .
   */
  public BigDecimal getTotalHTLignesSansPort() {
    BigDecimal port = BigDecimal.ZERO;
    if (montantPortFacture != null) {
      port = montantPortFacture;
    }
    return totalHTLignes.subtract(port);
  }
  
  /**
   * Calcule et retourne le montant manquant pour atteindre le montant minimum de commande.
   * Il s'agit de la différence entre le montant mini pour une commande et le total HT de la commande en cours.
   */
  public BigDecimal getMontantManquantPourAtteindreMontantMinimumCommande(BigDecimal pMontantMinimum) {
    if (pMontantMinimum == null) {
      throw new MessageErreurException("Le montant minimum de commande du fournisseur est incorrect.");
    }
    if (totalHTLignes == null) {
      totalHTLignes = BigDecimal.ZERO;
    }
    BigDecimal montantManquant = pMontantMinimum.subtract(totalHTLignes);
    // Si le montant est négatif c'est que le montant minimum de commande est atteint et on met 0 dans le montant
    // manquant
    if (montantManquant.compareTo(BigDecimal.ZERO) < 0) {
      montantManquant = BigDecimal.ZERO;
    }
    return montantManquant;
  }
  
  /**
   * Calcule et retourne le montant qu'il manque pour atteindre le montant franco de port.
   * Il s'agit de la différence entre le montant franco fournit par le fournisseur et le total HT des lignes du
   * document.
   */
  public BigDecimal getMontantManquantPourAtteindreFrancoPort(BigDecimal pMontantFrancoPortFournisseur) {
    if (pMontantFrancoPortFournisseur == null) {
      throw new MessageErreurException("Le montant franco de port du fournisseur est incorrect.");
    }
    if (totalHTLignes == null) {
      totalHTLignes = BigDecimal.ZERO;
    }
    BigDecimal montantManquant = pMontantFrancoPortFournisseur.subtract(totalHTLignes);
    // Si le montant est négatif c'est que le montant franco est atteint et on met 0 dans le montant manquant
    if (montantManquant.compareTo(BigDecimal.ZERO) < 0) {
      montantManquant = BigDecimal.ZERO;
    }
    return montantManquant;
  }
  
  /**
   * Indique si le montant document a atteint le montant franco de port du fournisseur.
   * Si le montant franco fournisseur vaut 0 alors il y a toujours des frais de port.
   */
  public boolean isMontantFrancoPortEstAtteint(BigDecimal pMontantFrancoPortFournisseur) {
    if (pMontantFrancoPortFournisseur == null) {
      throw new MessageErreurException("Le montant franco de port du fournisseur est incorrect.");
    }
    // Dans le cas où le montant Franco de port vaut 0 alors c'est qu'il y a toujours des frais de port quelques soit le
    // montant de la
    // commande
    if (pMontantFrancoPortFournisseur.compareTo(BigDecimal.ZERO) == 0) {
      return false;
    }
    // Si un montant Franco fournisseur a été enregistré
    BigDecimal montantManquant = getMontantManquantPourAtteindreFrancoPort(pMontantFrancoPortFournisseur);
    return montantManquant.compareTo(BigDecimal.ZERO) == 0;
  }
  
  /**
   * Convertie une liste d'indice de ligne achat en liste d'id de ligne achat.
   */
  public List<IdLigneAchat> convertirListeIndiceEnIdLigneArticle(int[] pListeIndicesLignes) {
    if (pListeIndicesLignes == null || pListeIndicesLignes.length == 0) {
      return null;
    }
    if (listeLigneAchat == null || listeLigneAchat.isEmpty()) {
      return null;
    }
    
    // Conversion des indices des lignes en lignes articles
    ArrayList<LigneAchat> listeLignesArticlesASupprimer = new ArrayList<LigneAchat>();
    for (int i = 0; i < pListeIndicesLignes.length; i++) {
      if ((pListeIndicesLignes[i] < 0) || (pListeIndicesLignes[i] >= listeLigneAchat.size())) {
        continue;
      }
      listeLignesArticlesASupprimer.add(listeLigneAchat.get(pListeIndicesLignes[i]));
    }
    
    // On parcourt la liste à l'envers pour la suppression des lignes articles
    List<IdLigneAchat> listeId = new ArrayList<IdLigneAchat>();
    int i = listeLignesArticlesASupprimer.size() - 1;
    while (i >= 0) {
      LigneAchat ligneAchat = listeLignesArticlesASupprimer.get(i);
      listeId.add(ligneAchat.getId());
      i--;
    }
    return listeId;
  }
  
  // -- Méthodes statiques
  
  /**
   * Lecture des lignes du document.
   */
  public static DocumentAchat lireLignesDocument(IdSession pIdSession, DocumentAchat pDocument, boolean pAvecHistorique) {
    if (pIdSession == null) {
      throw new MessageErreurException("L'id de la session est invalide.");
    }
    if (pDocument == null) {
      throw new MessageErreurException("Le document est invalide.");
    }
    
    // Netttoyage des listes des lignes de ventes
    pDocument.viderListes();
    // Initialisation des critères de recherche
    CritereLigneAchat criteres = new CritereLigneAchat();
    criteres.setIdDocumentAchat(pDocument.getId());
    
    // Rechargement du document afin de récupérer les informations à rafraichir comme les montants
    pDocument = ManagerServiceDocumentAchat.chargerDocumentAchat(pIdSession, pDocument.getId(), pDocument, pAvecHistorique);
    if (pDocument == null) {
      return null;
    }
    
    // Récupèration de toutes les lignes du document
    ListeLigneAchat listeLigneAchat = ListeLigneAchat.charger(pIdSession, criteres);
    if (listeLigneAchat != null && !listeLigneAchat.isEmpty()) {
      // Ajout des lignes lues dans la classe Document
      for (LigneAchat ligneAchat : listeLigneAchat) {
        // Contrôle que toutes les lignes du document ont été parcouru
        if (ligneAchat == null) {
          continue;
        }
        
        // En fonction de son état on stocke la ligne dans la liste qui va bien
        if (ligneAchat.isAnnulee()) {
          pDocument.getListeLigneAchatSupprimee().add(ligneAchat);
        }
        else {
          pDocument.getListeLigneAchat().add(ligneAchat);
        }
      }
    }
    
    return pDocument;
  }
  
  /**
   * Supprime une liste de lignes articles à partir de leur id.
   */
  public static DocumentAchat supprimerListeLigneAchat(IdSession pIdSession, DocumentAchat pDocument,
      List<IdLigneAchat> pListeIdLignesASupprimer, String pUtilisateur) {
    if (pIdSession == null) {
      throw new MessageErreurException("L'id de la session est invalide.");
    }
    if (pDocument == null) {
      throw new MessageErreurException("Le document est invalide.");
    }
    if (pListeIdLignesASupprimer == null || pListeIdLignesASupprimer.isEmpty()) {
      throw new MessageErreurException("La liste des id des lignes à supprimer est vide.");
    }
    if (pDocument.getListeLigneAchat() == null || pDocument.getListeLigneAchat().isEmpty()) {
      return pDocument;
    }
    
    // A partir des id on construit une liste des lignes à supprimer
    List<LigneAchat> listeASupprimer = new ArrayList<LigneAchat>();
    for (IdLigneAchat id : pListeIdLignesASupprimer) {
      listeASupprimer.add(pDocument.retournerLigneAchatAvecId(id));
    }
    // Suppression des lignes articles contenues dans la liste
    ManagerServiceDocumentAchat.supprimerListeLigneDocument(pIdSession, listeASupprimer);
    pDocument = DocumentAchat.lireLignesDocument(pIdSession, pDocument, false);
    
    return pDocument;
  }
  
  /**
   * Permet de contrôler si des frais de port sont facturés à partir de mode de livraison, du montant franco et si le
   * fournisseur
   * facture les frais de port
   */
  public static DocumentAchat controlerFacturationFraisPort(IdSession pIdSession, DocumentAchat pDocument, Fournisseur pFournisseur,
      String pProfilUtilisateur) {
    if (pDocument == null) {
      throw new MessageErreurException("Le document d'achat est incorrect.");
    }
    if (pFournisseur == null) {
      throw new MessageErreurException("Le fournisseur est incorrect.");
    }
    
    // On mode enlèvement, les frais de port ne sont pas appliqués
    if (pDocument.isModeEnlevement()) {
      pDocument.setFraisPortFournisseurFacture(false);
    }
    // Si le fournisseur ne facture pas les frais de port alors on initialise la valeur à false
    else if (!pFournisseur.isFactureFraisPort()) {
      pDocument.setFraisPortFournisseurFacture(false);
    }
    // Si le fournisseur facture les frais de port, on contrôle que si le montant franco est atteint
    else if (pDocument.isMontantFrancoPortEstAtteint(pFournisseur.getMontantFrancoPort())) {
      pDocument.setFraisPortFournisseurFacture(false);
    }
    // Dans tous les autres cas des frais de port sont facturés
    else {
      pDocument.setFraisPortFournisseurFacture(true);
    }
    return chiffrerDocument(pIdSession, pDocument, pProfilUtilisateur);
  }
  
  /**
   * Rechiffre les montants du document d'achat en enregistrant le document avec les nouvelles valeurs et en rechargeant
   * le document.
   */
  public static DocumentAchat chiffrerDocument(IdSession pIdSession, DocumentAchat pDocument, String pProfilUtilisateur) {
    if (pDocument != null && pDocument.isExistant()) {
      pDocument = ManagerServiceDocumentAchat.sauverDocumentAchat(pIdSession, pProfilUtilisateur, pDocument);
      pDocument = DocumentAchat.lireLignesDocument(pIdSession, pDocument, false);
    }
    return pDocument;
  }
  
  /**
   * Retourne si la commande a été réceptionnée ou pas (uniquement pour un document de type commande).
   */
  public boolean isCommandeReceptionnee() {
    if (!isCommande()) {
      return false;
    }
    if (getEtapeExtraction() == EnumEtapeExtraction.AUCUNE) {
      return false;
    }
    return true;
  }
  
  // -- Méthodes privées
  
  /**
   * Permet de gérer simplement le changement de mode Livraison/Enlèvement.
   * L'adresse de livraison est provisoirement mémorisée lors du basculement en mode Enlèvement au cas où.
   */
  private void initialiserAdresseLivraison() {
    if (modeEnlevement == null) {
      return;
    }
    // Dans le cas d'une commande direct usine on n'efface pas l'adresse de livraison car il y en a toujours une,
    // contenant :
    // - soit l'adresse du client qui va enlevé la marchandise
    // - soir l'adresse où doit être livré la marchandise
    if (isDirectUsine()) {
      return;
    }
    // Dans le cas d'une commande normale
    // Si on passe en mode Enlèvement on mémorise l'adresse de livraison
    if (isModeEnlevement()) {
      if (adresseLivraison != null) {
        sauvegardeAdresseLivraison = adresseLivraison.clone();
      }
      adresseLivraison = null;
    }
    // On passe en mode Livraison on restaure l'adresse qui avait été mémorisé
    else {
      if (sauvegardeAdresseLivraison != null) {
        adresseLivraison = sauvegardeAdresseLivraison.clone();
      }
    }
  }
  
  // -- Accesseurs
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public Date getDateDernierTraitement() {
    return dateDernierTraitement;
  }
  
  public void setDateDernierTraitement(Date dateDernierTraitement) {
    this.dateDernierTraitement = dateDernierTraitement;
  }
  
  public Date getDateHomologation() {
    return dateHomologation;
  }
  
  public void setDateHomologation(Date dateHomologation) {
    this.dateHomologation = dateHomologation;
  }
  
  public Date getDateReception() {
    return dateReception;
  }
  
  public void setDateReception(Date dateReception) {
    this.dateReception = dateReception;
  }
  
  public Date getDateFacturation() {
    return dateFacturation;
  }
  
  public void setDateFacturation(Date dateFacturation) {
    this.dateFacturation = dateFacturation;
  }
  
  public EnumCodeEtatDocument getCodeEtat() {
    return codeEtat;
  }
  
  public void setCodeEtat(EnumCodeEtatDocument codeEtat) {
    this.codeEtat = codeEtat;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
  
  public BigDecimal getTotalTTC() {
    return totalTTC;
  }
  
  public void setTotalTTC(BigDecimal totalTTC) {
    this.totalTTC = totalTTC;
  }
  
  public IdMagasin getIdMagasinSortie() {
    return idMagasinSortie;
  }
  
  public void setIdMagasinSortie(IdMagasin pIdMagasin) {
    this.idMagasinSortie = pIdMagasin;
  }
  
  public IdAcheteur getIdAcheteur() {
    return idAcheteur;
  }
  
  public void setIdAcheteur(IdAcheteur pIdAcheteur) {
    idAcheteur = pIdAcheteur;
  }
  
  /**
   * Référence de la commande chez le fournisseur.
   * Permet de mémoriser l'identifiant que le fournisseur à attribué à cette commande.
   * Correspond au champ RBC dans la base de données.
   * Ne pas confondre avec :
   * - Référence livraison : identifiant du bon de livraison chez le fournisseur.
   * - Référence interne : pour qualifier le document d'achats en interne.
   */
  public String getReferenceCommande() {
    return referenceCommande;
  }
  
  /**
   * Modifier la référence de la commande chez le fournisseur.
   */
  public void setReferenceCommande(String pReferenceCommande) {
    if (pReferenceCommande == null) {
      referenceCommande = "";
    }
    else if (pReferenceCommande.length() > LONGUEUR_REFERENCE_COMMANDE) {
      throw new MessageErreurException(
          "La référence commande du document d'achats doit être inférieure ou égale à " + LONGUEUR_REFERENCE_COMMANDE + " caractères");
    }
    else {
      referenceCommande = pReferenceCommande.trim();
    }
  }
  
  /**
   * Référence du bon de livraison du fournisseur.
   * Permet de mémoriser l'identifiant que le fournisseur à attribué au bon de livraison correspondant à cette commande.
   * Ne pas confondre avec :
   * - Référence commande : identifiant de la commande chez le fournisseur.
   * - Référence interne : pour qualifier le document d'achats en interne.
   */
  public String getReferenceBonLivraison() {
    return referenceBonLivraison;
  }
  
  public void setReferenceBonLivraison(String pReferenceBonLivraison) {
    if (pReferenceBonLivraison == null) {
      referenceBonLivraison = "";
    }
    else if (pReferenceBonLivraison.length() > LONGUEUR_REFERENCE_LIVRAISON) {
      throw new MessageErreurException(
          "La référence livraison du document d'achats doit être inférieure ou égale à " + LONGUEUR_REFERENCE_LIVRAISON + " caractères");
    }
    else {
      referenceBonLivraison = pReferenceBonLivraison.trim();
    }
  }
  
  /**
   * Référence interne du document d'achats.
   * Permet de qualifier le document d'achats, de donner une indication permettant de savoir sa raison d'être.
   * Correspond au champ RFL dans la base de données.
   * Ne pas confondre avec :
   * - Référence commande : identifiant de la commande chez le fournisseur.
   * - Référence livraison : identifiant du bon de livraison chez le fournisseur.
   */
  public String getReferenceInterne() {
    return referenceInterne;
  }
  
  /**
   * Modifier la référence interne.
   */
  public void setReferenceInterne(String pReferenceInterne) {
    if (pReferenceInterne == null) {
      referenceInterne = "";
    }
    else if (pReferenceInterne.length() > LONGUEUR_REFERENCE_INTERNE) {
      throw new MessageErreurException(
          "La référence interne du document d'achats doit être inférieure ou égale à " + LONGUEUR_REFERENCE_INTERNE + " caractères");
    }
    else {
      referenceInterne = pReferenceInterne.trim();
    }
  }
  
  public Boolean isModeEnlevement() {
    return modeEnlevement;
  }
  
  public Boolean isModeLivraison() {
    return modeEnlevement != null && !modeEnlevement;
  }
  
  public void setModeEnlevement(Boolean modeEnlevement) {
    this.modeEnlevement = modeEnlevement;
    initialiserAdresseLivraison();
  }
  
  public Date getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  public void setDateLivraisonPrevue(Date dateLivraisonPrevue) {
    this.dateLivraisonPrevue = dateLivraisonPrevue;
  }
  
  public ListeLigneAchat getListeLigneAchat() {
    return listeLigneAchat;
  }
  
  public ListeLigneAchat getListeLigneAchatSupprimee() {
    return listeLigneAchatSupprimee;
  }
  
  public Integer getTopSysteme() {
    return topSysteme;
  }
  
  public void setTopSysteme(Integer topSysteme) {
    this.topSysteme = topSysteme;
  }
  
  public Integer getNumeroFacture() {
    return numeroFacture;
  }
  
  public void setNumeroFacture(Integer numeroFacture) {
    this.numeroFacture = numeroFacture;
  }
  
  public Integer getTopEdition() {
    return topEdition;
  }
  
  public void setTopEdition(Integer topEdition) {
    this.topEdition = topEdition;
  }
  
  public Integer getTopFournisseurDeCommande() {
    return topFournisseurDeCommande;
  }
  
  public void setTopFournisseurDeCommande(Integer topFournisseurDeCommande) {
    this.topFournisseurDeCommande = topFournisseurDeCommande;
  }
  
  public Integer getTopMontantsFactureForces() {
    return topMontantsFactureForces;
  }
  
  public void setTopMontantsFactureForces(Integer topMontantsFactureForces) {
    this.topMontantsFactureForces = topMontantsFactureForces;
  }
  
  public Integer getRangReglement() {
    return rangReglement;
  }
  
  public void setRangReglement(Integer rangReglement) {
    this.rangReglement = rangReglement;
  }
  
  public Integer getTopReleve() {
    return topReleve;
  }
  
  public void setTopReleve(Integer topReleve) {
    this.topReleve = topReleve;
  }
  
  public Integer getNombreExemplaire() {
    return nombreExemplaire;
  }
  
  public void setNombreExemplaire(Integer nombreExemplaire) {
    this.nombreExemplaire = nombreExemplaire;
  }
  
  public Integer getNombreReglementsReels() {
    return nombreReglementsReels;
  }
  
  public void setNombreReglementsReels(Integer nombreReglementsReels) {
    this.nombreReglementsReels = nombreReglementsReels;
  }
  
  public Integer getSigneGere() {
    return signeGere;
  }
  
  public void setSigneGere(Integer signeGere) {
    this.signeGere = signeGere;
  }
  
  public Integer getTopLivraison() {
    return topLivraison;
  }
  
  public void setTopLivraison(Integer topLivraison) {
    this.topLivraison = topLivraison;
  }
  
  public Integer getTopComptabilisation() {
    return topComptabilisation;
  }
  
  public void setTopComptabilisation(Integer topComptabilisation) {
    this.topComptabilisation = topComptabilisation;
  }
  
  public Integer getTopPrixGaranti() {
    return topPrixGaranti;
  }
  
  public void setTopPrixGaranti(Integer topPrixGaranti) {
    this.topPrixGaranti = topPrixGaranti;
  }
  
  public Integer getTopChiffrage() {
    return topChiffrage;
  }
  
  public void setTopChiffrage(Integer topChiffrage) {
    this.topChiffrage = topChiffrage;
  }
  
  public Integer getTopAffectation() {
    return topAffectation;
  }
  
  public void setTopAffectation(Integer topAffectation) {
    this.topAffectation = topAffectation;
  }
  
  public List<IdDocumentAchat> getListeIdDocumentOrigine() {
    return listeIdDocumentOrigine;
  }
  
  public void setListeIdDocumentOrigine(List<IdDocumentAchat> pListeIdDocumentOrigine) {
    listeIdDocumentOrigine = pListeIdDocumentOrigine;
  }
  
  public Character getCodeAvoir() {
    return codeAvoir;
  }
  
  public void setCodeAvoir(Character codeAvoir) {
    this.codeAvoir = codeAvoir;
  }
  
  public Character getCodeTypeFacturation() {
    return codeTypeFacturation;
  }
  
  public void setCodeTypeFacturation(Character codeTypeFacturation) {
    this.codeTypeFacturation = codeTypeFacturation;
  }
  
  public Character getCodeNatureAchat() {
    return codeNatureAchat;
  }
  
  public void setCodeNatureAchat(Character codeNatureAchat) {
    this.codeNatureAchat = codeNatureAchat;
  }
  
  public Reglements getReglement() {
    return reglement;
  }
  
  public ListeTva getListeTva() {
    return listeTva;
  }
  
  public BigDecimal getPourcentageEscompte() {
    return pourcentageEscompte;
  }
  
  public void setPourcentageEscompte(BigDecimal pourcentageEscompte) {
    this.pourcentageEscompte = pourcentageEscompte;
  }
  
  public BigDecimal getMontantEscompte() {
    return montantEscompte;
  }
  
  public void setMontantEscompte(BigDecimal montantEscompte) {
    this.montantEscompte = montantEscompte;
  }
  
  public String getCodeSectionAnalytique() {
    return codeSectionAnalytique;
  }
  
  public void setCodeSectionAnalytique(String codeSectionAnalytique) {
    this.codeSectionAnalytique = codeSectionAnalytique;
  }
  
  public String getCodeAffaire() {
    return codeAffaire;
  }
  
  public void setCodeAffaire(String codeAffaire) {
    this.codeAffaire = codeAffaire;
  }
  
  public String getCodeLieuLivraison() {
    return codeLieuLivraison;
  }
  
  public void setCodeLieuLivraison(String codeLieuLivraison) {
    this.codeLieuLivraison = codeLieuLivraison;
  }
  
  public String getCodeJournalAchat() {
    return codeJournalAchat;
  }
  
  public void setCodeJournalAchat(String codeJournalAchat) {
    this.codeJournalAchat = codeJournalAchat;
  }
  
  public String getCodeDevise() {
    return codeDevise;
  }
  
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  public BigDecimal getTauxDeChange() {
    return tauxDeChange;
  }
  
  public void setTauxDeChange(BigDecimal tauxDeChange) {
    this.tauxDeChange = tauxDeChange;
  }
  
  public BigDecimal getCoefficientAchat() {
    return coefficientAchat;
  }
  
  public void setCoefficientAchat(BigDecimal coefficientAchat) {
    this.coefficientAchat = coefficientAchat;
  }
  
  public Integer getBaseDevise() {
    return baseDevise;
  }
  
  public void setBaseDevise(Integer baseDevise) {
    this.baseDevise = baseDevise;
  }
  
  public String getCodeDossierApprovisionnement() {
    return codeDossierApprovisionnement;
  }
  
  public void setCodeDossierApprovisionnement(String codeDossierApprovisionnement) {
    this.codeDossierApprovisionnement = codeDossierApprovisionnement;
  }
  
  public String getCodeContainer() {
    return codeContainer;
  }
  
  public void setCodeContainer(String codeContainer) {
    this.codeContainer = codeContainer;
  }
  
  public BigDecimal getPoids() {
    return poids;
  }
  
  public void setPoids(BigDecimal poids) {
    this.poids = poids;
  }
  
  public BigDecimal getVolume() {
    return volume;
  }
  
  public void setVolume(BigDecimal volume) {
    this.volume = volume;
  }
  
  public BigDecimal getMontantFraisRepartir() {
    return montantFraisRepartir;
  }
  
  public void setMontantFraisRepartir(BigDecimal montantFraisRepartir) {
    this.montantFraisRepartir = montantFraisRepartir;
  }
  
  public ListeRemise getListeRemiseLigne() {
    return listeRemiseLigne;
  }
  
  public void setListeRemiseLigne(ListeRemise listeRemiseLigne) {
    this.listeRemiseLigne = listeRemiseLigne;
  }
  
  public ListeRemise getListeRemisePied() {
    return listeRemisePied;
  }
  
  public void setListeRemisePied(ListeRemise listeRemisePied) {
    this.listeRemisePied = listeRemisePied;
  }
  
  public ListeZonePersonnalisee getListeTopPersonnalisable() {
    return listeTopPersonnalisable;
  }
  
  public void setListeTopPersonnalisable(ListeZonePersonnalisee listeTopPersonnalisable) {
    this.listeTopPersonnalisable = listeTopPersonnalisable;
  }
  
  public Character getCodeLitige() {
    return codeLitige;
  }
  
  public void setCodeLitige(Character codeLitige) {
    this.codeLitige = codeLitige;
  }
  
  public Boolean isReceptionDefinitive() {
    return receptionDefinitive;
  }
  
  public void setReceptionDefinitive(Boolean receptionDefinitive) {
    this.receptionDefinitive = receptionDefinitive;
  }
  
  public Boolean getComptabilisationEnStocksFlottants() {
    return comptabilisationEnStocksFlottants;
  }
  
  public void setComptabilisationEnStocksFlottants(Boolean comptabilisationEnStocksFlottants) {
    this.comptabilisationEnStocksFlottants = comptabilisationEnStocksFlottants;
  }
  
  public Boolean getBlocageComptabilisation() {
    return blocageComptabilisation;
  }
  
  public void setBlocageComptabilisation(Boolean blocageComptabilisation) {
    this.blocageComptabilisation = blocageComptabilisation;
  }
  
  public String getCodeContrat() {
    return codeContrat;
  }
  
  public void setCodeContrat(String codeContrat) {
    this.codeContrat = codeContrat;
  }
  
  public Date getDateConfirmationFournisseur() {
    return dateConfirmationFournisseur;
  }
  
  public void setDateConfirmationFournisseur(Date dateConfirmationFournisseur) {
    this.dateConfirmationFournisseur = dateConfirmationFournisseur;
  }
  
  public Date getDateIntermediaire1() {
    return dateIntermediaire1;
  }
  
  public void setDateIntermediaire1(Date dateIntermediaire1) {
    this.dateIntermediaire1 = dateIntermediaire1;
  }
  
  public Date getDateIntermediaire2() {
    return dateIntermediaire2;
  }
  
  public void setDateIntermediaire2(Date dateIntermediaire2) {
    this.dateIntermediaire2 = dateIntermediaire2;
  }
  
  public Character getCodeEtatIntermediaire() {
    return codeEtatIntermediaire;
  }
  
  public void setCodeEtatIntermediaire(Character codeEtatIntermediaire) {
    this.codeEtatIntermediaire = codeEtatIntermediaire;
  }
  
  public String getCodeModeExpedition() {
    return codeModeExpedition;
  }
  
  public void setCodeModeExpedition(String codeModeExpedition) {
    this.codeModeExpedition = codeModeExpedition;
  }
  
  public String getCodeTransporteur() {
    return codeTransporteur;
  }
  
  public void setCodeTransporteur(String codeTransporteur) {
    this.codeTransporteur = codeTransporteur;
  }
  
  public Boolean getTraiteParCommandeDilicom() {
    return traiteParCommandeDilicom;
  }
  
  public void setTraiteParCommandeDilicom(Boolean traiteParCommandeDilicom) {
    this.traiteParCommandeDilicom = traiteParCommandeDilicom;
  }
  
  public Boolean getBlocageCommande() {
    return blocageCommande;
  }
  
  public void setBlocageCommande(Boolean blocageCommande) {
    this.blocageCommande = blocageCommande;
  }
  
  public Character getCodeScenarioDates() {
    return codeScenarioDates;
  }
  
  public void setCodeScenarioDates(Character codeScenarioDates) {
    this.codeScenarioDates = codeScenarioDates;
  }
  
  public Character getCodeEtatFraisFixes() {
    return codeEtatFraisFixes;
  }
  
  public void setCodeEtatFraisFixes(Character codeEtatFraisFixes) {
    this.codeEtatFraisFixes = codeEtatFraisFixes;
  }
  
  public EnumTypeCommande getCodeTypeCommande() {
    return codeTypeCommande;
  }
  
  public void setCodeTypeCommande(EnumTypeCommande codeTypeCommande) {
    this.codeTypeCommande = codeTypeCommande;
  }
  
  public Adresse getAdresseFournisseur() {
    return adresseFournisseur;
  }
  
  public void setAdresseFournisseur(Adresse adresseFournisseur) {
    this.adresseFournisseur = adresseFournisseur;
  }
  
  public String getIndicatifContact() {
    return indicatifContact;
  }
  
  public void setIndicatifContact(String indicatifContact) {
    this.indicatifContact = indicatifContact;
  }
  
  public IdDocumentAchat getIdDocumentPourExtraction() {
    return idDocumentPourExtraction;
  }
  
  public void setIdDocumentPourExtraction(IdDocumentAchat idDocumentPourExtraction) {
    this.idDocumentPourExtraction = idDocumentPourExtraction;
  }
  
  public int getEtape() {
    return etape;
  }
  
  public void setEtape(int etape) {
    this.etape = etape;
  }
  
  public Boolean getModeEnlevement() {
    return modeEnlevement;
  }
  
  public void setListeLignesArticles(ListeLigneAchat pListeLigneAchat) {
    this.listeLigneAchat = pListeLigneAchat;
  }
  
  public void setListeLignesArticlesSupprimees(ListeLigneAchat pListeLigneAchatSupprimee) {
    this.listeLigneAchatSupprimee = pListeLigneAchatSupprimee;
  }
  
  public BigDecimal getTotalHTLignes() {
    return totalHTLignes;
  }
  
  public void setTotalHTLignes(BigDecimal totalHTLignes) {
    this.totalHTLignes = totalHTLignes;
  }
  
  public EnumEtapeExtraction getEtapeExtraction() {
    return etapeExtraction;
  }
  
  public void setEtapeExtraction(EnumEtapeExtraction etapeExtraction) {
    this.etapeExtraction = etapeExtraction;
  }
  
  public Character getCodeTypePrixAchat() {
    return codeTypePrixAchat;
  }
  
  public void setCodeTypePrixAchat(Character codeTypePrixAchat) {
    this.codeTypePrixAchat = codeTypePrixAchat;
  }
  
  public String getLibelleTypeDocument() {
    return libelleTypeDocument;
  }
  
  public void setLibelleTypeDocument(String libelleTypeDocument) {
    this.libelleTypeDocument = libelleTypeDocument;
  }
  
  public BigDecimal getMontantPortTheorique() {
    return montantPortTheorique;
  }
  
  public void setMontantPortTheorique(BigDecimal montantPortTheorique) {
    this.montantPortTheorique = montantPortTheorique;
  }
  
  public BigDecimal getMontantPortFacture() {
    return montantPortFacture;
  }
  
  public void setMontantPortFacture(BigDecimal montantPortFacture) {
    this.montantPortFacture = montantPortFacture;
  }
  
  public boolean isFraisPortFournisseurFacture() {
    return fraisPortFournisseurFacture;
  }
  
  public void setFraisPortFournisseurFacture(boolean pFraisPortFournisseurFacture) {
    this.fraisPortFournisseurFacture = pFraisPortFournisseurFacture;
  }
  
  public boolean isDirectUsine() {
    return codeTypeCommande != null && codeTypeCommande.equals(EnumTypeCommande.DIRECT_USINE);
  }
  
  public void setDirectUsine() {
    codeTypeCommande = EnumTypeCommande.DIRECT_USINE;
  }
  
  public Adresse getAdresseLivraison() {
    return adresseLivraison;
  }
  
  public void setAdresseLivraison(Adresse adresseLivraison) {
    this.adresseLivraison = adresseLivraison;
  }
  
}
