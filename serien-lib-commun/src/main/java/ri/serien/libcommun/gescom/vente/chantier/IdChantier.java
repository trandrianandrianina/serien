/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.chantier;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un chantier.
 *
 * L'identifiant est composé du code établissement et du numéro chantier.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdChantier extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_NUMERO = 6;
  public static final int LONGUEUR_INDICATIF = LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_NUMERO;
  
  public static final int INDICATIF_NUM = 0;
  public static final int INDICATIF_ETB_NUM = 1;
  
  // Variables
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdChantier(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, Integer pNumero) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   * Le numéro et n'est pas à fournir. Il est renseigné avec la valeur 0 dans ce cas.
   */
  private IdChantier(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    numero = 0;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdChantier getInstance(IdEtablissement pIdEtablissement, Integer pNumero) {
    return new IdChantier(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pNumero);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdChantier getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdChantier(pIdEtablissement);
  }
  
  /**
   * Contrôler la validité du numéro du chantier.
   * Il doit être supérieur à zéro et doit comporter au maximum 6 chiffres (entre 1 et 999 999).
   */
  private Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du chantier n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du chantier est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du chantier est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdChantier controlerId(IdChantier pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du chantier est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du chantier n'existe pas dans la base de données : " + pId.toString());
    }
    return pId;
  }
  
  // -- Méthodes publiques
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getCodeEtablissement().hashCode();
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdChantier)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de chantiers.");
    }
    IdChantier id = (IdChantier) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdChantier)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdChantier id = (IdChantier) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return "" + numero;
  }
  
  // -- Accesseurs
  
  /**
   * Code entête du chantier.
   * Le code entête d'un chantier est fixe. Cette méthode est présente car les chantiers sont stockés comme des documents de ventes et
   * le code entête fait partie de l'identifiant d'un document de ventes.
   */
  public EnumCodeEnteteDocumentVente getCodeEntete() {
    return EnumCodeEnteteDocumentVente.DEVIS;
  }
  
  /**
   * Numéro du chantier.
   * Le numéro du chantier est compris entre entre 0 et 999 999.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Suffixe du chantier.
   * Le suffixe d'un chantier est toujours 0. Cette méthode est présente car les chantiers sont stockés comme des documents de ventes et
   * le suffixe fait partie de l'identifiant d'un document de ventes.
   */
  public Integer getSuffixe() {
    return 0;
  }
}
