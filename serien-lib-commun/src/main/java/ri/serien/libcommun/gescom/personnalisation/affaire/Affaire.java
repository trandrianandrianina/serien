/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.affaire;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

public class Affaire extends AbstractClasseMetier<IdAffaire> {
  
  private String nom = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public Affaire(IdAffaire pIdAffaire) {
    super(pIdAffaire);
  }
  
  @Override
  public String getTexte() {
    return nom;
  }
  
  // -- Accesseurs
  /**
   * Nom de l'affaire.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Nom de l'affaire suivi de son code entre parenthèse
   * Exemple : ANDRESY (a0ac).
   * Par sécurité, le code est retourné seul au cas où le nom ne serait pas renseigné.
   */
  public String getNomAvecCode() {
    if (nom != null) {
      return nom + " (" + getId().getCode() + ")";
    }
    else {
      return getId().getCode();
    }
  }
  
  /**
   * Renseigner le nom de l'affaire.
   */
  public void setNom(String pNom) {
    this.nom = pNom;
  }
  
}
