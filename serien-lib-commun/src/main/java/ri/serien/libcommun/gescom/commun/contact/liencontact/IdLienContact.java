/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact.liencontact;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un LienContact (lien entre un contact et un tiers).
 *
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation. *
 */
public class IdLienContact extends AbstractId {
  
  // Constantes
  public static final int LONGUEUR_NUMERO = 6;
  public static final int CREATION = 0;
  
  // Variables
  private final EnumTypeContact typeLienContact;
  private final Integer numeroContact;
  private final IdClient idClient;
  private final IdFournisseur idFournisseur;
  
  // -- Constructeur
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdLienContact(EnumEtatObjetMetier pEnumEtatObjetMetier, EnumTypeContact pTypeLienContact, Integer pNumeroContact,
      IdClient pIdClient, IdFournisseur pIdFournisseur) {
    super(pEnumEtatObjetMetier);
    typeLienContact = pTypeLienContact;
    numeroContact = controlerNumero(pNumeroContact);
    idClient = pIdClient;
    idFournisseur = pIdFournisseur;
  }
  
  // -- Méthodes publiques
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdLienContact getInstance(EnumTypeContact pTypeLienContact, Integer pNumeroContact, IdClient pIdClient,
      IdFournisseur pIdFournisseur) {
    return new IdLienContact(EnumEtatObjetMetier.MODIFIE, pTypeLienContact, pNumeroContact, pIdClient, pIdFournisseur);
  }
  
  /**
   * Créer un identifiant de lien client.
   */
  public static IdLienContact getInstancePourClient(Integer pNumeroContact, IdClient pIdClient) {
    return new IdLienContact(EnumEtatObjetMetier.MODIFIE, EnumTypeContact.CLIENT, pNumeroContact, pIdClient, null);
  }
  
  /**
   * Créer un identifiant de lien fournisseur.
   */
  public static IdLienContact getInstancePourFournisseur(Integer pNumeroContact, IdFournisseur pIdFournisseur) {
    return new IdLienContact(EnumEtatObjetMetier.MODIFIE, EnumTypeContact.FOURNISSEUR, pNumeroContact, null, pIdFournisseur);
  }
  
  /**
   * Contrôler la validité du code contact.
   * Le code contact est une valeur entière (6 caractères).
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null || pValeur < 0) {
      throw new MessageErreurException("Le numéro unique du contact n'est pas renseigné.");
    }
    if (pValeur < 0) {
      throw new MessageErreurException("Le numéro du contact est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du contact est supérieur à la valeur maximum possible.");
    }
    
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdLienContact controlerId(IdLienContact pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du contact est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du contact n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + typeLienContact.hashCode();
    cle = 37 * cle + numeroContact.hashCode();
    if (idClient != null) {
      cle = 37 * cle + idClient.hashCode();
    }
    if (idFournisseur != null) {
      cle = 37 * cle + idFournisseur.hashCode();
    }
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdLienContact)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de LienContacts.");
    }
    IdLienContact id = (IdLienContact) pObject;
    return Constantes.equals(typeLienContact, id.typeLienContact) && Constantes.equals(numeroContact, id.numeroContact)
        && Constantes.equals(idClient, id.idClient) && Constantes.equals(idFournisseur, id.idFournisseur);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdLienContact)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdLienContact id = (IdLienContact) pObject;
    
    int comparaison = typeLienContact.compareTo(id.typeLienContact);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numeroContact.compareTo(id.numeroContact);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idClient.compareTo(id.idClient);
    if (comparaison != 0) {
      return comparaison;
    }
    
    return idFournisseur.compareTo(id.idFournisseur);
  }
  
  @Override
  public String getTexte() {
    return numeroContact.toString();
  }
  
  // -- Accesseurs
  /**
   * Renvoi si le lien est un lien client
   */
  public boolean isLienClient() {
    if (typeLienContact == null) {
      return false;
    }
    
    if (typeLienContact.equals(EnumTypeContact.CLIENT)) {
      return true;
    }
    return false;
  }
  
  /**
   * Renvoi si le lien est un lien fournisseur
   */
  public boolean isLienFournisseur() {
    if (typeLienContact == null) {
      return false;
    }
    
    if (typeLienContact.equals(EnumTypeContact.FOURNISSEUR)) {
      return true;
    }
    return false;
  }
  
  /**
   * Type de lien entre contact et tiers (client, fournisseur ou prospect)
   */
  public EnumTypeContact getTypeLienContact() {
    return typeLienContact;
  }
  
  /**
   * Code contact.
   * Le code contact est une valeur entière.
   */
  public Integer getNumeroContact() {
    return numeroContact;
  }
  
  /**
   * Identifiant du tiers client
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Identifiant du tiers fournisseur
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Retourne l'indicatif du tiers
   */
  public String getIndicatifLien() {
    switch (typeLienContact) {
      case CLIENT:
        return idClient.getIndicatif(IdClient.INDICATIF_NUM_SUF);
      
      case FOURNISSEUR:
        return idFournisseur.getIndicatifCourt();
      
      default:
        return null;
    }
  }
  
  /**
   * Retourne le code établissement du lien
   */
  public String getCodeEtablissementLien() {
    switch (typeLienContact) {
      case CLIENT:
        return idClient.getIdEtablissement().getCodeEtablissement();
      
      case FOURNISSEUR:
        return idFournisseur.getIdEtablissement().getCodeEtablissement();
      
      default:
        return null;
    }
  }
}
