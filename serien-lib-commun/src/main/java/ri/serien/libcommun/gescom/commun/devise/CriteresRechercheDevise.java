/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.devise;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;

public class CriteresRechercheDevise extends CriteresBaseRecherche {
  // Constantes
  public static final int RECHERCHE_DEVISE = 1;
  
}
