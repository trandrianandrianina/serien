/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import java.math.BigDecimal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * LienRèglement.
 * Utilisé lors des règlements multiples, permet de lier un règlement et un document de vente.
 */
public class LienReglement extends AbstractClasseMetier<IdReglement> {
  // Variables
  private Integer numeroDocumentVente = null;
  private Integer suffixeDocumentVente = null;
  private BigDecimal montant = null;
  
  /**
   * Constructeur.
   */
  public LienReglement(IdReglement pIdReglement) {
    super(pIdReglement);
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    if (montant != null) {
      return montant.toString();
    }
    else {
      return id.getTexte();
    }
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public LienReglement clone() {
    LienReglement o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (LienReglement) super.clone();
      o.setId(id);
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  public Integer getNumeroDocumentVente() {
    return numeroDocumentVente;
  }
  
  public void setNumeroDocumentVente(Integer numeroDocumentVente) {
    this.numeroDocumentVente = numeroDocumentVente;
  }
  
  public Integer getSuffixeDocumentVente() {
    return suffixeDocumentVente;
  }
  
  public void setSuffixeDocumentVente(Integer suffixeDocumentVente) {
    this.suffixeDocumentVente = suffixeDocumentVente;
  }
  
  public BigDecimal getMontant() {
    return montant;
  }
  
  public void setMontant(BigDecimal montant) {
    this.montant = montant;
  }
  
}
