/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un contact.
 *
 * L'identifiant du seul numero d'ordre du contact.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation. *
 */
public class IdContact extends AbstractId {
  
  // Constantes
  public static final int LONGUEUR_NUMERO = 6;
  public static final int CREATION = 0;
  // Variables
  private Integer numeroContact = null;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdContact(EnumEtatObjetMetier pEnumEtatObjetMetier, Integer pNumeroContact) {
    super(pEnumEtatObjetMetier);
    numeroContact = controlerNumero(pNumeroContact);
  }
  
  /**
   * Créer l'identifiant d'un nouveau contact non présent en base de données à partir de ses informations sous forme éclatée.
   */
  public static IdContact getInstanceCreation(Integer pNumeroContact) {
    return new IdContact(EnumEtatObjetMetier.CREE, pNumeroContact);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdContact getInstance(Integer pNumeroContact) {
    return new IdContact(EnumEtatObjetMetier.MODIFIE, pNumeroContact);
  }
  
  /**
   * Contrôler la validité du code contact.
   * Le code contact est une valeur entière (6 caractères).
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null || pValeur < 0) {
      throw new MessageErreurException("Le numéro unique du contact n'est pas renseigné.");
    }
    if (pValeur < 0) {
      throw new MessageErreurException("Le numéro du contact est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du contact est supérieur à la valeur maximum possible.");
    }
    
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdContact controlerId(IdContact pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du contact est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du contact n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + numeroContact.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdContact)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de contacts.");
    }
    IdContact id = (IdContact) pObject;
    return numeroContact.equals(id.numeroContact);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdContact)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdContact id = (IdContact) pObject;
    return numeroContact.compareTo(id.numeroContact);
  }
  
  @Override
  public String getTexte() {
    return numeroContact.toString();
  }
  
  // -- Accesseurs
  
  /**
   * Code contact.
   * Le code contact est une valeur entière.
   */
  public Integer getCode() {
    return numeroContact;
  }
}
