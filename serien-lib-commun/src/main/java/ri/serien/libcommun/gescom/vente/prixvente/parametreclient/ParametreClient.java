/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreclient;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumTypeApplicationConditionVente;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Classe regroupant tous les paramètres du client pour le calcul du prix de vente.
 * 
 * TODO
 * La deuxième colonne tarif du client :
 * C'est bien ça le champs FACTA du param. 'FA' peut valoir 2. Dans ce cas, la ligne est chiffrée avec la 2ème colonne de la fiche client.
 * La PS n°105 permet aussi de définir une colonne par défaut pour le chiffrage. Cette PS est utilisée pour les clients qui renseignent le
 * PRV dans la colonne n°1. Le colonne 2 est alors le prix de base calculé par PRV (colonne 1) x 1er coef. du tarif.
 */
public class ParametreClient implements Serializable {
  // Variables
  private IdClient idClient = null;
  private IdClient idClientFacture = null;
  private boolean modeTTC = false;
  private String codeDevise = null;
  private Integer numeroColonneTarif = null;
  private Integer numeroColonneTarif2 = null;
  private BigPercentage tauxRemise1 = null;
  private BigPercentage tauxRemise2 = null;
  private BigPercentage tauxRemise3 = null;
  
  // Ce champ ne contient qu'un code de la condition de vente (si en table le champ CLCNV contient un numéro à 6 chiffres alors le code
  // est initialisé avec null)
  private String codeConditionStandard = null;
  private String codeConditionPromo = null;
  // Contient le numéro du client référence pour les conditions de ventes si le champ CLCNV contient un numéro sur 6 chiffres
  private Integer numeroClientReferenceConditionVente = null;
  // S'il existe un numéro client référence alors les champs codeConditionVenteClientReference et codeConditionVentePromoClientReference
  // sont renseignés avec les valeurs CLCNV et CLCNP de ce client.
  private String codeConditionStandardClientReference = null;
  private String codeConditionPromoClientReference = null;
  private Integer numeroCentraleAchat1 = null;
  private Integer numeroCentraleAchat2 = null;
  private Integer numeroCentraleAchat3 = null;
  private Character typeFacturation = null;
  private EnumRegleExclusionConditionVenteEtablissement regleExclusionConditionVenteEtablissement = null;
  private EnumOrigineClient origineClient = null;
  // Indique si les conditions de ventes normales et/ou quantitatives doivent être prise en compte
  private EnumTypeApplicationConditionVente typeApplicationConditionVente = null;
  
  // -- Méthodes publiques
  
  /**
   * Indiquer si les conditions de ventes normales sont applicables pour ce client.
   * @return true=conditions de vente normales applicables, false=sinon.
   */
  public boolean isConditionVenteNormaleApplicable() {
    if (typeApplicationConditionVente != null
        && typeApplicationConditionVente == EnumTypeApplicationConditionVente.FORCE_CNV_QUANTITATIVE) {
      return false;
    }
    return true;
  }
  
  /**
   * Indiquer si les conditions de ventes quantitatives sont applicables pour ce client.
   * @return true=conditions de vente quantitatives applicables, false=sinon.
   */
  public boolean isConditionVenteQuantitativeApplicable() {
    if (typeApplicationConditionVente != null
        && typeApplicationConditionVente == EnumTypeApplicationConditionVente.IGNORE_CNV_QUANTITATIVE) {
      return false;
    }
    return true;
  }
  
  // -- Accesseurs
  
  /**
   * Retourner l'identifiant du client.
   * @return Identifiant client.
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Modifier l'identifiant du client.
   * @param pIdClient Identifiant client.
   */
  public void setIdClient(IdClient pIdClient) {
    idClient = pIdClient;
  }
  
  /**
   * Retourner l'identifiant du client facturé.
   * Contient le numéro et le suffixe du client facturé donc soit le numéro/suffixe du client courant soit celui d'un autre client
   * auquel il est rattaché.
   * @return Identifiant client.
   */
  public IdClient getIdClientFacture() {
    return idClientFacture;
  }
  
  /**
   * Modifier l'identifiant du client facturé.
   * @param pIdClientFacture Identifiant client.
   */
  public void setIdClientFacture(IdClient pIdClientFacture) {
    idClientFacture = pIdClientFacture;
  }
  
  /**
   * Indiquer si le client est facturé en HT ou TTC.
   * @return true=TTC, false=HT.
   */
  public boolean isModeTTC() {
    return modeTTC;
  }
  
  /**
   * Modifier si le client est facturé en HT ou TTC.
   * @param pModeTTC true=TTC, false=HT.
   */
  public void setModeTTC(boolean pModeTTC) {
    modeTTC = pModeTTC;
  }
  
  /**
   * Retourne le code devise du client.
   * 
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Initialise le code devise du client.
   * 
   * @param codeDevise
   */
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  /**
   * Retourner le numéro de la colonne tarif 1 du client.
   * 
   * Les valeurs 1 à 10 désignent les colonnes tarifs 1 à 10. La valeur 0 signifie qu'il n'y aucune colonne tarif. La valeur null
   * signifie que l'information n'a pas été renseignée.
   * 
   * @return Numéro de la colonne tarif (0=aucune, 1-10=colonne tarif, null=non renseigné).
   */
  public Integer getNumeroColonneTarif() {
    return numeroColonneTarif;
  }
  
  /**
   * Modifier le numéro de la colonne tarif 1 du client.
   * @param pNumeroColonneTarif Numéro de colonne tarif.
   */
  public void setNumeroColonneTarif(Integer pNumeroColonneTarif) {
    numeroColonneTarif = pNumeroColonneTarif;
  }
  
  /**
   * Retourner le numéro de la colonne tarif 2 du client.
   * 
   * CETTE REGLE DE GESTION N'EST PAS IMPLEMENTEE DANS LE CALCUL JAVA CAR ELLE N'EST PROBALEMENT PLUS UTILISEE.
   * 
   * La deuxième colonne de tarif du client est utilisé à la place de la première lorsque la famille de l'article indique qu'il faut
   * utiliser la deuxième colonne de tarif du client. Dans ce cas, L1TT vaut 3 dans la ligne de vente comme si la colonne avait été
   * saisie manuellement.
   * 
   * Les valeurs 1 à 10 désignent les colonnes tarifs 1 à 10. La valeur 0 signifie qu'il n'y aucune colonne tarif. La valeur null
   * signifie que l'information n'a pas été renseignée.
   * 
   * @return Numéro de la colonne tarif (0=aucune, 1-10=colonne tarif, null=non renseigné).
   */
  public Integer getNumeroColonneTarif2() {
    return numeroColonneTarif2;
  }
  
  /**
   * Modifier le numéro de la colonne tarif 2 du client.
   * @param pNumeroColonneTarif Numéro de colonne tarif.
   */
  public void setNumeroColonneTarif2(Integer pNumeroColonneTarif) {
    numeroColonneTarif2 = pNumeroColonneTarif;
  }
  
  /**
   * Indiquer si au moins un taux remise est présent.
   * @return true=il y a au moins un taux de remise, false=sinon.
   */
  public boolean isTauxRemisePresent() {
    return tauxRemise1 != null || tauxRemise2 != null || tauxRemise3 != null;
  }
  
  /**
   * Retourner le taux de remise dont l'indice est mentionné en paramètre, du client.
   * @param pIndex Indice du taux de remise à modifier (entre 1 et 6).
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise(int pIndex) {
    if (pIndex == 1) {
      return getTauxRemise1();
    }
    else if (pIndex == 2) {
      return getTauxRemise2();
    }
    else if (pIndex == 3) {
      return getTauxRemise3();
    }
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise, dont l'indice est mentionné en paramètre, du client.
   * @param pTauxRemise1 Taux de remise.
   * @param pIndex Indice du taux de remise à modifier.
   */
  public void setTauxRemise(BigPercentage pTauxRemise, int pIndex) {
    if (pIndex == 1) {
      setTauxRemise1(pTauxRemise);
    }
    else if (pIndex == 2) {
      setTauxRemise2(pTauxRemise);
    }
    else if (pIndex == 3) {
      setTauxRemise3(pTauxRemise);
    }
  }
  
  /**
   * Retourner le taux de remise 1 du client.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise1() {
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise 1 du client.
   * @param pTauxRemise1 Taux de remise.
   */
  public void setTauxRemise1(BigPercentage pTauxRemise1) {
    tauxRemise1 = pTauxRemise1;
  }
  
  /**
   * Retourner le taux de remise 1 du client.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise2() {
    return tauxRemise2;
  }
  
  /**
   * Modifier le taux de remise 2 du client.
   * @param pTauxRemise1 Taux de remise.
   */
  public void setTauxRemise2(BigPercentage pTauxRemise2) {
    tauxRemise2 = pTauxRemise2;
  }
  
  /**
   * Retourner le taux de remise 3 du client.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise3() {
    return tauxRemise3;
  }
  
  /**
   * Modifier le taux de remise 3 du client.
   * @param pTauxRemise1 Taux de remise.
   */
  public void setTauxRemise3(BigPercentage pTauxRemise3) {
    tauxRemise3 = pTauxRemise3;
  }
  
  /**
   * Retourne le code condition de vente du client.
   * 
   * @return
   */
  public String getCodeConditionVente() {
    return codeConditionStandard;
  }
  
  /**
   * Initialise le code condition de vente du client.
   * 
   * @param pCodeConditionVente
   */
  public void setCodeConditionVente(String pCodeConditionVente) {
    this.codeConditionStandard = pCodeConditionVente;
  }
  
  /**
   * Retourne le code condition de vente promo du client.
   * 
   * @return
   */
  public String getCodeConditionVentePromo() {
    return codeConditionPromo;
  }
  
  /**
   * Initialise le code condition de vente promo du client.
   * 
   * @param pCodeConditionVentePromo
   */
  public void setCodeConditionVentePromo(String pCodeConditionVentePromo) {
    this.codeConditionPromo = pCodeConditionVentePromo;
  }
  
  /**
   * Initialise le numéro du client référence pour les conditions de ventes.
   * 
   * @return
   */
  public Integer getNumeroClientReferenceConditionVente() {
    return numeroClientReferenceConditionVente;
  }
  
  /**
   * Retourne le numéro du client référence pour les conditions de ventes.
   * 
   * @param pNumeroClientReference
   */
  public void setNumeroClientReferenceConditionVente(Integer pNumeroClientReference) {
    this.numeroClientReferenceConditionVente = pNumeroClientReference;
  }
  
  /**
   * Initialise le code de la condition standard du client référence.
   * 
   * @return
   */
  public String getCodeConditionStandardClientReference() {
    return codeConditionStandardClientReference;
  }
  
  /**
   * Retourne le code de la condition standard du client référence.
   * 
   * @param pCodeConditionVente
   */
  public void setCodeConditionStandardClientReference(String pCodeConditionVente) {
    this.codeConditionStandardClientReference = pCodeConditionVente;
  }
  
  /**
   * Initialise le code de la condition promotionnelle du client référence.
   * 
   * @return
   */
  public String getCodeConditionPromoClientReference() {
    return codeConditionPromoClientReference;
  }
  
  /**
   * Retourne le code de la condition promotionnelle du client référence.
   * 
   * @param pCodeConditionVentePromo
   */
  public void setCodeConditionPromoClientReference(String pCodeConditionVentePromo) {
    this.codeConditionPromoClientReference = pCodeConditionVentePromo;
  }
  
  /**
   * Retourner le numéro de la centrale d'achat 1 du client.
   * @return Le numéro de la centrale d'achat 1.
   */
  public Integer getNumeroCentraleAchat1() {
    return numeroCentraleAchat1;
  }
  
  /**
   * Modifier le numéro de la centrale d'achat 1 du client.
   * @param pNumeroCentraleAchat1 Le numéro de la centrale d'achat 1.
   */
  public void setNumeroCentraleAchat1(Integer pNumeroCentraleAchat1) {
    numeroCentraleAchat1 = pNumeroCentraleAchat1;
  }
  
  /**
   * Retourner le numéro de la centrale d'achat 2 du client.
   * @return Le numéro de la centrale d'achat 2.
   */
  public Integer getNumeroCentraleAchat2() {
    return numeroCentraleAchat2;
  }
  
  /**
   * Modifier le numéro de la centrale d'achat 2 du client.
   * @param pNumeroCentraleAchat2 Le numéro de la centrale d'achat 2.
   */
  public void setNumeroCentraleAchat2(Integer pNumeroCentraleAchat2) {
    numeroCentraleAchat2 = pNumeroCentraleAchat2;
  }
  
  /**
   * Retourner le numéro de la centrale d'achat 3 du client.
   * @return Le numéro de la centrale d'achat 3.
   */
  public Integer getNumeroCentraleAchat3() {
    return numeroCentraleAchat3;
  }
  
  /**
   * Modifier le numéro de la centrale d'achat 3 du client.
   * @param pNumeroCentraleAchat3 Le numéro de la centrale d'achat 3.
   */
  public void setNumeroCentraleAchat3(Integer pNumeroCentraleAchat3) {
    numeroCentraleAchat1 = pNumeroCentraleAchat3;
  }
  
  /**
   * Retourne le type de facturation.
   * 
   * @return
   */
  public Character getTypeFacturation() {
    return typeFacturation;
  }
  
  /**
   * Initialise le type de facturation.
   * 
   * @param pTypeFacturation
   */
  public void setTypeFacturation(Character pTypeFacturation) {
    this.typeFacturation = pTypeFacturation;
  }
  
  /**
   * Retourner la règle d'exclusion des conditions de ventes établissement.
   * @return Règle d'exclusion des conditions de ventes établissement.
   */
  public EnumRegleExclusionConditionVenteEtablissement getRegleExclusionConditionVenteEtablissement() {
    return regleExclusionConditionVenteEtablissement;
  }
  
  /**
   * Modifier la règle d'exclusion des conditions de ventes établissement.
   * @param pRegleExclusionConditionVenteEtablissement Règle d'exclusion des conditions de ventes établissement.
   */
  public void setRegleExclusionConditionVenteEtablissement(
      EnumRegleExclusionConditionVenteEtablissement pRegleExclusionConditionVenteEtablissement) {
    regleExclusionConditionVenteEtablissement = pRegleExclusionConditionVenteEtablissement;
  }
  
  /**
   * Retourne l'origine de l'identifiant du client.
   * 
   * @return
   */
  public EnumOrigineClient getOrigineClient() {
    return origineClient;
  }
  
  /**
   * Initialise l'origine de l'identifiant du client.
   * 
   * @param pOrigineClient
   */
  public void setOrigineClient(EnumOrigineClient pOrigineClient) {
    this.origineClient = pOrigineClient;
  }
  
  /**
   * Retourne si les conditions de ventes normales et/ou quantitatives doivent être prise en compte.
   * 
   * @return
   */
  public EnumTypeApplicationConditionVente getTypeApplicationConditionVente() {
    return typeApplicationConditionVente;
  }
  
  /**
   * Retourne si les conditions de ventes normales et/ou quantitatives doivent être prise en compte.
   * 
   * @param pTypeApplicationConditionVente
   */
  public void setTypeApplicationConditionVente(EnumTypeApplicationConditionVente pTypeApplicationConditionVente) {
    this.typeApplicationConditionVente = pTypeApplicationConditionVente;
  }
  
}
