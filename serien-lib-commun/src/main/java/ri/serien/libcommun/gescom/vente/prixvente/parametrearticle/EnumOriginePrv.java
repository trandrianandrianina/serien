/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrearticle;

/**
 * Liste des origines possibles du PRV.
 */
public enum EnumOriginePrv {
  PRV_STANDARD_CNA("PRV standard CNA"),
  PRV_ARTICLE("PRV article"),
  PRV_MAGASIN("PRV magasin");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOriginePrv(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
