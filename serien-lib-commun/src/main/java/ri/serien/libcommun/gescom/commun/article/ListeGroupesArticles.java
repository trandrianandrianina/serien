/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.util.ArrayList;

/**
 * Liste de groupes d'articles.
 */
public class ListeGroupesArticles extends ArrayList<GroupeArticles> {
  
  /**
   * Rajouter un groupe à la liste de groupes articles
   */
  public void rajouterUnGroupe(GroupeArticles pGroupe) {
    if (pGroupe == null) {
      return;
    }
    add(pGroupe);
  }
  
  /**
   * On vérifie si le groupe passé en paramètre est dans la liste des groupes
   */
  public boolean contientUnGroupe(GroupeArticles pGroupe) {
    if (size() == 0) {
      return false;
    }
    
    for (GroupeArticles groupe : this) {
      if (groupe.equals(pGroupe)) {
        return true;
      }
    }
    
    return false;
  }
}
