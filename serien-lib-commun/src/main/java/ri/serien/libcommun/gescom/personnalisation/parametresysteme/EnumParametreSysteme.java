/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.parametresysteme;

import ri.serien.libcommun.gescom.commun.EnumTypeValeurPersonnalisation;

/**
 * Liste des paramètres systèmes.
 */
public enum EnumParametreSysteme {
  PS017(17, "Code catégorie obligatoire", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS018(18, "Code représentant obligatoire", "Clients", EnumTypeValeurPersonnalisation.TYPE_C),
  PS019(19, "Numérotation automatique", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS030(30, "Gestion par lot", "Bons de vente (lignes)", EnumTypeValeurPersonnalisation.TYPE_C),
  PS032(32, "Arrêt à la première condition trouvée", "Conditions de vente", EnumTypeValeurPersonnalisation.TYPE_D),
  PS047(47, "Prise en compte des conditions de vente quantitatives", "Conditions de vente", EnumTypeValeurPersonnalisation.TYPE_D),
  PS059(59, "Encours clients hors commandes", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS063(63, "Gestion des tournées", "Clients", EnumTypeValeurPersonnalisation.TYPE_C),
  PS086(86, "Saisie des numéros de lot à la commande", "Bons de vente (lignes)", EnumTypeValeurPersonnalisation.TYPE_C),
  PS105(105, "Colonne tarif par défaut / bon", "Clients", EnumTypeValeurPersonnalisation.TYPE_B),
  PS116(116, "Duplication fiche observation", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS117(117, "Gestion adresses de stockage", "Stocks", EnumTypeValeurPersonnalisation.TYPE_A),
  PS119(119, "Date utilisée pour la recherche de prix", "Conditions de vente", EnumTypeValeurPersonnalisation.TYPE_C),
  PS124(124, "PUMP par magasin ou établissement", "bon d'achats", EnumTypeValeurPersonnalisation.TYPE_A),
  PS134(134, "Clients 'grand public' de base", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS140(140, "Personnalisation fiche client", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS145(145, "Contrôle du type d'avoir", "Bons de vente (entête)", EnumTypeValeurPersonnalisation.TYPE_C),
  PS147(147, "Création client en compta", "Clients", EnumTypeValeurPersonnalisation.TYPE_C),
  PS149(149, "Numérotation automatique en création de fournisseur", "Fournisseurs", EnumTypeValeurPersonnalisation.TYPE_A),
  PS151(151, "Vérification clé client", "Clients", EnumTypeValeurPersonnalisation.TYPE_C),
  PS152(152, "Gestion historique proposition / ventes", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS153(153, "Gestion des comptes épargne", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS157(157, "Rendre obligatoire la référence courte dans les documents de vente", "Bons de vente (entête)",
      EnumTypeValeurPersonnalisation.TYPE_C),
  PS158(158, "Sous-famille indépendante de la famille", "Articles", EnumTypeValeurPersonnalisation.TYPE_A),
  PS190(190, "RIB obligatoire si règlement par traites", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS194(194, "Gestion des prospects associée", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS195(195, "Clients désactivés de base", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS198(198, "Contrôle centrale client", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS203(203, "Date de validité devis et date de commande obligatoire", "Bons de vente (entête)", EnumTypeValeurPersonnalisation.TYPE_D),
  PS216(216, "Emboîtage des conditions de ventes", "Conditions de vente", EnumTypeValeurPersonnalisation.TYPE_D),
  PS233(233, "Franco de port en poids", "Clients", EnumTypeValeurPersonnalisation.TYPE_A),
  PS253(253, "Recherche des conditions de ventes sur fournisseur", "Conditions de vente", EnumTypeValeurPersonnalisation.TYPE_A),
  PS281(281, "Application particulière des conditions client / article", "Bons de vente (lignes)", EnumTypeValeurPersonnalisation.TYPE_C),
  PS287(287, "Spécificité 'Négoce'", "Articles", EnumTypeValeurPersonnalisation.TYPE_A),
  PS300(300, "Demande de déblocage document si dépassement encours", "Clients", EnumTypeValeurPersonnalisation.TYPE_C),
  PS305(305, "Colonne tarif précédent si tarif client non défini", "Bons de vente (lignes)", EnumTypeValeurPersonnalisation.TYPE_A),
  PS312(312, "Autoriser direct usine multi fournisseurs", "Bons de vente (entête)", EnumTypeValeurPersonnalisation.TYPE_A),
  PS316(316, "Calcul prix net à partir tarif colonne 1", "Bons de vente (entête)", EnumTypeValeurPersonnalisation.TYPE_A),
  PS318(318, "Alerte commande d'achat en cours", "Bons d'achats", EnumTypeValeurPersonnalisation.TYPE_A),
  PS319(319, "Alerte surstock autres magasins", "Bons d'achats", EnumTypeValeurPersonnalisation.TYPE_A),
  PS327(327, "Consulter la commande d'achats suite à sa génération immédiate", "Bons d'achats", EnumTypeValeurPersonnalisation.TYPE_A),
  PS329(329, "Rendre obligatoire la référence longue dans les documents de vente", "Bons de vente (entête)",
      EnumTypeValeurPersonnalisation.TYPE_A),
  PS330(330, "Gestion des bons de cour", "Bons de vente (lignes)", EnumTypeValeurPersonnalisation.TYPE_A),
  PS331(331, "Mode de délivrement par défaut des documents de vente", "Bons de vente (entête)", EnumTypeValeurPersonnalisation.TYPE_C),
  PS332(332, "Gérer la date de relance sur les devis.", "Bons de vente (entête)", EnumTypeValeurPersonnalisation.TYPE_A),
  PS335(335, "Afficher les liens des fiches techniques et les photos dans les devis V3.", "Bons de vente",
      EnumTypeValeurPersonnalisation.TYPE_B);
  // TODO to be continued ...
  
  private final Integer numero;
  private final String libelle;
  private final String categorie;
  private final EnumTypeValeurPersonnalisation typeValeur;
  
  /**
   * Constructeur.
   */
  EnumParametreSysteme(Integer pNumero, String pLibelle, String pCategorie, EnumTypeValeurPersonnalisation pTypeValeur) {
    numero = pNumero;
    libelle = pLibelle;
    categorie = pCategorie;
    typeValeur = pTypeValeur;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * La catégorie associée au numéro.
   */
  public String getCategorie() {
    return categorie;
  }
  
  /**
   * Le type de valeur.
   */
  public EnumTypeValeurPersonnalisation getTypeValeur() {
    return typeValeur;
  }
  
  /**
   * Retourne le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return numero.toString();
  }
  
  /**
   * Retourner l'objet enum par son numéro.
   */
  static public EnumParametreSysteme valueOfByNumero(Integer pNumero) {
    for (EnumParametreSysteme value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    // Code commenté car toutes les PS n'ont pas été créée ici
    // throw new MessageErreurException("Le numéro du paramètre système est inconnu : " + pNumero);
    return null;
  }
}
