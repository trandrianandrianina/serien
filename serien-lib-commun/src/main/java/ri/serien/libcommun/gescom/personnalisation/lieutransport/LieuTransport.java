/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.lieutransport;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Lieu transport
 */
public class LieuTransport extends AbstractClasseMetier<IdLieuTransport> {
  private String libelle = "";
  
  public LieuTransport(IdLieuTransport pId) {
    super(pId);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  // --Accesseur
  
  /*
   * Retourne le nom du lieu de transport
   */
  public String getLibelle() {
    return libelle;
  }
  
  /*
   * Modifie le nom du lieu de transport
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
}
