/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

public class CritereDocumentAchat extends CriteresBaseRecherche {
  // Variables
  private List<EnumTypeDocumentAchat> listeTypeDocument = new ArrayList<EnumTypeDocumentAchat>();
  private List<EnumCodeEtatDocument> listeCodeEtat = new ArrayList<EnumCodeEtatDocument>();
  private IdFournisseur idFournisseur = null;
  private int numeroDocument = 0;
  private int suffixeDocument = 0;
  private int numeroLigne = 0;
  private IdMagasin idMagasin = null;
  private Date dateCreationDebut = null;
  private Date dateCreationFin = null;
  private String codeArticle = null;
  private boolean avecDocumentAchatReceptionne = false;
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  
  // -- Méthodes publiques
  
  /**
   * Supprime le contenu de la liste des types de document.
   */
  public void viderListeTypeDocument() {
    listeTypeDocument.clear();
  }
  
  /**
   * Ajoute un type de document d'achat à la liste.
   */
  public void ajouterTypeDocumentAchat(EnumTypeDocumentAchat pTypeDocumentAchat) {
    if (pTypeDocumentAchat == null) {
      return;
    }
    if (!listeTypeDocument.contains(pTypeDocumentAchat)) {
      listeTypeDocument.add(pTypeDocumentAchat);
    }
  }
  
  /**
   * Supprime tout le contenu de la liste des codes états.
   */
  public void viderListeCodeEtat() {
    listeCodeEtat.clear();
  }
  
  /**
   * Ajoute un code état à la liste.
   */
  public void ajouterCodeEtat(EnumCodeEtatDocument pCodeEtat) {
    if (pCodeEtat == null) {
      return;
    }
    if (!listeCodeEtat.contains(pCodeEtat)) {
      listeCodeEtat.add(pCodeEtat);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la liste des types de document d'achat.
   */
  public List<EnumTypeDocumentAchat> getListeTypeDocument() {
    return listeTypeDocument;
  }
  
  public List<EnumCodeEtatDocument> getListeCodeEtat() {
    return listeCodeEtat;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin pIdMagasin) {
    this.idMagasin = pIdMagasin;
  }
  
  public int getNumeroDocument() {
    return numeroDocument;
  }
  
  public void setNumeroDocument(int numeroDocument) {
    this.numeroDocument = numeroDocument;
  }
  
  public int getSuffixeDocument() {
    return suffixeDocument;
  }
  
  public void setSuffixeDocument(int suffixeDocument) {
    this.suffixeDocument = suffixeDocument;
  }
  
  public int getNumeroLigne() {
    return numeroLigne;
  }
  
  public void setNumeroLigne(int numeroLigne) {
    this.numeroLigne = numeroLigne;
  }
  
  public Date getDateCreationDebut() {
    return dateCreationDebut;
  }
  
  public void setDateCreationDebut(Date dateCreationDebut) {
    this.dateCreationDebut = dateCreationDebut;
  }
  
  public Date getDateCreationFin() {
    return dateCreationFin;
  }
  
  public void setDateCreationFin(Date dateCreationFin) {
    this.dateCreationFin = dateCreationFin;
  }
  
  public String getCodeArticle() {
    return codeArticle;
  }
  
  public void setCodeArticle(String codeArticle) {
    this.codeArticle = codeArticle;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void setTexteRechercheGenerique(String pTexteRecherche) {
    rechercheGenerique.setTexte(pTexteRecherche);
  }
  
  /**
   * Renvoyer si on recherche les documents d'achats avec y compris les documents entièrement réceptionnés.
   */
  public boolean isAvecDocumentAchatReceptionne() {
    return avecDocumentAchatReceptionne;
  }
  
  /**
   * Modifier si on doit rechercher les documents d'achats avec y compris les documents entièrement réceptionnés.
   */
  public void setAvecDocumentAchatEntierementReceptionne(boolean avecDocumentAchatReceptionne) {
    this.avecDocumentAchatReceptionne = avecDocumentAchatReceptionne;
  }
  
}
