/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.origine;

/**
 * Liste des origines possibles pour le prix de base HT utilisé pour le calcul du prix net HT.
 * Les origines sont classées par ordre de priorité (du plus prioritaire vers le moins prioritaire).
 */
public enum EnumOriginePrixBaseCalcul {
  HERITE("Hérité d'une autre formule"),
  CONSIGNATION("Consignation"),
  DECONSIGNATION("Déconsignation"),
  PRIX_PUBLIC_COLONNE_1("Prix public colonne 1"),
  LIGNE_VENTE("Ligne de vente"),
  CHANTIER("Chantier"),
  CONDITION_VENTE("Condition de vente"),
  PUMP_ARTICLE("PUMP article (paramètre CNV)"),
  PRV_ARTICLE("PRV article (paramètre CNV)"),
  COLONNE_TARIF("Colonne tarif"),
  INDEFINI("Prix de base non défini");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOriginePrixBaseCalcul(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
