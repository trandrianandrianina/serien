/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un acompte.
 * 
 * L'identifiant est composé du code établissement et d'un numéro d'acompte.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdReglementAcompte extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_NUMERO = 10;
  
  // Variables
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdReglementAcompte(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, Integer pNumero) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   */
  private IdReglementAcompte(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    numero = Integer.valueOf(0);
  }
  
  // -- Méthodes publiques
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdReglementAcompte getInstance(IdEtablissement pIdEtablissement, Integer pNumero) {
    return new IdReglementAcompte(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pNumero);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   */
  public static IdReglementAcompte getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdReglementAcompte(pIdEtablissement);
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdReglementAcompte controlerId(IdReglementAcompte pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de l'acompte est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de l'acompte n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du numéro de l'acompte.
   * Il doit être supérieur à zéro et doit comporter au maximum 10 chiffres (entre 1 et 9 999 999 999).
   */
  private Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de l'acompte n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro de l'acompte est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro de l'acompte est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  // -- Méthodes surchargées
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getIdEtablissement().hashCode();
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdReglementAcompte)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants d'acompte .");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdReglementAcompte id = (IdReglementAcompte) pObject;
    return getIdEtablissement().equals(id.getIdEtablissement()) && numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdReglementAcompte)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdReglementAcompte id = (IdReglementAcompte) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return numero.toString();
  }
  
  // -- Accesseurs
  
  public Integer getNumero() {
    return numero;
  }
  
}
