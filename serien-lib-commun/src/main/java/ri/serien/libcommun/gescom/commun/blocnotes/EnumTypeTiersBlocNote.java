/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.blocnotes;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de tiers pour le bloc-notes
 */
public enum EnumTypeTiersBlocNote {
  ARTICLE('A', "Article"),
  ARTICLE_GVM('V', "Article vente"),
  BORDEREAU_COMPOSITION('B', "Bordereau composition"),
  CLIENT('C', "Client"),
  CONTRAT_ENTRETIEN('E', "Contrat d'entretien"),
  DOCUMENT_VENTE('D', "Document de vente"),
  FOURNISSEUR('F', "Fournisseur"),
  INTERVENTION('I', "Intervention"),
  MATERIEL('M', "Matériel"),
  NOMENCLATURE('N', "Nomenclature"),
  PROSPECT('P', "Prospect"),
  REPRESENTANT('R', "Représentant"),
  BORDEREAU_TRANSPORTEUR_TYPE_1('T', "Bordereau transporteur type 1"),
  BORDEREAU_TRANSPORTEUR_TYPE_2('t', "Bordereau transporteur type 2"),
  CONDITION_VENTES('1', "Condition de vente"),
  CONDTION_ACHATS('2', "Condition d'achat"),
  AFFAIRE('1', "Affaire"),
  ACTION('2', "Action commerciale"),
  NOMENCLATURE_ENTETE('n', "Entête de nomenclature"),
  NOMENCLATURE_LIGNE('l', "Ligne de nomenclature"),
  OPERATION('o', "Opération"),
  BORDEREAU_PRODUCTION_ENTETE('p', "Entête de bordereau de production"),
  BORDEREAU_PRODUCTION_LIGNE('l', "Ligne de bordereau de production"),
  ATELIER('a', "Atelier"),
  ORDRE_FABRICATION_ENTETE('p', "Entête ordre de fabrication"),
  ORDRE_FABRICATION_LIGNE('q', "Ligne ordre de fabrication"),
  RELANCE('r', "Relance");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeTiersBlocNote(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeTiersBlocNote valueOfByCode(Character pCode) {
    for (EnumTypeTiersBlocNote value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de tiers du bloc-notes est invalide : " + pCode);
  }
}
