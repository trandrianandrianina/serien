/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.GroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.IdGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.ListeGroupeConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Liste de paramètres de conditions des ventes.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de paramètres de conditions de ventes.
 */
public class ListeParametreConditionVente
    extends ListeClasseMetier<IdParametreConditionVente, ParametreConditionVente, ListeParametreConditionVente> {
  
  // -- Méthodes publiques
  
  /**
   * Ajoute les éléments de la liste passée en paramètre en éliminant les doublons.
   * 
   * @param pListeACopier
   * @param pOrigine
   * @return
   */
  public ListeParametreConditionVente addAllSansDoublon(ListeParametreConditionVente pListeACopier, EnumOrigineConditionVente pOrigine) {
    if (pListeACopier == null || pListeACopier.isEmpty()) {
      return this;
    }
    for (ParametreConditionVente parametreConditionVente : pListeACopier) {
      if (this.contains(parametreConditionVente)) {
        continue;
      }
      parametreConditionVente.setOrigine(pOrigine);
      this.add(parametreConditionVente);
    }
    
    return this;
  }
  
  /**
   * Charge une liste de paramètres de conditions de vente à partir d'une liste d'identifiant.
   */
  @Override
  public ListeParametreConditionVente charger(IdSession pIdSession, List<IdParametreConditionVente> pListeId) {
    return null;
  }
  
  /**
   * Charger une liste de paramètres de conditions de vente pour un établissement.
   */
  @Override
  public ListeParametreConditionVente charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return null;
  }
  
  /**
   * Retourner la liste des conditions de ventes à partir d'identifiant de rattachement client code donné et d'une catégorie de
   * conditions.
   * @param pIdRattachementClient L'identifiant rattachement client.
   * @param pCategorieConditionVente La catégorie des conditions de ventes.
   * @param pRechercherConditionEmboitee Activer ou non la recheche dans les conditions emboitées.
   * @param pParametreEtablissement Le paramètre établissement.
   * @return La liste des conditions de ventes.
   */
  public ListeParametreConditionVente retournerListeConditionVente(IdRattachementClient pIdRattachementClient,
      EnumCategorieConditionVente pCategorieConditionVente, boolean pRechercherConditionEmboitee,
      ParametreEtablissement pParametreEtablissement) {
    // Contrôle que l'identifiant rattachement client est valide
    if (pIdRattachementClient == null) {
      return null;
    }
    
    // Contrôle que la liste des conditions de ventes ne soit pas vide
    if (isEmpty()) {
      return null;
    }
    
    List<IdRattachementClient> listeIdRattachementClientConditionEmboitee = new ArrayList<IdRattachementClient>();
    ListeParametreConditionVente listeParametreConditionVente = new ListeParametreConditionVente();
    
    // Parcourt la liste des conditions de ventes
    for (ParametreConditionVente parametreConditionVente : this) {
      // Filtre sur la catégorie
      if (pCategorieConditionVente != parametreConditionVente.getCategorie()) {
        continue;
      }
      
      // Filtre sur l'identifiant rattachement client de la condition de vente
      if (!pIdRattachementClient.equals(parametreConditionVente.getIdRattachementClient())) {
        continue;
      }
      
      // Filtre pour stocker l'identifiant des rattachements client des conditions emboîtées car elles sont traitées après
      if (parametreConditionVente.isConditionVenteEmboitee()) {
        // Si la recherche des conditions emboitées est activée et si le paramètre établissement est valide
        if (pRechercherConditionEmboitee && pParametreEtablissement != null) {
          IdRattachementArticle idRattachementArticle = parametreConditionVente.getIdRattachementArticle();
          // Contrôle l'identifiant de rattachement article
          if (idRattachementArticle == null) {
            continue;
          }
          // Si le code du rattachement article ne correspond pas à un numéro client, il faut contrôler la validité du groupe
          if (!IdClient.isNumeroClient(idRattachementArticle.getCodeRattachement())) {
            // Création de l'identifiant de groupe
            IdGroupeConditionVente idGroupeConditionVenteEmboitee = IdGroupeConditionVente.getInstance(
                pParametreEtablissement.getIdEtablissement(), parametreConditionVente.getIdRattachementArticle().getCodeRattachement());
            // Contrôle que l'identifiant du groupe soit dans la liste des groupes valides (cad que la plage de date est ok)
            if (pParametreEtablissement.getListeGroupeConditionVente() == null
                || pParametreEtablissement.getListeGroupeConditionVente().isEmpty()
                || pParametreEtablissement.getListeGroupeConditionVente().get(idGroupeConditionVenteEmboitee) == null) {
              continue;
            }
          }
          
          // Création de l'identifiant rattachement client
          IdRattachementClient idRattachementClientEmboitee =
              IdRattachementClient.getInstance(pParametreEtablissement.getIdEtablissement(), idRattachementArticle.getCodeRattachement());
          
          // Contrôle que le code de la condition emboitée corresponde à un identifiant de groupe actif
          if (!listeIdRattachementClientConditionEmboitee.contains(idRattachementClientEmboitee)) {
            listeIdRattachementClientConditionEmboitee.add(idRattachementClientEmboitee);
          }
        }
        continue;
      }
      
      // Ajout de la condition à la liste qui va être retournée
      listeParametreConditionVente.add(parametreConditionVente);
    }
    
    // Recherche des conditions de ventes emboitées si la liste n'est pas vide
    if (!listeIdRattachementClientConditionEmboitee.isEmpty()) {
      ListeParametreConditionVente listeConditionEmboitee =
          retournerListeConditionEmboitee(listeIdRattachementClientConditionEmboitee, pCategorieConditionVente);
      if (listeConditionEmboitee != null && !listeConditionEmboitee.isEmpty()) {
        listeParametreConditionVente.addAllSansDoublon(listeConditionEmboitee, null);
      }
    }
    
    return listeParametreConditionVente;
  }
  
  /**
   * Retourner la liste des conditions de ventes pour une liste de codes de conditions de ventes donnée et une catégorie de conditions.
   * @param pListeIdRattachementClientCondition La liste des identifiants rattachement client.
   * @param pCategorieConditionVente La catégorie des conditions de ventes.
   * @param pRechercherConditionEmboitee Activer ou non la recheche dans les conditions emboitées.
   * @param pParametreEtablissement Le paramètre établisemment.
   * @return La liste des conditions de ventes.
   */
  public ListeParametreConditionVente retournerListeConditionVente(List<IdRattachementClient> pListeIdRattachementClientCondition,
      EnumCategorieConditionVente pCategorieConditionVente, boolean pRechercherConditionEmboitee,
      ParametreEtablissement pParametreEtablissement) {
    if (pListeIdRattachementClientCondition == null || pListeIdRattachementClientCondition.isEmpty()) {
      return null;
    }
    
    ListeParametreConditionVente listeCompleteParametreConditionVente = new ListeParametreConditionVente();
    for (IdRattachementClient idRattachementClient : pListeIdRattachementClientCondition) {
      ListeParametreConditionVente liste = retournerListeConditionVente(idRattachementClient, pCategorieConditionVente,
          pRechercherConditionEmboitee, pParametreEtablissement);
      if (liste != null) {
        listeCompleteParametreConditionVente.addAllSansDoublon(liste, null);
      }
    }
    return listeCompleteParametreConditionVente;
  }
  
  /**
   * Extraire la liste en cours les conditions de ventes cumulatives.
   * C'est grâce au paramètre CN qu'une condition de ventes peut être définit comme cumulative ou non.
   * @param pIdEtablissement L'identifiant établissement.
   * @param pListeGroupeConditionVente Le liste des groupes de conditions de ventes.
   * @param pListeParametreConditionVenteCumulative La liste des conditions de ventes cumulatives.
   * @return La liste des conditions de ventes.
   */
  public ListeParametreConditionVente extraireConditionVenteCumulative(IdEtablissement pIdEtablissement,
      ListeGroupeConditionVente pListeGroupeConditionVente, ListeParametreConditionVente pListeParametreConditionVenteCumulative) {
    // Contôle des paramètres
    if (pIdEtablissement == null) {
      return null;
    }
    if (pListeGroupeConditionVente == null) {
      return null;
    }
    if (pListeParametreConditionVenteCumulative == null) {
      pListeParametreConditionVenteCumulative = new ListeParametreConditionVente();
    }
    
    // Recherche des conditions cumulatives dans la liste en cours
    int i = 0;
    while (i < this.size()) {
      IdRattachementClient idRattachementClient = get(i).getIdRattachementClient();
      // Contrôle du code afin de voir s'il peut s'agir ou non d'un groupe
      if (idRattachementClient == null || idRattachementClient.isClient()) {
        i++;
        continue;
      }
      // Vérification de la présence du groupe de condition de ventes dans la liste des paramètres 'CN'
      IdGroupeConditionVente idGroupeConditionVente = idRattachementClient.getIdGroupeConditionVente();
      GroupeConditionVente groupeConditionVente = pListeGroupeConditionVente.get(idGroupeConditionVente);
      // La condition de ventes est cumulative alors elle est déplacée dans la liste des cumulatives
      if (groupeConditionVente != null && groupeConditionVente.isCumulative()) {
        this.get(i).setCumulative(true);
        pListeParametreConditionVenteCumulative.add(this.get(i));
        this.remove(i);
      }
      else {
        i++;
      }
    }
    return pListeParametreConditionVenteCumulative;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourner une liste des conditions emboîtées à partir d'une liste d'identifiant rattachement client de conditions de ventes.
   * Cette recherche ne s'effectue que sur un seul niveau.
   * @param pListeIdRattachementClientConditionEmboitee La liste des identifiants des rattachements clients.
   * @param pCategorieConditionVente La catégorie des conditions de ventes.
   * @return La liste des conditions de ventes emboitées.
   */
  private ListeParametreConditionVente retournerListeConditionEmboitee(
      List<IdRattachementClient> pListeIdRattachementClientConditionEmboitee, EnumCategorieConditionVente pCategorieConditionVente) {
    if (pListeIdRattachementClientConditionEmboitee == null || pListeIdRattachementClientConditionEmboitee.isEmpty()) {
      return null;
    }
    
    ListeParametreConditionVente listeParametreConditionVenteEmboitee = new ListeParametreConditionVente();
    for (IdRattachementClient idRattachementClientConditionEmboitee : pListeIdRattachementClientConditionEmboitee) {
      for (ParametreConditionVente parametreConditionVente : this) {
        // Filtre sur la catégorie
        if (pCategorieConditionVente != parametreConditionVente.getCategorie()) {
          continue;
        }
        // Filtre sur l'identifiant du rattachement client de la condition de ventes
        if (!Constantes.equals(idRattachementClientConditionEmboitee, parametreConditionVente.getIdRattachementClient())) {
          continue;
        }
        // Filtre afin d'éliminer les références aux conditions emboitées (type rattachement = '*' et code rattachement <> vide)
        if (parametreConditionVente.isConditionVenteEmboitee()) {
          continue;
        }
        
        // Ajout à la liste s'il n'existe pas déjà
        if (!listeParametreConditionVenteEmboitee.contains(parametreConditionVente)) {
          listeParametreConditionVenteEmboitee.add(parametreConditionVente);
        }
      }
    }
    
    return listeParametreConditionVenteEmboitee;
  }
}
