/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête d'un document de vente.
 */
public enum EnumCodeEnteteDocumentVente {
  DEVIS('D', "Devis"),
  DEVIS_ORIGINE('d', "Devis d'origine"),
  COMMANDE_OU_BON('E', "Commande ou bon"),
  COMMANDE_OU_BON_ORIGINE('e', "Commande ou bon d'origine"),
  COMMANDE_OU_BON_ANNULE('9', "Bon annul\u00e9"),
  FACTURE_REGROUPEE('X', "Facture regroup\u00e9e"),
  RELEVE_FACTURES('R', "Relev\u00e9 de factures");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumCodeEnteteDocumentVente(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumCodeEnteteDocumentVente valueOfByCode(Character pCode) {
    for (EnumCodeEnteteDocumentVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code entête du document de vente est invalide : " + pCode);
  }
}
