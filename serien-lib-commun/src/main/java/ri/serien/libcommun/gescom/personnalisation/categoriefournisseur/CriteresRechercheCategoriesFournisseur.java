/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.categoriefournisseur;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;

public class CriteresRechercheCategoriesFournisseur extends CriteresBaseRecherche {
  
  public static final int RECHERCHE_CATEGORIE_FOURNISSEUR = 1;
}
