/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.IdCategorieClient;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe contient les données minimales d'un client pour permettre sa sélection.
 * 
 * L'idée est d'avoir une classe comportant le minimum de données mais permettant de sélectionner un client (dans le composant SNClient
 * par exemple). Moins il y a de données, plus le transfert réseau est rapide et plus l'encombrement mémoire est réduit. Il faut donc y
 * réfléchir à deux fois avant d'ajouter un nouvel attribut.
 */
public class ClientBase extends AbstractClasseMetier<IdClient> {
  // Constantes
  public static final int TAILLE_ZONE_NOM = 30;
  public static final int TAILLE_ZONE_VILLE = 24;
  public static final int TAILLE_ZONE_CODEPOSTAL = 5;
  
  // Variables fichiers
  private Adresse adresse = new Adresse();
  private IdCategorieClient idCategorieClient = null;
  private EnumTypeImageClient typeImageClient = EnumTypeImageClient.PROFESSIONNEL;
  private EnumTypeCompteClient typeCompteClient = EnumTypeCompteClient.EN_COMPTE;
  private Boolean factureEnTTC = false;
  private String numeroTelephone = "";
  
  /**
   * Constructeur avec l'idenfiant du client.
   */
  public ClientBase(IdClient pIdClient) {
    super(pIdClient);
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdClient controlerId(ClientBase pClientBase, boolean pVerifierExistance) {
    if (pClientBase == null) {
      throw new MessageErreurException("Le client est invalide.");
    }
    return IdClient.controlerId(pClientBase.getId(), true);
  }
  
  /**
   * Retourner le nom du client.
   * - "Civilité Nom Prénom" pour les particuliers.
   * - "Raison sociale" pour les professionels.
   */
  @Override
  public String getTexte() {
    if (adresse != null) {
      return adresse.toString();
    }
    else {
      return id.getTexte();
    }
  }
  
  /**
   * Indiquer si les informations nécessaires pour la création d'un client comptant sont présentes.
   */
  public boolean isCreationPossibleClientComptant() {
    return adresse != null && adresse.isCreationPossibleClientComptant();
  }
  
  /**
   * Adresse du client.
   */
  public Adresse getAdresse() {
    return adresse;
  }
  
  /**
   * Modifier l'adresse du client.
   */
  public void setAdresse(Adresse pAdresse) {
    if (pAdresse != null) {
      adresse = pAdresse;
    }
    else {
      adresse = new Adresse();
    }
  }
  
  /**
   * Identifiant de la catégorie client.
   */
  public IdCategorieClient getIdCategorieClient() {
    return idCategorieClient;
  }
  
  /**
   * Modifier la catégorie du client.
   */
  public void setIdCategorieClient(IdCategorieClient pIdCategorieClient) {
    idCategorieClient = pIdCategorieClient;
  }
  
  /**
   * Type d'image client (professionel, particulier, ...).
   * Les méthodes isParticulier() et isProfessionel() fournissent un accès rapide à l'information.
   */
  public EnumTypeImageClient getTypeImageClient() {
    return typeImageClient;
  }
  
  /**
   * Modifier l'image d'un client (professionnel ou un particulier).
   */
  public void setTypeImageClient(EnumTypeImageClient pTypeImageClient) {
    typeImageClient = pTypeImageClient;
  }
  
  /**
   * Retourne si le client est un client particulier.
   */
  public boolean isParticulier() {
    return Constantes.equals(typeImageClient, EnumTypeImageClient.PARTICULIER);
  }
  
  /**
   * Retourne si le client est un client professionel.
   * Un client est professionel dès lors qu'il n'est pas particulier.
   */
  public boolean isProfessionel() {
    return !isParticulier();
  }
  
  /**
   * Type de compte client (en compte ou comptant).
   */
  public EnumTypeCompteClient getTypeCompteClient() {
    return typeCompteClient;
  }
  
  /**
   * Modifier le type de compte client (en compte ou comptant).
   */
  public void setTypeCompteClient(EnumTypeCompteClient pTypeCompteClient) {
    if (pTypeCompteClient == null) {
      throw new MessageErreurException("Le type de compte client est invalide.");
    }
    typeCompteClient = pTypeCompteClient;
  }
  
  /**
   * Indiquer si le client donné est comptant.
   */
  public boolean isClientComptant() {
    return typeCompteClient != null && typeCompteClient.equals(EnumTypeCompteClient.COMPTANT);
  }
  
  /**
   * Indiquer si le client donné est en compte.
   */
  public boolean isClientEnCompte() {
    return typeCompteClient != null && typeCompteClient.equals(EnumTypeCompteClient.EN_COMPTE);
  }
  
  /**
   * Indiquer si le client donné est un prospect.
   */
  public boolean isClientProspect() {
    return typeCompteClient != null && typeCompteClient.equals(EnumTypeCompteClient.PROSPECT);
  }
  
  /**
   * Indique si le client est facturé en TTC (true = TTC, false = HT).
   */
  public Boolean isFactureEnTTC() {
    if (factureEnTTC == null) {
      return false;
    }
    return factureEnTTC;
  }
  
  /**
   * Indique si le client est facturé en TTC (true = TTC, false = HT).
   */
  public void setFactureEnTTC(boolean pFactureEnTTC) {
    factureEnTTC = pFactureEnTTC;
  }
  
  /**
   * Numéro de téléphone du client.
   */
  public String getNumeroTelephone() {
    return numeroTelephone;
  }
  
  /**
   * Modifier le numéro de téléphone du client.
   */
  public void setNumeroTelephone(String pNumeroTelephone) {
    numeroTelephone = pNumeroTelephone;
  }
}
