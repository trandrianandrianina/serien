/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.alertedocument;

import java.io.Serializable;

public class AlertesReglementsDocument implements Serializable {
  // Constantes TODO il va falloir trouver les messages exacts
  private static final String ALERTE_MODE_REGLEMENT = "Attention au mode de règlement (%s)";
  
  // Variables
  private String messages = "";
  
  // Variables SVGVM0007 : Service ecrireReglementsDocument
  private boolean modeReglementParamME = false;
  private String codeModeReglementParamME = "";
  
  /**
   * Controle et liste les messages d'alerte.
   */
  public boolean controlerAlertes() {
    StringBuilder message = new StringBuilder();
    boolean alertesTrouvees = false;
    messages = "";
    
    if (modeReglementParamME) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_MODE_REGLEMENT, codeModeReglementParamME)).append('\n');
    }
    
    if (alertesTrouvees) {
      messages = message.toString().trim();
    }
    return alertesTrouvees;
  }
  
  // -- Accesseurs
  
  public String getMessages() {
    return messages;
  }
  
  public void setMessages(String messages) {
    this.messages = messages;
  }
  
  public boolean isModeReglementParamME() {
    return modeReglementParamME;
  }
  
  public void setModeReglementParamME(boolean modeReglementParamME) {
    this.modeReglementParamME = modeReglementParamME;
  }
  
  public String getCodeModeReglementParamME() {
    return codeModeReglementParamME;
  }
  
  public void setCodeModeReglementParamME(String codeModeReglementParamME) {
    this.codeModeReglementParamME = codeModeReglementParamME;
  }
  
}
