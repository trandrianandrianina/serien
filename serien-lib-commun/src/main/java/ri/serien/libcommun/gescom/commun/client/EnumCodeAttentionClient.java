/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête d'un document de vente.
 */
public enum EnumCodeAttentionClient {
  ATTENTION_CLIENT_ACTIF(0, "Client actif"), ATTENTION_ZONE_EN_ROUGE(1, "Message en rouge"),
  ATTENTION_CLIENT_INTERDIT(2, "Client interdit"), ATTENTION_LIVRAISON_INTERDITE(3, "Livraison interdite si dépassement"),
  ATTENTION_ATTENTE_DEPASSEMENT(4, "'Attente' si dépassement"), ATTENTION_ACOMPTE_OBLIGATOIRE(5, "Acompte obligatoire"),
  ATTENTION_PAIEMENT_A_LA_COMMANDE(6, "Paiement à la commande"), ATTENTION_CLIENT_DESACTIVE(9, "Client désactivé");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumCodeAttentionClient(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumCodeAttentionClient valueOfByCode(Integer pCode) {
    for (EnumCodeAttentionClient value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code fourni est invalide : " + pCode);
  }
}
