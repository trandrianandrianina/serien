/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Choix édition
 * Liste l'ensemble des choix possibles lors de l'édition d'un document de vente ou d'achat.
 */
public enum EnumModeEdition {
  VISUALISER(0, "Visualiser"),
  IMPRIMER(1, "Imprimer"),
  ENVOYER_PAR_MAIL(2, "Envoyer par mail"),
  ENVOYER_PAR_FAX(3, "Envoyer par fax");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumModeEdition(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumModeEdition valueOfByCode(Integer pCode) {
    for (EnumModeEdition value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le choix d'édition est invalide : " + pCode);
  }
  
}
