/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.representant;

import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;

public class Representant extends AbstractClasseMetier<IdRepresentant> {
  
  // Variables fichiers
  private int topSysteme = 0;
  private Date dateCreation = null;
  private Date dateModification = null;
  private Date dateTraitement = null;
  private String civilite = "";
  private String nom = "";
  private Adresse adresse = new Adresse();
  
  /**
   * Constructeur avec l'idenfiant.
   */
  public Representant(IdRepresentant pIdRepresentant) {
    super(pIdRepresentant);
  }
  
  // Méthodes surchargées
  
  @Override
  public String getTexte() {
    return nom;
  }
  
  // -- Accesseurs
  
  public int getTopSysteme() {
    return topSysteme;
  }
  
  public void setTopSysteme(int topSysteme) {
    this.topSysteme = topSysteme;
  }
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public Date getDateModification() {
    return dateModification;
  }
  
  public void setDateModification(Date dateModification) {
    this.dateModification = dateModification;
  }
  
  public Date getDateTraitement() {
    return dateTraitement;
  }
  
  public void setDateTraitement(Date dateTraitement) {
    this.dateTraitement = dateTraitement;
  }
  
  public String getCivilite() {
    return civilite;
  }
  
  public void setCivilite(String civilite) {
    this.civilite = civilite;
  }
  
  /**
   * Nom du représentant.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Nom du représentant suivi de son code entre parenthèse
   * Exemple : Jean DUPOND (JDD).
   * Par sécurité, le code est retourné seul au cas où le nom ne serait pas renseigné.
   */
  public String getNomAvecCode() {
    if (nom != null) {
      return nom + " (" + getId().getCode() + ")";
    }
    else {
      return getId().getCode();
    }
  }
  
  /**
   * Modifier le nom du représentant.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }
  
  public Adresse getAdresse() {
    return adresse;
  }
  
  public void setAdresse(Adresse adresse) {
    this.adresse = adresse;
  }
  
}
