/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrechantier;

import java.io.Serializable;
import java.math.BigDecimal;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;

/**
 * Classe regroupant toutes les données d'un article pour un chantier.
 */
public class ParametreChantier implements Serializable {
  private IdChantier idChantier = null;
  private EnumTypeChantier typeChantier = null;
  private IdArticle idArticle = null;
  private BigDecimal quantiteMinimum = null;
  
  private String codeUniteVente = null;
  private Integer numeroColonneTarif = null;
  private BigDecimal prixBaseHT = null;
  private BigDecimal prixVenteHT = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Indique si le prix de vente est lié à la quantité article commandé.
   * @return true=Prix de vente lié à la quantité, false=Prix de vente non lié à la quantité.
   */
  public boolean isPrixQuantitatif() {
    if (typeChantier != null && typeChantier == EnumTypeChantier.QUANTITATIVE) {
      return true;
    }
    return false;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'identifiant du chantier.
   * @return Identifiant chantier.
   */
  public IdChantier getIdChantier() {
    return idChantier;
  }
  
  /**
   * Modifier l'identifiant du chantier.
   * @param pIdChantier Identifiant chantier.
   */
  public void setIdChantier(IdChantier pIdChantier) {
    idChantier = pIdChantier;
  }
  
  /**
   * Retourner le type de chantier (à l'article ou en quantité).
   * @return Type de chantier.
   */
  public EnumTypeChantier getTypeChantier() {
    return typeChantier;
  }
  
  /**
   * Modifier le type chantier (à l'article ou en quantité).
   * @param pTypeChantier Type de chantier.
   */
  public void setTypeChantier(EnumTypeChantier pTypeChantier) {
    typeChantier = pTypeChantier;
    
    // Si le prix de l'article n'est pas lié à sa quantité alors la quantité est initialisée avec 1 car la valeur n'est qu'à titre
    // indicatif (la quantité d'article que pourrait acheter le client)
    if (!isPrixQuantitatif()) {
      quantiteMinimum = BigDecimal.ONE;
    }
  }
  
  /**
   * Retourner l'identifiant de l'article.
   * @return Identifiant article.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Modifier l'identifiant de l'article.
   * @param pIdArticle Identifiant article.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  /**
   * Retourner la quantité minimum pour bénéficier de ce prix chantier.
   * Ne doit contenir que la valeur 1 si le type de la condition est "Article".
   * @return Quantité minimum.
   */
  public BigDecimal getQuantiteMinimum() {
    return quantiteMinimum;
  }
  
  /**
   * Modifier la quantité minimum pour bénéficier de ce prix chantier.
   * @param pQuantiteMinimum Quantité minimum.
   */
  public void setQuantiteManinimum(BigDecimal pQuantiteMinimum) {
    quantiteMinimum = pQuantiteMinimum;
  }
  
  /**
   * Retourner le code de l'unité de vente du chantier.
   * @return Code unité de vente.
   */
  public String getCodeUniteVente() {
    return codeUniteVente;
  }
  
  /**
   * Modifier le code de l'unité de vente du chantier.
   * @param pCodeUniteVente Code unité de vente.
   */
  public void setCodeUniteVente(String pCodeUniteVente) {
    codeUniteVente = pCodeUniteVente;
  }
  
  /**
   * Retourner le numéro de la colonne tarif du chantier.
   * @return Numéro colonne tarfi.
   */
  public Integer getNumeroColonneTarif() {
    return numeroColonneTarif;
  }
  
  /**
   * Modifier le numéro de la colonne tarif du chantier.
   * @param pNumeroColonneTarif Numéro colonne tarfi.
   */
  public void setNumeroColonneTarif(Integer pNumeroColonneTarif) {
    this.numeroColonneTarif = pNumeroColonneTarif;
  }
  
  /**
   * Retourner le prix de base HT du chantier.
   * @return Prix base HT.
   */
  public BigDecimal getPrixBaseHT() {
    return prixBaseHT;
  }
  
  /**
   * Modifier le prix de base HT du chantier.
   * @param pPrixBaseHT Prix base HT.
   */
  public void setPrixBaseHT(BigDecimal pPrixBaseHT) {
    prixBaseHT = pPrixBaseHT;
  }
  
  /**
   * Retourner le prix net HT du chantier.
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetHT() {
    return prixVenteHT;
  }
  
  /**
   * Modifier le prix net HT du chantier.
   * @return Prix net HT.
   */
  public void setPrixNetHT(BigDecimal pPrixNetHT) {
    prixVenteHT = pPrixNetHT;
  }
}
