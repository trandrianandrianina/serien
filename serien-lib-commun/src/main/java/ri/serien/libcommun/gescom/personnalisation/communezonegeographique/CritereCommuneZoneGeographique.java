/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.communezonegeographique;

import ri.serien.libcommun.commun.recherche.CritereAvecEtablissement;

/**
 * Critères de recherche des communes des zones géographiques.
 */
public class CritereCommuneZoneGeographique extends CritereAvecEtablissement {
  
}
