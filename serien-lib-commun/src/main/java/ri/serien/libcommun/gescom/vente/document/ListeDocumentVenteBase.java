/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.chantier.ListeChantier;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste de documents de vente de base.
 * L'utilisation de cette classe est à privilégier dès qu'on manipule une liste de documents de vente de base.
 */
public class ListeDocumentVenteBase extends ListeClasseMetier<IdDocumentVente, DocumentVenteBase, ListeDocumentVenteBase> {
  /**
   * Constructeur.
   */
  public ListeDocumentVenteBase() {
  }
  
  /**
   * Construire une liste de document de bases correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des documents de base ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeDocumentVenteBase creerListeNonChargee(List<IdDocumentVente> pListeIdDocumentVente) {
    ListeDocumentVenteBase listeDocumentVenteBase = new ListeDocumentVenteBase();
    if (pListeIdDocumentVente != null) {
      for (IdDocumentVente idDocumentVente : pListeIdDocumentVente) {
        DocumentVenteBase documentVenteBase = new DocumentVenteBase(idDocumentVente);
        documentVenteBase.setCharge(false);
        listeDocumentVenteBase.add(documentVenteBase);
      }
    }
    return listeDocumentVenteBase;
  }
  
  /**
   * Charger les documents de ventes de base dont les identifiants sont fournis.
   */
  @Override
  public ListeDocumentVenteBase charger(IdSession pIdSession, List<IdDocumentVente> pListeId) {
    return ManagerServiceDocumentVente.chargerListeDocumentVenteBase(pIdSession, pListeId);
  }
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeDocumentVenteBase charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charger les libellés des chantiers des documents de vente de la liste.
   * 
   * @param pIdSession Identifiant de la session.
   */
  public void chargerLibelleChantier(IdSession pIdSession) {
    // Sortir si la liste est vide
    if (isEmpty()) {
      return;
    }
    
    // Construire la liste des identifiants de chantier
    List<IdChantier> listeIdChantier = new ArrayList<IdChantier>();
    for (DocumentVenteBase documentVenteBase : this) {
      // Ne pas ajouter l'identifiant s'il est null ou déjà présent dans la liste
      if (documentVenteBase.getIdChantier() != null && !listeIdChantier.contains(documentVenteBase.getIdChantier())) {
        listeIdChantier.add(documentVenteBase.getIdChantier());
      }
    }
    
    // Charger la liste des chantiers à partir de ces identifiants
    if (!listeIdChantier.isEmpty()) {
      ListeChantier listeChantier = new ListeChantier();
      listeChantier = listeChantier.charger(pIdSession, listeIdChantier);
      
      // Renseigner le libellé des chantiers des documents de vente
      for (DocumentVenteBase documentVenteBase : this) {
        Chantier chantier = listeChantier.get(documentVenteBase.getIdChantier());
        if (chantier != null) {
          documentVenteBase.setLibelleChantier(chantier.getLibelle());
        }
        else {
          documentVenteBase.setLibelleChantier(null);
        }
      }
    }
  }
}
