/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.outils.MessageErreurException;

public enum EnumEtapeExtraction {
  AUCUNE(0, "Aucune extraction"),
  PARTIELLE(1, "Extraction partielle"),
  COMPLETE(2, "Extraction compl\u00e8te");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumEtapeExtraction(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumEtapeExtraction valueOfByCode(Integer pCode) {
    for (EnumEtapeExtraction value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'étape de l'extraction est invalide : " + pCode);
  }
  
}
