/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;

/**
 * Cette classe contient les données minimales nécessaires à l'affichage d'une ligne d'achats dans une liste.
 */
public class LigneAchatBase extends AbstractClasseMetier<IdLigneAchat> {
  // Variables
  private Date dateLivraisonPrevue = null;
  private PrixAchat prixAchat = null;
  private String libelleArticle = null;
  protected EnumTypeLigneAchat typeLigne = null;
  protected EnumTypeArticle typeArticle = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public LigneAchatBase(IdLigneAchat pIdLigneAchat) {
    super(pIdLigneAchat);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner s'il s'agit d'une ligne commentaire.
   */
  public boolean isLigneCommentaire() {
    return (typeArticle != null && typeArticle.equals(EnumTypeArticle.COMMENTAIRE))
        || (typeLigne != null && typeLigne.equals(EnumTypeLigneAchat.COMMENTAIRE));
  }
  
  /**
   * Retourner s'il s'agit d'une ligne palette (c'est à dire contenant un article palette).
   */
  public boolean isLignePalette() {
    return typeArticle != null && typeArticle.equals(EnumTypeArticle.PALETTE);
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public LigneAchatBase clone() {
    LigneAchatBase o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (LigneAchatBase) super.clone();
      o.setId(id);
      if (getPrixAchat() != null) {
        o.setPrixAchat(prixAchat.clone());
      }
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  /**
   * Comparer 2 lignes.
   */
  @Override
  public boolean equals(Object ligneAComparer) {
    // Tester si l'objet est nous-même (optimisation)
    if (ligneAComparer == this) {
      return true;
    }
    
    if (ligneAComparer == null) {
      return false;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(ligneAComparer instanceof LigneAchatBase)) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    LigneAchatBase ligne = (LigneAchatBase) ligneAComparer;
    // On teste le prix de vente
    boolean pa = false;
    if (getPrixAchat() == null && ligne.getPrixAchat() == null) {
      pa = true;
    }
    else {
      pa = prixAchat.equals(ligne.prixAchat);
    }
    return id.equals(ligne.getId()) && pa;
  }
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + id.hashCode();
    // code = (int) (37 * code + getQuantiteCommandeeUV());
    // code = (int) (37 * code + getPrixAchatNetHT());
    // code = (int) (37 * code + getMontantHT());
    return code;
  }
  
  @Override
  public String getTexte() {
    // Retourner le texte de l'identifiant à défaut de mieux car je ne vois pas quel libellé pertinent on a pour l'instant.
    return id.getTexte();
  }
  
  /**
   * Comparer si les unites de vente et d'achat sont identiques
   */
  public boolean isUniteConditionnementIdentique() {
    return this.prixAchat.getIdUCA().equals(this.prixAchat.getIdUA());
  }
  
  /**
   * Retourner le prix d'achat
   */
  public PrixAchat getPrixAchat() {
    return prixAchat;
  }
  
  /**
   * Mettre à jour le prix d'achat
   */
  public void setPrixAchat(PrixAchat prixAchat) {
    this.prixAchat = prixAchat;
  }
  
  /**
   * Retourner la date de livraison
   */
  public Date getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  /**
   * Mettre à jour la date de livraison
   */
  public void setDateLivraisonPrevue(Date dateLivraisonPrevue) {
    this.dateLivraisonPrevue = dateLivraisonPrevue;
  }
  
  /**
   * Retourner le libellé
   */
  public String getLibelleArticle() {
    return libelleArticle;
  }
  
  /**
   * Mettre à jour le libellé
   */
  public void setLibelleArticle(String pLibelle) {
    if (pLibelle == null) {
      libelleArticle = null;
    }
    else {
      libelleArticle = pLibelle.trim();
    }
  }
  
  /**
   * Retourner le type de l'article
   */
  public EnumTypeArticle getTypeArticle() {
    return typeArticle;
  }
  
  /**
   * Mettre à jour le type de l'article
   */
  public void setTypeArticle(EnumTypeArticle typeArticle) {
    this.typeArticle = typeArticle;
  }
  
  /**
   * Retourner le type de la ligne
   */
  public EnumTypeLigneAchat getTypeLigne() {
    return typeLigne;
  }
  
  /**
   * Mettre à jour le type de la ligne
   */
  public void setTypeLigne(EnumTypeLigneAchat typeLigne) {
    this.typeLigne = typeLigne;
  }
}
