/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste de ligne de ventes.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de lignes de vente.
 */
public class ListeLigneVenteBase extends ListeClasseMetier<IdLigneVente, LigneVenteBase, ListeLigneVenteBase> {
  /**
   * Constructeur.
   */
  public ListeLigneVenteBase() {
  }
  
  /**
   * Charger les lignes de ventes de base dont les identifiants sont fournis.
   */
  @Override
  public ListeLigneVenteBase charger(IdSession pIdSession, List<IdLigneVente> pListeId) {
    return ManagerServiceDocumentVente.chargerListeLigneVenteBase(pIdSession, pListeId);
  }
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeLigneVenteBase charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Construire une liste de lignes de base correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des lignes de base ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeLigneVenteBase creerListeNonChargee(List<IdLigneVente> pListeIdLigneVente) {
    ListeLigneVenteBase listeLigneVenteBase = new ListeLigneVenteBase();
    if (pListeIdLigneVente != null) {
      for (IdLigneVente idLigneVente : pListeIdLigneVente) {
        LigneVenteBase ligneVenteBase = new LigneVenteBase(idLigneVente);
        ligneVenteBase.setCharge(false);
        listeLigneVenteBase.add(ligneVenteBase);
      }
    }
    return listeLigneVenteBase;
  }
  
  /**
   * Retourner une LigneVente de la liste à partir de son identifiant.
   */
  public LigneVenteBase retournerLigneVenteBaseParId(IdLigneVente pIdLigneVente) {
    if (pIdLigneVente == null) {
      return null;
    }
    for (LigneVenteBase ligneVente : this) {
      if (ligneVente != null && Constantes.equals(ligneVente.getId(), pIdLigneVente)) {
        return ligneVente;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner une LigneVente de la liste à partir de l'ID de l'article qu'elle contient.
   */
  public LigneVenteBase retournerLigneVenteParArticle(IdArticle pIdArticle) {
    if (pIdArticle == null) {
      return null;
    }
    for (LigneVenteBase ligneVente : this) {
      if (ligneVente != null && Constantes.equals(ligneVente.getIdArticle(), pIdArticle)) {
        return ligneVente;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un LigneVente est présent dans la liste.
   */
  public boolean isPresent(IdLigneVente pIdLigneVente) {
    return retournerLigneVenteBaseParId(pIdLigneVente) != null;
  }
  
}
