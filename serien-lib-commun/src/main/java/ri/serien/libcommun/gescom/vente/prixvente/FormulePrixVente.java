/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;

import ri.serien.libcommun.gescom.personnalisation.formuleprix.FormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.IdFormulePrix;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineCoefficient;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineModeTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOriginePrixBaseCalcul;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOriginePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineTauxTVA;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineUniteVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.ParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.EnumTypeChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.ParametreChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.ParametreClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumModeTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.ParametreDocumentVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceCoefficient;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

/**
 * Cette classe regroupe tous les calculs permettant d'obtenir un prix de vente.
 * 
 * La séquence de calcul d'un prix de vente est unique à tous les cas de figure : prix standard, prix CNV, prix garanti. Ce qui détermine
 * le résultat ce sont les paramètres passés au constructeur. Notamment, le ParametreLigneVente qui pour le calcul du prix standard et le
 * calcul des CNV doit être à null car ce paramètre à la priorité sur les autres paramètres.
 * En mode prix garanti, le calcul n'est effectué qu'avec les données de ParametreLigneVente car les autres paramètres ont pu avoir leur
 * données changées depuis la création de la ligne et le but reste de retrouver le prix identique au calcul d'origine.
 */
public class FormulePrixVente implements Serializable {
  private static final String LIBELLE_PRIX_STANDARD = "Prix standard";
  private static final String LIBELLE_PRIX_CONSIGNE = "Prix consigné";
  private static final String LIBELLE_PRIX_CHANTIER = "Prix chantier";
  private static final String LIBELLE_CNV_PRIORITAIRE = "CNV Prioritaire";
  
  // Paramètres
  private ParametreEtablissement parametreEtablissement = null;
  private ParametreClient parametreClientLivre = null;
  private ParametreClient parametreClientFacture = null;
  private ParametreArticle parametreArticle = null;
  private ParametreTarif parametreTarif = null;
  private ParametreConditionVente parametreConditionVente = null;
  private ParametreChantier parametreChantier = null;
  private ParametreLigneVente parametreLigneVente = null;
  private ParametreDocumentVente parametreDocumentVente = null;
  private BigDecimal prixBaseHeriteHT = null;
  private BigDecimal prixBaseHeriteTTC = null;
  
  // Description de la formule prix de vente
  private EnumTypeFormulePrixVente typeFormulePrixVente = null;
  private String libelle = null;
  
  // Taux TVA
  private boolean modeTTC = false;
  private EnumOrigineTauxTVA origineTauxTVA = null;
  private BigPercentage tauxTVA = null;
  
  // Arrondi prix de vente
  private EnumOrigineUniteVente origineUniteVente = null;
  private String codeUniteVente = null;
  private ArrondiPrix arrondiPrix = null;
  
  // Colonne tarif
  private EnumOrigineColonneTarif origineColonneTarif = null;
  private Integer numeroColonneTarif = null;
  
  // Prix de base calcul HT
  private BigDecimal prixBaseColonneTarifHT = null;
  private BigDecimal prixBaseColonneTarifTTC = null;
  private EnumOriginePrixBaseCalcul originePrixBaseCalcul = null;
  private BigDecimal prixBaseCalculHT = null;
  private BigDecimal prixBaseCalculTTC = null;
  
  // Remises
  private boolean remisable = true;
  private BigDecimal remiseEnValeur = null;
  private EnumOrigineTauxRemise origineTauxRemise = null;
  private BigPercentage tauxRemise1 = null;
  private BigPercentage tauxRemise2 = null;
  private BigPercentage tauxRemise3 = null;
  private BigPercentage tauxRemise4 = null;
  private BigPercentage tauxRemise5 = null;
  private BigPercentage tauxRemise6 = null;
  private EnumOrigineModeTauxRemise origineModeTauxRemise = null;
  private EnumModeTauxRemise modeTauxRemise = null;
  private EnumOrigineAssietteTauxRemise origineAssietteTauxRemise = null;
  private EnumAssietteTauxRemise assietteTauxRemise = null;
  private EnumOrigineCoefficient origineCoefficient = null;
  private BigDecimal coefficient = null;
  private BigDecimal prixBaseRemiseHT = null;
  private BigDecimal prixBaseRemiseTTC = null;
  
  // Prix net HT
  private boolean gratuit = false;
  private EnumOriginePrixNet originePrixNet = null;
  private BigDecimal prixNetHT = null;
  private BigDecimal prixNetTTC = null;
  
  // Taux de remise unitaire
  private BigPercentage tauxRemiseUnitaire = null;
  
  // Montant HT
  private BigDecimal quantiteUV = null;
  private BigDecimal montantNonRemiseHT = null;
  private BigDecimal montantNonRemiseTTC = null;
  private BigDecimal montantHT = null;
  private BigDecimal montantTTC = null;
  private BigDecimal valeurRemiseMontantHT = null;
  private BigDecimal valeurRemiseMontantTTC = null;
  private BigDecimal montantTVA = null;
  
  // Divers
  private String commentaire = null;
  private Boolean utilise = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public FormulePrixVente(EnumTypeFormulePrixVente pTypeFormulePrixVente, ParametreEtablissement pParametreEtablissement,
      ParametreLigneVente pParametreLigneVente, ParametreDocumentVente pParametreDocumentVente, ParametreClient pParametreClientFacture,
      ParametreClient pParametreClientLivre, ParametreArticle pParametreArticle, ParametreTarif pParametreTarif,
      ParametreConditionVente pParametreConditionVente, ParametreChantier pParametreChantier) {
    
    // Contrôle des paramètres obligatoires
    if (pTypeFormulePrixVente == null) {
      throw new MessageErreurException(
          "Le type de formule prix de vente est invalide alors qu'il est oligatoire pour le calcul d'un prix.");
    }
    if (pParametreEtablissement == null) {
      throw new MessageErreurException("Le paramètre général est invalide alors qu'il est oligatoire pour le calcul d'un prix.");
    }
    if (pParametreArticle == null) {
      throw new MessageErreurException("Le paramètre article est invalide alors qu'il est oligatoire pour le calcul d'un prix.");
    }
    
    // Initialiser le type de formule prix de vente
    typeFormulePrixVente = pTypeFormulePrixVente;
    
    // Initialiser les paramètres
    parametreEtablissement = pParametreEtablissement;
    parametreLigneVente = pParametreLigneVente;
    parametreDocumentVente = pParametreDocumentVente;
    parametreClientFacture = pParametreClientFacture;
    parametreClientLivre = pParametreClientLivre;
    parametreArticle = pParametreArticle;
    parametreTarif = pParametreTarif;
    parametreConditionVente = pParametreConditionVente;
    parametreChantier = pParametreChantier;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Lancer le calcul du prix de vente à partir des paramètres.
   */
  public void calculer() {
    // Effacer le résultat du calcul précédent
    effacerResultat();
    
    // Déterminer le libellé.
    determinerLibelle();
    
    // Déterminer le mode HT ou TTC
    determinerModeTTC();
    
    // Calculer le taux de TVA
    // A faire avant calculerPrixBaseRemiseHT()
    determinerTauxTVA();
    
    // Calculer l'unité
    determinerUniteVente();
    
    // Calculer l'arrondi
    determinerArrondiPrix();
    
    // Déterminer la quantité en UV
    determinerQuantiteUV();
    
    // Calculer le prix de vente de la colonne tarif
    determinerNumeroColonneTarif();
    
    // Calculer le prix de base HT issu de la colonne tarif
    // A faire après determinerColonneTarif()
    determinerPrixBaseColonneTarif();
    
    // Calculer le prix de base HT calcul
    // A faire après determinerPrixBaseHTColonneTarif()
    determinerPrixBaseCalcul();
    
    // Contrôler si l'article est non remisable
    determinerRemisable();
    
    // Calculer la remise en valeur
    determinerRemiseEnValeur();
    
    // Calculer les tauxs de remises
    determinerTauxRemise();
    
    // Calculer le mode d'application des taux de remises
    // A faire après determinerTauxRemise()
    determinerModeTauxRemise();
    
    // Calculer l'assiette des taux de remises
    determinerAssietteTauxRemise();
    
    // Calculer coefficient
    determinerCoefficient();
    
    // Calculer le prix de base ajusté HT, c'est à dire le prix de base HT auquel on a déduit la remise ou ajouté l'augmentation
    // A faire après calculerTauxTVA() car la remise en valeur s'effectue sur le montant TTC
    calculerPrixBaseRemise();
    
    // Déterminer la gratuité
    determinerGratuit();
    
    // Calculer le prix net HT
    calculerPrixNet();
    
    // Calculer le taux de remise unitaire
    calculerTauxRemiseUnitaire();
    
    // Ajuster le numéro de la colonne tarif en fonction du prix net (en mode négoce)
    ajusterNumeroColonneTarif();
    
    // Calculer le montant HT avant application de la remise
    calculerMontantNonRemise();
    
    // Calculer le montant HT
    calculerMontant();
    
    // Calculer la valeur de la remise appliquée sur le montant HT
    // A faire après calculerMontantHT()
    calculerValeurRemiseMontant();
    
    // Calculer le montant de TVA
    // A faire après calculerMontantTTC()
    calculerMontantTVA();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées pour les différentes étapes du calcul
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Effacer le résultat du calcul prédédent.
   */
  private void effacerResultat() {
    // Général
    libelle = null;
    
    // TVA
    modeTTC = false;
    origineTauxTVA = null;
    tauxTVA = null;
    
    // Arrondi prix de vente
    origineUniteVente = null;
    codeUniteVente = null;
    arrondiPrix = null;
    
    // Colonne tarif
    origineColonneTarif = null;
    numeroColonneTarif = null;
    
    // Prix base calcul HT
    prixBaseColonneTarifHT = null;
    prixBaseColonneTarifTTC = null;
    originePrixBaseCalcul = null;
    prixBaseCalculHT = null;
    prixBaseCalculTTC = null;
    
    // Remises
    remisable = true;
    remiseEnValeur = null;
    origineTauxRemise = null;
    tauxRemise1 = null;
    tauxRemise2 = null;
    tauxRemise3 = null;
    tauxRemise4 = null;
    tauxRemise5 = null;
    tauxRemise6 = null;
    origineModeTauxRemise = null;
    modeTauxRemise = null;
    origineAssietteTauxRemise = null;
    assietteTauxRemise = null;
    origineCoefficient = null;
    coefficient = null;
    prixBaseRemiseHT = null;
    prixBaseRemiseTTC = null;
    
    // Prix net HT
    gratuit = false;
    originePrixNet = null;
    prixNetHT = null;
    prixNetTTC = null;
    
    // Taux de remise unitaire
    tauxRemiseUnitaire = null;
    
    // Montant HT
    quantiteUV = null;
    montantNonRemiseHT = null;
    montantNonRemiseTTC = null;
    montantHT = null;
    montantTTC = null;
    valeurRemiseMontantHT = null;
    valeurRemiseMontantTTC = null;
    montantTVA = null;
    
    // Divers
    commentaire = null;
  }
  
  /**
   * Déterminer le libellé de la formule prix de vente.
   * 
   * Le libellé est actuellement une valeur fixe qui dépend jute du type de formule prix de vente. Il est néanmoins déterminé
   * à chaque calcul pour permettre son adaptation à la volée si sa règle de construction évolue à l'avenir.
   * Il est stocké comme attribut de la classe pour être présent dans la sérialisation JSON.
   */
  private void determinerLibelle() {
    //
    switch (typeFormulePrixVente) {
      case STANDARD:
        libelle = LIBELLE_PRIX_STANDARD;
        break;
      
      case CONSIGNE:
        libelle = LIBELLE_PRIX_CONSIGNE;
        break;
      
      case CHANTIER:
        libelle = LIBELLE_PRIX_CHANTIER;
        break;
      
      case CNV_PRIORITAIRE:
        libelle = LIBELLE_CNV_PRIORITAIRE;
        break;
      
      case CNV_NORMALE:
      case CNV_CUMULATIVE:
      case CNV_QUANTITATIVE:
        libelle = parametreConditionVente.getLibelle();
        break;
    }
  }
  
  /**
   * Déterminer si le calcul est effectué en mode HT ou TTC.
   * 
   * Il est stocké comme attribut de la classe pour être présent dans la sérialisation JSON.
   * 
   * Pour déterminer le mode, on utilise par aordre de priorité :
   * 1- Le mode HT/TTC du document de vente.
   * 2- Le mode HT/TTC du Client.
   * 3- HT par défaut.
   */
  private void determinerModeTTC() {
    // Tester si document de vente est TTC
    if (parametreDocumentVente != null && parametreDocumentVente.isModeTTC()) {
      modeTTC = true;
      return;
    }
    
    // Testet si le client est TTC
    if (parametreClientFacture != null && parametreClientFacture.isModeTTC()) {
      modeTTC = true;
      return;
    }
    
    // Mode HT par défaut
    modeTTC = false;
  }
  
  /**
   * Déterminer le taux de TVA à appliquer.
   * 
   * Le taux de TVA est déterminé à partir du type de facturation.
   * 
   * TODO:
   * a) Se faire confirmer que le numéro de colonne de l'article soit bine celui dela DG et non du paramètre TF
   * b) Reste à gérer les cas des valeurs & et 5 dans les types de facturation (voir SNC-10897 + Marc). Mais peut être que cela n'a pas
   * d'impact ici.
   */
  private void determinerTauxTVA() {
    // Récupérer la colonne du taux de TVA dans la ligne de vente avec le numéro de colonne de l'entête
    // Le taux de TVA correspondant est lu dans l'entête du document de vente
    if (parametreLigneVente != null && parametreLigneVente.getNumeroTVAEntete() != null && parametreDocumentVente != null
        && parametreDocumentVente.getTauxTVA(parametreLigneVente.getNumeroTVAEntete()) != null) {
      origineTauxTVA = EnumOrigineTauxTVA.LIGNE_VENTE_AVEC_COLONNE_ENTETE;
      tauxTVA = parametreDocumentVente.getTauxTVA(parametreLigneVente.getNumeroTVAEntete());
      return;
    }
    
    // Récupérer le taux de TVA dans la ligne de vente avec le numéro de colonne de l'établissement
    // Le taux de TVA correspondant est lu dans l'établissement
    // Cas peu probable mais c'est au cas des données soient corrompues ou pas de document de vente
    if (parametreLigneVente != null && parametreLigneVente.getNumeroTVAEtablissement() != null && parametreEtablissement != null) {
      origineTauxTVA = EnumOrigineTauxTVA.LIGNE_VENTE_AVEC_COLONNE_ETABLISSEMENT;
      tauxTVA = parametreEtablissement.getTauxTVA(parametreLigneVente.getNumeroTVAEtablissement());
      return;
    }
    
    // Récupérer le numéro de colonne de la TVA de l'établissement stocké dans l'article
    // Le taux de TVA correspondant est lu dans l'établissement
    if (parametreArticle != null && parametreArticle.getNumeroTVAEtablissement() != null
        && parametreArticle.getNumeroTVAEtablissement() >= 1 && parametreArticle.getNumeroTVAEtablissement() <= 6
        && parametreEtablissement != null) {
      origineTauxTVA = EnumOrigineTauxTVA.ARTICLE;
      tauxTVA = parametreEtablissement.getTauxTVA(parametreArticle.getNumeroTVAEtablissement());
      return;
    }
    
    // Utiliser la colonne 1 si 0 est dans l'établissement
    // Le taux de TVA correspondant est lu dans l'établissement
    if (parametreArticle != null && parametreArticle.getNumeroTVAEtablissement() != null
        && parametreArticle.getNumeroTVAEtablissement() == 0 && parametreEtablissement != null) {
      origineTauxTVA = EnumOrigineTauxTVA.ARTICLE_AVEC_COLONNE_ZERO;
      tauxTVA = parametreEtablissement.getTauxTVA(1);
      return;
    }
    
    // Sinon pas de TVA
    origineTauxTVA = EnumOrigineTauxTVA.DEFAUT;
    tauxTVA = null;
  }
  
  /**
   * Déterminer l'unité utilisée pour le calcul.
   * 
   * Il est nécessaire de connaître quelle unité est utilisée pour le prix le vente afin d'en déduire l'arrondi. L'unité peut être
   * récupérer par ordre de priorité :
   * - Sur la ligne de vente.
   * - Sur l'article.
   */
  private void determinerUniteVente() {
    // Tester si l'arrondi est présent sur la ligne de vente
    if (parametreLigneVente != null && parametreLigneVente.getCodeUniteVente() != null
        && !parametreLigneVente.getCodeUniteVente().trim().isEmpty()) {
      origineUniteVente = EnumOrigineUniteVente.LIGNE_VENTE;
      codeUniteVente = parametreLigneVente.getCodeUniteVente();
    }
    // Tester si l'arrondi est présent sur l'article
    else if (parametreArticle != null && parametreArticle.getCodeUniteVente() != null
        && !parametreArticle.getCodeUniteVente().trim().isEmpty()) {
      origineUniteVente = EnumOrigineUniteVente.ARTICLE;
      codeUniteVente = parametreArticle.getCodeUniteVente();
    }
    // Sinon, pas d'unité
    else {
      origineUniteVente = EnumOrigineUniteVente.DEFAUT;
      codeUniteVente = null;
    }
  }
  
  /**
   * Déterminer l'arrondi de prix utilisé pour le calcul.
   * 
   * Par défaut, les prix de vente sont arrondis à 2 chiffres après la virgule. Par contre, si le code de l’unité de vente utilisée
   * pour le calcul comporte le caractère ‘*’ en deuxième position, l’article est vendu à la centaine. Du coup, le prix unitaire
   * est le prix de la centaine divisé par cent. Le prix de base HT et le prix net HT sont présentés avec 4 chiffres après la virgules.
   * A la sortie de cette méthode, la variable "arrondiPrix" doit être obligatoirement différente de null.
   */
  private void determinerArrondiPrix() {
    arrondiPrix = ArrondiPrix.getInstance(codeUniteVente);
  }
  
  /**
   * Déterminer la quantité en UV.
   * 
   * La quantité est lue dans la ligne de vente. Si la quantité lue est null ou égale à zéro, la quantité 1 est utilisée par défaut.
   * S'il n'y a pas de lignes de vente (cas d'un calcu ld eprix hors d'u ndocument de vente), la quantité 1 est utilisée par défaut.
   */
  private void determinerQuantiteUV() {
    if (parametreLigneVente != null && parametreLigneVente.getQuantiteUV() != null
        && parametreLigneVente.getQuantiteUV().compareTo(BigDecimal.ZERO) != 0) {
      quantiteUV = parametreLigneVente.getQuantiteUV();
    }
    else {
      quantiteUV = BigDecimal.ONE;
    }
  }
  
  /**
   * Déterminer le numéro de la colonne tarif utilisée pour le calcul.
   *
   * A la sortie de cette méthode, la variable "colonneTarif" doit être obligatoirement différente de null.
   * Note:
   * En thérorie, tous les prix de vente des colonnes tarifs sont HT mais il faut savoir que dans le SGVM77R, lorsque le client était TTC
   * il y avait une subtilité de merde (désactivée par Marc le 17/05/2022) qui lorsque le libellé de la colonne tarif contenait "TTC" ou
   * "T.T.C" alors le prix était considéré comme TTC et donc la TVA n'était pas appliquée dessus par contre il fallait faire l'opération
   * inverse pour avoir le HT.
   * 
   * La colonne tarif à utiliser peut être fourni par différentes sources. Il faut déterminer quelle colonne utiliser parmi celles
   * disponibles. Il y a une notion de priorité entre ces colonnes en fonction de leur origine. On sélectionne la colonne tarif suivant
   * l’ordre de priorité suivant :
   * 
   * 1- La colonne tarif du chantier. Le prix chantier est prioritaire sur tout. Il n'est pas modifiable dans la ligne de vente.
   * 2- La colonne de tarif de la ligne de vente (L1TAR). Si L1TAR contient une valeur différente de 0 et que L1TT = 3 alors cela
   * signifie que la valeur a été saisie manuellement pour la ligne et c’est cette colonne qui fait référence.
   * 3- La colonne de tarif de la condition de vente (T1VAL). Dans ce cas, le champ de la ligne de vente L1TT prend la valeur 2. A noter
   * que si cette condition de vente offre un meilleur prix net HT unitaire que les conditions de ventes précédentes, alors la colonne de
   * cette condition de ventes est conservée pour être réutilisée pour le prochain calcul de condition de vente. On appelle cette
   * information colonne de tarif héritée.
   * 4- La colonne de tarif hérité des conditions de ventes précédentes (T1VAL). A noter que si cette condition de ventes offre un
   * meilleur prix net HT unitaire alors la colonne de cette condition de ventes est stockée dans la ligne pour être réutilisée pour le
   * prochain calcul de CNV. Dans ce cas, le champ de la ligne de vente L1TT prend la valeur 2.
   * 5- La colonne de tarif de l’entête du document de vente (E1TAR) pour les cas de calculs avec un document de vente. Par défaut, il
   * s’agit de la colonne de tarif du client (CLTAR). La colonne de tarif sera récupérée ici puisque la colonne de tarif est forcément
   * renseignée dans l’entête du document de vente. Par contre, il faut prévoir les cas de calcul sans document de vente. E1TAR ne peut
   * pas être égal à 0, E1TAR est toujours initialisé à 1 si CLTAR = 0 et PS105 = ' '.
   * 6- La colonne de tarif du client (CLTAR). Noter que dans le client, la valeur 0 pour le champ CLTAR correspond à la colonne 10. Par
   * ailleurs, si le client à la colonne tarifaire 1 (ce qui signifie qu’il a la valeur par défaut) ou si le client n’est pas fourni,
   * utiliser les règles qui suivent.
   * 7- La colonne de tarif par défaut (prix public) paramétrée dans le PS105 si défini. On utilise la colonne tarifaire du PS105
   * uniquement si le client n’est pas fournit ou s’il est configuré avec la colonne tarifaire 1 (on parle bien de la colonne tarifaire du
   * client, pas celle de la condition de vente ou de l’entête du document de vente).
   * 8- Sinon la colonne de tarif 1 (valeur par défaut ou prix public).
   * 
   */
  private void determinerNumeroColonneTarif() {
    // Ne pas utiliser une colonne de tarif si l'article est consigné
    if (parametreArticle != null && parametreArticle.getIdReferenceTarif() != null
        && parametreArticle.getIdReferenceTarif().isConsigne()) {
      return;
    }
    
    // Utiliser la colonne tarif du chantier et si le numéro est supérieur à 0
    if (parametreChantier != null && parametreChantier.getNumeroColonneTarif() != null && parametreChantier.getNumeroColonneTarif() > 0) {
      origineColonneTarif = EnumOrigineColonneTarif.CHANTIER;
      numeroColonneTarif = parametreChantier.getNumeroColonneTarif();
      return;
    }
    
    // Utiliser la colonne tarif de la ligne de vente si elle définie
    // Si le numéro de colonne tarif est supérieur à 0 (L1TAR) et que la provenance (L1TT) est "Saisie manuellement", alors cela
    // signifie que la valeur a été saisie manuellement pour la ligne et c’est cette colonne tarif qui fait référence.
    if (parametreLigneVente != null && parametreLigneVente.getNumeroColonneTarif() != null
        && parametreLigneVente.getNumeroColonneTarif() > 0
        && parametreLigneVente.getProvenanceColonneTarif() == EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE) {
      origineColonneTarif = EnumOrigineColonneTarif.LIGNE_VENTE;
      numeroColonneTarif = parametreLigneVente.getNumeroColonneTarif();
      return;
    }
    
    // Utiliser la colonne tarif de la condition de vente si elle définie et si le numéro est supérieur à 0
    if (parametreConditionVente != null && parametreConditionVente.getNumeroColonneTarif() != null
        && parametreConditionVente.getNumeroColonneTarif() > 0) {
      origineColonneTarif = EnumOrigineColonneTarif.CONDITION_VENTE;
      numeroColonneTarif = parametreConditionVente.getNumeroColonneTarif();
      return;
    }
    
    // Utiliser la colonne tarif issue des conditions de vente précédentes
    // Issue de la condition de vente si un numéro de colonne tarif existe et est supérieur à 0
    if (parametreConditionVente != null && parametreConditionVente.getNumeroColonneTarifHeritee() != null
        && parametreConditionVente.getNumeroColonneTarifHeritee() > 0) {
      origineColonneTarif = EnumOrigineColonneTarif.HERITE_CNVS_PRECEDENTES;
      numeroColonneTarif = parametreConditionVente.getNumeroColonneTarifHeritee();
      return;
    }
    
    // Utiliser la colonne tarif du document de vente si elle définie
    // Issu du document de vente, si le numéro de colonne tarif est supérieur à 0 c'est ce numéro qui est pris en compte
    if (parametreDocumentVente != null && parametreDocumentVente.getNumeroColonneTarif() != null
        && parametreDocumentVente.getNumeroColonneTarif() > 0) {
      origineColonneTarif = EnumOrigineColonneTarif.DOCUMENT_VENTE;
      numeroColonneTarif = parametreDocumentVente.getNumeroColonneTarif();
      return;
    }
    
    // Utiliser la colonne tarif du client si elle définie
    if (parametreClientFacture != null && parametreClientFacture.getNumeroColonneTarif() != null
        && parametreClientFacture.getNumeroColonneTarif() > 1) {
      origineColonneTarif = EnumOrigineColonneTarif.CLIENT;
      numeroColonneTarif = parametreClientFacture.getNumeroColonneTarif();
      return;
    }
    
    // Utiliser la colonne tarif du PS105 si elle définie
    if (parametreEtablissement != null && parametreEtablissement.getNumeroColonneTarifPS105() != null
        && parametreEtablissement.getNumeroColonneTarifPS105() > 0) {
      // Uniquement si la colonne tarif issue du client n'est pas fournit ou si elle vaut 1
      origineColonneTarif = EnumOrigineColonneTarif.PS105;
      numeroColonneTarif = Constantes.convertirTexteEnInteger(parametreEtablissement.getNumeroColonneTarifPS105().toString());
      return;
    }
    
    // Utiliser la colonne tarif 1 par défaut
    origineColonneTarif = EnumOrigineColonneTarif.COLONNE_UN_PAR_DEFAUT;
    numeroColonneTarif = 1;
  }
  
  /**
   * Déterminer le prix de base issu de la colonne de tarif (HT et TTC).
   * 
   * Retourner le prix de base de la colonne souhaitée si :
   * - le prix de vente est supérieur à 0,
   * - le PS305 n'est pas actif,
   * - le numéro de colonne vaut 1 (donc la dernière et pas de balayage possible)
   */
  private void determinerPrixBaseColonneTarif() {
    // Vérifier si les pré-requis sont présents
    if (parametreTarif == null || numeroColonneTarif == null) {
      return;
    }
    
    // Récupérer le prix de base HT de la colonne de tarif sélectionnée
    // C'est toujours un prix HT qui est stocké dans les colonnes tarifs
    prixBaseColonneTarifHT = parametreTarif.getPrixBaseHT(numeroColonneTarif);
    
    // Si le PS305 est actif et que le prix de base HT de la colonne courante est vide ou non positif, on recherche le premier
    // prix de base HT valide dans les colonnes précédentes
    if ((prixBaseColonneTarifHT == null || prixBaseColonneTarifHT.compareTo(BigDecimal.ZERO) == 0)
        && parametreEtablissement.isRechercheColonneTarifNonNullePS305()) {
      for (int indexColonne = numeroColonneTarif - 1; indexColonne >= 1; indexColonne--) {
        prixBaseColonneTarifHT = parametreTarif.getPrixBaseHT(indexColonne);
        numeroColonneTarif = indexColonne;
        if (prixBaseColonneTarifHT != null && prixBaseColonneTarifHT.compareTo(BigDecimal.ZERO) > 0) {
          break;
        }
      }
    }
    
    // Arrondir le prix de base HT obtenu avec la graduation décimale du tarif
    // Les prix de base HT enregistrés dans la base de données sont sensés être correct mais on ne sait jamais, on refait donc le calcul
    if (prixBaseColonneTarifHT != null && parametreTarif.getGraduationDecimaleTarif() != null) {
      prixBaseColonneTarifHT = parametreTarif.getGraduationDecimaleTarif().appliquerGraduation(prixBaseColonneTarifHT);
    }
    
    // Appliquer l'arrondi utilisé dans ce calcul
    if (prixBaseColonneTarifHT != null) {
      prixBaseColonneTarifHT = ArrondiPrix.appliquer(prixBaseColonneTarifHT, arrondiPrix);
    }
    
    // Calculer du prix base TTC de la colonne de tarif
    if (modeTTC) {
      prixBaseColonneTarifTTC = calculerPrixTTC(prixBaseColonneTarifHT);
    }
  }
  
  /**
   * Déterminer le prix de base (HT et TTC) utilisé pour le calcul du prix net.
   * 
   * Il s'agit du prix sur lequel sont appliqués les remises afin de calculer le prix net. Ensuite, le prix de base qui sert de
   * référence pour le calcul du taux de remise unitaire. Le prix de base est affiché sur les lignes des documents de ventes.
   * 
   * Le prix de base calcul à utiliser peut être fourni par différentes sources. Le prix de base calcul est déterminé suivant l’ordre de
   * priorité suivant :
   * 1- Si le prix de base (HT ou TTC) hérité est renseigné, alors le prix de base calcul est ce prix. Ce cas est utilisé pour calculer
   * les conditions de ventes cumulatives. Le prix issus des autres conditions de ventes (normales ou quantitatives) est fourni à ce cas
   * de calcul via le prix de base dit « hérité ».
   * 2- Le prix consignation ou désignation pour les articles consignés.
   * 3- Le prix de base de la colonne 1 du tarif (désigné prix public dans ce cas) si PS316 = true.
   * 4- Le prix de base (HT ou TTC) de la ligne de vente s'il a été saisi manuellement.
   * 5- Le prix de base HT du chantier.
   * 6- Le prix de base HT de la condition de vente.
   * 7- Le PUMP HT de l'article si la condition de vente le demande.
   * 8- Le PRV HT de l'article si la condition de vente le demande.
   * 9- Le prix de base (HT ou TTC) est le prix de base de la colonne de tarif déterminée à l’étape précédente.
   */
  private void determinerPrixBaseCalcul() {
    // Utiliser le prix de base hérité (issu des autres formules prix de ventes)
    // Si le prix de base hérité est renseigné, alors le prix de base est ce prix. Ce cas est utilisé pour calculer les conditions de
    // ventes cumulatives. Le prix issu des autres conditions de ventes (normales ou quantitatives) est fourni à ce cas de calcul via le
    // prix de base dit « hérité ».
    if (prixBaseHeriteHT != null || prixBaseHeriteTTC != null) {
      originePrixBaseCalcul = EnumOriginePrixBaseCalcul.HERITE;
      prixBaseCalculHT = ArrondiPrix.appliquer(prixBaseHeriteHT, arrondiPrix);
      if (modeTTC) {
        prixBaseCalculTTC = ArrondiPrix.appliquer(prixBaseHeriteTTC, arrondiPrix);
      }
      return;
    }
    
    // Utiliser le prix consigné ou déconsigné si l'article est consigné
    // Suivant le signe de la quantité, la formule de calcul alimente le prix de base à partir :
    // - du prix consigné si la quantité est positive (on vend l'article consigné),
    // - du prix déconsigné si la quantité est négative (on reprend l'article consigné).
    if (parametreArticle != null && parametreArticle.getIdReferenceTarif() != null
        && parametreArticle.getIdReferenceTarif().isConsigne()) {
      if (quantiteUV == null || parametreTarif == null) {
        originePrixBaseCalcul = EnumOriginePrixBaseCalcul.CONSIGNATION;
      }
      else if (quantiteUV.compareTo(BigDecimal.ZERO) >= 0) {
        originePrixBaseCalcul = EnumOriginePrixBaseCalcul.CONSIGNATION;
        prixBaseCalculHT = parametreTarif.getPrixBaseHT(1);
      }
      else {
        originePrixBaseCalcul = EnumOriginePrixBaseCalcul.DECONSIGNATION;
        prixBaseCalculHT = parametreTarif.getPrixBaseHT(2);
      }
      
      if (modeTTC) {
        prixBaseCalculTTC = calculerPrixTTC(prixBaseCalculHT);
      }
      return;
    }
    
    // Récupérer le prix de base de la colonne 1 si le PS316 est actif
    // Tous les autres prix de base sont ignorés : prix de base de ligne de vente, du chantier, des CNVs, de la colonne tarif...
    // Le prix de base TTC est calculé à partir du prix de base HT de colonne 1
    if (parametreEtablissement != null && parametreEtablissement.isPrixPublicCommePrixBasePS316()) {
      originePrixBaseCalcul = EnumOriginePrixBaseCalcul.PRIX_PUBLIC_COLONNE_1;
      if (parametreTarif != null && parametreTarif.getPrixBaseHT(1) != null) {
        prixBaseCalculHT = parametreTarif.getPrixBaseHT(1);
        if (modeTTC) {
          prixBaseCalculTTC = calculerPrixTTC(prixBaseCalculHT);
        }
      }
      return;
    }
    
    // Utiliser le prix de base de la ligne de vente
    // La ligne de vente stocke à la fois les prix de base HT et TTC.
    // Un indicateur permet de connaître la provenance du prix de base (HT ou TTC) stocké dans la ligne de vente :
    // 1 = Prix article (en fait c'est le prix tarif donc par défaut si pas 2 et pas 3),
    // 2 = Prix condition de vente,
    // 3 = Prix saisi.
    // Si l'indicateur est égal à 3, cela signifie que le prix de base (HT ou TTC) a été explicitement saisi par l’utilisateur dans la
    // ligne de vente. Si l'indicateur est différent de 3, cela signifie que le prix de base HT est issu d’un calcul précédent et
    // qu’il faut ignorer la valeur.
    if (parametreLigneVente != null && parametreLigneVente.getProvenancePrixBase() == EnumProvenancePrixBase.SAISIE_LIGNE_VENTE) {
      if (modeTTC && parametreLigneVente.getPrixBaseTTC() != null) {
        originePrixBaseCalcul = EnumOriginePrixBaseCalcul.LIGNE_VENTE;
        prixBaseCalculTTC = ArrondiPrix.appliquer(parametreLigneVente.getPrixBaseTTC(), arrondiPrix);
        prixBaseCalculHT = calculerPrixHT(prixBaseCalculTTC);
        return;
      }
      else if (!modeTTC && parametreLigneVente.getPrixBaseHT() != null) {
        originePrixBaseCalcul = EnumOriginePrixBaseCalcul.LIGNE_VENTE;
        prixBaseCalculHT = ArrondiPrix.appliquer(parametreLigneVente.getPrixBaseHT(), arrondiPrix);
        return;
      }
    }
    
    // Utiliser le prix de base du chantier
    // Le prix TTC est déduit du prix HT car le chantier ne contient que le prix HT. En théorie, un chantier sur un client TTC
    // pourrait être stocké en TTC mais ce n'est pas le cas.
    if (parametreChantier != null && parametreChantier.getPrixBaseHT() != null) {
      originePrixBaseCalcul = EnumOriginePrixBaseCalcul.CHANTIER;
      prixBaseCalculHT = ArrondiPrix.appliquer(parametreChantier.getPrixBaseHT(), arrondiPrix);
      if (modeTTC) {
        prixBaseCalculTTC = calculerPrixTTC(prixBaseCalculHT);
      }
      return;
    }
    
    // Utiliser le prix de base issu de la condition de vente s'il est définit
    // Le prix TTC est déduit du prix HT car une condition de vente ne contient que le prix de base HT. En effet, une condition de vente
    // peut s'appliquer à tout type de calcul, HT ou TTC.
    if (parametreConditionVente != null && parametreConditionVente.getPrixBaseHT() != null) {
      originePrixBaseCalcul = EnumOriginePrixBaseCalcul.CONDITION_VENTE;
      prixBaseCalculHT = ArrondiPrix.appliquer(parametreConditionVente.getPrixBaseHT(), arrondiPrix);
      if (modeTTC) {
        prixBaseCalculTTC = calculerPrixTTC(prixBaseCalculHT);
      }
      return;
    }
    
    // Le prix de base est défini par le PUMP de l'article
    // Le prix TTC est déduit du prix HT car le PUMP est forcément HT.
    if (parametreConditionVente != null && parametreConditionVente.isPrixBaseHTAvecPump() && parametreArticle != null
        && parametreArticle.getPump() != null) {
      originePrixBaseCalcul = EnumOriginePrixBaseCalcul.PUMP_ARTICLE;
      prixBaseCalculHT = ArrondiPrix.appliquer(parametreArticle.getPump(), arrondiPrix);
      if (modeTTC) {
        prixBaseCalculTTC = calculerPrixTTC(prixBaseCalculHT);
      }
      return;
    }
    
    // Le prix de base est défini par le prix de revient
    // Le prix TTC est déduit du prix HT car le prix de revient est forcément HT.
    if (parametreConditionVente != null && parametreConditionVente.isPrixBaseHTAvecPrv() && parametreArticle != null
        && parametreArticle.getPrv() != null) {
      originePrixBaseCalcul = EnumOriginePrixBaseCalcul.PRV_ARTICLE;
      prixBaseCalculHT = ArrondiPrix.appliquer(retournerPrixMemeSiNull(parametreArticle.getPrv()), arrondiPrix);
      if (modeTTC) {
        prixBaseCalculTTC = calculerPrixTTC(prixBaseCalculHT);
      }
      return;
    }
    
    // Utiliser le prix de base de la colonne de tarif qui a été déterminée par calculerColonneTarif()
    // Récupérer les prix de bases HT et TTC de la colonne tarif tels quels.
    if (numeroColonneTarif != null && prixBaseColonneTarifHT != null) {
      originePrixBaseCalcul = EnumOriginePrixBaseCalcul.COLONNE_TARIF;
      prixBaseCalculHT = ArrondiPrix.appliquer(prixBaseColonneTarifHT, arrondiPrix);
      if (modeTTC) {
        prixBaseCalculTTC = ArrondiPrix.appliquer(prixBaseColonneTarifTTC, arrondiPrix);
      }
      return;
    }
    
    // Valeur vide par défaut
  }
  
  /**
   * Déterminer si le calcul est remisable.
   * 
   * Seul le changement de colonne est accepté : tout le reste est ignoré (les remises, les coefficients, ...).
   * 
   * Sauf exception via PS281 (à vérifier et améliorer) :
   * - Valeur vide : Par défaut, seules les conditions de ventes en prix net HT sont applicables sur les articles non remisables.
   * Les autres conditions de ventes (en valeur, en remise, coefficient ou prix de base) ne sont pas appliquées sur les articles non
   * remisables. A noter que les modifications de colonnes dans les conditions de vente sont toujours appliquées aux articles non
   * remisables.
   * - Valeur 1 : Les conditions de ventes clients en prix net HT sont applicables sur les articles non remisables. Une condition de
   * vente client est une condition de vente qui est rattachée directement à un identifiant client.
   * - Valeur 2 : Toutes les conditions de ventes sont applicables aux articles non remisables.
   * 
   * Note : ce chapitre est à investiguer plus profondément. Il faut tester le comportement RPG pour bien tour comprendre, définir
   * les règles de gestion et adapter le RPG et le Java. Pas sûr que résumé le résultat à un booléen remisable suffise.
   */
  private void determinerRemisable() {
    // Ne pas permettre les remises sur les articles consignés
    if (parametreArticle != null && parametreArticle.getIdReferenceTarif() != null
        && parametreArticle.getIdReferenceTarif().isConsigne()) {
      remisable = false;
      return;
    }
    
    // Déterminer si l'article est remisable (il est remisable par défaut)
    if (parametreArticle == null || parametreArticle.isRemisable()) {
      remisable = true;
      return;
    }
    
    // Sinon
    // L'article peut quand même être remisé dans certains cas (il y a en peut être d'autres mais le RPG est peu clair)
    if (parametreConditionVente != null && parametreEtablissement.getArticleNonRemisablePS281() != null) {
      // Contrôle exception 1 : si CNV en prix net et si PS281=' '
      if (parametreEtablissement.getArticleNonRemisablePS281().charValue() == ' ' && parametreConditionVente.isTypePrixNet()) {
        remisable = true;
        return;
      }
      // Contrôle exception 2 : si CNV quelconque et si PS281='1'
      if (parametreEtablissement.getArticleNonRemisablePS281().charValue() == '1' && parametreConditionVente.isTypePrixNet()
          && parametreConditionVente.isConditionVenteClient()) {
        remisable = true;
        return;
      }
      // Contrôle exception 2 : si CNV quelconque et si PS281='2'
      if (parametreEtablissement.getArticleNonRemisablePS281().charValue() == '2') {
        remisable = true;
        return;
      }
    }
    
    // En fin de compte l'article n'est pas remisable
    remisable = false;
  }
  
  /**
   * Déterminer la valeur de la remise/ajout en valeur utilisée pour le calcul.
   * 
   * C'est une remise si la valeur est négative et un ajout si la valeur est positive. Le seul endroit une où remise/ajout en valeur
   * peut être saisie c'est dans les conditions de ventes.
   * 
   * On sélectionne la valeur remise à appliquer suivant l’ordre de priorité suivant :
   * 1- Aucune remise en valeur si l'article n'est pas remisable
   * 2- La remise en valeur de la condition de vente.
   * 3- Aucune remise en valeur par défaut.
   */
  private void determinerRemiseEnValeur() {
    // Tester si l'article est non remisable
    if (!remisable) {
      return;
    }
    
    // Utiliser la remise/ajout en valeur de la condition de vente
    if (parametreConditionVente != null && parametreConditionVente.getRemiseEnValeur() != null) {
      remiseEnValeur = ArrondiPrix.appliquer(parametreConditionVente.getRemiseEnValeur(), arrondiPrix);
      return;
    }
    
    // Valeur vide par défaut
  }
  
  /**
   * Déterminer les taux de remises.
   * 
   * La remise peut être exprimée sous forme de 6 taux de remises (en %). Les valeurs peuvent être fournies par différentes sources.
   * Il faut déterminer quels taux de remises utiliser parmi ceux disponibles.
   * 
   * On sélectionne les taux de remises à appliquer suivant l’ordre de priorité suivant :
   * 1- Aucun taux de remise si un chantier est actif.
   * 2- Aucun taux de remise si l'article n'est pas remisable.
   * 3- Les taux de remises saisies manuellement dans la ligne de vente.
   * 4- Les taux de remises de la condition de vente.
   * 5- Les taux de remises de l’entête du document de vente. Ces taux de remises sont héritées du client.
   * 6- Les taux de remises du client.
   * 7- Sinon, pas de taux de remises.
   */
  private void determinerTauxRemise() {
    // Si prix chantier alors il n'y a pas de raison de rechercher la remise
    if (parametreChantier != null) {
      origineTauxRemise = EnumOrigineTauxRemise.CHANTIER_PRIORITAIRE;
      return;
    }
    
    // Tester si l'article est non remisable
    if (!remisable) {
      origineTauxRemise = EnumOrigineTauxRemise.ARTICLE_NON_REMISABLE;
      return;
    }
    
    // Utiliser les taux de remises de la ligne de vente si ils ont été saisis manuellement
    //
    // Même si les taux de remise ne sont pas renseignés (null ou égaux à zéro), on les utilise du moment qu'ils sont marqués comme
    // étant saisie par l'utilisateur. Cette situation arrive par exemple si un taux de remise est renseigné avec une valeur quelconque
    // par le logiciel (issus d'une condition de vente par exemple) et que l'utilisateur efface manuellement les taux de remises dans
    // la ligne de vente afin de ne pas les appliquer.
    //
    // Exception, on n'applique pas les remises de la ligne de vente lorsqu'un prix est hérité car cela signifie que nous sommes en
    // train de calculer une condition de vente cumulative.
    if (prixBaseHeriteHT == null && parametreLigneVente != null
        && parametreLigneVente.getProvenanceTauxRemise() == EnumProvenanceTauxRemise.SAISIE_LIGNE_VENTE) {
      origineTauxRemise = EnumOrigineTauxRemise.LIGNE_VENTE;
      tauxRemise1 = parametreLigneVente.getTauxRemise1();
      tauxRemise2 = parametreLigneVente.getTauxRemise2();
      tauxRemise3 = parametreLigneVente.getTauxRemise3();
      tauxRemise4 = parametreLigneVente.getTauxRemise4();
      tauxRemise5 = parametreLigneVente.getTauxRemise5();
      tauxRemise6 = parametreLigneVente.getTauxRemise6();
      return;
    }
    
    // Utiliser les taux de remises de la condition de vente
    if (parametreConditionVente != null && parametreConditionVente.isTauxRemisePresent()) {
      origineTauxRemise = EnumOrigineTauxRemise.CONDITION_VENTE;
      tauxRemise1 = parametreConditionVente.getTauxRemise1();
      tauxRemise2 = parametreConditionVente.getTauxRemise2();
      tauxRemise3 = parametreConditionVente.getTauxRemise3();
      tauxRemise4 = parametreConditionVente.getTauxRemise4();
      tauxRemise5 = parametreConditionVente.getTauxRemise5();
      tauxRemise6 = parametreConditionVente.getTauxRemise6();
      return;
    }
    
    // Utiliser les taux de remises du document de vente
    if (parametreDocumentVente != null && parametreDocumentVente.isTauxRemisePresent()) {
      origineTauxRemise = EnumOrigineTauxRemise.DOCUMENT_VENTE;
      tauxRemise1 = parametreDocumentVente.getTauxRemise1();
      tauxRemise2 = parametreDocumentVente.getTauxRemise2();
      tauxRemise3 = parametreDocumentVente.getTauxRemise3();
      tauxRemise4 = parametreDocumentVente.getTauxRemise4();
      tauxRemise5 = parametreDocumentVente.getTauxRemise5();
      tauxRemise6 = parametreDocumentVente.getTauxRemise6();
      return;
    }
    
    // Utiliser les taux de remises du client (3 valeurs pour le client)
    if (parametreClientFacture != null && parametreClientFacture.isTauxRemisePresent()) {
      origineTauxRemise = EnumOrigineTauxRemise.CLIENT;
      tauxRemise1 = parametreClientFacture.getTauxRemise1();
      tauxRemise2 = parametreClientFacture.getTauxRemise2();
      tauxRemise3 = parametreClientFacture.getTauxRemise3();
      return;
    }
    
    // Sinon pas de remises
  }
  
  /**
   * Déterminer le mode d'application des taux de remises.
   * 
   * Les taux de remises peuvent être appliquées en cascade ou en ajout. Par défaut, les remises sont appliquées en cascade
   * mais si l’option dédiée sur la ligne de vente est cochée alors les remises sont appliquées en ajout. Le mode de calcul en ajout
   * s’applique sur les remises issues des clients, des lignes de ventes et des documents de ventes. Par contre, les remises issues
   * des conditions de ventes sont toujours appliquées en cascade.
   * 
   * Le mode d’application des taux de remises peut être fournies par différentes sources. On sélectionne le mode d’application
   * des taux de remises à appliquer suivant l’ordre de priorité suivant :
   * 1- Aucun mode si l'article n'est pas remisable.
   * 2- Le mode d’application des taux de remises est en cascade si les taux sont issus de la condition de vente.
   * 3- Le mode d’application des taux de remises de la ligne de vente.
   * 4- Le mode d’application des taux de remises du document de vente.
   * 5- Le mode d’application des taux de remises est "En cascade" par défaut.
   */
  private void determinerModeTauxRemise() {
    // Tester si l'article est non remisable
    if (!remisable) {
      origineModeTauxRemise = EnumOrigineModeTauxRemise.ARTICLE_NON_REMISABLE;
      return;
    }
    
    // Utiliser mode d'application en cascade si les taux de remises sont issus de la condition de vente.
    if (origineTauxRemise == EnumOrigineTauxRemise.CONDITION_VENTE) {
      modeTauxRemise = EnumModeTauxRemise.CASCADE;
      origineModeTauxRemise = EnumOrigineModeTauxRemise.CONDITION_VENTE;
      return;
    }
    
    // Utiliser mode d'application des taux de remises de la ligne de vente
    if (parametreLigneVente != null && parametreLigneVente.getModeTauxRemise() != null) {
      modeTauxRemise = parametreLigneVente.getModeTauxRemise();
      origineModeTauxRemise = EnumOrigineModeTauxRemise.LIGNE_VENTE;
      return;
    }
    
    // Utiliser mode d'application des taux de remises du document de vente
    if (parametreLigneVente != null && parametreDocumentVente.getModeTauxRemise() != null) {
      modeTauxRemise = parametreDocumentVente.getModeTauxRemise();
      origineModeTauxRemise = EnumOrigineModeTauxRemise.DOCUMENT_VENTE;
      return;
    }
    
    // Utiliser mode d'application en cascade par défaut
    modeTauxRemise = EnumModeTauxRemise.CASCADE;
    origineModeTauxRemise = EnumOrigineModeTauxRemise.DEFAUT;
  }
  
  /**
   * Déterminer l'assiette d'application des remises.
   * 
   * Les taux de remises peuvent s’appliquer au prix unitaire ou au montant suivant le choix qui est effectué dans le document
   * de vente ou dans la ligne de vente. Par défaut, les taux de remises sont appliquées aux prix unitaires.
   * 
   * Déterminer l’assiette des taux de remises suivant l’ordre de priorité suivant :
   * 1- Aucune assiette si l'article n'est pas remisable.
   * 2- L’assiette des taux de remises de la ligne de vente.
   * 3- L’assiette des taux de remises de l’entête du document de vente.
   * 4- L’assiette est sur le prix unitaire par défaut.
   */
  private void determinerAssietteTauxRemise() {
    // Tester si l'article est non remisable
    if (!remisable) {
      origineAssietteTauxRemise = EnumOrigineAssietteTauxRemise.ARTICLE_NON_REMISABLE;
      return;
    }
    
    // Utiliser l'assiette des taux de remises de la ligne de vente
    if (parametreLigneVente != null && parametreLigneVente.getAssietteTauxRemise() != null) {
      origineAssietteTauxRemise = EnumOrigineAssietteTauxRemise.LIGNE_VENTE;
      assietteTauxRemise = parametreLigneVente.getAssietteTauxRemise();
      return;
    }
    
    // Utiliser l'assiette des taux de remises du document de vente
    if (parametreDocumentVente != null && parametreDocumentVente.getAssietteTauxRemise() != null) {
      origineAssietteTauxRemise = EnumOrigineAssietteTauxRemise.DOCUMENT_VENTE;
      assietteTauxRemise = parametreDocumentVente.getAssietteTauxRemise();
      return;
    }
    
    // Utiliser le prix unitaire comme assiette des taux de remises
    origineAssietteTauxRemise = EnumOrigineAssietteTauxRemise.DEFAUT;
    assietteTauxRemise = EnumAssietteTauxRemise.PRIX_UNITAIRE;
  }
  
  /**
   * Déterminer le coefficient.
   * 
   * La remise peut être exprimée sous forme d’un coefficient de remise. Le coefficient peut être inférieur à 1 (le prix diminue),
   * supérieur à 1 (ce n'est plus une remise car le prix augmente) ou égal à 1 (le prix ne change pas). Par défaut, le coefficient
   * vaut 1 pour ne pas fausser les calculs de remises.
   * 
   * Il est important de noter que parfois la valeur du coefficient stockée en table est négative, lors de l’application du coefficient il
   * faut utiliser la valeur absolue. Le coefficient est stocké en négatif pour indiquer que le coefficient ne doit pas apparaître lors
   * des impressions afin de ne pas montrer au client que potentiellement son prix n’est pas si avantageux que cela.
   * 
   * On sélectionne le coefficient de remise à appliquer suivant l’ordre de priorité suivant :
   * 1- Aucun coefficient si l'article n'est pas remisable.
   * 2- Le coefficient saisi manuellement dans la ligne de vente.
   * 3- Le coefficient de la condition de vente.
   * 4- Sinon, le coefficient vaut 1.
   */
  private void determinerCoefficient() {
    // Tester si l'article est non remisable
    if (!remisable) {
      origineCoefficient = EnumOrigineCoefficient.ARTICLE_NON_REMISABLE;
      return;
    }
    
    // Utiliser le coefficient de la ligne de vente s'il a été saisi manuellement et s'il est supérieur à zéro
    if (parametreLigneVente != null && parametreLigneVente.getProvenanceCoefficient() != null
        && parametreLigneVente.getProvenanceCoefficient() == EnumProvenanceCoefficient.SAISIE_LIGNE_VENTE
        && parametreLigneVente.getCoefficient() != null && parametreLigneVente.getCoefficient().compareTo(BigDecimal.ZERO) > 0) {
      origineCoefficient = EnumOrigineCoefficient.LIGNE_VENTE;
      coefficient = parametreLigneVente.getCoefficient();
      return;
    }
    
    // Utiliser le coefficient de la condition de vente s'il est supérieur à zéro
    if (parametreConditionVente != null && parametreConditionVente.getCoefficient() != null
        && parametreConditionVente.getCoefficient().compareTo(BigDecimal.ZERO) > 0) {
      origineCoefficient = EnumOrigineCoefficient.CONDITION_VENTE;
      coefficient = parametreConditionVente.getCoefficient();
      return;
    }
    
    // UItiliser la valeur 1 comme coefficient par défaut
    origineCoefficient = EnumOrigineCoefficient.DEFAUT;
    coefficient = BigDecimal.ONE;
  }
  
  /**
   * Appliquer les remises au prix de base afin de déterminer un prix de base remisé.
   * 
   * Le résultat est :
   * - un prix de base remisé si le résultat est inférieur au prix de base calcul (ajout en valeur ou coefficient > 1),
   * - un prix de base augmenté si le résultat est supérieur au prix de base calcul.
   * Néanmoins, cet attribut est nommé prix de base remisé car c'est le cas le plus courant et le plus intuitif. Même si ce terme est
   * inexacte lorsque le prix est augmenté mais cela reste un cas rare.
   * 
   * Il ya a trois étapes dans le calcul :
   * 1- Initialiser le prix de base remisé à partir du prix de base calculé HT ou TTC en fonction du mode.
   * 2- Appliquer la remise/ajout en valeur.
   * 3- Appliquer les taux de remises an ajout ou en cascade (si l'assiette de remise est le prix unitaire).
   * 4- Appliquer le coefficient (s'il est strictement supérieur à zéro).
   * 5- Contrôler la valeur du prix de base remisé et si cette valeur est négative alors le prix de base remisé est initialisé avec 0.
   */
  private void calculerPrixBaseRemise() {
    // Tester si l'article est non remisable
    if (!remisable) {
      prixBaseRemiseHT = prixBaseCalculHT;
      prixBaseRemiseTTC = prixBaseCalculTTC;
      return;
    }
    
    // Tester si le prix de base HT est null (aucune remise à appliquer dans ce cas)
    if (prixBaseCalculHT == null) {
      return;
    }
    
    // Déterminer le prix de base sur lequel sont appliqués les remises (prix de base HT ou TTC)
    BigDecimal prixBaseRemise;
    if (modeTTC) {
      prixBaseRemise = prixBaseCalculTTC;
    }
    else {
      prixBaseRemise = prixBaseCalculHT;
    }
    
    // Tester si le prix de base remisé est null (aucune remise à appliquer dans ce cas)
    if (prixBaseRemise == null) {
      return;
    }
    
    // Appliquer la remise en valeur
    // En mode TTC, la remise en valeur est retirée du TTC même si c'est une erreur car la remise en valeur, saisie dans une condition
    // de vente peut s'appliquer indifféremment à des cas HT ou TTC
    if (remiseEnValeur != null && remiseEnValeur.compareTo(BigDecimal.ZERO) != 0) {
      prixBaseRemise = ArrondiPrix.appliquer(prixBaseRemise.add(remiseEnValeur), arrondiPrix);
    }
    
    // Appliquer les taux de remises si l'assiette est le prix unitaire
    if (assietteTauxRemise == EnumAssietteTauxRemise.PRIX_UNITAIRE) {
      prixBaseRemise = appliquerTauxRemise(prixBaseRemise);
    }
    
    // Appliquer le coefficient s'il est strictement supérieur à zéro
    if (coefficient != null && coefficient.compareTo(BigDecimal.ZERO) > 0) {
      prixBaseRemise = prixBaseRemise.multiply(coefficient);
    }
    
    // Mettre 0.00 comme prix si le résultat du calcul donne uen valeur négative
    if (prixBaseRemise.compareTo(BigDecimal.ZERO) < 0) {
      prixBaseRemise = ArrondiPrix.appliquer(BigDecimal.ZERO, ArrondiPrix.DECIMALE_MAX);
    }
    
    // Renseigner les prix de bases remisés HT et TTC
    if (modeTTC) {
      prixBaseRemiseHT = calculerPrixHT(prixBaseRemise);
      prixBaseRemiseTTC = prixBaseRemise;
    }
    else {
      prixBaseRemiseHT = prixBaseRemise;
    }
  }
  
  /**
   * Déterminer si l'article est gratuit.
   * 
   * Attention ce n'est pas parce que l'article est gratuit que les prix de base, prix nets et prix nets calculés stockés dans la lignes
   * de ventes valent forcément 0 par contre le L1MHT vaut 0
   * 
   * Un article est considéré comme gratuit de 4 manières différentes :
   * 1- Le prix de base a été mis manuellement à 0 (L1PVB = 0 et le L1TB = 3).
   * 2- Un type de gratuit est renseigné (L1IN2 <> ‘ ‘) ce qui permet de faire le lien avec le paramètre TG. Les champs L1PVN, L1PVC,
   * L1MHT contiennent 0.
   * 3- Le prix net HT a été mis manuellement à 0 (L1PVN = 0 et le L1TN = 3) mais sans type de gratuit. Ce cas n'est en théorie pas
   * possible à saisir avec Série N. On le trouve néanmoins dans les bases de données clients (75 000 sur la base Tecnifibre par exemple)
   * et ce sont probablement des données importées.
   * 4- Par une condition de vente de type gratuit.
   */
  private void determinerGratuit() {
    // Contrôle si un type de gratuit a été sélectionné
    if (parametreLigneVente != null && parametreLigneVente.getIdTypeGratuit() != null
        && parametreLigneVente.getIdTypeGratuit().isGratuit()) {
      originePrixNet = EnumOriginePrixNet.LIGNE_VENTE_GRATUITE;
      gratuit = true;
      return;
    }
    
    // Contrôler si le prix net de la ligne de vente est égal à 0.00
    // Il faut regarder le prix net TTC en mode TTC et le prix net HT en mode HT car rien ne trouve que le prix de net HT est
    // correctement renseigné.
    if (parametreLigneVente != null && parametreLigneVente.getProvenancePrixNet() == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
      if (modeTTC && parametreLigneVente.getPrixNetSaisiTTC() != null
          && parametreLigneVente.getPrixNetSaisiTTC().compareTo(BigDecimal.ZERO) == 0) {
        originePrixNet = EnumOriginePrixNet.PRIX_NET_SAISI_GRATUIT;
        gratuit = true;
        return;
      }
      else if (!modeTTC && parametreLigneVente.getPrixNetSaisiHT() != null
          && parametreLigneVente.getPrixNetSaisiHT().compareTo(BigDecimal.ZERO) == 0) {
        originePrixNet = EnumOriginePrixNet.PRIX_NET_SAISI_GRATUIT;
        gratuit = true;
        return;
      }
    }
    
    // Détermination à partir des conditions de ventes
    if (parametreConditionVente != null && parametreConditionVente.isGratuit()) {
      originePrixNet = EnumOriginePrixNet.CONDITION_VENTE_GRATUITE;
      gratuit = true;
      return;
    }
  }
  
  /**
   * Calculer le prix net.
   *
   * Seul le cas où le calcul a été marqué comme gratuit permet d'avoir un prix net égal à 0.00. Par conséquent, tous les prix nets
   * inférieurs ou égal à zéro sont ignorés durant le processus de sélection. Le prix net doit être différent de zéro sauf dans les cas
   * de gratuité identifié.
   *
   * Le prix net peut être fourni par différentes sources. Le prix net est déterminé suivant l’ordre de priorité suivant :
   * 1- Le prix net est nul en cas de gratuité.
   * 2- Le prix net du chantier.
   * 3- Le prix net de la ligne de vente s’il a été saisi manuellement.
   * 4- Le prix net de la condition de vente.
   * 5- Le prix net issu de la formule prix de la condition de vente.
   * 6- Le prix net remisé.
   * 7- Le prix net donné par la colonne tarif (si le PS316 est actif).
   * 8- Le prix net est égal au prix de base calcul.
   * 9- Dernier contrôle : prix net est nul en cas de gratuité.
   * 10- Le prix net est invalide.
   */
  private void calculerPrixNet() {
    // Utiliser 0.00 si le cas a été détecté comme gratuit
    if (gratuit) {
      // L'origine du prix net HT a été déterminée lors dans la méthode determinerGratuit().
      prixNetHT = ArrondiPrix.appliquer(BigDecimal.ZERO, arrondiPrix);
      if (modeTTC) {
        prixNetTTC = ArrondiPrix.appliquer(BigDecimal.ZERO, arrondiPrix);
      }
      return;
    }
    
    // Utiliser le prix net HT du chantier
    // Le prix net doit être strictement supérieur à zéro car ce n'est pas un cas de gratuité identifié
    // Le prix TTC est déduit du prix HT car le chantier ne contient que le prix HT. En théorie, un chantier sur un client TTC
    // pourrait être stocké en TTC mais ce n'est pas le cas.
    if (parametreChantier != null && parametreChantier.getPrixNetHT() != null
        && parametreChantier.getPrixNetHT().compareTo(BigDecimal.ZERO) > 0) {
      originePrixNet = EnumOriginePrixNet.CHANTIER;
      prixNetHT = ArrondiPrix.appliquer(parametreChantier.getPrixNetHT(), arrondiPrix);
      if (modeTTC) {
        prixNetTTC = calculerPrixTTC(prixNetHT);
      }
      return;
    }
    
    // Utiliser le prix net (HT ou TTC) de la ligne de vente s'il a été manuellement saisi
    // La ligne de vente stocke à la fois les prix net HT et TTC.
    // Le prix net doit être strictement supérieur à zéro car ce n'est pas un cas de gratuité identifié.
    // Un indicateur permet de connaître la provenance du prix net stocké dans la ligne de vente. Si l'indicateur est égal à 3, cela
    // signifie que le prix net a été explicitement saisi par l’utilisateur dans la ligne de vente. Si l'indicateur est différent de 3,
    // cela signifie que le prix net est issu d’un calcul précédent et qu’il faut ignorer la valeur.
    if (parametreLigneVente != null && parametreLigneVente.getProvenancePrixNet() == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
      if (modeTTC && parametreLigneVente.getPrixNetSaisiTTC() != null
          && parametreLigneVente.getPrixNetSaisiTTC().compareTo(BigDecimal.ZERO) > 0) {
        originePrixNet = EnumOriginePrixNet.LIGNE_VENTE;
        prixNetTTC = ArrondiPrix.appliquer(parametreLigneVente.getPrixNetSaisiTTC(), arrondiPrix);
        prixNetHT = calculerPrixHT(prixNetTTC);
        return;
      }
      else if (!modeTTC && parametreLigneVente.getPrixNetSaisiHT() != null
          && parametreLigneVente.getPrixNetSaisiHT().compareTo(BigDecimal.ZERO) > 0) {
        originePrixNet = EnumOriginePrixNet.LIGNE_VENTE;
        prixNetHT = ArrondiPrix.appliquer(parametreLigneVente.getPrixNetSaisiHT(), arrondiPrix);
        return;
      }
    }
    
    // Utiliser le prix net HT de la condition de vente si l'article est remisable
    // Le prix TTC est déduit du prix HT car une condition de vente ne contient que le prix de base HT. En effet, une condition de vente
    // peut s'appliquer à tout type de calcul, HT ou TTC.
    // Le prix net doit être strictement supérieur à zéro car ce n'est pas un cas de gratuité identifié
    if (remisable && parametreConditionVente != null && parametreConditionVente.getPrixNetHT() != null
        && parametreConditionVente.getPrixNetHT().compareTo(BigDecimal.ZERO) > 0) {
      originePrixNet = EnumOriginePrixNet.PRIX_NET_HT_CONDITION_VENTE;
      prixNetHT = ArrondiPrix.appliquer(parametreConditionVente.getPrixNetHT(), arrondiPrix);
      if (modeTTC) {
        prixNetTTC = calculerPrixTTC(prixNetHT);
      }
      return;
    }
    
    // Utiliser la formule de prix de la condition de vente si l'article est remisable
    // La formule de prix s'applique au prix de base calcul HT pour les calculs HT et s'applique au prix de base calcul TTC pour les
    // calculs TTC. Dans le cas d'un calcul TTC, le prix net HT est déduit du prix net TTC et en appliquant le taux de TVA.
    // Le prix net obtenu doit être strictement supérieur à zéro car ce n'est pas un cas de gratuité identifié
    if (remisable && parametreConditionVente != null && parametreConditionVente.getIdFormulePrix() != null) {
      if (modeTTC) {
        BigDecimal prixTTC = calculerPrixNetParFormulePrix(prixBaseCalculTTC);
        if (prixTTC != null && prixTTC.compareTo(BigDecimal.ZERO) > 0) {
          originePrixNet = EnumOriginePrixNet.FORMULE_PRIX_CONDITION_VENTE;
          prixNetHT = calculerPrixHT(prixTTC);
          prixNetTTC = prixTTC;
          return;
        }
      }
      else {
        BigDecimal prixHT = calculerPrixNetParFormulePrix(prixBaseCalculHT);
        if (prixHT != null && prixHT.compareTo(BigDecimal.ZERO) > 0) {
          originePrixNet = EnumOriginePrixNet.FORMULE_PRIX_CONDITION_VENTE;
          prixNetHT = prixHT;
          return;
        }
      }
    }
    
    // Utiliser le prix de base remisé ou augmenté (HT et TTC)
    // Le prix de base remisé doit être strictement supérieur à zéro car ce n'est pas un cas de gratuité identifié.
    // Le prix de base remisé doit être différent du prix de base sinon c'est qu'il n'y a pas eu de remise.
    if (modeTTC && prixBaseRemiseTTC != null && prixBaseRemiseTTC.compareTo(BigDecimal.ZERO) > 0
        && prixBaseRemiseTTC.compareTo(prixBaseCalculTTC) != 0) {
      if (prixBaseCalculTTC != null && prixBaseRemiseTTC.compareTo(prixBaseCalculTTC) > 0) {
        originePrixNet = EnumOriginePrixNet.PRIX_BASE_AUGMENTE;
      }
      else {
        originePrixNet = EnumOriginePrixNet.PRIX_BASE_REMISE;
      }
      prixNetHT = ArrondiPrix.appliquer(prixBaseRemiseHT, arrondiPrix);
      prixNetTTC = ArrondiPrix.appliquer(prixBaseRemiseTTC, arrondiPrix);
      return;
      
    }
    else if (!modeTTC && prixBaseRemiseHT != null && prixBaseRemiseHT.compareTo(BigDecimal.ZERO) > 0
        && prixBaseRemiseHT.compareTo(prixBaseCalculHT) != 0) {
      if (prixBaseCalculHT != null && prixBaseRemiseHT.compareTo(prixBaseCalculHT) > 0) {
        originePrixNet = EnumOriginePrixNet.PRIX_BASE_AUGMENTE;
      }
      else {
        originePrixNet = EnumOriginePrixNet.PRIX_BASE_REMISE;
      }
      prixNetHT = ArrondiPrix.appliquer(prixBaseRemiseHT, arrondiPrix);
      return;
    }
    
    // Utiliser le prix de base de la colonne tarif si l'origine du prix de base est le prix public.
    // Cela se produit si le PS316 est actif et que le prix de base n'a pas été déterminé d'une autre façon.
    // Dans cas, la saisie d'une colonne tarif n'influe pas sur le prix de base mais permet de définir un prix net.
    // Le prix de base de la colonne tarif ne doit pas être nul car ce n'est pas un cas de gratuité autorisé.
    if (originePrixBaseCalcul == EnumOriginePrixBaseCalcul.PRIX_PUBLIC_COLONNE_1) {
      if (modeTTC && prixBaseColonneTarifTTC != null && prixBaseColonneTarifTTC.compareTo(BigDecimal.ZERO) > 0) {
        originePrixNet = EnumOriginePrixNet.PRIX_BASE_COLONNE_TARIF;
        prixNetHT = prixBaseColonneTarifHT;
        prixNetTTC = prixBaseColonneTarifTTC;
        return;
      }
      else if (!modeTTC && prixBaseColonneTarifHT != null && prixBaseColonneTarifHT.compareTo(BigDecimal.ZERO) > 0) {
        originePrixNet = EnumOriginePrixNet.PRIX_BASE_COLONNE_TARIF;
        prixNetHT = prixBaseColonneTarifHT;
        return;
      }
    }
    
    // Utiliser le prix de base calcul en dernier recours (HT et TTC)
    // Le prix net doit être strictement superieur à zéro car ce n'est pas un cas de gratuité identifié
    if (modeTTC && prixBaseCalculTTC != null && prixBaseCalculTTC.compareTo(BigDecimal.ZERO) > 0) {
      originePrixNet = EnumOriginePrixNet.PRIX_BASE;
      prixNetHT = ArrondiPrix.appliquer(prixBaseCalculHT, arrondiPrix);
      prixNetTTC = ArrondiPrix.appliquer(prixBaseCalculTTC, arrondiPrix);
      return;
    }
    else if (!modeTTC && prixBaseCalculHT != null && prixBaseCalculHT.compareTo(BigDecimal.ZERO) > 0) {
      originePrixNet = EnumOriginePrixNet.PRIX_BASE;
      prixNetHT = ArrondiPrix.appliquer(prixBaseCalculHT, arrondiPrix);
      return;
    }
    
    // Dernier contrôle de la gratuité : si le prix net vaut 0.00 et que le prix de base vaut 0.00 alors il s'agit d'un gratuit
    // Contrôler si le prix de base de la ligne de vente est égal à 0.00
    // Il faut regarder le prix de base TTC en mode TTC et le prix de base HT en mode HT car rien ne prouve que le prix de base HT est
    // correctement renseigné.
    if (parametreLigneVente != null && parametreLigneVente.getProvenancePrixBase() == EnumProvenancePrixBase.SAISIE_LIGNE_VENTE) {
      if (modeTTC && parametreLigneVente.getPrixBaseTTC() != null
          && parametreLigneVente.getPrixBaseTTC().compareTo(BigDecimal.ZERO) == 0) {
        originePrixNet = EnumOriginePrixNet.PRIX_BASE_SAISI_GRATUIT;
        gratuit = true;
        return;
      }
      else if (!modeTTC && parametreLigneVente.getPrixBaseHT() != null
          && parametreLigneVente.getPrixBaseHT().compareTo(BigDecimal.ZERO) == 0) {
        originePrixNet = EnumOriginePrixNet.PRIX_BASE_SAISI_GRATUIT;
        gratuit = true;
        return;
      }
    }
    
    // Prix net invalide car aucun prix net HT supérieur à zéro n'a pas être trouvé
    originePrixNet = EnumOriginePrixNet.INVALIDE;
  }
  
  /**
   * Ajuster le numéro de la colonne tarif en fonction du prix net.
   * 
   * Lorsque le P316 est actif, le prix de base est le prix de la colonne 1 (prix public). La colonne tarif ne sert donc pas à
   * définir le prix de base. Il y a alors deux cas en fonction de la présence de remise :
   * 1) Si aucune forme en remise n'est appliquée, la colonne tarif sert à définir le prix net. Le prix net est le prix de base de
   * la colonne tarif.
   * 2) Si des remises sont appliquées, c'est le prix de base remisé qui fait office de prix net. La colonne tarif ne sert à rien.
   * 
   * Dans le deuxième cas, il est possible que le prix net calculé via des remises soit identique au prix de base d'une des colonnes
   * de tarif. Dans ce cas, il est intéressant d'indiquer le numéro de la colonne tarif correspondante dans la ligne de vente, un
   * peu comme dans le cas 1. Si aucune colonne de tarif ne correspond au prix de base, la colonne de tarif est mise à null.
   */
  private void ajusterNumeroColonneTarif() {
    // Ne rien faire si le PS316 n'est pas actif
    if (parametreEtablissement == null || !parametreEtablissement.isPrixPublicCommePrixBasePS316()) {
      return;
    }
    
    // Ne pas modifier la colonne tarifaire si aucune remise n'a été appliquée, et donc c'est la colonne tarifaire qui a fixé le prix
    if (OutilCalculPrix.equals(modeTTC, prixNetHT, prixBaseCalculHT, prixNetTTC, prixBaseCalculTTC)) {
      return;
    }
    
    // Rechercher le prix net dans les colonnes tarifs
    if (parametreTarif != null) {
      ColonneTarif colonneTarif = parametreTarif.getColonneTarif(prixNetHT);
      if (colonneTarif != null) {
        numeroColonneTarif = colonneTarif.getNumero();
        origineColonneTarif = EnumOrigineColonneTarif.PRIX_NET;
        return;
      }
    }
    
    // Effacer la colonne tarif sinon
    numeroColonneTarif = null;
    origineColonneTarif = null;
  }
  
  /**
   * Calculer le taux de remise unitaire.
   * 
   * C'est la remise totale en pourcentage entre le prix de base et le prix net. C'est ce taux de remise qui est affiché
   * sur les documents de vente remis aux clients.
   * 
   * Le taux de remise est positif lorsque le prix a diminué (prix net < prix base).
   * Le taux de remise est négatif lorsque le prix a augmenté (prix net > prix base).
   * 
   * Si le PS316=true, le prix de base est identique est prix de base calcul. Dans ce cas, le taux de remise unitaire est
   * un résumé sous forme d'un taux unique et en tenant compte des arrondis de toutes les remises appliquées pour aboutir au
   * prix net (la remise en valeur, les 6 taux de remise, le coefficient, la formule de prix, ..).
   * 
   * Si le PS316=false, le prix de base est le prix de la colonne 1 (prix public). Il peut être différent du prix de base calcul.
   * Dans ce cas, le taux de remise calculé ici peut être singulièrement différent des remises effectivement appliquées. Il sera
   * souvent supérieur car le prix public est normalement le prix le plus cher.
   */
  private void calculerTauxRemiseUnitaire() {
    // Déterminer si le calcul s'effectue à partir des prix HT ou TTC
    BigDecimal prixBase;
    BigDecimal prixNet;
    if (modeTTC) {
      prixBase = prixBaseCalculTTC;
      prixNet = prixNetTTC;
    }
    else {
      prixBase = prixBaseCalculHT;
      prixNet = prixNetHT;
    }
    
    // Ne pas déterminer de remise si le prix net est null ou inférieur à zéro
    // Le calcul n'a pas pu aboutir pour une raison ou une autre ou a donné un résultat absurde (inférieur à 0), il ne faut pas
    // mettre de taux de remise.
    if (prixNet == null || prixNet.compareTo(BigDecimal.ZERO) < 0) {
      return;
    }
    
    // La remise est de 100% si le prix net est égal à 0
    // Y compris si le prix de base est null ou inférieur ou égal à 0
    if (prixNet.compareTo(BigDecimal.ZERO) == 0) {
      tauxRemiseUnitaire = BigPercentage.CENT;
      return;
    }
    
    // Ne pas déterminer de remise si le prix de base est null ou inférieur ou égal à 0
    if (prixBase == null || prixBase.compareTo(BigDecimal.ZERO) <= 0) {
      return;
    }
    
    // La remise est 0.00 si les prix de base HT et prix net HT sont identiques
    if (prixNet.compareTo(prixBase) == 0) {
      tauxRemiseUnitaire = BigPercentage.ZERO;
      return;
    }
    
    // Calculer le taux de remise
    // Le taux de remise est positif lorsque le prix a diminué (prix net < prix base).
    // La taux de remise est négatif lorsque le prix a augmenté (prix net > prix base).
    BigDecimal taux = prixBase.subtract(prixNet);
    taux = taux.divide(prixBase, MathContext.DECIMAL32);
    tauxRemiseUnitaire = new BigPercentage(taux, EnumFormatPourcentage.DECIMAL);
  }
  
  /**
   * Calculer le montant non remisé.
   * 
   * Le montant non remisé HT est calculé à partir du prix net HT et le montant non remisé TTC est calculé à partir du prix net TTC.
   * On applique la quantité vendue aux prix net HT et TTC en parallèle. Du coup, la valeur de la TVA sera à terme déterminée par la
   * différence entre les deux montants HT et TTC.
   * 
   * Une alternative aurait pu consister à déduire le montant HT à partir du montant TTC en divisant le montant TTC par le taux de TVA.
   * Ce calcul, tout à fait légal, peut cependant aboutir à ce que montant HT ne soit pas égal au prix net HT multiplié par la
   * quantité à cause des arrondis. Ce n'est pas cette méthode qui est utilisée.
   * 
   * Le montant peut être négatif dans le cas d'une reprise (quantité négative).
   */
  private void calculerMontantNonRemise() {
    // Pas de montant s'il n'y a pas de quantité
    if (getQuantiteUV() == null) {
      return;
    }
    
    // Calculer le montant non remisé HT
    if (prixNetHT != null) {
      montantNonRemiseHT = ArrondiPrix.appliquer(prixNetHT.multiply(getQuantiteUV()), arrondiPrix);
    }
    
    // Calculer le montant non remisé TTC
    if (modeTTC && prixNetTTC != null) {
      montantNonRemiseTTC = ArrondiPrix.appliquer(prixNetTTC.multiply(getQuantiteUV()), arrondiPrix);
    }
  }
  
  /**
   * Calculer le montant.
   * 
   * Le montant définitif est celui auquel on a appliqué les remises qui s'appliquent à la ligne. Les remises s'appliquent sur le
   * montant HT en mode HT ou sur le montant TTC en mode TTC. Le montant peut être négatif dans le cas d'une reprise.
   * 
   * En mode TTC, il y a deux techniques pour déterminer le montant HT remisé :
   * - Si le montant TTC remisé est identique au montant TTC non remisé, le montant HT remisé est simplement recopié du montant HT non
   * remisé.
   * - Si le montant TTC remisé est différent du montant TTC non remisé le montant HT remisé est déduit du montant TTC remisé en y
   * appliquant le taux de TVA.
   */
  private void calculerMontant() {
    // Tester si les remises ne s'appliquent pas sur le montant, auquel cas on recopie simplement les montants non remisés
    if (assietteTauxRemise != EnumAssietteTauxRemise.MONTANT_LIGNE) {
      montantHT = montantNonRemiseHT;
      montantTTC = montantNonRemiseTTC;
      return;
    }
    
    // Calculer le montant TTC remisé à partir du montant TTC non remisé
    // Si le montant TTC remisé est différent du montant TTC non remisé, le montant HT remisé est déduit du montant TTC remisé.
    if (modeTTC) {
      montantTTC = appliquerTauxRemise(montantNonRemiseTTC);
      if (montantTTC != null && montantNonRemiseTTC != null && montantTTC.compareTo(montantNonRemiseTTC) != 0) {
        montantHT = calculerPrixHT(montantTTC);
      }
      else {
        montantHT = montantNonRemiseHT;
      }
    }
    // Calculer le montant HT remisé à partir du montant HT non remisé
    else {
      montantHT = appliquerTauxRemise(montantNonRemiseHT);
    }
  }
  
  /**
   * Calculer la valeur de la remise effectuée sur le montant.
   * Cette valeur est HT en mode HT et TTC en mode TTC.
   */
  private void calculerValeurRemiseMontant() {
    // Calculer la valeur de la remise HT sur le montant HT
    if (montantNonRemiseHT != null && montantHT != null) {
      valeurRemiseMontantHT = montantNonRemiseHT.subtract(montantHT);
    }
    
    // Calculer la valeur de la remise TTC sur le montant TTC
    if (modeTTC && montantNonRemiseTTC != null && montantTTC != null) {
      valeurRemiseMontantTTC = montantNonRemiseTTC.subtract(montantTTC);
    }
  }
  
  /**
   * Calculer le montant de la TVA.
   * 
   * En mode TTC, on calcule le montant de TVA en faisant la différence entre le montant TTC et le montant HT.
   * En mode HT, on ne calcule pas le montant de TVA au niveau de la ligne car s'est effectué au niveau du document de vente.
   */
  private void calculerMontantTVA() {
    if (modeTTC && montantTTC != null && montantHT != null) {
      montantTVA = ArrondiPrix.appliquer(montantTTC.subtract(montantHT), arrondiPrix);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Calculer un prix HT à partir du prix TTC.
   * 
   * Le prix HT est déduit à partir du prix TTC et du taux de TVA. Si le taux de TVA est null, le prix HT est égal au prix TTC.
   * Si le prix TTC est null, le prix HT est null.
   * 
   * @param pPrixTTC Prix TTC.
   * @return Prix HT (null si le prix TTC est null).
   */
  private BigDecimal calculerPrixHT(BigDecimal pPrixTTC) {
    if (pPrixTTC != null && tauxTVA != null) {
      return ArrondiPrix.appliquer(pPrixTTC.divide(tauxTVA.getCoefficient(), MathContext.DECIMAL32), arrondiPrix);
    }
    else {
      return pPrixTTC;
    }
  }
  
  /**
   * Calculer un prix TTC à partir du prix HT.
   * 
   * Le prix TTC est déduit à partir du prix HT et du taux de TVA. Si le taux de TVA est null ou le prix HT est null, le prix TTC est
   * null. C'est une sécurité pour ne pas donner un prix HT en tant que prix TTC s'il y a eu un problème de chargement de TVA.
   * 
   * @param pPrixHT Prix HT.
   * @return Prix TTC (null si le mode de calcul est HT ou que le taux de TVA est inconnu).
   */
  private BigDecimal calculerPrixTTC(BigDecimal pPrixHT) {
    if (pPrixHT != null && tauxTVA != null) {
      return ArrondiPrix.appliquer(pPrixHT.multiply(tauxTVA.getCoefficient()), arrondiPrix);
    }
    else {
      return null;
    }
  }
  
  /**
   * Calculer un prix net à partir d'une formule de prix définit par une condition de vente.
   * 
   * La formule de prix s'applique au prix de base HT pour les calculs HT et s'applique au prix de base TTC pour les calculs TTC.
   * Cela donne respectivement un prix net HT et un prix net TTC. Dans le cas d'un calcul TTC, le prix net HT est déduit du prix net
   * TTC et en appliquant le taux de TVA.
   * 
   * @param pPrixBase Prix de base.
   * @return Prix net.
   */
  private BigDecimal calculerPrixNetParFormulePrix(BigDecimal pPrixBase) {
    // Tester les paramètres
    if (parametreConditionVente == null || parametreConditionVente.getIdFormulePrix() == null) {
      Trace.alerte("Le code de la formule de prix de la condition de ventes est invalide.");
      return null;
    }
    if (parametreEtablissement == null || parametreEtablissement.getListeFormulePrix() == null
        || parametreEtablissement.getListeFormulePrix().isEmpty()) {
      Trace.alerte("La liste des formules de prix (paramètre FP) est vide.");
      return null;
    }
    
    // Récupérer la formule prix à appliquer
    IdFormulePrix idFormulePrix = parametreConditionVente.getIdFormulePrix();
    FormulePrix formulePrix = parametreEtablissement.getListeFormulePrix().get(idFormulePrix);
    if (formulePrix == null) {
      Trace.alerte("La formule de prix avec l'identifiant " + idFormulePrix + " n'existe pas dans les paramètres FP.");
      return null;
    }
    
    // Calculer le prix net en appliquant la formule de prix
    try {
      return formulePrix.calculerPrix(pPrixBase);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'application d'une formule de prix");
      return null;
    }
  }
  
  /**
   * Retourner un prix quelque soit la valeur du paramètre.
   * Si sa valeur vaut null alors la méthode retourne 0.
   * 
   * @param pPrix
   * @return
   */
  private BigDecimal retournerPrixMemeSiNull(BigDecimal pPrix) {
    if (pPrix == null) {
      return BigDecimal.ZERO;
    }
    return pPrix;
  }
  
  /**
   * Calculer le montant remisé obtenu à partir des 6 taux de remise (appliqués en ajout ou en cascade).
   * 
   * Ce calcul peut s'appliquer à des prix HT, des prix TTC, des montants HT ou des montants TTC.
   * 
   * @param pMontant Montant à remiser.
   * @return Montant remisé.
   */
  private BigDecimal appliquerTauxRemise(BigDecimal pMontant) {
    BigDecimal montant = pMontant;
    if (montant == null) {
      return null;
    }
    
    // Appliquer les taux de remises en fonction du mode d'application
    switch (modeTauxRemise) {
      case AJOUT:
        // Calculer la valeur de la remise 1
        BigDecimal remise1 = BigDecimal.ZERO;
        if (tauxRemise1 != null) {
          remise1 =
              ArrondiPrix.appliquer(montant.multiply(BigDecimal.ONE.subtract(tauxRemise1.getCoefficient())), ArrondiPrix.DECIMALE_MAX);
        }
        
        // Calculer la valeur de la remise 2
        BigDecimal remise2 = BigDecimal.ZERO;
        if (tauxRemise2 != null) {
          remise2 =
              ArrondiPrix.appliquer(montant.multiply(BigDecimal.ONE.subtract(tauxRemise2.getCoefficient())), ArrondiPrix.DECIMALE_MAX);
        }
        
        // Calculer la valeur de la remise 3
        BigDecimal remise3 = BigDecimal.ZERO;
        if (tauxRemise3 != null) {
          remise3 =
              ArrondiPrix.appliquer(montant.multiply(BigDecimal.ONE.subtract(tauxRemise3.getCoefficient())), ArrondiPrix.DECIMALE_MAX);
        }
        
        // Calculer la valeur de la remise 4
        BigDecimal remise4 = BigDecimal.ZERO;
        if (tauxRemise4 != null) {
          remise4 =
              ArrondiPrix.appliquer(montant.multiply(BigDecimal.ONE.subtract(tauxRemise4.getCoefficient())), ArrondiPrix.DECIMALE_MAX);
        }
        
        // Calculer la valeur de la remise 5
        BigDecimal remise5 = BigDecimal.ZERO;
        if (tauxRemise5 != null) {
          remise5 =
              ArrondiPrix.appliquer(montant.multiply(BigDecimal.ONE.subtract(tauxRemise5.getCoefficient())), ArrondiPrix.DECIMALE_MAX);
        }
        
        // Calculer la valeur de la remise 6
        BigDecimal remise6 = BigDecimal.ZERO;
        if (tauxRemise6 != null) {
          remise6 =
              ArrondiPrix.appliquer(montant.multiply(BigDecimal.ONE.subtract(tauxRemise6.getCoefficient())), ArrondiPrix.DECIMALE_MAX);
        }
        
        // Calculer la valeur totale de la remise
        montant =
            ArrondiPrix.appliquer(montant.add(remise1).add(remise2).add(remise3).add(remise4).add(remise5).add(remise6), arrondiPrix);
        break;
      
      case CASCADE:
        // Calculer la valeur de la remise 1
        if (tauxRemise1 != null) {
          montant = ArrondiPrix.appliquer(montant.add(montant.multiply(BigDecimal.ONE.subtract(tauxRemise1.getCoefficient()))),
              ArrondiPrix.DECIMALE_MAX);
        }
        
        // Ajouter la valeur de la remise 2
        if (tauxRemise2 != null) {
          montant = ArrondiPrix.appliquer(montant.add(montant.multiply(BigDecimal.ONE.subtract(tauxRemise2.getCoefficient()))),
              ArrondiPrix.DECIMALE_MAX);
        }
        
        // Ajouter la valeur de la remise 3
        if (tauxRemise3 != null) {
          montant = ArrondiPrix.appliquer(montant.add(montant.multiply(BigDecimal.ONE.subtract(tauxRemise3.getCoefficient()))),
              ArrondiPrix.DECIMALE_MAX);
        }
        
        // Ajouter la valeur de la remise 4
        if (tauxRemise4 != null) {
          montant = ArrondiPrix.appliquer(montant.add(montant.multiply(BigDecimal.ONE.subtract(tauxRemise4.getCoefficient()))),
              ArrondiPrix.DECIMALE_MAX);
        }
        
        // Ajouter la valeur de la remise 5
        if (tauxRemise5 != null) {
          montant = ArrondiPrix.appliquer(montant.add(montant.multiply(BigDecimal.ONE.subtract(tauxRemise5.getCoefficient()))),
              ArrondiPrix.DECIMALE_MAX);
        }
        
        // Ajouter la valeur de la remise 6
        if (tauxRemise6 != null) {
          montant = ArrondiPrix.appliquer(montant.add(montant.multiply(BigDecimal.ONE.subtract(tauxRemise6.getCoefficient()))),
              ArrondiPrix.DECIMALE_MAX);
        }
        
        // Arrondir le résultalt
        montant = ArrondiPrix.appliquer(montant, arrondiPrix);
        break;
      
      default:
        // Rien à faire
        break;
    }
    
    return montant;
  }
  
  /**
   * Déterminer la provenance de la colonne tarif de la ligne de vente à partir du résultat du calcul.
   * @return Provenance de la colonne tarif.
   */
  public EnumProvenanceColonneTarif determinerProvenanceColonneTarif() {
    // Tester la présence de l'origine
    if (origineColonneTarif == null) {
      return EnumProvenanceColonneTarif.DEFAUT;
    }
    
    // Définir la provenance de la colonne de la ligne de vente en fonction de l'origine dans le résultat du calcul
    switch (origineColonneTarif) {
      case LIGNE_VENTE:
        return EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE;
      case CONDITION_VENTE:
      case CHANTIER:
      case HERITE_CNVS_PRECEDENTES:
        return EnumProvenanceColonneTarif.CONDITION_VENTE;
      case CLIENT:
      case DOCUMENT_VENTE:
        return EnumProvenanceColonneTarif.CLIENT;
      case PS105:
      case COLONNE_UN_PAR_DEFAUT:
      case PRIX_NET:
        return EnumProvenanceColonneTarif.DEFAUT;
    }
    
    // Obligatoire pour ne pas froisser le compilateur mais on ne passe jamais là car le switch gère tous les cas
    return EnumProvenanceColonneTarif.DEFAUT;
  }
  
  /**
   * Déterminer la provenance du prix de base de la ligne de vente à partir du résultat du calcul.
   * @return Provenance du prix de base.
   */
  public EnumProvenancePrixBase determinerProvenancePrixBase() {
    // Tester la présence de l'origine
    if (originePrixBaseCalcul == null) {
      return EnumProvenancePrixBase.DEFAUT;
    }
    
    // Définir la provenance du prix de base de la ligne de vente en fonction de l'origine dans le résultat du calcul
    switch (originePrixBaseCalcul) {
      case LIGNE_VENTE:
        return EnumProvenancePrixBase.SAISIE_LIGNE_VENTE;
      case CONDITION_VENTE:
      case CHANTIER:
      case HERITE:
        return EnumProvenancePrixBase.CONDITION_VENTE;
      case PRIX_PUBLIC_COLONNE_1:
      case COLONNE_TARIF:
      case CONSIGNATION:
      case DECONSIGNATION:
      case PRV_ARTICLE:
      case PUMP_ARTICLE:
        return EnumProvenancePrixBase.TARIF_ARTICLE;
      case INDEFINI:
        return EnumProvenancePrixBase.DEFAUT;
    }
    
    // Obligatoire pour ne pas froisser le compilateur mais on ne passe jamais là car le switch gère tous les cas
    return EnumProvenancePrixBase.DEFAUT;
  }
  
  /**
   * Déterminer la provenance du taux de remise après le calcul du prix à l'aide de son origine.
   * @return
   */
  public EnumProvenanceTauxRemise determinerProvenanceTauxRemise() {
    // Tester la présence de l'origine
    if (origineTauxRemise == null) {
      return EnumProvenanceTauxRemise.DEFAUT;
    }
    
    // Définir la provenance du taux de remise de la ligne de vente en fonction de l'origine dans le résultat du calcul
    switch (origineTauxRemise) {
      case CLIENT:
      case DOCUMENT_VENTE:
        return EnumProvenanceTauxRemise.CLIENT;
      case CONDITION_VENTE:
      case CHANTIER_PRIORITAIRE:
        return EnumProvenanceTauxRemise.CONDITION_VENTE;
      case LIGNE_VENTE:
        return EnumProvenanceTauxRemise.SAISIE_LIGNE_VENTE;
      default:
        return EnumProvenanceTauxRemise.DEFAUT;
    }
  }
  
  /**
   * Déterminer la provenance du coefficient après le calcul du prix à l'aide de son origine.
   * @return
   */
  public EnumProvenanceCoefficient determinerProvenanceCoefficient() {
    // Tester la présence de l'origine
    if (origineCoefficient == null) {
      return EnumProvenanceCoefficient.DEFAUT;
    }
    
    // Définir la provenance du coefficient de la ligne de vente en fonction de l'origine dans le résultat du calcul
    switch (origineCoefficient) {
      case CONDITION_VENTE:
        return EnumProvenanceCoefficient.CONDITION_VENTE;
      case LIGNE_VENTE:
        return EnumProvenanceCoefficient.SAISIE_LIGNE_VENTE;
      default:
        return EnumProvenanceCoefficient.DEFAUT;
    }
  }
  
  /**
   * Déterminer la provenance du prix net après le calcul du prix à l'aide de son origine.
   * @return
   */
  public EnumProvenancePrixNet determinerProvenancePrixNet() {
    // Tester la présence de l'origine
    if (originePrixNet == null) {
      return EnumProvenancePrixNet.DEFAUT;
    }
    
    // Définir la provenance du prix net de la ligne de vente en fonction de l'origine dans le résultat du calcul
    switch (originePrixNet) {
      case LIGNE_VENTE:
      case PRIX_NET_SAISI_GRATUIT:
        return EnumProvenancePrixNet.SAISIE_LIGNE_VENTE;
      case CONDITION_VENTE_GRATUITE:
      case FORMULE_PRIX_CONDITION_VENTE:
      case PRIX_NET_HT_CONDITION_VENTE:
      case PRIX_BASE_REMISE:
      case PRIX_BASE_AUGMENTE:
      case PRIX_BASE:
      case CHANTIER:
        return EnumProvenancePrixNet.CONDITION_VENTE;
      case INVALIDE:
      case PRIX_BASE_COLONNE_TARIF:
      case LIGNE_VENTE_GRATUITE:
      case PRIX_BASE_SAISI_GRATUIT:
        return EnumProvenancePrixNet.DEFAUT;
    }
    
    return EnumProvenancePrixNet.DEFAUT;
  }
  
  /**
   * Déterminer le type de la condition chantier si celle-ci existe.
   * Pour l'instant il s'agit d'une version simple qui détermine si la condition est quantitative ou non.
   * @return Le type.
   */
  public EnumTypeChantier getTypeConditionChantier() {
    // Contrôle s'il y a des paramètres chantier
    if (parametreChantier == null) {
      return null;
    }
    // Retourne le type de chantier de la condition (application : Article ou Quantitative)
    return parametreChantier.getTypeChantier();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs pour les paramètres
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner les paramètres établissement.
   * @return Paramètre établissement.
   */
  public ParametreEtablissement getParametreEtablissement() {
    return parametreEtablissement;
  }
  
  /**
   * Retourner les paramètres de l'article.
   * @return Paramètre article.
   */
  public ParametreArticle getParametreArticle() {
    return parametreArticle;
  }
  
  /**
   * Retourner les paramètres du tarif de l'article.
   * @return Paramètre tarif.
   */
  public ParametreTarif getParametreTarif() {
    return parametreTarif;
  }
  
  /**
   * Retourner les paramètres du client facturé.
   * @return Paramètre client facturé.
   */
  public ParametreClient getParametreClientFacture() {
    return parametreClientFacture;
  }
  
  /**
   * Retourner les paramètres du client livré.
   * @return Paramètre client livré.
   */
  public ParametreClient getParametreClientLivre() {
    return parametreClientLivre;
  }
  
  /**
   * Retourner les paramètres de la condition de ventes.
   * @return Paramètre condition de vente.
   */
  public ParametreConditionVente getParametreConditionVente() {
    return parametreConditionVente;
  }
  
  /**
   * Retourner les paramètres du chantier.
   * @return Paramètre chantier.
   */
  public ParametreChantier getParametreChantier() {
    return parametreChantier;
  }
  
  /**
   * Retourner les paramètres utilisé pour le calcul.
   * @return Paramètre ligne de vente.
   */
  public ParametreLigneVente getParametreLigneVente() {
    return parametreLigneVente;
  }
  
  /**
   * Retourner les paramètres du document de vente.
   * @return Paramètre document de vente.
   */
  public ParametreDocumentVente getParametreDocumentVente() {
    return parametreDocumentVente;
  }
  
  /**
   * Retourner si la formule est prise en compte dans le calcul du prix de vente.
   * @return true=Prise en compte, false=Non prise en compte.
   */
  public boolean isPrisEnComptePrixVente() {
    if (utilise != null && utilise.booleanValue()) {
      return true;
    }
    return false;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Type de la formule prix de vente.
   * Exemples : standard, chantier, CNV prioritaire, CNV normale, CNV quantitative, CNV cumulative...
   * @return Type de la formule prix de vente.
   */
  public EnumTypeFormulePrixVente getTypeFormulePrixVente() {
    return typeFormulePrixVente;
  }
  
  /**
   * Retourner le libellé définissant cette formule prix de vente.
   * @return Libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Indiquer si le calcul en mode HT ou TTC.
   * @return true=TTC, false=HT.
   */
  public boolean isModeTTC() {
    return modeTTC;
  }
  
  /**
   * Retourner l'origine du taux de TVA à appliquer.
   * @return Origine taux de TVA.
   */
  public EnumOrigineTauxTVA getOrigineTauxTVA() {
    return origineTauxTVA;
  }
  
  /**
   * Retourner le taux de TVA à appliquer.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA() {
    return tauxTVA;
  }
  
  /**
   * Retourner l'origine de l'unité de vente utilisée pour le calcul.
   * @return Origine de l'unité de vente.
   */
  public EnumOrigineUniteVente getOrigineUniteVente() {
    return origineUniteVente;
  }
  
  /**
   * Retourner le code de l'unité de vente utilisée pour le calcul.
   * @return Code unité de vente.
   */
  public String getCodeUniteVente() {
    return codeUniteVente;
  }
  
  /**
   * Modifier l'arrondi d eprix utilisé pour le calcul
   * @return Arrondi prix.
   */
  public ArrondiPrix getArrondiPrix() {
    return arrondiPrix;
  }
  
  /**
   * Retourner l'origine de la colonne tarif utilisée pour le calcul.
   * @return Origine de la colonne tarif.
   */
  public EnumOrigineColonneTarif getOrigineColonneTarif() {
    return origineColonneTarif;
  }
  
  /**
   * Retourner le numéro de la colonne tarif utilisée pour le calcul.
   * @return Colonne tarif.
   */
  public Integer getNumeroColonneTarif() {
    if (numeroColonneTarif == null) {
      return null;
    }
    return numeroColonneTarif;
  }
  
  /**
   * Retourner le prix de base HT issu de la colonne de tarif.
   * @return Prix de base HT.
   */
  public BigDecimal getPrixBaseColonneTarifHT() {
    return prixBaseColonneTarifHT;
  }
  
  /**
   * Retourner le prix de base TTC issu de la colonne de tarif.
   * @return Prix de base HT.
   */
  public BigDecimal getPrixBaseColonneTarifTTC() {
    return prixBaseColonneTarifTTC;
  }
  
  /**
   * Retourner le prix de base hérité HT issu d'une autre formule prix vente.
   * @return Prix de base HT.
   */
  public BigDecimal getPrixBaseHeriteHT() {
    return prixBaseHeriteHT;
  }
  
  /**
   * Modifier le prix de base hérité HT issu d'une autre formule prix vente.
   * Il faut également modifier le prix de base hérité TTC dans le cas d'un calcul TTC.
   * @param pPrixBaseHeriteHT Prix de base HT.
   */
  public void setPrixBaseHeriteHT(BigDecimal pPrixBaseHeriteHT) {
    prixBaseHeriteHT = pPrixBaseHeriteHT;
  }
  
  /**
   * Retourner le prix de base hérité TTC issu d'une autre formule prix vente.
   * @return Prix de base TTC.
   */
  public BigDecimal getPrixBaseHeriteTTC() {
    return prixBaseHeriteTTC;
  }
  
  /**
   * Modifier le prix de base hérité TTC issu d'une autre formule prix vente.
   * @param pPrixBaseHeriteTTC Prix de base TTC.
   */
  public void setPrixBaseHeriteTTC(BigDecimal pPrixBaseHeriteTTC) {
    prixBaseHeriteTTC = pPrixBaseHeriteTTC;
  }
  
  /**
   * Retourner l'origine du prix de base HT utilisé pour le calcul.
   * @return Origine du prix de nase HT.
   */
  public EnumOriginePrixBaseCalcul getOriginePrixBaseCalcul() {
    return originePrixBaseCalcul;
  }
  
  /**
   * Retourner le prix de base HT utilisé pour le calcul.
   * @return Prix de base HT.
   */
  public BigDecimal getPrixBaseCalculHT() {
    return prixBaseCalculHT;
  }
  
  /**
   * Retourner le prix de base TTC utilisé pour le calcul.
   * @return Prix de base TTC.
   */
  public BigDecimal getPrixBaseCalculTTC() {
    return prixBaseCalculTTC;
  }
  
  /**
   * Indiquer si le calcul est remisable.
   * @return true=article remisable, false=article non remisable.
   */
  public boolean isRemisable() {
    return remisable;
  }
  
  /**
   * Retourner la remise/ajout en valeur.
   * C'est une remise si la valeur est négative et un ajout si la valeur est positive.
   * @return Remise en valeur (négatif pour une remise, positif pour un ajout).
   */
  public BigDecimal getRemiseEnValeur() {
    return remiseEnValeur;
  }
  
  /**
   * Retourner l'origine des taux de remises.
   * @return Origine taux de remises.
   */
  public EnumOrigineTauxRemise getOrigineTauxRemise() {
    return origineTauxRemise;
  }
  
  /**
   * Modifier l'origine des taux de remises.
   * @param pOrigineTauxRemise Origine taux de remises.
   */
  public void setOrigineTauxRemise(EnumOrigineTauxRemise pOrigineTauxRemise) {
    origineTauxRemise = pOrigineTauxRemise;
  }
  
  /**
   * Retourner le taux de remise dont l'indice est mentionné en paramètre.
   * @param pIndex Indice du taux de remise à modifier (entre 1 et 6).
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise(int pIndex) {
    if (pIndex == 1) {
      return getTauxRemise1();
    }
    else if (pIndex == 2) {
      return getTauxRemise2();
    }
    else if (pIndex == 3) {
      return getTauxRemise3();
    }
    else if (pIndex == 4) {
      return getTauxRemise4();
    }
    else if (pIndex == 5) {
      return getTauxRemise5();
    }
    else if (pIndex == 6) {
      return getTauxRemise6();
    }
    return tauxRemise1;
  }
  
  /**
   * Retourner le taux de remise 1 utilisé pour le calcul.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise1() {
    return tauxRemise1;
  }
  
  /**
   * Retourner le taux de remise 1 utilisé pour le calcul.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise2() {
    return tauxRemise2;
  }
  
  /**
   * Retourner le taux de remise 3 utilisé pour le calcul.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise3() {
    return tauxRemise3;
  }
  
  /**
   * Retourner le taux de remise 4 utilisé pour le calcul.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise4() {
    return tauxRemise4;
  }
  
  /**
   * Retourner le taux de remise 5 utilisé pour le calcul.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise5() {
    return tauxRemise5;
  }
  
  /**
   * Retourner le taux de remise 6 utilisé pour le calcul.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise6() {
    return tauxRemise6;
  }
  
  /**
   * Retourner l'origine du mode d'application des taux de remises.
   * @return Origine du mode d'application des taux de remises.
   */
  public EnumOrigineModeTauxRemise getOrigineModeTauxRemise() {
    return origineModeTauxRemise;
  }
  
  /**
   * Retourner le mode d'application des taux de remises.
   * @return Mode d'application des taux de remises.
   */
  public EnumModeTauxRemise getModeTauxRemise() {
    return modeTauxRemise;
  }
  
  /**
   * Retourner l'origine de l'assiette d'application des taux de remises utilisée pour le calcul.
   * @return Origine de l'assiette d'application des taux de remises.
   */
  public EnumOrigineAssietteTauxRemise getOrigineAssietteTauxRemise() {
    return origineAssietteTauxRemise;
  }
  
  /**
   * Retourner l'assiette des taux de remises utilisée pour le calcul.
   * @return Assiette d'application des taux de remises.
   */
  public EnumAssietteTauxRemise getAssietteTauxRemise() {
    return assietteTauxRemise;
  }
  
  /**
   * Retourner l'origine du coefficient utilisé pour le calcul.
   * @return Origine du coefficient.
   */
  public EnumOrigineCoefficient getOrigineCoefficient() {
    return origineCoefficient;
  }
  
  /**
   * Retourner le coefficient utilisé pour le calcul.
   * @return Coefficient.
   */
  public BigDecimal getCoefficient() {
    return coefficient;
  }
  
  /**
   * Retourner le prix de base remisé HT.
   * @return Prix de base remisé HT.
   */
  public BigDecimal getPrixBaseRemiseHT() {
    return prixBaseRemiseHT;
  }
  
  /**
   * Retourner le prix de base remisé TTC.
   * @return Prix de base remisé TTC.
   */
  public BigDecimal getPrixBaseRemiseTTC() {
    return prixBaseRemiseTTC;
  }
  
  /**
   * Indiquer si une réduction a été appliquée au prix de base.
   * @return true=le prix de base a été remisé, false=le prix de base n'a pas été remisé.
   */
  public boolean isPrixBaseRemise() {
    if (modeTTC) {
      return prixBaseRemiseTTC != null && prixBaseCalculTTC != null && prixBaseRemiseTTC.compareTo(prixBaseCalculTTC) < 0;
    }
    else {
      return prixBaseRemiseHT != null && prixBaseCalculHT != null && prixBaseRemiseHT.compareTo(prixBaseCalculHT) < 0;
    }
  }
  
  /**
   * Indiquer si une augmentation a été appliquée au prix de base.
   * @return true=le prix de base a été augmenté, false=le prix de base n'a pas été augmenté.
   */
  public boolean isPrixBaseAugmente() {
    if (modeTTC) {
      return prixBaseRemiseTTC != null && prixBaseCalculTTC != null && prixBaseRemiseTTC.compareTo(prixBaseCalculTTC) > 0;
    }
    else {
      return prixBaseRemiseHT != null && prixBaseCalculHT != null && prixBaseRemiseHT.compareTo(prixBaseCalculHT) > 0;
    }
  }
  
  /**
   * Indiquer si le calcul de prix est gratuit .
   * @return true=gratuit, false=non gratuit.
   */
  public boolean isGratuit() {
    return gratuit;
  }
  
  /**
   * Retourner l'origine du prix net HT utilisé pour le calcul.
   * @return Origine du prix net HT.
   */
  public EnumOriginePrixNet getOriginePrixNet() {
    return originePrixNet;
  }
  
  /**
   * Retourner le prix net HT utilisé pour le calcul.
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetHT() {
    return prixNetHT;
  }
  
  /**
   * Retourner le prix net TTC.
   * @return Prix net TTC.
   */
  public BigDecimal getPrixNetTTC() {
    return prixNetTTC;
  }
  
  /**
   * Retourner le taux de remise unitaire.
   * @return Taux de remise unitaire.
   */
  public BigPercentage getTauxRemiseUnitaire() {
    return tauxRemiseUnitaire;
  }
  
  /**
   * Retourner la quantité en UV.
   * @return Quantité en UV.
   */
  public BigDecimal getQuantiteUV() {
    return quantiteUV;
  }
  
  /**
   * Retourner le montant non remisé HT.
   * @return Montant non remisé HT.
   */
  public BigDecimal getMontantNonRemiseHT() {
    return montantNonRemiseHT;
  }
  
  /**
   * Retourner le montant non remisé TTC.
   * @return Montant non remisé TTC.
   */
  public BigDecimal getMontantNonRemiseTTC() {
    return montantNonRemiseTTC;
  }
  
  /**
   * Retourner la valeur de la remise effectuée sur le montant HT.
   * @return Valeur remise HT.
   */
  public BigDecimal getValeurRemiseMontantHT() {
    return valeurRemiseMontantHT;
  }
  
  /**
   * Retourner la valeur de la remise effectuée sur le montant TTC.
   * @return Valeur remise TTC.
   */
  public BigDecimal getValeurRemiseMontantTTC() {
    return valeurRemiseMontantTTC;
  }
  
  /**
   * Retourner le montant HT.
   * @return Montant HT.
   */
  public BigDecimal getMontantHT() {
    return montantHT;
  }
  
  /**
   * Retourner le montant de la TVA.
   * 
   * @return
   */
  public BigDecimal getMontantTVA() {
    return montantTVA;
  }
  
  /**
   * Retourner le montant TTC.
   * 
   * @return
   */
  public BigDecimal getMontantTTC() {
    return montantTTC;
  }
  
  /**
   * Retourner le commentaire associé à cette formule prix de vente.
   * Le commentaire peut permettre d'expliquer pourquoi le calcul a été retenu ou au contraire non retenu.
   * @return Commentaire.
   */
  public String getCommentaire() {
    return commentaire;
  }
  
  /**
   * Modifier le commentaire associé à cette formule prix de vente.
   * @param pCommentaire Commentaire.
   */
  public void setCommentaire(String pCommentaire) {
    commentaire = pCommentaire;
  }
  
  /**
   * Modifier si la formule est utilisée dans le calcul du prix de vente final.
   * @param pUtilise true=Utilisée, false ou null=Non utilisée
   */
  public void setUtilise(Boolean pUtilise) {
    utilise = pUtilise;
  }
  
  /**
   * Retourner si le mode négoce est actif.
   * @return
   */
  public Boolean getModeNegoce() {
    if (parametreEtablissement == null) {
      return null;
    }
    return parametreEtablissement.isModeNegocePS287();
  }
  
}
