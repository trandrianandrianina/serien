/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.io.Serializable;
import java.math.BigDecimal;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;

public class Negociation implements Serializable {
  
  // public static final String TYPE_DE_LIGNE_ = "D";
  public static final String CODE_CONDITION_NORMALE = "";
  public static final String CODE_CONDITION_PAR_QUANTITE = "Q";
  public static final String CODE_CONDITION_DEROGATION = "D";
  public static final String CODE_CONDITION_FLASH = "F";
  public static final String CODE_CONDITION_NEGOCIE = "N";
  public static final String CODE_CONDITION_CHANTIER = "H";
  public static final String CODE_CONDITION_AFFAIRE = "E";
  public static final String CODE_CONDITION_CLIENT = "C";
  public static final String CODE_CONDITION_ZONE_GEOGRAPHIQUE = "g";
  public static final String CODE_CONDITION_PAYS = "p";
  public static final String CODE_CONDITION_CATEGORIE_CLIENT = "c";
  public static final String CODE_CONDITION_CANAL = "k";
  public static final String TYPE_RATTACHEMENT_ARTICLE = "A";
  public static final String TYPE_RATTACHEMENT_TARIF = "T";
  public static final String TYPE_RATTACHEMENT_GROUPE = "G";
  public static final String TYPE_RATTACHEMENT_FAMILLE = "F";
  public static final String TYPE_RATTACHEMENT_SOUS_FAMILLE = "S";
  public static final String TYPE_RATTACHEMENT_EMBOITAGE_OU_GENERAL = "*";
  public static final String TYPE_RATTACHEMENT_ENSEMBLE_ARTICLES = "R";
  public static final String TYPE_RATTACHEMENT_NUMERO_LIGNE_OU_RANG = "L";
  public static final String TYPE_CONDITION_PRIX_NET = "N";
  public static final String TYPE_CONDITION_PRIX_DE_BASE = "B";
  public static final String TYPE_CONDITION_REMISE_EN_POURCENTAGE = "R";
  public static final String TYPE_CONDITION_COEFFICIENT = "K";
  public static final String TYPE_CONDITION_FORMULE_PRIX = "F";
  public static final String TYPE_CONDITION_AJOUT_EN_VALEUR = "+";
  public static final String TYPE_CONDITION_REMISE_EN_VALEUR = "-";
  public static final String TYPE_CONDITION_AVOIR_SEPARE = "A";
  public static final String TYPE_CONDITION_ARTICLE_INTERDIT = "X";
  
  /*
   *  Variables fichiers
   */
  private int topSysteme = 0; // Top système
  private IdEtablissement idEtablissement = null; // Code établissement
  private EnumCodeEnteteDocumentVente codeEnteteDocumentVente = null; // Code ERL "D","E",X","9"
  private int numeroBon = 0; // numéro du bon
  private int suffixeBon = 0; // Suffixe du bon
  private int numeroLigne = 0; // Numéro de ligne ou 0
  private String optionTraitement = ""; // Option de traitement
  private IdArticle idArticle = null; // Code article
  private int dateCreation = 0; // Date de création
  private int dateModification = 0; // Date de modification
  private int dateTraitement = 0; // Date de traitement
  private String categorieCondition = ""; // Catégorie de la condition de vente
  private String codeCondition = ""; // Code de la condition de vente
  // = NORMALE
  // Q = PAR QTÉ
  // D = Dérogation
  // F = FLASH
  // N = NEGOCIE
  // H = CHANTIER
  // E = AFFAIRE
  // C = CLIENT
  // g = Zone géo
  // p = PAYS
  // z = ???
  // c = CATEGORIE CLIENT
  // k = CANAL
  private String typeRattachement = ""; // Type de rattachement
  // A = ARTICLE
  // T = TARIF
  // G = GROUPE
  // F = FAMILLE
  // S = SOUS FAMILLE
  // * = EMBOITAGE OU GENERAL
  // R = ENSEMBLE ARTICLES
  // L = N° LIGNE OU RANG
  private String codeRattachement = ""; // Code de la condition de vente
  private String quantiteMinimale = ""; // quantité minimale
  private Character typeCondition = null; // Type de la condition de vente
  // N = PRIX NET
  // B = PRIX DE BASE
  // R = REMISE EN %
  // K = COEFFICIENT
  // F = FORMULE PRIX (PARAM. FP)
  // + = AJOUT EN VALEUR
  // - = REMISE EN VALEUR
  // A = AVOIR SÉPARÉ
  // X = ARTICLE INTERDIT
  private BigDecimal valeur = null; // Valeur en euros
  private BigDecimal remise1 = null; // Remise 1
  private BigDecimal remise2 = null; // Remise 2
  private BigDecimal remise3 = null; // Remise 3
  private BigDecimal remise4 = null; // Remise 4
  private BigDecimal remise5 = null; // Remise 5
  private BigDecimal remise6 = null; // Remise 6
  private BigDecimal coefficient = null; // Coefficient < ou > à 1
  private int dateDebut = 0; // Date de début de validité
  private int dateFin = 0; // Date de fin de validité
  private String formulePrix = ""; // Formule de prix
  private String codeDevise = ""; // Code devise
  private String typeConditionDeux = ""; // Type de la condition de vente (deuxième)
  // N = PRIX NET
  // B = PRIX DE BASE
  // R = REMISE EN %
  // K = COEFFICIENT
  // F = FORMULE PRIX (PARAM. FP)
  // + = AJOUT EN VALEUR
  // - = REMISE EN VALEUR
  // A = AVOIR SÉPARÉ
  // X = ARTICLE INTERDIT
  private BigDecimal valeurDeux = null; // Valeur en euros (deuxième)
  private BigDecimal remise1Deux = null; // Remise 1 (deuxième)
  private BigDecimal remise2Deux = null; // Remise 2 (deuxième)
  private BigDecimal remise3Deux = null; // Remise 3 (deuxième)
  private BigDecimal remise4Deux = null; // Remise 4 (deuxième)
  private BigDecimal remise5Deux = null; // Remise 5 (deuxième)
  private BigDecimal remise6Deux = null; // Remise 6 (deuxième)
  private BigDecimal coefficientDeux = null; // Coefficient < ou > à 1 (deuxième)
  private int dateDebutDeux = 0; // Date de début de validité (deuxième)
  private int dateFinDeux = 0; // Date de fin de validité (deuxième)
  private String formulePrixDeux = ""; // Formule de prix (deuxième)
  
  // -- Accesseurs
  
  public int getTopSysteme() {
    return topSysteme;
  }
  
  public void setTopSysteme(int topSysteme) {
    this.topSysteme = topSysteme;
  }
  
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  public EnumCodeEnteteDocumentVente getCodeEnteteDocumentVente() {
    return codeEnteteDocumentVente;
  }
  
  public void setCodeEnteteDocumentVente(EnumCodeEnteteDocumentVente codeERL) {
    this.codeEnteteDocumentVente = codeERL;
  }
  
  public int getNumeroBon() {
    return numeroBon;
  }
  
  public void setNumeroBon(int numeroBon) {
    this.numeroBon = numeroBon;
  }
  
  public int getSuffixeBon() {
    return suffixeBon;
  }
  
  public void setSuffixeBon(int suffixeBon) {
    this.suffixeBon = suffixeBon;
  }
  
  public int getNumeroLigne() {
    return numeroLigne;
  }
  
  public void setNumeroLigne(int numeroLigne) {
    this.numeroLigne = numeroLigne;
  }
  
  public String getOptionTraitement() {
    return optionTraitement;
  }
  
  public void setOptionTraitement(String optionTraitement) {
    this.optionTraitement = optionTraitement;
  }
  
  public IdArticle getCodeArticle() {
    return idArticle;
  }
  
  public void setCodeArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  public int getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(int dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public int getDateModification() {
    return dateModification;
  }
  
  public void setDateModification(int dateModification) {
    this.dateModification = dateModification;
  }
  
  public int getDateTraitement() {
    return dateTraitement;
  }
  
  public void setDateTraitement(int dateTraitement) {
    this.dateTraitement = dateTraitement;
  }
  
  public String getCategorieCondition() {
    return categorieCondition;
  }
  
  public void setCategorieCondition(String categorieCondition) {
    this.categorieCondition = categorieCondition;
  }
  
  public String getCodeCondition() {
    return codeCondition;
  }
  
  public void setCodeCondition(String codeCondition) {
    this.codeCondition = codeCondition;
  }
  
  public String getTypeRattachement() {
    return typeRattachement;
  }
  
  public void setTypeRattachement(String typeRattachement) {
    this.typeRattachement = typeRattachement;
  }
  
  public String getCodeRattachement() {
    return codeRattachement;
  }
  
  public void setCodeRattachement(String codeRattachement) {
    this.codeRattachement = codeRattachement;
  }
  
  public String getQuantiteMinimale() {
    return quantiteMinimale;
  }
  
  public void setQuantiteMinimale(String quantiteMinimale) {
    this.quantiteMinimale = quantiteMinimale;
  }
  
  public Character getTypeCondition() {
    return typeCondition;
  }
  
  public void setTypeCondition(Character typeCondition) {
    this.typeCondition = typeCondition;
  }
  
  public BigDecimal getValeur() {
    return valeur;
  }
  
  public void setValeur(BigDecimal valeur) {
    this.valeur = valeur;
  }
  
  public BigDecimal getRemise1() {
    return remise1;
  }
  
  public void setRemise1(BigDecimal remise1) {
    this.remise1 = remise1;
  }
  
  public BigDecimal getRemise2() {
    return remise2;
  }
  
  public void setRemise2(BigDecimal remise2) {
    this.remise2 = remise2;
  }
  
  public BigDecimal getRemise3() {
    return remise3;
  }
  
  public void setRemise3(BigDecimal remise3) {
    this.remise3 = remise3;
  }
  
  public BigDecimal getRemise4() {
    return remise4;
  }
  
  public void setRemise4(BigDecimal remise4) {
    this.remise4 = remise4;
  }
  
  public BigDecimal getRemise5() {
    return remise5;
  }
  
  public void setRemise5(BigDecimal remise5) {
    this.remise5 = remise5;
  }
  
  public BigDecimal getRemise6() {
    return remise6;
  }
  
  public void setRemise6(BigDecimal remise6) {
    this.remise6 = remise6;
  }
  
  public BigDecimal getCoefficient() {
    return coefficient;
  }
  
  public void setCoefficient(BigDecimal coefficient) {
    this.coefficient = coefficient;
  }
  
  public int getDateDebut() {
    return dateDebut;
  }
  
  public void setDateDebut(int dateDebut) {
    this.dateDebut = dateDebut;
  }
  
  public int getDateFin() {
    return dateFin;
  }
  
  public void setDateFin(int dateFin) {
    this.dateFin = dateFin;
  }
  
  public String getFormulePrix() {
    return formulePrix;
  }
  
  public void setFormulePrix(String formulePrix) {
    this.formulePrix = formulePrix;
  }
  
  public String getCodeDevise() {
    return codeDevise;
  }
  
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  public String getTypeConditionDeux() {
    return typeConditionDeux;
  }
  
  public void setTypeConditionDeux(String typeConditionDeux) {
    this.typeConditionDeux = typeConditionDeux;
  }
  
  public BigDecimal getValeurDeux() {
    return valeurDeux;
  }
  
  public void setValeurDeux(BigDecimal valeurDeux) {
    this.valeurDeux = valeurDeux;
  }
  
  public BigDecimal getRemise1Deux() {
    return remise1Deux;
  }
  
  public void setRemise1Deux(BigDecimal remise1Deux) {
    this.remise1Deux = remise1Deux;
  }
  
  public BigDecimal getRemise2Deux() {
    return remise2Deux;
  }
  
  public void setRemise2Deux(BigDecimal remise2Deux) {
    this.remise2Deux = remise2Deux;
  }
  
  public BigDecimal getRemise3Deux() {
    return remise3Deux;
  }
  
  public void setRemise3Deux(BigDecimal remise3Deux) {
    this.remise3Deux = remise3Deux;
  }
  
  public BigDecimal getRemise4Deux() {
    return remise4Deux;
  }
  
  public void setRemise4Deux(BigDecimal remise4Deux) {
    this.remise4Deux = remise4Deux;
  }
  
  public BigDecimal getRemise5Deux() {
    return remise5Deux;
  }
  
  public void setRemise5Deux(BigDecimal remise5Deux) {
    this.remise5Deux = remise5Deux;
  }
  
  public BigDecimal getRemise6Deux() {
    return remise6Deux;
  }
  
  public void setRemise6Deux(BigDecimal remise6Deux) {
    this.remise6Deux = remise6Deux;
  }
  
  public BigDecimal getCoefficientDeux() {
    return coefficientDeux;
  }
  
  public void setCoefficientDeux(BigDecimal coefficientDeux) {
    this.coefficientDeux = coefficientDeux;
  }
  
  public int getDateDebutDeux() {
    return dateDebutDeux;
  }
  
  public void setDateDebutDeux(int dateDebutDeux) {
    this.dateDebutDeux = dateDebutDeux;
  }
  
  public int getDateFinDeux() {
    return dateFinDeux;
  }
  
  public void setDateFinDeux(int dateFinDeux) {
    this.dateFinDeux = dateFinDeux;
  }
  
  public String getFormulePrixDeux() {
    return formulePrixDeux;
  }
  
  public void setFormulePrixDeux(String formulePrixDeux) {
    this.formulePrixDeux = formulePrixDeux;
  }
  
}
