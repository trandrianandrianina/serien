/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.devise;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste des devise
 */
public class ListeDevise extends ListeClasseMetier<IdDevise, Devise, ListeDevise> {
  
  /**
   * Charge la liste des devise suivant un IdDevise
   */
  @Override
  public ListeDevise charger(IdSession pIdSession, List<IdDevise> pListeId) {
    return null;
  }
  
  /**
   * Charge la liste des divise suivant l'établissement
   */
  @Override
  public ListeDevise charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CriteresRechercheDevise criteres = new CriteresRechercheDevise();
    criteres.setTypeRecherche(CriteresRechercheDevise.RECHERCHE_DEVISE);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeDevise(pIdSession, criteres);
  }
  
}
