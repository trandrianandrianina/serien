/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.achat.document.EnumCodeEtatDocument;
import ri.serien.libcommun.gescom.achat.document.EnumTypeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.prix.ParametrePrixAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.groupe.IdGroupe;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.IdSousFamille;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.Unite;
import ri.serien.libcommun.gescom.vente.document.PrixFlash;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;

/**
 * Classe principale d'un article.
 */
public class Article extends ArticleBase {
  // Constantes
  public static final String TYPE_UNITE_DIMENSION_M = "";
  public static final String TYPE_UNITE_DIMENSION_m = "1";
  public static final String TYPE_UNITE_DIMENSION_CM = "2";
  public static final String TYPE_UNITE_DIMENSION_MM = "3";
  private static final String CODE_ARTICLE_REPRISE = "*RP";
  
  // L'article a été mis sur le Web et visible
  public static final String SUR_LE_WEB = " ";
  // L'article n'a jamais été mis sur le Web et donc invisible
  public static final String JAMAIS_VU_SUR_LE_WEB = "1";
  // L'article a déjà été mis sur le Web mais doit être désormais invisible
  public static final String EXCLU_WEB = "2";
  
  // Libellés
  public static final String LIBELLE_HORS_GAMME = "Hors gamme";
  
  // Nombre de décimales d'une UCV scindable
  public static final int NOMBRE_DECIMALE_UCV_SCINDABLE = 3;
  // Classe de rotation
  private static final Character CLASSE_ROTATION_HORS_GAMME = 'R';
  
  // Extentions
  private ExtensionArticle extension = new ExtensionArticle();
  
  // Variables fichiers
  private Boolean articleCharge = Boolean.FALSE;
  private Integer topSysteme = null; // Top système
  private Date dateCreation = null; // Date de création
  private Date dateModification = null; // Date de modification
  private Date dateTraitement = null; // Date de traitement
  private String topNePlusUtiliser = null;// Top ne plus utiliser
  private String clefClassement1 = null; // Première clef de classement de l'article (mot clée 1)
  private String clefClassement2 = null; // Deuxième clef de classement de l'article (mot clée 2)
  private String articleRemplacementStk = null; // Article de remplacement Stk
  private String gencode = null; // Gencode
  private String nomenclatureProduit = null; // Nomenclature produit(CEE)
  private Integer articleNomenclature = null; // Article nomenclature
  private String regroupementStatistique = null; // Regroupement statistique
  private String critereAnalyse1 = null; // Critères d'analyse 1
  private String critereAnalyse2 = null; // Critères d'analyse 2
  private String critereAnalyse3 = null; // Critères d'analyse 3
  private String critereAnalyse4 = null; // Critères d'analyse 4
  private String critereAnalyse5 = null; // Critères d'analyse 5
  private String topDesignation1 = null; // Top désignation 1
  private String topDesignation2 = null; // Top désignation 2
  private String topDesignation3 = null; // Top désignation 3
  private String topDesignation4 = null; // Top désignation 4
  private String codeTarif = null; // Code tarif de l'article
  private String rattachementCNV = null; // Code de rattachement condition de vente
  private Integer dateDerniereVente = null; // Date de la dernière vente de cet article
  private Integer codeTVA = null; // Code TVA de l'article
  private Integer codeTaxeParafiscale = null; // Code taxe parafiscale de l'article
  private Integer nonStocke = null; // Top article non géré en stock
  private Integer nombreDecimaleUS = null; // Top décimalisation pour unité de stockage
  private Integer nombreDecimaleUV = null; // Top décimalisation pour unité de vente
  private Integer articleUtiliseEnSaisie = null; // Article utilisé en saisie
  private Integer topNonCommissionne = null; // Top article sans commission
  private Integer codeArrondi = null; // Code arrondi
  private IdUnite idUV = null; // Code de l'unité de vente
  private BigDecimal coefficientUSParUV = null; // Coefficient US/UV
  private BigDecimal coefficientMarge = null; // Coefficient de marge
  private Integer pourcentageMargeMini = null; // Pourcentage de marge minimum
  private Integer pourcentageRemiseMaxi = null; // Pourcentage maximum de remise
  private IdFournisseur idFournisseur = null;
  private BigDecimal dernierPrixAchat = null; // Dernier prix d'achat
  private Date derniereDateAchat = null; // Dernière date d'achat
  private Integer topReappro = null; // Top réapprovisionnement
  private Date dateDebutSaison = null; // Date de début de saison
  private Date dateFinSaison = null; // Date de fin de saison
  private Integer pourcentageConso = null; // Pourcentage de consommation
  private Date dateDebutSaison2 = null; // Date de début de saison 2
  private Date dateFinSaison2 = null; // Date de fin de saison 2
  private Integer pourcentageConso2 = null; // Pourcentage de consommation 2
  private Integer ProtectionPrixRevient = null; // Protection du prix de revient
  private String codePrixRevient = null; // Code du prix de revient
  private IdUnite idUS = null; // Code de l'unité de stockage
  private Unite uniteStock = null;
  private BigDecimal poidsEnUS = null; // Poids en unité de stock
  private BigDecimal volume = null; // Volume
  private Integer colisage = null; // Colisage
  private String regroupementColisage = null; // Regroupement colisage
  private BigDecimal nombreUVParUCV = null;
  private BigDecimal conditionnement2 = null;
  private BigDecimal conditionnement3 = null;
  private BigDecimal conditionnement4 = null;
  private IdUnite idUCV = null;
  private IdUnite idUCV2 = null;
  private IdUnite idUCV3 = null;
  private IdUnite idUCV4 = null;
  private String topRattachementUnitaire2 = null;
  private String topRattachementUnitaire3 = null;
  private String topRattachementUnitaire4 = null;
  private BigDecimal prixInventaire = null;
  private BigDecimal prixFabricationOuMiniVente = null;
  private BigDecimal montantDepreciation = null;
  private BigDecimal pourcentageDepreciation = null;
  private String codeDepreciation = null;
  private IdMagasin idMagasin = null;
  private String codeModeExpedition = null;
  private String topNonSoumisEscompte = null;
  private String topGestionParLots = null;
  private String typeFicheArticle = null;
  private String typeSubstitutionASB = null;
  private String topNonGereEnStats = null;
  private String topNonEditionDetailNomenclature = null;
  private String typeExtensionArticleParLigne = null;
  private Boolean uniteConditionnementVenteNonScindable = null;
  private String topExclusionRemisesGlobales = null;
  private Integer topN11Specif1 = null;
  private Integer topZoneUtiliseeWspm001171 = null;
  private BigDecimal topN51Specif1 = null;
  private BigDecimal topN71Specif1 = null;
  private Date dateDebutCommercialisation = null;
  private Date dateFinReappro = null;
  private Date dateFinCommercialisation = null;
  private Boolean topEditionBonPreparation = null;
  private Boolean topAccuseReceptionCommande = null;
  private Boolean topBonExpedition = null;
  private Boolean topEditionBonTransporteur = null;
  private Boolean topFacture = null;
  private Boolean topDevis = null;
  private Character classeRotation = null;
  
  private IdUnite idUCS = null;
  private BigDecimal nombreUSParUCS = null;
  private IdSousFamille idSousFamille = null;
  private String topExclusionRistournes = null;
  private String topSpecificiteExtraction = null;
  private String topEnsembleMateriels = null;
  private String topTaxeAdditionnelle = null;
  private String topTaxeTgap = null;
  private String topExclusionWeb = null;
  private String reference1 = null;
  private BigDecimal coefficientConditionSurQuantite = null;
  private String topRegimeDouanier = null;
  private String systemeVariable1 = null;
  private String systemeVariable2 = null;
  private String codeUnitePresentation = null;
  private String typeChoixFournisseur = null;
  private String typeExtensionMateriel = null;
  private String codeUniteTransport = null;
  private BigDecimal nombreUniteStockageParUniteTransport = null;
  private BigDecimal longueur = null;
  private BigDecimal largeur = null;
  private BigDecimal hauteur = null;
  private BigDecimal poidsEnUniteConditionnement = null;
  private BigDecimal volumeEnUniteConditionnement = null;
  private String typeDateLimiteUtilisationOptimale = null; // Mois/Semaine/Jour
  private Integer delaiUtilisationOptimale1 = null;
  private Integer delaiUtilisationOptimale2 = null;
  private Integer delaiUtilisationOptimale3 = null;
  private BigDecimal poidsNetEnUniteStock = null;
  private String nomenclatureDouniere = null;
  private String codeOrigine = null;
  private String topElementFrais = null;
  private String typeRepartition = null;
  private String typeProduit = null;
  private String codeDisponibilite = null;
  private String topDepositaire = null;
  private Date dateSpecifGGEDRUBIS = null;
  private Date dateFinGarantieFixe = null;
  private Integer quantiteMaxPourAlerteEnUVouMultiple = null;
  private String codeUniteConditionnementVente5 = null;
  private BigDecimal conditionnement5 = null;
  private String zonePersoA20 = null;
  private String zonePersoA21 = null;
  private String codeNonSuivi = null; // Parametre NS
  private String topAutorisationVenteNonDispo = null;
  private String topNonSaisieDirecteSurBon = null;
  private String typeUniteLongueur = null; // Longueur en m (' ') ou cm('2') ou mm('3')
  private String typeUniteLargeur = null;
  private String typeUniteHauteur = null;
  private String topConditionRepriseArticle = null;
  private String motDirecteur1 = null;
  private String motDirecteur2 = null;
  private String motDirecteur3 = null;
  private String observationCourte = null;
  private String rattachementCNV1 = null;
  private String rattachementCNV2 = null;
  private String rattachementCNV3 = null;
  
  // Zones n'appartenants pas au fichier PGVMARTM (alimenté par le service rechercherArticles)
  private Integer nombreDecimaleStock = null;
  private BigDecimal quantitePhysique = null;
  private BigDecimal quantiteReserveeUCA = null;
  private BigDecimal quantiteAttendueUCA = null;
  private BigDecimal quantiteAffecteeUCA = null;
  private BigDecimal quantiteDisponibleVente = null;
  private BigDecimal quantiteDisponibleAchat = null;
  private BigDecimal quantiteStockMaximumUCA = null;
  private BigDecimal quantiteStockMinimumUCA = null;
  private BigDecimal quantiteStockIdealUCA = null;
  private BigDecimal consommationMoyenneUCA = null;
  private PrixAchat prixAchat = null;
  private PrixFlash prixFlash = null;
  private Boolean directUsine = null;
  private Boolean affichageMemoObligatoire = null;
  private Boolean alerteConditionnementFournisseur = null;
  private BigDecimal quantiteConditionnementFournisseurUS = null;
  
  private BigDecimal prixDeRevientStandardHT = null;
  private BigDecimal prixDeRevientHT = null;
  
  // Articles consignés (palettes)
  private BigDecimal quantiteStockClient = null;
  private BigDecimal prixConsignation = null;
  private BigDecimal prixDeconsignation = null;
  private Boolean gestionPalettesParChantier = null;
  private BigDecimal quantiteStockChantier = null;
  
  // Variables de travail
  private EnumTypeDecoupeArticle typeDecoupe = EnumTypeDecoupeArticle.IMPOSSIBLE;
  
  /**
   * Constructeur avec l'identifiant de l'article.
   */
  public Article(IdArticle pIdArticle) {
    super(pIdArticle);
  }
  
  @Override
  public String getTexte() {
    return getLibelle1();
  }
  // -- Méthodes publiques
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdArticle controlerId(Article pArticle, boolean pVerifierExistance) {
    if (pArticle == null) {
      throw new MessageErreurException("L'article est invalide.");
    }
    return IdArticle.controlerId(pArticle.getId(), true);
  }
  
  /**
   * Contôle qu'une longueur de découpe est acceptable (c'est à dire inférieure à la longueur totale de l'article et
   * supérieure à 0).
   * Si l'article est découpable et que la longueur d'origine de l'article est égale à 0 on accepte toute les découpes.
   * En effet la longueur d'origine n'est pas obligatoirement précisée dans la fiche article
   */
  public void controlerLongueurDecoupe(BigDecimal longueurDecoupe) {
    if (this.getLongueur().compareTo(BigDecimal.ZERO) != 0 && longueurDecoupe.compareTo(this.getLongueurEnMetre()) > 0) {
      String message = "La longueur de découpe indiquée est supérieure à la longueur totale de l'article\n";
      throw new MessageErreurException(message);
    }
    else if (longueurDecoupe.compareTo(BigDecimal.ZERO) <= 0) {
      String message = "Vous avez saisi une longueur inférieure ou égale à 0.\n Merci de saisir une valeur significative.\n";
      throw new MessageErreurException(message);
    }
  }
  
  /**
   * Contôle qu'une largeur de découpe est acceptable (c'est à dire inférieure à la largeur totale de l'article et
   * supérieure à 0).
   * Si l'article est découpable et que la largeur d'origine de l'article est égale à 0 on accepte toute les découpes.
   * En effet la largeur d'origine n'est pas obligatoirement précisée dans la fiche article
   */
  public void controlerLargeurDecoupe(BigDecimal largeurDecoupe) {
    if (this.getLargeur().compareTo(BigDecimal.ZERO) != 0 && largeurDecoupe.compareTo(this.getLargeurEnMetre()) > 0) {
      String message = "La largeur de découpe indiquée est supérieure à la largeur totale de l'article\n";
      throw new MessageErreurException(message);
    }
    else if (largeurDecoupe.compareTo(BigDecimal.ZERO) <= 0) {
      String message = "Vous avez saisi une largeur inférieure ou égale à 0.\n Merci de saisir une valeur significative.\n";
      throw new MessageErreurException(message);
    }
  }
  
  /**
   * Contôle qu'une hauteur de découpe est acceptable (c'est à dire inférieure à la hauteur totale de l'article et
   * supérieure à 0).
   * Si l'article est découpable et que la hauteur d'origine de l'article est égale à 0 on accepte toute les découpes.
   * En effet la hauteur d'origine n'est pas obligatoirement précisée dans la fiche article
   */
  public void controlerHauteurDecoupe(BigDecimal hauteurDecoupe) {
    if (this.getHauteur().compareTo(BigDecimal.ZERO) != 0 && hauteurDecoupe.compareTo(this.getHauteurEnMetre()) > 0) {
      String message = "La hauteur de découpe indiquée est supérieure à la hauteur totale de l'article\n";
      throw new MessageErreurException(message);
    }
    else if (hauteurDecoupe.compareTo(BigDecimal.ZERO) <= 0) {
      String message = "Vous avez saisi une hauteur inférieure ou égale à 0.\n Merci de saisir une valeur significative.\n";
      throw new MessageErreurException(message);
    }
  }
  
  /**
   * Contrôle s'il sagit d'un article reprit mais incomplet ou effacé de la base.
   * Il peut arriver quand on reprend des données clients que la référence à un article supprimé de la base persiste
   * dans les documents
   */
  public boolean isArticleGeneriqueReprise() {
    return getId().getCodeArticle().trim().equals(CODE_ARTICLE_REPRISE);
  }
  
  /**
   * Calcule le stock disponible (appelé aussi le stock à terme).
   */
  public BigDecimal calculerStockDisponible() {
    if (quantitePhysique != null && quantiteAttendueUCA != null && quantiteReserveeUCA != null) {
      BigDecimal stockdisponible = quantitePhysique.add(quantiteAttendueUCA).subtract(quantiteReserveeUCA);
      if (prixAchat != null && prixAchat.getNombreDecimaleUCA() != null) {
        return stockdisponible.setScale(prixAchat.getNombreDecimaleUCA(), RoundingMode.HALF_UP);
      }
      return stockdisponible;
    }
    return null;
  }
  
  /**
   * Retourne la liste des ids des lignes d'achat contenant un article en cours de commande pour un fournisseur donné.
   * Si non trouvé la liste est vide (sa valeur ne vaut pas null).
   */
  public static List<IdLigneAchat> getListeIdLigneAchatContenantArticleEnCoursCommande(IdSession pIdSession, IdArticle pIdArticle,
      IdFournisseur pIdFournisseur) {
    if (pIdArticle == null) {
      throw new MessageErreurException("L'id de l'article est invalide.");
    }
    if (pIdFournisseur == null) {
      throw new MessageErreurException("L'id du fournisseur est invalide.");
    }
    
    // Constitution des règles de recherche
    CritereLigneAchat criteres = new CritereLigneAchat();
    criteres.setIdEtablissement(pIdArticle.getIdEtablissement());
    criteres.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.COMMANDE);
    criteres.ajouterCodeEtat(EnumCodeEtatDocument.ATTENTE, EnumCodeEtatDocument.VALIDE);
    criteres.setIdFournisseur(pIdFournisseur);
    criteres.setIdArticle(pIdArticle);
    criteres.setExclureLigneReceptionnee(true);
    
    // Lancement de la recherche
    return ManagerServiceDocumentAchat.chargerListeIdLignesAchat(pIdSession, criteres);
  }
  
  // -- Méthodes privées
  
  // -- Méthodes statiques
  
  /**
   * Convertir les dimensions de découpe en mètres.
   */
  public static BigDecimal convertirDimensionEnMetre(BigDecimal pValeur, String pTypeUnite, int pNombreDecimales) {
    // Si dimension de découpe donnée en cm
    if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_CM)) {
      return pValeur.divide(Constantes.VALEUR_CENT, pNombreDecimales, RoundingMode.HALF_UP);
    }
    // Si dimension de découpe donnée en mm
    else if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_MM)) {
      return pValeur.divide(BigDecimal.valueOf(1000), pNombreDecimales, RoundingMode.HALF_UP);
    }
    // Sinon c'est déjà en mètres
    return pValeur;
  }
  
  /**
   * Convertir en cm ou mm les dimensions affichées en mètre.
   */
  public static BigDecimal convertirEnDivisionMetres(BigDecimal pDimension, String pUnite) {
    if (pUnite.equals("cm")) {
      pDimension = pDimension.multiply(Constantes.VALEUR_CENT);
    }
    else if (pUnite.equals("mm")) {
      pDimension = pDimension.multiply(BigDecimal.valueOf(1000));
    }
    return pDimension.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Convertir en mètres des dimensions.
   */
  public static BigDecimal convertirEnMetres(BigDecimal pDimension, String pTypeUnite, boolean pArrondir) {
    if (pDimension == null) {
      return null;
    }
    BigDecimal resultat;
    if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_M) || pTypeUnite.equals(TYPE_UNITE_DIMENSION_m)) {
      resultat = pDimension;
    }
    else if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_CM)) {
      resultat = pDimension.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32);
    }
    else if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_MM)) {
      resultat = pDimension.divide(new BigDecimal(1000), MathContext.DECIMAL32);
    }
    else {
      throw new MessageErreurException("L'unité de dimension '" + pTypeUnite + "' n'est pas gérée.");
    }
    
    if (pArrondir) {
      return resultat.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
    return resultat;
  }
  
  /**
   * Convertir des dimensions depuis le mètre.
   */
  public static BigDecimal convertirDepuisMetres(BigDecimal pDimension, String pTypeUnite) {
    if (pDimension == null) {
      return null;
    }
    else if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_M) || pTypeUnite.equals(TYPE_UNITE_DIMENSION_m)) {
      return pDimension.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
    else if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_CM)) {
      return pDimension.multiply(Constantes.VALEUR_CENT, MathContext.DECIMAL32).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
    else if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_MM)) {
      return pDimension.multiply(new BigDecimal(1000), MathContext.DECIMAL32).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
    else {
      throw new MessageErreurException("L'unité de dimension '" + pTypeUnite + "' n'est pas gérée.");
    }
  }
  
  /**
   * Retourner le symbole associé à un type d'unité de dimension.
   * Le type est " " pout m, "2" pour cm ou "3" pour mm.
   */
  public static String getSymboleUnite(String pTypeUnite) {
    if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_CM)) {
      return "cm";
    }
    else if (pTypeUnite.equals(TYPE_UNITE_DIMENSION_MM)) {
      return "mm";
    }
    else {
      return "m";
    }
  }
  
  /**
   * Permet de mettre à jour les quantités en stock de l'article avec la pécision fixée par les précisions de prix
   * d'achat
   */
  public void majQuantiteStock(ParametrePrixAchat parametre) {
    if (parametre == null) {
      return;
    }
    
    Integer precisionUniteStock = parametre.getNombreDecimaleUS();
    if (precisionUniteStock != null) {
      setQuantiteDisponibleAchat(getQuantiteDisponibleAchat().setScale(precisionUniteStock, RoundingMode.HALF_UP));
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Retourner une quantité formatée pour l'affichage à l'écran.
   * Le texte est écrit en rouge lorsque la valeur est négative ou nulle.
   */
  private String getTexteQuantite(BigDecimal pQuantite, int precision) {
    if (pQuantite == null) {
      return null;
    }
    BigDecimal quantiteArrondie = pQuantite.setScale(precision, RoundingMode.HALF_UP);
    if (quantiteArrondie.compareTo(BigDecimal.ZERO) <= 0) {
      return "<html><span style='color:#FF0000'>" + Constantes.formater(quantiteArrondie, false) + "</span></html>";
    }
    else {
      return Constantes.formater(quantiteArrondie, false);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Acces aux extentions article (libellés 2, 3 et 4)
   */
  public ExtensionArticle getExtension() {
    return extension;
  }
  
  public void setExtension(ExtensionArticle extension) {
    this.extension = extension;
  }
  
  // -- Accesseurs
  
  public Integer getTopSysteme() {
    return topSysteme;
  }
  
  public void setTopSysteme(Integer topSysteme) {
    this.topSysteme = topSysteme;
  }
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public Date getDateModification() {
    return dateModification;
  }
  
  public void setDateModification(Date dateModification) {
    this.dateModification = dateModification;
  }
  
  public Date getDateTraitement() {
    return dateTraitement;
  }
  
  public void setDateTraitement(Date dateTraitement) {
    this.dateTraitement = dateTraitement;
  }
  
  public String getTopNePlusUtiliser() {
    return topNePlusUtiliser;
  }
  
  public void setTopNePlusUtiliser(String topNePlusUtiliser) {
    this.topNePlusUtiliser = topNePlusUtiliser.trim();
  }
  
  /**
   * Identifiant du groupe de l'article.
   * L'identifiant du groupe est déduit de celui de la famille. Le groupe n'est donc pas modificable directement, il
   * faut modifier la
   * famille via la méthode setFamille()..
   */
  public IdGroupe getIdGroupe() {
    if (getIdFamille() == null) {
      return null;
    }
    
    return getIdFamille().getIdGroupe();
  }
  
  /**
   * Identifiant de la sous-famille de l'article.
   */
  public IdSousFamille getIdSousFamille() {
    return idSousFamille;
  }
  
  /**
   * Modifier l'identifiant de la sous-famille de l'article.
   */
  public void setIdSousFamille(IdSousFamille pIdSousFamille) {
    this.idSousFamille = pIdSousFamille;
  }
  
  public String getClefClassement1() {
    return clefClassement1;
  }
  
  public void setClefClassement1(String clefClassement1) {
    this.clefClassement1 = clefClassement1.trim();
  }
  
  public String getClefClassement2() {
    return clefClassement2;
  }
  
  public void setClefClassement2(String clefClassement2) {
    this.clefClassement2 = clefClassement2.trim();
  }
  
  public String getArticleRemplacementStk() {
    return articleRemplacementStk;
  }
  
  public void setArticleRemplacementStk(String articleRemplacementStk) {
    this.articleRemplacementStk = articleRemplacementStk.trim();
  }
  
  public String getGencode() {
    return gencode;
  }
  
  public void setGencode(String gencode) {
    this.gencode = gencode;
  }
  
  public String getNomenclatureProduit() {
    return nomenclatureProduit;
  }
  
  public void setNomenclatureProduit(String nomenclatureProduit) {
    this.nomenclatureProduit = nomenclatureProduit.trim();
  }
  
  public Integer getArticleNomenclature() {
    return articleNomenclature;
  }
  
  public void setArticleNomenclature(Integer articleNomenclature) {
    this.articleNomenclature = articleNomenclature;
  }
  
  public String getRegroupementStatistique() {
    return regroupementStatistique;
  }
  
  public void setRegroupementStatistique(String regroupementStatistique) {
    this.regroupementStatistique = regroupementStatistique.trim();
  }
  
  public String getCritereAnalyse1() {
    return critereAnalyse1;
  }
  
  public void setCritereAnalyse1(String critereAnalyse1) {
    this.critereAnalyse1 = critereAnalyse1.trim();
  }
  
  public String getCritereAnalyse2() {
    return critereAnalyse2;
  }
  
  public void setCritereAnalyse2(String critereAnalyse2) {
    this.critereAnalyse2 = critereAnalyse2.trim();
  }
  
  public String getCritereAnalyse3() {
    return critereAnalyse3;
  }
  
  public void setCritereAnalyse3(String critereAnalyse3) {
    this.critereAnalyse3 = critereAnalyse3.trim();
  }
  
  public String getCritereAnalyse4() {
    return critereAnalyse4;
  }
  
  public void setCritereAnalyse4(String critereAnalyse4) {
    this.critereAnalyse4 = critereAnalyse4.trim();
  }
  
  public String getCritereAnalyse5() {
    return critereAnalyse5;
  }
  
  public void setCritereAnalyse5(String critereAnalyse5) {
    this.critereAnalyse5 = critereAnalyse5.trim();
  }
  
  public String getTopDesignation1() {
    return topDesignation1;
  }
  
  public void setTopDesignation1(String topDesignation1) {
    this.topDesignation1 = topDesignation1.trim();
  }
  
  public String getTopDesignation2() {
    return topDesignation2;
  }
  
  public void setTopDesignation2(String topDesignation2) {
    this.topDesignation2 = topDesignation2.trim();
  }
  
  public String getTopDesignation3() {
    return topDesignation3;
  }
  
  public void setTopDesignation3(String topDesignation3) {
    this.topDesignation3 = topDesignation3.trim();
  }
  
  public String getTopDesignation4() {
    return topDesignation4;
  }
  
  public void setTopDesignation4(String topDesignation4) {
    this.topDesignation4 = topDesignation4.trim();
  }
  
  public String getCodeTarif() {
    return codeTarif;
  }
  
  public void setCodeTarif(String codeTarif) {
    this.codeTarif = codeTarif.trim();
  }
  
  public String getRattachementCNV() {
    return rattachementCNV;
  }
  
  public void setRattachementCNV(String rattachementCNV) {
    this.rattachementCNV = rattachementCNV.trim();
  }
  
  public Integer getDateDerniereVente() {
    return dateDerniereVente;
  }
  
  public void setDateDerniereVente(Integer dateDerniereVente) {
    this.dateDerniereVente = dateDerniereVente;
  }
  
  public Integer getCodeTVA() {
    return codeTVA;
  }
  
  public void setCodeTVA(Integer codeTVA) {
    this.codeTVA = codeTVA;
  }
  
  public Integer getCodeTaxeParafiscale() {
    return codeTaxeParafiscale;
  }
  
  public void setCodeTaxeParafiscale(Integer codeTaxeParafiscale) {
    this.codeTaxeParafiscale = codeTaxeParafiscale;
  }
  
  public Integer getNonStocke() {
    return nonStocke;
  }
  
  public void setNonStocke(Integer nonStocke) {
    this.nonStocke = nonStocke;
  }
  
  /**
   * Précision de l'unité de stocks.
   */
  public Integer getNombreDecimaleUS() {
    return nombreDecimaleUS;
  }
  
  /**
   * Modifier la précision de l'unité de stocks.
   */
  public void setNombreDecimaleUS(Integer pNombreDecimaleUS) {
    nombreDecimaleUS = pNombreDecimaleUS;
  }
  
  /**
   * Précision de l'unité de ventes.
   */
  public Integer getNombreDecimaleUV() {
    return nombreDecimaleUV;
  }
  
  /**
   * Modifier la précision de l'unité de ventes (A1DCV)
   * 
   * Lors de l'ajout d'une ligne article à un document de ventes, la précision utilisée pour l'unité de ventes (UV) est
   * celle-ci. Cela
   * permet de modifier la précision de l'unité dans les personnalisations sans impacter les articles précédemment
   * saisis. Le choix
   * d'adapter ou non la précision des articles est laissé au libre choix de l'utilisateur lorsqu'il modifie l'unité.
   */
  public void setNombreDecimaleUV(Integer pNombreDecimaleUV) {
    nombreDecimaleUV = pNombreDecimaleUV;
  }
  
  public Integer getArticleUtiliseEnSaisie() {
    return articleUtiliseEnSaisie;
  }
  
  public void setArticleUtiliseEnSaisie(Integer articleUtiliseEnSaisie) {
    this.articleUtiliseEnSaisie = articleUtiliseEnSaisie;
  }
  
  public Integer getTopNonCommissionne() {
    return topNonCommissionne;
  }
  
  public void setTopNonCommissionne(Integer topNonCommissionne) {
    this.topNonCommissionne = topNonCommissionne;
  }
  
  public Integer getCodeArrondi() {
    return codeArrondi;
  }
  
  public void setCodeArrondi(Integer codeArrondi) {
    this.codeArrondi = codeArrondi;
  }
  
  public IdUnite getIdUV() {
    return idUV;
  }
  
  public void setIdUV(IdUnite pIdUV) {
    this.idUV = pIdUV;
  }
  
  /**
   * Nombre d'unité de stocks par unité de vente.
   */
  public BigDecimal getCoefficientUSParUV() {
    if (coefficientUSParUV == null || coefficientUSParUV.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ONE;
    }
    return coefficientUSParUV;
  }
  
  public void setCoefficientUSParUV(BigDecimal coefficientUSParUV) {
    this.coefficientUSParUV = coefficientUSParUV;
  }
  
  public BigDecimal getCoefficientMarge() {
    return coefficientMarge;
  }
  
  public void setCoefficientMarge(BigDecimal coefficientMarge) {
    this.coefficientMarge = coefficientMarge;
  }
  
  public Integer getPourcentageMargeMini() {
    return pourcentageMargeMini;
  }
  
  public void setPourcentageMargeMini(Integer pourcentageMargeMini) {
    this.pourcentageMargeMini = pourcentageMargeMini;
  }
  
  public Integer getPourcentageRemiseMaxi() {
    return pourcentageRemiseMaxi;
  }
  
  public void setPourcentageRemiseMaxi(Integer pourcentageRemiseMaxi) {
    this.pourcentageRemiseMaxi = pourcentageRemiseMaxi;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur pIdFournisseur) {
    idFournisseur = pIdFournisseur;
  }
  
  public BigDecimal getDernierPrixAchat() {
    return dernierPrixAchat;
  }
  
  public void setDernierPrixAchat(BigDecimal dernierPrixAchat) {
    this.dernierPrixAchat = dernierPrixAchat;
  }
  
  public Date getDerniereDateAchat() {
    return derniereDateAchat;
  }
  
  public void setDerniereDateAchat(Date derniereDateAchat) {
    this.derniereDateAchat = derniereDateAchat;
  }
  
  public Integer getTopReappro() {
    return topReappro;
  }
  
  public void setTopReappro(Integer topReappro) {
    this.topReappro = topReappro;
  }
  
  public Date getDateDebutSaison() {
    return dateDebutSaison;
  }
  
  public void setDateDebutSaison(Date dateDebutSaison) {
    this.dateDebutSaison = dateDebutSaison;
  }
  
  public Date getDateFinSaison() {
    return dateFinSaison;
  }
  
  public void setDateFinSaison(Date dateFinSaison) {
    this.dateFinSaison = dateFinSaison;
  }
  
  public Integer getPourcentageConso() {
    return pourcentageConso;
  }
  
  public void setPourcentageConso(Integer pourcentageConso) {
    this.pourcentageConso = pourcentageConso;
  }
  
  public Date getDateDebutSaison2() {
    return dateDebutSaison2;
  }
  
  public void setDateDebutSaison2(Date dateDebutSaison2) {
    this.dateDebutSaison2 = dateDebutSaison2;
  }
  
  public Date getDateFinSaison2() {
    return dateFinSaison2;
  }
  
  public void setDateFinSaison2(Date dateFinSaison2) {
    this.dateFinSaison2 = dateFinSaison2;
  }
  
  public Integer getPourcentageConso2() {
    return pourcentageConso2;
  }
  
  public void setPourcentageConso2(Integer pourcentageConso2) {
    this.pourcentageConso2 = pourcentageConso2;
  }
  
  public Integer getProtectionPrixRevient() {
    return ProtectionPrixRevient;
  }
  
  public void setProtectionPrixRevient(Integer protectionPrixRevient) {
    ProtectionPrixRevient = protectionPrixRevient;
  }
  
  public String getCodePrixRevient() {
    return codePrixRevient;
  }
  
  public void setCodePrixRevient(String codePrixRevient) {
    this.codePrixRevient = codePrixRevient.trim();
  }
  
  public IdUnite getIdUS() {
    return idUS;
  }
  
  public void setIdUS(IdUnite pIdUS) {
    this.idUS = pIdUS;
  }
  
  public BigDecimal getPoidsEnUS() {
    return poidsEnUS;
  }
  
  public void setPoidsEnUS(BigDecimal poidsEnUS) {
    this.poidsEnUS = poidsEnUS;
  }
  
  public BigDecimal getVolume() {
    return volume;
  }
  
  public void setVolume(BigDecimal volume) {
    this.volume = volume;
  }
  
  public Integer getColisage() {
    return colisage;
  }
  
  public void setColisage(Integer colisage) {
    this.colisage = colisage;
  }
  
  public String getRegroupementColisage() {
    return regroupementColisage;
  }
  
  public void setRegroupementColisage(String regroupementColisage) {
    this.regroupementColisage = regroupementColisage.trim();
  }
  
  /**
   * Retourne true si le nombre d'unités de ventes (UV) par unités de commandes de ventes (UCV) est défini avec une
   * valeur autre que 1.
   * On considère que si ce coefficient n'est pas définit ou qu'il a la valeur zéro, cela équivaut à la valeur 1.
   */
  public boolean isDefiniNombreUVParUCV() {
    return nombreUVParUCV != null && nombreUVParUCV.compareTo(BigDecimal.ZERO) != 0 && nombreUVParUCV.compareTo(BigDecimal.ONE) != 0;
  }
  
  /**
   * Nombre d'unités de ventes (UV) par unités de commandes de ventes (UCV).
   */
  public BigDecimal getNombreUVParUCV() {
    return nombreUVParUCV;
  }
  
  /**
   * Texte décrivant le nombre d'unités de ventes (UV) par unités de commandes de ventes (UCV).
   * Exemple : 3.00 M²/U
   */
  public String getTexteUVParUCV() {
    if (idUCV == null || idUV == null || !isDefiniNombreUVParUCV()) {
      return null;
    }
    if (!idUCV.getCode().isEmpty() && !idUCV.equals(idUV)) {
      return Constantes.formater(nombreUVParUCV, false) + " " + idUV.getCode() + "/" + idUCV.getCode();
    }
    else {
      return "";
    }
  }
  
  /**
   * Modifier le nombre d'unités de ventes (UV) par unités de commandes de ventes (UCV).
   */
  public void setNombreUVParUCV(BigDecimal pNombreUVParUCV) {
    nombreUVParUCV = pNombreUVParUCV;
  }
  
  public BigDecimal getConditionnement2() {
    return conditionnement2;
  }
  
  public void setConditionnement2(BigDecimal conditionnement2) {
    this.conditionnement2 = conditionnement2;
  }
  
  public BigDecimal getConditionnement3() {
    return conditionnement3;
  }
  
  public void setConditionnement3(BigDecimal conditionnement3) {
    this.conditionnement3 = conditionnement3;
  }
  
  public BigDecimal getConditionnement4() {
    return conditionnement4;
  }
  
  public void setConditionnement4(BigDecimal conditionnement4) {
    this.conditionnement4 = conditionnement4;
  }
  
  /**
   * Code de l'unité de conditionnement de ventes.
   * Si l'article n'a pas d'unité de conditionnement de ventes, c'est l'unité de vente qui fait office d'unité de
   * conditionnement de
   * ventes.
   */
  public IdUnite getIdUCV() {
    if (idUCV != null) {
      return idUCV;
    }
    else {
      return idUV;
    }
  }
  
  public void setIdUCV(IdUnite pIdUCV) {
    this.idUCV = pIdUCV;
  }
  
  public IdUnite getIdUCV2() {
    return idUCV2;
  }
  
  public void setIdUCV2(IdUnite idUCV2) {
    this.idUCV2 = idUCV2;
  }
  
  public IdUnite getIdUCV3() {
    return idUCV3;
  }
  
  public void setIdUCV3(IdUnite idUCV3) {
    this.idUCV3 = idUCV3;
  }
  
  public IdUnite getIdUCV4() {
    return idUCV4;
  }
  
  public void setIdUCV4(IdUnite idUCV4) {
    this.idUCV4 = idUCV4;
  }
  
  public String getTopRattachementUnitaire2() {
    return topRattachementUnitaire2;
  }
  
  public void setTopRattachementUnitaire2(String topRattachementUnitaire2) {
    this.topRattachementUnitaire2 = topRattachementUnitaire2.trim();
  }
  
  public String getTopRattachementUnitaire3() {
    return topRattachementUnitaire3;
  }
  
  public void setTopRattachementUnitaire3(String topRattachementUnitaire3) {
    this.topRattachementUnitaire3 = topRattachementUnitaire3.trim();
  }
  
  public String getTopRattachementUnitaire4() {
    return topRattachementUnitaire4;
  }
  
  public void setTopRattachementUnitaire4(String topRattachementUnitaire4) {
    this.topRattachementUnitaire4 = topRattachementUnitaire4.trim();
  }
  
  public BigDecimal getPrixInventaire() {
    return prixInventaire;
  }
  
  public void setPrixInventaire(BigDecimal prixInventaire) {
    this.prixInventaire = prixInventaire;
  }
  
  public BigDecimal getPrixFabricationOuMiniVente() {
    return prixFabricationOuMiniVente;
  }
  
  public void setPrixFabricationOuMiniVente(BigDecimal prixFabricationOuMiniVente) {
    this.prixFabricationOuMiniVente = prixFabricationOuMiniVente;
  }
  
  public BigDecimal getMontantDepreciation() {
    return montantDepreciation;
  }
  
  public void setMontantDepreciation(BigDecimal montantDepreciation) {
    this.montantDepreciation = montantDepreciation;
  }
  
  public BigDecimal getPourcentageDepreciation() {
    return pourcentageDepreciation;
  }
  
  public void setPourcentageDepreciation(BigDecimal pourcentageDepreciation) {
    this.pourcentageDepreciation = pourcentageDepreciation;
  }
  
  public String getCodeDepreciation() {
    return codeDepreciation;
  }
  
  public void setCodeDepreciation(String codeDepreciation) {
    this.codeDepreciation = codeDepreciation.trim();
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin pIdMagasin) {
    this.idMagasin = pIdMagasin;
  }
  
  public String getCodeModeExpedition() {
    return codeModeExpedition;
  }
  
  public void setCodeModeExpedition(String codeModeExpedition) {
    this.codeModeExpedition = codeModeExpedition.trim();
  }
  
  public String getTopNonSoumisEscompte() {
    return topNonSoumisEscompte;
  }
  
  public void setTopNonSoumisEscompte(String topNonSoumisEscompte) {
    this.topNonSoumisEscompte = topNonSoumisEscompte.trim();
  }
  
  public String getTopGestionParLots() {
    return topGestionParLots;
  }
  
  public void setTopGestionParLots(String topGestionParLots) {
    this.topGestionParLots = topGestionParLots.trim();
  }
  
  public String getTypeFicheArticle() {
    return typeFicheArticle;
  }
  
  public void setTypeFicheArticle(String typeFicheArticle) {
    this.typeFicheArticle = typeFicheArticle.trim();
  }
  
  public String getTypeSubstitutionASB() {
    return typeSubstitutionASB;
  }
  
  public void setTypeSubstitutionASB(String typeSubstitutionASB) {
    this.typeSubstitutionASB = typeSubstitutionASB.trim();
  }
  
  public String getTopNonGereEnStats() {
    return topNonGereEnStats;
  }
  
  public void setTopNonGereEnStats(String topNonGereEnStats) {
    this.topNonGereEnStats = topNonGereEnStats.trim();
  }
  
  public String getTopNonEditionDetailNomenclature() {
    return topNonEditionDetailNomenclature;
  }
  
  public void setTopNonEditionDetailNomenclature(String topNonEditionDetailNomenclature) {
    this.topNonEditionDetailNomenclature = topNonEditionDetailNomenclature.trim();
  }
  
  public String getTypeExtensionArticleParLigne() {
    return typeExtensionArticleParLigne;
  }
  
  public void setTypeExtensionArticleParLigne(String typeExtensionArticleParLigne) {
    this.typeExtensionArticleParLigne = typeExtensionArticleParLigne.trim();
  }
  
  public boolean isUniteConditionnementVenteNonScindable() {
    if (uniteConditionnementVenteNonScindable == null) {
      return false;
    }
    return uniteConditionnementVenteNonScindable;
  }
  
  public void setUniteConditionnementVenteNonScindable(Boolean pUniteConditionnementVenteNonScindable) {
    uniteConditionnementVenteNonScindable = pUniteConditionnementVenteNonScindable;
  }
  
  public String getTopExclusionRemisesGlobales() {
    return topExclusionRemisesGlobales;
  }
  
  public void setTopExclusionRemisesGlobales(String topExclusionRemisesGlobales) {
    this.topExclusionRemisesGlobales = topExclusionRemisesGlobales.trim();
  }
  
  public Integer getTopN11Specif1() {
    return topN11Specif1;
  }
  
  public void setTopN11Specif1(Integer topN11Specif1) {
    this.topN11Specif1 = topN11Specif1;
  }
  
  public Integer getTopZoneUtiliseeWspm001171() {
    return topZoneUtiliseeWspm001171;
  }
  
  public void setTopZoneUtiliseeWspm001171(Integer topZoneUtiliseeWspm001171) {
    this.topZoneUtiliseeWspm001171 = topZoneUtiliseeWspm001171;
  }
  
  public BigDecimal getTopN51Specif1() {
    return topN51Specif1;
  }
  
  public void setTopN51Specif1(BigDecimal topN51Specif1) {
    this.topN51Specif1 = topN51Specif1;
  }
  
  public BigDecimal getTopN71Specif1() {
    return topN71Specif1;
  }
  
  public void setTopN71Specif1(BigDecimal topN71Specif1) {
    this.topN71Specif1 = topN71Specif1;
  }
  
  public Date getDateDebutCommercialisation() {
    return dateDebutCommercialisation;
  }
  
  public void setDateDebutCommercialisation(Date dateDebutCommercialisation) {
    this.dateDebutCommercialisation = dateDebutCommercialisation;
  }
  
  public Date getDateFinReappro() {
    return dateFinReappro;
  }
  
  public void setDateFinReappro(Date dateFinReappro) {
    this.dateFinReappro = dateFinReappro;
  }
  
  public Date getDateFinCommercialisation() {
    return dateFinCommercialisation;
  }
  
  public void setDateFinCommercialisation(Date dateFinCommercialisation) {
    this.dateFinCommercialisation = dateFinCommercialisation;
  }
  
  public IdUnite getIdUCS() {
    return idUCS;
  }
  
  public void setIdUCS(IdUnite pIdUCS) {
    this.idUCS = pIdUCS;
  }
  
  /**
   * Retourne true si le nombre d'unité de stocks (US) par unités de conditionnement de stocks (UCS) est défini est
   * différent de 1.
   * On considère que si ce coefficient n'est pas définit ou qu'il a la valeur zéro, cela équivaut à la valeur 1.
   */
  public boolean isDefiniNombreUSParUCS() {
    return nombreUSParUCS != null && nombreUSParUCS.compareTo(BigDecimal.ZERO) != 0 && nombreUSParUCS.compareTo(BigDecimal.ONE) != 0;
  }
  
  /**
   * Nombre d'unité de stocks (US) par unités de conditionnement de stocks (UCS).
   * 
   * Si la paramètre pCorrige est à true, la méthode retourne 1 si ce coefficient n'est pas définit ou qu'il a la valeur
   * zéro.
   * Si la paramètre pCorrige est à false, la méthode retourne la valeur native qui peut être null ou zéro. Dans ce cas,
   * il est
   * conseillé de vérifier que le coefficient est définit via la méthode isDefiniNombreUSParUCS() avant de l'utiliser.
   */
  public BigDecimal getNombreUSParUCS(boolean pCorrige) {
    if (pCorrige && !isDefiniNombreUSParUCS()) {
      return BigDecimal.ONE;
    }
    return nombreUSParUCS.setScale(nombreDecimaleUS);
  }
  
  /**
   * Modifier le nombre d'unité de stocks (US) par unités de conditionnement de stocks (UCS).
   */
  public void setNombreUSParUCS(BigDecimal pNombreUSParUCS) {
    nombreUSParUCS = pNombreUSParUCS;
  }
  
  public String getTopExclusionRistournes() {
    return topExclusionRistournes;
  }
  
  public void setTopExclusionRistournes(String topExclusionRistournes) {
    this.topExclusionRistournes = topExclusionRistournes.trim();
  }
  
  public String getTopSpecificiteExtraction() {
    return topSpecificiteExtraction;
  }
  
  public void setTopSpecificiteExtraction(String topSpecificiteExtraction) {
    this.topSpecificiteExtraction = topSpecificiteExtraction.trim();
  }
  
  public String getTopEnsembleMateriels() {
    return topEnsembleMateriels;
  }
  
  public void setTopEnsembleMateriels(String topEnsembleMateriels) {
    this.topEnsembleMateriels = topEnsembleMateriels.trim();
  }
  
  public String getTopTaxeAdditionnelle() {
    return topTaxeAdditionnelle;
  }
  
  public void setTopTaxeAdditionnelle(String topTaxeAdditionnelle) {
    this.topTaxeAdditionnelle = topTaxeAdditionnelle.trim();
  }
  
  public String getTopTaxeTgap() {
    return topTaxeTgap;
  }
  
  public void setTopTaxeTgap(String topTaxeTgap) {
    this.topTaxeTgap = topTaxeTgap.trim();
  }
  
  public String getTopExclusionWeb() {
    return topExclusionWeb;
  }
  
  public void setTopExclusionWeb(String topExclusionWeb) {
    this.topExclusionWeb = topExclusionWeb.trim();
  }
  
  public String getReference1() {
    return reference1;
  }
  
  public void setReference1(String reference1) {
    this.reference1 = reference1.trim();
  }
  
  public BigDecimal getCoefficientConditionSurQuantite() {
    return coefficientConditionSurQuantite;
  }
  
  public void setCoefficientConditionSurQuantite(BigDecimal coefficientConditionSurQuantite) {
    this.coefficientConditionSurQuantite = coefficientConditionSurQuantite;
  }
  
  public String getTopRegimeDouanier() {
    return topRegimeDouanier;
  }
  
  public void setTopRegimeDouanier(String topRegimeDouanier) {
    this.topRegimeDouanier = topRegimeDouanier.trim();
  }
  
  public String getSystemeVariable1() {
    return systemeVariable1;
  }
  
  public void setSystemeVariable1(String systemeVariable1) {
    this.systemeVariable1 = systemeVariable1.trim();
  }
  
  public String getSystemeVariable2() {
    return systemeVariable2;
  }
  
  public void setSystemeVariable2(String systemeVariable2) {
    this.systemeVariable2 = systemeVariable2.trim();
  }
  
  public String getCodeUnitePresentation() {
    return codeUnitePresentation;
  }
  
  public void setCodeUnitePresentation(String codeUnitePresentation) {
    this.codeUnitePresentation = codeUnitePresentation.trim();
  }
  
  public String getTypeChoixFournisseur() {
    return typeChoixFournisseur;
  }
  
  public void setTypeChoixFournisseur(String typeChoixFournisseur) {
    this.typeChoixFournisseur = typeChoixFournisseur.trim();
  }
  
  public String getTypeExtensionMateriel() {
    return typeExtensionMateriel;
  }
  
  public void setTypeExtensionMateriel(String typeExtensionMateriel) {
    this.typeExtensionMateriel = typeExtensionMateriel.trim();
  }
  
  public String getCodeUniteTransport() {
    return codeUniteTransport;
  }
  
  public void setCodeUniteTransport(String codeUniteTransport) {
    this.codeUniteTransport = codeUniteTransport.trim();
  }
  
  public BigDecimal getNombreUniteStockageParUniteTransport() {
    return nombreUniteStockageParUniteTransport;
  }
  
  public void setNombreUniteStockageParUniteTransport(BigDecimal nombreUniteStockageParUniteTransport) {
    this.nombreUniteStockageParUniteTransport = nombreUniteStockageParUniteTransport;
  }
  
  /**
   * Retourne le code de l'unité de découpe pour la longueur
   */
  public String getCodeUniteLongueur() {
    return getSymboleUnite(typeUniteLongueur);
  }
  
  /**
   * Retourne le code de l'unité de découpe pour la largeur
   */
  public String getCodeUniteLargeur() {
    return getSymboleUnite(typeUniteLargeur);
  }
  
  /**
   * Retourne le code de l'unité de découpe pour la hauteur
   */
  public String getCodeUniteHauteur() {
    return getSymboleUnite(typeUniteHauteur);
  }
  
  public BigDecimal getLongueur() {
    return longueur;
  }
  
  public BigDecimal getLongueur(int precision) {
    if (longueur == null) {
      return null;
    }
    return longueur.setScale(precision, RoundingMode.HALF_UP);
  }
  
  /**
   * Obtenir la longueur de l'article en mètre.
   */
  public BigDecimal getLongueurEnMetre() {
    return convertirEnMetres(longueur, typeUniteLongueur, false);
  }
  
  public void setLongueur(BigDecimal longueur) {
    this.longueur = longueur;
  }
  
  public BigDecimal getLargeur() {
    return largeur;
  }
  
  /**
   * Obtenir la largeur de l'article en mètre.
   */
  public BigDecimal getLargeur(int precision) {
    if (largeur == null) {
      return null;
    }
    return largeur.setScale(precision, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLargeurEnMetre() {
    return convertirEnMetres(largeur, typeUniteLargeur, false);
  }
  
  public void setLargeur(BigDecimal largeur) {
    this.largeur = largeur;
  }
  
  /**
   * Obtenir la hauteur de l'article en mètre.
   */
  public BigDecimal getHauteur() {
    return hauteur;
  }
  
  public BigDecimal getHauteur(int precision) {
    if (hauteur == null) {
      return null;
    }
    return hauteur.setScale(precision, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getHauteurEnMetre() {
    return convertirEnMetres(hauteur, typeUniteHauteur, false);
  }
  
  public void setHauteur(BigDecimal hauteur) {
    this.hauteur = hauteur;
  }
  
  public BigDecimal getPoidsEnUniteConditionnement() {
    return poidsEnUniteConditionnement;
  }
  
  public void setPoidsEnUniteConditionnement(BigDecimal poidsEnUniteConditionnement) {
    this.poidsEnUniteConditionnement = poidsEnUniteConditionnement;
  }
  
  public BigDecimal getVolumeEnUniteConditionnement() {
    return volumeEnUniteConditionnement;
  }
  
  public void setVolumeEnUniteConditionnement(BigDecimal volumeEnUniteConditionnement) {
    this.volumeEnUniteConditionnement = volumeEnUniteConditionnement;
  }
  
  public String getTypeDateLimiteUtilisationOptimale() {
    return typeDateLimiteUtilisationOptimale;
  }
  
  public void setTypeDateLimiteUtilisationOptimale(String typeDateLimiteUtilisationOptimale) {
    this.typeDateLimiteUtilisationOptimale = typeDateLimiteUtilisationOptimale.trim();
  }
  
  public Integer getDelaiUtilisationOptimale1() {
    return delaiUtilisationOptimale1;
  }
  
  public void setDelaiUtilisationOptimale1(Integer delaiUtilisationOptimale1) {
    this.delaiUtilisationOptimale1 = delaiUtilisationOptimale1;
  }
  
  public Integer getDelaiUtilisationOptimale2() {
    return delaiUtilisationOptimale2;
  }
  
  public void setDelaiUtilisationOptimale2(Integer delaiUtilisationOptimale2) {
    this.delaiUtilisationOptimale2 = delaiUtilisationOptimale2;
  }
  
  public Integer getDelaiUtilisationOptimale3() {
    return delaiUtilisationOptimale3;
  }
  
  public void setDelaiUtilisationOptimale3(Integer delaiUtilisationOptimale3) {
    this.delaiUtilisationOptimale3 = delaiUtilisationOptimale3;
  }
  
  public BigDecimal getPoidsNetEnUniteStock() {
    return poidsNetEnUniteStock;
  }
  
  public void setPoidsNetEnUniteStock(BigDecimal poidsNetEnUniteStock) {
    this.poidsNetEnUniteStock = poidsNetEnUniteStock;
  }
  
  public String getNomenclatureDouniere() {
    return nomenclatureDouniere;
  }
  
  public void setNomenclatureDouniere(String nomenclatureDouniere) {
    this.nomenclatureDouniere = nomenclatureDouniere.trim();
  }
  
  public String getCodeOrigine() {
    return codeOrigine;
  }
  
  public void setCodeOrigine(String codeOrigine) {
    this.codeOrigine = codeOrigine.trim();
  }
  
  public String getTopElementFrais() {
    return topElementFrais;
  }
  
  public void setTopElementFrais(String topElementFrais) {
    this.topElementFrais = topElementFrais.trim();
  }
  
  public String getTypeRepartition() {
    return typeRepartition;
  }
  
  public void setTypeRepartition(String typeRepartition) {
    this.typeRepartition = typeRepartition.trim();
  }
  
  public String getTypeProduit() {
    return typeProduit;
  }
  
  public void setTypeProduit(String typeProduit) {
    this.typeProduit = typeProduit.trim();
  }
  
  public String getCodeDisponibilite() {
    return codeDisponibilite;
  }
  
  public void setCodeDisponibilite(String codeDisponibilite) {
    this.codeDisponibilite = codeDisponibilite.trim();
  }
  
  public String getTopDepositaire() {
    return topDepositaire;
  }
  
  public void setTopDepositaire(String topDepositaire) {
    this.topDepositaire = topDepositaire.trim();
  }
  
  public Date getDateSpecifGGEDRUBIS() {
    return dateSpecifGGEDRUBIS;
  }
  
  public void setDateSpecifGGEDRUBIS(Date dateSpecifGGEDRUBIS) {
    this.dateSpecifGGEDRUBIS = dateSpecifGGEDRUBIS;
  }
  
  public Date getDateFinGarantieFixe() {
    return dateFinGarantieFixe;
  }
  
  public void setDateFinGarantieFixe(Date dateFinGarantieFixe) {
    this.dateFinGarantieFixe = dateFinGarantieFixe;
  }
  
  public Integer getQuantiteMaxPourAlerteEnUVouMultiple() {
    return quantiteMaxPourAlerteEnUVouMultiple;
  }
  
  public void setQuantiteMaxPourAlerteEnUVouMultiple(Integer quantiteMaxPourAlerteEnUVouMultiple) {
    this.quantiteMaxPourAlerteEnUVouMultiple = quantiteMaxPourAlerteEnUVouMultiple;
  }
  
  public String getCodeUniteConditionnementVente5() {
    return codeUniteConditionnementVente5;
  }
  
  public void setCodeUniteConditionnementVente5(String codeUniteConditionnementVente5) {
    this.codeUniteConditionnementVente5 = codeUniteConditionnementVente5.trim();
  }
  
  public BigDecimal getConditionnement5() {
    return conditionnement5;
  }
  
  public void setConditionnement5(BigDecimal conditionnement5) {
    this.conditionnement5 = conditionnement5;
  }
  
  public String getZonePersoA20() {
    return zonePersoA20;
  }
  
  public void setZonePersoA20(String zonePersoA20) {
    this.zonePersoA20 = zonePersoA20.trim();
  }
  
  public String getZonePersoA21() {
    return zonePersoA21;
  }
  
  public void setZonePersoA21(String zonePersoA21) {
    this.zonePersoA21 = zonePersoA21.trim();
  }
  
  public String getCodeNonSuivi() {
    return codeNonSuivi;
  }
  
  public void setCodeNonSuivi(String codeNonSuivi) {
    this.codeNonSuivi = codeNonSuivi.trim();
  }
  
  public String getTopAutorisationVenteNonDispo() {
    return topAutorisationVenteNonDispo;
  }
  
  public void setTopAutorisationVenteNonDispo(String topAutorisationVenteNonDispo) {
    this.topAutorisationVenteNonDispo = topAutorisationVenteNonDispo.trim();
  }
  
  public String getTopNonSaisieDirecteSurBon() {
    return topNonSaisieDirecteSurBon;
  }
  
  public void setTopNonSaisieDirecteSurBon(String topNonSaisieDirecteSurBon) {
    this.topNonSaisieDirecteSurBon = topNonSaisieDirecteSurBon.trim();
  }
  
  public String getTypeUniteLongueur() {
    return typeUniteLongueur;
  }
  
  public void setTypeUniteLongueur(String typeUniteLongueur) {
    this.typeUniteLongueur = typeUniteLongueur.trim();
  }
  
  public String getTypeUniteLargeur() {
    return typeUniteLargeur;
  }
  
  public void setTypeUniteLargeur(String typeUniteLargeur) {
    this.typeUniteLargeur = typeUniteLargeur.trim();
  }
  
  public String getTypeUniteHauteur() {
    return typeUniteHauteur;
  }
  
  public void setTypeUniteHauteur(String typeUniteHauteur) {
    this.typeUniteHauteur = typeUniteHauteur.trim();
  }
  
  public String getTopConditionRepriseArticle() {
    return topConditionRepriseArticle;
  }
  
  public void setTopConditionRepriseArticle(String topConditionRepriseArticle) {
    this.topConditionRepriseArticle = topConditionRepriseArticle.trim();
  }
  
  public String getMotDirecteur1() {
    return motDirecteur1;
  }
  
  public void setMotDirecteur1(String motDirecteur1) {
    this.motDirecteur1 = motDirecteur1.trim();
  }
  
  public String getMotDirecteur2() {
    return motDirecteur2;
  }
  
  public void setMotDirecteur2(String motDirecteur2) {
    this.motDirecteur2 = motDirecteur2.trim();
  }
  
  public String getMotDirecteur3() {
    return motDirecteur3;
  }
  
  public void setMotDirecteur3(String motDirecteur3) {
    this.motDirecteur3 = motDirecteur3.trim();
  }
  
  public String getObservationCourte() {
    return observationCourte;
  }
  
  public void setObservationCourte(String observationCourte) {
    this.observationCourte = Constantes.normerTexte(observationCourte);
  }
  
  public String getRattachementCNV1() {
    return rattachementCNV1;
  }
  
  public void setRattachementCNV1(String rattachementCNV1) {
    this.rattachementCNV1 = rattachementCNV1.trim();
  }
  
  public String getRattachementCNV2() {
    return rattachementCNV2;
  }
  
  public void setRattachementCNV2(String rattachementCNV2) {
    this.rattachementCNV2 = rattachementCNV2.trim();
  }
  
  public String getRattachementCNV3() {
    return rattachementCNV3;
  }
  
  public void setRattachementCNV3(String rattachementCNV3) {
    this.rattachementCNV3 = rattachementCNV3.trim();
  }
  
  /**
   * Retourner la quantité de l'article en stock physique.
   */
  public BigDecimal getQuantitePhysique() {
    if (prixAchat != null && prixAchat.getNombreDecimaleUCA() != null) {
      return quantitePhysique.setScale(prixAchat.getNombreDecimaleUCA(), RoundingMode.HALF_UP);
    }
    return quantitePhysique;
  }
  
  /**
   * Mettre à jour la quantité en stock physique de l'article
   */
  public void setQuantitePhysique(BigDecimal pQuantitePhysique) {
    this.quantitePhysique = pQuantitePhysique;
  }
  
  public BigDecimal getQuantiteReservee() {
    if (prixAchat != null && prixAchat.getNombreDecimaleUCA() != null) {
      return quantiteReserveeUCA.setScale(prixAchat.getNombreDecimaleUCA(), RoundingMode.HALF_UP);
    }
    return quantiteReserveeUCA;
  }
  
  public void setQuantiteReserveeUCA(BigDecimal quantiteReservee) {
    this.quantiteReserveeUCA = quantiteReservee;
  }
  
  public BigDecimal getQuantiteAttendueUCA() {
    if (prixAchat != null && prixAchat.getNombreDecimaleUCA() != null) {
      return quantiteAttendueUCA.setScale(prixAchat.getNombreDecimaleUCA(), RoundingMode.HALF_UP);
    }
    return quantiteAttendueUCA;
  }
  
  public void setQuantiteAttendueUCA(BigDecimal quantiteAttendue) {
    this.quantiteAttendueUCA = quantiteAttendue;
  }
  
  public BigDecimal getQuantiteAffecteeUCA() {
    if (prixAchat != null && prixAchat.getNombreDecimaleUCA() != null) {
      return quantiteAffecteeUCA.setScale(prixAchat.getNombreDecimaleUCA(), RoundingMode.HALF_UP);
    }
    return quantiteAffecteeUCA;
  }
  
  public void setQuantiteAffecteeUCA(BigDecimal pQuantiteAffectee) {
    quantiteAffecteeUCA = pQuantiteAffectee;
  }
  
  /**
   * Retourner la quantité en stock disponible à la vente de l'article : Il s'agit du stock physique + le stock commandé
   */
  public BigDecimal getQuantiteDisponibleVente() {
    return quantiteDisponibleVente;
  }
  
  /**
   * Modifier la quantité en stock disponible à la vente de l'article : Il s'agit du stock physique + le stock commandé
   */
  public void setQuantiteDisponibleVente(BigDecimal pQuantiteDisponibleVente) {
    if (pQuantiteDisponibleVente == null) {
      quantiteDisponibleVente = pQuantiteDisponibleVente;
      return;
    }
    if (nombreDecimaleStock != null) {
      quantiteDisponibleVente = pQuantiteDisponibleVente.setScale(nombreDecimaleStock, RoundingMode.HALF_UP);
    }
    else {
      quantiteDisponibleVente = pQuantiteDisponibleVente;
    }
  }
  
  public BigDecimal getQuantiteDisponibleAchat() {
    return quantiteDisponibleAchat;
  }
  
  public void setQuantiteDisponibleAchat(BigDecimal pQuantiteDisponibleAchat) {
    if (pQuantiteDisponibleAchat == null) {
      quantiteDisponibleAchat = pQuantiteDisponibleAchat;
      return;
    }
    if (nombreDecimaleStock != null) {
      quantiteDisponibleAchat = pQuantiteDisponibleAchat.setScale(nombreDecimaleStock, RoundingMode.HALF_UP);
    }
    else {
      quantiteDisponibleAchat = pQuantiteDisponibleAchat;
    }
  }
  
  public PrixFlash getPrixFlash() {
    return prixFlash;
  }
  
  public void setPrixFlash(PrixFlash prixFlash) {
    this.prixFlash = prixFlash;
  }
  
  public Boolean isDirectUsine() {
    return directUsine;
  }
  
  public void setDirectUsine(Boolean directUsine) {
    this.directUsine = directUsine;
  }
  
  public Boolean isAffichageMemoObligatoire() {
    return affichageMemoObligatoire;
  }
  
  public void setAffichageMemoObligatoire(Boolean affichageMemoObligatoire) {
    this.affichageMemoObligatoire = affichageMemoObligatoire;
  }
  
  public Boolean isTopEditionBonPreparation() {
    if (topEditionBonPreparation == null) {
      return false;
    }
    return topEditionBonPreparation;
  }
  
  public void setTopEditionBonPreparation(Boolean topEditionBonPreparation) {
    this.topEditionBonPreparation = topEditionBonPreparation;
  }
  
  public Boolean isTopAccuseReceptionCommande() {
    if (topAccuseReceptionCommande == null) {
      return false;
    }
    return topAccuseReceptionCommande;
  }
  
  public void setTopAccuseReceptionCommande(Boolean topAccuseReceptionCommande) {
    this.topAccuseReceptionCommande = topAccuseReceptionCommande;
  }
  
  public Boolean isTopBonExpedition() {
    if (topBonExpedition == null) {
      return false;
    }
    return topBonExpedition;
  }
  
  public void setTopBonExpedition(Boolean topBonExpedition) {
    this.topBonExpedition = topBonExpedition;
  }
  
  public Boolean isTopEditionBonTransporteur() {
    if (topEditionBonTransporteur == null) {
      return false;
    }
    return topEditionBonTransporteur;
  }
  
  public void setTopEditionBonTransporteur(Boolean topEditionBonTransporteur) {
    this.topEditionBonTransporteur = topEditionBonTransporteur;
  }
  
  public Boolean isTopFacture() {
    if (topFacture == null) {
      return false;
    }
    return topFacture;
  }
  
  public void setTopFacture(Boolean pTopFacture) {
    this.topFacture = pTopFacture;
    ri.serien.libcommun.outils.Trace.info(topFacture.toString());
  }
  
  public Boolean isTopDevis() {
    if (topDevis == null) {
      return false;
    }
    return topDevis;
  }
  
  public void setTopDevis(Boolean topDevis) {
    this.topDevis = topDevis;
  }
  
  /**
   * Retourner la quantité en stock chez le client.
   */
  public BigDecimal getQuantiteStockClient() {
    return quantiteStockClient;
  }
  
  public void setQuantiteStockClient(BigDecimal quantiteStockClient) {
    this.quantiteStockClient = quantiteStockClient;
  }
  
  public BigDecimal getPrixConsignation() {
    return prixConsignation;
  }
  
  public void setPrixConsignation(BigDecimal prixConsignation) {
    this.prixConsignation = prixConsignation;
  }
  
  public BigDecimal getPrixDeconsignation() {
    return prixDeconsignation;
  }
  
  public void setPrixDeconsignation(BigDecimal prixDeconsignation) {
    this.prixDeconsignation = prixDeconsignation;
  }
  
  public PrixAchat getPrixAchat() {
    return prixAchat;
  }
  
  public void setPrixAchat(PrixAchat prixAchat) {
    this.prixAchat = prixAchat;
  }
  
  public Boolean isArticleCharge() {
    return articleCharge;
  }
  
  public void setArticleCharge(Boolean pArticleCharge) {
    this.articleCharge = pArticleCharge;
  }
  
  /**
   * Retourne s'il s'agit d'un article découpable.
   */
  public boolean isArticleDecoupable() {
    return typeDecoupe.compareTo(EnumTypeDecoupeArticle.IMPOSSIBLE) > 0;
  }
  
  public EnumTypeDecoupeArticle getTypeDecoupe() {
    return typeDecoupe;
  }
  
  public void setTypeDecoupe(EnumTypeDecoupeArticle typeDecoupe) {
    this.typeDecoupe = typeDecoupe;
  }
  
  public BigDecimal getQuantiteStockChantier() {
    return quantiteStockChantier;
  }
  
  public void setQuantiteStockChantier(BigDecimal stockChantier) {
    this.quantiteStockChantier = stockChantier;
  }
  
  public BigDecimal getQuantiteStockMaximumUCA() {
    if (prixAchat != null && prixAchat.getNombreDecimaleUCA() != null) {
      return quantiteStockMaximumUCA.setScale(prixAchat.getNombreDecimaleUCA(), RoundingMode.HALF_UP);
    }
    return quantiteStockMaximumUCA;
  }
  
  public void setQuantiteStockMaximumUCA(BigDecimal quantiteStockMaximum) {
    this.quantiteStockMaximumUCA = quantiteStockMaximum;
  }
  
  public BigDecimal getQuantiteStockMinimumUCA() {
    if (prixAchat != null && prixAchat.getNombreDecimaleUCA() != null) {
      return quantiteStockMinimumUCA.setScale(prixAchat.getNombreDecimaleUCA(), RoundingMode.HALF_UP);
    }
    return quantiteStockMinimumUCA;
  }
  
  public void setQuantiteStockMinimumUCA(BigDecimal quantiteStockMinimum) {
    this.quantiteStockMinimumUCA = quantiteStockMinimum;
  }
  
  public BigDecimal getQuantiteStockIdealUCA() {
    if (prixAchat != null && prixAchat.getNombreDecimaleUCA() != null) {
      return quantiteStockIdealUCA.setScale(prixAchat.getNombreDecimaleUCA(), RoundingMode.HALF_UP);
    }
    return quantiteStockIdealUCA;
  }
  
  public void setQuantiteStockIdealUCA(BigDecimal quantiteStockIdeal) {
    this.quantiteStockIdealUCA = quantiteStockIdeal;
  }
  
  public BigDecimal getConsommationMoyenneUCA() {
    return consommationMoyenneUCA;
  }
  
  public void setConsommationMoyenneUCA(BigDecimal pConsommationMoyenne) {
    consommationMoyenneUCA = pConsommationMoyenne;
  }
  
  public Boolean getGestionPalettesParChantier() {
    return gestionPalettesParChantier;
  }
  
  public void setGestionPalettesParChantier(Boolean gestionPalettesParChantier) {
    this.gestionPalettesParChantier = gestionPalettesParChantier;
  }
  
  /**
   * Retourner si l'on doit alerter en cas de commande non multiple du conditionnement fournisseur
   */
  public boolean isAlerteConditionnementFournisseur() {
    return alerteConditionnementFournisseur != null && alerteConditionnementFournisseur;
  }
  
  public Boolean getAlerteConditionnementFournisseur() {
    return alerteConditionnementFournisseur;
  }
  
  /**
   * Modifier si l'on doit alerter en cas de commande non multiple du conditionnement fournisseur
   */
  public void setAlerteConditionnementFournisseur(Boolean alerteConditionnementFournisseur) {
    this.alerteConditionnementFournisseur = alerteConditionnementFournisseur;
  }
  
  /**
   * Retourner la quantité d'US par conditionnement fournisseur
   */
  public BigDecimal getQuantiteConditionnementFournisseurUS() {
    return quantiteConditionnementFournisseurUS;
  }
  
  /**
   * Retourner la quantité d'US par conditionnement fournisseur
   */
  public void setQuantiteConditionnementFournisseurUS(BigDecimal quantiteConditionnementFournisseurUS) {
    this.quantiteConditionnementFournisseurUS = quantiteConditionnementFournisseurUS;
  }
  
  /**
   * retourner l'unité de stockage de l'article
   */
  public Unite getUniteStock() {
    return uniteStock;
  }
  
  /**
   * Modifier l'unité de stockage de l'article
   */
  public void setUniteStock(Unite uniteStock) {
    this.uniteStock = uniteStock;
  }
  
  /**
   * Classe de rotation de l'article.
   * 
   * La classe de rotation permet de répartir les articles suivant leur rotation de stock. Noter que la classe de
   * rotation 'R' est
   * utilisée pour les articles qui n'ont pas de stock, pour lesquels la rotation n'a pas de sens (ce sont les articles
   * hors gamme).
   */
  public Character getClasseRotation() {
    return classeRotation;
  }
  
  /**
   * Modifier la classe de rotation de l'article.
   */
  public void setClasseRotation(Character pClasseRotation) {
    classeRotation = pClasseRotation;
  }
  
  /**
   * Retourne s'il s'agit d'un article en gamme (classe de rotation différente de 'R')..
   */
  public boolean isGamme() {
    return !isHorsGamme();
  }
  
  /**
   * Indique s'il s'agit d'un article hors gamme (classe de rotation égale à 'R').
   */
  public boolean isHorsGamme() {
    return Constantes.equals(classeRotation, CLASSE_ROTATION_HORS_GAMME);
  }
  
  /**
   * retourne le nombre de décimales associées au stocks physique, réservé, affecté,
   * attendu, disponible achat et disponible vente de l'article
   * Ce nombre de décimales est stocké dans l'article Série N et dépend directement de l'unité de stock qui lui est
   * associée
   */
  public Integer getNombreDecimaleStock() {
    return nombreDecimaleStock;
  }
  
  /**
   * Met à jour le nombre de décimales associées au stocks physique, réservé, affecté,
   * attendu, disponible achat et disponible vente de l'article
   * Ce nombre de décimales est stocké dans l'article Série N et dépend directement de l'unité de stock qui lui est
   * associée
   */
  public void setNombreDecimaleStock(Integer pNombreDecimaleStock) {
    nombreDecimaleStock = pNombreDecimaleStock;
  }
  
  /**
   * Met à jour le nombre de décimales associées au stocks physique, réservé, affecté,
   * attendu, disponible achat et disponible vente de l'article
   * Ce nombre de décimales est stocké dans l'article Série N et dépend directement de l'unité de stock qui lui est
   * associée
   * Version de la méthode native à partir d'un bigDecimal
   */
  public void setNombreDecimaleStock(BigDecimal pNombreDecimaleStock) {
    if (pNombreDecimaleStock == null) {
      return;
    }
    nombreDecimaleStock = pNombreDecimaleStock.intValue();
  }
  
  /**
   * Retourner le prix de revient standard HT de l'article.
   * 
   * @return
   */
  public BigDecimal getPrixDeRevientStandardHT() {
    return prixDeRevientStandardHT;
  }
  
  /**
   * Modifier le prix de revient standard HT de l'article.
   * 
   * @param pPrixDeRevientStandardHT
   */
  public void setPrixDeRevientStandardHT(BigDecimal pPrixDeRevientStandardHT) {
    this.prixDeRevientStandardHT = pPrixDeRevientStandardHT;
  }
  
  /**
   * Retourner le prix de revient HT de l'article.
   * 
   * @return
   */
  public BigDecimal getPrixDeRevientHT() {
    return prixDeRevientHT;
  }
  
  /**
   * Modifier le prix de revient HT de l'article.
   * 
   * @param pPrixDeRevientHT
   */
  public void setPrixDeRevientHT(BigDecimal pPrixDeRevientHT) {
    this.prixDeRevientHT = pPrixDeRevientHT;
  }
  
  /**
   * 
   * Retourner si l'article est géré par lots.
   * 
   * @return true si géré par lot
   */
  public boolean isArticleLot() {
    return !Constantes.normerTexte(getTopGestionParLots()).isEmpty();
  }
  
}
