/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.sousfamille;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.famille.IdFamille;
import ri.serien.libcommun.gescom.personnalisation.groupe.IdGroupe;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une sous famille.
 *
 * L'identifiant est composé du code établissement et du code de la sous famille. Le code sous famille correspond au paramètre SF du menu
 * des ventes ou des achats. Il est constitué de 1 à 5 caractères et se présente sous la forme AXXXX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdSousFamille extends AbstractIdAvecEtablissement {
  private static final int LONGUEUR_CODE_MIN = 1;
  private static final int LONGUEUR_CODE_MAX = 5;
  
  // Variables
  private final String code;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdSousFamille(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdSousFamille getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new IdSousFamille(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  /**
   * Contrôler la validité du code de la sous famille.
   * Le code acheteur est une chaîne alphanumérique de 1 à 5 caractères.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code de la sous famille n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MAX) {
      throw new MessageErreurException("Le code de la sous famille doit comporter entre " + LONGUEUR_CODE_MIN + " et " + LONGUEUR_CODE_MAX
          + " caractères : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdSousFamille controlerId(IdSousFamille pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du code de la sous famille est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du code de la sous famille n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdSousFamille)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de sous famille.");
    }
    IdSousFamille id = (IdSousFamille) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdSousFamille)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdSousFamille id = (IdSousFamille) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
  // -- Accesseurs
  
  /**
   * Code sous famille.
   * Le code de la sous famille est une chaîne alphanumérique de 1 à 5 caractères.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Identifiant du groupe de la sous-famille.
   * Le code du groupe est déduit du premier caractère du code de la sous-famille.
   * Noter que cette règle n'est vrai que si la sous-famille est liée hiérarchiquement à la famille et et groupe (dépend d'un PS).
   */
  public IdGroupe getIdGroupe() {
    return IdGroupe.getInstance(getIdEtablissement(), "" + getCode().charAt(0));
  }
  
  /**
   * Indiquer si la sous-famille appartient au groupe fourni en paramètre.
   */
  public boolean isDansGroupe(IdGroupe pIdGroupe) {
    if (pIdGroupe == null) {
      return false;
    }
    return pIdGroupe.equals(getIdGroupe());
  }
  
  /**
   * Identifiant de la famille de la sous-famille.
   * Le code de la famille est déduit des 3 premiers caractères du code de la sous-famille.
   * Noter que cette règle n'est vrai que si la sous-famille est liée hiérarchiquement à la famille et et groupe (dépend d'un PS).
   */
  public IdFamille getIdFamille() {
    if (getCode().length() < 3) {
      return null;
    }
    return IdFamille.getInstance(getIdEtablissement(), getCode().substring(0, 3));
  }
  
  /**
   * Indiquer si la sous-famille appartient à la famille fournie en paramètre.
   */
  public boolean isDansFamille(IdFamille pIdFamille) {
    if (pIdFamille == null) {
      return false;
    }
    return pIdFamille.equals(getIdFamille());
  }
}
