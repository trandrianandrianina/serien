/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.motifretour;

import java.math.BigDecimal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

public class MotifRetour extends AbstractClasseMetier<IdMotifRetour> {
  // Variables
  private String libelle = null;
  private String titreColonne = null;
  private BigDecimal incrementPostesComptabilisation = null;
  private IdMagasin magasinTransfert = null;
  private boolean isRetour = false;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public MotifRetour(IdMotifRetour pIdMotifRetour) {
    super(pIdMotifRetour);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  // -- Accesseurs
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  public String getTitreColonne() {
    return titreColonne;
  }
  
  public void setTitreColonne(String titreColonne) {
    this.titreColonne = titreColonne;
  }
  
  public BigDecimal getIncrementPostesComptabilisation() {
    return incrementPostesComptabilisation;
  }
  
  public void setIncrementPostesComptabilisation(BigDecimal incrementPostesComptabilisation) {
    this.incrementPostesComptabilisation = incrementPostesComptabilisation;
  }
  
  public IdMagasin getMagasinTransfert() {
    return magasinTransfert;
  }
  
  public void setMagasinTransfert(IdMagasin magasinTransfert) {
    this.magasinTransfert = magasinTransfert;
  }
  
  public boolean isRetour() {
    return isRetour;
  }
  
  public void setRetour(boolean isRetour) {
    this.isRetour = isRetour;
  }
}
