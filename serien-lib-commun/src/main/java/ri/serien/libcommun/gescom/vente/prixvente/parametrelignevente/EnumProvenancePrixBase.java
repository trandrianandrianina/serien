/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des provenances possibles du prix de base (HT et TTC) d'une ligne de vente.
 * 
 * Cette information est stockée dans le champ L1TB de la ligne de vente.Lors d'une création de ligne de vente, les valeurs peuvent
 * être initialisées avec les données du client, de l’article, des conditions de ventes, de l'entête... A chaque type de valeur est
 * associé un indicateur qui renseigne sur la provenance de la valeur.
 * 
 * Voici les valeurs pour le prix de base (champ L1TB) :
 * 0 = ???
 * 1 = Prix de base issu du tarif article,
 * 2 = Prix de base de la condition de vente.
 * 3 = Prix de base saisi manuellement dans la ligne de vente.
 * 
 * Si l'indicateur est égal à 3, cela signifie que le prix de base a été explicitement saisi par l’utilisateur dans la ligne de
 * vente. Si l'indicateur est différent de 3, cela signifie que le prix de base est issu d’un calcul précédent et qu’il faut
 * ignorer la valeur.
 */
public enum EnumProvenancePrixBase {
  DEFAUT(0, "Aucune"),
  TARIF_ARTICLE(1, "Tarif article"),
  CONDITION_VENTE(2, "Condition de vente"),
  SAISIE_LIGNE_VENTE(3, "Saisie ligne de vente");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumProvenancePrixBase(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumProvenancePrixBase valueOfByCode(Integer pNumero) {
    for (EnumProvenancePrixBase value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("La provenance du prix de base de la ligne de vente  est invalide : " + pNumero);
  }
  
}
