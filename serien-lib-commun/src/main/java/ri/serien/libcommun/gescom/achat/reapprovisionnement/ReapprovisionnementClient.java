/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.reapprovisionnement;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;

public class ReapprovisionnementClient implements Serializable {
  // Variables
  private IdLigneVente idLigneVente = null;
  private IdDocumentVente idDocumentVente = null;
  private Client client = null;
  private Date dateLivraisonPrevue = null;
  private Article article = null;
  private BigDecimal quantiteCommandeeLigneVente = null;
  private BigDecimal quantiteStock = null;
  private BigDecimal quantiteCommandesClients = null;
  private BigDecimal quantiteReserveesClients = null;
  private BigDecimal quantiteAttendue = null;
  private BigDecimal quantiteDisponibleAchat = null;
  private BigDecimal quantiteStockMaximum = null;
  
  // -- Accesseurs
  
  public IdLigneVente getIdLigneVente() {
    return idLigneVente;
  }
  
  public void setIdLigneVente(IdLigneVente idLigneVente) {
    this.idLigneVente = idLigneVente;
  }
  
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  public void setIdDocumentVente(IdDocumentVente idDocumentVente) {
    this.idDocumentVente = idDocumentVente;
  }
  
  public Client getClient() {
    return client;
  }
  
  public void setClient(Client client) {
    this.client = client;
  }
  
  public Date getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  public void setDateLivraisonPrevue(Date dateLivraisonPrevue) {
    this.dateLivraisonPrevue = dateLivraisonPrevue;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public void setArticle(Article article) {
    this.article = article;
  }
  
  public BigDecimal getQuantiteCommandeeLigneVente() {
    return quantiteCommandeeLigneVente;
  }
  
  public void setQuantiteCommandeeLigneVente(BigDecimal quantiteCommandeeLigneVente) {
    this.quantiteCommandeeLigneVente = quantiteCommandeeLigneVente;
  }
  
  public BigDecimal getQuantiteStock() {
    return quantiteStock;
  }
  
  public void setQuantiteStock(BigDecimal quantiteStock) {
    this.quantiteStock = quantiteStock;
  }
  
  public BigDecimal getQuantiteCommandesClients() {
    return quantiteCommandesClients;
  }
  
  public void setQuantiteCommandesClients(BigDecimal quantiteCommandesClients) {
    this.quantiteCommandesClients = quantiteCommandesClients;
  }
  
  public BigDecimal getQuantiteReserveesClients() {
    return quantiteReserveesClients;
  }
  
  public void setQuantiteReserveesClients(BigDecimal quantiteReserveesClients) {
    this.quantiteReserveesClients = quantiteReserveesClients;
  }
  
  public BigDecimal getQuantiteAttendue() {
    return quantiteAttendue;
  }
  
  public void setQuantiteAttendue(BigDecimal quantiteAttendue) {
    this.quantiteAttendue = quantiteAttendue;
  }
  
  public BigDecimal getQuantiteDisponibleAchat() {
    return quantiteDisponibleAchat;
  }
  
  public void setQuantiteDisponibleAchat(BigDecimal quantiteDisponibleAchat) {
    this.quantiteDisponibleAchat = quantiteDisponibleAchat;
  }
  
  public BigDecimal getQuantiteStockMaximum() {
    return quantiteStockMaximum;
  }
  
  public void setQuantiteStockMaximum(BigDecimal quantiteStockMaximum) {
    this.quantiteStockMaximum = quantiteStockMaximum;
  }
  
}
