/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockmouvement;

import java.math.BigDecimal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.commun.id.AbstractId;

/**
 * Mouvement de stock d'un article pour un magasin.
 * L'article et le magasin sont stockés dans l'identifiant.
 */
public class StockMouvement extends AbstractClasseMetier<IdStockMouvement> {
  private EnumTypeStockMouvement type = null;
  private EnumOrigineStockMouvement origine = null;
  private Character codeEnteteDocument = null;
  private Integer numeroDocument = null;
  private Integer suffixeDocument = null;
  private Integer numeroLigne = null;
  private String libelleTiers = null;
  private BigDecimal quantite = BigDecimal.ZERO;
  private BigDecimal stockPhysique = BigDecimal.ZERO;
  
  /**
   * Constructeur avec l'identififant du stock (obligatoire).
   */
  public StockMouvement(IdStockMouvement pIdStockMouvement) {
    super(pIdStockMouvement);
  }
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  /**
   * Retourner le type de mouvement de stock.
   * @return Type du mouvement de stock.
   */
  public EnumTypeStockMouvement getType() {
    return type;
  }
  
  /**
   * Modifier le type de mouvement de stock.
   * @param pType Type du mouvement de stock.
   */
  public void setType(EnumTypeStockMouvement pType) {
    type = pType;
  }
  
  /**
   * Retourner l'origine du mouvement de stock.
   * @return Origine du mouvement de stock.
   */
  public EnumOrigineStockMouvement getOrigine() {
    return origine;
  }
  
  /**
   * Modifier l'origine du mouvement de stock.
   * @param pOrigine Origine du mouvement de stock.
   */
  public void setOrigine(EnumOrigineStockMouvement pOrigine) {
    origine = pOrigine;
  }
  
  /**
   * Version textuelle de l'identifiant du document associé au mouvement de stock.
   * @return Identifiant du document au format texte.
   */
  public String getTexteDocument() {
    if (suffixeDocument == null || numeroDocument == null || suffixeDocument == null) {
      return "";
    }
    return codeEnteteDocument.toString() + AbstractId.SEPARATEUR_ID + numeroDocument + AbstractId.SEPARATEUR_ID + suffixeDocument;
  }
  
  /**
   * Version textuelle de l'identifiant de ligne de document associée au mouvement de stock.
   * @return Identifiant de ligne au format texte.
   */
  public String getTexteLigneDocument() {
    if (numeroLigne == null) {
      return "";
    }
    return getTexteDocument() + AbstractId.SEPARATEUR_ID + numeroLigne;
  }
  
  /**
   * Entête du document associé au mouvement de stock.
   * @return Entête du document.
   */
  public Character getCodeEnteteDocument() {
    return codeEnteteDocument;
  }
  
  /**
   * Modifier l'entête du document associé au mouvement de stock.
   * @param codeEnteteDocument Entête du document.
   */
  public void setCodeEnteteDocument(Character pCodeEnteteDocument) {
    codeEnteteDocument = pCodeEnteteDocument;
  }
  
  /**
   * Numéro du document associé au mouvement de stock.
   * @return Numéro du document.
   */
  public Integer getNumeroDocument() {
    return numeroDocument;
  }
  
  /**
   * Modifier le numéro du document associé au mouvement de stock.
   * @param pNumeroDocument Numéro du document.
   */
  public void setNumeroDocument(Integer pNumeroDocument) {
    numeroDocument = pNumeroDocument;
  }
  
  /**
   * Suffixe du document associé au mouvement de stock.
   * @return Suffixe du document.
   */
  public Integer getSuffixeDocument() {
    return suffixeDocument;
  }
  
  /**
   * Modifier le suffixe du document associé au mouvement de stock.
   * @param pSuffixeDocument Suffixe du document.
   */
  public void setSuffixe(Integer pSuffixeDocument) {
    suffixeDocument = pSuffixeDocument;
  }
  
  /**
   * Numéro de ligne associée au mouvement de stock.
   * @return Numéro de ligne.
   */
  public Integer getNumeroLigne() {
    return numeroLigne;
  }
  
  /**
   * Modifier le numéro de ligne associée au mouvement de stock.
   * @param pNumeroLigne Numéro de ligne.
   */
  public void setNumeroLigne(Integer pNumeroLigne) {
    numeroLigne = pNumeroLigne;
  }
  
  /**
   * Retourner le libellé du tiers concerné par le mouvement de stock.
   * @return Libellé du tiers.
   */
  public String getLibelleTiers() {
    return libelleTiers;
  }
  
  /**
   * Modifier le libellé du tiers concerné par le mouvement de stock.
   * @param pLibelleTiers Libellé du tiers.
   */
  public void setLibelleTiers(String pLibelleTiers) {
    libelleTiers = pLibelleTiers;
  }
  
  /**
   * Retourner la quantité du mouvement de stock.
   * @return Quantité du mouvement de stock.
   */
  public BigDecimal getQuantite() {
    return quantite;
  }
  
  /**
   * Modifier la quantité mouvement de stock.
   * @param pQuantite Quantité du mouvement de stock.
   */
  public void setQuantite(BigDecimal pQuantite) {
    quantite = pQuantite;
  }
  
  /**
   * Retourner la quantité en entrée du mouvement de stock.
   * @return Quantité en entrée du mouvement de stock.
   */
  public BigDecimal getQuantiteEntree() {
    if (getQuantite() != null && getQuantite().compareTo(BigDecimal.ZERO) >= 0) {
      return getQuantite();
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourner la quantité en sortie du mouvement de stock.
   * @return Quantité en sortie du mouvement de stock.
   */
  public BigDecimal getQuantiteSortie() {
    if (getQuantite() != null && getQuantite().compareTo(BigDecimal.ZERO) < 0) {
      return getQuantite();
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourner le stock physique après le mouvement de stock.
   * @return Quantité en stock physique après le mouvement de stock.
   */
  public BigDecimal getStockPhysique() {
    return stockPhysique;
  }
  
  /**
   * Modifier le stock physique après mouvement de stock.
   * @param pStockPhysique Quantité en stock physique après le mouvement de stock.
   */
  public void setStockPhysique(BigDecimal pStockPhysique) {
    stockPhysique = pStockPhysique;
  }
}
