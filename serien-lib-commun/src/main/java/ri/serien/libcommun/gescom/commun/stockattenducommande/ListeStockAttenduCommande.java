/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockattenducommande;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceStock;

/**
 * Liste des attendus et commandés.
 * L'utilisation de cette classe est à privilégier dès qu'on manipule une liste de StockAttenduCommande.
 * 
 */
public class ListeStockAttenduCommande extends ListeClasseMetier<IdStockAttenduCommande, StockAttenduCommande, ListeStockAttenduCommande> {
  private Date dateJour = null;
  private Date dateMiniReappro = null;
  private BigDecimal stockPhysique = null;
  private BigDecimal stockCommande = null;
  private BigDecimal stockReserve = null;
  private BigDecimal stockAttendu = null;
  private BigDecimal stockAvantRupture = null;
  private Date dateRupturePossible = null;
  private Date dateFinRupture = null;
  
  /**
   * Constructeurs.
   */
  public ListeStockAttenduCommande() {
  }
  
  @Override
  public ListeStockAttenduCommande charger(IdSession pIdSession, List<IdStockAttenduCommande> pListeId) {
    return null;
  }
  
  @Override
  public ListeStockAttenduCommande charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return null;
  }
  
  /**
   * Charger les attendus et commandés suivant les critères de recherche.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCritereStockAttenduCommande Critères de recherche des attendus et commandés.
   * @return Liste d'attendus et de commandés.
   */
  public static ListeStockAttenduCommande chargerListeStockAttenduCommande(IdSession pIdSession,
      CritereStockAttenduCommande pCritereStockAttenduCommande) {
    return ManagerServiceStock.chargerListeStockAttenduCommande(pIdSession, pCritereStockAttenduCommande);
  }
  
  /**
   * Calculer les valeurs de stock déduites à partir des attendus/commandés.
   * 
   * @param pIdArticle Identifiant article.
   * @param pStockPhysique Stock physique actuel, avant le premier attendu/commandé.
   * @param pDateJour Date du jour.
   * @param pDateMiniReappro Date minimum de réapprovisionnement.
   */
  public void calculerStock(IdArticle pIdArticle, BigDecimal pStockPhysique, Date pDateJour, Date pDateMiniReappro) {
    // Mémoriser le stock physique
    stockPhysique = pStockPhysique;
    
    // Ajouter des lignes pour la date du jour et la date minimum de réapprovionnement
    ajouterDateJour(pIdArticle, pDateJour);
    ajouterDateMiniReappro(pIdArticle, pDateMiniReappro);
    
    // Calculer les valeurs de stock
    calculerStockCommande();
    calculerStockReserve();
    calculerStockAttendu();
    calculerStockTheoriqueADate();
    calculerStockAvantRupture();
    calculerDateRupturePossible();
    calculerDateFinRupture();
  }
  
  /**
   * Ajouter un attendu/commandé dans la liste pour la date du jour.
   * 
   * @param pIdArticle Identifiant article.
   * @param pDateJour Date du jour.
   */
  public void ajouterDateJour(IdArticle pIdArticle, Date pDateJour) {
    // Contrôler les paramètres
    IdArticle.controlerId(pIdArticle, true);
    
    // Mémoriser la valeur
    dateJour = pDateJour;
    
    // Supprimer les lignes existantes s'il y en a déjà
    for (int i = 0; i < size(); i++) {
      if (get(i).getId().isDateJour()) {
        remove(i);
      }
    }
    
    // Générer une erreur car l'algorithme a besoin d'une date du jour
    if (dateJour == null) {
      throw new MessageErreurException("La date du jour est invalide.");
    }
    
    // Parcourir les attendus et commandés afin trouver la position d'insertion de la date
    // On recherche la première ligne avec une date supérieure ou égale à la date
    int i = 0;
    while (i < size() && dateJour.compareTo(get(i).getDateReference()) > 0) {
      i++;
    }
    
    // Créer un attendu/commandé pour la date du jour
    IdStockAttenduCommande idStockAttenduCommande = IdStockAttenduCommande.getInstancePourDateJour(pIdArticle);
    StockAttenduCommande stockAttenduCommande = new StockAttenduCommande(idStockAttenduCommande);
    stockAttenduCommande.setDateLivraisonPrevue(pDateJour);
    
    // Insérer la date du jour avant l'élément courant
    add(i, stockAttenduCommande);
    return;
  }
  
  /**
   * Ajouter un attendu/commandé dans la liste pour la date minimum de réapprovisionnement.
   * 
   * @param pIdArticle Identifiant article.
   * @param pDateMiniReappro Date minimum de réapprovisionnement.
   */
  public void ajouterDateMiniReappro(IdArticle pIdArticle, Date pDateMiniReappro) {
    // Contrôler les paramètres
    IdArticle.controlerId(pIdArticle, true);
    
    // Mémoriser la valeur
    dateMiniReappro = pDateMiniReappro;
    
    // Supprimer les lignes existantes s'il y en a déjà
    for (int i = 0; i < size(); i++) {
      if (get(i).getId().isDateMiniReappro()) {
        remove(i);
      }
    }
    
    // Ne pas ajouter de ligne pour la date mini de réappro s'il n'y en a pas
    // Cela arrive pour les articles sans conditions d'achats
    if (dateMiniReappro == null) {
      return;
    }
    
    // Parcourir les attendus et commandés afin trouver la position d'insertion de la date
    // On recherche la première ligne avec une date supérieure ou égale à la date
    int i = 0;
    while (i < size() && dateMiniReappro.compareTo(get(i).getDateReference()) > 0) {
      i++;
    }
    
    // Créer un attendu/commandé pour la date du jour
    IdStockAttenduCommande idStockAttenduCommande = IdStockAttenduCommande.getInstancePourDateMiniReappro(pIdArticle);
    StockAttenduCommande stockAttenduCommande = new StockAttenduCommande(idStockAttenduCommande);
    stockAttenduCommande.setDateLivraisonPrevue(pDateMiniReappro);
    
    // Insérer la date du jour avant l'élément courant
    add(i, stockAttenduCommande);
    return;
  }
  
  /**
   * Calculer le cumul du stock commandé.
   */
  private void calculerStockCommande() {
    // Initialiser le cumul
    stockCommande = BigDecimal.ZERO;
    
    // Parcourir les attendus/commandés
    for (StockAttenduCommande stockAttenduCommande : this) {
      if (stockAttenduCommande.getId().getTypeAttenduCommande().equals(EnumTypeAttenduCommande.COMMANDE)
          && stockAttenduCommande.getQuantiteSortie() != null) {
        stockCommande = stockCommande.add(stockAttenduCommande.getQuantiteSortie());
      }
    }
  }
  
  /**
   * Calculer le cumul du stock réservé.
   */
  private void calculerStockReserve() {
    // Initialiser le cumul
    stockReserve = BigDecimal.ZERO;
    
    // Parcourir les attendus/commandés
    for (StockAttenduCommande stockAttenduCommande : this) {
      if (stockAttenduCommande.getId().getTypeAttenduCommande().equals(EnumTypeAttenduCommande.COMMANDE)
          && stockAttenduCommande.isReserve() && stockAttenduCommande.getQuantiteSortie() != null) {
        stockReserve = stockReserve.add(stockAttenduCommande.getQuantiteSortie());
      }
    }
  }
  
  /**
   * Calculer le cumul du stock attendu.
   */
  private void calculerStockAttendu() {
    // Initialiser le cumul
    stockAttendu = BigDecimal.ZERO;
    
    // Parcourir les attendus/commandés
    for (StockAttenduCommande stockAttenduCommande : this) {
      if (stockAttenduCommande.getId().getTypeAttenduCommande().equals(EnumTypeAttenduCommande.ATTENDU)
          && stockAttenduCommande.getQuantiteEntree() != null) {
        stockAttendu = stockAttendu.add(stockAttenduCommande.getQuantiteEntree());
      }
    }
  }
  
  /**
   * Calculer le stock théorique à date pour chaque attendu/commandé.
   * 
   * Le stock physique doit avoir été renseigné.
   */
  private void calculerStockTheoriqueADate() {
    // Vérifier les pré-requis
    if (stockPhysique == null) {
      throw new MessageErreurException(
          "Il est obligatoire de fournir le stock physique pour calculer le stock théorique à date des attendus et commandés");
    }
    
    // Initialiser le stock théorique à date avec le stock physique
    BigDecimal stockTheoriqueADate = stockPhysique;
    
    // Parcourir les attendus et commandés
    for (StockAttenduCommande stockAttenduCommande : this) {
      // Ajouter les quantités en entrée
      if (stockAttenduCommande.getQuantiteEntree() != null) {
        stockTheoriqueADate = stockTheoriqueADate.add(stockAttenduCommande.getQuantiteEntree());
      }
      
      // Sosutraire les quantités en sortie
      if (stockAttenduCommande.getQuantiteSortie() != null) {
        stockTheoriqueADate = stockTheoriqueADate.subtract(stockAttenduCommande.getQuantiteSortie());
      }
      
      // Mettre à jour le stock théorique
      stockAttenduCommande.setStockTheoriqueADate(stockTheoriqueADate);
    }
  }
  
  /**
   * Calculer la somme des commandés en retard et des commandés à venir avant le prochain attendu.
   * 
   * @return Total de commandés.
   */
  private BigDecimal calculerTotalCommandeAvantProchainAttendu() {
    BigDecimal totalCommande = BigDecimal.ZERO;
    for (StockAttenduCommande stock : this) {
      // Stopper la somme des commandés si on arrive à un attendu au delà de la date du jour
      if (stock.getId().isAttendu() && dateJour != null && stock.getDateReference().compareTo(dateJour) > 0) {
        break;
      }
      
      // Stopper la somme des commandés si on arrive à la date minimum de réapprovisionnement
      if (stock.getId().isDateMiniReappro()) {
        break;
      }
      
      // Sommer le commandé
      if (stock.getId().isCommande()) {
        totalCommande = totalCommande.add(stock.getQuantiteSortie());
      }
    }
    
    return totalCommande;
  }
  
  /**
   * Calculer le stock avant rupture.
   * 
   * L'algorithme calcule d'abord le stock restant pour chaque attendu. C'est la valeur du stock théorique juste avant réception de
   * l'attendu. Cela correspond au nombre de pièces qui restent en stock juste avant de recevoir la livraison fournisseur. Cela
   * représente un point bas dans l'évolution du stock. Noter que ce calcul part du principe que tous les attendus précédents ont été
   * réceptionnés y compris ceux actuellement en retard. Ici, la prochaine date de livraison possible est considérée comme un
   * attendu potentiel.
   * 
   * Le stock restant pour la date du jour est déterminé un peu différemment. Pour la date du jour, on part du principe qu'on ne sait
   * pas quand seront livrés les attendus en retard. Du coup, on les compte pas. Le restant à la date du jour est donc le stock physique
   * moins tous les commandés clients jusqu'au prochain attendu fournisseur.
   * 
   * Nous avons donc un stock restant pour chaque attendu et la date du jour. Le stock avant rupture est le stock restant le plus
   * petit de tous. Cela représente le nombre de pièces que l'on peut vendre sans provoquer de rupture dans le futur.
   */
  private void calculerStockAvantRupture() {
    // Vérifier les pré-requis
    if (stockPhysique == null) {
      throw new MessageErreurException("Il est obligatoire de fournir le stock physique pour calculer le stock avant rupture");
    }
    
    // Initialiser le stock avant rupture
    stockAvantRupture = null;
    
    // Parcourir tous les attendus/commandés à l'envers pour conserver le stock restant le plut petit en partant du futur
    for (int i = size() - 1; i >= 0; i--) {
      StockAttenduCommande stockAttenduCommande = get(i);
      
      // Exclure les lignes avant la date du jour et après la date minimum de réapprovisionnement
      if ((dateJour != null && stockAttenduCommande.getDateReference().compareTo(dateJour) < 0)
          || (dateMiniReappro != null && stockAttenduCommande.getDateReference().compareTo(dateMiniReappro) > 0)) {
        continue;
      }
      
      // Calculer uniquement pour les attendus, la date du jour et la date minimum de réapprovisionnement
      EnumTypeAttenduCommande typeAttenduCommande = stockAttenduCommande.getId().getTypeAttenduCommande();
      if (!typeAttenduCommande.equals(EnumTypeAttenduCommande.ATTENDU)
          && !typeAttenduCommande.equals(EnumTypeAttenduCommande.DATE_MINI_REAPPRO)
          && !typeAttenduCommande.equals(EnumTypeAttenduCommande.DATE_JOUR)) {
        continue;
      }
      
      // Calculer le stock restant avant attendu
      BigDecimal stockRestantAvantAttendu = null;
      if (typeAttenduCommande.equals(EnumTypeAttenduCommande.DATE_JOUR)) {
        // C'est le stock physique moins tous les commandés jusqu'au prochain attendu
        stockRestantAvantAttendu = stockPhysique.subtract(calculerTotalCommandeAvantProchainAttendu());
      }
      else {
        // Initialiser le stock restant avant attendu avec le stock théorique après réception de l'attendu
        if (stockAttenduCommande.getStockTheoriqueADate() != null) {
          stockRestantAvantAttendu = stockAttenduCommande.getStockTheoriqueADate();
        }
        else {
          stockRestantAvantAttendu = BigDecimal.ZERO;
        }
        
        // Soustraire la quantité de l'attendu de manière à avoir le stock théorique avant réception de l'attendu
        if (stockAttenduCommande.getQuantiteEntree() != null) {
          stockRestantAvantAttendu = stockRestantAvantAttendu.subtract(stockAttenduCommande.getQuantiteEntree());
        }
      }
      
      // Renseigner le stock restant avant attendu de la ligne
      stockAttenduCommande.setStockRestantAvantAttendu(stockRestantAvantAttendu);
      
      // Le stock restant avant attendu devient le stock avant rupture s'il est plus petit (ou s'il n'a pas été encore initialisé)
      if (stockRestantAvantAttendu != null && (stockAvantRupture == null || stockAvantRupture.compareTo(stockRestantAvantAttendu) > 0)) {
        stockAvantRupture = stockRestantAvantAttendu;
      }
      
      // Renseigner le stock avant rupture de la ligne
      stockAttenduCommande.setStockAvantRupture(stockAvantRupture);
    }
  }
  
  /**
   * Calculer la date de rupture possible.
   * 
   * La date de rupture possible a deux variantes suivant les valeurs de stocks:
   * - Si le stock théorique à date devient négatif, c'est la date où il devient négatif qui est prise en compte. Même si le stock
   * devient envore plus négatif dans le futur, c'est la première date avec un stock négatif qui est conservée.
   * - Si le stock théorique à date n'est jamais négatif, la date de rupture possible est la date où le stock restant atteint son minimum.
   * Si ce minimum est atteint plusieurs fois dans le temps, c'est la date la plus petite qui est prise en compte.
   */
  private void calculerDateRupturePossible() {
    dateRupturePossible = null;
    
    // Parcourir les attendus et commandés
    BigDecimal stockRestantAvantAttenduMinimum = null;
    for (StockAttenduCommande stockAttenduCommande : this) {
      // Exclure les lignes avant la date du jour et après la date minimum de réapprovisionnement
      if ((dateJour != null && stockAttenduCommande.getDateReference().compareTo(dateJour) < 0)
          || (dateMiniReappro != null && stockAttenduCommande.getDateReference().compareTo(dateMiniReappro) > 0)) {
        continue;
      }
      
      // Sortir de suite si le stock théorique à date est négatif
      // Dans ce cas de figure, la date de rupture possible est en réalité une date de rupture effective.
      // Pas la peine de chercher un stock restant minimum dans les attendus.
      if (stockAttenduCommande.getStockTheoriqueADate() != null
          && stockAttenduCommande.getStockTheoriqueADate().compareTo(BigDecimal.ZERO) < 0) {
        dateRupturePossible = stockAttenduCommande.getDateReference();
        return;
      }
      
      // Passer les lignes sans restant avant attendu
      if (stockAttenduCommande.getStockRestantAvantAttendu() == null) {
        continue;
      }
      
      // Conserver le stock restant minimum avant attendu ainsi que sa date
      // On continue à parcourir les attendus/commandés car il peut y avoir plus petit après.
      // Si on rencontre plusieurs fois la valeur minimum, c'est la date de la première valeur qui est retournée
      if (stockRestantAvantAttenduMinimum == null
          || stockAttenduCommande.getStockRestantAvantAttendu().compareTo(stockRestantAvantAttenduMinimum) < 0) {
        stockRestantAvantAttenduMinimum = stockAttenduCommande.getStockRestantAvantAttendu();
        dateRupturePossible = stockAttenduCommande.getDateReference();
      }
    }
  }
  
  /**
   * Calculer la date de fin de rupture.
   * 
   * La date de fin de rupture est la date de l'attendu qui remet le stock avant rupture en positif après que celui-ci est été négatif
   * ou nul.
   * 
   * L'attendu qui remet le stock en positif est celui qui correspond à la dernière valeur négative ou nulle d'un attendu. En effet, la
   * quantité d'un attendu n'étant pas compté pour le stock avant rupture le jour de la réception, elle est donc prise en compte pour
   * l'attendu suivant mais c'est bien à partir de sa réception qu'on a à nouveau du stock positif.
   * 
   * Uen valeur négative ou nulle à la date du jour doit être traitée différemment. Si l'attendu suivant remet le stock en positif, c'est
   * sa date qui est utilisée comme date de fin de rupture.
   * 
   * En résumé :
   * - Rupture uniquement sur la date du jour, la date de fin de rupture = date du prochain attendu après la date du jour.
   * - Plusieurs attendus en rupture, la date de fin de rupture = date du dernier attendu négatif ou nul.
   * - Plusieurs plages de rupture, la date de fin de rupture = date du dernier attendu négatif ou nul.
   * - Tous les attendus à venir sont en rupture, la date de fin de rupture = date minimum de réapprovisionnement.
   */
  private void calculerDateFinRupture() {
    dateFinRupture = null;
    
    // Parcourir les attendus et commandés
    boolean rupture = false;
    for (StockAttenduCommande stockAttenduCommande : this) {
      // Passer les lignes sans stock avant rupture
      if (stockAttenduCommande.getStockAvantRupture() == null) {
        continue;
      }
      
      // Tester si le stock avant rupture est négatif ou nul
      if (stockAttenduCommande.getStockAvantRupture().compareTo(BigDecimal.ZERO) <= 0) {
        // Mettre l'indicateur de rupture à vrai si le stock est négatif ou nul
        rupture = true;
        
        // Déterminer la dernière date où le stock est négatif ou nul (sauf si c'est la date du jour)
        if (!stockAttenduCommande.getId().isDateJour()) {
          dateFinRupture = stockAttenduCommande.getDateReference();
        }
      }
      // Tester si c'est une sortie de rupture
      else if (rupture) {
        // Mettre l'indicateur de rupture à faux si le stock n'est plus négatif
        rupture = false;
        
        // Si la date de fin de rupture n'est pas renseignée, c'est que la valeur négative précédente était la date du jour
        // On garde alors la date du premier attendu reçu après la date du jour comme date de fin de rupture
        if (dateFinRupture == null) {
          dateFinRupture = stockAttenduCommande.getDateReference();
        }
      }
    }
  }
  
  /**
   * Retourner le stock physique.
   * @return Stock physique.
   */
  public BigDecimal getStockPhysique() {
    return stockPhysique;
  }
  
  /**
   * Retourner le stock commandé.
   * 
   * Le stock commandé est la quantité d’articles commandés ou réservés par les clients, quelle que soit la date de commande ou de
   * réservation. Ce sont des articles sur lesquels un client a déjà mis une option. Pour déterminer la quantité commandée, il faut faire
   * le total des commandes clients avec l'état VAL ou RES. Les commandes clients « Direct usine » ne sont pas comptées dans les
   * commandés.
   * 
   * @return Stock commandé.
   */
  public BigDecimal getStockCommande() {
    return stockCommande;
  }
  
  /**
   * Retourner le stock réservé.
   * 
   * Le stock réservé est la quantité d'article réservée pour des clients. Pour réserver un article, il faut valider une commande grâce
   * à l’option « Réservation (RES) ». Pour déterminer la quantité réservée, il faut faire le total des commandes avec l'état RES.
   * Une quantité réservée ne peut être utilisée dans une autre vente si le paramétrage l’interdit (voir PS067). Noter que le stock
   * réservé est également comptabilisé dans le stock commandé.
   * 
   * @return Stock réservé.
   */
  public BigDecimal getStockReserve() {
    return stockReserve;
  }
  
  /**
   * Retourner le stock attendu.
   * 
   * Le stock attendu est la quantité d’articles commandés aux fournisseurs et non encore réceptionnés, quelle que soit la date de
   * commande. Ces commandes viendront incrémenter le stock physique de l'article lorsqu'elles seront réceptionnées. Les commandes
   * fournisseurs « Direct usine » ne sont pas comptées dans les attendus.
   * 
   * @return Stock attendu.
   */
  public BigDecimal getStockAttendu() {
    return stockAttendu;
  }
  
  /**
   * Retourner le stock avant rupture.
   * 
   * Quantité correspondant au stock avant rupture de l’article en fonction des attendus et commandés à venir.
   * Il faut appeler la méthode calculerStock() de cette classe afin que cette valeur soit renseignée.
   * 
   * @return Stock avant rupture.
   */
  public BigDecimal getStockAvantRupture() {
    return stockAvantRupture;
  }
  
  /**
   * Retourner la date de rupture possible..
   * 
   * Date à laquelle le stock de l’article passera en rupture (quantité négative ou nulle) si on vend une quantité d’articles supérieure
   * ou égale au stock avant rupture. Par exemple, si le stock minimum est atteint le 19/10/2022 avec 292 pièces, le stock avant rupture
   * sera 292 et la date de rupture possible sera le 19/10/2022.
   * Il faut appeler la méthode calculerStock() de cette classe afin que cette valeur soit renseignée.
   * 
   * @return Date de rupture possible.
   */
  public Date getDateRupturePossible() {
    return dateRupturePossible;
  }
  
  /**
   * Retourner la date de fin de rupture.
   * 
   * Date à laquelle le stock avant rupture repasse à une valeur positive. Cette date n’est renseignée que si le stock avant rupture
   * est négatif.
   * Il faut appeler la méthode calculerStock() de cette classe afin que cette valeur soit renseignée.
   * 
   * @return Date de fin de rupture.
   */
  public Date getDateFinRupture() {
    return dateFinRupture;
  }
}
