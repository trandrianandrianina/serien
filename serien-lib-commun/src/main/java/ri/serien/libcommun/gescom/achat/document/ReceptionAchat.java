/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseur;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;

/**
 * Cette classe va regrouper les variables communes pour les différentes lignes possibles: ligneAchatArticle, ligneAchatCommentaire
 */
public class ReceptionAchat extends AbstractClasseMetier<IdReceptionAchat> {
  // Variables
  private IdArticle idArticle = null;
  private Integer numeroReception = null;
  private Date dateReception = null;
  private IdFournisseur idFournisseur = null;
  private String references = null;
  private BigDecimal quantiteUCA = null;
  private BigDecimal prixAchat = null;
  private String acheteur = null;
  private IdUnite idUniteAchat = null;
  private Boolean isCharge = null;
  
  // Type de l'article contenu dans la ligne
  protected EnumTypeArticle typeArticle = null;
  
  // A compléter
  
  /**
   * Constructeur
   */
  public ReceptionAchat(IdReceptionAchat pIdReceptionAchat) {
    super(pIdReceptionAchat);
  }
  
  @Override
  public String getTexte() {
    if (references == null) {
      return "";
    }
    return getReferences();
  }
  
  /**
   * Permet de comparer avec une autre ligne article.
   */
  public int compareTo(LigneAchatArticle pLigneArticleAchat) {
    return id.getNumeroLigne().compareTo(pLigneArticleAchat.getId().getNumeroLigne());
  }
  
  /**
   * Retourne s'il s'agit d'une ligne commentaire.
   */
  public boolean isLigneCommentaire() {
    return typeArticle != null && typeArticle.equals(EnumTypeArticle.COMMENTAIRE);
  }
  
  /**
   * Retourne le nom du fournisseur formaté sous forme de chaîne de caractères.
   */
  public String getFormaterNomFournisseur(ListeFournisseur pListeFournisseur) {
    if (idFournisseur == null || pListeFournisseur == null) {
      return "";
    }
    return pListeFournisseur.retournerNomFournisseurParId(idFournisseur);
  }
  
  /**
   * Retourne s'il s'agit d'une ligne regroupée (TODO).
   */
  public boolean isLigneRegroupee() {
    return true;
  }
  
  // -- Accesseurs
  
  public EnumTypeArticle getTypeArticle() {
    return typeArticle;
  }
  
  public void setTypeArticle(EnumTypeArticle typeArticle) {
    this.typeArticle = typeArticle;
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public void setIdArticle(IdArticle idArticle) {
    this.idArticle = idArticle;
  }
  
  public Integer getNumeroReception() {
    return numeroReception;
  }
  
  public void setNumeroReception(Integer numeroReception) {
    this.numeroReception = numeroReception;
  }
  
  public Date getDateReception() {
    return dateReception;
  }
  
  public void setDateReception(Date dateReception) {
    this.dateReception = dateReception;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur pIdFournisseur) {
    idFournisseur = pIdFournisseur;
  }
  
  public String getReferences() {
    return references;
  }
  
  public void setReferences(String references) {
    this.references = references;
  }
  
  public BigDecimal getQuantiteUCA() {
    return quantiteUCA;
  }
  
  public void setQuantiteUCA(BigDecimal pQuantiteUCA) {
    quantiteUCA = pQuantiteUCA;
  }
  
  public BigDecimal getPrixAchat() {
    return prixAchat;
  }
  
  public void setPrixAchat(BigDecimal prixAchat) {
    this.prixAchat = prixAchat;
  }
  
  public String getAcheteur() {
    return acheteur;
  }
  
  public void setAcheteur(String acheteur) {
    this.acheteur = acheteur;
  }
  
  public IdUnite getIdUniteAchat() {
    return idUniteAchat;
  }
  
  public void setIdUniteAchat(IdUnite idUnite) {
    this.idUniteAchat = idUnite;
  }
  
  @Override
  public boolean isCharge() {
    return isCharge;
  }
  
  public void setIsCharge(Boolean isCharge) {
    this.isCharge = isCharge;
  }
  
}
