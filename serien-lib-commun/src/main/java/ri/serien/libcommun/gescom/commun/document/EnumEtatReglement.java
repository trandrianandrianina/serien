/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Les différents types d'adresse fournisseur possibles.
 */
public enum EnumEtatReglement {
  REGLE('R', "R\u00e9gl\u00e9"),
  NON_REGLE('N', "Non r\u00e9gl\u00e9"),
  PARTIELLEMENT_REGLE('P', "Partiellement r\u00e9gl\u00e9");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumEtatReglement(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans uen chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumEtatReglement valueOfByCode(Character pCode) {
    for (EnumEtatReglement value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'état du réglement est invalide : " + pCode);
  }
}
