/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.negociation;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste utilisée pour identifier la raison de l'invalidité d'un prix saisi.
 * 
 * PRIX_NET_NEGATIF : Saisie d'un prix négatif
 * PRIX_NET_SUPERIEUR_PRIX_PUBLIC : Saisie d'un prix supérieur au prix public
 * PRIX_NET_INFERIEUR_PRIX_REVIENT : Saisie d'un prix inférieur au prix de revient
 */
public enum EnumTypePrixSaisiInvalide {
  PRIX_NET_NEGATIF(0, "Le prix net saisi est négatif."),
  PRIX_NET_SUPERIEUR_PRIX_PUBLIC(1, "Le prix net saisi est supérieur au prix public. "),
  PRIX_NET_INFERIEUR_PRIX_REVIENT(2, "Le prix net saisi est inférieur au prix de revient. ");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypePrixSaisiInvalide(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(numero);
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumTypePrixSaisiInvalide valueOfByCode(Integer pNumero) {
    for (EnumTypePrixSaisiInvalide value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type du prix saisi invalide n'existe pas : " + pNumero);
  }
  
}
