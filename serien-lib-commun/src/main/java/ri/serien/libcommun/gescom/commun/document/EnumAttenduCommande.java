/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de filtre sur les lignes de ventes contenants des articles qui concernent les attendues et/ou commandées.
 */
public enum EnumAttenduCommande {
  ATTENDU_ET_COMMANDE(1, "Attendu et commandé"),
  ATTENDU_FOURNISSEUR(2, "Attendu fournisseur"),
  COMMANDE_CLIENT(3, "Commande client"),
  COMMANDE_CLIENT_RESERVEE(4, "Commande client réservéee uniquement");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumAttenduCommande(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le code converti en caractère.
   */
  public Character getConvertirCodeEnCaractere() {
    return Character.forDigit(code.intValue(), 10);
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return code.toString();
  }
  
  /**
   * Retourner l'objet enum par son numéro.
   */
  static public EnumAttenduCommande valueOfByCode(Integer pCode) {
    for (EnumAttenduCommande value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de valeur est inconnu : " + pCode);
  }
  
}
