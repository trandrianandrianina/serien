/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.chantier;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Etat d'un chantier.
 */
public enum EnumStatutChantier {
  ATTENTE(0, "En attente"),
  VALIDE(1, "Validé"),
  EDITE(2, "Edité"),
  VALIDITE_DEPASSEE(4, "Validité dépassée"),
  CLOTURE(6, "Clôturé");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumStatutChantier(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumStatutChantier valueOfByCode(Integer pCode) {
    for (EnumStatutChantier value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'état du chantier est invalide : " + pCode);
  }
}
