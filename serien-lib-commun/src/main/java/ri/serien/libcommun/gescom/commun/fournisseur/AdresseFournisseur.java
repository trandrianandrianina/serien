/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;

/**
 * Un fournisseur peut avoir plusieurs adresses autres que celle du siège: une de commande, une d'expédition, une de facturation et une
 * du représentant.
 */
public class AdresseFournisseur extends AbstractClasseMetier<IdAdresseFournisseur> {
  // Variables
  private EnumTypeAdresseFournisseur type = null;
  private Adresse adresse = null;
  private String numeroTelephone = null;
  private String numeroFax = null;
  private String numeroTelex = null;
  private String observations = "";
  
  /**
   * Constructeur avec l'identifiant
   */
  public AdresseFournisseur(IdAdresseFournisseur pIdAdresseFournisseur) {
    super(pIdAdresseFournisseur);
  }
  
  /**
   * Indique si l'adresse fournisseur est l'adresse du siège
   */
  public boolean isSiege() {
    if (type == null) {
      return false;
    }
    return type.equals(EnumTypeAdresseFournisseur.SIEGE);
  }
  
  @Override
  public String getTexte() {
    if (adresse != null) {
      return adresse.getNom();
    }
    else {
      return id.getTexte();
    }
  }
  
  // -- Accesseurs
  /**
   * renvoit le type de l'adresse fournisseur (SIEGE, COMMANDE, EXPEDITION, FACTURATION, REPRESENTANT)
   */
  public EnumTypeAdresseFournisseur getType() {
    return type;
  }
  
  /**
   * Fixe le type de l'adresse fournisseur
   */
  public void setType(EnumTypeAdresseFournisseur type) {
    this.type = type;
  }
  
  /**
   * renvoit le bloc adresse de l'adresse fournisseur
   */
  public Adresse getAdresse() {
    return adresse;
  }
  
  /**
   * fixe le bloc adresse de l'adresse fournisseur
   */
  public void setAdresse(Adresse adresse) {
    this.adresse = adresse;
  }
  
  /**
   * renvoit le numéro de téléphone de l'adresse fournisseur
   */
  public String getNumeroTelephone() {
    return numeroTelephone;
  }
  
  /**
   * fixe le numéro de téléphone de l'adresse fournisseur
   */
  public void setNumeroTelephone(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }
  
  /**
   * renvoit le numéro de fax de l'adresse fournisseur
   */
  public String getNumeroFax() {
    return numeroFax;
  }
  
  /**
   * fixe le numéro de fax de l'adresse fournisseur
   */
  public void setNumeroFax(String numeroFax) {
    this.numeroFax = numeroFax;
  }
  
  /**
   * renvoit le numéro de telex de l'adresse fournisseur
   */
  public String getNumeroTelex() {
    return numeroTelex;
  }
  
  /**
   * fixe le numéro de telex de l'adresse fournisseur
   */
  public void setNumeroTelex(String numeroTelex) {
    this.numeroTelex = numeroTelex;
  }
  
  /**
   * renvoit les observations de l'adresse fournisseur
   */
  public String getObservations() {
    return observations;
  }
  
  /**
   * fixe les observations de l'adresse fournisseur
   */
  public void setObservations(String observations) {
    this.observations = observations;
  }
}
