/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

public class CritereCategorieZonePersonnalisee implements Serializable {
  // Constantes
  
  // Variables
  private IdEtablissement idEtablissement = null;
  
  /**
   * Retourner l'identifiant de l'établissement
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Mettre à jour l'identifiant de l'établissement
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
}
