/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.famille;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.groupe.IdGroupe;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de familles.
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de familles. Elle contient toutes les
 * méthodes oeuvrant sur la liste tandis que la classe Famille contient les méthodes oeuvrant sur une seul famille.
 */

public class ListeFamille extends ListeClasseMetier<IdFamille, Famille, ListeFamille> {
  /**
   * Constructeur.
   */
  public ListeFamille() {
  }
  
  /**
   * Charger la liste des familles suivant une liste d'identifiant
   */
  @Override
  public ListeFamille charger(IdSession pIdSession, List<IdFamille> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charger toutes les familles actifs de l'établissement donné.
   */
  @Override
  public ListeFamille charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    IdSession.controlerId(pIdSession, true);
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    CritereFamille criteres = new CritereFamille();
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeFamille(pIdSession, criteres);
  }
  
  /**
   * Retourner le libellé de la famille à partir de son code.
   */
  public String retournerLibelle(IdFamille pId) {
    IdFamille.controlerId(pId, true);
    
    for (Famille famille : this) {
      if (Constantes.equals(famille.getId(), pId)) {
        return famille.getLibelle();
      }
    }
    return "";
  }
  
  /**
   * Retourner la liste des familles d'un groupe.
   */
  public ListeFamille getListeFamillePourGroupe(IdGroupe pIdGroupe) {
    IdGroupe.controlerId(pIdGroupe, true);
    
    // On sélectionne les familles qui appartiennent au groupe
    ListeFamille listeFamilleGroupe = new ListeFamille();
    for (Famille famille : this) {
      if (famille.getId() != null && pIdGroupe.getCode().charAt(0) == famille.getId().getCode().charAt(0)) {
        listeFamilleGroupe.add(famille);
      }
    }
    return listeFamilleGroupe;
  }
}
