/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.lienligne;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Les différents types de lignes liées possibles.
 */
public enum EnumTypeLienLigne {
  LIEN_LIGNE_VENTE('V', "Vente"),
  LIEN_LIGNE_ACHAT('A', "Achat"),
  LIEN_LIGNE_FABRICATION('F', "Fabrication");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeLienLigne(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans uen chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeLienLigne valueOfByCode(Character pCode) {
    for (EnumTypeLienLigne value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de ligne liée est invalide : " + pCode);
  }
}
