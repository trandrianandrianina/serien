/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.chantier;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceClient;

/**
 * Liste de Chantier.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de chantiers.
 */
public class ListeChantier extends ListeClasseMetier<IdChantier, Chantier, ListeChantier> {
  /**
   * Constructeur.
   */
  public ListeChantier() {
  }
  
  /**
   * Construire une liste de document de bases correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des documents de base ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeChantier creerListeNonChargee(List<IdChantier> pListeIdChantier) {
    ListeChantier listeChantier = new ListeChantier();
    if (pListeIdChantier != null) {
      for (IdChantier idChantier : pListeIdChantier) {
        Chantier chantier = new Chantier(idChantier);
        chantier.setCharge(false);
        listeChantier.add(chantier);
      }
    }
    return listeChantier;
  }
  
  @Override
  public ListeChantier charger(IdSession pIdSession, List<IdChantier> pListeIdChantier) {
    return ManagerServiceClient.chargerListeChantier(pIdSession, pListeIdChantier);
  }
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeChantier charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
}
