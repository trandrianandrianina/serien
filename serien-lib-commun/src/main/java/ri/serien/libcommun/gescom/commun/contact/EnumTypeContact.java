/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Les différents types de contacts possibles.
 */
public enum EnumTypeContact {
  DESACTIVE('9', "Désactivé"),
  FOURNISSEUR('F', "Fournisseur"),
  CLIENT('C', "Client"),
  PROSPECT('P', "Prospect"),
  AUXILIAIRE('A', "Compte auxiliaire");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeContact(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans uen chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeContact valueOfByCode(Character pCode) {
    for (EnumTypeContact value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de contact est invalide : " + pCode);
  }
}
