/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de quantités.
 * 
 * Une valeur de cette énumération peut être associée à une quantité saisie afin de savoir si cette quantité a été saisie :
 * - En unité de consitionnement de ventes (UCV) : c'est la saisie par défaut.
 * - En unité de ventes (UV) : saisie d'une quantité suivie de la lettre 'm' ou 'v'.
 * - en unité de conditionnement de stocks (UCS): saisie d'une quantité suivie de la lettre 'p' ou 'c'.
 */
public enum EnumTypeQuantite {
  UNITE_CONDITIONNEMENT_VENTES(' ', "Unité de conditionnement de ventes"),
  UNITE_VENTES('m', "Unité de ventes"),
  UNITE_CONDITIONNEMENT_STOCKS('p', "Unité de conditionnement de stocks");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeQuantite(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return Integer.toString(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeQuantite valueOfByCode(Character pCode) {
    for (EnumTypeQuantite value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de saisie de quantités est invalide : " + pCode);
  }
  
}
