/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.groupe;

import java.math.BigDecimal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;

/**
 * Groupe d'article.
 * 
 * Un groupe d'articles est composé de plusieurs familles, qui elle-mêmes peuvent être décomposées en plusieurs sous-familles.
 * Le groupe d'article contient tous les paramètres qui s'appliquent à l'ensemble des articles d'un groupe.
 */
public class Groupe extends AbstractClasseMetier<IdGroupe> {
  private String libelle = "";
  private int codeTVA = 1;
  private IdUnite idUniteVente = null;
  private BigDecimal coefficientVente = null;
  private BigDecimal coefficientPRV = null;
  
  /**
   * Constructeur à partir de l'identifiant.
   */
  public Groupe(IdGroupe pIdgroupe) {
    super(pIdgroupe);
  }
  
  @Override
  public String getTexte() {
    if (libelle != null) {
      return libelle;
    }
    else {
      return id.getTexte();
    }
  }
  
  /**
   * Déterminer si une famille appartient à ce groupe.
   * Une famille appartient à un groupe lorsque la première lettre de son code est celle du groupe.
   */
  public boolean isAppartenir(Famille pFamille) {
    if (pFamille == null) {
      return false;
    }
    return pFamille.getId().getCode().startsWith(id.getCode());
  }
  
  // -- Accesseurs
  
  /**
   * Retourner le libellé du groupe.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé du groupe.
   */
  public void setLibelle(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Retourner le code TVA.
   */
  public int getCodeTVA() {
    return codeTVA;
  }
  
  /**
   * Modifier le code TVA.
   */
  public void setCodeTVA(int pCodeTVA) {
    codeTVA = pCodeTVA;
  }
  
  /**
   * Retourner l'identifiant de l'unité de ventes.
   */
  public IdUnite getIdUniteVente() {
    return idUniteVente;
  }
  
  /**
   * Modifier l'identifiant de l'unité de ventes.
   */
  public void setIdUniteVente(IdUnite pIdUniteVente) {
    idUniteVente = pIdUniteVente;
  }
  
  /**
   * Retourner le coefficient de vente.
   */
  public BigDecimal getCoefficientVente() {
    return coefficientVente;
  }
  
  /**
   * Modifier le coefficient de vente.
   */
  public void setCoefficientVente(BigDecimal pCoefficientVente) {
    coefficientVente = pCoefficientVente;
  }
  
  /**
   * Retourner le coefficient du prix de revient.
   */
  public BigDecimal getCoefficientPRV() {
    return coefficientPRV;
  }
  
  /**
   * Modifier le coefficient le prix de revient.
   */
  public void setCoefficientPRV(BigDecimal pCoefficientPRV) {
    coefficientPRV = pCoefficientPRV;
  }
}
