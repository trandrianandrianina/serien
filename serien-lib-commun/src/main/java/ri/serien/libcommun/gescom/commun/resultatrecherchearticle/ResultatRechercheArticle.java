/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.resultatrecherchearticle;

import java.math.BigDecimal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.Constantes;

/**
 * Ligne de résultat d'une recherche article.
 * 
 * Cette classe, utilisée par le comptoir, permet d'afficher le résultat lors d'une recherche article. En pratique, ce sont surtout des
 * articles standards qui sont affichés mais il peut y avoir également des articles particuliers comme des palettes. Cette classe est
 * en effet utilisée pour afficher le résultat lors d'une recherche d'article liée et les articles liés peuvent contenir des palettes.
 * 
 * L'identifiant est important car la première recherche retourne la liste complète mais uniquement avec l'identifiant de renseigné.
 * Lorsqu'une nouvelle page est affichée, la liste des identifiants est utilisée pour charger le reste des informations.
 * 
 * Voici la liste des colonnes du résultat d'une recherche article au comptoir :
 * 1) Qté
 * 2) UCV
 * 3) Libellé
 * 4) Stock disponible
 * 5) Prix net HT ou TTC
 * 6) UV
 * 7) UV/UCV
 * 8) Origine
 * 9) Observations
 */
public class ResultatRechercheArticle extends AbstractClasseMetier<IdResultatRechercheArticle> {
  // Divers
  private Boolean informationCharge = Boolean.FALSE;
  
  // Colonne 2 : UCV
  private IdUnite idUCV = null;
  
  // Colonne 3 : libellé article
  private String libelle1 = null;
  private String libelle2 = null;
  private String libelle3 = null;
  private String libelle4 = null;
  
  // Colonne 4 : stock disponible
  private BigDecimal stock = null;
  
  // Colonne 5 : prix net
  private BigDecimal prixNetHT = null;
  private BigDecimal prixNetTTC = null;
  
  // Colonne 6 : UV
  private IdUnite idUV = null;
  
  // Colonne 7 : UV/UCV
  private BigDecimal nombreUVParUCV = null;
  
  // Colonne 8 : Origine
  private String origine = null;
  
  // Colonne 9 : observations
  private String observation = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur avec l'identifiant de l'information de vente.
   */
  public ResultatRechercheArticle(IdResultatRechercheArticle pIdResultatRechercheArticle) {
    super(pIdResultatRechercheArticle);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  // --
  
  @Override
  public String getTexte() {
    return getLibelle1();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs communs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Tester si les informations de l'objet sont chargées.
   * @return true=informations chargées, false=sinon.
   */
  public Boolean isInformationCharge() {
    return informationCharge;
  }
  
  /**
   * Indiquer si les informations de l'objet sont chargées.
   * @param pCharge true=informations chargées, false=sinon.
   */
  public void setInformationCharge(Boolean pCharge) {
    this.informationCharge = pCharge;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs pours les colonnes du tableau de résultat d'un article standard
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'identifiant de l'unité de conditionnement de ventes.
   * Si l'article n'a pas d'unité de conditionnement de ventes, c'est l'unité de vente qui fait office d'unité de conditionnement de
   * ventes.
   * @return Identifiant unité de conditionnement de vente.
   */
  public IdUnite getIdUCV() {
    if (idUCV != null) {
      return idUCV;
    }
    else {
      return idUV;
    }
  }
  
  /**
   * Modifier l'identifiant de l'unité de conditionnement de ventes.
   * @param pIdUCV Identifiant unité de conditionnement de vente.
   */
  public void setIdUCV(IdUnite pIdUCV) {
    idUCV = pIdUCV;
  }
  
  /**
   * Retourner le libellé complet de l'article.
   * Cette méthode concaténe les 4 libellés articles en un seul libellé.
   */
  public String getLibelle() {
    if (libelle1 == null || libelle2 == null || libelle3 == null || libelle4 == null) {
      return null;
    }
    String libelle = "";
    if (libelle1 != null) {
      libelle += libelle1;
    }
    if (libelle2 != null) {
      libelle += libelle2;
    }
    if (libelle3 != null) {
      libelle += libelle3;
    }
    if (libelle4 != null) {
      libelle += libelle4;
    }
    return libelle;
  }
  
  /**
   * Retourner la première partie du libellé de l'article.
   */
  public String getLibelle1() {
    return libelle1;
  }
  
  /**
   * Modifier la première partie du libellé de l'article.
   */
  public void setLibelle1(String pLibelle) {
    libelle1 = pLibelle;
  }
  
  /**
   * Retourner la partie 2 du libellé de l'article.
   */
  public String getLibelle2() {
    return libelle2;
  }
  
  /**
   * Modifier la partie 2 du libellé de l'article.
   */
  public void setLibelle2(String pLibelle) {
    libelle2 = pLibelle;
  }
  
  /**
   * Retourner la partie 3 du libellé de l'article.
   */
  public String getLibelle3() {
    return libelle3;
  }
  
  /**
   * Modifier la partie 3 du libellé de l'article.
   */
  public void setLibelle3(String pLibelle) {
    libelle3 = pLibelle;
  }
  
  /**
   * Retourner la partie 4 du libellé de l'article.
   */
  public String getLibelle4() {
    return libelle4;
  }
  
  /**
   * Modifier la partie 4 du libellé de l'article.
   */
  public void setLibelle4(String pLibelle) {
    libelle4 = pLibelle;
  }
  
  /**
   * Retourner la quantité en stock de l'article.
   * @return Quantité en stock.
   */
  public BigDecimal getStock() {
    return stock;
  }
  
  /**
   * Modifier la quantité en stock de l'article.
   * 
   * C'est le stock disponible si le P269=1 et stock net si PS269 est vide. Cela utilise le nombre de décimales associées au stocks.
   * Ce nombre de décimales est stocké dans l'article et dépend directement de l'unité de stock qui lui est associée.
   * 
   * @param pStock Quantité en stock.
   */
  public void setStock(BigDecimal pStock) {
    stock = pStock;
  }
  
  /**
   * Retourner le prix net HT.
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetHT() {
    return prixNetHT;
  }
  
  /**
   * Modifier le prix net HT.
   * @param pPrixNetHT Prix net HT.
   */
  public void setPrixNetHT(BigDecimal pPrixNetHT) {
    prixNetHT = pPrixNetHT;
  }
  
  /**
   * Retourner le prix net TTC.
   * @return Prix net TTC.
   */
  public BigDecimal getPrixNetTTC() {
    return prixNetTTC;
  }
  
  public void setPrixNetTTC(BigDecimal pPrixNetTTC) {
    prixNetTTC = pPrixNetTTC;
  }
  
  /**
   * Modifier le prix net TTC.
   * @param pPrixNetHT Prix net TTC.
   */
  
  /**
   * Retourner le prix net HT ou TTC.
   * @param pIsTTC true=calcul TTC, false=calcul HT.
   * @return Prix net HT ou TTC.
   */
  public BigDecimal getPrixNet(Boolean pIsTTC) {
    if (pIsTTC && prixNetTTC != null) {
      return prixNetTTC;
    }
    else if (!pIsTTC && prixNetHT != null) {
      return prixNetHT;
    }
    return null;
  }
  
  /**
   * Retourner l'identifiant de l'unité de vente.
   * @return Identifiant de l'unité de vente.
   */
  public IdUnite getIdUV() {
    return idUV;
  }
  
  /**
   * Modifier l'identifiant de l'unité de vente.
   * @param pIdUV Identifiant de l'unité de vente.
   */
  public void setIdUV(IdUnite pIdUV) {
    idUV = pIdUV;
  }
  
  /**
   * Retourner le nombre d'unités de ventes (UV) par unités de commandes de ventes (UCV).
   * @return Nombre UV/UCV.
   */
  public BigDecimal getNombreUVParUCV() {
    return nombreUVParUCV;
  }
  
  /**
   * Modifier le nombre d'unités de ventes (UV) par unités de commandes de ventes (UCV).
   * @param pNombreUVParUCV Nombre UV/UCV.
   */
  public void setNombreUVParUCV(BigDecimal pNombreUVParUCV) {
    nombreUVParUCV = pNombreUVParUCV;
  }
  
  /**
   * Texte décrivant le nombre d'unités de ventes (UV) par unités de commandes de ventes (UCV).
   * Exemple : 3.00 M²/U
   */
  public String getTexteUVParUCV() {
    if (idUCV == null || idUV == null || idUCV.equals(idUV) || nombreUVParUCV == null || nombreUVParUCV.compareTo(BigDecimal.ZERO) == 0
        || nombreUVParUCV.compareTo(BigDecimal.ONE) == 0) {
      return null;
    }
    return Constantes.formater(nombreUVParUCV, false) + " " + idUV.getCode() + "/" + idUCV.getCode();
  }
  
  /**
   * Retourner l'origine du prix de vente.
   * 
   * La colonne origine affichée dnas le comptoir est construite suivant diverses règles de gestion. L'origine peut prendre les valeurs
   * suivantes :
   * - Négocié : si une des valeurs permettant de calculer le prix de vente est modifiée manuellement dans la ligne de vente.
   * - Chantier : si le prix affiché est issu d'un prix chantier.
   * - Public : si c'est le prix standard qui est affiché et que ce prix standard a été calculé à partir de la colonne 1.
   * - Colonne [n] : si c'est le prix standard qui est affiché et que ce prix standard a été calculé à partir de la colonne n.
   * - CNV [code] : si le prix est issu d'une condition de vente.
   * - Consignation : si le prix est celui d'un article consigné.
   * 
   * @return Texte résumant l'origine du prix de vente.
   */
  public String getOrigine() {
    return origine;
  }
  
  /**
   * Modifier l'origine du prix de vente.
   * @param pOrigine Texte résumant l'origine du prix de vente.
   */
  public void setOrigine(String pOrigine) {
    origine = pOrigine;
  }
  
  /**
   * Retourner l'observation.
   * @return Texte de l'observation.
   */
  public String getObservation() {
    return observation;
  }
  
  /**
   * Modifier l'observation.
   * L'observatino doit être un texte assez court car elle est affichée en n=9ème et derniere colonne du tableau de résultat.
   * @param pObservation Texte de l'observation.
   */
  public void setObservation(String pObservation) {
    observation = Constantes.normerTexte(pObservation);
  }
}
