/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import java.io.Serializable;

import ri.serien.libcommun.outils.Constantes;

/**
 * Champ de catalogue
 * Cette classe permet de décrire la configuration de l'import/export d'un catalogue articles fournisseur via un fichier
 * On y décrit le fournisseur concerné, le fichier à récupérer ainsi que le mapping des zones entre Série N et le fichier
 */
public class ChampCatalogue implements Serializable, Cloneable {
  private Integer ordre = 0;
  private String nom = null;
  private String valeurExemple = null;
  private Integer debutDecoupe = null;
  private Integer finDecoupe = null;
  
  /**
   * Comparer l'intégralité des données.
   */
  public static boolean equalsComplet(ChampCatalogue pObjet1, ChampCatalogue pObjet2) {
    if (pObjet1 == pObjet2) {
      return true;
    }
    if (pObjet1 == null || pObjet2 == null) {
      return false;
    }
    return Constantes.equals(pObjet1.ordre, pObjet2.ordre) && Constantes.equals(pObjet1.nom, pObjet2.nom)
        && Constantes.equals(pObjet1.debutDecoupe, pObjet2.debutDecoupe) && Constantes.equals(pObjet1.finDecoupe, pObjet2.finDecoupe);
  }
  
  /**
   * Cette méthode est surchargée pour définir l'affichage dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return nom;
  }
  
  @Override
  public boolean equals(Object pObject) {
    // Tester si l'objet est nous-même (optimisation)
    if (this == pObject) {
      return true;
    }
    // Générer une erreur si l'objet n'est pas du type attendu (teste la nullité également)
    if (!(pObject instanceof ChampCatalogue)) {
      return false;
    }
    // Comparer les identifiants des ojets métiers
    ChampCatalogue champCatalogue = (ChampCatalogue) pObject;
    return Constantes.equals(ordre, champCatalogue.ordre);
  }
  
  @Override
  public ChampCatalogue clone() {
    ChampCatalogue copie = null;
    try {
      copie = (ChampCatalogue) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
      return null;
    }
    
    // on renvoie le clone
    return copie;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + ordre.hashCode();
    return cle;
  }
  
  /**
   * On renseigne un champ catalogue stocké en base de données à partir de la liste de champs catalogue de son fichier CSV d'origine
   */
  public static void renseignerInfosFichier(ListeChampCatalogue pListeChampCatalogue, ChampCatalogue pChampCatalogue) {
    if (pChampCatalogue == null || pListeChampCatalogue == null) {
      return;
    }
    
    if (pListeChampCatalogue.size() >= pChampCatalogue.getOrdre()) {
      ChampCatalogue champCatalogueFichier = pListeChampCatalogue.get(pChampCatalogue.getOrdre() - 1);
      pChampCatalogue.setNom(champCatalogueFichier.getNom());
      pChampCatalogue.setValeurExemple(champCatalogueFichier.getValeurExemple());
    }
    
  }
  
  /**
   * Retourne le nom du champ catalogue du fichier CSV
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Renseigner le nom du champ catalogue
   */
  public void setNom(String nom) {
    this.nom = nom;
  }
  
  /**
   * Retourne l'ordre du champ catalogue dans les colonnes du fichier CSV
   */
  public Integer getOrdre() {
    return ordre;
  }
  
  /**
   * Renseigner l'ordre du champ catalogue dans les colonnes du fichier CSV
   */
  public void setOrdre(Integer pOrdre) {
    this.ordre = pOrdre;
  }
  
  /**
   * Retourner la valeur exemple du champ catalogue. Il s'agit de la valeur de la colonne [ordre]
   * de la première ligne après la ligne titre du fichier CSV
   */
  public String getValeurExemple() {
    return valeurExemple;
  }
  
  /**
   * Renseigner la valeur exemple du champ catalogue. Il s'agit de la valeur de la colonne [ordre]
   * de la première ligne après la ligne titre du fichier CSV
   */
  public void setValeurExemple(String valeurExemple) {
    this.valeurExemple = valeurExemple;
  }
  
  /**
   * Retourner le (n) caractère à partir duquel il faut découper la valeur de la zone catalogue afin de l'intégrer dans la zone
   * équivalente de l'ERP
   */
  public Integer getDebutDecoupe() {
    return debutDecoupe;
  }
  
  /**
   * Renseigner le (n) caractère à partir duquel il faut découper la valeur de la zone catalogue afin de l'intégrer dans la zone
   * équivalente de l'ERP
   */
  public void setDebutDecoupe(Integer debutDecoupe) {
    this.debutDecoupe = debutDecoupe;
  }
  
  /**
   * Retourner le (n) caractère jusqu'au quel il faut découper la valeur de la zone catalogue afin de l'intégrer dans la zone
   * équivalente de l'ERP
   */
  public Integer getFinDecoupe() {
    return finDecoupe;
  }
  
  /**
   * Renseigner le (n) caractère jusqu'au quel il faut découper la valeur de la zone catalogue afin de l'intégrer dans la zone
   * équivalente de l'ERP
   */
  public void setFinDecoupe(Integer finDecoupe) {
    this.finDecoupe = finDecoupe;
  }
}
