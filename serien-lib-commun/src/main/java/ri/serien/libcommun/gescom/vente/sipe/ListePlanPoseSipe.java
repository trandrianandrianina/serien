/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.sipe;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste de plans de pose Sipe.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de plans de pose.
 */
public class ListePlanPoseSipe extends ListeClasseMetier<IdPlanPoseSipe, PlanPoseSipe, ListePlanPoseSipe> {
  /**
   * Constructeur.
   */
  public ListePlanPoseSipe() {
  }
  
  /**
   * Retourne une liste d'identifiants à partir de critères de recherche.
   * 
   * @param pIdSession
   * @param pCritere
   * @param pMiseAJourDepuisIfs
   * @return
   */
  public List<IdPlanPoseSipe> chargerListeIdPlanPoseSIpe(IdSession pIdSession, CritereSipe pCritere, boolean pMiseAJourDepuisIfs) {
    return ManagerServiceDocumentVente.chargerListeIdPlanPoseSipe(pIdSession, pCritere, pMiseAJourDepuisIfs);
  }
  
  /**
   * Construire une liste de PlanPoseSipe correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des plans de pose ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListePlanPoseSipe creerListeNonChargee(List<IdPlanPoseSipe> pListeIdPlanPose) {
    ListePlanPoseSipe listePlanPose = new ListePlanPoseSipe();
    if (pListeIdPlanPose != null) {
      for (IdPlanPoseSipe idPlanPose : pListeIdPlanPose) {
        PlanPoseSipe planPose = new PlanPoseSipe(idPlanPose);
        planPose.setCharge(false);
        listePlanPose.add(planPose);
      }
    }
    return listePlanPose;
  }
  
  /**
   * Charger la liste des objets métiers suivant la liste d'identifiants donnée.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListePlanPoseSipe charger(IdSession pIdSession, List<IdPlanPoseSipe> pListeIdPlanPose) {
    return ManagerServiceDocumentVente.chargerListePlanPoseSipe(pIdSession, pListeIdPlanPose);
  }
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListePlanPoseSipe charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return null;
  }
  
}
