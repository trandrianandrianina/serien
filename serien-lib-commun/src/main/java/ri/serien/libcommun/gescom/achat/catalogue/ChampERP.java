/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;

/**
 * Cette classe décrit un champ de base de données Série N par le biais de son id
 * un libellé court, un libellé long, et un ordre d'affichage
 */
public class ChampERP extends AbstractClasseMetier<IdChampERP> {
  private String libelleCourt = null;
  private String libelleLong = null;
  private EnumTypeZoneERP type = null;
  private Integer longueur = null;
  private Integer ordreAffichage = null;
  private ListeChampCatalogue listeChampCatalogue = null;
  
  /**
   * Constructeur avec Id obligatoire
   */
  public ChampERP(IdChampERP pIdChampERP) {
    super(pIdChampERP);
  }
  
  @Override
  public String getTexte() {
    return libelleLong;
  }
  
  /**
   * Comparer l'intégralité des données.
   */
  public static boolean equalsComplet(ChampERP pObjet1, ChampERP pObjet2) {
    if (pObjet1 == pObjet2) {
      return true;
    }
    if (pObjet1 == null || pObjet2 == null) {
      return false;
    }
    return Constantes.equals(pObjet1.libelleCourt, pObjet2.libelleCourt) && Constantes.equals(pObjet1.libelleLong, pObjet2.libelleLong)
        && Constantes.equals(pObjet1.type, pObjet2.type) && Constantes.equals(pObjet1.longueur, pObjet2.longueur)
        && Constantes.equals(pObjet1.ordreAffichage, pObjet2.ordreAffichage)
        && ListeChampCatalogue.equalsComplet(pObjet1.listeChampCatalogue, pObjet2.listeChampCatalogue);
  }
  
  @Override
  public ChampERP clone() {
    ChampERP copie = null;
    try {
      copie = (ChampERP) super.clone();
      ListeChampCatalogue listeChampCatalogue = null;
      if (this.getListeChampCatalogue() != null) {
        listeChampCatalogue = new ListeChampCatalogue();
        for (ChampCatalogue champCatalogue : this.getListeChampCatalogue()) {
          listeChampCatalogue.add(champCatalogue.clone());
        }
      }
      
      copie.setListeChampCatalogue(listeChampCatalogue);
    }
    catch (CloneNotSupportedException cnse) {
      return null;
    }
    
    // on renvoie le clone
    return copie;
  }
  
  /**
   * Charger un champERP à partir de son identifiant
   */
  public static ChampERP chargerParId(IdSession pIdSession, IdChampERP pidChampERP) {
    IdChampERP.controlerId(pidChampERP, true);
    ChampERP champERP = ManagerServiceFournisseur.chargerUnChampERPparId(pIdSession, pidChampERP);
    return champERP;
  }
  
  /**
   * Retourner un libellé du type de la zone ERP :
   * A pour alphnumérique
   * N pour numérique
   */
  public String interpreterType() {
    if (type == null) {
      return "";
    }
    if (type.equals(EnumTypeZoneERP.TYPE_ALPHANUMERIQUE)) {
      return "Alphanumérique";
    }
    else if (type.equals(EnumTypeZoneERP.TYPE_NUMERIQUE)) {
      return "Numérique";
    }
    else {
      return "Non typé";
    }
  }
  
  /**
   * Ajouter un champ catalogue à la liste de champs catalogue associés au champ ERP
   */
  public void ajouterUneCorrespondanceChampCatalogue(ChampCatalogue pChampCatalogue) {
    if (listeChampCatalogue == null) {
      listeChampCatalogue = new ListeChampCatalogue();
    }
    
    listeChampCatalogue.add(pChampCatalogue);
  }
  
  /**
   * Retourner la formule d'association des champs catalogue d'un champ ERP
   * Le champ ERP est associé à 0 - n champs catalogue et le découpage de chacune de ces zones
   */
  public String retournerLaFormuleAssociation() {
    String retour = "";
    
    if (listeChampCatalogue != null) {
      if (isModeExpert()) {
        int i = 0;
        for (ChampCatalogue champCatalogue : listeChampCatalogue) {
          if (i > 0) {
            retour += " + ";
          }
          
          retour += champCatalogue.getNom();
          
          if (champCatalogue.getDebutDecoupe() != null && champCatalogue.getFinDecoupe() != null) {
            retour += " [" + champCatalogue.getDebutDecoupe() + "-" + champCatalogue.getFinDecoupe() + "] ";
          }
          
          i++;
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Le champ ERP est en mode expert si :
   * - Il a 1 champ catalogue associé et que ce champ est découpé de manière différente que valeurDebut = 1 && valeurFin = [longueurMax du
   * champ ERP]
   * - Ou si il a deux champs catalogues associés
   */
  public boolean isModeExpert() {
    if (listeChampCatalogue == null || listeChampCatalogue.size() == 0) {
      return false;
    }
    else if (listeChampCatalogue.size() == 1) {
      ChampCatalogue champCatalogue = listeChampCatalogue.get(0);
      int debut = champCatalogue.getDebutDecoupe();
      int fin = champCatalogue.getFinDecoupe();
      if (debut != 1 || fin != longueur) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return true;
    }
  }
  
  /**
   * Retourner le libellé court du champ Série N sur 10 caractères maximum uniquement
   */
  public String getLibelleCourt() {
    return libelleCourt;
  }
  
  /**
   * Renseigner le libellé court du champ Série N sur 10 caractères maximum uniquement
   */
  public void setLibelleCourt(String libelleCourt) {
    this.libelleCourt = libelleCourt;
  }
  
  /**
   * Retourner le libellé long du champ Série N sur 30 caractères maximum
   */
  public String getLibelleLong() {
    return libelleLong;
  }
  
  /**
   * Renseigner le libellé long du champ Série N sur 30 caractères maximum
   */
  public void setLibelleLong(String libelleLong) {
    this.libelleLong = libelleLong;
  }
  
  /**
   * Retourner le type du champ Série N : A pour alphanumérique et N pour numérique
   */
  public EnumTypeZoneERP getType() {
    return type;
  }
  
  /**
   * Renseigner le type du champ Série N : A pour alphanumérique et N pour numérique
   */
  public void setType(EnumTypeZoneERP pType) {
    if (type != null && type.equals(pType)) {
      return;
    }
    type = pType;
  }
  
  /**
   * Retourner la longueur maximum de la zone Série N
   */
  public Integer getLongueur() {
    return longueur;
  }
  
  /**
   * Renseigner la longueur maximum de la zone Série N
   */
  public void setLongueur(Integer longueur) {
    this.longueur = longueur;
  }
  
  /**
   * Retourner l'ordre de la zone Série N
   */
  public Integer getOrdreAffichage() {
    return ordreAffichage;
  }
  
  /**
   * Renseigner l'ordre de la zone Série N
   */
  public void setOrdreAffichage(Integer ordre) {
    this.ordreAffichage = ordre;
  }
  
  /**
   * Retourner la liste des champs catalogues qui sont associés à ce champ de l'ERP
   */
  public ListeChampCatalogue getListeChampCatalogue() {
    return listeChampCatalogue;
  }
  
  /**
   * Renseigner la liste des champs catalogues qui sont associés à ce champ de l'ERP
   */
  public void setListeChampCatalogue(ListeChampCatalogue listeChampCatalogue) {
    this.listeChampCatalogue = listeChampCatalogue;
  }
}
