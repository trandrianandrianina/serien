/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockattenducommande;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.outils.Constantes;

/**
 * Attendu ou commandé d'un article.
 * 
 * L'article et le magasin sont stockés dans l'identifiant.
 */
public class StockAttenduCommande extends AbstractClasseMetier<IdStockAttenduCommande> {
  private static final int ETAT_COMMANDE = 1;
  private static final int ETAT_RESERVE = 3;
  
  private Integer etat = null;
  private Date dateValidation = null;
  private Date dateLivraisonPrevue = null;
  private Date dateLivraisonSouhaite = null;
  private String libelleTiers = null;
  private BigDecimal quantiteEntree = BigDecimal.ZERO;
  private BigDecimal quantiteSortie = BigDecimal.ZERO;
  private BigDecimal stockTheoriqueADate = null;
  private BigDecimal stockRestantAvantAttendu = null;
  private BigDecimal stockAvantRupture = null;
  
  /**
   * Constructeur avec l'identififant du stock (obligatoire).
   */
  public StockAttenduCommande(IdStockAttenduCommande pIdStockAttenduCommande) {
    super(pIdStockAttenduCommande);
  }
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  /**
   * Retourner l'état du document attendu/commandé.
   * @return Etat du document attendu/commandé.
   */
  public Integer getEtat() {
    return etat;
  }
  
  /**
   * Modifier l'état du document attendu/commandé.
   * @return Etat du document attendu/commandé.
   */
  public void setEtat(Integer pEtat) {
    etat = pEtat;
  }
  
  /**
   * Indiquer si l'état de l'attendu/commandé est réservé.
   * @return true=réservé, false=sinon.
   */
  boolean isReserve() {
    return etat == ETAT_RESERVE;
  }
  
  /**
   * Retourner la date de validation.
   * 
   * @return Date
   */
  public Date getDateValidation() {
    return dateValidation;
  }
  
  /**
   * Modifier la date de validation.
   * 
   * @param Date
   */
  public void setDateValidation(Date pDateAttenduCommande) {
    dateValidation = pDateAttenduCommande;
  }
  
  /**
   * Retourner la date de livraison prévue.
   * @return Date de livraison prévue.
   */
  public Date getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  /**
   * Modifier la date de livraison prévue.
   * @param pDateLivraisonPrevue Date de livraison prévue.
   */
  public void setDateLivraisonPrevue(Date pDateLivraisonPrevue) {
    dateLivraisonPrevue = pDateLivraisonPrevue;
  }
  
  /**
   * Retourner la date de livraison souhaitée.
   * @return Date de livraison souhaitée.
   */
  public Date getDateLivraisonSouhaite() {
    return dateLivraisonSouhaite;
  }
  
  /**
   * Modifier la date de livraison souhaitée.
   * @param pDateLivraisonSouhaite Date de livraison souhaitée.
   */
  public void setDateLivraisonSouhaite(Date pDateLivraisonSouhaite) {
    dateLivraisonSouhaite = pDateLivraisonSouhaite;
  }
  
  /**
   * Retourner la date à prendre en compte pour l'attendu/commandé.
   * 
   * si pas de DLP, on prend la date de livraison souhaité, sinon date de validation.
   * 
   * @return Date de référence.
   */
  public Date getDateReference() {
    // Definition de la date à prendre en compte
    Date dateReference = new Date();
    if (dateLivraisonPrevue != null) {
      dateReference = dateLivraisonPrevue;
    }
    else if (dateLivraisonSouhaite != null) {
      dateReference = dateLivraisonSouhaite;
    }
    else if (dateValidation != null) {
      dateReference = dateValidation;
    }
    
    return dateReference;
  }
  
  /**
   * Retourner le libellé du tiers.
   * 
   * Il n'y a pas de tiers lorsque le type de l'attendu/commandé est la date du jour ou la date minimum de réapprovisionnement. Dans ce
   * cas, on retourne, le libellé du type.
   * 
   * @return Libellé du tiers.
   */
  public String getLibelleTiers() {
    if (getId().isDateJour() || getId().isDateMiniReappro()) {
      return getId().getTypeAttenduCommande().toString();
    }
    else {
      return libelleTiers;
    }
  }
  
  /**
   * Modifier le libellé du tiers.
   * @param pLibelleTiers Libellé du tiers.
   */
  public void setLibelleTiers(String pLibelleTiers) {
    libelleTiers = pLibelleTiers;
  }
  
  /**
   * Retourner la quantité attendue (achats)
   * @return BigDecimal
   */
  public BigDecimal getQuantiteEntree() {
    return quantiteEntree;
  }
  
  /**
   * Modifier la quantité attendue (achats) en unité de stock.
   * @param BigDecimal
   */
  public void setQuantiteEntree(BigDecimal pQuantiteAttenduCommande) {
    quantiteEntree = pQuantiteAttenduCommande;
  }
  
  /**
   * Retourner la quantité commandée (ventes).
   * 
   * @return BigDecimal
   */
  public BigDecimal getQuantiteSortie() {
    return quantiteSortie;
  }
  
  /**
   * Modifier la quantité en sortie (ventes) en unité de stock.
   * 
   * @param pQuantiteSortie Quantité en sortie en unité de stock.
   */
  public void setQuantiteSortie(BigDecimal pQuantiteSortie) {
    quantiteSortie = pQuantiteSortie;
  }
  
  /**
   * Modifier la quantité en sortie (ventes) en unité de vente.
   * 
   * Pour les attendus, la quantité en sortie est stockée en unité de vente dans le champ L1QTE. Il la faut en unité de stock. Pour
   * convertir la quantité en unité de stock, il suffit de la multiplier la quantité par le coefficient L1KSV.
   * 
   * Cela marche bien lorsque le coefficient est une valeur entière (par exemple 2). Par contre, si le coefficient est 0,333 par
   * exemple, on fait le calcul suivant : 1500 * 0.333 = 499.5 (alors que la bonne valeur est 500). On comprend que 0.333 est l’inverse
   * de 3 tronqué avec 3 décimales mais on ne retrouve pas la bonne valeur mathématiquement.
   * 
   * La technique utilisée par les programmes RPG est la suivante. On calcule l’inverse du conditionnement L1CND arrondi avec 3 chiffres
   * après la virgule. Si l’inverse du conditionnement est égal au coefficient, on calcule la quantité en unité de stock en divisant
   * la quantité en unité de vente par le conditionnement (1500 / 3 = 500 dans notre exemple).
   * 
   * @param pQuantiteSortie Quantité en sortie en unité de vente.
   * @param pCoefficientStock Coefficient de stock (peut être null).
   * @param pConditionnement Conditionnement (peut être null).
   */
  public void setQuantiteSortieEnUniteVente(BigDecimal pQuantiteSortie, BigDecimal pCoefficientStock, BigDecimal pConditionnement) {
    // Affecter directement la valeur fournie si le coefficient de stock n'est pas fourni ou il est égal à 1
    if (pCoefficientStock == null || pCoefficientStock.compareTo(BigDecimal.ZERO) == 0
        || pCoefficientStock.compareTo(BigDecimal.ONE) == 0) {
      quantiteSortie = pQuantiteSortie;
      return;
    }
    
    // Tester si le conditionnement est renseigné afin de déterminer s'il faut diviser par le conditionnement plutôt que multiplier
    // par le coefficient
    if (pConditionnement != null && !Constantes.equals(pConditionnement, BigDecimal.ZERO)) {
      // Calculer l'inverse du conditionnement avec 3 chiffres après la virgule
      BigDecimal inverseConditionnement = BigDecimal.ONE.divide(pConditionnement, 3, RoundingMode.HALF_EVEN);
      
      // Comparer l'inverse du conditionnement avec le coefficient de stock
      // Si les deux sont égaux, on divise plutôt par le conditionnement afin d'éviter des problèmes d'arrondis
      if (Constantes.equals(inverseConditionnement, pCoefficientStock)) {
        quantiteSortie = pQuantiteSortie.divide(pConditionnement, 3, RoundingMode.HALF_EVEN).stripTrailingZeros();
        return;
      }
    }
    
    // Multiplier la quantité fournie (en unité de vente) par le coefficient de stock pour avoir la quantité en unité de stock
    quantiteSortie = pQuantiteSortie.multiply(pCoefficientStock).setScale(3).stripTrailingZeros();
  }
  
  /**
   * Retourner le stock théorique à date.
   * 
   * Cette valeur correspond au stock physique à une date de référence en considérant que tous les attendus dont la date de livraison
   * prévue est inférieure à la date de référence sont réceptionnées et que toutes les commandés dont la date de livraison prévue est
   * inférieure à la date de référence ont été livrées au client. En résumé, il n'y a aucun retard. C'est pourquoi le terme théorique
   * est utilisé car on se situe dans un cas idéal pour calculer cette valeur.
   * 
   * @return Stock théroique à date.
   */
  public BigDecimal getStockTheoriqueADate() {
    return stockTheoriqueADate;
  }
  
  /**
   * Modifier le stock théorique à date.
   * @param pStockTheoriqueADate Stock théroique à date?
   */
  public void setStockTheoriqueADate(BigDecimal pStockTheoriqueADate) {
    stockTheoriqueADate = pStockTheoriqueADate;
  }
  
  /**
   * Retourner le stock restant avant réception de l'attendu.
   * @return Stock restant avant réception de l'attendu.
   */
  public BigDecimal getStockRestantAvantAttendu() {
    return stockRestantAvantAttendu;
  }
  
  /**
   * Modifier le stock restant avant réception de l'attendu.
   * @param pStockRestantAvantAttendu Stock restant avant réception de l'attendu.
   */
  public void setStockRestantAvantAttendu(BigDecimal pStockRestantAvantAttendu) {
    stockRestantAvantAttendu = pStockRestantAvantAttendu;
  }
  
  /**
   * Retourner la quantité en rupture à la date de l'attendu/commandé.
   * @return BigDecimal
   */
  public BigDecimal getStockAvantRupture() {
    return stockAvantRupture;
  }
  
  /**
   * Modifier la quantité en rupture à la date de l'attendu/commandé.
   * @param BigDecimal
   */
  public void setStockAvantRupture(BigDecimal pStockAvantRupture) {
    stockAvantRupture = pStockAvantRupture;
  }
}
