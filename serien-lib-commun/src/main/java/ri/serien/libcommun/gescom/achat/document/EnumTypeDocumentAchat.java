/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'un document d'achat.
 */
public enum EnumTypeDocumentAchat {
  NON_DEFINI(0, "Non défini"),
  COMMANDE(1, "Commande"),
  RECEPTION(2, "Réception"),
  FACTURE(3, "Facture"),
  TRANSFERT(4, "Transfert"),
  COMMANDE_ORIGINE(5, "Commande d'origine");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeDocumentAchat(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Le libellé associé au code en minuscules.
   */
  public String getLibelleEnMinuscules() {
    return libelle.toLowerCase();
  }
  
  /**
   * Retourne le code entête à partir du type.
   */
  public EnumCodeEnteteDocumentAchat retournerEnumCodeEnteteDocumentAchat() {
    switch (this) {
      case COMMANDE:
        return EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION;
      case RECEPTION:
        return EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION;
      case FACTURE:
        return EnumCodeEnteteDocumentAchat.FACTURE;
      case TRANSFERT:
        return EnumCodeEnteteDocumentAchat.TRANSFERT;
      case COMMANDE_ORIGINE:
        return EnumCodeEnteteDocumentAchat.COMMANDE_ORIGINE;
      default:
        return null;
    }
  }
  
  /**
   * Retourner le libellé associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  public static EnumTypeDocumentAchat valueOfByCode(Integer pCode) {
    for (EnumTypeDocumentAchat value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type du document d'achat est invalide : " + pCode);
  }
  
}
