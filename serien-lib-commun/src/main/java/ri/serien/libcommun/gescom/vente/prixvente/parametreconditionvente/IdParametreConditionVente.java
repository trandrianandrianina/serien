/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'un paramètre d'une condition de ventes.
 * Cet identifiant n'a aucune valeur en base, il est créé à la volée lors du chargement depuis la base.
 * 
 * L'identifiant est composé d'un numéro unique.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdParametreConditionVente extends AbstractId {
  // Variables
  private static int autoIncrement = 0;
  private final Integer numero;
  
  /**
   * Constructeur.
   */
  private IdParametreConditionVente(EnumEtatObjetMetier pEnumEtatObjetMetier, Integer pNumero) {
    super(pEnumEtatObjetMetier);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Créer un identifiant.
   */
  public static IdParametreConditionVente getInstance() {
    autoIncrement++;
    return new IdParametreConditionVente(EnumEtatObjetMetier.MODIFIE, Integer.valueOf(autoIncrement));
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du numéro.
   * Il doit être supérieur à zéro.
   */
  private Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du paramètre de la condition de ventes n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du paramètre de la condition de ventes est inférieur ou égal à zéro.");
    }
    return pValeur;
  }
  
  // -- Méthodes publiques
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdParametreConditionVente controlerId(IdParametreConditionVente pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de la ligne de vente est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de la ligne de vente n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdParametreConditionVente)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de paramètree de condition de ventes.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdParametreConditionVente id = (IdParametreConditionVente) pObject;
    return numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdParametreConditionVente)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdParametreConditionVente id = (IdParametreConditionVente) pObject;
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return "" + getNumero().intValue();
  }
  
  // -- Accesseurs
  
  /**
   * Numéro du document contenant la ligne.
   * Le numéro du document est compris entre entre 0 et 9 999.
   */
  public Integer getNumero() {
    return numero;
  }
  
}
