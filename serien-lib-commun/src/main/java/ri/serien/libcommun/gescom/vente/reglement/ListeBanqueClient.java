/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceClient;

/**
 * Liste de banques clients.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de banques clients.
 */
public class ListeBanqueClient extends ListeClasseMetier<IdBanqueClient, BanqueClient, ListeBanqueClient> {
  // Liste pré-saisie des principales banques françaises
  private static final String[] listeBanqueFrancaise =
      { "Société Générale", "BNP Paribas", "La Banque Postale", "HSBC", "LCL", "Crédit Mutuel", "Crédit Agricole", "Caisse d'Epargne",
          "Banque Populaire", "Crédit Coopératif", "Allianz Banque", "AXA Banque", "Banque Accord", "Banque Casino", "BRED",
          "Carrefour Banque", "Crédit du Nord", "Groupama Banque", "Milleis Banque", "PSA Banque", "RCI Banque", "VTB Bank France" };
  
  /**
   * Constructeur.
   */
  public ListeBanqueClient() {
  }
  
  // -- Méthodes surchargées
  
  /**
   * Charger les banques clients suivant l'établissement.
   */
  @Override
  public ListeBanqueClient charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CritereBanqueClient criteres = new CritereBanqueClient();
    criteres.setIdEtablissement(pIdEtablissement);
    ListeBanqueClient listeBanqueClient = ManagerServiceClient.chargerListeBanqueClient(pIdSession, criteres);
    
    // Chargement des principales banques françaises
    return completerAvecBanquePresaisie(listeBanqueClient);
  }
  
  @Override
  public ListeBanqueClient charger(IdSession pIdSession, List<IdBanqueClient> pListeId) {
    return null;
  }
  
  /**
   * Ajoute une banque à la liste.
   */
  @Override
  public boolean add(BanqueClient pBanqueClient) {
    if (pBanqueClient == null) {
      throw new MessageErreurException("La banque du client est invalide.");
    }
    // Ajoute la banque à la liste
    if (!contains(pBanqueClient) && super.add(pBanqueClient)) {
      return true;
    }
    return false;
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public ListeBanqueClient clone() {
    ListeBanqueClient o = null;
    // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
    o = (ListeBanqueClient) super.clone();
    return o;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner une banque client de la liste à partir de son identifiant.
   */
  public BanqueClient retournerBanqueParId(IdBanqueClient pIdBanqueClient) {
    IdBanqueClient.controlerId(pIdBanqueClient, false);
    
    for (BanqueClient banqueClient : this) {
      if (banqueClient != null && Constantes.equals(banqueClient.getId(), pIdBanqueClient)) {
        return banqueClient;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si une banque cliente est présente dans la liste.
   */
  public boolean isPresent(IdBanqueClient pIdBanqueClient) {
    return retournerBanqueParId(pIdBanqueClient) != null;
  }
  
  // -- Méthodes privées
  
  /**
   * Complète une liste de banque avec la liste présaisie.
   */
  private ListeBanqueClient completerAvecBanquePresaisie(ListeBanqueClient pListeBanqueClient) {
    if (pListeBanqueClient == null) {
      pListeBanqueClient = new ListeBanqueClient();
    }
    for (String nomBanque : listeBanqueFrancaise) {
      pListeBanqueClient.add(new BanqueClient(IdBanqueClient.getInstance(nomBanque)));
    }
    
    return pListeBanqueClient;
  }
  
}
