/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.google.gson.Gson;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.ParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.ListeParametreChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.ParametreClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ListeParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.ParametreDocumentVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Classe regroupant tous les paramètres nécessaires au chargement des données pour effectuer un calcul de prix.
 */
public class ParametreChargement implements Serializable, Cloneable {
  // Variables
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private IdArticle idArticle = null;
  private IdClient idClientLivre = null;
  private IdClient idClientFacture = null;
  private IdLigneVente idLigneDocumentVente = null;
  private IdDocumentVente idDocumentVente = null;
  private IdChantier idChantier = null;
  // Peut être utilisé pour calculer la date d'application
  private Date dateApplication = null;
  private BigDecimal prixBaseForce = null;
  
  private ParametreEtablissement parametreEtablissement = null;
  private ParametreArticle parametreArticle = null;
  private ParametreDocumentVente parametreDocumentVente = null;
  private ParametreLigneVente parametreLigneVente = null;
  private ParametreClient parametreClientLivre = null;
  private ParametreClient parametreClientFacture = null;
  private ParametreTarif parametreTarif = null;
  private ListeParametreChantier parametreChantier = null;
  
  private ParametreConditionVente parametreConditionVentePrioritaire = null;
  private ListeParametreConditionVente listeParametreConditionVenteNormale = null;
  private ListeParametreConditionVente listeParametreConditionVenteQuantitative = null;
  private ListeParametreConditionVente listeParametreConditionVenteCumulative = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Cloner l'objet.
   */
  @Override
  public ParametreChargement clone() {
    Gson gson = new Gson();
    return gson.fromJson(gson.toJson(this), ParametreChargement.class);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Permet de contrôler que les paramètres obligatoires pour le chargement soient bien initialisés avec une valeur.
   */
  public void controlerParametreObligatoirePourChargement() {
    if (idEtablissement == null) {
      throw new MessageErreurException("L'identifiant de l'établissement est invalide.");
    }
  }
  
  /**
   * Permet de contrôler que les paramètres obligatoires pour le calcul du prix.
   */
  public void controlerParametreObligatoirePourCalcul() {
    if (parametreEtablissement == null) {
      throw new MessageErreurException("Le paramètre établissement est invalide alors qu'il est oligatoire pour le calcul d'un prix.");
    }
    if (parametreArticle == null) {
      throw new MessageErreurException("Le paramètre article est invalide alors qu'il est oligatoire pour le calcul d'un prix.");
    }
  }
  
  // -- Accesseurs
  
  /**
   * Retourne l'identifiant de l'établissement.
   * 
   * @return
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Initialise l'identifiant de l'établissement.
   * 
   * @param pIdEtablissement
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    this.idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourne l'identifiant du magasin.
   * 
   * @return
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Initialise l'identifiant du magasin.
   * 
   * @param pIdMagasin
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    this.idMagasin = pIdMagasin;
  }
  
  /**
   * Retourne l'identifiant de l'article.
   * 
   * @return
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Initialise l'identifiant de l'article.
   * 
   * @param pIdArticle
   */
  public void setIdArticle(IdArticle pIdArticle) {
    this.idArticle = pIdArticle;
  }
  
  /**
   * Retourne l'identifiant du client livré.
   * 
   * @return
   */
  public IdClient getIdClientLivre() {
    return idClientLivre;
  }
  
  /**
   * Initialise l'identifiant du client livré.
   * 
   * @param pIdClientLivre
   */
  public void setIdClientLivre(IdClient pIdClientLivre) {
    this.idClientLivre = pIdClientLivre;
  }
  
  /**
   * Retourne l'identifiant du client facturé.
   * 
   * @return
   */
  public IdClient getIdClientFacture() {
    return idClientFacture;
  }
  
  /**
   * Initialise l'identifiant du client facturé.
   * 
   * @param pIdClientFacture
   */
  public void setIdClientFacture(IdClient pIdClientFacture) {
    this.idClientFacture = pIdClientFacture;
  }
  
  /**
   * Retourne l'identifiant de la ligne du document de vente.
   * 
   * @return
   */
  public IdLigneVente getIdLigneVente() {
    return idLigneDocumentVente;
  }
  
  /**
   * Initialise l'identifiant de la ligne du document de vente.
   * 
   * @param pIdLigneDocumentVente
   */
  public void setIdLigneDocumentVente(IdLigneVente pIdLigneDocumentVente) {
    this.idLigneDocumentVente = pIdLigneDocumentVente;
  }
  
  /**
   * Retourne l'identifiant du document de vente.
   * 
   * @return
   */
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  /**
   * Initialise l'identifiant du document de vente.
   * 
   * @param pIdDocumentVente
   */
  public void setIdDocumentVente(IdDocumentVente pIdDocumentVente) {
    this.idDocumentVente = pIdDocumentVente;
  }
  
  /**
   * Retourne l'identifiant du chantier.
   * 
   * @return
   */
  public IdChantier getIdChantier() {
    return idChantier;
  }
  
  /**
   * Initialise l'identifiant du chantier.
   * 
   * @param pIdChantier
   */
  public void setIdChantier(IdChantier pIdChantier) {
    this.idChantier = pIdChantier;
  }
  
  /**
   * Retourne les paramètres du document de vente.
   * 
   * @return
   */
  public ParametreDocumentVente getParametreDocumentVente() {
    return parametreDocumentVente;
  }
  
  /**
   * Initialise les paramètres du document de vente.
   * 
   * @param pParametreDocumentVente
   */
  public void setParametreDocumentVente(ParametreDocumentVente pParametreDocumentVente) {
    this.parametreDocumentVente = pParametreDocumentVente;
  }
  
  /**
   * Retourner la date qui sera utilisée pour le calcul du prix de vente.
   * Contient la date du jour ou la date de la session car cette dernière peut être modifié (sauf au Comptoir à l'heure actuelle).
   * @return Date de traitement.
   */
  public Date getDateApplication() {
    return dateApplication;
  }
  
  /**
   * Modifier la date qui sera utilisée pour le calcul du prix de vente.
   * Si la date de traitement n'est pas fournie, c'est la date du jour qui est utilisée par défaut lors des calculs.
   * @param pDateApplication Date de traitement.
   */
  public void setDateApplication(Date pDateApplication) {
    dateApplication = pDateApplication;
  }
  
  /**
   * Retourne le prix de base HT forcé.
   * 
   * @return
   */
  public BigDecimal getPrixBaseForce() {
    return prixBaseForce;
  }
  
  /**
   * Initialise le prix de base HT forcé.
   * 
   * @param pPrixBaseForce
   */
  public void setPrixBaseForce(BigDecimal pPrixBaseForce) {
    this.prixBaseForce = pPrixBaseForce;
  }
  
  /**
   * Retourne les paramètres généraux.
   * 
   * @return
   */
  public ParametreEtablissement getParametreEtablissement() {
    return parametreEtablissement;
  }
  
  /**
   * Initialise les paramètres généraux.
   * 
   * @param pParametreEtablissement
   */
  public void setParametreEtablissement(ParametreEtablissement pParametreEtablissement) {
    parametreEtablissement = pParametreEtablissement;
  }
  
  /**
   * Retourne les paramètres de l'article.
   * 
   * @return
   */
  public ParametreArticle getParametreArticle() {
    return parametreArticle;
  }
  
  /**
   * Initialise les paramètres de l'article.
   * 
   * @param pParametreArticle
   */
  public void setParametreArticle(ParametreArticle pParametreArticle) {
    this.parametreArticle = pParametreArticle;
  }
  
  /**
   * Retourne les paramètres de la ligne de vente.
   * 
   * @return
   */
  public ParametreLigneVente getParametreLigneVente() {
    return parametreLigneVente;
  }
  
  /**
   * Initialise les paramètres de la ligne de vente.
   * 
   * @param pParametreLigneVente
   */
  public void setParametreLigneVente(ParametreLigneVente pParametreLigneVente) {
    this.parametreLigneVente = pParametreLigneVente;
  }
  
  /**
   * Retourne les paramètres du client livré.
   * 
   * @return
   */
  public ParametreClient getParametreClientLivre() {
    return parametreClientLivre;
  }
  
  /**
   * Initialise les paramètres du client livré.
   * 
   * @param pParametreClientLivre
   */
  public void setParametreClientLivre(ParametreClient pParametreClientLivre) {
    parametreClientLivre = pParametreClientLivre;
  }
  
  /**
   * Retourne les paramètres du client facturé.
   * 
   * @return
   */
  public ParametreClient getParametreClientFacture() {
    return parametreClientFacture;
  }
  
  /**
   * Initialise les paramètres du client facturé.
   * 
   * @param pParametreClientFacture
   */
  public void setParametreClientFacture(ParametreClient pParametreClientFacture) {
    parametreClientFacture = pParametreClientFacture;
  }
  
  /**
   * Retourne les paramètres du tarif.
   * 
   * @return
   */
  public ParametreTarif getParametreTarif() {
    return parametreTarif;
  }
  
  /**
   * Initialise les paramètres du tarif.
   * 
   * @param pParametreTarif
   */
  public void setParametreTarif(ParametreTarif pParametreTarif) {
    this.parametreTarif = pParametreTarif;
  }
  
  /**
   * Retourne les paramètres du chantier.
   * 
   * @return
   */
  public ListeParametreChantier getParametreChantier() {
    return parametreChantier;
  }
  
  /**
   * Initialise les paramètres du chantier.
   * 
   * @param pParametreChantier
   */
  public void setParametreChantier(ListeParametreChantier pParametreChantier) {
    this.parametreChantier = pParametreChantier;
  }
  
  /**
   * Retourne la condition de ventes prioritaire si présente.
   * 
   * @return
   */
  public ParametreConditionVente getParametreConditionVentePrioritaire() {
    return parametreConditionVentePrioritaire;
  }
  
  /**
   * Initialise la condition de ventes prioritaire si présente.
   * 
   * @param pParametreConditionVentePrioritaire
   */
  public void setParametreConditionVentePrioritaire(ParametreConditionVente pParametreConditionVentePrioritaire) {
    this.parametreConditionVentePrioritaire = pParametreConditionVentePrioritaire;
  }
  
  /**
   * Retourne une liste de paramètres de conditions de ventes normales.
   * 
   * @return
   */
  public ListeParametreConditionVente getListeParametreConditionVenteNormale() {
    return listeParametreConditionVenteNormale;
  }
  
  /**
   * Initialise une liste de paramètres de conditions de ventes normales.
   * 
   * @param pListeParametreConditionVente
   */
  public void setListeParametreConditionVenteNormale(ListeParametreConditionVente pListeParametreConditionVente) {
    this.listeParametreConditionVenteNormale = pListeParametreConditionVente;
  }
  
  /**
   * Retourne une liste de paramètres de conditions de ventes quantitatives.
   * 
   * @return
   */
  public ListeParametreConditionVente getListeParametreConditionVenteQuantitative() {
    return listeParametreConditionVenteQuantitative;
  }
  
  /**
   * Initialise une liste de paramètres de conditions de ventes quantitatives.
   * 
   * @param pListeParametreConditionVente
   */
  public void setListeParametreConditionVenteQuantitative(ListeParametreConditionVente pListeParametreConditionVente) {
    this.listeParametreConditionVenteQuantitative = pListeParametreConditionVente;
  }
  
  /**
   * Retourne une liste de paramètres de conditions de ventes cumulatives.
   * 
   * @return
   */
  public ListeParametreConditionVente getListeParametreConditionVenteCumulative() {
    return listeParametreConditionVenteCumulative;
  }
  
  /**
   * Initialise une liste de paramètres de conditions de ventes cumulatives
   * 
   * @param pListeParametreConditionVenteCumulative
   */
  public void setListeParametreConditionVenteCumulative(ListeParametreConditionVente pListeParametreConditionVenteCumulative) {
    this.listeParametreConditionVenteCumulative = pListeParametreConditionVenteCumulative;
  }
  
}
