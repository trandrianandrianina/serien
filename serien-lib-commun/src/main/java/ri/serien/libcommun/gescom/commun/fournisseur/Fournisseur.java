/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.achat.document.EnumOptionEdition;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.CritereContact;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceCommun;

public class Fournisseur extends AbstractClasseMetier<IdFournisseur> {
  // Constantes
  public static final int TAILLE_ZONE_NOM = 30;
  public static final int TAILLE_ZONE_VILLE = 24;
  public static final int TAILLE_ZONE_CODEPOSTAL = 5;
  public static final int TOUS_FOURNISSEURS = -1;
  
  // Variables fichiers
  private String nomFournisseur = null; // Nom du fournisseur
  private Integer topSysteme = null; // Top système
  private Date dateCreation = null; // Date de création
  private Date dateModification = null; // Date de modification
  private Date dateTraitement = null; // Date de traitement
  private List<AdresseFournisseur> listeAdresseFournisseur = null;
  private ListeContact listeContact = null;
  private String topUtilisateur1 = null; // Top utilisateur 1
  private String topUtilisateur2 = null; // Top utilisateur 2
  private String topUtilisateur3 = null; // Top utilisateur 3
  private String topUtilisateur4 = null; // Top utilisateur 4
  private String topUtilisateur5 = null; // Top utilisateur 5
  private String codeCategorie = null; // Code catégorie
  private String cleClassement = null; // Clé de classement
  private String codeLangue = null; // Code langue
  private String typeFacturation = null; // Type facturation
  private String devise = null; // Devise
  private BigDecimal remise1 = null; // Remise 1
  private BigDecimal remise2 = null; // Remise 2
  private BigDecimal remise3 = null; // Remise 3
  private BigDecimal pourcentageEscompte = null; // Pourcentage d'escompte
  private BigDecimal minimumCommande = null; // Minimum de commande
  private BigDecimal franco = null; // Franco de port
  private BigDecimal periodiciteCommande = null; // Périodicité Cde en sem.
  private String notreNumeroClient = null; // Notre numéro client chez ce fournisseur
  private String codeReglement = null; // Code réglement
  private String echeance = null; // Echéance
  private Integer zoneSysteme = null; // Zone système ?
  private String codeLitige = null; // Code litige
  private String codePaysCEE = null; // Code pays CEE
  private String numeroLibelleArticle = null; // Numéro de libellé Article
  private String codeAdresse = null; // Code bloc adresse
  private String topStats = null; // Top stats en qte
  private String cumulRemises = null; // Cumul remises entête + ligne
  private String margeSecurite = null; // Marge sécurité en semaines
  private String nombreBonRec = null; // Nombre exemplaires Bon Rec.
  private String modeExpedition = null; // Mode d'expédition
  private String transporteur = null; // Code transporteur habituel
  private String codeRistourne = null; // Code ristourne
  private String codePaysTVAIntracom = null; // Code pays TVA Intracommun.
  private String codeInfoTVAIntracom = null; // Code informations TVA Intracommun.
  private String numeroSIREN = null; // SIREN
  private String complementSIRET = null; // Complément SIRET
  private BigDecimal maximumCommande = null; // Maximum de commande
  private Integer noteFournisseur = null; // Note du fournisseur
  private String relicat = null; // Reliquat O/N
  private String exclusionWeb = null; // Exclusion Web
  private String dateEcheance = null; // Date échéance/date réception
  private String delaiJours = null; // Delai en Jours et Non Semain
  private Integer numeroCompteGeneral = null; // NCG FRS à comptabiliser
  private Integer numeroCompteAuxiliaire = null; // NCA FRS à comptabiliser
  private String codeAPE = null; // Code APE
  private String fournisseurInterne = null; // Fournisseur interne
  private String codeExpeDILICOM = null; // Code d'expédition DILICOM
  private String codeNoteDILICOM = null; // Code de notation DILICOM
  private String codeAPEv2 = null; // Code APE version 2
  private String marque = null; // Marque
  private Integer collectifRegroupement = null; // collectif regroupement
  private Integer fournisseurRegroupement = null; // fournisseur regroupement
  private String prefixeArticle = null; // Préfixe code article
  private BigDecimal coeffApproche = null; // Coefficient d'approche fixe
  private Integer delaiLivraison = null; // Délai de livraison (FRDEL2)
  private Integer delaiLivraison2 = null; // Délai de livraison (FRCL2)
  private String cleClassement2 = null; // Clé de classement 2
  private BlocNote blocNote = null;
  private Boolean modeEnlevement = Boolean.FALSE;
  private EnumTypePrixAchat typePrixAchat = null;
  private EnumOptionEdition typeOptionChiffrageEdition = null;
  
  /**
   * Constructeur avec l'idenfiant du fournisseur.
   */
  public Fournisseur(IdFournisseur pIdFournisseur) {
    super(pIdFournisseur);
  }
  
  /**
   * Cette méthode est surchargée pour définir l'affichage dans les listes déroulantes.
   * Le format souhaité est "Libellé (Code)". Un format alternatif est présent par sécurité en cas de nullité du nom du fournisseur.
   */
  @Override
  public String getTexte() {
    if (nomFournisseur != null) {
      return nomFournisseur;
    }
    else {
      return id.getTexte();
    }
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdFournisseur controlerId(Fournisseur pFournisseur, boolean pVerifierExistance) {
    if (pFournisseur == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    return IdFournisseur.controlerId(pFournisseur.getId(), true);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne les adresses fournisseur à partir de son id.
   */
  public AdresseFournisseur getAdresseFournisseur(IdAdresseFournisseur pIdAdresseFournisseur) {
    if (listeAdresseFournisseur == null || listeAdresseFournisseur.isEmpty() || pIdAdresseFournisseur == null) {
      return null;
    }
    
    for (AdresseFournisseur adresseFournisseur : listeAdresseFournisseur) {
      if (adresseFournisseur.getId().equals(pIdAdresseFournisseur)) {
        return adresseFournisseur;
      }
    }
    return null;
  }
  
  /**
   * Retourne les adresses du siège.
   */
  public AdresseFournisseur getAdresseSiege() {
    if (listeAdresseFournisseur == null || listeAdresseFournisseur.isEmpty()) {
      return null;
    }
    
    for (AdresseFournisseur adresseFournisseur : listeAdresseFournisseur) {
      if (adresseFournisseur.isSiege()) {
        return adresseFournisseur;
      }
    }
    return null;
  }
  
  /**
   * Charger la liste des contacts du fournisseur
   */
  public void chargerListeContact(IdSession pIdSession) {
    listeContact = new ListeContact();
    CritereContact criteres = new CritereContact();
    criteres.setTypeContact(EnumTypeContact.FOURNISSEUR);
    criteres.setIdFournisseur(getId());
    List<IdContact> listeIdContact = ManagerServiceCommun.chargerListeIdContact(pIdSession, criteres);
    listeContact = listeContact.charger(pIdSession, listeIdContact);
  }
  
  /**
   * Retourne le contact principal du fournisseur.
   */
  public Contact getContactPrincipal() {
    if (listeContact == null || listeContact.isEmpty()) {
      return null;
    }
    return listeContact.getContactPrincipal(getId());
    
  }
  
  public boolean isUnFournisseurValide() {
    return (id.getNumero() > 0 && id.getCollectif() > 0);
  }
  
  /**
   * Permet de savoir si le fournisseur inclus les frais de port.
   */
  public boolean isFactureFraisPort() {
    return EnumTypePrixAchat.PRIX_DE_REVIENT.equals(typePrixAchat);
  }
  
  // -- Accesseurs
  
  public Integer getTopSysteme() {
    return topSysteme;
  }
  
  public void setTopSysteme(Integer topSysteme) {
    this.topSysteme = topSysteme;
  }
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public Date getDateModification() {
    return dateModification;
  }
  
  public void setDateModification(Date dateModification) {
    this.dateModification = dateModification;
  }
  
  public Date getDateTraitement() {
    return dateTraitement;
  }
  
  public void setDateTraitement(Date dateTraitement) {
    this.dateTraitement = dateTraitement;
  }
  
  public String getNomFournisseur() {
    return nomFournisseur;
  }
  
  public void setNomFournisseur(String nomFournisseur) {
    this.nomFournisseur = nomFournisseur;
  }
  
  public List<AdresseFournisseur> getListeAdresseFournisseur() {
    return listeAdresseFournisseur;
  }
  
  public void setListeAdresse(List<AdresseFournisseur> pListeCoordonnees) {
    this.listeAdresseFournisseur = pListeCoordonnees;
  }
  
  public ListeContact getListeContact() {
    return listeContact;
  }
  
  public void setListeContact(ListeContact listeContact) {
    this.listeContact = listeContact;
  }
  
  public String getTopUtilisateur1() {
    return topUtilisateur1;
  }
  
  public void setTopUtilisateur1(String topUtilisateur1) {
    this.topUtilisateur1 = topUtilisateur1;
  }
  
  public String getTopUtilisateur2() {
    return topUtilisateur2;
  }
  
  public void setTopUtilisateur2(String topUtilisateur2) {
    this.topUtilisateur2 = topUtilisateur2;
  }
  
  public String getTopUtilisateur3() {
    return topUtilisateur3;
  }
  
  public void setTopUtilisateur3(String topUtilisateur3) {
    this.topUtilisateur3 = topUtilisateur3;
  }
  
  public String getTopUtilisateur4() {
    return topUtilisateur4;
  }
  
  public void setTopUtilisateur4(String topUtilisateur4) {
    this.topUtilisateur4 = topUtilisateur4;
  }
  
  public String getTopUtilisateur5() {
    return topUtilisateur5;
  }
  
  public void setTopUtilisateur5(String topUtilisateur5) {
    this.topUtilisateur5 = topUtilisateur5;
  }
  
  public String getCodeCategorie() {
    return codeCategorie;
  }
  
  public void setCodeCategorie(String codeCategorie) {
    this.codeCategorie = codeCategorie;
  }
  
  public String getCleClassement() {
    return cleClassement;
  }
  
  public void setCleClassement(String cleClassement) {
    this.cleClassement = cleClassement;
  }
  
  public String getCodeLangue() {
    return codeLangue;
  }
  
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }
  
  public String getTypeFacturation() {
    return typeFacturation;
  }
  
  public void setTypeFacturation(String typeFacturation) {
    this.typeFacturation = typeFacturation;
  }
  
  public String getDevise() {
    return devise;
  }
  
  public void setDevise(String devise) {
    this.devise = devise;
  }
  
  public BigDecimal getRemise1() {
    return remise1;
  }
  
  public void setRemise1(BigDecimal remise1) {
    this.remise1 = remise1;
  }
  
  public BigDecimal getRemise2() {
    return remise2;
  }
  
  public void setRemise2(BigDecimal remise2) {
    this.remise2 = remise2;
  }
  
  public BigDecimal getRemise3() {
    return remise3;
  }
  
  public void setRemise3(BigDecimal remise3) {
    this.remise3 = remise3;
  }
  
  public BigDecimal getPourcentageEscompte() {
    return pourcentageEscompte;
  }
  
  public void setPourcentageEscompte(BigDecimal pourcentageEscompte) {
    this.pourcentageEscompte = pourcentageEscompte;
  }
  
  public BigDecimal getMinimumCommande() {
    return minimumCommande;
  }
  
  public void setMinimumCommande(BigDecimal minimumCommande) {
    this.minimumCommande = minimumCommande;
  }
  
  public BigDecimal getMontantFrancoPort() {
    return franco;
  }
  
  public void setMontantFrancoPort(BigDecimal franco) {
    this.franco = franco;
  }
  
  public BigDecimal getPeriodiciteCommande() {
    return periodiciteCommande;
  }
  
  public void setPeriodiciteCommande(BigDecimal periodiciteCommande) {
    this.periodiciteCommande = periodiciteCommande;
  }
  
  public String getNotreNumeroClient() {
    return notreNumeroClient;
  }
  
  public void setNotreNumeroClient(String notreNumeroClient) {
    this.notreNumeroClient = notreNumeroClient;
  }
  
  public String getCodeReglement() {
    return codeReglement;
  }
  
  public void setCodeReglement(String codeReglement) {
    this.codeReglement = codeReglement;
  }
  
  public String getEcheance() {
    return echeance;
  }
  
  public void setEcheance(String echeance) {
    this.echeance = echeance;
  }
  
  public Integer getZoneSysteme() {
    return zoneSysteme;
  }
  
  public void setZoneSysteme(Integer zoneSysteme) {
    this.zoneSysteme = zoneSysteme;
  }
  
  public String getCodeLitige() {
    return codeLitige;
  }
  
  public void setCodeLitige(String codeLitige) {
    this.codeLitige = codeLitige;
  }
  
  public String getCodePaysCEE() {
    return codePaysCEE;
  }
  
  public void setCodePaysCEE(String codePaysCEE) {
    this.codePaysCEE = codePaysCEE;
  }
  
  public String getNumeroLibelleArticle() {
    return numeroLibelleArticle;
  }
  
  public void setNumeroLibelleArticle(String numeroLibelleArticle) {
    this.numeroLibelleArticle = numeroLibelleArticle;
  }
  
  public String getCodeAdresse() {
    return codeAdresse;
  }
  
  public void setCodeAdresse(String codeAdresse) {
    this.codeAdresse = codeAdresse;
  }
  
  public String getTopStats() {
    return topStats;
  }
  
  public void setTopStats(String topStats) {
    this.topStats = topStats;
  }
  
  public String getCumulRemises() {
    return cumulRemises;
  }
  
  public void setCumulRemises(String cumulRemises) {
    this.cumulRemises = cumulRemises;
  }
  
  public String getMargeSecurite() {
    return margeSecurite;
  }
  
  public void setMargeSecurite(String margeSecurite) {
    this.margeSecurite = margeSecurite;
  }
  
  public String getNombreBonRec() {
    return nombreBonRec;
  }
  
  public void setNombreBonRec(String nombreBonRec) {
    this.nombreBonRec = nombreBonRec;
  }
  
  public String getModeExpedition() {
    return modeExpedition;
  }
  
  public void setModeExpedition(String modeExpedition) {
    this.modeExpedition = modeExpedition;
  }
  
  public String getTransporteur() {
    return transporteur;
  }
  
  public void setTransporteur(String transporteur) {
    this.transporteur = transporteur;
  }
  
  public String getCodeRistourne() {
    return codeRistourne;
  }
  
  public void setCodeRistourne(String codeRistourne) {
    this.codeRistourne = codeRistourne;
  }
  
  public String getCodePaysTVAIntracom() {
    return codePaysTVAIntracom;
  }
  
  public void setCodePaysTVAIntracom(String codePaysTVAIntracom) {
    this.codePaysTVAIntracom = codePaysTVAIntracom;
  }
  
  public String getCodeInfoTVAIntracom() {
    return codeInfoTVAIntracom;
  }
  
  public void setCodeInfoTVAIntracom(String codeInfoTVAIntracom) {
    this.codeInfoTVAIntracom = codeInfoTVAIntracom;
  }
  
  public String getNumeroSIREN() {
    return numeroSIREN;
  }
  
  public void setNumeroSIREN(String numeroSIREN) {
    this.numeroSIREN = numeroSIREN;
  }
  
  public String getComplementSIRET() {
    return complementSIRET;
  }
  
  public void setComplementSIRET(String complementSIRET) {
    this.complementSIRET = complementSIRET;
  }
  
  public BigDecimal getMaximumCommande() {
    return maximumCommande;
  }
  
  public void setMaximumCommande(BigDecimal maximumCommande) {
    this.maximumCommande = maximumCommande;
  }
  
  public Integer getNoteFournisseur() {
    return noteFournisseur;
  }
  
  public void setNoteFournisseur(Integer noteFournisseur) {
    this.noteFournisseur = noteFournisseur;
  }
  
  public String getRelicat() {
    return relicat;
  }
  
  public void setRelicat(String relicat) {
    this.relicat = relicat;
  }
  
  public String getExclusionWeb() {
    return exclusionWeb;
  }
  
  public void setExclusionWeb(String exclusionWeb) {
    this.exclusionWeb = exclusionWeb;
  }
  
  public String getDateEcheance() {
    return dateEcheance;
  }
  
  public void setDateEcheance(String dateEcheance) {
    this.dateEcheance = dateEcheance;
  }
  
  public String getDelaiJours() {
    return delaiJours;
  }
  
  public void setDelaiJours(String delaiJours) {
    this.delaiJours = delaiJours;
  }
  
  public Integer getNumeroCompteGeneral() {
    return numeroCompteGeneral;
  }
  
  public void setNumeroCompteGeneral(Integer numeroCompteGeneral) {
    this.numeroCompteGeneral = numeroCompteGeneral;
  }
  
  public Integer getNumeroCompteAuxiliaire() {
    return numeroCompteAuxiliaire;
  }
  
  public void setNumeroCompteAuxiliaire(Integer numeroCompteAuxiliaire) {
    this.numeroCompteAuxiliaire = numeroCompteAuxiliaire;
  }
  
  public String getCodeAPE() {
    return codeAPE;
  }
  
  public void setCodeAPE(String codeAPE) {
    this.codeAPE = codeAPE;
  }
  
  public String getFournisseurInterne() {
    return fournisseurInterne;
  }
  
  public void setFournisseurInterne(String fournisseurInterne) {
    this.fournisseurInterne = fournisseurInterne;
  }
  
  public String getCodeExpeDILICOM() {
    return codeExpeDILICOM;
  }
  
  public void setCodeExpeDILICOM(String codeExpeDILICOM) {
    this.codeExpeDILICOM = codeExpeDILICOM;
  }
  
  public String getCodeNoteDILICOM() {
    return codeNoteDILICOM;
  }
  
  public void setCodeNoteDILICOM(String codeNoteDILICOM) {
    this.codeNoteDILICOM = codeNoteDILICOM;
  }
  
  public String getCodeAPEv2() {
    return codeAPEv2;
  }
  
  public void setCodeAPEv2(String codeAPEv2) {
    this.codeAPEv2 = codeAPEv2;
  }
  
  public String getMarque() {
    return marque;
  }
  
  public void setMarque(String marque) {
    this.marque = marque;
  }
  
  public Integer getCollectifRegroupement() {
    return collectifRegroupement;
  }
  
  public void setCollectifRegroupement(Integer collectifRegroupement) {
    this.collectifRegroupement = collectifRegroupement;
  }
  
  public Integer getFournisseurRegroupement() {
    return fournisseurRegroupement;
  }
  
  public void setFournisseurRegroupement(Integer fournisseurRegroupement) {
    this.fournisseurRegroupement = fournisseurRegroupement;
  }
  
  public String getPrefixeArticle() {
    return prefixeArticle;
  }
  
  public void setPrefixeArticle(String prefixeArticle) {
    this.prefixeArticle = prefixeArticle;
  }
  
  public BigDecimal getCoeffApproche() {
    return coeffApproche;
  }
  
  public void setCoeffApproche(BigDecimal coeffApproche) {
    this.coeffApproche = coeffApproche;
  }
  
  public Integer getDelaiLivraison() {
    return delaiLivraison;
  }
  
  public void setDelaiLivraison(Integer delaiLivraison) {
    this.delaiLivraison = delaiLivraison;
  }
  
  public Integer getDelaiLivraison2() {
    return delaiLivraison2;
  }
  
  public void setDelaiLivraison2(Integer delaiLivraison2) {
    this.delaiLivraison2 = delaiLivraison2;
  }
  
  public String getCleClassement2() {
    return cleClassement2;
  }
  
  public void setCleClassement2(String cleClassement2) {
    this.cleClassement2 = cleClassement2;
  }
  
  public BlocNote getBlocNote() {
    return blocNote;
  }
  
  public void setBlocNote(BlocNote pBlocNote) {
    this.blocNote = pBlocNote;
  }
  
  public Boolean isModeEnlevement() {
    return modeEnlevement;
  }
  
  public void setModeEnlevement(Boolean modeEnlevement) {
    this.modeEnlevement = modeEnlevement;
  }
  
  public EnumTypePrixAchat getTypePrixAchat() {
    return typePrixAchat;
  }
  
  public void setTypePrixAchat(EnumTypePrixAchat typePrixAchat) {
    this.typePrixAchat = typePrixAchat;
  }
  
  public EnumOptionEdition getTypeOptionChiffrageEdition() {
    return typeOptionChiffrageEdition;
  }
  
  public void setTypeOptionChiffrageEdition(EnumOptionEdition typeOptionChiffrageEdition) {
    this.typeOptionChiffrageEdition = typeOptionChiffrageEdition;
  }
  
}
