/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.codepostal;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste des code postaux
 */
public class ListeCodePostal extends ListeClasseMetier<IdCodePostal, CodePostal, ListeCodePostal> {
  
  /**
   * Charge la liste de CodePostal suivant une liste d'IdCodePostal
   */
  @Override
  public ListeCodePostal charger(IdSession pIdSession, List<IdCodePostal> pListeId) {
    return null;
  }
  
  /**
   * Charge tout les CodePostal suivant l'établissement
   */
  @Override
  public ListeCodePostal charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CriteresCodePostal criteres = new CriteresCodePostal();
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeCodePostal(pIdSession, criteres);
  }
  
  /**
   * Charge une liste de CodePostal suivant le nom de la ville contenu dans une String
   */
  public ListeCodePostal chargerParVille(IdSession pIdSession, IdEtablissement pIdEtablissement, String pNomVille) {
    CriteresCodePostal criteres = new CriteresCodePostal();
    criteres.setIdEtablissement(pIdEtablissement);
    criteres.setNomVille(pNomVille);
    return ManagerServiceParametre.chargerListeCodePostal(pIdSession, criteres);
  }
}
