/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.lot;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un lot.
 *
 * L'identifiant est composé du code établissement, d'un identifiant d'article, d'un identifiant de magasin et du code du lot.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdLot extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_CODE = 25;
  public static final int LONGUEUR_INDICATIF = LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_CODE;
  
  public static final int INDICATIF_NUM = 0;
  public static final int INDICATIF_ETB_NUM = 1;
  
  // Variables
  private final IdArticle idArticle;
  private final IdMagasin idMagasin;
  private final String code;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdLot(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, IdArticle pIdArticle, IdMagasin pIdMagasin,
      String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    idArticle = IdArticle.controlerId(pIdArticle, true);
    idMagasin = IdMagasin.controlerId(pIdMagasin, true);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdLot getInstance(IdEtablissement pIdEtablissement, IdArticle pIdArticle, IdMagasin pIdMagasin, String pCode) {
    return new IdLot(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pIdArticle, pIdMagasin, pCode);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdLot getInstanceAvecCreationId(IdEtablissement pIdEtablissement, IdArticle pIdArticle, IdMagasin pIdMagasin,
      String pCode) {
    return new IdLot(EnumEtatObjetMetier.CREE, pIdEtablissement, pIdArticle, pIdMagasin, pCode);
  }
  
  /**
   * 
   * Contrôler la validité du code du lot..
   * 
   * @param pValeur
   * @return String Id valide
   */
  private String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code du lot n'est pas renseigné.");
    }
    if (pValeur.trim().isEmpty()) {
      throw new MessageErreurException("Le code du lot est vide.");
    }
    if (pValeur.length() > Constantes.valeurMaxZoneNumerique(LONGUEUR_CODE)) {
      throw new MessageErreurException("Le code du lot est trop long (La longueur maximale est de " + LONGUEUR_CODE + ".");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdLot controlerId(IdLot pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du lot est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du lot n'existe pas dans la base de données : " + pId.toString());
    }
    return pId;
  }
  
  // -- Méthodes publiques
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getCodeEtablissement().hashCode();
    code = 37 * code + getIdMagasin().hashCode();
    code = 37 * code + getIdArticle().hashCode();
    code = 37 * code + getCode().hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdLot)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de lots.");
    }
    IdLot id = (IdLot) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && getIdMagasin().equals(id.getIdMagasin())
        && getIdArticle().equals(id.getIdArticle()) && getCode().equals(id.getCode());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdLot)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdLot id = (IdLot) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = getIdMagasin().compareTo(id.getIdMagasin());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = getIdArticle().compareTo(id.getIdArticle());
    if (comparaison != 0) {
      return comparaison;
    }
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return getCode();
  }
  
  // -- Accesseurs
  /**
   * 
   * Retourner l'identifiant de l'article loti.
   * 
   * @return IdArticle
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * 
   * Retourner l'identifiant du magasin du lot.
   * 
   * @return IdMagasin
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * 
   * Retourner le code qui identifie le lot.
   * 
   * @return String
   */
  public String getCode() {
    return code;
  }
}
