/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

public class CategorieZonePersonnalisee extends AbstractClasseMetier<IdCategorieZonePersonnalisee> {
  // Constantes
  
  // Variables
  private String nom = "";
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public CategorieZonePersonnalisee(IdCategorieZonePersonnalisee pIdCategorieZonePersonnalisee) {
    super(pIdCategorieZonePersonnalisee);
  }
  
  @Override
  public String getTexte() {
    return nom;
  }
  
  // -- Accesseurs
  /**
   * Retourne le nom
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Modifier le nom
   */
  public void setNom(String pNom) {
    nom = pNom;
  }
  
}
