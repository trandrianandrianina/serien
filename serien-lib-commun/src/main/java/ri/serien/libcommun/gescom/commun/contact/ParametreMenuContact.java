/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact;

import ri.serien.libcommun.commun.EnumModeUtilisation;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.liencontact.IdLienContact;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;

public class ParametreMenuContact {
  // Variables
  private EnumModeUtilisation modeUtilisation = EnumModeUtilisation.CONSULTATION;
  private IdClient idClient = null;
  private IdFournisseur idFournisseur = null;
  private IdLienContact idLienContact = null;
  
  // -- Accesseurs
  
  /**
   * Mode d'utilisation de l'écran des contacts
   */
  public EnumModeUtilisation getModeUtilisation() {
    return modeUtilisation;
  }
  
  /**
   * Mode d'utilisation de l'écran des contacts
   */
  public void setModeUtilisation(EnumModeUtilisation pModeUtilisation) {
    modeUtilisation = pModeUtilisation;
  }
  
  /**
   * Identifiant du client pour lequel on consulte les contacts
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Identifiant du client pour lequel on consulte les contacts
   */
  public void setIdClient(IdClient idClient) {
    this.idClient = idClient;
  }
  
  /**
   * Identifiant du fournisseur pour lequel on consulte les contacts
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Identifiant du fournisseur pour lequel on consulte les contacts
   */
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
  
  /**
   * Identifiant du LienContact pour lequel on consulte les contacts
   */
  public IdLienContact getIdLienContact() {
    return idLienContact;
  }
  
  /**
   * Identifiant du LienContact pour lequel on consulte les contacts
   */
  public void setIdLienContact(IdLienContact idLienContact) {
    this.idLienContact = idLienContact;
  }
  
  /**
   * Retourne le point de menu correspondant.
   */
  @Override
  public String toString() {
    return "[EXP66] Exploitation -> Personnalisation, sécurité -> Contacts";
  }
}
