/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.origine;

/**
 * Liste des origines possibles pour le prix net HT.
 * Les origines sont classées par ordre de priorité (du plus prioritaire vers le moins prioritaire).
 */
public enum EnumOriginePrixNet {
  PRIX_BASE_SAISI_GRATUIT("Prix de base saisi gratuit"),
  LIGNE_VENTE_GRATUITE("Ligne de vente gratuite"),
  PRIX_NET_SAISI_GRATUIT("Prix net saisi gratuit"),
  CONDITION_VENTE_GRATUITE("Condition de vente gratuite"),
  CHANTIER("Prix chantier"),
  LIGNE_VENTE("Ligne de vente"),
  FORMULE_PRIX_CONDITION_VENTE("Formule prix condition de vente"),
  PRIX_NET_HT_CONDITION_VENTE("Prix net condition de vente"),
  PRIX_BASE_REMISE("Prix de base remisé"),
  PRIX_BASE_AUGMENTE("Prix de base augmenté"),
  PRIX_BASE("Prix de base"),
  PRIX_BASE_COLONNE_TARIF("Prix de base colonne tarif"),
  INVALIDE("Aucun prix net valide");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOriginePrixNet(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
