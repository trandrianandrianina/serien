/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.codepostal;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un code postal.
 *
 * L'identifiant est composé du code postal.
 * Il est constitué de 5 caractères et se présente sous la forme NNNNN
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique
 * facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdCodePostal extends AbstractId {
  
  private static final int LONGUEUR = 5;
  
  // Variables
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdCodePostal(EnumEtatObjetMetier pEnumEtatObjetMetier, Integer pCodePostal) {
    super(pEnumEtatObjetMetier);
    numero = controlerCode(pCodePostal);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdCodePostal getInstance(Integer pCodePostal) {
    return new IdCodePostal(EnumEtatObjetMetier.MODIFIE, pCodePostal);
  }
  
  /**
   * Contrôler la validité du code postal.
   * Le code postal est une chaîne numérique de 5 caractères.
   */
  private static Integer controlerCode(Integer pCodePostal) {
    if (pCodePostal == null) {
      throw new MessageErreurException("Le code postal n'est pas renseigné.");
    }
    // permet de savoir la taille du code postal
    int tailleCodePostal = String.valueOf(pCodePostal).length();
    String valeurCodePostal = String.valueOf(pCodePostal);
    
    // Rajoute un 0 devant les code postaux afin qu'il soit à la bonne taille
    if (tailleCodePostal < LONGUEUR || tailleCodePostal <= 0) {
      for (int i = 0 + tailleCodePostal; i < LONGUEUR; i++) {
        valeurCodePostal = '0' + valeurCodePostal;
      }
    }
    
    // permet de savoir la taille fianl du code postal
    int tailleFinalCodePostal = String.valueOf(valeurCodePostal).length();
    
    // vérification de la taille du code postal
    if (tailleFinalCodePostal != LONGUEUR) {
      throw new MessageErreurException("Le code postal doit comporter " + LONGUEUR + " caractères : ");
    }
    return pCodePostal;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + numero.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdCodePostal)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux code postaux.");
    }
    IdCodePostal id = (IdCodePostal) pObject;
    return numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdCodePostal)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdCodePostal id = (IdCodePostal) pObject;
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    String codePostalString = String.valueOf(numero);
    return codePostalString;
  }
  
  // -- Accesseurs
  
  /**
   * Code postal.
   * Le code postal est une chaîne numérique de 5 caractères.
   */
  public Integer getCodePostal() {
    return numero;
  }
  
  /**
   * Retourne true si le code postal n'est pas définit.
   */
  public Boolean isNonDefini() {
    if (numero == null || numero <= 0) {
      return true;
    }
    else {
      return false;
    }
  }
}
