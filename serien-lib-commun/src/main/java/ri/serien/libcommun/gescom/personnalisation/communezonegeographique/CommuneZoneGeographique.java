/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.communezonegeographique;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Commune d'une zone géographique.
 * 
 * Utilisée uniquement dans le cadre des zones géographiques, cela permet de définir quelles communes font parties d'une zone
 * géographique. Cette configuration peut être différente d'un magasin à l'autre.
 * 
 * Cette notion est disctincte de la notion de commune (classe Commune), utilisée ailleurs dans le logiciel.
 */
public class CommuneZoneGeographique extends AbstractClasseMetier<IdCommuneZoneGeographique> {
  // Variable
  private String ville = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public CommuneZoneGeographique(IdCommuneZoneGeographique pId) {
    super(pId);
  }
  
  @Override
  public String getTexte() {
    return getVille();
  }
  // -- Accesseurs
  
  /**
   * Renvoyer le nom de la commune.
   */
  public String getVille() {
    return ville;
  }
  
  /**
   * Mettre à jour le nom de la commune.
   */
  public void setVille(String ville) {
    this.ville = ville;
  }
  
}
