/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceArticle;

/**
 * Liste d'articles base.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste d'articles base.
 * La méthode chargerTout permet d'obtenir la liste de tous les Articles d'un établissement.
 */
public class ListeArticleBase extends ListeClasseMetier<IdArticle, ArticleBase, ListeArticleBase> {
  /**
   * Constructeur.
   */
  public ListeArticleBase() {
  }
  
  /**
   * Charger une liste d'ArticleBase suivant les critères donnés
   */
  public static ListeArticleBase charger(IdSession pidSession, CritereArticle pCritereArticle) {
    return ManagerServiceArticle.chargerListeArticleBase(pidSession, pCritereArticle);
  }
  
  /**
   * Charger les articles de base dont les identifiants sont fournis.
   */
  public static ListeArticleBase chargerParId(IdSession pidSession, List<IdArticle> pListeIdArticle) {
    return ManagerServiceArticle.chargerListeArticleBase(pidSession, pListeIdArticle);
  }
  
  /**
   * Charger les articles de base dont les identifiants sont fournis.
   */
  @Override
  public ListeArticleBase charger(IdSession pIdSession, List<IdArticle> pListeIdArticle) {
    return ListeArticleBase.chargerParId(pIdSession, pListeIdArticle);
  }
  
  /**
   * Charger les articles de base d'un établissement.
   */
  @Override
  public ListeArticleBase charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Construire une liste d'articles de basqe correspondant à la liste d'identifiants fournie en paramètre.
   * Les donnéesne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeArticleBase creerListeNonChargee(List<IdArticle> pListeIdArticle) {
    ListeArticleBase listeArticleBase = new ListeArticleBase();
    if (pListeIdArticle != null) {
      for (IdArticle idArticle : pListeIdArticle) {
        ArticleBase articleBase = new ArticleBase(idArticle);
        articleBase.setCharge(false);
        listeArticleBase.add(articleBase);
      }
    }
    return listeArticleBase;
  }
  
}
