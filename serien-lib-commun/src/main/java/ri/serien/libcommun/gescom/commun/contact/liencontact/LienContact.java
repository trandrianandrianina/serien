/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact.liencontact;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeWorkflow;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Classe définissant un lien entre un contact (personne physique) et un tiers (client ou fournisseur)
 * le lien entre tiers et contact est fait par un fichier de lien et stocké dans le contact dans la liste des liens (listLienContactTiers)
 */
public class LienContact extends AbstractClasseMetier<IdLienContact> {
  
  // Variables
  private IdEtablissement idEtablissementContact = null;
  private boolean contactPrincipal = false;
  private boolean contactWebshop = false;
  private EnumTypeWorkflow typeWorkflow = null;
  private char rlin4 = ' ';
  private Boolean isParticulier = false;
  
  // Variables de travail
  private String libelleTiers = "";
  private Adresse adresseTiers = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public LienContact(IdLienContact pIdLienContact) {
    super(pIdLienContact);
  }
  
  /**
   * Renvoit le nom complet du lien contact suivi de son numéro entre parenthèses.
   * C'est cette méthode qui définit le format d'affichage des liens contact dans les composants graphiques.
   * Format : NomComplet (CodeContact)
   */
  @Override
  public String getTexte() {
    return libelleTiers;
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public LienContact clone() {
    Object o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return (LienContact) o;
  }
  
  // Méthodes privées
  
  // -- Accesseurs
  
  /**
   * Etablissement du contact (donnée non utilisée).
   */
  public IdEtablissement getIdEtablissementContact() {
    return idEtablissementContact;
  }
  
  /**
   * Etablissement du contact (donnée non utilisée).
   */
  public void setIdEtablissementContact(IdEtablissement idEtablissementContact) {
    this.idEtablissementContact = idEtablissementContact;
  }
  
  /**
   * Renvoyer si le contact est le contact principal du tiers
   */
  public boolean isContactPrincipal() {
    return contactPrincipal;
  }
  
  /**
   * Renvoyer si le contact est le contact principal du tiers
   */
  public void setContactPrincipal(boolean contactPrincipal) {
    this.contactPrincipal = contactPrincipal;
  }
  
  /**
   * Renvoyer si le contact est un contact WebShop
   */
  public boolean isContactWebshop() {
    return contactWebshop;
  }
  
  /**
   * Renvoyer si le contact est un contact WebShop
   */
  public void setContactWebshop(boolean contactWebshop) {
    this.contactWebshop = contactWebshop;
  }
  
  /**
   * Renvoyer le type de workflow utilisé pour le contact
   */
  public EnumTypeWorkflow getTypeWorkflow() {
    return typeWorkflow;
  }
  
  /**
   * Renvoyer le type de workflow utilisé pour le contact
   */
  public void setTypeWorkflow(EnumTypeWorkflow typeWorkflow) {
    this.typeWorkflow = typeWorkflow;
  }
  
  /**
   * Non utilisé
   */
  public char getRlin4() {
    return rlin4;
  }
  
  /**
   * Non utilisé
   */
  public void setRlin4(char rlin4) {
    this.rlin4 = rlin4;
  }
  
  /**
   * Retourne le libellé + l'identifiant de tiers pour affichage dans les listes
   */
  public String getTexteTiers() {
    if (id.isLienClient()) {
      return libelleTiers + " (" + id.getIdClient().getTexte() + ")";
    }
    else {
      return libelleTiers + " (" + id.getIdFournisseur().getTexte() + ")";
    }
  }
  
  public String getLibelleTiers() {
    return libelleTiers;
  }
  
  public void setLibelleTiers(String pLibelleTiers) {
    libelleTiers = pLibelleTiers;
  }
  
  public Adresse getAdresseTiers() {
    return adresseTiers;
  }
  
  public void setAdresseTiers(Adresse adresseTiers) {
    this.adresseTiers = adresseTiers;
  }
  
  /**
   * Le lien fait-il référence à un particulier ?
   */
  public Boolean isParticulier() {
    return isParticulier;
  }
  
  /**
   * Le lien fait-il référence à un particulier ?
   */
  public void setIsParticulier(Boolean isParticulier) {
    this.isParticulier = isParticulier;
  }
}
