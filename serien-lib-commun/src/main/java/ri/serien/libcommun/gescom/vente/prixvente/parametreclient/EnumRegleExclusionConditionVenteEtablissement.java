/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreclient;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Règles d'exclusion des conditions de ventes établissement.
 * 
 * Correspond au champ "Condition générale sur la DG" dans la boîte de dialogue facturation de la fiche client (champ CLIN4).
 * Ce champ peut avoir les valeurs suivantes :
 * - ' ' = Pas d'exclusion.
 * - '1' = Exclusion condition de vente générale (ParamDG).
 * - '2' = Exclusion condition de vente sur les articles liés.
 * - '3' = Exclusion condition de vente générale et les articles liés.
 * - '4' = Si une condition client existe, elle prime.
 */
public enum EnumRegleExclusionConditionVenteEtablissement {
  SANS_EXCLUSION(' ', "Pas d'exclusion"),
  EXCLUSION_CNV_ETABLISSEMENT('1', "Exclusion condition de vente générale (ParamDG)"),
  EXCLUSION_CNV_ETABLISSEMENT_SUR_ARTICLE_LIE('2', "Exclusion condition de vente sur les articles liés"),
  EXCLUSION_CNV_ETABLISSEMENT_ET_ARTICLE_LIE('3', "Exclusion condition de vente générale et les articles liés"),
  EXCLUSION_CNV_ETABLISSEMENT_SI_CNV_CLIENT('4', "Si une condition client existe, elle prime");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumRegleExclusionConditionVenteEtablissement(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getcode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle + " (" + String.valueOf(code) + ")";
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumRegleExclusionConditionVenteEtablissement valueOfByCode(Character pCode) {
    for (EnumRegleExclusionConditionVenteEtablissement value : values()) {
      if (pCode.equals(value.getcode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code de la règle d'exclusion des conditions de ventes établissement est invalide : " + pCode);
  }
  
}
