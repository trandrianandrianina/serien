/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.conditionachat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Condition d'achats.
 * 
 * Sauf explicitement mentionné, tous les montants de cette classe sont exprimés en unité d'achats.
 */
public class ConditionAchat implements Serializable {
  // Constantes
  public final static Character FOURNISSEUR_GBA = 'G';
  public final static Character FOURNISSEUR_REFERENCE = 'R';
  public final static Character PRIX_PROMOTION = 'P';
  public final static Character PRIX_COLONNES = 'C';
  public final static int NOMBRE_DECIMALE_REMISE = 0;
  public final static String ERREUR_ARTICLE_INEXISTANT = "L'article est inexistant.";
  public final static String ERREUR_CONDITION_ACHAT_NON_TROUVEE = "La condition d'achat n'a pas été trouvé.";
  
  // Variables fichiers
  private Date dateCreation = null; // Date de création
  private Date dateModification = null; // Date de modification
  private Date dateTraitement = null; // Date de traitement
  private IdEtablissement idEtablissement = null; // Code établissement
  private IdArticle idArticle = null; // Code article
  private IdFournisseur idFournisseur = null;
  private Date dateApplication = null; // Date d'application du tarif
  private Character topCalculPrixVente = Character.valueOf(' '); // Top calcul prix de vente
  private IdUnite idUniteAchat = null; // Unité d'achat
  private String uniteCommande = ""; // Unité de commande
  private BigDecimal conditionnement = BigDecimal.ZERO; // Conditionnement
  private BigDecimal coefficientUAparUCA = BigDecimal.ZERO;
  private BigDecimal coefficientUSparUCA = BigDecimal.ZERO;
  private int delaiLivraison = 0; // Délai de livraison
  private String referenceFournisseur = ""; // Référence fournisseur
  private int nombreDecimalesUCA = 0;
  private int nombreDecimalesUA = 0;
  private String codeDevise = "";// Devise du tarif
  private BigDecimal quantiteMinimumCommande = BigDecimal.ZERO; // Quantité minimum de commande
  private BigDecimal quantiteEconomique = BigDecimal.ZERO; // Quantité économique
  private BigDecimal coefficientCalculPrixVente = BigDecimal.ZERO; // Coeff. calcul prix de vente
  private Date dateDernierPrix = null; // Date de dernier prix
  private String regroupementAchat = ""; // Regroupement Achat
  private int delaiSupplementaire = 0; // Délai supplémentaire')
  private Character originePrix = Character.valueOf(' '); // P=Promo, C=Colonnes
  private Character noteQualite = Character.valueOf(' '); // Note qualité de 0 à 9
  private Character typeFournisseur = Character.valueOf(' ');// G = Frs GBA / R= Frs Réf
  private Character remiseCalculPVminimum = Character.valueOf(' '); // 3 remises pour calcul PV min
  private Character typeRemiseLigne = Character.valueOf(' '); // Type remise ligne
  private String referenceConstructeur = ""; // Référence constructeur
  private int gencodFournisseur = 0; // Code gencod frs
  private Date dateLimiteValidite = null; // Date limite validité
  private String libelle = ""; // Libellé
  private String origine = ""; // origine
  private Character delaiJours = Character.valueOf(' '); // Delai en jours et Non semaine
  private BigDecimal prixDistributeur = BigDecimal.ZERO; // Prix distributeur
  private Character topPrixNegocie = Character.valueOf(' '); // Top prix négocié
  private Character uniteFrais1 = Character.valueOf(' '); // Unité frais 1
  private BigDecimal fraisEnValeur2 = BigDecimal.ZERO; // Frais 2 valeur
  private BigDecimal fraisEnCoeff2 = BigDecimal.ZERO; // Frais 2 coefficient
  private Character uniteFrais2 = Character.valueOf(' '); // Unité frais 2
  private Character uniteFrais3 = Character.valueOf(' '); // Unité frais 3
  // en negociation achat
  private EnumTypeConditionAchat typeCondition = null; // Type de condition
  
  private Character codeERL = Character.valueOf(' '); // Code ERL
  private int numeroBon = 0;// Numéro du bon
  private int suffixeBon = 0; // Suffixe du bon
  private int numeroLigne = 0; // numéro de ligne sur laquelle porte la négo d'achat
  
  // Variables d'erreur
  private boolean articleInexistant = false;
  private boolean conditionAchatNonTrouvee = false;
  
  private boolean erreurDetectee = false;
  private StringBuilder messageErreur = new StringBuilder();
  
  // Eléments de calcul du prix d'achats
  private BigDecimal prixAchatBrutHT = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise1 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise2 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise3 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise4 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise5 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise6 = BigDecimal.ZERO;
  private BigDecimal coefficientRemise = BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP);
  private BigDecimal pourcentageTaxeOuMajoration = BigDecimal.ZERO;
  private BigDecimal montantConditionnement = BigDecimal.ZERO;
  private BigDecimal prixAchatNetHT = BigDecimal.ZERO;
  private BigDecimal montantAuPoidsPortHT = BigDecimal.ZERO;
  private EnumModeSaisiePort modeSaisiePort = EnumModeSaisiePort.MONTANT_FOIS_POIDS;
  private BigDecimal poidsPort = BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP);
  private BigDecimal pourcentagePort = BigDecimal.ZERO;
  private BigDecimal montantPortHT = BigDecimal.ZERO;
  private BigDecimal prixRevientFournisseurHT = BigDecimal.ZERO;
  private BigDecimal prixRevientFournisseurHTEnUS = BigDecimal.ZERO;
  private BigDecimal fraisExploitation = BigDecimal.ZERO;
  private BigDecimal coefficientApprocheFixe = BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP);
  private BigDecimal prixRevientStandardHT = BigDecimal.ZERO;
  private BigDecimal prixRevientStandardHTEnUS = BigDecimal.ZERO;
  
  // -- Méthodes publiques
  
  /**
   * Controle s'il y a des erreurs.
   */
  public boolean isErreursDetectees(boolean pAfficheMessage) {
    boolean erreur = false;
    StringBuilder message = new StringBuilder();
    
    if (articleInexistant) {
      erreur = true;
      message.append(ERREUR_ARTICLE_INEXISTANT).append('\n');
    }
    if (conditionAchatNonTrouvee) {
      erreur = true;
      message.append(ERREUR_CONDITION_ACHAT_NON_TROUVEE).append('\n');
    }
    
    // Si on a demandé l'affichage des erreurs alors on déclenche une exception
    if (erreur && pAfficheMessage) {
      throw new MessageErreurException(message.toString());
    }
    return erreur;
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialisation des valeurs contribuant aux calculs de prix dans la CNA.
   */
  public void initialiser(BigDecimal pPrixAchatBrutHT, BigDecimal pPrixAchatNetHT, BigDecimal pPrixRevientFournisseurHT,
      BigDecimal pPrixRevientStandardHT, BigDecimal pRemise1, BigDecimal pRemise2, BigDecimal pRemise3, BigDecimal pRemise4,
      BigDecimal pCoefficientRemise, BigDecimal pPourcentageTaxe, BigDecimal pMontantConditionnement, BigDecimal pMontantPort,
      BigDecimal pCoefficientPort, BigDecimal pPourcentagePort, BigDecimal pFraisExploitation) {
    prixAchatBrutHT = pPrixAchatBrutHT;
    prixAchatNetHT = pPrixAchatNetHT;
    prixRevientFournisseurHT = pPrixRevientFournisseurHT;
    prixRevientStandardHT = pPrixRevientStandardHT;
    pourcentageRemise1 = pRemise1;
    pourcentageRemise2 = pRemise2;
    pourcentageRemise3 = pRemise3;
    pourcentageRemise4 = pRemise4;
    coefficientRemise = pCoefficientRemise;
    pourcentageTaxeOuMajoration = pPourcentageTaxe;
    montantConditionnement = pMontantConditionnement;
    montantAuPoidsPortHT = pMontantPort;
    poidsPort = pCoefficientPort;
    pourcentagePort = pPourcentagePort;
    fraisExploitation = pFraisExploitation;
    
    // C'est le montant du poids au port qui décide du mode de saisie du port
    if (montantAuPoidsPortHT.compareTo(BigDecimal.ZERO) > 0 || pourcentagePort.compareTo(BigDecimal.ZERO) == 0) {
      modeSaisiePort = EnumModeSaisiePort.MONTANT_FOIS_POIDS;
    }
    else {
      modeSaisiePort = EnumModeSaisiePort.POURCENTAGE_PRIX_NET;
    }
    calculer();
  }
  
  // -- Méthodes privées
  
  /**
   * Recalcule les valeurs de la condition d'achat
   */
  private void calculer() {
    calculerPrixAchatNetHT();
    calculerMontantPortHT();
    calculerPrixRevientFournisseurHT();
    calculerPrixRevientFournisseurHTEnUS();
    calculerPrixRevientStandardHT();
    calculerPrixRevientStandardHTEnUS();
  }
  
  /**
   * Calculer le prix d'achat net HT.
   */
  private void calculerPrixAchatNetHT() {
    // Appliquer les pourcentages de remise
    // UPNET0 = CAPRA*(100-CAREM1)/100*(100-CAREM2)/100*(100-CAREM3)/100*(100-CAREM4)/100*(100-CAREM5)/100*(100-CAREM6)/100
    prixAchatNetHT = prixAchatBrutHT
        .multiply((Constantes.VALEUR_CENT.subtract(pourcentageRemise1).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply((Constantes.VALEUR_CENT.subtract(pourcentageRemise2).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply((Constantes.VALEUR_CENT.subtract(pourcentageRemise3).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply((Constantes.VALEUR_CENT.subtract(pourcentageRemise4).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply((Constantes.VALEUR_CENT.subtract(pourcentageRemise5).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)))
        .multiply((Constantes.VALEUR_CENT.subtract(pourcentageRemise6).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)));
    
    // Appliquer le coefficient de remise
    // UPNET1=UPNET0*CAKPR
    if (coefficientRemise.compareTo(BigDecimal.ZERO) > 0) {
      prixAchatNetHT = prixAchatNetHT.multiply(coefficientRemise);
    }
    
    // Appliquer le pourcentage de taxe ou de majoration
    // UPNET2= UPNET1+(UPNET1*CAFK3/100)
    BigDecimal coefficientTaxeOuMajoration =
        BigDecimal.ONE.add(pourcentageTaxeOuMajoration.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32));
    prixAchatNetHT = prixAchatNetHT.multiply(coefficientTaxeOuMajoration);
    
    // Appliquer le montant de conditionnement
    // CAPANET=UPNET2+CAFV3
    prixAchatNetHT = prixAchatNetHT.add(montantConditionnement);
    
    // Arrondir
    prixAchatNetHT = prixAchatNetHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le montant total du port HT.
   * Cela peut se faire sous deux formes :
   * - soit une valeur (elle même calculée à partir d'une valeur unitaire multipliée par un coefficient représentant le poids)
   * - soit un pourcentage appliqué au prix net HT.
   */
  private void calculerMontantPortHT() {
    if (pourcentagePort.compareTo(BigDecimal.ZERO) > 0) {
      // FRAIS PORT=CAPANET*(CAFP1/100)
      montantPortHT = prixAchatNetHT.multiply(pourcentagePort.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32));
    }
    else {
      // FRAIS PORT=CAFV1*CAFK1 (si CAFK1 = 0 -> CAFK1=1)
      montantPortHT = montantAuPoidsPortHT.multiply(poidsPort);
    }
    
    // Arrondir
    montantPortHT = montantPortHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le prix de revient fournisseur HT.
   */
  private void calculerPrixRevientFournisseurHT() {
    // Appliquer le montant du port HT
    // CAPRF_UA=CAPANET+FRAIS PORT
    prixRevientFournisseurHT = prixAchatNetHT.add(montantPortHT);
    
    // Arrondir
    prixRevientFournisseurHT = prixRevientFournisseurHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le prix de revient fournisseur HT en US.
   * prixRevientFournisseurHTENUS = prixRevientFournisseurHT(UA) * US/UA.
   */
  private void calculerPrixRevientFournisseurHTEnUS() {
    // Calculer le prix de revient fournisseur en US.
    // prixUS = prixUA * (UA/UCA) / (US/UCA)
    prixRevientFournisseurHTEnUS =
        prixRevientFournisseurHT.multiply(getCoefficientUAparUCA()).divide(getCoefficientUSparUCA(), MathContext.DECIMAL32);
    
    // Arrondir
    prixRevientFournisseurHTEnUS = prixRevientFournisseurHTEnUS.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le prix de revient standard HT.
   */
  private void calculerPrixRevientStandardHT() {
    // Appliquer le pourcentage de majoration
    BigDecimal coefficientMajoration = BigDecimal.ONE.add(fraisExploitation.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32));
    prixRevientStandardHT = prixRevientFournisseurHT.multiply(coefficientMajoration);
    
    // Appliquer le coefficient d'approche fixe
    if (coefficientApprocheFixe.compareTo(BigDecimal.ZERO) > 0) {
      prixRevientStandardHT = prixRevientStandardHT.multiply(coefficientApprocheFixe);
    }
    
    // Arrondir
    prixRevientStandardHT = prixRevientStandardHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le prix de revient standard HT en US.
   * Le prix de revient standard en US n'est pas calculé à partir du prix de revient standard en UA. Il est calculé
   * à partir du prix de revient fournisseur en US suivant la même formule.
   */
  private void calculerPrixRevientStandardHTEnUS() {
    // Appliquer le pourcentage des frais d'exploitation
    BigDecimal coefficientMajoration = BigDecimal.ONE.add(fraisExploitation.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32));
    prixRevientStandardHTEnUS = prixRevientFournisseurHTEnUS.multiply(coefficientMajoration);
    
    // Appliquer le coefficient d'approche fixe
    if (coefficientApprocheFixe.compareTo(BigDecimal.ZERO) > 0) {
      prixRevientStandardHTEnUS = prixRevientFournisseurHTEnUS.multiply(coefficientApprocheFixe);
    }
    
    // Arrondir
    prixRevientStandardHTEnUS = prixRevientFournisseurHTEnUS.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  // -- Accesseurs
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public Date getDateModification() {
    return dateModification;
  }
  
  public void setDateModification(Date dateModification) {
    this.dateModification = dateModification;
  }
  
  public Date getDateTraitement() {
    return dateTraitement;
  }
  
  public void setDateTraitement(Date dateTraitement) {
    this.dateTraitement = dateTraitement;
  }
  
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    this.idEtablissement = pIdEtablissement;
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur pIdFournisseur) {
    idFournisseur = pIdFournisseur;
  }
  
  public Date getDateApplication() {
    return dateApplication;
  }
  
  public void setDateApplication(Date dateApplication) {
    this.dateApplication = dateApplication;
  }
  
  public Character getTopCalculPrixVente() {
    return topCalculPrixVente;
  }
  
  public void setTopCalculPrixVente(Character topCalculPrixVente) {
    this.topCalculPrixVente = topCalculPrixVente;
  }
  
  public IdUnite getIdUniteAchat() {
    return idUniteAchat;
  }
  
  public void setIdUniteAchat(IdUnite pIdUnite) {
    this.idUniteAchat = pIdUnite;
  }
  
  public String getUniteCommande() {
    return uniteCommande;
  }
  
  public void setUniteCommande(String uniteCommande) {
    this.uniteCommande = uniteCommande;
  }
  
  public BigDecimal getConditionnement() {
    return conditionnement;
  }
  
  public void setConditionnement(BigDecimal conditionnement) {
    this.conditionnement = conditionnement;
  }
  
  /**
   * Nombre d'unités d'achats par unité de commande d'achats (KAC).
   * 
   * Le coefficient KAC est le nombre d'unités d'achats pour une unité de commandes d'achats. Il est utilisé pour
   * convertir une quantité
   * exprimée en unité de commandes d'achats en une quantité exprimée en unités d'achats (et vice et versa).
   * 
   * Formulaire :
   * KAC = QTUA / QTUCA
   * QTUA = QTUCA * KAC
   * QTUCA = QTUA / KAC
   */
  public BigDecimal getCoefficientUAparUCA() {
    if (coefficientUAparUCA == null || coefficientUAparUCA.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ONE;
    }
    return coefficientUAparUCA;
  }
  
  /**
   * Modifier le nombre d'unités d'achats par unité de commande d'achats (KAC).
   */
  public void setCoefficientUAparUCA(BigDecimal pCoefficientUAparUCA) {
    coefficientUAparUCA = pCoefficientUAparUCA;
  }
  
  /**
   * Nombre d'unités de stocks par unité de commandes d'achats (KSC).
   * 
   * Le coefficient KSC est le nombre d'unité de stocks pour une unité de commande d'achats. Il est utilisé pour
   * convertir une quantité
   * exprimée en unités de commandes d'achats en une quantité exprimée en unités de stocks (et vice et versa).
   * 
   * Formulaire :
   * KSC = QTUS / QTUCA
   * QTUS = QTUC * KSC
   * QTUC = QTUS / KSC
   */
  public BigDecimal getCoefficientUSparUCA() {
    if (coefficientUSparUCA == null || coefficientUSparUCA.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ONE;
    }
    return coefficientUSparUCA;
  }
  
  /**
   * Modifier le nombre d'unités de stocks par unité de commandes d'achats (KSC).
   */
  public void setCoefficientUSparUCA(BigDecimal pCoefficientUSparUCA) {
    coefficientUSparUCA = pCoefficientUSparUCA;
  }
  
  public int getDelaiLivraison() {
    return delaiLivraison;
  }
  
  public void setDelaiLivraison(int delaiLivraison) {
    this.delaiLivraison = delaiLivraison;
  }
  
  public String getReferenceFournisseur() {
    return referenceFournisseur;
  }
  
  public void setReferenceFournisseur(String referenceFournisseur) {
    this.referenceFournisseur = referenceFournisseur;
  }
  
  public int getNombreDecimalesUCA() {
    return nombreDecimalesUCA;
  }
  
  public void setNombreDecimalesUCA(int podcc) {
    this.nombreDecimalesUCA = podcc;
  }
  
  public int getNombreDecimalesUA() {
    return nombreDecimalesUA;
  }
  
  public void setNombreDecimalesUA(int podca) {
    this.nombreDecimalesUA = podca;
  }
  
  public String getCodeDevise() {
    return codeDevise;
  }
  
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  public BigDecimal getQuantiteMinimumCommande() {
    return quantiteMinimumCommande;
  }
  
  public void setQuantiteMinimumCommande(BigDecimal quantiteMinimumCommande) {
    this.quantiteMinimumCommande = quantiteMinimumCommande;
  }
  
  public BigDecimal getQuantiteEconomique() {
    return quantiteEconomique;
  }
  
  public void setQuantiteEconomique(BigDecimal quantiteEconomique) {
    this.quantiteEconomique = quantiteEconomique;
  }
  
  public BigDecimal getCoefficientCalculPrixVente() {
    return coefficientCalculPrixVente;
  }
  
  public void setCoefficientCalculPrixVente(BigDecimal coefficientCalculPrixVente) {
    this.coefficientCalculPrixVente = coefficientCalculPrixVente;
  }
  
  public Date getDateDernierPrix() {
    return dateDernierPrix;
  }
  
  public void setDateDernierPrix(Date dateDernierPrix) {
    this.dateDernierPrix = dateDernierPrix;
  }
  
  public String getRegroupementAchat() {
    return regroupementAchat;
  }
  
  public void setRegroupementAchat(String regroupementAchat) {
    this.regroupementAchat = regroupementAchat;
  }
  
  public int getDelaiSupplementaire() {
    return delaiSupplementaire;
  }
  
  public void setDelaiSupplementaire(int delaiSupplementaire) {
    this.delaiSupplementaire = delaiSupplementaire;
  }
  
  public Character getOriginePrix() {
    return originePrix;
  }
  
  public void setOriginePrix(Character originePrix) {
    if (!originePrix.equals(PRIX_PROMOTION) || !originePrix.equals(PRIX_COLONNES)) {
      return;
    }
    this.originePrix = originePrix;
  }
  
  public Character getNoteQualite() {
    return noteQualite;
  }
  
  public void setNoteQualite(Character noteQualite) {
    this.noteQualite = noteQualite;
  }
  
  public Character getTypeFournisseur() {
    return typeFournisseur;
  }
  
  public void setTypeFournisseur(Character typeFournisseur) {
    if (!typeFournisseur.equals(FOURNISSEUR_GBA) || !typeFournisseur.equals(FOURNISSEUR_REFERENCE)) {
      return;
    }
    this.typeFournisseur = typeFournisseur;
  }
  
  public Character getRemiseCalculPVminimum() {
    return remiseCalculPVminimum;
  }
  
  public void setRemiseCalculPVminimum(Character remiseCalculPVminimum) {
    this.remiseCalculPVminimum = remiseCalculPVminimum;
  }
  
  public Character getTypeRemiseLigne() {
    return typeRemiseLigne;
  }
  
  public void setTypeRemiseLigne(Character typeRemiseLigne) {
    this.typeRemiseLigne = typeRemiseLigne;
  }
  
  public String getRefernceConstructeur() {
    return referenceConstructeur;
  }
  
  public void setReferenceConstructeur(String refernceConstructeur) {
    this.referenceConstructeur = refernceConstructeur;
  }
  
  public int getGencodFournisseur() {
    return gencodFournisseur;
  }
  
  public void setGencodFournisseur(int gencodFournisseur) {
    this.gencodFournisseur = gencodFournisseur;
  }
  
  public Date getDateLimiteValidite() {
    return dateLimiteValidite;
  }
  
  public void setDateLimiteValidite(Date dateLimiteValidite) {
    this.dateLimiteValidite = dateLimiteValidite;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  public String getOrigine() {
    return origine;
  }
  
  public void setOrigine(String origine) {
    this.origine = origine;
  }
  
  public Character getDelaiJours() {
    return delaiJours;
  }
  
  public void setDelaiJours(Character delaiJours) {
    this.delaiJours = delaiJours;
  }
  
  public BigDecimal getPrixDistributeur() {
    return prixDistributeur;
  }
  
  public void setPrixDistributeur(BigDecimal prixDistributeur) {
    this.prixDistributeur = prixDistributeur;
  }
  
  public Character getTopPrixNegocie() {
    return topPrixNegocie;
  }
  
  public void setTopPrixNegocie(Character topPrixNegocie) {
    this.topPrixNegocie = topPrixNegocie;
  }
  
  public Character getUniteFrais1() {
    return uniteFrais1;
  }
  
  public void setUniteFrais1(Character uniteFrais1) {
    this.uniteFrais1 = uniteFrais1;
  }
  
  public BigDecimal getFraisEnValeur2() {
    return fraisEnValeur2;
  }
  
  public void setFraisEnValeur2(BigDecimal fraisEnValeur2) {
    this.fraisEnValeur2 = fraisEnValeur2;
  }
  
  public BigDecimal getFraisEnCoeff2() {
    return fraisEnCoeff2;
  }
  
  public void setFraisEnCoeff2(BigDecimal fraisEnCoeff2) {
    this.fraisEnCoeff2 = fraisEnCoeff2;
  }
  
  public Character getUniteFrais2() {
    return uniteFrais2;
  }
  
  public void setUniteFrais2(Character uniteFrais2) {
    this.uniteFrais2 = uniteFrais2;
  }
  
  public Character getUniteFrais3() {
    return uniteFrais3;
  }
  
  public void setUniteFrais3(Character uniteFrais3) {
    this.uniteFrais3 = uniteFrais3;
  }
  
  /**
   * Renvoyer le type de condition d'achat
   */
  public EnumTypeConditionAchat getTypeCondition() {
    return typeCondition;
  }
  
  /**
   * Modifier le type de condition d'achat
   */
  public void setTypeCondition(EnumTypeConditionAchat pTypeCondition) {
    typeCondition = pTypeCondition;
  }
  
  public Character getCodeERL() {
    return codeERL;
  }
  
  public void setCodeERL(Character codeERL) {
    this.codeERL = codeERL;
  }
  
  public int getNumeroBon() {
    return numeroBon;
  }
  
  public void setNumeroBon(int numeroBon) {
    this.numeroBon = numeroBon;
  }
  
  public int getSuffixeBon() {
    return suffixeBon;
  }
  
  public void setSuffixeBon(int suffixeBon) {
    this.suffixeBon = suffixeBon;
  }
  
  public int getNumeroLigne() {
    return numeroLigne;
  }
  
  public void setNumeroLigne(int numeroLigne) {
    this.numeroLigne = numeroLigne;
  }
  
  public String getReferenceConstructeur() {
    return referenceConstructeur;
  }
  
  public boolean isArticleInexistant() {
    return articleInexistant;
  }
  
  public void setArticleInexistant(boolean articleInexistant) {
    this.articleInexistant = articleInexistant;
  }
  
  public boolean isConditionAchatNonTrouvee() {
    return conditionAchatNonTrouvee;
  }
  
  public void setConditionAchatNonTrouvee(boolean conditionAchatNonTrouvee) {
    this.conditionAchatNonTrouvee = conditionAchatNonTrouvee;
  }
  
  /**
   * Prix d'achat brut HT en unités d'achats (2 chiffres après la virgules).
   */
  public BigDecimal getPrixAchatBrutHT() {
    return prixAchatBrutHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le prix d'achat brut HT (en unités d'achats).
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPrixAchatBrutHT(BigDecimal pPrixAchatBrutHT) {
    if (pPrixAchatBrutHT == null) {
      throw new MessageErreurException("Le prix d'achat brut HT est invalide");
    }
    if (pPrixAchatBrutHT.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le prix d'achat brut HT doit être supérieur ou égal à 0");
    }
    prixAchatBrutHT = pPrixAchatBrutHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Pourcentage de remise 1 .
   */
  public BigDecimal getPourcentageRemise1() {
    return pourcentageRemise1.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le pourcentage de remise 1.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise1(BigDecimal pRemise) {
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 1 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_CENT) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 1 doit être compris entre 0 et 100 %.");
    }
    pourcentageRemise1 = pRemise.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Pourcentage de remise 2 .
   */
  public BigDecimal getPourcentageRemise2() {
    return pourcentageRemise2.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le pourcentage de remise 2 .
   */
  public void setPourcentageRemise2(BigDecimal pRemise) {
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 2 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_CENT) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 2 doit être compris entre 0 et 100 %.");
    }
    pourcentageRemise2 = pRemise.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Pourcentage de remise 3 .
   */
  public BigDecimal getPourcentageRemise3() {
    return pourcentageRemise3.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le pourcentage de remise 3.
   */
  public void setPourcentageRemise3(BigDecimal pRemise) {
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 3 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_CENT) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 3 doit être compris entre 0 et 100 %.");
    }
    pourcentageRemise3 = pRemise.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Pourcentage de remise 4 .
   */
  public BigDecimal getPourcentageRemise4() {
    return pourcentageRemise4.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le pourcentage de remise 4.
   */
  public void setPourcentageRemise4(BigDecimal pRemise) {
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 4 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_CENT) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 4 doit être compris entre 0 et 100 %.");
    }
    pourcentageRemise4 = pRemise.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Pourcentage de remise 5.
   */
  public BigDecimal getPourcentageRemise5() {
    return pourcentageRemise5.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le pourcentage de remise 5.
   */
  public void setPourcentageRemise5(BigDecimal pRemise) {
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 5 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_CENT) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 5 doit être compris entre 0 et 100 %.");
    }
    pourcentageRemise5 = pRemise.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Pourcentage de remise 6.
   */
  public BigDecimal getPourcentageRemise6() {
    return pourcentageRemise6.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le pourcentage de remise 6.
   */
  public void setPourcentageRemise6(BigDecimal pRemise) {
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 6 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_CENT) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 6 doit être compris entre 0 et 100 %.");
    }
    pourcentageRemise6 = pRemise.setScale(NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Coefficient de remise (4 chiffres après la virgules).
   */
  public BigDecimal getCoefficientRemise() {
    return coefficientRemise;
  }
  
  /**
   * Modifier le coefficient de remise.
   * La valeur fournie est arrondie à 4 chiffres après la virgule.
   */
  public void setCoefficientRemise(BigDecimal pCoefficientRemise) {
    if (pCoefficientRemise == null) {
      throw new MessageErreurException("Le coefficient de remise est invalide.");
    }
    if (pCoefficientRemise.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le coefficient de remise doit être supérieur ou égal à 0.");
    }
    if (pCoefficientRemise.compareTo(BigDecimal.ONE) > 0) {
      throw new MessageErreurException("Le coefficient de remise doit être inférieur ou égal à 1.");
    }
    if (coefficientRemise.compareTo(BigDecimal.ZERO) == 0) {
      coefficientRemise = BigDecimal.ONE;
    }
    else {
      coefficientRemise = pCoefficientRemise.setScale(4, RoundingMode.HALF_UP);
    }
    calculer();
  }
  
  /**
   * Pourcentage de taxe ou de majoration (4 chiffres aprés la virgules).
   */
  public BigDecimal getPourcentageTaxeOuMajoration() {
    return pourcentageTaxeOuMajoration.setScale(4, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le pourcentage de taxe ou de majoration.
   * La valeur fournie est arrondie à 4 chiffres aprés la virgule.
   */
  public void setPourcentageTaxeOuMajoration(BigDecimal pPourcentageTaxeOuMajoration) {
    if (pPourcentageTaxeOuMajoration == null) {
      throw new MessageErreurException("Le pourcentage de taxe ou de majoration est invalide.");
    }
    if (pPourcentageTaxeOuMajoration.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le pourcentage de taxe ou de majoration doit être supérieur ou égal à 0.");
    }
    pourcentageTaxeOuMajoration = pPourcentageTaxeOuMajoration.setScale(4, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Montant de conditionnement (2 chiffres aprés la virgules).
   */
  public BigDecimal getMontantConditionnement() {
    return montantConditionnement.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le montant du conditionnement
   * La valeur fournie est arrondie à 2 chiffres aprés la virgule.
   */
  public void setMontantConditionnement(BigDecimal pMontantConditionnement) {
    if (pMontantConditionnement == null) {
      throw new MessageErreurException("Le montant du conditionnement est invalide.");
    }
    if (pMontantConditionnement.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le montant du conditionnement doit être supérieur ou égal à 0.");
    }
    montantConditionnement = pMontantConditionnement.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Prix d'achat net HT par unités d'achats (2 chiffres aprés la virgules).
   */
  public BigDecimal getPrixAchatNetHT() {
    return prixAchatNetHT;
  }
  
  /**
   * Indiquer le mode de saisie du port.
   * Deux mode spossibles :
   * - Soit à partir du montant au poids multiplié par le poids.
   * - Soit à partir du pourcentage de port appliqué au prix net HT
   */
  public EnumModeSaisiePort getModeSaisiePort() {
    return modeSaisiePort;
  }
  
  /**
   * Modifier le mode de saisie du port.
   */
  public void setModeSaisiePort(EnumModeSaisiePort pModeSaisiePort) {
    // Mettre le mode par défaut si rien n'est fourni
    if (pModeSaisiePort == null) {
      pModeSaisiePort = EnumModeSaisiePort.MONTANT_FOIS_POIDS;
    }
    
    // Mémoriser le mode
    modeSaisiePort = pModeSaisiePort;
  }
  
  /**
   * Montant au poids du port HT (2 chiffres aprés la virgules).
   * Ce montant est multiplié par le poids pour calculer le montant total HT du port.
   * MontantPortHT = MontantAuPoidsHT x PoidsPort
   */
  public BigDecimal getMontantAuPoidsPortHT() {
    return montantAuPoidsPortHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le montant au poids du port HT.
   * La valeur fournie est arrondie à 2 chiffres aprés la virgule.
   */
  public void setMontantAuPoidsPortHT(BigDecimal pMontantAuPoidsPortHT) {
    if (pMontantAuPoidsPortHT == null) {
      throw new MessageErreurException("Le montant au poids du port HT est invalide.");
    }
    if (pMontantAuPoidsPortHT.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le montant au poids du port HT doit être supérieur ou égal à 0.");
    }
    montantAuPoidsPortHT = pMontantAuPoidsPortHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    // C'est le montant du poids au port qui décide du mode de saisie du port
    if (montantAuPoidsPortHT.compareTo(BigDecimal.ZERO) > 0 || pourcentagePort.compareTo(BigDecimal.ZERO) == 0) {
      modeSaisiePort = EnumModeSaisiePort.MONTANT_FOIS_POIDS;
    }
    else {
      modeSaisiePort = EnumModeSaisiePort.POURCENTAGE_PRIX_NET;
    }
    
    calculer();
  }
  
  /**
   * Poids associé au port (4 chiffres aprés la virgules).
   * Cette valeur est multipliée par le montant au poids pour calculer le montant total HT du port.
   * MontantPortHT = MontantAuPoidsHT x PoidsPort
   */
  public BigDecimal getPoidsPort() {
    return poidsPort;
  }
  
  /**
   * Modifier le poids associé au port.
   * La valeur fournie est arrondie à 4 chiffres aprés la virgule.
   */
  public void setPoidsPort(BigDecimal pPoidsPort) {
    if (pPoidsPort == null) {
      throw new MessageErreurException("Le poids associé au port est invalide.");
    }
    if (pPoidsPort.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le poids associé au port doit être supérieur ou égal à 0.");
    }
    if (pPoidsPort.compareTo(BigDecimal.ZERO) == 0) {
      poidsPort = BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP);
    }
    else {
      poidsPort = pPoidsPort.setScale(4, RoundingMode.HALF_UP);
    }
    calculer();
  }
  
  /**
   * Pourcentage de port (2 chiffres aprés la virgules).
   * Ce pourcentage est appliqué au prix d'achat net HT pour calculer le montant total HT du port.
   * MontantTotalPortHT = PrixAchatNetHT * (1 + PourcentagePort / 100)
   */
  public BigDecimal getPourcentagePort() {
    return pourcentagePort.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier le pourcentage de port.
   * La valeur fournie est arrondie à 2 chiffres aprés la virgule.
   */
  public void setPourcentagePort(BigDecimal pPourcentagePort) {
    if (pPourcentagePort == null) {
      throw new MessageErreurException("Le pourventage du port est invalide.");
    }
    if (pPourcentagePort.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le pourventage du port doit être supérieur ou égal à 0.");
    }
    pourcentagePort = pPourcentagePort.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    // C'est le montant du poids au port qui décide du mode de saisie du port
    if (montantAuPoidsPortHT.compareTo(BigDecimal.ZERO) > 0 || pourcentagePort.compareTo(BigDecimal.ZERO) == 0) {
      modeSaisiePort = EnumModeSaisiePort.MONTANT_FOIS_POIDS;
    }
    else {
      modeSaisiePort = EnumModeSaisiePort.POURCENTAGE_PRIX_NET;
    }
    calculer();
  }
  
  /**
   * Montant total du port HT (2 chiffres après la virgules).
   * Ce montant est calculé de deux façon, soit à partir du pourcentage de port appliqué au prix net HT, soit à partir du montant au
   * poids multiplié par le poids.
   * MontantPortHT = MontantAuPoidsHT x PoidsPort
   * MontantPortHT = PrixAchatNetHT * (1 + PourcentagePort / 100)
   */
  public BigDecimal getMontantPortHT() {
    return montantPortHT;
  }
  
  /**
   * Prix de revient forunisseur HT en unités d'achats (2 chiffres après la virgules).
   */
  public BigDecimal getPrixRevientFournisseurHT() {
    return prixRevientFournisseurHT;
  }
  
  /**
   * Prix de revient forunisseur HT en unités de stocks (2 chiffres après la virgules).
   */
  public BigDecimal getPrixRevientFournisseurHTEnUS() {
    return prixRevientFournisseurHTEnUS;
  }
  
  /**
   * Frais d'exploitation (2 chiffres après la virgules).
   */
  public BigDecimal getFraisExploitation() {
    return fraisExploitation.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Modifier les frais d'exploitation.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setFraisExploitation(BigDecimal pFraisExploitation) {
    if (pFraisExploitation == null) {
      throw new MessageErreurException("Les frais d'exploitation sont invalides.");
    }
    if (pFraisExploitation.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Les frais d'exploitation doivent être supérieur ou égal à 0.");
    }
    fraisExploitation = pFraisExploitation.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    calculer();
  }
  
  /**
   * Coefficient d'approche fixe (4 chiffres après la virgules).
   */
  public BigDecimal getCoefficientApprocheFixe() {
    return coefficientApprocheFixe;
  }
  
  /**
   * Modifier le coefficient d'approche fixe.
   * La valeur fournie est arrondie à 4 chiffres après la virgule.
   */
  public void setCoefficientApprocheFixe(BigDecimal pCoefficientApprocheFixe) {
    if (pCoefficientApprocheFixe == null) {
      throw new MessageErreurException("Le coefficient d'approche fixe est invalide.");
    }
    if (pCoefficientApprocheFixe.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le coefficient d'approche fixe doit être supérieur ou égal à 0.");
    }
    if (coefficientApprocheFixe.compareTo(BigDecimal.ZERO) == 0) {
      coefficientApprocheFixe = BigDecimal.ONE;
    }
    else {
      coefficientApprocheFixe = pCoefficientApprocheFixe.setScale(4, RoundingMode.HALF_UP);
    }
    calculer();
  }
  
  /**
   * Prix de revient standard en unité d'achats (2 chiffres après la virgules).
   */
  public BigDecimal getPrixRevientStandardHT() {
    return prixRevientStandardHT;
  }
  
  /**
   * Prix de revient standard en unité de stocks (2 chiffres après la virgules).
   */
  public BigDecimal getPrixRevientStandardHTEnUS() {
    return prixRevientStandardHTEnUS;
  }
  
  /**
   * Prix de revient standard en unité de ventes (2 chiffres après la virgules).
   * Le prix de revient standard en unité de ventes (UA) est recalculé à partir du prix de revient standard en unités d'achats (UA).
   * La méthode nécessite qu'on lui fournisse le coefficient US/UV.
   * PrixUV = PrixUA * (UA/UCA) / (US/UCA) * US/UV
   */
  public BigDecimal getPrixRevientStandardHTEnUV(BigDecimal pCoefficientUSParUV) {
    BigDecimal prixRevientStandardHTEnUV = prixRevientStandardHT.multiply(getCoefficientUAparUCA())
        .divide(getCoefficientUSparUCA(), MathContext.DECIMAL32).multiply(pCoefficientUSParUV);
    return prixRevientStandardHTEnUV.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Indiquer si des valeurs ont été saisies dans la condition d'achats.
   */
  public boolean isEmpty() {
    return pourcentageRemise1.compareTo(BigDecimal.ZERO) == 0 && pourcentageRemise2.compareTo(BigDecimal.ZERO) == 0
        && pourcentageRemise3.compareTo(BigDecimal.ZERO) == 0 && pourcentageRemise4.compareTo(BigDecimal.ZERO) == 0
        && pourcentageRemise5.compareTo(BigDecimal.ZERO) == 0 && pourcentageRemise6.compareTo(BigDecimal.ZERO) == 0
        && coefficientRemise.compareTo(BigDecimal.ONE) == 0 && pourcentageTaxeOuMajoration.compareTo(BigDecimal.ZERO) == 0
        && montantConditionnement.compareTo(BigDecimal.ZERO) == 0 && montantAuPoidsPortHT.compareTo(BigDecimal.ZERO) == 0
        && poidsPort.compareTo(BigDecimal.ONE) == 0 && pourcentagePort.compareTo(BigDecimal.ZERO) == 0
        && fraisExploitation.compareTo(BigDecimal.ZERO) == 0 && coefficientApprocheFixe.compareTo(BigDecimal.ONE) == 0;
  }
}
