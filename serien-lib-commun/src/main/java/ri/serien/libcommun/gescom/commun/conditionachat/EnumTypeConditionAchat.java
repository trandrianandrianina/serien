/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.conditionachat;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de condition d'achat
 *
 */
public enum EnumTypeConditionAchat {
  NEGOCIATION_ACHAT_SUR_DOCUMENT_VENTE('V', "Négociation d'achat sur document de vente"),
  CONDITION_ACHAT('A', "Condition d'achat ou négociation sur document d'achat");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeConditionAchat(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeConditionAchat valueOfByCode(Character pCode) {
    for (EnumTypeConditionAchat value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de condition d'achat est invalide : " + pCode);
  }
}
