/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.groupeconditionvente;

import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Cette classe contient toutes les données décrivant un groupe de conditions de ventes.
 */
public class GroupeConditionVente extends AbstractClasseMetier<IdGroupeConditionVente> {
  // Variables
  private String libelle = "";
  private Date dateDebut = null;
  private Date dateFin = null;
  private EnumTypeGroupeConditionVente typeGroupeConditionVente = null;
  private EnumModeApplicationConditionVente modeApplication = null;
  
  // -- Méthodes publiques
  
  /**
   * Indique si le mode d'application du groupe est de type cumulatif c'est à dire que les conditions de ventes de ce groupe s'ajoutent
   * à la condition déjà appliquée.
   * 
   * @return
   */
  public boolean isCumulative() {
    if (modeApplication == null) {
      return false;
    }
    if (modeApplication == EnumModeApplicationConditionVente.AJOUT_CONDITION) {
      return true;
    }
    return false;
  }
  
  // -- Accesseurs
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public GroupeConditionVente(IdGroupeConditionVente pId) {
    super(pId);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Retourne le libelle
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Renseigner le libelle condition de vente
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  /**
   * Retourne la date de début de la période d'application.
   * 
   * @return
   */
  public Date getDateDebut() {
    return dateDebut;
  }
  
  /**
   * Initialise la date de début de la période d'application.
   * 
   * @param pDateDebut
   */
  public void setDateDebut(Date pDateDebut) {
    this.dateDebut = pDateDebut;
  }
  
  /**
   * Retourne la date de fin de la période d'application.
   */
  public Date getDateFin() {
    return dateFin;
  }
  
  /**
   * Initialise la date de fin de la période d'application.
   * 
   * @param pDateFin
   */
  public void setDateFin(Date pDateFin) {
    this.dateFin = pDateFin;
  }
  
  /**
   * Retourne le type des conditions appartenant à ce groupe.
   * 
   * @return
   */
  public EnumTypeGroupeConditionVente getTypeConditionVente() {
    return typeGroupeConditionVente;
  }
  
  /**
   * Initialise le type des conditions appartenant à ce groupe.
   * 
   * @param pTypeConditionVente
   */
  public void setTypeConditionVente(EnumTypeGroupeConditionVente pTypeConditionVente) {
    this.typeGroupeConditionVente = pTypeConditionVente;
  }
  
  /**
   * Retourne le mode d'application des conditions appartenant à ce groupe.
   * 
   * @return
   */
  public EnumModeApplicationConditionVente getModeApplication() {
    return modeApplication;
  }
  
  /**
   * Initialiser le mode d'application des conditions appartenant à ce groupe.
   * 
   * @param pModeApplication
   */
  public void setModeApplication(EnumModeApplicationConditionVente pModeApplication) {
    this.modeApplication = pModeApplication;
  }
  
}
