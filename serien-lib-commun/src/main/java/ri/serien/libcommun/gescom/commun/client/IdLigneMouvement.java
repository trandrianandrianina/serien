/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.util.Date;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un client.
 * 
 * L'identifiant est composé du code établissement, du numéro client et du suffixe client.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation. *
 */
public class IdLigneMouvement extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_ARTICLE = 20;
  public static final int LONGUEUR_MAGASIN = 3;
  
  public static final int INDICATIF_ETB_ART_MAG = 0;
  public static final int INDICATIF_ART_MAG = 1;
  
  // Variables
  private final IdArticle idArticle;
  private final IdMagasin idMagasin;
  private final Date dateMouvement;
  private final Integer numeroOrdre;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdLigneMouvement(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, IdArticle pIdArticle,
      IdMagasin pIdMagasin, Date pDateMouvement, Integer pNumeroOrdre) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    idArticle = pIdArticle;
    idMagasin = pIdMagasin;
    dateMouvement = pDateMouvement;
    numeroOrdre = pNumeroOrdre;
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   */
  private IdLigneMouvement(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    idArticle = null;
    idMagasin = null;
    dateMouvement = null;
    numeroOrdre = null;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdLigneMouvement getInstance(IdEtablissement pIdEtablissement, IdArticle pIdArticle, IdMagasin pIdMagasin,
      Date pDateMouvement, Integer pNumeroOrdre) {
    return new IdLigneMouvement(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pIdArticle, pIdMagasin, pDateMouvement, pNumeroOrdre);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdLigneMouvement getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdLigneMouvement(pIdEtablissement);
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdLigneMouvement controlerId(IdLigneMouvement pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du mouvement est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du mouvement n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + idArticle.hashCode();
    cle = 37 * cle + idMagasin.hashCode();
    cle = 37 * cle + dateMouvement.hashCode();
    cle = 37 * cle + numeroOrdre.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdLigneMouvement)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de ligne de mouvement.");
    }
    IdLigneMouvement id = (IdLigneMouvement) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && idArticle.equals(id.idArticle) && idMagasin.equals(id.idMagasin)
        && dateMouvement.equals(id.dateMouvement) && numeroOrdre.equals(id.numeroOrdre);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdLigneMouvement)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdLigneMouvement id = (IdLigneMouvement) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idArticle.compareTo(id.idArticle);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idMagasin.compareTo(id.idMagasin);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = dateMouvement.compareTo(id.dateMouvement);
    if (comparaison != 0) {
      return comparaison;
    }
    return numeroOrdre.compareTo(id.numeroOrdre);
  }
  
  @Override
  public String getTexte() {
    return getCodeEtablissement().trim() + SEPARATEUR_ID + idArticle.getCodeArticle() + SEPARATEUR_ID + idMagasin.getCode();
  }
  
  // -- Accesseurs
  
  /**
   * ID de l'article mouvementé
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * ID du magasin dans lequel a lieu le mouvement de stock
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Date du mouvement
   */
  public Date getDateMouvement() {
    return dateMouvement;
  }
  
  /**
   * Numéro d'ordre du mouvement
   */
  public int getNumeroOrdre() {
    return numeroOrdre;
  }
  
  /**
   * Indicatif du client en fonction de l'indicatif souhaité.
   */
  public String getIndicatif(int pTypeIndicatif) {
    switch (pTypeIndicatif) {
      case INDICATIF_ETB_ART_MAG:
        return String.format("%" + LONGUEUR_CODE_ETABLISSEMENT + "s%0" + LONGUEUR_ARTICLE + "d%0" + LONGUEUR_MAGASIN + "d",
            getCodeEtablissement(), idArticle.getCodeArticle(), idMagasin.getCode());
      case INDICATIF_ART_MAG:
        return String.format("%0" + LONGUEUR_ARTICLE + "d%0" + LONGUEUR_MAGASIN + "d", idArticle.getCodeArticle(), idMagasin.getCode());
    }
    return "";
  }
}
