/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.modeexpedition;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Mode d'expédition.
 * Le mode d'expédition n'est pas lié à un établissement. Il correspond à la personnalisation EX des ventes ou des
 * achats.
 */
public class ModeExpedition extends AbstractClasseMetier<IdModeExpedition> {
  // Variables
  private String libelle = null;
  private Boolean isEnlevement = null;
  
  /**
   * Constructeur.
   */
  public ModeExpedition(IdModeExpedition pIdModeExpedition) {
    super(pIdModeExpedition);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Libellé du mode d'expédition.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé du mode d'expédition.
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  /**
   * Indique si le mode d'expédition est un enlèvement.
   */
  public Boolean isEnlevement() {
    return isEnlevement;
  }
  
  /**
   * Modifier le mode d'expéiditon pour indiquer qu'il s'agit d'un enlèvement.
   */
  public void setIsEnlevement(Boolean pIsEnlevement) {
    isEnlevement = pIsEnlevement;
  }
  
  /**
   * Indique si le mode d'expédition est une livraison.
   */
  public Boolean isLivraison() {
    return !isEnlevement;
  }
  
  /**
   * Indique si le mode d'expédition n'est pas définit.
   */
  public Boolean isNonDefini() {
    return id.isNonDefini();
  }
  
}
