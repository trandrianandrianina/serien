/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.acheteur;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Acheteur.
 * L'utilisation de cette classe est à privilégiée dès qu'on manipule une liste d'achteurs. Elle contient toutes les méthodes oeuvrant
 * sur la liste tandis que la classe Acheteur contient les méthodes oeuvrant sur un seul acheteur.
 */
public class Acheteur extends AbstractClasseMetier<IdAcheteur> {
  // Constantes
  public static final int LONGUEUR_NOM = 30;
  public static final int LONGUEUR_TELEPHONE = 20;
  public static final int LONGUEUR_EMAIL = 40;
  
  // Variables
  private String nom = null;
  private String numeroTelephone = null;
  private String adresseEmail = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public Acheteur(IdAcheteur pIdAcheteur) {
    super(pIdAcheteur);
  }
  
  @Override
  public String getTexte() {
    return nom;
  }
  
  // -- Accesseurs
  
  /**
   * Nom de l'acheteur.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Renseigner le nom de l'acheteur.
   */
  public void setNom(String pNom) {
    this.nom = pNom;
  }
  
  public String getNumeroTelephone() {
    return numeroTelephone;
  }
  
  public void setNumeroTelephone(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }
  
  public String getAdresseEmail() {
    return adresseEmail;
  }
  
  public void setAdresseEmail(String adresseEmail) {
    this.adresseEmail = adresseEmail;
  }
}
