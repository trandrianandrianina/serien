/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe contient les informatiques basiques d'un fournisseur.
 * Les données sont l'identifiant, la raison sociale, le code postal, la ville ainsi que les deux clés de classement.
 */
public class FournisseurBase extends AbstractClasseMetier<IdFournisseur> {
  private String raisonSociale = null;
  private String codePostal = null;
  private String ville = null;
  private String cleClassementUn = null;
  private String cleClassementDeux = null;
  private List<String> raisonSocialeEclatee = null;
  private Adresse adresse = null;
  
  /**
   * Constructeur avec l'identifiant du fournisseur.
   */
  public FournisseurBase(IdFournisseur pIdFournisseur) {
    super(pIdFournisseur);
  }
  
  @Override
  public String getTexte() {
    return raisonSociale;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdFournisseur controlerId(IdFournisseur pIdFournisseur, boolean pVerifierExistance) {
    if (pIdFournisseur == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    return IdFournisseur.controlerId(pIdFournisseur, true);
  }
  
  @Override
  public boolean equals(Object object) {
    // Tester si l'objet est nous-même (optimisation)
    if (object == this) {
      return true;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(object instanceof FournisseurBase)) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    FournisseurBase fournisseur = (FournisseurBase) object;
    return Constantes.equals(id, fournisseur.id) && Constantes.equals(raisonSociale, fournisseur.raisonSociale)
        && Constantes.equals(codePostal, fournisseur.codePostal) && Constantes.equals(ville, fournisseur.ville);
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + id.hashCode();
    cle = 37 * cle + raisonSociale.hashCode();
    cle = 37 * cle + codePostal.hashCode();
    cle = 37 * cle + ville.hashCode();
    return cle;
  }
  
  /**
   * Définir le format d'affichage dans les listes déroulantes.
   * Format "Libellé (code)".
   */
  @Override
  public String toString() {
    return raisonSociale + " (" + id + ")";
  }
  
  /**
   * Indiquer si le fournisseur est déjà existant dans la base de données.
   */
  @Override
  public boolean isExistant() {
    return getId().isExistant();
  }
  
  /**
   * Identifiant du fournisseur.
   */
  @Override
  public IdFournisseur getId() {
    return id;
  }
  
  /**
   * Modifier l'identifiant du fournisseur.
   */
  @Override
  public void setId(IdFournisseur pIdFournisseur) {
    IdFournisseur.controlerId(pIdFournisseur, false);
    id = pIdFournisseur;
  }
  
  /**
   * Raison sociale du fournisseur.
   */
  public String getRaisonSociale() {
    return raisonSociale;
  }
  
  /**
   * Modifier la raison sociale du fournisseur.
   */
  public void setRaisonSociale(String pRaisonSociale) {
    raisonSociale = pRaisonSociale;
    raisonSocialeEclatee = null;
  }
  
  /**
   * Code postal.
   */
  public String getCodePostal() {
    return codePostal;
  }
  
  /**
   * Modifier le code postal.
   */
  public void setCodePostal(String pCodePostal) {
    codePostal = pCodePostal;
  }
  
  /**
   * Ville.
   */
  public String getVille() {
    return ville;
  }
  
  /**
   * Modifier la ville.
   */
  public void setVille(String pVille) {
    ville = pVille;
  }
  
  public String getCleClassementUn() {
    return cleClassementUn;
  }
  
  /**
   * Modifier la clé de classement un.
   */
  public void setCleClassementUn(String pCleClassementUn) {
    cleClassementUn = pCleClassementUn;
  }
  
  /**
   * Clé de classement un
   */
  public String getCleClassementDeux() {
    return cleClassementDeux;
  }
  
  /**
   * Modifier la clé de classement deux.
   */
  public void setCleClassementDeux(String pCleClassementDeux) {
    cleClassementDeux = pCleClassementDeux;
  }
  
  /**
   * Retourner les différents mots composants la raison sociale du fournisseur.
   */
  public List<String> getRaisonSocialeEclatee() {
    if (raisonSocialeEclatee != null) {
      return raisonSocialeEclatee;
    }
    
    // Sortir si la raison sociale est vide
    raisonSocialeEclatee = new ArrayList<String>();
    if (raisonSociale == null || raisonSociale.isEmpty()) {
      return raisonSocialeEclatee;
    }
    
    // Mettre en majuscules
    String texte = raisonSociale.toUpperCase();
    
    // Normaliser le texte pour enlever les accents
    texte = Normalizer.normalize(texte, Normalizer.Form.NFD);
    texte = texte.replaceAll("[^\\x00-\\x7F]", "");
    
    // Enlever les points
    texte = texte.replaceAll("\\.", "");
    
    // Remplacer les caractères spéciaux par des espaces
    texte = texte.replaceAll("[^A-Z0-9.,-/ ]", " ");
    
    // Ne conserver qu'un espace entre chaque mot
    texte = texte.trim().replaceAll(" +", " ");
    
    // Répartir les mots dans les trois listes
    String[] mots = texte.split(" ");
    for (String mot : mots) {
      if (mot.length() >= 2) {
        raisonSocialeEclatee.add(mot);
      }
    }
    
    return raisonSocialeEclatee;
  }
  
  /**
   * retourne l'adresse du fournisseur
   */
  public Adresse getAdresse() {
    return adresse;
  }
  
  /**
   * Adresse du fournisseur
   */
  public void setAdresse(Adresse adresse) {
    this.adresse = adresse;
  }
  
}
