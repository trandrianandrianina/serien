/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.reglement;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de valeur possible pour les types de règlements.
 */
public enum EnumTypeReglement {
  CHEQUE('C', "Chèque"),
  CARTE('K', "Carte bancaire"),
  ESPECES('E', "Espèces"),
  VIREMENT('V', "Virement"),
  TRAITE('T', "Traite"),
  BILLET_A_ORDRE('B', "Billet à ordre"),
  RELEVE_FACTURE('R', "Relevé de facture"),
  REGLEMENTS_DIVERS('D', "Règlements divers"),
  PRELEVEMENTS('P', "Prélèvements"),
  TIP('I', "Titre interbancaire de paiement"),
  FOND_CAISSE('F', "Fond de caisse"),
  AVOIR('A', "Avoir"),
  AUTRE(' ', "Autre"); // Provisoire
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeReglement(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return code.toString();
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  static public EnumTypeReglement valueOfByCode(Character pCode) {
    for (EnumTypeReglement value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de règlement est inconnu : " + pCode);
  }
  
}
