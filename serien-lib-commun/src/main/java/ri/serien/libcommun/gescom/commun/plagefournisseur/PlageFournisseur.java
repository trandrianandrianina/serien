/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.plagefournisseur;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.MessageErreurException;

public class PlageFournisseur implements Serializable {
  private IdFournisseur idFournisseurDebut = null;
  private IdFournisseur idFournisseurFin = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public PlageFournisseur() {
    super();
  }
  
  /**
   * Constructeur avec les identifiants des fournisseurs de début et de fin.
   */
  public PlageFournisseur(IdFournisseur pIdFournisseurDebut, IdFournisseur pIdFournisseurFin) {
    super();
    if (!isParametrePlageFournisseurValide(pIdFournisseurDebut, pIdFournisseurFin)) {
      throw new MessageErreurException("Les valeurs à définir dans la plage de fournisseurs ne sont pas valides.");
    }
    idFournisseurDebut = pIdFournisseurDebut;
    idFournisseurFin = pIdFournisseurFin;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Contrôler la plage fournisseur.
   * @param pIdFournisseurDebut Identifiant du fournisseur de début.
   * @param pIdFournisseurFin Identifiant du fournisseur de fin.
   * @return
   *         true si le numéro fournisseur du début est inférieur ou égal à celui de la fin.<br>
   *         false si le numéro fournisseur du début est supérieur à celui de la fin.
   */
  private boolean isParametrePlageFournisseurValide(IdFournisseur pIdFournisseurDebut, IdFournisseur pIdFournisseurFin) {
    if (pIdFournisseurDebut != null && pIdFournisseurFin != null) {
      // Une valeur négatif indique que le fournisseur du début est inférieur au fournisseur de fin
      return pIdFournisseurDebut.compareTo(pIdFournisseurFin) < 0;
    }
    
    return true;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner l'identifiant du fournisseur de début.
   * @return Identifiant du fournisseur de début.
   */
  public IdFournisseur getIdFournisseurDebut() {
    return idFournisseurDebut;
  }
  
  /**
   * Définir l'identifiant du fournisseur de début.
   * @param pIdFournisseurDebut Identifiant du fournisseur de début à définir.
   */
  public void setIdFournisseurDebut(IdFournisseur pIdFournisseurDebut) {
    idFournisseurDebut = pIdFournisseurDebut;
  }
  
  /**
   * Retourner l'identifiant du fournisseur de fin.
   * @return Identifiant du fournisseur de fin.
   */
  public IdFournisseur getIdFournisseurFin() {
    return idFournisseurFin;
  }
  
  /**
   * Définir l'identifiant du fournisseur de fin.
   * @param pIdFournisseurFin Identifiant du fournisseur de fin à définir.
   */
  public void setIdFournisseurFin(IdFournisseur pIdFournisseurFin) {
    idFournisseurFin = pIdFournisseurFin;
  }
}
