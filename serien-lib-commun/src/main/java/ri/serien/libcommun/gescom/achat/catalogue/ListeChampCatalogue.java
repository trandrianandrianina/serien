/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import java.util.ArrayList;
import java.util.ListIterator;

import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;

/**
 * Liste des différents champs d'un catalogue fournisseur au format CSV.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de configurations.
 * La méthode chargerTout permet d'obtenir la liste de toutes les configurations d'un établissement.
 */
public class ListeChampCatalogue extends ArrayList<ChampCatalogue> {
  /**
   * Constructeur.
   */
  public ListeChampCatalogue() {
  }
  
  /**
   * Charger tous les champs du catalogue transmis en paramètre
   */
  public static ListeChampCatalogue chargerTout(IdSession pIdSession, String pNomFichier) {
    return ManagerServiceFournisseur.chargerListeChampUnCatalogue(pIdSession, pNomFichier);
  }
  
  /**
   * Comparer si la liste de champs catalogue correspond à une autre liste de champs catalogue.
   */
  public static boolean equalsComplet(ListeChampCatalogue pListe1, ListeChampCatalogue pListe2) {
    if (pListe1 == pListe2) {
      return true;
    }
    if (pListe1 == null || pListe2 == null) {
      return false;
    }
    
    ListIterator<ChampCatalogue> itr1 = pListe1.listIterator();
    ListIterator<ChampCatalogue> itr2 = pListe2.listIterator();
    while (itr1.hasNext() && itr2.hasNext()) {
      if (!ChampCatalogue.equalsComplet(itr1.next(), itr2.next())) {
        return false;
      }
    }
    return !(itr1.hasNext() || itr2.hasNext());
  }
}
