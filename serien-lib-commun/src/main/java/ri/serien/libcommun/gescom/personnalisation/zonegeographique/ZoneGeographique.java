/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.zonegeographique;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.ListeCommuneZoneGeographique;

/**
 * Zone géographique.
 * 
 * Une zone géographique permet de définir des zones de livraison. Ces zones de livraison auront des conditions tarifaires différentes.
 * Une zone géographique est dans la personnalisation ZG. Elle est ensuite associée :
 * - Soit à des départements, configurable pour chaque établissement. Ceci est effectué dans la personnalisation ZG.
 * - Soit à des communes, configurable pour chaque magasin. Ceci est effectué via le point de menu :
 * [GVM6A2] Gestion des ventes -> Opérations et éditions diverses -> Communes -> Affectation zone géo/communes
 * Les communes associées à des zoens géographique par ce biais sont gérées par la classe CommuneZoneGeographique.
 */
public class ZoneGeographique extends AbstractClasseMetier<IdZoneGeographique> {
  // Variables
  private String nom = null;
  private ListeCommuneZoneGeographique listeCommuneZoneGeographique = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public ZoneGeographique(IdZoneGeographique pIdZoneGeographique) {
    super(pIdZoneGeographique);
  }
  
  @Override
  public String getTexte() {
    return nom;
  }
  
  // -- Accesseurs
  
  /**
   * Renvoyer le nom de la zone géographique.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Mettre à jour le nom de la zone géographique.
   */
  public void setNom(String pNom) {
    nom = pNom;
  }
  
  /**
   * Renvoyer la liste des communes composant une zone géographique.
   */
  public ListeCommuneZoneGeographique getListeCommuneZoneGeographique() {
    return listeCommuneZoneGeographique;
  }
  
  /**
   * Mettre à jour la liste des communes composant une zone géographique.
   */
  public void setListeCommuneZoneGeographique(ListeCommuneZoneGeographique pListeCommuneZoneGeographique) {
    listeCommuneZoneGeographique = pListeCommuneZoneGeographique;
  }
  
}
