/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de compte client.
 * 
 * Cela permet de savoir si le client est en compte (compte enregistré, client récurent) ou comptant (pas de compte enregistré, client de
 * passage).
 */
public enum EnumTypeCompteClient {
  EN_COMPTE("", "En compte"),
  COMPTANT("1", "Comptant"),
  PROSPECT("P", "Prospect");
  
  private final String code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeCompteClient(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(libelle);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeCompteClient valueOfByCode(String pCode) {
    for (EnumTypeCompteClient value : values()) {
      if (pCode.trim().equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code du type de compte  client est invalide : " + pCode);
  }
}
