/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

/**
 * Cette classe regroupe l'ensemble des formules de calcul concernant les prix :
 * - conversion HT vers TTC,
 * - conversion TTC vers HT,
 * - calcul d'un pourcentage,
 * - indice de marge,
 * - taux de marque,
 * ...
 */
public class OutilCalculPrix {
  // Constantes
  public static final int NOMBRE_DECIMALE_POURCENTAGE = 2;
  
  /**
   * Calcule le prix TTC à partir du prix HT avec arrondi.
   */
  public static BigDecimal calculerPrixTTC(BigDecimal pPrixHT, BigDecimal pTauxTva, int pNombreChiffres) {
    return pPrixHT.multiply(pTauxTva).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32).add(pPrixHT).setScale(pNombreChiffres,
        RoundingMode.HALF_UP);
  }
  
  /**
   * Calcule le prix HT à partir du prix TTC avec arrondi.
   */
  public static BigDecimal calculerPrixHTAvecPrixTTC(BigDecimal pPrixTTC, BigDecimal pTauxTva, int pNombreChiffres) {
    return ArrondiPrix.appliquer(
        pPrixTTC.multiply(Constantes.VALEUR_CENT).divide(pTauxTva.add(Constantes.VALEUR_CENT), MathContext.DECIMAL32), pNombreChiffres);
  }
  
  /**
   * Calcule le prix à partir du prix de revient et de l'indice de marge avec arrondi.
   * Attention le prix de revient doit être soit HT soit TTC.
   */
  public static BigDecimal calculerPrixAvecIndiceDeMarge(BigDecimal pPrixRevient, BigDecimal pIndiceDeMarge, int pNombreChiffres) {
    return pPrixRevient.multiply(pIndiceDeMarge).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32).setScale(pNombreChiffres,
        RoundingMode.HALF_UP);
  }
  
  /**
   * Calcule le prix à partir du prix de revient et de la marge avec arrondi.
   * Attention le prix de revient doit être soit HT soit TTC.
   */
  public static BigDecimal calculerPrixAvecMarge(BigDecimal pPrixRevient, BigDecimal pMarge, int pNombreChiffres) {
    return Constantes.VALEUR_CENT.multiply(pPrixRevient).divide(Constantes.VALEUR_CENT.subtract(pMarge), MathContext.DECIMAL32)
        .setScale(pNombreChiffres, RoundingMode.HALF_UP);
  }
  
  /**
   * Calcule le prix de revient avec arrondi.
   */
  public static BigDecimal calculerPrixRevient(BigDecimal pPrixHT, BigDecimal pIndiceDeMarge, int pNombreChiffres) {
    return pPrixHT.divide(pIndiceDeMarge.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32), MathContext.DECIMAL32)
        .setScale(pNombreChiffres, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer la marge à partir du prix HT et du prix de revient avec arrondi.
   * marge = ((pPrixHT - pPrixRevient) / pPrixHT) * 100
   * Retourne null si le calcul est impossible (préférable pour un composant bas niveau).
   */
  public static BigDecimal calculerMarge(BigDecimal pPrixVenteHT, BigDecimal pPrixRevientHT, int pNombreChiffres) {
    if (pPrixVenteHT == null || pPrixVenteHT.compareTo(BigDecimal.ZERO) == 0 || pPrixRevientHT == null) {
      return null;
    }
    
    BigDecimal marge = pPrixVenteHT.subtract(pPrixRevientHT);
    return marge.divide(pPrixVenteHT, MathContext.DECIMAL32).multiply(Constantes.VALEUR_CENT).setScale(pNombreChiffres,
        RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer l'indice de marge avec arrondi.
   * Retourne null si le calcul est impossible (préférable pour un composant bas niveau).
   */
  public static BigDecimal calculerIndiceDeMarge(BigDecimal pPrixHT, BigDecimal pPrixRevient, int pNombreChiffres) {
    if (pPrixHT == null || pPrixRevient == null || pPrixRevient.compareTo(BigDecimal.ZERO) == 0) {
      return null;
    }
    return pPrixHT.divide(pPrixRevient, MathContext.DECIMAL32).multiply(Constantes.VALEUR_CENT).setScale(pNombreChiffres,
        RoundingMode.HALF_UP);
  }
  
  /**
   * Calcule l'indice de marge avec le taux de marge avec arrondi.
   */
  public static BigDecimal calculerIndiceDeMargeAvecMarge(BigDecimal pMarge, int pNombreChiffres) {
    return Constantes.VALEUR_CENT
        .divide(BigDecimal.ONE.subtract((pMarge.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32)), MathContext.DECIMAL32))
        .setScale(pNombreChiffres, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer le taux de remise à partir du prix de base et du prix net.
   * @param pPrixBase Prix de base (ou public).
   * @param pPrixNet Prix net.
   * @return Taux de remise.
   */
  public static BigPercentage calculerTauxRemise(BigDecimal pPrixBase, BigDecimal pPrixNet) {
    if (pPrixNet == null || pPrixBase == null || pPrixBase.compareTo(BigDecimal.ZERO) == 0) {
      return null;
    }
    if (pPrixNet.compareTo(BigDecimal.ZERO) == 0) {
      return BigPercentage.CENT;
    }
    BigDecimal tauxRemise = BigDecimal.ONE.subtract(pPrixNet.divide(pPrixBase, MathContext.DECIMAL32));
    return new BigPercentage(tauxRemise, EnumFormatPourcentage.DECIMAL);
  }
  
  /**
   * Applique un taux de remise sur un prix.
   */
  public static BigDecimal appliquerTauxRemiseSurPrix(BigDecimal pPrix, BigDecimal pTaux, int pNombreDecimales) {
    return appliquerPourcentage(pPrix, pTaux, pNombreDecimales);
  }
  
  /**
   * Calcule le taux de TVA.
   */
  public static BigDecimal calculerTauxTVA(BigDecimal pPrixTTC, BigDecimal pPrixHT, int pNombreDecimales) {
    if (pPrixTTC.compareTo(BigDecimal.ZERO) == 0 || pPrixHT.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ZERO;
    }
    return pPrixTTC.divide(pPrixHT, MathContext.DECIMAL32).subtract(BigDecimal.ONE).multiply(Constantes.VALEUR_CENT)
        .setScale(pNombreDecimales, RoundingMode.HALF_UP);
  }
  
  /**
   * Calcule un pourcentage.
   */
  public static BigDecimal calculerPourcentage(BigDecimal pTotal, BigDecimal pPartie) {
    return pPartie.divide(pTotal, MathContext.DECIMAL32).multiply(Constantes.VALEUR_CENT);
  }
  
  /**
   * Applique un pourcentage.
   */
  public static BigDecimal appliquerPourcentage(BigDecimal pMontant, BigDecimal pPourcentage, int pNombreDecimales) {
    if (pPourcentage == null || pPourcentage.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("La valeur du pourcentage est invalide.");
    }
    if (pMontant == null) {
      throw new MessageErreurException("La valeur du montant est invalide.");
    }
    if (pMontant.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ZERO;
    }
    return pMontant.subtract((pMontant.multiply(pPourcentage)).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32))
        .setScale(pNombreDecimales, RoundingMode.HALF_UP);
  }
  
  /**
   * Comparer des prix HT ou TTC suivant le mode de calcul
   * 
   * @param modeTTC true=TTC, false=HT.
   * @param prixHT1 Prix HT numéro 1.
   * @param prixHT2 Prix HT numéro 2.
   * @param prixTTC1 Prix TTC numéro 1.
   * @param prixTTC2 Prix TTC numéro 2.
   * @return true=prix égaux, false=prix inégaux.
   */
  public static boolean equals(boolean modeTTC, BigDecimal prixHT1, BigDecimal prixHT2, BigDecimal prixTTC1, BigDecimal prixTTC2) {
    if (modeTTC) {
      return Constantes.equals(prixTTC1, prixTTC2);
    }
    else {
      return Constantes.equals(prixHT1, prixHT2);
    }
  }
}
