/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.parametresysteme;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.EnumTypeValeurPersonnalisation;

/**
 * Paramètre système.
 * 
 * Le comportement général du logiciel pour un établissement est régis par les paramètres systèmes. Les paramètres systèmes sont
 * identifiés par un nombre compris entre 1 et 999 inclus.
 */
public class ParametreSysteme extends AbstractClasseMetier<IdParametreSysteme> {
  // Variables
  private EnumParametreSysteme enumParametreSysteme = null;
  private Character valeur = null;
  
  /**
   * Constructeur à partir de l'identifiant.
   */
  public ParametreSysteme(IdParametreSysteme pIdParametreSysteme) {
    super(pIdParametreSysteme);
    
    // Initialiser l'énum à partir du numéro du paramètre système
    enumParametreSysteme = EnumParametreSysteme.valueOfByNumero(pIdParametreSysteme.getNumero());
  }
  
  @Override
  public String getTexte() {
    if (enumParametreSysteme == null) {
      return "";
    }
    return enumParametreSysteme.getLibelle();
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public ParametreSysteme clone() {
    ParametreSysteme o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (ParametreSysteme) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Méthodes privées
  
  /**
   * Description du paramètre système.
   */
  public EnumParametreSysteme getEnumParametreSysteme() {
    return enumParametreSysteme;
  }
  
  /**
   * Libellé du paramètre système.
   */
  public String getLibelle() {
    if (enumParametreSysteme == null) {
      return "";
    }
    return enumParametreSysteme.getLibelle();
  }
  
  /**
   * Valeur du paramètre système.
   */
  public Character getValeur() {
    return valeur;
  }
  
  /**
   * Modifier la valeur du paramètre système.
   */
  public void setValeur(Character pValeur) {
    valeur = pValeur;
  }
  
  /**
   * Type de personnalisation du paramètre système.
   */
  public EnumTypeValeurPersonnalisation getType() {
    if (enumParametreSysteme == null) {
      return null;
    }
    return enumParametreSysteme.getTypeValeur();
  }
  
  /**
   * Indiquer si le paramètre système est de type A.
   */
  public boolean isTypeA() {
    return getType() == EnumTypeValeurPersonnalisation.TYPE_A;
  }
  
  /**
   * Indiquer si le paramètre système est de type C.
   */
  public boolean isTypeC() {
    return getType() == EnumTypeValeurPersonnalisation.TYPE_C;
  }
  
  /**
   * Indiquer si le paramètre système est de type D.
   */
  public boolean isTypeD() {
    return getType() == EnumTypeValeurPersonnalisation.TYPE_D;
  }
}
