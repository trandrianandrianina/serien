/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stock;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceStock;

/**
 * Liste de stocks par articles.
 * 
 * L'utilisation de cette classe est à privilégier dès qu'on manipule une liste de stocks.
 */
public class ListeStock extends ListeClasseMetier<IdStock, Stock, ListeStock> {
  /**
   * Constructeurs.
   */
  public ListeStock() {
  }
  
  /**
   * Charger les stocks dont les identifiants sont fournis.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pListeId Liste des identifiants de stock qu'il faut charger.
   * @return Liste des stocks correspondants aux identifiants.
   */
  @Override
  public ListeStock charger(IdSession pIdSession, List<IdStock> pListeId) {
    throw new MessageErreurException("Méthode non implémentée");
  }
  
  /**
   * Charger la liste complète des stocks par articles pour l'établissement donné.
   * 
   * Cette méthode est présente car elel fait partie des méthodes standards des listes mais il est peu probable qu'elle ait une
   * utilisation pratique un jour.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @return Liste des stocks des articles de l'établissement.
   */
  @Override
  public ListeStock charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    // Définir les critères de recherche
    CritereStock critereStock = new CritereStock();
    critereStock.setIdEtablissement(pIdEtablissement);
    
    // Charger les stocks suivant les critères de recherche
    return ManagerServiceStock.chargerListeStock(pIdSession, critereStock);
  }
  
  /**
   * Charger les stocks suivant les critères de recherche.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCritereStock Critères de recherche des stocks.
   * @return Liste de stocks.
   */
  public static ListeStock charger(IdSession pIdSession, CritereStock pCritereStock) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    if (pCritereStock == null) {
      throw new MessageErreurException("Impossible de charger le stock car les critères sont invalides");
    }
    
    // Charger les stocks suivant les critères de recherche
    return ManagerServiceStock.chargerListeStock(pIdSession, pCritereStock);
  }
  
  /**
   * Charger le stock d'un article pour un magasin ou un établissement si le magasin n'est pas renseigné.
   * 
   * Si l'identifiant du magasin est null, le stock des magasins est cumulé pour fournir le stock sur l'ensemble de l'établissement.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pIdMagasin Identifiant du magasin (optionnel).
   * @param pIdArticle Identifiant de l'article dont on souhaite charger le stock.
   * @return Stock du magasin.
   */
  public static Stock charger(IdSession pIdSession, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, IdArticle pIdArticle) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    IdEtablissement.controlerId(pIdEtablissement, true);
    IdArticle.controlerId(pIdArticle, true);
    
    // Définir les critères de recherche
    CritereStock critereStock = new CritereStock();
    critereStock.setIdEtablissement(pIdEtablissement);
    critereStock.setIdMagasin(pIdMagasin);
    critereStock.setIdArticle(pIdArticle);
    
    // Charger les stocks suivant les critères de recherche
    // Normalement, la liste retournée contient un seul élément. On gère quand même le cas ou la liste retournée serait vide.
    ListeStock listeStock = ManagerServiceStock.chargerListeStock(pIdSession, critereStock);
    if (listeStock == null || listeStock.isEmpty()) {
      return null;
    }
    else if (listeStock.size() > 1) {
      throw new MessageErreurException("Le chargement du stock d'un article a retourné plus d'un stock.");
    }
    else {
      return listeStock.get(0);
    }
  }
  
  /**
   * Charger le stock d'un article pour un établissement.
   * 
   * Le stock des magasins est cumulé pour fournir le stock sur l'ensemble de l'établissement.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pIdArticle Identifiant de l'article dont on souhaite charger le stock.
   * @return Stock de l'établissement (tous les magasins de l'établissement cumulés).
   */
  public static Stock charger(IdSession pIdSession, IdEtablissement pIdEtablissement, IdArticle pIdArticle) {
    return charger(pIdSession, pIdEtablissement, null, pIdArticle);
  }
  
  /**
   * Charger le stock d'un article pour un magasin.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdMagasin Identifiant du magasin.
   * @param pIdArticle Identifiant de l'article dont on souhaite charger le stock.
   * @return Stock du magasin.
   */
  public static Stock charger(IdSession pIdSession, IdMagasin pIdMagasin, IdArticle pIdArticle) {
    IdMagasin.controlerId(pIdMagasin, true);
    return charger(pIdSession, pIdMagasin.getIdEtablissement(), pIdMagasin, pIdArticle);
  }
  
  /**
   * Retourner le stock du magasin dont l'identifiant est fourni en paramètre.
   * 
   * Cette méthode recherche dans les stocks présents dans la liste si l'un deux correspond au magasin et le retourne. La méthode
   * retourne null si le magasin n'a pas été trouvé.
   * 
   * @param pIdMagasin Identifiant du magasin dont on cherche le stock.
   * @return Stock du magasin (null s'il n'a pas été trouvé dans la liste).
   */
  public Stock get(IdMagasin pIdMagasin, IdArticle pIdArticle) {
    // Contrôler les paramètres
    IdMagasin.controlerId(pIdMagasin, true);
    IdArticle.controlerId(pIdArticle, true);
    
    // Constuire l'identifiant à rechercher
    IdStock idStock = IdStock.getInstancePourMagasin(pIdMagasin, pIdArticle);
    
    // Rechercher le magasin dans la liste des stocks
    Stock stockMagasin = get(idStock);
    return stockMagasin;
  }
  
  /**
   * Retourner le stock de l'établissement dont l'identifiant est fourni en paramètre.
   * 
   * Cette méthode recherche dans les stocks présents dans la liste si l'un deux correspond à l'établissement et le retourne.
   * Si un stock dédié à l'établissement n'a pas été trouvé, la méthode recherche les stocks des magasins de l'établissement afin
   * de les cumuler et en déduire le stock de l'établissement. La méthode retourne null si aucun stock magasin n'a été trouvé pour
   * l'établissement.
   * 
   * @param pIdEtablissement Identifiant de l'établissement dont on cherche le stock.
   * @return Stock de l'établissement (null s'il n'a pas été trouvé dans la liste).
   */
  public Stock get(IdEtablissement pIdEtablissement, IdArticle pIdArticle) {
    // Contrôler les paramètres
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    // Constuire l'identifiant à rechercher
    IdStock idStock = IdStock.getInstancePourEtablissement(pIdEtablissement, pIdArticle);
    
    // Parcourir la liste des stocks pour rechercher l'établissement
    Stock stockEtablissement = get(idStock);
    if (stockEtablissement != null) {
      return null;
    }
    
    // Générer le stock de l'établissement à partir du stock des magasins contenu dans la liste
    for (Stock stockMagasin : this) {
      // Tester si le stock concerne l'établissement et l'article dont on calcule le stock
      if (pIdEtablissement.equals(stockMagasin.getId().getIdEtablissement()) && pIdArticle.equals(stockMagasin.getId().getIdArticle())) {
        // Créer l'objet stock uniquement si on trouve au moins un stock magasin pour l'établissement
        if (stockEtablissement == null) {
          stockEtablissement = new Stock(idStock);
        }
        
        // Ajouter le stock du magasin au stock de l'établissement
        stockEtablissement.ajouter(stockMagasin);
      }
    }
    
    return stockEtablissement;
  }
  
}
