/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête d'un document d'achat.
 */
public enum EnumCodeEnteteDocumentAchat {
  COMMANDE_OU_RECEPTION('E', "Commande ou réception"),
  FACTURE('F', "Facture"),
  TRANSFERT('T', "Transfert"),
  COMMANDE_ORIGINE('e', "Commande d'origine");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumCodeEnteteDocumentAchat(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  public static EnumCodeEnteteDocumentAchat valueOfByCode(Character pCode) {
    for (EnumCodeEnteteDocumentAchat value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code entête du document d'achat est invalide : " + pCode);
  }
  
}
