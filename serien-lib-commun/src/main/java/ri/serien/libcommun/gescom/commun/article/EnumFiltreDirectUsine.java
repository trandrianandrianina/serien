/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Filtre direct usine sur les recherches articles
 * 
 */
public enum EnumFiltreDirectUsine {
  OPTION_TOUS_LES_ARTICLES_SAUF_DIRECT_USINE(0, "Tous les articles sauf les directs usine"),
  OPTION_LES_ARTICLES_DIRECT_USINE_UNIQUEMENT(1, "Directs usine seulement"),
  OPTION_TOUS_LES_ARTICLES(2, "Tous les articles");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumFiltreDirectUsine(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle + " (" + code + ")";
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumFiltreDirectUsine valueOfByCode(Integer pCode) {
    for (EnumFiltreDirectUsine value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("La valeur du filtre direct usine est invalide : " + pCode);
  }
}
