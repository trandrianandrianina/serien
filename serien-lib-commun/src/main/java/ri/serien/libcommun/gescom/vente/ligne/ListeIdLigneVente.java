/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import java.util.ArrayList;

import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste d'IdLigneVente.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste d'identifiants de lignes de vente.
 */
public class ListeIdLigneVente extends ArrayList<IdLigneVente> {
  /**
   * Constructeur.
   */
  public ListeIdLigneVente() {
  }
  
  public ListeIdLigneVente chargerTout(IdSession pIdSession, CritereLigneVente pCriteres) {
    
    return ManagerServiceDocumentVente.chargerListeIdLigneVente(pIdSession, pCriteres);
  }
  
}
