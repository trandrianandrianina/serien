/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.lienligne;

import java.util.ArrayList;

import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste de Liens.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de liens.
 * Un lien est une ligne à l'origine ou au départ d'une autre ligne (d'achat ou de vente) observée.
 */
public class ListeLienLigne extends ArrayList<LienLigne> {
  
  /**
   * Charge l'ensemble des liens pour une ligne vente et/ou achat.
   */
  public static ListeLienLigne chargerTout(IdSession pIdSession, IdLienLigne pIdLigneOrigine, IdMagasin pIdMagasin) {
    // Chargement des identifiants des lignes liées
    ListeLienLigne listeLienChargee = ManagerServiceDocumentVente.chargerListeLienLigne(pIdSession, pIdLigneOrigine);
    
    // Si le service ne renvoie rien c'est que le document d'origine n'a pas de document lié. Donc la liste des liens ne contient que le
    // document d'origine.
    if (listeLienChargee == null || listeLienChargee.isEmpty()) {
      return construireListeLienAvecDocumentOrigine(pIdSession, pIdLigneOrigine, pIdMagasin);
    }
    
    // Chargement de la liste des unités
    ListeUnite listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(pIdSession);
    
    // Dans le cas contraire, le service renvoie les identifiants des lignes
    ListeLienLigne listeLien = new ListeLienLigne();
    
    // Chargement du lien origine
    listeLienChargee.get(0).setListeUnite(listeUnite);
    switch (listeLienChargee.get(0).getId().getTypeOrigine()) {
      case LIEN_LIGNE_VENTE:
        // Construction du lien de la ligne en cours
        LienLigne lienVente = construireLienLigneVenteOrigine(pIdSession, listeLienChargee.get(0).getId(), pIdMagasin);
        // Ajout du lien de la ligne d'origine à la liste
        listeLien.add(lienVente);
        break;
      
      case LIEN_LIGNE_ACHAT:
        // Construction du lien de la ligne en cours
        LienLigne lienAchat = construireLienLigneAchatOrigine(pIdSession, listeLienChargee.get(0).getId());
        // Ajout du lien de la ligne d'origine à la liste
        listeLien.add(lienAchat);
        break;
      
      default:
        break;
    }
    
    // Parcourt de la liste des lien afin de charger le document et la ligne associés
    for (LienLigne lienLigne : listeLienChargee) {
      if (lienLigne.isDocumentOrigine() || (lienLigne.getId().getNumeroDocument() == 0 && lienLigne.getId().getNumeroFacture() == 0)) {
        continue;
      }
      
      lienLigne.setListeUnite(listeUnite);
      
      switch (lienLigne.getId().getType()) {
        case LIEN_LIGNE_VENTE:
          // Construction du lien de la ligne en cours
          LienLigne lienVente = construireLienLigneVente(pIdSession, lienLigne);
          // Ajout du lien de la ligne à la liste
          listeLien.add(lienVente);
          break;
        
        case LIEN_LIGNE_ACHAT:
          // Construction du lien de la ligne en cours
          LienLigne lienAchat = construireLienLigneAchat(pIdSession, lienLigne.getId());
          // Ajout du lien de la ligne à la liste
          listeLien.add(lienAchat);
          break;
        
        default:
          break;
      }
    }
    
    return listeLien;
  }
  
  /**
   * Retourner un lien de la liste à partir de son identifiant.
   */
  public LienLigne retournerLienParId(IdLienLigne pIdLien) {
    if (pIdLien == null) {
      return null;
    }
    for (LienLigne lien : this) {
      if (lien != null && Constantes.equals(lien.getId(), pIdLien)) {
        return lien;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner un lien de la liste à partir de l'ID de l'article qu'il contient.
   */
  public LienLigne retournerLienParArticle(IdArticle pIdArticle) {
    if (pIdArticle == null) {
      return null;
    }
    for (LienLigne lien : this) {
      if (lien != null && Constantes.equals(lien.getIdArticle(), pIdArticle)) {
        return lien;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un Lien est présent dans la liste.
   */
  public boolean isPresent(IdLienLigne pIdLien) {
    return retournerLienParId(pIdLien) != null;
  }
  
  // -- Méthodes privées
  
  /**
   * Construit les liens avec uniquement le document d'origine.
   * Si le service ne renvoie rien c'est que le document d'origine n'a pas de document lié. La liste des liens ne contient que le document
   * d'origine.
   */
  private static ListeLienLigne construireListeLienAvecDocumentOrigine(IdSession pIdSession, IdLienLigne pIdLigneOrigine,
      IdMagasin pIdMagasin) {
    ListeLienLigne listeLienChargee = new ListeLienLigne();
    
    switch (pIdLigneOrigine.getTypeOrigine()) {
      case LIEN_LIGNE_VENTE:
        // Construction du lien de la ligne à partir de son identifiant
        LienLigne lienVente = construireLienLigneVenteOrigine(pIdSession, pIdLigneOrigine, pIdMagasin);
        // Ajout du lien de la ligne d'origine à la liste
        listeLienChargee.add(lienVente);
        break;
      
      case LIEN_LIGNE_ACHAT:
        // Construction du lien de la ligne à partir de son identifiant
        LienLigne lienAchat = construireLienLigneAchatOrigine(pIdSession, pIdLigneOrigine);
        // Ajout du lien de la ligne d'origine à la liste
        listeLienChargee.add(lienAchat);
        break;
      
      default:
        break;
    }
    
    return listeLienChargee;
  }
  
  /**
   * Construit et retoune le lien pour une ligne d'un document de vente d'origine.
   */
  private static LienLigne construireLienLigneVenteOrigine(IdSession pIdSession, IdLienLigne pIdLigneOrigineVente, IdMagasin pIdMagasin) {
    // Création de l'identifiant du document d'origine
    IdDocumentVente idDocumentOrigine = null;
    EnumCodeEnteteDocumentVente entete = EnumCodeEnteteDocumentVente.valueOfByCode(pIdLigneOrigineVente.getCodeEnteteOrigine());
    if (entete.equals(EnumCodeEnteteDocumentVente.FACTURE_REGROUPEE)) {
      entete = EnumCodeEnteteDocumentVente.COMMANDE_OU_BON;
      idDocumentOrigine = IdDocumentVente.getInstanceGenerique(pIdLigneOrigineVente.getIdEtablissement(), entete,
          pIdLigneOrigineVente.getNumeroOrigine(), pIdLigneOrigineVente.getSuffixeOrigine(), 0);
    }
    else {
      idDocumentOrigine = IdDocumentVente.getInstanceGenerique(pIdLigneOrigineVente.getIdEtablissement(),
          EnumCodeEnteteDocumentVente.valueOfByCode(pIdLigneOrigineVente.getCodeEnteteOrigine()), pIdLigneOrigineVente.getNumeroOrigine(),
          pIdLigneOrigineVente.getSuffixeOrigine(), pIdLigneOrigineVente.getNumeroFactureOrigine());
    }
    
    // Chargement du document d'origine
    DocumentVente documentVente = ManagerServiceDocumentVente.chargerEnteteDocumentVente(pIdSession, idDocumentOrigine);
    if (documentVente.getIdClientFacture() != null && documentVente.getAdresseFacturation().getNom().trim().isEmpty()) {
      documentVente.getAdresseFacturation().setNom(documentVente.chargerClientFacture(pIdSession).getAdresse().getNom());
    }
    
    // Initialisation des critères de recherche pour le chargement de la ligne d'origine du document d'origine
    CritereLigneVente criteres = new CritereLigneVente();
    criteres.setNumeroLigne(pIdLigneOrigineVente.getNumeroLigneOrigine());
    ListeLigneVente listeLigneVente =
        ManagerServiceDocumentVente.chargerListeLigneVente(pIdSession, documentVente.getId(), criteres, false);
    LigneVente ligneVenteOrigine = null;
    ListeStockComptoir stocks = null;
    if (listeLigneVente != null) {
      ligneVenteOrigine = listeLigneVente.get(0);
      // Chargement d'une ligne d'achat liée à la ligne de vente si elle existe
      ligneVenteOrigine.setIdLigneAchatLiee(ManagerServiceClient.chargerLienLigneVenteLigneAchat(pIdSession, ligneVenteOrigine.getId()));
      // Chargement des stocks
      if (ligneVenteOrigine.getIdArticle() != null) {
        stocks = ListeStockComptoir.chargerPourArticle(pIdSession, pIdLigneOrigineVente.getIdEtablissement(),
            ligneVenteOrigine.getIdArticle().getCodeArticle());
      }
    }
    
    // Création de l'objet lien ligne de vente d'origine
    LienLigne lienVente = new LienLigne(IdLienLigne.getInstanceVente(pIdLigneOrigineVente.getIdEtablissement(),
        pIdLigneOrigineVente.getTypeOrigine(), pIdLigneOrigineVente.getCodeEnteteOrigine(), pIdLigneOrigineVente.getNumeroOrigine(),
        pIdLigneOrigineVente.getSuffixeOrigine(), pIdLigneOrigineVente.getNumeroFactureOrigine(),
        pIdLigneOrigineVente.getNumeroLigneOrigine(), idDocumentOrigine, pIdLigneOrigineVente.getNumeroLigneOrigine(),
        pIdLigneOrigineVente.isLigneSoldee()));
    lienVente.setDocumentVente(documentVente);
    lienVente.setLigne(ligneVenteOrigine);
    if (stocks != null) {
      lienVente.setQuantiteDisponibleVente(stocks.getQuantiteDisponibleVente(pIdLigneOrigineVente.getIdEtablissement(), pIdMagasin));
    }
    
    return lienVente;
  }
  
  /**
   * Construit et retoune le lien pour une ligne d'un document de vente.
   */
  private static LienLigne construireLienLigneVente(IdSession pIdSession, LienLigne lienLigne) {
    // Chargement de l'entête du document
    DocumentVente documentVente =
        ManagerServiceDocumentVente.chargerEnteteDocumentVente(pIdSession, lienLigne.getDocumentVente().getId());
    if (documentVente.getIdClientFacture() != null && documentVente.getAdresseFacturation().getNom().trim().isEmpty()) {
      documentVente.getAdresseFacturation().setNom(documentVente.chargerClientFacture(pIdSession).getAdresse().getNom());
    }
    
    // Initialisation des critères de recherche pour le chargement de la ligne du document
    CritereLigneVente criteres = new CritereLigneVente();
    criteres.setNumeroLigne(lienLigne.getId().getNumeroLigne());
    ListeLigneVente listeLigneVente =
        ManagerServiceDocumentVente.chargerListeLigneVente(pIdSession, documentVente.getId(), criteres, false);
    LigneVente ligneVente = null;
    ListeStockComptoir stocks = null;
    if (listeLigneVente != null) {
      ligneVente = listeLigneVente.get(0);
      if (ligneVente.getIdArticle() != null) {
        stocks = ListeStockComptoir.chargerPourArticle(pIdSession, ligneVente.getId().getIdEtablissement(),
            listeLigneVente.get(0).getIdArticle().getCodeArticle());
      }
    }
    
    // Création de l'objet lien ligne de vente
    LienLigne lienVente = new LienLigne(lienLigne.getId());
    lienVente.setDocumentVente(documentVente);
    lienVente.setLigne(ligneVente);
    if (stocks != null) {
      lienVente.setQuantiteDisponibleVente(stocks.getQuantiteDisponibleVenteTotale());
    }
    
    return lienVente;
  }
  
  /**
   * Construit et retoune le lien pour une ligne d'un document d'achat d'origine.
   */
  private static LienLigne construireLienLigneAchatOrigine(IdSession pIdSession, IdLienLigne pIdLigneOrigineAchat) {
    // Création de l'identifiant du document d'origine
    EnumCodeEnteteDocumentAchat enumCodeEnteteDocumentAchat =
        EnumCodeEnteteDocumentAchat.valueOfByCode(pIdLigneOrigineAchat.getCodeEnteteOrigine());
    IdDocumentAchat idDocumentAchatOrigine = IdDocumentAchat.getInstance(pIdLigneOrigineAchat.getIdEtablissement(),
        enumCodeEnteteDocumentAchat, pIdLigneOrigineAchat.getNumeroOrigine(), pIdLigneOrigineAchat.getSuffixeOrigine());
    // Chargement du document d'origine
    DocumentAchat documentAchatOrigine = ManagerServiceDocumentAchat.chargerDocumentAchat(pIdSession, idDocumentAchatOrigine, null, true);
    
    // Initialisation des critères de recherche pour le chargement de la ligne d'origine du document d'origine
    CritereLigneAchat criteresAchat = new CritereLigneAchat();
    criteresAchat.setIdDocumentAchat(idDocumentAchatOrigine);
    criteresAchat.setIdLigneAchat(IdLigneAchat.getInstance(idDocumentAchatOrigine, pIdLigneOrigineAchat.getNumeroLigneOrigine()));
    ListeLigneAchat listeLigneAchat = ManagerServiceDocumentAchat.chargerListeLigneAchat(pIdSession, criteresAchat);
    LigneAchat ligneAchatOrigine = null;
    if (listeLigneAchat != null) {
      ligneAchatOrigine = listeLigneAchat.get(0);
    }
    
    // Création de l'objet lien ligne achat d'origine
    LienLigne lienAchat = new LienLigne(IdLienLigne.getInstanceAchat(pIdLigneOrigineAchat.getIdEtablissement(),
        pIdLigneOrigineAchat.getTypeOrigine(), pIdLigneOrigineAchat.getCodeEnteteOrigine(), pIdLigneOrigineAchat.getNumeroOrigine(),
        pIdLigneOrigineAchat.getSuffixeOrigine(), pIdLigneOrigineAchat.getNumeroFactureOrigine(),
        pIdLigneOrigineAchat.getNumeroLigneOrigine(), idDocumentAchatOrigine, pIdLigneOrigineAchat.getNumeroLigneOrigine()));
    lienAchat.setDocumentAchat(documentAchatOrigine);
    lienAchat.setLigne(ligneAchatOrigine);
    
    return lienAchat;
  }
  
  /**
   * Construit et retoune le lien pour une ligne d'un document d'achat.
   */
  private static LienLigne construireLienLigneAchat(IdSession pIdSession, IdLienLigne pIdLienLigneAchat) {
    // Création de l'identifiant du document pour le lien en cours
    IdDocumentAchat idDocumentAchat = IdDocumentAchat.getInstance(pIdLienLigneAchat.getIdEtablissement(),
        EnumCodeEnteteDocumentAchat.valueOfByCode(pIdLienLigneAchat.getCodeEnteteDocument()), pIdLienLigneAchat.getNumeroDocument(),
        pIdLienLigneAchat.getSuffixeDocument());
    
    // Chargement du document contenant la ligne en cours
    DocumentAchat documentAchat = ManagerServiceDocumentAchat.chargerDocumentAchat(pIdSession, idDocumentAchat, null, true);
    
    // Initialisation des critères de recherche pour le chargement de la ligne du document
    CritereLigneAchat criteresAchat = new CritereLigneAchat();
    criteresAchat.setIdDocumentAchat(idDocumentAchat);
    criteresAchat.setIdLigneAchat(IdLigneAchat.getInstance(idDocumentAchat, pIdLienLigneAchat.getNumeroLigne()));
    ListeLigneAchat listeLigneAchat = ManagerServiceDocumentAchat.chargerListeLigneAchat(pIdSession, criteresAchat);
    LigneAchat ligneAchat = null;
    if (listeLigneAchat != null) {
      ligneAchat = listeLigneAchat.get(0);
    }
    
    // Création de l'objet lien ligne achat
    LienLigne lienAchat = new LienLigne(pIdLienLigneAchat);
    lienAchat.setDocumentAchat(documentAchat);
    lienAchat.setLigne(ligneAchat);
    
    return lienAchat;
  }
}
