/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.groupeconditionvente;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les types de groupe des conditions de vente.
 */
public enum EnumTypeGroupeConditionVente {
  NON_RENSEIGNE(' ', "Non renseigné"),
  CONDITION_STANDARD('1', "Condition de vente standard"),
  CONDITION_PROMOTIONNELLE('2', "Condition de vente promotionnelle"),
  RISTOURNE_FIN_PERIODE('3', "Ristourne de fin de période (RFA)"),
  RISTOURNE_FIN_FACTURE('4', "Ristourne de fin de facture"),
  BONIFICATION_FIN_PERIODE('5', "Bonification fin de période");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeGroupeConditionVente(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet Enum par son numéro.
   * 
   * @param pCode
   * @return
   */
  static public EnumTypeGroupeConditionVente valueOfByCode(Character pCode) {
    for (EnumTypeGroupeConditionVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type du groupe des conditions de vente est invalide : " + pCode);
  }
  
}
