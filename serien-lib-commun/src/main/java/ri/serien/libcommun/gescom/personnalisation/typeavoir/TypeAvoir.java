/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typeavoir;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

/**
 * Type d'avoir
 * 
 * Un type d'avoir correspond à une personnalisation de type AV dans Série N. Il permet de typer les avoirs créés à des fins statistiques
 * mais aussi de modifier le comportement de ces documents.
 * Par exemple le magasin de transfert déterminera où la marchandise retournée sera stockée.
 */
public class TypeAvoir extends AbstractClasseMetier<IdTypeAvoir> {
  // Variables
  private String libelle = null;
  private String titreColonne = null;
  private Integer incrementPostesComptabilisation = null;
  private IdMagasin idMagasinTransfert = null;
  private Boolean isTypeRetour = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public TypeAvoir(IdTypeAvoir pTypeFacturation) {
    super(pTypeFacturation);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  // -- Accesseurs
  
  /**
   * Retourner libellé du type de facturation
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier libellé du type de facturation
   */
  public void setLibelle(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Retourner titre colonne sur type de lettre
   */
  public String getTitreColonne() {
    return titreColonne;
  }
  
  /**
   * Modifier titre colonne sur type de lettre
   */
  public void setTitreColonne(String pTitreColonne) {
    titreColonne = pTitreColonne;
  }
  
  /**
   * Retourner incrément des postes de comptabilisation de ventes
   */
  public Integer getIncrementPostesComptabilisation() {
    return incrementPostesComptabilisation;
  }
  
  /**
   * Modifier incrément des postes de comptabilisation de ventes
   */
  public void setIncrementPostesComptabilisation(Integer pIncrementPostesComptabilisation) {
    incrementPostesComptabilisation = pIncrementPostesComptabilisation;
  }
  
  /**
   * Retourner identifiant du magasin de transfert
   */
  public IdMagasin getIdMagasinTransfert() {
    return idMagasinTransfert;
  }
  
  /**
   * Modifier identifiant du magasin de transfert
   */
  public void setIdMagasinTransfert(IdMagasin pIdMagasinTransfert) {
    idMagasinTransfert = pIdMagasinTransfert;
  }
  
  /**
   * Retourner si c'est un avoir de type retour
   */
  public Boolean isTypeRetour() {
    if (isTypeRetour == null) {
      return false;
    }
    return isTypeRetour;
  }
  
  /**
   * Modifier si c'est un avoir de type retour
   */
  public void setIsTypeRetour(Boolean pIsTypeRetour) {
    isTypeRetour = pIsTypeRetour;
  }
}
