/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.magasin;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de magasins.
 */
public class ListeMagasin extends ListeClasseMetier<IdMagasin, Magasin, ListeMagasin> {
  /**
   * Constructeur.
   */
  public ListeMagasin() {
  }
  
  @Override
  public ListeMagasin charger(IdSession pIdSession, List<IdMagasin> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charger tous les magasins actifs de l'établissement donné.
   */
  @Override
  public ListeMagasin charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas défini.");
    }
    
    CritereMagasin critereMagasin = new CritereMagasin();
    critereMagasin.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeMagasin(pIdSession, critereMagasin);
  }
  
  /**
   * Charger les magasins actifs correspondants aux critères de recherche.
   */
  public static ListeMagasin charger(IdSession pIdSession, CritereMagasin pCritereMagasin) {
    return ManagerServiceParametre.chargerListeMagasin(pIdSession, pCritereMagasin);
  }
  
  /**
   * Retourner un magasin de la liste à partir de son identifiant.
   */
  public Magasin getMagasinParId(IdMagasin pIdMagasin) {
    if (pIdMagasin == null) {
      return null;
    }
    for (Magasin magasin : this) {
      if (magasin != null && Constantes.equals(magasin.getId(), pIdMagasin)) {
        return magasin;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner un magasin de la liste à partir de son nom.
   */
  public Magasin getMagasinParNom(String pNom) {
    if (pNom == null) {
      return null;
    }
    for (Magasin magasin : this) {
      if (magasin != null && magasin.getNom().equals(pNom)) {
        return magasin;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner le nom d'un magasin de la liste à partir de son identifiant.
   */
  public String getNomMagasin(IdMagasin pIdMagasin) {
    Magasin magasin = getMagasinParId(pIdMagasin);
    if (magasin == null || magasin.getNom() == null) {
      return "";
    }
    return magasin.getNom();
  }
  
  /**
   * Tester si un Magasin est présent dans la liste.
   */
  public boolean isPresent(IdMagasin pIdMagasin) {
    return getMagasinParId(pIdMagasin) != null;
  }
  
}
