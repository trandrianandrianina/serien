/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.unite;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste d'unités.
 */
public class ListeUnite extends ListeClasseMetier<IdUnite, Unite, ListeUnite> {
  
  @Override
  public ListeUnite charger(IdSession pIdSession, List<IdUnite> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  @Override
  public ListeUnite charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return charger(pIdSession);
  }
  
  /**
   * Charge toutes les unités de l'établissement donnée.
   */
  public ListeUnite charger(IdSession pIdSession) {
    CriteresRechercheUnites criteres = new CriteresRechercheUnites();
    criteres.setTypeRecherche(CriteresRechercheUnites.RECHERCHE_COMPTOIR);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    ListeUnite listeUnite = ManagerServiceParametre.chargerListeUnite(pIdSession, criteres);
    return listeUnite;
  }
  
  /**
   * Retourner une unité présente dans la liste sur la base de son code.
   */
  public Unite retournerUniteParCode(String pCodeUnite) {
    if (pCodeUnite == null || pCodeUnite.trim().isEmpty()) {
      return null;
    }
    for (Unite unite : this) {
      if (unite != null && Constantes.equals(unite.getId().getCode(), pCodeUnite)) {
        return unite;
      }
    }
    
    return null;
  }
  
  /**
   * Retourne le libellé de l'unité à partir de son identifiant.
   */
  public String getLibelleUnite(IdUnite pIdUnite) {
    if (pIdUnite == null) {
      return "";
    }
    return get(pIdUnite).getLibelle();
  }
  
  /**
   * Retourne la précision d'une unité.
   */
  public int getPrecisionUnite(IdUnite pIdUnite) {
    if (pIdUnite == null || get(pIdUnite) == null) {
      return 0;
      // throw new MessageErreurException("Impossible de retourner la précision de l'unité car les données sont insuffisantes.");
    }
    return get(pIdUnite).getNombreDecimale();
  }
  
  /**
   * Retourne l'idenitifiant de l'unité par défaut.
   */
  public IdUnite retournerIdUniteParDefaut() {
    for (Unite unite : this) {
      if (unite != null && Constantes.equals(unite.getId().getCode(), IdUnite.CODE_UNITE_PAR_DEFAUT)) {
        return unite.getId();
      }
    }
    return null;
  }
  
}
