/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.lieutransport;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de lieu de transport.
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de LieuTransport. Elle contient toutes les méthodes
 * oeuvrant sur la liste tandis que la classe LieuTransport contient les méthodes oeuvrant sur un seul LieuTransport.
 */
public class ListeLieuTransport extends ListeClasseMetier<IdLieuTransport, LieuTransport, ListeLieuTransport> {
  
  /**
   * Charger touts les transporteurs suivant une liste d'IdLieuTransport.
   */
  @Override
  public ListeLieuTransport charger(IdSession pIdSession, List<IdLieuTransport> pListeId) {
    return null;
  }
  
  /**
   * Charger les LieuTransport suivant l'établissement.
   */
  @Override
  public ListeLieuTransport charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas défini.");
    }
    CritereLieuTransport criteres = new CritereLieuTransport();
    criteres.setIdEtablissement(pIdEtablissement);
    ListeLieuTransport listeLieuTransport = ManagerServiceParametre.chargerListeLieuTransport(pIdSession, criteres);
    return listeLieuTransport;
  }
  
}
