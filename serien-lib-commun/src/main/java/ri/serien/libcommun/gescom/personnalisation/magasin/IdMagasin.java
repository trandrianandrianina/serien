/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.magasin;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un magasin.
 *
 * L'identifiant est composé du code établissement et du code magasin.
 * Le code magasin correspond au paramètre MA du menu des ventes ou des achats.
 * Il est constitué de 2 caractères et se présente sous la forme AA
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdMagasin extends AbstractIdAvecEtablissement {
  public static final int LONGUEUR_CODE = 2;
  private final String code;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Constructeur privé pour un identifiant complet.
   * @param pEnumEtatObjetMetier Etat de l'objet métier en base de données
   * @param pIdEtablissement identifiant de l'établissement
   * @param pCode code unique du magasin
   */
  private IdMagasin(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   * @param pIdEtablissement identifiant de l'établissement
   * @param pCode code unique du magasin
   */
  public static IdMagasin getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new IdMagasin(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  /**
   * Contrôler la validité du code magasin.
   * Le code magasin est une chaîne alphanumérique de 2 caractères.
   * @param pValeur code unique du magasin
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code magasin n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE || valeur.length() > LONGUEUR_CODE) {
      throw new MessageErreurException("Le code magasin doit comporter " + LONGUEUR_CODE + " caractères : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   * @param pId identifiant du magasin
   * @param pVerifierExistance doit on vérifier l'existance en base de données
   */
  public static IdMagasin controlerId(IdMagasin pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du code magasin est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du code magasin n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Renvoyer le hashCode de l'identifiant
   * @return int
   */
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  /**
   * Comparer l'égalité de deux identifiants de magasins
   * @return boolean
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdMagasin)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants magasin.");
    }
    IdMagasin id = (IdMagasin) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && code.equals(id.code);
  }
  
  /**
   * Comparer des identifiants de magasins
   * @return int
   */
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdMagasin)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdMagasin id = (IdMagasin) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return code.compareTo(id.code);
  }
  
  /**
   * Retourner la représentation de l'identifiant sous forme de chaine de caractères
   * @return String
   */
  @Override
  public String getTexte() {
    return code;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner le code magasin.
   * Le code magasin est une chaîne alphanumérique de 2 caractères.
   * @return String
   */
  public String getCode() {
    return code;
  }
}
