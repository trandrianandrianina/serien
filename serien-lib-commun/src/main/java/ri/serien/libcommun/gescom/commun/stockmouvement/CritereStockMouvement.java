/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockmouvement;

import java.io.Serializable;

import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

/**
 * Critères de recherche pour le stock des articles.
 * Pour l'instant, tout les critères sont optionnels à l'exception du code article qui doit être renseigné.
 */
public class CritereStockMouvement implements Serializable {
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private IdArticle idArticle = null;
  private IdDocumentVente idDocumentVente = null;
  private IdDocumentAchat idDocumentAchat = null;
  
  /**
   * Retourner l'établissement sur lequel doit se porter la recherche des stocks
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'établissement pour lequel on souhaite les stocks.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Identifiant du magasin pour lequel on souhaite les stocks.
   * Si aucun magasin n'est transmis on recherche sur tous les magasins de l'établissement
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier le filtre sur le magasin.
   * L'établissement est également mis à jour si le magasin n'est pas null.
   * Si aucun magasin n'est renseigné, la recherche est effectuée sur tous les magasins de l'établissement.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
    if (idMagasin != null) {
      idEtablissement = idMagasin.getIdEtablissement();
    }
  }
  
  /**
   * Retourner le code article sur lequel la recherche se base.
   * On ne retourne pas l'IdArticle car la recherche peut se faire sur le même code article et plusieurs établissement
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Modifier le code article pour lequel on souhaite les stocks.
   * On fournit un code article, et non pas un identifiant article, car la recherche peut se faire sur des articles partageant le même
   * code sur plusieurs établissements.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
}
