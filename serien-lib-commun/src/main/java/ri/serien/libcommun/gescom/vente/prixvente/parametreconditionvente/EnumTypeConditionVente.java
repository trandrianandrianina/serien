/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des types des conditions de vente.
 * TODO
 * Mettre à jour la classe ri.serien.libcommun.gescom.commun.client.Negociation qui utilise un type String pour la catégorie.
 */
public enum EnumTypeConditionVente {
  PRIX_NET_OU_GRATUIT('N', "Prix net ou gratuit"),
  PRIX_DE_BASE('B', "Prix de base"),
  REMISE_EN_TAUX('R', "Remise en pourcentage"),
  COEFFICIENT('K', "Coefficient"),
  // Voir le paramètre 'FP'
  FORMULE_PRIX('F', "Formule prix"),
  AJOUT_EN_VALEUR('+', "Ajout en valeur"),
  REMISE_EN_VALEUR('-', "Remise en valeur"),
  AVOIR_SEPARE('A', "Avoir séparé"),
  ARTICLE_INTERDIT('X', "Article interdit");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeConditionVente(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeConditionVente valueOfByCode(Character pCode) {
    for (EnumTypeConditionVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de la condition de vente est invalide : " + pCode);
  }
  
}
