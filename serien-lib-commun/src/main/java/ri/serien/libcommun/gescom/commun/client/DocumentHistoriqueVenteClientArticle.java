/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumEtatBonDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;

public class DocumentHistoriqueVenteClientArticle implements Serializable {
  // Ne doit jamais passer en public, il faut à terme changer le type de typeDocument afin qu'il soit en phase avec la classe Document
  // private static final String CODE_TYPE_NON_DEFINI = "";
  private static final String CODE_TYPE_DEVIS = "DEV";
  private static final String CODE_TYPE_COMMANDE = "CMD";
  private static final String CODE_TYPE_LIVRAISON_RETRAIT = "EXP";
  private static final String CODE_TYPE_FACTURE = "FAC";
  private static final String CODE_TYPE_CHANTIER = "*CH";
  public static final String MODE_RECUPERATION_ENLEVEMENT = "E";
  public static final String MODE_RECUPERATION_LIVRAISON = "L";
  
  // Variables
  private IdEtablissement idEtablissement = null; // Code établisssement
  private int numeroBon = 0; // Numéro de bon
  private int suffixeBon = 0; // Suffixe du bon
  private String typeDocument = ""; // Type du document
  private Date dateCreation = null; // Date de création du bon
  private EnumEtatBonDocumentVente etatBon = null; // Etat du bon
  private String codeVendeur = ""; // Code vendeur
  private Date dateFacture = null; // Date de facturation
  private int numeroFacture = 0; // Numéro de la facture
  private BigDecimal quantite = BigDecimal.ZERO; // Quantité vendue
  private BigDecimal prixBase = BigDecimal.ZERO; // Prix de vente de base
  private BigDecimal prixNet = BigDecimal.ZERO; // Etat du bon
  private BigDecimal prixCalcule = BigDecimal.ZERO; // Etat du bon
  private String modeRecuperation = MODE_RECUPERATION_ENLEVEMENT; // Mode de récupération de la marchandise (enlèvement ou livraison)
  
  // -- Méthodes publiques
  
  /**
   * Retourne si le document est un devis.
   */
  public boolean isDevis() {
    return typeDocument.equals(CODE_TYPE_DEVIS);
  }
  
  /**
   * Retourne si le document est un chantier.
   */
  public boolean isChantier() {
    return typeDocument.equals(CODE_TYPE_CHANTIER);
  }
  
  /**
   * Retourne le libellé du status en fonction du type du document.
   */
  public String retournerStatus() {
    return etatBon.getLibelle();
  }
  
  /**
   * Retourne le libellé du type du document.
   */
  public String getlibelleTypeDocument() {
    if (this.typeDocument.equals(CODE_TYPE_DEVIS)) {
      return EnumTypeDocumentVente.DEVIS.getLibelle();
    }
    else if (this.typeDocument.equals(CODE_TYPE_COMMANDE)) {
      return EnumTypeDocumentVente.COMMANDE.getLibelle();
    }
    else if (this.typeDocument.equals(CODE_TYPE_LIVRAISON_RETRAIT)) {
      return EnumTypeDocumentVente.BON.getLibelle();
    }
    else if (this.typeDocument.equals(CODE_TYPE_FACTURE)) {
      return EnumTypeDocumentVente.FACTURE.getLibelle();
    }
    return "";
  }
  
  // -- Accesseurs
  
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  public int getNumeroBon() {
    return numeroBon;
  }
  
  public void setNumeroBon(int numeroBon) {
    this.numeroBon = numeroBon;
  }
  
  public int getSuffixeBon() {
    return suffixeBon;
  }
  
  public void setSuffixeBon(int suffixeBon) {
    this.suffixeBon = suffixeBon;
  }
  
  public String getTypeDocument() {
    return typeDocument;
  }
  
  public void setTypeDocument(String typeDocument) {
    this.typeDocument = typeDocument;
  }
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public EnumEtatBonDocumentVente getEtatBon() {
    return etatBon;
  }
  
  public void setEtatBon(EnumEtatBonDocumentVente etatBon) {
    this.etatBon = etatBon;
  }
  
  public String getCodeVendeur() {
    return codeVendeur;
  }
  
  public void setCodeVendeur(String codeVendeur) {
    this.codeVendeur = codeVendeur;
  }
  
  public Date getDateFacture() {
    return dateFacture;
  }
  
  public void setDateFacture(Date dateFacture) {
    this.dateFacture = dateFacture;
  }
  
  public int getNumeroFacture() {
    return numeroFacture;
  }
  
  public void setNumeroFacture(int numeroFacture) {
    this.numeroFacture = numeroFacture;
  }
  
  public BigDecimal getQuantite() {
    return quantite;
  }
  
  public void setQuantite(BigDecimal quantite) {
    this.quantite = quantite;
  }
  
  public BigDecimal getPrixBase() {
    return prixBase;
  }
  
  public void setPrixBase(BigDecimal prixBase) {
    this.prixBase = prixBase;
  }
  
  public BigDecimal getPrixNet() {
    return prixNet;
  }
  
  public void setPrixNet(BigDecimal prixNet) {
    this.prixNet = prixNet;
  }
  
  public BigDecimal getPrixCalcule() {
    return prixCalcule;
  }
  
  public void setPrixCalcule(BigDecimal prixCalcule) {
    this.prixCalcule = prixCalcule;
  }
  
  public String getModeRecuperation() {
    return modeRecuperation;
  }
  
  public void setModeRecuperation(String modeRecuperation) {
    this.modeRecuperation = modeRecuperation;
  }
  
}
