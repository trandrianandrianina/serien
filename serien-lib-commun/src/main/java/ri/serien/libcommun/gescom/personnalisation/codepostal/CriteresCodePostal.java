/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.codepostal;

import ri.serien.libcommun.commun.recherche.CritereAvecEtablissement;

public class CriteresCodePostal extends CritereAvecEtablissement {
  
  // Variables
  private String nomVille = "";
  
  /**
   * Retourne le nom de la ville
   */
  public String getNomVille() {
    return nomVille;
  }
  
  /**
   * Indique le nom de la ville
   */
  public void setNomVille(String pNomVille) {
    nomVille = pNomVille.trim().toUpperCase();
  }
  
}
