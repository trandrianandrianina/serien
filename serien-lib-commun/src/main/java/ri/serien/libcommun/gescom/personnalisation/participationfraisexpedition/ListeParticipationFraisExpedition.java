/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de participations aux frais d'expédition.
 */
public class ListeParticipationFraisExpedition
    extends ListeClasseMetier<IdParticipationFraisExpedition, ParticipationFraisExpedition, ListeParticipationFraisExpedition> {
  /**
   * Constructeur.
   */
  public ListeParticipationFraisExpedition() {
  }
  
  @Override
  public ListeParticipationFraisExpedition charger(IdSession pIdSession, List<IdParticipationFraisExpedition> pListeId) {
    return null;
  }
  
  /**
   * Charger toutes les participations aux frais d'expédition de l'établissement donné.
   */
  @Override
  public ListeParticipationFraisExpedition charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    CriteresParticipationFraisExpedition criteres = new CriteresParticipationFraisExpedition();
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeParticipationFraisExpedition(pIdSession, criteres);
  }
  
  /**
   * Charger toutes les participations aux frais d'expédition suivant les critères passés en paramètre.
   */
  public static ListeParticipationFraisExpedition charger(IdSession pIdSession, CriteresParticipationFraisExpedition pCriteres) {
    return ManagerServiceParametre.chargerListeParticipationFraisExpedition(pIdSession, pCriteres);
  }
}
