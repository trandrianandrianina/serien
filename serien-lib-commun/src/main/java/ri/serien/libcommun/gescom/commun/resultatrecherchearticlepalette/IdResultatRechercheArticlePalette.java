/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une ligne de résultat d'une recherche article de type palette.
 * 
 * Utilisé dans le comptoir. Cet identifiant est important car la première recherche retourne la liste complète mais uniquement avec
 * l'identifiant de renseigné. Lorsqu'une nouvelle page est affichée, la liste des identifiants est utilisée poru charger le reste des
 * informations.
 *
 * L'identifiant est composé de l'identifiant établissement et de l'identifiant article.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdResultatRechercheArticlePalette extends AbstractIdAvecEtablissement {
  private IdArticle idArticle = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et factories
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdResultatRechercheArticlePalette(EnumEtatObjetMetier pEnumEtatObjetMetier, IdArticle pIdArticle) {
    super(pEnumEtatObjetMetier, pIdArticle.getIdEtablissement());
    idArticle = pIdArticle;
  }
  
  /**
   * Créer un identifiant à partir de ses informations.
   */
  public static IdResultatRechercheArticlePalette getInstance(IdArticle pIdArticle) {
    IdArticle.controlerId(pIdArticle, false);
    return new IdResultatRechercheArticlePalette(EnumEtatObjetMetier.MODIFIE, pIdArticle);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdResultatRechercheArticlePalette controlerId(IdResultatRechercheArticlePalette pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du résultat de la recherche article est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du résultat de la recherche article n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getIdEtablissement().hashCode();
    cle = 37 * cle + idArticle.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdResultatRechercheArticlePalette)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de résultat de recherche article.");
    }
    IdResultatRechercheArticlePalette id = (IdResultatRechercheArticlePalette) pObject;
    return getIdEtablissement().equals(id.getIdEtablissement()) && idArticle.equals(id.idArticle);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdResultatRechercheArticlePalette)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdResultatRechercheArticlePalette id = (IdResultatRechercheArticlePalette) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return idArticle.compareTo(id.idArticle);
  }
  
  @Override
  public String getTexte() {
    return idArticle.getTexte();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Code de l'article.
   * Le code article ne peut pas être vide et fait au plus 20 caractères alphanumériques.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
}
