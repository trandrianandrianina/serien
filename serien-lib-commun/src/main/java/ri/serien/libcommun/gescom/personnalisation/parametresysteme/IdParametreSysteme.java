/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.parametresysteme;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un paramètre système.
 *
 * L'identifiant est composé du code établissement et du numéro du paramètre système. Le numéro du paramètre système est un nombre
 * compris entre 1 et 999 inclus.
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation. *
 */
public class IdParametreSysteme extends AbstractIdAvecEtablissement {
  private static final int LONGUEUR_CODE = 1;
  
  // Variables
  private final Integer numero;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdParametreSysteme(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, Integer pNumero) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdParametreSysteme getInstance(IdEtablissement pIdEtablissement, Integer pNumero) {
    return new IdParametreSysteme(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pNumero);
  }
  
  /**
   * Contrôler la validité du code groupe.
   * Le code groupe est composé d'un caractère alphanumérique.
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du paramètre système n'est pas renseigné.");
    }
    if (pValeur < 1 || pValeur > 999) {
      throw new MessageErreurException("Le numéro du paramètre système doit être compris entre 001 et 999 inclus : " + pValeur);
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdParametreSysteme controlerId(IdParametreSysteme pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du paramètre système est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du paramètre système n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + numero.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdParametreSysteme)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de groupes.");
    }
    IdParametreSysteme id = (IdParametreSysteme) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdParametreSysteme)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdParametreSysteme id = (IdParametreSysteme) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  /**
   * Le numéro d'un PS est toujours présenté au format PS000.
   * Par exemple : PS058
   */
  @Override
  public String getTexte() {
    return String.format("PS%03d", numero);
  }
  
  // -- Accesseurs
  
  /**
   * Numéro.
   * Le numéro est un nombre compris entre 1 et 999 inclus.
   */
  public Integer getNumero() {
    return numero;
  }
}
