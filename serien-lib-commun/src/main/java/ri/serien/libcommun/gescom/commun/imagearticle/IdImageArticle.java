/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.imagearticle;

import java.io.File;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;

/**
 * Identifiant unique d'un document de type image lié à un article.
 *
 * L'identifiant est construit via le chemin d'accès à l'image article et son nom.
 * Le chemin d'accès servant à l'idImageArticle est construit via la récupération de champs RPG et n'est pas forcément interprétable.
 * Une version utilisable du chemin d'accès est générée dans l'objet ImageArticle à partir de l'IdImageArticle.
 *
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 * 
 */
public class IdImageArticle extends AbstractId {
  
  // -- Variables
  private String cheminDAccesFichierImageArticle = null;
  private String cheminDAccesDossierImageArticle = null;
  private String nomImageArticle = null;
  
  public IdImageArticle(EnumEtatObjetMetier pEnumEtatObjetMetier, String pCheminDAcces, String pNomImageArticle) {
    super(pEnumEtatObjetMetier);
    cheminDAccesDossierImageArticle = pCheminDAcces;
    nomImageArticle = pNomImageArticle;
    cheminDAccesFichierImageArticle = controlerCheminDAcces(pCheminDAcces, pNomImageArticle);
  }
  
  /**
   * Créer une IdImage à partir d'une chaîne de caractères correspondant au chemin d'accès au dossier contenant l'image et au nom de
   * l'image.
   * 
   * @param pCheminDAccesDossierImage Chemin d'accès au dossier contenant l'image.
   * @param pNomImageArticle Nom de l'image.
   */
  public static IdImageArticle getInstance(String pCheminDAccesDossierImage, String pNomImageArticle) {
    return new IdImageArticle(EnumEtatObjetMetier.MODIFIE, pCheminDAccesDossierImage, pNomImageArticle);
  }
  
  /**
   * Créer une IdImage à partir d'une chaîne de caractères correspondant au chemin d'accès à l'image.
   * 
   * @param pCheminDAccesDossierImage Chemin d'accès au dossier contenant l'image.
   * @param pNomImageArticle Nom de l'image.
   */
  public static IdImageArticle getInstance(String pCheminDAccesFichierImage) {
    File image = new File(pCheminDAccesFichierImage);
    return new IdImageArticle(EnumEtatObjetMetier.MODIFIE, image.getParent(), image.getName());
  }
  
  /**
   * Contrôler la validité du code ImageArticle.
   * Le code ImageArticle est le chemin d'accès à l'image.
   * 
   * @param pFichierImage chaîne de caractères à contrôler.
   */
  private static String controlerCheminDAcces(String pCheminDAcces, String pNomImageArticle) {
    if (pCheminDAcces == null) {
      throw new MessageErreurException("Pas de chemin d'accès renseigné");
    }
    if (isCheminDAccesIFS(pCheminDAcces)) {
      String cheminDAcces =
          ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath()
              + File.separatorChar + (pCheminDAcces + pNomImageArticle).replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_');
      if (!new File(cheminDAcces).exists()) {
        ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().fichierArecup(pCheminDAcces + pNomImageArticle);
      }
      return cheminDAcces;
    }
    else {
      String cheminDossierTemp = pCheminDAcces.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
      return cheminDossierTemp + File.separatorChar + pNomImageArticle;
    }
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + cheminDAccesFichierImageArticle.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdImageArticle)) {
      throw new MessageErreurException("Erreur lors de la comparaison.");
    }
    IdImageArticle id = (IdImageArticle) pObject;
    return Constantes.equals(cheminDAccesFichierImageArticle, id.cheminDAccesFichierImageArticle);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdImageArticle)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdImageArticle id = (IdImageArticle) pObject;
    return cheminDAccesFichierImageArticle.compareTo(id.cheminDAccesFichierImageArticle);
  }
  
  @Override
  public String getTexte() {
    return nomImageArticle;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la nature du chemin d'accès.
   *
   * @param pCheminDAcces Chemin d'accès aux fichiers
   *
   * @return <b>True</b> si chemin IFS, <b>false</b> si chemin Windows.
   */
  private static boolean isCheminDAccesIFS(String pCheminDAcces) {
    if ((pCheminDAcces.indexOf(':') != -1) || (pCheminDAcces.startsWith("\\\\"))) {
      return false;
    }
    return true;
  }
  
  // -- Accesseurs
  /**
   * Chemin d'accès à l'image article.
   * @return Le chemin d'accès sous forme de String.
   */
  public String getCheminDAccesImageArticle() {
    return cheminDAccesFichierImageArticle;
  }
  
  /**
   * Nom du fichier image, avec l'extension.
   * @return nom du fichier image avec extension.
   */
  public String getNomImageArticle() {
    return nomImageArticle;
  }
  
  /**
   * Dossier parent du fichier image.
   * @return chemin d'accès au dossier contenant l'image article.
   */
  public String getDossierImageArticle() {
    return cheminDAccesDossierImageArticle;
  }
}
