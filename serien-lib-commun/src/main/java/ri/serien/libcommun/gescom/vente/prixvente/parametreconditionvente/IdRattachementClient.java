/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.IdGroupeConditionVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'un rattachement client pour une condition de ventes.
 * Cet identifiant n'a aucune valeur en base, il est créé à la volée.
 * 
 * L'identifiant est composé:
 * - de l'établissement,
 * - soit de l'identifiant d'un groupe de condition de ventes, soit de l'identifiant d'un client.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdRattachementClient extends AbstractIdAvecEtablissement {
  // Variables
  private final IdGroupeConditionVente idGroupeConditionVente;
  private final IdClient idClient;
  
  /**
   * Constructeur pour un groupe de condition de ventes.
   */
  private IdRattachementClient(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement,
      IdGroupeConditionVente pIdGroupeConditionVente) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    idGroupeConditionVente = controlerGroupe(pIdGroupeConditionVente);
    idClient = null;
  }
  
  /**
   * Constructeur pour un client.
   */
  private IdRattachementClient(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, IdClient pIdClient) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    idGroupeConditionVente = null;
    idClient = controlerClient(pIdClient);
  }
  
  /**
   * Créer un identifiant avec un groupe de condition de ventes.
   */
  public static IdRattachementClient getInstanceAvecGroupe(IdEtablissement pIdEtablissement,
      IdGroupeConditionVente pIdGroupeConditionVente) {
    return new IdRattachementClient(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pIdGroupeConditionVente);
  }
  
  /**
   * Créer un identifiant avec un client.
   */
  public static IdRattachementClient getInstanceAvecClient(IdEtablissement pIdEtablissement, IdClient pIdClient) {
    return new IdRattachementClient(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pIdClient);
  }
  
  /**
   * Créer un identifiant générique capable de distinguer s'il s'agit d'un numéro client ou d'un code de groupe de conditions de ventes.
   */
  public static IdRattachementClient getInstance(IdEtablissement pIdEtablissement, String pCode) {
    if (IdClient.isNumeroClient(pCode)) {
      return new IdRattachementClient(EnumEtatObjetMetier.MODIFIE, pIdEtablissement,
          IdClient.getInstance(pIdEtablissement, pCode, "000"));
    }
    return new IdRattachementClient(EnumEtatObjetMetier.MODIFIE, pIdEtablissement,
        IdGroupeConditionVente.getInstance(pIdEtablissement, pCode));
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du groupe de condition de ventes.
   */
  private IdGroupeConditionVente controlerGroupe(IdGroupeConditionVente pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("L'identifiant du groupe de condition de ventes n'est pas renseigné.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du client.
   */
  private IdClient controlerClient(IdClient pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("L'identifiant du client n'est pas renseigné.");
    }
    return pValeur;
  }
  
  // -- Méthodes publiques
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdRattachementClient controlerId(IdRattachementClient pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du rattachement client est invalide.");
    }
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du rattachement client n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    if (idGroupeConditionVente != null) {
      code = 37 * code + idGroupeConditionVente.hashCode();
    }
    if (idClient != null) {
      code = 37 * code + idClient.hashCode();
    }
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdRattachementClient)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de rattachement client.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdRattachementClient id = (IdRattachementClient) pObject;
    return Constantes.equals(idGroupeConditionVente, id.idGroupeConditionVente) && Constantes.equals(idClient, id.idClient);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdRattachementClient)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdRattachementClient id = (IdRattachementClient) pObject;
    if (idGroupeConditionVente != null) {
      return idGroupeConditionVente.compareTo(id.idGroupeConditionVente);
    }
    return idClient.compareTo(id.idClient);
  }
  
  @Override
  public String getTexte() {
    if (idGroupeConditionVente != null) {
      return getCodeEtablissement() + SEPARATEUR_ID + idGroupeConditionVente.getCode();
    }
    return getCodeEtablissement() + SEPARATEUR_ID + idClient.getIndicatif(IdClient.INDICATIF_NUM);
  }
  
  // -- Accesseurs
  
  /**
   * Retourner si l'identifiant concerne un client.
   * @return true=un client, false=un groupe de client.
   */
  public boolean isClient() {
    return idClient != null;
  }
  
  /**
   * Retourner si l'identifiant concerne un groupe de client.
   * @return true=un groupe de client, false=un client.
   */
  public boolean isGroupeClient() {
    return idGroupeConditionVente != null;
  }
  
  /**
   * Retourner le code.
   * @return Le code.
   */
  public String getCode() {
    if (isClient()) {
      return idClient.getIndicatif(IdClient.INDICATIF_NUM);
    }
    return idGroupeConditionVente.getCode();
  }
  
  /**
   * Retourner l'identifiant du groupe de la condition.
   * @return
   */
  public IdGroupeConditionVente getIdGroupeConditionVente() {
    if (isGroupeClient()) {
      return idGroupeConditionVente;
    }
    return null;
  }
  
  /**
   * Retourner l'identifiant du client.
   * @return
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
}
