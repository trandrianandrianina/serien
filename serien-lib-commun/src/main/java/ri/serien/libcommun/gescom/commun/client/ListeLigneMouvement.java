/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceStock;

/**
 * Liste de stocks.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de stocks.
 * La méthode chargerTout permet d'obtenir la liste de tous les Stocks d'un établissement.
 */
public class ListeLigneMouvement extends ListeClasseMetier<IdLigneMouvement, LigneMouvement, ListeLigneMouvement> {
  private Integer nombreDecimaleUniteStock = null;
  
  /**
   * Constructeurs.
   */
  public ListeLigneMouvement() {
  }
  
  /**
   * Charger les mouvements de stock dont les identifiants sont fournis.
   */
  @Override
  public ListeLigneMouvement charger(IdSession pIdSession, List<IdLigneMouvement> listeIdACharger) {
    if (nombreDecimaleUniteStock != null) {
      return ManagerServiceStock.chargerListeLigneMouvement(pIdSession, listeIdACharger, nombreDecimaleUniteStock);
    }
    else {
      return ManagerServiceStock.chargerListeLigneMouvement(pIdSession, listeIdACharger, 0);
    }
  }
  
  /**
   * Charger les mouvements de stock d'un établissement.
   */
  @Override
  public ListeLigneMouvement charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Construire une liste de mouvements de stock correspondant à la liste d'identifiants fournie en paramètre.
   * Les données ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeLigneMouvement creerListeNonChargee(List<IdLigneMouvement> pListeIdLigneMouvement) {
    ListeLigneMouvement listeLigneMouvement = new ListeLigneMouvement();
    if (pListeIdLigneMouvement != null) {
      for (IdLigneMouvement idLigneMouvement : pListeIdLigneMouvement) {
        LigneMouvement ligneMouvement = new LigneMouvement(idLigneMouvement);
        ligneMouvement.setCharge(false);
        listeLigneMouvement.add(ligneMouvement);
      }
    }
    return listeLigneMouvement;
  }
  
  /**
   * Nombre de décimal de l'unité de stock.
   */
  public Integer getNombreDecimaleUniteStock() {
    return nombreDecimaleUniteStock;
  }
  
  /**
   * Modifier le nombre de décimale de l'unité de stock.
   * Cette information est utilisée lors du chargement des mouvements de stocks afin de fournir la précision attendue.
   */
  public void setNombreDecimaleUniteStock(Integer pNombreDecimaleUniteStock) {
    nombreDecimaleUniteStock = pNombreDecimaleUniteStock;
  }
  
}
