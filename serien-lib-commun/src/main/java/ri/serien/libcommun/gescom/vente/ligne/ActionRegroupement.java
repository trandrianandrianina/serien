/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import java.io.Serializable;

public class ActionRegroupement implements Serializable {
  // Constantes
  public static final int NE_RIEN_FAIRE = 0;
  public static final int SUPPRIMER = 1;
  public static final int METTRE_A_JOUR = 2;
  
  // Variables
  private int action = NE_RIEN_FAIRE;
  private IdLigneVente idLigneVente = null;
  private Regroupement regroupement = null;
  
  /**
   * Constructeur.
   */
  public ActionRegroupement() {
  }
  
  /**
   * Constructeur.
   */
  public ActionRegroupement(int pAction, Regroupement pRegroupement) {
    action = pAction;
    regroupement = pRegroupement;
  }
  
  /**
   * Constructeur.
   */
  public ActionRegroupement(int pAction, IdLigneVente pIdLigneVente) {
    action = pAction;
    idLigneVente = pIdLigneVente;
  }
  
  // -- Accesseurs
  
  public int getAction() {
    return action;
  }
  
  public void setAction(int action) {
    this.action = action;
  }
  
  public IdLigneVente getIdLigneVente() {
    return idLigneVente;
  }
  
  public void setIdLigne(IdLigneVente idLigneVente) {
    this.idLigneVente = idLigneVente;
  }
  
  public Regroupement getRegroupement() {
    return regroupement;
  }
  
  public void setRegroupement(Regroupement regroupement) {
    this.regroupement = regroupement;
  }
  
}
