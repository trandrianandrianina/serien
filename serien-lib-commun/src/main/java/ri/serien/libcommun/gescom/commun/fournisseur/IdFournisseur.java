/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un fournisseur.
 *
 * L'identifiant est composé du code établissement, du numéro de collectif fournisseur et du code fournisseur.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdFournisseur extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_COLLECTIF = 1;
  public static final int LONGUEUR_NUMERO = 6;
  public static final int LONGUEUR_INDICATIF_COURT = LONGUEUR_COLLECTIF + LONGUEUR_NUMERO;
  public static final int LONGUEUR_INDICATIF_LONG = LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_COLLECTIF + LONGUEUR_NUMERO;
  
  // Variables
  private final Integer collectif;
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdFournisseur(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, Integer pCollectif, Integer pNumero) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    collectif = controlerCollectif(pCollectif);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   * 
   * @param pIdEtablissement Identifiant de l'établissement;
   * @param pCollectif Collectif du fournisseur.
   * @param pNumero Numéor du fournisseur.
   * @return Identifiant du fournisseur.
   */
  public static IdFournisseur getInstance(IdEtablissement pIdEtablissement, Integer pCollectif, Integer pNumero) {
    return new IdFournisseur(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCollectif, pNumero);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée et en chaînes de caractères.
   * 
   * @param pIdEtablissement Identifiant de l'établissement;
   * @param pCollectif Collectif du fournisseur.
   * @param pNumero Numéor du fournisseur.
   * @return Identifiant du fournisseur.
   */
  public static IdFournisseur getInstance(IdEtablissement pIdEtablissement, String pCollectif, String pNumero) {
    // Tester les paramètres
    if (pCollectif == null) {
      throw new MessageErreurException("Impossible de construire l'identifiant du fournisseur car son collectif est invalide.");
    }
    if (pNumero == null) {
      throw new MessageErreurException("Impossible de construire l'identifiant du fournisseur car son numéro est invalide.");
    }
    
    // Convertir le collectif
    Integer collectif = 0;
    try {
      collectif = Integer.parseInt(pCollectif.trim());
    }
    catch (NumberFormatException e) {
      throw new MessageErreurException(
          "Impossible de construire l'identifiant du fournisseur car son collectif est invalide : " + pCollectif);
    }
    
    // Convertir le numéro
    Integer numero = 0;
    try {
      numero = Integer.parseInt(pNumero.trim());
    }
    catch (NumberFormatException e) {
      throw new MessageErreurException("Impossible de construire l'identifiant du fournisseur car son numéro est invalide : " + pNumero);
    }
    
    // Construire l'id fournisseur
    return new IdFournisseur(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, collectif, numero);
  }
  
  /**
   * Créer un identifiant à partir d'un établissement et de son indicatif court.
   */
  public static IdFournisseur getInstanceParIndicatif(IdEtablissement pIdEtablissement, String pIndicatifCourt) {
    if (pIndicatifCourt == null) {
      throw new MessageErreurException("L'indicatif n'est pas renseigné.");
    }
    if (pIndicatifCourt.length() != LONGUEUR_INDICATIF_COURT) {
      throw new MessageErreurException("L'indicatif n'a pas la longueur attendu : " + LONGUEUR_INDICATIF_COURT);
    }
    return new IdFournisseur(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, extraireCollectifDeIndicatif(pIndicatifCourt),
        extraireNumeroDeIndicatif(pIndicatifCourt));
  }
  
  /**
   * Créer un identifiant à partir de son indicatif long.
   */
  public static IdFournisseur getInstanceParIndicatif(String pIndicatifLong) {
    if (pIndicatifLong == null) {
      throw new MessageErreurException("L'indicatif n'est pas renseigné.");
    }
    if (pIndicatifLong.length() != LONGUEUR_INDICATIF_LONG) {
      throw new MessageErreurException("L'indicatif n'a pas la longueur attendu : " + LONGUEUR_INDICATIF_LONG);
    }
    return new IdFournisseur(EnumEtatObjetMetier.MODIFIE, extraireEtablissementDeIndicatif(pIndicatifLong),
        extraireCollectifDeIndicatif(pIndicatifLong), extraireNumeroDeIndicatif(pIndicatifLong));
  }
  
  /**
   * Contrôler la validité du collectif fournisseur.
   * Il doit être supérieur à zéro et doit comporter au maximum 1 chiffres (entre 1 et 9).
   */
  private static Integer controlerCollectif(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le collectif du fournisseur n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le collectif du fournisseur est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_COLLECTIF)) {
      throw new MessageErreurException("Le collectif du fournisseur doit comporter un seul chiffre.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du numéro fournisseur.
   * Il doit être supérieur à zéro et doit comporter au maximum 6 chiffres (entre 1 et 999 999).
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du fournisseur n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du fournisseur est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du fournisseur est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdFournisseur controlerId(IdFournisseur pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du fournisseur est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du fournisseur n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  /**
   * Extraire l'établissement à partir de l'indicatif fournisseur.
   */
  private static IdEtablissement extraireEtablissementDeIndicatif(String pIndicatif) {
    if (pIndicatif.length() == LONGUEUR_INDICATIF_LONG) {
      int offsetDeb = 0;
      int offsetFin = offsetDeb + LONGUEUR_CODE_ETABLISSEMENT;
      return IdEtablissement.getInstance(pIndicatif.substring(offsetDeb, offsetFin));
    }
    else {
      throw new MessageErreurException("La longueur du collectif fournisseur n'est pas correcte : " + pIndicatif);
    }
  }
  
  /**
   * Extraire l'établissement à partir de l'indicatif fournisseur.
   */
  private static Integer extraireCollectifDeIndicatif(String pIndicatif) {
    if (pIndicatif.length() == LONGUEUR_INDICATIF_LONG) {
      int offsetDeb = LONGUEUR_CODE_ETABLISSEMENT;
      int offsetFin = offsetDeb + LONGUEUR_COLLECTIF;
      return Constantes.convertirTexteEnInteger(pIndicatif.substring(offsetDeb, offsetFin));
    }
    else if (pIndicatif.length() == LONGUEUR_INDICATIF_COURT) {
      int offsetDeb = 0;
      int offsetFin = offsetDeb + LONGUEUR_COLLECTIF;
      return Constantes.convertirTexteEnInteger(pIndicatif.substring(offsetDeb, offsetFin));
    }
    else {
      throw new MessageErreurException("La longueur du collectif fournisseur n'est pas correcte : " + pIndicatif);
    }
  }
  
  /**
   * Extraire l'établissement à partir de l'indicatif fournisseur.
   */
  private static Integer extraireNumeroDeIndicatif(String pIndicatif) {
    if (pIndicatif.length() == LONGUEUR_INDICATIF_LONG) {
      int offsetDeb = LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_COLLECTIF;
      int offsetFin = offsetDeb + LONGUEUR_NUMERO;
      return Constantes.convertirTexteEnInteger(pIndicatif.substring(offsetDeb, offsetFin));
    }
    else if (pIndicatif.length() == LONGUEUR_INDICATIF_COURT) {
      int offsetDeb = LONGUEUR_COLLECTIF;
      int offsetFin = offsetDeb + LONGUEUR_NUMERO;
      return Constantes.convertirTexteEnInteger(pIndicatif.substring(offsetDeb, offsetFin));
    }
    else {
      throw new MessageErreurException("La longueur du collectif fournisseur n'est pas correcte : " + pIndicatif);
    }
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + collectif.hashCode();
    cle = 37 * cle + numero.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdFournisseur)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de fournisseur.");
    }
    
    IdFournisseur id = (IdFournisseur) pObject;
    return (getCodeEtablissement().equals(id.getCodeEtablissement()) && collectif.equals(id.collectif) && numero.equals(id.numero));
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdFournisseur)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdFournisseur id = (IdFournisseur) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = collectif.compareTo(id.collectif);
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  /**
   * Cette méthode est surchargée pour définir le format d'affichage standard de cet identifiant.
   * Le format souhaité est constitué par les composants de l'identifiant (sauf l'établissement) séparés par un SEPARATEUR_ID.
   */
  @Override
  public String getTexte() {
    return "" + collectif + SEPARATEUR_ID + numero;
  }
  
  // -- Accesseurs
  
  /**
   * Collectif fournisseur.
   * Le collectif fournisseur est compris entre 1 et 9.
   */
  public Integer getCollectif() {
    return collectif;
  }
  
  /**
   * Numéro fournisseur.
   * Le numéro de fournisseur est compris entre 1 et 999 999.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Indicatif long du fournisseur.
   * L'indicatif est une version condensée de l'identifiant en une chaîne de caractère. L'indicatif long comporte l'établissement,
   * le collectif et le numéro du fournisseur. L'indicatif court ne comporte que le collectif et le numéro du fournisseur.
   * Format : Code etablissement + Collectif + Numéro
   */
  public String getIndicatifLong() {
    return String.format("%" + LONGUEUR_CODE_ETABLISSEMENT + "s%d%0" + LONGUEUR_NUMERO + "d", getCodeEtablissement(), collectif, numero);
  }
  
  /**
   * Indicatif court fournisseur.
   * L'indicatif est une version condensée de l'identifiant en une chaîne de caractère. L'indicatif long comporte l'établissement,
   * le collectif et le numéro du fournisseur. L'indicatif court ne comporte que le collectif et le numéro du fournisseur.
   * Format : Collectif + Numéro
   */
  public String getIndicatifCourt() {
    return String.format("%d%0" + LONGUEUR_NUMERO + "d", collectif, numero);
  }
  
}
