/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Filtre article commentaire sur les recherches articles
 * 
 */
public enum EnumFiltreArticleCommentaire {
  OPTION_TOUS_LES_ARTICLES_SAUF_COMMENTAIRES(0, "Tous les articles sauf les articles commentaires"),
  OPTION_LES_ARTICLES_COMMENTAIRES_UNIQUEMENT(1, "Articles commentaires seulement"),
  OPTION_TOUS_LES_ARTICLES(2, "Tous les articles");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumFiltreArticleCommentaire(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumFiltreArticleCommentaire valueOfByCode(Integer pCode) {
    for (EnumFiltreArticleCommentaire value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("La valeur du filtre articles commentaires est invalide : " + pCode);
  }
}
