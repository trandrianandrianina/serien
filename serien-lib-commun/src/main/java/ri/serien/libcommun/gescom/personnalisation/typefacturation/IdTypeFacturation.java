/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typefacturation;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un type de facturation.
 *
 * L'identifiant est composé du code du type de facturation.
 * Le code type de facturation correspond au paramètre TF du menu des ventes ou des achats.
 * Il est constitué de 2 caractère et se présente sous la forme AA
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdTypeFacturation extends AbstractIdAvecEtablissement {
  private static final int LONGUEUR_CODE_MIN = 1;
  private static final int LONGUEUR_CODE_MAX = 2;
  private static final String CODE_FACTURATION_SANS_TVA = "5";
  private static final String CODE_FACTURATION_UE = "&";
  
  // Variables
  private final String code;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdTypeFacturation(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdTypeFacturation getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new IdTypeFacturation(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  /**
   * Créer un identifiant pour la facturation sans TVA.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public static IdTypeFacturation getInstanceFacturationSansTVA(IdEtablissement pIdEtablissement) {
    return new IdTypeFacturation(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, CODE_FACTURATION_SANS_TVA);
  }
  
  /**
   * Créer un identifiant pour la facturation UE avec TVA.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public static IdTypeFacturation getInstanceFacturationUE(IdEtablissement pIdEtablissement) {
    return new IdTypeFacturation(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, CODE_FACTURATION_UE);
  }
  
  /**
   * Contrôler la validité du code de du type de facturation.
   * Le code est une chaîne alphanumérique de 2 caractère.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code du type de facturation n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MAX) {
      throw new MessageErreurException("Le code du type de facturation doit comporter " + LONGUEUR_CODE_MIN + " caractère : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdTypeFacturation controlerId(IdTypeFacturation pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du type de facturation est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du type de facturation n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdTypeFacturation)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de type de facturation.");
    }
    IdTypeFacturation id = (IdTypeFacturation) pObject;
    return code.equals(id.getCode());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdTypeFacturation)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdTypeFacturation id = (IdTypeFacturation) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return "" + code;
  }
  
  // -- Accesseurs
  
  /**
   * Code type de facturation.
   * Le code est une chaîne alphanumérique de 1 caractère.
   */
  public String getCode() {
    return code;
  }
  
}
