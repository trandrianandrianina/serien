/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typegratuit;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Type de gratuit.
 * Le type de gratuit n'est pas lié à un établissement.
 */
public class TypeGratuit extends AbstractClasseMetier<IdTypeGratuit> {
  
  // Variables
  private String libelle = null;
  private EnumNonEditionTypeGratuit nonEdition = null;
  private EnumEditionLibelleCompletTypeGratuit LibelleComplet = null;
  private String EditionPrixRemise = null;
  private String aucunEcoTaxe = null;
  private String magasinTransfert = null;
  private EnumNonGereStatistiqueTypeGratuit nonGereStatic = null;
  private String articleCommentaire = null;
  
  /**
   * Constructeur
   */
  public TypeGratuit(IdTypeGratuit pId) {
    super(pId);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Affiche le libelle
   */
  public String getLibelle(String pLibelle) {
    return libelle;
  }
  
  /**
   * Modifier le département.
   */
  public void setLibelle(String pLibelle) {
    this.libelle = pLibelle;
  }
  
  /**
   * Affiche l'édition sur la facture.
   */
  public EnumNonEditionTypeGratuit getEditionFacture(String pEdiFacture) {
    return nonEdition;
  }
  
  /**
   * Modifier l'édition sur la facture.
   */
  public void setEditionFacture(EnumNonEditionTypeGratuit pEdiFacture) {
    this.nonEdition = pEdiFacture;
  }
  
  /**
   * Affiche l'édition sur le prix.
   */
  public EnumEditionLibelleCompletTypeGratuit getEditionLibelleComplet(String pLibelleComplet) {
    return LibelleComplet;
  }
  
  /**
   * Modifier l'édition sur le prix.
   */
  public void setEditionLibelleComplet(EnumEditionLibelleCompletTypeGratuit pLibelleComplet) {
    this.LibelleComplet = pLibelleComplet;
  }
  
  /**
   * Affiche le libelle complet.
   */
  public boolean getEditionPrix(String pEdiPrix) {
    if (pEdiPrix.contentEquals("**")) {
      return true;
    }
    else {
      return false;
    }
  }
  
  /**
   * Modifier le libelle complet.
   */
  public void setEditionPrix(String pEdiPrix) {
    this.EditionPrixRemise = pEdiPrix;
  }
  
  /**
   * Affiche le magasin pour transfert.
   */
  public String getEditionMagTransfert(String pMagTransfert) {
    return magasinTransfert;
  }
  
  /**
   * Modifier le magasin pour transfert.
   */
  public void setEditionMagTransfert(String pMagTransfert) {
    this.magasinTransfert = pMagTransfert;
  }
  
  /**
   * Affiche si c'est géré en statisitique.
   */
  public EnumNonGereStatistiqueTypeGratuit getEditionStatistique(String pStatistique) {
    return nonGereStatic;
  }
  
  /**
   * Modifier si c'est géré en statisitique.
   */
  public void setEditionStatistique(EnumNonGereStatistiqueTypeGratuit pStatistique) {
    this.nonGereStatic = pStatistique;
  }
  
  /**
   * Affiche si il n'y pas d'éco taxe.
   */
  public boolean getEditionEcoTaxe(String pEcoTaxe) {
    if (pEcoTaxe.contentEquals("**")) {
      return true;
    }
    else {
      return false;
    }
  }
  
  /**
   * Modifier si il n'y pas d'éco taxe.
   */
  public void setEditionEcoTaxe(String pEcoTaxe) {
    this.aucunEcoTaxe = pEcoTaxe;
  }
  
  /**
   * Affiche le commentaire.
   */
  public String getEditionCommentaire(String pCommentaire) {
    return articleCommentaire;
  }
  
  /**
   * Modifier le commentaire.
   */
  public void setEditionCommentaire(String pCommentaire) {
    this.articleCommentaire = pCommentaire;
  }
}
