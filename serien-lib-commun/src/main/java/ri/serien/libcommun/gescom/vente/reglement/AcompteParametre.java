/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import java.math.BigDecimal;

import ri.serien.libcommun.gescom.personnalisation.categorieclient.CategorieClient;

/**
 * Cette classe regroupe les variables qui doivent être collectées afin de pouvoir calculer l'acompte d'un document de vente.
 */
public class AcompteParametre {
  // Variables
  private boolean acompteObligatoireNormal = CategorieClient.ACOMPTE_OBLIGATOIRE_PAR_DEFAUT;
  private BigDecimal pourcentageNormal = new BigDecimal(CategorieClient.POURCENTAGE_ACOMPTE_PAR_DEFAUT);
  private boolean acompteObligatoireDetectionArticleSpecial = CategorieClient.ACOMPTE_OBLIGATOIRE_PAR_DEFAUT;
  private BigDecimal pourcentageDetectionArticleSpecial = new BigDecimal(CategorieClient.POURCENTAGE_ACOMPTE_PAR_DEFAUT);
  private boolean documentContientArticleSpecial = false;
  
  // -- Méthodes publiques
  
  /**
   * Efface toutes les variables.
   */
  public void effacerVariables() {
    acompteObligatoireNormal = CategorieClient.ACOMPTE_OBLIGATOIRE_PAR_DEFAUT;
    pourcentageNormal = new BigDecimal(CategorieClient.POURCENTAGE_ACOMPTE_PAR_DEFAUT);
    acompteObligatoireDetectionArticleSpecial = CategorieClient.ACOMPTE_OBLIGATOIRE_PAR_DEFAUT;
    pourcentageDetectionArticleSpecial = new BigDecimal(CategorieClient.POURCENTAGE_ACOMPTE_PAR_DEFAUT);
    documentContientArticleSpecial = false;
  }
  
  /**
   * Retourne si l'acompte est obligatoire (cas normal et cas avec détection d'articles spéciaux).
   */
  public boolean isObligatoire() {
    // Cas normal
    if (acompteObligatoireNormal) {
      return true;
    }
    // Cas avec détection d'article spécial
    if (documentContientArticleSpecial && acompteObligatoireDetectionArticleSpecial
        && pourcentageDetectionArticleSpecial.intValue() > 0) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne le pourcentage d'acompte à utiliser en fonction des cas d'utilisation.
   */
  public BigDecimal getPourcentage() {
    // Cas normal sans détection des articles spéciaux
    if (!documentContientArticleSpecial) {
      return pourcentageNormal;
    }
    // Cas où il y a détection des articles spéciaux
    // Si le pourcentage d'acompte est supérieur à celui des articles speciaux alors c'est lui qui est utilisé
    if (pourcentageNormal.compareTo(pourcentageDetectionArticleSpecial) > 0) {
      return pourcentageNormal;
    }
    return pourcentageDetectionArticleSpecial;
  }
  
  // -- Accesseurs
  
  public boolean isAcompteObligatoireNormal() {
    return acompteObligatoireNormal;
  }
  
  public void setAcompteObligatoireNormal(boolean acompteObligatoireNormal) {
    this.acompteObligatoireNormal = acompteObligatoireNormal;
  }
  
  public BigDecimal getPourcentageNormal() {
    return pourcentageNormal;
  }
  
  public void setPourcentageNormal(BigDecimal pourcentageNormal) {
    this.pourcentageNormal = pourcentageNormal;
  }
  
  public boolean isAcompteObligatoireDetectionArticleSpecial() {
    return acompteObligatoireDetectionArticleSpecial;
  }
  
  public void setAcompteObligatoireDetectionArticleSpecial(boolean acompteObligatoireDetectionArticleSpecial) {
    this.acompteObligatoireDetectionArticleSpecial = acompteObligatoireDetectionArticleSpecial;
  }
  
  public BigDecimal getPourcentageDetectionArticleSpecial() {
    return pourcentageDetectionArticleSpecial;
  }
  
  public void setPourcentageDetectionArticleSpecial(BigDecimal pourcentageDetectionArticleSpecial) {
    this.pourcentageDetectionArticleSpecial = pourcentageDetectionArticleSpecial;
  }
  
  public boolean isDocumentContientArticleSpecial() {
    return documentContientArticleSpecial;
  }
  
  public void setDocumentContientArticleSpecial(boolean documentContientArticleSpecial) {
    this.documentContientArticleSpecial = documentContientArticleSpecial;
  }
  
}
