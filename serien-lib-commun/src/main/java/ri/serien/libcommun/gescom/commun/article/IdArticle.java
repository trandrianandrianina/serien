/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un article.
 *
 * L'identifiant est composé du code établissement et du code article.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdArticle extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_CODE_ARTICLE = 20;
  public static final String CODE_ARTICLE_SUPPORT_REMISE = "*REMV1";
  
  // Variables
  private final String codeArticle;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdArticle(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCodeArticle) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    codeArticle = controlerCodeArticle(pCodeArticle);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   */
  private IdArticle(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    codeArticle = null;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdArticle getInstance(IdEtablissement pIdEtablissement, String pCodeArticle) {
    return new IdArticle(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCodeArticle);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdArticle getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdArticle(pIdEtablissement);
  }
  
  /**
   * Contrôler la validité du code article.
   * Le code article ne peut pas être vide et ne doit pas dépasser 20 caractères.
   */
  public static String controlerCodeArticle(String pValeur) {
    if (pValeur == null || pValeur.trim().isEmpty()) {
      throw new MessageErreurException("Le code article n'est pas renseigné.");
    }
    if (pValeur.trim().length() > LONGUEUR_CODE_ARTICLE) {
      throw new MessageErreurException("Le code article dépasse la taille maximum (20 caractères) : " + pValeur);
    }
    return Constantes.normerTexte(pValeur);
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdArticle controlerId(IdArticle pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de l'article est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de l'article n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getIdEtablissement().hashCode();
    cle = 37 * cle + codeArticle.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdArticle)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants d'article.");
    }
    IdArticle id = (IdArticle) pObject;
    return getIdEtablissement().equals(id.getIdEtablissement()) && codeArticle.equals(id.codeArticle);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdArticle)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdArticle id = (IdArticle) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return codeArticle.compareTo(id.codeArticle);
  }
  
  @Override
  public String getTexte() {
    return "" + codeArticle;
  }
  
  // -- Accesseurs
  
  /**
   * Code de l'article.
   * Le code article ne peut pas être vide et fait au plus 20 caractères alphanumériques.
   */
  public String getCodeArticle() {
    return codeArticle;
  }
  
  /**
   * Indiquer si c'est l'identifiant de l'article support pour les remises.
   */
  public boolean isArticleSupportRemise() {
    return codeArticle != null && codeArticle.equals(CODE_ARTICLE_SUPPORT_REMISE);
  }
  
  /**
   * Retourne le code article sur 20 caractères non trimés.
   */
  public String getIndicatifCodeArticle() {
    return String.format("%-" + LONGUEUR_CODE_ARTICLE + "s", codeArticle);
  }
}
