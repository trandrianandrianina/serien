/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.magasin;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Critères de recherche des magasins.
 * Aucun critère n'est obligatoire.
 */
public class CritereMagasin implements Serializable {
  private IdEtablissement idEtablissement = null;
  
  /**
   * Identifiant établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'identifiant de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement idEtablissement) {
    this.idEtablissement = idEtablissement;
  }
  
}
