/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.magasin;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Magasin.
 */
public class Magasin extends AbstractClasseMetier<IdMagasin> {
  private String nom = null;
  private IdRepresentant idRepresentant = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   * @param pIdMagasin identifiant du magasin
   */
  public Magasin(IdMagasin pIdMagasin) {
    super(pIdMagasin);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner la représentation du magasin sous forme de chaine de caractères
   * @return String
   */
  @Override
  public String getTexte() {
    return nom;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner le nom du magasin.
   * @return String
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Retourner le nom du représentant suivi de son code entre parenthèse
   * Exemple : Jean DUPOND (JDD).
   * Par sécurité, le code est retourné seul au cas où le nom ne serait pas renseigné.
   * @return String
   */
  public String getNomAvecCode() {
    if (nom != null) {
      return nom + " (" + getId().getCode() + ")";
    }
    else {
      return getId().getCode();
    }
  }
  
  /**
   * Modifier le nom du magasin.
   * @param pNom
   */
  public void setNom(String pNom) {
    if (pNom == null) {
      throw new MessageErreurException("Le nom du magasin est invalide");
    }
    nom = pNom.trim();
  }
  
  /**
   * Retourner l'identifiant du représentant par défaut pour ce magasin.
   * @return IdRepresentant
   */
  public IdRepresentant getIdRepresentant() {
    return idRepresentant;
  }
  
  /**
   * Modifier l'identifiant du représentant par défaut pour ce magasin.
   * @param idRepresentant
   */
  public void setIdRepresentant(IdRepresentant pIdRepresentant) {
    idRepresentant = pIdRepresentant;
  }
}
