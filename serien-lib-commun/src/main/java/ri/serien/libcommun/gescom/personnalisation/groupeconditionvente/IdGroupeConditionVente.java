/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.groupeconditionvente;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une condition de vente.
 *
 * L'identifiant est composé du code établissement et du code condition de vente.
 * Le code condition de vente correspond au paramètre CN du menu des ventes.
 * Il est constitué de 1 à 5 caractères et se présente sous la forme AXXXX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique
 * facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdGroupeConditionVente extends AbstractIdAvecEtablissement {
  private static final int LONGUEUR_CODE_MIN = 1;
  public static final int LONGUEUR_CODE_MAX = 5;
  
  // Variables
  private final String code;
  
  // -- Méthodes publiques
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdGroupeConditionVente(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   */
  private IdGroupeConditionVente(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    code = "";
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdGroupeConditionVente getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new IdGroupeConditionVente(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdGroupeConditionVente getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdGroupeConditionVente(pIdEtablissement);
  }
  
  /**
   * Contrôler la validité du code condition de vente.
   * Le code condition de vente est une chaîne alphanumérique de 1 à 5 caractères.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code condition de vente n'est pas renseigné.");
    }
    String valeur = pValeur.toUpperCase().trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MAX) {
      throw new MessageErreurException("Le code condition de vente doit comporter entre " + LONGUEUR_CODE_MIN + " et " + LONGUEUR_CODE_MAX
          + " caractères : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdGroupeConditionVente controlerId(IdGroupeConditionVente pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du code condition de vente est invalide.");
    }
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du code condition de vente n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdGroupeConditionVente)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de condition de vente.");
    }
    IdGroupeConditionVente id = (IdGroupeConditionVente) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdGroupeConditionVente)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdGroupeConditionVente id = (IdGroupeConditionVente) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
  // -- Accesseurs
  
  /**
   * Code condition de vente.
   * Le code condition de vente est une chaîne alphanumérique de 1 à 5 caractères.
   */
  public String getCode() {
    return code;
  }
}
