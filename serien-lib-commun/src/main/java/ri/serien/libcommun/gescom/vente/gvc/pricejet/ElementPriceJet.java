/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc.pricejet;

import java.io.Serializable;

public class ElementPriceJet implements Serializable {
  // Pour les articles TOUS : BUUUUUG DE l'API PRICEJET
  private String code_article = null;
  private String code = null;
  // Pour les concurrents
  private String id = null;
  private String nom = null;
  private String photo = null;

  public String getCode_article() {
    return code_article;
  }

  public void setCode_article(String code_article) {
    this.code_article = code_article;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

}
