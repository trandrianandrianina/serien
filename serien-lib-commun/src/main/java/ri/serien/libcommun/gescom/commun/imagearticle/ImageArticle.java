/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.imagearticle;

import javax.swing.ImageIcon;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Photo
 * Les objets Photo ne sont pas liés à un établissement.
 */
public class ImageArticle extends AbstractClasseMetier<IdImageArticle> {
  
  // -- Variables
  private ImageIcon iconeImageArticle = null;
  
  public ImageArticle(IdImageArticle pId) {
    super(pId);
  }
  
  @Override
  public String getTexte() {
    return id.getNomImageArticle();
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la nature du chemin d'accès.
   *
   * @param pCheminDAcces Chemin d'accès aux fichiers
   * @return <b>True</b> si chemin IFS, <b>false</b> sinon.
   */
  private boolean controlerPathIFS(String pCheminDAcces) {
    if ((pCheminDAcces.indexOf(':') != -1) || (pCheminDAcces.startsWith("\\\\"))) {
      return false;
    }
    return true;
  }
  
  // -- Accesseur
  
  /**
   * Retourner la représentation graphique de l'image.
   * @return Représentation graphique de l'image.
   */
  public ImageIcon getIconeImage() {
    return iconeImageArticle;
  }
  
  /**
   * Définir la représentation graphique de l'image.
   * @param pIconeImage Représentation graphique de l'image.
   */
  public void setIconeImage(ImageIcon pIconeImage) {
    iconeImageArticle = pIconeImage;
  }
}
