/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.categorieclient;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Catégorie client.
 */
public class CategorieClient extends AbstractClasseMetier<IdCategorieClient> {
  public static final boolean ACOMPTE_OBLIGATOIRE_PAR_DEFAUT = true;
  public static final int POURCENTAGE_ACOMPTE_PAR_DEFAUT = 30;
  
  // Variables
  private String libelle = "";
  private boolean chantierObligatoire = false;
  private boolean acompteObligatoire = ACOMPTE_OBLIGATOIRE_PAR_DEFAUT;
  private int pourcentageAcompte = POURCENTAGE_ACOMPTE_PAR_DEFAUT;
  // Dans le cas où le document de ventes contienne des articles spéciaux
  private boolean acompteObligatoireDetectionArticleSpecial = ACOMPTE_OBLIGATOIRE_PAR_DEFAUT;
  private int pourcentageAcompteDetectionArticleSpecial = POURCENTAGE_ACOMPTE_PAR_DEFAUT;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public CategorieClient(IdCategorieClient pIdCategorieClient) {
    super(pIdCategorieClient);
  }
  
  // -- Méthodes publiques
  
  // -- Accesseurs
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Libellé de la catégorie client.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Renseigner le libellé de la catégorie client.
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  /**
   * Vérifier si la saisie d'un chantier dans un document est obligatoire pour cette catégorie client.
   */
  public boolean isChantierObligatoire() {
    return chantierObligatoire;
  }
  
  /**
   * Indiquer si la saisie d'un chantier dans un document est obligatoire pour cette catégorie client.
   */
  public void setChantierObligatoire(boolean pChantierObligatoire) {
    this.chantierObligatoire = pChantierObligatoire;
  }
  
  /**
   * Vérifier si la saisie d'un acompte pour une commande est obligatoire pour cette catégorie client.
   */
  public boolean isAcompteObligatoire() {
    return acompteObligatoire;
  }
  
  /**
   * Indiquer si la saisie d'un acompte pour une commande est obligatoire pour cette catégorie client.
   */
  public void setAcompteObligatoire(boolean pCompteObligatoire) {
    this.acompteObligatoire = pCompteObligatoire;
  }
  
  /**
   * Pourcentage d'acompte minimum pour cette catégorie de client.
   */
  public int getPourcentageAcompte() {
    return pourcentageAcompte;
  }
  
  /**
   * Modifier le pourcentage d'acompte minimum pour cette catégorie de client.
   */
  public void setPourcentageAcompte(int pourcentageAcompte) {
    this.pourcentageAcompte = pourcentageAcompte;
  }
  
  public boolean isAcompteObligatoireDetectionArticleSpeciaux() {
    return acompteObligatoireDetectionArticleSpecial;
  }
  
  public void setAcompteObligatoireArticleSpeciauxPresent(boolean acompteObligatoireDetectionArticleSpecial) {
    this.acompteObligatoireDetectionArticleSpecial = acompteObligatoireDetectionArticleSpecial;
  }
  
  public int getPourcentageAcompteDetectionArticleSpecial() {
    return pourcentageAcompteDetectionArticleSpecial;
  }
  
  public void setPourcentageAcompteDetectionArticleSpecial(int pourcentageAcompteDetectionArticleSpecial) {
    this.pourcentageAcompteDetectionArticleSpecial = pourcentageAcompteDetectionArticleSpecial;
  }
}
