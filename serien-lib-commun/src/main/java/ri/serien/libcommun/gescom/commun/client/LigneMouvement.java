/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

public class LigneMouvement extends AbstractClasseMetier<IdLigneMouvement> {
  
  // Variables
  private Date dateMouvement = null; // date du mouvement
  private Integer numeroOrdre = null; // Numéro d'ordre
  private String erlModificateurPump = null; // ERL Modificateur de PUMP
  private EnumCodeOperationLigneMouvement codeOperation = null; // Code opération
  private BigDecimal quantiteMouvement = null; // Quantité mouvement
  private BigDecimal quantiteStock = null; // Quantité stock
  private BigDecimal prixUnitaire = null; // prix unitaire
  private BigDecimal pump = null; // PUMP
  private BigDecimal prixDeRevient = null; // prix de revient en US
  private BigDecimal prixUnitaireDevise = null; // prix unitaire en devises / US
  private String signeOperation = null; // Signe de l'opération
  private IdMagasin idMagasinRecepteur = null; // Code magasin récepteur transfert
  private Integer numeroOrdreTransfert = null; // Numéro d'ordre transfert
  private EnumOrigineOperationLigneMouvement origineOperation = null; // Origine de l'opération
  private String codeBonOrigine = null; // Code Bon origine
  private Integer numeroBonOrigine = null; // Numéro du bon d'origine
  private Integer suffixeBonOrigine = null; // Suffixe du bon d'origine
  private Integer ligneBonOrigine = null; // Numéro de ligne dans le bon d'origine
  private Integer collectifFournisseur = null; // Collectif si FRS
  private Integer numeroTiers = null; // Numéro de tiers (client ou fournisseur)
  private Integer suffixeLivraison = null; // Livraison si CLI
  private Date datePrecedenteOperation = null; // Date de précédente opération
  private Integer numeroOrdrePrecedenteOperation = null; // Numéro d'ordre de précédente opération
  private Date datePrecedentInventaire = null; // Date de précédent inventaire
  private Integer numeroOrdrePrecedentInventaire = null; // Numéro d'ordre de précédent inventaire
  private BigDecimal quantitePrecedentInventaire = null; // Quantité de précédent inventaire
  private BigDecimal cumulEntrees = null; // Cumul entrées depuis inv.pré
  private BigDecimal cumulSorties = null; // Cumul sorties depuis inv.pré
  private BigDecimal cumulDivers = null; // Cumul divers depuis inv.pré
  private Integer topInventairePurge = null; // Top inventaire purgé
  private Date dateReelleOperation = null; // Date réelle de l'opération
  private Date heureReelleOperation = null; // Heure réelle de l'opération
  private BigDecimal quantiteReelle = null; // Quantité réelle
  private IdVendeur idMagasinier = null;
  private IdClient idClient = null;
  private IdFournisseur idFournisseur = null;
  
  private Integer nombreDecimalesStock = Constantes.DEUX_DECIMALES;
  
  /**
   * Constructeur avec l'idenfiant.
   */
  public LigneMouvement(IdLigneMouvement pIdLigneMouvement) {
    super(pIdLigneMouvement);
  }
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdLigneMouvement controlerId(LigneMouvement pLigne, boolean pVerifierExistance) {
    if (pLigne == null) {
      throw new MessageErreurException("Le mouvement est invalide.");
    }
    return IdLigneMouvement.controlerId(pLigne.getId(), true);
  }
  
  public String getErlModificateurPump() {
    return erlModificateurPump;
  }
  
  public void setErlModificateurPump(String erlModificateurPump) {
    this.erlModificateurPump = erlModificateurPump;
  }
  
  public EnumCodeOperationLigneMouvement getCodeOperation() {
    return codeOperation;
  }
  
  public void setCodeOperation(EnumCodeOperationLigneMouvement codeOperation) {
    this.codeOperation = codeOperation;
  }
  
  public BigDecimal getQuantiteMouvement() {
    return quantiteMouvement;
  }
  
  public void setQuantiteMouvement(BigDecimal pQuantiteMouvement) {
    quantiteMouvement = pQuantiteMouvement;
    if (quantiteMouvement != null && nombreDecimalesStock != null) {
      quantiteMouvement = quantiteMouvement.setScale(nombreDecimalesStock, RoundingMode.HALF_UP);
    }
  }
  
  public BigDecimal getQuantiteStock() {
    return quantiteStock;
  }
  
  public void setQuantiteStock(BigDecimal pQuantiteStock) {
    quantiteStock = pQuantiteStock;
    if (quantiteStock != null && nombreDecimalesStock != null) {
      quantiteStock = quantiteStock.setScale(nombreDecimalesStock, RoundingMode.HALF_UP);
    }
  }
  
  public BigDecimal getPrixUnitaire() {
    return prixUnitaire;
  }
  
  public void setPrixUnitaire(BigDecimal prixUnitaire) {
    this.prixUnitaire = prixUnitaire;
  }
  
  public BigDecimal getPump() {
    return pump;
  }
  
  public void setPump(BigDecimal pump) {
    this.pump = pump;
  }
  
  public BigDecimal getPrixDeRevient() {
    return prixDeRevient;
  }
  
  public void setPrixDeRevient(BigDecimal prixDeRevient) {
    this.prixDeRevient = prixDeRevient;
  }
  
  public BigDecimal getPrixUnitaireDevise() {
    return prixUnitaireDevise;
  }
  
  public void setPrixUnitaireDevise(BigDecimal prixUnitaireDevise) {
    this.prixUnitaireDevise = prixUnitaireDevise;
  }
  
  public String getSigneOperation() {
    return signeOperation;
  }
  
  public void setSigneOperation(String signeOperation) {
    this.signeOperation = signeOperation;
  }
  
  public IdMagasin getIdMagasinRecepteur() {
    return idMagasinRecepteur;
  }
  
  public void setIdMagasinRecepteur(IdMagasin pMagasinRecepteur) {
    this.idMagasinRecepteur = pMagasinRecepteur;
  }
  
  public Integer getNumeroOrdreTransfert() {
    return numeroOrdreTransfert;
  }
  
  public void setNumeroOrdreTransfert(Integer numeroOrdreTransfert) {
    this.numeroOrdreTransfert = numeroOrdreTransfert;
  }
  
  public EnumOrigineOperationLigneMouvement getOrigineOperation() {
    return origineOperation;
  }
  
  public void setOrigineOperation(EnumOrigineOperationLigneMouvement origineOperation) {
    this.origineOperation = origineOperation;
  }
  
  public String getCodeBonOrigine() {
    return codeBonOrigine;
  }
  
  public void setCodeBonOrigine(String codeBonOrigine) {
    this.codeBonOrigine = codeBonOrigine;
  }
  
  public Integer getNumeroBonOrigine() {
    return numeroBonOrigine;
  }
  
  public void setNumeroBonOrigine(Integer numeroBonOrigine) {
    this.numeroBonOrigine = numeroBonOrigine;
  }
  
  public Integer getSuffixeBonOrigine() {
    return suffixeBonOrigine;
  }
  
  public void setSuffixeBonOrigine(Integer suffixeBonOrigine) {
    this.suffixeBonOrigine = suffixeBonOrigine;
  }
  
  public Integer getLigneBonOrigine() {
    return ligneBonOrigine;
  }
  
  public void setLigneBonOrigine(Integer ligneBonOrigine) {
    this.ligneBonOrigine = ligneBonOrigine;
  }
  
  public Integer getCollectifFournisseur() {
    return collectifFournisseur;
  }
  
  public void setCollectifFournisseur(Integer collectifFournisseur) {
    this.collectifFournisseur = collectifFournisseur;
  }
  
  public Integer getNumeroTiers() {
    return numeroTiers;
  }
  
  public void setNumeroTiers(Integer numeroTiers) {
    this.numeroTiers = numeroTiers;
  }
  
  public Integer getSuffixeLivraison() {
    return suffixeLivraison;
  }
  
  public void setSuffixeLivraison(int suffixeLivraison) {
    this.suffixeLivraison = suffixeLivraison;
  }
  
  public Date getDatePrecedenteOperation() {
    return datePrecedenteOperation;
  }
  
  public void setDatePrecedenteOperation(Date datePrecedenteOperation) {
    this.datePrecedenteOperation = datePrecedenteOperation;
  }
  
  public Integer getNumeroOrdrePrecedenteOperation() {
    return numeroOrdrePrecedenteOperation;
  }
  
  public void setNumeroOrdrePrecedenteOperation(Integer numeroOrdrePrecedenteOperation) {
    this.numeroOrdrePrecedenteOperation = numeroOrdrePrecedenteOperation;
  }
  
  public Date getDatePrecedentInventaire() {
    return datePrecedentInventaire;
  }
  
  public void setDatePrecedentInventaire(Date datePrecedentInventaire) {
    this.datePrecedentInventaire = datePrecedentInventaire;
  }
  
  public Integer getNumeroOrdrePrecedentInventaire() {
    return numeroOrdrePrecedentInventaire;
  }
  
  public void setNumeroOrdrePrecedentInventaire(Integer numeroOrdrePrecedentInventaire) {
    this.numeroOrdrePrecedentInventaire = numeroOrdrePrecedentInventaire;
  }
  
  public BigDecimal getQuantitePrecedentInventaire() {
    return quantitePrecedentInventaire;
  }
  
  public void setQuantitePrecedentInventaire(BigDecimal quantitePrecedentInventaire) {
    this.quantitePrecedentInventaire = quantitePrecedentInventaire;
  }
  
  public BigDecimal getCumulEntrees() {
    return cumulEntrees;
  }
  
  public void setCumulEntrees(BigDecimal cumulEntrees) {
    this.cumulEntrees = cumulEntrees;
  }
  
  public BigDecimal getCumulSorties() {
    return cumulSorties;
  }
  
  public void setCumulSorties(BigDecimal cumulSorties) {
    this.cumulSorties = cumulSorties;
  }
  
  public BigDecimal getCumulDivers() {
    return cumulDivers;
  }
  
  public void setCumulDivers(BigDecimal cumulDivers) {
    this.cumulDivers = cumulDivers;
  }
  
  public Integer getTopInventairePurge() {
    return topInventairePurge;
  }
  
  public void setTopInventairePurge(Integer topInventairePurge) {
    this.topInventairePurge = topInventairePurge;
  }
  
  public Date getDateReelleOperation() {
    return dateReelleOperation;
  }
  
  public void setDateReelleOperation(Date dateReelleOperation) {
    this.dateReelleOperation = dateReelleOperation;
  }
  
  public Date getHeureReelleOperation() {
    return heureReelleOperation;
  }
  
  public void setHeureReelleOperation(Date heureReelleOperation) {
    this.heureReelleOperation = heureReelleOperation;
  }
  
  public BigDecimal getQuantiteReelle() {
    return quantiteReelle;
  }
  
  public void setQuantiteReelle(BigDecimal quantiteReelle) {
    this.quantiteReelle = quantiteReelle;
  }
  
  public void setNombreDecimalesStock(Integer pNombreDecimalesStock) {
    nombreDecimalesStock = pNombreDecimalesStock;
  }
  
  /**
   * Identifiant du magasinier
   */
  public IdVendeur getIdMagasinier() {
    return idMagasinier;
  }
  
  /**
   * Identifiant du magasinier
   */
  public void setIdMagasinier(IdVendeur idMagasinier) {
    this.idMagasinier = idMagasinier;
  }
  
  public IdClient getIdClient() {
    return idClient;
  }
  
  public void setIdClient(IdClient idClient) {
    this.idClient = idClient;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
}
