/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.reglement;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.outils.Constantes;

/**
 * Mode de règlement paramétré dans la personnalisation RG.
 * 
 * Un mode de règlement définit un moyen de paiement : en espèce, par CB, par chèque, par virement...
 */
public class ModeReglement extends AbstractClasseMetier<IdModeReglement> {
  // Variables
  private String libelle = "";
  private EnumTypeReglement typeReglement = null;
  private EnumSensReglement sensReglement = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public ModeReglement(IdModeReglement pIdModeReglement) {
    super(pIdModeReglement);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  // -- Méthodes publiques
  
  /**
   * Indiquer si le mode de règlement est un chèque.
   * Pour l'instant, on utilise le code de l'identifiant. A terme, il faudra utiliser le type de règlement du mode de règlement.
   */
  public boolean isCheque() {
    return Constantes.equals(typeReglement, EnumTypeReglement.CHEQUE);
  }
  
  /**
   * Indiquer si le mode de règlement est une carte bancaire.
   * Pour l'instant, on utilise le code de l'identifiant. A terme, il faudra utiliser le type de règlement du mode de règlement.
   */
  public boolean isCarteBancaire() {
    return Constantes.equals(typeReglement, EnumTypeReglement.CARTE);
  }
  
  /**
   * Indiquer si le mode de règlement correspond à des espèces.
   * Pour l'instant, on utilise le code de l'identifiant. A terme, il faudra utiliser le type de règlement du mode de règlement.
   */
  public boolean isEspece() {
    return Constantes.equals(typeReglement, EnumTypeReglement.ESPECES);
  }
  
  /**
   * Indiquer si le mode de règlement est un acompte.
   * Pour l'instant, on utilise le code de l'identifiant. A terme, il faudra utiliser le type de règlement du mode de règlement.
   */
  public boolean isAcompte() {
    return getId().equals(IdModeReglement.ACOMPTE);
  }
  
  /**
   * Indiquer si le mode de règlement est un virement.
   * Pour l'instant, on utilise le code de l'identifiant. A terme, il faudra utiliser le type de règlement du mode de règlement.
   */
  public boolean isVirement() {
    return Constantes.equals(typeReglement, EnumTypeReglement.VIREMENT);
  }
  
  /**
   * Indiquer si le mode de règlement est un avoir.
   * Pour l'instant, on utilise le code de l'identifiant. A terme, il faudra utiliser le type de règlement du mode de règlement.
   */
  public boolean isAvoir() {
    return getId().equals(IdModeReglement.AVOIR);
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public ModeReglement clone() {
    ModeReglement o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (ModeReglement) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  /**
   * Libellé du mode de réglement.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé du mode de règlement.
   * Les espaces avant et après sont enlevés.
   */
  public void setLibelle(String pLibelle) {
    if (pLibelle != null) {
      libelle = pLibelle.trim();
    }
    else {
      libelle = "";
    }
  }
  
  public EnumTypeReglement getTypeReglement() {
    return typeReglement;
  }
  
  public void setTypeReglement(EnumTypeReglement typeReglement) {
    this.typeReglement = typeReglement;
  }
  
  public EnumSensReglement getSensReglement() {
    return sensReglement;
  }
  
  public void setSensReglement(EnumSensReglement sensReglement) {
    this.sensReglement = sensReglement;
  }
  
}
