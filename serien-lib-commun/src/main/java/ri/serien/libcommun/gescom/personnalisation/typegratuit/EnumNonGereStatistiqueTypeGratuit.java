/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typegratuit;

import ri.serien.libcommun.outils.MessageErreurException;

/*
 * Enum encadrant le champ "non géré en statistiques" dans les types de gratuits
 */
public enum EnumNonGereStatistiqueTypeGratuit {
  
  AUCUN(' ', "Aucun"),
  PARTICPE_PAS('1', "Ne participe pas aux stats(Toutes)"),
  PARTICPE_PAS_EXCEPTE('2', "Ne participe pas aux stats(Excepté stats/critères de type gratuit)");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumNonGereStatistiqueTypeGratuit(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumNonGereStatistiqueTypeGratuit valueOfByCode(Character pCode) {
    for (EnumNonGereStatistiqueTypeGratuit value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException(
        "Dans un paramètre type de gratuit (TG), le champ 'non géré en statistiques' est invalide : " + pCode);
  }
}
