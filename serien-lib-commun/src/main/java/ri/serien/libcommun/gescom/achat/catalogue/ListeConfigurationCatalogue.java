/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import java.util.ArrayList;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.CriteresRechercheAcheteur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;

/**
 * Liste de configurations.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de configurations.
 * La méthode chargerTout permet d'obtenir la liste de toutes les configurations d'un établissement.
 */
public class ListeConfigurationCatalogue extends ArrayList<ConfigurationCatalogue> {
  /**
   * Constructeur.
   */
  public ListeConfigurationCatalogue() {
  }
  
  /**
   * Charger tous les configurations de l'établissement donné et des critères transmis.
   */
  public static ListeConfigurationCatalogue charger(IdSession pIdSession, IdEtablissement pIdEtablissement,
      IdFournisseur pIdFournisseur) {
    CriteresRechercheCfgCatalogue criteres = new CriteresRechercheCfgCatalogue();
    criteres.setTypeRecherche(CriteresRechercheAcheteur.TOUT_RETOURNER_POUR_UN_ETABLISSEMENT);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    if (pIdFournisseur != null) {
      criteres.setIdFournisseur(pIdFournisseur);
    }
    return ManagerServiceFournisseur.chargerListeConfigsCatalogue(pIdSession, criteres);
  }
  
  /**
   * Retourner l'index de la configuration transmise en paramètre
   */
  public int retournerIndexConfigurationCatalogue(ConfigurationCatalogue pConfiguration) {
    
    int indexTrouve = -1;
    int indexListe = 0;
    // On récupère l'id de la configuration en cours
    for (ConfigurationCatalogue configuration : this) {
      if (Constantes.equals(configuration.getId(), pConfiguration.getId())) {
        indexTrouve = indexListe;
        break;
      }
      indexListe++;
    }
    
    return indexTrouve;
  }
  
  /**
   * Retourner la configuration catalogue précédente dans la liste des configurations
   */
  public ConfigurationCatalogue retournerConfigurationPrecedente(ConfigurationCatalogue pConfiguration) {
    if (this.size() == 0) {
      return null;
    }
    
    ConfigurationCatalogue configurationCatalogue = null;
    int indexConfigEnCours = retournerIndexConfigurationCatalogue(pConfiguration);
    int indexConfigPrecedente = -1;
    
    if (indexConfigEnCours == -1) {
      return null;
    }
    
    if (indexConfigEnCours == 0) {
      indexConfigPrecedente = 0;
    }
    else {
      indexConfigPrecedente = indexConfigEnCours - 1;
    }
    
    configurationCatalogue = this.get(indexConfigPrecedente);
    return configurationCatalogue;
  }
  
  /**
   * Retourner la configuration catalogue suivante dans la liste des configurations
   */
  public ConfigurationCatalogue retournerConfigurationSuivante(ConfigurationCatalogue pConfiguration) {
    if (this.size() == 0) {
      return null;
    }
    
    ConfigurationCatalogue configurationCatalogue = null;
    int indexConfigEnCours = retournerIndexConfigurationCatalogue(pConfiguration);
    int indexConfigSuivante = -1;
    
    if (indexConfigEnCours == -1) {
      return null;
    }
    
    if (indexConfigEnCours >= this.size() - 1) {
      indexConfigSuivante = this.size() - 1;
    }
    else {
      indexConfigSuivante = indexConfigEnCours + 1;
    }
    
    configurationCatalogue = this.get(indexConfigSuivante);
    return configurationCatalogue;
  }
}
