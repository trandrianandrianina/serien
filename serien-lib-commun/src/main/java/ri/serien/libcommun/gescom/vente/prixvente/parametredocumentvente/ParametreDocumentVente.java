/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.prixvente.MapQuantiteRattachement;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Classe regroupant tous les paramètres du document de vente pour le calcul du prix de vente.
 */
public class ParametreDocumentVente implements Serializable {
  // Variables entête
  private IdDocumentVente idDocumentVente = null;
  private IdClient idClientLivre = null;
  private IdClient idClientFacture = null;
  private String codeConditionVente = null;
  private Integer etat = null;
  private Boolean prixGaranti = null;
  private Integer numeroClientCentrale1 = null;
  
  // Attributs pour FormulePrixVente
  private boolean modeTTC = false;
  private BigPercentage tauxTVA1 = BigPercentage.ZERO;
  private BigPercentage tauxTVA2 = BigPercentage.ZERO;
  private BigPercentage tauxTVA3 = BigPercentage.ZERO;
  private Integer numeroColonneTarif = null;
  private BigPercentage tauxRemise1 = null;
  private BigPercentage tauxRemise2 = null;
  private BigPercentage tauxRemise3 = null;
  private BigPercentage tauxRemise4 = null;
  private BigPercentage tauxRemise5 = null;
  private BigPercentage tauxRemise6 = null;
  private EnumModeTauxRemise modeTauxRemise = null;
  private EnumAssietteTauxRemise assietteTauxRemise = null;
  
  // A trier
  private Date dateCreation = null;
  private String codeDevise = null;
  // Numéro de colonne de la DG
  private Character typeFacturation = null;
  // Map contenant le cumul des quantités des différents rattachements uniquement si des conditions quantitatives sont présentes
  private MapQuantiteRattachement mapQuantiteParRattachement = null;
  private Integer numeroChantier = null;
  
  // -- Méthodes publiques
  
  /**
   * Retourner l'identifiant du chantier.
   * 
   * @param pIdEtablissement
   * @return
   */
  public IdChantier retournerIdChantier(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null || numeroChantier == null || numeroChantier.intValue() <= 0) {
      return null;
    }
    
    return IdChantier.getInstance(pIdEtablissement, numeroChantier);
  }
  
  // -- Accesseurs
  
  /**
   * Retourner l'identifiant du document de vente.
   * @return Identifiant du document de vente.
   */
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  /**
   * Modifier l'identifiant du document de vente.
   * @param pIdDocumentVente Identifiant du document de vente.
   */
  public void setIdDocumentVente(IdDocumentVente pIdDocumentVente) {
    idDocumentVente = pIdDocumentVente;
  }
  
  /**
   * Retourner l'identifiant du client facturé du document de vente.
   * @return Identifiant client.
   */
  public IdClient getIdClientFacture() {
    return idClientFacture;
  }
  
  /**
   * Modifier l'identifiant du client facturé du document de vente.
   * @param pIdClientFacture Identifiant client.
   */
  public void setIdClientFacture(IdClient pIdClientFacture) {
    idClientFacture = pIdClientFacture;
  }
  
  /**
   * Retourner l'identifiant du client livré du document de vente.
   * @return Identifiant client livré.
   */
  public IdClient getIdClientLivre() {
    return idClientLivre;
  }
  
  /**
   * Modifier l'identifiant du client livré du document de vente.
   * @param pIdClientLivre Identifiant client livré.
   */
  public void setIdClientLivre(IdClient pIdClientLivre) {
    idClientLivre = pIdClientLivre;
  }
  
  /**
   * Retourner le code de la condition de vente sous forme de chaine de caractères.
   * 
   * @return
   */
  public String getCodeConditionVente() {
    return codeConditionVente;
  }
  
  /**
   * Initialise le code de la condition de vente sous forme de chaine de caractères.
   * 
   * @param pCodeCNV
   */
  public void setCodeConditionVente(String pCodeCNV) {
    this.codeConditionVente = pCodeCNV;
  }
  
  /**
   * Retourner l'état du document.
   * 
   * @return
   */
  public Integer getEtat() {
    return etat;
  }
  
  /**
   * Initialise l'état du document.
   * 
   * @param pEtat
   */
  public void setEtat(Integer pEtat) {
    this.etat = pEtat;
  }
  
  /**
   * Retourner le numéro client de la centrale 1.
   * @return Le numéro client de la centrale 1.
   */
  public Integer getNumeroClientCentrale1() {
    return numeroClientCentrale1;
  }
  
  /**
   * Modifier le numéro client de la centrale 1.
   * @param pNumeroClientCentrale1 le numéro client de la centrale 1.
   */
  public void setNumeroClientCentrale1(Integer pNumeroClientCentrale1) {
    numeroClientCentrale1 = pNumeroClientCentrale1;
  }
  
  /**
   * Indiquer si les prix du document de vente sont des prix garantis.
   * 
   * Si le prix est garanti, il faut calculer le prix uniquement avec les données de la ligne. Noter que l'indicateur prix garanti
   * est présent dans l'entête et dans les lignes du document de vente. Si l'un des deux indicateurs est présent, soit dans l'entête,
   * soit dans la ligne, cela signifie que le prix est garanti.
   * 
   * @return true=prix garanti, false=prix non garanti.
   */
  public boolean isPrixGaranti() {
    return prixGaranti != null && prixGaranti;
  }
  
  /**
   * Renseigner si les prix du document de vente sont des prix garantis.
   * @param pPrixGaranti true=prix garanti, false=prix non garanti
   */
  public void setPrixGaranti(Boolean pPrixGaranti) {
    prixGaranti = pPrixGaranti;
  }
  
  /**
   * Renseigner si les prix du document de vente sont des prix garantis (à partir du champ E1TO1G).
   * 
   * L'information prix garanti est stockée dans le champ E1TO1G. Le champ E1TO1G est une zone numérique de 9 chiffres (par
   * exemple "000000010") dont chaque chiffre correspondant à un indicateur indépendant. Le 8ème chiffre à partir de la gauche permet
   * de savoir si le document de vente a des prix garanti ('1' dans l'exemple).
   * 
   * Les valeurs possibles sont :
   * - 0 = prix non garanti
   * - 1 = prix garanti
   * - 2 = ancien prix
   * - 3 = prix garanti au devis
   * Pour l'instant, on gère juste la valeur 1 car le rôle des autres valeurs n'est pas clair.
   * 
   * @param pIndicateurE1TO1G Indicateur E1TO1G.
   */
  public void setPrixGarantiE1TO1G(BigDecimal pIndicateurE1TO1G) {
    if (pIndicateurE1TO1G != null) {
      String indicateur = pIndicateurE1TO1G.toString();
      if (indicateur.length() >= 2 && indicateur.charAt(indicateur.length() - 2) == '1') {
        prixGaranti = true;
      }
    }
  }
  
  /**
   * Retourner le numéro de la colonne tarif de l'entête du document de vente.
   * 
   * Les valeurs 1 à 10 désignent les colonnes tarifs 1 à 10. La valeur 0 signifie qu'il n'y aucune colonne tarif. La valeur null
   * signifie que l'information n'a pas été renseignée.
   * 
   * @return Numéro de la colonne tarif (0=aucune, 1-10=colonne tarif, null=non renseigné).
   */
  public Integer getNumeroColonneTarif() {
    return numeroColonneTarif;
  }
  
  /**
   * Modifier le numéro de la colonne tarif de l'entête du document de vente.
   * @param pNumeroColonneTarif Numéro de colonne tarif.
   */
  public void setNumeroColonneTarif(Integer pNumeroColonneTarif) {
    numeroColonneTarif = pNumeroColonneTarif;
  }
  
  /**
   * Indiquer si au moins un taux de remise est présent.
   * @return true=il y a au moins un taux de remise, false=sinon.
   */
  public boolean isTauxRemisePresent() {
    return tauxRemise1 != null || tauxRemise2 != null || tauxRemise3 != null || tauxRemise4 != null || tauxRemise5 != null
        || tauxRemise6 != null;
  }
  
  /**
   * Retourner le taux de remise, dont l'indice est mentionné en paramètre, de la ligne de vente.
   * @param pIndex Indice du taux de remise à modifier (entre 1 et 6).
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise(int pIndex) {
    if (pIndex == 1) {
      return getTauxRemise1();
    }
    else if (pIndex == 2) {
      return getTauxRemise2();
    }
    else if (pIndex == 3) {
      return getTauxRemise3();
    }
    else if (pIndex == 4) {
      return getTauxRemise4();
    }
    else if (pIndex == 5) {
      return getTauxRemise5();
    }
    else if (pIndex == 6) {
      return getTauxRemise6();
    }
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise, dont l'indice est mentionné en paramètre, de la ligne de vente.
   * @param pTauxRemise Taux de remise.
   * @param pIndex Indice du taux de remise à modifier.
   */
  public void setTauxRemise(BigPercentage pTauxRemise, int pIndex) {
    if (pIndex == 1) {
      setTauxRemise1(pTauxRemise);
    }
    else if (pIndex == 2) {
      setTauxRemise2(pTauxRemise);
    }
    else if (pIndex == 3) {
      setTauxRemise3(pTauxRemise);
    }
    else if (pIndex == 4) {
      setTauxRemise4(pTauxRemise);
    }
    else if (pIndex == 5) {
      setTauxRemise5(pTauxRemise);
    }
    else if (pIndex == 6) {
      setTauxRemise6(pTauxRemise);
    }
  }
  
  /**
   * Retourner le taux de remise 1 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise1() {
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise 1 de la ligne de vente.
   * @param pTauxRemise1 Taux de remise.
   */
  public void setTauxRemise1(BigPercentage pTauxRemise1) {
    tauxRemise1 = pTauxRemise1;
  }
  
  /**
   * Retourner le taux de remise 1 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise2() {
    return tauxRemise2;
  }
  
  /**
   * Modifier le taux de remise 2 de la ligne de vente.
   * @param pTauxRemise2 Taux de remise.
   */
  public void setTauxRemise2(BigPercentage pTauxRemise2) {
    tauxRemise2 = pTauxRemise2;
  }
  
  /**
   * Retourner le taux de remise 3 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise3() {
    return tauxRemise3;
  }
  
  /**
   * Modifier le taux de remise 3 de la ligne de vente.
   * @param pTauxRemise3 Taux de remise.
   */
  public void setTauxRemise3(BigPercentage pTauxRemise3) {
    tauxRemise3 = pTauxRemise3;
  }
  
  /**
   * Retourner le taux de remise 4 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise4() {
    return tauxRemise4;
  }
  
  /**
   * Modifier le taux de remise 4 de la ligne de vente.
   * @param pTauxRemise4 Taux de remise.
   */
  public void setTauxRemise4(BigPercentage pTauxRemise4) {
    tauxRemise4 = pTauxRemise4;
  }
  
  /**
   * Retourner le taux de remise 5 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise5() {
    return tauxRemise5;
  }
  
  /**
   * Modifier le taux de remise 5 de la ligne de vente.
   * @param pTauxRemise5 Taux de remise.
   */
  public void setTauxRemise5(BigPercentage pTauxRemise5) {
    tauxRemise5 = pTauxRemise5;
  }
  
  /**
   * Retourner le taux de remise 6 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise6() {
    return tauxRemise6;
  }
  
  /**
   * Modifier le taux de remise 6 de la ligne de vente.
   * @param pTauxRemise6 Taux de remise.
   */
  public void setTauxRemise6(BigPercentage pTauxRemise6) {
    tauxRemise6 = pTauxRemise6;
  }
  
  /**
   * Retourner le mode d'application des Taux de remises de la ligne de vente.
   * @return Mode d'application des taux de remises.
   */
  public EnumModeTauxRemise getModeTauxRemise() {
    return modeTauxRemise;
  }
  
  /**
   * Modifier le mode d'application des taux de remises de la ligne de vente.
   * @param pModeTauxRemise Mode d'application des taux de remises.
   */
  public void setModeTauxRemise(EnumModeTauxRemise pModeTauxRemise) {
    modeTauxRemise = pModeTauxRemise;
  }
  
  /**
   * Retourner l'assiette sur laquelle les taux de remises s'appliquent.
   * @return Assiette taux de remises (prix untaire ou montant ligne).
   */
  public EnumAssietteTauxRemise getAssietteTauxRemise() {
    return assietteTauxRemise;
  }
  
  /**
   * Modifier l'assiette sur laquelle les taux de remises s'appliquent.
   * @param pAssietteTauxRemise Assiette taux de remises (prix untaire ou montant ligne).
   */
  public void setAssietteTauxRemise(EnumAssietteTauxRemise pAssietteTauxRemise) {
    assietteTauxRemise = pAssietteTauxRemise;
  }
  
  /**
   * Initialise la date de création.
   * 
   * @return
   */
  public Date getDateCreation() {
    return dateCreation;
  }
  
  /**
   * Retourner la date de création.
   * 
   * @param pDateCreation
   */
  public void setDateCreation(Date pDateCreation) {
    this.dateCreation = pDateCreation;
  }
  
  /**
   * Retourner le code de la devise.
   * 
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Initialise le code de la devise.
   * 
   * @param codeDevise
   */
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  /**
   * Indiquer si le client est facturé en HT ou TTC.
   * @return true=TTC, false=HT.
   */
  public boolean isModeTTC() {
    return modeTTC;
  }
  
  /**
   * Modifier si le client est facturé en HT ou TTC.
   * @param pModeTTC true=TTC, false=HT.
   */
  public void setModeTTC(boolean pModeTTC) {
    modeTTC = pModeTTC;
  }
  
  /**
   * Retourner le taux de TVA correspondant au numéro de TVA fourni.
   * @param numeroTVA Numéro de TVA.
   * @return Taux de TVA (null si pas trouve).
   */
  public BigPercentage getTauxTVA(int pNumeroTVA) {
    switch (pNumeroTVA) {
      case 1:
        return getTauxTVA1();
      case 2:
        return getTauxTVA2();
      case 3:
        return getTauxTVA3();
      default:
        Trace.alerte("Le numéro de TVA des paramètres document de vente est invalide : " + pNumeroTVA);
        return null;
    }
  }
  
  /**
   * Modifier le taux de TVA correspondant au numéro de TVA fourni.
   * @param pTauxTVA Taux de TVA (null si exempté).
   * @param numeroTVA Numéro de TVA.
   */
  public void setTauxTVA(BigPercentage pTauxTVA, int pNumeroTVA) {
    switch (pNumeroTVA) {
      case 1:
        setTauxTVA1(pTauxTVA);
        break;
      case 2:
        setTauxTVA2(pTauxTVA);
        break;
      case 3:
        setTauxTVA3(pTauxTVA);
        break;
      default:
        Trace.alerte("Le numéro de TVA des paramètres document de vente est invalide : " + pNumeroTVA);
        break;
    }
  }
  
  /**
   * Retourner le taux de TVA 1.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA1() {
    return tauxTVA1;
  }
  
  /**
   * Modifier le taux de TVA 1.
   * @param pTauxTVA Taux de TVA.
   */
  public void setTauxTVA1(BigPercentage pTauxTVA) {
    tauxTVA1 = pTauxTVA;
  }
  
  /**
   * Retourner le taux de TVA 2.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA2() {
    return tauxTVA2;
  }
  
  /**
   * Modifier le taux de TVA 2.
   * @param pTauxTVA1 Taux de TVA.
   */
  public void setTauxTVA2(BigPercentage pTauxTVA) {
    tauxTVA2 = pTauxTVA;
  }
  
  /**
   * Retourner le taux de TVA 3.
   * @return Taux de TVA.
   */
  public BigPercentage getTauxTVA3() {
    return tauxTVA3;
  }
  
  /**
   * Modifier le taux de TVA 3.
   * @param pTauxTVA1 Taux de TVA.
   */
  public void setTauxTVA3(BigPercentage pTauxTVA) {
    tauxTVA3 = pTauxTVA;
  }
  
  /**
   * Retourner le type de facturation.
   * 
   * @return
   */
  public Character getTypeFacturation() {
    return typeFacturation;
  }
  
  /**
   * Initialise le type de facturation.
   * 
   * @param pTypeFacturation
   */
  public void setTypeFacturation(Character pTypeFacturation) {
    this.typeFacturation = pTypeFacturation;
  }
  
  /**
   * Retourner un clone de la map contenant les informations des articles des lignes de ventes pour les conditions quantitatives.
   * 
   * @return
   */
  public MapQuantiteRattachement getCloneMapQuantiteParRattachement() {
    if (mapQuantiteParRattachement == null) {
      return null;
    }
    return mapQuantiteParRattachement.clone();
  }
  
  /**
   * Retourner la map contenant les informations des articles des lignes de ventes pour les conditions quantitatives.
   * 
   * @return
   */
  public MapQuantiteRattachement getMapQuantiteParRattachement() {
    return mapQuantiteParRattachement;
  }
  
  /**
   * Initialise la map contenant les informations des articles des lignes de ventes pour les conditions quantitatives.
   * 
   * @param pMapQuantiteParRattachement
   */
  public void setMapQuantiteParRattachement(MapQuantiteRattachement pMapQuantiteParRattachement) {
    this.mapQuantiteParRattachement = pMapQuantiteParRattachement;
  }
  
  /**
   * Retourner le numéro chantier.
   * 
   * @return
   */
  public Integer getNumeroChantier() {
    return numeroChantier;
  }
  
  /**
   * Initialise le numéro chantier.
   * 
   * @param pNumeroChantier
   */
  public void setNumeroChantier(Integer pNumeroChantier) {
    this.numeroChantier = pNumeroChantier;
  }
  
}
