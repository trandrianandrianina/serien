/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste de LigneVente.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de lignes de vente.
 */
public class ListeLigneVente extends ListeClasseMetier<IdLigneVente, LigneVente, ListeLigneVente> {
  
  /**
   * Constructeur.
   */
  public ListeLigneVente() {
  }
  
  public ListeLigneVente chargerTout(IdSession pIdSession, DocumentVente pDocumentVente) {
    CritereLigneVente criteres = new CritereLigneVente();
    
    return ManagerServiceDocumentVente.chargerListeLigneVente(pIdSession, pDocumentVente.getId(), criteres, pDocumentVente.isTTC());
  }
  
  @Override
  public ListeLigneVente charger(IdSession pIdSession, List<IdLigneVente> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  @Override
  public ListeLigneVente charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public ListeLigneVente clone() {
    ListeLigneVente listeLigneVente = new ListeLigneVente();
    // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
    for (LigneVente ligneVente : this) {
      listeLigneVente.add(ligneVente.clone());
    }
    
    return listeLigneVente;
  }
  
  /**
   * Retourner une LigneVente de la liste à partir de son identifiant.
   */
  public LigneVente retournerLigneVenteParId(IdLigneVente pIdLigneVente) {
    if (pIdLigneVente == null) {
      return null;
    }
    for (LigneVente ligneVente : this) {
      if (ligneVente != null && Constantes.equals(ligneVente.getId(), pIdLigneVente)) {
        return ligneVente;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner une LigneVente de la liste à partir de l'ID de l'article qu'elle contient.
   */
  public LigneVente retournerLigneVenteParArticle(IdArticle pIdArticle) {
    if (pIdArticle == null) {
      return null;
    }
    for (LigneVente ligneVente : this) {
      if (ligneVente != null && Constantes.equals(ligneVente.getIdArticle(), pIdArticle)) {
        return ligneVente;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un LigneVente est présent dans la liste.
   */
  public boolean isPresent(IdLigneVente pIdLigneVente) {
    return retournerLigneVenteParId(pIdLigneVente) != null;
  }
  
  /**
   * Contrôle si la liste contient au moins un article spécial.
   */
  public boolean isContientArticleSpecial() {
    for (LigneVente ligneVente : this) {
      if (ligneVente != null && ligneVente.isLigneArticleSpecial()) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Contrôle la ligne afin qu'elle contienne au moins un article "physique" (standard, spécial, palette).
   */
  public boolean isContientArticle() {
    for (LigneVente ligneVente : this) {
      if (ligneVente == null) {
        continue;
      }
      if (ligneVente.isLigneArticleStandard() || ligneVente.isLigneArticleSpecial() || ligneVente.isLignePalette()) {
        return true;
      }
    }
    return false;
  }
}
