/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.boncour;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un carnet de bons de cour
 * 
 * Les carnets de bons de cour sont la représentation informatique de carnets papiers. Ces carnets contiennent des bons de cour numérotés.
 * Ces carnets sont eux-mêmes numérotés de manière unique et attribués à un et un seul magasinier. Une copie carbone des bons de cour est
 * conservée dans le carnet de bons de cour. Cela permet de garder une traçabilité sur les marchandises remises directement à un client au
 * parc matériaux.
 *
 * Pour plus de précisions, voir la COM-965 pour le client Allot.
 *
 * L'identifiant est composé du code établissement, du code magasin et du numéro de carnet.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdCarnetBonCour extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_NUMERO = 8;
  
  // Variables
  private final IdMagasin idMagasin;
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdCarnetBonCour(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin,
      Integer pNumero) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    idMagasin = IdMagasin.controlerId(pIdMagasin, true);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   * Le numéro et n'est pas à fournir. Il est renseigné avec la valeur 0 dans ce cas.
   */
  private IdCarnetBonCour(IdEtablissement pIdEtablissement, IdMagasin pIdMagasin) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    idMagasin = IdMagasin.controlerId(pIdMagasin, true);
    numero = 0;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdCarnetBonCour getInstance(IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, Integer pNumero) {
    return new IdCarnetBonCour(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pIdMagasin, pNumero);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdCarnetBonCour getInstanceAvecCreationId(IdEtablissement pIdEtablissement, IdMagasin pIdMagasin) {
    return new IdCarnetBonCour(pIdEtablissement, pIdMagasin);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données mais à partir de ses informations sous forme éclatée.
   * Cas particuliers de cet identifiant qui est renseigné et non déduit.
   */
  public static IdCarnetBonCour getInstanceAvecCreationId(IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, Integer pNumero) {
    return new IdCarnetBonCour(EnumEtatObjetMetier.CREE, pIdEtablissement, pIdMagasin, pNumero);
  }
  
  /**
   * Contrôler la validité du numéro de carnet.
   * Il doit être supérieur à zéro et doit comporter au maximum 8 chiffres (entre 1 et 99 999 999).
   */
  private Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du carnet de bons de cour n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du carnet de bons de cour est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du carnet de bons de cour est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdCarnetBonCour controlerId(IdCarnetBonCour pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du carnet de bons de cour est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException(
          "L'identifiant du carnet de bons de cour n'existe pas dans la base de données : " + pId.toString());
    }
    return pId;
  }
  
  // -- Méthodes publiques
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getCodeEtablissement().hashCode();
    code = 37 * code + getIdMagasin().hashCode();
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdCarnetBonCour)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de carnets de bons de cour .");
    }
    IdCarnetBonCour id = (IdCarnetBonCour) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && getIdMagasin().equals(id.getIdMagasin())
        && numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdCarnetBonCour)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdCarnetBonCour id = (IdCarnetBonCour) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = getIdMagasin().compareTo(id.getIdMagasin());
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return "" + numero;
  }
  
  // -- Accesseurs
  
  /**
   * Identifiant du magasin du carnet de bons de cour.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Numéro du carnet de bons de cour.
   * Le numéro du carnet de bons de cour est compris entre entre 1 et 99 999 999.
   */
  public Integer getNumero() {
    return numero;
  }
}
