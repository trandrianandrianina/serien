/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste l'ensemble des types d'unités de vente possibles pour les conditions de ventes quantitatives.
 */
public enum EnumTypeUniteConditionQuantitative {
  UNITE_VENTE('V', "En unité de vente"),
  UNITE_CONDITIONNEMENT('C', "En unité de conditionnement"),
  MULTIPLIE_PAR_100('X', "Multipliée par 100"),
  AU_COL('1', "Au col"),
  A_LA_TONNE('Y', "A la tonne"),
  AU_CUMUL_PALETTE('P', "Au cumul palette");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeUniteConditionQuantitative(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  static public EnumTypeUniteConditionQuantitative valueOfByCode(Character pCode) {
    for (EnumTypeUniteConditionQuantitative value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de l'unité de vente pour les conditions de ventes quantitatives est inconnu : " + pCode);
  }
}
