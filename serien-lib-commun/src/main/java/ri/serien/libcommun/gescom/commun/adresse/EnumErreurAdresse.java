/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.adresse;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'erreur sur un bloc adresse client.
 */
public enum EnumErreurAdresse {
  ERREUR_NON_GEREE(1, "Ce client est invalide et ne peut pas être utilisé.\nMerci de contacter le service d'assistance."),
  ERREUR_CIVILITE(2, "La civilité est obligatoire dans les coordonnées du client.\nMerci de sélectionner une civilité valide."),
  ERREUR_NOM(3, "Le nom est obligatoire dans les coordonnées du client.\nMerci de compléter cette information."),
  ERREUR_PRENOM(4, "Le prénom est obligatoire dans les coordonnées du client.\nMerci de compléter cette information."),
  ERREUR_LOCALISATION(5, "La localisation est obligatoire dans les coordonnées du client.\nMerci de compléter cette information."),
  ERREUR_RUE(6, "La rue est obligatoire dans les coordonnées du client.\nMerci de compléter cette information."),
  ERREUR_CODE_POSTAL(7, "Le code postal est obligatoire dans les coordonnées du client.\nMerci de compléter cette information."),
  ERREUR_VILLE_MANQUANTE(8, "La ville est obligatoire dans les coordonnées du client.\nMerci de compléter cette information."),
  ERREUR_VILLE_ERRONEE(9, "Aucune ville ne correspond à ce code postal.\nMerci de modifier le code postal et/ou la ville."),
  ERREUR_CONTACT(10, "Le nom du contact est obligatoire dans les coordonnées du client.\nMerci de compléter cette information."),
  ERREUR_TELEPHONE(11, "Le numéro de téléphone est obligatoire dans les coordonnées du client.\nMerci de compléter cette information."),
  ERREUR_FAX(12, "Le numéro de fax est obligatoire dans les coordonnées du client.\nMerci de compléter cette information."),
  ERREUR_MAIL(13, "Le mail est obligatoire dans les coordonnées du client.\nMerci de compléter cette information.");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumErreurAdresse(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le code converti en caractère.
   */
  public Character getConvertirCodeEnCaractere() {
    return Character.forDigit(code.intValue(), 10);
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return code.toString();
  }
  
  /**
   * Retourner l'objet enum par son numéro.
   */
  static public EnumErreurAdresse valueOfByCode(Integer pCode) {
    for (EnumErreurAdresse value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de valeur est inconnu : " + pCode);
  }
  
}
