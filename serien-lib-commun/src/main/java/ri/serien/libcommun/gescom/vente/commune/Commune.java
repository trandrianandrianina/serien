/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.commune;

import java.io.Serializable;
import java.text.Normalizer;

import ri.serien.libcommun.outils.Constantes;

/**
 * Commune.
 */
public class Commune implements Serializable {
  // Constantes
  public static final int LONGUEUR_CODEPOSTAL = 5;
  public static final int LONGUEUR_VILLE = 24;
  
  private int codePostal = 0;
  private String nom = "";
  
  // -- Méthodes publiques
  
  /**
   * Formate correctement le code postal.
   */
  public String getCodePostalFormate() {
    if (codePostal == 0) {
      return "";
    }
    return String.format("%0" + LONGUEUR_CODEPOSTAL + "d", codePostal);
  }
  
  /**
   * Initialise la ville en retirant le code postal.
   */
  public void setNomAvecCodePostal(String ville) {
    if ((ville != null) && (ville.length() >= (LONGUEUR_CODEPOSTAL + 1))) {
      this.nom = ville.substring(LONGUEUR_CODEPOSTAL + 1);
    }
    else {
      this.nom = ville;
    }
  }
  
  /**
   * Formate le nom d'une ville en enlevant les abréviasions.
   */
  public static String normerNomVille(String pVille) {
    // Mise en majuscule et suppression des espaces
    pVille = Constantes.normerTexte(pVille).toUpperCase();
    
    // Remplacement des lettres accentuées
    pVille = Normalizer.normalize(pVille, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    
    // Remplacement des underscore par des tirets
    pVille = pVille.replace('_', '-');
    // Suppression des tirets en début et en fin de chaine
    pVille = pVille.replaceAll("(^-)|(-$)", "");
    
    // Remplacement des abréviations
    // ST en SAINT
    pVille = pVille.replaceAll("\\b(e?)ST\\b", "SAINT");
    // STE en SAINTE
    pVille = pVille.replaceAll("\\b(e?)STE\\b", "SAINTE");
    // S/ en SUR
    pVille = pVille.replaceAll("\\b(e?)S/", "SUR ");
    // / en SUR
    pVille = pVille.replaceAll("/", " SUR ");
    // Suppression des espaces en trop entre les mots
    pVille = pVille.replaceAll("\\s+", " ");
    // Suppression des espaces devant et derrière la chaine de caractères
    pVille = pVille.trim();
    
    return pVille;
  }
  
  /**
   * Formate le nom d'une ville pour faciliter les comparaisons. Ne pas utiliser cette méthode pour afficher le nom de la ville car le nom
   * de la ville contient des abréviations, il faut utiliser la méthode normerNomVille() pour un affichage.
   * Cela permet de pouvoir comparer des noms de villes provenant de sources différentes.
   */
  public static String normerNomVillePourComparaison(String pVille) {
    // Mise en majuscule et suppression des espaces
    pVille = Constantes.normerTexte(pVille).toUpperCase();
    
    // Remplacement des lettres accentuées
    pVille = Normalizer.normalize(pVille, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    
    // Suppression des tirets et underscore
    pVille = pVille.replace('-', ' ');
    pVille = pVille.replace('_', ' ');
    
    // Remplacement des abréviations
    // SAINT en ST
    pVille = pVille.replaceAll("\\b(e?)SAINT\\b", "ST");
    // SAINTE en ST
    pVille = pVille.replaceAll("\\b(e?)SAINTE\\b", "ST");
    // STE en ST
    pVille = pVille.replaceAll("\\b(e?)STE\\b", "ST");
    // S/ en SUR
    pVille = pVille.replaceAll("\\b(e?)S/", "SUR ");
    // / en SUR
    pVille = pVille.replaceAll("/", " SUR ");
    // Suppression des espaces en trop entre les mots
    pVille = pVille.replaceAll("\\s+", " ");
    // Suppression des espaces devant et derrière la chaine de caractères
    pVille = pVille.trim();
    
    return pVille;
  }
  
  // -- Accesseurs
  
  public int getCodePostal() {
    return codePostal;
  }
  
  public void setCodePostal(int pCodePostal) {
    codePostal = pCodePostal;
  }
  
  public String getNom() {
    return nom;
  }
  
  public void setNom(String pNom) {
    nom = pNom;
  }
  
  @Override
  public String toString() {
    return getNom();
  }
}
