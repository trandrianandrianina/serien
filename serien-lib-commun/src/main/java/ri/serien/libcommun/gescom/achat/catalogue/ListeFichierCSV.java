/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import java.util.ArrayList;

import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;

/**
 * Liste de fichiers CSV contenant les catalogues fournisseurs
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de configurations.
 * La méthode chargerTout permet d'obtenir la liste de toutes les configurations d'un établissement.
 */
public class ListeFichierCSV extends ArrayList<String> {
  /**
   * Constructeur.
   */
  public ListeFichierCSV() {
  }
  
  /**
   * Charger la liste des fichiers CSV suscpetibles d'être configurés pour un import de catalogue fournisseur
   */
  public static ListeFichierCSV chargerTout(IdSession pIdSession) {
    return ManagerServiceFournisseur.chargerListeFichiersCataloguesCSV(pIdSession);
  }
}
