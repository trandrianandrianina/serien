/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.motifretour;

import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de Motifs de retour.
 */
public class ListeMotifRetour extends ArrayList<MotifRetour> {
  
  // -- Méthodes publiques
  
  /**
   * Charger tous les TypeFacturation de l'établissement donné.
   */
  public static ListeMotifRetour chargerTout(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return ManagerServiceParametre.chargerListeMotifRetour(pIdSession, pIdEtablissement);
  }
  
  /**
   * Retourner un MotifRetour de la liste à partir de son identifiant.
   */
  public MotifRetour retournerMotifRetourParId(IdMotifRetour pIdMotifRetour) {
    if (pIdMotifRetour == null) {
      return null;
    }
    for (MotifRetour motifRetour : this) {
      if (motifRetour != null && Constantes.equals(motifRetour.getId(), pIdMotifRetour)) {
        return motifRetour;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner un TypeFacturation de la liste à partir de son nom.
   */
  public MotifRetour retournerMotifRetourParNom(String pLibelle) {
    if (pLibelle == null) {
      return null;
    }
    for (MotifRetour motifRetour : this) {
      if (motifRetour != null && motifRetour.getLibelle().equals(pLibelle)) {
        return motifRetour;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un TypeFacturation est présent dans la liste.
   */
  public boolean isPresent(IdMotifRetour pIdTypeFacturation) {
    return retournerMotifRetourParId(pIdTypeFacturation) != null;
  }
}
