/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.imagearticle;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.SessionTransfert;

/**
 * Liste des objets Photo.
 * 
 * Cette liste peut être chargée via une liste d'IdImageArticle, par un tableau d'objets File ou bien depuis un chemin d'accès à un
 * dossier et
 * les informations sur l'envrionnement de l'utilisateur.
 * 
 */
public class ListeImageArticle extends ListeClasseMetier<IdImageArticle, ImageArticle, ListeImageArticle> {
  
  @Override
  public ListeImageArticle charger(IdSession pIdSession, List<IdImageArticle> pListeId) {
    ListeImageArticle listeImageArticle = new ListeImageArticle();
    for (IdImageArticle idImageArticle : pListeId) {
      ImageArticle imageArticle = new ImageArticle(idImageArticle);
      try {
        imageArticle.setIconeImage(new ImageIcon(ImageIO.read(new File(idImageArticle.getCheminDAccesImageArticle()))));
      }
      catch (IOException e) {
        Trace.erreur("Image non lue.");
        imageArticle.setIconeImage(null);
      }
      listeImageArticle.add(imageArticle);
    }
    return listeImageArticle;
  }
  
  // Non utilisée, pas de sens d'utiliser l'idEtablissement
  @Override
  public ListeImageArticle charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return null;
  }
  
  /**
   * Permet de contrôler la présence d'une image dans la liste.
   * La comparaison se fait sur l'IdImageArticle ainsi que sur le nom de l'image.
   * 
   * La comparaison sur le nom de l'image est nécessaire dans le cas où il faudrait téléverser une image d'un dossier vers le serveur.
   * Les dossiers d'origine de l'image étant différents, l'Id est différent, mais le dossier d'arrivée ne doit pas contenir un fichier du
   * même nom.
   * 
   * @param pIdImageArticle Id de l'ImageArticle à rechercher dans la liste.
   * @return True si la liste contient une image ayant la même Id ou le même nom, false sinon.
   */
  @Override
  public boolean contains(IdImageArticle pIdImageArticle) {
    for (ImageArticle image : this) {
      if (Constantes.equals(image.getId(), pIdImageArticle)) {
        return true;
      }
      if (Constantes.equals(image.getId().getNomImageArticle(), pIdImageArticle.getNomImageArticle())) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Téléverser une liste de photos vers le serveur.
   * 
   * @param pSessionTransfert Session de transfert.
   * @param pCheminDAccesDossier Chemin d'accès au dossier vers lequel téléverser les photos.
   * @param pListeIdImageArticlePourAjout Liste des idImageArticle à téléverser au serveur.
   */
  public void sauverPhoto(SessionTransfert pSessionTransfert, String pCheminDAccesDossier,
      List<IdImageArticle> pListeIdImageArticlePourAjout) {
    for (IdImageArticle idImageArticle : pListeIdImageArticlePourAjout) {
      if (controlerPathIFS(pCheminDAccesDossier)) {
        uploadFichierVersServeur(new File(idImageArticle.getCheminDAccesImageArticle()), pCheminDAccesDossier, pSessionTransfert);
      }
      else {
        FileNG fichier = new FileNG(idImageArticle.getCheminDAccesImageArticle());
        String cheminDossierTemp = pCheminDAccesDossier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
        fichier.copyTo(cheminDossierTemp + File.separatorChar + idImageArticle.getNomImageArticle());
      }
    }
  }
  
  /**
   * Supprimer une liste de photos du serveur.
   * 
   * @param pSessionTransfert Session de transfert.
   * @param pCheminDAccesDossier Chemin d'accès au dossier duquel supprimer les photos.
   * @param pListeIdImageArticlePourSuppression Liste des idImageArticle à supprimer du serveur.
   */
  public void supprimerPhoto(SessionTransfert pSessionTransfert, String pCheminDAccesDossier,
      List<IdImageArticle> pListeIdImageArticlePourSuppression) {
    for (IdImageArticle idImageArticleASupprimer : pListeIdImageArticlePourSuppression) {
      // Supprime le fichier.
      if (controlerPathIFS(pCheminDAccesDossier)) {
        String fichier = pCheminDAccesDossier + Constantes.SEPARATEUR_CHAINE_CHAR + idImageArticleASupprimer.getNomImageArticle();
        pSessionTransfert.fichierASupprimer(fichier);
      }
      else {
        File fichier = new File(idImageArticleASupprimer.getCheminDAccesImageArticle());
        fichier.delete();
      }
    }
  }
  
  /**
   * Contrôler la nature du chemin d'accès.
   *
   * @param pCheminDAcces Chemin d'accès aux fichiers
   * @return <b>True</b> si chemin IFS, <b>false</b> sinon.
   */
  private boolean controlerPathIFS(String pCheminDAcces) {
    if ((pCheminDAcces.indexOf(':') != -1) || (pCheminDAcces.startsWith("\\\\"))) {
      return false;
    }
    return true;
  }
  
  /**
   * Téléverser un fichier sur le serveur.
   *
   * @param pFichier Fichier à téléverser.
   * @param pCheminDestination Chemin d'accès du dossier de destination dans lequel le fichier est téléversé.
   */
  private void uploadFichierVersServeur(final File pFichier, final String pCheminDestination, final SessionTransfert pSessionTransfert) {
    Thread thread = new Thread() {
      @Override
      public void run() {
        pSessionTransfert.fichierAUploader(pFichier, pCheminDestination);
        pSessionTransfert.fichierUploade(pFichier, pCheminDestination);
      }
    };
    thread.start();
  }
  
}
