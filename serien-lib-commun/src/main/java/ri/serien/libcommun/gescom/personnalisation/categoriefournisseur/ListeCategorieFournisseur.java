/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.categoriefournisseur;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de CategorieFournisseur.
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de CategorieFournisseur. Elle
 * contient
 * toutes les méthodes oeuvrant sur la liste tandis que la classe CategorieFournisseur contient les méthodes oeuvrant
 * sur une
 * seule CategorieFournisseur.
 */
public class ListeCategorieFournisseur extends ListeClasseMetier<IdCategorieFournisseur, CategorieFournisseur, ListeCategorieFournisseur> {
  
  /**
   * Constructeur.
   */
  public ListeCategorieFournisseur() {
  }
  
  /**
   * Charge la liste de CategorieFournisseur suivant une liste d'IdCategorieFournisseur
   */
  @Override
  public ListeCategorieFournisseur charger(IdSession pIdSession, List<IdCategorieFournisseur> pListeId) {
    return null;
  }
  
  /**
   * Charger le liste de CategorieFournisseur suivant l'établissement.
   */
  @Override
  public ListeCategorieFournisseur charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CriteresRechercheCategoriesFournisseur criteres = new CriteresRechercheCategoriesFournisseur();
    criteres.setTypeRecherche(CriteresRechercheCategoriesFournisseur.RECHERCHE_CATEGORIE_FOURNISSEUR);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeCategorieFournisseur(pIdSession, criteres);
  }
  
}
