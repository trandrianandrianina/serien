/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.acheteur;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste d'acheteurs.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste d'acheteurs.
 * La méthode chargerTout permet d'obtenir la liste de tous les acheteurs d'un établissement.
 */
public class ListeAcheteur extends ListeClasseMetier<IdAcheteur, Acheteur, ListeAcheteur> {
  /**
   * Constructeur.
   */
  public ListeAcheteur() {
  }
  
  /**
   * Charger les acheteurs suivant l'établissement.
   */
  @Override
  public ListeAcheteur charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CriteresRechercheAcheteur criteres = new CriteresRechercheAcheteur();
    criteres.setTypeRecherche(CriteresRechercheAcheteur.TOUT_RETOURNER_POUR_UN_ETABLISSEMENT);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeAcheteur(pIdSession, criteres);
  }
  
  /**
   * Charger touts les acheteurs suivant une liste d'IdAcheteur.
   */
  @Override
  public ListeAcheteur charger(IdSession pIdSession, List<IdAcheteur> pListeId) {
    return null;
  }
  
  /**
   * Retourner un acheteur de la liste à partir de son identifiant.
   */
  public Acheteur retournerAcheteurParId(IdAcheteur pIdAcheteur) {
    if (pIdAcheteur == null) {
      return null;
    }
    for (Acheteur acheteur : this) {
      if (acheteur != null && Constantes.equals(acheteur.getId(), pIdAcheteur)) {
        return acheteur;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner un acheteur de la liste à partir de son nom.
   */
  public Acheteur retournerAcheteurParNom(String pNom) {
    if (pNom == null) {
      return null;
    }
    for (Acheteur acheteur : this) {
      if (acheteur != null && acheteur.getNom().equals(pNom)) {
        return acheteur;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un acheteur est présent dans la liste.
   */
  public boolean isPresent(IdAcheteur pIdAcheteur) {
    return retournerAcheteurParId(pIdAcheteur) != null;
  }
  
}
