/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.chantier;

import java.io.Serializable;
import java.util.Date;

import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Critères de recherche sur les chantiers.
 */
public class CritereChantier implements Serializable {
  
  // Variables
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  private IdEtablissement idEtablissement = null;
  private int numeroChantier = 0;
  private Date dateCreationDebut = null;
  private Date dateCreationFin = null;
  private IdClient idClient = null;
  private EnumStatutChantier statut = EnumStatutChantier.VALIDE;
  private boolean isAvecBloque = false;
  
  /**
   * Vide les critères de recherche
   */
  public void initialiser() {
    rechercheGenerique.initialiser();
    numeroChantier = 0;
    dateCreationDebut = null;
    dateCreationFin = null;
    statut = EnumStatutChantier.VALIDE;
  }
  
  // -- Accesseurs
  /**
   * Identifiant de l'établissement dans lequel est recherché le client.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Filtre sur l'établissement.
   */
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void setTexteRechercheGenerique(String pTexteRecherche) {
    rechercheGenerique.setTexte(pTexteRecherche);
  }
  
  /**
   * Filtre sur le numéro du document
   */
  public int getNumeroChantier() {
    return numeroChantier;
  }
  
  /**
   * Modifier le filtre sur le numéro du document
   */
  public void setNumeroChantier(int pNumeroChantier) {
    numeroChantier = pNumeroChantier;
  }
  
  /**
   * Filtre sur la date de création marquant le début de la plage de dates
   */
  public Date getDateCreationDebut() {
    return dateCreationDebut;
  }
  
  /**
   * Modifier le filtre sur la date de création marquant le début de la plage de dates
   */
  public void setDateCreationDebut(Date pDateCreationDebut) {
    dateCreationDebut = pDateCreationDebut;
  }
  
  /**
   * Filtre sur la date de création marquant la fin de la plage de dates
   */
  public Date getDateCreationFin() {
    return dateCreationFin;
  }
  
  /**
   * Modifier le filtre sur la date de création marquant la fin de la plage de dates
   */
  public void setDateCreationFin(Date pDateCreationFin) {
    dateCreationFin = pDateCreationFin;
  }
  
  /**
   * Filtre sur l'identifiant du client
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Modifier le filtre sur l'identifiant du client
   */
  public void setIdClient(IdClient pIdClient) {
    idClient = pIdClient;
  }
  
  /**
   * Filtre sur le statut du chantier.
   */
  public EnumStatutChantier getStatut() {
    return statut;
  }
  
  /**
   * Modifier le filtre sur le statut du chantier
   */
  public void setStatut(EnumStatutChantier pStatut) {
    statut = pStatut;
  }
  
  /**
   * Filtre avec chantiers bloqués
   */
  public boolean isAvecBloque() {
    return isAvecBloque;
  }
  
  /**
   * Modifier le filtre avec chantiers bloqués
   */
  public void setAvecBloque(boolean isAvecBloque) {
    this.isAvecBloque = isAvecBloque;
  }
  
}
