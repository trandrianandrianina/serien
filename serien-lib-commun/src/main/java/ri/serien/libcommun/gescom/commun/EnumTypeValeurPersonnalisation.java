/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de valeur possible pour la personnalisation et la sécurité.
 */
public enum EnumTypeValeurPersonnalisation {
  TYPE_A('a', "1 = OUI"),
  TYPE_B('b', "0 < Val <9"),
  TYPE_C('c', "Voir aide"),
  TYPE_D('d', "Voir documentation");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeValeurPersonnalisation(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return code.toString();
  }
  
  /**
   * Retourner l'objet enum par son numéro.
   */
  static public EnumTypeValeurPersonnalisation valueOfByCode(Character pCode) {
    for (EnumTypeValeurPersonnalisation value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de valeur est inconnu : " + pCode);
  }
  
}
