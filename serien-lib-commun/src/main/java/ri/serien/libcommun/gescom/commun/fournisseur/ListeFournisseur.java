/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;

/**
 * Liste de fournisseurs.
 */
public class ListeFournisseur extends ArrayList<Fournisseur> {
  /**
   * Constructeurs.
   */
  public ListeFournisseur() {
  }
  
  /**
   * Charge toutes les fournisseurs de l'établissement donnée.
   */
  public static ListeFournisseur chargerTout(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null || !pIdEtablissement.isExistant()) {
      throw new MessageErreurException("L'établissement est invalide.");
    }
    
    CritereFournisseur criteres = new CritereFournisseur();
    criteres.setTypeRecherche(CritereFournisseur.RECHERCHER_TOUS_FOURNISSEURS_POUR_UN_ETABLISSEMENT);
    criteres.setIdEtablissement(pIdEtablissement);
    
    return ManagerServiceFournisseur.chargerListeFournisseur(pIdSession, criteres);
  }
  
  public void rajouterUnFournisseur(Fournisseur pFournisseur) {
    if (pFournisseur == null) {
      return;
    }
    add(pFournisseur);
  }
  
  /**
   * Retourne un fournisseur à partir identifiant.
   */
  public Fournisseur retournerFournisseurParId(IdFournisseur pIdFournisseur) {
    for (Fournisseur fournisseur : this) {
      if (Constantes.equals(pIdFournisseur, fournisseur.getId())) {
        return fournisseur;
      }
    }
    return null;
  }
  
  /**
   * Retourner le nom du fournisseur correspondant à l'identifiant passé en paramètre.
   */
  public String retournerNomFournisseurParId(IdFournisseur pIdFournisseur) {
    if (pIdFournisseur == null) {
      return "";
    }
    for (Fournisseur fournisseur : this) {
      if (fournisseur != null && fournisseur.getId().equals(pIdFournisseur)) {
        return fournisseur.getNomFournisseur();
      }
    }
    return "";
  }
  
  /**
   * Trier les fournisseur suivant leurs identifiants
   */
  public void trierParId() {
    Collections.sort(this, new Comparator<Fournisseur>() {
      @Override
      public int compare(Fournisseur o1, Fournisseur o2) {
        int collectif1 = o1.getId().getCollectif();
        int collectif2 = o2.getId().getCollectif();
        int numero1 = o1.getId().getNumero();
        int numero2 = o2.getId().getNumero();
        
        if (collectif1 > collectif2) {
          return 1;
        }
        else if (collectif1 < collectif2) {
          return -1;
        }
        else {
          if (numero1 > numero2) {
            return 1;
          }
          else if (numero1 < numero2) {
            return -1;
          }
          
          else {
            return 0;
          }
        }
      }
    });
  }
  
}
