/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrearticle;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

/**
 * Cette contient un ensemble de critères qui permettent la recherche de paramètres articles.
 */
public class CritereParametreArticle implements Serializable {
  private IdArticle idArticle = null;
  private IdMagasin idMagasin = null;
  private Boolean ps124 = null;
  private Boolean modeNegoce = null;
  
  // -- Accesseurs
  
  /**
   * Retourner l'identifiant de l'article.
   * 
   * @return
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Modifier l'identifiant de l'article.
   * 
   * @param pIdArticle
   */
  public void setIdArticle(IdArticle pIdArticle) {
    this.idArticle = pIdArticle;
  }
  
  /**
   * Retourner l'identifiant du magasin.
   * 
   * @return
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier l'identifiant du magasin.
   * 
   * @param idMagasin
   */
  public void setIdMagasin(IdMagasin idMagasin) {
    this.idMagasin = idMagasin;
  }
  
  /**
   * Retourner si le PS124 est actif.
   * 
   * @return
   */
  public Boolean getPs124() {
    return ps124;
  }
  
  /**
   * Modifier le PS124.
   * 
   * @param pPs124
   */
  public void setPs124(Boolean pPs124) {
    this.ps124 = pPs124;
  }
  
  /**
   * Retourner si le mode négoce est actif.
   * 
   * @return
   */
  public Boolean getModeNegoce() {
    return modeNegoce;
  }
  
  /**
   * Modifier le mode négoce.
   * 
   * @param pModeNegoce
   */
  public void setModeNegoce(Boolean pModeNegoce) {
    this.modeNegoce = pModeNegoce;
  }
  
}
