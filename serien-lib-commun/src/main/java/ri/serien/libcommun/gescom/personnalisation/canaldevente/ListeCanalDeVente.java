/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.canaldevente;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste des canaux de vente.
 * L'utilisation de cette classe est à privilégiée dès qu'on manipule un canal de vente. Elle contient toutes les
 * méthodes oeuvrant
 * sur la liste tandis que la classe CanalDeVente contient les méthodes oeuvrant sur uns seul CanalDeVente.
 */
public class ListeCanalDeVente extends ListeClasseMetier<IdCanalDeVente, CanalDeVente, ListeCanalDeVente> {
  
  /**
   * Charger toute les affaires suivant une liste d'IdAffaire.
   */
  @Override
  public ListeCanalDeVente charger(IdSession pIdSession, List<IdCanalDeVente> pListeId) {
    return null;
  }
  
  /**
   * Charger les affaires suivant l'établissement.
   */
  @Override
  public ListeCanalDeVente charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas défini.");
    }
    CriteresRechercheCanalDeVente criteres = new CriteresRechercheCanalDeVente();
    criteres.setTypeRecherche(CriteresRechercheCanalDeVente.RECHERCHE_CANAL_DE_VENTE);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    ListeCanalDeVente listeCanalDeVente = ManagerServiceParametre.chargerListeCanalDeVente(pIdSession, criteres);
    return listeCanalDeVente;
  }
  
}
