/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametretarif;

import java.io.Serializable;
import java.math.BigDecimal;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Colonne tarif.
 * 
 * Une colonne tarif est définie par un numéro de colonne, un coefficient et un prix de base HT. Le numéro de colonne doit être compris
 * entre 1 et 10. En mode négoce, seules les 6 premières colonnes de tarif sont utilisées mais ce n'est pas génant s'il y en a plus.
 * Elles seront simplement ignorées lors des calculs.
 * 
 * A noter :
 * - en mode négoce les prix des colonnes tarifs sont systématiquement enregistrés en table,
 * - en mode classique les prix des colonnes tarifs sont calculés à la volé s'il y a un coefficient mais si l'utilisateur a saisi un
 * prix alors il est enregistré en table (c'est le programme VGVM50 en mode classique appelé depuis la fiche article).
 * 
 * En pratique, lors de la lecture d'une colonne tarif dans la base de données :
 * - Si le coefficient est présent (> 0) alors le prix de base HT la colonne est recalculé à partir de la colonne 1 et du coefficient.
 * - Sinon le prix de base HT de la colonne est considéré comme un prix saisi.
 */
public class ColonneTarif implements Serializable {
  // Constantes
  public static final int NOMBRE_MAX_COLONNE_TARIF = 10;
  
  private Integer numero = null;
  private BigDecimal coefficient = null;
  private BigDecimal prixBaseHT = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs ou fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé.
   * 
   * @param pNumero Numéro de la colonne de tarif.
   * @param pCoefficient Coefficient de la colonne de tarif.
   * @param pPrixBaseHT Prix de base HT de la colonne de tarif.
   */
  private ColonneTarif(Integer pNumero, BigDecimal pCoefficient, BigDecimal pPrixBaseHT) {
    numero = pNumero;
    prixBaseHT = pPrixBaseHT;
  }
  
  /**
   * Créé une instance de ColonneTarif.
   * 
   * @param pNumero Numéro de la colonne de tarif.
   * @param pCoefficient Coefficient de la colonne de tarif.
   * @param pPrixBaseHT Prix de base HT de la colonne de tarif.
   * @return Colonne d etarif.
   */
  public static ColonneTarif getInstance(Integer pNumero, BigDecimal pCoefficient, BigDecimal pPrixBaseHT) {
    controlerNumero(pNumero);
    return new ColonneTarif(pNumero, pCoefficient, pPrixBaseHT);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Contrôler la validité du numéro de colonne.
   * @param pNumero Numéro de colonne.
   */
  private static void controlerNumero(Integer pNumero) {
    if (pNumero == null) {
      throw new MessageErreurException("Le numéro de colonne est invalide.");
    }
    if (pNumero < 1 || pNumero > NOMBRE_MAX_COLONNE_TARIF) {
      throw new MessageErreurException("Le numéro de colonne doit être compris entre 1 et 10 inclus.");
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le numéro de la colonne de tarif.
   * @return Numero colonne de tarif (entre 1 et 10 inclus).
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Retourner le coefficient de la colonne de tarif.
   * @return Prix de base HT.
   */
  public BigDecimal getCoefficient() {
    return coefficient;
  }
  
  /**
   * Retourner le prix de base HT de la colonne de tarif.
   * @return Prix de base HT.
   */
  public BigDecimal getPrixBaseHT() {
    return prixBaseHT;
  }
  
  /**
   * Modifier le prix de base HT de la colonne de tarif.
   * @param Prix de base HT.
   */
  public void setPrixBaseHT(BigDecimal pPrixBaseHT) {
    prixBaseHT = pPrixBaseHT;
  }
  
}
