/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrechantier;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les différents types de conditions chantier.
 */
public enum EnumTypeChantier {
  PAR_DEFAUT(' ', "Par défaut"),
  ARTICLE('A', "Article"),
  GROUPE('G', "Groupe"),
  FAMILLE('F', "Famille"),
  SOUS_FAMILLE('S', "Sous famille"),
  QUANTITATIVE('1', "Quantitative");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeChantier(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  static public EnumTypeChantier valueOfByCode(Character pCode) {
    for (EnumTypeChantier value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code du type de la condition chantier est invalide : " + pCode);
  }
  
}
