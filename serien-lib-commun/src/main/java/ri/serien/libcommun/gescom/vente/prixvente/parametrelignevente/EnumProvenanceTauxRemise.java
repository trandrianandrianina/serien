/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des provenances possibles pour les tauxs remises dans une ligne de vente..
 * 
 * Cette information est stockée dans le champ L1TR de la ligne de vente. Lors d'une création de ligne de vente, les valeurs peuvent
 * être initialisées avec les données du client, de l’article, des conditions de ventes, de l'entête... A chaque type de valeur est
 * associé un indicateur qui renseigne sur la provenance de la valeur.
 * 
 * Voici les valeurs pour les taux de remises (champ L1TR) :
 * - 0 = ???
 * - 1 = Taux de remises du client (via l'entête du document de vente),
 * - 2 = Taux de remises de la condition de vente,
 * - 3 = Taux de remises saisis manuellement dans la ligne de vente.
 * 
 * Si l'indicateur est égal à 3, cela signifie que les taux de remises ont été explicitement saisie par l’utilisateur dans la
 * ligne de vente. Si l'indicateur est différent de 3, cela signifie que les taux de remises sont issus d’un calcul précédent
 * et qu’il faut ignorer leurs valeurs.
 */
public enum EnumProvenanceTauxRemise {
  DEFAUT(0, "Aucune"),
  CLIENT(1, "Client"),
  CONDITION_VENTE(2, "Condition de vente"),
  SAISIE_LIGNE_VENTE(3, "Saisie ligne de vente");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumProvenanceTauxRemise(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumProvenanceTauxRemise valueOfByCode(Integer pNumero) {
    for (EnumProvenanceTauxRemise value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("La provenance des taux de remises de la ligne de vente  est invalide : " + pNumero);
  }
  
}
