/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.util.ArrayList;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceClient;

/**
 * Liste de clients.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de client.
 */
public class ListeClientBase extends ArrayList<ClientBase> {
  /**
   * Constructeur.
   */
  public ListeClientBase() {
  }
  
  public static ListeClientBase charger(IdSession pidSession, CritereClient pCritereClient) {
    return ManagerServiceClient.chargerListeClientBase(pidSession, pCritereClient);
  }
  
  /**
   * Retourner un Client de la liste à partir de son identifiant.
   */
  public ClientBase getClientBaseParId(IdClient pIdClient) {
    if (pIdClient == null) {
      return null;
    }
    for (ClientBase clientBase : this) {
      if (clientBase != null && Constantes.equals(clientBase.getId(), pIdClient)) {
        return clientBase;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner un Client de la liste à partir de son nom.
   */
  public ClientBase getClientBaseParNom(String pNom) {
    if (pNom == null) {
      return null;
    }
    for (ClientBase clientBase : this) {
      if (clientBase != null && clientBase.getAdresse() != null && clientBase.getAdresse().getNom().equals(pNom)) {
        return clientBase;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un Client est présent dans la liste.
   */
  public boolean isPresent(IdClient pIdClient) {
    return getClientBaseParId(pIdClient) != null;
  }
}
