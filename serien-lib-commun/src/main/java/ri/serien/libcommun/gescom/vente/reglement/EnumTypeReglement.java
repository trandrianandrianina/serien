/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'un règlement.
 * 
 * Ne pas confondre le type de règlement et le mode de règlement, le type correspond au moyen physique et unique comme chèque, carte bleu,
 * espèce. Alors que le mode de règlement permet de personnaliser ce moyen physique par exemple: une carte bleu chez le Crédit Agricole.
 * Le mode de règlement se configure dans la personnalisation de la gescom (paramètre RG) alors que le type est défini en dur dans le code
 * RPG et Java.
 */
public enum EnumTypeReglement {
  CHEQUE('C', "Chèque", "Par chèque"),
  ESPECES('E', "Espèces", "En espèces"),
  CARTE_BANCAIRE('K', "Carte bancaire", "Par carte bancaire"),
  VIREMENT('V', "Virement", "Par virement"),
  // A priori n'est pas considéré comme un type de paiement classique - A voir avec Bruno
  AVOIR('A', "Avoir", "Avec l'avoir"),
  TRAITE('T', "Traite", "Par traite"),
  TIP('I', "Tip", "Par tip"),
  PRELEVEMENT('P', "Prélèvement", "Par prélèvement"),
  BILLET_A_ORDRE('B', "Billet à ordre", "Avec billet à ordre"),
  RELEVE_FACTURE('R', "Relevé de facture", "Avec relevé de facture"),
  REGLEMENT_DIVERS('D', "Règlement divers", "Avec reglement divers");
  
  private final Character code;
  private final String libelle;
  private final String libelleReglement;
  
  /**
   * Constructeur.
   */
  EnumTypeReglement(Character pCode, String pLibelle, String pLibelleReglement) {
    code = pCode;
    libelle = pLibelle;
    libelleReglement = pLibelleReglement;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Le libellé associé au code en minuscules.
   */
  public String getLibelleEnMinuscules() {
    return libelle.toLowerCase();
  }
  
  /**
   * Le libellé utilisé pour les règlements associé au code.
   */
  public String getLibelleReglement() {
    return libelleReglement;
  }
  
  /**
   * Le libellé en minuscule utilisé pour les règlements associé au code.
   */
  public String getLibelleReglementEnMinuscules() {
    return libelleReglement.toLowerCase();
  }
  
  /**
   * Retourner le libellé associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeReglement valueOfByCode(Character pCode) {
    for (EnumTypeReglement value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type du règlement est invalide : " + pCode);
  }
  
}
