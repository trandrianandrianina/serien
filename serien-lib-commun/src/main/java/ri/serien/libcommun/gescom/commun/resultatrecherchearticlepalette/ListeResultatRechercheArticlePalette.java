/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Liste d'objets ResultatRechercheArticle.
 * L'utilisation de cette classe est à privilégier dès qu'on manipule une liste de ResultatRechercheArticle.
 */
public class ListeResultatRechercheArticlePalette
    extends ListeClasseMetier<IdResultatRechercheArticlePalette, ResultatRechercheArticlePalette, ListeResultatRechercheArticlePalette> {
  /**
   * Constructeur.
   */
  public ListeResultatRechercheArticlePalette() {
  }
  
  /**
   * Charger une liste de résultat de recherche article.
   */
  @Override
  public ListeResultatRechercheArticlePalette charger(IdSession pIdSession, List<IdResultatRechercheArticlePalette> pListeId) {
    return null;
  }
  
  /**
   * Charger toutes les informations de ventes d'un établissement.
   */
  @Override
  public ListeResultatRechercheArticlePalette charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return null;
  }
}
