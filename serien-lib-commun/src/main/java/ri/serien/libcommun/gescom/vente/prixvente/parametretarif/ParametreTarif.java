/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametretarif;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.vente.prixvente.origine.EnumOrigineDevise;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.DateHeure;

/**
 * Classe regroupant tous les paramètres du tarif article pour le calcul du prix de vente.
 */
public class ParametreTarif implements Serializable {
  // Variables
  private IdArticle idArticle = null;
  private EnumOrigineDevise origineDevise = null;
  private String codeDevise = null;
  private Date dateTarif = null;
  private EnumGraduationDecimaleTarif graduationDecimaleTarif = null;
  private List<ColonneTarif> listeColonneTarif = new ArrayList<ColonneTarif>();
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le libellé du tarif.
   * @return Libellé.
   */
  public String getLibelle() {
    String libelle = "";
    
    // Ajouter l'identifiant de l'article
    if (idArticle != null) {
      libelle += idArticle;
    }
    
    // Ajouter la date du tarif
    if (dateTarif != null) {
      if (!libelle.isEmpty()) {
        libelle += " du ";
      }
      libelle += DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, dateTarif);
    }
    
    return libelle;
  }
  
  /**
   * Retourner l'identifiant article du tarif.
   * @return Code article.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Modifier l'identifiant article du tarif.
   * @param pIdArticle Code article.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  /**
   * Retourner l'origine de la devise du tarif.
   * @return Origine devise.
   */
  public EnumOrigineDevise getOrigineDevise() {
    return origineDevise;
  }
  
  /**
   * Modifier l'origine de la devise du tarif.
   * @param pOrigineDevise Origine devise.
   */
  public void setOrigineDevise(EnumOrigineDevise pOrigineDevise) {
    origineDevise = pOrigineDevise;
  }
  
  /**
   * Retourner le code devise du tarif.
   * @return Code devise.
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Modifier le code devise du tarif.
   * @param pCodeDevise Code devise.
   */
  public void setCodeDevise(String pCodeDevise) {
    codeDevise = pCodeDevise;
  }
  
  /**
   * Retourner la date du tarif.
   * @return Date du tarif.
   */
  public Date getDateTarif() {
    return dateTarif;
  }
  
  /**
   * Modifier la date du tarif.
   * @param dateTarif Date du tarif.
   */
  public void setDateTarif(Date pDateTarif) {
    dateTarif = pDateTarif;
  }
  
  /**
   * Retourner le type de la graduation des décimales à appliquer au tarif.
   * @return Graduation des décimales.
   */
  public EnumGraduationDecimaleTarif getGraduationDecimaleTarif() {
    return graduationDecimaleTarif;
  }
  
  /**
   * Modifier le type de la graduation des decimales à appliquer au tarif.
   * @param pGraduationDecimaleTarif Graduation des décimales.
   */
  public void setGraduationDecimaleTarif(EnumGraduationDecimaleTarif pGraduationDecimaleTarif) {
    graduationDecimaleTarif = pGraduationDecimaleTarif;
  }
  
  /**
   * Ajouter une colonne de tarif.
   * 
   * Si le coefficient est présent (!=0) alors le prix de la colonne est calculé. On utilise la graduation pour arrondir le prix.
   * 
   * Sinon le prix de la colonne est considéré comme un prix saisi. Dans ce cas, on laisse le prix tel qu'il est dans la base de données.
   * L'arrondi suivant la graduation sera effectuée dans les étapes de calcul de FormulePrixVente.
   * 
   * A noter :
   * - en mode négoce les prix des colonnes tarifs sont systématiquement enregistrés en table,
   * - en mode classique les prix des colonnes tarifs sont calculés à la volé s'il y a un coéfficient mais si l'utilisateur a saisi un
   * prix alors il est enregistré en table (c'est le programme VGVM50 en mode classique appelé depuis la fiche article).
   * 
   * @param pNumeroColonne Numéro de la colonne de tarif à modifier.
   * @param pCoefficient Coefficient de la colonne de tarif.
   * @param pPrixBaseHT Prix de base HT de la colonne de tarif.
   */
  public void ajouterColonneTarif(Integer pNumeroColonne, BigDecimal pCoefficient, BigDecimal pPrixBaseHT) {
    // Vérifier la validité du numéro de colonne
    if (pNumeroColonne < 1 || pNumeroColonne > ColonneTarif.NOMBRE_MAX_COLONNE_TARIF) {
      throw new MessageErreurException("Le numéro de colonne tarif est invalide : " + pNumeroColonne);
    }
    if (listeColonneTarif.size() != pNumeroColonne - 1) {
      throw new MessageErreurException("Il faut ajouter les colonnes tarifs dans l'ordre.");
    }
    
    // Calculer le prix de base HT si le coeffcient est renseigné
    if (pNumeroColonne > 1 && pCoefficient != null && pCoefficient.compareTo(BigDecimal.ZERO) > 0) {
      // Récupérer le prix de base HT de la colonne 1
      BigDecimal prixBaseHTColonne1 = getPrixBaseHT(1);
      if (prixBaseHTColonne1 != null) {
        // Calculer les prix de base HT à l'aide du coefficient
        pPrixBaseHT = prixBaseHTColonne1.multiply(pCoefficient, MathContext.DECIMAL32);
        
        // Arrondir le résulat suivant la graduation du tarif
        if (graduationDecimaleTarif != null) {
          pPrixBaseHT = graduationDecimaleTarif.appliquerGraduation(pPrixBaseHT);
        }
      }
    }
    
    // Créer et ajouter la colonne tarif à la ligne
    ColonneTarif colonneTarif = ColonneTarif.getInstance(pNumeroColonne, pCoefficient, pPrixBaseHT);
    listeColonneTarif.add(colonneTarif);
  }
  
  /**
   * Retourner la colonne tarif dont l'indice est fourni en paramètre.
   * @param pNumeroColonneTarif Numéro de la colonne tarif (de 1 à 6/10).
   * @return Colonne tarif.
   */
  public ColonneTarif getColonneTarif(Integer pNumeroColonneTarif) {
    if (pNumeroColonneTarif == null || pNumeroColonneTarif < 1 || pNumeroColonneTarif > listeColonneTarif.size()) {
      return null;
    }
    return listeColonneTarif.get(pNumeroColonneTarif - 1);
  }
  
  /**
   * Retourner le coefficient dont l'indice est fourni en paramètre.
   * @param pNumeroColonneTarif Numéro de la colonne tarif.
   * @return Coefficent.
   */
  public BigDecimal getCoefficient(Integer pNumeroColonneTarif) {
    ColonneTarif colonneTarif = getColonneTarif(pNumeroColonneTarif);
    if (colonneTarif == null) {
      return null;
    }
    return colonneTarif.getCoefficient();
  }
  
  /**
   * Retourner le prix de base HT dont l'indice est fourni en paramètre.
   * @param pNumeroColonneTarif Numéro de la colonne tarif (de 1 à 6/10).
   * @return Prix de base HT.
   */
  public BigDecimal getPrixBaseHT(Integer pNumeroColonneTarif) {
    ColonneTarif colonneTarif = getColonneTarif(pNumeroColonneTarif);
    if (colonneTarif == null) {
      return null;
    }
    return colonneTarif.getPrixBaseHT();
  }
  
  /**
   * Modifier le prix de base HT dont l'indice est fourni en paramètre.
   * 
   * Cette méthode permet de saisir des prix de base HT en théorie impossible comme par exemple un prix de base HT nul alors qu'un
   * coefficient est renseigné. Comem cette méthode est utilsiée en mode simulation, cette permissivité permet de tester le comportement
   * du composant dans toutes les situations.
   * 
   * @param pPrixBaseHT Prix de base HT.
   * @param pNumeroColonneTarif Numéro de la colonne tarif à modifier (de 1 à 6/10).
   */
  public void setPrixBaseHT(BigDecimal pPrixBaseHT, Integer pNumeroColonneTarif) {
    ColonneTarif colonneTarif = getColonneTarif(pNumeroColonneTarif);
    if (colonneTarif == null) {
      return;
    }
    colonneTarif.setPrixBaseHT(pPrixBaseHT);
  }
  
  /**
   * Retourner le nombre de colonnes tarif contenu dans la liste.
   * 
   * @return
   */
  public int getNombreColonneTarif() {
    if (listeColonneTarif == null || listeColonneTarif.isEmpty()) {
      return 0;
    }
    return listeColonneTarif.size();
  }
  
  /**
   * Retourner la colonne tarif correspondant à un prix de base HT.
   * 
   * On recherche le prix de base HT dans les colonnes tarifs en commençant par la première colonne. Si le prix de base HT est null
   * ou s'il n'est pas trouvé, cela retourne null.
   * 
   * @param pPrixBaseHT Prix de base HT.
   * @return Colonne tarif (null si pas trouvée).
   */
  public ColonneTarif getColonneTarif(BigDecimal pPrixBaseHT) {
    // Vérifier les paramètres
    if (pPrixBaseHT == null || listeColonneTarif == null) {
      return null;
    }
    
    // Rechercher dans les colonnes tarifs en commençant par la première
    for (ColonneTarif colonneTarif : listeColonneTarif) {
      if (colonneTarif != null && Constantes.equals(pPrixBaseHT, colonneTarif.getPrixBaseHT())) {
        return colonneTarif;
      }
    }
    
    return null;
  }
}
