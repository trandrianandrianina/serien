/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'une ligne de de ventes.
 * Trois types sont actuellement gérés dans le logiciel :
 * - Les lignes concernant des articles avec un impact sur le stock.
 * - Les lignes sans impact sur le stock (ligne en valeur, article hors stock ou article stocké vendu sans impacté le stock).
 * - Les lignes commentaires.
 */
public enum EnumTypeLigneVente {
  ARTICLE('C', "Ligne article"),
  HORS_STOCK('S', "Ligne hors stock"),
  COMMENTAIRE('*', "Ligne commentaire");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeLigneVente(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Le libellé associé au code en minuscules.
   */
  public String getLibelleEnMinuscules() {
    return libelle.toLowerCase();
  }
  
  /**
   * Retourner le libellé associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeLigneVente valueOfByCode(Character pCode) {
    for (EnumTypeLigneVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de la ligne de ventes est invalide : " + pCode);
  }
  
  /**
   * Retourner l'objet énum par son code fourni sous forme d'une chaîne de caractères d'une longueur de 1.
   */
  static public EnumTypeLigneVente valueOfByCode(String pCode) {
    if (pCode.length() == 1) {
      Character code = pCode.charAt(0);
      for (EnumTypeLigneVente value : values()) {
        if (code.equals(value.getCode())) {
          return value;
        }
      }
    }
    throw new MessageErreurException("Le type de la ligne de ventes est invalide : " + pCode);
  }
}
