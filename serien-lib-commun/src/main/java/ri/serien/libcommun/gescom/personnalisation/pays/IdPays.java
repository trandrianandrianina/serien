/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.pays;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un pays.
 *
 * L'identifiant est composé du code pays.
 * Il est constitué de 3 caractères et se présente sous la forme AXX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdPays extends AbstractId {
  // Constantes
  private static final int LONGUEUR_CODE_MINIMALE = 1;
  private static final int LONGUEUR_CODE_MAXIMALE = 3;
  
  // Variables
  private final String code;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdPays(EnumEtatObjetMetier pEnumEtatObjetMetier, String pCodePays) {
    super(pEnumEtatObjetMetier);
    code = controlerCode(pCodePays);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdPays getInstance(String pCodePays) {
    return new IdPays(EnumEtatObjetMetier.MODIFIE, pCodePays);
  }
  
  /**
   * Contrôler la validité du code pays.
   * Le code pays est une chaîne de 1 à 3 caractères.
   */
  private static String controlerCode(String pCodePays) {
    if (pCodePays == null) {
      throw new MessageErreurException("Le code pays n'est pas renseigné.");
    }
    
    String valeur = pCodePays.trim();
    if (valeur.length() < LONGUEUR_CODE_MINIMALE && valeur.length() > LONGUEUR_CODE_MAXIMALE) {
      throw new MessageErreurException(
          "Le code pays doit faire entre " + LONGUEUR_CODE_MINIMALE + " et " + LONGUEUR_CODE_MAXIMALE + " caractères : " + valeur);
    }
    return valeur;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdPays)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux code pays.");
    }
    IdPays id = (IdPays) pObject;
    return code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdPays)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdPays id = (IdPays) pObject;
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
}
