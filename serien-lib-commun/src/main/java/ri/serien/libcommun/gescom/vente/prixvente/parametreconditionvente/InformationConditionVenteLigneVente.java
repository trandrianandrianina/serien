/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.gescom.personnalisation.formuleprix.IdFormulePrix;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Cette classe regroupe les informations nécessaires pour la création d'enregistrement dans la table PGVMLCNM qui stocke les conditions
 * de ventes utilisées pour calculer le prix de vente d'une ligne de ventes.
 */
public class InformationConditionVenteLigneVente implements Serializable, Cloneable {
  private Integer etatEnregistrement = null;
  private IdLigneVente idLigneVente = null;
  private Integer ordreApplication = null;
  private IdRattachementClient idRattachementClient = null;
  private IdRattachementArticle idRattachementArticle = null;
  private EnumCategorieConditionVente categorie = null;
  private EnumTypeConditionVente type = null;
  private BigDecimal valeur = null;
  private BigPercentage tauxRemise1 = null;
  private BigPercentage tauxRemise2 = null;
  private BigPercentage tauxRemise3 = null;
  private BigPercentage tauxRemise4 = null;
  private BigPercentage tauxRemise5 = null;
  private BigPercentage tauxRemise6 = null;
  private BigDecimal coefficient = null;
  private Date dateDebutValidite = null;
  private Date dateFinValidite = null;
  private IdFormulePrix idFormulePrix = null;
  private BigDecimal differencePrixUnitaire = null;
  private Boolean conditionVenteCumulative = null;
  private Boolean conditionVentePromotionnelle = null;
  
  // -- Méthodes publiques
  
  /**
   * Initialiser les données à l'aide d'un paramètre des conditions de ventes.
   * @param pParametreConditionVente Paramètre condition de ventes.
   */
  public void initialiserAvecParametreConditionVente(ParametreConditionVente pParametreConditionVente) {
    // Contrôle le paramètre de la condition de ventes
    if (pParametreConditionVente == null) {
      throw new MessageErreurException("Le paramètre de la condition de vente est invalide.");
    }
    
    // Le code état de l'enregistrement
    etatEnregistrement = 0;
    // Le rattachement client
    idRattachementClient = pParametreConditionVente.getIdRattachementClient();
    // Le rattachement article
    idRattachementArticle = pParametreConditionVente.getIdRattachementArticle();
    // La catégorie
    categorie = pParametreConditionVente.getCategorie();
    // Le type de la condition
    type = pParametreConditionVente.getTypeCondition();
    // La valeur
    if (pParametreConditionVente.getValeur() != null) {
      valeur = pParametreConditionVente.getValeur();
    }
    // Les remises
    tauxRemise1 = pParametreConditionVente.getTauxRemise1();
    tauxRemise2 = pParametreConditionVente.getTauxRemise2();
    tauxRemise3 = pParametreConditionVente.getTauxRemise3();
    tauxRemise4 = pParametreConditionVente.getTauxRemise4();
    tauxRemise5 = pParametreConditionVente.getTauxRemise5();
    tauxRemise6 = pParametreConditionVente.getTauxRemise6();
    // Le coefficient
    coefficient = pParametreConditionVente.getCoefficient();
    // Date de validité
    dateDebutValidite = pParametreConditionVente.getDateDebutValidite();
    dateFinValidite = pParametreConditionVente.getDateFinValidite();
    // La formule de prix
    idFormulePrix = pParametreConditionVente.getIdFormulePrix();
    // Condition en ajout
    conditionVenteCumulative = pParametreConditionVente.getCumulative();
  }
  
  /**
   * Retourner le taux de remise, dont l'indice est mentionné en paramètre, de la condition de vente.
   * @param pIndex Indice du taux de remise à modifier (entre 1 et 6).
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise(int pIndex) {
    if (pIndex == 1) {
      return getTauxRemise1();
    }
    else if (pIndex == 2) {
      return getTauxRemise2();
    }
    else if (pIndex == 3) {
      return getTauxRemise3();
    }
    else if (pIndex == 4) {
      return getTauxRemise4();
    }
    else if (pIndex == 5) {
      return getTauxRemise5();
    }
    else if (pIndex == 6) {
      return getTauxRemise6();
    }
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise, dont l'indice est mentionné en paramètre, de la condition de vente.
   * @param pTauxRemise Taux de remise.
   * @param pIndex Indice du taux de remise à modifier.
   */
  public void setTauxRemise(BigPercentage pTauxRemise, int pIndex) {
    if (pIndex == 1) {
      tauxRemise1 = pTauxRemise;
    }
    else if (pIndex == 2) {
      tauxRemise2 = pTauxRemise;
    }
    else if (pIndex == 3) {
      tauxRemise3 = pTauxRemise;
    }
    else if (pIndex == 4) {
      tauxRemise4 = pTauxRemise;
    }
    else if (pIndex == 5) {
      tauxRemise5 = pTauxRemise;
    }
    else if (pIndex == 6) {
      tauxRemise6 = pTauxRemise;
    }
  }
  
  // -- Accesseurs
  
  /**
   * Retourner le code état de l'enregistrement.
   * @return La valeur du code.
   */
  public Integer getEtatEnregistrement() {
    return etatEnregistrement;
  }
  
  /**
   * Modifier le code état de l'enregistrement.
   * @param pEtatEnregistrement L'état.
   */
  public void setEtatEnregistrement(Integer pEtatEnregistrement) {
    etatEnregistrement = pEtatEnregistrement;
  }
  
  /**
   * Retourner l'identifiant de la ligne de ventes.
   * @return L'identifiant.
   */
  public IdLigneVente getIdLigneVente() {
    return idLigneVente;
  }
  
  /**
   * Modifier l'identifiant de la ligne de ventes.
   * @param pIdLigneVente L'identifiant.
   */
  public void setIdLigneVente(IdLigneVente pIdLigneVente) {
    idLigneVente = pIdLigneVente;
  }
  
  /**
   * Retourner l'ordre d'application de la condition de ventes.
   * @return Le numéro.
   */
  public Integer getOrdreApplication() {
    return ordreApplication;
  }
  
  /**
   * Modifier l'ordre d'application de la condition de ventes.
   * @param pOrdreApplication L'ordre.
   */
  public void setOrdreApplication(Integer pOrdreApplication) {
    ordreApplication = pOrdreApplication;
  }
  
  /**
   * Retourner l'identifiant rattachement client.
   * @return L'identifiant.
   */
  public IdRattachementClient getIdRattachementClient() {
    return idRattachementClient;
  }
  
  /**
   * Retourner l'identifiant rattachement article.
   * @return L'identifiant
   */
  public IdRattachementArticle getIdRattachementArticle() {
    return idRattachementArticle;
  }
  
  /**
   * Retourner la catégorie de la condition de ventes.
   * @return La catgorie.
   */
  public EnumCategorieConditionVente getCategorie() {
    return categorie;
  }
  
  /**
   * Retourner le type de la condition de ventes.
   * @return Le type.
   */
  public EnumTypeConditionVente getType() {
    return type;
  }
  
  /**
   * Retourner la valeur pouvant contenir différentes informations suivant le type de la condition de ventes.
   * @return La valeur.
   */
  public BigDecimal getValeur() {
    return valeur;
  }
  
  /**
   * Retourner le taux de remise 1.
   * @return La taux de remise.
   */
  public BigPercentage getTauxRemise1() {
    return tauxRemise1;
  }
  
  /**
   * Retourner le taux de remise 2.
   * @return La taux de remise.
   */
  public BigPercentage getTauxRemise2() {
    return tauxRemise2;
  }
  
  /**
   * Retourner le taux de remise 3.
   * @return La taux de remise.
   */
  public BigPercentage getTauxRemise3() {
    return tauxRemise3;
  }
  
  /**
   * Retourner le taux de remise 4.
   * @return La taux de remise.
   */
  public BigPercentage getTauxRemise4() {
    return tauxRemise4;
  }
  
  /**
   * Retourner le taux de remise 5.
   * @return La taux de remise.
   */
  public BigPercentage getTauxRemise5() {
    return tauxRemise5;
  }
  
  /**
   * Retourner le taux de remise 6.
   * @return La taux de remise.
   */
  public BigPercentage getTauxRemise6() {
    return tauxRemise6;
  }
  
  /**
   * Retourner le coefficient de remise.
   * @return Le coefficent.
   */
  public BigDecimal getCoefficient() {
    return coefficient;
  }
  
  /**
   * Retourner la date de début de validité.
   * @return La date.
   */
  public Date getDateDebutValidite() {
    return dateDebutValidite;
  }
  
  /**
   * Retourner la date de fin de validité.
   * @return La date.
   */
  public Date getDateFinValidite() {
    return dateFinValidite;
  }
  
  /**
   * Retourner l'identifiant de la formule de prix.
   * @return L'identifiant.
   */
  public IdFormulePrix getIdFormulePrix() {
    return idFormulePrix;
  }
  
  /**
   * Retourner la différence entre le prix unitaire standard et le prix unitaire de la condition de vente.
   * @return La différence.
   */
  public BigDecimal getDifferencePrixUnitaire() {
    return differencePrixUnitaire;
  }
  
  /**
   * Modifier la différence entre le prix unitaire standard et le prix unitaire de la condition de vente.
   * @param pDifferencePrixUnitaire La différence.
   */
  public void setDifferencePrixUnitaire(BigDecimal pDifferencePrixUnitaire) {
    differencePrixUnitaire = pDifferencePrixUnitaire;
  }
  
  /**
   * Retourner si la condition de ventes est cumulative.
   * @return true=Condition cumulative, false=Condition non cumulative.
   */
  public boolean isConditionVenteCumulative() {
    if (conditionVenteCumulative != null && conditionVenteCumulative.booleanValue()) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourner si la condition de ventes est promotionnelle.
   * @return true=Condition promotionnelle, false=Condition non promotionnelle.
   */
  public boolean isConditionVentePromotionnelle() {
    if (conditionVentePromotionnelle != null && conditionVentePromotionnelle.booleanValue()) {
      return true;
    }
    return false;
  }
  
  /**
   * Modifier si une condition de ventes est promotionnelle.
   * @param pConditionVentePromotionnelle true=Condition promotionnelle, false=Condition non promotionnelle.
   */
  public void setConditionVentePromotionnelle(Boolean pConditionVentePromotionnelle) {
    conditionVentePromotionnelle = pConditionVentePromotionnelle;
  }
}
