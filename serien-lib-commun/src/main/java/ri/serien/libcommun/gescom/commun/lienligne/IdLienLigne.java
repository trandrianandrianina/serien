/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.lienligne;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'une ligne liée à une autre ligne dans un document de ventes ou d'achats.
 * 
 * L'identifiant est composé de l'identifiant de l'établissement, du code entete, numéro et suffixe du document, du numéro de ligne dans
 * le document et d'un indicateur pour savoir si on a à faire à un document de vente ou un document d'achat.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdLienLigne extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_ENTETE = 1;
  public static final int LONGUEUR_NUMERO = 6;
  public static final int LONGUEUR_SUFFIXE = 1;
  public static final int LONGUEUR_NUMERO_LIGNE = 4;
  
  // Variables
  // Ligne d'origine
  private final Character codeEnteteOrigine;
  private final Integer numeroDocumentOrigine;
  private final Integer suffixeDocumentOrigine;
  private final Integer numeroFactureOrigine;
  private Integer numeroLigneOrigine;
  private final EnumTypeLienLigne typeOrigine;
  // Ligne liée
  private final Character codeEnteteLien;
  private final Integer numeroDocumentLien;
  private final Integer suffixeDocumentLien;
  private final Integer numeroFactureLien;
  private Integer numeroLigneLien;
  private final EnumTypeLienLigne typeLien;
  private final Boolean ligneSoldee;
  
  /**
   * Constructeur avec l'identifiant document et le numéro de ligne.
   */
  private IdLienLigne(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissementOrigine, EnumTypeLienLigne pTypeOrigine,
      Character pCodeEnteteOrigine, Integer pNumeroOrigine, Integer pSuffixeOrigine, Integer pNumeroFactureOrigine,
      Integer pNumeroLigneOrigine, IdEtablissement pIdEtablissement, EnumTypeLienLigne pType, Character pCodeEntete, Integer pNumero,
      Integer pSuffixe, Integer pNumeroFacture, Integer pNumeroLigne, Boolean pLigneSoldee) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    codeEnteteOrigine = pCodeEnteteOrigine;
    numeroDocumentOrigine = pNumeroOrigine;
    suffixeDocumentOrigine = pSuffixeOrigine;
    numeroFactureOrigine = pNumeroFactureOrigine;
    numeroLigneOrigine = controlerNumeroLigne(pNumeroLigneOrigine);
    typeOrigine = pTypeOrigine;
    codeEnteteLien = pCodeEntete;
    numeroDocumentLien = pNumero;
    suffixeDocumentLien = pSuffixe;
    numeroFactureLien = pNumeroFacture;
    numeroLigneLien = controlerNumeroLigne(pNumeroLigne);
    typeLien = pType;
    ligneSoldee = pLigneSoldee;
  }
  
  /**
   * Créer un identifiant à partir d'un identifiant de document de vente et le numéro de ligne.
   */
  public static IdLienLigne getInstanceVente(IdEtablissement pIdEtablissementOrigine, EnumTypeLienLigne pTypeOrigine,
      Character pCodeEnteteOrigine, Integer pNumeroOrigine, Integer pSuffixeOrigine, Integer pNumeroFactureOrigine,
      Integer pNumeroLigneOrigine, IdDocumentVente pIdDocument, Integer pNumeroLigne, Boolean pLigneSoldee) {
    
    if (pNumeroFactureOrigine != null && pNumeroFactureOrigine > 0) {
      pCodeEnteteOrigine = EnumCodeEnteteDocumentVente.FACTURE_REGROUPEE.getCode();
    }
    int numero = 0;
    int suffixe = 0;
    int numeroFacture = 0;
    Character codeEntete = EnumCodeEnteteDocumentVente.FACTURE_REGROUPEE.getCode();
    if (pIdDocument.getNumeroFacture() != null && pIdDocument.getNumeroFacture() > 0) {
      numeroFacture = pIdDocument.getNumeroFacture();
    }
    else {
      codeEntete = pIdDocument.getCodeEntete().getCode();
    }
    if (pIdDocument.getNumero() != null && pIdDocument.getNumero() > 0) {
      numero = pIdDocument.getNumero();
      suffixe = pIdDocument.getSuffixe();
    }
    
    return new IdLienLigne(EnumEtatObjetMetier.MODIFIE, pIdEtablissementOrigine, pTypeOrigine, pCodeEnteteOrigine, pNumeroOrigine,
        pSuffixeOrigine, pNumeroFactureOrigine, pNumeroLigneOrigine, pIdDocument.getIdEtablissement(), EnumTypeLienLigne.LIEN_LIGNE_VENTE,
        codeEntete, numero, suffixe, numeroFacture, pNumeroLigne, pLigneSoldee);
    
  }
  
  /**
   * Créer un identifiant à partir d'un identifiant de document d'achat et le numéro de ligne.
   */
  public static IdLienLigne getInstanceAchat(IdEtablissement pIdEtablissementOrigine, EnumTypeLienLigne pTypeOrigine,
      Character pCodeEnteteOrigine, Integer pNumeroOrigine, Integer pSuffixeOrigine, Integer pNumeroFactureOrigine,
      Integer pNumeroLigneOrigine, IdDocumentAchat pIdDocument, Integer pNumeroLigne) {
    return new IdLienLigne(EnumEtatObjetMetier.MODIFIE, pIdEtablissementOrigine, pTypeOrigine, pCodeEnteteOrigine, pNumeroOrigine,
        pSuffixeOrigine, pNumeroFactureOrigine, pNumeroLigneOrigine, pIdDocument.getIdEtablissement(), EnumTypeLienLigne.LIEN_LIGNE_ACHAT,
        pIdDocument.getCodeEntete().getCode(), pIdDocument.getNumero(), pIdDocument.getSuffixe(), 0, pNumeroLigne, false);
  }
  
  /**
   * Contrôler la validité du numéro de ligne.
   * Le numéro de ligne est compris entre 0 et 9 999.
   */
  private Integer controlerNumeroLigne(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de la ligne n'est pas renseigné.");
    }
    if (pValeur < 0) {
      throw new MessageErreurException("Le numéro de la ligne est inférieur à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO_LIGNE)) {
      throw new MessageErreurException("Le numéro de ligne est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdLienLigne controlerId(IdLienLigne pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du lien est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du lien n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + codeEnteteLien.hashCode();
    code = 37 * code + numeroDocumentLien.hashCode();
    code = 37 * code + suffixeDocumentLien.hashCode();
    code = 37 * code + numeroFactureLien.hashCode();
    code = 37 * code + numeroLigneLien.hashCode();
    code = 37 * code + typeLien.hashCode();
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdLienLigne)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de lien.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdLienLigne id = (IdLienLigne) pObject;
    return codeEnteteLien.equals(id.codeEnteteLien) && numeroDocumentLien.equals(id.numeroDocumentLien)
        && suffixeDocumentLien.equals(id.suffixeDocumentLien) && numeroLigneLien.equals(id.numeroLigneLien)
        && typeLien.equals(id.typeLien);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdLienLigne)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdLienLigne id = (IdLienLigne) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = codeEnteteOrigine.compareTo(id.codeEnteteOrigine);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numeroDocumentOrigine.compareTo(id.numeroDocumentOrigine);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = suffixeDocumentOrigine.compareTo(id.suffixeDocumentOrigine);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numeroFactureOrigine.compareTo(id.numeroFactureOrigine);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numeroLigneOrigine.compareTo(id.numeroLigneOrigine);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = typeOrigine.compareTo(id.typeOrigine);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numeroDocumentLien.compareTo(id.numeroDocumentLien);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = suffixeDocumentLien.compareTo(id.suffixeDocumentLien);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numeroFactureLien.compareTo(id.numeroFactureLien);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numeroLigneLien.compareTo(id.numeroLigneLien);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = typeLien.compareTo(id.typeLien);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = ligneSoldee.compareTo(id.ligneSoldee);
    if (comparaison != 0) {
      return comparaison;
    }
    return codeEnteteLien.compareTo(id.codeEnteteLien);
  }
  
  @Override
  public String getTexte() {
    switch (typeLien) {
      case LIEN_LIGNE_VENTE:
        if (numeroFactureLien > 0) {
          return "" + EnumCodeEnteteDocumentVente.valueOfByCode(codeEnteteLien) + SEPARATEUR_ID + numeroFactureLien + SEPARATEUR_ID
              + numeroLigneLien;
        }
        else {
          return "" + EnumCodeEnteteDocumentVente.valueOfByCode(codeEnteteLien) + SEPARATEUR_ID + numeroDocumentLien + SEPARATEUR_ID
              + suffixeDocumentLien + SEPARATEUR_ID + numeroLigneLien;
        }
        
      case LIEN_LIGNE_ACHAT:
        return "" + EnumCodeEnteteDocumentAchat.valueOfByCode(codeEnteteLien) + SEPARATEUR_ID + numeroDocumentLien + SEPARATEUR_ID
            + suffixeDocumentLien + SEPARATEUR_ID + numeroLigneLien;
      
      default:
        return "";
    }
  }
  
  // -- Accesseurs
  /**
   * Entête du document contenant la ligne.
   */
  public Character getCodeEnteteOrigine() {
    return codeEnteteOrigine;
  }
  
  /**
   * Numéro du document contenant la ligne.
   * Le numéro du document est compris entre entre 0 et 9 999.
   */
  public Integer getNumeroOrigine() {
    return numeroDocumentOrigine;
  }
  
  /**
   * Suffixe du document contenant la ligne.
   */
  public Integer getSuffixeOrigine() {
    return suffixeDocumentOrigine;
  }
  
  /**
   * Numéro de la facture contenant la ligne.
   */
  public Integer getNumeroFactureOrigine() {
    return numeroFactureOrigine;
  }
  
  /**
   * Numéro de ligne au sein du document.
   */
  public Integer getNumeroLigneOrigine() {
    return numeroLigneOrigine;
  }
  
  /**
   * Renvoit si c'est un lien vers un document de vente
   */
  public EnumTypeLienLigne getTypeOrigine() {
    return typeOrigine;
  }
  
  /**
   * Entête du document contenant la ligne.
   */
  public Character getCodeEnteteDocument() {
    return codeEnteteLien;
  }
  
  /**
   * Numéro du document contenant la ligne.
   * Le numéro du document est compris entre entre 0 et 9 999.
   */
  public Integer getNumeroDocument() {
    return numeroDocumentLien;
  }
  
  /**
   * Suffixe du document contenant la ligne.
   */
  public Integer getSuffixeDocument() {
    return suffixeDocumentLien;
  }
  
  /**
   * Numéro de la facture contenant la ligne.
   */
  public Integer getNumeroFacture() {
    return numeroFactureLien;
  }
  
  /**
   * Numéro de ligne au sein du document.
   */
  public Integer getNumeroLigne() {
    return numeroLigneLien;
  }
  
  /**
   * Renvoit si c'est un lien vers un document de vente
   */
  public EnumTypeLienLigne getType() {
    return typeLien;
  }
  
  /**
   * Renvoit si la ligne de commande est soldée, c'est à dire qu'elle a été extraite sans reliquat.
   */
  public Boolean isLigneSoldee() {
    return ligneSoldee;
  }
  
}
