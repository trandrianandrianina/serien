/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.avoir;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un chantier.
 * 
 * L'identifiant est composé du code établissement, du type de document chantier, du numéro du document et du suffixe à 0.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdAvoir extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_ENTETE = 1;
  public static final int LONGUEUR_NUMERO = 6;
  public static final int LONGUEUR_SUFFIXE = 1;
  
  // Variables
  private final IdClient idClient;
  // Ces 3 champs ne doivent pas être remplacés par IdDocument seul le code, le numéro et le suffixe sont stockés en base (PGVMAVRM)
  private final EnumCodeEnteteDocumentVente codeEntete;
  private final Integer numero;
  private final Integer suffixe;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdAvoir(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, IdClient pIdClient,
      EnumCodeEnteteDocumentVente pCodeEntete, Integer pNumero, Integer pSuffixe) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    idClient = pIdClient;
    codeEntete = controlerCodeEnteteDocumentVente(pCodeEntete);
    numero = controlerNumero(pNumero);
    suffixe = controlerSuffixe(pSuffixe);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   * Le numéro et le suffixe ne sont pas à fournir. Ils sont renseignés avec la valeur 0 dans ce cas.
   */
  private IdAvoir(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    idClient = null;
    codeEntete = null;
    numero = null;
    suffixe = null;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du code entête du document.
   */
  private static EnumCodeEnteteDocumentVente controlerCodeEnteteDocumentVente(EnumCodeEnteteDocumentVente pValeur) {
    // Renseigner l'indicatif tiers
    if (pValeur == null) {
      throw new MessageErreurException("Le code entête de la ligne de vente du document n'est pas renseigné.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du numéro du document.
   * Il doit être supérieur à zéro et doit comporter au maximum 6 chiffres (entre 1 et 999 999).
   */
  private Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de la ligne de vente du document n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro de la ligne de vente du document est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro de la ligne de vente du document est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du suffixe du document.
   * Il doit être supérieur ou égal à zéro et doit comporter au maximum 1 chiffre (entre 0 et 9).
   */
  private Integer controlerSuffixe(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le suffixe de la ligne de vente du document n'est pas renseigné.");
    }
    if (pValeur < 0 || pValeur > 9) {
      throw new MessageErreurException("Le suffixe de la ligne de vente du document soit être compris entre 0 et 9 inclus.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_SUFFIXE)) {
      throw new MessageErreurException("Le suffixe de la ligne de vente du document est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  // -- Méthodes publiques
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdAvoir getInstance(IdEtablissement pIdEtablissement, IdClient pIdClient, EnumCodeEnteteDocumentVente pCodeEntete,
      Integer pNumero, Integer pSuffixe) {
    return new IdAvoir(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pIdClient, pCodeEntete, pNumero, pSuffixe);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdAvoir getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdAvoir(pIdEtablissement);
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdAvoir controlerId(IdAvoir pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de l'avoir est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de l'avoir n'existe pas dans la base de données : " + pId.toString());
    }
    return pId;
  }
  
  // -- Méthodes publiques
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getCodeEtablissement().hashCode();
    code = 37 * code + idClient.getNumero().hashCode();
    code = 37 * code + idClient.getSuffixe().hashCode();
    code = 37 * code + codeEntete.hashCode();
    code = 37 * code + numero.hashCode();
    code = 37 * code + suffixe.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdAvoir)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants d'avoirs.");
    }
    IdAvoir id = (IdAvoir) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && idClient.equals(id.getIdClient())
        && codeEntete.equals(id.getCodeEntete()) && numero.equals(id.getNumero()) && suffixe.equals(id.getSuffixe());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdAvoir)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdAvoir id = (IdAvoir) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idClient.compareTo(id.idClient);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = codeEntete.compareTo(id.codeEntete);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numero.compareTo(id.numero);
    if (comparaison != 0) {
      return comparaison;
    }
    return suffixe.compareTo(id.suffixe);
  }
  
  @Override
  public String getTexte() {
    return "" + numero + SEPARATEUR_ID + suffixe;
  }
  
  // -- Accesseurs
  
  public IdClient getIdClient() {
    return idClient;
  }
  
  public EnumCodeEnteteDocumentVente getCodeEntete() {
    return codeEntete;
  }
  
  public Integer getNumero() {
    return numero;
  }
  
  public Integer getSuffixe() {
    return suffixe;
  }
  
}
