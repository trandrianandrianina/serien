/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.boncour;

import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

/**
 * Bon de cour.
 * 
 * Un bon de cour est un document manuscrit remis par un magasinier à un client lorsque celui-ci enlève directement des marchandises
 * dans le parc matériaux.
 * Ce bon est numéroté de manière unique. Il ne peut pas y avoir de doublons.
 * La marchandise délivrée avec un bon de cour est enlevée par le client avant qu'un bon de ventes ou une facture de ventes n'ait été
 * émis.
 * Le bon de cour intervient pour saisir le document de ventes adéquat après coup. Ce fonctionnement permet de fluidifier l'enlèvement
 * de marchandises par les clients.
 * 
 * Pour plus de précisions, voir la COM-965 pour le client Allot.
 *
 */
public class BonCour extends AbstractClasseMetier<IdBonCour> {
  // Variables
  EnumEtatBonCour etatBonCour = null;
  IdDocumentVente idDocumentGenere = null;
  Integer numeroLigne = null;
  Date dateDelivranceClient = null;
  
  /**
   * Constructeur.
   */
  public BonCour(IdBonCour pIdBonCour) {
    super(pIdBonCour);
  }
  
  // -- Méthodes publiques
  
  /**
   * Peut on annuler ce bon de cour ?
   */
  public boolean isAnnulable() {
    if (etatBonCour == null) {
      return false;
    }
    return etatBonCour.equals(EnumEtatBonCour.MANQUANT);
  }
  
  /**
   * Peut on réactiver ce bon de cour ?
   */
  public boolean isReactivable() {
    if (etatBonCour == null) {
      return false;
    }
    return etatBonCour.equals(EnumEtatBonCour.ANNULE);
  }
  
  @Override
  public String getTexte() {
    return id.getNumero() + "";
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public BonCour clone() throws CloneNotSupportedException {
    BonCour o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (BonCour) super.clone();
      o.setEtatBonCour(etatBonCour);
      o.setIdDocumentGenere(idDocumentGenere);
      o.setDateDelivranceClient(dateDelivranceClient);
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Accesseurs
  
  /**
   * Etat du bon de cour
   */
  public EnumEtatBonCour getEtatBonCour() {
    return etatBonCour;
  }
  
  /**
   * Etat du bon de cour
   */
  public void setEtatBonCour(EnumEtatBonCour pEtatBonCour) {
    etatBonCour = pEtatBonCour;
  }
  
  /**
   * Identifiant du document dans lequel a été saisi le bon de cour
   */
  public IdDocumentVente getIdDocumentGenere() {
    return idDocumentGenere;
  }
  
  /**
   * Identifiant du document dans lequel a été saisi le bon de cour
   */
  public void setIdDocumentGenere(IdDocumentVente pIdDocumentGenere) {
    idDocumentGenere = pIdDocumentGenere;
  }
  
  /**
   * Numéro de ligne du document de vente dans lequel a été saisi le bon de cour
   */
  public int getNumeroLigne() {
    if (numeroLigne == null) {
      return 10;
    }
    return numeroLigne;
  }
  
  /**
   * Numéro de ligne du document de vente dans lequel a été saisi le bon de cour
   */
  public void setNumeroLigne(Integer numeroLigne) {
    this.numeroLigne = numeroLigne;
  }
  
  /**
   * Date à laquelle le bon de cour a été délivré au client
   */
  public Date getDateDelivranceClient() {
    return dateDelivranceClient;
  }
  
  /**
   * Date à laquelle le bon de cour a été délivré au client
   */
  public void setDateDelivranceClient(Date pDateDelivranceClient) {
    dateDelivranceClient = pDateDelivranceClient;
  }
  
}
