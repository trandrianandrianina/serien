/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.io.Serializable;

import ri.serien.libcommun.exploitation.documentstocke.EnumTypeDocumentStocke;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.gescom.commun.document.ModeEdition;

public class OptionsEditionVente implements Serializable {
  // Constantes
  public static final String DOCUMENT_DEVIS = "DEV";
  public static final String DOCUMENT_COMMANDE = "VAL";
  public static final String DOCUMENT_BON = "EXP";
  public static final String DOCUMENT_FACTURE = "FAC";
  
  public static final int OPTION_AUCUNE = 0;
  public static final int OPTION_PROFORMA = 2;
  
  public static final String COMMANDE_SANS_TOTAUX = "N";
  public static final String COMMANDE_AVEC_TOTAUX = "B";
  public static final String COMMANDE_SANS_PIED = "L";
  public static final String COMMANDE_ETAT_PREPARATION_COMMANDE = "M";// "X";
  public static final String COMMANDE_BON_PREPARATION = "E";
  public static final String COMMANDE_ETIQUETTES_ARTICLES = "e";
  public static final String COMMANDE_FACTURE_PROFORMA_FAX = "O";
  public static final String COMMANDE_MAIL = "@";
  
  public static final String BON_SANS_TOTAUX = "L";
  public static final String BON_AVEC_TOTAUX = "B";
  public static final String BON_NON_CHIFFRE = "N";
  public static final String BON_MAIL = "@";
  
  public static final String FACTURE_CLASSIQUE = "F";
  public static final String FACTURE_BON_EXPEDITION_CHIFFRE = "1";
  public static final String FACTURE_BON_EXPEDITION_NON_CHIFFRE = "2";
  public static final String FACTURE_OPTION_CLIENT = "E";
  public static final String FACTURE_MAIL = "@";
  public static final String TICKET_DE_CAISSE = "T";
  
  // Variables
  private EnumTypeEditionDevis typeEditionDevis = EnumTypeEditionDevis.DEVIS_AVEC_TOTAUX;
  private EnumTypeEditionFacture typeEditionFacture = EnumTypeEditionFacture.EDITION_FACTURE;
  private EnumTypeEdition editionChiffree = EnumTypeEdition.EDITION_CHIFFREE;
  private int editionOption = OPTION_AUCUNE;
  private String codeValidation = "";
  private String codeEdition = "";
  private ModeEdition modeEdition = null;
  private boolean editionSupplementaireBonDePreparation = false;
  
  // Variables utilisée pour NewSim
  private IdMail idMail = null;
  private IdMail idFaxMail = null;
  private boolean stockageBaseDocumentaire = false;
  private EnumTypeDocumentStocke typeDocumentStocke = EnumTypeDocumentStocke.NON_DEFINI;
  // Utilisé pour les tables de liens pour les documents stockés
  private String indicatifDocument = null;
  private String indicatifClient = null;
  
  // -- Méthodes publiques
  
  /**
   * Efface les variables pour NewSim.
   */
  public void effacerVariableNewSim() {
    idMail = null;
    idFaxMail = null;
    stockageBaseDocumentaire = false;
    typeDocumentStocke = EnumTypeDocumentStocke.NON_DEFINI;
    indicatifDocument = null;
    indicatifClient = null;
  }
  
  /**
   * Initialise le code d'édition en fonction de différents paramètres.
   */
  public void initCodeEdition(EnumTypeDocumentVente pTypeDocument, String pCodeValidation, ModeEdition pModeEdition) {
    modeEdition = pModeEdition;
    
    // On vérifie le code de validation utilisé lors de la validation
    if (pCodeValidation.equals("COR") || pCodeValidation.equals("ATT")) {
      switch (pTypeDocument) {
        case DEVIS:
          codeValidation = DOCUMENT_DEVIS;
          break;
        case COMMANDE:
          codeValidation = DOCUMENT_COMMANDE;
          break;
        case BON:
          codeValidation = DOCUMENT_BON;
          break;
        case FACTURE:
          codeValidation = DOCUMENT_FACTURE;
          break;
        default:
          codeValidation = pCodeValidation;
      }
    }
    else {
      codeValidation = pCodeValidation;
    }
    
    // Initialisation du code édition en fonction du type du document
    if (codeValidation.equals(DOCUMENT_DEVIS)) {
      editerDevisNormal();
    }
    else if (codeValidation.equals(DOCUMENT_COMMANDE)) {
      editerCommandeNormale();
    }
    else if (codeValidation.equals(DOCUMENT_BON)) {
      editerBonNormal();
    }
    else if (codeValidation.equals(DOCUMENT_FACTURE)) {
      editerFactureNormale();
    }
  }
  
  /**
   * Initialise le code d'édition en fonction de différents paramètres pour les éditions des documents supplémentaires.
   */
  public void initCodeEditionDocumentSupplementaire(EnumTypeDocumentVente pTypeDocument, String pCodeValidation) {
    modeEdition = null;
    
    // Vérification du code de validation utilisé lors de la validation
    if (pCodeValidation.equals("COR") || pCodeValidation.equals("ATT")) {
      switch (pTypeDocument) {
        case DEVIS:
          codeValidation = DOCUMENT_DEVIS;
          break;
        case COMMANDE:
          codeValidation = DOCUMENT_COMMANDE;
          break;
        case BON:
          codeValidation = DOCUMENT_BON;
          break;
        case FACTURE:
          codeValidation = DOCUMENT_FACTURE;
          break;
        default:
          codeValidation = pCodeValidation;
      }
    }
    else {
      codeValidation = pCodeValidation;
    }
    
    // Initialisation du code édition en fonction du type du document
    if (codeValidation.equals(DOCUMENT_DEVIS)) {
      // TODO si besoin
    }
    else if (codeValidation.equals(DOCUMENT_COMMANDE)) {
      editerDocumentSupplementaireAvecCommande();
    }
    else if (codeValidation.equals(DOCUMENT_BON)) {
      // TODO si besoin
    }
    else if (codeValidation.equals(DOCUMENT_FACTURE)) {
      // TODO si besoin
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialisation du code d'édition pour un devis normal.
   */
  private void editerDevisNormal() {
    switch (typeEditionDevis) {
      case DEVIS_SANS_TOTAUX:
        codeEdition = EnumTypeEditionDevis.DEVIS_SANS_TOTAUX.getCode().toString();
        break;
      case FACTURE_PROFORMA:
        codeEdition = EnumTypeEditionDevis.FACTURE_PROFORMA.getCode().toString();
        break;
      
      default:
        codeEdition = EnumTypeEditionDevis.DEVIS_AVEC_TOTAUX.getCode().toString();
        break;
    }
  }
  
  /**
   * Initialisation du code d'édition pour une commande normale.
   */
  private void editerCommandeNormale() {
    switch (editionChiffree) {
      case EDITION_CHIFFREE:
        codeEdition = COMMANDE_AVEC_TOTAUX;
        break;
      
      case EDITION_SANS_TOTAUX:
        codeEdition = COMMANDE_SANS_PIED;
        break;
      
      case EDITION_NON_CHIFFREE:
        codeEdition = COMMANDE_SANS_TOTAUX;
        break;
      
      default:
        codeEdition = COMMANDE_AVEC_TOTAUX;
        break;
    }
  }
  
  /**
   * Initialisation du code d'édition pour un bon normal.
   */
  private void editerBonNormal() {
    switch (editionChiffree) {
      case EDITION_CHIFFREE:
        codeEdition = BON_AVEC_TOTAUX;
        break;
      
      // Note: aprés positionnement des indicateurs servant à l'édition (U4 et U6), le service modifiera l'option EXPL par EXPN
      case EDITION_SANS_TOTAUX:
        codeEdition = BON_SANS_TOTAUX;
        break;
      
      case EDITION_NON_CHIFFREE:
        codeEdition = BON_NON_CHIFFRE;
        break;
      
      default:
        codeEdition = BON_AVEC_TOTAUX;
        break;
    }
  }
  
  /**
   * Initialisation du code d'édition pour une facture normale.
   */
  private void editerFactureNormale() {
    switch (typeEditionFacture) {
      case EDITION_FACTURE:
        codeEdition = EnumTypeEditionFacture.EDITION_FACTURE.getCode().toString();
        break;
      
      case EDITION_TICKET:
        codeEdition = EnumTypeEditionFacture.EDITION_TICKET.getCode().toString();
        break;
      
      default:
        codeEdition = EnumTypeEditionFacture.EDITION_FACTURE.getCode().toString();
        break;
    }
  }
  
  /**
   * Initialisation du code d'édition pour les documents supplémentaires si nécessaire.
   */
  private void editerDocumentSupplementaireAvecCommande() {
    // Edition du bon de préparation
    if (editionSupplementaireBonDePreparation) {
      codeEdition = COMMANDE_ETAT_PREPARATION_COMMANDE;
    }
  }
  
  // -- Accesseurs
  
  public String getCodeValidation() {
    return codeValidation;
  }
  
  public void setCodeValidation(String pCodeValidation) {
    this.codeValidation = pCodeValidation;
  }
  
  public String getCodeEdition() {
    return codeEdition;
  }
  
  public void setCodeEdition(String pCodeEdition) {
    this.codeEdition = pCodeEdition;
  }
  
  public EnumTypeEdition getEditionChiffree() {
    return editionChiffree;
  }
  
  public void setEditionChiffree(EnumTypeEdition pEditionChiffree) {
    editionChiffree = pEditionChiffree;
  }
  
  /**
   * Renvoyer le type d'édition du devis (chiffré, non chiffré ou proforma)
   */
  public EnumTypeEditionDevis getTypeEditionDevis() {
    return typeEditionDevis;
  }
  
  /**
   * Mettre à jour le type d'édition du devis (chiffré, non chiffré ou proforma)
   */
  public void setTypeEditionDevis(EnumTypeEditionDevis pTypeEditionDevis) {
    typeEditionDevis = pTypeEditionDevis;
  }
  
  /**
   * 
   * Retourner le type d'édition de facture (facture ou ticket de caisse).
   * 
   * @return EnumTypeEditionFacture
   */
  public EnumTypeEditionFacture getTypeEditionFacture() {
    return typeEditionFacture;
  }
  
  /**
   * 
   * Modifier le type d'édition de facture (facture ou ticket de caisse).
   * 
   * @param EnumTypeEditionFacture
   */
  public void setTypeEditionFacture(EnumTypeEditionFacture pTypeEditionFacture) {
    typeEditionFacture = pTypeEditionFacture;
  }
  
  public int getEditionOption() {
    return editionOption;
  }
  
  public void setEditionOption(int editionOption) {
    this.editionOption = editionOption;
  }
  
  public ModeEdition getModeEdition() {
    return modeEdition;
  }
  
  public IdMail getIdMail() {
    return idMail;
  }
  
  public void setIdMail(IdMail pIdMail) {
    this.idMail = pIdMail;
  }
  
  public IdMail getIdFaxMail() {
    return idFaxMail;
  }
  
  public void setIdFaxMail(IdMail idFaxMail) {
    this.idFaxMail = idFaxMail;
  }
  
  public boolean isStockageBaseDocumentaire() {
    return stockageBaseDocumentaire;
  }
  
  public void setStockageBaseDocumentaire(boolean pStockageBaseDocumentaire) {
    this.stockageBaseDocumentaire = pStockageBaseDocumentaire;
  }
  
  public EnumTypeDocumentStocke getTypeDocumentStocke() {
    return typeDocumentStocke;
  }
  
  public void setTypeDocumentStocke(EnumTypeDocumentStocke typeDocumentStocke) {
    this.typeDocumentStocke = typeDocumentStocke;
  }
  
  public String getIndicatifDocument() {
    return indicatifDocument;
  }
  
  public void setIndicatifDocument(String indicatifDocument) {
    this.indicatifDocument = indicatifDocument;
  }
  
  public String getIndicatifClient() {
    return indicatifClient;
  }
  
  public void setIndicatifClient(String indicatifClient) {
    this.indicatifClient = indicatifClient;
  }
  
  public boolean isEditionSupplementaireBonDePreparation() {
    return editionSupplementaireBonDePreparation;
  }
  
  public void setEditionSupplementaireBonDePreparation(boolean editionSupplementaireBonDePreparation) {
    this.editionSupplementaireBonDePreparation = editionSupplementaireBonDePreparation;
  }
  
  public boolean isEditionDocumentSupplementaire() {
    if (editionSupplementaireBonDePreparation) {
      return true;
    }
    return false;
  }
  
}
