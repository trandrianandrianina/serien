/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.boncour;

import java.io.Serializable;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

/**
 * Cette classe contient un ensemble de critères qui permettent la recherche de bons de cour.
 */
public class CritereBonCour implements Serializable {
  // Variables
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private IdCarnetBonCour idCarnetBonCour = null;
  private int numeroBonCour = 0;
  private EnumEtatBonCour etatBonCour = null;
  private IdDocumentVente idDocument = null;
  private Date dateRemise = null;
  private IdVendeur idMagasinier = null;
  private boolean ignorerCarnetNonAttribue = true;
  
  // -- Accesseurs
  
  /**
   * Identifiant de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Identifiant de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Identifiant du magasin.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Identifiant du magasin.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  /**
   * Identifiant du carnet de bons de cour.
   */
  public IdCarnetBonCour getIdCarnetBonCour() {
    return idCarnetBonCour;
  }
  
  /**
   * Identifiant du carnet de bons de cour.
   */
  public void setIdCarnetBonCour(IdCarnetBonCour pIdCarnetBonCour) {
    idCarnetBonCour = pIdCarnetBonCour;
  }
  
  /**
   * Numéro de bons de cour.
   */
  public int getNumeroBonCour() {
    return numeroBonCour;
  }
  
  /**
   * Numéro de bons de cour.
   */
  public void setNumeroBonCour(int pNumeroBonCour) {
    numeroBonCour = pNumeroBonCour;
  }
  
  /**
   * Etat du bons de cour.
   */
  public EnumEtatBonCour getEtatBonCour() {
    return etatBonCour;
  }
  
  /**
   * Etat du bons de cour.
   */
  public void setEtatBonCour(EnumEtatBonCour pEtatBonCour) {
    etatBonCour = pEtatBonCour;
  }
  
  /**
   * Identifiant du document de vente associé.
   */
  public IdDocumentVente getIdDocument() {
    return idDocument;
  }
  
  /**
   * Identifiant du document de vente associé.
   */
  public void setIdDocument(IdDocumentVente pIdDocument) {
    idDocument = pIdDocument;
  }
  
  /**
   * Date de remise du bon de cour au client.
   */
  public Date getDateRemise() {
    return dateRemise;
  }
  
  /**
   * Date de remise du bon de cour au client.
   */
  public void setDateRemise(Date pDateRemise) {
    dateRemise = pDateRemise;
  }
  
  /**
   * Magasinier qui détient le carnet de bons de cour.
   */
  public IdVendeur getIdMagasinier() {
    return idMagasinier;
  }
  
  /**
   * Magasinier qui détient le carnet de bons de cour.
   */
  public void setIdMagasinier(IdVendeur pIdMagasinier) {
    idMagasinier = pIdMagasinier;
  }
  
  /**
   * Retourne s'il faut prendre en compte les carnets non attribués.
   */
  public boolean isIgnorerCarnetNonAttribue() {
    return ignorerCarnetNonAttribue;
  }
  
  /**
   * Indique si les carnets doivent être attribués ou non.
   */
  public void setIgnorerCarnetNonAttribue(boolean ignorerCarnetNonAttribue) {
    this.ignorerCarnetNonAttribue = ignorerCarnetNonAttribue;
  }
  
}
