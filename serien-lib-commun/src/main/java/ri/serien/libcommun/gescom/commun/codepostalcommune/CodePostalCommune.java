/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.codepostalcommune;

import java.io.Serializable;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * La collectivité est une classe contenant un code postal et le nom de la ville qui lui est associé.
 * Il sera principalement utiliser dans le composant.
 */
public class CodePostalCommune implements Serializable, Comparable {
  // Constantes
  public static final int LONGUEUR_CODEPOSTAL = 5;
  public static final int LONGUEUR_VILLE = 24;
  
  // Variables
  private String ville = "";
  private String codePostal = "";
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private CodePostalCommune(String pCodePostal, String pVille) {
    super();
    codePostal = controlerCodePostal(pCodePostal);
    ville = controlerVille(pVille);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static CodePostalCommune getInstance(String pCodePostal, String pVille) {
    return new CodePostalCommune(pCodePostal, pVille);
  }
  
  // -- Méthodes publiques
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + ville.hashCode();
    cle = 37 * cle + codePostal.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof CodePostalCommune)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux collectivite.");
    }
    CodePostalCommune collectivite = (CodePostalCommune) pObject;
    return codePostal.equals(collectivite.codePostal) && ville.equals(collectivite.ville);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof CodePostalCommune)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    CodePostalCommune collectivite = (CodePostalCommune) pObject;
    int comparaison = codePostal.compareTo(collectivite.codePostal);
    if (comparaison != 0) {
      return comparaison;
    }
    return ville.compareTo(collectivite.ville);
  }
  
  public String getTexte() {
    return ville + codePostal;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du code postal.
   * Le code postal est une chaîne numérique de 5 caractères.
   */
  private static String controlerCodePostal(String pCodePostal) {
    if (pCodePostal == null) {
      throw new MessageErreurException("Le code postal n'est pas renseigné.");
    }
    pCodePostal = pCodePostal.trim();
    
    // Permet de savoir la taille du code postal
    int tailleCodePostal = pCodePostal.length();
    
    // Rajoute un 0 devant les codes postaux afin qu'il soit à la bonne taille
    if (tailleCodePostal < LONGUEUR_CODEPOSTAL && tailleCodePostal > 0) {
      for (int i = 0 + tailleCodePostal; i < LONGUEUR_CODEPOSTAL; i++) {
        pCodePostal = '0' + pCodePostal;
      }
    }
    
    return pCodePostal;
  }
  
  /**
   * Contrôler la validité du nom de la ville.
   * La ville est une chaîne de 24 caractères.
   */
  private static String controlerVille(String pVille) {
    if (pVille == null) {
      throw new MessageErreurException("La ville n'est pas renseigné.");
    }
    
    // vérification de la taille de la ville
    if (pVille.length() > LONGUEUR_VILLE) {
      throw new MessageErreurException("La ville doit comporter " + LONGUEUR_VILLE + " caractères : " + pVille.length());
    }
    return Constantes.normerTexte(pVille);
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le code postal
   */
  public String getCodePostal() {
    return codePostal;
  }
  
  /**
   * Modifie le code postal
   */
  public void setCodePostal(String pCodePostal) {
    codePostal = pCodePostal;
  }
  
  /**
   * Retourne la ville
   */
  public String getVille() {
    return ville;
  }
  
  /**
   * Modifie la ville
   */
  public void setVille(String pNom) {
    ville = pNom.toUpperCase();
  }
  
  /**
   * Retourne si le CodePostalCommune est complet, c'est à dire s'il est composé d'un code postal et d'une ville.
   */
  public boolean isComplet() {
    if (!codePostal.isEmpty() && !ville.isEmpty()) {
      return true;
    }
    else {
      return false;
    }
  }
  
  /**
   * Formate le code postal en Integer.
   */
  public int getCodePostalFormate() {
    if (codePostal.isEmpty()) {
      return 0;
    }
    return Integer.parseInt(codePostal);
  }
  
}
