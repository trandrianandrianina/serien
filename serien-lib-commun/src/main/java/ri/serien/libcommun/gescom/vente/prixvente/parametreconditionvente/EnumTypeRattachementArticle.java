/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des types de rattachements articles des conditions de vente.
 */
public enum EnumTypeRattachementArticle {
  ARTICLE('A', "Article", true, 0),
  REGROUPEMENT_ACHAT('1', "Regroupement achat", true, 1),
  REGROUPEMENT_FOURNISSEUR('2', "Regroupement fournisseur", true, 2),
  REGROUPEMENT_CNV('R', "Regroupement CNV (Ensemble d'articles)", true, 3),
  TARIF('T', "Tarif", true, 4),
  SOUS_FAMILLE('S', "Sous famille", true, 5),
  FAMILLE('F', "Famille", true, 6),
  GROUPE('G', "Groupe", true, 7),
  EMBOITEE_OU_GLOBALE('*', "Emboitée ou globale", true, 8),
  // Les suivants sont à ignorer
  TROIS('3', "", false, null),
  QUATRE('4', "", false, null),
  CINQ('5', "", false, null),
  NUMERO_LIGNE_OU_RANG('L', "Numéro de ligne ou rang", false, null),
  A_IGNORER('V', "A ignorer", false, null);
  
  private final Character code;
  private final String libelle;
  private final boolean actif;
  private final Integer priorite;
  
  /**
   * Constructeur.
   */
  EnumTypeRattachementArticle(Character pCode, String pLibelle, boolean pActif, Integer pPriorite) {
    code = pCode;
    libelle = pLibelle;
    actif = pActif;
    priorite = pPriorite;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   * @return Le code.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   * @return Le libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Indique si le type de rattachement est actif.
   * C'est à dire à prendre en compte dans le calcul du prix.
   * @return true=Rattachement actif, false=Rattachement non actif.
   */
  public boolean isActif() {
    return actif;
  }
  
  /**
   * Retourner un entier lié au type de rattachement.
   * Cela permet d'effectuer par la suite un tri sur les conditions de ventes d'un même rattachement client et de ne prendre que celle
   * dont le rattachement article a la valeur de l'entier le plus faible.
   * @return La valeur.
   */
  public Integer getPriorite() {
    return priorite;
  }
  
  /**
   * Retourne le texte représentant cette instance.
   * @return Le texte.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeRattachementArticle valueOfByCode(Character pCode) {
    for (EnumTypeRattachementArticle value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de rattachement de la condition de vente est invalide : " + pCode);
  }
  
}
