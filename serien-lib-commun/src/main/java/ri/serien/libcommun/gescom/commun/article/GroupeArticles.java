/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.io.Serializable;

/**
 * Cette classe définit un groupe de familles du catalogue article de Série N
 * Le groupe de famille est défini sur un caractère (le premier du code famille)
 */
public class GroupeArticles implements Serializable {
  // TODO La notion de "tous" est à définir avec Thierry
  public static final String TOUS_LES_GROUPES = "*";
  private String code = null;
  private String libelle = null;

  public GroupeArticles(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }

  @Override
  public boolean equals(Object object) {
    // Tester si l'objet est nous-même (optimisation)
    if (object == this) {
      return true;
    }

    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(object instanceof GroupeArticles)) {
      return false;
    }

    // Comparer les valeurs internes en commençant par les plus discriminantes
    GroupeArticles groupeArticles = (GroupeArticles) object;
    return code == groupeArticles.code;
  }

  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();

    return cle;
  }

  @Override
  public String toString() {
    if (code != null && code.equals(TOUS_LES_GROUPES)) {
      return libelle;
    }
    else {
      return code + " - " + libelle;
    }
  }

  /**
   * On vérifie que le groupe en question est bien un groupe et non la sélection de tous les groupe
   */
  public boolean isUnGroupe() {
    return !code.equals(TOUS_LES_GROUPES);
  }

  public String getCode() {
    return code;
  }

  public String getLibelle() {
    return libelle;
  }
}
