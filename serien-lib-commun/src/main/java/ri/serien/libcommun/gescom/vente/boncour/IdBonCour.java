/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.boncour;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un bon de cour.
 * 
 * Un bon de cour est un document manuscrit remis par un magasinier à un client lorsque celui-ci enlève directement des marchandises
 * dans le parc matériaux.
 * Ce bon est numéroté de manière unique. Il ne peut pas y avoir de doublons.
 * La marchandise délivrée avec un bon de cour est enlevée par le client avant qu'un bon de ventes ou une facture de ventes n'ait été
 * émis.
 * Le bon de cour intervient pour saisir le document de ventes adéquat après coup. Ce fonctionnement permet de fluidifier l'enlèvement
 * de marchandises par les clients.
 * 
 * Pour plus de précisions, voir la COM-965 pour le client Allot.
 *
 * L'identifiant est composé de l'identifiant du carnet de bons de cour et du numéro du bon de cour.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdBonCour extends AbstractId {
  // Constantes
  public static final int LONGUEUR_NUMERO = 8;
  
  // Variables
  private final IdCarnetBonCour idCarnetBonCour;
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdBonCour(EnumEtatObjetMetier pEnumEtatObjetMetier, IdCarnetBonCour pIdCarnetBonCour, Integer pNumero) {
    super(pEnumEtatObjetMetier);
    idCarnetBonCour = IdCarnetBonCour.controlerId(pIdCarnetBonCour, isExistant());
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   * Le numéro et n'est pas à fournir. Il est renseigné avec la valeur 0 dans ce cas.
   */
  private IdBonCour(IdCarnetBonCour pIdCarnetBonCour) {
    super(EnumEtatObjetMetier.CREE);
    idCarnetBonCour = IdCarnetBonCour.controlerId(pIdCarnetBonCour, true);
    numero = 0;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdBonCour getInstance(IdCarnetBonCour pIdCarnetBonCour, Integer pNumero) {
    return new IdBonCour(EnumEtatObjetMetier.MODIFIE, pIdCarnetBonCour, pNumero);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.
   * Il sera définit lors de l'insertion en base de données ou par un service RPG.
   */
  public static IdBonCour getInstanceAvecCreationId(IdCarnetBonCour pIdCarnetBonCour) {
    return new IdBonCour(pIdCarnetBonCour);
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données mais à partir de ses informations sous forme éclatée.
   * Cas particuliers de cet identifiant qui est renseigné et non déduit.
   */
  public static IdBonCour getInstanceAvecCreationId(IdCarnetBonCour pIdCarnetBonCour, Integer pNumero) {
    return new IdBonCour(EnumEtatObjetMetier.CREE, pIdCarnetBonCour, pNumero);
  }
  
  /**
   * Contrôler la validité du numéro du bon de cour.
   * Il doit être supérieur à zéro et doit comporter au maximum 8 chiffres (entre 1 et 99 999 999).
   */
  public static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du bon de cour n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du bon de cour est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du bon de cour est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdBonCour controlerId(IdBonCour pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du bon de cour est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du bon de cour n'existe pas dans la base de données : " + pId.toString());
    }
    return pId;
  }
  
  // -- Méthodes publiques
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getIdCarnetBonCour().hashCode();
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdBonCour)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de bon de cours.");
    }
    IdBonCour id = (IdBonCour) pObject;
    return getIdCarnetBonCour().equals(id.getIdCarnetBonCour()) && numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdBonCour)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdBonCour id = (IdBonCour) pObject;
    int comparaison = getIdCarnetBonCour().compareTo(id.getIdCarnetBonCour());
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return "" + numero;
  }
  
  // -- Accesseurs
  
  /**
   * Identifiant du carnet qui contient le présent bon de cour
   */
  public IdCarnetBonCour getIdCarnetBonCour() {
    return idCarnetBonCour;
  }
  
  /**
   * Numéro du bon de cour.
   * Le numéro du bon de cour est compris entre entre 0 et 999 999.
   */
  public Integer getNumero() {
    return numero;
  }
  
}
