/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.commune;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de communes.
 */
public class ListeCommune extends ArrayList<Commune> {
  /**
   * Constructeur.
   */
  public ListeCommune() {
  }
  
  // -- Méthodes publiques
  
  /**
   * Charger toutes les communes.
   */
  public static ListeCommune charger(IdSession pIdSession) {
    CritereCommune criteres = new CritereCommune();
    criteres.setTriCodePostalAscendant(Boolean.TRUE);
    return ManagerServiceParametre.chargerListeCommune(pIdSession, criteres);
  }
  
  /**
   * Charger la liste des communes à partir des CritereCommune
   */
  public static ListeCommune charger(IdSession pIdSession, CritereCommune pCritere) {
    pCritere.setTriCodePostalAscendant(Boolean.TRUE);
    return ManagerServiceParametre.chargerListeCommune(pIdSession, pCritere);
  }
  
  /**
   * Charger la liste des communes à partir de la ville.
   */
  public static ListeCommune chargerPourVille(IdSession pIdSession, String pVille) {
    pVille = Constantes.normerTexte(pVille);
    CritereCommune criteres = new CritereCommune();
    criteres.setTriCodePostalAscendant(Boolean.TRUE);
    int codePostal = Constantes.convertirTexteEnInteger(pVille);
    criteres.setCodePostal(Integer.valueOf(codePostal));
    return ManagerServiceParametre.chargerListeCommune(pIdSession, criteres);
  }
  
  /**
   * Retourner la liste des communes sur un code postal partiel ou entier.
   */
  public ListeCommune filtrerCommuneAvecCodePostal(String pCodePostal) {
    // Controles
    pCodePostal = Constantes.normerTexte(pCodePostal);
    if (pCodePostal.isEmpty() || pCodePostal.length() > Commune.LONGUEUR_CODEPOSTAL) {
      return null;
    }
    int pValeur = 0;
    try {
      pValeur = Integer.parseInt(pCodePostal);
    }
    catch (NumberFormatException e) {
      return null;
    }
    if ((pValeur < 10) || (pValeur > Constantes.valeurMaxZoneNumerique(Commune.LONGUEUR_CODEPOSTAL))) {
      return null;
    }
    // Traitement
    int coef = 1;
    switch (pCodePostal.length()) {
      case 1:
        coef = 10000;
        break;
      case 2:
        coef = 1000;
        break;
      case 3:
        coef = 100;
        break;
      case 4:
        coef = 10;
        break;
    }
    int valeurMin = pValeur * coef;
    int valeurMax = valeurMin + (coef - 1);
    
    ListeCommune listCommuneFiltree = new ListeCommune();
    for (Commune commune : this) {
      if (commune.getCodePostal() < valeurMin) {
        continue;
      }
      if (commune.getCodePostal() <= valeurMax) {
        listCommuneFiltree.add(commune);
      }
      else {
        break;
      }
    }
    return listCommuneFiltree;
  }
  
  /**
   * Retourne la liste des noms des villes.
   */
  public List<String> getListeNomCommune() {
    ArrayList<String> listeNomCommune = new ArrayList<String>();
    for (Commune commune : this) {
      listeNomCommune.add(commune.getNom());
    }
    return listeNomCommune;
  }
  
  /**
   * Retourner une commune à partir de son nom.
   */
  public Commune getCommuneParVille(String pNomCommune) {
    pNomCommune = Constantes.normerTexte(pNomCommune);
    for (Commune commune : this) {
      if (pNomCommune.equalsIgnoreCase(commune.getNom())) {
        return commune;
      }
    }
    return null;
  }
  
  /**
   * Retourner une commune à partir de son code postal.
   */
  public Commune getCommuneParCodePostal(Integer pCodePostal) {
    for (Commune commune : this) {
      if (pCodePostal == commune.getCodePostal()) {
        return commune;
      }
    }
    return null;
  }
  
  /**
   * Retourner une liste de commune à partir de son code postal.
   */
  public ListeCommune getListeCommuneParCodePostal(Integer pCodePostal) {
    ListeCommune listeCommune = new ListeCommune();
    for (Commune commune : this) {
      if (pCodePostal == commune.getCodePostal()) {
        listeCommune.add(commune);
      }
    }
    return listeCommune;
  }
  
  /**
   * Retourner une commune à partir d'un début du nom de la ville.
   */
  public Commune getCommuneParPrefixeNom(String pPrefixeNomCommune) {
    String prefixe = Constantes.normerTexte(pPrefixeNomCommune).toUpperCase();
    for (Commune commune : this) {
      if (commune.getNom().toUpperCase().startsWith(prefixe)) {
        return commune;
      }
    }
    return null;
  }
  
  /**
   * Vérifier si une commune donnéee est dans la liste.
   */
  public boolean isPresent(String pNomCommune) {
    pNomCommune = Constantes.normerTexte(pNomCommune);
    if (pNomCommune.isEmpty()) {
      return false;
    }
    for (Commune commune : this) {
      if (pNomCommune.equalsIgnoreCase(commune.getNom())) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Retourne une ville à partir d'un début du nom de la ville.
   */
  public int retournerIndiceCommune(String pPrefixe) {
    String prefixe = Constantes.normerTexte(pPrefixe).toUpperCase();
    int indice = 0;
    for (Commune commune : this) {
      if (commune.getNom().toUpperCase().startsWith(prefixe)) {
        return indice;
      }
      indice++;
    }
    return 0;
  }
}
