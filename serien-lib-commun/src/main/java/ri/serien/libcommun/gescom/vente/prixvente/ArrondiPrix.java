/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import ri.serien.libcommun.outils.Constantes;

/**
 * Cette classe contient les informations qui décrivent un arrondi pour les prix.
 *
 * L'arrondi se fait sur le nombre de chiffres après la virgule.
 * Seule la variable "origine" peut potentiellement être à null, toutes les autres sont obligatoirement renseignées.
 */
public class ArrondiPrix implements Serializable {
  // Constantes
  public static final int DECIMALE_STANDARD = 2;
  public static final int DECIMALE_MAX = 4;
  
  // Variables
  private int nombreDecimale = DECIMALE_STANDARD;
  private boolean venduParCentaine = false;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Consctructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé.
   * Pour définir un arrondi par défaut.
   */
  private ArrondiPrix() {
  }
  
  /**
   * Constructeur privé.
   * @param pCodeUniteVente Code de l'unité de vente (null autorisé).
   */
  private ArrondiPrix(String pCodeUniteVente) {
    // Déterminer le nombre de décimale
    if (pCodeUniteVente != null) {
      pCodeUniteVente = Constantes.normerTexte(pCodeUniteVente);
      
      // Si le code de l’unité de vente comporte le caractère '*' en deuxième position alors l’article est vendu à la centaine
      // Du coup le prix de base HT et le prix net HT sont présentés avec 4 chiffres après la virgules
      if (pCodeUniteVente.length() == 2 && pCodeUniteVente.charAt(1) == '*') {
        nombreDecimale = DECIMALE_MAX;
        venduParCentaine = true;
      }
    }
  }
  
  /**
   * Créé une instance pour un arrondi en fonction de l'unité de vente.
   * Si l'unité de vente est indéfinie, c'ets l'arrondi par défaut qui est utilisé.
   * 
   * @param pCodeUniteVente Code de l'unité de vente (null autorisé).
   * @return Arrondi prix.
   */
  public static ArrondiPrix getInstance(String pCodeUniteVente) {
    return new ArrondiPrix(pCodeUniteVente);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Applique un arrondi à un prix.
   * 
   * @param pArrondiPrix
   * @return
   */
  public static BigDecimal appliquer(BigDecimal pPrix, ArrondiPrix pArrondiPrix) {
    if (pPrix == null) {
      return null;
    }
    if (pArrondiPrix == null) {
      pArrondiPrix = new ArrondiPrix();
    }
    
    // Si l'article n'est pas vendu par centaines ou si la valeur vaut 0 alors seul l'arrondi est appliqué
    if (!pArrondiPrix.isVenduParCentaine() || pPrix.intValue() == 0) {
      return pPrix.setScale(pArrondiPrix.getNombreDecimale(), RoundingMode.HALF_UP);
    }
    
    // Sinon
    // Corrige le prix de base HT si l’article est vendu par centaine
    pPrix = pPrix.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL64).setScale(pArrondiPrix.getNombreDecimale(), RoundingMode.HALF_UP);
    
    return pPrix.setScale(pArrondiPrix.getNombreDecimale(), RoundingMode.HALF_UP);
  }
  
  /**
   * Applique un arrondi à un prix.
   * 
   * @param pNombreDecimale
   * @return
   */
  public static BigDecimal appliquer(BigDecimal pPrix, int pNombreDecimale) {
    if (pPrix == null) {
      return null;
    }
    if (pNombreDecimale < 0 || pNombreDecimale > DECIMALE_MAX) {
      pNombreDecimale = DECIMALE_STANDARD;
    }
    
    return pPrix.setScale(pNombreDecimale, RoundingMode.HALF_UP);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourne le nombre de décimales.
   * 
   * @return
   */
  public int getNombreDecimale() {
    return nombreDecimale;
  }
  
  /**
   * Retourne si l'article est vendu par centaine.
   * 
   * @return
   */
  public boolean isVenduParCentaine() {
    return venduParCentaine;
  }
}
