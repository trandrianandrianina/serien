/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition;

import java.math.BigDecimal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;

/**
 * Participation aux frais d'expédition.
 * La participation aux frais d'expédition correspond à la personnalisation PE des ventes.
 */
public class ParticipationFraisExpedition extends AbstractClasseMetier<IdParticipationFraisExpedition> {
  // Variables
  private Integer seuil1 = null;
  private Integer seuil2 = null;
  private Integer seuil3 = null;
  private Integer seuil4 = null;
  private Integer seuil5 = null;
  private Integer seuil6 = null;
  private Integer seuil7 = null;
  private Integer seuil8 = null;
  private Integer seuil9 = null;
  private Integer seuil10 = null;
  private BigDecimal pourcentage1 = null;
  private BigDecimal pourcentage2 = null;
  private BigDecimal pourcentage3 = null;
  private BigDecimal pourcentage4 = null;
  private BigDecimal pourcentage5 = null;
  private BigDecimal pourcentage6 = null;
  private BigDecimal pourcentage7 = null;
  private BigDecimal pourcentage8 = null;
  private BigDecimal pourcentage9 = null;
  private BigDecimal pourcentage10 = null;
  private BigDecimal montant1 = null;
  private BigDecimal montant2 = null;
  private BigDecimal montant3 = null;
  private BigDecimal montant4 = null;
  private BigDecimal montant5 = null;
  private BigDecimal montant6 = null;
  private BigDecimal montant7 = null;
  private BigDecimal montant8 = null;
  private BigDecimal montant9 = null;
  private BigDecimal montant10 = null;
  private IdArticle idArticleAssocie = null;
  
  /**
   * Constructeur.
   */
  public ParticipationFraisExpedition(IdParticipationFraisExpedition pIdModeExpedition) {
    super(pIdModeExpedition);
  }
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  /**
   * Retourne le seuil numéro 1
   * 
   * @return Integer
   */
  public Integer getSeuil1() {
    return seuil1;
  }
  
  /**
   * Modifier le seuil numéro 1
   * 
   */
  public void setSeuil1(Integer seuil1) {
    this.seuil1 = seuil1;
  }
  
  /**
   * Retourne le seuil numéro 2
   * 
   * @return Integer
   */
  public Integer getSeuil2() {
    return seuil2;
  }
  
  /**
   * Modifier le seuil numéro 2
   * 
   */
  public void setSeuil2(Integer seuil2) {
    this.seuil2 = seuil2;
  }
  
  /**
   * Retourne le seuil numéro 3
   * 
   * @return Integer
   */
  public Integer getSeuil3() {
    return seuil3;
  }
  
  /**
   * Modifier le seuil numéro 3
   * 
   */
  public void setSeuil3(Integer seuil3) {
    this.seuil3 = seuil3;
  }
  
  /**
   * Retourne le seuil numéro 4
   * 
   * @return Integer
   */
  public Integer getSeuil4() {
    return seuil4;
  }
  
  /**
   * Modifier le seuil numéro 4
   * 
   */
  public void setSeuil4(Integer seuil4) {
    this.seuil4 = seuil4;
  }
  
  /**
   * Retourne le seuil numéro 5
   * 
   * @return Integer
   */
  public Integer getSeuil5() {
    return seuil5;
  }
  
  /**
   * Modifier le seuil numéro 5
   * 
   */
  public void setSeuil5(Integer seuil5) {
    this.seuil5 = seuil5;
  }
  
  /**
   * Retourne le seuil numéro 6
   * 
   * @return Integer
   */
  public Integer getSeuil6() {
    return seuil6;
  }
  
  /**
   * Modifier le seuil numéro 6
   * 
   */
  public void setSeuil6(Integer seuil6) {
    this.seuil6 = seuil6;
  }
  
  /**
   * Retourne le seuil numéro 7
   * 
   * @return Integer
   */
  public Integer getSeuil7() {
    return seuil7;
  }
  
  /**
   * Modifier le seuil numéro 7
   * 
   */
  public void setSeuil7(Integer seuil7) {
    this.seuil7 = seuil7;
  }
  
  /**
   * Retourne le seuil numéro 8
   * 
   * @return Integer
   */
  public Integer getSeuil8() {
    return seuil8;
  }
  
  /**
   * Modifier le seuil numéro 8
   * 
   */
  public void setSeuil8(Integer seuil8) {
    this.seuil8 = seuil8;
  }
  
  /**
   * Retourne le seuil numéro 9
   * 
   * @return Integer
   */
  public Integer getSeuil9() {
    return seuil9;
  }
  
  /**
   * Modifier le seuil numéro 9
   * 
   */
  public void setSeuil9(Integer seuil9) {
    this.seuil9 = seuil9;
  }
  
  /**
   * Retourne le seuil numéro 10
   * 
   * @return Integer
   */
  public Integer getSeuil10() {
    return seuil10;
  }
  
  /**
   * Modifier le seuil numéro 10
   * 
   */
  public void setSeuil10(Integer seuil10) {
    this.seuil10 = seuil10;
  }
  
  /**
   * Retourne le pourcentage numéro 1
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage1() {
    return pourcentage1;
  }
  
  /**
   * Modifier le pourcentage numéro 1
   * 
   */
  public void setPourcentage1(BigDecimal pourcentage1) {
    this.pourcentage1 = pourcentage1;
  }
  
  /**
   * Retourne le pourcentage numéro 2
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage2() {
    return pourcentage2;
  }
  
  /**
   * Modifier le pourcentage numéro 2
   * 
   */
  public void setPourcentage2(BigDecimal pourcentage2) {
    this.pourcentage2 = pourcentage2;
  }
  
  /**
   * Retourne le pourcentage numéro 3
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage3() {
    return pourcentage3;
  }
  
  /**
   * Modifier le pourcentage numéro 3
   * 
   */
  public void setPourcentage3(BigDecimal pourcentage3) {
    this.pourcentage3 = pourcentage3;
  }
  
  /**
   * Retourne le pourcentage numéro 4
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage4() {
    return pourcentage4;
  }
  
  /**
   * Modifier le pourcentage numéro 4
   * 
   */
  public void setPourcentage4(BigDecimal pourcentage4) {
    this.pourcentage4 = pourcentage4;
  }
  
  /**
   * Retourne le pourcentage numéro 5
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage5() {
    return pourcentage5;
  }
  
  /**
   * Modifier le pourcentage numéro 5
   * 
   */
  public void setPourcentage5(BigDecimal pourcentage5) {
    this.pourcentage5 = pourcentage5;
  }
  
  /**
   * Retourne le pourcentage numéro 6
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage6() {
    return pourcentage6;
  }
  
  /**
   * Modifier le pourcentage numéro 6
   * 
   */
  public void setPourcentage6(BigDecimal pourcentage6) {
    this.pourcentage6 = pourcentage6;
  }
  
  /**
   * Retourne le pourcentage numéro 7
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage7() {
    return pourcentage7;
  }
  
  /**
   * Modifier le pourcentage numéro 7
   * 
   */
  public void setPourcentage7(BigDecimal pourcentage7) {
    this.pourcentage7 = pourcentage7;
  }
  
  /**
   * Retourne le pourcentage numéro 8
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage8() {
    return pourcentage8;
  }
  
  /**
   * Modifier le pourcentage numéro 8
   * 
   */
  public void setPourcentage8(BigDecimal pourcentage8) {
    this.pourcentage8 = pourcentage8;
  }
  
  /**
   * Retourne le pourcentage numéro 9
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage9() {
    return pourcentage9;
  }
  
  /**
   * Modifier le pourcentage numéro 9
   * 
   */
  public void setPourcentage9(BigDecimal pourcentage9) {
    this.pourcentage9 = pourcentage9;
  }
  
  /**
   * Retourne le pourcentage numéro 10
   * 
   * @return BigDecimal
   */
  public BigDecimal getPourcentage10() {
    return pourcentage10;
  }
  
  /**
   * Modifier le pourcentage numéro 10
   * 
   */
  public void setPourcentage10(BigDecimal pourcentage10) {
    this.pourcentage10 = pourcentage10;
  }
  
  /**
   * Retourne le montant numéro 1
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant1() {
    return montant1;
  }
  
  /**
   * Modifier le montant numéro 1
   * 
   */
  public void setMontant1(BigDecimal montant1) {
    this.montant1 = montant1;
  }
  
  /**
   * Retourne le montant numéro 2
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant2() {
    return montant2;
  }
  
  /**
   * Modifier le montant numéro 2
   * 
   */
  public void setMontant2(BigDecimal montant2) {
    this.montant2 = montant2;
  }
  
  /**
   * Retourne le montant numéro 3
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant3() {
    return montant3;
  }
  
  /**
   * Modifier le montant numéro 3
   * 
   */
  public void setMontant3(BigDecimal montant3) {
    this.montant3 = montant3;
  }
  
  /**
   * Retourne le montant numéro 4
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant4() {
    return montant4;
  }
  
  /**
   * Modifier le montant numéro 4
   * 
   */
  public void setMontant4(BigDecimal montant4) {
    this.montant4 = montant4;
  }
  
  /**
   * Retourne le montant numéro 5
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant5() {
    return montant5;
  }
  
  /**
   * Modifier le montant numéro 5
   * 
   */
  public void setMontant5(BigDecimal montant5) {
    this.montant5 = montant5;
  }
  
  /**
   * Retourne le montant numéro 6
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant6() {
    return montant6;
  }
  
  /**
   * Modifier le montant numéro 6
   * 
   */
  public void setMontant6(BigDecimal montant6) {
    this.montant6 = montant6;
  }
  
  /**
   * Retourne le montant numéro 7
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant7() {
    return montant7;
  }
  
  /**
   * Modifier le montant numéro 7
   * 
   */
  public void setMontant7(BigDecimal montant7) {
    this.montant7 = montant7;
  }
  
  /**
   * Retourne le montant numéro 8
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant8() {
    return montant8;
  }
  
  /**
   * Modifier le montant numéro 8
   * 
   */
  public void setMontant8(BigDecimal montant8) {
    this.montant8 = montant8;
  }
  
  /**
   * Retourne le montant numéro 9
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant9() {
    return montant9;
  }
  
  /**
   * Modifier le montant numéro 9
   * 
   */
  public void setMontant9(BigDecimal montant9) {
    this.montant9 = montant9;
  }
  
  /**
   * Retourne le montant numéro 10
   * 
   * @return BigDecimal
   */
  public BigDecimal getMontant10() {
    return montant10;
  }
  
  /**
   * Modifier le montant numéro 10
   * 
   */
  public void setMontant10(BigDecimal montant10) {
    this.montant10 = montant10;
  }
  
  /**
   * Retourne l'identifiant de l'article associé
   * 
   * @return BigDecimal
   */
  public IdArticle getIdArticleAssocie() {
    return idArticleAssocie;
  }
  
  /**
   * Modifier l'identifiant de l'article associé
   * 
   */
  public void setIdArticleAssocie(IdArticle pIdArticleAssocie) {
    idArticleAssocie = pIdArticleAssocie;
  }
  
}
