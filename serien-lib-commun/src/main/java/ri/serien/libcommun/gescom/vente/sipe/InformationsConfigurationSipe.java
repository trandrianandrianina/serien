/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.sipe;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

public class InformationsConfigurationSipe implements Serializable {
  // Constantes
  public static final int TAILLE_NOM_DOSSIER = 100;
  public static final int TAILLE_RADICAL = 5;
  public static final int TAILLE_CODE_ETABLISSEMENT = 3;
  public static final int TAILLE_COLLECTIF_FOURNISSEUR = 1;
  public static final int TAILLE_NUMERO_FOURNISSEUR = 6;
  public static final int TAILLE_CODE_FAMILLE = 3;
  public static final int TAILLE_CODE_SOUSFAMILLE = 5;
  public static final int TAILLE_CODE_UNITE = 2;
  
  // Les offsets des données dans la dataarea (mode java, le premier à l'indice 0 et non 1)
  private static final int OFFSET_NOM_DOSSIER = 0;
  private static final int OFFSET_RADICAL = 100;
  private static final int OFFSET_CODE_ETABLISSEMENT = 110;
  private static final int OFFSET_COLLECTIF_FOURNISSEUR = 120;
  private static final int OFFSET_NUMERO_FOURNISSEUR = 121;
  private static final int OFFSET_CODE_FAMILLE = 130;
  private static final int OFFSET_CODE_SOUSFAMILLE = 133;
  private static final int OFFSET_CODE_UNITE = 138;
  
  // Variables
  private String nomDossierIFS = "";
  private String radical = "";
  private IdFournisseur idFournisseur = null;
  private String codeFamille = "";
  private String codeSousFamille = "";
  private String codeUnite = "";
  
  // -- Méthodes publique
  
  /**
   * Initialise les variables de la classe à partir d'une chaine (contenu de la dataarea PGVMPRODEV).
   */
  public void initialiserVariables(String pBuffer, int pLongueurDataarea) {
    if ((pBuffer == null) || (pBuffer.length() < pLongueurDataarea)) {
      throw new MessageErreurException("Le buffer pour initialiser les variables est incorrect.");
    }
    
    String codeEtablissement = pBuffer.substring(OFFSET_CODE_ETABLISSEMENT, OFFSET_CODE_ETABLISSEMENT + TAILLE_CODE_ETABLISSEMENT);
    int collectifFournisseur = Constantes.convertirTexteEnInteger(
        pBuffer.substring(OFFSET_COLLECTIF_FOURNISSEUR, OFFSET_COLLECTIF_FOURNISSEUR + TAILLE_COLLECTIF_FOURNISSEUR).trim());
    int numeroFournisseur = Constantes.convertirTexteEnInteger(
        pBuffer.substring(OFFSET_NUMERO_FOURNISSEUR, OFFSET_NUMERO_FOURNISSEUR + TAILLE_NUMERO_FOURNISSEUR).trim());
    IdEtablissement idEtablissement = IdEtablissement.getInstance(codeEtablissement);
    idFournisseur = IdFournisseur.getInstance(idEtablissement, collectifFournisseur, numeroFournisseur);
    
    setNomDossierIFS(pBuffer.substring(OFFSET_NOM_DOSSIER, TAILLE_NOM_DOSSIER).trim());
    setRadical(pBuffer.substring(OFFSET_RADICAL, OFFSET_RADICAL + TAILLE_RADICAL).trim());
    setCodeFamille(pBuffer.substring(OFFSET_CODE_FAMILLE, OFFSET_CODE_FAMILLE + TAILLE_CODE_FAMILLE).trim());
    setCodeSousFamille(pBuffer.substring(OFFSET_CODE_SOUSFAMILLE, OFFSET_CODE_SOUSFAMILLE + TAILLE_CODE_SOUSFAMILLE).trim());
    setCodeUnite(pBuffer.substring(OFFSET_CODE_UNITE, OFFSET_CODE_UNITE + TAILLE_CODE_UNITE).trim());
  }
  
  // -- Accesseurs
  
  public String getNomDossierIFS() {
    return nomDossierIFS;
  }
  
  public void setNomDossierIFS(String nomDossierIFS) {
    this.nomDossierIFS = Constantes.normerTexte(nomDossierIFS);
  }
  
  public String getRadical() {
    return radical;
  }
  
  public void setRadical(String radical) {
    this.radical = radical;
  }
  
  public IdFournisseur getNumeroFournisseur() {
    return idFournisseur;
  }
  
  public void setNumeroFournisseur(IdFournisseur pIdFournisseur) {
    idFournisseur = pIdFournisseur;
  }
  
  public String getCodeFamille() {
    return codeFamille;
  }
  
  public void setCodeFamille(String codeFamille) {
    this.codeFamille = codeFamille;
  }
  
  public String getCodeSousFamille() {
    return codeSousFamille;
  }
  
  public void setCodeSousFamille(String codeSousFamille) {
    this.codeSousFamille = codeSousFamille;
  }
  
  public String getCodeUnite() {
    return codeUnite;
  }
  
  public void setCodeUnite(String codeUnite) {
    this.codeUnite = codeUnite;
  }
  
}
