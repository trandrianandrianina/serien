/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.article.StatutArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Ensemble des paramètres qui permettent de définir le comportement du module GVC.
 */
public final class ListeParametresGVC implements Serializable {
  // On intercepte le traitement de certaines méthodes via le dynamic Proxy
  // La plupart du temps on logge le temps de traitement de ces émthodes sensibles
  private boolean proxyDynamiqueActif = true;
  
  // Nombre d'articles maximum par page dans le GVC
  private int nombreArticlesParPage = 20;
  
  // Filtre sur un statut d'articles dans la recherche
  private StatutArticle filtreStatutArticle;
  
  // Filtre sur les articles moins cher PriceJet dans la recherche
  private boolean filtreMoinsChersPriceJet = false;
  
  // Filtre sur l'établissement.
  // De base, le GVC utilise l'établissement associé au profil (dans ce cas, la valeur est null pour cet attribcut).
  // Il est possible de forcer l'établissement pour tous les utilisateurs via ce paramètre.
  private IdEtablissement filtreEtablissement = null;

  // Il s'agit du HOST du serveur SMTP avec lequel le GVC va envoyer des mails
  private String hostSMTP = null;

  // Il s'agit du port du serveur SMTP avec lequel le GVC va envoyer des mails
  private String portSMTP = null;
  
  // Il s'agit du profil expediteur avec lequel le GVC va envoyer des mails
  private String profilSMTP = null;
  
  // Il s'agit du mot de passe expediteur avec lequel le GVC va envoyer des mails
  private String mpSMTP = null;
  
  public StatutArticle getFiltreStatutArticle() {
    return filtreStatutArticle;
  }
  
  public void setFiltreStatutArticle(StatutArticle filtreStatutArticle) {
    this.filtreStatutArticle = filtreStatutArticle;
  }
  
  public boolean isFiltreMoinsChersPriceJet() {
    return filtreMoinsChersPriceJet;
  }
  
  public void setFiltreMoinsChersPriceJet(boolean filtreMoinsChersPriceJet) {
    this.filtreMoinsChersPriceJet = filtreMoinsChersPriceJet;
  }
  
  public IdEtablissement getFiltreEtablissement() {
    return filtreEtablissement;
  }
  
  public void setFiltreEtablissement(IdEtablissement pFiltreEtablissement) {
    this.filtreEtablissement = pFiltreEtablissement;
  }
  
  public boolean isProxyDynamiqueActif() {
    return proxyDynamiqueActif;
  }
  
  public void setProxyDynamiqueActif(boolean proxyDynamiqueActif) {
    this.proxyDynamiqueActif = proxyDynamiqueActif;
  }
  
  public int getNombreArticlesParPage() {
    return nombreArticlesParPage;
  }
  
  public void setNombreArticlesParPage(int nombreArticlesParPage) {
    this.nombreArticlesParPage = nombreArticlesParPage;
  }
  
  public String getHostSMTP() {
    return hostSMTP;
  }
  
  public void setHostSMTP(String hostSMTP) {
    this.hostSMTP = hostSMTP;
  }
  
  public String getPortSMTP() {
    return portSMTP;
  }
  
  public void setPortSMTP(String portSMTP) {
    this.portSMTP = portSMTP;
  }
  
  public String getProfilSMTP() {
    return profilSMTP;
  }
  
  public void setProfilSMTP(String profilSMTP) {
    this.profilSMTP = profilSMTP;
  }
  
  public String getMpSMTP() {
    return mpSMTP;
  }
  
  public void setMpSMTP(String mpSMTP) {
    this.mpSMTP = mpSMTP;
  }

}
