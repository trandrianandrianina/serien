/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typegratuit;

import ri.serien.libcommun.outils.MessageErreurException;

/*
 * Enum encadrant le type de document où sera édité le libellé complet d'un type de gratuit
 */
public enum EnumEditionLibelleCompletTypeGratuit {
  AUCUN(' ', "Aucun"),
  SUR_BON('1', "Sur bon"),
  SUR_FACTURE('2', "Sur factures"),
  SUR_BON_ET_FACTURES('3', "Sur bons et factures");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumEditionLibelleCompletTypeGratuit(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumEditionLibelleCompletTypeGratuit valueOfByCode(Character pCode) {
    for (EnumEditionLibelleCompletTypeGratuit value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException(
        "Dans un paramètre type de gratuit (TG), le champ 'édition du libelle complet' est invalide : " + pCode);
  }
}
