/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.alertedocument;

import java.io.Serializable;
import java.math.BigDecimal;

import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

public class AlertesLigneDocument implements Serializable {
  // Constantes TODO il va falloir trouver les messages exacts
  private static final String ALERTE_QUANTITE_SUSPECTE = "Quantité égale à la quantité suspecte défini dans les unités.";
  private static final String ALERTE_QUANTITE_SUPERIEURE = "Quantité supérieure à la quantité maximum défini dans les unités.";
  private static final String ALERTE_ARTICLE_REAPPRO_NON_GERE = "Article en réappro non géré, le conditionnement est %f.";
  private static final String ALERTE_STOCK_NON_DISPO_MAIS_MAG = "Article disponible dans le magasin %s.";
  private static final String ALERTE_STOCK_NON_DISPO = "Article non disponible.";
  private static final String ALERTE_QUANTITE_IMPORTANTE = "Attention quantité importante.";
  private static final String ALERTE_COMMANDE_ELEPHANT = "Attention commande éléphant.";
  private static final String ALERTE_QUANTITE_DEPASSEE = "Attention la quantité dépasse la limite de %f.";
  private static final String ALERTE_CONDITIONNEMENT_ET_QUANTITE_FORCEE = "Attention le conditionnement est de %f et la quantité de %f.";
  
  private static final String ALERTE_FIN_REAPPRO = "Attention fin de réappro.";
  private static final String ALERTE_RESTAURATION_AVANT_EXTRACTION = "Attention restauration avant l'extraction.";
  private static final String ALERTE_TYPE_SUIVI_INEXISTANT = "Le type de suivi est inexistant.";
  private static final String ALERTE_ARTICLE_DEPRECIE = "Attention l'article est déprécié de %f.";
  
  // Variables
  private String messages = "";
  
  // Variables SVGVM0004 : Service controlerLigneDocument
  private boolean quantiteEgaleQuantiteSuspecteParamUnite = false;
  private boolean quantiteSuperieureAuMaxDefiniParamUnite = false;
  private boolean articleEnReapproNonGere = false;
  private Integer quantiteConditionnement = 0;
  private boolean stockNonDisponible_DisponibleEnMagasin = false;
  private IdMagasin idMagasin = null;
  private boolean stockNonDisponible = false;
  private boolean quantiteImportante = false;
  private boolean commandeElephant = false;
  private boolean quantiteLimiteDepassee = false;
  private Integer quantiteLimite = 0;
  private boolean conditionnementEtQuantiteForcee = false;
  private Integer conditionnement = 0;
  private Integer quantiteForcee = 0;
  private boolean encoursDepasse = false;
  
  // Variables SVGVM0005 : Service controlerLigneDocument
  private boolean finReapproPourArticle = false;
  private boolean restaurationAvantExtraction = false;
  private boolean typeSuiviInexistant = false;
  private boolean articleDeprecie = false;
  private BigDecimal pourcentageDepreciation = BigDecimal.ZERO;
  
  /**
   * Controle et liste les messages d'alerte.
   */
  public boolean controlerAlertes() {
    StringBuilder message = new StringBuilder();
    boolean alertesTrouvees = false;
    messages = "";
    
    // SVGVM0004
    if (quantiteEgaleQuantiteSuspecteParamUnite) {
      alertesTrouvees = true;
      message.append(ALERTE_QUANTITE_SUSPECTE).append('\n');
    }
    if (quantiteSuperieureAuMaxDefiniParamUnite) {
      alertesTrouvees = true;
      message.append(ALERTE_QUANTITE_SUPERIEURE).append('\n');
    }
    if (articleEnReapproNonGere) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_ARTICLE_REAPPRO_NON_GERE, quantiteConditionnement)).append('\n');
    }
    if (stockNonDisponible_DisponibleEnMagasin) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_STOCK_NON_DISPO_MAIS_MAG, idMagasin.getCode())).append('\n');
    }
    if (stockNonDisponible) {
      alertesTrouvees = true;
      message.append(ALERTE_STOCK_NON_DISPO).append('\n');
    }
    if (quantiteImportante) {
      alertesTrouvees = true;
      message.append(ALERTE_QUANTITE_IMPORTANTE).append('\n');
    }
    if (commandeElephant) {
      alertesTrouvees = true;
      message.append(ALERTE_COMMANDE_ELEPHANT).append('\n');
    }
    if (quantiteLimiteDepassee) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_QUANTITE_DEPASSEE, quantiteLimite)).append('\n');
    }
    if (conditionnementEtQuantiteForcee) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_CONDITIONNEMENT_ET_QUANTITE_FORCEE, conditionnement, quantiteForcee)).append('\n');
    }
    // SVGVM0005
    if (finReapproPourArticle) {
      alertesTrouvees = true;
      message.append(ALERTE_FIN_REAPPRO).append('\n');
    }
    if (restaurationAvantExtraction) {
      alertesTrouvees = true;
      message.append(ALERTE_RESTAURATION_AVANT_EXTRACTION).append('\n');
    }
    if (typeSuiviInexistant) {
      alertesTrouvees = true;
      message.append(ALERTE_TYPE_SUIVI_INEXISTANT).append('\n');
    }
    if (articleDeprecie) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_ARTICLE_DEPRECIE, pourcentageDepreciation)).append('\n');
    }
    
    if (alertesTrouvees) {
      messages = message.toString().trim();
    }
    return alertesTrouvees;
  }
  
  // Accesseurs
  
  public String getMessages() {
    return messages;
  }
  
  public void setMessages(String messages) {
    this.messages = messages;
  }
  
  public boolean isQuantiteEgaleQuantiteSuspecteParamUnite() {
    return quantiteEgaleQuantiteSuspecteParamUnite;
  }
  
  public void setQuantiteEgaleQuantiteSuspecteParamUnite(boolean quantiteEgaleQuantiteSuspecteParamUnite) {
    this.quantiteEgaleQuantiteSuspecteParamUnite = quantiteEgaleQuantiteSuspecteParamUnite;
  }
  
  public boolean isQuantiteSuperieureAuMaxDefiniParamUnite() {
    return quantiteSuperieureAuMaxDefiniParamUnite;
  }
  
  public void setQuantiteSuperieureAuMaxDefiniParamUnite(boolean quantiteSuperieureAuMaxDefiniParamUnite) {
    this.quantiteSuperieureAuMaxDefiniParamUnite = quantiteSuperieureAuMaxDefiniParamUnite;
  }
  
  public boolean isArticleEnReapproNonGere() {
    return articleEnReapproNonGere;
  }
  
  public void setArticleEnReapproNonGere(boolean articleEnReapproNonGere) {
    this.articleEnReapproNonGere = articleEnReapproNonGere;
  }
  
  public Integer getQuantiteConditionnement() {
    return quantiteConditionnement;
  }
  
  public void setQuantiteConditionnement(Integer quantiteConditionnement) {
    this.quantiteConditionnement = quantiteConditionnement;
  }
  
  public boolean isStockNonDisponible_DisponibleEnMagasin() {
    return stockNonDisponible_DisponibleEnMagasin;
  }
  
  public void setStockNonDisponible_DisponibleEnMagasin(boolean stockNonDisponible_DisponibleEnMagasin) {
    this.stockNonDisponible_DisponibleEnMagasin = stockNonDisponible_DisponibleEnMagasin;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin pMagasin) {
    this.idMagasin = pMagasin;
  }
  
  public boolean isStockNonDisponible() {
    return stockNonDisponible;
  }
  
  public void setStockNonDisponible(boolean stockNonDisponible) {
    this.stockNonDisponible = stockNonDisponible;
  }
  
  public boolean isQuantiteImportante() {
    return quantiteImportante;
  }
  
  public void setQuantiteImportante(boolean quantiteImportante) {
    this.quantiteImportante = quantiteImportante;
  }
  
  public boolean isCommandeElephant() {
    return commandeElephant;
  }
  
  public void setCommandeElephant(boolean commandeElephant) {
    this.commandeElephant = commandeElephant;
  }
  
  public boolean isQuantiteLimiteDepassee() {
    return quantiteLimiteDepassee;
  }
  
  public void setQuantiteLimiteDepassee(boolean quantiteLimiteDepassee) {
    this.quantiteLimiteDepassee = quantiteLimiteDepassee;
  }
  
  public Integer getQuantiteLimite() {
    return quantiteLimite;
  }
  
  public void setQuantiteLimite(Integer quantiteLimite) {
    this.quantiteLimite = quantiteLimite;
  }
  
  public boolean isConditionnementEtQuantiteForcee() {
    return conditionnementEtQuantiteForcee;
  }
  
  public void setConditionnementEtQuantiteForcee(boolean conditionnementEtQuantiteForcee) {
    this.conditionnementEtQuantiteForcee = conditionnementEtQuantiteForcee;
  }
  
  public Integer getConditionnement() {
    return conditionnement;
  }
  
  public void setConditionnement(Integer conditionnement) {
    this.conditionnement = conditionnement;
  }
  
  public Integer getQuantiteForcee() {
    return quantiteForcee;
  }
  
  public void setQuantiteForcee(Integer quantiteForcee) {
    this.quantiteForcee = quantiteForcee;
  }
  
  public boolean isFinReapproPourArticle() {
    return finReapproPourArticle;
  }
  
  public void setFinReapproPourArticle(boolean finReapproPourArticle) {
    this.finReapproPourArticle = finReapproPourArticle;
  }
  
  public boolean isRestaurationAvantExtraction() {
    return restaurationAvantExtraction;
  }
  
  public void setRestaurationAvantExtraction(boolean restaurationAvantExtraction) {
    this.restaurationAvantExtraction = restaurationAvantExtraction;
  }
  
  public boolean isTypeSuiviInexistant() {
    return typeSuiviInexistant;
  }
  
  public void setTypeSuiviInexistant(boolean typeSuiviInexistant) {
    this.typeSuiviInexistant = typeSuiviInexistant;
  }
  
  public boolean isArticleDeprecie() {
    return articleDeprecie;
  }
  
  public void setArticleDeprecie(boolean articleDeprecie) {
    this.articleDeprecie = articleDeprecie;
  }
  
  public BigDecimal getPourcentageDepreciation() {
    return pourcentageDepreciation;
  }
  
  public void setPourcentageDepreciation(BigDecimal pourcentageDepreciation) {
    this.pourcentageDepreciation = pourcentageDepreciation;
  }
  
  public boolean isEncoursDepasse() {
    return encoursDepasse;
  }
  
  public void setEncoursDepasse(boolean encoursDepasse) {
    this.encoursDepasse = encoursDepasse;
  }
}
