/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;

/**
 * Cette classe décrit les critères de recherche de configurations de catalogue
 * On peut filtrer sur un id Fournisseur
 */
public class CriteresRechercheCfgCatalogue extends CriteresBaseRecherche {
  private IdFournisseur idFournisseur = null;
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
}
