/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typefacturation;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Classe décrivant le type de faturation.
 */
public class TypeFacturation extends AbstractClasseMetier<IdTypeFacturation> {
  // Constantes
  private static final String LIBELLE_FACTURATION_SANS_TVA = "Sans TVA";
  private static final String LIBELLE_FACTURATION_UE = "UE avec TVA";
  public static final int NOMBRE_CHIFFRE_CODE_TVA = 3;
  
  // Variables
  private String libelle = null;
  private Integer codeTVA = null;
  
  /**
   * Le constructeur.
   */
  public TypeFacturation(IdTypeFacturation pTypeFacturation) {
    super(pTypeFacturation);
  }
  
  /**
   * Créer un type de facturation pour une facturation sans TVA.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public static TypeFacturation getInstanceFacturationSansTVA(IdEtablissement pIdEtablissement) {
    IdTypeFacturation id = IdTypeFacturation.getInstanceFacturationSansTVA(pIdEtablissement);
    TypeFacturation typeFacturation = new TypeFacturation(id);
    typeFacturation.setLibelle(LIBELLE_FACTURATION_SANS_TVA);
    return typeFacturation;
  }
  
  /**
   * Créer un type de facturation pour une facturation UE.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public static TypeFacturation getInstanceFacturationUE(IdEtablissement pIdEtablissement) {
    IdTypeFacturation id = IdTypeFacturation.getInstanceFacturationUE(pIdEtablissement);
    TypeFacturation typeFacturation = new TypeFacturation(id);
    typeFacturation.setLibelle(LIBELLE_FACTURATION_UE);
    return typeFacturation;
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Retourne le chiffre du code TVA à une position donnée.
   * Les valeurs sont comprises entre 1 et 3.
   */
  public int getChiffreCodeTVAEnPosition(int pPositionChiffre) {
    if (codeTVA == null) {
      return 0;
    }
    pPositionChiffre = controlerChiffreCodeTVA(pPositionChiffre) - 1;
    String code = String.format("%0" + NOMBRE_CHIFFRE_CODE_TVA + "d", codeTVA);
    return Constantes.convertirTexteEnInteger("" + code.charAt(pPositionChiffre));
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du chiffre pour le code TVA.
   * Le suffixe client doit être supérieur ou égal à zéro et doit comportr au maximum 3 chiffres (entre 0 et 999).
   */
  private static int controlerChiffreCodeTVA(int pValeur) {
    if (pValeur < 1) {
      throw new MessageErreurException("Le chiffre du code TVA est inférieur à un.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(NOMBRE_CHIFFRE_CODE_TVA)) {
      throw new MessageErreurException("Le chiffre du code TVA est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Initialise le libellé.
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  /**
   * Retourne le code TVA.
   */
  public Integer getCodeTVA() {
    return codeTVA;
  }
  
  /**
   * Initialise le code TVA.
   */
  public void setCodeTVA(Integer codeTVA) {
    this.codeTVA = codeTVA;
  }
}
