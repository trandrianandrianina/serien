/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Description d'une banque client.
 */
public class BanqueClient extends AbstractClasseMetier<IdBanqueClient> {
  
  /**
   * Constructeur.
   */
  public BanqueClient(IdBanqueClient pIdBibliotheque) {
    super(pIdBibliotheque);
  }
  
  // -- Méthodes publiques
  
  @Override
  public String toString() {
    return getTexte();
  }
  
  @Override
  public String getTexte() {
    return getNom();
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  /**
   * Retourne le nom de la bibliothèque.
   */
  public String getNom() {
    if (id == null) {
      return null;
    }
    return id.getNom();
  }
  
}
