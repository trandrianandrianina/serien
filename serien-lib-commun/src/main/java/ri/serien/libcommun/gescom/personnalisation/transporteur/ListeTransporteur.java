/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.transporteur;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de Transporeur.
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de Transporteur. Elle contient
 * toutes les méthodes oeuvrant sur la liste tandis que la classe Transporteur contient les méthodes oeuvrant sur un
 * seul Transporteur.
 */
public class ListeTransporteur extends ListeClasseMetier<IdTransporteur, Transporteur, ListeTransporteur> {
  
  /**
   * Constructeur.
   */
  public ListeTransporteur() {
  }
  
  /**
   * Charger touts les transporteurs suivant une liste d'IdTransporteur.
   */
  @Override
  public ListeTransporteur charger(IdSession pIdSession, List<IdTransporteur> pListeId) {
    return null;
  }
  
  /**
   * Charger les transporteurs suivant l'établissement.
   */
  @Override
  public ListeTransporteur charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas défini.");
    }
    CriteresRechercheTransporteurs criteres = new CriteresRechercheTransporteurs();
    criteres.setTypeRecherche(CriteresRechercheTransporteurs.RECHERCHE_COMPTOIR);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    ListeTransporteur listeTransporteur = ManagerServiceParametre.chargerListeTransporteur(pIdSession, criteres);
    return listeTransporteur;
  }
}
