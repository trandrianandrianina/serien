/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente;

/**
 * Liste des types de calculs pour une formule prix de vente.
 */
public enum EnumTypeFormulePrixVente {
  STANDARD("Standard"),
  CONSIGNE("Consigne"),
  CHANTIER("Chantier"),
  CNV_PRIORITAIRE("CNV Prioritaire"),
  CNV_NORMALE("CNV Normale"),
  CNV_CUMULATIVE("CNV Cumulative"),
  CNV_QUANTITATIVE("CNV Quantitative");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeFormulePrixVente(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
