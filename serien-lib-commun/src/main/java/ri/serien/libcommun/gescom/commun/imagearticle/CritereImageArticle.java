/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.imagearticle;

import ri.serien.libcommun.commun.recherche.CritereAvecEtablissement;

/**
 * Pas de recherche pour le moment.
 */
public class CritereImageArticle extends CritereAvecEtablissement {
  
}
