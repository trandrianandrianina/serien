/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.codepostalcommune;

import java.util.ArrayList;
import java.util.Collections;

import ri.serien.libcommun.gescom.vente.commune.Commune;
import ri.serien.libcommun.gescom.vente.commune.CritereCommune;
import ri.serien.libcommun.gescom.vente.commune.ListeCommune;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Liste des couples code postal / commune.
 */
public class ListeCodePostalCommune extends ArrayList<CodePostalCommune> {
  
  /**
   * Constructeur.
   */
  public ListeCodePostalCommune() {
  }
  
  /**
   * Charger tous les couples code postal / commune (Charge une liste complete de commune et les mets dans une liste de
   * codePostalCommune).
   */
  public static ListeCodePostalCommune charger(IdSession pIdSession) {
    ListeCommune listeComplete = ListeCommune.charger(pIdSession);
    ListeCodePostalCommune listeCodePostalCommune = new ListeCodePostalCommune();
    if (listeComplete != null) {
      for (int i = 0; i < listeComplete.size(); i++) {
        CodePostalCommune codePostalCommune =
            CodePostalCommune.getInstance(listeComplete.get(i).getCodePostalFormate(), listeComplete.get(i).getNom());
        listeCodePostalCommune.add(codePostalCommune);
      }
    }
    return listeCodePostalCommune;
  }
  
  /**
   * Charger la liste des CodePostalCommune à partir de critères de recherche de communes
   */
  public static ListeCodePostalCommune chargerListeCodePostalCommune(IdSession pIdSession, CritereCommune pCritere) {
    if (pCritere == null) {
      return null;
    }
    ListeCodePostalCommune listeCodePostalCommune = new ListeCodePostalCommune();
    ListeCommune listeCommune = ListeCommune.charger(pIdSession, pCritere);
    for (Commune commune : listeCommune) {
      CodePostalCommune codePostalCommune = CodePostalCommune.getInstance(commune.getCodePostalFormate(), commune.getNom());
      listeCodePostalCommune.add(codePostalCommune);
    }
    return listeCodePostalCommune;
  }
  
  /**
   * Charger la liste des CodePostalCommune à partir d'un code postal
   */
  public static ListeCodePostalCommune chargerListeCodePostalCommuneParCodePostal(IdSession pIdSession, String pCodePostal) {
    pCodePostal = Constantes.normerTexte(pCodePostal);
    if (pCodePostal.isEmpty()) {
      return null;
    }
    int codePostal = Integer.parseInt(Constantes.normerTexte(pCodePostal));
    ListeCodePostalCommune listeCodePostalCommune = new ListeCodePostalCommune();
    CritereCommune critere = new CritereCommune();
    critere.setCodePostal(codePostal);
    ListeCommune listeCommune = ListeCommune.charger(pIdSession, critere);
    for (Commune commune : listeCommune) {
      CodePostalCommune codePostalCommune = CodePostalCommune.getInstance(commune.getCodePostalFormate(), commune.getNom());
      listeCodePostalCommune.add(codePostalCommune);
    }
    
    return listeCodePostalCommune;
  }
  
  /**
   * Charger la liste des CodePostalCommune à partir d'une ville
   */
  public static ListeCodePostalCommune chargerListeCodePostalCommuneParVille(IdSession pIdSession, String pVille) {
    pVille = Constantes.normerTexte(pVille);
    if (pVille.isEmpty() || pVille.length() > CodePostalCommune.LONGUEUR_CODEPOSTAL) {
      return null;
    }
    ListeCodePostalCommune listeCodePostalCommune = new ListeCodePostalCommune();
    CritereCommune critere = new CritereCommune();
    critere.setNomCommune(pVille);
    ListeCommune listeCommune = ListeCommune.charger(pIdSession, critere);
    for (Commune commune : listeCommune) {
      CodePostalCommune codePostalCommune = CodePostalCommune.getInstance(commune.getCodePostalFormate(), commune.getNom());
      listeCodePostalCommune.add(codePostalCommune);
    }
    
    return listeCodePostalCommune;
  }
  
  public CodePostalCommune getCodePostalCommuneParCodePostal(String pCodePostal) {
    if (pCodePostal == null || pCodePostal.trim().isEmpty()) {
      return null;
    }
    
    ListeCodePostalCommune listeCodePostalCommune = new ListeCodePostalCommune();
    
    for (CodePostalCommune commune : this) {
      if (commune.getCodePostal().startsWith(pCodePostal)) {
        listeCodePostalCommune.add(commune);
      }
    }
    if (listeCodePostalCommune.size() == 1) {
      return listeCodePostalCommune.get(0);
    }
    // Si on a pas trouvé dans la liste ou si on en trouve plusieurs on crée un CodePostalCommune qui n'existe pas dans la liste
    return CodePostalCommune.getInstance(pCodePostal, "");
  }
  
  public CodePostalCommune getCodePostalCommuneParCommune(String pVille) {
    if (pVille == null || pVille.trim().isEmpty()) {
      return null;
    }
    
    ListeCodePostalCommune listeCodePostalCommune = new ListeCodePostalCommune();
    
    for (CodePostalCommune commune : this) {
      if (commune.getVille().equalsIgnoreCase(pVille)) {
        listeCodePostalCommune.add(commune);
      }
    }
    if (listeCodePostalCommune.size() >= 1) {
      return listeCodePostalCommune.get(0);
    }
    // Si on a pas trouvé dans la liste ou si on en trouve plusieurs on crée un CodePostalCommune qui n'existe pas dans la liste
    return CodePostalCommune.getInstance("", pVille);
  }
  
  /**
   * Retourner la liste codePostalVille sur un code postal partiel ou entier.
   */
  public ListeCodePostalCommune getCodePostalCommuneViaCodePostal(String pCodePostal) {
    // Controles
    pCodePostal = Constantes.normerTexte(pCodePostal);
    if (pCodePostal.isEmpty() || pCodePostal.length() > CodePostalCommune.LONGUEUR_CODEPOSTAL) {
      return null;
    }
    int pValeur = 0;
    try {
      pValeur = Integer.parseInt(pCodePostal);
    }
    catch (NumberFormatException e) {
      return null;
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(CodePostalCommune.LONGUEUR_CODEPOSTAL)) {
      return null;
    }
    // Traitement
    int coef = 1;
    switch (pCodePostal.length()) {
      case 1:
        coef = 10000;
        break;
      case 2:
        coef = 1000;
        break;
      case 3:
        coef = 100;
        break;
      case 4:
        coef = 10;
        break;
    }
    int valeurMin = pValeur * coef;
    int valeurMax = valeurMin + (coef - 1);
    
    ListeCodePostalCommune listeCodePostalCommuneFiltrer = new ListeCodePostalCommune();
    for (CodePostalCommune codePostalCommune : this) {
      if (codePostalCommune.getCodePostalFormate() < valeurMin) {
        continue;
      }
      if (codePostalCommune.getCodePostalFormate() <= valeurMax) {
        listeCodePostalCommuneFiltrer.add(codePostalCommune);
      }
      else {
        break;
      }
    }
    return listeCodePostalCommuneFiltrer;
  }
  
  /**
   * Trier la liste de CodePostalCommune.
   */
  public void trier() {
    Collections.sort(this);
  }
}
