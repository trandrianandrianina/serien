/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Option de validation d'un document d'achat.
 */
public enum EnumOptionValidation {
  VALIDATION("VAL", "Validation"),
  ATTENTE("ATT", "Mise en attente"),
  ANNULATION("ANN", "Annulation");
  
  private final String code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOptionValidation(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumOptionValidation valueOfByCode(String pCode) {
    for (EnumOptionValidation value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code de validation est invalide : " + pCode);
  }
  
}
