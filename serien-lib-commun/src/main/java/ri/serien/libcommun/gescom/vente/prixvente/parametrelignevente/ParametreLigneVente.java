/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente;

import java.io.Serializable;
import java.math.BigDecimal;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.IdTypeGratuit;
import ri.serien.libcommun.gescom.vente.ligne.EnumOriginePrixVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumModeTauxRemise;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Classe regroupant tous les paramètres da la ligne de vente pour le calcul du prix de vente.
 */
public class ParametreLigneVente implements Serializable {
  // Attributs descriptifs
  private IdLigneVente idLigneVente = null;
  private IdArticle idArticle = null;
  private Integer etat = null;
  private EnumOriginePrixVente originePrixVente = null;
  
  // Attributs pour le chargement des données
  private EnumTypeApplicationConditionVente typeApplicationConditionVente = null;
  
  // Attributs pour le calcul
  private BigDecimal quantiteUV = null;
  private String codeUniteVente = null;
  private EnumProvenanceColonneTarif provenanceColonneTarif = null;
  private Integer numeroColonneTarif = null;
  private EnumProvenancePrixBase provenancePrixBase = null;
  private BigDecimal prixBaseHT = null;
  private BigDecimal prixBaseTTC = null;
  private EnumProvenanceTauxRemise provenanceTauxRemise = null;
  private BigPercentage tauxRemise1 = null;
  private BigPercentage tauxRemise2 = null;
  private BigPercentage tauxRemise3 = null;
  private BigPercentage tauxRemise4 = null;
  private BigPercentage tauxRemise5 = null;
  private BigPercentage tauxRemise6 = null;
  private EnumModeTauxRemise modeTauxRemise = null;
  private EnumAssietteTauxRemise assietteTauxRemise = null;
  private EnumProvenanceCoefficient provenanceCoefficient = null;
  private BigDecimal coefficient = null;
  private IdTypeGratuit idTypeGratuit = null;
  private EnumProvenancePrixNet provenancePrixNet = null;
  private BigDecimal prixNetSaisiHT = null;
  private BigDecimal prixNetSaisiTTC = null;
  private BigDecimal prixNetCalculeHT = null;
  private BigDecimal prixNetCalculeTTC = null;
  private Integer numeroTVAEtablissement = null;
  private Integer numeroTVAEntete = null;
  private BigDecimal montantHT = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs sur les attributs informatifs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'identifiant de la ligne de vente.
   * @return Identifiant de la ligne de vente.
   */
  public IdLigneVente getIdLigneVente() {
    return idLigneVente;
  }
  
  /**
   * Modifier l'identifiant de la ligne de vente.
   * @param pIdLigneVente Identifiant de la ligne de vente.
   */
  public void setIdLigneVente(IdLigneVente pIdLigneVente) {
    idLigneVente = pIdLigneVente;
  }
  
  /**
   * Retourner l'identifiant article de la ligne de vente.
   * @return Identifiant article.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Modifier l'identifiant article de la ligne de vente.
   * @param pIdArticle Identifiant article.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  /**
   * Retourner l'état de la ligne de vente..
   * @return Etat de la ligne de vente.
   */
  public Integer getEtat() {
    return etat;
  }
  
  /**
   * Modifier l'état de la ligne de vente.
   * @param pEtat Etat de la ligne de vente.
   */
  public void setEtat(Integer pEtat) {
    this.etat = pEtat;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs sur les attributs pour le chargement des données
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Indiquer si le prix de la ligne de vente est un prix garanti.
   * 
   * Si le prix est garanti, il faut calculer le prix uniquement avec les données de la ligne. Noter que l'indicateur prix garanti
   * est présent dans l'entête et dans les lignes du document de vente. Si l'un des deux indicateurs est présent, soit dans l'entête,
   * soit dans la ligne, cela signifie que le prix est garanti.
   * 
   * @return true=prix garanti, false=prix non garanti.
   */
  public boolean isPrixGaranti() {
    return originePrixVente == EnumOriginePrixVente.GARANTI_COMMANDE || originePrixVente == EnumOriginePrixVente.GARANTI_DEVIS
        || originePrixVente == EnumOriginePrixVente.GARANTI_PAR_REPRESENTANT;
  }
  
  /**
   * Retouner l'origine du prix de vente.
   * @return Origine du prix de vente.
   */
  public EnumOriginePrixVente getOriginePrixVente() {
    return originePrixVente;
  }
  
  /**
   * Renseigner l'origine du prix de vente.
   * @param pOriginePrixVente Origine du prix de vente.
   */
  public void setOriginePrixVente(EnumOriginePrixVente pOriginePrixVente) {
    originePrixVente = pOriginePrixVente;
  }
  
  /**
   * Retourner le type d'application des conditions de vente..
   * Indique si les conditions de ventes normales et/ou quantitatives doivent être prise en compte.
   * @return Type d'application des conditions de vente.
   */
  public EnumTypeApplicationConditionVente getTypeApplicationConditionVente() {
    return typeApplicationConditionVente;
  }
  
  /**
   * Modifier le type d'application des conditions de vente.
   * @param pTypeApplicationConditionVente Type d'application des conditions de vente.
   */
  public void setTypeApplicationConditionVente(EnumTypeApplicationConditionVente pTypeApplicationConditionVente) {
    typeApplicationConditionVente = pTypeApplicationConditionVente;
  }
  
  /**
   * Indiquer si les conditions de ventes normales sont applicables pour cette ligne de vente.
   * @return true=conditions de vente normales applicables, false=sinon.
   */
  public boolean isConditionVenteNormaleApplicable() {
    if (typeApplicationConditionVente != null
        && typeApplicationConditionVente == EnumTypeApplicationConditionVente.FORCE_CNV_QUANTITATIVE) {
      return false;
    }
    return true;
  }
  
  /**
   * Indiquer si les conditions de ventes quantitatives sont applicables pour cette ligne de vente.
   * @return true=conditions de vente quantitatives applicables, false=sinon.
   */
  public boolean isConditionVenteQuantitativeApplicable() {
    if (typeApplicationConditionVente != null
        && typeApplicationConditionVente == EnumTypeApplicationConditionVente.IGNORE_CNV_QUANTITATIVE) {
      return false;
    }
    return true;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs sur les attributs pour le calcul
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner la quantité en UV de la ligne de vente.
   * @return Quantité.
   */
  public BigDecimal getQuantiteUV() {
    return quantiteUV;
  }
  
  /**
   * Modifier la quantité en UV de la ligne de vente.
   * @param pQuantiteUV Quantité.
   */
  public void setQuantiteUV(BigDecimal pQuantiteUV) {
    quantiteUV = pQuantiteUV;
  }
  
  /**
   * Retourner l'unité de vente de la ligne de vente.
   * @return Unité de vente.
   */
  public String getCodeUniteVente() {
    return codeUniteVente;
  }
  
  /**
   * Initialise l'unité de vente.
   * 
   * @param pCodeUniteVente
   */
  public void setCodeUniteVente(String pCodeUniteVente) {
    this.codeUniteVente = pCodeUniteVente;
  }
  
  /**
   * Retourner la provenance de la colonne tarif de la ligne de vente.
   * @return Provenance de la colonne tarif.
   */
  public EnumProvenanceColonneTarif getProvenanceColonneTarif() {
    return provenanceColonneTarif;
  }
  
  /**
   * Renseigner la provenance de la colonne tarif de la ligne de vente.
   * @param pProvenanceColonneTarif Provenance de la colonne tarif.
   */
  public void setProvenanceColonneTarif(EnumProvenanceColonneTarif pProvenanceColonneTarif) {
    provenanceColonneTarif = pProvenanceColonneTarif;
  }
  
  /**
   * Retourner le numéro de la colonne tarif de la ligne de vente.
   * 
   * Les valeurs 1 à 10 désignent les colonnes tarifs 1 à 10. La valeur 0 signifie qu'il n'y aucune colonne tarif. La valeur null
   * signifie que l'information n'a pas été renseignée.
   * 
   * @return Numéro de la colonne tarif (0=aucune, 1-10=colonne tarif, null=non renseigné).
   */
  public Integer getNumeroColonneTarif() {
    return numeroColonneTarif;
  }
  
  /**
   * Modifier le numéro de la colonne tarif de la ligne de vente.
   * @param pNumeroColonneTarif Numéro de colonne tarif.
   */
  public void setNumeroColonneTarif(Integer pNumeroColonneTarif) {
    numeroColonneTarif = pNumeroColonneTarif;
  }
  
  /**
   * Retourner la provenance du prix de base (HT et TTC) de la ligne de vente.
   * @return Provenance du prix de base.
   */
  public EnumProvenancePrixBase getProvenancePrixBase() {
    return provenancePrixBase;
  }
  
  /**
   * Modifier la provenance du prix de base (HT et TTC) de la ligne de vente.
   * @param pProvenancePrixBase Provenance du prix de base.
   */
  public void setProvenancePrixBase(EnumProvenancePrixBase pProvenancePrixBase) {
    provenancePrixBase = pProvenancePrixBase;
  }
  
  /**
   * Retourner le prix de base HT de la ligne de vente.
   * @return Prix de base HT.
   */
  public BigDecimal getPrixBaseHT() {
    return prixBaseHT;
  }
  
  /**
   * Modifier le prix de base HT de la ligne de vente.
   * @param pPrixBaseHT Prix de base HT.
   */
  public void setPrixBaseHT(BigDecimal pPrixBaseHT) {
    prixBaseHT = pPrixBaseHT;
  }
  
  /**
   * Retourner le prix de base TTC de la ligne de vente.
   * @return Prix de base TTC.
   */
  public BigDecimal getPrixBaseTTC() {
    return prixBaseTTC;
  }
  
  /**
   * Modifier le prix de base TTC de la ligne de vente.
   * @param pPrixBaseHT Prix de base TTC.
   */
  public void setPrixBaseTTC(BigDecimal pPrixBaseTTC) {
    prixBaseTTC = pPrixBaseTTC;
  }
  
  /**
   * Retourner la provenance des taux de remises de la ligne de vente.
   * @return Provenance des taux de remises.
   */
  public EnumProvenanceTauxRemise getProvenanceTauxRemise() {
    return provenanceTauxRemise;
  }
  
  /**
   * Modifier la provenance des taux de remises de la ligne de vente.
   * @param pProvenanceTauxRemise Provenance des taux de remises.
   */
  public void setProvenanceTauxRemise(EnumProvenanceTauxRemise pProvenanceTauxRemise) {
    provenanceTauxRemise = pProvenanceTauxRemise;
  }
  
  /**
   * Indiquer si au moins un taux de remise est présent.
   * @return true=il y a au moins un taux de remise, false=sinon.
   */
  public boolean isTauxRemisePresent() {
    return tauxRemise1 != null || tauxRemise2 != null || tauxRemise3 != null || tauxRemise4 != null || tauxRemise5 != null
        || tauxRemise6 != null;
  }
  
  /**
   * Retourner le taux de remise, dont l'indice est mentionné en paramètre, de la ligne de vente.
   * @param pIndex Indice du taux de remise à modifier (entre 1 et 6).
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise(int pIndex) {
    if (pIndex == 1) {
      return getTauxRemise1();
    }
    else if (pIndex == 2) {
      return getTauxRemise2();
    }
    else if (pIndex == 3) {
      return getTauxRemise3();
    }
    else if (pIndex == 4) {
      return getTauxRemise4();
    }
    else if (pIndex == 5) {
      return getTauxRemise5();
    }
    else if (pIndex == 6) {
      return getTauxRemise6();
    }
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise, dont l'indice est mentionné en paramètre, de la ligne de vente.
   * @param pTauxRemise Taux de remise.
   * @param pIndex Indice du taux de remise à modifier.
   */
  public void setTauxRemise(BigPercentage pTauxRemise, int pIndex) {
    if (pIndex == 1) {
      setTauxRemise1(pTauxRemise);
    }
    else if (pIndex == 2) {
      setTauxRemise2(pTauxRemise);
    }
    else if (pIndex == 3) {
      setTauxRemise3(pTauxRemise);
    }
    else if (pIndex == 4) {
      setTauxRemise4(pTauxRemise);
    }
    else if (pIndex == 5) {
      setTauxRemise5(pTauxRemise);
    }
    else if (pIndex == 6) {
      setTauxRemise6(pTauxRemise);
    }
  }
  
  /**
   * Retourner le taux de remise 1 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise1() {
    return tauxRemise1;
  }
  
  /**
   * Modifier le taux de remise 1 de la ligne de vente.
   * @param pTauxRemise1 Taux de remise.
   */
  public void setTauxRemise1(BigPercentage pTauxRemise1) {
    tauxRemise1 = pTauxRemise1;
  }
  
  /**
   * Retourner le taux de remise 2 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise2() {
    return tauxRemise2;
  }
  
  /**
   * Modifier le taux de remise 2 de la ligne de vente.
   * @param pTauxRemise2 Taux de remise.
   */
  public void setTauxRemise2(BigPercentage pTauxRemise2) {
    tauxRemise2 = pTauxRemise2;
  }
  
  /**
   * Retourner le taux de remise 3 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise3() {
    return tauxRemise3;
  }
  
  /**
   * Modifier le taux de remise 3 de la ligne de vente.
   * @param pTauxRemise3 Taux de remise.
   */
  public void setTauxRemise3(BigPercentage pTauxRemise3) {
    tauxRemise3 = pTauxRemise3;
  }
  
  /**
   * Retourner le taux de remise 4 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise4() {
    return tauxRemise4;
  }
  
  /**
   * Modifier le taux de remise 4 de la ligne de vente.
   * @param pTauxRemise4 Taux de remise.
   */
  public void setTauxRemise4(BigPercentage pTauxRemise4) {
    tauxRemise4 = pTauxRemise4;
  }
  
  /**
   * Retourner le taux de remise 5 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise5() {
    return tauxRemise5;
  }
  
  /**
   * Modifier le taux de remise 5 de la ligne de vente.
   * @param pTauxRemise5 Taux de remise.
   */
  public void setTauxRemise5(BigPercentage pTauxRemise5) {
    tauxRemise5 = pTauxRemise5;
  }
  
  /**
   * Retourner le taux de remise 6 de la ligne de vente.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemise6() {
    return tauxRemise6;
  }
  
  /**
   * Modifier le taux de remise 6 de la ligne de vente.
   * @param pTauxRemise6 Taux de remise.
   */
  public void setTauxRemise6(BigPercentage pTauxRemise6) {
    tauxRemise6 = pTauxRemise6;
  }
  
  /**
   * Retourner le mode d'application des taux de remises de la ligne de vente.
   * @return Mode d'application des taux de remises.
   */
  public EnumModeTauxRemise getModeTauxRemise() {
    return modeTauxRemise;
  }
  
  /**
   * Modifier le mode d'application des taux de remises de la ligne de vente.
   * @param pModeTauxRemise Mode d'application des taux de remises.
   */
  public void setModeTauxRemise(EnumModeTauxRemise pModeTauxRemise) {
    modeTauxRemise = pModeTauxRemise;
  }
  
  /**
   * Retourne la base sur laquelle les taux de remises s'appliquent (prix untaire ou montant ligne).
   * @return Assiette des taux de remises.
   */
  public EnumAssietteTauxRemise getAssietteTauxRemise() {
    return assietteTauxRemise;
  }
  
  /**
   * Initialise la base sur laquelle les taux de remises s'appliquent (prix untaire ou montant ligne).
   * @param pAssietteTauxRemise Assiette des taux de remises.
   */
  public void setAssietteTauxRemise(EnumAssietteTauxRemise pAssietteTauxRemise) {
    assietteTauxRemise = pAssietteTauxRemise;
  }
  
  /**
   * Retourner la provenance du coefficient de la ligne de vente.
   * @return Provenance coefficient.
   */
  public EnumProvenanceCoefficient getProvenanceCoefficient() {
    return provenanceCoefficient;
  }
  
  /**
   * Modifier la provenance du coefficient de la ligne de vente.
   * @param pProvenanceCoefficient Provenance coefficient.
   */
  public void setProvenanceCoefficient(EnumProvenanceCoefficient pProvenanceCoefficient) {
    provenanceCoefficient = pProvenanceCoefficient;
  }
  
  /**
   * Retourner le coefficient de la ligne de vente.
   * @return Coefficient.
   */
  public BigDecimal getCoefficient() {
    return coefficient;
  }
  
  /**
   * Modifier le coeffiicient de la ligne de vente.
   * @param pCoefficient Coefficient
   */
  public void setCoefficient(BigDecimal pCoefficient) {
    coefficient = pCoefficient;
  }
  
  /**
   * Retourner l'identifiant du type de gratuit de la ligne de vente.
   * @return Identifiant du type de gratuit.
   */
  public IdTypeGratuit getIdTypeGratuit() {
    return idTypeGratuit;
  }
  
  /**
   * Modifier l'identifiant du type de gratuit de la ligne de vente.
   * @param pIdTypeGratuit Identifiant du type de gratuit.
   */
  public void setIdTypeGratuit(IdTypeGratuit pIdTypeGratuit) {
    idTypeGratuit = pIdTypeGratuit;
  }
  
  /**
   * Retourner la provenance du prix net saisi de la ligne de vente.
   * @return Provenance du prix net saisi.
   */
  public EnumProvenancePrixNet getProvenancePrixNet() {
    return provenancePrixNet;
  }
  
  /**
   * Modifier la provenance du prix net saisi de la ligne de vente.
   * @param pProvenancePrixNet Provenance du prix net saisi.
   */
  public void setProvenancePrixNet(EnumProvenancePrixNet pProvenancePrixNet) {
    provenancePrixNet = pProvenancePrixNet;
  }
  
  /**
   * Retourner le prix net HT.
   * 
   * Le prix net calculé n'est pas renseigné lorsque le prix net a été saisi par l'utilisateur. Pour obtenir le prix net de la ligne
   * de vente, il faut lire l'information dans le prix net saisi ou le prix net calculé en fonction de la provenance.
   * 
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetHT() {
    if (provenancePrixNet == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
      return prixNetSaisiHT;
    }
    return prixNetCalculeHT;
  }
  
  /**
   * Retourner le prix net TTC en fonction de la provenance.
   * 
   * Le prix net calculé n'est pas renseigné lorsque le prix net a été saisi par l'utilisateur. Pour obtenir le prix net de la ligne
   * de vente, il faut lire l'information dans le prix net saisi ou le prix net calculé en fonction de la provenance.
   * 
   * @return Prix net TTC.
   */
  public BigDecimal getPrixNetTTC() {
    if (provenancePrixNet == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
      return prixNetSaisiTTC;
    }
    return prixNetCalculeTTC;
  }
  
  /**
   * Retourner le prix net saisi HT (ou issu d'une condition de vente).
   * 
   * Ce prix net n'est pas le prix net HT de la ligne de vente (qui est stocké dans le champ L1PVC). Il contient uniquement la valeur
   * du prix net HT saisi par l'utilisateur (EnumProvenancePrixNetHT = SAISIE_LIGNE_VENTE dans ce cas) ou le prix net HT issu d'une
   * condition de vente (EnumProvenancePrixNetHT = CONDITION_VENTE dans ce cas). Il faut le voir comme une valeur intermédiaire.
   * Cette valeur n'a pas de pendant dans l'algorithme Java.
   * 
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetSaisiHT() {
    return prixNetSaisiHT;
  }
  
  /**
   * Modifier le prix net saisi HT (ou issu d'une condition de vente).
   * @param pPrixNetSaisiHT Prix net HT.
   */
  public void setPrixNetSaisiHT(BigDecimal pPrixNetSaisiHT) {
    prixNetSaisiHT = pPrixNetSaisiHT;
  }
  
  /**
   * Retourner le prix net saisi TTC (ou issu d'une condition de vente).
   * 
   * Ce prix net n'est pas le prix net TTC de la ligne de vente. Il contient uniquement la valeur du prix net TTC saisi par l'utilisateur
   * (EnumProvenancePrixNetHT = SAISIE_LIGNE_VENTE dans ce cas) ou le prix net TTC issu d'une condition de vente
   * (EnumProvenancePrixNetHT = CONDITION_VENTE dans ce cas). Il faut le voir comme une valeur intermédiaire. Cette valeur n'a pas de
   * pendant dans l'algorithme Java.
   * 
   * @return Prix net TTC.
   */
  public BigDecimal getPrixNetSaisiTTC() {
    return prixNetSaisiTTC;
  }
  
  /**
   * Modifier le prix net saisi TTC (ou issu d'une condition de vente).
   * @param pPrixNetSaisiTTC Prix net TTC.
   */
  public void setPrixNetSaisiTTC(BigDecimal pPrixNetSaisiTTC) {
    prixNetSaisiTTC = pPrixNetSaisiTTC;
  }
  
  /**
   * Retourner le prix net HT dit "calculé" de la ligne de vente.
   * 
   * C'est le prix net HT de la ligne de vente (qui est stocké dans le champ L1PVC). C'est ce prix net HT qui est affiché et appliqué
   * au client. Le RPG utilise ce prix net pour le calcul du montant HT. Cela correspond aux méthodes getPrixNetHT() de l'algorithme
   * Java.
   * 
   * Noter que l'algorithme Java n'a pas besoin de la valeur RPG puisque c'est ce qu'il doit recalculer. Cette valeur est présente dans
   * cette classe uniquement pour le programme de test qui compare les calculs de prix RPG et Java.
   * 
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetCalculeHT() {
    return prixNetCalculeHT;
  }
  
  /**
   * Modifier le prix net HT de la ligne de vente.
   * @param pPrixNetCalculeHT Prix net HT.
   */
  public void setPrixNetCalculeHT(BigDecimal pPrixNetCalculeHT) {
    prixNetCalculeHT = pPrixNetCalculeHT;
  }
  
  /**
   * Retourner le prix net TTC dit "calculé" de la ligne de vente.
   * 
   * C'est le prix net TTC de la ligne de vente (qui est stocké dans l'extension 6). C'est ce prix net TTC qui est affiché et appliqué
   * au client. Le RPG utilise ce prix net pour le calcul du montant TTC. Cela correspond aux méthodes getPrixNetTTC() de l'algorithme
   * Java.
   * 
   * Noter que l'algorithme Java n'a pas besoin de la valeur RPG puisque c'est ce qu'il doit recalculer. Cette valeur est présente dans
   * cette classe uniquement pour le programme de test qui compare les calculs de prix RPG et Java.
   * 
   * @return Prix net HT.
   */
  public BigDecimal getPrixNetCalculeTTC() {
    return prixNetCalculeTTC;
  }
  
  /**
   * Modifier le prix net TTC de la ligne de vente.
   * @param pPrixNetCalculeTTC Prix net TTC.
   */
  public void setPrixNetCalculeTTC(BigDecimal pPrixNetCalculeTTC) {
    prixNetCalculeTTC = pPrixNetCalculeTTC;
  }
  
  /**
   * Retourner le numéro de la colonne du taux de TVA issu de l'établissement.
   * Les taux de TVA correspondant doivent être lus dans l'établisssement.
   * @return Numéro de la colonne de TVA (entre 1 et 6).
   */
  public Integer getNumeroTVAEtablissement() {
    return numeroTVAEtablissement;
  }
  
  /**
   * Modifier le numéro de la colonne du taux de TVA issu de l'établissement.
   * Les 2 TVA sont liés et donc renseignent le même taux (tant que des modifications de la DG ne sont pas effectuées).
   * @param pNumeroTVAEtablissement
   */
  public void setNumeroTVAEtablissement(Integer pNumeroTVAEtablissement) {
    numeroTVAEtablissement = pNumeroTVAEtablissement;
  }
  
  /**
   * Retourner le numéro de la colonne TVA issu de l'entête du document de vente.
   * Les taux de TVA correspondant doivent être lus dans l'entête du document de vente.
   * @return Numéro de la colonne de TVA (entre 1 et 3).
   */
  public Integer getNumeroTVAEntete() {
    return numeroTVAEntete;
  }
  
  /**
   * Modifier le numéro de la colonne TVA issu de l'entête du document de vente.
   * @param pNumeroVAEntete Taux de TVA.
   */
  public void setNumeroTVAEntete(Integer pNumeroVAEntete) {
    numeroTVAEntete = pNumeroVAEntete;
  }
  
  /**
   * Retourner le montant HT qui a été persisté dans la ligne de vente.
   * 
   * Ce montant ne sert pas pour le calcul. Il sert pour l'outil de test afin de comparer le résultat du calcul RPG et le résultat du
   * calcul Java.
   * 
   * @return Montant HT.
   */
  public BigDecimal getMontantHT() {
    return montantHT;
  }
  
  /**
   * Modifier le montant HT qui a été persisté dans la ligne de vente.
   * @param pMontantHT Montant HT.
   */
  public void setMontantHT(BigDecimal pMontantHT) {
    this.montantHT = pMontantHT;
  }
}
