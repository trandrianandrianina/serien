/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;

/**
 * Cette classe contient les critères qui permettent de rechercher les articles liés d'un article donné.
 */
public class CritereArticleLie extends CriteresBaseRecherche {
  // Variables
  private IdArticle idArticle = null;
  // Permet de filtrer les articles liés ayant le même fournisseur que l'article principal
  private IdFournisseur idFournisseur = null;
  private Boolean isFiltreTousFournisseur = false;
  private Boolean isArticleValideSeulement = null;
  
  /**
   * Constructeur.
   */
  public CritereArticleLie(IdArticle pIdArticle) {
    setIdArticle(pIdArticle);
    if (pIdArticle != null) {
      setIdEtablissement(pIdArticle.getIdEtablissement());
    }
  }
  
  /**
   * Constructeur.
   */
  public CritereArticleLie(IdArticle pIdArticle, IdFournisseur pIdFournisseur) {
    this(pIdArticle);
    setIdFournisseur(pIdFournisseur);
  }
  
  // -- Accesseurs
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public void setIdArticle(IdArticle idArticle) {
    this.idArticle = idArticle;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
  
  /**
   * Modifier le critère qui détermine si on filtre les articles liés sur tous les fournisseurs
   */
  public void setFiltreTousFournisseur(Boolean pFiltreTousFournisseur) {
    if (pFiltreTousFournisseur != null) {
      isFiltreTousFournisseur = pFiltreTousFournisseur;
    }
  }
  
  /**
   * Récupérer le critère qui détermine si on filtre les articles liés sur tous les fournisseurs
   */
  public Boolean isFiltreTousFournisseur() {
    return isFiltreTousFournisseur;
  }
  
  /**
   * Retourner si la recherche concerne les seuls articles valides (A1TOP == 0)
   */
  public Boolean isArticleValideSeulement() {
    return isArticleValideSeulement;
  }
  
  /**
   * Modifier si la recherche doit concerner les seuls articles valides (A1TOP == 0)
   */
  public void setIsArticleValideSeulement(Boolean pIsArticleValideSeulement) {
    isArticleValideSeulement = pIsArticleValideSeulement;
    if (isArticleValideSeulement == null) {
      isArticleValideSeulement = false;
    }
  }
  
}
