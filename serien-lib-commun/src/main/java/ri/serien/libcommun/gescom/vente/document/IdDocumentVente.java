/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un document de ventes.
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 * 
 * Cet identifiant est complexe car il permet d'identifier à la fois des devis, des commandes, des bons et des factures. Ces quatre types
 * de documents dont stockés dans la même table dans la base de données. De plus, plusieurs types de documents peuvent être stockés sur
 * la même ligne (une seule ligne peut représenter un bon et une facture). Voici les valeurs des champs de la base de données attendues
 * en fonction du type de document.
 * 
 * Pour un devis :
 * - E1ETB = Code établissement
 * - E1COD = 'D'
 * - E1NUM = Numéro de devis
 * - E1SUF = Suffixe
 * - E1NFA = 0
 * 
 * Pour une commande :
 * - E1ETB = Code établissement
 * - E1COD = 'E'
 * - E1NUM = Numéro de commande
 * - E1SUF = Suffixe
 * - E1NFA = 0
 * 
 * Pour un bon :
 * - E1ETB = Code établissement
 * - E1COD = 'E'
 * - E1NUM = Numéro de bon
 * - E1SUF = Suffixe
 * - E1NFA = 0
 * 
 * Pour une facture :
 * - E1ETB = Code établissement
 * - E1COD = Null
 * - E1NUM = 0
 * - E1SUF = 0
 * - E1NFA = Numéro de facture
 */
public class IdDocumentVente extends AbstractIdAvecEtablissement {
  public static final int LONGUEUR_ENTETE = 1;
  public static final int LONGUEUR_NUMERO = 6;
  public static final int LONGUEUR_SUFFIXE = 1;
  public static final int LONGUEUR_NUMERO_COMPLET = 8;
  public static final int LONGUEUR_FACTURE = 7;
  public static final int LONGUEUR_INDICATIF = LONGUEUR_ENTETE + LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_NUMERO + LONGUEUR_SUFFIXE;
  public static final int LONGUEUR_INDICATIF_FACTURE = LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_FACTURE;
  public static final int NUMERO_FACTURE_NON_FACTURABLE = 9999999;
  public static final int INDICATIF_NUM = 0;
  public static final int INDICATIF_ENTETE_ETB_NUM_SUF = 1;
  public static final int INDICATIF_ENTETE_NUM_SUF = 2;
  public static final int INDICATIF_FACTURE = 3;
  private final EnumCodeEnteteDocumentVente codeEntete;
  private final Integer numero;
  private final Integer suffixe;
  private final Integer numeroFacture;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé pour un identifiant complet.
   * @param pEnumEtatObjetMetier Etat de l'identifiant pour la persistance.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pCodeEntete Code en-tête du document.
   * @param pNumero Numéro du document.
   * @param pSuffixe Suffixe du document.
   */
  private IdDocumentVente(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement,
      EnumCodeEnteteDocumentVente pCodeEntete, Integer pNumero, Integer pSuffixe) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    codeEntete = controlerCodeEnteteDocumentVente(pCodeEntete);
    numero = controlerNumero(pNumero);
    suffixe = controlerSuffixe(pSuffixe);
    numeroFacture = null;
  }
  
  /**
   * Constructeur privé pour un identifiant de type facture.
   * @param pEnumEtatObjetMetier Etat de l'identifiant pour la persistance.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pNumeroFacture Numéro de la facture.
   */
  private IdDocumentVente(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, Integer pNumeroFacture) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    codeEntete = null;
    numero = null;
    suffixe = null;
    numeroFacture = controlerNumeroFacture(pNumeroFacture);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   * Le numéro et le suffixe ne sont pas à fournir. Ils sont renseignés avec la valeur 0 dans ce cas.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pCodeEntete Code en-tête du document de vente.
   */
  private IdDocumentVente(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentVente pCodeEntete) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    codeEntete = controlerCodeEnteteDocumentVente(pCodeEntete);
    numero = 0;
    suffixe = 0;
    numeroFacture = null;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public int hashCode() {
    int code = 17;
    if (isIdFacture()) {
      code = 37 * code + getCodeEtablissement().hashCode();
      code = 37 * code + numeroFacture.hashCode();
    }
    else {
      code = 37 * code + getCodeEtablissement().hashCode();
      code = 37 * code + codeEntete.hashCode();
      code = 37 * code + numero.hashCode();
      code = 37 * code + suffixe.hashCode();
    }
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdDocumentVente)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de document de vente.");
    }
    IdDocumentVente id = (IdDocumentVente) pObject;
    
    if (isIdFacture()) {
      return (id.isIdFacture() && numeroFacture.equals(id.getNumeroFacture()));
    }
    else {
      return getCodeEtablissement().equals(id.getCodeEtablissement()) && codeEntete.equals(id.codeEntete) && numero.equals(id.numero)
          && suffixe.equals(id.suffixe);
    }
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdDocumentVente)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdDocumentVente id = (IdDocumentVente) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = codeEntete.compareTo(id.codeEntete);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numero.compareTo(id.numero);
    if (comparaison != 0) {
      return comparaison;
    }
    return suffixe.compareTo(id.suffixe);
  }
  
  /**
   * Retourner l'affichage standard de l'identifiant du document de vente.<br><br>
   * L'affichage standard est celui qui doit être utilisé dans l'essentiel des situations pour désigner un document de ventes.<br>
   * Le format choisi est :<br>
   * - "NuméroDocument-Suffixe" pour un devis, une commande ou un bon.<br>
   * - "NuméroFacture" pour une facture.<br>
   * L'établissement n'est pas précisé car il est implicite dans la majorité des contextes. L'entête n'est pas non plus précisé car on
   * part du principe que l'on sait si l'identifiant concerne un devis, un bon, une commande ou une facture. On considère que ce sont
   * des espaces de numérotation distinct (au même titre que client et fournisseur par exemple).
   */
  @Override
  public String getTexte() {
    if (isIdFacture()) {
      return "" + numeroFacture;
    }
    else {
      return "" + numero + SEPARATEUR_ID + suffixe;
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Créer un identifiant générique qui va traiter le cas des documents de type devis, commande et bon de manière dissociée du document de
   * type facture.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pCodeEntete Code en-tête du document de ventes.
   * @param pNumero Numéro du document.
   * @param pSuffixe Suffixe du document.
   * @param pNumeroFacture Numéro de la facture.
   * @return L'identifiant du document de ventes.
   */
  public static IdDocumentVente getInstanceGenerique(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentVente pCodeEntete,
      Integer pNumero, Integer pSuffixe, Integer pNumeroFacture) {
    // On interprète les données transmises pour identifier le type de document : notre cas ci dessous est celui de la facture
    // Si le numéro de facture vaut 9999999 alors il doit être considéré comme un bon et non une facture (car non facturable)
    if (pNumeroFacture != null && pNumeroFacture > 0 && pNumeroFacture < NUMERO_FACTURE_NON_FACTURABLE) {
      return new IdDocumentVente(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pNumeroFacture);
    }
    else {
      return new IdDocumentVente(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCodeEntete, pNumero, pSuffixe);
    }
  }
  
  /**
   * Créer un identifiant pour un devis, une commande ou un bon (tout sauf facture).
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pCodeEntete Code en-tête du document de ventes.
   * @param pNumero Numéro du document.
   * @param pSuffixe Suffixe du document.
   * @return L'identifiant du document de ventes.
   */
  public static IdDocumentVente getInstanceHorsFacture(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentVente pCodeEntete,
      Integer pNumero, Integer pSuffixe) {
    return new IdDocumentVente(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCodeEntete, pNumero, pSuffixe);
  }
  
  /**
   * Créer un identifiant pour une facture.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pNumeroFacture Numéro de la facture.
   * @return L'identifiant du document de ventes.
   */
  public static IdDocumentVente getInstancePourFacture(IdEtablissement pIdEtablissement, Integer pNumeroFacture) {
    return new IdDocumentVente(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pNumeroFacture);
  }
  
  /**
   * Créer un identifiant à partir de l'indicatif d'un document de ventes hors facture.
   * @param pIndicatif Indicatif du document de ventes.
   * @return L'identifiant du document de ventes.
   */
  public static IdDocumentVente getInstanceParIndicatif(String pIndicatif) {
    return new IdDocumentVente(EnumEtatObjetMetier.MODIFIE, extraireEtablissementDeIndicatif(pIndicatif),
        extraireEnteteDeIndicatif(pIndicatif), extraireNumeroDeIndicatif(pIndicatif), extraireSuffixeDeIndicatif(pIndicatif));
  }
  
  /**
   * Créer un identifiant à partir de l'indicatif d'une facture.
   * @param pIndicatif Indicatif de la facture.
   * @return L'identifiant du document de ventes.
   */
  public static IdDocumentVente getInstancePourFactureParIndicatif(String pIndicatif) {
    return new IdDocumentVente(EnumEtatObjetMetier.MODIFIE, extraireEtablissementDeIndicatifFacture(pIndicatif),
        extraireNumeroFactureDeIndicatifFacture(pIndicatif));
  }
  
  /**
   * Créer un nouvel identifiant, pas encore persisté en base de données.<br>
   * Il sera défini lors de l'insertion en base de données ou par un service RPG.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pCodeEntete Code en-tête du document de ventes.
   * @return L'identifiant du document de ventes.
   */
  public static IdDocumentVente getInstanceAvecCreationId(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentVente pCodeEntete) {
    return new IdDocumentVente(pIdEtablissement, pCodeEntete);
  }
  
  /**
   * Générer l'idenfiant d'origine d'un document.<br>
   * L'identifiant d'origine d'un document est le même que l'identifiant du document si ce n'est que l'entête est en minuscule au lieu
   * d'être en majucscule.
   * @param pIdDocumentVente Identifiant du document de ventes.
   * @return L'identifiant d'origine du document.
   */
  public static IdDocumentVente getInstanceIdOrigine(IdDocumentVente pIdDocumentVente) {
    switch (pIdDocumentVente.getCodeEntete()) {
      case DEVIS:
        if (pIdDocumentVente.isExistant()) {
          return new IdDocumentVente(EnumEtatObjetMetier.MODIFIE, pIdDocumentVente.getIdEtablissement(),
              EnumCodeEnteteDocumentVente.DEVIS_ORIGINE, pIdDocumentVente.getNumero(), pIdDocumentVente.getSuffixe());
        }
        else {
          return new IdDocumentVente(EnumEtatObjetMetier.CREE, pIdDocumentVente.getIdEtablissement(),
              EnumCodeEnteteDocumentVente.DEVIS_ORIGINE, pIdDocumentVente.getNumero(), pIdDocumentVente.getSuffixe());
        }
        
      case COMMANDE_OU_BON:
        if (pIdDocumentVente.isExistant()) {
          return new IdDocumentVente(EnumEtatObjetMetier.MODIFIE, pIdDocumentVente.getIdEtablissement(),
              EnumCodeEnteteDocumentVente.COMMANDE_OU_BON_ORIGINE, pIdDocumentVente.getNumero(), pIdDocumentVente.getSuffixe());
        }
        else {
          return new IdDocumentVente(EnumEtatObjetMetier.CREE, pIdDocumentVente.getIdEtablissement(),
              EnumCodeEnteteDocumentVente.COMMANDE_OU_BON_ORIGINE, pIdDocumentVente.getNumero(), pIdDocumentVente.getSuffixe());
        }
        
      default:
        throw new MessageErreurException("Impossible de générer l'identifiant d'origine d'un document pour ce type de document.");
    }
  }
  
  /**
   * Vérifier la validité de l'identifiant.<br>
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   * @param pIdDocumentVente Identifiant du document de ventes.
   * @param pVerifierExistance Indicatif de vérification d'existance.
   * @return L'identifiant du document de ventes.
   */
  public static IdDocumentVente controlerId(IdDocumentVente pIdDocumentVente, boolean pVerifierExistance) {
    if (pIdDocumentVente == null) {
      throw new MessageErreurException("L'identifiant du document de ventes est invalide.");
    }
    
    if (pVerifierExistance && !pIdDocumentVente.isExistant()) {
      throw new MessageErreurException(
          "L'identifiant du document de ventes n'existe pas dans la base de données : " + pIdDocumentVente.toString());
    }
    return pIdDocumentVente;
  }
  
  /**
   * Retourner l'indicatif du document en fonction du type souhaité.
   * @param pTypeIndicatif Type de l'indicatif.
   * @return L'indicatif du document.
   */
  public String getIndicatif(int pTypeIndicatif) {
    switch (pTypeIndicatif) {
      case INDICATIF_NUM:
        if (isIdFacture()) {
          return String.format("%0" + LONGUEUR_FACTURE + "d", numeroFacture);
        }
        return String.format("%0" + LONGUEUR_NUMERO + "d", numero);
      case INDICATIF_ENTETE_ETB_NUM_SUF:
        if (isIdFacture()) {
          throw new MessageErreurException("L'indicatif demandé concerne un document de vente pas une facture.");
        }
        return String.format("%c%" + LONGUEUR_CODE_ETABLISSEMENT + "s%0" + LONGUEUR_NUMERO + "d%0" + LONGUEUR_SUFFIXE + "d",
            codeEntete.getCode(), getCodeEtablissement(), numero, suffixe);
      case INDICATIF_ENTETE_NUM_SUF:
        if (isIdFacture()) {
          throw new MessageErreurException("L'indicatif demandé concerne un document de vente pas une facture.");
        }
        return String.format("%c%0" + LONGUEUR_NUMERO + "d%0" + LONGUEUR_SUFFIXE + "d", codeEntete.getCode(), numero, suffixe);
      case INDICATIF_FACTURE:
        if (!isIdFacture()) {
          throw new MessageErreurException("L'indicatif demandé concerne une facture pas un document de vente.");
        }
        return String.format("%" + LONGUEUR_CODE_ETABLISSEMENT + "s%0" + LONGUEUR_FACTURE + "d", getCodeEtablissement(), numeroFacture);
    }
    return "";
  }
  
  /**
   * Indiquer si l'identifiant de ce document de ventes est un identifiant de type devis.
   * @return
   *         true si l'identifiant est de type devis, false sinon.
   */
  public boolean isIdDevis() {
    return Constantes.equals(codeEntete, EnumCodeEnteteDocumentVente.DEVIS)
        || Constantes.equals(codeEntete, EnumCodeEnteteDocumentVente.DEVIS_ORIGINE);
  }
  
  /**
   * Indiquer si l'identifiant de ce document de ventes est un identifiant de type commande ou bon.
   * @return
   *         true si l'identifiant est de type commande, false sinon.
   */
  public boolean isIdCommandeOuBon() {
    return Constantes.equals(codeEntete, EnumCodeEnteteDocumentVente.COMMANDE_OU_BON)
        || Constantes.equals(codeEntete, EnumCodeEnteteDocumentVente.COMMANDE_OU_BON_ORIGINE);
  }
  
  /**
   * Indiquer si l'identifiant de ce document de ventes est un identifiant de type facture.
   * @return
   *         true si l'identifiant est de type facture, false sinon.
   */
  public boolean isIdFacture() {
    return numeroFacture != null && numeroFacture > 0;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Contrôler la validité du code entête du document.
   * @param pValeur Code en-tête du document de ventes à valider.
   * @return Le code en-tête du document de ventes.
   */
  private static EnumCodeEnteteDocumentVente controlerCodeEnteteDocumentVente(EnumCodeEnteteDocumentVente pValeur) {
    // Renseigner l'indicatif tiers
    if (pValeur == null) {
      throw new MessageErreurException("Le code entête du document n'est pas renseigné.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du numéro du document.<br>
   * Il doit être supérieur à zéro et doit comporter au maximum 6 chiffres (entre 1 et 999 999).
   * @param pValeur Numéro du document à valider.
   * @return Le numéro du document.
   */
  private Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du document n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du document est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du document est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du suffixe du document.<br>
   * Il doit être supérieur ou égal à zéro et doit comporter au maximum 1 chiffre (entre 0 et 9).
   * @param pValeur Suffixe à valider.
   * @return Le suffixe du document.
   */
  private Integer controlerSuffixe(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le suffixe du document n'est pas renseigné.");
    }
    if (pValeur < 0 || pValeur > 9) {
      throw new MessageErreurException("Le suffixe du document soit être compris entre 0 et 9 inclus.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_SUFFIXE)) {
      throw new MessageErreurException("Le suffixe du document est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du numéro de facture du document de vente type "Facture".
   * @param pValeur Numéro de facture à valider.
   * @return Le numéro de la facture.
   */
  private Integer controlerNumeroFacture(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de facture du document n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro de facture du document doit être supérieur à 0.");
    }
    if (pValeur == NUMERO_FACTURE_NON_FACTURABLE) {
      throw new MessageErreurException("Le numéro de facture doit être différent de " + NUMERO_FACTURE_NON_FACTURABLE
          + " car ce numéro est réservé aux bons non facturables.");
    }
    if (pValeur > NUMERO_FACTURE_NON_FACTURABLE) {
      throw new MessageErreurException("Le numéro de facture du document est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler l'indicatif d'un document.
   * @param pValeur Indicatif du document.
   * @return L'indicatif du document.
   */
  private static String controlerIndicatif(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("L'indicatif du document n'est pas renseigné.");
    }
    if (pValeur.length() < LONGUEUR_INDICATIF) {
      throw new MessageErreurException("L'indicatif du document n'est pas correct car il est inférieur à la longueur attendu.");
    }
    return Constantes.normerTexte(pValeur);
  }
  
  /**
   * Contrôler l'indicatif d'une facture.
   * @param pValeur Indicatif de la facture.
   * @return L'indicatif de la facture.
   */
  private static String controlerIndicatifFacture(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("L'indicatif de la facture n'est pas renseigné.");
    }
    if (pValeur.length() < LONGUEUR_INDICATIF_FACTURE) {
      throw new MessageErreurException("L'indicatif de la facture n'est pas correct car il est inférieur à la longueur attendu."
          + " Cet indicatif est composé du code établissement suivi du numéro de la facture pour une longueur totale de "
          + LONGUEUR_INDICATIF_FACTURE + '.');
    }
    return Constantes.normerTexte(pValeur);
  }
  
  /**
   * Extraire le code en-tête de l'indicatif du document.
   * @param pIndicatif Indicatif du code en-tête du document.
   * @return Le code en-tête de l'indicatif du document.
   */
  private static EnumCodeEnteteDocumentVente extraireEnteteDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    return EnumCodeEnteteDocumentVente.valueOfByCode(pIndicatif.charAt(0));
  }
  
  /**
   * Extraire le code établissement de l'indicatif du document.
   * @param pIndicatif Indicatif du code établissement du document.
   * @return L'identifiant de l'établissement.
   */
  private static IdEtablissement extraireEtablissementDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    int offsetDeb = LONGUEUR_ENTETE;
    int offsetFin = offsetDeb + LONGUEUR_CODE_ETABLISSEMENT;
    return IdEtablissement.getInstance(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  /**
   * Extraire le numéro de l'indicatif du document.
   * @param pIndicatif Indicatif du numéro du document.
   * @return Le numéro du document.
   */
  private static Integer extraireNumeroDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    int offsetDeb = LONGUEUR_ENTETE + LONGUEUR_CODE_ETABLISSEMENT;
    int offsetFin = offsetDeb + LONGUEUR_NUMERO;
    return Integer.valueOf(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  /**
   * Extraire le suffixe de l'indicatif du document.
   * @param pIndicatif Indicatif du suffixe du document.
   * @return Le suffixe du document.
   */
  private static Integer extraireSuffixeDeIndicatif(String pIndicatif) {
    pIndicatif = controlerIndicatif(pIndicatif);
    int offsetDeb = LONGUEUR_ENTETE + LONGUEUR_CODE_ETABLISSEMENT + LONGUEUR_NUMERO;
    int offsetFin = offsetDeb + LONGUEUR_SUFFIXE;
    return Integer.valueOf(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  /**
   * Extraire le code établissement de l'indicatif d'une facture.
   * @param pIndicatif Indicatif du code établissement d'une facture.
   * @return L'identifiant de l'établissement.
   */
  private static IdEtablissement extraireEtablissementDeIndicatifFacture(String pIndicatif) {
    pIndicatif = controlerIndicatifFacture(pIndicatif);
    int offsetDeb = 0;
    int offsetFin = offsetDeb + LONGUEUR_CODE_ETABLISSEMENT;
    return IdEtablissement.getInstance(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  /**
   * Extraire le numéro facture de l'indicatif d'une facture.
   * @param pIndicatif Indicatif du numéro de facture.
   * @return Le numéro de la facture.
   */
  private static Integer extraireNumeroFactureDeIndicatifFacture(String pIndicatif) {
    pIndicatif = controlerIndicatifFacture(pIndicatif);
    int offsetDeb = LONGUEUR_CODE_ETABLISSEMENT;
    int offsetFin = offsetDeb + LONGUEUR_FACTURE;
    return Integer.valueOf(pIndicatif.substring(offsetDeb, offsetFin));
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le code entête du document de ventes.<br>
   * Le code entête peut être null dans le cas d'un identifiant de facture.
   * @return Le code en-tête du document.
   */
  public EnumCodeEnteteDocumentVente getCodeEntete() {
    return codeEntete;
  }
  
  /**
   * Retourner l'en-tête du document de ventes.<br>
   * Retourne la lettre correspondant au code entête du document de ventes.
   * Le retour est ' ' dans le cas d'un identifiant de facture car c'est ce qui attendu par les programmes RPG.
   * @return L'en-tête du document.
   */
  public Character getEntete() {
    if (codeEntete == null) {
      return null;
    }
    return codeEntete.getCode();
  }
  
  /**
   * Retourner le numéro du document.<br>
   * Le numéro du document est compris entre entre 0 et 999 999.
   * Le numéro du document est égal à 0 dans le cas d'un identifiant de facture.
   * @return Le numéro du document.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Retourner le suffixe du document.<br>
   * Le suffixe est égal à 0 dans le cas d'un identifiant de facture.
   * @return Le suffixe du document.
   */
  public Integer getSuffixe() {
    return suffixe;
  }
  
  /**
   * Retourner le numéro de facture du document de vente de type facture.<br>
   * Ce champ n'est implémenté que dans le cas d'un id de document de type facture.
   * @return Le numéro de facture.
   */
  public Integer getNumeroFacture() {
    return numeroFacture;
  }
  
}
