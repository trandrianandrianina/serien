/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;

/**
 * Liste de LigneAchatBase.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de lignes d'achat de base.
 */
public class ListeLigneAchatBase extends ListeClasseMetier<IdLigneAchat, LigneAchatBase, ListeLigneAchatBase> {
  
  /**
   * Constructeur.
   */
  public ListeLigneAchatBase() {
  }
  
  // -- Méthodes publiques
  
  /**
   * Chargement de la liste des LigneAchatArticleBase correspondant aux identifiants fournis
   */
  @Override
  public ListeLigneAchatBase charger(IdSession pIdSession, List<IdLigneAchat> pListeId) {
    return ManagerServiceDocumentAchat.chargerListeLigneAchatBase(pIdSession, pListeId);
  }
  
  /**
   * Chargement de la liste des LigneAchatArticleBase d'un établissement
   */
  @Override
  public ListeLigneAchatBase charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return null;
  }
  
  /**
   * Construire une liste de LigneAchatArticleBase correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des lignes de base ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeLigneAchatBase creerListeNonChargee(List<IdLigneAchat> pListeIdLigneAchat) {
    ListeLigneAchatBase listeLigneAchatBase = new ListeLigneAchatBase();
    if (pListeIdLigneAchat != null) {
      for (IdLigneAchat idLigneAchat : pListeIdLigneAchat) {
        LigneAchatBase ligneAchatArticleBase = new LigneAchatBase(idLigneAchat);
        ligneAchatArticleBase.setCharge(false);
        listeLigneAchatBase.add(ligneAchatArticleBase);
      }
    }
    return listeLigneAchatBase;
  }
  
}
