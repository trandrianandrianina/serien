/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'édition pour les bons et les commandes.
 * 
 */
public enum EnumTypeEdition {
  EDITION_CHIFFREE(' ', "Edition avec totaux"),
  EDITION_NON_CHIFFREE('1', "Edition non chiffrée"),
  EDITION_SANS_TOTAUX('2', "Edition sans totaux");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeEdition(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return Integer.toString(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeEdition valueOfByCode(Character pCode) {
    for (EnumTypeEdition value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type d'édition est invalide : " + pCode);
  }
  
}
