/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typegratuit;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un type de gratuit.
 *
 * L'identifiant est composé du type de gratuit et n'est pas liée à l'établissement.
 * Il est constitué de 1 caractère et se présente sous la forme A
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique
 * facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdTypeGratuit extends AbstractId {
  private static final int LONGUEUR_CODE = 1;
  private static final char CODE_NON_GRATUIT = ' ';
  public static final IdTypeGratuit NON_GRATUIT = IdTypeGratuit.getInstance(CODE_NON_GRATUIT);
  
  // Variables
  private final Character code;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdTypeGratuit(EnumEtatObjetMetier pEnumEtatObjetMetier, Character pCode) {
    super(pEnumEtatObjetMetier);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir du code sur 1 caractères.
   * @param pCode Code du type de gratuit.
   * @return Identifiant du type de gratuit.
   */
  public static IdTypeGratuit getInstance(Character pCode) {
    return new IdTypeGratuit(EnumEtatObjetMetier.MODIFIE, pCode);
  }
  
  /**
   * Créer un identifiant à partir d'une chaîne de caractères.
   * @param pCode Code du type de gratuit (doit être sur 1 caractères).
   * @return Identifiant du type de gratuit.
   */
  public static IdTypeGratuit getInstance(String pCode) {
    if (pCode == null) {
      throw new MessageErreurException("Le code du type de gratuit n'est pas renseigné.");
    }
    if (pCode.trim().length() > LONGUEUR_CODE) {
      throw new MessageErreurException("Le code du type de gratuit doit être sur 1 caractère : " + pCode);
    }
    return new IdTypeGratuit(EnumEtatObjetMetier.MODIFIE, pCode.charAt(0));
  }
  
  /**
   * Contrôler la validité du type gratuit.
   * Le code du type gratuit est une chaîne numérique de 1 caractère.
   */
  private static Character controlerCode(Character pCode) {
    if (pCode == null) {
      throw new MessageErreurException("Le code du type de gratuit n'est pas renseigné.");
    }
    return pCode;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdTypeGratuit)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux types de gratuit.");
    }
    IdTypeGratuit id = (IdTypeGratuit) pObject;
    return code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdTypeGratuit)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdTypeGratuit id = (IdTypeGratuit) pObject;
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return "" + code;
  }
  
  /**
   * Retourner le code du type de gratuit.
   * @return Code du type de gratuit.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Indiquer si le type de gratuit est renseigné (différent de ' ').
   * @return true=le type d egrauti est renseigné, le type de gratuit n'est pas renseigné.
   */
  public boolean isGratuit() {
    return code != CODE_NON_GRATUIT;
  }
}
