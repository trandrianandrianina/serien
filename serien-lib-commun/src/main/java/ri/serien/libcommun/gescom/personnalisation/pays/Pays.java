/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.pays;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Pays.
 * Les pays n'est pas lié à un établissement.
 */
public class Pays extends AbstractClasseMetier<IdPays> {
  // Variables
  private String libelle = null;
  
  /**
   * Constructeur
   */
  public Pays(IdPays pId) {
    super(pId);
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Retourne si le pays en cours correspond à la France.
   */
  public boolean isFrance() {
    if (id.getTexte().equalsIgnoreCase("FR") || (libelle != null && libelle.equalsIgnoreCase("FRANCE"))) {
      return true;
    }
    return false;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le nom du pays.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifie le nom du pays.
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
}
