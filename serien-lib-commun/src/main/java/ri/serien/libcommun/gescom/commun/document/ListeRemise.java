/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste de remises fournisseurs.
 */
public class ListeRemise implements Serializable {
  // Variables
  private BigDecimal pourcentageRemise1 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise2 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise3 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise4 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise5 = BigDecimal.ZERO;
  private BigDecimal pourcentageRemise6 = BigDecimal.ZERO;
  private Character typeRemise = null;
  private Character baseRemise = null;
  
  // -- Accesseurs
  
  /**
   * Type de remise ligne.
   * Valeur : 1=cascade.
   * Correspond aux champs TRL de la base de données.
   */
  public Character getTypeRemise() {
    return typeRemise;
  }
  
  /**
   * Modifier le type de remise.
   */
  public void setTypeRemise(Character pTypeRemise) {
    typeRemise = pTypeRemise;
  }
  
  /**
   * Base de remise ligne.
   * 1=Montant
   */
  public Character getBaseRemise() {
    return baseRemise;
  }
  
  /**
   * Modifier la base de remise.
   */
  public void setBaseRemise(Character pBaseRemise) {
    baseRemise = pBaseRemise;
  }
  
  /**
   * Pourcentage de remise 1 (2 chiffres après la virgules).
   */
  public BigDecimal getPourcentageRemise1() {
    return pourcentageRemise1;
  }
  
  /**
   * Modifier le pourcentage de remise 1.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise1(BigDecimal pRemise) {
    // Contrôler la valeur fournie
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 1 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_99) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 1 doit être compris entre 0 et 99%.");
    }
    
    // Arrondir la valeur
    pourcentageRemise1 = pRemise.setScale(PrixAchat.NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Pourcentage de remise 2 (2 chiffres après la virgules).
   */
  public BigDecimal getPourcentageRemise2() {
    return pourcentageRemise2;
  }
  
  /**
   * Modifier le pourcentage de remise 2 (2 chiffres après la virgules).
   */
  public void setPourcentageRemise2(BigDecimal pRemise) {
    // Contrôler la valeur fournie
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 2 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_99) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 2 doit être compris entre 0 et 99%.");
    }
    
    // Arrondir la valeur
    pourcentageRemise2 = pRemise.setScale(PrixAchat.NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Pourcentage de remise 3 (2 chiffres après la virgules).
   */
  public BigDecimal getPourcentageRemise3() {
    return pourcentageRemise3;
  }
  
  /**
   * Modifier le pourcentage de remise 3.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise3(BigDecimal pRemise) {
    // Contrôler la valeur fournie
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 3 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_99) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 3 doit être compris entre 0 et 99%.");
    }
    
    // Arrondir la valeur
    pourcentageRemise3 = pRemise.setScale(PrixAchat.NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Pourcentage de remise 4 (2 chiffres après la virgules).
   */
  public BigDecimal getPourcentageRemise4() {
    return pourcentageRemise4;
  }
  
  /**
   * Modifier le pourcentage de remise 4.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise4(BigDecimal pRemise) {
    // Contrôler la valeur fournie
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 4 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_99) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 4 doit être compris entre 0 et 99%.");
    }
    
    // Arrondir la valeur
    pourcentageRemise4 = pRemise.setScale(PrixAchat.NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Pourcentage de remise 5 (2 chiffres après la virgules).
   */
  public BigDecimal getPourcentageRemise5() {
    return pourcentageRemise5;
  }
  
  /**
   * Modifier le pourcentage de remise 5.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise5(BigDecimal pRemise) {
    // Contrôler la valeur fournie
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 5 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_99) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 5 doit être compris entre 0 et 99%.");
    }
    
    // Arrondir la valeur
    pourcentageRemise5 = pRemise.setScale(PrixAchat.NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
  
  /**
   * Pourcentage de remise 6 (2 chiffres après la virgules).
   */
  public BigDecimal getPourcentageRemise6() {
    return pourcentageRemise6;
  }
  
  /**
   * Modifier le pourcentage de remise 6.
   * La valeur fournie est arrondie à 2 chiffres après la virgule.
   */
  public void setPourcentageRemise6(BigDecimal pRemise) {
    // Contrôler la valeur fournie
    if (pRemise == null) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 6 est invalide.");
    }
    if (pRemise.compareTo(BigDecimal.ZERO) < 0 || pRemise.compareTo(Constantes.VALEUR_99) > 0) {
      throw new MessageErreurException("Le pourcentage de remise fournisseur 6 doit être compris entre 0 et 99%.");
    }
    
    // Arrondir la valeur
    pourcentageRemise6 = pRemise.setScale(PrixAchat.NOMBRE_DECIMALE_REMISE, RoundingMode.HALF_UP);
  }
}
