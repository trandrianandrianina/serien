/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockmouvement;

import java.math.BigDecimal;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceStock;

/**
 * Liste des mouvements de stocks.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de StockMouvement.
 * 
 */
public class ListeStockMouvement extends ListeClasseMetier<IdStockMouvement, StockMouvement, ListeStockMouvement> {
  /**
   * Constructeurs.
   */
  public ListeStockMouvement() {
  }
  
  @Override
  public ListeStockMouvement charger(IdSession pIdSession, List<IdStockMouvement> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  @Override
  public ListeStockMouvement charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * 
   * Charger la liste des mouvements de stock à partir de critères.
   * 
   * @param pIdSession
   * @param pCritereStockMouvement
   * @return ListeStockMouvement
   */
  public static ListeStockMouvement chargerListeStockMouvement(IdSession pIdSession, CritereStockMouvement pCritereStockMouvement) {
    return ManagerServiceStock.chargerListeStockMouvement(pIdSession, pCritereStockMouvement);
  }
  
  /**
   * Retourner le dernier stock physique d'un magasin à partir des mouvements de stock.
   * 
   * @param pIdMagasin Identifiant du magasin dont on veut récupérer le dernier stock physique.
   * @return Stock physique.
   */
  public BigDecimal getDernierStockPhysique(IdMagasin pIdMagasin) {
    // Ne pas retourner le stock physique si le magasin n'est pas renseigné
    if (pIdMagasin == null) {
      return null;
    }
    
    // Parcourir les mouvements de stock à l'envers pour trouver le dernier mouvement de stock du magasin
    for (int i = size() - 1; i >= 0; i--) {
      StockMouvement stockMouvement = get(i);
      if (stockMouvement.getId().getIdMagasin().equals(pIdMagasin)) {
        return stockMouvement.getStockPhysique();
      }
    }
    
    // Le stock physique est égal à 0 s'il n'y aucun mouvement d'inventaire
    return BigDecimal.ZERO;
  }
}
