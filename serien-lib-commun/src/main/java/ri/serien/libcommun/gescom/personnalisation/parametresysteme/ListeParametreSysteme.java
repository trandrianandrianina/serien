/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.parametresysteme;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.EnumTypeValeurPersonnalisation;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste des paramètres systèmes.
 */
public class ListeParametreSysteme extends ListeClasseMetier<IdParametreSysteme, ParametreSysteme, ListeParametreSysteme> {
  
  @Override
  public ListeParametreSysteme charger(IdSession pIdSession, List<IdParametreSysteme> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charger tous les paramètres systèmes de l'établissement donné.
   */
  @Override
  public ListeParametreSysteme charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    IdSession.controlerId(pIdSession, true);
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    return ManagerServiceParametre.chargerListeParametreSysteme(pIdSession, pIdEtablissement);
  }
  
  /**
   * Vérifier si les paramètres systèmes d'un établissement ont déjà été chargés.
   */
  public boolean isEtablissementCharge(IdEtablissement pIdEtablissement) {
    for (ParametreSysteme parametreSysteme : this) {
      if (parametreSysteme.getId().getIdEtablissement().equals(pIdEtablissement)) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Retoune le paramètre système à partir de son numéro ou null si non trouvé.
   */
  public ParametreSysteme getParametreSysteme(IdEtablissement pIdEtablissement, EnumParametreSysteme pEnumParametreSysteme) {
    // Contrôler les paramètres
    IdEtablissement.controlerId(pIdEtablissement, true);
    if (pEnumParametreSysteme == null) {
      throw new MessageErreurException("Le paramètre système est invalide.");
    }
    
    // Rechercher le paramètre système dans la liste
    for (ParametreSysteme parametreSysteme : this) {
      if (parametreSysteme.getId().getIdEtablissement().equals(pIdEtablissement)
          && parametreSysteme.getEnumParametreSysteme() == pEnumParametreSysteme) {
        return parametreSysteme;
      }
    }
    
    // Retourner null si on ne l'a pas trouvé
    return null;
  }
  
  /**
   * Retourne la valeur d'un paramètre système.
   */
  public Character getValeurParametreSysteme(IdEtablissement pIdEtablissement, EnumParametreSysteme pEnumParametreSysteme) {
    // Contrôler les paramètres
    IdEtablissement.controlerId(pIdEtablissement, true);
    if (pEnumParametreSysteme == null) {
      throw new MessageErreurException("Le paramètre système est invalide.");
    }
    
    // Récupérer le paramètre système
    ParametreSysteme parametreSysteme = getParametreSysteme(pIdEtablissement, pEnumParametreSysteme);
    if (parametreSysteme == null) {
      throw new MessageErreurException(
          "Le paramètre système " + pEnumParametreSysteme + " est introuvable pour l'établissement " + pIdEtablissement);
    }
    
    // Retourner la valeur
    return parametreSysteme.getValeur();
  }
  
  /**
   * Vérifier si un paramètre système de type A est actif dans la liste des paramètres systèmes.
   */
  public boolean isParametreSystemeActif(IdEtablissement pIdEtablissement, EnumParametreSysteme pEnumParametreSysteme) {
    // Contrôler les paramètres
    IdEtablissement.controlerId(pIdEtablissement, true);
    if (pEnumParametreSysteme == null) {
      throw new MessageErreurException("Le paramètre système est invalide.");
    }
    if (pEnumParametreSysteme.getTypeValeur() != EnumTypeValeurPersonnalisation.TYPE_A) {
      throw new MessageErreurException("Le paramètre système n'est pas de type " + EnumTypeValeurPersonnalisation.TYPE_A.getCode());
    }
    
    // Récupérer le paramètre système
    ParametreSysteme parametreSysteme = getParametreSysteme(pIdEtablissement, pEnumParametreSysteme);
    if (parametreSysteme == null) {
      throw new MessageErreurException(
          "Le paramètre système " + pEnumParametreSysteme + " est introuvable pour l'établissement + " + pIdEtablissement);
    }
    
    // Vérifier que la valeur du paramètre système est égale à '1' pour être déclaré actif
    return parametreSysteme.getValeur().compareTo('1') == 0;
  }
  
}
