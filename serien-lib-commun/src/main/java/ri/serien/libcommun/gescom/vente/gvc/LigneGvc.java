/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.article.GroupeArticles;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.StatutArticle;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.vente.gvc.pricejet.TarifPriceJet;
import ri.serien.libcommun.outils.Constantes;

/**
 *
 *
 */
public class LigneGvc implements Serializable {
  // Constantes
  private final static BigDecimal VALUE_0v01 = new BigDecimal(0.01);
  private final static BigDecimal VALUE_100 = new BigDecimal(100);
  // Statuts de modification de la ligne (pas de modif, vient d'être modifié, tout juste sauvegardé, alerte tarif, erreur saisie)
  public final static int STATUT_LIGNE_AUCUN = 0;
  public final static int STATUT_LIGNE_MODIF = 1;
  public final static int STATUT_LIGNE_VALID = 2;
  public final static int STATUT_LIGNE_ALERT = 3;
  public final static int STATUT_LIGNE_ERREUR = 4;
  public final static String STATUT_LIB_AUCUN = "Article initial";
  public final static String STATUT_LIB_MODIF = "Article modifié";
  public final static String STATUT_LIB_VALID = "Article validé";
  public final static String STATUT_LIB_ALERT = "Alerte";
  public final static String STATUT_LIB_ERREUR = "Erreur";
  
  // Etat web de l'article
  // Article hors ligne = EXCLU DU WEB dans Série N A1IN15 N'a jamais été mise en ligne WEB
  public static final String ARTICLE_NON_WEB = "1";
  // En ligne A1IN15 = ' ' Web et visible
  public static final String ARTICLE_WEB_EN_LIGNE = " ";
  // Article Web mais invisible
  public static final String ARTICLE_WEB_HORS_LIGNE = "2";
  
  private static int NB_LIGNES_REQUETE = 0;
  
  // Attributs généraux
  public BigDecimal tauxTVA;
  public LigneGvc articleIniTial = null;
  private boolean aEteModifie = false;
  
  // Attributs de définition d'article
  private IdArticle idArticle = null;
  private String libelleArticle = null;
  
  private GroupeArticles groupeArticles = null;
  private Famille familleArticles = null;
  private ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille sousFamille = null;
  private String referenceFabricant = null;
  private String marque = null;
  
  private Fournisseur fournisseurPrincipal = null;
  
  private ConditionAchatGVC cnaFrsPrincipal = null;
  
  private ConditionAchatGVC cnaDistribRef = null;
  
  private Integer nbPointsWatt = null;
  
  // Attributs de gestion évolutifs
  // Date des dernieres modifs de tarif
  private String dateTarifs = null;
  // TARIF 1 PUBLIC
  private BigDecimal prixPublicHT = null;
  private BigDecimal prixPublicTTC = null;
  // TARIF 2 PARTICULIER
  private BigDecimal prixParticulierHT = null;
  private BigDecimal prixParticulierTTC = null;
  private BigDecimal remiseParticulier = null;
  private BigDecimal remiseParticulierPourcent = null;
  // TARIF 3 PROMO
  private BigDecimal prixPromoHT = null;
  private BigDecimal prixPromoTTC = null;
  private BigDecimal remisePromo = null;
  private BigDecimal remisePromoPourcent = null;
  // Prix moins cher entre particulier et promo
  private BigDecimal prixMoinsCher = null;
  private BigDecimal taux = null;
  // TARIF 4 ARTISAN NON ELEC
  private BigDecimal prixArtisNelecHT = null;
  private BigDecimal prixArtisNelecTTC = null;
  private BigDecimal tauxArtisNelec = null;
  private BigDecimal remiseArtisNelec = null;
  private BigDecimal remiseArtisNelecPourcent = null;
  // TARIF 5 ARTISAN ELEC
  private BigDecimal prixArtisanHT = null;
  private BigDecimal prixArtisanTTC = null;
  private BigDecimal tauxArtisan = null;
  private BigDecimal remiseArtisan = null;
  private BigDecimal remiseArtisanPourcent = null;
  // TARIF 6 REVENDEUR
  private BigDecimal prixRevendeurHT = null;
  private BigDecimal prixRevendeurTTC = null;
  private BigDecimal tauxRevendeur = null;
  private BigDecimal remiseRevendeur = null;
  private BigDecimal remiseRevendeurPourcent = null;
  // TARIF 7 GROSSISTE
  private BigDecimal prixGrossisteHT = null;
  private BigDecimal prixGrossisteTTC = null;
  private BigDecimal tauxGrossiste = null;
  private BigDecimal remiseGrossiste = null;
  private BigDecimal remiseGrossistePourcent = null;
  // TARIF 8 GSB + NEGOCE
  private BigDecimal prixGsbHT = null;
  private BigDecimal prixGsbTTC = null;
  private BigDecimal tauxGsb = null;
  private BigDecimal remiseGsb = null;
  private BigDecimal remiseGsbPourcent = null;
  // TARIF 9 ADMINISTRATION
  private BigDecimal prixAdminiHT = null;
  private BigDecimal prixAdminiTTC = null;
  private BigDecimal tauxAdmini = null;
  private BigDecimal remiseAdmini = null;
  private BigDecimal remiseAdminiPourcent = null;
  
  // TARIF 10 INTERNE
  private BigDecimal prixInterneHT = null;
  private BigDecimal prixInterneTTC = null;
  private BigDecimal tauxInterne = null;
  private BigDecimal remiseInterne = null;
  private BigDecimal remiseInternePourcent = null;
  
  // Veille prix GSB
  private BigDecimal veillePrixGsb = null;
  private String veilleNomGsb = null;
  private String veilleDateGsb = null;
  // WEB
  private TarifPriceJet tarifPjet = null;
  
  private String dateModifPrix = null;
  private BigDecimal prixFabricantHT = null;
  private BigDecimal DEEE = null;
  
  private BigDecimal H1PRV = null;
  private BigDecimal H1PMP = null;
  
  private BigDecimal prixDeRevient = null;
  private BigDecimal pump = null;
  
  // Si le pump n'est pas encore valorisé -> prix de revient pour calculer la marge
  private BigDecimal prixAchat = null;
  
  // WORKFLOW
  private WorkFlowGVC workFlowGVC = null;
  // L'article est-il en ligne sur Magento
  private String gestionWeb = null;
  // Statuts de modification des données
  private int statutLigne = STATUT_LIGNE_AUCUN;
  // Les libellés du statut de la ligne (sert surtout à retranscrire les alertes et les
  private ArrayList<String> libelleStatutsLigne = null;
  
  // Etat de l'article dans Série N
  private StatutArticle statutArticle = null;
  
  /**
   * Constructeur standard de l'article GVC
   */
  public LigneGvc() {
    libelleStatutsLigne = new ArrayList<String>();
    
    // TODO infoUser = ainfoUser;
    gestionWeb = ARTICLE_NON_WEB;
  }
  
  /*************************** COMPARAISON *************************/
  
  /**
   * Constructeur UNIQUEMENT dans le cadre de l'article initial
   */
  public LigneGvc(LigneGvc articleInit) {
    if (articleInit == null) {
      return;
    }
    
    // Ces valeurs serviront uniquement de valeurs de contrôle
    idArticle = articleInit.getIdArticle();
    nbPointsWatt = articleInit.getNbPointsWatt();
    
    if (articleInit.getPrixPublicHT() != null) {
      prixPublicHT = new BigDecimal(articleInit.getPrixPublicHT().toString());
    }
    
    if (articleInit.getPrixParticulierHT() != null) {
      prixParticulierHT = new BigDecimal(articleInit.getPrixParticulierHT().toString());
    }
    if (articleInit.getRemiseParticulier() != null) {
      remiseParticulier = new BigDecimal(articleInit.getRemiseParticulier().toString());
    }
    
    if (articleInit.getPrixPromoHT() != null) {
      prixPromoHT = new BigDecimal(articleInit.getPrixPromoHT().toString());
    }
    if (articleInit.getRemisePromo() != null) {
      remisePromo = new BigDecimal(articleInit.getRemisePromo().toString());
    }
    
    if (articleInit.getTaux() != null) {
      taux = new BigDecimal(articleInit.getTaux().toString());
    }
    
    if (articleInit.getPrixArtisNelecHT() != null) {
      prixArtisNelecHT = new BigDecimal(articleInit.getPrixArtisNelecHT().toString());
    }
    if (articleInit.getRemiseArtisNelec() != null) {
      remiseArtisNelec = new BigDecimal(articleInit.getRemiseArtisNelec().toString());
    }
    if (articleInit.getTauxArtisNelec() != null) {
      tauxArtisNelec = new BigDecimal(articleInit.getTauxArtisNelec().toString());
    }
    
    if (articleInit.getPrixArtisanHT() != null) {
      prixArtisanHT = new BigDecimal(articleInit.getPrixArtisanHT().toString());
    }
    if (articleInit.getRemiseArtisan() != null) {
      remiseArtisan = new BigDecimal(articleInit.getRemiseArtisan().toString());
    }
    if (articleInit.getTauxArtisan() != null) {
      tauxArtisan = new BigDecimal(articleInit.getTauxArtisan().toString());
    }
    
    if (articleInit.getPrixRevendeurHT() != null) {
      prixRevendeurHT = new BigDecimal(articleInit.getPrixRevendeurHT().toString());
    }
    if (articleInit.getRemiseRevendeur() != null) {
      remiseRevendeur = new BigDecimal(articleInit.getRemiseRevendeur().toString());
    }
    if (articleInit.getTauxRevendeur() != null) {
      tauxRevendeur = new BigDecimal(articleInit.getTauxRevendeur().toString());
    }
    
    if (articleInit.getPrixGrossisteHT() != null) {
      prixGrossisteHT = new BigDecimal(articleInit.getPrixGrossisteHT().toString());
    }
    if (articleInit.getRemiseGrossiste() != null) {
      remiseGrossiste = new BigDecimal(articleInit.getRemiseGrossiste().toString());
    }
    if (articleInit.getTauxGrossiste() != null) {
      tauxGrossiste = new BigDecimal(articleInit.getTauxGrossiste().toString());
    }
    
    if (articleInit.getPrixGsbHT() != null) {
      prixGsbHT = new BigDecimal(articleInit.getPrixGsbHT().toString());
    }
    if (articleInit.getRemiseGsb() != null) {
      remiseGsb = new BigDecimal(articleInit.getRemiseGsb().toString());
    }
    if (articleInit.getTauxGsb() != null) {
      tauxGsb = new BigDecimal(articleInit.getTauxGsb().toString());
    }
    
    if (articleInit.getPrixAdminiHT() != null) {
      prixAdminiHT = new BigDecimal(articleInit.getPrixAdminiHT().toString());
    }
    if (articleInit.getRemiseAdmini() != null) {
      remiseAdmini = new BigDecimal(articleInit.getRemiseAdmini().toString());
    }
    if (articleInit.getTauxAdmini() != null) {
      tauxAdmini = new BigDecimal(articleInit.getTauxAdmini().toString());
    }
    
    if (articleInit.getPrixInterneHT() != null) {
      prixInterneHT = new BigDecimal(articleInit.getPrixInterneHT().toString());
    }
    if (articleInit.getRemiseInterne() != null) {
      remiseInterne = new BigDecimal(articleInit.getRemiseInterne().toString());
    }
    if (articleInit.getTauxInterne() != null) {
      tauxInterne = new BigDecimal(articleInit.getTauxInterne().toString());
    }
    
    // WORKFLOW
    if (articleInit.getWorkFlowGVC() != null) {
      workFlowGVC = new WorkFlowGVC();
      workFlowGVC.setEtatWF(new EtatWorkFlow(articleInit.getWorkFlowGVC().getEtatWF().getCodeEtat()));
      workFlowGVC.setExpediteur(articleInit.getWorkFlowGVC().getExpediteur());
      workFlowGVC.setDestinataire(articleInit.getWorkFlowGVC().getDestinataire());
    }
    
    // Veille GSB
    veillePrixGsb = articleInit.getVeillePrixGsb();
    veilleNomGsb = articleInit.getVeilleNomGsb();
    
    gestionWeb = articleInit.getGestionWeb();
  }
  
  /**
   * On met à jour l'article initial sur la base des données de l'article parent
   * Cette méthode sert essentiellement après une validation en base de données de l'article
   */
  public void majArticleInitial() {
    articleIniTial = new LigneGvc(this);
    aEteModifie = false;
  }
  
  /**
   * On va gérer les statuts de la ligne d'article sur des comparaisons logiques mathématiques : modifié, alerte tarif, erreur, sauvegardé
   */
  private void gererLesStatutsDeLaLigne() {
    // ++++++++++++++ LES MODIFICATIONS
    if (articleIniTial != null) {
      majStatutsLigne(STATUT_LIGNE_AUCUN, null);
      
      // Prix publics
      if ((articleIniTial.getPrixPublicHT() != null && prixPublicHT.compareTo(articleIniTial.getPrixPublicHT()) != 0)
          || (articleIniTial.getPrixPublicHT() == null && prixPublicHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix public HT");
      }
      
      // Prix particulier
      if ((articleIniTial.getPrixParticulierHT() != null && prixParticulierHT.compareTo(articleIniTial.getPrixParticulierHT()) != 0)
          || (articleIniTial.getPrixParticulierHT() == null && prixParticulierHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix particulier HT");
      }
      
      // Prix Promo
      if ((articleIniTial.getPrixPromoHT() != null && prixPromoHT.compareTo(articleIniTial.getPrixPromoHT()) != 0)
          || (articleIniTial.getPrixPromoHT() == null && prixPromoHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix promo HT");
      }
      
      // Prix artisan non ELEC
      if ((articleIniTial.getPrixArtisNelecHT() != null && prixArtisNelecHT.compareTo(articleIniTial.getPrixArtisNelecHT()) != 0)
          || (articleIniTial.getPrixArtisNelecHT() == null && prixArtisNelecHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix artisan Non ELEC");
      }
      
      // Prix artisan ELEC
      if ((articleIniTial.getPrixArtisanHT() != null && prixArtisanHT.compareTo(articleIniTial.getPrixArtisanHT()) != 0)
          || (articleIniTial.getPrixArtisanHT() == null && prixArtisanHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix artisan ELEC HT");
      }
      
      // Prix Revendeur
      if ((articleIniTial.getPrixRevendeurHT() != null && prixRevendeurHT.compareTo(articleIniTial.getPrixRevendeurHT()) != 0)
          || (articleIniTial.getPrixRevendeurHT() == null && prixRevendeurHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix Revendeur HT");
      }
      
      // Prix Grossiste
      if ((articleIniTial.getPrixGrossisteHT() != null && prixGrossisteHT.compareTo(articleIniTial.getPrixGrossisteHT()) != 0)
          || (articleIniTial.getPrixGrossisteHT() == null && prixGrossisteHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix grossiste HT");
      }
      
      // Prix GSB/NEGOCE
      if ((articleIniTial.getPrixGsbHT() != null && prixGsbHT.compareTo(articleIniTial.getPrixGsbHT()) != 0)
          || (articleIniTial.getPrixGsbHT() == null && prixGsbHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix GSB HT");
      }
      
      // Prix Administration
      if ((articleIniTial.getPrixAdminiHT() != null && prixAdminiHT.compareTo(articleIniTial.getPrixAdminiHT()) != 0)
          || (articleIniTial.getPrixAdminiHT() == null && prixAdminiHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix Administration HT");
      }
      
      // Prix Interne
      if ((articleIniTial.getPrixInterneHT() != null && prixInterneHT.compareTo(articleIniTial.getPrixInterneHT()) != 0)
          || (articleIniTial.getPrixInterneHT() == null && prixInterneHT != null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Prix Interne HT");
      }
      
      // WORKFLOW
      if (workFlowGVC != null) {
        // Le workflow initial n'existait pas
        if (articleIniTial.getWorkFlowGVC() == null) {
          majStatutsLigne(STATUT_LIGNE_MODIF, "Nouveau WorkFlow");
        }
        // le workflow initial existe
        else {
          // On dépiote les différents arguments du WorkFlow
          // etat du WF
          if (!workFlowGVC.getEtatWF().equals(articleIniTial.getWorkFlowGVC().getEtatWF())) {
            majStatutsLigne(STATUT_LIGNE_MODIF, "Etat du WorkFlow");
          }
          // Expediteur
          if (articleIniTial.getWorkFlowGVC().getExpediteur() == null && workFlowGVC.getExpediteur() != null) {
            majStatutsLigne(STATUT_LIGNE_MODIF, "Profil demandeur");
          }
          else if (articleIniTial.getWorkFlowGVC().getExpediteur() != null && workFlowGVC.getExpediteur() == null) {
            majStatutsLigne(STATUT_LIGNE_MODIF, "Profil demandeur");
          }
          else if (articleIniTial.getWorkFlowGVC().getExpediteur() != null && workFlowGVC.getExpediteur() != null
              && !workFlowGVC.getExpediteur().equals(articleIniTial.getWorkFlowGVC().getExpediteur())) {
            majStatutsLigne(STATUT_LIGNE_MODIF, "Profil demandeur");
          }
          // Destinataire
          if (articleIniTial.getWorkFlowGVC().getDestinataire() == null && workFlowGVC.getDestinataire() != null) {
            majStatutsLigne(STATUT_LIGNE_MODIF, "Profil destinataire");
          }
          else if (articleIniTial.getWorkFlowGVC().getDestinataire() != null && workFlowGVC.getDestinataire() == null) {
            majStatutsLigne(STATUT_LIGNE_MODIF, "Profil destinataire");
          }
          else if (articleIniTial.getWorkFlowGVC().getDestinataire() != null && workFlowGVC.getDestinataire() != null
              && !workFlowGVC.getDestinataire().equals(articleIniTial.getWorkFlowGVC().getDestinataire())) {
            majStatutsLigne(STATUT_LIGNE_MODIF, "Profil destinataire");
          }
        }
      }
      
      if (articleIniTial.getGestionWeb() != null && gestionWeb != null && !articleIniTial.getGestionWeb().equals(gestionWeb)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "mise en ligne");
      }
      // Points Watt
      if (aModifiePointsWatt()) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Points Watt");
      }
      
      // Veille GSB
      if ((articleIniTial.getVeillePrixGsb() != null && veillePrixGsb != null && veillePrixGsb.compareTo(articleIniTial
          .getVeillePrixGsb()) != 0)
          || (articleIniTial.getVeillePrixGsb() == null && veillePrixGsb != null)
          || (articleIniTial.getVeillePrixGsb() != null && veillePrixGsb == null)) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Veille prix GSB");
      }
      
      if (articleIniTial.getVeilleNomGsb() == null && veilleNomGsb != null && !veilleNomGsb.trim().equals("")) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Veille nom GSB");
      }
      else if (articleIniTial.getVeilleNomGsb() != null && veilleNomGsb == null && !articleIniTial.getVeilleNomGsb().trim().equals("")) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Veille nom GSB");
      }
      else if (articleIniTial.getVeilleNomGsb() != null && veilleNomGsb != null && !veilleNomGsb.equals(articleIniTial.getVeilleNomGsb())) {
        majStatutsLigne(STATUT_LIGNE_MODIF, "Veille nom GSB");
      }
    }
    
    // On met l'article en modification même si derrière le statut de la ligne peut être en alerte
    aEteModifie = (statutLigne == STATUT_LIGNE_MODIF);
    
    // ++++++++++++++ LES ALERTES
    if (prixGsbHT != null && prixParticulierHT != null) {
      if (prixGsbHT.compareTo(prixParticulierHT) == 1) {
        majStatutsLigne(STATUT_LIGNE_ALERT, "Prix HT GSB > Prix HT Particulier");
      }
    }
  }
  
  /**
   * On met à jour le statut de la ligne mais aussi le libellé du statut correspondant
   */
  public void majStatutsLigne(int statut, String texte) {
    if (libelleStatutsLigne == null) {
      libelleStatutsLigne = new ArrayList<String>();
    }
    
    // On change de statut
    if (statut != statutLigne) {
      statutLigne = statut;
      libelleStatutsLigne.clear();
      switch (statut) {
        case STATUT_LIGNE_AUCUN:
          libelleStatutsLigne.add(STATUT_LIB_AUCUN);
          break;
        case STATUT_LIGNE_MODIF:
          libelleStatutsLigne.add(STATUT_LIB_MODIF);
          break;
        case STATUT_LIGNE_ALERT:
          libelleStatutsLigne.add(STATUT_LIB_ALERT);
          break;
        case STATUT_LIGNE_VALID:
          libelleStatutsLigne.add(STATUT_LIB_VALID);
          break;
        case STATUT_LIGNE_ERREUR:
          libelleStatutsLigne.add(STATUT_LIB_ERREUR);
          break;
        
        default:
          break;
      }
    }
    
    if (texte != null) {
      libelleStatutsLigne.add(texte);
    }
  }
  
  /**
   * Générer la bonne valeur DB2 de la gestion Web à partir de
   */
  public void gererMiseEnLigne(boolean isEnLigne) {
    if (isEnLigne) {
      gestionWeb = ARTICLE_WEB_EN_LIGNE;
    }
    else {
      // Si l'article n'a jamais été mise sur le web on garde cette notion
      if (articleIniTial.getGestionWeb().equals(ARTICLE_NON_WEB)) {
        gestionWeb = ARTICLE_NON_WEB;
        // Sinon on le met Web mais hors ligne
      }
      else {
        gestionWeb = ARTICLE_WEB_HORS_LIGNE;
      }
    }
    
    gererLesStatutsDeLaLigne();
  }
  
  /**
   * Les tarifs de l'article sont ils en modification par rapport à l'article initial
   */
  public boolean aModifieSesTarifs() {
    // Prix publics
    if ((articleIniTial.getPrixPublicHT() != null && prixPublicHT.compareTo(articleIniTial.getPrixPublicHT()) != 0)
        || (articleIniTial.getPrixPublicHT() == null && prixPublicHT != null)) {
      return true;
    }
    // Prix particulier
    if ((articleIniTial.getPrixParticulierHT() != null && prixParticulierHT.compareTo(articleIniTial.getPrixParticulierHT()) != 0)
        || (articleIniTial.getPrixParticulierHT() == null && prixParticulierHT != null)) {
      return true;
    }
    // Prix Promo
    if ((articleIniTial.getPrixPromoHT() != null && prixPromoHT.compareTo(articleIniTial.getPrixPromoHT()) != 0)
        || (articleIniTial.getPrixPromoHT() == null && prixPromoHT != null)) {
      return true;
    }
    // Prix artisan Non ELEC
    if ((articleIniTial.getPrixArtisNelecHT() != null && prixArtisNelecHT.compareTo(articleIniTial.getPrixArtisNelecHT()) != 0)
        || (articleIniTial.getPrixArtisNelecHT() == null && prixArtisNelecHT != null)) {
      return true;
    }
    // Prix artisan ELEC
    if ((articleIniTial.getPrixArtisanHT() != null && prixArtisanHT.compareTo(articleIniTial.getPrixArtisanHT()) != 0)
        || (articleIniTial.getPrixArtisanHT() == null && prixArtisanHT != null)) {
      return true;
    }
    // Prix Revendeur
    if ((articleIniTial.getPrixRevendeurHT() != null && prixRevendeurHT.compareTo(articleIniTial.getPrixRevendeurHT()) != 0)
        || (articleIniTial.getPrixRevendeurHT() == null && prixRevendeurHT != null)) {
      return true;
    }
    // Prix Grossiste
    if ((articleIniTial.getPrixGrossisteHT() != null && prixGrossisteHT.compareTo(articleIniTial.getPrixGrossisteHT()) != 0)
        || (articleIniTial.getPrixGrossisteHT() == null && prixGrossisteHT != null)) {
      return true;
    }
    // Prix GSB
    if ((articleIniTial.getPrixGsbHT() != null && prixGsbHT.compareTo(articleIniTial.getPrixGsbHT()) != 0)
        || (articleIniTial.getPrixGsbHT() == null && prixGsbHT != null)) {
      return true;
    }
    // Prix Administration
    if ((articleIniTial.getPrixAdminiHT() != null && prixAdminiHT.compareTo(articleIniTial.getPrixAdminiHT()) != 0)
        || (articleIniTial.getPrixAdminiHT() == null && prixAdminiHT != null)) {
      return true;
    }
    // Prix Interne
    if ((articleIniTial.getPrixInterneHT() != null && prixInterneHT.compareTo(articleIniTial.getPrixInterneHT()) != 0)
        || (articleIniTial.getPrixInterneHT() == null && prixInterneHT != null)) {
      return true;
    }
    
    return false;
  }
  
  /**
   * Le workflow de l'article est-il en modification par rapport à l'article initial ?
   */
  public boolean aModifieSonWorkFlow() {
    if (workFlowGVC == null && articleIniTial.getWorkFlowGVC() == null) {
      return false;
    }
    
    if (workFlowGVC != null && articleIniTial.getWorkFlowGVC() == null) {
      return true;
    }
    
    if (workFlowGVC == null && articleIniTial.getWorkFlowGVC() != null) {
      return true;
    }
    
    if (workFlowGVC != null && articleIniTial.getWorkFlowGVC() != null) {
      if (workFlowGVC.getEtatWF().equals(articleIniTial.getWorkFlowGVC().getEtatWF())
          && workFlowGVC.getExpediteur().equals(articleIniTial.getWorkFlowGVC().getExpediteur())
          && workFlowGVC.getDestinataire().equals(articleIniTial.getWorkFlowGVC().getDestinataire())) {
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * La mise en ligne de l'article a-t-elle été modifiée ?
   */
  public boolean aModifieSaMiseEnLigne() {
    if (articleIniTial.getGestionWeb() != null && gestionWeb != null) {
      return !gestionWeb.equals(articleIniTial.getGestionWeb());
    }
    else {
      return false;
    }
  }
  
  /**
   * Les points Watt de l'article ont-ils été modifés ?
   */
  public boolean aModifiePointsWatt() {
    if (articleIniTial.getNbPointsWatt() != null && nbPointsWatt != null) {
      return !(nbPointsWatt.compareTo(articleIniTial.getNbPointsWatt()) == 0);
    }
    else {
      return false;
    }
  }
  
  /**
   * Les zones de la veille GSB ont elles été modifiées ?
   */
  public boolean aModifieSaVeilleGSB() {
    // Tarifs de la veille GSB
    if ((articleIniTial.getVeillePrixGsb() != null && veillePrixGsb != null
        && veillePrixGsb.compareTo(articleIniTial.getVeillePrixGsb()) != 0)
        || (articleIniTial.getVeillePrixGsb() == null && veillePrixGsb != null)
        || (articleIniTial.getVeillePrixGsb() != null && veillePrixGsb == null)) {
      return true;
    }
    // Nom du GSB correspondant
    if ((articleIniTial.getVeilleNomGsb() != null && !articleIniTial.getVeilleNomGsb().equals(veilleNomGsb))
        || (articleIniTial.getVeilleNomGsb() == null && veilleNomGsb != null)) {
      return true;
    }
    
    return false;
  }
  
  /**
   * Mettre à jour le prix moyen unitaire pondéré
   */
  public void majPump() {
    if (H1PMP == null) {
      pump = H1PMP;
    }
    else if (cnaFrsPrincipal != null) {
      pump = H1PMP.multiply(cnaFrsPrincipal.retournerCoeffUniteAchat()).setScale(4, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Mettre à jour le prix de revient standard
   * Si on a aucun prix de revient en historique
   * On prend celui de la condition d'achat du fournisseur principal
   */
  public void majPrixRevientStandard() {
    if (H1PRV == null || H1PRV.compareTo(BigDecimal.ZERO) == 0) {
      if (cnaFrsPrincipal != null) {
        prixDeRevient = cnaFrsPrincipal.retournerPrixRevientStandard().setScale(4, RoundingMode.HALF_UP);
      }
    }
    else if (cnaFrsPrincipal != null) {
      prixDeRevient = H1PRV.multiply(cnaFrsPrincipal.retournerCoeffUniteAchat()).setScale(4, RoundingMode.HALF_UP);
    }
  }
  
  /**************************************************************************
   * Méthode privées
   *************************************************************************/
  
  /**
   * Caculer un prix TTC à partir d'un prix HT.
   * @param prixHT
   * @return Prix TTC
   */
  protected BigDecimal calculerPrixTTC(BigDecimal prixHT) {
    if (prixHT == null || tauxTVA == null) {
      return null;
    }
    return tauxTVA.multiply(VALUE_0v01).add(BigDecimal.ONE).multiply(prixHT).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calculer un prix HT à partir du prix TTC.
   * 
   * @param prixTTC
   * @return Prix HT
   */
  public BigDecimal calculerPrixHTwithTTC(BigDecimal prixTTC) {
    if (prixTTC == null || tauxTVA == null) {
      return null;
    }
    
    BigDecimal tmp = tauxTVA.multiply(VALUE_0v01).add(BigDecimal.ONE);
    
    if (tmp == null || tmp.compareTo(new BigDecimal(0)) == 0) {
      return null;
    }
    
    return prixTTC.divide(tmp, 2, RoundingMode.HALF_UP);
  }
  
  /**
   * Effacer toutes les valeurs résultantes du calcul.
   */
  private void initialiserValeursCalculees() {
    majStatutsLigne(STATUT_LIGNE_AUCUN, STATUT_LIB_AUCUN);
    
    prixPublicTTC = null;
    
    prixParticulierTTC = null;
    remiseParticulier = null;
    remiseParticulierPourcent = null;
    
    prixPromoTTC = null;
    remisePromo = null;
    remisePromoPourcent = null;
    
    prixMoinsCher = null;
    prixAchat = null;
    taux = null;
    
    prixArtisNelecTTC = null;
    remiseArtisNelec = null;
    remiseArtisNelecPourcent = null;
    tauxArtisNelec = null;
    
    prixArtisanTTC = null;
    remiseArtisan = null;
    remiseArtisanPourcent = null;
    tauxArtisan = null;
    
    prixRevendeurTTC = null;
    remiseRevendeur = null;
    remiseRevendeurPourcent = null;
    tauxRevendeur = null;
    
    prixGrossisteTTC = null;
    remiseGrossiste = null;
    remiseGrossistePourcent = null;
    tauxGrossiste = null;
    
    prixGsbTTC = null;
    remiseGsb = null;
    remiseGsbPourcent = null;
    tauxGsb = null;
    
    prixAdminiTTC = null;
    remiseAdmini = null;
    remiseAdminiPourcent = null;
    tauxAdmini = null;
    
    prixInterneTTC = null;
    remiseInterne = null;
    remiseInternePourcent = null;
    tauxInterne = null;
    
    // workFlowGVC = null;
  }
  
  /**************************************************************************
   * Méthodes publiques
   *************************************************************************/
  
  /**
   * Recalculer l'ensemble des données tarifaires de notre article.
   */
  public void recalculerTout() {
    initialiserValeursCalculees();
    
    calculerPrixAchat();
    calculerPrixPublic();
    calculerPrixParticulier();
    calculerPrixPromo();
    calculerPrixMoinsCher();
    calculerTaux();
    calculerPrixArtisanNE();
    calculerPrixArtisan();
    calculerPrixRevendeur();
    calculerPrixGrossiste();
    calculerPrixGsb();
    calculerPrixAdmini();
    calculerPrixInterne();
    // TODO AncienServiceGvc.majPrixVeilleWeb(this, infoUser);
    
    // On crée un article initial pour les comparaisons de tarif
    if (articleIniTial == null) {
      articleIniTial = new LigneGvc(this);
    }
    
    gererLesStatutsDeLaLigne();
  }
  
  /**
   * ON checke s'il s'agit d'une saisie négative afin de l'interdire si nécessaire
   */
  private boolean isSaisieNegative(BigDecimal saisie) {
    return (saisie != null && saisie.compareTo(BigDecimal.ZERO) < 0);
  }
  
  /**************************************************************************
   * Calculs des tarifs publics
   *************************************************************************/
  
  /**
   * Calculer le PVTTC public et le taux public à partir du PV HH public.
   * 
   * Cette méthode est détentrice de la formule de calcul des prix publics. On doit toujours passer par elle
   * pour recalculer n'importe quel élément de prix public. Elle nécessite le PV HT public en donnée d'entrée.
   * 
   * ((PV HT public - PUMP ) / PV HT public) * 100
   */
  private void calculerPrixPublic() {
    if (prixPublicHT == null) {
      // On n'y attribue le prix fabricant
      if (prixFabricantHT != null) {
        prixPublicHT = new BigDecimal(prixFabricantHT.toString());
      }
    }
    
    prixPublicTTC = calculerPrixTTC(prixPublicHT);
  }
  
  /**
   * Saise directe du prix public HT.
   * @param prixPublicHTSaisi
   */
  public void saisirPrixPublicHT(BigDecimal prixPublicHTSaisi) {
    if (prixPublicHTSaisi == null || isSaisieNegative(prixPublicHTSaisi)) {
      return;
    }
    
    prixPublicHT = prixPublicHTSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    recalculerTout();
  }
  
  /**
   * La saisie du prix public TTC n'est qu'un moyen détourné de saisir le prix public HT.
   * L'objectif de cette méthode est de recalculer un prix public HT comme si c'était lui qui avait été saisi.
   */
  public void saisirPrixPublicTTC(BigDecimal prixPublicTTCSaisi) {
    if (prixPublicTTCSaisi == null || isSaisieNegative(prixPublicTTCSaisi)) {
      return;
    }
    
    prixPublicHT = calculerPrixHTwithTTC(prixPublicTTCSaisi);
    
    recalculerTout();
  }
  
  /**************************************************************************
   * Calculs des tarifs particuliers
   *************************************************************************/
  
  /**
   * Calculer le PVTTC particulier, la remise et le taux à partir du PV HH particulier et PUBLIC.
   * Cette méthode est détentrice de la formule de calcul des prix particulier. On doit toujours passer par elle
   * pour recalculer n'importe quel élément de prix particulier.
   * On part sur la base que toutes les infos Particulier sont mises à jour sur la base du PV HT particulier
   * il faut donc que ce dernier soit mis à jour au préalable soit par import DB2 ou saisie HT
   * soit par calcul depuis la saisie du Taux ou du TTC
   */
  private void calculerPrixParticulier() {
    // TTC - PVTTC PART = PVHT PART * (1+(TVA*0.01))
    prixParticulierTTC = calculerPrixTTC(prixParticulierHT);
    
    // Calculer la remise entre le prix public et le prix HT (version monétaire et version %)
    if (prixPublicHT != null && prixParticulierHT != null && prixPublicHT.compareTo(BigDecimal.ZERO) != 0) {
      remiseParticulier = prixPublicHT.subtract(prixParticulierHT);
      remiseParticulierPourcent =
          prixPublicHT.subtract(prixParticulierHT).multiply(VALUE_100).divide(prixPublicHT, 2, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * On saisit le prix HT particulier directement. On recalcule tout sur cette base
   */
  public void saisirPrixParticulierHT(BigDecimal pprixParticulierHTSaisi) {
    if (pprixParticulierHTSaisi == null) {
      return;
    }
    
    prixParticulierHT = pprixParticulierHTSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    recalculerTout();
  }
  
  /**
   * La saisie du prix particulier TTC n'est qu'un moyen détourné de saisir le prix particulier HT.
   * L'objectif de cette méthode est de recalculer un prix particulier HT comme si c'était lui qui avait été saisi.
   */
  public void saisirPrixParticulierTTC(BigDecimal pprixTTCsaisi) {
    if (pprixTTCsaisi == null) {
      return;
    }
    
    prixParticulierHT = calculerPrixHTwithTTC(pprixTTCsaisi);
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT particulier à partir du taux particulier
   * L'objectif de cette méthode est de recalculer un prix public HT comme si c'était lui qui avait été saisi.
   * ht public - ((ht public * remise particulier) / 100)
   * 
   * La remise particulier impacte le prix de vente particulier, le prix Promo et le prix artisan.
   * Autoriser les valeurs négatives car cela peut arriver qu'on vende au particulie plus cher que le prix PV préconisé par le fabricant.
   */
  public void saisirRemiseParticulier(BigDecimal remiseSaisie) {
    if (prixPublicHT == null || remiseSaisie == null) {
      return;
    }
    
    BigDecimal tmp = prixPublicHT.multiply(remiseSaisie).divide(VALUE_100, 2, RoundingMode.HALF_UP);
    if (tmp != null) {
      prixParticulierHT = prixPublicHT.subtract(tmp);
    }
    
    recalculerTout();
  }
  
  /**************************************************************************
   * Calculs des tarifs promos
   *************************************************************************/
  
  /**
   * Calculer le PVTTC promo, la remise promo et le taux promo.
   * Cette méthode est détentrice de la formule de calcul des prix promo. On doit toujours passer par elle
   * pour recalculer n'importe quel élément de prix promo. On part sur la base que toutes les infos sont
   * mises à jour sur la base du PV HT promo. Il faut donc que ce dernier soit mis à jour au préalable soit
   * par import DB2 ou saisie HT , soit par calcul depuis la saisie du Taux ou du TTC
   */
  private void calculerPrixPromo() {
    // Calculer le TTC
    prixPromoTTC = calculerPrixTTC(prixPromoHT);
    
    // Calculer la remise entre le prix public et le prix HT (version monétaire et version %)
    if (prixPromoHT != null && prixParticulierHT != null && prixParticulierHT.compareTo(BigDecimal.ZERO) != 0) {
      remisePromo = prixParticulierHT.subtract(prixPromoHT);
      remisePromoPourcent =
          prixParticulierHT.subtract(prixPromoHT).multiply(VALUE_100).divide(prixParticulierHT, 2, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Permet de calculer le taux de marge d'un tarif particulier (PVHT PART ou PVHT PROMO)
   * Sur la base du prix le plus avantageux des 2
   */
  private void calculerTaux() {
    if (prixAchat == null || prixMoinsCher == null || prixMoinsCher.compareTo(BigDecimal.ZERO) == 0) {
      return;
    }
    
    taux =
        prixMoinsCher.subtract(prixAchat.add(convertirPointsWatt())).multiply(VALUE_100).divide(prixMoinsCher, 2, RoundingMode.HALF_UP);
  }
  
  /**
   * On saisit le prix HT Promo directement. On recalcule tout sur cette base
   * On doti pouvoir saisir 0, ou Suppr pour effacer les zones promos.
   */
  public void saisirPrixPromoHT(BigDecimal pprixPromoHTSaisi) {
    if (pprixPromoHTSaisi == null) {
      return;
    }
    
    prixPromoHT = pprixPromoHTSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    recalculerTout();
  }
  
  /**
   * La saisie du prix Promo TTC n'est qu'un moyen détourné de saisir le prix Promo HT.
   * L'objectif de cette méthode est de recalculer un prix Promo HT comme si c'était lui qui avait été saisi.
   * On doit pouvoir saisir 0, ou Suppr pour effacer les zones promos.
   */
  public void saisirPrixPromoTTC(BigDecimal pprixTTCsaisi) {
    if (pprixTTCsaisi == null) {
      return;
    }
    
    prixPromoHT = calculerPrixHTwithTTC(pprixTTCsaisi);
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT Promo à partir du taux Promo
   * L'objectif de cette méthode est de recalculer un prix promo HT comme si c'était lui qui avait été saisi.
   * HT part - ((ht part * remise Promo) / 100)
   * Interdire le svaleurs négatives car cela n'a pas de sens pour une "promo".
   * La remise particulier impacte le prix Promo et le prix artisan.
   */
  public void saisirRemisePromo(BigDecimal remiseSaisie) {
    // Vérif habituelles objet NULL mais aussi on n'autorise pas une saisie < 0
    if (prixParticulierHT == null || remiseSaisie == null || remiseSaisie.compareTo(new BigDecimal(0)) == -1) {
      return;
    }
    
    BigDecimal tmp = prixParticulierHT.multiply(remiseSaisie).divide(VALUE_100, 2, RoundingMode.HALF_UP);
    
    if (tmp != null) {
      prixPromoHT = prixParticulierHT.subtract(tmp);
    }
    
    recalculerTout();
  }
  
  /**
   * Permet de mettre à jour le tarif Non Pro le moins cher en comparant le prix HT particulier et le prix HT Promo
   */
  protected void calculerPrixMoinsCher() {
    // Si pas de prix Promo
    if (prixPromoHT == null || prixPromoHT.compareTo(BigDecimal.ZERO) == 0) {
      // Si le tarif particulier existe et n'est pas null c'est le mooins cher
      if (prixParticulierHT != null && prixParticulierHT.compareTo(BigDecimal.ZERO) > 0) {
        prixMoinsCher = prixParticulierHT;
      }
    }
    // Si on a un prix promo > 0
    else {
      // Si le tarif particulier existe et n'est pas null on compare
      if (prixParticulierHT != null && prixParticulierHT.compareTo(BigDecimal.ZERO) > 0) {
        if (prixPromoHT.compareTo(prixParticulierHT) > 0) {
          prixMoinsCher = prixParticulierHT;
        }
        else {
          prixMoinsCher = prixPromoHT;
          // Si pas de prix particulier >0 on prend le promo
        }
      }
      else {
        prixMoinsCher = prixPromoHT;
      }
    }
  }
  
  /**************************************************************************
   * Calculs des tarifs Artisans NON ELEC
   *************************************************************************/
  
  /**
   * Calculer toutes les déclinaisons du prix artisan Non ELEC: TTC, remise et taux
   */
  private void calculerPrixArtisanNE() {
    if (prixArtisNelecHT == null) {
      return;
    }
    
    // Le prix artisan Non Elec ne doit jamais être supérieur au prix le moins cher Particulier (part ou promo)
    if (prixMoinsCher != null && prixMoinsCher.compareTo(BigDecimal.ZERO) > 0 && prixArtisNelecHT.compareTo(BigDecimal.ZERO) > 0) {
      if (prixArtisNelecHT.compareTo(prixMoinsCher) >= 0) {
        prixArtisNelecHT = new BigDecimal(prixMoinsCher.toString());
      }
    }
    
    // Calculer le TTC
    prixArtisNelecTTC = calculerPrixTTC(prixArtisNelecHT);
    
    // Calculer le taux TAUX = ((PVHT - (PRIXACHAT+POINTS))/PVHT) * 100
    if (prixArtisNelecHT.compareTo(BigDecimal.ZERO) != 0) {
      if (prixAchat != null && prixArtisNelecHT != null && prixArtisNelecHT.compareTo(BigDecimal.ZERO) > 0) {
        tauxArtisNelec =
            prixArtisNelecHT.subtract(prixAchat.add(convertirPointsWatt())).multiply(VALUE_100)
                .divide(prixArtisNelecHT, 2, RoundingMode.HALF_UP);
      }
    }
    
    // Calculer la remise entre le prix promo/Part et le prix HT artisan NON ELEC (version monétaire et version %)
    if (prixMoinsCher != null && prixMoinsCher.compareTo(BigDecimal.ZERO) != 0) {
      remiseArtisNelec = prixMoinsCher.subtract(prixArtisNelecHT);
      remiseArtisNelecPourcent =
          prixMoinsCher.subtract(prixArtisNelecHT).multiply(VALUE_100).divide(prixMoinsCher, 2, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * On saisit le prix HT Artisan Non ELEC directement. On recalcule tout sur cette base
   */
  public void saisirPrixArtisNonElecHT(BigDecimal pprixSaisi) {
    if (pprixSaisi == null) {
      return;
    }
    
    prixArtisNelecHT = pprixSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    recalculerTout();
  }
  
  /**
   * La saisie du prix Artisan Non ELEC TTC n'est qu'un moyen détourné de saisir le prix artisan HT.
   * L'objectif de cette méthode est de recalculer un prix artisan HT comme si c'était lui qui avait été saisi.
   */
  public void saisirPrixArtisNonElecTTC(BigDecimal pprixTTCsaisi) {
    if (pprixTTCsaisi == null) {
      return;
    }
    
    prixArtisNelecHT = calculerPrixHTwithTTC(pprixTTCsaisi);
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT artisan Non ELEC à partir du taux artisan.
   * L'objectif de cette méthode est de recalculer un prix HT artisan NON ELEC comme si c'était lui qui avait été saisi.
   * (100 * pump) / (100 - tx) .
   * Titne compte de spoints watt (à vérifier).
   */
  public void saisirTauxArtisanNonELec(BigDecimal ttauxSaisi) {
    if (ttauxSaisi == null) {
      return;
    }
    
    BigDecimal tmp = VALUE_100.subtract(ttauxSaisi);
    
    if (tmp != null && tmp.compareTo(BigDecimal.ZERO) != 0 && prixAchat != null && prixAchat.compareTo(BigDecimal.ZERO) != 0) {
      prixArtisNelecHT = VALUE_100.multiply(prixAchat).divide(tmp, 2, RoundingMode.HALF_UP);
    }
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT Artisan Non elec à partir de la saisie
   * L'objectif de cette méthode est de recalculer un prix artisan HT Non Elec comme si c'était lui qui avait été saisi.
   * htpromo - ((ht promo * remise artisan) / 100)
   * La remise artisan non elec s'applique au moins cher des prix destinés au particulier (prix particulier ou prix promo).
   */
  public void saisirRemiseArtisanNonElec(BigDecimal remiseSaisie) {
    if (prixMoinsCher == null || remiseSaisie == null) {
      return;
    }
    
    BigDecimal tmp = prixMoinsCher.multiply(remiseSaisie).divide(VALUE_100, 2, RoundingMode.HALF_UP);
    
    if (tmp != null) {
      prixArtisNelecHT = prixMoinsCher.subtract(tmp);
    }
    
    recalculerTout();
  }
  
  /**************************************************************************
   * Calculs des tarifs Artisans ELEC
   *************************************************************************/
  /**
   * Calculer toutes les déclinaisons du prix artisan Non Elec: TTC, remise et taux
   */
  private void calculerPrixArtisan() {
    if (prixArtisanHT == null) {
      return;
    }
    
    // Le prix artisan ne doit jamais être supérieur au prix le moins cher Particulier (part ou promo)
    if (prixMoinsCher != null && prixMoinsCher.compareTo(BigDecimal.ZERO) > 0 && prixArtisanHT.compareTo(BigDecimal.ZERO) > 0) {
      if (prixArtisanHT.compareTo(prixMoinsCher) >= 0) {
        prixArtisanHT = new BigDecimal(prixMoinsCher.toString());
      }
    }
    
    // Calculer le TTC
    prixArtisanTTC = calculerPrixTTC(prixArtisanHT);
    
    // Calculer le taux TAUX = ((PVHT - (PRIXACHAT+POINTS))/PVHT) * 100
    if (prixArtisanHT.compareTo(BigDecimal.ZERO) != 0) {
      if (prixAchat != null && prixArtisanHT != null && prixArtisanHT.compareTo(BigDecimal.ZERO) > 0) {
        tauxArtisan =
            prixArtisanHT.subtract(prixAchat.add(convertirPointsWatt())).multiply(VALUE_100)
                .divide(prixArtisanHT, 2, RoundingMode.HALF_UP);
      }
    }
    
    // Calculer la remise entre le prix promo/Part et le prix HT artisan (version monétaire et version %)
    if (prixMoinsCher != null && prixMoinsCher.compareTo(BigDecimal.ZERO) != 0) {
      remiseArtisan = prixMoinsCher.subtract(prixArtisanHT);
      remiseArtisanPourcent = prixMoinsCher.subtract(prixArtisanHT).multiply(VALUE_100).divide(prixMoinsCher, 2, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * On saisit le prix HT Artisan directement. On recalcule tout sur cette base
   */
  public void saisirPrixArtisanHT(BigDecimal pprixSaisi) {
    if (pprixSaisi == null) {
      return;
    }
    
    prixArtisanHT = pprixSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    recalculerTout();
  }
  
  /**
   * La saisie du prix Artisan TTC n'est qu'un moyen détourné de saisir le prix artisan HT.
   * L'objectif de cette méthode est de recalculer un prix artisan HT comme si c'était lui qui avait été saisi.
   */
  public void saisirPrixArtisanTTC(BigDecimal pprixTTCsaisi) {
    if (pprixTTCsaisi == null) {
      return;
    }
    
    prixArtisanHT = calculerPrixHTwithTTC(pprixTTCsaisi);
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT artisan à partir du taux artisan.
   * L'objectif de cette méthode est de recalculer un prix HT artisan comme si c'était lui qui avait été saisi.
   * (100 * pump) / (100 - tx) .
   * Tient compte des points watt (à vérifier).
   * 
   */
  public void saisirTauxArtisan(BigDecimal ttauxSaisi) {
    if (ttauxSaisi == null) {
      return;
    }
    
    BigDecimal tmp = VALUE_100.subtract(ttauxSaisi);
    
    if (tmp != null && tmp.compareTo(BigDecimal.ZERO) != 0 && prixAchat != null && prixAchat.compareTo(BigDecimal.ZERO) != 0) {
      prixArtisanHT = VALUE_100.multiply(prixAchat).divide(tmp, 2, RoundingMode.HALF_UP);
    }
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT Artisan à partir de la saisie
   * L'objectif de cette méthode est de recalculer un prix artisan HT comme si c'était lui qui avait été saisi.
   * htpromo - ((ht promo * remise artisan) / 100)
   * 
   * La remise artisan s'applique au moins cher des prix destinés au particulier (prix particulier ou prix promo).
   */
  public void saisirRemiseArtisan(BigDecimal remiseSaisie) {
    if (prixMoinsCher == null || remiseSaisie == null) {
      return;
    }
    
    BigDecimal tmp = prixMoinsCher.multiply(remiseSaisie).divide(VALUE_100, 2, RoundingMode.HALF_UP);
    
    if (tmp != null) {
      prixArtisanHT = prixMoinsCher.subtract(tmp);
    }
    
    recalculerTout();
  }
  
  /**************************************************************************
   * Calculs des tarifs Revendeur
   *************************************************************************/
  
  /**
   * Calculer toutes les déclinaisons du prix artisan Non Elec: TTC, remise et taux
   */
  private void calculerPrixRevendeur() {
    if (prixRevendeurHT == null) {
      return;
    }
    
    // Le prix artisan ne doit jamais être supérieur au prix le moins cher Particulier (part ou promo)
    if (prixMoinsCher != null && prixMoinsCher.compareTo(BigDecimal.ZERO) > 0 && prixRevendeurHT.compareTo(BigDecimal.ZERO) > 0) {
      if (prixRevendeurHT.compareTo(prixMoinsCher) >= 0) {
        prixRevendeurHT = new BigDecimal(prixMoinsCher.toString());
      }
    }
    
    // Calculer le TTC
    prixRevendeurTTC = calculerPrixTTC(prixRevendeurHT);
    
    // Calculer le taux TAUX = ((PVHT - (PRIXACHAT+POINTS))/PVHT) * 100
    if (prixRevendeurHT.compareTo(BigDecimal.ZERO) != 0) {
      if (prixAchat != null && prixRevendeurHT != null && prixRevendeurHT.compareTo(BigDecimal.ZERO) > 0) {
        tauxRevendeur =
            prixRevendeurHT.subtract(prixAchat.add(convertirPointsWatt())).multiply(VALUE_100)
                .divide(prixRevendeurHT, 2, RoundingMode.HALF_UP);
      }
    }
    
    // Calculer la remise entre le prix promo/Part et le prix HT artisan (version monétaire et version %)
    if (prixMoinsCher != null && prixMoinsCher.compareTo(BigDecimal.ZERO) != 0) {
      remiseRevendeur = prixMoinsCher.subtract(prixRevendeurHT);
      remiseRevendeurPourcent =
          prixMoinsCher.subtract(prixRevendeurHT).multiply(VALUE_100).divide(prixMoinsCher, 2, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * On saisit le prix HT revendeur directement. On recalcule tout sur cette base
   */
  public void saisirPrixRevendeurHT(BigDecimal pprixSaisi) {
    if (pprixSaisi == null) {
      return;
    }
    
    prixRevendeurHT = pprixSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    recalculerTout();
  }
  
  /**
   * La saisie du prix revendeur n'est qu'un moyen détourné de saisir le prix revendeur HT.
   * L'objectif de cette méthode est de recalculer un prix revendeur HT comme si c'était lui qui avait été saisi.
   */
  public void saisirPrixRevendeurTTC(BigDecimal pprixTTCsaisi) {
    if (pprixTTCsaisi == null) {
      return;
    }
    
    prixRevendeurHT = calculerPrixHTwithTTC(pprixTTCsaisi);
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT revendeur à partir du taux revendeur.
   * L'objectif de cette méthode est de recalculer un prix HT revendeur comme si c'était lui qui avait été saisi.
   * (100 * pump) / (100 - tx) .
   */
  public void saisirTauxRevendeur(BigDecimal ttauxSaisi) {
    if (ttauxSaisi == null) {
      return;
    }
    
    BigDecimal tmp = VALUE_100.subtract(ttauxSaisi);
    
    if (tmp != null && tmp.compareTo(BigDecimal.ZERO) != 0 && prixAchat != null && prixAchat.compareTo(BigDecimal.ZERO) != 0) {
      prixRevendeurHT = VALUE_100.multiply(prixAchat).divide(tmp, 2, RoundingMode.HALF_UP);
    }
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT revendeur à partir de la saisie
   * L'objectif de cette méthode est de recalculer un prix revendeur comme si c'était lui qui avait été saisi.
   * htpromo - ((ht promo * remise revendeur) / 100)
   * La remise grossiste s'applique au moins cher des prix destinés au particulier (prix particulier ou prix promo).
   */
  public void saisirRemiseRevendeur(BigDecimal remiseSaisie) {
    if (prixMoinsCher == null || remiseSaisie == null) {
      return;
    }
    
    BigDecimal tmp = prixMoinsCher.multiply(remiseSaisie).divide(VALUE_100, 2, RoundingMode.HALF_UP);
    
    if (tmp != null) {
      prixRevendeurHT = prixMoinsCher.subtract(tmp);
    }
    
    recalculerTout();
  }
  
  /**************************************************************************
   * Calculs des tarifs Grossiste
   *************************************************************************/
  
  /**
   * Calculer toutes les déclinaisons du prix Grossiste: TTC, remise et taux
   */
  private void calculerPrixGrossiste() {
    if (prixGrossisteHT == null) {
      return;
    }
    
    // Calculer le TTC
    prixGrossisteTTC = calculerPrixTTC(prixGrossisteHT);
    
    // Calculer le taux TAUX = ((PVHT - (PRIXACHAT+POINTS))/PVHT) * 100
    if (prixGrossisteHT.compareTo(BigDecimal.ZERO) != 0) {
      if (prixAchat != null && prixGrossisteHT != null && prixGrossisteHT.compareTo(BigDecimal.ZERO) > 0) {
        tauxGrossiste = prixGrossisteHT.subtract(prixAchat).multiply(VALUE_100).divide(prixGrossisteHT, 2, RoundingMode.HALF_UP);
      }
    }
    
    // Calculer la remise entre le prix promo/Part et le prix HT artisan (version monétaire et version %)
    if (prixMoinsCher != null && prixMoinsCher.compareTo(BigDecimal.ZERO) != 0) {
      remiseGrossiste = prixMoinsCher.subtract(prixGrossisteHT);
      remiseGrossistePourcent =
          prixMoinsCher.subtract(prixGrossisteHT).multiply(VALUE_100).divide(prixMoinsCher, 2, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * On saisit le prix HT Grossiste directement. On recalcule tout sur cette base
   */
  public void saisirPrixGrossisteHT(BigDecimal pprixSaisi) {
    if (pprixSaisi == null) {
      return;
    }
    
    prixGrossisteHT = pprixSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    recalculerTout();
  }
  
  /**
   * La saisie du prix Grossiste n'est qu'un moyen détourné de saisir le prix Grossiste HT.
   * L'objectif de cette méthode est de recalculer un prix Grossiste HT comme si c'était lui qui avait été saisi.
   */
  public void saisirPrixGrossisteTTC(BigDecimal pprixTTCsaisi) {
    if (pprixTTCsaisi == null) {
      return;
    }
    
    prixGrossisteHT = calculerPrixHTwithTTC(pprixTTCsaisi);
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT grossiste à partir du taux grossiste.
   * L'objectif de cette méthode est de recalculer un prix HT grossiste comme si c'était lui qui avait été saisi.
   * (100 * pump) / (100 - tx) .
   * Tient compte des points watt (à vérifier).
   */
  public void saisirTauxGrossiste(BigDecimal ttauxSaisi) {
    if (ttauxSaisi == null) {
      return;
    }
    
    BigDecimal tmp = VALUE_100.subtract(ttauxSaisi);
    
    if (tmp != null && tmp.compareTo(BigDecimal.ZERO) != 0 && prixAchat != null && prixAchat.compareTo(BigDecimal.ZERO) != 0) {
      prixGrossisteHT = VALUE_100.multiply(prixAchat).divide(tmp, 2, RoundingMode.HALF_UP);
    }
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT grossiste à partir de la saisie
   * L'objectif de cette méthode est de recalculer un prix grossiste comme si c'était lui qui avait été saisi.
   * htpromo - ((ht promo * remise grossiste) / 100)
   * La remise grossiste s'applique au moins cher des prix destinés au particulier (prix particulier ou prix promo).
   */
  public void saisirRemiseGrossiste(BigDecimal remiseSaisie) {
    if (prixMoinsCher == null || remiseSaisie == null) {
      return;
    }
    
    BigDecimal tmp = prixMoinsCher.multiply(remiseSaisie).divide(VALUE_100, 2, RoundingMode.HALF_UP);
    
    if (tmp != null) {
      prixGrossisteHT = prixMoinsCher.subtract(tmp);
    }
    
    recalculerTout();
  }
  
  /**************************************************************************
   * Calculs des tarifs GSB
   *************************************************************************/
  private void calculerPrixGsb() {
    // Calculer le TTC
    prixGsbTTC = calculerPrixTTC(prixGsbHT);
    
    // Calculer le taux
    // TAUX = ((PVHT - PUMP)/PVHT) * 100
    if (prixGsbHT != null && prixAchat != null && prixGsbHT.compareTo(BigDecimal.ZERO) != 0) {
      tauxGsb = prixGsbHT.subtract(prixAchat).multiply(VALUE_100).divide(prixGsbHT, 2, RoundingMode.HALF_UP);
    }
    
    // Calculer la remise entre le prix promo et le prix HT Gsb(version monétaire et version %)
    if (prixGsbHT != null && prixPublicHT != null && prixPublicHT.compareTo(BigDecimal.ZERO) != 0) {
      remiseGsb = prixPublicHT.subtract(prixGsbHT);
      remiseGsbPourcent = prixPublicHT.subtract(prixGsbHT).multiply(VALUE_100).divide(prixPublicHT, 2, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Saisie directe du prix Gsb HT.
   * @param saisirPrixGsbHT
   */
  public void saisirPrixGsbHT(BigDecimal pprixSaisi) {
    if (pprixSaisi == null) {
      return;
    }
    
    prixGsbHT = pprixSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    recalculerTout();
  }
  
  /**
   * La saisie du prix Gsb TTC n'est qu'un moyen détourné de saisir le prix Gsb HT.
   * L'objectif de cette méthode est de recalculer un prix Gsb HT comme si c'était lui qui avait été saisi.
   */
  public void saisirPrixGsbTTC(BigDecimal pprixTTCsaisi) {
    if (pprixTTCsaisi == null) {
      return;
    }
    
    prixGsbHT = calculerPrixHTwithTTC(pprixTTCsaisi);
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT GSB à partir du taux Gsb.
   * L'objectif de cette méthode est de recalculer un prix HT Gsb comme si c'était lui qui avait été saisi.
   * (100 * pump) / (100 - tx)
   * 
   * Ne tient pas compte des points Watt.
   */
  public void saisirTauxGsb(BigDecimal ttauxSaisi) {
    if (ttauxSaisi == null) {
      return;
    }
    
    BigDecimal tmp = VALUE_100.subtract(ttauxSaisi);
    
    if (tmp != null && tmp.compareTo(BigDecimal.ZERO) != 0 && prixAchat != null && prixAchat.compareTo(BigDecimal.ZERO) != 0) {
      prixGsbHT = VALUE_100.multiply(prixAchat).divide(tmp, 2, RoundingMode.HALF_UP);
    }
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT Gsb à partir de la saisie
   * L'objectif de cette méthode est de recalculer un prix Gsb HT comme si c'était lui qui avait été saisi.
   * HTPROMO - ((HTPROMO * remise Gsb) / 100)
   * 
   * S'applique sur le PVHT public.
   */
  public void saisirRemiseGsb(BigDecimal remiseSaisie) {
    if (prixPublicHT == null || remiseSaisie == null) {
      return;
    }
    
    BigDecimal tmp = prixPublicHT.multiply(remiseSaisie).divide(VALUE_100, 2, RoundingMode.HALF_UP);
    
    if (tmp != null) {
      prixGsbHT = prixPublicHT.subtract(tmp);
    }
    
    recalculerTout();
  }
  
  /**************************************************************************
   * Calculs des tarifs Administration
   *************************************************************************/
  
  /**
   * Calculer toutes les déclinaisons du prix administration: TTC, remise et taux
   */
  private void calculerPrixAdmini() {
    if (prixAdminiHT == null) {
      return;
    }
    
    // Calculer le TTC
    prixAdminiTTC = calculerPrixTTC(prixAdminiHT);
    
    // Calculer le taux TAUX = ((PVHT - PRIXACHAT)/PVHT) * 100
    if (prixAdminiHT.compareTo(BigDecimal.ZERO) != 0) {
      if (prixAchat != null && prixAdminiHT != null && prixAdminiHT.compareTo(BigDecimal.ZERO) > 0) {
        tauxAdmini = prixAdminiHT.subtract(prixAchat).multiply(VALUE_100).divide(prixAdminiHT, 2, RoundingMode.HALF_UP);
      }
    }
    
    if (prixMoinsCher != null && prixMoinsCher.compareTo(BigDecimal.ZERO) != 0) {
      remiseAdmini = prixMoinsCher.subtract(prixAdminiHT);
      remiseAdminiPourcent = prixMoinsCher.subtract(prixAdminiHT).multiply(VALUE_100).divide(prixMoinsCher, 2, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * On saisit le prix administration directement. On recalcule tout sur cette base.
   */
  public void saisirPrixAdminHT(BigDecimal pprixSaisi) {
    if (pprixSaisi == null) {
      return;
    }
    
    prixAdminiHT = pprixSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    recalculerTout();
  }
  
  /**
   * La saisie du prix administration TTC n'est qu'un moyen détourné de saisir le prix administration HT.
   * L'objectif de cette méthode est de recalculer un prix administration HT comme si c'était lui qui avait été saisi.
   */
  public void saisirPrixAdminiTTC(BigDecimal pprixTTCsaisi) {
    if (pprixTTCsaisi == null) {
      return;
    }
    
    prixAdminiHT = calculerPrixHTwithTTC(pprixTTCsaisi);
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT administration à partir du taux artisan.
   * L'objectif de cette méthode est de recalculer un prix HT administration comme si c'était lui qui avait été saisi.
   * (100 * pump) / (100 - tx) .
   */
  public void saisirTauxAdmini(BigDecimal ttauxSaisi) {
    if (ttauxSaisi == null) {
      return;
    }
    
    BigDecimal tmp = VALUE_100.subtract(ttauxSaisi);
    
    if (tmp != null && tmp.compareTo(BigDecimal.ZERO) != 0 && prixAchat != null && prixAchat.compareTo(BigDecimal.ZERO) != 0) {
      prixAdminiHT = VALUE_100.multiply(prixAchat).divide(tmp, 2, RoundingMode.HALF_UP);
    }
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix Administration à partir de la remise
   * L'objectif de cette méthode est de recalculer un prix admin comme si c'était lui qui avait été saisi.
   * htpromo - ((ht promo * remise admin) / 100)
   * La remise admin s'applique au moins cher des prix destinés au particulier (prix particulier ou prix promo).
   */
  public void saisirRemiseAdmini(BigDecimal remiseSaisie) {
    if (prixMoinsCher == null || remiseSaisie == null) {
      return;
    }
    
    BigDecimal tmp = prixMoinsCher.multiply(remiseSaisie).divide(VALUE_100, 2, RoundingMode.HALF_UP);
    
    if (tmp != null) {
      prixAdminiHT = prixMoinsCher.subtract(tmp);
    }
    
    recalculerTout();
  }
  
  /**************************************************************************
   * Calculs des tarifs internes
   *************************************************************************/
  
  /**
   * Calculer toutes les déclinaisons du prix interne: TTC, remise et taux
   */
  private void calculerPrixInterne() {
    if (prixInterneHT == null) {
      return;
    }
    
    // Calculer le TTC
    prixInterneTTC = calculerPrixTTC(prixInterneHT);
    
    // Calculer le taux TAUX = ((PVHT - PRIXACHAT)/PVHT) * 100
    if (prixInterneHT.compareTo(BigDecimal.ZERO) != 0) {
      if (prixAchat != null && prixInterneHT != null && prixInterneHT.compareTo(BigDecimal.ZERO) > 0) {
        tauxInterne = prixInterneHT.subtract(prixAchat).multiply(VALUE_100).divide(prixInterneHT, 2, RoundingMode.HALF_UP);
      }
    }
    
    // TODO a vérifier si la remise se fait bien sur ce tarif et non sur le tarif public
    if (prixMoinsCher != null && prixMoinsCher.compareTo(BigDecimal.ZERO) != 0) {
      remiseInterne = prixMoinsCher.subtract(prixInterneHT);
      remiseInternePourcent = prixMoinsCher.subtract(prixInterneHT).multiply(VALUE_100).divide(prixMoinsCher, 2, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * On saisit le prix interne directement. On recalcule tout sur cette base.
   */
  public void saisirPrixinterneHT(BigDecimal pprixSaisi) {
    if (pprixSaisi == null) {
      return;
    }
    
    prixInterneHT = pprixSaisi.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    recalculerTout();
  }
  
  /**
   * La saisie du prix interne TTC n'est qu'un moyen détourné de saisir le prix interne HT.
   * L'objectif de cette méthode est de recalculer un prix interne HT comme si c'était lui qui avait été saisi.
   */
  public void saisirPrixInterneTTC(BigDecimal pprixTTCsaisi) {
    if (pprixTTCsaisi == null) {
      return;
    }
    
    prixInterneHT = calculerPrixHTwithTTC(pprixTTCsaisi);
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix HT interne à partir du taux interne.
   * L'objectif de cette méthode est de recalculer un prix HT interne comme si c'était lui qui avait été saisi.
   * (100 * pump) / (100 - tx) .
   */
  public void saisirTauxInterne(BigDecimal ttauxSaisi) {
    if (ttauxSaisi == null) {
      return;
    }
    
    BigDecimal tmp = VALUE_100.subtract(ttauxSaisi);
    
    if (tmp != null && tmp.compareTo(BigDecimal.ZERO) != 0 && prixAchat != null && prixAchat.compareTo(BigDecimal.ZERO) != 0) {
      prixInterneHT = VALUE_100.multiply(prixAchat).divide(tmp, 2, RoundingMode.HALF_UP);
    }
    
    recalculerTout();
  }
  
  /**
   * Formule qui calcule le prix interne à partir de la remise
   * L'objectif de cette méthode est de recalculer un prix interne comme si c'était lui qui avait été saisi.
   * htpromo - ((ht promo * remise interne) / 100)
   * La remise interne s'applique au moins cher des prix destinés au particulier (prix particulier ou prix promo).
   */
  public void saisirRemiseInterne(BigDecimal remiseSaisie) {
    if (prixMoinsCher == null || remiseSaisie == null) {
      return;
    }
    
    BigDecimal tmp = prixMoinsCher.multiply(remiseSaisie).divide(VALUE_100, 2, RoundingMode.HALF_UP);
    
    if (tmp != null) {
      prixInterneHT = prixMoinsCher.subtract(tmp);
    }
    
    recalculerTout();
  }
  
  /**
   * Permet de saisir les points Watt de la l'article dans la ligne
   */
  public void saisirPointsWatt(Integer pPoints) {
    if (pPoints == null) {
      return;
    }
    
    nbPointsWatt = pPoints;
    
    recalculerTout();
  }
  
  /*************************************************************/
  
  /**
   * On calcule prix d'achat afin de déterminser les marges
   * Si l'article possède un PUMP se baser dessus sinon prendre le prix de revient
   */
  private void calculerPrixAchat() {
    if (pump != null && pump.compareTo(new BigDecimal(0)) > 0) {
      prixAchat = pump;
    }
    else if (prixDeRevient != null && prixDeRevient.compareTo(BigDecimal.ZERO) > 0) {
      prixAchat = prixDeRevient;
    }
  }
  
  /**
   * Convertir le nombre de points Watt en montant euros
   */
  private BigDecimal convertirPointsWatt() {
    BigDecimal remPointsWatt = null;
    
    if (nbPointsWatt == null || nbPointsWatt == 0) {
      remPointsWatt = new BigDecimal(0);
    }
    else {
      remPointsWatt = new BigDecimal(nbPointsWatt * 0.1);
    }
    
    return remPointsWatt;
  }
  
  /**
   * Met à jour le couple code et libellé de l'état WorkFlow
   */
  public void majWorkFlow(EtatWorkFlow pEtat, String pExpe, String pDest) {
    // Contrôles sur l'existence du WorkFlow
    if (workFlowGVC == null && (pEtat != null || pExpe != null || pDest != null)) {
      workFlowGVC = new WorkFlowGVC();
    }
    if (workFlowGVC == null) {
      return;
    }
    
    // On attribue les valeurs transmises
    if (pExpe != null) {
      workFlowGVC.setExpediteur(pExpe);
    }
    if (pDest != null) {
      workFlowGVC.setDestinataire(pDest);
    }
    if (pEtat != null) {
      workFlowGVC.setEtatWF(pEtat);
    }
    
    // Si aucun état est transmis et que l'état est null on met l'état par défaut
    // Car c'est obligatoire
    if (workFlowGVC.getEtatWF() == null) {
      workFlowGVC.setEtatWF(new EtatWorkFlow());
    }
    
    gererLesStatutsDeLaLigne();
  }
  
  /**************************************************************************
   * Accesseurs
   *************************************************************************/
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
  
  public String getLibelleArticle() {
    return libelleArticle;
  }
  
  public void setLibelleArticle(String libelleArticle) {
    this.libelleArticle = libelleArticle;
  }
  
  public BigDecimal getPrixPublicHT() {
    return prixPublicHT;
  }
  
  public void setPrixPublicHT(BigDecimal prixPublicHT) {
    this.prixPublicHT = prixPublicHT;
  }
  
  public BigDecimal getPrixPublicTTC() {
    return prixPublicTTC;
  }
  
  public void setPrixPublicTTC(BigDecimal prixPublicTTC) {
    this.prixPublicTTC = prixPublicTTC;
  }
  
  public String getReferenceFabricant() {
    return referenceFabricant;
  }
  
  public void setReferenceFabricant(String referenceFabricant) {
    this.referenceFabricant = referenceFabricant;
  }
  
  public String getMarque() {
    return marque;
  }
  
  public void setMarque(String marque) {
    this.marque = marque;
  }
  
  public Fournisseur getFournisseurPrincipal() {
    return fournisseurPrincipal;
  }
  
  public void setFournisseurPrincipal(Fournisseur fournisseurPrincipal) {
    this.fournisseurPrincipal = fournisseurPrincipal;
  }
  
  public Integer getNbPointsWatt() {
    return nbPointsWatt;
  }
  
  public void setNbPointsWatt(Integer nbPointsWatt) {
    this.nbPointsWatt = nbPointsWatt;
  }
  
  public BigDecimal getPrixParticulierHT() {
    return prixParticulierHT;
  }
  
  public void setPrixParticulierHT(BigDecimal prixParticulierHT) {
    this.prixParticulierHT = prixParticulierHT;
  }
  
  public BigDecimal getPrixParticulierTTC() {
    return prixParticulierTTC;
  }
  
  public void setPrixParticulierTTC(BigDecimal prixParticulierTTC) {
    this.prixParticulierTTC = prixParticulierTTC;
  }
  
  public BigDecimal getRemisePromo() {
    return remisePromo;
  }
  
  public void setRemisePromo(BigDecimal remisePromo) {
    this.remisePromo = remisePromo;
  }
  
  public BigDecimal getPrixPromoHT() {
    return prixPromoHT;
  }
  
  public void setPrixPromoHT(BigDecimal prixPromoHT) {
    this.prixPromoHT = prixPromoHT;
  }
  
  public BigDecimal getPrixPromoTTC() {
    return prixPromoTTC;
  }
  
  public BigDecimal getTaux() {
    return taux;
  }
  
  public void setTaux(BigDecimal taux) {
    this.taux = taux;
  }
  
  public BigDecimal getRemiseArtisan() {
    return remiseArtisan;
  }
  
  public void setRemiseArtisan(BigDecimal remiseArtisan) {
    this.remiseArtisan = remiseArtisan;
  }
  
  public BigDecimal getPrixArtisanHT() {
    return prixArtisanHT;
  }
  
  public void setPrixArtisanHT(BigDecimal prixArtisanHT) {
    this.prixArtisanHT = prixArtisanHT;
  }
  
  public BigDecimal getTauxArtisan() {
    return tauxArtisan;
  }
  
  public BigDecimal getPrixArtisanTTC() {
    return prixArtisanTTC;
  }
  
  public BigDecimal getRemiseGsb() {
    return remiseGsb;
  }
  
  public void setPrixGsbHT(BigDecimal prixGsbHT) {
    this.prixGsbHT = prixGsbHT;
  }
  
  public BigDecimal getPrixGsbHT() {
    return prixGsbHT;
  }
  
  public BigDecimal getTauxGsb() {
    return tauxGsb;
  }
  
  public BigDecimal getPrixGsbTTC() {
    return prixGsbTTC;
  }
  
  public BigDecimal getVeillePrixGsb() {
    return veillePrixGsb;
  }
  
  public void setVeillePrixGsb(BigDecimal veillePrixGsb) {
    this.veillePrixGsb = veillePrixGsb;
    gererLesStatutsDeLaLigne();
  }
  
  public String getVeilleNomGsb() {
    return veilleNomGsb;
  }
  
  public void setVeilleNomGsb(String veille) {
    if (veille != null) {
      veilleNomGsb = veille;
    }
    else {
      veilleNomGsb = "";
    }
    
    gererLesStatutsDeLaLigne();
  }
  
  public String getVeilleDateGsb() {
    return veilleDateGsb;
  }
  
  public void setVeilleDateGsb(String veilleDateGsb) {
    this.veilleDateGsb = veilleDateGsb;
  }
  
  public String getDateModifPrix() {
    return dateModifPrix;
  }
  
  public void setDateModifPrix(String dateModifPrix) {
    this.dateModifPrix = dateModifPrix;
  }
  
  public BigDecimal getPrixFabricantHT() {
    return prixFabricantHT;
  }
  
  public void setPrixFabricantHT(BigDecimal prixFabricantHT) {
    this.prixFabricantHT = prixFabricantHT;
  }
  
  public BigDecimal getPrixDeRevient() {
    return prixDeRevient;
  }
  
  public void setPrixDeRevient(BigDecimal prixDeRevient) {
    this.prixDeRevient = prixDeRevient;
  }
  
  public BigDecimal getDEEE() {
    return DEEE;
  }
  
  public void setDEEE(BigDecimal dEEE) {
    DEEE = dEEE;
  }
  
  public BigDecimal getPump() {
    return pump;
  }
  
  public void setPump(BigDecimal pump) {
    this.pump = pump;
  }
  
  public String getGestionWeb() {
    return gestionWeb;
  }
  
  public void setGestionWeb(String gestionWeb) {
    this.gestionWeb = gestionWeb;
    gererLesStatutsDeLaLigne();
  }
  
  public BigDecimal getTauxTVA() {
    return tauxTVA;
  }
  
  public void setTauxTVA(BigDecimal tauxTVA) {
    this.tauxTVA = tauxTVA;
  }
  
  public GroupeArticles getGroupeArticles() {
    return groupeArticles;
  }
  
  public void setGroupeArticles(GroupeArticles groupeArticles) {
    this.groupeArticles = groupeArticles;
  }
  
  public Famille getFamilleArticles() {
    return familleArticles;
  }
  
  public void setFamilleArticles(Famille familleArticles) {
    this.familleArticles = familleArticles;
  }
  
  public ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille getSousFamille() {
    return sousFamille;
  }
  
  public void setSousFamille(ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille sousFamille2) {
    this.sousFamille = sousFamille2;
  }
  
  public StatutArticle getStatutArticle() {
    return statutArticle;
  }
  
  public void setStatutArticle(StatutArticle statutArticle) {
    this.statutArticle = statutArticle;
  }
  
  public BigDecimal getRemiseParticulier() {
    return remiseParticulier;
  }
  
  public BigDecimal getRemisePourcentParticulier() {
    return remiseParticulierPourcent;
  }
  
  public BigDecimal getRemiseParticulierPourcent() {
    return remiseParticulierPourcent;
  }
  
  public BigDecimal getRemisePromoPourcent() {
    return remisePromoPourcent;
  }
  
  public BigDecimal getRemiseArtisanPourcent() {
    return remiseArtisanPourcent;
  }
  
  public BigDecimal getRemiseGsbPourcent() {
    return remiseGsbPourcent;
  }
  
  public TarifPriceJet getTarifPjet() {
    return tarifPjet;
  }
  
  public void setTarifPjet(TarifPriceJet tarifPjet) {
    this.tarifPjet = tarifPjet;
  }
  
  public String getDateTarifs() {
    return dateTarifs;
  }
  
  public void setDateTarifs(String dateTarifs) {
    this.dateTarifs = dateTarifs;
  }
  
  public int getStatutLigne() {
    return statutLigne;
  }
  
  public LigneGvc getArticleIniTial() {
    return articleIniTial;
  }
  
  public void setArticleIniTial(LigneGvc articleIniTial) {
    this.articleIniTial = articleIniTial;
  }
  
  public boolean aEteModifie() {
    return aEteModifie;
  }
  
  public void setaEteModifie(boolean aEteModifie) {
    this.aEteModifie = aEteModifie;
  }
  
  public BigDecimal getPrixMoinsCher() {
    return prixMoinsCher;
  }
  
  public void setPrixMoinsCher(BigDecimal prixMoinsCher) {
    this.prixMoinsCher = prixMoinsCher;
  }
  
  public static int getNB_LIGNES_REQUETE() {
    return NB_LIGNES_REQUETE;
  }
  
  public static void setNB_LIGNES_REQUETE(int nB_LIGNES_REQUETE) {
    NB_LIGNES_REQUETE = nB_LIGNES_REQUETE;
  }
  
  public ConditionAchatGVC getCnaFrsPrincipal() {
    return cnaFrsPrincipal;
  }
  
  public ConditionAchatGVC getCnaDistribRef() {
    return cnaDistribRef;
  }
  
  public void setCnaDistribRef(ConditionAchatGVC cnaDistribRef) {
    this.cnaDistribRef = cnaDistribRef;
  }
  
  public void setCnaFrsPrincipal(ConditionAchatGVC cnaFrsPrincipal) {
    this.cnaFrsPrincipal = cnaFrsPrincipal;
  }
  
  public void setH1PRV(BigDecimal h1prv) {
    H1PRV = h1prv;
  }
  
  public void setH1PMP(BigDecimal h1pmp) {
    H1PMP = h1pmp;
  }
  
  public ArrayList<String> getLibelleStatutsLigne() {
    return libelleStatutsLigne;
  }
  
  public BigDecimal getPrixArtisNelecHT() {
    return prixArtisNelecHT;
  }
  
  public void setPrixArtisNelecHT(BigDecimal prixArtisNelecHT) {
    this.prixArtisNelecHT = prixArtisNelecHT;
  }
  
  public BigDecimal getPrixRevendeurHT() {
    return prixRevendeurHT;
  }
  
  public void setPrixRevendeurHT(BigDecimal prixRevendeurHT) {
    this.prixRevendeurHT = prixRevendeurHT;
  }
  
  public BigDecimal getPrixGrossisteHT() {
    return prixGrossisteHT;
  }
  
  public void setPrixGrossisteHT(BigDecimal prixGrossisteHT) {
    this.prixGrossisteHT = prixGrossisteHT;
  }
  
  public BigDecimal getTauxArtisNelec() {
    return tauxArtisNelec;
  }
  
  public BigDecimal getRemiseArtisNelec() {
    return remiseArtisNelec;
  }
  
  public BigDecimal getRemiseArtisNelecPourcent() {
    return remiseArtisNelecPourcent;
  }
  
  public BigDecimal getTauxRevendeur() {
    return tauxRevendeur;
  }
  
  public BigDecimal getRemiseRevendeur() {
    return remiseRevendeur;
  }
  
  public BigDecimal getRemiseRevendeurPourcent() {
    return remiseRevendeurPourcent;
  }
  
  public BigDecimal getTauxGrossiste() {
    return tauxGrossiste;
  }
  
  public BigDecimal getRemiseGrossiste() {
    return remiseGrossiste;
  }
  
  public BigDecimal getRemiseGrossistePourcent() {
    return remiseGrossistePourcent;
  }
  
  public BigDecimal getPrixArtisNelecTTC() {
    return prixArtisNelecTTC;
  }
  
  public BigDecimal getPrixRevendeurTTC() {
    return prixRevendeurTTC;
  }
  
  public BigDecimal getPrixGrossisteTTC() {
    return prixGrossisteTTC;
  }
  
  public BigDecimal getPrixAdminiHT() {
    return prixAdminiHT;
  }
  
  public void setPrixAdminiHT(BigDecimal prixAdminiHT) {
    this.prixAdminiHT = prixAdminiHT;
  }
  
  public BigDecimal getPrixInterneHT() {
    return prixInterneHT;
  }
  
  public void setPrixInterneHT(BigDecimal prixInterneHT) {
    this.prixInterneHT = prixInterneHT;
  }
  
  public BigDecimal getPrixAdminiTTC() {
    return prixAdminiTTC;
  }
  
  public BigDecimal getTauxAdmini() {
    return tauxAdmini;
  }
  
  public BigDecimal getRemiseAdmini() {
    return remiseAdmini;
  }
  
  public BigDecimal getRemiseAdminiPourcent() {
    return remiseAdminiPourcent;
  }
  
  public BigDecimal getPrixInterneTTC() {
    return prixInterneTTC;
  }
  
  public BigDecimal getTauxInterne() {
    return tauxInterne;
  }
  
  public BigDecimal getRemiseInterne() {
    return remiseInterne;
  }
  
  public BigDecimal getRemiseInternePourcent() {
    return remiseInternePourcent;
  }
  
  public WorkFlowGVC getWorkFlowGVC() {
    return workFlowGVC;
  }
  
  public void setWorkFlowGVC(WorkFlowGVC workFlowGVC) {
    this.workFlowGVC = workFlowGVC;
  }
  
}
