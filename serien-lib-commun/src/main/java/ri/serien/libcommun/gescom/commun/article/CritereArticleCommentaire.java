/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.io.Serializable;

import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Critères de recherche des articles de types commentaires.
 */
public class CritereArticleCommentaire implements Serializable {
  // Variables
  private IdEtablissement idEtablissement = null;
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  private Boolean isArticleValideSeulement = null;
  
  /**
   * Initialiser les valeurs du critère
   */
  public void initialiser() {
    rechercheGenerique.initialiser();
  }
  
  // Accesseurs
  
  /**
   * Retourner l'identifiant de l'établissement
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Retourner le code de l'établissement
   */
  public String getCodeEtablissement() {
    if (idEtablissement != null) {
      return idEtablissement.getCodeEtablissement();
    }
    return null;
  }
  
  /**
   * Modifier l'identifiant de l'établissement
   */
  public void setIdEtablissement(IdEtablissement idEtablissement) {
    this.idEtablissement = idEtablissement;
  }
  
  /**
   * Recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void setTexteRechercheGenerique(String pTexteRechercheGenerique) {
    rechercheGenerique.setTexte(pTexteRechercheGenerique);
  }
  
  /**
   * Retourner si la recherche concerne les seuls articles valides (A1TOP == 0)
   */
  public Boolean isArticleValideSeulement() {
    return isArticleValideSeulement;
  }
  
  /**
   * Modifier si la recherche doit concerner les seuls articles valides (A1TOP == 0)
   */
  public void setIsArticleValideSeulement(Boolean pIsArticleValideSeulement) {
    isArticleValideSeulement = pIsArticleValideSeulement;
    if (isArticleValideSeulement == null) {
      isArticleValideSeulement = false;
    }
  }
}
