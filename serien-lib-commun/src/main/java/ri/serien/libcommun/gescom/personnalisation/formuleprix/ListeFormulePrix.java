/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.formuleprix;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

public class ListeFormulePrix extends ListeClasseMetier<IdFormulePrix, FormulePrix, ListeFormulePrix> {
  
  /**
   * Charge la liste de FormulePrix suivant une liste d'IdFormulePrix.
   */
  @Override
  public ListeFormulePrix charger(IdSession pIdSession, List<IdFormulePrix> pListeId) {
    return null;
  }
  
  /**
   * Charge tous les FormulePrix suivant l'établissement.
   */
  @Override
  public ListeFormulePrix charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CritereFormulePrix criteres = new CritereFormulePrix();
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeFormulePrix(pIdSession, criteres);
  }
  
}
