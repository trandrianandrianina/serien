/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.communezonegeographique;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une commune de zone géographique.
 *
 * L'identifiant est composé de l'identifiant d'une zone géographique et du code postal.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdCommuneZoneGeographique extends AbstractIdAvecEtablissement {
  // Constante
  public static final int LONGUEUR_ZONE_GEOGRAPHIQUE_MIN = 1;
  public static final int LONGUEUR_ZONE_GEOGRAPHIQUE_MAX = 5;
  private static final int LONGUEUR_CODE_MAGASIN = 2;
  public static final int LONGUEUR_CODE_POSTAL = 5;
  
  // Variables
  private final IdZoneGeographique idZoneGeographique;
  private final Integer codePostal;
  private final IdMagasin idMagasin;
  
  /**
   * Constructeur protéger pour un identifiant complet.
   */
  protected IdCommuneZoneGeographique(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin,
      IdZoneGeographique pIdZoneGeographique, Integer pCodePostal) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    codePostal = controlerCodePostal(pCodePostal);
    idZoneGeographique = controlerIdZoneGeographique(pIdZoneGeographique);
    idMagasin = controlerIdMagasin(pIdMagasin);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdCommuneZoneGeographique getInstance(IdEtablissement pIdEtablissement, IdMagasin pIdMagasin,
      IdZoneGeographique pIdZoneGeologique, Integer pCodePostal) {
    return new IdCommuneZoneGeographique(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pIdMagasin, pIdZoneGeologique, pCodePostal);
  }
  
  /**
   * Contrôler la validité du code postal.
   * Le code postal est une chaîne alphanumérique de 5 caractères.
   */
  private static Integer controlerCodePostal(Integer pCodePostal) {
    if (pCodePostal == null) {
      throw new MessageErreurException("Le code postal n'est pas renseigné.");
    }
    if (pCodePostal <= 0) {
      throw new MessageErreurException("Le code postal est inférieur ou égal à zéro.");
    }
    if (pCodePostal > Constantes.valeurMaxZoneNumerique(LONGUEUR_CODE_POSTAL)) {
      throw new MessageErreurException("Le code postal est supérieur à la valeur maximum possible.");
    }
    return pCodePostal;
  }
  
  /**
   * Contrôler la validité du code de la zone géographique
   * Le code de la zone géographique est une chaîne alphanumérique de 1 à 5 caractères.
   */
  private static IdZoneGeographique controlerIdZoneGeographique(IdZoneGeographique pIdZoneGeographique) {
    if (pIdZoneGeographique == null) {
      throw new MessageErreurException("Le code de la zone géographique n'est pas renseigné.");
    }
    String valeur = pIdZoneGeographique.getCode();
    if (valeur.length() < LONGUEUR_ZONE_GEOGRAPHIQUE_MIN || valeur.length() > LONGUEUR_ZONE_GEOGRAPHIQUE_MAX) {
      throw new MessageErreurException("Le code de la zone géographique doit comporter entre " + LONGUEUR_ZONE_GEOGRAPHIQUE_MIN + " et "
          + LONGUEUR_ZONE_GEOGRAPHIQUE_MAX + " caractères : " + valeur);
    }
    return pIdZoneGeographique;
  }
  
  /**
   * Contrôler la validité du code du magasin
   * Le code du magasin est une chaîne alphanumérique de 2 caractères.
   */
  private static IdMagasin controlerIdMagasin(IdMagasin pIdMagasin) {
    if (pIdMagasin == null) {
      throw new MessageErreurException("Le code du magasin n'est pas renseigné.");
    }
    String valeur = pIdMagasin.getCode();
    if (valeur.length() < LONGUEUR_CODE_MAGASIN || valeur.length() > LONGUEUR_CODE_MAGASIN) {
      throw new MessageErreurException("Le code du magasin doit comporter entre " + LONGUEUR_CODE_MAGASIN + " caractères : " + valeur);
    }
    return pIdMagasin;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + codePostal.hashCode();
    cle = 37 * cle + idZoneGeographique.hashCode();
    cle = 37 * cle + idMagasin.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    
    if (pObject == null) {
      return false;
    }
    
    if (!(pObject instanceof IdCommuneZoneGeographique)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de commune de zone geographique.");
    }
    IdCommuneZoneGeographique id = (IdCommuneZoneGeographique) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && codePostal.equals(id.codePostal)
        && idZoneGeographique.equals(id.idZoneGeographique) && idMagasin.equals(id.idMagasin);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdCommuneZoneGeographique)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdCommuneZoneGeographique id = (IdCommuneZoneGeographique) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idMagasin.compareTo(id.idMagasin);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idZoneGeographique.compareTo(id.idZoneGeographique);
    if (comparaison != 0) {
      return comparaison;
    }
    return codePostal.compareTo(id.codePostal);
  }
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    return codePostal.toString();
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le code postal
   */
  public Integer getCodePostal() {
    return codePostal;
  }
  
  /**
   * Retourne l'IdMagasin
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Retourne l'IdZoneGeographique
   */
  public IdZoneGeographique getIdZoneGeographique() {
    return idZoneGeographique;
  }
}
