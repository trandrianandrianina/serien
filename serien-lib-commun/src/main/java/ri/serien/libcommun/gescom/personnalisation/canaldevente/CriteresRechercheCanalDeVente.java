/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.canaldevente;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;

public class CriteresRechercheCanalDeVente extends CriteresBaseRecherche {
  
  // Constantes
  public static final int RECHERCHE_CANAL_DE_VENTE = 1;
  
}
