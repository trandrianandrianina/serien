/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.util.ArrayList;

import ri.serien.libcommun.outils.Constantes;

/**
 * Liste d'articles.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste d'articles.
 * La méthode chargerTout permet d'obtenir la liste de tous les Articles d'un établissement.
 */
public class ListeArticle extends ArrayList<Article> {
  /**
   * Constructeur.
   */
  public ListeArticle() {
  }
  
  /**
   * Retourner un Article de la liste à partir de son identifiant.
   */
  public Article retournerArticleParId(IdArticle pIdArticle) {
    if (pIdArticle == null) {
      return null;
    }
    for (Article article : this) {
      if (article != null && Constantes.equals(article.getId(), pIdArticle)) {
        return article;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner un Article de la liste à partir de son libellé complet.
   */
  public Article retournerArticleParLibelle(String pLibelle) {
    if (pLibelle == null) {
      return null;
    }
    for (Article article : this) {
      if (article != null && article.getLibelleComplet().equals(pLibelle)) {
        return article;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un Article est présent dans la liste.
   */
  public boolean isPresent(IdArticle pIdArticle) {
    return retournerArticleParId(pIdArticle) != null;
  }
}
