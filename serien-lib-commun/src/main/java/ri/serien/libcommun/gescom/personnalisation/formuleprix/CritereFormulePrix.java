/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.formuleprix;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Ensemble des critères de recherche pour les formules de prix.
 * Aucun critère n'est obligatoire.
 */
public class CritereFormulePrix implements Serializable {
  // Constantes
  private IdEtablissement idEtablissement = null;
  
  // -- Accesseurs
  
  /**
   * Identifiant établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'identifiant de l'établissement.
   * 
   * @param pIdEtablissement
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    this.idEtablissement = pIdEtablissement;
  }
  
}
