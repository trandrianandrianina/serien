/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stock;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant du stock d'un article.
 *
 * L'identifiant standard est composé d'un identifiant article et d'un identifiant magasin car le stock est ventilé par article et par
 * magasin (c'est la raison d'être des magasins dans le logiciel). L'identifiant peut comporter uniquement un identifiant article et un
 * identifiant établissement (pas d'identifiant magasin donc) lorsqu'on veut gérer le stock d'un établissement, tous magasins confondus.
 * 
 * Concernant la persistance, le stock d'un article par magasin est persisté dans la base de données dans la table PGVMSTKM. Par contre,
 * le stock d'un article par établissement est un cumul articiel des lignes de la table PGVMSTKM pour un établissement. Le stock par
 * établissement n'est donc pas persisté dans la base de données. Seul le stock ventilé par magasin est persisté.
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdStock extends AbstractIdAvecEtablissement {
  private IdArticle idArticle;
  private IdMagasin idMagasin;
  
  /**
   * Constructeur interne.
   * 
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pIdMagasin Identifiant du magasin (peut-être null).
   * @param pIdArticle Identifiant de l'article.
   */
  private IdStock(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin,
      IdArticle pIdArticle) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    
    // Contrôler les identifiants
    IdArticle.controlerId(pIdArticle, false);
    
    // Contrôler les établissements des identifiants
    if (!getIdEtablissement().equals(pIdArticle.getIdEtablissement())) {
      throw new MessageErreurException("Identifiant stock invalide car les établissements sont incohérents.");
    }
    if (pIdMagasin != null && !getIdEtablissement().equals(pIdMagasin.getIdEtablissement())) {
      throw new MessageErreurException("Identifiant stock invalide car les établissements sont incohérents.");
    }
    
    // Renseigner les attributs
    idArticle = pIdArticle;
    idMagasin = pIdMagasin;
  }
  
  /**
   * Construire un identifiant stock à partir de toutes les informations constituants la clé unique.
   * 
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pIdMagasin Identifiant du magasin (peut-être null).
   * @param pIdArticle Identifiant de l'article.
   * @return Identifiant stock.
   */
  public static IdStock getInstance(IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, IdArticle pIdArticle) {
    // Construire l'identifiant stock pour le magasin si l'identifiant magasin est fourni
    if (pIdMagasin != null) {
      return IdStock.getInstancePourMagasin(pIdMagasin, pIdArticle);
    }
    // Construire l'identifiant stock pour l'établissement si l'identifiant établissement est fourni
    else if (pIdEtablissement != null) {
      return IdStock.getInstancePourEtablissement(pIdEtablissement, pIdArticle);
    }
    // Construire l'identifiant stock pour l'établissement à partir de l'article comme solution de secours
    else if (pIdArticle != null) {
      return IdStock.getInstancePourEtablissement(pIdArticle.getIdEtablissement(), pIdArticle);
    }
    else {
      throw new MessageErreurException("Impossible de générer l'identifiant de stock car il n'y a ni établissement ni magasin");
    }
  }
  
  /**
   * Créer un identifiant stock pour le stock d'un établissement.
   * 
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pIdArticle Identifiant de l'article.
   * @return Identifiant stock.
   */
  public static IdStock getInstancePourEtablissement(IdEtablissement pIdEtablissement, IdArticle pIdArticle) {
    return new IdStock(EnumEtatObjetMetier.SANS_PERSISTANCE, pIdEtablissement, null, pIdArticle);
  }
  
  /**
   * Créer un identifiant stock pour le stock d'un magasin.
   * 
   * @param pIdMagasin Identifiant du magasin.
   * @param pIdArticle Identifiant de l'article.
   * @return Identifiant stock.
   */
  public static IdStock getInstancePourMagasin(IdMagasin pIdMagasin, IdArticle pIdArticle) {
    IdMagasin.controlerId(pIdMagasin, true);
    return new IdStock(EnumEtatObjetMetier.MODIFIE, pIdMagasin.getIdEtablissement(), pIdMagasin, pIdArticle);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getIdEtablissement().hashCode();
    code = 37 * code + idMagasin.hashCode();
    code = 37 * code + idArticle.hashCode();
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdStock)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de stock.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdStock id = (IdStock) pObject;
    return idArticle.equals(id.idArticle) && idMagasin.equals(id.idMagasin) && getIdEtablissement().equals(id.getIdEtablissement());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdStock)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdStock id = (IdStock) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    if (idMagasin != null) {
      comparaison = idMagasin.compareTo(id.idMagasin);
      if (comparaison != 0) {
        return comparaison;
      }
    }
    else if (id.idMagasin != null) {
      return 1;
    }
    return idArticle.compareTo(id.idArticle);
  }
  
  @Override
  public String getTexte() {
    // Tester si l'identifiant magasin est renseigné
    if (idMagasin != null) {
      // Utiliser l'identifiant du magasin comme première partie de l'identifiant stock
      return idMagasin.toString() + SEPARATEUR_ID + idArticle.toString();
    }
    else {
      // Utiliser l'identifiant de l'établissement comme première partie de l'identifiant stock
      return getIdEtablissement().toString() + SEPARATEUR_ID + idArticle.toString();
    }
  }
  
  // -- Accesseurs
  
  /**
   * Identifiant du magasin associé au stock.
   * @return Identifiant du magasin (peut être null).
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Identifiant de l'article associé au stock.
   * @return Identifiant de l'article.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
}
