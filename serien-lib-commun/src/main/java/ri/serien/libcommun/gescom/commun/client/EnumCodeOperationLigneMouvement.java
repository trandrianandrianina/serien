/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête d'un document de vente.
 */
public enum EnumCodeOperationLigneMouvement {
  DIVERS('D', "Divers"),
  ENTREE('E', "Entrée"),
  INVENTAIRE('I', "Inventaire"),
  SORTIE('S', "Sortie"),
  TRANSFERT('T', "Transfert");
  
  private final Character code;
  private final String libelle;

  /**
   * Constructeur.
   */
  EnumCodeOperationLigneMouvement(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }

  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }

  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }

  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumCodeOperationLigneMouvement valueOfByCode(Character pCode) {
    for (EnumCodeOperationLigneMouvement value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code opération du mouvement de stock est invalide : " + pCode);
  }
}
