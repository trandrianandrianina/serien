/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique des coordonnées d'un fournisseur.
 *
 * L'identifiant est composé de l'identifiant fournisseur et d'un indice.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdAdresseFournisseur extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_INDICE = 4;
  
  // Variables
  private final IdFournisseur idFournisseur;
  private final Integer indice;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdAdresseFournisseur(EnumEtatObjetMetier pEnumEtatObjetMetier, IdFournisseur pId, Integer pIndice) {
    super(pEnumEtatObjetMetier, pId.getIdEtablissement());
    idFournisseur = controlerFournisseur(pId);
    indice = controlerIndice(pIndice);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdAdresseFournisseur getInstance(IdFournisseur pId, Integer pIndice) {
    return new IdAdresseFournisseur(EnumEtatObjetMetier.MODIFIE, pId, pIndice);
  }
  
  /**
   * Contrôler la validité de l'id fournisseur.
   */
  private static IdFournisseur controlerFournisseur(IdFournisseur pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("L'Id du fournisseur n'est pas renseigné.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du numéro.
   * Il doit être supérieur à zéro et doit comporter au maximum 4 chiffres (entre 1 et 9 999).
   */
  private static Integer controlerIndice(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("L'indice des adresses du fournisseur n'est pas renseigné.");
    }
    if (pValeur < 0) {
      throw new MessageErreurException("L'indice des adresses du fournisseur est inférieur à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_INDICE)) {
      throw new MessageErreurException("L'indice des adresses du fournisseur est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + idFournisseur.hashCode();
    cle = 37 * cle + indice.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdAdresseFournisseur)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants d'adresses fournisseur");
    }
    
    IdAdresseFournisseur id = (IdAdresseFournisseur) pObject;
    return (idFournisseur.equals(id.idFournisseur) && indice.equals(id.indice));
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdAdresseFournisseur)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdAdresseFournisseur id = (IdAdresseFournisseur) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idFournisseur.compareTo(id.idFournisseur);
    if (comparaison != 0) {
      return comparaison;
    }
    return indice.compareTo(id.indice);
  }
  
  /**
   * Cette méthode est surchargée pour définir le format d'affichage standard de cet identifiant.
   * Le format souhaité est constitué par les composants de l'identifiant (sauf l'établissement) séparés par un SEPARATEUR_ID.
   */
  @Override
  public String getTexte() {
    return idFournisseur.toString() + SEPARATEUR_ID + indice;
  }
  
  // -- Accesseurs
  
  /**
   * Id fournisseur.
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Indice des coordonnées fournisseur.
   * L'indice des coordonnées est compris entre 1 et 9 999.
   */
  public Integer getIndice() {
    return indice;
  }
}
