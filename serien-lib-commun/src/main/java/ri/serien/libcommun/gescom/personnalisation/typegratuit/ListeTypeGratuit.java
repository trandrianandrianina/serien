/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typegratuit;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

public class ListeTypeGratuit extends ListeClasseMetier<IdTypeGratuit, TypeGratuit, ListeTypeGratuit> {
  
  /**
   * Charge la liste de TypeGratuit suivant une liste d'IdTypeGratuit
   */
  @Override
  public ListeTypeGratuit charger(IdSession pIdSession, List<IdTypeGratuit> pListeId) {
    return null;
  }
  
  /**
   * Charge tout les TypeGratuit suivant l'établissement
   */
  @Override
  public ListeTypeGratuit charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CriteresRechercheTypeGratuit criteres = new CriteresRechercheTypeGratuit();
    criteres.setTypeRecherche(CriteresRechercheTypeGratuit.RECHERCHE_TYPE_GRATUIT);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeTypeGratuit(pIdSession, criteres);
  }
  
}
