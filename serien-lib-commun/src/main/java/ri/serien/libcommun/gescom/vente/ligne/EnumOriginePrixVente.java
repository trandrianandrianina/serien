/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Origine du prix de vente de la ligne.
 * 
 * Cela correspond au champ L1IN18 de la ligne de vente. Ce champ servait initialement à indiquer si un prix est garanti (le prix ne
 * doit plus être modifié par la suite sauf sous certaines conditions). Son usage a été étendu afin de stocker l'origine du calcul du
 * prix de la ligne de vente.
 * 
 * Voici les valeurs utilisées pour la gestion des prix garantis à la ligne :
 * - '0' = prix non garanti à la commande
 * - '1' = prix garanti à la commande
 * - '3' = prix garanti au devis
 * - '4' = prix garanti par représentant autorisé
 * - 'd' = simulation ou webshop : dérogation annuelle uniquement… *
 * - 'A' = prix affaire
 * - 'D' = dérogation
 * - 'H' = chantier
 * 
 * Voici les valeurs utilisées pour l'origine du prix de vente :
 * - 'P' = public, prix standard basé sur la colonne 1, considérée comme étant le prix public en négoce,
 * - 'S' = standard, prix standard du client, hors CNV, hors chantier, ...
 * - 'N' = négocié, prix modifié par l'utilisateur au moment de la saisie de la ligne de vente,
 * - 'H' = chantier, prix issu d'un chantier.
 * - 'C' = CNV client, prix issu d'une CNV qui est rattachée directement au client.
 * - 'G' = groupe CNV, prix issu d'une CNV qui est rattachée au client via un groupe de conditions de ventes.
 * - 'D' = dérogation, prix issu d'une dérogation.
 * - 'A' = affaire, prix issu d'une affaire.
 * - 'V' = consignation, prix de vente (V) d'un article consigné.
 * - 'R' = consignation, prix de reprise (R) d'un article consigné.
 */
public enum EnumOriginePrixVente {
  AUCUN(' ', ""),
  AUCUNE_GARANTIE('0', "Prix non garanti"),
  GARANTI_COMMANDE('1', "Prix garanti à la commande"),
  GARANTI_DEVIS('3', "Prix garanti au devis"),
  GARANTI_PAR_REPRESENTANT('4', "Prix garanti par représentant"),
  SIMULATION('d', "Simulation"),
  
  PUBLIC('P', "Public"),
  STANDARD('S', "Standard"),
  NEGOCIE('N', "Négocié"),
  CHANTIER('H', "Chantier"),
  CNV_CLIENT('C', "CNV Client"),
  GROUPE_CNV('G', "CNV"),
  DEROGATION('D', "Dérogation"),
  AFFAIRE('A', "Affaire"),
  CONSIGNATION('V', "Consignation"),
  DECONSIGNATION('R', "Déconsignation");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOriginePrixVente(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Le libellé associé au code en minuscules.
   */
  public String getLibelleEnMinuscules() {
    return libelle.toLowerCase();
  }
  
  /**
   * Retourner le libellé associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumOriginePrixVente valueOfByCode(Character pCode) {
    for (EnumOriginePrixVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'origine du prix de la ligne de vente est invalide : " + pCode);
  }
  
  /**
   * Retourner l'objet énum par son code fourni sous forme d'une chaîne de caractères d'une longueur de 1.
   */
  static public EnumOriginePrixVente valueOfByCode(String pCode) {
    if (pCode.length() == 1) {
      Character code = pCode.charAt(0);
      for (EnumOriginePrixVente value : values()) {
        if (code.equals(value.getCode())) {
          return value;
        }
      }
    }
    throw new MessageErreurException("L'origine du prix de la ligne de vente est invalide : " + pCode);
  }
}
