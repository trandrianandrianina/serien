/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.adresse;

import java.io.Serializable;

import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Bloc adresse.
 */
public class Adresse implements Serializable, Cloneable {
  // Constantes
  public static final int LONGUEUR_ADRESSE = 30;
  public static final int LONGUEUR_CODEPOSTAL = 5;
  public static final int LONGUEUR_VILLE = 24;
  
  // Variables
  private IdCivilite idCivilite = null;
  private String nom = "";
  private String complementNom = "";
  private String rue = "";
  private String localisation = "";
  private String ville = "";
  private String pays = "";
  private String codePays = "";
  private int codePostal = 0;
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    if (idCivilite != null) {
      code = 37 * code + idCivilite.hashCode();
    }
    if (nom != null) {
      code = 37 * code + nom.hashCode();
    }
    if (complementNom != null) {
      code = 37 * code + complementNom.hashCode();
    }
    if (rue != null) {
      code = 37 * code + rue.hashCode();
    }
    if (localisation != null) {
      code = 37 * code + localisation.hashCode();
    }
    if (ville != null) {
      code = 37 * code + ville.hashCode();
    }
    if (pays != null) {
      code = 37 * code + pays.hashCode();
    }
    if (codePays != null) {
      code = 37 * code + codePays.hashCode();
    }
    return code;
  }
  
  /**
   * Comparer 2 adresses.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    
    if (pObject == null) {
      return false;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof Adresse)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux adresses.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    Adresse adresse = (Adresse) pObject;
    return Constantes.equals(idCivilite, adresse.idCivilite) && Constantes.equals(nom, adresse.nom)
        && Constantes.equals(complementNom, adresse.complementNom) && Constantes.equals(rue, adresse.rue)
        && Constantes.equals(localisation, adresse.localisation) && Constantes.equals(ville, adresse.ville)
        && Constantes.equals(pays, adresse.pays) && Constantes.equals(codePays, adresse.codePays);
  }
  
  /**
   * Renvoit le nom du client.
   * C'est cette méthode qui définit le format d'affichage des clients dans les composants graphiques.
   * Le format attendu est :
   * - "Civilité Nom Prénom" pour les coordonnées avec une civilité (les particuliers).
   * - "RaisonSociale" pour les autres (professionnels).
   */
  @Override
  public String toString() {
    if (idCivilite != null) {
      return idCivilite.getCode() + " " + nom;
    }
    else {
      return nom;
    }
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public Adresse clone() {
    Object o = null;
    try {
      // Récupération l'instance à renvoyer par l'appel de la méthode super.clone()
      o = super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return (Adresse) o;
  }
  
  /**
   * Indique si l'adresse est à blanc.
   */
  public boolean isEmpty() {
    return (nom == null || nom.trim().isEmpty()) && (complementNom == null || complementNom.trim().isEmpty())
        && ((rue == null || rue.trim().isEmpty()) && (localisation == null || localisation.trim().isEmpty())) && (codePostal <= 0)
        && (ville == null || ville.trim().isEmpty());
  }
  
  /**
   * Indiquer si les informations nécessaires pour la création d'un client comptant sont présentes.
   */
  public boolean isCreationPossibleClientComptant() {
    if (nom == null || nom.trim().isEmpty()) {
      return false;
    }
    else if (complementNom == null || complementNom.trim().isEmpty()) {
      return false;
    }
    else if ((rue == null || rue.trim().isEmpty()) && (localisation == null || localisation.trim().isEmpty())) {
      return false;
    }
    else if (codePostal <= 0) {
      return false;
    }
    else if (ville == null || ville.trim().isEmpty()) {
      return false;
    }
    return true;
  }
  
  // -- Accesseurs
  
  /**
   * Civilité de l'adresse (peut être null car ce n'est pas une donnée obligatoire).
   */
  public IdCivilite getIdCivilite() {
    return idCivilite;
  }
  
  /**
   * Modifier la civilité.
   */
  public void setIdCivilite(IdCivilite pIdCivilite) {
    idCivilite = pIdCivilite;
  }
  
  /**
   * Nom.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Modifier le nom.
   */
  public void setNom(String pNom) {
    if (pNom != null) {
      if (pNom.trim().length() > LONGUEUR_ADRESSE) {
        throw new MessageErreurException("La longueur maximale pour le nom est de " + LONGUEUR_ADRESSE + " caractères.");
      }
      nom = pNom;
    }
    else {
      nom = "";
    }
  }
  
  /**
   * Complément de nom.
   */
  public String getComplementNom() {
    return complementNom;
  }
  
  /**
   * Modifier le complément de nom.
   */
  public void setComplementNom(String pComplementNom) {
    if (pComplementNom != null) {
      if (pComplementNom.trim().length() > LONGUEUR_ADRESSE) {
        throw new MessageErreurException("La longueur maximale pour le complément de nom est de " + LONGUEUR_ADRESSE + " caractères.");
      }
      complementNom = pComplementNom;
    }
    else {
      complementNom = "";
    }
  }
  
  /**
   * Rue.
   */
  public String getRue() {
    return rue;
  }
  
  /**
   * Modifier la rue.
   */
  public void setRue(String pRue) {
    if (pRue != null) {
      if (pRue.trim().length() > LONGUEUR_ADRESSE) {
        throw new MessageErreurException("La longueur maximale pour la rue est de " + LONGUEUR_ADRESSE + " caractères.");
      }
      rue = pRue;
    }
    else {
      rue = "";
    }
  }
  
  /**
   * Localisation.
   */
  public String getLocalisation() {
    return localisation;
  }
  
  /**
   * Modifier la localisation.
   */
  public void setLocalisation(String pLocalisation) {
    if (pLocalisation != null) {
      if (pLocalisation.trim().length() > LONGUEUR_ADRESSE) {
        throw new MessageErreurException("La longueur maximale pour la localisation est de " + LONGUEUR_ADRESSE + " caractères.");
      }
      localisation = pLocalisation;
    }
    else {
      localisation = "";
    }
  }
  
  /**
   * Ville.
   */
  public String getVille() {
    return ville;
  }
  
  /**
   * Modifier la ville.
   * La taille de la ville n'est pas contrôlée pour l'instant car il faut voir comment on gère le cas ou la ville contient le code postal.
   * Dans ces cas là, le cham pest plus grand avant découpage mais peut être correct après découpage. Faut-il découper automatiquement
   * en code postal et ville lorsque les deux sont concaténés ? Comment gérer le getVille ?
   */
  public void setVille(String pVille) {
    if (pVille != null) {
      ville = pVille.trim();
    }
    else {
      ville = "";
    }
  }
  
  /**
   * Modifier la ville en retirant le code postal.
   */
  public void setVilleAvecCodePostal(String pVille) {
    if (pVille != null && pVille.length() >= (LONGUEUR_CODEPOSTAL + 1)) {
      setVille(pVille.substring(LONGUEUR_CODEPOSTAL + 1));
    }
    else {
      setVille(pVille);
    }
  }
  
  /**
   * Pays.
   */
  public String getPays() {
    return pays;
  }
  
  /**
   * Modifier le pays.
   */
  public void setPays(String pPays) {
    if (pPays != null) {
      pays = pPays.trim();
    }
    else {
      pays = "";
    }
  }
  
  /**
   * Code pays.
   */
  public String getCodePays() {
    return codePays;
  }
  
  /**
   * Modifier le code pays.
   */
  public void setCodePays(String pCodePays) {
    if (pCodePays != null) {
      codePays = pCodePays.trim();
    }
    else {
      codePays = "";
    }
  }
  
  /**
   * Code postal.
   */
  public int getCodePostal() {
    return codePostal;
  }
  
  /**
   * Code postal formaté sur 5 caractères.
   */
  public String getCodePostalFormate() {
    if (codePostal == 0) {
      return "";
    }
    return String.format("%0" + LONGUEUR_CODEPOSTAL + "d", codePostal);
  }
  
  /**
   * Modifier le code postal.
   */
  public void setCodePostal(int pCodePostal) {
    codePostal = pCodePostal;
  }
  
}
