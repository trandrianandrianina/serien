/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.boncour;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Etat d'un bon de cour.
 * 
 * ATTENTION : En base l'état "manquant" n'est pas persisté. Il est déduit dans les programmes.
 * On persiste seulement non utilisé, utilisé et annulé.
 * 
 * Un manquant est un bon de cour non utilisé dont le numéro est inférieur au dernier bon de cour utilisé.
 */
public enum EnumEtatBonCour {
  NON_UTILISE(0, "Non utilisé"),
  UTILISE(1, "Utilisé"),
  ANNULE(2, "Annulé"),
  MANQUANT(9, "Manquant");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumEtatBonCour(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumEtatBonCour valueOfByCode(Integer pCode) {
    for (EnumEtatBonCour value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'état du bon de cour est invalide : " + pCode);
  }
}
