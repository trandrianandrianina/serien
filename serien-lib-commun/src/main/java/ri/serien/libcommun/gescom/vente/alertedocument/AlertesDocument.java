/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.alertedocument;

import java.io.Serializable;
import java.math.BigDecimal;

public class AlertesDocument implements Serializable {
  // Constantes TODO il va falloir trouver les messages exacts
  private static final String ALERTE_TYPE_AVOIR = "Attention le type d'avoir est %s.";
  private static final String ALERTE_FRANCO_GARANTI_COMMANDE = "Attention le franco est garanti à la commande.";
  private static final String ALERTE_FRANCO_NON_ATTEINT = "Le franco n'a pas atteint %f.";
  private static final String ALERTE_POIDS_FRANCO_GARANTI = "Attention au poids franco garanti.";
  private static final String ALERTE_MINI_COMMANDE_NON_ATTEINT = "Le montant minimum de %f n'a pas été atteint.";
  private static final String ALERTE_MARGE_MINI_NON_ATTEINTE = "La marge minimum de %f n'a pas été atteinte.";
  private static final String ALERTE_MARGE_ANORMALEMENT_HAUTE = "La marge est anormalement haute %f.";
  private static final String ALERTE_FACTURE_DIFFEREE = "Attention la facture est différée.";
  private static final String ALERTE_EXISTE_SITUATION_JURIDIQUE = "Attention il existe une situation juridique.";
  private static final String ALERTE_ECHEANCE_DEPASSEE = "Attention l'échéance est dépassée.";
  private static final String ALERTE_STOCK_NON_DISPONIBLE = "Stock non disponible.";
  private static final String ALERTE_AUCUNE_LIGNE_LIVRABLE = "Aucune ligne livrable.";
  private static final String ALERTE_NOMBRE_LIGNES_EN_ERREUR = "Le nombre de lignes est en erreur %d.";
  private static final String ALERTE_AU_MOINS_UN_LOT_NON_STOCK = "Au moins un des lots n'est pas en stock.";
  private static final String ALERTE_ACOMPTE_OBLIGATOIRE_NON_RECU = "L'acompte obligatoire n'a pas été reçu.";
  private static final String ALERTE_ACOMPTE_OBLIGATOIRE = "Un acompte de %.2f %% est obligatoire.";
  private static final String ALERTE_MONTANT_AUTORISE_ESCOMPTE = "Le montant de l'escompte autorisé est de %f.";
  private static final String ALERTE_PRIX_SUR_LIGNE = "Erreur de prix sur la ligne %s.";
  private static final String ALERTE_DOUBLE_QUANTITE_NON_SAISIE = "La double quantité n'a pas été saisie sur la ligne %s.";
  private static final String ALERTE_NUMERO_SERIE_INCORRECT = "Le numéro de série est incorrect sur la ligne %s.";
  private static final String ALERTE_NUMERO_LOT_INCORRECT = "Le numéro de lot est incorrect sur la ligne %s.";
  private static final String ALERTE_ADRESSE_STOCK_INCORRECTE = "L'adresse de stockage est incorrecte sur la ligne %s.";
  private static final String ERREUR_PRIX_VENTE_INFERIEUR_PUMP = "L'article %s présente un prix inférieur au PUMP.";
  private static final String ERREUR_PRIX_VENTE_INFERIEUR_PRV = "L'article %s présente un prix inférieur au PRV.";
  
  // Variables SVGVM0003 : Service controlerFinDocument
  private boolean typeAvoir = false;
  private String typeAvoirAttendu = "";
  private boolean francoGarantiCommande = false;
  private boolean francoNonAtteint = false;
  private BigDecimal francoAttendu = null;
  private boolean poidsFrancoNonAtteint = false;
  private boolean miniCommandeNonAtteint = false;
  private BigDecimal miniCommandeAttendu = null;
  private boolean margeMiniNonAtteinte = false;
  private BigDecimal poucentageMargeMiniAttendu = null;
  private boolean margeAnormalementHaute = false;
  private BigDecimal poucentageMargeHauteAttendu = null;
  private boolean attentionFactureDifferee = false;
  private boolean existeSituationJuridique = false;
  private boolean echeanceDepassee = false;
  private boolean stockNonDisponible = false;
  private boolean aucuneLigneLivrable = false;
  private boolean nombreLignesEnErreur = false;
  private int nombreLignesEnErreurAttendu = 0;
  private boolean auMoinsUnLotNonEnStock = false;
  private boolean premierPlafondDepasse = false;
  private String messagePremierPlafondDepasse = "";
  private boolean tousLesPlafondsDepasses = false;
  private String messageTousLesPlafondsDepasses = "";
  private boolean acompteObligatoireNonRecu = false;
  private boolean acompteObligatoireNormal = false;
  private BigDecimal pourcentageAcompteNormal = BigDecimal.ZERO; // Information du paramètre "CC"
  private boolean montantAutoriseEscompteDepasse = false;
  private BigDecimal montantAutoriseEscompte = null; // Information dans le champ CLVAE
  private boolean erreurPrixSurLigne = false;
  private String numeroLigneErreurPrixSurLigne = "";
  private boolean doubleQuantiteNonSaisie = false;
  private String numeroLigneDoubleQuantiteNonSaisie = "";
  private boolean numeroSerieIncorrect = false;
  private String numeroLigneNumeroSerieIncorrect = "";
  private boolean numeroLotIncorrect = false;
  private String numeroLigneNumeroLotIncorrect = "";
  private boolean adresseStockIncorrecte = false;
  private String numeroLigneAdresseStockIncorrecte = "";
  private boolean acompteObligatoireDetectionArticleSpecial = false;
  private BigDecimal pourcentageAcompteDetectionArticleSpecial = BigDecimal.ZERO; // Information du paramètre "CC"
  private boolean erreurPrixInferieurPump = false;
  private String codeArticlePrixInferieurPump = "";
  private boolean erreurPrixInferieurPrv = false;
  private String codeArticlePrixInferieurPrv = "";
  
  // Variables
  private String messages = "";
  
  // -- Méthodes publiques
  
  /**
   * Controle et liste les messages d'alerte.
   */
  public boolean controlerAlertes() {
    StringBuilder message = new StringBuilder();
    boolean alertesTrouvees = false;
    messages = "";
    
    if (typeAvoir) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_TYPE_AVOIR, typeAvoirAttendu)).append('\n');
    }
    if (francoGarantiCommande) {
      alertesTrouvees = true;
      message.append(ALERTE_FRANCO_GARANTI_COMMANDE).append('\n');
    }
    if (francoNonAtteint) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_FRANCO_NON_ATTEINT, francoAttendu)).append('\n');
    }
    if (poidsFrancoNonAtteint) {
      alertesTrouvees = true;
      message.append(ALERTE_POIDS_FRANCO_GARANTI).append('\n');
    }
    if (miniCommandeNonAtteint) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_MINI_COMMANDE_NON_ATTEINT, miniCommandeAttendu)).append('\n');
    }
    if (margeMiniNonAtteinte) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_MARGE_MINI_NON_ATTEINTE, poucentageMargeMiniAttendu)).append('\n');
    }
    if (margeAnormalementHaute) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_MARGE_ANORMALEMENT_HAUTE, poucentageMargeHauteAttendu)).append('\n');
    }
    if (attentionFactureDifferee) {
      alertesTrouvees = true;
      message.append(ALERTE_FACTURE_DIFFEREE).append('\n');
    }
    if (existeSituationJuridique) {
      alertesTrouvees = true;
      message.append(ALERTE_EXISTE_SITUATION_JURIDIQUE).append('\n');
    }
    if (echeanceDepassee) {
      alertesTrouvees = true;
      message.append(ALERTE_ECHEANCE_DEPASSEE).append('\n');
    }
    if (stockNonDisponible) {
      alertesTrouvees = true;
      message.append(ALERTE_STOCK_NON_DISPONIBLE).append('\n');
    }
    if (aucuneLigneLivrable) {
      alertesTrouvees = true;
      message.append(ALERTE_AUCUNE_LIGNE_LIVRABLE).append('\n');
    }
    if (nombreLignesEnErreur) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_NOMBRE_LIGNES_EN_ERREUR, nombreLignesEnErreurAttendu)).append('\n');
    }
    if (auMoinsUnLotNonEnStock) {
      alertesTrouvees = true;
      message.append(ALERTE_AU_MOINS_UN_LOT_NON_STOCK).append('\n');
    }
    if (premierPlafondDepasse) {
      alertesTrouvees = true;
      message.append(messagePremierPlafondDepasse).append('\n');
    }
    if (tousLesPlafondsDepasses) {
      alertesTrouvees = true;
      message.append(messageTousLesPlafondsDepasses).append('\n');
    }
    if (acompteObligatoireNonRecu) {
      alertesTrouvees = true;
      message.append(ALERTE_ACOMPTE_OBLIGATOIRE_NON_RECU).append('\n');
    }
    if (acompteObligatoireNormal) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_ACOMPTE_OBLIGATOIRE, pourcentageAcompteNormal)).append('\n');
    }
    if (montantAutoriseEscompteDepasse) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_MONTANT_AUTORISE_ESCOMPTE, montantAutoriseEscompte)).append('\n');
    }
    if (erreurPrixSurLigne) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_PRIX_SUR_LIGNE, numeroLigneErreurPrixSurLigne)).append('\n');
    }
    if (doubleQuantiteNonSaisie) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_DOUBLE_QUANTITE_NON_SAISIE, numeroLigneDoubleQuantiteNonSaisie)).append('\n');
    }
    if (numeroSerieIncorrect) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_NUMERO_SERIE_INCORRECT, numeroLigneNumeroSerieIncorrect)).append('\n');
    }
    if (numeroLotIncorrect) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_NUMERO_LOT_INCORRECT, numeroLigneNumeroLotIncorrect)).append('\n');
    }
    if (adresseStockIncorrecte) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_ADRESSE_STOCK_INCORRECTE, numeroLigneAdresseStockIncorrecte)).append('\n');
    }
    if (acompteObligatoireDetectionArticleSpecial) {
      alertesTrouvees = true;
      message.append(String.format(ALERTE_ACOMPTE_OBLIGATOIRE, pourcentageAcompteDetectionArticleSpecial)).append('\n');
    }
    if (erreurPrixInferieurPump) {
      alertesTrouvees = true;
      message.append(String.format(ERREUR_PRIX_VENTE_INFERIEUR_PUMP, codeArticlePrixInferieurPump)).append('\n');
    }
    if (erreurPrixInferieurPrv) {
      alertesTrouvees = true;
      message.append(String.format(ERREUR_PRIX_VENTE_INFERIEUR_PRV, codeArticlePrixInferieurPrv)).append('\n');
    }
    
    if (alertesTrouvees) {
      messages = message.toString().trim();
    }
    return alertesTrouvees;
  }
  
  // Accesseurs
  
  public String getMessages() {
    return messages;
  }
  
  public void setMessages(String messages) {
    this.messages = messages;
  }
  
  public boolean isTypeAvoir() {
    return typeAvoir;
  }
  
  public void setTypeAvoir(boolean typeAvoir) {
    this.typeAvoir = typeAvoir;
  }
  
  public String getTypeAvoirAttendu() {
    return typeAvoirAttendu;
  }
  
  public void setTypeAvoirAttendu(String typeAvoirAttendu) {
    this.typeAvoirAttendu = typeAvoirAttendu;
  }
  
  public boolean isFrancoGarantiCommande() {
    return francoGarantiCommande;
  }
  
  public void setFrancoGarantiCommande(boolean francoGarantiCommande) {
    this.francoGarantiCommande = francoGarantiCommande;
  }
  
  public boolean isFrancoNonAtteint() {
    return francoNonAtteint;
  }
  
  public void setFrancoNonAtteint(boolean francoNonAtteint) {
    this.francoNonAtteint = francoNonAtteint;
  }
  
  public BigDecimal getFrancoAttendu() {
    return francoAttendu;
  }
  
  public void setFrancoAttendu(BigDecimal francoAttendu) {
    this.francoAttendu = francoAttendu;
  }
  
  public boolean isPoidsFrancoNonAtteint() {
    return poidsFrancoNonAtteint;
  }
  
  public void setPoidsFrancoNonAtteint(boolean poidsFrancoNonAtteint) {
    this.poidsFrancoNonAtteint = poidsFrancoNonAtteint;
  }
  
  public boolean isMiniCommandeNonAtteint() {
    return miniCommandeNonAtteint;
  }
  
  public void setMiniCommandeNonAtteint(boolean miniCommandeNonAtteint) {
    this.miniCommandeNonAtteint = miniCommandeNonAtteint;
  }
  
  public BigDecimal getMiniCommandeAttendu() {
    return miniCommandeAttendu;
  }
  
  public void setMiniCommandeAttendu(BigDecimal miniCommandeAttendu) {
    this.miniCommandeAttendu = miniCommandeAttendu;
  }
  
  public boolean isMargeMiniNonAtteinte() {
    return margeMiniNonAtteinte;
  }
  
  public void setMargeMiniNonAtteinte(boolean margeMiniNonAtteinte) {
    this.margeMiniNonAtteinte = margeMiniNonAtteinte;
  }
  
  public BigDecimal getPoucentageMargeMiniAttendu() {
    return poucentageMargeMiniAttendu;
  }
  
  public void setPoucentageMargeMiniAttendu(BigDecimal poucentageMargeMiniAttendu) {
    this.poucentageMargeMiniAttendu = poucentageMargeMiniAttendu;
  }
  
  public boolean isMargeAnormalementHaute() {
    return margeAnormalementHaute;
  }
  
  public void setMargeAnormalementHaute(boolean margeAnormalementHaute) {
    this.margeAnormalementHaute = margeAnormalementHaute;
  }
  
  public BigDecimal getPoucentageMargeHauteAttendu() {
    return poucentageMargeHauteAttendu;
  }
  
  public void setPoucentageMargeHauteAttendu(BigDecimal poucentageMargeHauteAttendu) {
    this.poucentageMargeHauteAttendu = poucentageMargeHauteAttendu;
  }
  
  public boolean isAttentionFactureDifferee() {
    return attentionFactureDifferee;
  }
  
  public void setAttentionFactureDifferee(boolean attentionFactureDifferee) {
    this.attentionFactureDifferee = attentionFactureDifferee;
  }
  
  public boolean isExisteSituationJuridique() {
    return existeSituationJuridique;
  }
  
  public void setExisteSituationJuridique(boolean existeSituationJuridique) {
    this.existeSituationJuridique = existeSituationJuridique;
  }
  
  public boolean isEcheanceDepassee() {
    return echeanceDepassee;
  }
  
  public void setEcheanceDepassee(boolean echeanceDepassee) {
    this.echeanceDepassee = echeanceDepassee;
  }
  
  public boolean isStockNonDisponible() {
    return stockNonDisponible;
  }
  
  public void setStockNonDisponible(boolean stockNonDisponible) {
    this.stockNonDisponible = stockNonDisponible;
  }
  
  public boolean isAucuneLigneLivrable() {
    return aucuneLigneLivrable;
  }
  
  public void setAucuneLigneLivrable(boolean aucuneLigneLivrable) {
    this.aucuneLigneLivrable = aucuneLigneLivrable;
  }
  
  public boolean isNombreLignesEnErreur() {
    return nombreLignesEnErreur;
  }
  
  public void setNombreLignesEnErreur(boolean nombreLignesEnErreur) {
    this.nombreLignesEnErreur = nombreLignesEnErreur;
  }
  
  public int getNombreLignesEnErreurAttendu() {
    return nombreLignesEnErreurAttendu;
  }
  
  public void setNombreLignesEnErreurAttendu(int nombreLignesEnErreurAttendu) {
    this.nombreLignesEnErreurAttendu = nombreLignesEnErreurAttendu;
  }
  
  public boolean isAuMoinsUnLotNonEnStock() {
    return auMoinsUnLotNonEnStock;
  }
  
  public void setAuMoinsUnLotNonEnStock(boolean auMoinsUnLotNonEnStock) {
    this.auMoinsUnLotNonEnStock = auMoinsUnLotNonEnStock;
  }
  
  public boolean isPremierPlafondDepasse() {
    return premierPlafondDepasse;
  }
  
  public void setPremierPlafondDepasse(boolean premierPlafondDepasse) {
    this.premierPlafondDepasse = premierPlafondDepasse;
  }
  
  public String getMessagePremierPlafondDepasse() {
    return messagePremierPlafondDepasse;
  }
  
  public void setMessagePremierPlafondDepasse(String messagePremierPlafondDepasse) {
    this.messagePremierPlafondDepasse = messagePremierPlafondDepasse;
  }
  
  public boolean isTousLesPlafondsDepasses() {
    return tousLesPlafondsDepasses;
  }
  
  public void setTousLesPlafondsDepasses(boolean tousLesPlafondsDepasses) {
    this.tousLesPlafondsDepasses = tousLesPlafondsDepasses;
  }
  
  public String getMessageTousLesPlafondsDepasses() {
    return messageTousLesPlafondsDepasses;
  }
  
  public void setMessageTousLesPlafondsDepasses(String messageTousLesPlafondsDepasses) {
    this.messageTousLesPlafondsDepasses = messageTousLesPlafondsDepasses;
  }
  
  public boolean isAcompteObligatoireNonRecu() {
    return acompteObligatoireNonRecu;
  }
  
  public void setAcompteObligatoireNonRecu(boolean acompteObligatoireNonRecu) {
    this.acompteObligatoireNonRecu = acompteObligatoireNonRecu;
  }
  
  public boolean isAcompteObligatoireNormal() {
    return acompteObligatoireNormal;
  }
  
  public void setAcompteObligatoireNormal(boolean acompteObligatoire) {
    this.acompteObligatoireNormal = acompteObligatoire;
  }
  
  public BigDecimal getPourcentageAcompteNormal() {
    return pourcentageAcompteNormal;
  }
  
  public void setPourcentageAcompteNormal(BigDecimal pourcentageAcompte) {
    this.pourcentageAcompteNormal = pourcentageAcompte;
  }
  
  public boolean isMontantAutoriseEscompteDepasse() {
    return montantAutoriseEscompteDepasse;
  }
  
  public void setMontantAutoriseEscompteDepasse(boolean montantAutoriseEscompteDepasse) {
    this.montantAutoriseEscompteDepasse = montantAutoriseEscompteDepasse;
  }
  
  public BigDecimal getMontantAutoriseEscompte() {
    return montantAutoriseEscompte;
  }
  
  public void setMontantAutoriseEscompte(BigDecimal montantAutoriseEscompte) {
    this.montantAutoriseEscompte = montantAutoriseEscompte;
  }
  
  public boolean isErreurPrixSurLigne() {
    return erreurPrixSurLigne;
  }
  
  public void setErreurPrixSurLigne(boolean erreurPrixSurLigne) {
    this.erreurPrixSurLigne = erreurPrixSurLigne;
  }
  
  public String getNumeroLigneErreurPrixSurLigne() {
    return numeroLigneErreurPrixSurLigne;
  }
  
  public void setNumeroLigneErreurPrixSurLigne(String numeroLigneErreurPrixSurLigne) {
    this.numeroLigneErreurPrixSurLigne = numeroLigneErreurPrixSurLigne;
  }
  
  public boolean isDoubleQuantiteNonSaisie() {
    return doubleQuantiteNonSaisie;
  }
  
  public void setDoubleQuantiteNonSaisie(boolean doubleQuantiteNonSaisie) {
    this.doubleQuantiteNonSaisie = doubleQuantiteNonSaisie;
  }
  
  public String getNumeroLigneDoubleQuantiteNonSaisie() {
    return numeroLigneDoubleQuantiteNonSaisie;
  }
  
  public void setNumeroLigneDoubleQuantiteNonSaisie(String numeroLigneDoubleQuantiteNonSaisie) {
    this.numeroLigneDoubleQuantiteNonSaisie = numeroLigneDoubleQuantiteNonSaisie;
  }
  
  public boolean isNumeroSerieIncorrect() {
    return numeroSerieIncorrect;
  }
  
  public void setNumeroSerieIncorrect(boolean numeroSerieIncorrect) {
    this.numeroSerieIncorrect = numeroSerieIncorrect;
  }
  
  public String getNumeroLigneNumeroSerieIncorrect() {
    return numeroLigneNumeroSerieIncorrect;
  }
  
  public void setNumeroLigneNumeroSerieIncorrect(String numeroLigneNumeroSerieIncorrect) {
    this.numeroLigneNumeroSerieIncorrect = numeroLigneNumeroSerieIncorrect;
  }
  
  public boolean isNumeroLotIncorrect() {
    return numeroLotIncorrect;
  }
  
  public void setNumeroLotIncorrect(boolean numeroLotIncorrect) {
    this.numeroLotIncorrect = numeroLotIncorrect;
  }
  
  public String getNumeroLigneNumeroLotIncorrect() {
    return numeroLigneNumeroLotIncorrect;
  }
  
  public void setNumeroLigneNumeroLotIncorrect(String numeroLigneNumeroLotIncorrect) {
    this.numeroLigneNumeroLotIncorrect = numeroLigneNumeroLotIncorrect;
  }
  
  public boolean isAdresseStockIncorrecte() {
    return adresseStockIncorrecte;
  }
  
  public void setAdresseStockIncorrecte(boolean adresseStockIncorrecte) {
    this.adresseStockIncorrecte = adresseStockIncorrecte;
  }
  
  public String getNumeroLigneAdresseStockIncorrecte() {
    return numeroLigneAdresseStockIncorrecte;
  }
  
  public void setNumeroLigneadresseStockIncorrecte(String numeroLigneAdresseStockIncorrecte) {
    this.numeroLigneAdresseStockIncorrecte = numeroLigneAdresseStockIncorrecte;
  }
  
  public boolean isAcompteObligatoireDetectionArticleSpecial() {
    return acompteObligatoireDetectionArticleSpecial;
  }
  
  public void setAcompteObligatoireDetectionArticleSpecial(boolean acompteObligatoireDetectionArticleSpecial) {
    this.acompteObligatoireDetectionArticleSpecial = acompteObligatoireDetectionArticleSpecial;
  }
  
  public BigDecimal getPourcentageAcompteDetectionArticleSpecial() {
    return pourcentageAcompteDetectionArticleSpecial;
  }
  
  public void setPourcentageAcompteDetectionArticleSpecial(BigDecimal pourcentageAcompteDetectionArticleSpecial) {
    this.pourcentageAcompteDetectionArticleSpecial = pourcentageAcompteDetectionArticleSpecial;
  }
  
  public boolean isPrixInferieurPump() {
    return erreurPrixInferieurPump;
  }
  
  public void setPrixInferieurPump(boolean prixInferieurPump) {
    this.erreurPrixInferieurPump = prixInferieurPump;
  }
  
  public String getCodeArticlePrixInferieurPump() {
    return codeArticlePrixInferieurPump;
  }
  
  public void setCodeArticlePrixInferieurPump(String codeArticlePrixInferieurPump) {
    this.codeArticlePrixInferieurPump = codeArticlePrixInferieurPump;
  }
  
  public boolean isErreurPrixInferieurPump() {
    return erreurPrixInferieurPump;
  }
  
  public void setErreurPrixInferieurPump(boolean erreurPrixInferieurPump) {
    this.erreurPrixInferieurPump = erreurPrixInferieurPump;
  }
  
  public boolean isErreurPrixInferieurPrv() {
    return erreurPrixInferieurPrv;
  }
  
  public void setErreurPrixInferieurPrv(boolean erreurPrixInferieurPrv) {
    this.erreurPrixInferieurPrv = erreurPrixInferieurPrv;
  }
  
  public String getCodeArticlePrixInferieurPrv() {
    return codeArticlePrixInferieurPrv;
  }
  
  public void setCodeArticlePrixInferieurPrv(String codeArticlePrixInferieurPrv) {
    this.codeArticlePrixInferieurPrv = codeArticlePrixInferieurPrv;
  }
  
}
