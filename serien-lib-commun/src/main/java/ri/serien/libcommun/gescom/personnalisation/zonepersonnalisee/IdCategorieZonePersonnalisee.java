/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une catégorie de zone personnalisée longue.
 *
 * L'identifiant est composé du code établissement et du nom de la catégorie zone personnalisee
 * Le code catégorie zone personnalisee correspond au paramètre ZP du menu des ventes ou des achats.
 * Il est constitué de 1 à 5 caractères et se présente sous la forme AAAXX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdCategorieZonePersonnalisee extends AbstractIdAvecEtablissement {
  private static final int LONGUEUR_CODE_MIN = 1;
  private static final int LONGUEUR_CODE_MAX = 5;
  
  // Variables
  private final String code;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdCategorieZonePersonnalisee(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdCategorieZonePersonnalisee getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new IdCategorieZonePersonnalisee(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  /**
   * Contrôler la validité du code de la catégorie zone personnalisee
   * Le code de la zone géographique est une chaîne alphanumérique de 1 à 5 caractères.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code de la catégorie de zone personnalisée longue n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MAX) {
      throw new MessageErreurException(
          "Le code de la catégorie de zone personnalisée longue doit comporter  " + LONGUEUR_CODE_MIN + " caractères : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdCategorieZonePersonnalisee controlerId(IdCategorieZonePersonnalisee pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de la catégorie de zone personnalisée longue est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException(
          "L'identifiant de la catégorie de zone personnalisée longue n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdCategorieZonePersonnalisee)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de catégorie de zone personnalisée longue.");
    }
    IdCategorieZonePersonnalisee id = (IdCategorieZonePersonnalisee) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdCategorieZonePersonnalisee)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdCategorieZonePersonnalisee id = (IdCategorieZonePersonnalisee) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
  // -- Accesseurs
  
  /**
   * Code de la catégorie de zone personnalisée longue.
   * Le code est une chaîne alphanumérique de 1 caractère.
   */
  public String getCode() {
    return code;
  }
}
