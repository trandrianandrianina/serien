/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Etat d'un document de vente de type devis ou chantier.
 */
public enum EnumEtatDevisDocumentVente {
  ATTENTE(0, "En attente"),
  ENREGISTRE(1, "Validé"),
  EDITE(2, "Edité"),
  SIGNE(3, "Signé"),
  VALIDITE_DEPASSEE(4, "Validité dépassée"),
  PERDU(5, "Perdu"),
  CLOTURE(6, "Clôturé");
  
  private final Integer code;
  private final String libelle;

  /**
   * Constructeur.
   */
  EnumEtatDevisDocumentVente(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }

  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }

  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }

  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumEtatDevisDocumentVente valueOfByCode(Integer pCode) {
    for (EnumEtatDevisDocumentVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'état du document de vente est invalide : " + pCode);
  }
}
