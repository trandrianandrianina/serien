/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.unite;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une unité.
 *
 * L'identifiant est composé du code de l'unité.
 * Le code unité correspond au paramètre UN du menu des ventes ou des achats.
 * Il est constitué de 1 à 2 caractères et se présente sous la forme AX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdUnite extends AbstractId {
  // Constantes
  public static final String CODE_UNITE_PAR_DEFAUT = "U";
  
  private static final int LONGUEUR_CODE_MIN = 1;
  private static final int LONGUEUR_CODE_MAX = 2;
  
  // Variables
  private final String code;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdUnite(EnumEtatObjetMetier pEnumEtatObjetMetier, String pCode) {
    super(pEnumEtatObjetMetier);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdUnite getInstance(String pCode) {
    return new IdUnite(EnumEtatObjetMetier.MODIFIE, pCode);
  }
  
  /**
   * Contrôler la validité du code de l'unité.
   * Le code unité est une chaîne alphanumérique de 1 à 2 caractères.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code de l'unité n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MAX) {
      throw new MessageErreurException(
          "Le code de l'unité doit comporter entre " + LONGUEUR_CODE_MIN + " et " + LONGUEUR_CODE_MAX + " caractères : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdUnite controlerId(IdUnite pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de l'unité est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de l'unité n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // -- Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdUnite)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants d'unité.");
    }
    IdUnite id = (IdUnite) pObject;
    return code.equals(id.getCode());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdUnite)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdUnite id = (IdUnite) pObject;
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return "" + code;
  }
  
  // -- Accesseurs
  
  /**
   * Code unité.
   * Le code de l'unité est une chaîne alphanumérique de 1 à 2 caractères.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Indiquer si cet identifiant d'unité est associée à une gestion des prix sur 4 décimales.
   * Par défaut, le logiciel stocke les prix sur 2 décimales. Lorsque l'identifiant de l'unité contient le caractère *
   * en deuxième position, cela veut dire que le prix est gérée sur 4 décimales.
   */
  public boolean isGestion4Decimales() {
    return code != null && code.length() >= 2 && code.charAt(1) == '*';
  }
}
