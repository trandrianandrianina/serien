/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document.plage;

import java.io.Serializable;

import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.MessageErreurException;

public class PlageDocumentVente implements Serializable {
  private IdDocumentVente idDocumentVenteDebut = null;
  private IdDocumentVente idDocumentVenteFin = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public PlageDocumentVente() {
    super();
  }
  
  /**
   * Constructeur avec les identifiants des documents de ventes de début et de fin.
   * @param pIdDocumentVenteDebut Identifiant du document de ventes de début.
   * @param pIdDocumentVenteFin Identifiant du document de ventes de fin.
   */
  public PlageDocumentVente(IdDocumentVente pIdDocumentVenteDebut, IdDocumentVente pIdDocumentVenteFin) {
    super();
    if (!isParametrePlageDocumentVenteValide(pIdDocumentVenteDebut, pIdDocumentVenteFin)) {
      throw new MessageErreurException("Les valeurs à définir dans la plage de document de ventes ne sont pas valides.");
    }
    idDocumentVenteDebut = pIdDocumentVenteDebut;
    idDocumentVenteFin = pIdDocumentVenteFin;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Contrôler la plage document de ventes.
   * @param pIdDocumentVenteDebut Identifiant du document de ventes de début.
   * @param pIdDocumentVenteFin Identifiant du document de ventes de fin.
   * @return
   *         true si le numéro document de ventes de début est inférieur ou égal à celui de la fin.<br>
   *         false si le numéro document de ventes du début est supérieur à celui de la fin.
   */
  private boolean isParametrePlageDocumentVenteValide(IdDocumentVente pIdDocumentVenteDebut, IdDocumentVente pIdDocumentVenteFin) {
    if (pIdDocumentVenteDebut != null && pIdDocumentVenteFin != null) {
      // Une valeur négatif indique que le document de ventes de début est inférieur au document de ventes de fin.
      return pIdDocumentVenteDebut.compareTo(pIdDocumentVenteFin) < 0;
    }
    
    return true;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner l'identifiant du document de ventes de début.
   * @return Identifiant du document de ventes de début.
   */
  public IdDocumentVente getIdDocumentVenteDebut() {
    return idDocumentVenteDebut;
  }
  
  /**
   * Définir l'identifiant du document de ventes de début.
   * @param pIdDocumentVenteDebut Identifiant du document de ventes de début à définir.
   */
  public void setIdDocumentVenteDebut(IdDocumentVente pIdDocumentVenteDebut) {
    idDocumentVenteDebut = pIdDocumentVenteDebut;
  }
  
  /**
   * Retourner l'identifiant du document de ventes de fin.
   * @return Identifiant du document de ventes de fin.
   */
  public IdDocumentVente getIdDocumentVenteFin() {
    return idDocumentVenteFin;
  }
  
  /**
   * Définir l'identifiant du document de ventes de fin.
   * @param pIdDocumentVenteFin Identifiant du document de ventes de fin à définir.
   */
  public void setIdDocumentVenteFin(IdDocumentVente pIdDocumentVenteFin) {
    idDocumentVenteFin = pIdDocumentVenteFin;
  }
}
