/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.unite;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Unité.
 */
public class Unite extends AbstractClasseMetier<IdUnite> {
  // Variables
  private String libelleLong = "";
  private String libelleCourt = "";
  private int nbDecimale = 0;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public Unite(IdUnite pIdUnite) {
    super(pIdUnite);
  }
  
  @Override
  public String getTexte() {
    return getLibelle();
  }
  
  /**
   * Libellé de l'unité.
   */
  public String getLibelle() {
    if (libelleCourt != null && libelleCourt.length() > 0) {
      return libelleCourt;
    }
    if (libelleLong != null && libelleLong.length() > 0) {
      return libelleLong;
    }
    return getId().getCode();
  }
  
  /**
   * Libellé court de l'unité.
   */
  public String getLibelleCourt() {
    return libelleCourt;
  }
  
  /**
   * Modifier le libellé court de l'unité.
   */
  public void setLibelleCourt(String pLibelleCourt) {
    libelleCourt = pLibelleCourt;
  }
  
  /**
   * Libellé standard de l'unité.
   */
  public String getLibelleLong() {
    return libelleLong;
  }
  
  /**
   * Modifier le libellé standard de l'unité.
   */
  public void setLibelleLong(String pLibelleLong) {
    libelleLong = pLibelleLong;
  }
  
  /**
   * Précision de l'unité (nombre de décimales significatives).
   */
  public int getNombreDecimale() {
    return nbDecimale;
  }
  
  /**
   * Modifier la précision de l'unité (nombre de décimales significatives).
   */
  public void setNombreDecimale(int pPrecision) {
    nbDecimale = pPrecision;
  }
}
