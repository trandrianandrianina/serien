/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.reglement;

import java.util.ArrayList;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de modes de règlements.
 */
public class ListeModeReglement extends ArrayList<ModeReglement> {
  /**
   * Constructeur.
   */
  public ListeModeReglement() {
  }
  
  /**
   * Charge tous les règlements de l'établissement donnée.
   */
  public static ListeModeReglement chargerTout(IdSession pIdSession, boolean aAjouteEnregistrementVide) {
    CritereModeReglement criteres = new CritereModeReglement();
    criteres.setTypeRecherche(CritereModeReglement.TOUT_RETOURNER_POUR_UN_ETABLISSEMENT);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    return ManagerServiceParametre.chargerListeModeReglement(pIdSession, criteres);
  }
  
  /**
   * Retourne le libellé de la règlement à partir de son code.
   */
  public String retournerLibelle(IdModeReglement pId) {
    if (pId == null || pId.getCode().isEmpty()) {
      return "";
    }
    ModeReglement modeReglement = getModeReglementById(pId);
    if (modeReglement != null) {
      return modeReglement.getLibelle();
    }
    return "";
  }
  
  public ModeReglement getModeReglementById(IdModeReglement pIdModeReglement) {
    for (ModeReglement modeReglement : this) {
      if (Constantes.equals(pIdModeReglement, modeReglement.getId())) {
        return modeReglement;
      }
    }
    return null;
  }
}
