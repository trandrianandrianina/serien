/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.commun.adresse.EnumErreurAdresse;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.reglement.IdModeReglement;
import ri.serien.libcommun.gescom.personnalisation.transporteur.IdTransporteur;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.IdTypeFacturation;
import ri.serien.libcommun.gescom.vente.commune.CritereCommune;
import ri.serien.libcommun.gescom.vente.commune.ListeCommune;
import ri.serien.libcommun.gescom.vente.document.EnumTypeEdition;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceCommun;

public class Client extends ClientBase {
  // Constantes
  public static final int TAILLE_TYPE_ZONE_PERSONNALISEE = 1;
  public static final int TAILLE_ZONE_PERSONNALISEE = 30;
  
  // Variables fichiers
  private EnumEtatFicheClient etatFiche = null;
  private Date dateCreation = null; // Date de création
  private Date dateModification = null; // Date de modification
  private Date dateTraitement = null; // Date de traitement
  private String critereAnalyse1 = ""; // Critères d'analyse 1
  private String critereAnalyse2 = ""; // Critères d'analyse 2
  private String critereAnalyse3 = ""; // Critères d'analyse 3
  private String critereAnalyse4 = ""; // Critères d'analyse 4
  private String critereAnalyse5 = ""; // Critères d'analyse 5
  private String cleClassement = ""; // Clé de classement alpha
  private String codeAPE = ""; // Cod APE
  private String numeroFax = ""; // Télécopie
  private String numeroTelex = ""; // Télex
  private String codeLangue = ""; // Code Langue
  private String observations = ""; // Observations
  private IdRepresentant idRepresentant = null; // Code représentant
  private IdRepresentant idRepresentant2 = null; // Code représentant 2
  private String typeCommissionRepresentant = ""; // Type commissionnement rep.
  private String zoneGeographique = ""; // Zone géographique
  private String modeExpedition = ""; // Mode d'expédition
  private IdTransporteur idTransporteur = null; // Code transporteur habituel
  private IdMagasin idMagasinRattachement = null; // Code magasin de rattachement
  private int montantFrancoPort = 0; // Montant franco de port
  private IdTypeFacturation idTypeFacturation = null; // Type de facturation
  private String codeDevise = ""; // Code devise
  private int codeClientFacture = 0; // Code client facturé
  private int suffixeLivClientFacture = 0; // Code client facturé suffixe
  private int codeClientPayeur = 0; // Code Client Payeur
  private String codeAffacturage = ""; // Code Affacturage
  private BigDecimal tauxEscompte = null; // Taux d'escompte
  private Integer numeroColonneTarif = null;
  private int numeroTarif2 = 0; // 2ème numéro de tarif
  private BigDecimal remise1 = null; // Remise 1
  private BigDecimal remise2 = null; // Remise 2
  private BigDecimal remise3 = null; // Remise 3
  private BigDecimal remisePied1 = null; // Remise Pied 1
  private BigDecimal remisePied2 = null; // Remise Pied 2
  private BigDecimal remisePied3 = null; // Remise Pied 3
  private String codeCNV = ""; // Code condition de vente
  private String periodiciteFacturation = ""; // Périodicité de facturation
  private String periodiciteReleve = ""; // Périodicité de relevé
  private String codeReleve = ""; // Code relevé
  private int regroupementBonSurFacture = 0; // Regroupement Bon sur Facture
  private IdModeReglement idReglement = null; // Code règlement
  private String codeEcheance = ""; // Code échéance
  private int collectifComptable = 0; // Collectif comptable
  private int compteAuxiliaire = 0; // Compte auxiliaire
  private String codeAffaireEnCours = ""; // Code affaire en cours
  private int plafondEncoursAutorise = 0; // Plafond encours autorisé
  private int surPlafondEncoursAutorise = 0; // Sur-Plafond encours autorisé
  private String typeClient = ""; // SUSPECT = S
  private EnumCodeAttentionClient topAttention = EnumCodeAttentionClient.ATTENTION_CLIENT_ACTIF;
  private String texteAttentionCommande = ""; // Texte d'attention pour cde
  private String noteClient = ""; // Note CLient
  private int dateDerniereVisite = 0; // Date de dernière visite
  private int dateDerniereVente = 0; // Date de dernière vente
  private String numeroSIREN = ""; // Numéro SIREN
  private String complementSIRET = ""; // Complément SIRET
  private String depassementEncours = ""; // DÉPASSEMENT ENCOURS
  // 3=LIVRAISON INTERDITE
  // 4=ATTENTE SI DEPASSEMENT
  private String codePaysCEE = ""; // Code pays CEE
  private int venteAssimileExport = 0; // Vente assimilé export
  private BigDecimal encoursCommande = null; // Encours / Commande
  private BigDecimal encoursExpedie = null; // Encours / Expédié
  private BigDecimal encoursFacture = null; // Encours / Facturé
  private int positionComptable = 0; // Position comptable
  
  private int zoneSystemMajCompta = 0; // Zone système pour maj CGM
  private int topMaj = 0; // Top MAJ
  private int nbrExemplairesBonHom = 0; // Nombre exemplaires Bon Hom.
  private int nbrExemplairesBonExp = 0; // Nombre exemplaires Bon Exp.
  private int nbrExemplairesBonFac = 0; // Nombre exemplaires facture
  private String numeroLibelleArticle = ""; // Numéro libellé article/Edt
  private String zoneRegroupementStats = ""; // Zone regroupement stats
  private String topLivraisonPartielle = ""; // Livraison partielle oui/non (non si différent de blanc)
  private String reliquatsAcceptes = ""; // Reliquats acceptés oui/non
  private EnumTypeEdition editionBonExpeditionNonChiffre = EnumTypeEdition.EDITION_CHIFFREE;
  private Character nonUtilisationDGCNV = null; // Non utilisation DGCNV
  private String codeGBAaRepercuter = ""; // Code GBA à répercuter / bon
  private String rfaCentrale = ""; // "C" rfa sur centrale
  private String nonEditionEtqColis = ""; // Non édition etq colis / BL
  private String editionFacBonExpEnchaine = ""; // Edit. Fact,Bon Exp enchainé
  private String exclusionCNVQuantitative = ""; // Exclusion CNV Quantitative
  private String pasEtqColisExpedition = ""; // Pas ETQ colis / Expédit°
  private String factureDifsurCmdSoldee = ""; // Fac.Dif./ sur Cde soldée
  private String nonSoumisTaxeAddtit = ""; // Non Soumis à taxe addit.
  private String codePEParticipationFraisExp = ""; // Code PE particip. frais exp.
  private String generationCNVFacPrixNet = ""; // Gén.CNV si FAC avec prix net
  private String codeAdherent = ""; // Code adhérent
  private String codeCondCommissionnement = ""; // Code Cond. commissionnement
  private int centrale1 = 0; // Centrale 1
  private int centrale2 = 0; // Centrale 2
  private int centrale3 = 0; // Centrale 3
  private String codeFirme = ""; // Code Firme
  private int clientTransitaire = 0; // Client Transitaire
  private String codeConditionRistourne = ""; // Code condition ristourne
  private String codeConditionPromo = ""; // Code condition Promo
  private String codeRemisePiedBon = ""; // Code remise pied de bon
  private int dateLimitePourSurPlafond = 0; // Date Limite pour Sur-plafond
  private String applicationFraisFixes = ""; // Application frais fixes (CC)
  private String applicationRemisePiedFac = ""; // Application remise pied fact
  private String codeRegroupementTransport = ""; // Code Regroupt.Transport
  private int ordreDansLaTournee = 0; // Ordre dans la Tournée
  private String canalDeVente = ""; // Canal de vente
  private String prenoms = ""; // Prénoms
  private String recenceCmd = ""; // (R)Récence commande
  private String frequenceCmd = ""; // (F)Fréquence commande
  private String montantCmd = ""; // (M)Montant commande
  private String codePaysTVAIntracom = ""; // Code pays TVA Intracommun.
  private String cleTvaIntracom = ""; // Clé infor.TVA Intracommun.
  private String classeClient1 = ""; // Classe de client 1
  private String classeClient2 = ""; // Classe de client 2
  private String classeClient3 = ""; // Classe de client 3
  private String cleClassement2 = ""; // Clé de classement 2 alpha
  private String clientSoumisDroitPret = ""; // Client soumis à droitde prêt
  private String editionFactures = ""; // Edition factures
  // 1=Edition regroupée factures
  // 2=Récap.Bons en fin de facture
  private String precommandesAcceptees = ""; // Précommandes acceptées O/N
  private String nombreExemplairesAvoirs = ""; // Nombre exemplaires avoirs
  private String typeVente = ""; // Type de vente paramètre VT
  private String trancheEffectif = ""; // Tranche Effectif
  private String codePEParticipationFraisExp2 = ""; // Code PE particip. frais exp2
  private String codePEParticipationFraisExp3 = ""; // Code PE particip. frais exp3
  private String nonEditionRemises = ""; // Non édition remises
  private String nonEditionPrixBase = ""; // Non édition prix base
  private String codeAPEv2 = ""; // Code APE v.2
  private int plafondDemande = 0; // Plafond demandé
  private int dateDemandePlafond = 0; // Date demande du plafond
  private String identifiantAssurance = ""; // Identifiant assurance
  private String codeAssurance = ""; // Code assurance
  private int dateValiditePlafond = 0; // Date validité du plafond
  private int caPrevisionnel = 0; // CA Previsionnel
  private int idBizyNova = 0; // Id. BIZY NOVA
  private int plafondMaxEnDeblocage = 0; // Plafond maxi en déblocage
  private BigDecimal fraisFacturePied1 = null; // frais fact. pied 1
  private BigDecimal fraisFacturePied2 = null; // frais fact. pied 2
  private BigDecimal fraisFacturePied3 = null; // frais fact. pied 3
  private String nonEditionNumerosSeries = ""; // non édition numéros séries
  private String refClientBonObligatoire = ""; // Réf.client/bon obligatoire
  private BlocNote blocNote = null;
  private Contact contactPrincipal = null;
  
  // Champs provenant de l'extention client
  private String typeZonePersonnalisee = ""; // TYPE DE ZP: "C"
  private String zonePersonnalisee01 = "";
  private String zonePersonnalisee02 = "";
  private String zonePersonnalisee03 = "";
  private String zonePersonnalisee04 = "";
  private String zonePersonnalisee05 = "";
  private String zonePersonnalisee06 = "";
  private String zonePersonnalisee07 = "";
  private String zonePersonnalisee08 = "";
  private String zonePersonnalisee09 = "";
  private String zonePersonnalisee10 = "";
  private String zonePersonnalisee11 = "";
  private String zonePersonnalisee12 = "";
  private String zonePersonnalisee13 = "";
  private String zonePersonnalisee14 = "";
  private String zonePersonnalisee15 = "";
  private String zonePersonnalisee16 = "";
  private String zonePersonnalisee17 = "";
  private String zonePersonnalisee18 = "";
  private Boolean prisParEstObligatoire = null;
  private Boolean refCourteCommandeObligatoire = null;
  private Boolean refLongueCommandeObligatoire = null;
  private Boolean vehiculeEstObligatoire = null;
  private Boolean chantierEstObligatoire = null;
  private String fluxCommandeEDI = "";
  private String fluxExpeditionEDI = "";
  private String fluxFactureEDI = "";
  private String adresseFactureClientPayeur = "";
  private Boolean reglementComptantObligatoire = null;
  private Boolean reglementChequeEstInterdit = null;
  
  /**
   * Constructeur avec l'idenfiant du client.
   */
  public Client(IdClient pIdClient) {
    super(pIdClient);
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdClient controlerId(Client pClient, boolean pVerifierExistance) {
    if (pClient == null) {
      throw new MessageErreurException("Le client est invalide.");
    }
    return IdClient.controlerId(pClient.getId(), true);
  }
  
  /**
   * Retourne si le client est interdit.
   */
  public boolean isClientInterdit() {
    return topAttention.equals(EnumCodeAttentionClient.ATTENTION_CLIENT_INTERDIT);
  }
  
  /**
   * Retourne si le client est désactivé.
   */
  public boolean isClientDesactive() {
    return topAttention.equals(EnumCodeAttentionClient.ATTENTION_CLIENT_DESACTIVE);
  }
  
  /**
   * Charger le contact principal de ce client
   */
  public void chargerContactPrincipal(IdSession pIdSession) {
    contactPrincipal = ManagerServiceCommun.chargerContactPrincipalClient(pIdSession, getId());
  }
  
  // -- Accesseurs
  
  /**
   * Etat de la fiche client en base
   */
  public EnumEtatFicheClient getEtatFiche() {
    return etatFiche;
  }
  
  public void setEtatFiche(EnumEtatFicheClient pEtatFiche) {
    etatFiche = pEtatFiche;
  }
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public Date getDateModification() {
    return dateModification;
  }
  
  public void setDateModification(Date dateModification) {
    this.dateModification = dateModification;
  }
  
  public Date getDateTraitement() {
    return dateTraitement;
  }
  
  public void setDateTraitement(Date dateTraitement) {
    this.dateTraitement = dateTraitement;
  }
  
  public String getCritereAnalyse1() {
    return critereAnalyse1;
  }
  
  public void setCritereAnalyse1(String critereAnalyse1) {
    this.critereAnalyse1 = critereAnalyse1;
  }
  
  public String getCritereAnalyse2() {
    return critereAnalyse2;
  }
  
  public void setCritereAnalyse2(String critereAnalyse2) {
    this.critereAnalyse2 = critereAnalyse2;
  }
  
  public String getCritereAnalyse3() {
    return critereAnalyse3;
  }
  
  public void setCritereAnalyse3(String critereAnalyse3) {
    this.critereAnalyse3 = critereAnalyse3;
  }
  
  public String getCritereAnalyse4() {
    return critereAnalyse4;
  }
  
  public void setCritereAnalyse4(String critereAnalyse4) {
    this.critereAnalyse4 = critereAnalyse4;
  }
  
  public String getCritereAnalyse5() {
    return critereAnalyse5;
  }
  
  public void setCritereAnalyse5(String critereAnalyse5) {
    this.critereAnalyse5 = critereAnalyse5;
  }
  
  public String getCleClassement() {
    return cleClassement;
  }
  
  public void setCleClassement(String cleClassement) {
    this.cleClassement = cleClassement;
  }
  
  public String getCodeAPE() {
    return codeAPE;
  }
  
  public void setCodeAPE(String codeAPE) {
    this.codeAPE = codeAPE;
  }
  
  public String getNumeroFax() {
    return numeroFax;
  }
  
  public void setNumeroFax(String numeroFax) {
    this.numeroFax = numeroFax;
  }
  
  public String getNumeroTelex() {
    return numeroTelex;
  }
  
  public void setNumeroTelex(String numeroTelex) {
    this.numeroTelex = numeroTelex;
  }
  
  public String getCodeLangue() {
    return codeLangue;
  }
  
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }
  
  public String getObservations() {
    return observations;
  }
  
  public void setObservations(String observations) {
    this.observations = observations;
  }
  
  public IdRepresentant getIdRepresentant() {
    return idRepresentant;
  }
  
  public void setIdRepresentant(IdRepresentant pIdRepresentant) {
    this.idRepresentant = pIdRepresentant;
  }
  
  public IdRepresentant getIdRepresentant2() {
    return idRepresentant2;
  }
  
  public void setIdRepresentant2(IdRepresentant pIdRepresentant) {
    this.idRepresentant2 = pIdRepresentant;
  }
  
  public String getTypeCommissionRepresentant() {
    return typeCommissionRepresentant;
  }
  
  public void setTypeCommissionRepresentant(String typeCommissionRepresentant) {
    this.typeCommissionRepresentant = typeCommissionRepresentant;
  }
  
  public String getZoneGeographique() {
    return zoneGeographique;
  }
  
  public void setZoneGeographique(String zoneGeographique) {
    this.zoneGeographique = zoneGeographique;
  }
  
  public String getModeExpedition() {
    return modeExpedition;
  }
  
  public void setModeExpedition(String modeExpedition) {
    this.modeExpedition = modeExpedition;
  }
  
  public IdTransporteur getIdTransporteur() {
    return idTransporteur;
  }
  
  public void setIdTransporteur(IdTransporteur pId) {
    this.idTransporteur = pId;
  }
  
  public IdMagasin getIdMagasinRattachement() {
    return idMagasinRattachement;
  }
  
  public void setIdMagasinRattachement(IdMagasin pMagasinRattachement) {
    this.idMagasinRattachement = pMagasinRattachement;
  }
  
  public int getMontantFrancoPort() {
    return montantFrancoPort;
  }
  
  public void setMontantFrancoPort(int montantFrancoPort) {
    this.montantFrancoPort = montantFrancoPort;
  }
  
  public IdTypeFacturation getIdTypeFacturation() {
    return idTypeFacturation;
  }
  
  public void setIdTypeFacturation(IdTypeFacturation pIdTypeFacturation) {
    idTypeFacturation = pIdTypeFacturation;
  }
  
  public String getCodeDevise() {
    return codeDevise;
  }
  
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  public int getCodeClientFacture() {
    return codeClientFacture;
  }
  
  public void setCodeClientFacture(int codeClientFacture) {
    this.codeClientFacture = codeClientFacture;
  }
  
  public int getSuffixeLivClientFacture() {
    return suffixeLivClientFacture;
  }
  
  public void setSuffixeLivClientFacture(int suffixeLivClientFacture) {
    this.suffixeLivClientFacture = suffixeLivClientFacture;
  }
  
  public int getCodeClientPayeur() {
    return codeClientPayeur;
  }
  
  public void setCodeClientPayeur(int codeClientPayeur) {
    this.codeClientPayeur = codeClientPayeur;
  }
  
  public String getCodeAffacturage() {
    return codeAffacturage;
  }
  
  public void setCodeAffacturage(String codeAffacturage) {
    this.codeAffacturage = codeAffacturage;
  }
  
  public BigDecimal getTauxEscompte() {
    return tauxEscompte;
  }
  
  public void setTauxEscompte(BigDecimal tauxEscompte) {
    this.tauxEscompte = tauxEscompte;
  }
  
  /**
   * Retouner le numéro de la colonne tarif du client.
   * La colonne tarif correspondant à ce numéro est utilisée par défaut pour calculer les prix de ventes du client.
   * @return Numéro de colonne tarif.
   */
  public Integer getNumeroColonneTarif() {
    return numeroColonneTarif;
  }
  
  /**
   * Modifier le numéro de la colonne tarif du client.
   * @param pNumeroColonneTarif Numéro de colonne tarif.
   */
  public void setNumeroTarif(Integer pNumeroColonneTarif) {
    numeroColonneTarif = pNumeroColonneTarif;
  }
  
  public int getNumeroTarif2() {
    return numeroTarif2;
  }
  
  public void setNumeroTarif2(int numeroTarif2) {
    this.numeroTarif2 = numeroTarif2;
  }
  
  public BigDecimal getRemise1() {
    return remise1;
  }
  
  public void setRemise1(BigDecimal remise1) {
    this.remise1 = remise1;
  }
  
  public BigDecimal getRemise2() {
    return remise2;
  }
  
  public void setRemise2(BigDecimal remise2) {
    this.remise2 = remise2;
  }
  
  public BigDecimal getRemise3() {
    return remise3;
  }
  
  public void setRemise3(BigDecimal remise3) {
    this.remise3 = remise3;
  }
  
  public BigDecimal getRemisePied1() {
    return remisePied1;
  }
  
  public void setRemisePied1(BigDecimal remisePied1) {
    this.remisePied1 = remisePied1;
  }
  
  public BigDecimal getRemisePied2() {
    return remisePied2;
  }
  
  public void setRemisePied2(BigDecimal remisePied2) {
    this.remisePied2 = remisePied2;
  }
  
  public BigDecimal getRemisePied3() {
    return remisePied3;
  }
  
  public void setRemisePied3(BigDecimal remisePied3) {
    this.remisePied3 = remisePied3;
  }
  
  public String getCodeCNV() {
    return codeCNV;
  }
  
  public void setCodeCNV(String codeCNV) {
    this.codeCNV = codeCNV;
  }
  
  public String getPeriodiciteFacturation() {
    return periodiciteFacturation;
  }
  
  public void setPeriodiciteFacturation(String periodiciteFacturation) {
    this.periodiciteFacturation = periodiciteFacturation;
  }
  
  public String getPeriodiciteReleve() {
    return periodiciteReleve;
  }
  
  public void setPeriodiciteReleve(String periodiciteReleve) {
    this.periodiciteReleve = periodiciteReleve;
  }
  
  public String getCodeReleve() {
    return codeReleve;
  }
  
  public void setCodeReleve(String codeReleve) {
    this.codeReleve = codeReleve;
  }
  
  public int getRegroupementBonSurFacture() {
    return regroupementBonSurFacture;
  }
  
  public void setRegroupementBonSurFacture(int regroupementBonSurFacture) {
    this.regroupementBonSurFacture = regroupementBonSurFacture;
  }
  
  public IdModeReglement getIdReglement() {
    return idReglement;
  }
  
  public void setIdReglement(IdModeReglement pId) {
    this.idReglement = pId;
  }
  
  public String getCodeEcheance() {
    return codeEcheance;
  }
  
  public void setCodeEcheance(String codeEcheance) {
    this.codeEcheance = codeEcheance;
  }
  
  public int getCollectifComptable() {
    return collectifComptable;
  }
  
  public void setCollectifComptable(int collectifComptable) {
    this.collectifComptable = collectifComptable;
  }
  
  public int getCompteAuxiliaire() {
    return compteAuxiliaire;
  }
  
  public void setCompteAuxiliaire(int compteAuxiliaire) {
    this.compteAuxiliaire = compteAuxiliaire;
  }
  
  public String getCodeAffaireEnCours() {
    return codeAffaireEnCours;
  }
  
  public void setCodeAffaireEnCours(String codeAffaireEnCours) {
    this.codeAffaireEnCours = codeAffaireEnCours;
  }
  
  public int getPlafondEncoursAutorise() {
    return plafondEncoursAutorise;
  }
  
  public void setPlafondEncoursAutorise(int plafondEncoursAutorise) {
    this.plafondEncoursAutorise = plafondEncoursAutorise;
  }
  
  public int getSurPlafondEncoursAutorise() {
    return surPlafondEncoursAutorise;
  }
  
  public void setSurPlafondEncoursAutorise(int surPlafondEncoursAutorise) {
    this.surPlafondEncoursAutorise = surPlafondEncoursAutorise;
  }
  
  public EnumCodeAttentionClient getTopAttention() {
    return topAttention;
  }
  
  public void setTopAttention(EnumCodeAttentionClient pTopAttention) {
    topAttention = pTopAttention;
  }
  
  public String getTexteAttentionCommande() {
    return texteAttentionCommande;
  }
  
  public void setTexteAttentionCommande(String texteAttentionCommande) {
    this.texteAttentionCommande = texteAttentionCommande;
  }
  
  public String getNoteClient() {
    return noteClient;
  }
  
  public void setNoteClient(String noteClient) {
    this.noteClient = noteClient;
  }
  
  public int getDateDerniereVisite() {
    return dateDerniereVisite;
  }
  
  public void setDateDerniereVisite(int dateDerniereVisite) {
    this.dateDerniereVisite = dateDerniereVisite;
  }
  
  public int getDateDerniereVente() {
    return dateDerniereVente;
  }
  
  public void setDateDerniereVente(int dateDerniereVente) {
    this.dateDerniereVente = dateDerniereVente;
  }
  
  public String getNumeroSIREN() {
    return numeroSIREN;
  }
  
  public void setNumeroSIREN(String numeroSIREN) {
    this.numeroSIREN = numeroSIREN;
  }
  
  public String getComplementSIRET() {
    return complementSIRET;
  }
  
  public void setComplementSIRET(String complementSIRET) {
    this.complementSIRET = complementSIRET;
  }
  
  public String getDepassementEncours() {
    return depassementEncours;
  }
  
  public void setDepassementEncours(String depassementEncours) {
    this.depassementEncours = depassementEncours;
  }
  
  public String getCodePaysCEE() {
    return codePaysCEE;
  }
  
  public void setCodePaysCEE(String codePaysCEE) {
    this.codePaysCEE = codePaysCEE;
  }
  
  public int getVenteAssimileExport() {
    return venteAssimileExport;
  }
  
  public void setVenteAssimileExport(int venteAssimileExport) {
    this.venteAssimileExport = venteAssimileExport;
  }
  
  public BigDecimal getEncoursCommande() {
    return encoursCommande;
  }
  
  public void setEncoursCommande(BigDecimal encoursCommande) {
    this.encoursCommande = encoursCommande;
  }
  
  public BigDecimal getEncoursExpedie() {
    return encoursExpedie;
  }
  
  public void setEncoursExpedie(BigDecimal encoursExpedie) {
    this.encoursExpedie = encoursExpedie;
  }
  
  public BigDecimal getEncoursFacture() {
    return encoursFacture;
  }
  
  public void setEncoursFacture(BigDecimal encoursFacture) {
    this.encoursFacture = encoursFacture;
  }
  
  public int getPositionComptable() {
    return positionComptable;
  }
  
  public void setPositionComptable(int positionComptable) {
    this.positionComptable = positionComptable;
  }
  
  public String getCleClassement2() {
    return cleClassement2;
  }
  
  public void setCleClassement2(String cleClassement2) {
    this.cleClassement2 = cleClassement2;
  }
  
  public int getZoneSystemMajCompta() {
    return zoneSystemMajCompta;
  }
  
  public void setZoneSystemMajCompta(int zoneSystemMajCompta) {
    this.zoneSystemMajCompta = zoneSystemMajCompta;
  }
  
  public int getTopMaj() {
    return topMaj;
  }
  
  public void setTopMaj(int topMaj) {
    this.topMaj = topMaj;
  }
  
  public int getNbrExemplairesBonHom() {
    return nbrExemplairesBonHom;
  }
  
  public void setNbrExemplairesBonHom(int nbrExemplairesBonHom) {
    this.nbrExemplairesBonHom = nbrExemplairesBonHom;
  }
  
  public int getNbrExemplairesBonExp() {
    return nbrExemplairesBonExp;
  }
  
  public void setNbrExemplairesBonExp(int nbrExemplairesBonExp) {
    this.nbrExemplairesBonExp = nbrExemplairesBonExp;
  }
  
  public int getNbrExemplairesBonFac() {
    return nbrExemplairesBonFac;
  }
  
  public void setNbrExemplairesBonFac(int nbrExemplairesBonFac) {
    this.nbrExemplairesBonFac = nbrExemplairesBonFac;
  }
  
  public String getNumeroLibelleArticle() {
    return numeroLibelleArticle;
  }
  
  public void setNumeroLibelleArticle(String numeroLibelleArticle) {
    this.numeroLibelleArticle = numeroLibelleArticle;
  }
  
  public String getZoneRegroupementStats() {
    return zoneRegroupementStats;
  }
  
  public void setZoneRegroupementStats(String zoneRegroupementStats) {
    this.zoneRegroupementStats = zoneRegroupementStats;
  }
  
  public String getTopLivraisonPartielle() {
    return topLivraisonPartielle;
  }
  
  public void setTopLivraisonPartielle(String topLivraisonPartielle) {
    this.topLivraisonPartielle = topLivraisonPartielle;
  }
  
  public String getReliquatsAcceptes() {
    return reliquatsAcceptes;
  }
  
  public void setReliquatsAcceptes(String reliquatsAcceptes) {
    this.reliquatsAcceptes = reliquatsAcceptes;
  }
  
  public EnumTypeEdition getEditionChiffre() {
    return editionBonExpeditionNonChiffre;
  }
  
  public void setEditionChiffre(EnumTypeEdition pEditionChiffre) {
    editionBonExpeditionNonChiffre = pEditionChiffre;
  }
  
  public Character getNonUtilisationDGCNV() {
    return nonUtilisationDGCNV;
  }
  
  public void setNonUtilisationDGCNV(Character nonUtilisationDGCNV) {
    this.nonUtilisationDGCNV = nonUtilisationDGCNV;
  }
  
  public String getCodeGBAaRepercuter() {
    return codeGBAaRepercuter;
  }
  
  public void setCodeGBAaRepercuter(String codeGBAaRepercuter) {
    this.codeGBAaRepercuter = codeGBAaRepercuter;
  }
  
  public String getRfaCentrale() {
    return rfaCentrale;
  }
  
  public void setRfaCentrale(String rfaCentrale) {
    this.rfaCentrale = rfaCentrale;
  }
  
  public String getNonEditionEtqColis() {
    return nonEditionEtqColis;
  }
  
  public void setNonEditionEtqColis(String nonEditionEtqColis) {
    this.nonEditionEtqColis = nonEditionEtqColis;
  }
  
  public String getEditionFacBonExpEnchaine() {
    return editionFacBonExpEnchaine;
  }
  
  public void setEditionFacBonExpEnchaine(String editionFacBonExpEnchaine) {
    this.editionFacBonExpEnchaine = editionFacBonExpEnchaine;
  }
  
  public String getExclusionCNVQuantitative() {
    return exclusionCNVQuantitative;
  }
  
  public void setExclusionCNVQuantitative(String exclusionCNVQuantitative) {
    this.exclusionCNVQuantitative = exclusionCNVQuantitative;
  }
  
  public String getPasEtqColisExpedition() {
    return pasEtqColisExpedition;
  }
  
  public void setPasEtqColisExpedition(String pasEtqColisExpedition) {
    this.pasEtqColisExpedition = pasEtqColisExpedition;
  }
  
  public String getFactureDifsurCmdSoldee() {
    return factureDifsurCmdSoldee;
  }
  
  public void setFactureDifsurCmdSoldee(String factureDifsurCmdSoldee) {
    this.factureDifsurCmdSoldee = factureDifsurCmdSoldee;
  }
  
  public String getNonSoumisTaxeAddtit() {
    return nonSoumisTaxeAddtit;
  }
  
  public void setNonSoumisTaxeAddtit(String nonSoumisTaxeAddtit) {
    this.nonSoumisTaxeAddtit = nonSoumisTaxeAddtit;
  }
  
  public String getCodePEParticipationFraisExp() {
    return codePEParticipationFraisExp;
  }
  
  public void setCodePEParticipationFraisExp(String codePEParticipationFraisExp) {
    this.codePEParticipationFraisExp = codePEParticipationFraisExp;
  }
  
  public String getGenerationCNVFacPrixNet() {
    return generationCNVFacPrixNet;
  }
  
  public void setGenerationCNVFacPrixNet(String generationCNVFacPrixNet) {
    this.generationCNVFacPrixNet = generationCNVFacPrixNet;
  }
  
  public String getCodeAdherent() {
    return codeAdherent;
  }
  
  public void setCodeAdherent(String codeAdherent) {
    this.codeAdherent = codeAdherent;
  }
  
  public String getCodeCondCommissionnement() {
    return codeCondCommissionnement;
  }
  
  public void setCodeCondCommissionnement(String codeCondCommissionnement) {
    this.codeCondCommissionnement = codeCondCommissionnement;
  }
  
  public int getCentrale1() {
    return centrale1;
  }
  
  public void setCentrale1(int centrale1) {
    this.centrale1 = centrale1;
  }
  
  public int getCentrale2() {
    return centrale2;
  }
  
  public void setCentrale2(int centrale2) {
    this.centrale2 = centrale2;
  }
  
  public int getCentrale3() {
    return centrale3;
  }
  
  public void setCentrale3(int centrale3) {
    this.centrale3 = centrale3;
  }
  
  public String getCodeFirme() {
    return codeFirme;
  }
  
  public void setCodeFirme(String codeFirme) {
    this.codeFirme = codeFirme;
  }
  
  public int getClientTransitaire() {
    return clientTransitaire;
  }
  
  public void setClientTransitaire(int clientTransitaire) {
    this.clientTransitaire = clientTransitaire;
  }
  
  public String getCodeConditionRistourne() {
    return codeConditionRistourne;
  }
  
  public void setCodeConditionRistourne(String codeConditionRistourne) {
    this.codeConditionRistourne = codeConditionRistourne;
  }
  
  public String getCodeConditionPromo() {
    return codeConditionPromo;
  }
  
  public void setCodeConditionPromo(String codeConditionPromo) {
    this.codeConditionPromo = codeConditionPromo;
  }
  
  public String getCodeRemisePiedBon() {
    return codeRemisePiedBon;
  }
  
  public void setCodeRemisePiedBon(String codeRemisePiedBon) {
    this.codeRemisePiedBon = codeRemisePiedBon;
  }
  
  public int getDateLimitePourSurPlafond() {
    return dateLimitePourSurPlafond;
  }
  
  public void setDateLimitePourSurPlafond(int dateLimitePourSurPlafond) {
    this.dateLimitePourSurPlafond = dateLimitePourSurPlafond;
  }
  
  public String getApplicationFraisFixes() {
    return applicationFraisFixes;
  }
  
  public void setApplicationFraisFixes(String applicationFraisFixes) {
    this.applicationFraisFixes = applicationFraisFixes;
  }
  
  public String getApplicationRemisePiedFac() {
    return applicationRemisePiedFac;
  }
  
  public void setApplicationRemisePiedFac(String applicationRemisePiedFac) {
    this.applicationRemisePiedFac = applicationRemisePiedFac;
  }
  
  public String getCodeRegroupementTransport() {
    return codeRegroupementTransport;
  }
  
  public void setCodeRegroupementTransport(String codeRegroupementTransport) {
    this.codeRegroupementTransport = codeRegroupementTransport;
  }
  
  public int getOrdreDansLaTournee() {
    return ordreDansLaTournee;
  }
  
  public void setOrdreDansLaTournee(int ordreDansLaTournee) {
    this.ordreDansLaTournee = ordreDansLaTournee;
  }
  
  public String getCanalDeVente() {
    return canalDeVente;
  }
  
  public void setCanalDeVente(String canalDeVente) {
    this.canalDeVente = canalDeVente;
  }
  
  public String getPrenoms() {
    return prenoms;
  }
  
  public void setPrenoms(String prenoms) {
    this.prenoms = prenoms;
  }
  
  public String getRecenceCmd() {
    return recenceCmd;
  }
  
  public void setRecenceCmd(String recenceCmd) {
    this.recenceCmd = recenceCmd;
  }
  
  public String getFrequenceCmd() {
    return frequenceCmd;
  }
  
  public void setFrequenceCmd(String frequenceCmd) {
    this.frequenceCmd = frequenceCmd;
  }
  
  public String getMontantCmd() {
    return montantCmd;
  }
  
  public void setMontantCmd(String montantCmd) {
    this.montantCmd = montantCmd;
  }
  
  public String getCodePaysTVAIntracom() {
    return codePaysTVAIntracom;
  }
  
  public void setCodePaysTVAIntracom(String codePaysTVAIntracom) {
    this.codePaysTVAIntracom = codePaysTVAIntracom;
  }
  
  public String getCleTvaIntracom() {
    return cleTvaIntracom;
  }
  
  public void setCleTvaIntracom(String cleTvaIntracom) {
    this.cleTvaIntracom = cleTvaIntracom;
  }
  
  public String getClasseClient1() {
    return classeClient1;
  }
  
  public void setClasseClient1(String classeClient1) {
    this.classeClient1 = classeClient1;
  }
  
  public String getClasseClient2() {
    return classeClient2;
  }
  
  public void setClasseClient2(String classeClient2) {
    this.classeClient2 = classeClient2;
  }
  
  public String getClasseClient3() {
    return classeClient3;
  }
  
  public void setClasseClient3(String classeClient3) {
    this.classeClient3 = classeClient3;
  }
  
  public String getClientSoumisDroitPret() {
    return clientSoumisDroitPret;
  }
  
  public void setClientSoumisDroitPret(String clientSoumisDroitPret) {
    this.clientSoumisDroitPret = clientSoumisDroitPret;
  }
  
  public String getEditionFactures() {
    return editionFactures;
  }
  
  public void setEditionFactures(String editionFactures) {
    this.editionFactures = editionFactures;
  }
  
  public String getPrecommandesAcceptees() {
    return precommandesAcceptees;
  }
  
  public void setPrecommandesAcceptees(String precommandesAcceptees) {
    this.precommandesAcceptees = precommandesAcceptees;
  }
  
  public String getNombreExemplairesAvoirs() {
    return nombreExemplairesAvoirs;
  }
  
  public void setNombreExemplairesAvoirs(String nombreExemplairesAvoirs) {
    this.nombreExemplairesAvoirs = nombreExemplairesAvoirs;
  }
  
  public String getTypeVente() {
    return typeVente;
  }
  
  public void setTypeVente(String typeVente) {
    this.typeVente = typeVente;
  }
  
  public String getTrancheEffectif() {
    return trancheEffectif;
  }
  
  public void setTrancheEffectif(String trancheEffectif) {
    this.trancheEffectif = trancheEffectif;
  }
  
  public String getCodePEParticipationFraisExp2() {
    return codePEParticipationFraisExp2;
  }
  
  public void setCodePEParticipationFraisExp2(String codePEParticipationFraisExp2) {
    this.codePEParticipationFraisExp2 = codePEParticipationFraisExp2;
  }
  
  public String getCodePEParticipationFraisExp3() {
    return codePEParticipationFraisExp3;
  }
  
  public void setCodePEParticipationFraisExp3(String codePEParticipationFraisExp3) {
    this.codePEParticipationFraisExp3 = codePEParticipationFraisExp3;
  }
  
  public String getNonEditionRemises() {
    return nonEditionRemises;
  }
  
  public void setNonEditionRemises(String nonEditionRemises) {
    this.nonEditionRemises = nonEditionRemises;
  }
  
  public String getNonEditionPrixBase() {
    return nonEditionPrixBase;
  }
  
  public void setNonEditionPrixBase(String nonEditionPrixBase) {
    this.nonEditionPrixBase = nonEditionPrixBase;
  }
  
  public String getCodeAPEv2() {
    return codeAPEv2;
  }
  
  public void setCodeAPEv2(String codeAPEv2) {
    this.codeAPEv2 = codeAPEv2;
  }
  
  public int getPlafondDemande() {
    return plafondDemande;
  }
  
  public void setPlafondDemande(int plafondDemande) {
    this.plafondDemande = plafondDemande;
  }
  
  public int getDateDemandePlafond() {
    return dateDemandePlafond;
  }
  
  public void setDateDemandePlafond(int dateDemandePlafond) {
    this.dateDemandePlafond = dateDemandePlafond;
  }
  
  public String getIdentifiantAssurance() {
    return identifiantAssurance;
  }
  
  public void setIdentifiantAssurance(String identifiantAssurance) {
    this.identifiantAssurance = identifiantAssurance;
  }
  
  public String getCodeAssurance() {
    return codeAssurance;
  }
  
  public void setCodeAssurance(String codeAssurance) {
    this.codeAssurance = codeAssurance;
  }
  
  public int getDateValiditePlafond() {
    return dateValiditePlafond;
  }
  
  public void setDateValiditePlafond(int dateValiditePlafond) {
    this.dateValiditePlafond = dateValiditePlafond;
  }
  
  public int getCaPrevisionnel() {
    return caPrevisionnel;
  }
  
  public void setCaPrevisionnel(int caPrevisionnel) {
    this.caPrevisionnel = caPrevisionnel;
  }
  
  public int getIdBizyNova() {
    return idBizyNova;
  }
  
  public void setIdBizyNova(int idBizyNova) {
    this.idBizyNova = idBizyNova;
  }
  
  public int getPlafondMaxEnDeblocage() {
    return plafondMaxEnDeblocage;
  }
  
  public void setPlafondMaxEnDeblocage(int plafondMaxEnDeblocage) {
    this.plafondMaxEnDeblocage = plafondMaxEnDeblocage;
  }
  
  public BigDecimal getFraisFacturePied1() {
    return fraisFacturePied1;
  }
  
  public void setFraisFacturePied1(BigDecimal fraisFacturePied1) {
    this.fraisFacturePied1 = fraisFacturePied1;
  }
  
  public BigDecimal getFraisFacturePied2() {
    return fraisFacturePied2;
  }
  
  public void setFraisFacturePied2(BigDecimal fraisFacturePied2) {
    this.fraisFacturePied2 = fraisFacturePied2;
  }
  
  public BigDecimal getFraisFacturePied3() {
    return fraisFacturePied3;
  }
  
  public void setFraisFacturePied3(BigDecimal fraisFacturePied3) {
    this.fraisFacturePied3 = fraisFacturePied3;
  }
  
  public String getNonEditionNumerosSeries() {
    return nonEditionNumerosSeries;
  }
  
  public void setNonEditionNumerosSeries(String nonEditionNumerosSeries) {
    this.nonEditionNumerosSeries = nonEditionNumerosSeries;
  }
  
  public String getTypeClient() {
    return typeClient;
  }
  
  public void setTypeClient(String pTypeClient) {
    typeClient = pTypeClient;
  }
  
  @Override
  public EnumTypeImageClient getTypeImageClient() {
    return super.getTypeImageClient();
  }
  
  @Override
  public void setTypeImageClient(EnumTypeImageClient pTypeImageClient) {
    super.setTypeImageClient(pTypeImageClient);
  }
  
  public BlocNote getBlocNote() {
    return blocNote;
  }
  
  public void setBlocNote(BlocNote pBlocNote) {
    blocNote = pBlocNote;
  }
  
  public String getTypeZonePersonnalisee() {
    return typeZonePersonnalisee;
  }
  
  public void setTypeZonePersonnalisee(String typeZonePersonnalisee) {
    this.typeZonePersonnalisee = typeZonePersonnalisee;
  }
  
  public String getZonePersonnalisee01() {
    return zonePersonnalisee01;
  }
  
  public void setZonePersonnalisee01(String zonePersonnalisee01) {
    this.zonePersonnalisee01 = zonePersonnalisee01;
  }
  
  public String getZonePersonnalisee02() {
    return zonePersonnalisee02;
  }
  
  public void setZonePersonnalisee02(String zonePersonnalisee02) {
    this.zonePersonnalisee02 = zonePersonnalisee02;
  }
  
  public String getZonePersonnalisee03() {
    return zonePersonnalisee03;
  }
  
  public void setZonePersonnalisee03(String zonePersonnalisee03) {
    this.zonePersonnalisee03 = zonePersonnalisee03;
  }
  
  public String getZonePersonnalisee04() {
    return zonePersonnalisee04;
  }
  
  public void setZonePersonnalisee04(String zonePersonnalisee04) {
    this.zonePersonnalisee04 = zonePersonnalisee04;
  }
  
  public String getZonePersonnalisee05() {
    return zonePersonnalisee05;
  }
  
  public void setZonePersonnalisee05(String zonePersonnalisee05) {
    this.zonePersonnalisee05 = zonePersonnalisee05;
  }
  
  public String getZonePersonnalisee06() {
    return zonePersonnalisee06;
  }
  
  public void setZonePersonnalisee06(String zonePersonnalisee06) {
    this.zonePersonnalisee06 = zonePersonnalisee06;
  }
  
  public String getZonePersonnalisee07() {
    return zonePersonnalisee07;
  }
  
  public void setZonePersonnalisee07(String zonePersonnalisee07) {
    this.zonePersonnalisee07 = zonePersonnalisee07;
  }
  
  public String getZonePersonnalisee08() {
    return zonePersonnalisee08;
  }
  
  public void setZonePersonnalisee08(String zonePersonnalisee08) {
    this.zonePersonnalisee08 = zonePersonnalisee08;
  }
  
  public String getZonePersonnalisee09() {
    return zonePersonnalisee09;
  }
  
  public void setZonePersonnalisee09(String zonePersonnalisee09) {
    this.zonePersonnalisee09 = zonePersonnalisee09;
  }
  
  public String getZonePersonnalisee10() {
    return zonePersonnalisee10;
  }
  
  public void setZonePersonnalisee10(String zonePersonnalisee10) {
    this.zonePersonnalisee10 = zonePersonnalisee10;
  }
  
  public String getZonePersonnalisee11() {
    return zonePersonnalisee11;
  }
  
  public void setZonePersonnalisee11(String zonePersonnalisee11) {
    this.zonePersonnalisee11 = zonePersonnalisee11;
  }
  
  public String getZonePersonnalisee12() {
    return zonePersonnalisee12;
  }
  
  public void setZonePersonnalisee12(String zonePersonnalisee12) {
    this.zonePersonnalisee12 = zonePersonnalisee12;
  }
  
  public String getZonePersonnalisee13() {
    return zonePersonnalisee13;
  }
  
  public void setZonePersonnalisee13(String zonePersonnalisee13) {
    this.zonePersonnalisee13 = zonePersonnalisee13;
  }
  
  public String getZonePersonnalisee14() {
    return zonePersonnalisee14;
  }
  
  public void setZonePersonnalisee14(String zonePersonnalisee14) {
    this.zonePersonnalisee14 = zonePersonnalisee14;
  }
  
  public String getZonePersonnalisee15() {
    return zonePersonnalisee15;
  }
  
  public void setZonePersonnalisee15(String zonePersonnalisee15) {
    this.zonePersonnalisee15 = zonePersonnalisee15;
  }
  
  public String getZonePersonnalisee16() {
    return zonePersonnalisee16;
  }
  
  public void setZonePersonnalisee16(String zonePersonnalisee16) {
    this.zonePersonnalisee16 = zonePersonnalisee16;
  }
  
  public String getZonePersonnalisee17() {
    return zonePersonnalisee17;
  }
  
  public void setZonePersonnalisee17(String zonePersonnalisee17) {
    this.zonePersonnalisee17 = zonePersonnalisee17;
  }
  
  public String getZonePersonnalisee18() {
    return zonePersonnalisee18;
  }
  
  public void setZonePersonnalisee18(String zonePersonnalisee18) {
    this.zonePersonnalisee18 = zonePersonnalisee18;
  }
  
  public String getFluxCommandeEDI() {
    return fluxCommandeEDI;
  }
  
  public void setFluxCommandeEDI(String fluxCommandeEDI) {
    this.fluxCommandeEDI = fluxCommandeEDI;
  }
  
  public String getFluxExpeditionEDI() {
    return fluxExpeditionEDI;
  }
  
  public void setFluxExpeditionEDI(String fluxExpeditionEDI) {
    this.fluxExpeditionEDI = fluxExpeditionEDI;
  }
  
  public String getFluxFactureEDI() {
    return fluxFactureEDI;
  }
  
  public void setFluxFactureEDI(String fluxFactureEDI) {
    this.fluxFactureEDI = fluxFactureEDI;
  }
  
  public String getAdresseFactureClientPayeur() {
    return adresseFactureClientPayeur;
  }
  
  public void setAdresseFactureClientPayeur(String adresseFactureClientPayeur) {
    this.adresseFactureClientPayeur = adresseFactureClientPayeur;
  }
  
  public Boolean isPrisParEstObligatoire() {
    if (prisParEstObligatoire == null) {
      return false;
    }
    return prisParEstObligatoire;
  }
  
  public void setPrisParEstObligatoire(boolean prisParEstObligatoire) {
    this.prisParEstObligatoire = prisParEstObligatoire;
  }
  
  public Boolean isRefCourteCommandeEstObligatoire() {
    if (refCourteCommandeObligatoire == null) {
      return false;
    }
    return refCourteCommandeObligatoire;
  }
  
  public void setRefCourteCommandeObligatoire(boolean pRefCourteCommandeObligatoire) {
    this.refCourteCommandeObligatoire = pRefCourteCommandeObligatoire;
  }
  
  public Boolean isRefLongueCommandeEstObligatoire() {
    if (refLongueCommandeObligatoire == null) {
      return false;
    }
    return refLongueCommandeObligatoire;
  }
  
  public void setRefLongueCommandeObligatoire(boolean pRefLongueCommandeObligatoire) {
    this.refLongueCommandeObligatoire = pRefLongueCommandeObligatoire;
  }
  
  public Boolean isVehiculeEstObligatoire() {
    if (vehiculeEstObligatoire == null) {
      return false;
    }
    return vehiculeEstObligatoire;
  }
  
  public void setVehiculeEstObligatoire(boolean vehiculeEstObligatoire) {
    this.vehiculeEstObligatoire = vehiculeEstObligatoire;
  }
  
  public Boolean isChantierEstObligatoire() {
    if (chantierEstObligatoire == null) {
      return false;
    }
    return chantierEstObligatoire;
  }
  
  public void setChantierEstObligatoire(boolean chantierEstObligatoire) {
    this.chantierEstObligatoire = chantierEstObligatoire;
  }
  
  /**
   * Indique si le règlement comptant est obligatoire pour ce client.
   */
  public Boolean isReglementComptantObligatoire() {
    if (reglementComptantObligatoire == null) {
      return false;
    }
    return reglementComptantObligatoire;
  }
  
  /**
   * Modifier le client pour indiquer si le règlement comptant est obligatoire.
   */
  public void setReglementComptantEstObligatoire(boolean pReglementComptantObligatoire) {
    reglementComptantObligatoire = pReglementComptantObligatoire;
  }
  
  public Boolean isReglementChequeEstInterdit() {
    if (reglementChequeEstInterdit == null) {
      return false;
    }
    return reglementChequeEstInterdit;
  }
  
  public void setReglementChequeEstInterdit(Boolean reglementChequeEstInterdit) {
    this.reglementChequeEstInterdit = reglementChequeEstInterdit;
  }
  
  /**
   * Contact principal du client.
   * Il y a toujours un contact principal pour un client comptant. Si le contact principal n'existe pas, il est créé à vollée.
   */
  public Contact getContactPrincipal() {
    if (contactPrincipal == null && isClientComptant()) {
      contactPrincipal = new Contact(IdContact.getInstance(IdContact.CREATION));
    }
    return contactPrincipal;
  }
  
  /**
   * Modifier le contact principal du client.
   */
  public void setContactPrincipal(Contact contactPrincipal) {
    this.contactPrincipal = contactPrincipal;
  }
  
  /**
   * Contrôle que l'adresse contient les informations minimums.
   * La méthode retourne un une liste d'erreurs sous forme d'EnumErreurAdresse. Cette liste peut être traitée par la boite de dialogue
   * ModeleErreurAdresse.
   */
  public void controlerAdresse(IdSession pIdSession, List<EnumErreurAdresse> pListeErreur, boolean pAvecControleTelephoneEtMail) {
    
    pListeErreur.clear();
    
    if (getAdresse() == null) {
      pListeErreur.add(EnumErreurAdresse.ERREUR_NON_GEREE);
    }
    else {
      // Tester le nom ou la raison sociale
      if (isParticulier()) {
        if (getContactPrincipal() == null || getContactPrincipal().getNom() == null || getContactPrincipal().getNom().trim().isEmpty()) {
          pListeErreur.add(EnumErreurAdresse.ERREUR_NOM);
        }
      }
      else if (isProfessionel()) {
        if (getAdresse() == null || getAdresse().getNom() == null || getAdresse().getNom().trim().isEmpty()) {
          pListeErreur.add(EnumErreurAdresse.ERREUR_NOM);
        }
      }
      
      // Tester le prénom
      if (isParticulier() && getContactPrincipal().getPrenom().trim().isEmpty()) {
        pListeErreur.add(EnumErreurAdresse.ERREUR_PRENOM);
      }
      
      // Tester l'adresse
      if (getAdresse().getRue().trim().isEmpty() && getAdresse().getLocalisation().trim().isEmpty()) {
        pListeErreur.add(EnumErreurAdresse.ERREUR_RUE);
      }
      
      ListeCommune listeCommune = null;
      // Tester le code postal
      if (getAdresse().getCodePostal() <= 0) {
        pListeErreur.add(EnumErreurAdresse.ERREUR_CODE_POSTAL);
      }
      else {
        CritereCommune critereCommune = new CritereCommune();
        critereCommune.setCodePostal(Integer.parseInt(getAdresse().getCodePostalFormate()));
        listeCommune = ListeCommune.charger(pIdSession, critereCommune);
        
      }
      
      // Tester la ville
      if (getAdresse().getVille().trim().isEmpty()) {
        pListeErreur.add(EnumErreurAdresse.ERREUR_VILLE_MANQUANTE);
      }
      else if (listeCommune != null && listeCommune.size() > 0
          && (listeCommune.getListeNomCommune() == null || listeCommune.getListeNomCommune().size() == 0)) {
        pListeErreur.add(EnumErreurAdresse.ERREUR_VILLE_ERRONEE);
      }
      
      if (isClientComptant() && pAvecControleTelephoneEtMail) {
        /* Désactivation du contrôle sur les mails, demande d'Allot SNC-6521. On réactivera plus tard avec un paramétrage, si demandé.
        if (contactPrincipal == null || contactPrincipal.getEmail() == null || contactPrincipal.getEmail().trim().isEmpty()) {
          pListeErreur.add(EnumErreurAdresse.ERREUR_MAIL);
        }
        */
        if (contactPrincipal == null || getNumeroTelephone() == null || getNumeroTelephone().trim().isEmpty()) {
          pListeErreur.add(EnumErreurAdresse.ERREUR_TELEPHONE);
        }
      }
    }
  }
  
}
