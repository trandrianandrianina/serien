/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.sipe;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Statut applicables aux fichiers Sipe.
 */
public enum EnumStatutSipe {
  ATTENTE(0, "En attente"),
  IMPORTE(1, "Importé"),
  SUPPRIME(2, "Supprimé");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumStatutSipe(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumStatutSipe valueOfByCode(Integer pCode) {
    for (EnumStatutSipe value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le statut du fichier Sipe est invalide : " + pCode);
  }
}
