/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.io.Serializable;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.transporteur.IdTransporteur;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;

public class Transport implements Serializable {
  // Variables
  private IdTransporteur idTransporteur = null; // Code transporteur
  private Date dateLivraisonSouhaitee = null; // Date de livraison souhaitée
  private Date dateLivraisonPrevue = null; // Date de livraison prévue
  private Adresse adresseLivraison = new Adresse(); // Adresse de livraison
  private IdZoneGeographique idZoneGeographique = null; // Id de la zone géographique
  private IdEtablissement idEtablissement = IdEtablissement.getInstancePourEtablissementVide();
  private String topEditionBordereau = ""; // Edition bordereau transporteur
  private String codeReroupement = ""; // Code Regroupemt Transport
  
  // -- Accesseurs
  
  /**
   * Contrôler si les informations obligatoires pour une adresse de livraison sont présentes.
   * Un MessageErreurException est généré dans le cas contraire.
   */
  public String controlerAdresseLivraison() {
    StringBuilder message = new StringBuilder();
    if (adresseLivraison == null || adresseLivraison.getNom().isEmpty()) {
      message.append("- le nom ou la raison sociale\n");
    }
    if (adresseLivraison == null || (adresseLivraison.getRue().isEmpty() && adresseLivraison.getLocalisation().isEmpty())) {
      message.append("- le numéro et la rue\n");
    }
    if (adresseLivraison == null || adresseLivraison.getCodePostal() <= 0) {
      message.append("- le code postal\n");
    }
    if (adresseLivraison == null || adresseLivraison.getVille().isEmpty()) {
      message.append("- la ville\n");
    }
    
    // Générer l'exception
    if (message.length() > 0) {
      return "L'adresse de livraison est incomplète, il manque :\n" + message;
    }
    return null;
  }
  
  public IdTransporteur getIdTransporteur() {
    return idTransporteur;
  }
  
  public void setIdTransporteur(IdTransporteur pId) {
    this.idTransporteur = pId;
  }
  
  public Date getDateLivraisonSouhaitee() {
    return dateLivraisonSouhaitee;
  }
  
  public void setDateLivraisonSouhaitee(Date dateLivraisonSouhaitee) {
    this.dateLivraisonSouhaitee = dateLivraisonSouhaitee;
  }
  
  public Date getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  public void setDateLivraisonPrevue(Date dateLivraisonPrevue) {
    this.dateLivraisonPrevue = dateLivraisonPrevue;
  }
  
  public Adresse getAdresseLivraison() {
    return adresseLivraison;
  }
  
  public void setAdresseLivraison(Adresse adresseLivraison) {
    this.adresseLivraison = adresseLivraison;
  }
  
  public IdZoneGeographique getIdZoneGeographique() {
    return idZoneGeographique;
  }
  
  public void setIdZoneGeographique(IdZoneGeographique idZoneGeo) {
    this.idZoneGeographique = idZoneGeo;
  }
  
  public String getTopEditionBordereau() {
    return topEditionBordereau;
  }
  
  public void setTopEditionBordereau(String topEditionBordereau) {
    this.topEditionBordereau = topEditionBordereau;
  }
  
  public String getCodeReroupement() {
    return codeReroupement;
  }
  
  public void setCodeReroupement(String codeReroupement) {
    this.codeReroupement = codeReroupement;
  }
  
}
