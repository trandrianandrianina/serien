/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.client.alertes.AlertesEncoursClient;

/**
 * Ecnours d'un client.
 */
public class EncoursClient implements Serializable {
  private static final int POURCENTAGE_ENCOURS_LIMITE = 80;
  
  private IdClient idClient = null;
  private String indicateurs = "";
  private BigDecimal effets = BigDecimal.ZERO;
  private long positionComptable = 0;
  private BigDecimal totalAcompte = BigDecimal.ZERO;
  private BigDecimal totalPortefeuille = BigDecimal.ZERO;
  private BigDecimal remiseEscompte = BigDecimal.ZERO;
  private BigDecimal remiseEncaissement = BigDecimal.ZERO;
  private long totalImpayes = 0;
  private int limiteImpayes = 0;
  private String dateDernierImpaye = "";
  private int nombreImpayes = 0;
  private long totalAffacturage = 0;
  private char topAffacturage = ' ';
  private BigDecimal totalEcheancesDepassees = BigDecimal.ZERO;
  private Integer encoursCommande = null;
  private BigDecimal encoursExpedie = BigDecimal.ZERO;
  private BigDecimal encoursFacture = BigDecimal.ZERO;
  private int venteAssimileeExport = 0;
  private Integer plafond = 0;
  private Integer plafondExceptionnel = null;
  private Date dateLimitePlafondExceptionnel = null;
  private Integer plafondMaxDeblocage = null;
  private Integer depassement = null;
  private Integer encours = null;
  private AlertesEncoursClient alertesEncoursClient = new AlertesEncoursClient();
  
  /**
   * Identifiant du client.
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  public void setIdClient(IdClient pIdClient) {
    this.idClient = pIdClient;
  }
  
  /**
   * Indicateurs (POIND).
   * Pos 1 : Alertes pour écheances dépassées si PST,129 <> Blanc
   * Pos 2 : Blocage expéditions pour échéances dépassées si PST,129 <> Blanc
   * Pos 3 : Blocage commandes pour échéances dépassées si PST,129 <> Blanc
   * Pos 4 : Valeur de la PST,119 si PST,129 <> Blanc
   * Pos 5 : Niveau maximum de relance du client
   * Pos 6 : Non utilisée
   * Pos 7 : Valeur de la zone Top Attention (CLTNS) du fichier PGVMCLIM
   * Pos 8 à 10 non utilisées
   */
  public String getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * Modifier les indicateurs.
   */
  public void setIndicateurs(String indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  /**
   * Montant de position comptable (PoPCO).
   * Solde comptable, débits moins crédits.
   */
  public long getPositionComptable() {
    return positionComptable;
  }
  
  /**
   * Modifier la position comtpable.
   */
  public void setPositionComptable(long pPositionComptable) {
    positionComptable = pPositionComptable;
  }
  
  /**
   * Montant des effets non échus (PoEFF).
   * Montant total de l'ensemble des effets (chèques, traites et autres modes de règlements) qui ne sont pas arrivés à échéance (date
   * de règlement non atteinte).
   */
  public BigDecimal getEffets() {
    return effets;
  }
  
  /**
   * Modifier les effets.
   */
  public void setEffets(BigDecimal pEffets) {
    effets = pEffets;
  }
  
  /**
   * Montant total du portefeuille (POPOR).
   * Montant total des effets non échus qui sont en portefeuille.
   */
  public BigDecimal getTotalPortefeuille() {
    return totalPortefeuille;
  }
  
  /**
   * Modifier le montant total du portefeuille.
   */
  public void setTotalPortefeuille(BigDecimal totalPortefeuille) {
    this.totalPortefeuille = totalPortefeuille;
  }
  
  /**
   * Montant des effets remis à l'escompte (PoESC).
   * Montants total des effets non échus, remis à la banque qui sont escomptés.
   */
  public BigDecimal getRemiseEscompte() {
    return remiseEscompte;
  }
  
  /**
   * Modifier la remise escompte.
   */
  public void setRemiseEscompte(BigDecimal pRemiseEscompte) {
    remiseEscompte = pRemiseEscompte;
  }
  
  /**
   * Montant des effets remise à l'encaissement (POENC).
   * Montants total des effets non échus, remis à la banque qui sont à l'encaissement.
   */
  public BigDecimal getRemiseEncaissement() {
    return remiseEncaissement;
  }
  
  /**
   * Modifier la remise encaissement.
   */
  public void setRemiseEncaissement(BigDecimal premiseEncaissement) {
    remiseEncaissement = premiseEncaissement;
  }
  
  /**
   * Total d'affacturage (POAFM).
   * Montant total (arrondi à l'unité) des factures cédées à un organisme d'affacturage.
   */
  public long getTotalAffacturage() {
    return totalAffacturage;
  }
  
  /**
   * Modifier le total d'affacturage.
   */
  public void setTotalAffacturage(long totalAffacturage) {
    this.totalAffacturage = totalAffacturage;
  }
  
  /**
   * Indicateur d'affacturage (POTAF).
   */
  public char getTopAffacturage() {
    return topAffacturage;
  }
  
  /**
   * Modifier l'indicatreur d'affacturage.
   */
  public void setTopAffacturage(char pTopAffacturage) {
    topAffacturage = pTopAffacturage;
  }
  
  /**
   * Montant total d'acomptes (PoACC).
   * Cumul le montant des acomptes réglés par le client mais dont les factures ne sont pas réglés en totalité.
   */
  public BigDecimal getTotalAcompte() {
    return totalAcompte;
  }
  
  /**
   * Modifier le montant total d'acomptes.
   */
  public void setTotalAcompte(BigDecimal pTotalAcompte) {
    totalAcompte = pTotalAcompte;
  }
  
  /**
   * Montant total d'impayés (POTIM).
   * Montant total (arrondi à l'unité) des factures qui sont arrivé à échéance et qui ne sont pas payées.
   */
  public long getTotalImpayes() {
    return totalImpayes;
  }
  
  /**
   * Modifier le montant total d'impayés.
   */
  public void setTotalImpayes(long pTotalImpayes) {
    totalImpayes = pTotalImpayes;
  }
  
  /**
   * Impayés si date de création > date dernier impayé (PoLIM).
   */
  public int getLimiteImpayes() {
    return limiteImpayes;
  }
  
  /**
   * ?
   */
  public void setLimiteImpayes(int pLimiteImpayes) {
    limiteImpayes = pLimiteImpayes;
  }
  
  /**
   * Date de dernier impayé (PoDIM).
   * Date de la facture "la plus ancienne ou récente ?" impayé.
   */
  public String getDateDernierImpaye() {
    return dateDernierImpaye;
  }
  
  /**
   * Modifier le date de dernier impayé.
   */
  public void setDateDernierImpaye(String pDateDernierImpaye) {
    dateDernierImpaye = pDateDernierImpaye;
  }
  
  /**
   * Nombre d'impayés (PONIM).
   * Nombre de factures impayées.
   */
  public int getNombreImpayes() {
    return nombreImpayes;
  }
  
  /**
   * Modifier le nombre d"impayés.
   */
  public void setNombreImpayes(int pNombreImpayes) {
    nombreImpayes = pNombreImpayes;
  }
  
  /**
   * Montant d'encours des commandes (POCDE).
   * Montant total des commandes non encore expédiées.
   */
  public Integer getEncoursCommande() {
    return encoursCommande;
  }
  
  /**
   * Modifier l'encours de commande.
   */
  public void setEncoursCommande(Integer pEncoursCommande) {
    encoursCommande = pEncoursCommande;
  }
  
  /**
   * Modifier l'encours de commande.
   * De façon général, l'encours est géré à l'unité, sans décimales signifactives. Cette méthode convertie la valeur fournie en entier.
   */
  public void setEncoursCommande(BigDecimal pEncoursCommande) {
    encoursCommande = pEncoursCommande.intValue();
  }
  
  /**
   * Montant d'encours des commandes expédiés (POEXP).
   * Montant total des commandes expédiées mais pas encore facturées.
   */
  public BigDecimal getEncoursExpedie() {
    return encoursExpedie;
  }
  
  /**
   * Modifier l'encours expédié (POEXP).
   */
  public void setEncoursExpedie(BigDecimal pEncoursExpedie) {
    encoursExpedie = pEncoursExpedie;
  }
  
  /**
   * Modifier l'encours facturé (POFAC).
   * Montant total des commandes facturées mais pas encore comptabilisées.
   */
  public BigDecimal getEncoursFacture() {
    return encoursFacture;
  }
  
  /**
   * Modifier l'encours facturé (POFAC).
   */
  public void setEncoursFacture(BigDecimal pEncoursFacture) {
    encoursFacture = pEncoursFacture;
  }
  
  /**
   * Nombre total d'échéances dépassées (POECD).
   * Nombre de factures échues et non réglées.
   */
  public BigDecimal getTotalEcheancesDepassees() {
    return totalEcheancesDepassees;
  }
  
  /**
   * Total échéances dépassées.
   */
  public void setTotalEcheancesDepassees(BigDecimal pTotalEcheancesDepassees) {
    totalEcheancesDepassees = pTotalEcheancesDepassees;
  }
  
  /**
   * Montant de ventes assimilées export (POVAE).
   * Montant des ventes assimilées à l'export non réglées.
   */
  public int getVenteAssimileeExport() {
    return venteAssimileeExport;
  }
  
  /**
   * Modifier la vente assimilé export (POVAE)
   */
  public void setVenteAssimileeExport(int pVenteAssimileeExport) {
    venteAssimileeExport = pVenteAssimileeExport;
  }
  
  /**
   * Montant d'encours total (PODPOS).
   * C'est la valeur de ce que le client nous doit calculée à partir de l'encours comptable et de l'encours commerciale.
   * Encours = PositionComptable + Effets + Affacturage + EncoursFacturé + EncoursExpédié + EncoursCommandé(si paramétré).
   */
  public Integer getEncours() {
    return encours;
  }
  
  /**
   * Modifier la position encours.
   */
  public void setEncours(Integer pPositionEncours) {
    encours = pPositionEncours;
  }
  
  /**
   * Plafond d'encours autorisé (POPLF).
   * Montant du plafond d'encours autorisé définit sur la fiche client (précision à l'unité).
   */
  public Integer getPlafond() {
    return plafond;
  }
  
  /**
   * Modifier le plafond d'encours autorisé.
   */
  public void setPlafond(Integer pPlafond) {
    plafond = pPlafond;
  }
  
  /**
   * Indiquer si un plafond est définit (différent de zéro).
   * Le plafond est considéré comme existant s'il est différent de zéro. S'il est égal à zéro, le plafond est considéré comme inexistant.
   * Le reste et le dépassement sont égal à 0 dans ce cas.
   */
  public boolean isPlafondPresent() {
    return plafond > 0;
  }
  
  /**
   * Montant restant avant d'atteindre le plafond d'encours autorisé.
   * Le reste est égal au plafond autorisé moins l'encours. La valeur est positive si le plafond n'est pas atteint. La valeur est
   * négative si le plafond est dépassée. Le reste est toujours égal à zéro si le plafond n'est pas définit.
   * Reste = PlafondAutorise - PositionEncours
   */
  public Integer getResteAvantPlafond() {
    if (!isPlafondPresent()) {
      return 0;
    }
    return plafond - encours;
  }
  
  /**
   * Montant de dépassement du plafond d'encours autorisé (PODEPA).
   * Le dépassement est égal à l'encours moins le plafond autorisé. La valeur est positive si le plafond est dépassée. La valeur est
   * négative si le plafond n'est pas atteint. Le dépassement est toujours égal à zéro si le plafond n'est pas définit.
   * Depassement = PositionEncours - PlafondAutorise
   */
  public Integer getDepassement() {
    if (!isPlafondPresent()) {
      return 0;
    }
    return depassement;
  }
  
  /**
   * Modifier le dépassement.
   * 
   */
  public void setDepassement(Integer pDepassement) {
    depassement = pDepassement;
  }
  
  /**
   * Pourcentage d'encours atteint par rapport au plafond autorisé (1 = 1%, 100 = 100%).
   */
  public Integer getPourcentageEncours() {
    if (plafond == 0) {
      return 0;
    }
    return Integer.valueOf((encours / plafond) * 100);
  }
  
  /**
   * Indiquer si l'encours à atteint ou dépassé le pourcentage d'encours limite.
   * Le pourcentage d'encours limite est actuellement de 80%
   */
  public boolean isPourcentageLimiteEncoursAtteint() {
    return getPourcentageEncours() >= POURCENTAGE_ENCOURS_LIMITE;
  }
  
  /**
   * Retourne le message adapté suivant l'état de l'encours client.
   */
  public String getMessageEncours() {
    String euro = "\u20ac";
    if (getDepassement() > 0) {
      return String.format("Le client a dépassé son encours de %d " + euro + ".", getDepassement());
    }
    else if (isPourcentageLimiteEncoursAtteint()) {
      return String.format("Le client a depassé les %d%% du plafond d'encours !", POURCENTAGE_ENCOURS_LIMITE);
    }
    return "";
  }
  
  /**
   * Plafond exception nel d'encours autorisé (POPLF2).
   */
  public Integer getPlafondExceptionnel() {
    return plafondExceptionnel;
  }
  
  /**
   * Modifier le plafond exceptionnel d'encours autorisé.
   */
  public void setPlafondExceptionnel(Integer pPlafondExceptionnel) {
    plafondExceptionnel = pPlafondExceptionnel;
  }
  
  /**
   * Indiquer si un plafond exceptionnel est définit et différent du plafond standard.
   * Le plafond exceptionnel est considéré comme inexistant s'il égal à 0 ou s'il a la même valeur que la pafond standard.
   */
  public boolean isPlafondExceptionnelPresent() {
    return plafondExceptionnel != null && plafondExceptionnel.compareTo(0) != 0 && plafondExceptionnel.compareTo(plafond) != 0;
  }
  
  /**
   * Date limite pour le plafond exceptionnel (PODPL).
   */
  public Date getDateLimitePlafondExceptionnel() {
    return dateLimitePlafondExceptionnel;
  }
  
  /**
   * Modifier la date limite pour le plafond exceptionnel.
   */
  public void setDateLimitePlafondExceptionnel(Date pDateLimitePlafondExceptionnel) {
    dateLimitePlafondExceptionnel = pDateLimitePlafondExceptionnel;
  }
  
  /**
   * Plafond maximum de déblocage (POPLMX).
   */
  public Integer getPlafondMaxDeblocage() {
    return plafondMaxDeblocage;
  }
  
  /**
   * Modifier le plafond maximum de déblocage (POPLMX).
   */
  public void setPlafondMaxDeblocage(Integer pPlafondMaxDeblocage) {
    plafondMaxDeblocage = pPlafondMaxDeblocage;
  }
  
  /**
   * Liste des alertes sur l'encours du client.
   */
  public AlertesEncoursClient getAlertesEncoursClient() {
    return alertesEncoursClient;
  }
  
  /**
   * Modifier les alertes sur l'encours du client.
   */
  public void setAlertesEncoursClient(AlertesEncoursClient pAlertesEncoursClient) {
    alertesEncoursClient = pAlertesEncoursClient;
  }
}
