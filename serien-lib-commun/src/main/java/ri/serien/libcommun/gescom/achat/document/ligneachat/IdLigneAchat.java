/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'une ligne de document de ventes.
 * 
 * L'identifiant est composé de l'identiant de l'établissement et du numéro de ligne dans le document.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdLigneAchat extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_NUMEROLIGNE = 4;
  public static final int LONGUEUR_INDICATIF = IdDocumentAchat.LONGUEUR_ENTETE + LONGUEUR_CODE_ETABLISSEMENT
      + IdDocumentAchat.LONGUEUR_NUMERO + IdDocumentAchat.LONGUEUR_SUFFIXE + LONGUEUR_NUMEROLIGNE;
  
  public static final int INDICATIF_ENTETE_ETB_NUM_SUF_NLI = 0;
  public static final int INDICATIF_NUM_SUF_NLI = 1;
  public static final int INDICATIF_BASE = 2;
  
  // Variables
  private IdDocumentAchat idDocumentAchat;
  private Integer numeroLigne;
  
  /**
   * Constructeur avec l'identifiant document et le numéro de ligne.
   */
  private IdLigneAchat(EnumEtatObjetMetier pEnumEtatObjetMetier, IdDocumentAchat pIdDocument, Integer pNumeroLigne) {
    super(pEnumEtatObjetMetier, pIdDocument.getIdEtablissement());
    idDocumentAchat = pIdDocument;
    numeroLigne = controlerNumeroLigne(pNumeroLigne);
  }
  
  /**
   * Créer un identifiant à partir de l'identifiant document et le numéro de ligne.
   */
  public static IdLigneAchat getInstance(IdDocumentAchat pIdDocument, Integer pNumeroLigne) {
    return new IdLigneAchat(EnumEtatObjetMetier.MODIFIE, pIdDocument, pNumeroLigne);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdLigneAchat getInstance(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentAchat pCodeEntete, Integer pNumero,
      Integer pSuffixe, Integer pNumeroLigne) {
    IdDocumentAchat idDocumentAchat = IdDocumentAchat.getInstance(pIdEtablissement, pCodeEntete, pNumero, pSuffixe);
    return new IdLigneAchat(EnumEtatObjetMetier.MODIFIE, idDocumentAchat, pNumeroLigne);
  }
  
  /**
   * Contrôler la validité du numéro de ligne.
   * Le numéro de ligne est compris entre 0 et 9 999.
   */
  private Integer controlerNumeroLigne(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de la ligne n'est pas renseigné.");
    }
    if (pValeur < 0) {
      throw new MessageErreurException("Le numéro de la ligne est inférieur à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMEROLIGNE)) {
      throw new MessageErreurException("Le numéro de ligne est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdLigneAchat controlerId(IdLigneAchat pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de la ligne d'achat est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de la ligne d'achat n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + idDocumentAchat.hashCode();
    code = 37 * code + numeroLigne.hashCode();
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    
    if (pObject == null) {
      return false;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdLigneAchat)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de ligne d'achat.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdLigneAchat id = (IdLigneAchat) pObject;
    return idDocumentAchat.equals(id.idDocumentAchat) && numeroLigne.equals(id.numeroLigne);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdLigneAchat)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdLigneAchat id = (IdLigneAchat) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idDocumentAchat.compareTo(id.idDocumentAchat);
    if (comparaison != 0) {
      return comparaison;
    }
    return numeroLigne.compareTo(id.numeroLigne);
  }
  
  @Override
  public String getTexte() {
    return "" + getNumero() + SEPARATEUR_ID + getSuffixe() + SEPARATEUR_ID + numeroLigne;
  }
  
  /**
   * Retourner l'indicatif de la ligne en fonction du type souhaité.
   */
  public String getIndicatif(int pTypeIndicatif) {
    switch (pTypeIndicatif) {
      case INDICATIF_ENTETE_ETB_NUM_SUF_NLI:
        return idDocumentAchat.getIndicatif(IdDocumentAchat.INDICATIF_ENTETE_ETB_NUM_SUF)
            + String.format("%0" + LONGUEUR_NUMEROLIGNE + "d", numeroLigne);
      case INDICATIF_NUM_SUF_NLI:
        return idDocumentAchat.getIndicatif(IdDocumentAchat.INDICATIF_NUM_SUF)
            + String.format("%0" + LONGUEUR_NUMEROLIGNE + "d", numeroLigne);
    }
    return "";
  }
  
  // -- Accesseurs
  
  /**
   * Identifiant du document d'achat associé à la ligne.
   */
  public IdDocumentAchat getIdDocumentAchat() {
    return idDocumentAchat;
  }
  
  /**
   * Entête du document contenant la ligne.
   */
  public EnumCodeEnteteDocumentAchat getCodeEntete() {
    return idDocumentAchat.getCodeEntete();
  }
  
  /**
   * Numéro du document contenant la ligne.
   * Le numéro du document est compris entre entre 0 et 999 999.
   */
  public Integer getNumero() {
    return idDocumentAchat.getNumero();
  }
  
  /**
   * Suffixe du document contenant la ligne.
   */
  public Integer getSuffixe() {
    return idDocumentAchat.getSuffixe();
  }
  
  /**
   * Numéro de ligne au sein du document.
   */
  public Integer getNumeroLigne() {
    return numeroLigne;
  }
}
