/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.util.ArrayList;

/**
 * Liste de marques propres aux articles.
 * Actuellement pas d'objet métier car la notion est un simple champ alphanumérique dans l'article.
 */
public class ListeMarques extends ArrayList<String> {
  public static final String TOUTES_MARQUES = "Toutes";
}
