/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stock;

import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Stock d'un article.
 * 
 * Le plus souvent, le stock de l'article est celui pour un magasin. Dans ce cas, l'identifiant stock comporte l'identifiant du magasin.
 * Lorsqu'on souhaite avoir le stock pour l'ensemble de l'établissement, l'identifiant stock ne comporte pas d'identifiant magasin mais
 * seulement un identifiant établissement.
 */
public class Stock extends AbstractClasseMetier<IdStock> {
  private BigDecimal stockPhysique = null;
  private BigDecimal stockAttendu = null;
  private BigDecimal stockCommande = null;
  private BigDecimal stockCommandeLieSurAchat = null;
  private BigDecimal stockReserve = null;
  private BigDecimal stockAvantRupture = null;
  private Date dateRupturePossible = null;
  private Date dateFinRupture = null;
  
  /**
   * Constructeur avec l'identififant du stock (obligatoire).
   */
  public Stock(IdStock pIdStock) {
    super(pIdStock);
  }
  
  /**
   * Retourner une représentation de l'objet métier sous forme d'une chaîne de caractères.
   */
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  /**
   * Ajouter les quantités d'un stock à ce stock.
   * 
   * @param pStock Stock dont il faut ajouter les quantité au stock courant.
   */
  public void ajouter(Stock pStock) {
    // Vérifier les paramètres
    if (pStock == null) {
      return;
    }
    
    // Additionner les quantités
    if (stockPhysique != null) {
      stockPhysique = stockPhysique.add(pStock.stockPhysique);
    }
    else {
      stockPhysique = pStock.stockPhysique;
    }
    
    if (stockAttendu != null) {
      stockAttendu = stockAttendu.add(pStock.stockAttendu);
    }
    else {
      stockAttendu = pStock.stockAttendu;
    }
    
    if (stockCommande != null) {
      stockCommande = stockCommande.add(pStock.stockCommande);
    }
    else {
      stockCommande = pStock.stockCommande;
    }
    
    if (stockCommandeLieSurAchat != null) {
      stockCommandeLieSurAchat = stockCommandeLieSurAchat.add(pStock.stockCommandeLieSurAchat);
    }
    else {
      stockCommandeLieSurAchat = pStock.stockCommandeLieSurAchat;
    }
    
    if (stockReserve != null) {
      stockReserve = stockReserve.add(pStock.stockReserve);
    }
    else {
      stockReserve = pStock.stockReserve;
    }
  }
  
  /**
   * Retourner le stock physique.
   * 
   * Le stock physique comptabilise l’ensemble des articles présents physiquement. Ce sont les articles qui ont été réceptionnés et
   * n'ont pas encore été livrés à un client. Ils peuvent cependant avoir été réservés ou même commandés par un client mais ils sont
   * toujours présents.
   * 
   * @return Stock physique.
   */
  public BigDecimal getStockPhysique() {
    return stockPhysique;
  }
  
  /**
   * Modifier le stock physique.
   * @param pStockPhysique Nouveau stock physique.
   */
  public void setStockPhysique(BigDecimal pStockPhysique) {
    stockPhysique = pStockPhysique;
  }
  
  /**
   * Retourner le stock attendu.
   * 
   * Le stock attendu est la quantité d’articles commandés aux fournisseurs et non encore réceptionnés, quelle que soit la date de
   * commande. Ces commandes viendront incrémenter le stock physique de l'article lorsqu'elles seront réceptionnées. Les commandes
   * fournisseurs « Direct usine » ne sont pas comptées dans les attendus.
   * 
   * @return Stock attendu.
   */
  public BigDecimal getStockAttendu() {
    return stockAttendu;
  }
  
  /**
   * Modifier le stock attendu.
   * @param pStockAttendu Nouveau stock attendu.
   */
  public void setStockAttendu(BigDecimal pStockAttendu) {
    stockAttendu = pStockAttendu;
  }
  
  /**
   * Retourner le stock commandé.
   * 
   * Le stock commandé est la quantité d’articles commandés ou réservés par les clients, quelle que soit la date de commande ou de
   * réservation. Ce sont des articles sur lesquels un client a déjà mis une option. Pour déterminer la quantité commandée, il faut faire
   * le total des commandes clients avec l'état VAL ou RES. Les commandes clients « Direct usine » ne sont pas comptées dans les
   * commandés.
   * 
   * @return Stock commandé.
   */
  public BigDecimal getStockCommande() {
    return stockCommande;
  }
  
  /**
   * Modifier le stock commandé.
   * @param Nouveau stock commandé.
   */
  public void setStockCommande(BigDecimal pStockCommande) {
    stockCommande = pStockCommande;
  }
  
  /**
   * Retourner le stock commandé lié sur achats.
   * 
   * Le commandé lié sur achat est la quantité d’articles commandés par les clients et qui sont liés à une commande fournisseur.
   * Ce lien peut être de 2 natures :
   * - Soit par un GBA (génération de bon d’achat), c’est à dire qu’un bon d’achat est généré en contrepartie d’une vente.
   * - Soit par une affectation, c’est à dire que la quantité vendue au client est réservée sur un bon d’achat existant mais non
   * encore réceptionné.
   *
   * @return Stock commandé lié sur achats.
   */
  public BigDecimal getStockCommandeLieSurAchat() {
    return stockCommandeLieSurAchat;
  }
  
  /**
   * Modifier le stock commandé lié sur achats.
   * @param pStockCommandeLieSurAchat Nouveau stock commandé lié sur achats.
   */
  public void setStockCommandeLieSurAchat(BigDecimal pStockCommandeLieSurAchat) {
    stockCommandeLieSurAchat = pStockCommandeLieSurAchat;
  }
  
  /**
   * Retourner le stock réservé.
   * 
   * Le stock réservé est la quantité d'article réservée pour des clients. Pour réserver un article, il faut valider une commande grâce
   * à l’option « Réservation (RES) ». Pour déterminer la quantité réservée, il faut faire le total des commandes avec l'état RES.
   * Une quantité réservée ne peut être utilisée dans une autre vente si le paramétrage l’interdit (voir PS067). Noter que le stock
   * réservé est également comptabilisé dans le stock commandé.
   * 
   * @return Stock réservé.
   */
  public BigDecimal getStockReserve() {
    return stockReserve;
  }
  
  /**
   * Modifier le stock réservé.
   * 
   * @param pStockReserve Nouveau stock réservé.
   */
  public void setStockReserve(BigDecimal pStockReserve) {
    stockReserve = pStockReserve;
  }
  
  /**
   * Retourner le stock théorique.
   * 
   * Le stock théorique correspond au stock physique qui sera atteint lorsque toutes les commandes fournisseurs en cours auront été
   * réceptionnées et que toutes les commandes clients en cours auront été livrées.
   * Le stock théorique, également nommé disponible achat, est une quantité intéressante pour les acheteurs car elle intègre les attendus
   * fournisseurs. Cela évite de sur-commander puisque cette valeur intègre les quantités restantes à réceptionner.
   * 
   * Stock théorique = Stock physique + Stock attendu - Stock commandé
   * 
   * @return Stock théorique.
   */
  public BigDecimal getStockTheorique() {
    if (stockPhysique == null || stockAttendu == null || stockCommande == null) {
      return null;
    }
    return stockPhysique.add(stockAttendu).subtract(stockCommande);
  }
  
  /**
   * Retourner le stock net.
   * 
   * Le stock net correspond au stock physique moins l'ensemble des commandes clients qui n’ont pas encore été traitées. Ce sont les
   * articles présents physiquement dont on décompte les articles déjà commandés ou réservés par des clients. Ici, on décide d'ignorer
   * les commandes fournisseurs tant qu'elles n'ont pas été réceptionnées. Par contre, on prend en compte toutes les commandes clients
   * quelques soient leur date. C'est une approche prudente pour pouvoir annoncer au client un stock dont on est sûr qu'il sera
   * disponible pour la vente. Le stock net est également nommé disponible vente. Cela illustre que cette quantité est intéressante du
   * point de vue vendeur pour savoir quelle quantité il reste à vendre.
   * 
   * Stock net = Stock physique - Stock commandé
   * 
   * @return Stock net.
   */
  public BigDecimal getStockNet() {
    if (stockPhysique == null || stockCommande == null) {
      return null;
    }
    return stockPhysique.subtract(stockCommande);
  }
  
  /**
   * Retourner le stock disponible.
   * 
   * Le stock net est donc le stock physique moins le stock commandé. Un problème avec ce calcul concerne les commandes clients associées
   * à un attendu fournisseur. Les commandes clients sont décomptées du stock net mais les attendus fournisseurs correspondantes sont
   * ignorés. Cela donne une vision du stock disponible plus défavorable que la réalité.
   * 
   * La formule du stock disponible ci-dessous apporte une réponse à ce problème. C’est la même formule que le stock net si ce n’est
   * qu’on y réintègre les commandes clients liées à des attendus fournisseurs. L’idée est de ne pas décompter du stock disponible des
   * commandes clients qui sont couvertes par des attendus fournisseurs à venir.
   * 
   * Stock disponible = Stock physique - (Stock commandé - Stock commandé lié sur achat)
   * 
   * @return Stock disponible.
   */
  public BigDecimal getStockDisponible() {
    if (stockPhysique == null || stockCommande == null || stockCommandeLieSurAchat == null) {
      return null;
    }
    return stockPhysique.subtract(stockCommande).add(stockCommandeLieSurAchat);
  }
  
  /**
   * Retourner le stock avant rupture.
   * 
   * Quantité correspondant au stock avant rupture de l’article en fonction des attendus et commandés à venir.
   * Il faut appeler la méthode calculerStock() de cette classe afin que cette valeur soit renseignée.
   * 
   * @return Stock avant rupture.
   */
  public BigDecimal getStockAvantRupture() {
    return stockAvantRupture;
  }
  
  /**
   * Modifier le stock avant rupture.
   * @param pStockAvantRupture Stock avant rupture.
   */
  public void setStockAvantRupture(BigDecimal pStockAvantRupture) {
    stockAvantRupture = pStockAvantRupture;
  }
  
  /**
   * Retourner la date de rupture possible..
   * 
   * Date à laquelle le stock de l’article passera en rupture (quantité négative ou nulle) si on vend une quantité d’articles supérieure
   * ou égale au stock avant rupture. Par exemple, si le stock minimum est atteint le 19/10/2022 avec 292 pièces, le stock avant rupture
   * sera 292 et la date de rupture possible sera le 19/10/2022.
   * Il faut appeler la méthode calculerStock() de cette classe afin que cette valeur soit renseignée.
   * 
   * @return Date de rupture possible.
   */
  public Date getDateRupturePossible() {
    return dateRupturePossible;
  }
  
  /**
   * Modifier la date de rupture possible.
   * @param pDateRupturePossible Date de rupture possible.
   */
  public void setDateRupturePossible(Date pDateRupturePossible) {
    dateRupturePossible = pDateRupturePossible;
  }
  
  /**
   * Retourner la date de fin de rupture.
   * 
   * Date à laquelle le stock avant rupture repasse à une valeur positive. Cette date n’est renseignée que si le stock avant rupture
   * est négatif.
   * Il faut appeler la méthode calculerStock() de cette classe afin que cette valeur soit renseignée.
   * 
   * @return Date de fin de rupture.
   */
  public Date getDateFinRupture() {
    return dateFinRupture;
  }
  
  /**
   * Modifier la date de fin de rupture.
   * @param pDateFinRupture Date de fin de rupture.
   */
  public void setDateFinRupture(Date pDateFinRupture) {
    dateFinRupture = pDateFinRupture;
  }
}
