/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.blocnotes;

import java.util.ArrayList;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe implémente un bloc Notes Série N. Elle contrôle sa construction et son adaptation
 * aux règles Série N : nombre de lignes, longueur de lignes, etc...
 */
public class BlocNote extends AbstractClasseMetier<IdBlocNote> {
  // Constantes
  // Dans le fichier une ligne fait 120 caractères mais elle contient 2 lignes
  public static final int LONGUEUR_LIGNE_AFFICHEE = 60;
  public static final int LONGUEUR_LIGNE_STOCKEE = 120;
  public static final int NOMBRE_LIGNES_AFFICHEES = 18;
  public static final int NOMBRE_LIGNES_STOCKEES = 9;
  public static final int LONGUEUR_TEXTE_MAX = LONGUEUR_LIGNE_STOCKEE * NOMBRE_LIGNES_STOCKEES;
  
  // Variables
  private String texte = "";
  
  /**
   * Constructeur.
   */
  public BlocNote(IdBlocNote pIdBlocNote) {
    super(pIdBlocNote);
  }
  
  @Override
  public String getTexte() {
    if (texte == null) {
      texte = "";
    }
    return texte;
  }
  // -- Méthodes publiques
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdBlocNote controlerId(BlocNote pBlocNote, boolean pVerifierExistance) {
    if (pBlocNote == null) {
      throw new MessageErreurException("Le bloc-notes est invalide.");
    }
    return IdBlocNote.controlerId(pBlocNote.getId(), true);
  }
  
  /**
   * Contrôle si le mémo est vide.
   */
  public boolean isVide() {
    return Constantes.normerTexte(texte).isEmpty();
  }
  
  /**
   * Prépare le texte du mémo pour son enregistrement dans la base.
   */
  public String[] preparerEnregistrementTexte() {
    if (texte == null) {
      return new String[0];
    }
    
    // Conversion du texte en tableau (sans concaténation)
    ArrayList<String> lignes = convertirTexteEnTableau();
    
    // On contrôle le nombre de lignes afin qu'il ne dépasse pas le nombre max de lignes autorisées
    // Si ça dépasse, on supprime les lignes en trop
    while (lignes.size() > NOMBRE_LIGNES_AFFICHEES) {
      lignes.remove(lignes.size() - 1);
    }
    
    // On contrôle que le nombre de lignes est pair, c'est plus facile pour la concaténation
    if ((lignes.size() % 2) != 0) {
      lignes.add("");
    }
    
    // On va concaténer les lignes 2 à 2 (à cause du stockage en base)
    String[] lignesConcatenees = new String[lignes.size() / 2];
    int indice = 0;
    for (int i = 0; i < lignes.size(); i = i + 2) {
      lignesConcatenees[indice++] = String.format(
          "%-" + BlocNote.LONGUEUR_LIGNE_AFFICHEE + "s%-" + BlocNote.LONGUEUR_LIGNE_AFFICHEE + "s", lignes.get(i), lignes.get(i + 1));
    }
    
    return lignesConcatenees;
  }
  
  // -- Méthodes privées
  
  /**
   * Controler la validité du texte.
   */
  private void controlerValiditeTexte() {
    ArrayList<String> lignes = convertirTexteEnTableau();
    if (lignes.size() > BlocNote.NOMBRE_LIGNES_AFFICHEES) {
      throw new MessageErreurException(String.format(
          "Le mémo contient %d lignes, ce qui dépasse le nombre de lignes maximum admises qui est de %s. Votre mémo sera tronqué.",
          lignes.size(), BlocNote.NOMBRE_LIGNES_AFFICHEES));
    }
  }
  
  /**
   * Permet de convertir le texte du mémo en tableau,
   * c'est à dire x lignes qui ne dépassent pas la longueur "normale" (sans concaténation).
   */
  private ArrayList<String> convertirTexteEnTableau() {
    if (texte == null) {
      texte = "";
    }
    ArrayList<String> lignesBrutes = Constantes.splitString2List(texte, '\n');
    
    // On controle la dernière ligne, si elle est vide on la supprime
    if (lignesBrutes.size() > 1) {
      int indiceDerniereLigne = lignesBrutes.size() - 1;
      String ligne = lignesBrutes.get(indiceDerniereLigne);
      if (ligne.trim().isEmpty()) {
        lignesBrutes.remove(indiceDerniereLigne);
      }
    }
    
    // On formate chaque ligne à la longueur max d'une ligne mémo
    ArrayList<String> lignes = new ArrayList<String>();
    String chaine = "";
    int pos = -1;
    boolean complete = false;
    for (int i = 0; i < lignesBrutes.size(); i++) {
      // Si la longueur de la chaine est inférieure ou égale pas de problème
      if (lignesBrutes.get(i).length() <= BlocNote.LONGUEUR_LIGNE_AFFICHEE) {
        lignes.add(lignesBrutes.get(i));
        continue;
      }
      // Dans le cas où la chaine est trop longue
      complete = false;
      chaine = lignesBrutes.get(i);
      pos = 0;
      do {
        if ((chaine.length() - pos) > BlocNote.LONGUEUR_LIGNE_AFFICHEE) {
          lignes.add(chaine.substring(pos, pos + BlocNote.LONGUEUR_LIGNE_AFFICHEE));
          pos += BlocNote.LONGUEUR_LIGNE_AFFICHEE;
        }
        else {
          lignes.add(chaine.substring(pos));
          complete = true;
        }
      }
      while (!complete);
    }
    return lignes;
  }
  
  // -- Accesseurs
  
  public void setTexte(StringBuilder sbtexte) {
    this.texte = Constantes.trimRight(sbtexte);
    controlerValiditeTexte();
  }
  
  public void setTexte(String texte) {
    this.texte = Constantes.trimRight(texte);
    controlerValiditeTexte();
  }
  
  /**
   * Retourne le nombre maximal de ligne affichée dans un bloc note
   */
  public int getNombreLignesAffichees() {
    return NOMBRE_LIGNES_AFFICHEES;
  }
  
}
