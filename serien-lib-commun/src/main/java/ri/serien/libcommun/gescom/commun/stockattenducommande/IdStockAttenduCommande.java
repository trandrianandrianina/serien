/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockattenducommande;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'un attendu/commandé pour un article.
 *
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdStockAttenduCommande extends AbstractId {
  private EnumTypeAttenduCommande typeAttenduCommande;
  private final IdMagasin idMagasin;
  private IdArticle idArticle;
  private IdLigneVente idLigneVente;
  private IdLigneAchat idLigneAchat;
  
  /**
   * Constructeur interne.
   * 
   * @param pEnumEtatObjetMetier Etat de l'objet métier.
   * @param pTypeAttenduCommande Type de l'attendu/commandé.
   * @param pIdMagasin Identifiant du magasin (peut être null).
   * @param pIdArticle Identifiant de l'article.
   * @param pIdLigneVente Identifiant de la ligne de vente (peut être null).
   * @param pIdLigneAchat Identifiant de la ligne d'achat (peut être null).
   * @return Identitifiant de l'attendu/commandé.
   */
  private IdStockAttenduCommande(EnumEtatObjetMetier pEnumEtatObjetMetier, EnumTypeAttenduCommande pTypeAttenduCommande,
      IdMagasin pIdMagasin, IdArticle pIdArticle, IdLigneVente pIdLigneVente, IdLigneAchat pIdLigneAchat) {
    super(pEnumEtatObjetMetier);
    
    // Contrôler les paramètres
    if (pTypeAttenduCommande == null) {
      throw new MessageErreurException("Le type de l'attendu/commandé est invalide.");
    }
    switch (pTypeAttenduCommande) {
      case COMMANDE:
        IdMagasin.controlerId(pIdMagasin, false);
        IdArticle.controlerId(pIdArticle, false);
        IdLigneVente.controlerId(pIdLigneVente, false);
        break;
      
      case ATTENDU:
        IdMagasin.controlerId(pIdMagasin, false);
        IdArticle.controlerId(pIdArticle, false);
        IdLigneAchat.controlerId(pIdLigneAchat, false);
        break;
      
      case DATE_JOUR:
      case DATE_MINI_REAPPRO:
        IdArticle.controlerId(pIdArticle, false);
        break;
    }
    if (pIdMagasin != null && !pIdArticle.getIdEtablissement().equals(pIdMagasin.getIdEtablissement())) {
      throw new MessageErreurException("Stock invalide car les établissements de l'article et du magasin sont différents.");
    }
    
    // Renseigner les attributs
    typeAttenduCommande = pTypeAttenduCommande;
    idMagasin = pIdMagasin;
    idArticle = pIdArticle;
    idLigneVente = pIdLigneVente;
    idLigneAchat = pIdLigneAchat;
  }
  
  /**
   * Créer un identifiant pour un attendu/commandé.
   * 
   * @param pTypeAttenduCommande Type de l'attendu/commandé.
   * @param pIdMagasin Identifiant du magasin.
   * @param pIdArticle Identifiant de l'article.
   * @param pNumero Numéro du document de vente.
   * @param pSuffixe Suffixe du document de vente.
   * @param pLigne Ligne du document de vente.
   * @return Identitifiant de l'attendu/commandé.
   */
  public static IdStockAttenduCommande getInstance(EnumTypeAttenduCommande pTypeAttenduCommande, IdMagasin pIdMagasin,
      IdArticle pIdArticle, Integer pNumero, Integer pSuffixe, Integer pLigne) {
    // Contrôler les paramètres
    IdMagasin.controlerId(pIdMagasin, false);
    IdArticle.controlerId(pIdArticle, false);
    
    // Construire l'identifiant du document de vente
    IdLigneVente idLigneVente = null;
    IdLigneAchat idLigneAchat = null;
    switch (pTypeAttenduCommande) {
      case COMMANDE:
        // Construire l'identifiant du document de vente
        idLigneVente = IdLigneVente.getInstance(pIdMagasin.getIdEtablissement(), EnumCodeEnteteDocumentVente.COMMANDE_OU_BON, pNumero,
            pSuffixe, pLigne);
        break;
      
      case ATTENDU:
        // Construire l'identifiant du document d'achat
        idLigneAchat = IdLigneAchat.getInstance(pIdMagasin.getIdEtablissement(), EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION,
            pNumero, pSuffixe, pLigne);
        break;
      
      case DATE_JOUR:
      case DATE_MINI_REAPPRO:
        // Aucun identifiant de document dans ces cas
        break;
    }
    
    // Retourner l'identifiant attendu/commandé
    return new IdStockAttenduCommande(EnumEtatObjetMetier.MODIFIE, pTypeAttenduCommande, pIdMagasin, pIdArticle, idLigneVente,
        idLigneAchat);
  }
  
  /**
   * Créer un identifiant pour un commandé.
   * 
   * @param pIdLigneVente Identifiant de la ligne de vente.
   * @param pIdMagasin Identifiant du magasin.
   * @param pIdArticle Identifiant de l'article.
   * @return Identifiant de l'attendu/commandé.
   */
  public static IdStockAttenduCommande getInstancePourCommande(IdMagasin pIdMagasin, IdArticle pIdArticle, IdLigneVente pIdLigneVente) {
    return new IdStockAttenduCommande(EnumEtatObjetMetier.MODIFIE, EnumTypeAttenduCommande.COMMANDE, pIdMagasin, pIdArticle,
        pIdLigneVente, null);
  }
  
  /**
   * Créer un identifiant pour un attendu.
   * 
   * @param pIdMagasin Identifiant du magasin.
   * @param pIdArticle Identifiant de l'article.
   * @param pIdLigneAchat Identifiant de la ligne d'achat.
   * @return Identifiant de l'attendu/commandé.
   */
  public static IdStockAttenduCommande getInstancePourAttendu(IdMagasin pIdMagasin, IdArticle pIdArticle, IdLigneAchat pIdLigneAchat) {
    return new IdStockAttenduCommande(EnumEtatObjetMetier.MODIFIE, EnumTypeAttenduCommande.ATTENDU, pIdMagasin, pIdArticle, null,
        pIdLigneAchat);
  }
  
  /**
   * Créer un identifiant pour un attentu/commandé de type date du jour.
   * 
   * @param pIdArticle Identifiant de l'article.
   * @return Identifiant de l'attendu/commandé.
   */
  public static IdStockAttenduCommande getInstancePourDateJour(IdArticle pIdArticle) {
    return new IdStockAttenduCommande(EnumEtatObjetMetier.MODIFIE, EnumTypeAttenduCommande.DATE_JOUR, null, pIdArticle, null, null);
  }
  
  /**
   * Créer un identifiant pour un attentu/commandé de type date minimum de réapprovisionnement.
   * 
   * @param pIdArticle Identifiant de l'article.
   * @return Identifiant de l'attendu/commandé.
   */
  public static IdStockAttenduCommande getInstancePourDateMiniReappro(IdArticle pIdArticle) {
    return new IdStockAttenduCommande(EnumEtatObjetMetier.MODIFIE, EnumTypeAttenduCommande.DATE_MINI_REAPPRO, null, pIdArticle, null,
        null);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + typeAttenduCommande.hashCode();
    if (idMagasin != null) {
      code = 37 * code + idMagasin.hashCode();
    }
    if (idArticle != null) {
      code = 37 * code + idArticle.hashCode();
    }
    if (idLigneVente != null) {
      code = 37 * code + idLigneVente.hashCode();
    }
    if (idLigneAchat != null) {
      code = 37 * code + idLigneAchat.hashCode();
    }
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdStockAttenduCommande)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de stock attendu/commandé.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdStockAttenduCommande id = (IdStockAttenduCommande) pObject;
    return typeAttenduCommande.equals(id.typeAttenduCommande) && Constantes.equals(getIdMagasin(), id.getIdMagasin())
        && Constantes.equals(idArticle, id.idArticle) && Constantes.equals(idLigneVente, id.idLigneVente)
        && Constantes.equals(idLigneAchat, id.idLigneAchat);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdStockAttenduCommande)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdStockAttenduCommande id = (IdStockAttenduCommande) pObject;
    
    int comparaison = typeAttenduCommande.compareTo(id.typeAttenduCommande);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idMagasin.compareTo(idMagasin);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idArticle.compareTo(idArticle);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idLigneVente.compareTo(id.idLigneVente);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idLigneAchat.compareTo(id.idLigneAchat);
    if (comparaison != 0) {
      return comparaison;
    }
    return 0;
  }
  
  @Override
  public String getTexte() {
    return typeAttenduCommande.toString() + SEPARATEUR_ID + idMagasin + SEPARATEUR_ID + idArticle + SEPARATEUR_ID + idLigneVente
        + SEPARATEUR_ID + idLigneAchat;
  }
  
  /**
   * Retourner le type de l'attendu/commandé.
   * @return Type de l'attendu/commandé.
   */
  public EnumTypeAttenduCommande getTypeAttenduCommande() {
    return typeAttenduCommande;
  }
  
  /**
   * Identifiant de l'établissement.
   * @return Identifiant de l'établissement (peut être null).
   */
  public IdEtablissement getIdEtablissement() {
    if (idMagasin == null) {
      return null;
    }
    return idMagasin.getIdEtablissement();
  }
  
  /**
   * Identifiant du magasin.
   * @return Identifiant du magasin (peut être null).
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Identifiant de l'article.
   * @return Identifiant de l'article.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Identifiant de ligne de document de vente.
   * @return Identifiant de ligne du document de vente (peut être null).
   */
  public IdLigneVente getIdLigneVente() {
    return idLigneVente;
  }
  
  /**
   * Identifiant de ligne de document d'achat.
   * @return Identifiant de ligne du document d'achat (peut être null).
   */
  public IdLigneAchat getIdLigneAchat() {
    return idLigneAchat;
  }
  
  /**
   * Retourner le numéro du document attendu/commandé.
   * @return Numéro du document de vente ou d'achat (peut être null).
   */
  public Integer getNumeroDocument() {
    if (isCommande() && getIdLigneVente() != null) {
      return getIdLigneVente().getNumero();
    }
    else if (isAttendu() && getIdLigneAchat() != null) {
      return getIdLigneAchat().getNumero();
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourner le suffixe du document attendu/commandé.
   * @return Suffixe du document de vente ou d'achat (peut être null).
   */
  public Integer getSuffixeDocument() {
    if (isCommande() && getIdLigneVente() != null) {
      return getIdLigneVente().getSuffixe();
    }
    else if (isAttendu() && getIdLigneAchat() != null) {
      return getIdLigneAchat().getSuffixe();
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourner le numéro de ligne de l'attendu/commandé.
   * @return Numéro de ligne de l'attendu/commandé (peut être null).
   */
  public Integer getNumeroLigne() {
    if (isCommande() && getIdLigneVente() != null) {
      return getIdLigneVente().getNumeroLigne();
    }
    else if (isAttendu() && getIdLigneAchat() != null) {
      return getIdLigneAchat().getNumeroLigne();
    }
    else {
      return null;
    }
  }
  
  /**
   * Indiquer si l'identifiant correspond à un commandé.
   * @return true=commandé, false=sinon.
   */
  public boolean isCommande() {
    return typeAttenduCommande == EnumTypeAttenduCommande.COMMANDE;
  }
  
  /**
   * Indiquer si l'identifiant correspond à un attendu.
   * @return true=attendu, false=sinon.
   */
  public boolean isAttendu() {
    return typeAttenduCommande == EnumTypeAttenduCommande.ATTENDU;
  }
  
  /**
   * Indiquer si l'identifiant correspond à la date du jour.
   * @return true=date du jour, false=sinon.
   */
  public boolean isDateJour() {
    return typeAttenduCommande == EnumTypeAttenduCommande.DATE_JOUR;
  }
  
  /**
   * Indiquer si l'identifiant correspond à la date minimum de réapprovisionnement.
   * @return true=date minimum de réapprovisionnement, false=sinon.
   */
  public boolean isDateMiniReappro() {
    return typeAttenduCommande == EnumTypeAttenduCommande.DATE_MINI_REAPPRO;
  }
}
