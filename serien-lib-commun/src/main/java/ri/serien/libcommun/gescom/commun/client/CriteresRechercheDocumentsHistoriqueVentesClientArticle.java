/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;

public class CriteresRechercheDocumentsHistoriqueVentesClientArticle extends CriteresBaseRecherche {
  public static final String TYPE_DEVIS = "DEV";
  public static final String TYPE_CHANTIER = "*CH";
  public static final String TYPE_LIVRAISON_RETRAIT = "EXP";
  public static final String TYPE_COMMANDE = "CMD";
  public static final String TYPE_FACTURE = "FAC";
  
  // Variables
  private String type = "";
  
  public String getType() {
    return type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
  
}
