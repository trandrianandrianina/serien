/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.modeexpedition;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de modes d'expéditions.
 */
public class ListeModeExpedition extends ListeClasseMetier<IdModeExpedition, ModeExpedition, ListeModeExpedition> {
  /**
   * Constructeur.
   */
  public ListeModeExpedition() {
  }
  
  @Override
  public ListeModeExpedition charger(IdSession pIdSession, List<IdModeExpedition> pListeId) {
    return null;
  }
  
  /**
   * Charger tous les magasins actifs de l'établissement donné.
   */
  @Override
  public ListeModeExpedition charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas défini.");
    }
    
    CriteresModeExpedition critereModeExpedition = new CriteresModeExpedition();
    critereModeExpedition.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeModeExpedition(pIdSession, critereModeExpedition);
  }
  
  /**
   * Charger tous les acheteurs de l'établissement donné.
   */
  public static ListeModeExpedition charger(IdSession pIdSession) {
    CriteresModeExpedition criteres = new CriteresModeExpedition();
    criteres.setTypeRecherche(CriteresModeExpedition.RECHERCHE_COMPTOIR);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    return ManagerServiceParametre.chargerListeModeExpedition(pIdSession, criteres);
  }
  
  /**
   * Retourner le mode d'expédition suivant son identifiant.
   */
  public ModeExpedition getModeExpeditionParId(IdModeExpedition pIdModeExpedition) {
    if (pIdModeExpedition == null) {
      return null;
    }
    
    for (ModeExpedition modeExpedition : this) {
      if (modeExpedition.getId().equals(pIdModeExpedition)) {
        return modeExpedition;
      }
    }
    return null;
  }
  
  /**
   * Retourner le mode d'expédition de livraison.
   */
  public ModeExpedition getModeExpeditionLivraison() {
    for (ModeExpedition modeExpedition : this) {
      if (modeExpedition.getId().isLivraison()) {
        return modeExpedition;
      }
    }
    return null;
  }
  
  /**
   * Retourner le mode d'expédition de enlevement
   */
  public ModeExpedition getModeExpeditionEnlevement() {
    for (ModeExpedition modeExpedition : this) {
      if (modeExpedition.getId().isEnlevement()) {
        return modeExpedition;
      }
    }
    return null;
  }
  
}
