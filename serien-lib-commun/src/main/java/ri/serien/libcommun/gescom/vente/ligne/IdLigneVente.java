/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'une ligne de document de ventes.
 * 
 * L'identifiant est composé de l'identiant de l'établissement et du numéro de ligne dans le document.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdLigneVente extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_ENTETE = 1;
  public static final int LONGUEUR_NUMERO = 6;
  public static final int LONGUEUR_SUFFIXE = 1;
  public static final int LONGUEUR_NUMERO_LIGNE = 4;
  
  // Variables
  private final EnumCodeEnteteDocumentVente codeEntete;
  private final Integer numero;
  private final Integer suffixe;
  private final Integer numeroLigne;
  
  /**
   * Constructeur.
   */
  private IdLigneVente(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement,
      EnumCodeEnteteDocumentVente pCodeEntete, Integer pNumero, Integer pSuffixe, Integer pNumeroLigne) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    codeEntete = controlerCodeEnteteDocumentVente(pCodeEntete);
    numero = controlerNumero(pNumero);
    suffixe = controlerSuffixe(pSuffixe);
    numeroLigne = controlerNumeroLigne(pNumeroLigne);
  }
  
  /**
   * Créer un identifiant à partir de l'id établissement, du code entête, du numéro et du suffixe du document et le numéro de ligne.
   */
  public static IdLigneVente getInstance(IdEtablissement pIdEtablissement, EnumCodeEnteteDocumentVente pCodeEntete, Integer pNumero,
      Integer pSuffixe, Integer pNumeroLigne) {
    return new IdLigneVente(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCodeEntete, pNumero, pSuffixe, pNumeroLigne);
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du code entête du document.
   */
  private static EnumCodeEnteteDocumentVente controlerCodeEnteteDocumentVente(EnumCodeEnteteDocumentVente pValeur) {
    // Renseigner l'indicatif tiers
    if (pValeur == null) {
      throw new MessageErreurException("Le code entête de la ligne de vente du document n'est pas renseigné.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du numéro du document.
   * Il doit être supérieur à zéro et doit comporter au maximum 6 chiffres (entre 1 et 999 999).
   */
  private Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de la ligne de vente du document n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro de la ligne de vente du document est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro de la ligne de vente du document est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du suffixe du document.
   * Il doit être supérieur ou égal à zéro et doit comporter au maximum 1 chiffre (entre 0 et 9).
   */
  private Integer controlerSuffixe(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le suffixe de la ligne de vente du document n'est pas renseigné.");
    }
    if (pValeur < 0 || pValeur > 9) {
      throw new MessageErreurException("Le suffixe de la ligne de vente du document soit être compris entre 0 et 9 inclus.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_SUFFIXE)) {
      throw new MessageErreurException("Le suffixe de la ligne de vente du document est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du numéro de ligne.
   * Le numéro de ligne est compris entre 0 et 9 999.
   */
  private Integer controlerNumeroLigne(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de ligne de la ligne de vente n'est pas renseigné.");
    }
    if (pValeur < 0) {
      throw new MessageErreurException("Le numéro de ligne de la ligne de vente est inférieur à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO_LIGNE)) {
      throw new MessageErreurException("Le numéro de ligne de la ligne de vente est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  // -- Méthodes publiques
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdLigneVente controlerId(IdLigneVente pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de la ligne de vente est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de la ligne de vente n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getIdEtablissement().hashCode();
    code = 37 * code + codeEntete.hashCode();
    code = 37 * code + numero.hashCode();
    code = 37 * code + suffixe.hashCode();
    code = 37 * code + numeroLigne.hashCode();
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdLigneVente)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de ligne de vente.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdLigneVente id = (IdLigneVente) pObject;
    return getIdEtablissement().equals(id.getIdEtablissement()) && codeEntete.equals(id.codeEntete) && numero.equals(id.numero)
        && suffixe.equals(id.suffixe) && numeroLigne.equals(id.numeroLigne);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdLigneVente)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdLigneVente id = (IdLigneVente) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = codeEntete.compareTo(id.codeEntete);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = numero.compareTo(id.numero);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = suffixe.compareTo(id.suffixe);
    if (comparaison != 0) {
      return comparaison;
    }
    return numeroLigne.compareTo(id.numeroLigne);
  }
  
  /**
   * Retourner l'affichage standard de l'identifiant d'une ligne de vente.
   * 
   * L'affichage standard est celui qui doit être utilisé dans l'essentiel des situations pour désigner une ligne de ventes.
   * Le format choisi est "NuméroDocument-Suffixe-NuméroLigne".
   * L'établissement n'est pas précisé car il est implicite dans la majorité des contextes. L'entête n'est pas non plus précisé car on
   * part du principe que l'on sait si l'identifiant concerne un devis, un bon, une commande ou une facture. On considère que ce sont
   * des espaces de numérotation distinct (au même titre que client et fournisseur par exemple).
   */
  @Override
  public String getTexte() {
    return "" + getNumero().intValue() + SEPARATEUR_ID + getSuffixe() + SEPARATEUR_ID + numeroLigne;
  }
  
  /**
   * Retourne l'identifiant du document de vente à partir de l'identifiant de la ligne.
   * 
   * @return
   */
  public IdDocumentVente getIdDocumentVente() {
    if (isExistant()) {
      return IdDocumentVente.getInstanceHorsFacture(getIdEtablissement(), codeEntete, numero, suffixe);
    }
    return IdDocumentVente.getInstanceAvecCreationId(getIdEtablissement(), codeEntete);
  }
  
  // -- Accesseurs
  
  /**
   * Entête du document contenant la ligne.
   */
  public EnumCodeEnteteDocumentVente getCodeEntete() {
    return codeEntete;
  }
  
  /**
   * Numéro du document contenant la ligne.
   * Le numéro du document est compris entre entre 0 et 9 999.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Suffixe du document contenant la ligne.
   */
  public Integer getSuffixe() {
    return suffixe;
  }
  
  /**
   * Numéro de ligne au sein du document.
   */
  public Integer getNumeroLigne() {
    return numeroLigne;
  }
  
  /**
   * Indicatif de l'id de la ligne de vente simplifié pour affichage.
   */
  public String getIndicatifSimplifie() {
    return "" + getNumero().intValue() + SEPARATEUR_ID + getSuffixe() + SEPARATEUR_ID + numeroLigne;
  }
  
  /**
   * Indicatif de l'id de la ligne de vente en fonction de l'indicatif souhaité.
   */
  public String getIndicatif() {
    return String.format(
        "%c%" + LONGUEUR_CODE_ETABLISSEMENT + "s%0" + LONGUEUR_NUMERO + "d%0" + LONGUEUR_SUFFIXE + "d%0" + LONGUEUR_NUMERO_LIGNE + "d",
        codeEntete.getCode(), getCodeEtablissement(), numero, suffixe, numeroLigne);
  }
  
  /**
   * Indicatif de l'id de la ligne de vente en fonction de l'indicatif souhaité pour traitement par un service RPG.
   */
  public String getIndicatifPourTraitement() {
    return String.format(
        "%c%" + LONGUEUR_CODE_ETABLISSEMENT + "s%0" + LONGUEUR_NUMERO + "d%0" + LONGUEUR_SUFFIXE + "d%0" + LONGUEUR_NUMERO_LIGNE + "d",
        codeEntete.getCode(), getCodeEtablissement(), numero, suffixe, numeroLigne);
  }
}
