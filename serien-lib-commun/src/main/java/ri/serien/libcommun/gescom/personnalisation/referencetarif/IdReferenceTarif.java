/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.referencetarif;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une référence tarif.
 *
 * L'identifiant est composé du code établissement et du code référence tarif.
 * La référence tarif correspond au paramètre TA. Le code est constitué de 1 à 5 caractères et se présente sous la forme AXXXX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation. *
 */
public class IdReferenceTarif extends AbstractIdAvecEtablissement {
  private static final int LONGUEUR_CODE_MIN = 1;
  private static final int LONGUEUR_CODE_MAX = 5;
  private static final String REFERENCE_TARIF_CONSIGNE = "*CONS";
  
  // Variables
  private final String code;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdReferenceTarif(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdReferenceTarif getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new IdReferenceTarif(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdReferenceTarif controlerId(IdReferenceTarif pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de la référence tarif est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de la référence tarif n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdReferenceTarif)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants " + getClass().getSimpleName());
    }
    IdReferenceTarif id = (IdReferenceTarif) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdReferenceTarif)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdReferenceTarif id = (IdReferenceTarif) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Contrôler la validité de la référence tarif.
   * Le code acheteur est une chaîne alphanumérique de 1 à 3 caractères.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("La référence tarif n'est pas renseignée.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MAX) {
      throw new MessageErreurException(
          "La référence tarif doit comporter entre " + LONGUEUR_CODE_MIN + " et " + LONGUEUR_CODE_MAX + " caractères : " + valeur);
    }
    return valeur;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Code référence tarif.
   * La référence tarif est une chaîne alphanumérique de 1 à 5 caractères.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Indiquer si la référence tarif correspond à celle d'un article consigné.
   *
   * Un article consigné ne suit pas la logique tarifaire habituelle. Le tarif sert uniquement à définir 2 prix : le prix consigné qui
   * est dans la colonne 1 et le prix déconsigné qui est dans la colonne 2. Ce sont les prix de base de l'article. La notion de
   * colonnes de tarifs n'est donc pas utilisé ici.
   * 
   * @return true=article consigné, false=article non consigné.
   */
  public boolean isConsigne() {
    return code.equals(REFERENCE_TARIF_CONSIGNE);
  }
}
