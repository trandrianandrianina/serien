/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockcomptoir;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant d'un stock.
 *
 * L'identifiant est composé de l'identifiant d'un article et d'un magasin car le stock est ventilé par article et par magasin.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdStockComptoir extends AbstractIdAvecEtablissement {
  private IdArticle idArticle;
  private IdMagasin idMagasin;
  
  /**
   * Constructeur interne.
   */
  private IdStockComptoir(EnumEtatObjetMetier pEnumEtatObjetMetier, IdArticle pIdArticle, IdMagasin pIdMagasin) {
    super(pEnumEtatObjetMetier, pIdArticle.getIdEtablissement());
    
    // Contrôler les identifiants
    IdArticle.controlerId(pIdArticle, false);
    IdMagasin.controlerId(pIdMagasin, false);
    if (!pIdArticle.getIdEtablissement().equals(pIdMagasin.getIdEtablissement())) {
      throw new MessageErreurException("Stock invalide car les établissements de l'article et du magasin sont différents.");
    }
    
    // Renseigner les attributs
    idArticle = pIdArticle;
    idMagasin = pIdMagasin;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdStockComptoir getInstance(IdArticle pIdArticle, IdMagasin pIdMagasin) {
    return new IdStockComptoir(EnumEtatObjetMetier.MODIFIE, pIdArticle, pIdMagasin);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + idArticle.hashCode();
    code = 37 * code + idMagasin.hashCode();
    return code;
  }
  
  /**
   * Comparer 2 id.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdStockComptoir)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de stock.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdStockComptoir id = (IdStockComptoir) pObject;
    return idArticle.equals(id.idArticle) && idMagasin.equals(id.idMagasin);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdStockComptoir)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdStockComptoir id = (IdStockComptoir) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idMagasin.compareTo(id.idMagasin);
    if (comparaison != 0) {
      return comparaison;
    }
    return idArticle.compareTo(id.idArticle);
  }
  
  @Override
  public String getTexte() {
    return idArticle.toString() + SEPARATEUR_ID + idMagasin;
  }
  
  // -- Accesseurs
  
  /**
   * Identifiant de l'article associé au stock.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Code du magasin associé au stock.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
}
