/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Cette classe décrit l'ensemble des filtres qui peuvent être appliqués lors de la recherche d'un fournisseur de base
 */
public class FiltreFournisseurBase implements Serializable {
  private IdEtablissement idEtablissement = null;
  
  /**
   * retourner l'id établissement du filtre de recherche des fournisseurs
   * Si cette valeur n'est pas renseignée (NULL) on ne recherche pas sur l'établissement
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Renseigner l'id établissement du filtre de recherche des fournisseurs
   * Si cette valeur n'est pas renseignée (NULL) on ne recherche pas sur l'établissement
   */
  public void setIdEtablissement(IdEtablissement idEtablissement) {
    this.idEtablissement = idEtablissement;
  }
  
}
