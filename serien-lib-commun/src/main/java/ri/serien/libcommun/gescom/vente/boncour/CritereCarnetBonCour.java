/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.boncour;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;

/**
 * Cette classe contient un ensemble de critères qui permettent la recherche de carnet de bons de cour.
 */
public class CritereCarnetBonCour implements Serializable {
  // Variables
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private Integer numeroCarnet = null;
  private IdVendeur idMagasinier = null;
  private Integer numeroBonDeCour = null;
  
  // -- Accesseurs
  
  /**
   * Identifiant de l'établissement
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Identifiant de l'établissement
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Identifiant du magasin
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Identifiant du magasin
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  public Integer getNumeroCarnet() {
    return numeroCarnet;
  }
  
  public void setNumeroCarnet(Integer numeroCarnet) {
    this.numeroCarnet = numeroCarnet;
  }
  
  /**
   * Magasinier qui détien le carnet de bons de cour
   */
  public IdVendeur getIdMagasinier() {
    return idMagasinier;
  }
  
  /**
   * Magasinier qui détien le carnet de bons de cour
   */
  public void setIdMagasinier(IdVendeur pIdMagasinier) {
    idMagasinier = pIdMagasinier;
  }
  
  public Integer getNumeroBonDeCour() {
    return numeroBonDeCour;
  }
  
  public void setNumeroBonDeCour(Integer numeroBonDeCour) {
    this.numeroBonDeCour = numeroBonDeCour;
  }
  
}
