/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;

/**
 * Liste des FournisseurBase.
 * L'utilisation de cette classe est à privilégiée dès qu'on manipule une liste de FournisseurBase. Elle contient toutes les méthodes
 * oeuvrant sur la liste tandis que la classe FournisseurBase contient les méthodes oeuvrant sur un seul FournisseurBase.
 */
public class ListeFournisseurBase extends ListeClasseMetier<IdFournisseur, FournisseurBase, ListeFournisseurBase> {
  
  /**
   * Constructeurs.
   */
  public ListeFournisseurBase() {
  }
  
  /**
   * Charger touts les fournisseurs suivant une liste d'IdFournisseur.
   */
  @Override
  public ListeFournisseurBase charger(IdSession pIdSession, List<IdFournisseur> pListeId) {
    if (pListeId == null) {
      throw new MessageErreurException("La liste d'intentifiant est vide.");
    }
    
    return ManagerServiceFournisseur.chargerListeFournisseurBase(pIdSession, pListeId);
  }
  
  /**
   * Charger tous les fournisseurs de l'établissement donné.
   */
  @Override
  public ListeFournisseurBase charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null || !pIdEtablissement.isExistant()) {
      throw new MessageErreurException("L'établissement est invalide.");
    }
    
    FiltreFournisseurBase filtres = new FiltreFournisseurBase();
    filtres.setIdEtablissement(pIdEtablissement);
    
    return ManagerServiceFournisseur.chargerListeFournisseurBaseParEtablissement(pIdSession, filtres);
  }
  
  /**
   * Trier la liste par la raison sociale du fournisseur (ordre alphabétique croissant).
   */
  public void trierParRaisonSociale() {
    Collections.sort(this, new Comparator<FournisseurBase>() {
      @Override
      public int compare(FournisseurBase o1, FournisseurBase o2) {
        return o1.getRaisonSociale().compareTo(o2.getRaisonSociale());
      }
    });
  }
  
  /**
   * Trier les fournisseur suivant leurs identifiants
   */
  public void trierParId() {
    Collections.sort(this, new Comparator<FournisseurBase>() {
      @Override
      public int compare(FournisseurBase o1, FournisseurBase o2) {
        int collectif1 = o1.getId().getCollectif();
        int collectif2 = o2.getId().getCollectif();
        int numero1 = o1.getId().getNumero();
        int numero2 = o2.getId().getNumero();
        
        if (collectif1 > collectif2) {
          return 1;
        }
        else if (collectif1 < collectif2) {
          return -1;
        }
        else {
          if (numero1 > numero2) {
            return 1;
          }
          else if (numero1 < numero2) {
            return -1;
          }
          else {
            return 0;
          }
        }
      }
    });
    
  }
  
  /**
   * Construire une liste de fournisseur de base correspondant à la liste d'identifiants fournie en paramètre.
   * Les données ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeFournisseurBase creerListeNonChargee(List<IdFournisseur> pListeIdFournisseur) {
    ListeFournisseurBase listeFournisseurBase = new ListeFournisseurBase();
    if (pListeIdFournisseur != null) {
      for (IdFournisseur idFourniseur : pListeIdFournisseur) {
        FournisseurBase fournisseurBase = new FournisseurBase(idFourniseur);
        fournisseurBase.setCharge(false);
        listeFournisseurBase.add(fournisseurBase);
      }
    }
    return listeFournisseurBase;
  }
  
}
