/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact.liencontact;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceCommun;

/**
 * Liste de LienContact.
 */
public class ListeLienContact extends ListeClasseMetier<IdLienContact, LienContact, ListeLienContact> {
  /**
   * Constructeur.
   */
  public ListeLienContact() {
  }
  
  /**
   * Construire une liste de contacts correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des contacts ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeLienContact creerListeNonChargee(List<IdLienContact> pListeIdLienContact) {
    ListeLienContact listeLienContact = new ListeLienContact();
    if (pListeIdLienContact != null) {
      for (IdLienContact idLienContact : pListeIdLienContact) {
        LienContact lienContact = new LienContact(idLienContact);
        lienContact.setCharge(false);
        listeLienContact.add(lienContact);
      }
    }
    return listeLienContact;
  }
  
  /**
   * Charge les contacts à partir des id.
   */
  @Override
  public ListeLienContact charger(IdSession pIdSession, List<IdLienContact> pListeId) {
    return ManagerServiceCommun.chargerListeLienContact(pIdSession, pListeId);
  }
  
  /**
   * Charge les contacts d'un établissement
   */
  @Override
  public ListeLienContact charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charge tous les LienContact d'une liste d'identifiants contacts donnée.
   */
  public ListeLienContact chargerListeLienContact(IdSession pIdSession, List<IdContact> pListeId) {
    if (pListeId == null) {
      return null;
    }
    return ManagerServiceCommun.chargerListeLienContactTiers(pIdSession, pListeId);
  }
  
  /**
   * Charge tous les LienContact d'un contact donnée.
   * 
   */
  public static ListeLienContact charger(IdSession pIdSession, IdContact pIdContact) {
    if (pIdContact == null) {
      return null;
    }
    return ManagerServiceCommun.chargerListeLienContactTiers(pIdSession, pIdContact);
  }
  
  /**
   * Retourner tous les liens d'un contact
   * 
   * @param pNumeroContact numéro du contact pour lequel on filtre les liens
   * @return une ListeLienContact ne contenant que les liens du contact
   */
  public ListeLienContact retournerLienContact(int pNumeroContact) {
    ListeLienContact listeFiltree = new ListeLienContact();
    for (LienContact lien : this) {
      if (pNumeroContact == lien.getId().getNumeroContact()) {
        listeFiltree.add(lien);
      }
    }
    return listeFiltree;
  }
  
  /**
   * Retourner le lien du contact au client passé en paramètre, si ce lien existe
   * 
   * @param pIdClient
   * @return LienContact si le lien avec le client existe, null si le lien n'existe pas
   */
  public LienContact getLien(IdClient pIdClient) {
    // Parcourir les liens pour voir s'il existe un lien client
    for (LienContact lienContact : this) {
      if (lienContact.getId().isLienClient() && lienContact.getId().getIdClient().equals(pIdClient)) {
        return lienContact;
      }
    }
    return null;
  }
  
  /**
   * Retourner le lien du contact au fournisseur passé en paramètre, si ce lien existe
   * 
   * @param pIdFournisseur
   * @return LienContact si le lien avec le fournisseur existe, null si le lien n'existe pas
   */
  public LienContact getLien(IdFournisseur pIdFournisseur) {
    // Parcourir les liens pour voir s'il existe un lien fournisseur
    for (LienContact lienContact : this) {
      if (lienContact.getId().isLienFournisseur() && lienContact.getId().getIdFournisseur().equals(pIdFournisseur)) {
        return lienContact;
      }
    }
    return null;
  }
  
  /**
   * Vérifie l'égalité de deux ListeLienContact
   */
  public boolean isSimilaire(Object pObjet) {
    if (!(pObjet instanceof ListeLienContact)) {
      return false;
    }
    
    ListeLienContact ancienListeLienContact = (ListeLienContact) pObjet;
    
    // Vérifier les changements avant d'entamer
    if (ancienListeLienContact.isEmpty()) {
      return false;
    }
    // S'assurer que le nombre de liens est le même
    if (ancienListeLienContact.size() != this.size()) {
      return false;
    }
    // Nombre de lien similaire à l'ancienne valeur
    int nombreLienSimilaire = 0;
    for (LienContact ancienLienContact : ancienListeLienContact) {
      if (ancienLienContact.getId() != null && ancienLienContact.getId().getIdClient() != null) {
        if (verifierSimilitudeLienContact(ancienLienContact)) {
          nombreLienSimilaire++;
        }
      }
    }
    // Sortir du traitement si les liens n'ont pas changé
    if (this.size() == nombreLienSimilaire) {
      return true;
    }
    
    return false;
  }
  
  /**
   * Vérifie l'existance et la similitude d'un lienContact dans la nouvelle liste
   */
  private boolean verifierSimilitudeLienContact(LienContact pAncienLienContact) {
    for (LienContact nouveauLienContact : this) {
      if (nouveauLienContact.getId() != null && nouveauLienContact.getId().getIdClient() != null) {
        // Identifier le lien et vérifier si il est devenu Principal ou non par rapport à l'ancienne valeur
        if (nouveauLienContact.getId().getIdClient().getNumero() == pAncienLienContact.getId().getIdClient().getNumero()
            && nouveauLienContact.isContactPrincipal() == pAncienLienContact.isContactPrincipal()) {
          return true;
        }
      }
    }
    
    return false;
  }
}
