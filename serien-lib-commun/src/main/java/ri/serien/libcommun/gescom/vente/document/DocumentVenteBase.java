/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.IdModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceClient;

/**
 * Cette classe contient les données minimales nécessaires à l'affichage d'un document de vente dans une liste.
 */
public class DocumentVenteBase extends AbstractClasseMetier<IdDocumentVente> {
  public static final int LONGUEUR_REFERENCE_LONGUE = 25;
  private IdMagasin idMagasin = null;
  private IdVendeur idVendeur = null;
  private IdClient idClientFacture = null;
  private EnumTypeDocumentVente typeDocumentVente = null;
  private EnumEtatBonDocumentVente etat = null;
  private EnumEtatDevisDocumentVente etatDevis = null;
  private EnumEtatExtractionDocumentVente etatExtraction = null;
  private boolean verrouille = false;
  private Date dateCreation = null;
  private Date dateValiditeDevis = null;
  private Date dateRelanceDevis = null;
  private Date dateValidationCommande = null;
  private Date dateExpeditionBon = null;
  private Date dateFacturation = null;
  private IdModeExpedition idModeExpedition = null;
  private Boolean directUsine = false;
  private Boolean monoFournisseur = true;
  private String referenceLongue = null;
  private boolean generationImmediateCommandeAchat = false;
  private boolean commandeAchatGeneree = false;
  private BigDecimal totalHT = null;
  private BigDecimal totalTTC = null;
  private boolean modifiable = true;
  private IdChantier idChantier = null;
  private String libelleChantier = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pIdDocumentVente Identifiant du document de ventes.
   */
  public DocumentVenteBase(IdDocumentVente pIdDocumentVente) {
    super(pIdDocumentVente);
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public String getTexte() {
    // Retourner le texte de l'identifiant à défaut de mieux car je ne vois pas quel libellé pertinent on a pour l'instant.
    return id.getTexte();
  }
  
  @Override
  public DocumentVenteBase clone() {
    DocumentVenteBase documentVenteBase = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      documentVenteBase = (DocumentVenteBase) super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return documentVenteBase;
  }
  
  /**
   * Retourne l'état du chargement de données.<br>
   * La méthode se base sur la date de création du document de ventes qui est une donnée obligatoire. Si celle-ci est
   * présente, on considère que les données sont chargées.
   * @return
   *         true si les données ont été chargées, false sinon.
   */
  @Override
  public boolean isCharge() {
    return dateCreation != null;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Comparer les données de deux documents de ventes base.
   * @param pDocumentVenteBase Document de ventes de base.
   * @return L'égalité entre l'objet en paramètre.
   */
  public boolean equalsComplet(DocumentVenteBase pDocumentVenteBase) {
    // Tester si l'objet est nous-même (optimisation)
    if (pDocumentVenteBase == this) {
      return true;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (pDocumentVenteBase == null) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    boolean retour = true;
    retour &= Constantes.equals(getId().getCodeEtablissement(), pDocumentVenteBase.getId().getCodeEtablissement());
    retour &= Constantes.equals(getTotalHT(), pDocumentVenteBase.getTotalHT());
    retour &= Constantes.equals(getTotalTTC(), pDocumentVenteBase.getTotalTTC());
    retour &= isLivraison() == pDocumentVenteBase.isLivraison();
    retour &= Constantes.equals(getReferenceLongue(), pDocumentVenteBase.getReferenceLongue());
    retour &= Constantes.equals(getDateCreation(), pDocumentVenteBase.getDateCreation());
    return retour;
  }
  
  /**
   * Charger le client facturé du document.
   * @param pIdSession Identifiant de la session.
   * @return Le client facturé.
   */
  public Client chargerClientFacture(IdSession pIdSession) {
    return ManagerServiceClient.chargerClient(pIdSession, idClientFacture);
  }
  
  /**
   * Confirmer si le document est un devis.
   * @return
   *         true si c'est un devis, false sinon.
   */
  public boolean isDevis() {
    return typeDocumentVente != null && typeDocumentVente.equals(EnumTypeDocumentVente.DEVIS);
  }
  
  /**
   * Confirmer si le document est une commande.
   * @return
   *         true si c'est une commande, false sinon.
   */
  public boolean isCommande() {
    return typeDocumentVente != null && typeDocumentVente.equals(EnumTypeDocumentVente.COMMANDE);
  }
  
  /**
   * Confirmer si le document est un bon.
   * @return
   *         true si c'est un bon, false sinon.
   */
  public boolean isBon() {
    return typeDocumentVente != null && typeDocumentVente.equals(EnumTypeDocumentVente.BON);
  }
  
  /**
   * Confirmer si le document est une facture.
   * @return
   *         true si c'est une facture, false sinon.
   */
  public boolean isFacture() {
    return typeDocumentVente != null && typeDocumentVente.equals(EnumTypeDocumentVente.FACTURE);
  }
  
  /**
   * Renseigner le type de document sur la base des informations du document.<br>
   * Remarque : le code de cette méthode doit être cohérent avec celui des méthodes getRequeteWhere() de la classe
   * CritereDocumentVente.<br><br>
   * Règles de gestion :<br>
   * - On considère comme devis tout document de ventes avec un entête de type 'D' et qui n'est pas un chantier.<br>
   * - On considère comme commande tout document de ventes avec un entête de type 'E' qui n'est ni un bon ni une
   * facture. Un document de
   * ventes avec le statut "En attente" n'a aucune date de renseigné et est donc considéré comme une commande.<br>
   * - On considère comme bon tout document de ventes avec un entête de type 'E' qui a une date d'expédition.<br>
   * - On considère comme facture tout document de ventes possédant un numéro de facture et une date de facture.
   */
  public void deduireTypeDocumentVente() {
    // Tester si c'est d'un devis. Un devis doit avoir :
    // - un entête 'D'
    if (id.isIdDevis()) {
      typeDocumentVente = EnumTypeDocumentVente.DEVIS;
    }
    // Tester si c'est une commande. Une commande doit avoir :
    // - un entête 'E'
    // - aucune date d'expédition (ce sont des bons dans ce cas)
    // - aucune date de facture (ce sont des factures directes dans ce cas)
    else if (id.isIdCommandeOuBon() && dateExpeditionBon == null && dateFacturation == null) {
      typeDocumentVente = EnumTypeDocumentVente.COMMANDE;
    }
    // Tester si c'est un bon. Un bon doit avoir :
    // - un entête 'E'
    // - une date d'expédition renseignée (E1EXP)
    else if (id.isIdCommandeOuBon() && dateExpeditionBon != null) {
      typeDocumentVente = EnumTypeDocumentVente.BON;
    }
    // Tester si c'est une facture. Une facture doit avoir :
    // - un numéro de facture (E1NUM)
    // - une date de facture (E1FAC)
    else if (id.isIdFacture() && dateFacturation != null) {
      typeDocumentVente = EnumTypeDocumentVente.FACTURE;
    }
    // Mettre "Non définit" dans les autres cas
    else {
      typeDocumentVente = EnumTypeDocumentVente.NON_DEFINI;
    }
  }
  
  /**
   * Retourner l'état du document si c'est en cours de création.
   * @return
   *         true si c'est en cours, false sinon.
   */
  public boolean isEnCoursCreation() {
    if (isDevis()) {
      return etatDevis != null && etatDevis.equals(EnumEtatDevisDocumentVente.ATTENTE);
    }
    else {
      return etat != null && etat.equals(EnumEtatBonDocumentVente.ATTENTE);
    }
  }
  
  /**
   * Indiquer si le document de ventes est associé à un chantier.
   * @return
   *         true si c'est pour un chantier, false sinon.
   */
  public boolean isPourChantier() {
    return idChantier != null;
  }
  
  /**
   * Retourner le libellé de l'état du document.<br>
   * Le libellé n'est pas lu dans la même valeur en fonction du type du document.
   * @return Le libellé de l'état.
   */
  public String getLibelleEtat() {
    if (isDevis() && getEtatDevis() != null) {
      return getEtatDevis().getLibelle();
    }
    else if (!isDevis() && getEtat() != null) {
      return getEtat().getLibelle();
    }
    else {
      return "";
    }
  }
  
  /**
   * Indiquer le mode d'expédition non défini.
   * @return
   *         true si le mode d'expédition est non défini, false sinon.
   */
  public boolean isModeExpeditionNonDefini() {
    return idModeExpedition == null || idModeExpedition.isNonDefini();
  }
  
  /**
   * Indiquer s'il s'agit d'un document livraison.
   * @return
   *         true si c'est un document de livraison, false sinon.
   */
  public boolean isLivraison() {
    if (idModeExpedition == null) {
      return false;
    }
    return idModeExpedition.isLivraison();
  }
  
  /**
   * Indiquer s'il s'agit d'un document enlèvement.
   * @return
   *         true si c'est un document enlèvement, false sinon.
   */
  public boolean isEnlevement() {
    if (idModeExpedition == null) {
      return false;
    }
    return idModeExpedition.isEnlevement();
  }
  
  /**
   * Retourne le libellé du document de ventes au format :<br>
   * [Trigramme du type de document]-[client]-[date de création] ([numéro]-[suffixe])
   * @return Le libellé du document de ventes.
   */
  public String getLibelle() {
    return typeDocumentVente.getCodeAlpha() + "-" + idClientFacture.getTexte() + "-" + Constantes.convertirDateEnTexte(dateCreation) + "("
        + id.getTexte() + ")";
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'identifiant du magasin du document de ventes.
   * @return L'identifiant du magasin.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier l'identifiant du magasin du document de ventes.
   * @param pIdMagasin Identifiant du magasin.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  /**
   * Retourner l'identifiant du vendeur ayant vendu le document.<br>
   * Le caractère obligatoire d'un vendeur dans un document dépend du paramètre PS020. Il est donc possible que
   * l'identifiant du vendeur soit null.
   * @return L'identifiant du vendeur.
   */
  public IdVendeur getIdVendeur() {
    return idVendeur;
  }
  
  /**
   * Renseigner l'identifiant vendeur du document.
   * @param pIdVendeur Identifiant du vendeur.
   */
  public void setIdVendeur(IdVendeur pIdVendeur) {
    idVendeur = pIdVendeur;
  }
  
  /**
   * Retourner l'identifiant du client facturé.
   * @return L'identifiant du vendeur.
   */
  public IdClient getIdClientFacture() {
    return idClientFacture;
  }
  
  /**
   * Modifier l'identifiant du client facturé.
   * @param pIdClientFacture Identifiant du client facturé.
   */
  public void setIdClientFacture(IdClient pIdClientFacture) {
    idClientFacture = pIdClientFacture;
  }
  
  /**
   * Retourner le type du document (EnumTypeDocumentVente).
   * @return Le type de document de vente.
   */
  public EnumTypeDocumentVente getTypeDocumentVente() {
    return typeDocumentVente;
  }
  
  /**
   * Modifier le type du document de ventes (EnumTypeDocumentVente).
   * @param pTypeDocumentVente Type de document de vente.
   */
  public void setTypeDocumentVente(EnumTypeDocumentVente pTypeDocumentVente) {
    typeDocumentVente = pTypeDocumentVente;
  }
  
  /**
   * Retourner l'état du devis (EnumEtatDevisDocumentVente).
   * @return L'état du devis.
   */
  public EnumEtatDevisDocumentVente getEtatDevis() {
    return etatDevis;
  }
  
  /**
   * Modifier l'état du devis (EnumEtatDevisDocumentVente).
   * @param pEtatDevis Etat du devis.
   */
  public void setEtatDevis(EnumEtatDevisDocumentVente pEtatDevis) {
    etatDevis = pEtatDevis;
  }
  
  /**
   * Retourner l'état du document si ce n'est pas un devis (EnumEtatBonDocumentVente).
   * @return Etat du bon.
   */
  public EnumEtatBonDocumentVente getEtat() {
    return etat;
  }
  
  /**
   * Modifier l'état du document si ce n'est pas un devis (EnumEtatBonDocumentVente).
   * @param pEtat Etat du bon.
   */
  public void setEtat(EnumEtatBonDocumentVente pEtat) {
    etat = pEtat;
  }
  
  /**
   * Retourner l'état d'extraction du document.
   * @return L'état d'extraction.
   */
  public EnumEtatExtractionDocumentVente getEtatExtraction() {
    return etatExtraction;
  }
  
  /**
   * Modifier l'état d'extraction du document.
   * @param pEtatExtraction Etat d'extraction.
   */
  public void setEtatExtraction(EnumEtatExtractionDocumentVente pEtatExtraction) {
    etatExtraction = pEtatExtraction;
  }
  
  /**
   * Retourner la date de création du document de ventes.
   * @return La date de création du document.
   */
  public Date getDateCreation() {
    return dateCreation;
  }
  
  /**
   * Modifier la date de création du document de ventes.
   * @param pDateCréation Date de création du document.
   */
  public void setDateCreation(Date pDateCreation) {
    dateCreation = pDateCreation;
  }
  
  /**
   * Retourner la date de validité du devis.
   * @return La date de validité du devis.
   */
  public Date getDateValiditeDevis() {
    return dateValiditeDevis;
  }
  
  /**
   * Modifier la date de validité du devis.
   * @param pDateValiditeDevis Date de validité du devis.
   */
  public void setDateValiditeDevis(Date pDateValiditeDevis) {
    dateValiditeDevis = pDateValiditeDevis;
  }
  
  /**
   * Retourner la date de relance du devis.
   * @return La date de relance du devis.
   */
  public Date getDateRelanceDevis() {
    return dateRelanceDevis;
  }
  
  /**
   * Modifier la date de relance du devis.
   * @param pDateRelanceDevis Date de relance du devis.
   */
  public void setDateRelanceDevis(Date pDateRelanceDevis) {
    dateRelanceDevis = pDateRelanceDevis;
  }
  
  /**
   * Retourner la date de validation de la commande.
   * Cette information n'a de sens que pour un document de ventes de type commande.
   * @return La date de validation de la commande.
   */
  public Date getDateValidationCommande() {
    return dateValidationCommande;
  }
  
  /**
   * Modifier la date de validation de la commande.
   * Cette information n'a de sens que pour un document de ventes de type commande.
   * @param pDateValidationCommande Date de validation de la commande.
   */
  public void setDateValidationCommande(Date pDateValidationCommande) {
    dateValidationCommande = pDateValidationCommande;
  }
  
  /**
   * Retourner la date d'expédition ou d'enlèvement du bon.
   * Cette information n'a de sens que pour un document de ventes de type bon.
   * @return La date d'expédition ou d'enlèvement du bon.
   */
  public Date getDateExpeditionBon() {
    return dateExpeditionBon;
  }
  
  /**
   * Modifier la date d'expédition ou d'enlèvement du bon.
   * Cette information n'a de sens que pour un document de ventes de type bon.
   * @param pDateExpeditionBon Date d'expédition du bon.
   */
  public void setDateExpeditionBon(Date pDateExpeditionBon) {
    dateExpeditionBon = pDateExpeditionBon;
  }
  
  /**
   * Retourner la date de facturation du document de ventes.
   * Cette information n'a de sens que pour un document de ventes de type facture.
   * @return La date de facturation.
   */
  public Date getDateFacturation() {
    return dateFacturation;
  }
  
  /**
   * Modifier la date de facturation.
   * Cette information n'a de sens que pour un document de ventes de type facture.
   * @param pDateFacturation Date de facturation.
   */
  public void setDateFacturation(Date pDateFacturation) {
    dateFacturation = pDateFacturation;
  }
  
  /**
   * Retourner l'identifiant du mode d'expédition (livraison ou enlèvement).
   * @return L'identifiant du mode d'expédition.
   */
  public IdModeExpedition geIdModeExpedition() {
    return idModeExpedition;
  }
  
  /**
   * Modifier l'identifiant du mode d'expédition (livraison ou enlèvement).
   * @param pModeExpedition Identifiant du mode d'expédition.
   */
  public void setIdModeExpedition(IdModeExpedition pModeExpedition) {
    idModeExpedition = pModeExpedition;
  }
  
  /**
   * Retourner la référence longue du document de ventes.<br>
   * La référence longue est une zone de saisie libre pour l'utilisateur. Le logiciel l'initialise parfois avec les
   * références d'autres documents.
   * @return La référence longue.
   */
  public String getReferenceLongue() {
    return referenceLongue;
  }
  
  /**
   * Modifier la référence longue du document de ventes.
   * La valeur est tronquée à 25 caractères et les espaces au début et à la fin sont enlevés.
   * @param pReferenceLongue Référence longue.
   */
  public void setReferenceLongue(String pReferenceLongue) {
    if (pReferenceLongue != null) {
      referenceLongue = Constantes.formaterStringMax(pReferenceLongue, LONGUEUR_REFERENCE_LONGUE, true);
    }
    else {
      referenceLongue = "";
    }
  }
  
  /**
   * Indiquer si le document est un direct usine.
   * @return
   *         true si c'est un direct usine, false sinon.
   */
  public boolean isDirectUsine() {
    return directUsine != null && directUsine;
  }
  
  /**
   * Modifier si le document est un direct usine
   * @param pDirectUsine Indicatif d'un document direct usine.
   */
  public void setDirectUsine(boolean pDirectUsine) {
    directUsine = pDirectUsine;
  }
  
  /**
   * Indiquer si la commande d'achats est générée lors de l'édition d'une commande de ventes.
   * Par sécurité, la méthode retour toujours faux lorsque le document de ventes n'est pas une commande.
   * @return
   *         true si c'est une commande générée lors de l'édition d'une commande de ventes, false sinon.
   */
  public boolean isGenerationImmediateCommandeAchat() {
    return generationImmediateCommandeAchat;
  }
  
  /**
   * Modifier la demande de génération d'une commande d'achats lors de l'édition d'une commande de ventes.
   * @param pGenerationImmediateCommandeAchat Indicatif de la génération immediate.
   */
  public void setGenerationImmediateCommandeAchat(boolean pGenerationImmediateCommandeAchat) {
    generationImmediateCommandeAchat = pGenerationImmediateCommandeAchat;
  }
  
  /**
   * Indiquer si une commande d'achat a été générée pour ce document.
   * @return
   *         true si une commande est générée pour ce document, false sinon.
   */
  public boolean isCommandeAchatGeneree() {
    return commandeAchatGeneree;
  }
  
  /**
   * Modifier si une commande d'achat a été générée pour ce document.
   * @param pCommandeAchatGeneree Indicatif de génération d'une commande d'achat.
   */
  public void setCommandeAchatGeneree(boolean pCommandeAchatGeneree) {
    commandeAchatGeneree = pCommandeAchatGeneree;
  }
  
  /**
   * Retourner le total HT du document de ventes.
   * @return Le total HT du document.
   */
  public BigDecimal getTotalHT() {
    return totalHT;
  }
  
  /**
   * Modifier le Total HT du document de ventes.
   * @param pTotalHT Montant du total en HT.
   */
  public void setTotalHT(BigDecimal pTotalHT) {
    totalHT = pTotalHT;
    if (totalHT != null) {
      totalHT = totalHT.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Retourner le total TTC du documents de ventes.
   * @return Le total TTC du document.
   */
  public BigDecimal getTotalTTC() {
    return totalTTC;
  }
  
  /**
   * Modifier le total TTC du document de ventes.
   * @param pTotalTTC Montant du total en TTC.
   */
  public void setTotalTTC(BigDecimal pTotalTTC) {
    totalTTC = pTotalTTC;
    if (totalTTC != null) {
      totalTTC = totalTTC.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
  }
  
  /**
   * Indiquer si le document de vente est verrouillé.
   * @return
   *         true si ce document est verrouillé, false sinon.
   */
  public boolean isVerrouille() {
    return verrouille;
  }
  
  /**
   * Modifier le verrouillage du document de ventes.
   * @param pVerrouille Indicatif de verrouillage d'un document.
   */
  public void setVerrouille(boolean pVerrouille) {
    verrouille = pVerrouille;
  }
  
  /**
   * Indiquer si le document est modifiable.
   * @return
   *         true si c'est modifiable, false sinon.
   */
  public boolean isModifiable() {
    return modifiable;
  }
  
  /**
   * Modifier la possibilité d'édition du document.
   * @param pModifiable Indicatif d'édition du document.
   */
  public void setModifiable(boolean pModifiable) {
    modifiable = pModifiable;
  }
  
  /**
   * Indiquer si le document est mono fournisseur.
   * @return
   *         true si le document est mono fournisseur, false sinon.
   */
  public Boolean isMonoFournisseur() {
    return monoFournisseur;
  }
  
  /**
   * Modifier l'indicatif du document mono fournisseur.
   * @param pIsMonoFournisseur Indicatif d'un document mono fournisseur.
   */
  public void setMonoFournisseur(boolean pIsMonoFournisseur) {
    monoFournisseur = pIsMonoFournisseur;
  }
  
  /**
   * Retourner l'identifiant du chantier associé au document.
   * @return Identifiant du chantier.
   */
  public IdChantier getIdChantier() {
    return idChantier;
  }
  
  /**
   * Modifier l'identifiant du chantier associé au document.<br>
   * Cette méthode doit être appelée conjointement avec setLibelleChantier() pour que les deux informations restent synchronisées.
   * @param pIdChantier Nouvel identifiant du chantier.
   */
  public void setIdChantier(IdChantier pIdChantier) {
    idChantier = pIdChantier;
  }
  
  /**
   * Retourner le libellé du chantier lié au document de vente.<br>
   * Noter que le libellé du chantier n'est pas une donnée intrinsèque de l'objet métier "document de vente". Toutefois, à partir du
   * moment où un identfiiant de chantier est renseigné, il sera très souvent nécessaire de récupérer le libellé correspondant du
   * chantier. Il est préférable, d'un point de vue performance, de récupérer le libéllé du chantgier dès le chargement du
   * document de vente. Ce n'est pas optimum mais pourra être optimisé lorsqu'on pourra indiquer la liste des chanps à retourner lors
   * d'une recherche. Le libellé chantier fera alors partir des informatiosn complémentaires facultatives.
   * @return Libellé du chantier.
   */
  public String getLibelleChantier() {
    return libelleChantier;
  }
  
  /**
   * Modifier le libellé du chantier associé au document.<br>
   * Cette méthode doit être appelée conjointement avec setIdChantier() pour que les deux informations restent synchronisées.
   * @param pLibelleChantier Nouveau libellé du chantier.
   */
  public void setLibelleChantier(String pLibelleChantier) {
    libelleChantier = pLibelleChantier;
  }
}
