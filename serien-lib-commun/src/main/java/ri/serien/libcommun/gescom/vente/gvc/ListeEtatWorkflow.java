/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc;

import java.util.ArrayList;

/**
 * Liste d'états de workflow GVC.
 */
public class ListeEtatWorkflow extends ArrayList<EtatWorkFlow> {
  public ListeEtatWorkflow() {
  }
  
  /**
   * Retourner tous les états workflow.
   */
  public static ListeEtatWorkflow retournerTout() {
    ListeEtatWorkflow listeEtatWorkFlow = new ListeEtatWorkflow();
    listeEtatWorkFlow.add(new EtatWorkFlow(EnumEtatWorkFlow.NOUVEAU));
    listeEtatWorkFlow.add(new EtatWorkFlow(EnumEtatWorkFlow.A_MODIFIER));
    listeEtatWorkFlow.add(new EtatWorkFlow(EnumEtatWorkFlow.SUPPRIME));
    listeEtatWorkFlow.add(new EtatWorkFlow(EnumEtatWorkFlow.TERMINE));
    return listeEtatWorkFlow;
  }
}
