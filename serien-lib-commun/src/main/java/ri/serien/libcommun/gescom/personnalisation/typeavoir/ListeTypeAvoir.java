/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typeavoir;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de types d'avoirs.
 */
public class ListeTypeAvoir extends ListeClasseMetier<IdTypeAvoir, TypeAvoir, ListeTypeAvoir> {
  
  /**
   * Charger une liste de TypeAvoir correspondants à la liste de IdTypeAvoir donnée.
   */
  @Override
  public ListeTypeAvoir charger(IdSession pIdSession, List<IdTypeAvoir> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charger une liste de TypeAvoir pour l'établissement donné.
   */
  @Override
  public ListeTypeAvoir charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return ManagerServiceParametre.chargerListeTypeAvoir(pIdSession, pIdEtablissement);
  }
  
}
