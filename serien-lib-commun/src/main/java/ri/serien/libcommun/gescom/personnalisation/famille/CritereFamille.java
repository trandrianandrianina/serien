/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.famille;

import ri.serien.libcommun.commun.recherche.CritereAvecEtablissement;
import ri.serien.libcommun.gescom.commun.article.GroupeArticles;

public class CritereFamille extends CritereAvecEtablissement {
  
  // Variables
  private GroupeArticles groupe = null;
  
  private boolean libelleComplet = false;
  
  private boolean avecAnnule = false;
  
  /**
   * Retourner si les familles annulés sont inclus.
   */
  public boolean isAvecAnnule() {
    return avecAnnule;
  }
  
  /**
   * Indiquer si on souhaite ou non inclure les familles annulés.
   * @param pAvecAnnule true=inclure les familles annulés, false est la valeur par défaut.
   */
  public void setAvecAnnule(boolean pAvecAnnule) {
    avecAnnule = pAvecAnnule;
  }
  
  /**
   * Retourner le groupe de la famille
   */
  public GroupeArticles getGroupe() {
    return groupe;
  }
  
  /**
   * Modifier le groupe de la famille
   */
  public void setGroupe(GroupeArticles pGroupe) {
    pGroupe = groupe;
  }
  
  /**
   * Retourner si le libellé est complet
   */
  public boolean isLibelleComplet() {
    return libelleComplet;
  }
  
  /**
   * Indiquer si on souhaite ou non inclure le libellé complet.
   * @param pLibelleComplet true=inclure le libellé complet, false est la valeur par défaut.
   */
  public void setLibelleComplet(boolean pLibelleComplet) {
    pLibelleComplet = libelleComplet;
  }
  
}
