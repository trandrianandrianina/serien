/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe définit l'id d'une banque d'un client.
 * Le nom de la banque suffit à définir l'id car aucune autre information n'est stockée concernant ces banques. Le nom sera
 * systématiquement écrit en lettre majuscule pour homogénéisé le stockage car chaque utilisateur peut être susceptible de saisir comme
 * bon lui semble.
 */
public class IdBanqueClient extends AbstractId {
  // Constantes
  public static final int LONGUEUR_NOM_BANQUE = 24;
  
  // Variables
  private final String nom;
  
  /**
   * Constructeur.
   */
  private IdBanqueClient(EnumEtatObjetMetier pEnumEtatObjetMetier, String pNomBibliotheque) {
    super(pEnumEtatObjetMetier);
    nom = controlerNom(pNomBibliotheque);
  }
  
  /**
   * Créer un nouvel identifiant.
   */
  public static IdBanqueClient getInstance(String pNomBanque) {
    return new IdBanqueClient(EnumEtatObjetMetier.MODIFIE, pNomBanque);
  }
  
  // -- Méthodes publiques
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdBanqueClient controlerId(IdBanqueClient pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de la banque du client est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de la banque du client n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler le nom de la banque.
   */
  private static String controlerNom(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le nom de la banque n'est pas renseigné.");
    }
    if (pValeur.length() > LONGUEUR_NOM_BANQUE) {
      throw new MessageErreurException("Le nom de la banque n'est pas correct car il est supérieur à la longueur attendue.");
    }
    return pValeur.trim().toUpperCase();
  }
  
  // -- Méthodes abstraites à surcharger dans les classes enfants
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + nom.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    
    if (pObject == null) {
      return false;
    }
    
    if (!(pObject instanceof IdBanqueClient)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de banques.");
    }
    IdBanqueClient id = (IdBanqueClient) pObject;
    return getNom().equals(id.getNom());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdBanqueClient)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdBanqueClient id = (IdBanqueClient) pObject;
    return nom.compareTo(id.nom);
  }
  
  @Override
  public String getTexte() {
    return getNom();
  }
  
  // -- Accesseurs
  
  public String getNom() {
    return nom;
  }
  
}
