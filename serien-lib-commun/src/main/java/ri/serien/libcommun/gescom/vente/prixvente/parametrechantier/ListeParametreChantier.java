/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametrechantier;

import java.math.BigDecimal;
import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.article.IdArticle;

/**
 * Classe regroupant tous les paramètres d'un chantier pour le calcul du prix de vente.
 */
public class ListeParametreChantier extends ArrayList<ParametreChantier> {
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le paramètre chantier correspondant à l'article et à la quantité demandée.
   * 
   * @param pIdArticle Identifiant de l'article.
   * @param pQuantite Quanaité souhaitée.
   * @return Paramètre chantier correspondant.
   */
  public ParametreChantier get(IdArticle pIdArticle, BigDecimal pQuantite) {
    // Vérifier les paramètres
    if (pIdArticle == null) {
      return null;
    }
    
    // Utiliser comme quantité par défaut
    if (pQuantite == null) {
      pQuantite = BigDecimal.ONE;
    }
    
    ParametreChantier parametreChantierTrouve = null;
    for (ParametreChantier parametreChantier : this) {
      // L'article ne correspond pas
      if (!pIdArticle.equals(parametreChantier.getIdArticle())) {
        continue;
      }
      // La quantité saisie est inférieure à la quantité de l'article chantier
      if (pQuantite.compareTo(parametreChantier.getQuantiteMinimum()) < 0) {
        continue;
      }
      // Si article encore non trouvé
      if (parametreChantierTrouve == null) {
        parametreChantierTrouve = parametreChantier;
      }
      // Contrôle des quantités, l'article chantier ayant une quantité inférieure à la quantité saisie
      else if (parametreChantier.getQuantiteMinimum().compareTo(parametreChantierTrouve.getQuantiteMinimum()) >= 0) {
        parametreChantierTrouve = parametreChantier;
      }
    }
    
    return parametreChantierTrouve;
  }
}
