/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stock;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette Enum propose deux cas possibles de stock disponibles : Achat et vente
 * car leur calcul est différent.
 */
public enum EnumTypeStockDisponible {
  TYPE_ACHAT(1, "Stock disponible achat"),
  TYPE_VENTE(2, "Stock disponible vente");
  
  private final Integer type;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeStockDisponible(Integer pType, String pLibelle) {
    type = pType;
    libelle = pLibelle;
  }
  
  /**
   * Le type sous lequel la valeur est persistée en base de données.
   */
  public Integer getType() {
    return type;
  }
  
  /**
   * Le libellé associé au type.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le type associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return type.toString();
  }
  
  /**
   * Retourner l'objet enum par son type.
   */
  static public EnumTypeStockDisponible valueOfByCode(Integer pType) {
    for (EnumTypeStockDisponible value : values()) {
      if (pType.equals(value.getType())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de valeur est inconnu : " + pType);
  }
  
}
