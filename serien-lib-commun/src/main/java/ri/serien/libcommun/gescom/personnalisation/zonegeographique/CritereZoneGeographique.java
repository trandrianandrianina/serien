/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.zonegeographique;

import ri.serien.libcommun.commun.recherche.CritereAvecEtablissement;

/**
 * Critères de recherche des zones géographiques.
 */
public class CritereZoneGeographique extends CritereAvecEtablissement {
  
  // -- Accesseurs
  
  public String getCodeEtablissement() {
    if (getIdEtablissement() == null) {
      return null;
    }
    return getIdEtablissement().getCodeEtablissement();
  }
  
}
