/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.famille;

import java.math.BigDecimal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;

/**
 * Famille
 * 
 * Une famille d'articles est composé de plusieurs articles, qui elle-mêmes peuvent être décomposées en plusieurs sous-familles.
 * La famille contient tous les paramètres qui s'appliquent à l'ensemble des articles d'une famille.
 */
public class Famille extends AbstractClasseMetier<IdFamille> {
  public static final String TOUTES_LES_FAMILLES = "***";
  // Variables
  private String libelle = "";
  private int codeTVA = 1;
  private IdUnite idUniteVente = null;
  private BigDecimal coefficientVente = BigDecimal.ZERO;
  private BigDecimal coefficientPRV = BigDecimal.ZERO;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public Famille(IdFamille pIdFamille) {
    super(pIdFamille);
  }
  
  @Override
  public String getTexte() {
    if (libelle != null) {
      return libelle;
    }
    else {
      return id.getTexte();
    }
  }
  
  /**
   * Déterminer si la famille en question est bien une famille et non la sélection de toutes les familles
   */
  public boolean isUneFamille() {
    if (id == null) {
      return false;
    }
    return !id.getCode().equals(TOUTES_LES_FAMILLES);
  }
  
  // ---accesseur
  /**
   * Retournee le libellé de la famille
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé de la famille
   */
  public void setLibelle(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Retourner le code TVA
   */
  public int getCodeTVA() {
    return codeTVA;
  }
  
  /**
   * Modifier le code TVA
   */
  public void setCodeTVA(int codeTVA) {
    this.codeTVA = codeTVA;
  }
  
  /**
   * Retourner l'identifiant de l'unite de vente
   */
  public IdUnite getIdUniteVente() {
    return idUniteVente;
  }
  
  /**
   * Modifier l'identifiant de l'unite de vente
   */
  public void setIdUniteVente(IdUnite pId) {
    this.idUniteVente = pId;
  }
  
  /**
   * Retourner le coefficient de vente
   */
  public BigDecimal getCoefficientVente() {
    return coefficientVente;
  }
  
  /**
   * Modifier le coefficient de vente
   */
  public void setCoefficientVente(BigDecimal coefficientVente) {
    this.coefficientVente = coefficientVente;
  }
  
  /**
   * Retourner le coefficient de prix de revient
   */
  public BigDecimal getCoefficientPRV() {
    return coefficientPRV;
  }
  
  /**
   * Modifier le coefficient de vente
   */
  public void setCoefficientPRV(BigDecimal coefficientPRV) {
    this.coefficientPRV = coefficientPRV;
  }
  
}
