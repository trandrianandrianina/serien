/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.document;

import java.io.Serializable;
import java.math.BigDecimal;

public class ListeTva implements Serializable {
  // Variables
  private Integer numeroTVA1 = null;
  private Integer numeroTVA2 = null;
  private Integer numeroTVA3 = null;
  private BigDecimal montantSoumisTVA1 = null;
  private BigDecimal montantSoumisTVA2 = null;
  private BigDecimal montantSoumisTVA3 = null;
  private BigDecimal montantTVA1 = null;
  private BigDecimal montantTVA2 = null;
  private BigDecimal montantTVA3 = null;
  private BigDecimal tauxTVA1 = null;
  private BigDecimal tauxTVA2 = null;
  private BigDecimal tauxTVA3 = null;
  private BigDecimal totalTVA = BigDecimal.ZERO;
  
  // -- Méthodes privées
  
  /**
   * Retourne le total des TVA
   */
  private void calculerTotalTVA() {
    totalTVA = BigDecimal.ZERO;
    if (montantTVA1 != null) {
      totalTVA = totalTVA.add(montantTVA1);
    }
    if (montantTVA2 != null) {
      totalTVA = totalTVA.add(montantTVA2);
    }
    if (montantTVA3 != null) {
      totalTVA = totalTVA.add(montantTVA3);
    }
  }
  
  // -- Accesseurs
  
  public Integer getNumeroTVA1() {
    return numeroTVA1;
  }
  
  public void setNumeroTVA1(Integer numeroTVA1) {
    this.numeroTVA1 = numeroTVA1;
  }
  
  public Integer getNumeroTVA2() {
    return numeroTVA2;
  }
  
  public void setNumeroTVA2(Integer numeroTVA2) {
    this.numeroTVA2 = numeroTVA2;
  }
  
  public Integer getNumeroTVA3() {
    return numeroTVA3;
  }
  
  public void setNumeroTVA3(Integer numeroTVA3) {
    this.numeroTVA3 = numeroTVA3;
  }
  
  public BigDecimal getMontantSoumisTVA1() {
    return montantSoumisTVA1;
  }
  
  public void setMontantSoumisTVA1(BigDecimal montantSoumisTVA1) {
    this.montantSoumisTVA1 = montantSoumisTVA1;
  }
  
  public BigDecimal getMontantSoumisTVA2() {
    return montantSoumisTVA2;
  }
  
  public void setMontantSoumisTVA2(BigDecimal montantSoumisTVA2) {
    this.montantSoumisTVA2 = montantSoumisTVA2;
  }
  
  public BigDecimal getMontantSoumisTVA3() {
    return montantSoumisTVA3;
  }
  
  public void setMontantSoumisTVA3(BigDecimal montantSoumisTVA3) {
    this.montantSoumisTVA3 = montantSoumisTVA3;
  }
  
  public BigDecimal getMontantTVA1() {
    return montantTVA1;
  }
  
  public void setMontantTVA1(BigDecimal montantTVA1) {
    this.montantTVA1 = montantTVA1;
    calculerTotalTVA();
  }
  
  public BigDecimal getMontantTVA2() {
    return montantTVA2;
  }
  
  public void setMontantTVA2(BigDecimal montantTVA2) {
    this.montantTVA2 = montantTVA2;
    calculerTotalTVA();
  }
  
  public BigDecimal getMontantTVA3() {
    return montantTVA3;
  }
  
  public void setMontantTVA3(BigDecimal montantTVA3) {
    this.montantTVA3 = montantTVA3;
    calculerTotalTVA();
  }
  
  public BigDecimal getTauxTVA1() {
    return tauxTVA1;
  }
  
  public void setTauxTVA1(BigDecimal tauxTVA1) {
    this.tauxTVA1 = tauxTVA1;
  }
  
  public BigDecimal getTauxTVA2() {
    return tauxTVA2;
  }
  
  public void setTauxTVA2(BigDecimal tauxTVA2) {
    this.tauxTVA2 = tauxTVA2;
  }
  
  public BigDecimal getTauxTVA3() {
    return tauxTVA3;
  }
  
  public void setTauxTVA3(BigDecimal tauxTVA3) {
    this.tauxTVA3 = tauxTVA3;
  }
  
  public BigDecimal getTotalTVA() {
    return totalTVA;
  }
  
}
