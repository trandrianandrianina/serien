/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.commune;

import java.io.Serializable;

/**
 * Cette classe contient un ensemble de critères qui permettent la recherche d'une commune.
 */
public class CritereCommune implements Serializable {
  // Variables
  private Integer codePostal = null;
  private String codePostalFormate = null;
  private String nomCommune = null;
  // Si le tri vaut true=Ascendant, false=Descandant, null=Pas de tri
  private Boolean triCodePostalAscendant = null;
  
  public void initialiser() {
    codePostal = null;
    codePostalFormate = null;
    nomCommune = null;
  }
  
  // -- Accesseurs
  
  public Integer getCodePostal() {
    return codePostal;
  }
  
  public void setCodePostal(Integer pCodePostal) {
    codePostal = pCodePostal;
  }
  
  public String getCodePostalFormate() {
    return codePostalFormate;
  }
  
  public void setCodePostalFormate(String pCodePostalFormate) {
    codePostalFormate = pCodePostalFormate;
  }
  
  public String getNomCommune() {
    return nomCommune;
  }
  
  public void setNomCommune(String pNomCommune) {
    nomCommune = pNomCommune;
  }
  
  public Boolean getTriCodePostalAscendant() {
    return triCodePostalAscendant;
  }
  
  public void setTriCodePostalAscendant(Boolean pTriCodePostalAscendant) {
    triCodePostalAscendant = pTriCodePostalAscendant;
  }
  
}
