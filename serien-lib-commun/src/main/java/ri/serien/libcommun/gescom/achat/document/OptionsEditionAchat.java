/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import java.io.Serializable;

import ri.serien.libcommun.exploitation.documentstocke.EnumTypeDocumentStocke;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.gescom.commun.document.ModeEdition;

public class OptionsEditionAchat implements Serializable {
  // Variables
  private EnumOptionEdition optionEdition = null;
  private ModeEdition modeEdition = null;
  // Variables utilisée pour NewSim
  private IdMail idMail = null;
  private IdMail idFaxMail = null;
  private boolean stockageBaseDocumentaire = false;
  private EnumTypeDocumentStocke typeDocumentStocke = EnumTypeDocumentStocke.NON_DEFINI;
  private String indicatifIdDocumentAStocker = null;
  
  // -- Accesseurs
  
  public EnumOptionEdition getOptionEdition() {
    return optionEdition;
  }
  
  public void setOptionEdition(EnumOptionEdition optionEdition) {
    this.optionEdition = optionEdition;
  }
  
  public ModeEdition getModeEdition() {
    return modeEdition;
  }
  
  public void setModeEdition(ModeEdition modeEdition) {
    this.modeEdition = modeEdition;
  }
  
  public IdMail getIdMail() {
    return idMail;
  }
  
  public void setIdMail(IdMail idMail) {
    this.idMail = idMail;
  }
  
  public IdMail getIdFaxMail() {
    return idFaxMail;
  }
  
  public void setIdFaxMail(IdMail idFaxMail) {
    this.idFaxMail = idFaxMail;
  }
  
  public boolean isStockageBaseDocumentaire() {
    return stockageBaseDocumentaire;
  }
  
  public void setStockageBaseDocumentaire(boolean stockageBaseDocumentaire) {
    this.stockageBaseDocumentaire = stockageBaseDocumentaire;
  }
  
  public EnumTypeDocumentStocke getTypeDocumentStocke() {
    return typeDocumentStocke;
  }
  
  public void setTypeDocumentStocke(EnumTypeDocumentStocke typeDocumentStocke) {
    this.typeDocumentStocke = typeDocumentStocke;
  }
  
  public String getIndicatifIdDocumentAStocker() {
    return indicatifIdDocumentAStocker;
  }
  
  public void setIndicatifIdDocumentAStocker(String indicatifIdDocumentAStocker) {
    this.indicatifIdDocumentAStocker = indicatifIdDocumentAStocker;
  }
  
}
