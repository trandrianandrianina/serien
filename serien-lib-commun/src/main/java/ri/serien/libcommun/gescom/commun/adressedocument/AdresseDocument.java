/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.adressedocument;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Adresse d'un document de vente
 * 
 * Cette classe représente une adresse contenue dans un document de vente.
 * Ces adresse sont liées au document et sont numérotées car un document peut avoir plusieurs adresses (plusieurs adresses de livraison
 * sur un chan,tier par exemple). L'adresse numérotée 0 est l'adresse principale. Elle ne doit jamais être supprimée tant que le document
 * existe.
 */
public class AdresseDocument extends AbstractClasseMetier<IdAdresseDocument> {
  
  private Adresse adresse = null;
  private String codePays = null;
  private String numeroTelephone = null;
  private String numeroFax = null;
  
  /**
   * Constructeur avec l'identifiant de l'adresse.
   */
  public AdresseDocument(IdAdresseDocument pIdAdresseDocument) {
    super(pIdAdresseDocument);
  }
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getId().hashCode();
    cle = 37 * cle + adresse.hashCode();
    return cle;
  }
  
  /**
   * Comparer 2 adresses.
   */
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    
    if (pObject == null) {
      return false;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof AdresseDocument)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux adresses de document.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    AdresseDocument adresse = (AdresseDocument) pObject;
    return Constantes.equals(adresse, adresse.adresse);
  }
  
  /**
   * Renvoit le nom du client.
   * C'est cette méthode qui définit le format d'affichage des clients dans les composants graphiques.
   * Le format attendu est :
   * - "Civilité Nom Prénom" pour les coordonnées avec une civilité (les particuliers).
   * - "RaisonSociale" pour les autres (professionnels).
   */
  @Override
  public String toString() {
    if (adresse == null) {
      return null;
    }
    if (adresse.getIdCivilite() != null) {
      return adresse.getIdCivilite().getCode() + " " + adresse.getNom();
    }
    else {
      return adresse.getNom();
    }
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public AdresseDocument clone() {
    Object o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = super.clone();
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return (AdresseDocument) o;
  }
  
  /**
   * Indique si l'adresse est à blanc
   */
  public boolean isEmpty() {
    return (adresse == null || adresse.getNom() == null || adresse.getNom().trim().isEmpty())
        && (adresse.getComplementNom() == null || adresse.getComplementNom().trim().isEmpty())
        && ((adresse.getRue() == null || adresse.getRue().trim().isEmpty())
            && (adresse.getLocalisation() == null || adresse.getLocalisation().trim().isEmpty()))
        && (adresse.getCodePostal() <= 0) && (adresse.getVille() == null || adresse.getVille().trim().isEmpty());
  }
  
  // -- Accesseurs
  
  /**
   * Adresse du document
   */
  public Adresse getAdresse() {
    return adresse;
  }
  
  /**
   * Modifier l'adresse de document
   */
  public void setAdresse(Adresse adresse) {
    this.adresse = adresse;
  }
  
  /**
   * Code pays
   */
  public String getCodePays() {
    return codePays;
  }
  
  /**
   * Modifier le code pays
   */
  public void setCodePays(String codePays) {
    this.codePays = codePays;
  }
  
  /**
   * Numéro de téléphone
   */
  public String getNumeroTelephone() {
    return numeroTelephone;
  }
  
  /**
   * Modifier le numéro de téléphone
   */
  public void setNumeroTelephone(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }
  
  /**
   * Numéro de fax
   */
  public String getNumeroFax() {
    return numeroFax;
  }
  
  /**
   * Numéro de fax
   */
  public void setNumeroFax(String numeroFax) {
    this.numeroFax = numeroFax;
  }
  
  @Override
  public String getTexte() {
    return this.toString();
  }
  
}
