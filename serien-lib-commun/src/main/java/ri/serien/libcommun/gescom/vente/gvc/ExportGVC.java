/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

/**
 * Classe générique d'export EXCEL
 * il existe pour le moment une méthode qui prend en paramètre une liste de GenericRecord
 */
public class ExportGVC implements Serializable {

  public static long nbExports = 1;
  private SimpleDateFormat formatteurDate = null;
  private char separateurDates = '/';
  private HSSFWorkbook fichierExcel = null;
  private HSSFSheet feuille = null;
  private String[] tableauEntete = {
      "Etb",
      "Code Article",
      "Libellé article",
      "Prix fabricant",
      "Prix four. principal",
      "Prix four. référence",
      "Prix de revient",
      "PUMP",
      "Points WATT",
      "Date des tarifs",
      "Prix public HT",
      "Prix particulier HT",
      "Prix promo HT",
      "Prix artisan HT",
      "Prix GSB HT",
      "Prix Veille GSB HT",
      "Nom Veille GSB",
      "Prix Veille Web HT",
      "Nom Veille Web",
      "DEEE"
  };

  public ExportGVC() {
    fichierExcel = new HSSFWorkbook();
    formatteurDate = new SimpleDateFormat("yyyyy-mm-dd");
  }

  /**
   * On passe à l'export Excel un titre de feuille, une liste de GenericRecord et un chemin de destination du fichier
   */
  public boolean creerUnExportXls(String titreFeuille, ArrayList<LigneGvc> listeArticles, String cheminFichier) {
    if (listeArticles == null || cheminFichier == null) {
      return false;
    }
    // Si on a pas de titre on met la date de la création du doc
    if (titreFeuille == null) {
      titreFeuille = formatteurDate.format(new Date()).toString();
    }

    feuille = fichierExcel.createSheet(titreFeuille);

    attribuerEnTeteDocument();

    for (int i = 0; i < listeArticles.size(); i++) {
      Row ligne = feuille.createRow(i + 1);

      attribuerArticleAuxCellules(listeArticles.get(i), ligne);
    }

    try {
      FileOutputStream out = new FileOutputStream(new File(cheminFichier + File.separatorChar + titreFeuille + ".xls"));
      fichierExcel.write(out);
      out.close();
      nbExports++;

    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
      return false;
    }
    catch (IOException e) {
      e.printStackTrace();
      return false;
    }

    return true;
  }

  /**
   * On attribue à la ligne du fichier Excel des cellules au contenu typé
   */
  private void attribuerArticleAuxCellules(LigneGvc article, Row ligne) {
    if (ligne == null || article == null) {
      return;
    }

    for (int i = 0; i < tableauEntete.length; i++) {
      Cell cellule = ligne.createCell(i);

      switch (i) {
        case 0:
          if (article.getIdArticle() != null) {
            cellule.setCellValue(article.getIdArticle().getCodeEtablissement());
          }
          break;
        case 1:
          if (article.getIdArticle() != null) {
            cellule.setCellValue(article.getIdArticle().getCodeArticle());
          }
          break;
        case 2:
          if (article.getLibelleArticle() != null) {
            cellule.setCellValue(article.getLibelleArticle().toString().trim());
          }
          break;
        case 3:
          if (article.getPrixFabricantHT() != null) {
            cellule.setCellValue(Double.parseDouble(article.getPrixFabricantHT().toString().trim()));
          }
          break;
        case 4:
          if (article.getCnaFrsPrincipal() != null && article.getCnaFrsPrincipal().getPrixAchatRemise() != null) {
            cellule.setCellValue(Double.parseDouble(article.getCnaFrsPrincipal().getPrixAchatRemise().toString().trim()));
          }
          break;
        case 5:
          if (article.getCnaDistribRef() != null && article.getCnaDistribRef().getPrixAchatRemise() != null) {
            cellule.setCellValue(Double.parseDouble(article.getCnaDistribRef().getPrixAchatRemise().toString().trim()));
          }
          break;
        case 6:
          if (article.getPrixDeRevient() != null) {
            cellule.setCellValue(Double.parseDouble(article.getPrixDeRevient().toString().trim()));
          }
          break;
        case 7:
          if (article.getPump() != null) {
            cellule.setCellValue(Double.parseDouble(article.getPump().toString().trim()));
          }
          break;
        case 8:
          if (article.getNbPointsWatt() != null) {
            cellule.setCellValue(Double.parseDouble(article.getNbPointsWatt().toString().trim()));
          }
          break;
        case 9:
          if (article.getDateTarifs() != null) {
            cellule.setCellValue(ConvertDate.toNormal(article.getDateTarifs().toString().trim(), separateurDates, false));
          }
          break;
        case 10:
          if (article.getPrixPublicHT() != null) {
            cellule.setCellValue(Double.parseDouble(article.getPrixPublicHT().toString().trim()));
          }
          break;
        case 11:
          if (article.getPrixParticulierHT() != null) {
            cellule.setCellValue(Double.parseDouble(article.getPrixParticulierHT().toString().trim()));
          }
          break;
        case 12:
          if (article.getPrixPromoHT() != null) {
            cellule.setCellValue(Double.parseDouble(article.getPrixPromoHT().toString().trim()));
          }
          break;
        case 13:
          if (article.getPrixArtisanHT() != null) {
            cellule.setCellValue(Double.parseDouble(article.getPrixArtisanHT().toString().trim()));
          }
          break;
        case 14:
          if (article.getPrixGsbHT() != null) {
            cellule.setCellValue(Double.parseDouble(article.getPrixGsbHT().toString().trim()));
          }
          break;
        case 15:
          if (article.getVeillePrixGsb() != null) {
            cellule.setCellValue(Double.parseDouble(article.getVeillePrixGsb().toString().trim()));
          }
          break;
        case 16:
          if (article.getVeilleNomGsb() != null) {
            cellule.setCellValue(article.getVeilleNomGsb().toString().trim());
          }
          break;
        case 17:
          if (article.getTarifPjet() != null && article.getTarifPjet().getTarifPJ() != null) {
            cellule.setCellValue(Double.parseDouble(article.getTarifPjet().getTarifPJ().toString().trim()));
          }
          break;
        case 18:
          if (article.getTarifPjet() != null && article.getTarifPjet().getConcurrent() != null) {
            cellule.setCellValue(article.getTarifPjet().getConcurrent().getNom().toString().trim());
          }
          break;
        case 19:
          if (article.getDEEE() != null) {
            cellule.setCellValue(Double.parseDouble(article.getDEEE().toString().trim()));
          }
          break;

        default:
          break;
      }
    }
  }

  private void attribuerEnTeteDocument() {
    if (feuille == null) {
      return;
    }

    Row ligne = feuille.createRow(0);

    for (int i = 0; i < tableauEntete.length; i++) {
      Cell cellule = ligne.createCell(i);
      cellule.setCellValue(tableauEntete[i]);
    }
  }

}
