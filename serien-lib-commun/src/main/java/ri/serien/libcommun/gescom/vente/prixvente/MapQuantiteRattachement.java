/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente;

import java.math.BigDecimal;
import java.util.HashMap;

import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.EnumTypeRattachementArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.IdRattachementArticle;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

public class MapQuantiteRattachement extends HashMap<IdRattachementArticle, BigDecimal> {
  
  /**
   * Créer un nouvel enregistrement (si nécessaire) dans la map avec la quantité initialisée à 0.
   * 
   * @param pTypeRattachementConditionVente
   * @param pCodeRatatchement
   */
  public void creerNouveauRattachement(EnumTypeRattachementArticle pTypeRattachementConditionVente, String pCodeRatatchement) {
    try {
      IdRattachementArticle idRattachement = IdRattachementArticle.getInstance(pTypeRattachementConditionVente, pCodeRatatchement);
      if (!containsKey(idRattachement)) {
        put(idRattachement, BigDecimal.ZERO);
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Ajoute une quantité à un rattachement identifié par le type et le code.
   * 
   * @param pTypeRattachementConditionVente
   * @param pCode
   * @param pQuantite
   */
  public void ajouterQuantite(EnumTypeRattachementArticle pTypeRattachementConditionVente, String pCode, BigDecimal pQuantite) {
    try {
      IdRattachementArticle idRattachement = IdRattachementArticle.getInstance(pTypeRattachementConditionVente, pCode);
      ajouterQuantite(idRattachement, pQuantite);
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Ajoute une quantité à un rattachement identifié par son identifiant s'il existe sinon l'ajout est abandonné.
   * 
   * @param pIdRattachement
   * @param pQuantite
   */
  public void ajouterQuantite(IdRattachementArticle pIdRattachement, BigDecimal pQuantite) {
    if (pIdRattachement == null || pQuantite == null) {
      return;
    }
    
    BigDecimal quantite = get(pIdRattachement);
    if (quantite == null) {
      return;
    }
    put(pIdRattachement, quantite.add(pQuantite));
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public MapQuantiteRattachement clone() {
    MapQuantiteRattachement objetTemporaire = null;
    try {
      // Récupération de l'instance à renvoyer par l'appel de la méthode super.clone()
      objetTemporaire = (MapQuantiteRattachement) super.clone();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
    
    return objetTemporaire;
  }
}
