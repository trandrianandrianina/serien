/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'un document de vente.
 */
public enum EnumTypeDocumentVente {
  NON_DEFINI(0, "Non défini", ""),
  DEVIS(1, "Devis", "DEV"),
  FACTURE(2, "Facture", "FAC"),
  BON(3, "Bon", "EXP"),
  COMMANDE(4, "Commande", "CMD");
  
  private final Integer code;
  private final String libelle;
  private final String codeAlpha;
  
  /**
   * Constructeur.
   */
  EnumTypeDocumentVente(Integer pCode, String pLibelle, String pCodeAlpha) {
    code = pCode;
    libelle = pLibelle;
    codeAlpha = pCodeAlpha;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Le libellé associé au code en minuscules.
   */
  public String getLibelleEnMinuscules() {
    return libelle.toLowerCase();
  }
  
  /**
   * Le code alphabétique court associé au code.
   */
  public String getCodeAlpha() {
    return codeAlpha;
  }
  
  /**
   * Retourner le libellé associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeDocumentVente valueOfByCode(Integer pCode) {
    for (EnumTypeDocumentVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type du document de vente est invalide : " + pCode);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeDocumentVente valueOfByCodeAlpha(String pCode) {
    for (EnumTypeDocumentVente value : values()) {
      if (pCode.equals(value.getCodeAlpha())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type du document de vente est invalide : " + pCode);
  }
}
