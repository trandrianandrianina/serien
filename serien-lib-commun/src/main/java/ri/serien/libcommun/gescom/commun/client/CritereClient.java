/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import java.io.Serializable;

import ri.serien.libcommun.commun.recherche.ArgumentRecherche;
import ri.serien.libcommun.commun.recherche.ListeArgumentRecherche;
import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Critères de recherche des clients.
 */
public class CritereClient implements Serializable {
  private IdEtablissement idEtablissement = null;
  private EnumTypeCompteClient typeCompteClient = null;
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  private IdClient idClient = null;
  private CodePostalCommune codePostalCommune = null;
  private Boolean isRechercheProspectAutorisee = false;
  private Boolean isRechercheClientPrincipalUniquement = false;
  
  /**
   * Initialiser les critères de recherche.
   * Sauf l'établissement qui ne peut pas être null.
   */
  public void initialiser() {
    typeCompteClient = null;
    rechercheGenerique.initialiser();
    idClient = null;
    codePostalCommune = null;
    isRechercheProspectAutorisee = false;
    isRechercheClientPrincipalUniquement = false;
  }
  
  /**
   * Identifiant de l'établissement dans lequel est recherché le client.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Filtre sur l'établissement.
   */
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Filtre sur le type de compte client.
   */
  public EnumTypeCompteClient getTypeCompteClient() {
    return typeCompteClient;
  }
  
  /**
   * Modifier le fitre sur le type de compte client.
   */
  public void setTypeCompteClient(EnumTypeCompteClient pTypeCompteClient) {
    typeCompteClient = pTypeCompteClient;
  }
  
  /**
   * Filtre sur le code postal et/ou la commune du client.
   */
  public CodePostalCommune getCodePostalCommune() {
    return codePostalCommune;
  }
  
  /**
   * Modifier le fitre sur le code postal et/ou la commune du client.
   */
  public void setCodePostalCommune(CodePostalCommune pCodePostalCommune) {
    codePostalCommune = pCodePostalCommune;
  }
  
  /**
   * Indiquer si la recherche de prospects est autorisée
   */
  public Boolean getIsRechercheProspectAutorisee() {
    return isRechercheProspectAutorisee;
  }
  
  /**
   * Indiquer si la recherche de prospects est autorisée
   */
  public void setIsRechercheProspectAutorisee(Boolean pIsRechercheProspectAutorisee) {
    isRechercheProspectAutorisee = pIsRechercheProspectAutorisee;
  }
  
  /**
   * Indiquer si la recherche ne concerne que les clients principaux (suffixe 0)
   */
  public Boolean getIsRechercheClientPrincipalUniquement() {
    return isRechercheClientPrincipalUniquement;
  }
  
  /**
   * Indiquer si la recherche ne concerne que les clients principaux (suffixe 0)
   */
  public void setIsRechercheClientPrincipalUniquement(Boolean isRechercheClientPrincipalUniquement) {
    this.isRechercheClientPrincipalUniquement = isRechercheClientPrincipalUniquement;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void setTexteRechercheGenerique(String pTexteRecherche) {
    rechercheGenerique.setTexte(pTexteRecherche);
  }
  
  /**
   * Identifiant du client recherché
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Modifier l'identifiant du client recherché
   */
  public void setIdClient(IdClient pIdClient) {
    idClient = pIdClient;
  }
  
  /**
   * Construire la partie WHERE de la requête suivant les critères de recherche.
   */
  public String getRequeteWhere() {
    StringBuilder sql = new StringBuilder();
    
    // Filtrer sur l'établissement
    sql.append("cli.CLETB = '" + getCodeEtablissement() + "'");
    
    // Ne pas sélectionner les clients désactivés
    sql.append(" and cli.CLTNS <> 9");
    
    // Filtrer sur le type de client
    // - En compte : EBIN05 = null ou ""
    // - Comptant : EBIN05= "1"
    if (typeCompteClient != null) {
      if (typeCompteClient.equals(EnumTypeCompteClient.EN_COMPTE)) {
        sql.append(" and (ecli.EBIN05 = ' ' or ecli.EBIN05 is null) AND cli.CLIN29 = ' '");
      }
      else if (typeCompteClient.equals(EnumTypeCompteClient.COMPTANT)) {
        sql.append(" and ecli.EBIN05 = '1' AND cli.CLIN29 = ' '");
      }
      else if (typeCompteClient.equals(EnumTypeCompteClient.PROSPECT) && isRechercheProspectAutorisee) {
        sql.append(" and cli.CLIN29 = 'P'");
      }
    }
    
    // Si recherche à partir d'un identifiant
    if (idClient != null) {
      sql.append(" and cli.CLCLI = " + idClient.getNumero());
      sql.append(" and cli.CLLIV = " + idClient.getSuffixe());
    }
    
    // Si la recherche de prospect n'est pas autorisée
    if (!isRechercheProspectAutorisee) {
      sql.append(" and cli.CLIN29 <> 'P'");
    }
    
    // Si on recherche uniquement les clients principaux
    if (isRechercheClientPrincipalUniquement) {
      sql.append(" and cli.CLLIV = 0");
    }
    
    // Filtrer sur le code postal
    if (codePostalCommune != null) {
      // Si on a la ville mais pas de code postal
      if (!codePostalCommune.getVille().trim().isEmpty()
          && (codePostalCommune.getCodePostal().trim().isEmpty() || codePostalCommune.getCodePostal().equals("00000"))) {
        sql.append(" and UPPER(SUBSTRING(cli.CLVIL,7)) like '" + codePostalCommune.getVille() + "%'");
      }
      // Si on a le code postall mais pas de ville
      else if (!codePostalCommune.getCodePostal().trim().isEmpty() && !codePostalCommune.getCodePostal().equals("00000")
          && codePostalCommune.getVille().trim().isEmpty()) {
        sql.append(" and (cast(cli.CLCDP1 as char(5))) like '" + codePostalCommune.getCodePostal() + "%'");
      }
      // Si on a un code postal et une ville
      else if (!codePostalCommune.getCodePostal().equals("00000")) {
        sql.append(" and ((cast(cli.CLCDP1 as char(5))) like '" + codePostalCommune.getCodePostal()
            + "%' and UPPER(SUBSTRING(cli.CLVIL,7)) like '" + codePostalCommune.getVille() + "%')");
      }
    }
    
    // Ajouter les critères de la recherche générique
    ListeArgumentRecherche listeArgumentRecherche = rechercheGenerique.getListeArgumentRecherche();
    if (listeArgumentRecherche != null) {
      int compteurArgument = 0;
      for (ArgumentRecherche argumentRecherche : listeArgumentRecherche) {
        compteurArgument++;
        String valeur = argumentRecherche.getValeur();
        
        switch (argumentRecherche.getType()) {
          case TEXTE:
          case EMAIL:
            if (compteurArgument > 1) {
              sql.append(" OR ");
            }
            else {
              sql.append(" AND ");
            }
            sql.append("(TRANSLATE(UPPER(TRIM(cli.CLNOM)), 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU',"
                + " 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ') LIKE TRANSLATE('").append(valeur)
                .append("%', 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')")
                .append(" OR TRANSLATE(UPPER(TRIM(cli.CLNOM)), 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU',"
                    + " 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ') LIKE TRANSLATE('% ")
                .append(valeur)
                .append("%', 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')")
                .append(" OR TRANSLATE(UPPER(TRIM(cli.CLCPL)), 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU',"
                    + " 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ') LIKE TRANSLATE('")
                .append(valeur)
                .append("%', 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')")
                .append(" OR TRANSLATE(UPPER(TRIM(cli.CLCPL)), 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU',"
                    + " 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ') LIKE TRANSLATE('% ")
                .append(valeur)
                .append("%', 'aaaaaaceeeeiiiinooooouuuuAAAAAACEEEEIIIINOOOOOUUUU', 'áàâäãåçéèêëíìîïñóòôöõúùûüÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜ')")
                .append(" OR UPPER(cli.CLCLK) LIKE '").append(valeur).append("%'").append(" OR UPPER(cli.CLCLA) LIKE '").append(valeur)
                .append("%'").append(" OR UPPER(cli.CLCLK) LIKE '% ").append(valeur).append("%'").append(" OR UPPER(cli.CLCLA) LIKE '% ")
                .append(valeur).append("%'").append(")");
            break;
          case IDENTIFIANT:
            if (valeur.contains("-")) {
              String[] identifiant = valeur.split("-");
              sql.append(" and  (cli.CLCLI = '" + identifiant[0] + "' and  cli.CLLIV = '" + identifiant[1] + "')");
            }
            else {
              sql.append(" and  cli.CLCLI = '" + valeur + "'");
            }
            break;
          case TELEPHONE:
            sql.append(" and (cli.CLTEL = '" + valeur + "')");
            break;
          
          case DECIMAL:
          case CODE_POSTAL:
          case ENTIER:
            sql.append(" and (cast(cli.CLCLI as char(6)) = '" + valeur + "' or cli.CLSRN = '" + valeur + "')");
            break;
          
          case IGNORE:
            break;
        }
      }
    }
    
    return sql.toString();
  }
  
  /**
   * Construire la partie ORDER BY de la requête suivant les critères de recherche.
   */
  public String getRequeteOrderBy() {
    return "cli.CLNOM, cli.CLVIL, cli.CLCLI";
  }
  
}
