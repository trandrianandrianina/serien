/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stock;

import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.stockattenducommande.ListeStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockattenducommande.StockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockmouvement.ListeStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.StockMouvement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceStock;

/**
 * Stock détaillé d'un article pour un magasin.
 * 
 * Le stock détaillé de l'article peut être pour un magasin ou pour un établissement :
 * - Dans le cas d'un stock détaillé d'un magasin, l'identifiant stock comporte l'identifiant du magasin. Les attendus/commandés et les
 * mouvements de stock sont ceux du magasin.
 * - Dans le cas d'un stock détaillé de l'établissement, l'identifiant stock ne comporte pas d'identifiant de magasin mais uniquement un
 * identifiant d'établissement. Les attendus/commandés et les mouvements de stock sont ceux de tous les magasins de l'établissement.
 * 
 * Cette classe a pour vocation de déterminer les informations de stock à partir des attendus et commandés. Elle n'utilise pas la table
 * PGVMSTKM contrairement à la classe Stock. Le stock issu de la table stock est mémorisé dans l'attribut "stock" du stock détaillé.
 * Si bien que nous avons les quantités en stock deux fois, celles déduites à partir des attendus et commandés exposées par la classe
 * StockDetaille et celles issues de la table stock exposées par la classe Stock. Cela permet de mettre en place un mécanisme de contrôle
 * d'écart entre ces deux modes de calculs.
 */
public class StockDetaille extends AbstractClasseMetier<IdStock> {
  private Date dateMiniReappro = null;
  private Stock stock = null;
  private ListeStockAttenduCommande listeStockAttenduCommande = null;
  private ListeStockMouvement listeStockMouvement = null;
  private ListeStock listeStockMagasin = null;
  
  /**
   * Constructeur avec l'identififant du stock (obligatoire).
   */
  public StockDetaille(IdStock pIdStock) {
    super(pIdStock);
  }
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  /**
   * Charger le stock détaillé d'un article pour un magasin ou un établissement si le magasin n'est pas renseigné.
   * 
   * Si l'identifiant du magasin est null, le stock des magasins est cumulé pour fournir le stock sur l'ensemble de l'établissement.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pIdMagasin Identifiant du magasin (optionnel).
   * @param pIdArticle Identifiant de l'article dont on souhaite charger le stock détaillé.
   * @return Stock du magasin.
   */
  public static StockDetaille chargerStockDetaille(IdSession pIdSession, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin,
      IdArticle pIdArticle) {
    // Contrôler les paramètres
    IdSession.controlerId(pIdSession, true);
    IdEtablissement.controlerId(pIdEtablissement, true);
    IdArticle.controlerId(pIdArticle, true);
    
    // Charger les stocks suivant les critères de recherche
    // Normalement, la liste retournée contient un seul élément. On gère quand même le cas ou la liste retournée serait vide.
    return ManagerServiceStock.chargerStockDetaille(pIdSession, pIdEtablissement, pIdMagasin, pIdArticle);
  }
  
  /**
   * Charger le stock détaillé d'un article pour un établissement.
   * 
   * Le stock des magasins est cumulé pour fournir le stock sur l'ensemble de l'établissement.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pIdArticle Identifiant de l'article dont on souhaite charger le stock détaillé.
   * @return Stock de l'établissement (tous les magasins de l'établissement cumulés).
   */
  public static StockDetaille chargerStockDetaillee(IdSession pIdSession, IdEtablissement pIdEtablissement, IdArticle pIdArticle) {
    return chargerStockDetaille(pIdSession, pIdEtablissement, null, pIdArticle);
  }
  
  /**
   * Charger le stock détaillé d'un article pour un magasin.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdMagasin Identifiant du magasin.
   * @param pIdArticle Identifiant de l'article dont on souhaite charger le stock détaillé.
   * @return Stock du magasin.
   */
  public static StockDetaille chargerStockDetaillee(IdSession pIdSession, IdMagasin pIdMagasin, IdArticle pIdArticle) {
    IdMagasin.controlerId(pIdMagasin, true);
    return chargerStockDetaille(pIdSession, pIdMagasin.getIdEtablissement(), pIdMagasin, pIdArticle);
  }
  
  /**
   * Calculer les valeurs de stock détaillées.
   * 
   * @param pDateMiniReappro Date de prochain réapprovisionnement possible.
   */
  public void calculerStock() {
    listeStockAttenduCommande.calculerStock(getId().getIdArticle(), stock.getStockPhysique(), new Date(), dateMiniReappro);
  }
  
  /**
   * Texte résumant les informations de l'objet métier à destination du presse-papier.
   * @return Texte destiné au presse-papier.
   */
  public String getTextePressePapier() {
    // Ajouter l'identifiant
    String textePressePapier = "Id StockDetaille = " + id.getTexte() + "\n";
    
    // Ajouter les données de l'onglet principal
    textePressePapier += "Stock physique = " + getStockPhysique() + "\n";
    textePressePapier += "Stock commande = " + getStockCommande() + "\n";
    textePressePapier += "Stock reserve = " + getStockReserve() + "\n";
    textePressePapier += "Stock net = " + getStockNet() + "\n";
    textePressePapier += "Stock attendu = " + getStockAttendu() + "\n";
    textePressePapier += "Stock theorique = " + getStockTheorique() + "\n";
    textePressePapier += "Stock commande lie sur achat = " + getStockCommandeLieSurAchat() + "\n";
    textePressePapier += "Stock disponible = " + getStockDisponible() + "\n";
    textePressePapier += "Stock avant rupture = " + getStockAvantRupture() + "\n";
    textePressePapier += "Date rupture possible = " + getDateRupturePossible() + "\n";
    textePressePapier += "Date fin rupture = " + getDateFinRupture() + "\n";
    
    // Ajouter la liste des attendus/commandés
    if (listeStockAttenduCommande != null) {
      textePressePapier += "Liste des attendus et commandés =\n";
      for (StockAttenduCommande stockAttenduCommande : listeStockAttenduCommande) {
        textePressePapier += Constantes.convertirDateEnTexte(stockAttenduCommande.getDateReference()) + "|";
        textePressePapier += stockAttenduCommande.getQuantiteEntree() + "|";
        textePressePapier += stockAttenduCommande.getQuantiteSortie() + "|";
        textePressePapier += stockAttenduCommande.getStockTheoriqueADate() + "|";
        if (stockAttenduCommande.getStockRestantAvantAttendu() != null) {
          textePressePapier += stockAttenduCommande.getStockRestantAvantAttendu() + "|";
        }
        else {
          textePressePapier += "|";
        }
        if (stockAttenduCommande.getStockAvantRupture() != null) {
          textePressePapier += stockAttenduCommande.getStockAvantRupture() + "|";
        }
        else {
          textePressePapier += "|";
        }
        if (stockAttenduCommande.getId().getIdMagasin() != null) {
          textePressePapier += stockAttenduCommande.getId().getIdMagasin().getCode() + "|";
        }
        else {
          textePressePapier += "|";
        }
        if (stockAttenduCommande.getId().getIdLigneAchat() != null) {
          textePressePapier += stockAttenduCommande.getId().getIdLigneAchat() + "|";
        }
        else if (stockAttenduCommande.getId().getIdLigneVente() != null) {
          textePressePapier += stockAttenduCommande.getId().getIdLigneVente() + "|";
        }
        else {
          textePressePapier += "|";
        }
        textePressePapier += Constantes.convertirDateEnTexte(stockAttenduCommande.getDateValidation()) + "|";
        textePressePapier += stockAttenduCommande.getLibelleTiers() + "\n";
      }
    }
    
    // Ajouter la liste des mouvements de stock
    if (listeStockMouvement != null) {
      textePressePapier += "Liste des mouvements de stock =\n";
      for (StockMouvement stockMouvement : listeStockMouvement) {
        textePressePapier += Constantes.convertirDateEnTexte(stockMouvement.getId().getDate()) + "|";
        textePressePapier += stockMouvement.getId().getNumeroOrdre() + "|";
        textePressePapier += stockMouvement.getId().getCodeMagasin() + "|";
        textePressePapier += stockMouvement.getTexteLigneDocument() + "|";
        textePressePapier += stockMouvement.getLibelleTiers() + "|";
        textePressePapier += stockMouvement.getType().getLibelle() + "|";
        textePressePapier += stockMouvement.getOrigine().getLibelle() + "|";
        if (stockMouvement.getQuantiteEntree() != null) {
          textePressePapier += stockMouvement.getQuantiteEntree() + "|";
        }
        else {
          textePressePapier += "|";
        }
        if (stockMouvement.getQuantiteSortie() != null) {
          textePressePapier += stockMouvement.getQuantiteSortie() + "|";
        }
        else {
          textePressePapier += "|";
        }
        textePressePapier += stockMouvement.getStockPhysique() + "\n";
      }
    }
    
    // Ajouter la liste des stocks par magasin
    if (listeStockMagasin != null) {
      textePressePapier += "Liste des stocks par magasin =\n";
      for (Stock stock : listeStockMagasin) {
        textePressePapier += stock.getId().getIdMagasin() + "|";
        textePressePapier += stock.getStockPhysique() + "|";
        textePressePapier += stock.getStockCommande() + "|";
        textePressePapier += stock.getStockReserve() + "|";
        textePressePapier += stock.getStockNet() + "|";
        textePressePapier += stock.getStockAttendu() + "|";
        textePressePapier += stock.getStockTheorique() + "|";
        textePressePapier += stock.getStockDisponible() + "\n";
      }
    }
    return textePressePapier;
  }
  
  /**
   * Retourner le stock de l'article.
   * 
   * @return Stock de l'article.
   */
  public Stock getStock() {
    return stock;
  }
  
  /**
   * Modifier le stock de l'article.
   * 
   * Noter qu'on pourrait déduire le stock du magasin ou le stock de l'établissement à partir du stock détaillé par magasin
   * mais on préfère utiliser la méthode standard pour récupérer le stock afin de faire des recoupements et détecter
   * les écarts éventuels entre les différentes algorithmes.
   * 
   * @param pStock Stock de l'article.
   */
  public void setStock(Stock pStock) {
    stock = pStock;
  }
  
  /**
   * Retourner la liste des stocks de l'article par magasin.
   * @return Liste des stocks par magasin.
   */
  public ListeStock getListeStockMagasin() {
    return listeStockMagasin;
  }
  
  /**
   * Modifier la liste des stocks de l'article par magasin.
   * @param pListeStockMagasin Liste des stocks par magasin.
   */
  public void setListeStockMagasin(ListeStock pListeStockMagasin) {
    listeStockMagasin = pListeStockMagasin;
  }
  
  /**
   * Retourner le stock de l'article pour l'établissement.
   * 
   * @return Stock de l'article.
   */
  public Stock getStockEtablissement() {
    if (listeStockMagasin == null) {
      return null;
    }
    return listeStockMagasin.get(getId().getIdEtablissement(), getId().getIdArticle());
  }
  
  /**
   * 
   * Retourner la liste des attendus/commandés.
   * 
   * @return ListeStockAttenduCommande
   */
  public ListeStockAttenduCommande getListeStockAttenduCommande() {
    return listeStockAttenduCommande;
  }
  
  /**
   * 
   * Modifier la liste des attendus/commandés.
   * 
   * @param pListeStockAttenduCommande
   */
  public void setListeStockAttenduCommande(ListeStockAttenduCommande pListeStockAttenduCommande) {
    listeStockAttenduCommande = pListeStockAttenduCommande;
  }
  
  /**
   * 
   * Retourner la liste des mouvements de stock.
   * 
   * @return ListeStockMouvement
   */
  public ListeStockMouvement getListeStockMouvement() {
    return listeStockMouvement;
  }
  
  /**
   * 
   * Modifier la liste des mouvements de stock.
   * 
   * @param ListeStockMouvement
   */
  public void setListeStockMouvement(ListeStockMouvement pListeStockMouvement) {
    listeStockMouvement = pListeStockMouvement;
  }
  
  /**
   * Retourner le stock physique.
   * 
   * Le stock physique comptabilise l’ensemble des articles présents physiquement. Ce sont les articles qui ont été réceptionnés et
   * n'ont pas encore été livrés à un client. Ils peuvent cependant avoir été réservés ou même commandés par un client mais ils sont
   * toujours présents.
   * 
   * @return Stock physique.
   */
  public BigDecimal getStockPhysique() {
    if (stock == null) {
      return null;
    }
    return stock.getStockPhysique();
  }
  
  /**
   * Indiquer si le stock physique issu des mouvements de stock est égal au stock physique lu dans la table stock.
   * @return true=pas d'écart, false=écart détecté.
   */
  public boolean isStockPhysiqueCorrect() {
    // Ne pas retourner d'erreur si le stock n'est pas chargé ou si c'est un stock par établissement
    if (stock == null || listeStockMouvement == null || getId().getIdMagasin() == null) {
      return true;
    }
    
    // Comparer les résultats
    return Constantes.equals(stock.getStockPhysique(), listeStockMouvement.getDernierStockPhysique(getId().getIdMagasin()));
  }
  
  /**
   * Retourner le stock attendu.
   * 
   * Le stock attendu est la quantité d’articles commandés aux fournisseurs et non encore réceptionnés, quelle que soit la date de
   * commande. Ces commandes viendront incrémenter le stock physique de l'article lorsqu'elles seront réceptionnées. Les commandes
   * fournisseurs « Direct usine » ne sont pas comptées dans les attendus.
   * 
   * @return Stock attendu.
   */
  public BigDecimal getStockAttendu() {
    if (listeStockAttenduCommande == null) {
      return null;
    }
    return listeStockAttenduCommande.getStockAttendu();
  }
  
  /**
   * Indiquer si le stock attendu calculé via les attendus/commandés est égal au stock attendu lu dans la table stock.
   * @return true=pas d'écart, false=écart détecté.
   */
  public boolean isStockAttenduCorrect() {
    if (stock == null) {
      return true;
    }
    return Constantes.equals(stock.getStockAttendu(), getStockAttendu());
  }
  
  /**
   * Retourner le stock commandé.
   * 
   * Le stock commandé est la quantité d’articles commandés ou réservés par les clients, quelle que soit la date de commande ou de
   * réservation. Ce sont des articles sur lesquels un client a déjà mis une option. Pour déterminer la quantité commandée, il faut faire
   * le total des commandes clients avec l'état VAL ou RES. Les commandes clients « Direct usine » ne sont pas comptées dans les
   * commandés.
   * 
   * @return Stock commandé.
   */
  public BigDecimal getStockCommande() {
    if (listeStockAttenduCommande == null) {
      return null;
    }
    return listeStockAttenduCommande.getStockCommande();
  }
  
  /**
   * Indiquer si le stock commandé calculé via les attendus/commandés est égal au stock commandé lu dans la table stock.
   * @return true=pas d'écart, false=écart détecté.
   */
  public boolean isStockCommandeCorrect() {
    if (stock == null) {
      return true;
    }
    return Constantes.equals(stock.getStockCommande(), getStockCommande());
  }
  
  /**
   * Retourner le stock commandé lié sur achats.
   * 
   * Le commandé lié sur achat est la quantité d’articles commandés par les clients et qui sont liés à une commande fournisseur.
   * Ce lien peut être de 2 natures :
   * - Soit par un GBA (génération de bon d’achat), c’est à dire qu’un bon d’achat est généré en contrepartie d’une vente.
   * - Soit par une affectation, c’est à dire que la quantité vendue au client est réservée sur un bon d’achat existant mais non
   * encore réceptionné.
   *
   * @return Stock commandé lié sur achats.
   */
  public BigDecimal getStockCommandeLieSurAchat() {
    // Algorithme à compléter
    if (stock == null) {
      return null;
    }
    return stock.getStockCommandeLieSurAchat();
  }
  
  /**
   * Retourner le stock réservé.
   * 
   * Le stock réservé est la quantité d'article réservée pour des clients. Pour réserver un article, il faut valider une commande grâce
   * à l’option « Réservation (RES) ». Pour déterminer la quantité réservée, il faut faire le total des commandes avec l'état RES.
   * Une quantité réservée ne peut être utilisée dans une autre vente si le paramétrage l’interdit (voir PS067). Noter que le stock
   * réservé est également comptabilisé dans le stock commandé.
   * 
   * @return Stock réservé.
   */
  public BigDecimal getStockReserve() {
    if (listeStockAttenduCommande == null) {
      return null;
    }
    return listeStockAttenduCommande.getStockReserve();
  }
  
  /**
   * Indiquer si le stock réservé calculé via les attendus/commandés est égal au stock réservé lu dans la table stock.
   * @return true=pas d'écart, false=écart détecté.
   */
  public boolean isStockReserveCorrect() {
    if (stock == null) {
      return true;
    }
    return Constantes.equals(stock.getStockReserve(), getStockReserve());
  }
  
  /**
   * Retourner le stock théorique.
   * 
   * Le stock théorique correspond au stock physique qui sera atteint lorsque toutes les commandes fournisseurs en cours auront été
   * réceptionnées et que toutes les commandes clients en cours auront été livrées.
   * Le stock théorique, également nommé disponible achat, est une quantité intéressante pour les acheteurs car elle intègre les attendus
   * fournisseurs. Cela évite de sur-commander puisque cette valeur intègre les quantités restantes à réceptionner.
   * 
   * Stock théorique = Stock physique + Stock attendu - Stock commandé
   * 
   * @return Stock théorique.
   */
  public BigDecimal getStockTheorique() {
    if (getStockPhysique() == null || getStockAttendu() == null || getStockCommande() == null) {
      return null;
    }
    return getStockPhysique().add(getStockAttendu()).subtract(getStockCommande());
  }
  
  /**
   * Retourner le stock net.
   * 
   * Le stock net correspond au stock physique moins l'ensemble des commandes clients qui n’ont pas encore été traitées. Ce sont les
   * articles présents physiquement dont on décompte les articles déjà commandés ou réservés par des clients. Ici, on décide d'ignorer
   * les commandes fournisseurs tant qu'elles n'ont pas été réceptionnées. Par contre, on prend en compte toutes les commandes clients
   * quelques soient leur date. C'est une approche prudente pour pouvoir annoncer au client un stock dont on est sûr qu'il sera
   * disponible pour la vente. Le stock net est également nommé disponible vente. Cela illustre que cette quantité est intéressante du
   * point de vue vendeur pour savoir quelle quantité il reste à vendre.
   * 
   * Stock net = Stock physique - Stock commandé
   * 
   * @return Stock net.
   */
  public BigDecimal getStockNet() {
    if (getStockPhysique() == null || getStockCommande() == null) {
      return null;
    }
    return getStockPhysique().subtract(getStockCommande());
  }
  
  /**
   * Retourner le stock disponible.
   * 
   * Le stock net est donc le stock physique moins le stock commandé. Un problème avec ce calcul concerne les commandes clients associées
   * à un attendu fournisseur. Les commandes clients sont décomptées du stock net mais les attendus fournisseurs correspondantes sont
   * ignorés. Cela donne une vision du stock disponible plus défavorable que la réalité.
   * 
   * La formule du stock disponible ci-dessous apporte une réponse à ce problème. C’est la même formule que le stock net si ce n’est
   * qu’on y réintègre les commandes clients liées à des attendus fournisseurs. L’idée est de ne pas décompter du stock disponible des
   * commandes clients qui sont couvertes par des attendus fournisseurs à venir.
   * 
   * Stock disponible = Stock physique - (Stock commandé - Stock commandé lié sur achat)
   * 
   * @return Stock disponible.
   */
  public BigDecimal getStockDisponible() {
    if (getStockPhysique() == null || getStockCommande() == null || getStockCommandeLieSurAchat() == null) {
      return null;
    }
    return getStockPhysique().subtract(getStockCommande()).add(getStockCommandeLieSurAchat());
  }
  
  /**
   * Retourner le stock avant rupture.
   * 
   * Quantité correspondant au stock avant rupture de l’article en fonction des attendus et commandés à venir.
   * 
   * @return Stock avant rupture.
   */
  public BigDecimal getStockAvantRupture() {
    if (listeStockAttenduCommande == null) {
      return null;
    }
    return listeStockAttenduCommande.getStockAvantRupture();
  }
  
  /**
   * Retourner la date de rupture possible..
   * 
   * Date à laquelle le stock de l’article passera en rupture (quantité négative ou nulle) si on vend une quantité d’articles supérieure
   * ou égale au stock avant rupture. Par exemple, si le stock minimum est atteint le 19/10/2022 avec 292 pièces, le stock avant rupture
   * sera 292 et la date de rupture possible sera le 19/10/2022.
   * 
   * @return Date de rupture possible.
   */
  public Date getDateRupturePossible() {
    if (listeStockAttenduCommande == null) {
      return null;
    }
    return listeStockAttenduCommande.getDateRupturePossible();
  }
  
  /**
   * Retourner la date de fin de rupture.
   * 
   * Date à laquelle le stock avant rupture repasse à une valeur positive. Cette date n’est renseignée que si le stock avant rupture
   * est négatif.
   * 
   * @return Date de fin de rupture.
   */
  public Date getDateFinRupture() {
    if (listeStockAttenduCommande == null) {
      return null;
    }
    return listeStockAttenduCommande.getDateFinRupture();
  }
  
  /**
   * Retourner la date minimum du prochain réapprovisionnement possible.
   * 
   * Cette date est fixé à partir des délais de réapprovisionnement fournisseurs des conditions d'achats.
   * 
   * @return Date minimum du prochain réapprovisionnement possible.
   */
  public Date getDateMiniReappro() {
    if (listeStockAttenduCommande == null) {
      return null;
    }
    return dateMiniReappro;
  }
  
  /**
   * Modifier la date minimum du prochain réapprovisionnement possible.
   * 
   * @param pDateMiniReappro Date minimum de prochain réapprovisionnement possible.
   */
  public void setDateMiniReappro(Date pDateMiniReappro) {
    dateMiniReappro = pDateMiniReappro;
  }
  
  /**
   * Générer un message d'erreur décrivant les écarts rencontrés entre les différentes méthodes d'obtention du stock.
   * @return Message d'erreur.
   */
  public Message getMessageErreur() {
    String texteMessageErreur = "";
    if (!isStockPhysiqueCorrect()) {
      BigDecimal stockPhysique = null;
      if (listeStockMouvement != null) {
        stockPhysique = listeStockMouvement.getDernierStockPhysique(getId().getIdMagasin());
      }
      texteMessageErreur += "Ecart entre le stock physique de la table stock (" + Constantes.formater(stock.getStockPhysique(), false)
          + ") et le stock physique du dernier mouvement de stock (" + Constantes.formater(stockPhysique, false) + ").\n";
    }
    if (!isStockCommandeCorrect()) {
      texteMessageErreur += "Ecart entre le stock commandé de la table stock (" + Constantes.formater(stock.getStockCommande(), false)
          + ") et le stock commandé calculé à partir des attendus/commandés (" + Constantes.formater(getStockCommande(), false) + ").\n";
    }
    if (!isStockReserveCorrect()) {
      texteMessageErreur += "Ecart entre le stock réservé de la table stock (" + Constantes.formater(stock.getStockReserve(), false)
          + ") et le stock réservé calculé à partir des attendus/commandés (" + Constantes.formater(getStockReserve(), false) + ").\n";
    }
    if (!isStockAttenduCorrect()) {
      texteMessageErreur += "Ecart entre le stock attendu de la table stock (" + Constantes.formater(stock.getStockAttendu(), false)
          + ") et le stock attendu calculé à partir des attendus/commandés (" + Constantes.formater(getStockAttendu(), false) + ").\n";
    }
    return Message.getMessageImportant(texteMessageErreur);
  }
}
