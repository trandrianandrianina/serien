/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.MessageErreurException;

public class LigneAttenduCommande implements Serializable {
  // Constantes
  public static final char ORIGINE_LIGNE_ACHAT = 'A';
  public static final char ORIGINE_LIGNE_VENTE = 'V';
  public static final char ORIGINE_LIGNE_OF = 'O';
  
  // Variables
  private IdArticle idArticle = null; // Code article
  private String libelleArticle = null;
  private IdMagasin idMagasin = null; // Code magasin demande
  private String codeUniteStock = null;
  private Integer nombreDecimaleUS = null;
  private EnumAttenduCommande typefiltrage = null;
  // Variable qui en fonction de l'origine de la ligne contiendra soit la quantité attendu soit la quantité comandée
  private BigDecimal quantiteAttendueCommandeeUS = null;
  private String nomTiersMouvement = null; // nom du tiers concerné par le mouvement
  private Date dateValidite = null; // date de validité
  private Date dateLivraisonPrevue = null; // date de livraison prévue
  private Date dateLivraisonSouhaitee = null; // date de livraison souhaitée
  private Character origineLigne = null;
  
  // Les informations de la ligne de vente
  private IdLigneVente idLigneVente = null;
  private IdClient idClientLivre = null;
  private String nomClientFacture = null;
  private Date dateReservation = null; // date de réservation (commandé)
  private IdDocumentVente idDocumentVente = null;
  private IdDocumentAchat idDocumentAchat = null;
  
  // Les informations de la ligne d'achat
  private IdLigneAchat idLigneAchat = null;
  private IdFournisseur idFournisseur = null;
  private Integer adresseCommandeFournisseur = null;
  private Date dateConfirmation = null; // date de confirmation (attendu)
  
  // Les informations de la ligne liée achat ou vente
  private Character origineLigneLien = null;
  
  private IdLigneVente idLigneVenteLien = null;
  private IdClient idClientLivreLien = null;
  
  private IdLigneAchat idLigneAchatLien = null;
  private Integer adresseFournisseurLien = null;
  private IdFournisseur idFournisseurLien = null;
  
  private Integer codeEtatDocumentLien = null;
  private BigDecimal quantiteAffecteeUS = null;
  private BigDecimal quantiteStockGlisse = null;
  private String nomDocumentLien = null;
  
  // -- Méthodes publiques
  
  /**
   * Initialise les données si l'origine est une ligne de vente.
   */
  public void initialiserLigneVenteOrigine(IdLigneVente pIdLigneVente, IdClient pIdClientLivre, Date pDateReservation) {
    if (origineLigne == null || !origineLigne.equals(ORIGINE_LIGNE_VENTE)) {
      throw new MessageErreurException("La ligne d'origine n'est pas une ligne de vente.");
    }
    idLigneVente = pIdLigneVente;
    idClientLivre = pIdClientLivre;
    dateReservation = pDateReservation;
  }
  
  /**
   * Initialise les données si l'origine est une ligne d'achat.
   */
  public void initialiserLigneAchatOrigine(IdLigneAchat pIdLigneAchat, IdFournisseur pIdFournisseur, Integer pAdresseCommandeFournisseur,
      Date pDateConfirmation) {
    if (origineLigne == null || !origineLigne.equals(ORIGINE_LIGNE_ACHAT)) {
      throw new MessageErreurException("La ligne d'origine n'est pas une ligne d'achat.");
    }
    idLigneAchat = pIdLigneAchat;
    idFournisseur = pIdFournisseur;
    adresseCommandeFournisseur = pAdresseCommandeFournisseur;
    dateConfirmation = pDateConfirmation;
  }
  
  /**
   * Initialise les données si le lien est une ligne de vente.
   */
  public void initialiserLigneVenteLien(IdLigneVente pIdLigneVenteLien, IdClient pIdClientLivreLien) {
    if (origineLigneLien == null || !origineLigneLien.equals(ORIGINE_LIGNE_VENTE)) {
      throw new MessageErreurException("La ligne liée n'est pas une ligne de vente.");
    }
    idLigneVenteLien = pIdLigneVenteLien;
    idClientLivreLien = pIdClientLivreLien;
  }
  
  /**
   * Initialise les données si le lien est une ligne d'achat.
   */
  public void initialiserLigneAchatLien(IdLigneAchat pIdLigneAchatLien, IdFournisseur pIdFournisseurLien,
      Integer pAdresseFournisseurLien) {
    if (origineLigneLien == null || !origineLigneLien.equals(ORIGINE_LIGNE_ACHAT)) {
      throw new MessageErreurException("La ligne liée n'est pas une ligne d'achat.");
    }
    idLigneAchatLien = pIdLigneAchatLien;
    adresseFournisseurLien = pAdresseFournisseurLien;
    idFournisseurLien = pIdFournisseurLien;
  }
  
  // -- Accesseurs
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public void setIdArticle(IdArticle idArticle) {
    this.idArticle = idArticle;
  }
  
  public String getLibelleArticle() {
    return libelleArticle;
  }
  
  public void setLibelleArticle(String libelleArticle) {
    this.libelleArticle = libelleArticle;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin idMagasin) {
    this.idMagasin = idMagasin;
  }
  
  public String getCodeUniteStock() {
    return codeUniteStock;
  }
  
  public void setCodeUniteStock(String codeUniteStock) {
    this.codeUniteStock = codeUniteStock;
  }
  
  public Integer getNombreDecimaleUS() {
    return nombreDecimaleUS;
  }
  
  public void setNombreDecimaleUS(Integer nombreDecimaleUS) {
    this.nombreDecimaleUS = nombreDecimaleUS;
  }
  
  public EnumAttenduCommande getTypefiltrage() {
    return typefiltrage;
  }
  
  public void setTypefiltrage(EnumAttenduCommande typefiltrage) {
    this.typefiltrage = typefiltrage;
  }
  
  public BigDecimal getQuantiteAttendueUS() {
    return quantiteAttendueCommandeeUS;
  }
  
  public BigDecimal getQuantiteCommandeeUS() {
    return quantiteAttendueCommandeeUS;
  }
  
  public void setQuantiteAttendueCommandeeUS(BigDecimal quantiteAttendueCommandeeUS) {
    if (nombreDecimaleUS != null) {
      this.quantiteAttendueCommandeeUS = quantiteAttendueCommandeeUS.setScale(nombreDecimaleUS, RoundingMode.HALF_UP);
    }
    else {
      this.quantiteAttendueCommandeeUS = quantiteAttendueCommandeeUS;
    }
  }
  
  public String getNomTiersMouvement() {
    return nomTiersMouvement;
  }
  
  public void setNomTiersMouvement(String nomTiersMouvement) {
    this.nomTiersMouvement = nomTiersMouvement;
  }
  
  public Date getDateValidite() {
    return dateValidite;
  }
  
  public void setDateValidite(Date dateValidite) {
    this.dateValidite = dateValidite;
  }
  
  public Date getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  public void setDateLivraisonPrevue(Date dateLivraisonPrevue) {
    this.dateLivraisonPrevue = dateLivraisonPrevue;
  }
  
  public Date getDateLivraisonSouhaitee() {
    return dateLivraisonSouhaitee;
  }
  
  public void setDateLivraisonSouhaitee(Date dateLivraisonSouhaitee) {
    this.dateLivraisonSouhaitee = dateLivraisonSouhaitee;
  }
  
  public Character getOrigineLigneLien() {
    return origineLigneLien;
  }
  
  public void setOrigineLigneLien(Character origineLigneLien) {
    this.origineLigneLien = origineLigneLien;
  }
  
  public Character getOrigineLigne() {
    return origineLigne;
  }
  
  public void setOrigineLigne(Character origineLigne) {
    this.origineLigne = origineLigne;
  }
  
  public IdLigneVente getIdLigneVente() {
    return idLigneVente;
  }
  
  public IdClient getIdClientLivre() {
    return idClientLivre;
  }
  
  public Date getDateReservation() {
    return dateReservation;
  }
  
  public IdLigneAchat getIdLigneAchat() {
    return idLigneAchat;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public Integer getAdresseCommandeFournisseur() {
    return adresseCommandeFournisseur;
  }
  
  public Date getDateConfirmation() {
    return dateConfirmation;
  }
  
  public IdLigneVente getIdLigneVenteLien() {
    return idLigneVenteLien;
  }
  
  public IdClient getIdClientLivreLien() {
    return idClientLivreLien;
  }
  
  public IdLigneAchat getIdLigneAchatLien() {
    return idLigneAchatLien;
  }
  
  public Integer getAdresseFournisseurLien() {
    return adresseFournisseurLien;
  }
  
  public IdFournisseur getIdFournisseurLien() {
    return idFournisseurLien;
  }
  
  public Integer getCodeEtatDocumentLien() {
    return codeEtatDocumentLien;
  }
  
  public void setCodeEtatDocumentLien(Integer codeEtatDocumentLien) {
    this.codeEtatDocumentLien = codeEtatDocumentLien;
  }
  
  public BigDecimal getQuantiteAffecteeUS() {
    return quantiteAffecteeUS;
  }
  
  public void setQuantiteAffecteeUS(BigDecimal quantiteAffecteeUS) {
    if (nombreDecimaleUS != null) {
      this.quantiteAffecteeUS = quantiteAffecteeUS.setScale(nombreDecimaleUS, RoundingMode.HALF_UP);
    }
    else {
      this.quantiteAffecteeUS = quantiteAffecteeUS;
    }
  }
  
  public String getNomDocumentLien() {
    return nomDocumentLien;
  }
  
  public void setNomDocumentLien(String nomDocumentLien) {
    this.nomDocumentLien = nomDocumentLien;
  }
  
  public String getNomClientFacture() {
    return nomClientFacture;
  }
  
  public void setNomClientFacture(String nomClientFacture) {
    this.nomClientFacture = nomClientFacture;
  }
  
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  public void setIdDocumentVente(IdDocumentVente idDocumentVente) {
    this.idDocumentVente = idDocumentVente;
  }
  
  public BigDecimal getQuantiteStockGlisse() {
    return quantiteStockGlisse;
  }
  
  public void setQuantiteStockGlisse(BigDecimal pQuantiteStockGlisse) {
    if (nombreDecimaleUS != null) {
      quantiteStockGlisse = pQuantiteStockGlisse.setScale(nombreDecimaleUS, RoundingMode.HALF_UP);
    }
    else {
      quantiteStockGlisse = pQuantiteStockGlisse;
    }
  }
  
  public IdDocumentAchat getIdDocumentAchat() {
    return idDocumentAchat;
  }
  
  public void setIdDocumentAchat(IdDocumentAchat idDocumentAchat) {
    this.idDocumentAchat = idDocumentAchat;
  }
  
}
