/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de CategorieZonePersonnalisee
 */
public class ListeCategorieZonePersonnalisee
    extends ListeClasseMetier<IdCategorieZonePersonnalisee, CategorieZonePersonnalisee, ListeCategorieZonePersonnalisee> {
  /**
   * Constructeur.
   */
  public ListeCategorieZonePersonnalisee() {
  }
  
  /**
   * Charge les catégories de ZP correspondant aux identifiants passés en paramètre.
   */
  @Override
  public ListeCategorieZonePersonnalisee charger(IdSession pIdSession, List<IdCategorieZonePersonnalisee> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charge toutes les catégories de ZP de l'établissement donnée.
   */
  @Override
  public ListeCategorieZonePersonnalisee charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CritereCategorieZonePersonnalisee critere = new CritereCategorieZonePersonnalisee();
    critere.setIdEtablissement(pIdEtablissement);
    ListeCategorieZonePersonnalisee listeZP = ManagerServiceParametre.chargerListeCategorieZonePersonnalisee(pIdSession, critere);
    return listeZP;
  }
  
  /**
   * Retourner une catégorie de ZP présente dans la liste sur la base de son identifiant.
   */
  public CategorieZonePersonnalisee retournerCategorieZonePersonnaliseeParId(IdCategorieZonePersonnalisee pIdCategorieZonePersonnalisee) {
    if (pIdCategorieZonePersonnalisee == null) {
      return null;
    }
    for (CategorieZonePersonnalisee categorieZP : this) {
      if (categorieZP != null && Constantes.equals(categorieZP.getId(), pIdCategorieZonePersonnalisee)) {
        return categorieZP;
      }
    }
    
    return null;
  }
  
  /**
   * Retourner une catégorie de ZP présente dans la liste sur la base de son code.
   */
  public CategorieZonePersonnalisee retournerCategorieZonePersonnaliseeParCode(String pCodeCategorieZonePersonnalisee) {
    if (pCodeCategorieZonePersonnalisee == null || pCodeCategorieZonePersonnalisee.trim().isEmpty()) {
      return null;
    }
    for (CategorieZonePersonnalisee categorieZP : this) {
      if (categorieZP != null && Constantes.equals(categorieZP.getId().getCode(), pCodeCategorieZonePersonnalisee)) {
        return categorieZP;
      }
    }
    
    return null;
  }
  
}
