/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.sipe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Cette classe contient un ensemble de critères qui permettent la recherche un plan de pose Sipe.
 */
public class CritereSipe implements Serializable {
  // Variables
  private List<EnumStatutSipe> listeStatut = null;
  private Date dateDebutPlan = null;
  private Date dateFinPlan = null;
  
  // -- Accesseurs
  
  /**
   * Retourne le filtre sur la liste des statuts.
   * 
   * @return
   */
  public List<EnumStatutSipe> getListeStatut() {
    return listeStatut;
  }
  
  /**
   * Ajoute un statut à la liste des statuts.
   * 
   * @param pStatut
   */
  public void ajouterStatut(EnumStatutSipe pStatut) {
    if (listeStatut == null) {
      listeStatut = new ArrayList<EnumStatutSipe>();
    }
    if (pStatut != null && !listeStatut.contains(pStatut)) {
      listeStatut.add(pStatut);
    }
  }
  
  /**
   * Efface la liste des statuts.
   */
  public void effacerListeStatut() {
    if (listeStatut == null) {
      return;
    }
    listeStatut.clear();
  }
  
  /**
   * Retourne le filtre sur la date de début du plan.
   * 
   * @return
   */
  public Date getDateDebutPlan() {
    return dateDebutPlan;
  }
  
  /**
   * Initialise le filtre sur la date de début du plan.
   * 
   * @param pDateDebutPlan
   */
  public void setDateDebutPlan(Date pDateDebutPlan) {
    this.dateDebutPlan = pDateDebutPlan;
  }
  
  /**
   * Retourne le filtre sur la date de fin du plan.
   * 
   * @return
   */
  public Date getDateFinPlan() {
    return dateFinPlan;
  }
  
  /**
   * Initialise le filtre sur la date de fin du plan.
   * 
   * @param pDateFinPlan
   */
  public void setDateFinPlan(Date pDateFinPlan) {
    this.dateFinPlan = pDateFinPlan;
  }
  
}
