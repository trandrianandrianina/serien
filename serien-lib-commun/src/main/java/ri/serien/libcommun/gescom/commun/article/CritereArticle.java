/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.io.Serializable;

import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.famille.IdFamille;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

/**
 * Critères de recherche des articles.
 * 
 * Si le document est direct usine et que le logiciel est paramétré en direct-usine mono-fournisseur, le fournisseur doit être identique
 * pour tous les articles.
 */
public class CritereArticle implements Serializable {
  // Variables
  private IdEtablissement idEtablissement = null;
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  private EnumFiltreHorsGamme filtreHorsGamme = EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES;
  private EnumFiltreArticleSpecial filtreSpecial = EnumFiltreArticleSpecial.OPTION_TOUS_LES_ARTICLES_SAUF_SPECIAUX;
  private EnumFiltreDirectUsine filtreDirectUsine = EnumFiltreDirectUsine.OPTION_TOUS_LES_ARTICLES;
  private EnumFiltreArticleStock filtreStock = EnumFiltreArticleStock.OPTION_TOUS_LES_ARTICLES;
  private EnumFiltreArticleCommentaire filtreCommentaire = EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES_SAUF_COMMENTAIRES;
  private EnumFiltreArticleConsigne filtreSConsigne = EnumFiltreArticleConsigne.OPTION_TOUS_LES_ARTICLES_SAUF_CONSIGNE;
  private IdFournisseur idFournisseur;
  private IdMagasin idMagasin;
  private IdFamille idFamille = null;
  private Boolean isArticleValideSeulement = false;
  private Boolean verrouillerCritere = false;
  
  /**
   * Initialiser les valeurs du critère
   */
  public void initialiser() {
    if (!isVerrouillerCritere()) {
      filtreHorsGamme = EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES;
      filtreSpecial = EnumFiltreArticleSpecial.OPTION_TOUS_LES_ARTICLES;
      filtreDirectUsine = EnumFiltreDirectUsine.OPTION_TOUS_LES_ARTICLES;
      filtreStock = EnumFiltreArticleStock.OPTION_TOUS_LES_ARTICLES;
      filtreCommentaire = EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES_SAUF_COMMENTAIRES;
      filtreSConsigne = EnumFiltreArticleConsigne.OPTION_TOUS_LES_ARTICLES_SAUF_CONSIGNE;
    }
    idFournisseur = null;
    idMagasin = null;
    idFamille = null;
    rechercheGenerique.initialiser();
  }
  
  // Accesseurs
  
  /**
   * Retourner l'identifiant de l'établissement
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'identifiant de l'établissement
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void setTexteRechercheGenerique(String pTexteRechercheGenerique) {
    rechercheGenerique.setTexte(pTexteRechercheGenerique);
  }
  
  /**
   * Retourner le filtre de recherche des articles hors gamme
   */
  public EnumFiltreHorsGamme getFiltreHorsGamme() {
    return filtreHorsGamme;
  }
  
  /**
   * Modifier le filtre de recherche des articles hors gamme
   */
  public void setHorsGamme(EnumFiltreHorsGamme pHorsGamme) {
    filtreHorsGamme = pHorsGamme;
  }
  
  /**
   * Retourner le filtre de recherche des articles spéciaux
   */
  public EnumFiltreArticleSpecial getFiltreSpecial() {
    return filtreSpecial;
  }
  
  /**
   * Modifier le filtre de recherche des articles spéciaux
   */
  public void setFiltreSpecial(EnumFiltreArticleSpecial pSpecial) {
    filtreSpecial = pSpecial;
  }
  
  /**
   * Retourner le filtre de recherche des articles direct usine
   */
  public EnumFiltreDirectUsine getDirectUsine() {
    return filtreDirectUsine;
  }
  
  /**
   * Modifier le filtre de recherche des articles direct usine
   */
  public void setDirectUsine(EnumFiltreDirectUsine pDirectUsine) {
    filtreDirectUsine = pDirectUsine;
  }
  
  /**
   * Retourner le filtre sur l'identifiant du fournisseur
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Modifier le filtre sur l'identifiant du fournisseur
   */
  public void setIdFournisseur(IdFournisseur pIdFournisseur) {
    idFournisseur = pIdFournisseur;
  }
  
  /**
   * Retourner le filtre sur l'identifiant du magasin
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier le filtre sur l'identifiant du magasin
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  /**
   * Retourner le filtre sur l'identifiant de la famille
   */
  public IdFamille getIdFamille() {
    return idFamille;
  }
  
  /**
   * Modifier le filtre sur l'identifiant de la famille
   */
  public void setIdFamille(IdFamille pIdFamille) {
    idFamille = pIdFamille;
  }
  
  /**
   * Retourne le filtre sur la recherche des article gérés de stock
   */
  public EnumFiltreArticleStock getFiltreStock() {
    return filtreStock;
  }
  
  /**
   * Modifie le filtre sur la recherche des article gérés en stock
   */
  public void setFiltreStock(EnumFiltreArticleStock pFiltreStock) {
    filtreStock = pFiltreStock;
  }
  
  /**
   * Retourne le filtre sur la recherche des article commentaires
   */
  public EnumFiltreArticleCommentaire getFiltreCommentaire() {
    return filtreCommentaire;
  }
  
  /**
   * Modifie le filtre sur la recherche des article commentaires
   */
  public void setFiltreCommentaire(EnumFiltreArticleCommentaire pFiltreCommentaire) {
    filtreCommentaire = pFiltreCommentaire;
  }
  
  /**
   * Retourne le filtre sur la recherche des article consignés
   */
  public EnumFiltreArticleConsigne getFiltreConsigne() {
    return filtreSConsigne;
  }
  
  /**
   * Modifie le filtre sur la recherche des article consignés
   */
  public void setFiltreConsigne(EnumFiltreArticleConsigne pFiltreSConsigne) {
    filtreSConsigne = pFiltreSConsigne;
  }
  
  /**
   * Retourner si la recherche concerne les seuls articles valides (A1TOP == 0)
   */
  public Boolean isArticleValideSeulement() {
    return isArticleValideSeulement;
  }
  
  /**
   * Modifier si la recherche doit concerner les seuls articles valides (A1TOP == 0)
   */
  public void setIsArticleValideSeulement(Boolean pIsArticleValideSeulement) {
    isArticleValideSeulement = pIsArticleValideSeulement;
    if (isArticleValideSeulement == null) {
      isArticleValideSeulement = false;
    }
  }
  
  /**
   * Retourner si les critères de recherche sont verrouillés, c'est à dire non modifiables par l'utilisateur. L'initialisation ne doit pas
   * non plus modifier ce qui a été fixé pour l'utilisation initiale du composant.
   */
  public Boolean isVerrouillerCritere() {
    return verrouillerCritere;
  }
  
  /**
   * Modifier si les critères de recherche sont verrouillés, c'est à dire non modifiables par l'utilisateur. L'initialisation ne doit pas
   * non plus modifier ce qui a été fixé pour l'utilisation initiale du composant.
   */
  public void setVerrouillerCritere(Boolean pVerrouillerCritere) {
    verrouillerCritere = pVerrouillerCritere;
    if (verrouillerCritere == null) {
      verrouillerCritere = false;
    }
  }
  
}
