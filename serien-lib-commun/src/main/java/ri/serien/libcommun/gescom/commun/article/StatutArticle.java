/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Statut de l'article.
 * Le statut de l'article est stocké sous forme d'un code d'un caractère en base de données. Ce code
 * peut avoir la valeur ' ', ce qui signifie que l'article est actif. Cette classe permet d'associer le code
 * a un libellé.
 */
public class StatutArticle implements Serializable {
  private final static Map<Character, String> STATUT_ARTICLE = new HashMap<Character, String>();
  private final static String LIBELLE_STATUT_INCONNU = "Statut inconnu";
  private char code;
  private String libelle;
  
  public final static Character ACTIF = ' ';
  public final static Character DESACTIVE = '1';
  public final static Character EPUISE = '2';
  public final static Character SYSTEME_VARIABLE_PRINCIPAL = '3';
  public final static Character SYSTEME_VARIABLE_SECONDAIRE = '4';
  public final static Character PRE_FIN_DE_SERIE = '5';
  public final static Character FIN_DE_SERIE = '6';
  public final static Character TOUS = '*';
  
  {
    STATUT_ARTICLE.put(TOUS, "Tous");
    STATUT_ARTICLE.put(ACTIF, "Article actif");
    STATUT_ARTICLE.put(DESACTIVE, "Article d\u00e9sactiv\u00e9");
    STATUT_ARTICLE.put(EPUISE, "Article \u00e9puis\u00e9");
    STATUT_ARTICLE.put(SYSTEME_VARIABLE_PRINCIPAL, "Syst\u00e8me variable principal");
    STATUT_ARTICLE.put(SYSTEME_VARIABLE_SECONDAIRE, "Syst\u00e8me variable secondaire");
    STATUT_ARTICLE.put(PRE_FIN_DE_SERIE, "Pr\u00e9-fin de s\u00e9rie");
    STATUT_ARTICLE.put(FIN_DE_SERIE, "Fin de s\u00e9rie");
  }
  
  /**
   * Constructeur.
   */
  public StatutArticle(char code) {
    setCode(code);
  }
  
  /**
   * Comparer 2 objets.
   */
  @Override
  public boolean equals(Object object) {
    // Tester si l'objet est nous-même (optimisation)
    if (object == this) {
      return true;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(object instanceof StatutArticle)) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    StatutArticle statutArticle = (StatutArticle) object;
    return code == statutArticle.code;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code;
    return cle;
  }
  
  /**
   * Retourner le code statut de l'article (un caractère alphanumérique).
   */
  public char getCode() {
    return code;
  }
  
  /**
   * Modifie rle code de l'article.
   */
  public void setCode(char code) {
    this.code = code;
    this.libelle = STATUT_ARTICLE.get(Character.valueOf(code));
    if (libelle == null) {
      libelle = LIBELLE_STATUT_INCONNU;
    }
  }
  
  /**
   * On vérifie que le statut en question est bien un statut et non la sélection de tous les statuts
   */
  public boolean isUnstatut() {
    return (code != TOUS);
  }
  
  /**
   * Retourner le libellé du statut de l'article.
   */
  public String getLibelle() {
    return libelle;
  }
  
  public static Map<Character, String> getStatutArticle() {
    return STATUT_ARTICLE;
  }
  
  @Override
  public String toString() {
    return libelle;
  }
  
}
