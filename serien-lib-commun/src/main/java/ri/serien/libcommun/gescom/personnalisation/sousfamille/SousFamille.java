/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.sousfamille;

import java.math.BigDecimal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Sous famille
 * Une sous familles est composé de plusieurs articles.
 */
public class SousFamille extends AbstractClasseMetier<IdSousFamille> {
  public static final String TOUTES_LES_SOUS_FAMILLES = "*";
  
  // Variables
  private String type = "SF";
  private String libelle = "";
  private BigDecimal taxeParaFiscale = null;
  private String typeIntervention = "";
  private String garantie = "";
  
  /**
   * Constructeur à partir de l'identifiant
   */
  public SousFamille(IdSousFamille pIdSousFamille) {
    super(pIdSousFamille);
  }
  
  @Override
  public String getTexte() {
    if (libelle != null) {
      return libelle;
    }
    else {
      return id.getTexte();
    }
  }
  
  /**
   * Déterminer si la sous famille en question est bien une sous famille et non la sélection de toutes les sous familles
   */
  public boolean isUneSousFamille() {
    if (id == null) {
      return false;
    }
    return !id.getCode().equals(TOUTES_LES_SOUS_FAMILLES);
  }
  
  // -- Accesseurs
  
  /**
   * Retourner le libellé de la sous famille
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé de la sous famille
   */
  public void setLibelle(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Retourner la taxe para fiscale
   */
  public BigDecimal getTaxeParaFiscale() {
    return taxeParaFiscale;
  }
  
  /**
   * Modifier la taxe para fiscale
   */
  public void setTaxeParaFiscale(BigDecimal pTaxeParaFiscale) {
    pTaxeParaFiscale = taxeParaFiscale;
  }
  
  /**
   * Retourner le type d'intervention
   */
  public String getTypeIntervention() {
    return typeIntervention;
  }
  
  /**
   * Modifier le type d'intervention
   */
  public void setTypeIntervention(String pTypeIntervention) {
    pTypeIntervention = typeIntervention;
  }
  
  /**
   * Retourner le type de garantie
   */
  public String getGarantie() {
    return garantie;
  }
  
  /**
   * Modifier le type de garantie
   */
  public void setGarantie(String pGarantie) {
    pGarantie = garantie;
  }
  
  /**
   * Retourner le type
   */
  public String getType() {
    return type;
  }
  
  /**
   * Modifier le type
   */
  public void setType(String pType) {
    pType = type;
  }
  
}
