/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.groupeconditionvente;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Ensemble des critères de recherche pour les groupes de conditions de vente.
 * Aucun critère n'est obligatoire.
 */
public class CritereGroupeConditionVente implements Serializable {
  // Constantes
  private IdEtablissement idEtablissement = null;
  private Date dateApplication = null;
  private List<EnumTypeGroupeConditionVente> listeTypeGroupe = null;
  
  // -- Accesseurs
  
  /**
   * Identifiant établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'identifiant de l'établissement.
   * 
   * @param pIdEtablissement
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    this.idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourne la date d'application.
   * 
   * @return
   */
  public Date getDateApplication() {
    return dateApplication;
  }
  
  /**
   * Initialise la date d'application.
   * 
   * @param pDateApplication
   */
  public void setDateApplication(Date pDateApplication) {
    this.dateApplication = pDateApplication;
  }
  
  /**
   * Retourne la liste des types de groupe à charger.
   * 
   * @return
   */
  public List<EnumTypeGroupeConditionVente> getListeTypeGroupe() {
    return listeTypeGroupe;
  }
  
  /**
   * Initialise la liste des types de groupe à charger
   * 
   * @param pListeTypeGroupe
   */
  public void setListeTypeGroupe(List<EnumTypeGroupeConditionVente> pListeTypeGroupe) {
    this.listeTypeGroupe = pListeTypeGroupe;
  }
  
}
