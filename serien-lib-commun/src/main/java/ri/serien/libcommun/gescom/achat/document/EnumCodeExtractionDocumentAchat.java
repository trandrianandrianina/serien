/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête d'un document d'achat.
 */
public enum EnumCodeExtractionDocumentAchat {
  AUCUNE(' ', "Aucune"),
  AVEC_RELICAT('Y', "extraction avec relicat"),
  SANS_RELICAT('X', "extraction sans relicat"),
  MODIFICATION('M', "Modification"),
  ANNULATION('A', "Annulation");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumCodeExtractionDocumentAchat(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumCodeExtractionDocumentAchat valueOfByCode(Character pCode) {
    for (EnumCodeExtractionDocumentAchat value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code entête du document d'achat est invalide : " + pCode);
  }
}
