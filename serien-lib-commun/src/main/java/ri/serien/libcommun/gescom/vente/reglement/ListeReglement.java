/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.document.EnumEtatReglement;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.DateHeure;

/**
 * Liste de Reglement.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de reglement.
 */
public class ListeReglement extends ArrayList<Reglement> {
  // Constantes
  public static final int NOMBRE_REGLEMENTS_MAX = 18;
  
  // Variables
  private EnumEtatReglement etatReglement = null;
  // Montant total TTC du document + multirèglements
  private BigDecimal montantTotalARegler = null;
  // Montant total des règlements
  private BigDecimal montantTotalCalcule = null;
  private BigDecimal montantAcomptePris = null;
  private BigDecimal montantAcompteConsomme = null;
  // Concernant le document
  private boolean documentModifiable = true;
  private EnumTypeDocumentVente typeDocumentVente = null;
  
  // Cette variable contient la liste originale des règlements nécessaire lors de la sauvegarde en base
  private ListeReglement listeReglementOriginale = null;
  
  /**
   * Constructeur.
   */
  public ListeReglement() {
  }
  
  /**
   * Initialise les informations du document.
   */
  public void initialiserInfoDocument(DocumentVente pDocumentVente) {
    if (pDocumentVente == null) {
      throw new MessageErreurException("Le document est invalide.");
    }
    documentModifiable = pDocumentVente.isModifiable();
    typeDocumentVente = pDocumentVente.getTypeDocumentVente();
    // Le total à régler sera toujours initialisé avec le montants TTC car le paiement des acomptes et autres montants est toujours en TTC
    montantTotalARegler = pDocumentVente.getTotalTTC();
    calculer();
  }
  
  /**
   * Met à jour le montant total à régler.
   */
  public void modifierMontantTotalARegler(BigDecimal pMontantTotalARegler) {
    montantTotalARegler = pMontantTotalARegler;
    calculer();
  }
  
  public void modifierReglement(Reglement pReglement, BigDecimal pMontantFinal) {
    if (pReglement == null) {
      throw new MessageErreurException("Le règlement à modifier est invalide.");
    }
    if (pMontantFinal == null) {
      throw new MessageErreurException("Le montant du règlement à modifier est invalide.");
    }
    
    pReglement.setMontant(pMontantFinal);
    calculer();
  }
  
  /**
   * Retourne dans le cas où un acompte existe s'il est réglé ou non.
   */
  public boolean isAcompteRegle(Acompte pAcompte) {
    if (pAcompte == null) {
      return false;
    }
    if (pAcompte.getMontant().compareTo(montantAcomptePris) <= 0) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si le montant total est réglé ou non.
   * Attention s'il s'agit d'une commande seul l'acompte est pris en considération.
   */
  public boolean isRegle() {
    if (etatReglement != null && etatReglement.equals(EnumEtatReglement.REGLE)) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourner un règlement de la liste à partir de son identifiant.
   */
  public Reglement retournerReglementParId(IdReglement pIdReglement) {
    IdReglement.controlerId(pIdReglement, false);
    
    for (Reglement reglement : this) {
      if (reglement != null && Constantes.equals(reglement.getId(), pIdReglement)) {
        return reglement;
      }
    }
    
    return null;
  }
  
  /**
   * Supprimer un règlement de la liste à partir de son identifiant.
   */
  public void supprimerReglementParId(IdReglement pIdReglement) {
    if (pIdReglement == null) {
      return;
    }
    Reglement reglementASupprimer = null;
    for (Reglement reglement : this) {
      if (reglement != null && Constantes.equals(reglement.getId(), pIdReglement)) {
        reglementASupprimer = reglement;
        break;
      }
    }
    this.remove(reglementASupprimer);
  }
  
  /**
   * Retourne le dernier règlement effectué.
   */
  public Reglement retrournerDernierReglementEffectue() {
    if (isEmpty()) {
      return null;
    }
    return get(size() - 1);
  }
  
  /**
   * Retourne si le dernier mode de règlement est un avoir.
   */
  public boolean isDernierReglementEffectueEstAvoir() {
    Reglement reglement = retrournerDernierReglementEffectue();
    if (reglement == null) {
      return false;
    }
    return reglement.isAvecAvoir();
  }
  
  /**
   * Tester si un règlement est présent dans la liste.
   */
  public boolean isPresent(IdReglement pIdReglement) {
    return retournerReglementParId(pIdReglement) != null;
  }
  
  /**
   * Retourne si les règlements ont été modifié.
   */
  public boolean isModifie() {
    // Si c'est en création de document
    if (listeReglementOriginale == null) {
      if (isEmpty()) {
        return false;
      }
      return true;
    }
    // Si le règlement a été chargé (donc pas en création de document)
    if (Constantes.equals(getMontantTotalCalcule(), listeReglementOriginale.getMontantTotalCalcule())) {
      return false;
    }
    return true;
  }
  
  /**
   * Retourne si le sens des règlements de la liste est identique.
   * Ce sont tous des paiements ou des remboursements, ou les deux sont mélangés.
   */
  public boolean isSensIdentique() {
    int sensReglement = 0;
    for (Reglement reglement : this) {
      // Initialisation de la variable de test avec le premier élément de la liste des règlements
      if (sensReglement == 0) {
        sensReglement = reglement.getMontant().signum();
        continue;
      }
      // Si un des montants des règlements n'est pas dans le même sens (paiement/remboursement) alors les montants ne sont pas homogènes
      if (sensReglement != reglement.getMontant().signum()) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Retourne si tous les règlements de la liste sont du jour même.
   */
  public boolean isRegleAujourdhui() {
    // Convertie la date du jour en AAAAMMJJ
    String aujourdhui = DateHeure.getJourHeure(4);
    for (Reglement reglement : this) {
      if (reglement.getDateCreation() == null) {
        continue;
      }
      // Convertie la date du règlement en AAAAMMJJ
      String jour = DateHeure.getJourHeure(4, reglement.getDateCreation());
      // Si les jours sont différents alors le règlement n'est pas supprimable
      if (!aujourdhui.equals(jour)) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Permet de cloner la liste de règlments et de la stocker dans la classe elle même.
   * Nécessaire au service sauverListeReglement.
   */
  public static ListeReglement sauvegarderReglementOriginaux(ListeReglement pListeReglement) {
    if (pListeReglement.isEmpty()) {
      return pListeReglement;
    }
    // Clonage de la liste qui vient d'être chargée
    ListeReglement listeOriginale = pListeReglement.clone();
    // Clonage de la liste qui vient d'être chargée pour la sauvegarde
    ListeReglement listeOriginaleSauvegarde = pListeReglement.clone();
    listeOriginale.setListeReglementOriginale(listeOriginaleSauvegarde);
    // Stockage
    pListeReglement.setListeReglementOriginale(listeOriginale);
    return pListeReglement;
  }
  
  /**
   * Permet de restaurer la liste de reglements à partir de la liste sauevagrdée.
   */
  public static ListeReglement restaurerReglementOriginaux(ListeReglement pListeReglement) {
    if (pListeReglement == null) {
      return null;
    }
    if (pListeReglement.getListeReglementOriginale() == null) {
      return new ListeReglement();
    }
    ListeReglement listeReglementOriginale = pListeReglement.getListeReglementOriginale();
    return listeReglementOriginale.clone();
  }
  
  // -- Méthodes privées
  
  /**
   * Parcourt la liste des règlsments afin de faire la somme des montants des règlements et éventuellement de l'acompte.
   */
  public void calculer() {
    montantTotalCalcule = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    montantAcomptePris = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    montantAcompteConsomme = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    etatReglement = EnumEtatReglement.NON_REGLE;
    
    for (Reglement reglement : this) {
      if (reglement.getMontant() == null) {
        throw new MessageErreurException("Le montant du règlement " + reglement.getId() + "est invalide.");
      }
      
      // Le total de l'ensemble des règlements
      montantTotalCalcule = montantTotalCalcule.add(reglement.getMontant());
      
      // Le règlement concerne un acompte
      if (reglement.getReglementAcompte() != null) {
        ReglementAcompte acompte = reglement.getReglementAcompte();
        // Calcul du montant pris
        if (acompte.isPris()) {
          montantAcomptePris = montantAcomptePris.add(reglement.getMontant());
        }
        // Calcul du montant consommé
        else if (acompte.isConsomme()) {
          montantAcompteConsomme = montantAcompteConsomme.add(reglement.getMontant());
        }
        else {
          throw new MessageErreurException("Impossible de déterminer si l'acompte est pris ou consommé.");
        }
      }
    }
    
    // Détermination de l'état du règlement
    // Si le document n'est pas modifiable et s'il s'agit d'une commande
    if (!documentModifiable && Constantes.equals(typeDocumentVente, EnumTypeDocumentVente.COMMANDE)) {
      // Le document est considéré comme non réglé si le montant de l'acompte vaut 0
      if (montantAcomptePris.compareTo(BigDecimal.ZERO) == 0) {
        etatReglement = EnumEtatReglement.NON_REGLE;
      }
      // La commande est considéré comme réglée si elle n'est pas modifiable
      else {
        etatReglement = EnumEtatReglement.REGLE;
      }
    }
    // Sinon pour tous les documents (facture, commande, ...) l'état du règlement est basé sur le montant total
    else if (montantTotalCalcule != null && montantTotalARegler != null) {
      if (montantTotalCalcule.intValue() == 0 && montantTotalARegler.intValue() != 0) {
        etatReglement = EnumEtatReglement.NON_REGLE;
      }
      else if (montantTotalCalcule.compareTo(montantTotalARegler) == 0) {
        etatReglement = EnumEtatReglement.REGLE;
      }
      else {
        etatReglement = EnumEtatReglement.PARTIELLEMENT_REGLE;
      }
    }
  }
  
  // -- Méthodes surchargées
  
  /**
   * Ajoute un règlement à la liste.
   */
  @Override
  public boolean add(Reglement pReglement) {
    if (pReglement == null) {
      throw new MessageErreurException("Le règlement est invalide.");
    }
    // Ajoute le règlement à la liste et fait la somme des règlements
    if (super.add(pReglement)) {
      calculer();
      return true;
    }
    return false;
  }
  
  /**
   * Supprime un règlement à la liste.
   */
  @Override
  public boolean remove(Object pReglement) {
    if (pReglement == null) {
      throw new MessageErreurException("Le règlement est invalide.");
    }
    // Ajoute le règlement à la liste et fait la somme des règlements
    if (super.remove(pReglement)) {
      calculer();
      return true;
    }
    return false;
  }
  
  /**
   * Efface la liste des règlements.
   */
  @Override
  public void clear() {
    super.clear();
    calculer();
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public ListeReglement clone() {
    ListeReglement o = null;
    // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
    o = (ListeReglement) super.clone();
    return o;
  }
  
  // -- Accesseurs
  
  public BigDecimal getMontantAcompteRegle() {
    return montantAcomptePris;
  }
  
  public void setMontantAcompteRegle(BigDecimal montantAcompteRegle) {
    this.montantAcomptePris = montantAcompteRegle;
  }
  
  public BigDecimal getMontantTotalARegler() {
    return montantTotalARegler;
  }
  
  public BigDecimal getMontantTotalCalcule() {
    return montantTotalCalcule;
  }
  
  public ListeReglement getListeReglementOriginale() {
    return listeReglementOriginale;
  }
  
  public void setListeReglementOriginale(ListeReglement listeReglementOriginale) {
    this.listeReglementOriginale = listeReglementOriginale;
  }
  
  public EnumEtatReglement getEtatReglement() {
    return etatReglement;
  }
  
  public void setEtatReglement(EnumEtatReglement etatReglement) {
    this.etatReglement = etatReglement;
  }
  
  public boolean isDocumentModifiable() {
    return documentModifiable;
  }
  
}
