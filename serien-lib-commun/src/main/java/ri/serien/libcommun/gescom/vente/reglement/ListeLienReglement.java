/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import java.util.ArrayList;

import ri.serien.libcommun.outils.Constantes;

/**
 * Liste de LienReglement.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de lienReglement.
 */
public class ListeLienReglement extends ArrayList<LienReglement> {
  /**
   * Constructeur.
   */
  public ListeLienReglement() {
  }
  
  /**
   * Retourner un LienReglement de la liste à partir de son identifiant.
   */
  public LienReglement retournerLienReglementParId(IdReglement pIdReglement) {
    if (pIdReglement == null) {
      return null;
    }
    for (LienReglement lienReglement : this) {
      if (lienReglement != null && Constantes.equals(lienReglement.getId(), pIdReglement)) {
        return lienReglement;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un LienReglement est présent dans la liste.
   */
  public boolean isPresent(IdReglement pIdReglement) {
    return retournerLienReglementParId(pIdReglement) != null;
  }
}
