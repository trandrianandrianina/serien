/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.vendeur;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de vendeurs.
 * L'utilisation de cette classe est à privilégiée dès qu'on manipule une liste de vendeurs. Elle contient toutes les méthodes oeuvrant
 * sur la liste tandis que la classe Vendeur contient les méthodes oeuvrant sur un seul vendeur.
 */

public class ListeVendeur extends ListeClasseMetier<IdVendeur, Vendeur, ListeVendeur> {
  /**
   * Constructeur.
   */
  public ListeVendeur() {
  }
  
  /**
   * Charge tous les vendeurs de l'établissement donnée.
   */
  @Override
  public ListeVendeur charger(IdSession pIdSession, List<IdVendeur> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  @Override
  public ListeVendeur charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    
    CriteresRechercheVendeurs criteres = new CriteresRechercheVendeurs();
    criteres.setTypeRecherche(CriteresRechercheVendeurs.TOUT_RETOURNER_POUR_UN_ETABLISSEMENT);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeVendeur(pIdSession, criteres);
  }
  
  /**
   * Tester si un vendeur est présent dans la liste.
   */
  public boolean isPresent(IdVendeur pIdVendeur) {
    return get(pIdVendeur) != null;
  }
  
}
