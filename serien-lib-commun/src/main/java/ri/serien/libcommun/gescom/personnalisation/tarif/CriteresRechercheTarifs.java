/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.tarif;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;

public class CriteresRechercheTarifs extends CriteresBaseRecherche {
  // Constantes
  // public static final int SPECIFIQUE_COMPTOIR = 1;
  
  public static final int TOUT_RETOURNER_POUR_UN_ETABLISSEMENT = 1000;
  public static final int RECHERCHER_EXPRESSION_DANS_LIBELLE = 1001;
  
  // Variables
  
}
