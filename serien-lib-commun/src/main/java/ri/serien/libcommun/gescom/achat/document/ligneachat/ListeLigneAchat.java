/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;

/**
 * Liste de LigneAchat.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de lignes d'achat.
 */
public class ListeLigneAchat extends ListeClasseMetier<IdLigneAchat, LigneAchat, ListeLigneAchat> {
  
  /**
   * Constructeur.
   */
  public ListeLigneAchat() {
  }
  
  // -- Méthodes publiques
  
  @Override
  public ListeLigneAchat charger(IdSession pIdSession, List<IdLigneAchat> pListeId) {
    return ManagerServiceDocumentAchat.chargerListeLigneAchat(pIdSession, pListeId);
  }
  
  @Override
  public ListeLigneAchat charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CritereLigneAchat critere = new CritereLigneAchat();
    critere.setIdEtablissement(pIdEtablissement);
    return ManagerServiceDocumentAchat.chargerListeLigneAchat(pIdSession, critere);
  }
  
  /**
   * Retourne une ListeLigneAchat à partir de critères de recherche.
   * 
   * @param pIdSession IdSession
   * @param pcritere CritereLigneAchat
   * @return ListeLigneAchat
   */
  public static ListeLigneAchat charger(IdSession pIdSession, CritereLigneAchat pcritere) {
    return ManagerServiceDocumentAchat.chargerListeLigneAchat(pIdSession, pcritere);
  }
  
  /**
   * Retourne une LigneAchat à partir de son identifiant.
   * 
   * @param pIdSession IdSession
   * @param pIdLigneAchat IdLigneAchat
   * @return LigneAchat
   */
  @Override
  public LigneAchat charger(IdSession pIdSession, IdLigneAchat pIdLigneAchat) {
    if (this.isEmpty() && get(pIdLigneAchat) == null) {
      CritereLigneAchat critere = new CritereLigneAchat();
      critere.setIdLigneAchat(pIdLigneAchat);
      ListeLigneAchat listeLigneAchat = ManagerServiceDocumentAchat.chargerListeLigneAchat(pIdSession, critere);
      return listeLigneAchat.get(pIdLigneAchat);
    }
    else {
      return get(pIdLigneAchat);
    }
  }
  
  /**
   * Retourner une LigneAchat de la liste à partir de l'ID de l'article qu'elle contient.
   */
  public LigneAchat retournerLigneAchatParArticle(IdArticle pIdArticle) {
    if (pIdArticle == null) {
      return null;
    }
    for (LigneAchat ligneAchat : this) {
      if (ligneAchat != null && Constantes.equals(ligneAchat.getIdArticle(), pIdArticle)) {
        return ligneAchat;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un LigneAchat est présent dans la liste.
   */
  public boolean isPresent(IdLigneAchat pIdLigneAchat) {
    return get(pIdLigneAchat) != null;
  }
  
}
