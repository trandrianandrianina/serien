/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente;

/**
 * Liste les différents résultats possibles afin de mettre en évidence la valeur correspondante dans le résultat.
 */
public enum EnumTypePrixVente {
  AUCUN("Aucun"),
  PRIX_BASE_HT("Prix base HT"),
  PRIX_BASE_TTC("Prix base HT"),
  PRIX_NET_HT("Prix net HT"),
  PRIX_NET_TTC("Prix net TTC"),
  MONTANT_HT("Montant HT"),
  MONTANT_TTC("Montant TTC");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypePrixVente(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
