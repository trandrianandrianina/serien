/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.sipe;

import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Cette classe regroupe les champs d'un plan de pose Sipe issus de la table PGVMSIPM.
 */
public class PlanPoseSipe extends AbstractClasseMetier<IdPlanPoseSipe> {
  // Constantes
  public static final int TAILLE_NOM_FICHIER = 120;
  
  // Variables
  private EnumStatutSipe statut = null;
  private String utilisateurChangementStatut = null;
  private EnumTypePlanPoseSipe typePlanPoseSipe = null;
  private Date datePlan = null;
  private String nomEntreprise = null;
  private String nomBatiment = null;
  private String numeroAffaire = null;
  private String numeroPlan = null;
  private String nomChantier = null;
  private String nomFichier = null;
  
  /**
   * Constructeur.
   */
  public PlanPoseSipe(IdPlanPoseSipe pIdPlanPoseSipe) {
    super(pIdPlanPoseSipe);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne un texte identifiant le plan de pose.
   */
  @Override
  public String getTexte() {
    return id.getNumero() + "";
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le statut du plan de pose.
   * 
   * @return
   */
  public EnumStatutSipe getStatut() {
    return statut;
  }
  
  /**
   * Initialise le statut du plan de pose.
   * 
   * @param pStatut
   */
  public void setStatut(EnumStatutSipe pStatut) {
    this.statut = pStatut;
  }
  
  /**
   * Retourne le nom de l'utilisateur qui a changé le statut du plan.
   * 
   * @return
   */
  public String getUtilisateurChangementStatut() {
    return utilisateurChangementStatut;
  }
  
  /**
   * Initialise le nom de l'utilisateur qui a changé le statut du plan.
   * 
   * @param pUtilisateur
   */
  public void setUtilisateurChangementStatut(String pUtilisateur) {
    this.utilisateurChangementStatut = pUtilisateur;
  }
  
  /**
   * Retourne le type du plan de pose.
   * 
   * @return
   */
  public EnumTypePlanPoseSipe getTypePlanPoseSipe() {
    return typePlanPoseSipe;
  }
  
  /**
   * Initialise le type du plan de pose.
   * 
   * @param pTypePlanPoseSipe
   */
  public void setTypePlanPoseSipe(EnumTypePlanPoseSipe pTypePlanPoseSipe) {
    this.typePlanPoseSipe = pTypePlanPoseSipe;
  }
  
  /**
   * Retourne la date du plan de pose.
   * 
   * @return
   */
  public Date getDatePlan() {
    return datePlan;
  }
  
  /**
   * Initialise la date du plan de pose.
   * 
   * @param datePlan
   */
  public void setDatePlan(Date datePlan) {
    this.datePlan = datePlan;
  }
  
  /**
   * Retourne le nom de l'entreprise.
   * 
   * @return
   */
  public String getNomEntreprise() {
    return nomEntreprise;
  }
  
  /**
   * Initialise le nom de l'entreprise.
   * 
   * @param pNomEntreprise
   */
  public void setNomEntreprise(String pNomEntreprise) {
    this.nomEntreprise = pNomEntreprise;
  }
  
  /**
   * Retourne le nom du batiment.
   * 
   * @return
   */
  public String getNomBatiment() {
    return nomBatiment;
  }
  
  /**
   * Initialise le nom du batiment.
   * 
   * @param pNomBatiment
   */
  public void setNomBatiment(String pNomBatiment) {
    this.nomBatiment = pNomBatiment;
  }
  
  /**
   * Retourne le numéro de l'affaire.
   * 
   * @return
   */
  public String getNumeroAffaire() {
    return numeroAffaire;
  }
  
  /**
   * Initialise le numéro de l'affaire.
   * 
   * @param pNumeroAffaire
   */
  public void setNumeroAffaire(String pNumeroAffaire) {
    this.numeroAffaire = pNumeroAffaire;
  }
  
  /**
   * Retourne le numéro du plan de pose.
   * 
   * @return
   */
  public String getNumeroPlan() {
    return numeroPlan;
  }
  
  /**
   * Initialise le numéro du plan de pose.
   * 
   * @param pNumeroPlan
   */
  public void setNumeroPlan(String pNumeroPlan) {
    this.numeroPlan = pNumeroPlan;
  }
  
  /**
   * Retourne le nom du chantier.
   * 
   * @return
   */
  public String getNomChantier() {
    return nomChantier;
  }
  
  /**
   * Initialise le nom du chantier.
   * 
   * @param pNomChantier
   */
  public void setNomChantier(String pNomChantier) {
    this.nomChantier = pNomChantier;
  }
  
  /**
   * Initialise le nom du fichier.
   * 
   * @return
   */
  public String getNomFichier() {
    return nomFichier;
  }
  
  /**
   * Retourne le nom du fichier.
   * 
   * @param pNomFichier
   */
  public void setNomFichier(String pNomFichier) {
    this.nomFichier = pNomFichier;
  }
}
