/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockattenducommande;

import java.io.Serializable;

import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

/**
 * Critères de recherche pour les attendus et commandés des articles.
 * 
 * Tous les critères sont optionnels à l'exception du code article qui doit être renseigné.
 */
public class CritereStockAttenduCommande implements Serializable {
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private IdArticle idArticle = null;
  private IdDocumentVente idDocumentVente = null;
  private IdDocumentAchat idDocumentAchat = null;
  
  /**
   * Retourner l'identifiant de l'établissement pour lequel on souhaite effectuer la recherche.
   * 
   * @return Identifiant de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   * 
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Identifiant du magasin pour lequel on souhaite effectuer la recherche.
   * Si aucun magasin n'est transmis on recherche sur tous les magasins de l'établissement
   * 
   * @return Identifiant du magasin.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier le filtre sur le magasin.
   * 
   * L'établissement est également mis à jour si le magasin n'est pas null.
   * Si aucun magasin n'est renseigné, la recherche est effectuée sur tous les magasins de l'établissement.
   * 
   * @param pIdMagasin Identifiant du magasin.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
    if (idMagasin != null) {
      idEtablissement = idMagasin.getIdEtablissement();
    }
  }
  
  /**
   * Retourner l'identifiant de l'article pour lequel on souhaite effectuer la recherche.
   * 
   * @return Identifiant de l'article.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Modifier l'identifiant article pour lequel on souhaite effectuer la recherche.
   * 
   * @param pIdMagasin Identifiant de l'article.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }
}
