/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;

/**
 * Paramètres permettant de lire les données d'un article.
 * L'essentiel des paramètres sert au calcul du prix.
 */
public class ParametresLireArticle implements Serializable {
  // Variables
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private IdClient idClient = null;
  private IdDocumentVente idDocumentVente = null;
  private IdChantier idChantier = null;
  private Integer numeroColonneTarifForcee = null;
  private BigPercentage tauxRemiseForcee = null;
  private IdFournisseur idFournisseur = null;
  
  // -- Accesseurs
  
  public IdClient getIdClient() {
    return idClient;
  }
  
  public void setIdClient(IdClient idClient) {
    this.idClient = idClient;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin pIdMagasin) {
    this.idMagasin = pIdMagasin;
  }
  
  public Integer getNumeroColonneTarifForcee() {
    return numeroColonneTarifForcee;
  }
  
  public void setNumeroColonneTarifForcee(Integer pNumeroColonneTarifForcee) {
    numeroColonneTarifForcee = pNumeroColonneTarifForcee;
  }
  
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  public void setIdDocumentVente(IdDocumentVente idDocumentVente) {
    this.idDocumentVente = idDocumentVente;
  }
  
  /**
   * Identifiant du chantier.
   */
  public IdChantier getIdChantier() {
    return idChantier;
  }
  
  /**
   * Modifier l'identifiant du chantier.
   */
  public void setIdChantier(IdChantier pIdChantier) {
    idChantier = pIdChantier;
  }
  
  /**
   * Retourner le taux de remise forcée.
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemiseForcee() {
    return tauxRemiseForcee;
  }
  
  /**
   * Modifier le taux de remise forcée.
   * @param pTauxRemiseForcee Taux de remise.
   */
  public void setTauxRemiseForcee(BigPercentage pTauxRemiseForcee) {
    tauxRemiseForcee = pTauxRemiseForcee;
  }
  
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  public void setIdEtablissement(IdEtablissement idEtablissement) {
    this.idEtablissement = idEtablissement;
  }
  
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
  
}
