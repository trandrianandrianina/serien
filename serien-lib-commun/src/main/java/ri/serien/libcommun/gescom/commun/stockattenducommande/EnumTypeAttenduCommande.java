/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockattenducommande;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'un attendu/commandé.
 * 
 * Le code de l'attendu est plus petit que celui des commandés de façon à ce que les attendus soient listés avant les commandés
 * dans le résultat de la requête SQL.
 */
public enum EnumTypeAttenduCommande {
  ATTENDU(1, "Attendu"),
  COMMANDE(2, "Commandé"),
  DATE_JOUR(3, "Date du jour"),
  DATE_MINI_REAPPRO(4, "Date minimum de réapprovisionnement");
  
  private final int code;
  private final String libelle;
  
  /**
   * Constructeur.
   * 
   * @param pCode Code du type d'attendu/commandé.
   * @param pLibelle Libellé du type d'attendu/commandé.
   */
  EnumTypeAttenduCommande(int pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code du type d'attendu/commandé.
   * @return Code du type d'attendu/commandé.
   */
  public int getCode() {
    return code;
  }
  
  /**
   * Le libellé du type d'attendu/commandé.
   * @return Libellé du type d'attendu/commandé.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne une version textuelle du type d'attodé/commandé (c'est le libellé qui est retourné).
   * @return Libellé du type d'attendu/commandé.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner un type d'attendu/commandé à partir de son code.
   * 
   * @param pCode Code du type d'attendu/commandé.
   * @return Type d'attendu/commandé.
   */
  static public EnumTypeAttenduCommande valueOfByCode(int pCode) {
    for (EnumTypeAttenduCommande value : values()) {
      if (pCode == value.getCode()) {
        return value;
      }
    }
    throw new MessageErreurException("Le code de l'attendu/commandé est invalide : " + pCode);
  }
}
