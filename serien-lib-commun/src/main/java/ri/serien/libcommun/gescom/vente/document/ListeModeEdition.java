/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.document.EnumModeEdition;
import ri.serien.libcommun.gescom.commun.document.ModeEdition;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les modes possibles d'édition un type de document donné.
 */
public class ListeModeEdition extends ArrayList<ModeEdition> {
  // Constantes
  public static final int TYPE_MODE_EDITION_VENTE = 0;
  public static final int TYPE_MODE_EDITION_ACHAT = 1;
  
  // Variables
  private final int typeModeEdition;
  private boolean modeDebug = false;
  
  /**
   * Constructeur.
   */
  private ListeModeEdition(int pTypeModeEdition) {
    typeModeEdition = pTypeModeEdition;
    if (typeModeEdition != TYPE_MODE_EDITION_VENTE && typeModeEdition != TYPE_MODE_EDITION_ACHAT) {
      throw new MessageErreurException("Le type du mode d'édition est invalide.");
    }
    reinitialiserValeurParDefaut();
  }
  
  // -- Méthodes publiques
  
  /**
   * Créer une liste de modes d'édition pour les ventes.
   */
  public static ListeModeEdition getInstanceModeEditionVente() {
    return new ListeModeEdition(TYPE_MODE_EDITION_VENTE);
  }
  
  /**
   * Créer une liste de modes d'édition pour les achats.
   */
  public static ListeModeEdition getInstanceModeEditionAchat() {
    return new ListeModeEdition(TYPE_MODE_EDITION_ACHAT);
  }
  
  /**
   * Remet les valeurs de la classe comme à l'origine après une utilisation possible.
   */
  public void reinitialiserValeurParDefaut() {
    clear();
    switch (typeModeEdition) {
      case TYPE_MODE_EDITION_VENTE:
        construireLaListePourEditionVente();
        break;
      case TYPE_MODE_EDITION_ACHAT:
        construireLaListePourEditionAchat();
        break;
    }
  }
  
  /**
   * Retourne un mode d'édition à partir de son enum.
   */
  public ModeEdition getModeEditionAPartirEnum(EnumModeEdition pEnumModeEdition) {
    for (ModeEdition modeEdition : this) {
      if (modeEdition.getEnumModeEdition().equals(pEnumModeEdition)) {
        return modeEdition;
      }
    }
    return null;
  }
  
  /**
   * Vérifie qu'au moins un mode d'édition ait été fait (de l'ensemble des modes actifs).
   */
  public boolean isAuMoinsUneSelection() {
    for (ModeEdition modeEdition : this) {
      if (modeEdition.isActif() && modeEdition.isSelectionne()) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Vérifie qu'un mode n'a été fait (de l'ensemble des modes actifs).
   */
  public boolean isAucuneSelection() {
    for (ModeEdition modeEdition : this) {
      if (modeEdition.isActif() && modeEdition.isSelectionne()) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Désactive un mode d'édition donné.
   */
  public void desactiver(EnumModeEdition pEnumModeEdition) {
    ModeEdition modeEdition = getModeEditionAPartirEnum(pEnumModeEdition);
    if (modeEdition == null) {
      return;
    }
    modeEdition.setActif(false);
  }
  
  /**
   * Retourne le libellé construit à partir de l'enum et des doonées courantes.
   * Pour l'instant le texte reste basique : c'est celui de l'enum.
   */
  public String getTexte(EnumModeEdition pEnumModeEdition) {
    ModeEdition modeEdition = getModeEditionAPartirEnum(pEnumModeEdition);
    if (modeEdition == null) {
      throw new MessageErreurException("Le mode d'édition n'a pas été trouvé.");
    }
    return modeEdition.getTexte();
  }
  
  // -- Méthodes privées
  
  /**
   * Permet de construire la liste avec tous les modes possibles pour un document de vente.
   */
  private void construireLaListePourEditionVente() {
    clear();
    // En mode débug on coche la visualisation afin de facilité la vie du développeur
    if (modeDebug) {
      add(ModeEdition.getInstance(EnumModeEdition.VISUALISER, true));
      add(ModeEdition.getInstance(EnumModeEdition.IMPRIMER, false));
    }
    else {
      add(ModeEdition.getInstance(EnumModeEdition.VISUALISER, false));
      add(ModeEdition.getInstance(EnumModeEdition.IMPRIMER, true));
    }
    add(ModeEdition.getInstance(EnumModeEdition.ENVOYER_PAR_MAIL, false));
    add(ModeEdition.getInstance(EnumModeEdition.ENVOYER_PAR_FAX, false));
  }
  
  /**
   * Permet de construire la liste avec tous les modes possibles pour un document d'achat.
   */
  private void construireLaListePourEditionAchat() {
    clear();
    // En mode débug on coche la visualisation afin de facilité la vie du développeur
    if (modeDebug) {
      add(ModeEdition.getInstance(EnumModeEdition.VISUALISER, true));
      add(ModeEdition.getInstance(EnumModeEdition.IMPRIMER, false));
    }
    else {
      add(ModeEdition.getInstance(EnumModeEdition.VISUALISER, false));
      add(ModeEdition.getInstance(EnumModeEdition.IMPRIMER, true));
    }
    add(ModeEdition.getInstance(EnumModeEdition.ENVOYER_PAR_MAIL, false));
    add(ModeEdition.getInstance(EnumModeEdition.ENVOYER_PAR_FAX, false));
  }
  
  public void setModeDebug(boolean modeDebug) {
    this.modeDebug = modeDebug;
  }
  
}
