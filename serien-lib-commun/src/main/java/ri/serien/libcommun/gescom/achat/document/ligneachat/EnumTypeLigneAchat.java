/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête d'un document d'achat.
 */
public enum EnumTypeLigneAchat {
  COURANTE_STOCKEE('C', "Ligne courante stock\u00e9"),
  COMMENTAIRE('*', "Ligne commentaire"),
  PALETTE(' ', "Ligne palette"),
  SPECIALE_NON_STOCKEE('S', "Ligne spéciale non stock\u00e9e");

  private final Character code;
  private final String libelle;

  /**
   * Constructeur.
   */
  EnumTypeLigneAchat(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }

  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }

  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }

  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeLigneAchat valueOfByCode(Character pCode) {
    for (EnumTypeLigneAchat value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code entête du document d'achat est invalide : " + pCode);
  }
}
