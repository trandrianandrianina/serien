/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.adressedocument;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête d'une adresse de document de vente (pour les adresses tirées de la table Adresses de ventes sur EBC (PGVMADVM)).
 */
public enum EnumCodeEnteteAdresseDocument {
  ADRESSE_LIVRAISON_HORS_DEVIS('L', "Adresse de livraison hors devis et chantiers"),
  ADRESSE_FACTURATION_HORS_DEVIS('F', "Adresse de facturation hors devis et chantiers"),
  ADRESSE_LIVRAISON_DEVIS('C', "Adresse de livraison devis et chantiers"),
  ADRESSE_FACTURATION_DEVIS('D', "Adresse de facturation devis et chantiers");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumCodeEnteteAdresseDocument(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumCodeEnteteAdresseDocument valueOfByCode(Character pCode) {
    for (EnumCodeEnteteAdresseDocument value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code entête de l'adresse du document de vente est invalide : " + pCode);
  }
}
