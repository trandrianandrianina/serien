/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.origine;

/**
 * Liste des origines possibles d'un groupe de condition de ventes standard.
 */
public enum EnumOrigineGroupeConditionVenteStandard {
  GROUPE_CNV_CLIENT_LIVRE("Groupe CNV client livré"),
  GROUPE_CNV_CLIENT_FACTURE("Groupe CNV client facturé"),
  GROUPE_CNV_DOCUMENT_VENTE("Groupe CNV document vente"),
  CNV_CLIENT_DU_CLIENT_FACTURE("CNV client du client facturé"),
  CNV_CLIENT_DU_DOCUMENT_VENTE("CNV client du document vente");
  
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrigineGroupeConditionVenteStandard(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
}
