/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typegratuit;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;

public class CriteresRechercheTypeGratuit extends CriteresBaseRecherche {
  // Constantes
  public static final int RECHERCHE_TYPE_GRATUIT = 1;
}
