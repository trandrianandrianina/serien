/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.categoriefournisseur;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

public class CategorieFournisseur extends AbstractClasseMetier<IdCategorieFournisseur> {
  
  // Variables
  private String libelle = "";
  private String magasinTransfert = "";
  private String traitementSpecifique = "";
  
  // -- Accesseurs
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public CategorieFournisseur(IdCategorieFournisseur pIdCategorieFournisseur) {
    super(pIdCategorieFournisseur);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Libellé de la catégorie fournisseur.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Renseigner le libellé de la catégorie fournisseur.
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  /**
   * Retourne le magasin pour achat/transfert.
   */
  public String getMagasinTransfert() {
    return magasinTransfert;
  }
  
  /**
   * Renseigner le magasin de la catégorie fournisseur.
   */
  public void setMagasinTransfert(String magasinTransfert) {
    this.magasinTransfert = magasinTransfert;
  }
  
  /**
   * Retourne le traitement spécifique.
   */
  public String getTraitementSpecifique() {
    return traitementSpecifique;
  }
  
  /**
   * Renseigner le traitement spécifique.
   */
  public void setTraitementSpecifique(String traitementSpecifique) {
    this.traitementSpecifique = traitementSpecifique;
  }
  
}
