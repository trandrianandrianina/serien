/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.formuleprix;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;

import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe contient toutes les données décrivant une ligne pour une formule de prix.
 */
public class LigneFormulePrix implements Serializable {
  // Variables
  private String libelle = null;
  private Character operande = null;
  private BigDecimal valeur = null;
  private EnumTypeValeurFormulePrix typeValeur = null;
  
  // -- Méthodes publiques
  
  /**
   * Calcule le montant d'une ligne de formule de prix.
   * 
   * @param pPrixBase
   * @return
   */
  public BigDecimal calculLigne(BigDecimal pPrixBase) {
    if (typeValeur == null) {
      Trace.alerte("La valeur est incorrecte.");
      return null;
    }
    BigDecimal montant = null;
    switch (typeValeur) {
      // Soit application d'un coefficient au prix de base
      case COEFFICIENT:
        if (pPrixBase != null) {
          montant = pPrixBase.multiply(valeur, MathContext.DECIMAL32);
        }
        break;
      // Soit initialisation avec une valeur
      case VALEUR:
        montant = valeur;
        break;
    }
    // Le signe du montant de la ligne
    if (montant != null && operande != null && operande.charValue() == '-') {
      montant = montant.negate();
    }
    
    return ArrondiPrix.appliquer(montant, ArrondiPrix.DECIMALE_MAX);
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le libellé.
   * 
   * @return
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Initialise le libellé.
   * 
   * @param pLibelle
   */
  public void setLibelle(String pLibelle) {
    this.libelle = pLibelle;
  }
  
  /**
   * Retourne l'opérande pour l'opération.
   * 
   * @return
   */
  public Character getOperande() {
    return operande;
  }
  
  /**
   * Initialise l'opérande pour l'opération.
   * 
   * @param pOperande
   */
  public void setOperande(Character pOperande) {
    this.operande = pOperande;
  }
  
  /**
   * Retourne la valeur.
   * 
   * @return
   */
  public BigDecimal getValeur() {
    return valeur;
  }
  
  /**
   * Initialise la valeur.
   * 
   * @param pValeur
   */
  public void setValeur(BigDecimal pValeur) {
    this.valeur = pValeur;
  }
  
  /**
   * Retourne le type de la valeur.
   * 
   * @return
   */
  public EnumTypeValeurFormulePrix getTypeValeur() {
    return typeValeur;
  }
  
  /**
   * Initialise le type de la valeur.
   * 
   * @param pTypeValeur
   */
  public void setTypeValeur(EnumTypeValeurFormulePrix pTypeValeur) {
    this.typeValeur = pTypeValeur;
  }
  
}
