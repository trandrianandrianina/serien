/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc.pricejet;

import java.io.Serializable;

public class ConcurrentPriceJet implements Serializable {
  private String id = null;
  private String nom = null;
  private String photo = null;

  public ConcurrentPriceJet(String idC, String nomC, String photoC) {
    id = idC;
    nom = nomC;
    photo = photoC;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

}
