/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.transporteur;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Transporteur
 */
public class Transporteur extends AbstractClasseMetier<IdTransporteur> {
  private String libelle = "";
  private String modeExpedition = "";
  private String zoneGeographique = "";
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public Transporteur(IdTransporteur pIdTransporteur) {
    super(pIdTransporteur);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  // -- Accesseurs
  
  /*
   * Retourne le nom du transporteur
   */
  public String getLibelle() {
    return libelle;
  }
  
  /*
   * Modifie le nom du transporteur
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  /*
   * Retourne le mode d'expedition
   */
  public String getModeExpedition() {
    return modeExpedition;
  }
  
  /*
   * Modifie le mode d'expedition
   */
  public void setModeExpedition(String modeExpedition) {
    this.modeExpedition = modeExpedition;
  }
  
  /*
   * Retourne la zone géographique
   */
  public String getZoneGeographique() {
    return zoneGeographique;
  }
  
  /*
   * Modifie la zone géographique
   */
  public void setZoneGeographique(String zoneGeographique) {
    this.zoneGeographique = zoneGeographique;
  }
}
