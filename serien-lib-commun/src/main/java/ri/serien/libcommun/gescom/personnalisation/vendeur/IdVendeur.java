/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.vendeur;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un vendeur.
 *
 * L'identifiant est composé du code établissement et du code vendeur.
 * Le code vendeur correspond au paramètre VD du menu des ventes.
 * Il est constitué de 2 à 3 caractères et se présente sous la forme AAX
 * (A = caractère alphanumérique obligatoire, N = caractère numérique obligatoire, X caractère alphanumérique facultatif)
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdVendeur extends AbstractIdAvecEtablissement {
  private static final int LONGUEUR_CODE_MIN = 2;
  private static final int LONGUEUR_CODE_MAX = 3;
  
  // Variables
  private final String code;
  
  // -- Méthodes publiques
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdVendeur(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdVendeur getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new IdVendeur(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  /**
   * Contrôler la validité du code vendeur.
   * Le code vendeur est une chaîne alphanumérique de 1 à 3 caractères.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code vendeur n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_CODE_MIN || valeur.length() > LONGUEUR_CODE_MAX) {
      throw new MessageErreurException(
          "Le code vendeur doit comporter entre " + LONGUEUR_CODE_MIN + " et " + LONGUEUR_CODE_MAX + " caractères : " + valeur);
    }
    return valeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdVendeur controlerId(IdVendeur pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du code vendeur est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du code vendeur n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdVendeur)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de vendeur.");
    }
    IdVendeur id = (IdVendeur) pObject;
    return getCodeEtablissement().equals(id.getCodeEtablissement()) && code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdVendeur)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdVendeur id = (IdVendeur) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
  // -- Accesseurs
  
  /**
   * Code vendeur.
   * Le code vendeur est une chaîne alphanumérique de 2 à 3 caractères.
   */
  public String getCode() {
    return code;
  }
}
