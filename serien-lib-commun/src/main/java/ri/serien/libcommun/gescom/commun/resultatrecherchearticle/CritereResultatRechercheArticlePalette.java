/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.resultatrecherchearticle;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

/**
 * Critères de recherche pour les résultat de recherche des articles palettes au comptoir.
 */
public class CritereResultatRechercheArticlePalette implements Serializable {
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private IdClient idClient = null;
  private IdDocumentVente idDocumentVente = null;
  private IdChantier idChantier = null;
  private String texteRecherche = "";
  
  /**
   * Retourner le filtre sur l'identifiant établissement.
   * @return Identifiant établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier le filtre sur l'identifiant établissement.
   * @param pIdEtablissement Identifiant établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourner le filtre sur l'identifiant magasin.
   * @return Identifiant magasin.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier le filtre sur l'identifiant magasin.
   * @param pIdMagasin Identifiant magasin.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  /**
   * Retourner le filtre sur l'identifiant client.
   * @return Identifiant client.
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Modifier le filtre sur l'identifiant client.
   * @param pIdClient Identifiant client.
   */
  public void setIdClient(IdClient pIdClient) {
    idClient = pIdClient;
  }
  
  /**
   * Retourner le filtre sur l'identifiant document de vente.
   * @return Identifiant document de vente.
   */
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  /**
   * Modifier le filtre sur l'identifiant document de vente.
   * @param pIdDocumentVente Identifiant document de vente.
   */
  public void setIdDocumentVente(IdDocumentVente pIdDocumentVente) {
    this.idDocumentVente = pIdDocumentVente;
  }
  
  /**
   * Retourner le filtre sur l'identifiant chantier.
   * @return Identifiant chantier.
   */
  public IdChantier getIdChantier() {
    return idChantier;
  }
  
  /**
   * Modifier le filtre sur l'identifiant chantier.
   * @param pIdChantier Identifiant chantier.
   */
  public void setIdChantier(IdChantier pIdChantier) {
    idChantier = pIdChantier;
  }
  
  /**
   * Retourner le texte utilisé pour la recherche.
   * @return Texte recherché.
   */
  public String getTexteRecherche() {
    return texteRecherche;
  }
  
  /**
   * Modifier le texte utilisé pour la recherche.
   * @param pTexteRecherche Texte recherché.
   */
  public void setTexteRecherche(String pTexteRecherche) {
    texteRecherche = pTexteRecherche.toUpperCase();
  }
}
