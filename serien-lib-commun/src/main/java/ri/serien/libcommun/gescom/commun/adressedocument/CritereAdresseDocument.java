/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.adressedocument;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

/**
 * Critères de recherche des adresses de documents de ventes.
 */
public class CritereAdresseDocument implements Serializable {
  
  private IdEtablissement idEtablissement = null;
  private EnumCodeEnteteAdresseDocument typeAdresse = null;
  private IdDocumentVente idDocument = null;
  private Integer numeroAdresse = null;
  
  /**
   * Identifiant de l'établissement dans lequel est recherché le client.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Filtre sur l'établissement.
   */
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Filtre sur le code entête des adresses de documents.
   */
  public EnumCodeEnteteAdresseDocument getTypeAdresse() {
    return typeAdresse;
  }
  
  /**
   * Modifier le filtre sur le code entête des adresses de documents.
   */
  public void setTypeAdresse(EnumCodeEnteteAdresseDocument pTypeAdresse) {
    typeAdresse = pTypeAdresse;
  }
  
  /**
   * Filtre sur l'identifiant du document des adresses de documents.
   */
  public IdDocumentVente getIdDocument() {
    return idDocument;
  }
  
  /**
   * Modifier le filtre sur l'identifiant du document des adresses de documents.
   */
  public void setIdDocument(IdDocumentVente pIdDocument) {
    idDocument = pIdDocument;
  }
  
  /**
   * Filtre sur le numéro des adresses de documents.
   */
  public Integer getNumeroAdresse() {
    return numeroAdresse;
  }
  
  /**
   * Modifier le filtre sur le numéro des adresses de documents.
   */
  public void setNumeroAdresse(Integer pNumero) {
    numeroAdresse = pNumero;
  }
  
}
