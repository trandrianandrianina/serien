/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typestockagemagasin;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de types de facturations.
 */
public class ListeTypeStockageMagasin extends ListeClasseMetier<IdTypeStockageMagasin, TypeStockageMagasin, ListeTypeStockageMagasin> {
  // Constantes
  public static final int NOMBREMAXTYPES = 6;
  
  /**
   * Charger les types de facturation pour les identifiants donnés.
   */
  @Override
  public ListeTypeStockageMagasin charger(IdSession pIdSession, List<IdTypeStockageMagasin> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charger les types de facturation pour l'établissement donné.
   */
  @Override
  public ListeTypeStockageMagasin charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return ManagerServiceParametre.chargerListeTypeStockageMagasin(pIdSession, pIdEtablissement);
  }
  
  /**
   * Retourner le type de facturation par défaut.
   * C'est le premier de la liste.
   */
  public IdTypeStockageMagasin getIdTypeFacturationParDefaut() {
    if (isEmpty()) {
      return null;
    }
    return get(0).getId();
  }
  
  /**
   * Retourner un type de facturation de la liste à partir de son nom.
   */
  public TypeStockageMagasin getTypeFacturationParNom(String pLibelle) {
    if (pLibelle == null) {
      return null;
    }
    for (TypeStockageMagasin typeFacturation : this) {
      if (typeFacturation != null && typeFacturation.getLibelle().equals(pLibelle)) {
        return typeFacturation;
      }
    }
    
    return null;
  }
  
}
