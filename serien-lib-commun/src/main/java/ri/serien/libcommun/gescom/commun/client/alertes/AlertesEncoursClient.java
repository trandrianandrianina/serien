/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client.alertes;

import java.io.Serializable;

public class AlertesEncoursClient implements Serializable {
  // Constantes TODO il va falloir trouver les messages exacts
  
  // Variables
  private String messages = "";
  
  // Variables SVGVM0026 : Service controlerEncoursClient
  private boolean plafondDepasseDemandeAutorisationGeneree = false;
  private String messageDemandeAutorisationGeneree = "";
  private boolean plafondDepassePaiementComptantObligatoire = false;
  private String messagePaiementComptantObligatoire = "";
  
  /**
   * Controle et liste les messages d'alerte.
   */
  public boolean controlerAlertes() {
    StringBuilder message = new StringBuilder();
    boolean alertesTrouvees = false;
    messages = "";
    
    if (plafondDepasseDemandeAutorisationGeneree) {
      alertesTrouvees = true;
      message.append(messageDemandeAutorisationGeneree).append('\n');
    }
    if (plafondDepassePaiementComptantObligatoire) {
      alertesTrouvees = true;
      message.append(messagePaiementComptantObligatoire).append('\n');
    }
    
    if (alertesTrouvees) {
      messages = message.toString().trim();
    }
    return alertesTrouvees;
  }
  
  // -- Accesseurs
  
  public String getMessages() {
    return messages;
  }
  
  public void setMessages(String messages) {
    this.messages = messages;
  }
  
  public boolean isPlafondDepasseDemandeAutorisationGeneree() {
    return plafondDepasseDemandeAutorisationGeneree;
  }
  
  public void setPlafondDepasseDemandeAutorisationGeneree(boolean plafondDepasseDemandeAutorisationGeneree) {
    this.plafondDepasseDemandeAutorisationGeneree = plafondDepasseDemandeAutorisationGeneree;
  }
  
  public String getMessageDemandeAutorisationGeneree() {
    return messageDemandeAutorisationGeneree;
  }
  
  public void setMessageDemandeAutorisationGeneree(String messageDemandeAutorisationGeneree) {
    this.messageDemandeAutorisationGeneree = messageDemandeAutorisationGeneree;
  }
  
  public boolean isPlafondDepassePaiementComptantObligatoire() {
    return plafondDepassePaiementComptantObligatoire;
  }
  
  public void setPlafondDepassePaiementComptantObligatoire(boolean plafondDepassePaiementComptantObligatoire) {
    this.plafondDepassePaiementComptantObligatoire = plafondDepassePaiementComptantObligatoire;
  }
  
  public String getMessagePaiementComptantObligatoire() {
    return messagePaiementComptantObligatoire;
  }
  
  public void setMessagePaiementComptantObligatoire(String messagePaiementComptantObligatoire) {
    this.messagePaiementComptantObligatoire = messagePaiementComptantObligatoire;
  }
  
}
