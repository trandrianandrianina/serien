/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.representant;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;

/**
 * Stocke les critères pour les différents types de recherche clients.
 */
public class CriteresRechercheRepresentant extends CriteresBaseRecherche {
  // Constantes
  // Retour: code etablissement, code representant, civilité, nom
  public static final int RECHERCHE_COMPTOIR = 1;
  
  // Variables
}
