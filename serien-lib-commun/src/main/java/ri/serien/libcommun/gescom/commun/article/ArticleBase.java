/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.personnalisation.famille.IdFamille;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Informations de base de l'article.
 * Les informations de base sont le code article et le libellé article. Cette classe doit rester légère car elle est
 * utilisé dans des contextes ou la rapidité ainsi que l'encombrement mémoire sont déterminants.
 */
public class ArticleBase extends AbstractClasseMetier<IdArticle> {
  // Constantes
  public static final int LONGUEUR_LIBELLE_ARTICLE = 30;
  public static final int NOMBRE_LIBELLE_ARTICLE = 4;
  
  // Variables
  private EnumTypeArticle typeArticle = null;
  private String libelle1 = null;
  private String libelle2 = null;
  private String libelle3 = null;
  private String libelle4 = null;
  private IdFamille idFamille = null;
  
  /**
   * Constructeur avec l'identifiant de l'article.
   */
  public ArticleBase(IdArticle pIdArticle) {
    super(pIdArticle);
  }
  
  /**
   * Pour affichage de l'article
   */
  @Override
  public String getTexte() {
    String libelleComplet = getLibelleComplet();
    if (libelleComplet != null) {
      return libelleComplet;
    }
    else {
      return id.getTexte();
    }
  }
  
  /**
   * Retourne s'il s'agit d'un article standard.
   */
  public boolean isArticleStandard() {
    return Constantes.equals(typeArticle, EnumTypeArticle.STANDARD);
  }
  
  /**
   * Retourne s'il s'agit d'un article transport.
   */
  public boolean isArticleTransport() {
    return Constantes.equals(typeArticle, EnumTypeArticle.TRANSPORT);
  }
  
  /**
   * Retourne s'il s'agit d'un article géré par numéros de série.
   */
  public boolean isArticleSerie() {
    return Constantes.equals(typeArticle, EnumTypeArticle.SERIE);
  }
  
  /**
   * Retourne s'il s'agit d'un article spécial.
   */
  public boolean isArticleSpecial() {
    return Constantes.equals(typeArticle, EnumTypeArticle.SPECIAL);
  }
  
  /**
   * Retourne s'il s'agit d'un article commentaire.
   */
  public boolean isArticleCommentaire() {
    return Constantes.equals(typeArticle, EnumTypeArticle.COMMENTAIRE);
  }
  
  /**
   * Retourne s'il s'agit d'un article sur-conditionnement (palette).
   */
  public boolean isArticlePalette() {
    return Constantes.equals(typeArticle, EnumTypeArticle.PALETTE);
  }
  
  // -- Accesseurs
  
  /**
   * Type de l'article (standard, commentaire, spécial, trnasport...).
   */
  public EnumTypeArticle getTypeArticle() {
    return typeArticle;
  }
  
  /**
   * Modifier le type de l'article.
   */
  public void setTypeArticle(EnumTypeArticle pTypeArticle) {
    typeArticle = pTypeArticle;
  }
  
  /**
   * Libellé long de l'article.
   * Cette méthode concaténe les 4 libellés articles en un seul libellé. Dans un contexte ou la saisie des articles
   * s'effectue sur
   * deux lignes de 60 caractères, il faut séparer les libellés 2 et 3 par un espace (mais pas les libellés 1 et 2 ainsi
   * que 3 et 4).
   */
  public String getLibelleComplet() {
    if (libelle1 == null || libelle2 == null || libelle3 == null || libelle4 == null) {
      return null;
    }
    return (libelle1 + libelle2 + libelle3 + libelle4);
  }
  
  /**
   * Modifier le libellé complet de l'article.
   * Le libellé fourni est découpé en 4 portions de 30 caractères.
   */
  public void setLibelleComplet(String pLibelle) {
    if (pLibelle == null) {
      throw new MessageErreurException("Le libellé article est invalide.");
    }
    int longueurMax = NOMBRE_LIBELLE_ARTICLE * LONGUEUR_LIBELLE_ARTICLE;
    if (pLibelle.length() > longueurMax) {
      throw new MessageErreurException("Le libellé article ne doit pas dépasser " + longueurMax + " caractères.");
    }
    String[] decoupe = Constantes.splitString(pLibelle, NOMBRE_LIBELLE_ARTICLE, LONGUEUR_LIBELLE_ARTICLE);
    if (decoupe == null || decoupe.length != NOMBRE_LIBELLE_ARTICLE) {
      throw new MessageErreurException("Il y a eu un problème lors du découpage du libellé article.");
    }
    setLibelle1(decoupe[0]);
    setLibelle2(decoupe[1]);
    setLibelle3(decoupe[2]);
    setLibelle4(decoupe[3]);
  }
  
  /**
   * Première partie du libellé de l'article.
   */
  public String getLibelle1() {
    return libelle1;
  }
  
  /**
   * Modifier la première partie du libellé de l'article.
   */
  public void setLibelle1(String pLibelle) {
    if (pLibelle == null) {
      throw new MessageErreurException("Le libellé article 1 est invalide.");
    }
    if (pLibelle.length() > LONGUEUR_LIBELLE_ARTICLE) {
      throw new MessageErreurException("Le libellé article 1 ne doit pas dépasser " + LONGUEUR_LIBELLE_ARTICLE + " caractères.");
    }
    libelle1 = pLibelle;
  }
  
  /**
   * Partie 2 du libellé de l'article.
   * Cette information est stockée dans l'extension article.
   */
  public String getLibelle2() {
    return libelle2;
  }
  
  /**
   * Modifier la partie 2 du libellé de l'article.
   */
  public void setLibelle2(String pLibelle) {
    if (pLibelle == null) {
      throw new MessageErreurException("Le libellé article 2 est invalide.");
    }
    if (pLibelle.length() > LONGUEUR_LIBELLE_ARTICLE) {
      throw new MessageErreurException("Le libellé article 2 ne doit pas dépasser " + LONGUEUR_LIBELLE_ARTICLE + " caractères.");
    }
    libelle2 = pLibelle;
  }
  
  /**
   * Partie 3 du libellé de l'article.
   * Cette information est stockée dans l'extension article.
   */
  public String getLibelle3() {
    return libelle3;
  }
  
  /**
   * Modifier la partie 3 du libellé de l'article.
   */
  public void setLibelle3(String pLibelle) {
    if (pLibelle == null) {
      throw new MessageErreurException("Le libellé article 3 est invalide.");
    }
    if (pLibelle.length() > LONGUEUR_LIBELLE_ARTICLE) {
      throw new MessageErreurException("Le libellé article 3 ne doit pas dépasser " + LONGUEUR_LIBELLE_ARTICLE + " caractères.");
    }
    libelle3 = pLibelle;
  }
  
  /**
   * Partie 4 du libellé de l'article.
   */
  public String getLibelle4() {
    return libelle4;
  }
  
  /**
   * Modifier la partie 4 du libellé de l'article.
   * Cette information est stockée dans l'extension article.
   */
  public void setLibelle4(String pLibelle) {
    if (pLibelle == null) {
      throw new MessageErreurException("Le libellé article 4 est invalide.");
    }
    if (pLibelle.length() > LONGUEUR_LIBELLE_ARTICLE) {
      throw new MessageErreurException("Le libellé article 4 ne doit pas dépasser " + LONGUEUR_LIBELLE_ARTICLE + " caractères.");
    }
    libelle4 = pLibelle;
  }
  
  /**
   * Renvoyer l'identifiant de la famille de l'article
   */
  public IdFamille getIdFamille() {
    return idFamille;
  }
  
  /**
   * Modifier l'identifiant de la famille de l'article
   */
  public void setIdFamille(IdFamille pIdFamille) {
    idFamille = pIdFamille;
  }
}
