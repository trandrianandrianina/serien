/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import ri.serien.libcommun.gescom.achat.document.EnumCodeExtractionDocumentAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;

/**
 * Cette classe va regrouper les variables communes pour les différentes lignes possibles: ligneAchatArticle, ligneAchatCommentaire.
 */
public abstract class LigneAchat extends LigneAchatBase {
  // Constantes
  // Le numéro de ligne de document (inclu) à conserver (au dela il s'agit des remises et autres)
  public static final int NUMERO_LIGNE_ARTICLE_POUR_REMISES_SPECIALES = 9699;
  
  // Variables
  private IdArticle idArticle = null;
  
  // Type de l'article contenu dans la ligne
  protected EnumCodeExtractionDocumentAchat codeExtraction = null;
  protected String indicateurs = null; // Indicateurs (POIND)
  protected Integer codeEtat = null;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public LigneAchat(IdLigneAchat pIdLigneAchat) {
    super(pIdLigneAchat);
  }
  
  @Override
  public String getTexte() {
    return indicateurs;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne s'il s'agit d'une ligne annulée.
   */
  public boolean isAnnulee() {
    if (codeExtraction != null) {
      return codeExtraction == EnumCodeExtractionDocumentAchat.ANNULATION;
    }
    return false;
  }
  
  /**
   * Retourne s'il s'agit d'une ligne de remise spéciale (les lignes au dela de cette valeur).
   */
  public boolean isLigneRemiseSpeciales() {
    return id.getNumeroLigne().compareTo(NUMERO_LIGNE_ARTICLE_POUR_REMISES_SPECIALES) > 0;
  }
  
  /**
   * Retourne s'il s'agit d'une ligne regroupée (TODO).
   */
  public boolean isLigneRegroupee() {
    return false;
  }
  
  // -- Accesseurs
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public void setIdArticle(IdArticle idArticle) {
    this.idArticle = idArticle;
  }
  
  public EnumCodeExtractionDocumentAchat getCodeExtraction() {
    return codeExtraction;
  }
  
  public void setCodeExtraction(EnumCodeExtractionDocumentAchat pCodeExtraction) {
    codeExtraction = pCodeExtraction;
  }
  
  public String getIndicateurs() {
    return indicateurs;
  }
  
  public void setIndicateurs(String indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  public Integer getCodeEtat() {
    return codeEtat;
  }
  
  public void setCodeEtat(Integer codeEtat) {
    this.codeEtat = codeEtat;
  }
  
}
