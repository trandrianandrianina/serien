/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.communezonegeographique;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de communes de zones géographiques.
 * L'utilisation de cette classe est à privilégier dès que l'on manipule une liste de communes de zones géographiques. Elle contient
 * toutes les méthodes oeuvrant sur la liste tandis que la classe ZoneGeographique contient les méthodes oeuvrant sur une seule
 * zone géographique.
 */
public class ListeCommuneZoneGeographique
    extends ListeClasseMetier<IdCommuneZoneGeographique, CommuneZoneGeographique, ListeCommuneZoneGeographique> {
  
  /**
   * Constructeur par défaut
   */
  public ListeCommuneZoneGeographique() {
    super();
    
  }
  
  /**
   * Charge la liste de Commune suivant une liste d'IdCommuneZoneGeographique
   */
  @Override
  public ListeCommuneZoneGeographique charger(IdSession pIdSession, List<IdCommuneZoneGeographique> pListeId) {
    return ManagerServiceParametre.chargerListeCommuneZoneGeographique(pIdSession, (IdZoneGeographique) pListeId);
  }
  
  /**
   * Charge la liste des communes suivant l'établissement
   */
  @Override
  public ListeCommuneZoneGeographique charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    CritereCommuneZoneGeographique critere = new CritereCommuneZoneGeographique();
    critere.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeCommuneZoneGeographique(pIdSession, critere);
  }
  
}
