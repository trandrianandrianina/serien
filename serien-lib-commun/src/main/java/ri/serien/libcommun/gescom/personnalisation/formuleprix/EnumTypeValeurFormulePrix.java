/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.formuleprix;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les types de valeur pour une ligne de formule de prix.
 */
public enum EnumTypeValeurFormulePrix {
  COEFFICIENT(0, "Coefficient"),
  VALEUR(1, "Valeur");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeValeurFormulePrix(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(numero);
  }
  
  /**
   * Retourner l'objet Enum par son numéro.
   * 
   * @param pNumero
   * @return
   */
  static public EnumTypeValeurFormulePrix valueOfByCode(Integer pNumero) {
    for (EnumTypeValeurFormulePrix value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de la valeur pour la ligne de la formule de prix est invalide : " + pNumero);
  }
  
}
