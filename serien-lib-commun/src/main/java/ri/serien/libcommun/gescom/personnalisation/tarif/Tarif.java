/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.tarif;

import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;

public class Tarif extends AbstractClasseMetier<IdTarif> {
  // Constantes
  public static final int NOMBRE_MAX_TARIFS = 10;
  
  // Variables
  private String designation = "";
  private IdArticle idArticleSupport = null;
  private String[] libellesPrix = new String[NOMBRE_MAX_TARIFS];
  private BigDecimal[] coefficientsPrix = new BigDecimal[NOMBRE_MAX_TARIFS];
  private Date dateApplicationTarifPreparation = null;
  private boolean majAutomatiqueEuro_Devise = false;
  private String codeRattachementTarif = "";
  private String titreRattachementTarif = "";
  private boolean tarifEnPreparation = false;
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public Tarif(IdTarif pIdTarif) {
    super(pIdTarif);
  }
  
  @Override
  public String getTexte() {
    return designation;
  }
  
  // -- Accesseurs
  
  public String getDesignation() {
    return designation;
  }
  
  public void setDesignation(String designation) {
    this.designation = designation;
  }
  
  public IdArticle getIdArticleSupport() {
    return idArticleSupport;
  }
  
  public void setIdArticleSupport(IdArticle pIdArticleSupport) {
    idArticleSupport = pIdArticleSupport;
  }
  
  public String[] getLibellesPrix() {
    return libellesPrix;
  }
  
  public void setLibellesPrix(String[] libellesPrix) {
    this.libellesPrix = libellesPrix;
  }
  
  public BigDecimal[] getCoefficientsPrix() {
    return coefficientsPrix;
  }
  
  public void setCoefficientsPrix(BigDecimal[] coefficientsPrix) {
    this.coefficientsPrix = coefficientsPrix;
  }
  
  public Date getDateApplicationTarifPreparation() {
    return dateApplicationTarifPreparation;
  }
  
  public void setDateApplicationTarifPreparation(Date dateApplicationTarifPreparation) {
    this.dateApplicationTarifPreparation = dateApplicationTarifPreparation;
  }
  
  public boolean isMajAutomatiqueEuro_Devise() {
    return majAutomatiqueEuro_Devise;
  }
  
  public void setMajAutomatiqueEuro_Devise(boolean majAutomatiqueEuro_Devise) {
    this.majAutomatiqueEuro_Devise = majAutomatiqueEuro_Devise;
  }
  
  public String getCodeRattachementTarif() {
    return codeRattachementTarif;
  }
  
  public void setCodeRattachementTarif(String codeRattachementTarif) {
    this.codeRattachementTarif = codeRattachementTarif;
  }
  
  public String getTitreRattachementTarif() {
    return titreRattachementTarif;
  }
  
  public void setTitreRattachementTarif(String titreRattachementTarif) {
    this.titreRattachementTarif = titreRattachementTarif;
  }
  
  public boolean isTarifEnPreparation() {
    return tarifEnPreparation;
  }
  
  public void setTarifEnPreparation(boolean tarifEnPreparation) {
    this.tarifEnPreparation = tarifEnPreparation;
  }
  
}
