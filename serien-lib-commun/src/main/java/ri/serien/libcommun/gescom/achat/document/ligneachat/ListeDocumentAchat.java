/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;

/**
 * Liste de document d'achats.
 * L'utilisation de cette classe est à priviiégier dès qu'on manipule une liste de documents d'achats.
 */
public class ListeDocumentAchat extends ListeClasseMetier<IdDocumentAchat, DocumentAchat, ListeDocumentAchat> {
  /**
   * Constructeur.
   */
  public ListeDocumentAchat() {
  }
  
  /**
   * Charger une liste de documents d'achats à partir de leur ids.
   */
  @Override
  public ListeDocumentAchat charger(IdSession pIdSession, List<IdDocumentAchat> pListeId) {
    return ManagerServiceDocumentAchat.chargerListeDocumentAchat(pIdSession, pListeId);
  }
  
  /**
   * Charger une liste de documents d'achats d'un établissement.
   */
  @Override
  public ListeDocumentAchat charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Construire une liste de document d'achats correspondant à la liste d'identifiants fournie en paramètre.
   * Les données ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeDocumentAchat creerListeNonChargee(List<IdDocumentAchat> pListeIdDocumentAchat) {
    ListeDocumentAchat listeDocumentAchat = new ListeDocumentAchat();
    if (pListeIdDocumentAchat != null) {
      for (IdDocumentAchat idDocumentAchat : pListeIdDocumentAchat) {
        DocumentAchat documentAchat = new DocumentAchat(idDocumentAchat);
        documentAchat.setCharge(false);
        listeDocumentAchat.add(documentAchat);
      }
    }
    return listeDocumentAchat;
  }
  
}
