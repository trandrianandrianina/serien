/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.blocnotes;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Bloc-notes.
 *
 * L'identifiant est composé du code établissement, d'un code type de tiers et d'un indicatif tiers.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdBlocNote extends AbstractIdAvecEtablissement {
  // Constantes
  public static final int LONGUEUR_ORIGINE = 1;
  public static final int LONGUEUR_INDICATIF_TIERS = 20;
  public static final int LONGUEUR_INDICATIF = 30;
  public static final int LONGUEUR_NUMERO = 4;
  
  // Variables
  private final EnumTypeTiersBlocNote codeTypeTiers;
  private final String codeTiers;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdBlocNote(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, EnumTypeTiersBlocNote pCodeTypeTiers,
      String pCodeTiers) {
    super(EnumEtatObjetMetier.MODIFIE, pIdEtablissement);
    codeTypeTiers = controlerTypeTiers(pCodeTypeTiers);
    codeTiers = controlerCodeTiers(pCodeTiers);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdBlocNote getInstance(IdEtablissement pIdEtablissement, EnumTypeTiersBlocNote pCodeTypeTiers, String pCodeTiers) {
    return new IdBlocNote(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCodeTypeTiers, pCodeTiers);
  }
  
  /**
   * Créer un identifiant pour un article.
   */
  public static IdBlocNote getInstancePourArticle(IdArticle pIdArticle) {
    return new IdBlocNote(EnumEtatObjetMetier.MODIFIE, pIdArticle.getIdEtablissement(), EnumTypeTiersBlocNote.ARTICLE,
        pIdArticle.getCodeArticle());
  }
  
  /**
   * Créer un identifiant pour un client.
   */
  public static IdBlocNote getInstancePourClient(IdClient pIdClient) {
    return new IdBlocNote(EnumEtatObjetMetier.MODIFIE, pIdClient.getIdEtablissement(), EnumTypeTiersBlocNote.CLIENT,
        pIdClient.getIndicatif(IdClient.INDICATIF_NUM_SUF));
  }
  
  /**
   * Créer un identifiant pour un fournisseur.
   */
  public static IdBlocNote getInstancePourFournisseur(IdFournisseur pIdFournisseur) {
    return new IdBlocNote(EnumEtatObjetMetier.MODIFIE, pIdFournisseur.getIdEtablissement(), EnumTypeTiersBlocNote.FOURNISSEUR,
        pIdFournisseur.getIndicatifCourt());
  }
  
  /**
   * Créer un identifiant pour une condition d'achat.
   */
  public static IdBlocNote getInstancePourConditionAchat(IdArticle pIdArticle, IdFournisseur pIdFournisseur) {
    return new IdBlocNote(EnumEtatObjetMetier.MODIFIE, pIdArticle.getIdEtablissement(), EnumTypeTiersBlocNote.CONDTION_ACHATS,
        (pIdArticle.getIndicatifCodeArticle() + pIdFournisseur.getIndicatifCourt()));
  }
  
  /**
   * Créer un identifiant pour un document de vente
   */
  public static IdBlocNote getInstancePourDocumentVente(IdDocumentVente pIdDocumentVente) {
    return new IdBlocNote(EnumEtatObjetMetier.MODIFIE, pIdDocumentVente.getIdEtablissement(), EnumTypeTiersBlocNote.DOCUMENT_VENTE,
        (pIdDocumentVente.toString()));
  }
  
  /**
   * Contrôler la validité du code ligne.
   */
  private static EnumTypeTiersBlocNote controlerTypeTiers(EnumTypeTiersBlocNote pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le code type de tiers n'est pas renseigné.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler la validité du code tiers.
   */
  private static String controlerCodeTiers(String pValeur) {
    if (pValeur == null || pValeur.trim().isEmpty()) {
      throw new MessageErreurException("Le code tiers n'est pas renseigné.");
    }
    return pValeur.trim();
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdBlocNote controlerId(IdBlocNote pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du bloc-notes est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du bloc-notes n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + codeTypeTiers.hashCode();
    cle = 37 * cle + codeTiers.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdBlocNote)) {
      return false;
    }
    IdBlocNote id = (IdBlocNote) pObject;
    return getIdEtablissement().equals(id.getIdEtablissement()) && codeTypeTiers.equals(id.codeTypeTiers)
        && codeTiers.equals(id.codeTiers);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdBlocNote)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdBlocNote id = (IdBlocNote) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = codeTypeTiers.compareTo(id.codeTypeTiers);
    if (comparaison != 0) {
      return comparaison;
    }
    return codeTiers.compareTo(id.codeTiers);
  }
  
  @Override
  public String getTexte() {
    return "" + codeTypeTiers + SEPARATEUR_ID + codeTiers;
  }
  
  // Accesseurs
  
  /**
   * Code du type de tiers associé au bloc-notes.
   */
  public EnumTypeTiersBlocNote getCodeTypeTiers() {
    return codeTypeTiers;
  }
  
  /**
   * Code du tiers.
   * Le code tiers dépend du type de tiers. Dans la plupart des cas, il contient l'indicatif du tiers (le code article, le numéro du
   * document par exemple) sauf pour les cas suivant :
   * - 1 (condition de ventes) : INDCNV
   * - 2 (condition d'achats) : ART + COL + FRS
   * - 1 (affaire) : numéro d'affaire
   * - 2 (action) : numéro d'action
   * - p (ordre de fabrication prévisionnel entête) : P cadré droite/OBIND
   * - q (ordre de fabrication prévisionnel ligne) : P cadré droite/OBIND
   */
  public String getCodeTiers() {
    return codeTiers;
  }
  
  /**
   * Indicatif complet du bloc-notes.
   * Clé formatée: si numéro > 0 alors code article + numéro sinon juste le code article.
   */
  public String getIndicatif() {
    if (codeTypeTiers.equals(EnumTypeTiersBlocNote.CONDTION_ACHATS)) {
      return String.format("%-" + LONGUEUR_INDICATIF + "s", codeTiers);
    }
    
    String indicatif = Constantes.normerTexte(codeTiers);
    
    if (indicatif.isEmpty()) {
      throw new MessageErreurException("L'indicatif tiers est vide.");
    }
    
    if (indicatif.length() > LONGUEUR_INDICATIF_TIERS) {
      indicatif = indicatif.substring(0, LONGUEUR_INDICATIF_TIERS);
    }
    return String.format("%-" + LONGUEUR_INDICATIF + "s", indicatif);
  }
  
}
