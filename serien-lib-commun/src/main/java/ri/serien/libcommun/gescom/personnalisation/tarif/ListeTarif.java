/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.tarif;

import java.util.ArrayList;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de tarifs.
 */
public class ListeTarif extends ArrayList<Tarif> {
  /**
   * Constructeur.
   */
  public ListeTarif() {
  }
  
  /**
   * Charge tous les tarifs de l'établissement donnée.
   */
  public static ListeTarif chargerTout(IdSession pIdSession, boolean aAjouteEnregistrementVide, IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas défini.");
    }
    
    CriteresRechercheTarifs criteres = new CriteresRechercheTarifs();
    criteres.setTypeRecherche(CriteresRechercheTarifs.TOUT_RETOURNER_POUR_UN_ETABLISSEMENT);
    criteres.setNumeroPage(1);
    criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
    criteres.setIdEtablissement(pIdEtablissement);
    
    return ManagerServiceParametre.chargerListeTarif(pIdSession, criteres);
  }
  
  /**
   * Retourne le libellé de la tarif à partir de son code.
   */
  public String retournerDesignation(String aCode) {
    if ((aCode == null) || (aCode.trim().equals(""))) {
      return "";
    }
    for (Tarif tarif : this) {
      if (tarif.getId().getCode().equals(aCode)) {
        return tarif.getDesignation();
      }
    }
    return "";
  }
}
