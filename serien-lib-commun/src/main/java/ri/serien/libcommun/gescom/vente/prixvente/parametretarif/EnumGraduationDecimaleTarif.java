/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prixvente.parametretarif;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les graduations sur les décimales possibles pour les tarifs d'un article.
 */
public enum EnumGraduationDecimaleTarif {
  AUCUNE(0, null, "Aucune"),
  A_00_05(1, "0.05", "Graduation à 0.05"),
  A_00_10(2, "0.10", "Graduation à 0.10"),
  A_00_25(3, "0.25", "Graduation à 0.25"),
  A_00_50(4, "0.50", "Graduation à 0.50"),
  A_01_00(5, "1.00", "Graduation à 1.00"),
  A_10_00(6, "10.00", "Graduation à 10.00");
  
  private final Integer numero;
  private final BigDecimal graduationDecimale;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumGraduationDecimaleTarif(Integer pNumero, String pGraduationDecimale, String pLibelle) {
    numero = pNumero;
    if (pGraduationDecimale != null) {
      graduationDecimale = new BigDecimal(pGraduationDecimale);
    }
    else {
      graduationDecimale = null;
    }
    libelle = pLibelle;
  }
  
  // -- Méthodes publiques
  
  /**
   * Applique la graduation sur une valeur donnée.
   * Algo retranscrit à partir du programme RPG SGVMX/FGVMTAR afin d'être sur de calculer les mêmes valeurs.
   * 
   * @param pValeur
   * @return
   */
  public BigDecimal appliquerGraduation(BigDecimal pValeur) {
    if (pValeur == null || pValeur.compareTo(BigDecimal.ZERO) == 0 || graduationDecimale == null) {
      return pValeur;
    }
    
    // Sauvegarde du nombre de décimales
    int scale = pValeur.scale();
    // Si un arrondi sur les décimales doit être appliqué alors le nombre de chiffres après la virgule est forcé à 2
    if (numero > 0) {
      scale = ArrondiPrix.DECIMALE_STANDARD;
    }
    // Application de la graduation sur les décimales
    BigDecimal moitieArrondi = graduationDecimale.divide(BigDecimal.valueOf(2), MathContext.DECIMAL32);
    pValeur = pValeur.add(moitieArrondi).divide(graduationDecimale, MathContext.DECIMAL32);
    int valeurEntiere = pValeur.toBigInteger().intValue();
    if (valeurEntiere == 0) {
      pValeur = new BigDecimal(graduationDecimale.toString());
    }
    else {
      pValeur = BigDecimal.valueOf(valeurEntiere).multiply(graduationDecimale);
    }
    return pValeur.setScale(scale, RoundingMode.HALF_UP);
  }
  
  // -- Accesseurs
  
  /**
   * Le numéro sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numéro associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return getLibelle();
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumGraduationDecimaleTarif valueOfByNumero(Integer pNumero) {
    for (EnumGraduationDecimaleTarif value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le numéro de la graduation des décimales du tarif est invalide : " + pNumero);
  }
  
}
