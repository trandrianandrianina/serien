/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun;

import ri.serien.libcommun.outils.MessageErreurException;

public enum EnumOrdreCritereDeTriEtDeSelection {
  AUCUN_CRITERE(0, " "), CRITERE_PRINCIPAL(1, "1er critère de tri"), CRITERE_SECONDAIRE(2, "2eme critère de tri"),
  CRITERE_TERTIAIRE(3, "3eme critère de tri");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrdreCritereDeTriEtDeSelection(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données sous forme de chaine de caractères.
   */
  public String getStringCode() {
    return code.toString();
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  static public EnumOrdreCritereDeTriEtDeSelection valueOfByCode(String pCode) {
    for (EnumOrdreCritereDeTriEtDeSelection value : values()) {
      int valeur = Integer.parseInt(pCode);
      if (valeur == value.getCode()) {
        return value;
      }
    }
    throw new MessageErreurException("L'ordre de tri est inconnu : " + pCode);
  }
}
