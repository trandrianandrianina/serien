/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.devise;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'une devise.
 *
 * L'identifiant est composé du libelle court.
 * Il est composé de 1 à 3 caractères
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation. *
 */
public class IdDevise extends AbstractId {
  
  // Constantes
  public static final int LONGUEUR_DEVISE_MIN = 1;
  public static final int LONGUEUR_DEVISE_MAX = 3;
  // Variables
  private String codeDevise = null;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdDevise(EnumEtatObjetMetier pEnumEtatObjetMetier, String pCodeDevise) {
    super(pEnumEtatObjetMetier);
    codeDevise = controlerCode(pCodeDevise);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdDevise getInstance(String pCodeDevise) {
    return new IdDevise(EnumEtatObjetMetier.MODIFIE, pCodeDevise);
  }
  
  /**
   * Contrôler la validité du code devise.
   * Le code devise client est une chaîne 3 caractères.
   */
  private static String controlerCode(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("La devise n'est pas renseigné.");
    }
    String valeur = pValeur.trim();
    if (valeur.length() < LONGUEUR_DEVISE_MIN || valeur.length() > LONGUEUR_DEVISE_MAX) {
      throw new MessageErreurException(
          "La devise doit comporter entre " + LONGUEUR_DEVISE_MIN + " et " + LONGUEUR_DEVISE_MAX + " caractères : " + valeur);
    }
    return valeur;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + codeDevise.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdDevise)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux devise.");
    }
    IdDevise id = (IdDevise) pObject;
    return codeDevise.equals(id.codeDevise);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdDevise)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdDevise id = (IdDevise) pObject;
    return codeDevise.compareTo(id.codeDevise);
  }
  
  @Override
  public String getTexte() {
    return codeDevise;
  }
  
  // -- Accesseurs
  /**
   * Code Devise
   * Le code devise est une chaine de 3 caractères.
   */
  public String getCodeDevise() {
    return codeDevise;
  }
}
