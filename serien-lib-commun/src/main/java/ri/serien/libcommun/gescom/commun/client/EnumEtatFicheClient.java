/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.client;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Etat d'une fiche client. Champs nommé topSystème en base.
 * L'état verrouillé est possiblement représenté par toutes les valeurs entre 1 et 8 (normalement 8)
 */
public enum EnumEtatFicheClient {
  ACTIVE(0, "Active"),
  VERROUILLE(8, "Verrouillée"),
  ANNULE(9, "Annulée");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumEtatFicheClient(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   * pour l'état verrouillé, les valeurs de 1 à 8 sont possibles, bien que normalement ce devrait être 8
   */
  static public EnumEtatFicheClient valueOfByCode(Integer pCode) {
    if (pCode > 0 && pCode < 9) {
      pCode = 8;
    }
    for (EnumEtatFicheClient value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("L'état de la fiche client est invalide : " + pCode);
  }
}
