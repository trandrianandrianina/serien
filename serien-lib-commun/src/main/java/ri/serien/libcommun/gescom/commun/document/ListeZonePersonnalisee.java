/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.document;

import java.io.Serializable;

/**
 * Liste de zones personnalisées.
 */
public class ListeZonePersonnalisee implements Serializable {
  // Constantes
  private static final int NOMBRE_TOP_PERSONNALISATION = 5;
  public static final int TOP_PERSONNALISATION_1 = 0;
  public static final int TOP_PERSONNALISATION_2 = 1;
  public static final int TOP_PERSONNALISATION_3 = 2;
  public static final int TOP_PERSONNALISATION_4 = 3;
  public static final int TOP_PERSONNALISATION_5 = 4;
  
  // Variables
  private String[] topPersonnalisation = new String[NOMBRE_TOP_PERSONNALISATION];
  
  // -- Accesseurs
  
  /**
   * Retourne le top de la personnalisation à l'index voulu.
   */
  public String getTopPersonnalisation(int pIndex) {
    if (0 < pIndex && pIndex >= NOMBRE_TOP_PERSONNALISATION) {
      return null;
    }
    if (topPersonnalisation[pIndex] == null) {
      return "  ";
    }
    return topPersonnalisation[pIndex];
  }
  
  /**
   * Initialise le top de la personnalisation à l'index voulu.
   */
  public void setTopPersonnalisation(int pIndex, String pValeur) {
    if (0 < pIndex && pIndex >= NOMBRE_TOP_PERSONNALISATION) {
      return;
    }
    if (pValeur != null) {
      topPersonnalisation[pIndex] = pValeur.trim();
    }
    else {
      topPersonnalisation[pIndex] = pValeur;
    }
  }
  
  public String getTopPersonnalisation1() {
    return getTopPersonnalisation(TOP_PERSONNALISATION_1);
  }
  
  public void setTopPersonnalisation1(String pValeur) {
    setTopPersonnalisation(TOP_PERSONNALISATION_1, pValeur);
  }
  
  public String getTopPersonnalisation2() {
    return getTopPersonnalisation(TOP_PERSONNALISATION_2);
  }
  
  public void setTopPersonnalisation2(String pValeur) {
    setTopPersonnalisation(TOP_PERSONNALISATION_2, pValeur);
  }
  
  public String getTopPersonnalisation3() {
    return getTopPersonnalisation(TOP_PERSONNALISATION_3);
  }
  
  public void setTopPersonnalisation3(String pValeur) {
    setTopPersonnalisation(TOP_PERSONNALISATION_3, pValeur);
  }
  
  public String getTopPersonnalisation4() {
    return getTopPersonnalisation(TOP_PERSONNALISATION_4);
  }
  
  public void setTopPersonnalisation4(String pValeur) {
    setTopPersonnalisation(TOP_PERSONNALISATION_4, pValeur);
  }
  
  public String getTopPersonnalisation5() {
    return getTopPersonnalisation(TOP_PERSONNALISATION_5);
  }
  
  public void setTopPersonnalisation5(String pValeur) {
    setTopPersonnalisation(TOP_PERSONNALISATION_5, pValeur);
  }
  
}
