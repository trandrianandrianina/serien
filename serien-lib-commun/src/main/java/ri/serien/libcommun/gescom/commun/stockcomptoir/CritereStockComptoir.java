/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockcomptoir;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

/**
 * Critères de recherche pour le stock des articles.
 * Pour l'instant, tout les critères sont optionnels à l'exception du code article qui doit être renseigné.
 */
public class CritereStockComptoir implements Serializable {
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private String codeArticle = null;
  private Integer nbDecimales = null;
  
  /**
   * Retourner l'établissement sur lequel doit se porter la recherche des stocks
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'établissement pour lequel on souhaite les stocks.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Identifiant du magasin pour lequel on souhaite les stocks.
   * Si aucun magasin n'est transmis on recherche sur tous les magasins de l'établissement
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier le filtre sur le magasin.
   * L'établissement est également mis à jour si le magasin n'est pas null.
   * Si aucun magasin n'est renseigné, la recherche est effectuée sur tous les magasins de l'établissement.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
    if (idMagasin != null) {
      idEtablissement = idMagasin.getIdEtablissement();
    }
  }
  
  /**
   * Retourner le code article sur lequel la recherche se base.
   * On ne retourne pas l'IdArticle car la recherche peut se faire sur le même code article et plusieurs établissement
   */
  public String getCodeArticle() {
    return codeArticle;
  }
  
  /**
   * Modifier le code article pour lequel on souhaite les stocks.
   * On fournit un code article, et non pas un identifiant article, car la recherche peut se faire sur des articles partageant le même
   * code sur plusieurs établissements.
   */
  public void setCodeArticle(String pCodeArticle) {
    codeArticle = pCodeArticle;
  }
  
  /**
   * Retourner le nombre de décimales des stocks retournés par la recherche.
   */
  public Integer getNbDecimales() {
    return nbDecimales;
  }
  
  /**
   * Fixer le nombre de décimales des stocks retournés par la recherche.
   */
  public void setNbDecimales(Integer nbDecimales) {
    this.nbDecimales = nbDecimales;
  }
}
