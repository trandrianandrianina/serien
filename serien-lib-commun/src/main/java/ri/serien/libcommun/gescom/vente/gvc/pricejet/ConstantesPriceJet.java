/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.gvc.pricejet;

import java.io.Serializable;

public class ConstantesPriceJet implements Serializable {
  // VALEURS A PASSER DANS URL PRICEJET POUR RECUPERER UNE LISTE
  // Renvoie la liste de tous les codes articles du compte
  public final static String LISTE_TOUS = "TOUS";
  // Renvoie la liste de tous les concurrents du compte
  public final static String LISTE_CONCURRENT = "CONCURRENT";
  // Renvoie la liste des groupes du compte
  public final static String LISTE_GROUPE = "GROUPE";
  // Renvoie la liste des codes articles dont mon prix est inférieur au prix moyen.
  public final static String LISTE_PRIXINFMOY = "PRIXINFMOY";
  // PRIXSUPMOY : Renvoi la liste des codes articles dont mon prix est supérieur au prix moyen.
  public final static String LISTE_PRIXSUPMOY = "PRIXSUPMOY";
  // PRIXPLUSCHER : Renvoi la liste des codes articles dont mon prix est le plus cher de tous les concurrents.
  public final static String LISTE_PRIXPLUSCHER = "PRIXPLUSCHER";
  // PRIXMOINSCHER : Renvoi la liste des codes articles dont mon prix est le moins cher de tous les concurrents.
  public final static String LISTE_PRIXMOINSCHER = "PRIXMOINSCHER";
  // PRIXPASMOINSCHER : Renvoi la liste des codes articles dont mon prix n'est pas le moins cher.
  public final static String LISTE_PRIXPASMOINSCHER = "PRIXPASMOINSCHER";

  // VALEURS A PASSER DANS URL PRICEJET POUR RECUPERER UNE TARIF OU UN NB DE CONCURRENTS
  // Prix minimum
  public final static String ARTICLE_PRIX_MINI = "MIN";
  // Prix maximum
  public final static String ARTICLE_PRIX_MAX = "MAX";
  // Prix Moyen
  public final static String ARTICLE_PRIX_AVG = "AVG";
  // NB DE CONCURRENTS CONCERNES PAR L'ANALYSE
  public final static String NB_CONCURRENTS = "COUNT";
}
