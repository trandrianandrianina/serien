/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.prodevis;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

public class DescriptionProDevis implements Serializable {
  
  // Constantes
  public static final int TAILLE_NOM_FICHIER = 120;
  public static final int TAILLE_NUMERO = 15;
  public static final int TAILLE_NOM_CLIENT = 30;
  public static final int TAILLE_PRENOM_CLIENT = 20;
  public static final int NOMBRE_DECIMALE_MONTANT = 2;
  
  // Variables
  private IdEtablissement etablissement = null;
  private Integer cleUnique = null;
  private EnumStatutProDevis statut = null;
  private String utilisateurChangementStatut = "";
  private String numero = "";
  private String nomFichier = "";
  private Date dateCreation = null;
  private String nomClient = "";
  private String prenomClient = "";
  private int nombreTotalDArticles = 0;
  private BigDecimal montantTotalHT = BigDecimal.ZERO;
  private Date dateImport = null;
  private IdDocumentVente idDocumentImporte = null;
  
  // -- Accesseurs
  
  public IdEtablissement getEtablissement() {
    return etablissement;
  }
  
  public void setEtablissement(IdEtablissement etablissement) {
    this.etablissement = etablissement;
  }
  
  public Integer getCleUnique() {
    return cleUnique;
  }
  
  public void setCleUnique(Integer cleUnique) {
    this.cleUnique = cleUnique;
  }
  
  public EnumStatutProDevis getStatut() {
    return statut;
  }
  
  public void setStatut(EnumStatutProDevis statut) {
    this.statut = statut;
  }
  
  public String getUtilisateurChangementStatut() {
    return utilisateurChangementStatut;
  }
  
  public void setUtilisateurChangementStatut(String utilisateurChangementStatut) {
    this.utilisateurChangementStatut = utilisateurChangementStatut;
  }
  
  public String getNumero() {
    return numero;
  }
  
  public void setNumero(String numero) {
    this.numero = numero;
  }
  
  public String getNomFichier() {
    return nomFichier;
  }
  
  public void setNomFichier(String nomFichier) {
    this.nomFichier = nomFichier;
  }
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public String getNomClient() {
    return nomClient;
  }
  
  public void setNomClient(String nomClient) {
    this.nomClient = nomClient;
  }
  
  public String getPrenomClient() {
    return prenomClient;
  }
  
  public void setPrenomClient(String prenomClient) {
    this.prenomClient = prenomClient;
  }
  
  public int getNombreTotalDArticles() {
    return nombreTotalDArticles;
  }
  
  public void setNombreTotalDArticles(int nombreTotalDArticles) {
    this.nombreTotalDArticles = nombreTotalDArticles;
  }
  
  public BigDecimal getMontantTotalHT() {
    return montantTotalHT;
  }
  
  public void setMontantTotalHT(BigDecimal montantTotalHT) {
    this.montantTotalHT = montantTotalHT;
  }
  
  public Date getDateImport() {
    return dateImport;
  }
  
  public void setDateImport(Date dateImport) {
    this.dateImport = dateImport;
  }
  
  public IdDocumentVente getIdDocumentImporte() {
    return idDocumentImporte;
  }
  
  public void setIdDocumentImporte(IdDocumentVente idDocumentImporte) {
    this.idDocumentImporte = idDocumentImporte;
  }
  
}
