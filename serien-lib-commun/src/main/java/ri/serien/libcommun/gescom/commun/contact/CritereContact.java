/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.contact;

import java.io.Serializable;

import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.IdCategorieContact;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;

/**
 * Critères de recherche des contacts.
 */
public class CritereContact implements Serializable {
  // L'établissement n'est pas renseigné dans la table PSEMRTEM (il vaut blanc) par contre il est utilisé dans la table des liens PSEMRTLM
  private IdEtablissement idEtablissement = null;
  private EnumTypeContact typeContact = null;
  private IdFournisseur idFournisseur = null;
  private IdClient idClient = null;
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  private IdCategorieContact idCategorieContact = null;
  private String filtreAlphabetique = "";
  private CodePostalCommune filtreCommune = null;
  private boolean isVisualisationContactDesactive = false;
  
  /**
   * Initialiser les critères de recherche pour les contacts fournisseurs.
   * L'établissement est initialisé avec celui de l'identifiant du fournisseur.
   * Le type de contact qui ne doit pas être null.
   */
  public void initialiserContactFournisseur(EnumTypeContact pEnumTypeContact, IdFournisseur pIdFournisseur) {
    if (pIdFournisseur != null) {
      idEtablissement = pIdFournisseur.getIdEtablissement();
    }
    typeContact = pEnumTypeContact;
    rechercheGenerique.initialiser();
    idFournisseur = pIdFournisseur;
    idClient = null;
    idCategorieContact = null;
    filtreCommune = null;
    filtreAlphabetique = "";
    isVisualisationContactDesactive = false;
  }
  
  /**
   * Initialiser les critères de recherche pour les contacts clients.
   * L'établissement est initialisé avec celui de l'identifiant du client.
   * Le type de contact qui ne doit pas être null.
   */
  public void initialiserContactClient(EnumTypeContact pEnumTypeContact, IdClient pIdClient) {
    if (pIdClient != null) {
      idEtablissement = pIdClient.getIdEtablissement();
    }
    typeContact = pEnumTypeContact;
    rechercheGenerique.initialiser();
    idFournisseur = null;
    idClient = pIdClient;
    idCategorieContact = null;
    filtreCommune = null;
    filtreAlphabetique = "";
    isVisualisationContactDesactive = false;
  }
  
  /**
   * Identifiant de l'établissement dans lequel est recherché le client.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Filtre sur l'établissement.
   */
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Filtre sur le type de contact.
   */
  public EnumTypeContact getTypeContact() {
    return typeContact;
  }
  
  /**
   * Modifie le type de contact.
   */
  public void setTypeContact(EnumTypeContact typeContact) {
    this.typeContact = typeContact;
  }
  
  /**
   * /**
   * Texte de la recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void setTexteRechercheGenerique(String pTexteRecherche) {
    rechercheGenerique.setTexte(pTexteRecherche);
  }
  
  /**
   * Identifiant du fournisseur auquel est lié le contact
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Identifiant du fournisseur auquel est lié le contact
   */
  public void setIdFournisseur(IdFournisseur idFournisseur) {
    this.idFournisseur = idFournisseur;
  }
  
  /**
   * Identifiant du client auquel est lié le contact
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Identifiant du client auquel est lié le contact
   */
  public void setIdClient(IdClient idClient) {
    this.idClient = idClient;
  }
  
  /**
   * Identifiant de la catégorie du contact
   */
  public IdCategorieContact getIdCategorieContact() {
    return idCategorieContact;
  }
  
  /**
   * Identifiant de la catégorie du contact
   */
  public void setIdCategorieContact(IdCategorieContact idCategorieContact) {
    this.idCategorieContact = idCategorieContact;
  }
  
  /**
   * Filtre sur une lettre de l'alphabet
   */
  public String getFiltreAlphabetique() {
    return filtreAlphabetique;
  }
  
  /**
   * Filtre sur une lettre de l'alphabet
   */
  public void setFiltreAlphabetique(String pFiltreAlphabetique) {
    filtreAlphabetique = pFiltreAlphabetique;
  }
  
  /**
   * Filtre sur le code postal et la ville
   */
  public CodePostalCommune getFiltreCodePostalCommune() {
    return filtreCommune;
  }
  
  /**
   * Filtre sur le code postal et la ville
   */
  public void setFiltreCodePostalCommune(CodePostalCommune pFiltreCommune) {
    filtreCommune = pFiltreCommune;
  }
  
  /**
   * Filtre sur l'affichage des contacts désactivés
   */
  public boolean isVisualisationContactDesactive() {
    return isVisualisationContactDesactive;
  }
  
  /**
   * Filtre sur l'affichage des contacts désactivés
   */
  public void setVisualisationContactDesactive(boolean isVisualisationContactDesactive) {
    this.isVisualisationContactDesactive = isVisualisationContactDesactive;
  }
}
