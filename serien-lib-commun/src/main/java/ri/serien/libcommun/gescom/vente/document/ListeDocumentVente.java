/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Liste de DocumentVente.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de documents de vente.
 */
public class ListeDocumentVente extends ListeClasseMetier<IdDocumentVente, DocumentVente, ListeDocumentVente> {
  /**
   * Constructeur.
   */
  public ListeDocumentVente() {
  }
  
  /**
   * Constructeur par copie d'une liste.
   */
  public ListeDocumentVente(ListeDocumentVente pListeDocumentVente) {
    super(pListeDocumentVente);
  }
  
  /**
   * Charger les documents de ventes dont les identifiants sont fournis.
   */
  @Override
  public ListeDocumentVente charger(IdSession pIdSession, List<IdDocumentVente> pListeId) {
    throw new MessageErreurException("Le chargement de données paginé n'est pas opérationnel.");
  }
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeDocumentVente charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
}
