/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.lot;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceArticle;

/**
 * Liste de lots.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de Lot.
 */
public class ListeLot extends ListeClasseMetier<IdLot, Lot, ListeLot> {
  /**
   * Constructeur.
   */
  public ListeLot() {
  }
  
  /**
   * Construire une liste de document de bases correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des documents de base ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeLot creerListeNonChargee(List<IdLot> pListeIdLot) {
    ListeLot listeLot = new ListeLot();
    if (pListeIdLot != null) {
      for (IdLot idLot : pListeIdLot) {
        Lot lot = new Lot(idLot);
        lot.setCharge(false);
        listeLot.add(lot);
      }
    }
    return listeLot;
  }
  
  @Override
  public ListeLot charger(IdSession pIdSession, List<IdLot> pListeIdLot) {
    return ManagerServiceArticle.chargerListeLot(pIdSession, pListeIdLot);
  }
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeLot charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  public ListeLot charger(IdSession pIdSession, CritereLot pCritereLot) {
    return ManagerServiceArticle.chargerListeLot(pIdSession, pCritereLot);
  }
  
  /**
   * 
   * Trier la liste par code lot.
   *
   */
  public void trierParNumero() {
    Collections.sort(this, new Comparator<Lot>() {
      @Override
      public int compare(Lot o1, Lot o2) {
        String code1 = o1.getId().getCode();
        String code2 = o2.getId().getCode();
        return code1.compareToIgnoreCase(code2);
      }
    });
  }
  
  /**
   * 
   * Trier la liste par ordre croissant de quantité disponible.
   *
   */
  public void trierParQuantiteDisponible() {
    Collections.sort(this, new Comparator<Lot>() {
      @Override
      public int compare(Lot o1, Lot o2) {
        BigDecimal quantite1 = o1.getStockDisponible();
        BigDecimal quantite2 = o2.getStockDisponible();
        return (quantite1.compareTo(quantite2));
      }
    });
  }
  
  /**
   * 
   * Filtrer les lots dont la quantité disponible est supérieure ou égale à la quantité commandée.
   * 
   * @param pQuantiteCommandee
   * @return ListeLot
   */
  public ListeLot filtrerParQuantiteDisponible(BigDecimal pQuantiteCommandee) {
    ListeLot listeFiltree = new ListeLot();
    for (Lot lot : this) {
      if (lot.getStockDisponible().compareTo(pQuantiteCommandee) >= 0) {
        listeFiltree.add(lot);
      }
    }
    return listeFiltree;
  }
  
  /**
   * 
   * Trier la liste par date de péremption.
   *
   */
  public void trierParDatePeremption() {
    Collections.sort(this, new Comparator<Lot>() {
      @Override
      public int compare(Lot o1, Lot o2) {
        Date date1 = o1.getDatePeremption();
        Date date2 = o2.getDatePeremption();
        return (date1.compareTo(date2));
      }
    });
  }
  
  /**
   * 
   * Filtrer les lots dont la quantité disponible est à 0.
   * 
   * @return ListeLot
   */
  public ListeLot filtrerDisponibleAZero() {
    ListeLot listeFiltree = new ListeLot();
    for (Lot lot : this) {
      if (lot.getStockDisponible().compareTo(BigDecimal.ZERO) != 0) {
        listeFiltree.add(lot);
      }
    }
    return listeFiltree;
  }
  
  /**
   * 
   * Filtrer les lots dont la quantité saisie est à 0.
   * 
   * @return ListeLot
   */
  public ListeLot filtrerQuantiteSaisieAZero() {
    ListeLot listeFiltree = new ListeLot();
    for (Lot lot : this) {
      if ((lot.getQuantiteSaisi() != null && lot.getQuantiteSaisi().compareTo(BigDecimal.ZERO) != 0)
          || (lot.getQuantitePrecedentSaisi() != null && lot.getQuantitePrecedentSaisi().compareTo(BigDecimal.ZERO) != 0)) {
        listeFiltree.add(lot);
      }
    }
    return listeFiltree;
  }
  
}
