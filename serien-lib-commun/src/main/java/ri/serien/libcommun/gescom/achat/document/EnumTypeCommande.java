/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'une ligne d'un document achat ou vente.
 */
public enum EnumTypeCommande {
  NORMALE(' ', "Commande normale"),
  INTERNE('I', "Commande interne"),
  DIRECT_USINE('D', "Commande direct usine");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeCommande(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeCommande valueOfByCode(Character pCode) {
    for (EnumTypeCommande value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de la commande est invalide : " + pCode);
  }
  
}
