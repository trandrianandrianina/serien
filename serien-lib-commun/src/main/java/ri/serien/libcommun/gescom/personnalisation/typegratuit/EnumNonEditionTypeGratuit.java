/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typegratuit;

import ri.serien.libcommun.outils.MessageErreurException;

/*
 * Enum encadrant le champ "non édition sur bon et/ou facture" dans les types de gratuits
 */
public enum EnumNonEditionTypeGratuit {
  EDITE(' ', "Editer"),
  NON_EDITE_SUR_FACTURE('1', "Non éditer sur les factures"),
  NON_EDITE_SUR_BON_EXPEDITION('2', "Non éditer sur les bons d'expédition"),
  NON_EDITE_SUR_FACTURE_ET_BON_EXPEDITION('3', "Non éditer sur les factures et les bons d'expédition"),
  JAMAIS_EDITE('9', "Jamais éditer");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumNonEditionTypeGratuit(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumNonEditionTypeGratuit valueOfByCode(Character pCode) {
    for (EnumNonEditionTypeGratuit value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException(
        "Dans un paramètre type de gratuit (TG), le champ 'non édition sur bons et factures' est invalide : " + pCode);
  }
}
