/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.stockcomptoir;

import java.math.BigDecimal;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceStock;

/**
 * Liste de stocks.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de stocks.
 * La méthode chargerTout permet d'obtenir la liste de tous les Stocks d'un établissement.
 * 
 * Note: le nom de cette classe est ambigu car si j'ai bien compris il s'agit d'une liste de stock pour un MEME article. Un meilleur nom
 * serait par exemple ListeStockParArticle ou ListeStockUnArticle.
 */
public class ListeStockComptoir extends ListeClasseMetier<IdStockComptoir, StockComptoir, ListeStockComptoir> {
  /**
   * Constructeurs.
   */
  public ListeStockComptoir() {
  }
  
  /**
   * Charger les stocks de base dont les identifiants sont fournis.
   */
  @Override
  public ListeStockComptoir charger(IdSession pIdSession, List<IdStockComptoir> pListeId) {
    if (pListeId == null) {
      throw new MessageErreurException("Impossible de charger le stock car la liste des identifiants est invalide");
    }
    
    return ManagerServiceStock.chargerListeStockComptoir(pIdSession, pListeId);
  }
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  @Override
  public ListeStockComptoir charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charger le stock d'un article pour l'établissement donné.
   */
  public static ListeStockComptoir charger(IdSession pIdSession, CritereStockComptoir pCritereStock) {
    if (pCritereStock == null) {
      throw new MessageErreurException("Impossible de charger le stock car les critères sont invalides");
    }
    
    return ManagerServiceStock.chargerListeStockComptoir(pIdSession, pCritereStock);
  }
  
  /**
   * Charger le stock d'un article pour l'établissement donné.
   * Si aucun établissement n'est transmis on remonte tous les stocks de tous les magasins de tous les établissements
   */
  public static ListeStockComptoir chargerPourArticle(IdSession pIdSession, IdEtablissement pIdEtablissement, String pCodeArticle) {
    if (pCodeArticle == null || pCodeArticle.isEmpty()) {
      throw new MessageErreurException("Impossible de charger le stock car le code article est invalide.");
    }
    
    CritereStockComptoir critereStock = new CritereStockComptoir();
    critereStock.setIdEtablissement(pIdEtablissement);
    critereStock.setCodeArticle(pCodeArticle);
    
    return ManagerServiceStock.chargerListeStockComptoir(pIdSession, critereStock);
  }
  
  /**
   * Quantité disponible à la vente pour un établissement et/ou un magasin en particulier.
   */
  public BigDecimal getQuantiteDisponibleVente(IdEtablissement pIdEtablissement, IdMagasin pIdMagasin) {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock == null) {
        continue;
      }
      if (!Constantes.equals(stock.getId().getIdEtablissement(), pIdEtablissement)) {
        continue;
      }
      if (pIdMagasin != null && !Constantes.equals(stock.getId().getIdMagasin(), pIdMagasin)) {
        continue;
      }
      if (stock.getQuantiteDisponibleVente() != null) {
        quantite = quantite.add(stock.getQuantiteDisponibleVente());
      }
    }
    return quantite;
  }
  
  /**
   * Somme des quantités physiques de tous les stocks de la liste.
   */
  public BigDecimal getQuantitePhysiqueTotale() {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock != null && stock.getQuantitePhysique() != null) {
        quantite = quantite.add(stock.getQuantitePhysique());
      }
    }
    return quantite;
  }
  
  /**
   * Somme des quantités commandées de tous les stocks de la liste.
   */
  public BigDecimal getQuantiteCommandeeTotale() {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock != null && stock.getQuantiteCommandee() != null) {
        quantite = quantite.add(stock.getQuantiteCommandee());
      }
    }
    return quantite;
  }
  
  /**
   * Somme des quantités affectées de tous les stocks de la liste.
   */
  public BigDecimal getQuantiteAffecteeTotale() {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock != null && stock.getQuantiteAffectee() != null) {
        quantite = quantite.add(stock.getQuantiteAffectee());
      }
    }
    return quantite;
  }
  
  /**
   * Somme des quantités attendues de tous les stocks de la liste.
   */
  public BigDecimal getQuantiteAttendueTotale() {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock != null && stock.getQuantiteAttendue() != null) {
        quantite = quantite.add(stock.getQuantiteAttendue());
      }
    }
    return quantite;
  }
  
  /**
   * Somme des quantités disponibles à la vente de tous les stocks de la liste.
   */
  public BigDecimal getQuantiteDisponibleVenteTotale() {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock != null && stock.getQuantiteDisponibleVente() != null) {
        quantite = quantite.add(stock.getQuantiteDisponibleVente());
      }
    }
    return quantite;
  }
  
  /**
   * Somme des quantités disponibles à l'achat de tous les stocks de la liste.
   */
  public BigDecimal getQuantiteDisponibleAchatTotale() {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock != null && stock.getQuantiteDisponibleAchat() != null) {
        quantite = quantite.add(stock.getQuantiteDisponibleAchat());
      }
    }
    return quantite;
  }
  
  /**
   * Somme des quantités minimales de tous les stocks de la liste.
   */
  public BigDecimal getQuantiteMinimaleTotale() {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock != null && stock.getQuantiteMinimum() != null) {
        quantite = quantite.add(stock.getQuantiteMinimum());
      }
    }
    return quantite;
  }
  
  /**
   * Somme des quantités maximales de tous les stocks de la liste.
   */
  public BigDecimal getQuantiteMaximaleTotale() {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock != null && stock.getQuantiteMax() != null) {
        quantite = quantite.add(stock.getQuantiteMax());
      }
    }
    return quantite;
  }
  
  /**
   * Somme des quantités idéales de tous les stocks de la liste.
   */
  public BigDecimal getQuantiteIdealeTotale() {
    BigDecimal quantite = BigDecimal.ZERO;
    for (StockComptoir stock : this) {
      if (stock != null && stock.getQuantiteIdeale() != null) {
        quantite = quantite.add(stock.getQuantiteIdeale());
      }
    }
    return quantite;
  }
  
  /**
   * Recherche si l'article est en surstock dans au moins un établissement/magasin.
   */
  public boolean isArticleEnSurStock() {
    for (StockComptoir stock : this) {
      if (stock.isArticleEnSurStock()) {
        return true;
      }
    }
    return false;
  }
  
}
