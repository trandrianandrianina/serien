/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typestockagemagasin;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Cette personnalisation ne comprend qu'un libellé. Elle défini un type de stockage pour un préparateur/magasinier.
 * Ce type est défini dans la personnalisation MS et est attribuée à un vendeur dans le paramètre VD.
 */
public class TypeStockageMagasin extends AbstractClasseMetier<IdTypeStockageMagasin> {
  // Variables
  private String libelle = "";
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public TypeStockageMagasin(IdTypeStockageMagasin pTypeFacturation) {
    super(pTypeFacturation);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  // -- Accesseurs
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
}
