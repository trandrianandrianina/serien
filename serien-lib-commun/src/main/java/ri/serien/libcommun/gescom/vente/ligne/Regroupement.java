/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import ri.serien.libcommun.outils.MessageErreurException;

public class Regroupement implements Serializable, Cloneable {
  // Variables
  private Character reference = ' ';
  private IdLigneVente idLigneVenteDebut = null;
  private IdLigneVente idLigneVenteFin = null;
  private String texteTitre = null;
  private String texteTotal = null;
  private boolean editionDetailArticles = true;
  private boolean editionPrixArticles = false;
  private BigDecimal montantTotalHT = BigDecimal.ZERO;
  private BigDecimal totalPrixRevient = BigDecimal.ZERO;
  private BigDecimal montantMargeTotal = BigDecimal.ZERO;
  private BigDecimal pourcentageMarge = BigDecimal.ZERO;
  private boolean isEnCoursCreation = false;
  private int numeroLigneTitre = 0;
  private int numeroLignePied = 0;
  
  private ListeLigneVente listeLigneVente = new ListeLigneVente();
  
  /**
   * Constructeur.
   */
  public Regroupement() {
  }
  
  /**
   * Constructeur.
   */
  public Regroupement(Character pReference) {
    reference = pReference;
  }
  
  // -- Méthodes publiques
  
  /**
   * Supprime des lignes d'un regroupement,
   * il retourne le code action (de ActionRegroupement) à effectuer.
   */
  public int supprimerLignes(ListeLigneVente pListeLigneLigneASupprimer) {
    if ((pListeLigneLigneASupprimer == null) || (pListeLigneLigneASupprimer.isEmpty())) {
      return ActionRegroupement.NE_RIEN_FAIRE;
    }
    // Au cas où l'id de la première ligne serait à null on l'initialise
    if (idLigneVenteDebut == null) {
      idLigneVenteDebut = listeLigneVente.get(0).getId();
    }
    
    // On teste si les lignes à supprimer sont l'entête ou le pied
    for (LigneVente ligneVente : pListeLigneLigneASupprimer) {
      // Si on supprime l'entête ou le pied du regroupement alors on supprime le regroupement
      if (ligneVente != null && (isEntete(ligneVente.getId()) || isPied(ligneVente.getId()))) {
        pListeLigneLigneASupprimer.addAll(listeLigneVente);
        listeLigneVente.clear();
        return ActionRegroupement.SUPPRIMER;
      }
    }
    // Sinon
    // On supprime des lignes de la liste du regroupement
    for (LigneVente ligneVente : pListeLigneLigneASupprimer) {
      if (ligneVente != null) {
        listeLigneVente.remove(ligneVente);
      }
    }
    // On analyse ce qui reste comme lignes
    if (listeLigneVente.isEmpty()) {
      return ActionRegroupement.SUPPRIMER;
    }
    // On vérifie qu'il ne reste pas que l'entête et/ou le pied (si c'est le cas on supprime le regroupement)
    else {
      boolean supprimerRegroupement = true;
      for (LigneVente ligneVente : listeLigneVente) {
        if (ligneVente != null && !ligneVente.isLigneCommentaire()) {
          // On a trouvé une ligne autre que commentaire donc on va se contenter d'une mise à jour
          supprimerRegroupement = false;
          idLigneVenteDebut = ligneVente.getId();
          return ActionRegroupement.METTRE_A_JOUR;
        }
      }
      // On n'a pas trouvé d'autres lignes autre que commentaire alors on supprime le regroupement
      if (supprimerRegroupement) {
        return ActionRegroupement.SUPPRIMER;
      }
    }
    return ActionRegroupement.NE_RIEN_FAIRE;
  }
  
  /**
   * Retourne la liste des lignes appartenant au regroupement (sans l'entête et le pied).
   */
  public void retourneListeIdLignesHorsEnteteEtPied(ArrayList<IdLigneVente> pListeLigneVente) {
    if (pListeLigneVente == null) {
      return;
    }
    pListeLigneVente.clear();
    
    // On va ignorer les lignes commentaires de début et fin du regroupement
    int debut = retournerIndicePremiereLigneNonCommentaire();
    int fin = retournerIndiceDerniereLigneNonCommentaire();
    // On ne stocke que les lignes qui vont rester après la suppression du regroupement
    for (int i = debut; i <= fin; i++) {
      pListeLigneVente.add(listeLigneVente.get(i).getId());
    }
  }
  
  /**
   * Charge les lignes du regroupement à partir des indices sur la liste des lignes du documents.
   */
  public void chargerLignesArticles(ListeLigneVente pListeLigneVente, int pIndiceDebut, int pIndiceFin) {
    if (pIndiceDebut < 0 || pIndiceFin >= pListeLigneVente.size() || pIndiceDebut > pIndiceFin) {
      throw new MessageErreurException("Les indices des lignes pour charger le regroupement sont incorrects.");
    }
    
    // Contrôle des numéros de lignes
    // Insertion en tête (contrôle de la ligne de début)
    if (pIndiceDebut == 0 && pListeLigneVente.get(pIndiceDebut).getId().getNumeroLigne().intValue() == 1) {
      throw new MessageErreurException("Aucune ligne ne peut être insérée avant la ligne sélectionnée "
          + pListeLigneVente.get(pIndiceDebut).getId() + " car il n'y a plus de numéro de ligne disponible.");
    }
    // Contrôle de la ligne de début
    if (pIndiceDebut > 0) {
      int numeroPrecedent = pListeLigneVente.get(pIndiceDebut - 1).getId().getNumeroLigne().intValue();
      int numeroSuivant = pListeLigneVente.get(pIndiceDebut).getId().getNumeroLigne().intValue();
      int increment = numeroSuivant - numeroPrecedent;
      if (increment <= 1) {
        throw new MessageErreurException("Aucune ligne ne peut être insérée avant la ligne sélectionnée "
            + pListeLigneVente.get(pIndiceDebut).getId() + " car il n'y a plus de numéro de ligne disponible.");
      }
    }
    // Contrôle de la ligne de fin
    if (pIndiceFin < pListeLigneVente.size() - 1) {
      int numeroPrecedent = pListeLigneVente.get(pIndiceFin).getId().getNumeroLigne().intValue();
      int numeroSuivant = pListeLigneVente.get(pIndiceFin + 1).getId().getNumeroLigne().intValue();
      int increment = numeroSuivant - numeroPrecedent;
      if (increment <= 1) {
        throw new MessageErreurException("Aucune ligne ne peut être insérée après la ligne sélectionnée "
            + pListeLigneVente.get(pIndiceFin).getId() + " car il n'y a plus de numéro de ligne disponible.");
      }
    }
    
    // Copie des lignes du regroupement dans une liste
    listeLigneVente.clear();
    setIdLigneVenteDebut(pListeLigneVente.get(pIndiceDebut).getId());
    setIdLigneVenteFin(pListeLigneVente.get(pIndiceFin).getId());
    for (int i = pIndiceDebut; i <= pIndiceFin; i++) {
      listeLigneVente.add(pListeLigneVente.get(i));
    }
  }
  
  /**
   * Retourne si l'id est celui de l'entête du regroupement.
   */
  public boolean isEntete(IdLigneVente pIdLigneVente) {
    if (pIdLigneVente == null) {
      return false;
    }
    
    LigneVente ligneVente = listeLigneVente.get(0);
    if (pIdLigneVente.equals(ligneVente.getId())) {
      return ligneVente.isLigneCommentaire();
    }
    return false;
  }
  
  /**
   * Retourne si l'id est celui du pied du regroupement.
   */
  public boolean isPied(IdLigneVente pIdLigneVente) {
    if (pIdLigneVente == null) {
      return false;
    }
    
    LigneVente ligneVente = listeLigneVente.get(listeLigneVente.size() - 1);
    if (pIdLigneVente.equals(ligneVente.getId())) {
      return ligneVente.isLigneCommentaire();
    }
    return false;
  }
  
  /**
   * Controle que le numéro de ligne donnée est compris entre les bornes du regroupement.
   */
  public boolean isContient(int pNumeroLigne) {
    if (listeLigneVente.isEmpty()) {
      return false;
    }
    return (getIdLigneVenteDebut().getNumeroLigne().compareTo(pNumeroLigne) < 0)
        && (getIdLigneVenteFin().getNumeroLigne().compareTo(pNumeroLigne) > 0);
  }
  
  /**
   * Retourne le total des prix de revient standard du total des lignes du regroupement.
   */
  public BigDecimal retournerPrixRevientStandard() {
    BigDecimal total = BigDecimal.ZERO;
    for (LigneVente ligneVente : listeLigneVente) {
      if (ligneVente.isLigneCommentaire()) {
        continue;
      }
      total = total.add(ligneVente.getPrixDeRevientLigneHT());
    }
    return total;
  }
  
  /**
   * Retourne le total TTC du total des lignes du regroupement.
   */
  public BigDecimal retournerTotalTTC() {
    BigDecimal total = BigDecimal.ZERO;
    for (LigneVente ligneVente : listeLigneVente) {
      if (ligneVente.isLigneCommentaire()) {
        continue;
      }
      total = total.add(ligneVente.getMontantTTC());
    }
    return total;
  }
  
  /**
   * Retourne le total HT du total des lignes du regroupement.
   */
  public BigDecimal retournerTotalHT() {
    BigDecimal total = BigDecimal.ZERO;
    for (LigneVente ligneVente : listeLigneVente) {
      if (ligneVente.isLigneCommentaire()) {
        continue;
      }
      total = total.add(ligneVente.getMontantHT());
    }
    return total;
  }
  
  /**
   * Vide la liste des lignes articles.
   */
  public void viderListeLignesArticles() {
    listeLigneVente.clear();
  }
  
  /**
   * Comparer 2 regroupements.
   */
  @Override
  public boolean equals(Object regroupementAComparer) {
    // Tester si l'objet est nous-même (optimisation)
    if (regroupementAComparer == this) {
      return true;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(regroupementAComparer instanceof Regroupement)) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    Regroupement regroupement = (Regroupement) regroupementAComparer;
    return reference.equals(regroupement.reference) && listeLigneVente.equals(regroupement.listeLigneVente);
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + reference.hashCode();
    cle = 37 * cle + listeLigneVente.hashCode();
    return cle;
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public Regroupement clone() {
    Regroupement o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (Regroupement) super.clone();
      ArrayList<LigneVente> listeLigneVenteTemp = new ArrayList<LigneVente>(listeLigneVente.size());
      for (LigneVente ligneVente : listeLigneVente) {
        listeLigneVenteTemp.add(ligneVente.clone());
      }
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne l'indice de la ligne commentaire d'entête, -1 sinon.
   */
  private int retournerIndicePremiereLigneNonCommentaire() {
    int debut = 0;
    if (listeLigneVente.get(debut).isLigneCommentaire()) {
      debut = 1;
    }
    return debut;
  }
  
  /**
   * Retourne l'indice de la ligne commentaire de pied, -1 sinon.
   */
  private int retournerIndiceDerniereLigneNonCommentaire() {
    int fin = listeLigneVente.size() - 1;
    if (listeLigneVente.get(fin).isLigneCommentaire()) {
      fin = fin - 1;
    }
    return fin;
  }
  
  // -- Accesseurs
  
  public Character getReference() {
    return reference;
  }
  
  public void setReference(Character reference) {
    this.reference = reference;
  }
  
  public ListeLigneVente getListeLigneVente() {
    return listeLigneVente;
  }
  
  public void setListeLigneVente(ListeLigneVente pListeLigneVente) {
    this.listeLigneVente = pListeLigneVente;
  }
  
  public IdLigneVente getIdLigneVenteDebut() {
    if (idLigneVenteDebut == null) {
      // Si l'id est à null alors on le récupère à l'aide de la liste
      if (!listeLigneVente.isEmpty()) {
        return listeLigneVente.get(0).getId();
      }
    }
    return idLigneVenteDebut;
  }
  
  public void setIdLigneVenteDebut(IdLigneVente pIdLigneVenteDebut) {
    idLigneVenteDebut = pIdLigneVenteDebut;
  }
  
  public IdLigneVente getIdLigneVenteFin() {
    if (idLigneVenteFin == null) {
      // Si l'id est à null alors on le récupère à l'aide de la liste
      if (!listeLigneVente.isEmpty()) {
        return listeLigneVente.get(listeLigneVente.size() - 1).getId();
      }
    }
    return idLigneVenteFin;
  }
  
  public void setIdLigneVenteFin(IdLigneVente pIdLigneVenteFin) {
    idLigneVenteFin = pIdLigneVenteFin;
  }
  
  public void setTexteTitre(String titre) {
    this.texteTitre = titre;
  }
  
  public String getTexteTitre() {
    return texteTitre;
  }
  
  public void setTexteTotal(String total) {
    this.texteTotal = total;
  }
  
  public String getTexteTotal() {
    return texteTotal;
  }
  
  public boolean isEditionDetailArticles() {
    return editionDetailArticles;
  }
  
  public void setEditionDetailArticles(boolean editionDetailArticles) {
    this.editionDetailArticles = editionDetailArticles;
  }
  
  public boolean isEditionPrixArticles() {
    return editionPrixArticles;
  }
  
  public void setEditionPrixArticles(boolean editionPrixArticles) {
    this.editionPrixArticles = editionPrixArticles;
  }
  
  public BigDecimal getMontantTotalHT() {
    return montantTotalHT;
  }
  
  public void setMontantTotalHT(BigDecimal montantTotal) {
    this.montantTotalHT = montantTotal;
  }
  
  public BigDecimal getTotalPrixRevient() {
    return totalPrixRevient;
  }
  
  public void setTotalPrixRevient(BigDecimal totalPrixRevient) {
    this.totalPrixRevient = totalPrixRevient;
  }
  
  public BigDecimal getMontantMargeTotal() {
    return montantMargeTotal;
  }
  
  public void setMontantMargeTotal(BigDecimal montantMargeTotal) {
    this.montantMargeTotal = montantMargeTotal;
  }
  
  public BigDecimal getPourcentageMarge() {
    return pourcentageMarge;
  }
  
  public void setPourcentageMarge(BigDecimal pourcentageMarge) {
    this.pourcentageMarge = pourcentageMarge;
  }
  
  public boolean isEnCoursCreation() {
    return isEnCoursCreation;
  }
  
  public void setEnCoursCreation(boolean isCreation) {
    this.isEnCoursCreation = isCreation;
  }
  
  /**
   * Retourner le numéro de la ligne commentaire d'entête
   */
  public int getNumeroLigneTitre() {
    return numeroLigneTitre;
  }
  
  /**
   * Modifier le numéro de la ligne commentaire d'entête
   */
  public void setNumeroLigneTitre(int numeroLigneTitre) {
    this.numeroLigneTitre = numeroLigneTitre;
  }
  
  /**
   * Retourner le numéro de la ligne commentaire de pied
   */
  public int getNumeroLignePied() {
    return numeroLignePied;
  }
  
  /**
   * Modifier le numéro de la ligne commentaire de pied
   */
  public void setNumeroLignePied(int numeroLignePied) {
    this.numeroLignePied = numeroLignePied;
  }
  
}
