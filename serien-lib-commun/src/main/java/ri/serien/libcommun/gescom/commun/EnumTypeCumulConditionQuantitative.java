/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste l'ensemble des types de cumul possibles pour les conditions de ventes quantitatives.
 */
public enum EnumTypeCumulConditionQuantitative {
  AUCUN(' ', ""),
  QUANTITE('S', "Quantité"),
  QUANTITE_PAR_FAMILLE('M', "Quantité par famille");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeCumulConditionQuantitative(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numero associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  static public EnumTypeCumulConditionQuantitative valueOfByCode(Character pCode) {
    for (EnumTypeCumulConditionQuantitative value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type du cumul pour les conditions de ventes quantitatives est inconnu : " + pCode);
  }
}
