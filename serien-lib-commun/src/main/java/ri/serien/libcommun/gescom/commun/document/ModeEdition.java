/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.document;

import java.io.Serializable;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe contient le détail d'un mode d'édition (visualiser, imprimer, ...)
 */
public class ModeEdition implements Serializable {
  // Variables
  private final EnumModeEdition enumModeEdition;
  private boolean selectionne;
  private boolean actif = true;
  private boolean traite = false;
  
  /**
   * Constructeur.
   */
  private ModeEdition(EnumModeEdition pEnumModeEdition, boolean pSelectionne) {
    enumModeEdition = controlerModeEdition(pEnumModeEdition);
    selectionne = pSelectionne;
  }
  
  // -- Méthodes publiques
  
  /**
   * Créer un mode d'édition.
   */
  public static ModeEdition getInstance(EnumModeEdition pEnumModeEdition, boolean pSelectionne) {
    return new ModeEdition(pEnumModeEdition, pSelectionne);
  }
  
  /**
   * Marque que le mode d'édition a été traité.
   */
  public void marquerCommeTraite() {
    if (actif && selectionne) {
      traite = true;
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du mode d'édition.
   */
  private static EnumModeEdition controlerModeEdition(EnumModeEdition pEnumModeEdition) {
    if (pEnumModeEdition == null) {
      throw new MessageErreurException("Le mode d'édition est invalide.");
    }
    return pEnumModeEdition;
  }
  
  // -- Accesseurs
  
  public EnumModeEdition getEnumModeEdition() {
    return enumModeEdition;
  }
  
  public boolean isSelectionne() {
    return selectionne;
  }
  
  public void setSelectionne(boolean selectionne) {
    this.selectionne = selectionne;
  }
  
  public boolean isActif() {
    return actif;
  }
  
  public void setActif(boolean actif) {
    this.actif = actif;
  }
  
  public boolean isTraite() {
    return traite;
  }
  
  public String getTexte() {
    return enumModeEdition.getLibelle();
  }
}
