/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête d'un document d'achat.
 */
public enum EnumCodeEtatDocument {
  ATTENTE(0, "En attente"),
  VALIDE(1, "Valid\u00e9"),
  RECU(4, "R\u00e9ceptionn\u00e9"),
  RECU_ET_RAPPROCHE_FACTURE(5, "R\u00e9ceptionn\u00e9 rapproch\u00e9 sur facture"),
  FACTURE(6, "Factur\u00e9 en achats"),
  COMPTABILISE(7, "Comptabilis\u00e9 en achats");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumCodeEtatDocument(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumCodeEtatDocument valueOfByCode(Integer pCode) {
    for (EnumCodeEtatDocument value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code état du document d'achat est invalide : " + pCode);
  }
}
