/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.adressedocument;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.MessageErreurException;

public class IdAdresseDocument extends AbstractIdAvecEtablissement {
  
  // Variables
  EnumCodeEnteteAdresseDocument codeAdresse = null;
  IdDocumentVente idDocumentVente = null;
  Integer numeroAdresse = null;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdAdresseDocument(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement,
      EnumCodeEnteteAdresseDocument pCodeAdresse, IdDocumentVente pIdDocument, int pNumero) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    codeAdresse = pCodeAdresse;
    idDocumentVente = IdDocumentVente.controlerId(pIdDocument, true);
    numeroAdresse = pNumero;
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   */
  private IdAdresseDocument(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    codeAdresse = null;
    idDocumentVente = null;
    numeroAdresse = null;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdAdresseDocument getInstance(IdEtablissement pIdEtablissement, EnumCodeEnteteAdresseDocument pCodeAdresse,
      IdDocumentVente pIdDocument, int pNumero) {
    return new IdAdresseDocument(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCodeAdresse, pIdDocument, pNumero);
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getIdEtablissement().hashCode();
    cle = 37 * cle + codeAdresse.hashCode();
    cle = 37 * cle + idDocumentVente.hashCode();
    cle = 37 * cle + numeroAdresse.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdAdresseDocument)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants d'adresses de document.");
    }
    IdAdresseDocument id = (IdAdresseDocument) pObject;
    return getIdEtablissement().equals(id.getIdEtablissement()) && codeAdresse.equals(id.codeAdresse)
        && idDocumentVente.equals(id.idDocumentVente) && numeroAdresse.equals(id.numeroAdresse);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdAdresseDocument)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdAdresseDocument id = (IdAdresseDocument) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = idDocumentVente.compareTo(id.idDocumentVente);
    if (comparaison != 0) {
      return comparaison;
    }
    return numeroAdresse.compareTo(id.numeroAdresse);
  }
  
  @Override
  public String getTexte() {
    return "" + numeroAdresse;
  }
  
  // -- Accesseurs
  
  /**
   * Type cette adresse
   */
  public EnumCodeEnteteAdresseDocument getCodeAdresse() {
    return codeAdresse;
  }
  
  /**
   * Identifiant du document qui contient cette adresse
   */
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  /**
   * Numéro de l'adresse
   * le numéro 0 (zéro) correspond à l'adresse principale. Cette adresse principale ne doit pas être effacée tant que le document existe.
   */
  public Integer getNumeroAdresse() {
    return numeroAdresse;
  }
  
}
