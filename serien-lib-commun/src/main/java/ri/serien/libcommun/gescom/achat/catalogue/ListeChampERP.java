/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.catalogue;

import java.util.ArrayList;
import java.util.ListIterator;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;

/**
 * Liste des différents champs de l'ERP susceptibles d'etre associé à des champs d'un catalogue fournisseur
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de configurations.
 * La méthode chargerTout permet d'obtenir la liste de toutes les zones de l'ERP
 */
public class ListeChampERP extends ArrayList<ChampERP> {
  /**
   * Constructeur.
   */
  public ListeChampERP() {
  }
  
  /**
   * Charger tous les chamsp du catalogue transmis en paramètre
   */
  public static ListeChampERP chargerTout(IdSession pIdSession) {
    return ManagerServiceFournisseur.chargerListeChampERP(pIdSession);
  }
  
  /**
   * Retourner un champ ERP sur la base de son identifiant.
   * Si le champ n'existe pas dans la liste on retourn NULL
   */
  public ChampERP recupererUnChampParId(IdChampERP pIdChampERP) {
    if (pIdChampERP == null) {
      return null;
    }
    
    for (ChampERP champERP : this) {
      if (Constantes.equals(champERP.getId(), pIdChampERP)) {
        return champERP;
      }
    }
    
    return null;
  }
  
  /**
   * Comparer si la liste de champs catalogue correspond à une autre liste de champs catalogue.
   */
  public static boolean equalsComplet(ListeChampERP pListe1, ListeChampERP pListe2) {
    if (pListe1 == pListe2) {
      return true;
    }
    if (pListe1 == null || pListe2 == null) {
      return false;
    }
    
    ListIterator<ChampERP> itr1 = pListe1.listIterator();
    ListIterator<ChampERP> itr2 = pListe2.listIterator();
    while (itr1.hasNext() && itr2.hasNext()) {
      if (!ChampERP.equalsComplet(itr1.next(), itr2.next())) {
        return false;
      }
    }
    return !(itr1.hasNext() || itr2.hasNext());
  }
}
