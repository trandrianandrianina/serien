/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document;

/**
 * Cette classe contient toutes les données qu'il est possible d'utiliser lors du lancement des commandes d'achats afin d'en contrôler
 * l'exécution.
 */
public class ParametreMenuAchat {
  // Variables
  IdDocumentAchat idDocumentAchat = null;
  Integer numeroOnglet = null;
  Boolean fermetureAutomatique = null;
  Integer etapeDocument = null;
  
  // -- Accesseurs
  
  public IdDocumentAchat getIdDocumentAchat() {
    return idDocumentAchat;
  }
  
  public void setIdDocumentAchat(IdDocumentAchat idDocumentAchat) {
    this.idDocumentAchat = idDocumentAchat;
  }
  
  public Integer getNumeroOnglet() {
    return numeroOnglet;
  }
  
  public void setNumeroOnglet(Integer numeroOnglet) {
    this.numeroOnglet = numeroOnglet;
  }
  
  public Boolean getFermetureAutomatique() {
    return fermetureAutomatique;
  }
  
  public void setFermetureAutomatique(Boolean fermetureAutomatique) {
    this.fermetureAutomatique = fermetureAutomatique;
  }
  
  public Integer getEtapeDocument() {
    return etapeDocument;
  }
  
  public void setEtapeDocument(Integer pEtapeDocument) {
    etapeDocument = pEtapeDocument;
  }
  
}
