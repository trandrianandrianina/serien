/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

/**
 * Cette contient un ensemble de critères qui permettent la recherche de lignes de documents de vente.
 */
public class CritereLigneVente implements Serializable {
  // Variables
  private IdEtablissement idEtablissement = null;
  private IdDocumentVente idDocument = null;
  private int numeroDocument = 0;
  private int numeroSuffixe = 0;
  private int numeroLigne = 0;
  private Date dateCreationDebut = null;
  private Date dateCreationFin = null;
  private IdMagasin idMagasin = null;
  private IdClient idClientFacture = null;
  private IdFournisseur idFournisseur = null;
  private Boolean commandeDirectUsine = null;
  private boolean exlusionLignesEnCoursDeCommande = false;
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  // Tous clients/en compte/comptants
  private EnumTypeCompteClient typeClient = null;
  private List<EnumTypeDocumentVente> listeTypeDocumentVente = null;
  private boolean sansHistorique = false;
  // Critères concernant les articles
  private IdArticle idArticle = null;
  // Permet de filtrer les documents contenant des articles spéciaux
  private Boolean contenantArticleSpecial = null;
  // Permet de filtrer les documents contenant des articles hors gammes
  private Boolean contenantArticleHorsGamme = null;
  // Permet de filtrer les documents contenant des articles non gérés en stock
  private Boolean contenantArticleNonStockes = null;
  private Chantier chantier = null;
  // Exclure les remises spéciales
  private Boolean exclureRemiseSpeciale = null;
  private Boolean prixGaranti = null;
  private Boolean ligneSpeciale = null;
  
  // -- Accesseurs
  
  /**
   * Identifiant de l'établissement dans lequel est recherché le client.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Filtre sur l'établissement.
   */
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void setTexteRechercheGenerique(String pTexteRecherche) {
    rechercheGenerique.setTexte(pTexteRecherche);
  }
  
  /**
   * Filtre sur la date de début de la période de création du document
   */
  public Date getDateCreationDebut() {
    return dateCreationDebut;
  }
  
  /**
   * Modifier le filtre sur la date de début de la période de création du document
   */
  public void setDateCreationDebut(Date pDateCreationDebut) {
    dateCreationDebut = pDateCreationDebut;
  }
  
  /**
   * Filtre sur la date de fin de la période de création du document
   */
  public Date getDateCreationFin() {
    return dateCreationFin;
  }
  
  /**
   * Modifier le filtre sur la date de fin de la période de création du document
   */
  public void setDateCreationFin(Date pDateCreationFin) {
    dateCreationFin = pDateCreationFin;
  }
  
  /**
   * Filtre sur l'identifiant du magasin
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Modifier le filtre sur l'identifiant du magasin
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }
  
  public int getNumeroDocument() {
    return numeroDocument;
  }
  
  public void setNumeroDocument(int numeroDocument) {
    this.numeroDocument = numeroDocument;
  }
  
  public int getNumeroSuffixe() {
    return numeroSuffixe;
  }
  
  public void setNumeroSuffixe(int numeroSuffixe) {
    this.numeroSuffixe = numeroSuffixe;
  }
  
  /**
   * Filtre sur le numéro de ligne
   */
  public long getNumeroLigne() {
    return numeroLigne;
  }
  
  /**
   * Modifier le filtre sur le numéro de ligne
   */
  public void setNumeroLigne(int pNumeroLigne) {
    numeroLigne = pNumeroLigne;
  }
  
  public IdClient getIdClientFacture() {
    return idClientFacture;
  }
  
  public void setIdClientFacture(IdClient idClientFacture) {
    this.idClientFacture = idClientFacture;
  }
  
  /**
   * Filtre sur l'identifiant du fournisseur
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Modifier le filtre sur l'identifiant du fournisseur
   */
  public void setIdFournisseur(IdFournisseur pIdFournisseur) {
    idFournisseur = pIdFournisseur;
  }
  
  /**
   * Filtre de sélection des documents direct usine
   */
  public Boolean isCommandeDirectUsine() {
    return commandeDirectUsine;
  }
  
  /**
   * Modifier le filtre de sélection des documents direct usine
   */
  public void setCommandeDirectUsine(Boolean pCommandeDirectUsine) {
    commandeDirectUsine = pCommandeDirectUsine;
  }
  
  public boolean isExlusionLignesEnCoursDeCommande() {
    return exlusionLignesEnCoursDeCommande;
  }
  
  public void setExlusionLignesEnCoursDeCommande(boolean exlusionLignesEnCoursDeCommande) {
    this.exlusionLignesEnCoursDeCommande = exlusionLignesEnCoursDeCommande;
  }
  
  /**
   * Filtre de le type de client
   */
  public EnumTypeCompteClient getTypeClient() {
    return typeClient;
  }
  
  /**
   * Modifier le filtre de le type de client
   */
  public void setTypeClient(EnumTypeCompteClient pTypeClient) {
    typeClient = pTypeClient;
  }
  
  public Boolean isContenantArticleSpecial() {
    return contenantArticleSpecial;
  }
  
  public void setContenantArticleSpecial(Boolean contenantArticleSpecial) {
    this.contenantArticleSpecial = contenantArticleSpecial;
  }
  
  public Boolean isContenantArticleHorsGamme() {
    return contenantArticleHorsGamme;
  }
  
  public void setContenantArticleHorsGamme(Boolean contenantArticleHorsGamme) {
    this.contenantArticleHorsGamme = contenantArticleHorsGamme;
  }
  
  public Boolean isContenantArticleNonStockes() {
    return contenantArticleNonStockes;
  }
  
  public void setContenantArticleNonStockes(Boolean pContenantArticleNonStockes) {
    contenantArticleNonStockes = pContenantArticleNonStockes;
  }
  
  public IdDocumentVente getIdDocument() {
    return idDocument;
  }
  
  public void setIdDocument(IdDocumentVente idDocument) {
    this.idDocument = idDocument;
  }
  
  /**
   * Retourner le type de document de vente dont on veut les lignes de ventes.
   * @return Type de document de vente.
   */
  public List<EnumTypeDocumentVente> getListeTypeDocumentVente() {
    return listeTypeDocumentVente;
  }
  
  /**
   * Modifier le type de document de vente dont on veut les lignes de ventes.
   * @param pTypeDocumentVente Type de document de vente.
   */
  public void ajouterTypeDocumentVente(EnumTypeDocumentVente pTypeDocumentVente) {
    // Contrôle la valeur du type de document
    if (pTypeDocumentVente == null) {
      return;
    }
    // Si le type du document est non défini, il n'est pas ajouté à la liste car cela n'a aucun sens (et cela génère une requête
    // incorrecte)
    if (pTypeDocumentVente == EnumTypeDocumentVente.NON_DEFINI) {
      return;
    }
    
    // Créer la liste si elle est vide
    if (listeTypeDocumentVente == null) {
      listeTypeDocumentVente = new ArrayList<EnumTypeDocumentVente>();
    }
    
    // Ajouter le nouveau type s'il n'y ai pas déjà
    if (!listeTypeDocumentVente.contains(pTypeDocumentVente)) {
      listeTypeDocumentVente.add(pTypeDocumentVente);
    }
  }
  
  /**
   * Retourne true si on ne sélectionne pas les documents de ventes historisés.
   */
  public boolean isSansHistorique() {
    return sansHistorique;
  }
  
  /**
   * Modifier le filtre sur les documents de ventes historisés.
   * Par défaut, les critères de recherche retournent tous les documents de ventes. Activer ce filtre restreint la recherche aux documents
   * de ventes non historisés.
   */
  public void setSansHistorique(boolean pSansHistorique) {
    sansHistorique = pSansHistorique;
  }
  
  /**
   * Article de la ligne
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Article de la ligne
   */
  public void setIdArticle(IdArticle idArticle) {
    this.idArticle = idArticle;
  }
  
  /**
   * Filtre sur le chantier.
   */
  public Chantier getChantier() {
    return chantier;
  }
  
  /**
   * Modifier le filtre sur le chantier.
   */
  public void setChantier(Chantier chantier) {
    this.chantier = chantier;
  }
  
  /**
   * Retourner si l'exclusion des remises spéciales est actif.
   */
  public Boolean getExclureRemiseSpeciale() {
    return exclureRemiseSpeciale;
  }
  
  /**
   * Modifier le filtre sur les remises spéciales.
   * 
   * @param pExclureRemiseSpeciale
   */
  public void setExclureRemiseSpeciale(Boolean pExclureRemiseSpeciale) {
    this.exclureRemiseSpeciale = pExclureRemiseSpeciale;
  }
  
  /**
   * Retourner le critère prix garanti.
   * @return true=prix garanti, false=prix non garanti, null=les deux.
   */
  public Boolean getPrixGaranti() {
    return prixGaranti;
  }
  
  /**
   * Modifier le critère prix garanti.
   * @param pPrixGaranti true=prix garanti, false=prix non garanti, null=les deux.
   */
  public void setPrixGaranti(Boolean pPrixGaranti) {
    prixGaranti = pPrixGaranti;
  }
  
  /**
   * Retourner le critère sur les lignes spéciales.
   * 
   * Les documents de ventes comportent des lignes spéciales identifiées par leur numéro. Cette méthode permet de les exclure ou de les
   * inclure dans leur globalité. Il serait intéressant d'avoir un Enum permettant d'être plus fin dans leur filtrage.
   *
   * Liste des lignes spéciales :
   * - 9501 à 9503 : FRAIS MULTIPLES
   * - 9701 à 9718 : ARTICLES GRATUITS
   * - 9751 à 9790 : ECO-TAXES
   * - 9800 : PORT
   * - 9806 : TPF SPECIALE
   * - 9809 : REMISE AU COL
   * - 9811 à 9829 : 6 REMISES DE BON
   * - 9830 à 9850 : LIGNES COMMENTAIRES SPÉCIALES
   * - 9851 à 9855 : LIGNES DE SURCONDITIONNEMENT
   * - 9860 : FRAIS FIXES POUR MINI COMMANDE NON ATTEINT
   * - 9861 à 9863 : FRAIS FACTURATION
   * - 9900 à 9918 : ALCOOL
   * - 9988 à 9989 : PLURI-TPF
   * - 9990 : TVA NON PERÇUE
   */
  public Boolean getLigneSpeciale() {
    return ligneSpeciale;
  }
  
  /**
   * Retourner le critère sur les lignes spéciales.
   * .
   * 
   * @param pLigneSpeciale
   */
  public void setLigneSpeciale(Boolean pLigneSpeciale) {
    ligneSpeciale = pLigneSpeciale;
  }
}
