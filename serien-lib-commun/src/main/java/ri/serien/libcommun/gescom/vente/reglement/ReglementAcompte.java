/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Cette classe décrit le détail d'un règlement lorsqu'il s'agit d'un acompte.
 * Un acompte (au sens général) peut être réglé avec plusieurs règlements (dont chacun aura une classe reglementAcompte instanciée).
 */
public class ReglementAcompte extends AbstractClasseMetier<IdReglementAcompte> {
  // Variables
  private boolean pris = false;
  private boolean consomme = false;
  
  /**
   * Constructeur.
   */
  public ReglementAcompte(IdReglementAcompte pIdAcompte) {
    super(pIdAcompte);
  }
  
  // -- Méthodes publiques
  
  // -- Méthodes privées
  
  // -- Méthodes surchargées
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public ReglementAcompte clone() {
    ReglementAcompte o = null;
    try {
      o = (ReglementAcompte) super.clone();
      o.setId(id);
    }
    catch (CloneNotSupportedException e) {
    }
    return o;
  }
  
  // -- Accesseurs
  
  public boolean isPris() {
    return pris;
  }
  
  public void setPris(boolean acomptePris) {
    this.pris = acomptePris;
  }
  
  public boolean isConsomme() {
    return consomme;
  }
  
  public void setConsomme(boolean acompteConsomme) {
    this.consomme = acompteConsomme;
  }
  
}
