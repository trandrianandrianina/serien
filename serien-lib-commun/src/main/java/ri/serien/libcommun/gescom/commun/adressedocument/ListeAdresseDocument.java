/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.adressedocument;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * Liste de contacts.
 */
public class ListeAdresseDocument extends ListeClasseMetier<IdAdresseDocument, AdresseDocument, ListeAdresseDocument> {
  /**
   * Constructeur.
   */
  public ListeAdresseDocument() {
  }
  
  /**
   * Charge les AdresseDocuments d'un établissement.
   */
  @Override
  public ListeAdresseDocument charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charge les AdresseDocuments à partir des id.
   */
  @Override
  public ListeAdresseDocument charger(IdSession pIdSession, List<IdAdresseDocument> pListeId) {
    return ManagerServiceDocumentVente.chargerListeAdresseDocument(pIdSession, pListeId);
  }
  
  /**
   * Construire une liste de contacts correspondant à la liste d'identifiants fournie en paramètre.
   * Les données des AdresseDocument ne sont pas chargées à l'exception de l'identifiant.
   */
  public static ListeAdresseDocument creerListeNonChargee(List<IdAdresseDocument> pListeIdAdresseDocument) {
    ListeAdresseDocument listeAdresseDocument = new ListeAdresseDocument();
    if (pListeIdAdresseDocument != null) {
      for (IdAdresseDocument idAdresseDocument : pListeIdAdresseDocument) {
        AdresseDocument AdresseDocument = new AdresseDocument(idAdresseDocument);
        AdresseDocument.setCharge(false);
        listeAdresseDocument.add(AdresseDocument);
      }
    }
    return listeAdresseDocument;
  }
  
}
