/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.typefacturation;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de types de facturations.
 */
public class ListeTypeFacturation extends ListeClasseMetier<IdTypeFacturation, TypeFacturation, ListeTypeFacturation> {
  public static final int NOMBREMAXTYPES = 6;
  
  /**
   * Charger les types de facturation pour les identifiants donnés.
   */
  @Override
  public ListeTypeFacturation charger(IdSession pIdSession, List<IdTypeFacturation> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charger les types de facturation pour l'établissement donné.
   */
  @Override
  public ListeTypeFacturation charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return ManagerServiceParametre.chargerListeTypeFacturation(pIdSession, pIdEtablissement);
  }
  
  /**
   * Retourner le type de facturation par défaut.
   * C'est le premier de la liste.
   */
  public IdTypeFacturation getIdTypeFacturationParDefaut() {
    if (isEmpty()) {
      return null;
    }
    return get(0).getId();
  }
  
  /**
   * Retourner un type de facturation de la liste à partir de son nom.
   */
  public TypeFacturation getTypeFacturationParNom(String pLibelle) {
    if (pLibelle == null) {
      return null;
    }
    for (TypeFacturation typeFacturation : this) {
      if (typeFacturation != null && typeFacturation.getLibelle().equals(pLibelle)) {
        return typeFacturation;
      }
    }
    
    return null;
  }
  
  /**
   * Ajouter le type de facturation sans TVA à la liste.
   * 
   * Le type type de facturation sans TVA n'est pas ajouté à la liste s'il est déjà présent.
   * 
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public void ajouterFacturationSansTVA(IdEtablissement pIdEtablissement) {
    TypeFacturation typeFacturation = TypeFacturation.getInstanceFacturationSansTVA(pIdEtablissement);
    if (contains(typeFacturation)) {
      return;
    }
    add(typeFacturation);
  }
  
  /**
   * Ajouter le type de facturation UE à la liste.
   * 
   * Le type type de facturation UE n'est pas ajouté à la liste s'il est déjà présent.
   * 
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public void ajouterFacturationUE(IdEtablissement pIdEtablissement) {
    TypeFacturation typeFacturation = TypeFacturation.getInstanceFacturationUE(pIdEtablissement);
    if (contains(typeFacturation)) {
      return;
    }
    add(typeFacturation);
  }
}
