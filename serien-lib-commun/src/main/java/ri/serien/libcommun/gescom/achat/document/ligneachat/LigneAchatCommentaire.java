/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.achat.document.ligneachat;

import ri.serien.libcommun.outils.Constantes;

/**
 * Ligne d'achat de type commentaire
 * Une ligne d'achat commentaire est identifiée de la même manière qu'une ligne d'achat classique
 * si ce n'est qu'elle ne contient pas d'article associé mais un texte de commentaire
 * dont la longueur max est déterminée dans TAILLE_MAX_TEXTE
 */
public class LigneAchatCommentaire extends LigneAchat implements Comparable<LigneAchatCommentaire> {
  // Constantes
  public static final int TAILLE_MAX_TEXTE = 120;
  
  private String texte = "";
  
  /**
   * Le constructeur impose la fourniture d'un identifiant.
   */
  public LigneAchatCommentaire(IdLigneAchat pIdLigneAchat) {
    super(pIdLigneAchat);
    typeLigne = EnumTypeLigneAchat.COMMENTAIRE;
  }
  
  // -- Méthodes publiques
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public LigneAchatCommentaire clone() {
    LigneAchatCommentaire o = null;
    // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
    o = (LigneAchatCommentaire) super.clone();
    o.setId(id);
    
    return o;
  }
  
  /**
   * Comparer 2 lignes.
   */
  @Override
  public boolean equals(Object ligneAComparer) {
    // Tester si l'objet est nous-même (optimisation)
    if (ligneAComparer == this) {
      return true;
    }
    
    if (ligneAComparer == null) {
      return false;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(ligneAComparer instanceof LigneAchatCommentaire)) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    LigneAchatCommentaire ligne = (LigneAchatCommentaire) ligneAComparer;
    // On teste le libellé
    boolean textesIdentiques = false;
    if (texte == null && ligne.getTexte() == null) {
      textesIdentiques = true;
    }
    else {
      textesIdentiques = texte.equals(ligne.texte);
    }
    return id.equals(ligne.getId()) && textesIdentiques;
  }
  
  @Override
  public int compareTo(LigneAchatCommentaire pLigneAComparer) {
    return id.getNumeroLigne().compareTo(pLigneAComparer.getId().getNumeroLigne());
  }
  
  /**
   * Retourner le hashcode de l'objet.
   */
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + getIdArticle().hashCode();
    code = 37 * code + texte.hashCode();
    return code;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourner le texte de la ligne d'achat commentaire
   */
  @Override
  public String getTexte() {
    return texte;
  }
  
  /**
   * Mettre à jour le texte de la ligne d'achat de type commentaire
   */
  public void setTexte(String pTexte) {
    texte = Constantes.normerTexte(pTexte);
  }
  
}
