/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.etablissement;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un établissement.
 *
 * L'identifiant d'un établissement est uniquement composé du code établissement.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdEtablissement extends AbstractId {
  // Constantes
  public static final int LONGUEUR_CODE_ETABLISSEMENT = 3;
  public static final String CODE_ETABLISSEMENT_VIDE = "   ";
  
  // Variables
  private final String codeEtablissement;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdEtablissement(EnumEtatObjetMetier pEnumEtatObjetMetier, String pCodeEtablissement) {
    super(pEnumEtatObjetMetier);
    codeEtablissement = controlerEtablissement(pCodeEtablissement);
  }
  
  /**
   * Constructeur privé pour un établissement vide.
   */
  private IdEtablissement() {
    super(EnumEtatObjetMetier.CREE);
    codeEtablissement = CODE_ETABLISSEMENT_VIDE;
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdEtablissement getInstance(String pCodeEtablissement) {
    return new IdEtablissement(EnumEtatObjetMetier.MODIFIE, pCodeEtablissement);
  }
  
  /**
   * Créer un identifiant pour un établissement vide.
   * L'établissement vide (" ") est utilisé pour certains paramètres communs à plsuieurs établissements.
   */
  public static IdEtablissement getInstancePourEtablissementVide() {
    return new IdEtablissement();
  }
  
  /**
   * Contrôler la validité du code établissement.
   * Celui-ci doit faire exactement 3 caractères. Si le code établissement fourni dépasse, une erreur est générée. Si le code
   * établissement fourni est plus petit, il est complété par la droite avec des espaces.
   */
  private static String controlerEtablissement(String pValeur) {
    if (pValeur == null || pValeur.isEmpty()) {
      throw new MessageErreurException("Le code établissement n'est pas renseigné.");
    }
    if (pValeur.length() > LONGUEUR_CODE_ETABLISSEMENT) {
      throw new MessageErreurException("Le code établissement dépasse la taille maximum (3 caractères).");
    }
    return Constantes.formaterStringStrict(pValeur, LONGUEUR_CODE_ETABLISSEMENT).toUpperCase();
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdEtablissement controlerId(IdEtablissement pId, boolean pVerifierExistance) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant de l'établissement est invalide.");
    }
    
    if (pVerifierExistance && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant de l'établissement n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdEtablissement)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants d'établissement.");
    }
    IdEtablissement idEtablissement = (IdEtablissement) pObject;
    return getCodeEtablissement().equals(idEtablissement.getCodeEtablissement());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdEtablissement)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdEtablissement id = (IdEtablissement) pObject;
    return codeEtablissement.compareTo(id.codeEtablissement);
  }
  
  @Override
  public String getTexte() {
    return getCodeEtablissement().trim();
  }
  
  // -- Accesseurs
  
  /**
   * Code établissement.
   * Le code établissement est toujours de 3 caractères alphanumériques. Il est complété de caractères espaces à droite si nécessaire.
   * Il peut être composé de 3 espaces dans les cas où les données ne sont liées à aucun établissement (cas de certains paramètres).
   */
  public String getCodeEtablissement() {
    return codeEtablissement;
  }
  
  /**
   * Tester si cet identifiant correspond à un code d'établissement vide, ce qui signifie l'absence d'établissement.
   */
  public boolean isEtablissementVide() {
    return codeEtablissement.equals(CODE_ETABLISSEMENT_VIDE);
  }
}
