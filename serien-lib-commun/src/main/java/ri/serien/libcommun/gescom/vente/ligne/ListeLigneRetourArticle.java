/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.ligne;

import java.io.Serializable;
import java.util.Date;

/**
 * Cette classe permet de stocker les lignes d'un article déjà acheté par un client, en vue d'un retour d'article.
 * Elle stocke une liste de lignes et les dates de recherche des dites lignes.
 */
public class ListeLigneRetourArticle implements Serializable {
  // Variables
  private ListeLigneVente listeLigneVente = null;
  private Date dateDebut = null;
  private Date dateFin = null;
  
  /**
   * Constructeur.
   */
  public ListeLigneRetourArticle() {
  }
  
  // -- Méthodes publiques
  
  public boolean isEmpty() {
    if (listeLigneVente == null || listeLigneVente.isEmpty()) {
      return true;
    }
    return false;
  }
  
  // -- Accesseurs
  
  /**
   * Renvoit la liste des lignes déjà achetées
   */
  public ListeLigneVente getListeLigneVente() {
    return listeLigneVente;
  }
  
  /**
   * Met à jour la liste des lignes déjà achetées
   */
  public void setListeLigneVente(ListeLigneVente pListeLigneVente) {
    listeLigneVente = pListeLigneVente;
  }
  
  /**
   * Renvoit la date de début de recherche
   */
  public Date getDateDebut() {
    return dateDebut;
  }
  
  /**
   * Met à jour la date de début de recherche
   */
  public void setDateDebut(Date pDateDebut) {
    dateDebut = pDateDebut;
  }
  
  /**
   * Renvoit la date de fin de recherche
   */
  public Date getDateFin() {
    return dateFin;
  }
  
  /**
   * Met à jour la date de fin de recherche
   */
  public void setDateFin(Date pDateFin) {
    dateFin = pDateFin;
  }
  
}
