/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.article;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;

public class Surconditionnement {
  // Constantes
  
  // Variables
  private Article article = null;
  private LigneVente ligneVente = null;
  private StringBuffer libelleQuantiteSaisie = new StringBuffer();
  private StringBuffer libelleQuantiteSuperieure = new StringBuffer();
  private ListeUnite listeUnite = null;
  private String libelleUniteDeSaisie = "";
  private String libelleUniteSurconditionnement = "";
  int nombreDecimalesUC = 0;
  int nombreDecimalesUS = 0;
  private BigDecimal quantiteSaisieEnSurconditionnement = BigDecimal.ZERO;
  private BigDecimal resteEnUnites = BigDecimal.ZERO;
  private BigDecimal quantiteSurconditionnementSuperieur = BigDecimal.ZERO;
  // Le taux minimum attendu entre la quantité saisie par l'utilisateur : valeur1
  // et la première quantité supérieure à valeur1 multiple du conditionnement de l'article
  private static final BigDecimal TAUX_MINI_CHOIX_CONDITIONNEMENT = new BigDecimal(0.9);
  
  // -- Constructeur
  
  public Surconditionnement(Article pArticle, LigneVente pLigneVente) {
    article = pArticle;
    ligneVente = pLigneVente;
  }
  
  // -- Méthodes publiques
  
  public void initialiser(IdSession pIdSession) {
    // Récupération de la liste des unités
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(pIdSession);
    libelleUniteDeSaisie = listeUnite.getLibelleUnite(article.getIdUCV());
    libelleUniteSurconditionnement = listeUnite.getLibelleUnite(article.getIdUCS());
    nombreDecimalesUC = listeUnite.getPrecisionUnite(article.getIdUCV());
    nombreDecimalesUS = listeUnite.getPrecisionUnite(article.getIdUCS());
    quantiteSaisieEnSurconditionnement = calculerQuantiteSaisieEnSurconditionnement();
    resteEnUnites = calculerResteEnUnites();
    quantiteSurconditionnementSuperieur = calculerQuantiteSurconditionnementSuperieur();
    constituerLibelleQuantiteSaisie();
    constituerLibelleQuantiteSuperieure();
  }
  
  public BigDecimal getQuantiteCalculee() {
    return ligneVente.getQuantiteUCV().subtract(resteEnUnites).add(article.getNombreUSParUCS(true));
  }
  
  /**
   * Si l'article a une unité de surconditionnement et que la quantité saisie dépasse la moitié d'un multiple du conditionnement
   */
  public boolean isProposable() {
    return (article.getIdUCS() != null) && (isSurconditionnementProche());
  }
  
  // -- Méthodes privées
  /**
   * Retourne la quantité saisie en nombre de surconditionnements
   * qte saisie divisée par nombre d'UC par US
   */
  private BigDecimal calculerQuantiteSaisieEnSurconditionnement() {
    return ligneVente.getQuantiteUCV().divide(article.getNombreUSParUCS(true), MathContext.DECIMAL32);
  }
  
  /**
   * Retourne le nombre d'unités en plus des surconditionnements complets
   * qte saisie modulo nombre d'UC par US
   */
  private BigDecimal calculerResteEnUnites() {
    return ligneVente.getQuantiteUCV().remainder(article.getNombreUSParUCS(true));
  }
  
  /**
   * Retourne la quantité si l'on prend le conditionnement supérieur
   * qte saisie - (qte saisie modulo nombre d'UC par US) / nombre d'UC par US + 1
   */
  private BigDecimal calculerQuantiteSurconditionnementSuperieur() {
    return (ligneVente.getQuantiteUCV().subtract(resteEnUnites).divide(article.getNombreUSParUCS(true), MathContext.DECIMAL32))
        .add(BigDecimal.ONE);
  }
  
  /**
   * Retourne la quantité au surconditionnement supérieur traduite en unités
   * quantiteSurconditionnementSuperieur * nombre d'UC par US
   */
  private BigDecimal calculerQuantiteSuperieureEnUnites() {
    return quantiteSurconditionnementSuperieur.multiply(article.getNombreUSParUCS(true));
  }
  
  /**
   * Retourne si la quantité saisie par l'utilisateur dépasse la quantité minimum (quantiteMinimum)
   * pour choisir un conditionnement supérieur
   * La quantitité minimum (quantiteMinimum) est calculée à partir du taux tauxMiniChoixConditionnement
   * tauxMiniChoixConditionnement : Le taux minimum attendu entre la quantité saisie par l'utilisateur : valeur1
   * et la première quantité supérieure à valeur1 multiple du conditionnement de l'article
   */
  private boolean isSurconditionnementProche() {
    if (!article.isDefiniNombreUSParUCS()) {
      return false;
    }
    if (Surconditionnement.TAUX_MINI_CHOIX_CONDITIONNEMENT == null
        || Surconditionnement.TAUX_MINI_CHOIX_CONDITIONNEMENT.compareTo(BigDecimal.ONE) > 1) {
      return false;
    }
    BigDecimal quantiteMinimum =
        article.getNombreUSParUCS(true).multiply(Surconditionnement.TAUX_MINI_CHOIX_CONDITIONNEMENT, MathContext.DECIMAL32);
    return calculerResteEnUnites().compareTo(quantiteMinimum) >= 0;
  }
  
  private void constituerLibelleQuantiteSaisie() {
    libelleQuantiteSaisie.append("Conserver la quantité saisie de ");
    libelleQuantiteSaisie.append(Constantes.formater(ligneVente.getQuantiteUCV(), false));
    libelleQuantiteSaisie.append(" " + libelleUniteDeSaisie);
    if (ligneVente.getQuantiteUCV().compareTo(BigDecimal.ONE) > 0) {
      libelleQuantiteSaisie.append("s");
    }
    libelleQuantiteSaisie.append(" (soit ");
    if (quantiteSaisieEnSurconditionnement.compareTo(BigDecimal.ONE) < 0) {
      libelleQuantiteSaisie.append("0");
    }
    else {
      BigDecimal quantite = quantiteSaisieEnSurconditionnement.subtract(BigDecimal.ONE).setScale(nombreDecimalesUS, RoundingMode.HALF_UP);
      libelleQuantiteSaisie.append(Constantes.formater(quantite, false));
    }
    libelleQuantiteSaisie.append(" " + libelleUniteSurconditionnement);
    if (quantiteSaisieEnSurconditionnement.compareTo(BigDecimal.ONE) > 0) {
      libelleQuantiteSaisie.append("s");
    }
    libelleQuantiteSaisie.append(" de ");
    libelleQuantiteSaisie.append(article.getNombreUSParUCS(true).setScale(nombreDecimalesUS, RoundingMode.HALF_UP).toString());
    libelleQuantiteSaisie.append(" " + libelleUniteDeSaisie);
    if (article.getNombreUSParUCS(true).compareTo(BigDecimal.ONE) > 0) {
      libelleQuantiteSaisie.append("s");
    }
    libelleQuantiteSaisie.append(" et ");
    BigDecimal reste = resteEnUnites.setScale(nombreDecimalesUC, RoundingMode.HALF_UP);
    libelleQuantiteSaisie.append(Constantes.formater(reste, false));
    libelleQuantiteSaisie.append(" " + libelleUniteDeSaisie);
    if (resteEnUnites.compareTo(BigDecimal.ONE) > 0) {
      libelleQuantiteSaisie.append("s");
    }
    libelleQuantiteSaisie.append(")");
  }
  
  private void constituerLibelleQuantiteSuperieure() {
    libelleQuantiteSuperieure.append("Choisir la quantité au surconditionnement supérieur (");
    BigDecimal quantite = quantiteSurconditionnementSuperieur.setScale(nombreDecimalesUS, RoundingMode.HALF_UP);
    libelleQuantiteSuperieure.append(Constantes.formater(quantite, false));
    libelleQuantiteSuperieure.append(" " + libelleUniteSurconditionnement);
    if (quantiteSurconditionnementSuperieur.compareTo(BigDecimal.ONE) > 0) {
      libelleQuantiteSuperieure.append("s");
    }
    libelleQuantiteSuperieure.append(" de ");
    libelleQuantiteSuperieure.append(article.getNombreUSParUCS(true).setScale(nombreDecimalesUS, RoundingMode.HALF_UP).toString());
    libelleQuantiteSuperieure.append(" " + libelleUniteDeSaisie);
    if (article.getNombreUSParUCS(true).compareTo(BigDecimal.ONE) > 0) {
      libelleQuantiteSuperieure.append("s");
    }
    libelleQuantiteSuperieure.append(" soit ");
    quantite = calculerQuantiteSuperieureEnUnites().setScale(nombreDecimalesUC, RoundingMode.HALF_UP);
    libelleQuantiteSuperieure.append(Constantes.formater(quantite, false));
    libelleQuantiteSuperieure.append(" " + libelleUniteDeSaisie);
    if (calculerQuantiteSuperieureEnUnites().compareTo(BigDecimal.ONE) > 0) {
      libelleQuantiteSuperieure.append("s");
    }
    libelleQuantiteSuperieure.append(")");
  }
  
  public StringBuffer getLibelleQuantiteSaisie() {
    return libelleQuantiteSaisie;
  }
  
  public StringBuffer getLibelleQuantiteSuperieure() {
    return libelleQuantiteSuperieure;
  }
  
}
