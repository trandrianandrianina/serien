/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Règlement.
 * Un règlement est le paiement entier ou partiel du montant d'un document de vente. Un règlement peut dans certains cas être utilisé
 * pour payer plusieurs documents dans le cas des règlements multiples.
 * En base deux tables sont utilisées pour stocker ces informations :
 * - une pour associer un règlement avec son montant à un document
 * - une pour détailler la liste des documents qui ont été payé avec ce règlement et pour quel montant
 */
public class Reglement extends AbstractClasseMetier<IdReglement> {
  // Constantes
  public static final String PAIEMENT = "Paiement";
  public static final String REMBOURSEMENT = "Remboursement";
  public static final int LONGUEUR_NUMERO_CHEQUE = 7;
  public static final int LONGUEUR_MONTANT = 11;
  
  // Variables
  private Date dateCreation = null;
  private EnumTypeReglement typeReglement = null;
  private boolean modifiable = true;
  private BigDecimal montant = null;
  private String libelle = null;
  private ListeLienReglement listeLienReglement = null;
  private ReglementAcompte reglementAcompte = null;
  // Dans le cas d'un chèque: numéro du chèque sur 7 chiffres
  private String referenceTiree = null;
  private BanqueClient banqueClient = null;
  
  /**
   * Constructeur.
   */
  public Reglement(IdReglement pIdReglement) {
    super(pIdReglement);
  }
  
  // -- Méthodes publiques
  
  /**
   * Contrôle si le mode règlement est en espèces.
   */
  public boolean isEnEspeces() {
    if (typeReglement != null) {
      return typeReglement.equals(EnumTypeReglement.ESPECES);
    }
    return false;
  }
  
  /**
   * Contrôle si le mode règlement est un avoir.
   */
  public boolean isAvecAvoir() {
    if (typeReglement != null) {
      return typeReglement.equals(EnumTypeReglement.AVOIR);
    }
    return false;
  }
  
  /**
   * Retourne le libellé corresponant au sens du règlement : paiement ou remboursement.
   */
  public String retournerLibelleSensReglement() {
    if (montant == null) {
      return null;
    }
    if (montant.signum() >= 0) {
      return PAIEMENT;
    }
    return REMBOURSEMENT;
  }
  
  // -- Méthodes privées
  
  // -- Méthodes surchargées
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  /**
   * Permet la duplication de la classe avec ses informations.
   */
  @Override
  public Reglement clone() {
    Reglement o = null;
    try {
      // On récupère l'instance à renvoyer par l'appel de la méthode super.clone()
      o = (Reglement) super.clone();
      o.setId(id);
      if (listeLienReglement != null) {
        ListeLienReglement listeLienReglementTemp = new ListeLienReglement();
        for (LienReglement lienReglement : listeLienReglement) {
          listeLienReglementTemp.add(lienReglement.clone());
        }
        o.setListeLienReglement(listeLienReglementTemp);
      }
      if (reglementAcompte != null) {
        o.setReglementAcompte(reglementAcompte.clone());
      }
    }
    catch (CloneNotSupportedException cnse) {
    }
    
    return o;
  }
  
  // -- Accesseurs
  
  public BigDecimal getMontant() {
    return montant;
  }
  
  /**
   * Initialisation du montant s'il est modifiable uniquement ou lors du chargement (c'est à dire quand il vaut null).
   */
  public void setMontant(BigDecimal pMontant) {
    if (!modifiable && montant != null) {
      throw new MessageErreurException("Le montant du règlement n'est pas modifiable.");
    }
    if (pMontant != null) {
      montant = pMontant.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
    else {
      montant = pMontant;
    }
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  public ListeLienReglement getListeLienReglement() {
    return listeLienReglement;
  }
  
  public void setListeLienReglement(ListeLienReglement listeLienReglement) {
    this.listeLienReglement = listeLienReglement;
  }
  
  public EnumTypeReglement getTypeReglement() {
    return typeReglement;
  }
  
  public void setTypeReglement(EnumTypeReglement typeReglement) {
    this.typeReglement = typeReglement;
  }
  
  public Date getDateCreation() {
    return dateCreation;
  }
  
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }
  
  public boolean isModifiable() {
    return modifiable;
  }
  
  public void setModifiable(boolean modifiable) {
    this.modifiable = modifiable;
  }
  
  public ReglementAcompte getReglementAcompte() {
    return reglementAcompte;
  }
  
  public void setReglementAcompte(ReglementAcompte acompte) {
    this.reglementAcompte = acompte;
  }
  
  public String getReferenceTiree() {
    return referenceTiree;
  }
  
  public void setReferenceTiree(String referenceTiree) {
    this.referenceTiree = referenceTiree;
  }
  
  public BanqueClient getBanqueClient() {
    return banqueClient;
  }
  
  public void setBanqueClient(BanqueClient banqueClient) {
    this.banqueClient = banqueClient;
  }
  
}
