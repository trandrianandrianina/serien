/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Les différents types d'adresse fournisseur possibles.
 * REPRESENTANT('R', "Repr\u00e9sentant") existait mais il a été décidé de ne plus permettre son utilisation
 */
public enum EnumTypeAdresseFournisseur {
  SIEGE(' ', "Si\u00e8ge"),
  COMMANDE('C', "Commande"),
  EXPEDITION('E', "Exp\u00e9dition"),
  FACTURATION('F', "Facturation");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeAdresseFournisseur(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans uen chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(libelle);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeAdresseFournisseur valueOfByCode(Character pCode) {
    for (EnumTypeAdresseFournisseur value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de l'adresse fournisseur est invalide : " + pCode);
  }
}
