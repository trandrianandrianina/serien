/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.reglement;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe décrit les informations d'un acompte pour un document de vente.
 */
public class Acompte {
  // Variables
  private BigDecimal pourcentage = Constantes.VALEUR_CENT;
  private BigDecimal montant = null;
  private boolean obligatoire = false;
  
  /**
   * Constructeur.
   */
  public Acompte(BigDecimal pPourcentage, BigDecimal pMontantTotalDocument, boolean pObligatoire) {
    setMontant(BigDecimal.ZERO);
    obligatoire = pObligatoire;
    pourcentage = controlerValiditePourcentage(pPourcentage);
    calculerMontantAcompteARegler(pMontantTotalDocument);
  }
  
  /**
   * Constructeur.
   */
  public Acompte(AcompteParametre pAcompteParametre, BigDecimal pMontantTotalDocument) {
    setMontant(BigDecimal.ZERO);
    setAcompteParametre(pAcompteParametre);
    calculerMontantAcompteARegler(pMontantTotalDocument);
  }
  
  // -- Méthodes publiques
  
  /**
   * Met à jour le montant de l'acompte à régler à partir du montant du document de vente.
   */
  public void modifierMontantAcompte(BigDecimal pMontantTotalDocument) {
    calculerMontantAcompteARegler(pMontantTotalDocument);
  }
  
  /**
   * Retourne si un acompte doit être réglé.
   */
  public static boolean isARegler(DocumentVente pDocumentVente) {
    if (pDocumentVente != null && pDocumentVente.isCommande()) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si un acompte est de 100%.
   */
  public static boolean isTotal(Acompte pAcompte) {
    if (pAcompte == null) {
      return false;
    }
    if (pAcompte.getPourcentage().compareTo(Constantes.VALEUR_CENT) == 0) {
      return true;
    }
    return false;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôle la validité du pourcentage d'acompte.
   */
  private BigDecimal controlerValiditePourcentage(BigDecimal pPourcentage) {
    if (pPourcentage == null) {
      throw new MessageErreurException("Le pourcentage de l'acompte est invalide.");
    }
    // On contrôle que le pourcentage soit entre 0 et 100
    if (pPourcentage.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("La valeur du pourcentage de l'acompte est inférieure à 0.");
    }
    else if (pPourcentage.compareTo(Constantes.VALEUR_CENT) > 0) {
      throw new MessageErreurException("La valeur du pourcentage de l'acompte est supérieure à 100.");
    }
    
    // Si le pourcentage vaut 0 et que l'acompte est obligatoire alors on le force 1%
    if (pPourcentage.compareTo(BigDecimal.ZERO) == 0 && obligatoire) {
      pPourcentage = BigDecimal.ONE;
    }
    
    return pPourcentage.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Calcule le montant de l'acompte à partir du montant total du document.
   */
  private void calculerMontantAcompteARegler(BigDecimal pMontantTotalARegler) {
    if (pMontantTotalARegler == null) {
      throw new MessageErreurException("Le montant total à régler de la commande est invalide.");
    }
    BigDecimal montant = pMontantTotalARegler.multiply(pourcentage).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32);
    setMontant(montant);
  }
  
  /**
   * Initialise le montant de l'acompte.
   */
  private void setMontant(BigDecimal pMontant) {
    montant = pMontant;
    if (montant != null) {
      montant = montant.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Met à jour les variables nécesssaires au calcul de l'acompte.
   */
  public void setAcompteParametre(AcompteParametre pAcompteParametre) {
    if (pAcompteParametre == null) {
      throw new MessageErreurException("Les paramètres pour calculer l'acompte sont invalides.");
    }
    obligatoire = pAcompteParametre.isObligatoire();
    pourcentage = controlerValiditePourcentage(pAcompteParametre.getPourcentage());
  }
  
  public BigDecimal getPourcentage() {
    return pourcentage;
  }
  
  /**
   * Montant de l'acompte à régler.
   */
  public BigDecimal getMontant() {
    return montant;
  }
  
  public boolean isObligatoire() {
    return obligatoire;
  }
  
}
