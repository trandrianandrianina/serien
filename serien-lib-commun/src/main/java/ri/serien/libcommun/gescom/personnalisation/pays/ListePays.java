/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.personnalisation.pays;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste des pays
 */
public class ListePays extends ListeClasseMetier<IdPays, Pays, ListePays> {
  
  /**
   * constructeur
   */
  public ListePays() {
  }
  
  /**
   * Charge la liste de pays suivant une liste d'IdPays.
   */
  @Override
  public ListePays charger(IdSession pIdSession, List<IdPays> pListeId) {
    return null;
  }
  
  /**
   * Charge tout les pays suivant l'établissement.
   */
  @Override
  public ListePays charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CriteresPays critere = new CriteresPays();
    critere.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListePays(pIdSession, critere);
  }
  
  /**
   * Retourner l'identifiant d'un pays à partir de son nom.
   */
  public IdPays getIdPaysParNom(String pLibelle) {
    for (Pays pays : this) {
      if (pLibelle.equalsIgnoreCase(pays.getLibelle())) {
        return pays.getId();
      }
    }
    return null;
  }
  
}
