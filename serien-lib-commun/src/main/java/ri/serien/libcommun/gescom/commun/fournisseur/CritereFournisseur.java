/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.commun.fournisseur;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

public class CritereFournisseur extends CriteresBaseRecherche {
  public static final int RECHERCHER_TOUS_FOURNISSEURS_POUR_UN_ETABLISSEMENT = 1;
  public static final int RECHERCHER_ADRESSE_FOURNISSEURS_DOCUMENTACHAT = 2;
  public static final int RECHERCHER_FOURNISSEURS_DOCUMENTACHAT = 3;
  public static final int RECHERCHER_TOUTES_ADRESSE_FOURNISSEURS = 4;
  private int typeRecherche = PAS_DE_CRITERES;
  private IdEtablissement idEtablissement = null;
  private final RechercheGenerique rechercheGenerique = new RechercheGenerique();
  private EnumTypeAdresseFournisseur typeAdresseFournisseur = null;
  private IdFournisseur idFournisseurDebut = null;
  private IdFournisseur idFournisseurFin = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Initialiser les critères de recherche sauf l'établissement.
   * 
   * Cette méthode est principalement appelée lorsque l'utilisateur clique sur le bouton "Initialiser" pour effacer les critères de
   * recherche. Tous les critères de recherche sont effacés sauf l'établissement car on souhaite conserver l'établissement d'une
   * recherche à l'autre.
   */
  public void initialiser() {
    typeRecherche = PAS_DE_CRITERES;
    rechercheGenerique.initialiser();
    typeAdresseFournisseur = null;
    idFournisseurDebut = null;
    idFournisseurFin = null;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Interroger le filtre sur le type de recherche à effectuer.
   */
  @Override
  public int getTypeRecherche() {
    return typeRecherche;
  }
  
  /**
   * Modifier le filtre sur le type de recherche à effectuer.
   */
  @Override
  public void setTypeRecherche(int typeRecherche) {
    this.typeRecherche = typeRecherche;
  }
  
  /**
   * Identifiant de l'établissement dans lequel est recherché le client.
   */
  @Override
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Filtre sur l'établissement.
   */
  @Override
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Modifier le filtre sur l'établissement.
   */
  @Override
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    IdEtablissement.controlerId(pIdEtablissement, true);
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourne le texte de la recherche générique.
   * @return Objet de recheche générique
   */
  public RechercheGenerique getRechercheGenerique() {
    return rechercheGenerique;
  }
  
  /**
   * Retourne le texte de la recherche générique.
   * @return Texte de recherche générique en minuscule.
   */
  public String getTexteRechercheGenerique() {
    return rechercheGenerique.getTexteFinal().toLowerCase();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   * @param pTexteRecherche Texte à rechercher.
   */
  public void setTexteRechercheGenerique(String pTexteRecherche) {
    rechercheGenerique.setTexte(pTexteRecherche);
  }
  
  /**
   * Interroger le filtre sur le type d'adresse fournisseur.
   * @return Type d'adresse du fournisseur.
   */
  public EnumTypeAdresseFournisseur getTypeAdresseFournisseur() {
    return typeAdresseFournisseur;
  }
  
  /**
   * Modifier le filtre sur le type d'adresse fournisseur.
   * @param pTypeAdresseFournisseur Type d'adresse du fournisseur.
   */
  public void setTypeAdresseFournisseur(EnumTypeAdresseFournisseur pTypeAdresseFournisseur) {
    typeAdresseFournisseur = pTypeAdresseFournisseur;
  }
  
  /**
   * Retourner le filtre sur l'id fournisseur de début.<br>
   * Méthode utilisée pour un composant de plage.
   * @return Identifiant du fournisseur de début.
   */
  public IdFournisseur getIdFournisseurDebut() {
    return idFournisseurDebut;
  }
  
  /**
   * Modifier le filtre sur l'id fournisseur de début.<br>
   * Méthode utilisée pour un composant de plage.
   * @param idFournisseur Identifiant du fournisseur de début.
   */
  public void setIdFournisseurDebut(IdFournisseur pIdFournisseurDebut) {
    idFournisseurDebut = pIdFournisseurDebut;
  }
  
  /**
   * Retourner le filtre sur l'id fournisseur de la fin.<br>
   * Méthode utilisée pour un composant de plage.
   * @return Identifiant du fournisseur de fin.
   */
  public IdFournisseur getIdFournisseurFin() {
    return idFournisseurFin;
  }
  
  /**
   * Modifier le filtre sur l'id fournisseur de la fin.<br>
   * Méthode utilisée pour un composant de plage.
   * @param idFournisseur Identifiant du fournisseur de fin.
   */
  public void setIdFournisseurFin(IdFournisseur pIdFournisseurFin) {
    idFournisseurFin = pIdFournisseurFin;
  }
  
}
