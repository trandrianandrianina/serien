/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.gescom.vente.document;

import java.io.Serializable;
import java.util.Date;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class DemandeDeblocage implements Serializable {
  // Constantes
  public static final int LONGUEUR_DATE_CREATION = 7;
  public static final int LONGUEUR_HEURE_CREATION = 6;
  public static final int LONGUEUR_PROFIL = 10;
  public static final int LONGUEUR_REPONSE = 3;
  public static final int LONGUEUR_LIBELLE = 60;

  private static final String ACCEPTEE = "OUI";

  // Variables
  private Date dateCreation = null;
  private String profilDemandeur = "";
  private IdDocumentVente idDocumentVente = null;
  private String profilResponsable = "";
  // Si reponse est vide: pas traitée, sinon OUI ou NON
  private String reponse = "";
  private String motif = "";

  // -- Méthodes publiques

  /**
   * Retourne si la demande a été acceptée.
   */
  public boolean isDemandeAcceptee() {
    reponse = Constantes.normerTexte(reponse);
    return reponse.equalsIgnoreCase(ACCEPTEE);
  }

  /**
   * Retourne la date de création formatée en SAAMMJJ.
   */
  public int retournerDateCreationFormatee() {
    return ConvertDate.dateToDb2(dateCreation);
  }

  /**
   * Retourne l'heure de création formatée en HHMMSS .
   */
  public int retournerHeureCreationFormatee() {
    return ConvertDate.dateToHeureDb2(dateCreation, true);
  }

  // -- Accesseurs

  public Date getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  public String getProfilDemandeur() {
    return profilDemandeur;
  }

  public void setProfilDemandeur(String profilDemandeur) {
    this.profilDemandeur = Constantes.formaterStringMax(profilDemandeur, LONGUEUR_PROFIL, true);
  }

  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }

  public void setIdDocumentVente(IdDocumentVente pIdDocumentVente) {
    this.idDocumentVente = pIdDocumentVente;
  }

  public String getProfilResponsable() {
    return profilResponsable;
  }

  public void setProfilResponsable(String profilResponsable) {
    this.profilResponsable = Constantes.formaterStringMax(profilResponsable, LONGUEUR_PROFIL, true);
  }

  public String getReponse() {
    return reponse;
  }

  public void setReponse(String reponse) {
    this.reponse = Constantes.normerTexte(reponse);
  }

  public String getMotif() {
    return motif;
  }

  public void setMotif(String motif) {
    this.motif = Constantes.formaterStringMax(motif, LONGUEUR_LIBELLE, false);
  }

}
