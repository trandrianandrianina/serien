/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.comptabilite.personnalisation.journal;

import ri.serien.libcommun.commun.recherche.CritereAvecEtablissement;

public class CriteresRechercheJournaux extends CritereAvecEtablissement {
  // Constantes
  public static final int RECHERCHE_STANDARD = 0;
  public static final int RECHERCHE_LIBECRAN = 1;
  
  public static final int FILTRE_AUCUN = 0;
  public static final int FILTRE_TRESORERIE = 1;
  public static final int FILTRE_NON_TRESORERIE = 2;
  public static final int FILTRE_BANQUE = 3;
  public static final int FILTRE_CAISSE = 4;
  public static final int FILTRE_BLANC = 5;
  
  // Variables
  private String codeJournal = "";
  private int typeJournal = Journal.TYPE_ALL;
  private int filtre = 0;
  
  // Accesseurs
  
  public String getCodeJournal() {
    return codeJournal;
  }
  
  public void setCodeJournal(String codeJournal) {
    this.codeJournal = codeJournal;
  }
  
  public int getTypeJournal() {
    return typeJournal;
  }
  
  public void setTypeJournal(int typeJournal) {
    this.typeJournal = typeJournal;
  }
  
  public int getFiltre() {
    return filtre;
  }
  
  public void setFiltre(int filtre) {
    this.filtre = filtre;
  }
}
