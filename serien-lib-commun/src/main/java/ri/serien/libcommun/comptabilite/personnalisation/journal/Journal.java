/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.comptabilite.personnalisation.journal;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

public class Journal extends AbstractClasseMetier<IdJournal> {
  
  // Constantes
  public static final byte TYPE_ALL = 0;
  public static final byte TYPE_NORMAL = 1;
  public static final byte TYPE_PREVISIONNEL = 2;
  public static final byte TYPE_ANALYTIQUE = 3;
  
  // Variables
  private String libelle = null;
  private int type = TYPE_ALL;
  private String tresorerie = "";
  
  /**
   * Constructeur
   */
  public Journal(IdJournal pId) {
    super(pId);
  }
  
  @Override
  public String getTexte() {
    return libelle;
  }
  // Accesseurs
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String pLibelle) {
    libelle = pLibelle;
  }
  
  public int getType() {
    return type;
  }
  
  public void setType(int pType) {
    type = pType;
  }
  
  public String getTresorerie() {
    return tresorerie;
  }
  
  public void setTresorerie(String pTresorerie) {
    tresorerie = pTresorerie;
  }
  
}
