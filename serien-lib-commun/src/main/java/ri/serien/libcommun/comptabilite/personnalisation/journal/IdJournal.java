/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.comptabilite.personnalisation.journal;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un journal.
 *
 * L'identifiant est composé du code établissement et du code journal. Le code journal correspond à la personnalisation JO de la
 * comptabilité. Le code journal est constitué de 12caractère et se présente sous la forme AA (A = caractère alphanumérique obligatoire,
 * N = caractère numérique obligatoire, X caractère alphanumérique facultatif).
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation. *
 */
public class IdJournal extends AbstractIdAvecEtablissement {
  
  private static final int LONGUEUR = 2;
  // Variables
  private final String code;
  
  /**
   * Constructeur privé pour un identifiant complet
   */
  public IdJournal(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, String pCode) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    code = controlerCode(pCode);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdJournal getInstance(IdEtablissement pIdEtablissement, String pCode) {
    return new IdJournal(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCode);
  }
  
  /**
   * Contrôler la validité du code journal.
   * Le code pays est une chaîne de 2 caractères.
   */
  private static String controlerCode(String pCode) {
    if (pCode == null) {
      throw new MessageErreurException("Le code journal n'est pas renseigné.");
    }
    
    String valeur = pCode.trim();
    if (valeur.length() < LONGUEUR && valeur.length() > LONGUEUR) {
      throw new MessageErreurException("Le code journal doit faire " + LONGUEUR + " caractères : " + valeur);
    }
    return valeur;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + code.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdJournal)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux code journal.");
    }
    IdJournal id = (IdJournal) pObject;
    return code.equals(id.code);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdJournal)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdJournal id = (IdJournal) pObject;
    return code.compareTo(id.code);
  }
  
  @Override
  public String getTexte() {
    return code;
  }
  
  // -- Accesseurs
  
  /**
   * Code journal.
   * Le code journal est une chaîne alphanumérique de 2 caractères.
   */
  public String getCode() {
    return code;
  }
}
