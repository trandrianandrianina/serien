/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.comptabilite.personnalisation.journal;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de journaux.
 */
public class ListeJournal extends ListeClasseMetier<IdJournal, Journal, ListeJournal> {
  /**
   * Constructeur.
   */
  public ListeJournal() {
  }
  
  /**
   * Charge la liste de pays suivant une liste d'IdJournal
   */
  @Override
  public ListeJournal charger(IdSession pIdSession, List<IdJournal> pListeId) {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Charge tous les journaux.
   */
  @Override
  public ListeJournal charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    CriteresRechercheJournaux criteres = new CriteresRechercheJournaux();
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeJournal(pIdSession, criteres);
  }
  
  /**
   * Retourne la liste des libellés des journaux.
   */
  public List<String> retournerListeLibelleJournaux(boolean pAjouteValeurVide) {
    List<String> liste = new ArrayList<String>();
    if (pAjouteValeurVide) {
      liste.add("");
    }
    for (Journal journal : this) {
      liste.add(journal.getLibelle());
    }
    return liste;
  }
  
  /**
   * Retourne le code du journal à partir de son indice dans la liste.
   */
  public String retournerCodeJournal(int indice) {
    if (indice >= this.size()) {
      return null;
    }
    return get(indice).getId().toString();
  }
  
  /**
   * Retourne l'indice du journal dans la liste.
   */
  public int retournerIndiceCodeJournal(String aCodeJournal) {
    int indice = 0;
    for (Journal journal : this) {
      if (journal.getId().toString().equalsIgnoreCase(aCodeJournal)) {
        return indice;
      }
      indice++;
    }
    return -1;
  }
  
}
