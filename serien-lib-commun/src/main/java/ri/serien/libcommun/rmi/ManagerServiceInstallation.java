/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.List;

import ri.serien.libcommun.installation.FichierBinaire;
import ri.serien.libcommun.installation.InformationsFichierInstallation;

/**
 * Gestionnaire d'accès au service installation.
 * 
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 * 
 * !! Il ne faut pas modifier les méthodes de cette classe car elles sont utilisées par la procédure d'installation !!
 */
public class ManagerServiceInstallation {
  private static InterfaceServiceInstallation service;
  
  /**
   * Retourne le service ServiceInstallation.
   */
  private static InterfaceServiceInstallation getService() {
    if (service == null) {
      InterfaceServiceInstallation interfaceService =
          (InterfaceServiceInstallation) ManagerClientRMI.recupererService(InterfaceServiceInstallation.NOMSERVICE);
      
      service = (InterfaceServiceInstallation) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceInstallation.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<String> listerDossierServeur(String pDossier) {
    try {
      return getService().listerDossierServeur(pDossier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static InformationsFichierInstallation retournerInformationsFichier(String pNomFichier) {
    try {
      return getService().retournerInformationsFichier(pNomFichier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static FichierBinaire demanderFichier(String pNomFichier) {
    try {
      return getService().demanderFichier(pNomFichier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
