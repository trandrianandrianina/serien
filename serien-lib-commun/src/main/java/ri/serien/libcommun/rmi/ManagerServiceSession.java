/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;

import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.ListeSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.session.EnumTypeSession;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service RMI session.
 *
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les
 * convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un
 * résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceSession {
  private static InterfaceServiceSession service;
  
  /**
   * Retourne le service ServiceInfosServeur.
   */
  private static InterfaceServiceSession getService() {
    if (service == null) {
      InterfaceServiceSession interfaceService =
          (InterfaceServiceSession) ManagerClientRMI.recupererService(InterfaceServiceSession.NOMSERVICE);
      
      service = (InterfaceServiceSession) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceSession.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String getVersion() {
    try {
      return getService().getVersion();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdClientDesktop creerClientDesktop() {
    try {
      return getService().creerClientDesktop();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void detruireClientDesktop(IdClientDesktop pIdClientDesktop) {
    try {
      getService().detruireClientDesktop(pIdClientDesktop);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean connecterClientDesktop(IdClientDesktop pIdClientDesktop, EnvUser pEnvUser) {
    try {
      return getService().connecterClientDesktop(pIdClientDesktop, pEnvUser);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static EnvUser getEnvUser(IdClientDesktop pIdClientDesktop) {
    try {
      return getService().getEnvUser(pIdClientDesktop);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeSousEnvironnement listerSousEnvironnement(IdSession pIdSession) {
    try {
      return getService().listerSousEnvironnement(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static SousEnvironnement recupererSousEnvironnement(IdSession pIdSession, IdSousEnvironnement pIdSousEnvironnement) {
    try {
      return getService().recupererSousEnvironnement(pIdSession, pIdSousEnvironnement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdSession creerSession(IdClientDesktop pIdClientDesktop) {
    try {
      return getService().creerSession(pIdClientDesktop);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void ouvrirSession(IdSession pIdSession, EnumTypeSession pEnumSession, String pMessage) {
    try {
      getService().ouvrirSession(pIdSession, pEnumSession, pMessage);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void fermerSession(IdSession pIdSession) {
    try {
      getService().fermerSession(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void connecterBdd(IdClientDesktop pIdClientDesktop, BDD pBaseDeDonnees) {
    try {
      getService().connecterBdd(pIdClientDesktop, pBaseDeDonnees);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void deconnecterBdd(IdClientDesktop pIdClientDesktop) {
    try {
      getService().deconnecterBdd(pIdClientDesktop);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static BDD recupererBdd(IdClientDesktop pIdClientDesktop) {
    try {
      return getService().recupererBdd(pIdClientDesktop);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static BDD recupererBddParDefaut(IdClientDesktop pIdClientDesktop) {
    try {
      return getService().recupererBddParDefaut(pIdClientDesktop);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static int controlerVersion(IdClientDesktop pIdClientDesktop) {
    try {
      return getService().controlerVersion(pIdClientDesktop);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String getNomDossierRacineServeur() {
    try {
      return getService().getNomDossierRacineServeur();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
