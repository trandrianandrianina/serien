/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.exploitation.dataarea.LdaSerien;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.reapprovisionnement.ReapprovisionnementClient;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.adressedocument.AdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.CritereAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.IdAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.ListeAdresseDocument;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.Negociation;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.ListeLienLigne;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.motifretour.IdMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.IdTypeAvoir;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.avoir.ListeAvoir;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeCarnetBonCour;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DemandeDeblocage;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.OptionsEditionVente;
import ri.serien.libcommun.gescom.vente.document.OptionsValidation;
import ri.serien.libcommun.gescom.vente.document.PrixFlash;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeIdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneRetourArticle;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.Regroupement;
import ri.serien.libcommun.gescom.vente.prodevis.CriteresRechercheProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.DescriptionProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.EnumStatutProDevis;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.gescom.vente.sipe.CritereSipe;
import ri.serien.libcommun.gescom.vente.sipe.EnumTypePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.IdPlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.ListePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.PlanPoseSipe;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service document.
 * 
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les
 * convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un
 * résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceDocumentVente {
  private static InterfaceServiceDocumentVente service;
  
  /**
   * Retourner service dédié à la gestion des documents de vente.
   */
  private static InterfaceServiceDocumentVente getService() {
    if (service == null) {
      InterfaceServiceDocumentVente interfaceService =
          (InterfaceServiceDocumentVente) ManagerClientRMI.recupererService(InterfaceServiceDocumentVente.NOMSERVICE);
      
      service = (InterfaceServiceDocumentVente) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceDocumentVente.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdDocumentVente creerDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement) {
    try {
      return getService().creerDocumentVente(pIdSession, pDocumentVente, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdDocumentVente> chargerListeIdDocumentVente(IdSession pIdSession, CritereDocumentVente pCritereDocumentVente) {
    try {
      return getService().chargerListeIdDocumentVente(pIdSession, pCritereDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeDocumentVenteBase chargerListeDocumentVenteBase(IdSession pIdSession, List<IdDocumentVente> pListeIdDocumentVente) {
    try {
      return getService().chargerListeDocumentVenteBase(pIdSession, pListeIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdDocumentVente> chargerListeIdDocumentAvoir(IdSession pIdSession, IdDocumentVente pIdDocument) {
    try {
      return getService().chargerListeIdDocumentAvoir(pIdSession, pIdDocument);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeDocumentVenteBase chargerListeAvoir(IdSession pIdSession, List<IdDocumentVente> pListeIdDocumentAvoir) {
    try {
      return getService().chargerListeAvoir(pIdSession, pListeIdDocumentAvoir);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdDocumentVente> chargerListeIdFactureNonReglee(IdSession pIdSession, CritereDocumentVente pCritereDocumentVente) {
    try {
      return getService().chargerListeIdFactureNonReglee(pIdSession, pCritereDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static DocumentVente chargerEnteteDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      return getService().chargerEnteteDocumentVente(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<DocumentVente> chargerEnteteListeDocumentVente(IdSession pIdSession, List<IdDocumentVente> pListeIdDocumentVente) {
    try {
      return getService().chargerEnteteListeDocumentVente(pIdSession, pListeIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverEnteteDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement) {
    try {
      getService().sauverEnteteDocumentVente(pIdSession, pDocumentVente, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void controlerDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement) {
    try {
      getService().controlerDocumentVente(pIdSession, pDocumentVente, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static OptionsValidation controlerFinDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente,
      OptionsValidation pOptionsValidation, Date pDateTraitement) {
    try {
      return getService().controlerFinDocumentVente(pIdSession, pDocumentVente, pOptionsValidation, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void chiffrerDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pNePasReactualiserPrix) {
    try {
      getService().chiffrerDocumentVente(pIdSession, pIdDocumentVente, pNePasReactualiserPrix);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static DocumentVente validerDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, OptionsValidation pOptions,
      Date pDateTraitement) {
    try {
      return getService().validerDocumentVente(pIdSession, pDocumentVente, pOptions, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void annulerDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      getService().annulerDocumentVente(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String editerDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pDocumentModifiable,
      Date pDateTraitement, OptionsEditionVente pOptions, LdaSerien pLdaSerien) {
    try {
      return getService().editerDocumentVente(pIdSession, pIdDocumentVente, pDocumentModifiable, pDateTraitement, pOptions, pLdaSerien);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static LigneVente initialiserLigneVente(IdSession pIdSession, IdLigneVente pIdLigneVente, IdArticle pIdArticle,
      Date pDateTraitement) {
    try {
      return getService().initialiserLigneVente(pIdSession, pIdLigneVente, pIdArticle, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static AlertesLigneDocument controlerLigneDocumentVente(IdSession pIdSession, LigneVente pLigneVente, Date pDateTraitementp) {
    try {
      return getService().controlerLigneDocumentVente(pIdSession, pLigneVente, pDateTraitementp);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverLigneVente(IdSession pIdSession, LigneVente pLigneVente) {
    try {
      getService().sauverLigneVente(pIdSession, pLigneVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean modifierLignedocumentVente(IdSession pIdSession, LigneVente pLigneVente) {
    try {
      return getService().modifierLigneDocumentVente(pIdSession, pLigneVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean supprimerLigneDocumentVente(IdSession pIdSession, LigneVente pLigneVente) {
    try {
      return getService().supprimerLigneDocumentVente(pIdSession, pLigneVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeIdLigneVente chargerListeIdLigneVente(IdSession pIdSession, CritereLigneVente pCriteres) {
    try {
      return getService().chargerListeIdLigneVente(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdLigneVente> chargerIdLigneVenteParCodeArticle(IdSession pIdSession, CritereLigneVente pCriteres) {
    try {
      return getService().chargerIdLigneVenteParCodeArticle(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static BlocNote chargerPetitBlocNoteDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      return getService().chargerPetitBlocNoteDocumentVente(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static int sauverPetitBlocNoteDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, BlocNote pBlocNote) {
    try {
      return getService().sauverPetitBlocNoteDocumentVente(pIdSession, pIdDocumentVente, pBlocNote);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLigneVente chargerListeLigneVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente,
      CritereLigneVente pCriteres, boolean isTTC) {
    try {
      return getService().chargerListeLigneVente(pIdSession, pIdDocumentVente, pCriteres, isTTC);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLigneVenteBase chargerListeLigneVenteBase(IdSession pIdSession, List<IdLigneVente> pListeIdLigneVente) {
    try {
      return getService().chargerListeLigneVenteBase(pIdSession, pListeIdLigneVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Regroupement chargerRegroupement(IdSession pIdSession, IdLigneVente pIdLigneVente, Regroupement pRegroupement) {
    try {
      return getService().chargerRegroupement(pIdSession, pIdLigneVente, pRegroupement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverRegroupement(IdSession pIdSession, Regroupement pRegroupement) {
    try {
      getService().sauverRegroupement(pIdSession, pRegroupement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerRegroupement(IdSession pIdSession, IdLigneVente pIdLigneVente) {
    try {
      getService().supprimerRegroupement(pIdSession, pIdLigneVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverAdresseDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, Adresse pAdresseFacturation,
      Adresse pAdresseLivraison) {
    try {
      getService().sauverAdresseDocumentVente(pIdSession, pIdDocumentVente, pAdresseFacturation, pAdresseLivraison);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdDocumentVente dupliquerDocument(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pAvecRecalculPrix) {
    try {
      return getService().dupliquerDocument(pIdSession, pIdDocumentVente, pAvecRecalculPrix);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Créer un avoir (par duplication d'un bon ou d'une facture). Renvoi l'identifiant de l'avoir créé.
   */
  public static IdDocumentVente creerAvoir(IdSession pIdSession, IdDocumentVente pIdDocumentVente, IdTypeAvoir pIdTypeAvoir) {
    try {
      return getService().creerAvoir(pIdSession, pIdDocumentVente, pIdTypeAvoir);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdDocumentVente dupliquerDocumentEtatSuivant(IdSession pIdSession, IdDocumentVente pIdDocumentVente,
      boolean pAvecRecalculPrix) {
    try {
      return getService().dupliquerDocumentEtatSuivant(pIdSession, pIdDocumentVente, pAvecRecalculPrix);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeReglement chargerListeReglement(IdSession pIdSession, IdDocumentVente pIdDocumentVente, Date pDateTraitement) {
    try {
      return getService().chargerListeReglement(pIdSession, pIdDocumentVente, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverListeReglement(IdSession pIdSession, IdDocumentVente pIdDocumentVente, ListeReglement pListeReglements) {
    try {
      getService().sauverListeReglement(pIdSession, pIdDocumentVente, pListeReglements);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverDocumentVenteReglementMultiple(IdSession pIdSession, boolean pViderListe, IdDocumentVente pIdDocumentVente) {
    try {
      getService().sauverDocumentVenteReglementMultiple(pIdSession, pViderListe, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerReglement(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      getService().supprimerReglement(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void solderReliquat(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      getService().solderReliquat(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static PrixFlash chargerPrixFlash(IdSession pIdSession, IdLigneVente pIdLigneVente, IdArticle pIdArticle, IdClient pIdClient) {
    try {
      return getService().chargerPrixFlash(pIdSession, pIdLigneVente, pIdArticle, pIdClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverDocumentVenteClientModifie(IdSession pIdSession, IdDocumentVente pIdDocumentVente, IdClient pIdClient,
      boolean pChangerClientFacture, boolean pChangerClientLivre) {
    try {
      getService().sauverDocumentVenteClientModifie(pIdSession, pIdDocumentVente, pIdClient, pChangerClientFacture, pChangerClientLivre);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static DemandeDeblocage chargerDemandeDeblocage(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      return getService().chargerDemandeDeblocage(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverDemandeDeblocage(IdSession pIdSession, IdDocumentVente pIdDocumentVente, String pProfil) {
    try {
      getService().sauverDemandeDeblocage(pIdSession, pIdDocumentVente, pProfil);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerDemandeDeblocage(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      getService().supprimerDemandeDeblocage(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static DemandeDeblocage chargerDemandeDeblocageVenteSousPrv(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      return getService().chargerDemandeDeblocageVenteSousPrv(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverDemandeDeblocageVenteSousPrv(IdSession pIdSession, IdDocumentVente pIdDocumentVente, String pProfil) {
    try {
      getService().sauverDemandeDeblocageVenteSousPrv(pIdSession, pIdDocumentVente, pProfil);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerDemandeDeblocageVenteSousPrv(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      getService().supprimerDemandeDeblocageVenteSousPrv(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static int sauverLigneVenteDocumentAvecExtraction(IdSession pIdSession, IdLigneVente pIdLigneVenteOrigine,
      IdLigneVente pIdLigneVenteDestination, int pTypeTraitement, BigDecimal pQuantiteDemandee, boolean pAvecRecalculPrix) {
    try {
      return getService().sauverLigneVenteDocumentAvecExtraction(pIdSession, pIdLigneVenteOrigine, pIdLigneVenteDestination,
          pTypeTraitement, pQuantiteDemandee, pAvecRecalculPrix);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void validerExtractionDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement,
      OptionsValidation pOptions) {
    try {
      getService().validerExtractionDocumentVente(pIdSession, pDocumentVente, pDateTraitement, pOptions);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static int sauverLigneVenteCommentaire(IdSession pIdSession, LigneVente pLigneVente, DocumentVente pDocumentVente,
      boolean pIsEditionSurFactures, boolean pConserverSurAchat) {
    try {
      return getService().sauverLigneVenteCommentaire(pIdSession, pLigneVente, pDocumentVente, pIsEditionSurFactures, pConserverSurAchat);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static LigneVente chargerLigneVenteCommentaire(IdSession pIdSession, IdLigneVente pIdLigneVente) {
    try {
      return getService().chargerLigneVenteCommentaire(pIdSession, pIdLigneVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLigneRetourArticle chargerListeLigneRetourArticle(IdSession pIdSession, BigDecimal pQuantite, Article pArticle,
      Client pClient, ListeLigneRetourArticle pListeOrigine) {
    try {
      return getService().chargerListeLigneRetourArticle(pIdSession, pQuantite, pArticle, pClient, pListeOrigine);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverLigneRetourArticle(IdSession pIdSession, BigDecimal pQuantiteRetour, BigDecimal pPrixNetRetour,
      IdMotifRetour pMotifRetour, LigneVente pLigneVente, DocumentVente pDocument) {
    try {
      getService().sauverLigneRetourArticle(pIdSession, pQuantiteRetour, pPrixNetRetour, pMotifRetour, pLigneVente, pDocument);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void initialiserPrixClient(IdSession pIdSession, Client pClient) {
    try {
      getService().initialiserPrixClient(pIdSession, pClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<DescriptionProDevis> chargerListeDescriptionProDevis(IdSession pIdSession, CriteresRechercheProDevis pCriteres) {
    try {
      return getService().chargerListeDescriptionProDevis(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdDocumentVente importerDevisProDevis(IdSession pIdSession, DescriptionProDevis pDescription, IdClient pIdClient) {
    try {
      return getService().importerDevisProDevis(pIdSession, pDescription, pIdClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void modifierStatutProDevis(IdSession pIdSession, DescriptionProDevis pDescription, EnumStatutProDevis pNouveauStatut) {
    try {
      getService().modifierStatutProDevis(pIdSession, pDescription, pNouveauStatut);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Negociation chargerNegociation(IdSession pIdSession, LigneVente pLigneVente, Date pDateTraitement,
      String pOptionTraitement) {
    try {
      return getService().chargerNegociation(pIdSession, pLigneVente, pDateTraitement, pOptionTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverNegociation(IdSession pIdSession, LigneVente pLigneVente, Date pDateTraitement, String pOptionTraitement) {
    try {
      getService().sauverNegociation(pIdSession, pLigneVente, pDateTraitement, pOptionTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ConditionAchat chargerConditionAchat(IdSession pIdSession, LigneVente pLigneVente) {
    try {
      return getService().chargerConditionAchat(pIdSession, pLigneVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ConditionAchat chargerNegociationAchat(IdSession pIdSession, LigneAchat pLigneAchat) {
    try {
      return getService().chargerNegociationAchat(pIdSession, pLigneAchat);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeAvoir chargerListeAvoir(IdSession pIdSession, IdClient pIdClient) {
    try {
      return getService().chargerListeAvoir(pIdSession, pIdClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<ReapprovisionnementClient> chargerListeArticlesAApprovisionner(IdSession pIdSession,
      List<IdLigneVente> pListeIdLigneVente, ParametresLireArticle pParametres) {
    try {
      return getService().chargerListeArticlesAApprovisionner(pIdSession, pListeIdLigneVente, pParametres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdLigneVente> chargerListeIdLigneVenteAApprovisionner(IdSession pIdSession, CritereLigneVente pCriteres) {
    try {
      return getService().chargerListeIdLigneVenteAApprovisionner(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLienLigne chargerListeLienLigne(IdSession pIdSession, IdLienLigne pIdLigneOrigine) {
    try {
      return getService().chargerListeLienLigne(pIdSession, pIdLigneOrigine);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<ListeLienLigne> chargerListeLienLigne(IdSession pIdSession, ListeLigneVenteBase pListeLigneVenteBase) {
    try {
      return getService().chargerListeLienLigne(pIdSession, pListeLigneVenteBase);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Boolean existeArticleDirectUsine(IdSession pIdSession, ListeLigneVente plisteLigneVente) {
    try {
      return getService().existeArticleDirectUsine(pIdSession, plisteLigneVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void genererCNVDepuisDocument(IdSession pIdSession, IdLigneVente pIdLigneVente, Date pDateDebut, Date pDateFin,
      String pReference) {
    try {
      getService().genererCNVDepuisDocument(pIdSession, pIdLigneVente, pDateDebut, pDateFin, pReference);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void changerClientDocument(IdSession pIdSession, IdDocumentVente pIdDocumentVente, IdClient pIdClientOrigine,
      boolean isAvecRecalcul) {
    try {
      getService().changerClientDocument(pIdSession, pIdDocumentVente, pIdClientOrigine, isAvecRecalcul);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static CarnetBonCour sauverCarnetBonCour(IdSession pIdSession, CarnetBonCour pCarnetBonCour) {
    try {
      return getService().sauverCarnetBonCour(pIdSession, pCarnetBonCour);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdCarnetBonCour> chargerListeIdCarnetBonCour(IdSession pIdSession, CritereCarnetBonCour pCritereCarnetBonCour) {
    try {
      return getService().chargerListeIdCarnetBonCour(pIdSession, pCritereCarnetBonCour);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdCarnetBonCour> chargerListeIdCarnetBonCour(IdSession pIdSession, CritereBonCour critereBonCour) {
    try {
      return getService().chargerListeIdCarnetBonCour(pIdSession, critereBonCour);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdBonCour> chargerListeIdBonCour(IdSession pIdSession, CritereBonCour critereBonCour) {
    try {
      return getService().chargerListeIdBonCour(pIdSession, critereBonCour);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeCarnetBonCour chargerListeCarnetBonCour(IdSession pIdSession, List<IdCarnetBonCour> pListeIdCarnetBonCour) {
    try {
      return getService().chargerListeCarnetBonCour(pIdSession, pListeIdCarnetBonCour);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static CarnetBonCour chargerCarnetBonCour(IdSession pIdSession, IdCarnetBonCour pIdCarnetBonCour) {
    try {
      return getService().chargerCarnetBonCour(pIdSession, pIdCarnetBonCour);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeBonCour chargerListeBonCour(IdSession pIdSession, List<IdBonCour> pListeIdBonCour) {
    try {
      return getService().chargerListeBonCour(pIdSession, pListeIdBonCour);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void modifierBonCour(IdSession pIdSession, BonCour pBonCour) {
    try {
      getService().modifierBonCour(pIdSession, pBonCour);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static BonCour chargerBonCour(IdSession pIdSession, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, int pNumeroBonCour) {
    try {
      return getService().chargerBonCour(pIdSession, pIdEtablissement, pIdMagasin, pNumeroBonCour);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdAdresseDocument> chargerListeIdAdresseDocument(IdSession pIdSession, CritereAdresseDocument pCritere) {
    try {
      return getService().chargerListeIdAdresseDocument(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeAdresseDocument chargerListeAdresseDocument(IdSession pIdSession, List<IdAdresseDocument> pListeIdAdresseDocument) {
    try {
      return getService().chargerListeAdresseDocument(pIdSession, pListeIdAdresseDocument);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static int chercherPremierNumeroAdresseLibre(IdSession pIdSession, IdDocumentVente pIdDocument) {
    try {
      return getService().chercherPremierNumeroAdresseLibre(pIdSession, pIdDocument);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void creerAdresseDocument(IdSession pIdSession, AdresseDocument pAdresseDocument) {
    try {
      getService().creerAdresseDocument(pIdSession, pAdresseDocument);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerAdresseDocument(IdSession pIdSession, AdresseDocument pAdresseDocument) {
    try {
      getService().supprimerAdresseDocument(pIdSession, pAdresseDocument);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverAdresseDocument(IdSession pIdSession, AdresseDocument pAdresseDocument) {
    try {
      getService().sauverAdresseDocument(pIdSession, pAdresseDocument);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdPlanPoseSipe> chargerListeIdPlanPoseSipe(IdSession pIdSession, CritereSipe pCritereSipe,
      boolean pMiseAJourDepuisIfs) {
    try {
      return getService().chargerListeIdPlanPoseSipe(pIdSession, pCritereSipe, pMiseAJourDepuisIfs);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListePlanPoseSipe chargerListePlanPoseSipe(IdSession pIdSession, List<IdPlanPoseSipe> pListePlanPoseSipe) {
    try {
      return getService().chargerListePlanPoseSipe(pIdSession, pListePlanPoseSipe);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void importerPlanPoseSipe(IdSession pIdSession, PlanPoseSipe pPlanPoseSipe, IdDocumentVente pIdDocument) {
    try {
      getService().importerPlanPoseSipe(pIdSession, pPlanPoseSipe, pIdDocument);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String retournerDossierSipe(IdSession pIdSession, EnumTypePlanPoseSipe pTypePlanPoseSipe, boolean pDossierSauvegarde) {
    try {
      return getService().retournerDossierSipe(pIdSession, pTypePlanPoseSipe, pDossierSauvegarde);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerFichierSipe(IdSession pIdSession, PlanPoseSipe pPlanPoseSipe) {
    try {
      getService().supprimerFichierSipe(pIdSession, pPlanPoseSipe);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
