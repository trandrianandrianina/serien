/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.exploitation.dataarea.LdaSerien;
import ri.serien.libcommun.gescom.achat.document.CritereDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumOptionEdition;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ListeReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.OptionsEditionAchat;
import ri.serien.libcommun.gescom.achat.document.ValeurInitialeCommandeAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchatBase;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceDocumentAchat extends Remote {
  public static final String NOMSERVICE = "ServiceDocumentAchat";
  
  public boolean ping() throws RemoteException;
  
  public List<IdDocumentAchat> chargerListeIdDocumentAchat(IdSession pIdSession, CritereDocumentAchat pCriteres) throws RemoteException;
  
  public ListeDocumentAchat chargerListeDocumentAchat(IdSession pIdSession, List<IdDocumentAchat> pListeIdDocumentAchat)
      throws RemoteException;
  
  public DocumentAchat sauverDocumentAchat(IdSession pIdSession, String pUtilisateur, DocumentAchat pDocumentAchat)
      throws RemoteException;
  
  public DocumentAchat chargerDocumentAchat(IdSession pIdSession, IdDocumentAchat pIdDocument, DocumentAchat pDocument,
      boolean avecHistorique) throws RemoteException;
  
  public LigneAchat initialiserLigneDocument(IdSession pIdSession, LigneAchat pLigneAchat, Date pDateTraitement) throws RemoteException;
  
  public LigneAchat controlerLigneDocument(IdSession pIdSession, LigneAchat pLigneAchat, Date pDateTraitement) throws RemoteException;
  
  public void sauverLigneAchat(IdSession pIdSession, LigneAchat pLigneAchat, Date pDateTraitement) throws RemoteException;
  
  public ListeLigneAchat chargerListeLigneAchat(IdSession pIdSession, CritereLigneAchat pCriteres) throws RemoteException;
  
  public List<IdLigneAchat> chargerListeIdLignesAchat(IdSession pIdSession, CritereLigneAchat pCriteres) throws RemoteException;
  
  public ListeReceptionAchat chargerListeReceptionAchat(IdSession pIdSession, List<IdLigneAchat> listeIdLignes) throws RemoteException;
  
  public void supprimerListeLigneDocument(IdSession pIdSession, List<LigneAchat> pListeLigne) throws RemoteException;
  
  public void validerDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement, EnumOptionEdition pOptionEdition)
      throws RemoteException;
  
  public void annulerDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement) throws RemoteException;
  
  public void mettreEnAttenteDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement) throws RemoteException;
  
  public String editerDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement,
      OptionsEditionAchat pOptionEdition, LdaSerien pLdaSerien) throws RemoteException;
  
  public ListeDocumentAchat chargerListeDocumentAchatAvecLigne(IdSession pIdSession, List<IdLigneAchat> pListeId) throws RemoteException;
  
  public ListeLigneAchatBase chargerListeLigneAchatBase(IdSession pIdSession, List<IdLigneAchat> pListeId) throws RemoteException;
  
  public ListeDocumentAchat recupererCommandeAchatliee(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException;
  
  public ValeurInitialeCommandeAchat chargerDonneesCommandeInitiale(IdSession pIdSession, IdDocumentAchat pIdDocumentAchat)
      throws RemoteException;
  
  public ListeLigneAchat chargerListeLigneAchat(IdSession pIdSession, List<IdLigneAchat> pListeIdLigneAchat) throws RemoteException;
}
