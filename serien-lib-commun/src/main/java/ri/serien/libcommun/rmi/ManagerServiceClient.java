/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.List;

import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.CritereClient;
import ri.serien.libcommun.gescom.commun.client.CriteresRechercheDocumentsHistoriqueVentesClientArticle;
import ri.serien.libcommun.gescom.commun.client.DocumentHistoriqueVenteClientArticle;
import ri.serien.libcommun.gescom.commun.client.EncoursClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeIdClient;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.chantier.CritereChantier;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.chantier.ListeChantier;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.reglement.CritereBanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.ListeBanqueClient;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service client.
 *
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceClient {
  private static InterfaceServiceClient service;
  
  /**
   * Retourner service dédié à la gestion des clients.
   */
  private static InterfaceServiceClient getService() {
    if (service == null) {
      InterfaceServiceClient interfaceService =
          (InterfaceServiceClient) ManagerClientRMI.recupererService(InterfaceServiceClient.NOMSERVICE);
      
      service = (InterfaceServiceClient) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceClient.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeClientBase chargerListeClientBase(IdSession pIdSession, CritereClient pCriteres) {
    try {
      return getService().chargerListeClientBase(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeClientBase chargerListeClientBase(IdSession pIdSession, ListeIdClient pListeIdClient) {
    try {
      return getService().chargerListeClientBase(pIdSession, pListeIdClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Client chargerClient(IdSession pIdSession, IdClient pIdClient) {
    try {
      return getService().chargerClient(pIdSession, pIdClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdChantier> chargerListeIdChantier(IdSession pIdSession, CritereChantier pCriteres) {
    try {
      return getService().chargerListeIdChantier(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeChantier chargerListeChantier(IdSession pIdSession, List<IdChantier> pListeIdChantier) {
    try {
      return getService().chargerListeChantier(pIdSession, pListeIdChantier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Chantier chargerChantier(IdSession pIdSession, IdChantier pIdChantier) {
    try {
      return getService().chargerChantier(pIdSession, pIdChantier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdLigneAchat chargerLienLigneVenteLigneAchat(IdSession pIdSession, IdLigneVente pIdLigneVente) {
    try {
      return getService().chargerLienLigneVenteLigneAchat(pIdSession, pIdLigneVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static EncoursClient chargerEncoursClient(IdSession pIdSession, IdClient pIdClient) {
    try {
      return getService().chargerEncoursClient(pIdSession, pIdClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static EncoursClient controlerEncoursClient(IdSession pIdSession, DocumentVente pDocumentVente, int pCollectifComptableClient) {
    try {
      return getService().controlerEncoursClient(pIdSession, pDocumentVente, pCollectifComptableClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeContact chargerListeContact(IdSession pIdSession, IdClient pIdClient) {
    try {
      return getService().chargerListeContact(pIdSession, pIdClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Client sauverClientComptant(IdSession pIdSession, Client pClient) {
    try {
      return getService().sauverClientComptant(pIdSession, pClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverClient(IdSession pIdSession, Client pClient) {
    try {
      getService().sauverClient(pIdSession, pClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<DocumentHistoriqueVenteClientArticle> chargerDocumentHistoriqueVenteClientArticle(IdSession pIdSession,
      IdClient pIdClient, IdArticle pIdArticle, CriteresRechercheDocumentsHistoriqueVentesClientArticle pCriteres) {
    try {
      return getService().chargerDocumentHistoriqueVenteClientArticle(pIdSession, pIdClient, pIdArticle, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeBanqueClient chargerListeBanqueClient(IdSession pIdSession, CritereBanqueClient pCriteres) {
    try {
      return getService().chargerListeBanqueClient(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
