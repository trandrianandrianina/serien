/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.math.BigDecimal;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.CritereArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.CritereArticleLie;
import ri.serien.libcommun.gescom.commun.article.CriteresRechercheArticles;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libcommun.gescom.commun.article.ListeGroupesArticles;
import ri.serien.libcommun.gescom.commun.article.ListeMarques;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.CritereResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.IdResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.ListeResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette.ListeResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.lot.CritereLot;
import ri.serien.libcommun.gescom.vente.lot.IdLot;
import ri.serien.libcommun.gescom.vente.lot.ListeLot;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceArticle extends Remote {
  public static final String NOMSERVICE = "ServiceArticle";
  
  public boolean ping() throws RemoteException;
  
  public Article lireArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException;
  
  public List<IdArticle> chargerListeIdArticle(IdSession pIdSession, CritereArticle pCritereArticle) throws RemoteException;
  
  public ListeArticleBase chargerListeArticleBase(IdSession pIdSession, List<IdArticle> pListeIdArticle) throws RemoteException;
  
  public ListeArticleBase chargerListeArticleBase(IdSession pIdSession, CritereArticle pCritereArticle) throws RemoteException;
  
  public ArticleBase chargerArticleBase(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException;
  
  public ListeArticleBase chargerListeIdArticlesEnRupture(IdSession pIdSession, CritereArticle pCritereArticle) throws RemoteException;
  
  public ListeResultatRechercheArticle chargerListeResultatRechercheArticle(IdSession pIdSession,
      List<IdResultatRechercheArticle> pListeIdResultatRechercheArticle, ParametresLireArticle pParametres) throws RemoteException;
  
  public ListeArticle chargerListeInformationAchat(IdSession pIdSession, List<IdArticle> pListeIdArticle,
      ParametresLireArticle pParametres) throws RemoteException;
  
  public ListeArticle chargerListeArticlesEnRupture(IdSession pIdSession, List<IdArticle> pListeIdArticle,
      ParametresLireArticle pParametres) throws RemoteException;
  
  public ListeArticle chargerListeArticleTransport(IdSession pIdSession, CriteresRechercheArticles pCriteres) throws RemoteException;
  
  public ListeArticle chargerListeArticleCommentaire(IdSession pIdSession, CritereArticleCommentaire pCritere) throws RemoteException;
  
  public ListeResultatRechercheArticlePalette chargerListeResultatRechercheArticlePalette(IdSession pIdSession,
      CritereResultatRechercheArticlePalette pCritere) throws RemoteException;
  
  public ListeArticle chargerListeArticlePaletteFournisseur(IdSession pIdSession, CritereArticle pCriteres) throws RemoteException;
  
  public Article chargerArticle(IdSession pIdSession, Article pArticle) throws RemoteException;
  
  public boolean verifierExistenceArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException;
  
  public TarifArticle chargerTarifArticle(IdSession pIdSession, IdArticle pIdArticle, String pCodeDevise, Date pDateTarif, boolean isTTC)
      throws RemoteException;
  
  public String[] chargerListeURLArticle(IdSession pIdSession, Article pArticle) throws RemoteException;
  
  public ConditionAchat chargerConditionAchat(IdSession pIdSession, IdArticle pIdArticle, Date pDate) throws RemoteException;
  
  public ListeGroupesArticles chargerListeGroupesArticles(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException;
  
  public ListeSousFamille chargerListeSousFamille(IdSession pIdSession, IdEtablissement pIdEtablissement, String pFamille)
      throws RemoteException;
  
  public ListeMarques chargerListeMarques(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException;
  
  public BigDecimal chargerMontantDEEEArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException;
  
  public BlocNote chargerBlocNoteConditionAchat(IdSession pIdSession, IdArticle pIdArticle, IdFournisseur pIdFournisseur)
      throws RemoteException;
  
  public String chargerTexteReferenceFabricant(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException;
  
  public BigDecimal chargerPrixFabricantArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException;
  
  public Article sauverArticleSpecial(IdSession pIdSession, Article pArticle, Client pClient, BigDecimal pPrixNetHT, ConditionAchat pCNA)
      throws RemoteException;
  
  public void modifierArticleSpecial(IdSession pIdSession, Article pArticle, Client pClient, BigDecimal pPrixNetHT, ConditionAchat pCNA)
      throws RemoteException;
  
  public ListeArticle chargerListeArticleLie(IdSession pIdSession, CritereArticleLie pCritere) throws RemoteException;
  
  public ListeArticle chargerListeArticleLieProposes(IdSession pIdSession, CritereArticleLie pCriteres) throws RemoteException;
  
  public ListeArticle chargerListeArticleLieAutomatiquement(IdSession pIdSession, CritereArticleLie pCritere) throws RemoteException;
  
  public boolean verifierExistenceArticleLiePropose(IdSession pIdSession, CritereArticleLie pCriteres) throws RemoteException;
  
  public boolean verifierExistenceArticleLieAutomatiquement(IdSession pIdSession, CritereArticleLie pCritere) throws RemoteException;
  
  public void sauverTopMemoObligatoire(IdSession pIdSession, Article aArticle) throws RemoteException;
  
  public void sauverArticleCommentaire(IdSession pIdSession, Article pArticle) throws RemoteException;
  
  public Article chargerArticleCommentaire(IdSession pIdSession, Article pArticle) throws RemoteException;
  
  public void supprimerArticleCommentaire(IdSession pIdSession, Article pArticle) throws RemoteException;
  
  public void sauverNegociationAchat(IdSession pIdSession, ConditionAchat pCNA) throws RemoteException;
  
  public CalculPrixVente calculerPrixVente(IdSession pIdSession, ParametreChargement pParametreChargement) throws RemoteException;
  
  public ParametreChargement chargerParametreCalculPrixVente(IdSession pIdSession, ParametreChargement pParametreChargement)
      throws RemoteException;
  
  public void testerCalculPrixVente(IdSession pIdSession, CritereLigneVente pCritereLigneVente, String pCheminRacine,
      int pNombreCalculMax) throws RemoteException;
  
  public ListeLot chargerListeLot(IdSession pIdSession, List<IdLot> pListeIdLot) throws RemoteException;
  
  public ListeLot chargerListeLot(IdSession pIdSession, CritereLot pCritereLot) throws RemoteException;
  
  public ListeLot chargerListeLotDocument(IdSession pIdSession, CritereLot pCritereLot) throws RemoteException;
  
  public IdLigneVente sauverListeLot(IdSession pIdSession, IdLigneVente pIdLigneVente, BigDecimal pQuantiteArticle,
      BigDecimal pQuantiteTotale, ListeLot pListeLot) throws RemoteException;
  
}
