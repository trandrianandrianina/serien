/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import ri.serien.libcommun.gescom.commun.representant.CriteresRechercheRepresentant;
import ri.serien.libcommun.gescom.commun.representant.ListeRepresentant;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceRepresentant extends Remote {
  public static final String NOMSERVICE = "ServiceRepresentant";
  
  public boolean ping() throws RemoteException;
  
  public ListeRepresentant chargerListeRepresentant(IdSession pIdSession, CriteresRechercheRepresentant pCriteres)
      throws RemoteException;
}
