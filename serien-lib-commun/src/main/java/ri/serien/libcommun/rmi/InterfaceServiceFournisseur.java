/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.gescom.achat.catalogue.ChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.CriteresRechercheCfgCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.IdChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.IdConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ListeConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeFichierCSV;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.FiltreFournisseurBase;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseurBase;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceFournisseur extends Remote {
  public static final String NOMSERVICE = "ServiceFournisseur";
  
  public boolean ping() throws RemoteException;
  
  public Fournisseur chargerFournisseur(IdSession pIdSession, Fournisseur pFournisseur) throws RemoteException;
  
  public Fournisseur chargerFournisseur(IdSession pIdSession, IdFournisseur pIdFournisseur) throws RemoteException;
  
  public ListeFournisseurBase chargerListeFournisseurBase(IdSession pIdSession, List<IdFournisseur> pListeId) throws RemoteException;
  
  public ListeFournisseur chargerListeFournisseurEtablissement(IdSession pIdSession, IdEtablissement pIdEtablissement)
      throws RemoteException;
  
  public List<IdFournisseur> chargerListeIdFournisseur(IdSession pIdSession, CritereFournisseur pCritereFournisseur)
      throws RemoteException;
  
  public ListeAdresseFournisseur chargerListeAdresseFournisseur(IdSession pIdSession,
      List<IdAdresseFournisseur> pListeIdAdresseFournisseur) throws RemoteException;
  
  public ArrayList<IdAdresseFournisseur> chargerListeIdAdresseFournisseur(IdSession pIdSession, CritereFournisseur criteres)
      throws RemoteException;
  
  public AdresseFournisseur chargerAdresseFournisseur(IdSession pIdSession, IdAdresseFournisseur pIdAdresseFournisseur)
      throws RemoteException;
  
  public ListeAdresseFournisseur chargerListeAdresseFournisseur(IdSession pIdSession, IdEtablissement pIdEtablissement)
      throws RemoteException;
  
  public ListeFournisseur chargerListeFournisseur(IdSession pIdSession, CritereFournisseur pCriteres) throws RemoteException;
  
  public List<AdresseFournisseur> chargerListeAdresseFournisseur(IdSession pIdSession, IdFournisseur pIdFournisseur,
      boolean pInclureSiege) throws RemoteException;
  
  public ListeConfigurationCatalogue chargerListeConfigsCatalogue(IdSession pIdSession, CriteresRechercheCfgCatalogue criteres)
      throws RemoteException;
  
  public ConfigurationCatalogue chargerConfigurationCatalogueParId(IdSession pIdSession, IdConfigurationCatalogue pIdConfiguration)
      throws RemoteException;
  
  public ListeFichierCSV chargerListeFichiersCataloguesCSV(IdSession pIdSession) throws RemoteException;
  
  public ListeChampCatalogue chargerListeChampUnCatalogue(IdSession pIdSession, String pNomFichier) throws RemoteException;
  
  public ListeChampERP chargerListeChampERP(IdSession pIdSession) throws RemoteException;
  
  public void sauverUneConfigurationCatalogue(IdSession pIdSession, ConfigurationCatalogue pConfigurationCatalogue)
      throws RemoteException;
  
  public void supprimerUneConfigurationCatalogue(IdSession pIdSession, ConfigurationCatalogue pConfigurationCatalogue)
      throws RemoteException;
  
  public ListeFournisseurBase chargerListeFournisseurBaseParEtablissement(IdSession pIdSession, FiltreFournisseurBase pFiltres)
      throws RemoteException;
  
  public ChampERP chargerUnChampERPparId(IdSession pIdSession, IdChampERP pIdChampERP) throws RemoteException;
  
  public void sauverAssociationChampsConfigurationCatalogue(IdSession pIdSession, ConfigurationCatalogue pConfiguration,
      IdChampERP pIdChampERP) throws RemoteException;
  
}
