/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.List;

import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.ListeBDD;
import ri.serien.libcommun.exploitation.IdServeurPower;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.CritereBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.exploitation.documentlie.IdDossierDocumentLie;
import ri.serien.libcommun.exploitation.edition.demandeedition.DemandeEdition;
import ri.serien.libcommun.exploitation.edition.imprimante.CritereImprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.IdImprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.Imprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.ListeImprimante;
import ri.serien.libcommun.exploitation.edition.spool.CritereSpool;
import ri.serien.libcommun.exploitation.edition.spool.IdSpool;
import ri.serien.libcommun.exploitation.edition.spool.ListeSpool;
import ri.serien.libcommun.exploitation.edition.spool.Spool;
import ri.serien.libcommun.exploitation.environnement.FichierRuntime;
import ri.serien.libcommun.exploitation.flux.CritereFlux;
import ri.serien.libcommun.exploitation.flux.Flux;
import ri.serien.libcommun.exploitation.flux.IdFlux;
import ri.serien.libcommun.exploitation.flux.ListeFlux;
import ri.serien.libcommun.exploitation.licence.CodeTypeLicence;
import ri.serien.libcommun.exploitation.licence.CritereLicence;
import ri.serien.libcommun.exploitation.licence.EnumStatutLicence;
import ri.serien.libcommun.exploitation.licence.EnumTypeLicence;
import ri.serien.libcommun.exploitation.licence.IdLicence;
import ri.serien.libcommun.exploitation.licence.Licence;
import ri.serien.libcommun.exploitation.licence.ListeLicence;
import ri.serien.libcommun.exploitation.mail.CritereMail;
import ri.serien.libcommun.exploitation.mail.CritereTypeMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeMail;
import ri.serien.libcommun.exploitation.mail.ListeTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeVariableMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.exploitation.mail.TypeMail;
import ri.serien.libcommun.exploitation.notification.CritereNotification;
import ri.serien.libcommun.exploitation.notification.IdNotification;
import ri.serien.libcommun.exploitation.notification.ListeNotification;
import ri.serien.libcommun.exploitation.notification.Notification;
import ri.serien.libcommun.exploitation.personnalisation.parametrent.ParametreNT;
import ri.serien.libcommun.exploitation.utilisateur.CritereUtilisateur;
import ri.serien.libcommun.exploitation.utilisateur.IdUtilisateur;
import ri.serien.libcommun.exploitation.utilisateur.ListeUtilisateur;
import ri.serien.libcommun.outils.fichier.GestionFichierByte;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service technique.
 * 
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les
 * convertir en MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service
 * retourne un résultat, c'est qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut
 * niveau par le logiciel.
 */
public class ManagerServiceTechnique {
  private static InterfaceServiceTechnique service;
  
  /**
   * Retourne le service ServiceTransfert.
   */
  private static InterfaceServiceTechnique getService() {
    if (service == null) {
      InterfaceServiceTechnique interfaceService =
          (InterfaceServiceTechnique) ManagerClientRMI.recupererService(InterfaceServiceTechnique.NOMSERVICE);
      
      service = (InterfaceServiceTechnique) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceTechnique.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static void activerTraceDebug(boolean pDebug) {
    try {
      getService().activerTraceDebug(pDebug);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void confirmerPresenceClient(IdClientDesktop pIdClientDesktop) {
    try {
      getService().confirmerPresenceClient(pIdClientDesktop);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Méthodes échaénges buffers
  
  public static String dialoguerAvecProgramme(IdSession pIdSession, String pMessage, int pDelaiAttente) {
    try {
      return getService().dialoguerAvecProgramme(pIdSession, pMessage, pDelaiAttente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean envoyerMessageAuProgramme(IdSession pIdSession, String pMessage) {
    try {
      return getService().envoyerMessageAuProgramme(pIdSession, pMessage);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String recevoirMessageDuProgramme(IdSession pIdSession, int pDelaiAttente) {
    try {
      return getService().recevoirMessageDuProgramme(pIdSession, pDelaiAttente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void repondreMessageErreur(IdSession pIdSession, String pReponse) {
    try {
      getService().repondreMessageErreur(pIdSession, pReponse);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static FichierRuntime demanderFichier(IdSession pIdSession, String pNomFichier) {
    try {
      return getService().demanderFichier(pIdSession, pNomFichier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static GestionFichierByte telechargerFichier(IdSession pIdSession, String pFichierSourceIFS) {
    try {
      return getService().telechargerFichier(pIdSession, pFichierSourceIFS);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean televerserFichier(IdSession pIdSession, String pFichierDestinationIfs, GestionFichierByte pFichier) {
    try {
      return getService().televerserFichier(pIdSession, pFichierDestinationIfs, pFichier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean supprimerFichier(IdSession pIdSession, String pNomFichier) {
    try {
      return getService().supprimerFichier(pIdSession, pNomFichier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean copierFichier(IdSession pIdSession, String pCheminFichierSource, String pCheminFichierDestination,
      boolean pSuppressionFichierOriginal) {
    try {
      return getService().copierFichier(pIdSession, pCheminFichierSource, pCheminFichierDestination, pSuppressionFichierOriginal);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String demanderContenuDossier(IdSession pIdSession, String pNomFichier) {
    try {
      return getService().demanderContenuDossier(pIdSession, pNomFichier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static FichierRuntime telechargerFichierRuntime(IdSession pIdSession, String pNomFichierRuntime) {
    try {
      return getService().telechargerFichierRuntime(pIdSession, pNomFichierRuntime);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String effectuerRequeteSysteme(IdSession pIdSession, String pChaine) {
    try {
      return getService().effectuerRequeteSysteme(pIdSession, pChaine);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Copie un fichier SAVF de l'IFS vers une bibliothèque en 5250.
   */
  public static void copierFichierSavf(IdSession pIdSession, String pFichierSavf, Bibliotheque pBibliotheque) {
    try {
      getService().copierFichierSavf(pIdSession, pFichierSavf, pBibliotheque);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Restaure un fichier SAVF.
   */
  public static void restaurerFichierSavf(IdSession pIdSession, Bibliotheque pBibliotheque, String pNomFichierSavf) {
    try {
      getService().restaurerFichierSavf(pIdSession, pBibliotheque, pNomFichierSavf);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Méthodes concernant la gestion des spools
  
  public static ParametreNT retournerInformationsParamNT(IdSession pIdSession, String pCurlib) {
    try {
      return getService().retournerInformationsParamNT(pIdSession, pCurlib);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String exporterSpool(IdSession pIdSession, DemandeEdition pDemandeEdition) {
    try {
      return getService().exporterSpool(pIdSession, pDemandeEdition);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Nouvelles méthodes pour la gestion des spool et des imprimantes
  
  public static ListeSpool chargerListeSpool(IdSession pIdSession, List<IdSpool> pListeIdSpool) {
    try {
      return getService().chargerListeSpool(pIdSession, pListeIdSpool);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdSpool> chargerListeIdSpool(IdSession pIdSession, CritereSpool pCritereSpool) {
    try {
      return getService().chargerListeIdSpool(pIdSession, pCritereSpool);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdImprimante> chargerListeIdImprimante(IdSession pIdSession, CritereImprimante pCritereImprimante) {
    try {
      return getService().chargerListeIdImprimante(pIdSession, pCritereImprimante);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeImprimante chargerListeImprimante(IdSession pIdSession, List<IdImprimante> pListeId) {
    try {
      return getService().chargerListeImprimante(pIdSession, pListeId);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean imprimerSpool(IdSession pIdSession, IdSpool pIdSpool, String pNomOutq, int pNombreExemplaire) {
    try {
      return getService().imprimerSpool(pIdSession, pIdSpool, pNomOutq, pNombreExemplaire);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean demarrerImprimante(IdSession pIdSession, Imprimante pImprimante) {
    try {
      return getService().demarrerImprimante(pIdSession, pImprimante);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean suspendreSpool(IdSession pIdSession, IdSpool pIdSpool) {
    try {
      return getService().suspendreSpool(pIdSession, pIdSpool);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean libererSpool(IdSession pIdSession, IdSpool pIdSpool) {
    try {
      return getService().libererSpool(pIdSession, pIdSpool);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean supprimerSpool(IdSession pIdSession, IdSpool pIdSpool) {
    try {
      return getService().supprimerSpool(pIdSession, pIdSpool);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Spool chargerContenuSpool(IdSession pIdSession, Spool pSpool) {
    try {
      return getService().chargerContenuSpool(pIdSession, pSpool);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Méthodes concernant la gestion des bibliothèques
  
  public static List<Bibliotheque> chargerListeBibliothequeOS400(IdSession pIdSession, CritereBibliotheque pCriteres) {
    try {
      return getService().chargerListeBibliothequeOS400(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeBDD chargerListeBaseDeDonneesOS400(IdSession pIdSession, CritereBibliotheque pCriteres) {
    try {
      return getService().chargerListeBaseDeDonneesOS400(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String chargerDescriptionBibliotheque(IdSession pIdSession, IdBibliotheque pIdBibliotheque) {
    try {
      return getService().chargerDescriptionBibliotheque(pIdSession, pIdBibliotheque);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeBDD chargerListeBaseDeDonnees(IdSession pIdSession, List<IdBibliotheque> pListeIdBibliotheque) {
    try {
      return getService().chargerListeBaseDeDonnees(pIdSession, pListeIdBibliotheque);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Méthodes concernant la gestion des dataareas
  
  public static String chargerDataarea(IdSession pIdSession, Dataarea pDataarea) {
    try {
      return getService().chargerDataarea(pIdSession, pDataarea);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Méthodes concernant les flux
  
  /**
   * Charger une liste d'IdFlux à partir de critères de recherche.
   * 
   * @param pIdSession
   * @param pCritere Critères de recherche
   * @return Liste d'IdFlux
   */
  public static List<IdFlux> chargerListeIdFlux(IdSession pIdSession, CritereFlux pCritere) {
    try {
      return getService().chargerListeIdFlux(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger une liste deflux à partir d'une liste d'IdFlux.
   * 
   * @param pIdSession
   * @param pListeIdFlux Liste d'IdFlux
   * @return Liste de flux
   */
  public static ListeFlux chargerListeFlux(IdSession pIdSession, List<IdFlux> pListeIdFlux) {
    try {
      return getService().chargerListeFlux(pIdSession, pListeIdFlux);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Retourner la version par défaut des flux.
   * 
   * @param pIdSession
   * @return Version par défaut des flux
   */
  public static String getVersionDefautFlux(IdSession pIdSession) {
    try {
      return getService().getVersionDefautFlux(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Sauver un flux en base.
   * 
   * @param pIdSession
   * @param pFlux Flux à persister en base.
   */
  public static void sauverFlux(IdSession pIdSession, Flux pFlux) {
    try {
      getService().sauverFlux(pIdSession, pFlux);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Contrôler la présence de la table des flux en base de données..
   * 
   * @param pIdSession
   */
  public static void controlerPresenceTableFlux(IdSession pIdSession) {
    try {
      getService().controlerPresenceTableFlux(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Rejouer une liste de flux.
   * 
   * @param pIdSession
   * @param pListeFlux Liste de flux à rejouer.
   */
  public static void rejouerFlux(IdSession pIdSession, ListeFlux pListeFlux) {
    try {
      getService().rejouerFlux(pIdSession, pListeFlux);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
    return;
  }
  
  // -- Méthodes concernant la gestion des mails
  
  /**
   * Créer un mail.
   */
  public static IdMail creerMail(IdSession pIdSession, Mail pMail) {
    try {
      return getService().creerMail(pIdSession, pMail);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Mail chargerMail(IdSession pIdSession, IdMail pIdMail) {
    try {
      return getService().chargerMail(pIdSession, pIdMail);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Liste les mails.
   */
  public static ListeMail chargerListeMail(IdSession pIdSession, CritereMail pCritereMail) {
    try {
      return getService().chargerListeMail(pIdSession, pCritereMail);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charge une liste d'IdMail suivant des CritereMail.
   */
  public static List<IdMail> chargerListeIdMail(IdSession pIdSession, CritereMail pCritereMail) {
    try {
      return getService().chargerListeIdMail(pIdSession, pCritereMail);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charge la liste des mail suivant une liste d'IdMail.
   */
  public static ListeMail chargerListeMail(IdSession pIdSession, List<IdMail> pListeId) {
    try {
      return getService().chargerListeMail(pIdSession, pListeId);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeTypeMail chargerListeTypeMail(IdSession pIdSession, List<IdTypeMail> pListeIdTypeMail) {
    try {
      return getService().chargerListeTypeMail(pIdSession, pListeIdTypeMail);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeTypeMail chargerListeTypeMail(IdSession pIdSession, CritereTypeMail pCritereTypeMail) {
    try {
      return getService().chargerListeTypeMail(pIdSession, pCritereTypeMail);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverTypeMail(IdSession pIdSession, TypeMail pTypeMail, boolean pJusteLeCorps) {
    try {
      getService().sauverTypeMail(pIdSession, pTypeMail, pJusteLeCorps);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeVariableMail chargerListeVariableMail(IdSession pIdSession, IdMail pIdMail) {
    try {
      return getService().chargerListeVariableMail(pIdSession, pIdMail);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void creerListeVariableMail(IdSession pIdSession, ListeVariableMail pListeVariableMail) {
    try {
      getService().creerListeVariableMail(pIdSession, pListeVariableMail);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Méthodes concernant la gestion des documents liés
  
  /**
   * Charger la liste des IdDossierDocumentLie client
   */
  public static List<IdDossierDocumentLie> chargerListeIdDossierDocumentLieClient(IdSession pIdSession) {
    try {
      return getService().chargerListeIdDossierDocumentLieClient(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger la liste des documents liés dans un dossier
   */
  public static String[] chargerListeDocumentLie(IdSession pIdSession, String pDossierDocumentLie) {
    try {
      return getService().chargerListeDocumentLie(pIdSession, pDossierDocumentLie);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Renvoyer le nombre de documents liés dans un dossier
   */
  public static int compterDocumentLie(IdSession pIdSession, String pDossierDocumentLie) {
    try {
      return getService().compterDocumentLie(pIdSession, pDossierDocumentLie);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void ajouterDocumentLie(IdSession pIdSession, String pCheminDossier, GestionFichierByte pCheminDocument) {
    try {
      getService().ajouterDocumentLie(pIdSession, pCheminDossier, pCheminDocument);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Méthodes concernant la gestion des licences
  
  public static List<IdLicence> chargerListeIdLicence(IdSession pIdSession, CritereLicence pCriteres) {
    try {
      return getService().chargerListeIdLicence(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLicence chargerListeLicence(IdSession pIdSession, List<IdLicence> pListeIdLicence) {
    try {
      return getService().chargerListeLicence(pIdSession, pListeIdLicence);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdLicence sauverLicence(IdSession pIdSession, Licence pLicence) {
    try {
      return getService().sauverLicence(pIdSession, pLicence);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Licence chargerLicence(IdSession pIdSession, IdLicence pIdLicence) {
    try {
      return getService().chargerLicence(pIdSession, pIdLicence);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void changerStatutLicence(IdSession pIdSession, IdLicence pIdLicence, EnumStatutLicence pStatutLicence) {
    try {
      getService().changerStatutLicence(pIdSession, pIdLicence, pStatutLicence);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void controlerLicence(EnumTypeLicence pTypeLicence, CodeTypeLicence pCodeTypeLicence) {
    try {
      getService().controlerLicence(pTypeLicence, pCodeTypeLicence);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<String> retournerListeDescriptionLicenceFinValiditeProche(int pNombreJourRestants) {
    try {
      return getService().retournerListeDescriptionLicenceFinValiditeProche(pNombreJourRestants);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void rafraichirListeLicenceActiveServeur(IdSession pIdSession) {
    try {
      getService().rafraichirListeLicenceActiveServeur(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Méthodes concernant les informations systèmes
  
  public static IdServeurPower retournerIdServeurPower() {
    try {
      return getService().retournerIdServeurPower();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Méthodes concernant les notifications
  
  public static List<IdNotification> chargerListeIdNotification(IdSession pIdSession, CritereNotification pCriteres) {
    try {
      return getService().chargerListeIdNotification(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeNotification chargerListeNotification(IdSession pIdSession, List<IdNotification> pListeIdNotification) {
    try {
      return getService().chargerListeNotification(pIdSession, pListeIdNotification);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdNotification sauverNotification(IdSession pIdSession, Notification pNotification) {
    try {
      return getService().sauverNotification(pIdSession, pNotification);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Notification chargerNotification(IdSession pIdSession, IdNotification pIdNotification) {
    try {
      return getService().chargerNotification(pIdSession, pIdNotification);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeUtilisateur chargerListeUtilisateur(IdSession pIdSession, List<IdUtilisateur> pListeIdUtilisateur) {
    try {
      return getService().chargerListeUtilisateur(pIdSession, pListeIdUtilisateur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdUtilisateur> chargerListeIdUtilisateur(IdSession pIdSession, CritereUtilisateur pCritereUtilisateur) {
    try {
      return getService().chargerListeIdUtilisateur(pIdSession, pCritereUtilisateur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
}
