/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.ListeBDD;
import ri.serien.libcommun.exploitation.IdServeurPower;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.CritereBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.exploitation.documentlie.IdDossierDocumentLie;
import ri.serien.libcommun.exploitation.edition.demandeedition.DemandeEdition;
import ri.serien.libcommun.exploitation.edition.imprimante.CritereImprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.IdImprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.Imprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.ListeImprimante;
import ri.serien.libcommun.exploitation.edition.spool.CritereSpool;
import ri.serien.libcommun.exploitation.edition.spool.IdSpool;
import ri.serien.libcommun.exploitation.edition.spool.ListeSpool;
import ri.serien.libcommun.exploitation.edition.spool.Spool;
import ri.serien.libcommun.exploitation.environnement.FichierRuntime;
import ri.serien.libcommun.exploitation.flux.CritereFlux;
import ri.serien.libcommun.exploitation.flux.Flux;
import ri.serien.libcommun.exploitation.flux.IdFlux;
import ri.serien.libcommun.exploitation.flux.ListeFlux;
import ri.serien.libcommun.exploitation.licence.CodeTypeLicence;
import ri.serien.libcommun.exploitation.licence.CritereLicence;
import ri.serien.libcommun.exploitation.licence.EnumStatutLicence;
import ri.serien.libcommun.exploitation.licence.EnumTypeLicence;
import ri.serien.libcommun.exploitation.licence.IdLicence;
import ri.serien.libcommun.exploitation.licence.Licence;
import ri.serien.libcommun.exploitation.licence.ListeLicence;
import ri.serien.libcommun.exploitation.mail.CritereMail;
import ri.serien.libcommun.exploitation.mail.CritereTypeMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeMail;
import ri.serien.libcommun.exploitation.mail.ListeTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeVariableMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.exploitation.mail.TypeMail;
import ri.serien.libcommun.exploitation.module.CritereParametreModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.notification.CritereNotification;
import ri.serien.libcommun.exploitation.notification.IdNotification;
import ri.serien.libcommun.exploitation.notification.ListeNotification;
import ri.serien.libcommun.exploitation.notification.Notification;
import ri.serien.libcommun.exploitation.personnalisation.parametrent.ParametreNT;
import ri.serien.libcommun.exploitation.utilisateur.CritereUtilisateur;
import ri.serien.libcommun.exploitation.utilisateur.IdUtilisateur;
import ri.serien.libcommun.exploitation.utilisateur.ListeUtilisateur;
import ri.serien.libcommun.outils.fichier.GestionFichierByte;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceTechnique extends Remote {
  public static final String NOMSERVICE = "ServiceTechnique";
  
  // -- Méthodes bas niveaux
  
  public void activerTraceDebug(boolean pDebug) throws RemoteException;
  
  public boolean ping() throws RemoteException;
  
  public void confirmerPresenceClient(IdClientDesktop pIdClientDesktop) throws RemoteException;
  
  // -- Méthodes échanges buffers
  
  public String dialoguerAvecProgramme(IdSession pIdSession, String pMessage, int pDelaiAttente) throws RemoteException;
  
  public boolean envoyerMessageAuProgramme(IdSession pIdSession, String pMessage) throws RemoteException;
  
  public String recevoirMessageDuProgramme(IdSession pIdSession, int pDelaiAttente) throws RemoteException;
  
  public void repondreMessageErreur(IdSession pIdSession, String pReponse) throws RemoteException;
  
  public FichierRuntime demanderFichier(IdSession pIdSession, String pNomFichier) throws RemoteException;
  
  public GestionFichierByte telechargerFichier(IdSession pIdSession, String pFichierSourceIFS) throws RemoteException;
  
  public boolean televerserFichier(IdSession pIdSession, String pFichierDestinationIfs, GestionFichierByte pFichier)
      throws RemoteException;
  
  public boolean supprimerFichier(IdSession pIdSession, String pNomFichier) throws RemoteException;
  
  public boolean copierFichier(IdSession pIdSession, String pCheminFichierSource, String pCheminFichierDestination,
      boolean pSuppressionFichierOriginal) throws RemoteException;
  
  public String demanderContenuDossier(IdSession pIdSession, String pNomFichier) throws RemoteException;
  
  public FichierRuntime telechargerFichierRuntime(IdSession pIdSession, String pNomFichierRuntime) throws RemoteException;
  
  public String effectuerRequeteSysteme(IdSession pIdSession, String pChaine) throws RemoteException;
  
  public void copierFichierSavf(IdSession pIdSession, String pFichierSavf, Bibliotheque pBibliotheque) throws RemoteException;
  
  public void restaurerFichierSavf(IdSession pIdSession, Bibliotheque pBibliotheque, String pNomFichierSavf) throws RemoteException;
  
  // -- Méthodes concernant la gestion des spools
  
  public ParametreNT retournerInformationsParamNT(IdSession pIdSession, String pCurlib) throws RemoteException;
  
  public String exporterSpool(IdSession pIdSession, DemandeEdition pDemandeEdition) throws RemoteException;
  
  public ListeSpool chargerListeSpool(IdSession pIdSession, List<IdSpool> pListeIdSpool) throws RemoteException;
  
  public List<IdSpool> chargerListeIdSpool(IdSession pIdSession, CritereSpool pCritereSpool) throws RemoteException;
  
  public boolean suspendreSpool(IdSession pIdSession, IdSpool pIdSpool) throws RemoteException;
  
  public boolean libererSpool(IdSession pIdSession, IdSpool pIdSpool) throws RemoteException;
  
  public boolean supprimerSpool(IdSession pIdSession, IdSpool pIdSpool) throws RemoteException;
  
  public Spool chargerContenuSpool(IdSession pIdSession, Spool pSpool) throws RemoteException;
  
  public List<IdImprimante> chargerListeIdImprimante(IdSession pIdSession, CritereImprimante pCritereImprimante) throws RemoteException;
  
  public ListeImprimante chargerListeImprimante(IdSession pIdSession, List<IdImprimante> pListeId) throws RemoteException;
  
  public boolean imprimerSpool(IdSession pIdSession, IdSpool pIdSpool, String pNomOutq, int pNombreExemplaire) throws RemoteException;
  
  public boolean demarrerImprimante(IdSession pIdSession, Imprimante pImprimante) throws RemoteException;
  
  // -- Méthodes concernant la gestion des bibliothèques
  public List<Bibliotheque> chargerListeBibliothequeOS400(IdSession pIdSession, CritereBibliotheque pCriteres) throws RemoteException;
  
  public ListeBDD chargerListeBaseDeDonneesOS400(IdSession pIdSession, CritereBibliotheque pCriteres) throws RemoteException;
  
  public String chargerDescriptionBibliotheque(IdSession pIdSession, IdBibliotheque pIdBibliotheque) throws RemoteException;
  
  public ListeBDD chargerListeBaseDeDonnees(IdSession pIdSession, List<IdBibliotheque> pListeIdBibliotheque) throws RemoteException;
  
  // -- Méthodes concernant la gestion des dataareas
  public String chargerDataarea(IdSession pIdSession, Dataarea pDataarea) throws RemoteException;
  
  // -- Méthodes concernant les flux
  
  public List<IdFlux> chargerListeIdFlux(IdSession pIdSession, CritereFlux pCritere) throws RemoteException;
  
  public ListeFlux chargerListeFlux(IdSession pIdSession, List<IdFlux> pListeIdFlux) throws RemoteException;
  
  public String getVersionDefautFlux(IdSession pIdSession) throws RemoteException;
  
  public void sauverFlux(IdSession pIdSession, Flux pFlux) throws RemoteException;
  
  public void controlerPresenceTableFlux(IdSession pIdSession) throws RemoteException;
  
  public void rejouerFlux(IdSession pIdSession, ListeFlux pListeFlux) throws RemoteException;
  
  // -- Méthodes concernant la gestion des mails
  
  public IdMail creerMail(IdSession pIdSession, Mail pMail) throws RemoteException;
  
  public Mail chargerMail(IdSession pIdSession, IdMail pIdMail) throws RemoteException;
  
  public List<IdMail> chargerListeIdMail(IdSession pIdSession, CritereMail pCritereMail) throws RemoteException;
  
  public ListeMail chargerListeMail(IdSession pIdSession, CritereMail pCritereMail) throws RemoteException;
  
  public ListeMail chargerListeMail(IdSession pIdSession, List<IdMail> pListeId) throws RemoteException;
  
  public ListeTypeMail chargerListeTypeMail(IdSession pIdSession, List<IdTypeMail> pListeIdTypeMail) throws RemoteException;
  
  public ListeTypeMail chargerListeTypeMail(IdSession pIdSession, CritereTypeMail pCritereTypeMail) throws RemoteException;
  
  public void sauverTypeMail(IdSession pIdSession, TypeMail pTypeMail, boolean pJusteLeCorps) throws RemoteException;
  
  public void creerListeTypeMail(IdSession pIdSession, List<TypeMail> pListeTypeMail) throws RemoteException;
  
  public ListeVariableMail chargerListeVariableMail(IdSession pIdSession, IdMail pIdMail) throws RemoteException;
  
  public void creerListeVariableMail(IdSession pIdSession, ListeVariableMail pListeVariableMail) throws RemoteException;
  
  // -- Méthodes documents liés
  
  public List<IdDossierDocumentLie> chargerListeIdDossierDocumentLieClient(IdSession pIdSession) throws RemoteException;
  
  public String[] chargerListeDocumentLie(IdSession pIdSession, String pDossierDocumentLie) throws RemoteException;
  
  public int compterDocumentLie(IdSession pIdSession, String pDossierDocumentLie) throws RemoteException;
  
  public void ajouterDocumentLie(IdSession pIdSession, String pCheminDossier, GestionFichierByte pCheminDocument) throws RemoteException;
  
  // -- Méthode paramètres modules
  
  public ListeParametreModule chargerListeParametre(IdSession pIdSession, CritereParametreModule pCritere) throws RemoteException;
  
  // -- Méthodes concernant la gestion des licences
  
  public List<IdLicence> chargerListeIdLicence(IdSession pIdSession, CritereLicence pCriteres) throws RemoteException;
  
  public ListeLicence chargerListeLicence(IdSession pIdSession, List<IdLicence> pListeIdLicence) throws RemoteException;
  
  public IdLicence sauverLicence(IdSession pIdSession, Licence pLicence) throws RemoteException;
  
  public Licence chargerLicence(IdSession pIdSession, IdLicence pIdLicence) throws RemoteException;
  
  public void changerStatutLicence(IdSession pIdSession, IdLicence pIdLicence, EnumStatutLicence pStatutLicence) throws RemoteException;
  
  public void controlerLicence(EnumTypeLicence pTypeLicence, CodeTypeLicence pCodeTypeLicence) throws RemoteException;
  
  public List<String> retournerListeDescriptionLicenceFinValiditeProche(int pNombreJourRestants) throws RemoteException;
  
  public void rafraichirListeLicenceActiveServeur(IdSession pIdSession) throws RemoteException;
  
  // -- Méthodes concernant les informations systèmes
  
  public IdServeurPower retournerIdServeurPower() throws RemoteException;
  
  // -- Méthodes concernant les notifications
  
  public List<IdNotification> chargerListeIdNotification(IdSession pIdSession, CritereNotification pCriteres) throws RemoteException;
  
  public ListeNotification chargerListeNotification(IdSession pIdSession, List<IdNotification> pListeIdNotification)
      throws RemoteException;
  
  public IdNotification sauverNotification(IdSession pIdSession, Notification pNotification) throws RemoteException;
  
  public Notification chargerNotification(IdSession pIdSession, IdNotification pIdNotification) throws RemoteException;
  
  // -- Méthode concernant les utilisateurs
  
  public ListeUtilisateur chargerListeUtilisateur(IdSession pIdSession, List<IdUtilisateur> pIdUtilisateur) throws RemoteException;
  
  public List<IdUtilisateur> chargerListeIdUtilisateur(IdSession pIdSession, CritereUtilisateur pCritereUtilisateur)
      throws RemoteException;
  
}
