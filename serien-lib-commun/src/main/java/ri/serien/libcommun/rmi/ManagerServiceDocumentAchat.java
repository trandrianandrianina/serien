/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.exploitation.dataarea.LdaSerien;
import ri.serien.libcommun.gescom.achat.document.CritereDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumOptionEdition;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ListeReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.OptionsEditionAchat;
import ri.serien.libcommun.gescom.achat.document.ValeurInitialeCommandeAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchatBase;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.session.IdSession;

public class ManagerServiceDocumentAchat {
  private static InterfaceServiceDocumentAchat service;
  
  /**
   * Retourner service dédié à la gestion des documents d'achat.
   */
  private static InterfaceServiceDocumentAchat getService() {
    if (service == null) {
      InterfaceServiceDocumentAchat interfaceService =
          (InterfaceServiceDocumentAchat) ManagerClientRMI.recupererService(InterfaceServiceDocumentAchat.NOMSERVICE);
      
      service = (InterfaceServiceDocumentAchat) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceDocumentAchat.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdDocumentAchat> chargerListeIdDocumentAchat(IdSession pIdSession, CritereDocumentAchat pCriteres) {
    try {
      return getService().chargerListeIdDocumentAchat(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeDocumentAchat chargerListeDocumentAchat(IdSession pIdSession, List<IdDocumentAchat> pListeIdDocumentAchat) {
    try {
      return getService().chargerListeDocumentAchat(pIdSession, pListeIdDocumentAchat);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static DocumentAchat sauverDocumentAchat(IdSession pIdSession, String pUtilisateur, DocumentAchat pDocumentAchat) {
    try {
      return getService().sauverDocumentAchat(pIdSession, pUtilisateur, pDocumentAchat);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static DocumentAchat chargerDocumentAchat(IdSession pIdSession, IdDocumentAchat pIdDocumentAchat, DocumentAchat pDocumentAchat,
      boolean avecHistorique) {
    try {
      return getService().chargerDocumentAchat(pIdSession, pIdDocumentAchat, pDocumentAchat, avecHistorique);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static LigneAchat initialiserLigneDocument(IdSession pIdSession, LigneAchat pLigneAchat, Date pDateTraitement) {
    try {
      return getService().initialiserLigneDocument(pIdSession, pLigneAchat, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static LigneAchat controlerLigneDocument(IdSession pIdSession, LigneAchat pLigneAchat, Date pDateTraitement) {
    try {
      return getService().controlerLigneDocument(pIdSession, pLigneAchat, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverLigneAchat(IdSession pIdSession, LigneAchat pLigneAchat, Date pDateTraitement) {
    try {
      getService().sauverLigneAchat(pIdSession, pLigneAchat, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLigneAchat chargerListeLigneAchat(IdSession pIdSession, List<IdLigneAchat> pListeIdLigneAchat) {
    try {
      return getService().chargerListeLigneAchat(pIdSession, pListeIdLigneAchat);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLigneAchat chargerListeLigneAchat(IdSession pIdSession, CritereLigneAchat pCriteres) {
    try {
      return getService().chargerListeLigneAchat(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdLigneAchat> chargerListeIdLignesAchat(IdSession pIdSession, CritereLigneAchat pCriteres) {
    try {
      return getService().chargerListeIdLignesAchat(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeReceptionAchat chargerListeReceptionAchat(IdSession pIdSession, List<IdLigneAchat> listeIdLignes) {
    try {
      return getService().chargerListeReceptionAchat(pIdSession, listeIdLignes);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerListeLigneDocument(IdSession pIdSession, List<LigneAchat> pListeLignes) {
    try {
      getService().supprimerListeLigneDocument(pIdSession, pListeLignes);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void validerDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement,
      EnumOptionEdition pOptionEdition) {
    try {
      getService().validerDocument(pIdSession, pIdDocument, pDateTraitement, pOptionEdition);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void annulerDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement) {
    try {
      getService().annulerDocument(pIdSession, pIdDocument, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void mettreEnAttenteDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement) {
    try {
      getService().mettreEnAttenteDocument(pIdSession, pIdDocument, pDateTraitement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String editerDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement,
      OptionsEditionAchat pOptionEdition, LdaSerien pLdaSerien) {
    try {
      return getService().editerDocument(pIdSession, pIdDocument, pDateTraitement, pOptionEdition, pLdaSerien);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeDocumentAchat chargerListeDocumentAchatAvecLigne(IdSession pIdSession, List<IdLigneAchat> pListeId) {
    try {
      return getService().chargerListeDocumentAchatAvecLigne(pIdSession, pListeId);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLigneAchatBase chargerListeLigneAchatBase(IdSession pIdSession, List<IdLigneAchat> pListeId) {
    try {
      return getService().chargerListeLigneAchatBase(pIdSession, pListeId);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeDocumentAchat recupererCommandeAchatliee(IdSession pIdSession, IdDocumentVente pIdDocumentVente) {
    try {
      return getService().recupererCommandeAchatliee(pIdSession, pIdDocumentVente);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ValeurInitialeCommandeAchat chargerDonneesCommandeInitiale(IdSession pIdSession, IdDocumentAchat pIdDocumentAchat) {
    try {
      return getService().chargerDonneesCommandeInitiale(pIdSession, pIdDocumentAchat);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
