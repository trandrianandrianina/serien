/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.ListeLigneMouvement;
import ri.serien.libcommun.gescom.commun.document.CritereAttenduCommande;
import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.stock.CritereStock;
import ri.serien.libcommun.gescom.commun.stock.ListeStock;
import ri.serien.libcommun.gescom.commun.stock.StockDetaille;
import ri.serien.libcommun.gescom.commun.stockattenducommande.CritereStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockattenducommande.ListeStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockcomptoir.CritereStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.IdStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.commun.stockmouvement.CritereStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.ListeStockMouvement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service stock.
 * 
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceStock {
  private static InterfaceServiceStock service;
  
  /**
   * Retourner service dédié à la gestion des stocks.
   */
  private static InterfaceServiceStock getService() {
    if (service == null) {
      InterfaceServiceStock interfaceService =
          (InterfaceServiceStock) ManagerClientRMI.recupererService(InterfaceServiceStock.NOMSERVICE);
      
      service = (InterfaceServiceStock) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceStock.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeStockComptoir chargerListeStockComptoir(IdSession pIdSession, CritereStockComptoir pCritereStock) {
    try {
      return getService().chargerListeStockComptoir(pIdSession, pCritereStock);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeStockComptoir chargerListeStockComptoir(IdSession pIdSession, List<IdStockComptoir> pListeId) {
    try {
      return getService().chargerListeStockComptoir(pIdSession, pListeId);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<LigneAttenduCommande> chargerListeLigneAttenduCommande(IdSession pIdSession,
      CritereAttenduCommande pCritereAttenduCommande) {
    try {
      return getService().chargerListeLigneAttenduCommande(pIdSession, pCritereAttenduCommande);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdLigneMouvement> chargerListeIdLigneMouvement(IdSession pIdSession, IdArticle pIdArticle, Magasin pMagasin) {
    try {
      return getService().chargerListeIdLigneMouvement(pIdSession, pIdArticle, pMagasin);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLigneMouvement chargerListeLigneMouvement(IdSession pIdSession, List<IdLigneMouvement> pListeIdMouvements,
      Integer pNombreDecimalesStock) {
    try {
      return getService().chargerListeLigneMouvement(pIdSession, pListeIdMouvements, pNombreDecimalesStock);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeStock chargerListeStock(IdSession pIdSession, CritereStock pCritereStock) {
    try {
      return getService().chargerListeStock(pIdSession, pCritereStock);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static StockDetaille chargerStockDetaille(IdSession pIdSession, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin,
      IdArticle pIdArticle) {
    try {
      return getService().chargerStockDetaille(pIdSession, pIdEtablissement, pIdMagasin, pIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeStockAttenduCommande chargerListeStockAttenduCommande(IdSession pIdSession,
      CritereStockAttenduCommande pCritereStockAttenduCommande) {
    try {
      return getService().chargerListeStockAttenduCommande(pIdSession, pCritereStockAttenduCommande);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeStockMouvement chargerListeStockMouvement(IdSession pIdSession, CritereStockMouvement pCritereStockMouvement) {
    try {
      return getService().chargerListeStockMouvement(pIdSession, pCritereStockMouvement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Date chargerDateMiniReappro(IdSession pIdSession, IdArticle pIdArticle) {
    try {
      return getService().chargerDateMiniReappro(pIdSession, pIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
}
