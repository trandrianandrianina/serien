/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.CritereArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.CritereArticleLie;
import ri.serien.libcommun.gescom.commun.article.CriteresRechercheArticles;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libcommun.gescom.commun.article.ListeGroupesArticles;
import ri.serien.libcommun.gescom.commun.article.ListeMarques;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.CritereResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.IdResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.ListeResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette.ListeResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.lot.CritereLot;
import ri.serien.libcommun.gescom.vente.lot.IdLot;
import ri.serien.libcommun.gescom.vente.lot.ListeLot;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service article.
 * 
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les
 * convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un
 * résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceArticle {
  private static InterfaceServiceArticle service;
  
  /**
   * Retourner service dédié à la gestion des articles.
   */
  private static InterfaceServiceArticle getService() {
    if (service == null) {
      InterfaceServiceArticle interfaceService =
          (InterfaceServiceArticle) ManagerClientRMI.recupererService(InterfaceServiceArticle.NOMSERVICE);
      
      service = (InterfaceServiceArticle) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceArticle.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Article lireArticle(IdSession pIdSession, IdArticle pIdArticle) {
    try {
      return getService().lireArticle(pIdSession, pIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdArticle> chargerListeIdArticle(IdSession pIdSession, CritereArticle pCritereArticle) {
    try {
      return getService().chargerListeIdArticle(pIdSession, pCritereArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ArticleBase chargerArticleBase(IdSession pIdSession, IdArticle pIdArticle) {
    try {
      return getService().chargerArticleBase(pIdSession, pIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticleBase chargerListeArticleBase(IdSession pIdSession, List<IdArticle> pListeIdArticle) {
    try {
      return getService().chargerListeArticleBase(pIdSession, pListeIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticleBase chargerListeArticleBase(IdSession pIdSession, CritereArticle pCritereArticle) {
    try {
      return getService().chargerListeArticleBase(pIdSession, pCritereArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticleBase chargerListeIdArticlesEnRupture(IdSession pIdSession, CritereArticle pCritereArticle) {
    try {
      return getService().chargerListeIdArticlesEnRupture(pIdSession, pCritereArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeResultatRechercheArticle chargerListeResultatRechercheArticle(IdSession pIdSession,
      List<IdResultatRechercheArticle> pListeIdResultatRechercheArticle, ParametresLireArticle pParametres) {
    try {
      return getService().chargerListeResultatRechercheArticle(pIdSession, pListeIdResultatRechercheArticle, pParametres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticle chargerListeInformationAchat(IdSession pIdSession, List<IdArticle> pListeIdArticles,
      ParametresLireArticle pParametres) {
    try {
      return getService().chargerListeInformationAchat(pIdSession, pListeIdArticles, pParametres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticle chargerListeArticleTransport(IdSession pIdSession, CriteresRechercheArticles pCriteres) {
    try {
      return getService().chargerListeArticleTransport(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticle chargerListeArticleCommentaire(IdSession pIdSession, CritereArticleCommentaire pCritere) {
    try {
      return getService().chargerListeArticleCommentaire(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeResultatRechercheArticlePalette chargerListeResultatRechercheArticlePalette(IdSession pIdSession,
      CritereResultatRechercheArticlePalette pCritere) {
    try {
      return getService().chargerListeResultatRechercheArticlePalette(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticle chargerListeArticlePaletteFournisseur(IdSession pIdSession, CritereArticle pCriteres) {
    try {
      return getService().chargerListeArticlePaletteFournisseur(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticle chargerListeArticlesEnRupture(IdSession pIdSession, List<IdArticle> pListeIdArticles,
      ParametresLireArticle pParametres) {
    try {
      return getService().chargerListeArticlesEnRupture(pIdSession, pListeIdArticles, pParametres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Article completerArticleStandard(IdSession pIdSession, Article pArticle) {
    try {
      return getService().chargerArticle(pIdSession, pArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean verifierExistenceArticle(IdSession pIdSession, IdArticle pIdArticle) {
    try {
      return getService().verifierExistenceArticle(pIdSession, pIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static TarifArticle chargerTarifArticle(IdSession pIdSession, IdArticle pIdArticle, String pCodeDevise, Date pDateTarif,
      boolean isTTC) {
    try {
      return getService().chargerTarifArticle(pIdSession, pIdArticle, pCodeDevise, pDateTarif, isTTC);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String[] chargerListeURLArticle(IdSession pIdSession, Article pArticle) {
    try {
      return getService().chargerListeURLArticle(pIdSession, pArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ConditionAchat chargerConditionAchat(IdSession pIdSession, IdArticle pIdArticle, Date pDate) {
    try {
      return getService().chargerConditionAchat(pIdSession, pIdArticle, pDate);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeGroupesArticles chargerListeGroupesArticles(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerListeGroupesArticles(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeSousFamille chargerListeSousFamille(IdSession pIdSession, IdEtablissement pIdEtablissement, String pFamille) {
    try {
      return getService().chargerListeSousFamille(pIdSession, pIdEtablissement, pFamille);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeMarques chargerListeMarques(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerListeMarques(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static BlocNote chargerBlocNoteConditionAchat(IdSession pIdSession, IdArticle pIdArticle, IdFournisseur pIdFournisseur) {
    try {
      return getService().chargerBlocNoteConditionAchat(pIdSession, pIdArticle, pIdFournisseur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static BigDecimal chargerMontantDEEEArticle(IdSession pIdSession, IdArticle pIdArticle) {
    try {
      return getService().chargerMontantDEEEArticle(pIdSession, pIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static String chargerTexteReferenceFabricant(IdSession pIdSession, IdArticle pIdArticle) {
    try {
      return getService().chargerTexteReferenceFabricant(pIdSession, pIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static BigDecimal chargerPrixFabricantArticle(IdSession pIdSession, IdArticle pIdArticle) {
    try {
      return getService().chargerPrixFabricantArticle(pIdSession, pIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Article sauverArticleSpecial(IdSession pIdSession, Article pArticle, Client pClient, BigDecimal pPrixNetHT,
      ConditionAchat pCNA) {
    try {
      return getService().sauverArticleSpecial(pIdSession, pArticle, pClient, pPrixNetHT, pCNA);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void modifierArticleSpecial(IdSession pIdSession, Article pArticle, Client pClient, BigDecimal pPrixNetHT,
      ConditionAchat pCNA) {
    try {
      getService().modifierArticleSpecial(pIdSession, pArticle, pClient, pPrixNetHT, pCNA);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticle chargerListeArticleLie(IdSession pIdSession, CritereArticleLie pCriteres) {
    try {
      return getService().chargerListeArticleLie(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticle chargerListeArticleLieProposes(IdSession pIdSession, CritereArticleLie pCriteres) {
    try {
      return getService().chargerListeArticleLieProposes(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeArticle chargerListeArticleLieAutomatiquement(IdSession pIdSession, CritereArticleLie pCritere) {
    try {
      return getService().chargerListeArticleLieAutomatiquement(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean verifierExistenceArticleLiePropose(IdSession pIdSession, CritereArticleLie pCriteres) {
    try {
      return getService().verifierExistenceArticleLiePropose(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean verifierExistenceArticleLieAutomatiquement(IdSession pIdSession, CritereArticleLie pCriteres) {
    try {
      return getService().verifierExistenceArticleLieAutomatiquement(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverTopMemoObligatoire(IdSession pIdSession, Article aArticle) {
    try {
      getService().sauverTopMemoObligatoire(pIdSession, aArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverArticleCommentaire(IdSession pIdSession, Article pArticle) {
    try {
      getService().sauverArticleCommentaire(pIdSession, pArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Article chargerArticleCommentaire(IdSession pIdSession, Article pArticle) {
    try {
      return getService().chargerArticleCommentaire(pIdSession, pArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerArticleCommentaire(IdSession pIdSession, Article pArticle) {
    try {
      getService().supprimerArticleCommentaire(pIdSession, pArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverNegociationAchat(IdSession pIdSession, ConditionAchat pCNA) {
    try {
      getService().sauverNegociationAchat(pIdSession, pCNA);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static CalculPrixVente calculerPrixVente(IdSession pIdSession, ParametreChargement pParametreChargement) {
    try {
      return getService().calculerPrixVente(pIdSession, pParametreChargement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ParametreChargement chargerParametreCalculPrixVente(IdSession pIdSession, ParametreChargement pParametreChargement) {
    try {
      return getService().chargerParametreCalculPrixVente(pIdSession, pParametreChargement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void testerCalculPrixVente(IdSession pIdSession, CritereLigneVente pCritereLigneVente, String pCheminRacine,
      int pNombreCalculMax) {
    try {
      getService().testerCalculPrixVente(pIdSession, pCritereLigneVente, pCheminRacine, pNombreCalculMax);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLot chargerListeLot(IdSession pIdSession, List<IdLot> pListeIdLot) {
    try {
      return getService().chargerListeLot(pIdSession, pListeIdLot);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLot chargerListeLot(IdSession pIdSession, CritereLot pCritereLot) {
    try {
      return getService().chargerListeLot(pIdSession, pCritereLot);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLot chargerListeLotDocument(IdSession pIdSession, CritereLot pCritereLot) {
    try {
      return getService().chargerListeLotDocument(pIdSession, pCritereLot);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Object sauverListeLot(IdSession pIdSession, IdLigneVente pIdLigneVente, BigDecimal pQuantiteArticle,
      BigDecimal pQuantiteTotale, ListeLot pListeLot) {
    try {
      return getService().sauverListeLot(pIdSession, pIdLigneVente, pQuantiteArticle, pQuantiteTotale, pListeLot);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
