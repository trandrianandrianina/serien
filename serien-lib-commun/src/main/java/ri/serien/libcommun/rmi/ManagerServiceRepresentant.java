/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;

import ri.serien.libcommun.gescom.commun.representant.CriteresRechercheRepresentant;
import ri.serien.libcommun.gescom.commun.representant.ListeRepresentant;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service représentant.
 *
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceRepresentant {
  private static InterfaceServiceRepresentant service;
  
  /**
   * Retourner service dédié à la gestion des représentants.
   */
  private static InterfaceServiceRepresentant getService() {
    if (service == null) {
      InterfaceServiceRepresentant interfaceService =
          (InterfaceServiceRepresentant) ManagerClientRMI.recupererService(InterfaceServiceRepresentant.NOMSERVICE);
      
      service = (InterfaceServiceRepresentant) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceRepresentant.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeRepresentant chargerListeRepresentant(IdSession pIdSession, CriteresRechercheRepresentant pCriteres) {
    try {
      return getService().chargerListeRepresentant(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
}
