/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.ListeSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.session.EnumTypeSession;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceSession extends Remote {
  public static final String NOMSERVICE = "ServiceSession";
  
  public boolean ping() throws RemoteException;
  
  public String getVersion() throws RemoteException;
  
  public IdClientDesktop creerClientDesktop() throws RemoteException;
  
  public void detruireClientDesktop(IdClientDesktop pIdClientDesktop) throws RemoteException;
  
  public boolean connecterClientDesktop(IdClientDesktop pIdClientDesktop, EnvUser pEnvUser) throws RemoteException;
  
  public EnvUser getEnvUser(IdClientDesktop pIdClientDesktop) throws RemoteException;
  
  public ListeSousEnvironnement listerSousEnvironnement(IdSession pIdSession) throws RemoteException;
  
  public SousEnvironnement recupererSousEnvironnement(IdSession pIdSession, IdSousEnvironnement pIdSousEnvironnement)
      throws RemoteException;
  
  public IdSession creerSession(IdClientDesktop pIdClientDesktop) throws RemoteException;
  
  public void ouvrirSession(IdSession pIdSession, EnumTypeSession pEnumSession, String pMessage) throws RemoteException;
  
  public void fermerSession(IdSession pIdSession) throws RemoteException;
  
  public void connecterBdd(IdClientDesktop pIdClientDesktop, BDD pBaseDeDonnees) throws RemoteException;
  
  public void deconnecterBdd(IdClientDesktop pIdClientDesktop) throws RemoteException;
  
  public BDD recupererBdd(IdClientDesktop pIdClientDesktop) throws RemoteException;
  
  public BDD recupererBddParDefaut(IdClientDesktop pIdClientDesktop) throws RemoteException;
  
  public int controlerVersion(IdClientDesktop pIdClientDesktop) throws RemoteException;
  
  public String getNomDossierRacineServeur() throws RemoteException;
  
}
