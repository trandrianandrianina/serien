/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.math.BigDecimal;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.vente.gvc.ConditionAchatGVC;
import ri.serien.libcommun.gescom.vente.gvc.FiltresGVC;
import ri.serien.libcommun.gescom.vente.gvc.LigneGvc;
import ri.serien.libcommun.gescom.vente.gvc.ListeParametresGVC;
import ri.serien.libcommun.gescom.vente.gvc.pricejet.TarifPriceJet;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceGVC extends Remote {
  public static final String NOMSERVICE = "ServiceGVC";

  public boolean ping() throws RemoteException;

  public ListeParametresGVC chargerListeParametresGVC(IdSession pIdSession) throws RemoteException;

  public void sauverListeParametresGVC(IdSession pIdSession, ListeParametresGVC listeParametresGVC) throws RemoteException;

  public BigDecimal chargerTauxTVAetbPilote(IdSession pIdSession) throws RemoteException;

  public String[] chargerListeEtablissement(IdSession pIdSession) throws RemoteException;

  public ArrayList<String> chargerListeProfils(IdSession pIdSession) throws RemoteException;

  public int chargerNombreLignesGVC(IdSession pIdSession, FiltresGVC pFiltres) throws RemoteException;

  public ArrayList<LigneGvc> chargerListeLigneGvc(IdSession pIdSession, FiltresGVC pFiltres, boolean onLitTout) throws RemoteException;

  public ConditionAchatGVC chargerConditionAchatGVC(IdSession pIdSession, LigneGvc pLigne, Fournisseur pFournisseur)
      throws RemoteException;

  public ConditionAchatGVC chargerConditionAchatGVCdistributeurReference(IdSession pIdSession, LigneGvc pLigne) throws RemoteException;

  public BigDecimal[] chargerPrixAchatArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException;

  public boolean sauverTarifsArticle(IdSession pIdSession, LigneGvc pLigne) throws RemoteException;

  public boolean sauverWorkFlow(IdSession pIdSession, LigneGvc pLigne) throws RemoteException;

  public boolean sauverMiseEnLigneArticle(IdSession pIdSession, LigneGvc pLigne) throws RemoteException;

  public boolean sauverVeilleGSB(IdSession pIdSession, LigneGvc pLigne) throws RemoteException;

  public void sauverFluxMagento(IdSession pIdSession, LigneGvc ligneGvc) throws RemoteException;

  public TarifPriceJet chargerTarifPriceJet(IdSession pIdSession, LigneGvc pLigne) throws RemoteException;

  public boolean sauverPointsWattArticle(IdSession pIdSession, LigneGvc pLigne) throws RemoteException;

  public String chargerAdresseMailProfilAs400(IdSession pIdSession, String pProfil) throws RemoteException;
}
