/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.List;

import ri.serien.libcommun.exploitation.export.CritereExport;
import ri.serien.libcommun.exploitation.export.Export;
import ri.serien.libcommun.exploitation.export.IdExport;
import ri.serien.libcommun.exploitation.export.ListeExport;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service export.
 *
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceExport {
  private static InterfaceServiceExport service;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le service dédié à la gestion des exports.
   * @return Le service dédié à la gestion des exports.
   */
  private static InterfaceServiceExport getService() {
    if (service == null) {
      InterfaceServiceExport interfaceService =
          (InterfaceServiceExport) ManagerClientRMI.recupererService(InterfaceServiceExport.NOMSERVICE);
      
      service = (InterfaceServiceExport) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceExport.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Tester que le service Export est actif.
   */
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger une liste d'identifiants d'export à partir de critères de filtrage.
   * @param pIdSession La session.
   * @param pCritere Les critères de filtrage.
   * @return La liste d'identifiants d'exports.
   */
  public static List<IdExport> chargerListeIdExport(IdSession pIdSession, CritereExport pCritere) {
    try {
      return getService().chargerListeIdExport(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger une liste d'exports à partir d'une liste d'identifiants.
   * Le chargement n'est pas complet c'est à dire que les colonnes ne sont pas chargées.
   * @param pIdSession La session.
   * @param pListeIdExport La liste d'identifiants d'exports.
   * @return La liste d'exports.
   */
  public static ListeExport chargerListeExport(IdSession pIdSession, List<IdExport> pListeIdExport) {
    try {
      return getService().chargerListeExport(pIdSession, pListeIdExport);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger un export à partir d'un identifiant.
   * Le chargement est complet c'est à dire que les colonnes sont également chargées.
   * @param pIdSession La session.
   * @param pListeIdExport La liste d'identifiants d'exports.
   * @return La liste d'exports.
   */
  public static Export chargerExport(IdSession pIdSession, IdExport pIdExport) {
    try {
      return getService().chargerExport(pIdSession, pIdExport);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Sauver un export.
   * @param pIdSession La session.
   * @param pExport L'export.
   * @return L'export.
   */
  public static Export sauverExport(IdSession pIdSession, Export pExport) {
    try {
      return getService().sauverExport(pIdSession, pExport);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
