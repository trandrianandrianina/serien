/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.menu.CriteresMenus;
import ri.serien.libcommun.exploitation.menu.EnregistrementMneMnp;
import ri.serien.libcommun.exploitation.menu.MenuDetail;
import ri.serien.libcommun.exploitation.menu.ProgrammeALancer;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceMenu extends Remote {
  public static final String NOMSERVICE = "ServiceMenu";
  
  public boolean ping() throws RemoteException;
  
  public EnregistrementMneMnp[] chargerListeEnregistrementMneMnp(IdSession pIdSession, CriteresMenus pCriteres) throws RemoteException;
  
  public EnregistrementMneMnp[] chargerListeEnregistrementMneMnpSpecifique(IdSession pIdSession, CriteresMenus pCriteres)
      throws RemoteException;
  
  public EnregistrementMneMnp[] rechercherDetailModulesMenu(IdSession pIdSession, CriteresMenus pCriteres) throws RemoteException;
  
  public EnregistrementMneMnp[] chargerListeEnregistrementMneMnpSpecifiqueDetailSpecifique(IdSession pIdSession, CriteresMenus pCriteres)
      throws RemoteException;
  
  public ArrayList<String> chargerListeBibliothequeSpecifiqueEnvironnement(IdSession pIdSession, Bibliotheque pBibliothequeEnv,
      Bibliotheque pCurlib, char pLettre) throws RemoteException;
  
  public LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> chargerListeMenuDetail(IdSession pIdSession, EnvUser pEnvUSer)
      throws RemoteException;
  
  public LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> chargerListeMenuDetailPersonnalise(IdSession pIdSession, EnvUser pEnvUSer,
      String pNomFichierMenuPerso) throws RemoteException;
  
  public ProgrammeALancer chargerMenuProgramme(IdSession pIdSession, MenuDetail pPointDeMenu, char pLettreEnvironnement)
      throws RemoteException;
  
  public boolean sauverMenuDetail(IdSession pIdSession, MenuDetail pPointDeMenu) throws RemoteException;
  
}
