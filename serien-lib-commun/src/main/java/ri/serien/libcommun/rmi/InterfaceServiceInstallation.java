/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import ri.serien.libcommun.installation.FichierBinaire;
import ri.serien.libcommun.installation.InformationsFichierInstallation;

/**
 * Méthodes spéciales qu'il ne faut pas modifier car elles sont utilisées par la procédure d'installation
 */
public interface InterfaceServiceInstallation extends Remote {
  public static final String NOMSERVICE = "ServiceInstallation";
  
  public boolean ping() throws RemoteException;
  
  public List<String> listerDossierServeur(String pDossierRelatif) throws RemoteException;
  
  public InformationsFichierInstallation retournerInformationsFichier(String pNomFichier) throws RemoteException;
  
  public FichierBinaire demanderFichier(String pNomFichier) throws RemoteException;
}
