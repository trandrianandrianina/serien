/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.net.ConnectException;
import java.rmi.ConnectIOException;
import java.rmi.NoSuchObjectException;
import java.rmi.Remote;
import java.rmi.ServerError;
import java.rmi.UnmarshalException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Gestionnaire des services RMI côte client.
 *
 * Le gestionnaire se charge de créer les connexions aves les services RMI et de mettre à disposition les services RMI auprès du client.
 * Il est nécessaire d'appeler la méthode configurerServeur() pour fournir les paramètres de connexion avant d'utiliser ce gestionnaire.
 *
 * La connexion avec un service RMI s'effectue à la première utilisation (méthode getService...) puis est stockée pour être
 * réutilisée par la suite. La méthode verifierServices() appelle la méthode ping() de tous les services RMI, ce qui a pour effet
 * d'initier les connexions des services RMI et les tester.
 */
public class ManagerClientRMI {
  // Constantes
  private static final int NOMBRE_TENTATIVES = 10;
  
  // Variables
  private static String host = null;
  private static int port = 0;
  
  /**
   * Configurer le nom et le numéro de port du serveur RMI.
   * Cette méthode appele la méthode ping() d'un service afin de vérifier la validité des informations fournies. Plusieurs tentatives
   * sont effectuées pour laisser le temps au serveur de démarrer s'il est en cours de lancement.
   */
  public static void configurerServeur(String pHost, int pPortRegistre) {
    ManagerClientRMI.host = pHost;
    ManagerClientRMI.port = pPortRegistre;
    
    // Tenter de contacter le serveur.
    // Effectuer plusieurs tentatives espacées de 1s. Cela laisse le serveur démarrer s'il a été lancé peu de temps avant le client.
    for (int i = 1; i <= NOMBRE_TENTATIVES; i++) {
      try {
        Trace.info("Tentavive de connexion RMI n°" + i + "...");
        ManagerServiceSession.ping();
        break;
      }
      catch (Exception e) {
        if (i >= NOMBRE_TENTATIVES) {
          throw new MessageErreurException("Impossible de se connecter au serveur " + pHost + ":" + pPortRegistre
              + ".\nVérifier le paramètrage, la connexion réseau et le statut du serveur.");
        }
        // Attendre 1s
        try {
          Thread.sleep(1000);
        }
        catch (InterruptedException e2) {
          // RAS
        }
      }
    }
  }
  
  /**
   * Récupérer le stub client d'un service à partir de son nom.
   */
  protected static Remote recupererService(String nomservice) {
    if (nomservice == null) {
      throw new MessageErreurException("Impossible de récupérer un service RMI car sons nom n'est pas définie");
    }
    if (host == null || port == 0) {
      throw new MessageErreurException("Impossible de récupérer un service RMI car l'adresse du serveur n'est pas définie");
    }
    
    Remote service = null;
    try {
      Registry registry = LocateRegistry.getRegistry(host, port);
      service = registry.lookup(nomservice);
      if (service == null) {
        throw new MessageErreurException(
            "Impossible de récupérer le service RMI : host=" + host + " port=" + port + " service=" + nomservice);
      }
    }
    catch (Exception e) {
      throw new MessageErreurException(e,
          "Impossible de récupérer le service RMI : host=" + host + " port=" + port + " service=" + nomservice);
    }
    
    Trace.info("Service RMI récupéré :" + service.toString());
    return service;
  }
  
  /**
   * Analyser une exception issue d'un appel de méthode RMI pour en extraire les exceptions remarquables.
   * En particulier : MessageErreurException, UnmarshalException, ServerError.
   */
  protected static MessageErreurException genererMessageErreurException(Exception e) {
    // Parcourir la pile d'exceptions pour y rechercher une exception particulière
    for (Throwable cause = e; cause != null; cause = cause.getCause()) {
      // Message d'erreur standard Série N renvoyé par le serveur
      if (cause instanceof MessageErreurException) {
        Trace.erreur(cause, "Message d'erreur envoyé par le serveur");
        return (MessageErreurException) cause;
      }
      // Cette exception se produit si on ne parvient pas à trouver de route réseau pour atteindre le serveur.
      // C'est probablement un problème de connectivité réseau entre le poste de travail et le serveur.
      else if (cause instanceof ConnectIOException) {
        return new MessageErreurException(e,
            "Impossible de terminer l'opération. Le serveur IBM Power Systems n'est pas joignable. Vérifier la connectivité réseau et contacter votre administrateur si le problème persiste.");
      }
      // Cette exception se produit Lorsqu'on arrête le serveur Série N.
      else if (cause instanceof ConnectException) {
        return new MessageErreurException(e,
            "Impossible de terminer l'opération. Il est possible que le serveur Série N ait été stoppé. Contacter votre administrateur si le problème persiste.");
      }
      // Cette exception se produit lorsque le serveur Série N a été arrété puis relancé.
      // RMI parvient à contacter le serveur métier mais celui-ci ne retrouve plus les objets distants attendus.
      else if (cause instanceof NoSuchObjectException) {
        return new MessageErreurException(e,
            "Impossible de terminer l'opération. Il est possible que le serveur Série N ait été redémarré. Merci de relancer Série N Desktop.");
      }
      // Cette exception se produit lorsque les classes n'ont pas la même sérialisation.
      // Cela signifie que les jar installés sur le client et le serveur ne correspondent pas.
      else if (cause instanceof UnmarshalException) {
        return new MessageErreurException(e,
            "Impossible de terminer l'opération. Le client et le serveur Série N ont des versions incompatibles. Merci de contacter le service assistance.");
      }
      // Cette exception se produit pour des erreurs côté serveur
      else if (cause instanceof ServerError) {
        // Cette erreur se produit si le JDK ne trouve pas une méthode dans un .jar
        if (e.getCause() != null && e.getCause() instanceof NoSuchMethodError) {
          return new MessageErreurException(e,
              "Impossible de terminer l'opération. Certains fichiers du serveur ne sont pas à jour. Merci de contacter le service assistance.");
        }
        else {
          return new MessageErreurException(e,
              "Impossible de terminer l'opération. Le serveur Série N a rencontré un problème. Merci de contacter le service assistance.");
        }
      }
    }
    
    // Mettre un message générique s'il n'y a aucune exception connue
    return new MessageErreurException(e,
        "Impossible de terminer l'opération. Il y a eu un problème lors de la connexion au serveur Série N.");
  }
}
