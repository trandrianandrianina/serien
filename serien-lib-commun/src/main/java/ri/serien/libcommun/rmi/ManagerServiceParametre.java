/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.List;

import ri.serien.libcommun.commun.memo.FiltreMemo;
import ri.serien.libcommun.commun.memo.IdMemo;
import ri.serien.libcommun.commun.memo.ListeMemo;
import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.comptabilite.personnalisation.journal.CriteresRechercheJournaux;
import ri.serien.libcommun.comptabilite.personnalisation.journal.ListeJournal;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.DocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.ListeDocumentStocke;
import ri.serien.libcommun.exploitation.parametreavance.CritereParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.IdParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.ListeParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.ParametreAvance;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.ListeCategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.civilite.ListeCivilite;
import ri.serien.libcommun.exploitation.personnalisation.flux.CriterePersonnalisationFlux;
import ri.serien.libcommun.exploitation.personnalisation.flux.ListePersonnalisationFlux;
import ri.serien.libcommun.exploitation.personnalisation.zonepersonnaliseeexploitation.ListeZonePersonnaliseeExploitation;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.exploitation.securiteutilisateurgescom.SecuriteUtilisateurGescom;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.blocnotes.IdBlocNote;
import ri.serien.libcommun.gescom.commun.devise.CriteresRechercheDevise;
import ri.serien.libcommun.gescom.commun.devise.ListeDevise;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.personnalisation.acheteur.CriteresRechercheAcheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.ListeAcheteur;
import ri.serien.libcommun.gescom.personnalisation.affaire.CriteresRechercheAffaires;
import ri.serien.libcommun.gescom.personnalisation.affaire.ListeAffaire;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.CriteresRechercheCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.ListeCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.CriteresRechercheCategoriesClients;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.ListeCategorieClient;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.CriteresRechercheCategoriesFournisseur;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.ListeCategorieFournisseur;
import ri.serien.libcommun.gescom.personnalisation.codepostal.CriteresCodePostal;
import ri.serien.libcommun.gescom.personnalisation.codepostal.ListeCodePostal;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.CritereCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.ListeCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.CriteresConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.ListeConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.famille.CritereFamille;
import ri.serien.libcommun.gescom.personnalisation.famille.ListeFamille;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.CritereFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.ListeFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.groupe.CritereGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupe.ListeGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.CritereGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.ListeGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.CritereLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.ListeLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.magasin.CritereMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.CriteresModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.motifretour.ListeMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.ListeParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.CriteresParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.ListeParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.pays.CriteresPays;
import ri.serien.libcommun.gescom.personnalisation.pays.ListePays;
import ri.serien.libcommun.gescom.personnalisation.reglement.CritereModeReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.ListeModeReglement;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.CriteresRechercheSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.ListeSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.CritereSousFamilles;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.personnalisation.tarif.CriteresRechercheTarifs;
import ri.serien.libcommun.gescom.personnalisation.tarif.ListeTarif;
import ri.serien.libcommun.gescom.personnalisation.tarif.Tarif;
import ri.serien.libcommun.gescom.personnalisation.transporteur.CriteresRechercheTransporteurs;
import ri.serien.libcommun.gescom.personnalisation.transporteur.ListeTransporteur;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.ListeTypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.ListeTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.CriteresRechercheTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.ListeTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typestockagemagasin.ListeTypeStockageMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.CriteresRechercheUnites;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.Unite;
import ri.serien.libcommun.gescom.personnalisation.vendeur.CriteresRechercheVendeurs;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.CritereZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ListeZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.CritereCategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.ListeCategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.vente.commune.CritereCommune;
import ri.serien.libcommun.gescom.vente.commune.ListeCommune;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service paramètre.
 * 
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les
 * convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un
 * résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceParametre {
  private static InterfaceServiceParametre service;
  
  /**
   * Retourner service dédié à la gestion des paramètres.
   */
  private static InterfaceServiceParametre getService() {
    if (service == null) {
      InterfaceServiceParametre interfaceService =
          (InterfaceServiceParametre) ManagerClientRMI.recupererService(InterfaceServiceParametre.NOMSERVICE);
      
      service = (InterfaceServiceParametre) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceParametre.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Retourne les informations d'un utilisateur en gescom.
   */
  public static UtilisateurGescom chargerUtilisateurGescom(IdSession pIdSession, String pProfil, Bibliotheque pCurLib,
      IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerUtilisateurGescom(pIdSession, pProfil, pCurLib, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des groupes suivant certains critères.
   */
  public static ListeGroupe chargerListeGroupe(IdSession pIdSession, CritereGroupe pCritere) {
    try {
      return getService().chargerListeGroupe(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des familles suivant certains critères.
   */
  public static ListeFamille chargerListeFamille(IdSession pIdSession, CritereFamille pCriteres) {
    try {
      return getService().chargerListeFamille(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des sous-familles suivant certains critères.
   */
  public static ListeSousFamille chargerListeSousFamille(IdSession pIdSession, CritereSousFamilles pCriteres) {
    try {
      return getService().chargerListeSousFamille(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des magasins suivant certains critères.
   */
  public static ListeMagasin chargerListeMagasin(IdSession pIdSession, CritereMagasin pCriteres) {
    try {
      return getService().chargerListeMagasin(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Lit les types de facturation pour un établissement donné.
   */
  public static ListeTypeFacturation chargerListeTypeFacturation(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerListeTypeFacturation(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Lit les motifs de retour pour un établissement donné.
   */
  public static ListeMotifRetour chargerListeMotifRetour(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerListeMotifRetour(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Rechercher les types de stockages magasin pour un établissement donné.
   */
  public static ListeTypeStockageMagasin chargerListeTypeStockageMagasin(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerListeTypeStockageMagasin(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche les unités suivant certains critères.
   */
  public static ListeUnite chargerListeUnite(IdSession pIdSession, CriteresRechercheUnites pCriteres) {
    try {
      return getService().chargerListeUnite(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des catégories clients suivant certains critères.
   */
  public static ListeCategorieClient chargerListeCategorieClient(IdSession pIdSession, CriteresRechercheCategoriesClients pCriteres) {
    try {
      return getService().chargerListeCategorieClient(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des catégories fournisseur suivant certains critères.
   */
  public static ListeCategorieFournisseur chargerListeCategorieFournisseur(IdSession pIdSession,
      CriteresRechercheCategoriesFournisseur pCriteres) {
    try {
      return getService().chargerListeCategorieFournisseur(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger le tabeau de tous les établissements.
   */
  public static ListeEtablissement chargerListeEtablissement(IdSession pIdSession) {
    try {
      return getService().chargerListeEtablissement(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger un établissement donné à partir de son id.
   */
  public static Etablissement chargerEtablissement(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerEtablissement(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche les transporteurs suivant certains critères.
   */
  public static ListeTransporteur chargerListeTransporteur(IdSession pIdSession, CriteresRechercheTransporteurs pCriteres) {
    try {
      return getService().chargerListeTransporteur(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche les lei de transport suivant certains critères.
   */
  public static ListeLieuTransport chargerListeLieuTransport(IdSession pIdSession, CritereLieuTransport pCriteres) {
    try {
      return getService().chargerListeLieuTransport(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des code postal suivant certains critères.
   */
  public static ListeCodePostal chargerListeCodePostal(IdSession pIdSession, CriteresCodePostal pCriteres) {
    try {
      return getService().chargerListeCodePostal(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des devises suivant certains critères.
   */
  public static ListeDevise chargerListeDevise(IdSession pIdSession, CriteresRechercheDevise pCriteres) {
    try {
      return getService().chargerListeDevise(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des type de gratuit suivant certains critères.
   */
  public static ListeTypeGratuit chargerListeTypeGratuit(IdSession pIdSession, CriteresRechercheTypeGratuit pCriteres) {
    try {
      return getService().chargerListeTypeGratuit(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche les sections analytique suivant certains critères
   */
  public static ListeSectionAnalytique chargerListeSectionAnalytique(IdSession pIdSession, CriteresRechercheSectionAnalytique pCriteres) {
    try {
      return getService().chargerListeSectionAnalytique(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche les groupes de conditions de ventes suivant certains critères
   */
  public static ListeGroupeConditionVente chargerListeGroupeConditionVente(IdSession pIdSession, CritereGroupeConditionVente pCriteres) {
    try {
      return getService().chargerListeGroupeConditionVente(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche les affaires suivant certains critères
   */
  public static ListeAffaire chargerListeAffaire(IdSession pIdSession, CriteresRechercheAffaires pCriteres) {
    try {
      return getService().chargerListeAffaire(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche les canaux de vente suivant certains critères
   */
  public static ListeCanalDeVente chargerListeCanalDeVente(IdSession pIdSession, CriteresRechercheCanalDeVente pCriteres) {
    try {
      return getService().chargerListeCanalDeVente(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des règlements suivant certains critères.
   */
  public static ListeModeReglement chargerListeModeReglement(IdSession pIdSession, CritereModeReglement pCriteres) {
    try {
      return getService().chargerListeModeReglement(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des acheteurs suivant certains critères.
   */
  public static ListeVendeur chargerListeVendeur(IdSession pIdSession, CriteresRechercheVendeurs pCriteres) {
    try {
      return getService().chargerListeVendeur(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des acheteurs suivant certains critères.
   */
  public static ListeAcheteur chargerListeAcheteur(IdSession pIdSession, CriteresRechercheAcheteur pCriteres) {
    try {
      return getService().chargerListeAcheteur(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des tarifs suivant certains critères.
   */
  public static ListeTarif chargerListeTarif(IdSession pIdSession, CriteresRechercheTarifs pCriteres) {
    try {
      return getService().chargerListeTarif(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Lire un tarif.
   */
  public static Tarif chargerTarif(IdSession pIdSession, IdEtablissement pIdEtablissement, String pCodeTarif) {
    try {
      return getService().chargerTarif(pIdSession, pIdEtablissement, pCodeTarif);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger la liste des paramètres systèmes d'un établissement.
   */
  public static ListeParametreSysteme chargerListeParametreSysteme(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerListeParametreSysteme(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des civilités suivant certains critères.
   */
  public static ListeCivilite chargerListeCivilite(IdSession pIdSession) {
    try {
      return getService().chargerListeCivilite(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des catégories contact suivant certains critères.
   */
  public static ListeCategorieContact chargerListeCategorieContact(IdSession pIdSession) {
    try {
      return getService().chargerListeCategorieContact(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des personnalisations de flux suivant certains critères.
   */
  public static ListePersonnalisationFlux chargerListePersonnalisationFlux(IdSession pIdSession, CriterePersonnalisationFlux pCritere) {
    try {
      return getService().chargerListePersonnalisationFlux(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des modes d'expédition suivant certains critères.
   */
  public static ListeModeExpedition chargerListeModeExpedition(IdSession pIdSession, CriteresModeExpedition pCriteres) {
    try {
      return getService().chargerListeModeExpedition(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charge les données d'un mode d'expédition
   */
  public static ModeExpedition chargerModeExpedition(IdSession pIdSession, ModeExpedition pModeExpedition) {
    try {
      return getService().chargerModeExpedition(pIdSession, pModeExpedition);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche les journaux suivant certains critères.
   */
  public static ListeJournal chargerListeJournal(IdSession pIdSession, CriteresRechercheJournaux pCriteres) {
    try {
      return getService().chargerListeJournal(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Retourne la liste des communes suivant les critères donnés.
   */
  public static ListeCommune chargerListeCommune(IdSession pIdSession, CritereCommune pCriteres) {
    try {
      return getService().chargerListeCommune(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des mémos suivant des critères données.
   */
  public static ListeMemo chargerListeMemo(IdSession pIdSession, FiltreMemo pFiltreMemo) {
    try {
      return getService().chargerListeMemo(pIdSession, pFiltreMemo);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Lit un mémo.
   */
  public static Memo chargerMemo(IdSession pIdSession, IdMemo pIdMemo) {
    try {
      return getService().chargerMemo(pIdSession, pIdMemo);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Ecrit un mémo.
   */
  public static void sauverMemo(IdSession pIdSession, Memo pMemo) {
    try {
      getService().sauverMemo(pIdSession, pMemo);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Supprime un mémo.
   */
  public static void supprimerMemo(IdSession pIdSession, IdMemo pIdMemo) {
    try {
      getService().supprimerMemo(pIdSession, pIdMemo);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Lit un bloc-notes.
   */
  public static BlocNote chargerBlocNote(IdSession pIdSession, IdBlocNote pIdBlocnotes) {
    try {
      return getService().chargerBlocNote(pIdSession, pIdBlocnotes);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Ecrit un bloc-notes.
   */
  public static void sauverBlocNote(IdSession pIdSession, BlocNote pBlocNote) {
    try {
      getService().sauverBlocNote(pIdSession, pBlocNote);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Supprime un bloc-notes.
   */
  public static void supprimerBlocNote(IdSession pIdSession, IdBlocNote pIdBlocNote) {
    try {
      getService().supprimerBlocNote(pIdSession, pIdBlocNote);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Retourne la commune de la zone géographique à l'aide du code établissement, du code magasin, du code postal et de la ville.
   */
  public static ListeCommuneZoneGeographique chargerListeCommuneZoneGeographique(IdSession pIdSession,
      IdZoneGeographique pIdZoneGeographique) {
    try {
      return getService().chargerListeCommuneZoneGeographique(pIdSession, pIdZoneGeographique);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Retourne les communes des zone géographique suivant certain critères.
   */
  public static ListeCommuneZoneGeographique chargerListeCommuneZoneGeographique(IdSession pIdSession,
      CritereCommuneZoneGeographique pCriteres) {
    try {
      return getService().chargerListeCommuneZoneGeographique(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des zones géographique clients suivant certains critères.
   */
  public static ListeZoneGeographique chargerListeZoneGeographique(IdSession pIdSession, CritereZoneGeographique pCriteres) {
    try {
      return getService().chargerListeZoneGeographique(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche une liste d'IdZoneGeographique suivant certains critères.
   */
  public static List<IdZoneGeographique> chargerListeIdZoneGeographique(IdSession pIdSession, CritereZoneGeographique pCriteres) {
    try {
      return getService().chargerListeIdZoneGeographique(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Recherche des pays clients suivant certains critères.
   */
  public static ListePays chargerListePays(IdSession pIdSession, CriteresPays pCriteres) {
    try {
      return getService().chargerListePays(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Retourne une unité sur la base de son identifiant
   */
  public static Unite chargerUnite(IdSession pIdSession, IdUnite pIdUnite) {
    try {
      return getService().chargerUnite(pIdSession, pIdUnite);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Retourne une liste de catégories de zones personnalisées
   */
  public static ListeCategorieZonePersonnalisee chargerListeCategorieZonePersonnalisee(IdSession pIdSession,
      CritereCategorieZonePersonnalisee pCritere) {
    try {
      return getService().chargerListeCategorieZonePersonnalisee(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger les sécurités de la gescom d'un utilisateur.
   */
  public static SecuriteUtilisateurGescom chargerSecuriteGescom(IdSession pIdSession, IdEtablissement pIdEtablissement, String pProfil) {
    try {
      return getService().chargerSecuriteGescom(pIdSession, pIdEtablissement, pProfil);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeTypeAvoir chargerListeTypeAvoir(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerListeTypeAvoir(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger une liste de document stocké.
   */
  public static ListeDocumentStocke chargerListeDocumentStocke(IdSession pIdSession, CritereDocumentStocke pCriteres) {
    try {
      return getService().chargerListeDocumentStocke(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charge le chemin complet d'un document Stocke
   */
  public static String chargerCheminCompletDocumentStocke(IdSession pIdSession, DocumentStocke pDocumentStocke) {
    try {
      return getService().chargerCheminCompletDocumentStocke(pIdSession, pDocumentStocke);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charge la liste des zones personnalisées d'exploitation
   */
  public static ListeZonePersonnaliseeExploitation chargerListeZonePersonnaliseeExploitation(IdSession pIdSession,
      CritereCategorieZonePersonnalisee pCritere) {
    try {
      return getService().chargerListeZonePersonnaliseeExploitation(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charge la liste des conditions de commissionnement
   */
  public static ListeConditionCommissionnement chargerListeConditionCommissionnement(IdSession pIdSession,
      CriteresConditionCommissionnement pCritere) {
    try {
      return getService().chargerListeConditionCommissionnement(pIdSession, pCritere);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Vérifie si un paramètre système donné est actif sur au moins un établissement de la base de données.
   */
  public static boolean isParametreSystemeActifSurUnEtablissement(IdSession pIdSession, EnumParametreSysteme pEnumParametreSysteme) {
    try {
      return getService().isParametreSystemeActifSurUnEtablissement(pIdSession, pEnumParametreSysteme);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -----------
  // PARAMETRES AVANCES
  // -----------
  
  /**
   * Charge une liste d'identifiants des paramètres avancés.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCriteres Critères de recherche de paramètres avancés.
   * @return Liste d'identifiants de paramètres avancés.
   */
  public static List<IdParametreAvance> chargerListeIdParametreAvance(IdSession pIdSession, CritereParametreAvance pCriteres) {
    try {
      return getService().chargerListeIdParametreAvance(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charge une liste de paramètres avancés à partir de critères
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCriteres Critères de recherche des paramètres avancés.
   * @return Liste de paramètres avancés.
   */
  public static ListeParametreAvance chargerListeParametreAvance(IdSession pIdSession, CritereParametreAvance pCriteres) {
    try {
      return getService().chargerListeParametreAvance(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charge une liste de paramètres avancés à partir d'une liste d'identifiants
   * 
   * @param pIdSession Identifiant de la session.
   * @param pListeIdParametreAvance Liste d'identifiants des paramètres avancés à charger.
   * @return Liste de paramètres avancés.
   */
  public static ListeParametreAvance chargerListeParametreAvance(IdSession pIdSession, List<IdParametreAvance> pListeIdParametreAvance) {
    try {
      return getService().chargerListeParametreAvance(pIdSession, pListeIdParametreAvance);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Créer ou modifier un paramètre avancé en base.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pParametreAvance Paramètre avancé à persister en base.
   */
  public static void sauverParametreAvance(IdSession pIdSession, ParametreAvance pParametreAvance) {
    try {
      getService().sauverParametreAvance(pIdSession, pParametreAvance);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Supprimer un paramètre avancé de la base.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pParametreAvance Paramètre avancé à supprimer de la base
   */
  public static void supprimerParametreAvance(IdSession pIdSession, ParametreAvance pParametreAvance) {
    try {
      getService().supprimerParametreAvance(pIdSession, pParametreAvance);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Retourner l'identifiant maximal existant pour les paramètres avancés.
   * 
   * @param pIdSession
   * @return Identifiant maximal existant.
   */
  public static int getIdMaxParametreAvance(IdSession pIdSession) {
    try {
      return getService().getIdMaxParametreAvance(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger une liste de participations aux frais d'expédition.
   */
  public static ListeParticipationFraisExpedition chargerListeParticipationFraisExpedition(IdSession pIdSession,
      CriteresParticipationFraisExpedition pCriteres) {
    try {
      return getService().chargerListeParticipationFraisExpedition(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  /**
   * Charger une liste de formules de prix.
   */
  public static ListeFormulePrix chargerListeFormulePrix(IdSession pIdSession, CritereFormulePrix pCriteres) {
    try {
      return getService().chargerListeFormulePrix(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
