/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.menu.CriteresMenus;
import ri.serien.libcommun.exploitation.menu.EnregistrementMneMnp;
import ri.serien.libcommun.exploitation.menu.MenuDetail;
import ri.serien.libcommun.exploitation.menu.ProgrammeALancer;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service menu.
 *
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceMenu {
  private static InterfaceServiceMenu service;
  
  /**
   * Retourne le service ServiceMenu.
   */
  private static InterfaceServiceMenu getService() {
    if (service == null) {
      InterfaceServiceMenu interfaceService = (InterfaceServiceMenu) ManagerClientRMI.recupererService(InterfaceServiceMenu.NOMSERVICE);
      
      service = (InterfaceServiceMenu) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceMenu.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static EnregistrementMneMnp[] chargerListeEnregistrementMneMnp(IdSession pIdSession, CriteresMenus pCriteres) {
    try {
      return getService().chargerListeEnregistrementMneMnp(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static EnregistrementMneMnp[] chargerListeEnregistrementMneMnpSpecifique(IdSession pIdSession, CriteresMenus pCriteres) {
    try {
      return getService().chargerListeEnregistrementMneMnpSpecifique(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static EnregistrementMneMnp[] rechercherDetailModulesMenu(IdSession pIdSession, CriteresMenus pCriteres) {
    try {
      return getService().rechercherDetailModulesMenu(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static EnregistrementMneMnp[] chargerListeEnregistrementMneMnpSpecifiqueDetailSpecifique(IdSession pIdSession,
      CriteresMenus pCriteres) {
    try {
      return getService().chargerListeEnregistrementMneMnpSpecifiqueDetailSpecifique(pIdSession, pCriteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ArrayList<String> chargerListeBibliothequeSpecifiqueEnvironnement(IdSession pIdSession, Bibliotheque pBibliothequeEnv,
      Bibliotheque pCurlib, char pLettre) {
    try {
      return getService().chargerListeBibliothequeSpecifiqueEnvironnement(pIdSession, pBibliothequeEnv, pCurlib, pLettre);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> chargerListeMenuDetail(IdSession pIdSession, EnvUser pEnvUser) {
    try {
      return getService().chargerListeMenuDetail(pIdSession, pEnvUser);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> chargerListeMenuDetailPersonnalise(IdSession pIdSession,
      EnvUser pEnvUser, String pNomFichierMenuPerso) {
    try {
      return getService().chargerListeMenuDetailPersonnalise(pIdSession, pEnvUser, pNomFichierMenuPerso);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ProgrammeALancer chargerMenuProgramme(IdSession pIdSession, MenuDetail pPointDeMenu, char pLettreEnvironnement) {
    try {
      return getService().chargerMenuProgramme(pIdSession, pPointDeMenu, pLettreEnvironnement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean sauverMenuDetail(IdSession pIdSession, MenuDetail pPointDeMenu) {
    try {
      return getService().sauverMenuDetail(pIdSession, pPointDeMenu);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
}
