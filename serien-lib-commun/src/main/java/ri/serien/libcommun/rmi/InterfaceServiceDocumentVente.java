/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.math.BigDecimal;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.exploitation.dataarea.LdaSerien;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.reapprovisionnement.ReapprovisionnementClient;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.adressedocument.AdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.CritereAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.IdAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.ListeAdresseDocument;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.Negociation;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.ListeLienLigne;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.motifretour.IdMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.IdTypeAvoir;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.avoir.ListeAvoir;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeCarnetBonCour;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DemandeDeblocage;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.OptionsEditionVente;
import ri.serien.libcommun.gescom.vente.document.OptionsValidation;
import ri.serien.libcommun.gescom.vente.document.PrixFlash;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeIdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneRetourArticle;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.Regroupement;
import ri.serien.libcommun.gescom.vente.prodevis.CriteresRechercheProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.DescriptionProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.EnumStatutProDevis;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.gescom.vente.sipe.CritereSipe;
import ri.serien.libcommun.gescom.vente.sipe.EnumTypePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.IdPlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.ListePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.PlanPoseSipe;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceDocumentVente extends Remote {
  public static final String NOMSERVICE = "ServiceDocumentVente";
  
  public boolean ping() throws RemoteException;
  
  public IdDocumentVente creerDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement)
      throws RemoteException;
  
  public List<IdDocumentVente> chargerListeIdDocumentVente(IdSession pIdSession, CritereDocumentVente pCritereDocumentVente)
      throws RemoteException;
  
  public ListeDocumentVenteBase chargerListeDocumentVenteBase(IdSession pIdSession, List<IdDocumentVente> pListeIdDocumentVente)
      throws RemoteException;
  
  public List<IdDocumentVente> chargerListeIdDocumentAvoir(IdSession pIdSession, IdDocumentVente pIdDocument) throws RemoteException;
  
  public DocumentVente chargerEnteteDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException;
  
  public List<DocumentVente> chargerEnteteListeDocumentVente(IdSession pIdSession, List<IdDocumentVente> pListeIdDocumentVente)
      throws RemoteException;
  
  public void sauverEnteteDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement) throws RemoteException;
  
  public void controlerDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement) throws RemoteException;
  
  public OptionsValidation controlerFinDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente,
      OptionsValidation pOptionsValidation, Date pDateTraitement) throws RemoteException;
  
  public void chiffrerDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pNePasReactualiserPrix)
      throws RemoteException;
  
  public DocumentVente validerDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, OptionsValidation pOptions,
      Date pDateTraitement) throws RemoteException;
  
  public void annulerDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException;
  
  public String editerDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pDocumentModifiable,
      Date pDateTraitement, OptionsEditionVente pOptions, LdaSerien pLdaSerien) throws RemoteException;
  
  public LigneVente initialiserLigneVente(IdSession pIdSession, IdLigneVente pIdLigneVente, IdArticle pIdArticle, Date pDateTraitement)
      throws RemoteException;
  
  public AlertesLigneDocument controlerLigneDocumentVente(IdSession pIdSession, LigneVente pLigneVente, Date pDateTraitementp)
      throws RemoteException;
  
  public void sauverLigneVente(IdSession pIdSession, LigneVente pLigneVente) throws RemoteException;
  
  public boolean modifierLigneDocumentVente(IdSession pIdSession, LigneVente pLigneVente) throws RemoteException;
  
  public boolean supprimerLigneDocumentVente(IdSession pIdSession, LigneVente pLigneVente) throws RemoteException;
  
  public List<IdLigneVente> chargerIdLigneVenteParCodeArticle(IdSession pIdSession, CritereLigneVente pCriteres) throws RemoteException;
  
  public BlocNote chargerPetitBlocNoteDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException;
  
  public int sauverPetitBlocNoteDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, BlocNote pBlocNote)
      throws RemoteException;
  
  public ListeLigneVente chargerListeLigneVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, CritereLigneVente pCriteres,
      boolean isTTC) throws RemoteException;
  
  public Regroupement chargerRegroupement(IdSession pIdSession, IdLigneVente pIdLigneVente, Regroupement pRegroupement)
      throws RemoteException;
  
  public void sauverRegroupement(IdSession pIdSession, Regroupement pRegroupement) throws RemoteException;
  
  public void supprimerRegroupement(IdSession pIdSession, IdLigneVente pIdLigneVente) throws RemoteException;
  
  public void sauverAdresseDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, Adresse pAdresseFacturation,
      Adresse pAdresseLivraison) throws RemoteException;
  
  public IdDocumentVente dupliquerDocument(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pAvecRecalculPrix)
      throws RemoteException;
  
  public IdDocumentVente creerAvoir(IdSession pIdSession, IdDocumentVente pIdDocumentVente, IdTypeAvoir pIdTypeAvoir)
      throws RemoteException;
  
  public IdDocumentVente dupliquerDocumentEtatSuivant(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pAvecRecalculPrix)
      throws RemoteException;
  
  public ListeReglement chargerListeReglement(IdSession pIdSession, IdDocumentVente pIdDocumentVente, Date pDateTraitement)
      throws RemoteException;
  
  public void sauverListeReglement(IdSession pIdSession, IdDocumentVente pIdDocumentVente, ListeReglement pListeReglements)
      throws RemoteException;
  
  public void sauverDocumentVenteReglementMultiple(IdSession pIdSession, boolean pViderListe, IdDocumentVente pIdDocumentVente)
      throws RemoteException;
  
  public void supprimerReglement(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException;
  
  public void solderReliquat(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException;
  
  public PrixFlash chargerPrixFlash(IdSession pIdSession, IdLigneVente pIdLigneVente, IdArticle pIdArticle, IdClient pIdClient)
      throws RemoteException;
  
  public void sauverDocumentVenteClientModifie(IdSession pIdSession, IdDocumentVente pIdDocumentVente, IdClient pIdClient,
      boolean pChangerClientFacture, boolean pChangerClientLivre) throws RemoteException;
  
  public DemandeDeblocage chargerDemandeDeblocage(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException;
  
  public void sauverDemandeDeblocage(IdSession pIdSession, IdDocumentVente pIdDocumentVente, String pProfil) throws RemoteException;
  
  public void supprimerDemandeDeblocage(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException;
  
  public DemandeDeblocage chargerDemandeDeblocageVenteSousPrv(IdSession pIdSession, IdDocumentVente pIdDocumentVente)
      throws RemoteException;
  
  public void sauverDemandeDeblocageVenteSousPrv(IdSession pIdSession, IdDocumentVente pIdDocumentVente, String pProfil)
      throws RemoteException;
  
  public void supprimerDemandeDeblocageVenteSousPrv(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException;
  
  public int sauverLigneVenteDocumentAvecExtraction(IdSession pIdSession, IdLigneVente pIdLigneVenteOrigine,
      IdLigneVente pIdLigneVenteDestination, int pTypeTraitement, BigDecimal pQuantiteDemandee, boolean pAvecRecalculPrix)
      throws RemoteException;
  
  public void validerExtractionDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement,
      OptionsValidation pOptions) throws RemoteException;
  
  public int sauverLigneVenteCommentaire(IdSession pIdSession, LigneVente pLigneVente, DocumentVente pDocumentVente,
      boolean pIsEditionSurFactures, boolean pConserverSurAchat) throws RemoteException;
  
  public LigneVente chargerLigneVenteCommentaire(IdSession pIdSession, IdLigneVente pIdLigneVente) throws RemoteException;
  
  public void initialiserPrixClient(IdSession pIdSession, Client pClient) throws RemoteException;
  
  public List<DescriptionProDevis> chargerListeDescriptionProDevis(IdSession pIdSession, CriteresRechercheProDevis pCriteres)
      throws RemoteException;
  
  public IdDocumentVente importerDevisProDevis(IdSession pIdSession, DescriptionProDevis pDescription, IdClient pIdClient)
      throws RemoteException;
  
  public void modifierStatutProDevis(IdSession pIdSession, DescriptionProDevis pDescription, EnumStatutProDevis pNouveauStatut)
      throws RemoteException;
  
  public Negociation chargerNegociation(IdSession pIdSession, LigneVente pLigneVente, Date pDateTraitement, String pOptionTraitement)
      throws RemoteException;
  
  public void sauverNegociation(IdSession pIdSession, LigneVente pLigneVente, Date pDateTraitement, String pOptionTraitement)
      throws RemoteException;
  
  public ConditionAchat chargerConditionAchat(IdSession pIdSession, LigneVente pLigneVente) throws RemoteException;
  
  public ConditionAchat chargerNegociationAchat(IdSession pIdSession, LigneAchat pLigneAchat) throws RemoteException;
  
  public ListeLigneRetourArticle chargerListeLigneRetourArticle(IdSession pIdSession, BigDecimal pQuantite, Article pArticle,
      Client pClient, ListeLigneRetourArticle pListeOrigine) throws RemoteException;
  
  public void sauverLigneRetourArticle(IdSession pIdSession, BigDecimal pQuantiteRetour, BigDecimal pPrixNetRetour,
      IdMotifRetour pMotifRetour, LigneVente pLigne, DocumentVente pDocument) throws RemoteException;
  
  public ListeAvoir chargerListeAvoir(IdSession pIdSession, IdClient pIdClient) throws RemoteException;
  
  public List<ReapprovisionnementClient> chargerListeArticlesAApprovisionner(IdSession pIdSession, List<IdLigneVente> pListeIdLigneVente,
      ParametresLireArticle pParametres) throws RemoteException;
  
  public List<IdLigneVente> chargerListeIdLigneVenteAApprovisionner(IdSession pIdSession, CritereLigneVente pCriteres)
      throws RemoteException;
  
  public ListeLienLigne chargerListeLienLigne(IdSession pIdSession, IdLienLigne pIdLigneOrigine) throws RemoteException;
  
  public List<ListeLienLigne> chargerListeLienLigne(IdSession pIdSession, ListeLigneVenteBase pListeLigneVenteBase)
      throws RemoteException;
  
  public ListeIdLigneVente chargerToutesIdLigneVenteParCodeArticle(IdSession pIdSession, CritereLigneVente pCriteres)
      throws RemoteException;
  
  public ListeLigneVenteBase chargerListeLigneVenteBase(IdSession pIdSession, List<IdLigneVente> pListeIdLigneVente)
      throws RemoteException;
  
  public ListeIdLigneVente chargerListeIdLigneVente(IdSession pIdSession, CritereLigneVente pCriteres) throws RemoteException;
  
  public Boolean existeArticleDirectUsine(IdSession pIdSession, ListeLigneVente plisteLigneVente) throws RemoteException;
  
  public void genererCNVDepuisDocument(IdSession pIdSession, IdLigneVente pIdLigneVente, Date pDateDebut, Date pDateFin,
      String pReference) throws RemoteException;
  
  public void changerClientDocument(IdSession pIdSession, IdDocumentVente pIdDocumentVente, IdClient pIdClientOrigine,
      boolean isAvecRecalcul) throws RemoteException;
  
  public ListeDocumentVenteBase chargerListeAvoir(IdSession pIdSession, List<IdDocumentVente> pListeIdDocumentAvoir)
      throws RemoteException;
  
  public List<IdDocumentVente> chargerListeIdFactureNonReglee(IdSession pIdSession, CritereDocumentVente pCritereDocumentVente)
      throws RemoteException;
  
  public CarnetBonCour sauverCarnetBonCour(IdSession pIdSession, CarnetBonCour pCarnetBonCour) throws RemoteException;
  
  public List<IdCarnetBonCour> chargerListeIdCarnetBonCour(IdSession pIdSession, CritereCarnetBonCour pCritereCarnetBonCour)
      throws RemoteException;
  
  public List<IdCarnetBonCour> chargerListeIdCarnetBonCour(IdSession pIdSession, CritereBonCour critereBonCour) throws RemoteException;
  
  public List<IdBonCour> chargerListeIdBonCour(IdSession pIdSession, CritereBonCour critereBonCour) throws RemoteException;
  
  public ListeCarnetBonCour chargerListeCarnetBonCour(IdSession pIdSession, List<IdCarnetBonCour> pListeIdCarnetBonCour)
      throws RemoteException;
  
  public ListeBonCour chargerListeBonCour(IdSession pIdSession, List<IdBonCour> pListeIdBonCour) throws RemoteException;
  
  public CarnetBonCour chargerCarnetBonCour(IdSession pIdSession, IdCarnetBonCour pIdCarnetBonCour) throws RemoteException;
  
  public void modifierBonCour(IdSession pIdSession, BonCour pBonCour) throws RemoteException;
  
  public BonCour chargerBonCour(IdSession pIdSession, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, int pNumeroBonCour)
      throws RemoteException;
  
  public List<IdAdresseDocument> chargerListeIdAdresseDocument(IdSession pIdSession, CritereAdresseDocument pCritere)
      throws RemoteException;
  
  public ListeAdresseDocument chargerListeAdresseDocument(IdSession pIdSession, List<IdAdresseDocument> pListeIdAdresseDocument)
      throws RemoteException;
  
  public void creerAdresseDocument(IdSession pIdSession, AdresseDocument pAdresseDocument) throws RemoteException;
  
  public int chercherPremierNumeroAdresseLibre(IdSession pIdSession, IdDocumentVente pIdDocument) throws RemoteException;
  
  public void supprimerAdresseDocument(IdSession pIdSession, AdresseDocument pAdresseDocument) throws RemoteException;
  
  public void sauverAdresseDocument(IdSession pIdSession, AdresseDocument pAdresseDocument) throws RemoteException;
  
  public List<IdPlanPoseSipe> chargerListeIdPlanPoseSipe(IdSession pIdSession, CritereSipe pCritereSipe, boolean pMiseAJourDepuisIfs)
      throws RemoteException;
  
  public ListePlanPoseSipe chargerListePlanPoseSipe(IdSession pIdSession, List<IdPlanPoseSipe> pListeIpPlanPose) throws RemoteException;
  
  public void importerPlanPoseSipe(IdSession pIdSession, PlanPoseSipe pPlanPoseSipe, IdDocumentVente pIdDocument) throws RemoteException;
  
  public String retournerDossierSipe(IdSession pIdSession, EnumTypePlanPoseSipe pTypePlanPoseSipe, boolean pDossierSauvegarde)
      throws RemoteException;
  
  public void supprimerFichierSipe(IdSession pIdSession, PlanPoseSipe pPlanPoseSipe) throws RemoteException;
}
