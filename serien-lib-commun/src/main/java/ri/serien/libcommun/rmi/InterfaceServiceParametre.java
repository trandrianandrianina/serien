/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import ri.serien.libcommun.commun.memo.FiltreMemo;
import ri.serien.libcommun.commun.memo.IdMemo;
import ri.serien.libcommun.commun.memo.ListeMemo;
import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.comptabilite.personnalisation.journal.CriteresRechercheJournaux;
import ri.serien.libcommun.comptabilite.personnalisation.journal.ListeJournal;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.DocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.ListeDocumentStocke;
import ri.serien.libcommun.exploitation.parametreavance.CritereParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.IdParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.ListeParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.ParametreAvance;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.ListeCategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.civilite.ListeCivilite;
import ri.serien.libcommun.exploitation.personnalisation.flux.CriterePersonnalisationFlux;
import ri.serien.libcommun.exploitation.personnalisation.flux.ListePersonnalisationFlux;
import ri.serien.libcommun.exploitation.personnalisation.zonepersonnaliseeexploitation.ListeZonePersonnaliseeExploitation;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.exploitation.securiteutilisateurgescom.SecuriteUtilisateurGescom;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.blocnotes.IdBlocNote;
import ri.serien.libcommun.gescom.commun.devise.CriteresRechercheDevise;
import ri.serien.libcommun.gescom.commun.devise.ListeDevise;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.personnalisation.acheteur.CriteresRechercheAcheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.ListeAcheteur;
import ri.serien.libcommun.gescom.personnalisation.affaire.CriteresRechercheAffaires;
import ri.serien.libcommun.gescom.personnalisation.affaire.ListeAffaire;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.CriteresRechercheCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.ListeCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.CriteresRechercheCategoriesClients;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.ListeCategorieClient;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.CriteresRechercheCategoriesFournisseur;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.ListeCategorieFournisseur;
import ri.serien.libcommun.gescom.personnalisation.codepostal.CriteresCodePostal;
import ri.serien.libcommun.gescom.personnalisation.codepostal.ListeCodePostal;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.CritereCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.ListeCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.CriteresConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.ListeConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.famille.CritereFamille;
import ri.serien.libcommun.gescom.personnalisation.famille.ListeFamille;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.CritereFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.ListeFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.groupe.CritereGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupe.ListeGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.CritereGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.ListeGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.CritereLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.ListeLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.magasin.CritereMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.CriteresModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.motifretour.ListeMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.ListeParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.CriteresParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.ListeParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.pays.CriteresPays;
import ri.serien.libcommun.gescom.personnalisation.pays.ListePays;
import ri.serien.libcommun.gescom.personnalisation.reglement.CritereModeReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.ListeModeReglement;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.CriteresRechercheSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.ListeSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.CritereSousFamilles;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.personnalisation.tarif.CriteresRechercheTarifs;
import ri.serien.libcommun.gescom.personnalisation.tarif.ListeTarif;
import ri.serien.libcommun.gescom.personnalisation.tarif.Tarif;
import ri.serien.libcommun.gescom.personnalisation.transporteur.CriteresRechercheTransporteurs;
import ri.serien.libcommun.gescom.personnalisation.transporteur.ListeTransporteur;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.ListeTypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.ListeTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.CriteresRechercheTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.ListeTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typestockagemagasin.ListeTypeStockageMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.CriteresRechercheUnites;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.Unite;
import ri.serien.libcommun.gescom.personnalisation.vendeur.CriteresRechercheVendeurs;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.CritereZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ListeZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.CritereCategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.ListeCategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.vente.commune.CritereCommune;
import ri.serien.libcommun.gescom.vente.commune.ListeCommune;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceParametre extends Remote {
  public static final String NOMSERVICE = "ServiceParametre";
  
  // -- Paramètres de la Gescom
  public boolean ping() throws RemoteException;
  
  /**
   * Retourne les informations d'un utilisateur en gescom.
   */
  public UtilisateurGescom chargerUtilisateurGescom(IdSession pIdSession, String pProfil, Bibliotheque pCurLib,
      IdEtablissement pIdEtablissement) throws RemoteException;
  
  /**
   * Recherche des groupes suivant certains critères.
   */
  public ListeGroupe chargerListeGroupe(IdSession pIdSession, CritereGroupe pCritere) throws RemoteException;
  
  /**
   * Recherche des familles suivant certains critères.
   */
  public ListeFamille chargerListeFamille(IdSession pIdSession, CritereFamille pCriteres) throws RemoteException;
  
  /**
   * Recherche des sous-familles suivant certains critères.
   */
  public ListeSousFamille chargerListeSousFamille(IdSession pIdSession, CritereSousFamilles pCriteres) throws RemoteException;
  
  /**
   * Recherche des magasins suivant certains critères.
   */
  public ListeMagasin chargerListeMagasin(IdSession pIdSession, CritereMagasin pCriteres) throws RemoteException;
  
  /**
   * Lit les types de facturation pour un établissement donné.
   */
  public ListeTypeFacturation chargerListeTypeFacturation(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException;
  
  /**
   * Lit les motifs de retour pour un établissement donné.
   */
  public ListeMotifRetour chargerListeMotifRetour(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException;
  
  /**
   * Charger les types de stockage magasin pour un établissement donné.
   */
  public ListeTypeStockageMagasin chargerListeTypeStockageMagasin(IdSession pIdSession, IdEtablissement pIdEtablissement)
      throws RemoteException;
  
  /**
   * Recherche les unités suivant certains critères.
   */
  public ListeUnite chargerListeUnite(IdSession pIdSession, CriteresRechercheUnites pCriteres) throws RemoteException;
  
  /**
   * Recherche des catégories clients suivant certains critères.
   */
  public ListeCategorieClient chargerListeCategorieClient(IdSession pIdSession, CriteresRechercheCategoriesClients pCriteres)
      throws RemoteException;
  
  /**
   * Recherche des catégories fournisseur suivant certains critères.
   */
  public ListeCategorieFournisseur chargerListeCategorieFournisseur(IdSession pIdSession,
      CriteresRechercheCategoriesFournisseur pCriteres) throws RemoteException;
  
  /**
   * Charger la liste de tous les établissements
   */
  public ListeEtablissement chargerListeEtablissement(IdSession pIdSession) throws RemoteException;
  
  /*
  * Recherche des code postaux suivant certains critères.
  */
  public ListeCodePostal chargerListeCodePostal(IdSession pIdSession, CriteresCodePostal pCriteres) throws RemoteException;
  
  /*
   * Recherche des devises suivant certains critères.
   */
  public ListeDevise chargerListeDevise(IdSession pIdSession, CriteresRechercheDevise pCriteres) throws RemoteException;
  
  /*
   * Recherche des type de gratuit suivant certains critères.
   */
  public ListeTypeGratuit chargerListeTypeGratuit(IdSession pIdSession, CriteresRechercheTypeGratuit pCriteres) throws RemoteException;
  
  /**
   * Charger un établissement donné.
   */
  public Etablissement chargerEtablissement(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException;
  
  /**
   * Recherche les transporteurs suivant certains critères.
   */
  public ListeTransporteur chargerListeTransporteur(IdSession pIdSession, CriteresRechercheTransporteurs pCriteres)
      throws RemoteException;
  
  /**
   * Recherche les lieu de transport suivant certains critères.
   */
  public ListeLieuTransport chargerListeLieuTransport(IdSession pIdSession, CritereLieuTransport pCriteres) throws RemoteException;
  
  /**
   * Recherche les affaires suivant certains critères.
   */
  public ListeAffaire chargerListeAffaire(IdSession pIdSession, CriteresRechercheAffaires pCriteres) throws RemoteException;
  
  /**
   * Recherche les canaux de vente suivant certains critères
   */
  public ListeCanalDeVente chargerListeCanalDeVente(IdSession pIdSession, CriteresRechercheCanalDeVente pCriteres) throws RemoteException;
  
  /**
   * Recherche les sections analytiques suivant certains critères
   */
  public ListeSectionAnalytique chargerListeSectionAnalytique(IdSession pIdSession, CriteresRechercheSectionAnalytique pCriteres)
      throws RemoteException;
  
  /**
   * Recherche les groupes de conditions de ventes suivant certains critères
   */
  public ListeGroupeConditionVente chargerListeGroupeConditionVente(IdSession pIdSession, CritereGroupeConditionVente pCriteres)
      throws RemoteException;
  
  /**
   * Recherche des règlements suivant certains critères.
   */
  public ListeModeReglement chargerListeModeReglement(IdSession pIdSession, CritereModeReglement pCriteres) throws RemoteException;
  
  /**
   * Recherche des vendeurs suivant certains critères.
   */
  public ListeVendeur chargerListeVendeur(IdSession pIdSession, CriteresRechercheVendeurs pCriteres) throws RemoteException;
  
  /**
   * Recherche des acheteurs suivant certains critères.
   */
  public ListeAcheteur chargerListeAcheteur(IdSession pIdSession, CriteresRechercheAcheteur pCriteres) throws RemoteException;
  
  /**
   * Recherche des tarifs suivant certains critères.
   */
  public ListeTarif chargerListeTarif(IdSession pIdSession, CriteresRechercheTarifs pCriteres) throws RemoteException;
  
  /**
   * Lire un tarif.
   */
  public Tarif chargerTarif(IdSession pIdSession, IdEtablissement pIdEtablissement, String aCodeTarif) throws RemoteException;
  
  /**
   * Charger la liste des paramètres systèmes d'un établissement.
   */
  public ListeParametreSysteme chargerListeParametreSysteme(IdSession pIdSession, IdEtablissement pIdEtablissement)
      throws RemoteException;
  
  /**
   * Recherche des civilités suivant certains critères.
   */
  public ListeCivilite chargerListeCivilite(IdSession pIdSession) throws RemoteException;
  
  /**
   * Retourne une liste de catégories de contact.
   */
  public ListeCategorieContact chargerListeCategorieContact(IdSession pIdSession) throws RemoteException;
  
  /**
   * Retourne une liste de personnalisation de flux.
   */
  public ListePersonnalisationFlux chargerListePersonnalisationFlux(IdSession pIdSession, CriterePersonnalisationFlux pCritere)
      throws RemoteException;
  
  /**
   * Recherche des modes d'expédition suivant certains critères.
   */
  public ListeModeExpedition chargerListeModeExpedition(IdSession pIdSession, CriteresModeExpedition pCriteres) throws RemoteException;
  
  /**
   * Recherche des modes d'expédition suivant certains critères.
   */
  public ModeExpedition chargerModeExpedition(IdSession pIdSession, ModeExpedition pModeExpedition) throws RemoteException;
  
  /**
   * Recherche les journaux suivant certains critères.
   */
  public ListeJournal chargerListeJournal(IdSession pIdSession, CriteresRechercheJournaux pCriteres) throws RemoteException;
  
  /**
   * Retourne la liste des communes suivant les critères donnés.
   */
  public ListeCommune chargerListeCommune(IdSession pIdSession, CritereCommune pCriteres) throws RemoteException;
  
  /**
   * Recherche des mémos suivant des critères données.
   */
  public ListeMemo chargerListeMemo(IdSession pIdSession, FiltreMemo pFiltreMemo) throws RemoteException;
  
  /**
   * Lit un mémo.
   */
  public Memo chargerMemo(IdSession pIdSession, IdMemo pIdMemo) throws RemoteException;
  
  /**
   * Ecrit un mémo.
   */
  public void sauverMemo(IdSession pIdSession, Memo pMemo) throws RemoteException;
  
  /**
   * Supprime un mémo.
   */
  public void supprimerMemo(IdSession pIdSession, IdMemo pIdMemo) throws RemoteException;
  
  /**
   * Lit un bloc-notes.
   */
  public BlocNote chargerBlocNote(IdSession pIdSession, IdBlocNote pIdBlocnotes) throws RemoteException;
  
  /**
   * Ecrit un bloc-notes.
   */
  public void sauverBlocNote(IdSession pIdSession, BlocNote pBlocnotes) throws RemoteException;
  
  /**
   * Supprime un bloc-notes.
   */
  public void supprimerBlocNote(IdSession pIdSession, IdBlocNote pIdBlocnotes) throws RemoteException;
  
  /**
   * Retourne les communes d'une zone géographique
   */
  public ListeCommuneZoneGeographique chargerListeCommuneZoneGeographique(IdSession pIdSession, IdZoneGeographique pIdZoneGeographique)
      throws RemoteException;
  
  /**
   * Retourne les communes de toutes les zone géographique
   */
  public ListeCommuneZoneGeographique chargerListeCommuneZoneGeographique(IdSession pIdSession, CritereCommuneZoneGeographique pCriteres)
      throws RemoteException;
  
  /**
   * Recherche des zone geographique suivant certains critères.
   */
  public ListeZoneGeographique chargerListeZoneGeographique(IdSession pIdSession, CritereZoneGeographique pCriteres)
      throws RemoteException;
  
  /**
   * Recherche une liste d'IdZoneGeographique à partir de critères de recherche.
   */
  public List<IdZoneGeographique> chargerListeIdZoneGeographique(IdSession pIdSession, CritereZoneGeographique pCriteres)
      throws RemoteException;
  
  /**
   * Recherche des pays suivant certains critères.
   */
  public ListePays chargerListePays(IdSession pIdSession, CriteresPays pCriteres) throws RemoteException;
  
  /**
   * Retourne une unité sur la base de son identifiant
   */
  public Unite chargerUnite(IdSession pIdSession, IdUnite pIdUnite) throws RemoteException;
  
  /**
   * Retourne une liste de catégories de zones personnalisées
   */
  public ListeCategorieZonePersonnalisee chargerListeCategorieZonePersonnalisee(IdSession pIdSession,
      CritereCategorieZonePersonnalisee pCritere) throws RemoteException;
  
  /**
   * Retourne une liste de catégories de zones personnalisées du module exploitation
   */
  public ListeZonePersonnaliseeExploitation chargerListeZonePersonnaliseeExploitation(IdSession pIdSession,
      CritereCategorieZonePersonnalisee pCritere) throws RemoteException;
  
  /**
   * Retourne la liste des ConditionCommissionnement
   */
  public ListeConditionCommissionnement chargerListeConditionCommissionnement(IdSession pIdSession,
      CriteresConditionCommissionnement pCriteres) throws RemoteException;
  
  /**
   * Charger les sécurités d'un utilisateur
   */
  /*
  public ListeSecurite chargerListeSecurite(IdSession pIdSession, IdEtablissement pIdEtablissement, String pProfil)
      throws RemoteException;*/
  
  /**
   * Charger les sécurités de la gescom d'un utilisateur.
   */
  public SecuriteUtilisateurGescom chargerSecuriteGescom(IdSession pIdSession, IdEtablissement pIdEtablissement, String pProfil)
      throws RemoteException;
  
  /**
   * Charger les types d'avoir
   */
  public ListeTypeAvoir chargerListeTypeAvoir(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException;
  
  /**
   * Charger les documentstocke
   */
  public ListeDocumentStocke chargerListeDocumentStocke(IdSession pIdSession, CritereDocumentStocke pCriteres) throws RemoteException;
  
  /**
   * Charge le chemin complet d'un document Stocke
   */
  public String chargerCheminCompletDocumentStocke(IdSession pIdSession, DocumentStocke pDocumentStocke) throws RemoteException;
  
  /**
   * Vérifie si un paramètre système donné est actif sur au moins un établissement de la base de données.
   */
  public boolean isParametreSystemeActifSurUnEtablissement(IdSession pIdSession, EnumParametreSysteme pEnumParametreSysteme)
      throws RemoteException;
  
  // ------
  // PARAMETRES AVANCES
  // ------
  
  /**
   * Charge la liste des identifiants des paramètres avancés à partir de critères.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCriteres Critères de recherches de paramètres avancés.
   * @return Liste d'identifiants de paramètres avancés.
   */
  public List<IdParametreAvance> chargerListeIdParametreAvance(IdSession pIdSession, CritereParametreAvance pCriteres)
      throws RemoteException;
  
  /**
   * Charge une liste de paramètres avancés à partir de critères.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCriteres Critères de recherches de paramètres avancés.
   * @return Liste de paramètres avancés.
   */
  public ListeParametreAvance chargerListeParametreAvance(IdSession pIdSession, CritereParametreAvance pCriteres) throws RemoteException;
  
  /**
   * Charge une liste de paramètres avancés à partir d'une liste d'identifiants.
   * 
   * @param pIdSession Identifiant de la session
   * @param pListeIdParametreAvance Liste d'identifiants de paramètres avancés.
   * @return Liste de paramètres avancés.
   */
  public ListeParametreAvance chargerListeParametreAvance(IdSession pIdSession, List<IdParametreAvance> pListeIdParametreAvance)
      throws RemoteException;
  
  /**
   * Créer ou modifier un paramètre avancé en base.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pParametreAvance Paramètre avancé à persister en base.
   */
  public void sauverParametreAvance(IdSession pIdSession, ParametreAvance pParametreAvance) throws RemoteException;
  
  /**
   * Supprimer un paramètre avancé de la base.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pParametreAvance Paramètre avancé à supprimer de la base.
   */
  public void supprimerParametreAvance(IdSession pIdSession, ParametreAvance pParametreAvance) throws RemoteException;
  
  /**
   * Retourner l'identifiant maximal existant pour les paramètres avancés.
   * 
   * @param pIdSession
   * @return Identifiant maximal existant.
   */
  public int getIdMaxParametreAvance(IdSession pIdSession) throws RemoteException;
  
  /**
   * Charger une liste de participations aux frais d'expédition.
   */
  public ListeParticipationFraisExpedition chargerListeParticipationFraisExpedition(IdSession pIdSession,
      CriteresParticipationFraisExpedition pCriteres) throws RemoteException;
  
  /**
   * Charger une liste de formules de prix.
   */
  public ListeFormulePrix chargerListeFormulePrix(IdSession pIdSession, CritereFormulePrix pCriteres) throws RemoteException;
}
