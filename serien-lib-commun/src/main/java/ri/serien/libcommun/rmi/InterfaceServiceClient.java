/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.CritereClient;
import ri.serien.libcommun.gescom.commun.client.CriteresRechercheDocumentsHistoriqueVentesClientArticle;
import ri.serien.libcommun.gescom.commun.client.DocumentHistoriqueVenteClientArticle;
import ri.serien.libcommun.gescom.commun.client.EncoursClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeIdClient;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.chantier.CritereChantier;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.chantier.ListeChantier;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.reglement.CritereBanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.ListeBanqueClient;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceClient extends Remote {
  public static final String NOMSERVICE = "ServiceClient";
  
  public boolean ping() throws RemoteException;
  
  public ListeClientBase chargerListeClientBase(IdSession pIdSession, CritereClient pCritereClient) throws RemoteException;
  
  public ListeClientBase chargerListeClientBase(IdSession pIdSession, ListeIdClient pListeIdClient) throws RemoteException;
  
  public Client chargerClient(IdSession pIdSession, IdClient pIdClient) throws RemoteException;
  
  public EncoursClient chargerEncoursClient(IdSession pIdSession, IdClient pIdClient) throws RemoteException;
  
  public EncoursClient controlerEncoursClient(IdSession pIdSession, DocumentVente pDocumentVente, int pCollectifComptableClient)
      throws RemoteException;
  
  public ListeContact chargerListeContact(IdSession pIdSession, IdClient pIdClient) throws RemoteException;
  
  public Client sauverClientComptant(IdSession pIdSession, Client pClient) throws RemoteException;
  
  public void sauverClient(IdSession pIdSession, Client pClient) throws RemoteException;
  
  public List<DocumentHistoriqueVenteClientArticle> chargerDocumentHistoriqueVenteClientArticle(IdSession pIdSession, IdClient pIdClient,
      IdArticle pIdArticle, CriteresRechercheDocumentsHistoriqueVentesClientArticle pCriteres) throws RemoteException;
  
  public List<IdChantier> chargerListeIdChantier(IdSession pIdSession, CritereChantier pCriteres) throws RemoteException;
  
  public ListeChantier chargerListeChantier(IdSession pIdSession, List<IdChantier> pListeIdChantier) throws RemoteException;
  
  public Chantier chargerChantier(IdSession pIdSession, IdChantier pIdChantier) throws RemoteException;
  
  public IdLigneAchat chargerLienLigneVenteLigneAchat(IdSession pIdSession, IdLigneVente pIdLigneVente) throws RemoteException;
  
  public ListeBanqueClient chargerListeBanqueClient(IdSession pIdSession, CritereBanqueClient pCriteres) throws RemoteException;
  
}
