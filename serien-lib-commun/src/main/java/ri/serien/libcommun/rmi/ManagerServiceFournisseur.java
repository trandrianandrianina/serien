/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.gescom.achat.catalogue.ChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.CriteresRechercheCfgCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.IdChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.IdConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ListeConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeFichierCSV;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.FiltreFournisseurBase;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseurBase;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service fournisseur.
 *
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceFournisseur {
  private static InterfaceServiceFournisseur service;
  
  /**
   * Retourner service dédié à la gestion des articles.
   */
  private static InterfaceServiceFournisseur getService() {
    if (service == null) {
      InterfaceServiceFournisseur interfaceService =
          (InterfaceServiceFournisseur) ManagerClientRMI.recupererService(InterfaceServiceFournisseur.NOMSERVICE);
      
      service = (InterfaceServiceFournisseur) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceFournisseur.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Fournisseur chargerFournisseur(IdSession pIdSession, Fournisseur pFournisseur) {
    try {
      return getService().chargerFournisseur(pIdSession, pFournisseur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Fournisseur chargerFournisseur(IdSession pIdSession, IdFournisseur pIdFournisseur) {
    try {
      return getService().chargerFournisseur(pIdSession, pIdFournisseur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeFournisseurBase chargerListeFournisseurBase(IdSession pIdSession, List<IdFournisseur> pListeId) {
    try {
      return getService().chargerListeFournisseurBase(pIdSession, pListeId);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeFournisseur chargerListeFournisseurEtablissement(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerListeFournisseurEtablissement(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<IdFournisseur> chargerListeIdFournisseur(IdSession pIdSession, CritereFournisseur pCritereFournisseur) {
    try {
      return getService().chargerListeIdFournisseur(pIdSession, pCritereFournisseur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeAdresseFournisseur chargerListeAdresseFournisseur(IdSession pIdSession,
      List<IdAdresseFournisseur> pListeIdAdresseFournisseur) {
    try {
      return getService().chargerListeAdresseFournisseur(pIdSession, pListeIdAdresseFournisseur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeAdresseFournisseur chargerListeAdresseFournisseur(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    try {
      return getService().chargerListeAdresseFournisseur(pIdSession, pIdEtablissement);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static AdresseFournisseur chargerAdresseFournisseur(IdSession pIdSession, IdAdresseFournisseur pIdAdresseFournisseur) {
    try {
      return getService().chargerAdresseFournisseur(pIdSession, pIdAdresseFournisseur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ArrayList<IdAdresseFournisseur> chargerListeIdAdresseFournisseur(IdSession pIdSession, CritereFournisseur criteres) {
    try {
      return getService().chargerListeIdAdresseFournisseur(pIdSession, criteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static List<AdresseFournisseur> chargerListeAdresseFournisseur(IdSession pIdSession, IdFournisseur pIdFournisseur,
      boolean pInclureSiege) {
    try {
      return getService().chargerListeAdresseFournisseur(pIdSession, pIdFournisseur, pInclureSiege);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeFournisseur chargerListeFournisseur(IdSession pIdSession, CritereFournisseur criteres) {
    try {
      return getService().chargerListeFournisseur(pIdSession, criteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeConfigurationCatalogue chargerListeConfigsCatalogue(IdSession pIdSession, CriteresRechercheCfgCatalogue criteres) {
    try {
      return getService().chargerListeConfigsCatalogue(pIdSession, criteres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ConfigurationCatalogue chargerUneConfigCatalogueParId(IdSession pIdSession, IdConfigurationCatalogue pIdConfiguration) {
    try {
      return getService().chargerConfigurationCatalogueParId(pIdSession, pIdConfiguration);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeFichierCSV chargerListeFichiersCataloguesCSV(IdSession pIdSession) {
    try {
      return getService().chargerListeFichiersCataloguesCSV(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeChampCatalogue chargerListeChampUnCatalogue(IdSession pIdSession, String pNomFichier) {
    try {
      return getService().chargerListeChampUnCatalogue(pIdSession, pNomFichier);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeChampERP chargerListeChampERP(IdSession pIdSession) {
    try {
      return getService().chargerListeChampERP(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverUneConfigurationCatalogue(IdSession pIdSession, ConfigurationCatalogue pConfigurationCatalogue) {
    try {
      getService().sauverUneConfigurationCatalogue(pIdSession, pConfigurationCatalogue);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerUneConfigurationCatalogue(IdSession pIdSession, ConfigurationCatalogue pConfigurationCatalogue) {
    try {
      getService().supprimerUneConfigurationCatalogue(pIdSession, pConfigurationCatalogue);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeFournisseurBase chargerListeFournisseurBaseParEtablissement(IdSession pIdSession, FiltreFournisseurBase pFiltres) {
    try {
      return getService().chargerListeFournisseurBaseParEtablissement(pIdSession, pFiltres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ChampERP chargerUnChampERPparId(IdSession pIdSession, IdChampERP pidChampERP) {
    try {
      return getService().chargerUnChampERPparId(pIdSession, pidChampERP);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverAssociationChampsConfigurationCatalogue(IdSession pIdSession, ConfigurationCatalogue pConfiguration,
      IdChampERP pIdChampERP) {
    try {
      getService().sauverAssociationChampsConfigurationCatalogue(pIdSession, pConfiguration, pIdChampERP);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
