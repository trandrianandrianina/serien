/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.vente.gvc.ConditionAchatGVC;
import ri.serien.libcommun.gescom.vente.gvc.FiltresGVC;
import ri.serien.libcommun.gescom.vente.gvc.LigneGvc;
import ri.serien.libcommun.gescom.vente.gvc.ListeParametresGVC;
import ri.serien.libcommun.gescom.vente.gvc.pricejet.TarifPriceJet;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service GVC.
 *
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceGVC {
  private static InterfaceServiceGVC service;

  /**
   * Retourner le service dédié à la gestion du GVC.
   */
  private static InterfaceServiceGVC getService() {
    if (service == null) {
      InterfaceServiceGVC interfaceService = (InterfaceServiceGVC) ManagerClientRMI.recupererService(InterfaceServiceGVC.NOMSERVICE);

      service = (InterfaceServiceGVC) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceGVC.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }

  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static ListeParametresGVC chargerListeParametresGVC(IdSession pIdSession) {
    try {
      return getService().chargerListeParametresGVC(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static void sauverListeParametresGVC(IdSession pIdSession, ListeParametresGVC listeParametresGVC) {
    try {
      getService().sauverListeParametresGVC(pIdSession, listeParametresGVC);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static BigDecimal chargerTauxTVAetbPilote(IdSession pIdSession) {
    try {
      return getService().chargerTauxTVAetbPilote(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static String[] chargerListeEtablissement(IdSession pIdSession) {
    try {
      return getService().chargerListeEtablissement(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static ArrayList<String> chargerListeProfils(IdSession pIdSession) {
    try {
      return getService().chargerListeProfils(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static int chargerNombreLignesGVC(IdSession pIdSession, FiltresGVC pFiltres) {
    try {
      return getService().chargerNombreLignesGVC(pIdSession, pFiltres);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static ArrayList<LigneGvc> chargerListeLigneGvc(IdSession pIdSession, FiltresGVC pFiltres, boolean onLitTout) {
    try {
      return getService().chargerListeLigneGvc(pIdSession, pFiltres, onLitTout);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static ConditionAchatGVC chargerConditionAchatGVC(IdSession pIdSession, LigneGvc pLigne, Fournisseur pFournisseur) {
    try {
      return getService().chargerConditionAchatGVC(pIdSession, pLigne, pFournisseur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static ConditionAchatGVC chargerConditionAchatGVCdistributeurReference(IdSession pIdSession, LigneGvc pLigne) {
    try {
      return getService().chargerConditionAchatGVCdistributeurReference(pIdSession, pLigne);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static BigDecimal[] chargerPrixAchatArticle(IdSession pIdSession, IdArticle pIdArticle) {
    try {
      return getService().chargerPrixAchatArticle(pIdSession, pIdArticle);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static boolean sauverTarifsArticle(IdSession pIdSession, LigneGvc pLigne) {
    try {
      return getService().sauverTarifsArticle(pIdSession, pLigne);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static boolean sauverWorkFlow(IdSession pIdSession, LigneGvc pLigne) {
    try {
      return getService().sauverWorkFlow(pIdSession, pLigne);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static boolean sauverMiseEnLigneArticle(IdSession pIdSession, LigneGvc pLigne) {
    try {
      return getService().sauverMiseEnLigneArticle(pIdSession, pLigne);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static boolean sauverVeilleGSB(IdSession pIdSession, LigneGvc pLigne) {
    try {
      return getService().sauverVeilleGSB(pIdSession, pLigne);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static void sauverFluxMagento(IdSession pIdSession, LigneGvc ligneGvc) {
    try {
      getService().sauverFluxMagento(pIdSession, ligneGvc);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static TarifPriceJet chargerTarifPriceJet(IdSession pIdSession, LigneGvc pLigne) {
    try {
      return getService().chargerTarifPriceJet(pIdSession, pLigne);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static boolean sauverPointsWattArticle(IdSession pIdSession, LigneGvc pLigne) {
    try {
      return getService().sauverPointsWattArticle(pIdSession, pLigne);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }

  public static String chargerAdresseMailProfilAs400(IdSession pIdSession, String pProfil) {
    try {
      return getService().chargerAdresseMailProfilAs400(pIdSession, pProfil);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
}
