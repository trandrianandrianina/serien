/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import ri.serien.libcommun.exploitation.export.CritereExport;
import ri.serien.libcommun.exploitation.export.Export;
import ri.serien.libcommun.exploitation.export.IdExport;
import ri.serien.libcommun.exploitation.export.ListeExport;
import ri.serien.libcommun.outils.session.IdSession;

public interface InterfaceServiceExport extends Remote {
  public static final String NOMSERVICE = "ServiceExport";
  
  /**
   * Tester que le service Export est actif.
   */
  public boolean ping() throws RemoteException;
  
  /**
   * Charger une liste d'identifiants d'export à partir de critères de filtrage.
   * @param pIdSession La session.
   * @param pCritere Les critères de filtrage.
   * @return La liste d'identifiants d'exports.
   * @throws RemoteException
   */
  public List<IdExport> chargerListeIdExport(IdSession pIdSession, CritereExport pCritere) throws RemoteException;
  
  /**
   * Charger une liste d'exports à partir d'une liste d'identifiants.
   * Le chargement n'est pas complet c'est à dire que les colonnes ne sont pas chargées.
   * @param pIdSession La session.
   * @param pListeIdExport La liste d'identifiants d'exports.
   * @return La liste d'exports.
   * @throws RemoteException
   */
  public ListeExport chargerListeExport(IdSession pIdSession, List<IdExport> pListeIdExport) throws RemoteException;
  
  /**
   * Charger un export à partir d'un identifiant.
   * Le chargement est complet c'est à dire que les colonnes sont également chargées.
   * @param pIdSession La session.
   * @param pListeIdExport La liste d'identifiants d'exports.
   * @return La liste d'exports.
   */
  public Export chargerExport(IdSession pIdSession, IdExport pIdExport) throws RemoteException;
  
  /**
   * Sauver un export.
   * @param pIdSession La session.
   * @param pExport L'export.
   * @return L'export.
   */
  public Export sauverExport(IdSession pIdSession, Export pExport) throws RemoteException;
}
