/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import ri.serien.libcommun.outils.Trace;

/**
 * Intercepte les appels de méthodes RMI du côté client.
 * La methode invoke de cette classe est appelée avant chaque appel à une méthode du stuck client RMI.
 */
public class InvocationHandlerClientRMI implements InvocationHandler {
  private static final int NOMBRE_TENTATIVE_APPEL_RMI = 10;
  private static final int DELAI_INITIAL_ENTRE_TENTATIVE = 200;
  private static final String PREFIXE_RMI = "[RMI] ";
  private Object obj;
  
  public InvocationHandlerClientRMI(Object pObj) {
    obj = pObj;
  }
  
  /**
   * Méthode appelée avant tout appel à une méthode distance via RMI.
   * Elle a plusieurs objectifs :
   * - génèrer une trace avant et après la méthode RMI appelée,
   * - mesurer le temps d'exécution côte client,
   * - effectuer plusieurs tentatives en cas de coupure réseau,
   * - analyser les exceptions retournées par le serveur pour générer un message utilisateur cohérent.
   */
  @Override
  public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {
    // Construire le nom de la méthode sous la forme "classe.methode"
    final String methode = m.getDeclaringClass().getSimpleName() + "." + m.getName();
    
    // Tracer le début de l'appel
    Trace.info(PREFIXE_RMI + "Appel  " + methode);
    
    // Stocker le temps avant l'appel de méthode RMI
    long debut = System.currentTimeMillis();
    
    // Faire plusieurs tentatives pour appeler la méthode RMI
    Object objetRetour = null;
    for (int tentative = 1; tentative <= NOMBRE_TENTATIVE_APPEL_RMI; tentative++) {
      try {
        // Appeler la méthode RMI prévue
        objetRetour = m.invoke(obj, args);
        
        // Sortir de la boucle si l'appel a réussi
        break;
      }
      catch (InvocationTargetException invocationTargetException) {
        // L'appel RMI a échoué, on recherche si un type d'exception connu
        EnumTypeRMIException typeException = EnumTypeRMIException.valueOfByException(invocationTargetException);
        
        // Refaire d'autres tentatives si c'est une erreur de connexion réseau et que le nombre de tentatives max n'est pas atteint.
        // Les erreurs de connexion génèrent :
        // - ConnectIOException dans la plupart des cas,
        // - ConnectException (trouvé dans des traces sur Mac),
        // - UnmarshalException dans de rares cas.
        // Voir si d'autres exceptions peuvent se produire.
        if (tentative < NOMBRE_TENTATIVE_APPEL_RMI && (typeException == EnumTypeRMIException.CONNECT_IO_EXCEPTION
            || typeException == EnumTypeRMIException.CONNECT_EXCEPTION || typeException == EnumTypeRMIException.UNMARSHAL_EXCEPTION)) {
          // Tracer l'échec de l'appel RMI
          Trace.erreur(PREFIXE_RMI + "Tentative " + tentative + " " + methode);
          
          // Attendre avant la prochaine tentative, on attend plus à chaque tentative
          Thread.sleep(tentative * DELAI_INITIAL_ENTRE_TENTATIVE);
        }
        else {
          // Tracer l'échec de l'appel RMI
          // Ce n'est pas utile de tracer l'exception car cela sera fait dans la méthode au dessus, celle du "ManagerService...." qui
          // a été utilisée pour déclencher l'appel au serveur RMI. Les méthodes des "ManagerService..." capturent toutes les exceptions
          // et appelent ManagerClientRMI.genererMessageErreurException(e) pour convertir l'exception retournée lors de l'appel RMI en
          // MessageUtilisateurException. La trace est générée à ce moment là, dans le new MessageUtilisateurException.
          Trace.erreur(PREFIXE_RMI + "Echec " + methode);
          
          // Transmettre l'exception
          throw invocationTargetException;
        }
      }
    }
    
    // Calculer la durée de l'appel de méthode RMI
    long duree = System.currentTimeMillis() - debut;
    
    // Tracer le retour avec succès
    Trace.info(PREFIXE_RMI + "Succès " + methode + " (" + duree + " ms)");
    
    // Transmettre le retour de la méthode RMI à l'appelant
    return objetRetour;
  }
}
