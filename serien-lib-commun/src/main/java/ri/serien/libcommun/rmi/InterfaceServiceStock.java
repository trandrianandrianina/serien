/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.ListeLigneMouvement;
import ri.serien.libcommun.gescom.commun.document.CritereAttenduCommande;
import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.stock.CritereStock;
import ri.serien.libcommun.gescom.commun.stock.ListeStock;
import ri.serien.libcommun.gescom.commun.stock.StockDetaille;
import ri.serien.libcommun.gescom.commun.stockattenducommande.CritereStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockattenducommande.ListeStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockcomptoir.CritereStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.IdStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.commun.stockmouvement.CritereStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.ListeStockMouvement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Interface RMI pour l'objet métier stock.
 */
public interface InterfaceServiceStock extends Remote {
  public static final String NOMSERVICE = "ServiceStock";
  
  public boolean ping() throws RemoteException;
  
  public ListeStockComptoir chargerListeStockComptoir(IdSession pIdSession, CritereStockComptoir pCritereStock) throws RemoteException;
  
  public ListeStockComptoir chargerListeStockComptoir(IdSession pIdSession, List<IdStockComptoir> pListeId) throws RemoteException;
  
  public List<LigneAttenduCommande> chargerListeLigneAttenduCommande(IdSession pIdSession, CritereAttenduCommande pCritereAttenduCommande)
      throws RemoteException;
  
  public List<IdLigneMouvement> chargerListeIdLigneMouvement(IdSession pIdSession, IdArticle pIdArticle, Magasin pMagasin)
      throws RemoteException;
  
  public ListeLigneMouvement chargerListeLigneMouvement(IdSession pIdSession, List<IdLigneMouvement> pListeIdMouvements,
      Integer pNombreDecimalesStock) throws RemoteException;
  
  /**
   * Charger des stocks suivant les critères de recherche.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCritereStock Critères de recherche des stocks.
   * @return Liste de stocks.
   * @throws RemoteException
   */
  public ListeStock chargerListeStock(IdSession pIdSession, CritereStock pCritereStock) throws RemoteException;
  
  /**
   * Charger le stock détaillé d'un article pour un magasin ou un établissement.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pIdMagasin Identifiant du magasin.
   * @param pIdArticle Identifiant de l'article.
   * @return Stock détaillé de l'article.
   * @throws RemoteException
   */
  public StockDetaille chargerStockDetaille(IdSession pIdSession, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin,
      IdArticle pIdArticle) throws RemoteException;
  
  /**
   * Charger les lignes attendus et commandés suivant les critères de recerche.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCritereStockAttenduCommande Critères de recherche des attendus et commandés.
   * @return Liste d'attendus et commandés.
   * @throws RemoteException
   */
  public ListeStockAttenduCommande chargerListeStockAttenduCommande(IdSession pIdSession,
      CritereStockAttenduCommande pCritereStockAttenduCommande) throws RemoteException;
  
  /**
   * Charger la liste des mouvements de stock suivant les critères de recherche.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCritereStockMouvement Critères de recherche.
   * @return Liste de mouvements de stock.
   * @throws RemoteException
   */
  public ListeStockMouvement chargerListeStockMouvement(IdSession pIdSession, CritereStockMouvement pCritereStockMouvement)
      throws RemoteException;
  
  /**
   * Charger la date minimuum de réapprovisionnement possible.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pIdArticle Identifiant de l'article.
   * @return Date minimum de reapprovisionnement.
   * @throws RemoteException
   */
  public Date chargerDateMiniReappro(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException;
}
