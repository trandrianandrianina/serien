/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.net.ConnectException;
import java.rmi.ConnectIOException;
import java.rmi.NoSuchObjectException;
import java.rmi.ServerError;
import java.rmi.UnmarshalException;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Les différents type d'exceptions retournées lors d'un appel RMI vers le serveur.
 */
public enum EnumTypeRMIException {
  MESSAGE_ERREUR_EXCEPTION("Message utilisateur"),
  CONNECT_IO_EXCEPTION("Pas de route vers le serveur"),
  CONNECT_EXCEPTION("Serveur stoppé"),
  NO_SUCH_OBJECT_EXCEPTION("Serveur redémarré"),
  UNMARSHAL_EXCEPTION("Versions incompatibles"),
  NO_SUCH_METHOD_EXCEPTION("Serveur pas à jour"),
  SERVER_ERROR("Erreur serveur"),;
  
  private String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeRMIException(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * Le libellé associé au type d'exception.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le libellé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Détermine le type d'exception le plus approprié en parcourant la pile des exceptions.
   */
  static public EnumTypeRMIException valueOfByException(Exception e) {
    // Parcourir la pile d'exceptions pour y rechercher une exception particulière
    for (Throwable cause = e; cause != null; cause = cause.getCause()) {
      // Message d'erreur standard Série N renvoyé par le serveur
      if (cause instanceof MessageErreurException) {
        return MESSAGE_ERREUR_EXCEPTION;
      }
      // Cette exception se produit si on ne parvient pas à trouver de route réseau pour atteindre le serveur.
      // C'est probablement un problème de connectivité réseau entre le poste de travail et le serveur.
      else if (cause instanceof ConnectIOException) {
        return CONNECT_IO_EXCEPTION;
      }
      // Cette exception se produit Lorsqu'on arrête le serveur Série N.
      else if (cause instanceof ConnectException) {
        return CONNECT_EXCEPTION;
      }
      // Cette exception se produit lorsque le serveur Série N a été arrété puis relancé.
      // RMI parvient à contacter le serveur métier mais celui-ci ne retrouve plus les objets distants attendus.
      else if (cause instanceof NoSuchObjectException) {
        return NO_SUCH_OBJECT_EXCEPTION;
      }
      // Cette exception se produit lorsque les classes n'ont pas la même sérialisation.
      // Cela signifie que les jar installés sur le client et le serveur ne correspondent pas.
      else if (cause instanceof UnmarshalException) {
        return UNMARSHAL_EXCEPTION;
      }
      // Cette exception se produit pour des erreurs côté serveur
      else if (cause instanceof ServerError) {
        // Cette erreur se produit si le JDK ne trouve pas une méthode dans un .jar
        if (e.getCause() != null && e.getCause() instanceof NoSuchMethodError) {
          return NO_SUCH_METHOD_EXCEPTION;
        }
        else {
          return SERVER_ERROR;
        }
      }
    }
    
    // Aucune exception connue
    return null;
  }
}
