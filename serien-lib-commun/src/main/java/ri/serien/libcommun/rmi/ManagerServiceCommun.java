/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.rmi;

import java.lang.reflect.Proxy;
import java.util.List;

import ri.serien.libcommun.commun.planification.IdPlanification;
import ri.serien.libcommun.commun.planification.ListePlanification;
import ri.serien.libcommun.commun.planification.Planification;
import ri.serien.libcommun.exploitation.configurationtableau.ConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.CritereConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.IdConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.ListeConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.colonneconfigurationtableau.CritereConfigurationColonne;
import ri.serien.libcommun.exploitation.configurationtableau.colonneconfigurationtableau.ListeColonneConfigurationTableau;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.CritereContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.IdLienContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.LienContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.ListeLienContact;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Gestionnaire d'accès au service commun.
 *
 * L'objectif principal de cette classe est de capturer les exceptions remontées par les appels RMI afin de les convertir en
 * MessageErreurException. L'appelant n'a donc pas à se soucier du traitement des erreurs. Si le service retourne un résultat, c'est
 * qu'il a réussi. Sinon, il retourne une MessageErreurException qui sera traitée à haut niveau par le logiciel.
 */
public class ManagerServiceCommun {
  private static InterfaceServiceCommun service;
  
  /**
   * Retourner service dédié à la gestion des méthodes communes.
   */
  private static InterfaceServiceCommun getService() {
    if (service == null) {
      InterfaceServiceCommun interfaceService =
          (InterfaceServiceCommun) ManagerClientRMI.recupererService(InterfaceServiceCommun.NOMSERVICE);
      
      service = (InterfaceServiceCommun) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceCommun.class }, new InvocationHandlerClientRMI(interfaceService));
    }
    return service;
  }
  
  public static boolean ping() {
    try {
      return getService().ping();
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- Les contacts --------------------------------------------------------------------------------------------------------------------
  
  public static List<IdContact> chargerListeIdContact(IdSession pIdSession, CritereContact pCritereContact) {
    try {
      return getService().chargerListeIdContact(pIdSession, pCritereContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeContact chargerListeContact(IdSession pIdSession, List<IdContact> pListeIdContact) {
    try {
      return getService().chargerListeContact(pIdSession, pListeIdContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeContact chargerListeContact(IdSession pIdSession, CritereContact pCritereContact) {
    try {
      return getService().chargerListeContact(pIdSession, pCritereContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Contact chargerContact(IdSession pIdSession, IdContact pIdContact) {
    try {
      return getService().chargerContact(pIdSession, pIdContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLienContact chargerListeLienContact(IdSession pIdSession, List<IdLienContact> pListeIdLienContact) {
    try {
      return getService().chargerListeLienContact(pIdSession, pListeIdLienContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static LienContact chargerLienContact(IdSession pIdSession, IdLienContact pIdLienContact) {
    try {
      return getService().chargerLienContact(pIdSession, pIdLienContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLienContact chargerListeLienContactTiers(IdSession pIdSession, List<IdContact> pListeIdContact) {
    try {
      return getService().chargerListeLienContactTiers(pIdSession, pListeIdContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeLienContact chargerListeLienContactTiers(IdSession pIdSession, IdContact pIdContact) {
    try {
      return getService().chargerListeLienContactTiers(pIdSession, pIdContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Contact chargerContactPrincipalClient(IdSession pIdSession, IdClient pIdClient) {
    try {
      return getService().chargerContactPrincipalClient(pIdSession, pIdClient);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Contact chargerContactPrincipalFournisseur(IdSession pIdSession, IdFournisseur pIdFournisseur) {
    try {
      return getService().chargerContactPrincipalFournisseur(pIdSession, pIdFournisseur);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static IdContact chercherPremiereIdContactLibre(IdSession pIdSession) {
    try {
      return getService().chercherPremiereIdContactLibre(pIdSession);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void sauverContact(IdSession pIdSession, Contact pContact) {
    try {
      getService().sauverContact(pIdSession, pContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void supprimerContact(IdSession pIdSession, IdContact pIdContact) {
    try {
      getService().supprimerContact(pIdSession, pIdContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void desactiverContact(IdSession pIdSession, Contact pContact) {
    try {
      getService().desactiverContact(pIdSession, pContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static void activerContact(IdSession pIdSession, Contact pContact) {
    try {
      getService().activerContact(pIdSession, pContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static boolean controlerExistenceLienClientParticulier(IdSession pIdSession, IdLienContact pIdLienContact) {
    try {
      return getService().controlerExistenceLienClientParticulier(pIdSession, pIdLienContact);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeConfigurationTableau chargerListeConfigurationTableau(IdSession pIdSession,
      CritereConfigurationTableau pCritereConfigurationTableau) {
    try {
      return getService().chargerListeConfigurationTableau(pIdSession, pCritereConfigurationTableau);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ConfigurationTableau chargerConfigurationTableau(IdSession pIdSession, IdConfigurationTableau pIdConfigurationTableau) {
    try {
      return getService().chargerConfigurationTableau(pIdSession, pIdConfigurationTableau);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ListeColonneConfigurationTableau chargerListeConfigurationColonne(IdSession pIdSession,
      CritereConfigurationColonne pCritereConfigurationColonne) {
    try {
      return getService().chargerListeConfigurationColonne(pIdSession, pCritereConfigurationColonne);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static ConfigurationTableau sauverConfigurationTableau(IdSession pIdSession, ConfigurationTableau pConfigurationTableau) {
    try {
      return getService().sauverConfigurationTableau(pIdSession, pConfigurationTableau);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  // -- La planification ----------------------------------------------------------------------------------------------------------------
  
  public static ListePlanification chargerListePlanification(IdSession pIdSession, List<IdPlanification> pListeIdPlanification) {
    try {
      return getService().chargerListePlanification(pIdSession, pListeIdPlanification);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Planification chargerPlanification(IdSession pIdSession, IdPlanification pIdPlanification) {
    try {
      return getService().chargerPlanification(pIdSession, pIdPlanification);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
  public static Planification sauverPlanification(IdSession pIdSession, Planification pPlanification) {
    try {
      return getService().sauverPlanification(pIdSession, pPlanification);
    }
    catch (Exception e) {
      throw ManagerClientRMI.genererMessageErreurException(e);
    }
  }
  
}
