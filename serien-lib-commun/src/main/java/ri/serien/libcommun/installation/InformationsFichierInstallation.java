/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.installation;

import java.io.Serializable;

/**
 * Cette classe ne doit être utilisée que par la procédure d'installation de Série N (DemarrageSerieN)
 */
public class InformationsFichierInstallation implements Serializable {
  // Variables
  private String nom = "";
  private long taille = 0;
  private long derniereModification = 0;
  
  // -- Accesseurs
  
  public String getNom() {
    return nom;
  }
  
  public void setNom(String nom) {
    this.nom = nom;
  }
  
  public long getTaille() {
    return taille;
  }
  
  public void setTaille(long taille) {
    this.taille = taille;
  }
  
  public long getDerniereModification() {
    return derniereModification;
  }
  
  public void setDerniereModification(long derniereModification) {
    this.derniereModification = derniereModification;
  }
}
