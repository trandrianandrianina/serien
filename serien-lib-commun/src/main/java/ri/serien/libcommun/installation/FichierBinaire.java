/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.installation;

/**
 * Cette classe ne doit être utilisée que par la procédure d'installation de Série N (DemarrageSerieN).
 * Cette classe n'est pas librement modifiable.
 */
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;

import ri.serien.libcommun.outils.Constantes;

/**
 * Gestion des fichiers byte.
 */
public class FichierBinaire implements Serializable {
  // Constantes erreurs de chargement
  private static final String ERREUR_FICHIER_INTROUVABLE = "Le fichier est introuvable.";
  private static final String ERREUR_LECTURE_FICHIER = "Erreur lors de la lecture du fichier.";
  // Variables
  private String nomFichier = null;
  protected byte[] contenuFichier = null;
  protected File fichier = null;
  
  protected String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur de la classe.
   */
  public FichierBinaire() {
  }
  
  /**
   * Constructeur de la classe.
   */
  public FichierBinaire(String nomFichier) {
    setNomFichier(nomFichier);
  }
  
  /**
   * Constructeur de la classe.
   */
  public FichierBinaire(File nomFichier) {
    setNomFichier(nomFichier);
  }
  
  /**
   * Initialise le nom du fichier.
   */
  public void setNomFichier(String nomFichier) {
    setNomFichier(new File(nomFichier));
  }
  
  /**
   * Initialise le nom du fichier.
   */
  public void setNomFichier(File afichier) {
    if (afichier == null) {
      fichier = null;
      nomFichier = null;
      return;
    }
    nomFichier = afichier.getAbsolutePath();
    fichier = afichier;
    videContenuFichier();
  }
  
  /**
   * Change le nom du fichier.
   */
  public void changeNomFichier(String newNomFichier) {
    if (newNomFichier == null) {
      fichier = null;
      nomFichier = null;
    }
    nomFichier = newNomFichier;
    fichier = new File(nomFichier);
  }
  
  /**
   * Retourne le nom du fichier.
   */
  public String getNomFichier() {
    return nomFichier;
  }
  
  /**
   * Vérifie l'existence d'un fichier.
   */
  private boolean isPresent() {
    if (fichier == null) {
      return false;
    }
    return fichier.exists();
  }
  
  /**
   * Vide la contenu du buffer.
   */
  public void videContenuFichier() {
    this.contenuFichier = null;
  }
  
  /**
   * Initialise le buffer.
   */
  public void setContenuFichier(byte[] contenuFichier) {
    this.contenuFichier = contenuFichier;
  }
  
  /**
   * Retourne le buffer.
   */
  public byte[] getContenuFichier() {
    if (contenuFichier == null) {
      if (lectureFichier() == Constantes.ERREUR) {
        return null;
      }
    }
    
    return this.contenuFichier;
  }
  
  /**
   * Lecture du fichier texte.
   */
  public int lectureFichier() {
    // On vérifie que le fichier existe
    if (!isPresent()) {
      msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + ERREUR_FICHIER_INTROUVABLE;
      return Constantes.FALSE;
    }
    
    // On lit le fichier
    if (contenuFichier == null) {
      contenuFichier = new byte[(int) fichier.length()];
    }
    try {
      RandomAccessFile raf = new RandomAccessFile(nomFichier, "r");
      raf.read(contenuFichier);// , 0, Constantes.TAILLE_BUFFER);
      raf.close();
    }
    catch (Exception e) {
      msgErreur = ERREUR_LECTURE_FICHIER + Constantes.crlf + nomFichier + Constantes.crlf + e;
      return Constantes.FALSE;
    }
    
    return Constantes.OK;
  }
  
  /**
   * Retourne le file.
   */
  public File getFile() {
    return fichier;
  }
  
  /**
   * Ecrit le fichier.
   */
  public int ecritureFichier() {
    if ((contenuFichier == null) || (contenuFichier.length == 0)) {
      return Constantes.ERREUR;
    }
    
    // Vérification du chemin
    if (!fichier.getParentFile().exists()) {
      fichier.getParentFile().mkdirs();
    }
    
    // Ecriture du contenu
    try {
      DataOutputStream f = new DataOutputStream(new FileOutputStream(fichier));
      f.write(contenuFichier, 0, contenuFichier.length);
      f.close();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return Constantes.OK;
  }
  
  /**
   * Initialise les erreurs.
   */
  public void addMsgErreur(String msg) {
    if (msgErreur.trim().length() == 0) {
      msgErreur = msg;
    }
    else {
      msgErreur += '\n' + msg;
    }
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    // La récupération du message est à usage unique
    String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
