/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier;

import com.google.gson.Gson;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.commun.objetmetier.champmetier.ChampMetier;
import ri.serien.libcommun.commun.objetmetier.champmetier.IdChampMetier;
import ri.serien.libcommun.commun.objetmetier.champmetier.InterfacePersistanceChampMetier;
import ri.serien.libcommun.commun.objetmetier.champmetier.ListeChampMetier;
import ri.serien.libcommun.commun.objetmetier.champmetier.ManagerChampMetier;
import ri.serien.libcommun.outils.Trace;

/**
 * Objet métier.
 * 
 * Cette classe permet de décrire un objet métier : libellé, liste de ses champs. Ces informations sont utilisés par les composants
 * génériques pour adapter leur comportement en fonction de l'objet métier.
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class ObjetMetier extends AbstractClasseMetier<IdObjetMetier> {
  private final String libelle;
  private final String trigramme;
  private final ListeChampMetier listeChampMetier;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur de l'objet métier.
   * 
   * C'est le seul contructeur valide car l'objet métier doit être renseigné à partir de l'énumération dédiée à la persistance.
   * Le constructeur est protégé de façon à ce que seul ManagerObjetMetier puisse l'utiliser.
   * 
   * @param pEnumPersistanceObjetMetier Enumération de persistance de l'objet métier.
   */
  protected ObjetMetier(EnumPersistanceObjetMetier pEnumPersistanceObjetMetier) {
    super(IdObjetMetier.getInstance(pEnumPersistanceObjetMetier.getEnumObjetMetier()));
    libelle = pEnumPersistanceObjetMetier.getLibelle();
    trigramme = pEnumPersistanceObjetMetier.getTrigramme();
    
    // Construire la liste des champs métiers
    listeChampMetier = new ListeChampMetier();
    InterfacePersistanceChampMetier[] listeEnumPersistanceChampMetier = pEnumPersistanceObjetMetier.getListeEnumPersistanceChampMetier();
    for (InterfacePersistanceChampMetier enumPersistanceChampMetier : listeEnumPersistanceChampMetier) {
      ChampMetier champMetier = ManagerChampMetier.getInstance().creerChampMetier(enumPersistanceChampMetier);
      listeChampMetier.add(champMetier);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public String getTexte() {
    return libelle;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Afficher le détail de l'objet métier dans les traces.
   */
  public void tracer() {
    Gson gson = new Gson();
    String json = gson.toJson(this);
    Trace.info(json);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le libellé de l'objet métier.
   * @return Libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner le trigramme de l'objet métier.
   * @return Trigramme.
   */
  public String getTrigramme() {
    return trigramme;
  }
  
  /**
   * Retourner la liste des champs de l'objet métier.
   * @return Liste de champs métiers.
   */
  public ListeChampMetier getListeChampMetier() {
    return listeChampMetier;
  }
  
  /**
   * Retouner un champ métier de l'objet métier par son identifiant.
   * @param pIdChampMetier Identifiant du champ métier.
   * @return Champ métier (null si pas trouvé).
   */
  public ChampMetier getChampMetier(IdChampMetier pIdChampMetier) {
    if (listeChampMetier == null) {
      return null;
    }
    return listeChampMetier.get(pIdChampMetier);
  }
}
