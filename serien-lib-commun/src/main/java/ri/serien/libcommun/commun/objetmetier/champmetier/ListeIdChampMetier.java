/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier.champmetier;

import java.util.ArrayList;

/**
 * Liste d'identifiant de champs métiers.
 * 
 * Cette classe est enssentiellement un héritage de ArrayList contenant une liste de IdChampMetier. De fait, les entrées sont
 * conservées dans l'ordre où elles sont insérées et il peut y avoir plusieurs fois la même entrée. Je ne sais pas si cette dernière
 * propriété est utile en pratique (si on voulait garantir l'unicité des entrées, on pourrait utiliser LinkedHashSet).
 * 
 * Elle ajoute la possibilité de manipuler les entrées de la liste via l'énumération d'un champ métier. L'énumération d'un champ métier
 * est en effet une manière de désigner un IdChampMetier via un nom plus parlant. Dans l'exemple ci-dessous, on constate que la
 * deuxième version de la méthode add() est plus simple :
 *
 * Exemple :
 * ListeIdChampMetier liste = new ListeIdChampMetier();
 * liste.add(IdChampMetier.getInstance(EnumChampClient.NOM);
 * liste.add(EnumChampClient.NOM);
 **/
public class ListeIdChampMetier extends ArrayList<IdChampMetier> {
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Créer une liste d'identifiants de champs métiers à partir d'un tableau d'énumérations.
   * @param pListeInterfaceEnumChampMetier Tableau d'énumérations de champs métiers.
   */
  public ListeIdChampMetier(InterfaceEnumChampMetier[] pListeInterfaceEnumChampMetier) {
    addAll(pListeInterfaceEnumChampMetier);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Ajouter un identifiant de champ métier via son énumération.
   * @param pEnumChampMetier Enumération du champ métier.
   * @return true=si la collection a changé, false=la collection n'a pas changé.
   */
  public boolean add(InterfaceEnumChampMetier pEnumChampMetier) {
    return add(IdChampMetier.getInstance(pEnumChampMetier));
  }
  
  /**
   * Ajouter une liste d'identifiants de champs métiers à partir d'un tableau d'énumérations.
   * @param pListeInterfaceEnumChampMetier Tableau d'énumérations de champs métiers.
   * @return true=si la collection a changé, false=la collection n'a pas changé.
   */
  public boolean addAll(InterfaceEnumChampMetier[] pListeInterfaceEnumChampMetier) {
    if (pListeInterfaceEnumChampMetier == null) {
      return false;
    }
    boolean modifie = false;
    for (InterfaceEnumChampMetier enumChampMetier : pListeInterfaceEnumChampMetier) {
      modifie |= add(enumChampMetier);
    }
    return modifie;
  }
  
  /**
   * Enlever un identifiant de champ métier via son énumération.
   * @param pEnumChampMetier Enumération du champ métier.
   * @return true=si la collection a changé, false=la collection n'a pas changé.
   */
  public boolean remove(InterfaceEnumChampMetier pEnumChampMetier) {
    return remove(IdChampMetier.getInstance(pEnumChampMetier));
  }
  
  /**
   * Enlever une liste d'identifiants de champs métiers à partir d'un tableau d'énumérations.
   * @param pListeInterfaceEnumChampMetier Tableau d'énumérations de champs métiers.
   * @return true=si la collection a changé, false=la collection n'a pas changé.
   */
  public boolean removeAll(InterfaceEnumChampMetier[] pListeInterfaceEnumChampMetier) {
    if (pListeInterfaceEnumChampMetier == null) {
      return false;
    }
    boolean modifie = false;
    for (InterfaceEnumChampMetier enumChampMetier : pListeInterfaceEnumChampMetier) {
      modifie |= remove(enumChampMetier);
    }
    return modifie;
  }
  
  /**
   * Indiquer si la liste contient l'identifiant de champ métier correspondant à l'énumération fournie en parmaètre.
   * @param pEnumChampMetier Enumération du champ métier.
   * @return true=identifiant présent, false=identifiant non présent.
   */
  public boolean contains(InterfaceEnumChampMetier pEnumChampMetier) {
    return contains(IdChampMetier.getInstance(pEnumChampMetier));
  }
}
