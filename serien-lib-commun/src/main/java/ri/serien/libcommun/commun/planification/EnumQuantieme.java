/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.planification;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Valeurs possibles pour les quantièmes.
 */
public enum EnumQuantieme {
  PREMIER(1, "Premier"),
  DERNIER(2, "Dernier");
  
  private final Integer numero;
  private final String libelle;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pNumero Le numéro du quantième.
   * @param pLibelle Le libellé.
   */
  EnumQuantieme(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourne le libellé associé dans une chaîne de caractère.
   * @return Le libellé.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'objet énum par son numéro.
   * @param pNumero Le numéro du quantième.
   * @return L'énum.
   */
  public static EnumQuantieme valueOfByNumero(Integer pNumero) {
    for (EnumQuantieme value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le quantième est invalide : " + pNumero);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Le numéro sous lequel la valeur est persistée en base de données.
   * @return Le numéro.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au quantième.
   * @return La libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
}
