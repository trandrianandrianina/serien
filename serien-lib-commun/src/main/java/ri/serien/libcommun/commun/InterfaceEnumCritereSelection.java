/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun;

/**
 * Interface pour les énumérations qui servent de critères de filtrage aux objets métiers.
 * 
 * Il doit y avoir une implémentation de cette interface pour chaque énumération servant de critères de filtrage d'un objet métier.
 * Voir ri.serien.libcommun.exploitation.flux.EnumStatutFlux pour exemple.
 */
public interface InterfaceEnumCritereSelection {
  
  /**
   * Retourner le libellé de l'énumération.
   * @return Le libellé.
   */
  public String getLibelle();
  
  /**
   * Retourner la valeur persistée en base de l'énumération.
   * @return La valeur persistée en base.
   */
  public Object getValeurPersiste();
  
  /**
   * Retourner l'énumération correspondant à la valeur persistée en base.
   * @param pValeurPersiste La valeur persistée en base.
   * @return L'énumération correspondant.
   */
  public InterfaceEnumCritereSelection getEnumByValeurPersiste(Object pValeurPersiste);
}
