/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les modes d'utilisation des écrans.
 */
public enum EnumModeUtilisation {
  CONSULTATION(0, "Consultation"),
  CREATION(1, "Création"),
  MODIFICATION(2, "Modification"),
  DUPLICATION(3, "Duplication"),
  ANNULATION(4, "Annulation"),
  SUPPRESSION(5, "Suppression"),
  RECHERCHE(6, "Recherche");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumModeUtilisation(Integer pNumero, String pLibelle) {
    code = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numéro sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumModeUtilisation valueOfByCode(Integer pNumero) {
    for (EnumModeUtilisation value : values()) {
      if (pNumero == value.getCode()) {
        return value;
      }
    }
    throw new MessageErreurException("Le mode d'utilisation est invalide : " + pNumero);
  }
  
}
