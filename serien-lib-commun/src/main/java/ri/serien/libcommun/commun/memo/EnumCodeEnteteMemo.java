/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.memo;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Code entête du mémo.
 * Ce code permet de définir à quelques types d'objet métier est rattaché le mémo. Les codes pour les entêtes sont en majuscules
 * tandis que ceux pour les lignes sont en majuscules (voir EnumCodeLigneMemo).
 */
public enum EnumCodeEnteteMemo {
  ARTICLE("A", "Article"),
  DEVIS("D", "Devis ou chantier"),
  DOCUMENT("E", "Document");
  
  private final String code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumCodeEnteteMemo(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumCodeEnteteMemo valueOfByCode(String pCode) {
    for (EnumCodeEnteteMemo value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code entête du mémo est invalide : " + pCode);
  }
}
