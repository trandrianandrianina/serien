/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.planification;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Valeurs possibles pour la périodicité d'une planification.
 */
public enum EnumPlanificationPeriodicite {
  AUCUNE('U', "Aucune"),
  JOURNALIERE('J', "Journalière"),
  HEBDOMADAIRE('S', "Hebdomadaire"),
  MENSUELLE('M', "Mensuelle"),
  ANNUELLE('A', "Annuelle");
  
  private final Character code;
  private final String libelle;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pCode Le code.
   * @param pLibelle Le libellé.
   */
  EnumPlanificationPeriodicite(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourne le numéro associé dans une chaîne de caractère.
   * @return Le libellé.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'objet énum par son code.
   * @param pCode Le code.
   * @return L'énum.
   */
  public static EnumPlanificationPeriodicite valueOfByCode(Character pCode) {
    for (EnumPlanificationPeriodicite value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("La périodicité est invalide : " + pCode);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   * @return Le code.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   * @return La libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
}
