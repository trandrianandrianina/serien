/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.message;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Message associé à un niveau d'importance.
 * 
 * Les messages normaux sont destinés à être affichés en noir tandis que les messages importants sont affichés en rouge.
 * Il est possible de formatter le texte en html.
 */
public class Message {
  final private EnumImportanceMessage importanceMessage;
  final private String texte;
  
  /**
   * Constructeur permettant de définir un message avec son niveau d'importance normale au format choisi.
   */
  private Message(String pTexte, EnumImportanceMessage pImportanceMessage) {
    if (pImportanceMessage == null) {
      throw new MessageErreurException("L'importance du message est invalide.");
    }
    texte = Constantes.normerTexte(pTexte);
    importanceMessage = pImportanceMessage;
  }
  
  /**
   * Créer un message d'importance normale.
   */
  static public Message getMessageNormal(String pTexte) {
    return new Message(pTexte, EnumImportanceMessage.NORMAL);
  }
  
  /**
   * Créer un message d'importance moyenne.
   */
  static public Message getMessageMoyen(String pTexte) {
    return new Message(pTexte, EnumImportanceMessage.MOYEN);
  }
  
  /**
   * Créer un message important.
   */
  static public Message getMessageImportant(String pTexte) {
    return new Message(pTexte, EnumImportanceMessage.HAUT);
  }
  
  /**
   * Créer un message en précisant le niveau d'importance.
   */
  static public Message getMessage(String pTexte, EnumImportanceMessage pEnumImportanceMessage) {
    return new Message(pTexte, pEnumImportanceMessage);
  }
  
  /**
   * Texte du message.
   */
  public String getTexte() {
    return texte;
  }
  
  /**
   * Importance du message : normal ou important.
   */
  public EnumImportanceMessage getImportanceMessage() {
    return importanceMessage;
  }
  
  /**
   * Indique si le message est d'importance haute.
   */
  public boolean isImportanceHaute() {
    return importanceMessage.equals(EnumImportanceMessage.HAUT);
  }
}
