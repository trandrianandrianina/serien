/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.champbdd;

import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;

/**
 * Interface pour la description des champs de la base de données.
 * 
 * Cette interface est à implémenter par les énumérations qui décrivent la liste des champs d'une table de la base de données.
 * Idéalement, on aurait pu décrire l'ensemble des champs de la base de données dans un énorme Enum mais nous allions atteindre une
 * limite technique de Java sur la taille de l'Enum. La base de données comportent plusieurs dizaines de milliers de champs. Nous avons
 * donc choisit de faire un Enum par table, qui hérite de cette interface.
 * 
 * L'objectif est de fournir les informations techniques du champ tel qu'il est persisté dans la base de données. A terme, on souhaite
 * pouvoir générer ces classes automatiquement à partir de la base de données. Il ne faut donc pas y mettre des attributs qui
 * nécessite une intervention manuelle. Il faut y mettre des informations qui sont récupérables à partir de la base de données.
 * 
 * Il doit y avoir une implémentation de cette interface pour chaque table de la base de données.
 */
public interface InterfaceChampBDD {
  /**
   * Retourner la table de base de données dont ce champ est constitutif.
   * @return EnumTableBDD Table de la base de données.
   */
  public EnumTableBDD getTableBDD();
  
  /**
   * Retourner le nom technique du champ dans la table.
   * Il s'agit du nom technique du champ dans la table afin d'être utilisé dans des requêtes SQL.
   * @return String Nom du champ.
   */
  public String getNom();
  
  /**
   * Retourner la longueur maximale du champ dans la table.
   * Il s'agit ici de la longueur technique du champ dans la base de données.
   * @return int Longueur du champ.
   */
  public Integer getLongueur();
  
  /**
   * Retourner le type du champ dans la table.
   * @return Object Le type du champ.
   */
  public Class getType();
  
}
