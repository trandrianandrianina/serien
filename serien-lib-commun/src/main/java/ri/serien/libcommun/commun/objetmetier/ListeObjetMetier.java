/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Liste d'objets métiers.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de ObjetMetier.
 */
public class ListeObjetMetier extends ListeClasseMetier<IdObjetMetier, ObjetMetier, ListeObjetMetier> {
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public ListeObjetMetier() {
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Inutilisé.
   */
  @Override
  public ListeObjetMetier charger(IdSession pIdSession, List<IdObjetMetier> pListeId) {
    return null;
  }
  
  /**
   * Charger l'ensemble des objets métiers.
   */
  @Override
  public ListeObjetMetier charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return ManagerObjetMetier.getInstance().getListeObjetMetier();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner un objet métier de la liste via son énumération.
   * @param pEnumObjetMetier Enumération de l'objet métier
   * @return Objet métier (null si pas trouvé).
   */
  public ObjetMetier get(EnumObjetMetier pEnumObjetMetier) {
    if (pEnumObjetMetier == null) {
      return null;
    }
    return get(IdObjetMetier.getInstance(pEnumObjetMetier));
  }
}
