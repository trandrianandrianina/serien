/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.memo;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type de mémo.
 */
public enum EnumTypeMemo {
  AUCUN("", "Aucun"),
  VENTE("VTE", "Vente"),
  INFORMATION_TECHNICOCOMMERCIALE("ITC", "Information technicocommerciale"),
  LIVRAISON("*LV", "Livraison/Enlèvement");
  
  private final String code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeMemo(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeMemo valueOfByCode(String pCode) {
    for (EnumTypeMemo value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de mémo est invalide : " + pCode);
  }
}
