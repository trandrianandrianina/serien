/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.recherche;

import java.util.ArrayList;

/**
 * Liste d'arguments de recherche.
 */
public class ListeArgumentRecherche extends ArrayList<ArgumentRecherche> {
  /**
   * Constructeur.
   */
  public ListeArgumentRecherche() {
  }
}
