/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.champbdd;

import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;

/**
 * Liste les champs pour la table export.
 */
public enum EnumChampPSEMTEXPM implements InterfaceChampBDD {
  ID_ETABLISSEMENT("EXETB", 3, String.class),
  NUMERO("EXID", 10, Integer.class),
  ID_OBJET_METIER("EXOMID", 10, Integer.class),
  PROFIL_UTILISATEUR("EXPRF", 10, String.class),
  DESCRIPTION("EXDESC", 250, String.class),
  DATE_CREATION("EXDCRE", 7, Integer.class),
  DATE_MODIFICATION("EXDMOD", 7, Integer.class),
  DATE_DERNIER_EXPORT("EXDEXP", 7, Integer.class),
  ID_PLANIFICATION("EXPLID", 10, Integer.class);
  
  private final EnumTableBDD tableBDD = EnumTableBDD.EXPORT;
  private final String nom;
  private final Integer longueur;
  private final Class type;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  EnumChampPSEMTEXPM(String pNom, Integer pLongueur, Class pType) {
    nom = pNom;
    longueur = pLongueur;
    type = pType;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le nom du champ précédé du nom de sa table.
   */
  @Override
  public String toString() {
    return tableBDD.getNom() + "." + nom;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner la table de base de données dont ce champ est constitutif.
   * @return EnumTableBDD Table de la base de données.
   */
  @Override
  public EnumTableBDD getTableBDD() {
    return tableBDD;
  }
  
  /**
   * Retourner le nom du champ dans la table.
   * Il s'agit du nom technique du champ dans la table afin d'être utilisé dans des requêtes SQL.
   * @return String Nom du champ.
   */
  @Override
  public String getNom() {
    return nom;
  }
  
  /**
   * Retourner la longueur maximale du champ dans la table.
   * Il s'agit ici de la longueur technique du champ dans la base de données.
   * @return int Longueur du champ.
   */
  @Override
  public Integer getLongueur() {
    return longueur;
  }
  
  /**
   * Retourner le type du champ dans la table.
   * @return Class Le type du champ.
   */
  @Override
  public Class getType() {
    return type;
  }
  
}
