/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.planification;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe décrit l'identifiant unique d'une planification et regroupe les méthodes qui lui sont associées.
 */
public class IdPlanification extends AbstractIdAvecEtablissement {
  
  private final Integer numero;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé pour un identifiant complet.
   * @param pEnumEtatObjetMetier L'état de l'objet métier.
   * @param pIdEtablissement L'identifiant de l'établissement.
   * @param pNumero Le numéro de la planification.
   */
  private IdPlanification(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, Integer pNumero) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Constructeur privé pour un identifiant en cours de création.
   * @param pIdEtablissement L'identifiant de l'établissement.
   */
  private IdPlanification(IdEtablissement pIdEtablissement) {
    super(EnumEtatObjetMetier.CREE, pIdEtablissement);
    numero = 0;
  }
  
  /**
   * Créer un identifiant à partir des informations sous forme éclatée.
   * @param pIdEtablissement L'identifiant de l'établissement.
   * @param pNumero Le numéro de la planification.
   * @return L'identifiant de la planification.
   */
  public static IdPlanification getInstance(IdEtablissement pIdEtablissement, Integer pNumero) {
    return new IdPlanification(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pNumero);
  }
  
  /**
   * Créer un nouvel identifiant pas encore persisté en base de données.
   * @param pIdEtablissement L'identifiant de l'établissement.
   * @return L'identifiant de la planification.
   */
  public static IdPlanification getInstanceAvecCreationId(IdEtablissement pIdEtablissement) {
    return new IdPlanification(pIdEtablissement);
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + numero.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdPlanification)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de planification.");
    }
    IdPlanification id = (IdPlanification) pObject;
    return Constantes.equals(getCodeEtablissement(), id.getCodeEtablissement()) && Constantes.equals(numero, id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdPlanification)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdPlanification id = (IdPlanification) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return "" + numero;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Contrôler la validité du numéro de la planification.
   * Le numéro de la planification doit être défini et supérieur à 0.
   * @param pNumero Numéro de la planification.
   */
  private static Integer controlerNumero(Integer pNumero) {
    if (pNumero == null) {
      throw new MessageErreurException("Le numéro de la planification n'est pas renseigné.");
    }
    if (pNumero <= 0) {
      throw new MessageErreurException("Le numéro de la planification est inférieur ou égal à zéro.");
    }
    return pNumero;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Numéro de la planification.
   * Le numéro de la planification est supérieur ou égal à 1. Il peut être égal à 0 pour une planification en cours de création.
   * @return Numéro de la planification.
   */
  public Integer getNumero() {
    return numero;
  }
}
