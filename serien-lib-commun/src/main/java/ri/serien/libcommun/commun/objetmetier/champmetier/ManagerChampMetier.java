/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier.champmetier;

import ri.serien.libcommun.commun.objetmetier.EnumObjetMetier;
import ri.serien.libcommun.commun.objetmetier.IdObjetMetier;
import ri.serien.libcommun.commun.objetmetier.ManagerObjetMetier;
import ri.serien.libcommun.commun.objetmetier.ObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Création des instances en relations avec les champs métiers.
 * 
 * Cette classes est un singleton. Elle ne dispose donc que d'une seule instance accessible via la méthode getInstance().
 * Elle a pour objectif de fournir les instances des classes en relation avec les champs métiers (ChampMetier, ListeChampMetier).
 * 
 * Cette classe construit la liste des objets métiers une fois pour toute lors de son premier appel. Les objets retournés ensuite
 * proviennent de cette liste.
 */
public class ManagerChampMetier {
  private static final ManagerChampMetier INSTANCE = new ManagerChampMetier();
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé pour garantir le singleton.
   */
  private ManagerChampMetier() {
  }
  
  /**
   * Retourner l'instance unique de cette classe.
   * @return Instance de la classse.
   */
  public static ManagerChampMetier getInstance() {
    return INSTANCE;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner un champ métier à partir de son identifiant.
   * @param pIdChampMetier Identifiant du champ métier.
   * @return Champ métier.
   */
  public ChampMetier getChampMetier(IdChampMetier pIdChampMetier) {
    // Tester les paramètres
    if (pIdChampMetier == null) {
      throw new MessageErreurException("L'identifiant du champ métier est invalide.");
    }
    
    // Récupérer l'objet métier
    ObjetMetier objetMetier = ManagerObjetMetier.getInstance().getObjetMetier(pIdChampMetier.getIdObjetMetier());
    if (objetMetier == null) {
      throw new MessageErreurException("L'identifiant du champ métier ne fait pas référence à un objet métier connu.");
    }
    
    // Récupérer le champ métier
    ChampMetier champMetier = objetMetier.getChampMetier(pIdChampMetier);
    if (champMetier == null) {
      throw new MessageErreurException("L'identifiant du champ métier ne fait pas référence à un champ métier connu.");
    }
    
    return champMetier;
  }
  
  /**
   * Retourner un champ métier à partir de son énumération.
   * @param pEnumObjetMetier Enunération du champ métier.
   * @return Champ métier.
   */
  public ChampMetier getChampMetier(InterfaceEnumChampMetier pEnumChampMetier) {
    // Tester les paramètres
    if (pEnumChampMetier == null) {
      throw new MessageErreurException("L'énumération du champ métier est invalide.");
    }
    
    // Récupérer le champ métier
    return getChampMetier(IdChampMetier.getInstance(pEnumChampMetier));
  }
  
  /**
   * Créer un champ métier à partir de l'énumération de sa persistance.
   * 
   * Cette méthode ne doit pas être utilisée directement dans le code. Son usage est réservée à ObjetMetier qui l'utilise pour
   * initialiser la liste des champs métiers des objets métiers. Utiliser les méthodes getChampMetier(...).
   * 
   * @param pEnumPersistanceChampMetier Persistance du champ métier.
   * @return Champ métier.
   */
  public ChampMetier creerChampMetier(InterfacePersistanceChampMetier pEnumPersistanceChampMetier) {
    return new ChampMetier(pEnumPersistanceChampMetier);
  }
  
  /**
   * Retourner la liste des champs d'un objet métier à partir de l'identifiant de l'objet métier.
   * @param pIdObjetMetier Identifiant de l'objet métier.
   * @return Liste de champs métiers.
   */
  public ListeChampMetier getListeChampMetier(IdObjetMetier pIdObjetMetier) {
    // Tester les paramètres
    if (pIdObjetMetier == null) {
      throw new MessageErreurException("L'identifiant de l'objet métier est invalide.");
    }
    
    // Récupérer l'objet métier
    ObjetMetier objetMetier = ManagerObjetMetier.getInstance().getObjetMetier(pIdObjetMetier);
    if (objetMetier == null) {
      throw new MessageErreurException("L'identifiant de l'objet métier ne fait pas référence à un objet métier connu.");
    }
    
    // Retourner la liste des champs métiers de l'objet métier
    return objetMetier.getListeChampMetier();
  }
  
  /**
   * Retourner la liste des champs d'un objet métier à partir de l'énumération de l'objet métier.
   * @param pEnumObjetMetier Enumération de l'objet métier.
   * @return Liste de champs métiers.
   */
  public ListeChampMetier getListeChampMetier(EnumObjetMetier pEnumObjetMetier) {
    return getListeChampMetier(IdObjetMetier.getInstance(pEnumObjetMetier));
  }
  
  /**
   * Retourner une liste de champs métiers à partir d'une liste d'identifiant de champs métiers.
   * @param pListeIdChampMetier Liste d'identifiants de champs métiers.
   */
  public ListeChampMetier getListeChampMetier(ListeIdChampMetier pListeIdChampMetier) {
    // Tester les paramètres
    if (pListeIdChampMetier == null) {
      throw new MessageErreurException("La liste d'identifiants de champs métiers est invalide.");
    }
    
    // Créer et compélter la liste de champs métiers
    ListeChampMetier listeChampMetier = new ListeChampMetier();
    listeChampMetier.addAll(pListeIdChampMetier);
    return listeChampMetier;
  }
  
  /**
   * Retourner une liste de champs métiers à partir d'une liste d'énumérations de champs métiers.
   * @param pListeInterfaceEnumChampMetier Liste d'énumérations de champs métiers.
   */
  public ListeChampMetier getListeChampMetier(InterfaceEnumChampMetier[] pListeEnumChampMetier) {
    // Tester les paramètres
    if (pListeEnumChampMetier == null) {
      throw new MessageErreurException("La liste d'énumérations de champs métiers est invalide.");
    }
    
    // Créer et compélter la liste de champs métiers
    ListeChampMetier listeChampMetier = new ListeChampMetier();
    listeChampMetier.addAll(pListeEnumChampMetier);
    return listeChampMetier;
  }
}
