/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.id;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Classe de base de tous les identifiants comportant l'établissement.
 *
 * La classe est abstraite pour éviter son instanciation par erreur. Il n'y a pas de raison d'instancier directement cette classe
 * puisqu'elle n'existe que pour mutualiser la gestion du code établissement dans tous les identifiants.
 * Pour gérer l'identifiant d'un établissement, c'est la classe IdEtablissement qu'il faut utiliser.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public abstract class AbstractIdAvecEtablissement extends AbstractId {
  // Constantes
  public static final int LONGUEUR_CODE_ETABLISSEMENT = 3;
  
  // Variables
  private final IdEtablissement idEtablissement;
  
  /**
   * Constructeur.
   */
  protected AbstractIdAvecEtablissement(EnumEtatObjetMetier pEtatObjetMetier, IdEtablissement pIdEtablissement) {
    super(pEtatObjetMetier);
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement n'est pas renseigné.");
    }
    idEtablissement = pIdEtablissement;
  }
  
  // -- Accesseurs
  
  /**
   * Code établissement.
   * Le code établissement est toujours de 3 caractères alphanumériques. Il est complété de caractères espaces à droite si nécessaire.
   */
  public String getCodeEtablissement() {
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Id établissement.
   * Le code établissement est toujours de 3 caractères alphanumériques. Il est complété de caractères espaces à droite si nécessaire.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
}
