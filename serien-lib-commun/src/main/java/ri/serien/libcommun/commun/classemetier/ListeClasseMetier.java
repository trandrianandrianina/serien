/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.classemetier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Classe parente de toutes les listes d'objets métiers.
 * 
 * Cette classe mutualise ce qui est commun aux listes d'objets métiers.
 * 
 * Manipulation via l'id
 * ---------------------
 * Elle donne notamment la possibilité de manipuler les éléments de la liste via l'identifiant de l'objet métier (en plus de l'index et
 * de l'objet métier lui-même gérés par la classe ArrayList). Voici les méthodes concernées :
 * - objet get(pId);
 * - int indexOf(pId);
 * - boolean contains(pId);
 * 
 * Pagination
 * ----------
 * La classe gère le chargement paginé des objets métiers de la liste. Seuls les objets métiers dont les données n'ont pas été chargées
 * sont chargés lorsqu'on se déplace dans la liste.
 * 
 * Utilisation :
 * - Utiliser la méthode chargerPremierePage() après une nouvelle recherche :
 * liste.chargerPremierePage(getIdSession(), taillePage);
 * 
 * - Utiliser la méthode chargerPage() lorsque la zone d'affichage du tableau évolue :
 * if (liste != null) {
 * liste.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
 * }
 *
 * Sélection
 * ---------
 * La classe permet de gérer les sélections dans la liste. Il est possible de sélectionner un objet métier via son index, son
 * identifiant ou l'objet métier lui-même. Il y a deux types de méthodes pour modifier la sélection :
 * - Les méthodes selectionner() remplace la sélection existante.
 * - Les méthodes changerSelection() conserve la sélection existante et permute juste l'objet métier fourni en paramètre.
 * 
 * La sélection ne peut pas contenir d'objet null. Le code mettant à jour l'ensemble des objets sélectionnés (setSelection) doit
 * s'assurer qu'aucun null n'est ajouté. A contrario, l'utilisation de setSelection n'a pas besoin de contrôler la nullité de ses
 * éléments.
 */
public abstract class ListeClasseMetier<I extends AbstractId, O extends AbstractClasseMetier, L extends ListeClasseMetier>
    extends ArrayList<O> {
  private static final int TAILLE_MAX_PAGE = 30;
  
  private Set<O> setSelection = new HashSet<O>();
  private int indexVisibleDebut = -1;
  private int indexVisibleFin = -1;
  private int indexChargementDebut = -1;
  private int indexChargementFin = -1;
  private int taillePage = 0;
  
  /**
   * Constructeur par défaut.
   */
  public ListeClasseMetier() {
  }
  
  /**
   * Constructeur par copie d'une liste.
   */
  public ListeClasseMetier(List<O> pListeObjetmetier) {
    super(pListeObjetmetier);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes de manipulation des objets de la liste
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Supprimer tous les éléments de la liste.
   * La sélection est également effacées.
   */
  @Override
  public void clear() {
    super.clear();
    setSelection.clear();
  }
  
  /**
   * Retourner un objet métier de la liste à partir de son identifiant.
   * Le nom de méthode get() a été choisi par analogie avec la méthode ArrayList.get();
   */
  public O get(I pIdObjetMetier) {
    if (pIdObjetMetier == null) {
      return null;
    }
    for (O objetMetier : this) {
      if (objetMetier != null && objetMetier.getId().equals(pIdObjetMetier)) {
        return objetMetier;
      }
    }
    return null;
  }
  
  /**
   * Remplacer l'objet métier dans la liste.
   */
  public O set(O pObjetMetier) {
    if (pObjetMetier == null) {
      return null;
    }
    for (int i = 0; i < size(); i++) {
      O objetMetier = get(i);
      if (objetMetier != null && objetMetier.getId().equals(pObjetMetier.getId())) {
        return set(i, pObjetMetier);
      }
    }
    return null;
  }
  
  /**
   * Retourner l'index d'un objet métier de la liste à partir de son identifiant.
   * Le nom de méthode indexOf() a été choisi par analogie avec la méthode ArrayList.indexOf();
   */
  public int indexOf(I pIdObjetMetier) {
    if (pIdObjetMetier == null) {
      return -1;
    }
    for (int i = 0; i < size(); i++) {
      O objetMetier = get(i);
      if (objetMetier != null && objetMetier.getId().equals(pIdObjetMetier)) {
        return i;
      }
    }
    return -1;
  }
  
  /**
   * Tester si l'objet métier dont l'identifiant est fournit en paramètre est présent dans la liste.
   * Le nom de méthode contains() a été choisi par analogie avec la méthode ArrayList.contains();
   */
  public boolean contains(I pIdObjetMetier) {
    return get(pIdObjetMetier) != null;
  }
  
  /**
   * Tester si l'objet métier fournit en paramètre est présent dans la liste.
   * Le nom de méthode contains() a été choisi par analogie avec la méthode ArrayList.contains();
   */
  public boolean contains(O pObjetMetier) {
    if (pObjetMetier == null) {
      return false;
    }
    return get((I) pObjetMetier.getId()) != null;
  }
  
  /**
   * Supprimer un objet de la liste à partir de son index.
   * L'objet est également retiré de la sélection s'il y était.
   */
  @Override
  public O remove(int pIndex) {
    O objetMetier = super.remove(pIndex);
    if (objetMetier != null) {
      setSelection.remove(objetMetier);
    }
    return objetMetier;
  }
  
  /**
   * Supprimer un objet de la liste.
   * L'objet est également retiré de la sélection s'il y était.
   */
  @Override
  public boolean remove(Object pObjetMetier) {
    if (pObjetMetier != null) {
      setSelection.remove(pObjetMetier);
    }
    return super.remove(pObjetMetier);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes de chargement/sauvegarde des objets métiers
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Charger une liste d'objets métiers à partir d'une liste d'identifiant fournie en paramètre.
   * 
   * La liste des objets métiers chargés est retournée en paramètre.
   * Cette méthode doit être surchargée dans chaque classe enfant. C'est l'unique moyen qui doit être implémenté pour charger un objet
   * métier à partir de son identifiant.
   */
  public abstract L charger(IdSession pIdSession, List<I> pListeId);
  
  /**
   * Charger la liste complète des objets métiers pour l'établissement donné.
   * Retourne null si il n'existe aucun objet métier.
   */
  public abstract L charger(IdSession pIdSession, IdEtablissement pIdEtablissement);
  
  /**
   * Charger un objet métier à partir de son identifiant.
   * Retourne null si l'identifiant ne correspond à aucun objet métier.
   */
  public O charger(IdSession pIdSession, I pId) {
    // Créer la liste d'identifiants passée en paramètre
    List<I> listeId = new ArrayList<I>();
    listeId.add(pId);
    
    // Charger l'objet métier
    L listeObjetMetier = charger(pIdSession, listeId);
    
    // Retourner l'objet métier s'il a été trouvé
    if (listeObjetMetier == null || listeObjetMetier.isEmpty()) {
      return null;
    }
    return (O) listeObjetMetier.get(0);
  }
  
  /**
   * Afficher la première page du tableau.
   * 
   * Cette méthode s'assure que les objets métiers de la première page du tableau sont correctement chargés. La hauteur de la première
   * page est fixée par défaut à 50. Les objets métiers sont chargés uniquement si cela n'a pas déjà été fait. La méthode retourne la
   * liste des objets dont le contenu vient d'être chargé.
   * 
   * Utiliser cette méthode après une nouvelle recherche :
   * liste.chargerPremierePage(getIdSession());
   */
  public L chargerPremierePage(IdSession pIdSession, int pTaillePage) {
    // Mémoriser la taille de la page
    // Si on ne connait pas la taille de la page, on prend la constante TAILLE_MAX_PAGE par défaut
    // Cela va conditionner le nombre de lignes chargé lors du premier chargement de données
    if (pTaillePage > 0) {
      taillePage = pTaillePage;
    }
    else {
      taillePage = TAILLE_MAX_PAGE;
    }
    
    // Déterminer les index visible en fonction de la taille de la page
    if (taillePage > 0) {
      indexVisibleDebut = 0;
      indexVisibleFin = Math.min(size() - 1, taillePage - 1);
    }
    else {
      indexVisibleDebut = -1;
      indexVisibleFin = -1;
    }
    
    // Renseigner les index de chargement à l'identique des index visible pour la première page
    indexChargementDebut = indexVisibleDebut;
    indexChargementFin = indexVisibleFin;
    
    // Charger les objets non chargés de la page
    return chargerPlageObjetManquant(pIdSession);
  }
  
  /**
   * Modifier la page du tableau en cours d'affichage.
   * 
   * Cette méthode s'assure que les objets métiers de la page visible du tableau sont correctement chargés. La page est définie par
   * l'index de la première ligne visible et l'index de la dernière ligne visible. Les objets métiers sont chargés uniquement si cela
   * n'a pas déjà été fait. La méthode retourne la liste des objets dont le contenu vient d'être chargé.
   * 
   * On fait la distinction entre les index corresponds à la plage à afficher des index correspondants à la plage à charger. Lorsque
   * l'utilisateur scrolle vers le haut ou vers le bas, le composant pré-charge les lignes dans la direction du défilement pour éviter
   * des appels successifs à la méthode de chargement de données. Les lignes sont chargées page par page.
   * 
   * Utiliser cette méthode lorsque la zone d'affichage du tableau évolue :
   * if (liste != null) {
   * liste.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
   * }
   */
  public L chargerPage(IdSession pIdSession, int pIndexDebut, int pIndexFin) {
    // Calculer la taille de la page (avant ajustement des valeurs)
    if (pIndexDebut >= 0 && pIndexDebut <= pIndexFin) {
      taillePage = pIndexFin - pIndexDebut + 1;
    }
    else {
      taillePage = 0;
    }
    
    // Calculer l'index de la première ligne à charger
    // L'idée est de charger une page à l'avance si on détecte que l'utilisateur scrolle vers le haut
    int offsetDebut = pIndexDebut - indexVisibleDebut;
    boolean deplacementVersHaut = -taillePage / 2 < offsetDebut && offsetDebut < 0;
    if (deplacementVersHaut) {
      indexChargementDebut = Math.max(0, pIndexDebut - taillePage);
    }
    else {
      indexChargementDebut = Math.max(0, pIndexDebut);
    }
    
    // Calculer l'index de la dernière ligne à charger
    // L'idée est de charger une page à l'avance si on détecte que l'utilisateur scrolle vers le bas
    int offsetFin = pIndexFin - indexVisibleFin;
    boolean deplacementVersBas = 0 < offsetFin && offsetFin < taillePage / 2;
    if (deplacementVersBas) {
      indexChargementFin = Math.min(size() - 1, pIndexFin + taillePage);
    }
    else {
      indexChargementFin = Math.min(size() - 1, pIndexFin);
    }
    
    // Renseigner l'index de la première ligne visible (minimum à 0)
    indexVisibleDebut = Math.max(0, pIndexDebut);
    
    // Renseigner l'index de la dernière ligne visible (plafonné au maximum)
    indexVisibleFin = Math.min(size() - 1, pIndexFin);
    
    // Charger les objets non chargés de la page
    return chargerPlageObjetManquant(pIdSession);
  }
  
  /**
   * Charger les objets métiers non chargés compris entre les deux index de chargement.
   * 
   * Les objets métiers sont chargés uniquement si cela n'a pas déjà été fait. La méthode retourne la liste des objets dont le contenu
   * vient d'être chargé.
   */
  private L chargerPlageObjetManquant(IdSession pIdSession) {
    // Tracer
    Trace.debug(this.getClass(), "chargerPlage", "indexVisible=" + indexVisibleDebut + " à " + indexVisibleFin + ", indexChargement="
        + indexChargementDebut + " à " + indexChargementFin);
    
    // Sortir si rien n'est à afficher
    if (isAffichageVide()) {
      return null;
    }
    
    // Lister les identifiants dont il faut charger les informations
    boolean declencherChargement = false;
    List<I> listeIdACharger = new ArrayList<I>();
    for (int i = indexChargementDebut; i <= indexChargementFin; i++) {
      O objetMetier = get(i);
      if (!objetMetier.isCharge()) {
        listeIdACharger.add((I) objetMetier.getId());
        if (isVisible(i)) {
          declencherChargement = true;
        }
      }
    }
    
    // Sortir si aucune ligne visible n'est à charger
    if (!declencherChargement) {
      return null;
    }
    
    // Charger les informations des documents de ventes
    L listeObjetMetierCharge = charger(pIdSession, listeIdACharger);
    if (listeObjetMetierCharge == null) {
      return null;
    }
    
    // Insérer les données chargées dans la liste
    for (int i = indexChargementDebut; i <= indexChargementFin; i++) {
      O objetMetier = get(i);
      if (!objetMetier.isCharge()) {
        O objetMetierCharge = (O) listeObjetMetierCharge.get(objetMetier.getId());
        if (objetMetierCharge != null) {
          set(i, objetMetierCharge);
        }
        else {
          Trace.erreur("Impossible de remplacer l'objet métier dans la liste : " + objetMetier.getId());
        }
      }
    }
    
    // Retourner la liste des documents chargés
    return listeObjetMetierCharge;
  }
  
  /**
   * true si l'index fournit fait partie de la plage de lignes visibles.
   */
  public boolean isVisible(int pIndex) {
    return indexVisibleDebut <= pIndex && pIndex <= indexVisibleFin;
  }
  
  /**
   * true si aucune données n'est à afficher.
   */
  public boolean isAffichageVide() {
    return indexVisibleDebut < 0 || indexVisibleFin < indexVisibleDebut || taillePage <= 0;
  }
  
  /**
   * Nombre de lignes dans la dernière page affichée.
   */
  public int getTaillePage() {
    return taillePage;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes de gestion des sélections
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Désélectionner tous les objets métiers.
   */
  public void deselectionnerTout() {
    setSelection.clear();
  }
  
  /**
   * Sélectionner l'objet métier fourni en paramètre.
   * Tous les autres objets métiers sont déselectionnés.
   */
  public void selectionner(O pObjetMetier) {
    // Effacer la sélection
    setSelection.clear();
    
    // Sélectionner l'objet métier correspondant à l'index
    if (pObjetMetier != null) {
      setSelection.add(pObjetMetier);
    }
  }
  
  /**
   * Sélectionner l'objet métier fourni en paramètre.
   * Tous les autres objets métiers sont déselectionnés.
   */
  public void selectionner(I pIdObjetMetier) {
    selectionner(get(pIdObjetMetier));
  }
  
  /**
   * Sélectionner l'objet métier situé à l'index fourni en paramètre.
   * Tous les autres objets métiers sont déselectionnés.
   */
  public void selectionner(int pIndex) {
    // Effacer la sélection
    setSelection.clear();
    
    // Sélectionner l'objet métier correspondant à l'index
    if (0 <= pIndex && pIndex < size()) {
      O objetMetier = get(pIndex);
      if (objetMetier != null) {
        setSelection.add(objetMetier);
      }
    }
  }
  
  /**
   * Sélectionner les objets métiers fournis en paramètre.
   * Tous les autres objets métiers sont déselectionnés.
   */
  public void selectionner(List<O> pListeObjetMetier) {
    // Effacer la sélection
    setSelection.clear();
    
    // Vérifier que la liste des index n'est pas nulle
    if (pListeObjetMetier != null) {
      // Sélectionner les objets métiers suivant les index fournis
      for (O objetMetier : pListeObjetMetier) {
        if (objetMetier != null) {
          setSelection.add(objetMetier);
        }
      }
    }
  }
  
  /**
   * Sélectionner les objets métiers dont les index sont fournis en paramètre.
   * Tous les autres objets métiers sont déselectionnés.
   */
  public void selectionner(int[] pListeIndex) {
    // Effacer la sélection
    setSelection.clear();
    
    // Vérifier que la liste des index n'est pas nulle
    if (pListeIndex != null) {
      // Sélectionner les objets métiers suivant les index fournis
      for (int index : pListeIndex) {
        if (0 <= index && index < size()) {
          O objetMetier = get(index);
          if (objetMetier != null) {
            setSelection.add(objetMetier);
          }
        }
      }
    }
  }
  
  /**
   * Sélectionner les objets métiers dont les index sont fournis en paramètre.
   * Tous les autres objets métiers sont déselectionnés.
   */
  public void selectionner(ArrayList<Integer> pListeIndex) {
    // Effacer la sélection
    setSelection.clear();
    
    // Vérifier que la liste des index n'est pas nulle
    if (pListeIndex != null) {
      // Sélectionner les objets métiers suivant les index fournis
      for (int index : pListeIndex) {
        if (0 <= index && index < size()) {
          O objetMetier = get(index);
          if (objetMetier != null) {
            setSelection.add(objetMetier);
          }
        }
      }
    }
  }
  
  /**
   * Sélectionner tous les objets métiers.
   */
  public void selectionnerTout() {
    for (O objetMetier : this) {
      if (objetMetier != null) {
        setSelection.add(objetMetier);
      }
    }
  }
  
  /**
   * Sélectionner ou désélectionner l'objet métier fourni en paramètre.
   * Contrairement aux methodes selectionner(), cette méthode ne désélectionne pas les autres objets métiers de la liste. Seul l'objet
   * métier fourni en paramètre est modifié.
   */
  public void changerSelection(O pObjetMetier, boolean pSelection) {
    if (pObjetMetier != null) {
      if (pSelection) {
        setSelection.add(pObjetMetier);
      }
      else {
        setSelection.remove(pObjetMetier);
      }
    }
  }
  
  /**
   * Sélectionner ou désélectionner l'objet métier dont l'identifiant est fourni en paramètre.
   * Contrairement aux methodes selectionner(), cette méthode ne désélectionne pas les autres objets métiers de la liste. Seul l'objet
   * métier fourni en paramètre est modifié.
   */
  public void changerSelection(I pIdObjetMetier, boolean pSelection) {
    changerSelection(get(pIdObjetMetier), pSelection);
  }
  
  /**
   * Sélectionner ou désélectionner l'objet métier dont l'index est fourni en paramètre.
   * Contrairement aux methodes selectionner(), cette méthode ne désélectionne pas les autres objets métiers de la liste. Seul l'objet
   * métier dont l'index est fourni en paramètre est modifié.
   */
  public void changerSelection(int pIndex, boolean pSelection) {
    if (0 <= pIndex && pIndex < size()) {
      O objetMetier = get(pIndex);
      if (objetMetier != null) {
        if (pSelection) {
          setSelection.add(objetMetier);
        }
        else {
          setSelection.remove(objetMetier);
        }
      }
    }
  }
  
  /**
   * Sélectionner ou désélectionner tous les objets métiers en fonction du paramètre pSelection.
   */
  public void changerSelectionTout(boolean pSelection) {
    if (pSelection) {
      selectionnerTout();
    }
    else {
      deselectionnerTout();
    }
  }
  
  /**
   * Retourne true si aucune objet métier n'est sélectionné.
   */
  public boolean isSelectionVide() {
    return setSelection.isEmpty();
  }
  
  /**
   * Retourne true si tous les objets métiers de la liste sont sélectionnés.
   */
  public boolean isSelectionComplete() {
    return setSelection.size() == size();
  }
  
  /**
   * Retourne true si l'objet métier fourni en paramètre est sélectionné.
   */
  public boolean isSelectionne(O pObjetMetier) {
    if (pObjetMetier == null) {
      return false;
    }
    return setSelection.contains(pObjetMetier);
  }
  
  /**
   * Retourne true si l'objet métier dont l'identifiant est fourni en paramètre est sélectionné.
   */
  public boolean isSelectionne(I pIdObjetMetier) {
    if (pIdObjetMetier != null) {
      for (O objetMetier : setSelection) {
        if (objetMetier != null && objetMetier.getId().equals(pIdObjetMetier)) {
          return true;
        }
      }
    }
    return false;
  }
  
  /**
   * Retourne true si l'objet métier correspondant à l'index fourni en paramètre est sélectionné.
   */
  public boolean isSelectionne(int pIndex) {
    if (0 <= pIndex && pIndex < size()) {
      O objetMetier = get(pIndex);
      if (objetMetier != null) {
        return setSelection.contains(objetMetier);
      }
    }
    return false;
  }
  
  /**
   * Retourner l'objet métier sélectionné (null si aucun).
   * Si plusieurs objets métiers sont sélectionnés, cela retourne le premier objet sélectionné suivant l'ordre de la liste.
   */
  public O getSelection() {
    // Tester si un seul objet métier est sélectionné
    if (setSelection.size() == 1) {
      // Dans ce cas, on retourne l'index de cet objet
      for (O objetMetier : setSelection) {
        return objetMetier;
      }
    }
    else {
      // Dans ce cas, on retourne l'index du premier objet sélectionné suivant l'ordre de la liste
      for (O objetMetier : this) {
        if (objetMetier != null && setSelection.contains(objetMetier)) {
          return objetMetier;
        }
      }
    }
    return null;
  }
  
  /**
   * Retourner l'identifiant de l'objet métier sélectionné (null si aucun).
   * Si plusieurs objets métiers sont sélectionnés, cela retourne le premier objet sélectionné suivant l'ordre de la liste.
   */
  public I getIdSelection() {
    AbstractClasseMetier objetMetier = getSelection();
    if (objetMetier == null) {
      return null;
    }
    return (I) objetMetier.getId();
  }
  
  /**
   * Retourner l'index de l'objet métier sélectionné (-1 si aucun).
   * Si plusieurs objets métiers sont sélectionnés, cela retourne l'index du premier obejt sélectionné suivant l'ordre de la liste.
   */
  public int getIndexSelection() {
    O objetMetier = getSelection();
    if (objetMetier != null) {
      return indexOf(objetMetier);
    }
    return -1;
  }
  
  /**
   * Retourner la liste des objets métiers sélectionnés.
   * La liste retournée est vide si aucun objet n'est sélectionné.
   */
  public List<O> getListeSelection() {
    List<O> listeObjetMetier = new ArrayList<O>();
    listeObjetMetier.addAll(setSelection);
    return listeObjetMetier;
  }
  
  /**
   * Retourner la liste des identifiants des objets métiers sélectionnés.
   * La liste retournée est vide si aucun objet n'est sélectionné.
   */
  public List<I> getListeIdSelection() {
    List<I> listeIdObjetMetier = new ArrayList<I>();
    for (O objetMetier : setSelection) {
      listeIdObjetMetier.add((I) objetMetier.getId());
    }
    return listeIdObjetMetier;
  }
  
  /**
   * Retourner la liste des index des objets métiers sélectionnés.
   */
  public List<Integer> getListeIndexSelection() {
    List<Integer> listeIndex = new ArrayList<Integer>();
    for (int index = 0; index < size(); index++) {
      O objetMetier = get(index);
      if (objetMetier != null && setSelection.contains(objetMetier)) {
        listeIndex.add(index);
      }
    }
    return listeIndex;
  }
  
  /**
   * Permet d'indiquer s'il faut se repositionner en début de liste dans le composant graphique.
   */
  public boolean isAfficherDebutListe() {
    return indexVisibleDebut == 0;
  }
}
