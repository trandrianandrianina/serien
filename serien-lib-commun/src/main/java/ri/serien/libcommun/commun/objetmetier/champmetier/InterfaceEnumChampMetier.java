/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier.champmetier;

import ri.serien.libcommun.commun.objetmetier.IdObjetMetier;

/**
 * Interface pour les énumérations qui associent un numéro unique aux champs métiers.
 * 
 * L'identifiant d'un champ métier doit être unique et ne jamais évoluer, y compris suite à un refactoring : renommage d'une classe ou
 * d'une énumération, réordonnancement des entrées dans une énumération...
 * 
 * Cette interface doit être implémentée pour chaque objet métier afin de définir les identifiants uniques de ses champs métiers.
 * L'interface doit retourner l'identifiant unique de l'objet métier (qui est le même pour tous les champs) et un numéro différent
 * pour chaque champ.
 * 
 * Il doit y avoir une implémentation de cette interface pour chaque objet métier.
 * Voir ri.serien.libcommun.gescom.commun.client.EnumChampClient pour exemple.
 */
public interface InterfaceEnumChampMetier {
  
  /**
   * Retourner l'identifiant de l'objet métier contenant le champ métier.
   * @return Identifiant de l'objet métier.
   */
  public IdObjetMetier getIdObjetMetier();
  
  /**
   * Retourner le numéro unique du champ métier.
   * @return Integer Numéro du champ métier.
   */
  public Integer getNumero();
}
