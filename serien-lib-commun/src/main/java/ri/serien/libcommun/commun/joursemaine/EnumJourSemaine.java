/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.joursemaine;

import java.time.DayOfWeek;
import java.util.Calendar;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Valeurs possibles pour les jours de la semaine.
 */
public enum EnumJourSemaine {
  LUNDI(1, "Lundi"),
  MARDI(2, "Mardi"),
  MERCREDI(3, "Mercredi"),
  JEUDI(4, "Jeudi"),
  VENDREDI(5, "Vendredi"),
  SAMEDI(6, "Samedi"),
  DIMANCHE(7, "Dimanche");
  
  private final Integer numero;
  private final String libelle;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pNumero Le numéro du jour de la semaine.
   * @param pLibelle Le libellé.
   */
  EnumJourSemaine(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourne le libellé associé dans une chaîne de caractère.
   * @return Le libellé.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'objet énum par son numéro.
   * @param pNumero Le numéro du jour de la semaine.
   * @return L'énum.
   */
  public static EnumJourSemaine valueOfByNumero(Integer pNumero) {
    for (EnumJourSemaine value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le jour de la semaine est invalide : " + pNumero);
  }
  
  /**
   * Convertir un EnumJourSemaine en entier représentant le jour dans Calendar.
   * @param pJourSemaine Le jour de la semaine.
   * @return L'entier représentant le jour dans Calendar.
   */
  public static int convertirJourSemaineEnJourCalendar(EnumJourSemaine pJourSemaine) {
    switch (pJourSemaine) {
      case LUNDI:
        return Calendar.MONDAY;
      case MARDI:
        return Calendar.TUESDAY;
      case MERCREDI:
        return Calendar.WEDNESDAY;
      case JEUDI:
        return Calendar.THURSDAY;
      case VENDREDI:
        return Calendar.FRIDAY;
      case SAMEDI:
        return Calendar.SATURDAY;
      case DIMANCHE:
        return Calendar.SUNDAY;
    }
    // Par défaut
    throw new MessageErreurException("Le jour de la semaine est invalide.");
  }
  
  /**
   * Convertir un EnumJourSemaine en entier représentant le jour dans DayOfWeek.
   * @param pJourSemaine Le jour de la semaine.
   * @return L'entier représentant le jour dans DayOfWeek.
   */
  public static DayOfWeek convertirJourSemaineEnJourDayOfWeek(EnumJourSemaine pJourSemaine) {
    switch (pJourSemaine) {
      case LUNDI:
        return DayOfWeek.MONDAY;
      case MARDI:
        return DayOfWeek.TUESDAY;
      case MERCREDI:
        return DayOfWeek.WEDNESDAY;
      case JEUDI:
        return DayOfWeek.THURSDAY;
      case VENDREDI:
        return DayOfWeek.FRIDAY;
      case SAMEDI:
        return DayOfWeek.SATURDAY;
      case DIMANCHE:
        return DayOfWeek.SUNDAY;
    }
    // Par défaut
    throw new MessageErreurException("Le jour de la semaine est invalide.");
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Le numéro sous lequel la valeur est persistée en base de données.
   * @return Le numéro.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au jour de la semaine.
   * @return La libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
}
