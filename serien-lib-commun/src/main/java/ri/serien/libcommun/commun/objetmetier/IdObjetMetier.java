/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un objet métier.
 * 
 * L'identifiant unique de l'objet métier est composé du numéro unique de l'objet métier.
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdObjetMetier extends AbstractId {
  private final Integer numero;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé pour un identifiant complet.
   * @param pEnumEtatObjetMetier Etat de l'objet métier.
   * @param pNumero Numéro de l'objet métier.
   */
  private IdObjetMetier(EnumEtatObjetMetier pEnumEtatObjetMetier, Integer pNumero) {
    super(pEnumEtatObjetMetier);
    numero = controlerNumeroObjetMetier(pNumero);
  }
  
  /**
   * Créer un identifiant à partir du numéro de l'objet métier.
   * @param pNumero Numéro de l'objet métier.
   */
  public static IdObjetMetier getInstance(Integer pNumero) {
    return new IdObjetMetier(EnumEtatObjetMetier.MODIFIE, pNumero);
  }
  
  /**
   * Créer un identifiant à partir du numéro de l'objet métier.
   * @param pEnumObjetMetier Numéro de l'objet métier.
   */
  public static IdObjetMetier getInstance(EnumObjetMetier pEnumObjetMetier) {
    if (pEnumObjetMetier == null) {
      throw new MessageErreurException("L'énumération de l'objet métier est invalide");
    }
    return new IdObjetMetier(EnumEtatObjetMetier.MODIFIE, pEnumObjetMetier.getNumero());
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   * @param pIdObjetMetier Identifiant de l'objet métier.
   * @param pVerifierExistance true=vérifie que l'objet existe, false=sinon.
   */
  public static IdObjetMetier controlerId(IdObjetMetier pIdObjetMetier, boolean pVerifierExistance) {
    if (pIdObjetMetier == null) {
      throw new MessageErreurException("L'identifiant de l'objet métier est invalide.");
    }
    return pIdObjetMetier;
  }
  
  /**
   * Contrôler la validité du numéro de l'objet métier.
   * @param pEnumNumeroObjetMetier Numéro de l'objet métier.
   * @return Numéro de l'objet métier.
   */
  private static Integer controlerNumeroObjetMetier(Integer pNumero) {
    if (pNumero == null) {
      throw new MessageErreurException("Le numéro de l'objet métier est invalide.");
    }
    if (pNumero <= 0) {
      throw new MessageErreurException("Le numéro de l'objet métier doit être supérieur ou égale à zéro.");
    }
    return pNumero;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdObjetMetier)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants d'objets métiers.");
    }
    IdObjetMetier id = (IdObjetMetier) pObject;
    return getNumero().equals(id.getNumero());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdObjetMetier)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdObjetMetier id = (IdObjetMetier) pObject;
    return getNumero().compareTo(id.getNumero());
  }
  
  @Override
  public String getTexte() {
    return numero.toString();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le numéro unique de l'objet métier.
   * @return Numéro de l'objet métier.
   */
  public Integer getNumero() {
    return numero;
  }
}
