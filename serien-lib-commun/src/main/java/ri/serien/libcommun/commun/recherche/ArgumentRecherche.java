/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.recherche;

import java.io.Serializable;

/**
 * Argument de recherche d'une recherche générique.
 * 
 * Une recherche dite générique effectue une recherche "intelligente" afin de trouver les résultats les plus pertinents. Pour cela,
 * chaque mot de recherche est séparé, normalisé et qualifié suivant sa nature (texte, nombre, ...). Certains mots sont ignorés car trop
 * petit pour être pris en compte dans la recherche. Un argument de recherche est toujours en majuscules, sans accents et sans caractères
 * spéciaux.
 * 
 * Cette classe est immutable pour ne pas être modifiable.
 */
public class ArgumentRecherche implements Serializable {
  // Variables
  private final int index;
  private final EnumTypeArgumentRecherche typeArgumentRecherche;
  private final String valeur;
  
  /**
   * Constructeur.
   */
  public ArgumentRecherche(int pIndex, EnumTypeArgumentRecherche pTypeArgumentRecherche, String pValeur) {
    index = pIndex;
    typeArgumentRecherche = pTypeArgumentRecherche;
    valeur = pValeur;
  }
  
  /**
   * Indice de l'argument de recherche, c'est à dire la position de l'argument de recherche dans le texte utilisé pour la recherche.
   */
  public int getIndex() {
    return index;
  }
  
  /**
   * Type de l'argument de recherche.
   */
  public EnumTypeArgumentRecherche getType() {
    return typeArgumentRecherche;
  }
  
  /**
   * Valeur de l'argument de recherche.
   */
  public String getValeur() {
    return valeur;
  }
}
