/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.id;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Classe de base de tous les identifiants comportant le magasin.
 *
 * La classe est abstraite pour éviter son instanciation par erreur. Il n'y a pas de raison d'instancier directement cette classe
 * puisqu'elle n'existe que pour mutualiser la gestion du code établissement dans tous les identifiants.
 * Pour gérer l'identifiant d'un magasin, c'est la classe IdMagasin qu'il faut utiliser.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public abstract class AbstractIdAvecMagasin extends AbstractId {
  // Constantes
  public static final int LONGUEUR_CODE_ETABLISSEMENT = 3;
  
  // Variables
  private final IdMagasin idMagasin;
  
  /**
   * Constructeur.
   */
  protected AbstractIdAvecMagasin(EnumEtatObjetMetier pEtatObjetMetier, IdMagasin pIdMagasin) {
    super(pEtatObjetMetier);
    if (pIdMagasin == null) {
      throw new MessageErreurException("Le magasin n'est pas renseigné.");
    }
    idMagasin = pIdMagasin;
  }
  
  // -- Accesseurs
  
  /**
   * Code établissement.
   * @return Code de l'établissement du magasin, sur 3 caractères alphanumériques, complété de caractères espaces à droite si nécessaire.
   */
  public String getCodeEtablissement() {
    return idMagasin.getCodeEtablissement();
  }
  
  /**
   * Identifiant de l'établissement.
   * @return Identifiant de l'établissement du magasin.
   */
  public IdEtablissement getIdEtablissement() {
    return idMagasin.getIdEtablissement();
  }
  
  /**
   * Code magasin.
   * @return Code du magasin sur 2 caractères alphanumériques.
   */
  public String getCodeMagasin() {
    return idMagasin.getCode();
  }
  
  /**
   * Identifiant du magasin.
   * @return Identifiant de l'établissement du magasin.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
}
