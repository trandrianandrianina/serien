/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Création des instances en relations avec les objets métiers.
 * 
 * Cette classes est un singleton. Elle ne dispose donc que d'une seule instance accessible via la méthode getInstance().
 * Elle a pour objectif de fournir les instances des classes en relation avec les objets métiers (ObjetMetier, ListeObjetMetier).
 * 
 * Cette classe construit la liste des objets métiers une fois pour toute lors de son premier appel. Les objets retournés ensuite
 * proviennent de cette liste.
 */
public class ManagerObjetMetier {
  private static final ManagerObjetMetier INSTANCE = new ManagerObjetMetier();
  private final ListeObjetMetier listeObjetMetier;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé pour garantir le singleton.
   */
  private ManagerObjetMetier() {
    // Initialiser la liste permanente des objets métiers
    listeObjetMetier = new ListeObjetMetier();
    for (EnumPersistanceObjetMetier persistanceObjetMetier : EnumPersistanceObjetMetier.values()) {
      ObjetMetier objetMetier = new ObjetMetier(persistanceObjetMetier);
      listeObjetMetier.add(objetMetier);
    }
  }
  
  /**
   * Retourner l'instance unique de cette classe.
   * @return Instance de la classse.
   */
  public static ManagerObjetMetier getInstance() {
    return INSTANCE;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner un objet métier à partir de son identifiant.
   * @param pIdObjetMetier Identifiant de l'objet métier.
   * @return Objet métier.
   */
  public ObjetMetier getObjetMetier(IdObjetMetier pIdObjetMetier) {
    // Tester les paramètres
    if (pIdObjetMetier == null) {
      throw new MessageErreurException("L'identifiant de l'objet métier est invalide.");
    }
    
    // Récupérer l'objet métier
    ObjetMetier objetMetier = listeObjetMetier.get(pIdObjetMetier);
    if (objetMetier == null) {
      throw new MessageErreurException("L'identifiant de l'objet métier ne correspond pas à un objet métier connu.");
    }
    
    return objetMetier;
  }
  
  /**
   * Retourner un objet métier à partir de son énumération.
   * @param pEnumObjetMetier Enunération de l'objet métier.
   * @return Objet métier.
   */
  public ObjetMetier getObjetMetier(EnumObjetMetier pEnumObjetMetier) {
    // Tester les paramètres
    if (pEnumObjetMetier == null) {
      throw new MessageErreurException("L'énumération de l'objet métier est invalide.");
    }
    
    // Récupérer l'objet métier
    return getObjetMetier(IdObjetMetier.getInstance(pEnumObjetMetier));
  }
  
  /**
   * Retourner la liste complète des objets métiers.
   * @return Liste d'objets métiers.
   */
  public ListeObjetMetier getListeObjetMetier() {
    return listeObjetMetier;
  }
}
