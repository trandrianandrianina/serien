/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.memo;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Critères de recherche des mémos.
 */
public class FiltreMemo implements Serializable {
  private IdEtablissement idEtablissement;
  private EnumCodeEnteteMemo codeEntete;
  private String codeTiers;
  private Integer numero;
  private EnumTypeMemo type;
  
  /**
   * Identifiant établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Modifier l'identifiant de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Code entête du mémo.
   */
  public EnumCodeEnteteMemo getCodeEntete() {
    return codeEntete;
  }
  
  /**
   * Modifier le code entête du mémo.
   */
  public void setCodeEntete(EnumCodeEnteteMemo pCodeEntete) {
    codeEntete = pCodeEntete;
  }
  
  /**
   * Code tiers du mémo.
   */
  public String getCodeTiers() {
    return codeTiers;
  }
  
  /**
   * Modifier le code tiers du mémo.
   */
  public void setCodeTiers(String pCodeTiers) {
    codeTiers = pCodeTiers;
  }
  
  /**
   * Numéro du mémo.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Modifier le numéro du mémo.
   */
  public void setNumero(Integer numero) {
    this.numero = numero;
  }
  
  /**
   * Type du mémo.
   */
  public EnumTypeMemo getType() {
    return type;
  }
  
  /**
   * Modifier le type du mémo.
   */
  public void setType(EnumTypeMemo type) {
    this.type = type;
  }
}
