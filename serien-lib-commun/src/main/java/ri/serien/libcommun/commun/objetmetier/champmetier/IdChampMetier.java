/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier.champmetier;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.commun.objetmetier.IdObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Identifiant unique d'un champ métier.
 * 
 * L'identifiant unique du champ métier est composé de l'identifiant de l'objet métier et du numéro unique du champ métier.
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdChampMetier extends AbstractId {
  private final IdObjetMetier idObjetMetier;
  private final Integer numero;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé.
   * @param pIdObjetMetier Identifiant de l'objet métier.
   * @param pNumero Numéro du champ métier.
   */
  private IdChampMetier(EnumEtatObjetMetier pEnumEtatObjetMetier, IdObjetMetier pIdObjetMetier, Integer pNumero) {
    super(pEnumEtatObjetMetier);
    idObjetMetier = controlerIdObjetMetier(pIdObjetMetier);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Créer un identifiant à partir des éléments sous forme éclatée.
   * @param pIdObjetMetier Identifiant de l'objet métier.
   * @param pNumero Numéro du champ métier.
   */
  public static IdChampMetier getInstance(IdObjetMetier pIdObjetMetier, Integer pNumero) {
    return new IdChampMetier(EnumEtatObjetMetier.MODIFIE, pIdObjetMetier, pNumero);
  }
  
  /**
   * Créer un identifiant à partir de l'énumération du champ métier.
   * @param pEnumChampMetier Enumération du champ métier.
   */
  public static IdChampMetier getInstance(InterfaceEnumChampMetier pEnumChampMetier) {
    if (pEnumChampMetier == null) {
      throw new MessageErreurException("L'énumération du champ métier est invalide");
    }
    return new IdChampMetier(EnumEtatObjetMetier.MODIFIE, pEnumChampMetier.getIdObjetMetier(), pEnumChampMetier.getNumero());
  }
  
  /**
   * Créer un identifiant à partir de la persistance d'un champ métier.
   * @param pNumeroObjetMetier Persistance d'un champ métier.
   */
  public static IdChampMetier getInstance(InterfacePersistanceChampMetier pInterfaceChampObjetMetier) {
    return new IdChampMetier(EnumEtatObjetMetier.MODIFIE, pInterfaceChampObjetMetier.getEnumChampMetier().getIdObjetMetier(),
        pInterfaceChampObjetMetier.getEnumChampMetier().getNumero());
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   * @param pIdChampMetier L'identifiant du champ métier.
   * @param pVerifierExistance Indique s'il faut vérifier son existance.
   */
  public static IdChampMetier controlerId(IdChampMetier pIdChampMetier, boolean pVerifierExistance) {
    if (pIdChampMetier == null) {
      throw new MessageErreurException("L'identifiant de champ de base de données est invalide.");
    }
    return pIdChampMetier;
  }
  
  /**
   * Contrôler la validité du libellé de l'objet métier.
   * @param pIdObjetMetier Identifiant du champ métier.
   * @return Identifiant du champ métier.
   */
  private static IdObjetMetier controlerIdObjetMetier(IdObjetMetier pIdObjetMetier) {
    if (pIdObjetMetier == null) {
      throw new MessageErreurException("L'identifiant  de l'objet métier est invalide.");
    }
    return pIdObjetMetier;
  }
  
  /**
   * Contrôler la validité du numéro du champ métier.
   * @param pNumero Numéro du champ métier.
   * @return Numéro du champ métier.
   */
  private static Integer controlerNumero(Integer pNumero) {
    if (pNumero == null) {
      throw new MessageErreurException("Le numéro du champ métier est invalide.");
    }
    if (pNumero <= 0) {
      throw new MessageErreurException("Le numéro du champ métier doit être supérieur ou égale à zéro.");
    }
    return pNumero;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + idObjetMetier.hashCode();
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    if (!(pObject instanceof IdChampMetier)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de champs de base de données.");
    }
    IdChampMetier id = (IdChampMetier) pObject;
    return getIdObjetMetier().equals(id.getIdObjetMetier()) && getNumero().equals(id.getNumero());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdChampMetier)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdChampMetier id = (IdChampMetier) pObject;
    int comparaison = getIdObjetMetier().compareTo(id.getIdObjetMetier());
    if (comparaison != 0) {
      return comparaison;
    }
    return getNumero().compareTo(id.getNumero());
  }
  
  @Override
  public String getTexte() {
    return idObjetMetier.toString() + " - " + numero;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'identifiant de l'objet métier.
   * @return Identifiant objet métier.
   */
  public IdObjetMetier getIdObjetMetier() {
    return idObjetMetier;
  }
  
  /**
   * Retourner le numéro unique du champ métier.
   * @return Numéro du champ métier.
   */
  public Integer getNumero() {
    return numero;
  }
}
