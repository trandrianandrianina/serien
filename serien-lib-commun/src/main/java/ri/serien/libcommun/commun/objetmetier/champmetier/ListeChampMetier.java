/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier.champmetier;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Liste de champs métiers.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de ChampObjetMetier.
 */
public class ListeChampMetier extends ListeClasseMetier<IdChampMetier, ChampMetier, ListeChampMetier> {
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public ListeChampMetier() {
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Inutilisé.
   */
  @Override
  public ListeChampMetier charger(IdSession pIdSession, List<IdChampMetier> pListeId) {
    return null;
  }
  
  /**
   * Inutilisé.
   */
  @Override
  public ListeChampMetier charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return null;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes de manipulations de la liste
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner un champ métier de la liste via son énumération.
   * @param pEnumChampMetier Enumération du champ métier
   * @return Champ métier (null si pas trouvé).
   */
  public ChampMetier get(InterfaceEnumChampMetier pEnumChampMetier) {
    if (pEnumChampMetier == null) {
      return null;
    }
    return get(IdChampMetier.getInstance(pEnumChampMetier));
  }
  
  /**
   * Ajouter un champ métier à la liste à partir de son identifiant.
   * @param pIdChampMetier Identifiant du champ métier.
   * @return true=si la liste a changé, false=si la liste n'a pas changé.
   */
  public boolean add(IdChampMetier pIdChampMetier) {
    // Tester les paramètres
    if (pIdChampMetier == null) {
      return false;
    }
    
    // Ajouter le champ métier à la liste
    ChampMetier champMetier = ManagerChampMetier.getInstance().getChampMetier(pIdChampMetier);
    return add(champMetier);
  }
  
  /**
   * Ajouter un champ métier à la liste à partir de son énumération.
   * @param pEnumChampMetier Enumération du champ métier.
   * @return true=si la liste a changé, false=si la liste n'a pas changé.
   */
  public boolean add(InterfaceEnumChampMetier pEnumChampMetier) {
    // Tester les paramètres
    if (pEnumChampMetier == null) {
      return false;
    }
    
    // Ajouter le champ métier à la liste
    ChampMetier champMetier = ManagerChampMetier.getInstance().getChampMetier(pEnumChampMetier);
    return add(champMetier);
  }
  
  /**
   * Ajouter les champs métiers correspondant à la liste d'identifiants de champs métiers fournie en paramètre.
   * @param pListeIdChampMetier Liste d'identifiants de champs métiers.
   */
  public void addAll(ListeIdChampMetier pListeIdChampMetier) {
    // Tester les paramètres
    if (pListeIdChampMetier == null) {
      return;
    }
    
    // Ajouter la liste de champs métiers
    for (IdChampMetier idChampMetier : pListeIdChampMetier) {
      add(idChampMetier);
    }
  }
  
  /**
   * Ajouter les champs métiers correspondant à la liste d'énumérations de champs métiers fournie en paramètre.
   * @param pListeInterfaceEnumChampMetier Liste d'énumérations de champs métiers.
   */
  public void addAll(InterfaceEnumChampMetier[] pListeEnumChampMetier) {
    // Tester les paramètres
    if (pListeEnumChampMetier == null) {
      return;
    }
    
    // Ajouter la liste de champs métiers
    for (InterfaceEnumChampMetier enumChampMetier : pListeEnumChampMetier) {
      add(enumChampMetier);
    }
  }
}
