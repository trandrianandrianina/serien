/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.recherche;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Type d'argument de recherche.
 * 
 * Qualifie les différents éléments de recherche que l'on trouve dans une recherche générique. Les différents mots d'une recherche
 * générique sont séparés, normalisés et qualifiés par nature : nombre, texte, ... Un type spécial regroupe les textes ignorés.
 */
public enum EnumTypeArgumentRecherche {
  IGNORE(0, "Ignoré"),
  TEXTE(1, "Texte"),
  ENTIER(2, "Nombre entier"),
  DECIMAL(3, "Nombre décimal"),
  CODE_POSTAL(4, "Code postal"),
  TELEPHONE(5, "Téléphone"),
  IDENTIFIANT(6, "Identifiant"),
  EMAIL(7, "E-Mail");
  
  private final Integer code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeArgumentRecherche(Integer pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Integer getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeArgumentRecherche valueOfByCode(Integer pCode) {
    for (EnumTypeArgumentRecherche value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code de l'argument de recherche est invalide : " + pCode);
  }
}
