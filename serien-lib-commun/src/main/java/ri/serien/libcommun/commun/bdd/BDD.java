/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.bdd;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.version.Version;
import ri.serien.libcommun.outils.Constantes;

/**
 * Cette classe décrit une base de données Série N.
 */
public class BDD extends Bibliotheque {
  // Variables
  private Version version = null;
  private Character lettreEnvironnement = null;
  
  /**
   * Constructeur.
   */
  public BDD(IdBibliotheque pIdBibliotheque, Character pLettreEnvironnement) {
    super(pIdBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES);
    if (pLettreEnvironnement != null && pLettreEnvironnement.charValue() == ' ') {
      lettreEnvironnement = null;
    }
    else {
      lettreEnvironnement = pLettreEnvironnement;
    }
  }
  
  /**
   * Constructeur.
   */
  public BDD(IdBibliotheque pIdBibliotheque, Character pLettreEnvironnement, String pDescription) {
    this(pIdBibliotheque, pLettreEnvironnement);
    setDescription(Constantes.normerTexte(pDescription));
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    if (version != null) {
      return getNom() + " " + version.toString();
    }
    return getNom();
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la version de la base de données.
   */
  public Version getVersion() {
    return version;
  }
  
  /**
   * Initialise la version de la base de données.
   */
  public void setVersion(Version pVersion) {
    version = pVersion;
  }
  
  /**
   * Retourne la lettre d'environnement dont dépend la base de données.
   */
  public Character getLettreEnvironnement() {
    return lettreEnvironnement;
  }
  
}
