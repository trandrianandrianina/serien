/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.memo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Mémo.
 * 
 * Le mémo est présenté dans la fiche article comme un groupe de 18 lignes de 60 caractères maximum de longueur.
 * Cette représentation sert de base à la persistance dans cet objet. Les textes renseignés via la méthode setTexte() sont convertis
 * à ce format. A contrario, la méthode getTexte() concatène les 18 lignes pour produire un bloc de texte unique.
 */
public class Memo extends AbstractClasseMetier<IdMemo> {
  public static final int LONGUEUR_MAX_LIGNE = 60;
  public static final int NOMBRE_MAX_LIGNE = 18;
  public static final int LONGUEUR_LIGNE_STOCKEE = 120;
  public static final int NOMBRE_LIGNES_STOCKEES = 9;
  public static final int LONGUEUR_TEXTE_MAX = LONGUEUR_LIGNE_STOCKEE * NOMBRE_LIGNES_STOCKEES;
  
  // Variables
  private EnumTypeMemo type = EnumTypeMemo.AUCUN;
  private Date dateCreation = null;
  private Date dateModification = null;
  private String profil = "";
  private String codeIntervenant = "";
  private List<String> listeLigne = new ArrayList<String>();
  
  /**
   * Constructeur avec l'identificant mémo.
   */
  public Memo(IdMemo pIdMemo) {
    super(pIdMemo);
  }
  
  /**
   * Texte du mémo.
   */
  @Override
  public String getTexte() {
    String texte = "";
    for (String ligneTexte : listeLigne) {
      texte += ligneTexte + '\n';
    }
    return texte;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdMemo controlerId(Memo pMemo, boolean pVerifierExistence) {
    if (pMemo == null) {
      throw new MessageErreurException("Le mémo est invalide.");
    }
    return IdMemo.controlerId(pMemo.getId(), pVerifierExistence);
  }
  
  /**
   * Charger le premier mémo d'un certain type associé à un article.
   */
  public static Memo charger(IdSession pIdSession, IdArticle pIdArticle, EnumTypeMemo pTypeMemo) {
    // Contrôler les paramètres
    IdArticle.controlerId(pIdArticle, true);
    if (pTypeMemo == null) {
      throw new MessageErreurException("Le type de mémo est invalide.");
    }
    
    // Renseigner les critères de recherche
    FiltreMemo filtreMemo = new FiltreMemo();
    filtreMemo.setIdEtablissement(pIdArticle.getIdEtablissement());
    filtreMemo.setCodeEntete(EnumCodeEnteteMemo.ARTICLE);
    filtreMemo.setCodeTiers(pIdArticle.getCodeArticle());
    filtreMemo.setType(pTypeMemo);
    
    // Charger la liste des mémos suivant les critères de recherche
    ListeMemo listeMemo = ListeMemo.charger(pIdSession, filtreMemo);
    if (listeMemo == null || listeMemo.isEmpty()) {
      return null;
    }
    
    // Seul le premier mémo nous intéresse
    return listeMemo.get(0);
  }
  
  /**
   * Charge le premier mémo d'un certain type associé à un document de ventes.
   */
  public static Memo charger(IdSession pIdSession, IdDocumentVente pIdDocumentVente, EnumTypeMemo pTypeMemo) {
    // Contrôler les paramètres
    IdDocumentVente.controlerId(pIdDocumentVente, true);
    if (pTypeMemo == null) {
      throw new MessageErreurException("Le type de mémo est invalide.");
    }
    
    // Déterminer l'entête du mémo en fonction du type du document de vente
    EnumCodeEnteteMemo enumCodeEnteteMemo = null;
    if (pIdDocumentVente.isIdDevis()) {
      enumCodeEnteteMemo = EnumCodeEnteteMemo.DEVIS;
    }
    else {
      enumCodeEnteteMemo = EnumCodeEnteteMemo.DOCUMENT;
    }
    
    // Renseigner les critères de recherche
    FiltreMemo filtreMemo = new FiltreMemo();
    filtreMemo.setIdEtablissement(pIdDocumentVente.getIdEtablissement());
    filtreMemo.setCodeEntete(enumCodeEnteteMemo);
    filtreMemo.setCodeTiers(pIdDocumentVente.getIndicatif(IdDocumentVente.INDICATIF_NUM));
    filtreMemo.setType(pTypeMemo);
    
    // Charger la liste des mémos suivant les critères de recherche
    ListeMemo listeMemo = ListeMemo.charger(pIdSession, filtreMemo);
    if (listeMemo == null || listeMemo.isEmpty()) {
      return null;
    }
    
    // Seul le premier mémo nous intéresse
    return listeMemo.get(0);
  }
  
  /**
   * Charge le premier mémo d'un certain type associé à un chantier.
   */
  public static Memo charger(IdSession pIdSession, IdChantier pIdChantier, EnumTypeMemo pTypeMemo) {
    // Contrôler les paramètres
    IdChantier.controlerId(pIdChantier, true);
    if (pTypeMemo == null) {
      throw new MessageErreurException("Le type de mémo est invalide.");
    }
    
    // Convertir l'identifiant chantier en identifiant document de vente (un chantier est géré comme un devis dans la base de données)
    IdDocumentVente idDocumentVente = IdDocumentVente.getInstanceHorsFacture(pIdChantier.getIdEtablissement(),
        pIdChantier.getCodeEntete(), pIdChantier.getNumero(), pIdChantier.getSuffixe());
    
    // Charger le mémo du document de vente
    return Memo.charger(pIdSession, idDocumentVente, EnumTypeMemo.LIVRAISON);
  }
  
  // -- Méthodes publiques
  
  /**
   * Sauver le mémo.
   */
  public void sauver(IdSession pIdSession) {
    ManagerServiceParametre.sauverMemo(pIdSession, this);
  }
  
  /**
   * Contrôler si le texte du mémo est vide.
   */
  public boolean isVide() {
    return listeLigne.isEmpty();
  }
  
  /**
   * Préparer le texte du mémo pour son enregistrement dans la base.
   */
  public List<String> formaterPourSauvegarde() {
    // Supprimer les lignes qui dépassent le nombre max de lignes autorisées
    while (listeLigne.size() > NOMBRE_MAX_LIGNE) {
      listeLigne.remove(listeLigne.size() - 1);
    }
    
    // On contrôle que le nombre de lignes est pair, c'est plus facile pour la concaténation
    if ((listeLigne.size() % 2) != 0) {
      listeLigne.add("");
    }
    
    // Concaténer les lignes 2 à 2 (à cause du stockage en base)
    List<String> ligneConcatenee = new ArrayList<String>();
    for (int i = 0; i < listeLigne.size(); i = i + 2) {
      ligneConcatenee.add(String.format("%-" + Memo.LONGUEUR_MAX_LIGNE + "s%-" + Memo.LONGUEUR_MAX_LIGNE + "s", listeLigne.get(i),
          listeLigne.get(i + 1)));
    }
    
    return ligneConcatenee;
  }
  
  // -- Méthodes privées
  
  /**
   * Permet de convertir le texte du mémo en liste de lignes.
   * c'est à dire x lignes qui ne dépassent pas la longueur "normale" (sans concaténation).
   */
  private ArrayList<String> convertirTexteEnListeLigne(String pTexte) {
    if (pTexte == null) {
      pTexte = "";
    }
    
    // Découper le texte en lignes
    ArrayList<String> listeLigneSource = Constantes.splitString2List(pTexte, '\n');
    
    // Supprimer les dernières lignes si elles sont vides
    for (int i = listeLigneSource.size() - 1; i >= 0; i--) {
      String ligneTexte = listeLigneSource.get(i);
      if (ligneTexte.trim().isEmpty()) {
        listeLigneSource.remove(i);
      }
      else {
        break;
      }
    }
    
    // Formater chaque ligne à la longueur max d'une ligne mémo
    ArrayList<String> listeLigneDest = new ArrayList<String>();
    for (String ligneSource : listeLigneSource) {
      // Tester si la longueur de la chaine est inférieure ou égale à la taille maximum
      if (ligneSource.length() <= Memo.LONGUEUR_MAX_LIGNE) {
        listeLigneDest.add(ligneSource.trim());
      }
      // Dans le cas où la chaine est trop longue
      else {
        int position = 0;
        while (position < ligneSource.length()) {
          // Calculer la taille de la ligne à insérer
          int taille = ligneSource.length() - position;
          if (taille > Memo.LONGUEUR_MAX_LIGNE) {
            taille = Memo.LONGUEUR_MAX_LIGNE;
          }
          
          // Copier le tronàon de ligne
          listeLigneDest.add(ligneSource.substring(position, position + taille).trim());
          position = position + taille;
        }
      }
    }
    
    // Supprimer les lignes qui dépassent le nombre max de lignes autorisées
    while (listeLigneDest.size() > NOMBRE_MAX_LIGNE) {
      listeLigneDest.remove(listeLigneDest.size() - 1);
    }
    return listeLigneDest;
  }
  
  // -- Accesseurs
  
  /**
   * Type de mémo.
   */
  public EnumTypeMemo getType() {
    return type;
  }
  
  /**
   * Modifier le type de mémo.
   */
  public void setType(EnumTypeMemo pType) {
    type = pType;
  }
  
  /**
   * Date de création du mémo.
   */
  public Date getDateCreation() {
    return dateCreation;
  }
  
  /**
   * Modifier la date de création.
   */
  public void setDateCreation(Date pDateCreation) {
    dateCreation = pDateCreation;
  }
  
  /**
   * Date de modification.
   */
  public Date getDateModification() {
    return dateModification;
  }
  
  /**
   * Modifier la date de modification.
   */
  public void setDateModification(Date dateModification) {
    this.dateModification = dateModification;
  }
  
  /**
   * Profil ayant créé le mémo.
   */
  public String getProfil() {
    return profil;
  }
  
  /**
   * Modifier le profil ayant créé le mémo.
   */
  public void setProfil(String profil) {
    this.profil = profil;
  }
  
  /**
   * Code du vendeur ou de l'achteur associé au mémo.
   * Cette notion n'est pas réellement utilisée.
   */
  public String getCodeIntervenant() {
    return codeIntervenant;
  }
  
  /**
   * Modifier le code du vendeur ou de l'achteur associé au mémo.
   */
  public void setCodeIntervenant(String pCodeIntervenant) {
    codeIntervenant = pCodeIntervenant;
  }
  
  /**
   * Modifier le texte du mémo.
   */
  public void setTexte(String pTexte) {
    listeLigne = convertirTexteEnListeLigne(pTexte);
  }
}
