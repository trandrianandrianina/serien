/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.recherche;

import java.io.Serializable;
import java.text.Normalizer;

/**
 * Recherche générique.
 * 
 * Une recherche dite générique effectue une recherche "intelligente" afin de trouver les résultats les plus pertinents. Pour cela,
 * chaque mot de recherche est séparé, normalisé et qualifié suivant sa nature (texte, nombre, ...). Certains mots sont ignorés car trop
 * petit pour être pris en compte dans la recherche. Un argument de recherche est toujours en majuscules, sans accents et sans caractères
 * spéciaux.
 */
public class RechercheGenerique implements Serializable {
  public static final int LONGUEUR_MINI_TEXTE_RECHERCHE = 2;
  
  private final ListeArgumentRecherche listeArgumentRecherche = new ListeArgumentRecherche();
  private String texteInitial = "";
  private String texteFinal = "";
  private String message = "";
  private Boolean enErreur = false;
  
  /**
   * Constructeur.
   */
  public RechercheGenerique() {
  }
  
  /**
   * Constructeur.
   */
  public RechercheGenerique(String pTexte) {
    texteInitial = pTexte;
    decomposerTexteRecherche();
  }
  
  /**
   * Initialiser la recherche générique.
   */
  public void initialiser() {
    listeArgumentRecherche.clear();
    texteInitial = "";
    texteFinal = "";
    message = "";
    enErreur = false;
  }
  
  /**
   * Décomposer le texte
   */
  private void decomposerTexteRecherche() {
    // Initialiser les données
    listeArgumentRecherche.clear();
    texteFinal = "";
    message = "";
    enErreur = false;
    
    // Sortir si le texte recherche est vide
    if (texteInitial == null || texteInitial.isEmpty()) {
      return;
    }
    
    // Mettre en majuscules
    String texte = texteInitial.toUpperCase();
    
    // Normaliser le texte pour enlever les accents
    texte = Normalizer.normalize(texte, Normalizer.Form.NFD);
    texte = texte.replaceAll("[^\\p{ASCII}]", "");
    texte = texte.replaceAll("[^\\x00-\\x7F]", "");
    
    // Remplacer les caractères spéciaux par des espaces
    // Les caractères contenu dans le replaceAll ne sont pas remplacés
    texte = texte.replaceAll("[^A-Z0-9.,-/_() ]@", " ");
    texte = texte.replaceAll("\\'", " ");
    
    // Ne conserver que un espace entre chaque mot
    texte = texte.trim().replaceAll(" +", " ");
    
    // Indice du mot dans la recherche
    int index = 1;
    
    // Répartir les mots dans les trois listes
    String[] mots = texte.split(" ");
    for (String mot : mots) {
      // Tester si le mot est trop court
      if (mot.length() < LONGUEUR_MINI_TEXTE_RECHERCHE) {
        listeArgumentRecherche.add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.IGNORE, mot));
      }
      // Tester si c'est un identifiant
      else if (mot.startsWith("(")) {
        if (mot.endsWith(")")) {
          listeArgumentRecherche
              .add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.IDENTIFIANT, mot.substring(1, mot.length() - 1)));
        }
        else {
          listeArgumentRecherche.add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.IDENTIFIANT, mot.substring(1)));
        }
      }
      // Tester si c'est un identifiant avec suffixe de livraison
      else if ((mot.matches("\\d*-\\d*"))) {
        listeArgumentRecherche.add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.IDENTIFIANT, mot));
      }
      // Tester si c'est un code postal
      else if (mot.matches("\\d{5}")) {
        listeArgumentRecherche.add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.CODE_POSTAL, mot));
      }
      // Tester si c'est un numéro de téléphone
      else if (mot.matches("\\d{2}[.-]?\\d{2}[.-]?\\d{2}[.-]?\\d{2}[.-]?\\d{2}")) {
        String motSimplifie = mot.replaceAll("[^0123456789]", "");
        if (motSimplifie.length() == 10) {
          listeArgumentRecherche.add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.TELEPHONE, mot));
        }
      }
      // Tester si c'est un nombre entier
      else if (mot.matches("-?\\d+")) {
        mot = mot.replace('.', ',');
        listeArgumentRecherche.add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.ENTIER, mot));
      }
      // Tester si c'est un nombre décimal (remplacer les points par des virgules (format de stockage des nombres
      // indexés)
      else if (mot.matches("-?\\d+\\,\\d+") || mot.matches("-?\\d+\\.\\d+")) {
        mot = mot.replace('.', ',');
        listeArgumentRecherche.add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.DECIMAL, mot));
      }
      // Tester si c'est un mail
      else if (mot.contains("@")) {
        listeArgumentRecherche.add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.EMAIL, mot.toLowerCase()));
      }
      // C'est une chaîne
      else {
        listeArgumentRecherche.add(new ArgumentRecherche(index++, EnumTypeArgumentRecherche.TEXTE, mot));
      }
    }
    
    // Contruire le texte de recherche final
    texteFinal = "";
    for (ArgumentRecherche argumentRecherche : listeArgumentRecherche) {
      switch (argumentRecherche.getType()) {
        case IGNORE:
          break;
        
        case IDENTIFIANT:
          texteFinal += "(" + argumentRecherche.getValeur() + ") ";
          break;
        
        default:
          texteFinal += argumentRecherche.getValeur() + " ";
          break;
      }
    }
    texteFinal = texteFinal.trim();
    
    // Tester
    if (texteFinal.length() < LONGUEUR_MINI_TEXTE_RECHERCHE) {
      message = "Le texte à rechercher doit faire au minimum " + LONGUEUR_MINI_TEXTE_RECHERCHE + " caractères.";
      enErreur = true;
      return;
    }
    
    // Construire le message pour indiquer les mots qui ont été ignorés dans la recherche
    message = "";
    String texteIgnore = "";
    int nombre = 0;
    for (ArgumentRecherche argumentRecherche : listeArgumentRecherche) {
      switch (argumentRecherche.getType()) {
        case IGNORE:
          texteIgnore += argumentRecherche.getValeur() + " ";
          nombre++;
          break;
        
        default:
          break;
      }
    }
    
    if (nombre == 1) {
      message = "Le mot suivant est ignoré dans la recherche : " + texteIgnore;
    }
    else if (nombre > 1) {
      message = "Les mots suivants sont ignorés : " + texteIgnore;
    }
  }
  
  /**
   * Liste des arguments de la recherche géénrique.
   */
  public ListeArgumentRecherche getListeArgumentRecherche() {
    return listeArgumentRecherche;
  }
  
  /**
   * Texte recherché avant transformation.
   */
  public String getTexteInitial() {
    return texteInitial;
  }
  
  /**
   * Texte ayant effectivement servit de base à la recherche (après normalisation).
   */
  public String getTexteFinal() {
    return texteFinal;
  }
  
  /**
   * Texte transformé pour la recherche d'une référence de document.
   * On enlève seulement les accents.
   * 
   */
  public String getTexteRechercheReference() {
    // Mettre en majuscules
    String texte = texteInitial.toUpperCase();
    
    // Normaliser le texte pour enlever les accents
    texte = Normalizer.normalize(texte, Normalizer.Form.NFD);
    texte = texte.replaceAll("[^\\p{ASCII}]", "");
    texte = texte.replaceAll("[^\\x00-\\x7F]", "");
    
    return texte;
  }
  
  /**
   * Modifier le texte recherché.
   */
  public void setTexte(String pTexte) {
    texteInitial = pTexte;
    decomposerTexteRecherche();
  }
  
  /**
   * Message informatif sur la recherche générique.
   * Utiliser par exemple pour indiquer les mots qui ont été ignorés dans la recherche ou pour indiquer la cause d'une
   * erreur.
   */
  public String getMessage() {
    return message;
  }
  
  /**
   * Indique si l'analyse du texte de recherche n'a pas pu aboutir à cause d'une erreur.
   * L'explication sera dans le message.
   */
  public Boolean isEnErreur() {
    return enErreur;
  }
}
