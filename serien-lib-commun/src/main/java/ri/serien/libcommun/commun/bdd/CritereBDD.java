/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.bdd;

import ri.serien.libcommun.exploitation.bibliotheque.CritereBibliotheque;

/**
 * Cette classe regroupe l'ensemble des critères qu'il est possible de renseigner dans le cas d'une recherhe d'une base de données.
 */
public class CritereBDD extends CritereBibliotheque {
  // Variables
  private Character lettreEnvironnement = null;
  
  // -- Accesseurs
  
  /**
   * Retourne le filtre sur la lettre d'environnement.
   */
  public Character getLettreEnvironnement() {
    return lettreEnvironnement;
  }
  
  /**
   * Initialise le filtre sur la lettre d'environnement.
   */
  public void setLettreEnvironnement(Character lettreEnvironnement) {
    this.lettreEnvironnement = lettreEnvironnement;
  }
}
