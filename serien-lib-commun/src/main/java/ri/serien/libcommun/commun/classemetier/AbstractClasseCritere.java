/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.classemetier;

import java.util.HashMap;
import java.util.Map;

import ri.serien.libcommun.commun.objetmetier.champmetier.IdChampMetier;
import ri.serien.libcommun.commun.objetmetier.champmetier.InterfaceEnumChampMetier;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Classe parente de toutes les classes comportant des critères de recherche pour un objet métier.
 * 
 * Les critères de recherche sont stockés dans une map comme cela la liste des critères de recherche est entièrement évolutive.
 * La map contient l'identifiant du champ métier utilisé comme critère de recherche dans la clé et la valeur recherchée dans la valeur.
 */
public class AbstractClasseCritere {
  private Map<IdChampMetier, Object> mapCritere = new HashMap<IdChampMetier, Object>();
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur standard.
   */
  private AbstractClasseCritere() {
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthode publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Ajouter un critère de recherche (ou l'enlève si null est passé comme valeur).
   * @param pIdChampMetier Identifiant du champ métier qui sert de critère de recherche.
   * @param pValeur Valeur associée au critère de recherche (null pour supprimer le critère de recherche).
   */
  public void ajouterCritere(IdChampMetier pIdChampMetier, Object pValeur) {
    // Tester les paramètres
    if (pIdChampMetier == null) {
      throw new MessageErreurException("L'identifiant du champ métier est invalide");
    }
    
    // Tester s'il faut ajouter ou effacer le critère de recherche
    if (pValeur != null) {
      // Ajouter le critère de recherche
      mapCritere.put(pIdChampMetier, pValeur);
    }
    else {
      // Effacer le critère de recherche
      mapCritere.remove(pIdChampMetier);
    }
  }
  
  /**
   * Ajouter un critère de recherche (ou l'enlève si null est passé comme valeur).
   * @param pEnumChampMetier Enumération du champ métier qui sert de critère de recherche.
   * @param pValeur Valeur associée au critère de recherche (null pour supprimer le critère de recherche).
   */
  public void ajouterCritere(InterfaceEnumChampMetier pEnumChampMetier, Object pValeur) {
    ajouterCritere(IdChampMetier.getInstance(pEnumChampMetier), pValeur);
  }
  
  /**
   * Supprimer un critère de recherche.
   * @param pIdChampMetier Identifiant du champ métier qui sert de critère de recherche.
   */
  private void effacerCritere(IdChampMetier pIdChampMetier) {
    // Tester les paramètres
    if (pIdChampMetier == null) {
      throw new MessageErreurException("L'identifiant du champ métier est invalide");
    }
    
    // Effacer le critère de recherche
    mapCritere.remove(pIdChampMetier);
  }
  
  /**
   * Supprimer un critère de recherche.
   * @param pEnumChampMetier Enumération du champ métier qui sert de critère de recherche.
   */
  private void effacerCritere(InterfaceEnumChampMetier pEnumChampMetier) {
    effacerCritere(IdChampMetier.getInstance(pEnumChampMetier));
  }
  
  /**
   * Tester si le critère de recherche est présent.
   * @param pIdChampMetier Identifiant du champ métier qui sert de critère de recherche.
   * @return true=critère de recherche présent, false=critère de recherche non présent.
   */
  public boolean isCriterePresent(IdChampMetier pIdChampMetier) {
    // Tester les paramètres
    if (pIdChampMetier == null) {
      throw new MessageErreurException("L'identifiant du champ métier est invalide");
    }
    
    // Retourner la valeur associée au critère.
    return mapCritere.containsKey(pIdChampMetier);
  }
  
  /**
   * Tester si le critère de recherche est présent.
   * @param pEnumChampMetier Enumération du champ métier qui sert de critère de recherche.
   * @return true=critère de recherche présent, false=critère de recherche non présent.
   */
  public boolean isCriterePresent(InterfaceEnumChampMetier pEnumChampMetier) {
    return isCriterePresent(IdChampMetier.getInstance(pEnumChampMetier));
  }
  
  /**
   * Récupérer la valeur associée à un critère de recherche.
   * 
   * @param pIdChampMetier Identifiant du champ métier qui sert de critère de recherche.
   * @return Valeur de filtrage du champ métier.
   */
  public Object getCritere(IdChampMetier pIdChampMetier) {
    // Tester les paramètres
    if (pIdChampMetier == null) {
      throw new MessageErreurException("L'identifiant du champ métier est invalide");
    }
    
    // Retourner la valeur associée au critère.
    return mapCritere.get(pIdChampMetier);
  }
  
  /**
   * Récupérer la valeur associée à un critère de recherche.
   * @param pEnumChampMetier Enumération du champ métier qui sert de critère de recherche.
   * @return Valeur de filtrage du champ métier.
   */
  public Object getCritere(InterfaceEnumChampMetier pEnumChampMetier) {
    return getCritere(IdChampMetier.getInstance(pEnumChampMetier));
  }
}
