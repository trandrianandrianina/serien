/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.champbdd;

import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;

/**
 * Liste des champs de base de données pour la table client.
 */
public enum EnumChampPGVMCLIM implements InterfaceChampBDD {
  NUMERO("CLCLI", 6, Integer.class),
  // CIVILITE("CLCIV", 3, String.class), Ce champ n'existe pas en table
  NOM("CLNOM", 30, String.class),
  COMPLEMENT("CLCPL", 30, String.class),
  LOCALISATION("CLLOC", 30, String.class),
  CODE_POSTAL("CLCDP1", 5, String.class),
  COMMUNE("CLVIL", 30, String.class),
  PAYS("CLPAY", 26, String.class),
  TELEPHONE("CLTEL", 20, String.class),
  CATEGORIE("CLCAT", 3, String.class),
  TYPE("CLIN29", 1, Character.class),
  ETABLISSEMENT("CLETB", 3, String.class),
  NUMERO_LIVRE("CLLIV", 3, Integer.class);
  
  private final EnumTableBDD tableBDD = EnumTableBDD.CLIENT;
  private final String nom;
  private final Integer longueur;
  private final Class type;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  EnumChampPGVMCLIM(String pNom, Integer pLongueur, Class pType) {
    nom = pNom;
    longueur = pLongueur;
    type = pType;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le nom du champ précédé du nom de sa table.
   */
  @Override
  public String toString() {
    return tableBDD.getNom() + "." + nom;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner la table de base de données dont ce champ est constitutif.
   * @return EnumTableBDD Table de la base de données.
   */
  @Override
  public EnumTableBDD getTableBDD() {
    return tableBDD;
  }
  
  /**
   * Retourner le nom du champ dans la table.
   * Il s'agit du nom technique du champ dans la table afin d'être utilisé dans des requêtes SQL.
   * @return String Nom du champ.
   */
  @Override
  public String getNom() {
    return nom;
  }
  
  /**
   * Retourner la longueur maximale du champ dans la table.
   * Il s'agit ici de la longueur technique du champ dans la base de données.
   * @return int Longueur du champ.
   */
  @Override
  public Integer getLongueur() {
    return longueur;
  }
  
  /**
   * Retourner le type du champ dans la table.
   * @return Class Le type du champ.
   */
  @Override
  public Class getType() {
    return type;
  }
}
