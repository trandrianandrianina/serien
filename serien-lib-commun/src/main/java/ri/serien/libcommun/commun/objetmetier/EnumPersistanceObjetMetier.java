/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier;

import ri.serien.libcommun.commun.objetmetier.champmetier.InterfacePersistanceChampMetier;
import ri.serien.libcommun.exploitation.export.EnumPersistanceChampExport;
import ri.serien.libcommun.gescom.commun.client.EnumPersistanceChampClient;

/**
 * Description d'un objet métier.
 * 
 * Cette énumération permet de décrire les caractéristiques d'un objet métier. C'est une forme de persistance "interne" dans le code.
 * Les mêmes informations pourraient être sauvegardées dans un fichier XML ou dans une table de la base de données.
 * 
 * Il ne faut pas utiliser directement cette énumération dans le code. Il faut utiliser les classes "ObjetMetier" à la place. Cette
 * énumération ne doit servir que pour alimenter ces classes métiers.
 */
public enum EnumPersistanceObjetMetier {
  CLIENT(EnumObjetMetier.CLIENT, "Client", "cli", EnumPersistanceChampClient.values()),
  EXPORT(EnumObjetMetier.EXPORT, "Export", "exp", EnumPersistanceChampExport.values()),
  GROUPE(EnumObjetMetier.GROUPE, "Groupe", "grp", EnumPersistanceChampExport.values());
  
  private final EnumObjetMetier enumObjetMetier;
  private final String libelle;
  private final String trigramme;
  private final InterfacePersistanceChampMetier[] listeEnumPersistanceChampMetier;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pEnumObjetMetier Enumération correspondant à l'objet métier.
   * @param pLibelle Libellé de l'oblet métier.
   * @param pTrigramme Trigramme de l'oblet métier.
   * @param pListeEnumPersistanceChampMetier Liste des énumérations de persistance ds champs métiers.
   */
  EnumPersistanceObjetMetier(EnumObjetMetier pEnumObjetMetier, String pLibelle, String pTrigramme,
      InterfacePersistanceChampMetier[] pListeEnumPersistanceChampMetier) {
    enumObjetMetier = pEnumObjetMetier;
    libelle = pLibelle;
    trigramme = pTrigramme;
    listeEnumPersistanceChampMetier = pListeEnumPersistanceChampMetier;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le libellé associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return libelle + " (" + enumObjetMetier.toString() + ")";
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'énumération par son numéro.
   * @param pNumero Numéro unique de l'objet métier.
   * @return Enumération pour la persistance de l'objet métier.
   */
  static public EnumPersistanceObjetMetier valueOfByNumero(Integer pNumero) {
    if (pNumero == null) {
      return null;
    }
    for (EnumPersistanceObjetMetier value : values()) {
      EnumObjetMetier enumObjetMetier = value.getEnumObjetMetier();
      if (pNumero.equals(enumObjetMetier.getNumero())) {
        return value;
      }
    }
    return null;
  }
  
  /**
   * Retourner l'énumération par son identifiant.
   * @param pNumero Numéro unique de l'objet métier.
   * @return Enumération pour la persistance de l'objet métier.
   */
  static public EnumPersistanceObjetMetier valueOfById(IdObjetMetier pIdObjetMetier) {
    if (pIdObjetMetier == null) {
      return null;
    }
    for (EnumPersistanceObjetMetier value : values()) {
      EnumObjetMetier enumObjetMetier = value.getEnumObjetMetier();
      if (pIdObjetMetier.getNumero().equals(enumObjetMetier.getNumero())) {
        return value;
      }
    }
    return null;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'énumération de l'objet métier.
   * @return Enumération de l'objet métier.
   */
  public EnumObjetMetier getEnumObjetMetier() {
    return enumObjetMetier;
  }
  
  /**
   * Retourner le libellé de l'objet métier.
   * @return Libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner le trigramme de l'objet métier.
   * @return Trigramme.
   */
  public String getTrigramme() {
    return trigramme;
  }
  
  /**
   * Retourner la liste des énumération de persistance des champs métiers de l'objet métier.
   * @return Liste des énumérations de persistance des champ métiers.
   */
  public InterfacePersistanceChampMetier[] getListeEnumPersistanceChampMetier() {
    return listeEnumPersistanceChampMetier;
  }
}
