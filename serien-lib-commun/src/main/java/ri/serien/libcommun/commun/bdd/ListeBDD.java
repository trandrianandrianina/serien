/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.bdd;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;

/**
 * Liste d'objets Licence.
 * L'utilisation de cette classe est à privilégier dès qu'on manipule une liste de base de données.
 */
public class ListeBDD extends ListeClasseMetier<IdBibliotheque, BDD, ListeBDD> {
  
  /**
   * Constructeur.
   */
  public ListeBDD() {
  }
  
  /**
   * Constructeur par copie d'une liste.
   */
  public ListeBDD(ListeBDD pListeLicence) {
    super(pListeLicence);
  }
  
  // -- Méthodes publiques
  
  /**
   * Charger les base de données dont les identifiants sont fournis.
   */
  @Override
  public ListeBDD charger(IdSession pIdSession, List<IdBibliotheque> pListeIdBibliotheque) {
    return ManagerServiceTechnique.chargerListeBaseDeDonnees(pIdSession, pListeIdBibliotheque);
  }
  
  /**
   * Charge la liste complète des objets métiers pour l'établissement donné.
   */
  @Override
  public ListeBDD charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    throw new MessageErreurException("Le chargement des licences de l'application Série N par établissement n'a pas de raison d'être.");
  }
  
}
