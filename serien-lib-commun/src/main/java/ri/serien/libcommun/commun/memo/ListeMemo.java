/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.memo;

import java.util.ArrayList;

import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceParametre;

/**
 * Liste de mémos.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de mémos.
 */
public class ListeMemo extends ArrayList<Memo> {
  /**
   * Constructeur.
   */
  public ListeMemo() {
  }
  
  /**
   * Charger la liste des mémos associés à un document de vente.
   */
  public static ListeMemo charger(IdDocumentVente pIdDocumentVente) {
    return null;
  }
  
  /**
   * Charger la liste des mémos suivant les critères de recherche
   */
  public static ListeMemo charger(IdSession pIdSession, FiltreMemo pFiltreMemo) {
    return ManagerServiceParametre.chargerListeMemo(pIdSession, pFiltreMemo);
  }
  
  /**
   * Retourner un acheteur de la liste à partir de son identifiant.
   */
  public Memo retournerMemoParId(IdMemo pIdMemo) {
    if (pIdMemo == null) {
      return null;
    }
    for (Memo memo : this) {
      if (memo != null && Constantes.equals(memo.getId(), pIdMemo)) {
        return memo;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un mémo est présent dans la liste.
   */
  public boolean isPresent(IdMemo pIdMemo) {
    return retournerMemoParId(pIdMemo) != null;
  }
}
