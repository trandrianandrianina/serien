/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.planification;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.commun.joursemaine.EnumJourSemaine;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.DateHeure;

/**
 * Cette classe décrit les données propres à un objet métier de type planification et regroupe les méthodes qui lui sont associées.
 */
public class Planification extends AbstractClasseMetier<IdPlanification> {
  
  private EnumPlanificationPeriodicite periodicite = null;
  private Integer intervalle = null;
  private EnumJourSemaine jourEcheance = null;
  private EnumQuantieme quantiemeEcheance = null;
  private Date dateDeclenchement = null;
  private Integer heureDeclenchement = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pIdPlanification L'identifiant de la planification.
   */
  public Planification(IdPlanification pIdPlanification) {
    super(pIdPlanification);
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Cette méthode est surchargée pour définir l'affichage dans les listes déroulantes.
   */
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner si la planification est valide en fonction de la périodicité choisie.
   * @return true=Valide, false=Non valide.
   */
  public boolean isValide() {
    // Contrôle de la périodicité
    if (periodicite == null) {
      throw new MessageErreurException("La périodicité est manquante.");
    }
    // Contrôle des valeurs par périodicité
    switch (periodicite) {
      case AUCUNE:
        intervalle = null;
        jourEcheance = null;
        quantiemeEcheance = null;
        break;
      case JOURNALIERE:
        controlerIntervalleEnJour(intervalle);
        jourEcheance = null;
        quantiemeEcheance = null;
        break;
      case HEBDOMADAIRE:
        controlerIntervalleEnSemaine(intervalle);
        if (jourEcheance == null) {
          throw new MessageErreurException("Le jour d'échéance de la périodicité hebdomadaire est manquant.");
        }
        quantiemeEcheance = null;
        break;
      case MENSUELLE:
        controlerIntervalleEnMois(intervalle);
        if (jourEcheance == null) {
          throw new MessageErreurException("Le jour d'échéance de la périodicité mensuelle est manquant.");
        }
        if (quantiemeEcheance == null) {
          throw new MessageErreurException("Le quantième d'échéance de la périodicité mensuelle est manquant.");
        }
        break;
      case ANNUELLE:
        jourEcheance = null;
        if (quantiemeEcheance == null) {
          throw new MessageErreurException("Le quantième d'échéance de la périodicité mensuelle est manquant.");
        }
        break;
    }
    
    // Contrôler la date de déclenchement
    if (dateDeclenchement == null) {
      throw new MessageErreurException("La date de déclenchement est manquante.");
    }
    controlerJourDeclenchement(dateDeclenchement);
    
    // Contrôler l'heure de déclenchement
    if (heureDeclenchement == null) {
      throw new MessageErreurException("L'heure de déclenchement est manquante.");
    }
    
    // Contrôler la validité de la date et heure de déclenchement
    controlerDateDeclenchement(dateDeclenchement, false);
    
    return true;
  }
  
  /**
   * Effacer les variables.
   * Utilisée lors du changement de périodicité.
   */
  public void effacerVariable() {
    intervalle = null;
    jourEcheance = null;
    quantiemeEcheance = null;
    dateDeclenchement = null;
    heureDeclenchement = null;
  }
  
  /**
   * Calculer la date du prochain déclenchement.
   * 
   * Les minutes, secondes et millisecondes sont mises systématiqement à zéro.
   */
  public void calculerDateProchainDeclenchement() {
    // Contrôle de la périodicité
    if (periodicite == null) {
      dateDeclenchement = null;
      return;
    }
    
    // Date de l'instant
    Calendar calendrier = Calendar.getInstance();
    
    // Calcul en fonction de la périodicité
    switch (periodicite) {
      case AUCUNE:
        if (dateDeclenchement == null) {
          return;
        }
        // Mise à jour avec la date de déclenchement
        calendrier.setTime(dateDeclenchement);
        break;
      case JOURNALIERE:
        dateDeclenchement = null;
        if (intervalle == null) {
          return;
        }
        // Ajout du nombre de jours à la date d'aujourd'hui
        calendrier.add(Calendar.DATE, intervalle.intValue());
        break;
      case HEBDOMADAIRE:
        dateDeclenchement = null;
        if (intervalle == null || jourEcheance == null) {
          return;
        }
        // Conversion du nombre de semaines en jours
        int nombreJour = 7 * intervalle;
        // Contrôler le jour de la semaine de l'échéance, si c'est dimanche il faut ajouter une semaine de plus car c'est considérer comme
        // la semaine prochaine chez les anglo-saxon
        if (jourEcheance == EnumJourSemaine.DIMANCHE) {
          nombreJour = nombreJour + 7;
        }
        // Ajouter le nombre de jour à la date d'aujourd'hui pour atteindre la semaine
        calendrier.add(Calendar.DATE, nombreJour);
        // Calcul afin de trouver le jour de la semaine souhaitée
        int jourDeLaSemaineActuel = calendrier.get(Calendar.DAY_OF_WEEK);
        int jourSouhaite = EnumJourSemaine.convertirJourSemaineEnJourCalendar(jourEcheance);
        int delta = jourSouhaite - jourDeLaSemaineActuel;
        calendrier.add(Calendar.DATE, delta);
        break;
      
      case MENSUELLE:
        dateDeclenchement = null;
        if (jourEcheance == null || quantiemeEcheance == null) {
          return;
        }
        // Ajout du nombre de mois à la date d'aujourd'hui
        calendrier.add(Calendar.MONTH, intervalle);
        // Conversion de la date en LocalDate
        LocalDate localDateMensuelle = LocalDateTime.ofInstant(calendrier.toInstant(), calendrier.getTimeZone().toZoneId()).toLocalDate();
        // Conversion l'enum jour échéance en DayOfWeek
        DayOfWeek jourDeLaSemaine = EnumJourSemaine.convertirJourSemaineEnJourDayOfWeek(jourEcheance);
        
        // Calcul en fonction du quantième
        if (quantiemeEcheance == EnumQuantieme.PREMIER) {
          localDateMensuelle = localDateMensuelle.with(TemporalAdjusters.firstInMonth(jourDeLaSemaine));
        }
        else if (quantiemeEcheance == EnumQuantieme.DERNIER) {
          localDateMensuelle = localDateMensuelle.with(TemporalAdjusters.lastInMonth(jourDeLaSemaine));
        }
        // Conversion de la date au format Calendar
        Date dateMensuelle = Date.from(localDateMensuelle.atStartOfDay(ZoneId.systemDefault()).toInstant());
        calendrier.setTime(dateMensuelle);
        break;
      
      case ANNUELLE:
        dateDeclenchement = null;
        if (quantiemeEcheance == null || heureDeclenchement == null) {
          return;
        }
        // Récupération de la date du jour
        LocalDate localDateAnnuelle = LocalDate.now();
        // Calcul en fonction du quantième
        if (quantiemeEcheance == EnumQuantieme.PREMIER) {
          // Calcul du premier jour de l'année
          localDateAnnuelle = localDateAnnuelle.with(TemporalAdjusters.firstDayOfYear());
        }
        else if (quantiemeEcheance == EnumQuantieme.DERNIER) {
          // Calcul du dernier jour de l'année
          localDateAnnuelle = localDateAnnuelle.with(TemporalAdjusters.lastDayOfYear());
        }
        // Conversion de la date en calendar
        Date date = Date.from(localDateAnnuelle.atStartOfDay(ZoneId.systemDefault()).toInstant());
        calendrier.setTime(date);
        // Contrôle de l'heure de déclenchement, si elle est inférieure ou égale à l'heure actuelle alors le déclenchement sera pour
        // l'année prochaine
        int heureActuelle = LocalTime.now().getHour();
        if (heureDeclenchement <= heureActuelle) {
          calendrier.add(Calendar.YEAR, 1);
        }
        break;
    }
    
    // Mise à jour de la date de déclenchement
    if (heureDeclenchement != null) {
      calendrier.set(Calendar.HOUR_OF_DAY, heureDeclenchement);
    }
    else {
      calendrier.set(Calendar.HOUR_OF_DAY, 0);
    }
    calendrier.set(Calendar.MINUTE, 0);
    calendrier.set(Calendar.SECOND, 0);
    calendrier.set(Calendar.MILLISECOND, 0);
    dateDeclenchement = calendrier.getTime();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Contrôler le jour de déclenchement (ne tient pas compte de l'heure).
   * @param pDate La date.
   */
  public static Date controlerJourDeclenchement(Date pDate) {
    // Contrôle de la valeur saisie
    if (pDate == null) {
      throw new MessageErreurException("La date de déclenchement est invalide.");
    }
    Date maintenant = Constantes.mettreHeureAZero(new Date());
    pDate = Constantes.mettreHeureAZero(pDate);
    if (pDate.compareTo(maintenant) < 0) {
      throw new MessageErreurException("Le jour de déclenchement doit être aujourd'hui ou plus tard.");
    }
    
    return pDate;
  }
  
  /**
   * Contrôler la date et heure de déclenchement .
   * @param pDate La date.
   */
  public static Date controlerDateDeclenchement(Date pDate, boolean pSilencieux) {
    // Contrôle de la valeur saisie
    if (pDate == null) {
      // Si le mode silencieux est activé, null est retourné
      if (pSilencieux) {
        return null;
      }
      // Sinon une exception est déclenchée
      throw new MessageErreurException("La date de déclenchement est invalide.");
    }
    Calendar calendrier = Calendar.getInstance();
    
    // Préparer la date actuelle en mettant à zéro les minutes et seondes
    Date maintenant = new Date();
    calendrier.set(Calendar.MINUTE, 0);
    calendrier.set(Calendar.SECOND, 0);
    calendrier.set(Calendar.MILLISECOND, 0);
    maintenant = calendrier.getTime();
    
    // La date de déclenchement doit être stritement supérieure à la date et heure actuelle
    if (pDate.compareTo(maintenant) <= 0) {
      // Si le mode silencieux est activé, null est retourné
      if (pSilencieux) {
        return null;
      }
      // Sinon une exception est déclenchée
      throw new MessageErreurException("La date de déclenchement doit être supérieure à la date et heure actuelle.");
    }
    
    return pDate;
  }
  
  /**
   * Contrôler la valeur de l'intervalle pour les jours.
   * @param pIntervalle L'intervalle.
   */
  public static Integer controlerIntervalleEnJour(Integer pIntervalle) {
    // Contrôle de la valeur saisie
    if (pIntervalle == null) {
      throw new MessageErreurException("L'intervalle est invalide.");
    }
    if (pIntervalle < 1 || pIntervalle > 7) {
      throw new MessageErreurException("L'intervalle de temps doit être compris entre 1 et 7 jours inclus.");
    }
    
    return pIntervalle;
  }
  
  /**
   * Contrôler la valeur de l'intervalle pour les semaines.
   * @param pIntervalle L'intervalle.
   */
  public static Integer controlerIntervalleEnSemaine(Integer pIntervalle) {
    // Contrôle de la valeur saisie
    if (pIntervalle == null) {
      throw new MessageErreurException("L'intervalle est invalide.");
    }
    if (pIntervalle < 1 || pIntervalle > 5) {
      throw new MessageErreurException("L'intervalle de temps doit être compris entre 1 et 5 semaines inclus.");
    }
    
    return pIntervalle;
  }
  
  /**
   * Contrôler la valeur de l'intervalle pour les mois.
   * @param pIntervalle L'intervalle.
   */
  public static Integer controlerIntervalleEnMois(Integer pIntervalle) {
    // Contrôle de la valeur saisie
    if (pIntervalle == null) {
      throw new MessageErreurException("L'intervalle est invalide.");
    }
    if (pIntervalle < 1 || pIntervalle > 12) {
      throw new MessageErreurException("L'intervalle de temps doit être compris entre 1 et 12 mois inclus.");
    }
    
    return pIntervalle;
  }
  
  /**
   * Contrôler la valeur de l'heure.
   * @param pHeure L'heure.
   */
  public static Integer controlerHeure(Integer pHeure) {
    // Contrôle de la valeur saisie
    if (pHeure == null) {
      throw new MessageErreurException("L'heure est invalide.");
    }
    if (pHeure < 0 || pHeure > 23) {
      throw new MessageErreurException("L'heure doit être comprise entre 0 et 23 heures.");
    }
    
    return pHeure;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner la périodicité l'évènement.
   * @return L'enum.
   */
  public EnumPlanificationPeriodicite getPeriodicite() {
    return periodicite;
  }
  
  /**
   * Modifier la périodicité de l'évènement.
   * @param pPeriodicite La périodicité de l'évènement.
   */
  public void setPeriodicite(EnumPlanificationPeriodicite pPeriodicite) {
    periodicite = pPeriodicite;
  }
  
  /**
   * Retourner l'intervalle de temps (en fonction de la périodicité en cours).
   * @return L'intervalle de temps.
   */
  public Integer getIntervalle() {
    return intervalle;
  }
  
  /**
   * Modifier l'intervalle de temps (en fonction de la périodicité en cours).
   * @param pIntervalle L'intervalle de temps.
   */
  public void setIntervalle(Integer pIntervalle) {
    intervalle = pIntervalle;
  }
  
  /**
   * Retourner le jour échéance du déclenchement de l'évènement.
   * @return Le jour.
   */
  public EnumJourSemaine getJourEcheance() {
    return jourEcheance;
  }
  
  /**
   * Modifier le jour échéance du déclenchement de l'évènement.
   * @param pJourEcheance Le jour de la semaine ou null.
   */
  public void setJourEcheance(EnumJourSemaine pJourEcheance) {
    jourEcheance = pJourEcheance;
  }
  
  /**
   * Retourner le quantième échéance du déclenchement de l'évènement.
   * @return Le quantième.
   */
  public EnumQuantieme getQuantiemeEcheance() {
    return quantiemeEcheance;
  }
  
  /**
   * Modifier le quantième échéance du déclenchement de l'évènement.
   * @param pQuantiemeEcheance Le quantième ou null.
   */
  public void setQuantiemeEcheance(EnumQuantieme pQuantiemeEcheance) {
    quantiemeEcheance = pQuantiemeEcheance;
  }
  
  /**
   * Retourner la date de déclenchement de l'évènement.
   * @return La date.
   */
  public Date getDateDeclenchement() {
    return dateDeclenchement;
  }
  
  /**
   * Modifier la date de déclenchement de l'évènement.
   * @param pDateDeclenchement La date de déclenchement.
   */
  public void setDateDeclenchement(Date pDateDeclenchement) {
    dateDeclenchement = pDateDeclenchement;
  }
  
  /**
   * Retourner l'heure de déclenchement de l'évènement.
   * @return L'heure.
   */
  public Integer getHeureDeclenchement() {
    return heureDeclenchement;
  }
  
  /**
   * Modifier l'heure de déclenchement de l'évènement.
   * @param pHeure L'heure de déclenchement (valeurs possibles de 0 à 23).
   */
  public void setHeureDeclenchement(Integer pHeure) {
    heureDeclenchement = pHeure;
  }
  
  /**
   * Retourner la date et l'heure de déclenchement sous forme d'une chaine formatée.
   * @return La chaine formatée.
   */
  public String getDateHeureDeclenchementFormate() {
    if (dateDeclenchement == null) {
      return "";
    }
    // Le jour
    String texte = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, dateDeclenchement);
    
    // L'heure
    if (heureDeclenchement != null) {
      texte += " à " + heureDeclenchement + " heure";
    }
    return texte;
  }
}
