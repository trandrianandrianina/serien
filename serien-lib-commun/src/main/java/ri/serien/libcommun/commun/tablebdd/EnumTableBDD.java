/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.tablebdd;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des tables de la base de données.
 * 
 * Chaque table est décrite par son nom dans la base de données (PGVMCLIM pour la table clientr par exemple) et une description qui
 * résume le rôle de la table en une ligne.
 * 
 * Pour rendre homogène le nommage des entrées :
 * - séparer chaque mot par le caractère '_'.
 * - ne pas mettre de pluriels,
 * - ne pas utiliser d'abréviation saufs les exceptions citées ci-dessous,
 * - la mention ENTETE n'est pas nécessaire, elle est implicite (DOCUMENT_VENTE = entête document de vente).
 * 
 * Il est demandé de présenter les entrées dans l'ordre alpabétique. Ne pas oublier qu'il y aura 800 entrées à terme. Il est judicieux
 * de penser le nom commme les différentes parties d'une arboresence plutôt que comme du français. Cela permet de regrouper les tables
 * qui ont du sens ensembles. Par exemple :
 * - les tables pour les clients se nomment CLIENT et CLIENT_EXTENSION (plutôt que EXTENSION_CLIENT).
 * - les tables pour les documents de ventes se nomment DOCUMENT_VENTE, DOCUMENT_VENTE_LIGNE, DOCUMENT_VENTE_EXTENSION,
 * DOCUMENT_VENTE_INDEX et DOCUMENT_VENTE_ADRESSE.
 * 
 * Exceptions permises pour les abréviations :
 * - BDD pour base de données.
 */
public enum EnumTableBDD {
  ARTICLE("PGVMARTM", "Articles"),
  ARTICLE_EXTENSION("PGVMEAAM", "Extentions d'un article"),
  ARTICLE_INDEX("PGVMDTA2", "Indexation des articles avec coefficient de pertinence"),
  ARTICLE_LIE("PGVMARLM", "Articles liés"),
  ARTICLE_TARIF("PGVMTARM", "Tarifs d'un article"),
  ARTICLE_TAXE("PGVMTEEM", "Taxes d'un article"),
  ARTICLE_URL("PSEMFTCM", "URLs d'un article"),
  ARTICLE_LOT_STOCK("PGVMSTLM", "Stock par lots"),
  ARTICLE_LOT_HISTORIQUE_STOCK("PGVMHLOM", "Historique stock par lots"),
  DOCUMENT_STOCKE("PSEMDSKM", "Document stocké"),
  DOCUMENT_STOCKE_LIEN_DOCUMENT_VENTE("PSEMDSKBCM", "Liens entre documents stockés et documents de ventes"),
  DOCUMENT_STOCKE_LIEN_CLIENT("PSEMDSKCLM", "Liens entre documents stockés et clients"),
  LETTRE_ENVIRONNMENT_BDD("PSEMFMVM", "Indique la lettre d'environnement d'une bases de données"),
  BLOCNOTES("PGVMOBSM", "Bloc-notes"),
  BON_COUR("PGVMBDCM", "Bons de cours"),
  BORDEREAU_STOCK("PGVMEBSM", "Entête des bordereaux de stock"),
  CARNET_BON_COUR("PGVMCDCM", "Carnets de bons de cours"),
  CLIENT("PGVMCLIM", "Clients"),
  CLIENT_COMPTABILITE("PCGMPCAM", "Plan comptable auxiliaire"),
  CLIENT_EXTENSION("PGVMECBM", "Extentions de la fiche client"),
  COMMUNE("PSEMRCOM", "Communes"),
  CONDITION_ACHAT("PGVMCNAM", "Conditions d'achats"),
  CONDITION_VENTE("PGVMCNVM", "Conditions de ventes"),
  CONDITION_VENTE_QUANTITATIVE("PGVMCNQM", "Conditions de ventes quantitatives"),
  CONDITION_VENTE_LIGNE_VENTE("PGVMLCNM", "Conditions de ventes de la ligne de ventes"),
  CATALOGUE_FOURNISSEUR("PGVMCTEM", "Entêtes du paramétrage du catalogue fournisseur"),
  CATALOGUE_FOURNISSEUR_LIGNE("PGVMCTDM", "Détails du paramétrage du catalogue fournisseur"),
  CONFIGURATION_ZONE_ERP("PGVMCT01", "Configuration des zones ERP"),
  CONTACT("PSEMRTEM", "Contacts"),
  CONTACT_LIEN("PSEMRTLM", "Liens entre les contacts et les fiches clients ou fournisseurs"),
  CONTACT_ADRESSE("PSEMRTAM", "Adresses des contacts"),
  CONTACT_WEBSHOP("PSEMRTWM", "Contacts du webshop"),
  DEMANDE_DEBLOCAGE_DOCUMENT("PGVMHDEM", "Demandes de déblocage de documents"),
  DEMANDE_DEBLOCAGE_VENTE_SOUS_PRV("PGVMDPXM", "Demandes de déblocages des ventes en dessous du prix de revient"),
  DEMANDE_EDITION("PSEMPNSIMM", "Demandes d'éditions NewSim"),
  DEMANDE_EDITION_EXTENSION("PSEMENSIMM", "Extentions des demandes d'éditions NewSim"),
  DESCRIPTION_PRODEVIS("PGVMPRDM", "Description des pro-devis"),
  DEVISE("PSEMDEVM", "Devises"),
  DOCUMENT_LIE_PARAMETRE("PSEMPDMM", "Paramètres des documents liés"),
  DOCUMENT_VENTE("PGVMEBCM", "Entêtes des documents de ventes"),
  DOCUMENT_VENTE_ADRESSE("PGVMADVM", "Adresses des documents de ventes"),
  DOCUMENT_VENTE_EXTENSION("PGVMXLIM", "Extentions des documents de ventes"),
  DOCUMENT_VENTE_INDEX("PGVMIDXM", "Indexation généralisée des documents de ventes"),
  DOCUMENT_VENTE_LIGNE("PGVMLBCM", "Lignes des documents de ventes"),
  DOCUMENT_ACHAT("PGVMEBFM", "Entêtes des documents d'achats"),
  DOCUMENT_ACHAT_ADRESSE("PGVMADAM", "Adresses des documents d'achats"),
  DOCUMENT_ACHAT_INDEX("PGVMIDAM", "Indexation généralisée des documents d'achats"),
  DOCUMENT_ACHAT_LIGNE("PGVMLBFM", "Lignes des documents d'achats"),
  ENVIRONNEMENT("PSEMENVM", "Environnements"),
  EXPORT("PSEMTEXPM", "Exports"),
  EXPORT_COLONNE("PSEMCEXPM", "Colonnes des exports"),
  FLUX("PSEMHFLXM", "Flux"),
  FLUX_LIEN_INSTANCE("PSEMINSTM", "Liens entre les flux et les instances"),
  FLUX_VERSION_INSTANCE("PSEMGSTFM", "Versions des instances"),
  FLUX_HISTORIQUE_ENVOI_STOCK("PSEMFSTKM", "Historique du flux des stocks"),
  FLUX_PARAMETRE("PSEMFXPA", "Paramétrage des flux"),
  FOURNISSEUR("PGVMFRSM", "Fournisseurs"),
  FOURNISSEUR_ADRESSE("PGVMFREM", "Adresses fournisseur"),
  HISTORIQUE_MONOPUMP("PGVMHMPM", "Historique mono PUMP"),
  IMPORT_PRODEVIS("PGVMPRDM", "Importation des pro-devis"),
  LICENCE("PSEMLICM", "Licences des logiciels"),
  LICENCE_ANCIENNE("PSEMDRTM", "Anciennes licences des logiciels"),
  LIEN_LIGNE_ACHAT("PGVMLDAM", "Liens lignes achats"),
  LIEN_LIGNE_VENTE_LIGNE_ACHAT("PGVMALAM", "Liens entre les documents de ventes et les documents d'achats"),
  MAIL("PSEMMAILMM", "E-mails"),
  MAIL_CONFIGURATION_ENVOI("PSEMSMM", "Configurations de l'envoi d'E-mails"),
  MAIL_DESTINATAIRE("PSEMDCCMAM", "Destinataires des E-mails"),
  MAIL_PIECE_JOINTE("PSEMPCJMAM", "Pièces jointes des E-mails"),
  MAIL_TYPE("PSEMTYPMAM", "Types d'E-mails"),
  MAIL_VARIABLE("PSEMVARMAM", "Variables des E-mails"),
  MENU_EXTENSION("MNEMSGM", "Extentions du menu du logiciel"),
  MENU_EXTENSION_SPECIFIQUE("MNSPMEM", "Extentions spécifiques du menu du logiciel"),
  MENU_PROGRAMME("MNPMSGM", "Programmes affichés dans le menu du logiciel"),
  MENU_PROGRAMME_SPECIFIQUE("MNSPMPM", "Programmes spécifiques affichés dans le menu du logiciel"),
  NOTIFICATION("PSEMNOTIFM", "Notifications"),
  OBSERVATION("PSEMOBPM", "Entêtes des observations"),
  OBSERVATION_LIGNE("PSEMOBSM", "Lignes des observations"),
  PARAMETRE_AVANCE("PSEMPAVM", "Paramètres avancés"),
  PARAMETRE_COMPTABILITE("PCGMPACM", "Paramètres de la comptabilité"),
  PARAMETRE_EXPLOITATION("PSEMPARM", "Paramètres de l'exploitation"),
  PARAMETRE_GESCOM("PGVMPARM", "Paramètres de la gestion commerciale"),
  PARAMETRE_MODULE("PSEMPMODM", "Liste des paramètres de l'application et de ses modules"),
  PETIT_BLOCNOTES_DOCUMENT("PGVMXLIM", "Petits bloc-notes des documents"),
  PLAN_POSE_SIPE("PGVMSIPM", "Plan pose SIPE"),
  CONFIGURATION_TABLEAU("PSEMCTABM", "Configuration des tableaux pour affichage"),
  CONFIGURATION_COLONNE("PSEMCCOLM", "Configuration des colonnes de tableaux pour affichage"),
  PRIX_CLIENT_ARTICLE_SUR_DOC("PGVMPCAM", "Prix client/articles sur le document en cours"),
  REGLEMENT("PGEMRGLM", "Reglements"),
  REPRESENTANT("PGVMREPM", "Représentants"),
  STOCK("PGVMSTKM", "Stocks"),
  STOCK_HISTORIQUE("PGVMHISM", "Historique des stocks"),
  TRACE_CONNEXION("PSEMUSRCM", "Traces des connexions"),
  UTILISATEUR("PSEMUSSM", "Utilisateurs du logiciel"),
  ZONE_GEOGRAPHIQUE("PSEMRZGM", "Zones géographiques"),
  PLANIFICATION("PSEMPLANM", "Planification d'actions");
  
  private final String nom;
  private final String description;
  
  /**
   * Constructeur.
   * @param pNom Nom de la table.
   * @param pDescription Description de la table.
   */
  EnumTableBDD(String pNom, String pDescription) {
    nom = pNom;
    description = pDescription;
  }
  
  /**
   * Nom de la table dans la base de données.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * Description courte de la table.
   */
  public String getDescription() {
    return description;
  }
  
  /**
   * Retourner le nom de la table.
   * 
   * L'idée est de faciliter l'utilisation de cette énumération dans la génération des requêtes SQL. Il suffit de mettre EnumTableBDD.MAIL
   * plutôt que EnumTableBDD.MAIL.getNom() pour ajouter le nom d'une table dans une requête SQL.
   * 
   * Exemple :
   * String sql = "UPDATE " + querymg.getLibrary() + "." + EnumTableBDD.MAIL + " SET MASTAT = 1 WHERE MASTAT = 0");
   */
  @Override
  public String toString() {
    return nom;
  }
  
  /**
   * Retourner l'objet énum par le nom de la table.
   */
  static public EnumTableBDD valueOfByCode(String pNom) {
    for (EnumTableBDD value : values()) {
      if (pNom.equals(value.getNom())) {
        return value;
      }
    }
    throw new MessageErreurException("Le nom de table de base de données n'est pas référencé dans la liste des tables : " + pNom);
  }
}
