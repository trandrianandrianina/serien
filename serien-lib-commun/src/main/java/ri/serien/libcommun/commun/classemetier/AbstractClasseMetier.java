/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.classemetier;

import java.io.Serializable;
import java.text.Normalizer;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Classe parente de tous les objets métiers comportant un identifiant.
 * 
 * Cette classe mutualise ce qui est commun à tous les objets métiers :
 * - Gestion de l'identifiant,
 * - Implémentation de equals(): un objet métier est égal à un autre objet métier si les identifiants sont identiques.
 * - Implémentation de hashCode().
 * - Gestion du caractère chargé ou non de l'objet métier.
 * - Gestion de données complémentaires.
 */
public abstract class AbstractClasseMetier<I extends AbstractId> implements Serializable, Cloneable {
  protected I id = null;
  private boolean charge = true;
  private Object complement = null;
  
  /**
   * Constructeur nécessitant la fourniture d'un identifiant.
   */
  public AbstractClasseMetier(I pId) {
    setId(pId);
  }
  
  /**
   * Retourner une représentation de l'objet métier sous forme d'une chaîne de caractères.
   * 
   * Cette méthode est utilisée à chaque fois qu'on veut une représentation de l'objet métier sous forme textuelle. Cette méthode est
   * notamment utilisée comme libellé des entrées dans les listes déroulantes
   */
  abstract public String getTexte();
  
  /**
   * Comparer de deux objets métiers.
   * Deux objets métiers sont considérés comme égaux s'ils ont le même identifiant fonctionnel.
   * Si on souhaite comparer exhaustivement les données, utiliser la méthode equalsComplet().
   */
  @Override
  public boolean equals(Object pObject) {
    // Tester si l'objet est nous-même (optimisation)
    if (pObject == this) {
      return true;
    }
    
    // Tester le type de l'objet (teste la nullité également)
    // Ce cas se rencontre notamment lorsqu'une liste déroulante comporte la valeur "" ou "Tous".
    if (!(pObject instanceof AbstractClasseMetier<?>)) {
      return false;
    }
    
    // Comparer les identifiants des ojets métiers
    AbstractClasseMetier<I> objetMetier = (AbstractClasseMetier<I>) pObject;
    return id.equals(objetMetier.id);
  }
  
  /**
   * Retourner la clé de hachage de l'objet métier (c'est celle de l'identifiant).
   */
  @Override
  public int hashCode() {
    return id.hashCode();
  }
  
  /**
   * Cloner un objet de la classe.
   */
  @Override
  public AbstractClasseMetier clone() throws CloneNotSupportedException {
    AbstractClasseMetier o = null;
    o = (AbstractClasseMetier) super.clone();
    o.setId(getId());
    o.setCharge(charge);
    o.setComplement(getComplement());
    
    return o;
  }
  
  /**
   * Retourner une représentation de l'objet métier pour les listes déroulantes.
   * 
   * Lorsqu'un objet métier est ajouté dans une liste déroulante, c'est cette méthode toString() qui est utilisée pour déterminer le
   * libellé à afficher. Pour l'instant, l'implémentation par défaut de toString() retourne le texte de l'objet métier suivi du texte
   * de l'identifiant entre parenthèses : Libellé (Identifiant)
   * 
   * On avait essayé une implémentation dans laquelle seul le libellé était retourné si l'identifiant était identique au libellé
   * mais cela fait bizarre dans les listes. Certaines entrées sont au format "Libellé (Identifiant)" tandis que d'autres ont justes
   * "Libellé". Ce n'est pas homogène.
   */
  @Override
  public String toString() {
    final String libelle = getTexte();
    final String identifiant = id.getTexte();
    
    // Retourner le libellé suivi de l'identifiant entre parenthèses si tout va bien (libellé et identifiant valides)
    if (libelle != null && !libelle.isEmpty() && identifiant != null && !identifiant.isEmpty()) {
      return libelle + " (" + identifiant + ")";
    }
    // Retourner le libellé seul s'il est valide (ce cas ne doit pas arriver mais est géré par sécurité)
    else if (libelle != null && !libelle.isEmpty()) {
      return libelle;
    }
    // Retourner l'identifiant seul s'il est valide
    else if (identifiant != null && !identifiant.isEmpty()) {
      return "(" + identifiant + ")";
    }
    else {
      // Ne rien retourner si le libellé et l'identifiant sont invalides
      return "";
    }
  }
  
  /**
   * Identifiant de l'objet métier.
   */
  public I getId() {
    return id;
  }
  
  /**
   * Modifier l'identifiant de l'objet métier.
   */
  public void setId(I pId) {
    if (pId == null) {
      throw new MessageErreurException("Un objet métier doit obligatoirement avoir un identifiant.");
    }
    id = pId;
  }
  
  /**
   * Libellé de l'objet métier sous forme d'une chaîne de caractères normalisées.
   * Ce libellé peut servir pour les recherches. Il est normalisé de la façon suivante :
   * - En majuscules.
   * - Sans accents.
   */
  public String getTexteNormalise() {
    String texte = getTexte();
    if (texte == null) {
      return null;
    }
    
    // Mettre en majuscules
    texte = texte.toUpperCase();
    
    // Normaliser le texte pour enlever les accents
    texte = Normalizer.normalize(texte, Normalizer.Form.NFD);
    texte = texte.replaceAll("[^\\x00-\\x7F]", "");
    return texte;
  }
  
  /**
   * Tester si l'objet métier est déjà existant dans la base de données.
   * Il est considéré existant si son identifiant est complet.
   */
  public boolean isExistant() {
    return id != null && id.isExistant();
  }
  
  /**
   * Tester si les données de l'objet métier ont été chargée.
   */
  public boolean isCharge() {
    return charge;
  }
  
  /**
   * Indiquer si les données de l'objet métier sont chargées ou non.
   * Par défaut, lorsqu'on objet métier est instancié, on considère que ses données sont chargées et cette émthode retourne true.
   * Sans les cas où on instantie un objet métier sans renseigner ses données, il faut appeler setCharge(false) pour le signaler.
   */
  public void setCharge(boolean pCharge) {
    charge = pCharge;
  }
  
  /**
   * Complément d'information éventuellement rattaché à l'objet métier.
   */
  public Object getComplement() {
    return complement;
  }
  
  /**
   * Modifier le complément d'information rattaché à l'objet métier.
   * Le complément d'information permet d'associer des informations supplémentaires à un objet métier pour son affichage dans une table.
   */
  public void setComplement(Object pComplement) {
    complement = pComplement;
  }
  
  /**
   * Contrôler un message afin de générer une exception ou un retour true/false suivant le paramètre.
   * Cette méthode
   */
  protected boolean controlerMessage(String pMessage, boolean pAvecException) {
    if (pMessage != null && !pMessage.trim().isEmpty()) {
      if (pAvecException) {
        throw new MessageErreurException(pMessage);
      }
      else {
        return false;
      }
    }
    return true;
  }
}
