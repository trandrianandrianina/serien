/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier.champmetier;

import ri.serien.libcommun.commun.champbdd.InterfaceChampBDD;
import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;

/**
 * Champ métier.
 * 
 * Un champ métier permet de décrire les informations disponibles pour un champ, en lui fournissant un identifiant unique, un libellé
 * court et une description en une ligne. Des informations sur l'affichage sont également disponibles comme le caractère obligatoire
 * de l'information, la largeur d'affichage préconisée en pixels et la justification par défaut (à gauche, au centre ou à droite).
 * 
 * Noter que la plupart des champs métiers sont associés à un champ base de données mais cela n'est pas systématique. Les objets métiers
 * peuvent proposer des informations qui ne sont pas persistées en base de données, des informations calculées par exemple.
 * 
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class ChampMetier extends AbstractClasseMetier<IdChampMetier> {
  private final String libelle;
  private final InterfaceChampBDD champBDD;
  private final Integer largeurAffichage;
  private final Integer justification;
  private final String description;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur de l'objet métier.
   * 
   * C'est le seul contructeur valide car l'objet métier doit être renseigné à partir de l'énumération dédiée à la persistance.
   * Le constructeur est protégé de façon à ce que seul ManagerChampMetier puisse l'utiliser.
   */
  protected ChampMetier(InterfacePersistanceChampMetier pInterfaceChampObjetMetier) {
    super(IdChampMetier.getInstance(pInterfaceChampObjetMetier));
    libelle = pInterfaceChampObjetMetier.getLibelle();
    champBDD = pInterfaceChampObjetMetier.getChampBDD();
    largeurAffichage = pInterfaceChampObjetMetier.getLargeurAffichage();
    justification = pInterfaceChampObjetMetier.getJustification();
    description = pInterfaceChampObjetMetier.getDescription();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public String getTexte() {
    return id.getTexte();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le libellé du champ métier.
   * @return Libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner le champ base de données associé au champ métier.
   * Cette information peut être nulle pour des informations métiers qui ne sont pas directement présentes en base de données mais
   * issues d'un calcul.
   * @return InterfaceChampBDD Champ base de données (peut être null).
   */
  public InterfaceChampBDD getChampBDD() {
    return champBDD;
  }
  
  /**
   * Indiquer si ce champ métier est obligatoirement affiché.
   * Cela permet à'linterface graphique d'imposer l'affichage de cette information. Actuellement, seul le premier champ de chaque
   * objet métier est considéré comme obligatoire.
   * @return boolean true=affichage obligatoire, false= affichage optionnel (valeur par défaut).
   */
  public boolean isAffichageObligatoire() {
    return getId().getNumero() == 1;
  }
  
  /**
   * Retourner la largeur préconisée pour l'affichage du champ métier.
   * Cette information peut être utilisée pour définir la largeur d'un champ dans un écran ou la largeur de la colonne dans un tableau.
   * @return Integer Largeur en pixel.
   */
  public Integer getLargeurAffichage() {
    return largeurAffichage;
  }
  
  /**
   * Retourner la justification préconisée pour l'affichage du champ métier.
   * Cette information peut être utilisée pour jusitifier la valeur uc hamp métiezr dans un composant graphique ou dans la colonne
   * d'un tableau.
   * @return Integer JUSTIFICATION_GAUCHE, JUSTIFICATION_CENTRE ou JUSTIFICATION_DROITE.
   */
  public Integer getJustification() {
    return justification;
  }
  
  /**
   * Retourner la description du champ métier.
   * Cette description est un résumé en une ligne su rôle du champ métier.
   * @return String Description.
   */
  public String getDescription() {
    return description;
  }
}
