/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.id;

import java.io.Serializable;

import ri.serien.libcommun.outils.Constantes;

/**
 * Classe de base de tous les identifiants du logiciel.
 *
 * Les langages RPG et Java doivent s'échanger des informations sur des concepts qui peuvent être orientés fonctionnels
 * (client, fournisseur, article, ...) ou orientés techniques (fichier, imprimante, poste de travail, ...). Ces concepts sont
 * désignés de manière collective sous le terme "objets métiers".
 * 
 * Un point important est d'avoir un identifiant commun. Lorsque le langage RPG communique par exemple un article au langage Java,
 * il ne faut pas que le Java se trompe d'article. La base de la communication entre Java et RPG repose donc sur la création d'un
 * identifiant commun pour chaque objet métier.
 * 
 * Définition : L'identifiant d'un objet métier permet d'identifier de façon unique un objet métier que cela soit en RPG ou en
 * Java. Règle 1 : un objet métier ne doit avoir qu'un seul identifiant. Règle 2 : un identifiant est unique, il ne désigne qu'un
 * seul objet métier.
 * 
 * Afin de mutualiser et normaliser la création d'identifiant d'objets métiers, les classes AbstractId et AbstractIdAvecEtablissement
 * (pour les identifiants comportant un code établissement) ont été créées.
 * 
 * Cette classe possède un état modifiable qui correspond au traitement en base de données que l'on souhaite appliquer à l'objet métier
 * correspondant à l'identifiant.
 */
public abstract class AbstractId implements Serializable, Comparable {
  // Constantes
  public static final char SEPARATEUR_ID = '-';
  
  // Variables
  private EnumEtatObjetMetier etatObjetMetier = EnumEtatObjetMetier.MODIFIE;
  
  /**
   * Constructeur.
   */
  public AbstractId(EnumEtatObjetMetier pEtatObjetMetier) {
    if (pEtatObjetMetier != null) {
      etatObjetMetier = pEtatObjetMetier;
    }
  }
  
  // Méthodes abstraites à surcharger dans les classes enfants
  
  /**
   * Retourner la clé de hachage de l'identifiant.
   * 
   * La clé de hachage doit être déterminée à partir de tous les champs constituant l'identifiant. Il est également très important
   * que les méthodes hashCode(), equals() et compareTo() utilisent les mêmes champs sous peine d'avoir des bugs subtils.
   * 
   * Voici l'implémentation à suivre en exemple :
   * public int hashCode() {
   * int cle = 17;
   * cle = 37 * cle + [Champ 1];
   * cle = 37 * cle + [Champ 2];
   * cle = 37 * cle + [Champ 3];
   * ...
   * return cle;
   * }
   */
  @Override
  abstract public int hashCode();
  
  /**
   * Texter l'égalité de deux identifiants.
   * 
   * Deux identifiant sont égaux si tous leur champs sont égaux. Il est très important que les méthodes hashCode(), equals() et
   * compareTo() utilisent les mêmes champs sous peine d'avoir des bugs subtils.
   */
  @Override
  abstract public boolean equals(Object pObject);
  
  /**
   * Comparer deux identifiants.
   * 
   * La valeur de retour est :
   * - négative si cet identifiant est inférieur à l'identifiant fourni en paramètre,
   * - nulle si cet identifiant est égal à l'identifiant fourni en paramètre,
   * - positive si cet identifiant est supérieur à l'identifiant fourni en paramètre.
   * 
   * Il est très important que les méthodes hashCode(), equals() et compareTo() utilisent les mêmes champs sous peine d'avoir des bugs
   * subtils.
   */
  @Override
  abstract public int compareTo(Object pObject);
  
  /**
   * Retourner une représentation de l'identifiant sous forme d'une chaîne de caractères.
   * 
   * Cette méthode est utilisée à chaque fois qu'on veut une représentation de l'identifiant sous forme textuelle. C'est notamment le
   * cas dans les listes déroulantes, le texte de l'identifiant situés entre parenthèses provient de cette méthode.
   * 
   * Typiquement, le texte d'un identifiant composé de deux champs (établissement et code) sera uniquement composé du code.
   * Exemple : idRepresentant
   * public String getTexte() {
   * return code;
   * }
   * 
   * Le texte d'un identifiant composé de trois champs ou plus (dont l'ébablissement) sera construit à partir de tous les champs
   * sauf l'établissement, séparés par la constante SEPARATEUR_ID.
   * 
   * Exemple : idFournisseur
   * public String getTexte() {
   * return "" + collectif + SEPARATEUR_ID + numero;
   * }
   */
  abstract public String getTexte();
  
  /**
   * Retourner une représentation de l'identifiant pour les listes déroulantes.
   * 
   * Lorsqu'un identifiant est ajouté dans une liste déroulante, c'est cette méthode toString() qui est utilisée pour déterminer le
   * libellé à afficher. Pour l'instant, l'implémentation par défaut de toString() retourne getTexte().).
   */
  @Override
  public String toString() {
    return getTexte();
  }
  
  // -- Accesseurs
  
  /**
   * Indiquer si l'identifiant a été persisté dans la base de données.
   * Cette valeur est à true si l'identifiant a été lu dans la base de données. Elle est à false si un nouvel identifiant a été créé
   * et n'a pas encore été sauvegardé en base de données. Dans le cas d'une création, les données de l'identifiant peuvent être
   * vides, cas d'un identifiant attribué lors de l'insertion dans la base de données, ou renseignées dans le cas d'un identifiant
   * attribué logiciellement.
   */
  public boolean isExistant() {
    return !Constantes.equals(etatObjetMetier, EnumEtatObjetMetier.CREE);
  }
  
  /**
   * Indiquer si l'identifiant n'existe pas encore en base de données mais est en cours de création.
   * 
   * @return true si l'état est en cours de création, false dans le cas contraire
   */
  public boolean isCree() {
    return Constantes.equals(etatObjetMetier, EnumEtatObjetMetier.CREE);
  }
  
  /**
   * Indiquer si l'identifiant existe en base de données et est en cours de modification.
   * 
   * @return true si l'état est en cours de modification, false dans le cas contraire
   */
  public boolean isModifie() {
    return Constantes.equals(etatObjetMetier, EnumEtatObjetMetier.MODIFIE);
  }
  
  /**
   * Indiquer si l'identifiant existe en base de données et est en cours de désactivation.
   * 
   * @return true si l'état est en cours de désctivation, false dans le cas contraire
   */
  public boolean isDesactive() {
    return Constantes.equals(etatObjetMetier, EnumEtatObjetMetier.DESACTIVE);
  }
  
  /**
   * Indiquer si l'identifiant existe en base de données et est en cours de suppression.
   * 
   * @return true si l'état est en cours de suppression, false dans le cas contraire
   */
  public boolean isSupprime() {
    return Constantes.equals(etatObjetMetier, EnumEtatObjetMetier.SUPPRIME);
  }
  
  /**
   * Indiquer si l'identifiant n'est jamais persisté en base de données.
   * 
   * @return true si l'objet métier ne doit jamais être persisté, false dans le cas contraire
   */
  public boolean isSansPersistance() {
    return Constantes.equals(etatObjetMetier, EnumEtatObjetMetier.SANS_PERSISTANCE);
  }
  
  /**
   * Modifier l'état de l'objet métier représenté par l'identifiant
   * 
   * @param pEtatObjetMetier
   */
  public void setEtatObjetMetier(EnumEtatObjetMetier pEtatObjetMetier) {
    etatObjetMetier = pEtatObjetMetier;
  }
}
