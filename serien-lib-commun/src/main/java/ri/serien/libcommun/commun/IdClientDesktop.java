/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe définit l'id d'un client desktop.
 */
public class IdClientDesktop extends AbstractId {
  // Constantes
  public static final IdClientDesktop AUCUN = new IdClientDesktop();
  public static final int LONGUEUR_NUMERO = 9;
  
  // Variables
  private final Integer numero;
  
  /**
   * Constructeur.
   */
  private IdClientDesktop(EnumEtatObjetMetier pEtatObjetMetier, int pNumero) {
    super(pEtatObjetMetier);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Créer un nouvel identifiant.
   */
  public static IdClientDesktop getInstance(int pNumero) {
    return new IdClientDesktop(EnumEtatObjetMetier.MODIFIE, pNumero);
  }
  
  /**
   * Constructeur privé pour le client desktop nul.
   */
  private IdClientDesktop() {
    super(EnumEtatObjetMetier.CREE);
    numero = 0;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler la validité du numéro du client desktop.
   * Il doit être supérieur à zéro et doit comporter au maximum 9 chiffres (entre 1 et 999 999 999).
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro du client desktop n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro du client desktop est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro du client desktop est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + numero.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    if (pObject == null) {
      return false;
    }
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pObject instanceof IdClientDesktop)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de client desktop.");
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    IdClientDesktop id = (IdClientDesktop) pObject;
    return numero.equals(id.numero);
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdClientDesktop)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdClientDesktop id = (IdClientDesktop) pObject;
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return "" + numero;
  }
  
  // -- Accesseurs
  
  /**
   * Numéro du client desktop.
   * Le numéro du client desktop est compris entre entre 0 et 999 999 999.
   */
  public Integer getNumero() {
    return numero;
  }
  
}
