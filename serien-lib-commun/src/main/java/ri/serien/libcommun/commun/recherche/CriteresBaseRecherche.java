/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.recherche;

import java.io.Serializable;

import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

/**
 * Classe de base pour toutes les recherches.
 * 
 * Il faut supprimer cette classe à terme.
 * NOTE: Penser à alimenter dans les constantes le type de recherche (ex: RECHERCHE_COMPTOIR).
 */
public class CriteresBaseRecherche implements Serializable {
  // Constantes
  public static final int TOUTES_LES_LIGNES = -1;
  // Car c'est la généralité en RPG
  public static final int NOMBRE_LIGNES_PAR_PAGE = 15;
  
  public static final int PAS_DE_CRITERES = 0;
  public static final int RECHERCHE_EN_COMPTE = 1;
  public static final int RECHERCHE_COMPTANT = 2;
  
  // Le numéro de la page à récupérer (la première page a le numéro 1)
  private int numeroPage = 1;
  // Le nombre de ligne par page
  private int nbrLignesParPage = NOMBRE_LIGNES_PAR_PAGE;
  // Le nombre de page maximum quand il est connu ou détecté (c'est la limite imposé par la longueur de la variable RPG 3/0)
  private int nombrePagesMax = 999;
  
  private int typeRecherche = PAS_DE_CRITERES;
  private boolean ignoreCasse = true;
  private boolean chaineExacte = true;
  private IdEtablissement idEtablissement = null;
  private String expression = "";
  // Tous clients/en compte/comptants
  private EnumTypeCompteClient typeClient = null;
  
  // -- Méthodes publiques
  
  /**
   * Retourne le critère transformé pour la requête.
   */
  public String getCriterePourRequete() {
    String chaine = expression;
    if (ignoreCasse) {
      chaine = expression.toUpperCase();
    }
    if (!chaineExacte) {
      chaine = '%' + chaine + '%';
    }
    return chaine;
  }
  
  /**
   * Retourne le critère transformé pour le RPG.
   */
  public String getCriterePourRPG() {
    String chaine = expression;
    if (ignoreCasse) {
      chaine = expression.toUpperCase();
    }
    return chaine;
  }
  
  /**
   * Initialise avec la valeur par défaut le nombre de page max.
   */
  public void initialiserNombrePagesMax() {
    nombrePagesMax = Integer.MAX_VALUE;
  }
  
  // -- Accesseurs
  
  public int getNumeroPage() {
    return numeroPage;
  }
  
  public void setNumeroPage(int numeroPage) {
    this.numeroPage = numeroPage;
  }
  
  public int getNbrLignesParPage() {
    return nbrLignesParPage;
  }
  
  public void setNbrLignesParPage(int nbrLignesParPage) {
    this.nbrLignesParPage = nbrLignesParPage;
  }
  
  public int getNombrePagesMax() {
    return nombrePagesMax;
  }
  
  public void setNombrePagesMax(int nombrePageMax) {
    this.nombrePagesMax = nombrePageMax;
  }
  
  public boolean isIgnoreCasse() {
    return ignoreCasse;
  }
  
  public void setIgnoreCasse(boolean ignoreCasse) {
    this.ignoreCasse = ignoreCasse;
  }
  
  public boolean isChaineExacte() {
    return chaineExacte;
  }
  
  public void setChaineExacte(boolean chaineExacte) {
    this.chaineExacte = chaineExacte;
  }
  
  public int getTypeRecherche() {
    return typeRecherche;
  }
  
  public void setTypeRecherche(int typeRecherche) {
    this.typeRecherche = typeRecherche;
  }
  
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  public String getTexteRecherche() {
    return expression;
  }
  
  public void setTexteRecherche(String aexpression) {
    this.expression = aexpression.toUpperCase();
  }
  
  public EnumTypeCompteClient getTypeClient() {
    return typeClient;
  }
  
  public void setTypeClient(EnumTypeCompteClient typeClient) {
    this.typeClient = typeClient;
  }
}
