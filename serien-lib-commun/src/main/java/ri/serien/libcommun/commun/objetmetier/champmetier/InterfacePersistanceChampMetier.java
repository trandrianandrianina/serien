/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier.champmetier;

import ri.serien.libcommun.commun.champbdd.InterfaceChampBDD;

/**
 * Interface pour la description des champs métiers.
 * 
 * Cette interface est à implémenter par les énumérations qui décrivent la liste des champs d'un objet métier.
 * En théorie, on aurait pu décrire l'ensemble des champs des objets métiers dans un énorme Enum mais nous allions atteindre une
 * limite technique de Java sur la taille de l'Enum. La base de données comportent plusieurs dizaines de milliers de champs. Nous avons
 * donc choisit de faire un Enum par objet métier. Cela permet accessoirement de mettre cet Enum dans le package correspondant à son
 * objet métier.
 * 
 * Cette énumération permet de décrire les caractéristiques d'un champ métier. C'est une forme de persistance "interne" dans le code.
 * Les mêmes informations pourraient être sauvegardées dans un fichier XML ou dans une table de la base de données.
 * Il ne faut pas utiliser directement cette énumération dans le code. Il faut utiliser les classes "ChampMetier" à la place. Cette
 * énumération ne doit servir que pour alimenter ces classes métiers.
 *
 * L'objectif est de fournir les informations fonctionnelles des champs métiers. On y trouve le nom et description du champ mais aussi
 * des informations sur la manière d'afficher le champ : ordre par défaut, affichage obligatoire, taille préconisée en pixels,
 * justification préconisée.
 * 
 * Le champ métier est également associé à un champ BDD lorsque le champ métier est persisté dans la base de données. Cette information
 * peut être nulle pour des informations métiers qui ne sont pas directement présentes en base de données mais issues d'un calcul :
 * opérations arithmétiques entre d'autres champs métiers, informations déduites d'un autre champ, ...
 * 
 * Il doit y avoir une implémentation de cette interface pour chaque objet métier.
 * Voir ri.serien.libcommun.gescom.commun.client.EnumPersistanceChampClient pour exemple.
 */
public interface InterfacePersistanceChampMetier {
  public static final int JUSTIFICATION_GAUCHE = 0;
  public static final int JUSTIFICATION_DROITE = 1;
  public static final int JUSTIFICATION_CENTRE = 2;
  
  /**
   * Retourner l'énumération du champ métier qui contient l'identifiant unique.
   * 
   * @return Enumération du champ métier.
   */
  public InterfaceEnumChampMetier getEnumChampMetier();
  
  /**
   * Retourner le libellé du champ métier (obligatoire).
   * 
   * Ce libellé qui se doit d'être court est utilisé pour l'affichage du titre de colonne par exemple.
   * Le libellé et l'objet métier associé au libellé du champ consitue l'identifiant unique de l'objet métier. Si leur valeur est
   * modifiée, cela peut rendre invalide des données persistées avec cet indentiiant.
   * 
   * @return String Libellé court.
   */
  public String getLibelle();
  
  /**
   * Retourner le champ base de données associé au champ métier (optionnel).
   * 
   * Cette information peut être nulle pour des informations métiers qui ne sont pas directement présentes en base de données mais
   * issues d'un calcul.
   * 
   * @return InterfaceChampBDD Champ base de données (peut être null).
   */
  public InterfaceChampBDD getChampBDD();
  
  /**
   * Retourner la largeur préconisée pour l'affichage du champ métier.
   * 
   * Cette information peut être utilisée pour définir la largeur d'un champ dans un écran ou la largeur de la colonne dans un tableau.
   * 
   * @return Integer Largeur en pixel.
   */
  public Integer getLargeurAffichage();
  
  /**
   * Retourner la justification préconisée pour l'affichage du champ métier.
   * 
   * @return Integer JUSTIFICATION_GAUCHE, JUSTIFICATION_CENTRE ou JUSTIFICATION_DROITE.
   */
  public Integer getJustification();
  
  /**
   * Retourner la description du champ métier.
   * 
   * Cette description est un résumé en une ligne du rôle du champ métier.
   * 
   * @return String Description.
   */
  public String getDescription();
  
}
