/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.planification;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceCommun;

public class ListePlanification extends ListeClasseMetier<IdPlanification, Planification, ListePlanification> {
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur
   */
  public ListePlanification() {
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Charger les planifications.
   * @param pIdSession Identifiant de la session.
   * @param pListeId Liste d'identifiants de planifications.
   * @return Liste de planifications.
   */
  @Override
  public ListePlanification charger(IdSession pIdSession, List<IdPlanification> pListeId) {
    if (pListeId == null) {
      throw new MessageErreurException("La liste d'IdPlanification passée en paramètre ne peut pas être null");
    }
    return ManagerServiceCommun.chargerListePlanification(pIdSession, pListeId);
  }
  
  /**
   * Charger les planifications d'un établissement.
   * Méthode non implémentée.
   * @param pIdSession La session.
   * @param pIdEtablissement L'identifiant de l'établissement.
   */
  @Override
  public ListePlanification charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    return null;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
}
