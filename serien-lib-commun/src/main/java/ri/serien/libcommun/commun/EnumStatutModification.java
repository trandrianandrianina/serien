/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des statuts possibles pour une modification d'un objet.
 */
public enum EnumStatutModification {
  TOTALEMENT(0, "Totalement modifiable"),
  PARTIELLEMENT(1, "Partiellement modifiable"),
  NON(2, "Non modifiable");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumStatutModification(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numéro sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le numéro associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(numero);
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumStatutModification valueOfByNumero(Integer pNumero) {
    for (EnumStatutModification value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le statut de la modification est invalide : " + pNumero);
  }
  
}
