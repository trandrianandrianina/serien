/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.memo;

import ri.serien.libcommun.commun.id.AbstractIdAvecEtablissement;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * L'identifiant unique d'un mémo.
 *
 * L'identifiant est composé du code établissement, d'un code entête, d'un code ligne, d'un code tiers et d'un numéro.
 * Cette classe est "Immutable" car ses attributs ne peuvent plus changer après son instanciation.
 */
public class IdMemo extends AbstractIdAvecEtablissement {
  // Constantes
  private static final int LONGUEUR_INDICATIF_TIERS = 20;
  private static final int LONGUEUR_INDICATIF = 30;
  private static final int LONGUEUR_NUMERO = 4;
  
  // Variables
  private final EnumCodeEnteteMemo codeEntete;
  private final EnumCodeLigneMemo codeLigne;
  private final String codeTiers;
  private final Integer numero;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdMemo(EnumEtatObjetMetier pEnumEtatObjetMetier, IdEtablissement pIdEtablissement, EnumCodeEnteteMemo pCodeEntete,
      EnumCodeLigneMemo pCodeLigne, String pCodeTiers, Integer pNumero) {
    super(pEnumEtatObjetMetier, pIdEtablissement);
    codeEntete = controlerCodeEntete(pCodeEntete, pCodeLigne);
    codeLigne = controlerCodeLigne(pCodeEntete, pCodeLigne);
    codeTiers = controlerCodeTiers(pCodeTiers);
    numero = controlerNumero(pNumero);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdMemo getInstance(IdEtablissement pIdEtablissement, EnumCodeEnteteMemo pCodeEntete, EnumCodeLigneMemo pCodeLigne,
      String pCodeTiers, Integer pNumero) {
    return new IdMemo(EnumEtatObjetMetier.MODIFIE, pIdEtablissement, pCodeEntete, pCodeLigne, pCodeTiers, pNumero);
  }
  
  /**
   * Créer un identifiant mémo pour un document de vente.
   * Le mémo est de type D pour les devis ou chantier et de type E pour les autres documents de ventes.
   */
  public static IdMemo getInstancePourDocumentVente(IdDocumentVente pIdDocumentVente, Integer pNumero) {
    if (pIdDocumentVente.isIdDevis()) {
      return new IdMemo(EnumEtatObjetMetier.MODIFIE, pIdDocumentVente.getIdEtablissement(), EnumCodeEnteteMemo.DEVIS,
          EnumCodeLigneMemo.DEVIS, pIdDocumentVente.getIndicatif(IdDocumentVente.INDICATIF_NUM), pNumero);
    }
    else {
      return new IdMemo(EnumEtatObjetMetier.MODIFIE, pIdDocumentVente.getIdEtablissement(), EnumCodeEnteteMemo.DOCUMENT,
          EnumCodeLigneMemo.DOCUMENT, pIdDocumentVente.getIndicatif(IdDocumentVente.INDICATIF_NUM), pNumero);
    }
  }
  
  /**
   * Créer un identifiant mémo pour un article.
   */
  public static IdMemo getInstancePourArticle(IdArticle pIdArticle, Integer pNumero) {
    return new IdMemo(EnumEtatObjetMetier.MODIFIE, pIdArticle.getIdEtablissement(), EnumCodeEnteteMemo.ARTICLE, EnumCodeLigneMemo.ARTICLE,
        pIdArticle.getCodeArticle(), pNumero);
  }
  
  /**
   * Contrôler la validité du code entête.
   * Les codes entête et lignes sont liés (et ne doivent pas être vides).
   */
  private static EnumCodeEnteteMemo controlerCodeEntete(EnumCodeEnteteMemo pCodeEntete, EnumCodeLigneMemo pCodeLigne) {
    if (pCodeEntete == null) {
      if (pCodeLigne == null) {
        throw new MessageErreurException("Le code entête et le code ligne ne sont pas renseignés.");
      }
      else if (pCodeLigne.equals(EnumCodeLigneMemo.ARTICLE)) {
        pCodeEntete = EnumCodeEnteteMemo.ARTICLE;
      }
      else if (pCodeLigne.equals(EnumCodeLigneMemo.DEVIS)) {
        pCodeEntete = EnumCodeEnteteMemo.DEVIS;
      }
      else if (pCodeLigne.equals(EnumCodeLigneMemo.DOCUMENT)) {
        pCodeEntete = EnumCodeEnteteMemo.DOCUMENT;
      }
      else {
        throw new MessageErreurException("Le code entête et le code ligne sont invalides.");
      }
    }
    
    return pCodeEntete;
  }
  
  /**
   * Contrôler la validité du code ligne.
   * Les codes entête et lignes sont liés (et ne doivent pas être vides).
   */
  private static EnumCodeLigneMemo controlerCodeLigne(EnumCodeEnteteMemo pCodeEntete, EnumCodeLigneMemo pCodeLigne) {
    if (pCodeLigne == null) {
      if (pCodeEntete == null) {
        throw new MessageErreurException("Le code entête et le code ligne ne sont pas renseignés.");
      }
      else if (pCodeEntete.equals(EnumCodeEnteteMemo.ARTICLE)) {
        pCodeLigne = EnumCodeLigneMemo.ARTICLE;
      }
      else if (pCodeEntete.equals(EnumCodeEnteteMemo.DEVIS)) {
        pCodeLigne = EnumCodeLigneMemo.DEVIS;
      }
      else if (pCodeEntete.equals(EnumCodeEnteteMemo.DOCUMENT)) {
        pCodeLigne = EnumCodeLigneMemo.DOCUMENT;
      }
      else {
        throw new MessageErreurException("Le code entête et le code ligne sont invalides.");
      }
    }
    
    return pCodeLigne;
  }
  
  /**
   * Contrôler la validité du code tiers.
   */
  private static String controlerCodeTiers(String pValeur) {
    // Renseigner l'indicatif tiers
    if (pValeur == null || pValeur.trim().isEmpty()) {
      throw new MessageErreurException("Le code tiers n'est pas renseigné.");
    }
    return pValeur.trim();
  }
  
  /**
   * Contrôler la validité du numéro.
   * Le numéro doit être compris entre 1 et 9 999 inclus.
   */
  private static Integer controlerNumero(Integer pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le numéro de mémo n'est pas renseigné.");
    }
    if (pValeur <= 0) {
      throw new MessageErreurException("Le numéro de mémo est inférieur ou égal à zéro.");
    }
    if (pValeur > Constantes.valeurMaxZoneNumerique(LONGUEUR_NUMERO)) {
      throw new MessageErreurException("Le numéro de mémo est supérieur à la valeur maximum possible.");
    }
    return pValeur;
  }
  
  /**
   * Vérifier la validité de l'identifiant.
   * Un identifiant est considéré comme valide s'il n'est pas null et si ses données sont renseignées.
   */
  public static IdMemo controlerId(IdMemo pId, boolean pVerifierExistence) {
    if (pId == null) {
      throw new MessageErreurException("L'identifiant du mémo est invalide.");
    }
    
    if (pVerifierExistence && !pId.isExistant()) {
      throw new MessageErreurException("L'identifiant du mémo n'existe pas dans la base de données : " + pId);
    }
    return pId;
  }
  
  // Méthodes surchargées
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + getCodeEtablissement().hashCode();
    cle = 37 * cle + codeEntete.hashCode();
    cle = 37 * cle + codeLigne.hashCode();
    cle = 37 * cle + codeTiers.hashCode();
    cle = 37 * cle + numero.hashCode();
    return cle;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    
    if (pObject == null) {
      return false;
    }
    
    if (!(pObject instanceof IdMemo)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de mémo.");
    }
    IdMemo id = (IdMemo) pObject;
    return (getCodeEtablissement().equals(id.getCodeEtablissement()) && codeEntete.equals(id.codeEntete) && codeLigne.equals(id.codeLigne)
        && codeTiers.equals(id.codeTiers) && numero.equals(id.numero));
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdMemo)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdMemo id = (IdMemo) pObject;
    int comparaison = getIdEtablissement().compareTo(id.getIdEtablissement());
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = codeEntete.compareTo(id.codeEntete);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = codeLigne.compareTo(id.codeLigne);
    if (comparaison != 0) {
      return comparaison;
    }
    comparaison = codeTiers.compareTo(id.codeTiers);
    if (comparaison != 0) {
      return comparaison;
    }
    return numero.compareTo(id.numero);
  }
  
  @Override
  public String getTexte() {
    return "" + codeEntete + SEPARATEUR_ID + codeLigne + SEPARATEUR_ID + codeTiers + SEPARATEUR_ID + numero;
  }
  
  // -- Accesseurs
  
  /**
   * Code entête du mémo.
   * Les codes entête et lignes sont liés (et ne doivent pas être vides).
   */
  public EnumCodeEnteteMemo getCodeEntete() {
    return codeEntete;
  }
  
  /**
   * Code ligne du mémo.
   * Les codes entête et lignes sont liés (et ne doivent pas être vides).
   */
  public EnumCodeLigneMemo getCodeLigne() {
    return codeLigne;
  }
  
  /**
   * Code tiers du mémo.
   * Comme le code article, le numéro du document par exemple.
   */
  public String getCodeTiers() {
    return codeTiers;
  }
  
  /**
   * Numéro du mémo.
   * Le numéro est compris entre 1 et 9 999.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Indicatif complet du mémo.
   * C'est une clé formatée : si le numéro est > 0 alors Code article + Numéro sinon juste le code article.
   */
  public String getIndicatif() {
    String indicatif = Constantes.normerTexte(codeTiers);
    if (indicatif.isEmpty()) {
      throw new MessageErreurException("L'indicatif tiers est vide.");
    }
    
    if (indicatif.length() > LONGUEUR_INDICATIF_TIERS) {
      indicatif = indicatif.substring(0, LONGUEUR_INDICATIF_TIERS);
    }
    if (numero > 0) {
      return String.format("%-" + (LONGUEUR_INDICATIF - LONGUEUR_NUMERO) + "s%0" + LONGUEUR_NUMERO + "d", indicatif, numero);
    }
    return String.format("%-" + LONGUEUR_INDICATIF + "s", indicatif);
  }
}
