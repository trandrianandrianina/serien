/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.message;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Importance d'un message.
 * 
 * Cela permet de qualifier le niveau d'importance d'un message textuel : soit normal, soit important. Les messages normaux sont
 * destinés à être affichés en noir tandis que les messages importants sont affichés en rouge.
 */
public enum EnumImportanceMessage {
  NORMAL(0, "Normal"),
  MOYEN(1, "Moyen"),
  HAUT(2, "Haut");
  
  private final int code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumImportanceMessage(int pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public int getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumImportanceMessage valueOfByCode(int pCode) {
    for (EnumImportanceMessage value : values()) {
      if (pCode == value.getCode()) {
        return value;
      }
    }
    throw new MessageErreurException("Le code du niveau d'importance du message est invalide : " + pCode);
  }
}
