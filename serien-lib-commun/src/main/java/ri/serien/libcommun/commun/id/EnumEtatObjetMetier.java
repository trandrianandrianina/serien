/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.id;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Etat d'un identifiant en vu de sa persistance dans la base de données.
 * 
 * Voici la description des états possibles :
 * - CREE : L'objet métier correspondant à cet identifiant n'a pas été persisté dans la base de données. Il est en cours de création.
 * L'objet métier devra être persisté par une requête d'insertion (INSERT).
 * - MODIFIE : L'objet métier correspondant à cet identifiant a déjà été persisté dans la base de données. C'est objet métier que l'on
 * lit et qui pourra éventuellement être modifié. L'objet métier devra être persisté par une requête de mise à jour (UPDATE).
 * - DESACTIVE : L'objet métier correspondant à cet identifiant a déjà été persisté dans la base de données mais il a été désactivé. La
 * désactivation est l'équivalent d'une suppression mais en conservant les données dans la base. L'objet métier devra être persisté par
 * une requête de mise à jour (UPDATE).
 * - SUPPRIME : L'objet métier correspondant à cet identifiant a déjà été persisté dans la base de données mais il doit être supprimé.
 * L'objet métier devra être persisté par une requête de suppression (DELETE).
 * - SANS_PERSISTANCE : L'objet métier correspondant à cet identifiant ne doit pas être persisté dans la base de données.
 */
public enum EnumEtatObjetMetier {
  CREE(1, "En cours de création"),
  MODIFIE(2, "Existe et peut être modifié"),
  DESACTIVE(3, "Désactivé"),
  SUPPRIME(4, "En cours de suppression"),
  SANS_PERSISTANCE(5, "Sans persistance");
  
  private final int code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumEtatObjetMetier(int pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code correspondant au traitement voule en base de données.
   */
  public int getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourner le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumEtatObjetMetier valueOfByCode(int pCode) {
    for (EnumEtatObjetMetier value : values()) {
      if (pCode == value.getCode()) {
        return value;
      }
    }
    throw new MessageErreurException("L'état de l'identifiant est invalide : " + pCode);
  }
}
