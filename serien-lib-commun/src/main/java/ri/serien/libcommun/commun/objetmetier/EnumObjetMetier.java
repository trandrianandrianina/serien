/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.objetmetier;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des numéros permettant d'identifier un objet métier.
 * 
 * L'identifiant d'un objet métier doit être unique et ne jamais évoluer, y compris suite à un refactoring : renommage d'une classe ou
 * d'une énumération, réordonnancement des entrées dans une énumération... Du coup, cette énumération permet d'attribuer un numéro
 * manuellement à chaque objet métier. Ce numéro ne doit ensuite jamais être modifié.
 */
public enum EnumObjetMetier {
  CLIENT(1),
  EXPORT(2),
  GROUPE(3);
  
  private final Integer numero;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pNumero Numéro unique de l'objet métier.
   */
  EnumObjetMetier(Integer pNumero) {
    numero = pNumero;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'objet énum par son numéro.
   * @return L'énum.
   */
  static public EnumObjetMetier valueOfByCode(Integer pNumero) {
    for (EnumObjetMetier value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le numéro ne correspond pas à objet métier existant : " + pNumero);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le numéro unique de l'objet métier.
   * @return Le numéro.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Retourner le libellé associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   * @return Le texte.
   */
  @Override
  public String toString() {
    return "" + numero;
  }
  
}
