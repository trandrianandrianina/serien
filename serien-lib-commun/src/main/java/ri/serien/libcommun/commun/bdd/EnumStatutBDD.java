/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libcommun.commun.bdd;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des statuts possibles pour une base de données.
 */
public enum EnumStatutBDD {
  NORMAL_ALTERNATIF(' ', "Normal"),
  NORMAL('0', "Normal"),
  TRAITEMENT_EXCLUSIF('1', "Traitement exlusif"),
  PROBLEME_BLOQUANT('2', "Problème bloquant");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumStatutBDD(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée dans la dataarea PSEMDTA.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Le libellé associé au code en minuscules.
   */
  public String getLibelleEnMinuscules() {
    return libelle.toLowerCase();
  }
  
  /**
   * Retourner le libellé associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumStatutBDD valueOfByCode(Character pCode) {
    for (EnumStatutBDD value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le statut de la base de données est invalide : " + pCode);
  }
  
  /**
   * Retourner l'objet énum par son code fourni sous forme d'une chaîne de caractères d'une longueur de 1.
   */
  static public EnumStatutBDD valueOfByCode(String pCode) {
    if (pCode.length() == 1) {
      Character code = pCode.charAt(0);
      for (EnumStatutBDD value : values()) {
        if (code.equals(value.getCode())) {
          return value;
        }
      }
    }
    throw new MessageErreurException("Le statut de la base de données est invalide : " + pCode);
  }
}
