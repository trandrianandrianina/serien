/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage.parametres;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

import ri.serien.demarrage.Constantes;
import ri.serien.demarrage.MessageErreurException;
import ri.serien.libcommun.outils.parametreclient.ParametresClient;

/**
 * Cette classe permet d'afficher un écran de saisie pour les paramètres de connexion de Série N.
 */
public class ParametreSerieN extends JDialog {
  // Variables
  private ParametresClient parametresClient = null;
  
  /**
   * Constructeur.
   */
  public ParametreSerieN(Window owner, ParametresClient pParametresClient) {
    super(owner);
    initComponents();
    
    parametresClient = pParametresClient;
    decorerWindow();
    initialiserComposant();
  }
  
  // -- Méthodes privées
  
  private void decorerWindow() {
    pnlPrincipal.setBackground(Constantes.COULEUR_FOND);
    tfDossierRacine.setBackground(Constantes.COULEUR_FOND_CHAMP_DESACTIVE);
    btValider.setFont(Constantes.POLICE_BOUTON);
    btValider.setBackground(Constantes.COULEUR_VERTE);
    btAnnuler.setFont(Constantes.POLICE_BOUTON);
    btAnnuler.setBackground(Constantes.COULEUR_ROUGE);
  }
  
  /**
   * Initialise les données de la fenêtre avec les paramètres.
   */
  private void initialiserComposant() {
    lbErreur.setText("");
    tfHost.requestFocusInWindow();
    
    if (parametresClient == null) {
      return;
    }
    
    tfReference.setText(parametresClient.getNom());
    tfHost.setText(parametresClient.getAdresseServeur());
    tfPort.setText("" + parametresClient.getPortServeur());
    tfDossierRacine.setText(parametresClient.getDossierRacineDesktop());
  }
  
  /**
   * Initialise les paramètres avec les données de la fenêtre.
   */
  private void initParameters() {
    if (parametresClient == null) {
      return;
    }
    
    // Faire un controle de la validité des données
    try {
      parametresClient.setNom(Constantes.normerTexte(tfReference.getText()));
      parametresClient.setAdresseServeur(Constantes.normerTexte(tfHost.getText()));
      try {
        parametresClient.setPortServeur(Integer.parseInt(Constantes.normerTexte(tfPort.getText())));
      }
      catch (Exception e) {
        parametresClient.setPortServeur(Constantes.SERVER_PORT);
      }
    }
    catch (MessageErreurException me) {
      lbErreur.setText("<html>" + me.getMessage() + "</html>");
    }
  }
  
  // -- Méthodes évènementielles
  
  private void btAnnulerActionPerformed(ActionEvent e) {
    System.exit(0);
  }
  
  private void btValiderActionPerformed(ActionEvent e) {
    initParameters();
    if (parametresClient.isValide()) {
      setVisible(false);
    }
    else {
      lbErreur.setText(parametresClient.getMsgErreur(true));
    }
  }
  
  private void genericFocusGained(FocusEvent e) {
    ((JTextComponent) e.getComponent()).selectAll();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlParametre = new JPanel();
    lbReference = new JLabel();
    tfReference = new JTextField();
    lbHost = new JLabel();
    tfHost = new JTextField();
    lbPort = new JLabel();
    tfPort = new JTextField();
    lbDossierRacine = new JLabel();
    tfDossierRacine = new JTextField();
    lbErreur = new JLabel();
    pnlBarreBouton = new JPanel();
    btValider = new JButton();
    btAnnuler = new JButton();
    
    // ======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setModal(true);
    setResizable(false);
    setTitle("Saisie des param\u00e8tres de S\u00e9rie N");
    setMinimumSize(new Dimension(600, 300));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBorder(new EmptyBorder(12, 12, 12, 12));
      pnlPrincipal.setMaximumSize(new Dimension(600, 300));
      pnlPrincipal.setMinimumSize(new Dimension(500, 250));
      pnlPrincipal.setPreferredSize(new Dimension(500, 250));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlParametre ========
      {
        pnlParametre.setOpaque(false);
        pnlParametre.setName("pnlParametre");
        pnlParametre.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlParametre.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlParametre.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlParametre.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlParametre.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ---- lbReference ----
        lbReference.setText("R\u00e9f\u00e9rence");
        lbReference.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbReference.setMaximumSize(new Dimension(200, 30));
        lbReference.setMinimumSize(new Dimension(200, 30));
        lbReference.setPreferredSize(new Dimension(200, 30));
        lbReference.setHorizontalAlignment(SwingConstants.RIGHT);
        lbReference.setName("lbReference");
        pnlParametre.add(lbReference, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfReference ----
        tfReference.setPreferredSize(new Dimension(150, 30));
        tfReference.setMinimumSize(new Dimension(150, 30));
        tfReference.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfReference.setName("tfReference");
        tfReference.addFocusListener(new FocusAdapter() {
          @Override
          public void focusGained(FocusEvent e) {
            genericFocusGained(e);
          }
        });
        pnlParametre.add(tfReference, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbHost ----
        lbHost.setText("Adresse IP ou nom du host");
        lbHost.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbHost.setHorizontalAlignment(SwingConstants.RIGHT);
        lbHost.setName("lbHost");
        pnlParametre.add(lbHost, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfHost ----
        tfHost.setPreferredSize(new Dimension(200, 30));
        tfHost.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfHost.setName("tfHost");
        tfHost.addFocusListener(new FocusAdapter() {
          @Override
          public void focusGained(FocusEvent e) {
            genericFocusGained(e);
          }
        });
        pnlParametre.add(tfHost, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPort ----
        lbPort.setText("Port");
        lbPort.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbPort.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPort.setName("lbPort");
        pnlParametre.add(lbPort, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPort ----
        tfPort.setHorizontalAlignment(SwingConstants.RIGHT);
        tfPort.setPreferredSize(new Dimension(100, 30));
        tfPort.setMinimumSize(new Dimension(75, 30));
        tfPort.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfPort.setName("tfPort");
        tfPort.addFocusListener(new FocusAdapter() {
          @Override
          public void focusGained(FocusEvent e) {
            genericFocusGained(e);
          }
        });
        pnlParametre.add(tfPort, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDossierRacine ----
        lbDossierRacine.setText("Dossier racine");
        lbDossierRacine.setPreferredSize(new Dimension(200, 30));
        lbDossierRacine.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbDossierRacine.setMaximumSize(new Dimension(2147483647, 2147483647));
        lbDossierRacine.setMinimumSize(new Dimension(200, 30));
        lbDossierRacine.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDossierRacine.setName("lbDossierRacine");
        pnlParametre.add(lbDossierRacine, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfDossierRacine ----
        tfDossierRacine.setPreferredSize(new Dimension(200, 30));
        tfDossierRacine.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfDossierRacine.setMaximumSize(new Dimension(2147483647, 2147483647));
        tfDossierRacine.setMinimumSize(new Dimension(200, 30));
        tfDossierRacine.setEditable(false);
        tfDossierRacine.setName("tfDossierRacine");
        pnlParametre.add(tfDossierRacine, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbErreur ----
        lbErreur.setText("Erreur");
        lbErreur.setForeground(Color.red);
        lbErreur.setFont(new Font("sansserif", Font.BOLD, 14));
        lbErreur.setPreferredSize(new Dimension(200, 60));
        lbErreur.setMinimumSize(new Dimension(200, 30));
        lbErreur.setMaximumSize(new Dimension(2147483647, 2147483647));
        lbErreur.setName("lbErreur");
        pnlParametre.add(lbErreur, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlParametre, BorderLayout.CENTER);
      
      // ======== pnlBarreBouton ========
      {
        pnlBarreBouton.setBorder(null);
        pnlBarreBouton.setMinimumSize(new Dimension(190, 50));
        pnlBarreBouton.setPreferredSize(new Dimension(400, 50));
        pnlBarreBouton.setOpaque(false);
        pnlBarreBouton.setName("pnlBarreBouton");
        pnlBarreBouton.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlBarreBouton.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlBarreBouton.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlBarreBouton.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlBarreBouton.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ---- btValider ----
        btValider.setText("Valider");
        btValider.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btValider.setPreferredSize(new Dimension(140, 50));
        btValider.setMinimumSize(new Dimension(140, 50));
        btValider.setMaximumSize(new Dimension(140, 50));
        btValider.setName("btValider");
        btValider.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            btValiderActionPerformed(e);
          }
        });
        pnlBarreBouton.add(btValider, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- btAnnuler ----
        btAnnuler.setText("Annuler");
        btAnnuler.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btAnnuler.setMaximumSize(new Dimension(140, 50));
        btAnnuler.setMinimumSize(new Dimension(140, 50));
        btAnnuler.setPreferredSize(new Dimension(140, 50));
        btAnnuler.setName("btAnnuler");
        btAnnuler.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            btAnnulerActionPerformed(e);
          }
        });
        pnlBarreBouton.add(btAnnuler, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlParametre;
  private JLabel lbReference;
  private JTextField tfReference;
  private JLabel lbHost;
  private JTextField tfHost;
  private JLabel lbPort;
  private JTextField tfPort;
  private JLabel lbDossierRacine;
  private JTextField tfDossierRacine;
  private JLabel lbErreur;
  private JPanel pnlBarreBouton;
  private JButton btValider;
  private JButton btAnnuler;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
