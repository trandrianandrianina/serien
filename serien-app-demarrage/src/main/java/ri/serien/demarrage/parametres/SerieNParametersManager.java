/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage.parametres;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.serien.demarrage.Constantes;
import ri.serien.demarrage.GestionFichierINI;
import ri.serien.demarrage.GestionFichierTexte;
import ri.serien.libcommun.outils.parametreclient.ParametresClient;

/**
 * Gestion des paramètres pour Série N.
 * Il peut arriver que "user.home" ne retourne pas le bon dossier (vu sur des TSE). Le fichier SerieN.ini va être
 * rechercher dans le dossier d'execution de SerieN.jar
 */
public class SerieNParametersManager {
  // Constantes
  private static final String SERIEN_INI = "SerieN.ini";
  
  // Variables
  private String rootfolder = null;
  private ArrayList<ParametresClient> listeParametresClient = new ArrayList<ParametresClient>();
  private File serienIni = null;
  // Conserve le dernier message d'erreur émit et non lu
  private String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public SerieNParametersManager() {
    this(Constantes.getUserHome() + File.separatorChar + Constantes.DOSSIER_RACINE);
  }
  
  /**
   * Constructeur.
   */
  public SerieNParametersManager(String arootfolder) {
    // On effectue un contôle du dossier racine de l'application (car parfois la valeur retournée par Java est fausse cas TSE)
    rootfolder = arootfolder;
    serienIni = new File(rootfolder + File.separatorChar + Constantes.DOSSIER_WEB + File.separator + SERIEN_INI);
  }
  
  /**
   * Retourne un paramètre de la liste non encore testé ou ok dans le but d'une connexion automatique.
   */
  public ParametresClient next() {
    for (int i = 0; i < listeParametresClient.size(); i++) {
      if ((listeParametresClient.get(i).getStatus() != ParametresClient.NO_OK)) {
        return listeParametresClient.get(i);
      }
    }
    return null;
  }
  
  /**
   * Retourne le choix des paramètres pour l'exécution.
   */
  public ParametresClient getParameters4Execution() {
    return getParameters4Execution(null);
  }
  
  /**
   * Retourne le choix des paramètres pour l'exécution.
   */
  public ParametresClient getParameters4Execution(String sparameters) {
    // Analyse des paramètres potentiels
    if (!analyze(sparameters)) {
      return null;
    }
    
    // Récupération des paramètres qui nous intéressent
    if (listeParametresClient.size() == 0) {
      return null;
    }
    if (listeParametresClient.size() == 1) {
      return listeParametresClient.get(0);
    }
    
    // On balaye la liste possible afin voir si l'un est marqué comme par défaut
    for (int i = 0; i < listeParametresClient.size(); i++) {
      if (listeParametresClient.get(i).isParametresParDefaut()) {
        return listeParametresClient.get(i);
      }
    }
    
    return null;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur(boolean html) {
    String chaine;
    
    // La récupération du message est à usage unique (TODO encoder les caractères accentués)
    if (html) {
      chaine = "<html>" + msgErreur.replaceAll("\n", "<br>") + "</html>";
    }
    else {
      chaine = msgErreur;
    }
    msgErreur = "";
    
    return chaine;
  }
  
  // -- Méthodes privées
  
  /**
   * Analyse et recherche les paramètres pour Série N.
   */
  private boolean analyze(String parameters) {
    boolean retour = false;
    listeParametresClient.clear();
    
    // Il n'y a pas de paramètres passés en ligne de commande
    if (parameters == null || parameters.trim().isEmpty()) {
      // On a un fichier ini
      if (serienIni.exists()) {
        retour = loadIniFile();
        if (retour) {
          return true;
        }
      }
      // Pas de fichier ini ou pas correct
      if (displayParametersWindow()) {
        return writeIniFile();
      }
      else {
        return false;
      }
    }
    else {
      // On a des paramètres en ligne de commande
      listeParametresClient.add(new ParametresClient(rootfolder));
      listeParametresClient.get(0).setParametres(parameters);
      return retour;
    }
  }
  
  /**
   * Charge le fichier SerieN.ini (à partir de DemarrageSerieN).
   */
  private boolean loadIniFile() {
    if (!serienIni.exists()) {
      return false;
    }
    
    // Lecture du fichier
    GestionFichierINI gfini = new GestionFichierINI(serienIni.getAbsolutePath());
    LinkedHashMap<String, LinkedHashMap<String, String>> sections = gfini.getSections();
    if (sections == null || sections.size() == 0) {
      return false;
    }
    
    // Chargement des sections dans la liste des paramètres
    listeParametresClient.clear();
    for (Entry<String, LinkedHashMap<String, String>> entry : sections.entrySet()) {
      ParametresClient parametresClient = new ParametresClient(rootfolder);
      // Les paramètres pour Série N
      parametresClient.setParametres(entry.getValue().get("parameters"));
      parametresClient.setNom(Constantes.code2unicode(entry.getKey()));
      String valeur = entry.getValue().get("default");
      if (valeur != null) {
        parametresClient.setParametresParDefaut(valeur.trim().toLowerCase().equals("true"));
      }
      // Les paramètres pour la JVM
      valeur = entry.getValue().get("jvmparameters");
      if (valeur != null) {
        parametresClient.setJvmParam(valeur);
      }
      listeParametresClient.add(parametresClient);
    }
    return true;
  }
  
  /**
   * Ecriture du fichier ini.
   */
  private boolean writeIniFile() {
    if (listeParametresClient.size() == 0) {
      msgErreur += "Il n'y a pas de paramètres à écrire dans le fichier " + serienIni.getName() + '\n';
      return false;
    }
    
    // Préparation des données à écrire
    ArrayList<String> data = new ArrayList<String>();
    for (int i = 0; i < listeParametresClient.size(); i++) {
      ParametresClient parametresClient = listeParametresClient.get(i);
      data.add(Constantes.lettreori2code("[" + parametresClient.getNom() + "]"));
      if (parametresClient.isParametresParDefaut()) {
        data.add("default=true");
      }
      data.add("parameters=" + parametresClient.getLigneCommande());
      data.add("");
    }
    
    // Ecriture du fichier
    GestionFichierTexte gfini = new GestionFichierTexte(serienIni.getAbsoluteFile());
    gfini.setContenuFichier(data);
    gfini.ecritureFichier();
    data.clear();
    
    return true;
  }
  
  /**
   * Affiche une fenêtre de saisie des paramètres lors du premier lancement.
   */
  private boolean displayParametersWindow() {
    ParametresClient parametresClient = new ParametresClient(rootfolder);
    ParametreSerieN vueParametreSerieN = new ParametreSerieN(null, parametresClient);
    vueParametreSerieN.setVisible(true);
    
    boolean retour = parametresClient.isValide();
    if (!retour) {
      msgErreur = "Aucun paramètre n'a été saisi";
    }
    else {
      listeParametresClient.add(parametresClient);
    }
    vueParametreSerieN.dispose();
    
    return retour;
  }
  
}
