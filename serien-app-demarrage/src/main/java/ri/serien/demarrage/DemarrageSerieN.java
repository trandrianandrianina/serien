/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

import ri.serien.demarrage.parametres.SerieNParametersManager;
import ri.serien.libcommun.installation.FichierBinaire;
import ri.serien.libcommun.installation.InformationsFichierInstallation;
import ri.serien.libcommun.outils.parametreclient.ParametresClient;
import ri.serien.libcommun.rmi.ManagerClientRMI;
import ri.serien.libcommun.rmi.ManagerServiceInstallation;

/**
 * Programme d'amorçage côté desktop.
 * Ce programme permet de mettre à jour le poste client à partir des fichiers du serveur. Et il permet de lancer SerieNDesktop.
 * 
 * Cette version est capable de fonctionner avec les versions antérieure à Maven c'est à dire à lancer le programme "SerieNDesktop.jar"
 * sans version dans le nom.
 */
public class DemarrageSerieN {
  // Constantes
  public static final String VERSION = "3.1";
  
  private static final String THISPROGRAM = "serien-app-demarrage";
  private static final int VERSION_JVM_MINIMUM = 6;
  private static final String SITE_JAVA = "http://www.java.com/fr/download";
  private static final String SERIEN_APP_DEMARRAGE_JAR = THISPROGRAM + ".jar";
  private static final String SERIEN_APP_DEMARRAGE_INI = THISPROGRAM + ".ini";
  private static final String SERIEN_APP_DEMARRAGE_JARNEW = THISPROGRAM + ".jar.new";
  
  private static final String SERIEN_NAMELINK_US = "Serie N";
  private static final String SERIEN_NAMELINK = "S\u00e9rie N";
  private static final String SERIEN_JAR = "serien-app-desktop.jar";
  // Icone sans l'extension car elle dépend du système
  private static final String SERIEN_ICON_ICO = "ri_logo.ico";
  private static final String SERIEN_ICON_PNG = "ri_logo.png";
  private static final String SPLASHSCREEN = "splashscreen.jpg";
  private static final String SERIEN_INI = "SerieN.ini";
  private static final String VERSION_TXT = "version.txt";
  
  // Variables
  private String rootFolder = null;
  private File serienAppDemarrageJar = null;
  private SplashScreen splash = null;
  private SerieNParametersManager parametersManager = null;
  // Paramètres pour Série N en cours
  private ParametresClient parametresClient = null;
  private DemarrageSerieNParameters demarrageparameters = new DemarrageSerieNParameters();
  private File fdossierTmp = null;
  private FileNG thisprogram = null;
  private boolean estUnJar = false;
  private boolean estUneInstallation = false;
  private String dossierExecution = null;
  
  private String dossierWeb = null;
  private String dossierTmp = null;
  private String dossierMaj = null;
  private String cacheTxt = null;
  
  private String paramLink = "";
  
  private LinkedHashMap<String, FileAtDownload> listFileToDownload = new LinkedHashMap<String, FileAtDownload>();
  private HashMap<String, String> cache = new HashMap<String, String>();
  
  // -- Méthodes publiques
  
  /**
   * Initialisation des paramètres et du chemin d'installation.
   */
  public void initialisationParametres(String pParam) {
    determinerNatureProgramme();
    
    pParam = Constantes.normerTexte(pParam);
    if (!pParam.isEmpty()) {
      // Analyse des paramètres passés (ils sont prédominants)
      demarrageparameters.setStringParameters(pParam);
      rootFolder = demarrageparameters.getDossierRacine();
    }
    else {
      // Chargement du fichier serien-app-demarrage.ini s'il existe
      if (loadDemarrageIni()) {
        rootFolder = demarrageparameters.getDossierRacine();
      }
      else {
        rootFolder = demarrageparameters.getDossierRacine();
        // Vérification s'il s'agit d'une nouvelle installation ou pas
        File file = null;
        if (isChoisirDossierRacine()) {
          file = getSelectionDossier(Constantes.getUserHome());
          if (file == null) {
            throw new MessageErreurException("Installation de Série N interrompu par l'utilisateur.");
          }
          rootFolder = file.getAbsolutePath();
        }
        if (rootFolder != null) {
          // Remplacement les espaces par %20 car sur certains PC l'espace pose problème et tronque le chemin
          paramLink += "-dir=" + rootFolder.replaceAll(" ", "%20");
        }
      }
    }
    parametersManager = new SerieNParametersManager(rootFolder);
    
    // Initialisation des traces
    Trace.demarrerLogiciel(rootFolder, "demarrage", "Série N Démarrage");
    Trace.info("Initialiser les paramètres : dossierExecution=" + dossierExecution + " dossieRacine=" + rootFolder + " userhome="
        + Constantes.getUserHome());
  }
  
  /**
   * Traitement du programme.
   */
  public void traitement() {
    // Contrôle de la version de java
    if (!isVersionJavaEstValide()) {
      throw new MessageErreurException(String.format("Série N a besoin de la version %s au minimum pour fonctionner.\n"
          + "Veuillez mettre à jour votre version de Java à partir de ce site %s.", VERSION_JVM_MINIMUM, SITE_JAVA));
    }
    
    // Initialisation
    initialiserDonnees();
    operations();
  }
  
  /**
   * Teste la connection avec le serveur en lançant un ping en boucle sanas fin.
   */
  public void testerPing() {
    // Récupèration des paramètres d'une façon ou d'une autre
    parametresClient = parametersManager.getParameters4Execution();
    if (parametresClient == null) {
      String msg = parametersManager.getMsgErreur(false);
      msg = Constantes.normerTexte(msg);
      if (!msg.isEmpty()) {
        Trace.info(msg);
      }
      return;
    }
    
    // Initialisation RMI
    testerConnexionServeur();
    ManagerClientRMI.configurerServeur(parametresClient.getAdresseServeur(), parametresClient.getPortServeur());
    
    // Boucler sur le ping
    do {
      try {
        // Pause de deux secondes
        Thread.sleep(2000);
        // Ping du serveur sans arrêt de la boucle même si une exception est détectée
        ManagerServiceInstallation.ping();
      }
      catch (Exception e) {
        Trace.erreur(e, "Exception détéctée mais le traitement continue.");
      }
    }
    while (true);
  }
  
  /**
   * Ferme le splashscreen.
   */
  public void fermerSplashscreen() {
    if (splash != null) {
      splash.setVisible(false);
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Controle l'existence du dossier d'installation.
   */
  private boolean isChoisirDossierRacine() {
    // Contrôle l'existence du dossier racine
    File frootFolder = new File(rootFolder);
    // S'il n'existe pas il est créé directement
    if (!frootFolder.exists()) {
      return !frootFolder.mkdirs();
    }
    
    int retour = JOptionPane.showConfirmDialog(null,
        "Une configuration existe déjà.\nSouhaitez-vous installer dans un nouveau dossier pour une nouvelle configuration ?", THISPROGRAM,
        JOptionPane.YES_NO_CANCEL_OPTION);
    
    // Sort directement si l'utilisateur ne clique pas sur oui ou non
    if (retour == JOptionPane.CANCEL_OPTION || retour == JOptionPane.CLOSED_OPTION) {
      System.exit(0);
    }
    return retour == JOptionPane.OK_OPTION;
  }
  
  /**
   * Ouvre une boite de dialogue afin de sélectionner un dossier.
   */
  private File getSelectionDossier(String origine) {
    origine = Constantes.normerTexte(origine);
    File choix = null;
    RiFileChooser selectionDossier = new RiFileChooser(null, origine);
    // Demande de sélectionner un dossier (il doit être différent du dossier d'origine)
    do {
      choix = selectionDossier.choisirDossier(null);
      if (choix == null) {
        return null;
      }
    }
    while (origine.equals(choix.getAbsolutePath()));
    
    return choix;
  }
  
  /**
   * Initialisation des données et de l'environnement.
   */
  private void initialiserDonnees() {
    // Construction des chemins nécessaires
    dossierWeb = rootFolder + File.separator + Constantes.DOSSIER_WEB;
    dossierTmp = rootFolder + File.separator + Constantes.DOSSIER_TMP;
    dossierMaj = dossierWeb + File.separator + Constantes.DOSSIER_MAJ;
    cacheTxt = dossierWeb + File.separator + "cache.txt";
    
    // Création du dossier temporaire si besoin
    fdossierTmp = new File(dossierTmp);
    if (!fdossierTmp.exists() && !fdossierTmp.mkdirs()) {
      throw new MessageErreurException(String.format("Erreur lors de la création du dossier : %s", dossierTmp));
    }
    
    // Création du dossier racine et web
    File fdossierWeb = new File(dossierWeb);
    if (!fdossierWeb.exists() && !fdossierWeb.mkdirs()) {
      throw new MessageErreurException(String.format("Erreur lors de la création du dossier : %s", dossierWeb));
    }
    
    // Création du dossier maj
    File fdossierMaj = new File(dossierMaj);
    if (!fdossierMaj.exists() && !fdossierMaj.mkdirs()) {
      throw new MessageErreurException(String.format("Erreur lors de la création du dossier : %s", dossierMaj));
    }
    
    serienAppDemarrageJar = new File(dossierWeb + File.separator + SERIEN_APP_DEMARRAGE_JAR);
    
    // Est ce une installation ou une simple mise à jour ?
    controlerSiInstallation();
  }
  
  /**
   * Détermine si le programme s'exécute depuis un jar ou sous eclipse.
   */
  private void determinerNatureProgramme() {
    // Teste si l'executable est un jar
    try {
      thisprogram = new FileNG(DemarrageSerieN.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
      dossierExecution = thisprogram.getParent();
      estUnJar = thisprogram.getAbsolutePath().endsWith(".jar");
    }
    catch (Exception e) {
      estUnJar = false;
    }
  }
  
  /**
   * Controle s'il s'agit d'une installation ou d'une mise à jour.
   */
  private void controlerSiInstallation() {
    dossierExecution = Constantes.normerTexte(dossierExecution);
    
    // Contrôle l'existence du dossier racine
    File frootFolder = new File(rootFolder);
    boolean dossierRacinePresent = frootFolder.exists();
    
    // Teste si on s'exécute dans le home du user ou pas
    boolean executionDossierHome =
        dossierExecution.startsWith(Constantes.getUserHome()) && dossierExecution.endsWith(Constantes.DOSSIER_WEB);
    
    // Teste l'existence du SerieN.ini et du liste.txt
    File fichierIni = new File(dossierExecution + File.separator + SERIEN_INI);
    boolean fichierIniExiste = fichierIni.exists();
    
    // Détermine enfin s'il s'agit d'une installation ou pas
    estUneInstallation = false;
    if (!dossierRacinePresent || !executionDossierHome || !fichierIniExiste) {
      estUneInstallation = true;
    }
  }
  
  /**
   * Connexion au serveur Série N.
   */
  private boolean testerConnexionServeur() {
    boolean estConnecte = false;
    while (!estConnecte) {
      try {
        Socket s = new Socket();
        Trace.info("Tester la connexion : host=" + parametresClient.getAdresseServeur() + " port=" + parametresClient.getPortServeur());
        s.connect(new InetSocketAddress(parametresClient.getAdresseServeur(), parametresClient.getPortServeur()), 1000);
        estConnecte = s.isConnected();
        s.close();
      }
      catch (Exception e) {
        if (parametresClient != null) {
          parametresClient.setStatus(ParametresClient.NO_OK);
          parametresClient = parametersManager.next();
          if (parametresClient == null) {
            throw new MessageErreurException(e, "Vos paramètres de connexion contenus dans le fichier serien.ini ne sont pas corrects .");
          }
        }
      }
    }
    
    return estConnecte;
  }
  
  /**
   * Déroulement des opérations.
   */
  private void operations() {
    // Récupèration des paramètres d'une façon ou d'une autre
    parametresClient = parametersManager.getParameters4Execution();
    if (parametresClient == null) {
      String message = parametersManager.getMsgErreur(false);
      message = Constantes.normerTexte(message);
      if (!message.isEmpty()) {
        Trace.info(message);
      }
      return;
    }
    
    // Affichage du splash
    afficherSplashScreen(dossierWeb + File.separator + SPLASHSCREEN, -1);
    
    // Initialisation RMI
    testerConnexionServeur();
    ManagerClientRMI.configurerServeur(parametresClient.getAdresseServeur(), parametresClient.getPortServeur());
    
    // Chargement du fichier de cache contenant les dates (des fichiers du serveur) des fichiers pour le poste de travail
    chargerFichierCache();
    
    // Chargement de la liste des fichiers à télécharger
    if (!chargerFichiersATelecharger()) {
      telechargerDemarrageSerienDepuisLeSupport();
    }
    
    // Contrôle si les fichiers doivent être mis à jour
    if (!controlerFichiersATelecharger()) {
      telechargerDemarrageSerienDepuisLeSupport();
    }
    
    // Télécharge les fichiers si besoin
    if (!telechargerFichiers()) {
      telechargerDemarrageSerienDepuisLeSupport();
    }
    
    // Ecriture du fichier de cache pour les futures utilisations
    ecrireFichierCache();
    
    // Traitement s'il s'agit d'une installation
    if (estUneInstallation) {
      installation();
    }
    
    // Suppression des anciens fichiers jar qui ont été renommé depuis
    File ancienDemarrage = new File(dossierWeb + File.separator + "DemarrageSerieN.jar");
    if (ancienDemarrage.exists()) {
      ancienDemarrage.delete();
    }
    File ancienDesktop = new File(dossierWeb + File.separator + "SerieNDesktop.jar");
    if (ancienDesktop.exists()) {
      ancienDesktop.delete();
    }
    
    // Construction du nom du jar à lancer (avec le numéro de version)
    GestionFichierTexte gft = new GestionFichierTexte(dossierWeb + File.separatorChar + VERSION_TXT);
    String version = Constantes.normerTexte(gft.getContenuFichierString(false));
    gft.dispose();
    String program = SERIEN_JAR;
    if (!version.isEmpty()) {
      program = SERIEN_JAR.replace(".jar", "-" + version + ".jar");
    }
    Trace.info("--> Nom du programme de lancement " + program);
    
    // Contrôle si le lancement du programme est possible
    if (listFileToDownload == null || listFileToDownload.get(program) == null) {
      Trace.alerte(
          "--> Arrêt du programme de démarrage car la liste des fichiers à télécharger est invalide puisque la connexion n'a pas pu être établie.");
      System.exit(-1);
    }
    
    // Lancement du fichier jar
    executeJavaProgram(listFileToDownload.get(program).getNameFileDownloaded());
    
    // Execution d'un script de nettoyage à la fin du traitement
    if (Constantes.isWindows()) {
      lancerScriptWindow(fdossierTmp, thisprogram);
    }
    else if (Constantes.isMac()) {
      lancerScriptMacOs(fdossierTmp, thisprogram);
    }
  }
  
  /**
   * Traitement pour une installation.
   */
  private void installation() {
    // Création du fichier serien-app-demarrage.ini si nécessaire
    if (!paramLink.isEmpty()) {
      GestionFichierINI gfini = new GestionFichierINI(dossierWeb + File.separatorChar + SERIEN_APP_DEMARRAGE_INI);
      gfini.setContenuFichier("[Default]\nparameters=" + paramLink);
      gfini.ecritureFichier();
    }
    
    // Création du raccourci sur le bureau
    if (Constantes.isWindows()) {
      createLink(fdossierTmp, serienAppDemarrageJar, listFileToDownload.get(SERIEN_ICON_ICO).getFileDownloaded());
    }
    else {
      createLink(fdossierTmp, serienAppDemarrageJar, listFileToDownload.get(SERIEN_ICON_PNG).getFileDownloaded());
    }
  }
  
  /**
   * Vérifie si la version installée sur le poste est obsolète par rapport à la version du serveur.
   */
  private boolean isVersionInstalleeObsolete() {
    // Récupération du fichier version.txt sur le serveur, il est stocké dans le dossier tmp côté Desktop
    try {
      // Récupération des informations du fichier version.txt côté serveur
      FileAtDownload fichierVersion = new FileAtDownload(VERSION_TXT, dossierWeb + File.separator + VERSION_TXT);
      InformationsFichierInstallation infosFichier =
          ManagerServiceInstallation.retournerInformationsFichier(fichierVersion.getNameFileAtDownload());
      if (infosFichier.getNom().isEmpty()) {
        fichierVersion.setExistOnServer(false);
      }
      else {
        fichierVersion.setNameFileAtDownload(infosFichier.getNom());
        fichierVersion.setExistOnServer(true);
      }
      fichierVersion.setLength(infosFichier.getTaille());
      fichierVersion.setLastModified(infosFichier.getDerniereModification());
      fichierVersion.setOkForDownload(true);
      
      // Téléchargement du fichier version.txt et stockage dans le dossier tmp
      FichierBinaire fichierBinaire = ManagerServiceInstallation.demanderFichier(fichierVersion.getNameFileAtDownload());
      fichierBinaire.changeNomFichier(dossierTmp + File.separatorChar + VERSION_TXT);
      fichierBinaire.ecritureFichier();
    }
    catch (Exception e) {
      // Si une exception est déclenchée, les jar ne seront pas supprimés car il s'agit certainement d'une version avant Maven
      return false;
    }
    
    // Lecture du fichier version.txt téléchargé
    GestionFichierTexte gft = new GestionFichierTexte(dossierTmp + File.separatorChar + VERSION_TXT);
    String versionServeur = Constantes.normerTexte(gft.getContenuFichierString(false));
    gft.dispose();
    // Si le contennu est vide alors c'est que le fichier version.txt n'est pas présent sur le serveur donc il s'agit d'une version
    // pré-Maven
    if (versionServeur.isEmpty()) {
      return false;
    }
    
    // Lecture du fichier version.txt en local
    gft = new GestionFichierTexte(dossierWeb + File.separatorChar + VERSION_TXT);
    String versionDesktop = Constantes.normerTexte(gft.getContenuFichierString(false));
    gft.dispose();
    
    // Le fichier version.txt n'a pas été trouvé en local (il a peut-être été effacé manuellement)
    // Le numéro de version va être extrait du nom du SerieNDesktop
    if (versionDesktop.isEmpty()) {
      // Recherche de tous les fichiers jar commençant par SerieNDesktop
      File fdossierWeb = new File(dossierWeb);
      File[] listeDesktop = fdossierWeb.listFiles(new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
          if (name.startsWith("serien-app-desktop") && name.toLowerCase().endsWith(".jar")) {
            return true;
          }
          return false;
        }
      });
      // Aucun fichier SerieNDesktop trouvé, ce n'est pas normal en théorie (ou première installation maven) donc effacement de tous les
      // jars
      if (listeDesktop == null || listeDesktop.length == 0) {
        return true;
      }
      // Plusieurs fichiers SerieNDesktop trouvés, ce n'est pas normal en théorie donc effacement de tous les jars
      if (listeDesktop.length > 1) {
        return true;
      }
      // Un seul fichier SerieNDesktop trouvé
      if (listeDesktop.length == 1) {
        // Récupération du numéro de la version dans le nom du fichier
        String nomFichier = listeDesktop[0].getName();
        int posd = nomFichier.indexOf('-');
        int posf = nomFichier.lastIndexOf('.');
        if (posd != -1 && posf != -1) {
          versionDesktop = nomFichier.substring(posd + 1, posf);
        }
      }
    }
    
    // Comparaison des versions
    if (versionDesktop.equals(versionServeur)) {
      return false;
    }
    return true;
  }
  
  /**
   * Supprime tous les fichiers jars des dossiers www et lib ainsi que le fichier version.txt.
   */
  private void supprimerTousLesJars() {
    // Suppression du fichier version.txt dans le dossier www
    File fichierVersion = new File(dossierWeb + File.separatorChar + VERSION_TXT);
    fichierVersion.delete();
    
    // Recherche des fichiers jar dans le dossier www
    File fdossierWeb = new File(dossierWeb);
    File[] listeFichierJar = fdossierWeb.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        if (!name.equals(SERIEN_APP_DEMARRAGE_JAR) && name.toLowerCase().endsWith(".jar")) {
          return true;
        }
        return false;
      }
    });
    // Suppression des fichiers jar dans le dossier www à l'exception du programme serien-app-demarrage.jar
    if (listeFichierJar != null) {
      for (File fichier : listeFichierJar) {
        fichier.delete();
      }
    }
    // Recherche des fichiers jar dans le dossier www/lib
    File fdossierLib = new File(dossierWeb + File.separatorChar + Constantes.DOSSIER_LIB);
    listeFichierJar = fdossierLib.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        if (name.toLowerCase().endsWith(".jar")) {
          return true;
        }
        return false;
      }
    });
    // Suppression des fichiers jar dans le dossier www/lib
    if (listeFichierJar != null) {
      for (File fichier : listeFichierJar) {
        fichier.delete();
      }
    }
  }
  
  /**
   * Charge la liste des fichiers (dossier www sur le serveur) à télécharger sur le poste.
   * S'il y a des fichiers obsolètes à supprimer cela se passe dans la classe Application pour le Desktop et NettoyerServeur pour le
   * serveur.
   */
  private boolean chargerFichiersATelecharger() {
    listFileToDownload.clear();
    
    // Listage du dossier www sur le serveur
    List<String> listeFichierSurServeur = null;
    try {
      listeFichierSurServeur = ManagerServiceInstallation.listerDossierServeur(Constantes.DOSSIER_WEB);
    }
    catch (Exception e) {
      // Si une exception se déclenche le téléchargement du programme depuis notre site d'assistance est proposé
      return false;
    }
    if (listeFichierSurServeur == null || listeFichierSurServeur.isEmpty()) {
      throw new MessageErreurException("Le dossier " + Constantes.DOSSIER_WEB + " sur le serveur ne contient aucun fichier.");
    }
    
    // Contrôle de la version installé sur le poste vs la version sur le serveur
    if (!estUneInstallation && isVersionInstalleeObsolete()) {
      supprimerTousLesJars();
    }
    
    // Parcourt de la liste des fichiers présent sur le serveur
    for (String fichier : listeFichierSurServeur) {
      String nomfichier = Constantes.normerTexte(fichier);
      // Cas particuliers: en mode mise à jour et le fichier est le serien-app-demarrage.jar
      if (!estUneInstallation && nomfichier.equals(SERIEN_APP_DEMARRAGE_JAR)) {
        listFileToDownload.put(SERIEN_APP_DEMARRAGE_JARNEW, new FileAtDownload(SERIEN_APP_DEMARRAGE_JAR,
            dossierWeb + File.separator + SERIEN_APP_DEMARRAGE_JARNEW, demarrageparameters.getCompare()));
        listFileToDownload.get(SERIEN_APP_DEMARRAGE_JARNEW).setTestExistOnClient(false);
      }
      else {
        listFileToDownload.put(nomfichier, new FileAtDownload(nomfichier,
            dossierWeb + File.separator + nomfichier.replace('/', File.separatorChar), demarrageparameters.getCompare()));
      }
    }
    return true;
  }
  
  /**
   * Contrôle avec le serveur s'il est nécessaire de télécharger les fichiers et récupération de leurs attributs.
   */
  private boolean controlerFichiersATelecharger() {
    FileAtDownload.totalBytes = 0;
    for (Entry<String, FileAtDownload> entry : listFileToDownload.entrySet()) {
      FileAtDownload fileatdownload = entry.getValue();
      InformationsFichierInstallation infosFichier = null;
      try {
        infosFichier = ManagerServiceInstallation.retournerInformationsFichier(fileatdownload.getNameFileAtDownload());
      }
      catch (Exception e) {
        // Si une exception se déclenche le téléchargement du programme depuis notre site d'assistance est proposé
        return false;
      }
      
      if (infosFichier.getNom().isEmpty()) {
        fileatdownload.setExistOnServer(false);
      }
      else {
        fileatdownload.setNameFileAtDownload(infosFichier.getNom());
        fileatdownload.setExistOnServer(true);
      }
      fileatdownload.setLength(infosFichier.getTaille());
      fileatdownload.setLastModified(infosFichier.getDerniereModification());
      fileatdownload.setOkForDownload(true);
      if (fileatdownload.isOkForDownload() && (cache != null)) {
        fileatdownload.setOkForDownload(fileatdownload.isOkForDownload(cache.get(fileatdownload.getNameFileDownloaded())));
      }
    }
    
    return true;
  }
  
  /**
   * Liste le contenu d'un dossier.
   * @param pDossierAScanner
   * @return
   */
  private ArrayList<File> retournerlisteFichier(String pDossierAScanner) {
    pDossierAScanner = Constantes.normerTexte(pDossierAScanner);
    if (pDossierAScanner.isEmpty()) {
      return null;
    }
    
    File dossier = new File(pDossierAScanner);
    if (!dossier.exists()) {
      return null;
    }
    ArrayList<File> listeFichier = new ArrayList<File>();
    FileNG.listeFichier(dossier, listeFichier);
    
    /*Trace.alerte("Liste fichier de " + pDossierAScanner);
    for (File fichier : listeFichier) {
      Trace.alerte(fichier.getAbsolutePath());
    }*/
    
    return listeFichier;
  }
  
  /**
   * Retourne l'objet File du dossier www\maj associé à un nom de fichier s'il se trouve dans la liste.
   */
  private FileNG retournerFichierMaj(String pNomFichier, ArrayList<File> pListeFichier) {
    pNomFichier = Constantes.normerTexte(pNomFichier);
    if (pNomFichier.isEmpty()) {
      return null;
    }
    if (pListeFichier == null || pListeFichier.isEmpty()) {
      return null;
    }
    
    for (File fichier : pListeFichier) {
      if (pNomFichier.equals(fichier.getName())) {
        return new FileNG(fichier);
      }
    }
    
    return null;
  }
  
  /**
   * Téléchargement des fichiers nécessaires au lancement.
   */
  private boolean telechargerFichiers() {
    int nbrfile = 0;
    boolean telechargementEncours = false;
    
    // Listage du contenu du dossier www\maj
    ArrayList<File> listeFichier = retournerlisteFichier(dossierMaj);
    
    // Parcourt de la liste des fichiers à télécharger
    for (Entry<String, FileAtDownload> entry : listFileToDownload.entrySet()) {
      FileAtDownload fichierATelecharger = entry.getValue();
      // Téléchargement uniquement de ceux qui sont nécessaires
      if (fichierATelecharger.isOkForDownload()) {
        if (!telechargementEncours) {
          if (splash != null) {
            splash.setProgressMax(listFileToDownload.size() - 1);
          }
          telechargementEncours = true;
        }
        
        // Copie du fichier depuis le dossier www\maj du poste de travail s'il est présent
        boolean fichierCopieDepuisDossierMaj = false;
        if (listeFichier != null && !listeFichier.isEmpty()) {
          File fichierAMettreAJour = new File(fichierATelecharger.getNameFileDownloaded());
          FileNG fichierdossierMaj = retournerFichierMaj(fichierAMettreAJour.getName(), listeFichier);
          // Le fichier à mettre à jour a été trouvé dans le dossier www\maj
          if (fichierdossierMaj != null) {
            try {
              Trace.info("--> Copier : " + fichierdossierMaj.getAbsolutePath() + " dans " + fichierAMettreAJour.getAbsolutePath());
              if (fichierdossierMaj.copyTo(fichierAMettreAJour)) {
                Trace.info("Fin de copie");
                fichierdossierMaj.delete();
                Trace.alerte("Fin de suppression de " + fichierdossierMaj.getAbsolutePath());
                fichierCopieDepuisDossierMaj = true;
              }
              else {
                fichierCopieDepuisDossierMaj = false;
              }
            }
            catch (Exception e) {
              fichierCopieDepuisDossierMaj = false;
            }
          }
        }
        Trace.info("Téléchargement de " + fichierATelecharger.getNameFileAtDownload() + " " + !fichierCopieDepuisDossierMaj);
        
        // Téléchargement depuis le serveur s'il n'a pas été trouvé dans le dossier www\maj
        if (!fichierCopieDepuisDossierMaj) {
          FichierBinaire fichierBinaire = null;
          try {
            fichierBinaire = ManagerServiceInstallation.demanderFichier(fichierATelecharger.getNameFileAtDownload());
          }
          catch (Exception e) {
            return false;
          }
          fichierBinaire.changeNomFichier(fichierATelecharger.getNameFileDownloaded());
          fichierBinaire.ecritureFichier();
        }
        
        // Mise à jour du fichier cache
        cache.put(fichierATelecharger.getNameFileDownloaded(), fichierATelecharger.getDateLastModified());
        fichierATelecharger.setDownloadEnded(true);
        Trace.info("Fichier " + nbrfile + " " + fichierATelecharger.getNameFileAtDownload());
      }
      
      nbrfile++;
      if (telechargementEncours && splash != null) {
        splash.setProgress(nbrfile);
      }
    }
    return true;
  }
  
  /**
   * Démarre le splash screen.
   */
  private void afficherSplashScreen(final String pNomLogo, final int pNombreMax) {
    Runnable code = new Runnable() {
      @Override
      public void run() {
        if (splash != null) {
          splash.setProgressMax(pNombreMax);
          splash.setScreenVisible(true);
          return;
        }
        
        // Affichage du splash screen
        ImageIcon imageLogo = null;
        File logo = new File(pNomLogo);
        if (logo.exists()) {
          imageLogo = new ImageIcon(pNomLogo);
        }
        else {
          imageLogo = new ImageIcon(getClass().getResource("/images/" + SPLASHSCREEN));
        }
        splash = new SplashScreen("Version du lanceur : " + VERSION, imageLogo);
        splash.setProgressMax(pNombreMax);
        splash.setScreenVisible(true);
      }
    };
    if (SwingUtilities.isEventDispatchThread()) {
      code.run();
    }
    else {
      SwingUtilities.invokeLater(code);
    }
  }
  
  /**
   * Charge le fichier serien-app-demarrage s'il existe.
   */
  private boolean loadDemarrageIni() {
    File demarrageIni = new File(System.getProperty("user.dir") + File.separatorChar + SERIEN_APP_DEMARRAGE_INI);
    
    // Renommage de l'ancien fichier ini suite au renommage du jar
    File demarrageOldIni = new File(System.getProperty("user.dir") + File.separatorChar + "DemarrageSerieN.ini");
    if (demarrageOldIni.exists()) {
      demarrageOldIni.renameTo(demarrageIni);
    }
    
    // Contrôle de présence du fichier ini
    if (!demarrageIni.exists() && !controlerDossierRacine()) {
      // Contrôle quand même que l'exécution est dans un dossier correct mais où il ne manque juste que le fichier ini
      return false;
    }
    
    // Lecture du fichier
    GestionFichierINI gfini = new GestionFichierINI(demarrageIni.getAbsolutePath());
    LinkedHashMap<String, LinkedHashMap<String, String>> sections = gfini.getSections();
    if ((sections == null) || (sections.size() == 0)) {
      return true;
    }
    
    // Chargement des sections dans la liste des paramètres
    for (Entry<String, LinkedHashMap<String, String>> entry : sections.entrySet()) {
      demarrageparameters.setStringParameters(entry.getValue().get("parameters"));
    }
    return true;
  }
  
  /**
   * Contrôle si le programme s'execute dans le bon dossier ou s'il s'agit une installation.
   */
  private boolean controlerDossierRacine() {
    if ((dossierExecution == null) || dossierExecution.isEmpty()) {
      return false;
    }
    
    // Si le dossier d'execution ne contient pas www alors nous ne sommes pas dans un dossier "racine" il s'agit d'une installation
    if (!dossierExecution.endsWith(Constantes.DOSSIER_WEB)) {
      return false;
    }
    
    File fDossierExecution = new File(dossierExecution);
    paramLink = "-dir=" + fDossierExecution.getParent();
    String parametre = "[Default]\nparameters=" + paramLink;
    // Exécution à priori dans un dossier racine mais le fichier ini n'existe pas
    GestionFichierTexte gfini = new GestionFichierTexte(dossierExecution + File.separatorChar + SERIEN_APP_DEMARRAGE_INI);
    gfini.setContenuFichier(parametre);
    gfini.ecritureFichier();
    gfini.dispose();
    return true;
  }
  
  /**
   * Chargement du fichier de cache.
   */
  private void chargerFichierCache() {
    GestionFichierTexte gfcache = new GestionFichierTexte(cacheTxt);
    if (gfcache.lectureFichier() == Constantes.ERREUR) {
      return;
    }
    
    cache.clear();
    // Charge les informations du cache dans la hashmap
    ArrayList<String> contenu = gfcache.getContenuFichier();
    for (int i = 0; i < contenu.size(); i++) {
      String chaine = contenu.get(i).trim();
      if (chaine.length() == 0) {
        continue;
      }
      String[] data = chaine.split("=");
      if (data.length == 2) {
        cache.put(data[0], data[1]);
      }
      else {
        cache.put(data[0], "");
      }
    }
  }
  
  /**
   * Ecrit le fichier de cache.
   */
  private void ecrireFichierCache() {
    if ((cache == null) || (cache.size() == 0)) {
      return;
    }
    
    GestionFichierTexte gfcache = new GestionFichierTexte(cacheTxt);
    
    // Mise en forme les informations du cache
    String[] contenu = new String[cache.size()];
    int i = 0;
    for (Entry<String, String> entry : cache.entrySet()) {
      contenu[i++] = entry.getKey().trim() + '=' + entry.getValue().trim();
    }
    gfcache.setContenuFichier(contenu);
    gfcache.ecritureFichier();
  }
  
  /**
   * Contrôler la version du JRE pour l'exécution de Série N.
   * 
   * Avant Java 9, le format des version de JRE était 1.8.2. Le premier nombre était toujours un 1 tandis que le second était le numéro
   * de version majeure.
   * 
   * Le format de version du JRE a été modifié à partir de Java 9. Le nouveau format suit les standards dans le domaine avec une version
   * en 3 parties séparées par un point (par exemple 9.3.2). Une version longue avec le numéro de build et la disponibilité est
   * également disponible (9.3.2-beta).
   * - Le premier nombre est le numéro majeur, qui correspond au numéro de version Java utilisé dans le language courant.
   * - Le second nombre est le numéro mineur pour les corrections de bugs non critiques ou ajouts de petites fonctionnalités.
   * - Le troisième nombre est le numéro de sécurité pour les corrections de bugs critiques.
   * Noter que les zéros terminaux sont optionnels, ainsi 9.0.0 peut être retourné au format 9.
   */
  private static boolean isVersionJavaEstValide() {
    try {
      // Récupérer la version du JRE
      String version = System.getProperty("java.version");
      Trace.info("Version JRE = " + version);
      
      // Lire les deux premiers numéros de la version
      String[] listeNumero = version.split("\\.");
      int numero1 = Integer.parseInt(listeNumero[0]);
      int numero2 = 0;
      if (listeNumero.length >= 2) {
        numero2 = Integer.parseInt(listeNumero[1]);
      }
      
      // Récupérer le numéro de version majeure
      int versionMajeure = 0;
      if (numero1 == 1) {
        versionMajeure = numero2;
      }
      else {
        versionMajeure = numero1;
      }
      
      // Vérifier si la version minimum est respectée
      return versionMajeure >= VERSION_JVM_MINIMUM;
    }
    catch (Exception e) {
      Trace.erreur(
          "Erreur lors du contrôle du numéro de version du JRE. Le contrôle retourne ok par défaut pour ne pas bloquer le démarrage de Série N inutilement.");
      return true;
    }
  }
  
  /**
   * Lancement d'un programme.
   */
  private void executeJavaProgram(String program) {
    try {
      // Préparation de l'environnement d'exécution
      int nbrparam = 3;
      String javabin = System.getProperty("java.home") + File.separatorChar + "bin" + File.separatorChar;
      String jexe = javabin + "javaw";
      if (Constantes.isMac()) {
        jexe = javabin + "java";
      }
      String[] jvmparam = null;
      if (parametresClient.getJvmParam() != null) {
        jvmparam = parametresClient.getJvmParam().split(" ");
        nbrparam += jvmparam.length;
      }
      
      final String[] cmd = new String[nbrparam];
      cmd[0] = jexe;
      int i = 0;
      if (jvmparam != null) {
        for (i = 0; i < jvmparam.length; i++) {
          cmd[i + 1] = jvmparam[i];
        }
      }
      i++;
      cmd[i++] = program;
      cmd[i++] = parametresClient != null ? " " + parametresClient.getLigneCommande() : "";
      
      Process process = Runtime.getRuntime().exec(cmd);
      AfficheurFlux fluxSortie = new AfficheurFlux(process.getInputStream());
      AfficheurFlux fluxErreur = new AfficheurFlux(process.getErrorStream());
      new Thread(fluxSortie).start();
      new Thread(fluxErreur).start();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution du programme.");
    }
  }
  
  /**
   * Création du raccourci en fonction du système d'exploitation.
   */
  private void createLink(File dossierScript, File fichierjar, File fichiericon) {
    // Test sur le fichier jar
    if ((fichierjar == null) || (!fichierjar.exists())) {
      throw new MessageErreurException("Erreur le fichier jar est null ou ne se trouve pas à l'endroit spécifié");
    }
    if ((dossierScript == null) || (!dossierScript.exists())) {
      throw new MessageErreurException("Erreur le dossier devant contenir le script est null ou n'existe pas");
    }
    
    // Windows
    if (Constantes.isWindows()) {
      createWindowsLink(dossierScript, fichierjar, fichiericon);
    }
    
    // Mac
    if (Constantes.isMac()) {
      createMacOsXLink(dossierScript, fichierjar, fichiericon);
    }
  }
  
  /**
   * Création du script de nettoyage à la fin de l'execution de ce programme.
   */
  private boolean lancerScriptWindow(File dossierScript, File fichierjarinstall) {
    boolean execute = false;
    
    // Construction du script
    String script = "WScript.sleep 1000\n" + "Dim fichier\n" + "Set fichier = CreateObject(\"Scripting.FileSystemObject\")\n";
    // Ajout de la partie concernant la suppression du fichier jar d'installation
    if (estUneInstallation) {
      if (fichierjarinstall != null) {
        // Vérification qu'il ne s'agit pas de l'execution du serien-app-demarrage.jar de l'IFS
        if (parametresClient != null && fichierjarinstall.getParent().indexOf("\\" + parametresClient.getAdresseServeur()) == -1) {
          script += "fichier.DeleteFile \"" + fichierjarinstall.getAbsolutePath() + "\", True\n";
          execute = true;
        }
      }
    }
    else {
      File newdemarrejar = listFileToDownload.get(SERIEN_APP_DEMARRAGE_JARNEW).getFileDownloaded();
      // Ajout de la partie concernant la renommage de fichier de serien-app-demarrage
      if ((newdemarrejar != null) && (newdemarrejar.exists())) {
        int pos = newdemarrejar.getAbsolutePath().lastIndexOf(".new");
        String demarrejar = newdemarrejar.getAbsolutePath().substring(0, pos);
        // Suppression du fichier
        script += "fichier.DeleteFile \"" + demarrejar + "\", True\n";
        // Renommage du fichier
        script += "fichier.MoveFile \"" + newdemarrejar.getAbsolutePath() + "\", \"" + demarrejar + "\"\n";
        execute = true;
      }
    }
    if (!execute) {
      return true;
    }
    
    // Ajout de la suppression du script lui-même
    File fichierscript = new File(dossierScript.getAbsolutePath() + File.separatorChar + "endscript.vbs");
    script += "fichier.DeleteFile \"" + fichierscript.getAbsolutePath() + "\", True\n";
    
    // Génération du fichier script
    if (fichierscript.exists()) {
      // Contrôle que le script n'est pas été créé aujourd'hui avant de le péter
      SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yy");
      if (formater.format(new Date()).equals(formater.format(fichierscript.lastModified()))) {
        return true; // Sort direct si le script existe car sinon le même script est lancé 2 fois
      }
      else {
        fichierscript.delete();
      }
    }
    
    GestionFichierTexte gft = new GestionFichierTexte(fichierscript);
    gft.setContenuFichier(script);
    gft.ecritureFichier();
    
    // Si l'executable n'est pas un fichier jar, le script n'est pas execute (car execution depuis Eclipse)
    if (!estUnJar) {
      return true;
    }
    
    // Exécution du script
    String[] cmd = { "wscript.exe", fichierscript.getAbsolutePath() };
    try {
      Runtime.getRuntime().exec(cmd, null);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution du fichier script.");
    }
    
    return true;
  }
  
  /**
   * Création du script de nettoyage à la fin de l'execution de ce programme.
   */
  private boolean lancerScriptMacOs(File dossierScript, File fichierjarinstall) {
    boolean execute = false;
    
    // Construction du script
    String script = "#!/bin/bash\n" + "sleep 1\n";
    if (estUneInstallation) {
      // Ajout de la partie concernant la suppression du fichier jar d'installation
      if (fichierjarinstall != null) {
        script += "rm \"" + fichierjarinstall.getAbsolutePath() + "\"\n";
        execute = true;
      }
    }
    else {
      File newdemarrejar = listFileToDownload.get(SERIEN_APP_DEMARRAGE_JARNEW).getFileDownloaded();
      // Ajout de la partie concernant la renommage de fichier de serien-app-demarrage
      if ((newdemarrejar != null) && (newdemarrejar.exists())) {
        int pos = newdemarrejar.getAbsolutePath().lastIndexOf(".new");
        String demarrejar = newdemarrejar.getAbsolutePath().substring(0, pos);
        // Suppression du fichier
        script += "rm \"" + demarrejar + "\"\n";
        // Renommage le fichier
        script += "mv \"" + newdemarrejar.getAbsolutePath() + "\" \"" + demarrejar + "\"\n";
        execute = true;
      }
    }
    if (!execute) {
      return true;
    }
    
    // Ajout de la suppression du script lui-même
    File fichierscript = new File(dossierScript.getAbsolutePath() + File.separatorChar + "endscript.sh");
    script += "rm \"" + fichierscript + "\"\n";
    
    // Génération du fichier script
    if (fichierscript.exists()) {
      // Contrôle que le script n'est pas été créé aujourd'hui avant de le péter
      SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yy");
      if (formater.format(new Date()).equals(formater.format(fichierscript.lastModified()))) {
        return true; // Sort direct si le script existe car sinon le même script est lancé 2 fois
      }
      else {
        fichierscript.delete();
      }
    }
    
    GestionFichierTexte gft = new GestionFichierTexte(fichierscript);
    gft.setContenuFichier(script);
    gft.ecritureFichier();
    fichierscript.setExecutable(true);
    
    // Si l'executable n'est pas un fichier jar, le script n'est pas executé (car execution depuis Eclipse)
    if (!estUnJar) {
      return true;
    }
    
    // Exécution du script
    String[] cmd = { "bash", fichierscript.getAbsolutePath() };
    try {
      Runtime.getRuntime().exec(cmd, null);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution du fichier script.");
    }
    
    return true;
  }
  
  /**
   * Controle l'existence d'un fichier dans le dossier Web.
   */
  private boolean aliasExist(String alinkname) {
    File link = new File(getDesktopPath() + File.separatorChar + alinkname);
    return link.exists();
  }
  
  /**
   * Retourne le chemin du bureau.
   */
  private String getDesktopPath() {
    if (Constantes.isWindows()) {
      // Windows XP
      String path = Constantes.getUserHome() + File.separatorChar + "Bureau";
      if (new File(path).exists()) {
        return path;
      }
      else {
        return Constantes.getUserHome() + File.separatorChar + "Desktop";
      }
    }
    if (Constantes.isMac()) {
      return Constantes.getUserHome() + File.separatorChar + "Desktop";
    }
    
    return "";
  }
  
  /**
   * Création du raccourci du lanceur sur le bureau Windows.
   */
  private boolean createWindowsLink(File dossierScript, File fichierjar, File fichiericon) {
    String icon = null;
    String fichierscript = dossierScript.getAbsolutePath() + File.separatorChar + "crtlnk.vbs";
    
    // Test sur l'icone
    if ((fichiericon != null) && (fichiericon.exists())) {
      icon = fichiericon.getAbsolutePath();
    }
    
    String linkname = SERIEN_NAMELINK;
    while (aliasExist(linkname + ".lnk")) {
      linkname += "_New";
    }
    
    // Construction du script
    final String script = "Dim Shell, DesktopPath, Lnk\n" + "Set Shell = CreateObject(\"WScript.Shell\")\n"
        + "DesktopPath = Shell.SpecialFolders(\"Desktop\")\n" + "Set Lnk = Shell.CreateShortcut(DesktopPath & \"\\" + linkname
        + ".lnk\")\n" + "Lnk.TargetPath = \"" + fichierjar.getAbsolutePath() + "\"\n"
        // + (paramLink.equals("")?"":"Lnk.Arguments = " + paramLink + "\n")
        + ((icon != null) ? "Lnk.IconLocation = \"" + icon + "\"\n" : "") + "Lnk.WorkingDirectory = \"" + fichierjar.getParent() + "\"\n"
        + "Lnk.Save";
        
    // Génération du fichier script
    File ffichierscript = new File(fichierscript);
    if (ffichierscript.exists()) {
      ffichierscript.delete();
    }
    GestionFichierTexte gft = new GestionFichierTexte(ffichierscript);
    gft.setContenuFichier(script);
    gft.ecritureFichier();
    
    // Exécution du script
    try {
      Process p = new ProcessBuilder("wscript.exe", fichierscript).start();
      p.waitFor();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution du fichier script.");
    }
    
    return true;
  }
  
  /**
   * Création du raccourci du lanceur sur le bureau Max Os X.
   */
  private boolean createMacOsXLink(File dossierScript, File fichierjar, File fichiericon) {
    String icon = null;
    String fichierscript = dossierScript.getAbsolutePath() + File.separatorChar + "crtlnk.scpt";
    
    // Test sur l'icone
    if ((fichiericon != null) && (fichiericon.exists())) {
      icon = fichiericon.getAbsolutePath().replaceAll(File.separator, ":");
    }
    if (icon == null) {
      return false;
    }
    
    // Construction du script (test sur le file.encoding en attendant qu'ils corrigent le bug de la JVM)
    String linkname = (System.getProperty("file.encoding").equalsIgnoreCase("us-ascii") ? SERIEN_NAMELINK_US : SERIEN_NAMELINK);
    while (aliasExist(linkname)) {
      linkname += "_New";
    }
    
    final String script =
        "on run\n" + "\tset aliasname to \"" + linkname + "\"\n" + "\tset icon_image_file to file \"" + icon.substring(1)
            + "\" as alias\n" + "\tset jarfile to file \"" + fichierjar.getAbsolutePath().substring(1).replaceAll(File.separator, ":")
            + "\" as alias\n" + "\tset aliasfolder to file \"" + Constantes.getUserHome().substring(1).replaceAll(File.separator, ":")
            + ":Desktop\" as alias\n\n" + "\tmy createAlias(jarfile, aliasfolder, aliasname)\n" + "\tset aliasfile to alias \""
            + Constantes.getUserHome().substring(1).replaceAll(File.separator, ":") + ":Desktop:" + linkname + "\"\n\n"
            + "\tmy saveImageWithItselfAsIcon(icon_image_file)\n" + "\tmy copyIconOfTo(icon_image_file, aliasfile)\n" + "end run\n\n"
            + "on createAlias(origin_file, alias_folder, alias_name)\n" + "\ttell application \"Finder\"\n"
            + "\t\tmake new alias file at alias_folder to origin_file\n" + "\t\tset name of result to alias_name\n" + "\tend tell\n"
            + "end createAlias\n\n" + "on saveImageWithItselfAsIcon(icon_image_file)\n"
            + "\tset icon_image_file_string to icon_image_file as string\n" + "\ttell application \"Image Events\"\n" + "\t\tlaunch\n"
            + "\t\tset icon_image to open file icon_image_file_string\n" + "\t\tsave icon_image with icon\n" + "\t\tclose icon_image\n"
            + "\tend tell\n" + "end saveImageWithItselfAsIcon\n\n" + "on copyIconOfTo(aFileOrFolderWithIcon, aFileOrFolder)\n"
            + "\ttell application \"Finder\" to set f to aFileOrFolderWithIcon as alias\n" + "\tmy CopyAndPaste(f, \"c\")\n"
            + "\ttell application \"Finder\" to set c to aFileOrFolder as alias\n" + "\tmy CopyAndPaste(result, \"v\")\n"
            + "end copyIconOfTo\n\n" + "on CopyAndPaste(i, cv)\n" + "\ttell application \"Finder\"\n" + "\t\tactivate\n"
            + "\t\topen information window of i\n" + "\tend tell\n"
            + "\ttell application \"System Events\" to tell process \"Finder\" to tell window 1\n" + "\t\tkeystroke tab\n"
            + "\t\tkeystroke (cv & \"w\") using command down\n" + "\tend tell\n" + "end CopyAndPaste";
    
    // Génération du fichier script
    File ffichierscript = new File(fichierscript);
    if (ffichierscript.exists()) {
      ffichierscript.delete();
    }
    GestionFichierTexte gft = new GestionFichierTexte(ffichierscript);
    gft.setContenuFichier(script);
    gft.ecritureFichier();
    
    // Exécution du script
    try {
      Process p = new ProcessBuilder("osascript", fichierscript).start();
      p.waitFor();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution du fichier script.");
    }
    
    return true;
  }
  
  /**
   * Télécharge le fichier serien-app-demarrage.jar depuis le site du support.
   */
  private void telechargerDemarrageSerienDepuisLeSupport() {
    // Affichage d'un message pour demander la confirmation à l'utilisateur
    int reponse = JOptionPane.showConfirmDialog(null,
        "<html>L'application n'a pas pu dialoguer avec le serveur Série N.<br>"
            + "Il y a certainement eu une mise à jour de Série N sur votre AS400 qui provoque cette incompatibilité<br><br>"
            + "Répondez <b>OUI</b> pour télécharger le programme de mise à jour directement depuis notre site.<br>"
            + "Si vous ne savez pas, réponder NON et demander à votre responsable.",
        THISPROGRAM, JOptionPane.YES_NO_OPTION);
    if (reponse == JOptionPane.NO_OPTION) {
      return;
    }
    
    // Téléchargement du serien-app-demarrage.jar depuis le site www.serien-support.com/serien dans le dossier
    // tmp/serien-app-demarrage.new
    // Génération d'un script qui va :
    // - renommer le serien-app-demarrage.jar du dossier www en serien-app-demarrage.old
    // - copier le fichier tmp/serien-app-demarrage.new dans www/serien-app-demarrage.jar
    // - afficher un message à l'utilisateur pour l'avertir qu'il peut relancer en cliquant sur le raccourci
    try {
      telechargerDemarrageSerieNNew(dossierTmp);
      if (Constantes.isWindows()) {
        creerScriptWindowsMajNew();
      }
      else if (Constantes.isMac()) {
        creerScriptMacosMajNew();
      }
    }
    catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), THISPROGRAM, JOptionPane.ERROR_MESSAGE);
    }
    System.exit(0);
  }
  
  /**
   * Télécharge le fichier serien-app-demarrage.jar depuis le site d'assistance.
   */
  private void telechargerDemarrageSerieNNew(String pDossierTelechargement) throws Exception {
    if (pDossierTelechargement == null) {
      pDossierTelechargement = "";
    }
    pDossierTelechargement = pDossierTelechargement.trim();
    if (!pDossierTelechargement.endsWith(File.separator)) {
      pDossierTelechargement = pDossierTelechargement + File.separatorChar;
    }
    try {
      URL url = new URL("http://www.seriem-support.com/serien/" + SERIEN_APP_DEMARRAGE_JAR);
      ReadableByteChannel rbc = Channels.newChannel(url.openStream());
      FileOutputStream fos = new FileOutputStream(pDossierTelechargement + SERIEN_APP_DEMARRAGE_JAR);
      fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
      fos.flush();
      fos.close();
    }
    catch (Exception e) {
      throw new MessageErreurException(
          "Le fichier " + SERIEN_APP_DEMARRAGE_JAR + " n'a pas pu être téléchargé depuis le site www.seriem-support.com/serien");
    }
  }
  
  /**
   * Création du script de mise à jour pour la nouvelle version à la fin de l'execution de ce programme.
   */
  private void creerScriptWindowsMajNew() throws Exception {
    File fichierscript = new File(dossierTmp + File.separatorChar + "majnewscript.vbs");
    if (fichierscript.exists()) {
      fichierscript.delete();
    }
    File demarrageOld = new File(dossierWeb + File.separatorChar + SERIEN_APP_DEMARRAGE_JAR);
    if (!demarrageOld.exists()) {
      throw new MessageErreurException("Le fichier " + demarrageOld.getAbsolutePath() + " n'a pas été trouvé.");
    }
    File demarrageNew = new File(dossierTmp + File.separatorChar + SERIEN_APP_DEMARRAGE_JAR);
    if (!demarrageNew.exists()) {
      throw new MessageErreurException("Le fichier " + demarrageNew.getAbsolutePath() + " n'a pas été trouvé.");
    }
    
    // Construction du script
    String script = "WScript.sleep 2000\n" + "Dim fichier\n" + "Set fichier = CreateObject(\"Scripting.FileSystemObject\")\n";
    // Ajoute la partie concernant la renommage de fichier de www\serien-appa-demarrage.jar
    // Renommage du fichier de l'ancienne version
    script += "fichier.MoveFile \"" + demarrageOld.getAbsolutePath() + "\", \"" + demarrageOld.getAbsolutePath() + ".old\"\n";
    // Copie du fichier qui se trouve dans tmp dans www
    script += "fichier.MoveFile \"" + demarrageNew.getAbsolutePath() + "\", \"" + demarrageOld.getAbsolutePath() + "\"\n";
    // Ajoute la suppression du script lui-même
    script += "WScript.sleep 2000\n";
    script += "fichier.DeleteFile \"" + fichierscript.getAbsolutePath() + "\", True\n";
    script += "WScript.echo \"La mise à jour est terminée sur votre poste. Veuillez cliquer à nouveau sur votre raccourci.\"";
    
    // Chargement du fichier script
    GestionFichierTexte gft = new GestionFichierTexte(fichierscript);
    gft.setContenuFichier(script);
    gft.ecritureFichier();
    
    // Exécution du script
    String[] cmd = { "wscript.exe", fichierscript.getAbsolutePath() };
    try {
      Runtime.getRuntime().exec(cmd, null);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution du fichier script.");
    }
  }
  
  /**
   * Création du script de mise à jour pour la nouvelle version à la fin de l'execution de ce programme.
   */
  private void creerScriptMacosMajNew() throws Exception {
    File fichierscript = new File(dossierTmp + File.separatorChar + "majnewscript.sh");
    if (fichierscript.exists()) {
      fichierscript.delete();
    }
    File demarrageOld = new File(dossierWeb + File.separatorChar + SERIEN_APP_DEMARRAGE_JAR);
    if (!demarrageOld.exists()) {
      throw new MessageErreurException("Le fichier " + demarrageOld.getAbsolutePath() + " n'a pas été trouvé.");
    }
    File demarrageNew = new File(dossierTmp + File.separatorChar + SERIEN_APP_DEMARRAGE_JAR);
    if (!demarrageNew.exists()) {
      throw new MessageErreurException("Le fichier " + demarrageNew.getAbsolutePath() + " n'a pas été trouvé.");
    }
    
    // Construction du script
    String script = "#!/bin/bash\n" + "sleep 2\n";
    // Ajoute la partie concernant la renommage de fichier de www\serien-app-demarrage.jar
    // Renommage du fichier de l'ancienne version
    script += "mv \"" + demarrageOld.getAbsolutePath() + "\" \"" + demarrageOld.getAbsolutePath() + ".old\"\n";
    // Copie du fichier qui se trouve dans tmp dans www
    script += "mv \"" + demarrageNew.getAbsolutePath() + "\" \"" + demarrageOld.getAbsolutePath() + "\"\n";
    // Ajoute la suppression du script lui-même
    script += "sleep 2\n";
    script += "rm \"" + fichierscript.getAbsolutePath() + "\"\n";
    script += "echo \"La mise à jour est terminée sur votre poste. Veuillez cliquer à nouveau sur votre raccourci.\"";
    
    // Chargement du fichier script
    GestionFichierTexte gft = new GestionFichierTexte(fichierscript);
    gft.setContenuFichier(script);
    gft.ecritureFichier();
    fichierscript.setExecutable(true);
    
    // Exécution du script
    String[] cmd = { "bash", fichierscript.getAbsolutePath() };
    try {
      Runtime.getRuntime().exec(cmd, null);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution du fichier script.");
    }
  }
  
  /**
   * Programme principal.
   */
  public static void main(String[] args) {
    final StringBuffer parametres = new StringBuffer();
    boolean testPingActif = false;
    
    // Force le look and feel Nimbus
    Constantes.forcerLookAndFeelNimbus();
    // Change la couleur par défaut de la barre de progression (orange->bleu)
    UIDefaults defaults = UIManager.getLookAndFeelDefaults();
    defaults.put("nimbusOrange", defaults.get("nimbusInfoBlue"));
    
    // Concatènation des arguments trouvés dans une chaine
    for (int i = 0; i < args.length; i++) {
      parametres.append(args[i].trim()).append(' ');
    }
    // Controle si le paramètre ping est dans les arguments
    if (parametres.toString().toLowerCase().indexOf("-ping") != -1) {
      testPingActif = true;
    }
    
    // Démarrage du programme
    DemarrageSerieN demarrage = new DemarrageSerieN();
    try {
      demarrage.initialisationParametres(parametres.toString());
      // Si le mode test Ping est activé, les autres traitements sont ignorés
      if (testPingActif) {
        demarrage.testerPing();
        System.exit(0);
      }
      // Fonctionnement normal du programme
      demarrage.traitement();
    }
    catch (Exception e) {
      if (e instanceof java.rmi.UnknownHostException) {
        String msg = e.getMessage();
        int pos = msg.lastIndexOf(':');
        if (pos != -1) {
          msg =
              String.format("Vous devez ajouter l'enregistrement concernant l'hôte %s dans votre fichier hosts.", msg.substring(pos + 1));
          if (Constantes.isWindows()) {
            msg += "\nLe fichier se trouve habituellement dans C:\\Windows\\System32\\drivers\\etc.";
          }
          else {
            msg += "\nLe fichier se trouve habituellement dans /etc.";
          }
        }
        MessageUtilisateur.afficher(msg);
        return;
      }
      MessageUtilisateur.afficher(e);
    }
    
    demarrage.fermerSplashscreen();
    System.exit(0);
  }
}
