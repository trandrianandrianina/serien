/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.io.File;

/**
 * Ensemble des paramètres pour DemarrageSerie N.
 * Le fichier DemarrageSerieN.ini va être rechercher dans le dossier d'exécution de DemarrageSerieN.jar.
 */
public class DemarrageSerieNParameters {
  // Constantes
  public static final String DOSSIERRACINE = Constantes.getUserHome() + File.separator + Constantes.DOSSIER_RACINE;
  
  private static final String OLD_ONSERVER = "OLD_ONSERVER";
  private static final String DIFFERENT_ONSERVER = "DIFFERENT_ONSERVER";
  private static final String RECENT_ONSERVER = "RECENT_ONSERVER";
  
  // Variables
  private String dossierRacine = DOSSIERRACINE;
  private int compare = FileAtDownload.CMP_MOST_RECENT_ONSERVER;
  
  // Conserve le dernier message d'erreur émit et non lu
  private String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public DemarrageSerieNParameters() {
  }
  
  /**
   * Contrôle la validité des paramètres.
   */
  public boolean isValid() {
    if ((dossierRacine == null) || (dossierRacine.trim().equals(""))) {
      msgErreur += "Erreur le dossier racine est incorrect " + dossierRacine + '\n';
      return false;
    }
    if ((compare < -1) || (compare > 1)) {
      msgErreur += "Erreur le critère de comparaison est incorrect " + compare + '\n';
      return false;
    }
    
    return true;
  }
  
  /**
   * Récupère les paramètres en analysant la chaine.
   */
  public boolean setStringParameters(String parameters) {
    if (parameters == null) {
      return false;
    }
    
    String[] listeparametres = null;
    StringBuffer param = new StringBuffer(parameters.trim());
    String chaine = null;
    
    // On arrange la chaine pour récupérer les paramètres correctement
    int pos = param.indexOf(" ");
    while (pos != -1) {
      if (param.charAt(pos + 1) != '-') {
        param.replace(pos, pos + 1, "%20");
      }
      pos = param.indexOf(" ", pos + 1);
    }
    
    // On découpe la chaine et on stocke les différents paramètres dans un tableau
    listeparametres = Constantes.splitString(param.toString(), ' ');
    
    // On parcourt la liste afin d'en extraire les infos
    for (int i = 0; i < listeparametres.length; i++) {
      chaine = listeparametres[i].replaceAll("%20", " ").trim();
      
      // On regarde le critère de comparaison afin de voir dans quel cas on fait la mise à jour du SerieN.ini
      if (chaine.toLowerCase().startsWith("-cmp=")) {
        if (chaine.substring(5).trim().equalsIgnoreCase(OLD_ONSERVER)) {
          compare = FileAtDownload.CMP_MOST_OLD_ONSERVER;
        }
        else if (chaine.substring(5).trim().equalsIgnoreCase(DIFFERENT_ONSERVER)) {
          compare = FileAtDownload.CMP_DIFFERENT_ONSERVER;
        }
        else if (chaine.substring(5).trim().equalsIgnoreCase(RECENT_ONSERVER)) {
          compare = FileAtDownload.CMP_MOST_RECENT_ONSERVER;
        }
      }
      else if (chaine.toLowerCase().startsWith("-dir=")) {
        // On recherche le dossier utilisateur
        dossierRacine = chaine.substring(5);
      }
    }
    
    return isValid();
  }
  
  /**
   * Retourne une chaine avec les paramètres.
   */
  public String getStringParameters() {
    if (!isValid()) {
      return null;
    }
    
    StringBuffer parameters = new StringBuffer();
    parameters.append("-cmp=").append(getStringCompare()).append(' ');
    if (!dossierRacine.equals(DOSSIERRACINE)) {
      parameters.append("-dir=").append(dossierRacine).append(' ');
    }
    
    return parameters.toString();
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur(boolean html) {
    String chaine;
    
    // La récupération du message est à usage unique
    if (html) {
      chaine = "<html>" + msgErreur.replaceAll("\n", "<br>") + "</html>";
    }
    else {
      chaine = msgErreur;
    }
    msgErreur = "";
    
    return chaine;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  // -- Accesseurs ----------------------------------------------------------
  /**
   * @return le dossierRacine.
   */
  public String getDossierRacine() {
    return dossierRacine;
  }
  
  /**
   * @param dossierRacine le dossierRacine à définir.
   */
  public void setDossierRacine(String dossierRacine) {
    if ((dossierRacine != null) && (!dossierRacine.equals(""))) {
      this.dossierRacine = dossierRacine;
    }
  }
  
  /**
   * @return le compare.
   */
  public int getCompare() {
    return compare;
  }
  
  /**
   * @return le compare.
   */
  public String getStringCompare() {
    switch (compare) {
      case FileAtDownload.CMP_MOST_OLD_ONSERVER:
        return OLD_ONSERVER;
      case FileAtDownload.CMP_DIFFERENT_ONSERVER:
        return DIFFERENT_ONSERVER;
      case FileAtDownload.CMP_MOST_RECENT_ONSERVER:
        return RECENT_ONSERVER;
      default:
        break;
    }
    return "";
  }
  
  /**
   * @param compare le compare à définir.
   */
  public void setCompare(int compare) {
    this.compare = compare;
  }
}
