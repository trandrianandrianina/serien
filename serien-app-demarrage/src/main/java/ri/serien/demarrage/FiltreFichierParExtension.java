/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Description de l'objet Filtre poour les boites de dialogue.
 */
public class FiltreFichierParExtension extends FileFilter {
  private String[] extensions = null;
  private String description = null;
  
  /**
   * Constructeur.
   */
  public FiltreFichierParExtension(String[] ext, String lib) {
    extensions = ext;
    description = lib;
  }
  
  /**
   * Filtre.
   */
  @Override
  public boolean accept(File f) {
    int i = 0;
    
    if (f.isDirectory()) {
      return true;
    }
    
    String extension = getExtension(f);
    if ((extension != null) && (extensions != null)) {
      for (i = 0; i < extensions.length; i++) {
        if (extension.equals(extensions[i])) {
          return true;
        }
      }
    }
    return false;
  }
  
  /**
   * Retourne le libellé du filtre choisie.
   */
  @Override
  public String getDescription() {
    return description;
  }
  
  /**
   * Retourne l'extension du fichier.
   */
  public String getExtension(File f) {
    String ext = null;
    String s = f.getName();
    int i = s.lastIndexOf('.');
    
    if (i > 0 && i < s.length() - 1) {
      ext = s.substring(i + 1).toLowerCase();
    }
    return ext;
  }
  
  /**
   * Retourne l'extension du filtre.
   */
  public String getFiltreExtension() {
    return extensions[0];
  }
  
}
