/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

/**
 * Boîte de dialogue permettant d'afficher des messages à l'utilisateur.
 */
public class MessageUtilisateur extends JDialog {
  private final String MESSAGE_PAR_DEFAUT = "Une erreur technique est survenue, merci de contacter le service support.";
  private String messageUtilisateur = null;
  private Exception exception = null;
  
  /**
   * Constructeur privé pour empêcher l'instanciation directe. Il faut utiliser la méthode afficher() pour utiliser cette classe.
   */
  private MessageUtilisateur(String pmessage, Exception pexception) {
    super();
    messageUtilisateur = pmessage;
    exception = pexception;
    
    // Tracer le message
    if (exception != null) {
      Trace.erreur(exception, messageUtilisateur);
    }
    else {
      Trace.info(messageUtilisateur);
    }
    
    // Initialiser les composantes graphiques
    initialiserComposants();
  }
  
  /**
   * Afficher la boîte de dialogue avec un message.
   */
  public static MessageUtilisateur afficher(String aMessage) {
    return new MessageUtilisateur(aMessage, null);
  }
  
  /**
   * Afficher la boîte de dialogue avec un message issu d'une exception.
   */
  public static MessageUtilisateur afficher(Exception e) {
    MessageErreurException messageUtilisateurException = null;
    
    // Parcourir la pile d'exceptions pour y rechercher un MessageUtilisateurException
    for (Throwable cause = e; cause != null; cause = cause.getCause()) {
      if (cause instanceof MessageErreurException) {
        messageUtilisateurException = (MessageErreurException) cause;
        break;
      }
    }
    
    // Tester si c'est un MessageUtilisateurException (avec message inclu)
    if (messageUtilisateurException != null) {
      return new MessageUtilisateur(messageUtilisateurException.getLocalizedMessage(), messageUtilisateurException);
    }
    else {
      return new MessageUtilisateur(null, e);
    }
  }
  
  /**
   * Initialiser les composants graphiques de la boîte de dialogue.
   */
  private void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();
    scrollPaneException.setVisible(false);
    lbAvertissement.setVisible(false);
    // C'est la couleur des fons de panel
    setBackground(new Color(239, 239, 222));
    
    // Afficher le message utilisateur
    taMessageUtilisateur.setText(formaterMessageUtilisateur());
    
    // Afficher les informations de l'exception
    if (exception != null) {
      boutonDetail.setVisible(true);
      taMessageException.setText(formaterException());
      taMessageException.setCaretPosition(0);
    }
    
    // Gestion du clavier
    InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    ActionMap actionMap = getRootPane().getActionMap();
    // Touche entree : on sort
    inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "fermeture");
    actionMap.put("fermeture", new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        setVisible(false);
        dispose();
      }
    });
    // Touche entree : on enregistre et on sort
    inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "valide");
    actionMap.put("valide", new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        setVisible(false);
        dispose();
      }
    });
    
    // Afficher la boîte de dialogue
    setVisible(true);
    repaint();
  }
  
  /**
   * Formater le message utilisateur pour l'afficher en HTML.
   */
  private String formaterMessageUtilisateur() {
    // Afficher le message par défaut s'il n'y a pas de message utilisateur
    String str;
    if (messageUtilisateur == null || messageUtilisateur.trim().isEmpty()) {
      str = MESSAGE_PAR_DEFAUT;
    }
    else {
      str = messageUtilisateur;
    }
    
    // Formater le message en HTML
    str = str.replaceAll("<", "&lt;");
    str = str.replaceAll(">", "&gt;");
    str = str.replaceAll("\\n", "<br>");
    str = "<html><span>" + str + "</span></html>";
    return str;
  }
  
  /**
   * Formater une exception pour l'afficher sous forme d'un texte lisible par un technicien.
   */
  private String formaterException() {
    try {
      StringBuilder sb = new StringBuilder();
      if (exception == null) {
        return sb.toString();
      }
      
      // Afficher un résumé des messages en début de message
      for (Throwable cause = exception; cause != null; cause = cause.getCause()) {
        sb.append(cause.getMessage() + "\n");
      }
      sb.append("\n");
      
      // Afficher les messages empilés dans les exceptions
      for (Throwable cause = exception; cause != null; cause = cause.getCause()) {
        sb.append(cause.getClass().getSimpleName() + " : " + cause.getMessage() + "\n");
        for (StackTraceElement element : cause.getStackTrace()) {
          sb.append(element.toString());
          sb.append("\n");
        }
        sb.append("\n");
      }
      return sb.toString();
    }
    catch (Exception e) {
      // Sécurité pour afficher la boîte de dialogue y compris en cas d'erreur lors du traitement de l'exception
      return "";
    }
  }
  
  private void bt_FermerActionPerformed(ActionEvent e) {
    setVisible(false);
    dispose();
  }
  
  private void boutonDetailMouseClicked(MouseEvent e) {
    scrollPaneException.setVisible(!scrollPaneException.isVisible());
    lbAvertissement.setVisible(!lbAvertissement.isVisible());
    taMessageUtilisateur.setVisible(!taMessageUtilisateur.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_contenu = new JPanel();
    taMessageUtilisateur = new JLabel();
    scrollPaneException = new JScrollPane();
    taMessageException = new JTextPane();
    lbAvertissement = new JLabel();
    p_Controle = new JPanel();
    boutonDetail = new JLabel();
    bt_Fermer = new JButton();
    
    // ======== this ========
    setTitle("Message");
    setBackground(new Color(238, 238, 210));
    setResizable(false);
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(null);
    
    // ======== p_contenu ========
    {
      p_contenu.setBackground(new Color(238, 238, 210));
      p_contenu.setName("p_contenu");
      p_contenu.setLayout(null);
      
      // ---- taMessageUtilisateur ----
      taMessageUtilisateur.setFont(taMessageUtilisateur.getFont().deriveFont(taMessageUtilisateur.getFont().getStyle() | Font.BOLD,
          taMessageUtilisateur.getFont().getSize() + 3f));
      taMessageUtilisateur.setBackground(new Color(238, 238, 210));
      taMessageUtilisateur.setBorder(new EmptyBorder(10, 10, 10, 10));
      taMessageUtilisateur.setAutoscrolls(false);
      taMessageUtilisateur.setHorizontalAlignment(SwingConstants.CENTER);
      taMessageUtilisateur.setName("taMessageUtilisateur");
      p_contenu.add(taMessageUtilisateur);
      taMessageUtilisateur.setBounds(15, 28, 580, 145);
      
      // ======== scrollPaneException ========
      {
        scrollPaneException.setName("scrollPaneException");
        
        // ---- taMessageException ----
        taMessageException.setBackground(new Color(238, 238, 210));
        taMessageException.setEditable(false);
        taMessageException.setOpaque(false);
        taMessageException.setName("taMessageException");
        scrollPaneException.setViewportView(taMessageException);
      }
      p_contenu.add(scrollPaneException);
      scrollPaneException.setBounds(5, 15, 600, 185);
      
      // ---- lbAvertissement ----
      lbAvertissement.setText("Ces informations sont destin\u00e9es au service technique de R\u00e9solution Informatique");
      lbAvertissement.setFont(lbAvertissement.getFont().deriveFont(Font.BOLD | Font.ITALIC));
      lbAvertissement.setName("lbAvertissement");
      p_contenu.add(lbAvertissement);
      lbAvertissement.setBounds(10, 2, 512, 15);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < p_contenu.getComponentCount(); i++) {
          Rectangle bounds = p_contenu.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_contenu.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_contenu.setMinimumSize(preferredSize);
        p_contenu.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(p_contenu);
    p_contenu.setBounds(0, 0, 610, 205);
    
    // ======== p_Controle ========
    {
      p_Controle.setPreferredSize(new Dimension(296, 65));
      p_Controle.setBackground(new Color(238, 238, 210));
      p_Controle.setName("p_Controle");
      p_Controle.setLayout(null);
      
      // ---- boutonDetail ----
      boutonDetail.setFont(
          boutonDetail.getFont().deriveFont(boutonDetail.getFont().getStyle() | Font.BOLD, boutonDetail.getFont().getSize() - 1f));
      boutonDetail.setBackground(new Color(238, 238, 210));
      boutonDetail.setIcon(new ImageIcon(getClass().getResource("/images/engrenage.png")));
      boutonDetail.setToolTipText("D\u00e9tails du message d'erreur");
      boutonDetail.setVisible(false);
      boutonDetail.setName("boutonDetail");
      boutonDetail.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          boutonDetailMouseClicked(e);
        }
      });
      p_Controle.add(boutonDetail);
      boutonDetail.setBounds(5, 25, 30, 30);
      
      // ---- bt_Fermer ----
      bt_Fermer.setFont(bt_Fermer.getFont().deriveFont(bt_Fermer.getFont().getStyle() | Font.BOLD, bt_Fermer.getFont().getSize() + 3f));
      bt_Fermer.setBackground(new Color(177, 121, 116));
      bt_Fermer.setText("Fermer");
      bt_Fermer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Fermer.setName("bt_Fermer");
      bt_Fermer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_FermerActionPerformed(e);
        }
      });
      p_Controle.add(bt_Fermer);
      bt_Fermer.setBounds(465, 5, 136, 50);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < p_Controle.getComponentCount(); i++) {
          Rectangle bounds = p_Controle.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_Controle.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_Controle.setMinimumSize(preferredSize);
        p_Controle.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(p_Controle);
    p_Controle.setBounds(0, 205, 610, 60);
    
    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for (int i = 0; i < contentPane.getComponentCount(); i++) {
        Rectangle bounds = contentPane.getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = contentPane.getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      contentPane.setMinimumSize(preferredSize);
      contentPane.setPreferredSize(preferredSize);
    }
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_contenu;
  private JLabel taMessageUtilisateur;
  private JScrollPane scrollPaneException;
  private JTextPane taMessageException;
  private JLabel lbAvertissement;
  private JPanel p_Controle;
  private JLabel boutonDetail;
  private JButton bt_Fermer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
