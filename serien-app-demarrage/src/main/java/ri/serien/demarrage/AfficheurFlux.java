/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Programme qui installe, paramètre et lance le SerieN.jar
 * Permet de rediriger les flux vers la console pour le programme lancé
 */
class AfficheurFlux implements Runnable {
  private final InputStream inputStream;
  
  AfficheurFlux(InputStream inputStream) {
    this.inputStream = inputStream;
  }
  
  private BufferedReader getBufferedReader(InputStream is) {
    return new BufferedReader(new InputStreamReader(is));
  }
  
  @Override
  public void run() {
    BufferedReader br = getBufferedReader(inputStream);
    String ligne = "";
    try {
      while ((ligne = br.readLine()) != null) {
        Trace.debug(AfficheurFlux.class, "run", ligne);
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
}
