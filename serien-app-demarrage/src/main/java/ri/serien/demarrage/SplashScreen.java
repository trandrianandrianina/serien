/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;

/**
 * Affiche un écran avec barre de progression si nécessaire.
 */
public class SplashScreen extends JWindow {
  // Variables
  private JProgressBar progressBar = new JProgressBar();
  
  /**
   * Cosntructeur.
   */
  public SplashScreen(String amessage, ImageIcon aimage) {
    try {
      init(amessage, aimage);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
  /**
   * Initialise la fenêtre.
   */
  private void init(String amessage, ImageIcon aimage) throws Exception {
    getRootPane().setBorder(BorderFactory.createLineBorder(Color.black));
    getContentPane().setLayout(new BorderLayout());
    getContentPane().setBackground(Color.WHITE);
    
    // Le texte
    if ((amessage != null) && (!amessage.trim().equals(""))) {
      JLabel lText = new JLabel();
      lText.setText(amessage);
      lText.setOpaque(false);
      getContentPane().add(lText, BorderLayout.NORTH);
    }
    // L'image
    if (aimage != null) {
      JLabel lImage = new JLabel();
      lImage.setIcon(aimage);
      getContentPane().add(lImage, BorderLayout.CENTER);
    }
    // La progressBar
    getContentPane().add(progressBar, BorderLayout.SOUTH);
    progressBar.setOpaque(false);
    
    pack();
    setLocationRelativeTo(getOwner());
  }
  
  /**
   * Configure la taille max de la progress bar.
   */
  public void setProgressMax(int maxProgress) {
    if (maxProgress < 0) {
      progressBar.setVisible(false);
    }
    else {
      progressBar.setValue(0);
      progressBar.setMaximum(maxProgress);
      progressBar.setVisible(true);
    }
  }
  
  /**
   * Définir la progression.
   */
  public void setProgress(int progress) {
    final int theProgress = progress;
    SwingUtilities.invokeLater(new Runnable() {
      // @Override
      @Override
      public void run() {
        progressBar.setValue(theProgress);
      }
    });
  }
  
  /**
   * Définir la progression avec un message informatif.
   */
  public void setProgress(String textinprogress, int progress) {
    final int theProgress = progress;
    final String theMessage = textinprogress;
    setProgress(progress);
    SwingUtilities.invokeLater(new Runnable() {
      // @Override
      @Override
      public void run() {
        progressBar.setValue(theProgress);
        setMessage(theMessage);
      }
    });
  }
  
  /**
   * Afficher la boîte de dialogue.
   */
  public void setScreenVisible(boolean b) {
    final boolean boo = b;
    SwingUtilities.invokeLater(new Runnable() {
      // @Override
      @Override
      public void run() {
        setVisible(boo);
      }
    });
  }
  
  /**
   * Définir le message.
   */
  private void setMessage(String textinprogress) {
    if (textinprogress == null) {
      textinprogress = "";
      progressBar.setStringPainted(false);
    }
    else {
      progressBar.setStringPainted(true);
    }
    progressBar.setString(textinprogress);
  }
  
}
