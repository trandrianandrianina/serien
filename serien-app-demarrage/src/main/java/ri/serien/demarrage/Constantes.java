/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.awt.Color;
import java.awt.Font;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

/**
 * Liste des constantes génériques utilisées dans le projet.
 * @todo : Récupérer le séparateur décimal & millier de manière dynamique pour la localisation.
 */
public class Constantes {
  // Génériques
  public static final int ON = 1;
  public static final int OFF = 0;
  public static final int OK = 0;
  public static final int ERREUR = -1;
  public static final int TRUE = 0;
  public static final int FALSE = -1;
  public static final int TRAITEE = -2;
  public static final int EN_COURS_TRAITEMENT = -3;
  public static final int BOTH = 3;
  public static final int HORIZONTAL = 2;
  public static final int VERTICAL = 1;
  public static final int NONE = -1;
  public static final int LEFT = 0;
  public static final int CENTER = 1;
  public static final int RIGHT = 2;
  public static final int TOP = 3;
  public static final int BOTTOM = 4;
  public static final int TAILLE_BUFFER = 32768;
  public static final int DATAQLONG = 5120;
  // Délai attente sur dataqueue en seconde
  public static final int DELAI_ATTENTE_DATAQ_PROGRAMME = 15 * 60;
  public static final int DELAI_ATTENTE_DATAQ_MENU = -1;
  public static final int MAXVAR = 1000;
  public static final int DEBUG = TRUE;
  public static final String SEPARATEUR = "\t";
  public static final String SEPARATEUR_CHAINE = "|";
  public static final String SEPARATEUR_DOSSIER = "&";
  public static final char SEPARATEUR_CHAR = '\t';
  public static final char SEPARATEUR_CHAINE_CHAR = '|';
  public static final char SEPARATEUR_DOSSIER_CHAR = '&';
  public static final String COD_CRLF = "#CRLF#";
  public static final String crlf = System.getProperty("line.separator");
  public static final char[] SAUTLIGNE_CHAR = crlf.toCharArray();
  public static final String SAUTPAGE = "µ&&";
  public static final char[] SAUTPAGE_CHAR = SAUTPAGE.toCharArray();
  
  public static final char SEPARATEUR_DECIMAL_CHAR = ',';
  public static final String SEPARATEUR_DECIMAL = ",";
  public static final char SEPARATEUR_MILLIER_CHAR = '.';
  public static final String SEPARATEUR_MILLIER = ".";
  public static final int IMG_SYS = 0;
  public static final int IMG_EQU = 1;
  public static final int IMG_VAR = 2;
  public static final int PLAIN = 0;
  public static final int BOLD = 1;
  public static final int ITALIC = 2;
  public static final String THUMBAIL = "p_";
  public static final char SEPARATEUR_DATE_CHAR = '/';
  
  public static final char TYPE_ERR_SIMPLE = ' ';
  public static final char TYPE_WRNG_SIMPLE = 'W';
  
  // Numéros des requêtes
  public static final int STOP = 0;
  public static final int CONNEXION_OK = 1;
  public static final int DEMANDE_OUVERTURE_PANEL_SESSION = 2;
  public static final int DEMANDE_LISTE_RT = 3;
  public static final int ENVOI_LISTE_RT = 4;
  public static final int DEMANDE_FICHIER_RT = 5;
  public static final int ENVOI_FICHIER_RT = 6;
  public static final int RECOIT_FICHIER_RT = ENVOI_FICHIER_RT;
  public static final int ENVOI_BUFFER_GFX = 7;
  public static final int RECOIT_BUFFER_GFX = ENVOI_BUFFER_GFX;
  public static final int ENVOI_BUFFER_RPG = 8;
  public static final int RECOIT_BUFFER_RPG = ENVOI_BUFFER_RPG;
  public static final int DEMANDE_LISTE_RTCONFIG = 9;
  public static final int ENVOI_LISTE_RTCONFIG = 10;
  public static final int OUVERTURE_SESSION_OK = 11;
  public static final int DEMANDE_DEMARRE_SESSION = 12;
  public static final int DEMANDE_OUVERTURE_TRANSFERT_SESSION = 19;
  public static final int FORCE_AFFICHE_PANEL = 20;
  public static final int TRF_LIGNE_FIC_CSV = 21;
  public static final int ENVOI_ENREGISTREMENT_MENU = 22;
  public static final int RECOIT_ENREGISTREMENT_MENU = ENVOI_ENREGISTREMENT_MENU;
  public static final int RECOIT_CHOIX_MENU = 23;
  public static final int ENVOI_CHOIX_MENU = RECOIT_CHOIX_MENU;
  public static final int DEMANDE_STOP_SESSION = 24;
  public static final int RECOIT_INFOS_USER = 25;
  public static final int ENVOI_INFOS_USER = RECOIT_INFOS_USER;
  public static final int DEMANDE_OUVERTURE_MIRE_SESSION = 26;
  public static final int DEMANDE_FICHIER = 27;
  public static final int ENVOI_FICHIER = 28;
  public static final int RECOIT_FICHIER = ENVOI_FICHIER;
  public static final int DEMANDE_CONTENU_DOSSIER = 29;
  public static final int ENVOI_CONTENU_DOSSIER = 30;
  public static final int RECOIT_CONTENU_DOSSIER = ENVOI_CONTENU_DOSSIER;
  public static final int DEMANDE_PROTOCOLE_SERVEUR = 31;
  public static final int RECOIT_PROTOCOLE_SERVEUR = DEMANDE_PROTOCOLE_SERVEUR;
  public static final int ENVOI_DSPF = 32;
  public static final int RECOIT_DSPF = ENVOI_DSPF;
  public static final int ENVOI_REQUETE_EDITION = 33;
  public static final int RECOIT_REQUETE_EDITION = ENVOI_REQUETE_EDITION;
  public static final int PING = 34; // Permet de tester la validité d'une connexion de la part du serveur
  public static final int PONG = PING;
  public static final int UPLOAD_FICHIER = 35;
  public static final int SUPPRIME_FICHIER = 36;
  public static final int ENVOI_REQUETE_SYSTEME = 37;
  public static final int RECOIT_REQUETE_SYSTEME = ENVOI_REQUETE_SYSTEME;
  public static final int NOTIFICATION_FROM_RPG = 38;
  
  public static final int ANALYSE_DSPF = 1000000;
  public static final int HTTP_GET = 1195725856;
  public static final int HTTP_POST = 1347375956;
  public static final int HTTP_HEAD = 1212498244;
  
  // Valeur 100 en BigDecimal
  public static final BigDecimal VALEUR_CENT = BigDecimal.valueOf(100).setScale(0, RoundingMode.HALF_UP);
  public static final int DEUX_DECIMALES = 2;
  
  // Code message DTAQ
  public static final int LONGUEUR_CODE_RPGMSG = 5;
  public static final String RPGMSG_BUFFER = "00001";
  public static final String RPGMSG_BUFFER_FRC = "00002";
  public static final String RPGMSG_ERR = "00003";
  public static final String RPGMSG_CSV_TITLE = "10001";
  public static final String RPGMSG_CSV_DETAIL = "10002";
  public static final String RPGMSG_CSV_END = "10003";
  public static final String RPGMSG_MNU_GRP = "20001";
  public static final String RPGMSG_MNU_GRP_END = "20011";
  public static final String RPGMSG_MNU_MOD = "20002";
  public static final String RPGMSG_MNU_MOD_END = "20012";
  public static final String RPGMSG_MNU_REFRESH = "20013";
  public static final String RPGMSG_USR_INFOS = "20000";
  public static final String RPGMSG_NOTIFICATION = "30000";
  public static final String RPGMSG_DELAI_ATTENTE_EXPIRE = "99997";
  public static final String RPGMSG_JOBENDED = "99998";
  public static final String RPGMSG_ENDJOB = "99999";
  
  // Codes notification
  public static final int NOTIF_DLFILE = 1;
  
  // Propres au projet
  public static final String MAILTO = "assistance@resolution-informatique.com";
  // en ms = 1H si =0 alors ne coupe jamais
  public static final int SOCKET_TIMEOUT = 0;
  public static final int DATAQ_TIMEOUT = 3600000;
  public static final int MAXRECONNEXION = 1000;
  
  public static final String VERSION_PROTOCOLE = "01.02";
  public static final String VERSION_SERVER = VERSION_PROTOCOLE + ".02";
  public static final String VERSION_CLIENT = VERSION_PROTOCOLE + ".02";
  public static final int SERVER_PORT = 8888;
  public static final String INIT_CONFIG = "config.equ";
  public static final String INIT_LIBIMG = "libsimages.equ";
  public static final String INIT_TRADUCTION = "traduction_fr.equ";
  public static final String DOSSIER_IMAGE = "images";
  public static final String DOSSIER_RACINE = "serien";
  public static final String DOSSIER_LIB = "lib";
  public static final String DOSSIER_RT = "rt";
  public static final String DOSSIER_WEB = "www";
  public static final String DOSSIER_TMP = "tmp";
  public static final String DOSSIER_CONFIG = "config";
  public static final String DOSSIER_CTRL = "ctrl";
  public static final String DOSSIER_OTHERS = "others";
  public static final String DOSSIER_METIER = "metier";
  public static final String DOSSIER_MENUS = "menus";
  public static final String DOSSIER_SIM = "sim";
  public static final String DOSSIER_MAJ = "maj";
  public static final String FIC_CONFIG = "rt.ini";
  
  public static final String FICHIER_ID = "/console/conf.xml";
  
  // Couleurs définies dans le nouveau look.
  public static final Color COULEUR_BLANCHE = new Color(255, 255, 255);
  public static final Color COULEUR_CONTENU = new Color(239, 239, 222);
  public static final Color COULEUR_POP_CONTENU = new Color(238, 238, 210);
  public static final Color COULEUR_MENUS = new Color(238, 239, 241);
  public static final int[] COULEUR_CENTRAGE_FONCE = { 90, 90, 90 };
  public static final int[] COULEUR_CENTRAGE_CLAIR = { 198, 198, 200 };
  public static final Color COULEUR_ENABLED = new Color(125, 125, 125);
  public static final Color COULEUR_F1 = new Color(90, 90, 90);
  public static final Color COULEUR_ERREURS = new Color(106, 23, 21);
  public static final Color COULEUR_NEGATIF = new Color(204, 0, 0);
  public static final Color COULEUR_LISTE_LETTRAGE = new Color(160, 70, 160);
  public static final Color COULEUR_LISTE_COMMENTAIRE = new Color(0, 102, 255);
  public static final Color COULEUR_LISTE_ANNULATION = new Color(140, 140, 140);
  public static final Color COULEUR_LISTE_FOND_BILAN = new Color(57, 105, 138);
  public static final Color COULEUR_LISTE_FOND_COMMENTAIRE = new Color(230, 240, 250);
  public static final Color COULEUR_LISTE_FOND_ANNULATION = new Color(250, 210, 210);
  public static final Color COULEUR_LISTE_BILAN = Color.white;
  public static final Color COULEUR_LISTE_ERREUR = new Color(255, 44, 47);
  public static final Color COULEUR_LISTE_GRATUIT = new Color(55, 80, 50);
  public static final Color COULEUR_TEXTE_DESACTIVE = new Color(77, 77, 80);
  public static final Color CL_DEGRADE_CLAIR = new Color(198, 198, 198);
  public static final Color CL_DEGRADE_FONCE = new Color(130, 130, 130);
  public static final Color CL_BT_CLAIR = new Color(235, 235, 235);
  public static final Color CL_BT_FONCE = new Color(195, 195, 195);
  public static final Color CL_BT_BORD = new Color(95, 95, 95);
  public static final Color CL_ZONE_SORTIE = new Color(243, 243, 236);
  public static final Color CL_TEXT_SORTIE = new Color(0, 0, 0);
  public static final Color CL_BANDEAUF_COMPTA = new Color(128, 204, 40);
  public static final Color CL_BANDEAUF_GESCOM = new Color(35, 93, 193); // (56, 113, 193)
  public static final Color CL_BANDEAUF_ACHAT = new Color(80, 173, 229);
  public static final Color CL_BANDEAUF_EXPLOITATION = new Color(204, 227, 16);
  public static final Color CL_BANDEAUF_DEFAUT =
      new Color(COULEUR_CENTRAGE_CLAIR[0], COULEUR_CENTRAGE_CLAIR[1], COULEUR_CENTRAGE_CLAIR[2]);
  public static final Color CL_BANDEAU_TEXTE = Color.BLACK;
  
  // Nouvelle charte graphique
  public static final Color COULEUR_FOND = new Color(239, 239, 222);
  public static final Color COULEUR_FOND_CHAMP_DESACTIVE = new Color(243, 243, 236);
  public static final Font POLICE_BOUTON = new Font("sansserif", Font.BOLD, 13);
  public static final Color COULEUR_VERTE = new Color(109, 166, 126);
  public static final Color COULEUR_ROUGE = new Color(177, 121, 116);
  
  // Liste contenant les correspondances des codes couleurs utilisés notamment dans les listes
  // Voir la doc http://88.188.171.20/documentation/?p=6417190 pour la mise à jour
  // l'ordre est couleur du texte puis couleur du fond
  public static final HashMap<String, Color> CORRESPONDANCE_COULEURS = new HashMap<String, Color>() {
    
    {
      put("PK", new Color(255, 192, 220));
    } // ROSE
    
    {
      // put("RD", new Color(205, 110, 110));
      put("RD", new Color(255, 0, 0));
    } // ROUGE
    
    {
      put("TQ", new Color(135, 255, 250));
    } // TURQUOISE
    
    {
      put("YL", new Color(240, 230, 150));
    } // JAUNE
    
    {
      // put("BL", new Color(180, 210, 255));
      put("BL", new Color(140, 140, 140));
    } // BLEU mais en fait gris
    
    {
      put("GR", new Color(200, 230, 110));
    } // VERT
    
    {
      put("OR", new Color(255, 210, 120));
    } // ORANGE
    
    {
      put("WH", new Color(255, 255, 255));
    } // BLANC
  };
  
  // Numéro des requêtes traitées par la classe TraitementRequeteSystemeMetier
  public static final int ID_TEXTTITLEBARINFORMATIONS = 1;
  
  public static final String EXT_BIN = ".dat";
  
  // Caractères interdits pour les noms de fichers
  public static char[] caracteresNonAutorisesPourFichier = { '\\', '/', ':', '*', '?', '"', '<', '>', '|' };
  
  // Variables pour la conversion des caractères unicodes
  private static String[] code = { "\\u00e9", "\\u00e8", "\\u00e0", "\\u00f4", "\\u00e2", "\\u00e4", "\\u00e7", "\\u00ea", "\\u00eb",
      "\\u00ee", "\\u00ef", "\\u00f6", "\\u00f9", "\\u00fb", "\\u00fc", "\\u00b0", "\\u00f3", "\\u20ac" };
  private static String[] lettre = { "\u00e9", "\u00e8", "\u00e0", "\u00F4", "\u00e2", "\u00e4", "\u00e7", "\u00ea", "\u00eb", "\u00ee",
      "\u00ef", "\u00f6", "\u00f9", "\u00fb", "\u00fc", "\u00b0", "\u00f3", "\u20ac" };
  private static String[] lettreori = { "é", "è", "à", "ô", "â", "ä", "ç", "ê", "ë", "î", "ï", "ö", "ù", "û", "ü", "°", "ó", "€" };
  
  public static char[] spaces = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', // 260 espaces
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', };
  
  /**
   * Conversion des caractères unicodes.
   */
  public static StringBuffer code2unicode(StringBuffer bchaine) {
    // Analyse de la liste
    for (int j = 0; j < code.length; j++) {
      int k = bchaine.indexOf(code[j]);
      while (k != -1) {
        bchaine.replace(k, k + 6, lettre[j]);
        k = bchaine.indexOf(code[j], k);
      }
    }
    return bchaine;
  }
  
  /**
   * Conversion des caractères unicodes.
   */
  public static String[] code2unicode(String[] liste) {
    // analyse de la liste
    for (int i = 0; i < liste.length; i++) {
      liste[i] = code2unicode(new StringBuffer(liste[i])).toString();
    }
    return liste;
  }
  
  /**
   * Conversion des caractères unicodes.
   */
  public static String code2unicode(String chaine) {
    return code2unicode(new StringBuffer(chaine)).toString();
  }
  
  /**
   * Conversion des caractères ascii en code.
   * @todo renommer la méthode en ascii2code ou mieux.
   */
  public static StringBuffer lettreori2code(StringBuffer bchaine) {
    // analyse da la chaine
    for (int j = 0; j < code.length; j++) {
      int k = bchaine.indexOf(lettreori[j]);
      while (k != -1) {
        bchaine.replace(k, k + 1, code[j]);
        k = bchaine.indexOf(lettreori[j], k);
      }
    }
    
    return bchaine;
  }
  
  /**
   * Convertion des caractères ascii en code.
   */
  public static String lettreori2code(String chaine) {
    return lettreori2code(new StringBuffer(chaine)).toString();
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static int[] splitString2Int(String chaine, char separateur) {
    ArrayList<String> vlisting = splitString2List(chaine, separateur);
    int[] liste = new int[vlisting.size()];
    for (int i = 0; i < liste.length; i++) {
      liste[i] = Integer.parseInt(vlisting.get(i));
    }
    
    return liste;
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static String[] splitString(String chaine, char separateur) {
    ArrayList<String> vlisting = splitString2List(chaine, separateur);
    String[] liste = new String[vlisting.size()];
    for (int i = 0; i < liste.length; i++) {
      liste[i] = vlisting.get(i);
    }
    
    return liste;
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static String[] splitString(String chaine, String separateur) {
    ArrayList<String> vlisting = splitString2List(chaine, separateur);
    String[] liste = new String[vlisting.size()];
    for (int i = 0; i < liste.length; i++) {
      liste[i] = vlisting.get(i);
    }
    
    return liste;
  }
  
  /**
   * Découpe un texte pour en faire un tableau où chaque élément aura la même longueur voulue.
   */
  public static String[] splitString(String pTexte, int pNombreElements, int pLongueurElement) {
    if (pTexte == null) {
      pTexte = "";
    }
    int longueurTotale = pNombreElements * pLongueurElement;
    if (pTexte.length() > longueurTotale) {
      pTexte = pTexte.substring(0, longueurTotale);
    }
    else {
      pTexte = String.format("%-" + longueurTotale + "s", pTexte);
    }
    return pTexte.split("(?<=\\G.{" + pLongueurElement + "})");
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static ArrayList<String> splitString2List(String chaine, char separateur) {
    int position = 0;
    int i = 0;
    ArrayList<String> vlisting = new ArrayList<String>(50);
    
    position = chaine.indexOf(separateur, i);
    while (position != -1) {
      if (i == position) {
        vlisting.add("");
      }
      else {
        vlisting.add(chaine.substring(i, position));
      }
      i = position + 1;
      position = chaine.indexOf(separateur, i);
    }
    vlisting.add(chaine.substring(i));
    
    return vlisting;
  }
  
  /**
   * Retourne une liste généré depuis une chaine.
   */
  public static ArrayList<String> splitString2List(String chaine, String separateur) {
    int position = 0;
    int i = 0;
    int lgSep = separateur.length();
    ArrayList<String> vlisting = new ArrayList<String>(50);
    
    position = chaine.indexOf(separateur, i);
    while (position != -1) {
      if (i == position) {
        vlisting.add("");
      }
      else {
        vlisting.add(chaine.substring(i, position));
      }
      i = position + lgSep;
      position = chaine.indexOf(separateur, i);
    }
    vlisting.add(chaine.substring(i));
    
    return vlisting;
  }
  
  /**
   * Conversion des couleurs RGB en entier.
   */
  public static int rgb2int(int r, int g, int b) {
    return (r << 16) + (g << 8) + b;
  }
  
  /**
   * Affichage d'un tableau de byte sous forme de chaine hexadécimale.
   */
  public static String hexString(byte[] buf) {
    char[] tabByteHex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    
    StringBuffer sb = new StringBuffer(buf.length * 2);
    
    for (int i = 0; i < buf.length; i++) {
      sb.append(tabByteHex[(buf[i] >>> 4) & 0xf]);
      sb.append(tabByteHex[buf[i] & 0x0f]);
    }
    return sb.toString();
  }
  
  /**
   * Forcer le look et feel Nimbus.
   */
  public static void forcerLookAndFeelNimbus() {
    // On force le L&F Nimbus s'il est disponible
    try {
      for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    }
    catch (Exception e) {
      // @todo Gérer l'exception correctement.
    }
  }
  
  /**
   * Retourne la date avec gestion d'un délai en jour (en plus ou moins).
   */
  public static int getDate(int delai) {
    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.DAY_OF_MONTH, delai);
    
    return c.get(Calendar.YEAR) * 10000 + (c.get(Calendar.MONTH) + 1) * 100 + c.get(Calendar.DAY_OF_MONTH);
  }
  
  /**
   * Retourne la date.
   */
  public static int getDate() {
    return getDate(0);
  }
  
  /**
   * Retourne une date separée de la date donnée en paramètre suivant le délai donné en paramètre en années (en plus ou moins).
   */
  public static Date getDateDelaiAnnee(Date date, int nbAnnee) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.YEAR, nbAnnee);
    
    return cal.getTime();
  }
  
  /**
   * Retourne une date separée de la date donnée en paramètre suivant le délai donné en paramètre en amois (en plus ou moins).
   */
  public static Date getDateDelaiMois(Date date, int nbMois) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.MONTH, nbMois);
    
    return cal.getTime();
  }
  
  /**
   * Retourner le premier jour du mois précédent la date fournie en paramètre.
   */
  public static Date getDateDebutMoisPrecedent(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.MONTH, -1);
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    return calendar.getTime();
  }
  
  /**
   * Retourne l'heure avec gestion d'un délai en seconde (en plus ou moins).
   */
  public static int getHeure(int delai) {
    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.SECOND, delai);
    
    return c.get(Calendar.HOUR_OF_DAY) * 10000 + (c.get(Calendar.MINUTE) + 1) * 100 + c.get(Calendar.SECOND);
  }
  
  /**
   * Retourne l'heure.
   */
  public static int getHeure() {
    return getHeure(0);
  }
  
  /**
   * Retourne le dossier racine du serveur Série N en cours d'exécution.
   */
  public static String getApplicationRootDirectory(final Class<?> classe) {
    String u = classe.getResource('/' + classe.getName().replace('.', '/') + ".class").toString();
    int deb = u.lastIndexOf(':');
    int fin = u.indexOf(Constantes.DOSSIER_RT);
    return u.substring(deb + 1, fin - 1);
  }
  
  /**
   * Retourne le dossier home de l'utilisateur.
   * (quelques soit le système et surtout la configuration de Windows).
   */
  public static String getUserHome() {
    String home = System.getenv("USERPROFILE");
    if (home == null || Constantes.normerTexte(home).isEmpty()) {
      return System.getProperty("user.home");
    }
    else {
      return home;
    }
  }
  
  /**
   * Analyse et récupère les paramètres contenus dans une chaine.
   */
  public static void analyseParametres(String ligne, HashMap<String, String> parametres) {
    if ((ligne == null) || (ligne.trim().equals(""))) {
      return;
    }
    
    ligne = ligne.trim();
    String[] liste = ligne.split(" ");
    String key = null;
    String value = null;
    int pos = -1;
    for (String chaine : liste) {
      if (chaine.startsWith("-")) {
        pos = chaine.indexOf('=');
        if (pos != -1) {
          key = chaine.substring(1, pos);
          value = chaine.substring(pos + 1);
          if (value.charAt(0) != '"') {
            parametres.put(key, value);
            key = null;
            value = null;
          }
          else if (value.charAt(value.length() - 1) == '"') {
            parametres.put(key, value.substring(1, value.length() - 1));
            key = null;
            value = null;
          }
        }
        else {
          key = chaine.substring(1);
          value = "true";
          parametres.put(key, value);
          key = null;
          value = null;
        }
      }
      else if (key != null) {
        value += " " + chaine;
        if (value.charAt(value.length() - 1) == '"') {
          parametres.put(key, value.substring(1, value.length() - 1));
          key = null;
          value = null;
        }
      }
    }
  }
  
  /**
   * Retourne s'il s'agit de l'OS400.
   */
  public static boolean isOs400() {
    return System.getProperty("os.name").toLowerCase().equalsIgnoreCase("OS/400");
  }
  
  /**
   * Retourne s'il s'agit de Windows.
   */
  public static boolean isWindows() {
    return (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0);
  }
  
  /**
   * Retourne s'il s'agit de Os X.
   */
  public static boolean isMac() {
    return (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0);
  }
  
  /**
   * Retourne s'il s'agit d'unix.
   */
  public static boolean isUnix() {
    String os = System.getProperty("os.name").toLowerCase();
    return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0);
  }
  
  /**
   * Convertir une chaîne en valeur numérique décimale.
   */
  public static BigDecimal convertirTexteEnBigDecimal(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("La valeur à convertir n'est pas un nombre décimal valide.");
    }
    
    // Remplacer le point comme séparateur décimal par une virgule
    pValeur = pValeur.replace('.', ',');
    
    // Supprimer les séparateurs de milliers
    pValeur = normerTexte(pValeur).replaceAll(" ", "");
    
    // Un texte vide équivaut à la valeur 0.
    if (pValeur.isEmpty()) {
      return BigDecimal.ZERO;
    }
    
    // Convertir le texte en nombre
    try {
      NumberFormat formatNombreDecimal = NumberFormat.getInstance(Locale.FRANCE);
      return new BigDecimal(formatNombreDecimal.parse(pValeur).toString());
    }
    catch (ParseException e) {
      throw new MessageErreurException("La valeur '" + pValeur + "' n'est pas un nombre décimal valide.");
    }
  }
  
  /**
   * Convertir une valeur décimale en chaîne de caractère en respectant la précision de l'objet BigDecimal.
   */
  public static String convertirBigDecimalEnTexte(BigDecimal pValeur) {
    if (pValeur == null) {
      return "";
    }
    return String.format(Locale.FRANCE, "%." + pValeur.scale() + "f", pValeur);
  }
  
  /**
   * Convertie une chaine en valeur numérique entière.
   * TODO A améliorer
   */
  public static int convertirTexteEnInteger(String pValeur) {
    pValeur = normerTexte(pValeur);
    if (pValeur.isEmpty()) {
      return 0;
    }
    try {
      return Integer.parseInt(pValeur.trim());
    }
    catch (NumberFormatException e) {
      try {
        NumberFormat format = NumberFormat.getInstance();
        Number number = format.parse(pValeur);
        return number.intValue();
      }
      catch (ParseException e1) {
      }
    }
    return 0;
  }
  
  /**
   * Convertie une chaine en valeur numérique entière.
   */
  public static String convertirIntegerEnTexte(int pValeur, int pNombreChiffres) {
    if (pNombreChiffres > 0) {
      return String.format("%0" + pNombreChiffres + "d", pValeur);
    }
    return String.format("%d", pValeur);
  }
  
  /**
   * Convertir une date en texte pour affichage à l'écran.
   * Le format est : JJ/MM/AAAA.
   */
  public static String convertirDateEnTexte(Date pDate) {
    if (pDate == null) {
      return "";
    }
    return String.format("%1$td/%1$tm/%1$tY", pDate);
  }
  
  /**
   * Convertir une date et heure en texte pour affichage à l'écran.
   * Le format est : JJ/MM/AAAA hh:mm.
   */
  public static String convertirDateHeureEnTexte(Date pDate) {
    return String.format("%1$td/%1$tm/%1$tY %1$tH:%1$tM", pDate);
  }
  
  /**
   * Permet de normaliser le contenu d'une chaîne de texte saisie.
   * Remplacer null par une chaîne vide, enlever les espaces, gérer les caractères spéciaux.
   */
  public static String normerTexte(String pTexte) {
    if (pTexte == null) {
      return "";
    }
    return pTexte.trim();
  }
  
  /**
   * Permet de normaliser le contenu d'une date.
   * Remplacer null par la date du jour.
   */
  public static Date normerDate(Date pDate) {
    if (pDate == null) {
      return new Date();
    }
    return pDate;
  }
  
  /**
   * Retourne si le montant est égal à zéro.
   */
  /*
  public static boolean isEgalAZero(double montant) {
    if ((-ZERO_RELATIF < montant) && (montant < ZERO_RELATIF)) {
      return true;
    }
    return false;
  }*/
  
  /**
   * Retourne si le montant est inférieur ou égal à zéro.
   */
  /*
  public static boolean isInferieurOuEgalAZero(double montant) {
    if (montant < ZERO_RELATIF) {
      return true;
    }
    return false;
  }*/
  
  /**
   * Retourne si le montant est inférieur à zéro.
   */
  /*
  public static boolean isInferieurAZero(double montant) {
    if (montant < -ZERO_RELATIF) {
      return true;
    }
    return false;
  }*/
  
  /**
   * Retourne si le montant est superieur à zéro.
   */
  /*
  public static boolean isSuperieurAZero(double montant) {
    if (montant > ZERO_RELATIF) {
      return true;
    }
    return false;
  }*/
  
  /**
   * Retourne si le montant est supérieur ou égal à zéro.
   */
  /*
  public static boolean isSuperieurOuEgalAZero(double montant) {
    if (montant >= ZERO_RELATIF) {
      return true;
    }
    return false;
  }*/
  
  /**
   * Retourne un double avec un nombre de chiffres après la virgule donné.
   * A remplacer avec arrondiClassique.
   */
  /*
  public static double round(double pValeur, int pNombreChiffres) {
    if (pNombreChiffres < 0) {
      throw new IllegalArgumentException();
    }
    long factor = (long) Math.pow(10, pNombreChiffres);
    pValeur = pValeur * factor;
    long tmp = Math.round(pValeur);
    return (double) tmp / factor;
  }*/
  
  /**
   * Retourne un double avec un nombre de chiffres après la virgule donné.
   * Utilise la règle classique de l'arrondi. Exemple :
   * 17.12465454 -> 17.12
   * 17.12545454 -> 17.13
   */
  public static double arrondiClassique(double pValeur, int pNombreChiffres) {
    if (Double.isNaN(pValeur)) {
      return 0;
    }
    if (pNombreChiffres < 0) {
      throw new IllegalArgumentException();
    }
    
    BigDecimal bd = new BigDecimal(pValeur);
    return bd.setScale(pNombreChiffres, RoundingMode.HALF_UP).doubleValue();
  }
  
  /**
   * Retourne un double avec un nombre de chiffres après la virgule donné.
   * Arrondi à la valeur inférieure. Exemple:
   * 17.56655454 -> 17.56
   * 17.52445454 -> 17.52
   */
  public static double arrondiInferieur(double pValeur, int pNombreChiffres) {
    if (Double.isNaN(pValeur)) {
      return 0;
    }
    if (pNombreChiffres < 0) {
      throw new IllegalArgumentException();
    }
    
    BigDecimal bd = new BigDecimal(pValeur);
    return bd.setScale(pNombreChiffres, RoundingMode.DOWN).doubleValue();
  }
  
  /**
   * Retourne la valeur maximum possible d'une zone numérique (RPG).
   */
  public static long valeurMaxZoneNumerique(int pNombreChiffres) {
    return (long) (Math.pow(10, pNombreChiffres) - 1);
  }
  
  /**
   * Formate simplement une chaine de caractères (trim et controle de la longueur) afin que sa longueur ne dépasse pas la longueur voulu.
   */
  public static String formaterStringMax(String pString, int pLongueur, boolean pTrim) {
    if (pTrim) {
      pString = normerTexte(pString);
    }
    else {
      if (pString == null) {
        pString = "";
      }
    }
    if (pString.length() > pLongueur) {
      pString = pString.substring(0, pLongueur);
    }
    return pString;
  }
  
  /**
   * Formate strictement une chaine de caractères à la longueur voulue.
   */
  public static String formaterStringStrict(String pString, int pLongueur) {
    if (pString == null) {
      pString = "";
    }
    if (pString.length() > pLongueur) {
      pString = pString.substring(0, pLongueur);
    }
    else {
      pString = String.format("%-" + pLongueur + "s", pString);
    }
    return pString;
  }
  
  /**
   * Supprime les espaces et équuivalents à la fin du texte.
   */
  public static String trimRight(String pTexte) {
    if (pTexte == null) {
      return "";
    }
    return trimRight(new StringBuilder(pTexte));
  }
  
  /**
   * Supprime les espaces et équuivalents à la fin du texte.
   */
  public static String trimRight(StringBuilder pTexte) {
    if (pTexte == null) {
      return "";
    }
    
    // On parcourt le texte depuis la fin
    for (int i = pTexte.length() - 1; i > 0; i--) {
      if ((pTexte.charAt(i) == ' ') || (pTexte.charAt(i) == '\n') || (pTexte.charAt(i) == '\t') || (pTexte.charAt(i) == '\r')) {
        pTexte.deleteCharAt(i);
      }
      else {
        break;
      }
    }
    return pTexte.toString();
  }
  
  /**
   * Parcourt un tableau et retoune le nombre d'éléments non null consécutifs.
   */
  public static int controlerElementsTableau(Object[] pTableau) {
    if (pTableau == null) {
      return 0;
    }
    // On controle le nombre d'élements non null
    int nombreElements = 0;
    for (int i = 0; i < pTableau.length; i++) {
      if (pTableau[i] == null) {
        break;
      }
      nombreElements++;
    }
    return nombreElements;
  }
  
  /**
   * Conversion du fichier xml de la sérialisation des classes pour la 2.14.
   * @throws Exception
   */
  public static boolean convert213to214(String pFichierAConvertir, String pNomPackage) throws Exception {
    String fichierXML = Constantes.normerTexte(pFichierAConvertir);
    if (fichierXML.isEmpty()) {
      throw new Exception("Le nom du fichier est vide.");
    }
    
    // Chargement du fichier xml en mode texte
    GestionFichierTexte gft = new GestionFichierTexte(fichierXML);
    ArrayList<String> contenu = gft.getContenuFichier();
    
    // Modification des noms des packages non officiels
    boolean enregistrer = false;
    for (int i = 0; i < contenu.size(); i++) {
      if (contenu.get(i).contains("class=\"") && !contenu.get(i).contains("class=\"java")
          && !contenu.get(i).contains("class=\"" + pNomPackage)) {
        contenu.set(i, contenu.get(i).replaceFirst("class=\"", "class=\"" + pNomPackage));
        enregistrer = true;
      }
    }
    if (enregistrer) {
      if (gft.ecritureFichier() == Constantes.ERREUR) {
        throw new Exception(gft.getMsgErreur());
      }
    }
    return enregistrer;
  }
  
  /**
   * Retourne l'id session assemblé.
   */
  public static long computeIdSession(int idpart1, int idpart2) {
    long idSession = 0;
    if (idpart1 != 0) {
      idSession = idpart1;
    }
    else {
      idSession = (int) System.currentTimeMillis();
    }
    
    idSession = (idSession << 32) | idpart2;
    return idSession;
  }
  
  /**
   * Tester l'égalité entre deux objets en gérant correctement les valeurs null.
   * Si la deux valeurs passées sont null, la métohde retourne true.
   * Si les deux valeurs, sont des BigDecimal, cela compare les valeurs.
   */
  public static boolean equals(Object pObject1, Object pObject2) {
    if (pObject1 == null) {
      return pObject2 == null;
    }
    else if (pObject1 instanceof BigDecimal && pObject2 instanceof BigDecimal) {
      return ((BigDecimal) pObject1).compareTo((BigDecimal) pObject2) == 0;
    }
    else {
      return pObject1.equals(pObject2);
    }
  }
  
  /**
   * Controle la validité de la syntaxe d'une adresse mail.
   */
  public static boolean controlerEmail(String pEmail) {
    if (pEmail == null || pEmail.isEmpty()) {
      return false;
    }
    // Il s'agit un contrôle simple pour l'instant
    return pEmail.matches(".+@.+\\.[a-z]+");
  }
}
