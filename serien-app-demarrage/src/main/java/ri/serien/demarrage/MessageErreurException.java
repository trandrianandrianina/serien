/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

/**
 * Message d'erreur destiné à l'utilisateur du logiciel.
 * <br>
 * Cette exception permet de générer un message qui sera affiché à l'utilisateur. Il doit donc être rédigé dans un français clair et
 * simple.
 * Il faut limiter le jargon technique. Ce message ne peut correspondre qu'à un message d'erreur car le fait de lever une exception
 * interrompt le traitement en cours. Ce mécanisme n'est donc pas adapté pour des messages d'informations ou d'alertes.
 * <br>
 * Ce message peut-être généré côté client ou serveur. Côté serveur, si c'est dans le cadre d'un appel de méthode RMI, l'exception
 * sera remontée au client qui pourra l'afficher à l'utilisateur.
 */
public class MessageErreurException extends RuntimeException {
  private static final String PREFIXE = "[MessageErreurException] ";
  public static final String MESSAGE_ERREUR_TECHNIQUE = "Une erreur technique est survenue. Merci de contacter le service assistance.";
  
  /**
   * Constructeur avec un message utilisateur simple.
   * <br>
   * Ce type de message permet de remonter des erreurs fonctionnels, par exemple "La date du document est supérieure à la date du jour".
   * Ce message génère une trace de type "Information".
   */
  public MessageErreurException(String ptexteMessage) {
    super(ptexteMessage);
    Trace.erreur(this, PREFIXE + ptexteMessage);
  }
  
  /**
   * Constructeur avec un message utilisateur accompagné d'une exception.
   * <br>
   * Ce type de message est adapté aux erreurs techniques pour lesquelles ont veut afficher un message plus compréhensible pour
   * l'utilisateur, par exemple "Erreur lors de la lecture des informations du client".
   * <br>
   * Les messages plus techniques contenus dans les exceptions ainsi que la stacktrace seront consultables via l'icône engrenage de la
   * boîte de message standard. Ces informations génèrent une trace de type "Erreur".
   */
  public MessageErreurException(Exception e, String ptexteMessage) {
    super(ptexteMessage, e);
    Trace.erreur(e, PREFIXE + ptexteMessage);
  }
}
