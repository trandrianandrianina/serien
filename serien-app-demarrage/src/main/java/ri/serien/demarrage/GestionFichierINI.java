/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * Gestion des fichiers INI.
 */
public class GestionFichierINI extends GestionFichierTexte {
  // Constantes
  private static final String FILTER_EXT_INI = "ini";
  private static final String FILTER_DESC_INI = "Fichier INI (*.ini)";
  
  // Variables
  private LinkedHashMap<String, LinkedHashMap<String, String>> listeNomSection = null;
  
  /**
   * Constructeur.
   */
  public GestionFichierINI() {
  }
  
  /**
   * Constructeur de la classe.
   */
  public GestionFichierINI(String fch) {
    super(fch);
  }
  
  /**
   * Retourne la hashmap des Sections.
   */
  public LinkedHashMap<String, LinkedHashMap<String, String>> getSections() {
    int i = 0;
    int pos = 0;
    String chaine = null;
    String section = null;
    ArrayList<String> lst = getContenuFichier();
    LinkedHashMap<String, String> listeDetailSection = null;
    
    listeNomSection = new LinkedHashMap<String, LinkedHashMap<String, String>>();
    if (lst == null) {
      return listeNomSection;
    }
    
    // On parcourt la liste afin d'en extraire les données
    for (i = 0; i < lst.size(); i++) {
      chaine = lst.get(i).trim();
      
      // Les erreurs possibles ou lignes ignorées
      if (chaine.equals("")) {
        continue;
      }
      if (chaine.startsWith(";")) {
        continue;
      }
      
      // Traitement section
      if ((chaine.startsWith("[")) && (chaine.endsWith("]"))) {
        section = chaine.substring(1, chaine.length() - 1).trim();
        listeDetailSection = new LinkedHashMap<String, String>();
        listeNomSection.put(section, listeDetailSection);
      }
      else if (listeDetailSection != null) {
        // Traitement propriété
        pos = chaine.indexOf('=');
        if (pos != -1) {
          listeDetailSection.put(chaine.substring(0, pos).trim().toLowerCase(), chaine.substring(pos + 1).trim());
        }
        else {
          listeDetailSection.put(chaine.trim(), chaine.trim());
        }
      }
    }
    lst.clear();
    
    return listeNomSection;
  }
  
  /**
   * Retourne la liste des Sections.
   * A toujours utiliser après getListeNomSection (TODO à corriger).
   */
  public String[] getListeNomSection() {
    int i = 0;
    String[] lst = null;
    
    if (listeNomSection == null) {
      listeNomSection = getSections();
    }
    
    lst = new String[listeNomSection.size()];
    // Set<String> lSection = listeNomSection.keySet();
    Iterator<String> iterateur = listeNomSection.keySet().iterator();
    i = 0;
    while (iterateur.hasNext()) {
      lst[i++] = iterateur.next();
    }
    
    return lst;
  }
  
  /**
   * Libère la mémoire.
   */
  @Override
  public void dispose() {
    super.dispose();
    if (listeNomSection != null) {
      for (Entry<String, LinkedHashMap<String, String>> entry : listeNomSection.entrySet()) {
        if (entry.getValue() != null) {
          entry.getValue().clear();
        }
      }
    }
    listeNomSection.clear();
    listeNomSection = null;
  }
  
}
