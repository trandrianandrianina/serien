/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Cette classe gère les attributs d'un fichier à télécharger.
 */
public class FileAtDownload {
  // Constantes
  public static final int CMP_MOST_OLD_ONSERVER = -1;
  public static final int CMP_DIFFERENT_ONSERVER = 0;
  public static final int CMP_MOST_RECENT_ONSERVER = 1;
  
  // Variables
  private String nameFileAtDownload = null;
  private String nameFileDownloaded = null;
  private long length = 0;
  private long lastModified = 0;
  private String dateLastModified = "";
  private boolean okForDownload = false;
  private boolean downloadEnded = false;
  private FileNG fileDownloaded = null;
  private boolean testExistOnClient = true;
  private boolean existOnServer = true;
  
  private int compare = CMP_MOST_RECENT_ONSERVER;
  
  public static int totalBytes = 0; // Nombre d'octets total à télécharger
  public static int downloadedBytes = 0; // Nombre d'octets déjà téléchargé
  
  // private static SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
  private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
  
  /**
   * Constructeur.
   */
  public FileAtDownload(String anameFileAtDownload, String anameFileDownloaded, int acompare) {
    setNameFileAtDownload(anameFileAtDownload);
    nameFileDownloaded = anameFileDownloaded;
    setCompare(acompare);
  }
  
  /**
   * Constructeur.
   */
  public FileAtDownload(String anameFileAtDownload, String anameFileDownloaded) {
    this(anameFileAtDownload, anameFileDownloaded, CMP_MOST_RECENT_ONSERVER);
  }
  
  /**
   * Initialise et met en forme le fichier à télécharger.
   */
  public void setNameFileAtDownload(String file) {
    if (file == null) {
      return;
    }
    
    file = file.trim();
    if (file.charAt(0) != '/') {
      nameFileAtDownload = '/' + file;
    }
    else {
      nameFileAtDownload = file;
    }
  }
  
  /**
   * Retourne le fichier télécharger.
   */
  public FileNG getFileDownloaded() {
    if (fileDownloaded == null) {
      fileDownloaded = new FileNG(nameFileDownloaded);
    }
    return fileDownloaded;
  }
  
  /**
   * Indiquer que c'est ok pour le téléchargement.
   */
  public void setOkForDownload(boolean isok) {
    if (existOnServer) {
      if (!okForDownload && isok) {
        totalBytes += length;
      }
      else if (okForDownload && !isok) {
        totalBytes -= length;
      }
      okForDownload = isok;
    }
  }
  
  /**
   * Tester si c'est ok pour le téléchargement.
   */
  public boolean isOkForDownload() {
    if (existOnServer && testExistOnClient && !getFileDownloaded().exists()) {
      setOkForDownload(true);
    }
    return okForDownload;
  }
  
  /**
   * Compare les dates du fichier local et distant, et retourne true si le fichier distant (server) est plus récent
   * @param datelocalfile information lue dans le cache.txt et non sur le fichier lui-même.
   */
  public boolean isOkForDownload(String datelocalfile) {
    if ((datelocalfile == null) || (datelocalfile.equals("")) || (dateLastModified.isEmpty())) {
      return true;
    }
    
    switch (compare) {
      case CMP_MOST_OLD_ONSERVER:
        try {
          dateFormat.setLenient(false);
          return (dateFormat.parse(datelocalfile).compareTo(dateFormat.parse(dateLastModified)) > 0);
        }
        catch (ParseException e) {
          return true;
        }
        
      case CMP_DIFFERENT_ONSERVER:
        try {
          return !dateFormat.parse(datelocalfile).equals(dateFormat.parse(dateLastModified));
        }
        catch (ParseException e) {
          return true;
        }
        
      case CMP_MOST_RECENT_ONSERVER:
        try {
          dateFormat.setLenient(false);
          return (dateFormat.parse(datelocalfile).compareTo(dateFormat.parse(dateLastModified)) < 0);
        }
        catch (ParseException e) {
          // msgErreur += "Erreur lors de conversion des dates: locale=" + datelocalfile + " distante=" + lastModified + '\n';
          return true;
        }
        
      default:
        break;
    }
    return true;
  }
  
  /**
   * Converti un timestamp en date au format texte.
   */
  private void convertirDateLongEnDateString(long pTimestamp) {
    Timestamp stamp = new Timestamp(pTimestamp);
    try {
      dateLastModified = dateFormat.format(stamp.getTime());
    }
    catch (Exception e) {
      dateLastModified = "";
    }
  }
  
  // -- Accesseurs
  
  public boolean isTestExistOnClient() {
    return testExistOnClient;
  }
  
  public void setTestExistOnClient(boolean testExistOnClient) {
    this.testExistOnClient = testExistOnClient;
  }
  
  public boolean isExistOnServer() {
    return existOnServer;
  }
  
  public void setExistOnServer(boolean existOnServer) {
    this.existOnServer = existOnServer;
  }
  
  /**
   * @return le compare.
   */
  public int getCompare() {
    return compare;
  }
  
  /**
   * @param compare le compare à définir.
   */
  public void setCompare(int compare) {
    this.compare = compare;
  }
  
  public String getNameFileDownloaded() {
    return nameFileDownloaded;
  }
  
  public void setNameFileDownloaded(String nameFileDownloaded) {
    this.nameFileDownloaded = nameFileDownloaded;
  }
  
  public long getLastModified() {
    return lastModified;
  }
  
  public void setLastModified(long lastModified) {
    this.lastModified = lastModified;
    convertirDateLongEnDateString(this.lastModified);
  }
  
  public boolean isDownloadEnded() {
    return downloadEnded;
  }
  
  public void setDownloadEnded(boolean downloadEnded) {
    this.downloadEnded = downloadEnded;
  }
  
  public String getNameFileAtDownload() {
    return nameFileAtDownload;
  }
  
  public void setLength(long alength) {
    length = alength;
  }
  
  public long getLength() {
    return length;
  }
  
  public void setFileDownloaded(FileNG fileDownloaded) {
    this.fileDownloaded = fileDownloaded;
  }
  
  public String getDateLastModified() {
    return dateLastModified;
  }
}
