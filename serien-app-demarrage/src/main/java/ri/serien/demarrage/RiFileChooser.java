/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.demarrage;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class RiFileChooser {
  // Variables
  private File dernierChemin = null;
  private JFrame frame = null;
  private JFileChooser choixFichier = null;
  
  /**
   * Constructeur.
   */
  public RiFileChooser(JFrame frame, File derchemin) {
    setFrame(frame);
    setDossier(derchemin);
  }
  
  /**
   * Constructeur.
   */
  public RiFileChooser(JFrame frame, String derchemin) {
    setFrame(frame);
    if (derchemin == null) {
      return;
    }
    setDossier(new File(derchemin));
  }
  
  /**
   * Initialise la fenêtre maitresse.
   */
  private void setFrame(JFrame frame) {
    this.frame = frame;
  }
  
  /**
   * Initialise le dossier par défaut.
   */
  public void setDossier(File derchemin) {
    dernierChemin = derchemin;
    // if (dernierChemin != null)
    // choixFichier.setCurrentDirectory(dernierChemin);
  }
  
  /**
   * Retourne le dossier par défaut.
   */
  public File getDossierFile() {
    return dernierChemin;
  }
  
  /**
   * Retourne le dossier par défaut.
   */
  public String getDossier() {
    return dernierChemin.getAbsolutePath();
  }
  
  /**
   * Recherche le fichier à ouvrir.
   */
  public String choisirFichier(String extension, String extdescription) {
    String fichier = null;
    
    choixFichier = new JFileChooser();
    choixFichier.setCurrentDirectory(dernierChemin);
    choixFichier.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { extension }, extdescription));
    
    // Récupération du chemin
    if (choixFichier.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
      fichier = choixFichier.getSelectedFile().getPath();
    }
    else {
      return null;
    }
    
    // On stocke le dossier courant
    dernierChemin = choixFichier.getSelectedFile().getParentFile();
    
    return fichier;
  }
  
  /**
   * Ouvre une boite de dialogue afin de sélectionner un dossier.
   */
  public File choisirDossier(String origine) {
    if (origine == null) {
      origine = dernierChemin.getAbsolutePath();
    }
    choixFichier = new JFileChooser(origine);
    choixFichier.setApproveButtonText("Sélectionner");
    choixFichier.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (choixFichier.showOpenDialog(frame) == JFileChooser.CANCEL_OPTION) {
      return null;
    }
    dernierChemin = choixFichier.getSelectedFile();
    
    return dernierChemin;
  }
  
  /**
   * Recherche le fichier pour enregistrer.
   */
  public File enregistreFichier(File fichier, String extension, String extdescription) {
    choixFichier = new JFileChooser();
    choixFichier.setCurrentDirectory(dernierChemin);
    choixFichier.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { extension }, extdescription));
    choixFichier.setSelectedFile(fichier);
    
    // Récupération du chemin
    if (choixFichier.showSaveDialog(choixFichier) == JFileChooser.APPROVE_OPTION) {
      fichier = choixFichier.getSelectedFile();
    }
    else {
      return null;
    }
    
    // On stocke le dossier courant
    dernierChemin = choixFichier.getSelectedFile().getParentFile();
    
    return fichier;
  }
}
