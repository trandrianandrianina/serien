/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFormattedTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.MaskFormatter;

import org.jdesktop.swingx.JXDatePicker;

/**
 * Cette classe permet d'instancier un calendrier de type JXDatePicker formaté pour les dates de Série M.
 * Composant datepicker aménagé pour nos besoins.
 * @todo Problème avec typesaisie (dans les propriétés) le TypeSaisieEditor n'est pas pris en compte.
 * @todo Empêcher la saisie du bouton lorsque la zone est jugée NonEditable().
 */
public class RiCalendrier extends JXDatePicker {
  // Constantes
  
  public static final String JJMMAA = "JJMMAA";
  public static final String MMAA = "MMAA";
  public static final int SAISIE_JJMMAA = 8;
  public static final int SAISIE_MMAA = 6;
  
  // Variables
  private int typeSaisie = SAISIE_JJMMAA;
  private boolean selectionComplete = true;
  
  /**
   * Cet attribut est un tableau qui représente les chaines de caractères autorisées lors de la saisie de la date. La première chaine est
   * le format par défaut.
   */
  private String[][] formatsDates = { { "dd.MM.yy", "ddMMyy", "MMyy", ".MM.yy" }, { "MM.yy", "MMyy" } };
  
  /**
   * Cet attribut est un masque de saisie permettant de forcer l'utilsateur à saisir certains types de caractères dans un ordre donné.
   * Attention,actif seulement lors de la première saisie !!!
   */
  MaskFormatter moule = null;
  
  /**
   * Cet attribut est l'éditeur permettant à l'utilisateur de saisir manuellement une date dans une zone formatée. Voir
   * <a href="http://java.sun.com/j2se/1.4.2/docs/api/javax/swing/JFormattedTextField.html">JFormattedTextField</a>
   */
  JFormattedTextField zoneSaisie = new JFormattedTextField();
  
  /**
   * Constructeur prenant le type de date (<b>JJ.MM.AA (8)</b> ou <b>MM.AA(5)</b>) en paramètre.
   * @param type ce paramètre représente le type de date donné ( c'est à dire 8 caractères pour les dates complètes
   *          JJ.MM.AA ou 5 pour les dates MM.AA
   */
  public RiCalendrier(int type) {
    super();
    setInit();
    setTypeSaisie(type);
  }
  
  /**
   * Constructeur à vide -> il appelle le constructeur de type 8 par défaut.
   */
  public RiCalendrier() {
    this(SAISIE_JJMMAA);
  }
  
  /**
   * Valider automatiquement la sélection saisie dans le textfield.
   */
  public void validerSelection() {
    // Envoyer la saisie au bloc Calendrier
    try {
      this.commitEdit();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Permet de renvoyer le type de saisie (MMAA ou JJMMAA).
   */
  public int getTypeSaisie() {
    return typeSaisie;
  }
  
  /**
   * Permet de modifier le type de saisie (MMAA ou JJMMAA).
   */
  public void setTypeSaisie(int typeSaisie) {
    this.typeSaisie = typeSaisie;
    if (typeSaisie == SAISIE_JJMMAA) {
      this.setFormats(formatsDates[0]);
      this.setSize(115, this.getPreferredSize().height);
      // modele = "##.##.##";
    }
    else if (typeSaisie == SAISIE_MMAA) {
      this.setFormats(formatsDates[1]);
      this.setSize(100, this.getPreferredSize().height);
      // modele = "##.##";
    }
  }
  
  /**
   * Initialise le format de saisie.
   */
  private void setInit() {
    // Interception de la touche ENTER (pour l'activation du défaut bouton)
    zoneSaisie.addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent e) {
        KeyEvent key = e;
        if (key.getKeyCode() == KeyEvent.VK_ENTER) {
          seBarrer();
        }
      }
    });
    
    // Gérer les pertes et les gains de focus de la zone de saisie
    zoneSaisie.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent arg0) {
        if (selectionComplete) {
          if ((!zoneSaisie.getText().equals("  .  ")) && (!zoneSaisie.getText().equals(""))
              && (!zoneSaisie.getText().equals("  .  .  "))) {
            if (typeSaisie == SAISIE_JJMMAA && zoneSaisie.getText().length() == 8) {
              SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                  zoneSaisie.selectAll();
                }
              });
            }
            else {
              SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                  zoneSaisie.setCaretPosition(0);
                }
              });
            }
          }
        }
        else {
          if ((!zoneSaisie.getText().equals("  .  ")) && (!zoneSaisie.getText().equals(""))
              && (!zoneSaisie.getText().equals("  .  .  "))) {
            SwingUtilities.invokeLater(new Runnable() {
              @Override
              public void run() {
                zoneSaisie.setCaretPosition(0);
              }
            });
          }
        }
      }
      
      @Override
      public void focusLost(FocusEvent arg0) {
        validerSelection();
      }
    });
    
    this.setEditor(zoneSaisie);
  }
  
  /**
   * Modifie le type de sélection automatique sur prise de focus de la zone de saisie.
   */
  public void setSelectionComplete(boolean select) {
    selectionComplete = select;
  }
  
  /**
   * Accède au type de sélection automatique sur prise de focus de la zone de saisie.
   */
  public boolean isSelectionComplete() {
    return selectionComplete;
  }
  
  /**
   * @todo A commenter.
   */
  public void seBarrer() {
    try {
      getRootPane().getDefaultButton().doClick();
    }
    catch (Exception e) {
    }
  }
  
}
