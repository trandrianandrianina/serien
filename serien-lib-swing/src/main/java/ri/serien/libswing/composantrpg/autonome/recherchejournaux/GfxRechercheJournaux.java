/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome.recherchejournaux;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.comptabilite.personnalisation.journal.CriteresRechercheJournaux;
import ri.serien.libcommun.comptabilite.personnalisation.journal.Journal;
import ri.serien.libcommun.comptabilite.personnalisation.journal.ListeJournal;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;

/**
 * Récupère la liste des journaux
 */
public class GfxRechercheJournaux extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private boolean valide = true;
  private int typejo = Journal.TYPE_ALL;
  private int filtrejo = CriteresRechercheJournaux.FILTRE_AUCUN;
  private CriteresRechercheJournaux criteres = new CriteresRechercheJournaux();
  private ListeJournal listeJournaux = null;
  
  /**
   * Constructeur.
   */
  public GfxRechercheJournaux(SNPanelEcranRPG parent) {
    this(parent, true, Journal.TYPE_ALL, CriteresRechercheJournaux.FILTRE_AUCUN);
  }
  
  /**
   * Constructeur.
   */
  public GfxRechercheJournaux(SNPanelEcranRPG parent, byte atypejo) {
    this(parent, true, atypejo, CriteresRechercheJournaux.FILTRE_AUCUN);
  }
  
  /**
   * Constructeur.
   */
  public GfxRechercheJournaux(SNPanelEcranRPG parent, int atypejo, int afiltrejo) {
    this(parent, true, atypejo, afiltrejo);
  }
  
  /**
   * Constructeur.
   */
  public GfxRechercheJournaux(SNPanelEcranRPG parent, boolean avalide) {
    this(parent, avalide, Journal.TYPE_ALL, CriteresRechercheJournaux.FILTRE_AUCUN);
  }
  
  /**
   * Constructeur.
   */
  public GfxRechercheJournaux(SNPanelEcranRPG parent, boolean avalide, int atypejo) {
    this(parent, avalide, atypejo, CriteresRechercheJournaux.FILTRE_AUCUN);
  }
  
  /**
   * Constructeur.
   */
  public GfxRechercheJournaux(SNPanelEcranRPG parent, boolean avalide, int atypejo, int afiltrejo) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // icones
    bouton_valider.setIcon(lexique.chargerImage("images/ok_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    setTitle("Recherche code journal");
    setValide(avalide);
    typejo = atypejo;
    filtrejo = afiltrejo;
    
    // Lancement de la requête
    // AutoCompleteDecorator.decorate(chk_LibelleJO);
    listeJournaux = new ListeJournal();
  }
  
  @Override
  public void setData() {
    super.setData();
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) lexique.getValeurVariableGlobale("CODE_SOC"));
    criteres.setIdEtablissement(idEtablissement);
    String codeJo = lexique.HostFieldGetData((String) lexique.getValeurVariableGlobale("ZONE_JO"));
    criteres.setCodeJournal(codeJo);
    criteres.setTypeJournal(typejo);
    criteres.setFiltre(filtrejo);
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          listeJournaux = listeJournaux.charger(getIdSession(), criteres.getIdEtablissement());
          fillComponent(chk_LibelleJO, listeJournaux, false);
        }
        catch (Exception e) {
          Trace.erreur(e, "Erreur lors du chargement des journaux");
        }
      }
    });
  }
  
  /**
   * Méthode générique de remplissage d'un composant avec les données reçues.
   */
  private void fillComponent(JComponent component, ListeJournal pListeJournal, boolean pAjouteEnregistrementVide) {
    if (component instanceof RiFilterComboBox) {
      fillFilterCombobox((RiFilterComboBox) component, pListeJournal, pAjouteEnregistrementVide);
    }
    else if (component instanceof JComboBox) {
      fillCombobox((JComboBox) component, pListeJournal, pAjouteEnregistrementVide);
    }
  }
  
  /**
   * Remplissage d'une combobox avec les données reçues.
   */
  private void fillCombobox(JComboBox component, ListeJournal pListeJournal, boolean pAjouteEnregistrementVide) {
    component.requestFocus();
    component.removeAllItems();
    for (String nom : pListeJournal.retournerListeLibelleJournaux(pAjouteEnregistrementVide)) {
      component.addItem(nom);
    }
  }
  
  /**
   * Remplissage d'une combobox avec les données reçues.
   */
  private void fillFilterCombobox(RiFilterComboBox component, ListeJournal pListeJournal, boolean pAjouteEnregistrementVide) {
    component.requestFocus();
    List<String> listeLibelles = pListeJournal.retournerListeLibelleJournaux(pAjouteEnregistrementVide);
    component.init(listeLibelles.toArray(), true);
    int indice = listeJournaux.retournerIndiceCodeJournal(criteres.getCodeJournal());
    if (indice > 0) {
      component.setSelectedIndex(indice);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    // rj.dispose();
    if (getJDialog() != null) {
      getJDialog().dispose();
    }
  }
  
  /**
   * @return le valid
   */
  public boolean isValide() {
    return valide;
  }
  
  /**
   * Initialise la validation après le choix du journal false on n'envoi pas entrée, true valeur par défaut.
   */
  public void setValide(boolean valide) {
    this.valide = valide;
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    if (chk_LibelleJO.getSelectedIndex() > -1) {
      String codeJournal = listeJournaux.retournerCodeJournal(chk_LibelleJO.getSelectedIndex());
      lexique.HostFieldPutData((String) lexique.getValeurVariableGlobale("ZONE_JO"), 0, codeJournal);
    }
    getData();
    
    closePopupLinkWithBuffer(true);
    if (valide) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_Principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_8 = new JLabel();
    chk_LibelleJO = new RiFilterComboBox();
    
    // ======== this ========
    setForeground(Color.black);
    setBackground(new Color(238, 238, 210));
    setPreferredSize(new Dimension(635, 140));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_Principal ========
    {
      p_Principal.setName("p_Principal");
      p_Principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_Principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(255, 200));
        p_contenu.setMinimumSize(new Dimension(255, 200));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          
          // ---- OBJ_8 ----
          OBJ_8.setText("Journal");
          OBJ_8.setName("OBJ_8");
          
          // ---- chk_LibelleJO ----
          chk_LibelleJO.setEditable(true);
          chk_LibelleJO.setName("chk_LibelleJO");
          
          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup().addGap(23, 23, 23)
                  .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(chk_LibelleJO, GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE).addGap(36, 36, 36)));
          panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup().addGap(22, 22, 22)
                  .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(OBJ_8).addComponent(chk_LibelleJO,
                      GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        }
        p_contenu.add(panel1);
        panel1.setBounds(25, 25, 400, 90);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_Principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_Principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_Principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_8;
  private RiFilterComboBox chk_LibelleJO;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
