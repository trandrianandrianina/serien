/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * Personnalisation du composant XRiSpinner.
 */
public class XRiSpinner extends JSpinner implements IXRiComposant {
  // Variables
  private Lexical lexique = null;
  private oData hostfield = null;
  
  // Liste des buffers constinuant le panel final (à voir si on peut s'en passer en gérant mieux les setData)
  private HashMap<String, oRecord> listeRecord = null;
  // Interpréteur de variables (à voir si on peut pas le récupérer avec lexical)
  private iData interpreteurD = null;
  // <- ne sert pas ici car fait le lien entre le hostfield et le composant
  private ConcurrentHashMap<oData, Object> listeoDataFlux = null;
  // Stocke l'objet qui a le focus
  private JComponent[] requestcomposant = null;
  // Valeurs renvoyées/reçues par l'AS400
  private String[] valueHostfield = null;
  
  /**
   * Constructeur.
   */
  public XRiSpinner() {
    super();
    
  }
  
  /**
   * Methode de oFrame un peu adaptées.
   * Stocke les variables qui composent la table.
   */
  @Override
  public void init(final Lexical lexique, HashMap<String, oRecord> alisteRecord, iData ainterpreteurD,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, JComponent[] arequestcomposant) {
    // Initialisation des variables nécessaires
    this.lexique = lexique;
    setInterpreteurD(ainterpreteurD);
    setListeRecord(alisteRecord);
    setListeoDataFlux(alisteoDataFlux);
    requestcomposant = arequestcomposant;
  }
  
  /**
   * Initialise les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    hostfield = lexique.getHostField(this.getName()); // nom du host
    if (hostfield != null) {
      if (hostfield.getEvalVisibility() == Constantes.OFF) {
        setVisible(false);
      }
      else {
        setVisible(true);
        try {
          // Dans le cas où la valeur du host ne soit pas dans la liste et le model soit différent
          chargeModele();
          
          // Analyse de la valeur reçue
          String valeur = hostfield.getValeurToFrame();
          boolean isVide = valeur.equals("");
          boolean isAlpha = true;
          if (isVide) {
            if (valueHostfield != null) {
              valeur = valueHostfield[0];
            }
            else if (hostfield.isAlpha()) {
              valeur = " ";
            }
            else {
              valeur = "0";
            }
          }
          // Affectation de la valeur
          if (getModel() instanceof javax.swing.SpinnerNumberModel) {
            setValue(Integer.parseInt(valeur));
          }
          else if (getModel() instanceof javax.swing.SpinnerListModel) {
            if (hostfield.isAlpha()) {
              setValue(valeur);
            }
            else {
              if (isAlpha) {
                setValue(valeur);
              }
              else {
                setValue(Integer.parseInt(valeur));
              }
            }
          }
          else {
            setValue(valeur);
          }
        }
        catch (Exception e) {
        }
        
        // On vérifie si le oData est en E/S
        if (lexique.isIntoCurrentRecord(hostfield)) {
          // Filtre de saisie
          listeoDataFlux.put(hostfield, this); // Nécessaire pour le getData de oFrame
          setEnabled(true);
          if (hostfield.getEvalFocus() == Constantes.ON) {
            requestcomposant[0] = this;
          }
        }
        else {
          setEnabled(false);
        }
      }
    }
    
  }
  
  /**
   * Met à jour le oData du flux courant.
   */
  @Override
  public void getData() {
    if (!isVisible()) {
      return;
    }
    hostfield.setValeurFromFrame("" + getValue());
  }
  
  /**
   * Modèle des données.
   */
  private void chargeModele() {
    if (valueHostfield != null) {
      setModel(new SpinnerListModel(valueHostfield));
    }
  }
  
  /**
   * Initialise la table des valeurs du spinner (à utiliser juste après initDiverses()).
   **/
  public void setValeurs(String[] value) {
    valueHostfield = value;
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  public void setListeRecord(HashMap<String, oRecord> listeRecord) {
    this.listeRecord = listeRecord;
  }
  
  public HashMap<String, oRecord> getListeRecord() {
    return listeRecord;
  }
  
  public iData getInterpreteurD() {
    return interpreteurD;
  }
  
  public void setInterpreteurD(iData interpreteurD) {
    this.interpreteurD = interpreteurD;
  }
  
  public ConcurrentHashMap<oData, Object> getListeoDataFlux() {
    return listeoDataFlux;
  }
  
  public void setListeoDataFlux(ConcurrentHashMap<oData, Object> listeoDataFlux) {
    this.listeoDataFlux = listeoDataFlux;
  }
  
  @Override
  public void dispose() {
    lexique = null;
    hostfield = null;
    listeRecord = null;
    interpreteurD = null;
    listeoDataFlux = null;
    requestcomposant = null;
    valueHostfield = null;
  }
  
}
