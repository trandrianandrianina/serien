/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

public class ProprieteTouchePopupPerso {
  // Variables
  private boolean rafraichirPanelParent = true;
  private boolean miseAJourbuffer = true;
  private boolean envoyerToucheAuRPG = false;
  
  // -- Accesseurs
  
  public boolean isRafraichirPanelParent() {
    return rafraichirPanelParent;
  }
  
  public void setRafraichirPanelParent(boolean rafraichirPanelParent) {
    this.rafraichirPanelParent = rafraichirPanelParent;
  }
  
  public boolean isMiseAJourbuffer() {
    return miseAJourbuffer;
  }
  
  public void setMiseAJourbuffer(boolean miseAJourbuffer) {
    this.miseAJourbuffer = miseAJourbuffer;
  }
  
  public boolean isEnvoyerToucheAuRPG() {
    return envoyerToucheAuRPG;
  }
  
  public void setEnvoyerToucheAuRPG(boolean envoyerToucheAuRPG) {
    this.envoyerToucheAuRPG = envoyerToucheAuRPG;
  }
  
}
