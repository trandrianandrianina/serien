/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Permet de personnaliser une cellule de JTable
 */
public class MyCellRenderer extends DefaultTableCellRenderer {
  // Variables
  private int[] justification = null;
  private Color[][] couleursFond = null;
  private Color[][] couleurs = null;
  private Font[][] polices = null;
  
  /**
   * Constructeur
   */
  public MyCellRenderer() {
  }
  
  /**
   * Constructeur
   * @param couleurs
   * @param couleursFond
   * @param polices
   * @param ajustification
   */
  public MyCellRenderer(Color[][] couleurs, Color[][] couleursFond, Font[][] polices, int[] ajustification) {
    setPolices(polices);
    setCouleurs(couleurs);
    setCouleursFond(couleursFond);
    setJustification(ajustification);
  }
  
  /**
   * Traite une cellule
   */
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    if (justification != null) {
      setHorizontalAlignment(justification[column]);
    }
    if (polices != null) {
      if ((polices[row].length - 1) < column) {
        c.setFont(polices[row][0]);
      }
      else {
        c.setFont(polices[row][column]);
      }
    }
    
    // Uniquement si cellules non sélectionnées
    if (!isSelected) {
      if (couleurs != null) {
        if ((couleurs[row].length - 1) < column) {
          c.setForeground(couleurs[row][0]);
        }
        else {
          c.setForeground(couleurs[row][column]);
        }
      }
      if (couleursFond != null) {
        if ((couleursFond[row].length - 1) < column) {
          c.setBackground(couleursFond[row][0]);
        }
        else {
          c.setBackground(couleursFond[row][column]);
        }
      }
    }
    
    return c;
  }
  
  /**
   * Initialise les polices
   * @param polices the polices to set
   */
  public void setPolices(Font[][] polices) {
    this.polices = polices;
  }
  
  /**
   * Initialise les couleurs du texte
   * @param couleurs the couleurs to set
   */
  public void setCouleurs(Color[][] couleurs) {
    this.couleurs = couleurs;
  }
  
  /**
   * Initialise les couleurs du fond
   * @param couleursFond the couleursFond to set
   */
  public void setCouleursFond(Color[][] couleursFond) {
    this.couleursFond = couleursFond;
  }
  
  /**
   * Initialisation du tableau des justifications
   * @param ajustification
   */
  public void setJustification(int[] ajustification) {
    justification = ajustification;
  }
}
