/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.beans.PropertyEditorSupport;

/**
 * Gère la saisie de la variable typeSaisie de RiCalendrier.
 */
public class TypeSaisieEditor extends PropertyEditorSupport {
  private String[] options = { RiCalendrier.JJMMAA, RiCalendrier.MMAA };
  private int[] values = { RiCalendrier.SAISIE_JJMMAA, RiCalendrier.SAISIE_MMAA };
  protected int typeSaisie = RiCalendrier.SAISIE_JJMMAA;
  
  @Override
  public String getJavaInitializationString() {
    return "" + typeSaisie;
  }
  
  @Override
  public void setValue(Object obj) {
    typeSaisie = ((Integer) obj).intValue();
  }
  
  @Override
  public Object getValue() {
    return Integer.valueOf(typeSaisie);
  }
  
  @Override
  public String[] getTags() {
    return options;
  }
  
  @Override
  public String getAsText() {
    int valeur = ((Integer) getValue()).intValue();
    for (int i = 0; i < values.length; i++) {
      if (values[i] == valeur) {
        return options[i];
      }
    }
    return options[0];
  }
  
  @Override
  public void setAsText(String s) {
    for (int i = 0; i < options.length; i++) {
      if (options[i].equals(s)) {
        setValue(values[i]);
        return;
      }
    }
  }
}
