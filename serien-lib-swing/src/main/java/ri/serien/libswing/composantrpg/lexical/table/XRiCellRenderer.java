/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Permet de personnaliser une cellule de JTable.
 * http://www.developpez.net/forums/d1136731/java/interfaces-graphiques-java/debuter/saisie-date-jtable/
 */
public class XRiCellRenderer extends DefaultTableCellRenderer {
  
  // Variables
  private int[] justification = null;
  private Color[][] couleursFond = null;
  private Color[][] couleurs = null;
  private Font[][] polices = null;
  
  /**
   * Constructeur.
   */
  public XRiCellRenderer() {
  }
  
  /**
   * Constructeur.
   */
  public XRiCellRenderer(Color[][] couleurs, Color[][] couleursFond, Font[][] polices, int[] ajustification) {
    setPolices(polices);
    setCouleurs(couleurs);
    setCouleursFond(couleursFond);
    setJustification(ajustification);
  }
  
  /**
   * Traite une cellule.
   */
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    if (justification != null) {
      setHorizontalAlignment(justification[column]);
    }
    if (polices != null) {
      if ((polices[row].length - 1) < column) {
        c.setFont(polices[row][0]);
      }
      else {
        c.setFont(polices[row][column]);
      }
    }
    
    // Uniquement si cellules non sélectionnées
    if (!isSelected) {
      if (couleurs != null) {
        if ((couleurs[row].length - 1) < column) {
          if (couleurs[row][0] != null) {
            c.setForeground(couleurs[row][0]);
          }
          else {
            c.setForeground(UIManager.getColor("Table.foreground"));
          }
        }
        else {
          if (couleurs[row][column] != null) {
            c.setForeground(couleurs[row][column]);
          }
          else {
            c.setForeground(UIManager.getColor("Table.foreground"));
          }
        }
      }
      if (couleursFond != null) {
        if ((couleursFond[row].length - 1) < column) {
          if (couleursFond[row][0] != null) {
            c.setBackground(couleursFond[row][0]);
          }
          else {
            c.setBackground(UIManager.getColor("Table.background"));
          }
        }
        else {
          if (couleursFond[row][column] != null) {
            c.setBackground(couleursFond[row][column]);
          }
          else {
            c.setBackground(UIManager.getColor("Table.background"));
          }
        }
      }
    }
    
    return c;
  }
  
  public void setPolices(Font[][] polices) {
    this.polices = polices;
  }
  
  public void setCouleurs(Color[][] couleurs) {
    this.couleurs = couleurs;
  }
  
  public void setCouleursFond(Color[][] couleursFond) {
    this.couleursFond = couleursFond;
  }
  
  public void setJustification(int[] ajustification) {
    justification = ajustification;
  }
}
