/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JComponent;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;
import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Personnalisation du composant XRiTable.
 */
public class XRiPasswordField extends JPasswordField implements IXRiComposant {
  // Variables
  private Lexical lexique = null;
  // Liste des hosfield en E/S utilisés par cet objet
  private oData hostField = null;
  // Liste des buffers constinuant le panel final (à voir si on peut s'en passer en gérant mieux les setData)
  private HashMap<String, oRecord> listeRecord = null;
  // Interpréteur de variables (à voir si on peut pas le récupérer avec lexical)
  private iData interpreteurD = null;
  // <- ne sert pas ici car fait le lien entre le hostfield et le composant
  private ConcurrentHashMap<oData, Object> listeoDataFlux = null;
  // Stocke l'objet qui a le focus
  private JComponent[] requestcomposant = null;

  /**
   * Constructeur.
   */
  public XRiPasswordField() {
    super();
  }

  public XRiPasswordField(String text) {
    super(text);
  }

  public XRiPasswordField(int columns) {
    super(columns);
  }

  public XRiPasswordField(String text, int columns) {
    super(text, columns);
  }

  public XRiPasswordField(Document doc, String txt, int columns) {
    super(doc, txt, columns);
  }

  @Override
  public String getText() {
    return new String(getPassword());
  }

  /**
   * Methode de oFrame un peu adaptées.
   * Stocke les variables qui composent le composant.
   */
  @Override
  public void init(final Lexical lexique, HashMap<String, oRecord> alisteRecord, iData ainterpreteurD,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, JComponent[] arequestcomposant) {
    // Initialisation des variables nécessaires
    this.lexique = lexique;
    setInterpreteurD(ainterpreteurD);
    setListeRecord(alisteRecord);
    setListeoDataFlux(alisteoDataFlux);
    requestcomposant = arequestcomposant;
  }

  /**
   * Initialise les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    hostField = lexique.getHostField(getName());
    if (hostField == null) {
      return;
    }

    if (hostField.getEvalVisibility() == Constantes.OFF) {
      setVisible(false);
    }
    else {
      setVisible(true);
      if (!hostField.isAlpha()) {
        this.setHorizontalAlignment(SwingConstants.RIGHT);
      }
      setText(hostField.getValeurToFrameWithTrimR());
      // On vérifie si le oData est en E/S
      if (lexique.isIntoCurrentRecord(hostField) && (hostField.getEvalReadOnly() == Constantes.OFF)) {
        // Filtre de saisie
        ((AbstractDocument) this.getDocument()).setDocumentFilter(new SaisieDefinition(hostField.getLongueurFormatee(),
            !hostField.isLowerCase(), hostField.getCaractereAutorises(), hostField.isAlpha()));
        listeoDataFlux.put(hostField, this); // Nécessaire pour le getData de oFrame
        setEnabled(true);
        if (hostField.getEvalFocus() == Constantes.ON) {
          requestcomposant[0] = this;
        }
      }
      else {
        setEnabled(false);
      }
    }
  }

  /**
   * Met à jour le oData du flux courant.
   */
  @Override
  public void getData() {
    if (!isVisible()) {
      return;
    }
    // if (HostField != null)
    hostField.setValeurFromFrame(this.getText());
  }

  // --> Accesseurs <--------------------------------------------------------

  public void setListeRecord(HashMap<String, oRecord> listeRecord) {
    this.listeRecord = listeRecord;
  }

  public HashMap<String, oRecord> getListeRecord() {
    return listeRecord;
  }

  public iData getInterpreteurD() {
    return interpreteurD;
  }

  public void setInterpreteurD(iData interpreteurD) {
    this.interpreteurD = interpreteurD;
  }

  public ConcurrentHashMap<oData, Object> getListeoDataFlux() {
    return listeoDataFlux;
  }

  public void setListeoDataFlux(ConcurrentHashMap<oData, Object> listeoDataFlux) {
    this.listeoDataFlux = listeoDataFlux;
  }

  @Override
  public void dispose() {
    ((AbstractDocument) this.getDocument()).setDocumentFilter(null);
    lexique = null;
    hostField = null;
    listeRecord = null;
    interpreteurD = null;
    listeoDataFlux = null;
    requestcomposant = null;
  }
}
