/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Gestion des fenêtres CPF AS400
 * ==> Tiré de la classe DialogError mais utilisation de JDialog
 * ==> Ajout d'un bouton pour mettre le message du CPF dans le presse papier
 */
public class DialogErrorCPF {
  
  // Constantes
  private static final int NBR_CARACTERES_MAX = 80;
  private static final int TYPE_AUCUN_MESSAGE = 0;
  private static final int TYPE_MESSAGE_ERREUR = 1;
  private static final int TYPE_MESSAGE_VERROUILLAGE = 2;
  
  // Variables
  private String title = "Erreur";
  private String[] infos = null;
  private ComboItem intemParDefaut = null;
  private String reponseParDefaut = null;
  private int typeMessage = TYPE_AUCUN_MESSAGE;
  
  public DialogErrorCPF(JFrame parent, String atitle, String[] informations) {
    initComponents();
    ((java.awt.Frame) dialogError.getOwner()).setIconImage(parent.getIconImage());
    title = atitle;
    infos = informations;
    
    // On fixe la réponse par défaut
    setReponseParDefaut("C");
    // Il s'agit d'un verrouillage
    if ((infos.length > 0) && (infos[0] != null) && (infos[0].endsWith("" + Constantes.TYPE_WRNG_SIMPLE))) {
      comboReponse.setVisible(false);
      typeMessage = TYPE_MESSAGE_VERROUILLAGE;
      formateMessageVerrouillage(infos);
    }
    // Il s'agit d'une erreur CPF
    else {
      typeMessage = TYPE_MESSAGE_ERREUR;
      formateDialogError(infos);
    }
    dialogError.setTitle(title);
    bt_Valider.requestFocusInWindow();
    dialogError.getRootPane().setDefaultButton(bt_Valider);
    dialogError.getContentPane().setBackground(SNCharteGraphique.COULEUR_FOND);
    dialogError.setPreferredSize(new Dimension(520, 535));
    comboReponse.setSelectedItem(getIntemParDefaut());
    dialogError.pack();
    dialogError.setVisible(true);
  }
  
  /**
   * Construit et affiche la boite de dialogue des erreurs CPF
   *
   * @param chaine
   * @param titre
   * @return
   */
  public void formateDialogError(String[] infos) {
    // On découpe la chaine en fonction des marqueurs &N
    String[] texte = infos[4].split("&N");
    
    // On extrait les réponses possibles
    StringBuilder reponsehtml = new StringBuilder();
    for (int i = 0; i < texte.length; i++) {
      if (!texte[i].startsWith(" Réponses possibles")) {
        continue;
      }
      
      String[] listeReponse = texte[i].split("&B");
      
      if (listeReponse.length > 0) {
        // On met la section dans le texte
        texte[i] = "<b>" + listeReponse[0] + "</b>";
        
        // Remplir le tableau des valeurs possibles et le texte des réponses possibles
        for (int j = 1; j < listeReponse.length; j++) {
          String reponse = listeReponse[j];
          int posd = 0;
          int posf = NBR_CARACTERES_MAX;
          boolean fin = false;
          reponsehtml.append("<div style=\"text-indent:-10px; margin-left:10px;\">");
          do {
            if (posf < reponse.length()) {
              // On cherche un blanc afin d'éviter de tronquer un mot
              while (posf < reponse.length() && reponse.charAt(posf) != ' ') {
                posf++;
              }
              reponsehtml.append(reponse.substring(posd, posf)).append("<br>");
              posd = posf;
              posf += NBR_CARACTERES_MAX;
            }
            else {
              reponsehtml.append(reponse.substring(posd));
              fin = true;
            }
          }
          while (!fin);
          reponsehtml.append("</div>");
          
          // Remplir la liste déroulante avec la lettre de réponse en valeur de retour et le texte descriptif en
          // affichage
          int positionTiret = reponse.indexOf("--");
          if (positionTiret > 0) {
            comboReponse.addItem(new ComboItem(reponse.substring(0, positionTiret).trim(), reponse));
          }
          
          if ((reponse.substring(0, reponse.indexOf("--")).trim()).equalsIgnoreCase(getReponseParDefaut())) {
            setIntemParDefaut((ComboItem) comboReponse.getItemAt(comboReponse.getItemCount() - 1));
          }
          
        }
        
        reponsehtml.append("<br>");
      }
    }
    
    // On construit la boite de dialogue
    StringBuilder sb = new StringBuilder();
    sb.append("<html><b> ID message . . :   </b>").append(infos[1]).append("<br><b> Gravité . . . . . :   </b>").append(infos[2])
        .append("<br><b> Date . . . . . . . :   </b>").append(infos[5]).append("<br><b> Travail . . . . :   </b>").append(infos[0])
        .append("<br><br><b>").append(infos[3]).append("</b>");
    for (int i = 0; i < texte.length; i++) {
      sb.append(texte[i]).append("<br>");
    }
    sb.append(reponsehtml).append("</html>");
    
    // On intègre le stringBuffer dans l'étiquette d'affichage
    message.setText(sb.toString());
  }
  
  /**
   * Construit et affiche la boite de dialogue des verrouillages.
   */
  public void formateMessageVerrouillage(String[] infos) {
    StringBuilder sb = new StringBuilder();
    
    sb.append("<html>");
    for (int i = 1; i < infos.length; i++) {
      sb.append(infos[i]).append("<br>");
    }
    sb.append("</html>");
    
    // On intègre le stringBuffer dans l'étiquette d'affichage
    message.setText(sb.toString());
  }
  
  private void bt_ValiderActionPerformed(ActionEvent e) {
    dialogError.dispose();
  }
  
  /**
   * Copie du message dans le presse-papier système
   */
  private void bt_CopierMessageActionPerformed(ActionEvent e) {
    String texteSansBalisesHTML = message.getText().replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ");
    StringSelection selection = new StringSelection(texteSansBalisesHTML);
    Toolkit toolKit = Toolkit.getDefaultToolkit();
    Clipboard cb = toolKit.getSystemClipboard();
    cb.setContents(selection, null);
  }
  
  /**
   * Renvoi la valeur de la réponse sélectionnée par l'utilisateur au message AS400
   */
  public String getReponse() {
    if (typeMessage == TYPE_MESSAGE_ERREUR) {
      if (comboReponse.getSelectedItem() != null) {
        // Retourner la réponse sélectionné par l'utilisateur
        return infos[0] + Constantes.SEPARATEUR_CHAINE + ((ComboItem) comboReponse.getSelectedItem()).getValue();
      }
      else {
        // Retourner la réponse "Cancel" par défaut
        return infos[0] + Constantes.SEPARATEUR_CHAINE + "C";
      }
    }
    return "";
  }
  
  public ComboItem getIntemParDefaut() {
    return intemParDefaut;
  }
  
  public void setIntemParDefaut(ComboItem intemParDefaut) {
    this.intemParDefaut = intemParDefaut;
  }
  
  public String getReponseParDefaut() {
    return reponseParDefaut;
  }
  
  public void setReponseParDefaut(String reponseParDefaut) {
    this.reponseParDefaut = reponseParDefaut;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    dialogError = new JDialog();
    message = new JLabel();
    bt_Valider = new JButton();
    bt_CopierMessage = new JButton();
    comboReponse = new JComboBox();

    //======== dialogError ========
    {
      dialogError.setBackground(new Color(238, 238, 210));
      dialogError.setModal(true);
      dialogError.setAlwaysOnTop(true);
      dialogError.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
      dialogError.setName("dialogError");
      Container dialogErrorContentPane = dialogError.getContentPane();
      dialogErrorContentPane.setLayout(null);

      //---- message ----
      message.setVerticalAlignment(SwingConstants.TOP);
      message.setName("message");
      dialogErrorContentPane.add(message);
      message.setBounds(10, 10, 485, 385);

      //---- bt_Valider ----
      bt_Valider.setText("OK");
      bt_Valider.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Valider.setName("bt_Valider");
      bt_Valider.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_ValiderActionPerformed(e);
        }
      });
      dialogErrorContentPane.add(bt_Valider);
      bt_Valider.setBounds(420, 455, 75, 33);

      //---- bt_CopierMessage ----
      bt_CopierMessage.setText("Copier le message");
      bt_CopierMessage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_CopierMessage.setName("bt_CopierMessage");
      bt_CopierMessage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_CopierMessageActionPerformed(e);
        }
      });
      dialogErrorContentPane.add(bt_CopierMessage);
      bt_CopierMessage.setBounds(10, 455, 145, 33);

      //---- comboReponse ----
      comboReponse.setActionCommand("rep");
      comboReponse.setName("comboReponse");
      dialogErrorContentPane.add(comboReponse);
      comboReponse.setBounds(10, 410, 485, 35);

      dialogErrorContentPane.setPreferredSize(new Dimension(520, 535));
      dialogError.pack();
      dialogError.setLocationRelativeTo(dialogError.getOwner());
    }
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JDialog dialogError;
  private JLabel message;
  private JButton bt_Valider;
  private JButton bt_CopierMessage;
  private JComboBox comboReponse;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}

class ComboItem {
  
  private String value;
  private String label;
  
  public ComboItem(String value, String label) {
    this.value = value;
    this.label = label;
  }
  
  public String getValue() {
    return this.value;
  }
  
  public String getLabel() {
    return this.label;
  }
  
  @Override
  public String toString() {
    return label;
  }
}
