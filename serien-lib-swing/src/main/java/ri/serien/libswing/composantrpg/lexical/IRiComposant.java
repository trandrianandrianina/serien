/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JComponent;

import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Description d'un RiComposant.
 */
public interface IRiComposant {
  public void setLexical(Lexical lexique);
  
  public void setHostField(oData hostfield);
  
  public void setNomFlux(String nomFlux);
  
  public void setNomDernierFlux(String nomflux);
  
  public void setInfosFlux(String anomflux, String anomDernierflux, HashMap<String, oData> alisteoData,
      ConcurrentHashMap<oData, Object> alisteoDataFlux);
  
  public void setInfosFlux(String anomflux, String anomDernierflux, HashMap<String, oData> alisteoData,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, Lexical lexique);
  
  public void setData();
  
  public void getData();
  
  public JComponent getRequestComposant(JComponent composant);
}
