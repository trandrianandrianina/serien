/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;

import javax.swing.JOptionPane;

import org.jdesktop.swingx.JXDatePickerBeanInfo;

/**
 * BeanInfo pour le composant XRiCalendrier.
 */
public class XRiCalendrierBeanInfo extends JXDatePickerBeanInfo {
  @Override
  public PropertyDescriptor[] getPropertyDescriptors() {
    try {
      // Type de saisie du calendrier
      PropertyDescriptor saisie = new PropertyDescriptor("typeSaisie", XRiCalendrier.class);
      saisie.setPropertyEditorClass(TypeSaisieEditor.class);

      // Active la selection complete du calendrier
      PropertyDescriptor selection = new PropertyDescriptor("selectionComplete", XRiCalendrier.class);

      PropertyDescriptor[] superProp = super.getPropertyDescriptors();
      PropertyDescriptor[] prop = new PropertyDescriptor[superProp.length + 2];
      System.arraycopy(superProp, 0, prop, 0, superProp.length);
      prop[prop.length - 2] = saisie;
      prop[prop.length - 1] = selection;
      return prop;
      // return new PropertyDescriptor[]{saisie};
    }
    catch (IntrospectionException e) {
      JOptionPane.showMessageDialog(null, e, "Debug", JOptionPane.ERROR_MESSAGE);
    }
    return null;
  }
}
