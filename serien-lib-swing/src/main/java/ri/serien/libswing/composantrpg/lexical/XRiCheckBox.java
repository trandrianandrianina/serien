/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Color;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * Personnalisation du composant XRiCheckbox.
 */
public class XRiCheckBox extends JCheckBox implements IXRiComposant {
  // Variables
  private Lexical lexique = null;
  private oData hostField = null;
  // Liste des buffers constinuant le panel final (à voir si on peut s'en passer en gérant mieux les setData)
  private HashMap<String, oRecord> listeRecord = null;
  // Interpréteur de variables (à voir si on peut pas le récupérer avec lexical)
  private iData interpreteurD = null;
  // ne sert pas ici car fait le lien entre le hostfield et le composant
  private ConcurrentHashMap<oData, Object> listeoDataFlux = null;

  // Stocke l'objet qui a le focus
  private JComponent[] requestcomposant = null;

  // Variables utilisées dans le getData
  private String selected = "1";
  private String unselected = " ";
  private JPopupMenu clicDroit = null;

  // private XRiModeleCheckbox modele = null;

  /**
   * Constructeur.
   */
  public XRiCheckBox() {
    super();
  }

  /**
   * Methode de oFrame un peu adaptées.
   * Stocke les variables qui composent la table.
   */
  @Override
  public void init(final Lexical lexique, HashMap<String, oRecord> alisteRecord, iData ainterpreteurD,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, JComponent[] arequestcomposant) {
    // Initialisation des variables nécessaires
    this.lexique = lexique;
    setInterpreteurD(ainterpreteurD);
    setListeRecord(alisteRecord);
    setListeoDataFlux(alisteoDataFlux);
    requestcomposant = arequestcomposant;
    clicDroit = getComponentPopupMenu();
  }

  /**
   * Initialise les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    hostField = lexique.getHostField(getName());
    if (hostField == null) {
      return;
    }

    if (hostField.getEvalVisibility() == Constantes.OFF) {
      setVisible(false);
    }
    else {

      // Sélection ou dé-sélection suivant la valeur d'entrée
      setSelected(lexique.HostFieldGetData(getName()).trim().equalsIgnoreCase(selected));
      setVisible(true);
      // On vérifie si le oData est en E/S
      if (lexique.isIntoCurrentRecord(hostField) && (hostField.getEvalReadOnly() == Constantes.OFF)) {
        // Filtre de saisie
        listeoDataFlux.put(hostField, this); // Nécessaire pour le getData de oFrame
        setEnabled(true);
        if (hostField.getEvalFocus() == Constantes.ON) {
          requestcomposant[0] = this;
        }
      }
      else {
        setEnabled(false);
      }
    }

  }

  /**
   * Met à jour le oData du flux courant.
   */
  @Override
  public void getData() {
    if (isVisible()) {
      if (isSelected()) {
        lexique.HostFieldPutData(this.getName(), 0, selected);
      }
      else if (unselected != null) {
        lexique.HostFieldPutData(this.getName(), 0, unselected);
      }
    }
  }

  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    setFocusable(pEnabled);

    // changement de look
    if (pEnabled) {
      setForeground(Color.BLACK);
    }
    else {
      setForeground(Color.DARK_GRAY);
    }

  }

  // -- Méthodes privées

  // -- Accesseurs

  public void setValeursSelection(String sel, String unsel) {
    selected = sel;
    unselected = unsel;
  }

  public void setListeRecord(HashMap<String, oRecord> listeRecord) {
    this.listeRecord = listeRecord;
  }

  public HashMap<String, oRecord> getListeRecord() {
    return listeRecord;
  }

  public iData getInterpreteurD() {
    return interpreteurD;
  }

  public void setInterpreteurD(iData interpreteurD) {
    this.interpreteurD = interpreteurD;
  }

  public ConcurrentHashMap<oData, Object> getListeoDataFlux() {
    return listeoDataFlux;
  }

  public void setListeoDataFlux(ConcurrentHashMap<oData, Object> listeoDataFlux) {
    this.listeoDataFlux = listeoDataFlux;
  }

  @Override
  public void dispose() {
    lexique = null;
    hostField = null;
    listeRecord = null;
    interpreteurD = null;
    listeoDataFlux = null;
    requestcomposant = null;
    clicDroit = null;
  }

}
