/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import javax.swing.JButton;

public interface iRiPanel {
  public JButton getDefautBouton();
  
  public void dispose();
}
