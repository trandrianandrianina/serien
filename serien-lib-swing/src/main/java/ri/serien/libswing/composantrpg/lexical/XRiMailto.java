/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;

import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * Composant Mailto permettant la saisie d'une adresse email et l'envoi vers le mailer.
 */
public class XRiMailto extends JPanel implements IXRiComposant {

  /**
   * Constructeur.
   */
  public XRiMailto() {
    super();
    initComponents();
  }

  /**
   * Methode de oFrame un peu adaptées.
   * Stocke les variables qui composent la table.
   */
  @Override
  public void init(final Lexical lexique, HashMap<String, oRecord> alisteRecord, iData ainterpreteurD,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, JComponent[] arequestcomposant) {
    // Initialisation des variables nécessaires
    tf_AdresseMail.init(lexique, alisteRecord, ainterpreteurD, alisteoDataFlux, arequestcomposant);
  }

  /**
   * Initialise le nom de l'objet.
   */
  @Override
  public void setName(String name) {
    super.setName(name);
    tf_AdresseMail.setName(name);
  }

  /**
   * Initialise le menu contextuel pour la zone de saisie.
   */
  public void setMonClicDroit(JPopupMenu btd) {
    tf_AdresseMail.setComponentPopupMenu(btd);
  }

  /**
   * Retourne le menu contextuel pour la zone de saisie.
   */
  public JPopupMenu getMonClicDroit() {
    return tf_AdresseMail.getComponentPopupMenu();
  }

  /**
   * Initialise les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    tf_AdresseMail.setData();
    setVisible(tf_AdresseMail.isVisible());
  }

  /**
   * Met à jour le oData du flux courant.
   * @todo A voir si c'est pas mieux de le rajouter directement dans le getData du panel.
   */
  @Override
  public void getData() {
    tf_AdresseMail.getData();
  }

  /**
   * Initialise l'adresse email.
   */
  public void setEmail(String adresse) {
    if ((adresse == null) || adresse.trim().equals("") || (adresse.indexOf('@') == -1)) {
      return;
    }

    tf_AdresseMail.setText(adresse.toLowerCase());
  }

  /**
   * Retourne l'adresse email.
   */
  public String getEmail() {
    return tf_AdresseMail.getText().trim().toLowerCase();
  }

  /**
   * Envoi dans le mailer.
   */
  private void mailto() {
    String adresse = getEmail();
    if (adresse.equals("") || (adresse.indexOf('@') == -1)) {
      return;
    }

    Desktop desktop = Desktop.getDesktop();
    URI uri = URI.create("mailto:" + adresse);
    try {
      desktop.mail(uri);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  public XRiTextField getTf_AdresseMail() {
    return tf_AdresseMail;
  }

  public JButton getBt_MailTo() {
    return bt_MailTo;
  }

  public JPanel getP_MailTo() {
    return this;
  }

  private void bt_MailToActionPerformed(ActionEvent e) {
    mailto();
  }

  // CHECKSTYLE:OFF
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bt_MailTo = new JButton();
    tf_AdresseMail = new XRiTextField();
    CellConstraints cc = new CellConstraints();
    
    // ======== this ========
    setOpaque(false);
    setName("this");
    setLayout(new FormLayout(new ColumnSpec[] { FormFactory.MIN_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
        new ColumnSpec(ColumnSpec.FILL, Sizes.PREFERRED, FormSpec.DEFAULT_GROW) }, RowSpec.decodeSpecs("fill:pref")));
    
    // ---- bt_MailTo ----
    bt_MailTo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_MailTo.setToolTipText("Ouvre votre logiciel de messagerie avec l'adresse");
    bt_MailTo.setIcon(new ImageIcon(getClass().getResource("/images/mail_24x15.png")));
    bt_MailTo.setName("bt_MailTo");
    bt_MailTo.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_MailToActionPerformed(e);
      }
    });
    add(bt_MailTo, cc.xy(1, 1));
    
    // ---- tf_AdresseMail ----
    tf_AdresseMail.setName("tf_AdresseMail");
    add(tf_AdresseMail, cc.xy(3, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  protected JButton bt_MailTo;
  protected XRiTextField tf_AdresseMail;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  public void dispose() {
    // TODO Stub de la méthode généré automatiquement

  }

}
