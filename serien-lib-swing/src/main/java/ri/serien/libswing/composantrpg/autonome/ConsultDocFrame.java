/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.SessionTransfert;

/**
 * Affiche une liste de documents et permet de les consulter avec le viewer natif
 */
public class ConsultDocFrame {
  private JFrame jFrame = null; // @jve:decl-index=0:visual-constraint="142,16"
  private JPanel jContentPane = null;
  private JPanel P_Controle = null;
  private JButton BT_Consulter = null;
  private String[] listedoc = null;
  private SessionTransfert trfSession = null;
  private EnvUser infoUser = null;
  private JComboBox CB_ListeDoc = null;
  private boolean supprfichiercache = false;
  
  /**
   * Constructeur
   * @param alistedoc
   * @param ainfoUser
   * @param titre
   */
  public ConsultDocFrame(String[] alistedoc, EnvUser ainfoUser, String titre) {
    // Vérif validité (TODO à améliorer)
    if ((alistedoc == null) || (alistedoc.length <= 1)) {
      JOptionPane.showMessageDialog(null, "Pas de documents disponible.", "Consultation des documents", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    // Filtre sur les documents
    ArrayList<String> temp = new ArrayList<String>(alistedoc.length);
    temp.add(alistedoc[0]);
    for (int i = 1; i < alistedoc.length; i++) {
      if ((alistedoc[i].toLowerCase().lastIndexOf(".gif") == -1) && (alistedoc[i].toLowerCase().lastIndexOf(".jpg") == -1)
          && (alistedoc[i].toLowerCase().lastIndexOf(".png") == -1) && (alistedoc[i].toLowerCase().lastIndexOf(".jpeg") == -1)
          && (alistedoc[i].toLowerCase().lastIndexOf(".bmp") == -1) && (!alistedoc[i].equalsIgnoreCase("Thumbs.db"))) {
        temp.add(alistedoc[i]);
      }
    }
    
    listedoc = new String[temp.size()];
    temp.toArray(listedoc);
    
    // Affichage d'un message comme quoi pas de doc
    if ((listedoc == null) || (listedoc.length == 1)) {
      JOptionPane.showMessageDialog(null, "Pas de documents disponibles.", "Consultation des documents", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    infoUser = ainfoUser;
    trfSession = infoUser.getTransfertSession();
    
    // S'il n'y a qu'un seul doc on affiche pas la fenêtre
    if (listedoc.length == 2) {
      afficheDoc(listedoc[1]);
    }
    else {
      getJFrame();
      jFrame.setTitle(listedoc.length - 1 + " " + titre);
      jFrame.setAlwaysOnTop(true);
      jFrame.setVisible(true);
    }
  }
  
  // A revoir caca
  public ConsultDocFrame(String[] alistedoc, EnvUser ainfoUser, String titre, boolean suppr) {
    setSupprFichierCache(suppr);
    
    // Vérif validité (TODO à améliorer)
    if ((alistedoc == null) || (alistedoc.length <= 1)) {
      JOptionPane.showMessageDialog(null, "Pas de documents disponibles.", "Consultation des documents", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    // Filtre sur les documents
    ArrayList<String> temp = new ArrayList<String>(alistedoc.length);
    temp.add(alistedoc[0]);
    for (int i = 1; i < alistedoc.length; i++) {
      if ((alistedoc[i].toLowerCase().lastIndexOf(".gif") == -1) && (alistedoc[i].toLowerCase().lastIndexOf(".jpg") == -1)
          && (alistedoc[i].toLowerCase().lastIndexOf(".png") == -1) && (alistedoc[i].toLowerCase().lastIndexOf(".jpeg") == -1)
          && (alistedoc[i].toLowerCase().lastIndexOf(".bmp") == -1) && (!alistedoc[i].equalsIgnoreCase("Thumbs.db"))) {
        temp.add(alistedoc[i]);
      }
    }
    
    listedoc = new String[temp.size()];
    temp.toArray(listedoc);
    
    // Affichage d'un message comme quoi pas de doc
    if ((listedoc == null) || (listedoc.length == 1)) {
      JOptionPane.showMessageDialog(null, "Pas de documents disponibles.", "Consultation des documents", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    infoUser = ainfoUser;
    trfSession = infoUser.getTransfertSession();
    
    // S'il n'y a qu'un seul doc on affiche pas la fenêtre
    if (listedoc.length == 2) {
      afficheDoc(listedoc[1]);
    }
    else {
      getJFrame();
      jFrame.setTitle(listedoc.length - 1 + " " + titre);
      jFrame.setAlwaysOnTop(true);
      jFrame.setVisible(true);
    }
  }
  
  /**
   * Affiche un document à partir de son chemin complet sur le serveur
   * @param fichier
   * @param ainfoUser
   * @param suppr
   */
  public ConsultDocFrame(String fichier, EnvUser ainfoUser, boolean suppr) {
    // Affichage d'un message comme quoi pas de doc
    if ((fichier == null) || (fichier.trim().equals(""))) {
      JOptionPane.showMessageDialog(null, "Pas de documents disponibles.", "Consultation des documents", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    setSupprFichierCache(suppr);
    infoUser = ainfoUser;
    trfSession = infoUser.getTransfertSession();
    
    File fichiercache = null;
    LaunchViewer lv = null;
    
    String nomDossierTempUser = infoUser.getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
    String chaine = nomDossierTempUser + File.separatorChar + fichier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_');
    fichiercache = new File(chaine);
    
    if (supprfichiercache) {
      fichiercache.delete();
    }
    // Récupération si nécessaire du fichier en local
    if (!fichiercache.exists()) {
      trfSession.fichierArecup(fichier);
    }
    // Lancment de l'appplication associée
    lv = new LaunchViewer(chaine);
    if (!lv.lancerViewer()) {
      JOptionPane.showMessageDialog(null, lv.getMsgErreur(), "Affichage du document", JOptionPane.WARNING_MESSAGE);
    }
  }
  
  /**
   * Supprime le fichier dans le cache avant récupération
   * @param supprfic
   */
  public void setSupprFichierCache(boolean supprfic) {
    supprfichiercache = supprfic;
  }
  
  /**
   * This method initializes jFrame
   *
   * @return javax.swing.JFrame
   */
  private JFrame getJFrame() {
    if (jFrame == null) {
      jFrame = new JFrame();
      jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      jFrame.setSize(new Dimension(336, 103));
      jFrame.setContentPane(getJContentPane());
    }
    return jFrame;
  }
  
  /**
   * This method initializes jContentPane
   *
   * @return javax.swing.JPanel
   */
  private JPanel getJContentPane() {
    if (jContentPane == null) {
      jContentPane = new JPanel();
      jContentPane.setLayout(new BorderLayout());
      jContentPane.add(getP_Controle(), BorderLayout.CENTER);
      jContentPane.add(getCB_ListeDoc(), BorderLayout.NORTH);
    }
    return jContentPane;
  }
  
  /**
   * This method initializes P_Controle
   *
   * @return javax.swing.JPanel
   */
  private JPanel getP_Controle() {
    if (P_Controle == null) {
      P_Controle = new JPanel();
      P_Controle.setLayout(new FlowLayout());
      P_Controle.add(getBT_Consulter(), null);
    }
    return P_Controle;
  }
  
  /**
   * Affiche un document juste à partir du nom du fichier (pas du chemin complet)
   *
   */
  private void afficheDoc(String doc) {
    File fichiercache = null;
    LaunchViewer lv = null;
    
    String fichier = listedoc[0] + doc.trim();
    String nomDossierTempUser = infoUser.getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
    String chaine = nomDossierTempUser + File.separatorChar + fichier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_');
    fichiercache = new File(chaine);
    
    if (supprfichiercache) {
      fichiercache.delete();
    }
    // Récupération si nécessaire du fichier en local
    if (!fichiercache.exists()) {
      trfSession.fichierArecup(fichier);
    }
    // Lancment de l'appplication associée
    lv = new LaunchViewer(chaine);
    if (!lv.lancerViewer()) {
      JOptionPane.showMessageDialog(null, lv.getMsgErreur(), "Affichage du document", JOptionPane.WARNING_MESSAGE);
    }
  }
  
  /**
   * This method initializes BT_Consulter
   *
   * @return javax.swing.JButton
   */
  private JButton getBT_Consulter() {
    if (BT_Consulter == null) {
      BT_Consulter = new JButton();
      BT_Consulter.setText("Consulter");
      BT_Consulter.addActionListener(new java.awt.event.ActionListener() {
        @Override
        public void actionPerformed(java.awt.event.ActionEvent e) {
          afficheDoc((String) CB_ListeDoc.getItemAt(CB_ListeDoc.getSelectedIndex()));
        }
        
      });
    }
    return BT_Consulter;
  }
  
  /**
   * This method initializes CB_ListeDoc
   *
   * @return javax.swing.JComboBox
   */
  private JComboBox getCB_ListeDoc() {
    if (CB_ListeDoc == null) {
      CB_ListeDoc = new JComboBox();
      
      // Remplissage de la liste
      for (int i = 1; i < listedoc.length; i++) {
        if ((listedoc[i].toLowerCase().indexOf(".gif") != -1) || (listedoc[i].toLowerCase().indexOf(".png") != -1)
            || (listedoc[i].toLowerCase().indexOf(".jpg") != -1) || (listedoc[i].toLowerCase().indexOf(".jpeg") != -1)
            || (listedoc[i].toLowerCase().indexOf(".bmp") != -1)) {
          continue;
        }
        else {
          CB_ListeDoc.addItem(listedoc[i]);
        }
      }
    }
    return CB_ListeDoc;
  }
  
}
