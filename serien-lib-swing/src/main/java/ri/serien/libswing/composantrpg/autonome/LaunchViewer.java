/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.io.File;
import java.io.IOException;

/**
 * Lanche l'application associée au fichier
 */
public class LaunchViewer {
  // Variables
  private File fichier = null;
  private boolean isWindows = false;
  private String commande = "cmd /c start \"\" ";
  private boolean isExist = true;
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /***
   * Constructeur
   */
  public LaunchViewer() {
    ControlWinOS();
  }
  
  /**
   * Constructeur
   * @param file
   */
  public LaunchViewer(File file) {
    setFichier(file);
    ControlWinOS();
  }
  
  /**
   * Constructeur
   * @param file
   */
  public LaunchViewer(String file) {
    this(new File(file));
  }
  
  /**
   * Vérifie que l'OS soit de la famille des windows
   */
  private void ControlWinOS() {
    isWindows = System.getProperty("os.name").startsWith("Windows");
  }
  
  /**
   * Initialise le nom du fichier
   * @param file
   */
  public void setFichier(String file) {
    if (file == null) {
      return;
    }
    setFichier(new File(file));
  }
  
  /**
   * Initialise le nom du fichier
   * @param file
   */
  public void setFichier(File file) {
    if (file == null) {
      return;
    }
    fichier = file;
    isExist = fichier.exists();
  }
  
  /**
   * Lance l'application associée
   * @return
   */
  public boolean lancerViewer() {
    if (fichier == null) {
      msgErreur = "Nom du fichier à null.";
      return false;
    }
    
    if (!isExist) {
      msgErreur = "Le fichier " + fichier.getName() + " n'existe pas dans le dossier " + fichier.getParent();
      return false;
    }
    
    // Dans le cas de Windows, on a une solution de secours
    if (isWindows) {
      if (lancerWin()) {
        return true;
      }
    }
    
    // On vérifie si on peut utiliser Desktop
    if (Desktop.isDesktopSupported()) {
      // On vérifie si on peut utiliser la méthode open de Desktop
      if (Desktop.getDesktop().isSupported(Action.OPEN)) {
        try {
          Desktop.getDesktop().open(fichier);
          return true;
        }
        catch (IOException ex) {
          msgErreur =
              "Aucune application, sur votre poste, n'est en mesure d'ouvrir le fichier. Installez l'application adéquate et recommencez l'opération.";
        }
        catch (Exception ex) {
          ex.printStackTrace();
          msgErreur = ex.toString();
        }
        
      }
      else {
        msgErreur = "La fonction n'est pas supportée par votre système d'exploitation.";
      }
    }
    else {
      msgErreur = "Desktop pas supportée par votre système d'exploitation.";
    }
    
    return false;
  }
  
  /**
   * Lance une application associée au fichier sous windows uniquement
   * @param cmd
   * @return
   */
  private boolean lancerWin() {
    try {
      Runtime.getRuntime().exec(commande + "\"" + fichier.getAbsolutePath() + "\"");
      return true;
    }
    catch (Exception exc) {
      msgErreur = msgErreur + "\n" + exc.toString();
    }
    return false;
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
