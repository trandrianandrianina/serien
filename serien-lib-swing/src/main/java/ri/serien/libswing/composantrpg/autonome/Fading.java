/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;
import javax.swing.Timer;

/**
 * Gère l'effet visuel d'affichage et de sortie d'un panel
 */
public class Fading {
  // Constantes
  public static final int OPEN = 1;
  public static final int CLOSE = 0;
  
  // Variables
  private Timer timer = null;
  private BufferedImage img;
  private BufferedImage fond;
  private float scale = 0;
  private int action = OPEN;
  
  private long startTime = -1;
  private int playTime = 2000;
  public boolean finish = false;
  
  private JComponent component = null;
  
  /**
   * Constructeur
   * @param componant
   */
  public Fading(JComponent component, boolean reset) {
    this.component = component;
    init(reset);
  }
  
  public void fadein(BufferedImage img, BufferedImage fond) {
    setImg(img);
    setFond(fond);
    action = OPEN;
    scale = 0;
    timer.start();
  }
  
  public void fadeout(BufferedImage img, BufferedImage fond) {
    setImg(img);
    setFond(fond);
    action = CLOSE;
    scale = 1;
    timer.start();
  }
  
  private void init(final boolean reset) {
    timer = new Timer(20, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (startTime == -1) {
          startTime = System.currentTimeMillis();
          finish = false;
        }
        else {
          long currentTime = System.currentTimeMillis();
          long diff = currentTime - startTime;
          if (diff >= playTime) {
            diff = 0;
            startTime = -1;
          }
          // On ferme
          if (action == CLOSE) {
            scale -= 0.05f;
            if (scale <= 0) {
              timer.stop();
              if (reset) {
                reset();
              }
              finish = true;
            }
          }
          else {
            // On ouvre
            scale += 0.05f;
            if (scale >= OPEN) {
              timer.stop();
              if (reset) {
                reset();
              }
              finish = true;
            }
          }
        }
        if (component != null) {
          component.repaint();
        }
      }
    });
  }
  
  /**
   * Redéfinition de la méthode de dessin du composant
   * @param g
   */
  public void paintComponent(Graphics g) {
    if (img != null) {
      Graphics2D g2d = (Graphics2D) g.create();
      if (fond != null) {
        g2d.drawImage(fond, 0, 0, component);
      }
      if (scale > 0) {
        AffineTransform at = new AffineTransform();
        at.translate((component.getWidth() / 2) - (img.getWidth() * scale) / 2,
            (component.getHeight() / 2) - (img.getHeight() * scale) / 2);
        at.scale(scale, scale);
        g2d.drawImage(img, at, component);
      }
      g2d.dispose();
    }
  }
  
  private void reset() {
    img = null;
    fond = null;
  }
  
  public void dispose() {
    reset();
    component = null;
    timer = null;
  }
  
  /**
   * @return le img
   */
  public BufferedImage getImg() {
    return img;
  }
  
  /**
   * @param img le img à définir
   */
  public void setImg(BufferedImage img) {
    this.img = img;
  }
  
  /**
   * @return le fond
   */
  public BufferedImage getFond() {
    return fond;
  }
  
  /**
   * @param fond le fond à définir
   */
  public void setFond(BufferedImage fond) {
    this.fond = fond;
  }
  
}
