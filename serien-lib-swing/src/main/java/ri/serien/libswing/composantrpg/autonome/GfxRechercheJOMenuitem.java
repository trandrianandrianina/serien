/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Informations pour une imprimante connectée à un System I contenu dans un menuitem
 */
public class GfxRechercheJOMenuitem extends JPanel {
  /**
   * Constructeur
   * @param aprinter
   */
  public GfxRechercheJOMenuitem() {
    super();
    initComponents();
    setOpaque(false);
  }
  
  private void initComponents() {
    texte = new JLabel();
    chk_Journaux = new JComboBox();
    valider = new JButton();
    
    // ---- this ----
    setName("this");
    setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
    
    // ---- texte ----
    texte.setText("Journaux");
    texte.setPreferredSize(new Dimension(100, 28));
    
    // ---- exemplaire ----
    chk_Journaux.setPreferredSize(new Dimension(250, 28));
    chk_Journaux.setEditable(true);
    
    // ---- icone ----
    valider.setPreferredSize(new Dimension(45, 45));
    valider.setName("valider");
    valider.setText("Ok");
    
    // Dimension d = getPreferredSize();
    // d.setSize(d.width + texte.getPreferredSize().width + exemplaire.getPreferredSize().width +
    // icone.getPreferredSize().width + 20, hauteur);
    // icone.getPreferredSize().width + " " + getPreferredSize().width);
    setPreferredSize(new Dimension(225, 28));
    setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
    add(texte);
    add(chk_Journaux);
    add(valider);
    
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel texte;
  private JComboBox chk_Journaux;
  private JButton valider;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
