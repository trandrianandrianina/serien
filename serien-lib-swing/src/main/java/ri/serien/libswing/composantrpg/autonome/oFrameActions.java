/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.FocusManager;

import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Gestion des actions liées aux touches
 */
public class oFrameActions {
  // Variables
  private Lexical lexique = null;
  private SNPanelEcranRPG thisFrame = null;
  private Action actionF1 = null;
  private Action actionF2 = null;
  private Action actionF3 = null;
  private Action actionF4 = null;
  private Action actionF5 = null;
  private Action actionF6 = null;
  private Action actionF7 = null;
  private Action actionF8 = null;
  private Action actionF9 = null;
  private Action actionF10 = null;
  private Action actionF11 = null;
  private Action actionF12 = null;
  private Action actionF13 = null;
  private Action actionF14 = null;
  private Action actionF15 = null;
  private Action actionF16 = null;
  private Action actionF17 = null;
  private Action actionF18 = null;
  private Action actionF19 = null;
  private Action actionF20 = null;
  private Action actionF21 = null;
  private Action actionF22 = null;
  private Action actionF23 = null;
  private Action actionF24 = null;
  private Action actionENTER = null;
  private Action actionPGUP = null;
  private Action actionPGDOWN = null;
  private Action actionC2 = null;
  
  /**
   * Constructeur
   * @param alexique
   * @param athisFrame
   */
  public oFrameActions(Lexical alexique, SNPanelEcranRPG athisFrame) {
    lexique = alexique;
    thisFrame = athisFrame;
  }
  
  /**
   * Retourne l'action pour la touche voulue
   * @param key
   * @return
   */
  public Action getKeyAction(String key) {
    if (key.equals("F1")) {
      return getActionF1();
    }
    else if (key.equals("F2")) {
      return getActionF2();
    }
    else if (key.equals("F3")) {
      return getActionF3();
    }
    else if (key.equals("F4")) {
      return getActionF4();
    }
    else if (key.equals("F5")) {
      return getActionF5();
    }
    else if (key.equals("F6")) {
      return getActionF6();
    }
    else if (key.equals("F7")) {
      return getActionF7();
    }
    else if (key.equals("F8")) {
      return getActionF8();
    }
    else if (key.equals("F9")) {
      return getActionF9();
    }
    else if (key.equals("F10")) {
      return getActionF10();
    }
    else if (key.equals("F11")) {
      return getActionF11();
    }
    else if (key.equals("F12")) {
      return getActionF12();
    }
    else if (key.equals("F13")) {
      return getActionF13();
    }
    else if (key.equals("F14")) {
      return getActionF14();
    }
    else if (key.equals("F15")) {
      return getActionF15();
    }
    else if (key.equals("F16")) {
      return getActionF16();
    }
    else if (key.equals("F17")) {
      return getActionF17();
    }
    else if (key.equals("F18")) {
      return getActionF18();
    }
    else if (key.equals("F19")) {
      return getActionF19();
    }
    else if (key.equals("F20")) {
      return getActionF20();
    }
    else if (key.equals("F21")) {
      return getActionF21();
    }
    else if (key.equals("F22")) {
      return getActionF22();
    }
    else if (key.equals("F23")) {
      return getActionF23();
    }
    else if (key.equals("F24")) {
      return getActionF24();
    }
    else if (key.equals("ENTER")) {
      return getActionENTER();
    }
    else if (key.equals("PGUP")) {
      return getActionPGUP();
    }
    else if (key.equals("PGDOWN")) {
      return getActionPGDOWN();
    }
    else if (key.equals("C2")) {
      return getActionC2();
    }
    return null;
  }
  
  /**
   * Action pour la touche F1
   * @return
   */
  public Action getActionF1() {
    if (actionF1 == null) {
      actionF1 = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F1");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F1");
          }
        }
      };
    }
    return actionF1;
  }
  
  /**
   * Action pour la touche F2
   * @return
   */
  public Action getActionF2() {
    if (actionF2 == null) {
      actionF2 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F2");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F2");
          }
        }
      };
    }
    return actionF2;
  }
  
  /**
   * Action pour la touche F3
   * @return
   */
  public Action getActionF3() {
    if (actionF3 == null) {
      actionF3 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F3");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F3");
          }
        }
      };
    }
    return actionF3;
  }
  
  /**
   * Action pour la touche F4
   * @return
   */
  public Action getActionF4() {
    if (actionF4 == null) {
      actionF4 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null && !thisFrame.getPopupPerso().isEnvoyerToucheAuRPG("F4")) {
            thisFrame.getPopupPerso().traiterTouche("F4");
          }
          lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
          lexique.HostScreenSendKey(thisFrame, "F4");
        }
      };
    }
    return actionF4;
  }
  
  /**
   * Action pour la touche F5
   * @return
   */
  public Action getActionF5() {
    if (actionF5 == null) {
      actionF5 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F5");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F5");
          }
        }
      };
    }
    return actionF5;
  }
  
  /**
   * Action pour la touche F6
   * @return
   */
  public Action getActionF6() {
    if (actionF6 == null) {
      actionF6 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F6");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F6");
          }
        }
      };
    }
    return actionF6;
  }
  
  /**
   * Action pour la touche F7
   * @return
   */
  public Action getActionF7() {
    if (actionF7 == null) {
      actionF7 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F7");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F7");
          }
        }
      };
    }
    return actionF7;
  }
  
  /**
   * Action pour la touche F8
   * @return
   */
  public Action getActionF8() {
    if (actionF8 == null) {
      actionF8 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F8");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F8");
          }
        }
      };
    }
    return actionF8;
  }
  
  /**
   * Action pour la touche F9
   * @return
   */
  public Action getActionF9() {
    if (actionF9 == null) {
      actionF9 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F9");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F9");
          }
        }
      };
    }
    return actionF9;
  }
  
  /**
   * Action pour la touche F10
   * @return
   */
  public Action getActionF10() {
    if (actionF10 == null) {
      actionF10 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F10");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F10");
          }
        }
      };
    }
    return actionF10;
  }
  
  /**
   * Action pour la touche F11
   * @return
   */
  public Action getActionF11() {
    if (actionF11 == null) {
      actionF11 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F11");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F11");
          }
        }
      };
    }
    return actionF11;
  }
  
  /**
   * Action pour la touche F12
   * @return
   */
  public Action getActionF12() {
    if (actionF12 == null) {
      actionF12 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F12");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F12");
          }
        }
      };
    }
    return actionF12;
  }
  
  /**
   * Action pour la touche F13
   * @return
   */
  public Action getActionF13() {
    if (actionF13 == null) {
      actionF13 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F13");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F13");
          }
        }
      };
    }
    return actionF13;
  }
  
  /**
   * Action pour la touche F14
   * @return
   */
  public Action getActionF14() {
    if (actionF14 == null) {
      actionF14 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F14");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F14");
          }
        }
      };
    }
    return actionF14;
  }
  
  /**
   * Action pour la touche F15
   * @return
   */
  public Action getActionF15() {
    if (actionF15 == null) {
      actionF15 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F15");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F15");
          }
        }
      };
    }
    return actionF15;
  }
  
  /**
   * Action pour la touche F16
   * @return
   */
  public Action getActionF16() {
    if (actionF16 == null) {
      actionF16 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F16");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F16");
          }
        }
      };
    }
    return actionF16;
  }
  
  /**
   * Action pour la touche F17
   * @return
   */
  public Action getActionF17() {
    if (actionF17 == null) {
      actionF17 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F17");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F17");
          }
        }
      };
    }
    return actionF17;
  }
  
  /**
   * Action pour la touche F18
   * @return
   */
  public Action getActionF18() {
    if (actionF18 == null) {
      actionF18 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F18");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F18");
          }
        }
      };
    }
    return actionF18;
  }
  
  /**
   * Action pour la touche F19
   * @return
   */
  public Action getActionF19() {
    if (actionF19 == null) {
      actionF19 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F19");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F19");
          }
        }
      };
    }
    return actionF19;
  }
  
  /**
   * Action pour la touche F20
   * @return
   */
  public Action getActionF20() {
    if (actionF20 == null) {
      actionF20 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F20");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F20");
          }
        }
      };
    }
    return actionF20;
  }
  
  /**
   * Action pour la touche F21
   * @return
   */
  public Action getActionF21() {
    if (actionF21 == null) {
      actionF21 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F21");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F21");
          }
        }
      };
    }
    return actionF21;
  }
  
  /**
   * Action pour la touche F22
   * @return
   */
  public Action getActionF22() {
    if (actionF22 == null) {
      actionF22 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F22");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F22");
          }
        }
      };
    }
    return actionF22;
  }
  
  /**
   * Action pour la touche F23
   * @return
   */
  public Action getActionF23() {
    if (actionF23 == null) {
      actionF23 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F23");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F23");
          }
        }
      };
    }
    return actionF23;
  }
  
  /**
   * Action pour la touche F24
   * @return
   */
  public Action getActionF24() {
    if (actionF24 == null) {
      actionF24 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("F24");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "F24");
          }
        }
      };
    }
    return actionF24;
  }
  
  /**
   * Action pour la touche ENTER
   * @return
   */
  public Action getActionENTER() {
    if (actionENTER == null) {
      actionENTER = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("ENTER");
          }
          else {
            lexique.HostScreenSendKey(thisFrame, "ENTER");
          }
        }
      };
    }
    return actionENTER;
  }
  
  /**
   * Action pour la touche PGUP
   * @return
   */
  public Action getActionPGUP() {
    if (actionPGUP == null) {
      actionPGUP = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("PGUP");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "PGUP");
          }
        }
      };
    }
    return actionPGUP;
  }
  
  /**
   * Action pour la touche PGDOWN
   * @return
   */
  public Action getActionPGDOWN() {
    if (actionPGDOWN == null) {
      actionPGDOWN = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          if (thisFrame.getPopupPerso() != null) {
            thisFrame.getPopupPerso().traiterTouche("PGDOWN");
          }
          else {
            lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
            lexique.HostScreenSendKey(thisFrame, "PGDOWN");
          }
        }
      };
    }
    return actionPGDOWN;
  }
  
  /**
   * Action pour la touche C2
   * @return
   */
  public Action getActionC2() {
    if (actionC2 == null) {
      actionC2 = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          // thisFrame.getProgram().getEnvUserClient().getLog().ecritureMessage("-oFrameActions->key=C2");
          lexique.HostCursorPut(FocusManager.getCurrentManager().getFocusOwner().getName());
          lexique.HostScreenSendKey(thisFrame, "F2");
        }
      };
    }
    return actionC2;
  }
  
  /**
   * Libère les ressources
   */
  public void dispose() {
    lexique = null;
    thisFrame = null;
  }
}
