/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioGeneric;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.composantrpg.lexical.IXRiComposant;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;
import ri.serien.libswing.outil.font.CustomFont;

/**
 * Personnalisation du composant XRiTable.
 *
 * Par défaut la ligne sélectionnée par l'utilisateur n'est pas mémorisée (demande COM-298). Mais il est possible de d'activer la
 * mémorisation lorsque cela est souhaité.
 *
 * @todo A lire pour les pb d'erreur en rouge de swing.
 *       http://www.developpez.net/forums/d835694/java/interfaces-graphiques-java/awt-swing/edt-swingworker/erreurs-swing-lors-maj-rapide-
 *       rafale-jtable/
 *       http://gfx.developpez.com/tutoriel/java/swing/swing-threading/
 */
public class XRiTable extends JTable implements IXRiComposant {
  // Ne pas mettre Courier New car pose soucis dans les listes
  public static final Font POLICE_HEAD_LIST = CustomFont.loadFont("fonts/VeraMono.ttf", 12);
  public static final Font POLICE_LIST = CustomFont.loadFont("fonts/VeraMono.ttf", 12); // notamment VCGM73
  public static final Font POLICE_HEAD_LIST_SS = new Font("SansSerif", Font.PLAIN, 12);
  public static final Font POLICE_LIST_SS = new Font("SansSerif", Font.PLAIN, 12);

  // public static final String PREFIXE_HASH_LIGNE_SELECTIONNEE = "HASH_LIGNE_SELECTIONNEE";
  public static final String PREFIXE_NUM_LIGNE_SELECTIONNEE = "NUM_LIGNE_SELECTIONNEE_";

  // Variables
  private XRiTable cetteTable = this;

  // Liste des noms de hostfield correspondant aux tops de chaque ligne (WTPXX)
  private String[] topsHostField = null;
  // Liste des noms de hostfield correspondant données du titre
  private String[] titleHostField = null;
  // Liste des noms de hostfield correspondant données de la liste
  private String[][] dataHostField = null;
  // Liste des largeurs des colonnes en pixel
  private int[] columnwidth = null;
  // Liste des cellules éditables
  private boolean[][] columnEditable = null;
  // Liste des hostfields pour les données de la liste
  private oData[][] dhostfield = null;

  private int[] justification;
  private Color[][] couleurs;
  private Color[][] couleursFond;
  private Font[][] polices;
  // Contient les lignes qui sont sélectionnables
  private boolean[] topSelectionnables = null;
  // Valeur à mettre par défaut dans les tops lors d'une validation
  private String valeurTopDefaut = "1";
  // Valeur à mettre dans les tops si différent de null (à usage unique) à la place de la valeur par défaut
  private String valeurTop = null;

  private Lexical lexique = null;
  // Liste des hosfield en E/S utilisés par cet objet
  private ArrayList<oData> listeHostField = new ArrayList<oData>();
  // Liste des buffers constinuant le panel final (à voir si on peut s'en passer en gérant mieux les setData)
  private HashMap<String, oRecord> listeRecord = null;
  // Interpréteur de variables (à voir si on peut pas le récupérer avec lexical)
  private iData interpreteurD = null;
  // <- ne sert pas ici mias dans le oFrame car fait le lien entre le hostfield et le composant
  private ConcurrentHashMap<oData, Object> listeoDataFlux = null;
  // Stocke l'objet qui a le focus
  private JComponent[] requestcomposant = null;
  // Défini les cellules (Titre et Valeurs) sur lesquelles on applique la translation table
  private boolean[][] useTranslationTable = null;

  // Variables utilisées dans le majData
  private String[] topvalue = null;
  private String[] titlevalue = null;
  private String[][] datavalue = null;
  private int[] largeurcol = null;
  private XRiModeleTable modele = null;

  private boolean clearTop = true; // Permet momentanément de ne pas effacer les top lors de la validationselection
  private boolean isEditable = false;

  private FontMetrics fm = getFontMetrics(POLICE_LIST);

  // Gestion des couleurs dynamiques
  private HashMap<String, Color> correspondancesCouleurs = null;
  // Tableau de lignes sélectionnées
  private int[] ligneSelectionnees = null;
  // Clé pour le stockage et numéro de la ligne qui a été sélectionnée lors de la validation (afin de pouvoir resélectionner la ligne en
  // revenant dessus)
  private String cleLigneSelectionne = null;
  private int numLigneSelectionnee = -1;
  private boolean memorisationLigneSelectionnee = false;

  private Action actionTabulation = new AbstractAction() {

    @Override
    public void actionPerformed(ActionEvent e) {
      KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
      manager.focusNextComponent();
    }
  };

  /**
   * Constructeur.
   */
  public XRiTable() {
    super();
  }

  // -- Méthodes publiques

  /**
   * Methode de oFrame un peu adaptées.
   * Stocke les variables qui composent la table.
   */
  @Override
  public void init(final Lexical lexique, HashMap<String, oRecord> alisteRecord, iData ainterpreteurD,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, JComponent[] arequestcomposant) {
    // Initialisation des variables nécessaires
    this.lexique = lexique;
    setInterpreteurD(ainterpreteurD);
    setListeRecord(alisteRecord);
    setListeoDataFlux(alisteoDataFlux);
    requestcomposant = arequestcomposant;

    // On interdit le déplacement des colonnes car pour l'instant on ne sait pas mémoriser l'ordre et surtout les réagencer
    getTableHeader().setReorderingAllowed(false);

    // Amélioration de la gestion de la tabulation (permet d'aller sur le composant précédent ou suivant)
    class PreviousFocusHandler extends AbstractAction {
      @Override
      public void actionPerformed(ActionEvent evt) {
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.focusPreviousComponent();
      }
    }

    class NextFocusHandler extends AbstractAction {
      @Override
      public void actionPerformed(ActionEvent evt) {
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.focusNextComponent();
      }
    }

    ActionMap am = cetteTable.getActionMap();
    am.put("selectPreviousColumnCell", new PreviousFocusHandler());
    am.put("selectNextColumnCell", new NextFocusHandler());

    if (getListeners(MouseWheelListener.class).length == 1) {
      // Test Pour éviter les doublons
      return;
    }

    // Permet de rentrer dans une cellule en un seul clic
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        Point p = e.getPoint();
        int col = cetteTable.columnAtPoint(p);
        int row = cetteTable.rowAtPoint(p);
        if (editCellAt(row, col)) {
          Component editor = getEditorComponent();
          editor.requestFocusInWindow();
        }

        // Si on a historique de lignes sélectionnées et que l'on clique Gauche
        if (ligneSelectionnees != null && SwingUtilities.isLeftMouseButton(e)) {
          // On cherche l'historique de selection
          for (int i = 0; i < getLigneSelectionnees().length; i++) {
            int nbLignes = getLigneSelectionnees().length;
            // Si on clique sur l'index historisé on clear et on reselectionne l'index cliqué
            if (getLigneSelectionnees()[i] == getSelectedRow()) {
              int ligneReference = getSelectedRow();
              clearSelection();
              ligneSelectionnees = null;
              if (nbLignes > 1) {
                changeSelection(ligneReference, 0, false, false);
              }
              break;
            }
          }
        }
        // On attribue un historique de sélection à la fucking table
        ligneSelectionnees = getSelectedRows();
      }
    });

    // Ajoute la posssiblité de scroller la liste avec la molette
    addMouseWheelListener(new MouseWheelListener() {
      @Override
      public void mouseWheelMoved(MouseWheelEvent e) {
        lexique.defileListe(cetteTable, e.getWheelRotation());
      }
    });

    // On vérifie l'existence d'un menu contextuel
    final JPopupMenu pop = getComponentPopupMenu();
    if (pop != null) {
      pop.addPopupMenuListener(new PopupMenuListener() {
        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
          // On grise les menus si aucune ligne n'est sélectionnée
          boolean actif = (getSelectedRowCount() > 0);
          Component[] lstmenu = pop.getComponents();
          for (int i = 0; i < lstmenu.length; i++) {
            if (lstmenu[i] instanceof JMenuItem) {
              lstmenu[i].setEnabled(actif);
            }
          }
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
        }
      });
    }
  }

  /**
   * Initialisation pour l'aspect de la table.
   */
  public void setAspectTable(String[] atop, String[] atitle, String[][] adata, int[] acolwidth, boolean editable, int[] ajustification,
      Color[][] acouleurs, Color[][] acouleursFond, Font[][] apolices) {
    setAspectTable(atop, atitle, adata, acolwidth, editable, ajustification, acouleurs, acouleursFond, apolices, true);
  }

  /**
   * Initialisation pour l'aspect de la table.
   */
  public void setAspectTable(String[] atop, String[] atitle, String[][] adata, int[] acolwidth, boolean editable, int[] ajustification,
      Font[][] apolices, final HashMap<String, Color> acorrespondancesCouleurs) {
    correspondancesCouleurs = acorrespondancesCouleurs;
    setAspectTable(atop, atitle, adata, acolwidth, editable, ajustification, null, null, apolices, true);
  }

  /**
   * Initialisation pour l'aspect de la table.
   */
  public void setAspectTable(String[] atop, String[] atitle, String[][] adata, int[] acolwidth, boolean editable, int[] ajustification,
      Color[][] acouleurs, Color[][] acouleursFond, Font[][] apolices, boolean monopsaced) {
    topsHostField = atop;
    titleHostField = atitle;
    dataHostField = adata;
    columnwidth = acolwidth;
    // columnEditable = acoledit;
    isEditable = editable;
    if (dataHostField != null) {
      dhostfield = new oData[dataHostField.length][dataHostField[0].length];
      if (isEditable) {
        columnEditable = new boolean[dataHostField.length][dataHostField[0].length];
        putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
      }
      if (correspondancesCouleurs != null) {
        // On initialise les variables qui recevront les couleurs provenant du RPG
        acouleurs = new Color[dataHostField.length][1];
        acouleursFond = new Color[dataHostField.length][1];
      }
    }

    setJustification(ajustification);
    setCouleurs(acouleurs);
    setCouleursFond(acouleursFond);
    setPolices(apolices);

    ((DefaultTableCellRenderer) this.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.LEFT);
    if (polices == null) {
      // Initialisation par défaut des polices
      if (monopsaced) {
        getTableHeader().setFont(POLICE_HEAD_LIST);
        setFont(POLICE_LIST);
      }
      else {
        getTableHeader().setFont(POLICE_HEAD_LIST_SS);
        setFont(POLICE_LIST_SS);
      }
    }
  }

  /**
   * Efface les informations concernant la mémorisation de la ligne sélectionnée.
   */
  public void effacerMemorisationLigne() {
    lexique.removeVariableGlobaleFromSession(getCleMemorisationLigne());
    numLigneSelectionnee = -1;
  }

  // -- Méthodes privées

  /**
   * Construit la clé pour mémoriser le numéro de la ligne sélectionnée pour ce composant.
   */
  private String getCleMemorisationLigne() {
    if (cleLigneSelectionne == null) {
      cleLigneSelectionne = PREFIXE_NUM_LIGNE_SELECTIONNEE + lexique.getHostField(getName()).getNomFormat() + "_"
          + lexique.getHostField(getName()).getRecord();
    }
    return cleLigneSelectionne;
  }

  /**
   * Récupère les informations éventuelles sur la ligne à sélectionner si elle a été mémorisée.
   */
  private void retournerInfosLigneMemorisee() {
    if (!memorisationLigneSelectionnee) {
      effacerMemorisationLigne();
      return;
    }

    // Récupération éventuelle du numéro de la ligne précédemment sélectionnée (mémorisée)
    Object valeur = lexique.getValeurVariableGlobaleFromSession(getCleMemorisationLigne());
    if (valeur != null) {
      numLigneSelectionnee = (Integer) valeur;
    }
    else {
      numLigneSelectionnee = -1;
    }
  }

  /**
   * Initialisation des touches.
   */
  private void setTouches() {
    new Thread() {
      @Override
      public void run() {
        cetteTable.getInputMap(JComponent.WHEN_FOCUSED).clear();
        cetteTable.getActionMap().clear();

        // Récupération des actions des touches du panel parent
        KeyStroke[] allkey = lexique.getPanel().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).allKeys();
        if (allkey == null) {
          return;
        }

        for (KeyStroke key : allkey) {
          Object obj = lexique.getPanel().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).get(key);
          cetteTable.getInputMap(JComponent.WHEN_FOCUSED).put(key, obj);
          cetteTable.getActionMap().put(obj, lexique.getPanel().getActionMap().get(obj));
        }

        // Touches spécifiques à la liste
        cetteTable.getInputMap(JComponent.WHEN_FOCUSED).put(SNCharteGraphique.TOUCHE_TAB, "TABULATION");
        cetteTable.getActionMap().put("TABULATION", actionTabulation);
      }
    }.start();
  }

  /**
   * Initialise les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    retournerInfosLigneMemorisee();
    setTouches();
    initListeHostField();
    majTable();
    ligneSelectionnees = null;
  }

  /**
   * Met à jour le oData du flux courant.
   * TODO A voir si c'est pas mieux de le rajouter directement dans le getData du panel.
   */
  @Override
  public void getData() {
    validSelection();

    if (!isEditable) {
      return;
    }

    if ((dataHostField == null) || (dataHostField.length == 0)) {
      return;
    }

    oData hostfield = null;
    for (int ligne = 0; ligne < dataHostField.length; ligne++) {
      for (int col = 0; col < dataHostField[ligne].length; col++) {
        if (modele.isCellEditable(ligne, col)) {
          hostfield = lexique.getHostField(dataHostField[ligne][col]);
          hostfield.setValeurFromFrame((String) modele.getValueAt(ligne, col));
        }
      }
    }
  }

  /**
   * Methode de oFrame un peu adaptée.
   * Ajoute à la liste des oData les variables non liée directement à un composant graphique.
   */
  public void majTable() {
    // On met à jour la table dans le thread de l'EDT
    Runnable code = new Runnable() {
      @Override
      public void run() {

        int i = 0;
        int j = 0;
        int col = 0;
        int posdeb = 0;
        int oldlg = 0;
        String chaine = null;
        StringBuffer buffer = new StringBuffer();
        boolean dynamique = false;
        oData hostfield = null;
        LinkedHashMap<String, ioGeneric> listeElement = null;
        int[] largeurcolchar = null;
        int totalchar = 0;
        boolean lignesDejaSelectionnees = false;

        // Recherche du record où sont les données (à Tester avec le VGAM91-B1 GAM45 Tableau de bord premier de la liste, il n'a pas de
        // data)
        // (à améliorer car on pourrait systématiser l'utilisation du nom de la liste puisque c'est le nom d'un hostfield ca doit etre la
        // règle)
        if (dataHostField != null) {
          chaine = getName() == null ? dataHostField[0][0] : getName().toUpperCase();
          for (Map.Entry<String, oRecord> entry : listeRecord.entrySet()) {
            listeElement = entry.getValue().listeElement;
            // On teste avec le nom de la table qui doit toujours avoir comme nom un nom de hostifield présent dans le format
            hostfield = (oData) listeElement.get(chaine);
            if (hostfield != null) {
              break;
            }
          }
        }
        else {
          for (Map.Entry<String, oRecord> entry : listeRecord.entrySet()) {
            listeElement = entry.getValue().listeElement;
            // On teste avec la première valeur le nom de la liste
            hostfield = (oData) listeElement.get(getName());
            if (hostfield != null) {
              break;
            }
          }
        }

        // Pour tous les buffers arrivés, on extrait les données
        if (topsHostField != null) {
          if (topvalue == null) {
            topvalue = new String[topsHostField.length];
            topSelectionnables = new boolean[topsHostField.length];
          }
          // On commence par les tops
          if (listeElement != null) {
            for (i = 0; i < topsHostField.length; i++) {
              hostfield = (oData) listeElement.get(topsHostField[i]);
              if (hostfield != null) {
                // listeoData.put(hostfield.getNom(), hostfield);
                topvalue[i] = hostfield.getValeurToFrame().trim();
                topSelectionnables[i] =
                    ((hostfield.getEvalVisibility() == Constantes.ON) && (hostfield.getEvalReadOnly() == Constantes.OFF));
                // On gère le focus avec l'attribut PC
                if (hostfield.getEvalFocus() == Constantes.ON) {
                  requestcomposant[0] = cetteTable;
                }

              }
            }
          }
        }
        else {
          topvalue = null;
        }

        // Les titres (il faut obligatoirement un tableau non null contenant des valeurs non nulles)
        if (titleHostField != null) {
          // Est-ce un colonnage dynamique ?
          chaine = getValeur(listeElement, titleHostField[0], 0, 0);
          dynamique = ((chaine.indexOf(Constantes.SEPARATEUR_CHAINE_CHAR) != -1) && (titleHostField.length == 1));
          // C'est du dynamique
          if (dynamique) {
            oldlg = titlevalue == null ? 0 : titlevalue.length;

            // TODO amélioration si pas null et longueur identique alors pas de new
            titlevalue = splitString4Table(titlevalue, chaine, Constantes.SEPARATEUR_CHAINE_CHAR);

            modele = titlevalue.length != oldlg ? null : modele;
            // Stockage du nombre de caractères de chaque colonne
            largeurcolchar = new int[titlevalue.length];
            totalchar = 0;
            for (col = 0; col < titlevalue.length; col++) {
              largeurcolchar[col] = titlevalue[col].length() + 1;
              totalchar += largeurcolchar[col];
            }
          }
          else {
            // C'est du fixe
            if (titlevalue == null) {
              titlevalue = new String[titleHostField.length];
            }
            titlevalue[0] = chaine;
            for (j = 1; j < titleHostField.length; j++) {
              titlevalue[j] = getValeur(listeElement, titleHostField[j], 0, j);
            }
          }
        }
        else {
          if ((titlevalue == null) && (dataHostField != null)) {
            titlevalue = new String[dataHostField[0].length];
            for (int c = 0; i < titlevalue.length; c++) {
              titlevalue[c] = "";
            }
          }
        }

        // On continue avec les données
        if (dataHostField != null) {
          // Test pour l'allocation des variables si nécessaire
          // Si c'est dynamique
          if (dynamique) {
            datavalue = new String[dataHostField.length][titlevalue.length];
          }
          else if (datavalue == null) {
            // Si c'est fixe
            datavalue = new String[dataHostField.length][dataHostField[0].length];
          }

          // On remplit datavalue avec les valeurs
          for (i = 0; i < dataHostField.length; i++) {
            // Traitement des colonnes
            for (j = 0; j < dataHostField[i].length; j++) {
              chaine = getValeur(listeElement, dataHostField[i][j], i + 1, j);
              if (dynamique) {
                // On utilise un stringbuffer pour éviter les erreurs de chaine trop courte...
                buffer.setLength(0);
                buffer.append(chaine);
                // On initialise avec des espaces le buffer si besoin (car sinon caractères carrés)
                int dif = totalchar - chaine.length();
                if (dif > 0) {
                  buffer.append(Constantes.spaces, 0, dif);
                }
                posdeb = 0;
                if (largeurcolchar != null) {
                  for (col = 0; col < largeurcolchar.length; col++) {
                    datavalue[i][col] = buffer.substring(posdeb, posdeb + largeurcolchar[col]);
                    posdeb = posdeb + largeurcolchar[col];
                  }
                }
              }
              else {
                // Si colonne editable alors on la trime
                if (isEditable) {
                  datavalue[i][j] = chaine.trim();
                  hostfield = lexique.getHostField(dataHostField[i][j]);
                  dhostfield[i][j] = hostfield;
                  if ((hostfield != null) && (hostfield.getEvalReadOnly() == Constantes.OFF)
                      && (lexique.getMode() != Lexical.MODE_CONSULTATION)) {
                    listeHostField.add(hostfield);
                    // hostfield:" + hostfield.getNom());
                    columnEditable[i][j] = true;
                  }
                  else {
                    columnEditable[i][j] = false;
                  }
                }
                else {
                  datavalue[i][j] = chaine;
                }
              }
            }
          }
        }
        else {
          datavalue = null;
        }

        // Met à jour les données
        chargeData(dynamique);

        // On récupère ou on calcule la largeur des colonnes
        if (dynamique) {
          // colonnes, à mettre en variable globale
          if (largeurcolchar != null) {
            for (col = 0; col < largeurcolchar.length; col++) {
              largeurcol[col] = fm.stringWidth(titlevalue[col]);
            }
          }
        }
        else if ((columnwidth == null) || (columnwidth.length < largeurcol.length)) {
          for (col = 0; col < getColumnModel().getColumnCount(); col++) {
            largeurcol[col] = getColumnModel().getColumn(col).getPreferredWidth();
          }
        }

        // Justification des cellules des colonnes
        if (isEditable || (justification != null) || (couleurs != null) || (couleursFond != null) || (polices != null)) {
          initColorFromRPG();
          setDefaultRenderer(String.class, new XRiCellRenderer(couleurs, couleursFond, polices, justification));
          if (isEditable) {
            setDefaultEditor(String.class, new XRiTableCellEditor(dhostfield, justification));
          }
        }

        // On défini la largeur des colonnes
        TableColumnModel cm = getColumnModel();
        if ((columnwidth == null) || (columnwidth.length < largeurcol.length)) {
          for (col = 0; col < cm.getColumnCount(); col++) {
            cm.getColumn(col).setPreferredWidth(largeurcol[col]);
          }
        }
        else {
          for (col = 0; col < cm.getColumnCount(); col++) {
            cm.getColumn(col).setPreferredWidth(columnwidth[col]);
          }
        }

        // On s'occupe des sélections
        if (topsHostField != null) {
          for (i = 0; i < topsHostField.length; i++) {
            if (topvalue[i] != null) {
              if (!topvalue[i].equals("")) {
                getSelectionModel().addSelectionInterval(i, i);
                lignesDejaSelectionnees = true;
              }
            }
          }
        }

        // Dans le cas où une ligne sélectionnée a été mémorisée
        if (!lignesDejaSelectionnees && numLigneSelectionnee != -1) {
          oData hostfieldLine = lexique.getHostField(dataHostField[numLigneSelectionnee][0]);
          // Contrôle que la ligne ne soit pas vide
          if (hostfieldLine != null && !hostfieldLine.getValeurToFrame().trim().isEmpty()) {
            getSelectionModel().addSelectionInterval(numLigneSelectionnee, numLigneSelectionnee);
            // Le focus est forcée sur la table si une ligne est mémorisée si le focus n'a pas été mémorisée sur un autre champ
            oData xxlighost = lexique.getHostField("XXLIG");
            oData xxcolhost = lexique.getHostField("XXCOL");
            if (xxlighost == null || xxlighost.getValeurToBuffer().equals("000") || xxcolhost == null
                || xxcolhost.getValeurToBuffer().equals("000")) {
              lexique.HostCursorPut(cetteTable.getName());
            }
          }
        }

        // On met juste un hostfield dans la liste pour que la méthode getData du composant soit appelé, il faut que la
        // liste est le nom de la première zone saisissable de la liste (hors top)
        if (listeHostField.size() > 0) {
          listeoDataFlux.put(listeHostField.get(0), cetteTable);
        }
      }
    };

    // On s'assure qu'il est bien envoyé dans l'EDT
    if (SwingUtilities.isEventDispatchThread()) {
      code.run();
    }
    else {
      SwingUtilities.invokeLater(code);
    }
  }

  /**
   * Double clic qui met à jour les tops des listes avec la valeur souhaitée.
   */
  public boolean doubleClicSelection(MouseEvent e) {
    if ((getSelectedRow() >= 0) && (e.getClickCount() == 2)) {
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          validSelection();
        }
      });
      return true;
    }
    return false;
  }

  /**
   * Permet de choisir les lignes sélectionnables.
   */
  @Override
  public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
    if ((topSelectionnables != null) && (!topSelectionnables[rowIndex])) {
      return;
    }
    else {
      super.changeSelection(rowIndex, columnIndex, toggle, extend);

      // Permet de saisir directement dans la cellule en ce déplaçant avec les flèches
      if (editCellAt(rowIndex, columnIndex)) {
        Component editor = getEditorComponent();
        editor.requestFocusInWindow();
      }
    }
  }

  /**
   * Double clic sur une position particulière dans une liste.
   */
  public boolean doubleClicSelection(int ligne, int col, MouseEvent e) {
    if ((getSelectedRow() >= 0) && (e.getClickCount() == 2)) {
      // On parcourt les lignes sélectionnées
      int[] selected = getSelectedRows();
      if (col > 0) {
        for (int i = 0; i < selected.length; i++) {
          lexique.HostCursorPut(ligne + selected[i], col);
        }
      }
      return true;
    }
    return false;
  }

  /**
   * Retourne si la ligne à l'index indiquée contient des données.
   */
  public boolean isLigneVide(int pIndex) {
    // La ligne est hors tableau ou le tableau contenant les données n'est pas initialisé
    if (datavalue == null || pIndex < 0 || pIndex > datavalue.length) {
      return true;
    }
    // Parcourt de toutes les colonnes de la ligne
    for (int colonne = 0; colonne < datavalue[pIndex].length; colonne++) {
      String valeur = Constantes.normerTexte(datavalue[pIndex][colonne]);
      if (!valeur.isEmpty()) {
        return false;
      }
    }

    return true;
  }

  /**
   * Retourne si la ou les lignes sélectionnées contiennent des données.
   */
  public boolean isLigneSelectioneeVide() {
    // Le tableau contenant les données n'est pas initialisé
    if (datavalue == null) {
      return true;
    }

    int[] listeLigne = getSelectedRows();
    if (listeLigne == null || listeLigne.length == 0) {
      return true;
    }

    // Parcourt de toutes les colonnes des lignes sélectionnées
    for (int ligne : listeLigne) {
      for (int colonne = 0; colonne < datavalue[ligne].length; colonne++) {
        String valeur = Constantes.normerTexte(datavalue[ligne][colonne]);
        if (!valeur.isEmpty()) {
          return false;
        }
      }
    }

    return true;
  }

  // --> Méthodes privées

  /**
   * Retourne la couleur correspondant à un code donné.
   */
  private Color getColor(int indice, String prefixe) {
    // On construit le nom du hostfield
    if (indice < 10) {
      prefixe = prefixe + '0' + indice;
    }
    else {
      prefixe = prefixe + indice;
    }
    // On récupère la valeur envoyée par le RPG
    String valeur = lexique.HostFieldGetData(prefixe);
    if ((valeur == null) || (valeur.trim().equals(""))) {
      return null;
    }

    // On retourne l'équivalence ou null si on ne la trouve pas
    return correspondancesCouleurs.get(valeur);
  }

  /**
   * Initialise les tableaux des couleurs avec les variables provenant du RPG: FG et BG.
   */
  private void initColorFromRPG() {
    if (correspondancesCouleurs == null) {
      return;
    }

    for (int i = 0; i < couleurs.length; i++) {
      couleurs[i][0] = getColor(i + 1, "FG");
      couleursFond[i][0] = getColor(i + 1, "BG");
    }
  }

  /**
   * Retourne la valeur d'un champ de la table.
   */
  private String getValeur(LinkedHashMap<String, ioGeneric> alisteElement, String acle, int l, int c) {
    // if (acle == null) return "";
    oData hostfield = null;
    String chaine;
    boolean ishtml = false;

    hostfield = (oData) alisteElement.get(acle);
    if (hostfield == null) {
      if (acle == null) {
        return "";
      }
      else if (acle.indexOf('@') != -1) {
        // Cas où la valeur peut être @toto@ @titi@
        chaine = useTranslationTable(l, c, interpreteurD.analyseExpression(acle));

        // On cherche s'il y a du code html dans le titre
        ishtml = (chaine.indexOf("<html") != -1);
        if (!ishtml) {
          return useTranslationTable(l, c, interpreteurD.analyseExpression(acle));
        }
        else {
          return transformStringHtml(chaine);
        }
      }
      else {
        return useTranslationTable(l, c, acle);
      }
    }
    else {
      return useTranslationTable(l, c, hostfield.getValeurToFrame());
    }
  }

  /**
   * Converti une chaine à l'aide de la table de translation.
   */
  private String useTranslationTable(int l, int c, String val) {
    if ((useTranslationTable != null) && (useTranslationTable[l][c])) {
      return lexique.TranslationTable(val != null ? val.trim() : val);
    }
    else {
      return val;
    }
  }

  /**
   * Transforme une chaine contenant du html (notamment les espaces).
   */
  private String transformStringHtml(String achaine) {
    char lettre;
    boolean isstring = false;
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < achaine.length(); i++) {
      lettre = achaine.charAt(i);
      if ((!isstring) && (lettre == '>')) {
        // Détection du début de la chaine
        isstring = true;
        sb.append(lettre);
        continue;
      }
      else if ((isstring) && (lettre == '<')) {
        isstring = false;
        sb.append(lettre);
        continue;
      }
      else if ((isstring) && (lettre == ' ')) {
        sb.append("&nbsp;");
      }
      else {
        sb.append(lettre);
      }
    }

    return sb.toString();
  }

  /**
   * Met à jour les tops des listes avec la valeur souhaitée.
   */
  private void validSelection() {
    if (topsHostField == null) {
      return;
    }

    int i;
    oData[] thostfield = new oData[topsHostField.length];
    final String valeur = (valeurTop == null ? valeurTopDefaut : valeurTop);
    // On remet la valeur à null (obligatoire)
    valeurTop = null;

    // On désélectionne et on récupère les hostfields
    for (i = 0; i < topsHostField.length; i++) {
      if (clearTop) {
        lexique.HostFieldPutData(topsHostField[i], 0, "");
      }
      thostfield[i] = lexique.getHostField(topsHostField[i]);
    }
    clearTop = true;

    // Nécessaire lorsque'il y a pagination pour ne pas aller dans la fiche
    final String toucheEnvoyee = lexique.getToucheEnvoyee();
    if (toucheEnvoyee != null && (toucheEnvoyee.equals("PGUP") || toucheEnvoyee.equals("PGDOWN"))) {
      lexique.removeVariableGlobaleFromSession(getCleMemorisationLigne());
      return;
    }

    // On parcourt les lignes sélectionnées
    int[] selected = getSelectedRows();
    // Si une seule ligne sélectionnée alors on mémorise le hashcode du oData de la ligne
    if (selected.length == 1) {
      lexique.addVariableGlobaleFromSession(getCleMemorisationLigne(), selected[0]);
    }
    for (i = 0; i < selected.length; i++) {
      if (topSelectionnables[selected[i]]) {
        lexique.HostFieldPutData(topsHostField[selected[i]], 0, valeur);
      }
    }
  }

  /**
   * Vide et récupère les hostfield dans les tops s'il y en a.
   */
  private void initListeHostField() {
    listeHostField.clear();
    if (topsHostField != null) {
      for (int i = 0; i < topsHostField.length; i++) {
        listeHostField.add(lexique.getHostField(topsHostField[i]));
      }
    }
  }

  /**
   * Charge la table avec les données et créé le model si nécessaire.
   */
  private void chargeData(boolean dynamique) {
    if (modele == null) {
      modele = new XRiModeleTable(datavalue, titlevalue, columnEditable);
      setModel(modele);
      // Permet d'afficher les séparateurs sous Nimbus
      setIntercellSpacing(new Dimension(1, 1));
      setShowVerticalLines(true);
      largeurcol = new int[titlevalue.length];
    }
    else {
      // On charge le titre (à cause des autres vues)
      if (titlevalue != null) {
        for (int col = 0; col < titlevalue.length; col++) {
          modele.setTitleAt(titlevalue[col], col);
        }
      }

      // On charge les données
      if (datavalue != null) {
        for (int row = 0; row < datavalue.length; row++) {
          for (int col = 0; col < datavalue[row].length; col++) {
            modele.setValueAt(datavalue[row][col], row, col);
          }
        }
      }
    }
  }

  /**
   * Retourne une liste généré depuis une chaine.
   */
  private String[] splitString4Table(String[] liste, String chaine, char separateur) {
    int position = 0;
    int i = 0;
    ArrayList<String> vlisting = new ArrayList<String>(50);

    position = chaine.indexOf(separateur, i);
    while (position != -1) {
      if (i == position) {
        vlisting.add("");
      }
      else {
        vlisting.add(chaine.substring(i, position));
      }
      i = position + 1;
      position = chaine.indexOf(separateur, i);
    }
    vlisting.add(chaine.substring(i));

    // On recopie les données du Vector vers le tableau liste (moins la derniere colonne)
    if ((liste == null) || (liste.length != vlisting.size() - 1)) {
      liste = new String[vlisting.size() - 1];
    }
    for (i = 0; i < vlisting.size() - 1; i++) {
      liste[i] = vlisting.get(i);
    }

    return liste;
  }

  // -- Accesseurs

  public void setListeRecord(HashMap<String, oRecord> listeRecord) {
    this.listeRecord = listeRecord;
  }

  public HashMap<String, oRecord> getListeRecord() {
    return listeRecord;
  }

  public iData getInterpreteurD() {
    return interpreteurD;
  }

  public void setInterpreteurD(iData interpreteurD) {
    this.interpreteurD = interpreteurD;
  }

  public int[] getJustification() {
    return justification;
  }

  public void setJustification(int[] justification) {
    this.justification = justification;
  }

  public Color[][] getCouleurs() {
    return couleurs;
  }

  public void setCouleurs(Color[][] couleurs) {
    this.couleurs = couleurs;
  }

  public Color[][] getCouleursFond() {
    return couleursFond;
  }

  public void setCouleursFond(Color[][] couleursFond) {
    this.couleursFond = couleursFond;
  }

  public Font[][] getPolices() {
    return polices;
  }

  public void setPolices(Font[][] polices) {
    this.polices = polices;
  }

  public void setValeurTopDefaut(String valeurTopDefaut) {
    this.valeurTopDefaut = valeurTopDefaut;
  }

  public String getValeurTopDefaut() {
    return valeurTopDefaut;
  }

  public void setValeurTop(String valeurTop) {
    this.valeurTop = valeurTop;
  }

  public String getValeurTop() {
    return valeurTop;
  }

  public ConcurrentHashMap<oData, Object> getListeoDataFlux() {
    return listeoDataFlux;
  }

  public void setListeoDataFlux(ConcurrentHashMap<oData, Object> listeoDataFlux) {
    this.listeoDataFlux = listeoDataFlux;
  }

  public void setUseTranslationTable(boolean[][] auseTranslationTable) {
    this.useTranslationTable = auseTranslationTable;
  }

  public boolean[][] getUseTranslationTable() {
    return useTranslationTable;
  }

  public boolean isClearTop() {
    return clearTop;
  }

  public void setClearTop(boolean clearTop) {
    this.clearTop = clearTop;
  }

  public int[] getLigneSelectionnees() {
    return ligneSelectionnees;
  }

  public void setMemorisationLigneSelectionnee(boolean memorisationLigneSelectionnee) {
    this.memorisationLigneSelectionnee = memorisationLigneSelectionnee;
  }

  @Override
  public void dispose() {
    // if (isEditable){
    // setDefaultEditor(String.class, new XRiTableCellEditor(dhostfield, justification));
    // }
    cetteTable = null;
    topsHostField = null;
    titleHostField = null;
    dataHostField = null;
    columnwidth = null;
    columnEditable = null;
    dhostfield = null;
    justification = null;
    couleurs = null;
    couleursFond = null;
    polices = null;
    topSelectionnables = null;
    lexique = null;
    // listeHostField.clear();
    listeHostField = null;
    listeRecord = null;
    interpreteurD = null;
    listeoDataFlux = null;
    requestcomposant = null;
    useTranslationTable = null;

    topvalue = null;
    titlevalue = null;
    datavalue = null;
    largeurcol = null;
    modele = null;

    fm = null;
    correspondancesCouleurs = null;
  }
}
