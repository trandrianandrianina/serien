/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Color;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * Personnalisation du composant XRiCheckbox.
 */
public class XRiRadioButton extends JRadioButton implements IXRiComposant {
  // Variables
  private Lexical lexique = null;
  private oData hostField = null;
  // Liste des buffers constinuant le panel final (à voir si on peut s'en passer en gérant mieux les setData)
  private HashMap<String, oRecord> listeRecord = null;
  // Liste des hostfield (mais voir si ça fait pas doublons avec listeHostField)
  private HashMap<String, oData> listeoData = null;
  // Interpréteur de variables (à voir si on peut pas le récupérer avec lexical)
  private iData interpreteurD = null;
  // <- ne sert pas ici car fait le lien entre le hostfield et le composant
  private ConcurrentHashMap<oData, Object> listeoDataFlux = null;
  // Stocke l'objet qui a le focus
  private JComponent[] requestcomposant = null;

  // Variables utilisées dans le getData
  private String selected = "X";
  private String namehostfieldgroup = null;
  private ButtonGroup hostfieldgroup = null;
  private JPopupMenu clicDroit = null;

  /**
   * Constructeur.
   */
  public XRiRadioButton() {
    super();

  }

  /**
   * Methode de oFrame un peu adaptées.
   * Stocke les variables qui composent la table.
   */
  @Override
  public void init(final Lexical lexique, HashMap<String, oRecord> alisteRecord, iData ainterpreteurD,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, JComponent[] arequestcomposant) {
    // Initialisation des variables nécessaires
    this.lexique = lexique;
    setInterpreteurD(ainterpreteurD);
    setListeRecord(alisteRecord);
    setListeoDataFlux(alisteoDataFlux);
    requestcomposant = arequestcomposant;
    clicDroit = getComponentPopupMenu();
  }

  /**
   * Initialise les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    String chaine;

    hostField = lexique.getHostField(getName());
    if (hostField == null) {
      return;
    }

    if (hostField.getEvalVisibility() == Constantes.OFF) {
      setVisible(false);
    }
    else {
      if (namehostfieldgroup != null) {
        setSelected(lexique.HostFieldGetData(namehostfieldgroup).trim().equalsIgnoreCase(selected));
      }
      else if (hostfieldgroup != null) {
        chaine = lexique.HostFieldGetData(getName()).trim();
        Enumeration<AbstractButton> listeRadio = hostfieldgroup.getElements();
        while (listeRadio.hasMoreElements()) {
          XRiRadioButton radio = (XRiRadioButton) listeRadio.nextElement();
          radio.setSelected(chaine.equals(radio.getValeur()));
          // if (radio.isSelected()) break; Commenté le 01/04/14 car source de bug
        }
      }
      else {
        setSelected(lexique.HostFieldGetData(getName()).trim().equalsIgnoreCase(selected));
      }
      setVisible(true);
      // On vérifie si le oData est en E/S
      if (lexique.isIntoCurrentRecord(hostField) && (hostField.getEvalReadOnly() == Constantes.OFF)) {
        // Filtre de saisie
        listeoDataFlux.put(hostField, this); // Nécessaire pour le getData de oFrame
        setEnabled(true);
        if (hostField.getEvalFocus() == Constantes.ON) {
          requestcomposant[0] = this;
        }
      }
      else {
        setEnabled(false);
      }
    }

  }

  /**
   * Met à jour le oData du flux courant.
   * TODO A voir si c'est pas mieux de le rajouter directement dans le getData du panel.
   */
  @Override
  public void getData() {
    if (isVisible()) {
      if (namehostfieldgroup != null) {
        if (hostField != null) {
          hostField.setValeurFromFrame(" ");
        }
        if (isSelected()) {
          lexique.HostFieldPutData(namehostfieldgroup, 0, selected);
        }
      }
      else if (hostfieldgroup != null) {
        // Pas besoin d'être selected puisqu'il regroupe l'ensemble des radiobuttons
        Enumeration<AbstractButton> listeRadio = hostfieldgroup.getElements();
        while (listeRadio.hasMoreElements()) {
          XRiRadioButton radio = (XRiRadioButton) listeRadio.nextElement();
          if (radio.isSelected()) {
            lexique.HostFieldPutData(getName(), 0, radio.getValeur());
            break;
          }
        }
      }
      else if (isSelected()) {
        lexique.HostFieldPutData(getName(), 0, selected);
      }
    }
  }

  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    setFocusable(pEnabled);

    // changement de look
    if (pEnabled) {
      setForeground(Color.BLACK);
    }
    else {
      setForeground(Color.DARK_GRAY);
    }
  }

  // --> Méthodes privées <--------------------------------------------------

  // --> Accesseurs <--------------------------------------------------------

  public void setValeurs(String sel) {
    selected = sel;
  }

  public void setValeurs(String sel, String hostfieldg) {
    selected = sel;
    namehostfieldgroup = hostfieldg;
  }

  public void setValeurs(String sel, ButtonGroup grp) {
    selected = sel;
    hostfieldgroup = grp;
  }

  public String getValeur() {
    return selected;
  }

  public void setListeRecord(HashMap<String, oRecord> listeRecord) {
    this.listeRecord = listeRecord;
  }

  public HashMap<String, oRecord> getListeRecord() {
    return listeRecord;
  }

  public HashMap<String, oData> getListeoData() {
    return listeoData;
  }

  public void setListeoData(HashMap<String, oData> listeoData) {
    this.listeoData = listeoData;
  }

  public iData getInterpreteurD() {
    return interpreteurD;
  }

  public void setInterpreteurD(iData interpreteurD) {
    this.interpreteurD = interpreteurD;
  }

  public ConcurrentHashMap<oData, Object> getListeoDataFlux() {
    return listeoDataFlux;
  }

  public void setListeoDataFlux(ConcurrentHashMap<oData, Object> listeoDataFlux) {
    this.listeoDataFlux = listeoDataFlux;
  }

  @Override
  public void dispose() {
    lexique = null;
    hostField = null;
    listeRecord = null;
    listeoData = null;
    interpreteurD = null;
    listeoDataFlux = null;
    requestcomposant = null;
    hostfieldgroup = null;
    clicDroit = null;
  }

}
