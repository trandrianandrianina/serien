/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical.table;

import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

/**
 * Personnalisation du modèle pour la XRiTable.
 */
public class XRiModeleTable extends AbstractTableModel {
  
  private Object[][] donnees;
  private String[] titres;
  // private TableModel model;
  private boolean[][] columnEditable;
  
  /**
   * Constructeur.
   */
  public XRiModeleTable(Object[][] donnees, String[] titres, boolean[][] colEdit) {
    this.donnees = donnees;
    this.titres = titres;
    this.columnEditable = colEdit;
  }
  
  @Override
  public int getColumnCount() {
    return titres.length;
  }
  
  @Override
  public int getRowCount() {
    if (donnees == null) {
      return 0;
    }
    return donnees.length;
  }
  
  @Override
  public String getColumnName(int col) {
    if (titres == null) {
      return "";
    }
    return titres[col];
  }
  
  @Override
  public Object getValueAt(int row, int col) {
    return donnees[row][col];
  }
  
  @Override
  public Class<?> getColumnClass(int columnIndex) {
    return String.class;
  }
  
  @Override
  public boolean isCellEditable(int row, int col) {
    if (columnEditable == null) {
      return false;
    }
    else {
      return columnEditable[row][col];
    }
  }
  
  public void setTitleAt(String value, int col) {
    titres[col] = value;
    fireTableCellUpdated(TableModelEvent.HEADER_ROW, col);
  }
  
  @Override
  public void setValueAt(Object value, int row, int col) {
    donnees[row][col] = value;
    fireTableCellUpdated(row, col);
  }
  
  public boolean[][] getColumnEditable() {
    return columnEditable;
  }
  
  public void setColumnEditable(boolean[][] columnEditable) {
    this.columnEditable = columnEditable;
  }
  
}
