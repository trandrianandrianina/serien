/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical.table;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.AbstractDocument;

import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Personnalisation du modèle de l'éditeur de cellule.
 */
public class XRiTableCellEditor extends DefaultCellEditor {
  // Variables
  private oData[][] dhostfield = null;
  private int[] justification = null;
  
  /**
   * Constructeur.
   */
  public XRiTableCellEditor(oData[][] adhostfield, int[] ajustification) {
    super(new JTextField());
    dhostfield = adhostfield;
    justification = ajustification;
  }
  
  @Override
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    final JTextField c = (JTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);
    
    c.setBorder(null);
    // Avec la tabulation auto en zone pleine, c'est pénible
    c.selectAll();
    ((AbstractDocument) c.getDocument()).setDocumentFilter(new SaisieDefinition(dhostfield[row][column].getLongueurFormatee(),
        !dhostfield[row][column].isLowerCase(), dhostfield[row][column].getCaractereAutorises(), dhostfield[row][column].isAlpha()));
    if (justification == null) {
      if (dhostfield[row][column].isAlpha()) {
        c.setHorizontalAlignment(SwingConstants.LEFT);
      }
      else {
        c.setHorizontalAlignment(SwingConstants.RIGHT);
      }
    }
    else {
      c.setHorizontalAlignment(justification[column]);
    }
    
    return c;
  }
  
  /**
   * Nettoyage de la mémoire.
   */
  public void dispose() {
    dhostfield = null;
    justification = null;
    
  }
}
