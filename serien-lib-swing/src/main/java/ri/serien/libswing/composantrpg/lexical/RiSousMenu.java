/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JMenuBar;

public class RiSousMenu extends JMenuBar {
  /**
   * Constructeur.
   */
  public RiSousMenu() {
    super();
    setMargin(new Insets(0, -5, 0, 0));
    setPreferredSize(new Dimension(170, 26));
    
  }
}
