/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 * Micro navigateur internet
 */
public class viewerHTML extends JPanel implements HyperlinkListener, ActionListener {
  
  // Variables
  private JButton homeButton;
  private JTextField urlField;
  private JEditorPane htmlPane;
  private JPanel topPanel = null;
  private URL Url = null;
  private String url = "";
  private String anchor = "";
  
  /**
   * Constructeur
   * @param aurl
   */
  public viewerHTML(String aurl) {
    String[] detail = getDetailUrl(aurl);
    url = detail[0];
    anchor = detail[1];
    try {
      File fichier = new File(url);
      Url = fichier.toURI().toURL();
    }
    catch (Exception e) {
      Url = null;
    }
    setFenetre();
  }
  
  /**
   * Constructeur
   * @param aURL
   */
  public viewerHTML(URL aURL) {
    Url = aURL;
    String[] detail = getDetailUrl(Url.getPath());
    url = detail[0];
    anchor = detail[1];
    setFenetre();
  }
  
  /**
   * Constructeur
   * @param aurl
   * @param visible
   */
  public viewerHTML(String aurl, boolean visible) {
    this(aurl);
    setAdresseVisible(visible);
  }
  
  /**
   * Constructeur
   * @param aURL
   * @param visible
   */
  public viewerHTML(URL aURL, boolean visible) {
    this(aURL);
    setAdresseVisible(visible);
  }
  
  /**
   * Initialise la fenêtre du navigateur
   *
   */
  private void setFenetre() {
    setLayout(new BorderLayout());
    topPanel = new JPanel();
    topPanel.setBackground(Color.lightGray);
    homeButton = new JButton("Home");
    homeButton.addActionListener(this);
    JLabel urlLabel = new JLabel("Adresse:");
    urlField = new JTextField(30);
    urlField.setText(url + anchor);
    urlField.addActionListener(this);
    topPanel.add(homeButton);
    topPanel.add(urlLabel);
    topPanel.add(urlField);
    add(topPanel, BorderLayout.NORTH);
    
    try {
      if (Url != null) {
        htmlPane = new JEditorPane(Url + anchor);
      }
      htmlPane.setEditable(false);
      htmlPane.addHyperlinkListener(this);
      JScrollPane scrollPane = new JScrollPane(htmlPane);
      add(scrollPane, BorderLayout.CENTER);
    }
    catch (IOException ioe) {
      warnUser("Impossible de construire la page pour " + url + ": " + ioe);
    }
  }
  
  /**
   * Retourne le détail de l'URL
   * @return
   */
  private String[] getDetailUrl(String aadresse) {
    String detail[] = { "", "" };
    int pos = aadresse.indexOf('#');
    if (pos != -1) {
      detail[0] = aadresse.substring(0, pos).trim();
      detail[1] = aadresse.substring(pos).trim();
    }
    else {
      detail[0] = aadresse.trim();
    }
    
    return detail;
  }
  
  /**
   * Permet de cacher ou non la zone de saisie de l'adresse du navigateur
   * @param visible
   */
  public void setAdresseVisible(boolean visible) {
    topPanel.setVisible(visible);
  }
  
  /**
   * Initialise la page avec un nouveau lien
   * @param aurl
   * @param aanchor
   */
  public void setInitialiseUrl(URL aurl, String aanchor) {
    try {
      // On positionne sur le lien exact (avec anchois ou pas)
      htmlPane.setPage(aurl + aanchor);
    }
    catch (Exception ioe) {
      warnUser("Impossible de se connecter au lien " + aurl + ": " + ioe);
    }
  }
  
  /**
   * Initialise la page avec un nouveau lien
   * @param aanchor
   */
  public void setInitialiseUrl(String aanchor) {
    setInitialiseUrl(Url, aanchor);
  }
  
  /**
   * Déclenchement de l'action sur un nouveau lien
   */
  @Override
  public void actionPerformed(ActionEvent event) {
    if (event.getSource() == urlField) {
      // On découpe l'url afin de récupérer les infos
      String[] detail = getDetailUrl(urlField.getText());
      url = detail[0];
      anchor = detail[1];
      try {
        File fichier = new File(url);
        Url = fichier.toURI().toURL();
      }
      catch (Exception e) {
        Url = null;
      }
      
      setInitialiseUrl(Url, anchor);
    }
  }
  
  /**
   * ???
   */
  @Override
  public void hyperlinkUpdate(HyperlinkEvent event) {
    if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
      try {
        htmlPane.setPage(event.getURL());
        urlField.setText(event.getURL().toExternalForm());
      }
      catch (IOException ioe) {
        warnUser("Impossible de se connecter au lien " + event.getURL().toExternalForm() + ": " + ioe);
      }
    }
  }
  
  /**
   * Affiche un message d'erreur
   * @param message
   */
  private void warnUser(String message) {
    JOptionPane.showMessageDialog(this, message, "[viewerHTML]", JOptionPane.ERROR_MESSAGE);
  }
}
