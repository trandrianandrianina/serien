/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;

/**
 * Cette classe permet d'instancier une calculatrice de solde partiel: stocker des écritures au débit et au crédit afin
 * d'en déduire un solde partiel
 * @author David Biason
 * @version 1.2
 */
public class CalculatriceSolde extends JFrame {
  
  /* ========================================================= VARIABLES =====================================================*/
  
  /**
   * Instance privée de la fenêtre afin de créer un SINGLETON
   */
  private static CalculatriceSolde instance;
  
  private static final String BOUTON_EFFACER = "Effacer";
  
  /**
   * ce tableau recoit le titre de chaque colonne et la longueur de celle-ci en nb de caractères. (Pour le découpage des
   * String)
   */
  private String[][] header;
  /**
   * cette collection reçoit une liste de String constituant les données à intégrer au tableau
   */
  private ArrayList<ArrayList<String>> donnees = null;
  /**
   * cette collection contient des String NON DECOUPEES constituant les données à intégrer au tableau
   */
  private ArrayList<String> donneesS = null;
  /**
   * Ce tableau est le tableau primitif de données à insérer dans la JTable.
   */
  private String[][] tabDonnees = null;
  /**
   * Ce tableau contient les indices des colonnes sur lesquels les calculs de solde vont se porter .
   */
  private int[] calculs = { 0, 0 };
  
  /**
   * représente le formatage des colonnes du tableau.
   */
  TableColumnModel tcm = null;
  
  /* ========================================================= METHODES ========================================================*/
  
  /**
   * Constructeur lorsque la fenêtre est chargée à vide.
   */
  private CalculatriceSolde(String[][] head, int[] indexs) {
    initComponents();

    header = head;
    adapterColonnes(header);
    calculs[0] = indexs[0];
    calculs[1] = indexs[1];
    donnees = new ArrayList<ArrayList<String>>();
    donneesS = new ArrayList<String>();

    scrollPane1.setBackground(Color.WHITE);

    snBarreBouton.ajouterBouton(BOUTON_EFFACER, 'e', true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    setVisible(true);
    setTitle("Calculette de solde partiel");
  }
  
  /**
   * Constructeur lorsque une sélection de lignes est passée, adapté à SGM ( découpage de la string)
   */
  private CalculatriceSolde(String[][] head, ArrayList<String> liste, int[] indexs) {
    this(head, indexs);
    if (liste != null) {
      donneesS = liste;
      magicArrayList(donneesS, head);
      modifierTable(donnees);
    }
  }
  
  /**
   * permet de contrôler la présence d'un SINGLETON
   */
  public static CalculatriceSolde getInstance(String[][] head, ArrayList<String> liste, int[] indexs) {
    if (null == instance) {
      instance = new CalculatriceSolde(head, liste, indexs);
    }
    else {
      instance.ajouterLignesS(liste);
    }
    
    return instance;
  }
  
  /**
   * permet de fermer la calculatrice
   */
  public static void fermerCalculatriceSolde() {
    if (instance != null) {
      instance.dispose();
    }
  }
  
  /**
   * permet de mettre à jour la JTable avec les nouvelles données
   */
  public void modifierTable(ArrayList<ArrayList<String>> liste) {
    if (liste != null) {
      String[] head_nom = new String[header.length];
      int[] head_taille = new int[header.length];
      
      for (int i = 0; i < header.length; i++) {
        head_nom[i] = header[i][0];
        head_taille[i] = Integer.parseInt(header[i][1]);
      }
      
      convertirDonnees(liste);
      table1.setModel(new DefaultTableModel(tabDonnees, head_nom));
      adapterColonnes(header);
      majTotaux(tabDonnees);
    }
  }
  
  /**
   * Calcule les totaux (débit, crédit et le solde partiel) des écritures sélectionnées
   */
  public void majTotaux(String[][] list) {
    double totalDebit = 0;
    double totalCredit = 0;
    double totalCalcul = 0;
    
    String nb = "";
    
    // Calcul des totaux
    for (int i = 0; i < list.length; i++) {
      nb = list[i][calculs[0]];
      if (nb != null) {
        if (!nb.equals("")) {
          nb = nb.replace(',', '.');
          totalDebit += Double.parseDouble(nb);
        }
      }
      
      nb = list[i][calculs[1]];
      if (nb != null) {
        if (!nb.equals("")) {
          nb = nb.replace(',', '.');
          totalCredit += Double.parseDouble(nb);
        }
      }
      
    }
    
    // mise à jour des zones de totaux
    totalCalcul = totalDebit - totalCredit;
    DecimalFormat df = new DecimalFormat("#0.00#");
    String texte_Total = df.format(totalCalcul);
    
    if (totalCalcul < 0) {
      l_type.setText("Crédit");
      texte_Total = texte_Total.substring(1);
    }
    else {
      l_type.setText("Débit");
    }
    
    debit.setText(df.format(totalDebit));
    credit.setText(df.format(totalCredit));
    total.setText(texte_Total);
  }
  
  /**
   * remet à zéro les données envoyées à la calculatrice
   */
  public void razDonnees() {
    donnees = new ArrayList<ArrayList<String>>();
    donneesS = new ArrayList<String>();
    tabDonnees = new String[12][header.length];
    modifierTable(donnees);
  }
  
  /**
   * Convertit les données (ArrayList) en données exploitables par une JTable (String[][])
   */
  public void convertirDonnees(ArrayList<ArrayList<String>> liste) {
    int nbLignes = 0;
    
    if (liste.size() > 12) {
      nbLignes = liste.size();
    }
    else {
      nbLignes = 12;
    }
    
    tabDonnees = new String[nbLignes][header.length];
    for (int i = 0; i < liste.size(); i++) {
      for (int j = 0; j < header.length; j++) {
        tabDonnees[i][j] = liste.get(i).get(j);
      }
    }
  }
  
  /**
   * Supprime une à plusieurs lignes d'écriture de la calculatrice
   */
  public void supprimerLignes(ArrayList<ArrayList<String>> liste) {
    if (liste != null) {
      int lignes[] = table1.getSelectedRows();
      
      for (int i = 0; i < lignes.length; i++) {
        liste.remove(lignes[i] - i);
      }
      
      modifierTable(liste);
    }
  }
  
  /**
   * Ajoute une à plusieurs lignes d'écriture adapté à SGM
   */
  public void ajouterLignesS(ArrayList<String> listeSupp) {
    this.setVisible(true);
    this.requestFocusInWindow();
    
    if (listeSupp != null) {
      for (int i = 0; i < listeSupp.size(); i++) {
        donneesS.add(listeSupp.get(i));
      }
      
      donnees = new ArrayList<ArrayList<String>>();
      magicArrayList(donneesS, header);
      modifierTable(donnees);
    }
  }
  
  /**
   * Transforme les arrayList primitives (Arraylist<String>) en ArrayList dynamiques (ArrayList<ArrayList<String>>) :
   * Spécial SGM
   */
  public void magicArrayList(ArrayList<String> liste, String[][] head) {
    String colonne = "";
    for (int i = 0; i < liste.size(); i++) {
      ArrayList<String> ligne = new ArrayList<String>();
      int debut = 0;
      int fin = 0;
      for (int j = 0; j < head.length; j++) {
        fin = debut + Integer.parseInt(head[j][1]);
        
        // découpe spéciale du N°pèce
        if (j == 5) {
          colonne = liste.get(i).substring(debut, debut + 8);
        }
        else {
          colonne = liste.get(i).substring(debut, fin);
        }
        
        colonne = colonne.trim();
        debut += Integer.parseInt(head[j][1]);
        ligne.add(colonne);
        
      }
      donnees.add(ligne);
    }
  }
  
  /**
   * Mettre à jour la largeur des colonnes de la table
   */
  public void adapterColonnes(String[][] head) {
    DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
    renderer.setHorizontalAlignment(SwingConstants.RIGHT);
    
    tcm = table1.getColumnModel();
    int longueur = 0;
    for (int i = 0; i < head.length; i++) {
      // retailler la taille des colonnes
      if (i == 5) {
        longueur = ((8 - 1) * 10) + 20;
      }
      else {
        longueur = ((Integer.parseInt(head[i][1]) - 1) * 10) + 20;
      }
      tcm.getColumn(i).setPreferredWidth(longueur);
      // cadrer à droite les valeurs numériques
      if (i > 4) {
        tcm.getColumn(i).setCellRenderer(renderer);
      }
    }
  }
  
  // *********************************************** Getter Setter ****************************************************/
  
  public ArrayList<ArrayList<String>> getDonnees() {
    return donnees;
  }
  
  public ArrayList<String> getDonneesS() {
    return donneesS;
  }
  
  // ************************************************ Evenementiel ****************************************************/
  
  private void menu_supprActionPerformed(ActionEvent e) {
    supprimerLignes(donnees);
  }
  
  private void menu_effActionPerformed(ActionEvent e) {
    razDonnees();
  }
  
  private void menu_quitActionPerformed(ActionEvent e) {
    razDonnees();
    this.dispose();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        razDonnees();
        this.dispose();
      }
      else if (pSNBouton.isBouton(BOUTON_EFFACER)) {
        razDonnees();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // ************************************************* Pas touch -> JFD
  // ***********************************************//
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlEcritures = new SNPanelTitre();
    scrollPane1 = new JScrollPane();
    table1 = new JTable();
    pnlTotaux = new SNPanelTitre();
    lbDebit = new SNLabelChamp();
    debit = new JTextField();
    lbCredit = new SNLabelChamp();
    credit = new JTextField();
    lbSolde = new SNLabelChamp();
    total = new JTextField();
    l_type = new JLabel();
    popupMenu1 = new JPopupMenu();
    menu_suppr = new JMenuItem();
    menu_eff = new JMenuItem();
    menu_quit = new JMenuItem();
    
    // ======== this ========
    setResizable(false);
    setMinimumSize(new Dimension(910, 480));
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        
        // ======== pnlEcritures ========
        {
          pnlEcritures.setTitre("Ecritures s\u00e9lectionn\u00e9es");
          pnlEcritures.setName("pnlEcritures");
          pnlEcritures.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEcritures.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlEcritures.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlEcritures.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEcritures.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== scrollPane1 ========
          {
            scrollPane1.setBackground(Color.white);
            scrollPane1.setPreferredSize(new Dimension(456, 225));
            scrollPane1.setName("scrollPane1");
            
            // ---- table1 ----
            table1.setFont(new Font("Courier New", Font.PLAIN, 14));
            table1.setModel(new DefaultTableModel(
                new Object[][] { { null, "", null, null, null, null, "", null, null },
                    { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                    { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                    { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                    { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                    { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                    { null, null, null, null, null, null, null, null, null }, },
                new String[] { "Jo", "Folio", "Date", "C", "Libell\u00e9", "N\u00b0 pi\u00e8ce", "D\u00e9bit", "Cr\u00e9dit", "Aff" }) {
              Class<?>[] columnTypes = new Class<?>[] { String.class, String.class, String.class, String.class, String.class,
                  String.class, String.class, String.class, String.class };
              boolean[] columnEditable = new boolean[] { false, false, false, false, false, false, false, false, false };
              
              @Override
              public Class<?> getColumnClass(int columnIndex) {
                return columnTypes[columnIndex];
              }
              
              @Override
              public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnEditable[columnIndex];
              }
            });
            {
              TableColumnModel cm = table1.getColumnModel();
              cm.getColumn(0).setResizable(false);
              cm.getColumn(0).setPreferredWidth(30);
              cm.getColumn(1).setResizable(false);
              cm.getColumn(1).setPreferredWidth(50);
              cm.getColumn(2).setResizable(false);
              cm.getColumn(2).setPreferredWidth(76);
              cm.getColumn(3).setResizable(false);
              cm.getColumn(3).setPreferredWidth(20);
              cm.getColumn(4).setResizable(false);
              cm.getColumn(4).setPreferredWidth(130);
              cm.getColumn(5).setResizable(false);
              cm.getColumn(5).setPreferredWidth(60);
              cm.getColumn(6).setResizable(false);
              cm.getColumn(6).setPreferredWidth(90);
              cm.getColumn(7).setResizable(false);
              cm.getColumn(7).setPreferredWidth(90);
              cm.getColumn(8).setResizable(false);
              cm.getColumn(8).setPreferredWidth(30);
            }
            table1.setComponentPopupMenu(popupMenu1);
            table1.setName("table1");
            scrollPane1.setViewportView(table1);
          }
          pnlEcritures.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlEcritures, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTotaux ========
        {
          pnlTotaux.setTitre("Totaux pour cette s\u00e9lection");
          pnlTotaux.setName("pnlTotaux");
          pnlTotaux.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTotaux.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbDebit ----
          lbDebit.setText("Total d\u00e9bit");
          lbDebit.setName("lbDebit");
          pnlTotaux.add(lbDebit, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- debit ----
          debit.setHorizontalAlignment(SwingConstants.RIGHT);
          debit.setFont(new Font("sansserif", Font.PLAIN, 14));
          debit.setMinimumSize(new Dimension(100, 30));
          debit.setPreferredSize(new Dimension(100, 30));
          debit.setMaximumSize(new Dimension(100, 30));
          debit.setEditable(false);
          debit.setName("debit");
          pnlTotaux.add(debit, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbCredit ----
          lbCredit.setText("Total cr\u00e9dit");
          lbCredit.setName("lbCredit");
          pnlTotaux.add(lbCredit, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- credit ----
          credit.setHorizontalAlignment(SwingConstants.RIGHT);
          credit.setMinimumSize(new Dimension(100, 30));
          credit.setPreferredSize(new Dimension(100, 30));
          credit.setMaximumSize(new Dimension(100, 30));
          credit.setEditable(false);
          credit.setFont(new Font("sansserif", Font.PLAIN, 14));
          credit.setName("credit");
          pnlTotaux.add(credit, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbSolde ----
          lbSolde.setText("Total du solde");
          lbSolde.setName("lbSolde");
          pnlTotaux.add(lbSolde, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- total ----
          total.setFont(new Font("sansserif", Font.BOLD, 14));
          total.setHorizontalAlignment(SwingConstants.RIGHT);
          total.setMinimumSize(new Dimension(100, 30));
          total.setPreferredSize(new Dimension(100, 30));
          total.setMaximumSize(new Dimension(100, 30));
          total.setEditable(false);
          total.setName("total");
          pnlTotaux.add(total, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- l_type ----
          l_type.setText("D\u00e9bit");
          l_type.setFont(l_type.getFont().deriveFont(l_type.getFont().getStyle() | Font.ITALIC));
          l_type.setName("l_type");
          pnlTotaux.add(l_type, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlTotaux, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    // ======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");
      
      // ---- menu_suppr ----
      menu_suppr.setText("Supprimer la ligne");
      menu_suppr.setHorizontalAlignment(SwingConstants.LEFT);
      menu_suppr.setName("menu_suppr");
      menu_suppr.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menu_supprActionPerformed(e);
        }
      });
      popupMenu1.add(menu_suppr);
      
      // ---- menu_eff ----
      menu_eff.setText("Tout effacer");
      menu_eff.setName("menu_eff");
      menu_eff.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menu_effActionPerformed(e);
        }
      });
      popupMenu1.add(menu_eff);
      
      // ---- menu_quit ----
      menu_quit.setText("Quitter la calculatrice");
      menu_quit.setName("menu_quit");
      menu_quit.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menu_quitActionPerformed(e);
        }
      });
      popupMenu1.add(menu_quit);
    }
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlEcritures;
  private JScrollPane scrollPane1;
  private JTable table1;
  private SNPanelTitre pnlTotaux;
  private SNLabelChamp lbDebit;
  private JTextField debit;
  private SNLabelChamp lbCredit;
  private JTextField credit;
  private SNLabelChamp lbSolde;
  private JTextField total;
  private JLabel l_type;
  private JPopupMenu popupMenu1;
  private JMenuItem menu_suppr;
  private JMenuItem menu_eff;
  private JMenuItem menu_quit;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
