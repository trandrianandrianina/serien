/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.SessionTransfert;

/**
 * Affiche une liste de documents et permet de les consulter avec le viewer natif
 */
public class ConsultDocPopup {
  private String[] listedoc = null;
  private SessionTransfert trfSession = null;
  private EnvUser infoUser = null;
  private JPopupMenu pop_LstDoc = new JPopupMenu();
  private JMenuItem m_item = null;
  private boolean supprfichiercache = false;
  
  /**
   * Constructeur
   * @param alistedoc
   * @param ainfoUser
   */
  public ConsultDocPopup(String[] alistedoc, EnvUser ainfoUser) {
    // Vérif validité (TODO à améliorer)
    if ((alistedoc == null) || (alistedoc.length <= 1)) {
      JOptionPane.showMessageDialog(null, "Pas de documents disponible.", "Consultation des documents", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    // Filtre sur les documents
    ArrayList<String> temp = new ArrayList<String>(alistedoc.length);
    temp.add(alistedoc[0]);
    for (int i = 1; i < alistedoc.length; i++) {
      if ((alistedoc[i].toLowerCase().lastIndexOf(".gif") == -1) && (alistedoc[i].toLowerCase().lastIndexOf(".jpeg") == -1)
          && (alistedoc[i].toLowerCase().lastIndexOf(".jpg") == -1) && (alistedoc[i].toLowerCase().lastIndexOf(".png") == -1)
          && (alistedoc[i].toLowerCase().lastIndexOf(".bmp") == -1) && (!alistedoc[i].equalsIgnoreCase("Thumbs.db"))) {
        temp.add(alistedoc[i]);
      }
    }
    
    listedoc = new String[temp.size()];
    temp.toArray(listedoc);
    
    // Affichage d'un message comme quoi pas de doc
    if ((listedoc == null) || (listedoc.length == 1)) {
      JOptionPane.showMessageDialog(null, "Pas de documents disponibles.", "Consultation des documents", JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    
    infoUser = ainfoUser;
    trfSession = infoUser.getTransfertSession();
    
    // S'il n'y a qu'un seul doc on affiche pas la fenêtre
    // if (listedoc.length == 2)
    // afficheDoc(listedoc[1]);
    // else
    for (int i = 1; i < listedoc.length; i++) {
      m_item = new JMenuItem(listedoc[i]);
      m_item.setName(Integer.toString(i));
      m_item.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          setSupprFichierCache(true);
          afficheDoc(listedoc[Integer.parseInt(((JMenuItem) e.getSource()).getName())]);
        }
      });
      pop_LstDoc.add(m_item);
    }
  }
  
  /**
   * Supprime le fichier dans le cache avant récupération
   * @param supprfic
   */
  public void setSupprFichierCache(boolean supprfic) {
    supprfichiercache = supprfic;
  }
  
  /**
   * Affiche le doc sélectionné
   *
   */
  private void afficheDoc(String doc) {
    File fichiercache = null;
    LaunchViewer lv = null;
    
    String fichier = listedoc[0] + doc.trim();
    String nomDossierTempUser = infoUser.getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
    String chaine = nomDossierTempUser + File.separatorChar + fichier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_');
    fichiercache = new File(chaine);
    
    if (supprfichiercache) {
      fichiercache.delete();
    }
    if (!fichiercache.exists()) {
      trfSession.fichierArecup(fichier);
    }
    lv = new LaunchViewer(chaine);
    if (!lv.lancerViewer()) {
      JOptionPane.showMessageDialog(null, lv.getMsgErreur(), "Affichage du document", JOptionPane.WARNING_MESSAGE);
    }
  }
  
  /**
   * Retourne le menu contectuel
   * @return
   */
  public JPopupMenu getPopupMenu() {
    return pop_LstDoc;
  }
}
