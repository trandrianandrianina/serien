/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.Component;

import javax.swing.JOptionPane;

import ri.serien.libcommun.outils.Constantes;

/**
 * Gestion des fenêtres d'erreur AS400
 * Revoir cette gestion aussi bien cette classe que sur le serveur (JSON à intégrer)
 */
public class DialogError {
  // Constantes
  private static final int NBR_CARACTERES_MAX = 80;
  
  // Variables
  private String title = "Erreur";
  private Component parent = null;
  private boolean errorEdition = false; // A supprimer plus tard lorsque on améliorera vraiment cette classe
  String[] buttons = { "Copier" }; // boutons supplémentaires sur la fenêtre
  
  /**
   * Constructeur
   * @param aparent
   * @param atitle
   */
  public DialogError(Component aparent, String atitle) {
    setTitle(atitle);
    setParent(aparent);
  }
  
  /**
   * Constructeur
   * @param aparent
   * @param atitle
   * @param isedition
   */
  public DialogError(Component aparent, String atitle, boolean isedition) {
    this(aparent, atitle);
    setErrorEdition(isedition);
  }
  
  /**
   * Construit et affiche la boite de dialogue des erreurs CPF
   * @param chaine
   * @param titre
   * @return
   */
  public String formateDialogError(String[] infos) {
    String[] texte = null;
    String[] reponse = null;
    Object[] possibilities = null;
    String rep = null;
    StringBuilder sb = new StringBuilder();
    StringBuilder reponsehtml = new StringBuilder();
    int posd;
    int posf;
    boolean fin = false;
    
    if ((errorEdition) && ((infos == null) || (infos[0] == null))) {
      return null;
    }
    
    // for (int i=0; i<infos.length; i++)
    
    // Pour les messages simples sans demande de retour
    if ((infos[0].length() == 0) || (infos[0].charAt(0) == Constantes.TYPE_ERR_SIMPLE)) {
      // On construit la boite de dialogue
      sb.setLength(0);
      sb.append("<html>");
      for (int i = 1; i < infos.length; i++) {
        sb.append(infos[i]).append("<br>");
      }
      sb.append("</html>");
      
      JOptionPane.showMessageDialog(parent, sb.toString(), title, JOptionPane.ERROR_MESSAGE);
      
      return null;
    }
    else if (infos[0].charAt(0) == Constantes.TYPE_WRNG_SIMPLE) {
      // On construit la boite de dialogue
      sb.setLength(0);
      sb.append("<html>");
      for (int i = 1; i < infos.length; i++) {
        sb.append(infos[i]).append("<br>");
      }
      sb.append("</html>");
      
      JOptionPane.showMessageDialog(parent, sb.toString(), title, JOptionPane.WARNING_MESSAGE);
      
      return "";
    }
    // Pour les messages erreurs de type CPF avec réponse
    else {
      if ((errorEdition) && (infos.length < 5)) {
        return null;
      }
      
      // On découpe la chaine en fonction des marqueurs &N
      // for( String elt: infos)
      texte = infos[4].split("&N");
      
      // On extrait les réponses possibles
      if (texte.length > 0) {
        reponse = texte[texte.length - 1].split("&B");
        
        if (reponse.length > 0) {
          // On met la section dans le texte
          texte[texte.length - 1] = "<b>" + reponse[0] + "</b>";
          
          // On remplit le tableau des valeurs possibles et le texte des réponses possibles
          possibilities = new Object[reponse.length - 1];
          reponsehtml.setLength(0);
          for (int i = 1; i < reponse.length; i++) {
            posd = 0;
            posf = NBR_CARACTERES_MAX;
            fin = false;
            reponsehtml.append("<div style=\"text-indent:-10px; margin-left:10px;\">");
            do {
              if (posf < reponse[i].length()) {
                // On cherche un blanc afin d'éviter de tronquer un mot
                while ((posf < reponse[i].length()) && (reponse[i].charAt(posf) != ' ')) {
                  posf++;
                }
                reponsehtml.append(reponse[i].substring(posd, posf)).append("<br>");
                posd = posf;
                posf += NBR_CARACTERES_MAX;
              }
              else {
                reponsehtml.append(reponse[i].substring(posd));
                fin = true;
              }
            }
            while (!fin);
            reponsehtml.append("</div>");
            
            // On récupère juste le code réponse à renvoyer
            possibilities[reponse.length - i - 1] = reponse[i].substring(0, reponse[i].indexOf("--")).trim();
          }
          reponsehtml.append("<br>");
        }
      }
      
      // On coupe les chaines trop longues (max 80 caractères à peu près)
      int j = 0;
      for (int i = 0; i < texte.length; i++) {
        sb.setLength(0);
        sb.append(texte[i]);
        j = NBR_CARACTERES_MAX;
        do {
          if (sb.length() > NBR_CARACTERES_MAX) {
            while ((j < sb.length()) && (sb.charAt(j) != ' ')) {
              j++;
            }
            sb.deleteCharAt(j);
            sb.insert(j, "<br>");
            j += 84;
          }
        }
        while (j < sb.length());
        
        // On met en gras la section
        j = sb.indexOf(":   ");
        if (j != -1) {
          sb.insert(j + 1, "</b>");
          sb.insert(0, "<b>");
        }
        texte[i] = "<br>" + sb.toString();
      }
      
      // On construit la boite de dialogue
      sb.setLength(0);
      sb.append("<html><b> ID message . . :   </b>").append(infos[1]).append("<br><b> Gravité . . . . . :   </b>").append(infos[2])
          .append("<br><b> Date . . . . . . . :   </b>").append(infos[5]).append("<br><b> Travail . . . . :   </b>").append(infos[0])
          .append("<br><br><b>").append(infos[3]).append("</b>");
      for (int i = 0; i < texte.length; i++) {
        sb.append(texte[i]).append("<br>");
      }
      sb.append(reponsehtml).append("</html>");
      rep = (String) JOptionPane.showInputDialog(parent, sb.toString(), title, JOptionPane.ERROR_MESSAGE, null, possibilities, null);
      // rep = (String)JOptionPane.showInputDialog(parent, sb.toString(), title, JOptionPane.ERROR_MESSAGE, null,
      // possibilities, null);
      
      if (rep == null) {
        return null;
      }
      
      return infos[0] + Constantes.SEPARATEUR_CHAINE + rep.charAt(0);
    }
  }
  
  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }
  
  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }
  
  /**
   * @return the parent
   */
  public Component getParent() {
    return parent;
  }
  
  /**
   * @param parent the parent to set
   */
  public void setParent(Component parent) {
    this.parent = parent;
  }
  
  /**
   * @return the errorEdition
   */
  public boolean isErrorEdition() {
    return errorEdition;
  }
  
  /**
   * @param errorEdition the errorEdition to set
   */
  public void setErrorEdition(boolean errorEdition) {
    this.errorEdition = errorEdition;
  }
  
}
