/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

/**
 * Cette classe permet d'instancier la calculatrice Série M.
 * @author David Biason
 * @version 1.0
 */
public class Calculatrice extends JFrame {

  /**
   * Premier nombre à soumettre au calcul.
   */
  private String nb1_S = "0";
  /**
   * Deuxième nombre à soumettre au calcul.
   */
  private String nb2_S = "";
  /**
   * Une fois validé le nombre 2 est mis en mémoire pour d'éventuels calculs automatiques.
   */
  private String nbMem_S = "";
  /**
   * Signe de l'opération en cours.
   */
  private String signe = "";
  /**
   * Résultat du dernier calcul.
   */
  private String resultat_S = "";
  /**
   * Dernière touche appuyée par l'utilisateur.
   */
  private String dernier = "";
  /**
   * Définit si le premier nombre est défini ou non.
   */
  private boolean nb1_isDef = false;
  /**
   * Définit si le deuxième nombre est défini ou non.
   */
  private boolean nb2_isDef = false;
  /**
   * Définit si une virgule a été définie sur le nombre en cours.
   */
  private boolean point_isDef = false;
  /**
   * Cet objet est la fenêtre qui prend en charge l'historique de la calculatrice.
   */
  private Historique_Calcu histo;
  /**
   * Cette liste décrit le dernier calcul saisi par l'utilisateur (nombre 1, signe, nombre 2 et résultat) .
   */
  private ArrayList<String> histoCalcul = null;

  private Object objetParent = null;

  /**
   * Constructeur à vide de la calculatrice
   */
  public Calculatrice() {
    initComponents();
    setTitle("Calculatrice Série M");
    majAffichage();
    setVisible(true);
    histo = new Historique_Calcu();
    gererLeClavier();

    ClassLoader cl = this.getClass().getClassLoader();
    ImageIcon imgHisto = new ImageIcon(cl.getResource("images/ark2.png"));
    ImageIcon imgValid = new ImageIcon(cl.getResource("images/button_ok.png"));
    bt_histo.setIcon(imgHisto);
    bt_valider.setIcon(imgValid);
  }

  /**
   * Constructeur de la calculatrice dont on passe l'objet appellant
   * @param objet
   */
  public Calculatrice(Object objet) {
    this();
    if (objet != null) {
      // cas où c'est une jtable qui l'appelle
      if (objet instanceof JTable) {
        initialisationParJTable((JTable) objet);
      }
      else if (objet instanceof JTextField) {
        initialisationParJText((JTextField) objet);
      }
    }
  }

  /**
   * Constructeur de la calculatrice dont on passe l'objet appellant
   * @param objet
   */
  public void reveiller(Object objet) {
    if (objet != null) {
      // cas où c'est une jtable qui l'appelle
      if (objet instanceof JTable) {
        initialisationParJTable((JTable) objet);
      }
      else if (objet instanceof JTextField) {
        initialisationParJText((JTextField) objet);
      }
    }
    this.setVisible(true);
  }

  /**
   * Initialise le données de la calculatrice à partir d'une JTextfield
   * @param objet
   */
  public void initialisationParJText(JTextField objet) {
    objetParent = objet;
    String valeurRecuperee = objet.getText();
    if (valeurRecuperee.trim().equals("")) {
      valeurRecuperee = "0";
    }

    affecterValeurs(valeurRecuperee);
  }

  /**
   * Initialise le données de la calculatrice à partir d'une cellule de JTable
   * @param objet
   */
  public void initialisationParJTable(JTable objet) {
    objetParent = objet;
    int rowIndex = objet.getSelectedRow();
    int columnIndex = objet.getSelectedColumn();
    String valeurRecuperee = (String) objet.getModel().getValueAt(rowIndex, columnIndex);
    if (valeurRecuperee.trim().equals("")) {
      valeurRecuperee = "0";
    }

    affecterValeurs(valeurRecuperee);
  }

  /**
   * envoie la valeur résultat à l'objet qui a appellé la calculatrice
   */
  public void envoyerValeur() {
    String resultatNettoye = null;

    // nettoyer les .0 inutiles
    if (resultat_S.length() > 1) {
      if (resultat_S.substring(resultat_S.length() - 2).equals(".0")) {
        resultatNettoye = resultat_S.substring(0, resultat_S.length() - 2);
      }
    }

    if (resultatNettoye == null) {
      resultatNettoye = resultat_S;
    }

    if (objetParent instanceof JTable) {
      // envoyer le résultat dans l'objet appellant
      int rowIndex = ((JTable) objetParent).getSelectedRow();
      int columnIndex = ((JTable) objetParent).getSelectedColumn();
      ((JTable) objetParent).getModel().setValueAt(resultatNettoye, rowIndex, columnIndex);
    }
    else if (objetParent instanceof JTextField) {
      ((JTextField) objetParent).setText(resultatNettoye);
    }

    razDonnees(0);
    histo.dispose();
    this.dispose();
  }

  /**
   * permet d'incrémenter le nbombre 1 ou le nombre 2 avec la valeur passée en paramètre.
   * @param chiffre
   */
  public void affecterValeurs(String chiffre) {
    erreur.setText("");
    if (!nb1_isDef) {
      nb1_S = nettoyerLeZero(chiffre);
    }
    else {
      if (isEgal()) {
        razDonnees(1);
        nb1_S = chiffre;
      }
      else if (isSigne()) {
        nb2_isDef = false;
        nb2_S = chiffre;
      }
      else {
        nb2_S = nettoyerLeZero(chiffre);
      }
    }
    majAffichage();
    majDernier(chiffre);
  }

  /**
   * permet de mettre à jour le signe de l'opération, et définit ainsi l'état du nombre 1 et du nombre 2.
   * @param sign
   */
  public void majSigne(String sign) {
    if (sign != null) {
      if (nb2_isDef) {
        if (isEgal()) {
          nb2_isDef = false;
          nb2_S = nb1_S;
        }
      }
      else {
        if (nb1_isDef) {
          if (nb2_S.equals("")) {
            nb2_S = nb1_S;
          }

          calculer(signe);
        }
        else {
          nb1_isDef = true;
        }
      }
      signe = sign;
      point_isDef = false;
      majDernier(sign);
    }
  }

  /**
   * permet de mettre à jour l'historique de la dernière touche pressée par l'utilisateur.
   * @param valeur
   */
  public void majDernier(String valeur) {
    dernier = valeur;
  }

  /**
   * Détermine si la dernière touche pressée est le signe égal(=).
   */
  public boolean isEgal() {
    if (dernier.equals("=")) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Détermine si la dernière touche pressée est un signe d'opération(+,-,*,/).
   */
  public boolean isSigne() {
    String[] tabS = { "+", "-", "*", "/" };
    boolean isPresent = false;

    for (int i = 0; i < tabS.length; i++) {
      if (dernier.equals(tabS[i])) {
        isPresent = true;
      }
    }
    return isPresent;
  }

  /**
   * Met à jour l'affichage de la calculatrice
   */
  public void majAffichage() {
    if (nb1_isDef) {
      if (nb2_isDef) {
        resultatAff.setText(resultat_S + " ");
      }
      else {
        resultatAff.setText(nb2_S + " ");
      }
    }
    else {
      resultatAff.setText(nb1_S + " ");
    }
  }

  /**
   * Effectue les calculs entre le nombre 1 et le nombre 2 en fonction de l'opérateur passé.
   * @param sign
   */
  public void calculer(String sign) {
    if (nb2_S.equals("")) {
      nb2_S = nb1_S;
    }

    nb2_isDef = true;
    nbMem_S = nb2_S;
    point_isDef = false;
    Double result = 0.0;

    if (sign != null) {
      if (sign.equals("+")) {
        result = Double.parseDouble(nb1_S) + Double.parseDouble(nb2_S);
      }

      if (sign.equals("-")) {
        result = Double.parseDouble(nb1_S) - Double.parseDouble(nb2_S);
      }

      if (sign.equals("/")) {
        if (!nb2_S.equals("0")) {
          result = Double.parseDouble(nb1_S) / Double.parseDouble(nb2_S);
        }
        else {
          result = Double.parseDouble(nb1_S);
          erreur.setText("Division par zéro impossible");
        }
      }

      if (sign.equals("*")) {
        result = Double.parseDouble(nb1_S) * Double.parseDouble(nb2_S);
      }

      resultat_S = result.toString();

      majAffichage();

      // mise à jour de l'historique
      histoCalcul = new ArrayList<String>();
      histoCalcul.add(nb1_S);
      histoCalcul.add(signe);
      histoCalcul.add(nb2_S);
      histoCalcul.add(resultat_S);
      histo.majDonnees(histoCalcul);

      // récup du résultat pour prochain calcul
      nb1_S = resultat_S;
    }
  }

  /**
   * Effectue une remise à zéro de toutes les données ou d'une partie des données en fonction du type
   * @param type
   */
  public void razDonnees(int type) {
    nb1_S = "0";
    nb2_S = "";
    nbMem_S = "";
    signe = "";
    resultat_S = "0";
    nb1_isDef = false;
    nb2_isDef = false;
    point_isDef = false;

    erreur.setText("");

    if (type == 0) {
      histo.razDonnees();
    }

    majAffichage();
  }

  /**
   * Permet de nettoyer les données avant l'affichage ( zéros et virgules superflus )
   * @param valeur
   */
  public String nettoyerLeZero(String valeur) {
    String resultat = "";
    if (nb1_isDef) {
      resultat = nb2_S;
    }
    else {
      resultat = nb1_S;
    }

    if (resultat.equals("0")) {
      if (!valeur.equals(".")) {
        resultat = "";
      }
    }

    resultat += valeur;
    return resultat;
  }

  /**
   * Permet de gérer les touches clavier autorisées à intervenir sur la calculatrice.
   */
  public void gererLeClavier() {
    Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
      @Override
      public void eventDispatched(AWTEvent event) {
        KeyEvent key = (KeyEvent) event;
        if (key.getID() == KeyEvent.KEY_PRESSED) {
          if (key.getKeyCode() == KeyEvent.VK_NUMPAD1) {
            affecterValeurs("1");
          }
          else if (key.getKeyCode() == KeyEvent.VK_NUMPAD2) {
            affecterValeurs("2");
          }
          else if (key.getKeyCode() == KeyEvent.VK_NUMPAD3) {
            affecterValeurs("3");
          }
          else if (key.getKeyCode() == KeyEvent.VK_NUMPAD4) {
            affecterValeurs("4");
          }
          else if (key.getKeyCode() == KeyEvent.VK_NUMPAD5) {
            affecterValeurs("5");
          }
          else if (key.getKeyCode() == KeyEvent.VK_NUMPAD6) {
            affecterValeurs("6");
          }
          else if (key.getKeyCode() == KeyEvent.VK_NUMPAD7) {
            affecterValeurs("7");
          }
          else if (key.getKeyCode() == KeyEvent.VK_NUMPAD8) {
            affecterValeurs("8");
          }
          else if (key.getKeyCode() == KeyEvent.VK_NUMPAD9) {
            affecterValeurs("9");
          }
          else if (key.getKeyCode() == KeyEvent.VK_NUMPAD0) {
            affecterValeurs("0");
          }
          else if (key.getKeyCode() == KeyEvent.VK_DECIMAL) {
            if (!point_isDef) {
              affecterValeurs(".");
              point_isDef = true;
            }
            else {
              erreur.setText("la virgule existe déjà.");
            }
          }
          else if (key.getKeyCode() == KeyEvent.VK_ENTER) {
            if (nb1_isDef) {
              calculer(signe);
            }
            majDernier("=");
          }
          else if (key.getKeyCode() == KeyEvent.VK_ADD) {
            majSigne("+");
          }
          else if (key.getKeyCode() == KeyEvent.VK_SUBTRACT) {
            majSigne("-");
          }
          else if (key.getKeyCode() == KeyEvent.VK_MULTIPLY) {
            majSigne("*");
          }
          else if (key.getKeyCode() == KeyEvent.VK_DIVIDE) {
            majSigne("/");
          }
          else if (key.getKeyCode() == KeyEvent.VK_ESCAPE) {
            razDonnees(0);
          }

        }
      }
    }, AWTEvent.KEY_EVENT_MASK);
  }

  /**
   * Permet d'afficher un certain nombre de valeurs dans la console JAVA.
   * @param message
   */
  public void console(String message) {
  }

  private void bt_1ActionPerformed(ActionEvent e) {
    affecterValeurs("1");
  }

  private void bt_2ActionPerformed(ActionEvent e) {
    affecterValeurs("2");
  }

  private void bt_3ActionPerformed(ActionEvent e) {
    affecterValeurs("3");
  }

  private void bt_4ActionPerformed(ActionEvent e) {
    affecterValeurs("4");
  }

  private void bt_5ActionPerformed(ActionEvent e) {
    affecterValeurs("5");
  }

  private void bt_6ActionPerformed(ActionEvent e) {
    affecterValeurs("6");
  }

  private void bt_7ActionPerformed(ActionEvent e) {
    affecterValeurs("7");
  }

  private void bt_8ActionPerformed(ActionEvent e) {
    affecterValeurs("8");
  }

  private void bt_9ActionPerformed(ActionEvent e) {
    affecterValeurs("9");
  }

  private void bt_pointActionPerformed(ActionEvent e) {
    if (!point_isDef) {
      affecterValeurs(".");
      point_isDef = true;
    }
    else {
      erreur.setText("la virgule existe déjà.");
    }
  }

  private void bt_0ActionPerformed(ActionEvent e) {
    affecterValeurs("0");
  }

  private void bt_egalActionPerformed(ActionEvent e) {
    erreur.setText("");
    if (nb1_isDef) {
      calculer(signe);
    }
    else {
      resultat_S = nb1_S;
    }

    majDernier("=");
  }

  private void bt_CActionPerformed(ActionEvent e) {
    razDonnees(0);
  }

  private void bt_plusActionPerformed(ActionEvent e) {
    erreur.setText("");
    majSigne("+");
  }

  private void bt_moinsActionPerformed(ActionEvent e) {
    erreur.setText("");
    majSigne("-");
  }

  private void bt_divActionPerformed(ActionEvent e) {
    erreur.setText("");
    majSigne("/");
  }

  private void bt_multActionPerformed(ActionEvent e) {
    erreur.setText("");
    majSigne("*");
  }

  private void bt_histoActionPerformed(ActionEvent e) {
    histo.setLocation(this.getLocation().x + 300, this.getLocation().y);

    if (histo.isVisible()) {
      histo.setVisible(false);
    }
    else {
      histo.setVisible(true);
    }
  }

  private void bt_validerActionPerformed(ActionEvent e) {
    if (!isEgal()) {
      if (nb1_isDef) {
        calculer(signe);
      }
      else {
        resultat_S = nb1_S;
      }

      majDernier("=");
    }

    envoyerValeur();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    resultatAff = new JLabel();
    bt_valider = new JButton();
    bt_histo = new JButton();
    panel2 = new JPanel();
    bt_1 = new JButton();
    bt_2 = new JButton();
    bt_3 = new JButton();
    bt_0 = new JButton();
    bt_egal = new JButton();
    bt_C = new JButton();
    bt_plus = new JButton();
    bt_moins = new JButton();
    bt_div = new JButton();
    bt_mult = new JButton();
    bt_point = new JButton();
    bt_7 = new JButton();
    bt_4 = new JButton();
    bt_5 = new JButton();
    bt_6 = new JButton();
    bt_8 = new JButton();
    bt_9 = new JButton();
    panel3 = new JPanel();
    erreur = new JLabel();
    
    // ======== this ========
    setResizable(false);
    setMinimumSize(new Dimension(280, 250));
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== panel1 ========
    {
      panel1.setMinimumSize(new Dimension(280, 47));
      panel1.setPreferredSize(new Dimension(280, 47));
      panel1.setName("panel1");
      panel1.setLayout(null);
      
      // ---- resultatAff ----
      resultatAff.setText("0 ");
      resultatAff.setHorizontalAlignment(SwingConstants.RIGHT);
      resultatAff
          .setFont(resultatAff.getFont().deriveFont(resultatAff.getFont().getStyle() | Font.BOLD, resultatAff.getFont().getSize() + 4f));
      resultatAff.setBorder(LineBorder.createBlackLineBorder());
      resultatAff.setHorizontalTextPosition(SwingConstants.LEADING);
      resultatAff.setBackground(Color.white);
      resultatAff.setName("resultatAff");
      panel1.add(resultatAff);
      resultatAff.setBounds(55, 10, 160, 35);
      
      // ---- bt_valider ----
      bt_valider.setToolTipText("Valider le total");
      bt_valider.setName("bt_valider");
      bt_valider.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_validerActionPerformed(e);
        }
      });
      panel1.add(bt_valider);
      bt_valider.setBounds(220, 7, 40, 40);
      
      // ---- bt_histo ----
      bt_histo.setName("bt_histo");
      bt_histo.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_histoActionPerformed(e);
        }
      });
      panel1.add(bt_histo);
      bt_histo.setBounds(20, 7, 30, 40);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(panel1, BorderLayout.NORTH);
    
    // ======== panel2 ========
    {
      panel2.setMinimumSize(new Dimension(280, 260));
      panel2.setPreferredSize(new Dimension(280, 260));
      panel2.setMaximumSize(new Dimension(280, 260));
      panel2.setName("panel2");
      
      // ---- bt_1 ----
      bt_1.setText("1");
      bt_1.setFont(bt_1.getFont().deriveFont(bt_1.getFont().getStyle() | Font.BOLD, bt_1.getFont().getSize() + 5f));
      bt_1.setName("bt_1");
      bt_1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_1ActionPerformed(e);
        }
      });
      
      // ---- bt_2 ----
      bt_2.setText("2");
      bt_2.setFont(bt_2.getFont().deriveFont(bt_2.getFont().getStyle() | Font.BOLD, bt_2.getFont().getSize() + 5f));
      bt_2.setName("bt_2");
      bt_2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_2ActionPerformed(e);
        }
      });
      
      // ---- bt_3 ----
      bt_3.setText("3");
      bt_3.setFont(bt_3.getFont().deriveFont(bt_3.getFont().getStyle() | Font.BOLD, bt_3.getFont().getSize() + 5f));
      bt_3.setName("bt_3");
      bt_3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_3ActionPerformed(e);
        }
      });
      
      // ---- bt_0 ----
      bt_0.setText("0");
      bt_0.setFont(bt_0.getFont().deriveFont(bt_0.getFont().getStyle() | Font.BOLD, bt_0.getFont().getSize() + 5f));
      bt_0.setName("bt_0");
      bt_0.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_0ActionPerformed(e);
        }
      });
      
      // ---- bt_egal ----
      bt_egal.setText("=");
      bt_egal.setFont(bt_egal.getFont().deriveFont(bt_egal.getFont().getStyle() | Font.BOLD, bt_egal.getFont().getSize() + 5f));
      bt_egal.setName("bt_egal");
      bt_egal.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_egalActionPerformed(e);
        }
      });
      
      // ---- bt_C ----
      bt_C.setText("C");
      bt_C.setFont(bt_C.getFont().deriveFont(bt_C.getFont().getStyle() | Font.BOLD, bt_C.getFont().getSize() + 5f));
      bt_C.setName("bt_C");
      bt_C.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_CActionPerformed(e);
        }
      });
      
      // ---- bt_plus ----
      bt_plus.setText("+");
      bt_plus.setFont(bt_plus.getFont().deriveFont(bt_plus.getFont().getStyle() | Font.BOLD, bt_plus.getFont().getSize() + 7f));
      bt_plus.setName("bt_plus");
      bt_plus.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_plusActionPerformed(e);
        }
      });
      
      // ---- bt_moins ----
      bt_moins.setText("-");
      bt_moins.setFont(bt_moins.getFont().deriveFont(bt_moins.getFont().getStyle() | Font.BOLD, bt_moins.getFont().getSize() + 7f));
      bt_moins.setName("bt_moins");
      bt_moins.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_moinsActionPerformed(e);
        }
      });
      
      // ---- bt_div ----
      bt_div.setText("/");
      bt_div.setFont(bt_div.getFont().deriveFont(bt_div.getFont().getStyle() | Font.BOLD, bt_div.getFont().getSize() + 7f));
      bt_div.setName("bt_div");
      bt_div.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_divActionPerformed(e);
        }
      });
      
      // ---- bt_mult ----
      bt_mult.setText("*");
      bt_mult.setFont(bt_mult.getFont().deriveFont(bt_mult.getFont().getStyle() | Font.BOLD, bt_mult.getFont().getSize() + 7f));
      bt_mult.setName("bt_mult");
      bt_mult.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_multActionPerformed(e);
        }
      });
      
      // ---- bt_point ----
      bt_point.setText(".");
      bt_point.setFont(bt_point.getFont().deriveFont(bt_point.getFont().getStyle() | Font.BOLD, bt_point.getFont().getSize() + 7f));
      bt_point.setName("bt_point");
      bt_point.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_pointActionPerformed(e);
        }
      });
      
      // ---- bt_7 ----
      bt_7.setText("7");
      bt_7.setFont(bt_7.getFont().deriveFont(bt_7.getFont().getStyle() | Font.BOLD, bt_7.getFont().getSize() + 5f));
      bt_7.setName("bt_7");
      bt_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_7ActionPerformed(e);
        }
      });
      
      // ---- bt_4 ----
      bt_4.setText("4");
      bt_4.setFont(bt_4.getFont().deriveFont(bt_4.getFont().getStyle() | Font.BOLD, bt_4.getFont().getSize() + 5f));
      bt_4.setName("bt_4");
      bt_4.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_4ActionPerformed(e);
        }
      });
      
      // ---- bt_5 ----
      bt_5.setText("5");
      bt_5.setFont(bt_5.getFont().deriveFont(bt_5.getFont().getStyle() | Font.BOLD, bt_5.getFont().getSize() + 5f));
      bt_5.setName("bt_5");
      bt_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_5ActionPerformed(e);
        }
      });
      
      // ---- bt_6 ----
      bt_6.setText("6");
      bt_6.setFont(bt_6.getFont().deriveFont(bt_6.getFont().getStyle() | Font.BOLD, bt_6.getFont().getSize() + 5f));
      bt_6.setName("bt_6");
      bt_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_6ActionPerformed(e);
        }
      });
      
      // ---- bt_8 ----
      bt_8.setText("8");
      bt_8.setFont(bt_8.getFont().deriveFont(bt_8.getFont().getStyle() | Font.BOLD, bt_8.getFont().getSize() + 5f));
      bt_8.setName("bt_8");
      bt_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_8ActionPerformed(e);
        }
      });
      
      // ---- bt_9 ----
      bt_9.setText("9");
      bt_9.setFont(bt_9.getFont().deriveFont(bt_9.getFont().getStyle() | Font.BOLD, bt_9.getFont().getSize() + 5f));
      bt_9.setName("bt_9");
      bt_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_9ActionPerformed(e);
        }
      });
      
      GroupLayout panel2Layout = new GroupLayout(panel2);
      panel2.setLayout(panel2Layout);
      panel2Layout.setHorizontalGroup(panel2Layout.createParallelGroup()
          .addGroup(panel2Layout.createSequentialGroup().addGap(20, 20, 20)
              .addGroup(panel2Layout.createParallelGroup().addComponent(bt_7, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_0, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createParallelGroup().addComponent(bt_8, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_point, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createParallelGroup().addComponent(bt_9, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_6, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_egal, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createParallelGroup().addComponent(bt_C, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_plus, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_moins, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_div, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_mult, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addContainerGap(28, Short.MAX_VALUE)));
      panel2Layout
          .setVerticalGroup(panel2Layout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING,
              panel2Layout.createSequentialGroup().addContainerGap(22, Short.MAX_VALUE)
                  .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                          .addComponent(bt_7, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_0, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                          .addComponent(bt_8, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_point, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                          .addComponent(bt_9, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_6, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_egal, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                          .addComponent(bt_C, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_plus, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_moins, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_div, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(bt_mult, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)))));
    }
    contentPane.add(panel2, BorderLayout.CENTER);
    
    // ======== panel3 ========
    {
      panel3.setMinimumSize(new Dimension(280, 40));
      panel3.setPreferredSize(new Dimension(280, 40));
      panel3.setName("panel3");
      panel3.setLayout(null);
      
      // ---- erreur ----
      erreur.setForeground(new Color(255, 51, 51));
      erreur.setHorizontalAlignment(SwingConstants.LEFT);
      erreur.setName("erreur");
      panel3.add(erreur);
      erreur.setBounds(25, 8, 185, 25);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < panel3.getComponentCount(); i++) {
          Rectangle bounds = panel3.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel3.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel3.setMinimumSize(preferredSize);
        panel3.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(panel3, BorderLayout.SOUTH);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JLabel resultatAff;
  private JButton bt_valider;
  private JButton bt_histo;
  private JPanel panel2;
  private JButton bt_1;
  private JButton bt_2;
  private JButton bt_3;
  private JButton bt_0;
  private JButton bt_egal;
  private JButton bt_C;
  private JButton bt_plus;
  private JButton bt_moins;
  private JButton bt_div;
  private JButton bt_mult;
  private JButton bt_point;
  private JButton bt_7;
  private JButton bt_4;
  private JButton bt_5;
  private JButton bt_6;
  private JButton bt_8;
  private JButton bt_9;
  private JPanel panel3;
  private JLabel erreur;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
