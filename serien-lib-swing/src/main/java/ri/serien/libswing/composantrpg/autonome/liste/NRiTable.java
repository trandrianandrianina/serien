/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome.liste;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.EventObject;

import javax.swing.Action;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.JTextComponent;

public class NRiTable extends JTable {
  // Constantes
  
  public static final int GAUCHE = 0;
  public static final int DROITE = 1;
  public static final int CENTRE = 2;
  
  // Variables
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  
  private boolean isSelectAllForMouseEvent = false;
  private boolean isSelectAllForActionEvent = false;
  private boolean isSelectAllForKeyEvent = false;
  
  private DefaultTableCellRenderer renduCelules = null;
  private int[] justification = null;
  private boolean autoriserTriColonne = false;
  
  /**
   * Constructeur.
   */
  public NRiTable() {
    super(null, null, null);
    initialiserVariables();
  }
  
  // -- Méthodes publiques
  
  /**
   * Permet de définir l'aspect de la table.
   */
  public void personnaliserAspect(DefaultTableModel pModeleTable, int[] pDimension, int[] pJustification, int pTaillePoliceEntete) {
    setModel(pModeleTable);
    initialiserAspect(pDimension, null, pJustification, pTaillePoliceEntete);
  }
  
  /**
   * Permet de définir l'aspect de la table.
   */
  public void personnaliserAspect(String pTitre[], int[] pDimension, int[] pJustification, int pTaillePoliceEntete) {
    DefaultTableModel modele = new DefaultTableModel(pTitre, 0) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    setModel(modele);
    initialiserAspect(pDimension, null, pJustification, pTaillePoliceEntete);
  }
  
  /**
   * Permet de définir l'aspect de la table.
   */
  public void personnaliserAspect(String pTitre[], final boolean[] pColEditable, int[] pDimension, int[] pJustification,
      int pTaillePoliceEntete) {
    DefaultTableModel modele = new DefaultTableModel(pTitre, 0) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (pColEditable == null) {
          return false;
        }
        return columnIndex < pColEditable.length ? pColEditable[columnIndex] : false;
      }
    };
    setModel(modele);
    initialiserAspect(pDimension, null, pJustification, pTaillePoliceEntete);
  }
  
  /**
   * Permet de définir l'aspect de la table.
   */
  public void personnaliserAspect(String pTitre[], int[] pDimension, int[] pDimensionMax, int[] pJustification, int pTaillePoliceEntete) {
    DefaultTableModel modele = new DefaultTableModel(pTitre, 0) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    setModel(modele);
    initialiserAspect(pDimension, pDimensionMax, pJustification, pTaillePoliceEntete);
  }
  
  /**
   * Permet de personnaliser le rendu des cellules de toutes les colonnes.
   */
  public void personnaliserAspectCellules(DefaultTableCellRenderer pRenduCelules) {
    for (int col = 0; col < getColumnCount(); col++) {
      getColumnModel().getColumn(col).setCellRenderer(pRenduCelules);
    }
  }
  
  /**
   * Permet de personnaliser le rendu des cellules de toutes les colonnes.
   */
  public void personnaliserAspectCellules(DefaultTableCellRenderer pRenduCelules, DefaultTableModel pModeleTable, int[] pDimension,
      int[] pJustification, int pTaillePoliceEntete) {
    setModel(pModeleTable);
    initialiserAspect(pDimension, null, pJustification, pTaillePoliceEntete);
    for (int col = 0; col < getColumnCount(); col++) {
      getColumnModel().getColumn(col).setCellRenderer(pRenduCelules);
    }
  }
  
  /**
   * Permet de forcer la saisie de la cellule en cours d'édition lors de la perte du focus de cette dernière.
   */
  public void forcerSaisieCellule(boolean pForcer) {
    putClientProperty("terminateEditOnFocusLost", pForcer);
  }
  
  /**
   * Permet de mettre à jour les données de la table.
   */
  public void mettreAJourDonnees(Object[][] pDonnees) {
    DefaultTableModel tableModel = (DefaultTableModel) getModel();
    tableModel.setRowCount(0);
    
    // Liste avec données
    if (pDonnees != null) {
      // Ajout des lignes
      for (int ligne = 0; ligne < pDonnees.length; ligne++) {
        tableModel.addRow(pDonnees[ligne]);
      }
      // Recalcule la taille de la liste afin d'avoir l'ascenceur uniquement quand c'est nécessaire
      setPreferredSize(new Dimension(getWidth(), pDonnees.length * getRowHeight()));
      // Activation ou non du tri par colonnes
      if (autoriserTriColonne) {
        trierColonnes();
      }
    }
    // Liste sans données
    else {
      // Recalcule la taille de la liste si aucune donnée
      setPreferredSize(new Dimension(getWidth(), 0));
    }
  }
  
  /**
   * Permet de remplacer l'action sur une touche donnée.
   */
  public void modifierAction(Action pAction, KeyStroke pTouche) {
    String marqueur = "action" + pTouche;
    getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(pTouche, marqueur);
    getActionMap().put(marqueur, pAction);
  }
  
  /**
   * Override to provide Select All editing functionality
   */
  @Override
  public boolean editCellAt(int row, int column, EventObject e) {
    boolean result = super.editCellAt(row, column, e);
    
    if (isSelectAllForMouseEvent || isSelectAllForActionEvent || isSelectAllForKeyEvent) {
      selectAll(e);
    }
    
    return result;
  }
  
  /**
   * Sets the Select All property for for all event types
   */
  public void setSelectAllForEdit(boolean isSelectAllForEdit) {
    setSelectAllForMouseEvent(isSelectAllForEdit);
    setSelectAllForActionEvent(isSelectAllForEdit);
    setSelectAllForKeyEvent(isSelectAllForEdit);
  }
  
  /**
   * Set the Select All property when editing is invoked by the mouse
   */
  public void setSelectAllForMouseEvent(boolean isSelectAllForMouseEvent) {
    this.isSelectAllForMouseEvent = isSelectAllForMouseEvent;
  }
  
  /**
   * Set the Select All property when editing is invoked by the "F2" key
   */
  public void setSelectAllForActionEvent(boolean isSelectAllForActionEvent) {
    this.isSelectAllForActionEvent = isSelectAllForActionEvent;
  }
  
  /**
   * Set the Select All property when editing is invoked by
   * typing directly into the cell
   */
  public void setSelectAllForKeyEvent(boolean isSelectAllForKeyEvent) {
    this.isSelectAllForKeyEvent = isSelectAllForKeyEvent;
  }
  
  /**
   * Retourne l'indice de la première ligne du tableau sélectionnée en tenant compte des tri sur colonne.
   */
  public int getIndiceSelection() {
    int indexvisuel = getSelectedRow();
    if (indexvisuel >= 0 && getRowSorter() != null) {
      int indexreel = getRowSorter().convertRowIndexToModel(indexvisuel);
      return indexreel;
    }
    return indexvisuel;
  }
  
  /**
   * Retourne une liste d'indices des lignes du tableau sélectionnées en tenant compte des tri sur colonne.
   */
  public ArrayList<Integer> getListeIndiceSelection() {
    int[] listeLigneSelectionnee = getSelectedRows();
    // Aucune ligne n'a été sélectionnée
    if (listeLigneSelectionnee == null) {
      return null;
    }
    ArrayList<Integer> listeConverti = new ArrayList<Integer>();
    // On parcourt l'ensemble de la liste afin de convertir les indices si un tri a été activé
    for (int i = 0; i < listeLigneSelectionnee.length; i++) {
      int indexvisuel = listeLigneSelectionnee[i];
      if (indexvisuel >= 0) {
        if (getRowSorter() != null) {
          listeConverti.add(getRowSorter().convertRowIndexToModel(indexvisuel));
        }
        else {
          listeConverti.add(indexvisuel);
        }
      }
    }
    return listeConverti;
  }

  // -- Méthodes privées
  
  /**
   * Initialise les variables génériques.
   */
  private void initialiserVariables() {
    leftRenderer.setHorizontalAlignment(DefaultTableCellRenderer.LEFT);
    rightRenderer.setHorizontalAlignment(DefaultTableCellRenderer.RIGHT);
    centerRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
  }
  
  /**
   * Initialise l'aspect de la table.
   */
  private void initialiserAspect(int[] pDimension, int[] pDimensionMax, int[] pJustification, int pTaillePoliceEntete) {
    justification = pJustification;
    getTableHeader().setFont(new Font("SansSerif", Font.PLAIN, pTaillePoliceEntete));
    setShowVerticalLines(true);
    setShowHorizontalLines(true);
    setBackground(Color.white);
    setOpaque(false);
    setRowHeight(20);
    setGridColor(new Color(204, 204, 204));
    
    // On dimension la largeur des colonnes
    dimensionnerColonnes(pDimension, pDimensionMax);
    // On personnalise la justification de chaque colonne
    justifierColonnes(pJustification);
    // On active la possibilité de trier chaque colonne si cela est autorisée
    if (autoriserTriColonne) {
      trierColonnes();
    }
  }
  
  /**
   * Dimensionnne les colonnes.
   */
  private void dimensionnerColonnes(int[] pDimension, int[] pDimensionMax) {
    TableColumnModel cm = getColumnModel();
    if ((pDimension != null) && (pDimension.length >= cm.getColumnCount())) {
      for (int colonne = 0; colonne < cm.getColumnCount(); colonne++) {
        cm.getColumn(colonne).setPreferredWidth(pDimension[colonne]);
        cm.getColumn(colonne).setMinWidth(pDimension[colonne]);
        if ((pDimensionMax != null) && (colonne < pDimensionMax.length) && (pDimensionMax[colonne] >= 0)) {
          cm.getColumn(colonne).setMaxWidth(pDimensionMax[colonne]);
        }
      }
    }
  }
  
  /**
   * Justifie les colonnes.
   */
  private void justifierColonnes(int[] pJustification) {
    TableColumnModel cm = getColumnModel();
    if ((pJustification != null) && (pJustification.length >= cm.getColumnCount())) {
      for (int colonne = 0; colonne < cm.getColumnCount(); colonne++) {
        switch (pJustification[colonne]) {
          case GAUCHE:
            cm.getColumn(colonne).setCellRenderer(leftRenderer);
            break;
          case DROITE:
            cm.getColumn(colonne).setCellRenderer(rightRenderer);
            break;
          case CENTRE:
            cm.getColumn(colonne).setCellRenderer(centerRenderer);
            break;
        }
      }
    }
  }
  
  /**
   * Permet d'activer le tri sur les colonnes si l'opération est autorisée.
   */
  public void trierColonnes() {
    // Tri sur les tables
    TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>((DefaultTableModel) getModel());
    // Comparator sur mesure :
    // toutes nos tables sortent des Strings. Pour avoir un tri numérique on caste ces Strings en double
    // Si il n'arrive pas à caster en double c'est que l'on a une date ou un String à l'origine et on fait un CompareTo.
    
    Comparator<String> comp = new Comparator<String>() {
      @Override
      public int compare(String s1, String s2) {
        int retour = 0;
        
        if (s1.contains("/") && s2.contains("/")) {
          SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
          try {
            Date dateS1 = sdf.parse(s1);
            Date dateS2 = sdf.parse(s2);
            retour = dateS1.compareTo(dateS2);
          }
          catch (ParseException e) {
            retour = s1.compareTo(s2);
          }
        }
        else {
          // Cast des valeurs à comparer en double si c'est possible
          try {
            s1 = s1.replace(',', '.');
            s2 = s2.replace(',', '.');
            Double INTs1 = Double.valueOf(s1);
            Double INTs2 = Double.valueOf(s2);
            
            if (INTs1 < INTs2) {
              retour = -1;
            }
            else if (INTs1 > INTs2) {
              retour = 1;
            }
          }
          // Si on a une exception c'est que le cast est impossible : on fait un compareTo
          catch (NumberFormatException e) {
            retour = s1.compareTo(s2);
          }
        }
        return retour;
      }
    };
    
    // On applique le sorter à chaque colonne de la table
    for (int i = 0; i < (this.getColumnCount() - 1); i++) {
      sorter.setComparator(i, comp);
    }
    
    setRowSorter(sorter);
  }
  
  /**
   * Select the text when editing on a text related cell is started
   */
  private void selectAll(EventObject e) {
    final Component editor = getEditorComponent();
    
    if (editor == null || !(editor instanceof JTextComponent)) {
      return;
    }
    
    if (e == null) {
      ((JTextComponent) editor).selectAll();
      return;
    }
    
    // Typing in the cell was used to activate the editor
    
    if (e instanceof KeyEvent && isSelectAllForKeyEvent) {
      ((JTextComponent) editor).selectAll();
      return;
    }
    
    // F2 was used to activate the editor
    
    if (e instanceof ActionEvent && isSelectAllForActionEvent) {
      ((JTextComponent) editor).selectAll();
      return;
    }
    
    // A mouse click was used to activate the editor.
    // Generally this is a double click and the second mouse click is
    // passed to the editor which would remove the text selection unless
    // we use the invokeLater()
    
    if (e instanceof MouseEvent && isSelectAllForMouseEvent) {
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          ((JTextComponent) editor).selectAll();
        }
      });
    }
  }
  
  // -- Accesseurs
  
  public DefaultTableCellRenderer getRenduCelules() {
    return renduCelules;
  }
  
  public boolean isAutoriserTriColonne() {
    return autoriserTriColonne;
  }
  
  public void setAutoriserTriColonne(boolean autoriserTriColonne) {
    this.autoriserTriColonne = autoriserTriColonne;
  }
  
}
