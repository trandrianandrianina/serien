/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.DocumentFilter;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Description d'un RiTextArea (dérivé du JTextArea).
 * Fonctionne soit comme un composant normal (il doit avoir le nom d'un hostfield) et c'est géré de manière automatique, soit on lui donne
 * un nom quelconque et on gère le setData/getData de manière manuelle dans le panel.
 * Version suivante
 * - getData(longueur + lexique) permet de traiter les oData et la hashmap directement dans le get ( pas besoin de le faire dans le panel)
 * - setData(String[],lexique,boolean,boolean) permet d'initialiser directement les données dans le setData ( pas besoin de faire dans le
 * panel)
 */
public class RiTextArea extends JTextArea implements IRiComposant {
  // Constantes

  // Variables
  private Lexical lexique = null;
  private ConcurrentHashMap<oData, Object> listeoDataFlux = null;
  private String nomFlux = null;
  private String nomDernierFlux = null;
  private oData hostfield = null;
  private JComponent requestcomposant = null;
  private LinkedHashMap<String, String> donnees = null;
  private DefaultStyledDocument doc;
  private JPopupMenu popPressePapiers = new JPopupMenu();
  private Clipboard pressePapiers = Toolkit.getDefaultToolkit().getSystemClipboard();
  private JPopupMenu monClicDroit = null;

  /**
   * Initialise le lexique.
   */
  @Override
  public void setLexical(Lexical lexique) {
    this.lexique = lexique;
  }

  /**
   * Initialise le Hostfield.
   */
  @Override
  public void setHostField(oData hostfield) {
    this.hostfield = hostfield;
  }

  /**
   * Initialise le nom du record en cours d'analyse.
   */
  @Override
  public void setNomFlux(String nomflux) {
    this.nomFlux = nomflux;
  }

  /**
   * Initialise le nom du record en cours d'analyse.
   */
  @Override
  public void setNomDernierFlux(String nomflux) {
    this.nomDernierFlux = nomflux;
  }

  /**
   * Initialisation des infos sur le flux.
   */
  @Override
  public void setInfosFlux(String anomflux, String anomDernierflux, HashMap<String, oData> alisteoData,
      ConcurrentHashMap<oData, Object> alisteoDataFlux) {
    setInfosFlux(anomflux, anomDernierflux, alisteoData, alisteoDataFlux, null);
  }

  /**
   * Initialisation des infos sur le flux.
   */
  @Override
  public void setInfosFlux(String anomflux, String anomDernierflux, HashMap<String, oData> alisteoData,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, Lexical lexique) {
    setNomFlux(anomflux);
    setNomDernierFlux(anomDernierflux);
    // listeoData = alisteoData;
    listeoDataFlux = alisteoDataFlux;
    setLexical(lexique);
  }

  @Override
  public void setEditable(boolean editable) {
    super.setEditable(editable);
    // look de la zone
    if (editable) {
      setBackground(Color.WHITE);
    }
    else {
      setBackground(Constantes.CL_ZONE_SORTIE);
    }
  }

  /**
   * Initialise les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    if (hostfield != null) {
      if (hostfield.getEvalVisibility() == Constantes.OFF) {
        setVisible(false);
      }
      else {
        setVisible(true);
        setText(hostfield.getValeurToFrame().trim());

        // On vérifie si le oData est en E/S
        // listeoData.put(hostfield.getNom(), hostfield); // Nécessaire pour isPresent de Lexical
        if ((hostfield.getEvalReadOnly() == Constantes.OFF) && (nomFlux.equals(nomDernierFlux))) {
          // Filtre de saisie
          ((AbstractDocument) (getDocument()))
              .setDocumentFilter(new SaisieDefinition(hostfield.getLongueur(), !hostfield.isLowerCase(), true, false));
          listeoDataFlux.put(hostfield, this);
          setEditable(true);
          if (hostfield.getEvalFocus() == Constantes.ON) {
            requestcomposant = this;
          }
        }
        else {
          setEditable(false);
        }
      }
    }
  }

  /**
   * Initialise les données et les propriétés du composant.
   */
  public void setData(LinkedHashMap<String, String> donnees, boolean trim, boolean withspace) {
    if (donnees == null) {
      return;
    }

    this.donnees = donnees;

    StringBuffer buffer = new StringBuffer();
    int longueur = 0;
    setLineWrap(true);
    setWrapStyleWord(true);

    buffer.setLength(0);
    for (Map.Entry<String, String> entry : donnees.entrySet()) {
      longueur += entry.getValue().length();
      if (trim) {
        if (withspace) {
          buffer.append(entry.getValue().trim()).append(' ');
        }
        else {
          buffer.append(entry.getValue().trim());
        }
      }
      else {
        buffer.append(entry.getValue());
      }
    }

    ((AbstractDocument) (getDocument())).setDocumentFilter(new SaisieDefinition(longueur));
    setText(buffer.toString().trim());
  }

  /**
   * Initialise les données et les propriétés du composant directement dans le setData avec les oData souhaités.
   */
  public void setData(String[] tableau, Lexical lexique, boolean trim, boolean withspace) {
    if (tableau == null) {
      return;
    }

    // this.donnees = donnees;
    setLexical(lexique);
    donnees = new LinkedHashMap<String, String>();

    for (int i = 0; i < tableau.length; i++) {
      donnees.put(tableau[i], lexique.HostFieldGetData(tableau[i]));
    }

    StringBuffer buffer = new StringBuffer();
    int longueur = 0;
    setLineWrap(true);
    setWrapStyleWord(true);

    buffer.setLength(0);
    for (Map.Entry<String, String> entry : donnees.entrySet()) {
      longueur += entry.getValue().length();
      if (trim) {
        if (withspace) {
          buffer.append(entry.getValue().trim()).append(' ');
        }
        else {
          buffer.append(entry.getValue().trim());
        }
      }
      else {
        buffer.append(entry.getValue());
      }
    }

    ((AbstractDocument) (getDocument())).setDocumentFilter(new SaisieDefinition(longueur));
    setText(buffer.toString().trim());
  }

  /**
   * Initialise les données et les propriétés du composant SANS TRIM().
   */
  public void setDataNoTrim() {
    if (hostfield != null) {
      if (hostfield.getEvalVisibility() == Constantes.OFF) {
        setVisible(false);
      }
      else {
        setVisible(true);
        setText(hostfield.getValeurToFrame().trim());

        // On vérifie si le oData est en E/S
        // listeoData.put(hostfield.getNom(), hostfield); // Nécessaire pour isPresent de Lexical
        if ((hostfield.getEvalReadOnly() == Constantes.OFF) && (nomFlux.equals(nomDernierFlux))) {
          // Filtre de saisie
          ((AbstractDocument) (getDocument())).setDocumentFilter(new SaisieDefinition(hostfield.getLongueur(), !hostfield.isLowerCase()));
          listeoDataFlux.put(hostfield, this);
          setEditable(true);
          if (hostfield.getEvalFocus() == Constantes.ON) {
            requestcomposant = this;
          }
        }
        else {
          setEditable(false);
        }
      }
    }

  }

  /**
   * Initialise les données et les propriétés du composant SANS TRIM().
   */
  public void setDataNoTrim(LinkedHashMap<String, String> donnees, boolean trim, boolean withspace) {
    if (donnees == null) {
      return;
    }

    this.donnees = donnees;

    StringBuffer buffer = new StringBuffer();
    int longueur = 0;
    setLineWrap(true);
    setWrapStyleWord(true);

    buffer.setLength(0);
    for (Map.Entry<String, String> entry : donnees.entrySet()) {
      longueur += entry.getValue().length();
      if (trim) {
        if (withspace) {
          buffer.append(entry.getValue().trim()).append(' ');
        }
        else {
          buffer.append(entry.getValue().trim());
        }
      }
      else {
        buffer.append(entry.getValue());
      }
    }

    ((AbstractDocument) (getDocument())).setDocumentFilter(new SaisieDefinition(longueur));
    setText(buffer.toString().trim());
  }

  /**
   * Initialise les données et les propriétés du composant directement dans le setData avec les oData souhaités SANS
   * TRIM().
   */
  public void setDataNoTrim(String[] tableau, Lexical lexique, boolean trim, boolean withspace) {
    if (tableau == null) {
      return;
    }

    // this.donnees = donnees;
    setLexical(lexique);
    donnees = new LinkedHashMap<String, String>();

    for (int i = 0; i < tableau.length; i++) {
      donnees.put(tableau[i], lexique.HostFieldGetData(tableau[i]));
    }

    StringBuffer buffer = new StringBuffer();
    int longueur = 0;
    setLineWrap(true);
    setWrapStyleWord(true);

    buffer.setLength(0);
    for (Map.Entry<String, String> entry : donnees.entrySet()) {
      longueur += entry.getValue().length();
      if (trim) {
        if (withspace) {
          buffer.append(entry.getValue()).append(' ');
        }
        else {
          buffer.append(entry.getValue());
        }
      }
      else {
        buffer.append(entry.getValue());
      }
    }

    ((AbstractDocument) (getDocument())).setDocumentFilter(new SaisieDefinition(longueur));
    setText(buffer.toString());
  }

  /**
   * Met à jour le oData du flux courant.
   */
  @Override
  public void getData() {
    if (hostfield != null) {
      hostfield.setValeurFromFrame(getText());
    }
  }

  /**
   * Met à jour la hashmap pour mettre à jour les oData depuis le panel.
   */
  public void getData(int longueurDta) {
    if (donnees != null) {
      int i = 0;
      int fin = 0;
      int deb = 0;

      String result = getText().trim();

      i = 0;
      for (Map.Entry<String, String> entry : donnees.entrySet()) {
        if (result.length() >= longueurDta) {
          deb = i * longueurDta;
          fin = deb + longueurDta;
          if (fin < result.length()) {
            donnees.put(entry.getKey(), result.substring(deb, fin));
          }
          else {
            donnees.put(entry.getKey(), result.substring(deb));
          }
          i++;
        }
        // Données inférieures à la longueur d'une variable
        else {
          donnees.put(entry.getKey(), result);
          break;
        }
      }
    }
  }

  /**
   * permet de mettre à jour la hashmap et le oData directement via la textArea.
   **/
  public void getData(int longueurDta, Lexical lexique) {
    if (donnees != null) {
      // setLexical(lexique);
      String result = getText().trim();

      // càs où la saisie dépasse la longueur max des données
      if (result.length() > (donnees.size() * longueurDta)) {
        result.substring(0, donnees.size() * longueurDta);
      }

      // découper la chaine si il y a des sauts de ligne en plusieurs variables
      String expReg = "\\n";
      Pattern p = null; // Pattern d'expressions régulières
      try {
        p = Pattern.compile(expReg);
        String[] blocs = p.split(result);
        // !!!!! IL FAUT TRAITER le cas où la longueur de la ligne dépasse le nb total de caractères et rebalancer un
        // tableau propre -> A voir

        int i = 0;
        for (Map.Entry<String, String> entry : donnees.entrySet()) {
          // mettre à jour la hashmap
          if (i < blocs.length) {
            donnees.put(entry.getKey(), blocs[i]);
          }
          else {
            donnees.put(entry.getKey(), "");
          }
          // mettre à jour le oData
          lexique.HostFieldPutData(entry.getKey(), 0, entry.getValue());
          i++;
        }
      }
      catch (PatternSyntaxException pse) {
        pse.printStackTrace();
      }
    }
  }

  /**
   * Met à jour la hashmap pour mettre à jour les oData depuis le panel SANS TRIM().
   */
  public void getDataNoTrim(int longueurDta) {
    if (donnees != null) {
      int i = 0;
      int fin = 0;
      int deb = 0;

      String result = getText();

      i = 0;
      for (Map.Entry<String, String> entry : donnees.entrySet()) {
        if (result.length() >= longueurDta) {
          deb = i * longueurDta;
          fin = deb + longueurDta;
          if (fin < result.length()) {
            donnees.put(entry.getKey(), result.substring(deb, fin));
          }
          else {
            donnees.put(entry.getKey(), result.substring(deb));
          }
          i++;
        }
        // Données inférieures à la longueur d'une variable
        else {
          donnees.put(entry.getKey(), result);
          break;
        }
      }
    }
  }

  /**
   * permet de mettre à jour la hashmap et le oData directement via la textArea SANS TRIM().
   **/
  public void getDataNoTrim(int longueurDta, Lexical lexique) {
    if (donnees != null) {
      // setLexical(lexique);
      String result = getText();

      // càs où la saisie dépasse la longueur max des données
      if (result.length() > (donnees.size() * longueurDta)) {
        result.substring(0, donnees.size() * longueurDta);
      }

      // découper la chaine si il y a des sauts de ligne en plusieurs variables
      String expReg = "\\n";
      Pattern p = null; // Pattern d'expressions régulières
      try {
        p = Pattern.compile(expReg);
        String[] blocs = p.split(result);
        // !!!!! IL FAUT TRAITER le cas où la longueur de la ligne dépasse le nb total de caractères et rebalancer un
        // tableau propre -> A voir

        int i = 0;
        for (Map.Entry<String, String> entry : donnees.entrySet()) {
          // mettre à jour la hashmap
          if (i < blocs.length) {
            donnees.put(entry.getKey(), blocs[i]);
          }
          else {
            donnees.put(entry.getKey(), "");
          }
          // mettre à jour le oData
          lexique.HostFieldPutData(entry.getKey(), 0, entry.getValue());
          i++;
        }
      }
      catch (PatternSyntaxException pse) {
        pse.printStackTrace();
      }
    }
  }

  /**
   * Fixe le nombre maximum de caractères que l'on peut entrer dans le RITextArea
   */
  public void setLongueurMaximumTexte(int aSize) {
    doc = new DefaultStyledDocument();
    doc.setDocumentFilter(new DocumentSizeFilter(aSize));
    doc.addDocumentListener(new DocumentListener() {
      @Override
      public void changedUpdate(DocumentEvent e) {
        updateCount();
      }

      @Override
      public void insertUpdate(DocumentEvent e) {
        updateCount();
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
        updateCount();
      }
    });
    this.setDocument(doc);

    updateCount();

  }

  @Override
  public void setEnabled(boolean pEnabled) {
    setEditable(pEnabled);
    setFocusable(pEnabled);
    if (!pEnabled) {
      setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
    }
    else {
      setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_EDITABLE);
    }
    // gérer le clic droit de la zone
    gererClicDroit(pEnabled);
  }

  private void gererClicDroit(boolean pEnabled) {
    // si la zone est bloquée ou non
    if (!pEnabled) {
      setComponentPopupMenu(popPressePapiers);
    }
    else {
      setComponentPopupMenu(monClicDroit);
    }
  }

  /**
   * Renvoi le nombre de caractères encore saisissables
   */
  private String updateCount() {
    return ("Vous pouvez encore saisir " + (500 - doc.getLength()) + " caractères.");
  }

  /**
   * Retourne si le composant a le focus sinon la valeur initiale.
   */
  @Override
  public JComponent getRequestComposant(JComponent composant) {
    if (requestcomposant == null) {
      return composant;
    }
    else {
      return requestcomposant;
    }
  }

  /**
   * Gestion de la saisie.
   */
  public class DocumentSizeFilter extends DocumentFilter {
    int maxCharacters;

    public DocumentSizeFilter(int maxChars) {
      maxCharacters = maxChars;
    }

    @Override
    public void insertString(FilterBypass fb, int offs, String str, AttributeSet a) throws BadLocationException {

      // This rejects the entire insertion if it would make
      // the contents too long. Another option would be
      // to truncate the inserted string so the contents
      // would be exactly maxCharacters in length.
      if ((fb.getDocument().getLength() + str.length()) <= maxCharacters) {
        super.insertString(fb, offs, str, a);
      }
      else {
        Toolkit.getDefaultToolkit().beep();
      }
    }

    @Override
    public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a) throws BadLocationException {

      // This rejects the entire replacement if it would make
      // the contents too long. Another option would be
      // to truncate the replacement string so the contents
      // would be exactly maxCharacters in length.
      if ((fb.getDocument().getLength() + str.length() - length) <= maxCharacters) {
        super.replace(fb, offs, length, str, a);
      }
      else {
        Toolkit.getDefaultToolkit().beep();
      }
    }

  }

}
