/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome.liste;

import java.awt.Cursor;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragSource;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.TransferHandler;
import javax.swing.table.DefaultTableModel;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;

public class NRiTableRowTransferHandler extends TransferHandler {
  private final DataFlavor localObjectFlavor =
      new ActivationDataFlavor(Integer.class, "application/x-java-Integer;class=java.lang.Integer", "Integer Row Index");
  private NRiTable table = null;

  public NRiTableRowTransferHandler(NRiTable table) {
    this.table = table;
  }

  @Override
  protected Transferable createTransferable(JComponent c) {
    assert (c == table);
    return new DataHandler(new Integer(table.getSelectedRow()), localObjectFlavor.getMimeType());
  }

  @Override
  public boolean canImport(TransferHandler.TransferSupport info) {
    boolean b = info.getComponent() == table && info.isDrop() && info.isDataFlavorSupported(localObjectFlavor);
    table.setCursor(b ? DragSource.DefaultMoveDrop : DragSource.DefaultMoveNoDrop);
    return b;
  }

  @Override
  public int getSourceActions(JComponent c) {
    return TransferHandler.COPY_OR_MOVE;
  }

  @Override
  public boolean importData(TransferHandler.TransferSupport info) {
    JTable target = (JTable) info.getComponent();
    JTable.DropLocation dropLocation = (JTable.DropLocation) info.getDropLocation();
    int indexDestination = dropLocation.getRow();
    int nombreLignes = table.getModel().getRowCount();
    if (indexDestination < 0 || indexDestination > nombreLignes) {
      indexDestination = nombreLignes;
    }
    target.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    
    try {
      Integer indexOrigine = (Integer) info.getTransferable().getTransferData(localObjectFlavor);
      if (indexOrigine != -1 && indexOrigine != indexDestination) {
        
        Object[] ligne = new Object[table.getModel().getColumnCount()];
        for (int colonne = 0; colonne < table.getModel().getColumnCount(); colonne++) {
          Object celluleOrigine = table.getModel().getValueAt(indexOrigine, colonne);
          ligne[colonne] = celluleOrigine;
        }
        
        if (indexOrigine > indexDestination) {
          ((DefaultTableModel) table.getModel()).moveRow(indexOrigine, indexOrigine, indexDestination);
        }
        else {
          ((DefaultTableModel) table.getModel()).moveRow(indexOrigine, indexOrigine, indexDestination - 1);
        }
        
      }
      
      return true;
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
    return false;
  }

  @Override
  protected void exportDone(JComponent c, Transferable t, int act) {
    if ((act == TransferHandler.MOVE) || (act == TransferHandler.NONE)) {
      table.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
  }
}
