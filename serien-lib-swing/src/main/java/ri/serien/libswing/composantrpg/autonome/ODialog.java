/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.EventQueue;
import java.awt.Point;
import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * Stocke les informations pour une popup
 */
public class ODialog {
  
  // Variables
  private int idClassName = -1;
  private SNPanelEcranRPG ecran = null;
  private JDialog fenetre = null;
  // private JFrame parent=null;
  private Window parent = null;
  private boolean isPerso = false;
  // private boolean modal = false;
  public boolean godispose = false;
  
  private char blocageEcran = ' ';
  
  /**
   * Constructeur classique utilisé par la classe SessionPanel
   * @param nomRecord
   * @param ecran
   * @param parent
   */
  public ODialog(int idclassname, SNPanelEcranRPG ecran, JFrame parent, char blocage) {
    setIdClassName(idclassname);
    setEcran(ecran);
    this.parent = parent;
    setBlocageEcran(blocage);
  }
  
  /**
   * Constructeur (pour les popup liés à un panel cad avec le meme buffer que le panel principal)
   * @param nomRecord
   * @param ecran
   * @param parent
   */
  public ODialog(Window aparent, SNPanelEcranRPG apopup) {
    this.parent = aparent;
    setEcran(apopup);
    isPerso = true;
  }
  
  /**
   * Ajout d'un listener sur la fenêtre
   *
   * private void addListener(JDialog afenetre)
   * {
   * /*
   * afenetre.addWindowFocusListener(new WindowAdapter() {
   * public void windowGainedFocus(WindowEvent e) {
   * ecran.activeRequestComponent();
   * }
   * });
   *
   * // Permet une meilleure gestion du focus (panel déjà affiché)
   * /*
   * afenetre.addHierarchyListener(new HierarchyListener()
   * {
   * //@Override
   * public void hierarchyChanged(HierarchyEvent e)
   * {
   * // Dans le cas des panels classiques
   * if ((e.getChangeFlags() & HierarchyEvent.DISPLAYABILITY_CHANGED) != 0)
   * //if ((e.getChanged() instanceof oFrame) && (((oFrame)e.getChanged()).idClassName == getFlux().getIdClassName()))
   * if (e.getComponent().isDisplayable())
   * {
   * e.paramString() + "\n\t-->" + e.getChanged().isShowing() );
   * //ecran.activeRequestComponent();
   * ecran.activeRequestComponent();
   * }
   * }
   * });
   */
  /*
      afenetre.addComponentListener(new ComponentAdapter() {
        //@Override
        public void componentShown(ComponentEvent e) {
          //  SwingUtilities.invokeLater(new Runnable() {
          //      public void run()
                {
  //                  if ((ecran != null) && (ecran.isDialog()))
                //ecran.requestFocus();
                    ecran.activeRequestComponent();
                }
            //});
        }
      });*/
  
  /*
      afenetre.addWindowListener(new java.awt.event.WindowAdapter() {
          public void windowOpened(WindowEvent e) {
                //if ((ecran != null) && (ecran.isDialog()))
                  ecran.activeRequestComponent();
              }
          });
  }*/
  
  /**
   * @param className the classname to set
   */
  public void setIdClassName(int idclassname) {
    idClassName = idclassname;
  }
  
  /**
   * Retourne le nom du classname
   * @return
   */
  public int getIdClassName() {
    return idClassName;
  }
  
  /**
   * @param ecran the ecran to set
   */
  public void setEcran(SNPanelEcranRPG ecran) {
    this.ecran = ecran;
    ecran.setJDialog(fenetre);
  }
  
  /**
   * @return the ecran
   */
  public SNPanelEcranRPG getEcran() {
    return ecran;
  }
  
  /**
   * @param fenetre the fenetre to set
   */
  public void setFenetre(JDialog fenetre) {
    this.fenetre = fenetre;
  }
  
  /**
   * @return the fenetre
   */
  public JDialog getFenetre() {
    if (fenetre == null) {
      initFenetre();
    }
    return fenetre;
  }
  
  /**
   * Affiche la popup
   */
  public void affiche() {
    boolean creation = false;
    
    if (fenetre == null) {
      initFenetre();
      creation = true;
      // ecran.requestFocus();
    }
    if (ecran == null) {
      return;
    }
    
    ecran.setJDialog(fenetre);
    fenetre.getContentPane().removeAll();
    fenetre.getContentPane().add(ecran);
    fenetre.setResizable(false);
    fenetre.pack();
    fenetre.setTitle(ecran.getTitle());
    if (creation) {
      // Calcul de la position de la popup en fonction du parent
      calculPositionXY();
    }
    
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        if (!isPerso) {
          fenetre.setVisible(true);
        }
        fenetre.toFront();
        fenetre.repaint();
        
        ecran.activeRequestComponent4Dialog();
        godispose = true;
      }
    });
    
  }
  
  /**
   * Détruit l'objet
   */
  public void dispose() {
    // Permet de faire patienter le dispose le temps que la méthode affiche() se termine, sinon la popup reste affichée
    // (pas bô faudra un jour faire mieux)
    while (!godispose) {
      try {
        Thread.sleep(100);
      }
      catch (InterruptedException e) {
      }
    }
    
    if (fenetre != null) {
      fenetre.setVisible(false);
      fenetre.dispose();
    }
    fenetre = null;
    ecran.getLexique().setPopupPerso(null);
    if (isPerso) {
      ecran.dispose();
    }
    parent = null;
    ecran = null;
  }
  
  /**
   * Affiche ou cache la fenetre
   * @param visible
   */
  public void setVisible(boolean visible) {
    if (fenetre != null) {
      fenetre.setVisible(visible);
    }
  }
  
  /**
   * Initialise la JDialog
   */
  private void initFenetre() {
    godispose = false;
    if (parent == null) {
      fenetre = new JDialog();
    }
    else {
      fenetre = new JDialog(parent);
    }
    
    fenetre.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    // addListener(fenetre);
  }
  
  /**
   * Calcule la position X, Y de la popup
   */
  private void calculPositionXY() {
    if (parent != null) {
      parent.setVisible(true);
      Point position = parent.getLocationOnScreen();
      fenetre.setLocation((int) position.getX() + (parent.getWidth() / 2) - (fenetre.getWidth() / 2),
          (int) position.getY() + (parent.getHeight() / 2) - (fenetre.getHeight() / 2));
    }
  }
  
  /**
   * @param blocageEcran the blocageEcran to set
   */
  public void setBlocageEcran(char blocageEcran) {
    this.blocageEcran = blocageEcran;
  }
  
  /**
   * @return the blocageEcran
   */
  public char getBlocageEcran() {
    return blocageEcran;
  }
  
  /**
   * Effectue une mise à jour des données de la popup (pour les popup liés à un panel)
   */
  public void affichePopupPerso() {
    ecran.oFrameParent.usePopupPerso = true;
    ecran.oFrameParent.oFramePopupPerso = ecran;
    ecran.oFrameParent.setRenvoyable(false);
    
    ecran.setData();
    affiche();
    
    // Gestion du focus sur le panel actif et du bouton par défaut
    fenetre.getRootPane().setDefaultButton(ecran.getDefaultButton());
    ecran.setRenvoyable(true); // Sinon on ne peut pas faire F4 (entre autres) sur les popups personnalisées
    
    // fenetre.setModal(true);
    fenetre.setVisible(true);
  }
  
}
