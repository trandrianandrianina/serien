/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * Description d'un XRiComboBox (dérivé du JComboBox).
 * Fonctionne soit comme un composant normal (il doit avoir le nom d'un hostfield) et c'est géré de manière automatique, soit on lui donne
 * un nom quelconque et on gère le setData/getData de manière manuelle dans le panel.
 */
public class XRiComboBox extends JComboBox implements IXRiComposant {
  // Variables
  private Lexical lexique = null;
  private oData hostfield = null;
  
  // Liste des buffers constinuant le panel final (à voir si on peut s'en passer en gérant mieux les setData)
  private HashMap<String, oRecord> listeRecord = null;
  // Interpréteur de variables (à voir si on peut pas le récupérer avec lexical)
  private iData interpreteurD = null;
  // <- ne sert pas ici car fait le lien entre le hostfield et le composant
  private ConcurrentHashMap<oData, Object> listeoDataFlux = null;
  // Stocke l'objet qui a le focus
  private JComponent[] requestcomposant = null;
  // Valeurs affichées à l'utilisateur mais venant de zones variables (sous la forme @XXXX@)
  private String[] dataDisplay = null;
  // Valeurs renvoyées/reçues par l'AS400
  private String[] valueHostfield = null;
  private boolean useDataDisplay = true;
  private boolean ignorerCasse = true;
  
  /**
   * Constructeur.
   */
  public XRiComboBox() {
    super();
  }
  
  /**
   * Methode de oFrame un peu adaptées.
   * Stocke les variables qui composent la table.
   */
  @Override
  public void init(final Lexical pLexique, HashMap<String, oRecord> pListeRecord, iData pInterpreteurD,
      ConcurrentHashMap<oData, Object> pListeoDataFlux, JComponent[] pRequestComposant) {
    // Initialisation des variables nécessaires
    this.lexique = pLexique;
    setInterpreteurD(pInterpreteurD);
    setListeRecord(pListeRecord);
    setListeoDataFlux(pListeoDataFlux);
    requestcomposant = pRequestComposant;
    // clic droit enlevé systématiquement
    this.setComponentPopupMenu(null);
    
    // Interception des touches pour ce composant
    setGestionTouchesFonctions();
  }
  
  /**
   * Initialise les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    
    hostfield = lexique.getHostField(this.getName()); // nom du host
    if (hostfield != null) {
      if (hostfield.getEvalVisibility() == Constantes.OFF) {
        setVisible(false);
      }
      else {
        setVisible(true);
        conversionData();
        if (getItemCount() > 0) {
          // Dans le cas d'une hostconversion
          if (useDataDisplay) {
            setSelectedIndex(getIndexTableau(hostfield.getValeurToFrame(), valueHostfield));
          }
          else {
            setSelectedItem(hostfield.getValeurToFrame().trim());
            // setSelectedIndex(lexique.getIndice(getName(), dataDisplay));
          }
        }
      }
      // On vérifie si le oData est en E/S
      if (lexique.isIntoCurrentRecord(hostfield) && (hostfield.getEvalReadOnly() == Constantes.OFF)) {
        // Filtre de saisie
        listeoDataFlux.put(hostfield, this); // Nécessaire pour le getData de oFrame
        setEnabled(true);
        if (hostfield.getEvalFocus() == Constantes.ON) {
          requestcomposant[0] = this;
        }
      }
      else {
        setEnabled(false);
      }
    }
  }
  
  /**
   * Met à jour le oData du flux courant.
   */
  @Override
  public void getData() {
    if (!isVisible()) {
      return;
    }
    // Est ce mieux le setValeurFromFrame par raapport au lexique.HostFieldPutData comme dans le XRiCheckBox ? A voir
    if (valueHostfield == null) {
      hostfield.setValeurFromFrame((String) getSelectedItem());
    }
    else {
      hostfield.setValeurFromFrame(valueHostfield[getSelectedIndex()]);
    }
  }
  
  /**
   * Charge la combobox avec conversion des données si nécessaire.
   */
  private void conversionData() {
    if (dataDisplay != null) {
      String[] donnees = new String[dataDisplay.length];
      for (int i = 0; i < dataDisplay.length; i++) {
        donnees[i] = lexique.TranslationTable(lexique.PanelFieldGetData(dataDisplay[i])).trim();
      }
      setModel(new DefaultComboBoxModel(donnees));
    }
    else if (((getModel() == null) || (getItemCount() == 0)) && (valueHostfield != null)) {
      setModel(new DefaultComboBoxModel(valueHostfield));
      useDataDisplay = false;
    }
    else if ((dataDisplay == null) && (valueHostfield == null)) {
      useDataDisplay = false;
    }
  }
  
  /**
   * Retourne l'index d'un élément d'un tableau de String.
   */
  private int getIndexTableau(String pChaine, String[] pTableau) {
    if (pTableau == null || pChaine == null) {
      return 0;
    }
    pChaine = pChaine.trim();
    // La casse des valeurs comparées est ignorée
    if (ignorerCasse) {
      for (int i = 0; i < pTableau.length; i++) {
        if (pTableau[i].trim().equalsIgnoreCase(pChaine)) {
          return i;
        }
      }
    }
    // La casse des valeurs comparées est prise en compte
    else {
      for (int i = 0; i < pTableau.length; i++) {
        if (pTableau[i].trim().equals(pChaine)) {
          return i;
        }
      }
    }
    return 0;
  }
  
  /**
   * Force l'apparition du composant (à mettre dans le setData du panel) bien que cela défi les lois de la nature.
   */
  public void forceVisibility() {
    if (isVisible()) {
      return;
    }
    
    setVisible(true);
    setEnabled(false);
    
    // Force le rafraichissement de la valeur
    conversionData();
    if (getItemCount() > 0) {
      if (useDataDisplay) {
        setSelectedIndex(getIndexTableau(hostfield.getValeurToFrame(), valueHostfield));
      }
      else {
        setSelectedItem(hostfield.getValeurToFrame().trim());
      }
    }
  }
  
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);

    if (!pEnabled) {
      super.setRenderer(new DefaultListCellRenderer() {
        @Override
        public void paint(Graphics g) {
          setForeground(SNCharteGraphique.COULEUR_TEXTE_CHAMP_DESACTIVE);
          getEditor().getEditorComponent().setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
          Rectangle bounds = getBounds();
          g.setColor(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
          g.fillRect(1, 1, bounds.width, bounds.height);
          super.paint(g);
        }
      });
    }
    else {
      super.setRenderer(new DefaultListCellRenderer() {

        @Override
        public void paint(Graphics g) {
          setForeground(Color.BLACK);
          getEditor().getEditorComponent().setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
          Rectangle bounds = getBounds();
          g.setColor(SNCharteGraphique.COULEUR_FOND_CHAMP_EDITABLE);
          g.fillRect(1, 1, bounds.width, bounds.height);
          super.paint(g);
        }

      });
    }
  }
  
  /**
   * Initialisation des données brutes ou données normales et des valeurs pour la host conversion.
   * Par défaut la comparaison des valeurs ignore la casse.
   */
  public void setValeurs(String[] pValueHost, String[] pLibelle) {
    setValeurs(pValueHost, pLibelle, true);
  }
  
  /**
   * Initialisation des données brutes ou données normales et des valeurs pour la host conversion.
   */
  public void setValeurs(String[] pValueHost, String[] pLibelle, boolean pIgnorerCasse) {
    // Valeurs hostfield
    valueHostfield = pValueHost;
    // Valeurs des libellés affichés si précisé
    dataDisplay = pLibelle;
    if (dataDisplay == null && valueHostfield == null) {
      useDataDisplay = false;
    }
    ignorerCasse = pIgnorerCasse;
  }
  
  /**
   * Gère les touches de fonctions spéciales pour ce composants.
   * C'est pas parfait car le texte de la combo pagine qd même mais les PGUP & PG DOWN fonctionnent sur la liste (à
   * améliorer voir le VCGM91 2411 pour test).
   */
  private void setGestionTouchesFonctions() {
    addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent e) {
        KeyEvent key = e;
        e.consume();
        switch (key.getKeyCode()) {
          // case KeyEvent.VK_ENTER :
          // lexique.HostScreenSendKey("ENTER");
          // break;
          case KeyEvent.VK_PAGE_DOWN:
            lexique.HostScreenSendKey(lexique.getPanel(), "PGDOWN");
            break;
          case KeyEvent.VK_PAGE_UP:
            lexique.HostScreenSendKey(lexique.getPanel(), "PGUP");
            break;
          default:
            break;
        }
      }
    });
  }
  
  public void setListeRecord(HashMap<String, oRecord> listeRecord) {
    this.listeRecord = listeRecord;
  }
  
  public HashMap<String, oRecord> getListeRecord() {
    return listeRecord;
  }
  
  public iData getInterpreteurD() {
    return interpreteurD;
  }
  
  public void setInterpreteurD(iData interpreteurD) {
    this.interpreteurD = interpreteurD;
  }
  
  public ConcurrentHashMap<oData, Object> getListeoDataFlux() {
    return listeoDataFlux;
  }
  
  public void setListeoDataFlux(ConcurrentHashMap<oData, Object> listeoDataFlux) {
    this.listeoDataFlux = listeoDataFlux;
  }
  
  @Override
  public void dispose() {
    lexique = null;
    hostfield = null;
    listeRecord = null;
    interpreteurD = null;
    listeoDataFlux = null;
    requestcomposant = null;
    dataDisplay = null;
    valueHostfield = null;
  }
}
