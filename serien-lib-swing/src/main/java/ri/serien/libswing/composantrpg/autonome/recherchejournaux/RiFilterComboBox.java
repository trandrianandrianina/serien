/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome.recherchejournaux;

import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * Composant combobox intégrant l'autocomplétion.
 */
public class RiFilterComboBox extends JComboBox {
  // Variables
  private List<String> filterArray = new ArrayList<String>();
  private String[] data = null;
  
  /**
   * Constructeur.
   */
  public RiFilterComboBox() {
    this.setEditable(true);
    final JTextField textfield = (JTextField) this.getEditor().getEditorComponent();
    textfield.addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent ke) {
        setPopupVisible(true);
        int keyCode = ke.getKeyCode();
        if ((keyCode == KeyEvent.VK_A) && ((ke.getModifiers() & InputEvent.CTRL_DOWN_MASK) != 0)) {
          ke.consume();
          return;
        }
        if (((keyCode >= KeyEvent.VK_0) && (keyCode <= KeyEvent.VK_9)) || (keyCode == KeyEvent.VK_SPACE)
            || ((keyCode >= KeyEvent.VK_A) && (keyCode <= KeyEvent.VK_Z)) || (keyCode == KeyEvent.VK_BACK_SPACE)
            || (keyCode == KeyEvent.VK_DELETE)) {
          SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
              comboFilter(textfield.getText());
            }
          });
          return;
        }
        if (keyCode == KeyEvent.VK_ENTER) {
          if (getSelectedIndex() == -1) {
            setSelectedIndex(0);
          }
          textfield.setText((String) getSelectedItem());
          setPopupVisible(false);
          return;
        }
      }
    });
  }
  
  /**
   * Initialise le tableau des valeurs et remplit la combobox.
   */
  public void init(Object[][] adata, int col, boolean selected) {
    data = new String[adata.length];
    for (int i = 0; i < data.length; i++) {
      data[i] = (String) adata[i][col];
    }
    DefaultComboBoxModel model = (DefaultComboBoxModel) this.getModel();
    model.removeAllElements();
    if (data != null) {
      for (int i = 0; i < data.length; i++) {
        model.addElement(data[i]);
      }
    }
    
    if (selected) {
      ((JTextField) this.getEditor().getEditorComponent()).selectAll();
    }
    
  }
  
  /**
   * Initialise le tableau des valeurs et remplit la combobox.
   */
  public void init(Object[] adata, boolean selected) {
    data = new String[adata.length];
    for (int i = 0; i < data.length; i++) {
      data[i] = (String) adata[i];
    }
    DefaultComboBoxModel model = (DefaultComboBoxModel) this.getModel();
    model.removeAllElements();
    if (data != null) {
      for (int i = 0; i < data.length; i++) {
        model.addElement(data[i]);
      }
    }
    
    if (selected) {
      ((JTextField) this.getEditor().getEditorComponent()).selectAll();
    }
    
  }
  
  /**
   * Gestion du filtrage.
   */
  public void comboFilter(String enteredText) {
    filterArray.clear();
    for (int i = 0; i < data.length; i++) {
      if (data[i].toLowerCase().contains(enteredText.toLowerCase())) {
        filterArray.add(data[i]);
      }
    }
    if (filterArray.size() > 0) {
      DefaultComboBoxModel model = (DefaultComboBoxModel) this.getModel();
      model.removeAllElements();
      for (String s : filterArray) {
        model.addElement(s);
      }
      
      JTextField textfield = (JTextField) this.getEditor().getEditorComponent();
      textfield.setText(enteredText);
    }
  }
}
