/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.util.HashMap;

public class PopupPerso {
  // Variables
  private SNPanelEcranRPG frame = null;
  private HashMap<String, ProprieteTouchePopupPerso> proprieteTouchePopupPerso = new HashMap<String, ProprieteTouchePopupPerso>();
  
  /**
   * Constructeur.
   */
  public PopupPerso(SNPanelEcranRPG pFrame) {
    frame = pFrame;
  }
  
  // -- Méthodes publiques
  
  /**
   * Effectue le traitement en fonction des variables initialiser lors d'un appuie sur une touche de fonction.
   */
  public void traiterTouche(String pTouche) {
    if (pTouche == null) {
      return;
    }
    boolean miseAJour = true;
    boolean rafraichirPanelParent = true;
    // On recherche dans la liste des touches, les propriétés liées
    ProprieteTouchePopupPerso proprieteTouche = proprieteTouchePopupPerso.get(pTouche);
    // Si on ne l'a pas trouvé dans la liste, on ne met pas à jour le buffer
    if (proprieteTouche != null) {
      miseAJour = proprieteTouche.isMiseAJourbuffer();
      rafraichirPanelParent = proprieteTouche.isRafraichirPanelParent();
    }
    if (miseAJour) {
      frame.getData();
    }
    if (frame.isCloseKey(pTouche)) {
      frame.closePopupLinkWithBuffer(rafraichirPanelParent);
    }
  }
  
  /**
   * Ajoute les propriétés d'une touche dans la liste.
   */
  public void ajouterProprieteTouche(String pTouche, boolean pMiseAJour, boolean pRafraichirPanelParent, boolean pEnvoyerToucheAuRPG) {
    if (pTouche == null) {
      return;
    }
    ProprieteTouchePopupPerso propriete = new ProprieteTouchePopupPerso();
    propriete.setMiseAJourbuffer(pMiseAJour);
    propriete.setRafraichirPanelParent(pRafraichirPanelParent);
    propriete.setEnvoyerToucheAuRPG(pEnvoyerToucheAuRPG);
    proprieteTouchePopupPerso.put(pTouche, propriete);
  }
  
  /**
   * Indique di l'on doit envoyer la touche au programme RPG.
   */
  public boolean isEnvoyerToucheAuRPG(String pTouche) {
    ProprieteTouchePopupPerso proprieteTouche = proprieteTouchePopupPerso.get(pTouche);
    if (proprieteTouche == null) {
      return false;
    }
    return proprieteTouche.isEnvoyerToucheAuRPG();
  }
}
