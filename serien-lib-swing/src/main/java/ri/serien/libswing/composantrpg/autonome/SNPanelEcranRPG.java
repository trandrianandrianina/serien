/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.ioGeneric;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oKey;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.lexical.IRiComposant;
import ri.serien.libswing.composantrpg.lexical.IXRiComposant;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.EnvProgramClient;
import ri.serien.libswing.moteur.interpreteur.FluxRecord;
import ri.serien.libswing.moteur.interpreteur.InterpreteurV03F;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * Panneau principal des écrans RPG.
 *
 * Ce panneau est le panneau parent des écrans RPG. Il gère les touches, le lexique...
 * Idéalement à passer en Abstract.
 */
public class SNPanelEcranRPG extends SNPanelFond {
  // Constantes
  private static final HashMap<String, KeyStroke> listeKeyEnrg = new HashMap<String, KeyStroke>() {
    {
      put("F1", KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
    }

    {
      put("F2", KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
    }

    {
      put("F3", KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
    }

    {
      put("F4", KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0));
    }

    {
      put("F5", KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
    }

    {
      put("F6", KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
    }

    {
      put("F7", KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0));
    }

    {
      put("F8", KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0));
    }

    {
      put("F9", KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0));
    }

    {
      put("F10", KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0));
    }

    {
      put("F11", KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0));
    }

    {
      put("F12", KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0));
    }

    {
      put("F13", KeyStroke.getKeyStroke(KeyEvent.VK_F1, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F14", KeyStroke.getKeyStroke(KeyEvent.VK_F2, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F15", KeyStroke.getKeyStroke(KeyEvent.VK_F3, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F16", KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F17", KeyStroke.getKeyStroke(KeyEvent.VK_F5, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F18", KeyStroke.getKeyStroke(KeyEvent.VK_F6, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F19", KeyStroke.getKeyStroke(KeyEvent.VK_F7, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F20", KeyStroke.getKeyStroke(KeyEvent.VK_F8, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F21", KeyStroke.getKeyStroke(KeyEvent.VK_F9, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F22", KeyStroke.getKeyStroke(KeyEvent.VK_F10, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F23", KeyStroke.getKeyStroke(KeyEvent.VK_F11, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("F24", KeyStroke.getKeyStroke(KeyEvent.VK_F12, InputEvent.SHIFT_DOWN_MASK));
    }

    {
      put("ENTER", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
    }

    {
      put("PGUP", KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0));
    }

    {
      put("PGDOWN", KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0));
    }

    {
      put("C2", KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.CTRL_DOWN_MASK));
    }
  };

  // Variables
  private boolean redimensionnable = true;
  private boolean modale = true;
  protected iData interpreteurD = null;
  private String titre = null;
  protected EnvProgramClient infosPrg = null;
  private SNPanelEcranRPG thisFrame = this;
  private PopupPerso popupPerso = null;

  // ... en execution
  public HashMap<String, oRecord> listeRecord = new HashMap<String, oRecord>();
  protected ConcurrentHashMap<oData, Object> listeoDataFlux = new ConcurrentHashMap<oData, Object>();
  protected ArrayList<Component> listeComposant = null;
  private FluxRecord fluxRpg = null;
  // SessionBase est vouée à disparaitre à terme au profit de ModeleSessionRPG
  private SessionBase session = null;
  protected Lexical lexique = null;
  private JButton defaultButton = null;
  // C'est un tableau de un pour le passage en paramètre au ioXRiComposant
  private JComponent[] requestcomposant = new JComponent[1];
  private ArrayList<ArrayList<Component>> tousMenus = new ArrayList<ArrayList<Component>>();
  private ArrayList<RiMenu> tetesMenus = new ArrayList<RiMenu>();

  private int dernierMenu = -1;
  private ArrayList<Component> modesAffichage = new ArrayList<Component>();
  private JButton button_erreurs = null;
  private JMenu menuCommand = null;
  private RiMenu V01F = null;
  private JLabel titrePanel = null;
  private boolean isDebug = false;
  private int versionLook = 1;
  private boolean envoyerF1 = false;
  protected boolean dialog = false;
  protected JDialog dialogue = null;
  // Empêche "java.util.ConcurrentModificationException" sur listeoDataFlux
  private boolean renvoyable = false;
  private String derniereToucheEnvoyee = null;
  // id du Nom du Flux/Panel ex: RGVM13FM.RGVM13FM_AB
  private int idClassName = -1;
  private oFrameActions oframeActions = null;
  public boolean usePopupPerso = false;

  public boolean noRefresh = false;

  public SNPanelEcranRPG oFrameParent = null;
  public SNPanelEcranRPG oFramePopupPerso = null;

  // Liste des touches qui doivent fermer la popup (dépend totalement du programme)
  private ArrayList<String> listCloseKeys = new ArrayList<String>(26);

  /**
   * Constructeur par défaut.
   * Ce constructeur est nécessaire pour permettre à JFormDesigner d'utiliser cette classe en tant que composant.
   */
  public SNPanelEcranRPG() {
  }

  /**
   * Constructeur de la classe
   */
  public SNPanelEcranRPG(ArrayList<?> param) {
    super();
    setProgram((EnvProgramClient) param.get(0));
    lexique = new Lexical(this);

    initActions();
  }

  /**
   * Constructeur de la classe (dans le cas des popup dépendante d'une oFrame déjà existante)
   */
  public SNPanelEcranRPG(SNPanelEcranRPG parent) {
    super();
    oFrameParent = parent;
    idClassName = oFrameParent.getIdClassName();
    lexique = parent.getLexique();
    lexique.setPopupPerso(this);
    setProgram(parent.getProgram());
    setFlux(parent.fluxRpg);
    // Mise à jour des record de oFrame avec ceux de FluxRecord
    fluxRpg.majRecords(listeRecord, idClassName);
    // TODO setSession disparaitra quand ModeleSessionRPG sera opérationnel
    setSession(parent.getSession());

    initActions();
  }

  // -- Méthodes publiques

  /**
   * Permet de mettre à jour les variables des libellés de certains composants.
   */
  public void mettreAJourVariableLibelle() {
    // Existe pour être appelée dans les panels finaux
    // Rendre cette classe abstract serait mieux mes des composants évolués l'instancie.
    // Il faut regarder à tête reposée et là je n'ai pas le temps...
  }

  // -- Méthodes privées

  /**
   * Initialise les évènements pour le panel
   */
  private void initActions() {
    // Voir ce que ça fait ??
    oframeActions = new oFrameActions(lexique, thisFrame);
  }

  /**
   * Retourne le flux courant (celui sur lequel on est en E/S)
   * @return
   */
  public FluxRecord getFlux() {
    return fluxRpg;
  }

  /**
   * Initialise le flux
   * @param afluxRpg
   */
  public void setFlux(FluxRecord afluxRpg) {
    fluxRpg = afluxRpg;
    lexique.setDataBuffer(fluxRpg);
  }

  /**
   * Génére la liste de tous les composants d'un objet
   */
  public ArrayList<Component> genereListeComposants(JComponent root) {
    ArrayList<Component> allComposants = new ArrayList<Component>();
    getComposantsRec(allComposants, root);

    return allComposants;
  }

  /**
   * Parcourt récursif des composants
   * @param aallComposants
   * @param root
   */
  private void getComposantsRec(ArrayList<Component> aallComposants, JComponent root) {
    aallComposants.add(root);
    Component[] listecomposants = root.getComponents();
    for (int i = 0; i < listecomposants.length; i++) {
      if (listecomposants[i] instanceof JComponent) {
        if (((JComponent) listecomposants[i]).getComponentCount() != 0) {
          getComposantsRec(aallComposants, (JComponent) listecomposants[i]);
        }
        else if (listecomposants[i].getName() != null) {
          aallComposants.add(listecomposants[i]);
        }
      }
    }
  }

  /**
   * Initialise les infos du point de menu sélectionné
   * @param ainfosPrg
   */
  public void setProgram(EnvProgramClient ainfosPrg) {
    infosPrg = ainfosPrg;
    isDebug = ManagerSessionClient.getInstance().getEnvUser().isDebugStatus();
    interpreteurD = infosPrg.getInterpreteurD();
  }

  /**
   * Retourne les infos du point de menu sélectionné
   */
  public EnvProgramClient getProgram() {
    return infosPrg;
  }

  /**
   * Initialise le redimensionnement
   * @param valeur
   */
  public void setResizable(boolean redimensionnable) {
    this.redimensionnable = redimensionnable;
  }

  /**
   * Initialise si modale
   * @param modal
   */
  public void setModal(boolean modal) {
    this.modale = modal;
  }

  /**
   * Retourne si redimensionnale
   * @return
   */
  public boolean isResizable() {
    return redimensionnable;
  }

  /**
   * Retourne si modale
   * @return
   */
  public boolean isModal() {
    return modale;
  }

  /**
   * Titre de la fenêtre
   * @param atitre
   */
  public void setTitle(String atitre) {
    titre = atitre;
  }

  public String getTitle() {
    return titre;
  }

  /**
   * Rafraichir les données de l'écran à partir du buffer envoyé par le RPG.
   */
  public void setData() {
    // Effacer la hashmap afin qu'elle soit propre pour la réception du prochain buffer
    listeoDataFlux.clear();

    // Initialiser l'interpréteur avec la liste des buffers à mettre à jour
    interpreteurD.setFlux(listeRecord);

    // Activer les touches de l'écran si on est pas en mode refresh (panel sous popup)
    if (isFocusable()) {
      setActiveKey();
    }

    // Récupèrer la liste des composants de l'écran
    if (listeComposant == null) {
      listeComposant = genereListeComposants(this);
    }

    // Parcourir la liste des buffers envoyés par le RPG
    for (Map.Entry<String, oRecord> entry : listeRecord.entrySet()) {
      LinkedHashMap<String, ioGeneric> listeElement = entry.getValue().listeElement;

      // Parcourir la liste des composants graphiques saisissables de l'écran
      for (Component composant : listeComposant) {
        // Capturer les erreurs lors de l'initialisation d'un composant pour ne pas tout planter à cause d'une erreur sur un seul
        // composant
        try {
          // Récupérer le nom du composant
          String nomComposant = composant.getName();

          // Tester si le composant est le bandeau de titre
          if (composant instanceof SNBandeauTitre) {
            SNBandeauTitre bandeauTitre = (SNBandeauTitre) composant;

            // Récupérer le message envoyé par le programme RPG dans le champ V03F
            String messageV03F = lexique.HostFieldGetData("V03F").trim();

            // Convertir en message plus clair
            // Seules les informations explicitement connues et gérées sont conservées, toutes les autres sont supprimées.
            // Le but est d'avoir un sous-titre le plus propre possible. Comme nous ne sommes pas certains de la qualité des messages
            // envoyés par les programmes RPG, il a été décidé de supprimer tout sauf si ce qui est prévu.
            InterpreteurV03F interpreteurV03F = new InterpreteurV03F(messageV03F);

            // Modifier le sous-titre du bandeau de titre
            // Le message initial envoyé par le RPG est affiché dans l'infobulle essentiellement à des fins de debuggages
            bandeauTitre.setSousTitre(interpreteurV03F.getMessageFormate(), messageV03F);
          }

          // Récupérer la zone du buffer correspondant au composant graphique
          oData hostField = (oData) listeElement.get(nomComposant);
          if (hostField == null) {
            continue;
          }

          // Traiter différemment le composant en fonction de son type
          if (composant instanceof IXRiComposant) {
            IXRiComposant riComposant = ((IXRiComposant) composant);
            riComposant.setData();
          }
          else if (composant instanceof RiTextArea) {
            // Vérifier que le composant concerne le buffer en cours
            if (entry.getKey().equals(hostField.getRecord())) {
              IRiComposant riComposant = ((IRiComposant) composant);
              riComposant.setHostField(hostField);
              riComposant.setInfosFlux(entry.getKey(), fluxRpg.getRecordName(), null, listeoDataFlux, lexique);
              riComposant.setData();
              requestcomposant[0] = riComposant.getRequestComposant(requestcomposant[0]);
            }
          }
        }
        catch (Exception e) {
          // Afficher l'erreur
          DialogueErreur.afficher(e);
        }
      }
    }

    // Mettre à jour la boîte de dialogue s'il y en une
    if (oFramePopupPerso != null) {
      oFramePopupPerso.fluxRpg.majRecords(oFramePopupPerso.listeRecord, idClassName);
      oFramePopupPerso.setData();
    }

    // Initialisation des variables contenu dans les libellés
    mettreAJourVariableLibelle();

    // Tracer de debug
    Trace.debug(SNPanelEcranRPG.class, "setData", "" + fluxRpg.getClassName() + "|" + fluxRpg.getRecordName());
    Trace.debugMemoire(SNPanelEcranRPG.class, "setData");
  }

  /**
   * Récupérer les données de l'interface graphique et mettre à jour le buffer RPG.
   */
  public void getData() {
    // Parcourir les composants graphiques de l'écran
    for (Map.Entry<oData, Object> entry : listeoDataFlux.entrySet()) {
      // Récupérer le champ
      oData hostField = entry.getKey();
      if (hostField == null) {
        return;
      }

      // Mettre le champ en mode débug si on est en mode débug
      hostField.setDebug(isDebug);

      // Vérifier si le champ correspond à une des données du buffer courant
      if (lexique.isIntoCurrentRecord(hostField)) {
        // Capcturer les exceptions afin qu'un problème dans un composant ne plante pas tout le système
        try {
          // Vérifier si le champ est un ioXRiComposant
          if (entry.getValue() instanceof IXRiComposant) {
            ((IXRiComposant) entry.getValue()).getData();
          }
          // Vérifier si le champ est un RiTextArea
          else if ((entry.getValue() instanceof RiTextArea)) {
            ((IRiComposant) entry.getValue()).getData();
          }
        }
        catch (Exception e) {
          // Afficher l'erreur
          DialogueErreur.afficher(e);
        }
      }
    }
  }

  /**
   * Retourne l'id de la session.
   */
  public IdSession getIdSession() {
    if (session != null) {
      return session.getIdSession();
    }
    throw new MessageErreurException("Le panel ne peut pas retourner d'id session car il n'est pas rattaché à une session.");
  }

  // -- Méthodes protégées

  /**
   * Methode à supprimer une fois l'objet Jcomobox remplacé pas le XRiComboBox
   * Retourne l'indice correspondant à une hostconversion
   * @param anomData
   * @param avalue
   * @return
   */
  protected int getIndice(String anomData, String[] avalue) {
    int i = 0;
    String valeur = lexique.HostFieldGetData(anomData);

    // On commence par les tops
    for (i = 0; i < avalue.length; i++) {
      if (avalue[i].trim().equals(valeur)) {
        return i;
      }
    }
    return 0;
  }

  /**
   * Initialisations diverses que l'on ajoute aux panels sans avoir à les retoucher
   */
  protected void initDiverses() {
    if (versionLook == 2) {
      initNouveauLook();
    }
    else {
      initAncienLook();
    }
  }

  /**
   * initialiser tous les éléments récurrents du panel (menus, modes d'affichage, boutons, couleurs et images (V2)
   */
  private void initNouveauLook() {
    // La couleur de fond des panels est forcée ici
    setBackground(SNCharteGraphique.COULEUR_FOND);

    try {
      // On récupère la liste des composants du panel
      if (listeComposant == null) {
        listeComposant = genereListeComposants(this);
      }

      int lg = listeComposant.size();
      for (int i = 0; i < lg; i++) {
        if (listeComposant.get(i).getName() == null) {
          continue;
        }

        // Gestion du bandeau de présentation
        if (listeComposant.get(i) instanceof SNBandeauTitre) {
          ((SNBandeauTitre) listeComposant.get(i)).setCouleurFoncee(infosPrg.getCouleurBandeauFoncee());
        }
        // GESTION DES PANELS
        else if (listeComposant.get(i) instanceof JPanel) {
          // Initialise la couleur de fond des panels
          if (listeComposant.get(i).getName().equals("p_contenu")) {
            ((JPanel) listeComposant.get(i)).setBackground(SNCharteGraphique.COULEUR_FOND);

            // Changement de la taille pour avoir l'ascenseur sur des écrans trop petits (on est passé de 200 à 150 à
            // cause du VCGM73 DP)
            if (!isDialog()) {
              ((JPanel) listeComposant.get(i)).setBorder(new EmptyBorder(0, 0, 0, 00));
              int largeur = listeComposant.get(i).getPreferredSize().width + 150;
              int hauteur = listeComposant.get(i).getPreferredSize().height + 100;
              Dimension d = new Dimension(largeur, hauteur);
              setPreferredSize(d);
              setMinimumSize(d);
            }
          }

          if ((listeComposant.get(i).getName().equals("p_menus")) || (listeComposant.get(i).getName().equals("p_titre"))
              || (listeComposant.get(i).getName().equals("p_logo"))) {
            // intégré
            ((JPanel) listeComposant.get(i)).setBackground(Constantes.COULEUR_MENUS);
          }
          if (listeComposant.get(i).getName().equals("menus_haut")) {
            initLesMenus(((JPanel) listeComposant.get(i)));
            ((JPanel) listeComposant.get(i)).setBackground(Constantes.COULEUR_MENUS);
          }
        }
        // GESTION DES LABELS
        else if (listeComposant.get(i) instanceof JLabel) {
          // Le fond de la présentation
          if (listeComposant.get(i).getName().equals("fond_titre")) {
            // intégré
            ((JLabel) listeComposant.get(i)).setIcon(lexique.chargerImage("images/fond_presentation.png", true));
          }
          if (listeComposant.get(i).getName().equals("titre")) {
            // intégré
            titrePanel = ((JLabel) listeComposant.get(i));
          }
        }
        // GESTION DES RIMENUS
        else if (listeComposant.get(i) instanceof RiMenu) {
          // Le mode d'affichage en cours
          if (listeComposant.get(i).getName().equals("riMenu_V01F")) {
            V01F = (RiMenu) listeComposant.get(i);
            modesAffichage.add(listeComposant.get(i));
            ((RiMenu) listeComposant.get(i)).setMargin(new Insets(0, -2, 0, 0));
          }

        }
        // GESTION DES RISOUSMENUS
        else if (listeComposant.get(i) instanceof RiSousMenu) {
          // Les modes d'affichage possibles
          if ((listeComposant.get(i).getName().equals("riSousMenu_consult"))
              || (listeComposant.get(i).getName().equals("riSousMenu_modif"))
              || (listeComposant.get(i).getName().equals("riSousMenu_crea"))
              || (listeComposant.get(i).getName().equals("riSousMenu_suppr"))
              || (listeComposant.get(i).getName().equals("riSousMenuF_dupli"))
              || (listeComposant.get(i).getName().equals("riSousMenu_rappel"))
              || (listeComposant.get(i).getName().equals("riSousMenu_reac"))
              || (listeComposant.get(i).getName().equals("riSousMenu_destr"))) {
            modesAffichage.add(listeComposant.get(i));
          }
        }
        // GESTION DES JBUTTONS
        else if (listeComposant.get(i) instanceof JButton) {
          // TETES DE MENUS------------------------------------------------------------------
          if (listeComposant.get(i).getName().startsWith("riMenu_bt")) {
            // Accordéon
            ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                // Permet la sélection du menu sur lequel on vient de cliquer
                dernierMenu = -1; // Ne pas enlever sinon gererAffichageMenus va marcher bcp moins bien
                gererAffichageMenus(e.getSource());
              }
            });
          }
          // Les SOUS MENUS------------------------------------------------------------------
          else if (listeComposant.get(i).getName().startsWith("riSousMenu_bt_")) {
            // V01F
            if (listeComposant.get(i).getName().equals("riSousMenu_bt_consult")) {
              ((JButton) listeComposant.get(i)).setToolTipText("raccourci : SHIFT+F3");
              ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                  lexique.HostScreenSendKey(thisFrame, "F15");
                }
              });

            }
            else if (listeComposant.get(i).getName().equals("riSousMenu_bt_modif")) {
              ((JButton) listeComposant.get(i)).setToolTipText("raccourci : SHIFT+F2");
              ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  lexique.HostScreenSendKey(thisFrame, "F14");
                }
              });
            }
            else if (listeComposant.get(i).getName().equals("riSousMenu_bt_crea")) {
              ((JButton) listeComposant.get(i)).setToolTipText("raccourci : SHIFT+F1");
              ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  lexique.HostScreenSendKey(thisFrame, "F13");
                }
              });
            }
            else if (listeComposant.get(i).getName().equals("riSousMenu_bt_suppr")) {
              ((JButton) listeComposant.get(i)).setToolTipText("raccourci : SHIFT+F4");
              ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  lexique.HostScreenSendKey(thisFrame, "F16");
                }
              });
            }
            else if (listeComposant.get(i).getName().equals("riSousMenu_bt_dupli")) {
              ((JButton) listeComposant.get(i)).setToolTipText("raccourci : SHIFT+F6");
              ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  lexique.HostScreenSendKey(thisFrame, "F18");
                }
              });
            }
            else if (listeComposant.get(i).getName().equals("riSousMenu_bt_reac")) {
              ((JButton) listeComposant.get(i)).setToolTipText("raccourci : SHIFT+F5");
              ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  lexique.HostScreenSendKey(thisFrame, "F17");
                }
              });
            }
            else if (listeComposant.get(i).getName().equals("riSousMenu_bt_destr")) {
              ((JButton) listeComposant.get(i)).setToolTipText("raccourci : SHIFT+F7");
              ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  lexique.HostScreenSendKey(thisFrame, "F19");
                }
              });
            }
          }
          // NAVIGATION + ERREURS----------------------------------------------------------------
          else if (listeComposant.get(i).getName().equals("bouton_valider")) {
            ((JButton) listeComposant.get(i)).setIcon(lexique.chargerImage("images/OK_p.png", true));
          }
          else if (listeComposant.get(i).getName().equals("bouton_retour")) {
            ((JButton) listeComposant.get(i)).setIcon(lexique.chargerImage("images/retour_p.png", true));
          }
          else if (listeComposant.get(i).getName().equals("bouton_erreurs")) {
            ((JButton) listeComposant.get(i)).setIcon(lexique.chargerImage("images/alertes.png", true));
            ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {

              @Override
              public void actionPerformed(ActionEvent e) {
                lexique.HostScreenSendKey(thisFrame, "F1");
              }
            });
            // Gestion de l'info bulle des erreurs
            button_erreurs = ((JButton) listeComposant.get(i));
          }
          else if (listeComposant.get(i).getName().equals("BT_PGUP")) {
            ((JButton) listeComposant.get(i)).setIcon(lexique.chargerImage("images/pgup20.png", true));
            ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                lexique.HostScreenSendKey(thisFrame, "PGUP");
              }
            });
          }
          else if (listeComposant.get(i).getName().equals("BT_PGDOWN")) {
            ((JButton) listeComposant.get(i)).setIcon(lexique.chargerImage("images/pgdwn20.png", true));
            ((JButton) listeComposant.get(i)).addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                lexique.HostScreenSendKey(thisFrame, "PGDOWN");
              }
            });
          }
        }
        else if (listeComposant.get(i) instanceof IXRiComposant) {
          ((IXRiComposant) listeComposant.get(i)).init(lexique, listeRecord, interpreteurD, listeoDataFlux, requestcomposant);
        }
      }

    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }

  /**
   * Initialise ce qu'il faut pour l'ancien look (V1)
   */
  private void initAncienLook() {
    try {
      // On récupère la liste des composants du panel
      if (listeComposant == null) {
        listeComposant = genereListeComposants(this);
      }

      int lg = listeComposant.size();
      for (int i = 0; i < lg; i++) {
        if (listeComposant.get(i).getName() == null) {
          continue;
        }

        if (listeComposant.get(i) instanceof IXRiComposant) {
          ((IXRiComposant) listeComposant.get(i)).init(lexique, listeRecord, interpreteurD, listeoDataFlux, requestcomposant);
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }

  /**
   * Trie les menus de manière logique afin de préparer leur affichage dynamique
   * @param panel_menus
   */
  public void initLesMenus(JPanel panel_menus) {
    Component[] listeComposants = panel_menus.getComponents();
    tetesMenus.clear();
    for (ArrayList<Component> elt : tousMenus) {
      elt.clear();
    }
    tousMenus.clear();
    ArrayList<Component> menusTries = null;

    for (int i = 0; i < listeComposants.length; i++) {
      if (listeComposants[i] instanceof RiMenu) {
        tetesMenus.add((RiMenu) listeComposants[i]);
      }

      if (listeComposants[i] instanceof RiSousMenu) {
        if (menusTries != null) {
          menusTries.add(listeComposants[i]);
          if (i == listeComposants.length - 1) {
            tousMenus.add(menusTries);
          }
        }
      }
      else {
        if (i != 0) {
          tousMenus.add(menusTries);
        }
        menusTries = new ArrayList<Component>();
      }
    }
  }

  /**
   * Affiche ou ferme les menus en fonction de la tête de menu choisie
   * @param tiroir
   * @throws InvocationTargetException
   * @throws InterruptedException
   */
  public void gererAffichageMenus(final Object tiroir) {
    if (noRefresh) {
      return;
    }

    if ((tousMenus.size() == 0) || (tetesMenus.size() == 0)) {
      return;
    }
    int indexMenu = -1;

    // Si un tiroir (RiMenu_bt) a été sélectionné, repérer lequel
    if (tiroir != null) {
      if (tiroir.toString().equalsIgnoreCase("aucun")) {
        for (int i = 0; i < tousMenus.size(); i++) {
          tetesMenus.get(i).setOuvert(false);
        }
      }
      // Dans le cas où l'on a pas déjà ouvert un menu sur le panel en cours lors d'un setData précédent (F5 ou autres)
      else if (dernierMenu != -1) {
        indexMenu = dernierMenu;
      }
      else {
        // Recherche l'indice du menu que l'on souhaite ouvrir
        for (int i = tousMenus.size(); --i >= 0;) {
          if (tetesMenus.get(i).getName().equals(((Component) tiroir).getParent().getName())) {
            indexMenu = i;
            // pas de dernier menu ouvert si on ferme la tête de menu
            if (tetesMenus.get(i).isOuvert()) {
              dernierMenu = -1;
            }
            else {
              dernierMenu = i;
            }
            // changer état du bouton en ouvert ou non ouvert
            tetesMenus.get(i).setOuvert(!tetesMenus.get(i).isOuvert());
            // changé pour le fonctionement du menu outil à droite : il doit rester toujours ouvert si choisi, pas une
            // fois sur 2.
            // tetesMenus.get(i).setOuvert(true); <- Empêche la fermeture du menu si on clique dessus
          }
          else {
            tetesMenus.get(i).setOuvert(false);
          }
        }
      }
    }
    // si on ne passe aucun RiMenu_bt alors on est dans le setData() : on teste dernierMenu
    else {
      // si on a un seul riMenu alors ouvrir automatiquement
      if (((dernierMenu == -1) && (tetesMenus.size() == 1) && !tetesMenus.get(0).getName().equals("riMenu_V01F"))
          || ((tetesMenus.size() == 2) && tetesMenus.get(0).getName().equals("riMenu_V01F"))) {
        indexMenu = tetesMenus.size() - 1;
      }
      else {
        indexMenu = dernierMenu;
      }

      // On ferme tous les menus
      for (int i = tousMenus.size(); --i >= 0;) {
        tetesMenus.get(i).setOuvert(false);
      }
      // On ouvre le menu si l'index est différent de -1
      if (indexMenu > -1) {
        tetesMenus.get(indexMenu).setOuvert(true);
      }
    }

    // Ouvrir ou fermer les sous-menus correspondant à la tête de menu choisi et fermer les autres
    for (int i = tousMenus.size(); --i >= 0;) {
      for (int j = tousMenus.get(i).size(); --j >= 0;) {
        if ((i == indexMenu) && tousMenus.get(i).get(j).isEnabled()) {
          tousMenus.get(i).get(j).setVisible(tetesMenus.get(i).isOuvert());
        }
        else {
          tousMenus.get(i).get(j).setVisible(false);
        }
      }
    }
  }

  /**
   * Afficher correctement les modes d'affichage courants et possibles (TODO faire une classe serait mieux)
   */
  private void gererModeAffichage() {
    // long t0 = System.currentTimeMillis();
    String v01f = lexique.HostFieldGetData("V01F").trim();
    if (/*(v01f == null) ||*/(modesAffichage == null) || (modesAffichage.isEmpty())) {
      return;
    }

    final char[] listeInitiales = { 'I', 'M', 'C', 'A', 'D', 'R', 'r', 'S' };
    final String[] listeLibelle =
        { "Consultation", "Modification", "Création", "Annulation", "Duplication", "Rappel", "Réactivation", "Suppression" };
    final String[] listeOverhelp = { "Vous êtes actuellement en mode consultation", "Vous êtes actuellement en mode modification",
        "Vous êtes actuellement en mode création", "Vous êtes actuellement en mode annulation",
        "Vous êtes actuellement en mode duplication", "Vous êtes actuellement en mode rappel",
        "Vous êtes actuellement en mode réactivation", "Vous êtes actuellement en mode suppression" };
    final String[] listeImages = { "file_search_g.png", "file_edit_g.png", "file_add_g.png", "file_delete_g.png", "file_dupli_g.png",
        "file_rappel_g.png", "file_rappel_g.png", "file_delete_g.png" };

    // Gérer le cas d'un V01F où l'utilisateur n'a pas les droits
    if (v01f.equals("")) {
      V01F.setVisible(false);
      return;
    }
    // Gérer le cas d'un V01F qui n'a pas encore été modifié ou qui est hors pattern
    else if ((v01f.length() <= 2) || (v01f.length() > listeInitiales.length + 2) || (v01f.charAt(1) != '/')) {
      return;
    }
    else {
      V01F.setVisible(true);
    }

    char[] Valeurs = v01f.toCharArray();
    final char FctCourante = Valeurs[0];
    int indice = 0;

    // Initialiser les m_aff à enabled = false
    for (int i = modesAffichage.size(); --i > 0;) {
      // enabled
      modesAffichage.get(i).setEnabled(false);
    }

    // Afficher les sous menus possibles
    for (int i = 2; i < Valeurs.length; i++) {
      indice = 0;
      while (indice < listeInitiales.length) {
        if (Valeurs[i] == FctCourante) {
          break;
        }
        else if (Valeurs[i] == listeInitiales[indice]) {
          modesAffichage.get(indice + 1).setEnabled(true);
          break;
        }
        indice++;
      }
    }

    // Affichage du mode en cours
    RiMenu mon_v01f = (RiMenu) modesAffichage.get(0);
    for (int j = 0; j < listeInitiales.length; j++) {
      if (FctCourante == listeInitiales[j]) {
        ((JButton) mon_v01f.getComponent(0)).setIcon(lexique.chargerImage("images/" + listeImages[j], true));
        ((JButton) mon_v01f.getComponent(0)).setText(listeLibelle[j]);
        ((JButton) mon_v01f.getComponent(0)).setToolTipText(listeOverhelp[j]);
        break;
      }
    }
  }

  /**
   * gérer l'affichage des erreurs.
   * @param pIndicateur
   */
  public void gererLesErreurs(final String pIndicateur) {
    gererLesErreurs(pIndicateur, lexique.HostFieldGetData("V03F"));
  }

  /**
   * gérer l'affichage des erreurs.
   * @param pIndicateur
   */
  public void gererLesErreurs(final String pIndicateur, final String pMessage) {
    if (noRefresh || pIndicateur == null) {
      return;
    }

    if (button_erreurs != null) {
      button_erreurs.setVisible(false);
      button_erreurs.getParent().setVisible(button_erreurs.isVisible());
    }

    try {
      if (!pMessage.trim().isEmpty()) {
        Integer.valueOf(pMessage.replaceAll(" ", ""));
      }
      // Si l'indicateur des erreurs est levé alors le F1 sera envoyé en fin de setData().
      // Certains programmes RPG gèrent différemment les erreurs et appellent automatiquement le SEXPER, si le F1 est envoyé alors
      // l'utilisatrur va tomber dans une boucle sans fin sur la boite de dialogue des erreurs. Pour ces programmes idéalement il faudrait
      // ne pas appeler la méthode gererLesErreur() comme par exemple le VGAM15
      // Voir VGTM71 pour résoudre le problème on peut faire un SetOF de l'indicateur d'erreur après la gestion des erreurs.
      if (lexique.isTrue(pIndicateur)) {
        // Envoi de la touche F1 en fin de setData
        envoyerF1 = true;
      }
    }
    catch (NumberFormatException e) {
      if (lexique.isTrue(pIndicateur) && pMessage != null && !pMessage.isEmpty()) {
        // Afficher le message (dans la thread EDT car le traitement des trames RPG est effectué dans une thread dédié)
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            DialogueInformation.afficher(pMessage);
          }
        });
      }
    }
  }

  /**
   * Gérer diverses fonctions dans le setData() après l'initialisation des données
   */
  public void setDiverses() {
    // TODO en cours de dev
    if (!modesAffichage.isEmpty()) {
      gererModeAffichage();
    }

    // Formate le titre du panel Minuscule/majuscule (devrait disparaitre à terme puisque l'objet SNBandeauTitre le gère)
    if (titrePanel != null) {
      Constantes.capitaliserPremiereLettre(titrePanel.getText());
    }
  }

  /**
   * Initialise la liste des touches qui doivent fermer la popup
   * @param keys
   */
  public void setCloseKey(String... keys) {
    listCloseKeys.clear();
    for (String key : keys) {
      listCloseKeys.add(key.toUpperCase());
    }
  }

  /**
   * Détermine si une touche doit fermer la popup
   * @return
   */
  public boolean isCloseKey(String key) {
    return listCloseKeys.contains(key.toUpperCase());
  }

  /**
   * Reset les actions sur le panel
   */
  public void resetActions() {
    getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).clear();
    getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).clear();
    getActionMap().clear();
  }

  /**
   * Permet d'activer les touches de fonction sur le panel principal (décritent dans le FMT)
   * @param composant
   * @param afluxRpg
   * @param isAffichable
   */
  public void setActiveKey() {
    int i = 0;
    String key = null;
    int fait = Constantes.FALSE;
    // Si la liste des flux est vide on dégage
    if (listeRecord.isEmpty()) {
      return;
    }

    if (fluxRpg != null && fluxRpg.isPanelAffichage()) {
      resetActions();
      ArrayList<oKey> listeKey = fluxRpg.getListeKey();
      for (i = 0; i < listeKey.size(); i++) {
        key = listeKey.get(i).getKey();
        if (key == null) {
          continue;
        }
        else {
          key = key.toUpperCase();
          if (listeKeyEnrg.containsKey(key)) {
            getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(listeKeyEnrg.get(key), key);
            getActionMap().put(key, oframeActions.getKeyAction(key));
            if (key.equals("F2")) { // Verrue pour le problème du F2 qui ne fonctionne pas
              getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(listeKeyEnrg.get("C2"), "C2");
              getActionMap().put("C2", oframeActions.getKeyAction("C2"));
            }
            if (key.equalsIgnoreCase("ENTER")) {
              fait = Constantes.TRUE;
            }
          }
        }
      }
      // On ajoute systématiquement la touche ENTER sauf si existe déjà
      if (fait == Constantes.FALSE) {
        key = "ENTER";
        getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(listeKeyEnrg.get(key), key);
        getActionMap().put(key, oframeActions.getKeyAction(key));

      }
    }
  }

  /**
   * Initialise le bouton par défaut du panel.
   * @param db
   */
  protected void setDefaultButton(JButton db) {
    defaultButton = db;
  }

  /**
   * Initialise le composant qui à le curseur
   * @param composant
   */
  protected void setRequestComponent(JComponent composant) {
    requestcomposant[0] = composant;
  }

  /**
   * Traitement à lancer lorsque le setData est terminé.
   */
  public void traitementApresSetData(SNPanelEcranRPG pEcranprec) {
    // Envoi de la touche F1 automatiquement en cas d'erreur lorsque le setData() du panel en cours est terminé
    if (envoyerF1) {
      envoyerF1 = false;
      // Certains programmes utilisent le SEXPER afin d'afficher les erreurs et le programme s'affiche automatiquement sans appuie du F1.
      // Afin d'éviter une boucle sans fin, un contrôle est effectué sur le panel précédent et la dernière touche envoyée au RPG.
      // Le F1 automatique est envoyé si :
      // - Le programme précédent n'est pas le SEXPER
      // - Le programme précédent est le SEXPER mais que la dernière touche envoyée n'est ni le F1 ni le F1_AUTOMATIQUE
      if (!pEcranprec.getClass().getName().endsWith("SEXPERFM_ER") || (pEcranprec.getClass().getName().endsWith("SEXPERFM_ER")
          && !Constantes.equals(derniereToucheEnvoyee, "F1_AUTOMATIQUE") && !Constantes.equals(derniereToucheEnvoyee, "F1"))) {
        setRenvoyable(true);
        lexique.HostScreenSendKey(this, "F1_AUTOMATIQUE");
      }
    }
  }

  /**
   * Recherche le composant qui aura le focus.
   */
  public void searchRequestComponent() {
    // L'attribut PC est pris en compte dans le setData de chaque composant (même la xRiTable mais que sur les tops).
    // C'est XXLIG et XXCOL qui prédomine sur l'attribut PC du SDA (modifié le 16/05/2013)
    // Position du focus avec XXLIG & XXCOL
    oData xxlighost = lexique.getHostField("XXLIG");
    oData xxcolhost = lexique.getHostField("XXCOL");
    if (xxlighost != null && !xxlighost.getValeurToBuffer().equals("000") && xxcolhost != null
        && !xxcolhost.getValeurToBuffer().equals("000")) {
      int xxlig = Integer.parseInt(xxlighost.getValeurToBuffer().trim());
      int xxcol = Integer.parseInt(xxcolhost.getValeurToBuffer().trim());
      oData HostField;
      for (Map.Entry<oData, Object> entry : listeoDataFlux.entrySet()) {
        HostField = entry.getKey();
        if (HostField.cursorIsInto(xxlig, xxcol, listeRecord.get(HostField.getRecord()).getDebLigneCLR())) {
          requestcomposant[0] = (JComponent) listeoDataFlux.get(HostField);
          break;
        }
      }

      // Si on n'a pas d'attribut PC et si le composant qui doit recevoir le focus n'est toujours pas identifié avec le XXLIG & XXCOL
      // On donne le focus à la première zone TextField vide que l'on trouve
      if (requestcomposant[0] == null && listeComposant != null) {
        for (int i = 0; i < listeComposant.size(); i++) {
          if (lexique.getMode() != Lexical.MODE_CONSULTATION
              && (listeComposant.get(i) instanceof JTextField || listeComposant.get(i) instanceof XRiTextField)) {
            if ((listeComposant.get(i).isVisible()) && (listeComposant.get(i).isEnabled())) {
              requestcomposant[0] = (JComponent) listeComposant.get(i);
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Active le focus sur le composant désigné
   */
  public void activeRequestComponent4Frame() {
    if (requestcomposant[0] != null) {
      infosPrg.setDebugFocus(requestcomposant[0].getName());
    }

    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        // thisFrame.requestFocus(); // Il le faut sinon problème de focus sur panel en Interro (impossible de faire
        // fonctionner les touches de fonction)

        // On met le focus sur le composant si trouvé
        if (requestcomposant[0] != null && requestcomposant[0].isVisible()) {
          requestcomposant[0].requestFocusInWindow();
          requestcomposant[0] = null; // On le réinitialise pour le prochain coup
        }
        // Sinon c'est le default button qui aura le focus
        else if (defaultButton != null) {
          defaultButton.requestFocusInWindow();
          thisFrame.requestFocus();
        }
        else {
          thisFrame.requestFocusInWindow();
        }

        // On attribut le défaut Bouton
        if ((thisFrame.getRootPane() != null) && (defaultButton != null)) {
          thisFrame.getRootPane().setDefaultButton(defaultButton);
        }
      }
    });
  }

  /**
   * Active le focus sur le composant désigné
   */
  public void activeRequestComponent4Dialog() {
    if (requestcomposant[0] != null) {
      infosPrg.setDebugFocus(requestcomposant[0].getName());
    }

    // On met le focus sur le composant si trouvé
    if ((requestcomposant[0] != null) && requestcomposant[0].isVisible()) {
      requestcomposant[0].requestFocusInWindow();
      requestcomposant[0] = null; // On le réinitialise pour le prochain coup
    }
    // Sinon c'est le default button qui aura le focus
    else if (defaultButton != null) {
      defaultButton.requestFocusInWindow();
      thisFrame.requestFocus();
    }
    else {
      thisFrame.requestFocus();
    }

    // On attribut le défaut Bouton
    if ((thisFrame.getRootPane() != null) && (defaultButton != null)) {
      thisFrame.getRootPane().setDefaultButton(defaultButton);
    }
  }

  /**
   * Retourne le bouton par défaut du panel
   * @return
   */
  public JButton getDefaultButton() {
    return defaultButton;
  }

  /**
   * Retourner le menu commande afin de l'intégrer dans la fenêtre principale si besoin
   */
  public JMenu getMenuCommand() {
    return menuCommand;
  }

  /**
   * Initialise le menu commande afin de l'intégrer dans la fenêtre principale si besoin
   * @param menuCommand menuCommand à définir
   */
  public void setMenuCommand(JMenu menuCommand) {
    this.menuCommand = menuCommand;
  }

  /**
   * @return dialog
   */
  public boolean isDialog() {
    return dialog;
  }

  /**
   * @param dialog dialog à définir
   */
  public void setDialog(boolean dialog) {
    this.dialog = dialog;
  }

  public void setJDialog(JDialog dialog) {
    dialogue = dialog;
  }

  public JDialog getJDialog() {
    return dialogue;
  }

  /**
   * Permet de fermer les popups liés
   * ATTENTION ne plus utiliser directement voir le traitement fait dans le VGAM15FM_DEVISE, _REGLEMENT pour adapter les
   * écrans.
   * refreshParent, true avec retour sur panel parent, false pour les popup qui valide la saisie (VGVM11OPTPIECE et
   * VCGM11CON)
   */
  public void closePopupLinkWithBuffer(boolean refreshPanelParent) {
    if ((dialogue == null) || (oFrameParent == null)) {
      return;
    }

    dialogue.setModal(false);

    // On baisse systématiquement cette variable pour assurer le rafrachissement du panel (elle est levé dans le setData
    // de oDialog)
    oFrameParent.usePopupPerso = false;
    oFrameParent.oFramePopupPerso = null;

    // Avec le contenu du buffer en cours (si on a saisit des choses elles seront perdu)
    if (refreshPanelParent) {
      oFrameParent.setData(); // Attention la position est importante (doit être avant le dispose)
      getRootPane().setDefaultButton(oFrameParent.getDefaultButton());
    }
    // <- a voir si intelligent sur le long terme car dans l'autre cas on est bloqué si on passe pas dans la boucle
    oFrameParent.setRenvoyable(true);

    // Ajout car pb si mais pas bô
    oFrameParent.usePopupPerso = false;

    dialogue.dispose();
  }

  /**
   * @return lexique
   */
  public Lexical getLexique() {
    return lexique;
  }

  /**
   * @return the sessionPanel
   */
  public SessionBase getSession() {
    return session;
  }

  /**
   * @param pSession the sessionPanel to set
   */
  public void setSession(SessionBase pSession) {
    this.session = pSession;
  }

  public int getVersionLook() {
    return versionLook;
  }

  public void setVersionLook(int nouvelle) {
    versionLook = nouvelle;
  }

  public boolean isRenvoyable() {
    return renvoyable;
  }

  public void setRenvoyable(boolean val) {
    renvoyable = val;
  }

  /**
   * @param idclassName the idClassName to set
   */
  public void setIdClassName(int idclassName) {
    this.idClassName = idclassName;
  }

  /**
   * @return the className
   */
  public int getIdClassName() {
    return idClassName;
  }

  /**
   * Retourne si la condition est vérifié par rapport aux indicateurs (du record courant donc en EXFMT)
   * @param condition
   * @return
   */
  public boolean isTrue(String cond) {
    if (cond == null) {
      return false;
    }
    else {
      return fluxRpg.getEvaluationIndicateur(idClassName, cond) == Constantes.ON;
    }
  }

  /**
   * Retourne si la condition est vérifié par rapport aux indicateurs du record voulu
   * @param condition
   * @return
   */
  public boolean isTrue(String cond, String nomrecord) {
    if (cond == null) {
      return false;
    }
    else {
      return fluxRpg.getEvaluationIndicateur(idClassName, nomrecord, cond) == Constantes.ON;
    }
  }

  public PopupPerso getPopupPerso() {
    return popupPerso;
  }

  public void setPopupPerso(PopupPerso pPopup) {
    popupPerso = pPopup;
  }

  public void setDerniereToucheEnvoyee(String derniereToucheEnvoyee) {
    this.derniereToucheEnvoyee = derniereToucheEnvoyee;
  }

  /**
   * Nettoyage des variables
   */
  public void dispose() {
    listCloseKeys.clear();
    // ((Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory())/(1024*1024)) + " de " +
    // (Runtime.getRuntime().maxMemory()/(1024*1024)) + " Mo utilisés");
    removeAll();
    listeoDataFlux.clear();
    if (listeComposant != null) {
      for (Component elt : listeComposant) {
        if (elt instanceof IXRiComposant) {
          ((IXRiComposant) elt).dispose();
        }
      }
      listeComposant.clear();
      listeComposant = null;
    }
    if (listeRecord != null) {
      for (Entry<String, oRecord> entry : listeRecord.entrySet()) {
        entry.getValue().dispose();
      }
      listeRecord.clear();
      listeRecord = null;
    }
    if (tousMenus != null) {
      for (ArrayList<Component> liste : tousMenus) {
        liste.clear();
      }
      tousMenus.clear();
      tousMenus = null;
    }

    if (tetesMenus != null) {
      tetesMenus.clear();
      tetesMenus = null;
    }
    if (modesAffichage != null) {
      modesAffichage.clear();
      modesAffichage = null;
    }
    if (lexique != null) {
      lexique.dispose();
    }
    interpreteurD.dispose();

    lexique = null;
    fluxRpg = null;
    dialogue = null;
    oFrameParent = null;
    oFramePopupPerso = null;
    if (infosPrg != null) {
      infosPrg.dispose();
      infosPrg = null;
    }
    requestcomposant = null;
    thisFrame = null;
  }
}
