/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical.table;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;

/**
 * Permet de détecter les changement de colonnes - Non utilisé
 * C'est intégré dans la classe lexique mais non impémenté dans XRiTable car il y a un problème pour la réorganisation des colonnes de la
 * liste.
 * L'algo est complexe et ça risque de tout péter dans la logique du setData du composant.
 * Voir plus tard si c'est vraiment demandé (c'est ssocié au programme test39 pour testouiller).
 */
public class XRiTableColumnModelListener implements TableColumnModelListener {
  private boolean deplaceCol = false;
  private String[] titreNouvelOrdre = null;
  
  public XRiTableColumnModelListener(String[] titreApr) {
    this.titreNouvelOrdre = titreApr;
  }
  
  @Override
  public void columnAdded(TableColumnModelEvent e) {
  }
  
  @Override
  public void columnRemoved(TableColumnModelEvent e) {
  }
  
  @Override
  public void columnMoved(TableColumnModelEvent e) {
    String chaine = null;
    if (e.getFromIndex() != e.getToIndex()) {
      setDeplaceCol(true);
      chaine = titreNouvelOrdre[e.getToIndex()];
      titreNouvelOrdre[e.getToIndex()] = titreNouvelOrdre[e.getFromIndex()];
      titreNouvelOrdre[e.getFromIndex()] = chaine;
    }
  }
  
  @Override
  public void columnMarginChanged(ChangeEvent e) {
  }
  
  @Override
  public void columnSelectionChanged(ListSelectionEvent e) {
  }
  
  public void setDeplaceCol(boolean deplaceCol) {
    this.deplaceCol = deplaceCol;
  }
  
  public boolean isDeplaceCol() {
    return deplaceCol;
  }
  
}
