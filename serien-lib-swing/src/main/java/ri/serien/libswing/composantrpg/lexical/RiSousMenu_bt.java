/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class RiSousMenu_bt extends JButton {
  
  /**
   * Constructeur.
   */
  public RiSousMenu_bt() {
    super();
    
    setBackground(new Color(238, 239, 241));
    setPreferredSize(new Dimension(167, 26));
    setMinimumSize(new Dimension(167, 26));
    setBorderPainted(false);
    setContentAreaFilled(false);
    setMaximumSize(new Dimension(167, 26));
    // setFont(getFont().deriveFont(getFont().getStyle() - 1f));
    setFont(new Font(getFont().getFamily(), 0, 11));
    setHorizontalAlignment(SwingConstants.LEADING);
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    setMargin(new Insets(0, 5, 0, -4));
    setIconTextGap(5);
    setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/puce.png")));
    
    this.addMouseListener(new MouseListener() {
      
      @Override
      public void mouseReleased(MouseEvent arg0) {
      }
      
      @Override
      public void mousePressed(MouseEvent arg0) {
      }
      
      @Override
      public void mouseExited(MouseEvent arg0) {
        JButton bt = (JButton) arg0.getSource();
        bt.setOpaque(false);
      }
      
      @Override
      public void mouseEntered(MouseEvent arg0) {
        JButton bt = (JButton) arg0.getSource();
        bt.setOpaque(true);
      }
      
      @Override
      public void mouseClicked(MouseEvent arg0) {
      }
    });
  }
}
