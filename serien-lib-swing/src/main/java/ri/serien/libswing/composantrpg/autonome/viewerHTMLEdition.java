/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import ri.serien.libcommun.outils.fichier.FiltreFichierParExtension;
import ri.serien.libcommun.outils.fichier.GestionFichierCSV;

/**
 * Micro navigateur internet
 */
public class viewerHTMLEdition extends JPanel implements HyperlinkListener, ActionListener {
  
  private JButton ButtonPrint;
  private JButton ButtonSave;
  private JLabel titre = null;
  private JTextField urlField;
  private JEditorPane htmlPane;
  private String initialURL;
  private JPanel topPanel = null;
  private GestionFichierCSV fichiercsv = null;
  private String nomFic = null;
  
  public viewerHTMLEdition(String initialURL, GestionFichierCSV gfcsv) {
    this.initialURL = initialURL;
    fichiercsv = gfcsv;
    setLayout(new BorderLayout());
    topPanel = new JPanel();
    topPanel.setBackground(Color.lightGray);
    titre = new JLabel(gfcsv.getNomFichier() + "\t");
    titre.setFont(new Font("Arial", Font.BOLD, 20));
    titre.setForeground(Color.BLUE);
    topPanel.add(titre);
    ButtonPrint = new JButton(/*new ImageIcon("C:\\Users\\ritousv\\nim\\rt\\Toulouse\\images\\imprimer48.png")*/);
    ButtonPrint.setToolTipText("Impimer");
    ButtonPrint.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent evt) {
        PrinterJob printJob = PrinterJob.getPrinterJob();
        PageFormat pageFormat = printJob.defaultPage();
        printJob.setPrintable(null, pageFormat);
        if (printJob.printDialog()) { // le dialogue d'impression
          try {
            printJob.print();
          }
          catch (PrinterException exception) {
            // JOptionPane.showMessageDialog(this, exception);
          }
        }
      }
    });
    topPanel.add(ButtonPrint);
    ButtonSave = new JButton(new ImageIcon("C:\\Users\\ritousv\\nim\\rt\\Toulouse\\images\\disquette48.png"));
    ButtonSave.setToolTipText("Enregistrer");
    ButtonSave.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent evt) {
        if (fichiercsv != null) {
          JFileChooser fileChooser = new JFileChooser();
          GestionFichierCSV.initFiltre(fileChooser);
          // d.setCurrentDirectory(new File("c:\\windows\\bureau"));
          fileChooser.setSelectedFile(new File(fichiercsv.getNomFichier()));
          if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            if (fileChooser.getSelectedFile().getName().lastIndexOf('.') == -1) {
              nomFic = fileChooser.getSelectedFile().getPath().trim() + "."
                  + ((FiltreFichierParExtension) fileChooser.getFileFilter()).getFiltreExtension();
            }
            else {
              nomFic = fileChooser.getSelectedFile().getPath();
            }
            
            fichiercsv.setNomFichier(nomFic);
            // if (nomFic.toLowerCase().endsWith(GestionFichierCSV.FILTER_EXT_XLS))
            // fichiercsv.CSVtoXLS();
            // else
            if (nomFic.toLowerCase().endsWith(GestionFichierCSV.FILTER_EXT_HTM)) {
              fichiercsv.CSVtoHTM();
            }
            fichiercsv.ecritureFichier();
          }
        }
      }
    });
    topPanel.add(ButtonSave);
    
    add(topPanel, BorderLayout.NORTH);
    
    try {
      htmlPane = new JEditorPane(initialURL);
      htmlPane.setEditable(false);
      htmlPane.addHyperlinkListener(this);
      // JScrollPane scrollPane = new JScrollPane(htmlPane);
      // add(scrollPane, BorderLayout.CENTER);
      add(htmlPane, BorderLayout.CENTER);
    }
    catch (IOException ioe) {
      warnUser("Impossible de construire la page pour " + initialURL + ": " + ioe);
    }
  }
  
  public viewerHTMLEdition(String initialURL, boolean visible, GestionFichierCSV gfcsv) {
    this(initialURL, gfcsv);
    setAdresseVisible(visible);
    // fichiercsv = gfcsv;
  }
  
  public void setAdresseVisible(boolean visible) {
    topPanel.setVisible(visible);
  }
  
  @Override
  public void actionPerformed(ActionEvent event) {
    String url;
    if (event.getSource() == urlField) {
      url = urlField.getText();
    }
    else {
      url = initialURL;
    }
    try {
      htmlPane.setPage(new URL(url));
      urlField.setText(url);
    }
    catch (Exception ioe) {
      warnUser("Impossible de se connecter au lien " + url + ": " + ioe);
    }
  }
  
  @Override
  public void hyperlinkUpdate(HyperlinkEvent event) {
    if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
      try {
        htmlPane.setPage(event.getURL());
        urlField.setText(event.getURL().toExternalForm());
      }
      catch (IOException ioe) {
        warnUser("Impossible de se connecter au lien " + event.getURL().toExternalForm() + ": " + ioe);
      }
    }
  }
  
  private void warnUser(String message) {
    JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
  }
}
