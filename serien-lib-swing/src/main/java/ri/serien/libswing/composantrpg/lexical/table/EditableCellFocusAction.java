/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical.table;

import java.awt.event.ActionEvent;

import javax.swing.KeyStroke;

/**
 * Ne sert pas mais à voir sur des listes éditables plus complexe 25/07/2013.
 */
public class EditableCellFocusAction extends WrappedAction {
  private XRiTable table;
  
  /*
   *  Specify the component and KeyStroke for the Action we want to wrap
   */
  public EditableCellFocusAction(XRiTable table, KeyStroke keyStroke) {
    super(table, keyStroke);
    this.table = table;
  }
  
  /*
   *  Provide the custom behaviour of the Action
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    int originalRow = table.getSelectedRow();
    int originalColumn = table.getSelectedColumn();
    
    invokeOriginalAction(e);
    
    int row = table.getSelectedRow();
    int column = table.getSelectedColumn();
    
    // Keep invoking the original action until we find an editable cell
    
    while (!table.isCellEditable(row, column)) {
      invokeOriginalAction(e);
      
      // We didn't move anywhere, reset cell selection and get out.
      
      if (row == table.getSelectedRow() && column == table.getSelectedColumn()) {
        table.changeSelection(originalRow, originalColumn, false, false);
        break;
      }
      
      row = table.getSelectedRow();
      column = table.getSelectedColumn();
      
      // Back to where we started, get out.
      
      if (row == originalRow && column == originalColumn) {
        break;
      }
    }
  }
}
