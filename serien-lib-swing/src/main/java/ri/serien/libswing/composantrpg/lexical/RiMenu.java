/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JMenuBar;

public class RiMenu extends JMenuBar {
  private boolean isOuvert = false;
  
  /**
   * Constructeur.
   */
  public RiMenu() {
    super();
    setMargin(new Insets(0, -5, 0, 0));
    setPreferredSize(new Dimension(170, 36));
    
  }
  
  // Les ascesseurs ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  public boolean isOuvert() {
    return isOuvert;
  }
  
  public void setOuvert(boolean isOuvert) {
    this.isOuvert = isOuvert;
  }
  
}
