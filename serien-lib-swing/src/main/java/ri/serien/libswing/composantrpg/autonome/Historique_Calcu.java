/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 * Cette classe instancie une pop-up contenant l'historique des calculs éffectués par la calculatrice Série M
 * @author David Biason
 * @version 1.0
 */
public class Historique_Calcu extends JFrame {

  /**
   * collection contenant l'ensemble des données saisies
   */
  private ArrayList<ArrayList<String>> listeDonnees = null;
  /**
   * listeDonnees convertie en tableau de String pour favoriser l'implantation des données dans la JTable
   */
  private String[][] tabDonnees = null;
  /**
   * en-tête de la JTable
   */
  private String[] head_nom = { "Nombre 1", "Op", "Nombre 2", "Résultat" };

  /**
   * Constructeur de la pop-up
   */
  public Historique_Calcu() {
    initComponents();
    setTitle("Historique de la calculatrice");
    listeDonnees = new ArrayList<ArrayList<String>>();
  }

  /**
   * Convertir la collection <b>listeDonnees</b> en tableau <b>tabDonnees</b>
   * @param liste
   */
  public void convertirDonnees(ArrayList<ArrayList<String>> liste) {
    int nbLignes = 0;

    if (liste.size() > 20) {
      nbLignes = liste.size();
    }
    else {
      nbLignes = 20;
    }

    tabDonnees = new String[nbLignes][4];

    for (int i = 0; i < liste.size(); i++) {
      for (int j = 0; j < 4; j++) {
        tabDonnees[i][j] = liste.get(i).get(j);
      }
    }
  }

  /**
   * Modifie la JTable avec les données converties
   * @param liste
   */
  public void modifierTable(ArrayList<ArrayList<String>> liste) {
    if (liste != null) {
      convertirDonnees(liste);
      table1.setModel(new DefaultTableModel(tabDonnees, head_nom));
    }
  }

  /**
   * Rajoute une ligne à la collection de calculs <b>listeDonnees</b>
   * @param liste
   */
  public void majDonnees(ArrayList<String> liste) {
    if (liste != null) {
      listeDonnees.add(0, liste);
    }

    modifierTable(listeDonnees);
  }

  /**
   * Effectue une remise à zéro des éléments des données de la pop-up.
   */
  public void razDonnees() {
    tabDonnees = null;
    listeDonnees = new ArrayList<ArrayList<String>>();
    majDonnees(null);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    scrollPane1 = new JScrollPane();
    table1 = new JTable();

    // ======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setResizable(false);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      // ======== scrollPane1 ========
      {
        scrollPane1.setName("scrollPane1");

        // ---- table1 ----
        table1.setModel(new DefaultTableModel(
            new Object[][] { { null, null, "", "" }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
                { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
                { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
                { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
                { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, },
            new String[] { "Nombre 1", "Op", "Nombre 2", "R\u00e9sultat" }) {
          boolean[] columnEditable = new boolean[] { false, false, false, false };

          @Override
          public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnEditable[columnIndex];
          }
        });
        {
          TableColumnModel cm = table1.getColumnModel();
          cm.getColumn(0).setResizable(false);
          cm.getColumn(0).setPreferredWidth(100);
          cm.getColumn(1).setResizable(false);
          cm.getColumn(1).setPreferredWidth(30);
          cm.getColumn(2).setResizable(false);
          cm.getColumn(2).setPreferredWidth(100);
          cm.getColumn(3).setResizable(false);
          cm.getColumn(3).setPreferredWidth(180);
        }
        table1.setFont(table1.getFont().deriveFont(table1.getFont().getSize() - 1f));
        table1.setName("table1");
        scrollPane1.setViewportView(table1);
      }
      panel1.add(scrollPane1);
      scrollPane1.setBounds(0, 0, 330, 345);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(panel1, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JScrollPane scrollPane1;
  private JTable table1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
