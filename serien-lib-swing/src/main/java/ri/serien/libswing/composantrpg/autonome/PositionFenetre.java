/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;

import ri.serien.libcommun.outils.Trace;

/**
 * Gére la position d'une fenêtre (Frame).
 *
 * Cette classe est chargée de conserver la position d'une fenêtre afin de la rétablir dans cette position via la méthode
 * positionnerFenetre().
 * Les informations conservées sur la fenêtre sont : les coordonnées x et y, la largeur, la hauteur, le caractère maximisée ou non.
 * La classe gère également un jeu de position standards.
 */
public class PositionFenetre {
  // Constantes
  private final static int LARGEUR_PAR_DEFAUT = 1280;
  private final static int HAUTEUR_PAR_DEFAUT = 800;
  private final static int LARGEUR_STANDARD_2 = 1280;
  private final static int HAUTEUR_STANDARD_2 = 1024;
  private final static int LARGEUR_STANDARD_3 = 1440;
  private final static int HAUTEUR_STANDARD_3 = 900;
  
  // Variables
  private boolean maximise = false;
  private int indexEcran = -1;
  private int positionX = 0;
  private int positionY = 0;
  private int largeur = LARGEUR_PAR_DEFAUT;
  private int hauteur = HAUTEUR_PAR_DEFAUT;
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Modifier la position et la taille en sa basant sur celles de la fenêtre fournie en paramètre.
   */
  public void memoriserPosition(Frame pFrame) {
    // Récupérer l'écran courant
    GraphicsConfiguration graphicsConfiguration = pFrame.getGraphicsConfiguration();
    GraphicsDevice graphicsDevice = graphicsConfiguration.getDevice();
    
    // Déterminer l'index de l'écran
    GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] listGraphicsDevice = graphicsEnvironment.getScreenDevices();
    indexEcran = -1;
    for (int i = 0; i < listGraphicsDevice.length; i++) {
      if (listGraphicsDevice[i].equals(graphicsDevice)) {
        indexEcran = i;
        break;
      }
    }
    if ((pFrame.getExtendedState() & Frame.ICONIFIED) == Frame.ICONIFIED) {
      // On ne sauvegarde pas cet état
    }
    else if ((pFrame.getExtendedState() & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH) {
      maximise = true;
    }
    else {
      maximise = false;
      
      // Déterminer l'abscisse de la fenêtre relativement à l'écran
      if (graphicsDevice != null && graphicsDevice.getDefaultConfiguration().getBounds().x < pFrame.getX()) {
        positionX = pFrame.getX() - graphicsDevice.getDefaultConfiguration().getBounds().x;
      }
      else {
        positionX = pFrame.getX();
      }
      
      // Déterminer l'ordonné de la fenêtre relativement à l'écran
      if (graphicsDevice != null && graphicsDevice.getDefaultConfiguration().getBounds().y < pFrame.getY()) {
        positionY = pFrame.getY() - graphicsDevice.getDefaultConfiguration().getBounds().y;
      }
      else {
        positionY = pFrame.getY();
      }
      
      // Mémoriser la la taille de la fenêtre
      largeur = pFrame.getWidth();
      hauteur = pFrame.getHeight();
    }
  }
  
  /**
   * Positionner la fenêtre fournie en paramètre suivant les dimensions standards.
   * La classe a plusieurs jeux de dimensions standards. Chaque appel à cette méthode passe au jeu suivant.
   */
  public void positionnerFenetreSuivantStandard(Frame pFrame) {
    if (maximise) {
      maximise = false;
      largeur = LARGEUR_PAR_DEFAUT;
      hauteur = HAUTEUR_PAR_DEFAUT;
    }
    else if (largeur == LARGEUR_PAR_DEFAUT && hauteur == HAUTEUR_PAR_DEFAUT) {
      maximise = false;
      largeur = LARGEUR_STANDARD_2;
      hauteur = HAUTEUR_STANDARD_2;
    }
    else if (largeur == LARGEUR_STANDARD_2 && hauteur == HAUTEUR_STANDARD_2) {
      maximise = false;
      largeur = LARGEUR_STANDARD_3;
      hauteur = HAUTEUR_STANDARD_3;
    }
    else {
      maximise = true;
      largeur = LARGEUR_PAR_DEFAUT;
      hauteur = HAUTEUR_PAR_DEFAUT;
    }
    positionnerFenetre(pFrame);
  }
  
  /**
   * Positionner la fenêtre fournie en paramètre suivant les valeurs de cet objet.
   */
  public void positionnerFenetre(Frame pFrame) {
    try {
      corrigerPosition();
      
      GraphicsDevice graphicsDevice = getEcranConfigure();
      if (graphicsDevice != null) {
        pFrame.setLocation(graphicsDevice.getDefaultConfiguration().getBounds().x + positionX,
            graphicsDevice.getDefaultConfiguration().getBounds().y + positionY);
        pFrame.setSize(largeur, hauteur);
      }
      else {
        pFrame.setLocation(positionX, positionY);
        pFrame.setSize(largeur, hauteur);
      }
      
      if (maximise) {
        pFrame.setExtendedState(Frame.MAXIMIZED_BOTH);
      }
      else {
        pFrame.setExtendedState(Frame.NORMAL);
      }
    }
    catch (Exception e) {
      // Dans tous les cas, il ne faut pas empêcher le démarrage de l'application pour des problèmes de positionnement de fenêtre
      Trace.alerte("Erreur lors du positionnement de la fenêtre principale à partir des préférences utilisateurs.");
    }
  }
  
  /**
   * Récupérer l'écran correspondant à l'index mémorisé.
   */
  private GraphicsDevice getEcranConfigure() {
    GraphicsEnvironment draphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] listGraphicsDevice = draphicsEnvironment.getScreenDevices();
    if (indexEcran > -1 && indexEcran < listGraphicsDevice.length) {
      return listGraphicsDevice[indexEcran];
    }
    else if (listGraphicsDevice.length > 0) {
      return listGraphicsDevice[0];
    }
    return null;
  }
  
  /**
   * Contrôler et corriger la taille souhaitée pour la fenêtre.
   * Si la fenêtre est très petite, les dimensions sont corrigées pour attaindre la moitié de la taille standard.
   * Si la fenêtre st plus grande que l'écran, les dimensions sont réajustées à la taille de l'écran.
   */
  private void corrigerPosition() {
    GraphicsDevice graphicsDevice = getEcranConfigure();
    
    // Lire les dimensions de l'écran courant
    int largeurEcran = Toolkit.getDefaultToolkit().getScreenSize().width;
    int hauteurEcran = Toolkit.getDefaultToolkit().getScreenSize().height;
    if (graphicsDevice != null) {
      largeurEcran = graphicsDevice.getDefaultConfiguration().getBounds().width;
      hauteurEcran = graphicsDevice.getDefaultConfiguration().getBounds().height;
    }
    
    // Contrôler la largeur
    if (largeur < LARGEUR_PAR_DEFAUT / 2) {
      largeur = LARGEUR_PAR_DEFAUT / 2;
    }
    if (largeur > largeurEcran) {
      largeur = largeurEcran;
    }
    
    // Contrôler la hauteur
    if (hauteur < HAUTEUR_PAR_DEFAUT / 2) {
      hauteur = HAUTEUR_PAR_DEFAUT / 2;
    }
    if (hauteur > hauteurEcran) {
      hauteur = hauteurEcran;
    }
    
    // Contrôler la position x
    if (positionX < -10) {
      positionX = -10;
    }
    else if (positionX + largeur > largeurEcran) {
      positionX = largeurEcran - largeur;
    }
    
    // Contrôler la position y
    if (positionY < 0) {
      positionY = 0;
    }
    else if (positionY + hauteur > hauteurEcran) {
      positionY = hauteurEcran - hauteur;
    }
  }
}
