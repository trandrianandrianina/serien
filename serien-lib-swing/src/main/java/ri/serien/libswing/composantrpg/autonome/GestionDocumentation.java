/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.SessionTransfert;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Permet de supprimer ou d'ajouter un document dans une fenêtre indépendante
 */
public class GestionDocumentation extends JFrame {
  // Variables
  public boolean open = false;
  private final GestionDocumentation gestionDoc = this;
  private Lexical lexique = null;
  private String cheminDossier = null;
  private SessionTransfert trfSession = null;
  private boolean isIFS = true;

  /**
   * Constructeur
   */
  public GestionDocumentation(Lexical lex, String chemin) {
    super();
    initComponents();

    lexique = lex;
    cheminDossier = chemin;
    trfSession = ManagerSessionClient.getInstance().getEnvUser().getTransfertSession();
    isIFS = ctrlIFSPath(cheminDossier);
    if (isIFS) {
      gestionDragndrop(l_ListeDoc);
      setSize(400, 200);
      setTitle("Liste des documents");
      l_ListeDoc.setModel(new DefaultListModel());
      listageDossier();
      open = true;
    }
    else {
      setVisible(false);
      ouvreDossier();
      open = false;
    }
  }

  /**
   * Controle si le chemin est un chemin Windows ou IFS
   * @param chemin
   * @return
   */
  private boolean ctrlIFSPath(String chemin) {
    if ((chemin.indexOf(':') != -1) || (chemin.startsWith("&&"))) {
      cheminDossier = cheminDossier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
      return false;
    }
    return true;
  }

  /**
   * Listage du dossier et remplissage de la liste
   */
  private void listageDossier() {
    if (cheminDossier == null) {
      return;
    }
    String[] listedoc = ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().listerDossier(cheminDossier);
    ((DefaultListModel) l_ListeDoc.getModel()).clear();

    // Filtre sur les documents
    for (int i = 1; i < listedoc.length; i++) {
      if (!listedoc[i].equalsIgnoreCase("Thumbs.db")) {
        /*
        if ( (listedoc[i].toLowerCase().lastIndexOf(".gif") == -1)
            && (listedoc[i].toLowerCase().lastIndexOf(".jpeg") == -1)
            && (listedoc[i].toLowerCase().lastIndexOf(".jpg") == -1)
            && (listedoc[i].toLowerCase().lastIndexOf(".png") == -1)
            && (listedoc[i].toLowerCase().lastIndexOf(".bmp") == -1)
            && (!listedoc[i].equalsIgnoreCase("Thumbs.db")) )
            */
        ((DefaultListModel) l_ListeDoc.getModel()).addElement(listedoc[i]);
      }
    }

    validate();
    repaint();
  }

  /**
   * Détermine si c'est un fichier image ou pas
   * @param fichier
   * @return
   *
   *         private boolean isFichierImage(String fichier)
   *         {
   *         if (fichier == null) return false;
   *         fichier = fichier.toLowerCase();
   *         return (!fichier.startsWith(Constantes.THUMBAIL)) &&
   *         ( (fichier.lastIndexOf(".gif") != -1) || (fichier.lastIndexOf(".jpg") != -1) ||
   *         (fichier.lastIndexOf(".jpeg") != -1)
   *         || (fichier.lastIndexOf(".png") != -1) || (fichier.lastIndexOf(".bmp") != -1) );
   *         }
   */

  /**
   * Gestion du drag and drop
   * @param composant
   */
  private void gestionDragndrop(final JList composant) {
    new FileDrop(null, composant, /*dragBorder,*/new FileDrop.Listener() {
      @Override
      public void filesDropped(File[] files) {
        for (int i = 0; i < files.length; i++) {
          if (!files[i].isFile()) {
            JOptionPane.showMessageDialog(gestionDoc, "Le fichier " + files[i].getName() + " n'est pas un fichier, il est donc ignoré.",
                "Attention !!", JOptionPane.WARNING_MESSAGE);
            continue;
          }
          if (isIFS) {
            trfSession.fichierAUploader(files[i], cheminDossier);
            trfSession.fichierUploade(files[i], cheminDossier);
          } /*
            else{
            FileNG fichier = new FileNG(files[i].getAbsolutePath());
            fichier.copyTo(cheminDossier + File.separatorChar + files[i].getName());
            }*/
        }
        // On rafraichit la liste des images du serveur suite à l'upload
        listageDossier();
      }
    });
  }

  /**
   * Consultation des fichiers
   */
  private void consulteFichier() {
    int[] listeIndices = l_ListeDoc.getSelectedIndices();
    // for (int i=0; i<listeIndices.length; i++)
    if (listeIndices.length == 0) {
      return;
    }

    boolean supprfichiercache = false;

    String nomDossierTempUser =
        ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
    String fichier = (String) ((DefaultListModel) l_ListeDoc.getModel()).getElementAt(l_ListeDoc.getSelectedIndex()/*listeIndices[0]*/);
    String chfichierdistant = cheminDossier + Constantes.SEPARATEUR_DOSSIER_CHAR + fichier;
    File chfichiercacheori =
        new File(nomDossierTempUser + File.separatorChar + chfichierdistant.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_'));
    File chfichiercachefin = new File(nomDossierTempUser + File.separatorChar + fichier);

    if (supprfichiercache) {
      chfichiercacheori.delete();
      chfichiercachefin.delete();
    }
    if (!chfichiercachefin.exists()) {
      trfSession.fichierArecup(chfichierdistant);
      chfichiercacheori.renameTo(chfichiercachefin);
    }
    LaunchViewer lv = new LaunchViewer(chfichiercachefin);
    if (!lv.lancerViewer()) {
      JOptionPane.showMessageDialog(null, lv.getMsgErreur(), "Affichage du document", JOptionPane.WARNING_MESSAGE);
    }
  }

  /**
   * Suppression des fichiers
   */
  private void supprimeFichier() {
    int[] listeIndices = l_ListeDoc.getSelectedIndices();
    if (listeIndices.length == 0) {
      return;
    }

    int reply = JOptionPane.showConfirmDialog(this, "Confirmez vous la suppression ?", getTitle(), JOptionPane.YES_NO_OPTION);
    if (reply == JOptionPane.NO_OPTION) {
      return;
    }

    for (int i = 0; i < listeIndices.length; i++) {
      String fichier =
          cheminDossier + Constantes.SEPARATEUR_CHAINE_CHAR + ((DefaultListModel) l_ListeDoc.getModel()).getElementAt(listeIndices[i]);
      trfSession.fichierASupprimer(fichier);
      // trfSession.fichierSuppr(fichier);
    }
    listageDossier();
  }

  /**
   * Ouvre l'explorateur sur le dossier en cours
   */
  private void ouvreDossier() {
    if (isIFS) {
      lexique.ouvrirExplorer(
          ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath());
    }
    else {
      lexique.ouvrirExplorer(cheminDossier, true);
    }
  }

  private void mi_SuppressionActionPerformed(ActionEvent e) {
    supprimeFichier();
  }

  private void pop_BTDPopupMenuWillBecomeVisible(PopupMenuEvent e) {
    boolean visible = l_ListeDoc.getSelectedIndices().length > 0;
    mi_Consultation.setEnabled(visible);
    mi_Suppression.setEnabled(visible);
  }

  private void thisWindowClosed(WindowEvent e) {
    open = false;
  }

  private void mi_ConsultationActionPerformed(ActionEvent e) {
    consulteFichier();
  }

  private void mi_OuvrirDossierActionPerformed(ActionEvent e) {
    ouvreDossier();
  }

  private void l_ListeDocMouseClicked(MouseEvent e) {
    if (e.getClickCount() == 2) {
      consulteFichier();
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    scrollPane1 = new JScrollPane();
    l_ListeDoc = new JList();
    pop_BTD = new JPopupMenu();
    mi_Consultation = new JMenuItem();
    mi_Suppression = new JMenuItem();
    mi_OuvrirDossier = new JMenuItem();

    // ======== this ========
    setTitle("Gestion des documents");
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setVisible(true);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setAlwaysOnTop(true);
    setName("this");
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        thisWindowClosed(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== scrollPane1 ========
    {
      scrollPane1.setName("scrollPane1");

      // ---- l_ListeDoc ----
      l_ListeDoc.setComponentPopupMenu(pop_BTD);
      l_ListeDoc.setName("l_ListeDoc");
      l_ListeDoc.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          l_ListeDocMouseClicked(e);
        }
      });
      scrollPane1.setViewportView(l_ListeDoc);
    }
    contentPane.add(scrollPane1, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());

    // ======== pop_BTD ========
    {
      pop_BTD.setName("pop_BTD");
      pop_BTD.addPopupMenuListener(new PopupMenuListener() {
        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        }

        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
          pop_BTDPopupMenuWillBecomeVisible(e);
        }
      });

      // ---- mi_Consultation ----
      mi_Consultation.setText("Consultation");
      mi_Consultation.setName("mi_Consultation");
      mi_Consultation.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_ConsultationActionPerformed(e);
        }
      });
      pop_BTD.add(mi_Consultation);

      // ---- mi_Suppression ----
      mi_Suppression.setText("Suppression");
      mi_Suppression.setName("mi_Suppression");
      mi_Suppression.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_SuppressionActionPerformed(e);
        }
      });
      pop_BTD.add(mi_Suppression);
      pop_BTD.addSeparator();

      // ---- mi_OuvrirDossier ----
      mi_OuvrirDossier.setText("Ouvrir le dossier");
      mi_OuvrirDossier.setName("mi_OuvrirDossier");
      mi_OuvrirDossier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_OuvrirDossierActionPerformed(e);
        }
      });
      pop_BTD.add(mi_OuvrirDossier);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JList l_ListeDoc;
  private JPopupMenu pop_BTD;
  private JMenuItem mi_Consultation;
  private JMenuItem mi_Suppression;
  private JMenuItem mi_OuvrirDossier;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
