/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JComponent;

import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * Description d'un XRiComposant.
 */
public interface IXRiComposant {
  public String getName();
  
  public void init(Lexical lexique, HashMap<String, oRecord> alisteRecord, iData ainterpreteurD,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, JComponent[] arequestcomposant);
  
  public void setData();
  
  public void getData();
  
  public void dispose();
}
