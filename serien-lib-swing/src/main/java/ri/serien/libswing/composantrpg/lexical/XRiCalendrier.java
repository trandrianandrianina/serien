/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.ParseException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;
import javax.swing.text.MaskFormatter;

import org.jdesktop.swingx.JXDatePicker;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;
import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Cette classe permet d'instancier un calendrier de type JXDatePicker formaté pour les dates de Série M.
 * Composant datepicker aménagé pour nos besoins.
 * Il y a possibilité de prendre en compte la touche Entrée (Validation d'un panel) si la variable activerToucheEntreeZoneSaisie est
 * initialisée à true.
 *
 * @todo Problème avec typesaisie (dans les propriétés) le TypeSaisieEditor n'est pas pris en compte
 * @todo Empêcher la saisie du bouton lorsque la zone est jugée NonEditable()
 **/
public class XRiCalendrier extends JXDatePicker implements IXRiComposant {
  // Constantes
  public static final String JJMMAA = "JJMMAA";
  public static final String MMAA = "MMAA";
  public static final int SAISIE_JJMMAA = 8;
  public static final int SAISIE_MMAA = 6;

  // Variables
  private JButton boutonCalendrier = null;
  private int typeSaisie = SAISIE_JJMMAA;
  private ImageIcon iconeCalendrier = null;
  private ImageIcon iconeCalendrier2 = null;
  private boolean selectionComplete = true;
  // Variables propres à l'interface
  private Lexical lexique = null;
  // Liste des hosfield en E/S utilisés par cet objet
  private oData hostField = null;
  // Liste des buffers constinuant le panel final (à voir si on peut s'en passer en gérant mieux les setData)
  private HashMap<String, oRecord> listeRecord = null;
  // Interpréteur de variables (à voir si on peut pas le récupérer avec lexical)
  private iData interpreteurD = null;
  // <- ne sert pas ici car fait le lien entre le hostfield et le composant
  private ConcurrentHashMap<oData, Object> listeoDataFlux = null;
  // Stocke l'objet qui a le focus
  private JComponent[] requestcomposant = null;
  // Indique si le composant est en mode éditable
  private boolean lock = false;
  // Active ou non la touche Entrée sur le champ de saisie
  private boolean activerToucheEntreeZoneSaisie = false;

  /**
   * Cet attribut est un tableau qui représente les chaines de caractères autorisées lors de la saisie de la date. La
   * première chaine est le format par défaut.
   */
  private String[][] formatsDates = { { "dd.MM.yy", "ddMMyy", "MMyy", ".MM.yy" }, { "MM.yy", "MMyy" } };

  /**
   * Cet attribut est un masque de saisie permettant de forcer l'utilsateur à saisir certains types de caractères dans
   * un ordre donné. Attention,actif seulement lors de la première saisie !!!
   */
  private MaskFormatter moule = null;

  /**
   * Cet attribut est l'éditeur permettant à l'utilisateur de saisir manuellement une date dans une zone formatée. Voir
   * <a href="http://java.sun.com/j2se/1.4.2/docs/api/javax/swing/JFormattedTextField.html">JFormattedTextField</a>
   */
  private JFormattedTextField zoneSaisie = new JFormattedTextField();

  /**
   * Constructeur prenant le type de date (<b>JJ.MM.AA (8)</b> ou <b>MM.AA(5)</b>) en paramètre.
   * @param type ce paramètre représente le type de date donné ( c'est à dire 8 caractères pour les dates complètes
   *          JJ.MM.AA ou 5 pour les dates MM.AA
   */
  public XRiCalendrier(int type) {
    super();
    setInit();
    setTypeSaisie(type);
    iconeCalendrier = new ImageIcon(getClass().getClassLoader().getResource("images/calendrier.png"));
    iconeCalendrier2 = new ImageIcon(getClass().getClassLoader().getResource("images/calendrier2.png"));
    modificationLook();
  }

  /**
   * Constructeur à vide -> il appelle le constructeur de type 8 par défaut.
   */
  public XRiCalendrier() {
    this(SAISIE_JJMMAA);
  }

  /**
   * Methode de oFrame un peu adaptées.
   * Stocke les variables qui composent la table.
   */
  @Override
  public void init(final Lexical lexique, HashMap<String, oRecord> pListeRecord, iData pInterpreteurD,
      ConcurrentHashMap<oData, Object> pListeoDataFlux, JComponent[] pRequestcomposant) {
    // Initialisation des variables nécessaires
    this.lexique = lexique;
    setInterpreteurD(pInterpreteurD);
    setListeRecord(pListeRecord);
    setListeoDataFlux(pListeoDataFlux);
    requestcomposant = pRequestcomposant;
  }

  /**
   * Initialise les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    hostField = lexique.getHostField(getName());
    if (hostField == null) {
      return;
    }

    if (hostField.getEvalVisibility() == Constantes.OFF) {
      setVisible(false);
    }
    else {
      setVisible(true);
      if (!hostField.isAlpha()) {
        this.getEditor().setHorizontalAlignment(SwingConstants.RIGHT);
      }
      this.getEditor().setText(hostField.getValeurToFrameWithTrimR());
      // On vérifie si le oData est en E/S
      if (lexique.isIntoCurrentRecord(hostField) && (hostField.getEvalReadOnly() == Constantes.OFF)) {
        // Filtre de saisie
        ((AbstractDocument) this.getEditor().getDocument()).setDocumentFilter(new SaisieDefinition(hostField.getLongueurFormatee(),
            !hostField.isLowerCase(), hostField.getCaractereAutorises(), hostField.isAlpha()));
        listeoDataFlux.put(hostField, this); // Nécessaire pour le getData de oFrame
        setEnabled(true);
        if (hostField.getEvalFocus() == Constantes.ON) {
          requestcomposant[0] = this;
        }
      }
      else {
        setEnabled(false);
      }
    }
  }

  /**
   * Met à jour le oData du flux courant.
   */
  @Override
  public void getData() {
    if (hostField != null) {
      hostField.setValeurFromFrame(this.getEditor().getText());
    }
  }

  private void modificationLook() {
    // Récupérer l'objet bouton
    for (int i = 0; i < getComponentCount(); i++) {
      if (getComponent(i) instanceof JButton) {
        boutonCalendrier = (JButton) getComponent(i);
      }
    }

    // Modifier l'aspect du bouton
    if (boutonCalendrier != null) {
      boutonCalendrier.setSize(40, 30);
      boutonCalendrier.setPreferredSize(new Dimension(40, 30));
      boutonCalendrier.setMaximumSize(new Dimension(40, 30));
      boutonCalendrier.setBorderPainted(false);
      boutonCalendrier.setContentAreaFilled(false);
      boutonCalendrier.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      if (iconeCalendrier != null) {
        boutonCalendrier.setIcon(iconeCalendrier2);
      }
      if (iconeCalendrier2 != null) {
        boutonCalendrier.setDisabledIcon(iconeCalendrier2);
      }

      boutonCalendrier.addMouseListener(new MouseListener() {
        @Override
        public void mouseReleased(MouseEvent arg0) {
        }

        @Override
        public void mousePressed(MouseEvent arg0) {
        }

        @Override
        public void mouseExited(MouseEvent arg0) {
          if (iconeCalendrier2 != null) {
            boutonCalendrier.setIcon(iconeCalendrier2);
          }
        }

        @Override
        public void mouseEntered(MouseEvent arg0) {
          if (iconeCalendrier != null) {
            boutonCalendrier.setIcon(iconeCalendrier);
          }
        }

        @Override
        public void mouseClicked(MouseEvent arg0) {
        }
      });
    }
  }

  // -- Accesseurs
  
  /**
   * @param listeRecord the listeRecord to set.
   */
  public void setListeRecord(HashMap<String, oRecord> listeRecord) {
    this.listeRecord = listeRecord;
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    if (!enabled) {
      getEditor().setEnabled(!enabled);
      getEditor().setEditable(enabled);
      getEditor().setBackground(Constantes.CL_ZONE_SORTIE);
    }
    else {
      getEditor().setEditable(enabled);
      getEditor().setBackground(Color.WHITE);
    }

  }

  public HashMap<String, oRecord> getListeRecord() {
    return listeRecord;
  }

  public iData getInterpreteurD() {
    return interpreteurD;
  }

  /**
   * @param interpreteurD the interpreteurD to set.
   */
  public void setInterpreteurD(iData interpreteurD) {
    this.interpreteurD = interpreteurD;
  }

  public ConcurrentHashMap<oData, Object> getListeoDataFlux() {
    return listeoDataFlux;
  }

  /**
   * @param listeoDataFlux the listeoDataFlux to set.
   */
  public void setListeoDataFlux(ConcurrentHashMap<oData, Object> listeoDataFlux) {
    this.listeoDataFlux = listeoDataFlux;
  }

  /**
   * Valider automatiquement la sélection saisie dans le textfield.
   * @throws ParseException
   */
  public void validerSelection() {
    // Envoyer la saisie au bloc Calendrier
    try {
      this.commitEdit();
    }
    catch (ParseException e) {
    }
  }

  /**
   * Permet de renvoyer le type de saisie (MMAA ou JJMMAA).
   */
  public int getTypeSaisie() {
    return typeSaisie;
  }

  /**
   * Permet de modifier le type de saisie (MMAA ou JJMMAA).
   */
  public void setTypeSaisie(int typeSaisie) {
    this.typeSaisie = typeSaisie;
    if (typeSaisie == SAISIE_JJMMAA) {
      this.setFormats(formatsDates[0]);
      this.setSize(115, this.getPreferredSize().height);
      // modele = "##.##.##";
    }
    else if (typeSaisie == SAISIE_MMAA) {
      this.setFormats(formatsDates[1]);
      this.setSize(100, this.getPreferredSize().height);
      // modele = "##.##";
    }
  }

  /**
   * Initialise le format de saisie.
   */
  private void setInit() {
    // Interception de la touche ENTER (pour l'activation du défaut bouton)
    zoneSaisie.addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent pKeyEvent) {
        if (pKeyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
          gererActionToucheEntree();
        }
      }
    });

    // Gérer les pertes et les gains de focus de la zone de saisie
    zoneSaisie.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent arg0) {
        if (selectionComplete) {
          if ((!zoneSaisie.getText().equals("  .  ")) && (!zoneSaisie.getText().equals("")) && (!zoneSaisie.getText().equals("  .  .  "))
              && (!zoneSaisie.getText().trim().startsWith("."))) {
            SwingUtilities.invokeLater(new Runnable() {
              @Override
              public void run() {
                zoneSaisie.selectAll();
              }
            });
          }
          else {
            zoneSaisie.setText(zoneSaisie.getText().trim());
            SwingUtilities.invokeLater(new Runnable() {
              @Override
              public void run() {
                zoneSaisie.setCaretPosition(0);
              }
            });
          }
        }
        else {
          if ((!zoneSaisie.getText().equals("  .  ")) && (!zoneSaisie.getText().equals(""))
              && (!zoneSaisie.getText().equals("  .  .  "))) {
            SwingUtilities.invokeLater(new Runnable() {
              @Override
              public void run() {
                zoneSaisie.setCaretPosition(0);
              }
            });
          }
        }
      }

      @Override
      public void focusLost(FocusEvent arg0) {
        validerSelection();
      }
    });

    this.setEditor(zoneSaisie);
  }
  
  /**
   * Gère la touche Entrée dans la zone de saisie.
   */
  public void gererActionToucheEntree() {
    // Si la variable lexique est initialisée et si la touche Entrée est active pour la zone de saisie
    if (activerToucheEntreeZoneSaisie && lexique != null) {
      lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
      return;
    }

    // Sinon
    try {
      if (getRootPane() != null && getRootPane().getDefaultButton() != null) {
        getRootPane().getDefaultButton().doClick();
      }
    }
    catch (Exception e) {
    }
  }

  @Override
  public void dispose() {
    ((AbstractDocument) this.getEditor().getDocument()).setDocumentFilter(null);
    boutonCalendrier = null;
    iconeCalendrier = null;
    iconeCalendrier2 = null;
    lexique = null;
    hostField = null;
    listeRecord = null;
    interpreteurD = null;
    listeoDataFlux = null;
    requestcomposant = null;
  }
  
  // -- Accesseurs

  /**
   * Modifie le type de sélection automatique sur prise de focus de la zone de saisie.
   */
  public void setSelectionComplete(boolean select) {
    selectionComplete = select;
  }

  /**
   * Accède au type de sélection automatique sur prise de focus de la zone de saisie.
   */
  public boolean isSelectionComplete() {
    return selectionComplete;
  }

  /**
   * Permet d'activer ou non l'action de la touche Entrée dans la zone de saisie.
   * Par défaut l'action est désactivée.
   */
  public void setActiverToucheEntreeZoneSaisie(boolean pActiverToucheEntreeZoneSaisie) {
    this.activerToucheEntreeZoneSaisie = pActiverToucheEntreeZoneSaisie;
  }
}
