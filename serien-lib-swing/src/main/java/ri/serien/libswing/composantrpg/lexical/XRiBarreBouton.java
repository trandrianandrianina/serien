/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.moteur.interpreteur.Lexical;

public class XRiBarreBouton extends SNBarreBouton {
  
  private Lexical lexique = null;
  
  // Boutons (le bouton rappel n'a pas été ajouté car on à pas trouvé son action)
  private static final String BOUTON_CREATION = "Créer";
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_CONSULTATION = "Consulter";
  // L'espace à la fin du mot est mis volontairement afin qu'il n'y ait pas d'ambiguité avec le EnumBouton.ANNULER
  // Il faudra trouver un autre libellé
  private static final String BOUTON_ANNULATION = "Annuler ";
  private static final String BOUTON_DUPLICATION = "Dupliquer";
  private static final String BOUTON_REACTIVATION = "Réactiver";
  private static final String BOUTON_DESTRUCTION = "Supprimer";
  
  // Liste contenant tout les noms des boutons
  private String[] listeNomDesBoutons = { BOUTON_CREATION, BOUTON_MODIFICATION, BOUTON_CONSULTATION, BOUTON_ANNULATION,
      BOUTON_DUPLICATION, BOUTON_REACTIVATION, BOUTON_DESTRUCTION };
  
  // Liste des boutons présent récuperé grâce au V01F
  private List<String> listeNomDesBoutonUtilisable = new ArrayList<String>();
  
  // Liste faisant le lien entre le code V01F et le nom du bouton
  private static final HashMap<Character, String> hashMapCodeV01FLieeNomBouton = new HashMap<Character, String>() {
    {
      put('C', BOUTON_CREATION);
    }
    
    {
      put('M', BOUTON_MODIFICATION);
    }
    
    {
      put('I', BOUTON_CONSULTATION);
    }
    
    {
      put('A', BOUTON_ANNULATION);
    }
    
    {
      put('D', BOUTON_DUPLICATION);
    }
    
    {
      put('r', BOUTON_REACTIVATION);
    }
    
    {
      put('S', BOUTON_DESTRUCTION);
    }
  };
  
  // Liste des caractère mnémonique pourr chaque bouton
  private static final HashMap<String, Character> caractereMnemonique = new HashMap<String, Character>() {
    {
      put(BOUTON_CREATION, 'T');
    }
    
    {
      put(BOUTON_MODIFICATION, 'M');
    }
    
    {
      put(BOUTON_CONSULTATION, 'C');
    }
    
    {
      put(BOUTON_ANNULATION, 'A');
    }
    
    {
      put(BOUTON_DUPLICATION, 'D');
    }
    
    {
      put(BOUTON_REACTIVATION, 'R');
    }
    
    {
      put(BOUTON_DESTRUCTION, 'D');
    }
  };
  
  // Liste faisant le lien entre le nom du bouton et son code action
  private static final HashMap<String, String> hashMapNomBoutonLieeCodeTouche = new HashMap<String, String>() {
    {
      put(BOUTON_CREATION, "F13");
    }
    
    {
      put(BOUTON_MODIFICATION, "F14");
    }
    
    {
      put(BOUTON_CONSULTATION, "F15");
    }
    
    {
      put(BOUTON_ANNULATION, "F16");
    }
    
    {
      put(BOUTON_DUPLICATION, "F18");
    }
    
    {
      put(BOUTON_REACTIVATION, "F17");
    }
    
    {
      put(BOUTON_DESTRUCTION, "F19");
    }
  };
  
  /**
   * Permet d'ajouter sur la barre bouton uniquement les boutons utilisables.
   */
  private void initialiser(Lexical pLexique) {
    lexique = pLexique;
    setName("xRiBarreBouton");
    
    // Recupère les infos du V01F s'il est présent
    oData data = pLexique.getHostField("V01F");
    if (data == null) {
      return;
    }
    String modesDisponibles = data.getValeurToFrameWithTrimR();
    if (modesDisponibles == null) {
      return;
    }
    
    // Recherche le mode actif
    char modeActif = ' ';
    int pos = modesDisponibles.indexOf('/');
    if (pos > -1) {
      modeActif = modesDisponibles.charAt(0);
      modesDisponibles = modesDisponibles.substring(pos + 1);
    }
    
    // Mets le code V01F sous format d'une liste de char afin de faire le traitement
    char[] listeModesDisponibles = modesDisponibles.toCharArray();
    
    // Parcourt de la liste des modes disponibles
    listeNomDesBoutonUtilisable.clear();
    for (int i = 0; i < listeModesDisponibles.length; i++) {
      // Permet de créer une liste des boutons utilisables grâce au VO1F
      if (hashMapCodeV01FLieeNomBouton.containsKey(listeModesDisponibles[i]) == true && modeActif != listeModesDisponibles[i]) {
        // Met le nom des boutons present dans une liste
        listeNomDesBoutonUtilisable.add(hashMapCodeV01FLieeNomBouton.get(listeModesDisponibles[i]));
      }
    }
    
    // Ajoute les bouton ne sont pas déjà dans la barre de bouton
    for (int i = 0; i < listeNomDesBoutonUtilisable.size(); i++) {
      SNBouton bouton = getBouton(listeNomDesBoutonUtilisable.get(i));
      Character mnemonique = caractereMnemonique.get(listeNomDesBoutonUtilisable.get(i));
      if (bouton == null) {
        ajouterBouton(listeNomDesBoutonUtilisable.get(i), mnemonique, true);
      }
    }
    
    // Suppression du bouton correspondant au mode actif en cours
    if (modeActif != ' ') {
      String libelleBouton = hashMapCodeV01FLieeNomBouton.get(modeActif);
      if (libelleBouton != null) {
        SNBouton bouton = getBouton(libelleBouton);
        if (bouton != null) {
          supprimerBouton(bouton);
        }
      }
    }
  }
  
  /**
   * Permet d'activer uniquement les boutons utilisables.
   */
  public void rafraichir(Lexical pLexique) {
    if (pLexique == null) {
      return;
    }
    initialiser(pLexique);
    
    // On désactive les boutons deja activé
    for (String libelle : listeNomDesBoutons) {
      if (getBouton(libelle) != null) {
        activerBouton(libelle, false);
      }
    }
    // On active exclusivement les boutons de la liste
    for (String libelle : listeNomDesBoutonUtilisable) {
      activerBouton(libelle, true);
    }
  }
  
  /**
   * Traiter les clics sur les boutons situés dans la barre de boutons.
   */
  public boolean isTraiterClickBouton(SNBouton pSNBouton) {
    // Si le bouton existe et que la liste contient le nom du bouton, alors on vas faire le traitement
    if (pSNBouton != null && listeNomDesBoutonUtilisable.contains(pSNBouton.getName())) {
      lexique.HostScreenSendKey(lexique.getPanel(), hashMapNomBoutonLieeCodeTouche.get(pSNBouton.getName()));
      return true;
    }
    return false;
  }
  
  /**
   * Retourne la liste des boutons actif
   */
  public List<String> getListeNomDesBoutonUtilisable() {
    return listeNomDesBoutonUtilisable;
  }
  
  public static String getBoutonCreation() {
    return BOUTON_CREATION;
  }
  
  public static String getBoutonModification() {
    return BOUTON_MODIFICATION;
  }
  
  public static String getBoutonConsultation() {
    return BOUTON_CONSULTATION;
  }
  
  public static String getBoutonAnnulation() {
    return BOUTON_ANNULATION;
  }
  
  public static String getBoutonDuplication() {
    return BOUTON_DUPLICATION;
  }
  
  public static String getBoutonReactivation() {
    return BOUTON_REACTIVATION;
  }
  
  public static String getBoutonDestruction() {
    return BOUTON_DESTRUCTION;
  }
  
}
