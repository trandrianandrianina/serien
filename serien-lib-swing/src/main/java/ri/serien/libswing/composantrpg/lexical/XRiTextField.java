/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.AbstractDocument;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;
import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Personnalisation du composant XRiTextField.
 * Composant héritant du JTextfield et de ses propriétés auxquelles certaines fonctionnalités ont été rajoutées.
 * @author ritoudb
 *
 */
public class XRiTextField extends JTextField implements IXRiComposant {
  private Lexical lexique = null;
  private oData hostField = null;
  private HashMap<String, oRecord> listeRecord = null;
  private iData interpreteurD = null;
  private ConcurrentHashMap<oData, Object> listeoDataFlux = null;
  private JComponent[] requestcomposant = null;
  private JPopupMenu popPressePapiers = new JPopupMenu();
  private Clipboard pressePapiers = Toolkit.getDefaultToolkit().getSystemClipboard();
  private JPopupMenu monClicDroit = null;
  private boolean isSelectAll = true;
  private char[] listeCaracteresNonAutorises = null;
  private boolean isModeLabel = false;
  private String monLabel = null;
  private int longueurMaxPartieEntiere = 7;
  private int longueurMaxPartieDecimale = 2;

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Constructeur.
   */
  public XRiTextField() {
    super();

    this.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent ev1) {
        passerModeSaisie();
      }

      @Override
      public void focusLost(FocusEvent ev2) {
        passerModeLabel();
      }
    });

    // Gestion du presse papiers
    // popPressePapiers = new JPopupMenu();
    JMenuItem menuItem = new JMenuItem("Copier vers le presse papiers");
    // pressePapiers = Toolkit.getDefaultToolkit().getSystemClipboard();
    menuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        pressePapiers.setContents(new StringSelection(((XRiTextField) popPressePapiers.getInvoker()).getText()), null);
      }
    });
    popPressePapiers.add(menuItem);
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Adapter une méthode de oFrame.
   * Stocker les variables qui composent la table.
   */
  @Override
  public void init(final Lexical lexique, HashMap<String, oRecord> alisteRecord, iData ainterpreteurD,
      ConcurrentHashMap<oData, Object> alisteoDataFlux, JComponent[] arequestcomposant) {
    // Initialisation des variables nécessaires
    this.lexique = lexique;
    setInterpreteurD(ainterpreteurD);
    setListeRecord(alisteRecord);
    setListeoDataFlux(alisteoDataFlux);
    requestcomposant = arequestcomposant;

    monClicDroit = getComponentPopupMenu();
  }
  
  /**
   * Initialiser les données et les propriétés du composant.
   */
  @Override
  public void setData() {
    // remettre en mode saisie par défaut
    initModeSaisie();

    hostField = lexique.getHostField(getName());
    if (hostField == null) {
      return;
    }

    if (hostField.getEvalVisibility() == Constantes.OFF) {
      setVisible(false);
    }
    else {
      setVisible(true);
      if (!hostField.isAlpha()) {
        this.setHorizontalAlignment(SwingConstants.RIGHT);
      }
      setText(hostField.getValeurToFrameWithTrimR());
      // listeoData.put(HostField.getNom(), HostField); // Nécessaire pour isPresent de Lexical
      // On vérifie si le oData est en E/S
      if (lexique.isIntoCurrentRecord(hostField) && (hostField.getEvalReadOnly() == Constantes.OFF)) {
        // Filtre de saisie
        if (((AbstractDocument) this.getDocument()).getDocumentFilter() == null) {
          ((AbstractDocument) this.getDocument()).setDocumentFilter(new SaisieDefinition(hostField.getLongueurFormatee(),
              !hostField.isLowerCase(), hostField.getCaractereAutorises(), listeCaracteresNonAutorises, hostField.isAlpha()));
        }
        listeoDataFlux.put(hostField, this); // Nécessaire pour le getData de oFrame
        // spécifique mode label
        if (hostField.getValeurToFrame().trim().equals("")) {
          passerModeLabel();
        }

        setEnabled(true);
        if (hostField.getEvalFocus() == Constantes.ON) {
          requestcomposant[0] = this;
        }
      }
      else {
        // spécifique RiTextfield
        if (hostField.getValeurToFrame().trim().equals("")) {
          passerModeLabel();
        }

        setEnabled(false);
      }
    }
  }
  
  /**
   * Mettre à jour le oData du flux courant.
   * TODO A voir si c'est pas mieux de le rajouter directement dans le getData du panel.
   */
  @Override
  public void getData() {
    if (!isVisible()) {
      return;
    }
    hostField.setValeurFromFrame(this.nettoyerLaSaisie());
  }
  
  @Override
  public void setEnabled(boolean pEnabled) {
    setEditable(pEnabled);
    setFocusable(pEnabled);

    // changement de look
    if (pEnabled) {
      setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_EDITABLE);
      if (isModeLabel) {
        setText("");
      }
    }
    else {
      setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
    }

    // gérer le clic droit de la zone
    gererClicDroit(pEnabled);
  }

  /**
   * Réinitialiser les données
   */
  @Override
  public void dispose() {
    ((AbstractDocument) this.getDocument()).setDocumentFilter(null);
    lexique = null;
    hostField = null;
    listeRecord = null;
    interpreteurD = null;
    listeoDataFlux = null;
    requestcomposant = null;
    popPressePapiers = null;
    pressePapiers = null;
    monClicDroit = null;
    listeCaracteresNonAutorises = null;
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Initialiser pour une utilisation hors écran RPG.
   */
  public void init(int aLongueur, boolean aMajuscule, boolean aAlphaNum) {
    ((AbstractDocument) this.getDocument()).setDocumentFilter(new SaisieDefinition(aLongueur, aMajuscule, aAlphaNum, false));
  }

  /**
   * Initialiser pour une utilisation hors écran RPG.
   */
  public void init(int aLongueur, int pTypeNumerique) {
    ((AbstractDocument) this.getDocument()).setDocumentFilter(new SaisieDefinition(aLongueur, pTypeNumerique));
  }

  /**
   * Forcer l'apparition du composant (à mettre dans le setData du panel) bien que cela défi les lois de la nature.
   */
  public void forceVisibility() {
    if (isVisible()) {
      return;
    }

    setVisible(true);
    setEnabled(false);

    // Force le rafraichissement de la valeur
    if (!hostField.isAlpha()) {
      this.setHorizontalAlignment(SwingConstants.RIGHT);
    }
    setText(hostField.getValeurToFrameWithTrimR());
  }

  /**
   * @todo A commenter.
   */
  public void activerModeFantome(String label) {
    monLabel = label;
    setToolTipText(monLabel);
  }
  
  /**
   * Afficher le label du JTextfield.
   */
  public void passerModeLabel() {
    if (monLabel != null && getText().trim().equals("")) {
      setForeground(Color.lightGray);
      this.setText(monLabel);
      isModeLabel = true;
    }
  }

  /**
   * Passer en mode saisie : supprimer le label et changer la couleur.
   */
  public void passerModeSaisie() {
    if (isModeLabel) {
      setText("");
      setForeground(Color.black);
    }
    else if (isEditable() && isSelectAll) {
      selectAll();
    }

    isModeLabel = false;
  }

  /**
   * Nettoyer la saisie avant getData() afin de faire disparaitre le label si présent.
   */
  public String nettoyerLaSaisie() {
    if (isModeLabel) {
      return "";
    }
    else {
      return getText();
    }
  }

  /**
   * Fixer le nombre maximal de caractères que l'on peut saisir dans un RITextField.
   */
  public void setLongueurSaisie(int maxLength) {
    ((AbstractDocument) getDocument()).setDocumentFilter(new SaisieDefinition(maxLength));
  }
  
  /**
   * Vérifier si le format du montant est correct.
   * @param pLongueurMaxPartieEntiere : le nombre maximal de chiffre pour la partie entière du montant.
   * @param pLongueurMaxPartieDecimale : le nombre maximal de chiffre pour la partie décimale du montant.
   * @param pCaractereTape : le caractère tapé par l'utilisateur.
   * @return true si le monatant renseigné est correct et false dans le cas contraire.
   */
  public boolean verifierFormatMontant(int pLongueurMaxPartieEntiere, int pLongueurMaxPartieDecimale, char pCaractereTape) {
    if (pLongueurMaxPartieEntiere > longueurMaxPartieEntiere || pLongueurMaxPartieDecimale > longueurMaxPartieDecimale) {
      pLongueurMaxPartieEntiere = longueurMaxPartieEntiere;
      pLongueurMaxPartieDecimale = longueurMaxPartieDecimale;
    }
    String montant = this.getText();
    montant =
        montant.substring(0, this.getCaretPosition()) + pCaractereTape + montant.substring(this.getCaretPosition(), montant.length());
    // Vérifier si le texte matche avec le format défini
    if (montant.matches("^[0-9]{0," + pLongueurMaxPartieEntiere + "}([.,][0-9]{0," + pLongueurMaxPartieDecimale + "})?$")) {
      return true;
    }
    else {
      return false;
    }
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialiser le mode saisie en mode label.
   */
  private void initModeSaisie() {
    isModeLabel = false;
    setForeground(Color.black);
  }
  
  /**
   * Gérer le clic droit
   * @param pEnabled clic droit
   */
  private void gererClicDroit(boolean pEnabled) {
    // si la zone est bloquée ou non
    if (!pEnabled) {
      setComponentPopupMenu(popPressePapiers);
    }
    else {
      setComponentPopupMenu(monClicDroit);
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Mettre à jour la liste des buffers.
   * @param pListeRecord Liste des buffers.
   */
  public void setListeRecord(HashMap<String, oRecord> pListeRecord) {
    this.listeRecord = pListeRecord;
  }

  /**
   * Retourner la liste des buffers constituant le panel final.
   * @return Liste des buffers.
   */
  public HashMap<String, oRecord> getListeRecord() {
    return listeRecord;
  }

  /**
   * Retourner l'interpréteur de variables.
   * @return L'interpréteur de variables.
   */
  public iData getInterpreteurD() {
    return interpreteurD;
  }

  /**
   * Mettre à jour l'interpreteur de variables.
   * @param pInterpreteurD L'interpreteur de variables.
   */
  public void setInterpreteurD(iData pInterpreteurD) {
    this.interpreteurD = pInterpreteurD;
  }

  /**
   * Retourner le flux.
   * @return La liste de données du flux.
   */
  public ConcurrentHashMap<oData, Object> getListeoDataFlux() {
    return listeoDataFlux;
  }

  /**
   * Mettre à jour le flux.
   * @param pListeoDataFlux Le flux.
   */
  public void setListeoDataFlux(ConcurrentHashMap<oData, Object> pListeoDataFlux) {
    this.listeoDataFlux = pListeoDataFlux;
  }

  /**
   * Tester si le champ est en mode label.
   * @return true si le champ est en model label et false sinon.
   */
  public boolean isEnModeLabel() {
    return isModeLabel;
  }

  /**
   * Retourner le label.
   * @return Le label.
   */
  public String getLabel() {
    return monLabel;
  }

  /**
   * Tester si la fonction de sélection complète est active ou pas.
   * @return true s'il y a sélection complète et false sinon.
   */
  public boolean isSelectAll() {
    return isSelectAll;
  }

  /**
   * Activer ou pas le sélection complète lors du focus sur la zone.
   * @param pIsSelectAll est à true si le texte est complètement sélectionné et false sinon.
   */
  public void setSelectAll(boolean pIsSelectAll) {
    this.isSelectAll = pIsSelectAll;
  }

  /**
   * Retourner la liste des caractères non autorisés.
   * @return La liste des caractères non autorisés.
   */
  public char[] getListeCaracteresNonAutorises() {
    return listeCaracteresNonAutorises;
  }

  /**
   * Mettre à jour la liste des caractères non autorisés dans le champ.
   * @param pListeCaracteresNonAutorises La liste des caractères non autorisés dans le champ.
   */
  public void setListeCaracteresNonAutorises(char[] pListeCaracteresNonAutorises) {
    this.listeCaracteresNonAutorises = pListeCaracteresNonAutorises;
  }
}
