/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.lexical;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Panel permettant la navigation sur image.
 * C'est un JPanel Pas de paramètres. Voir VGAM15FM_A2 pour un exemplev.
 */
public class RiPanelNav extends JPanel {
  // Constantes
  
  public static final int ETAT_NEUTRE = 0;
  public static final int ETAT_SELECTION = 1;
  public static final int ETAT_ENCOURS = 2;
  
  // Variables
  private JLabel labelNav;
  private ImageIcon imageDeFond = null; // new
  // ImageIcon((getClass().getClassLoader().getResource("images/fac999.jpg")));
  private ImageIcon[] imgEtat = new ImageIcon[3];
  private ArrayList<RiBoutonNav> arrayBoutons = new ArrayList<RiBoutonNav>();
  private int boutonActif = 0;
  
  /**
   * Constructeur.
   */
  public RiPanelNav() {
    super();
    setOpaque(false);
    initComponents();
    labelNav.setPreferredSize(new Dimension(160, 250));
    labelNav.setOpaque(false);
  }
  
  /**
   * Placer les boutons sur le panel.
   * On paramètre et on place le fond décoratif sur le panel.
   */
  public void setData() {
    for (int i = 0; i < arrayBoutons.size(); i++) {
      this.add(arrayBoutons.get(i));
    }
    
    initImage();
    this.add(labelNav);
  }
  
  /**
   * Instancie un bouton de navigation.
   */
  public void setBoutonNav(int positionXbouton, int positionYbouton, int largeurBouton, int hauteurBouton, String nomBouton,
      String hoverhelpBouton, boolean enCoursBouton, JButton boutonAactionner) {
    
    try {
      arrayBoutons.add(new RiBoutonNav(positionXbouton, positionYbouton, largeurBouton, hauteurBouton, nomBouton, hoverhelpBouton,
          enCoursBouton, boutonAactionner));
    }
    catch (Throwable e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    setData();
  }
  
  /**
   * Fixe la taille du panel décoratif.
   */
  @Override
  public void setPreferredSize(Dimension d) {
    labelNav.setPreferredSize(d);
  }
  
  // fixe l'image de fond du panel décoratif
  public void setImageDeFond(ImageIcon img) {
    imageDeFond = img;
    labelNav.setIcon(imageDeFond);
  }
  
  // Mets à jour les images liées aux trois états de navigation
  public void setImageEtat(ImageIcon[] img) {
    imgEtat = img;
  }
  
  /**
   * Pour rentrer une image dans le tableau des images d'état de navigation.
   */
  public void setImageEtatAtIndex(int index, ImageIcon img) {
    if ((index < 0) || (index > 2)) {
      return;
    }
    imgEtat[index] = img;
  }
  
  public int getBoutonActif() {
    return boutonActif;
  }
  
  /**
   * initialise l'image de fond du panel décoratif.
   */
  private void initImage() {
    labelNav.setIcon(imageDeFond);
  }
  
  private void initComponents() {
    labelNav = new JLabel();
    
    // ======== this ========
    setOpaque(false);
    setName("this");
    setLayout(null);
    
    // ---- labelNav ----
    labelNav.setPreferredSize(new Dimension(160, 250));
    labelNav.setMinimumSize(new Dimension(160, 250));
    labelNav.setMaximumSize(new Dimension(160, 250));
    labelNav.setName("labelNav");
    add(labelNav);
    labelNav.setBounds(new Rectangle(new Point(0, 0), labelNav.getPreferredSize()));
    
    { // compute preferred size
      Dimension preferredSize = new Dimension();
      for (int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }
    
  }
  
  public class RiBoutonNav extends JLabel {
    
    /**
     * OBJET RiBoutonNav Ce sont des JLabel qui servent de bouton de
     * navigation sur le panel. paramètres : int positionXbouton : position
     * horizontale dans le panel de navigation int positionYbouton :
     * position verticale dans le panel de navigation int largeurBouton :
     * largeur du bouton int hauteurBouton : hauteur du bouton String
     * nomBouton : nom de l'objet bouton String hoverhelpBouton : aide
     * contextuelle du bouton int positionXfonction : position horizontale
     * sur l'écran, où sera réalisée la touche de fonction int
     * positionYfonction : position verticale sur l'écran, où sera réalisée
     * la touche de fonction final int toucheFonction : touche de fonction
     * lancée par le bouton final boolean enCoursBouton : bouton en cours du
     * programme appelant (vrai/faux)
     */
    
    /**
     * Constructeur.
     */
    public RiBoutonNav(int positionX, int positionY, int largeur, int hauteur, String nomBouton, String hoverhelpBouton,
        final boolean enCours, final JButton boutonAactionner) throws Throwable {
      setToolTipText(hoverhelpBouton);
      setName(nomBouton);
      setBounds(positionX, positionY, largeur, hauteur);
      
      // si ce n'est pas le bouton en cours
      if (enCours != true) {
        // état neutre si on passe pas dessus mais curseur "main" pour
        // montrer l'action possible
        setIcon(imgEtat[ETAT_NEUTRE]);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            if (enCours != true) {
              // sur clic on envoi la fonction
              boutonAactionner.doClick();
            }
          }
          
          @Override
          public void mouseEntered(MouseEvent e) {
            if (enCours != true) {
              // on passe sur le bouton : on le met en
              // surbrillance avec l'icone "sélection"
              ((JLabel) e.getSource()).setIcon(imgEtat[ETAT_SELECTION]);
            }
          }
          
          @Override
          public void mouseExited(MouseEvent e) {
            if (enCours != true) {
              // on sort du bouton on revient à l'état neutre
              ((JLabel) e.getSource()).setIcon(imgEtat[ETAT_NEUTRE]);
            }
          }
          
        });
      }
      else {
        // Si c'est le bouton en cours
        // curseur flèche et arrière plan du bouton "en cours"
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        setIcon(imgEtat[ETAT_ENCOURS]);
      }
      
    }
    
  }
  
}
