/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * Fenêtre générique pour les informations satellites
 */
public class FenetreIndependante extends JFrame {
  private JFrame fenetreMere = null;
  private JPanel panelprincipal = null;
  
  /**
   * Constructeur
   */
  public FenetreIndependante(JFrame mere) {
    super();
    initComponents();
    
    fenetreMere = mere;
    // getContentPane().setBackground(Constantes.COULEUR_CONTENU);
    p_Bas.setOpaque(false);
    
    // Bouton par défaut
    getRootPane().setDefaultButton((bt_Fermer));
  }
  
  /**
   * Redéfinit le bouton par défaut
   * @param bouton
   */
  public void setDefautBouton(JButton bouton) {
    if (bouton != null) {
      getRootPane().setDefaultButton((bouton));
    }
  }
  
  /**
   * Initialise le panel principal
   * @param principal
   */
  public void setPanelPrincipal(final JPanel principal, int w, int h, boolean opaque) {
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        if (principal instanceof iRiPanel) {
          ((iRiPanel) principal).dispose();
        }
      }
    });
    setDefautBouton(((iRiPanel) principal).getDefautBouton());
    getContentPane().add(principal, BorderLayout.CENTER);
    panelprincipal = principal;
    // if (!opaque) principal.setBackground(Constantes.COULEUR_CONTENU);
    setSize(w, h);
    setVisible(true);
  }
  
  /**
   * Initialise le panel principal
   * @param principal
   */
  public void setSessionPanel(final JPanel principal, int w, int h, boolean opaque) {
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        // if (principal instanceof iRiPanel)
        // ((iRiPanel)principal).dispose();
      }
    });
    // setDefautBouton(((iRiPanel)principal).getDefautBouton());
    getContentPane().add(principal, BorderLayout.CENTER);
    panelprincipal = principal;
    // if (!opaque) principal.setBackground(Constantes.COULEUR_CONTENU);
    setSize(w, h);
    setVisible(true);
  }
  
  /**
   * Retourne le panel principal
   * @return
   */
  public JPanel getPanelPrincipal() {
    return panelprincipal;
  }
  
  // -- Méthodes privées -----------------------------------------------------
  private void FermerFenetre() {
    dispose();
  }
  
  // -- Evènementiels --------------------------------------------------------
  
  private void bt_FermerActionPerformed(ActionEvent e) {
    FermerFenetre();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_Bas = new JPanel();
    bt_Fermer = new JButton();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== p_Bas ========
    {
      p_Bas.setOpaque(false);
      p_Bas.setName("p_Bas");
      p_Bas.setLayout(new FlowLayout());

      //---- bt_Fermer ----
      bt_Fermer.setText("Fermer");
      bt_Fermer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Fermer.setName("bt_Fermer");
      bt_Fermer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_FermerActionPerformed(e);
        }
      });
      p_Bas.add(bt_Fermer);
    }
    contentPane.add(p_Bas, BorderLayout.SOUTH);
    pack();
    setLocationRelativeTo(null);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_Bas;
  private JButton bt_Fermer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
