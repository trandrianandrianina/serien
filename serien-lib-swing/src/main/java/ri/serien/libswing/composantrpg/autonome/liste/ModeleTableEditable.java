/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome.liste;

import javax.swing.table.DefaultTableModel;

public class ModeleTableEditable extends DefaultTableModel {
  // Variables
  private boolean[] colonnesEditables = null;
  private boolean[][] cellulesEditables = null;
  
  /**
   * Constructeur.
   */
  public ModeleTableEditable(String[] pTitresColonnes, boolean[] pColonnesEditable) {
    super(null, pTitresColonnes);
    colonnesEditables = pColonnesEditable;
  }
  
  @Override
  public boolean isCellEditable(int pLigne, int pColonne) {
    if (colonnesEditables[pColonne]) {
      if (cellulesEditables != null) {
        return cellulesEditables[pLigne][pColonne];
      }
      else {
        return true;
      }
    }
    else {
      return false;
    }
  }
  
  /**
   * Initialise les cellules éditables.
   */
  public void setCellulesEditables(boolean[][] pCellulesEditables) {
    cellulesEditables = pCellulesEditables;
  }
  
  /**
   * Initialise les cellules éditables.
   */
  public void setCellEditable(int pLigne, int pColonne, boolean pEditable) {
    cellulesEditables[pLigne][pColonne] = pEditable;
    fireTableCellUpdated(pLigne, pColonne);
  }
  
  /**
   * Retourne le tableau des colonnes éditables.
   */
  public boolean[] getColonnesEditables() {
    return colonnesEditables;
  }
}
