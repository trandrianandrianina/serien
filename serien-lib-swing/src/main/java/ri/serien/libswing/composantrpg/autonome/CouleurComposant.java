/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composantrpg.autonome;

import java.awt.Color;

public class CouleurComposant {
  // final String FOREGROUND_LIBELLE = "";
  public static final Color FOREGROUND_LIBELLE = new Color(102, 102, 102);
  
}
