/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.outil.clipboard;

import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Permet d'envoyer une image dans le presse-pied.
 * Cette classe est le pendant de StrignSelection pour une image.
 */
public class ImageClipboard implements Transferable {
  private Image image;
  
  /**
   * Constructeur.
   */
  public ImageClipboard(Image pImage) {
    image = pImage;
  }
  
  /**
   * Retourner les types de données supportés.
   */
  @Override
  public DataFlavor[] getTransferDataFlavors() {
    return new DataFlavor[] { DataFlavor.imageFlavor };
  }
  
  /**
   * Indiquer si le type de données fournis en parmaètre est supporté par cette classe.
   * Seules les images sont supportées.
   */
  @Override
  public boolean isDataFlavorSupported(DataFlavor flavor) {
    return DataFlavor.imageFlavor.equals(flavor);
  }
  
  /**
   * Retourner l'objet à transférer (l'image).
   */
  @Override
  public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
    if (!DataFlavor.imageFlavor.equals(flavor)) {
      throw new UnsupportedFlavorException(flavor);
    }
    
    return image;
  }
}
