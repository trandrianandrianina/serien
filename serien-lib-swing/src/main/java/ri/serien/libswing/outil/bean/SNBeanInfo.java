/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.outil.bean;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import java.util.HashMap;
import java.util.Map;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Classe pour faciliter la manipulation des PropertyDescriptor.
 *
 * Toutes les classes SNBeanInfo de nos composants graphiques doivent hériter de cette classe. Cette classe apporte des facilités pour
 * développer les BeanInfo :
 * - elle fournit des méthodes pour modifier facilement les propriétés des Bean (setCategorie, setCategorieAvecNomComposant,
 * setLectureSeule, setVisible, ...).
 * - ces méthodes attendent un EnumProprieteBean pour connaître la propriété à modifier, ce qui évite des erreurs sur les noms.
 * - la méthode bloquerTout() permet de mettre ne lecture seule toutes les propriétés.
 * - la gestion des PropertyDescriptor est effectuée automatiquement par cette classe (pas besoin de les gérer).
 * - la méthode getListePropertyDescriptor() retourne ce qui doit être retournée par getPropertyDescriptors().
 */
public class SNBeanInfo extends SimpleBeanInfo {
  private static final String ATTRIBUT_CATEGORIE = "category";
  private static final String ATTRIBUT_LECTURE_SEULE = "readOnly";
  
  private Map<EnumProprieteBean, PropertyDescriptor> mapPropertyDescriptor = new HashMap<EnumProprieteBean, PropertyDescriptor>();
  private Class beanClass = null;
  
  /**
   * Retourner la description de la propriété (PropertyDescriptor) pour la proriété dont le nom est passé en paramètre.
   * Lors du premier appel, le PropertyDescriptor est créé est stocké dans une Map. Lors des appels suivants, c'est le PropertyDescriptor
   * créé initialement qui est retourné.
   */
  private PropertyDescriptor getPropertyDescriptor(EnumProprieteBean pEnumProprieteBean) {
    // Tester les paramètres
    if (beanClass == null) {
      throw new MessageErreurException("La classe du composant graphique n'est pas renseignée, appeler setBeanClass() pour le faire.");
    }
    if (pEnumProprieteBean == null) {
      throw new MessageErreurException("Le nom de la propriété du composant graphique est invalide.");
    }
    
    // Rechercher la propriété dans la liste
    PropertyDescriptor propertyDescriptor = mapPropertyDescriptor.get(pEnumProprieteBean);
    
    // Tester si on n'a pas trouvé la propriété dans la map
    if (propertyDescriptor == null) {
      // Créer la propriété
      try {
        propertyDescriptor = new PropertyDescriptor(pEnumProprieteBean.getNom(), beanClass);
      }
      catch (IntrospectionException e) {
        throw new MessageErreurException(e, "Erreur lors de la récupération de la propriété '" + pEnumProprieteBean.getNom()
            + "' du composant graphique " + beanClass.getSimpleName());
      }
      
      // Ajouter la propriété à la map
      mapPropertyDescriptor.put(pEnumProprieteBean, propertyDescriptor);
    }
    
    // Retourner la propriété
    return propertyDescriptor;
  }
  
  /**
   * Retourner la liste des descriptions de propriétés que la méthode getPropertyDescriptors() doit renvoyer.
   * Cette méthode doit être appelée par les méthodes getPropertyDescriptors() des classes enfants.
   */
  @Override
  public PropertyDescriptor[] getPropertyDescriptors() {
    PropertyDescriptor[] liste = new PropertyDescriptor[mapPropertyDescriptor.size()];
    int index = 0;
    for (Map.Entry<EnumProprieteBean, PropertyDescriptor> mapEntry : mapPropertyDescriptor.entrySet()) {
      liste[index++] = mapEntry.getValue();
    }
    return liste;
  }
  
  /**
   * Retourner le BeanInfo de la classe parente s'il y a un.
   */
  @Override
  public BeanInfo[] getAdditionalBeanInfo() {
    try {
      return new BeanInfo[] { Introspector.getBeanInfo(getBeanClass().getSuperclass()) };
    }
    catch (IntrospectionException e) {
      return null;
    }
  }
  
  /**
   * Mettre en lecture seule toutes les propriétes.
   * Cette méthode parcourt toutes les propriétés disponibles dans EnumProprieteBean et recherche si elles existante dans le JavaBean
   * à modifier. Si la propriété est trouvée, elle est mise en lecture seule. L'idée est de tout bloquer par défaut et d'autoriser
   * explictement, composant par composant, les propriétés qui sont modifiables.
   */
  public void bloquerTout() {
    for (EnumProprieteBean enumProprieteBean : EnumProprieteBean.values()) {
      // Vérifier que le nom de la propriété est viable
      if (enumProprieteBean != null) {
        try {
          // Récupérer la propriété du JavaBean
          PropertyDescriptor propertyDescriptor = getPropertyDescriptor(enumProprieteBean);
          if (propertyDescriptor != null) {
            // Mettre la propriété en lecture seule si on l'a trouvé
            propertyDescriptor.setValue(ATTRIBUT_LECTURE_SEULE, Boolean.TRUE);
          }
        }
        catch (MessageErreurException e) {
          // Ne pas faire d'erreur
        }
      }
    }
  }
  
  /**
   * Affecter une catégorie à la propriété du composant graphique.
   * La propriété est désignée par un EnumProprieteBean.
   */
  public void setCategorie(EnumProprieteBean pEnumBeanProperty, String pCategorie) {
    // Tester les paramètres
    if (beanClass == null) {
      throw new MessageErreurException("La classe du composant graphique n'est pas renseignée, appeler setBeanClass() pour le faire.");
    }
    if (pEnumBeanProperty == null) {
      throw new MessageErreurException("Le nom de la propriété du composant graphique est invalide.");
    }
    if (pCategorie == null || pCategorie.isEmpty()) {
      throw new MessageErreurException("La catégorie est invalide pour le composant graphique : " + beanClass.getSimpleName());
    }
    
    // Récupérer la propriété
    PropertyDescriptor propertyDescriptor = getPropertyDescriptor(pEnumBeanProperty);
    
    // Renseigner l'attribut lecture seule
    if (propertyDescriptor != null) {
      propertyDescriptor.setValue(ATTRIBUT_CATEGORIE, pCategorie);
    }
  }
  
  /**
   * Affecter une catégorie à la propriété du composant graphique.
   * La propriété est désignée par un EnumProprieteBean.
   * Le nom de la catégorie est le nom de la classe du composant graphique. Avec cela par exemple, les propriétés de SNBouton son rangées
   * dans la catégorie "SNBouton".
   */
  public void setCategorieAvecNomComposant(EnumProprieteBean pEnumBeanProperty) {
    // Tester les paramètres
    if (beanClass == null) {
      throw new MessageErreurException("La classe du composant graphique n'est pas renseignée, appeler setBeanClass() pour le faire.");
    }
    if (pEnumBeanProperty == null) {
      throw new MessageErreurException("Le nom de la propriété du composant graphique est invalide.");
    }
    
    // Modifier la catégorie de la propriété
    setCategorie(pEnumBeanProperty, beanClass.getSimpleName());
  }
  
  /**
   * Mettre une propriété en lecture seule.
   * La propriété est désignée par un EnumProprieteBean.
   */
  public void setLectureSeule(EnumProprieteBean pEnumBeanProperty, boolean pLectureSeule) {
    // Tester les paramètres
    if (pEnumBeanProperty == null) {
      throw new MessageErreurException("Le nom de la propriété du composant graphique est invalide.");
    }
    
    // Récupérer la propriété
    PropertyDescriptor propertyDescriptor = getPropertyDescriptor(pEnumBeanProperty);
    
    // Renseigner l'attribut lecture seule
    if (propertyDescriptor != null) {
      propertyDescriptor.setValue(ATTRIBUT_LECTURE_SEULE, pLectureSeule);
    }
  }
  
  /**
   * Cacher une propriété du composant graphique.
   * La propriété est désignée par un EnumProprieteBean.
   */
  public void setVisible(EnumProprieteBean pEnumBeanProperty, boolean pVisible) {
    // Tester les paramètres
    if (pEnumBeanProperty == null) {
      throw new MessageErreurException("Le nom de la propriété du composant graphique est invalide.");
    }
    
    // Récupérer la propriété
    PropertyDescriptor propertyDescriptor = getPropertyDescriptor(pEnumBeanProperty);
    
    // Renseigner l'attribut lecture seule
    if (propertyDescriptor != null) {
      propertyDescriptor.setHidden(!pVisible);
    }
  }
  
  /**
   * Obtenir la classe du composant graphique "Bean" dont cette classe est le BeanInfo.
   */
  public Class getBeanClass() {
    return beanClass;
  }
  
  /**
   * Renseigner la classe du composant graphique "Bean" dont cette classe est le BeanInfo.
   */
  public void setBeanClass(Class pBeanClass) {
    beanClass = pBeanClass;
  }
}
