/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.outil.clipboard;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import ri.serien.libcommun.outils.Trace;

/**
 * Gestion du clipboard.
 *
 * Cette classe centralise les méthodes permettant de copier quelquechose dans le presse-papier Windows et de le récupérer. Deux types
 * d'objets sont gérés : les chaînes de caractères et les images.
 */
public class Clipboard {
  /**
   * Envoyer un texte dans le presse-papier.
   * @param pTexte Le texte à copier dans le presse-papier.
   */
  public static void envoyerTexte(String pTexte) {
    // Vérifier les paramètres
    if (pTexte == null) {
      return;
    }

    // Créer l'objet qui est envoyé au clipboard
    StringSelection transferable = new StringSelection(pTexte);

    // Envoyer l'objet dans le clipboard
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(transferable, null);
  }

  /**
   * Récupérer un texte à partir du presse-papier.
   * @return Texte récupéré ou null si rien.
   */
  public static String recupererTexte() {
    // Récupérer le contenu du clipboard
    Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
    if (transferable == null) {
      return null;
    }

    // Tester s'il contient du texte
    boolean contientTexte = transferable.isDataFlavorSupported(DataFlavor.stringFlavor);
    if (!contientTexte) {
      return null;
    }

    // Récupérer le texte
    try {
      return (String) transferable.getTransferData(DataFlavor.stringFlavor);
    }
    catch (UnsupportedFlavorException e) {
      Trace.erreur(e, "Erreur lors de la récupération d'une chaîne de caractères à partir du clipboard");
    }
    catch (IOException e) {
      Trace.erreur(e, "Erreur lors de la récupération d'une chaîne de caractères à partir du clipboard");
    }

    return null;
  }

  /**
   * Envoyer une image dans le presse papier.
   * @param pImage L'image à copier dans le presse-papier.
   */
  public static void envoyerImage(Image pImage) {
    // Vérifier les paramètres
    if (pImage == null) {
      return;
    }

    // Créer l'objet qui est envoyé au clipboard
    ImageClipboard transferable = new ImageClipboard(pImage);

    // Envoyer l'objet dans le clipboard
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(transferable, null);
  }

  /**
   * Récupérer une image à partir du presse-papier.
   * @return Image récupérée ou null si rien.
   */
  public static Image recupererImage() {
    // Récupérer le contenu du clipboard
    Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
    if (transferable == null) {
      return null;
    }

    // Tester s'il contient du texte
    boolean contientImage = transferable.isDataFlavorSupported(DataFlavor.imageFlavor);
    if (!contientImage) {
      return null;
    }
    
    // Récupérer l'image
    try {
      return (Image) transferable.getTransferData(DataFlavor.imageFlavor);
    }
    catch (UnsupportedFlavorException e) {
      Trace.erreur(e, "Erreur lors de la récupération d'une image à partir du clipboard");
    }
    catch (IOException e) {
      Trace.erreur(e, "Erreur lors de la récupération d'une image à partir du clipboard");
    }
    return null;
  }
}
