/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.outil.font;

import java.awt.Font;

/**
 * Chargement des polices personnalisées.
 * Site: http://ftp.gnome.org/pub/GNOME/sources/ttf-bitstream-vera/1.10/
 */
public class CustomFont {
  /**
   * Charge des polices personnalisées.
   */
  public static Font loadFont(String path, float size) {
    try {
      Font font = Font.createFont(Font.TRUETYPE_FONT, CustomFont.class.getClassLoader().getResourceAsStream(path));
      return font.deriveFont(size);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return new Font("Courier New", Font.PLAIN, (int) size);
  }
}
