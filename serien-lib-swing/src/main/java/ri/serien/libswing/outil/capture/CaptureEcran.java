/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.outil.capture;

import java.awt.AWTException;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FiltreFichierParExtension;
import ri.serien.libswing.outil.clipboard.Clipboard;

/**
 * Permet d'effectuer une capture graphique de l'écran.
 * La classe permet de capture une eone de l'écran, une fenêtre ou l'écran en entier.
 */
public class CaptureEcran {
  private static final String EXTENSION = "jpg";

  // Variables
  private Robot robot = null;
  private BufferedImage image = null;
  private File dossierSauvegarde = null;
  private int compteurImage = 0;
  private JFrame fenetre = null;
  
  /**
   * Constructeur.
   */
  public CaptureEcran(JFrame pFrame) {
    // Mémoriser la fenêtre associée à l'outil de capture
    fenetre = pFrame;
    
    // Initialiser le robot
    try {
      robot = new Robot();
    }
    catch (AWTException e) {
      Trace.erreur(e, "Erreur lors de l'initialisation du composant de capture d'écrans");
    }
  }
  
  /**
   * Capture une zone de l'écran définie par des coordonnées.
   * @param pRectangleCapture Capture de l'écran.
   * @return Image capturée.
   */
  public BufferedImage capturerZone(Rectangle pRectangleCapture) {
    // Vérifier les paramètres
    if (robot == null || pRectangleCapture == null) {
      return null;
    }
    
    // Capturer l'image de la zone
    image = robot.createScreenCapture(pRectangleCapture);
    return image;
  }
  
  /**
   * Capture la fenêtre associée à l'outil de capture.
   * @return Image capturée.
   */
  public BufferedImage capturerFenetre() {
    // Récupérer les dimensions des bords de la fenêtre
    Insets insets = fenetre.getInsets();
    
    // Calculer les coordonnées de la zone à capturer
    // Le bord supérieur n'est pas retiré car il dcontient la barre de titre Windows et on souhaite la conserver
    int x = fenetre.getX() + insets.left;
    int y = fenetre.getY();
    int largeur = fenetre.getWidth() - insets.left - insets.right;
    if (largeur < 0) {
      largeur = fenetre.getWidth();
    }
    int hauteur = fenetre.getHeight() - insets.bottom;
    if (hauteur < 0) {
      hauteur = fenetre.getHeight();
    }
    
    // Capturer la zone
    return capturerZone(new Rectangle(x, y, largeur, hauteur));
  }
  
  /**
   * Capturer l'écran entier.
   * @return Image capturée.
   */
  public BufferedImage capturerEcran() {
    // Calculer les coordonnées de la zone à capturer
    Rectangle rectangleCapture = new Rectangle(java.awt.Toolkit.getDefaultToolkit().getScreenSize());
    
    // Capturer la zone
    return capturerZone(rectangleCapture);
  }
  
  /**
   * Enregistre l'image capturée à l'aide d'une boite de dialogue.
   */
  public void sauverCapture() {
    // Instancier la boîte de dialogue
    JFileChooser choixFichier = new JFileChooser();
    choixFichier.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { "gif" }, "Fichier image (*.gif)"));
    choixFichier.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { "png" }, "Fichier image (*.png)"));
    choixFichier.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { "jpg" }, "Fichier image (*.jpg)"));
    
    // Se placer dans le dernier dossier courant s'il y en a un
    if (dossierSauvegarde != null) {
      choixFichier.setCurrentDirectory(dossierSauvegarde);
    }
    
    // Construire le nom du fichier à sauvegarder
    File fichier = new File("image" + (compteurImage++ < 10 ? "0" + compteurImage : compteurImage) + "." + EXTENSION);
    choixFichier.setSelectedFile(fichier);
    
    // Afficher la boîte de dialogue
    if (choixFichier.showSaveDialog(fenetre) == JFileChooser.APPROVE_OPTION) {
      try {
        ImageIO.write(image, EXTENSION, choixFichier.getSelectedFile());
      }
      catch (IOException e) {
        throw new MessageErreurException(e, "Erreur lors de la sauvegarde de la capture d'écran");
      }
    }
    
    // Mémoriser le dossier sélectionné pour la sauvegarde
    dossierSauvegarde = choixFichier.getCurrentDirectory();
  }
  
  /**
   * Envoyer la capture d'écran dans le presse papier.
   */
  public void envoyerCaptureVersClipboard() {
    if (image != null) {
      Clipboard.envoyerImage(image);
    }
  }
}
