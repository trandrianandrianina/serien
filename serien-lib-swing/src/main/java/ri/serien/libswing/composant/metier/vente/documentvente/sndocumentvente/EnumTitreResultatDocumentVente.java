/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.sndocumentvente;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Titre de message par rapport au résultat de recherche de document de ventes.
 */
public enum EnumTitreResultatDocumentVente {
  DOCUMENT_TROUVE(1, "Documents correspondant à votre recherche (%s)"),
  NUMERO_INFERIEUR_DEBUT(2, "Le numéro du document saisi est inférieur au numéro de document de ventes de début."),
  NUMERO_SUPERIEUR_FIN(3, "Le numéro du document saisi est supérieur au numéro de document de ventes de fin."),
  FACTURE_INFERIEUR_DEBUT(4, "Le numéro de facture saisi est inférieur au numéro de facture de début"),
  FACTURE_SUPERIEUR_FIN(5, "Le numéro de facture saisi est supérieur au numéro de facture de fin.");
  
  private final int code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTitreResultatDocumentVente(int pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public int getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return Integer.toString(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTitreResultatDocumentVente valueOfByCode(int pCode) {
    for (EnumTitreResultatDocumentVente value : values()) {
      if (pCode == value.getCode()) {
        return value;
      }
    }
    throw new MessageErreurException("Le type mode de délivrement est invalide : " + pCode);
  }
  
}
