/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.chantier.snchantier;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.List;

import javax.swing.AbstractAction;

import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.chantier.CritereChantier;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un chantier.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un chantier dans le logiciel. Il gère l'affichage d'un chantier et la
 * sélection de l'un d'entre eux.
 * Il comprend un TextField et un bouton permettant de sélectionner un chantier dans une boîte de dialogue.
 *
 * Utilisation :
 * - Ajouter un composant SNChantier à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé. Les informations nécessaires sont la session et l'établissement.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le VueOngletLivraison.
 */
public class SNChantier extends SNComposantAvecEtablissement {
  private SNTexte tfTexteRecherche;
  private SNBoutonRecherche btnDialogueRechercheChantier;
  private Chantier chantierSelectionne = null;
  private boolean fenetreChantierAffichee = false;
  private boolean rafraichir = false;
  private String texteRecherchePrecedente = null;
  private CritereChantier critereChantier = new CritereChantier();
  private int modeComposant = ModeleChantier.MODE_COMPOSANT_SELECTION;

  /**
   * Constructeur par défaut.
   */
  public SNChantier() {
    super();
    setName("SNChantier");

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    // Définir la largeur par défaut si la largeur définie dans JFormDesigner lui est inférieure ou égale
    if (SNCharteGraphique.TAILLE_COMPOSANT_SELECTION.width >= getPreferredSize().width) {
      setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    }
    else {
      setPreferredSize(new Dimension(getWidth(), SNCharteGraphique.TAILLE_COMPOSANT_SELECTION.height));
    }

    setLayout(new GridBagLayout());

    // Texte de recherche
    tfTexteRecherche = new SNTexte();
    tfTexteRecherche.setName("tfChantier");
    tfTexteRecherche.setEditable(true);
    tfTexteRecherche.setToolTipText(VueChantier.RECHERCHE_GENERIQUE_TOOLTIP);
    add(tfTexteRecherche,
        new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

    // Bouton loupe
    btnDialogueRechercheChantier = new SNBoutonRecherche();
    btnDialogueRechercheChantier.setToolTipText("Recherche de chantier");
    btnDialogueRechercheChantier.setName("btnChantier");
    add(btnDialogueRechercheChantier, new GridBagConstraints());

    // Focus listener pour lancer la recherche quand on quitte la zone de saisie
    tfTexteRecherche.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        tfTexteRechercheFocusLost(e);
      }

      @Override
      public void focusGained(FocusEvent e) {
        tfTexteRechercheFocusGained(e);
      }
    });

    // ActionListener pour capturer l'utilisation de la touche entrée
    tfTexteRecherche.addActionListener(new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tfTexteRechercheActionPerformed(e);
      }
    });

    // Etre notifier lorsque le bouton est cliqué
    btnDialogueRechercheChantier.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnDialogueRechercheClientActionPerformed(e);
      }
    });
  }

  /**
   * Activer ou désactiver le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    tfTexteRecherche.setEnabled(pEnabled);
    btnDialogueRechercheChantier.setEnabled(pEnabled);
  }

  /**
   * Charger la liste des clients.
   * Cette méthode ne fait rien dans ce composant car la liste des clients est chargée à la demande en fonction de la saisie. Elle est
   * conservée par souci de comptabilité avec les autres composants graphiques.
   */
  public void charger(boolean pForcerRafraichissement) {
    // Tester s'il faut rafraîchir la liste
    if (!rafraichir && !pForcerRafraichissement) {
      return;
    }
    rafraichir = false;
  }

  /**
   * Initialiser les valeurs du composant pour se retrouver dans la même situation que si aucun chantier n'est sélectionné.
   */
  public void initialiserRecherche() {
    // Effacer les critères de recherche
    critereChantier.initialiser();

    // Ne sélectionner aucun client
    setSelection(null);
  }

  /**
   * Lancer une recherche parmi les chantiers
   */
  private void lancerRecherche(boolean forcerAffichageDialogue) {
    // Vérifier la session
    if (getSession() == null) {
      throw new MessageErreurException("La session du composant de sélection de chantier est invalide.");
    }

    // Ne rien faire si le texte à rechercher est vide
    if (tfTexteRecherche.getText().trim().isEmpty()) {
      setSelection(null);
      if (forcerAffichageDialogue) {
        afficherDialogueRechercheChantier(null);
      }
      return;
    }

    critereChantier.setIdEtablissement(getIdEtablissement());

    // Ne rien faire si le texte à rechercher n'a pas changé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && tfTexteRecherche.getText().equals(texteRecherchePrecedente)) {
      return;
    }
    else {
      critereChantier.setTexteRechercheGenerique(tfTexteRecherche.getText());
    }

    // Comportement de la recherche suivant le mode du composant
    switch (modeComposant) {
      case ModeleChantier.MODE_COMPOSANT_SELECTION:
        critereChantier.setAvecBloque(false);
        break;

      case ModeleChantier.MODE_COMPOSANT_SELECTION_AVEC_BLOQUES:
        critereChantier.setAvecBloque(true);
        break;

      case ModeleChantier.MODE_FILTRE_RECHERCHE:
        critereChantier.setStatut(null);
        critereChantier.setAvecBloque(true);
        break;
      
      default:
        break;
    }

    // Lancer une recherche de chantier
    List<IdChantier> listeIdChantier = ManagerServiceClient.chargerListeIdChantier(getIdSession(), critereChantier);

    // Sélectionner automatiquement le seul client trouvé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && listeIdChantier.size() == 1) {
      setSelection(ManagerServiceClient.chargerChantier(getIdSession(), listeIdChantier.get(0)));
    }
    // Afficher la boîte de dialogue si : affichage forcé de la boîte de dialogue, aucun ou plusieurs clients trouvés
    else {
      afficherDialogueRechercheChantier(listeIdChantier);
    }

    // Sélectionner automatiquement l'ensemble du champ afin que la prochaine saisie vienne en remplacement
    tfTexteRecherche.selectAll();
  }

  /**
   * Affiche la fenêtre de recherche des chantiers
   */
  private void afficherDialogueRechercheChantier(List<IdChantier> pListeIdChantier) {
    // Sécurité pour éviter que la boîte de dialogue ne s'affiche deux fois
    if (fenetreChantierAffichee) {
      return;
    }
    fenetreChantierAffichee = true;

    // Afficher la boîte de dialogue de recherche d'un client
    critereChantier.setIdEtablissement(getIdEtablissement());
    critereChantier.setTexteRechercheGenerique(tfTexteRecherche.getText());
    ModeleChantier modele = new ModeleChantier(getSession());
    modele.setModeComposant(modeComposant);
    modele.setCriteresChantier(critereChantier);
    modele.setListeIdChantier(pListeIdChantier);
    VueChantier vue = new VueChantier(modele);
    vue.afficher();

    // Sélectionner le client retourné par la boîte de dialogue (ou aucun si null)
    if (modele.isSortieAvecValidation()) {
      Chantier chantier = modele.getChantierSelectionne();
      setSelection(chantier);
    }
    // La boîte de dialogue est annulé, on remet le client sélectionné précédement
    else {
      rafraichirTexteRecherche();
    }
    fenetreChantierAffichee = false;
  }

  /**
   * Rafraîchir le texte de la recherche avec le client sélectionné.
   */
  private void rafraichirTexteRecherche() {
    // Mettre à jour le composant
    if (chantierSelectionne != null) {
      tfTexteRecherche.setText(chantierSelectionne.toString());
    }
    else {
      tfTexteRecherche.setText("");
    }

    // Mémoriser le dernier texte pour ne pas relancer une recherche inutile
    texteRecherchePrecedente = tfTexteRecherche.getText();
  }

  // Méthodes évènementielles

  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      tfTexteRecherche.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfTexteRechercheFocusLost(FocusEvent e) {
    try {
      lancerRecherche(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfTexteRechercheActionPerformed(ActionEvent e) {
    try {
      lancerRecherche(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void btnDialogueRechercheClientActionPerformed(ActionEvent e) {
    try {
      lancerRecherche(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  // Accesseurs

  /**
   * Chantier sélectionné.
   */
  public Chantier getSelection() {
    return chantierSelectionne;
  }

  /**
   * Modifier le chantier sélectionné.
   */
  public void setSelection(Chantier pChantierSelectionne) {
    // Tester si la sélection a changé
    boolean bSelectionModifiee = !Constantes.equals(chantierSelectionne, pChantierSelectionne);

    // Modifier le client sélectionné
    chantierSelectionne = pChantierSelectionne;

    // Effacer les critères de recherche si aucun client n'est sélectionné
    if (chantierSelectionne == null) {
      critereChantier.initialiser();
    }

    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();

    // Notifier uniquement si la sélection a changé
    if (bSelectionModifiee) {
      fireValueChanged();
    }
  }

  /**
   * Sélectionner chantier à partir des champs RPG.
   */
  public boolean setSelectionParChampRPG(Lexical pLexical, String pChampNumeroChantier) {
    if (pChampNumeroChantier == null) {
      throw new MessageErreurException("Impossible de sélectionner le chantier car son identifiant est invalide.");
    }
    
    // Convertir le numéro
    Integer numeroChantier = 0;
    try {
      numeroChantier = Integer.parseInt(pLexical.HostFieldGetData(pChampNumeroChantier).trim());
    }
    catch (NumberFormatException e) {
      // RAS
    }

    // Construire l'id fournisseur
    IdChantier idChantier = null;
    if (numeroChantier != 0) {
      idChantier = IdChantier.getInstance(getIdEtablissement(), numeroChantier);
    }

    setSelectionParId(idChantier);
    return true;
  }

  /**
   * Renseigner les champs RPG correspondant au chantier sélectionné.
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampNumeroChantier) {
    Chantier chantier = getSelection();
    if (chantier != null) {
      pLexical.HostFieldPutData(pChampNumeroChantier, 0, chantier.getId().getNumero().toString());
    }
    else {
      pLexical.HostFieldPutData(pChampNumeroChantier, 0, "");
    }
  }
  
  /**
   * Sélectionner le chantier correspondant à l'identifiant passé en paramètre.
   */
  public void setSelectionParId(IdChantier pIdChantier) {
    if (pIdChantier == null) {
      setSelection(null);
      return;
    }
    // Si on recherche le chantier déjà sélectionné, on sort
    if (getSelection() != null && Constantes.equals(pIdChantier, getSelection().getId())) {
      return;
    }
    
    critereChantier.setIdEtablissement(pIdChantier.getIdEtablissement());
    critereChantier.setNumeroChantier(pIdChantier.getNumero());
    // Lancer une recherche de chantier
    List<IdChantier> listeIdChantier = ManagerServiceClient.chargerListeIdChantier(getIdSession(), critereChantier);
    // Sélectionner automatiquement le seul client trouvé (sauf si la boîte de dialogue est forcée)
    setSelection(ManagerServiceClient.chargerChantier(getIdSession(), listeIdChantier.get(0)));
  }

  /**
   * Modifier le client pour lequel on sélectionne le chantier
   */
  public void setIdClient(IdClient pIdClient) {
    if (Constantes.equals(critereChantier.getIdClient(), pIdClient)) {
      return;
    }
    critereChantier.setIdClient(pIdClient);
    rafraichir = true;
  }

  /**
   * Mode d'utilisation du composant
   */
  public int getModeComposant() {
    return modeComposant;
  }

  /**
   * Modifier le mode d'utilisation du composant
   */
  public void setModeComposant(int pModeComposant) {
    modeComposant = pModeComposant;
  }
}
