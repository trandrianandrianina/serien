/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import java.beans.BeanDescriptor;
import java.beans.PropertyDescriptor;

import ri.serien.libswing.outil.bean.EnumProprieteBean;
import ri.serien.libswing.outil.bean.SNBeanInfo;

/**
 * BeanInfo pour SNBouton.
 * Gère le composant graphique sous JFormDesigner.
 */
public class SNBoutonLegerBeanInfo extends SNBeanInfo {

  {
    // Renseigner la classe JavaBean associé à ce BeanInfo
    setBeanClass(SNBoutonLeger.class);
  }

  @Override
  public BeanDescriptor getBeanDescriptor() {
    // Indiquer si le composant est un container ou non (pour ne pas avoir à choisir lors de l'import sous JFormDesigner)
    BeanDescriptor beanDescriptor = new BeanDescriptor(getBeanClass());
    beanDescriptor.setValue("isContainer", Boolean.FALSE);
    return beanDescriptor;
  }

  @Override
  public PropertyDescriptor[] getPropertyDescriptors() {
    // Mettre en lecture seule toutes les propriétes
    bloquerTout();

    // Activer les propriétés utiles
    setLectureSeule(EnumProprieteBean.TEXT, false);
    setCategorieAvecNomComposant(EnumProprieteBean.TEXT);

    setLectureSeule(EnumProprieteBean.ICON, false);
    setCategorieAvecNomComposant(EnumProprieteBean.ICON);

    setLectureSeule(EnumProprieteBean.MAXIMUM_SIZE, false);
    setCategorieAvecNomComposant(EnumProprieteBean.MAXIMUM_SIZE);

    setLectureSeule(EnumProprieteBean.MINIMUM_SIZE, false);
    setCategorieAvecNomComposant(EnumProprieteBean.MINIMUM_SIZE);

    setLectureSeule(EnumProprieteBean.PREFERRED_SIZE, false);
    setCategorieAvecNomComposant(EnumProprieteBean.PREFERRED_SIZE);
    
    // Retourner la liste des propriétés configurées
    return super.getPropertyDescriptors();
  }
}
