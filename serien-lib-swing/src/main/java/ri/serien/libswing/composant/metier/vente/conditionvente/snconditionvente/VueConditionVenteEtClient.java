/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.conditionvente.snconditionvente;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Boite de dialogue de sélection d'une condition de vente ou d'une condition client
 */
public class VueConditionVenteEtClient extends AbstractVueDialogue<ModeleConditionVenteEtClient> {
  // Constantes
  private static final String BOUTON_SUPPRIMER = "Déselectionner";
  
  // Variables

  /**
   * Constructeur
   */
  public VueConditionVenteEtClient(ModeleConditionVenteEtClient pModele) {
    super(pModele);
    setBackground(SNCharteGraphique.COULEUR_FOND);
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setTitle("Sélection d'un rattachement client");

    // Composant CNV
    snConditionVente.setSession(getModele().getSession());
    snConditionVente.setIdEtablissement(getModele().getIdEtablissement());
    snConditionVente.charger(false);

    // Composant client
    snClientPrincipal.setSession(getModele().getSession());
    snClientPrincipal.setIdEtablissement(getModele().getIdEtablissement());
    snClientPrincipal.charger(false);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER, 's', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Rafraichissement de l'écran
   */
  @Override
  public void rafraichir() {
    rafraichirRadioBoutons();
    rafraichirConditionVente();
    rafraichirClient();
    rafraichirBoutons();
  }
  
  // -- Méthodes privées

  /**
   * Rafraichir l'affichage des radios boutons
   */
  private void rafraichirRadioBoutons() {
    switch (getModele().getModeSelection()) {
      case ModeleConditionVenteEtClient.MODE_CNV:
        rbCNV.setSelected(true);
        break;
      
      case ModeleConditionVenteEtClient.MODE_CLIENT:
        rbClient.setSelected(true);
        break;

      default:
        rbCNV.setSelected(false);
        rbClient.setSelected(false);
        break;
    }
  }

  /**
   * Rafraichir l'affichage du composant de sélection des CNV
   */
  private void rafraichirConditionVente() {
    if (getModele().getModeSelection() == ModeleConditionVenteEtClient.MODE_CNV) {
      snConditionVente.setEnabled(true);
      snConditionVente.setIdSelection(getModele().getIdConditionSelectionnee());
    }
    else {
      snConditionVente.setEnabled(false);
    }
  }

  /**
   * Rafraichir l'affichage du composant de sélection d'un client
   */
  private void rafraichirClient() {
    if (getModele().getModeSelection() == ModeleConditionVenteEtClient.MODE_CLIENT) {
      snClientPrincipal.setEnabled(true);
      if (getModele().getIdClientSelectionne() != null) {
        snClientPrincipal.forcerRecherche(getModele().getIdClientSelectionne().getTexte());
        snClientPrincipal.setSelectionParId(getModele().getIdClientSelectionne());
      }
      else {
        snClientPrincipal.setSelection(null);
      }
    }
    else {
      snClientPrincipal.setEnabled(false);
    }
  }

  /**
   * Rafraichir l'affichage des boutons
   */
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_SUPPRIMER,
        isDonneesChargees() && (getModele().getIdClientSelectionne() != null || getModele().getIdConditionSelectionnee() != null));
    snBarreBouton.activerBouton(EnumBouton.VALIDER,
        isDonneesChargees() && (getModele().getIdClientSelectionne() != null || getModele().getIdConditionSelectionnee() != null));
  }

  // -- Méthodes interractives
  
  /**
   * Barre de boutons
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER)) {
        getModele().supprimerCondition();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Radio bouton cnv
   */
  private void rbCNVItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierMode(ModeleConditionVenteEtClient.MODE_CNV);
        snClientPrincipal.setSelection(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Radio bouton condition client
   */
  private void rbClientItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierMode(ModeleConditionVenteEtClient.MODE_CLIENT);
        snConditionVente.setSelection(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Evènement sur notification de sélection d'une CNV
   */
  private void snConditionVenteValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierIdConditionSelectionnee(snConditionVente.getIdSelection());
        snClientPrincipal.setSelection(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Evènement sur notification de sélection d'un client
   */
  private void snClientPrincipalValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierIdClientSelectionne(snClientPrincipal.getIdSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    rbCNV = new SNRadioButton();
    snConditionVente = new SNConditionVente();
    rbClient = new SNRadioButton();
    snClientPrincipal = new SNClientPrincipal();
    buttonGroup1 = new ButtonGroup();

    // ======== this ========
    setTitle("S\u00e9lection d'un rattachement client");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(700, 200));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());

      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);

      // ======== sNPanelContenu1 ========
      {
        sNPanelContenu1.setName("sNPanelContenu1");
        sNPanelContenu1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };

        // ---- rbCNV ----
        rbCNV.setText("Param\u00e8tre CN");
        rbCNV.setMinimumSize(new Dimension(200, 30));
        rbCNV.setMaximumSize(new Dimension(200, 19));
        rbCNV.setPreferredSize(new Dimension(200, 30));
        rbCNV.setName("rbCNV");
        rbCNV.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbCNVItemStateChanged(e);
          }
        });
        sNPanelContenu1.add(rbCNV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        // ---- snConditionVente ----
        snConditionVente.setName("snConditionVente");
        snConditionVente.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snConditionVenteValueChanged(e);
          }
        });
        sNPanelContenu1.add(snConditionVente, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

        // ---- rbClient ----
        rbClient.setText("Client");
        rbClient.setMaximumSize(new Dimension(200, 30));
        rbClient.setMinimumSize(new Dimension(200, 30));
        rbClient.setPreferredSize(new Dimension(200, 30));
        rbClient.setName("rbClient");
        rbClient.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbClientItemStateChanged(e);
          }
        });
        sNPanelContenu1.add(rbClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

        // ---- snClientPrincipal ----
        snClientPrincipal.setName("snClientPrincipal");
        snClientPrincipal.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snClientPrincipalValueChanged(e);
          }
        });
        sNPanelContenu1.add(snClientPrincipal, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(sNPanelContenu1, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);

    // ---- buttonGroup1 ----
    buttonGroup1.add(rbCNV);
    buttonGroup1.add(rbClient);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelContenu1;
  private SNRadioButton rbCNV;
  private SNConditionVente snConditionVente;
  private SNRadioButton rbClient;
  private SNClientPrincipal snClientPrincipal;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
