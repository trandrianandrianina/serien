/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import java.awt.Cursor;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton au design léger pouvant être affiché dans un écran.
 *
 * Ce bouton est une alternative à SNBouton afin de proposer un affichage plus léger visuellement. Le composant SNBouton affiche des
 * boutons dans des couleurs assez vives pour attirer l'oeil mais cela peut être trop pour un bouton en plein milieu de l'écran.
 * La couleur de fond de ce bouton est jaune pâle, donc légèrement plus claire que la couleur de fond utilisée pour les écrans.
 * Sa hauteur correspond également à la hauteur stanard des composants (30 pixels) alors que SNBouton est plus haut (50 pixels).
 *
 * Ce bouton peut également être utilsié pour afficher des images. Ce type d'utilisation de ce type de bouton est à limiter au possible.
 * En effet, comme Résolution Informatique ne dispose pas de quelqu'un pour faire le design graphique, il n'est pas facile de trouver des
 * images qui illustrent sans ambiguité possible l'action effectuée par le bouton. La plupart du temps, les images sont puisées sur
 * internet mais cela pose le problème de la cohérence graphique. Notre logiciel se devant d'être le plus clair possible, il est
 * préférable d'utiliser des boutons avec du texte qui résume l'action du bouton. Cela reste toutefois utile dans les cas particuliers,
 * par exemple pour illustrer les modes de paiement lors d'un règlement, carte bancaire, chèque, espèce...
 *
 * Caractéristiques :
 * - Hauteur standard par défaut.
 * - Pas de marges.
 * - Couleur de fond jaune claire.
 * - Police standard pour les boutons.
 * - Curseur en forme de main lorsque situé dessus.
 */
public class SNBoutonLeger extends JButton {
  /**
   * Constructeur.
   */
  public SNBoutonLeger() {
    // Définir la taille bu bouton
    setMinimumSize(SNCharteGraphique.TAILLE_BOUTON_LEGER);
    setPreferredSize(SNCharteGraphique.TAILLE_BOUTON_LEGER);
    setMaximumSize(SNCharteGraphique.TAILLE_BOUTON_LEGER);

    // Supprimer la marge
    setMargin(null);

    // Fixer la couleur de fond
    setBackground(SNCharteGraphique.COULEUR_BOUTON_IMAGE);

    // Utiliser la police standard pour les boutons
    setFont(SNCharteGraphique.POLICE_BOUTON);

    // Centrer le texte horizontalement et verticalement
    setHorizontalAlignment(SwingConstants.CENTER);
    setVerticalAlignment(SwingConstants.CENTER);

    // Afficher le curseur en forme de main sur ce bouton
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
  }
}
