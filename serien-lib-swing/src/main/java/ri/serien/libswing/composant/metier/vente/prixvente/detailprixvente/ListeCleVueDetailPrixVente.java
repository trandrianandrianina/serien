/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente;

import java.util.ArrayList;

import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;

/**
 * Liste de clés pour les vues de la boîte de dialogue qui affiche le détail du calcul d'un prix de vente.
 *
 * La méthode genererListeCleVue() générère la liste des vues à afficher en fonction de CalculPrixVente.
 */
public class ListeCleVueDetailPrixVente extends ArrayList<CleVueDetailPrixVente> {
  /**
   * Générer la liste des clés pour les vues.
   */
  public void genererListeCleVue(CalculPrixVente pCalculPrixVente) {
    int compteurIndex = 0;

    // Effacer la liste précédente
    clear();

    // Ajouter une vue synthèse dans tous les cas
    add(new CleVueDetailPrixVente(compteurIndex++, "Synthèse"));

    // Ajouter des vues pour les formule prix de vente
    if (pCalculPrixVente != null) {
      for (FormulePrixVente formulePrixVente : pCalculPrixVente.getListeFormulePrixVente()) {
        add(new CleVueDetailPrixVente(compteurIndex++, formulePrixVente.getLibelle()));
      }
    }
  }

  /**
   * Retourner la clé d'une vue par son indice.
   * @return Clé de la vue.
   */
  public CleVueDetailPrixVente valueOfByCode(Integer pIndex) {
    for (CleVueDetailPrixVente cleVueDetailPrixVente : this) {
      if (pIndex == cleVueDetailPrixVente.getIndex()) {
        return cleVueDetailPrixVente;
      }
    }
    return null;
  }

}
