/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.stock.detailstock;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import ri.serien.libcommun.gescom.commun.stockattenducommande.StockAttenduCommande;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Renderer du tableau d'affichage des attendus et commandés
 * Les lignes correspondants à des attendus doivent être en gras.
 */
class ListeAttenduCommandeRenderer extends DefaultTableCellRenderer {
  public static final int COLONNE_DATE_LIVRAISON = 0;
  public static final int COLONNE_ENTREE = 1;
  public static final int COLONNE_SORTIE = 2;
  public static final int COLONNE_THEORIQUE = 3;
  public static final int COLONNE_RESTANT_AVANT_ATTENDU = 4;
  public static final int COLONNE_AVANT_RUPTURE = 5;
  public static final int COLONNE_MAGASIN = 6;
  public static final int COLONNE_LIGNE_DOCUMENT = 7;
  public static final int COLONNE_DATE_VALIDATION = 8;
  public static final int COLONNE_TIERS = 9;
  
  private ModeleDetailStock modele = null;
  
  public ListeAttenduCommandeRenderer(ModeleDetailStock pModele) {
    super();
    modele = pModele;
  }
  
  @Override
  public Component getTableCellRendererComponent(JTable pTable, Object pValeur, boolean pSelectionne, boolean pFocus, int pLigne,
      int pColonne) {
    // Récupérer le composant de la cellule
    JLabel composant = (JLabel) super.getTableCellRendererComponent(pTable, pValeur, pSelectionne, pFocus, pLigne, pColonne);
    
    // Sortir de suite si la liste est vide
    if (modele == null || modele.getStockDetaille() == null || modele.getStockDetaille().getListeStockAttenduCommande() == null) {
      return composant;
    }

    // Récupérer l'attendu/commandé de la ligne
    StockAttenduCommande stockAttenduCommande = modele.getStockDetaille().getListeStockAttenduCommande().get(pLigne);
    if (stockAttenduCommande == null) {
      return composant;
    }

    // Mettre la police en gras pour les lignes autres que les commandés
    if (!stockAttenduCommande.getId().isCommande()) {
      composant.setFont(composant.getFont().deriveFont(Font.BOLD));
    }
    
    // Mettre une couleur de fond à la date du jour et la date minimum de réapprovionnement
    if (stockAttenduCommande.getId().isDateJour() || stockAttenduCommande.getId().isDateMiniReappro()) {
      composant.setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
    }
    else {
      composant.setBackground(Color.WHITE);
    }
    
    // Gérer l'alignement suivant le numéro de colonne
    // Le fait d'utiliser ce Renderer annule la configuration de l'alignement effectuée via NRiTable.personnaliserAspect()
    switch (pColonne) {
      case COLONNE_DATE_LIVRAISON:
      case COLONNE_MAGASIN:
      case COLONNE_DATE_VALIDATION:
        composant.setHorizontalAlignment(CENTER);
        break;
      
      case COLONNE_ENTREE:
      case COLONNE_SORTIE:
      case COLONNE_THEORIQUE:
      case COLONNE_RESTANT_AVANT_ATTENDU:
      case COLONNE_AVANT_RUPTURE:
        composant.setHorizontalAlignment(RIGHT);
        break;
      
      default:
        composant.setHorizontalAlignment(LEFT);
    }
    
    return composant;
  }
}
