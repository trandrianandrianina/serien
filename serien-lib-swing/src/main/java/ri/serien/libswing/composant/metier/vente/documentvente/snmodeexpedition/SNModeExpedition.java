/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.snmodeexpedition;

import ri.serien.libcommun.gescom.personnalisation.modeexpedition.IdModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un mode d'expedition.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un ModeExpedition dans le logiciel. Il gère l'affichage des mode
 * d'expedition et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNModeExpedition à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM25FM_B3.
 */
public class SNModeExpedition extends SNComboBoxObjetMetier<IdModeExpedition, ModeExpedition, ListeModeExpedition> {
  
  /**
   * Constructeur par defaut
   */
  public SNModeExpedition() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeModeExpedition());
  }
  
  /**
   * Sélectionner un mode d'expedition de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampModeExpedition) {
    if (pChampModeExpedition == null || pLexical.HostFieldGetData(pChampModeExpedition) == null) {
      throw new MessageErreurException("Impossible de sélectionner le mode d'expedition car son code est invalide.");
    }
    
    IdModeExpedition idModeExpedition = null;
    String valeurModeExpedition = pLexical.HostFieldGetData(pChampModeExpedition);
    if (valeurModeExpedition != null && !valeurModeExpedition.trim().isEmpty()) {
      idModeExpedition = IdModeExpedition.getInstance(pLexical.HostFieldGetData(pChampModeExpedition));
    }
    setIdSelection(idModeExpedition);
  }
  
  /**
   * Renseigner les champs RPG correspondant au mode d'expedition sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampModeExpedition) {
    ModeExpedition modeExpedition = getSelection();
    if (modeExpedition != null) {
      pLexical.HostFieldPutData(pChampModeExpedition, 0, modeExpedition.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampModeExpedition, 0, "");
    }
  }
}
