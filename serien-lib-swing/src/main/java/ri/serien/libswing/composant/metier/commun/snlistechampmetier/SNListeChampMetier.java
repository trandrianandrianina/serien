/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.commun.snlistechampmetier;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.commun.objetmetier.champmetier.ChampMetier;
import ri.serien.libcommun.commun.objetmetier.champmetier.ListeChampMetier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDeselectionTotale;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDeselectionUnitaire;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonSelectionParDefaut;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonSelectionTotale;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonSelectionUnitaire;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposant;

/**
 * Composant de sélection d'une liste de champs métiers appartenant à un objet métier.
 *
 * Ce composant pour fonctionner a besoin de :
 * - la liste complète des champs métiers (obligatoire dans le sens où sans elle ce composant ne sert à rien),
 * - la liste des champs métiers par défaut (facultatif),
 * - la liste des champs métiers utilisés (facultatif).
 *
 * Ce composant peut aussi être personnalisé via les accesseurs :
 * - si l'attribut "obligatoire" du champ métier doit être pris en compte (par défaut c'est oui),
 * - le titre de la colonne des champs métiers disponibles,
 * - le titre de la colonne des champs métiers utilisés.
 */
public class SNListeChampMetier extends SNComposant {
  private static final String TITRE_LISTE_DISPONIBLE = "Non sélectionnés";
  private static final String TITRE_LISTE_UTILISE = "Sélectionnés";
  private static final String[] TITRE_COLONNE_LISTE = new String[] { "Champ", "Description" };

  private ListeChampMetier listeChampMetierComplete = new ListeChampMetier();
  private ListeChampMetier listeChampMetierParDefaut = new ListeChampMetier();
  private ListeChampMetier listeChampMetierDisponible = new ListeChampMetier();
  private ListeChampMetier listeChampMetierUtilise = new ListeChampMetier();
  private String titreListeDisponible = TITRE_LISTE_DISPONIBLE;
  private String titreListeUtilise = TITRE_LISTE_UTILISE;
  private boolean affichageObligatoireActif = true;

  private boolean evenementActif = true;
  private List<Integer> listeIndexDisponible = null;
  private List<Integer> listeIndexUtilise = null;

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Classes internes
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Renderer de la liste des champs métiers utilisés
   */
  public class ListeChampMetierUtiliseRenderer extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row,
        int column) {
      final Component cellule = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

      // Passer les lignes en gras si le champ métier est obligatoire et que la ligne n'est pas sélectionnée
      if (!isSelected && row >= 0 && row < listeChampMetierUtilise.size()) {
        ChampMetier champMetier = listeChampMetierUtilise.get(row);
        if (champMetier.isAffichageObligatoire()) {
          cellule.setFont(SNCharteGraphique.POLICE_TABLEAU_GRAS);
        }
        else {
          cellule.setFont(SNCharteGraphique.POLICE_TABLEAU);
        }
      }
      return cellule;
    }
  }

  /**
   * Gestion du drag'n drop sur les listes de champs métiers.
   */
  public class ChampMetierTransfertHandler extends TransferHandler {
    private final DataFlavor localObjectFlavor =
        new ActivationDataFlavor(Integer.class, DataFlavor.javaJVMLocalObjectMimeType, "Integer Row Index");
    private NRiTable tableSource = null;

    @Override
    protected Transferable createTransferable(JComponent pComposant) {
      tableSource = (NRiTable) pComposant;
      return new DataHandler(tableSource.getSelectedRow(), localObjectFlavor.getMimeType());
    }

    @Override
    public boolean canImport(TransferHandler.TransferSupport pSupport) {
      // Contrôle si le drag'n drop n'est pas possible
      if (!pSupport.isDataFlavorSupported(localObjectFlavor)) {
        return false;
      }

      // Le drag'n drop est possible mais le contrôle est affiné
      NRiTable tableDestination = (NRiTable) pSupport.getComponent();

      // Le déplacement dans la table des champs métiers disponibles est interdit
      if (Constantes.equals(tableDestination, tableSource) && !tableDestination.getName().equals("tblListeChampMetierUtilise")) {
        return false;
      }
      else {
        return true;
      }
    }

    @Override
    public int getSourceActions(JComponent pComposant) {
      return TransferHandler.MOVE;
    }

    @Override
    public boolean importData(TransferHandler.TransferSupport pSupport) {
      NRiTable tableDestination = (NRiTable) pSupport.getComponent();
      NRiTable.DropLocation cibleDestination = (JTable.DropLocation) pSupport.getDropLocation();
      try {
        // Si les deux tables sont identiques alors le déplacement s'effectue dans la même table
        if (Constantes.equals(tableDestination, tableSource)) {
          // Déplacer le champ dans la liste des utilisés
          if (tableDestination.getName().equals("tblListeChampMetierUtilise")) {
            deplacerChampMetierUtilise(cibleDestination.getRow());
          }
          // Interdire de déplacer le champ dans la liste des disponibles
          else {
            return false;
          }
        }
        // Déplacer le champ d'une liste vers une autre
        else {
          // Dépacer des disponibles vers les utilisés
          if (tableDestination.getName().equals("tblListeChampMetierUtilise")) {
            deplacerChampMetierDisponibleVersUtilise(cibleDestination.getRow());
          }
          else {
            // Dépacer des utilisés vers les disponibles
            deplacerChampMetierUtiliseVersDisponible();
          }
        }
        return true;
      }
      catch (Exception e) {
        DialogueErreur.afficher(e);
      }
      return false;
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Constructeur.
   */
  public SNListeChampMetier() {
    super();
    // Construit le composant (via JFormDesigner)
    initComponents();

    // Personnalise les éléments du composant
    initialiserComposants();
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // MODELE - Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Griser ou non tous les menus d'un menu contextuel.
   * @param pJPopupMenu Le menu contextuel.
   * @param pActif true=Actif, false=Non actif
   */
  private void activerMenuContextuel(JPopupMenu pJPopupMenu, boolean pActif) {
    if (pJPopupMenu == null) {
      return;
    }

    // Récupération de tous les composants du menu contextel
    Component[] listeMenu = pmMenuListeUtilise.getComponents();
    // Parcours des différents composants
    for (Component composant : listeMenu) {
      if (composant instanceof JMenuItem) {
        composant.setEnabled(pActif);
      }
    }
  }

  /**
   * Sélectionner le premier élément de la liste dans une liste de sélections.
   *
   * Si l'index sélectionné dépasse la fin de la liste, cela sélectionne le dernier élément de la liste. Si la liste est vide, rien
   * n'est sélectionné.
   *
   * @param pListeIndex Liste de sélections.
   * @param pListeChampMetier Liste des objets métiers concernés par la sélection.
   * @return Liste de sélections corrigée.
   */
  private List<Integer> selectionnerPremierIndex(List<Integer> pListeIndex, ListeChampMetier pListeChampMetier) {
    // Vérifier les paramètres
    if (pListeIndex == null || pListeIndex.isEmpty() || pListeChampMetier == null) {
      return null;
    }

    // Récupérer l'index de la première sélection (c'est forcément la plus petite)
    int index = pListeIndex.get(0);

    // Corriger l'index s'il dépasse la taille de la liste
    if (index >= pListeChampMetier.size()) {
      index = pListeChampMetier.size() - 1;
    }

    // Construire la nouvelle sélection avec ce seul index
    List<Integer> listeIndex = new ArrayList<Integer>();
    if (index >= 0) {
      listeIndex.add(index);
    }
    return listeIndex;
  }

  /**
   * Indiquer si au moins un champ métier non obligatoire est présent dans la liste des champs métiers utilisés.
   * @return true=un champ métier non obligatoire est présent, false=tous les champs métiers présents sont obligatoires.
   */
  private boolean isChampMetierUtiliseNonObligatoirePresent() {
    for (ChampMetier champMetier : listeChampMetierUtilise) {
      if (!champMetier.isAffichageObligatoire()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Indiquer si au moins un champ métier utilisé non obligatoire est sélectionné.
   * @return true=au moins un champ métier non obligatoire est sélectionné, false=tous les champs métiers sélectionnés sont obligatoires.
   */
  private boolean isChampMetierUtiliseNonObligatoireSelectionne() {
    // Vérifier les paramètres
    if (listeIndexUtilise == null) {
      return false;
    }

    for (Integer index : listeIndexUtilise) {
      ChampMetier champMetier = listeChampMetierUtilise.get(index);
      if (champMetier != null && !champMetier.isAffichageObligatoire()) {
        return true;
      }
    }

    return false;
  }

  /**
   * Construire la liste des champs métiers disponibles.
   *
   * La liste des champs métiers disponibles est construite à partir de la liste complète des champs métiers à laquelle on retire la liste
   * des champs métiers utilisés. L'intérêt de toujours repartir de la liste complète est de conserver un ordre constant dans
   * la liste des champs disponibles. Autant la liste des champs utilisés doit être présentée dans l'ordre souhaité par l'utilisateur,
   * autant la liste des champs disponibles doit toujours présenter ses champs dans le même ordre.
   */
  private void determinerListeChampMetierDisponible() {
    // Vider la liste des champs métiers disponibles
    listeChampMetierDisponible.clear();

    // Sortir si la liste complète des champs est null
    if (listeChampMetierComplete == null) {
      return;
    }

    // Ajouter les champs métiers non utilisés dans la liste des champs disponibles
    for (ChampMetier champMetier : listeChampMetierComplete) {
      if (!listeChampMetierUtilise.contains(champMetier)) {
        listeChampMetierDisponible.add(champMetier);
      }
    }
  }

  /**
   * Déplacer la liste des champs métiers disponibles sélectionnés vers la liste des champs métiers utilisés.
   * @param pIndexDestination Index de la ligne à laquelle insérer les champs déplacés.
   */
  private void deplacerChampMetierDisponibleVersUtilise(int pIndexDestination) {
    // Ne rien faire si aucun champ métier disponible n'est sélectionné
    if (listeIndexDisponible == null) {
      return;
    }

    // Déplacer les champs métiers à la dernière place en cas de bizarrerie
    if (pIndexDestination < 0 || pIndexDestination >= listeChampMetierUtilise.size()) {
      pIndexDestination = listeChampMetierUtilise.size();
    }

    // Lister les champs métiers à déplacer
    ListeChampMetier listeChampMetierADeplacer = new ListeChampMetier();
    for (Integer index : listeIndexDisponible) {
      ChampMetier champMetier = listeChampMetierDisponible.get(index);
      if (champMetier != null) {
        listeChampMetierADeplacer.add(champMetier);
      }
    }

    // Ajouter les champs métiers sélectionnés à la liste des champs utilisés
    listeChampMetierUtilise.addAll(pIndexDestination, listeChampMetierADeplacer);

    // Supprimer les champs métiers sélectionnés de la liste des champs disponibles
    listeChampMetierDisponible.removeAll(listeChampMetierADeplacer);

    // Ajuster la sélection
    listeIndexDisponible = selectionnerPremierIndex(listeIndexDisponible, listeChampMetierDisponible);

    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }

  /**
   * Ajouter tous les champs métiers disponibles à la liste des utilisés.
   */
  private void deplacerTousChampMetierDisponibleVersUtilise() {
    // Ajouter les champs métiers à la fin de la liste des champs métiers utilisés
    listeChampMetierUtilise.addAll(listeChampMetierDisponible);

    // Recalculer la liste des champs métiers disponibles
    determinerListeChampMetierDisponible();

    // Effacer les index des lignes sélectionnés
    listeIndexDisponible = null;
    listeIndexUtilise = null;

    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }

  /**
   * Déplacer la liste des champs métiers utilisés sélectionnés vers la liste des champs métiers disponibles.
   */
  private void deplacerChampMetierUtiliseVersDisponible() {
    // Ne rien faire si aucun champ métier utilisé n'est sélectionné
    if (listeIndexUtilise == null) {
      return;
    }

    // Lister les champs métiers à déplacer
    ListeChampMetier listeChampMetierADeplacer = new ListeChampMetier();
    for (Integer index : listeIndexUtilise) {
      ChampMetier champMetier = listeChampMetierUtilise.get(index);
      if (champMetier != null && !champMetier.isAffichageObligatoire()) {
        listeChampMetierADeplacer.add(champMetier);
      }
    }

    // Ajouter les champs métiers sélectionnés à la liste des champs utilisés
    listeChampMetierDisponible.addAll(listeChampMetierADeplacer);

    // Supprimer les champs métiers sélectionnés de la liste des champs utilisés
    listeChampMetierUtilise.removeAll(listeChampMetierADeplacer);

    // Ajuster la sélection
    listeIndexUtilise = selectionnerPremierIndex(listeIndexUtilise, listeChampMetierUtilise);

    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }

  /**
   * Ajouter tous les champs métiers utilisés à la liste des disponibles (sauf les champs obligatoires).
   */
  private void deplacerTousChampMetierUtiliseVersDisponible() {
    // Effacer la liste des champs métiers utilisés
    listeChampMetierUtilise.clear();

    // Ajouter les champs métiers obligatoires
    if (affichageObligatoireActif) {
      for (ChampMetier champMetier : listeChampMetierComplete) {
        if (champMetier.isAffichageObligatoire()) {
          listeChampMetierUtilise.add(champMetier);
        }
      }
    }

    // Recalculer la liste des champs métiers disponibles
    determinerListeChampMetierDisponible();

    // Désélection des lignes dans les listes
    listeIndexDisponible = null;
    listeIndexUtilise = null;

    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }

  /**
   * Déplacer la liste des champs métiers utilisés sélectionnés à un autre endroit de la liste.
   * @param pIndexDestination Indice du rang destination.
   */
  private void deplacerChampMetierUtilise(int pIndexDestination) {
    // Ne rien faire si aucun champ métier utilisé n'est sélectionné
    if (listeIndexUtilise == null) {
      return;
    }

    // Déplacer les champs métiers à la dernière place en cas de bizarrerie
    if (pIndexDestination < 0 || pIndexDestination > listeChampMetierUtilise.size()) {
      pIndexDestination = listeChampMetierUtilise.size();
    }

    // Lister les champs métiers à déplacer
    ListeChampMetier listeChampMetierADeplacer = new ListeChampMetier();
    for (Integer index : listeIndexUtilise) {
      ChampMetier champMetier = listeChampMetierUtilise.get(index);
      if (champMetier != null) {
        listeChampMetierADeplacer.add(champMetier);
      }
    }

    // Supprimer les champs métiers sélectionnés de la liste des champs utilisés
    listeChampMetierUtilise.removeAll(listeChampMetierADeplacer);

    // Calculer l'index pour insérer les champs déplacés
    int offset = 0;
    for (Integer index : listeIndexUtilise) {
      if (index < pIndexDestination) {
        offset--;
      }
    }
    pIndexDestination = pIndexDestination + offset;

    // Ajouter le champ métier au bon emplacement dans la liste des utilisé
    listeChampMetierUtilise.addAll(pIndexDestination, listeChampMetierADeplacer);

    // Sélectionner les lignes insérées
    listeIndexUtilise.clear();
    for (int i = pIndexDestination; i < pIndexDestination + listeChampMetierADeplacer.size(); i++) {
      listeIndexUtilise.add(i);
    }

    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }

  /**
   * Réinitialiser la liste des champs métiers utilisés avec les valeurs par défaut.
   */
  private void remettreListeChampMetierParDefaut() {
    // Renseigner la liste des champs métiers utilisés avec la liste des champs métiers par défaut
    listeChampMetierUtilise.clear();
    listeChampMetierUtilise.addAll(listeChampMetierParDefaut);

    // Recalculer la liste des champs métiers disponibles
    // Cela remet les champs métiers obligatoires en même temps
    determinerListeChampMetierDisponible();

    // Effacer les index des lignes sélectionnés
    listeIndexDisponible = null;
    listeIndexUtilise = null;

    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }

  /**
   * Modifier la liste des champs métiers disponibles sélectionnés.
   * @param pListeIndexDisponible Liste d'index des champs métiers disponibles sélectionnés.
   */
  private void modifierChampMetierDisponibleSelectionne(List<Integer> pListeIndexDisponible) {
    // Vérifier si la valeur a changé
    if (Constantes.equals(pListeIndexDisponible, listeIndexDisponible)) {
      return;
    }

    // Modifier les index
    listeIndexDisponible = pListeIndexDisponible;
    listeIndexUtilise = null;

    // Rafraîchir l'affichage
    rafraichir();
  }

  /**
   * Modifier la liste des champs métiers utilisés sélectionnés.
   * @param pListeIndexUtilise Liste d'index des champs métiers utilisés sélectionnés.
   */
  private void modifierChampMetierUtiliseSelectionne(List<Integer> pListeIndexUtilise) {
    // Vérifier si la valeur a changé
    if (Constantes.equals(pListeIndexUtilise, listeIndexDisponible)) {
      return;
    }

    // Modifier les index
    listeIndexDisponible = null;
    listeIndexUtilise = pListeIndexUtilise;

    // Rafraîchir l'affichage
    rafraichir();
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // MODELE - Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Modifier le titre de la liste des champs métiers disponibles.
   * @param pTitreListeDisponible Le titre.
   */
  public void setTitreListeDisponible(String pTitreListeDisponible) {
    titreListeDisponible = pTitreListeDisponible;
  }

  /**
   * Modifier le titre de la liste des champs métiers utilisés.
   * @param pTitreListeUtilise Le titre.
   */
  public void setTitreListeUtilise(String pTitreListeUtilise) {
    titreListeUtilise = pTitreListeUtilise;
  }

  /**
   * Retourner la liste complète des champs métiers.
   * @return Liste de champs métiers.
   */
  public ListeChampMetier getListeChampMetierComplete() {
    return listeChampMetierDisponible;
  }

  /**
   * Modifier la liste complète des champs métiers.
   *
   * Cette méthode garantie que la liste complète des champs métiers ne peut pas être null. Si null est fournien paramètre, une liste
   * vide est instanciée à la place.
   *
   * @param pListeChampMetierComplete Liste de champs métiers.
   */
  public void setListeChampMetierComplete(ListeChampMetier pListeChampMetierComplete) {
    // Mettre à jour la liste complète des champs métiers
    if (pListeChampMetierComplete != null) {
      listeChampMetierComplete = pListeChampMetierComplete;
    }
    else {
      listeChampMetierComplete = new ListeChampMetier();
    }

    // Recalculer la liste des champs disponibles
    determinerListeChampMetierDisponible();
  }

  /**
   * Retourner la liste des champs métiers par défaut.
   *
   * @return Liste de champs métiers.
   */
  public ListeChampMetier getListeChampMetierParDefaut() {
    return listeChampMetierParDefaut;
  }

  /**
   * Modifier la liste des champs métier par défaut.
   *
   * Cette méthode garantie que la liste des champs métiers par défaut ne peut pas être null. Si null est fournien paramètre, une liste
   * vide est instanciée à la place.
   *
   * @param pListeChampMetierParDefaut Liste de champs métiers.
   */
  public void setListeChampMetierParDefaut(ListeChampMetier pListeChampMetierParDefaut) {
    if (pListeChampMetierParDefaut != null) {
      listeChampMetierParDefaut = pListeChampMetierParDefaut;
    }
    else {
      listeChampMetierParDefaut = new ListeChampMetier();
    }
  }

  /**
   * Retourner la liste des champs métiers utilisés.
   * @return Liste de champs métiers.
   */
  public ListeChampMetier getListeChampMetierUtilise() {
    return listeChampMetierUtilise;
  }

  /**
   * Modifier la liste des champs métiers utilisés.
   *
   * Cette méthode garantie que la liste des champs métiers utilisés ne peut pas être null. Si null est fournien paramètre, une liste
   * vide est instanciée à la place.
   *
   * @param pListeChampMetierUtilise Liste de champs métiers.
   */
  public void setListeChampMetierUtilise(ListeChampMetier pListeChampMetierUtilise) {
    // Mettre à jour la liste des champs métiers utilisés
    if (pListeChampMetierUtilise != null) {
      listeChampMetierUtilise = pListeChampMetierUtilise;
    }
    else {
      listeChampMetierUtilise = new ListeChampMetier();
    }

    // Recalculer la liste des champs disponibles
    determinerListeChampMetierDisponible();
  }

  /**
   * Activer le contrôle de l'affichage obligatoire des champs métiers.
   * @param pAffichageObligatoireActif true=l'affichage obligatoire est actif, false=ignore l'affichage obligatoire.
   */
  public void setAffichageObligatoireActif(boolean pAffichageObligatoireActif) {
    affichageObligatoireActif = pAffichageObligatoireActif;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // VUE - Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Initialiser le composant.
   */
  private void initialiserComposants() {
    // Paramétrer la liste des champs métiers disponibles
    // --------------------------------------------------
    scpListeChampMetierDisponible.getViewport().setBackground(Color.WHITE);
    tblListeChampMetierDisponible.personnaliserAspect(TITRE_COLONNE_LISTE, new int[] { 150, 350 },
        new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE }, 13);

    // Ajouter un listener sur les changement de sélection de la liste des champs métiers disponibles
    tblListeChampMetierDisponible.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeChampMetierDisponibleSelectionChanged(e);
      }
    });

    // Redéfinir la touche Enter sur la liste des champs métiers disponibles
    Action nouvelleActionListeDisponible = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        tblListeChampMetierDisponibleActionPerformed(ae);
      }
    };
    tblListeChampMetierDisponible.modifierAction(nouvelleActionListeDisponible, SNCharteGraphique.TOUCHE_ENTREE);

    // Ajouter le drag and drop sur la liste des champs métiers disponibles
    ChampMetierTransfertHandler handler = new ChampMetierTransfertHandler();
    tblListeChampMetierDisponible.setDragEnabled(true);
    tblListeChampMetierDisponible.setDropMode(DropMode.INSERT_ROWS);
    tblListeChampMetierDisponible.setTransferHandler(handler);

    // Ajouter le menu contextuel pour la liste des champs métiers disponibles
    pmMenuListeDisponible.addPopupMenuListener(new PopupMenuListener() {
      @Override
      public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        // Sélection de la ligne sur laquelle le menu contextuel a été activé
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            // Afficher "Sélectionner" si la sélection n'est pas vide
            miDisponibleSelectionner.setEnabled(listeIndexDisponible != null && !listeIndexDisponible.isEmpty());

            // Afficher "Sélectionner tout" si la liste des champs métiers n'est pas vide
            miDisponibleSelectionnerTout.setEnabled(!listeChampMetierDisponible.isEmpty());

            // Activer les menus contextels si la liste n'est pas vide
            activerMenuContextuel(pmMenuListeDisponible, !listeChampMetierDisponible.isEmpty());
          }
        });
      }

      @Override
      public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
      }

      @Override
      public void popupMenuCanceled(PopupMenuEvent e) {
      }
    });
    tblListeChampMetierDisponible.setComponentPopupMenu(pmMenuListeDisponible);

    // Paramétrer la liste des champs métiers utilisés
    // -----------------------------------------------
    scpListeChampMetierUtilise.getViewport().setBackground(Color.WHITE);
    tblListeChampMetierUtilise.personnaliserAspect(TITRE_COLONNE_LISTE, new int[] { 150, 350 },
        new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE }, 13);
    tblListeChampMetierUtilise.personnaliserAspectCellules(new ListeChampMetierUtiliseRenderer());

    // Ajouter un listener sur les changement de sélection de la liste des champs métiers utilisés
    tblListeChampMetierUtilise.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeChampMetierUtiliseSelectionChanged(e);
      }
    });

    // Redéfinir la touche Enter sur la liste des champs métiers utilisés
    Action nouvelleActionListeSelectionne = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        tblListeChampMetierUtiliseActionPerformed(ae);
      }
    };
    tblListeChampMetierUtilise.modifierAction(nouvelleActionListeSelectionne, SNCharteGraphique.TOUCHE_ENTREE);

    // Ajouter le drag and drop sur la liste des champs métiers utilisés
    tblListeChampMetierUtilise.setDragEnabled(true);
    tblListeChampMetierUtilise.setDropMode(DropMode.INSERT_ROWS);
    tblListeChampMetierUtilise.setTransferHandler(handler);

    // Ajouter le menu contextuel pour la liste des champs métiers utilisés
    pmMenuListeUtilise.addPopupMenuListener(new PopupMenuListener() {
      @Override
      public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        // Sélection de la ligne sur laquelle le menu contextuel a été activé
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {

            // Si la liste est vide, tous les menus sont grisés
            if (listeChampMetierUtilise.isEmpty()) {
              activerMenuContextuel(pmMenuListeUtilise, false);
              return;
            }

            // Active tous les menus du menu contextel
            activerMenuContextuel(pmMenuListeUtilise, true);

            // Afficher "Ne pas sélectionner" si au moins un champ métier non obligatoire est sélectionné
            miUtiliseNePasSelectionner.setEnabled(isChampMetierUtiliseNonObligatoireSelectionne());

            // Afficher "Ne rien sélectionner" si au moins un champ non obligatoire est présent dans la liste
            miUtiliseNeRienSelectionner.setEnabled(isChampMetierUtiliseNonObligatoirePresent());

            // Contrôle si le champ est en tête de liste
            miUtiliseDeplacerEnTete
                .setEnabled(listeIndexUtilise != null && (listeIndexUtilise.size() > 1 || listeIndexUtilise.get(0) != 0));

            // Contrôle sie le champ est en fin de liste
            miUtiliseDeplacerEnFin.setEnabled(listeIndexUtilise != null
                && (listeIndexUtilise.size() > 1 || listeIndexUtilise.get(0) != listeChampMetierUtilise.size() - 1));
          }
        });
      }

      @Override
      public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
      }

      @Override
      public void popupMenuCanceled(PopupMenuEvent e) {
      }
    });
    tblListeChampMetierUtilise.setComponentPopupMenu(pmMenuListeUtilise);
  }

  /**
   * Rafraichir le composant.
   */
  public void rafraichir() {
    evenementActif = false;

    // Les listes
    rafraichirTitreListeChampDisponible();
    rafraichirTitreListeChampUtilise();
    rafraichirListeChampMetierDisponible();
    rafraichirListeChampMetierUtilise();

    // Les boutons
    rafraichirBoutonSelectionUnitaire();
    rafraichirBoutonDeselectionUnitaire();
    rafraichirBoutonSelectionTotale();
    rafraichirBoutonDeselectionTotale();
    rafraichirBoutonParDefaut();

    evenementActif = true;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // VUE - Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir le titre de la liste des champs métiers disponibles.
   */
  private void rafraichirTitreListeChampDisponible() {
    if (titreListeDisponible != null) {
      lbTitreChampMetierDisponible.setText(titreListeDisponible);
    }
    else {
      lbTitreChampMetierDisponible.setText(TITRE_LISTE_DISPONIBLE);
    }
  }

  /**
   * Rafraichir le titre de la liste des champs métiers utilisés.
   */
  private void rafraichirTitreListeChampUtilise() {
    if (titreListeUtilise != null) {
      lbTitreChampMetierUtilise.setText(titreListeUtilise);
    }
    else {
      lbTitreChampMetierUtilise.setText(TITRE_LISTE_UTILISE);
    }
  }

  /**
   * Rafraichir la liste des champs métiers disponibles.
   */
  private void rafraichirListeChampMetierDisponible() {
    // Effacer la table si la liste est vide
    if (listeChampMetierDisponible.isEmpty()) {
      tblListeChampMetierDisponible.mettreAJourDonnees(null);
      return;
    }

    // Mettre à jour la table
    String[][] donnees = new String[listeChampMetierDisponible.size()][TITRE_COLONNE_LISTE.length];
    int ligne = 0;
    for (ChampMetier champMetier : listeChampMetierDisponible) {
      donnees[ligne][0] = champMetier.getLibelle();
      donnees[ligne][1] = champMetier.getDescription();
      ligne++;
    }
    tblListeChampMetierDisponible.mettreAJourDonnees(donnees);

    // Parcourir les champs sélectionnés
    if (listeIndexDisponible != null) {
      for (Integer index : listeIndexDisponible) {
        if (index > -1 && index < tblListeChampMetierDisponible.getRowCount()) {
          tblListeChampMetierDisponible.getSelectionModel().addSelectionInterval(index, index);
        }
      }
    }
  }

  /**
   * Rafraichir la liste des champs métiers utilisés.
   */
  private void rafraichirListeChampMetierUtilise() {
    // Effacer la table si la liste est vide
    if (listeChampMetierUtilise.isEmpty()) {
      tblListeChampMetierUtilise.mettreAJourDonnees(null);
      return;
    }

    // Mettre à jour la table
    String[][] donnees = new String[listeChampMetierUtilise.size()][TITRE_COLONNE_LISTE.length];
    int ligne = 0;
    for (ChampMetier champMetier : listeChampMetierUtilise) {
      donnees[ligne][0] = champMetier.getLibelle();
      donnees[ligne][1] = champMetier.getDescription();
      ligne++;
    }
    tblListeChampMetierUtilise.mettreAJourDonnees(donnees);

    // Parcourir les champs sélectionnés
    if (listeIndexUtilise != null) {
      for (Integer index : listeIndexUtilise) {
        if (index > -1 && index < tblListeChampMetierUtilise.getRowCount()) {
          tblListeChampMetierUtilise.getSelectionModel().addSelectionInterval(index, index);
        }
      }
    }
  }

  /**
   * Rafraichir le bouton permettant la sélection unitaire.
   */
  private void rafraichirBoutonSelectionUnitaire() {
    boolean actif = true;
    if (listeIndexDisponible == null || listeIndexDisponible.isEmpty()) {
      actif = false;
    }
    snBoutonSelectionUnitaire.setVisible(actif);
  }

  /**
   * Rafraichir le bouton permettant la désélection unitaire.
   */
  private void rafraichirBoutonDeselectionUnitaire() {
    boolean actif = true;
    if (listeIndexUtilise == null || listeIndexUtilise.isEmpty()) {
      actif = false;
    }
    else if (affichageObligatoireActif && !isChampMetierUtiliseNonObligatoireSelectionne()) {
      actif = false;
    }
    snBoutonDeselectionUnitaire.setVisible(actif);
  }

  /**
   * Rafraichir le bouton permettant la sélection totale.
   */
  private void rafraichirBoutonSelectionTotale() {
    boolean actif = true;
    if (listeChampMetierDisponible.isEmpty()) {
      actif = false;
    }
    snBoutonSelectionTotale.setVisible(actif);
  }

  /**
   * Rafraichir le bouton permettant la désélection totale.
   */
  private void rafraichirBoutonDeselectionTotale() {
    boolean actif = false;
    if (affichageObligatoireActif) {
      for (ChampMetier champMetier : listeChampMetierUtilise) {
        if (!champMetier.isAffichageObligatoire()) {
          actif = true;
        }
      }
    }
    snBoutonDeselectionTotale.setVisible(actif);
  }

  /**
   * Rafraichir le bouton permettant de réinitialiser la liste par défaut des champs sélectionnés.
   */
  private void rafraichirBoutonParDefaut() {
    boolean actif = true;
    if (Constantes.equals(listeChampMetierParDefaut, listeChampMetierUtilise)) {
      actif = false;
    }
    snBoutonSelectionParDefaut.setVisible(actif);
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // VUE - Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Méthode appelée lorsque la sélection change dans la liste des champs métiers disponibles.
   * La sélection change lorsque l'écran est rafraîchi mais il faut ignorer ces changements.
   * @param e Evénement.
   */
  private void tblListeChampMetierDisponibleSelectionChanged(ListSelectionEvent e) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierChampMetierDisponibleSelectionne(tblListeChampMetierDisponible.getListeIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Méthode appelée lors d'un clic dans la liste des champs métiers disponibles.
   * Utiliser pour déplacer un champ d'une liste à l'autre lors d'un double-clic.
   * @param pMouseEvent L'évènement.
   */
  private void tblListeChampMetierDisponibleMouseClicked(MouseEvent pMouseEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      if (pMouseEvent.getClickCount() == 2) {
        deplacerChampMetierDisponibleVersUtilise(-1);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Méthode appelée lors d'un appui sur la touche entrée dans la liste des champs métiers disponibles.
   * Utiliser pour déplacer un champ d'une liste à l'autre lors de l'appui sur la touchbe entrée.
   * @param pMouseEvent L'évènement.
   */
  private void tblListeChampMetierDisponibleActionPerformed(ActionEvent e) {
    try {
      if (!evenementActif) {
        return;
      }
      deplacerChampMetierDisponibleVersUtilise(-1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Méthode appelée lorsque la sélection change dans la liste des champs métiers utilisés.
   * La sélection change lorsque l'écran est rafraîchi mais il faut ignorer ces changements.
   * @param e Evénement.
   */
  private void tblListeChampMetierUtiliseSelectionChanged(ListSelectionEvent e) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierChampMetierUtiliseSelectionne(tblListeChampMetierUtilise.getListeIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Méthode appelée lors d'un clic dans la liste des champs métiers utilisés.
   * Utiliser pour déplacer un champ d'une liste à l'autre lors d'un double-clic.
   * @param pMouseEvent L'évènement.
   */
  private void tblListeChampMetierUtiliseMouseClicked(MouseEvent pMouseEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      if (pMouseEvent.getClickCount() == 2) {
        deplacerChampMetierUtiliseVersDisponible();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Méthode appelée lors d'un appui sur la touche entrée dans la liste des champs métiers utilisés.
   * Utiliser pour déplacer un champ d'une liste à l'autre lors de l'appui sur la touchbe entrée.
   * @param pMouseEvent L'évènement.
   */
  private void tblListeChampMetierUtiliseActionPerformed(ActionEvent e) {
    try {
      if (!evenementActif) {
        return;
      }
      deplacerChampMetierUtiliseVersDisponible();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le bouton permettant de déplacer un champ métier vers la liste des utilisés.
   * @param pActionEvent L'évènement.
   */
  private void snBoutonSelectionUnitaireActionPerformed(ActionEvent e) {
    try {
      deplacerChampMetierDisponibleVersUtilise(-1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le bouton permettant de déplacer un champ métier vers la liste des disponibles.
   * @param pActionEvent L'évènement.
   */
  private void snBoutonDeselectionUnitaireActionPerformed(ActionEvent pActionEvent) {
    try {
      deplacerChampMetierUtiliseVersDisponible();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le bouton permettant de déplacer tous les champs métiers vers la liste des utilisés.
   * @param pActionEvent L'évènement.
   */
  private void snBoutonSelectionTotaleActionPerformed(ActionEvent pActionEvent) {
    try {
      deplacerTousChampMetierDisponibleVersUtilise();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le bouton permettant de déplacer tous les champs métiers vers la liste des disponibles.
   * @param pActionEvent L'évènement.
   */
  private void snBoutonDeselectionTotaleActionPerformed(ActionEvent pActionEvent) {
    try {
      deplacerTousChampMetierUtiliseVersDisponible();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le bouton permettant remettre la liste par défaut des champs métiers utilisés.
   * @param pActionEvent L'évènement.
   */
  private void snBoutonSelectionParDefautActionPerformed(ActionEvent e) {
    try {
      remettreListeChampMetierParDefaut();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le menu "Sélectionner".
   * @param pActionEvent L'évènement.
   */
  private void miDisponibleSelectionnerActionPerformed(ActionEvent pActionEvent) {
    try {
      deplacerChampMetierDisponibleVersUtilise(listeChampMetierUtilise.size());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le menu "Sélectionner tout".
   * @param pActionEvent L'évènement.
   */
  private void miDisponibleSelectionnerToutActionPerformed(ActionEvent pActionEvent) {
    try {
      deplacerTousChampMetierDisponibleVersUtilise();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le menu "Remettre par défaut".
   * @param pActionEvent L'évènement.
   */
  private void miDisponibleRemettreParDefautActionPerformed(ActionEvent pActionEvent) {
    try {
      remettreListeChampMetierParDefaut();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le menu "Ne pas sélectionner".
   * @param pActionEvent L'évènement.
   */
  private void miUtiliseNePasSelectionnerActionPerformed(ActionEvent pActionEvent) {
    try {
      deplacerChampMetierUtiliseVersDisponible();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le menu "Ne rien sélectionner".
   * @param pActionEvent L'évènement.
   */
  private void miUtiliseNeRienSelectionnerActionPerformed(ActionEvent pActionEvent) {
    try {
      deplacerTousChampMetierUtiliseVersDisponible();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le menu "Remettre par défaut".
   * @param pActionEvent L'évènement.
   */
  private void miUtiliseRemettreParDefautActionPerformed(ActionEvent pActionEvent) {
    try {
      remettreListeChampMetierParDefaut();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le menu "Dépacer en tête de liste".
   * @param pActionEvent L'évènement.
   */
  private void miUtiliseDeplacerEnTeteActionPerformed(ActionEvent pActionEvent) {
    try {
      deplacerChampMetierUtilise(0);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Gérer le clic sur le menu "Dépacer en fin de liste".
   * @param pActionEvent L'évènement.
   */
  private void miUtiliseDeplacerEnFinActionPerformed(ActionEvent pActionEvent) {
    try {
      deplacerChampMetierUtilise(listeChampMetierUtilise.size());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // VUE - JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Constuire le composant.
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snSelectionChampMetier = new SNPanel();
    lbTitreChampMetierDisponible = new SNLabelTitre();
    lbTitreChampMetierUtilise = new SNLabelTitre();
    scpListeChampMetierDisponible = new JScrollPane();
    tblListeChampMetierDisponible = new NRiTable();
    snPanelCentre = new SNPanel();
    snBoutonSelectionUnitaire = new SNBoutonSelectionUnitaire();
    snBoutonDeselectionUnitaire = new SNBoutonDeselectionUnitaire();
    snBoutonSelectionTotale = new SNBoutonSelectionTotale();
    snBoutonDeselectionTotale = new SNBoutonDeselectionTotale();
    snBoutonSelectionParDefaut = new SNBoutonSelectionParDefaut();
    scpListeChampMetierUtilise = new JScrollPane();
    tblListeChampMetierUtilise = new NRiTable();
    pmMenuListeDisponible = new JPopupMenu();
    miDisponibleSelectionner = new JMenuItem();
    miDisponibleSelectionnerTout = new JMenuItem();
    miDisponibleRemettreParDefaut = new JMenuItem();
    pmMenuListeUtilise = new JPopupMenu();
    miUtiliseNePasSelectionner = new JMenuItem();
    miUtiliseNeRienSelectionner = new JMenuItem();
    miUtiliseRemettreParDefaut = new JMenuItem();
    miUtiliseDeplacerEnTete = new JMenuItem();
    miUtiliseDeplacerEnFin = new JMenuItem();

    // ======== this ========
    setName("this");
    setLayout(new GridLayout());

    // ======== snSelectionChampMetier ========
    {
      snSelectionChampMetier.setName("snSelectionChampMetier");
      snSelectionChampMetier.setLayout(new GridBagLayout());
      ((GridBagLayout) snSelectionChampMetier.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) snSelectionChampMetier.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) snSelectionChampMetier.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) snSelectionChampMetier.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };

      // ---- lbTitreChampMetierDisponible ----
      lbTitreChampMetierDisponible.setText("Non s\u00e9lectionn\u00e9s");
      lbTitreChampMetierDisponible.setName("lbTitreChampMetierDisponible");
      snSelectionChampMetier.add(lbTitreChampMetierDisponible,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

      // ---- lbTitreChampMetierUtilise ----
      lbTitreChampMetierUtilise.setText("S\u00e9lectionn\u00e9s");
      lbTitreChampMetierUtilise.setName("lbTitreChampMetierUtilise");
      snSelectionChampMetier.add(lbTitreChampMetierUtilise,
          new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

      // ======== scpListeChampMetierDisponible ========
      {
        scpListeChampMetierDisponible.setName("scpListeChampMetierDisponible");

        // ---- tblListeChampMetierDisponible ----
        tblListeChampMetierDisponible.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Libell\u00e9", "Description" }) {
          Class<?>[] columnTypes = new Class<?>[] { String.class, String.class };
          boolean[] columnEditable = new boolean[] { false, true };

          @Override
          public Class<?> getColumnClass(int columnIndex) {
            return columnTypes[columnIndex];
          }

          @Override
          public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnEditable[columnIndex];
          }
        });
        {
          TableColumnModel cm = tblListeChampMetierDisponible.getColumnModel();
          cm.getColumn(0).setMinWidth(220);
          cm.getColumn(0).setMaxWidth(220);
          cm.getColumn(0).setPreferredWidth(220);
        }
        tblListeChampMetierDisponible.setMinimumSize(new Dimension(160, 560));
        tblListeChampMetierDisponible.setPreferredSize(new Dimension(360, 560));
        tblListeChampMetierDisponible.setBackground(Color.white);
        tblListeChampMetierDisponible.setShowHorizontalLines(true);
        tblListeChampMetierDisponible.setShowVerticalLines(true);
        tblListeChampMetierDisponible.setSelectionForeground(Color.white);
        tblListeChampMetierDisponible.setGridColor(new Color(204, 204, 204));
        tblListeChampMetierDisponible.setRowHeight(20);
        tblListeChampMetierDisponible.setComponentPopupMenu(pmMenuListeDisponible);
        tblListeChampMetierDisponible.setName("tblListeChampMetierDisponible");
        tblListeChampMetierDisponible.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblListeChampMetierDisponibleMouseClicked(e);
          }
        });
        scpListeChampMetierDisponible.setViewportView(tblListeChampMetierDisponible);
      }
      snSelectionChampMetier.add(scpListeChampMetierDisponible,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

      // ======== snPanelCentre ========
      {
        snPanelCentre.setName("snPanelCentre");
        snPanelCentre.setLayout(new GridBagLayout());
        ((GridBagLayout) snPanelCentre.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) snPanelCentre.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) snPanelCentre.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) snPanelCentre.getLayout()).rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };

        // ---- snBoutonSelectionUnitaire ----
        snBoutonSelectionUnitaire.setName("snBoutonSelectionUnitaire");
        snBoutonSelectionUnitaire.addActionListener(e -> snBoutonSelectionUnitaireActionPerformed(e));
        snPanelCentre.add(snBoutonSelectionUnitaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

        // ---- snBoutonDeselectionUnitaire ----
        snBoutonDeselectionUnitaire.setName("snBoutonDeselectionUnitaire");
        snBoutonDeselectionUnitaire.addActionListener(e -> snBoutonDeselectionUnitaireActionPerformed(e));
        snPanelCentre.add(snBoutonDeselectionUnitaire, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

        // ---- snBoutonSelectionTotale ----
        snBoutonSelectionTotale.setName("snBoutonSelectionTotale");
        snBoutonSelectionTotale.addActionListener(e -> snBoutonSelectionTotaleActionPerformed(e));
        snPanelCentre.add(snBoutonSelectionTotale, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

        // ---- snBoutonDeselectionTotale ----
        snBoutonDeselectionTotale.setName("snBoutonDeselectionTotale");
        snBoutonDeselectionTotale.addActionListener(e -> snBoutonDeselectionTotaleActionPerformed(e));
        snPanelCentre.add(snBoutonDeselectionTotale, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

        // ---- snBoutonSelectionParDefaut ----
        snBoutonSelectionParDefaut.setName("snBoutonSelectionParDefaut");
        snBoutonSelectionParDefaut.addActionListener(e -> snBoutonSelectionParDefautActionPerformed(e));
        snPanelCentre.add(snBoutonSelectionParDefaut, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      }
      snSelectionChampMetier.add(snPanelCentre,
          new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

      // ======== scpListeChampMetierUtilise ========
      {
        scpListeChampMetierUtilise.setBackground(Color.white);
        scpListeChampMetierUtilise.setName("scpListeChampMetierUtilise");

        // ---- tblListeChampMetierUtilise ----
        tblListeChampMetierUtilise.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Libell\u00e9", "Description" }) {
          Class<?>[] columnTypes = new Class<?>[] { String.class, String.class };
          boolean[] columnEditable = new boolean[] { false, true };

          @Override
          public Class<?> getColumnClass(int columnIndex) {
            return columnTypes[columnIndex];
          }

          @Override
          public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnEditable[columnIndex];
          }
        });
        {
          TableColumnModel cm = tblListeChampMetierUtilise.getColumnModel();
          cm.getColumn(0).setMinWidth(220);
          cm.getColumn(0).setMaxWidth(220);
          cm.getColumn(0).setPreferredWidth(220);
        }
        tblListeChampMetierUtilise.setMinimumSize(new Dimension(160, 560));
        tblListeChampMetierUtilise.setPreferredSize(new Dimension(360, 560));
        tblListeChampMetierUtilise.setBackground(Color.white);
        tblListeChampMetierUtilise.setShowHorizontalLines(true);
        tblListeChampMetierUtilise.setShowVerticalLines(true);
        tblListeChampMetierUtilise.setSelectionForeground(Color.white);
        tblListeChampMetierUtilise.setGridColor(new Color(204, 204, 204));
        tblListeChampMetierUtilise.setRowHeight(20);
        tblListeChampMetierUtilise.setComponentPopupMenu(pmMenuListeUtilise);
        tblListeChampMetierUtilise.setName("tblListeChampMetierUtilise");
        tblListeChampMetierUtilise.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblListeChampMetierUtiliseMouseClicked(e);
          }
        });
        scpListeChampMetierUtilise.setViewportView(tblListeChampMetierUtilise);
      }
      snSelectionChampMetier.add(scpListeChampMetierUtilise,
          new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(snSelectionChampMetier);

    // ======== pmMenuListeDisponible ========
    {
      pmMenuListeDisponible.setName("pmMenuListeDisponible");

      // ---- miDisponibleSelectionner ----
      miDisponibleSelectionner.setText("S\u00e9lectionner");
      miDisponibleSelectionner.setName("miDisponibleSelectionner");
      miDisponibleSelectionner.addActionListener(e -> miDisponibleSelectionnerActionPerformed(e));
      pmMenuListeDisponible.add(miDisponibleSelectionner);

      // ---- miDisponibleSelectionnerTout ----
      miDisponibleSelectionnerTout.setText("S\u00e9lectionner tout");
      miDisponibleSelectionnerTout.setName("miDisponibleSelectionnerTout");
      miDisponibleSelectionnerTout.addActionListener(e -> miDisponibleSelectionnerToutActionPerformed(e));
      pmMenuListeDisponible.add(miDisponibleSelectionnerTout);

      // ---- miDisponibleRemettreParDefaut ----
      miDisponibleRemettreParDefaut.setText("Remettre par d\u00e9faut");
      miDisponibleRemettreParDefaut.setName("miDisponibleRemettreParDefaut");
      miDisponibleRemettreParDefaut.addActionListener(e -> miDisponibleRemettreParDefautActionPerformed(e));
      pmMenuListeDisponible.add(miDisponibleRemettreParDefaut);
    }

    // ======== pmMenuListeUtilise ========
    {
      pmMenuListeUtilise.setName("pmMenuListeUtilise");

      // ---- miUtiliseNePasSelectionner ----
      miUtiliseNePasSelectionner.setText("Ne pas s\u00e9lectionner");
      miUtiliseNePasSelectionner.setName("miUtiliseNePasSelectionner");
      miUtiliseNePasSelectionner.addActionListener(e -> miUtiliseNePasSelectionnerActionPerformed(e));
      pmMenuListeUtilise.add(miUtiliseNePasSelectionner);

      // ---- miUtiliseNeRienSelectionner ----
      miUtiliseNeRienSelectionner.setText("Ne rien s\u00e9lectionner");
      miUtiliseNeRienSelectionner.setName("miUtiliseNeRienSelectionner");
      miUtiliseNeRienSelectionner.addActionListener(e -> miUtiliseNeRienSelectionnerActionPerformed(e));
      pmMenuListeUtilise.add(miUtiliseNeRienSelectionner);

      // ---- miUtiliseRemettreParDefaut ----
      miUtiliseRemettreParDefaut.setText("Remettre par d\u00e9faut");
      miUtiliseRemettreParDefaut.setName("miUtiliseRemettreParDefaut");
      miUtiliseRemettreParDefaut.addActionListener(e -> miUtiliseRemettreParDefautActionPerformed(e));
      pmMenuListeUtilise.add(miUtiliseRemettreParDefaut);
      pmMenuListeUtilise.addSeparator();

      // ---- miUtiliseDeplacerEnTete ----
      miUtiliseDeplacerEnTete.setText("Mettre en t\u00eate de liste");
      miUtiliseDeplacerEnTete.setName("miUtiliseDeplacerEnTete");
      miUtiliseDeplacerEnTete.addActionListener(e -> miUtiliseDeplacerEnTeteActionPerformed(e));
      pmMenuListeUtilise.add(miUtiliseDeplacerEnTete);

      // ---- miUtiliseDeplacerEnFin ----
      miUtiliseDeplacerEnFin.setText("Mettre en fin de liste");
      miUtiliseDeplacerEnFin.setName("miUtiliseDeplacerEnFin");
      miUtiliseDeplacerEnFin.addActionListener(e -> miUtiliseDeplacerEnFinActionPerformed(e));
      pmMenuListeUtilise.add(miUtiliseDeplacerEnFin);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel snSelectionChampMetier;
  private SNLabelTitre lbTitreChampMetierDisponible;
  private SNLabelTitre lbTitreChampMetierUtilise;
  private JScrollPane scpListeChampMetierDisponible;
  private NRiTable tblListeChampMetierDisponible;
  private SNPanel snPanelCentre;
  private SNBoutonSelectionUnitaire snBoutonSelectionUnitaire;
  private SNBoutonDeselectionUnitaire snBoutonDeselectionUnitaire;
  private SNBoutonSelectionTotale snBoutonSelectionTotale;
  private SNBoutonDeselectionTotale snBoutonDeselectionTotale;
  private SNBoutonSelectionParDefaut snBoutonSelectionParDefaut;
  private JScrollPane scpListeChampMetierUtilise;
  private NRiTable tblListeChampMetierUtilise;
  private JPopupMenu pmMenuListeDisponible;
  private JMenuItem miDisponibleSelectionner;
  private JMenuItem miDisponibleSelectionnerTout;
  private JMenuItem miDisponibleRemettreParDefaut;
  private JPopupMenu pmMenuListeUtilise;
  private JMenuItem miUtiliseNePasSelectionner;
  private JMenuItem miUtiliseNeRienSelectionner;
  private JMenuItem miUtiliseRemettreParDefaut;
  private JMenuItem miUtiliseDeplacerEnTete;
  private JMenuItem miUtiliseDeplacerEnFin;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
