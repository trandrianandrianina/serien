/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.plagedate;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.moteur.composant.SNComposant;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant de saisie d'une plage de dates.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une plage de dates dans le logiciel.
 *
 * Utilisation :
 * - Ajouter un composant SNPlageDate à un écran.
 * - Utiliser la méthode setDefilement() pour choisir le mode de défilement des dates lorsqu'on utilise les boutons précédent et suivant
 * (plus ou moins une semaine, mois ou année). Si on initialise à "sans défilement" le composant n'affiche pas les boutons
 * précédent et suivant.
 * - Utiliser les accesseurs setDateDebutParChampRPG() et setDateFinParChampRPG() pour renseigner les dates de début et de fin du
 * composant graphique à partir de champs RPG. Les méthodes renseignerChampRPGDebut() et renseignerChampRPGFGin() permettent au
 * contraire de renseigner les champs RPG à partir du composant graphique.
 * - Utiliser les accesseurs setDateDebut() et setDateFin() pour renseigner les dates de début et de fin manuellement.
 *
 * Voir un exemple d'implémentation dans le VueListeConsultationDocumentVente.
 */
public class SNPlageDate extends SNComposant {
  // Variables
  EnumDefilementPlageDate defilement = EnumDefilementPlageDate.SANS_DEFILEMENT;
  Date dateDebut = null;
  Date dateFin = null;
  
  /**
   * Constructeur.
   */
  public SNPlageDate() {
    super();
    initComponents();
  }

  /**
   * Gérer l'activation des composants.
   */
  @Override
  public void setEnabled(boolean pIsEnabled) {
    btPlagePrecedente.setEnabled(pIsEnabled);
    btPlageSuivante.setEnabled(pIsEnabled);
    snDateDebut.setEnabled(pIsEnabled);
    snDateFin.setEnabled(pIsEnabled);
  }
  
  /**
   * Rafraîchir les composants.
   */
  public void rafraichir() {
    rafraichirBoutonDefilement();
    rafraichirDateDebut();
    rafraichirDateFin();
  }
  
  /**
   * Rafraichir l'affichage des boutons de défilement.
   */
  private void rafraichirBoutonDefilement() {
    if (defilement == EnumDefilementPlageDate.SANS_DEFILEMENT) {
      btPlagePrecedente.setVisible(false);
      btPlageSuivante.setVisible(false);
    }
    else {
      btPlagePrecedente.setVisible(true);
      btPlageSuivante.setVisible(true);
    }
  }
  
  /**
   * Rafra^cihir l'affichage de la date de début.
   */
  private void rafraichirDateDebut() {
    snDateDebut.setDate(dateDebut);
  }

  /**
   * Rafraîchir l'affichage de la date de fin.
   */
  private void rafraichirDateFin() {
    snDateFin.setDate(dateFin);
  }

  /**
   * Renseigner la date de début.
   *
   * @param pDateDebut Date de début de la plage.
   */
  public void setDateDebut(Date pDateDebut) {
    // Si la date renseignée et la date en mémoire sont les mêmes, on quitte la méthode.
    if (Constantes.equals(pDateDebut, dateDebut)) {
      return;
    }
    dateDebut = pDateDebut;
    
    // Si la date de début est après la date de fin, on efface la date de fin.
    if (!controlerSaisieDateDebut()) {
      dateFin = null;
    }
    rafraichir();
  }

  /**
   * Retourner la date de début.
   *
   * @return Date de début de la plage.
   */
  public Date getDateDebut() {
    return dateDebut;
  }

  /**
   * Renseigner la date de fin.
   *
   * @param pDateFin Date de fin de la plage.
   */
  public void setDateFin(Date pDateFin) {
    // Si la date renseignée et la date en mémoire sont les mêmes, on quitte la méthode.
    if (Constantes.equals(pDateFin, dateFin)) {
      return;
    }
    dateFin = pDateFin;

    // Si la date de fin est avant la date de début, on efface la date de début.
    if (!controlerSaisieDateFin()) {
      dateDebut = null;
    }
    rafraichir();
    // fireValueChanged();
  }

  /**
   * Retourner la date de fin.
   *
   * @return Date de fin de la plage.
   */
  public Date getDateFin() {
    return dateFin;
  }
  
  /**
   * Renseigner la date de début à partir d'un champ RPG.
   *
   * @param pLexique
   * @param pChampDate Champ RPG contenant la date de début.
   */
  public void setDateDebutParChampRPG(Lexical pLexique, String pChampDate) {
    // Contrôle des paramètres
    if (pLexique == null) {
      throw new MessageErreurException("Impossible d'afficher la date car le lexique est invalide.");
    }
    if (pChampDate == null || pChampDate.isEmpty()) {
      throw new MessageErreurException("Impossible d'afficher la date car son champ est invalide.");
    }

    // Récupération de la date dans le buffer
    String dateDeRPG = pLexique.HostFieldGetData(pChampDate);
    setDateDebut(Constantes.convertirTexteEnDate(dateDeRPG));
  }
  
  /**
   * Renseigner la date de fin à partir d'un champ RPG.
   *
   * @param pLexique
   * @param pChampDate Champ RPG contenant la date de fin.
   */
  public void setDateFinParChampRPG(Lexical pLexique, String pChampDate) {
    // Contrôle des paramètres
    if (pLexique == null) {
      throw new MessageErreurException("Impossible d'afficher la date car le lexique est invalide.");
    }
    if (pChampDate == null || pChampDate.isEmpty()) {
      throw new MessageErreurException("Impossible d'afficher la date car son champ est invalide.");
    }
    
    // Récupération de la date dans le buffer
    String dateDeRPG = pLexique.HostFieldGetData(pChampDate);
    setDateFin(Constantes.convertirTexteEnDate(dateDeRPG));
  }
  
  /**
   * Retourner la date de début dans un champ RPG.
   *
   * @param pLexique
   * @param pChampDate Champ RPG contenant la date de début.
   */
  public void renseignerChampRPGDebut(Lexical pLexique, String pChampDate) {
    // Contrôle des paramètres.
    if (pLexique == null) {
      throw new MessageErreurException("Impossible de renseigner la date car le lexique est invalide.");
    }
    if (pChampDate == null || pChampDate.isEmpty()) {
      throw new MessageErreurException("Impossible d'afficher la date car son champ est invalide.");
    }
    
    // Si la date est à null, on envoie un blanc au RPG.
    if (dateDebut == null) {
      pLexique.HostFieldPutData(pChampDate, 0, "");
      return;
    }
    // Sinon on formate la date pour le rpg, jj.mm.aa, et on l'envoie.
    SimpleDateFormat dateFormat = new SimpleDateFormat(DateHeure.FORMAT8_DATE);
    pLexique.HostFieldPutData(pChampDate, 0, dateFormat.format(dateDebut));
  }

  /**
   * Retourner la date de fin dans un champ RPG.
   *
   * @param pLexique
   * @param pChampDate Champ RPG contenant la date de fin.
   */
  public void renseignerChampRPGFin(Lexical pLexique, String pChampDate) {
    // Contrôle des paramètres.
    if (pLexique == null) {
      throw new MessageErreurException("Impossible de renseigner la date car le lexique est invalide.");
    }
    if (pChampDate == null || pChampDate.isEmpty()) {
      throw new MessageErreurException("Impossible d'afficher la date car son champ est invalide.");
    }
    
    // Si la date est à null, on envoie un blanc au RPG.
    if (dateFin == null) {
      pLexique.HostFieldPutData(pChampDate, 0, "");
      return;
    }
    // Sinon on formate la date pour le rpg, jj.mm.aa, et on l'envoie.
    SimpleDateFormat dateFormat = new SimpleDateFormat(DateHeure.FORMAT8_DATE);
    pLexique.HostFieldPutData(pChampDate, 0, dateFormat.format(dateFin));
  }
  
  /**
   * Définir le mode de défilement du composant.
   *
   * @param pEnumDefilement
   */
  public void setDefilement(EnumDefilementPlageDate pEnumDefilement) {
    if (pEnumDefilement == null || Constantes.equals(defilement, pEnumDefilement)) {
      return;
    }
    defilement = pEnumDefilement;
  }

  /**
   * Retourner le mode de défilement du composant.
   *
   * @return
   */
  public EnumDefilementPlageDate getDefilement() {
    return defilement;
  }

  /**
   * Avancer la plage de date en fonction du mode de défilement paramétré.
   */
  private void avancerPlageDate() {
    // Récupérer la date de début actuelle
    Calendar calendarDebut = Calendar.getInstance();
    if (dateDebut != null) {
      calendarDebut.setTime(dateDebut);
    }
    
    // Récupérer la date de fin actuelle
    Calendar calendarFin = Calendar.getInstance();
    if (dateFin != null) {
      calendarFin.setTime(dateFin);
    }
    
    // Avancer la plage de dates suivant le mode de défilement en cours
    switch (defilement) {
      case SANS_DEFILEMENT:
        break;
      
      case DEFILEMENT_SEMAINE:
        calendarDebut.add(Calendar.DATE, 7);
        calendarFin.add(Calendar.DATE, 7);
        break;
      
      case DEFILEMENT_MOIS:
        calendarDebut.add(Calendar.MONTH, 1);
        calendarFin.add(Calendar.MONTH, 1);
        break;
      
      case DEFILEMENT_ANNEE:
        calendarDebut.add(Calendar.YEAR, 1);
        calendarFin.add(Calendar.YEAR, 1);
        break;
    }
    
    // Mettre à jour les dates décalées
    dateDebut = calendarDebut.getTime();
    dateFin = calendarFin.getTime();
  }

  /**
   * Reculer la plage de date en fonction du mode de défilement paramétré.
   */
  private void reculerPlageDate() {
    // Récupérer la date de début actuelle
    
    Calendar calendarDebut = Calendar.getInstance();
    if (dateDebut != null) {
      calendarDebut.setTime(dateDebut);
    }
    
    // Récupérer la date de fin actuelle
    Calendar calendarFin = Calendar.getInstance();
    if (dateFin != null) {
      calendarFin.setTime(dateFin);
    }
    
    // Reculer la plage de dates suivant le mode de défilement en cours
    switch (defilement) {
      case SANS_DEFILEMENT:
        break;
      
      case DEFILEMENT_SEMAINE:
        calendarDebut.add(Calendar.DATE, -7);
        calendarFin.add(Calendar.DATE, -7);
        break;
      
      case DEFILEMENT_MOIS:
        calendarDebut.add(Calendar.MONTH, -1);
        calendarFin.add(Calendar.MONTH, -1);
        break;
      
      case DEFILEMENT_ANNEE:
        calendarDebut.add(Calendar.YEAR, -1);
        calendarFin.add(Calendar.YEAR, -1);
        break;
    }
    
    // Mettre à jour les dates décalées
    dateDebut = calendarDebut.getTime();
    dateFin = calendarFin.getTime();
  }

  /**
   * Contrôler la date de début saisie par rapport à la date de fin en mémoire si elle existe.
   *
   * @return true si la date de début est antérieure à la date de fin, false sinon.
   */
  private boolean controlerSaisieDateDebut() {
    // S'il n'y a pas de date de fin en mémoire, pas de problème d'antériorité.
    if (dateFin == null) {
      return true;
    }
    
    // On formate la date de fin pour lui retirer sa composante horaire.
    Date dateFinFormateSansHeure = Constantes.formaterDateSansHeure(dateFin);

    // La date de début saisie est comparée à la date de fin formatée.
    if (dateDebut != null && dateFin != null && dateDebut.compareTo(dateFinFormateSansHeure) > 0) {
      return false;
    }
    return true;
  }

  /**
   * Contrôler la date de fin saisie par rapport à la date de début en mémoire si elle existe.
   *
   * @return true si la date de fin est postérieure à la date de début, false sinon.
   */
  private boolean controlerSaisieDateFin() {
    // S'il n'y a pas de date de début en mémoire, pas de problème de postériorité.
    if (dateDebut == null) {
      return true;
    }
    
    // On formate la date de debut pour lui retirer sa composante horaire.
    Date dateDebutFormateSansHeure = Constantes.formaterDateSansHeure(dateDebut);

    // La date de fin saisie est comparée à la date de début formatée.
    if (dateFin != null && dateDebut != null && dateFin.compareTo(dateDebutFormateSansHeure) < 0) {
      return false;
    }
    return true;
  }

  // -- Méthodes événementielles
  
  /**
   * Traiter le clic sur le bouton "Plage pracadente"
   *
   * @param e
   */
  private void btPlagePrecedenteActionPerformed(ActionEvent e) {
    try {
      reculerPlageDate();
      rafraichir();
      fireValueChanged();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Traiter le clic sur le bouton "Plage suivante".
   *
   * @param e
   */
  private void btPlageSuivanteActionPerformed(ActionEvent e) {
    try {
      avancerPlageDate();
      rafraichir();
      fireValueChanged();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de la date de dbéut saisie.
   *
   * @param e
   */
  private void snDateDebutValueChanged(SNComposantEvent e) {
    try {
      dateDebut = snDateDebut.getDate();
      if (!controlerSaisieDateDebut()) {
        dateFin = null;
      }
      rafraichir();
      fireValueChanged();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Traiter la modification de la date de fin saisie.
   *
   * @param e
   */
  private void snDateFinValueChanged(SNComposantEvent e) {
    try {
      dateFin = snDateFin.getDate();
      if (!controlerSaisieDateFin()) {
        dateDebut = null;
      }
      rafraichir();
      fireValueChanged();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    btPlagePrecedente = new JButton();
    lbDu2 = new SNLabelChamp();
    snDateDebut = new SNDate();
    lbAu = new SNLabelChamp();
    snDateFin = new SNDate();
    btPlageSuivante = new JButton();

    // ======== this ========
    setBackground(new Color(239, 239, 222));
    setMinimumSize(new Dimension(315, 30));
    setPreferredSize(new Dimension(315, 30));
    setRequestFocusEnabled(false);
    setMaximumSize(new Dimension(315, 30));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 35, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };

    // ---- btPlagePrecedente ----
    btPlagePrecedente.setFont(new Font("sansserif", Font.BOLD, 12));
    btPlagePrecedente.setBackground(new Color(171, 148, 79));
    btPlagePrecedente.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    btPlagePrecedente.setPreferredSize(new Dimension(30, 30));
    btPlagePrecedente.setMinimumSize(new Dimension(30, 30));
    btPlagePrecedente.setMaximumSize(new Dimension(30, 30));
    btPlagePrecedente.setToolTipText("Mois pr\u00e9c\u00e9dent");
    btPlagePrecedente.setIcon(new ImageIcon(getClass().getResource("/images/flechePrecedent.png")));
    btPlagePrecedente.setContentAreaFilled(false);
    btPlagePrecedente.setBorderPainted(false);
    btPlagePrecedente.setVisible(false);
    btPlagePrecedente.setName("btPlagePrecedente");
    btPlagePrecedente.addActionListener(e -> btPlagePrecedenteActionPerformed(e));
    add(btPlagePrecedente,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbDu2 ----
    lbDu2.setText("du");
    lbDu2.setMaximumSize(new Dimension(30, 30));
    lbDu2.setMinimumSize(new Dimension(30, 30));
    lbDu2.setPreferredSize(new Dimension(30, 30));
    lbDu2.setHorizontalAlignment(SwingConstants.CENTER);
    lbDu2.setName("lbDu2");
    add(lbDu2,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- snDateDebut ----
    snDateDebut.setMaximumSize(new Dimension(126, 30));
    snDateDebut.setName("snDateDebut");
    snDateDebut.addSNComposantListener(e -> snDateDebutValueChanged(e));
    add(snDateDebut,
        new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbAu ----
    lbAu.setText("au");
    lbAu.setPreferredSize(new Dimension(30, 30));
    lbAu.setMinimumSize(new Dimension(30, 30));
    lbAu.setHorizontalAlignment(SwingConstants.CENTER);
    lbAu.setMaximumSize(new Dimension(30, 30));
    lbAu.setName("lbAu");
    add(lbAu,
        new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- snDateFin ----
    snDateFin.setMaximumSize(new Dimension(126, 30));
    snDateFin.setName("snDateFin");
    snDateFin.addSNComposantListener(e -> snDateFinValueChanged(e));
    add(snDateFin,
        new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- btPlageSuivante ----
    btPlageSuivante.setFont(new Font("sansserif", Font.BOLD, 12));
    btPlageSuivante.setBackground(Color.white);
    btPlageSuivante.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    btPlageSuivante.setPreferredSize(new Dimension(30, 30));
    btPlageSuivante.setMinimumSize(new Dimension(30, 30));
    btPlageSuivante.setMaximumSize(new Dimension(30, 30));
    btPlageSuivante.setToolTipText("Mois suivant");
    btPlageSuivante.setIcon(new ImageIcon(getClass().getResource("/images/flecheSuivant.png")));
    btPlageSuivante.setBorderPainted(false);
    btPlageSuivante.setContentAreaFilled(false);
    btPlageSuivante.setVisible(false);
    btPlageSuivante.setName("btPlageSuivante");
    btPlageSuivante.addActionListener(e -> btPlageSuivanteActionPerformed(e));
    add(btPlageSuivante,
        new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JButton btPlagePrecedente;
  private SNLabelChamp lbDu2;
  private SNDate snDateDebut;
  private SNLabelChamp lbAu;
  private SNDate snDateFin;
  private JButton btPlageSuivante;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
