/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente;

import java.math.BigDecimal;
import java.util.List;

import com.google.gson.Gson;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.IdFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.IdTypeGratuit;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.EnumTypePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.ParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.ParametreChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.ParametreClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumModeTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.ParametreDocumentVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceCoefficient;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.EnumGraduationDecimaleTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libswing.composant.dialoguestandard.attente.SwingWorkerAttente;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.InterfaceSoumission;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;
import ri.serien.libswing.outil.clipboard.Clipboard;

/**
 * Modèle de la boîte de dialogue du détail du calcul d'un prix de vente.
 */
public class ModeleDialogueDetailPrixVente extends AbstractModeleDialogue {
  // Constantes
  private static final String TITRE = "Détail du calcul du prix de vente";
  
  // Variables
  private CalculPrixVente calculPrixVente = null;
  private CalculPrixVente calculPrixVenteInitial = null;
  private ParametreChargement parametreChargement = null;
  private EnumTypePrixVente typePrixVente = null;
  private ListeCleVueDetailPrixVente listeCleVue = new ListeCleVueDetailPrixVente();
  private boolean modeSimulation = false;
  private boolean prixHTVisible = false;
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur avec fourniture du calcul déjà effectué.
   *
   * Utiliser ce constructeur si on souhaite afficher le détail d'un calcul dont les données ont déjà été chargées. La boîte de dialoque
   * affiche simplement le résultat.
   *
   * Les données fournies sont clonées car la boîte de dialogue n'est pas modale et les données fournies peuvent évoluer en parallèle.
   *
   * @param pSession Session.
   * @param pCalculPrixVente Calcul d'un prix de vente à afficher.
   * @param pTypePrixVente Etape de prix à mettre en évidence.
   */
  public ModeleDialogueDetailPrixVente(SessionBase pSession, CalculPrixVente pCalculPrixVente, EnumTypePrixVente pTypePrixVente) {
    super(pSession);
    calculPrixVente = pCalculPrixVente.clone();
    typePrixVente = pTypePrixVente;
  }
  
  /**
   * Constructeur avec fourniture des paramètres pour faire le calcul.
   *
   * Utiliser ce constructeur si on souhaite afficher le détail d'un calcul mais que les données n'ont pas encore été chargée. La
   * boîte de dialogue va effectuer le chargement et le calcul pendant la phase de chargement des des données pui affichera le résulat
   * ensuite.
   *
   * Les données fournies sont clonées car la boîte de dialogue n'est pas modale et les données fournies peuvent évoluer en parallèle.
   *
   * @param pSession Session.
   * @param pCalculPrixVente Calcul d'un prix de vente à afficher.
   * @param pTypePrixVente Etape de prix à mettre en évidence.
   */
  public ModeleDialogueDetailPrixVente(SessionBase pSession, ParametreChargement pParametreChargement, EnumTypePrixVente pTypePrixVente) {
    super(pSession);
    parametreChargement = pParametreChargement.clone();
    typePrixVente = pTypePrixVente;
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards du modèle
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public void initialiserDonnees() {
    // Initialiser les vues de suite si on a déjà le calcul
    if (calculPrixVente != null) {
      // Créer les clés des vues à partir du calcul du tarif de vente
      listeCleVue.genererListeCleVue(calculPrixVente);
      
      // Définir la première vue comme vue active (normalement la vue synthèse)
      if (!listeCleVue.isEmpty()) {
        setCleVueEnfantActive(listeCleVue.valueOfByCode(0));
      }
    }
  }
  
  /**
   * Charge les données nécessaires.
   */
  @Override
  public void chargerDonnees() {
    // Charger les données si cela n'a pas encore été effectué
    if (calculPrixVente == null && parametreChargement != null) {
      // Lancer le calcul via un service qui s'occupe du chargement des données
      try {
        calculPrixVente = ManagerServiceArticle.calculerPrixVente(getIdSession(), parametreChargement);
      }
      catch (Exception e) {
        DialogueErreur.afficher(e);
      }
      
      // Créer les clés des vues à partir du calcul du tarif de vente
      listeCleVue.genererListeCleVue(calculPrixVente);
      
      // Définir la première vue comme vue active (normalement la vue synthèse)
      if (!listeCleVue.isEmpty()) {
        setCleVueEnfantActive(listeCleVue.valueOfByCode(0));
      }
    }
    // Refaire le calcul pour être sûr que tout est à jour si CalculPrixVente a été fourni en paramètre
    else if (calculPrixVente != null) {
      calculPrixVente.calculer();
    }
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Contrôleurs généraux
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Changer l'onglet actif.
   *
   * @param pIndiceOnglet Indice de l'onglet sélectionné.
   */
  public void changerOnglet(int pIndiceOnglet) {
    // Changer la vue active
    CleVueDetailPrixVente cleVue = listeCleVue.valueOfByCode(pIndiceOnglet);
    if (cleVue != null) {
      setCleVueEnfantActive(cleVue);
    }
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Copier les informations de stock détaillées dans le presse-papier.
   */
  public void copierPressePapier() {
    // Générer le texte à mettre dans le presse-papier
    String textePressePapier = "";
    if (calculPrixVente != null) {
      Gson gson = new Gson();
      textePressePapier = gson.toJson(calculPrixVente);
    }
    
    // Mettre à jour le presse-papier
    Clipboard.envoyerTexte(textePressePapier);
  }
  
  /**
   * Afficher ou masquer les prix HT (en mode TTC).
   * @param pSaisie true=afficher les prix HT, false=masquer les prix HT.
   */
  public void afficherPrixHT(boolean pSaisie) {
    // Tester si la valeur a changé
    if (pSaisie == prixHTVisible) {
      return;
    }
    
    // Modifier la valeur
    prixHTVisible = pSaisie;
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Activer ou désactiuver le mode simulation.
   *
   * @param pModeSimulation true=activer le mode simulation, false=désactiver le mode simulation.
   */
  public void activerModeSimulation(boolean pModeSimulation) {
    // Tester si la valeur a changé
    if (calculPrixVente == null || pModeSimulation == modeSimulation) {
      return;
    }
    
    // Modifier la valeur
    modeSimulation = pModeSimulation;
    
    // Sauver les paramètres actuels si le mode simulation commence
    if (modeSimulation) {
      calculPrixVenteInitial = calculPrixVente;
      calculPrixVente = calculPrixVenteInitial.clone();
    }
    // Restaurer les paramètres initiaux si le mode simulation se termine
    else {
      calculPrixVente = calculPrixVenteInitial;
      calculPrixVenteInitial = null;
    }
    
    // Lancer un nouveau calcul car la sérialisation/désarialisation a fait un clonage profond. C'est ce que l'on veut mais cela a
    // pour effet de créer autant d'instance d'objets qu'il y a d'attributs. Par exemple, l'objet ParametreGeneral est unique et partagé
    // par CalculPrixVente et toutes les FormulePrixVente avant le clonage. Après le clonage, il y a une instance pour CalculPrixVente
    // et des instances différentes pour chaque FormulePrixVente. Du coup, si on modifie une valeur, cela ne modifie pas l'instance
    // située dans CalculPrixVente.
    // Relancer un calcul remet les choses en ordre.
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Lancer le test de comparaison entre le calcul prix de vente Java et la calcul prix de vente RPG.
   * Les paramètres permetatnt de sélectionner les documents de ventes à tester sont en dur dans cette méthode.
   */
  public void testerCalculPrixVente() {
    // Limiter le nombre de calcul
    final int nombreCalculMax = 0;
    
    // Renseigner les critères de recherche des documents de vente à recalculer
    final CritereLigneVente critereLigneVente = new CritereLigneVente();
    critereLigneVente.setIdEtablissement(IdEtablissement.getInstance("OP "));
    critereLigneVente.setDateCreationDebut(ConvertDate.db2ToDate(1220615, null));
    critereLigneVente.setDateCreationFin(ConvertDate.db2ToDate(1220615, null));
    critereLigneVente.setPrixGaranti(false);
    critereLigneVente.ajouterTypeDocumentVente(EnumTypeDocumentVente.DEVIS);
    critereLigneVente.ajouterTypeDocumentVente(EnumTypeDocumentVente.COMMANDE);
    critereLigneVente.ajouterTypeDocumentVente(EnumTypeDocumentVente.BON);
    
    InterfaceSoumission interfaceSoumission = new InterfaceSoumission() {
      @Override
      public void executer() {
        // Lancer le test
        try {
          ManagerServiceArticle.testerCalculPrixVente(getIdSession(), critereLigneVente, null, nombreCalculMax);
        }
        catch (Exception e) {
          DialogueErreur.afficher(e);
        }
      }
    };
    
    SwingWorkerAttente.executer(null, interfaceSoumission, "Test calcul prix de vente en cours...");
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Contrôleurs paramètres établissement
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier le taux de TVA, dont l'indice est mentionné en paramètre, de l'établissement.
   * @param pSaisie Nouvelle valeur saisie.
   * @param pIndex Numéro du taux de TVA à modifier.
   */
  public void modifierTauxTVAEtablissement(String pSaisie, int pIndex) {
    // Convertir la valeur saisie
    BigPercentage tauxTVA = new BigPercentage(pSaisie, EnumFormatPourcentage.POURCENTAGE);
    
    // Tester si la valeur a changé
    ParametreEtablissement parametreEtablissement = getParametreEtablissement();
    if (parametreEtablissement == null || Constantes.equals(tauxTVA, parametreEtablissement.getTauxTVA(pIndex))) {
      return;
    }
    
    // Mettre à jour la valeur {
    parametreEtablissement.setTauxTVA(tauxTVA, pIndex);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le paramètre activant ou désactivant mode négoce.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierModeNegoce(boolean pSaisie) {
    // Tester si la valeur a changé
    ParametreEtablissement parametreEtablissement = getParametreEtablissement();
    if (parametreEtablissement == null || pSaisie == parametreEtablissement.isModeNegocePS287()) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreEtablissement.setModeNegocePS287(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la colonne tarif du client.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierColonneTarifPS105(Integer pSaisie) {
    // Tester si la valeur a changé
    ParametreEtablissement parametreEtablissement = getParametreEtablissement();
    if (parametreEtablissement == null || Constantes.equals(pSaisie, parametreEtablissement.getNumeroColonneTarifPS105())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreEtablissement.setNumeroColonneTarifPS105(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le paramètre activant la recherche de la première colonne de tarif non nulle.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierRechercheColonneTarifNonNulle(boolean pSaisie) {
    // Tester si la valeur a changé
    ParametreEtablissement parametreEtablissement = getParametreEtablissement();
    if (parametreEtablissement == null || pSaisie == parametreEtablissement.isRechercheColonneTarifNonNullePS305()) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreEtablissement.setRechercheColonneTarifNonNullePS305(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le paramètre activant ou désactivant mode négoce.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixPublicCommePrixBase(boolean pSaisie) {
    // Tester si la valeur a changé
    ParametreEtablissement parametreEtablissement = getParametreEtablissement();
    if (parametreEtablissement == null || pSaisie == parametreEtablissement.isPrixPublicCommePrixBasePS316()) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreEtablissement.setPrixPublicCommePrixBasePS316(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le type de gratuit de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierCNVArticleNonRemisable(String pSaisie) {
    // Tester si la valeur a changé
    ParametreEtablissement parametreEtablissement = getParametreEtablissement();
    if (parametreEtablissement == null || Constantes.equals(pSaisie, parametreEtablissement.getArticleNonRemisablePS281())) {
      return;
    }
    
    // Mettre à jour la valeur
    pSaisie = pSaisie.trim();
    if (pSaisie != null && pSaisie.length() >= 1) {
      parametreEtablissement.setArticleNonRemisablePS281(pSaisie.charAt(0));
    }
    else {
      parametreEtablissement.setArticleNonRemisablePS281(' ');
    }
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Contrôleurs paramètres client facturé (TODO ceux du client livré)
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier le paramètre activant ou désactivant mode TTC du client facturé.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierModeTTCClient(boolean pSaisie) {
    // Tester si la valeur a changé
    ParametreClient parametreClient = getParametreClientFacture();
    if (parametreClient == null || pSaisie == parametreClient.isModeTTC()) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreClient.setModeTTC(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la colonne tarif du client.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierColonneTarifClient(Integer pSaisie) {
    // Tester si la valeur a changé
    ParametreClient parametreClient = getParametreClientFacture();
    if (parametreClient == null || Constantes.equals(pSaisie, parametreClient.getNumeroColonneTarif())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreClient.setNumeroColonneTarif(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le taux de remise, dont l'indice est mentionné en paramètre, du client.
   * @param pSaisie Nouvelle valeur saisie.
   * @param pIndex Indice du taux de remise à modifier.
   */
  public void modifierTauxRemiseClient(String pSaisie, int pIndex) {
    // Convertir la valeur saisie
    BigPercentage tauxRemise = new BigPercentage(pSaisie, EnumFormatPourcentage.POURCENTAGE);
    
    // Tester si la valeur a changé
    ParametreClient parametreClient = getParametreClientFacture();
    if (parametreClient == null || Constantes.equals(tauxRemise, parametreClient.getTauxRemise(pIndex))) {
      return;
    }
    
    // Mettre à jour la valeur
    if (!tauxRemise.isZero()) {
      parametreClient.setTauxRemise(tauxRemise, pIndex);
    }
    else {
      parametreClient.setTauxRemise(null, pIndex);
    }
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Contrôleurs paramètres article
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier le numéro de TVA issu de l'établissement de l'article.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierNumeroTVAEtablissementArticle(Integer pSaisie) {
    // Tester si la valeur a changé
    ParametreArticle parametreArticle = getParametreArticle();
    if (parametreArticle == null || Constantes.equals(pSaisie, parametreArticle.getNumeroTVAEtablissement())) {
      return;
    }
    
    // Mettre à jour la provenance de la colonne tarif pour que la valeur soit prise en compte
    parametreArticle.setNumeroTVAEtablissement(pSaisie);
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier l'unité d evente de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierUniteVenteArticle(String pSaisie) {
    // Tester si la valeur a changé
    ParametreArticle parametreArticle = getParametreArticle();
    if (parametreArticle == null || Constantes.equals(pSaisie, parametreArticle.getCodeUniteVente())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreArticle.setCodeUniteVente(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier l'indicateur remisable de l'article.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierRemisableArticle(boolean pSaisie) {
    // Tester si la valeur a changé
    ParametreArticle parametreArticle = getParametreArticle();
    if (parametreArticle == null || pSaisie == parametreArticle.isRemisable()) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreArticle.setRemisable(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le PUMP de l'article
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPumpArticle(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreArticle parametreArticle = getParametreArticle();
    if (parametreArticle == null || Constantes.equals(pSaisie, parametreArticle.getPump())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreArticle.setPump(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le PRV de l'article
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrvArticle(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreArticle parametreArticle = getParametreArticle();
    if (parametreArticle == null || Constantes.equals(pSaisie, parametreArticle.getPrv())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreArticle.setPrv(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Contrôleurs paramètres tarif
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier la graduation décimale du tarif.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierGraduationDecimaleTarif(EnumGraduationDecimaleTarif pSaisie) {
    // Tester si la valeur a changé
    ParametreTarif parametreTarif = getParametreTarif();
    if (parametreTarif == null || Constantes.equals(pSaisie, parametreTarif.getGraduationDecimaleTarif())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreTarif.setGraduationDecimaleTarif(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le prix de base HT d'une colonne tarif.
   * @param pSaisie Nouvelle valeur saisie.
   * @param pIndex Indice de la colonne tarif à modifier (1 à 10).
   */
  public void modifierPrixBaseHTColonneTarif(BigDecimal pSaisie, int pIndex) {
    // Tester si la valeur a changé
    ParametreTarif parametreTarif = getParametreTarif();
    if (parametreTarif == null || Constantes.equals(pSaisie, parametreTarif.getPrixBaseHT(pIndex))) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreTarif.setPrixBaseHT(pSaisie, pIndex);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Contrôleurs paramètres condition de vente
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier la colonne tarif de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierColonneTarifConditionVente(Integer pSaisie) {
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (parametreConditionVente == null || Constantes.equals(pSaisie, parametreConditionVente.getNumeroColonneTarif())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreConditionVente.setNumeroColonneTarif(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le prix de base HT de la condition de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixBaseHTConditionVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (parametreConditionVente == null || Constantes.equals(pSaisie, parametreConditionVente.getPrixBaseHT())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreConditionVente.setPrixBaseHT(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier l'indicateur prix de base HT avec PUMP.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixBaseHTAvecPumpConditionVente(boolean pSaisie) {
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (parametreConditionVente == null || pSaisie == parametreConditionVente.isPrixBaseHTAvecPump()) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreConditionVente.setPrixBaseHTAvecPump(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier l'indicateur prix de base HT avec PRV.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixBaseHTAvecPrvConditionVente(boolean pSaisie) {
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (parametreConditionVente == null || pSaisie == parametreConditionVente.isPrixBaseHTAvecPrv()) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreConditionVente.setPrixBaseHTAvecPrv(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la remise en valeur de la condition de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierRemiseEnValeurConditionVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (parametreConditionVente == null || Constantes.equals(pSaisie, parametreConditionVente.getRemiseEnValeur())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreConditionVente.setRemiseEnValeur(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le taux de remise, dont l'indice est mentionné en paramètre, de la condition de vente.
   * @param pSaisie Nouvelle valeur saisie.
   * @param pIndex Indice du taux de remise à modifier.
   */
  public void modifierTauxRemiseConditionVente(String pSaisie, int pIndex) {
    // Convertir la valeur saisie
    BigPercentage tauxRemise = new BigPercentage(pSaisie, EnumFormatPourcentage.POURCENTAGE);
    
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (Constantes.equals(tauxRemise, parametreConditionVente.getTauxRemise(pIndex))) {
      return;
    }
    
    // Mettre à jour la valeur
    if (!tauxRemise.isZero()) {
      parametreConditionVente.setTauxRemise(tauxRemise, pIndex);
    }
    else {
      parametreConditionVente.setTauxRemise(null, pIndex);
    }
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le coefficient de la condition de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierCoefficientConditionVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (parametreConditionVente == null || Constantes.equals(pSaisie, parametreConditionVente.getCoefficient())) {
      return;
    }
    
    // Mettre à jour la valeur
    if (pSaisie != null) {
      parametreConditionVente.setCoefficient(pSaisie);
    }
    else {
      parametreConditionVente.setCoefficient(BigDecimal.ZERO);
    }
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la formule prix de la condition de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierFormulePrixConditionVente(IdFormulePrix pSaisie) {
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (parametreConditionVente == null || Constantes.equals(pSaisie, parametreConditionVente.getIdFormulePrix())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreConditionVente.setIdFormulePrix(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le prix net HT de la condition de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixNetHTConditionVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (parametreConditionVente == null || Constantes.equals(pSaisie, parametreConditionVente.getPrixNetHT())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreConditionVente.setPrixNetHT(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier l'indicateur gratuit de la condition de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierGratuitConditionVente(boolean pSaisie) {
    // Tester si la valeur a changé
    ParametreConditionVente parametreConditionVente = getParametreConditionVente();
    if (parametreConditionVente == null || pSaisie == parametreConditionVente.isGratuit()) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreConditionVente.setGratuit(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Contrôleurs paramètres chantier
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier l'unité de vente du chantier.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierUniteVenteChantier(String pSaisie) {
    // Tester si la valeur a changé
    ParametreChantier parametreChantier = getParametreChantier();
    if (parametreChantier == null || Constantes.equals(pSaisie, parametreChantier.getCodeUniteVente())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreChantier.setCodeUniteVente(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la colonne tarif du chantier.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierColonneTarifChantier(Integer pSaisie) {
    // Tester si la valeur a changé
    ParametreChantier parametreChantier = getParametreChantier();
    if (parametreChantier == null || Constantes.equals(pSaisie, parametreChantier.getNumeroColonneTarif())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreChantier.setNumeroColonneTarif(pSaisie);
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le prix de base HT du chantier.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixBaseHTChantier(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreChantier parametreChantier = getParametreChantier();
    if (parametreChantier == null || Constantes.equals(pSaisie, parametreChantier.getPrixBaseHT())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreChantier.setPrixBaseHT(pSaisie);
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le prix net HT du chantier.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixNetHTChantier(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreChantier parametreChantier = getParametreChantier();
    if (parametreChantier == null || Constantes.equals(pSaisie, parametreChantier.getPrixNetHT())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreChantier.setPrixNetHT(pSaisie);
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Contrôleurs paramètres document de vente
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier le paramètre activant ou désactivant mode TTC du document de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierModeTTCDocumentVente(boolean pSaisie) {
    // Tester si la valeur a changé
    ParametreDocumentVente parametreDocumentVente = getParametreDocumentVente();
    if (parametreDocumentVente == null || pSaisie == parametreDocumentVente.isModeTTC()) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreDocumentVente.setModeTTC(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le taux de TVA, dont l'indice est mentionné en paramètre, du document de vente.
   * @param pSaisie Nouvelle valeur saisie.
   * @param pIndex Numéro du taux de TVA à modifier.
   */
  public void modifierTauxTVADocumentVente(String pSaisie, int pIndex) {
    // Convertir la valeur saisie
    BigPercentage tauxTVA = new BigPercentage(pSaisie, EnumFormatPourcentage.POURCENTAGE);
    
    // Tester si la valeur a changé
    ParametreDocumentVente parametreDocumentVente = getParametreDocumentVente();
    if (parametreDocumentVente == null || Constantes.equals(tauxTVA, parametreDocumentVente.getTauxTVA(pIndex))) {
      return;
    }
    
    // Mettre à jour la valeur {
    parametreDocumentVente.setTauxTVA(tauxTVA, pIndex);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la colonne tarif du document de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierColonneTarifDocumentVente(Integer pSaisie) {
    // Tester si la valeur a changé
    ParametreDocumentVente parametreDocumentVente = getParametreDocumentVente();
    if (parametreDocumentVente == null || Constantes.equals(pSaisie, parametreDocumentVente.getNumeroColonneTarif())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreDocumentVente.setNumeroColonneTarif(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le taux de remise, dont l'indice est mentionné en paramètre, du document de vente.
   * @param pSaisie Nouvelle valeur saisie.
   * @param pIndex Indice du taux de remise à modifier.
   */
  public void modifierTauxRemiseDocumentVente(String pSaisie, int pIndex) {
    // Convertir la valeur saisie
    BigPercentage tauxRemise = new BigPercentage(pSaisie, EnumFormatPourcentage.POURCENTAGE);
    
    // Tester si la valeur a changé
    ParametreDocumentVente parametreDocumentVente = getParametreDocumentVente();
    if (parametreDocumentVente == null || Constantes.equals(tauxRemise, parametreDocumentVente.getTauxRemise(pIndex))) {
      return;
    }
    
    // Mettre à jour la valeur
    if (!tauxRemise.isZero()) {
      parametreDocumentVente.setTauxRemise(tauxRemise, pIndex);
    }
    else {
      parametreDocumentVente.setTauxRemise(null, pIndex);
    }
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le mode d'application des taux de remises du document de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierModeTauxRemiseDocumentVente(EnumModeTauxRemise pSaisie) {
    // Tester si la valeur a changé
    ParametreDocumentVente parametreDocumentVente = getParametreDocumentVente();
    if (parametreDocumentVente == null || Constantes.equals(pSaisie, parametreDocumentVente.getModeTauxRemise())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreDocumentVente.setModeTauxRemise(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier l'assiette des taux de remises du document de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierAssiettetauxRemiseDocumentVente(EnumAssietteTauxRemise pSaisie) {
    // Tester si la valeur a changé
    ParametreDocumentVente parametreDocumentVente = getParametreDocumentVente();
    if (parametreDocumentVente == null || Constantes.equals(pSaisie, parametreDocumentVente.getAssietteTauxRemise())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreDocumentVente.setAssietteTauxRemise(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Contrôleurs paramètres ligne de vente
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier le numéro de TVA issu de d'entête de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierNumeroTVAEnteteLigneVente(Integer pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getNumeroTVAEntete())) {
      return;
    }
    
    // Mettre à jour la provenance de la colonne tarif pour que la valeur soit prise en compte
    parametreLigneVente.setNumeroTVAEntete(pSaisie);
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le numéro de TVA issu de l'établissement de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierNumeroTVAEtablissementLigneVente(Integer pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getNumeroTVAEtablissement())) {
      return;
    }
    
    // Mettre à jour la provenance de la colonne tarif pour que la valeur soit prise en compte
    parametreLigneVente.setNumeroTVAEtablissement(pSaisie);
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier l'unité de vente de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierUniteVenteLigneVente(String pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getCodeUniteVente())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setCodeUniteVente(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la provenance de la colonne tarif de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierProvenanceColonneTarifLigneVente(EnumProvenanceColonneTarif pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getProvenanceColonneTarif())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setProvenanceColonneTarif(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la colonne tarif de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierColonneTarifLigneVente(Integer pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getNumeroColonneTarif())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setNumeroColonneTarif(pSaisie);
    
    // Mettre à jour la provenance de la colonne tarif pour que la valeur soit prise en compte
    if (parametreLigneVente.getNumeroColonneTarif() != null && parametreLigneVente.getNumeroColonneTarif() != 0) {
      parametreLigneVente.setProvenanceColonneTarif(EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE);
    }
    else {
      parametreLigneVente.setProvenanceColonneTarif(null);
    }
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la provenance du prix de base de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierProvenancePrixBaseLigneVente(EnumProvenancePrixBase pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getProvenancePrixBase())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setProvenancePrixBase(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le prix de base HT de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixBaseHTLigneVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getPrixBaseHT())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setPrixBaseHT(pSaisie);
    
    // Mettre à jour la provenance du prix de base HT pour que la valeur soit prise en compte
    if (parametreLigneVente.getPrixBaseHT() != null) {
      parametreLigneVente.setProvenancePrixBase(EnumProvenancePrixBase.SAISIE_LIGNE_VENTE);
    }
    else {
      parametreLigneVente.setProvenancePrixBase(null);
    }
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le prix de base TTC de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixBaseTTCLigneVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getPrixBaseTTC())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setPrixBaseTTC(pSaisie);
    
    // Mettre à jour la provenance du prix de base HT pour que la valeur soit prise en compte
    if (parametreLigneVente.getPrixBaseTTC() != null) {
      parametreLigneVente.setProvenancePrixBase(EnumProvenancePrixBase.SAISIE_LIGNE_VENTE);
    }
    else {
      parametreLigneVente.setProvenancePrixBase(null);
    }
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la provenance des taux de remises de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierProvenanceTauxRemiseLigneVente(EnumProvenanceTauxRemise pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getProvenanceTauxRemise())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setProvenanceTauxRemise(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le taux de remise, dont l'indice est mentionné en paramètre, de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   * @param pIndex Indice du taux de remise à modifier.
   */
  public void modifierTauxRemiseLigneVente(String pSaisie, int pIndex) {
    // Convertir la valeur saisie
    BigPercentage tauxRemise = new BigPercentage(pSaisie, EnumFormatPourcentage.POURCENTAGE);
    
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(tauxRemise, parametreLigneVente.getTauxRemise(pIndex))) {
      return;
    }
    
    // Mettre à jour la valeur
    if (!tauxRemise.isZero()) {
      parametreLigneVente.setTauxRemise(tauxRemise, pIndex);
    }
    else {
      parametreLigneVente.setTauxRemise(null, pIndex);
    }
    parametreLigneVente.setProvenanceTauxRemise(EnumProvenanceTauxRemise.SAISIE_LIGNE_VENTE);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le mode d'application des taux de remises de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierModeTauxRemiseLigneVente(EnumModeTauxRemise pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getModeTauxRemise())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setModeTauxRemise(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier l'assiette des taux de remises du document de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierAssietteTauxRemiseLigneVente(EnumAssietteTauxRemise pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getAssietteTauxRemise())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setAssietteTauxRemise(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la provenance du coefficient de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierProvenanceCoefficientLigneVente(EnumProvenanceCoefficient pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getProvenanceCoefficient())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setProvenanceCoefficient(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le coefficient de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierCoefficientLigneVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getCoefficient())) {
      return;
    }
    
    // Mettre à jour la valeur
    if (pSaisie != null) {
      parametreLigneVente.setCoefficient(pSaisie);
    }
    else {
      parametreLigneVente.setCoefficient(BigDecimal.ZERO);
    }
    
    // Mettre à jour la provenance
    if (getParametreLigneVenteInitial() != null
        && Constantes.equals(parametreLigneVente.getCoefficient(), getParametreLigneVenteInitial().getCoefficient())) {
      parametreLigneVente.setProvenanceCoefficient(getParametreLigneVenteInitial().getProvenanceCoefficient());
    }
    else {
      parametreLigneVente.setProvenanceCoefficient(EnumProvenanceCoefficient.SAISIE_LIGNE_VENTE);
    }
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le type de gratuit de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierTypeGratuitLigneVente(String pSaisie) {
    // Transformer la valeur saisie en identifiant de type de gratuit
    IdTypeGratuit idTypeGratuit = null;
    if (pSaisie != null && pSaisie.length() >= 1) {
      idTypeGratuit = IdTypeGratuit.getInstance(pSaisie.charAt(0));
    }
    else {
      idTypeGratuit = IdTypeGratuit.NON_GRATUIT;
    }
    
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(idTypeGratuit, parametreLigneVente.getIdTypeGratuit())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setIdTypeGratuit(idTypeGratuit);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la provenance du prix net de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierProvenancePrixNetLigneVente(EnumProvenancePrixNet pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getProvenancePrixNet())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setProvenancePrixNet(pSaisie);
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le prix net HT de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixNetHTLigneVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getPrixNetSaisiHT())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setPrixNetSaisiHT(pSaisie);
    
    // Mettre à jour la provenance du prix de base HT pour que la valeur soit prise en compte
    if (parametreLigneVente.getPrixNetSaisiHT() != null) {
      parametreLigneVente.setProvenancePrixNet(EnumProvenancePrixNet.SAISIE_LIGNE_VENTE);
    }
    else {
      parametreLigneVente.setProvenancePrixNet(null);
    }
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le prix net TTC de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierPrixNetTTCLigneVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getPrixNetSaisiTTC())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setPrixNetSaisiTTC(pSaisie);
    
    // Mettre à jour la provenance du prix de base HT pour que la valeur soit prise en compte
    if (parametreLigneVente.getPrixNetSaisiTTC() != null) {
      parametreLigneVente.setProvenancePrixNet(EnumProvenancePrixNet.SAISIE_LIGNE_VENTE);
    }
    else {
      parametreLigneVente.setProvenancePrixNet(null);
    }
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la quantité en UV de la ligne de vente.
   * @param pSaisie Nouvelle valeur saisie.
   */
  public void modifierQuantiteUVLigneVente(BigDecimal pSaisie) {
    // Tester si la valeur a changé
    ParametreLigneVente parametreLigneVente = getParametreLigneVente();
    if (parametreLigneVente == null || Constantes.equals(pSaisie, parametreLigneVente.getQuantiteUV())) {
      return;
    }
    
    // Mettre à jour la valeur
    parametreLigneVente.setQuantiteUV(pSaisie);
    
    // Relancer le calcul
    calculer();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Calculer le prix de façon sécurisée.
   * Si une erreur se produit lors du calcul du prix, l'erreur est capturée et affichée dans une boîte de dialogue. Le reste des
   * opérations peut ensuite se poursuivre normalement, dont le rafraichissement de l'affichage.
   */
  private void calculer() {
    try {
      calculPrixVente.calculer();
    }
    catch (Exception e) {
      DialogueErreur.afficher(e);
    }
  }
  
  /**
   * Retourner la formule de prix de vente correspondant à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * La formule prix de vente retournée dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne null.
   * - Si la vue active est l'onglet d'une formule, cela retourne la FormulePrixVente associée à l'onglet.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Formule prix de vente associée à la vue active.
   */
  private FormulePrixVente getFormulePrixVenteVueActive(CalculPrixVente pCalculPrixVente) {
    // Tester s'il y a un calcul prix de vente
    if (pCalculPrixVente == null) {
      return null;
    }
    
    // Récupérer l'interface de clé de la vue active
    InterfaceCleVue interfaceCleVue = getCleVueEnfantActive();
    if (interfaceCleVue == null) {
      return null;
    }
    
    // Récupérer l'index de la vue active
    // On retire 1 car l'index 0 est occupé par la vue synthèse (qui devient donc l'index -1).
    // Du coup, l'index correspond à l'index de la FormulePrixVente dans la liste.
    int index = interfaceCleVue.getIndex() - 1;
    
    // Tester si l'index est celui de la vue synthèse
    if (index < 0) {
      return null;
    }
    
    // Tester si l'index est plus grand que le nombre de formules
    List<FormulePrixVente> listeFormulePrixVente = pCalculPrixVente.getListeFormulePrixVente();
    if (index >= listeFormulePrixVente.size()) {
      return null;
    }
    
    // Retourner la formule associée à l'index
    return listeFormulePrixVente.get(index);
  }
  
  /**
   * Retourner les paramètres établissement associés à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * Les paramètres retournés dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne les paramètres de CalcuLPrixVente.
   * - Si la vue active est le détail d'une formule, cela retourne les paramètres de FormulePrixVente.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Paramètres établissement.
   */
  public ParametreEtablissement getParametreEtablissementVueActive(CalculPrixVente pCalculPrixVente) {
    FormulePrixVente formulePrixVente = getFormulePrixVenteVueActive(pCalculPrixVente);
    if (formulePrixVente != null) {
      return formulePrixVente.getParametreEtablissement();
    }
    else if (calculPrixVente != null) {
      return calculPrixVente.getParametreEtablissement();
    }
    return null;
  }
  
  /**
   * Retourner les paramètres client livrés associés à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * Les paramètres retournés dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne les paramètres de CalcuLPrixVente.
   * - Si la vue active est le détail d'une formule, cela retourne les paramètres de FormulePrixVente.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Paramètres client.
   */
  private ParametreClient getParametreClientLivreVueActive(CalculPrixVente pCalculPrixVente) {
    FormulePrixVente formulePrixVente = getFormulePrixVenteVueActive(pCalculPrixVente);
    if (formulePrixVente != null) {
      return formulePrixVente.getParametreClientLivre();
    }
    else if (calculPrixVente != null) {
      return calculPrixVente.getParametreClientLivre();
    }
    return null;
  }
  
  /**
   * Retourner les paramètres client facturé associés à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * Les paramètres retournés dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne les paramètres de CalcuLPrixVente.
   * - Si la vue active est le détail d'une formule, cela retourne les paramètres de FormulePrixVente.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Paramètres client.
   */
  private ParametreClient getParametreClientFactureVueActive(CalculPrixVente pCalculPrixVente) {
    FormulePrixVente formulePrixVente = getFormulePrixVenteVueActive(pCalculPrixVente);
    if (formulePrixVente != null) {
      return formulePrixVente.getParametreClientFacture();
    }
    else if (calculPrixVente != null) {
      return calculPrixVente.getParametreClientFacture();
    }
    return null;
  }
  
  /**
   * Retourner les paramètres article associés à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * Les paramètres retournés dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne les paramètres de CalcuLPrixVente.
   * - Si la vue active est le détail d'une formule, cela retourne les paramètres de FormulePrixVente.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Paramètres article.
   */
  private ParametreArticle getParametreArticleVueActive(CalculPrixVente pCalculPrixVente) {
    FormulePrixVente formulePrixVente = getFormulePrixVenteVueActive(pCalculPrixVente);
    if (formulePrixVente != null) {
      return formulePrixVente.getParametreArticle();
    }
    else if (calculPrixVente != null) {
      return calculPrixVente.getParametreArticle();
    }
    return null;
  }
  
  /**
   * Retourner les paramètres tarif associés à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * Les paramètres retournés dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne les paramètres de CalcuLPrixVente.
   * - Si la vue active est le détail d'une formule, cela retourne les paramètres de FormulePrixVente.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Paramètres tarif.
   */
  public ParametreTarif getParametreTarifVueActive(CalculPrixVente pCalculPrixVente) {
    FormulePrixVente formulePrixVente = getFormulePrixVenteVueActive(pCalculPrixVente);
    if (formulePrixVente != null) {
      return formulePrixVente.getParametreTarif();
    }
    else if (calculPrixVente != null) {
      return calculPrixVente.getParametreTarif();
    }
    return null;
  }
  
  /**
   * Retourner les paramètres condition de vente associés à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * Les paramètres retournés dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne null.
   * - Si la vue active est le détail d'une formule, cela retourne les paramètres de FormulePrixVente.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Paramètres condition de vente.
   */
  private ParametreConditionVente getParametreConditionVenteVueActive(CalculPrixVente pCalculPrixVente) {
    FormulePrixVente formulePrixVente = getFormulePrixVenteVueActive(pCalculPrixVente);
    if (formulePrixVente != null) {
      return formulePrixVente.getParametreConditionVente();
    }
    return null;
  }
  
  /**
   * Retourner les paramètres chantier associés à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * Les paramètres retournés dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne null.
   * - Si la vue active est le détail d'une formule, cela retourne les paramètres de FormulePrixVente.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Paramètres chantier.
   */
  private ParametreChantier getParametreChantierVueActive(CalculPrixVente pCalculPrixVente) {
    FormulePrixVente formulePrixVente = getFormulePrixVenteVueActive(pCalculPrixVente);
    if (formulePrixVente != null) {
      return formulePrixVente.getParametreChantier();
    }
    return null;
  }
  
  /**
   * Retourner les paramètres document de vente associés à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * Les paramètres retournés dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne les paramètres de CalcuLPrixVente.
   * - Si la vue active est le détail d'une formule, cela retourne les paramètres de FormulePrixVente.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Paramètres document de vente.
   */
  private ParametreDocumentVente getParametreDocumentVenteVueActive(CalculPrixVente pCalculPrixVente) {
    FormulePrixVente formulePrixVente = getFormulePrixVenteVueActive(pCalculPrixVente);
    if (formulePrixVente != null) {
      return formulePrixVente.getParametreDocumentVente();
    }
    else if (calculPrixVente != null) {
      return calculPrixVente.getParametreDocumentVente();
    }
    return null;
  }
  
  /**
   * Retourner les paramètres ligne de vente associés à la vue active pour le calcul prix de vente fourni en paramètre.
   *
   * Les paramètres retournés dépendent de la vue active :
   * - Si la vue active est l'onglet synthèse, cela retourne les paramètres de CalcuLPrixVente.
   * - Si la vue active est le détail d'une formule, cela retourne les paramètres de FormulePrixVente.
   *
   * @param pCalculPrixVente Calcul prix de vente en cours ou sa sauvegarde.
   * @return Paramètres ligne de vente.
   */
  private ParametreLigneVente getParametreLigneVenteVueActive(CalculPrixVente pCalculPrixVente) {
    FormulePrixVente formulePrixVente = getFormulePrixVenteVueActive(pCalculPrixVente);
    if (formulePrixVente != null) {
      return formulePrixVente.getParametreLigneVente();
    }
    else if (calculPrixVente != null) {
      return calculPrixVente.getParametreLigneVente();
    }
    return null;
  }
  
  // ------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Titre de la boite de dialogue.
   * @return Titre.
   */
  public String getTitre() {
    String titre = TITRE;
    
    // Vérifier la présence d'un calcul prix de vente
    if (calculPrixVente != null && calculPrixVente.getParametreClientFacture() != null
        && calculPrixVente.getParametreClientFacture().getIdClient() != null) {
      titre += ", client " + calculPrixVente.getParametreClientFacture().getIdClient();
    }
    
    // Vérifier la présence d'un calcul prix de vente
    if (calculPrixVente != null && calculPrixVente.getParametreArticle() != null
        && calculPrixVente.getParametreArticle().getCodeArticle() != null) {
      titre += ", article " + calculPrixVente.getParametreArticle().getCodeArticle();
    }
    
    return titre;
  }
  
  /**
   * Retourner le calcul du prix de vente.
   * @return Calcul du prix de vente.
   */
  public CalculPrixVente getCalculPrixVente() {
    return calculPrixVente;
  }
  
  /**
   * Indique si le mode simulation est actif ou pas.
   * @return true= mode simulation actif, false=mode simulation inactif.
   */
  public boolean isModeSimulation() {
    return modeSimulation;
  }
  
  /**
   * Indique si les prix HT sont affichés..
   * @return true=prix HT affichés, false=prix HT masqués.
   */
  public boolean isPrixHTVisible() {
    return prixHTVisible;
  }
  
  /**
   * Retourne le type du prix de vente référencé par ce composant.
   *
   * @return
   */
  public EnumTypePrixVente getTypePrixVente() {
    return typePrixVente;
  }
  
  /**
   * Retourne le calcul du prix de vente final.
   *
   * @return
   */
  public FormulePrixVente getFormulePrixVenteFinal() {
    if (calculPrixVente == null) {
      return null;
    }
    return calculPrixVente.getFormulePrixVenteFinale();
  }
  
  /**
   * Liste de clés des vues de la boîte de dialogue.
   * @return Liste de clés vues.
   */
  public ListeCleVueDetailPrixVente getListeCleVue() {
    return listeCleVue;
  }
  
  /**
   * Retourner la liste des formules prix de vente calculées pendant ce calcul.
   * @return Liste de formules prix de vente.
   */
  public List<FormulePrixVente> getListeFormulePrixVente() {
    if (calculPrixVente == null) {
      return null;
    }
    return calculPrixVente.getListeFormulePrixVente();
  }
  
  /**
   * Retourner la formule de prix de vente correspondant à la vue active.
   * @return Formule prix de vente
   */
  public FormulePrixVente getFormulePrixVente() {
    return getFormulePrixVenteVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde de la formule de prix de vente correspondant à la vue active
   * @return Formule prix de vente
   */
  public FormulePrixVente getFormulePrixVenteInitial() {
    return getFormulePrixVenteVueActive(calculPrixVenteInitial);
  }
  
  /**
   * Retourner les paramètres établissement associés à la vue active.
   * @return Paramètres établissement.
   */
  public ParametreEtablissement getParametreEtablissement() {
    return getParametreEtablissementVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde des paramètres établissement associés à la vue active.
   * @return Paramètres établissement.
   */
  public ParametreEtablissement getParametreEtablissementInitial() {
    return getParametreEtablissementVueActive(calculPrixVenteInitial);
  }
  
  /**
   * Retourner les paramètres client livré associés à la vue active.
   * @return Paramètres client livré.
   */
  public ParametreClient getParametreClientLivre() {
    return getParametreClientLivreVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde des paramètres client livré associés à la vue active.
   * @return Paramètres client livré.
   */
  public ParametreClient getParametreClientLivreInitial() {
    return getParametreClientLivreVueActive(calculPrixVenteInitial);
  }
  
  /**
   * Retourner les paramètres client facturé associés à la vue active.
   * @return Paramètres client facturé.
   */
  public ParametreClient getParametreClientFacture() {
    return getParametreClientFactureVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde des paramètres client facturé associés à la vue active.
   * @return Paramètres client facturé.
   */
  public ParametreClient getParametreClientFactureInitial() {
    return getParametreClientFactureVueActive(calculPrixVenteInitial);
  }
  
  /**
   * Retourner les paramètres article associés à la vue active.
   * @return Paramètres article.
   */
  public ParametreArticle getParametreArticle() {
    return getParametreArticleVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde des paramètres article associés à la vue active.
   * @return Paramètres article.
   */
  public ParametreArticle getParametreArticleInitial() {
    return getParametreArticleVueActive(calculPrixVenteInitial);
  }
  
  /**
   * Retourner les paramètres tarif associés à la vue active.
   * @return Paramètres tarif.
   */
  public ParametreTarif getParametreTarif() {
    return getParametreTarifVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde des paramètres tarif associés à la vue active.
   * @return Paramètres tarif.
   */
  public ParametreTarif getParametreTarifInitial() {
    return getParametreTarifVueActive(calculPrixVenteInitial);
  }
  
  /**
   * Retourner les paramètres condition de vente associés à la vue active.
   * @return Paramètres condition de vente.
   */
  public ParametreConditionVente getParametreConditionVente() {
    return getParametreConditionVenteVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde des paramètres condition de vente associés à la vue active.
   * @return Paramètres condition de vente.
   */
  public ParametreConditionVente getParametreConditionVenteInitial() {
    return getParametreConditionVenteVueActive(calculPrixVenteInitial);
  }
  
  /**
   * Retourner les paramètres chantier associés à la vue active.
   * @return Paramètres condition de vente.
   */
  public ParametreChantier getParametreChantier() {
    return getParametreChantierVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde des paramètres chantier associés à la vue active.
   * @return Paramètres condition de vente.
   */
  public ParametreChantier getParametreChantierInitial() {
    return getParametreChantierVueActive(calculPrixVenteInitial);
  }
  
  /**
   * Retourner les paramètres document de vente associés à la vue active.
   * @return Paramètres document de vente.
   */
  public ParametreDocumentVente getParametreDocumentVente() {
    return getParametreDocumentVenteVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde des paramètres document de vente associés à la vue active.
   * @return Paramètres document de vente.
   */
  public ParametreDocumentVente getParametreDocumentVenteInitial() {
    return getParametreDocumentVenteVueActive(calculPrixVenteInitial);
  }
  
  /**
   * Retourner les paramètres ligne de vente associés à la vue active.
   * @return Paramètres ligne de vente.
   */
  public ParametreLigneVente getParametreLigneVente() {
    return getParametreLigneVenteVueActive(calculPrixVente);
  }
  
  /**
   * Retourner la sauvegarde des paramètres ligne de vente associés à la vue active.
   * @return Paramètres ligne de vente.
   */
  public ParametreLigneVente getParametreLigneVenteInitial() {
    return getParametreLigneVenteVueActive(calculPrixVenteInitial);
  }
}
