/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.client.snclient;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.outils.Constantes;

public class JTableClientRenderer extends DefaultTableCellRenderer {
  // Variable
  private Color couleurOriginale = null;
  
  /**
   * Retourne la cellule.
   */
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    final JLabel cellule = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

    if (couleurOriginale == null) {
      couleurOriginale = cellule.getBackground();
    }
    
    // Colorier les ligne si client comptant
    if (!isSelected) {
      cellule.setForeground(Color.BLACK);
      String valeur = (String) table.getValueAt(row, 0);
      if ((valeur != null) && valeur.contains(EnumTypeCompteClient.COMPTANT.getLibelle())) {
        cellule.setBackground(Constantes.COULEUR_LISTE_FOND_COMMENTAIRE);
      }
      else if ((valeur != null) && valeur.contains(EnumTypeCompteClient.PROSPECT.getLibelle())) {
        cellule.setBackground(Constantes.COULEUR_LISTE_FOND_PROSPECT);
      }
      else {
        cellule.setBackground(couleurOriginale);
      }
    }
    return cellule;
  }
}
