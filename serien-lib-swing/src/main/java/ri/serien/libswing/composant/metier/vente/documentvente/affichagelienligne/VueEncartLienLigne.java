/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.affichagelienligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.commun.lienligne.EnumTypeLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;

/**
 * Encart représentant une ligne liée à un document.
 */
public class VueEncartLienLigne extends SNPanel {
  // Variables
  LienLigne lienLigne = null;
  ListeUnite listeUnite = null;
  boolean isEnCompte = false;
  
  public VueEncartLienLigne(LienLigne pLienLigne, ListeUnite pListeUnite) {
    initComponents();
    setName("VueEncartLienLigne");
    lienLigne = pLienLigne;
    listeUnite = pListeUnite;
  }
  
  public void rafraichir() {
    rafraichirTitre();
    rafraichirRaisonSociale();
    rafraichirQuantite();
    rafraichirStatut();
  }
  
  public EnumTypeLienLigne getTypeLienLigne() {
    return lienLigne.getId().getType();
  }
  
  private void rafraichirTitre() {
    if (lienLigne != null) {
      switch (lienLigne.getId().getType()) {
        case LIEN_LIGNE_VENTE:
          String message = lienLigne.getDocumentVente().getTypeDocumentVente().getLibelle() + " de ventes "
              + lienLigne.getDocumentVente().getId().toString();
          if (lienLigne.getDocumentVente().isCommande()) {
            message += " du " + Constantes.convertirDateEnTexte(lienLigne.getDocumentVente().getDateValidationCommande());
          }
          else if (lienLigne.getDocumentVente().isFacture()) {
            if (isEnCompte) {
              if (lienLigne.getId().getNumeroDocument() != null && lienLigne.getId().getNumeroDocument() > 0) {
                message = "Bon de vente " + lienLigne.getId().getNumeroDocument() + "-" + lienLigne.getId().getSuffixeDocument();
                message += " du " + Constantes.convertirDateEnTexte(lienLigne.getDocumentVente().getDateDocument());
              }
              else {
                message = "Bon de vente " + ((LigneVente) lienLigne.getLigne()).getId().getNumero() + "-"
                    + ((LigneVente) lienLigne.getLigne()).getId().getSuffixe();
                message += " du " + Constantes.convertirDateEnTexte(lienLigne.getDocumentVente().getDateDocument());
              }
            }
            else {
              message = "Facture " + lienLigne.getDocumentVente().getId().getNumeroFacture();
              message += " du " + Constantes.convertirDateEnTexte(lienLigne.getDocumentVente().getDateFacturation());
            }
          }
          else if (lienLigne.getDocumentVente().isBon()) {
            message += " du " + Constantes.convertirDateEnTexte(lienLigne.getDocumentVente().getDateCreation());
          }
          lbTitre.setText(message);
          break;
        
        case LIEN_LIGNE_ACHAT:
          if (lienLigne.getDocumentAchat().isFacture()) {
            lbTitre.setText("Facture d'achats " + lienLigne.getDocumentAchat().getId() + " du "
                + Constantes.convertirDateEnTexte(lienLigne.getDocumentAchat().getDateCreation()));
          }
          else if (lienLigne.getDocumentAchat().isCommande()) {
            Date dateAffichageCommande = lienLigne.getDocumentAchat().getDateHomologation();
            if (dateAffichageCommande == null) {
              dateAffichageCommande = lienLigne.getDocumentAchat().getDateCreation();
            }
            lbTitre.setText("Commande d'achats " + lienLigne.getDocumentAchat().getId() + " du "
                + Constantes.convertirDateEnTexte(dateAffichageCommande));
          }
          else if (lienLigne.getDocumentAchat().isReception()) {
            Date dateAffichageCommande = lienLigne.getDocumentAchat().getDateHomologation();
            if (dateAffichageCommande == null) {
              dateAffichageCommande = lienLigne.getDocumentAchat().getDateCreation();
            }
            lbTitre.setText("Réception d'achats " + lienLigne.getDocumentAchat().getId() + " du "
                + Constantes.convertirDateEnTexte(dateAffichageCommande));
          }
          break;
        
        default:
          lbTitre.setText("Impossible de charger le lien");
          break;
      }
    }
    else {
      lbTitre.setText("Impossible de charger le lien");
    }
  }
  
  private void rafraichirRaisonSociale() {
    if (lienLigne != null) {
      switch (lienLigne.getId().getType()) {
        case LIEN_LIGNE_VENTE:
          if (lienLigne.getDocumentVente().getAdresseFacturation().getIdCivilite() != null) {
            lbClient.setText(lienLigne.getDocumentVente().getAdresseFacturation().getIdCivilite().toString()
                + lienLigne.getDocumentVente().getAdresseFacturation().getNom());
          }
          else {
            lbClient.setText(lienLigne.getDocumentVente().getAdresseFacturation().getNom());
          }
          break;
        
        case LIEN_LIGNE_ACHAT:
          lbClient.setText(lienLigne.getDocumentAchat().getAdresseFournisseur().getNom());
          break;
        default:
          lbClient.setText("");
          break;
      }
    }
    else {
      lbClient.setText("");
    }
  }
  
  private void rafraichirQuantite() {
    String quantite = "";
    if (lienLigne != null) {
      switch (lienLigne.getId().getType()) {
        case LIEN_LIGNE_VENTE:
          LigneVente ligneVente = (LigneVente) lienLigne.getLigne();
          
          // Ajouter la quantité en UCV
          if (ligneVente.isArticleDecoupable()) {
            quantite = Constantes.convertirIntegerEnTexte(ligneVente.getNombreDecoupe(), 0);
          }
          else {
            BigDecimal conditionnement = ligneVente.getNombreUVParUCV();
            if (conditionnement == null || conditionnement.compareTo(BigDecimal.ZERO) == 0) {
              conditionnement = BigDecimal.ONE;
            }
            if (lienLigne.getDocumentVente().isCommande() && lienLigne.getId().getSuffixeDocument() == 0) {
              quantite = Constantes.formater(ligneVente.getQuantiteDocumentOrigineUC(), false);
            }
            else {
              quantite = Constantes.formater(ligneVente.getQuantiteUV().divide(conditionnement), false);
            }
          }
          
          // Ajouter l'unité
          quantite += " " + ligneVente.getIdUniteConditionnementVente().getCode();
          break;
        
        case LIEN_LIGNE_ACHAT:
          LigneAchatArticle ligneAchat = (LigneAchatArticle) lienLigne.getLigne();
          if (ligneAchat == null) {
            return;
          }
          if (lienLigne.getDocumentAchat().isCommande()) {
            quantite = Constantes.formater(lienLigne.calculerQuantiteOrigine(), false);
          }
          else {
            quantite = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUCA(), false);
          }
          quantite += " " + ligneAchat.getPrixAchat().getIdUCA().getCode();
          break;
        
        default:
          break;
      }
    }
    
    lbQuantite.setText(quantite);
  }
  
  private void rafraichirStatut() {
    List<Message> listeMessage = lienLigne.getStatut();
    
    if (listeMessage.size() >= 1) {
      Message message = listeMessage.get(0);
      lbStatut1.setText(message.getTexte());
      if (message.isImportanceHaute()) {
        lbStatut1.setForeground(Color.RED);
      }
      else {
        lbStatut1.setForeground(Color.BLACK);
      }
    }
    else {
      lbStatut1.setText("");
    }
    
    if (listeMessage.size() >= 2) {
      Message message = listeMessage.get(1);
      lbStatut2.setText(message.getTexte());
      if (message.isImportanceHaute()) {
        lbStatut2.setForeground(Color.RED);
      }
      else {
        lbStatut2.setForeground(Color.BLACK);
      }
    }
    else {
      lbStatut2.setText("");
    }
    
    if (listeMessage.size() >= 3) {
      Message message = listeMessage.get(2);
      lbStatut3.setText(message.getTexte());
      if (message.isImportanceHaute()) {
        lbStatut3.setForeground(Color.RED);
      }
      else {
        lbStatut3.setForeground(Color.BLACK);
      }
    }
    else {
      lbStatut3.setText("");
    }
  }
  
  public boolean isCommande() {
    if (lienLigne.getDocumentVente() != null) {
      return lienLigne.getDocumentVente().isCommande();
    }
    else if (lienLigne.getDocumentAchat() != null) {
      return lienLigne.getDocumentAchat().isCommande();
    }
    return false;
  }
  
  public boolean isFacture() {
    if (lienLigne.getDocumentVente() != null) {
      return lienLigne.getDocumentVente().isFacture();
    }
    else if (lienLigne.getDocumentAchat() != null) {
      return lienLigne.getDocumentAchat().isFacture();
    }
    return false;
  }
  
  public boolean isNonDefini() {
    if (lienLigne.getDocumentVente() != null && lienLigne.getDocumentVente().getTypeDocumentVente() != null) {
      return lienLigne.getDocumentVente().getTypeDocumentVente().equals(EnumTypeDocumentVente.NON_DEFINI);
    }
    return false;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlEncart = new SNPanelContenu();
    lbTitre = new JLabel();
    lbClient = new JLabel();
    lbQuantite = new JLabel();
    lbStatut1 = new JTextPane();
    lbStatut2 = new JTextPane();
    lbStatut3 = new JTextPane();
    
    // ======== this ========
    setBorder(new CompoundBorder(new EtchedBorder(), new EmptyBorder(5, 5, 5, 5)));
    setMinimumSize(new Dimension(300, 130));
    setPreferredSize(new Dimension(300, 130));
    setBackground(Color.white);
    setMaximumSize(new Dimension(300, 130));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlEncart ========
    {
      pnlEncart.setBackground(Color.white);
      pnlEncart.setOpaque(true);
      pnlEncart.setBorder(new EmptyBorder(1, 1, 1, 1));
      pnlEncart.setName("pnlEncart");
      pnlEncart.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlEncart.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlEncart.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlEncart.getLayout()).columnWeights = new double[] { 0.5, 1.0E-4 };
      ((GridBagLayout) pnlEncart.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
      
      // ---- lbTitre ----
      lbTitre.setMaximumSize(new Dimension(150, 18));
      lbTitre.setMinimumSize(new Dimension(150, 18));
      lbTitre.setOpaque(true);
      lbTitre.setBackground(Color.white);
      lbTitre.setHorizontalAlignment(SwingConstants.LEADING);
      lbTitre.setFont(new Font("sansserif", Font.BOLD, 13));
      lbTitre.setPreferredSize(new Dimension(150, 18));
      lbTitre.setName("lbTitre");
      pnlEncart.add(lbTitre,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
      
      // ---- lbClient ----
      lbClient.setPreferredSize(new Dimension(150, 18));
      lbClient.setMinimumSize(new Dimension(150, 18));
      lbClient.setFont(new Font("sansserif", Font.PLAIN, 13));
      lbClient.setMaximumSize(new Dimension(150, 18));
      lbClient.setName("lbClient");
      pnlEncart.add(lbClient,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
      
      // ---- lbQuantite ----
      lbQuantite.setMinimumSize(new Dimension(150, 18));
      lbQuantite.setPreferredSize(new Dimension(150, 18));
      lbQuantite.setFont(new Font("sansserif", Font.BOLD, 13));
      lbQuantite.setMaximumSize(new Dimension(150, 18));
      lbQuantite.setName("lbQuantite");
      pnlEncart.add(lbQuantite,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
      
      // ---- lbStatut1 ----
      lbStatut1.setPreferredSize(new Dimension(150, 18));
      lbStatut1.setMinimumSize(new Dimension(150, 18));
      lbStatut1.setBackground(Color.white);
      lbStatut1.setFocusable(false);
      lbStatut1.setBorder(null);
      lbStatut1.setFont(new Font("sansserif", Font.PLAIN, 13));
      lbStatut1.setMaximumSize(new Dimension(150, 18));
      lbStatut1.setName("lbStatut1");
      pnlEncart.add(lbStatut1,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
      
      // ---- lbStatut2 ----
      lbStatut2.setPreferredSize(new Dimension(150, 18));
      lbStatut2.setMinimumSize(new Dimension(150, 18));
      lbStatut2.setBackground(Color.white);
      lbStatut2.setFocusable(false);
      lbStatut2.setBorder(null);
      lbStatut2.setFont(new Font("sansserif", Font.PLAIN, 13));
      lbStatut2.setMaximumSize(new Dimension(150, 18));
      lbStatut2.setName("lbStatut2");
      pnlEncart.add(lbStatut2,
          new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
      
      // ---- lbStatut3 ----
      lbStatut3.setPreferredSize(new Dimension(150, 18));
      lbStatut3.setMinimumSize(new Dimension(150, 18));
      lbStatut3.setBackground(Color.white);
      lbStatut3.setFocusable(false);
      lbStatut3.setBorder(null);
      lbStatut3.setFont(new Font("sansserif", Font.PLAIN, 13));
      lbStatut3.setMaximumSize(new Dimension(150, 18));
      lbStatut3.setName("lbStatut3");
      pnlEncart.add(lbStatut3,
          new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    }
    add(pnlEncart, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlEncart;
  private JLabel lbTitre;
  private JLabel lbClient;
  private JLabel lbQuantite;
  private JTextPane lbStatut1;
  private JTextPane lbStatut2;
  private JTextPane lbStatut3;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
  public boolean isEnCompte() {
    return isEnCompte;
  }
  
  public void setEnCompte(boolean isEnCompte) {
    this.isEnCompte = isEnCompte;
  }
}
