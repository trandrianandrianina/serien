/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.commun.sncodepostal;

import ri.serien.libcommun.gescom.personnalisation.codepostal.CodePostal;
import ri.serien.libcommun.gescom.personnalisation.codepostal.IdCodePostal;
import ri.serien.libcommun.gescom.personnalisation.codepostal.ListeCodePostal;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un code postal.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une CodePostal dans le logiciel. Il gère l'affichage et la sélection de
 * l'un d'entre eux
 *
 * Utilisation :
 * - Ajouter un composant SNCodePostal à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM22FM_B2.
 */
public class SNCodePostal extends SNComboBoxObjetMetier<IdCodePostal, CodePostal, ListeCodePostal> {
  
  private ListeCodePostal listeCodePostal = null;
  
  /**
   * Constructeur par défaut.
   */
  public SNCodePostal() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeCodePostal());
  }
  
  /**
   * Sélectionner un code postal de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pCodePostal) {
    if (pCodePostal == null || pLexical.HostFieldGetData(pCodePostal) == null || pCodePostal.trim().isEmpty()) {
      throw new MessageErreurException("Impossible de sélectionner le code postal car son code est invalide.");
    }
    
    IdCodePostal idCodePostal = null;
    String valeurCodePostal = pLexical.HostFieldGetData(pCodePostal);
    if (!valeurCodePostal.trim().isEmpty()) {
      int resultatCodePostal = Integer.parseInt(valeurCodePostal.trim());
      idCodePostal = IdCodePostal.getInstance(resultatCodePostal);
      setIdSelection(idCodePostal);
    }
  }
  
  /**
   * Renseigner les champs RPG correspondant au code postal sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pCodePostal) {
    CodePostal codePostal = getSelection();
    if (codePostal != null) {
      pLexical.HostFieldPutData(pCodePostal, 0, codePostal.getId().getCodePostal().toString());
    }
    else {
      pLexical.HostFieldPutData(pCodePostal, 0, "");
    }
  }
}
