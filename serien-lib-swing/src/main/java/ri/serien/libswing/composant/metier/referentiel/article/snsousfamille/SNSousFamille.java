/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.snsousfamille;

import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.IdSousFamille;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.sngroupe.SNGroupe;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une Sous-famille.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une SousFamille dans le logiciel. Il gère l'affichage des SousFamilles
 * existantes et la sélection de l'une d'entre elle.
 *
 * Utilisation :
 * - Ajouter un composant SNSousFamille à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVX35FM_B1 (GVM3237)
 */
public class SNSousFamille extends SNComboBoxObjetMetier<IdSousFamille, SousFamille, ListeSousFamille> {
  // Variables
  private SNGroupe snGroupe = null;
  private SNFamille snFamille = null;

  /**
   * Constructeur par défaut
   */
  public SNSousFamille() {
    super();
  }

  /**
   * Indiquer si la sous-famille est sélectionnable dans la liste déroulante.
   *
   * Si la PS158 est égale à 0, seules les sous-familles du groupe ou de la famille sélectionnés sont sélectionnables.
   * Si la PS158 est égale à 1, les sous-familles sont indépendantes du groupe et de la famille.
   */
  @Override
  public boolean isSelectionnable(SousFamille pSousFamille) {
    // Ne pas sélectionner les familles "null"
    if (pSousFamille == null) {
      return false;
    }

    // Ne pas lier la sous-famille à la famille ou au groupe si la PS158 est active (égale à 1)
    if (getIdSession() != null && getIdEtablissement() != null) {
      Boolean ps158 = ManagerSessionClient.getInstance().isParametreSystemeActif(getIdSession(), EnumParametreSysteme.PS158);
      if (ps158) {
        return true;
      }
    }

    // Ne pas sélectionner les sous-familles qui ne sont pas dans le groupe sélectionné
    if (snGroupe != null && snGroupe.getIdSelection() != null && !pSousFamille.getId().isDansGroupe(snGroupe.getIdSelection())) {
      return false;
    }

    // Ne pas sélectionner les sous-familles qui ne sont pas dans la famille sélectionnée
    if (snFamille != null && snFamille.getIdSelection() != null && !pSousFamille.getId().isDansFamille(snFamille.getIdSelection())) {
      return false;
    }

    // Ne pas sélectionner les sous-familles qui sont inférieures au groupe début sélectionné
    if (snGroupe != null && snGroupe.getComposantDebut() != null && snGroupe.getComposantDebut().getIdSelection() != null
        && pSousFamille.getId().getIdGroupe().compareTo(snGroupe.getComposantDebut().getIdSelection()) < 0) {
      return false;
    }

    // Ne pas sélectionner les sous-familles qui sont supérieures au groupe fin sélectionné
    if (snGroupe != null && snGroupe.getComposantFin() != null && snGroupe.getComposantFin().getIdSelection() != null
        && pSousFamille.getId().getIdGroupe().compareTo(snGroupe.getComposantFin().getIdSelection()) > 0) {
      return false;
    }

    // Ne pas sélectionner les sous-familles qui sont inférieures à la famille début sélectionnée
    if (snFamille != null && snFamille.getComposantDebut() != null && snFamille.getComposantDebut().getIdSelection() != null
        && pSousFamille.getId().getIdFamille().compareTo(snFamille.getComposantDebut().getIdSelection()) < 0) {
      return false;
    }

    // Ne pas sélectionner les sous-familles qui sont supérieures à la famille fin sélectionnée
    if (snFamille != null && snFamille.getComposantFin() != null && snFamille.getComposantFin().getIdSelection() != null
        && pSousFamille.getId().getIdFamille().compareTo(snFamille.getComposantFin().getIdSelection()) > 0) {
      return false;
    }

    return true;
  }

  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeSousFamille());
  }

  /**
   * Sélectionner une sous-famille de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampSousFamille) {
    String valeurSousFamille = pLexical.HostFieldGetData(pChampSousFamille);
    if (valeurSousFamille == null) {
      throw new MessageErreurException("Impossible de sélectionner la sous-famille car le nom du champ est invalide.");
    }

    // Construire l'identifiant de la sous famille
    IdSousFamille idSousFamille = null;
    if (!valeurSousFamille.trim().isEmpty() && !valeurSousFamille.trim().equals("**")) {
      idSousFamille = IdSousFamille.getInstance(getIdEtablissement(), valeurSousFamille);
    }

    setIdSelection(idSousFamille);
  }

  /**
   * Renseigner les champs RPG correspondant à la sous-famille sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampSousFamille) {
    IdSousFamille idSousfamille = getIdSelection();
    if (idSousfamille != null) {
      pLexical.HostFieldPutData(pChampSousFamille, 0, idSousfamille.getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampSousFamille, 0, "");
    }
  }

  /**
   * Etre notifié lorsque la sélection change.
   */
  @Override
  public void valueChanged() {
    // Modifier le groupe du composant groupe afin qu'il corresponde à la sous-famille sélectionnée.
    if (snGroupe != null && getIdSelection() != null) {
      snGroupe.setIdSelection(getIdSelection().getIdGroupe());
    }

    // Modifier la famille du composant famille afin qu'il corresponde à la sous-famille sélectionnée.
    if (snFamille != null && getIdSelection() != null) {
      snFamille.setIdSelection(getIdSelection().getIdFamille());
    }
  }

  /**
   * Retourner le composant groupe lié à ce composant.
   */
  public SNGroupe getComposantGroupe() {
    return snGroupe;
  }

  /**
   * Lier un composant groupe à ce composant.
   */
  public void lierComposantGroupe(SNGroupe psnGroupe) {
    // Tester si al valeur a changée
    if (Constantes.equals(psnGroupe, snGroupe)) {
      return;
    }

    // Mettre à jour la valeur
    snGroupe = psnGroupe;

    // Faire le lien dans l'autre sens
    snGroupe.lierComposantSousFamille(this);

    // Faire également le lien avec la famille si on la connaît
    if (snFamille != null) {
      snFamille.lierComposantGroupe(snGroupe);
    }

    // Rafraichir la liste des composants sélectionnable
    rafraichirListeSelectionnable();
  }

  /**
   * Retourner le composant famille lié à ce composant.
   */
  public SNFamille getComposantFamille() {
    return snFamille;
  }

  /**
   * Lier un composant groupe à ce composant.
   */
  public void lierComposantFamille(SNFamille psnFamille) {
    // Tester si la valeur a changée
    if (Constantes.equals(psnFamille, snFamille)) {
      return;
    }

    // Mettre à jour la valeur
    snFamille = psnFamille;

    // Faire le lien dans l'autre sens
    snFamille.lierComposantSousFamille(this);

    // Faire également le lien avec le groupe si on la connaît
    if (snGroupe != null) {
      snGroupe.lierComposantFamille(snFamille);
    }

    // Rafraichir la liste des composants sélectionnable
    rafraichirListeSelectionnable();
  }
}
