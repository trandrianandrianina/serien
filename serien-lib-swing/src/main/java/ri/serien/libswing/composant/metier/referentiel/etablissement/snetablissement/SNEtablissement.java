/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantAvecSession;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un établissement.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'établissement dans le logiciel. Il gère l'affichage des établissements
 * existants et la sélection de l'un d'entre eux.
 *
 * Session Client
 * Ce composant peut être forcé pour mettre à jour l'établissement au niveau de la session client grâce à la variable
 * composantEtablissementPrincipal
 *
 * Utilisation :
 * - Ajouter un composant SNEtablissement à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le RGVX08FM_A4.
 */
public class SNEtablissement extends SNComposantAvecSession {
  private ListeEtablissement listeEtablissement = null;
  private boolean rafraichir = false;
  private boolean chargementEnCours = false;
  private boolean avecValeurVide = false;
  private boolean composantEtablissementPrincipal = false;
  
  /**
   * Constructeur par défaut.
   */
  public SNEtablissement() {
    super();
    initComponents();
    
    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    
    // Evènements sur la liste déroulante
    cbEtablissement.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        cbEtablissementItemStateChanged(e);
      }
    });
  }
  
  /**
   * Activer ou désactiver le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    cbEtablissement.setEnabled(pEnabled);
  }
  
  /**
   * Charger la liste des établissements.
   * La liste n'est pas rechargée si les paramètres n'ont pas changés, sauf si un rafraîchissement explicite a été
   * demandé lors de l'appel
   * de la méthode.
   */
  public void charger(boolean pForcerRafraichissement) {
    // Tester s'il faut rafraîchir la liste
    if (!rafraichir && !pForcerRafraichissement && listeEtablissement != null) {
      return;
    }
    rafraichir = false;
    
    // Charger la liste des établissements
    if (getIdSession() != null) {
      setListe(ListeEtablissement.charger(getIdSession()));
    }
    else {
      setListe(null);
    }
  }
  
  /**
   * Liste des établissements de la liste déroulante.
   */
  public ListeEtablissement getListe() {
    return listeEtablissement;
  }
  
  /**
   * Modifier la liste des établissements de la liste déroulante.
   */
  public void setListe(ListeEtablissement pListeEtablissement) {
    // Trier avant la comparaison
    if (pListeEtablissement != null) {
      pListeEtablissement.trierParRaisonSociale();
    }
    
    // Tester si le contenu à changé
    if (Constantes.equals(listeEtablissement, pListeEtablissement)) {
      return;
    }
    listeEtablissement = pListeEtablissement;
    
    // Mémoriser l'objet sélectionné
    Object object = cbEtablissement.getSelectedItem();
    
    // Bloquer les notifications du listener si on est en cours de chargement de la liste
    chargementEnCours = true;
    
    // Mettre à jour la liste
    cbEtablissement.removeAllItems();
    // Ajout élément blanc (sauf pour composant principal pour ne pas mettre à blanc l'établissement de la session)
    if (avecValeurVide && !isComposantEtablissementPrincipal()) {
      cbEtablissement.addItem(" ");
    }
    if (listeEtablissement != null) {
      for (Etablissement etablissement : listeEtablissement) {
        cbEtablissement.addItem(etablissement);
      }
    }
    
    // Réactiver les notifications
    chargementEnCours = false;
    
    // Resléectionner l'objet
    cbEtablissement.setSelectedItem(object);
  }
  
  /**
   * Etablissement sélectionné (null si aucun).
   */
  public Etablissement getSelection() {
    if (cbEtablissement.getSelectedItem() instanceof Etablissement) {
      return (Etablissement) cbEtablissement.getSelectedItem();
    }
    else {
      return null;
    }
  }
  
  /**
   * Identifiant de l'établissement sélectionné (null si aucun).
   */
  public IdEtablissement getIdSelection() {
    if (cbEtablissement.getSelectedItem() instanceof Etablissement) {
      return ((Etablissement) cbEtablissement.getSelectedItem()).getId();
    }
    else {
      return null;
    }
  }
  
  /**
   * Code de l'établissement sélectionné ("" si aucun).
   */
  public String getCodeSelection() {
    if (getSelection() != null && getSelection().getId() != null && getSelection().getId().getCodeEtablissement() != null) {
      return ((Etablissement) cbEtablissement.getSelectedItem()).getId().getCodeEtablissement();
    }
    else {
      return "";
    }
  }
  
  /**
   * Sélectionner l'établissement.
   */
  public void setSelection(Etablissement pEtablissement) {
    if (listeEtablissement != null && pEtablissement != null) {
      cbEtablissement.setSelectedItem(pEtablissement);
      if (isComposantEtablissementPrincipal()) {
        ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), pEtablissement.getId());
      }
    }
    else {
      cbEtablissement.setSelectedItem(null);
    }
  }
  
  /**
   * Sélectionner l'établissement par son id.
   */
  public void setIdSelection(IdEtablissement pIdEtablissement) {
    Etablissement etablissement = null;
    if (listeEtablissement != null && pIdEtablissement != null) {
      etablissement = listeEtablissement.getEtablissementParId(pIdEtablissement);
    }
    setSelection(etablissement);
  }
  
  /**
   * Sélectionner un établissement de la liste déroulante à partir des champs RPG.
   * La méthode retourne true si la sélection a été modifiée ou false dans le cas inverse.
   */
  public void setSelectionParChampRPG(Lexical pLexical, String pChampEtablissement) {
    if (pChampEtablissement == null || pLexical.HostFieldGetData(pChampEtablissement) == null) {
      throw new MessageErreurException("Impossible de sélectionner l'établissement car son code est invalide.");
    }
    
    IdEtablissement idEtablissement = null;
    String valeurEtablissement = pLexical.HostFieldGetData(pChampEtablissement);
    if (valeurEtablissement != null && !valeurEtablissement.trim().isEmpty()) {
      idEtablissement = IdEtablissement.getInstance(pLexical.HostFieldGetData(pChampEtablissement));
    }
    
    setIdSelection(idEtablissement);
  }
  
  /**
   * Renseigner les champs RPG correspondant à l'établissement sélectionné.
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampEtablissement) {
    Etablissement etablissement = getSelection();
    if (etablissement != null) {
      pLexical.HostFieldPutData(pChampEtablissement, 0, etablissement.getId().getCodeEtablissement());
    }
    else {
      pLexical.HostFieldPutData(pChampEtablissement, 0, "");
    }
  }
  
  /**
   * Met à jour si le choix "aucun etablissement" est inclu dans le composant
   */
  public void setEtablissementBlanc(boolean pAvecValeurVide) {
    avecValeurVide = pAvecValeurVide;
  }
  
  /**
   * Tester si la liste déroulante est vide.
   */
  public boolean isEmpty() {
    return listeEtablissement == null || listeEtablissement.isEmpty();
  }
  
  /**
   * Notifier lorsqu'un nouveau établissement est sélectionné.
   */
  private void cbEtablissementItemStateChanged(ItemEvent e) {
    try {
      if (e.getStateChange() != ItemEvent.SELECTED || chargementEnCours) {
        return;
      }
      fireValueChanged();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Renvoyer si le composant est le composant établissement principal.
   * Le composant principal modifie l'identifiant établissement au niveau de la session
   */
  public boolean isComposantEtablissementPrincipal() {
    return composantEtablissementPrincipal;
  }
  
  /**
   * Modifier si le composant est le composant établissement principal.
   * Le composant principal modifie l'identifiant établissement au niveau de la session
   */
  public void setComposantEtablissementPrincipal(boolean composantEtablissementPrincipal) {
    this.composantEtablissementPrincipal = composantEtablissementPrincipal;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    cbEtablissement = new SNComboBox();
    
    // ======== this ========
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
    
    // ---- cbEtablissement ----
    cbEtablissement.setName("cbEtablissement");
    add(cbEtablissement,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNComboBox cbEtablissement;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
