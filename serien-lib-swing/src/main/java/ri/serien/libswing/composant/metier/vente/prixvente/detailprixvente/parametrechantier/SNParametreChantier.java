/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametrechantier;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.ParametreChantier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Ce composant permet d'afficher les paramètres chantier pour une formule de prix de vente.
 */
public class SNParametreChantier extends SNPanelTitre {
  private static final String TITRE = "Chantier";

  private ModeleDialogueDetailPrixVente modele = null;
  private boolean evenementsActifs = false;
  private ParametreChantier parametreChantier = null;
  private ParametreChantier parametreChantierInitial = null;
  private FormulePrixVente formulePrixVente = null;

  /**
   * Constructeur.
   */
  public SNParametreChantier() {
    super();
    initComponents();
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Modèle associé au composant graphique.
   * @return Modèle de la boîte de dialogue.
   */
  public ModeleDialogueDetailPrixVente getModele() {
    return modele;
  }

  /**
   * Modifier le modèle associé au composant graphique.
   * @param pModele Modèle de la boîte de dialogue.
   */
  public void setModele(ModeleDialogueDetailPrixVente pModele) {
    modele = pModele;
  }

  /**
   * Indiquer si les évènements doivent être traités.
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir des données du composant.
   */
  public void rafraichir() {
    // Empêcher les évènements pendant le rafraichissement
    evenementsActifs = false;

    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    parametreChantier = getModele().getParametreChantier();
    parametreChantierInitial = getModele().getParametreChantierInitial();
    formulePrixVente = modele.getFormulePrixVente();

    // Configurer tous les composants SNMontant avec la précision souhaitée
    int nombreDecimale = ArrondiPrix.DECIMALE_STANDARD;
    if (formulePrixVente != null && formulePrixVente.getArrondiPrix() != null) {
      nombreDecimale = formulePrixVente.getArrondiPrix().getNombreDecimale();
    }
    tfPrixBaseHT.setLongueurPartieDecimale(nombreDecimale);
    tfPrixNetHT.setLongueurPartieDecimale(nombreDecimale);

    // Rafraichir les composants
    rafraichirTitre();
    rafraichirType();
    rafraichirIdArticle();
    rafraichirQuantiteMinimum();
    rafraichirUniteVente();
    rafraichirColonneTarif();
    rafraichirPrixBaseHT();
    rafraichirPrixNetHT();

    // Réactiver les évènements pendant le rafraichissement
    evenementsActifs = true;
  }

  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirTitre() {
    if (parametreChantier != null && parametreChantier.getIdChantier() != null) {
      setTitre(TITRE + " " + parametreChantier.getIdChantier());
    }
    else {
      setTitre(TITRE);
    }
  }

  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirType() {
    if (parametreChantier != null && parametreChantier.getTypeChantier() != null) {
      tfType.setText(parametreChantier.getTypeChantier().getLibelle());
    }
    else {
      tfType.setText("");
    }
  }
  
  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirIdArticle() {
    if (parametreChantier != null && parametreChantier.getIdArticle() != null) {
      tfIdArticle.setText(parametreChantier.getIdArticle().toString());
    }
    else {
      tfIdArticle.setText("");
    }
  }

  /**
   * Rafraichir la quantité en UV du chantier.
   */
  private void rafraichirQuantiteMinimum() {
    if (parametreChantier != null && parametreChantier.getQuantiteMinimum() != null) {
      tfQuantiteMinimum.setMontant(parametreChantier.getQuantiteMinimum());
    }
    else {
      tfQuantiteMinimum.setMontant("");
    }
  }
  
  /**
   * Rafraichir l'unité de vente du chantier.
   */
  private void rafraichirUniteVente() {
    if (parametreChantier != null && parametreChantier.getCodeUniteVente() != null) {
      tfUniteVente.setText(parametreChantier.getCodeUniteVente());
    }
    else {
      tfUniteVente.setText("");
    }

    tfUniteVente.setEnabled(modele != null && getModele().isModeSimulation());
    lbUniteVente.setImportance(parametreChantier != null && parametreChantierInitial != null
        && !Constantes.equals(parametreChantier.getCodeUniteVente(), parametreChantierInitial.getCodeUniteVente()));
  }

  /**
   * Rafraichir la colonne tarif du chantier.
   */
  private void rafraichirColonneTarif() {
    if (parametreChantier != null && parametreChantier.getNumeroColonneTarif() != null) {
      Integer numeroColonneTarif = parametreChantier.getNumeroColonneTarif();
      if (numeroColonneTarif >= 1 && numeroColonneTarif <= 10) {
        cbColonneTarif.setSelectedIndex(numeroColonneTarif);
      }
      else {
        cbColonneTarif.setSelectedIndex(0);
      }
    }
    else {
      cbColonneTarif.setSelectedIndex(0);
    }

    cbColonneTarif.setEnabled(modele != null && getModele().isModeSimulation());
    lbColonneTarif.setImportance(parametreChantier != null && parametreChantierInitial != null
        && !Constantes.equals(parametreChantier.getNumeroColonneTarif(), parametreChantierInitial.getNumeroColonneTarif()));
  }

  /**
   * Rafraichir le prix de base HT du chantier.
   */
  private void rafraichirPrixBaseHT() {
    if (parametreChantier != null && parametreChantier.getPrixBaseHT() != null) {
      tfPrixBaseHT.setMontant(parametreChantier.getPrixBaseHT());
    }
    else {
      tfPrixBaseHT.setMontant("");
    }

    tfPrixBaseHT.setEnabled(modele != null && getModele().isModeSimulation());
    lbPrixBaseHT.setImportance(parametreChantier != null && parametreChantierInitial != null
        && !Constantes.equals(parametreChantier.getPrixBaseHT(), parametreChantierInitial.getPrixBaseHT()));
  }

  /**
   * Rafraichir le prix net HT du chantier.
   */
  private void rafraichirPrixNetHT() {
    if (parametreChantier != null && parametreChantier.getPrixNetHT() != null) {
      tfPrixNetHT.setMontant(parametreChantier.getPrixNetHT());
    }
    else {
      tfPrixNetHT.setMontant("");
    }

    tfPrixNetHT.setEnabled(modele != null && modele.isModeSimulation());
    lbPrixNetHT.setImportance(parametreChantier != null && parametreChantierInitial != null
        && !Constantes.equals(parametreChantier.getPrixNetHT(), parametreChantierInitial.getPrixNetHT()));
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void tfUniteVenteFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierUniteVenteChantier(tfUniteVente.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbColonneTarifItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbColonneTarif.getSelectedIndex() >= 1 && cbColonneTarif.getSelectedIndex() <= 10) {
        getModele().modifierColonneTarifChantier(cbColonneTarif.getSelectedIndex());
      }
      else {
        getModele().modifierColonneTarifChantier(0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfPrixBaseHTValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTChantier(tfPrixBaseHT.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfPrixNetHTValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixNetHTChantier(tfPrixNetHT.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbType = new SNLabelChamp();
    tfType = new SNTexte();
    lbIdArticle = new SNLabelChamp();
    tfIdArticle = new SNTexte();
    lbQuantiteMinimum = new SNLabelChamp();
    tfQuantiteMinimum = new SNMontant();
    lbUniteVente = new SNLabelChamp();
    tfUniteVente = new SNTexte();
    lbColonneTarif = new SNLabelChamp();
    cbColonneTarif = new SNComboBoxReduit();
    lbPrixBaseHT = new SNLabelChamp();
    tfPrixBaseHT = new SNMontant();
    lbPrixNetHT = new SNLabelChamp();
    tfPrixNetHT = new SNMontant();
    
    // ======== this ========
    setModeReduit(true);
    setTitre("Chantier");
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    
    // ---- lbType ----
    lbType.setModeReduit(true);
    lbType.setText("Type condition chantier");
    lbType.setMinimumSize(new Dimension(30, 22));
    lbType.setPreferredSize(new Dimension(30, 22));
    lbType.setName("lbType");
    add(lbType,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfType ----
    tfType.setModeReduit(true);
    tfType.setEnabled(false);
    tfType.setMaximumSize(new Dimension(100, 22));
    tfType.setMinimumSize(new Dimension(100, 22));
    tfType.setPreferredSize(new Dimension(100, 22));
    tfType.setName("tfType");
    add(tfType,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbIdArticle ----
    lbIdArticle.setModeReduit(true);
    lbIdArticle.setText("Article");
    lbIdArticle.setMinimumSize(new Dimension(30, 22));
    lbIdArticle.setPreferredSize(new Dimension(30, 22));
    lbIdArticle.setName("lbIdArticle");
    add(lbIdArticle,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfIdArticle ----
    tfIdArticle.setModeReduit(true);
    tfIdArticle.setEnabled(false);
    tfIdArticle.setName("tfIdArticle");
    add(tfIdArticle,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbQuantiteMinimum ----
    lbQuantiteMinimum.setText("Quantit\u00e9 minimum");
    lbQuantiteMinimum.setModeReduit(true);
    lbQuantiteMinimum.setName("lbQuantiteMinimum");
    add(lbQuantiteMinimum,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfQuantiteMinimum ----
    tfQuantiteMinimum.setModeReduit(true);
    tfQuantiteMinimum.setEnabled(false);
    tfQuantiteMinimum.setName("tfQuantiteMinimum");
    add(tfQuantiteMinimum,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbUniteVente ----
    lbUniteVente.setText("Unit\u00e9 de vente");
    lbUniteVente.setModeReduit(true);
    lbUniteVente.setName("lbUniteVente");
    add(lbUniteVente,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfUniteVente ----
    tfUniteVente.setEnabled(false);
    tfUniteVente.setModeReduit(true);
    tfUniteVente.setName("tfUniteVente");
    tfUniteVente.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfUniteVenteFocusLost(e);
      }
    });
    add(tfUniteVente,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbColonneTarif ----
    lbColonneTarif.setModeReduit(true);
    lbColonneTarif.setText("Colonne tarif");
    lbColonneTarif.setMinimumSize(new Dimension(30, 22));
    lbColonneTarif.setPreferredSize(new Dimension(30, 22));
    lbColonneTarif.setName("lbColonneTarif");
    add(lbColonneTarif,
        new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbColonneTarif ----
    cbColonneTarif.setModel(new DefaultComboBoxModel<>(new String[] { "Aucune", "Colonne 1", "Colonne 2", "Colonne 3", "Colonne 4",
        "Colonne 5", "Colonne 6", "Colonne 7", "Colonne 8", "Colonne 9", "Colonne 10" }));
    cbColonneTarif.setSelectedIndex(-1);
    cbColonneTarif.setEnabled(false);
    cbColonneTarif.setName("cbColonneTarif");
    cbColonneTarif.addItemListener(e -> cbColonneTarifItemStateChanged(e));
    add(cbColonneTarif,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseHT ----
    lbPrixBaseHT.setModeReduit(true);
    lbPrixBaseHT.setText("Prix de base HT");
    lbPrixBaseHT.setMinimumSize(new Dimension(30, 22));
    lbPrixBaseHT.setPreferredSize(new Dimension(30, 22));
    lbPrixBaseHT.setName("lbPrixBaseHT");
    add(lbPrixBaseHT,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseHT ----
    tfPrixBaseHT.setModeReduit(true);
    tfPrixBaseHT.setEnabled(false);
    tfPrixBaseHT.setName("tfPrixBaseHT");
    tfPrixBaseHT.addSNComposantListener(e -> tfPrixBaseHTValueChanged(e));
    add(tfPrixBaseHT,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixNetHT ----
    lbPrixNetHT.setModeReduit(true);
    lbPrixNetHT.setText("Prix net HT");
    lbPrixNetHT.setMinimumSize(new Dimension(30, 22));
    lbPrixNetHT.setPreferredSize(new Dimension(30, 22));
    lbPrixNetHT.setName("lbPrixNetHT");
    add(lbPrixNetHT,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixNetHT ----
    tfPrixNetHT.setModeReduit(true);
    tfPrixNetHT.setEnabled(false);
    tfPrixNetHT.setName("tfPrixNetHT");
    tfPrixNetHT.addSNComposantListener(e -> tfPrixNetHTValueChanged(e));
    add(tfPrixNetHT,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbType;
  private SNTexte tfType;
  private SNLabelChamp lbIdArticle;
  private SNTexte tfIdArticle;
  private SNLabelChamp lbQuantiteMinimum;
  private SNMontant tfQuantiteMinimum;
  private SNLabelChamp lbUniteVente;
  private SNTexte tfUniteVente;
  private SNLabelChamp lbColonneTarif;
  private SNComboBoxReduit cbColonneTarif;
  private SNLabelChamp lbPrixBaseHT;
  private SNMontant tfPrixBaseHT;
  private SNLabelChamp lbPrixNetHT;
  private SNMontant tfPrixNetHT;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
