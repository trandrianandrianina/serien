/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.dialoguestandard.confirmation;

import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue de confirmation.
 */
public class ModeleDialogueConfirmation extends AbstractModeleDialogue {
  // Variables
  private String texteMessage = "";
  private String titreFenetre = "";

  /**
   * Constructeur.
   */
  public ModeleDialogueConfirmation(SessionBase pSession, String aTitre, String aTexte) {
    super(pSession);
    titreFenetre = aTitre;
    texteMessage = aTexte;
  }

  // -- Méthodes publiques

  @Override
  public void initialiserDonnees() {
  }

  @Override
  public void chargerDonnees() {
    if (texteMessage != null) {
      texteMessage = "<html><div style=\"text-align:center\">" + texteMessage.replaceAll("\\n", "<br>") + "</div></html>";
    }
  }

  // -- Accesseurs

  public String getTitreFenetre() {
    return titreFenetre;
  }

  public String getTexteMessage() {
    return texteMessage;
  }

  public void setTexteMessage(String texteMessage) {
    this.texteMessage = texteMessage;
  }
}
