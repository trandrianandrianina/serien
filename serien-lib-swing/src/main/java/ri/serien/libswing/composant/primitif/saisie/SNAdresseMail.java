/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.saisie;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.IOException;
import java.net.URI;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonMail;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposant;

/**
 * Composant Mailto permettant la saisie d'une adresse email et l'envoi vers le mailer.
 */
public class SNAdresseMail extends SNComposant {
  // Variables
  private String adresseMail = "";
  private boolean saisissable = true;
  private boolean adresseMailNonValide = false;
  
  /**
   * Constructeur.
   */
  public SNAdresseMail() {
    super();
    initComponents();

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
    
    // Rendre le composant transparent
    setOpaque(false);

    tfAdresseMail.setFont(SNCharteGraphique.POLICE_STANDARD);
    tfAdresseMail.setForeground(Color.black);
  }
  
  /**
   * Activation du composant
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    saisissable = pEnabled;
    btMailTo.setEnabled(true);
    tfAdresseMail.setEnabled(pEnabled);
  }
  
  @Override
  public boolean isEnabled() {
    return saisissable;
  }

  /**
   * Modifier l'adresse mail
   */
  public void setText(String pAdresse) {
    if (pAdresse == null) {
      throw new MessageErreurException("L'adresse mail entrée est invalide");
    }
    adresseMailNonValide = false;
    pAdresse = Constantes.normerTexte(pAdresse);

    // Contrôle de validité de l'émail uniquement si le composant en modifiable (sinon c'est l'enfer à gérer).
    if (saisissable && !pAdresse.isEmpty() && !Constantes.controlerEmail(pAdresse)) {
      adresseMailNonValide = true;
      adresseMail = pAdresse.toLowerCase();
      // Mise à jour l'affichage pour garder la trace du mail non valide à l'écran (SNC-9588).
      rafraichir();
      throw new MessageErreurException(
          "L'adresse mail entrée " + pAdresse + " n'est pas au format standard\nBoiteLocale@Serveur.NomDeDomaine");
    }
    adresseMail = pAdresse.toLowerCase();
    fireValueChanged();
    rafraichir();
  }
  
  /**
   * Retourne si l'adresse mail est valide.
   * Permet de contrer les problèmes dans les vues de l'évènement snMailValueChanged exécuté après l'évènement du bouton Valider.
   *
   * @return
   */
  public boolean isAdresseMailNonValide() {
    return adresseMailNonValide;
  }
  
  /**
   * Retourner l'adresse email.
   */
  public String getText() {
    return adresseMail.trim().toLowerCase();
  }
  
  /**
   * Rafraichit l'aspect du composant.
   */
  private void rafraichir() {
    tfAdresseMail.setText(adresseMail);
  }

  /**
   * Ouvre le logiciel de messagerie par défaut sur le poste de travail.
   */
  private void mailto() {
    if (Constantes.normerTexte(adresseMail).isEmpty() || (adresseMail.indexOf('@') == -1)) {
      return;
    }
    
    Desktop desktop = Desktop.getDesktop();
    URI uri = URI.create("mailto:" + adresseMail);
    try {
      desktop.mail(uri);
    }
    catch (IOException e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Evènement d'envoi d'un mail
   */
  private void btMailToActionPerformed(ActionEvent e) {
    try {
      mailto();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Evènement sur perte de focus dans le composant de saisie
   */
  private void tfAdresseMailFocusLost(FocusEvent e) {
    try {
      setText(tfAdresseMail.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  // CHECKSTYLE:OFF
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    tfAdresseMail = new SNTexte();
    btMailTo = new SNBoutonMail();

    // ======== this ========
    setOpaque(false);
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };

    // ---- tfAdresseMail ----
    tfAdresseMail.setPreferredSize(new Dimension(200, 30));
    tfAdresseMail.setName("tfAdresseMail");
    tfAdresseMail.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfAdresseMailFocusLost(e);
      }
    });
    add(tfAdresseMail,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

    // ---- btMailTo ----
    btMailTo.setName("btMailTo");
    btMailTo.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btMailToActionPerformed(e);
      }
    });
    add(btMailTo,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNTexte tfAdresseMail;
  private SNBoutonMail btMailTo;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
