/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.client.snbanqueclient;

import ri.serien.libcommun.gescom.vente.reglement.BanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.IdBanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.ListeBanqueClient;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une banque client.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une banque pour un client dans le logiciel.
 * Il gère l'affichage des banques existantes et la sélection de l'une d'entre elle.
 *
 * Utilisation :
 * - Ajouter un composant SNBanqueClient à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le XXXXXX
 */
public class SNBanqueClient extends SNComboBoxObjetMetier<IdBanqueClient, BanqueClient, ListeBanqueClient> {
  
  /**
   * Constructeur par défaut
   */
  public SNBanqueClient() {
    super();
  }
  
  /**
   * Chargement des données de la combo.
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeBanqueClient());
  }
  
  /**
   * Sélectionner une banque client de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampBanque) {
    if (pChampBanque == null || pLexical.HostFieldGetData(pChampBanque) == null) {
      throw new MessageErreurException("Impossible de sélectionner la banque car son nom est invalide.");
    }
    
    IdBanqueClient idBanqueClient = null;
    String valeur = pLexical.HostFieldGetData(pChampBanque);
    if (valeur != null && !valeur.trim().isEmpty()) {
      idBanqueClient = IdBanqueClient.getInstance(pLexical.HostFieldGetData(pChampBanque));
    }
    setIdSelection(idBanqueClient);
  }
  
  /**
   * Renseigne les champs RPG correspondant à la banque sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampBanque) {
    BanqueClient banqueClient = getSelection();
    if (banqueClient != null) {
      pLexical.HostFieldPutData(pChampBanque, 0, banqueClient.getId().getNom());
    }
    else {
      pLexical.HostFieldPutData(pChampBanque, 0, "");
    }
  }
}
