/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton affichant une icône en forme de calendrier permettant de sélectionner une date dans un calendrier.
 *
 * La taille standard est de 26 x 26 pixels afin de correspondre à la hauteur standard des composants qui est de 30 pixels.
 * Avec les effets de dégradés des composants, il faut que l'icône soit plus petite que 30x30 pixels sinon cela donne l'impression
 * qu'elle est plus haute que les composants standards.
 */
public class SNBoutonCalendrier extends SNBoutonIcone {
  private static final int TAILLE_IMAGE = 26;
  private static final String INFOBULLE = "Ouvrir le calendrier";

  /**
   * Constructeur.
   */
  public SNBoutonCalendrier() {
    super(SNCharteGraphique.ICONE_CALENDRIER, TAILLE_IMAGE, INFOBULLE);
  }
}
