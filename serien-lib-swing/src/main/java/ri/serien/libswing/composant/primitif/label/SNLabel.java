/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.label;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Cette classe abstraite remplace les JLabel.
 *
 * Elle ne peut pas être utilisée directement. Il faut utiliser ses composants enfants :
 * - SNLabelChamp : pour nommer les champs
 * - SNLabelUnite : pour les labels cadrés à gauche apparaissant à la droite d'un champ
 * - SNLabelTitre : pour les titres de sections ou de tableaux.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 */
abstract class SNLabel extends JLabel {
  private EnumImportanceMessage importanceMessage = EnumImportanceMessage.NORMAL;
  private String texte;
  private boolean modeReduit = false;
  private boolean changementMode = false;
  private Font policeTitre = SNCharteGraphique.POLICE_TITRE;
  private Font policeStandard = SNCharteGraphique.POLICE_STANDARD;

  /**
   * Constructeur.
   */
  protected SNLabel() {
    super();

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
    
    // Rendre le composant transparent
    setOpaque(false);
  }

  /**
   * Rafraichit l'aspect du composant.
   */
  private void rafraichirAspect() {
    // Sélection de la police en fonction du mode uniquement si le mode a été modifié
    if (changementMode) {
      if (modeReduit) {
        setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE_REDUIT);
        setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE_REDUIT);
        setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE_REDUIT);
      }
      else {
        setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
        setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
        setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
      }
      changementMode = false;
    }
    
    // Sélection des tailles en fonction du mode
    if (modeReduit) {
      policeTitre = SNCharteGraphique.POLICE_TITRE_REDUIT;
      policeStandard = SNCharteGraphique.POLICE_STANDARD_REDUIT;
    }
    else {
      policeTitre = SNCharteGraphique.POLICE_TITRE;
      policeStandard = SNCharteGraphique.POLICE_STANDARD;
    }

    // Rafraîchir le texte
    if (texte != null) {
      super.setText(texte);
    }
    else {
      super.setText("");
    }
    
    // Changer la police en fonction de l'importance
    if (importanceMessage != null) {
      // Les labels de titres sont toujours en gras
      if (this instanceof SNLabelTitre) {
        switch (importanceMessage) {
          case HAUT:
            setFont(policeTitre);
            setForeground(Color.red);
            break;

          default:
            setFont(policeTitre);
            setForeground(Color.black);
            break;
        }
      }
      else {
        switch (importanceMessage) {
          case NORMAL:
            setFont(policeStandard);
            setForeground(Color.black);
            break;

          case MOYEN:
            setFont(policeTitre);
            setForeground(Color.black);
            break;

          case HAUT:
            setFont(policeTitre);
            setForeground(Color.red);
            break;

          default:
            setFont(policeStandard);
            setForeground(Color.black);
            break;
        }
      }
    }
    // L'importance est nulle alors, on utilise les valeurs par défaut du composant
    else {
      setFont(policeStandard);
      setForeground(Color.black);
    }
  }

  // -- Accesseurs

  /**
   * Active ou non le mode réduit du composant.
   * @param pActiverModeReduit
   */
  public void setModeReduit(boolean pActiverModeReduit) {
    // Contrôle s'il y a un chagement de mode (réduit ou non)
    if (modeReduit != pActiverModeReduit) {
      changementMode = true;
    }
    else {
      changementMode = false;
    }
    modeReduit = pActiverModeReduit;
    rafraichirAspect();
  }
  
  /**
   * Retourne si le mode réduit est actif.
   * @return
   */
  public boolean isModeReduit() {
    return modeReduit;
  }

  /**
   * Message affiché.
   */
  public Message getMessage() {
    return Message.getMessage(texte, importanceMessage);
  }

  /**
   * Modifier le message à afficher.
   *
   * @param pMessage Message.
   */
  public void setMessage(Message pMessage) {
    if (pMessage != null) {
      importanceMessage = pMessage.getImportanceMessage();
      setText(pMessage.getTexte());
    }
    else {
      importanceMessage = EnumImportanceMessage.NORMAL;
      setText("");
    }
  }

  /**
   * Modifier le texte du message à afficher.
   *
   * Si des retours à la ligne sont présents dans le texte (caractère '\n'), le texte est converti en HTML afin d'utiliser le tag "<br>"
   * pour faire des retours à la ligne.
   *
   * Il est possible de définir l'importance du message avec setImportanceMessage(). La méthode setMessage() permet de faire les deux à
   * la fois.
   *
   * @param pTexte Texte du libellé.
   */
  @Override
  public void setText(String pTexte) {
    texte = pTexte;
    if (texte.contains("\n")) {
      texte = "<html>" + texte.replace("\n", "<br>") + "</html>";
    }
    rafraichirAspect();
  }

  /**
   * Importance du message.
   */
  public EnumImportanceMessage getImportanceMessage() {
    return importanceMessage;
  }

  /**
   * Modifier l'importance du message.
   * A utiliser conjointement avec setText() pour définir le texte du message. La méthode setMessage() permet de faire les deux à la fois.
   * @param pImportanceMessage Importance du message.
   */
  public void setImportanceMessage(EnumImportanceMessage pImportanceMessage) {
    if (pImportanceMessage != null) {
      importanceMessage = pImportanceMessage;
    }
    else {
      importanceMessage = EnumImportanceMessage.NORMAL;
    }
    rafraichirAspect();
  }

  /**
   * Définir l'importance du message via un booléen.
   *
   * Cette méthode est une version simplifiée de la méthode setImportanceMessage() qui ne gère que deux états :
   * - EnumImportanceMessage.NORMAL si la paramètre fourni est false.
   * - EnumImportanceMessage.HAUT si la paramètre fourni est true.
   *
   * @param pImportant true=EnumImportanceMessage, false=EnumImportanceMessage.NORMAL.
   */
  public void setImportance(boolean pImportant) {
    if (pImportant) {
      importanceMessage = EnumImportanceMessage.HAUT;
    }
    else {
      importanceMessage = EnumImportanceMessage.NORMAL;
    }
    rafraichirAspect();
  }

  /**
   * Retourne la police du titre en cours.
   * @return
   */
  public Font getPoliceTitre() {
    return policeTitre;
  }

  /**
   * Retourne la police standard en cours.
   * @return
   */
  public Font getPoliceStandard() {
    return policeStandard;
  }
}
