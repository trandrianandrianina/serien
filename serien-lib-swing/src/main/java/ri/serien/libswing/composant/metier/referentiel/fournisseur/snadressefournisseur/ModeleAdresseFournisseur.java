/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.fournisseur.snadressefournisseur;

import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.EnumTypeAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeAdresseFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue de sélection d'une adresse fournisseur.
 */
public class ModeleAdresseFournisseur extends AbstractModeleDialogue {
  // Variables
  private String messageInformation = "";
  private CritereFournisseur critereFournisseur = new CritereFournisseur();
  private List<IdAdresseFournisseur> listeIdAdresseFournisseur = null;
  private ListeAdresseFournisseur listeAdresseFournisseur = null;
  private int ligneSelectionnee = -1;
  private AdresseFournisseur adresseFournisseurSelectionnee = null;
  
  /**
   * Constructeur.
   */
  public ModeleAdresseFournisseur(SessionBase pSession) {
    super(pSession);
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    IdSession.controlerId(getIdSession(), true);
  }
  
  @Override
  public void chargerDonnees() {
    // Tester si une liste d'identifiant a été fournie
    if (listeIdAdresseFournisseur != null) {
      // Charger la liste des adresses si une liste d'identifiants a été fournie par l'appelant
      listeAdresseFournisseur = ListeAdresseFournisseur.creerListeNonChargee(listeIdAdresseFournisseur);
      
      // Trier les adresses par identifiant
      listeAdresseFournisseur.trierParId();
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    // Vérifier si la sélection est valide
    if (!isSelectionValide()) {
      throw new MessageErreurException("L'adresse du fournisseur sélectionnée n'est pas valide.");
    }
    
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    initialiserRecherche();
    super.quitterAvecAnnulation();
  }
  
  /**
   * Charger la liste des identifiants d'adresses fournisseurs correspondant aux critères de recherche.
   */
  private void chargerListe() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeAdresseFournisseur != null) {
      taillePage = listeAdresseFournisseur.getTaillePage();
    }
    
    // Tout déselectionner
    listeIdAdresseFournisseur = null;
    listeAdresseFournisseur = null;
    ligneSelectionnee = -1;
    adresseFournisseurSelectionnee = null;
    rafraichir();
    
    // Charger les identifiants des adresses fournisseurs correspondants à la recherche
    listeIdAdresseFournisseur = ManagerServiceFournisseur.chargerListeIdAdresseFournisseur(getIdSession(), critereFournisseur);
    
    // Initier la liste des adresses fournisseurs à partir des identifiants
    listeAdresseFournisseur = ListeAdresseFournisseur.creerListeNonChargee(listeIdAdresseFournisseur);
    
    // Trier les adresses par identifiant
    listeAdresseFournisseur.trierParId();
    
    // Charger la première page
    listeAdresseFournisseur.chargerPremierePage(getIdSession(), taillePage);
  }
  
  /**
   * Afficher la plage d'adresses fournisseurs comprises entre deux lignes.
   * Les informations des adresses sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageAdresseFournisseurAffiches(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeAdresseFournisseur != null) {
      listeAdresseFournisseur.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
    }
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Affiche la liste des chantiers possibles à partir des critères de recherche.
   */
  public void modifierRechercheGenerique(String pRechercheGenerique) {
    pRechercheGenerique = Constantes.normerTexte(pRechercheGenerique);
    if (Constantes.equals(pRechercheGenerique, critereFournisseur.getTexteRechercheGenerique())) {
      return;
    }
    critereFournisseur.setTexteRechercheGenerique(pRechercheGenerique);
    chargerListe();
    rafraichir();
  }
  
  /**
   * Modifier le filtre sur le type d'adresse fournisseur
   */
  public void modifierTypeAdresseFournisseur(EnumTypeAdresseFournisseur pFiltreTypeAdresse) {
    if (pFiltreTypeAdresse == null) {
      critereFournisseur.setTypeAdresseFournisseur(null);
    }
    else if (Constantes.equals(critereFournisseur.getTypeAdresseFournisseur(), pFiltreTypeAdresse)) {
      return;
    }
    else {
      critereFournisseur.setTypeAdresseFournisseur(pFiltreTypeAdresse);
    }
  }
  
  public void initialiserRecherche() {
    critereFournisseur.initialiser();
    critereFournisseur.setTypeRecherche(CritereFournisseur.RECHERCHER_ADRESSE_FOURNISSEURS_DOCUMENTACHAT);
    listeIdAdresseFournisseur = null;
    listeAdresseFournisseur = null;
    rafraichir();
  }
  
  /**
   * Rechercher la liste des adresses des fournisseurs possibles à partir des critères de recherche.
   */
  public void rechercher() {
    chargerListe();
    rafraichir();
  }
  
  /**
   * Modifie l'indice de la ligne séléctionnée dans le tableau.
   */
  public void modifierLigneSelectionnee(int pLigneSelectionnee) {
    if (pLigneSelectionnee == ligneSelectionnee) {
      return;
    }
    
    // Mémoriser l'indice de la ligne sélectionnée
    ligneSelectionnee = pLigneSelectionnee;
    
    // Sélectionner l'adresse fournisseur correspondante à la ligne sélectionnée
    if (listeAdresseFournisseur != null && 0 <= pLigneSelectionnee && pLigneSelectionnee < listeAdresseFournisseur.size()) {
      adresseFournisseurSelectionnee = listeAdresseFournisseur.get(pLigneSelectionnee);
    }
    else {
      adresseFournisseurSelectionnee = null;
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Message d'information à afficher dans la boîte de dialogue.
   */
  public Message getTitreListe() {
    if (listeIdAdresseFournisseur == null) {
      return Message.getMessageNormal("Adresses fournisseurs correspondants à votre recherche");
    }
    else if (listeIdAdresseFournisseur.size() == 1) {
      return Message.getMessageNormal("Adresse fournisseur correspondant à votre recherche (1)");
    }
    else if (listeIdAdresseFournisseur.size() > 1) {
      return Message
          .getMessageNormal("Adresses fournisseurs correspondants à votre recherche (" + listeIdAdresseFournisseur.size() + ")");
    }
    else {
      return Message.getMessageImportant("Aucune adresse fournisseur ne correspond à votre recherche");
    }
  }
  
  /**
   * Message d'informatino pour expliquer pourquoi on a besoi nde sléectionner une adresse fournisseur.
   */
  public String getMessageInformation() {
    return messageInformation;
  }
  
  /**
   * Modifier le messgage d'information.
   */
  public void setMessageInformation(String pMessageInformation) {
    messageInformation = pMessageInformation;
  }
  
  /**
   * Critère de recherche des adresses fournisseurs.
   */
  public CritereFournisseur getCritereFournisseur() {
    return critereFournisseur;
  }
  
  /**
   * Fournir les critères de recherche des adresses fournisseurs à la boîte de dialogue.
   */
  public void setCritereFournisseur(CritereFournisseur pCritereFournisseur) {
    if (pCritereFournisseur != null) {
      critereFournisseur = pCritereFournisseur;
    }
    else {
      critereFournisseur = new CritereFournisseur();
    }
  }
  
  /**
   * Liste des adresses fournisseurs résultat de la recherche.
   */
  public List<IdAdresseFournisseur> getListeIdAdresseFournisseur() {
    return listeIdAdresseFournisseur;
  }
  
  /**
   * Fournir le résultat de la recherche à la boîte de dialogue.
   */
  public void setListeIdAdresseFournisseur(List<IdAdresseFournisseur> pListeIdAdresseFournisseur) {
    listeIdAdresseFournisseur = pListeIdAdresseFournisseur;
  }
  
  /**
   * Liste des adresses fournisseurs.
   */
  public List<AdresseFournisseur> getListeAdresseFournisseur() {
    return listeAdresseFournisseur;
  }
  
  /**
   * Numéro de la ligne sélectionnée.
   */
  public int getLigneSelectionnee() {
    return ligneSelectionnee;
  }
  
  /**
   * Identifiant de l'adresse fournisseur sélectionnée (null si aucune sélection).
   */
  public AdresseFournisseur getAdresseFournisseurSelectionnee() {
    return adresseFournisseurSelectionnee;
  }
  
  /**
   * Indique si l'adresse fournisseur sélectionnée est valide.
   */
  public boolean isSelectionValide() {
    return adresseFournisseurSelectionnee != null;
  }
  
  /**
   * Fournir le type d'adresse fournisseur à rechercher
   */
  public EnumTypeAdresseFournisseur getTypeAdresse() {
    return critereFournisseur.getTypeAdresseFournisseur();
  }
  
}
