/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin;

import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un magasin.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un magasin dans le logiciel. Il gère l'affichage des magasins existants
 * pour un magasin donné et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNMagasin à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le RGVX08FM_A4.
 */
public class SNMagasin extends SNComboBoxObjetMetier<IdMagasin, Magasin, ListeMagasin> {
  
  /**
   * Constructeur par défaut.
   */
  public SNMagasin() {
    super();
    
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeMagasin());
  }
  
  /**
   * Sélectionner un magasin de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampMagasin) {
    if (pChampMagasin == null || pLexical.HostFieldGetData(pChampMagasin) == null) {
      throw new MessageErreurException("Impossible de sélectionner le magasin car l'identifiant du champ est invalide.");
    }
    
    IdMagasin idMagasin = null;
    String valeurMagasin = pLexical.HostFieldGetData(pChampMagasin).trim();
    if (valeurMagasin != null && !valeurMagasin.trim().isEmpty() && !valeurMagasin.equals("**")) {
      idMagasin = IdMagasin.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampMagasin));
    }
    
    setIdSelection(idMagasin);
  }
  
  /**
   * Renseigner les champs RPG correspondant au magasin sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampMagasin) {
    renseignerChampRPG(pLexical, pChampMagasin, false);
  }
  
  /**
   * Renseigner les champs RPG correspondant au magasin sélectionné.
   * cette méthode permet de sp"cifier la valeur qui est envoyée au programme RPG comme valeur "Tous" :
   * - Si pTousAvecEtoile est à false, le champ RPG contient des espaces lorsque "Tous" est sélectionné".
   * - Si pTousAvecEtoile est à true, le champ RPG contient "**" lorsque "Tous" est sélectionné".
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampMagasin, boolean pTousAvecEtoile) {
    Magasin magasin = getSelection();
    if (magasin != null) {
      pLexical.HostFieldPutData(pChampMagasin, 0, magasin.getId().getCode());
    }
    else if (pTousAvecEtoile) {
      pLexical.HostFieldPutData(pChampMagasin, 0, "**");
    }
    else {
      pLexical.HostFieldPutData(pChampMagasin, 0, "");
    }
  }
  
}
