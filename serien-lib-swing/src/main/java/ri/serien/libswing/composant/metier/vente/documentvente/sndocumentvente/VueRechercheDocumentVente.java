/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.sndocumentvente;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.ModeleChantier;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.SNChantier;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.checkbox.SNCheckBoxReduit;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNIdentifiant;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de l'écran de recherche de documents de ventes.
 */
public class VueRechercheDocumentVente extends AbstractVueDialogue<ModeleDocumentVente> {
  private static final String[] TITRE_LISTE_DOCUMENT = new String[] { "Numéro", "Date", "Type", "Montant HT", "Montant TTC", "Client",
      "Référence longue", "Vendeur", "Référence chantier", "" };
  public static final String RECHERCHE_GENERIQUE_TOOLTIP = "<html>Recherche sur :<br>\r\n- La référence commande<br>\r\n</html>";
  private DefaultTableModel tableModelDocument = null;
  private JTableCellRendererDocumentVente listeDocumentsRenderer = new JTableCellRendererDocumentVente();
  private EnumTitreResultatDocumentVente enumTitreResultatDocumentVente = EnumTitreResultatDocumentVente.DOCUMENT_TROUVE;
  private boolean isErreurValidation = false;

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Constructeur.
   * @param pModele Modèle associé à la vue.
   */
  public VueRechercheDocumentVente(ModeleDocumentVente pModele) {
    super(pModele);
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public void initialiserComposants() {
    initComponents();
    tfNumeroDocument.setLongueur(IdDocumentVente.LONGUEUR_NUMERO_COMPLET);
    tableModelDocument = new DefaultTableModel(null, TITRE_LISTE_DOCUMENT) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex < 0);
      }
    };
    
    // Initialise l'aspect de la table des documents
    scpListeDocuments.getViewport().setBackground(Color.WHITE);
    scpListeDocuments.setBackground(Color.white);
    
    // Permet de redéfinir la touche Enter sur la liste documents
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER lance la recherce
        if (tblListeDocuments.getSelectedRowCount() == 0) {
          validerRecherche();
        }
        else {
          validerListeSelectionDocument();
        }
      }
    };
    pnlGeneral.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW).put(SNCharteGraphique.TOUCHE_ENTREE, "enter");
    pnlGeneral.getActionMap().put("enter", nouvelleAction);
    tblListeDocuments.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    tblListeDocuments.getTableHeader().setFont(new Font(tblListeDocuments.getTableHeader().getFont().getName(), Font.PLAIN, 14));
    
    tblListeDocuments.getTableHeader().addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        getModele().modifierPlageDocumentVenteAffiche(0, tblListeDocuments.getModel().getRowCount());
      }
    });
    
    // Permet de redéfinir la touche Bas et Haut sur la liste
    final Action originalActionHaut = retournerActionComposant(tblListeDocuments, SNCharteGraphique.TOUCHE_HAUT);
    final Action originalActionBas = retournerActionComposant(tblListeDocuments, SNCharteGraphique.TOUCHE_BAS);
    Action nouvelleActionFlecheHaut = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionHaut.actionPerformed(ae);
      }
    };
    Action nouvelleActionFlecheBas = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionBas.actionPerformed(ae);
      }
    };
    tblListeDocuments.modifierAction(nouvelleActionFlecheHaut, SNCharteGraphique.TOUCHE_HAUT);
    tblListeDocuments.modifierAction(nouvelleActionFlecheBas, SNCharteGraphique.TOUCHE_BAS);
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeDocuments.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeDocumentSelectionChanged(e);
      }
    });
    
    // Renseigner les types de documents de vente
    cbTypeDocument.removeAllItems();
    cbTypeDocument.addItem("Tous");
    cbTypeDocument.addItem(EnumTypeDocumentVente.DEVIS);
    cbTypeDocument.addItem(EnumTypeDocumentVente.COMMANDE);
    cbTypeDocument.addItem(EnumTypeDocumentVente.BON);
    cbTypeDocument.addItem(EnumTypeDocumentVente.FACTURE);

    // Ajouter un listener sur les changements d'affichage des lignes de la table
    scpListeDocuments.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
      @Override
      public void adjustmentValueChanged(AdjustmentEvent e) {
        scpListeDocumentsStateChanged(null);
      }
    });

    // Configurer la barre de boutons de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBoutonDocument(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBoutonDocument(pSNBouton);
      }
    });
    
    snPlageDateDocument.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    requestFocus();
  }
  
  @Override
  public void rafraichir() {
    // Rafraîchir filtre colonne 1
    rafraichirNumeroDocument();
    rafraichirTypeDocument();
    rafraichirNomCLientDocument();
    rafraichirArticleDocument();
    rafraichirChantierDocument();
    // Rafraîchir filtre colonne 2
    rafraichirDateCreationDocument();
    // Rafraîchir tableau
    rafraichirTitreResultatDocument();
    rafraichirTableauResultatDocument();
    // Rafraîchir boutons
    rafraichirBoutonValider();
    
    // Gérer le focus
    if ((getModele().getListeDocumentVenteBase() != null) && (!getModele().getListeDocumentVenteBase().isEmpty())) {
      tblListeDocuments.requestFocusInWindow();
    }
  }
  
  /**
   * Rafraîchir le numéro de document.
   */
  private void rafraichirNumeroDocument() {
    String valeur = "";
    if (getModele().getNumeroDocument() > 0) {
      valeur = Constantes.convertirIntegerEnTexte(getModele().getNumeroDocument(), 0);
    }
    tfNumeroDocument.setText(valeur);
    tfNumeroDocument.setEnabled(isDonneesChargees());
  }

  /**
   * Rafraîchir le type de document.
   */
  private void rafraichirTypeDocument() {
    if (getModele().getTypeDocumentVente() != EnumTypeDocumentVente.NON_DEFINI) {
      cbTypeDocument.setSelectedItem(getModele().getTypeDocumentVente());
    }
    else {
      cbTypeDocument.setSelectedItem("Tous");
    }
    cbTypeDocument.setEnabled(isDonneesChargees());
  }
  
  /**
   * Rafraîchir la case à cocher de l'affichage des documents historisés.
   */
  private void rafraichirDocumentHistorise() {
    snCheckBoxDocumentHistorise.setSelected(getModele().isAfficherDocumentHistorise());
    snCheckBoxDocumentHistorise.setEnabled(isDonneesChargees());
  }

  /**
   * Rafraîchir le nom du client.
   */
  private void rafraichirNomCLientDocument() {
    if (getModele().getIdEtablissement() == null) {
      return;
    }
    snClient.setSession(getModele().getSession());
    snClient.setIdEtablissement(getModele().getIdEtablissement());
    snClient.charger(false);
    snClient.setSelection(getModele().getClientBase());
  }

  /**
   * Rafraîchir l'article.
   */
  private void rafraichirArticleDocument() {
    if (getModele().getIdEtablissement() == null) {
      return;
    }
    snArticleDocument.setSession(getModele().getSession());
    snArticleDocument.setIdEtablissement(getModele().getIdEtablissement());
    snArticleDocument.charger(false);

    if (getModele().getIdArticle() == null) {
      snArticleDocument.initialiserRecherche();
    }

    snArticleDocument.setEnabled(isDonneesChargees());
  }
  
  /**
   * Rafraîchir le chantier.
   */
  private void rafraichirChantierDocument() {
    if (getModele().getIdEtablissement() == null) {
      return;
    }
    snChantierDocument.setSession(getModele().getSession());
    snChantierDocument.setIdEtablissement(getModele().getIdEtablissement());
    if (getModele().getClientBase() != null) {
      snChantierDocument.setIdClient(getModele().getClientBase().getId());
    }
    else {
      snChantierDocument.setIdClient(null);
    }
    snChantierDocument.setModeComposant(ModeleChantier.MODE_FILTRE_RECHERCHE);
    snChantierDocument.charger(false);
    snChantierDocument.setSelection(getModele().getChantier());
  }
  
  /**
   * Rafraîchir la plage de date de création du document.
   */
  private void rafraichirDateCreationDocument() {
    snPlageDateDocument.setEnabled(isDonneesChargees());
    snPlageDateDocument.setDateDebut(getModele().getDateDebut());
    snPlageDateDocument.setDateFin(getModele().getDateFin());
  }
  
  /**
   * Rafraîchir le titre du résultat de recherche.
   */
  private void rafraichirTitreResultatDocument() {
    if (getModele().getTitreResultat() != null) {
      lblTitreResultat.setMessage(getModele().getTitreResultat());
    }
    lblTitreResultat.setText(String.format(enumTitreResultatDocumentVente.getLibelle(), getModele().getNombreResultat()));
  }

  /**
   * Rafraîchir le tableau contenant la liste de documents.
   */
  private void rafraichirTableauResultatDocument() {
    tableModelDocument.setRowCount(0);
    rafraichirListeDocument();
  }
  
  /**
   * Exécuter la recherche.
   */
  private void validerRecherche() {
    try {
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
      if (snArticleDocument.getSelection() != null) {
        getModele().modifierArticle(snArticleDocument.getSelection().getId());
      }
      getModele().rechercher();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charger les données dans la liste.
   */
  private void rafraichirListeDocument() {
    String[][] donnees = null;
    
    // Convertir les données à afficher dans le format attendu par le tableau
    ListeDocumentVenteBase listeDocumentVenteBase = getModele().getListeDocumentVenteBase();
    if (listeDocumentVenteBase != null && !listeDocumentVenteBase.isEmpty()) {
      donnees = new String[listeDocumentVenteBase.size()][TITRE_LISTE_DOCUMENT.length];
      for (int i = 0; i < listeDocumentVenteBase.size(); i++) {
        DocumentVenteBase documentVenteBase = listeDocumentVenteBase.get(i);
        String[] ligne = donnees[i];
        
        // Numéro du document
        ligne[0] = documentVenteBase.getId().toString();
        
        // Date de création
        if (documentVenteBase.isVerrouille()) {
          ligne[1] = "Verrouillé";
        }
        else if (documentVenteBase.getDateCreation() != null) {
          if (documentVenteBase.isFacture()) {
            ligne[1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateFacturation());
          }
          else {
            ligne[1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
          }
        }
        
        // Type du document et mode d'expédition
        String type = "";
        if (documentVenteBase.getTypeDocumentVente() != null) {
          type += documentVenteBase.getTypeDocumentVente().getLibelle();
        }
        if (documentVenteBase.geIdModeExpedition() != null && getModele().getListeModeExpedition() != null) {
          ModeExpedition modeExpedition =
              getModele().getListeModeExpedition().getModeExpeditionParId(documentVenteBase.geIdModeExpedition());
          if (modeExpedition != null) {
            type += " - " + modeExpedition.getLibelle();
          }
        }
        ligne[2] = type;
        
        // Montant du document HT
        if (documentVenteBase.getTotalHT() != null) {
          ligne[3] = Constantes.formater(documentVenteBase.getTotalHT(), true);
        }
        
        // Montant du document TTC
        if (documentVenteBase.getTotalTTC() != null) {
          ligne[4] = Constantes.formater(documentVenteBase.getTotalTTC(), true);
        }
        
        // Nom du client
        if (documentVenteBase.getComplement() instanceof ClientBase) {
          ClientBase clientBase = (Client) documentVenteBase.getComplement();
          if (clientBase.getAdresse() != null && clientBase.getAdresse().getNom() != null) {
            ligne[5] = clientBase.getAdresse().getNom();
            if (clientBase.getTypeCompteClient() != null) {
              ligne[5] += " (" + clientBase.getTypeCompteClient().getLibelle() + ")";
            }
          }
        }
        
        // Référence longue
        ligne[6] = documentVenteBase.getReferenceLongue();
        
        // Code vendeur
        if (documentVenteBase.getIdVendeur() != null) {
          ligne[7] = documentVenteBase.getIdVendeur().getCode();
        }
        
        // Libellé chantier
        if (documentVenteBase.getIdChantier() != null) {
          ligne[8] = documentVenteBase.getLibelleChantier();
        }
        
        // Etat du document (vert = entièrement traité, orange = partiellement traité, blanc = pas traité du tout).
        if (documentVenteBase.getLibelleEtat() != null) {
          ligne[9] = documentVenteBase.getLibelleEtat();
        }
      }
    }
    else {
      // Se positionner en haut du tableau si la liste est vide
      scpListeDocuments.getViewport().reshape(0, 0, scpListeDocuments.getViewport().getWidth(), 0);
      scpListeDocuments.getVerticalScrollBar().setValue(0);
      tblListeDocuments.getRowSorter().setSortKeys(null);
    }
    
    // Mettre à jour les données
    tableModelDocument.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelDocument.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblListeDocuments.getModel().equals(tableModelDocument)) {
      tblListeDocuments.setModel(tableModelDocument);
      tblListeDocuments.setGridColor(new Color(204, 204, 204));
      tblListeDocuments.setDefaultRenderer(Object.class, listeDocumentsRenderer);
      tblListeDocuments.trierColonnes();
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblListeDocuments.getRowCount() == 0) {
      listeDocumentsRenderer.redimensionnerColonnes(tblListeDocuments);
    }
    
    // Sélectionner la ligne en cours
    int index = getModele().getIndexDocumentSelectionne();
    if (index > -1 && index < tblListeDocuments.getRowCount()) {
      tblListeDocuments.getSelectionModel().setSelectionInterval(index, index);
    }
  }
  
  /**
   * Afficher le bouton Valider.
   */
  private void rafraichirBoutonValider() {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, isDonneesChargees() && tblListeDocuments.getSelectedRow() >= 0);
  }
  
  /**
   * Récupérer le document sélectionné.
   */
  private void validerListeSelectionDocument() {
    int indexvisuel = tblListeDocuments.getSelectedRow();
    
    if (indexvisuel >= 0) {
      int indexreel = indexvisuel;
      if (tblListeDocuments.getRowSorter() != null) {
        indexreel = tblListeDocuments.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().modifierDocumentSelectionne(indexreel);
      getModele().quitterAvecValidation();
    }
  }
  
  /**
   * Sélectionner la ligne sur laquelle on a double cliqué.
   */
  private void selectionnerLigne(MouseEvent e) {
    // Rafraîchir boutons
    rafraichirBoutonValider();
    
    if (e.getClickCount() == 2) {
      validerListeSelectionDocument();
    }
  }

  /**
   * Contrôler le numéro de document saisie.<br>
   * Cette méthode définit le message d'erreur à afficher.
   */
  private void controlerNumeroDocument() {
    isErreurValidation = false;
    enumTitreResultatDocumentVente = EnumTitreResultatDocumentVente.DOCUMENT_TROUVE;
    CritereDocumentVente critereDocumentVente = getModele().getCritereDocumentVente();
    if (critereDocumentVente != null && critereDocumentVente.isComposantDePlage() && !Constantes.equals(tfNumeroDocument.getText(), "")) {
      if (critereDocumentVente.getNumeroDocumentDebut() != 0
          && critereDocumentVente.getNumeroDocumentDebut() > Constantes.convertirTexteEnInteger(tfNumeroDocument.getText())) {
        enumTitreResultatDocumentVente = EnumTitreResultatDocumentVente.NUMERO_INFERIEUR_DEBUT;
        isErreurValidation = true;
      }
      else if (critereDocumentVente.getNumeroDocumentFin() != 0
          && critereDocumentVente.getNumeroDocumentFin() < Constantes.convertirTexteEnInteger(tfNumeroDocument.getText())) {
        enumTitreResultatDocumentVente = EnumTitreResultatDocumentVente.NUMERO_SUPERIEUR_FIN;
        isErreurValidation = true;
      }
      else if (critereDocumentVente.getNumeroFactureDebut() != 0
          && critereDocumentVente.getNumeroFactureDebut() > Constantes.convertirTexteEnInteger(tfNumeroDocument.getText())) {
        enumTitreResultatDocumentVente = EnumTitreResultatDocumentVente.FACTURE_INFERIEUR_DEBUT;
        isErreurValidation = true;
      }
      else if (critereDocumentVente.getNumeroFactureFin() != 0
          && critereDocumentVente.getNumeroFactureFin() < Constantes.convertirTexteEnInteger(tfNumeroDocument.getText())) {
        enumTitreResultatDocumentVente = EnumTitreResultatDocumentVente.FACTURE_SUPERIEUR_FIN;
        isErreurValidation = true;
      }
      rafraichirTitreResultatDocument();
    }
  }
  
  /**
   * Retourner l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }

  /**
   * Effectuer l'action adéquat sur l'appuie d'un bouton.
   * @param pSNBouton Bouton concerné.
   */
  private void btTraiterClicBoutonDocument(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        validerListeSelectionDocument();
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        if (!snClient.hasFocus()) {
          controlerNumeroDocument();
          if (!isErreurValidation) {
            getModele().rechercher();
          }
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
        tableModelDocument.setRowCount(0);
        RowSorter<?> rs = tblListeDocuments.getRowSorter();
        rs.setSortKeys(null);
        rafraichirListeDocument();
        snClient.initialiserRecherche();
        snArticleDocument.initialiserRecherche();
        snChantierDocument.initialiserRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Capturer les évènements sur les touches de direction.
   */
  private void keyFleches(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
    try {
      int indexLigneSeletionnee = tblListeDocuments.getSelectedRow();
      if (indexLigneSeletionnee == -1) {
        return;
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Capturer l'évènement sur le scroll pane.
   */
  private void scpListeDocumentsStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeDocuments == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeDocuments.getViewport().getViewRect();
      int premiereLigne = tblListeDocuments.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeDocuments.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeDocuments.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageDocumentVenteAffiche(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Effectuer le changement de sélection dans le tableau résultat des documents.
   */
  private void tblListeDocumentSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      // Informer le modèle
      getModele().modifierDocumentSelectionne(tblListeDocuments.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Effectuer le changement de valeur du champ numéro de document lorsque le curseur n'est plus focalisé dessus.
   */
  private void tfNumeroDocumentFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Effectuer le changement de valeur du champ article lorsque le curseur n'est plus focalisé dessus.
   */
  private void tfArticleFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierArticle(snArticleDocument.getSelection().getId());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Effectuer le changement de valeur du type de document.
   */
  private void cbTypeDocumentItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs() || e.getStateChange() != ItemEvent.SELECTED) {
        return;
      }
      if (cbTypeDocument.getSelectedItem() != null && cbTypeDocument.getSelectedItem() instanceof EnumTypeDocumentVente) {
        getModele().modifierTypeDocumentVente((EnumTypeDocumentVente) cbTypeDocument.getSelectedItem());
      }
      else {
        getModele().modifierTypeDocumentVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Effectuer la sélection d'une ligne sur la liste de documents au clic de la souris.
   */
  private void tblListeDocumentsMouseClicked(MouseEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      selectionnerLigne(e);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Effectuer le changement du client sur le champ client.
   */
  private void snClientValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierClient(snClient.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Effectuer le changement sur le champ article lorsque sa valeur change.
   */
  private void snArticleDocumentValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (snArticleDocument.getSelection() != null) {
        getModele().modifierArticle(snArticleDocument.getSelection().getId());
      }
      else {
        getModele().modifierArticle(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Effectuer le changement sur le champ chantier lorsque sa valeur change.
   */
  private void snChantierDocumentValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierChantier(snChantierDocument.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Effectuer le changement sur la plage de date lorsque ses valeurs changent.
   */
  private void snPlageDateDocumentValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierDateCreationDebut(snPlageDateDocument.getDateDebut());
      getModele().modifierDateCreationFin(snPlageDateDocument.getDateFin());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Effectuer le changement sur la case à cocher d'affichage de documents historisés.
   */
  private void snCheckBoxDocumentHistoriseStateChanged(ChangeEvent e) {
    try {
      getModele().modifierAffichageDocumentHistorise(snCheckBoxDocumentHistorise.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlGeneral = new JPanel();
    pnlContenuDocument = new JPanel();
    pnlFiltre = new JPanel();
    pnlFiltre1 = new JPanel();
    lbNumeroDocument = new SNLabelChamp();
    tfNumeroDocument = new SNIdentifiant();
    lbTypeDocument = new SNLabelChamp();
    cbTypeDocument = new SNComboBox();
    lbClient = new SNLabelChamp();
    snClient = new SNClient();
    lbChantier = new SNLabelChamp();
    snChantierDocument = new SNChantier();
    lbArticle = new SNLabelChamp();
    snArticleDocument = new SNArticle();
    pnlFiltre2 = new JPanel();
    ldDateCreation = new SNLabelChamp();
    snPlageDateDocument = new SNPlageDate();
    snCheckBoxDocumentHistorise = new SNCheckBoxReduit();
    snBarreRecherche = new SNBarreRecherche();
    lblTitreResultat = new SNLabelTitre();
    scpListeDocuments = new JScrollPane();
    tblListeDocuments = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlGeneral ========
    {
      pnlGeneral.setOpaque(false);
      pnlGeneral.setBackground(new Color(238, 238, 210));
      pnlGeneral.setName("pnlGeneral");
      pnlGeneral.setLayout(new CardLayout());
      
      // ======== pnlContenuDocument ========
      {
        pnlContenuDocument.setBackground(new Color(238, 238, 210));
        pnlContenuDocument.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenuDocument.setOpaque(false);
        pnlContenuDocument.setName("pnlContenuDocument");
        pnlContenuDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenuDocument.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenuDocument.getLayout()).rowHeights = new int[] { 0, 11, 279, 0 };
        ((GridBagLayout) pnlContenuDocument.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenuDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlFiltre ========
        {
          pnlFiltre.setOpaque(false);
          pnlFiltre.setBorder(new TitledBorder(""));
          pnlFiltre.setBackground(new Color(238, 238, 210));
          pnlFiltre.setName("pnlFiltre");
          pnlFiltre.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlFiltre1 ========
          {
            pnlFiltre1.setOpaque(false);
            pnlFiltre1.setBackground(new Color(238, 238, 210));
            pnlFiltre1.setName("pnlFiltre1");
            pnlFiltre1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDocument ----
            lbNumeroDocument.setText("Num\u00e9ro document");
            lbNumeroDocument.setName("lbNumeroDocument");
            pnlFiltre1.add(lbNumeroDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfNumeroDocument ----
            tfNumeroDocument.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
            tfNumeroDocument.setName("tfNumeroDocument");
            tfNumeroDocument.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfNumeroDocumentFocusLost(e);
              }
            });
            pnlFiltre1.add(tfNumeroDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTypeDocument ----
            lbTypeDocument.setText("Type document");
            lbTypeDocument.setName("lbTypeDocument");
            pnlFiltre1.add(lbTypeDocument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbTypeDocument ----
            cbTypeDocument.setMinimumSize(new Dimension(150, 30));
            cbTypeDocument.setPreferredSize(new Dimension(150, 30));
            cbTypeDocument.setName("cbTypeDocument");
            cbTypeDocument.addItemListener(e -> cbTypeDocumentItemStateChanged(e));
            pnlFiltre1.add(cbTypeDocument, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClient ----
            lbClient.setText("Client");
            lbClient.setName("lbClient");
            pnlFiltre1.add(lbClient, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snClient ----
            snClient.setName("snClient");
            snClient.addSNComposantListener(e -> snClientValueChanged(e));
            pnlFiltre1.add(snClient, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbChantier ----
            lbChantier.setText("Chantier");
            lbChantier.setName("lbChantier");
            pnlFiltre1.add(lbChantier, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snChantierDocument ----
            snChantierDocument.setName("snChantierDocument");
            snChantierDocument.addSNComposantListener(e -> snChantierDocumentValueChanged(e));
            pnlFiltre1.add(snChantierDocument, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticle ----
            lbArticle.setText("Article");
            lbArticle.setName("lbArticle");
            pnlFiltre1.add(lbArticle, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snArticleDocument ----
            snArticleDocument.setName("snArticleDocument");
            snArticleDocument.addSNComposantListener(e -> snArticleDocumentValueChanged(e));
            pnlFiltre1.add(snArticleDocument, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre.add(pnlFiltre1);
          
          // ======== pnlFiltre2 ========
          {
            pnlFiltre2.setOpaque(false);
            pnlFiltre2.setName("pnlFiltre2");
            pnlFiltre2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- ldDateCreation ----
            ldDateCreation.setText("Date de cr\u00e9ation");
            ldDateCreation.setName("ldDateCreation");
            pnlFiltre2.add(ldDateCreation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPlageDateDocument ----
            snPlageDateDocument.setName("snPlageDateDocument");
            snPlageDateDocument.addSNComposantListener(e -> snPlageDateDocumentValueChanged(e));
            pnlFiltre2.add(snPlageDateDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCheckBoxDocumentHistorise ----
            snCheckBoxDocumentHistorise.setText("Afficher les documents historis\u00e9s");
            snCheckBoxDocumentHistorise.setToolTipText("Cocher pour inclure les documents historis\u00e9s \u00e0 la recherche");
            snCheckBoxDocumentHistorise.setSelected(true);
            snCheckBoxDocumentHistorise.setName("snCheckBoxDocumentHistorise");
            snCheckBoxDocumentHistorise.addChangeListener(e -> snCheckBoxDocumentHistoriseStateChanged(e));
            pnlFiltre2.add(snCheckBoxDocumentHistorise, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlFiltre2.add(snBarreRecherche, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlFiltre.add(pnlFiltre2);
        }
        pnlContenuDocument.add(pnlFiltre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lblTitreResultat ----
        lblTitreResultat.setText("Documents correspondant \u00e0 votre recherche");
        lblTitreResultat.setName("lblTitreResultat");
        pnlContenuDocument.add(lblTitreResultat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeDocuments ========
        {
          scpListeDocuments.setPreferredSize(new Dimension(1050, 424));
          scpListeDocuments.setFont(new Font("sansserif", Font.PLAIN, 14));
          scpListeDocuments.setName("scpListeDocuments");
          
          // ---- tblListeDocuments ----
          tblListeDocuments.setShowVerticalLines(true);
          tblListeDocuments.setShowHorizontalLines(true);
          tblListeDocuments.setBackground(Color.white);
          tblListeDocuments.setRowHeight(20);
          tblListeDocuments.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeDocuments.setAutoCreateRowSorter(true);
          tblListeDocuments.setSelectionBackground(new Color(57, 105, 138));
          tblListeDocuments.setGridColor(new Color(204, 204, 204));
          tblListeDocuments.setName("tblListeDocuments");
          tblListeDocuments.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeDocumentsMouseClicked(e);
            }
          });
          scpListeDocuments.setViewportView(tblListeDocuments);
        }
        pnlContenuDocument.add(scpListeDocuments, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlGeneral.add(pnlContenuDocument, "card1");
    }
    contentPane.add(pnlGeneral, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    contentPane.add(snBarreBouton, BorderLayout.SOUTH);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlGeneral;
  private JPanel pnlContenuDocument;
  private JPanel pnlFiltre;
  private JPanel pnlFiltre1;
  private SNLabelChamp lbNumeroDocument;
  private SNIdentifiant tfNumeroDocument;
  private SNLabelChamp lbTypeDocument;
  private SNComboBox cbTypeDocument;
  private SNLabelChamp lbClient;
  private SNClient snClient;
  private SNLabelChamp lbChantier;
  private SNChantier snChantierDocument;
  private SNLabelChamp lbArticle;
  private SNArticle snArticleDocument;
  private JPanel pnlFiltre2;
  private SNLabelChamp ldDateCreation;
  private SNPlageDate snPlageDateDocument;
  private SNCheckBoxReduit snCheckBoxDocumentHistorise;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lblTitreResultat;
  private JScrollPane scpListeDocuments;
  private NRiTable tblListeDocuments;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
