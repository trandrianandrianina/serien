/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.glasspane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

/**
 * Un GlassPane qui affiche une animation en forme de soleil pour faire patienter pendant une longue tâche.
 *
 * An infinite progress panel displays a rotating figure and a message to notice
 * the user of a long, duration unknown task. The shape and the text are drawn
 * upon a white veil which alpha level (or shield value) lets the underlying
 * component shine through. This panel is meant to be used asa <i>glass pane</i>
 * in the window performing the long operation.
 *
 * <p>Contrary to regular glass panes, you don't need to set it visible or not by
 * yourself. Once you've started the animation all the mouse events are
 * intercepted by this panel, preventing them from being forwared to the
 * underlying components.
 *
 * <p>The panel can be controlled by the <code>start()</code>, <code>stop()</code>
 * and <code>interrupt()</code> methods.
 *
 * <p>Example:
 *
 * <pre>
 * GlassPaneSoleilAttente pane = new GlassPaneSoleilAttente();
 * frame.setGlassPane(pane);
 * ... later in some other EDT event (otherwise the panel doesn't know the size and draws real funky)
 * pane.start()
 * </pre>
 *
 * <p>Several properties can be configured at creation time. The message and its
 * font can be changed at runtime. Changing the font can be done using
 * <code>setFont()</code> and <code>setForeground()</code>.
 */

public class GlassPaneSoleilAttente extends JComponent implements MouseListener {
  /** Contains the bars composing the circular shape. */
  protected Ticker ticker = null;
  /**
   * The animation thread is responsible for fade in/out and rotation.
   */
  protected Thread animation = null;
  /**
   * Notifies whether the animation is running or not.
   */
  protected boolean started = false;
  /**
   * Alpha level of the veil, used for fade in/out.
   */
  protected int alphaLevel = 0;
  /**
   * Duration of the veil's fade in/out.
   */
  protected int rampDelay = 300;
  /**
   * Alpha level of the veil.
   */
  protected float shield = 0.70f;
  /**
   * Message displayed below the circular shape.
   */
  protected String text = "";
  /**
   * Amount of bars composing the circular shape.
   */
  protected int barsCount = 14;
  /**
   * Amount of frames per seconde. Lowers this to save CPU.
   */
  protected float fps = 15.0f;
  /**
   * Rendering hints to set anti aliasing.
   */
  protected RenderingHints hints = null;
  /**
   * Ratio si le conteneur est trop petit.
   */
  private float ratio = 1f;

  /**
   * Creates a new progress panel with default values:.
   * <ul>
   * <li>No message</li>
   * <li>14 bars</li>
   * <li>Veil's alpha level is 70%</li>
   * <li>15 frames per second</li>
   * <li>Fade in/out last 300 ms</li>
   * </ul>
   */
  public GlassPaneSoleilAttente() {
    this("");
  }

  /**
   * Creates a new progress panel with default values:<br />
   * <ul>
   * <li>14 bars</li>
   * <li>Veil's alpha level is 70%</li>
   * <li>15 frames per second</li>
   * <li>Fade in/out last 300 ms</li>
   * </ul>
   *
   * @param text
   *          The message to be displayed. Can be null or empty.
   */
  public GlassPaneSoleilAttente(String text) {
    this(text, 14);
  }

  /**
   * Creates a new progress panel with default values:<br />
   * <ul>
   * <li>Veil's alpha level is 70%</li>
   * <li>15 frames per second</li>
   * <li>Fade in/out last 300 ms</li>
   * </ul>
   *
   * @param text
   *          The message to be displayed. Can be null or empty.
   * @param barsCount
   *          The amount of bars composing the circular shape
   */
  public GlassPaneSoleilAttente(String text, int barsCount) {
    this(text, barsCount, 0.70f);
  }

  /**
   * Creates a new progress panel with default values:<br />
   * <ul>
   * <li>15 frames per second</li>
   * <li>Fade in/out last 300 ms</li>
   * </ul>
   *
   * @param text
   *          The message to be displayed. Can be null or empty.
   * @param barsCount
   *          The amount of bars composing the circular shape.
   * @param shield
   *          The alpha level between 0.0 and 1.0 of the colored shield (or
   *          veil).
   */
  public GlassPaneSoleilAttente(String text, int barsCount, float shield) {
    this(text, barsCount, shield, 15.0f);
  }

  /**
   * Creates a new progress panel with default values:<br />
   * <ul>
   * <li>Fade in/out last 300 ms</li>
   * </ul>
   *
   * @param text
   *          The message to be displayed. Can be null or empty.
   * @param barsCount
   *          The amount of bars composing the circular shape.
   * @param shield
   *          The alpha level between 0.0 and 1.0 of the colored shield (or
   *          veil).
   * @param fps
   *          The number of frames per second. Lower this value to decrease
   *          CPU usage.
   */
  public GlassPaneSoleilAttente(String text, int barsCount, float shield, float fps) {
    this(text, barsCount, shield, fps, 300);
  }

  /**
   * Creates a new progress panel.
   *
   * @param text
   *          The message to be displayed. Can be null or empty.
   * @param barsCount
   *          The amount of bars composing the circular shape.
   * @param shield
   *          The alpha level between 0.0 and 1.0 of the colored shield (or
   *          veil).
   * @param fps
   *          The number of frames per second. Lower this value to decrease
   *          CPU usage.
   * @param rampDelay
   *          The duration, in milli seconds, of the fade in and the fade
   *          out of the veil.
   */
  public GlassPaneSoleilAttente(String text, int barsCount, float shield, float fps, int rampDelay) {
    this.text = text;
    this.rampDelay = rampDelay >= 0 ? rampDelay : 0;
    this.shield = shield >= 0.0f ? shield : 0.0f;
    this.fps = fps > 0.0f ? fps : 15.0f;
    this.barsCount = barsCount > 0 ? barsCount : 14;

    this.hints = new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    this.hints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    this.hints.put(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    // setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
  }

  /**
   * Changes the displayed message at runtime.
   *
   * @param text
   *          The message to be displayed. Can be null or empty.
   */
  public void setText(String text) {
    repaint();
    this.text = text;
  }

  /**
   * Returns the current displayed message.
   */
  public String getText() {
    return text;
  }

  /**
   * Starts the waiting animation by fading the veil in, then rotating the
   * shapes. This method handles the visibility of the glass pane.
   */
  public void start() {
    addMouseListener(this);
    setVisible(true);
    buildTicker();
    animation = new Thread(new Animator(true));
    animation.start();
  }

  /**
   * Stops the waiting animation by stopping the rotation of the circular
   * shape and then by fading out the veil. This methods sets the panel
   * invisible at the end.
   */
  public void stop() {
    if (animation != null) {
      animation.interrupt();
      try {
        animation.join();
      }
      catch (InterruptedException e) {
        Thread.currentThread().interrupt(); // Très important de réinterrompre (ajouté après coup)
      }
      animation = null;

      animation = new Thread(new Animator(false));
      animation.start();
    }
  }

  public JComponent getComponent() {
    return this;
  }

  /**
   * Interrupts the animation, whatever its state is. You can use it when you
   * need to stop the animation without running the fade out phase. This
   * methods sets the panel invisible at the end.
   */
  public void interrupt() {
    if (animation != null) {
      animation.interrupt();
      animation = null;

      removeMouseListener(this);
      setVisible(false);
    }
  }

  @Override
  public void paintComponent(Graphics g) {
    if (started) {
      int width = getWidth();
      int height = getHeight();

      if (width == 0 || height == 0) {
        return;
      }

      Ticker ticker = getTicker();
      if (ticker == null) {
        return;
      }

      Graphics2D g2 = (Graphics2D) g;
      g2.setRenderingHints(hints);

      g2.setColor(new Color(255, 255, 255, (int) (alphaLevel * shield)));
      g2.fillRect(0, 0, getWidth(), getHeight());

      for (int i = 0; i < ticker.bars.length; i++) {
        int channel = 224 - 128 / (i + 1);
        g2.setColor(new Color(channel, channel, channel, alphaLevel));
        g2.fill(ticker.bars[i]);
      }

      drawTextAt(text, getFont(), g2, width, ticker.maxY, getForeground());
    }
  }

  /**
   * Draw text in a Graphics2D.
   *
   * @param text
   *          the text to draw
   * @param font
   *          the font to use
   * @param g2
   *          the graphics context to draw in
   * @param width
   *          the width of the parent, so it can be centered
   * @param y
   *          the height at which to draw
   * @param foreGround
   *          the foreground color to draw in
   * @return the y value that is the y param + the text height.
   */
  public static double drawTextAt(String text, Font font, Graphics2D g2, int width, double y, Color foreGround) {
    if (text != null && text.length() > 0) {
      FontRenderContext context = g2.getFontRenderContext();
      TextLayout layout = new TextLayout(text, font, context);
      Rectangle2D bounds = layout.getBounds();
      g2.setColor(foreGround);
      float textX = (float) (width - bounds.getWidth()) / 2;
      y = (float) (y + layout.getLeading() + 2 * layout.getAscent());
      layout.draw(g2, textX, (float) y);
    }
    return y;
  }

  /**
   * Ticker is not built until set bounds is called (or our width and height
   * are > 0).
   *
   * @return null if not ready, or the built ticker
   */
  private Ticker getTicker() {
    if (ticker == null) {
      buildTicker();
    }
    return ticker;
  }

  /**
   * Builds the circular shape and returns the result as an array of
   * <code>Area</code>. Each <code>Area</code> is one of the bars composing
   * the shape.
   */
  private void buildTicker() {
    Area[] areas = new Area[barsCount];
    int width = getWidth();
    int height = getHeight();
    // getPreferredSize());
    // Sometimes the bounds are set, rebuild the ticker later.
    if (width == 0 || height == 0) {
      return;
    }
    // On calule le ratio (simple)
    ratio = 1f;
    if ((width < 200) || (height < 200)) {
      ratio = 0.5f;
    }

    Point2D.Double center = new Point2D.Double((double) width / 2, (double) height / 2);
    double fixedAngle = 2.0 * Math.PI / (barsCount);
    double maxY = 0.0d;
    for (double i = 0.0; i < barsCount; i++) {
      Area primitive = buildPrimitive();

      AffineTransform toCenter = AffineTransform.getTranslateInstance(center.getX(), center.getY());
      // AffineTransform toBorder = AffineTransform.getTranslateInstance(45.0, -6.0);
      AffineTransform toBorder = AffineTransform.getTranslateInstance(45.0 * ratio, -6.0 * ratio);

      AffineTransform toWheel = new AffineTransform();
      toWheel.concatenate(toCenter);
      toWheel.concatenate(toBorder);
      primitive.transform(toWheel);

      AffineTransform toCircle = AffineTransform.getRotateInstance(-i * fixedAngle, center.getX(), center.getY());
      primitive.transform(toCircle);

      areas[(int) i] = primitive;

      Rectangle2D bounds = primitive.getBounds2D();
      if (bounds.getMaxY() > maxY) {
        maxY = bounds.getMaxY();
      }
    }

    ticker = new Ticker();
    ticker.bars = areas;
    ticker.maxY = maxY;
  }

  /**
   * Builds a bar.
   */
  private Area buildPrimitive() {
    Rectangle2D.Double body = new Rectangle2D.Double(6 * ratio, 0, 30 * ratio, 12 * ratio);
    Ellipse2D.Double head = new Ellipse2D.Double(0, 0, 12 * ratio, 12 * ratio);
    Ellipse2D.Double tail = new Ellipse2D.Double(30 * ratio, 0, 12 * ratio, 12 * ratio);

    /*
    Rectangle2D.Double body = new Rectangle2D.Double(6, 0, 30, 12);
    Ellipse2D.Double head = new Ellipse2D.Double(0, 0, 12, 12);
    Ellipse2D.Double tail = new Ellipse2D.Double(30, 0, 12, 12);
    */

    Area tick = new Area(body);
    tick.add(new Area(head));
    tick.add(new Area(tail));

    return tick;
  }

  private class Ticker {
    double maxY = 0.0;
    Area[] bars;
  }

  /**
   * Animation thread.
   */
  private class Animator implements Runnable {
    private boolean rampUp = true;

    protected Animator(boolean rampUp) {
      this.rampUp = rampUp;
    }

    // @Override
    @Override
    public void run() {
      Ticker ticker = getTicker();
      if (ticker == null) {
        return;
      }

      Point2D.Double center = new Point2D.Double((double) getWidth() / 2, (double) getHeight() / 2);
      double fixedIncrement = 2.0 * Math.PI / (barsCount);
      AffineTransform toCircle = AffineTransform.getRotateInstance(fixedIncrement, center.getX(), center.getY());

      long start = System.currentTimeMillis();
      if (rampDelay == 0) {
        alphaLevel = rampUp ? 255 : 0;
      }

      started = true;
      boolean inRamp = rampUp;

      while (!Thread.interrupted()) {
        if (!inRamp) {
          for (int i = 0; i < ticker.bars.length; i++) {
            ticker.bars[i].transform(toCircle);
          }
        }

        repaint();

        if (rampUp) {
          if (alphaLevel < 255) {
            alphaLevel = (int) (255 * (System.currentTimeMillis() - start) / rampDelay);
            if (alphaLevel >= 255) {
              alphaLevel = 255;
              inRamp = false;
            }
          }
        }
        else if (alphaLevel > 0) {
          alphaLevel = (int) (255 - (255 * (System.currentTimeMillis() - start) / rampDelay));
          if (alphaLevel <= 0) {
            alphaLevel = 0;
            break;
          }
        }
        else {
          break;
        }

        try {
          Thread.sleep(inRamp ? 10 : (int) (1000 / fps));
        }
        catch (InterruptedException e) {
          Thread.currentThread().interrupt(); // Très important de réinterrompre (ajouté après coup)
          break;
        }
        Thread.yield();
      }

      if (!rampUp) {
        started = false;
        repaint();
        setVisible(false);
        removeMouseListener(GlassPaneSoleilAttente.this);
      }
    }
  }

  // @Override
  @Override
  public void mouseClicked(MouseEvent e) {
  }

  // @Override
  @Override
  public void mousePressed(MouseEvent e) {
  }

  // @Override
  @Override
  public void mouseReleased(MouseEvent e) {
  }

  // @Override
  @Override
  public void mouseEntered(MouseEvent e) {
  }

  // @Override
  @Override
  public void mouseExited(MouseEvent e) {
  }
}
