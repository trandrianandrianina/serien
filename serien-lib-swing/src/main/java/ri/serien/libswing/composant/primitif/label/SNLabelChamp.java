/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.label;

import javax.swing.SwingConstants;

/**
 * Libellé de champ avec la présentation standard.
 *
 * Ce composant remplace JLabel et dérive de SNLabel.
 * Il est à utiliser pour tous les libellés de champs. Il utilise la police standard alignée vers la droite.
 * Si le message est important il sera affiché en rouge sinon en noir.
 *
 * D'autres composants libellés sont disponibles :
 * - SNLabelTitre : pour les titres de sections ou de tableaux.
 * - SNLabelUnite : pour les unités associées à un champ.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 * - Aligné à droite.
 */
public class SNLabelChamp extends SNLabel {
  /**
   * Constructeur.
   */
  public SNLabelChamp() {
    super();

    // Rendre le composant transparent
    setOpaque(false);
    
    // Justifier à droite
    setHorizontalAlignment(SwingConstants.TRAILING);
  }
  
  /**
   * Modifier le texte du libellé à afficher.
   *
   * Le texte des libellés est également affiché dans l'infobulle. C'est utile si la fenêtre n'est pas assez large et que le libellé
   * est tronqué. L'utilisateur peut positionner la souris dessus pour voir l'intégralité du libellé dans l'infobulle.
   *
   * @param pTexte Texte du libellé.
   */
  @Override
  public void setText(String pTexte) {
    super.setText(pTexte);
    setToolTipText(getText());
  }
}
