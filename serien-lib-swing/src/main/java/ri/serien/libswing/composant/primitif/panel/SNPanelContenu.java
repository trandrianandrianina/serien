/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.panel;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Panneau de contenu d'un écran ou d'une boîte de dialogue.
 *
 * Le panneau contenu est utilisé comme second panneau des écrans et boîtes de dialogue.(après SNPanelFond). Il contient l'ensemble des
 * composants de l'écran à l'exception du bandeau supérieur et de la barre de boutons (SNBarreBouton). Il comporte une marge pour séparer
 * les composants des bords de l'écran.
 *
 * Caractéristiques graphiques :
 * - Transparent.
 * - Pas de cadre et pas de titre.
 * - Marge externe sur le pourtour.
 */
public class SNPanelContenu extends JPanel {

  /**
   * Constructeur standard.
   */
  public SNPanelContenu() {
    super();
    setOpaque(false);
    setBorder(new EmptyBorder(SNCharteGraphique.MARGE_EXTERNE, SNCharteGraphique.MARGE_EXTERNE, SNCharteGraphique.MARGE_EXTERNE,
        SNCharteGraphique.MARGE_EXTERNE));

  }

}
