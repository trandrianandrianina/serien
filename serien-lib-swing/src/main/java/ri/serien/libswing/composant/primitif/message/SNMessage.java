/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.message;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Affichage d'un message multi-lignes.
 *
 * Ce composant affiche un message pouvant éventuellement être présenté sur plusieurs lignes. Des retours à la ligne sont automatiquement
 * insérés entre les mots si le texte dépasse la largeur du composant.
 *
 * De plus, si le nombre de lignes du message dépasse la hauteur du composant, un ascenseur vertical est affiché permettant de consulter
 * la suite du message. L'ascenseur est une sécurité. Les écrans biens conçus doivent prévoir la place suffisante afin que ce composant
 * affiche les messages complets sans ascenseur.
 *
 * Le message est aligné vers la gauche. Par défaut, il utilise la police standard. Si le message est d'importance moyenne, il sera
 * affiché en gras et noir. Si le message est d'importance haute, il sera affiché en gras et rouge.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 * - Aligné à gauche.
 * - La couleur dépend de l'importance du message.
 * - Affichage d'un ascenseur vertical si nécessaire.
 */
public class SNMessage extends JPanel {
  private JTextArea jTextArea = null;
  private JScrollPane jScrollPane = null;
  private String texte = null;
  private EnumImportanceMessage importanceMessage = EnumImportanceMessage.NORMAL;
  
  /**
   * Constructeur.
   */
  public SNMessage() {
    super();

    // Créer le composant de texte multi-lignes
    jTextArea = new JTextArea();
    jTextArea.setLineWrap(true);
    jTextArea.setWrapStyleWord(true);
    jTextArea.setFocusable(false);
    jTextArea.setEditable(false);
    
    // Créer le composant de scrolling
    jScrollPane = new JScrollPane(jTextArea);
    jScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    jScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

    // Ajouter les composants au panel
    setLayout(new BorderLayout());
    add(jScrollPane, BorderLayout.CENTER);
    
    // Rendre le tout transparent
    setOpaque(false);
    jScrollPane.setOpaque(false);
    jScrollPane.getViewport().setOpaque(false);
    jTextArea.setBackground(new Color(0, 0, 0, 0));
    jTextArea.setOpaque(false);
    
    // Enlever les bords
    jScrollPane.setBorder(BorderFactory.createEmptyBorder());
    jScrollPane.setViewportBorder(BorderFactory.createEmptyBorder());
    jTextArea.setBorder(BorderFactory.createEmptyBorder());
    
    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
  }
  
  /**
   * Rafraichir l'affichage.
   */
  private void rafraichir() {
    // Rafraichir le texte
    if (texte != null) {
      jTextArea.setText(texte);
    }
    else {
      jTextArea.setText("");
    }
    
    // Rafraichir la policie en fonction de l'importance du message
    if (importanceMessage != null) {
      switch (importanceMessage) {
        case HAUT:
          jTextArea.setFont(SNCharteGraphique.POLICE_TITRE);
          jTextArea.setForeground(Color.RED);
          break;
        
        case MOYEN:
          jTextArea.setFont(SNCharteGraphique.POLICE_TITRE);
          jTextArea.setForeground(Color.BLACK);
          break;
        
        default:
          jTextArea.setFont(SNCharteGraphique.POLICE_STANDARD);
          jTextArea.setForeground(Color.BLACK);
          break;
      }
    }
    else {
      jTextArea.setFont(SNCharteGraphique.POLICE_STANDARD);
      jTextArea.setForeground(Color.BLACK);
    }
  }
  
  /**
   * Texte du message.
   * @return Texte du message.
   */
  public String getText() {
    return texte;
  }
  
  /**
   * Modifier le texte du message. L'importance du message reste inchangée.
   * @param Texte du message.
   */
  public void setText(String pTexte) {
    texte = pTexte;
    rafraichir();
  }
  
  /**
   * Importance du message.
   * @return Importance du message.
   */
  public EnumImportanceMessage getImportanceMessage() {
    return importanceMessage;
  }
  
  /**
   * Modifier l'importance du message. Le texte du message reste inchangé.
   * @param Importance du message.
   */
  public void setImportanceMessage(EnumImportanceMessage pImportanceMessage) {
    importanceMessage = pImportanceMessage;
    rafraichir();
  }
  
  /**
   * Texte et l'importance du message.
   * @return Message.
   */
  public Message getMessage() {
    return Message.getMessage(texte, importanceMessage);
  }
  
  /**
   * Modifier le texte et l'importance du message.
   * @param pMessage Message.
   */
  public void setMessage(Message pMessage) {
    if (pMessage != null) {
      texte = pMessage.getTexte();
      importanceMessage = pMessage.getImportanceMessage();
    }
    else {
      texte = null;
      importanceMessage = EnumImportanceMessage.NORMAL;
    }
    rafraichir();
  }
}
