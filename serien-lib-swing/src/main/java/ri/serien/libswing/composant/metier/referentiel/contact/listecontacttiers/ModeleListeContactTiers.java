/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.contact.listecontacttiers;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import ri.serien.libcommun.commun.EnumModeUtilisation;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.menu.EnumPointMenu;
import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.CategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.IdCategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.ListeCategorieContact;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.CritereContact;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.gescom.commun.contact.ParametreMenuContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.IdLienContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.LienContact;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceCommun;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.confirmation.ModeleDialogueConfirmation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue permettant de lister les contacts d'un tiers (client ou fournisseur).
 */
public class ModeleListeContactTiers extends AbstractModeleDialogue {
  private CritereContact critereContact = new CritereContact();
  private ListeCategorieContact listeCategorieContact = null;
  private ListeContact listeContact = null;
  
  /**
   * Constructeur pour les contacts clients.
   */
  public ModeleListeContactTiers(SessionBase pSession, IdClient pIdClient) {
    super(pSession);
    
    // Initialiser les critères de recherche avec ceux pour les clients
    critereContact.initialiserContactClient(EnumTypeContact.CLIENT, pIdClient);
    
    // Préciser le tiers dont on affiche les contacts dans la titre de la fenêtre
    setTitreEcran("Liste des contacts du client " + getIdClient());
  }
  
  /**
   * Constructeur pour les contacts fournisseurs.
   */
  public ModeleListeContactTiers(SessionBase pSession, IdFournisseur pIdFournisseur) {
    super(pSession);
    
    // Initialiser les critères de recherche avec ceux pour les fournisseurs
    critereContact.initialiserContactFournisseur(EnumTypeContact.FOURNISSEUR, pIdFournisseur);
    
    // Préciser le tiers dont on affiche les contacts dans la titre de la fenêtre
    setTitreEcran("Liste des contacts du fournisseur " + getIdFournisseur());
  }
  
  @Override
  public void initialiserDonnees() {
    IdSession.controlerId(getIdSession(), true);
  }
  
  @Override
  public void chargerDonnees() {
    // charger la liste des fonctions
    listeCategorieContact = ManagerServiceParametre.chargerListeCategorieContact(getIdSession());
    
    // Charger la liste des contacts
    chargerListeContact();
  }
  
  @Override
  public void quitterAvecValidation() {
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    super.quitterAvecAnnulation();
  }
  
  /**
   * Charger la liste des contacts correspondant aux critères de recherche.
   */
  private void chargerListeContact() {
    // Tout déselectionner
    listeContact = null;
    
    // Initier la liste des contacts
    listeContact = ManagerServiceCommun.chargerListeContact(getIdSession(), critereContact);
  }
  
  /**
   * Affiche la liste des contacts possibles à partir des critères de recherche.
   */
  public void modifierRechercheGenerique(String pRechercheGenerique) {
    // Tester si la valeur a changé
    if (Constantes.equals(pRechercheGenerique, critereContact.getTexteRechercheGenerique())) {
      return;
    }
    
    // Mettre à jour la valeur
    critereContact.setTexteRechercheGenerique(pRechercheGenerique);
    
    // Rafraichir la liste des contacts
    chargerListeContact();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le filtre sur la fonction du contact.
   */
  public void modifierCategorieContact(CategorieContact pCategorieContact) {
    // Récupérer l'identifiant de la catégorie sélectionnée
    IdCategorieContact idCategorieContact = null;
    if (pCategorieContact != null) {
      idCategorieContact = pCategorieContact.getId();
    }
    
    // Tester si la valeur a changé
    if (Constantes.equals(critereContact.getIdCategorieContact(), idCategorieContact)) {
      return;
    }
    
    // Modifier la valeur
    critereContact.setIdCategorieContact(idCategorieContact);
    
    // Rafraichir la liste des contacts
    chargerListeContact();
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Modifie l'indice de la ligne séléctionnée dans le tableau.
   */
  public void modifierLigneSelectionnee(int pLigneSelectionnee) {
    // Vérifier si la sélection a changé
    if (listeContact != null && listeContact.isSelectionne(pLigneSelectionnee)) {
      return;
    }
    
    // Modifier le contact sélectionné
    if (listeContact != null) {
      listeContact.selectionner(pLigneSelectionnee);
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier un contact
   */
  public void afficherContact() {
    // Vérifier que la liste des contacts n'est pas null
    if (listeContact == null) {
      throw new MessageErreurException("La liste des contacts est invalide.");
    }
    
    // Vérifier qu'un contact est sélectionné
    IdContact idContact = listeContact.getIdSelection();
    if (idContact == null) {
      throw new MessageErreurException("Aucun contact de sélectionné.");
    }
    
    // Préparer les paramètres de lancement du point de menu
    ParametreMenuContact parametre = new ParametreMenuContact();
    parametre.setModeUtilisation(EnumModeUtilisation.CONSULTATION);
    if (getIdClient() != null) {
      parametre.setIdClient(getIdClient());
      parametre.setIdLienContact(IdLienContact.getInstancePourClient(idContact.getCode(), getIdClient()));
    }
    else if (getIdFournisseur() != null) {
      parametre.setIdFournisseur(getIdFournisseur());
      parametre.setIdLienContact(IdLienContact.getInstancePourFournisseur(idContact.getCode(), getIdFournisseur()));
    }
    
    // Lancer le point de menu
    ManagerSessionClient.getInstance().lancerPointMenu(getSession(), IdEnregistrementMneMnp.getInstance(EnumPointMenu.CONTACTS),
        parametre);
    
    // Fermer la boîte de dialogue
    quitterAvecValidation();
  }
  
  /**
   * Rédiger un mail pour le contact sélectionné
   */
  public void redigerMail() {
    // Vérifier que la liste des contacts n'est pas null
    if (listeContact == null) {
      throw new MessageErreurException("La liste des contacts est invalide.");
    }
    
    // Vérifier qu'un contact est sélectionné
    Contact contact = listeContact.getSelection();
    if (contact == null) {
      throw new MessageErreurException("Aucun contact de sélectionné.");
    }
    
    // Vérifier que le contact a une adresse mail
    if (contact.getEmail() == null || contact.getEmail().isEmpty()) {
      throw new MessageErreurException(
          "Le contact sélectionné ne possède pas d'adresse mail valide. Modifiez ce contact pour ajouter son adresse mail.");
    }
    
    // Lancer l'outil de messagerie du poste de travail
    try {
      URI uriMailTo = new URI("mailto", contact.getEmail(), null);
      Desktop.getDesktop().mail(uriMailTo);
    }
    catch (IOException ex) {
    }
    catch (URISyntaxException ex) {
      throw new MessageErreurException(
          "Le contact sélectionné ne possède pas d'adresse mail valide. Modifiez ce contact pour ajouter son adresse mail.");
    }
  }
  
  /**
   * Créer un nouveau contact.
   */
  public void creerContact() {
    // Préparer les paramètres de lancement du point de menu
    ParametreMenuContact parametre = new ParametreMenuContact();
    parametre.setModeUtilisation(EnumModeUtilisation.CREATION);
    parametre.setIdClient(getIdClient());
    parametre.setIdFournisseur(getIdFournisseur());
    
    // Lancer le point de menu
    ManagerSessionClient.getInstance().lancerPointMenu(getSession(), IdEnregistrementMneMnp.getInstance(EnumPointMenu.CONTACTS),
        parametre);
    
    // Fermer la boîte de dialogue
    quitterAvecValidation();
  }
  
  /**
   * Supprimer le lien entre le tiers en cours et le contact sélectionné.
   */
  public void supprimerLien() {
    // Vérifier que la liste des contacts n'est pas null
    if (listeContact == null) {
      throw new MessageErreurException("La liste des contacts est invalide.");
    }
    
    // Vérifier qu'un contact est sélectionné
    Contact contact = listeContact.getSelection();
    if (contact == null) {
      throw new MessageErreurException("Aucun contact de sélectionné.");
    }
    
    // Vérifier que le tiers a plus d'un contact dans sa liste
    if (listeContact.size() <= 1) {
      throw new MessageErreurException(
          "Ce tiers n'est lié qu'à un seul contact. Vous ne pouvez pas supprimer le dernier contact d'un tiers. "
              + "Ajoutez un nouveau contact à ce tiers avant de supprimer le contact sélectionné.");
    }
    
    // Vérifier que le contact est lié à plusieurs tiers
    if (contact.getListeLienContact() == null || contact.getListeLienContact().size() <= 1) {
      throw new MessageErreurException("Ce contact a pour seul lien le tiers en cours. Vous ne pouvez pas supprimer leur lien. "
          + "Rendez-vous dans la gestion des contacts et supprimez ce contact pour qu'il n'apparaisse "
          + "plus dans la liste des contacts liés à ce tiers.");
    }
    
    // Construire le message de confirmation
    String messageConfirmation = null;
    if (getIdClient() != null) {
      messageConfirmation = "Vous avez demandé la suppression du lien entre le contact " + contact.getTexte() + " et le client en cours ("
          + getIdClient().getTexte() + "). Confirmez-vous cette demande ?";
    }
    else if (getIdFournisseur() != null) {
      messageConfirmation = "Vous avez demandé la suppression du lien entre le contact " + contact.getTexte()
          + " et le fournisseur en cours (" + getIdFournisseur().getTexte() + "). Confirmez-vous cette demande ?";
    }
    else {
      return;
    }
    
    // Demander confirmation
    ModeleDialogueConfirmation modeleConfirmation =
        new ModeleDialogueConfirmation(getSession(), "Supprimer le lien avec le contact", messageConfirmation);
    DialogueConfirmation vue = new DialogueConfirmation(modeleConfirmation);
    vue.afficher();
    if (!modeleConfirmation.isSortieAvecValidation()) {
      return;
    }
    
    // Rechercher l'identifiant du lien contact/tiers
    IdLienContact idLienSupprime = null;
    if (getIdClient() != null) {
      idLienSupprime = IdLienContact.getInstancePourClient(contact.getId().getCode(), getIdClient());
    }
    else if (getIdFournisseur() != null) {
      idLienSupprime = IdLienContact.getInstancePourFournisseur(contact.getId().getCode(), getIdFournisseur());
    }
    else {
      return;
    }
    
    // Récupérer le lien à supprimer
    LienContact lienSupprime = contact.getListeLienContact().get(idLienSupprime);
    if (lienSupprime == null) {
      return;
    }
    
    // Supprimer le lien de la liste du contact sélectionné
    contact.getListeLienContact().remove(lienSupprime);
    
    // Sauvegarder la suppression en base
    ManagerServiceCommun.sauverContact(getIdSession(), contact);
    
    // Recharger la liste des contacts
    chargerListeContact();
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Définir comme contact principal le contact sélectionné.
   */
  public void modifierContactPrincipal() {
    // Vérifier que la liste des contacts n'est pas null
    if (listeContact == null) {
      throw new MessageErreurException("La liste des contacts est invalide.");
    }
    
    // Vérifier qu'un contact est sélectionné
    Contact contact = listeContact.getSelection();
    if (contact == null) {
      throw new MessageErreurException("Aucun contact de sélectionné.");
    }
    
    // Vérifier que le contact sélectionné n'est pas déjà le contact principal
    Contact contactPrincipalActuel = null;
    if (getIdClient() != null) {
      contactPrincipalActuel = listeContact.getContactPrincipal(getIdClient());
    }
    else {
      contactPrincipalActuel = listeContact.getContactPrincipal(getIdFournisseur());
    }
    if (contactPrincipalActuel != null && contact.equals(contactPrincipalActuel)) {
      throw new MessageErreurException("Le contact sélectionné est déjà le contact principal.");
    }
    
    // Modifier le contact principal
    if (getIdClient() != null) {
      contact.getListeLienContact().getLien(getIdClient()).setContactPrincipal(true);
    }
    else if (getIdFournisseur() != null) {
      contact.getListeLienContact().getLien(getIdFournisseur()).setContactPrincipal(true);
    }
    
    // Sauvegarder en base de données
    ManagerServiceCommun.sauverContact(getIdSession(), contact);

    // Recharger la liste des contacts pour tenir compte des modifications à l'affichage
    chargerListeContact();
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Type de tiers dont on affiche les contacts (client, prospect ou fournisseur).
   */
  public EnumTypeContact getTypeContact() {
    return critereContact.getTypeContact();
  }
  
  /**
   * Retourner l'identifiant du client dont on affiche les contacts.
   * L'identifiant est null si on affiche ceux d'un fournisseur.
   */
  public IdClient getIdClient() {
    return critereContact.getIdClient();
  }
  
  /**
   * Retourner l'identifiant du fournisseur dont on affiche les contacts.
   * L'identifiant est null si on affiche ceux d'un client.
   */
  public IdFournisseur getIdFournisseur() {
    return critereContact.getIdFournisseur();
  }
  
  /**
   * Liste des catégories de contacts.
   * La catégorie de contacts sert à définir les fonctions.
   */
  public ListeCategorieContact getListeCatégorieContact() {
    return listeCategorieContact;
  }
  
  /**
   * Catégorie de contact renseignée dans les critères de recherche.
   * La catégorie de contacts sert à définir les fonctions.
   */
  public CategorieContact getCategorieContact() {
    if (listeCategorieContact == null) {
      return null;
    }
    return listeCategorieContact.get(critereContact.getIdCategorieContact());
  }
  
  /**
   * Retourner le texte de la recherche générique.
   */
  public String getRechercheGenerique() {
    return critereContact.getTexteRechercheGenerique();
  }
  
  /**
   * Titre de la liste des contacts.
   */
  public Message getTitreListe() {
    if (listeContact != null && listeContact.size() >= 1) {
      return Message.getMessageNormal("Liste des contacts (" + listeContact.size() + ")");
    }
    else {
      return Message.getMessageNormal("Aucun contact");
    }
  }
  
  /**
   * Liste des contacts.
   */
  public List<Contact> getListeContact() {
    return listeContact;
  }
  
  /**
   * Numéro de la ligne sélectionnée.
   */
  public int getLigneSelectionnee() {
    if (listeContact == null) {
      return -1;
    }
    return listeContact.getIndexSelection();
  }
  
  /**
   * Identifiant du contact sélectionné (null si aucune sélection).
   */
  public Contact getContactSelectionne() {
    if (listeContact == null) {
      return null;
    }
    return listeContact.getSelection();
  }
  
  /**
   * Identifiant du contact sélectionné (null si aucune sélection).
   */
  public Contact getContactPrincipal() {
    if (listeContact == null) {
      return null;
    }
    if (getIdClient() != null) {
      return listeContact.getContactPrincipal(getIdClient());
    }
    else if (getIdFournisseur() != null) {
      return listeContact.getContactPrincipal(getIdFournisseur());
    }
    return null;
  }
}
