/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.lot;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.vente.lot.EnumModeSaisieLot;
import ri.serien.libcommun.gescom.vente.lot.ListeLot;
import ri.serien.libcommun.gescom.vente.lot.Lot;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.ModeleTableEditable;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueLot extends AbstractVueDialogue<ModeleLot> {
  private static final String BOUTON_INITIALISER_SAISIE = "Initialiser la saisie";
  private static final String BOUTON_CORRIGER_QUANTITE = "Corriger quantité vendue";
  private static final String BOUTON_CREER_LOT = "Créer lot";
  public final static String[] TITRE_LISTE_LOT_SAISIE = new String[] { "Quantité", "Qte saisie", "UCV", "Disponible", "Numéro de lot" };
  public final static String[] TITRE_LISTE_LOT_RETOUR = new String[] { "Quantité", "Qte saisie", "UCV", "Numéro de lot" };
  public final static String[] TITRE_LISTE_CREATION_LOT = new String[] { "Quantité", "Numéro de lot" };
  final boolean[] colonnesEditables = new boolean[] { true, false, false, false, false };
  final boolean[] colonnesEditablesCreation = new boolean[] { true, true };
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Constructeur
   * @param ModeleLot pModele le modèle
   */
  public VueLot(ModeleLot pModele) {
    super(pModele);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialise les composants graphiques.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(1000, 550);
    setResizable(false);

    // La table contenant la liste des commandes d'achat
    scpListeLot.getViewport().setBackground(Color.WHITE);

    if (Constantes.equals(getModele().getModeSaisie(), EnumModeSaisieLot.MODE_CREATION_LOT)) {
      final int[] dimension = new int[] { 100, 1000 };
      final int[] justification = new int[] { NRiTable.DROITE, NRiTable.GAUCHE };
      ModeleTableEditable modeleTable = new ModeleTableEditable(TITRE_LISTE_CREATION_LOT, colonnesEditablesCreation);
      tblListeLot.personnaliserAspect(modeleTable, dimension, justification, 14);
    }
    else if (Constantes.equals(getModele().getModeSaisie(), EnumModeSaisieLot.MODE_RETOUR_LOT)) {
      final int[] dimension = new int[] { 100, 100, 50, 1000 };
      final int[] justification = new int[] { NRiTable.DROITE, NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.GAUCHE };
      ModeleTableEditable modeleTable = new ModeleTableEditable(TITRE_LISTE_LOT_RETOUR, colonnesEditables);
      tblListeLot.personnaliserAspect(modeleTable, dimension, justification, 14);
    }
    else {
      final int[] dimension = new int[] { 100, 100, 50, 150, 1000 };
      final int[] justification = new int[] { NRiTable.DROITE, NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.GAUCHE };
      ModeleTableEditable modeleTable = new ModeleTableEditable(TITRE_LISTE_LOT_SAISIE, colonnesEditables);
      tblListeLot.personnaliserAspect(modeleTable, dimension, justification, 14);
    }

    if (!Constantes.equals(getModele().getModeSaisie(), EnumModeSaisieLot.MODE_LECTURE_SEULE)) {
      tblListeLot.setSelectAllForEdit(true);
      // Permet de forcer la saisie d'une cellule si l'on clique sur un autre composant (le bouton valider par exemple)
      tblListeLot.forcerSaisieCellule(true);
    }
    else {
      tblListeLot.setEnabled(false);
    }

    // Listener sur modification des valeurs saisies dans la table
    tblListeLot.getModel().addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE) {
          modifierQuantiteLot();
        }
      }
    });
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeLot.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        getModele().selectionnerLot(tblListeLot.getListeIndiceSelection());
      }
    });

    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_INITIALISER_SAISIE, 'i', false);
    snBarreBouton.ajouterBouton(BOUTON_CORRIGER_QUANTITE, 'q', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_LOT, 'l', false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });

    // Configurer la barre de boutons de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirTitre();
    rafraichirArticle();
    rafraichirTexteRecherche();
    rafraichirAvecQuantiteSaisie();
    rafraichirTitreListe();
    if (Constantes.equals(getModele().getModeSaisie(), EnumModeSaisieLot.MODE_RETOUR_LOT)) {
      rafraichirListeRetourLot();
    }
    else {
      rafraichirListeSaisieLot();
    }
    
    rafraichirCreationLot();
    rafraichirBoutonInitialiserSaisie();
    rafraichirBoutonCorrigerQuantite();
    rafraichirBoutonValider();
    rafraichirBoutonCreerLot();
    rafraichirMessage();
    
    // Positionnement du focus
    tblListeLot.requestFocusInWindow();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraichir le titre de la boite de dialogue.
   */
  private void rafraichirTitre() {
    setTitle(getModele().getTitreEcran());
  }
  
  /**
   * Rafraichir l'article concerné par la saisie de lots.
   */
  private void rafraichirArticle() {
    Article article = getModele().getArticle();
    if (article == null) {
      return;
    }
    snArticle.setSession(getModele().getSession());
    snArticle.setIdEtablissement(article.getId().getIdEtablissement());
    snArticle.charger(false);
    snArticle.setSelection(article);
    snArticle.setEnabled(false);
  }
  
  /**
   * Rafraichir le texte de recherche.
   */
  private void rafraichirTexteRecherche() {
    tfRecherche.setText(getModele().getTexteRecherche());
  }
  
  /**
   *
   * Coche la case à cocher qui défini si on affiche seulement les lots pour lesquels une valeur est saisie.
   *
   */
  private void rafraichirAvecQuantiteSaisie() {
    boolean avecSaisis = getModele().isAffichageAvecSaisieSeulement();
    cbQuantiteSaisie.setSelected(avecSaisis);
  }
  
  /**
   * Rafraichir le titre de la liste.
   */
  private void rafraichirTitreListe() {
    Message titre = getModele().getTitreListe();
    if (titre == null) {
      return;
    }
    lbTitreListe.setMessage(titre);
  }
  
  /**
   * Rafraichir les données dans la liste en mode vente d'articles.
   */
  private void rafraichirListeSaisieLot() {
    ListeLot listeLot = getModele().getListeLot();
    String[][] donnees = null;
    if (listeLot != null) {
      donnees = new String[listeLot.size()][TITRE_LISTE_LOT_SAISIE.length];
      for (int ligne = 0; ligne < listeLot.size(); ligne++) {
        Lot lot = listeLot.get(ligne);
        if (lot != null) {
          // Quantité saisie
          if (lot.getQuantiteSaisi() != null) {
            donnees[ligne][0] = Constantes.formater(lot.getQuantiteSaisi(), false);
          }
          // Quantité précédemment saisie
          donnees[ligne][1] = Constantes.formater(lot.getQuantitePrecedentSaisi(), false);
          // Unité de conditionnement de vente
          if (getModele().getArticle() != null && getModele().getArticle().getIdUCV() != null) {
            donnees[ligne][2] = getModele().getArticle().getIdUCV().getCode();
          }
          // Quantité disponible
          donnees[ligne][3] = Constantes.formater(lot.getStockDisponible(), false);
          // code lot
          donnees[ligne][4] = lot.getTexte();
        }
      }
    }
    tblListeLot.mettreAJourDonnees(donnees);
    
    // Réactualiser les lignes sélectionnées
    if (getModele().getListeIndiceLotSelectionne() != null && !getModele().getListeIndiceLotSelectionne().isEmpty()) {
      for (int indexSelectionne : getModele().getListeIndiceLotSelectionne()) {
        tblListeLot.getSelectionModel().addSelectionInterval(indexSelectionne, indexSelectionne);
      }
    }
  }

  /**
   * Rafraichir les données dans la liste en mode retours d'articles.
   */
  private void rafraichirListeRetourLot() {
    ListeLot listeLot = getModele().getListeLot();
    String[][] donnees = null;
    if (listeLot != null) {
      donnees = new String[listeLot.size()][TITRE_LISTE_LOT_SAISIE.length];
      for (int ligne = 0; ligne < listeLot.size(); ligne++) {
        Lot lot = listeLot.get(ligne);
        if (lot != null) {
          // Quantité saisie
          if (lot.getQuantiteSaisi() != null) {
            donnees[ligne][0] = Constantes.formater(lot.getQuantiteSaisi(), false);
          }
          // Quantité précédemment saisie
          donnees[ligne][1] = Constantes.formater(lot.getQuantitePrecedentSaisi(), false);
          // Unité de conditionnement de vente
          if (getModele().getArticle() != null && getModele().getArticle().getIdUCV() != null) {
            donnees[ligne][2] = getModele().getArticle().getIdUCV().getCode();
          }
          // code lot
          donnees[ligne][3] = lot.getTexte();
        }
      }
    }
    tblListeLot.mettreAJourDonnees(donnees);
    
    // Réactualiser les lignes sélectionnées
    if (getModele().getListeIndiceLotSelectionne() != null && !getModele().getListeIndiceLotSelectionne().isEmpty()) {
      for (int indexSelectionne : getModele().getListeIndiceLotSelectionne()) {
        tblListeLot.getSelectionModel().addSelectionInterval(indexSelectionne, indexSelectionne);
      }
    }
  }
  
  /**
   * Rafraichir les champs de création de lot.
   */
  private void rafraichirCreationLot() {
    pnlCreationLot.setVisible(Constantes.equals(getModele().getModeSaisie(), EnumModeSaisieLot.MODE_CREATION_LOT));
  }
  
  /**
   * Rafraichir le bouton valider
   */
  private void rafraichirBoutonValider() {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, getModele().isValidationPossible());
  }
  
  /**
   * Rafraichir le bouton initialiser saisie
   */
  private void rafraichirBoutonInitialiserSaisie() {
    snBarreBouton.activerBouton(BOUTON_INITIALISER_SAISIE, getModele().isQuantiteSaisie());
  }

  /**
   * Rafraichir le bouton corriger quantité vendue
   */
  private void rafraichirBoutonCorrigerQuantite() {
    snBarreBouton.activerBouton(BOUTON_CORRIGER_QUANTITE, getModele().isQuantiteVendueModifiable());
  }
  
  /**
   * Rafraichir le bouton créer lot
   */
  private void rafraichirBoutonCreerLot() {
    snBarreBouton.activerBouton(BOUTON_CREER_LOT, getModele().isCreationLotPossible());
  }
  
  /**
   * Rafraichir le message qui donne les instructions de saisie.
   */
  private void rafraichirMessage() {
    if (getModele().getMessageResteASaisir() != null) {
      snMessageReste.setMessage(getModele().getMessageResteASaisir());
    }
  }

  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Barre des boutons
   * @param pSNBouton le bouton activé
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().rechercher();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
      else if (pSNBouton.isBouton(BOUTON_INITIALISER_SAISIE)) {
        getModele().initialiserSaisie();
      }
      else if (pSNBouton.isBouton(BOUTON_CORRIGER_QUANTITE)) {
        getModele().modifierQuantiteVendue();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_LOT)) {
        getModele().creerLot();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Informer le modèle que tfRecherche a perdu le focust.
   * @param e
   */
  private void tfRechercheFocusLost(FocusEvent e) {
    try {
      getModele().modifierRecherche(tfRecherche.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Informer le modèle qu'une valeur a été saisie pour un lot.
   */
  private void modifierQuantiteLot() {
    int indexLigneSeletionnee = tblListeLot.getIndiceSelection();
    if (indexLigneSeletionnee == -1) {
      return;
    }
    
    String valeur = (String) tblListeLot.getValueAt(indexLigneSeletionnee, 0);
    getModele().modifierQuantiteLot(indexLigneSeletionnee, valeur);
  }
  
  /**
   * Informer le modèle qu'une quantité a été saisie pour un lot.
   * @param e
   */
  private void cbQuantiteSaisieItemStateChanged(ItemEvent e) {
    try {
      getModele().modifierQuantiteSaisieSeule(cbQuantiteSaisie.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Informer le modèle que le focus est perdu sur le tfLibelleLot.
   * @param e
   */
  private void tfLibelleLotFocusLost(FocusEvent e) {
    try {
      getModele().modifierNumeroLot(tfLibelleLot.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes auto-générées (JFormDesigner)
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlCouleurFond = new JPanel();
    pnlPrincipal = new SNPanelContenu();
    sNPanel1 = new SNPanel();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbRecherche = new SNLabelChamp();
    tfRecherche = new SNTexte();
    cbQuantiteSaisie = new JCheckBox();
    snBarreRecherche = new SNBarreRecherche();
    lbTitreListe = new SNLabelTitre();
    scpListeLot = new JScrollPane();
    tblListeLot = new NRiTable();
    pnlCreationLot = new SNPanelTitre();
    lbLibelleLot = new SNLabelChamp();
    tfLibelleLot = new SNTexte();
    snMessageReste = new SNMessage();
    snBarreBouton = new SNBarreBouton();

    // ======== this ========
    setMinimumSize(new Dimension(1000, 650));
    setForeground(Color.black);
    setTitle("Saisie des quantit\u00e9s sur lots");
    setBackground(new Color(238, 238, 210));
    setResizable(false);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== pnlCouleurFond ========
    {
      pnlCouleurFond.setBackground(new Color(238, 238, 210));
      pnlCouleurFond.setMinimumSize(new Dimension(45, 250));
      pnlCouleurFond.setName("pnlCouleurFond");
      pnlCouleurFond.setLayout(new BorderLayout());

      // ======== pnlPrincipal ========
      {
        pnlPrincipal.setBackground(new Color(238, 238, 210));
        pnlPrincipal.setPreferredSize(new Dimension(20, 220));
        pnlPrincipal.setName("pnlPrincipal");
        pnlPrincipal.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowHeights = new int[] { 0, 0, 205, 0, 0, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0, 1.0E-4 };

        // ======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };

          // ---- lbArticle ----
          lbArticle.setText("Article");
          lbArticle.setName("lbArticle");
          sNPanel1.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

          // ---- snArticle ----
          snArticle.setName("snArticle");
          sNPanel1.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

          // ---- lbRecherche ----
          lbRecherche.setText("Recherche");
          lbRecherche.setName("lbRecherche");
          sNPanel1.add(lbRecherche, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

          // ---- tfRecherche ----
          tfRecherche.setMaximumSize(new Dimension(300, 30));
          tfRecherche.setMinimumSize(new Dimension(300, 30));
          tfRecherche.setPreferredSize(new Dimension(300, 30));
          tfRecherche.setName("tfRecherche");
          tfRecherche.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfRechercheFocusLost(e);
            }
          });
          sNPanel1.add(tfRecherche, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

          // ---- cbQuantiteSaisie ----
          cbQuantiteSaisie.setText("Lots avec quantit\u00e9s saisies");
          cbQuantiteSaisie.setFont(new Font("sansserif", Font.PLAIN, 14));
          cbQuantiteSaisie.setPreferredSize(new Dimension(192, 30));
          cbQuantiteSaisie.setMinimumSize(new Dimension(192, 30));
          cbQuantiteSaisie.setMaximumSize(new Dimension(192, 30));
          cbQuantiteSaisie.setName("cbQuantiteSaisie");
          cbQuantiteSaisie.addItemListener(e -> cbQuantiteSaisieItemStateChanged(e));
          sNPanel1.add(cbQuantiteSaisie, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

          // ---- snBarreRecherche ----
          snBarreRecherche.setName("snBarreRecherche");
          sNPanel1.add(snBarreRecherche, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPrincipal.add(sNPanel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ---- lbTitreListe ----
        lbTitreListe.setText("Liste des lots");
        lbTitreListe.setName("lbTitreListe");
        pnlPrincipal.add(lbTitreListe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ======== scpListeLot ========
        {
          scpListeLot.setPreferredSize(new Dimension(1050, 200));
          scpListeLot.setMaximumSize(new Dimension(32767, 200));
          scpListeLot.setName("scpListeLot");

          // ---- tblListeLot ----
          tblListeLot.setName("tblListeLot");
          scpListeLot.setViewportView(tblListeLot);
        }
        pnlPrincipal.add(scpListeLot, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ======== pnlCreationLot ========
        {
          pnlCreationLot.setTitre("Cr\u00e9ation de lot");
          pnlCreationLot.setName("pnlCreationLot");
          pnlCreationLot.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCreationLot.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCreationLot.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlCreationLot.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCreationLot.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };

          // ---- lbLibelleLot ----
          lbLibelleLot.setText("Num\u00e9ro de lot");
          lbLibelleLot.setName("lbLibelleLot");
          pnlCreationLot.add(lbLibelleLot, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

          // ---- tfLibelleLot ----
          tfLibelleLot.setPreferredSize(new Dimension(200, 30));
          tfLibelleLot.setMinimumSize(new Dimension(200, 30));
          tfLibelleLot.setMaximumSize(new Dimension(500, 30));
          tfLibelleLot.setName("tfLibelleLot");
          tfLibelleLot.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfLibelleLotFocusLost(e);
            }
          });
          pnlCreationLot.add(tfLibelleLot, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPrincipal.add(pnlCreationLot, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ---- snMessageReste ----
        snMessageReste.setText("Message reste a saisir");
        snMessageReste.setName("snMessageReste");
        pnlPrincipal.add(snMessageReste, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlCouleurFond.add(pnlPrincipal, BorderLayout.CENTER);

      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlCouleurFond.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlCouleurFond, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlCouleurFond;
  private SNPanelContenu pnlPrincipal;
  private SNPanel sNPanel1;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbRecherche;
  private SNTexte tfRecherche;
  private JCheckBox cbQuantiteSaisie;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lbTitreListe;
  private JScrollPane scpListeLot;
  private NRiTable tblListeLot;
  private SNPanelTitre pnlCreationLot;
  private SNLabelChamp lbLibelleLot;
  private SNTexte tfLibelleLot;
  private SNMessage snMessageReste;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
