/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.snparticipationfrais;

import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.IdParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.ListeParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.ParticipationFraisExpedition;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une participation aux frais d'expédition.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un ParticipationFraisExpedition dans le logiciel. Il gère l'affichage des
 * ParticipationFraisExpedition et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNParticipationFraisExpedition à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le VGVM01FX_EX.
 */
public class SNParticipationFraisExpedition
    extends SNComboBoxObjetMetier<IdParticipationFraisExpedition, ParticipationFraisExpedition, ListeParticipationFraisExpedition> {

  /**
   * Constructeur par defaut
   */
  public SNParticipationFraisExpedition() {
    super();
  }

  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeParticipationFraisExpedition());
  }

  /**
   * Sélectionner un ParticipationFraisExpedition de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampParticipationFraisExpedition) {
    if (pChampParticipationFraisExpedition == null || pLexical.HostFieldGetData(pChampParticipationFraisExpedition) == null) {
      throw new MessageErreurException("Impossible de sélectionner la participation aux frais d'expedition car son code est invalide.");
    }

    IdParticipationFraisExpedition idParticipationFraisExpedition = null;
    String valeurParticipationFraisExpedition = pLexical.HostFieldGetData(pChampParticipationFraisExpedition);
    if (valeurParticipationFraisExpedition != null && !valeurParticipationFraisExpedition.trim().isEmpty()) {
      idParticipationFraisExpedition =
          IdParticipationFraisExpedition.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampParticipationFraisExpedition));
    }
    setIdSelection(idParticipationFraisExpedition);
  }

  /**
   * Renseigner les champs RPG correspondant au ParticipationFraisExpedition sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampParticipationFraisExpedition) {
    ParticipationFraisExpedition participationFraisExpedition = getSelection();
    if (participationFraisExpedition != null) {
      pLexical.HostFieldPutData(pChampParticipationFraisExpedition, 0, participationFraisExpedition.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampParticipationFraisExpedition, 0, "");
    }
  }
}
