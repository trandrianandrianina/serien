/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.commun.snpays;

import ri.serien.libcommun.gescom.personnalisation.pays.IdPays;
import ri.serien.libcommun.gescom.personnalisation.pays.ListePays;
import ri.serien.libcommun.gescom.personnalisation.pays.Pays;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un pays.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un pays dans le logiciel. Il gère l'affichage des pays existants et la
 * sélection de l'un d'entre eux.
 * Il peut également mettre à jour le champs RPG contenant le libellé du pays.
 *
 * Utilisation :
 * - Ajouter un composant SNPays à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM03FM_B7.
 */
public class SNPays extends SNComboBoxObjetMetier<IdPays, Pays, ListePays> {
  /**
   * Constructeur par défaut.
   */
  public SNPays() {
    super();
  }

  /**
   * Chargement des données pour la combobox.
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListePays());
  }

  /**
   * Sélectionner un pays de la liste déroulante à partir du champ RPG contenant le code pays.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampPays) {
    if (pChampPays == null) {
      throw new MessageErreurException("Impossible de sélectionner le pays car le nom du champ du code pays est invalide.");
    }

    // Récupérer la valeur du champ
    String valeurPays = pLexical.HostFieldGetData(pChampPays);

    // Construire l'identifiant du pays
    IdPays idPays = null;
    if (!valeurPays.trim().isEmpty()) {
      idPays = IdPays.getInstance(valeurPays);
    }

    // Sélectionner le pays dans la liste déroulante
    setIdSelection(idPays);
  }

  /**
   * Sélectionner un pays de la liste déroulante à partir du champ RPG contenant le code pays et du champ contenant le libellé du pays.
   * Parfois dans les bases de données le champ code n'est pas renseigné alors que le champ libellé l'a été.
   * On privilégie le champ code. Mais s'il est vide on essai de rechercher par libellé.
   */
  public void setSelectionParChampRPG(Lexical pLexical, String pChampPays, String pChampLibellePays) {
    if (pChampPays == null) {
      throw new MessageErreurException("Impossible de sélectionner le pays car le nom du champ du code pays est invalide.");
    }
    if (pChampLibellePays == null) {
      throw new MessageErreurException("Impossible de sélectionner le pays car le nom du champ du libellé pays est invalide.");
    }

    // Récupérer les valeurs du code et du libéllé du pays
    String codePays = pLexical.HostFieldGetData(pChampPays);
    String libellePays = Constantes.normerTexte(pLexical.HostFieldGetData(pChampLibellePays));

    // Chercher le pays via son code si celui-ci est renseigné
    IdPays idPays = null;
    if (codePays != null && !codePays.trim().isEmpty()) {
      // Construire l'identifiant du pays
      idPays = IdPays.getInstance(codePays);
    }
    // Chercher le pays via son libellé si celui-ci est renseigné
    else if (libellePays != null && !libellePays.trim().isEmpty()) {
      // Récupérer la liste des pays
      ListePays listePays = getListe();
      if (listePays != null) {
        // Rechercher le libellé dans la liste des pays
        idPays = listePays.getIdPaysParNom(libellePays);
      }
    }

    // Sélectionner le pays dans la liste déroulante
    setIdSelection(idPays);

    // Si on n'a pas trouvé d'identifiant mais que le libellé du pays est renseigné, on force son affichage.
    if (idPays == null && libellePays != null && !libellePays.trim().isEmpty()) {
      setForceTexte(libellePays);
      DialogueInformation.afficher("Le libellé du pays \"" + libellePays
          + "\" n'a pas pu trouvé dans la liste des pays référencés dans le paramètre 'CP' de la personnalisation des ventes.\n"
          + "Pour corriger le problème, vous devez vérifier que la bonne orthographe a été utilisée ou ajoutez ce pays s'il n'est pas "
          + "présent dans le paramètre 'CP'.");
    }
  }

  /**
   * Renseigner les champs RPG correspondant au pays sélectionnée.
   * Ne met à jour que le champs RPG contenant le code du pays.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampPays) {
    IdPays idPays = getIdSelection();
    if (idPays != null) {
      pLexical.HostFieldPutData(pChampPays, 0, idPays.getTexte());
    }
    else {
      pLexical.HostFieldPutData(pChampPays, 0, "");
    }
  }

  /**
   * Renseigner les champs RPG correspondant au pays sélectionnée.
   * Met à jour que les champs RPG contenant le code du pays et le libellé du pays.
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampCodePays, String pChampLibellePays) {
    renseignerChampRPG(pLexical, pChampCodePays);
    Pays Pays = getSelection();
    if (Pays != null) {
      pLexical.HostFieldPutData(pChampLibellePays, 0, Pays.getLibelle());
    }
    else {
      pLexical.HostFieldPutData(pChampLibellePays, 0, "");
    }
  }

}
