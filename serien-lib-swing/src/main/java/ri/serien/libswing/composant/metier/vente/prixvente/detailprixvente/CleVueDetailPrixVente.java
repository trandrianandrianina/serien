/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente;

import ri.serien.libswing.moteur.mvc.InterfaceCleVue;

/**
 * Clé d'une vue de la boîte de dialogue qui affiche le détail du calcul d'un prix de vente.
 *
 * Chaque vue est identifié par un index (sa position dans la barre d'onglets) et un libellé (le titre de l'onglet).
 */
public class CleVueDetailPrixVente implements InterfaceCleVue {
  private Integer index = null;
  private String libelle = null;
  
  /**
   * Constructeur privé.
   *
   * @param pIndex Index de la vue.
   * @param pLibelle Libellé de la vue.
   */
  protected CleVueDetailPrixVente(Integer pIndex, String pLibelle) {
    index = pIndex;
    libelle = pLibelle;
  }
  
  /**
   * Indice de la vue (sa position dans la barre d'onglets).
   */
  @Override
  public Integer getIndex() {
    return index;
  }
  
  /**
   * Libellé de la vue (le titre de l'onglet).
   */
  @Override
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Clé de la vue résumé dans une chaîne de caractères.
   */
  @Override
  public String toString() {
    return String.valueOf(libelle + " (" + index + ")");
  }
}
