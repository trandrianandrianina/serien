/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.contact.sncontact;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;

import ri.serien.libcommun.commun.EnumModeUtilisation;
import ri.serien.libcommun.exploitation.menu.EnumPointMenu;
import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.CritereContact;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.contact.ParametreMenuContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.IdLienContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.LienContact;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceCommun;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * SNC-5512 - Projet provisoirement avorté
 *
 * Composant d'affichage et de sélection d'un contact.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un contact dans le logiciel. Il gère l'affichage d'un contact et la
 * sélection de l'un d'entre eux. Il comprend un TextField et un bouton permettant de sélectionner un contact dans une boîte de dialogue.
 *
 * Utilisation :
 * - Ajouter un composant SNContact à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé. Les informations nécessaires sont la session et l'établissement.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 */
public class SNContact extends SNComposantAvecEtablissement {
  private SNTexte tfTexteRecherche;
  private SNBoutonRecherche btnDialogueRechercheContact;
  private Contact contactSelectionne = null;
  private boolean fenetreContactAffichee = false;
  private boolean rafraichir = false;
  private String texteRecherchePrecedente = null;
  private String message = null;
  private CritereContact critereContact = new CritereContact();
  private boolean keyEnterPressed = false;
  // Défini quel type de contact : client, fournisseur, ...
  private EnumTypeContact typeContact = null;
  private IdFournisseur idFournisseur = null;
  private IdClient idClient = null;
  private LienContact lienContact = null;
  private boolean isComposantAffichage = false;
  
  /**
   * Constructeur par défaut.
   */
  public SNContact() {
    super();
    setName("SNContact");

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    
    setLayout(new GridBagLayout());
    
    // Texte de recherche
    tfTexteRecherche = new SNTexte();
    tfTexteRecherche.setName("tfTexteRecherche");
    tfTexteRecherche.setEditable(true);
    tfTexteRecherche.setToolTipText(VueComposantContact.RECHERCHE_GENERIQUE_TOOLTIP);
    add(tfTexteRecherche,
        new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
    
    // Bouton loupe
    btnDialogueRechercheContact = new SNBoutonRecherche();
    btnDialogueRechercheContact.setToolTipText("Recherche de contact");
    btnDialogueRechercheContact.setName("btnContact");
    add(btnDialogueRechercheContact, new GridBagConstraints());
    
    // Focus listener pour lancer la recherche quand on quitte la zone de saisie
    tfTexteRecherche.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        tfTexteRechercheFocusLost(e);
      }
      
      @Override
      public void focusGained(FocusEvent e) {
        tfTexteRechercheFocusGained(e);
      }
    });
    
    // ActionListener pour capturer l'utilisation de la touche entrée
    tfTexteRecherche.addActionListener(new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        lancerRecherche(false);
      }
    });
    
    // Etre notifier lorsque le bouton est cliqué
    btnDialogueRechercheContact.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnDialogueRechercheContactActionPerformed(e);
      }
    });
  }
  
  /**
   * Activer ou désactiver la saisie du composant.
   *
   * Cette méthode est à utiliser lorsque le composant devient saisissable ou non saisissable en fonction du contexte. Dans ce cas,
   * le bouton de recherche est grisé et on le laisse visible pour que l'utilisateur comprenne qu'il pourra saisir des informations
   * dans ce composant dans certains cas de figure.
   *
   * Voir aussi setEnabled().
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    tfTexteRecherche.setEnabled(pEnabled);
    btnDialogueRechercheContact.setEnabled(pEnabled);
  }
  
  /**
   * Passer le composant en mode consultation seule.
   *
   * Cette méthode est à utiliser lorsque le composant n'est jamais saisissable quelque soit le contexte. Dans ce cas,
   * le bouton de recherche n'est pas affiché du tout.
   *
   * Lorsque le mode consultation est à false, le composant n'est pas saisissable mais n'est pas grisé pour autant. Il est recommandé
   * d'appeler également setEnabled(false) pour griser le composant.
   */
  public void setEditable(boolean pEditable) {
    tfTexteRecherche.setEditable(pEditable);
    btnDialogueRechercheContact.setVisible(pEditable);
  }
  
  /**
   * Utilliser ce composant en mode affichage (affiche les contacts d'un tiers mais ne renvoi pas de sélection)
   */
  public void setModeComposantAffichage(boolean pModeAffichage) {
    if (pModeAffichage) {
      tfTexteRecherche.setEnabled(false);
      isComposantAffichage = true;
    }
  }
  
  /**
   * Lancement du menu des contacts
   */
  private void afficherMenuContact(IdContact pIdContact) {
    ParametreMenuContact parametres = new ParametreMenuContact();
    parametres.setModeUtilisation(EnumModeUtilisation.CONSULTATION);
    if (typeContact == null) {
      return;
    }
    
    if (typeContact.equals(EnumTypeContact.CLIENT)) {
      parametres.setIdLienContact(IdLienContact.getInstancePourClient(pIdContact.getCode(), idClient));
    }
    else if (typeContact.equals(EnumTypeContact.FOURNISSEUR)) {
      parametres.setIdLienContact(IdLienContact.getInstancePourFournisseur(pIdContact.getCode(), idFournisseur));
    }
    ManagerSessionClient.getInstance().lancerPointMenu(getSession(), IdEnregistrementMneMnp.getInstance(EnumPointMenu.CONTACTS),
        parametres);
  }
  
  /**
   * Charger la liste des contacts.
   */
  public void charger(boolean pForcerRafraichissement, boolean pForcerAffichageDialogue) {
    // Tester s'il faut rafraîchir la liste
    if (!rafraichir && !pForcerRafraichissement) {
      return;
    }
    lancerRecherche(pForcerAffichageDialogue);
    
    rafraichir = false;
  }
  
  /**
   * Initialiser les valeurs du composant pour se retrouver dans la même situation que si aucun contact n'est sélectionnée.
   */
  private void initialiserRechercheContactFournisseur(IdFournisseur pIdFournisseur) {
    typeContact = EnumTypeContact.FOURNISSEUR;
    idFournisseur = pIdFournisseur;
    idClient = null;
    if (idFournisseur == null) {
      throw new MessageErreurException("L'id du fournisseur est invalide pour la recherche des contacts fournisseurs.");
    }
    
    // Effacer les critères de recherche (mais pas celui du type de contact)
    critereContact.initialiserContactFournisseur(typeContact, pIdFournisseur);
  }
  
  /**
   * Initialiser les valeurs du composant pour se retrouver dans la même situation que si aucun contact n'est sélectionnée.
   */
  private void initialiserRechercheContactClient(IdClient pIdClient) {
    typeContact = EnumTypeContact.CLIENT;
    idFournisseur = null;
    idClient = pIdClient;
    if (idClient == null) {
      throw new MessageErreurException("L'id du client est invalide pour la recherche des contacts clients.");
    }
    
    // Effacer les critères de recherche (mais pas celui du type de contact)
    critereContact.initialiserContactClient(typeContact, pIdClient);
  }
  
  /**
   * Lancer une recherche parmi les contacts.
   */
  private void lancerRecherche(boolean forcerAffichageDialogue) {
    // Vérifier la session
    if (getSession() == null) {
      throw new MessageErreurException("La session du composant de sélection des contacts est invalide.");
    }
    
    // Ne rien faire si le texte à rechercher n'a pas changé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && tfTexteRecherche.getText().equals(texteRecherchePrecedente)) {
      return;
    }
    
    // Lancer une recherche de contact
    critereContact.setIdEtablissement(getIdEtablissement());
    critereContact.setTexteRechercheGenerique(tfTexteRecherche.getText());
    
    if (idClient != null) {
      initialiserRechercheContactClient(idClient);
    }
    else if (idFournisseur != null) {
      initialiserRechercheContactFournisseur(idFournisseur);
    }
    
    List<IdContact> listeIdContact = ManagerServiceCommun.chargerListeIdContact(getIdSession(), critereContact);
    
    // Sélectionner automatiquement le seul contact trouvé (sauf si la boîte de dialogue est forcée)
    if (listeIdContact != null && !forcerAffichageDialogue && listeIdContact.size() == 1) {
      setSelection(ManagerServiceCommun.chargerContact(getIdSession(), listeIdContact.get(0)));
    }
    // Afficher la boîte de dialogue si : affichage forcé de la boîte de dialogue, aucun ou plusieurs contacts trouvés
    else {
      afficherDialogueRechercheContact(listeIdContact);
    }
    
    // Sélectionner automatiquement l'ensemble du champ afin que la prochaine saisie vienne en remplacement
    tfTexteRecherche.selectAll();
  }
  
  /**
   * Affiche la fenêtre de recherche des contacts.
   */
  private void afficherDialogueRechercheContact(List<IdContact> pListeIdContact) {
    // Sécurité pour éviter que la boîte de dialogue ne s'affiche deux fois
    if (fenetreContactAffichee) {
      return;
    }
    fenetreContactAffichee = true;
    
    // Afficher la boîte de dialogue de recherche d'un contact
    critereContact.setIdEtablissement(getIdEtablissement());
    critereContact.setTexteRechercheGenerique(tfTexteRecherche.getText());
    if (idClient != null) {
      initialiserRechercheContactClient(idClient);
    }
    else if (idFournisseur != null) {
      initialiserRechercheContactFournisseur(idFournisseur);
    }
    
    ModeleComposantContact modele = null;
    switch (typeContact) {
      case CLIENT:
      case PROSPECT:
        modele = new ModeleComposantContact(getSession(), typeContact, idClient);
        break;
      case FOURNISSEUR:
        modele = new ModeleComposantContact(getSession(), typeContact, idFournisseur);
        break;
      default:
        throw new MessageErreurException("Cette recherche n'est pas prise en compte.");
    }
    modele.setCritereContact(critereContact);
    modele.setListeIdContact(pListeIdContact);
    modele.setMessageInformation(message);
    
    VueComposantContact vueComposantContact = new VueComposantContact(modele);
    vueComposantContact.afficher();
    
    // Sélectionner le contact retourné par la boîte de dialogue (ou aucun si null)
    if (modele.isSortieAvecValidation()) {
      setSelection(modele.getContactSelectionnee());
    }
    // La boîte de dialogue est annulé, on remet le contact sélectionné précédement
    else {
      rafraichirTexteRecherche();
    }
    fenetreContactAffichee = false;
  }
  
  /**
   * Rafraîchir le texte de la recherche avec le contact sélectionné.
   */
  private void rafraichirTexteRecherche() {
    // Mettre à jour le composant
    if (contactSelectionne != null) {
      tfTexteRecherche.setText(contactSelectionne.toString());
    }
    else {
      tfTexteRecherche.setText("");
    }
    
    // Mémoriser le dernier texte pour ne pas relancer une recherche inutile
    texteRecherchePrecedente = tfTexteRecherche.getText();
  }
  
  // -- Méthodes évènementielles
  
  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      tfTexteRecherche.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteRechercheFocusLost(FocusEvent e) {
    try {
      if (keyEnterPressed) {
        lancerRecherche(false);
      }
      keyEnterPressed = false;
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btnDialogueRechercheContactActionPerformed(ActionEvent e) {
    try {
      lancerRecherche(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Contact sélectionné.
   */
  public Contact getSelection() {
    return contactSelectionne;
  }
  
  /**
   * Modifier l'adresse fournisseur sélectionnée.
   */
  public void setSelection(Contact pContactSelectionne) {
    // Si mode affichage
    if (isComposantAffichage && pContactSelectionne != null) {
      afficherMenuContact(pContactSelectionne.getId());
    }
    else {
      // Modifier le client sélectionné
      contactSelectionne = pContactSelectionne;
      
      // Effacer les critères de recherche si aucun client n'est sélectionné
      if (contactSelectionne == null) {
        if (typeContact != null) {
          switch (typeContact) {
            case CLIENT:
            case PROSPECT:
              critereContact.initialiserContactClient(typeContact, idClient);
              break;
            case FOURNISSEUR:
              critereContact.initialiserContactFournisseur(typeContact, idFournisseur);
              break;
            default:
              throw new MessageErreurException("Cette recherche n'est pas prise en compte.");
          }
        }
      }
      
      // Rafraichir l'affichage dans tous les cas
      // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
      rafraichirTexteRecherche();
      
      // Notifier uniquement si la sélection a changé
      fireValueChanged();
    }
  }
  
  /**
   * Modifier le contact sélectionné à partir de son identifiant.
   */
  public void setSelectionParId(IdContact pIdContactSelectionne) {
    // Tester si le contact a changé
    IdContact idContactActuel = null;
    if (contactSelectionne != null) {
      idContactActuel = contactSelectionne.getId();
    }
    if (Constantes.equals(idContactActuel, pIdContactSelectionne)) {
      return;
    }
    
    // Renseigner le contact sélectionné
    if (pIdContactSelectionne == null) {
      contactSelectionne = null;
    }
    else {
      // Charger le nouveau contact à partir de son identifiant
      contactSelectionne = ManagerServiceCommun.chargerContact(getIdSession(), pIdContactSelectionne);
    }
    
    // Si mode affichage
    if (pIdContactSelectionne != null && isComposantAffichage) {
      afficherMenuContact(pIdContactSelectionne);
    }
    
    // Effacer les critères de recherche si aucun client n'est sélectionné
    if (contactSelectionne == null) {
      switch (typeContact) {
        case CLIENT:
        case PROSPECT:
          critereContact.initialiserContactClient(typeContact, idClient);
          break;
        case FOURNISSEUR:
          critereContact.initialiserContactFournisseur(typeContact, idFournisseur);
          break;
        default:
          throw new MessageErreurException("Cette recherche n'est pas prise en compte.");
      }
    }
    
    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();
    
    // Notifier uniquement si la sélection a changé
    fireValueChanged();
  }
  
  /**
   * Modifier le contact sélectionné en sélectionnant le contact principal. Si pas de contact principal on prend le premier contact de la
   * liste.
   */
  public void setSelectionContactPrincipal() {
    contactSelectionne = null;
    
    // Lancer une recherche de contact
    critereContact.setIdEtablissement(getIdEtablissement());
    critereContact.setTexteRechercheGenerique(tfTexteRecherche.getText());
    
    if (idClient != null) {
      initialiserRechercheContactClient(idClient);
      contactSelectionne = ManagerServiceCommun.chargerContactPrincipalClient(getIdSession(), idClient);
    }
    else if (idFournisseur != null) {
      initialiserRechercheContactFournisseur(idFournisseur);
      contactSelectionne = ManagerServiceCommun.chargerContactPrincipalFournisseur(getIdSession(), idFournisseur);
    }
    else {
      return;
    }
    
    if (contactSelectionne == null) {
      List<IdContact> listeIdContact = ManagerServiceCommun.chargerListeIdContact(getIdSession(), critereContact);
      if (listeIdContact != null && listeIdContact.size() > 0) {
        contactSelectionne = ManagerServiceCommun.chargerContact(getIdSession(), listeIdContact.get(0));
      }
    }
    
    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();
    
    // Notifier uniquement si la sélection a changé
    fireValueChanged();
  }
  
  /**
   * Sélectionner un contact à partir des champs RPG.
   */
  public void setSelectionParChampRPG(Lexical pLexical, String pNomChampRPG) {
    // Récupérer la valeur du champ
    String numeroContactEnTexte = pLexical.HostFieldGetData(pNomChampRPG);
    if (numeroContactEnTexte == null) {
      throw new MessageErreurException("Impossible de sélectionner le contact car le nom du champ est invalide.");
    }
    
    // Convertir le champ RPG
    Integer numeroContact = 0;
    try {
      numeroContact = Integer.parseInt(numeroContactEnTexte);
    }
    catch (NumberFormatException e) {
      // RAS
    }
    
    // Sélectionner le contact
    if (numeroContact != 0) {
      IdContact idContact = IdContact.getInstance(numeroContact);
      setSelectionParId(idContact);
    }
    else {
      setSelectionParId(null);
    }
  }
  
  /**
   * Modifier le contact sélectionné à partir de son nom.
   */
  public void setSelectionParCritereContact(CritereContact pCriteres) {
    if (pCriteres == null) {
      return;
    }
    
    List<IdContact> listeIdContact = new ArrayList<IdContact>();
    listeIdContact = ManagerServiceCommun.chargerListeIdContact(getIdSession(), pCriteres);
    
    // Modifier le client sélectionné
    contactSelectionne = ManagerServiceCommun.chargerContact(getIdSession(), listeIdContact.get(0));
    
    // Effacer les critères de recherche si aucun client n'est sélectionné
    if (contactSelectionne == null) {
      switch (typeContact) {
        case CLIENT:
        case PROSPECT:
          critereContact.initialiserContactClient(typeContact, idClient);
          break;
        case FOURNISSEUR:
          critereContact.initialiserContactFournisseur(typeContact, idFournisseur);
          break;
        default:
          throw new MessageErreurException("Cette recherche n'est pas prise en compte.");
      }
    }
    
    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();
    
    // Notifier uniquement si la sélection a changé
    fireValueChanged();
  }
  
  public String getMessage() {
    return message;
  }
  
  public void setMessage(String pMessage) {
    message = pMessage;
  }
  
  /**
   * Renvoyer si le composant sert pour quel type de contact.
   */
  public EnumTypeContact getTypeContact() {
    return typeContact;
  }
  
  /**
   * Mettre à jour si le composant sert pour quel type de contact.
   */
  public void setTypeContact(EnumTypeContact pTypeContact) {
    typeContact = pTypeContact;
  }
  
  /**
   * Retourner l'identifiant du fournisseur pour lequel on affiche les contacts
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Mettre à jour l'identifiant du fournisseur pour lequel on affiche les contacts
   */
  public void setIdFournisseur(IdFournisseur pIdFournisseur) {
    idFournisseur = pIdFournisseur;
  }
  
  /**
   * Retourner l'identifiant du client pour lequel on affiche les contacts
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  /**
   * Mettre à jour l'identifiant du client pour lequel on affiche les contacts
   */
  public void setIdClient(IdClient pIdClient) {
    idClient = pIdClient;
  }
}
