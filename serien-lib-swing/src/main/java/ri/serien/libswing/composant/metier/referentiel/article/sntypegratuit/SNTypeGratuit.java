/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.sntypegratuit;

import ri.serien.libcommun.gescom.personnalisation.typegratuit.IdTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.ListeTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.TypeGratuit;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un TypeGratuit.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un TypeGratuit dans le logiciel. Il gère l'affichage des TypeGratuit
 * existants et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNTypeGratuit à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM66_B4.
 */
public class SNTypeGratuit extends SNComboBoxObjetMetier<IdTypeGratuit, TypeGratuit, ListeTypeGratuit> {
  /**
   * Constructeur par défaut
   */
  public SNTypeGratuit() {
    super();
  }

  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeTypeGratuit());
  }

  /**
   * Sélectionner un TypeGratuit de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampTypeGratuit) {
    if (pChampTypeGratuit == null || pLexical.HostFieldGetData(pChampTypeGratuit) == null) {
      throw new MessageErreurException("Impossible de sélectionner le type de gratuit car son code est invalide.");
    }

    IdTypeGratuit idTypeGratuit = null;
    String valeurTypeGratuit = pLexical.HostFieldGetData(pChampTypeGratuit);
    if (valeurTypeGratuit != null && !valeurTypeGratuit.trim().isEmpty()) {
      idTypeGratuit = IdTypeGratuit.getInstance(valeurTypeGratuit);
    }
    setIdSelection(idTypeGratuit);
  }

  /**
   * Renseigner les champs RPG correspondant au TypeGratuit sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampTypeGratuit) {
    TypeGratuit typeGratuit = getSelection();
    if (typeGratuit != null) {
      pLexical.HostFieldPutData(pChampTypeGratuit, 0, typeGratuit.getId().toString());
    }
    else {
      pLexical.HostFieldPutData(pChampTypeGratuit, 0, "");
    }
  }
}
