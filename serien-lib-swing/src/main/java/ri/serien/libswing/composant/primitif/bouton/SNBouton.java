/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import java.awt.Dimension;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton.
 *
 * Un bouton est définit par une catégorie, un texte et un mnémonique. La catégorie de boutons définit l'aspect général du bouton
 * (validation, annulation, standard ou image). Le texte est le libellé du bouton pour les boutons textuels ou le nom de l'image
 * pour les boutons images. Le mnémonique est la touche de raccourcie associée au bouton, soit sous forme de caractère (char),
 * soit sous forme de code unicode (int).
 *
 * Les boutons images n'ont pas de bord et ont un fond transparent. Seul l'image est visible sur l'écran. Afin de fournir un ressenti à
 * l'utilisateur, il est nécessaire de fournir deux images supplémentaires : une lorsque la souris passe au dessus du bouton et une
 * autre lorsque l'utilisateur clique sur le bouton. Cette classe recherche donc trois images (remplacer le mot "texte" par la valeur
 * fournie lors de la création du bouton) :
 * - /images/texte.png : l'image standard.
 * - /images/texte_rollover.png : l'image lorsque la souris est au dessus du bouton.
 * - /images/texte_pressed.png : l'image lorsque le bouton est enfoncé.
 */
public class SNBouton extends JButton {
  private EnumBouton boutonPreconfigure = null;
  private EnumCategorieBouton categorie = null;
  private String libelle = null;
  private boolean contextuel = false;
  private String infobulle = null;
  private Integer index = null;
  private SNBarreBouton snBarreBouton = null;

  /**
   * Constructeur par défaut.
   */
  public SNBouton() {
    super();
    rafraichir();
  }

  /**
   * Constructeur le plus complet d'un bouton.
   */
  public SNBouton(EnumCategorieBouton pCategorie, String pLibelle, Character pMnemonique, boolean pContextuel) {
    super();

    // Tester les paramètres obligatoires
    if (pCategorie == null) {
      throw new MessageErreurException("La catégorie du bouton est invalide.");
    }
    if (pLibelle == null) {
      throw new MessageErreurException("Le libellé du bouton est invalide.");
    }

    // Nommer le bouton (pour QFTest), on utilise le libellé non transformé
    setName(pLibelle);

    // Configurer le bouton
    categorie = pCategorie;
    libelle = pLibelle;
    setMnemonicSansRafraichir(pMnemonique);
    contextuel = pContextuel;
    rafraichir();
  }

  /**
   * Constructeur d'un bouton qui accepte un code unicode comme mnémonique.
   */
  public SNBouton(EnumCategorieBouton pCategorie, String pTexte, int pMnemonique, boolean pContextuel) {
    this(pCategorie, pTexte, Character.valueOf((char) pMnemonique), pContextuel);
  }

  /**
   * Constructeur pour un bouton standard.
   * Un bouton standard a une couleur de fond orange. Il est forcément associé à un mnémonique fournit en paramètre.
   */
  public SNBouton(String pLibelle, Character pMnemonique, boolean pContextuel) {
    this(EnumCategorieBouton.STANDARD, pLibelle, pMnemonique, pContextuel);
  }

  /**
   * Constructeur pour un bouton pré-configuré.
   * Les boutons pré-configrés sont les boutons de type validation ou annulation. Ils ont des libellés standardisés.
   */
  public SNBouton(EnumBouton pBoutonPreconfigure, boolean pContextuel) {
    setBoutonPreconfigure(pBoutonPreconfigure, pContextuel);
  }
  
  /**
   * Constructeur pour un bouton pré-configuré.
   * Les boutons pré-configrés sont les boutons de type validation ou annulation. Ils ont des libellés standardisés.
   */
  public SNBouton(EnumBouton pBoutonPreconfigure) {
    setBoutonPreconfigure(pBoutonPreconfigure, false);
  }

  /**
   * Rafraîchir le bouton.
   */
  private void rafraichir() {
    // Afficher le curseur "main" lorsque la souris passe au dessus du bouton
    setCursor(SNCharteGraphique.CURSEUR_MAIN);

    // Définir la couleur de fond
    if (categorie != null) {
      setBackground(categorie.getCouleurFond());
    }
    else {
      setBackground(SNCharteGraphique.COULEUR_BOUTON_ORANGE);
    }

    // Définir la police
    if (categorie != null) {
      setFont(categorie.getPolice());
    }
    else {
      setFont(SNCharteGraphique.POLICE_BOUTON);
    }

    // Définir l'infobulle qui rapelle le mnémonique
    if (getMnemonic() != 0) {
      setToolTipText(genererInfobulle(getMnemonic()));
    }
    else {
      setToolTipText(null);
    }

    // Traiter les boutons image et texte à part
    if (categorie != null && categorie == EnumCategorieBouton.IMAGE) {
      // Déterminer les noms des fichiers images
      String fichierStandard = "/images/" + libelle + ".png";
      String fichierRollover = "/images/" + libelle + "_rollover.png";
      String fichierPressed = "/images/" + libelle + "_pressed.png";

      try {
        ImageIcon imageIcon = new ImageIcon(getClass().getResource(fichierStandard));
        setIcon(imageIcon);

        // Définir la taille bu bouton en focntion de la taille de l'image
        setMinimumSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
        setPreferredSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
        setPreferredSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));

        // Rendre le bouton transparent
        setOpaque(false);
        setContentAreaFilled(false);
        setBorderPainted(false);

        // Charger l'image du rollover
        try {
          setRolloverIcon(new ImageIcon(getClass().getResource(fichierRollover)));
        }
        catch (Exception ex) {
          Trace.alerte("Erreur lors du chargement de l'image du bouton : " + fichierRollover);
        }

        // Charger l'image du bouton pressé
        try {
          setPressedIcon(new ImageIcon(getClass().getResource(fichierPressed)));
        }
        catch (Exception ex) {
          Trace.alerte("Erreur lors du chargement de l'image du bouton : " + fichierPressed);
        }
      }
      catch (Exception ex) {
        Trace.alerte("Erreur lors du chargement de l'image du bouton : " + fichierStandard);
        // Le nom de l'image sert d'alternative si on ne trouve pas l'image
        setText(libelle);
      }
    }
    else {
      // Initialiser le libellé initial
      String libelleHTML = libelle;

      // Définir le libellé affiché dans le bouton en fonction du mnémonique
      if (getMnemonic() != 0) {
        libelleHTML = soulignerMnemonique(libelleHTML, getMnemonic());
        libelleHTML = libelleHTML.replace("\n", "<br>");
        libelleHTML = "<html><center>" + libelleHTML + "</center></html>";
      }
      else {
        libelleHTML = "<html><center>" + libelleHTML + "</center></html>";
      }
      setText(libelleHTML);

      // Définir la taille bu bouton, fixe pour un bouton textuel
      setMinimumSize(SNCharteGraphique.TAILLE_BOUTON_STANDARD);
      setPreferredSize(SNCharteGraphique.TAILLE_BOUTON_STANDARD);
      setMaximumSize(SNCharteGraphique.TAILLE_BOUTON_STANDARD);
    }
  }

  /**
   * Souligner la lettre correspondant au mnémonique dans le texte.
   * Le mnémonique est d'abord recherché dans la première de chaque mot puis il est recherché sur l'intégralité du texte.
   */
  private String soulignerMnemonique(String pTexte, int pMnemonique) {
    // Ne rien faire si le texte ou le mnémonique fourni est null
    if (pTexte == null || pMnemonique == 0) {
      return pTexte;
    }

    // Rechercher le mnémonique à la première lettre de chaque mot
    int indexMnemonique = -1;
    for (int i = 0; i < pTexte.length(); i++) {
      if (Character.toUpperCase(pTexte.charAt(i)) == Character.toUpperCase(pMnemonique)) {
        if (i == 0 || pTexte.charAt(i - 1) == ' ' || pTexte.charAt(i - 1) == '\'') {
          indexMnemonique = i;
          break;
        }
      }
    }

    // Rechercher le mnémonique n'importe où dans le texte
    if (indexMnemonique == -1) {
      indexMnemonique = pTexte.toUpperCase().indexOf(Character.toUpperCase(pMnemonique));
    }

    // Entourer la lettre trouvée par <u> et </u>
    String texte = pTexte;
    if (indexMnemonique >= 0 && indexMnemonique <= pTexte.length()) {
      texte = pTexte.substring(0, indexMnemonique) + "<u>" + pTexte.charAt(indexMnemonique) + "</u>"
          + pTexte.substring(indexMnemonique + 1, pTexte.length());
    }

    return texte;
  }

  /**
   * Générer le texte d'infobulle qui rappelle le raccourci utilisable pour déclencher le bouton.
   */
  private String genererInfobulle(int pMnemonique) {
    if (pMnemonique == 0) {
      return "";
    }
    switch (pMnemonique) {
      case KeyEvent.VK_ENTER:
        return "Alt + Entrée";
      case KeyEvent.VK_BACK_SPACE:
        return "Alt + Retour arrière";
      case KeyEvent.VK_SPACE:
        return "Alt + Espace";
      case KeyEvent.VK_LEFT:
        return "Alt + Flèche gauche";
      case KeyEvent.VK_RIGHT:
        return "Alt + Flèche doite";
      case KeyEvent.VK_UP:
        return "Alt + Flèche supérieure";
      case KeyEvent.VK_DOWN:
        return "Alt + Flèche inférieure";
      case KeyEvent.VK_INSERT:
        return "Alt + Insert";
      case KeyEvent.VK_DELETE:
        return "Alt + Suppr";
      case KeyEvent.VK_PAGE_UP:
        return "Alt + Page supérieure";
      case KeyEvent.VK_PAGE_DOWN:
        return "Alt + Page inférieure";
      case KeyEvent.VK_HOME:
        return "Alt + Début";
      case KeyEvent.VK_END:
        return "Alt + Fin";
      default:
        // Le cast en char est nécessaire pour avoir la lettre et pas le code de la lettre
        return "Alt + " + (char) Character.toUpperCase(pMnemonique);
    }
  }

  /**
   * Tester si le bouton est actif ou non.
   */
  public boolean isActif() {
    return isVisible() && isEnabled();
  }

  /**
   * Activer un bouton.
   * Les boutons des catégories standards et annulation sont masqués. Par défaut, les boutons des catégories validation et image sont
   * grisés sauf si l'indicateur pour forcer le masquage est à true.
   */
  public void activer(boolean pVisible, boolean forcerMasquage) {
    // Mettre le bouton visible et dégrisé s'il faut l'activer (on fait les deux pour que cela marche indépendemment de l'état de départ)
    if (pVisible) {
      setVisible(true);
      setEnabled(true);
    }
    // Griser les boutons validation et images sauf si le masquage est forcé
    else if (!forcerMasquage && (categorie == EnumCategorieBouton.VALIDATION || categorie == EnumCategorieBouton.IMAGE)) {
      setVisible(true);
      setEnabled(false);
    }
    // Masquer les autres boutons
    else {
      setVisible(false);
      setEnabled(false);
    }
  }

  /**
   * Récupérer la description du bouton préconfiguré si ce bouton est un bouton préconfiguré.
   * Cela retourne une valeur null si ce n'est pas un bouton préconfiguré.
   */
  public EnumBouton getBoutonPreconfigure() {
    return boutonPreconfigure;
  }

  /**
   * Transformer ce bouton en bouton préconfiguré.
   */
  public void setBoutonPreconfigure(EnumBouton pBoutonPreconfigure) {
    setBoutonPreconfigure(pBoutonPreconfigure, false);
  }
  
  /**
   * Transformer ce bouton en bouton préconfiguré.
   */
  public void setBoutonPreconfigure(EnumBouton pBoutonPreconfigure, boolean pContextuel) {
    boutonPreconfigure = pBoutonPreconfigure;
    contextuel = pContextuel;
    
    // Mettre à jour les propriétés avec celles du bouton préconfiguré
    if (boutonPreconfigure != null) {
      // Mettre à jour la catégorie avec celle du bouton préconfiguré
      categorie = boutonPreconfigure.getCategorie();

      // Mettre à jour le libellé avec celui du bouton préconfiguré
      libelle = boutonPreconfigure.getTexte();

      // Mettre à jour le mnémonique avec celui du bouton préconfiguré
      setMnemonicSansRafraichir(boutonPreconfigure.getMnemonique());

      // Nommer le bouton (pour QFTest), on utilise le libellé non transformé
      setName(libelle);
    }
    else {
      // Mettre la catégorie standard si le bouton n'est plus un bouton préconfiguré
      categorie = EnumCategorieBouton.STANDARD;
    }
    
    // Rafraîchir le bouton
    rafraichir();
  }

  /**
   * Catégorie du bouton : standard, validation ou annulation.
   * Si le bouton est préconfiguré, la catégorie de la préconfiguration à la priorité sur celle configurée sur ce bouton.
   * Si aucune catégorie n'est définie, la catégorie de boutons STANDARD est retournée par défaut.
   */
  public EnumCategorieBouton getCategorie() {
    return categorie;
  }

  /**
   * Texte brut pour créer le bouton, non transformé.
   * Si le bouton est préconfiguré, le libellé de la préconfiguration à la priorité sur celui configuré sur ce bouton.
   */
  public String getLibelle() {
    return libelle;
  }

  /**
   * Modifier le texte du bouton.
   */
  public void setLibelle(String pLibelle) {
    // Ne rien modifier s'il s'agit d'un bouton préconfiguré
    if (boutonPreconfigure != null) {
      throw new MessageErreurException("Il n'est pas possible de modifier le libellé d'un bouton préconfiguré.");
    }

    // Stocker le texte non transformé
    libelle = pLibelle;

    // Nommer le bouton (pour QFTest), on utilise le libellé non transformé
    setName(pLibelle);

    // Rafraîchir le bouton
    rafraichir();
  }

  /**
   * Modifier le raccourci clavier associé au bouton.
   */
  private void setMnemonicSansRafraichir(int pMnemonic) {
    // Appeler la méthode parente pour modifier le mnémonique du bouton
    super.setMnemonic(pMnemonic);
  }

  /**
   * Modifier le raccourci clavier associé au bouton.
   * Les mnémoniques doivent être en majuscules pour fonctionner. On utilise l'attribut standard des composants Java pour mémoriser le
   * mnémonique.
   */
  @Override
  public void setMnemonic(int pMnemonic) {
    // Ne rien modifier s'il s'agit d'un bouton préconfiguré
    if (boutonPreconfigure != null) {
      throw new MessageErreurException("Il n'est pas possible de modifier le mnémonique d'un bouton préconfiguré.");
    }

    // Appeler la méthode parente pour modifier le mnémonique du bouton
    super.setMnemonic(pMnemonic);

    // Rafraîchir le bouton
    rafraichir();
  }

  /**
   * Indique si l'opération permise par ce bouton est également accessible via un menu contextuel.
   */
  public boolean isContextuel() {
    return contextuel;
  }

  /**
   * Tester si le bouton a le libellé fourni en paramètre.
   */
  public boolean isBouton(String pLibelle) {
    return getName().equals(pLibelle);
  }

  /**
   * Tester si le bouton est le bouton pré-configuré fourni en paramètre.
   */
  public boolean isBouton(EnumBouton pEnumBouton) {
    if (boutonPreconfigure == null) {
      return false;
    }
    return boutonPreconfigure.equals(pEnumBouton);
  }

  /**
   * Index associé au bouton (null si aucun index).
   */
  public Integer getIndex() {
    return index;
  }

  /**
   * Modifier l'index associé au bouton.
   * Dans certains cas, il est utile d'associer un index au bouton afin de faciliter son identification ultérieurement. C'est
   * notamment le cas pour les boutons où le libellé est construit dynamiquement.
   */
  public void setIndex(Integer pIndex) {
    index = pIndex;
  }

  /**
   * Récupérer la barre de boutons dans laquelle est situé le bouton.
   * @return null si la barre de boutons n'est dans aucune barre.
   */
  public SNBarreBouton getBarreBouton() {
    return snBarreBouton;
  }

  /**
   * Ajouter le bouton à une barre de boutons.
   */
  public void setBarreBouton(SNBarreBouton pSNBarreBouton) {
    // Tester si la valeur a changé
    if (Constantes.equals(snBarreBouton, pSNBarreBouton)) {
      return;
    }

    // Mémoriser l'ancienne barre de boutons
    SNBarreBouton snBarreBoutonPrecedente = snBarreBouton;

    // Renseigner la nouvelle valeur
    snBarreBouton = pSNBarreBouton;

    // S'enlever de l'ancienne barre de boutons.
    // ATTENTION : A faire après avoir renseigner la nouvelle barre de boutons dans ce bouton pour éviter une boucle infinie.
    // En effet, la méthode ici présente va être rappelée par la barre de boutons pour supprimer le lien dans les deux sens dans le cas où
    // le bouton est enlevé à partir de la barre de boutons.
    if (snBarreBoutonPrecedente != null) {
      snBarreBoutonPrecedente.supprimerBouton(this);
    }

    // Ajouter le bouton à la nouvelle barre de boutons
    if (snBarreBouton != null) {
      snBarreBouton.ajouterBouton(this, true);
    }
  }
}
