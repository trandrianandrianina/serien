/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.dialoguestandard.confirmation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumCategorieBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Demander une confirmation.
 * La boîte de dialogue pose une question à laquelle l'utilisateur doit répondre par "Oui" ou "Non".
 */
public class DialogueConfirmation extends AbstractVueDialogue<ModeleDialogueConfirmation> {
  // Variables
  private static String BOUTON_OUI = "Oui";
  private static String BOUTON_NON = "Non";

  /**
   * Constructeur.
   */
  public DialogueConfirmation(ModeleDialogueConfirmation pModele) {
    super(pModele, false);
    setBackground(SNCharteGraphique.COULEUR_FOND);
  }

  // -- Méthodes publiques

  /**
   * Affiche une boite de dialogue pour une demande de confirmation.
   */
  public static boolean afficher(SessionBase pSessionBase, String pTitre, String pMessage) {
    pTitre = Constantes.normerTexte(pTitre);
    pMessage = Constantes.normerTexte(pMessage);
    ModeleDialogueConfirmation modele = new ModeleDialogueConfirmation(pSessionBase, pTitre, pMessage);
    DialogueConfirmation vue = new DialogueConfirmation(modele);
    vue.afficher();
    return modele.isSortieAvecValidation();
  }

  @Override
  public void initialiserComposants() {
    initComponents();
    setTitle("");
    laMessage.setText("");

    laMessage.setFont(SNCharteGraphique.POLICE_TITRE);
    laMessage.setForeground(Color.BLACK);

    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(new SNBouton(EnumCategorieBouton.VALIDATION, BOUTON_OUI, KeyEvent.VK_ENTER, false), true);
    snBarreBouton.ajouterBouton(new SNBouton(EnumCategorieBouton.ANNULATION, BOUTON_NON, KeyEvent.VK_BACK_SPACE, false), true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  @Override
  public void rafraichir() {
    // Rafraîchir le titre
    setTitle(getModele().getTitreFenetre());

    // Récupérer le message à afficher
    String message = getModele().getTexteMessage();
    
    // Formater le message en HTML s'il ne l'est pas déjà
    if (!message.contains("<html>")) {
      message = message.replaceAll("<", "&lt;");
      message = message.replaceAll(">", "&gt;");
      message = message.replaceAll("\\n", "<br>");
      message = "<html><div style='text-align: center;'>" + message + "</div></html>";
    }

    // Rafraîchir le titre
    laMessage.setText(message);
  }

  // -- Méthodes interactives

  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_OUI)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(BOUTON_NON)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    snPanelPrincipal = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    laMessage = new JLabel();
    snBarreBouton = new SNBarreBouton();

    // ======== this ========
    setTitle("Merci de confirmer cette action");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(600, 300));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== snPanelPrincipal ========
    {
      snPanelPrincipal.setName("snPanelPrincipal");
      snPanelPrincipal.setLayout(new BorderLayout());

      // ======== pnlContenu ========
      {
        pnlContenu.setOpaque(false);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new BorderLayout());

        // ---- laMessage ----
        laMessage.setBorder(null);
        laMessage.setAutoscrolls(false);
        laMessage.setFont(new Font("sansserif", Font.BOLD, 14));
        laMessage.setHorizontalAlignment(SwingConstants.CENTER);
        laMessage.setHorizontalTextPosition(SwingConstants.CENTER);
        laMessage.setName("laMessage");
        pnlContenu.add(laMessage, BorderLayout.CENTER);
      }
      snPanelPrincipal.add(pnlContenu, BorderLayout.CENTER);

      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      snPanelPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(snPanelPrincipal, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond snPanelPrincipal;
  private SNPanelContenu pnlContenu;
  private JLabel laMessage;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
