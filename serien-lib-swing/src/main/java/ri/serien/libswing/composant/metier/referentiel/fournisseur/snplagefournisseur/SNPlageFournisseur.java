/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.fournisseur.snplagefournisseur;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import ri.serien.libcommun.gescom.commun.plagefournisseur.PlageFournisseur;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Composant de saisie de plage de fournisseurs.<br>
 * Ce composant doit être utilisé pour affichage et choix d'une plage de fournisseurs.<br>
 * Utilisation : <br>
 * - Ajouter le composant à un écran.<br>
 * - Définir une SessionBase et un IdEtablissement pour ce composant.<br>
 * - Appeler la méthode rafraichir() après avoir défini la SessionBase et l'IdEtablissement pour que ce composant les transmette aux
 * composants SNFournisseur.<br>
 * - Récupérer la plage de fournisseurs grâce à la méthode getPlageFournisseur().<br>
 * Comportement :<br>
 * - Si le numéro founisseur du début est supérieur au numéro fournisseur de la fin, ce dernier sera effacé.<br>
 * - Si le numéro founisseur de la fin est inférieur au numéro fournisseur du début, ce dernier sera effacé.
 */
public class SNPlageFournisseur extends SNComposantAvecEtablissement {
  private PlageFournisseur plageFournisseur = new PlageFournisseur();

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Constructeur.
   */
  public SNPlageFournisseur() {
    super();
    initComponents();
    tfFournisseurDebut.lierComposantFin(tfFournisseurFin);
    tfFournisseurFin.lierComposantDebut(tfFournisseurDebut);
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Gérer l'activation des composants.
   */
  @Override
  public void setEnabled(boolean pIsEnabled) {
    lbFournisseurDebut.setEnabled(pIsEnabled);
    lbFournisseurFin.setEnabled(pIsEnabled);
    tfFournisseurDebut.setEnabled(pIsEnabled);
    tfFournisseurFin.setEnabled(pIsEnabled);
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir le composant.
   */
  public void rafraichir() {
    rafraichirFournisseurDebut();
    rafraichirFournisseurFin();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Contrôler l'Id fournisseur de la fin.
   * @return
   *         true si le fournisseur de la fin est supérieur ou égal au fournisseur du début.<br>
   *         false si le fournisseur de la fin est inférieur au fournisseur du début.
   */
  private boolean isFournisseurFinValide() {
    if (plageFournisseur.getIdFournisseurDebut() != null) {
      if (plageFournisseur.getIdFournisseurFin() != null) {
        // Comparer uniquement quand les deux valeurs sont renseignées
        if (plageFournisseur.getIdFournisseurDebut().compareTo(plageFournisseur.getIdFournisseurFin()) > 0) {
          return false;
        }
      }
    }

    // Il se peut que l'un des valeurs ou les deux ne sont pas renseignées
    return true;
  }
  
  /**
   * Contrôler l'Id fournisseur du début.
   * @return
   *         true si le fournisseur du début est inférieur ou égal au fournisseur de la fin.<br>
   *         false si le fournisseur du début est supérieur au fournisseur de la fin.
   */
  private boolean isFournisseurDebutValide() {
    if (plageFournisseur.getIdFournisseurFin() != null) {
      if (plageFournisseur.getIdFournisseurDebut() != null) {
        // Comparer uniquement quand les deux valeurs sont renseignées
        if (plageFournisseur.getIdFournisseurFin().compareTo(plageFournisseur.getIdFournisseurDebut()) < 0) {
          return false;
        }
      }
    }
    
    // Il se peut que l'un des valeurs ou les deux ne sont pas renseignées
    return true;
  }

  /**
   * Rafraichir le composant fournisseur du début.
   */
  private void rafraichirFournisseurDebut() {
    tfFournisseurDebut.setSession(getSession());
    tfFournisseurDebut.setIdEtablissement(getIdEtablissement());
    if (!isFournisseurFinValide()) {
      tfFournisseurFin.setSelectionParId(null);
      plageFournisseur.setIdFournisseurFin(null);
      return;
    }
    tfFournisseurDebut.setSelectionParId(plageFournisseur.getIdFournisseurDebut());
  }
  
  /**
   * Rafraichir le composant fournisseur de la fin.
   */
  private void rafraichirFournisseurFin() {
    tfFournisseurFin.setSession(getSession());
    tfFournisseurFin.setIdEtablissement(getIdEtablissement());
    if (!isFournisseurDebutValide()) {
      tfFournisseurDebut.setSelectionParId(null);
      plageFournisseur.setIdFournisseurDebut(null);
      return;
    }
    tfFournisseurFin.setSelectionParId(plageFournisseur.getIdFournisseurFin());
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner la plage de founisseurs.
   * @return PlageFournisseur
   */
  public PlageFournisseur getPlageFournisseur() {
    return plageFournisseur;
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Modifier la valeur de l'Id fournisseur du début.
   * @param e
   */
  private void tfFournisseurDebutValueChanged(SNComposantEvent e) {
    try {
      plageFournisseur.setIdFournisseurDebut(tfFournisseurDebut.getIdSelection());
      // rafraichirFournisseurDebut();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Modifier la valeur de l'Id fournisseur de la fin.
   * @param e
   */
  private void tfFournisseurFinValueChanged(SNComposantEvent e) {
    try {
      plageFournisseur.setIdFournisseurFin(tfFournisseurFin.getIdSelection());
      // rafraichirFournisseurFin();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbFournisseurDebut = new SNLabelChamp();
    tfFournisseurDebut = new SNFournisseur();
    lbFournisseurFin = new SNLabelChamp();
    tfFournisseurFin = new SNFournisseur();
    
    // ======== this ========
    setMinimumSize(new Dimension(357, 70));
    setPreferredSize(new Dimension(400, 70));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
    
    // ---- lbFournisseurDebut ----
    lbFournisseurDebut.setText("Fournisseur de d\u00e9but");
    lbFournisseurDebut.setName("lbFournisseurDebut");
    add(lbFournisseurDebut,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
    
    // ---- tfFournisseurDebut ----
    tfFournisseurDebut.setMaximumSize(new Dimension(32767, 32767));
    tfFournisseurDebut.setPreferredSize(new Dimension(250, 30));
    tfFournisseurDebut.setName("tfFournisseurDebut");
    tfFournisseurDebut.addSNComposantListener(e -> tfFournisseurDebutValueChanged(e));
    add(tfFournisseurDebut,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
    
    // ---- lbFournisseurFin ----
    lbFournisseurFin.setText("Fournisseur de fin");
    lbFournisseurFin.setName("lbFournisseurFin");
    add(lbFournisseurFin,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
    
    // ---- tfFournisseurFin ----
    tfFournisseurFin.setMaximumSize(new Dimension(32767, 32767));
    tfFournisseurFin.setPreferredSize(new Dimension(250, 30));
    tfFournisseurFin.setName("tfFournisseurFin");
    tfFournisseurFin.addSNComposantListener(e -> tfFournisseurFinValueChanged(e));
    add(tfFournisseurFin,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbFournisseurDebut;
  private SNFournisseur tfFournisseurDebut;
  private SNLabelChamp lbFournisseurFin;
  private SNFournisseur tfFournisseurFin;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
