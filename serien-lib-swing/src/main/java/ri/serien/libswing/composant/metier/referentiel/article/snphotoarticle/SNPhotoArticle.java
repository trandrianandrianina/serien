/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import ri.serien.libcommun.gescom.commun.imagearticle.ListeImageArticle;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.image.OutilImage;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.glasspane.SNPanelGlassPaneSoleilAttente;

/**
 * Affiche la photographie principale d'un article.
 *
 * Le composant comporte deux parties. La partie principale, à gauche, est destinée à afficher la photographie de l'article. Par défaut,
 * elle a un cadre noir et un fond blanc. La partie de droite comporte le bouton "plus" en haut à droite permettant d'afficher la
 * boîte de dialogue avec toutes les photographies de l'article. Lors du chargement de la photographie, un soleil d'attente est affiché
 * dans le cadre de gauche (SNPanelGlassPaneSoleilAttente).
 *
 * La partie de gauche mesure 25 pixels de large (20 pixels pour le bouton plus et 5 pixels de marge). Si on souhaite avoir un cadre
 * carré pour afficher la photographie, ce composant doit donc avoir 25 pixels de plus en largeur par rapport à sa hauteur.
 *
 * Caractéristiques :
 * - Cadre noir autour de la photographie.
 * - Fond blanc sous la photgraphie.
 * - Laisser le bouton transparent pour voir le fond sous les parties transparentes de l'image : btnPhoto.setContentAreaFilled(false);
 * - Avoir un curseur en forme de main au dessus de la photographie : btnPhoto.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
 */
public class SNPhotoArticle extends JPanel {
  // Constantes
  private static final ImageIcon LOGO_PAS_DE_PHOTO =
      new ImageIcon(SNPhotoArticle.class.getClassLoader().getResource("images/pas_photo.png"));

  // Variables
  private String cheminDossier = null;
  private String codeArticle = null;
  private boolean afficherPhoto = false;
  private ListeImageArticle listePhotoArticle = new ListeImageArticle();
  private ImageIcon miniatureImageArticle = null;
  private ThreadDownload threadTelechargementImage = null;
  
  private Boolean isConsultation = null;

  /**
   * Constructeur
   */
  public SNPhotoArticle() {
    super();
    initComponents();
  }

  /**
   * Charger les photographies de l'article.
   *
   * @param pCheminDossier Chemin du dossier comportant les phorographies de l'article.
   * @param pCodeArticle Code de l'article.
   * @param pAfficherPhoto true=Affiche la photographie de l'article, false=n'affiche rien.
   */
  public void chargerImageArticle(String pCheminDossier, String pCodeArticle, boolean pAfficherPhoto) {
    cheminDossier = pCheminDossier;
    codeArticle = pCodeArticle;
    afficherPhoto = pAfficherPhoto;
    listePhotoArticle = null;
    
    // Effacer l'image (avec null pour avoir un fond blanc et non directement le logo pas de photo) et le tooltip actuel
    if (!Constantes.equals(codeArticle, pCodeArticle) || !Constantes.equals(cheminDossier, pCheminDossier)) {
      btnAffichageMiniatureImageArticle.setIcon(null);
    }
    
    // Chargement de l'image de l'article
    chargerImageArticle();
  }

  /**
   * Charger les photographies de l'article.
   *
   * @param pCheminDossier Chemin du dossier comportant les phorographies de l'article.
   * @param pCodeArticle Code de l'article.
   * @param pAfficherPhoto true=Affiche la photographie de l'article, false=n'affiche rien.
   */
  public void chargerImageArticle(String pCheminDossier, String pCodeArticle, boolean pAfficherPhoto, boolean pIsConsultation) {
    isConsultation = pIsConsultation;
    chargerImageArticle(pCheminDossier, pCodeArticle, pAfficherPhoto);
  }

  /**
   * Charger les photographies de l'article courant.
   * Si l'article possède des images, afficher la première.
   */
  private void chargerImageArticle() {
    // Attention !!! Il y a un blocage au niveau de la gestion des download de SessionTransfert car cela se fait un par un et si l'image
    // est grosse alors cela bloque l'affichage du panel suivant qui attend pour les icones et autres (cela se passe dans le
    if (threadTelechargementImage != null && threadTelechargementImage.isAlive()) {
      threadTelechargementImage.interrupt();
    }
    
    // Lancer la thread de téléchargement des images
    threadTelechargementImage = new ThreadDownload(cheminDossier, afficherPhoto);
    threadTelechargementImage.start();
  }

  /**
   * Thread de téléchargement des photographies des articles.
   */
  private class ThreadDownload extends Thread {
    private String cheminDossier = null;
    private boolean afficherPhoto = false;

    // Les fichiers contenus dans le dossier sont listés dans un tableau.
    String[] tableauCheminDAccesFichier = null;

    public ThreadDownload(String pCheminDossier, boolean pAfficherPhoto) {
      cheminDossier = pCheminDossier;
      afficherPhoto = pAfficherPhoto;
    }

    @Override
    public void run() {
      // Quitter de suite si le chemin est invalide et ajout du logo pas de photo
      if (cheminDossier == null || cheminDossier.trim().isEmpty()) {
        btnAffichageMiniatureImageArticle.setIcon(LOGO_PAS_DE_PHOTO);
        return;
      }
      tableauCheminDAccesFichier = ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().listerDossier(cheminDossier);
      // Si le paramétrage du lecteur pour accéder aux images est mal fait, on envoie un message d'erreur pour en informer l'utilistateur.
      if (tableauCheminDAccesFichier == null) {
        throw new MessageErreurException("Impossible de visualiser ou d'ajouter des photos à l'article. \n "
            + "La racine du chemin d'accès aux photos liées à un article est invalide. \n\n"
            + "Il se peut que le paramétrage ne soit pas fait ou que le lecteur réseau paramétré ne soit pas créé sur votre poste.");
      }
      if (tableauCheminDAccesFichier.length <= 1) {
        // Si le tableau ni contient pas d'image alors la miniature est passée à null.
        miniatureImageArticle = null;
      }
      else {
        // Lancer l'animation d'attente
        pnlGlassPane.demarrerAnimation();
        // On parcourt la liste des images dans le dossier sur le lecteur paramétré.
        for (String fichier : tableauCheminDAccesFichier) {
          // Si le fichier est bien une image,
          if (isFichierImage(fichier)) {
            // Si le chemin d'accès à l'image est un chemin réseau,
            if (isCheminReseau(cheminDossier)) {
              // On charge la miniature de l'image à partir du chemin d'accès au dossier et du nom de l'image.
              miniatureImageArticle = ManagerSessionClient.getInstance().chargerImage(cheminDossier + fichier, false, true);
            }
            else {
              // Sinon, si l'image est sur un lecteur réseau, on transforme le chemin d'accès pour qu'il soit interprétable.
              String cheminDossierTemp = cheminDossier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
              // On charge la miniature de l'image à partir de ce nouveau chemin d'accès.
              miniatureImageArticle = new ImageIcon(cheminDossierTemp + fichier);
            }
            // On adapte la taille de la miniature afin qu'elle s'affiche correctement dans la zone dédiée.
            miniatureImageArticle = OutilImage.reduireIcone(miniatureImageArticle, btnAffichageMiniatureImageArticle.getWidth(),
                btnAffichageMiniatureImageArticle.getHeight());
            break;
          }
        }

        // Stopper l'animation d'attente
        pnlGlassPane.stopperAnimation();
      }

      rafraichir();
    }
  }
  
  /**
   * Rafraichir l'affichage du composant.
   */
  private void rafraichir() {
    rafraichirComposantAffichageMiniatureImageArticle();
  }
  
  /**
   * Rafraichir l'affichage du composant d'affichage de la miniature de l'image article.
   */
  private void rafraichirComposantAffichageMiniatureImageArticle() {
    // Afficher la première photographie détectée dans le dossier.
    if (afficherPhoto && miniatureImageArticle != null) {
      btnAffichageMiniatureImageArticle.setIcon(miniatureImageArticle);
    }
    else {
      // Sinon on affiche l'image placeholder par défaut.
      btnAffichageMiniatureImageArticle.setIcon(LOGO_PAS_DE_PHOTO);
    }
  }

  /**
   * Indiquer si le chemin est un chemin réseau.
   * @param pCheminDossier Chemin du dossier à contrôler.
   * @return true=chemin réseau, false=chemin local.
   */
  private boolean isCheminReseau(String pCheminDossier) {
    if (pCheminDossier == null || pCheminDossier.indexOf(':') != -1 || pCheminDossier.startsWith("&&")) {
      return false;
    }
    return true;
  }

  /**
   * Contrôler si un fichier est une image à partir de son nom ou son chemin d'accès, en String. <br>
   * On considère que le fichier est une image si son extension correspond aux format jpg, jpeg, png, bmp ou gif.
   *
   * @param pFichier nom ou chemin d'accès au fichier à contrôler.
   * @return <b>True</b> si le fichier est une image, <b>false</b> sinon.
   */
  private boolean isFichierImage(String pFichier) {
    if (pFichier == null) {
      return false;
    }
    pFichier = pFichier.toLowerCase();
    return (!pFichier.startsWith(Constantes.THUMBAIL)) && ((pFichier.lastIndexOf(".gif") != -1) || (pFichier.lastIndexOf(".jpg") != -1)
        || (pFichier.lastIndexOf(".jpeg") != -1) || (pFichier.lastIndexOf(".png") != -1) || (pFichier.lastIndexOf(".bmp") != -1));
  }
  
  /**
   * Afficher la boîte de dialogue qui liste les photographies d'un article.
   */
  public void afficherDialogueListeImage() {
    if (cheminDossier == null) {
      return;
    }
    // On affiche la boîte de dialogue de diaporama des images associées à l'article.
    // Les actions réalisables dans cette boite de dialogue sont coditionnées par le mode d'utilisation de l'écran.
    ModeleDialogueDiaporamaPhoto modele =
        new ModeleDialogueDiaporamaPhoto(ManagerSessionClient.getInstance().getEnvUser(), cheminDossier, isConsultation);
    DialogueDiaporamaPhoto vue = new DialogueDiaporamaPhoto(modele);
    vue.afficher();
    // Si la boîte de dialogue est quittée après un clic sur le bouton valider, on recharge les images afin de mettre à jour le contenu de
    // la miniature.
    if (modele.isSortieAvecValidation()) {
      chargerImageArticle();
    }
  }

  /**
   * Afficher la boîte de dialogue qui liste les photographies de l'article si on clique sur la photographie.
   */
  private void btnAffichageMiniatureImageArticleActionPerformed(ActionEvent e) {
    try {
      afficherDialogueListeImage();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Afficher la boîte de dialogue qui liste les photographies de l'article si on clique sur le bouton détail.
   */
  private void btnAfficherDiaporamaImageArticleActionPerformed(ActionEvent e) {
    try {
      afficherDialogueListeImage();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlGlassPane = new SNPanelGlassPaneSoleilAttente();
    btnAffichageMiniatureImageArticle = new JButton();
    btnAfficherDiaporamaImageArticle = new SNBoutonDetail();
    
    // ======== this ========
    setBorder(null);
    setOpaque(false);
    setMinimumSize(new Dimension(125, 100));
    setPreferredSize(new Dimension(125, 100));
    setMaximumSize(new Dimension(125, 100));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
    
    // ======== pnlGlassPane ========
    {
      pnlGlassPane.setMinimumSize(new Dimension(100, 100));
      pnlGlassPane.setPreferredSize(new Dimension(100, 100));
      pnlGlassPane.setMaximumSize(new Dimension(100, 100));
      pnlGlassPane.setBackground(Color.white);
      pnlGlassPane.setBorder(LineBorder.createBlackLineBorder());
      pnlGlassPane.setName("pnlGlassPane");
      pnlGlassPane.setLayout(new BorderLayout());
      
      // ---- btnAffichageMiniatureImageArticle ----
      btnAffichageMiniatureImageArticle.setMaximumSize(new Dimension(100, 100));
      btnAffichageMiniatureImageArticle.setMinimumSize(new Dimension(100, 100));
      btnAffichageMiniatureImageArticle.setPreferredSize(new Dimension(100, 100));
      btnAffichageMiniatureImageArticle.setBorder(null);
      btnAffichageMiniatureImageArticle.setContentAreaFilled(false);
      btnAffichageMiniatureImageArticle.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btnAffichageMiniatureImageArticle.setBorderPainted(false);
      btnAffichageMiniatureImageArticle.setName("btnAffichageMiniatureImageArticle");
      btnAffichageMiniatureImageArticle.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btnAffichageMiniatureImageArticleActionPerformed(e);
        }
      });
      pnlGlassPane.add(btnAffichageMiniatureImageArticle, BorderLayout.CENTER);
    }
    add(pnlGlassPane,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
    
    // ---- btnAfficherDiaporamaImageArticle ----
    btnAfficherDiaporamaImageArticle.setMaximumSize(new Dimension(20, 20));
    btnAfficherDiaporamaImageArticle.setMinimumSize(new Dimension(20, 20));
    btnAfficherDiaporamaImageArticle.setPreferredSize(new Dimension(20, 20));
    btnAfficherDiaporamaImageArticle.setName("btnAfficherDiaporamaImageArticle");
    btnAfficherDiaporamaImageArticle.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnAfficherDiaporamaImageArticleActionPerformed(e);
      }
    });
    add(btnAfficherDiaporamaImageArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
        GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelGlassPaneSoleilAttente pnlGlassPane;
  private JButton btnAffichageMiniatureImageArticle;
  private SNBoutonDetail btnAfficherDiaporamaImageArticle;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
