/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametreclient;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.ParametreClient;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.checkbox.SNCheckBoxReduit;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;

/**
 * Ce composant permet d'afficher les paramètres d'un client.
 */
public class SNParametreClient extends SNPanelTitre {
  private static final String TITRE = "Client";
  
  // Variables
  private ModeleDialogueDetailPrixVente modele = null;
  private boolean evenementsActifs = false;
  private ParametreClient parametreClient = null;
  private ParametreClient parametreClientInitial = null;
  
  /**
   * Constructeur.
   */
  public SNParametreClient() {
    super();
    initComponents();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modèle associé au composant graphique.
   * @return Modèle de la boîte de dialogue.
   */
  public ModeleDialogueDetailPrixVente getModele() {
    return modele;
  }
  
  /**
   * Modifier le modèle associé au composant graphique.
   * @param pModele Modèle de la boîte de dialogue.
   */
  public void setModele(ModeleDialogueDetailPrixVente pModele) {
    modele = pModele;
  }
  
  /**
   * Indiquer si les évènements doivent être traités.
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraichir les données du composant.
   */
  public void rafraichir() {
    // Empêcher les évènements pendant le rafraichissement
    evenementsActifs = false;
    
    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    parametreClient = getModele().getParametreClientFacture();
    parametreClientInitial = getModele().getParametreClientFactureInitial();
    
    // Rafraichir les composants
    rafraichirTitre();
    rafraichirModeTTC();
    rafraichirColonneTarif();
    rafraichirLibelleTauxRemise();
    rafraichirTauxRemise1();
    rafraichirTauxRemise2();
    rafraichirTauxRemise3();
    
    // Réactiver les évènements pendant le rafraichissement
    evenementsActifs = true;
  }
  
  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirTitre() {
    if (parametreClient != null && parametreClient.getIdClient() != null) {
      setTitre(TITRE + " " + parametreClient.getIdClient());
    }
    else {
      setTitre(TITRE);
    }
  }
  
  /**
   * Rafraichir le mode HT ou TTC.
   */
  private void rafraichirModeTTC() {
    if (parametreClient != null && parametreClient.isModeTTC()) {
      cbModeTTC.setSelected(true);
    }
    else {
      cbModeTTC.setSelected(false);
    }

    cbModeTTC.setEnabled(modele != null && getModele().isModeSimulation());
    lbModeTTC.setImportance(parametreClient != null && parametreClientInitial != null
        && !Constantes.equals(parametreClient.isModeTTC(), parametreClientInitial.isModeTTC()));
  }

  /**
   * Rafraichir la colonne tarif du client (paramètres client).
   */
  private void rafraichirColonneTarif() {
    if (parametreClient != null && parametreClient.getNumeroColonneTarif() != null) {
      Integer numeroColonneTarif = parametreClient.getNumeroColonneTarif();
      if (numeroColonneTarif >= 1 && numeroColonneTarif <= 10) {
        cbColonneTarif.setSelectedIndex(numeroColonneTarif);
      }
      else {
        cbColonneTarif.setSelectedIndex(0);
      }
    }
    else {
      cbColonneTarif.setSelectedIndex(0);
    }
    
    cbColonneTarif.setEnabled(modele != null && getModele().isModeSimulation());
    lbColonneTarif.setImportance(parametreClient != null && parametreClientInitial != null
        && !Constantes.equals(parametreClient.getNumeroColonneTarif(), parametreClientInitial.getNumeroColonneTarif()));
  }
  
  /**
   * Rafraichir le libellé du taux de remise.
   */
  private void rafraichirLibelleTauxRemise() {
    lbTauxRemise.setImportance(parametreClient != null && parametreClientInitial != null
        && (!Constantes.equals(parametreClient.getTauxRemise1(), parametreClientInitial.getTauxRemise1())
            || !Constantes.equals(parametreClient.getTauxRemise2(), parametreClientInitial.getTauxRemise2())
            || !Constantes.equals(parametreClient.getTauxRemise3(), parametreClientInitial.getTauxRemise3())));
  }
  
  /**
   * Rafraichir le taux de remise 1.
   */
  private void rafraichirTauxRemise1() {
    if (parametreClient != null && parametreClient.getTauxRemise1() != null) {
      tfTauxRemise1.setText(parametreClient.getTauxRemise1().getTexteSansSymbole());
    }
    else {
      tfTauxRemise1.setText("");
    }
    
    tfTauxRemise1.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 2.
   */
  private void rafraichirTauxRemise2() {
    if (parametreClient != null && parametreClient.getTauxRemise2() != null) {
      tfTauxRemise2.setText(parametreClient.getTauxRemise2().getTexteSansSymbole());
    }
    else {
      tfTauxRemise2.setText("");
    }
    
    tfTauxRemise2.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 3.
   */
  private void rafraichirTauxRemise3() {
    if (parametreClient != null && parametreClient.getTauxRemise3() != null) {
      tfTauxRemise3.setText(parametreClient.getTauxRemise3().getTexteSansSymbole());
    }
    else {
      tfTauxRemise3.setText("");
    }
    
    tfTauxRemise3.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void cbModeTTCActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      modele.modifierModeTTCClient(cbModeTTC.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbColonneTarifItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbColonneTarif.getSelectedIndex() >= 1 && cbColonneTarif.getSelectedIndex() <= 10) {
        getModele().modifierColonneTarifClient(cbColonneTarif.getSelectedIndex());
      }
      else {
        getModele().modifierColonneTarifClient(0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise1FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseClient(tfTauxRemise1.getText(), 1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise2FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseClient(tfTauxRemise2.getText(), 2);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise3FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseClient(tfTauxRemise3.getText(), 3);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbModeTTC = new SNLabelChamp();
    cbModeTTC = new SNCheckBoxReduit();
    lbColonneTarif = new SNLabelChamp();
    cbColonneTarif = new SNComboBoxReduit();
    lbTauxRemise = new SNLabelChamp();
    pnTauxRemise = new SNPanel();
    tfTauxRemise1 = new SNTexte();
    tfTauxRemise2 = new SNTexte();
    tfTauxRemise3 = new SNTexte();

    // ======== this ========
    setTitre("Client");
    setModeReduit(true);
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };

    // ---- lbModeTTC ----
    lbModeTTC.setText("Mode TTC");
    lbModeTTC.setModeReduit(true);
    lbModeTTC.setName("lbModeTTC");
    add(lbModeTTC,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbModeTTC ----
    cbModeTTC.setEnabled(false);
    cbModeTTC.setName("cbModeTTC");
    cbModeTTC.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        cbModeTTCActionPerformed(e);
      }
    });
    add(cbModeTTC,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbColonneTarif ----
    lbColonneTarif.setText("Colonne tarif");
    lbColonneTarif.setModeReduit(true);
    lbColonneTarif.setName("lbColonneTarif");
    add(lbColonneTarif,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbColonneTarif ----
    cbColonneTarif.setModel(new DefaultComboBoxModel(new String[] { "Aucune", "Colonne 1", "Colonne 2", "Colonne 3", "Colonne 4",
        "Colonne 5", "Colonne 6", "Colonne 7", "Colonne 8", "Colonne 9", "Colonne 10" }));
    cbColonneTarif.setSelectedIndex(-1);
    cbColonneTarif.setEnabled(false);
    cbColonneTarif.setName("cbColonneTarif");
    cbColonneTarif.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        cbColonneTarifItemStateChanged(e);
      }
    });
    add(cbColonneTarif,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbTauxRemise ----
    lbTauxRemise.setText("Taux remises (%)");
    lbTauxRemise.setModeReduit(true);
    lbTauxRemise.setName("lbTauxRemise");
    add(lbTauxRemise,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ======== pnTauxRemise ========
    {
      pnTauxRemise.setName("pnTauxRemise");
      pnTauxRemise.setLayout(new GridLayout(1, 0, 3, 0));

      // ---- tfTauxRemise1 ----
      tfTauxRemise1.setEnabled(false);
      tfTauxRemise1.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise1.setModeReduit(true);
      tfTauxRemise1.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise1.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise1.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise1.setName("tfTauxRemise1");
      tfTauxRemise1.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise1FocusLost(e);
        }
      });
      pnTauxRemise.add(tfTauxRemise1);

      // ---- tfTauxRemise2 ----
      tfTauxRemise2.setEnabled(false);
      tfTauxRemise2.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise2.setModeReduit(true);
      tfTauxRemise2.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise2.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise2.setName("tfTauxRemise2");
      tfTauxRemise2.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise2FocusLost(e);
        }
      });
      pnTauxRemise.add(tfTauxRemise2);

      // ---- tfTauxRemise3 ----
      tfTauxRemise3.setEnabled(false);
      tfTauxRemise3.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise3.setModeReduit(true);
      tfTauxRemise3.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise3.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise3.setName("tfTauxRemise3");
      tfTauxRemise3.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise3FocusLost(e);
        }
      });
      pnTauxRemise.add(tfTauxRemise3);
    }
    add(pnTauxRemise,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbModeTTC;
  private SNCheckBoxReduit cbModeTTC;
  private SNLabelChamp lbColonneTarif;
  private SNComboBoxReduit cbColonneTarif;
  private SNLabelChamp lbTauxRemise;
  private SNPanel pnTauxRemise;
  private SNTexte tfTauxRemise1;
  private SNTexte tfTauxRemise2;
  private SNTexte tfTauxRemise3;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
