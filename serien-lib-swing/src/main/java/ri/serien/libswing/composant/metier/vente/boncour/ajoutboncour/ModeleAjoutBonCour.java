/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.boncour.ajoutboncour;

import java.util.List;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereBonCour;
import ri.serien.libcommun.gescom.vente.boncour.EnumEtatBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeBonCour;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue qui ajoute un bon de cour à un bon ou une facture d'enlèvement
 *
 */
public class ModeleAjoutBonCour extends AbstractModeleDialogue {
  
  // variables
  private DocumentVente document = null;
  private int ligneInsertion = -1;
  private LigneVente ligneCommentaireBonCour = null;
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private List<IdCarnetBonCour> listeIdCarnetBonCourDisponible = null;
  private ListeBonCour listeBonCourDisponible = null;
  private Vendeur magasinierSelectionne = null;
  private BonCour bonCourSelectionne = null;
  private CarnetBonCour carnetBonCour = null;
  
  /**
   * Constructeur.
   */
  public ModeleAjoutBonCour(SessionBase pSession, DocumentVente pDocumentVente, int pLigneInsertion) {
    super(pSession);
    document = pDocumentVente;
    ligneInsertion = pLigneInsertion;
    idEtablissement = document.getId().getIdEtablissement();
    idMagasin = document.getIdMagasin();
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    
  }
  
  @Override
  public void chargerDonnees() {
    chargerListeBonCourDisponible();
  }
  
  /**
   * Quand on quitte avec validation, on marque le bon de cour choisi comme "utilisé" et on lui associe le document en cours.
   */
  @Override
  public void quitterAvecValidation() {
    if (bonCourSelectionne != null) {
      Article articleCommentaire = new Article(IdArticle.getInstanceAvecCreationId(idEtablissement));
      articleCommentaire.setLibelleComplet(retournerCommentaire());
      ligneCommentaireBonCour = document.ajouterLigneArticleCommentaire(getSession().getIdSession(), articleCommentaire, ligneInsertion);
      bonCourSelectionne.setEtatBonCour(EnumEtatBonCour.UTILISE);
      bonCourSelectionne.setIdDocumentGenere(document.getId());
      bonCourSelectionne.setNumeroLigne(ligneCommentaireBonCour.getId().getNumeroLigne());
      ManagerServiceDocumentVente.modifierBonCour(getIdSession(), bonCourSelectionne);
    }
    super.quitterAvecValidation();
  }
  
  // Méthodes publiques
  
  /**
   * Changement de magasinier par l'utilisateur
   */
  public void modifierMagasinier(Vendeur pMagasinier) {
    if (Constantes.equals(magasinierSelectionne, pMagasinier)) {
      return;
    }
    magasinierSelectionne = pMagasinier;
    chargerListeBonCourDisponible();
  }
  
  /**
   * Changement de numéro de bon de cour
   */
  public void modifierBonCour(BonCour pBonCourSelectionne) {
    if (Constantes.equals(bonCourSelectionne, pBonCourSelectionne)) {
      return;
    }
    bonCourSelectionne = pBonCourSelectionne;
    if (bonCourSelectionne != null) {
      carnetBonCour = ManagerServiceDocumentVente.chargerCarnetBonCour(getIdSession(), bonCourSelectionne.getId().getIdCarnetBonCour());
      IdVendeur idMagasinier = carnetBonCour.getIdMagasinier();
      
      ListeVendeur listeVendeur = new ListeVendeur();
      listeVendeur = listeVendeur.charger(getIdSession(), carnetBonCour.getId().getIdEtablissement());
      magasinierSelectionne = listeVendeur.get(idMagasinier);
    }
    else {
      magasinierSelectionne = null;
    }
    rafraichir();
  }
  
  /**
   * Renvoi le texte du commentaire à éditer
   */
  public String retournerCommentaire() {
    return "Bon de cour numéro " + bonCourSelectionne.getTexte() + "  -  délivré par " + magasinierSelectionne.getNomAvecCode();
  }
  
  // Méthodes privées
  
  /**
   * Charge la liste des bons de cour à afficher
   */
  private void chargerListeBonCourDisponible() {
    listeBonCourDisponible = new ListeBonCour();
    CritereBonCour critereBonCour = new CritereBonCour();
    critereBonCour.setIdEtablissement(idEtablissement);
    critereBonCour.setIdMagasin(idMagasin);
    if (magasinierSelectionne != null) {
      critereBonCour.setIdMagasinier(magasinierSelectionne.getId());
    }
    List<IdBonCour> listeIdBonCour = ManagerServiceDocumentVente.chargerListeIdBonCour(getIdSession(), critereBonCour);
    
    if (listeIdBonCour == null || listeIdBonCour.size() == 0) {
      return;
    }
    
    // On enlève les bons de cour annulés et utilisés
    listeBonCourDisponible = ManagerServiceDocumentVente.chargerListeBonCour(getIdSession(), listeIdBonCour);
    ListeBonCour listeBonCourFiltre = new ListeBonCour();
    for (BonCour bonCour : listeBonCourDisponible) {
      if (!bonCour.getEtatBonCour().equals(EnumEtatBonCour.ANNULE) && !bonCour.getEtatBonCour().equals(EnumEtatBonCour.UTILISE)) {
        listeBonCourFiltre.add(bonCour);
      }
    }
    listeBonCourDisponible = listeBonCourFiltre;
  }
  
  // Accesseurs
  
  /**
   * Identifiant de l'établissement en cours
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Liste des bons de cour trouvés
   */
  public ListeBonCour getListeBonCour() {
    return listeBonCourDisponible;
  }
  
  /**
   * Vendeur sélectionné
   */
  public Vendeur getMagasinierSelectionne() {
    return magasinierSelectionne;
  }
  
  /**
   * Bon de cour sélectionné
   */
  public BonCour getBonCourSelectionne() {
    return bonCourSelectionne;
  }
  
  public LigneVente getLigneCommentaireBonCour() {
    return ligneCommentaireBonCour;
  }
  
  // -- Accesseurs
  
}
