/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.stock.detailstock;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.Date;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.gescom.commun.stock.ListeStock;
import ri.serien.libcommun.gescom.commun.stock.Stock;
import ri.serien.libcommun.gescom.commun.stock.StockDetaille;
import ri.serien.libcommun.gescom.commun.stockattenducommande.ListeStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockattenducommande.StockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockmouvement.ListeStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.StockMouvement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.commun.snunite.SNUnite;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNNombreDecimal;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue qui affiche le détail du stock d'un article.
 */
public class DialogueDetailStock extends AbstractVueDialogue<ModeleDetailStock> {
  private static final String BOUTON_COPIER_PRESSE_PAPIER = "Copier dans le presse-papier";
  private static final String[] TITRE_LISTE_ATTENDU_COMMANDE =
      new String[] { "<html>Date<br>livraison</html>", "Entr\u00e9e", "Sortie", "Th\u00e9orique", "<html>Restant</html>",
          "<html>Avant<Br>rupture</html>", "Mag", "<html>Ligne<br>document</html>", "<html>Date<br>validation</html>", "Tiers" };
  private static final String[] TITRE_LISTE_MOUVEMENT = new String[] { "Date", "Ordre", "Mag", "<html>Ligne<br>document</html>", "Tiers",
      "Type", "Origine", "Entr\u00e9e", "Sortie", "Physique" };
  private static final String[] TITRE_LISTE_PAR_MAGASINS =
      new String[] { "Magasin", "Physique", "Command\u00e9", "Dont r\u00e9serv\u00e9", "Net", "Attendu", "Th\u00e9orique", "Disponible" };
  
  /**
   * Constructeur standard de la vue.
   */
  public DialogueDetailStock(ModeleDetailStock pModele) {
    super(pModele);
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Table liste des attendus/commandés
    spListeAttenduCommande.getViewport().setBackground(Color.WHITE);
    tbListeAttenduCommande.personnaliserAspect(TITRE_LISTE_ATTENDU_COMMANDE, new int[] { 80, 80, 80, 80, 80, 80, 40, 100, 80, 400 },
        new int[] { NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.DROITE,
            NRiTable.DROITE, NRiTable.DROITE },
        14);
    tbListeAttenduCommande.personnaliserAspectCellules(new ListeAttenduCommandeRenderer(getModele()));
    tbListeAttenduCommande.getTableHeader().setPreferredSize(new Dimension(40, 40));
    
    // Table liste des mouvements
    spListeMouvement.getViewport().setBackground(Color.WHITE);
    tbListeMouvement.personnaliserAspect(TITRE_LISTE_MOUVEMENT, new int[] { 80, 50, 40, 100, 250, 120, 120, 80, 80, 80 },
        new int[] { NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE,
            NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE },
        14);
    tbListeMouvement.getTableHeader().setPreferredSize(new Dimension(40, 40));
    
    // Table liste des stocks par magasin
    spListeMagasin.getViewport().setBackground(Color.WHITE);
    tbListeMagasin.personnaliserAspect(TITRE_LISTE_PAR_MAGASINS, new int[] { 300, 100, 100, 100, 100, 100, 100, 100 },
        new int[] { NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE,
            NRiTable.DROITE },
        14);
    tbListeMagasin.personnaliserAspectCellules(new ListeStockMagasinRenderer());
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_COPIER_PRESSE_PAPIER, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    // Rafraîchir les informations situées en entête
    rafraichirArticle();
    rafraichirEtablissement();
    rafraichirMagasin();
    
    // Rafraîchir l'onglet actif
    switch (getModele().getIndexOngletActif()) {
      case ModeleDetailStock.ONGLET_GENERAL:
        rafraichirStockPhysique();
        rafraichirStockCommande();
        rafraichirStockNet();
        rafraichirStockAttendu();
        rafraichirStockTheorique();
        rafraichirDate();
        rafraichirStockReserve();
        rafraichirStockCommandeLieAchat();
        rafraichirStockDisponible();
        rafraichirUnite();
        rafraichirStockAvantRupture();
        rafraichirDateRupturePossible();
        rafraichirDateFinRupture();
        rafraichirMessageErreur();
        break;
      
      case ModeleDetailStock.ONGLET_ATTENDUS_COMMANDES:
        rafraichirListeAttenduCommande();
        break;
      
      case ModeleDetailStock.ONGLET_MOUVEMENTS_STOCKS:
        rafraichirListeMouvement();
        break;
      
      case ModeleDetailStock.ONGLET_STOCK_PAR_MAGASIN:
        rafraichirListeMagasin();
        break;
      
      default:
        break;
    }
  }
  
  /**
   * Rafraîchir la liste déroulante des articles.
   */
  private void rafraichirArticle() {
    snArticle.setSession(getModele().getSession());
    snArticle.setIdEtablissement(getModele().getIdEtablissement());
    snArticle.charger(false);
    snArticle.setSelection(getModele().getArticle());
  }
  
  /**
   * Rafraîchir la liste déroulante des établissement.
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getModele().getSession());
    snEtablissement.charger(false);
    snEtablissement.setIdSelection(getModele().getIdEtablissement());
  }
  
  /**
   * Rafraîchir la liste déroulante des magasins.
   */
  private void rafraichirMagasin() {
    snMagasin.setSession(getModele().getSession());
    snMagasin.setIdEtablissement(getModele().getIdEtablissement());
    snMagasin.charger(false);
    snMagasin.setTousAutorise(true);
    snMagasin.setIdSelection(getModele().getIdMagasin());
  }
  
  /**
   * Rafraîchir l'unité.
   */
  private void rafraichirUnite() {
    snUnite.setSession(getModele().getSession());
    snUnite.setIdEtablissement(getModele().getIdEtablissement());
    snUnite.charger(false);
    if (getModele().getArticle() != null) {
      snUnite.setIdSelection(getModele().getArticle().getIdUS());
    }
    else {
      snUnite.setIdSelection(null);
    }
  }
  
  /**
   * Rafraîchir le stock physique.
   */
  private void rafraichirStockPhysique() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      tfStockPhysique.setText(Constantes.formater(stockDetaille.getStockPhysique(), false));
    }
    else {
      tfStockPhysique.setText("");
    }
    
    if (stockDetaille != null && !stockDetaille.isStockPhysiqueCorrect()) {
      lbStockPhysique.setImportanceMessage(EnumImportanceMessage.HAUT);
    }
    else {
      lbStockPhysique.setImportanceMessage(EnumImportanceMessage.NORMAL);
    }
  }
  
  /**
   * Rafraîchir le stock commandé.
   */
  private void rafraichirStockCommande() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      tfStockCommande.setText(Constantes.formater(stockDetaille.getStockCommande(), false));
    }
    else {
      tfStockCommande.setText("");
    }
    
    if (stockDetaille != null && !stockDetaille.isStockCommandeCorrect()) {
      lbStockCommande.setImportanceMessage(EnumImportanceMessage.HAUT);
    }
    else {
      lbStockCommande.setImportanceMessage(EnumImportanceMessage.NORMAL);
    }
  }
  
  /**
   * Rafraîchir le stock net.
   */
  private void rafraichirStockNet() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      tfStockNet.setText(Constantes.formater(stockDetaille.getStockNet(), false));
    }
    else {
      tfStockNet.setText("");
    }
  }
  
  /**
   * Rafraîchir le stock attendu.
   */
  private void rafraichirStockAttendu() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      tfStockAttendu.setText(Constantes.formater(stockDetaille.getStockAttendu(), false));
    }
    else {
      tfStockAttendu.setText("");
    }
    
    if (stockDetaille != null && !stockDetaille.isStockAttenduCorrect()) {
      lbStockAttendu.setImportanceMessage(EnumImportanceMessage.HAUT);
    }
    else {
      lbStockAttendu.setImportanceMessage(EnumImportanceMessage.NORMAL);
    }
  }
  
  /**
   * Rafraîchir le stock théorique.
   */
  private void rafraichirStockTheorique() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      tfStockTheorique.setText(Constantes.formater(stockDetaille.getStockTheorique(), false));
    }
    else {
      tfStockTheorique.setText("");
    }
  }
  
  /**
   * Rafraîchir la date.
   */
  private void rafraichirDate() {
    snDate.setDate(new Date());
  }
  
  /**
   * Rafraîchir le stock réservé.
   */
  private void rafraichirStockReserve() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      tfStockReserve.setText(Constantes.formater(stockDetaille.getStockReserve(), false));
    }
    else {
      tfStockReserve.setText("");
    }
    
    if (stockDetaille != null && !stockDetaille.isStockReserveCorrect()) {
      lbStockReserve.setImportanceMessage(EnumImportanceMessage.HAUT);
    }
    else {
      lbStockReserve.setImportanceMessage(EnumImportanceMessage.NORMAL);
    }
  }
  
  /**
   * Rafraîchir le stock commandé lié sur achats.
   */
  private void rafraichirStockCommandeLieAchat() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      tfStockCommandeLieAchat.setText(Constantes.formater(stockDetaille.getStockCommandeLieSurAchat(), false));
    }
    else {
      tfStockCommandeLieAchat.setText("");
    }
  }
  
  /**
   * Rafraîchir le stock disponible.
   */
  private void rafraichirStockDisponible() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      tfStockDisponible.setText(Constantes.formater(stockDetaille.getStockDisponible(), false));
    }
    else {
      tfStockDisponible.setText("");
    }
  }
  
  /**
   * Rafraîchir le stock avant rupture.
   */
  private void rafraichirStockAvantRupture() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      tfAvantRupture.setText(Constantes.formater(stockDetaille.getStockAvantRupture(), false));
    }
    else {
      tfAvantRupture.setText("");
    }
  }
  
  /**
   * Rafraîchir la date de rupture possible.
   */
  private void rafraichirDateRupturePossible() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      snDateRupturePossible.setDate(stockDetaille.getDateRupturePossible());
    }
    else {
      snDateRupturePossible.setDate(null);
    }
  }
  
  /**
   * Rafraîchir la date de fin de rupture.
   */
  private void rafraichirDateFinRupture() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null) {
      snDateFinRupture.setDate(stockDetaille.getDateFinRupture());
    }
    else {
      snDateFinRupture.setDate(null);
    }
  }
  
  /**
   * Rafraîchir le message d'erreur.
   */
  private void rafraichirMessageErreur() {
    StockDetaille stockDetaille = getModele().getStockDetaille();
    if (stockDetaille != null && stockDetaille.getMessageErreur() != null) {
      lbMessageErreur.setMessage(stockDetaille.getMessageErreur());
      lbMessageErreur.setVisible(true);
    }
    else {
      lbMessageErreur.setMessage(null);
      lbMessageErreur.setVisible(false);
    }
  }
  
  /**
   * Rafraîchir la liste des attendus et commandés.
   */
  private void rafraichirListeAttenduCommande() {
    // Récupérer la liste des attendus et commandésq
    ListeStockAttenduCommande listeAttenduCommande = null;
    if (getModele().getStockDetaille() != null) {
      listeAttenduCommande = getModele().getStockDetaille().getListeStockAttenduCommande();
    }
    
    // Générer les données du tableau
    String[][] donnees = null;
    if (listeAttenduCommande != null) {
      donnees = new String[listeAttenduCommande.size()][TITRE_LISTE_ATTENDU_COMMANDE.length];
      for (int i = 0; i < listeAttenduCommande.size(); i++) {
        StockAttenduCommande stock = listeAttenduCommande.get(i);
        String[] ligne = donnees[i];
        
        // Date de livraison prévue
        ligne[0] = Constantes.convertirDateEnTexte(stock.getDateReference());
        
        // Entrée
        if (stock.getQuantiteEntree() != null && stock.getQuantiteEntree().compareTo(BigDecimal.ZERO) != 0) {
          ligne[1] = Constantes.formater(stock.getQuantiteEntree(), false);
        }
        else {
          ligne[1] = "";
        }
        
        // Sortie
        if (stock.getQuantiteSortie() != null && stock.getQuantiteSortie().compareTo(BigDecimal.ZERO) != 0) {
          ligne[2] = Constantes.formater(stock.getQuantiteSortie().negate(), false);
        }
        else {
          ligne[2] = "";
        }
        
        // Théorique
        if (stock.getStockTheoriqueADate() != null) {
          ligne[3] = Constantes.formater(stock.getStockTheoriqueADate(), false);
        }
        else {
          ligne[3] = "";
        }
        
        // Restant avant attendu
        if (stock.getStockRestantAvantAttendu() != null) {
          ligne[4] = Constantes.formater(stock.getStockRestantAvantAttendu(), false);
        }
        else {
          ligne[4] = "";
        }
        
        // Avant rupture
        if (stock.getStockAvantRupture() != null) {
          ligne[5] = Constantes.formater(stock.getStockAvantRupture(), false);
        }
        else {
          ligne[5] = "";
        }
        
        // Magasin
        if (stock.getId().getIdMagasin() != null) {
          ligne[6] = stock.getId().getIdMagasin().getCode();
        }
        else {
          ligne[6] = "";
        }
        
        // Ligne document
        if (stock.getId().getNumeroDocument() != null && stock.getId().getSuffixeDocument() != null
            && stock.getId().getNumeroLigne() != null) {
          ligne[7] = Constantes.convertirIntegerEnTexte(stock.getId().getNumeroDocument(), 0) + AbstractId.SEPARATEUR_ID
              + Constantes.convertirIntegerEnTexte(stock.getId().getSuffixeDocument(), 0) + AbstractId.SEPARATEUR_ID
              + Constantes.convertirIntegerEnTexte(stock.getId().getNumeroLigne(), 0);
        }
        else {
          ligne[7] = "";
        }
        
        // Date de validation
        ligne[8] = Constantes.convertirDateEnTexte(stock.getDateValidation());
        
        // Tiers
        ligne[9] = stock.getLibelleTiers();
      }
    }
    
    // Mettre à jour le tableau
    tbListeAttenduCommande.mettreAJourDonnees(donnees);
  }
  
  /**
   * Rafraichir la liste des mouvements de stock
   */
  private void rafraichirListeMouvement() {
    // Récupérer la liste des stocks par magasin
    ListeStockMouvement listeStockMouvement = null;
    if (getModele().getStockDetaille() != null) {
      listeStockMouvement = getModele().getStockDetaille().getListeStockMouvement();
    }
    
    // Générer les données du tableau
    String[][] donnees = null;
    if (listeStockMouvement != null) {
      // Créer le tableau résultat avec auntant de lignes que de magasins plus une ligne pour le total de l'établissement
      donnees = new String[listeStockMouvement.size()][TITRE_LISTE_MOUVEMENT.length];
      
      // Ajouter une ligne par magasin
      for (int i = 0; i < listeStockMouvement.size(); i++) {
        StockMouvement mouvement = listeStockMouvement.get(i);
        String[] ligne = donnees[i];
        
        // Date
        ligne[0] = Constantes.convertirDateEnTexte(mouvement.getId().getDate());
        
        // Ordre
        ligne[1] = Constantes.convertirIntegerEnTexte(mouvement.getId().getNumeroOrdre(), 0);
        
        // Magasin
        ligne[2] = mouvement.getId().getCodeMagasin();
        
        // Ligne
        if (mouvement.getTexteLigneDocument() != null) {
          ligne[3] = mouvement.getTexteLigneDocument();
        }
        else {
          ligne[3] = "";
        }
        
        // Tiers
        if (mouvement.getLibelleTiers() != null) {
          ligne[4] = mouvement.getLibelleTiers();
        }
        else {
          ligne[4] = "";
        }
        
        // Type
        if (mouvement.getType() != null) {
          ligne[5] = mouvement.getType().getLibelle();
        }
        else {
          ligne[5] = "";
        }
        
        // Origine
        if (mouvement.getOrigine() != null) {
          ligne[6] = mouvement.getOrigine().getLibelle();
        }
        else {
          ligne[6] = "";
        }
        
        // Entrée
        if (mouvement.getQuantiteEntree() != null) {
          ligne[7] = Constantes.formater(mouvement.getQuantiteEntree(), false);
        }
        else {
          ligne[7] = "";
        }
        
        // Sortie
        if (mouvement.getQuantiteSortie() != null) {
          ligne[8] = Constantes.formater(mouvement.getQuantiteSortie(), false);
        }
        else {
          ligne[8] = "";
        }
        
        // Physique
        if (mouvement.getStockPhysique() != null) {
          ligne[9] = Constantes.formater(mouvement.getStockPhysique(), false);
        }
        else {
          ligne[9] = "";
        }
      }
    }
    
    // Mettre à jour le tableau
    tbListeMouvement.mettreAJourDonnees(donnees);
  }
  
  /**
   * Rafraîchir la liste des magasins.
   */
  private void rafraichirListeMagasin() {
    // Récupérer la liste des stocks par magasin
    ListeStock listeStockMagasin = null;
    if (getModele().getStockDetaille() != null) {
      listeStockMagasin = getModele().getStockDetaille().getListeStockMagasin();
    }
    
    // Générer les données du tableau
    String[][] donnees = null;
    if (listeStockMagasin != null) {
      // Créer le tableau résultat avec auntant de lignes que de magasins plus une ligne pour le total de l'établissement
      donnees = new String[listeStockMagasin.size() + 1][TITRE_LISTE_PAR_MAGASINS.length];
      
      // Ajouter une ligne par magasin
      for (int i = 0; i < listeStockMagasin.size(); i++) {
        Stock stock = listeStockMagasin.get(i);
        String[] ligne = donnees[i];
        ajouterLigneStock(stock, ligne);
      }
      
      // Ajouter le stock de l'établissement sur la dernière ligne
      Stock stock = getModele().getStockDetaille().getStockEtablissement();
      String[] ligne = donnees[donnees.length - 1];
      ajouterLigneStock(stock, ligne);
      
      // Mettre à jour le tableau. Mise à jour seulement si les données ont changées pour conserver le stock par magasin même après choix
      // d'un stock à 0 (après modification de magasin ou établissement par exemple).
      tbListeMagasin.mettreAJourDonnees(donnees);
    }
  }
  
  /**
   * Renseigner une ligne de la liste des stocks par magasin.
   *
   * @param pStock Stock à ajouter.
   * @param pLigne Ligne à compléter.
   */
  private void ajouterLigneStock(Stock pStock, String[] pLigne) {
    // Libellé
    if (pStock.getId().getIdMagasin() != null) {
      pLigne[0] = getModele().getNomMagasin(pStock.getId().getIdMagasin());
    }
    else {
      pLigne[0] = "Total établissement";
    }
    
    // Stock physique
    if (pStock.getStockPhysique() != null && pStock.getStockPhysique().compareTo(BigDecimal.ZERO) != 0) {
      pLigne[1] = Constantes.formater(pStock.getStockPhysique(), false);
    }
    else {
      pLigne[1] = "";
    }
    
    // Stock commandé
    if (pStock.getStockCommande() != null && pStock.getStockCommande().compareTo(BigDecimal.ZERO) != 0) {
      pLigne[2] = Constantes.formater(pStock.getStockCommande(), false);
    }
    else {
      pLigne[2] = "";
    }
    
    // Stock réservé
    if (pStock.getStockReserve() != null && pStock.getStockReserve().compareTo(BigDecimal.ZERO) != 0) {
      pLigne[3] = Constantes.formater(pStock.getStockReserve(), false);
    }
    else {
      pLigne[3] = "";
    }
    
    // Stock net
    if (pStock.getStockNet() != null && pStock.getStockNet().compareTo(BigDecimal.ZERO) != 0) {
      pLigne[4] = Constantes.formater(pStock.getStockNet(), false);
    }
    else {
      pLigne[4] = "";
    }
    
    // Stock attendu
    if (pStock.getStockAttendu() != null && pStock.getStockAttendu().compareTo(BigDecimal.ZERO) != 0) {
      pLigne[5] = Constantes.formater(pStock.getStockAttendu(), false);
    }
    else {
      pLigne[5] = "";
    }
    
    // Stock théorique
    if (pStock.getStockTheorique() != null && pStock.getStockTheorique().compareTo(BigDecimal.ZERO) != 0) {
      pLigne[6] = Constantes.formater(pStock.getStockTheorique(), false);
    }
    else {
      pLigne[6] = "";
    }
    
    // Stock disponible
    if (pStock.getStockDisponible() != null && pStock.getStockDisponible().compareTo(BigDecimal.ZERO) != 0) {
      pLigne[7] = Constantes.formater(pStock.getStockDisponible(), false);
    }
    else {
      pLigne[7] = "";
    }
  }
  
  /**
   * Traiter les clics sur les boutons des barres de boutons.
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(BOUTON_COPIER_PRESSE_PAPIER)) {
        getModele().copierPressePapier();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter les clics sur les onglets
   * @param e Evènement.
   */
  private void tpStockMouseClicked(MouseEvent e) {
    try {
      getModele().modifierOngletActif(tbpStock.getSelectedIndex());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   *
   * Modifier l'établissement en cours.
   *
   * @param SNComposantEvent e
   */
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (!isDonneesChargees()) {
        return;
      }
      getModele().modifierEtablissement(snEtablissement.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   *
   * Modifier le magasin en cours.
   *
   * @param SNComposantEvent
   */
  private void snMagasinValueChanged(SNComposantEvent e) {
    try {
      if (!isDonneesChargees()) {
        return;
      }
      getModele().modifierMagasin(snMagasin.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    snPanelEntete = new SNPanel();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    tbpStock = new JTabbedPane();
    pnlPrincipal = new SNPanelContenu();
    pnlPrincipalGauche = new SNPanel();
    pnlQuantite = new SNPanelTitre();
    lbStockPhysique = new SNLabelChamp();
    tfStockPhysique = new SNNombreDecimal();
    lbDate = new SNLabelChamp();
    snDate = new SNDate();
    lbStockCommande = new SNLabelChamp();
    tfStockCommande = new SNNombreDecimal();
    lbStockReserve = new SNLabelChamp();
    tfStockReserve = new SNNombreDecimal();
    lbStockNet = new SNLabelChamp();
    tfStockNet = new SNNombreDecimal();
    lbStockAttendu = new SNLabelChamp();
    tfStockAttendu = new SNNombreDecimal();
    lbStockCommandeLieAchat = new SNLabelChamp();
    tfStockCommandeLieAchat = new SNNombreDecimal();
    lbStockTheorique = new SNLabelChamp();
    tfStockTheorique = new SNNombreDecimal();
    lbStockDisponible = new SNLabelChamp();
    tfStockDisponible = new SNNombreDecimal();
    pnlMiniEstime = new SNPanelTitre();
    lbStockAvantRupture = new SNLabelChamp();
    tfAvantRupture = new SNNombreDecimal();
    lbDateRupturePossible = new SNLabelChamp();
    snDateRupturePossible = new SNDate();
    lbDateFinRupture = new SNLabelChamp();
    snDateFinRupture = new SNDate();
    pnlPrincipalDroite = new SNPanel();
    pnlUnite = new SNPanelTitre();
    sNLabelChamp11 = new SNLabelChamp();
    snUnite = new SNUnite();
    lbMessageErreur = new SNMessage();
    pnlAttenduCommande = new SNPanelContenu();
    spListeAttenduCommande = new JScrollPane();
    tbListeAttenduCommande = new NRiTable();
    pnlMouvementStock = new SNPanelContenu();
    spListeMouvement = new JScrollPane();
    tbListeMouvement = new NRiTable();
    pnlStockParMagasin = new SNPanelContenu();
    spListeMagasin = new JScrollPane();
    tbListeMagasin = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("D\u00e9tail du stock article");
    setMinimumSize(new Dimension(400, 365));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ======== snPanelEntete ========
        {
          snPanelEntete.setName("snPanelEntete");
          snPanelEntete.setLayout(new GridBagLayout());
          ((GridBagLayout) snPanelEntete.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) snPanelEntete.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) snPanelEntete.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) snPanelEntete.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbArticle ----
          lbArticle.setText("Article");
          lbArticle.setName("lbArticle");
          snPanelEntete.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticle ----
          snArticle.setEnabled(false);
          snArticle.setName("snArticle");
          snPanelEntete.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          snPanelEntete.add(lbEtablissement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(e -> snEtablissementValueChanged(e));
          snPanelEntete.add(snEtablissement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          snPanelEntete.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setName("snMagasin");
          snMagasin.addSNComposantListener(e -> snMagasinValueChanged(e));
          snPanelEntete.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(snPanelEntete, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== tbpStock ========
        {
          tbpStock.setFont(new Font("sansserif", Font.PLAIN, 14));
          tbpStock.setBorder(null);
          tbpStock.setName("tbpStock");
          tbpStock.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tpStockMouseClicked(e);
            }
          });
          
          // ======== pnlPrincipal ========
          {
            pnlPrincipal.setName("pnlPrincipal");
            pnlPrincipal.setLayout(new GridLayout(1, 2));
            
            // ======== pnlPrincipalGauche ========
            {
              pnlPrincipalGauche.setName("pnlPrincipalGauche");
              pnlPrincipalGauche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ======== pnlQuantite ========
              {
                pnlQuantite.setTitre("Stock");
                pnlQuantite.setName("pnlQuantite");
                pnlQuantite.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlQuantite.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlQuantite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlQuantite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlQuantite.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0, 1.0, 1.0, 1.0E-4 };
                
                // ---- lbStockPhysique ----
                lbStockPhysique.setText("Physique");
                lbStockPhysique.setName("lbStockPhysique");
                pnlQuantite.add(lbStockPhysique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfStockPhysique ----
                tfStockPhysique.setEditable(false);
                tfStockPhysique.setEnabled(false);
                tfStockPhysique.setToolTipText("Quantit\u00e9 d\u2019articles pr\u00e9sents physiquement");
                tfStockPhysique.setName("tfStockPhysique");
                pnlQuantite.add(tfStockPhysique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbDate ----
                lbDate.setText("Date");
                lbDate.setName("lbDate");
                pnlQuantite.add(lbDate, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- snDate ----
                snDate.setEnabled(false);
                snDate.setName("snDate");
                pnlQuantite.add(snDate, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbStockCommande ----
                lbStockCommande.setText("Command\u00e9");
                lbStockCommande.setName("lbStockCommande");
                pnlQuantite.add(lbStockCommande, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfStockCommande ----
                tfStockCommande.setEditable(false);
                tfStockCommande.setEnabled(false);
                tfStockCommande.setToolTipText("Quantit\u00e9 d\u2019articles command\u00e9s par des clients et non livr\u00e9s");
                tfStockCommande.setName("tfStockCommande");
                pnlQuantite.add(tfStockCommande, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbStockReserve ----
                lbStockReserve.setText("Dont r\u00e9serv\u00e9");
                lbStockReserve.setName("lbStockReserve");
                pnlQuantite.add(lbStockReserve, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfStockReserve ----
                tfStockReserve.setEnabled(false);
                tfStockReserve.setToolTipText("Sous-ensemble des articles command\u00e9s contenant les articles r\u00e9serv\u00e9s");
                tfStockReserve.setName("tfStockReserve");
                pnlQuantite.add(tfStockReserve, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbStockNet ----
                lbStockNet.setText("Net");
                lbStockNet.setImportanceMessage(EnumImportanceMessage.MOYEN);
                lbStockNet.setName("lbStockNet");
                pnlQuantite.add(lbStockNet, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfStockNet ----
                tfStockNet.setEditable(false);
                tfStockNet.setEnabled(false);
                tfStockNet.setFont(tfStockNet.getFont().deriveFont(tfStockNet.getFont().getStyle() | Font.BOLD));
                tfStockNet.setToolTipText("Stock physique - Stock command\u00e9");
                tfStockNet.setName("tfStockNet");
                pnlQuantite.add(tfStockNet, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbStockAttendu ----
                lbStockAttendu.setText("Attendu");
                lbStockAttendu.setName("lbStockAttendu");
                pnlQuantite.add(lbStockAttendu, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfStockAttendu ----
                tfStockAttendu.setEnabled(false);
                tfStockAttendu
                    .setToolTipText("Quantit\u00e9 d\u2019articles command\u00e9s \u00e0 des fournisseurs et non r\u00e9ceptionn\u00e9s");
                tfStockAttendu.setName("tfStockAttendu");
                pnlQuantite.add(tfStockAttendu, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbStockCommandeLieAchat ----
                lbStockCommandeLieAchat.setText("Command\u00e9 li\u00e9 achats");
                lbStockCommandeLieAchat.setName("lbStockCommandeLieAchat");
                pnlQuantite.add(lbStockCommandeLieAchat, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfStockCommandeLieAchat ----
                tfStockCommandeLieAchat.setEnabled(false);
                tfStockCommandeLieAchat.setToolTipText(
                    "Sous-ensemble des articles command\u00e9s contenant les articles li\u00e9s \u00e0 des commandes d\u2019achats");
                tfStockCommandeLieAchat.setName("tfStockCommandeLieAchat");
                pnlQuantite.add(tfStockCommandeLieAchat, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbStockTheorique ----
                lbStockTheorique.setText("Th\u00e9orique");
                lbStockTheorique.setImportanceMessage(EnumImportanceMessage.MOYEN);
                lbStockTheorique.setName("lbStockTheorique");
                pnlQuantite.add(lbStockTheorique, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfStockTheorique ----
                tfStockTheorique.setEnabled(false);
                tfStockTheorique.setFont(tfStockTheorique.getFont().deriveFont(tfStockTheorique.getFont().getStyle() | Font.BOLD));
                tfStockTheorique.setToolTipText("Stock physique \u2013 Stock command\u00e9 + Stock Attendu");
                tfStockTheorique.setName("tfStockTheorique");
                pnlQuantite.add(tfStockTheorique, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbStockDisponible ----
                lbStockDisponible.setText("Disponible");
                lbStockDisponible.setImportanceMessage(EnumImportanceMessage.MOYEN);
                lbStockDisponible.setName("lbStockDisponible");
                pnlQuantite.add(lbStockDisponible, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfStockDisponible ----
                tfStockDisponible.setEnabled(false);
                tfStockDisponible.setFont(tfStockDisponible.getFont().deriveFont(tfStockDisponible.getFont().getStyle() | Font.BOLD));
                tfStockDisponible.setToolTipText("Stock physique \u2013 (Stock command\u00e9 - Command\u00e9 li\u00e9 sur achats)");
                tfStockDisponible.setName("tfStockDisponible");
                pnlQuantite.add(tfStockDisponible, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPrincipalGauche.add(pnlQuantite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== pnlMiniEstime ========
              {
                pnlMiniEstime.setTitre("Stock avant rupture");
                pnlMiniEstime.setName("pnlMiniEstime");
                pnlMiniEstime.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlMiniEstime.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlMiniEstime.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlMiniEstime.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlMiniEstime.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- lbStockAvantRupture ----
                lbStockAvantRupture.setText("Avant rupture");
                lbStockAvantRupture.setName("lbStockAvantRupture");
                pnlMiniEstime.add(lbStockAvantRupture, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfAvantRupture ----
                tfAvantRupture.setEnabled(false);
                tfAvantRupture.setToolTipText(
                    "Quantit\u00e9 correspondant au minimum estim\u00e9 d\u2019articles en stock en fonction des attendus et command\u00e9s \u00e0 venir");
                tfAvantRupture.setName("tfAvantRupture");
                pnlMiniEstime.add(tfAvantRupture, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbDateRupturePossible ----
                lbDateRupturePossible.setText("Date rupture possible");
                lbDateRupturePossible.setName("lbDateRupturePossible");
                pnlMiniEstime.add(lbDateRupturePossible, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- snDateRupturePossible ----
                snDateRupturePossible.setEnabled(false);
                snDateRupturePossible.setName("snDateRupturePossible");
                pnlMiniEstime.add(snDateRupturePossible, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbDateFinRupture ----
                lbDateFinRupture.setText("Date fin de rupture");
                lbDateFinRupture.setName("lbDateFinRupture");
                pnlMiniEstime.add(lbDateFinRupture, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- snDateFinRupture ----
                snDateFinRupture.setEnabled(false);
                snDateFinRupture.setName("snDateFinRupture");
                pnlMiniEstime.add(snDateFinRupture, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPrincipalGauche.add(pnlMiniEstime, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlPrincipal.add(pnlPrincipalGauche);
            
            // ======== pnlPrincipalDroite ========
            {
              pnlPrincipalDroite.setName("pnlPrincipalDroite");
              pnlPrincipalDroite.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              
              // ======== pnlUnite ========
              {
                pnlUnite.setTitre("Unit\u00e9s");
                pnlUnite.setName("pnlUnite");
                pnlUnite.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlUnite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlUnite.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlUnite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlUnite.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
                
                // ---- sNLabelChamp11 ----
                sNLabelChamp11.setText("Unit\u00e9 de stock");
                sNLabelChamp11.setName("sNLabelChamp11");
                pnlUnite.add(sNLabelChamp11, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- snUnite ----
                snUnite.setEnabled(false);
                snUnite.setName("snUnite");
                pnlUnite.add(snUnite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPrincipalDroite.add(pnlUnite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMessageErreur ----
              lbMessageErreur.setText("Message");
              lbMessageErreur.setName("lbMessageErreur");
              pnlPrincipalDroite.add(lbMessageErreur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlPrincipal.add(pnlPrincipalDroite);
          }
          tbpStock.addTab("Principal", pnlPrincipal);
          
          // ======== pnlAttenduCommande ========
          {
            pnlAttenduCommande.setName("pnlAttenduCommande");
            pnlAttenduCommande.setLayout(new BorderLayout());
            
            // ======== spListeAttenduCommande ========
            {
              spListeAttenduCommande.setName("spListeAttenduCommande");
              
              // ---- tbListeAttenduCommande ----
              tbListeAttenduCommande
                  .setModel(new DefaultTableModel(new Object[][] { { null, null, null, null, null, null, null, null, null, null }, },
                      new String[] { "Date livraison", "Entr\u00e9e", "Sortie", "Th\u00e9orique", "Restant", "Avant rupture", "Magasin",
                          "Ligne document", "Date validation", "Tiers" }) {
                    Class<?>[] columnTypes = new Class<?>[] { String.class, Integer.class, Integer.class, Integer.class, Object.class,
                        Object.class, Object.class, Integer.class, Integer.class, Integer.class };
                    
                    @Override
                    public Class<?> getColumnClass(int columnIndex) {
                      return columnTypes[columnIndex];
                    }
                  });
              {
                TableColumnModel cm = tbListeAttenduCommande.getColumnModel();
                cm.getColumn(0).setMinWidth(80);
                cm.getColumn(0).setPreferredWidth(80);
                cm.getColumn(1).setMinWidth(80);
                cm.getColumn(1).setPreferredWidth(80);
                cm.getColumn(2).setMinWidth(80);
                cm.getColumn(2).setPreferredWidth(80);
                cm.getColumn(3).setMinWidth(80);
                cm.getColumn(3).setPreferredWidth(80);
                cm.getColumn(4).setMinWidth(80);
                cm.getColumn(4).setPreferredWidth(80);
                cm.getColumn(5).setMinWidth(80);
                cm.getColumn(5).setPreferredWidth(80);
                cm.getColumn(6).setMinWidth(50);
                cm.getColumn(6).setPreferredWidth(50);
                cm.getColumn(7).setMinWidth(100);
                cm.getColumn(7).setPreferredWidth(100);
                cm.getColumn(8).setMinWidth(80);
                cm.getColumn(8).setPreferredWidth(80);
                cm.getColumn(9).setMinWidth(400);
                cm.getColumn(9).setPreferredWidth(400);
              }
              tbListeAttenduCommande.setRowSelectionAllowed(false);
              tbListeAttenduCommande.setEnabled(false);
              tbListeAttenduCommande.setName("tbListeAttenduCommande");
              spListeAttenduCommande.setViewportView(tbListeAttenduCommande);
            }
            pnlAttenduCommande.add(spListeAttenduCommande, BorderLayout.CENTER);
          }
          tbpStock.addTab("Attendus et command\u00e9s", pnlAttenduCommande);
          
          // ======== pnlMouvementStock ========
          {
            pnlMouvementStock.setName("pnlMouvementStock");
            pnlMouvementStock.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlMouvementStock.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlMouvementStock.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlMouvementStock.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlMouvementStock.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== spListeMouvement ========
            {
              spListeMouvement.setName("spListeMouvement");
              
              // ---- tbListeMouvement ----
              tbListeMouvement.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Magasin", "Physique", "Command\u00e9",
                  "Dont r\u00e9serv\u00e9", "Net", "Attendu", "Th\u00e9orique", "Disponible" }) {
                Class<?>[] columnTypes = new Class<?>[] { String.class, Integer.class, Integer.class, Integer.class, Integer.class,
                    Integer.class, Integer.class, Integer.class };
                
                @Override
                public Class<?> getColumnClass(int columnIndex) {
                  return columnTypes[columnIndex];
                }
              });
              {
                TableColumnModel cm = tbListeMouvement.getColumnModel();
                cm.getColumn(0).setMinWidth(300);
                cm.getColumn(0).setPreferredWidth(300);
              }
              tbListeMouvement.setName("tbListeMouvement");
              spListeMouvement.setViewportView(tbListeMouvement);
            }
            pnlMouvementStock.add(spListeMouvement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          tbpStock.addTab("Mouvements de stock", pnlMouvementStock);
          
          // ======== pnlStockParMagasin ========
          {
            pnlStockParMagasin.setName("pnlStockParMagasin");
            pnlStockParMagasin.setLayout(new BorderLayout());
            
            // ======== spListeMagasin ========
            {
              spListeMagasin.setName("spListeMagasin");
              
              // ---- tbListeMagasin ----
              tbListeMagasin.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Magasin", "Physique", "Command\u00e9",
                  "Dont r\u00e9serv\u00e9", "Net", "Attendu", "Th\u00e9orique", "Disponible" }) {
                Class<?>[] columnTypes = new Class<?>[] { String.class, Integer.class, Integer.class, Integer.class, Integer.class,
                    Integer.class, Integer.class, Integer.class };
                
                @Override
                public Class<?> getColumnClass(int columnIndex) {
                  return columnTypes[columnIndex];
                }
              });
              {
                TableColumnModel cm = tbListeMagasin.getColumnModel();
                cm.getColumn(0).setMinWidth(300);
                cm.getColumn(0).setPreferredWidth(300);
              }
              tbListeMagasin.setName("tbListeMagasin");
              spListeMagasin.setViewportView(tbListeMagasin);
            }
            pnlStockParMagasin.add(spListeMagasin, BorderLayout.CENTER);
          }
          tbpStock.addTab("Stock par magasin", pnlStockParMagasin);
        }
        pnlContenu.add(tbpStock, new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.PAGE_END);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNPanelContenu pnlContenu;
  private SNPanel snPanelEntete;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private JTabbedPane tbpStock;
  private SNPanelContenu pnlPrincipal;
  private SNPanel pnlPrincipalGauche;
  private SNPanelTitre pnlQuantite;
  private SNLabelChamp lbStockPhysique;
  private SNNombreDecimal tfStockPhysique;
  private SNLabelChamp lbDate;
  private SNDate snDate;
  private SNLabelChamp lbStockCommande;
  private SNNombreDecimal tfStockCommande;
  private SNLabelChamp lbStockReserve;
  private SNNombreDecimal tfStockReserve;
  private SNLabelChamp lbStockNet;
  private SNNombreDecimal tfStockNet;
  private SNLabelChamp lbStockAttendu;
  private SNNombreDecimal tfStockAttendu;
  private SNLabelChamp lbStockCommandeLieAchat;
  private SNNombreDecimal tfStockCommandeLieAchat;
  private SNLabelChamp lbStockTheorique;
  private SNNombreDecimal tfStockTheorique;
  private SNLabelChamp lbStockDisponible;
  private SNNombreDecimal tfStockDisponible;
  private SNPanelTitre pnlMiniEstime;
  private SNLabelChamp lbStockAvantRupture;
  private SNNombreDecimal tfAvantRupture;
  private SNLabelChamp lbDateRupturePossible;
  private SNDate snDateRupturePossible;
  private SNLabelChamp lbDateFinRupture;
  private SNDate snDateFinRupture;
  private SNPanel pnlPrincipalDroite;
  private SNPanelTitre pnlUnite;
  private SNLabelChamp sNLabelChamp11;
  private SNUnite snUnite;
  private SNMessage lbMessageErreur;
  private SNPanelContenu pnlAttenduCommande;
  private JScrollPane spListeAttenduCommande;
  private NRiTable tbListeAttenduCommande;
  private SNPanelContenu pnlMouvementStock;
  private JScrollPane spListeMouvement;
  private NRiTable tbListeMouvement;
  private SNPanelContenu pnlStockParMagasin;
  private JScrollPane spListeMagasin;
  private NRiTable tbListeMagasin;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
