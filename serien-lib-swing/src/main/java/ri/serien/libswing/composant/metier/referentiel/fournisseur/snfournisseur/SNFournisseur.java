/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;

import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.FournisseurBase;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseurBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.plage.SNComposantPlage;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un fournisseur.<br><br>
 * Ce composant doit être utilisé pour tout affichage et choix d'un fournisseur dans le logiciel. Il gère l'affichage d'un fournisseur et
 * la sélection de l'un d'entre eux.<br>
 * Il comprend un TextField et un bouton permettant de sélectionner un fournisseur dans une boîte de dialogue.<br>
 * Utilisation :<br>
 * - Ajouter un composant SNFournisseur à un écran.<br>
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé. Les informations nécessaires sont la session et l'établissement.<br>
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 */
public class SNFournisseur extends SNComposantPlage {
  private SNTexte tfTexteRecherche;
  private SNBoutonRecherche btnDialogueRechercheFournisseur;
  private FournisseurBase fournisseurSelectionne = null;
  private boolean fenetreFournisseurAffichee = false;
  private boolean rafraichir = false;
  private String texteRecherchePrecedente = null;
  private CritereFournisseur critereFournisseur = new CritereFournisseur();
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Constructeur.
   */
  public SNFournisseur() {
    super();
    setName("SNFournisseur");

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);

    setLayout(new GridBagLayout());

    // Texte de recherche
    tfTexteRecherche = new SNTexte();
    tfTexteRecherche.setName("tfFournisseur");
    tfTexteRecherche.setEditable(true);
    tfTexteRecherche.setToolTipText(VueFournisseur.RECHERCHE_GENERIQUE_TOOLTIP);
    add(tfTexteRecherche,
        new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

    // Bouton loupe
    btnDialogueRechercheFournisseur = new SNBoutonRecherche();
    btnDialogueRechercheFournisseur.setToolTipText("Recherche de fournisseur");
    btnDialogueRechercheFournisseur.setName("btnRechercheFournisseur");
    add(btnDialogueRechercheFournisseur, new GridBagConstraints());

    // Focus listener pour lancer la recherche quand on quitte la zone de saisie
    tfTexteRecherche.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        tfTexteRechercheFocusLost(e);
      }

      @Override
      public void focusGained(FocusEvent e) {
        tfTexteRechercheFocusGained(e);
      }
    });

    // ActionListener pour capturer l'utilisation de la touche entrée
    tfTexteRecherche.addActionListener(new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tfTexteRechercheActionPerformed(e);
      }
    });

    // Être notifié lorsque le bouton est cliqué
    btnDialogueRechercheFournisseur.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnDialogueRechercheFournisseurActionPerformed(e);
      }
    });
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Activer ou désactiver le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    tfTexteRecherche.setEnabled(pEnabled);
    btnDialogueRechercheFournisseur.setEnabled(pEnabled);
  }

  /**
   * Positionner le focus sur le composant de saisie.
   */
  @Override
  public boolean requestFocusInWindow() {
    return tfTexteRecherche.requestFocusInWindow();
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Charger la liste des fournisseurs.<br>
   * Cette méthode ne fait rien dans ce composant car la liste des fournisseurs est chargée à la demande en fonction de la saisie.<br>
   * Elle est conservée par souci de comptabilité avec les autres composants graphiques.<br>
   * @param pForcerRafraichissement Indicatif de rafraîchissement.
   */
  public void charger(boolean pForcerRafraichissement) {
    // Tester s'il faut rafraîchir la liste
    if (!rafraichir && !pForcerRafraichissement) {
      return;
    }
    rafraichir = false;
  }

  /**
   * Initialiser les valeurs du composant pour se retrouver à la situation initiale.
   */
  public void initialiserRecherche() {
    // Effacer les critères de recherche
    critereFournisseur.initialiser();

    // Ne sélectionner aucun fournisseur
    setSelection(null);
  }

  /**
   * Sélectionner un fournisseur à partir d'un champ RPG comportant l'indicatif du fournisseur.
   * @param pLexical Lexique des champs RPG.
   * @param pChampIndicatifFournisseur Nom du champ RPG contenant l'indicatif du fournisseur.
   * @return
   *         true si une modification a été faite.<br>
   *         false si rien n'a changé.
   */
  public boolean setSelectionParChampRPG(Lexical pLexical, String pChampIndicatifFournisseur) {
    if (pChampIndicatifFournisseur == null || pLexical.HostFieldGetData(pChampIndicatifFournisseur) == null) {
      throw new MessageErreurException("Impossible de sélectionner le fournisseur car son indicatif est invalide.");
    }

    // Construire l'id fournisseur
    IdFournisseur idFournisseur = null;
    if (!Constantes.normerTexte(pLexical.HostFieldGetData(pChampIndicatifFournisseur)).isEmpty()) {
      idFournisseur = IdFournisseur.getInstanceParIndicatif(getIdEtablissement(), pLexical.HostFieldGetData(pChampIndicatifFournisseur));
    }

    // Sortir si rien n'a changé
    if (Constantes.equals(idFournisseur, getIdSelection())) {
      return false;
    }

    setSelectionParId(idFournisseur);
    return true;
  }

  /**
   * Sélectionner un fournisseur à partir des champs RPG.
   * @param pLexical Lexique des champs RPG de l'écran.
   * @param pChampCollectifFournisseur Nom du champ RPG contenant le collectif fournisseur.
   * @param pChampNumeroFournisseur Nom du champ RPG contenant le numéro fournisseur.
   * @return
   *         true si le fournisseur a changé.<br>
   *         false si le fournisseur n'a pas changé.
   */
  public boolean setSelectionParChampRPG(Lexical pLexical, String pChampCollectifFournisseur, String pChampNumeroFournisseur) {
    // Tester les paramètres
    if (pChampCollectifFournisseur == null || pLexical.HostFieldGetData(pChampCollectifFournisseur) == null) {
      throw new MessageErreurException("Impossible de sélectionner le fournisseur car son collectif est invalide.");
    }
    if (pChampNumeroFournisseur == null || pLexical.HostFieldGetData(pChampNumeroFournisseur) == null) {
      throw new MessageErreurException("Impossible de sélectionner le fournisseur car son numéro est invalide.");
    }
    
    // Récupérer les informations du fournisseur
    String collectifFournisseur = Constantes.normerTexte(pLexical.HostFieldGetData(pChampCollectifFournisseur));
    String numeroFournisseur = Constantes.normerTexte(pLexical.HostFieldGetData(pChampNumeroFournisseur));

    // Construire l'id fournisseur
    IdFournisseur idFournisseur = null;
    if (!collectifFournisseur.isEmpty() && !numeroFournisseur.isEmpty()) {
      idFournisseur = IdFournisseur.getInstance(getIdEtablissement(), collectifFournisseur, numeroFournisseur);
    }
    
    // Sortir si rien n'a changé
    if (Constantes.equals(idFournisseur, getIdSelection())) {
      return false;
    }
    setSelectionParId(idFournisseur);
    return true;
  }

  /**
   * Sélectionner le fournisseur correspondant à l'identifiant passé en paramètre.
   * @param pIdFournisseur Id du fournisseur à sélectionner.
   */
  public void setSelectionParId(IdFournisseur pIdFournisseur) {
    FournisseurBase fournisseurBase = null;
    ListeFournisseurBase listeFournisseurBase = new ListeFournisseurBase();
    listeFournisseurBase = listeFournisseurBase.charger(getIdSession(), getIdEtablissement());
    if (listeFournisseurBase != null && pIdFournisseur != null) {
      fournisseurBase = listeFournisseurBase.get(pIdFournisseur);
    }
    setSelection(fournisseurBase);
  }

  /**
   * Renseigner les champs RPG correspondant au fournisseur sélectionné.
   * @param pLexical Lexique des champs RPG.
   * @param pChampCollectifFournisseur Nom du champ RPG contenant le collectif du fournisseur.
   * @param pChampNumeroFournisseur Nom du champ RPG contenant le numéro du fournisseur.
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampCollectifFournisseur, String pChampNumeroFournisseur) {
    FournisseurBase fournisseurBase = getSelection();
    if (fournisseurBase != null) {
      pLexical.HostFieldPutData(pChampCollectifFournisseur, 0, fournisseurBase.getId().getCollectif().toString());
      pLexical.HostFieldPutData(pChampNumeroFournisseur, 0, fournisseurBase.getId().getNumero().toString());
    }
    else {
      pLexical.HostFieldPutData(pChampCollectifFournisseur, 0, "");
      pLexical.HostFieldPutData(pChampNumeroFournisseur, 0, "");
    }
  }

  /**
   * Renseigner le champ RPG correspondant au fournisseur sélectionné via sont indicatif.
   * @param pLexical Lexique des champs RPG.
   * @param pChampIndicatifFournisseur Nom du champ RPG contenant l'indicatif du fournisseur.
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampIndicatifFournisseur) {
    FournisseurBase fournisseurBase = getSelection();
    if (fournisseurBase != null) {
      pLexical.HostFieldPutData(pChampIndicatifFournisseur, 0, fournisseurBase.getId().getIndicatifCourt());
    }
    else {
      pLexical.HostFieldPutData(pChampIndicatifFournisseur, 0, "");
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Lancer une recherche parmi les fournisseurs.
   * @param forcerAffichageDialogue Indicatif d'affichage de la boîte de dialogue.
   */
  private void lancerRecherche(boolean forcerAffichageDialogue) {
    // Vérifier la session
    if (getSession() == null) {
      throw new MessageErreurException("La session du composant de sélection de fournisseur est invalide.");
    }

    // Ne rien faire si le texte à rechercher est vide
    if (Constantes.normerTexte(tfTexteRecherche.getText()).isEmpty()) {
      setSelection(null);
      if (forcerAffichageDialogue) {
        afficherDialogueRechercheFournisseur(null);
      }
      return;
    }

    // Ne rien faire si le texte à rechercher n'a pas changé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && tfTexteRecherche.getText().equals(texteRecherchePrecedente)) {
      return;
    }

    fournisseurSelectionne = null;

    // Rechercher les identifiants fournisseurs correspondants aux critères de recherche
    critereFournisseur.setIdEtablissement(getIdEtablissement());
    critereFournisseur.setTexteRechercheGenerique(tfTexteRecherche.getText());
    List<IdFournisseur> listeIdFournisseur =
        ManagerServiceFournisseur.chargerListeIdFournisseur(getSession().getIdSession(), critereFournisseur);

    // Sélectionner automatiquement le seul fournisseur trouvé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && listeIdFournisseur != null && listeIdFournisseur.size() == 1) {
      ListeFournisseurBase listeFournisseurBase = new ListeFournisseurBase();
      listeFournisseurBase = listeFournisseurBase.charger(getSession().getIdSession(), listeIdFournisseur);
      setSelection(listeFournisseurBase.get(0));
    }
    // Afficher la boîte de dialogue si : l'affichage forcé de la boîte de dialogue est activé, aucun ou plusieurs fournisseurs trouvés
    else {
      afficherDialogueRechercheFournisseur(listeIdFournisseur);
    }

    // Sélectionner automatiquement l'ensemble du champ afin que la prochaine saisie vienne en remplacement
    tfTexteRecherche.selectAll();
  }

  /**
   * Afficher la fenêtre de recherche des fournisseurs.
   * @param pListeIdFournisseur Liste d'identifiants de fournisseurs.
   */
  private void afficherDialogueRechercheFournisseur(List<IdFournisseur> pListeIdFournisseur) {
    // Sécurité pour éviter que la boîte de dialogue ne s'affiche deux fois
    if (fenetreFournisseurAffichee) {
      return;
    }
    fenetreFournisseurAffichee = true;

    // Afficher la boîte de dialogue de recherche d'un fournisseur
    critereFournisseur.setIdEtablissement(getIdEtablissement());
    critereFournisseur.setTexteRechercheGenerique(tfTexteRecherche.getText());
    // Si on a déjà sélectionné un fournisseur, force la recherche par l'indicatif court du fournisseur, cela va permettre d'afficher le
    // fournisseur sélectionné uniquement
    if (fournisseurSelectionne != null) {
      critereFournisseur.setTexteRechercheGenerique(fournisseurSelectionne.getRaisonSociale());
    }
    // Définition des critères pour les composants de plage :
    // Si le composant est un composant de début, n'afficher que les fournisseurs inférieurs au fournisseur de fin.
    if (isComposantDeDebut()) {
      SNFournisseur composantFin = (SNFournisseur) snComposantFin;
      critereFournisseur.setIdFournisseurFin(composantFin.getIdSelection());
    }
    // Si le composant est un composant de fin, n'afficher que les fournisseurs supérieurs au fournisseur de début.
    else if (isComposantDeFin()) {
      SNFournisseur composantDebut = (SNFournisseur) snComposantDebut;
      critereFournisseur.setIdFournisseurDebut(composantDebut.getIdSelection());
    }
    ModeleFournisseur modele = new ModeleFournisseur(getSession(), critereFournisseur);
    modele.setListeIdFournisseur(pListeIdFournisseur);
    // La recherche est lancée uniquement si le texte de la recherche générique n'est pas vide dans le cas contraire ce sera à
    // l'utilisateur de lancer la recherche manuellement car le temps de chargement peut être long dans ce cas de figure
    if (!Constantes.normerTexte(tfTexteRecherche.getText()).isEmpty()) {
      modele.lancerRecherche();
    }

    VueFournisseur vue = new VueFournisseur(modele);
    vue.afficher();

    // Sélectionner le fournisseur retourné par la boîte de dialogue (ou aucun si null)
    if (modele.isSortieAvecValidation()) {
      setSelection(modele.getFournisseurSelectionne());
    }
    // La boîte de dialogue est annulé, on remet le fournisseur sélectionné précédement
    else {
      rafraichirTexteRecherche();
    }
    fenetreFournisseurAffichee = false;
  }

  /**
   * Rafraîchir le texte de la recherche avec le fournisseur sélectionné.
   */
  private void rafraichirTexteRecherche() {
    // Mettre à jour le composant
    if (fournisseurSelectionne != null) {
      tfTexteRecherche.setText(fournisseurSelectionne.toString());
    }
    else {
      tfTexteRecherche.setText("");
    }

    tfTexteRecherche.setCaretPosition(0);
    // Mémoriser le dernier texte pour ne pas relancer une recherche inutile
    texteRecherchePrecedente = tfTexteRecherche.getText();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner l'identifiant du fournisseur sélectionné.
   */
  public IdFournisseur getIdSelection() {
    if (fournisseurSelectionne == null) {
      return null;
    }
    return fournisseurSelectionne.getId();
  }

  /**
   * Retourner le fournisseur sélectionné.
   * @return FournisseurBase
   */
  public FournisseurBase getSelection() {
    return fournisseurSelectionne;
  }

  /**
   * Modifier le fournisseur sélectionné.
   * @param pFournisseurBaseSelectionne Fournisseur à sélectionner.
   */
  public void setSelection(FournisseurBase pFournisseurBaseSelectionne) {
    // Tester si la sélection a changé
    boolean bSelectionModifiee = !Constantes.equals(fournisseurSelectionne, pFournisseurBaseSelectionne);

    // Modifier le fournisseur sélectionné
    fournisseurSelectionne = pFournisseurBaseSelectionne;

    // Effacer les critères de recherche si aucun fournisseur n'est sélectionné
    if (fournisseurSelectionne == null) {
      critereFournisseur.initialiser();
    }

    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();

    // Notifier uniquement si la sélection a changé
    if (bSelectionModifiee) {
      fireValueChanged();
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      if (getSelection() == null) {
        return;
      }
      
      // On sélectionne l'ensemble du libellé affiché
      tfTexteRecherche.selectAll();
      // Si le textField est trop petit pour tout afficher on met le curseur au début du libellé pour ne pas afficher que la fin
      if (getSelection().getRaisonSociale() != null
          && tfTexteRecherche.getSize().getWidth() < (getSelection().getRaisonSociale().length() * 10)) {
        tfTexteRecherche.setCaretPosition(0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteRechercheFocusLost(FocusEvent e) {
    try {
      lancerRecherche(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteRechercheActionPerformed(ActionEvent e) {
    try {
      lancerRecherche(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btnDialogueRechercheFournisseurActionPerformed(ActionEvent e) {
    try {
      if (fournisseurSelectionne == null) {
        lancerRecherche(true);
      }
      else {
        ArrayList<IdFournisseur> listeIdFournisseur = new ArrayList<IdFournisseur>();
        afficherDialogueRechercheFournisseur(listeIdFournisseur);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
}
