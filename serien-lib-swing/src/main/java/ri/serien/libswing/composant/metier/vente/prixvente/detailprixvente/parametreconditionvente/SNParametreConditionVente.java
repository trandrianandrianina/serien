/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametreconditionvente;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.math.BigDecimal;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.personnalisation.formuleprix.FormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.ListeFormulePrix;
import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.EnumCategorieConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.EnumTypeConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.checkbox.SNCheckBoxReduit;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Composant permettant d'afficher toutes les informations d'une condition de ventes.
 */
public class SNParametreConditionVente extends SNPanelTitre {
  private static final String TITRE = "Condition de vente";
  private static final String LIBELLE_CODE = "Code";
  private static final String LIBELLE_CODE_CLIENT = "Code client";
  private static final String LIBELLE_CODE_GROUPE_CNV = "Code groupe CNV";
  private static final String LIBELLE_REMISE_VALEUR = "Remise en valeur";
  private static final String LIBELLE_AJOUT_VALEUR = "Ajout en valeur";
  private static final String PREFIXE_ORIGINE = "= ";
  
  private ModeleDialogueDetailPrixVente modele = null;
  private boolean evenementsActifs = false;
  private ParametreConditionVente parametreConditionVente = null;
  private ParametreConditionVente parametreConditionVenteInitial = null;
  private FormulePrixVente formulePrixVente = null;
  
  /**
   * Constructeur.
   */
  public SNParametreConditionVente() {
    super();
    initComponents();

    // Autoriser les ramises négatives
    tfRemiseEnValeur.setNegatifAutorise(true);
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Modèle associé au composant graphique.
   * @return Modèle de la boîte de dialogue.
   */
  public ModeleDialogueDetailPrixVente getModele() {
    return modele;
  }

  /**
   * Modifier le modèle associé au composant graphique.
   * @param pModele Modèle de la boîte de dialogue.
   */
  public void setModele(ModeleDialogueDetailPrixVente pModele) {
    modele = pModele;
  }

  /**
   * Indiquer si les évènements doivent être traités.
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichissement des données du composant.
   */
  public void rafraichir() {
    // Empêcher les évènements pendant le rafraichissement
    evenementsActifs = false;

    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    parametreConditionVente = getModele().getParametreConditionVente();
    parametreConditionVenteInitial = getModele().getParametreConditionVenteInitial();
    formulePrixVente = modele.getFormulePrixVente();
    
    // Configurer tous les composants SNMontant avec la précision souhaitée
    int nombreDecimale = ArrondiPrix.DECIMALE_STANDARD;
    if (formulePrixVente != null && formulePrixVente.getArrondiPrix() != null) {
      nombreDecimale = formulePrixVente.getArrondiPrix().getNombreDecimale();
    }
    tfPrixBaseHT.setLongueurPartieDecimale(nombreDecimale);
    tfRemiseEnValeur.setLongueurPartieDecimale(nombreDecimale);
    tfPrixNetHT.setLongueurPartieDecimale(nombreDecimale);
    
    // Rafraichir les composants
    rafraichirTitre();
    rafraichirOrigine();
    rafraichirCode();
    rafraichirTypeRattachementArticle();
    rafraichirCodeRattachementArticle();
    rafraichirCategorie();
    rafraichirType();
    rafraichirDateDebutValidite();
    rafraichirDateFinValidite();
    rafraichirQuantiteMinimale();

    rafraichirDevise();
    rafraichirColonneTarif();
    rafraichirColonneTarifHeritee();
    rafraichirPrixBaseHT();
    rafraichirPrixBaseHTAvecPump();
    rafraichirPrixBaseHTAvecPrv();
    rafraichirRemiseValeur();
    rafraichirLibelleTauxRemise();
    rafraichirTauxRemise1();
    rafraichirTauxRemise2();
    rafraichirTauxRemise3();
    rafraichirTauxRemise4();
    rafraichirTauxRemise5();
    rafraichirTauxRemise6();
    rafraichirCoefficient();
    rafraichirFormulePrix();
    rafraichirPrixNetHT();
    rafraichirGratuit();

    // Réactiver les évènements pendant le rafraichissement
    evenementsActifs = true;
  }

  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirTitre() {
    if (parametreConditionVente != null && parametreConditionVente.getLibelle() != null) {
      setTitre(TITRE + " " + parametreConditionVente.getLibelle());
    }
    else {
      setTitre(TITRE);
    }
  }

  /**
   * Rafraichir l'origine de la condition de vente.
   */
  private void rafraichirOrigine() {
    if (parametreConditionVente != null && parametreConditionVente.getOrigine() != null) {
      tfOrigine.setText(PREFIXE_ORIGINE + parametreConditionVente.getOrigine().getLibelle());
    }
    else {
      tfOrigine.setText("");
    }
  }
  
  /**
   * Rafraichir le code de la condition de vente.
   */
  private void rafraichirCode() {
    if (parametreConditionVente != null && parametreConditionVente.isConditionVenteClient()) {
      tfCode.setText(parametreConditionVente.getIdRattachementClient().getCode());
      lbCode.setText(LIBELLE_CODE_CLIENT);
    }
    else if (parametreConditionVente != null && parametreConditionVente.isConditionVenteGroupeClient()) {
      tfCode.setText(parametreConditionVente.getIdRattachementClient().getCode());
      lbCode.setText(LIBELLE_CODE_GROUPE_CNV);
    }
    else {
      tfCode.setText("");
      lbCode.setText(LIBELLE_CODE);
    }
  }

  /**
   * Rafraichir le type du rattachement article.
   */
  private void rafraichirTypeRattachementArticle() {
    if (parametreConditionVente == null || parametreConditionVente.getIdRattachementArticle() == null
        || parametreConditionVente.getIdRattachementArticle().getTypeRattachement() == null) {
      tfTypeRattachementArticle.setText("");
      return;
    }
    String code = parametreConditionVente.getIdRattachementArticle().getTypeRattachement().toString();
    tfTypeRattachementArticle.setText(code);
  }

  /**
   * Rafraichir le rattachement article.
   */
  private void rafraichirCodeRattachementArticle() {
    if (parametreConditionVente == null || parametreConditionVente.getIdRattachementArticle() == null
        || parametreConditionVente.getIdRattachementArticle().getCodeRattachement() == null) {
      tfCodeRattachementArticle.setText("");
      return;
    }
    tfCodeRattachementArticle.setText(parametreConditionVente.getIdRattachementArticle().getCodeRattachement());
  }

  /**
   * Rafraichir la catégorie de la condition.
   */
  private void rafraichirCategorie() {
    if (parametreConditionVente == null || parametreConditionVente.getCategorie() == null) {
      tfCategorie.setText("");
      return;
    }
    EnumCategorieConditionVente categorie = parametreConditionVente.getCategorie();
    tfCategorie.setText(categorie.getLibelle());
  }

  /**
   * Rafraichir le type de la condition.
   */
  private void rafraichirType() {
    if (parametreConditionVente == null || parametreConditionVente.getTypeCondition() == null) {
      tfType.setText("");
      return;
    }
    EnumTypeConditionVente type = parametreConditionVente.getTypeCondition();
    if (type == null) {
      tfType.setText("");
    }
    else if (type == EnumTypeConditionVente.PRIX_NET_OU_GRATUIT) {
      // Condition de type gratuit
      if (parametreConditionVente.isGratuit()) {
        tfType.setText("Gratuit");
      }
      // Condition de type prix net
      else {
        tfType.setText("Prix net");
      }
    }
    else {
      tfType.setText(type.getLibelle());
    }
  }

  /**
   * Rafraichir la date de début de validité.
   */
  private void rafraichirDateDebutValidite() {
    if (parametreConditionVente == null || parametreConditionVente.getDateDebutValidite() == null) {
      tfDateDebutValidite.setText("Aucune");
      return;
    }
    Date date = parametreConditionVente.getDateDebutValidite();
    tfDateDebutValidite.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
  }

  /**
   * Rafraichir la date de fin de validité.
   */
  private void rafraichirDateFinValidite() {
    if (parametreConditionVente == null || parametreConditionVente.getDateFinValidite() == null) {
      tfDateFinValidite.setText("Aucune");
      return;
    }
    Date date = parametreConditionVente.getDateFinValidite();
    tfDateFinValidite.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
  }

  /**
   * Rafraichir la quantité minimale.
   */
  private void rafraichirQuantiteMinimale() {
    if (parametreConditionVente == null || parametreConditionVente.getQuantiteMinimale() == null) {
      tfQuantiteMinimale.setText("");
      return;
    }
    tfQuantiteMinimale.setText(parametreConditionVente.getQuantiteMinimale().toPlainString());
  }

  /**
   * Rafraichir la devise.
   */
  private void rafraichirDevise() {
    if (parametreConditionVente == null || parametreConditionVente.getCodeDevise() == null) {
      tfDevise.setText("");
      return;
    }
    tfDevise.setText(parametreConditionVente.getCodeDevise());
  }

  /**
   * Rafraichir la colonne tarif.
   */
  private void rafraichirColonneTarif() {
    if (parametreConditionVente != null && parametreConditionVente.getNumeroColonneTarif() != null) {
      Integer numeroColonneTarif = parametreConditionVente.getNumeroColonneTarif();
      if (numeroColonneTarif >= 1 && numeroColonneTarif <= 10) {
        cbColonneTarif.setSelectedIndex(numeroColonneTarif);
      }
      else {
        cbColonneTarif.setSelectedIndex(0);
      }
    }
    else {
      cbColonneTarif.setSelectedIndex(0);
    }

    cbColonneTarif.setEnabled(modele != null && getModele().isModeSimulation());
    lbColonneTarif.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && !Constantes.equals(parametreConditionVente.getNumeroColonneTarif(), parametreConditionVenteInitial.getNumeroColonneTarif()));
  }

  /**
   * Rafraichir la colonne tarif.
   */
  private void rafraichirColonneTarifHeritee() {
    if (parametreConditionVente != null && parametreConditionVente.getNumeroColonneTarifHeritee() != null) {
      Integer numeroColonne = parametreConditionVente.getNumeroColonneTarifHeritee();
      if (numeroColonne >= 1 && numeroColonne <= 10) {
        cbColonneTarifHeritee.setSelectedIndex(numeroColonne);
      }
      else {
        cbColonneTarifHeritee.setSelectedIndex(0);
      }
    }
    else {
      cbColonneTarifHeritee.setSelectedIndex(0);
    }
  }

  /**
   * Rafraichir le prix de base HT.
   */
  private void rafraichirPrixBaseHT() {
    if (parametreConditionVente != null && parametreConditionVente.getPrixBaseHT() != null) {
      tfPrixBaseHT.setMontant(parametreConditionVente.getPrixBaseHT());
    }
    else {
      tfPrixBaseHT.setMontant("");
    }

    tfPrixBaseHT.setEnabled(modele != null && getModele().isModeSimulation());
    lbPrixBaseHT.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && !Constantes.equals(parametreConditionVente.getPrixBaseHT(), parametreConditionVenteInitial.getPrixBaseHT()));
  }

  /**
   * Rafraichir l'indicateur prix de base HT avec PUMP.
   */
  private void rafraichirPrixBaseHTAvecPump() {
    if (parametreConditionVente != null && parametreConditionVente.isPrixBaseHTAvecPump()) {
      cbPrixBaseHTAvecPUMP.setSelected(true);
    }
    else {
      cbPrixBaseHTAvecPUMP.setSelected(false);
    }

    cbPrixBaseHTAvecPUMP.setEnabled(modele != null && getModele().isModeSimulation());
    lbPrixBaseHTAvecPUMP.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && !Constantes.equals(parametreConditionVente.isPrixBaseHTAvecPump(), parametreConditionVenteInitial.isPrixBaseHTAvecPump()));
  }

  /**
   * Rafraichir l'indicateur prix de base HT avec PRV.
   */
  private void rafraichirPrixBaseHTAvecPrv() {
    if (parametreConditionVente != null && parametreConditionVente.isPrixBaseHTAvecPrv()) {
      cbPrixBaseHTAvecPRV.setSelected(true);
    }
    else {
      cbPrixBaseHTAvecPRV.setSelected(false);
    }

    cbPrixBaseHTAvecPRV.setEnabled(modele != null && getModele().isModeSimulation());
    lbPrixBaseHTAvecPRV.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && !Constantes.equals(parametreConditionVente.isPrixBaseHTAvecPrv(), parametreConditionVenteInitial.isPrixBaseHTAvecPrv()));
  }

  /**
   * Rafraichir la remise en valeur.
   */
  private void rafraichirRemiseValeur() {
    if (parametreConditionVente != null && parametreConditionVente.getRemiseEnValeur() != null) {
      BigDecimal remiseEnValeur = parametreConditionVente.getRemiseEnValeur();
      tfRemiseEnValeur.setMontant(remiseEnValeur);

      // C'est une remise si la valeur est négative et un ajout si la valeur est positive.
      if (remiseEnValeur.compareTo(BigDecimal.ZERO) <= 0) {
        lbRemiseEnValeur.setText(LIBELLE_REMISE_VALEUR);
      }
      else {
        lbRemiseEnValeur.setText(LIBELLE_AJOUT_VALEUR);
      }
    }
    else {
      tfRemiseEnValeur.setMontant("");
      lbRemiseEnValeur.setText(LIBELLE_REMISE_VALEUR);
    }

    tfRemiseEnValeur.setEnabled(modele != null && getModele().isModeSimulation());
    lbRemiseEnValeur.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && !Constantes.equals(parametreConditionVente.getRemiseEnValeur(), parametreConditionVenteInitial.getRemiseEnValeur()));
  }

  /**
   * Rafraichir le libellé du taux de remise.
   */
  private void rafraichirLibelleTauxRemise() {
    lbTauxRemise.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && (!Constantes.equals(parametreConditionVente.getTauxRemise1(), parametreConditionVenteInitial.getTauxRemise1())
            || !Constantes.equals(parametreConditionVente.getTauxRemise2(), parametreConditionVenteInitial.getTauxRemise2())
            || !Constantes.equals(parametreConditionVente.getTauxRemise3(), parametreConditionVenteInitial.getTauxRemise3())
            || !Constantes.equals(parametreConditionVente.getTauxRemise4(), parametreConditionVenteInitial.getTauxRemise4())
            || !Constantes.equals(parametreConditionVente.getTauxRemise5(), parametreConditionVenteInitial.getTauxRemise5())
            || !Constantes.equals(parametreConditionVente.getTauxRemise6(), parametreConditionVenteInitial.getTauxRemise6())));
  }

  /**
   * Rafraichir le taux de remise 1.
   */
  private void rafraichirTauxRemise1() {
    if (parametreConditionVente != null && parametreConditionVente.getTauxRemise1() != null) {
      tfTauxRemise1.setText(parametreConditionVente.getTauxRemise1().getTexteSansSymbole());
    }
    else {
      tfTauxRemise1.setText("");
    }

    tfTauxRemise1.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir le taux de remise 2.
   */
  private void rafraichirTauxRemise2() {
    if (parametreConditionVente != null && parametreConditionVente.getTauxRemise2() != null) {
      tfTauxRemise2.setText(parametreConditionVente.getTauxRemise2().getTexteSansSymbole());
    }
    else {
      tfTauxRemise2.setText("");
    }

    tfTauxRemise2.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir le taux de remise 3.
   */
  private void rafraichirTauxRemise3() {
    if (parametreConditionVente != null && parametreConditionVente.getTauxRemise3() != null) {
      tfTauxRemise3.setText(parametreConditionVente.getTauxRemise3().getTexteSansSymbole());
    }
    else {
      tfTauxRemise3.setText("");
    }

    tfTauxRemise3.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir le taux de remise 4.
   */
  private void rafraichirTauxRemise4() {
    if (parametreConditionVente != null && parametreConditionVente.getTauxRemise4() != null) {
      tfTauxRemise4.setText(parametreConditionVente.getTauxRemise4().getTexteSansSymbole());
    }
    else {
      tfTauxRemise4.setText("");
    }

    tfTauxRemise4.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir le taux de remise 5.
   */
  private void rafraichirTauxRemise5() {
    if (parametreConditionVente != null && parametreConditionVente.getTauxRemise5() != null) {
      tfTauxRemise5.setText(parametreConditionVente.getTauxRemise5().getTexteSansSymbole());
    }
    else {
      tfTauxRemise5.setText("");
    }

    tfTauxRemise5.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir le taux de remise 6.
   */
  private void rafraichirTauxRemise6() {
    if (parametreConditionVente != null && parametreConditionVente.getTauxRemise6() != null) {
      tfTauxRemise6.setText(parametreConditionVente.getTauxRemise6().getTexteSansSymbole());
    }
    else {
      tfTauxRemise6.setText("");
    }

    tfTauxRemise6.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir le coefficient.
   */
  private void rafraichirCoefficient() {
    if (parametreConditionVente != null && parametreConditionVente.getCoefficient() != null
        && parametreConditionVente.getCoefficient().compareTo(BigDecimal.ZERO) != 0) {
      tfCoefficient.setMontant(parametreConditionVente.getCoefficient());
    }
    else {
      tfCoefficient.setMontant("");
    }

    tfCoefficient.setEnabled(modele != null && modele.isModeSimulation());
    lbCoefficient.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && !Constantes.equals(parametreConditionVente.getCoefficient(), parametreConditionVenteInitial.getCoefficient()));
  }

  /**
   * Rafraichir la formule prix.
   */
  private void rafraichirFormulePrix() {
    ParametreEtablissement parametreEtablissement = getModele().getParametreEtablissement();

    // Renseigner la liste déroulante avec la liste des formules de prix disponibles
    if (tfFormulePrix.getItemCount() == 0) {
      tfFormulePrix.addItem("Aucune");
      if (parametreEtablissement != null && parametreEtablissement.getListeFormulePrix() != null) {
        ListeFormulePrix listeFormulePrix = getModele().getParametreEtablissement().getListeFormulePrix();
        for (FormulePrix formulePrix : listeFormulePrix) {
          tfFormulePrix.addItem(formulePrix);
        }
      }
    }

    if (parametreConditionVente != null && parametreConditionVente.getIdFormulePrix() != null && parametreEtablissement != null
        && parametreEtablissement.getListeFormulePrix() != null) {
      tfFormulePrix.setSelectedItem(parametreEtablissement.getListeFormulePrix().get(parametreConditionVente.getIdFormulePrix()));
    }
    else {
      tfFormulePrix.setSelectedIndex(0);
    }

    tfFormulePrix.setEnabled(modele != null && modele.isModeSimulation());
    lbFormulePrix.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && !Constantes.equals(parametreConditionVente.getIdFormulePrix(), parametreConditionVenteInitial.getIdFormulePrix()));
  }

  /**
   * Rafraichir le prix net HT.
   */
  private void rafraichirPrixNetHT() {
    if (parametreConditionVente != null && parametreConditionVente.getPrixNetHT() != null) {
      tfPrixNetHT.setMontant(parametreConditionVente.getPrixNetHT());
    }
    else {
      tfPrixNetHT.setMontant("");
    }

    tfPrixNetHT.setEnabled(modele != null && modele.isModeSimulation());
    lbPrixNetHT.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && !Constantes.equals(parametreConditionVente.getPrixNetHT(), parametreConditionVenteInitial.getPrixNetHT()));
  }

  /**
   * Rafraichir le type gratuit.
   */
  private void rafraichirGratuit() {
    if (parametreConditionVente != null && parametreConditionVente.isGratuit()) {
      cbGratuit.setSelected(true);
    }
    else {
      cbGratuit.setSelected(false);
    }

    cbGratuit.setEnabled(modele != null && modele.isModeSimulation());
    lbGratuit.setImportance(parametreConditionVente != null && parametreConditionVenteInitial != null
        && !Constantes.equals(parametreConditionVente.isGratuit(), parametreConditionVenteInitial.isGratuit()));
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void cbColonneTarifItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbColonneTarif.getSelectedIndex() >= 1 && cbColonneTarif.getSelectedIndex() <= 10) {
        getModele().modifierColonneTarifConditionVente(cbColonneTarif.getSelectedIndex());
      }
      else {
        getModele().modifierColonneTarifConditionVente(0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfPrixBaseHTValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTConditionVente(tfPrixBaseHT.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbPrixBaseHTAvecPUMPActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTAvecPumpConditionVente(cbPrixBaseHTAvecPUMP.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbPrixBaseHTAvecPRVActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTAvecPrvConditionVente(cbPrixBaseHTAvecPRV.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxRemise1FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseConditionVente(tfTauxRemise1.getText(), 1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxRemise2FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseConditionVente(tfTauxRemise2.getText(), 2);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxRemise3FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseConditionVente(tfTauxRemise3.getText(), 3);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxRemise4FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseConditionVente(tfTauxRemise4.getText(), 4);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxRemise5FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseConditionVente(tfTauxRemise5.getText(), 5);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxRemise6FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseConditionVente(tfTauxRemise6.getText(), 6);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfRemiseEnValeurValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierRemiseEnValeurConditionVente(tfRemiseEnValeur.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfCoefficientValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierCoefficientConditionVente(tfCoefficient.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfFormulePrixItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      Object selection = tfFormulePrix.getSelectedItem();
      if (selection instanceof FormulePrix) {
        getModele().modifierFormulePrixConditionVente(((FormulePrix) selection).getId());
      }
      else {
        getModele().modifierFormulePrixConditionVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfPrixNetHTValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixNetHTConditionVente(tfPrixNetHT.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbGratuitActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierGratuitConditionVente(cbGratuit.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbOrigine = new SNLabelChamp();
    tfOrigine = new SNLabelUnite();
    lbCode = new SNLabelChamp();
    tfCode = new SNTexte();
    lbTypeRattachementArticle = new SNLabelChamp();
    tfTypeRattachementArticle = new SNTexte();
    lbCodeRattachementArticle = new SNLabelChamp();
    tfCodeRattachementArticle = new SNTexte();
    lbCategorie = new SNLabelChamp();
    tfCategorie = new SNTexte();
    lbType = new SNLabelChamp();
    tfType = new SNTexte();
    lbDateDebutValidite = new SNLabelChamp();
    tfDateDebutValidite = new SNTexte();
    lbDateFinValidite = new SNLabelChamp();
    tfDateFinValidite = new SNTexte();
    lbQuantiteMinimale = new SNLabelChamp();
    tfQuantiteMinimale = new SNTexte();
    lbDevise = new SNLabelChamp();
    tfDevise = new SNTexte();
    lbColonneTarif = new SNLabelChamp();
    cbColonneTarif = new SNComboBoxReduit();
    lbColonneTarifHeritee = new SNLabelChamp();
    cbColonneTarifHeritee = new SNComboBoxReduit();
    lbPrixBaseHT = new SNLabelChamp();
    tfPrixBaseHT = new SNMontant();
    lbPrixBaseHTAvecPUMP = new SNLabelChamp();
    cbPrixBaseHTAvecPUMP = new SNCheckBoxReduit();
    lbPrixBaseHTAvecPRV = new SNLabelChamp();
    cbPrixBaseHTAvecPRV = new SNCheckBoxReduit();
    lbRemiseEnValeur = new SNLabelChamp();
    tfRemiseEnValeur = new SNMontant();
    lbTauxRemise = new SNLabelChamp();
    pnlTauxRemise = new SNPanel();
    tfTauxRemise1 = new SNTexte();
    tfTauxRemise2 = new SNTexte();
    tfTauxRemise3 = new SNTexte();
    tfTauxRemise4 = new SNTexte();
    tfTauxRemise5 = new SNTexte();
    tfTauxRemise6 = new SNTexte();
    lbCoefficient = new SNLabelChamp();
    tfCoefficient = new SNMontant();
    lbFormulePrix = new SNLabelChamp();
    tfFormulePrix = new SNComboBoxReduit();
    lbPrixNetHT = new SNLabelChamp();
    tfPrixNetHT = new SNMontant();
    lbGratuit = new SNLabelChamp();
    cbGratuit = new SNCheckBoxReduit();

    // ======== this ========
    setTitre("Condition de vente");
    setModeReduit(true);
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights =
        new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };

    // ---- lbOrigine ----
    lbOrigine.setModeReduit(true);
    lbOrigine.setText("Origine");
    lbOrigine.setMinimumSize(new Dimension(130, 22));
    lbOrigine.setPreferredSize(new Dimension(130, 22));
    lbOrigine.setName("lbOrigine");
    add(lbOrigine,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfOrigine ----
    tfOrigine.setModeReduit(true);
    tfOrigine.setName("tfOrigine");
    add(tfOrigine,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbCode ----
    lbCode.setModeReduit(true);
    lbCode.setText("Code");
    lbCode.setMinimumSize(new Dimension(30, 22));
    lbCode.setPreferredSize(new Dimension(30, 22));
    lbCode.setName("lbCode");
    add(lbCode,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfCode ----
    tfCode.setModeReduit(true);
    tfCode.setEnabled(false);
    tfCode.setName("tfCode");
    add(tfCode,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbTypeRattachementArticle ----
    lbTypeRattachementArticle.setModeReduit(true);
    lbTypeRattachementArticle.setText("Type rattachement article");
    lbTypeRattachementArticle.setMinimumSize(new Dimension(30, 22));
    lbTypeRattachementArticle.setPreferredSize(new Dimension(30, 22));
    lbTypeRattachementArticle.setName("lbTypeRattachementArticle");
    add(lbTypeRattachementArticle,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfTypeRattachementArticle ----
    tfTypeRattachementArticle.setModeReduit(true);
    tfTypeRattachementArticle.setEnabled(false);
    tfTypeRattachementArticle.setName("tfTypeRattachementArticle");
    add(tfTypeRattachementArticle,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbCodeRattachementArticle ----
    lbCodeRattachementArticle.setModeReduit(true);
    lbCodeRattachementArticle.setText("Code rattachement article");
    lbCodeRattachementArticle.setMinimumSize(new Dimension(30, 22));
    lbCodeRattachementArticle.setPreferredSize(new Dimension(30, 22));
    lbCodeRattachementArticle.setName("lbCodeRattachementArticle");
    add(lbCodeRattachementArticle,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfCodeRattachementArticle ----
    tfCodeRattachementArticle.setModeReduit(true);
    tfCodeRattachementArticle.setEnabled(false);
    tfCodeRattachementArticle.setName("tfCodeRattachementArticle");
    add(tfCodeRattachementArticle,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbCategorie ----
    lbCategorie.setModeReduit(true);
    lbCategorie.setText("Cat\u00e9gorie");
    lbCategorie.setMinimumSize(new Dimension(30, 22));
    lbCategorie.setPreferredSize(new Dimension(30, 22));
    lbCategorie.setName("lbCategorie");
    add(lbCategorie,
        new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfCategorie ----
    tfCategorie.setModeReduit(true);
    tfCategorie.setEnabled(false);
    tfCategorie.setName("tfCategorie");
    add(tfCategorie,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbType ----
    lbType.setModeReduit(true);
    lbType.setText("Type");
    lbType.setMinimumSize(new Dimension(30, 22));
    lbType.setPreferredSize(new Dimension(30, 22));
    lbType.setName("lbType");
    add(lbType,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfType ----
    tfType.setModeReduit(true);
    tfType.setEnabled(false);
    tfType.setName("tfType");
    add(tfType,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbDateDebutValidite ----
    lbDateDebutValidite.setModeReduit(true);
    lbDateDebutValidite.setText("Date d\u00e9but validit\u00e9");
    lbDateDebutValidite.setMinimumSize(new Dimension(30, 22));
    lbDateDebutValidite.setPreferredSize(new Dimension(30, 22));
    lbDateDebutValidite.setName("lbDateDebutValidite");
    add(lbDateDebutValidite,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfDateDebutValidite ----
    tfDateDebutValidite.setModeReduit(true);
    tfDateDebutValidite.setEnabled(false);
    tfDateDebutValidite.setName("tfDateDebutValidite");
    add(tfDateDebutValidite,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbDateFinValidite ----
    lbDateFinValidite.setModeReduit(true);
    lbDateFinValidite.setText("Date fin validit\u00e9");
    lbDateFinValidite.setMinimumSize(new Dimension(30, 22));
    lbDateFinValidite.setPreferredSize(new Dimension(30, 22));
    lbDateFinValidite.setName("lbDateFinValidite");
    add(lbDateFinValidite,
        new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfDateFinValidite ----
    tfDateFinValidite.setModeReduit(true);
    tfDateFinValidite.setEnabled(false);
    tfDateFinValidite.setName("tfDateFinValidite");
    add(tfDateFinValidite,
        new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbQuantiteMinimale ----
    lbQuantiteMinimale.setModeReduit(true);
    lbQuantiteMinimale.setText("Quantite minimale");
    lbQuantiteMinimale.setMinimumSize(new Dimension(30, 22));
    lbQuantiteMinimale.setPreferredSize(new Dimension(30, 22));
    lbQuantiteMinimale.setName("lbQuantiteMinimale");
    add(lbQuantiteMinimale,
        new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfQuantiteMinimale ----
    tfQuantiteMinimale.setModeReduit(true);
    tfQuantiteMinimale.setEnabled(false);
    tfQuantiteMinimale.setForeground(Color.black);
    tfQuantiteMinimale.setHorizontalAlignment(SwingConstants.TRAILING);
    tfQuantiteMinimale.setName("tfQuantiteMinimale");
    add(tfQuantiteMinimale,
        new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbDevise ----
    lbDevise.setModeReduit(true);
    lbDevise.setText("Devise");
    lbDevise.setMinimumSize(new Dimension(30, 22));
    lbDevise.setPreferredSize(new Dimension(30, 22));
    lbDevise.setName("lbDevise");
    add(lbDevise,
        new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfDevise ----
    tfDevise.setModeReduit(true);
    tfDevise.setEnabled(false);
    tfDevise.setName("tfDevise");
    add(tfDevise,
        new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbColonneTarif ----
    lbColonneTarif.setModeReduit(true);
    lbColonneTarif.setText("Colonne tarif");
    lbColonneTarif.setMinimumSize(new Dimension(30, 22));
    lbColonneTarif.setPreferredSize(new Dimension(30, 22));
    lbColonneTarif.setName("lbColonneTarif");
    add(lbColonneTarif,
        new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbColonneTarif ----
    cbColonneTarif.setModel(new DefaultComboBoxModel<>(new String[] { "Aucune", "Colonne 1", "Colonne 2", "Colonne 3", "Colonne 4",
        "Colonne 5", "Colonne 6", "Colonne 7", "Colonne 8", "Colonne 9", "Colonne 10" }));
    cbColonneTarif.setSelectedIndex(-1);
    cbColonneTarif.setEnabled(false);
    cbColonneTarif.setName("cbColonneTarif");
    cbColonneTarif.addItemListener(e -> cbColonneTarifItemStateChanged(e));
    add(cbColonneTarif,
        new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbColonneTarifHeritee ----
    lbColonneTarifHeritee.setModeReduit(true);
    lbColonneTarifHeritee.setText("Colonne tarif h\u00e9rit\u00e9e");
    lbColonneTarifHeritee.setMinimumSize(new Dimension(30, 22));
    lbColonneTarifHeritee.setPreferredSize(new Dimension(30, 22));
    lbColonneTarifHeritee.setName("lbColonneTarifHeritee");
    add(lbColonneTarifHeritee,
        new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbColonneTarifHeritee ----
    cbColonneTarifHeritee.setModel(new DefaultComboBoxModel<>(new String[] { "Aucune", "Colonne 1", "Colonne 2", "Colonne 3", "Colonne 4",
        "Colonne 5", "Colonne 6", "Colonne 7", "Colonne 8", "Colonne 9", "Colonne 10" }));
    cbColonneTarifHeritee.setSelectedIndex(-1);
    cbColonneTarifHeritee.setEnabled(false);
    cbColonneTarifHeritee.setName("cbColonneTarifHeritee");
    add(cbColonneTarifHeritee,
        new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbPrixBaseHT ----
    lbPrixBaseHT.setModeReduit(true);
    lbPrixBaseHT.setText("Prix base HT");
    lbPrixBaseHT.setMinimumSize(new Dimension(30, 22));
    lbPrixBaseHT.setPreferredSize(new Dimension(30, 22));
    lbPrixBaseHT.setName("lbPrixBaseHT");
    add(lbPrixBaseHT,
        new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfPrixBaseHT ----
    tfPrixBaseHT.setModeReduit(true);
    tfPrixBaseHT.setEnabled(false);
    tfPrixBaseHT.setName("tfPrixBaseHT");
    tfPrixBaseHT.addSNComposantListener(e -> tfPrixBaseHTValueChanged(e));
    add(tfPrixBaseHT, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbPrixBaseHTAvecPUMP ----
    lbPrixBaseHTAvecPUMP.setModeReduit(true);
    lbPrixBaseHTAvecPUMP.setText("Prix base HT avec PUMP");
    lbPrixBaseHTAvecPUMP.setMinimumSize(new Dimension(30, 22));
    lbPrixBaseHTAvecPUMP.setPreferredSize(new Dimension(30, 22));
    lbPrixBaseHTAvecPUMP.setName("lbPrixBaseHTAvecPUMP");
    add(lbPrixBaseHTAvecPUMP,
        new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbPrixBaseHTAvecPUMP ----
    cbPrixBaseHTAvecPUMP.setEnabled(false);
    cbPrixBaseHTAvecPUMP.setName("cbPrixBaseHTAvecPUMP");
    cbPrixBaseHTAvecPUMP.addActionListener(e -> cbPrixBaseHTAvecPUMPActionPerformed(e));
    add(cbPrixBaseHTAvecPUMP, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbPrixBaseHTAvecPRV ----
    lbPrixBaseHTAvecPRV.setModeReduit(true);
    lbPrixBaseHTAvecPRV.setText("Prix base HT avec PRV");
    lbPrixBaseHTAvecPRV.setMinimumSize(new Dimension(30, 22));
    lbPrixBaseHTAvecPRV.setPreferredSize(new Dimension(30, 22));
    lbPrixBaseHTAvecPRV.setName("lbPrixBaseHTAvecPRV");
    add(lbPrixBaseHTAvecPRV,
        new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbPrixBaseHTAvecPRV ----
    cbPrixBaseHTAvecPRV.setEnabled(false);
    cbPrixBaseHTAvecPRV.setName("cbPrixBaseHTAvecPRV");
    cbPrixBaseHTAvecPRV.addActionListener(e -> cbPrixBaseHTAvecPRVActionPerformed(e));
    add(cbPrixBaseHTAvecPRV, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbRemiseEnValeur ----
    lbRemiseEnValeur.setModeReduit(true);
    lbRemiseEnValeur.setText("Remise en valeur");
    lbRemiseEnValeur.setMinimumSize(new Dimension(30, 22));
    lbRemiseEnValeur.setPreferredSize(new Dimension(30, 22));
    lbRemiseEnValeur.setName("lbRemiseEnValeur");
    add(lbRemiseEnValeur,
        new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfRemiseEnValeur ----
    tfRemiseEnValeur.setModeReduit(true);
    tfRemiseEnValeur.setEnabled(false);
    tfRemiseEnValeur.setName("tfRemiseEnValeur");
    tfRemiseEnValeur.addSNComposantListener(e -> tfRemiseEnValeurValueChanged(e));
    add(tfRemiseEnValeur, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbTauxRemise ----
    lbTauxRemise.setModeReduit(true);
    lbTauxRemise.setText("Taux remises (%)");
    lbTauxRemise.setMinimumSize(new Dimension(30, 22));
    lbTauxRemise.setPreferredSize(new Dimension(30, 22));
    lbTauxRemise.setName("lbTauxRemise");
    add(lbTauxRemise, new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 3), 0, 0));

    // ======== pnlTauxRemise ========
    {
      pnlTauxRemise.setName("pnlTauxRemise");
      pnlTauxRemise.setLayout(new GridLayout(2, 0, 3, 0));

      // ---- tfTauxRemise1 ----
      tfTauxRemise1.setEnabled(false);
      tfTauxRemise1.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise1.setModeReduit(true);
      tfTauxRemise1.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise1.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise1.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise1.setName("tfTauxRemise1");
      tfTauxRemise1.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise1FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise1);

      // ---- tfTauxRemise2 ----
      tfTauxRemise2.setEnabled(false);
      tfTauxRemise2.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise2.setModeReduit(true);
      tfTauxRemise2.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise2.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise2.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise2.setName("tfTauxRemise2");
      tfTauxRemise2.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise2FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise2);

      // ---- tfTauxRemise3 ----
      tfTauxRemise3.setEnabled(false);
      tfTauxRemise3.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise3.setModeReduit(true);
      tfTauxRemise3.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise3.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise3.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise3.setName("tfTauxRemise3");
      tfTauxRemise3.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise3FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise3);

      // ---- tfTauxRemise4 ----
      tfTauxRemise4.setEnabled(false);
      tfTauxRemise4.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise4.setModeReduit(true);
      tfTauxRemise4.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise4.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise4.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise4.setName("tfTauxRemise4");
      tfTauxRemise4.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise4FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise4);

      // ---- tfTauxRemise5 ----
      tfTauxRemise5.setEnabled(false);
      tfTauxRemise5.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise5.setModeReduit(true);
      tfTauxRemise5.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise5.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise5.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise5.setName("tfTauxRemise5");
      tfTauxRemise5.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise5FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise5);

      // ---- tfTauxRemise6 ----
      tfTauxRemise6.setEnabled(false);
      tfTauxRemise6.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise6.setModeReduit(true);
      tfTauxRemise6.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise6.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise6.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise6.setName("tfTauxRemise6");
      tfTauxRemise6.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise6FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise6);
    }
    add(pnlTauxRemise,
        new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbCoefficient ----
    lbCoefficient.setModeReduit(true);
    lbCoefficient.setText("Coefficient");
    lbCoefficient.setMinimumSize(new Dimension(30, 22));
    lbCoefficient.setPreferredSize(new Dimension(30, 22));
    lbCoefficient.setName("lbCoefficient");
    add(lbCoefficient,
        new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfCoefficient ----
    tfCoefficient.setModeReduit(true);
    tfCoefficient.setEnabled(false);
    tfCoefficient.setName("tfCoefficient");
    tfCoefficient.addSNComposantListener(e -> tfCoefficientValueChanged(e));
    add(tfCoefficient, new GridBagConstraints(1, 17, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbFormulePrix ----
    lbFormulePrix.setModeReduit(true);
    lbFormulePrix.setText("Formule prix");
    lbFormulePrix.setMinimumSize(new Dimension(30, 22));
    lbFormulePrix.setPreferredSize(new Dimension(30, 22));
    lbFormulePrix.setName("lbFormulePrix");
    add(lbFormulePrix,
        new GridBagConstraints(0, 18, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfFormulePrix ----
    tfFormulePrix.setEnabled(false);
    tfFormulePrix.setName("tfFormulePrix");
    tfFormulePrix.addItemListener(e -> tfFormulePrixItemStateChanged(e));
    add(tfFormulePrix,
        new GridBagConstraints(1, 18, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbPrixNetHT ----
    lbPrixNetHT.setModeReduit(true);
    lbPrixNetHT.setText("Prix net HT");
    lbPrixNetHT.setMinimumSize(new Dimension(30, 22));
    lbPrixNetHT.setPreferredSize(new Dimension(30, 22));
    lbPrixNetHT.setName("lbPrixNetHT");
    add(lbPrixNetHT,
        new GridBagConstraints(0, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfPrixNetHT ----
    tfPrixNetHT.setModeReduit(true);
    tfPrixNetHT.setEnabled(false);
    tfPrixNetHT.setName("tfPrixNetHT");
    tfPrixNetHT.addSNComposantListener(e -> tfPrixNetHTValueChanged(e));
    add(tfPrixNetHT, new GridBagConstraints(1, 19, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbGratuit ----
    lbGratuit.setModeReduit(true);
    lbGratuit.setText("Gratuit");
    lbGratuit.setMinimumSize(new Dimension(30, 22));
    lbGratuit.setPreferredSize(new Dimension(30, 22));
    lbGratuit.setName("lbGratuit");
    add(lbGratuit,
        new GridBagConstraints(0, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbGratuit ----
    cbGratuit.setEnabled(false);
    cbGratuit.setName("cbGratuit");
    cbGratuit.addActionListener(e -> cbGratuitActionPerformed(e));
    add(cbGratuit, new GridBagConstraints(1, 20, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbOrigine;
  private SNLabelUnite tfOrigine;
  private SNLabelChamp lbCode;
  private SNTexte tfCode;
  private SNLabelChamp lbTypeRattachementArticle;
  private SNTexte tfTypeRattachementArticle;
  private SNLabelChamp lbCodeRattachementArticle;
  private SNTexte tfCodeRattachementArticle;
  private SNLabelChamp lbCategorie;
  private SNTexte tfCategorie;
  private SNLabelChamp lbType;
  private SNTexte tfType;
  private SNLabelChamp lbDateDebutValidite;
  private SNTexte tfDateDebutValidite;
  private SNLabelChamp lbDateFinValidite;
  private SNTexte tfDateFinValidite;
  private SNLabelChamp lbQuantiteMinimale;
  private SNTexte tfQuantiteMinimale;
  private SNLabelChamp lbDevise;
  private SNTexte tfDevise;
  private SNLabelChamp lbColonneTarif;
  private SNComboBoxReduit cbColonneTarif;
  private SNLabelChamp lbColonneTarifHeritee;
  private SNComboBoxReduit cbColonneTarifHeritee;
  private SNLabelChamp lbPrixBaseHT;
  private SNMontant tfPrixBaseHT;
  private SNLabelChamp lbPrixBaseHTAvecPUMP;
  private SNCheckBoxReduit cbPrixBaseHTAvecPUMP;
  private SNLabelChamp lbPrixBaseHTAvecPRV;
  private SNCheckBoxReduit cbPrixBaseHTAvecPRV;
  private SNLabelChamp lbRemiseEnValeur;
  private SNMontant tfRemiseEnValeur;
  private SNLabelChamp lbTauxRemise;
  private SNPanel pnlTauxRemise;
  private SNTexte tfTauxRemise1;
  private SNTexte tfTauxRemise2;
  private SNTexte tfTauxRemise3;
  private SNTexte tfTauxRemise4;
  private SNTexte tfTauxRemise5;
  private SNTexte tfTauxRemise6;
  private SNLabelChamp lbCoefficient;
  private SNMontant tfCoefficient;
  private SNLabelChamp lbFormulePrix;
  private SNComboBoxReduit tfFormulePrix;
  private SNLabelChamp lbPrixNetHT;
  private SNMontant tfPrixNetHT;
  private SNLabelChamp lbGratuit;
  private SNCheckBoxReduit cbGratuit;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
