/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.snarticle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleConsigne;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleSpecial;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleStock;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreHorsGamme;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue de sélection d'un article.
 */
public class VueArticle extends AbstractVueDialogue<ModeleArticle> {
  // Constantes
  public final static String RECHERCHE_GENERIQUE_TOOLTIP = "<html>Recherche sur :<br>\r\n" + "- code article (début)<br>\r\n"
      + "- libellé article (début)<br>\r\n" + "- mot de classement 1 (début)<br>\r\n" + "- mot de classement 2 (début)<br>\r\n";
  private final static String[] TITRE_LISTE = new String[] { "Identifiant", "Libell\u00e9", "Famille" };
  
  /**
   * Constructeur.
   */
  public VueArticle(ModeleArticle pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(1200, 380);
    setResizable(false);
    
    // Infobulle pour description des champs utilisés pour la recherche
    tfTexteRecherche.setToolTipText(RECHERCHE_GENERIQUE_TOOLTIP);
    
    // Configurer le tableau
    scpListeArticle.getViewport().setBackground(Color.WHITE);
    tblListeArticle.personnaliserAspect(TITRE_LISTE, new int[] { 120, 600, 150 }, new int[] { 80, 800, 330 },
        new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE }, 13);
    tblListeArticle.personnaliserAspectCellules(new JTableArticleRenderer());
    
    // Ajouter un listener si la zone d'affichage du tableau est modifiée
    JViewport viewport = scpListeArticle.getViewport();
    viewport.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        if (isEvenementsActifs()) {
          scpListeArticleStateChanged(e);
        }
      }
    });
    scpListeArticle.setViewport(viewport);
    scpListeArticle.getViewport().setBackground(Color.WHITE);
    
    // Permet de redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER ferme la fenêtre
        if (tblListeArticle.getSelectedRowCount() > 0) {
          getModele().quitterAvecValidation();
        }
      }
    };
    tblListeArticle.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    
    // Etre notifier des changements de sélection
    tblListeArticle.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeArticleSelectionChanged(e);
      }
    });
    
    // initialiser les combos
    // Hors gamme
    cbHorsGamme.removeAllItems();
    for (EnumFiltreHorsGamme filtre : EnumFiltreHorsGamme.values()) {
      cbHorsGamme.addItem(filtre);
    }
    if (getModele().getHorsGamme() != null) {
      cbHorsGamme.setSelectedItem(getModele().getHorsGamme());
    }
    // Spécial
    cbSpeciaux.removeAllItems();
    for (EnumFiltreArticleSpecial filtre : EnumFiltreArticleSpecial.values()) {
      cbSpeciaux.addItem(filtre);
    }
    if (getModele().getSpecial() != null) {
      cbSpeciaux.setSelectedItem(getModele().getSpecial());
    }
    // Affiche la recherche sur les articles spéciaux s'il n'y a pas le filtre sur le filtre article géré en stock
    if (getModele().getStock() == EnumFiltreArticleStock.OPTION_GERE_STOCK) {
      lbSpecial.setVisible(false);
      cbSpeciaux.setVisible(false);
    }
    
    // Consigné
    cbConsigne.removeAllItems();
    for (EnumFiltreArticleConsigne filtre : EnumFiltreArticleConsigne.values()) {
      cbConsigne.addItem(filtre);
    }
    if (getModele().getConsigne() != null) {
      cbConsigne.setSelectedItem(getModele().getConsigne());
    }

    // Commentaire
    cbCommentaire.removeAllItems();
    for (EnumFiltreArticleCommentaire filtre : EnumFiltreArticleCommentaire.values()) {
      cbCommentaire.addItem(filtre);
    }
    if (getModele().getConsigne() != null) {
      cbCommentaire.setSelectedItem(getModele().getCommentaire());
    }

    // Configurer la barre de boutons
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Initialiser les familles
    snFamille.setSession(getModele().getSession());
    snFamille.setIdEtablissement(getModele().getIdEtablissement());
    snFamille.charger(false);
    snFamille.setTousAutorise(true);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirTexteRecherche();
    rafraichirTitreListeArticle();
    rafraichirFamille();
    rafraichirFiltreHorsGamme();
    rafraichirFiltreSpecial();
    rafraichirFiltreConsigne();
    rafraichirFiltreCommentaire();
    rafraichirListe();
    rafraichirBouton();
    
    if (getModele().getListeArticleBase() != null && !getModele().getListeArticleBase().isEmpty()) {
      tblListeArticle.requestFocus();
    }
    else {
      tfTexteRecherche.requestFocus();
    }
  }
  
  // -- Méthodes privées

  private void rafraichirTexteRecherche() {
    if (getModele().getTexteRecherche() != null) {
      tfTexteRecherche.setText(getModele().getTexteRecherche());
    }
    else {
      tfTexteRecherche.setText("");
    }
  }
  
  private void rafraichirTitreListeArticle() {
    Message message = getModele().getMessage();
    if (message != null) {
      lbTitreListeArticle.setMessage(getModele().getMessage());
    }
    else {
      lbTitreListeArticle.setText("");
    }
  }
  
  private void rafraichirFamille() {
    snFamille.setSelection(getModele().getFamille());
  }
  
  private void rafraichirFiltreHorsGamme() {
    if (getModele().getHorsGamme() != null) {
      cbHorsGamme.setSelectedItem(getModele().getHorsGamme());
    }
    cbHorsGamme.setEnabled(!getModele().isVerrouillerCriteres());
  }
  
  private void rafraichirFiltreSpecial() {
    if (getModele().getSpecial() != null) {
      cbSpeciaux.setSelectedItem(getModele().getSpecial());
    }
    cbSpeciaux.setEnabled(!getModele().isVerrouillerCriteres());
  }
  
  private void rafraichirFiltreConsigne() {
    if (getModele().getConsigne() != null) {
      cbConsigne.setSelectedItem(getModele().getConsigne());
    }
    cbConsigne.setEnabled(!getModele().isVerrouillerCriteres());
  }
  
  private void rafraichirFiltreCommentaire() {
    if (getModele().getCommentaire() != null) {
      cbCommentaire.setSelectedItem(getModele().getCommentaire());
    }
    cbCommentaire.setEnabled(!getModele().isVerrouillerCriteres());
  }
  
  private void rafraichirBouton() {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, tblListeArticle.getSelectedRow() >= 0);
  }
  
  private void rafraichirListe() {
    // Récupérer la liste des articles à afficher
    ListeArticleBase listeArticleBase = getModele().getListeArticleBase();
    if (listeArticleBase == null) {
      tblListeArticle.mettreAJourDonnees(null);
      scpListeArticle.getVerticalScrollBar().setValue(0);
      return;
    }
    
    // Mettre à jour le tableau
    String[][] donnees = new String[listeArticleBase.size()][VueArticle.TITRE_LISTE.length];
    for (int ligne = 0; ligne < listeArticleBase.size(); ligne++) {
      ArticleBase articleBase = listeArticleBase.get(ligne);
      donnees[ligne][0] = articleBase.getId().toString();
      donnees[ligne][1] = articleBase.getLibelleComplet();
      if (articleBase.getIdFamille() != null) {
        donnees[ligne][2] = getModele().getListeFamille().retournerLibelle(articleBase.getIdFamille());
      }
    }
    tblListeArticle.mettreAJourDonnees(donnees);
    
    // Sélectionner l'article adéquat
    ArticleBase articleBase = getModele().getArticleSelectionne();
    if (articleBase != null) {
      int index = listeArticleBase.indexOf(articleBase);
      if (index >= 0 && index < tblListeArticle.getRowCount()) {
        tblListeArticle.setRowSelectionInterval(index, index);
      }
    }
  }
  
  // -- Méthodes protégées
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().lancerRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      tfTexteRecherche.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteRechercheFocusLost(FocusEvent e) {
    try {
      try {
        getModele().modifierTexteRecherche(tfTexteRecherche.getText());
      }
      catch (Exception exception) {
        DialogueErreur.afficher(exception);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteRechercheActionPerformed(ActionEvent e) {
    try {
      getModele().modifierTexteRecherche(tfTexteRecherche.getText());
      getModele().lancerRecherche();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeArticleValueChanged(ListSelectionEvent e) {
    try {
      if (!isEvenementsActifs() && !e.getValueIsAdjusting()) {
        return;
      }
      ArticleBase articleBase = null;
      int index = tblListeArticle.getSelectedRow();
      if (index >= 0 && index < tblListeArticle.getRowCount()) {
        int indexModele = tblListeArticle.convertRowIndexToModel(index);
        if (indexModele >= 0 || indexModele < getModele().getListeArticleBase().size()) {
          articleBase = getModele().getListeArticleBase().get(indexModele);
        }
      }
      getModele().setArticleSelectionne(articleBase);
      rafraichirBouton();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpListeArticleStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeArticle == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeArticle.getViewport().getViewRect();
      int premiereLigne = tblListeArticle.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeArticle.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeArticle.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageArticleBaseAffiches(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * La ligne sélectionnée a changée dans le tableau résultat.
   */
  private void tblListeArticleSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      // Informer le modèle
      getModele().modifierLigneSelectionnee(tblListeArticle.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbHorsGammeItemStateChanged(ItemEvent e) {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      if (cbHorsGamme.getSelectedItem() != null && cbHorsGamme.getSelectedItem() instanceof EnumFiltreHorsGamme) {
        getModele().modifierHorsGamme((EnumFiltreHorsGamme) cbHorsGamme.getSelectedItem());
      }
      else {
        getModele().modifierHorsGamme(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeArticleMouseClicked(MouseEvent e) {
    try {
      getModele().modifierLigneSelectionnee(tblListeArticle.getIndiceSelection());
      if (e.getClickCount() == 2) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbSpeciauxItemStateChanged(ItemEvent e) {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      if (cbSpeciaux.getSelectedItem() != null && cbSpeciaux.getSelectedItem() instanceof EnumFiltreArticleSpecial) {
        getModele().modifierSpecial((EnumFiltreArticleSpecial) cbSpeciaux.getSelectedItem());
      }
      else {
        getModele().modifierSpecial(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void cbConsigneItemStateChanged(ItemEvent e) {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      if (cbConsigne.getSelectedItem() != null && cbConsigne.getSelectedItem() instanceof EnumFiltreArticleConsigne) {
        getModele().modifierConsigne((EnumFiltreArticleConsigne) cbConsigne.getSelectedItem());
      }
      else {
        getModele().modifierConsigne(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snFamilleValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierFamille(snFamille.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbCommentaireItemStateChanged(ItemEvent e) {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      if (cbCommentaire.getSelectedItem() != null && cbCommentaire.getSelectedItem() instanceof EnumFiltreArticleCommentaire) {
        getModele().modifierCommentaire((EnumFiltreArticleCommentaire) cbCommentaire.getSelectedItem());
      }
      else {
        getModele().modifierConsigne(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new JPanel();
    pnlRecherche = new JPanel();
    pnlRechercheGauche = new JPanel();
    lbTexteRecherche = new SNLabelChamp();
    tfTexteRecherche = new SNTexte();
    lbFamille = new SNLabelChamp();
    snFamille = new SNFamille();
    lbHorsGamme = new SNLabelChamp();
    cbHorsGamme = new SNComboBox();
    lbCommentaire = new SNLabelChamp();
    cbCommentaire = new SNComboBox();
    pnlRechercheDroite = new JPanel();
    lbSpecial = new SNLabelChamp();
    cbSpeciaux = new SNComboBox();
    lbConsigne = new SNLabelChamp();
    cbConsigne = new SNComboBox();
    snBarreRecherche = new SNBarreRecherche();
    lbTitreListeArticle = new SNLabelTitre();
    scpListeArticle = new JScrollPane();
    tblListeArticle = new NRiTable();
    snBarreBouton = new SNBarreBouton();

    // ======== this ========
    setMinimumSize(new Dimension(1000, 600));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("S\u00e9lection d'un article");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setPreferredSize(new Dimension(950, 400));
        pnlContenu.setMinimumSize(new Dimension(950, 350));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 37, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };

        // ======== pnlRecherche ========
        {
          pnlRecherche.setOpaque(false);
          pnlRecherche.setBorder(new TitledBorder(""));
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridLayout(1, 2, 5, 5));

          // ======== pnlRechercheGauche ========
          {
            pnlRechercheGauche.setOpaque(false);
            pnlRechercheGauche.setBorder(null);
            pnlRechercheGauche.setName("pnlRechercheGauche");
            pnlRechercheGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWidths = new int[] { 165, 300, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };

            // ---- lbTexteRecherche ----
            lbTexteRecherche.setText("Recherche g\u00e9n\u00e9rique");
            lbTexteRecherche.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTexteRecherche.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTexteRecherche.setPreferredSize(new Dimension(100, 30));
            lbTexteRecherche.setName("lbTexteRecherche");
            pnlRechercheGauche.add(lbTexteRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- tfTexteRecherche ----
            tfTexteRecherche.setName("tfTexteRecherche");
            tfTexteRecherche.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                tfTexteRechercheActionPerformed(e);
              }
            });
            tfTexteRecherche.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                tfTexteRechercheFocusGained(e);
              }

              @Override
              public void focusLost(FocusEvent e) {
                tfTexteRechercheFocusLost(e);
              }
            });
            pnlRechercheGauche.add(tfTexteRecherche, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

            // ---- lbFamille ----
            lbFamille.setText("Famille");
            lbFamille.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFamille.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFamille.setPreferredSize(new Dimension(100, 30));
            lbFamille.setName("lbFamille");
            pnlRechercheGauche.add(lbFamille, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- snFamille ----
            snFamille.setName("snFamille");
            snFamille.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snFamilleValueChanged(e);
              }
            });
            pnlRechercheGauche.add(snFamille, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            // ---- lbHorsGamme ----
            lbHorsGamme.setText("Hors gamme");
            lbHorsGamme.setHorizontalAlignment(SwingConstants.RIGHT);
            lbHorsGamme.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbHorsGamme.setPreferredSize(new Dimension(100, 30));
            lbHorsGamme.setName("lbHorsGamme");
            pnlRechercheGauche.add(lbHorsGamme, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- cbHorsGamme ----
            cbHorsGamme.setName("cbHorsGamme");
            cbHorsGamme.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbHorsGammeItemStateChanged(e);
              }
            });
            pnlRechercheGauche.add(cbHorsGamme, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            // ---- lbCommentaire ----
            lbCommentaire.setText("Commentaire");
            lbCommentaire.setName("lbCommentaire");
            pnlRechercheGauche.add(lbCommentaire, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

            // ---- cbCommentaire ----
            cbCommentaire.setName("cbCommentaire");
            cbCommentaire.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbCommentaireItemStateChanged(e);
              }
            });
            pnlRechercheGauche.add(cbCommentaire, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheGauche);

          // ======== pnlRechercheDroite ========
          {
            pnlRechercheDroite.setOpaque(false);
            pnlRechercheDroite.setName("pnlRechercheDroite");
            pnlRechercheDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };

            // ---- lbSpecial ----
            lbSpecial.setText("Sp\u00e9cial");
            lbSpecial.setHorizontalAlignment(SwingConstants.RIGHT);
            lbSpecial.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbSpecial.setPreferredSize(new Dimension(100, 30));
            lbSpecial.setName("lbSpecial");
            pnlRechercheDroite.add(lbSpecial, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- cbSpeciaux ----
            cbSpeciaux.setName("cbSpeciaux");
            cbSpeciaux.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbSpeciauxItemStateChanged(e);
              }
            });
            pnlRechercheDroite.add(cbSpeciaux, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            // ---- lbConsigne ----
            lbConsigne.setText("Consign\u00e9");
            lbConsigne.setHorizontalAlignment(SwingConstants.RIGHT);
            lbConsigne.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbConsigne.setPreferredSize(new Dimension(100, 30));
            lbConsigne.setName("lbConsigne");
            pnlRechercheDroite.add(lbConsigne, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- cbConsigne ----
            cbConsigne.setName("cbConsigne");
            cbConsigne.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbConsigneItemStateChanged(e);
              }
            });
            pnlRechercheDroite.add(cbConsigne, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlRechercheDroite.add(snBarreRecherche, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheDroite);
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ---- lbTitreListeArticle ----
        lbTitreListeArticle.setText("Articles correspondants \u00e0 votre recherche");
        lbTitreListeArticle.setFont(lbTitreListeArticle.getFont().deriveFont(lbTitreListeArticle.getFont().getStyle() | Font.BOLD,
            lbTitreListeArticle.getFont().getSize() + 2f));
        lbTitreListeArticle.setPreferredSize(new Dimension(200, 30));
        lbTitreListeArticle.setMinimumSize(new Dimension(200, 30));
        lbTitreListeArticle.setMaximumSize(new Dimension(200, 30));
        lbTitreListeArticle.setName("lbTitreListeArticle");
        pnlContenu.add(lbTitreListeArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));

        // ======== scpListeArticle ========
        {
          scpListeArticle.setPreferredSize(new Dimension(950, 424));
          scpListeArticle.setName("scpListeArticle");

          // ---- tblListeArticle ----
          tblListeArticle.setShowVerticalLines(true);
          tblListeArticle.setShowHorizontalLines(true);
          tblListeArticle.setBackground(Color.white);
          tblListeArticle.setRowHeight(20);
          tblListeArticle.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeArticle.setSelectionBackground(new Color(57, 105, 138));
          tblListeArticle.setGridColor(new Color(204, 204, 204));
          tblListeArticle.setPreferredSize(new Dimension(950, 220));
          tblListeArticle.setName("tblListeArticle");
          tblListeArticle.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeArticleMouseClicked(e);
            }
          });
          scpListeArticle.setViewportView(tblListeArticle);
        }
        pnlContenu.add(scpListeArticle, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);

      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlContenu;
  private JPanel pnlRecherche;
  private JPanel pnlRechercheGauche;
  private SNLabelChamp lbTexteRecherche;
  private SNTexte tfTexteRecherche;
  private SNLabelChamp lbFamille;
  private SNFamille snFamille;
  private SNLabelChamp lbHorsGamme;
  private SNComboBox cbHorsGamme;
  private SNLabelChamp lbCommentaire;
  private SNComboBox cbCommentaire;
  private JPanel pnlRechercheDroite;
  private SNLabelChamp lbSpecial;
  private SNComboBox cbSpeciaux;
  private SNLabelChamp lbConsigne;
  private SNComboBox cbConsigne;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lbTitreListeArticle;
  private JScrollPane scpListeArticle;
  private NRiTable tblListeArticle;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
