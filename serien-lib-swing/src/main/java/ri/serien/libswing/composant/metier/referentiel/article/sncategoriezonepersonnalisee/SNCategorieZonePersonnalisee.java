/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.sncategoriezonepersonnalisee;

import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.CategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.IdCategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.ListeCategorieZonePersonnalisee;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une catégorie de zone personnalisée longue.
 *
 * Ce composant doit être utilisé pour tout affichage et choix de zone personnalisée longue dans le logiciel.
 * Il gère l'affichage des catégories de zones personnalisées longues existantes et la sélection de l'une d'entre elles.
 *
 * Utilisation :
 * - Ajouter un composant SNCategorieZonePersonnalisee à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le RGVX08FM_A4.
 */
public class SNCategorieZonePersonnalisee
    extends SNComboBoxObjetMetier<IdCategorieZonePersonnalisee, CategorieZonePersonnalisee, ListeCategorieZonePersonnalisee> {
  private ListeCategorieZonePersonnalisee listeCategorieZonePersonnalisee = null;
  
  /**
   * Constructeur par défaut.
   */
  public SNCategorieZonePersonnalisee() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeCategorieZonePersonnalisee());
  }
  
  /**
   * Sélectionner une catégorie de zone personnalisée longue de la liste déroulante à partir des champs RPG.
   * La méthode retourne true si la sélection a été modifiée ou false dans le cas inverse.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampCategorieZonePersonnalisee) {
    if (pChampCategorieZonePersonnalisee == null || pLexical.HostFieldGetData(pChampCategorieZonePersonnalisee) == null) {
      throw new MessageErreurException("Impossible de sélectionner la catégorie de zone personnalisée car son code est invalide.");
    }
    
    IdCategorieZonePersonnalisee idCategorieZonePersonnalisee = null;
    String valeurCategorieZonePersonnalisee = pLexical.HostFieldGetData(pChampCategorieZonePersonnalisee);
    if (valeurCategorieZonePersonnalisee != null && !valeurCategorieZonePersonnalisee.trim().isEmpty()) {
      idCategorieZonePersonnalisee =
          IdCategorieZonePersonnalisee.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampCategorieZonePersonnalisee));
    }
    
    setIdSelection(idCategorieZonePersonnalisee);
  }
  
  /**
   * Renseigner les champs RPG correspondant à la catégorie de zone personnalisée longue sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampCategorieZonePersonnalisee) {
    CategorieZonePersonnalisee categorieZP = getSelection();
    if (categorieZP != null) {
      pLexical.HostFieldPutData(pChampCategorieZonePersonnalisee, 0, categorieZP.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampCategorieZonePersonnalisee, 0, "");
    }
  }
  
}
