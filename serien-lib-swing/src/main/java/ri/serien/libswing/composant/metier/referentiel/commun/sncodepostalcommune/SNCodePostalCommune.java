/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.codepostalcommune.ListeCodePostalCommune;
import ri.serien.libcommun.gescom.vente.commune.CritereCommune;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.SessionBase;

/**
 * Composant d'affichage et de sélection d'un code postal et/ou d'une ville.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un code postal ou d'une ville dans le logiciel. Il gère l'affichage d'un
 * code postal ainsi que la ville associé et la sélection de ceux-ci.
 * Il comprend deux combos contenant respectivement le code postal et la ville.
 * L'utilisateur pourra aussi renseigner un code postal et/ou une ville qui n'existe pas dans la BDD
 * La liste de la combobox sera vide tant que l'utilisateur n'aura rentré aucune donnée, la liste sera triée au fur et à mesure.
 *
 * Note :
 * Lors de l'utilisation de ce composant dans un paenl, la méthode snCodePostalCommuneValueChanged doit contenir en plus du test avec
 * !isEvenementsActifs(), le test avec !getModele().isDonneesChargees(). Car lors de l'initialisation des données la valeur d'origine de
 * snCodePostalCommune peut être modifiée (voir ri.serien.desktop.metier.gescom.comptoir.changementclient.VueChangementClient).
 */
public class SNCodePostalCommune extends SNComposantAvecEtablissement {
  // Constantes
  private static final int NOMBRE_ELEMENT_MAX = 15;
  private static final int FOCUS_HORS_COMPOSANT = 0;
  private static final int FOCUS_SUR_CODEPOSTAL = 1;
  private static final int FOCUS_SUR_VILLE = 2;
  
  // Variables
  private int aLeFocus = FOCUS_HORS_COMPOSANT;
  private boolean evenementActif = true;

  private CritereCommune critereCommune = new CritereCommune();
  private ListeCodePostalCommune listeCodePostalCommune = new ListeCodePostalCommune();
  private ListeCodePostalCommune listeCodePostalCommuneAAfficher = new ListeCodePostalCommune();
  private ArrayList<String> listeCodePostalAAfficher = new ArrayList<String>();
  private ArrayList<String> listeCommuneAAfficher = new ArrayList<String>();
  private String texteSaisiCodePostal = "";
  private String texteSaisiVille = "";
  private boolean desactivationRecherche = false;
  private CodePostalCommune codePostalCommune = null;

  /**
   * Constructeur.
   */
  public SNCodePostalCommune() {
    super();
    initComponents();

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    
    cbCodePostal.setSelectedItem("");
    cbVille.setSelectedItem("");

    // Permet l'affichage de 15 élèments maximum
    cbCodePostal.setMaximumRowCount(NOMBRE_ELEMENT_MAX);
    cbVille.setMaximumRowCount(NOMBRE_ELEMENT_MAX);

    // Gestion du focus sur la combo code postal
    JTextField tfSaisieCodePostal = (JTextField) cbCodePostal.getEditor().getEditorComponent();
    tfSaisieCodePostal.setName("tfCodePostal");
    tfSaisieCodePostal.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        cbCodePostalFocusLost(e);
      }

      @Override
      public void focusGained(FocusEvent e) {
        aLeFocus = FOCUS_SUR_CODEPOSTAL;
        ouvrirListeCodePostal();
      }
    });

    // Gestion du clavier sur la combobox code postal.
    tfSaisieCodePostal.addKeyListener(new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (!evenementActif || isToucheDesactivantEvenement(e)) {
          return;
        }
        JTextField tfSaisieCodePostal = (JTextField) cbCodePostal.getEditor().getEditorComponent();
        modifierTexteCodePostal(tfSaisieCodePostal.getText());
        if (!isToucheAIgnorer(e)) {
          fireValueChanged();
        }
      }

      @Override
      public void keyPressed(KeyEvent e) {
      }
    });

    // Gestion du focus sur la combobox ville.
    JTextField tfSaisieVille = (JTextField) cbVille.getEditor().getEditorComponent();
    tfSaisieVille.setName("tfSaisieVille");
    tfSaisieVille.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        cbVilleFocusLost(e);
      }

      @Override
      public void focusGained(FocusEvent e) {
        aLeFocus = FOCUS_SUR_VILLE;
        ouvrirListeVille();
      }
    });

    // Gestion du clavier sur la combo ville.
    // Listener pour lancer la recherche sur la touche entrée
    tfSaisieVille.addKeyListener(new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
      }

      @Override
      public void keyReleased(KeyEvent e) {
        if (!evenementActif || isToucheDesactivantEvenement(e)) {
          return;
        }
        JTextField tfSaisieVille = (JTextField) cbVille.getEditor().getEditorComponent();
        modifierTexteVille(tfSaisieVille.getText());
        if (!isToucheAIgnorer(e)) {
          fireValueChanged();
        }
      }

      @Override
      public void keyPressed(KeyEvent e) {
      }
    });
  }
  
  /**
   * Initialisation de la session.
   */
  @Override
  public void setSession(SessionBase pSession) {
    super.setSession(pSession);
  }
  
  /**
   * Activation du composant.
   */
  @Override
  public void setEnabled(boolean pIsEnable) {
    cbCodePostal.setEnabled(pIsEnable);
    cbVille.setEnabled(pIsEnable);
  }
  
  /**
   * Mettre le focus sur la combo code postal.
   */
  @Override
  public void requestFocus() {
    cbCodePostal.requestFocus();
  }
  
  /**
   * Chargement des données.
   */
  public void charger(boolean pForcerRafraichissement) {
    // Tester s'il faut rafraîchir la liste
    if (!pForcerRafraichissement) {
      return;
    }
    effacerVariable();
    lancerRecherche();
    rafraichir();
  }

  // -- Méthodes publiques

  /**
   * Désactive la recherche sur le couple code postal / ville (cas des pays étrangers).
   */
  public void setDesactivationRecherche(boolean pDesactivationRecherche) {
    desactivationRecherche = pDesactivationRecherche;
  }

  /**
   * Modifier le codePostalCommune sélectionné.
   */
  public void setSelection(CodePostalCommune pCodePostalCommune) {
    if (pCodePostalCommune == null) {
      effacerVariable();
      return;
    }
    initialiserValeur(pCodePostalCommune.getCodePostal(), pCodePostalCommune.getVille());
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Retourne le codePostalCommune selectionné.
   */
  public CodePostalCommune getSelection() {
    return CodePostalCommune.getInstance(texteSaisiCodePostal, texteSaisiVille);
  }
  
  /**
   * Sélectionner un code postal et une ville à partir des champs RPG.
   */
  public boolean setSelectionParChampRPG(Lexical pLexical, String pChampCodePostal, String pChampVille) {
    if (pChampCodePostal == null || pLexical.HostFieldGetData(pChampCodePostal) == null) {
      throw new MessageErreurException("Impossible de sélectionner le code postal car il est invalide.");
    }
    if (pChampVille == null || pLexical.HostFieldGetData(pChampVille) == null) {
      throw new MessageErreurException("Impossible de sélectionner la ville car son nom est invalide.");
    }
    
    // Formatage du code postal
    String codePostal = Constantes.normerTexte(pLexical.HostFieldGetData(pChampCodePostal));
    if (codePostal.matches("[0-9]+")) {
      codePostal = String.format("%05d", Constantes.convertirTexteEnInteger(codePostal));
    }
    modifierTexteCodePostal(codePostal);
    modifierTexteVille(pLexical.HostFieldGetData(pChampVille));
    
    return true;
  }
  
  /**
   * Renseigner les champs RPG correspondant au code postal et à la ville sélectionné.
   * Cette méthode permet de spécifier la valeur qui est envoyée au programme RPG comme valeur "Tous" :
   * - Si pTousAvecEtoile est à false, le champ RPG contient des espaces lorsque "Tous" est sélectionné".
   * - Si pTousAvecEtoile est à true, le champ RPG contient "**" lorsque "Tous" est sélectionné".
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampCodePostal, String pChampVille, boolean pTousAvecEtoile) {
    CodePostalCommune collectivite = getSelection();
    if (collectivite.getCodePostal() != null || collectivite.getVille() != null) {
      pLexical.HostFieldPutData(pChampCodePostal, 0, collectivite.getCodePostal());
      pLexical.HostFieldPutData(pChampVille, 0, collectivite.getVille());
    }
    else if (pTousAvecEtoile) {
      pLexical.HostFieldPutData(pChampCodePostal, 0, "**");
      pLexical.HostFieldPutData(pChampVille, 0, "**");
    }
    else {
      pLexical.HostFieldPutData(pChampCodePostal, 0, "");
      pLexical.HostFieldPutData(pChampVille, 0, "");
    }
  }

  // -- Méthodes privées
  
  /**
   * Contrôle que les touches actionnées soient celles qui ne doivent pas déclencher l'évènement.
   * Les touches flèches haut et bas qui permettent de sélectionner un élément dans l'une des 2 listes.
   */
  private boolean isToucheDesactivantEvenement(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
      return true;
    }
    return false;
  }
  
  /**
   * Initialise le code postal et la ville.
   */
  private void initialiserValeur(String pCodePostal, String pVille) {
    effacerVariable();
    texteSaisiCodePostal = Constantes.normerTexte(pCodePostal);
    texteSaisiVille = Constantes.normerTexte(pVille);
  }
  
  /**
   * Rafraichir l'affichage du composant.
   */
  private void rafraichir() {
    evenementActif = false;
    rafraichirListeCodePostal();
    rafraichirListeVille();
    evenementActif = true;
  }
  
  // -- Méthodes privées
  
  /**
   * Effacer les variables métiers.
   */
  private void effacerVariable() {
    critereCommune.initialiser();
    texteSaisiCodePostal = "";
    texteSaisiVille = "";
    listeCodePostalCommune.clear();
    listeCodePostalCommuneAAfficher.clear();
    listeCodePostalAAfficher.clear();
    listeCommuneAAfficher.clear();
    rafraichir();
  }

  /**
   * Rafraichir la liste des codes postaux.
   */
  private void rafraichirListeCodePostal() {
    // Récupération de la position du curseur avant rafraichissement des données
    JTextField tfSaisieCodePostal = (JTextField) cbCodePostal.getEditor().getEditorComponent();
    String codePostalAvant = tfSaisieCodePostal.getText();
    int positionCurseur = tfSaisieCodePostal.getCaretPosition();

    // Remplissage de la liste déroulante
    cbCodePostal.removeAllItems();
    cbCodePostal.addItem(" ");
    for (String codePostal : listeCodePostalAAfficher) {
      cbCodePostal.addItem(codePostal);
    }
    // Sélection
    cbCodePostal.setSelectedItem(texteSaisiCodePostal);
    ouvrirListeCodePostal();

    // Positionnement du curseur
    if (codePostalAvant.equals(tfSaisieCodePostal.getText())) {
      tfSaisieCodePostal.setCaretPosition(positionCurseur);
    }
  }

  /**
   * Rafraichir la liste des villes.
   */
  private void rafraichirListeVille() {
    // Rafraichissement de l'affichage
    JTextField tfSaisieVille = (JTextField) cbVille.getEditor().getEditorComponent();
    String villeAvant = tfSaisieVille.getText();
    int positionCurseur = tfSaisieVille.getCaretPosition();

    cbVille.removeAllItems();
    cbVille.addItem(" ");
    for (String commune : listeCommuneAAfficher) {
      cbVille.addItem(commune);
    }
    // Sélection
    cbVille.setSelectedItem(texteSaisiVille);
    ouvrirListeVille();
    
    // Positionnement du curseur
    if (villeAvant.equals(tfSaisieVille.getText())) {
      tfSaisieVille.setCaretPosition(positionCurseur);
    }
  }

  /**
   * Gestion de l'ouverture de la liste déroulante des codes postaux.
   */
  private void ouvrirListeCodePostal() {
    // Si le focus n'est pas sur le code postal, sa liste déroulante est fermée
    if (aLeFocus != FOCUS_SUR_CODEPOSTAL) {
      cbCodePostal.hidePopup();
      return;
    }

    // Ouverture de la liste déroulante et ajustement de sa taille en fonction du nombre d'éléments
    if (cbCodePostal.getItemCount() > 1) {
      int nombreElement = cbCodePostal.getItemCount();
      // La popup est cachée dans le particulier où il y a 2 éléments : un vide et un qui est déjà dans le texte saisi
      if (nombreElement == 2 && cbCodePostal.getItemAt(1).equals(texteSaisiCodePostal)) {
        cbCodePostal.hidePopup();
        return;
      }
      if (nombreElement > NOMBRE_ELEMENT_MAX) {
        nombreElement = NOMBRE_ELEMENT_MAX;
      }
      cbCodePostal.setMaximumRowCount(nombreElement);
      cbCodePostal.showPopup();
    }
    else {
      cbCodePostal.hidePopup();
    }
  }
  
  /**
   * Gestion de l'ouverture de la liste déroulante des villes.
   */
  private void ouvrirListeVille() {
    // Si le focus n'est pas sur la ville, sa liste déroulante est fermée
    if (aLeFocus != FOCUS_SUR_VILLE) {
      cbVille.hidePopup();
      return;
    }
    
    // Ouverture de la liste déroulante et ajustement de sa taille en fonction du nombre d'éléments
    if (cbVille.getItemCount() > 1) {
      int nombreElement = cbVille.getItemCount();
      // La popup est cachée dans le particulier où il y a 2 éléments : un vide et un qui est déjà dans le texte saisi
      if (nombreElement == 2 && cbVille.getItemAt(1).equals(texteSaisiVille)) {
        cbVille.hidePopup();
        return;
      }
      if (nombreElement > NOMBRE_ELEMENT_MAX) {
        nombreElement = NOMBRE_ELEMENT_MAX;
      }
      cbVille.setMaximumRowCount(nombreElement);
      cbVille.showPopup();
    }
    else {
      cbVille.hidePopup();
    }
  }

  /**
   * Tester si la touche appuyée est à traiter.
   */
  private boolean isToucheAIgnorer(KeyEvent e) {
    return e.getKeyCode() != KeyEvent.VK_DOWN && e.getKeyCode() != KeyEvent.VK_UP & e.getKeyCode() != KeyEvent.VK_LEFT
        && e.getKeyCode() != KeyEvent.VK_RIGHT && e.getKeyCode() != KeyEvent.VK_HOME && e.getKeyCode() != KeyEvent.VK_SHIFT
        && e.getKeyCode() != KeyEvent.VK_ESCAPE;
  }

  /**
   * Modifier le code postal à rechercher.
   */
  private void modifierTexteCodePostal(String pCodePostal) {
    pCodePostal = Constantes.normerTexte(pCodePostal);
    if (texteSaisiCodePostal.equals(pCodePostal)) {
      return;
    }
    if (pCodePostal.length() > CodePostalCommune.LONGUEUR_CODEPOSTAL) {
      rafraichir();
      return;
    }
    texteSaisiCodePostal = pCodePostal;
    lancerRecherche();
    rafraichir();
  }
  
  /**
   * Modifier la ville à rechercher.
   */
  private void modifierTexteVille(String pVille) {
    pVille = Constantes.normerTexte(pVille).toUpperCase();
    if (texteSaisiVille.equals(pVille)) {
      return;
    }
    if (pVille.length() > CodePostalCommune.LONGUEUR_VILLE) {
      rafraichir();
      return;
    }
    texteSaisiVille = pVille;
    lancerRecherche();
    rafraichir();
  }
  
  /**
   * Lancer une recherche sur les codePostalCommune.
   */
  private void lancerRecherche() {
    if (desactivationRecherche) {
      return;
    }
    // Nettoyage des listes avant de lancer une recherche
    listeCodePostalCommuneAAfficher.clear();
    listeCodePostalAAfficher.clear();
    listeCommuneAAfficher.clear();
    
    // Ne rien faire :
    // - si la longueur du code postal est inférieure à 2 caractères et que le champ a le focus et que le champ ville est vide
    // - si la longueur de la ville est inférieure à 2 caractères et que le champ a le focus et que le champ code postal est vide
    // - si le texte à rechercher est vide
    if ((texteSaisiCodePostal.length() < 2 && aLeFocus == FOCUS_SUR_CODEPOSTAL && texteSaisiVille.trim().isEmpty())
        || (texteSaisiVille.length() < 2 && aLeFocus == FOCUS_SUR_VILLE && texteSaisiCodePostal.trim().isEmpty())
        || (texteSaisiCodePostal.trim().isEmpty() && texteSaisiVille.trim().isEmpty())) {
      return;
    }

    critereCommune.initialiser();
    // Mise à jour des critères de recherche
    if (!texteSaisiCodePostal.trim().isEmpty() && !texteSaisiCodePostal.trim().equals("0")) {
      critereCommune.setCodePostalFormate(texteSaisiCodePostal);
    }
    critereCommune.setNomCommune(texteSaisiVille);

    // Lancer une recherche de CodePostalCommune
    listeCodePostalCommune = ListeCodePostalCommune.chargerListeCodePostalCommune(getIdSession(), critereCommune);

    // Filtre du résultat pour ne pas avoir de doublons dans l'affichage des combos
    filtrerListe();
  }

  /**
   * Tri la recherche des code postaux afin de n'afficher que ceux correspondants à la saisie de l'utilisateur.
   */
  private void filtrerListe() {
    // Remise à vide des listes d'affichage afin d'éviter qu'un ancien traitement n'interfère
    listeCodePostalCommuneAAfficher.clear();
    listeCodePostalAAfficher.clear();
    listeCommuneAAfficher.clear();

    if (listeCodePostalCommune == null || listeCodePostalCommune.isEmpty()) {
      return;
    }

    // Chargement de la liste des CodePostalCommune à afficher
    // Si le critère de recherche est sur le code postal
    if (critereCommune.getCodePostalFormate() != null && !critereCommune.getCodePostalFormate().trim().isEmpty()) {
      for (CodePostalCommune codePostalCommune : listeCodePostalCommune) {
        if (codePostalCommune.getCodePostal().startsWith(critereCommune.getCodePostalFormate())) {
          listeCodePostalCommuneAAfficher.add(codePostalCommune);
        }
      }
    }
    // Si le critère de recherche est sur la ville
    else if (critereCommune.getNomCommune() != null && !critereCommune.getNomCommune().trim().isEmpty()) {
      for (CodePostalCommune codePostalCommune : listeCodePostalCommune) {
        if (codePostalCommune.getVille().startsWith(critereCommune.getNomCommune())) {
          listeCodePostalCommuneAAfficher.add(codePostalCommune);
        }
      }
    }
    
    // Trie de la liste des CodePostalCommune à afficher
    listeCodePostalCommuneAAfficher.trier();
    
    // Remplissage des listes des codes postaux et des villes à partir de la liste des couples code postal / ville
    for (CodePostalCommune codePostalCommune : listeCodePostalCommuneAAfficher) {
      if (!listeCodePostalAAfficher.contains(codePostalCommune.getCodePostal())) {
        listeCodePostalAAfficher.add(codePostalCommune.getCodePostal());
      }
      if (!listeCommuneAAfficher.contains(codePostalCommune.getVille())) {
        listeCommuneAAfficher.add(codePostalCommune.getVille());
      }
    }
  }

  /**
   * Met à jour le code postal.
   */
  private void mettreAJourCodePostal() {
    // Indique que le focus n'est plus théoriquement sur le composant
    aLeFocus = FOCUS_HORS_COMPOSANT;
    // Mise à jour du code postal saisi
    modifierTexteCodePostal((String) cbCodePostal.getSelectedItem());
    // Mise à jour de la ville uniquement si la ville est vide et s'il n'y a qu'un seul couple code postal / ville
    if (listeCodePostalCommune != null && listeCommuneAAfficher.size() == 1 && texteSaisiVille.isEmpty()) {
      modifierTexteVille(listeCommuneAAfficher.get(0));
    }
    // Quand on modifie le code postal mais qu'un CodePostalCommune était précédement sélectionné, on sélectionne la ville pour faire
    // apparaitre qu'il faut éventuellement la modifier
    else if (!texteSaisiVille.isEmpty()) {
      JTextField tfSaisieVille = (JTextField) cbVille.getEditor().getEditorComponent();
      tfSaisieVille.setCaretPosition(0);
      tfSaisieVille.selectAll();
    }
    
    // Déclenche l'évènement ValueChanged du composant
    validerValeurCodePostalCommune();
  }

  /**
   * Met à jour le ville.
   */
  private void mettreAJourVille() {
    // Indique que le focus n'est plus théoriquement sur le composant
    aLeFocus = FOCUS_HORS_COMPOSANT;
    // Mise à jour de la ville saisie
    modifierTexteVille((String) cbVille.getSelectedItem());
    
    // Déclenche l'évènement ValueChanged du composant
    validerValeurCodePostalCommune();
  }

  /**
   * Déclenche l'évènement ValueChanged du composant SNCodePostalCommune.
   */
  private void validerValeurCodePostalCommune() {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        if (aLeFocus != FOCUS_HORS_COMPOSANT) {
          return;
        }
        // Déclenche l'évènement si le composant n'a plus le focus sur aucun de ses composants
        fireValueChanged();
      }
    });
  }

  // -- Méthodes évènementielles

  /**
   * Mettre à jour l'élément sélectionné si la combo code postal perd le focus.
   */
  private void cbCodePostalFocusLost(FocusEvent e) {
    try {
      if (!evenementActif) {
        return;
      }
      mettreAJourCodePostal();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Mettre à jour l'élément sélectionné si la combo ville perd le focus.
   */
  private void cbVilleFocusLost(FocusEvent e) {
    try {
      if (!evenementActif) {
        return;
      }
      mettreAJourVille();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Mettre à jour l'élément sélectionné si le code postal est sélectionné.
   */
  private void cbCodePostalItemStateChanged(ItemEvent e) {
    try {
      if (!evenementActif) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED) {
        mettreAJourCodePostal();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Mettre à jour l'élément sélectionné si la ville est sélectionnée.
   */
  private void cbVilleItemStateChanged(ItemEvent e) {
    try {
      if (!evenementActif) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED) {
        mettreAJourVille();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    cbCodePostal = new SNComboBox();
    cbVille = new SNComboBox();
    
    // ======== this ========
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
    
    // ---- cbCodePostal ----
    cbCodePostal.setEditable(true);
    cbCodePostal.setMaximumSize(new Dimension(100, 30));
    cbCodePostal.setAutoscrolls(true);
    cbCodePostal.setName("cbCodePostal");
    cbCodePostal.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        cbCodePostalItemStateChanged(e);
      }
    });
    add(cbCodePostal,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
    
    // ---- cbVille ----
    cbVille.setEditable(true);
    cbVille.setName("cbVille");
    cbVille.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        cbVilleItemStateChanged(e);
      }
    });
    add(cbVille,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNComboBox cbCodePostal;
  private SNComboBox cbVille;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
