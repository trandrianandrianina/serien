/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.contact.listecontacttiers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.CategorieContact;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue permettant de lister les contacts d'un tiers (client ou fournisseur).
 */
public class DialogueListeContactTiers extends AbstractVueDialogue<ModeleListeContactTiers> {
  // Constantes
  public static final String[] TITRE_LISTE = new String[] { "Code", "Nom", "Fonction", "Mail", "Téléphone", "Princ" };
  public static final String RECHERCHE_GENERIQUE_TOOLTIP = "<html>Recherche sur :<br>\r\n" + "- numéro contact (entier)<br>\r\n"
      + "- nom (milieu)<br>\r\n" + "- prénom (milieu)<br>\r\n" + "- mot de classement (milieu)<br>\r\n" + "</html>";
  private static final String BOUTON_CREER_CONTACT = "Créer contact";
  private static final String BOUTON_REDIGER_MAIL = "Rédiger mail";
  private static final String BOUTON_METTRE_PRINCIPAL = "Mettre en principal";
  private static final String BOUTON_SUPPRIMER_LIEN = "Supprimer lien contact";

  // Variables
  private int derniereLigneSelectionnee = 0;

  /**
   * Constructeur.
   */
  public DialogueListeContactTiers(ModeleListeContactTiers pModele) {
    super(pModele);
  }

  // -- Méthodes publiques

  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();

    // Ne pas permettre le redimensionnement
    setResizable(false);

    // Configurer le composant de saisie de texte
    tfRechercheGenerique.setToolTipText(DialogueListeContactTiers.RECHERCHE_GENERIQUE_TOOLTIP);

    // Configurer le tableau
    int[] justification =
        new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.CENTRE };
    int[] dimension = new int[] { 100, 300, 150, 250, 110, 40 };
    int[] dimensionMax = new int[] { 100, 300, 150, -1, 110, 40 };
    tblListeContact.personnaliserAspect(TITRE_LISTE, dimension, dimensionMax, justification, 13);

    // Redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        tblListeContactEnterKey();
      }
    };
    tblListeContact.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);

    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeContact.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeContactSelectionChanged(e);
      }
    });

    // Configurer le fond du tableau en blanc
    scpListeContact.getViewport().setBackground(Color.WHITE);

    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CREER_CONTACT, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_REDIGER_MAIL, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_METTRE_PRINCIPAL, 'p', true);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER_LIEN, 's', true);
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });

    // Mettre le focus dans la recherche générique lors de l'affichage de la boite de dialogue
    tfRechercheGenerique.requestFocusInWindow();
  }

  /**
   * Rafraichir l'écran.
   */
  @Override
  public void rafraichir() {
    // Rafraichir les composants
    rafraichirTitreFenetre();
    rafraichirRechercheGenerique();
    rafraichirCategorie();
    rafraichirTitreListe();
    rafraichirListe();

    // Rafraichir les boutons
    rafraichirBoutonAfficherContact();
    rafraichirBoutonEnvoyerMail();
    rafraichirBoutonMettrePrincipal();
    rafraichirBoutonCreerContact();
    rafraichirBoutonSupprimerLien();
  }

  /**
   * Rafraichir le titre de la boîte de dialogue.
   */
  private void rafraichirTitreFenetre() {
    if (getModele().getTitreEcran() != null) {
      setTitle(getModele().getTitreEcran());
    }
    else {
      setTitle("");
    }
  }

  /**
   * Rafraichir le texte de la recherche générique.
   */
  private void rafraichirRechercheGenerique() {
    if (getModele().getRechercheGenerique() != null) {
      tfRechercheGenerique.setText(getModele().getRechercheGenerique());
    }
    else {
      tfRechercheGenerique.setText("");
    }

    tfRechercheGenerique.setEnabled(isDonneesChargees());
  }

  /**
   * Rafraichir la catégorie de contacts.
   * La catégorie de contacts sert à définir les fonctions.
   */
  private void rafraichirCategorie() {
    // Charger la liste déroulante avec les différentes fonctions du contact
    cbFonction.removeAllItems();
    cbFonction.addItem("Toutes les fonctions");
    if (getModele().getListeCatégorieContact() != null) {
      for (CategorieContact categorieContact : getModele().getListeCatégorieContact()) {
        cbFonction.addItem(categorieContact);
      }
    }

    // Sélectionner la fonction en cours de filtrage
    if (getModele().getCategorieContact() != null) {
      cbFonction.setSelectedItem(getModele().getCategorieContact());
    }
    else {
      cbFonction.setSelectedIndex(0);
    }
  }

  /**
   * Rafraichir le titre de la liste.
   */
  private void rafraichirTitreListe() {
    lbTitreListe.setMessage(getModele().getTitreListe());
  }

  /**
   * Rafraichir la liste des contacts.
   */
  private void rafraichirListe() {
    String[][] donnees = null;

    // Préparer les données pour le tableau
    List<Contact> listeContact = getModele().getListeContact();
    if (listeContact != null && !listeContact.isEmpty()) {
      donnees = new String[listeContact.size()][DialogueListeContactTiers.TITRE_LISTE.length];
      for (int ligne = 0; ligne < listeContact.size(); ligne++) {
        Contact contact = listeContact.get(ligne);
        if (contact != null) {
          donnees[ligne][0] = contact.getId().toString();

          // Rafraichir la civilité, le nom et le prénom (attention il ne s'agit pas ici du champ REPAC qui est ignoré)
          if (contact.getNomComplet() != null && !contact.getNomComplet().isEmpty()) {
            donnees[ligne][1] = contact.getNomComplet();
          }
          else {
            donnees[ligne][1] = "";
          }

          // Rafraichir la fonction
          if (contact.getIdCategorie() != null && getModele().getListeCatégorieContact() != null) {
            CategorieContact categorieContact = getModele().getListeCatégorieContact().get(contact.getIdCategorie());
            if (categorieContact != null) {
              donnees[ligne][2] = categorieContact.getLibelle();
            }
            else {
              donnees[ligne][2] = "";
            }
          }
          else {
            donnees[ligne][2] = "";
          }

          // Rafraichir le mail
          if (contact.getEmail() != null) {
            donnees[ligne][3] = contact.getEmail();
          }
          else {
            donnees[ligne][3] = "";
          }

          // Rafraichir le téléphone
          if (contact.getNumeroTelephone() != null) {
            donnees[ligne][4] = contact.getNumeroTelephone();
          }
          else {
            donnees[ligne][4] = "";
          }

          // Contact principal
          if (contact.isContactPrincipal(getModele().getIdClient()) || contact.isContactPrincipal(getModele().getIdFournisseur())) {
            donnees[ligne][5] = "✓";
          }
          else {
            donnees[ligne][5] = "";
          }
        }
      }
    }
    else {
      scpListeContact.getVerticalScrollBar().setValue(0);
    }

    // Mettre à jour le tableau
    tblListeContact.mettreAJourDonnees(donnees);

    // Sélectionner la ligne en cours
    int indice = getModele().getLigneSelectionnee();
    if (indice > -1 && indice < tblListeContact.getRowCount()) {
      tblListeContact.setRowSelectionInterval(indice, indice);
    }
    else {
      tblListeContact.clearSelection();
    }
  }

  /**
   * Rafraichir le bouton pour créer un contact.
   */
  private void rafraichirBoutonCreerContact() {
    // Toujours permettre la création d'un contact
    snBarreBouton.activerBouton(BOUTON_CREER_CONTACT, true);
  }

  /**
   * Rafraichir le bouton pour envoyer un mail à un contact.
   */
  private void rafraichirBoutonEnvoyerMail() {
    boolean actif = true;

    // Ne pas afficher si aucun contact n'est sélectionné
    if (getModele().getContactSelectionne() == null) {
      actif = false;
    }
    // Ne pas afficher si le contact sélectionné n'a pas d'adresse mail
    else if (getModele().getContactSelectionne().getEmail() == null || getModele().getContactSelectionne().getEmail().isEmpty()) {
      actif = false;
    }

    // Afficher le bouton si un contact est sélectionné
    snBarreBouton.activerBouton(BOUTON_REDIGER_MAIL, actif);
  }

  /**
   * Rafraichir le bouton pour passer le contact en principal.
   */
  private void rafraichirBoutonMettrePrincipal() {
    boolean actif = true;

    // Ne pas afficher si aucun contact n'est sélectionné
    Contact contactSelectionne = getModele().getContactSelectionne();
    if (contactSelectionne == null) {
      actif = false;
    }
    // Ne pas afficher le bouton si le contact sélectionné est un contact principal
    else if (contactSelectionne.isContactPrincipal(getModele().getIdClient())
        || contactSelectionne.isContactPrincipal(getModele().getIdFournisseur())) {
      actif = false;
    }

    // Afficher le bouton si un contact est sélectionné
    snBarreBouton.activerBouton(BOUTON_METTRE_PRINCIPAL, actif);
  }

  /**
   * Rafraichir le bouton pour supprimer un lien.
   */
  private void rafraichirBoutonSupprimerLien() {
    // Afficher le bouton si un contact est sélectionné
    snBarreBouton.activerBouton(BOUTON_SUPPRIMER_LIEN, getModele().getContactSelectionne() != null);
  }

  /**
   * Rafraichir le bouton pour afficher un contact.
   */
  private void rafraichirBoutonAfficherContact() {
    // Afficher le bouton si un contact est sélectionné
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, getModele().getContactSelectionne() != null);
  }

  /**
   * Traiter les clics sur les boutons des barres de boutons.
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        getModele().afficherContact();
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_CONTACT)) {
        getModele().creerContact();
      }
      else if (pSNBouton.isBouton(BOUTON_REDIGER_MAIL)) {
        getModele().redigerMail();
      }
      else if (pSNBouton.isBouton(BOUTON_METTRE_PRINCIPAL)) {
        getModele().modifierContactPrincipal();
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER_LIEN)) {
        getModele().supprimerLien();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Sélectionner l'ensemble du texte du champ lorsqu'on arrive dans la recherche générique.
   */
  private void tfRechercheGeneriqueFocusGained(FocusEvent e) {
    try {
      tfRechercheGenerique.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Modifier le modèle lorsqu'on quitte le champ.
   */
  private void tfRechercheGeneriqueFocusLost(FocusEvent e) {
    try {
      getModele().modifierRechercheGenerique(tfRechercheGenerique.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Modifier le modèle lorsqu'on appui sur la touche entrée.
   */
  private void tfRechercheGeneriqueActionPerformed(ActionEvent e) {
    try {
      getModele().modifierRechercheGenerique(tfRechercheGenerique.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Mettre le focus sur le tableau.
   */
  private void tfRechercheGeneriqueKeyReleased(KeyEvent e) {
    try {
      if (e.getKeyCode() == SNCharteGraphique.TOUCHE_BAS.getKeyCode() || e.getKeyCode() == SNCharteGraphique.TOUCHE_DROITE.getKeyCode()) {
        tblListeContact.requestFocusInWindow();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Modifier le modèle lorsqu'une nouvelle fonction est sélectionnée dans la liste déroulante.
   */
  private void cbFonctionItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbFonction.getSelectedItem() instanceof CategorieContact) {
        getModele().modifierCategorieContact((CategorieContact) cbFonction.getSelectedItem());
      }
      else {
        getModele().modifierCategorieContact(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * La ligne sélectionnée a changée dans le tableau résultat.
   */
  private void tblListeContactSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }

      // Informer le modèle
      getModele().modifierLigneSelectionnee(tblListeContact.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Sélectionner un contact lors d'un clic dans le tableau.
   * Ou afficher le contact lors d'un double-clic.
   */
  private void tblListeContactMouseClicked(MouseEvent e) {
    try {
      // Sélectioner la ligne que laquelle on a cliqué
      getModele().modifierLigneSelectionnee(tblListeContact.getIndiceSelection());

      // Afficher le contact si c'est un double clic
      if (e.getClickCount() == 2 && getModele().getContactSelectionne() != null) {
        getModele().afficherContact();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Remettre le focus dans la recherche générique.
   */
  private void tblListeContactKeyReleased(KeyEvent e) {
    try {
      if (derniereLigneSelectionnee == 0 && (e.getKeyCode() == SNCharteGraphique.TOUCHE_HAUT.getKeyCode()
          || e.getKeyCode() == SNCharteGraphique.TOUCHE_GAUCHE.getKeyCode())) {
        tfRechercheGenerique.requestFocusInWindow();
      }
      derniereLigneSelectionnee = tblListeContact.getSelectedRow();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Afficher le contact sélectionné lors d'un appui sur la touche entrée dans le tableau.
   */
  private void tblListeContactEnterKey() {
    try {
      if (getModele().getContactSelectionne() != null) {
        getModele().afficherContact();
      }
      else {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new SNPanelContenu();
    pnlRecherche = new SNPanel();
    pnlRechercheGauche = new JPanel();
    lbTexteRecherche = new SNLabelChamp();
    tfRechercheGenerique = new SNTexte();
    lbFonction = new SNLabelChamp();
    cbFonction = new SNComboBox();
    pnlRechercheDroite = new JPanel();
    lbTitreListe = new SNLabelTitre();
    scpListeContact = new JScrollPane();
    tblListeContact = new NRiTable();
    snBarreBouton = new SNBarreBouton();

    // ======== this ========
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Liste de contacts");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), null));
        pnlContenu.setPreferredSize(new Dimension(975, 500));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };

        // ======== pnlRecherche ========
        {
          pnlRecherche.setOpaque(false);
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridLayout());

          // ======== pnlRechercheGauche ========
          {
            pnlRechercheGauche.setOpaque(false);
            pnlRechercheGauche.setBorder(null);
            pnlRechercheGauche.setName("pnlRechercheGauche");
            pnlRechercheGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWidths = new int[] { 165, 300, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };

            // ---- lbTexteRecherche ----
            lbTexteRecherche.setText("Recherche g\u00e9n\u00e9rique");
            lbTexteRecherche.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTexteRecherche.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTexteRecherche.setPreferredSize(new Dimension(100, 30));
            lbTexteRecherche.setName("lbTexteRecherche");
            pnlRechercheGauche.add(lbTexteRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- tfRechercheGenerique ----
            tfRechercheGenerique.setMinimumSize(new Dimension(300, 30));
            tfRechercheGenerique.setPreferredSize(new Dimension(300, 30));
            tfRechercheGenerique.setName("tfRechercheGenerique");
            tfRechercheGenerique.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                tfRechercheGeneriqueActionPerformed(e);
              }
            });
            tfRechercheGenerique.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                tfRechercheGeneriqueKeyReleased(e);
              }
            });
            tfRechercheGenerique.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                tfRechercheGeneriqueFocusGained(e);
              }

              @Override
              public void focusLost(FocusEvent e) {
                tfRechercheGeneriqueFocusLost(e);
              }
            });
            pnlRechercheGauche.add(tfRechercheGenerique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            // ---- lbFonction ----
            lbFonction.setText("Fonction");
            lbFonction.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFonction.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFonction.setPreferredSize(new Dimension(100, 30));
            lbFonction.setName("lbFonction");
            pnlRechercheGauche.add(lbFonction, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

            // ---- cbFonction ----
            cbFonction.setName("cbFonction");
            cbFonction.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbFonctionItemStateChanged(e);
              }
            });
            pnlRechercheGauche.add(cbFonction, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheGauche);

          // ======== pnlRechercheDroite ========
          {
            pnlRechercheDroite.setOpaque(false);
            pnlRechercheDroite.setName("pnlRechercheDroite");
            pnlRechercheDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          }
          pnlRecherche.add(pnlRechercheDroite);
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ---- lbTitreListe ----
        lbTitreListe.setText("Liste des contacts");
        lbTitreListe.setImportanceMessage(EnumImportanceMessage.MOYEN);
        lbTitreListe.setName("lbTitreListe");
        pnlContenu.add(lbTitreListe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ======== scpListeContact ========
        {
          scpListeContact.setName("scpListeContact");

          // ---- tblListeContact ----
          tblListeContact.setShowVerticalLines(true);
          tblListeContact.setShowHorizontalLines(true);
          tblListeContact.setBackground(Color.white);
          tblListeContact.setRowHeight(20);
          tblListeContact.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeContact.setSelectionBackground(new Color(57, 105, 138));
          tblListeContact.setGridColor(new Color(204, 204, 204));
          tblListeContact.setName("tblListeContact");
          tblListeContact.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeContactMouseClicked(e);
            }
          });
          tblListeContact.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tblListeContactKeyReleased(e);
            }
          });
          scpListeContact.setViewportView(tblListeContact);
        }
        pnlContenu.add(scpListeContact, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);

      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlRecherche;
  private JPanel pnlRechercheGauche;
  private SNLabelChamp lbTexteRecherche;
  private SNTexte tfRechercheGenerique;
  private SNLabelChamp lbFonction;
  private SNComboBox cbFonction;
  private JPanel pnlRechercheDroite;
  private SNLabelTitre lbTitreListe;
  private JScrollPane scpListeContact;
  private NRiTable tblListeContact;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
