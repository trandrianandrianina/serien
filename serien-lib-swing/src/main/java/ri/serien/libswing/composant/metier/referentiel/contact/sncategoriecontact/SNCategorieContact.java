/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.contact.sncategoriecontact;

import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.CategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.IdCategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.ListeCategorieContact;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection des catégories contact.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une CategorieContact dans le logiciel.
 * Il gère l'affichage des CategorieContact existantes suivant l'établissement et gère la sélection de l'une de ces CategorieContact
 *
 * Utilisation :
 * - Ajouter un composant SNCategorieContact à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans la gestion des contacts.
 */
public class SNCategorieContact extends SNComboBoxObjetMetier<IdCategorieContact, CategorieContact, ListeCategorieContact> {
  
  /**
   * Constructeur par défaut.
   */
  public SNCategorieContact() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeCategorieContact());
  }
  
  /**
   * Sélectionner une catégorie contact de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampCategorieContact) {
    if (pChampCategorieContact == null || pLexical.HostFieldGetData(pChampCategorieContact) == null) {
      throw new MessageErreurException("Impossible de sélectionner la catégorie contact car son code est invalide.");
    }
    
    IdCategorieContact idCategorieContact = null;
    String valeurCategorieClient = pLexical.HostFieldGetData(pChampCategorieContact).trim();
    if (valeurCategorieClient != null && !valeurCategorieClient.trim().isEmpty() && !valeurCategorieClient.equals("**")) {
      idCategorieContact = IdCategorieContact.getInstance(pLexical.HostFieldGetData(pChampCategorieContact));
    }
    setIdSelection(idCategorieContact);
  }
  
  /**
   * Renseigner les champs RPG correspondant à la catégorie client sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampCategorieContact) {
    CategorieContact categorie = getSelection();
    if (categorie != null) {
      pLexical.HostFieldPutData(pChampCategorieContact, 0, categorie.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampCategorieContact, 0, "");
    }
  }
}
