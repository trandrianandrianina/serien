/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.saisie;

import javax.swing.SwingConstants;
import javax.swing.text.AbstractDocument;

import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Saisie ou consultation d'un nombre décimal (caractères autorisées : 012345679).
 *
 * Ce composant remplace JTextField. Il est à utiliser pour tous les saisies de nombres décimaux. Le nombre est justifié à droite (comme
 * les nombres dans un tableur). Il est possible de contrainte le nombre de chiffres saisis avec la méthode setLongueur().
 *
 * Reprend les caractéristiques graphiques de SNTexte sauf :
 * - Alignement à droite.
 */
public class SNNombreDecimal extends SNTexte {
  public SNNombreDecimal() {
    super();
    
    // Aligner les nombres à droite.
    setHorizontalAlignment(SwingConstants.RIGHT);
  }
  
  /**
   * Modifier le nombre de chiffres autorisés pour la saisie.
   * @param pLongueur Nombre de chiffres (ne peut être inférieur à 1).
   */
  @Override
  public void setLongueur(int pLongueur) {
    // Contrôler le paramètre
    if (pLongueur < 1) {
      pLongueur = 1;
    }
    
    // Formater la saisie
    SaisieDefinition saisieDefinition = new SaisieDefinition(pLongueur, SaisieDefinition.NOMBRE_DECIMAL);
    ((AbstractDocument) getDocument()).setDocumentFilter(saisieDefinition);
  }
}
