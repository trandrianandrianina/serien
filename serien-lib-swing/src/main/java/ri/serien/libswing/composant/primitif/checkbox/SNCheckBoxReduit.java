/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.checkbox;

import java.awt.Cursor;

import javax.swing.JCheckBox;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Boîte à cocher avec la présentation réduite.
 *
 * Ce composant remplace JCheckBox et permet de présenter une boîte à cocher avec le format réduit.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 */
public class SNCheckBoxReduit extends JCheckBox {
  /**
   * Constructeur.
   */
  public SNCheckBoxReduit() {
    super();

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE_REDUIT);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE_REDUIT);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE_REDUIT);

    // Rendre le composant transparent
    setOpaque(false);

    // Fixer la police standard
    setFont(SNCharteGraphique.POLICE_STANDARD_REDUIT);

    // Fixer le curseur
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
  }
}
