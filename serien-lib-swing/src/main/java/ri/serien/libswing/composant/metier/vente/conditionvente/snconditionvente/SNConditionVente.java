/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.conditionvente.snconditionvente;

import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.GroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.IdGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.ListeGroupeConditionVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection des Condition de vente.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une ConditionVente dans le logiciel.
 * Il gère l'affichage des ConditionVente existantes suivant l'établissement et gère la sélection de l'une de ces ConditionVente
 *
 * Utilisation :
 * - Ajouter un composant SNConditionVente à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le ... (A implementer).
 */
public class SNConditionVente extends SNComboBoxObjetMetier<IdGroupeConditionVente, GroupeConditionVente, ListeGroupeConditionVente> {
  
  /**
   * Constructeur par défaut.
   */
  public SNConditionVente() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeGroupeConditionVente());
  }
  
  /**
   * Sélectionner une condition de vente de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pConditionVente) {
    if (pConditionVente == null || pLexical.HostFieldGetData(pConditionVente) == null) {
      throw new MessageErreurException("Impossible de sélectionner la condition de vente car son code est invalide.");
    }
    
    IdGroupeConditionVente idConditionVente = null;
    String valeurConditionVente = pLexical.HostFieldGetData(pConditionVente);
    if (valeurConditionVente != null && !valeurConditionVente.trim().isEmpty()) {
      idConditionVente = IdGroupeConditionVente.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pConditionVente));
    }
    setIdSelection(idConditionVente);
  }
  
  /**
   * Renseigner les champs RPG correspondant à la condition de vente sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pConditionVente) {
    GroupeConditionVente condition = getSelection();
    if (condition != null) {
      pLexical.HostFieldPutData(pConditionVente, 0, condition.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pConditionVente, 0, "");
    }
  }
  
}
