/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.conditionvente.snconditionvente;

import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.IdGroupeConditionVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Boite de dialogue de sélection d'une condition de vente ou d'une condition client
 */
public class ModeleConditionVenteEtClient extends AbstractModeleDialogue {
  // Constantes
  public static final int MODE_NON_DEFINI = 0;
  public static final int MODE_CNV = 1;
  public static final int MODE_CLIENT = 2;
  public static final int MODE_SUPPRESSION = 3;

  // Variables
  private IdEtablissement idEtablissement = null;
  private int modeSelection = MODE_NON_DEFINI;
  private IdGroupeConditionVente idConditionSelectionnee = null;
  private IdClient idClientSelectionne = null;

  /**
   * Constructeur.
   */
  public ModeleConditionVenteEtClient(SessionBase pSession, IdEtablissement pIdEtablissement) {
    super(pSession);
    idEtablissement = pIdEtablissement;
  }

  // Méthodes standards du modèle

  @Override
  public void initialiserDonnees() {
    initialiserCondition();
  }
  
  @Override
  public void chargerDonnees() {
    initialiserCondition();
  }
  
  @Override
  public void quitterAvecValidation() {
    if (modeSelection == MODE_CNV) {
      idClientSelectionne = null;
    }
    else if (modeSelection == MODE_CLIENT) {
      idConditionSelectionnee = null;
    }
    else if (modeSelection == MODE_SUPPRESSION) {
      idClientSelectionne = null;
      idConditionSelectionnee = null;
    }
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    idConditionSelectionnee = null;
    idClientSelectionne = null;
    super.quitterAvecAnnulation();
  }
  
  public void supprimerCondition() {
    modeSelection = MODE_SUPPRESSION;
    quitterAvecValidation();
  }

  /**
   * A partir du libellé contenu dans le composant, on détermine quel type de condition est en cours
   */
  public void initialiserCondition() {
    if (idConditionSelectionnee == null && idClientSelectionne == null) {
      modeSelection = MODE_NON_DEFINI;
    }
    else if (idConditionSelectionnee != null) {
      modeSelection = MODE_CNV;
    }
    else if (idClientSelectionne != null) {
      modeSelection = MODE_CLIENT;
    }
  }
  
  // -- Méthodes privées

  // Accesseurs

  /**
   * Modifier le mode de sélection
   */
  public void modifierMode(int pMode) {
    if (Constantes.equals(modeSelection, pMode)) {
      return;
    }
    modeSelection = pMode;
    rafraichir();
  }
  
  /**
   * Retourner l'identifiant de l'établissement en cours
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Retourner le mode de sélection en cours
   */
  public int getModeSelection() {
    return modeSelection;
  }

  /**
   * Retourner l'identifiant de la CNV sélectionnée
   */
  public IdGroupeConditionVente getIdConditionSelectionnee() {
    return idConditionSelectionnee;
  }

  /**
   * Retourner l'identifiant du client sélectionné (pour condition client)
   */
  public IdClient getIdClientSelectionne() {
    return idClientSelectionne;
  }
  
  /**
   * Modifier l'identifiant du client sélectionné (pour condition client)
   */
  public void modifierIdClientSelectionne(IdClient pIdClient) {
    idClientSelectionne = pIdClient;
    rafraichir();
  }

  /**
   * Modifier l'identifiant de la CNV sélectionnée
   */
  public void modifierIdConditionSelectionnee(IdGroupeConditionVente pIdConditionVente) {
    idConditionSelectionnee = pIdConditionVente;
    rafraichir();
  }

}
