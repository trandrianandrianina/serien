/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.plagedate;

/**
 * Valeurs possibles pour le défilement d'une plage de dates.
 *
 * Lorsqu'un mode de défilement est actif, des boutons apparaissents à droite et à gauche du composant plage de dates. Lorsqu'un bouton
 * est cliqué, la plage de dates est décalée vers le passé ou vers le futur d'un cran :
 * - SANS_DEFILEMENT : pas de défilement possible,
 * - DEFILEMENT_SEMAINE : décalage une semaine avant ou après,
 * - DEFILEMENT_MOIS : décalage un mois avant ou après,
 * - DEFILEMENT_ANNEE : décalage une année avant ou après.
 */
public enum EnumDefilementPlageDate {
  SANS_DEFILEMENT,
  DEFILEMENT_SEMAINE,
  DEFILEMENT_MOIS,
  DEFILEMENT_ANNEE;
  
  EnumDefilementPlageDate() {
  }
}
