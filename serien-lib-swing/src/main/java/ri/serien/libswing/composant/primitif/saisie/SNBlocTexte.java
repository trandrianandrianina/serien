/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.saisie;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Highlighter;

import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.outil.clipboard.Clipboard;
import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Saisie ou consultation d'un bloc de texte alphanumérique (caractères autorisées : aplhapnumériques).
 *
 * Ce composant remplace JTextArea. Il est à utiliser pour tous les saisies de bloc de textes aplhanumériques. Il est possible de
 * contrainte le nombre de caractères saisissables via la méthode setLongueur().
 *
 * Ce composant est équipé d'un menu contextuel qui apparait avec un clic droit de la souris et permet de copier le texte qu'il contient
 * dans le presse papier.
 *
 * Il existe aussi :
 * - SNTexte pour les textes.
 * - SNNombreEntier pour les nombres entiers.
 * - SNNombreDecimal pour les nombres décimaux.
 * - SNIdentifiant pour les identifiants.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 * - Alignement à gauche.
 */
public class SNBlocTexte extends JTextArea {
  private final JPopupMenu menuContextuel = new JPopupMenu("Copie");
  private JMenuItem copie = new JMenuItem("Copier");
  private int longueur = 0;

  public SNBlocTexte() {
    super();
    
    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);

    // Rendre le composant transparent
    setOpaque(false);

    // Utiliser la police standard
    setFont(SNCharteGraphique.POLICE_STANDARD);

    // Menu contextuel de copie
    copie.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        // Si une partie du texte du composant est surlignée (sélectionnée) c'est elle que l'on copie
        Highlighter surlignement = getHighlighter();
        if (surlignement.getHighlights() != null && surlignement.getHighlights().length > 0) {
          String texteSelectionne =
              getText().substring(surlignement.getHighlights()[0].getStartOffset(), surlignement.getHighlights()[0].getEndOffset());
          Clipboard.envoyerTexte(texteSelectionne);
        }
        // Sinon on copie tout le texte
        else {
          Clipboard.envoyerTexte(getText());
        }
      }
    });
    menuContextuel.add(copie);
    setComponentPopupMenu(menuContextuel);
    
  }

  @Override
  public void setEnabled(boolean pEnabled) {
    setEditable(pEnabled);
    setFocusable(pEnabled);
    if (!pEnabled) {
      setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
    }
    else {
      setBackground(Color.WHITE);
    }
  }

  /**
   * Nombre de caractères autorisés pour la saisie.
   * @return Nombre de caractères.
   */
  public int getLongueur() {
    return longueur;
  }
  
  /**
   * Modifier le nombre de caractéres autorisés pour la saisie.
   * @param pLongueur Nombre de caractères (ne peut être inférieur à 1).
   */
  public void setLongueur(int pLongueur) {
    setLongueur(pLongueur, true);
  }

  /**
   * Modifier le nombre de caractéres autorisés pour la saisie.
   * @param pLongueur Nombre de caractères (ne peut être inférieur à 1).
   * @param pAlphanumerique true si alphanumérique, false si numérique uniquement.
   */
  public void setLongueur(int pLongueur, boolean pAlphanumerique) {
    // Contrôler le paramètre
    if (pLongueur < 1) {
      pLongueur = 1;
    }
    
    // Formater la saisie
    SaisieDefinition saisieDefinition = new SaisieDefinition(pLongueur, false, pAlphanumerique, false);
    ((AbstractDocument) this.getDocument()).setDocumentFilter(saisieDefinition);
  }
  
}
