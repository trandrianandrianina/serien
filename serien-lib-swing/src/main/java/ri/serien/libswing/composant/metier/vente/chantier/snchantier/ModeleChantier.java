/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.chantier.snchantier;

import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.chantier.CritereChantier;
import ri.serien.libcommun.gescom.vente.chantier.EnumStatutChantier;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.chantier.ListeChantier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modele du composant de sélection d'un chantier
 */
public class ModeleChantier extends AbstractModeleDialogue {
  // Constantes
  public static final int MODE_FILTRE_RECHERCHE = 0;
  public static final int MODE_COMPOSANT_SELECTION = 1;
  public static final int MODE_COMPOSANT_SELECTION_AVEC_BLOQUES = 2;

  // Variables
  private CritereChantier critereChantier = new CritereChantier();
  private List<IdChantier> listeIdChantier = null;
  private ListeChantier listeChantier = null;
  private int ligneSelectionnee = -1;
  private Chantier chantierSelectionne = null;
  private boolean isModeFiltre = false;
  private int modeComposant = MODE_COMPOSANT_SELECTION;

  /**
   * Constructeur.
   */
  public ModeleChantier(SessionBase pSession) {
    super(pSession);
  }

  // -- Méthodes standards du modèle

  @Override
  public void initialiserDonnees() {
    IdSession.controlerId(getIdSession(), true);

    // Comportement du filtre d'état suivant le mode du composant
    switch (modeComposant) {
      case MODE_COMPOSANT_SELECTION:
        critereChantier.setStatut(EnumStatutChantier.VALIDE);
        break;

      case MODE_COMPOSANT_SELECTION_AVEC_BLOQUES:
        critereChantier.setStatut(EnumStatutChantier.VALIDE);
        break;

      case MODE_FILTRE_RECHERCHE:
        critereChantier.setStatut(null);
        break;
      
      default:
        break;
    }
  }

  @Override
  public void chargerDonnees() {
    lancerRechercheInterne();
  }

  @Override
  public void quitterAvecValidation() {
    // Contrôler que le chantier est valide
    if (!isChantierSelectionnable()) {
      return;
    }

    super.quitterAvecValidation();
  }

  @Override
  public void quitterAvecAnnulation() {
    critereChantier.initialiser();
    super.quitterAvecAnnulation();
  }

  /**
   * Lancer une recherche sur les chantiers mais déclencher de rafraîchissement.
   */
  private void lancerRechercheInterne() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeChantier != null) {
      taillePage = listeChantier.getTaillePage();
    }

    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeIdChantier = null;
    listeChantier = null;
    ligneSelectionnee = -1;
    chantierSelectionne = null;
    
    // Comportement de la recherche suivant le mode du composant
    switch (modeComposant) {
      case MODE_COMPOSANT_SELECTION:
        critereChantier.setAvecBloque(false);
        break;

      case MODE_COMPOSANT_SELECTION_AVEC_BLOQUES:
        critereChantier.setAvecBloque(true);
        break;

      case MODE_FILTRE_RECHERCHE:
        critereChantier.setAvecBloque(true);
        break;
      
      default:
        break;
    }

    // Charger la liste des ids chantiers
    listeIdChantier = ManagerServiceClient.chargerListeIdChantier(getIdSession(), critereChantier);

    // Initier la liste des documents de ventes à partir de la liste d'identifiants
    listeChantier = ListeChantier.creerListeNonChargee(listeIdChantier);

    // Charger la première page
    listeChantier.chargerPremierePage(getIdSession(), taillePage);
  }

  /**
   * Afficher la plage de chantiers compris entre deux lignes.
   * Les informations des chantiers sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageChantierAffiches(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeChantier != null) {
      listeChantier.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
    }

  }

  // -- Méthodes publiques

  /**
   * Lancer une recherche sur les chantiers.
   */
  public void lancerRecherche() {
    lancerRechercheInterne();
    // Rafraichir l'écran
    rafraichir();
  }

  /**
   * Initialiser la recherche.
   */
  public void initialiserRecherche() {
    critereChantier.initialiser();
    listeIdChantier = null;
    listeChantier = null;
    ligneSelectionnee = -1;
    chantierSelectionne = null;
    lancerRechercheInterne();
    rafraichir();
  }

  /**
   * Affiche la liste des chantiers possibles à partir des critères de recherche.
   */
  public void modifierRechercheGenerique(String pRechercheGenerique) {
    pRechercheGenerique = Constantes.normerTexte(pRechercheGenerique);
    if (Constantes.equals(pRechercheGenerique, critereChantier.getTexteRechercheGenerique())) {
      return;
    }
    critereChantier.setTexteRechercheGenerique(pRechercheGenerique);
    lancerRechercheInterne();
    rafraichir();
  }

  /**
   * Modifier le statut des chantiers à rechercher
   */
  public void modifierStatut(EnumStatutChantier pEtat) {
    // listeIdChantier = null;
    initialiserRecherche();
    critereChantier.setStatut(pEtat);
    lancerRechercheInterne();
    rafraichir();
  }

  /**
   * Modifie l'indice de la ligne séléctionnée dans le tableau.
   */
  public void modifierLigneSelectionnee(int pLigneSelectionnee) {
    if (ligneSelectionnee == pLigneSelectionnee) {
      return;
    }
    ligneSelectionnee = pLigneSelectionnee;

    // Déterminer le chantier sélectionné
    if (listeChantier != null && 0 <= ligneSelectionnee && ligneSelectionnee < listeChantier.size()) {
      chantierSelectionne = listeChantier.get(ligneSelectionnee);
    }
    else {
      chantierSelectionne = null;
    }

    rafraichir();
  }

  // -- Accesseurs

  /**
   * Titre à afficher au dessus de la liste de chantiers et qui résume le résultat de la recherche.
   */
  public Message getTitreListe() {
    if (listeIdChantier == null) {
      return Message.getMessageNormal("Chantiers correspondant à votre recherche");
    }
    else if (listeIdChantier.size() == 1) {
      return Message.getMessageNormal("Chantier correspondant à votre recherche (1)");
    }
    else if (listeIdChantier.size() > 1) {
      return Message.getMessageNormal("Chantiers correspondants à votre recherche (" + listeIdChantier.size() + ")");
    }
    else {
      return Message.getMessageImportant("Aucun chantier ne correspond à votre recherche");
    }
  }

  /**
   * Retourne le chantier sélectionné.
   */
  public Chantier getChantierSelectionne() {
    return chantierSelectionne;
  }

  /**
   * Indiquer si le chantier sélectionné est validable.
   */
  public boolean isChantierSelectionnable() {
    if (modeComposant == MODE_FILTRE_RECHERCHE) {
      return chantierSelectionne != null;
    }
    else {
      return chantierSelectionne != null && !chantierSelectionne.isVerrouille()
          && chantierSelectionne.getStatutChantier() != null && (chantierSelectionne.getStatutChantier().equals(EnumStatutChantier.VALIDE)
              || chantierSelectionne.getStatutChantier().equals(EnumStatutChantier.EDITE));
    }
  }

  /**
   * Retourner la liste des chantiers
   */
  public ListeChantier getListeChantier() {
    return listeChantier;
  }

  /**
   * Modifier la liste des identifiants chantiers
   */
  public void setListeIdChantier(List<IdChantier> pListeIdChantier) {
    listeIdChantier = pListeIdChantier;
  }

  /**
   * Retourner les critères de recherche chantier
   */
  public CritereChantier getCriteresChantier() {
    return critereChantier;
  }

  /**
   * Modifier les critères de recherche chantier
   */
  public void setCriteresChantier(CritereChantier pCriteresChantier) {
    critereChantier = pCriteresChantier;
  }

  /**
   * Retourner l'indice sélectionné dans le tableau
   */
  public int getLigneSelectionnee() {
    return ligneSelectionnee;
  }
  
  /**
   * Retourner le mode d'utilisation du composant
   */
  public int getModeComposant() {
    return modeComposant;
  }

  /**
   * Modifier le mode d'utilisation du composant
   */
  public void setModeComposant(int pMode) {
    modeComposant = pMode;
  }
  
}
