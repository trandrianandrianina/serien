/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;

import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.detailcalcul.SNDetailCalcul;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametrearticle.SNParametreArticle;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametrechantier.SNParametreChantier;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametreclient.SNParametreClient;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametreconditionvente.SNParametreConditionVente;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametredocumentvente.SNParametreDocumentVente;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametreetablissement.SNParametreEtablissement;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametrelignevente.SNParametreLigneVente;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametretarif.SNParametreTarif;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue du détail d'une formule de calcul d'un prix de vente (classe FormulePrixVente).
 */
public class VueFormule extends AbstractVuePanel<ModeleDialogueDetailPrixVente> {
  /**
   * Contructeur.
   */
  public VueFormule(ModeleDialogueDetailPrixVente pModele) {
    super(pModele);
  }

  /**
   * Initialiser les composants graphiques de la boîte de dialogue.
   */
  @Override
  public void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();
    setLocation(0, 0);

    // Renseigner les modèles des composants
    snParametreEtablissement.setModele(getModele());
    snParametreArticle.setModele(getModele());
    snParametreTarif.setModele(getModele());
    snParametreClient.setModele(getModele());
    snParametreConditionVente.setModele(getModele());
    snParametreChantier.setModele(getModele());
    snParametreDocumentVente.setModele(getModele());
    snParametreLigneVente.setModele(getModele());
    snDetailCalcul.setModele(getModele());
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichissement des données à l'écran.
   */
  @Override
  public void rafraichir() {
    // Rafraîchir les encarts paramètres
    rafraichirParametreEtablissement();
    rafraichirParametreArticle();
    rafraichirParametreTarif();
    rafraichirParametreClient();
    rafraichirParanetreConditionVente();
    rafraichirParanetreChantier();
    rafraichirParametreDocumentVente();
    rafraichirParametreLigneVente();

    // Rafraîchir le détail du calcul
    rafraichirDetailCalcul();
  }

  /**
   * Rafraichir les paramètres établissement.
   */
  private void rafraichirParametreEtablissement() {
    if (getModele().getParametreEtablissement() != null) {
      snParametreEtablissement.rafraichir();
      snParametreEtablissement.setVisible(true);
    }
    else {
      snParametreEtablissement.setVisible(false);
    }
  }

  /**
   * Rafraichir les paramètres de l'article.
   */
  private void rafraichirParametreArticle() {
    if (getModele().getParametreArticle() != null) {
      snParametreArticle.rafraichir();
      snParametreArticle.setVisible(true);
    }
    else {
      snParametreArticle.setVisible(false);
    }
  }

  /**
   * Rafraichir les paramètres tarif de l'article.
   */
  private void rafraichirParametreTarif() {
    if (getModele().getParametreTarif() != null) {
      snParametreTarif.rafraichir();
      snParametreTarif.setVisible(true);
    }
    else {
      snParametreTarif.setVisible(false);
    }
  }

  /**
   * Rafraichir les paramètres du client facturé (TODO les paramètres du client livré).
   */
  private void rafraichirParametreClient() {
    if (getModele().getParametreClientFacture() != null) {
      snParametreClient.rafraichir();
      snParametreClient.setVisible(true);
    }
    else {
      snParametreClient.setVisible(false);
    }
  }

  /**
   * Rafraichir les paramètres de la condition de vente.
   */
  private void rafraichirParanetreConditionVente() {
    snParametreConditionVente.rafraichir();
    snParametreConditionVente.setVisible(getModele().getParametreConditionVente() != null);
  }
  
  /**
   * Rafraichir les paramètres du chantier.
   */
  private void rafraichirParanetreChantier() {
    snParametreChantier.rafraichir();
    snParametreChantier.setVisible(getModele().getParametreChantier() != null);
  }

  /**
   * Rafraichir les paramètres du document de vente.
   */
  private void rafraichirParametreDocumentVente() {
    if (getModele().getParametreDocumentVente() != null) {
      snParametreDocumentVente.rafraichir();
      snParametreDocumentVente.setVisible(true);
    }
    else {
      snParametreDocumentVente.setVisible(false);
    }
  }

  /**
   * Rafraichir les paramètres de la ligne de vente.
   */
  private void rafraichirParametreLigneVente() {
    if (getModele().getParametreLigneVente() != null) {
      snParametreLigneVente.rafraichir();
      snParametreLigneVente.setVisible(true);
    }
    else {
      snParametreLigneVente.setVisible(false);
    }
  }

  /**
   * Rafraichir le détail du calcul du prix.
   */
  private void rafraichirDetailCalcul() {
    snDetailCalcul.rafraichir();
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    scpFond = new JScrollPane();
    pnlFond = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    pnlColonne1 = new SNPanel();
    snParametreEtablissement = new SNParametreEtablissement();
    snParametreClient = new SNParametreClient();
    snParametreArticle = new SNParametreArticle();
    snParametreTarif = new SNParametreTarif();
    pnlColonne2 = new SNPanel();
    snParametreConditionVente = new SNParametreConditionVente();
    snParametreChantier = new SNParametreChantier();
    pnlColonne3 = new SNPanel();
    snParametreDocumentVente = new SNParametreDocumentVente();
    snParametreLigneVente = new SNParametreLigneVente();
    pnlColonne4 = new SNPanel();
    snDetailCalcul = new SNDetailCalcul();

    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());

    // ======== scpFond ========
    {
      scpFond.setViewportBorder(null);
      scpFond.setBorder(BorderFactory.createEmptyBorder());
      scpFond.setName("scpFond");

      // ======== pnlFond ========
      {
        pnlFond.setBorder(null);
        pnlFond.setName("pnlFond");
        pnlFond.setLayout(new BorderLayout());

        // ======== pnlContenu ========
        {
          pnlContenu.setBackground(new Color(238, 238, 210));
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(new GridLayout());

          // ======== pnlColonne1 ========
          {
            pnlColonne1.setName("pnlColonne1");
            pnlColonne1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlColonne1.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlColonne1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlColonne1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlColonne1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };

            // ---- snParametreEtablissement ----
            snParametreEtablissement.setBackground(new Color(238, 238, 210));
            snParametreEtablissement.setModeReduit(true);
            snParametreEtablissement.setName("snParametreEtablissement");
            pnlColonne1.add(snParametreEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

            // ---- snParametreClient ----
            snParametreClient.setName("snParametreClient");
            pnlColonne1.add(snParametreClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

            // ---- snParametreArticle ----
            snParametreArticle.setBackground(new Color(238, 238, 210));
            snParametreArticle.setModeReduit(true);
            snParametreArticle.setName("snParametreArticle");
            pnlColonne1.add(snParametreArticle, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

            // ---- snParametreTarif ----
            snParametreTarif.setName("snParametreTarif");
            pnlColonne1.add(snParametreTarif, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlColonne1);

          // ======== pnlColonne2 ========
          {
            pnlColonne2.setName("pnlColonne2");
            pnlColonne2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlColonne2.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlColonne2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlColonne2.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlColonne2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };

            // ---- snParametreConditionVente ----
            snParametreConditionVente.setName("snParametreConditionVente");
            pnlColonne2.add(snParametreConditionVente, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

            // ---- snParametreChantier ----
            snParametreChantier.setName("snParametreChantier");
            pnlColonne2.add(snParametreChantier, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlColonne2);

          // ======== pnlColonne3 ========
          {
            pnlColonne3.setName("pnlColonne3");
            pnlColonne3.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlColonne3.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlColonne3.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlColonne3.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlColonne3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };

            // ---- snParametreDocumentVente ----
            snParametreDocumentVente.setName("snParametreDocumentVente");
            pnlColonne3.add(snParametreDocumentVente, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

            // ---- snParametreLigneVente ----
            snParametreLigneVente.setName("snParametreLigneVente");
            pnlColonne3.add(snParametreLigneVente, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlColonne3);

          // ======== pnlColonne4 ========
          {
            pnlColonne4.setName("pnlColonne4");
            pnlColonne4.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlColonne4.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlColonne4.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlColonne4.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlColonne4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };

            // ---- snDetailCalcul ----
            snDetailCalcul.setName("snDetailCalcul");
            pnlColonne4.add(snDetailCalcul, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlColonne4);
        }
        pnlFond.add(pnlContenu, BorderLayout.CENTER);
      }
      scpFond.setViewportView(pnlFond);
    }
    add(scpFond, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scpFond;
  private SNPanelFond pnlFond;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlColonne1;
  private SNParametreEtablissement snParametreEtablissement;
  private SNParametreClient snParametreClient;
  private SNParametreArticle snParametreArticle;
  private SNParametreTarif snParametreTarif;
  private SNPanel pnlColonne2;
  private SNParametreConditionVente snParametreConditionVente;
  private SNParametreChantier snParametreChantier;
  private SNPanel pnlColonne3;
  private SNParametreDocumentVente snParametreDocumentVente;
  private SNParametreLigneVente snParametreLigneVente;
  private SNPanel pnlColonne4;
  private SNDetailCalcul snDetailCalcul;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
