/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.stock.detailstock;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Renderer du tableau affiche la liste des stocks par magasin.
 * La dernière ligne, le total pour l'établissement, doitg être mise en gras.
 */
class ListeStockMagasinRenderer extends DefaultTableCellRenderer {
  @Override
  public Component getTableCellRendererComponent(JTable pTable, Object pValeur, boolean pSelectionne, boolean pFocus, int pLigne,
      int pColonne) {
    // Récupérer le composant de la cellule
    JLabel composant = (JLabel) super.getTableCellRendererComponent(pTable, pValeur, pSelectionne, pFocus, pLigne, pColonne);

    // Tester si on est sur la dernière ligne du tableau
    if (pLigne == pTable.getRowCount() - 1) {
      // Mettre la police en gras
      composant.setFont(composant.getFont().deriveFont(Font.BOLD));
    }
    
    // Justifier les quantités à droite sauf pour le libellé du magasin
    if (pColonne == 0) {
      composant.setHorizontalAlignment(LEFT);
    }
    else {
      composant.setHorizontalAlignment(RIGHT);
    }
    return composant;
  }
}
