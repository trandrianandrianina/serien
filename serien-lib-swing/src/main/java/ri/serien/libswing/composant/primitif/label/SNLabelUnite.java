/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.label;

import java.awt.Dimension;

import javax.swing.SwingConstants;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Libellé pour l'unité associée à un champ.
 *
 * Ce composant remplace JLabel et dérive de SNLabel.
 * Il est à utiliser pour tous les libellés contenant l'unité d'un champ. Il utilise la police standard alignée vers la gauche.
 * Si le message est important il sera affiché en rouge sinon en noir.
 *
 * D'autres composants libellés sont disponibles :
 * - SNLabelChamp : pour les libellés de champs standards.
 * - SNLabelTitre : pour les titres de sections ou de tableaux.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police Satndard.
 * - Aligné à gauche.
 */
public class SNLabelUnite extends SNLabel {
  private final Dimension TAILLE = new Dimension(50, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);
  
  /**
   * Constructeur.
   */
  public SNLabelUnite() {
    super();

    // Fixer la taille standard du composant
    setMinimumSize(TAILLE);
    setPreferredSize(TAILLE);
    setMaximumSize(TAILLE);

    // Rendre le composant transparent
    setOpaque(false);
    
    // Utiliser la police par défaut
    setFont(SNCharteGraphique.POLICE_STANDARD);
    
    // Justifier à gauche
    setHorizontalAlignment(SwingConstants.LEADING);
  }
}
