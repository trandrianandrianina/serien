/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.saisie;

import javax.swing.text.AbstractDocument;

import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Saisie ou consultation d'un identifiant numérique (caractères autorisées : 012345679 et les séparateurs / ou -).
 *
 * Ce composant remplace JTextField. Il est à utiliser pour tous les saisies d'identifiants numériques avec ou sans séparateur. Le nombre
 * est justifié à gauche comme pour une chaîne de caractères. Il est possible de contrainte le nombre de chiffres saisis avec la méthode
 * setLongueur().
 *
 * Cette classe peut être améliorée pour pouvoir paramétrer la taille de chaque partie, paramétrer le séparateur, et autoriser la
 * saisie de caractères alphanumériques dans certaines parties.
 *
 * Reprend les caractéristiques graphiques de SNTexte sauf.
 */
public class SNIdentifiant extends SNTexte {
  public SNIdentifiant() {
    super();
  }

  /**
   * Modifier le nombre de caractéres autorisés pour la saisie.
   * @param pLongueur Nombre de caractères (ne peut être inférieur à 1).
   */
  @Override
  public void setLongueur(int pLongueur) {
    // Contrôler le paramètre
    if (pLongueur < 1) {
      pLongueur = 1;
    }

    // Formater la saisie
    SaisieDefinition saisieDefinition = new SaisieDefinition(pLongueur, SaisieDefinition.NOMBRE_ENTIER_AVEC_SEPARATEUR);
    ((AbstractDocument) getDocument()).setDocumentFilter(saisieDefinition);
  }
}
