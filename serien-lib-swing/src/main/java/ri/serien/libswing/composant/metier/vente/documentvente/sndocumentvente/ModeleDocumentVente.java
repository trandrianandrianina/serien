/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.sndocumentvente;

import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeIdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleDocumentVente extends AbstractModeleDialogue {
  private CritereDocumentVente critereDocumentVente = null;
  private DocumentVenteBase documentVenteSelectionne = null;
  private ListeDocumentVenteBase listeDocumentVenteBase = null;
  private List<IdDocumentVente> listeIdDocumentVente = null;
  private int ligneSelectionnee = -1;
  private int numeroDocument = 0;
  private int numeroFacture = 0;
  private EnumTypeDocumentVente typeDocumentVente = EnumTypeDocumentVente.NON_DEFINI;
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private ClientBase clientBase = null;
  private IdArticle idArticle = null;
  private Chantier chantier = null;
  private Date dateDebut = null;
  private Date dateFin = null;
  private Message titreResultat = null;
  private int indexDocumentSelectionne = -1;
  private ListeModeExpedition listeModeExpedition = null;
  private UtilisateurGescom utilisateurGescom = null;
  private boolean isAfficherDocumentHistorise = true;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Constructeur.
   * @param pSession La session.
   * @param pIdEtablissement Identifiant de l'établissement.
   * @param pCritereDocumentVente Critère de recherche pour un document de ventes.
   */
  public ModeleDocumentVente(SessionBase pSession, IdEtablissement pIdEtablissement, CritereDocumentVente pCritereDocumentVente) {
    super(pSession);
    idEtablissement = pIdEtablissement;
    critereDocumentVente = pCritereDocumentVente;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------

  @Override
  public void initialiserDonnees() {
    if (critereDocumentVente == null) {
      critereDocumentVente = new CritereDocumentVente();
    }
    
    if (!critereDocumentVente.isComposantDePlage()) {
      numeroDocument = critereDocumentVente.getNumeroDocument();
    }
  }
  
  @Override
  public void chargerDonnees() {
    // Charger la liste des modes d'expédition
    if (listeModeExpedition == null) {
      listeModeExpedition = ListeModeExpedition.charger(getIdSession());
      if (listeModeExpedition == null) {
        throw new MessageErreurException("Aucun mode d'expédition n'a été trouvé dans la base de données courante.");
      }
    }
    // Charger l'utilisateur
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), null);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());

    // Charger le magasin courant
    if (idMagasin == null) {
      idMagasin = utilisateurGescom.getIdMagasin();
    }

  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Lancer la recherche de document de ventes.
   */
  public void lancerRecherche() {
    lancerRechercheInterne();
    rafraichir();
  }
  
  /**
   * Modifier l'indice de la ligne séléctionnée dans le tableau.
   * @param pLigneSelectionnee Indice de la ligne à sélectionner.
   */
  public void modifierLigneSelectionnee(int pLigneSelectionnee) {
    if (ligneSelectionnee == pLigneSelectionnee) {
      return;
    }
    ligneSelectionnee = pLigneSelectionnee;

    // Définir le document de vente sélectionné
    if (listeDocumentVenteBase != null && 0 <= ligneSelectionnee && ligneSelectionnee < listeDocumentVenteBase.size()) {
      documentVenteSelectionne = listeDocumentVenteBase.get(ligneSelectionnee);
    }
    else {
      documentVenteSelectionne = null;
    }

    rafraichir();
  }
  
  /**
   * Afficher la plage d'articles comprise entre deux lignes.<br>
   * Les informations des articles sont chargées si cela n'a pas déjà été fait.
   * @param pIndexPremiereLigne Index de la première ligne.
   * @param pIndexDerniereLigne Index de la dernière ligne.
   */
  public void modifierPlageDocumentVenteAffiche(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeDocumentVenteBase != null) {
      ListeDocumentVenteBase listeDocumentVenteBaseCharge =
          listeDocumentVenteBase.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
      
      // Charger les informations complémentaires
      chargerComplement(listeDocumentVenteBaseCharge);
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }

  /**
   * Modifier le numéro du document à rechercher.
   * @param pNumeroDocument Nouveau numéro de document de ventes.
   */
  public void modifierNumeroDocument(String pNumeroDocument) {
    // Convertir la saisie en nombre
    int numero = 0;
    try {
      pNumeroDocument = Constantes.normerTexte(pNumeroDocument);
      if (!pNumeroDocument.isEmpty()) {
        if (pNumeroDocument.length() > 7) {
          throw new MessageErreurException("Le numéro saisi est trop long");
        }
        else {
          numero = Integer.parseInt(pNumeroDocument);
        }
      }
    }
    catch (NumberFormatException e) {
      numero = 0;
    }
    
    if (numeroDocument == numero) {
      return;
    }
    
    numeroDocument = numero;
    numeroFacture = numero;
    
    if (numero > 0) {
      rechercher();
    }
    rafraichir();
  }

  /**
   * Excécuter la recherche.
   */
  public void rechercher() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeDocumentVenteBase != null) {
      taillePage = listeDocumentVenteBase.getTaillePage();
    }

    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeDocumentVenteBase = null;
    setIndexDocumentSelectionne(-1);
    rafraichir();

    // Renseigner les critères de recherche
    critereDocumentVente.setIdEtablissement(idMagasin.getIdEtablissement());
    if (clientBase != null) {
      critereDocumentVente.setIdClient(clientBase.getId());
    }
    critereDocumentVente.setTypeDocumentVente(typeDocumentVente);
    critereDocumentVente.setNumeroFacture(numeroFacture);
    critereDocumentVente.setNumeroDocument(numeroDocument);
    if (idArticle != null) {
      critereDocumentVente.setCodeArticle(idArticle.getCodeArticle());
    }
    if (chantier != null) {
      critereDocumentVente.setChantier(chantier);
    }
    if (dateFin == null) {
      dateFin = new Date();
    }
    critereDocumentVente.setDateCreationFin(dateFin);
    critereDocumentVente.setDateCreationDebut(dateDebut);
    critereDocumentVente.setIdMagasin(idMagasin);
    if (isAfficherDocumentHistorise) {
      critereDocumentVente.setSansHistorique(false);
    }
    else {
      critereDocumentVente.setSansHistorique(true);
    }

    // Charger la liste des identifiants des documents de vente correspondant au résultat de la recherche
    List<IdDocumentVente> listeIdDocumentVente =
        ManagerServiceDocumentVente.chargerListeIdDocumentVente(getIdSession(), critereDocumentVente);
    setListeIdDocumentVente(listeIdDocumentVente);

    // Créer le résulat de la recherche
    listeDocumentVenteBase = ListeDocumentVenteBase.creerListeNonChargee(listeIdDocumentVente);

    // Charger la première page
    ListeDocumentVenteBase listeDocumentVenteBaseCharge = listeDocumentVenteBase.chargerPremierePage(getIdSession(), taillePage);
    chargerComplement(listeDocumentVenteBaseCharge);

    // Construire le titre
    construireTitre();

    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Modifier l'indice du document de ventes sélectionné.
   * @param pIndexDocumentSelectionne Index du document de vente sélectionné.
   */
  public void setIndexDocumentSelectionne(int pIndexDocumentSelectionne) {
    if (pIndexDocumentSelectionne < 0 || pIndexDocumentSelectionne >= listeIdDocumentVente.size()) {
      indexDocumentSelectionne = -1;
    }
    indexDocumentSelectionne = pIndexDocumentSelectionne;
  }

  /**
   * Modifier l'article à rechercher.
   * @param pIdArticle Identifiant de l'article.
   */
  public void modifierArticle(IdArticle pIdArticle) {
    if (pIdArticle != null && Constantes.equals(pIdArticle, idArticle)) {
      return;
    }
    idArticle = pIdArticle;
    
    rafraichir();
  }

  /**
   * Modifier le document sélectionné par l'utilisateur.
   * @param pIndexDocumentSelectionne Index du document de vente sélectionné.
   */
  public void modifierDocumentSelectionne(int pIndexDocumentSelectionne) {
    if (pIndexDocumentSelectionne == getIndexDocumentSelectionne()) {
      return;
    }

    setIndexDocumentSelectionne(pIndexDocumentSelectionne);
    documentVenteSelectionne = listeDocumentVenteBase.get(pIndexDocumentSelectionne);
    rafraichir();
  }
  
  /**
   * Initialiser le résultat de la recherche (remet les valeurs par défaut aux filtres).
   */
  public void initialiserRecherche() {
    // Initialiser les filtres
    numeroDocument = 0;
    numeroFacture = 0;
    typeDocumentVente = EnumTypeDocumentVente.NON_DEFINI;
    clientBase = null;
    idArticle = null;
    chantier = null;
    dateFin = new Date();
    dateDebut = Constantes.getDateDebutMoisPrecedent(dateFin);
    isAfficherDocumentHistorise = false;
    critereDocumentVente.initialiser();
    
    // Effacer les données
    listeDocumentVenteBase = null;
    setListeIdDocumentVente(null);
    setIndexDocumentSelectionne(-1);
    construireTitre();
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Modifier le type de documents à rechercher.
   * @param pTypeDocumentVente Type de document de ventes.
   */
  public void modifierTypeDocumentVente(EnumTypeDocumentVente pTypeDocumentVente) {
    if (Constantes.equals(typeDocumentVente, pTypeDocumentVente)) {
      return;
    }
    
    if (pTypeDocumentVente != null) {
      typeDocumentVente = pTypeDocumentVente;
    }
    else {
      typeDocumentVente = EnumTypeDocumentVente.NON_DEFINI;
    }
    
    rafraichir();
  }
  
  /**
   * Modifier le magasin des documents à rechercher.
   * @param pIdMagasin Identifiant du magasin.
   */
  public void modifierMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
    rafraichir();
  }

  /**
   * Modifier le client à rechercher.
   * @param pClientBase Client de base.
   */
  public void modifierClient(ClientBase pClientBase) {
    if (Constantes.equals(clientBase, pClientBase)) {
      return;
    }
    
    clientBase = pClientBase;
    rafraichir();
  }

  /**
   * Modifie le chantier à rechercher.
   * @param pChantier Chantier.
   */
  public void modifierChantier(Chantier pChantier) {
    chantier = pChantier;
    rafraichir();
  }
  
  /**
   * Modifier la date de début.
   * @param pDate Date de début.
   */
  public void modifierDateCreationDebut(Date pDate) {
    if (Constantes.equals(pDate, dateDebut)) {
      return;
    }
    dateDebut = pDate;
  }
  
  /**
   * Modifier la date de fin.
   * @param pDate Date de fin.
   */
  public void modifierDateCreationFin(Date pDate) {
    if ((pDate == null) || Constantes.equals(pDate, dateFin)) {
      return;
    }
    dateFin = pDate;
  }
  
  /**
   * Modifier l'inclusion des documents historisés dans la recherche.
   * @param pAfficher Inclure ou non les documents historisés.
   */
  public void modifierAffichageDocumentHistorise(boolean pAfficher) {
    if (Constantes.equals(isAfficherDocumentHistorise, pAfficher)) {
      return;
    }
    isAfficherDocumentHistorise = pAfficher;
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Lancer une recherche sur les documents de ventes mais sans déclencher de rafraîchissement.
   */
  private void lancerRechercheInterne() {
    if (critereDocumentVente == null) {
      return;
    }
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeDocumentVenteBase != null) {
      taillePage = listeDocumentVenteBase.getTaillePage();
    }

    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeIdDocumentVente = null;
    listeDocumentVenteBase = null;
    ligneSelectionnee = -1;
    rafraichir();

    // Analyse erreur sur recherche
    if (critereDocumentVente != null && critereDocumentVente.getRechercheGenerique() != null
        && critereDocumentVente.getRechercheGenerique().isEnErreur()) {
      return;
    }

    if (isAfficherDocumentHistorise) {
      critereDocumentVente.setSansHistorique(false);
    }
    else {
      critereDocumentVente.setSansHistorique(true);
    }
    // Charger la liste des identifiants
    listeIdDocumentVente = ManagerServiceDocumentVente.chargerListeIdDocumentVente(getIdSession(), critereDocumentVente);

    // Initier la liste des documents de ventes à partir de la liste d'identifiants
    listeDocumentVenteBase = ListeDocumentVenteBase.creerListeNonChargee(listeIdDocumentVente);

    // Charger la première page
    listeDocumentVenteBase.chargerPremierePage(getIdSession(), taillePage);
  }

  /**
   * Charger les informations complémentaires d'une liste de document de ventes.
   * @param listeDocumentVenteBaseCharge Liste de document de ventes de base à charger.
   */
  private void chargerComplement(ListeDocumentVenteBase listeDocumentVenteBaseCharge) {
    if (listeDocumentVenteBaseCharge == null) {
      return;
    }

    // Lister les identifiants des clients à charger
    ListeIdClient listeIdClient = new ListeIdClient();
    for (DocumentVenteBase documentVenteBase : listeDocumentVenteBaseCharge) {
      IdClient idClient = documentVenteBase.getIdClientFacture();
      if (idClient != null && !listeIdClient.contains(idClient)) {
        listeIdClient.add(documentVenteBase.getIdClientFacture());
      }
    }

    // Charger les clients en complément
    if (listeIdClient.size() > 0) {
      ListeClientBase listeClientBase = ManagerServiceClient.chargerListeClientBase(getIdSession(), listeIdClient);

      // Mettre le client dans le complément du document de ventes
      for (DocumentVenteBase documentVenteBase : listeDocumentVenteBaseCharge) {
        ClientBase clientBase = listeClientBase.getClientBaseParId(documentVenteBase.getIdClientFacture());
        documentVenteBase.setComplement(clientBase);
      }
    }
  }
  
  /**
   * Construire le titre de la liste suivant le résultat de recherche.
   */
  private void construireTitre() {
    if (listeDocumentVenteBase == null) {
      titreResultat = Message.getMessageNormal("Documents de ventes correspondant à votre recherche");
    }
    else if (listeDocumentVenteBase.size() == 1) {
      titreResultat = Message.getMessageNormal("Document de ventes correspondant à votre recherche (1)");
    }
    else if (listeDocumentVenteBase.size() > 1) {
      titreResultat =
          Message.getMessageNormal("Documents de ventes correspondant à votre recherche (" + listeDocumentVenteBase.size() + ")");
    }
    else {
      titreResultat = Message.getMessageImportant("Aucun document de ventes ne correspond à votre recherche");
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner le critère de recherche de document de ventes.
   * @return L'objet critère de recherche de document de ventes.
   */
  public CritereDocumentVente getCritereDocumentVente() {
    return critereDocumentVente;
  }

  /**
   * Définir le critère de recherche de document de ventes.
   * @param pCritereDocumentVente Nouveau critère de recherche de document de ventes.
   */
  public void setCritereDocumentVente(CritereDocumentVente pCritereDocumentVente) {
    critereDocumentVente = pCritereDocumentVente;
  }

  /**
   * Retourner le document de vente sélectionné.
   * @return Le document de vente sélectionné.
   */
  public DocumentVenteBase getDocumentVenteSelectionne() {
    return documentVenteSelectionne;
  }
  
  /**
   * Définir le document de vente à sélectionner.
   * @param pDocumentVenteSelectionne Document de vente à sélectionner.
   */
  public void setDocumentVenteSelectionne(DocumentVente pDocumentVenteSelectionne) {
    documentVenteSelectionne = pDocumentVenteSelectionne;
  }
  
  /**
   * Retourner la liste d'identifiants de document de ventes.
   * @return Liste d'identifiant de document de ventes.
   */
  public List<IdDocumentVente> getListeIdDocumentVente() {
    return listeIdDocumentVente;
  }

  /**
   * Définir la liste d'identifiants de document de ventes.
   * @param pListeIdDocumentVente Liste d'identifiants de document de ventes.
   */
  public void setListeIdDocumentVente(List<IdDocumentVente> pListeIdDocumentVente) {
    listeIdDocumentVente = pListeIdDocumentVente;
  }

  /**
   * Retourner l'indice de la ligne sélectionnée.
   * @return L'indice de la ligne sélectionnée.
   */
  public int getLigneSelectionnee() {
    return ligneSelectionnee;
  }

  /**
   * Définir l'indice de la ligne à sélectionner.
   * @param pLigneSelectionnee Indice de la ligne à sélectionner.
   */
  public void setLigneSelectionnee(int pLigneSelectionnee) {
    ligneSelectionnee = pLigneSelectionnee;
  }
  
  /**
   * Retourner la liste de document de ventes.
   * @return La liste de document de ventes.
   */
  public ListeDocumentVenteBase getListeDocumentVenteBase() {
    return listeDocumentVenteBase;
  }
  
  /**
   * Définir la liste de document de ventes.
   * @param pListeDocumentVenteBase Liste de document de ventes.
   */
  public void setListeDocumentVenteBase(ListeDocumentVenteBase pListeDocumentVenteBase) {
    listeDocumentVenteBase = pListeDocumentVenteBase;
  }

  /**
   * Retourner le numéro du document de ventes.
   * @return Le numéro du document de ventes.
   */
  public int getNumeroDocument() {
    return numeroDocument;
  }

  /**
   * Modifier le numéro du document de ventes.
   * @param pNumeroDocument Nouveau numéro de document de ventes.
   */
  public void setNumeroDocument(int pNumeroDocument) {
    numeroDocument = pNumeroDocument;
  }
  
  /**
   * Retourner le type de document de ventes.
   * @return Le type de document de ventes.
   */
  public EnumTypeDocumentVente getTypeDocumentVente() {
    return typeDocumentVente;
  }
  
  /**
   * Définir le type de document de ventes.
   * @param pTypeDocumentVente Type de document de ventes.
   */
  public void setTypeDocumentVente(EnumTypeDocumentVente pTypeDocumentVente) {
    typeDocumentVente = pTypeDocumentVente;
  }

  /**
   * Retourner l'identifiant de l'établissement.
   * @return L'identifiant de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Définir l'identifiant de l'établissement.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }

  /**
   * Retourner le client de base.
   * @return Le client de base.
   */
  public ClientBase getClientBase() {
    return clientBase;
  }

  /**
   * Définir le client de base.
   * @param pClientBase Client de base.
   */
  public void setClientBase(ClientBase pClientBase) {
    clientBase = pClientBase;
  }
  
  /**
   * Retourner l'identifiant de l'article.
   * @return L'identifiant de l'article.
   */
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  /**
   * Définir l'identifiant de l'article.
   * @param pIdArticle Identifiant de l'article.
   */
  public void setIdArticle(IdArticle pIdArticle) {
    idArticle = pIdArticle;
  }

  /**
   * Retourner le chantier.
   * @return Le chantier.
   */
  public Chantier getChantier() {
    return chantier;
  }

  /**
   * Définir le chantier.
   * @param pChantier Chantier.
   */
  public void setChantier(Chantier pChantier) {
    chantier = pChantier;
  }
  
  /**
   * Retourner l'identifant du magasin.
   * @return L'identifiant du magasin.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Définir l'identifiant du magasin.
   * @param pIdMagasin Identifiant du magasin.
   */
  public void setIdMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
  }

  /**
   * Retourner la date de début.
   * @return La date de début.
   */
  public Date getDateDebut() {
    return dateDebut;
  }

  /**
   * Définir la date de début.
   * @param pDateDebut Date de début.
   */
  public void setDateDebut(Date pDateDebut) {
    dateDebut = pDateDebut;
  }

  /**
   * Retourner la date de fin.
   * @return La date de fin.
   */
  public Date getDateFin() {
    return dateFin;
  }

  /**
   * Définir la date de fin.
   * @param pDateFin Date de fin.
   */
  public void setDateFin(Date pDateFin) {
    dateFin = pDateFin;
  }
  
  /**
   * Retourner le titre du résultat.
   * @return Le titre du résultat.
   */
  public Message getTitreResultat() {
    return titreResultat;
  }
  
  /**
   * Définir le titre du résultat.
   * @param pTitreResultat Titre du résultat.
   */
  public void setTitreResultat(Message pTitreResultat) {
    titreResultat = pTitreResultat;
  }
  
  /**
   * Retourner le nombre de résultats retournés par la recherche.
   * @return Le nombre de résultats de recherche.
   */
  public int getNombreResultat() {
    if (getListeIdDocumentVente() == null) {
      return 0;
    }
    return getListeIdDocumentVente().size();
  }
  
  /**
   * Retourner le numéro de facture.
   * @return Le numéro de facture.
   */
  public int getNumeroFacture() {
    return numeroFacture;
  }
  
  /**
   * Définir le numéro de facture.
   * @param pNumeroFacture Numéro de facture.
   */
  public void setNumeroFacture(int pNumeroFacture) {
    numeroFacture = pNumeroFacture;
  }
  
  /**
   * Retourner l'index du document de vente sélectionné.
   * @return L'index du document de vente sélectionné.
   */
  public int getIndexDocumentSelectionne() {
    return indexDocumentSelectionne;
  }

  /**
   * Retourner la liste du mode d'expédition.
   * @return La liste du mode d'expédition.
   */
  public ListeModeExpedition getListeModeExpedition() {
    return listeModeExpedition;
  }

  /**
   * Définir la liste du mode d'expédition.
   * @param pListeModeExpedition Liste du mode d'expédition.
   */
  public void setListeModeExpedition(ListeModeExpedition pListeModeExpedition) {
    listeModeExpedition = pListeModeExpedition;
  }
  
  /**
   * Retourner l'inclusion des documents historisés dans la recherche.
   * @return
   *         true si on inclut les documents historisés, false sinon.
   */
  public boolean isAfficherDocumentHistorise() {
    return isAfficherDocumentHistorise;
  }
  
}
