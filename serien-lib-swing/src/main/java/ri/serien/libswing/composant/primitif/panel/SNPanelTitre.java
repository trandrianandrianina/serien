/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.panel;

import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Panneau standard avec un cadre et un titre.
 *
 * Ce panneau dipose d'un cadre et d'un titre (le tire peut éventuellement être vide mais ce n'est pas recommandé). Les méthodes
 * getTitre() et setTitre() permettant de modifier facilement le titre.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Titre écrit avec la police réservée aux titres "Sans Serif 14 en gras".
 * - Titre justifié à gauche.
 * - Marge de 5 pixels au dessus du panneau.
 */
public class SNPanelTitre extends JPanel {
  private CompoundBorder compoundBorder = null;
  private TitledBorder titledBorder = null;
  private boolean modeReduit = false;
  private boolean changementMode = false;
  private Font policeTitre = SNCharteGraphique.POLICE_TITRE;

  /**
   * Constructeur standard.
   */
  public SNPanelTitre() {
    super();
    
    // Rendre le composant transparent
    setOpaque(false);
    
    // Créer un bord avec le titre écrit avec la police réservé aux titre
    titledBorder = new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, policeTitre);
    
    // Créer un bord pour ajouter une marge de 5 pixels au dessus du panneau
    compoundBorder = new CompoundBorder(new EmptyBorder(5, 0, 0, 0), titledBorder);
    setBorder(compoundBorder);
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit l'aspect du composant.
   */
  private void rafraichirAspect() {
    // Sélection de la police en fonction du mode
    if (changementMode) {
      if (modeReduit) {
        policeTitre = SNCharteGraphique.POLICE_TITRE_REDUIT;
      }
      else {
        policeTitre = SNCharteGraphique.POLICE_TITRE;
      }
      titledBorder.setTitleFont(policeTitre);
      changementMode = false;
    }
  }

  // -- Accesseurs
  
  /**
   * Active ou non le mode réduit du composant.
   * @param pActiverModeReduit
   */
  public void setModeReduit(boolean pActiverModeReduit) {
    // Contrôle s'il y a un chagement de mode (réduit ou non)
    if (modeReduit != pActiverModeReduit) {
      changementMode = true;
    }
    else {
      changementMode = false;
    }
    modeReduit = pActiverModeReduit;
    rafraichirAspect();
  }

  /**
   * Retourne si le mode réduit est actif.
   * @return
   */
  public boolean isModeReduit() {
    return modeReduit;
  }

  /**
   * Titre du panneau.
   */
  public String getTitre() {
    if (titledBorder == null) {
      throw new MessageErreurException("Impossible de récupérer le titre du panneau car il ne dispose pas de bord adéquat.");
    }
    return titledBorder.getTitle();
  }
  
  /**
   * Modifier le titre du panneau.
   */
  public void setTitre(String pTitre) {
    if (titledBorder == null) {
      throw new MessageErreurException("Impossible de modifier le titre du panneau car il ne dispose pas de bord adéquat.");
    }
    
    // Modifier le titre
    if (pTitre != null) {
      titledBorder.setTitle(pTitre);
    }
    else {
      titledBorder.setTitle(null);
    }
    // Obligatoire sinon le titre du panel n'est pas rafraichit
    repaint();
  }
  
}
