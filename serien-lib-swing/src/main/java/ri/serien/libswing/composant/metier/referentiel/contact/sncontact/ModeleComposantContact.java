/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.contact.sncontact;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import ri.serien.libcommun.commun.EnumModeUtilisation;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.menu.EnumPointMenu;
import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.CategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.ListeCategorieContact;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.CritereContact;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.gescom.commun.contact.ParametreMenuContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.IdLienContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.LienContact;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceCommun;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.confirmation.ModeleDialogueConfirmation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue de sélection d'un contact.
 * SNC-5512 - Provisoirement avorté
 */
public class ModeleComposantContact extends AbstractModeleDialogue {
  // Variables
  private String messageInformation = "";
  private CritereContact critereContact = new CritereContact();
  private List<IdContact> listeIdContact = null;
  private ListeContact listeContact = null;
  private int ligneSelectionnee = -1;
  private Contact contactSelectionnee = null;
  private EnumTypeContact typeContact = null;
  private ListeCategorieContact listeCategorieContact = null;
  private IdFournisseur idFournisseur = null;
  private IdClient idClient = null;
  private IdFournisseur idFournisseurSelectionne = null;
  private IdClient idClientSelectionne = null;
  
  /**
   * Constructeur pour les contacts fournisseurs.
   */
  public ModeleComposantContact(SessionBase pSession, EnumTypeContact pTypeContact, IdFournisseur pIdFournisseur) {
    super(pSession);
    typeContact = pTypeContact;
    idFournisseur = pIdFournisseur;
  }
  
  /**
   * Constructeur pour les contacts clients.
   */
  public ModeleComposantContact(SessionBase pSession, EnumTypeContact pTypeContact, IdClient pIdClient) {
    super(pSession);
    typeContact = pTypeContact;
    idClient = pIdClient;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    IdSession.controlerId(getIdSession(), true);
  }
  
  @Override
  public void chargerDonnees() {
    // Charger la liste des catégories contact
    listeCategorieContact = ManagerServiceParametre.chargerListeCategorieContact(getIdSession());
    
    // Tester si une liste d'identifiant a été fournie
    if (listeIdContact != null) {
      // Charger la liste des contacts si une liste d'identifiants a été fournie par l'appelant
      listeContact = ListeContact.creerListeNonChargee(listeIdContact);
      
      // Trier les contacts par identifiant
      // listeContact.trierParId();
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    // Vérifier si la sélection est valide
    if (!isSelectionValide()) {
      throw new MessageErreurException("Le contact sélectionné n'est pas valide.");
    }
    
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    super.quitterAvecAnnulation();
  }
  
  /**
   * Charger la liste des identifiants des contacts correspondant aux critères de recherche.
   */
  private void chargerListe() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeContact != null) {
      taillePage = listeContact.getTaillePage();
    }
    
    // Tout déselectionner
    listeIdContact = null;
    listeContact = null;
    ligneSelectionnee = -1;
    contactSelectionnee = null;
    rafraichir();
    
    switch (typeContact) {
      case CLIENT:
      case PROSPECT:
        if (idClientSelectionne != null) {
          critereContact.setIdClient(idClientSelectionne);
        }
        else {
          critereContact.setIdClient(idClient);
        }
        critereContact.setIdFournisseur(null);
        break;
      
      case FOURNISSEUR:
        if (idFournisseurSelectionne != null) {
          critereContact.setIdFournisseur(idFournisseurSelectionne);
        }
        else {
          critereContact.setIdFournisseur(idFournisseur);
        }
        critereContact.setIdClient(null);
        break;
      
      default:
        break;
    }
    
    // Charger les identifiants des contacts correspondants à la recherche
    listeIdContact = ManagerServiceCommun.chargerListeIdContact(getIdSession(), critereContact);
    
    // Initier la liste des contacts à partir des identifiants
    listeContact = ListeContact.creerListeNonChargee(listeIdContact);
    
    // Trier les contacts par identifiant
    // listeContact.trierParId();
    
    // Charger la première page
    listeContact.chargerPremierePage(getIdSession(), taillePage);
  }
  
  /**
   * Afficher la plage des contacts comprises entre deux lignes.
   * Les informations des contacts sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageContactAffiches(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeContact != null) {
      listeContact.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
      
    }
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Affiche la liste des contacts possibles à partir des critères de recherche.
   */
  public void modifierRechercheGenerique(String pRechercheGenerique) {
    pRechercheGenerique = Constantes.normerTexte(pRechercheGenerique);
    if (Constantes.equals(pRechercheGenerique, critereContact.getTexteRechercheGenerique())) {
      return;
    }
    critereContact.setIdClient(null);
    critereContact.setTexteRechercheGenerique(pRechercheGenerique);
    chargerListe();
    rafraichir();
  }
  
  /**
   * Modifier le filtre sur la fonction du contact.
   */
  public void modifierCategorieContact(CategorieContact pCategorieContact) {
    if (pCategorieContact == null) {
      critereContact.setIdCategorieContact(null);
    }
    else if (Constantes.equals(critereContact.getIdCategorieContact(), pCategorieContact.getId())) {
      return;
    }
    else {
      critereContact.setIdCategorieContact(pCategorieContact.getId());
    }
  }
  
  public void initialiserRecherche() {
    idClientSelectionne = null;
    idFournisseurSelectionne = null;
    switch (typeContact) {
      case CLIENT:
      case PROSPECT:
        critereContact.initialiserContactClient(typeContact, idClient);
        break;
      case FOURNISSEUR:
        critereContact.initialiserContactFournisseur(typeContact, idFournisseur);
        break;
      default:
        throw new MessageErreurException("Cette recherche n'est pas prise en compte.");
    }
    chargerListe();
    rafraichir();
  }
  
  /**
   * Rechercher la liste des contacts possibles à partir des critères de recherche.
   */
  public void rechercher() {
    chargerListe();
    rafraichir();
  }
  
  /**
   * Modifie l'indice de la ligne séléctionnée dans le tableau.
   */
  public void modifierLigneSelectionnee(int pLigneSelectionnee) {
    if (pLigneSelectionnee == ligneSelectionnee) {
      return;
    }
    
    // Mémoriser l'indice de la ligne sélectionnée
    ligneSelectionnee = pLigneSelectionnee;
    
    // Sélectionner le contact correspondant à la ligne sélectionnée
    if (listeContact != null && 0 <= pLigneSelectionnee && pLigneSelectionnee < listeContact.size()) {
      contactSelectionnee = listeContact.get(pLigneSelectionnee);
    }
    else {
      contactSelectionnee = null;
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  public void modifierClientSelectionne(IdClient pIdClientSelectionne) {
    idClientSelectionne = pIdClientSelectionne;
    rafraichir();
  }
  
  public void modifierFournisseurSelectionne(IdFournisseur pIdFournisseurSelectionne) {
    idFournisseurSelectionne = pIdFournisseurSelectionne;
    rafraichir();
  }
  
  /**
   * Créer un nouveau contact.
   */
  public void creerContact() {
    // Lancement d'un point de menu à partir de son identifiant
    ParametreMenuContact parametres = new ParametreMenuContact();
    parametres.setModeUtilisation(EnumModeUtilisation.CREATION);
    parametres.setIdClient(idClient);
    parametres.setIdFournisseur(idFournisseur);
    ManagerSessionClient.getInstance().lancerPointMenu(getSession(), IdEnregistrementMneMnp.getInstance(EnumPointMenu.CONTACTS),
        parametres);
  }
  
  /**
   * Modifier un contact
   */
  public void modifierContact() {
    ParametreMenuContact parametres = new ParametreMenuContact();
    parametres.setModeUtilisation(EnumModeUtilisation.MODIFICATION);
    parametres.setIdClient(idClient);
    parametres.setIdFournisseur(idFournisseur);
    if (idClient != null) {
      parametres.setIdLienContact(IdLienContact.getInstancePourClient(contactSelectionnee.getId().getCode(), idClient));
    }
    else if (idFournisseur != null) {
      parametres.setIdLienContact(IdLienContact.getInstancePourFournisseur(contactSelectionnee.getId().getCode(), idFournisseur));
    }
    ManagerSessionClient.getInstance().lancerPointMenu(getSession(), IdEnregistrementMneMnp.getInstance(EnumPointMenu.CONTACTS),
        parametres);
  }

  /**
   * Rédiger un mail pour le contact sélectionné
   */
  public void redigerMail() {
    try {
      URI uriMailTo = new URI("mailto", contactSelectionnee.getEmail(), null);
      Desktop.getDesktop().mail(uriMailTo);
    }
    catch (IOException ex) {
    }
    catch (URISyntaxException ex) {
      throw new MessageErreurException(
          "Le contact sélectionné ne possède pas d'adresse e-mail valide.\nModifiez ce contact pour ajouter son adresse électronique.");
    }
  }
  
  /**
   * Créer un lien entre le tiers en cours et le contact sélectionné.
   */
  public void creerLien() {
    if (contactSelectionnee == null) {
      return;
    }
    
    // teste si le contact est déjà lié au tiers
    for (LienContact lien : contactSelectionnee.getListeLienContact()) {
      if (lien.getId().getIdClient() != null && lien.getId().getIdClient().equals(idClient)) {
        throw new MessageErreurException(
            "Ce contact est déjà lié au tiers en cours.\nRecherchez le contact à lier parmi les contacts d'un autre client.");
      }
      if (lien.getId().getIdFournisseur() != null && lien.getId().getIdFournisseur().equals(idFournisseur)) {
        throw new MessageErreurException(
            "Ce contact est déjà lié au tiers en cours.\nRecherchez le contact à lier parmi les contacts d'un autre fournisseur.");
      }
    }
    
    LienContact lienContact = null;
    if (idClient != null) {
      lienContact = new LienContact(IdLienContact.getInstancePourClient(contactSelectionnee.getId().getCode(), idClient));
      Client clientEnCours = ManagerServiceClient.chargerClient(getIdSession(), idClient);
      lienContact.setAdresseTiers(clientEnCours.getAdresse());
      lienContact.setLibelleTiers(clientEnCours.getTexte());
    }
    else if (idFournisseur != null) {
      lienContact = new LienContact(IdLienContact.getInstancePourFournisseur(contactSelectionnee.getId().getCode(), idFournisseur));
      Fournisseur fournisseurEnCours = ManagerServiceFournisseur.chargerFournisseur(getIdSession(), idFournisseur);
      lienContact.setAdresseTiers(fournisseurEnCours.getAdresseSiege().getAdresse());
      lienContact.setLibelleTiers(fournisseurEnCours.getTexte());
    }
    contactSelectionnee.getListeLienContact().add(lienContact);
    ManagerServiceCommun.sauverContact(getIdSession(), contactSelectionnee);
    initialiserRecherche();
    rechercher();
  }
  
  /**
   * Supprimer le lien entre le tiers en cours et le contact sélectionné.
   */
  public void supprimerLien() {
    if (contactSelectionnee == null) {
      return;
    }
    
    IdLienContact idLienSupprime = null;
    String messageConfirmation = "Vous avez demandé la suppression du lien entre ";
    
    // Recherche de l'identifiant du lien contact/tiers
    if (idClient != null) {
      idLienSupprime = IdLienContact.getInstancePourClient(contactSelectionnee.getId().getCode(), idClient);
      messageConfirmation += "le client en cours (" + idClient.getTexte();
    }
    else if (idFournisseur != null) {
      idLienSupprime = IdLienContact.getInstancePourFournisseur(contactSelectionnee.getId().getCode(), idFournisseur);
      messageConfirmation += "le fournisseur en cours (" + idFournisseur.getTexte();
    }
    else {
      return;
    }
    
    messageConfirmation += ") et le contact " + contactSelectionnee.getTexte() + ".\n\nConfirmez-vous cette demande ?";
    
    // Suppression du LienContact de la liste du contact sélectionné
    LienContact lienSupprime = contactSelectionnee.getListeLienContact().get(idLienSupprime);
    contactSelectionnee.getListeLienContact().remove(lienSupprime);

    // Erreur et sortie si il n'y a qu'un seul lien et pas détecté avant
    if (listeContact.size() <= 1) {
      throw new MessageErreurException(
          "Ce tiers n'est lié qu'à un seul contact : vous ne pouvez pas supprimer le dernier contact d'un tiers."
              + "\nAjoutez un nouveau contact pour ce tiers avant de supprimer le contact sélectionné.");
    }

    // Erreur et sortie si le contact n'est lié qu'au tiers en cours
    if (contactSelectionnee.getListeLienContact().size() == 0) {
      throw new MessageErreurException("Ce contact a pour seul lien le tiers en cours : vous ne pouvez pas supprimer leur lien.\n"
          + "Rendez-vous dans la gestion des contacts et supprimez ce contact pour qu'il n'apparaisse "
          + "plus dans la liste des contacts liés à ce tiers.");
    }

    // Message de confirmation
    ModeleDialogueConfirmation modeleConfirmation =
        new ModeleDialogueConfirmation(getSession(), "Supprimer le lien tiers/contact", messageConfirmation);
    DialogueConfirmation vue = new DialogueConfirmation(modeleConfirmation);
    vue.afficher();
    
    // Si l'utilisateur confirme, on sauvegarde la suppression en base
    if (modeleConfirmation.isSortieAvecValidation()) {
      ManagerServiceCommun.sauverContact(getIdSession(), contactSelectionnee);
      rechercher();
      rafraichir();
    }
  }
  
  public boolean isContactPrincipal(Contact pContact) {
    if (pContact == null) {
      return false;
    }
    
    switch (typeContact) {
      case CLIENT:
      case PROSPECT:
        if (pContact.equals(listeContact.getContactPrincipal(idClient))) {
          return true;
        }
        else {
          return false;
        }
        
      case FOURNISSEUR:
        if (pContact.equals(listeContact.getContactPrincipal(idFournisseur))) {
          return true;
        }
        else {
          return false;
        }
        
      default:
        throw new MessageErreurException("Cette recherche n'est pas prise en compte.");
    }
  }
  
  /**
   * Message d'information à afficher dans la boîte de dialogue.
   */
  public Message getTitreListe() {
    if (listeIdContact == null) {
      return Message.getMessageImportant("Aucun contact ne correspond à votre recherche");
    }
    else if (listeIdContact.size() == 1) {
      return Message.getMessageNormal("Contact correspondant à votre recherche (1)");
    }
    else if (listeIdContact.size() > 1) {
      return Message.getMessageNormal("Contacts correspondants à votre recherche (" + listeIdContact.size() + ")");
    }
    else {
      return Message.getMessageNormal("Saisir les critères de recherche");
    }
  }
  
  /**
   * Message d'informatino pour expliquer pourquoi on a besoin de sléectionner un contact.
   */
  public String getMessageInformation() {
    return messageInformation;
  }
  
  /**
   * Modifier le messgage d'information.
   */
  public void setMessageInformation(String pMessageInformation) {
    messageInformation = pMessageInformation;
  }
  
  /**
   * Critère de recherche des contacts.
   */
  public CritereContact getCritereContact() {
    return critereContact;
  }
  
  /**
   * Fournir les critères de recherche des contacts à la boîte de dialogue.
   */
  public void setCritereContact(CritereContact pCritereContact) {
    if (pCritereContact != null) {
      critereContact = pCritereContact;
    }
    else {
      critereContact = new CritereContact();
    }
  }
  
  /**
   * Liste des contacts résultat de la recherche.
   */
  public List<IdContact> getListeIdContact() {
    return listeIdContact;
  }
  
  /**
   * Fournir le résultat de la recherche à la boîte de dialogue.
   */
  public void setListeIdContact(List<IdContact> pListeIdContact) {
    listeIdContact = pListeIdContact;
  }
  
  /**
   * Liste des contacts.
   */
  public List<Contact> getListeContact() {
    return listeContact;
  }
  
  /**
   * Numéro de la ligne sélectionnée.
   */
  public int getLigneSelectionnee() {
    return ligneSelectionnee;
  }
  
  /**
   * Identifiant du contact sélectionnée (null si aucune sélection).
   */
  public Contact getContactSelectionnee() {
    return contactSelectionnee;
  }
  
  /**
   * Indique si le contact sélectionnée est valide.
   */
  public boolean isSelectionValide() {
    return contactSelectionnee != null;
  }
  
  /**
   * Indique si le contact sélectionnée est en mesure d'être lié au tiers en cours
   */
  public boolean isLiaisonValide() {
    return contactSelectionnee != null && (idClientSelectionne != null || idFournisseurSelectionne != null);
  }
  
  public ListeCategorieContact getListeCategorieContact() {
    return listeCategorieContact;
  }
  
  /**
   * Fournir la fonction du contact à rechercher.
   */
  public CategorieContact getFiltreCategorieContact() {
    if (listeCategorieContact == null) {
      return null;
    }
    return listeCategorieContact.get(critereContact.getIdCategorieContact());
  }
  
  /**
   * Retourner le fournisseur sélectionné par l'utilisateur
   * Ce fournisseur peut être différent du fournisseur de la fiche en cours. Permet de sélectionner d'autres contacts pour les lier
   */
  public IdFournisseur getIdFournisseurSelectionne() {
    return idFournisseurSelectionne;
  }
  
  /**
   * Retourner le client sélectionné par l'utilisateur
   * Ce client peut être différent du client de la fiche en cours. Permet de sélectionner d'autres contacts pour les lier
   */
  public IdClient getIdClientSelectionne() {
    return idClientSelectionne;
  }
  
  /**
   * Retourner le fournisseur sélectionné par le programme
   * C'est à dire le fournisseur de la fiche en cours, si précisé
   */
  public IdFournisseur getIdFournisseur() {
    return idFournisseur;
  }
  
  /**
   * Retourner le client sélectionné par le programme
   * C'est à dire le client de la fiche en cours, si précisé
   */
  public IdClient getIdClient() {
    return idClient;
  }
  
  public EnumTypeContact getTypeContact() {
    return typeContact;
  }
  
}
