/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.stock.sntypestockagemagasin;

import ri.serien.libcommun.gescom.personnalisation.typestockagemagasin.IdTypeStockageMagasin;
import ri.serien.libcommun.gescom.personnalisation.typestockagemagasin.ListeTypeStockageMagasin;
import ri.serien.libcommun.gescom.personnalisation.typestockagemagasin.TypeStockageMagasin;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un TypeStockageMagasin.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un TypeStockageMagasin dans le logiciel. Il gère l'affichage des
 * TypeStockageMagasin existants et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant TypeStockageMagasin à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le VGVM01FX_VD.
 */
public class SNTypeStockageMagasin extends SNComboBoxObjetMetier<IdTypeStockageMagasin, TypeStockageMagasin, ListeTypeStockageMagasin> {
  /**
   * Constructeur par défaut
   */
  public SNTypeStockageMagasin() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeTypeStockageMagasin());
  }
  
  /**
   * Sélectionner un TypeStockageMagasin de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampTypeStockageMagasin) {
    if (pChampTypeStockageMagasin == null || pLexical.HostFieldGetData(pChampTypeStockageMagasin) == null) {
      throw new MessageErreurException("Impossible de sélectionner le type de stockage magasin car son code est invalide.");
    }
    
    IdTypeStockageMagasin idTypeStockageMagasin = null;
    String valeurTypeGratuit = pLexical.HostFieldGetData(pChampTypeStockageMagasin);
    if (valeurTypeGratuit != null && !valeurTypeGratuit.trim().isEmpty()) {
      idTypeStockageMagasin =
          IdTypeStockageMagasin.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampTypeStockageMagasin));
    }
    setIdSelection(idTypeStockageMagasin);
  }
  
  /**
   * Renseigner les champs RPG correspondant au TypeStockageMagasin sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampTypeStockageMagasin) {
    TypeStockageMagasin typeStockageMagasin = getSelection();
    if (typeStockageMagasin != null) {
      pLexical.HostFieldPutData(pChampTypeStockageMagasin, 0, typeStockageMagasin.getId().toString());
    }
    else {
      pLexical.HostFieldPutData(pChampTypeStockageMagasin, 0, "");
    }
  }
}
