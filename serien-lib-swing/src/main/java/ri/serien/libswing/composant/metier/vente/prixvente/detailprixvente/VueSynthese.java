/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.ParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.ParametreClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.ParametreDocumentVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.checkbox.SNCheckBoxReduit;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de synthèse du calcul d'un prix de vente.
 */
public class VueSynthese extends AbstractVuePanel<ModeleDialogueDetailPrixVente> {
  private static final String TITRE_ETABLISSEMENT = "Etablissement";
  private static final String TITRE_CLIENT_FACTURE = "Client facturé";
  private static final String TITRE_CLIENT_LIVRE = "Client livré";
  private static final String TITRE_ARTICLE = "Article";
  private static final String TITRE_DOCUMENT_VENTE = "Document de vente";
  private static final String TITRE_RESULTAT = "Résultat";
  private static final String PREFIXE_ORIGINE = " = ";

  private static final String[] TITRE_COLONNE = new String[] { "Libellé", "Type", "Rattachement client", "Rattachement article",
      "Prix base HT", "Prix net HT", "Montant HT", "Commentaire" };
  private static final int[] TAILLE_COLONNE = new int[] { 150, 150, 200, 200, 80, 80, 80, 500 };
  private static final int[] TAILLE_COLONNE_MAX = new int[] { 150, 150, 300, 300, 100, 100, 100, 5000 };
  private static final int[] JUSTIFICATION_COLONNE = new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE,
      NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.GAUCHE };

  private CalculPrixVente calculPrixVente = null;
  private ParametreEtablissement parametreEtablissement = null;
  private ParametreClient parametreClientFacture = null;
  private ParametreClient parametreClientLivre = null;
  private ParametreArticle parametreArticle = null;
  private ParametreDocumentVente parametreDocumentVente = null;
  private ParametreTarif parametreTarif = null;
  private FormulePrixVente formulePrixVente = null;
  private int nombreDecimale = ArrondiPrix.DECIMALE_STANDARD;

  /**
   * Contructeur.
   */
  public VueSynthese(ModeleDialogueDetailPrixVente pModele) {
    super(pModele);
  }

  /**
   * Initialiser les composants graphiques de la boîte de dialogue.
   */
  @Override
  public void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();

    spListeConditionVente.getViewport().setBackground(Color.WHITE);
    tblListeFormulePrixVente.personnaliserAspect(TITRE_COLONNE, TAILLE_COLONNE, TAILLE_COLONNE_MAX, JUSTIFICATION_COLONNE, 11);
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir les données du composant.
   */
  @Override
  public void rafraichir() {
    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    calculPrixVente = getModele().getCalculPrixVente();
    parametreEtablissement = getModele().getParametreEtablissement();
    parametreClientFacture = getModele().getParametreClientFacture();
    parametreClientLivre = getModele().getParametreClientLivre();
    parametreArticle = getModele().getParametreArticle();
    parametreDocumentVente = getModele().getParametreDocumentVente();
    parametreTarif = getModele().getParametreTarif();
    formulePrixVente = getModele().getFormulePrixVenteFinal();

    // Déterminer la précision souhaitée pour l'affichage des montants
    if (calculPrixVente != null) {
      nombreDecimale = calculPrixVente.getArrondiNombreDecimale();
    }
    else {
      nombreDecimale = ArrondiPrix.DECIMALE_STANDARD;
    }

    // Rafraichir l'encart établissement
    rafraichirTitreEtablissement();
    rafraichirMoisEnCoursDG();
    rafraichirDeviseEtablissement();
    rafraichirGroupeCNVStandardEtablissement();
    rafraichirGroupeCNVPromoEtablissement();
    rafraichirCNVClientArticlePrioritaire();

    // Rafraichir l'encart client facturé
    rafraichirTitreClientFacture();
    rafraichirIdentifiantClientFactureClientFacture();
    rafraichirDeviseClientFacture();
    rafraichirCentraleAchat1ClientFacture();
    rafraichirCentraleAchat2ClientFacture();
    rafraichirCentraleAchat3ClientFacture();
    rafraichirGroupeCNVStandardClientFacture();
    rafraichirGroupeCNVPromoClientFacture();
    rafraichirExclusionCNVEtablissementClientFacture();
    rafraichirIgnorerCNVQuantitativeClientFacture();

    // Rafraichir l'encart client livré
    rafraichirTitreClientLivre();
    rafraichirIdentifiantClientFactureClientLivre();
    rafraichirDeviseClientLivre();
    rafraichirCentraleAchat1ClientLivre();
    rafraichirCentraleAchat2ClientLivre();
    rafraichirCentraleAchat3ClientLivre();
    rafraichirGroupeCNVStandardClientLivre();
    rafraichirGroupeCNVPromoClientLivre();
    rafraichirExclusionCNVEtablissementClientLivre();
    rafraichirIgnorerCNVQuantitativeClientLivre();

    // Rafraichir l'encart article
    rafraichirTitreArticle();
    rafraichirFamille();
    rafraichirSousFamille();
    rafraichirIdFournisseur();
    rafraichirIdReferenceTarif();
    rafraichirRegroupementAchat();
    rafraichirRattachementCNV();

    // Rafraichir l'encart document de vente
    rafraichirTitreDocumentVente();
    rafraichirDateCreation();
    rafraichirDeviseDocumentVente();
    rafraichirCodeConditionVente();
    rafraichirNumeroClientCentrale1();

    // Rafraichir l'encart résultat du calcul
    rafraichirTitreResultat();
    rafraichirDateApplication();
    rafraichirOrigineDevise();
    rafraichirDevise();
    rafraichirOriginePrixVente();
    rafraichirColonneTarif();
    rafraichirPrixBaseHT();
    rafraichirPrixBaseTTC();
    rafraichirTauxRemiseUnitaire();
    rafraichirPrixNetHT();
    rafraichirPrixNetTTC();
    rafraichirQuantiteUV();
    rafraichirMontantHT();
    rafraichirMontantTTC();

    // Rafraichir la liste
    rafraichirListe();
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir pour l'encart établissement
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir le titre de l'encart établissement.
   */
  private void rafraichirTitreEtablissement() {
    if (parametreEtablissement != null && parametreEtablissement.getIdEtablissement() != null) {
      pnlEtablissement.setTitre(TITRE_ETABLISSEMENT + " " + parametreEtablissement.getIdEtablissement());
    }
    else {
      pnlEtablissement.setTitre(TITRE_ETABLISSEMENT);
    }
  }

  /**
   * Rafraichir la date du mois en cours de la DG (paramètres généraux).
   */
  private void rafraichirMoisEnCoursDG() {
    if (parametreEtablissement != null && parametreEtablissement.getMoisEnCours() != null) {
      tfMoisEnCoursDG.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, parametreEtablissement.getMoisEnCours()));
    }
    else {
      tfMoisEnCoursDG.setText("");
    }
  }

  /**
   * Rafraichir la devise de l'établissement.
   */
  private void rafraichirDeviseEtablissement() {
    if (parametreEtablissement != null && parametreEtablissement.getCodeDevise() != null) {
      tfDeviseEtablissement.setText(parametreEtablissement.getCodeDevise());
    }
    else {
      tfDeviseEtablissement.setText("");
    }
  }

  /**
   * Rafraichir le groupe de conditions de ventes standards de l'établissement.
   */
  private void rafraichirGroupeCNVStandardEtablissement() {
    if (parametreEtablissement != null && parametreEtablissement.getIdGroupeConditionVenteGenerale() != null) {
      tfGroupeCNVStandardEtablissement.setText(parametreEtablissement.getIdGroupeConditionVenteGenerale().getCode());
    }
    else {
      tfGroupeCNVStandardEtablissement.setText("");
    }
  }

  /**
   * Rafraichir le groupe de conditions de ventes promotionnelles de l'établissement.
   */
  private void rafraichirGroupeCNVPromoEtablissement() {
    if (parametreEtablissement != null && parametreEtablissement.getIdGroupeConditionVentePromotionnelle() != null) {
      tfGroupeCNVPromoEtablissement.setText(parametreEtablissement.getIdGroupeConditionVentePromotionnelle().getCode());
    }
    else {
      tfGroupeCNVPromoEtablissement.setText("");
    }
  }

  /**
   * Rafraichir si une condition client/article avec prix net est prioritaire.
   */
  private void rafraichirCNVClientArticlePrioritaire() {
    if (parametreEtablissement != null && parametreEtablissement.isConditionVenteClientArticlePourPrixNetPrioritaire()) {
      cbCNVClientArticlePrioritaire.setSelected(true);
    }
    else {
      cbCNVClientArticlePrioritaire.setSelected(false);
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir pour l'encart client facturé
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir le titre de l'encart client facturé.
   */
  private void rafraichirTitreClientFacture() {
    if (parametreClientFacture != null && parametreClientFacture.getIdClient() != null) {
      pnlClientFacture.setTitre(TITRE_CLIENT_FACTURE + " " + parametreClientFacture.getIdClient());
    }
    else {
      pnlClientFacture.setTitre(TITRE_CLIENT_FACTURE);
    }
  }

  /**
   * Rafraichir la devise du client facturé.
   */
  private void rafraichirDeviseClientFacture() {
    if (parametreClientFacture != null && parametreClientFacture.getCodeDevise() != null) {
      tfDeviseClientFacture.setText(parametreClientFacture.getCodeDevise());
    }
    else {
      tfDeviseClientFacture.setText("");
    }
  }

  /**
   * Rafraichir l'identifiant client facturé (paramètres client facturé).
   */
  private void rafraichirIdentifiantClientFactureClientFacture() {
    if (parametreClientFacture != null && parametreClientFacture.getIdClientFacture() != null) {
      tfIdentifiantClientFactureClientFacture.setText(parametreClientFacture.getIdClientFacture().getTexte());
    }
    else {
      tfIdentifiantClientFactureClientFacture.setText("");
    }
  }

  /**
   * Rafraichir le numéro de la centrale d'achat 1 du client (paramètres client facturé).
   */
  private void rafraichirCentraleAchat1ClientFacture() {
    if (parametreClientFacture == null) {
      tfNumeroCentraleAchat1ClientFacture.setText("");
      return;
    }

    if (parametreClientFacture.getNumeroCentraleAchat1() == null) {
      tfNumeroCentraleAchat1ClientFacture.setText("");
    }
    else {
      tfNumeroCentraleAchat1ClientFacture.setText(parametreClientFacture.getNumeroCentraleAchat1().toString());
    }
  }
  
  /**
   * Rafraichir le numéro de la centrale d'achat 2 du client (paramètres client facturé).
   */
  private void rafraichirCentraleAchat2ClientFacture() {
    if (parametreClientFacture == null) {
      tfNumeroCentraleAchat2ClientFacture.setText("");
      return;
    }

    if (parametreClientFacture.getNumeroCentraleAchat1() == null) {
      tfNumeroCentraleAchat2ClientFacture.setText("");
    }
    else {
      tfNumeroCentraleAchat2ClientFacture.setText(parametreClientFacture.getNumeroCentraleAchat2().toString());
    }
  }
  
  /**
   * Rafraichir le numéro de la centrale d'achat 3 du client (paramètres client facturé).
   */
  private void rafraichirCentraleAchat3ClientFacture() {
    if (parametreClientFacture == null) {
      tfNumeroCentraleAchat3ClientFacture.setText("");
      return;
    }

    if (parametreClientFacture.getNumeroCentraleAchat3() == null) {
      tfNumeroCentraleAchat3ClientFacture.setText("");
    }
    else {
      tfNumeroCentraleAchat3ClientFacture.setText(parametreClientFacture.getNumeroCentraleAchat3().toString());
    }
  }
  
  /**
   * Rafraichir le groupe de conditions de ventes standards du client facturé.
   */
  private void rafraichirGroupeCNVStandardClientFacture() {
    if (parametreClientFacture != null && parametreClientFacture.getCodeConditionVente() != null) {
      tfGroupeCNVStandardClientFacture.setText(parametreClientFacture.getCodeConditionVente().toString());
    }
    else {
      tfGroupeCNVStandardClientFacture.setText("");
    }
  }

  /**
   * Rafraichir le groupe de conditions de ventes promotionnelles du client facturé.
   */
  private void rafraichirGroupeCNVPromoClientFacture() {
    if (parametreClientFacture != null && parametreClientFacture.getCodeConditionVentePromo() != null) {
      tfGroupeCNVPromoClientFacture.setText(parametreClientFacture.getCodeConditionVentePromo().toString());
    }
    else {
      tfGroupeCNVPromoClientFacture.setText("");
    }
  }

  /**
   * Rafraichir si les conditions de la DG doivent être ignorées (paramètres client facturé).
   */
  private void rafraichirExclusionCNVEtablissementClientFacture() {
    if (parametreClientFacture != null && parametreClientFacture.getRegleExclusionConditionVenteEtablissement() != null) {
      tfExclusionCNVEtablissementClientFacture
          .setText(parametreClientFacture.getRegleExclusionConditionVenteEtablissement().getLibelle());
    }
    else {
      tfExclusionCNVEtablissementClientFacture.setText("");
    }
  }

  /**
   * Rafraichir si les conditions quantitatives doivent être ignorées (paramètres client facturé).
   */
  private void rafraichirIgnorerCNVQuantitativeClientFacture() {
    if (parametreClientFacture == null || parametreClientFacture.isConditionVenteQuantitativeApplicable()) {
      cbIgnorerCNVQuantitativeClientFacture.setSelected(false);
    }
    else {
      cbIgnorerCNVQuantitativeClientFacture.setSelected(true);
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir pour l'encart client livré
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir le titre de l'encart client livré.
   */
  private void rafraichirTitreClientLivre() {
    if (parametreClientLivre != null && parametreClientLivre.getIdClient() != null) {
      pnlClientLivre.setTitre(TITRE_CLIENT_LIVRE + " " + parametreClientLivre.getIdClient());
    }
    else {
      pnlClientLivre.setTitre(TITRE_CLIENT_LIVRE);
    }
  }

  /**
   * Rafraichir la devise du client livré.
   */
  private void rafraichirDeviseClientLivre() {
    if (parametreClientLivre != null && parametreClientLivre.getCodeDevise() != null) {
      tfDeviseClientLivre.setText(parametreClientLivre.getCodeDevise());
    }
    else {
      tfDeviseClientLivre.setText("");
    }
  }

  /**
   * Rafraichir l'identifiant client facturé (paramètres client livré).
   */
  private void rafraichirIdentifiantClientFactureClientLivre() {
    if (parametreClientLivre != null && parametreClientLivre.getIdClientFacture() != null) {
      tfIdentifiantClientFactureClientLivre.setText(parametreClientLivre.getIdClientFacture().getTexte());
    }
    else {
      tfIdentifiantClientFactureClientLivre.setText("");
    }
  }

  /**
   * Rafraichir le numéro de la centrale d'achat 1 du client (paramètres client livré).
   */
  private void rafraichirCentraleAchat1ClientLivre() {
    if (parametreClientLivre == null) {
      tfNumeroCentraleAchat1ClientLivre.setText("");
      return;
    }

    if (parametreClientLivre.getNumeroCentraleAchat1() == null) {
      tfNumeroCentraleAchat1ClientLivre.setText("");
    }
    else {
      tfNumeroCentraleAchat1ClientLivre.setText(parametreClientLivre.getNumeroCentraleAchat1().toString());
    }
  }
  
  /**
   * Rafraichir le numéro de la centrale d'achat 2 du client (paramètres client livré).
   */
  private void rafraichirCentraleAchat2ClientLivre() {
    if (parametreClientLivre == null) {
      tfNumeroCentraleAchat2ClientLivre.setText("");
      return;
    }

    if (parametreClientLivre.getNumeroCentraleAchat2() == null) {
      tfNumeroCentraleAchat2ClientLivre.setText("");
    }
    else {
      tfNumeroCentraleAchat2ClientLivre.setText(parametreClientLivre.getNumeroCentraleAchat2().toString());
    }
  }

  /**
   * Rafraichir le numéro de la centrale d'achat 3 du client (paramètres client livré).
   */
  private void rafraichirCentraleAchat3ClientLivre() {
    if (parametreClientLivre == null) {
      tfNumeroCentraleAchat3ClientLivre.setText("");
      return;
    }

    if (parametreClientLivre.getNumeroCentraleAchat3() == null) {
      tfNumeroCentraleAchat3ClientLivre.setText("");
    }
    else {
      tfNumeroCentraleAchat3ClientLivre.setText(parametreClientLivre.getNumeroCentraleAchat3().toString());
    }
  }

  /**
   * Rafraichir le groupe de conditions de ventes standards du client livré.
   */
  private void rafraichirGroupeCNVStandardClientLivre() {
    if (parametreClientLivre != null && parametreClientLivre.getCodeConditionVente() != null) {
      tfGroupeCNVStandardClientLivre.setText(parametreClientLivre.getCodeConditionVente().toString());
    }
    else {
      tfGroupeCNVStandardClientLivre.setText("");
    }
  }

  /**
   * Rafraichir le groupe de conditions de ventes promotionnelles du client livré.
   */
  private void rafraichirGroupeCNVPromoClientLivre() {
    if (parametreClientLivre != null && parametreClientLivre.getCodeConditionVentePromo() != null) {
      tfGroupeCNVPromoClientLivre.setText(parametreClientLivre.getCodeConditionVentePromo().toString());
    }
    else {
      tfGroupeCNVPromoClientLivre.setText("");
    }
  }

  /**
   * Rafraichir si les conditions de la DG doivent être ignorées (paramètres client livré).
   */
  private void rafraichirExclusionCNVEtablissementClientLivre() {
    if (parametreClientLivre != null && parametreClientLivre.getRegleExclusionConditionVenteEtablissement() != null) {
      tfExclusionCNVEtablissementClientLivre.setText(parametreClientLivre.getRegleExclusionConditionVenteEtablissement().getLibelle());
    }
    else {
      tfExclusionCNVEtablissementClientLivre.setText("");
    }
  }

  /**
   * Rafraichir si les conditions quantitatives doivent être ignorées (paramètres client livré).
   */
  private void rafraichirIgnorerCNVQuantitativeClientLivre() {
    if (parametreClientLivre == null || parametreClientLivre.isConditionVenteQuantitativeApplicable()) {
      cbIgnorerCNVQuantitativeClientLivre.setSelected(false);
    }
    else {
      cbIgnorerCNVQuantitativeClientLivre.setSelected(true);
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir pour l'encart article
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirTitreArticle() {
    if (parametreArticle != null && parametreArticle.getIdArticle() != null) {
      pnlArticle.setTitre(TITRE_ARTICLE + " " + parametreArticle.getIdArticle());
    }
    else {
      pnlArticle.setTitre(TITRE_ARTICLE);
    }
  }

  /**
   * Rafraichir la famille de l'article.
   */
  private void rafraichirFamille() {
    if (parametreArticle != null && parametreArticle.getFamille() != null) {
      tfFamille.setText(parametreArticle.getFamille());
    }
    else {
      tfFamille.setText("");
    }
  }

  /**
   * Rafraichir la sous-famille de l'article.
   */
  private void rafraichirSousFamille() {
    if (parametreArticle != null && parametreArticle.getSousFamille() != null) {
      tfSousFamille.setText(parametreArticle.getSousFamille());
    }
    else {
      tfSousFamille.setText("");
    }
  }

  /**
   * Rafraichir l'identifiant du fournisseur.
   */
  private void rafraichirIdFournisseur() {
    if (parametreArticle != null && parametreArticle.getIdFournisseur() != null) {
      tfIdFournisseur.setText(parametreArticle.getIdFournisseur().getTexte());
    }
    else {
      tfIdFournisseur.setText("");
    }
  }

  /**
   * Rafraichir la référence tarif.
   */
  private void rafraichirIdReferenceTarif() {
    if (parametreArticle != null && parametreArticle.getIdReferenceTarif() != null) {
      tfIdReferenceTarif.setText(parametreArticle.getIdReferenceTarif().getTexte());
    }
    else {
      tfIdReferenceTarif.setText("");
    }
  }

  /**
   * Rafraichir le regroupement achat de l'article.
   */
  private void rafraichirRegroupementAchat() {
    if (parametreArticle != null && parametreArticle.getCodeRegroupementAchat() != null) {
      tfRegroupementAchat.setText(parametreArticle.getCodeRegroupementAchat());
      return;
    }
    else {
      tfRegroupementAchat.setText("");
    }
  }

  /**
   * Rafraichir les rattachements aux conditions de vente.
   */
  private void rafraichirRattachementCNV() {
    if (parametreArticle == null) {
      tfRattachementCNV.setText("");
      tfRattachementCNV1.setText("");
      tfRattachementCNV2.setText("");
      tfRattachementCNV3.setText("");
      return;
    }

    String valeur = parametreArticle.getCodeRattachementCNV();
    if (valeur == null) {
      tfRattachementCNV.setText(valeur);
    }
    else {
      tfRattachementCNV.setText(valeur);
    }
    valeur = parametreArticle.getCodeRattachementCNV1();
    if (valeur == null) {
      tfRattachementCNV1.setText(valeur);
    }
    else {
      tfRattachementCNV1.setText(valeur);
    }
    valeur = parametreArticle.getCodeRattachementCNV2();
    if (valeur == null) {
      tfRattachementCNV2.setText(valeur);
    }
    else {
      tfRattachementCNV2.setText(valeur);
    }
    valeur = parametreArticle.getCodeRattachementCNV3();
    if (valeur == null) {
      tfRattachementCNV3.setText(valeur);
    }
    else {
      tfRattachementCNV3.setText(valeur);
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir pour l'encart document de vente
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir le titre de l'encart du document de vente.
   */
  private void rafraichirTitreDocumentVente() {
    if (parametreDocumentVente != null && parametreDocumentVente.getIdDocumentVente() != null) {
      pnlDocumentVente.setTitre(TITRE_DOCUMENT_VENTE + " " + parametreDocumentVente.getIdDocumentVente());
    }
    else {
      pnlDocumentVente.setTitre(TITRE_DOCUMENT_VENTE);
    }
  }

  /**
   * Rafraichir la date de création du document de vente.
   */
  private void rafraichirDateCreation() {
    if (parametreDocumentVente != null && parametreDocumentVente.getDateCreation() != null) {
      Date date = parametreDocumentVente.getDateCreation();
      tfDateCreation.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
    }
    else {
      tfDateCreation.setText("");
    }
  }

  /**
   * Rafraichir la devise du document de vente.
   */
  private void rafraichirDeviseDocumentVente() {
    if (parametreDocumentVente != null && parametreDocumentVente.getCodeDevise() != null) {
      tfDeviseDocumentVente.setText(parametreDocumentVente.getCodeDevise());
    }
    else {
      tfDeviseDocumentVente.setText("");
    }
  }

  /**
   * Rafraichir le code de la condition de vente.
   */
  private void rafraichirCodeConditionVente() {
    if (parametreDocumentVente != null && parametreDocumentVente.getCodeConditionVente() != null) {
      tfCNVDocumentVente.setText(parametreDocumentVente.getCodeConditionVente());
    }
    else {
      tfCNVDocumentVente.setText("");
    }
  }

  /**
   * Rafraichir le numéro client de la centrale 1.
   */
  private void rafraichirNumeroClientCentrale1() {
    if (parametreDocumentVente != null && parametreDocumentVente.getNumeroClientCentrale1() != null) {
      tfNumeroClientCentrale1DocumentVente.setText(parametreDocumentVente.getNumeroClientCentrale1().toString());
    }
    else {
      tfNumeroClientCentrale1DocumentVente.setText("");
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir pour l'encart résultat du calcul
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir le titre de l'encart résultat.
   */
  private void rafraichirTitreResultat() {
    if (formulePrixVente != null && formulePrixVente.getLibelle() != null) {
      pnlResultat.setTitre(TITRE_RESULTAT + " : " + formulePrixVente.getLibelle());
    }
    else {
      pnlResultat.setTitre(TITRE_RESULTAT);
    }
  }

  /**
   * Rafraichir la date d'application (paramètres tarif).
   */
  private void rafraichirDateApplication() {
    if (calculPrixVente != null && calculPrixVente.getDateApplication() != null) {
      tfDateApplication.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, getModele().getCalculPrixVente().getDateApplication()));
    }
    else {
      tfDateApplication.setText("");
    }
  }

  /**
   * Rafraichir l'origine de la devise utilisée pour le calcul.
   */
  private void rafraichirOrigineDevise() {
    if (parametreTarif != null && parametreTarif.getOrigineDevise() != null) {
      tfOrigineDevise.setText(PREFIXE_ORIGINE + parametreTarif.getOrigineDevise());
    }
    else {
      tfOrigineDevise.setText("");
    }
  }

  /**
   * Rafraichir la devise.
   */
  private void rafraichirDevise() {
    if (parametreTarif != null && parametreTarif.getCodeDevise() != null) {
      String codeDevise = Constantes.normerTexte(parametreTarif.getCodeDevise());
      if (codeDevise.isEmpty()) {
        codeDevise = "EUR";
      }
      tfDevise.setText(codeDevise);
    }
    else {
      tfDevise.setText("");
    }
  }

  /**
   * Rafraichir l'origine du prix de vente.
   */
  private void rafraichirOriginePrixVente() {
    if (calculPrixVente != null && calculPrixVente.getOriginePrixVente() != null) {
      tfOriginePrixVente.setText(PREFIXE_ORIGINE + calculPrixVente.getOriginePrixVente());
    }
    else {
      tfOriginePrixVente.setText("");
    }
  }

  /**
   * Rafraichir la colonne tarif.
   */
  private void rafraichirColonneTarif() {
    if (calculPrixVente != null && calculPrixVente.getNumeroColonneTarif() != null) {
      Integer numeroColonneTarif = calculPrixVente.getNumeroColonneTarif();
      if (numeroColonneTarif >= 1 && numeroColonneTarif <= 10) {
        cbColonneTarif.setSelectedIndex(numeroColonneTarif);
      }
      else {
        cbColonneTarif.setSelectedIndex(0);
      }
    }
    else {
      cbColonneTarif.setSelectedIndex(0);
    }
  }

  /**
   * Rafraichir le prix de base HT.
   */
  private void rafraichirPrixBaseHT() {
    tfPrixBaseHT.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseHT.setNegatifAutorise(true);
    tfPrixBaseHT.setModeNegatif(true);

    if (calculPrixVente != null && calculPrixVente.getPrixBaseHT() != null) {
      tfPrixBaseHT.setMontant(calculPrixVente.getPrixBaseHT());
    }
    else {
      tfPrixBaseHT.setMontant("");
    }
  }

  /**
   * Rafraichir le prix base TTC.
   */
  private void rafraichirPrixBaseTTC() {
    tfPrixBaseTTC.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseTTC.setNegatifAutorise(true);
    tfPrixBaseTTC.setModeNegatif(true);

    if (calculPrixVente != null && calculPrixVente.getPrixBaseTTC() != null) {
      tfPrixBaseTTC.setMontant(calculPrixVente.getPrixBaseTTC());
    }
    else {
      tfPrixBaseTTC.setMontant("");
    }

    lbPrixBaseTTC.setVisible(calculPrixVente != null && calculPrixVente.isModeTTC());
    tfPrixBaseTTC.setVisible(calculPrixVente != null && calculPrixVente.isModeTTC());
  }

  /**
   * Rafraichir le taux de remise unitaire.
   */
  private void rafraichirTauxRemiseUnitaire() {
    if (calculPrixVente != null && calculPrixVente.getTauxRemiseUnitaire() != null) {
      tfTauxRemiseUnitaire.setText(calculPrixVente.getTauxRemiseUnitaire().getTexteSansSymbole());
    }
    else {
      tfTauxRemiseUnitaire.setText("");
    }
  }

  /**
   * Rafraichir le prix net HT utilisé pour le calcul.
   */
  private void rafraichirPrixNetHT() {
    tfPrixNetHT.setLongueurPartieDecimale(nombreDecimale);
    tfPrixNetHT.setNegatifAutorise(true);
    tfPrixNetHT.setModeNegatif(true);

    if (calculPrixVente != null && calculPrixVente.getPrixNetHT() != null) {
      tfPrixNetHT.setMontant(calculPrixVente.getPrixNetHT());
    }
    else {
      tfPrixNetHT.setMontant("");
    }
  }

  /**
   * Rafraichir le prix net TTC.
   */
  private void rafraichirPrixNetTTC() {
    tfPrixNetTTC.setLongueurPartieDecimale(nombreDecimale);
    tfPrixNetTTC.setNegatifAutorise(true);
    tfPrixNetTTC.setModeNegatif(true);

    if (calculPrixVente != null && calculPrixVente.getPrixNetTTC() != null) {
      tfPrixNetTTC.setMontant(calculPrixVente.getPrixNetTTC());
    }
    else {
      tfPrixNetTTC.setMontant("");
    }

    lbPrixNetTTC.setVisible(calculPrixVente != null && calculPrixVente.isModeTTC());
    tfPrixNetTTC.setVisible(calculPrixVente != null && calculPrixVente.isModeTTC());
  }

  /**
   * Rafraichir la quantité d'article en UV.
   */
  private void rafraichirQuantiteUV() {
    if (calculPrixVente != null && calculPrixVente.getQuantiteUV() != null) {
      tfQuantiteUV.setMontant(calculPrixVente.getQuantiteUV());
    }
    else {
      tfQuantiteUV.setMontant("");
    }
  }

  /**
   * Rafraichir le montant HT.
   */
  private void rafraichirMontantHT() {
    tfMontantHT.setLongueurPartieDecimale(nombreDecimale);
    tfMontantHT.setNegatifAutorise(true);
    tfMontantHT.setModeNegatif(true);

    if (calculPrixVente != null && calculPrixVente.getMontantHT() != null) {
      tfMontantHT.setMontant(calculPrixVente.getMontantHT());
    }
    else {
      tfMontantHT.setMontant("");
    }
  }

  /**
   * Rafraichir le montant TTC.
   */
  private void rafraichirMontantTTC() {
    tfMontantTTC.setLongueurPartieDecimale(nombreDecimale);
    tfMontantTTC.setNegatifAutorise(true);
    tfMontantTTC.setModeNegatif(true);

    if (calculPrixVente != null && calculPrixVente.getMontantTTC() != null) {
      tfMontantTTC.setMontant(calculPrixVente.getMontantTTC());
    }
    else {
      tfMontantTTC.setMontant("");
    }

    lbMontantTTC.setVisible(calculPrixVente != null && calculPrixVente.isModeTTC());
    tfMontantTTC.setVisible(calculPrixVente != null && calculPrixVente.isModeTTC());
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir liste des cas de calcul
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void rafraichirListe() {
    // Récupérer la liste des formules prix d evente à afficher
    List<FormulePrixVente> listeFormulePrixVente = getModele().getListeFormulePrixVente();
    if (listeFormulePrixVente == null) {
      tblListeFormulePrixVente.mettreAJourDonnees(null);
      return;
    }

    // Mettre à jour le tableau
    String[][] donnees = null;
    donnees = new String[listeFormulePrixVente.size()][8];
    for (int ligne = 0; ligne < listeFormulePrixVente.size(); ligne++) {
      FormulePrixVente formulePrixVente = listeFormulePrixVente.get(ligne);

      // Libellé
      if (formulePrixVente.getLibelle() != null) {
        donnees[ligne][0] = formulePrixVente.getLibelle();
      }

      // Type
      String type = "";
      if (formulePrixVente.getTypeFormulePrixVente() != null) {
        type += formulePrixVente.getTypeFormulePrixVente().getLibelle();
      }
      if (formulePrixVente.getParametreConditionVente() != null && formulePrixVente.getParametreConditionVente().getCumulative()) {
        type += " cumulative";
      }
      donnees[ligne][1] = type;

      // Rattachement client
      if (formulePrixVente.getParametreConditionVente() != null) {
        String rattachementClient = "";
        if (formulePrixVente.getParametreConditionVente().getOrigine() != null) {
          rattachementClient = formulePrixVente.getParametreConditionVente().getOrigine().getLibelle();
        }
        else if (formulePrixVente.getParametreConditionVente().isConditionVenteClient()) {
          rattachementClient = "Client : " + formulePrixVente.getParametreConditionVente().getIdRattachementClient().getCode();
        }
        else if (formulePrixVente.getParametreConditionVente().isConditionVenteGroupeClient()) {
          rattachementClient = "Groupe CNV : " + formulePrixVente.getParametreConditionVente().getIdRattachementClient().getCode();
        }
        donnees[ligne][2] = rattachementClient;
      }

      // Rattachement article
      if (formulePrixVente.getParametreConditionVente() != null
          && formulePrixVente.getParametreConditionVente().getIdRattachementArticle() != null) {
        String rattachementArticle = "";
        if (formulePrixVente.getParametreConditionVente().getIdRattachementArticle().getTypeRattachement() != null) {
          rattachementArticle +=
              formulePrixVente.getParametreConditionVente().getIdRattachementArticle().getTypeRattachement().getLibelle() + " : ";
        }
        if (formulePrixVente.getParametreConditionVente().getIdRattachementArticle().getCodeRattachement() != null) {
          rattachementArticle += formulePrixVente.getParametreConditionVente().getIdRattachementArticle().getCodeRattachement();
        }
        donnees[ligne][3] = rattachementArticle;
      }

      // Prix base HT
      if (formulePrixVente.getPrixBaseCalculHT() != null) {
        donnees[ligne][4] = formulePrixVente.getPrixBaseCalculHT().toPlainString();
      }

      // Prix net HT
      if (formulePrixVente.getPrixNetHT() != null) {
        donnees[ligne][5] = formulePrixVente.getPrixNetHT().toPlainString();
      }

      // Montant HT
      if (formulePrixVente.getMontantHT() != null) {
        donnees[ligne][6] = formulePrixVente.getMontantHT().toPlainString();
      }

      // Commentaire
      if (formulePrixVente.getCommentaire() != null) {
        donnees[ligne][7] = formulePrixVente.getCommentaire();
      }
    }

    tblListeFormulePrixVente.mettreAJourDonnees(donnees);
  }
  
  private void cbColonneTarifItemStateChanged(ItemEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    scpFond = new JScrollPane();
    pnlFond = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    pnlEtablissement = new SNPanelTitre();
    lbMoisEnCoursDG = new SNLabelChamp();
    tfMoisEnCoursDG = new SNTexte();
    lbDeviseEtablissement = new SNLabelChamp();
    tfDeviseEtablissement = new SNTexte();
    lbGroupeCNVStandardEtablissement = new SNLabelChamp();
    tfGroupeCNVStandardEtablissement = new SNTexte();
    lbGroupeCNVPromoEtablissement = new SNLabelChamp();
    tfGroupeCNVPromoEtablissement = new SNTexte();
    lbCNVClientArticlePrioritaire = new SNLabelChamp();
    cbCNVClientArticlePrioritaire = new SNCheckBoxReduit();
    pnlDocumentVente = new SNPanelTitre();
    lbDateCreation = new SNLabelChamp();
    tfDateCreation = new SNTexte();
    lbDeviseDocumentVente = new SNLabelChamp();
    tfDeviseDocumentVente = new SNTexte();
    lbCNVDocumentVente = new SNLabelChamp();
    tfCNVDocumentVente = new SNTexte();
    lbNumeroClientCentrale1DocumentVente = new SNLabelChamp();
    tfNumeroClientCentrale1DocumentVente = new SNTexte();
    pnlResultat = new SNPanelTitre();
    lbDateApplication = new SNLabelChamp();
    tfDateApplication = new SNTexte();
    lbOrigineDevise = new SNLabelChamp();
    tfOrigineDevise = new JLabel();
    lbDevise = new SNLabelChamp();
    tfDevise = new SNTexte();
    lbOriginePrixVente = new SNLabelChamp();
    tfOriginePrixVente = new JLabel();
    lbColonneTarif = new SNLabelChamp();
    cbColonneTarif = new SNComboBoxReduit();
    lbPrixBaseHT = new SNLabelChamp();
    tfPrixBaseHT = new SNMontant();
    lbPrixBaseTTC = new SNLabelChamp();
    tfPrixBaseTTC = new SNMontant();
    lbTauxRemiseUnitaire = new SNLabelChamp();
    tfTauxRemiseUnitaire = new SNTexte();
    lbPrixNetHT = new SNLabelChamp();
    tfPrixNetHT = new SNMontant();
    lbPrixNetTTC = new SNLabelChamp();
    tfPrixNetTTC = new SNMontant();
    lbQuantiteUV = new SNLabelChamp();
    tfQuantiteUV = new SNMontant();
    lbMontantHT = new SNLabelChamp();
    tfMontantHT = new SNMontant();
    lbMontantTTC = new SNLabelChamp();
    tfMontantTTC = new SNMontant();
    pnlClientFacture = new SNPanelTitre();
    lbDeviseCLientFacture = new SNLabelChamp();
    tfDeviseClientFacture = new SNTexte();
    lbIdentifiantClientFactureClientFacture = new SNLabelChamp();
    tfIdentifiantClientFactureClientFacture = new SNTexte();
    lbNumeroCentraleAchatClientFacture = new SNLabelChamp();
    pnlNumeroCentraleAchatClientFacture = new SNPanel();
    tfNumeroCentraleAchat1ClientFacture = new SNTexte();
    tfNumeroCentraleAchat2ClientFacture = new SNTexte();
    tfNumeroCentraleAchat3ClientFacture = new SNTexte();
    lbGroupeCNVStandardClientFacture = new SNLabelChamp();
    tfGroupeCNVStandardClientFacture = new SNTexte();
    lbGroupeCNVPromoClientFacture = new SNLabelChamp();
    tfGroupeCNVPromoClientFacture = new SNTexte();
    lbExclusionCNVEtablissementClientFacture = new SNLabelChamp();
    tfExclusionCNVEtablissementClientFacture = new SNTexte();
    lbIgnorerCNVQuantitativeClientFacture = new SNLabelChamp();
    cbIgnorerCNVQuantitativeClientFacture = new SNCheckBoxReduit();
    pnlArticle = new SNPanelTitre();
    lbFamille = new SNLabelChamp();
    tfFamille = new SNTexte();
    lbSousFamille = new SNLabelChamp();
    tfSousFamille = new SNTexte();
    lbIdFournisseur = new SNLabelChamp();
    tfIdFournisseur = new SNTexte();
    lbIdRéférenceTarif = new SNLabelChamp();
    tfIdReferenceTarif = new SNTexte();
    lbRegroupementAchat = new SNLabelChamp();
    tfRegroupementAchat = new SNTexte();
    lbRattachementCNV = new SNLabelChamp();
    pnlPRattachementCNV = new SNPanel();
    tfRattachementCNV = new SNTexte();
    tfRattachementCNV1 = new SNTexte();
    tfRattachementCNV2 = new SNTexte();
    tfRattachementCNV3 = new SNTexte();
    pnlClientLivre = new SNPanelTitre();
    lbDeviseCLientLivre = new SNLabelChamp();
    tfDeviseClientLivre = new SNTexte();
    lbIdentifiantClientFactureClientLivre = new SNLabelChamp();
    tfIdentifiantClientFactureClientLivre = new SNTexte();
    lbNumeroCentraleAchatClientLivre = new SNLabelChamp();
    pnlNumeroCentraleAchatClientLivre = new SNPanel();
    tfNumeroCentraleAchat1ClientLivre = new SNTexte();
    tfNumeroCentraleAchat2ClientLivre = new SNTexte();
    tfNumeroCentraleAchat3ClientLivre = new SNTexte();
    lbGroupeCNVStandardClientLivre = new SNLabelChamp();
    tfGroupeCNVStandardClientLivre = new SNTexte();
    lbGroupeCNVPromoClientLivre = new SNLabelChamp();
    tfGroupeCNVPromoClientLivre = new SNTexte();
    lbExclusionCNVEtablissementClientLivre = new SNLabelChamp();
    tfExclusionCNVEtablissementClientLivre = new SNTexte();
    lbIgnorerCNVQuantitativeClientLivre = new SNLabelChamp();
    cbIgnorerCNVQuantitativeClientLivre = new SNCheckBoxReduit();
    sNLabelTitre1 = new SNLabelTitre();
    spListeConditionVente = new JScrollPane();
    tblListeFormulePrixVente = new NRiTable();
    
    // ======== this ========
    setBorder(null);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== scpFond ========
    {
      scpFond.setViewportBorder(BorderFactory.createEmptyBorder());
      scpFond.setBorder(BorderFactory.createEmptyBorder());
      scpFond.setName("scpFond");
      
      // ======== pnlFond ========
      {
        pnlFond.setBorder(null);
        pnlFond.setName("pnlFond");
        pnlFond.setLayout(new BorderLayout());
        
        // ======== pnlContenu ========
        {
          pnlContenu.setPreferredSize(new Dimension(726, 700));
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setModeReduit(true);
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMoisEnCoursDG ----
            lbMoisEnCoursDG.setText("Mois en cours (DG)");
            lbMoisEnCoursDG.setModeReduit(true);
            lbMoisEnCoursDG.setName("lbMoisEnCoursDG");
            pnlEtablissement.add(lbMoisEnCoursDG, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfMoisEnCoursDG ----
            tfMoisEnCoursDG.setEnabled(false);
            tfMoisEnCoursDG.setModeReduit(true);
            tfMoisEnCoursDG.setName("tfMoisEnCoursDG");
            pnlEtablissement.add(tfMoisEnCoursDG, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbDeviseEtablissement ----
            lbDeviseEtablissement.setText("Devise");
            lbDeviseEtablissement.setModeReduit(true);
            lbDeviseEtablissement.setName("lbDeviseEtablissement");
            pnlEtablissement.add(lbDeviseEtablissement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfDeviseEtablissement ----
            tfDeviseEtablissement.setEnabled(false);
            tfDeviseEtablissement.setModeReduit(true);
            tfDeviseEtablissement.setName("tfDeviseEtablissement");
            pnlEtablissement.add(tfDeviseEtablissement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbGroupeCNVStandardEtablissement ----
            lbGroupeCNVStandardEtablissement.setText("Groupe CNV standard");
            lbGroupeCNVStandardEtablissement.setModeReduit(true);
            lbGroupeCNVStandardEtablissement.setName("lbGroupeCNVStandardEtablissement");
            pnlEtablissement.add(lbGroupeCNVStandardEtablissement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfGroupeCNVStandardEtablissement ----
            tfGroupeCNVStandardEtablissement.setEnabled(false);
            tfGroupeCNVStandardEtablissement.setModeReduit(true);
            tfGroupeCNVStandardEtablissement.setName("tfGroupeCNVStandardEtablissement");
            pnlEtablissement.add(tfGroupeCNVStandardEtablissement, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbGroupeCNVPromoEtablissement ----
            lbGroupeCNVPromoEtablissement.setText("Groupe CNV promotionnel");
            lbGroupeCNVPromoEtablissement.setModeReduit(true);
            lbGroupeCNVPromoEtablissement.setName("lbGroupeCNVPromoEtablissement");
            pnlEtablissement.add(lbGroupeCNVPromoEtablissement, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfGroupeCNVPromoEtablissement ----
            tfGroupeCNVPromoEtablissement.setEnabled(false);
            tfGroupeCNVPromoEtablissement.setModeReduit(true);
            tfGroupeCNVPromoEtablissement.setName("tfGroupeCNVPromoEtablissement");
            pnlEtablissement.add(tfGroupeCNVPromoEtablissement, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbCNVClientArticlePrioritaire ----
            lbCNVClientArticlePrioritaire.setText("CNV client/article prioritaire");
            lbCNVClientArticlePrioritaire.setModeReduit(true);
            lbCNVClientArticlePrioritaire.setToolTipText("Si est d\u00e9finit un prix net");
            lbCNVClientArticlePrioritaire.setName("lbCNVClientArticlePrioritaire");
            pnlEtablissement.add(lbCNVClientArticlePrioritaire, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- cbCNVClientArticlePrioritaire ----
            cbCNVClientArticlePrioritaire.setEnabled(false);
            cbCNVClientArticlePrioritaire.setName("cbCNVClientArticlePrioritaire");
            pnlEtablissement.add(cbCNVClientArticlePrioritaire, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlDocumentVente ========
          {
            pnlDocumentVente.setTitre("Document de vente");
            pnlDocumentVente.setModeReduit(true);
            pnlDocumentVente.setName("pnlDocumentVente");
            pnlDocumentVente.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDocumentVente.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlDocumentVente.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlDocumentVente.getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDocumentVente.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateCreation ----
            lbDateCreation.setModeReduit(true);
            lbDateCreation.setText("Date cr\u00e9ation");
            lbDateCreation.setMinimumSize(new Dimension(30, 22));
            lbDateCreation.setPreferredSize(new Dimension(30, 22));
            lbDateCreation.setName("lbDateCreation");
            pnlDocumentVente.add(lbDateCreation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfDateCreation ----
            tfDateCreation.setModeReduit(true);
            tfDateCreation.setEnabled(false);
            tfDateCreation.setName("tfDateCreation");
            pnlDocumentVente.add(tfDateCreation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbDeviseDocumentVente ----
            lbDeviseDocumentVente.setModeReduit(true);
            lbDeviseDocumentVente.setText("Devise");
            lbDeviseDocumentVente.setMinimumSize(new Dimension(100, 22));
            lbDeviseDocumentVente.setPreferredSize(new Dimension(100, 22));
            lbDeviseDocumentVente.setName("lbDeviseDocumentVente");
            pnlDocumentVente.add(lbDeviseDocumentVente, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfDeviseDocumentVente ----
            tfDeviseDocumentVente.setModeReduit(true);
            tfDeviseDocumentVente.setEnabled(false);
            tfDeviseDocumentVente.setName("tfDeviseDocumentVente");
            pnlDocumentVente.add(tfDeviseDocumentVente, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbCNVDocumentVente ----
            lbCNVDocumentVente.setModeReduit(true);
            lbCNVDocumentVente.setText("Code CNV");
            lbCNVDocumentVente.setMinimumSize(new Dimension(130, 22));
            lbCNVDocumentVente.setPreferredSize(new Dimension(130, 22));
            lbCNVDocumentVente.setMaximumSize(new Dimension(130, 22));
            lbCNVDocumentVente.setName("lbCNVDocumentVente");
            pnlDocumentVente.add(lbCNVDocumentVente, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfCNVDocumentVente ----
            tfCNVDocumentVente.setModeReduit(true);
            tfCNVDocumentVente.setEnabled(false);
            tfCNVDocumentVente.setName("tfCNVDocumentVente");
            pnlDocumentVente.add(tfCNVDocumentVente, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbNumeroClientCentrale1DocumentVente ----
            lbNumeroClientCentrale1DocumentVente.setModeReduit(true);
            lbNumeroClientCentrale1DocumentVente.setText("Num\u00e9ro client centrale 1");
            lbNumeroClientCentrale1DocumentVente.setMinimumSize(new Dimension(30, 22));
            lbNumeroClientCentrale1DocumentVente.setPreferredSize(new Dimension(30, 22));
            lbNumeroClientCentrale1DocumentVente.setName("lbNumeroClientCentrale1DocumentVente");
            pnlDocumentVente.add(lbNumeroClientCentrale1DocumentVente, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfNumeroClientCentrale1DocumentVente ----
            tfNumeroClientCentrale1DocumentVente.setEnabled(false);
            tfNumeroClientCentrale1DocumentVente.setHorizontalAlignment(SwingConstants.LEFT);
            tfNumeroClientCentrale1DocumentVente.setModeReduit(true);
            tfNumeroClientCentrale1DocumentVente.setMaximumSize(new Dimension(40, 22));
            tfNumeroClientCentrale1DocumentVente.setPreferredSize(new Dimension(80, 22));
            tfNumeroClientCentrale1DocumentVente.setMinimumSize(new Dimension(40, 22));
            tfNumeroClientCentrale1DocumentVente.setName("tfNumeroClientCentrale1DocumentVente");
            pnlDocumentVente.add(tfNumeroClientCentrale1DocumentVente, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlDocumentVente, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlResultat ========
          {
            pnlResultat.setModeReduit(true);
            pnlResultat.setTitre("R\u00e9sultat");
            pnlResultat.setName("pnlResultat");
            pnlResultat.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlResultat.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlResultat.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlResultat.getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlResultat.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateApplication ----
            lbDateApplication.setText("Date d'application");
            lbDateApplication.setModeReduit(true);
            lbDateApplication.setName("lbDateApplication");
            pnlResultat.add(lbDateApplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfDateApplication ----
            tfDateApplication.setEnabled(false);
            tfDateApplication.setModeReduit(true);
            tfDateApplication.setName("tfDateApplication");
            pnlResultat.add(tfDateApplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbOrigineDevise ----
            lbOrigineDevise.setText("Origine devise");
            lbOrigineDevise.setModeReduit(true);
            lbOrigineDevise.setName("lbOrigineDevise");
            pnlResultat.add(lbOrigineDevise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfOrigineDevise ----
            tfOrigineDevise.setText("-");
            tfOrigineDevise.setFont(new Font("sansserif", Font.PLAIN, 11));
            tfOrigineDevise.setName("tfOrigineDevise");
            pnlResultat.add(tfOrigineDevise, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbDevise ----
            lbDevise.setText("Devise");
            lbDevise.setModeReduit(true);
            lbDevise.setName("lbDevise");
            pnlResultat.add(lbDevise, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfDevise ----
            tfDevise.setEnabled(false);
            tfDevise.setModeReduit(true);
            tfDevise.setName("tfDevise");
            pnlResultat.add(tfDevise, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbOriginePrixVente ----
            lbOriginePrixVente.setText("Origine prix de vente");
            lbOriginePrixVente.setModeReduit(true);
            lbOriginePrixVente.setName("lbOriginePrixVente");
            pnlResultat.add(lbOriginePrixVente, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfOriginePrixVente ----
            tfOriginePrixVente.setText("-");
            tfOriginePrixVente.setFont(new Font("sansserif", Font.PLAIN, 11));
            tfOriginePrixVente.setName("tfOriginePrixVente");
            pnlResultat.add(tfOriginePrixVente, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbColonneTarif ----
            lbColonneTarif.setText("Colonne tarif");
            lbColonneTarif.setModeReduit(true);
            lbColonneTarif.setName("lbColonneTarif");
            pnlResultat.add(lbColonneTarif, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- cbColonneTarif ----
            cbColonneTarif.setModel(new DefaultComboBoxModel<>(new String[] { "Aucune", "Colonne 1", "Colonne 2", "Colonne 3",
                "Colonne 4", "Colonne 5", "Colonne 6", "Colonne 7", "Colonne 8", "Colonne 9", "Colonne 10" }));
            cbColonneTarif.setSelectedIndex(-1);
            cbColonneTarif.setEnabled(false);
            cbColonneTarif.setName("cbColonneTarif");
            cbColonneTarif.addItemListener(e -> cbColonneTarifItemStateChanged(e));
            pnlResultat.add(cbColonneTarif, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbPrixBaseHT ----
            lbPrixBaseHT.setText("Prix base HT");
            lbPrixBaseHT.setModeReduit(true);
            lbPrixBaseHT.setName("lbPrixBaseHT");
            pnlResultat.add(lbPrixBaseHT, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfPrixBaseHT ----
            tfPrixBaseHT.setModeReduit(true);
            tfPrixBaseHT.setEnabled(false);
            tfPrixBaseHT.setName("tfPrixBaseHT");
            pnlResultat.add(tfPrixBaseHT, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbPrixBaseTTC ----
            lbPrixBaseTTC.setText("Prix base TTC");
            lbPrixBaseTTC.setModeReduit(true);
            lbPrixBaseTTC.setName("lbPrixBaseTTC");
            pnlResultat.add(lbPrixBaseTTC, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfPrixBaseTTC ----
            tfPrixBaseTTC.setEnabled(false);
            tfPrixBaseTTC.setModeReduit(true);
            tfPrixBaseTTC.setName("tfPrixBaseTTC");
            pnlResultat.add(tfPrixBaseTTC, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbTauxRemiseUnitaire ----
            lbTauxRemiseUnitaire.setText("Taux remise unitaire (%)");
            lbTauxRemiseUnitaire.setModeReduit(true);
            lbTauxRemiseUnitaire.setName("lbTauxRemiseUnitaire");
            pnlResultat.add(lbTauxRemiseUnitaire, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfTauxRemiseUnitaire ----
            tfTauxRemiseUnitaire.setEnabled(false);
            tfTauxRemiseUnitaire.setHorizontalAlignment(SwingConstants.TRAILING);
            tfTauxRemiseUnitaire.setModeReduit(true);
            tfTauxRemiseUnitaire.setName("tfTauxRemiseUnitaire");
            pnlResultat.add(tfTauxRemiseUnitaire, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbPrixNetHT ----
            lbPrixNetHT.setText("Prix net HT");
            lbPrixNetHT.setFont(new Font("sansserif", Font.PLAIN, 11));
            lbPrixNetHT.setVerticalAlignment(SwingConstants.CENTER);
            lbPrixNetHT.setModeReduit(true);
            lbPrixNetHT.setName("lbPrixNetHT");
            pnlResultat.add(lbPrixNetHT, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfPrixNetHT ----
            tfPrixNetHT.setEnabled(false);
            tfPrixNetHT.setModeReduit(true);
            tfPrixNetHT.setName("tfPrixNetHT");
            pnlResultat.add(tfPrixNetHT, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbPrixNetTTC ----
            lbPrixNetTTC.setText("Prix net TTC");
            lbPrixNetTTC.setModeReduit(true);
            lbPrixNetTTC.setName("lbPrixNetTTC");
            pnlResultat.add(lbPrixNetTTC, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfPrixNetTTC ----
            tfPrixNetTTC.setEnabled(false);
            tfPrixNetTTC.setModeReduit(true);
            tfPrixNetTTC.setName("tfPrixNetTTC");
            pnlResultat.add(tfPrixNetTTC, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbQuantiteUV ----
            lbQuantiteUV.setText("Quantit\u00e9 en UV");
            lbQuantiteUV.setModeReduit(true);
            lbQuantiteUV.setName("lbQuantiteUV");
            pnlResultat.add(lbQuantiteUV, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfQuantiteUV ----
            tfQuantiteUV.setEnabled(false);
            tfQuantiteUV.setModeReduit(true);
            tfQuantiteUV.setName("tfQuantiteUV");
            pnlResultat.add(tfQuantiteUV, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbMontantHT ----
            lbMontantHT.setText("Montant HT");
            lbMontantHT.setModeReduit(true);
            lbMontantHT.setName("lbMontantHT");
            pnlResultat.add(lbMontantHT, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfMontantHT ----
            tfMontantHT.setEnabled(false);
            tfMontantHT.setModeReduit(true);
            tfMontantHT.setName("tfMontantHT");
            pnlResultat.add(tfMontantHT, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbMontantTTC ----
            lbMontantTTC.setText("Montant TTC");
            lbMontantTTC.setFont(new Font("sansserif", Font.PLAIN, 11));
            lbMontantTTC.setModeReduit(true);
            lbMontantTTC.setVerticalAlignment(SwingConstants.CENTER);
            lbMontantTTC.setName("lbMontantTTC");
            pnlResultat.add(lbMontantTTC, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- tfMontantTTC ----
            tfMontantTTC.setEnabled(false);
            tfMontantTTC.setModeReduit(true);
            tfMontantTTC.setName("tfMontantTTC");
            pnlResultat.add(tfMontantTTC, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlResultat, new GridBagConstraints(2, 0, 1, 2, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlClientFacture ========
          {
            pnlClientFacture.setTitre("Client factur\u00e9");
            pnlClientFacture.setModeReduit(true);
            pnlClientFacture.setPreferredSize(new Dimension(71, 186));
            pnlClientFacture.setName("pnlClientFacture");
            pnlClientFacture.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlClientFacture.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlClientFacture.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlClientFacture.getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlClientFacture.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDeviseCLientFacture ----
            lbDeviseCLientFacture.setText("Devise");
            lbDeviseCLientFacture.setModeReduit(true);
            lbDeviseCLientFacture.setName("lbDeviseCLientFacture");
            pnlClientFacture.add(lbDeviseCLientFacture, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfDeviseClientFacture ----
            tfDeviseClientFacture.setEnabled(false);
            tfDeviseClientFacture.setModeReduit(true);
            tfDeviseClientFacture.setName("tfDeviseClientFacture");
            pnlClientFacture.add(tfDeviseClientFacture, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbIdentifiantClientFactureClientFacture ----
            lbIdentifiantClientFactureClientFacture.setText("Identifiant client factur\u00e9");
            lbIdentifiantClientFactureClientFacture.setModeReduit(true);
            lbIdentifiantClientFactureClientFacture.setName("lbIdentifiantClientFactureClientFacture");
            pnlClientFacture.add(lbIdentifiantClientFactureClientFacture, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfIdentifiantClientFactureClientFacture ----
            tfIdentifiantClientFactureClientFacture.setEnabled(false);
            tfIdentifiantClientFactureClientFacture.setModeReduit(true);
            tfIdentifiantClientFactureClientFacture.setName("tfIdentifiantClientFactureClientFacture");
            pnlClientFacture.add(tfIdentifiantClientFactureClientFacture, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbNumeroCentraleAchatClientFacture ----
            lbNumeroCentraleAchatClientFacture.setText("Centrales d'achats");
            lbNumeroCentraleAchatClientFacture.setModeReduit(true);
            lbNumeroCentraleAchatClientFacture.setName("lbNumeroCentraleAchatClientFacture");
            pnlClientFacture.add(lbNumeroCentraleAchatClientFacture, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ======== pnlNumeroCentraleAchatClientFacture ========
            {
              pnlNumeroCentraleAchatClientFacture.setName("pnlNumeroCentraleAchatClientFacture");
              pnlNumeroCentraleAchatClientFacture.setLayout(new GridLayout(1, 0, 3, 0));
              
              // ---- tfNumeroCentraleAchat1ClientFacture ----
              tfNumeroCentraleAchat1ClientFacture.setEnabled(false);
              tfNumeroCentraleAchat1ClientFacture.setHorizontalAlignment(SwingConstants.RIGHT);
              tfNumeroCentraleAchat1ClientFacture.setModeReduit(true);
              tfNumeroCentraleAchat1ClientFacture.setMinimumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat1ClientFacture.setPreferredSize(new Dimension(40, 22));
              tfNumeroCentraleAchat1ClientFacture.setMaximumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat1ClientFacture.setName("tfNumeroCentraleAchat1ClientFacture");
              pnlNumeroCentraleAchatClientFacture.add(tfNumeroCentraleAchat1ClientFacture);
              
              // ---- tfNumeroCentraleAchat2ClientFacture ----
              tfNumeroCentraleAchat2ClientFacture.setEnabled(false);
              tfNumeroCentraleAchat2ClientFacture.setHorizontalAlignment(SwingConstants.RIGHT);
              tfNumeroCentraleAchat2ClientFacture.setModeReduit(true);
              tfNumeroCentraleAchat2ClientFacture.setMinimumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat2ClientFacture.setPreferredSize(new Dimension(40, 22));
              tfNumeroCentraleAchat2ClientFacture.setMaximumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat2ClientFacture.setName("tfNumeroCentraleAchat2ClientFacture");
              pnlNumeroCentraleAchatClientFacture.add(tfNumeroCentraleAchat2ClientFacture);
              
              // ---- tfNumeroCentraleAchat3ClientFacture ----
              tfNumeroCentraleAchat3ClientFacture.setEnabled(false);
              tfNumeroCentraleAchat3ClientFacture.setHorizontalAlignment(SwingConstants.RIGHT);
              tfNumeroCentraleAchat3ClientFacture.setModeReduit(true);
              tfNumeroCentraleAchat3ClientFacture.setMaximumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat3ClientFacture.setMinimumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat3ClientFacture.setPreferredSize(new Dimension(40, 22));
              tfNumeroCentraleAchat3ClientFacture.setName("tfNumeroCentraleAchat3ClientFacture");
              pnlNumeroCentraleAchatClientFacture.add(tfNumeroCentraleAchat3ClientFacture);
            }
            pnlClientFacture.add(pnlNumeroCentraleAchatClientFacture, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbGroupeCNVStandardClientFacture ----
            lbGroupeCNVStandardClientFacture.setText("Groupe CNV standard");
            lbGroupeCNVStandardClientFacture.setModeReduit(true);
            lbGroupeCNVStandardClientFacture.setName("lbGroupeCNVStandardClientFacture");
            pnlClientFacture.add(lbGroupeCNVStandardClientFacture, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfGroupeCNVStandardClientFacture ----
            tfGroupeCNVStandardClientFacture.setEnabled(false);
            tfGroupeCNVStandardClientFacture.setModeReduit(true);
            tfGroupeCNVStandardClientFacture.setName("tfGroupeCNVStandardClientFacture");
            pnlClientFacture.add(tfGroupeCNVStandardClientFacture, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbGroupeCNVPromoClientFacture ----
            lbGroupeCNVPromoClientFacture.setText("Groupe CNV promotionnel");
            lbGroupeCNVPromoClientFacture.setModeReduit(true);
            lbGroupeCNVPromoClientFacture.setName("lbGroupeCNVPromoClientFacture");
            pnlClientFacture.add(lbGroupeCNVPromoClientFacture, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfGroupeCNVPromoClientFacture ----
            tfGroupeCNVPromoClientFacture.setEnabled(false);
            tfGroupeCNVPromoClientFacture.setModeReduit(true);
            tfGroupeCNVPromoClientFacture.setName("tfGroupeCNVPromoClientFacture");
            pnlClientFacture.add(tfGroupeCNVPromoClientFacture, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbExclusionCNVEtablissementClientFacture ----
            lbExclusionCNVEtablissementClientFacture.setText("Exclusion CNV \u00e9tablissement");
            lbExclusionCNVEtablissementClientFacture.setModeReduit(true);
            lbExclusionCNVEtablissementClientFacture.setName("lbExclusionCNVEtablissementClientFacture");
            pnlClientFacture.add(lbExclusionCNVEtablissementClientFacture, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfExclusionCNVEtablissementClientFacture ----
            tfExclusionCNVEtablissementClientFacture.setEnabled(false);
            tfExclusionCNVEtablissementClientFacture.setModeReduit(true);
            tfExclusionCNVEtablissementClientFacture.setName("tfExclusionCNVEtablissementClientFacture");
            pnlClientFacture.add(tfExclusionCNVEtablissementClientFacture, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbIgnorerCNVQuantitativeClientFacture ----
            lbIgnorerCNVQuantitativeClientFacture.setText("Ignorer CNV quantitatives");
            lbIgnorerCNVQuantitativeClientFacture.setModeReduit(true);
            lbIgnorerCNVQuantitativeClientFacture.setName("lbIgnorerCNVQuantitativeClientFacture");
            pnlClientFacture.add(lbIgnorerCNVQuantitativeClientFacture, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- cbIgnorerCNVQuantitativeClientFacture ----
            cbIgnorerCNVQuantitativeClientFacture.setEnabled(false);
            cbIgnorerCNVQuantitativeClientFacture.setName("cbIgnorerCNVQuantitativeClientFacture");
            pnlClientFacture.add(cbIgnorerCNVQuantitativeClientFacture, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlClientFacture, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlArticle ========
          {
            pnlArticle.setTitre("Article");
            pnlArticle.setModeReduit(true);
            pnlArticle.setName("pnlArticle");
            pnlArticle.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlArticle.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlArticle.getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlArticle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbFamille ----
            lbFamille.setText("Famille");
            lbFamille.setModeReduit(true);
            lbFamille.setName("lbFamille");
            pnlArticle.add(lbFamille, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfFamille ----
            tfFamille.setEnabled(false);
            tfFamille.setModeReduit(true);
            tfFamille.setName("tfFamille");
            pnlArticle.add(tfFamille, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbSousFamille ----
            lbSousFamille.setText("Sous-famille");
            lbSousFamille.setModeReduit(true);
            lbSousFamille.setName("lbSousFamille");
            pnlArticle.add(lbSousFamille, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfSousFamille ----
            tfSousFamille.setEnabled(false);
            tfSousFamille.setModeReduit(true);
            tfSousFamille.setName("tfSousFamille");
            pnlArticle.add(tfSousFamille, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbIdFournisseur ----
            lbIdFournisseur.setText("Identifiant fournisseur");
            lbIdFournisseur.setModeReduit(true);
            lbIdFournisseur.setName("lbIdFournisseur");
            pnlArticle.add(lbIdFournisseur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfIdFournisseur ----
            tfIdFournisseur.setEnabled(false);
            tfIdFournisseur.setModeReduit(true);
            tfIdFournisseur.setName("tfIdFournisseur");
            pnlArticle.add(tfIdFournisseur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbIdRéférenceTarif ----
            lbIdRéférenceTarif.setText("Identifiant r\u00e9f\u00e9rence tarif");
            lbIdRéférenceTarif.setModeReduit(true);
            lbIdRéférenceTarif.setName("lbIdR\u00e9f\u00e9renceTarif");
            pnlArticle.add(lbIdRéférenceTarif, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfIdReferenceTarif ----
            tfIdReferenceTarif.setEnabled(false);
            tfIdReferenceTarif.setModeReduit(true);
            tfIdReferenceTarif.setName("tfIdReferenceTarif");
            pnlArticle.add(tfIdReferenceTarif, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbRegroupementAchat ----
            lbRegroupementAchat.setText("Regroupement achat");
            lbRegroupementAchat.setModeReduit(true);
            lbRegroupementAchat.setName("lbRegroupementAchat");
            pnlArticle.add(lbRegroupementAchat, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfRegroupementAchat ----
            tfRegroupementAchat.setEnabled(false);
            tfRegroupementAchat.setModeReduit(true);
            tfRegroupementAchat.setName("tfRegroupementAchat");
            pnlArticle.add(tfRegroupementAchat, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbRattachementCNV ----
            lbRattachementCNV.setText("Rattachements CNV");
            lbRattachementCNV.setModeReduit(true);
            lbRattachementCNV.setName("lbRattachementCNV");
            pnlArticle.add(lbRattachementCNV, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 3), 0, 0));
            
            // ======== pnlPRattachementCNV ========
            {
              pnlPRattachementCNV.setName("pnlPRattachementCNV");
              pnlPRattachementCNV.setLayout(new GridLayout(2, 2, 3, 0));
              
              // ---- tfRattachementCNV ----
              tfRattachementCNV.setEnabled(false);
              tfRattachementCNV.setHorizontalAlignment(SwingConstants.LEFT);
              tfRattachementCNV.setModeReduit(true);
              tfRattachementCNV.setMaximumSize(new Dimension(40, 22));
              tfRattachementCNV.setMinimumSize(new Dimension(40, 22));
              tfRattachementCNV.setPreferredSize(new Dimension(40, 22));
              tfRattachementCNV.setName("tfRattachementCNV");
              pnlPRattachementCNV.add(tfRattachementCNV);
              
              // ---- tfRattachementCNV1 ----
              tfRattachementCNV1.setEnabled(false);
              tfRattachementCNV1.setHorizontalAlignment(SwingConstants.LEFT);
              tfRattachementCNV1.setModeReduit(true);
              tfRattachementCNV1.setMaximumSize(new Dimension(40, 22));
              tfRattachementCNV1.setMinimumSize(new Dimension(40, 22));
              tfRattachementCNV1.setPreferredSize(new Dimension(40, 22));
              tfRattachementCNV1.setName("tfRattachementCNV1");
              pnlPRattachementCNV.add(tfRattachementCNV1);
              
              // ---- tfRattachementCNV2 ----
              tfRattachementCNV2.setEnabled(false);
              tfRattachementCNV2.setHorizontalAlignment(SwingConstants.LEFT);
              tfRattachementCNV2.setModeReduit(true);
              tfRattachementCNV2.setMaximumSize(new Dimension(40, 22));
              tfRattachementCNV2.setMinimumSize(new Dimension(40, 22));
              tfRattachementCNV2.setPreferredSize(new Dimension(40, 22));
              tfRattachementCNV2.setName("tfRattachementCNV2");
              pnlPRattachementCNV.add(tfRattachementCNV2);
              
              // ---- tfRattachementCNV3 ----
              tfRattachementCNV3.setEnabled(false);
              tfRattachementCNV3.setHorizontalAlignment(SwingConstants.LEFT);
              tfRattachementCNV3.setModeReduit(true);
              tfRattachementCNV3.setMaximumSize(new Dimension(40, 22));
              tfRattachementCNV3.setMinimumSize(new Dimension(40, 22));
              tfRattachementCNV3.setPreferredSize(new Dimension(40, 22));
              tfRattachementCNV3.setName("tfRattachementCNV3");
              pnlPRattachementCNV.add(tfRattachementCNV3);
            }
            pnlArticle.add(pnlPRattachementCNV, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlArticle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlClientLivre ========
          {
            pnlClientLivre.setTitre("Client livr\u00e9");
            pnlClientLivre.setModeReduit(true);
            pnlClientLivre.setPreferredSize(new Dimension(71, 186));
            pnlClientLivre.setName("pnlClientLivre");
            pnlClientLivre.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlClientLivre.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlClientLivre.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlClientLivre.getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlClientLivre.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDeviseCLientLivre ----
            lbDeviseCLientLivre.setText("Devise");
            lbDeviseCLientLivre.setModeReduit(true);
            lbDeviseCLientLivre.setName("lbDeviseCLientLivre");
            pnlClientLivre.add(lbDeviseCLientLivre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfDeviseClientLivre ----
            tfDeviseClientLivre.setEnabled(false);
            tfDeviseClientLivre.setModeReduit(true);
            tfDeviseClientLivre.setName("tfDeviseClientLivre");
            pnlClientLivre.add(tfDeviseClientLivre, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbIdentifiantClientFactureClientLivre ----
            lbIdentifiantClientFactureClientLivre.setText("Identifiant client factur\u00e9");
            lbIdentifiantClientFactureClientLivre.setModeReduit(true);
            lbIdentifiantClientFactureClientLivre.setName("lbIdentifiantClientFactureClientLivre");
            pnlClientLivre.add(lbIdentifiantClientFactureClientLivre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfIdentifiantClientFactureClientLivre ----
            tfIdentifiantClientFactureClientLivre.setEnabled(false);
            tfIdentifiantClientFactureClientLivre.setModeReduit(true);
            tfIdentifiantClientFactureClientLivre.setName("tfIdentifiantClientFactureClientLivre");
            pnlClientLivre.add(tfIdentifiantClientFactureClientLivre, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbNumeroCentraleAchatClientLivre ----
            lbNumeroCentraleAchatClientLivre.setText("Centrales d'achats");
            lbNumeroCentraleAchatClientLivre.setModeReduit(true);
            lbNumeroCentraleAchatClientLivre.setName("lbNumeroCentraleAchatClientLivre");
            pnlClientLivre.add(lbNumeroCentraleAchatClientLivre, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ======== pnlNumeroCentraleAchatClientLivre ========
            {
              pnlNumeroCentraleAchatClientLivre.setName("pnlNumeroCentraleAchatClientLivre");
              pnlNumeroCentraleAchatClientLivre.setLayout(new GridLayout(1, 0, 3, 0));
              
              // ---- tfNumeroCentraleAchat1ClientLivre ----
              tfNumeroCentraleAchat1ClientLivre.setEnabled(false);
              tfNumeroCentraleAchat1ClientLivre.setHorizontalAlignment(SwingConstants.RIGHT);
              tfNumeroCentraleAchat1ClientLivre.setModeReduit(true);
              tfNumeroCentraleAchat1ClientLivre.setMinimumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat1ClientLivre.setPreferredSize(new Dimension(40, 22));
              tfNumeroCentraleAchat1ClientLivre.setMaximumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat1ClientLivre.setName("tfNumeroCentraleAchat1ClientLivre");
              pnlNumeroCentraleAchatClientLivre.add(tfNumeroCentraleAchat1ClientLivre);
              
              // ---- tfNumeroCentraleAchat2ClientLivre ----
              tfNumeroCentraleAchat2ClientLivre.setEnabled(false);
              tfNumeroCentraleAchat2ClientLivre.setHorizontalAlignment(SwingConstants.RIGHT);
              tfNumeroCentraleAchat2ClientLivre.setModeReduit(true);
              tfNumeroCentraleAchat2ClientLivre.setMinimumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat2ClientLivre.setPreferredSize(new Dimension(40, 22));
              tfNumeroCentraleAchat2ClientLivre.setMaximumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat2ClientLivre.setName("tfNumeroCentraleAchat2ClientLivre");
              pnlNumeroCentraleAchatClientLivre.add(tfNumeroCentraleAchat2ClientLivre);
              
              // ---- tfNumeroCentraleAchat3ClientLivre ----
              tfNumeroCentraleAchat3ClientLivre.setEnabled(false);
              tfNumeroCentraleAchat3ClientLivre.setHorizontalAlignment(SwingConstants.RIGHT);
              tfNumeroCentraleAchat3ClientLivre.setModeReduit(true);
              tfNumeroCentraleAchat3ClientLivre.setMaximumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat3ClientLivre.setMinimumSize(new Dimension(40, 22));
              tfNumeroCentraleAchat3ClientLivre.setPreferredSize(new Dimension(40, 22));
              tfNumeroCentraleAchat3ClientLivre.setName("tfNumeroCentraleAchat3ClientLivre");
              pnlNumeroCentraleAchatClientLivre.add(tfNumeroCentraleAchat3ClientLivre);
            }
            pnlClientLivre.add(pnlNumeroCentraleAchatClientLivre, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbGroupeCNVStandardClientLivre ----
            lbGroupeCNVStandardClientLivre.setText("Groupe CNV standard");
            lbGroupeCNVStandardClientLivre.setModeReduit(true);
            lbGroupeCNVStandardClientLivre.setName("lbGroupeCNVStandardClientLivre");
            pnlClientLivre.add(lbGroupeCNVStandardClientLivre, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfGroupeCNVStandardClientLivre ----
            tfGroupeCNVStandardClientLivre.setEnabled(false);
            tfGroupeCNVStandardClientLivre.setModeReduit(true);
            tfGroupeCNVStandardClientLivre.setName("tfGroupeCNVStandardClientLivre");
            pnlClientLivre.add(tfGroupeCNVStandardClientLivre, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbGroupeCNVPromoClientLivre ----
            lbGroupeCNVPromoClientLivre.setText("Groupe CNV promotionnel");
            lbGroupeCNVPromoClientLivre.setModeReduit(true);
            lbGroupeCNVPromoClientLivre.setName("lbGroupeCNVPromoClientLivre");
            pnlClientLivre.add(lbGroupeCNVPromoClientLivre, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfGroupeCNVPromoClientLivre ----
            tfGroupeCNVPromoClientLivre.setEnabled(false);
            tfGroupeCNVPromoClientLivre.setModeReduit(true);
            tfGroupeCNVPromoClientLivre.setName("tfGroupeCNVPromoClientLivre");
            pnlClientLivre.add(tfGroupeCNVPromoClientLivre, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbExclusionCNVEtablissementClientLivre ----
            lbExclusionCNVEtablissementClientLivre.setText("Exclusion CNV \u00e9tablissement");
            lbExclusionCNVEtablissementClientLivre.setModeReduit(true);
            lbExclusionCNVEtablissementClientLivre.setName("lbExclusionCNVEtablissementClientLivre");
            pnlClientLivre.add(lbExclusionCNVEtablissementClientLivre, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- tfExclusionCNVEtablissementClientLivre ----
            tfExclusionCNVEtablissementClientLivre.setEnabled(false);
            tfExclusionCNVEtablissementClientLivre.setModeReduit(true);
            tfExclusionCNVEtablissementClientLivre.setName("tfExclusionCNVEtablissementClientLivre");
            pnlClientLivre.add(tfExclusionCNVEtablissementClientLivre, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbIgnorerCNVQuantitativeClientLivre ----
            lbIgnorerCNVQuantitativeClientLivre.setText("Ignorer CNV quantitatives");
            lbIgnorerCNVQuantitativeClientLivre.setModeReduit(true);
            lbIgnorerCNVQuantitativeClientLivre.setName("lbIgnorerCNVQuantitativeClientLivre");
            pnlClientLivre.add(lbIgnorerCNVQuantitativeClientLivre, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
            
            // ---- cbIgnorerCNVQuantitativeClientLivre ----
            cbIgnorerCNVQuantitativeClientLivre.setEnabled(false);
            cbIgnorerCNVQuantitativeClientLivre.setName("cbIgnorerCNVQuantitativeClientLivre");
            pnlClientLivre.add(cbIgnorerCNVQuantitativeClientLivre, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlClientLivre, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- sNLabelTitre1 ----
          sNLabelTitre1.setText("Liste des cas de calculs");
          sNLabelTitre1.setModeReduit(true);
          sNLabelTitre1.setName("sNLabelTitre1");
          pnlContenu.add(sNLabelTitre1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== spListeConditionVente ========
          {
            spListeConditionVente.setBackground(Color.white);
            spListeConditionVente.setName("spListeConditionVente");
            
            // ---- tblListeFormulePrixVente ----
            tblListeFormulePrixVente.setModel(new DefaultTableModel(
                new Object[][] { { null, null, null, null, null, null, null, null }, }, new String[] { "Libell\u00e9", "Type",
                    "Rattachement client", "Rattachement article", "Prix base HT", "Prix net HT", "Montant HT", "Commentaire" }));
            {
              TableColumnModel cm = tblListeFormulePrixVente.getColumnModel();
              cm.getColumn(0).setPreferredWidth(200);
              cm.getColumn(1).setPreferredWidth(200);
              cm.getColumn(2).setPreferredWidth(200);
              cm.getColumn(3).setPreferredWidth(200);
              cm.getColumn(4).setPreferredWidth(100);
              cm.getColumn(5).setPreferredWidth(100);
              cm.getColumn(6).setPreferredWidth(100);
              cm.getColumn(7).setPreferredWidth(200);
            }
            tblListeFormulePrixVente.setFont(new Font("sansserif", Font.PLAIN, 11));
            tblListeFormulePrixVente.setBackground(Color.white);
            tblListeFormulePrixVente.setFillsViewportHeight(true);
            tblListeFormulePrixVente.setName("tblListeFormulePrixVente");
            spListeConditionVente.setViewportView(tblListeFormulePrixVente);
          }
          pnlContenu.add(spListeConditionVente, new GridBagConstraints(0, 4, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlFond.add(pnlContenu, BorderLayout.CENTER);
      }
      scpFond.setViewportView(pnlFond);
    }
    add(scpFond, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scpFond;
  private SNPanelFond pnlFond;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbMoisEnCoursDG;
  private SNTexte tfMoisEnCoursDG;
  private SNLabelChamp lbDeviseEtablissement;
  private SNTexte tfDeviseEtablissement;
  private SNLabelChamp lbGroupeCNVStandardEtablissement;
  private SNTexte tfGroupeCNVStandardEtablissement;
  private SNLabelChamp lbGroupeCNVPromoEtablissement;
  private SNTexte tfGroupeCNVPromoEtablissement;
  private SNLabelChamp lbCNVClientArticlePrioritaire;
  private SNCheckBoxReduit cbCNVClientArticlePrioritaire;
  private SNPanelTitre pnlDocumentVente;
  private SNLabelChamp lbDateCreation;
  private SNTexte tfDateCreation;
  private SNLabelChamp lbDeviseDocumentVente;
  private SNTexte tfDeviseDocumentVente;
  private SNLabelChamp lbCNVDocumentVente;
  private SNTexte tfCNVDocumentVente;
  private SNLabelChamp lbNumeroClientCentrale1DocumentVente;
  private SNTexte tfNumeroClientCentrale1DocumentVente;
  private SNPanelTitre pnlResultat;
  private SNLabelChamp lbDateApplication;
  private SNTexte tfDateApplication;
  private SNLabelChamp lbOrigineDevise;
  private JLabel tfOrigineDevise;
  private SNLabelChamp lbDevise;
  private SNTexte tfDevise;
  private SNLabelChamp lbOriginePrixVente;
  private JLabel tfOriginePrixVente;
  private SNLabelChamp lbColonneTarif;
  private SNComboBoxReduit cbColonneTarif;
  private SNLabelChamp lbPrixBaseHT;
  private SNMontant tfPrixBaseHT;
  private SNLabelChamp lbPrixBaseTTC;
  private SNMontant tfPrixBaseTTC;
  private SNLabelChamp lbTauxRemiseUnitaire;
  private SNTexte tfTauxRemiseUnitaire;
  private SNLabelChamp lbPrixNetHT;
  private SNMontant tfPrixNetHT;
  private SNLabelChamp lbPrixNetTTC;
  private SNMontant tfPrixNetTTC;
  private SNLabelChamp lbQuantiteUV;
  private SNMontant tfQuantiteUV;
  private SNLabelChamp lbMontantHT;
  private SNMontant tfMontantHT;
  private SNLabelChamp lbMontantTTC;
  private SNMontant tfMontantTTC;
  private SNPanelTitre pnlClientFacture;
  private SNLabelChamp lbDeviseCLientFacture;
  private SNTexte tfDeviseClientFacture;
  private SNLabelChamp lbIdentifiantClientFactureClientFacture;
  private SNTexte tfIdentifiantClientFactureClientFacture;
  private SNLabelChamp lbNumeroCentraleAchatClientFacture;
  private SNPanel pnlNumeroCentraleAchatClientFacture;
  private SNTexte tfNumeroCentraleAchat1ClientFacture;
  private SNTexte tfNumeroCentraleAchat2ClientFacture;
  private SNTexte tfNumeroCentraleAchat3ClientFacture;
  private SNLabelChamp lbGroupeCNVStandardClientFacture;
  private SNTexte tfGroupeCNVStandardClientFacture;
  private SNLabelChamp lbGroupeCNVPromoClientFacture;
  private SNTexte tfGroupeCNVPromoClientFacture;
  private SNLabelChamp lbExclusionCNVEtablissementClientFacture;
  private SNTexte tfExclusionCNVEtablissementClientFacture;
  private SNLabelChamp lbIgnorerCNVQuantitativeClientFacture;
  private SNCheckBoxReduit cbIgnorerCNVQuantitativeClientFacture;
  private SNPanelTitre pnlArticle;
  private SNLabelChamp lbFamille;
  private SNTexte tfFamille;
  private SNLabelChamp lbSousFamille;
  private SNTexte tfSousFamille;
  private SNLabelChamp lbIdFournisseur;
  private SNTexte tfIdFournisseur;
  private SNLabelChamp lbIdRéférenceTarif;
  private SNTexte tfIdReferenceTarif;
  private SNLabelChamp lbRegroupementAchat;
  private SNTexte tfRegroupementAchat;
  private SNLabelChamp lbRattachementCNV;
  private SNPanel pnlPRattachementCNV;
  private SNTexte tfRattachementCNV;
  private SNTexte tfRattachementCNV1;
  private SNTexte tfRattachementCNV2;
  private SNTexte tfRattachementCNV3;
  private SNPanelTitre pnlClientLivre;
  private SNLabelChamp lbDeviseCLientLivre;
  private SNTexte tfDeviseClientLivre;
  private SNLabelChamp lbIdentifiantClientFactureClientLivre;
  private SNTexte tfIdentifiantClientFactureClientLivre;
  private SNLabelChamp lbNumeroCentraleAchatClientLivre;
  private SNPanel pnlNumeroCentraleAchatClientLivre;
  private SNTexte tfNumeroCentraleAchat1ClientLivre;
  private SNTexte tfNumeroCentraleAchat2ClientLivre;
  private SNTexte tfNumeroCentraleAchat3ClientLivre;
  private SNLabelChamp lbGroupeCNVStandardClientLivre;
  private SNTexte tfGroupeCNVStandardClientLivre;
  private SNLabelChamp lbGroupeCNVPromoClientLivre;
  private SNTexte tfGroupeCNVPromoClientLivre;
  private SNLabelChamp lbExclusionCNVEtablissementClientLivre;
  private SNTexte tfExclusionCNVEtablissementClientLivre;
  private SNLabelChamp lbIgnorerCNVQuantitativeClientLivre;
  private SNCheckBoxReduit cbIgnorerCNVQuantitativeClientLivre;
  private SNLabelTitre sNLabelTitre1;
  private JScrollPane spListeConditionVente;
  private NRiTable tblListeFormulePrixVente;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
