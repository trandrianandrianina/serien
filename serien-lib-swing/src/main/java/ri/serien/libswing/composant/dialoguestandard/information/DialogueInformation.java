/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.dialoguestandard.information;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue "Message d'information".
 * Elle permet d'afficher des messages simples à l'utilisateur. Celui-ci peut uniquement cliquer sur un bouton "Fermer" pour fermer
 * la boîte de dialogue.
 */
public class DialogueInformation extends AbstractVueDialogue<ModeleDialogueInformation> {
  /**
   * Contructeur.
   */
  public DialogueInformation(ModeleDialogueInformation pModele) {
    super(pModele, false);
  }
  
  /**
   * Afficher la boîte de dialogue avec un message.
   * A utiliser uniquement si la variante avec Component n'est pas utilisable.
   */
  public static void afficher(String pMessage) {
    ModeleDialogueInformation modele = new ModeleDialogueInformation();
    DialogueInformation vue = new DialogueInformation(modele);
    modele.setMessage(pMessage);
    vue.afficher();
  }
  
  /**
   * Afficher la boîte de dialogue avec un message sans subir le formatage Html par défaut.
   * A utiliser uniquement si la variante avec Component n'est pas utilisable.
   */
  public static void afficherTexteNonCentre(String pMessage) {
    ModeleDialogueInformation modele = new ModeleDialogueInformation();
    DialogueInformation vue = new DialogueInformation(modele);
    modele.setCentrerTexte(false);
    modele.setMessage(pMessage);
    vue.afficher();
  }

  /**
   * Initialiser les composants graphiques de la boîte de dialogue.
   */
  @Override
  public void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Rafraichissement des données à l'écran.
   */
  @Override
  public void rafraichir() {
    // Lire le message du modèle
    String message = getModele().getMessage();
    if (message == null) {
      message = "";
    }
    
    // Formater le message utilisateur pour l'afficher en HTML si demandé
    message = message.replaceAll("<", "&lt;");
    message = message.replaceAll(">", "&gt;");
    message = message.replaceAll("\\n", "<br>");
    message = message.replaceAll("\\t", "&nbsp;&nbsp;");
    if (getModele().isCentreTexte()) {
      message = "<html><div style='text-align: center;'>" + message + "</div></html>";
    }
    else {
      message = "<html><div style='text-align: left;'>" + message + "</div></html>";
    }
    lbMessageUtilisateur.setText(message);
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new JPanel();
    lbMessageUtilisateur = new JLabel();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Message d'information");
    setBackground(new Color(238, 238, 210));
    setResizable(false);
    setModal(true);
    setMinimumSize(new Dimension(600, 300));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBorder(null);
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setOpaque(false);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new BorderLayout());
        
        // ---- lbMessageUtilisateur ----
        lbMessageUtilisateur.setFont(lbMessageUtilisateur.getFont().deriveFont(lbMessageUtilisateur.getFont().getStyle() | Font.BOLD,
            lbMessageUtilisateur.getFont().getSize() + 3f));
        lbMessageUtilisateur.setBackground(new Color(238, 238, 210));
        lbMessageUtilisateur.setBorder(new EmptyBorder(10, 10, 10, 10));
        lbMessageUtilisateur.setAutoscrolls(false);
        lbMessageUtilisateur.setHorizontalAlignment(SwingConstants.CENTER);
        lbMessageUtilisateur.setHorizontalTextPosition(SwingConstants.CENTER);
        lbMessageUtilisateur.setName("lbMessageUtilisateur");
        pnlContenu.add(lbMessageUtilisateur, BorderLayout.CENTER);
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlContenu;
  private JLabel lbMessageUtilisateur;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
