/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.client.sntypefacturation;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.IdTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.ListeTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.TypeFacturation;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un type de facturation.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un type de facturation dans le logiciel. Il gère l'affichage des
 * TypeFacturations existants et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNTypeFacturation à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le RGVX08FM_A4.
 */
public class SNTypeFacturation extends SNComboBoxObjetMetier<IdTypeFacturation, TypeFacturation, ListeTypeFacturation> {
  
  /**
   * Constructeur par défaut.
   */
  public SNTypeFacturation() {
    super();
    
  }
  
  /**
   * Charger les données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeTypeFacturation());
  }
  
  /**
   * Ajouter le type de facturation sans TVA à la liste déroulante.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public void ajouterFacturationSansTVA(IdEtablissement pIdEtablissement) {
    ajouter(TypeFacturation.getInstanceFacturationSansTVA(pIdEtablissement));
  }
  
  /**
   * Ajouter le type de facturation UE à la liste déroulante.
   * @param pIdEtablissement Identifiant de l'établissement.
   */
  public void ajouterFacturationUE(IdEtablissement pIdEtablissement) {
    ajouter(TypeFacturation.getInstanceFacturationUE(pIdEtablissement));
  }
  
  /**
   * Sélectionner un TypeFacturation de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampTypeFacturation) {
    if (pChampTypeFacturation == null || pLexical.HostFieldGetData(pChampTypeFacturation) == null) {
      throw new MessageErreurException("Impossible de sélectionner le typeFacturation car son code est invalide.");
    }
    
    IdTypeFacturation idTypeFacturation = null;
    String valeurTypeFacturation = pLexical.HostFieldGetData(pChampTypeFacturation);
    if (valeurTypeFacturation != null && !valeurTypeFacturation.trim().isEmpty()) {
      idTypeFacturation = IdTypeFacturation.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampTypeFacturation));
    }
    
    setIdSelection(idTypeFacturation);
  }
  
  /**
   * Renseigner les champs RPG correspondant au TypeFacturation sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampTypeFacturation) {
    TypeFacturation typeFacturation = getSelection();
    if (typeFacturation != null) {
      pLexical.HostFieldPutData(pChampTypeFacturation, 0, typeFacturation.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampTypeFacturation, 0, "");
    }
  }
}
