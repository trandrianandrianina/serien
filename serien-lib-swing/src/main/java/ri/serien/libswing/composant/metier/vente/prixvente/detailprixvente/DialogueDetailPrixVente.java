/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue du détail du calcul d'un prix de vente.
 */
public class DialogueDetailPrixVente extends AbstractVueDialogue<ModeleDialogueDetailPrixVente> {
  private static final String BOUTON_COPIER_PRESSE_PAPIER = "Copier dans le presse-papier";
  private static final String BOUTON_AFFICHER_PRIX_HT = "Afficher prix HT";
  private static final String BOUTON_MASQUER_PRIX_HT = "Masquer prix HT";
  private static final String BOUTON_DEMARRER_SIMULATION = "Démarrer une simulation";
  private static final String BOUTON_ARRETER_SIMULATION = "Arrêter la simulation";
  private static final String BOUTON_TESTER_CALCUL_PRIX_VENTE = "Lancer testeur";

  /**
   * Contructeur.
   */
  public DialogueDetailPrixVente(ModeleDialogueDetailPrixVente pModele) {
    super(pModele);
  }

  /**
   * Initialiser les composants graphiques de la boîte de dialogue.
   */
  @Override
  public void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();

    // Agrandir la boîte de dialogue à sa taille préférable pour la consulter (cela évite les ascenseurs)
    // La taille minimum de la boîte de dialogue est fixée à 1250 x 750 mais elle est affichée en 1440 x 1024 par défaut,
    // taille pour laquelle le confort de consultation est le mieux.
    // Rappel des largeurs standards de Série N :
    // - Version standard = 1280 x 800
    // - Version haute = 1280 x 1024
    // - Version large = 1440 x 900
    // - Version grande = 1440 x 1024
    setPreferredSize(new Dimension(1440, 1024));
    pack();

    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_COPIER_PRESSE_PAPIER, 'C', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_PRIX_HT, 'H', false);
    snBarreBouton.ajouterBouton(BOUTON_MASQUER_PRIX_HT, 'H', false);
    snBarreBouton.ajouterBouton(BOUTON_DEMARRER_SIMULATION, 'S', true);
    snBarreBouton.ajouterBouton(BOUTON_ARRETER_SIMULATION, 'S', false);
    snBarreBouton.ajouterBouton(BOUTON_TESTER_CALCUL_PRIX_VENTE, 'T', false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichissement des données à l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirTitre();
    rafraichirListeVue();
    rafraichirVueActive();

    // Rafraîchir les boutons
    rafraichirBoutonAfficherPrixHT();
    rafraichirBoutonMasquerPrixHT();
    rafraichirBoutonDemarrerSimulation();
    rafraichirBoutonArreterSimultation();
    rafraichirBoutonTesterCalculPrixVente();
  }

  /**
   * Rafraichir le titre de la boîte de dialogue.
   */
  private void rafraichirTitre() {
    setTitle(getModele().getTitre());
  }

  /**
   * Rafraichir la liste des vues.
   * Pour l'instant, la méthode se contente d'initialiser les vues après le chargement des données.
   * Elle ne gère pas le changement du nombre de vues.
   */
  private void rafraichirListeVue() {
    ListeCleVueDetailPrixVente listeCleVue = getModele().getListeCleVue();
    if (tbpOnglet.getTabCount() == 0 && listeCleVue != null) {
      for (CleVueDetailPrixVente cleVue : listeCleVue) {
        if (cleVue.getIndex() == 0) {
          // Créer l'onglet synthèse en première position
          VueSynthese vueSynthese = new VueSynthese(getModele());
          ajouterVueEnfant(cleVue, vueSynthese);
          tbpOnglet.addTab(cleVue.getLibelle(), null, vueSynthese, cleVue.getLibelle());
        }
        else {
          // Créer un onglet pour chaque formule de calcul
          VueFormule vueFormule = new VueFormule(getModele());
          ajouterVueEnfant(cleVue, vueFormule);
          tbpOnglet.addTab(cleVue.getLibelle(), null, vueFormule, cleVue.getLibelle());
        }
      }
    }
  }

  /**
   * Rafraichir la vue active.
   */
  private void rafraichirVueActive() {
    InterfaceCleVue interfaceCleVue = getModele().getCleVueEnfantActive();
    if (interfaceCleVue != null) {
      tbpOnglet.setSelectedIndex(interfaceCleVue.getIndex());
    }
  }

  /**
   * Rafraichir le bouton pour démarrer une simulation.
   */
  private void rafraichirBoutonAfficherPrixHT() {
    if (getModele().getFormulePrixVente() != null && getModele().getFormulePrixVente().isModeTTC() && !getModele().isPrixHTVisible()) {
      snBarreBouton.activerBouton(BOUTON_AFFICHER_PRIX_HT, true);
    }
    else {
      snBarreBouton.activerBouton(BOUTON_AFFICHER_PRIX_HT, false);
    }
  }

  /**
   * Rafraichir le bouton pour démarrer une simulation.
   */
  private void rafraichirBoutonMasquerPrixHT() {
    if (getModele().getFormulePrixVente() != null && getModele().getFormulePrixVente().isModeTTC() && getModele().isPrixHTVisible()) {
      snBarreBouton.activerBouton(BOUTON_MASQUER_PRIX_HT, true);
    }
    else {
      snBarreBouton.activerBouton(BOUTON_MASQUER_PRIX_HT, false);
    }
  }
  
  /**
   * Rafraichir le bouton pour démarrer une simulation.
   */
  private void rafraichirBoutonDemarrerSimulation() {
    snBarreBouton.activerBouton(BOUTON_DEMARRER_SIMULATION, !getModele().isModeSimulation());
  }

  /**
   * Rafraichir le bouton pour arrêter une simulation.
   */
  private void rafraichirBoutonArreterSimultation() {
    snBarreBouton.activerBouton(BOUTON_ARRETER_SIMULATION, getModele().isModeSimulation());
  }

  private void rafraichirBoutonTesterCalculPrixVente() {
    snBarreBouton.activerBouton(BOUTON_TESTER_CALCUL_PRIX_VENTE, Trace.isModeDebug());
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(BOUTON_COPIER_PRESSE_PAPIER)) {
        getModele().copierPressePapier();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_PRIX_HT)) {
        getModele().afficherPrixHT(true);
      }
      else if (pSNBouton.isBouton(BOUTON_MASQUER_PRIX_HT)) {
        getModele().afficherPrixHT(false);
      }
      else if (pSNBouton.isBouton(BOUTON_DEMARRER_SIMULATION)) {
        getModele().activerModeSimulation(true);
      }
      else if (pSNBouton.isBouton(BOUTON_ARRETER_SIMULATION)) {
        getModele().activerModeSimulation(false);
      }
      else if (pSNBouton.isBouton(BOUTON_TESTER_CALCUL_PRIX_VENTE)) {
        getModele().testerCalculPrixVente();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Traiter les clics que les onglets pour changer la vue active.
   */
  private void tbpOngletStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().changerOnglet(tbpOnglet.getSelectedIndex());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snPanelFond = new SNPanelFond();
    tbpOnglet = new JTabbedPane();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("D\u00e9tail du calcul du prix de vente");
    setBackground(new Color(238, 238, 210));
    setMinimumSize(new Dimension(1250, 750));
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== snPanelFond ========
    {
      snPanelFond.setName("snPanelFond");
      snPanelFond.setLayout(new BorderLayout());
      
      // ======== tbpOnglet ========
      {
        tbpOnglet.setFocusable(false);
        tbpOnglet.setBorder(null);
        tbpOnglet.setName("tbpOnglet");
        tbpOnglet.addChangeListener(e -> tbpOngletStateChanged(e));
      }
      snPanelFond.add(tbpOnglet, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      snPanelFond.add(snBarreBouton, BorderLayout.PAGE_END);
    }
    contentPane.add(snPanelFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond snPanelFond;
  private JTabbedPane tbpOnglet;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
