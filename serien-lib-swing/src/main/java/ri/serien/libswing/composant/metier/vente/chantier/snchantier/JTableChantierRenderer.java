/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.chantier.snchantier;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.vente.chantier.EnumStatutChantier;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;

public class JTableChantierRenderer extends DefaultTableCellRenderer {
  // Variable
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  
  private int[] justification = null;
  private int[] dimension = null;
  
  /**
   * Constructeur.
   */
  public JTableChantierRenderer() {
  }
  
  /**
   * Constructeur.
   */
  public JTableChantierRenderer(int[] pDimension, int[] pJustification) {
    justification = pJustification;
    dimension = pDimension;
  }
  
  /**
   * Retourne la cellule.
   */
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    final Component cellule = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    TableColumnModel cm = table.getColumnModel();
    
    ColorRenderer cr = new ColorRenderer("Statut");
    
    // Désactivation des lignes non validées
    for (int i = 0; i < dimension.length; i++) {
      table.getColumn(table.getColumnName(i)).setCellRenderer(cr);
    }
    
    // Taille des colonnes
    redimensionnerColonnes(cm);
    
    // Justification des colonnes
    justifierColonnes(cellule, column);
    
    return cellule;
  }
  
  /**
   * Redimensionne les colonnes de la table.
   */
  public void redimensionnerColonnes(TableColumnModel pTableColonneModele) {
    if (dimension == null) {
      return;
    }
    for (int i = 0; (i < dimension.length) && (i < pTableColonneModele.getColumnCount()); i++) {
      pTableColonneModele.getColumn(i).setMinWidth(dimension[i]);
      // pTableColonneModele.getColumn(i).setMaxWidth(dimension[i]);
    }
  }
  
  /**
   * Initialisation du tableau des justifications.
   */
  public void justifierColonnes(Component pCellule, int pIndice) {
    if ((justification == null) && (pIndice < 0)) {
      return;
    }
    
    switch (justification[pIndice]) {
      case NRiTable.GAUCHE:
        ((JLabel) pCellule).setHorizontalAlignment(JLabel.LEFT);
        break;
      case NRiTable.CENTRE:
        ((JLabel) pCellule).setHorizontalAlignment(JLabel.CENTER);
        break;
      case NRiTable.DROITE:
        ((JLabel) pCellule).setHorizontalAlignment(JLabel.RIGHT);
        break;
    }
  }
  
}

class ColorRenderer extends JLabel implements TableCellRenderer {
  private String columnName;
  
  public ColorRenderer(String column) {
    this.columnName = column;
    setOpaque(true);
  }
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Object columnValue = table.getValueAt(row, table.getColumnModel().getColumnIndex(columnName));
    
    if (value != null) {
      setText(value.toString());
    }
    if (isSelected) {
      setBackground(table.getSelectionBackground());
      setForeground(table.getSelectionForeground());
    }
    else {
      setBackground(table.getBackground());
      setForeground(table.getForeground());
      if (columnValue != null) {
        if (columnValue.equals(EnumStatutChantier.VALIDE.getLibelle()) || columnValue.equals(EnumStatutChantier.EDITE.getLibelle())) {
          setEnabled(true);
        }
        else {
          setEnabled(false);
        }
      }
    }
    return this;
  }
}
