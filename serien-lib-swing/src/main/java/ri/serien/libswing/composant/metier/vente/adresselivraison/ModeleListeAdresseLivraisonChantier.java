/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.adresselivraison;

import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.adressedocument.AdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.CritereAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.EnumCodeEnteteAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.IdAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.ListeAdresseDocument;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.confirmation.ModeleDialogueConfirmation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Boite de dialogue de gestion des adresses associées à un document de vente.
 */
public class ModeleListeAdresseLivraisonChantier extends AbstractModeleDialogue {
  // Constantes
  public static final int ETAPE_CONSULTATION = 0;
  public static final int ETAPE_CREATION = 1;
  public static final int ETAPE_MODIFICATION = 2;
  public static final int ETAPE_SUPPRESSION = 3;

  // Variables
  private int etape = ETAPE_CONSULTATION;
  private IdDocumentVente idDocument = null;
  private EnumCodeEnteteAdresseDocument typeAdresse = null;
  private ListeAdresseDocument listeAdresseDocument = null;
  private int indexAdresseSelectionne = -1;
  private AdresseDocument adresseSelectionnee = null;
  private boolean isAdresseModifiable = false;
  private boolean isAvecBonton = true;
  private Adresse blocAdresseEnCours = null;
  private Message titreListe = null;

  /**
   * Constructeur.
   */
  public ModeleListeAdresseLivraisonChantier(SessionBase pSession, IdDocumentVente pIdDocumentVente,
      EnumCodeEnteteAdresseDocument pType) {
    super(pSession);
    idDocument = pIdDocumentVente;
    typeAdresse = pType;
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    etape = ETAPE_CONSULTATION;
    
  }

  @Override
  public void chargerDonnees() {
    chargerListeAdresseDocument();
  }

  /**
   * Charger la liste des adresses pour le document en cours
   */
  private void chargerListeAdresseDocument() {
    if (idDocument == null) {
      throw new MessageErreurException("L'identifiant du document est invalide pour la recherche des adresses du document");
    }

    CritereAdresseDocument critereAdresseDocument = new CritereAdresseDocument();
    critereAdresseDocument.setIdEtablissement(idDocument.getIdEtablissement());
    critereAdresseDocument.setIdDocument(idDocument);
    if (typeAdresse != null) {
      critereAdresseDocument.setTypeAdresse(typeAdresse);
    }
    
    // Chargement des identifiants
    List<IdAdresseDocument> listeIdAdresseDocument =
        ManagerServiceDocumentVente.chargerListeIdAdresseDocument(getIdSession(), critereAdresseDocument);
    
    if (listeIdAdresseDocument == null || listeIdAdresseDocument.size() == 0) {
      return;
    }

    listeAdresseDocument = ListeAdresseDocument.creerListeNonChargee(listeIdAdresseDocument);
    
    // Chargement de la liste
    listeAdresseDocument = ManagerServiceDocumentVente.chargerListeAdresseDocument(getIdSession(), listeIdAdresseDocument);
    construireTitre();
  }
  
  /**
   * Afficher la plage d'adresses comprise entre deux lignes.
   * Les informations des adresses sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageAdresseAffiche(int premiereLigne, int derniereLigne) {
    // XXX Auto-generated method stub

  }
  
  /**
   * Créer une adresse pour le document
   */
  public void creerAdresseDocument() {
    etape = ETAPE_CREATION;
    indexAdresseSelectionne = -1;
    setIsAdresseModifiable();
    blocAdresseEnCours = new Adresse();
    chargerListeAdresseDocument();
    adresseSelectionnee = null;

    rafraichir();
  }

  /**
   * Modifier l'adresse sélectionnée
   */
  public void modifierAdresseDocument() {
    if (indexAdresseSelectionne < 0) {
      return;
    }
    etape = ETAPE_MODIFICATION;
    blocAdresseEnCours = adresseSelectionnee.getAdresse();
    setIsAdresseModifiable();
    chargerListeAdresseDocument();
    rafraichir();
  }

  /**
   * Supprimer l'adresse sélectionnée
   */
  public void supprimerAdresseDocument() {
    if (indexAdresseSelectionne <= 0) {
      return;
    }
    etape = ETAPE_SUPPRESSION;
    setIsAdresseModifiable();
    ModeleDialogueConfirmation modeleConfirmation = new ModeleDialogueConfirmation(getSession(), "Suppression de contact",
        "<center>ATTENTION\nVous avez demandé la suppression de l'adresse en cours.\nCette action sera définitive.\n"
            + "\nConfirmez vous cette demande ?</center>");
    DialogueConfirmation vueConfirmation = new DialogueConfirmation(modeleConfirmation);
    vueConfirmation.afficher();
    if (modeleConfirmation.isSortieAvecValidation()) {
      ManagerServiceDocumentVente.supprimerAdresseDocument(getIdSession(), adresseSelectionnee);
    }
    chargerListeAdresseDocument();
    adresseSelectionnee = null;
    indexAdresseSelectionne = -1;
    etape = ETAPE_CONSULTATION;
    setIsAdresseModifiable();
    rafraichir();
  }
  
  /**
   * Modifier le nom dans le bloc adresse
   */
  public void modifierNom(String pNom) {
    if (blocAdresseEnCours == null) {
      return;
    }
    blocAdresseEnCours.setNom(pNom);
    rafraichir();
  }

  /**
   * Modifier le complément de nom dans le bloc adresse
   */
  public void modifierComplementNom(String pComplementNom) {
    if (blocAdresseEnCours == null) {
      return;
    }
    blocAdresseEnCours.setComplementNom(pComplementNom);
    rafraichir();
  }

  /**
   * Modifier l'adresse 1 dans le bloc adresse
   */
  public void modifierRue(String pRue) {
    if (blocAdresseEnCours == null) {
      return;
    }
    blocAdresseEnCours.setRue(pRue);
    rafraichir();
  }

  /**
   * Modifier l'adresse 2 dans le bloc adresse
   */
  public void modifierlocalisation(String pLocalisation) {
    if (blocAdresseEnCours == null) {
      return;
    }
    blocAdresseEnCours.setLocalisation(pLocalisation);
    rafraichir();
  }

  /**
   * Modifier la commune dans le bloc adresse
   */
  public void modifierCommune(CodePostalCommune pCommune) {
    if (blocAdresseEnCours == null) {
      return;
    }
    if (pCommune != null) {
      blocAdresseEnCours.setCodePostal(pCommune.getCodePostalFormate());
      blocAdresseEnCours.setVille(pCommune.getVille());
    }
    else {
      blocAdresseEnCours.setCodePostal(0);
      blocAdresseEnCours.setVille("");
    }
    rafraichir();
  }

  /**
   * Modifier l'adresse sélectionnée
   */
  public void afficherDetailAdresse() {
    if (indexAdresseSelectionne < 0) {
      adresseSelectionnee = null;
    }
    else {
      adresseSelectionnee = listeAdresseDocument.get(indexAdresseSelectionne);
    }
    rafraichir();
  }

  /**
   * Construction du titre de la liste suivant le résultat de recherche
   */
  private void construireTitre() {
    if (listeAdresseDocument == null) {
      titreListe = Message.getMessageNormal("Adresses correspondant à votre recherche");
    }
    else if (listeAdresseDocument.size() == 1) {
      titreListe = Message.getMessageNormal("Adresse correspondant à votre recherche (1)");
    }
    else if (listeAdresseDocument.size() > 1) {
      titreListe = Message.getMessageNormal("Adresses correspondant à votre recherche (" + listeAdresseDocument.size() + ")");
    }
    else {
      titreListe = Message.getMessageImportant("Aucune adresses ne correspond à votre recherche");
    }
  }
  
  private void setIsAdresseModifiable() {
    if (indexAdresseSelectionne >= 0 && etape != ETAPE_CONSULTATION) {
      isAdresseModifiable = true;
    }
    else if (etape == ETAPE_CREATION) {
      isAdresseModifiable = true;
    }
    else {
      isAdresseModifiable = false;
    }
  }

  @Override
  public void quitterAvecValidation() {
    switch (etape) {
      case ETAPE_CREATION:
        if (blocAdresseEnCours != null && !blocAdresseEnCours.isEmpty()) {
          int numeroAdresse = ManagerServiceDocumentVente.chercherPremierNumeroAdresseLibre(getIdSession(), idDocument);
          IdAdresseDocument idAdresse = IdAdresseDocument.getInstance(idDocument.getIdEtablissement(),
              EnumCodeEnteteAdresseDocument.ADRESSE_LIVRAISON_DEVIS, idDocument, numeroAdresse);
          AdresseDocument adresseDocument = new AdresseDocument(idAdresse);
          adresseDocument.setAdresse(blocAdresseEnCours);
          ManagerServiceDocumentVente.creerAdresseDocument(getIdSession(), adresseDocument);
          creerAdresseDocument();
          etape = ETAPE_CONSULTATION;
          setIsAdresseModifiable();
          rafraichir();
        }
        break;

      case ETAPE_MODIFICATION:
        ManagerServiceDocumentVente.sauverAdresseDocument(getIdSession(), adresseSelectionnee);
        chargerListeAdresseDocument();
        etape = ETAPE_CONSULTATION;
        setIsAdresseModifiable();
        rafraichir();
        break;

      case ETAPE_SUPPRESSION:
        super.quitterAvecValidation();
        break;
      
      default:
        super.quitterAvecValidation();
        break;
    }
  }
  
  /**
   * Liste des adresses chargées
   */
  public ListeAdresseDocument getListeAdresseDocument() {
    return listeAdresseDocument;
  }

  /**
   * Index sélectionné dans la liste
   */
  public int getIndexAdresseSelectionne() {
    return indexAdresseSelectionne;
  }
  
  /**
   * Modifier l'index sélectionné dans la liste
   */
  public void modifierAdresseSelectionnee(int pIndexAdresseSelectionne) {
    // Contrôle de l'index sélectionné
    if (pIndexAdresseSelectionne < 0 || pIndexAdresseSelectionne >= listeAdresseDocument.size()) {
      pIndexAdresseSelectionne = -1;
    }
    if (pIndexAdresseSelectionne == indexAdresseSelectionne) {
      return;
    }
    if (pIndexAdresseSelectionne == 0) {
      etape = ETAPE_CONSULTATION;
    }
    indexAdresseSelectionne = pIndexAdresseSelectionne;
    adresseSelectionnee = listeAdresseDocument.get(indexAdresseSelectionne);
    blocAdresseEnCours = adresseSelectionnee.getAdresse();
    setIsAdresseModifiable();
    rafraichir();
  }
  
  /**
   * Adresse sélectionnée
   */
  public AdresseDocument getAdresseSelectionnee() {
    return adresseSelectionnee;
  }
  
  /**
   * Indiquer si l'adresse en cours est modifiable
   */
  public boolean isAdresseModifiable() {
    return isAdresseModifiable;
  }
  
  /**
   * Etape en cours dans l'utillisation de la boite de dialogue
   */
  public int getEtape() {
    return etape;
  }

  /**
   * Identifiant du document en cours
   */
  public IdDocumentVente getIdDocument() {
    return idDocument;
  }
  
  /**
   * Retourner le titre de la liste modifié selon le contenu de la liste
   */
  public Message getTitreListe() {
    return titreListe;
  }

  /**
   * Indiquer si le composant propose les boutons de création, modification et suppression
   */
  public boolean isAvecBonton() {
    return isAvecBonton;
  }

  /**
   * Modifier si le composant propose les boutons de création, modification et suppression
   */
  public void setAvecBonton(boolean pIsAvecBonton) {
    isAvecBonton = pIsAvecBonton;
  }
  
  public Adresse getAdressePrincipale() {
    if (listeAdresseDocument != null && listeAdresseDocument.get(0) != null) {
      return listeAdresseDocument.get(0).getAdresse();
    }
    return null;
  }
  
  public Adresse getBlocAdresseEnCours() {
    return blocAdresseEnCours;
  }

}
