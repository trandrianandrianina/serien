/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.dialoguestandard.erreur;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;
import ri.serien.libswing.outil.clipboard.Clipboard;

/**
 * Modèle de la boîte de dialogue d'affichage des messages d'erreur.
 */
public class ModeleDialogueErreur extends AbstractModeleDialogue {
  private Throwable throwable = null;
  private String message = null;
  private String messageDetaille = null;
  private boolean afficherDetail = false;

  /**
   * Constructeur.
   */
  public ModeleDialogueErreur() {
    super(null);
  }

  @Override
  public void initialiserDonnees() {
  }

  @Override
  public void chargerDonnees() {
  }

  /**
   * Renseigner l'exception à afficher.
   *
   * Si une exception générique est passée en paramètre, c'est le message par défaut qui est affiché ("Erreur technique").
   *
   * Si une exception MessageUtilisateurException est passée en paramètre, le message à afficher sera extrait de cette exception.
   * Il peut arriver que plusieurs MessageUtilisateurException soient empilés, chacun avec un message différent. Le premier message sera
   * le plus vague tandis que le dernier sera le plus précis. ans ce cas, tous les messages sont affichés dans la boîte de dialogue,
   * séparés par un retour un ligne.
   *
   * @param pThrowable Exception contenant le message à afficher dans la boîte de dialogue.
   */
  public void setException(Throwable pThrowable) {
    // Vérifier si la valeur a changé
    if (pThrowable == throwable) {
      return;
    }
    
    // Mettre à jour la valeur
    throwable = pThrowable;

    // Rechercher le premier MessageUtilisateurException dans la pile d'exceptions
    MessageErreurException messageErreurException = null;
    for (Throwable cause = throwable; cause != null; cause = cause.getCause()) {
      if (cause instanceof MessageErreurException) {
        messageErreurException = (MessageErreurException) cause;
        break;
      }
    }

    // Créer une exception "Erreur technique" si on a pas trouvé de MessageErreurException
    if (messageErreurException == null) {
      messageErreurException = new MessageErreurException(pThrowable, MessageErreurException.MESSAGE_ERREUR_TECHNIQUE);
    }
    
    // Récupérer le message utilisateur
    message = messageErreurException.getMessageUtilisateur();
    if (message == null || message.trim().isEmpty()) {
      message = MessageErreurException.MESSAGE_ERREUR_TECHNIQUE;
    }

    // Construire le message détaillé
    messageDetaille = "";
    if (messageErreurException.getMessageTechnique() != null) {
      messageDetaille += messageErreurException.getMessageTechnique();
    }
    if (messageErreurException.getMessageStackTrace() != null) {
      if (!messageDetaille.isEmpty()) {
        messageDetaille += "\n\n";
      }
      messageDetaille += messageErreurException.getMessageStackTrace();
    }

    // Tracer le message
    String trace = message.replaceAll("(\\r|\\n)", " ");
    if (messageDetaille != null && !messageDetaille.isEmpty()) {
      trace += "\n" + messageDetaille;
    }
    Trace.erreur(trace);
  }

  /**
   * Activer ou désactiver l'affichage du message détaillé.
   *
   * @param pAfficherDetail false=afficher uniquement le message d'erreur, true = afficher le détail de l'exception.
   */
  public void modifierAfficherDetail(boolean pAfficherDetail) {
    afficherDetail = pAfficherDetail;
    rafraichir();
  }

  /**
   * Copier le message détaillé dans le clipboard.
   */
  public void copierMessageDetaille() {
    if (messageDetaille != null && !messageDetaille.isEmpty()) {
      Clipboard.envoyerTexte(messageDetaille);
    }
    else if (message != null && !message.isEmpty()) {
      Clipboard.envoyerTexte(message);
    }
    else {
      Clipboard.envoyerTexte(MessageErreurException.MESSAGE_ERREUR_TECHNIQUE);
    }
  }

  // -- Accesseurs

  /**
   * Message associé à l'exception.
   */
  public String getMessage() {
    return message;
  }

  /**
   * Message détaillé associé à l'exception.
   * Ce message contient la stacktrace.
   */
  public String getMessageDetaille() {
    return messageDetaille;
  }

  /**
   * Indique s'il faut afficher le message détaillé.
   */
  public boolean isAfficherDetail() {
    return afficherDetail;
  }

}
