/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametrearticle;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.ParametreArticle;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.checkbox.SNCheckBoxReduit;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Ce composant permet d'afficher les paramètres d'un article.
 */
public class SNParametreArticle extends SNPanelTitre {
  private static final String TITRE = "Article";

  // Variables
  private ModeleDialogueDetailPrixVente modele = null;
  private boolean evenementsActifs = false;
  private ParametreArticle parametreArticle = null;
  private ParametreArticle parametreArticleInitial = null;

  /**
   * Constructeur.
   */
  public SNParametreArticle() {
    super();
    initComponents();

    // Afficher le PUMP et le PRV avec 4 décimales
    tfPump.setLongueurPartieDecimale(4);
    tfPrv.setLongueurPartieDecimale(4);
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Modèle associé au composant graphique.
   * @return Modèle de la boîte de dialogue.
   */
  public ModeleDialogueDetailPrixVente getModele() {
    return modele;
  }

  /**
   * Modifier le modèle associé au composant graphique.
   * @param pModele Modèle de la boîte de dialogue.
   */
  public void setModele(ModeleDialogueDetailPrixVente pModele) {
    modele = pModele;
  }

  /**
   * Indiquer si les évènements doivent être traités.
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir les données du composant.
   */
  public void rafraichir() {
    // Empêcher les évènements pendant le rafraichissement
    evenementsActifs = false;

    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    parametreArticle = getModele().getParametreArticle();
    parametreArticleInitial = getModele().getParametreArticleInitial();

    // Rafraichir les composants
    rafraichirTitre();
    rafraichirNumeroTVAEtablissement();
    rafraichirUniteVente();
    rafraichirRemisable();
    rafraichirTauxRemiseMaximum();
    rafraichirOriginePump();
    rafraichirPump();
    rafraichirTypePrv();
    rafraichirOriginePrv();
    rafraichirPrv();

    // Réactiver les évènements pendant le rafraichissement
    evenementsActifs = true;
  }

  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirTitre() {
    if (parametreArticle != null && parametreArticle.getIdArticle() != null) {
      setTitre(TITRE + " " + parametreArticle.getIdArticle());
    }
    else {
      setTitre(TITRE);
    }
  }

  /**
   * Rafraichir le numéro de TVA issu de l'établissement.
   * La liste contient "Aucun, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9" pour être capable d'afficher les bizzareries non possibles mais existantes.
   */
  private void rafraichirNumeroTVAEtablissement() {
    if (parametreArticle != null && parametreArticle.getNumeroTVAEtablissement() != null
        && parametreArticle.getNumeroTVAEtablissement() >= 0 && parametreArticle.getNumeroTVAEtablissement() <= 9) {
      cbNumeroTVAEtablissement.setSelectedIndex(parametreArticle.getNumeroTVAEtablissement() + 1);
    }
    else {
      cbNumeroTVAEtablissement.setSelectedIndex(0);
    }

    cbNumeroTVAEtablissement.setEnabled(modele != null && getModele().isModeSimulation());
    lbNumeroTVAEtablissement.setImportance(parametreArticle != null && parametreArticleInitial != null
        && !Constantes.equals(parametreArticle.getNumeroTVAEtablissement(), parametreArticleInitial.getNumeroTVAEtablissement()));
  }

  /**
   * Rafraichir l'unité de vente de l'article.
   */
  private void rafraichirUniteVente() {
    if (parametreArticle != null && parametreArticle.getCodeUniteVente() != null) {
      tfUniteVente.setText(parametreArticle.getCodeUniteVente());
    }
    else {
      tfUniteVente.setText("");
    }

    tfUniteVente.setEnabled(modele != null && modele.isModeSimulation());
    lbUniteVente.setImportance(parametreArticle != null && parametreArticleInitial != null
        && !Constantes.equals(parametreArticle.getCodeUniteVente(), parametreArticleInitial.getCodeUniteVente()));
  }

  /**
   * Rafraichir le caractère remisable.
   */
  private void rafraichirRemisable() {
    if (parametreArticle != null && parametreArticle.isRemisable()) {
      cbRemisable.setSelected(true);
    }
    else {
      cbRemisable.setSelected(false);
    }

    cbRemisable.setEnabled(modele != null && modele.isModeSimulation());
    lbRemisable.setImportance(parametreArticle != null && parametreArticleInitial != null
        && !Constantes.equals(parametreArticle.isRemisable(), parametreArticleInitial.isRemisable()));
  }

  /**
   * Rafraichir le taux de remise maximum.
   */
  private void rafraichirTauxRemiseMaximum() {
    if (parametreArticle != null && parametreArticle.getTauxRemiseMaximum() != null) {
      tfTauxRemiseMaximum.setText(parametreArticle.getTauxRemiseMaximum().toString());
    }
    else {
      tfTauxRemiseMaximum.setText("");
    }
  }

  /**
   * Rafraichir l'origine du PUMP de l'article.
   */
  private void rafraichirOriginePump() {
    if (parametreArticle != null && parametreArticle.getOriginePump() != null) {
      tfOriginePump.setText(parametreArticle.getOriginePump().getLibelle());
    }
    else {
      tfOriginePump.setText("");
    }
  }

  /**
   * Rafraichir le PUMP de l'article.
   */
  private void rafraichirPump() {
    if (parametreArticle != null && parametreArticle.getPump() != null) {
      tfPump.setMontant(parametreArticle.getPump());
    }
    else {
      tfPump.setMontant("");
    }

    tfPump.setEnabled(modele != null && modele.isModeSimulation());
    lbPump.setImportance(parametreArticle != null && parametreArticleInitial != null
        && !Constantes.equals(parametreArticle.getPump(), parametreArticleInitial.getPump()));
  }

  /**
   * Rafraichir le type de prix de revient de l'article.
   */
  private void rafraichirTypePrv() {
    if (parametreArticle != null & parametreArticle.getTypePrv() != null) {
      tfTypePrv.setText(parametreArticle.getTypePrv().getLibelle());
    }
    else {
      tfTypePrv.setText("");
    }
  }

  /**
   * Rafraichir l'origine du prix de revient de l'article.
   */
  private void rafraichirOriginePrv() {
    if (parametreArticle != null && parametreArticle.getOriginePrv() != null) {
      tfOriginePrv.setText(parametreArticle.getOriginePrv().getLibelle());
    }
    else {
      tfOriginePrv.setText("");
    }
  }

  /**
   * Rafraichir le prix de revient de l'article.
   */
  private void rafraichirPrv() {
    if (parametreArticle != null && parametreArticle.getPrv() != null) {
      tfPrv.setMontant(parametreArticle.getPrv());
    }
    else {
      tfPrv.setMontant("");
    }

    tfPrv.setEnabled(modele != null && modele.isModeSimulation());
    lbPrv.setImportance(parametreArticle != null && parametreArticleInitial != null
        && !Constantes.equals(parametreArticle.getPrv(), parametreArticleInitial.getPrv()));
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void cbNumeroTVAEtablissementItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbNumeroTVAEtablissement.getSelectedIndex() >= 1 && cbNumeroTVAEtablissement.getSelectedIndex() <= 10) {
        getModele().modifierNumeroTVAEtablissementArticle(cbNumeroTVAEtablissement.getSelectedIndex() - 1);
      }
      else {
        getModele().modifierNumeroTVAEtablissementArticle(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfUniteVenteFocusLost(FocusEvent e) {
    try {
      modele.modifierUniteVenteArticle(tfUniteVente.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbRemisableActionPerformed(ActionEvent e) {
    try {
      modele.modifierRemisableArticle(cbRemisable.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfPumpValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPumpArticle(tfPump.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfPrvValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrvArticle(tfPrv.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbNumeroTVAEtablissement = new SNLabelChamp();
    cbNumeroTVAEtablissement = new SNComboBoxReduit();
    lbUniteVente = new SNLabelChamp();
    tfUniteVente = new SNTexte();
    lbRemisable = new SNLabelChamp();
    cbRemisable = new SNCheckBoxReduit();
    lbTauxRemiseMaximum = new SNLabelChamp();
    tfTauxRemiseMaximum = new SNTexte();
    lbOriginePump = new SNLabelChamp();
    tfOriginePump = new SNTexte();
    lbPump = new SNLabelChamp();
    tfPump = new SNMontant();
    lbTypePrv = new SNLabelChamp();
    tfTypePrv = new SNTexte();
    lbOriginePrv = new SNLabelChamp();
    tfOriginePrv = new SNTexte();
    lbPrv = new SNLabelChamp();
    tfPrv = new SNMontant();
    
    // ======== this ========
    setTitre("Article");
    setModeReduit(true);
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    
    // ---- lbNumeroTVAEtablissement ----
    lbNumeroTVAEtablissement.setText("Num\u00e9ro TVA \u00e9tablissement");
    lbNumeroTVAEtablissement.setModeReduit(true);
    lbNumeroTVAEtablissement.setName("lbNumeroTVAEtablissement");
    add(lbNumeroTVAEtablissement,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbNumeroTVAEtablissement ----
    cbNumeroTVAEtablissement
        .setModel(new DefaultComboBoxModel(new String[] { "Aucun ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }));
    cbNumeroTVAEtablissement.setSelectedIndex(-1);
    cbNumeroTVAEtablissement.setEnabled(false);
    cbNumeroTVAEtablissement.setName("cbNumeroTVAEtablissement");
    cbNumeroTVAEtablissement.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        cbNumeroTVAEtablissementItemStateChanged(e);
      }
    });
    add(cbNumeroTVAEtablissement,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbUniteVente ----
    lbUniteVente.setText("Unit\u00e9 de vente");
    lbUniteVente.setModeReduit(true);
    lbUniteVente.setName("lbUniteVente");
    add(lbUniteVente,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfUniteVente ----
    tfUniteVente.setEnabled(false);
    tfUniteVente.setModeReduit(true);
    tfUniteVente.setName("tfUniteVente");
    tfUniteVente.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfUniteVenteFocusLost(e);
      }
    });
    add(tfUniteVente,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbRemisable ----
    lbRemisable.setModeReduit(true);
    lbRemisable.setText("Remisable");
    lbRemisable.setMinimumSize(new Dimension(30, 22));
    lbRemisable.setPreferredSize(new Dimension(30, 22));
    lbRemisable.setName("lbRemisable");
    add(lbRemisable,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbRemisable ----
    cbRemisable.setEnabled(false);
    cbRemisable.setName("cbRemisable");
    cbRemisable.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        cbRemisableActionPerformed(e);
      }
    });
    add(cbRemisable,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbTauxRemiseMaximum ----
    lbTauxRemiseMaximum.setModeReduit(true);
    lbTauxRemiseMaximum.setText("Taux remise maximum");
    lbTauxRemiseMaximum.setMinimumSize(new Dimension(30, 22));
    lbTauxRemiseMaximum.setPreferredSize(new Dimension(30, 22));
    lbTauxRemiseMaximum.setName("lbTauxRemiseMaximum");
    add(lbTauxRemiseMaximum,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfTauxRemiseMaximum ----
    tfTauxRemiseMaximum.setEnabled(false);
    tfTauxRemiseMaximum.setHorizontalAlignment(SwingConstants.RIGHT);
    tfTauxRemiseMaximum.setModeReduit(true);
    tfTauxRemiseMaximum.setMaximumSize(new Dimension(40, 22));
    tfTauxRemiseMaximum.setMinimumSize(new Dimension(40, 22));
    tfTauxRemiseMaximum.setPreferredSize(new Dimension(40, 22));
    tfTauxRemiseMaximum.setName("tfTauxRemiseMaximum");
    add(tfTauxRemiseMaximum,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOriginePump ----
    lbOriginePump.setModeReduit(true);
    lbOriginePump.setText("Origine PUMP");
    lbOriginePump.setName("lbOriginePump");
    add(lbOriginePump,
        new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOriginePump ----
    tfOriginePump.setModeReduit(true);
    tfOriginePump.setEnabled(false);
    tfOriginePump.setName("tfOriginePump");
    add(tfOriginePump,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPump ----
    lbPump.setModeReduit(true);
    lbPump.setText("PUMP");
    lbPump.setName("lbPump");
    add(lbPump,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPump ----
    tfPump.setModeReduit(true);
    tfPump.setEnabled(false);
    tfPump.setName("tfPump");
    tfPump.addSNComposantListener(new InterfaceSNComposantListener() {
      @Override
      public void valueChanged(SNComposantEvent e) {
        tfPumpValueChanged(e);
      }
    });
    add(tfPump,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbTypePrv ----
    lbTypePrv.setModeReduit(true);
    lbTypePrv.setText("Type PRV");
    lbTypePrv.setName("lbTypePrv");
    add(lbTypePrv,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfTypePrv ----
    tfTypePrv.setEnabled(false);
    tfTypePrv.setModeReduit(true);
    tfTypePrv.setName("tfTypePrv");
    add(tfTypePrv,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOriginePrv ----
    lbOriginePrv.setModeReduit(true);
    lbOriginePrv.setText("Origine PRV");
    lbOriginePrv.setName("lbOriginePrv");
    add(lbOriginePrv,
        new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOriginePrv ----
    tfOriginePrv.setModeReduit(true);
    tfOriginePrv.setEnabled(false);
    tfOriginePrv.setName("tfOriginePrv");
    add(tfOriginePrv,
        new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrv ----
    lbPrv.setModeReduit(true);
    lbPrv.setText("PRV");
    lbPrv.setName("lbPrv");
    add(lbPrv,
        new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrv ----
    tfPrv.setModeReduit(true);
    tfPrv.setEnabled(false);
    tfPrv.setName("tfPrv");
    tfPrv.addSNComposantListener(new InterfaceSNComposantListener() {
      @Override
      public void valueChanged(SNComposantEvent e) {
        tfPrvValueChanged(e);
      }
    });
    add(tfPrv,
        new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbNumeroTVAEtablissement;
  private SNComboBoxReduit cbNumeroTVAEtablissement;
  private SNLabelChamp lbUniteVente;
  private SNTexte tfUniteVente;
  private SNLabelChamp lbRemisable;
  private SNCheckBoxReduit cbRemisable;
  private SNLabelChamp lbTauxRemiseMaximum;
  private SNTexte tfTauxRemiseMaximum;
  private SNLabelChamp lbOriginePump;
  private SNTexte tfOriginePump;
  private SNLabelChamp lbPump;
  private SNMontant tfPump;
  private SNLabelChamp lbTypePrv;
  private SNTexte tfTypePrv;
  private SNLabelChamp lbOriginePrv;
  private SNTexte tfOriginePrv;
  private SNLabelChamp lbPrv;
  private SNMontant tfPrv;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
