/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.snplagedocumentvente;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import ri.serien.libcommun.gescom.vente.document.plage.PlageDocumentVente;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.documentvente.sndocumentvente.SNDocumentVente;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Composant de saisie de plage de document de ventes.<br>
 * Ce composant doit être utilisé pour affichage et choix d'une plage de document de ventes.<br>
 * Utilisation : <br>
 * - Ajouter le composant à un écran.<br>
 * - Définir une SessionBase et un IdEtablissement pour ce composant.<br>
 * - Appeler la méthode rafraichir() après avoir défini la SessionBase et l'IdEtablissement pour que ce composant les transmette aux
 * composants SNDocumentVente.<br>
 * - Récupérer la plage de document de ventes grâce à la méthode getPlageDocumentVente().<br>
 * Comportement :<br>
 * - Si le numéro de document de ventes de début est supérieur au numéro de document de ventes de fin, ce dernier sera effacé.<br>
 * - Si le numéro de document de ventes de fin est inférieur au numéro de document de ventes de début, ce dernier sera effacé.
 */
public class SNPlageDocumentVente extends SNComposantAvecEtablissement {
  private PlageDocumentVente plageDocumentVente = new PlageDocumentVente();
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public SNPlageDocumentVente() {
    super();
    initComponents();
    tfDocumentVenteDebut.lierComposantFin(tfDocumentVenteFin);
    tfDocumentVenteFin.lierComposantDebut(tfDocumentVenteDebut);
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Gérer l'activation des composants.
   */
  @Override
  public void setEnabled(boolean pIsEnabled) {
    lbDocumentVenteDebut.setEnabled(pIsEnabled);
    lbDocumentVenteFin.setEnabled(pIsEnabled);
    tfDocumentVenteDebut.setEnabled(pIsEnabled);
    tfDocumentVenteFin.setEnabled(pIsEnabled);
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraichir le composant.
   */
  public void rafraichir() {
    rafraichirDocumentVenteDebut();
    rafraichirDocumentVenteFin();
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Contrôler l'identifiant de document de ventes de fin.
   * @return
   *         true si le document de ventes de fin est supérieur ou égal au document de ventes de début.<br>
   *         false si le document de ventes de fin est inférieur au document de ventes de début.
   */
  private boolean isDocumentVenteFinValide() {
    if (plageDocumentVente.getIdDocumentVenteDebut() != null) {
      if (plageDocumentVente.getIdDocumentVenteFin() != null) {
        // Comparer uniquement quand les deux valeurs sont renseignées
        if (plageDocumentVente.getIdDocumentVenteDebut().compareTo(plageDocumentVente.getIdDocumentVenteFin()) > 0) {
          return false;
        }
      }
    }
    
    // Il se peut que l'un des valeurs ou les deux ne sont pas renseignées
    return true;
  }

  /**
   * Contrôler l'identifiant de document de ventes de début.
   * @return
   *         true si le document de ventes de début est inférieur ou égal au document de ventes de fin.<br>
   *         false si le document de ventes de début est supérieur au document de ventes de fin.
   */
  private boolean isDocumentVenteDebutValide() {
    if (plageDocumentVente.getIdDocumentVenteFin() != null) {
      if (plageDocumentVente.getIdDocumentVenteDebut() != null) {
        // Comparer uniquement quand les deux valeurs sont renseignées
        if (plageDocumentVente.getIdDocumentVenteFin().compareTo(plageDocumentVente.getIdDocumentVenteDebut()) < 0) {
          return false;
        }
      }
    }

    // Il se peut que l'un des valeurs ou les deux ne sont pas renseignées
    return true;
  }
  
  /**
   * Rafraichir le composant document de ventes de début.
   */
  private void rafraichirDocumentVenteDebut() {
    tfDocumentVenteDebut.setSession(getSession());
    tfDocumentVenteDebut.setIdEtablissement(getIdEtablissement());
    if (!isDocumentVenteFinValide()) {
      tfDocumentVenteFin.setSelectionParId(null);
      plageDocumentVente.setIdDocumentVenteFin(null);
      return;
    }
    tfDocumentVenteDebut.setSelectionParId(plageDocumentVente.getIdDocumentVenteDebut());
  }

  /**
   * Rafraichir le composant document de ventes de fin.
   */
  private void rafraichirDocumentVenteFin() {
    tfDocumentVenteFin.setSession(getSession());
    tfDocumentVenteFin.setIdEtablissement(getIdEtablissement());
    if (!isDocumentVenteDebutValide()) {
      tfDocumentVenteDebut.setSelectionParId(null);
      plageDocumentVente.setIdDocumentVenteDebut(null);
      return;
    }
    tfDocumentVenteFin.setSelectionParId(plageDocumentVente.getIdDocumentVenteFin());
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Retourner la plage de document de ventes.
   * @return La plage de document de ventes.
   */
  public PlageDocumentVente getPlageDocumentVente() {
    return plageDocumentVente;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier la valeur de l'identifiant document de ventes de début.
   */
  private void tfDocumentVenteDebutValueChanged(SNComposantEvent e) {
    try {
      plageDocumentVente.setIdDocumentVenteDebut(tfDocumentVenteDebut.getIdSelection());
      rafraichirDocumentVenteDebut();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Modifier la valeur de l'identifiant document de ventes de fin.
   */
  private void tfDocumentVenteFinValueChanged(SNComposantEvent e) {
    try {
      plageDocumentVente.setIdDocumentVenteFin(tfDocumentVenteFin.getIdSelection());
      rafraichirDocumentVenteFin();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbDocumentVenteDebut = new SNLabelChamp();
    tfDocumentVenteDebut = new SNDocumentVente();
    lbDocumentVenteFin = new SNLabelChamp();
    tfDocumentVenteFin = new SNDocumentVente();

    // ======== this ========
    setMinimumSize(new Dimension(457, 70));
    setPreferredSize(new Dimension(500, 70));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 204, 189, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };

    // ---- lbDocumentVenteDebut ----
    lbDocumentVenteDebut.setText("Document de ventes de d\u00e9but");
    lbDocumentVenteDebut.setName("lbDocumentVenteDebut");
    add(lbDocumentVenteDebut,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    // ---- tfDocumentVenteDebut ----
    tfDocumentVenteDebut.setMaximumSize(new Dimension(32767, 32767));
    tfDocumentVenteDebut.setPreferredSize(new Dimension(350, 30));
    tfDocumentVenteDebut.setName("tfDocumentVenteDebut");
    tfDocumentVenteDebut.addSNComposantListener(e -> tfDocumentVenteDebutValueChanged(e));
    add(tfDocumentVenteDebut,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

    // ---- lbDocumentVenteFin ----
    lbDocumentVenteFin.setText("Document de ventes de fin");
    lbDocumentVenteFin.setName("lbDocumentVenteFin");
    add(lbDocumentVenteFin,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

    // ---- tfDocumentVenteFin ----
    tfDocumentVenteFin.setMaximumSize(new Dimension(32767, 32767));
    tfDocumentVenteFin.setPreferredSize(new Dimension(350, 30));
    tfDocumentVenteFin.setName("tfDocumentVenteFin");
    tfDocumentVenteFin.addSNComposantListener(e -> tfDocumentVenteFinValueChanged(e));
    add(tfDocumentVenteFin,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbDocumentVenteDebut;
  private SNDocumentVente tfDocumentVenteDebut;
  private SNLabelChamp lbDocumentVenteFin;
  private SNDocumentVente tfDocumentVenteFin;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
