/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.dialoguestandard.attente;

import java.awt.Window;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.SwingWorker;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.mvc.InterfaceSoumission;

/**
 * Cette classe permet d'afficher une boite de dialogue pendant un traitement long, une fois le traitement terminé la boite de dialogue se
 * ferme automatiquement.
 * On peut personnaliser le message à afficher pendant le traitement.
 */
public class SwingWorkerAttente extends SwingWorker<Void, Void> {
  // Variables
  private Window parent = null;
  private ModeleDialogueAttente modeleAttente = null;
  private DialogueAttente vueAttente = null;
  private InterfaceSoumission objetMetier = null;

  /**
   * Constructeur.
   */
  private SwingWorkerAttente(Window pParent, InterfaceSoumission pObjet) {
    parent = pParent;
    objetMetier = pObjet;
    initialiserComposants();
  }

  /**
   * Initialise les composants nécessaires.
   */
  private void initialiserComposants() {
    modeleAttente = new ModeleDialogueAttente();
    vueAttente = new DialogueAttente(modeleAttente);

    // Détection de l'arrêt du traitement
    addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("state")) {
          if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
            vueAttente.cacher();
          }
        }
      }
    });
  }

  /**
   * Traitement à effetcuer en tâche de fond.
   */
  @Override
  protected Void doInBackground() throws Exception {
    try {
      // Nommer la thread pour la repérer dans les traces
      Thread.currentThread().setName("SwingWorkerAttente.doInBackground");
      if (objetMetier != null) {
        objetMetier.executer();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
    return null;
  }

  /**
   * Affiche la boite de dialogue et démarre le traitement en tache de fond.
   */
  public static void executer(Window pParent, InterfaceSoumission pObjet, String pMessage) {
    SwingWorkerAttente attente = new SwingWorkerAttente(pParent, pObjet);

    // Exécute doit être placée avant l'affichage car la boite de dialogue est modale
    attente.execute();
    // On personnalise le message et on affiche
    attente.getModeleAttente().setMessage(pMessage);
    attente.getVueAttente().afficher();
  }

  // -- Accesseurs

  public ModeleDialogueAttente getModeleAttente() {
    return modeleAttente;
  }

  public DialogueAttente getVueAttente() {
    return vueAttente;
  }
}
