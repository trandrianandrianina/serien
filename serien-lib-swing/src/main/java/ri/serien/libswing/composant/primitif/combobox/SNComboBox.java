/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.combobox;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyVetoException;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Liste déroulante avec la présentation standard.
 *
 * Ce composant remplace JComboBox. Il est à utiliser pour toutes les saisies de listes déroulantes. Le grisé de cette liste déroulante
 * est conforme à celui du SNTextField.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Couleur de fond stdandard pour la zone de saisie.
 * - Police standard.
 * - Actif par défaut.
 */
public class SNComboBox extends JComboBox {
  private ListCellRenderer listCellRenderer = null;
  private JTextField tfSaisie = null;
  private String saisiePrecedente = "";
  private boolean modeReduit = false;
  private boolean changementMode = false;
  private Font policeStandard = SNCharteGraphique.POLICE_STANDARD;
  
  /**
   * Constructeur.
   */
  public SNComboBox() {
    super();

    // Fixer la taille standard du composant
    rafraichirAspect();
    
    // Rendre le composant transparent
    setOpaque(false);

    setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_EDITABLE);
    setEnabled(true);
    
    // Si la SNComboBox est éditable on ajoute un keyListener sur le texte saisi
    tfSaisie = (JTextField) getEditor().getEditorComponent();
    tfSaisie.addKeyListener(new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
      }
      
      @Override
      public void keyReleased(KeyEvent e) {
        if (isToucheATraiter(e) && isEditable) {
          notifierModificationTexte("keyReleased");
        }
      }
      
      @Override
      public void keyPressed(KeyEvent e) {
      }
    });
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit l'aspect du composant.
   */
  private void rafraichirAspect() {
    // Taille par défaut du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
    // Sélection de la police en fonction du mode uniquement si le mode a été modifié
    if (changementMode) {
      if (modeReduit) {
        setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE_REDUIT);
        setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE_REDUIT);
        setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE_REDUIT);
      }
      changementMode = false;
    }
    
    // Sélection des tailles en fonction du mode
    if (modeReduit) {
      policeStandard = SNCharteGraphique.POLICE_STANDARD_REDUIT;
    }
    else {
      policeStandard = SNCharteGraphique.POLICE_STANDARD;
    }
    setFont(policeStandard);
  }

  /**
   * Notifier que la SNComboBox éditable a été modifiée au clavier
   */
  private void notifierModificationTexte(String pNomComposant) {
    try {
      fireVetoableChange(getName(), saisiePrecedente, tfSaisie.getText());
      saisiePrecedente = tfSaisie.getText();
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "");
    }
  }
  
  /**
   * Tester si la touche appuyée est à traiter.
   */
  private boolean isToucheATraiter(KeyEvent pKeyEvent) {
    return pKeyEvent.getKeyCode() != KeyEvent.VK_DOWN
        && pKeyEvent.getKeyCode() != KeyEvent.VK_UP & pKeyEvent.getKeyCode() != KeyEvent.VK_LEFT
        && pKeyEvent.getKeyCode() != KeyEvent.VK_RIGHT && pKeyEvent.getKeyCode() != KeyEvent.VK_HOME
        && pKeyEvent.getKeyCode() != KeyEvent.VK_SHIFT && pKeyEvent.getKeyCode() != KeyEvent.VK_ESCAPE;
  }
  
  // -- Accesseurs
  
  /**
   * Active ou non le mode réduit du composant.
   * @param pActiverModeReduit
   */
  public void setModeReduit(boolean pActiverModeReduit) {
    // Contrôle s'il y a un chagement de mode (réduit ou non)
    if (modeReduit != pActiverModeReduit) {
      changementMode = true;
    }
    else {
      changementMode = false;
    }
    modeReduit = pActiverModeReduit;
    rafraichirAspect();
  }
  
  /**
   * Retourne si le mode réduit est actif.
   * @return
   */
  public boolean isModeReduit() {
    return modeReduit;
  }

  /**
   * Activer ou désactiver le composant.
   **/
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    
    if (!pEnabled) {
      super.setRenderer(new DefaultListCellRenderer() {
        @Override
        public void paint(Graphics g) {
          setForeground(SNCharteGraphique.COULEUR_TEXTE_CHAMP_DESACTIVE);
          getEditor().getEditorComponent().setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
          Rectangle bounds = getBounds();
          g.setColor(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
          g.fillRect(1, 1, bounds.width, bounds.height);
          super.paint(g);
        }
      });
    }
    else if (listCellRenderer != null) {
      super.setRenderer(listCellRenderer);
    }
    else {
      setRenderer(new DefaultListCellRenderer());
    }
  }
  
  /**
   * Modifier le renderer de la liste déroulante.
   * Le renderer est stocké car la méthode utilisée pour griser le composant consiste à modifier le renderer. Il faut pouvoir remettre le
   * renderer initial lorsque le composant est dégrisé.
   */
  @Override
  public void setRenderer(ListCellRenderer pListCellRenderer) {
    listCellRenderer = pListCellRenderer;
    super.setRenderer(pListCellRenderer);
  }
}
