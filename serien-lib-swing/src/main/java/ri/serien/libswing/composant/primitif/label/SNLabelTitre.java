/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.label;

import javax.swing.SwingConstants;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Libellé de titre.
 *
 * Ce composant remplace JLabel et dérive de SNLabel.
 * Il est à utiliser pour tous les libellés contenant un titre de section ou un titre de tableau.
 * Il utilise la police dédiée au titre, alignée vers la gauche. Le titre est également aligné vers le bas pour accentuer son lien avec
 * le composant dont il est le titre, situé en dessous, et augmenter la marge avec le composant situé dessus.
 * Si le message est important il sera affiché en rouge sinon en noir.
 *
 * D'autres composants libellés sont disponibles :
 * - SNLabelChamp : pour les libellés de champs standards.
 * - SNLabelUnite : pour les unités associées à un champ.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Police pour les titres.
 * - Aligné à droite.
 * - Aligné vers le bas.
 */
public class SNLabelTitre extends SNLabel {
  /**
   * Constructeur.
   */
  public SNLabelTitre() {
    super();
    
    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_LIBELLE);

    // Rendre le composant transparent
    setOpaque(false);
    
    // Fixer la couleur de fond (utile vu que c'est transparent ?)
    setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_EDITABLE);

    // Utilise rla police par défaut
    setFont(SNCharteGraphique.POLICE_TITRE);

    // Justifier à gauche et aligner en bas
    setHorizontalAlignment(SwingConstants.LEFT);
    setVerticalAlignment(SwingConstants.BOTTOM);
  }
}
