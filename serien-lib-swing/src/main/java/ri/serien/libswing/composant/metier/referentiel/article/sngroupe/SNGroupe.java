/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.sngroupe;

import ri.serien.libcommun.gescom.personnalisation.groupe.Groupe;
import ri.serien.libcommun.gescom.personnalisation.groupe.IdGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupe.ListeGroupe;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection des groupes d'article.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un groupe dans le logiciel. Il gère l'affichage des groupes existants et
 * la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNGroupe à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant.
 * - Utiliser charger() pour charger les données. Les données n'ont pas changées si la configuration n'a pas changé.
 * Il est possible de forcer le rechargement via le paramètre pForcerRafraichissement.
 *
 * Voir un exemple d'implémentation A IMPLEMENTER
 */
public class SNGroupe extends SNComboBoxObjetMetier<IdGroupe, Groupe, ListeGroupe> {
  private SNFamille snFamille = null;
  private SNSousFamille snSousFamille = null;
  
  // Constructeur par défaut
  public SNGroupe() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeGroupe());
  }
  
  /**
   * Sélectionner un groupe de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampGroupe) {
    String valeurGroupe = pLexical.HostFieldGetData(pChampGroupe);
    if (valeurGroupe == null) {
      throw new MessageErreurException("Impossible de sélectionner le groupe car le nom du champ est invalide.");
    }
    
    // Construire l'identifiant du groupe
    IdGroupe idGroupe = null;
    if (!valeurGroupe.trim().isEmpty()) {
      idGroupe = IdGroupe.getInstance(getIdEtablissement(), valeurGroupe);
    }
    
    // Sélectionner le groupe dans la liste déroulante
    setIdSelection(idGroupe);
  }
  
  /**
   * Renseigner les champs RPG correspondant au groupe sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampGroupe) {
    IdGroupe idGroupe = getIdSelection();
    if (idGroupe != null) {
      pLexical.HostFieldPutData(pChampGroupe, 0, idGroupe.getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampGroupe, 0, "");
    }
  }
  
  /**
   * Etre notifié lorsque la sélection change.
   */
  @Override
  public void valueChanged() {
    // Rafraîchir la liste sélectionnable du composant famille si le groupe change
    if (snFamille != null) {
      snFamille.rafraichirListeSelectionnable();
    }
    
    // Rafraîchir la liste sélectionnable du composant sous-famille si le groupe change
    if (snSousFamille != null) {
      snSousFamille.rafraichirListeSelectionnable();
    }
    
    // Rafraîchir la liste sélectionnable des composants famille et sous-famille débuts liés
    if (getComposantDebut() != null) {
      SNGroupe snGroupeDebut = (SNGroupe) getComposantDebut();
      if (snGroupeDebut.getComposantFamille() != null) {
        snGroupeDebut.getComposantFamille().rafraichirListeSelectionnable();
      }
      if (snGroupeDebut.getComposantSousFamille() != null) {
        snGroupeDebut.getComposantSousFamille().rafraichirListeSelectionnable();
      }
    }
    
    // Rafraîchir la liste sélectionnable des composants famille et sous-famille fins liés
    if (getComposantFin() != null) {
      SNGroupe snGroupeFin = (SNGroupe) getComposantFin();
      if (snGroupeFin.getComposantFamille() != null) {
        snGroupeFin.getComposantFamille().rafraichirListeSelectionnable();
      }
      if (snGroupeFin.getComposantSousFamille() != null) {
        snGroupeFin.getComposantSousFamille().rafraichirListeSelectionnable();
      }
    }
  }
  
  /**
   * Retourner le composant famille lié à ce composant.
   */
  public SNFamille getComposantFamille() {
    return snFamille;
  }
  
  /**
   * Lier un composant famille à ce composant.
   */
  public void lierComposantFamille(SNFamille psnFamille) {
    // Tester si al valeur a changée
    if (Constantes.equals(psnFamille, snFamille)) {
      return;
    }
    
    // Mettre à jour la valeur
    snFamille = psnFamille;
    
    // Faire le lien dans l'autre sens
    snFamille.lierComposantGroupe(this);
    
    // Faire également le lien avec la sous-famille si on la connaît
    if (snSousFamille != null) {
      snSousFamille.lierComposantFamille(snFamille);
    }
    
    // Rafraichir la liste des composants sélectionnable
    rafraichirListeSelectionnable();
  }
  
  /**
   * Retourner le composant sous-famille lié à ce composant.
   */
  public SNSousFamille getComposantSousFamille() {
    return snSousFamille;
  }
  
  /**
   * Lier un composant sous-famille à ce composant.
   */
  public void lierComposantSousFamille(SNSousFamille psnSousFamille) {
    // Tester si al valeur a changée
    if (Constantes.equals(psnSousFamille, snSousFamille)) {
      return;
    }
    
    // Mettre à jour la valeur
    snSousFamille = psnSousFamille;
    
    // Faire le lien dans l'autre sens
    snSousFamille.lierComposantGroupe(this);
    
    // Faire également le lien avec la famille si on la connaît
    if (snFamille != null) {
      snFamille.lierComposantSousFamille(snSousFamille);
    }
    
    // Rafraichir la liste des composants sélectionnable
    rafraichirListeSelectionnable();
  }
}
